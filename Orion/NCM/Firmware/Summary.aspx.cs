﻿using SolarWinds.NCMModule.Web.Resources.Firmware;
using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;
using System.Data;
using SolarWinds.InformationService.Contract2;

public partial class Orion_NCM_Firmware_Summary : System.Web.UI.Page
{
    private SolarWinds.NCMModule.Web.Resources.Calendar calendar = new SolarWinds.NCMModule.Web.Resources.Calendar();
    
    private static List<UpgradeNodeOptions> OperationNodes
    {
        get { return FirmwareUpgradeWizardController.FirmwareOperationNodes; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = FirmwareUpgradeWizardController.PageTitle;
        LoadCalendar();

        if (!Page.IsPostBack)
        {
            LoadScheduleOptions();
        }
    }

    private void LoadCalendar()
    {
        calendar.ID = @"ScheduleCalendar";
        calendar.TimeSpan = DateTime.Now.AddMinutes(10);
        Calendar.Controls.Add(calendar);
    }

    private void LoadScheduleOptions()
    {
        var isWrapper = new ISWrapper();
        using (var proxy = isWrapper.Proxy)
        {
            var dt = proxy.Query(@"SELECT RunAt FROM NCM.FirmwareOperations WHERE ID=@id", new PropertyBag() { { @"id", FirmwareUpgradeWizardController.FirmwareOperationID } });

            if (dt != null && dt.Rows.Count > 0)
            {
                var runAt = dt.AsEnumerable().Select(r => r.IsNull("RunAt") ? DateTime.Now.AddMinutes(10) : r.Field<DateTime>("RunAt")).FirstOrDefault();
                calendar.TimeSpan = runAt != null ? runAt.ToLocalTime() : DateTime.Now.AddMinutes(10);
            }
            else
                calendar.TimeSpan = DateTime.Now.AddMinutes(10);
            rbSpecTime.Checked = true;
        }
    }

    protected void Next_Click(object sender, FirmwareUpgradeWizardEventArgs e)
    {
        if (ValidateCheckWord() && ValidateSchedule())
        {
            var runAt = rbSpecTime.Checked ? calendar.TimeSpan.ToUniversalTime() : (DateTime?) null;

            RunUpgrade(FirmwareUpgradeWizardController.FirmwareOperationID, OperationNodes, runAt, FirmwareUpgradeWizardController.FirmwareEmailSettings);
            FirmwareUpgradeWizardController.ClearSessionData();
            e.IsValid = true;
        }
        else
        {
            e.IsValid = false;
        }
    }

    private bool ValidateSchedule()
    {
        if (rbSpecTime.Checked)
            return ValidateDate();
        else
            return true;
    }

    private bool ValidateDate()
    {
        if (calendar.IsEmpty() || !calendar.ValidateDate() || calendar.TimeSpan < DateTime.Now)
        {
            valCalendar.IsValid = false;
            return false;
        }

        return true;
    }

    private bool ValidateCheckWord()
    {
        var isCorrect = String.Equals(txtCheckword.Text, Resources.NCMWebContent.FirmwareUpgradeWizard_YES);
        if (!isCorrect)
            valCheckword.IsValid = false;

        return isCorrect;
    }

    private void RunUpgrade(int operationId, List<UpgradeNodeOptions> nodeOptions, DateTime? runAt, EmailSettings emailSettings)
    {
        var isWrapper = new ISWrapper();
        using (var proxy = isWrapper.Proxy)
        {
            proxy.Cirrus.FirmwareOperations.StartUpgrade(operationId, nodeOptions, runAt, emailSettings);
        }
    }
}