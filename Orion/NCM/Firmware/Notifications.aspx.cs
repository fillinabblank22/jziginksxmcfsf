﻿using SolarWinds.InformationService.Contract2;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.Firmware;
using System;
using System.Data;
using System.Linq;

public partial class Orion_NCM_Firmware_Notifications : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = FirmwareUpgradeWizardController.PageTitle;

        if (!Page.IsPostBack)
            LoadEmailSettings();
    }

    protected void Next_Click(object sender, FirmwareUpgradeWizardEventArgs e)
    {
        SaveEmailSettings();
    }

    private void LoadEmailSettings()
    {
        if (FirmwareUpgradeWizardController.FirmwareEmailSettings == null)
        {
            var isWrapper = new ISWrapper();
            using (var proxy = isWrapper.Proxy)
            {
                var dt = proxy.Query(@"SELECT EmailSettings FROM NCM.FirmwareOperations WHERE ID=@id", new PropertyBag() { { @"id", FirmwareUpgradeWizardController.FirmwareOperationID } });
                if (dt != null && dt.Rows.Count > 0)
                {
                    FirmwareUpgradeWizardController.FirmwareEmailSettings = dt.AsEnumerable().Select(r =>
                        r.IsNull("EmailSettings") ?
                        new EmailSettings()
                        {
                            EmailFrom = proxy.Cirrus.Settings.GetSetting(Settings.DefaultEmailFromName, Resources.NCMWebContent.Settings_DefaultEmailSenderName, null),
                            EmailReplyAddress = proxy.Cirrus.Settings.GetSetting(Settings.DefaultEmailReplyAddress, @"SolarWinds@yourdomain.name", null),
                            EmailSubject = Resources.NCMWebContent.FirmwareUpgradeWizard_EmailSubject,
                            EmailRecipient = proxy.Cirrus.Settings.GetSetting(Settings.DefaultEmailTo, string.Empty, null),
                            EmailCC = proxy.Cirrus.Settings.GetSetting(Settings.DefaultEmailCC, string.Empty, null),
                            EmailBCC = proxy.Cirrus.Settings.GetSetting(Settings.DefaultEmailBCC, string.Empty, null)
                        } :
                        EmailSettings.Deserialize(r.Field<string>("EmailSettings"))).FirstOrDefault();
                }

                if (FirmwareUpgradeWizardController.FirmwareEmailSettings == null)
                {
                    FirmwareUpgradeWizardController.FirmwareEmailSettings = new EmailSettings()
                    {
                        EmailFrom = proxy.Cirrus.Settings.GetSetting(Settings.DefaultEmailFromName, Resources.NCMWebContent.Settings_DefaultEmailSenderName, null),
                        EmailReplyAddress = proxy.Cirrus.Settings.GetSetting(Settings.DefaultEmailReplyAddress, @"SolarWinds@yourdomain.name", null),
                        EmailSubject = Resources.NCMWebContent.FirmwareUpgradeWizard_EmailSubject,
                        EmailRecipient = proxy.Cirrus.Settings.GetSetting(Settings.DefaultEmailTo, string.Empty, null),
                        EmailCC = proxy.Cirrus.Settings.GetSetting(Settings.DefaultEmailCC, string.Empty, null),
                        EmailBCC = proxy.Cirrus.Settings.GetSetting(Settings.DefaultEmailBCC, string.Empty, null)
                    };
                }
            }
        }

        rbEnableEmailInfo.Checked = FirmwareUpgradeWizardController.FirmwareEmailSettings.EmailResults;
        rbDisableEmailInfo.Checked = !FirmwareUpgradeWizardController.FirmwareEmailSettings.EmailResults;
        txtSenderName.Text = FirmwareUpgradeWizardController.FirmwareEmailSettings.EmailFrom;
        txtReplyAddress.Text = FirmwareUpgradeWizardController.FirmwareEmailSettings.EmailReplyAddress;
        txtSubject.Text = FirmwareUpgradeWizardController.FirmwareEmailSettings.EmailSubject;
        txtTo.Text = FirmwareUpgradeWizardController.FirmwareEmailSettings.EmailRecipient;
        txtEmailCC.Text = FirmwareUpgradeWizardController.FirmwareEmailSettings.EmailCC;
        txtEmailBCC.Text = FirmwareUpgradeWizardController.FirmwareEmailSettings.EmailBCC;
    }

    private void SaveEmailSettings()
    {
        if (FirmwareUpgradeWizardController.FirmwareEmailSettings != null)
        {
            FirmwareUpgradeWizardController.FirmwareEmailSettings.EmailFrom = txtSenderName.Text;
            FirmwareUpgradeWizardController.FirmwareEmailSettings.EmailReplyAddress = txtReplyAddress.Text;
            FirmwareUpgradeWizardController.FirmwareEmailSettings.EmailSubject = txtSubject.Text;
            FirmwareUpgradeWizardController.FirmwareEmailSettings.EmailRecipient = txtTo.Text;
            FirmwareUpgradeWizardController.FirmwareEmailSettings.EmailCC = txtEmailCC.Text;
            FirmwareUpgradeWizardController.FirmwareEmailSettings.EmailBCC = txtEmailBCC.Text;
            FirmwareUpgradeWizardController.FirmwareEmailSettings.EmailResults = rbEnableEmailInfo.Checked;
        }
        else
            Page.Response.Redirect(FirmwareUpgradeWizardController.GetReturnUrl());
    }
}