﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="Notifications.aspx.cs" Inherits="Orion_NCM_Firmware_Notifications" %>

<%@ Register Src="~/Orion/NCM/Firmware/Controls/FirmwareUpgradeWizardContainer.ascx" TagName="WizardContainer" TagPrefix="ncm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmHeadPlaceHolder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Resources/Jobs/Jobs.css" />
    <style type="text/css">
        .EmailSettings {
            padding-top: 20px;
            padding-bottom: 10px;
            width: 90%;
        }
        
        .TextField {
            width: 50%;
        }
    </style>

    <script type="text/javascript">
        function AddCCClick() {
            $("#ccBody").show();
            $("#<%=btnAddCC.ClientID%>").hide();
            return false;
        }

        function AddBCCClick() {
            $("#bccBody").show();
            $("[id$='_btnAddBCC']").hide();
            return false;
        }

        function EnableMailValidators(isEnable) {
            var validator1 = $("#<%=rfvSenderName.ClientID%>")[0];
            var validator2 = $("#<%=rfvReplyAddress.ClientID%>")[0];
            var validator3 = $("#<%=rfvTo.ClientID%>")[0];

            ValidatorEnable(validator1, isEnable);
            ValidatorEnable(validator2, isEnable);
            ValidatorEnable(validator3, isEnable);
        }

        $(function () {
            var disableEnableEmailInfo = $("#<%=rbDisableEmailInfo.ClientID%>").prop('checked');
            if (disableEnableEmailInfo) {
                $('.EmailSettings').hide();
                EnableMailValidators(false);
            }
            else {
                $('.EmailSettings').show();
                EnableMailValidators(true);
            }

            $("#<%=rbEnableEmailInfo.ClientID%>").change(function () {
                if ($(this).is(":checked")) {
                    $('.EmailSettings').show();
                    EnableMailValidators(true);
                }
            });

            $("#<%=rbDisableEmailInfo.ClientID%>").change(function () {
                if ($(this).is(":checked")) {
                    $('.EmailSettings').hide();
                    EnableMailValidators(false);
                }
            });

            if ($("#<%= txtEmailCC.ClientID%>").val() != "") {
                AddCCClick();
            }

            if ($("#<%= txtEmailBCC.ClientID%>").val() != "") {
                AddBCCClick();
            }
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmExportToPDFPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ncmCustomizePagePlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="ncmHelpIconPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
    <ncm:WizardContainer ID="wizard" runat="server" OnNextClick="Next_Click">
        <Content>
            <div class="EnableEmailToolbar">
                <asp:RadioButton ID="rbEnableEmailInfo" GroupName="EnableEmail" Checked="true" Text="<%$ Resources: NCMWebContent, WEBDATA_RB_01 %>" runat="server" />
                <div style="display:inline;margin-left:20px;">
                    <asp:RadioButton ID="rbDisableEmailInfo" GroupName="EnableEmail" Text="<%$ Resources: NCMWebContent, WEBDATA_RB_02 %>" runat="server" />
                </div>
            </div>

            <div class="EmailSettings">
                <div style="padding-bottom:5px;">
                    <b><%= Resources.NCMWebContent.WEBDATA_IC_70 %></b>
                </div>
                <div>
                    <asp:TextBox ID="txtSenderName" runat="server"  CssClass="TextField" MaxLength="4000" Width="50%" />
                    <asp:RequiredFieldValidator ID="rfvSenderName" runat="server" Display="Dynamic" ControlToValidate="txtSenderName" ErrorMessage="*" style="padding-left:5px;"></asp:RequiredFieldValidator>
                </div>
                <div style="padding-top:20px;padding-bottom:5px;">
                    <b><%= Resources.NCMWebContent.WEBDATA_IC_71 %></b>
                </div>
                <div>
                    <asp:TextBox ID="txtReplyAddress" runat="server" CssClass="TextField" MaxLength="4000" Width="50%" />
                    <asp:RequiredFieldValidator ID="rfvReplyAddress" runat="server" Display="Dynamic" ControlToValidate="txtReplyAddress" ErrorMessage="*" style="padding-left:5px;"></asp:RequiredFieldValidator>
                </div>
                <div style="padding-top:20px;padding-bottom:5px;">
                    <b><%= Resources.NCMWebContent.WEBDATA_IC_72 %></b>
                </div>
                <div>
                    <asp:TextBox ID="txtSubject" runat="server" CssClass="TextField" MaxLength="4000"/>                            
                </div>

                <div style="padding-top:20px;padding-bottom:5px;">
                    <b><%= Resources.NCMWebContent.WEBDATA_IC_73 %></b>
                </div>
                <asp:TextBox ID="txtTo" runat="server" CssClass="TextField" MaxLength="4000" Width="50%"/>
                <asp:RequiredFieldValidator ID="rfvTo" runat="server" Display="Dynamic" ControlToValidate="txtTo" ErrorMessage="*" style="padding-left:5px;"></asp:RequiredFieldValidator>
                <div style="margin-left:10px;display:inline;">
                    <orion:LocalizableButton runat="server" ID="btnAddCC" OnClientClick="return AddCCClick();" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_IC_74 %>" DisplayType="Small" CausesValidation="false" />
                    <span><orion:LocalizableButton runat="server" ID="btnAddBCC" OnClientClick="return AddBCCClick();" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_IC_75 %>" DisplayType="Small" CausesValidation="false"/></span>
                </div>
                <div class="Hint">
                    <%= Resources.NCMWebContent.WEBDATA_VM_168 %>
                </div>
                <div id="ccBody"style="display:none;">
                    <div style="padding-top:20px;padding-bottom:5px;">
                        <b><%= Resources.NCMWebContent.WEBDATA_IC_76 %></b>
                    </div>
                    <div>
                        <asp:TextBox ID="txtEmailCC" runat="server" TextMode="SingleLine" CssClass="TextField" Width="50%" />
                    </div>
                </div>
                <div id="bccBody"style="display:none;">
                    <div style="padding-top:20px;padding-bottom:5px;">
                        <b><%= Resources.NCMWebContent.WEBDATA_IC_77 %></b>
                    </div>
                    <div>
                        <asp:TextBox ID="txtEmailBCC" runat="server" TextMode="SingleLine" CssClass="TextField" Width="50%" />
                    </div>
                </div>

                <div style="padding-top:50px;">
                    <span class="Suggestion">
                        <span class="Suggestion-Icon"></span> <%=string.Format(Resources.NCMWebContent.FirmwareUpgradeWizard_SMTPServerDetailsNote, @"<a href='/Orion/NCM/Admin/Settings/SMTPServer.aspx' target='_blank'>", @"</a>")%>
                    </span>
                </div>
            </div>
        </Content>
    </ncm:WizardContainer>
</asp:Content>

<asp:Content ID="Content8" ContentPlaceHolderID="ncmOutsideFormPlaceHolder" Runat="Server">
</asp:Content>

