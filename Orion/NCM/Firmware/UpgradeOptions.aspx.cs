﻿using SolarWinds.NCMModule.Web.Resources.Firmware;
using System;
using System.Linq;
using SolarWinds.NCM.Common.Help;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_Firmware_UpgradeOptions : System.Web.UI.Page
{
    protected string LearnMore(string fragment)
    {
        var helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
        return helpHelperWrap.GetHelpUrl(fragment);
    }

    protected int ConfirmedNodes
    {
        get { return FirmwareUpgradeWizardController.FirmwareOperationNodes.Count(n => n.Confirmed); }
    }

    protected int AllNodes
    {
        get { return FirmwareUpgradeWizardController.FirmwareOperationNodes.Count; }
    }

    protected int NodesWithFreeSpaceIssues
    {
        get { return FirmwareUpgradeWizardController.FirmwareOperationNodes.Count(n => n.UnableToDetectFreeSpace); }
    }

    protected string ButtonNextClientID
    {
        get { return wizard.ButtonNextClientID; }
    }

    protected bool HidePathToUploadImage
    {
        get
        {
            var firmwareOperationNode = FirmwareUpgradeWizardController.FirmwareOperationNodes.FirstOrDefault();
            if (firmwareOperationNode == null)
            {
                return false;
            }

            return !firmwareOperationNode.Definition.HasUploadImagePath();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = FirmwareUpgradeWizardController.PageTitle;
    }
}