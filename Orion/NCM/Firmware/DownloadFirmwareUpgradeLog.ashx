﻿<%@ WebHandler Language="C#" Class="DownloadFirmwareUpgradeLog" %>

using System;
using System.Globalization;
using System.Web;
using System.Web.SessionState;

public class DownloadFirmwareUpgradeLog : IHttpHandler, IRequiresSessionState
{
    public void ProcessRequest(HttpContext context)
    {
        try
        {
            var id = Convert.ToInt32(context.Request[@"ID"]);
            var name = context.Request[@"Name"];

            var isLayer = new SolarWinds.NCMModule.Web.Resources.ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                var log = string.Empty;
                
                var dt = proxy.Query(@"SELECT Log FROM NCM.FirmwareOperationsView WHERE ID=@id", new SolarWinds.InformationService.Contract2.PropertyBag() { { @"id", id } });
                if (dt != null && dt.Rows.Count > 0)
                    log = Convert.ToString(dt.Rows[0][0], CultureInfo.InvariantCulture);

                var fileName =
                    $@"""{SolarWinds.NCMModule.Web.Resources.CommonHelper.EncodeFilenameIfNeeded(context, name)}.log""";

                context.Response.Clear();
                context.Response.ContentType = @"application/txt";
                context.Response.AddHeader(@"Content-Disposition", @"attachment; filename=" + fileName);
                context.Response.Write(log);
            }
        }
        catch (Exception ex)
        {
            context.Response.Clear();
            var divFormat = @"<div style=""color:Red;font-size:12px;font-family: Arial,Verdana,Helvetica,sans-serif;"">{0}</div>";
            if (ex.GetType() == typeof(System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>))
                context.Response.Write(string.Format(divFormat, (ex as System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>).Detail.Message));
            else
                context.Response.Write(string.Format(divFormat, ex.Message));
        }
        context.ApplicationInstance.CompleteRequest();
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}