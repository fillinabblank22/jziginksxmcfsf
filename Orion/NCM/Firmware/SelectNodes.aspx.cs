﻿using SolarWinds.NCMModule.Web.Resources.Firmware;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using SolarWinds.InformationService.Contract2;
using SolarWinds.Logging;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_Firmware_SelectNodes : System.Web.UI.Page
{
    private const string Key = "CBBB32E9-F4EB-41C5-B050-484576BFDABE";

    private static readonly Log log = new Log();
    private readonly AdvToobarNodes toolBarNodes = new AdvToobarNodes();

    private bool IsBackFromUpgradeOperationsStep
    {
        get
        {
            return FirmwareUpgradeWizardController.FirmwareOperationID > 0 &&
                   string.IsNullOrEmpty(FirmwareUpgradeWizardController.FirmwareOperationName) &&
                   FirmwareUpgradeWizardController.FirmwareImages == null;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        ResourceSettings resourceSettings = new ResourceSettings();
        resourceSettings.Prefix = "FirmwareSelectNodes";
        NPMDefaultSettingsHelper.SetUserSettings("FirmwareSelectNodesGroupBy", "Vendor");

        toolBarNodes.ResourceSettings = resourceSettings;
        toolBarNodes.Visible = true;
        toolBarNodes.ShowClearAll = true;
        toolBarNodes.ShowSelectAll = true;
        toolBarNodes.NodesControl.ShowConfigs = false;
        toolBarNodes.NodesControl.ShowGroupCheckBoxes = true;
        toolBarNodes.NodesControl.ShowNodeCheckBoxes = true;
        toolBarNodes.NodesControl.ExcludeUnmanagedNodes = false;
        toolBarNodes.NodesControl.TreeControl.ClientSideEvents.NodeChecked = @"UltraWebTree_TheeLevelNodeCheckedDownloadConfig";

        nodes_container.Controls.Add(toolBarNodes);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = FirmwareUpgradeWizardController.PageTitle;

        if (!Page.IsPostBack)
        {
            HttpContext.Current.Session[@"SelectClearAllNodes"] = null;
            if (IsBackFromUpgradeOperationsStep)
            {
                FetchOperationInfo(FirmwareUpgradeWizardController.FirmwareOperationID);
            }
        }
    }

    private void FetchOperationInfo(int operationId)
    {
        const string swqlFormat = @"
        SELECT Name,
               DefinitionID
        FROM NCM.FirmwareOperations
        WHERE ID = @operationId";

        var isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            var dt = proxy.Query(swqlFormat, new PropertyBag() { { @"operationId", operationId } });
            var info = dt.AsEnumerable().FirstOrDefault();
            if (info != null)
            {
                FirmwareUpgradeWizardController.FirmwareOperationName = (string)info["Name"];
                FirmwareUpgradeWizardController.FirmwareDefinitionId = (int)info["DefinitionID"];
                var nodeOptions = FirmwareUpgradeWizardController.FirmwareOperationNodes.First();

                FirmwareUpgradeWizardController.FirmwareImages = nodeOptions != null
                    ? nodeOptions.Definition.FirmwareImages
                    : new List<FirmwareImage>();
            }
            else
            {
                log.WarnFormat("FirmwareOperation with ID {0} is no longer exist", operationId);
            }
        }
    }

    protected void Next_Click(object sender, FirmwareUpgradeWizardEventArgs e)
    {
        var coreNodeIds = toolBarNodes.NodesControl.GetCheckedCoreNodeIds().ToList();
        if (coreNodeIds.Count > 0)
        {
            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                if (FirmwareUpgradeWizardController.FirmwareOperationID > 0)
                {
                    proxy.Cirrus.FirmwareOperations.DeleteFirmwareOperations(new int[] { FirmwareUpgradeWizardController.FirmwareOperationID });
                }

                if (FirmwareUpgradeWizardController.FirmwareDefinitionId != null && FirmwareUpgradeWizardController.FirmwareOperationName != null)
                {
                    try
                    {
                        var definitionId = (int)FirmwareUpgradeWizardController.FirmwareDefinitionId;
                        var operationName = FirmwareUpgradeWizardController.FirmwareOperationName;
                        var images = FirmwareUpgradeWizardController.FirmwareImages;

                        proxy.Cirrus.FirmwareOperations.PrepareFirmwareUpgrade(coreNodeIds, definitionId, operationName, images);
                    }
                    catch (Exception ex)
                    {
                        log.Error("Failed to start PrepareFirmwareUpgrade", ex);
                    }
                }
            }

            FirmwareUpgradeWizardController.ClearSessionData();
            e.IsValid = true;
            Page.Response.Redirect(FirmwareUpgradeWizardController.GetReturnUrl());
        }
        else
        {
            e.IsValid = false;
            var selectNodesScript = $@"
            Ext.Msg.show({{
                title: '{Resources.NCMWebContent.FirmwareUpgradeWizard_SelectNodesDialogTitle}',
                msg: '{Resources.NCMWebContent.FirmwareUpgradeWizard_SelectNodesDialogMessage}',
                minWidth: 500,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.WARNING
            }});";
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), Key, selectNodesScript, true);
        }
    }
}