﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="Summary.aspx.cs" Inherits="Orion_NCM_Firmware_Summary" %>

<%@ Register Src="~/Orion/NCM/Firmware/Controls/FirmwareUpgradeWizardContainer.ascx" TagName="WizardContainer" TagPrefix="ncm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmHeadPlaceHolder" Runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />

    <script type="text/javascript" src="/Orion/NCM/JavaScript/main.js"></script>
    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Firmware/js/ReorderGrid.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Firmware/js/PollersGrid.js"></script>
    
    
   <script type="text/javascript"> 
       function clientValidateYes(source, arg)
       {
           arg.IsValid = arg.Value === '<%=Resources.NCMWebContent.FirmwareUpgradeWizard_YES%>';
       }


       function hideCalendar() {
           var calendarExtenderBehaviour = $find("calendarExtenderBehaviorIDScheduleCalendar");

           if (calendarExtenderBehaviour) {
               calendarExtenderBehaviour.hide();
           }
       }

   </script>
    
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
    <style type="text/css">
        .ContentSection {
            padding-bottom: 30px;
        }

        .SchduleTable {
            display: inline-table;
            padding-left: 10px;
            padding-top: 5px;
        }

        .ScheduleCells {
            display: table-cell;
            vertical-align: middle;
            line-height: 25px
        }

        .ShceduleRadioButtons {
            padding-right: 15px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmExportToPDFPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ncmCustomizePagePlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="ncmHelpIconPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
    <ncm:WizardContainer ID="wizard" runat="server" OnNextClick="Next_Click" OnNextClientClick="SW.NCM.Firmware.ReorderGrid.save();" OnBackClientClick="SW.NCM.Firmware.ReorderGrid.save();">
        <Content ID="WizzardBodyContent">
            <div style="padding-bottom:10px;">
                <%=Resources.NCMWebContent.FirmwareUpgradeWizard_SummaryUpgradeDescription%>
            </div>
            <div style="padding:10px;border:#e6e6dd 1px solid;">
                <div class="ContentSection" style="width:100%;">
                    <table cellpadding="0" cellspacing="0" style="width:100%;padding-top:5px;">
                        <tr>
                            <td>
                                <b><%=Resources.NCMWebContent.FirmwareUpgradeWizard_UpgradeOrderLabel%></b>
                            </td>
                            <td/>
                            <td>
                                <b><%=Resources.NCMWebContent.FirmwareUpgrade_PollerSpaceSummary%></b>
                            </td>
                        </tr>
                        <tr>
                            <td id="gridCell" style="width:70%;padding-right:10px;">
                                <div id="Grid"> </div>
                            </td>
                            <td id="orderedBtns" style="width:5%;">
                                <asp:ImageButton ID="btnMoveTop" runat="server" style="padding-top:2px;padding-bottom:2px;" Tooltip="<%$ Resources: NCMWebContent, WEBDATA_VK_1037 %>" OnClientClick=" SW.NCM.Firmware.ReorderGrid.moveRow('top'); return false;" ImageUrl="~/orion/images/SubViewImages/MoveTop.png"/><br/>
                                <asp:ImageButton ID="btnMoveUp" runat="server" style="padding-top:2px;padding-bottom:2px;" Tooltip="<%$ Resources: NCMWebContent, WEBDATA_VK_1038 %>" OnClientClick="SW.NCM.Firmware.ReorderGrid.moveRow('up'); return false;" ImageUrl="~/orion/images/SubViewImages/MoveUp.png"/><br/>
                                <asp:ImageButton ID="btnMoveDown" runat="server" style="padding-top:2px;padding-bottom:2px;" Tooltip="<%$ Resources: NCMWebContent, WEBDATA_VK_1039 %>" OnClientClick="SW.NCM.Firmware.ReorderGrid.moveRow('down'); return false;" ImageUrl="~/orion/images/SubViewImages/MoveDown.png"/><br/>
                                <asp:ImageButton ID="btnMoveBottom" runat="server" style="padding-top:2px;padding-bottom:2px;" Tooltip="<%$ Resources: NCMWebContent, WEBDATA_VK_1040 %>" OnClientClick="SW.NCM.Firmware.ReorderGrid.moveRow('bottom'); return false;" ImageUrl="~/orion/images/SubViewImages/MoveBottom.png"/><br/>
                            </td>
                            <td id="pollersGridCell" style="width:25%">
                                <div id="PollersGrid"> </div>
                            </td>

                        </tr>
                    </table>
                </div>
                <div class="ContentSection">
                    <div>
                        <b><%=Resources.NCMWebContent.FirmwareUpgradeWizard_ScheduleOptionsLabel%></b>
                    </div>
                    <div class="SchduleTable">
                        <div class="ScheduleCells">
                            <asp:RadioButton ID="rbRunImmediately" GroupName="scheduleOption" runat="server" Text="<%$ Resources: NCMWebContent, FirmwareUpgradeWizard_RunImmediatelyRadioButtonText %>" CssClass="ShceduleRadioButtons" onclick="hideCalendar();"></asp:RadioButton>
                            <asp:RadioButton ID="rbSpecTime" GroupName="scheduleOption" runat="server" Text="<%$ Resources: NCMWebContent, FirmwareUpgradeWizard_SpecifiedTimeRadioButtonText %>" CssClass="ShceduleRadioButtons"></asp:RadioButton>
                        </div>
                        <div id="Calendar" runat="server" style="width: 200px;" class="ScheduleCells"></div>
                        <asp:CustomValidator ClientIDMode="Static" ID="valCalendar" runat="server" ErrorMessage="*"></asp:CustomValidator>
                    </div>
                </div>
                <div class="ContentSection">
                    <span><%=Resources.NCMWebContent.FirmwareUpgradeWizard_InformationLabel%></span>
                    <div style="padding-top:5px;">
                        <span><%=string.Format(Resources.NCMWebContent.FirmwareUpgradeWizard_TypeYESToContinueLabel, Resources.NCMWebContent.FirmwareUpgradeWizard_YES)%> </span><asp:TextBox ID="txtCheckword" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="ReqFdValCheckword" runat="server" ErrorMessage="*" ControlToValidate="txtCheckword"></asp:RequiredFieldValidator>
                        <asp:CustomValidator id="valCheckword" ControlToValidate="txtCheckword" ClientValidationFunction="clientValidateYes" ErrorMessage="*" runat="server"/>
                    </div>
                </div>
            </div>
        </Content>
    </ncm:WizardContainer>
</asp:Content>

<asp:Content ID="Content8" ContentPlaceHolderID="ncmOutsideFormPlaceHolder" Runat="Server">
</asp:Content>

