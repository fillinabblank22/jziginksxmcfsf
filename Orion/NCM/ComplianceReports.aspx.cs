using System;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCM.Contracts.InformationService;

public partial class ComplianceReports : OrionView
{
    private const string NCM_VIEW_TYPE = "NCMComplianceReport";

    protected int disabledReportValue = (int)ReportStatus.Disabled;
    protected int enabledReportValue = (int)ReportStatus.Enabled;

    private readonly ICommonHelper commonHelper;

    public ComplianceReports()
    {
        commonHelper = ServiceLocator.Container.Resolve<ICommonHelper>();
    }

    public override string ViewType => NCM_VIEW_TYPE;

    protected string HelpLink => string.Empty;

    protected override void OnInit(EventArgs e)
    {
        SWISValidator validator = new SWISValidator();
        validator.ContentContainer = ContentContainer;
        validator.RedirectIfValidationFailed = true;
        ErrorContainer.Controls.Add(validator);

        if (validator.IsValid)
        {
            commonHelper.Initialise();
        }

        base.OnInit(e);
    }
}
