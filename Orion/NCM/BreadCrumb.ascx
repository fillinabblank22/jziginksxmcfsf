﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BreadCrumb.ascx.cs" Inherits="Orion_NCM_BreadCrumb" %>
<div style="margin:5px 0px 0px 10px;">
	<orion:DropDownMapPath ID="ctrSiteMapPath" Provider="AdminSitemapProvider" OnInit="OnSiteMapPath_OnInit" runat="server">
		<RootNodeTemplate>
			<a href="<%# Eval(@"url") %>"><u> <%# Eval(@"title") %></u> </a>
		</RootNodeTemplate>
		<CurrentNodeTemplate>
			<a href="<%# Eval(@"url") %>"> <%# Eval(@"title") %> </a>
		</CurrentNodeTemplate>
	</orion:DropDownMapPath>
</div>