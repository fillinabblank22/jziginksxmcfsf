using System;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Core.Common;
using SolarWinds.NCMModule.Web.Resources;
using SWUI = SolarWinds.Orion.Web.UI;
using StringRegistrar = SolarWinds.Orion.Core.Common.i18n.Registrar.ResourceManagerRegistrar;

public partial class Orion_NCM_Discovery_NcmDevicesDiscoveryPlugin : System.Web.UI.UserControl
{
    private static Log _log;
    private static Log _logger { get { return _log ?? (_log = new Log()); } }

    protected int NcmNodeCount { get; set; }
    protected int CoreNodeCount { get; set; }
    protected string AllowedNumberOfNodes { get;set;}

    public string ButtonTextAddDevice
    {
        get
        {
            return StringRegistrar.Instance.GetResourceString(@"Core.Strings", @"CommonButtonType_AddDevice") ?? Resources.NCMWebContent.DiscoveryPlugin_AddSingleNode_ButtonText;
        }
    }

    public string ButtonTextDiscoverNodes
    {
        get
        {
            return StringRegistrar.Instance.GetResourceString(@"Core.Strings", @"CommonButtonType_DiscoverNodes") ?? Resources.NCMWebContent.DiscoveryPlugin_DiscoverMyNetwork_ButtonText;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var validator = new SWISValidator();
        validator.OnlyValidateIS = true;
        validator.ContentContainer = ncmDiscoveryPluginContainer;
        ncmExceptionWarning.Controls.Add(validator);

        if (validator.IsValid)
        {
            try
            {
                var featureManager = new FeatureManager();
                var maxCountOfNodes = featureManager.GetMaxElementCount(@"NCM.Nodes");
                if (maxCountOfNodes > 1000000)
                {
                    AllowedNumberOfNodes = Resources.NCMWebContent.WEBDATA_VK_486;
                }
                else
                {
                    AllowedNumberOfNodes = string.Format(Resources.NCMWebContent.WEBDATA_VK_487, maxCountOfNodes);
                }

                using (var webDAL = new WebDAL())
                {
                    CoreNodeCount = webDAL.GetNodeCount();
                }

                var isLayer = new ISWrapper();
                using (var proxy = isLayer.Proxy)
                {
                    var dt = proxy.Query(@"SELECT COUNT(NCMNodeProperties.NodeID) AS NodesCount FROM Cirrus.NodeProperties AS NCMNodeProperties INNER JOIN Orion.Nodes AS Nodes ON NCMNodeProperties.CoreNodeID=Nodes.NodeID");
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        NcmNodeCount = Convert.ToInt32(dt.Rows[0][0]);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("Cannot get number of managed objects. Details: {0}", ex);
            }

            DataBind();
        }
    }

    protected void btnDiscoverNodes_Click(object sender, EventArgs e)
    {
        LeaveDiscoveryCentral(sender);
    }

    protected void btnAddNode_Click(object sender, EventArgs e)
    {
        LeaveDiscoveryCentral(sender);
    }

    protected void btnManageMoreNodes_Click(object sender, EventArgs e)
    {
        LeaveDiscoveryCentral(sender);
    }

    private void LeaveDiscoveryCentral(object sender)
    {
        var btn = sender as LocalizableButton;
        DiscoveryCentralHelper.Leave();
        Response.Redirect(btn.CommandArgument);
    }

    protected SWUI.ButtonType GetDiscoverNodesButtonDisplayType(int coreNodeCount)
    {
        if (coreNodeCount == 0)
        {
            return SWUI.ButtonType.Primary;
        }
        return SWUI.ButtonType.Secondary;
    }

    protected SWUI.ButtonType GetManageMoreNodesButtonDisplayType(int coreNodeCount, int ncmNodeCount)
    {
        if (coreNodeCount > 0 && ncmNodeCount == 0)
        {
            return SWUI.ButtonType.Primary;
        }
        return SWUI.ButtonType.Secondary;
    }

    protected string GetBackgroundStyle(int coreNodeCount, int ncmNodeCount)
    {
        if ((coreNodeCount == 0 && ncmNodeCount == 0) ||
            (coreNodeCount > 0 && ncmNodeCount == 0))
        {
            return @"background-color:#fffdcc;";
        }

        return string.Empty;
    }
}
