﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Discovery/Config/DiscoveryWizardPage.master" AutoEventWireup="true"
    CodeFile="NCMCredentials.aspx.cs" Inherits="Orion_Discovery_Config_NCMCredentials" Title="<%$ Resources: NCMWebContent,WEBDATA_DP_01%>" %>

<%@ Register Src="~/Orion/Controls/TransparentBox.ascx" TagPrefix="orion" TagName="TransparentBox" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />

    <style type="text/css">
        .ncm_infoIcon {
            background: transparent url(/Orion/NCM/Resources/images/Icon.Info.gif) scroll no-repeat center;
            padding: 2px 0px 2px 20px;
        }

        .ncm_EditControls > input, select {
            height: 18px;
        }
    </style>

    <script type="text/javascript">
        function pageLoad() {
            initTooltip('ColHeaderInfo', '<asp:Literal runat="server" Text="<%$ Resources: NCMWebContent,Config_NCMCredentials_ColHeaderInfo%>" />');
            initTooltip('LabelInfo', '<asp:Literal runat="server" Text="<%$ Resources: NCMWebContent,Config_NCMCredentials_LabelInfo%>" />');
            initTooltip('CheckboxInfo', '<asp:Literal runat="server" Text="<%$ Resources: NCMWebContent,Config_NCMCredentials_CheckboxInfo%>" />');

            (function initCheckbox() {
                if (document.getElementById("AutoTestWhenAllowed"))
                    document.getElementById("AutoTestWhenAllowed").textContent =
                        '<asp:Literal runat="server" Text="<%$ Resources: NCMWebContent,Config_NCMCredentials_AutoTestWhenAllowed%>" />'; 
                if (document.getElementById("AutomaticallyTestCheckbox"))
                    document.getElementById("AutomaticallyTestCheckbox").textContent =
                        '<asp:Literal runat="server" Text="<%$ Resources: NCMWebContent,Config_NCMCredentials_AutomaticallyTestCheckbox%>" />';
            })();
        }

        function initTooltip(targetEl, msg) {
            var toolTip = new Ext.ToolTip({
                anchor: 'top',
                html: String.format('<table><tr><td style="padding:5px;font-size:8pt;">{0}</td></tr></table>', msg),
                trackMouse: true
            });
            toolTip.initTarget(targetEl);
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" />            
<asp:UpdatePanel ID="CredentialsUpdatePanel" runat="server">
    <ContentTemplate>
        <asp:PlaceHolder ID="ncmActionName" runat="server">
            <div class="ActionName"><%=Resources.NCMWebContent.Config_NCMCredentials_Heading %></div>
        </asp:PlaceHolder>

        <asp:PlaceHolder ID="ncmControls" runat="server">            
            <div style="margin-top: 15px; vertical-align: middle;">
                <span><%=Resources.NCMWebContent.Config_NCMCredentials_ListHeading %></span>
                <div style="padding-left:15px;padding-top:5px;">
                    <div>-&nbsp;<%=Resources.NCMWebContent.Config_NCMCredentials_ListItem1 %></div>
                    <div>-&nbsp;<%=Resources.NCMWebContent.Config_NCMCredentials_ListItem2 %></div>
                    <div>-&nbsp;<%=Resources.NCMWebContent.Config_NCMCredentials_ListItem3 %></div>
                </div>
            </div>

            <orion:TransparentBox runat="server" ID="TBox" />
            <div>
                <div style="width:100%;padding-top:10px;padding-bottom:10px;"><%=Resources.NCMWebContent.Config_NCMCredentials_Description %></div>
                <table width="100%">
                    <tr class="ButtonHeader">
                        <td style="padding-left: 10px;">
                            <asp:LinkButton ID="AddLinkButton" runat="server" OnClick="btnAddNewConnectionProfile_OnClick" CssClass="iconLink"><%=Resources.NCMWebContent.Config_NCMCredentials_AddCredentialsButton %></asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView Width="100%" ID="ConnectionProfilesGrid" runat="server" AutoGenerateColumns="False" GridLines="None" CssClass="NetObjectTable" AlternatingRowStyle-CssClass="NetObjectAlternatingRow">
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources: NCMWebContent, Config_NCMCredentials_CredentialName %>" HeaderStyle-Width="50%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblProfileName" runat="server" Text='<%#Eval(@"Name")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<span id='AutoTestWhenAllowed'></span><span id='ColHeaderInfo' class='ncm_infoIcon'></span>" HeaderStyle-Width="50%" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblUseForAutoDetect" runat="server" Text='<%#RenderUseForAutoDetect(Eval(@"UseForAutoDetect"))%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>                       
                                    <asp:TemplateField HeaderText="<%$ Resources: NCMWebContent, Config_NCMCredentials_Actions %>">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnEdit" runat="server" ToolTip="<%$ Resources: NCMWebContent, Config_NCMCredentials_EditCredentialsTooltip %>" ImageUrl="~/Orion/Discovery/images/icon_edit.gif" OnCommand="ActionItemCommand" CommandName="EditItem" CommandArgument="<%#Container.DataItemIndex %>" />
                                            <asp:ImageButton ID="btnDelete" runat="server" ToolTip="<%$ Resources: NCMWebContent, Config_NCMCredentials_DeleteCredentialsTooltip %>" ImageUrl="~/Orion/Discovery/images/icon_delete.gif" OnCommand="ActionItemCommand" CommandName="DeleteItem" CommandArgument="<%#Container.DataItemIndex %>" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <%=Resources.NCMWebContent.Config_NCMCredentials_EmptyDataTemplate %>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>

            <asp:Panel ID="ConnectionProfileDialog" runat="server" BorderWidth="1"  Visible="false" CssClass="EditPanel">
                <p class="DialogHeaderText">
                    <asp:Label ID="ActionHeader" runat="server" Font-Bold="true" Font-Size="Larger" />
                </p>
                
                <asp:Panel ID="ConnectionProfileControls" runat="server" BorderWidth="0" Visible="true" Class="EditControls ncm_EditControls">
                    <asp:HiddenField ID="Index" runat="server" />
                    
                    <div style="font-weight:bold;" class="smallTitle"><%=Resources.NCMWebContent.Config_NCMCredentials_ProfileName %></div>
                    <asp:TextBox ID="ProfileName" MaxLength="256" runat="server" TextMode="SingleLine" Width="300px" />
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                            ControlToValidate="ProfileName"
                            Display="Dynamic"
                            ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_855 %>"
                            runat="server"/>

                    <div style="font-weight:bold;" class="smallTitle"><%=Resources.NCMWebContent.Config_NCMCredentials_CLILogin %></div>
                    <asp:TextBox ID="UserName" runat="server" TextMode="SingleLine" Width="300px" />
                    <ncm:PasswordTextBox ID="UserNamePwd" runat="server" TextMode="Password" Width="300px" />

                    <div style="font-weight:bold;" class="smallTitle"><%=Resources.NCMWebContent.Config_NCMCredentials_CLIPassword %></div>
                    <ncm:PasswordTextBox ID="Password" runat="server" TextMode="Password" Width="300px" />
                
                    <div style="font-weight:bold;" class="smallTitle"><%=Resources.NCMWebContent.Config_NCMCredentials_EnableLevel %><span id='LabelInfo' class="ncm_infoIcon"></span></div>
                    <asp:DropDownList ID="EnableLevel" runat="server" Width="300px">
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_314 %>" Value="<No Enable Login>"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_315 %>" Value="enable"></asp:ListItem>
                    </asp:DropDownList>
                
                    <div style="font-weight:bold;padding-top:10px;" class="smallTitle"><%=Resources.NCMWebContent.Config_NCMCredentials_EnablePassword %></div>
                    <ncm:PasswordTextBox ID="EnablePassword" runat="server" TextMode="Password" Width="300px" />

                    <div style="font-weight:bold;" class="smallTitle"><%=Resources.NCMWebContent.Config_NCMCredentials_ExecuteCommandsUsing %></div>
                    <asp:DropDownList ID="ExecProtocol" runat="server" Width="300px">
                    </asp:DropDownList>
                
                    <div style="font-weight:bold;" class="smallTitle"><%=Resources.NCMWebContent.Config_NCMCredentials_RequestConfigUsing %></div>
                    <asp:DropDownList ID="RequestProtocol" runat="server" Width="300px" AutoPostBack="true" OnSelectedIndexChanged="RequestProtocol_SelectedIndexChanged">
                    </asp:DropDownList>

                    <div style="font-weight:bold;" class="smallTitle"><%=Resources.NCMWebContent.Config_NCMCredentials_TransferConfigUsing %></div>
                    <asp:DropDownList ID="TransferProtocol" runat="server" Width="300px">
                    </asp:DropDownList>

                    <div style="font-weight:bold;" class="smallTitle"><%=Resources.NCMWebContent.Config_NCMCredentials_TelnetPort %></div>
                    <asp:TextBox runat="server" ID="TelnetPort" Width="300px" ></asp:TextBox>
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                        ControlToValidate="TelnetPort"
                        Display="Dynamic"
                        ErrorMessage="<%$ Resources: NCMWebContent,WEBDATA_VM_144 %>"
                        runat="server"/> 
                    <asp:CompareValidator runat="server" ID="CompareValidator1" 
                        ControlToValidate="TelnetPort" 
                        ValueToCompare="0"
                        ErrorMessage="<%$ Resources: NCMWebContent,WEBDATA_VM_145 %>"
                        Operator="GreaterThan" type="Integer" Display="Dynamic" />
                    
                    <div style="font-weight:bold;" class="smallTitle"><%=Resources.NCMWebContent.Config_NCMCredentials_SSHPort %></div>
                    <asp:TextBox runat="server" ID="SSHPort" Width="300px"></asp:TextBox>
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                        ControlToValidate="SSHPort"
                        Display="Dynamic"
                        ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VM_147 %>"
                        runat="server" /> 
                    <asp:CompareValidator runat="server" ID="CompareValidator2" 
                        ControlToValidate="SSHPort" 
                        ValueToCompare="0"
                        ErrorMessage="<%$ Resources: NCMWebContent,WEBDATA_VM_148 %>"
                        Operator="GreaterThan" Type="Integer" Display="Dynamic" />

                    <div class="smallTitle"><asp:CheckBox ID="UseForAutoDetect" runat="server" Text="<span id='AutomaticallyTestCheckbox'></span><span id='CheckboxInfo' class='ncm_infoIcon'></span>" Width="300px"></asp:CheckBox></div>
                </asp:Panel>
        
                <asp:Panel ID="Buttons" runat="server" style="margin-left:15px;" CssClass="EditButtons">           
                    <div class="sw-btn-bar">
                        <orion:LocalizableButton ID="SubmitDialog" runat="server" DisplayType="Primary" OnClick="SubmitDialog_OnClick" />                
                        <orion:LocalizableButton ID="CancelDialog" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="CancelDialog_OnClick" CausesValidation="false" />
                    </div>            
                </asp:Panel>
            </asp:Panel>
        </asp:PlaceHolder>
	
        <asp:PlaceHolder ID="ncmErrorBanner" runat="server">
            <br />
            <span class="sw-suggestion sw-suggestion-warn">
                <span class="sw-suggestion-icon"></span>
                <asp:Label ID="ncmErrorMessage" runat="server" />            
            </span>
        </asp:PlaceHolder>

        <table class="NavigationButtons">
            <tr>
                <td>
                    <div class="sw-btn-bar-wizard">
                        <orion:LocalizableButton LocalizedText="Back" DisplayType="Secondary"
                            runat="server" ID="imgBack" OnClick="imgbBack_Click" CausesValidation="false" />
                        <orion:LocalizableButton LocalizedText="Next" DisplayType="Primary"
                            runat="server" ID="imgbNext" OnClick="imgbNext_Click" CssClass="NetworkNextButton"/>
                        <orion:LocalizableButton class="CancelButton" LocalizedText ="Cancel" DisplayType="Secondary" runat="server"
                            ID="imgbCancel" OnClick="imgbCancel_Click" CausesValidation="false" />
                    </div>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
