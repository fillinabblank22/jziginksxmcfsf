﻿using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.ComponentModel;
using System.ServiceModel;
using System.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.NCM.Contracts.Discovery;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;
using System.Linq;
using SolarWinds.NCM.Common.BusinessLayer.Interfaces;
using SolarWinds.NCM.Common.Dal;
using SolarWinds.NCM.Common.Transfer;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_Discovery_Config_NCMCredentials : ConfigWizardBasePage, IStep
{
    protected DiscoveryConfiguration configuration;

    private const string key = "C9D10632-1B1F-4A37-AB82-D7B42C7CF22C";
    private string script =
    @"Ext.Msg.show({{
        title: '{1}',
        msg: {0},
        minWidth: 500,
        buttons: Ext.Msg.OK,
        icon: Ext.MessageBox.ERROR
    }});";

    private readonly INcmBusinessLayerProxyHelper blProxy;
    private string[] executeScriptProtocols;
    private string[] requestConfigProtocols;

    public Orion_Discovery_Config_NCMCredentials()
    {
        blProxy = ServiceLocator.Container.Resolve<INcmBusinessLayerProxyHelper>();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        var results = blProxy.CallMain(x => x.GetCommandProtocolsWithoutGlobalProtocols());
        executeScriptProtocols = results.ExecuteScriptProtocols;
        requestConfigProtocols = results.RequestConfigProtocols;

        CommonHelper.PopulateDropDownList(results.ExecuteScriptProtocols, ExecProtocol);
        CommonHelper.PopulateDropDownList(results.RequestConfigProtocols, RequestProtocol);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        InitButtons(imgbCancel);

        bool isVisible = true;
        if (!SecurityHelper.IsValidNcmAccountRole) //NCM account role must be not 'NONE'
        {
            isVisible = false;
            ncmErrorMessage.Text = Resources.NCMWebContent.WEBDATA_IC_237;
        }

        ncmControls.Visible = isVisible;
        ncmErrorBanner.Visible = !isVisible;

        LoadConfigurationForDiscoveryPlugin();

        if (!isVisible)
            return;

        RefreshConnectionProfilesGrid();
        UserName.Visible = !HideUsernames;
        UserNamePwd.Visible = HideUsernames;
    }

    protected string Encode(object o)
    {
        return Server.HtmlEncode((string)o);
    }
    
    protected override void Next()
    {
        SaveConfigurationForDiscoveryPlugin();
    }

    protected void LoadConfigurationForDiscoveryPlugin()
    {
        configuration = ConfigWorkflowHelper.DiscoveryConfigurationInfo;
        if (configuration == null)
        {
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails
            {
                Error = new Exception(Resources.NCMWebContent.WEBDATA_DP_02),
                Title = Resources.NCMWebContent.WEBDATA_DP_03
            });
        }
    }

    protected void SaveConfigurationForDiscoveryPlugin()
    {
        // we get configuration for this plugin
        var ncmDiscoveryPluginConfiguration = configuration.GetDiscoveryPluginConfiguration<NcmDiscoveryPluginConfiguration>();
        if (ncmDiscoveryPluginConfiguration == null)
        {
            // configuration not created yet - we'll create it and register
            ncmDiscoveryPluginConfiguration = new NcmDiscoveryPluginConfiguration();
            configuration.AddDiscoveryPluginConfiguration(ncmDiscoveryPluginConfiguration);
        }
        ncmDiscoveryPluginConfiguration.AccountID = HttpContext.Current.Profile.UserName;
    }

    #region ConnectionProfiles
    protected bool HideUsernames
    {
        get
        {
            if (ViewState["NCM_HideUsernames"] == null)
            {
                var isWrapper = new ISWrapper();
                using (var proxy = isWrapper.Proxy)
                {
                    bool hideUsernames = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.HideUsernames, false, null), CultureInfo.InvariantCulture);
                    ViewState["NCM_HideUsernames"] = hideUsernames;
                }
            }
            return Convert.ToBoolean(ViewState["NCM_HideUsernames"], CultureInfo.InvariantCulture);
        }
    }

    protected List<ConnectionProfile> ConnectionProfiles
    {
        get
        {
            if (ViewState["NCM_ConnectionProfiles"] != null)
                return (List<ConnectionProfile>)ViewState["NCM_ConnectionProfiles"];

            var isWrapper = new ISWrapper();
            using (var proxy = isWrapper.Proxy)
            {
                var profiles = proxy.Cirrus.Nodes.GetAllConnectionProfiles().ToList();
                ViewState["NCM_ConnectionProfiles"] = profiles;
                return profiles;
            }
        }
    }

    protected string RenderUseForAutoDetect(object useForAutoDetect)
    {
        return Convert.ToBoolean(useForAutoDetect, CultureInfo.InvariantCulture) ? Resources.NCMWebContent.WEBDATA_VK_311 : Resources.NCMWebContent.WEBDATA_VK_312;
    }

    [Localizable(false)]
    protected void RequestProtocol_SelectedIndexChanged(object sender, EventArgs e)
    {
        TBox.ShowBlock = true;
        CommonHelper.InitializeTransferConfigProtocol(TransferProtocol, RequestProtocol.SelectedItem, false);
    }

    protected void btnAddNewConnectionProfile_OnClick(object sender, EventArgs e)
    {
        SubmitDialog.Text = Resources.NCMWebContent.Config_NCMCredentials_SubmitDialogAdd;
        ActionHeader.Text = Resources.NCMWebContent.Config_NCMCredentials_AddNewConnectionProfile;

        Index.Value = @"-1";
        SetConnectionProfile(GetDefaultConnectionProfile());

        TBox.ShowBlock = true;
        ConnectionProfileDialog.Visible = true;
    }

    protected void ActionItemCommand(object sender, CommandEventArgs e)
    {
        int index = -1;

        if (e.CommandArgument != null)
            int.TryParse(e.CommandArgument.ToString(), out index);

        switch (e.CommandName)
        {
            case "EditItem":
                EditConnectionProfile(index);
                break;
            case "DeleteItem":
                DeleteConnectionProfile(index);
                break;
        }
    }

    protected void SubmitDialog_OnClick(object sender, EventArgs e)
    {
        int index = -1;
        int.TryParse(Index.Value, out index);

        var isWrapper = new ISWrapper();
        using (var proxy = isWrapper.Proxy)
        {
            if (index >= 0) //edit mode
            {
                var profileToEdit = ConnectionProfiles[index];
                if (profileToEdit != null)
                {
                    profileToEdit = GetConnectionProfile(profileToEdit);
                    proxy.Cirrus.Nodes.UpdateConnectionProfile(profileToEdit);
                }
            }
            else //create mode
            {
                var profileToAdd = GetConnectionProfile();
                int profileId = proxy.Cirrus.Nodes.AddConnectionProfile(profileToAdd);
                profileToAdd.ID = profileId;
                ConnectionProfiles.Add(profileToAdd);
            }
        }

        ConnectionProfileDialog.Visible = false;
        RefreshConnectionProfilesGrid();
    }

    protected void CancelDialog_OnClick(object sender, EventArgs e)
    {
        ConnectionProfileDialog.Visible = false;
        RefreshConnectionProfilesGrid();
    }

    private void EditConnectionProfile(int index)
    {
        if (index >= 0)
        {
            var profileToEdit = ConnectionProfiles[index];
            if (profileToEdit != null)
            {
                SubmitDialog.Text = Resources.NCMWebContent.Config_NCMCredentials_SubmitDialogSave;
                ActionHeader.Text = Resources.NCMWebContent.Config_NCMCredentials_EditConnectionProfile;

                Index.Value = index.ToString();
                SetConnectionProfile(profileToEdit);

                TBox.ShowBlock = true;
                ConnectionProfileDialog.Visible = true;
            }
        }
    }

    private void DeleteConnectionProfile(int index)
    {
        if (index >= 0)
        {
            var profileToDelete = ConnectionProfiles[index];
            if (profileToDelete != null)
            {
                try
                {
                    var isWrapper = new ISWrapper();
                    using (var proxy = isWrapper.Proxy)
                    {
                        proxy.Cirrus.Nodes.DeleteConnectionProfile(profileToDelete.ID);
                        ConnectionProfiles.RemoveAt(index);
                    }
                    RefreshConnectionProfilesGrid();
                }
                catch (Exception ex)
                {
                    var msg = Newtonsoft.Json.JsonConvert.SerializeObject((ex is FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>) ? (ex as FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>).Detail.Message : ex.Message);
                    ScriptManager.RegisterClientScriptBlock(Page,
                        GetType(),
                        key,
                        string.Format(script, msg, Resources.NCMWebContent.Config_NCMCredentials_DeleteProfileDialogTitle),
                        true);
                }
            }
        }
    }

    private void SetConnectionProfile(ConnectionProfile profile)
    {
        ProfileName.Text = profile.Name;
        if (HideUsernames)
        {
            UserNamePwd.PasswordText = profile.UserName;
        }
        else
        {
            UserName.Text = profile.UserName;
            Password.PasswordText = profile.Password;
            EnableLevel.SelectedValue = profile.EnableLevel;
            EnablePassword.PasswordText = profile.EnablePassword;
            ExecProtocol.SelectedValue = profile.ExecuteScriptProtocol;
            RequestProtocol.SelectedValue = profile.RequestConfigProtocol;
            RequestProtocol_SelectedIndexChanged(RequestProtocol, EventArgs.Empty);
            TransferProtocol.SelectedValue = profile.TransferConfigProtocol;
            TelnetPort.Text = profile.TelnetPort.ToString(CultureInfo.InvariantCulture);
            SSHPort.Text = profile.SSHPort.ToString(CultureInfo.InvariantCulture);
            UseForAutoDetect.Checked = profile.UseForAutoDetect;
        }

    }

    private ConnectionProfile GetConnectionProfile(ConnectionProfile profile = null)
    {
        if (profile == null)
            profile = GetDefaultConnectionProfile();

        profile.Name = ProfileName.Text;
        profile.UserName = HideUsernames ? UserNamePwd.PasswordText : UserName.Text;
        profile.Password = Password.PasswordText;
        profile.EnableLevel = EnableLevel.SelectedValue;
        profile.EnablePassword = EnablePassword.PasswordText;
        profile.ExecuteScriptProtocol = ExecProtocol.SelectedValue;
        profile.RequestConfigProtocol = RequestProtocol.SelectedValue;
        profile.TransferConfigProtocol = TransferProtocol.SelectedValue;
        profile.TelnetPort = Convert.ToInt32(TelnetPort.Text, CultureInfo.InvariantCulture);
        profile.SSHPort = Convert.ToInt32(SSHPort.Text, CultureInfo.InvariantCulture);
        profile.UseForAutoDetect = UseForAutoDetect.Checked;
        return profile;
    }

    private void RefreshConnectionProfilesGrid()
    {
        TBox.ShowBlock = false;
        ConnectionProfilesGrid.DataSource = ConnectionProfiles;
        ConnectionProfilesGrid.DataBind();
    }

    [Localizable(false)]
    private ConnectionProfile GetDefaultConnectionProfile()
    {
        var fipsConfiguration = ServiceLocator.Container.Resolve<IFipsConfiguration>();
        bool isFipsEnabled = fipsConfiguration.FipsEnabledInOrion();

        return new ConnectionProfile()
        {
            UseForAutoDetect = true,
            ExecuteScriptProtocol = executeScriptProtocols[0],
            RequestConfigProtocol = requestConfigProtocols[0],
            TransferConfigProtocol = isFipsEnabled ? @"SSH2" : @"SSH auto"
        };
    }
    #endregion

    #region IStep Members
    public string Step
    {
        get
        {
            return @"NCM";
        }
    }
    #endregion
}
