<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NcmDevicesDiscoveryPlugin.ascx.cs" Inherits="Orion_NCM_Discovery_NcmDevicesDiscoveryPlugin" %>

<%@ Register Src="~/Orion/Admin/ManagedNetObjectsInfo.ascx" TagPrefix="orion" TagName="ManagedNetObjectsInfo" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>

<style type="text/css">
    #ncmDiscoveryPluginMenu div.sw-suggestion
    {
        padding:10px;
        color:black;
        font-size:small;
        font-weight:normal;
        display:block;
    }
    
    #ncmDiscoveryPluginMenu span.sw-suggestion-noicon
    {
        padding-left:0px;
    }
</style>

<div class="coreDiscoveryIcon">
    <asp:Image ID="imgDevice" runat="server" ImageUrl="~/Orion/NCM/Resources/images/device_32x32.png"/>
</div>

<div class="coreDiscoveryPluginBody" style="<%# GetBackgroundStyle(CoreNodeCount, NcmNodeCount) %>">
    <h2>
        <%=Resources.NCMWebContent.WEBDATA_VK_476 %>
    </h2>
    
    <asp:Panel id="ncmDiscoveryPluginContainer" runat="server">
    
    <div>
        <%=Resources.NCMWebContent.WEBDATA_VK_478 %>
    </div>
    
    <div id="ncmDiscoveryPluginMenu">
        <%--Discover Nodes section--%>
        <%if (NcmNodeCount == 0 && CoreNodeCount == 0)
          { %>
        <div class='sw-suggestion sw-suggestion-fail'><span class='sw-suggestion-noicon'></span>
        <%}
          else
          { %>
        <div style="padding:10px;">
        <%
          } %>
            <b>&bull;&nbsp;<%=Resources.NCMWebContent.WEBDATA_VK_479 %></b>
            <div style="padding-left:10px;">
                <%if (NcmNodeCount == 0 && CoreNodeCount == 0)
                { %>
                <img alt="" src="/Orion/NCM/Resources/images/icon_failed.gif" />&nbsp;
                <% } %>
                <%=Resources.NCMWebContent.WEBDATA_VK_480 %>
                <div style="padding-top:10px;">
                    <orion:LocalizableButton runat="server" ID="btnDiscoverNodes" LocalizedText="CustomText" 
                    Text="<%# ButtonTextDiscoverNodes %>" OnClick="btnDiscoverNodes_Click" DisplayType="<%# GetDiscoverNodesButtonDisplayType(CoreNodeCount) %>"
                    CommandArgument="~/Orion/Discovery/Default.aspx" />
                </div>
            </div>
        </div>
        
        <%--Add a New Node section--%>
        <div style="padding:10px;">
        <b>&bull;&nbsp;<%=Resources.NCMWebContent.WEBDATA_VK_481 %></b>
            <div style="padding-left:10px;">
                <%=Resources.NCMWebContent.WEBDATA_VK_482 %>
                <div style="padding-top:10px;">
                    <orion:LocalizableButton runat="server" ID="btnAddNode" LocalizedText="CustomText" 
                    Text="<%# ButtonTextAddDevice %>" OnClick="btnAddNode_Click" DisplayType="Secondary"
                    CommandArgument="~/Orion/Nodes/Add/Default.aspx" />
                </div>
            </div>
        </div>
            
        <%--Manage More Nodes section--%>
        <%if (CoreNodeCount > 0 && NcmNodeCount == 0)
          { %>
        <div class='sw-suggestion sw-suggestion-fail'><span class='sw-suggestion-noicon'></span>
        <%}
          else
          { %>
        <div style="padding:10px;">
        <%
          } %>
            <b>&bull;&nbsp;<%=Resources.NCMWebContent.WEBDATA_VK_483 %></b>
            <div style="padding-left:10px;">
                <%=Resources.NCMWebContent.WEBDATA_VK_484 %>
                <div style="padding-top:10px;">
                    <%if (CoreNodeCount > 0 && NcmNodeCount == 0)
                      { %>
                    <img alt="" src="/Orion/NCM/Resources/images/icon_failed.gif" />&nbsp;
                    <b style="color:red;"><%=string.Format(Resources.NCMWebContent.WEBDATA_VK_485, string.Empty, NcmNodeCount, string.Empty, CoreNodeCount) %></b>
                    <%}
                      else
                      { %>
                    <img alt="" src="/Orion/NCM/Resources/images/icon_ok.gif" />&nbsp;
                    <%=string.Format(Resources.NCMWebContent.WEBDATA_VK_485, @"<b>", NcmNodeCount, @"</b>", CoreNodeCount) %>
                      <%
                      } %>
                    <br /><%=AllowedNumberOfNodes %>
                </div>
                <div style="padding-top:10px;">
                    <span class="LinkArrow">&#0187;</span>
                    <orion:HelpLink ID="hlLearnMoreAboutLicensing" runat="server" HelpUrlFragment="OrionNCMPHLicenseLearnMore" HelpDescription="<%$ Resources: NCMWebContent, WEBDATA_VK_488 %>" CssClass="helpLink" />
                </div>
                <div style="padding-top:10px;">
                    <orion:LocalizableButton runat="server" ID="btnManageMoreNodes" LocalizedText="CustomText" 
                    Text="<%$ Resources: NCMWebContent, WEBDATA_VK_489 %>" OnClick="btnManageMoreNodes_Click" DisplayType="<%# GetManageMoreNodesButtonDisplayType(CoreNodeCount, NcmNodeCount) %>"
                    CommandArgument="~/Orion/Nodes/Default.aspx" />                
                </div>
            </div>
        </div>
    </div>
    
    </asp:Panel>
    <div style="padding-top:10px;" id="ncmExceptionWarning" runat="server"/>
</div>


