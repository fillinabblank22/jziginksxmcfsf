<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="ChartDrilldown.aspx.cs" Inherits="Orion_NCM_Resources_ChartDrilldown" %>

<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register TagPrefix="ncm1" TagName="IconExportToPdf" Src="~/Orion/NCM/Controls/IconExportToPdf.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ncmHeadPlaceHolder">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/PolicyReports/PolicyReport.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
    
    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippetsHelper.js"></script>
    <script type="text/javascript" src="ChartDrilldown.js" ></script>
    <ncm:ExtLocalDateTimePattern ID="ExtLocalDateTimePattern1" runat="server"/>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ncmPageTitlePlaceHolder">
	<%=Page.Title%>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" runat="server">
    <ncm:DropDownMapPath ID="NodeSiteMapPath" Provider="NCMSitemapProvider" runat="server"  OnInit="SiteMapPath_OnInit">
        <RootNodeTemplate>
            <a href="<%# Eval(@"url") %>" ><u> <%# Eval(@"displayTitle") %></u> </a>
        </RootNodeTemplate>
        <CurrentNodeTemplate>
	        <a href="<%# Eval(@"url") %>" ><%# Eval(@"displayTitle") %> </a>
	    </CurrentNodeTemplate>
   </ncm:DropDownMapPath>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmExportToPDFPlaceHolder" runat="server">
    <ncm1:IconExportToPdf ID="IconExportToPdf1" runat="server"></ncm1:IconExportToPdf>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ncmMainContentPlaceHolder" runat="server">
    <link rel="stylesheet" type="text/css" href="styles/NCMResources.css" />
	
	<script type="text/javascript">
	    SW.NCM.IsDemoMode = <%=SolarWinds.NCMModule.Web.Resources.CommonHelper.IsDemoMode().ToString().ToLowerInvariant() %>;
	    SW.NCM.ReturnUrl = '<%= ReturnUrl %>';
	    SW.NCM.GroupByData = <%=CreateGroupByData() %>;
	    SW.NCM.ConfigTypesStore = <%=GetConfigTypesStore() %>;
    </script>
    
    <asp:Panel id="ContentContainer" runat="server">
        <div style="padding:10px;">
            <asp:Panel ID="GridContainer" style="padding:10px;border:solid 1px #dcdbd7;background-color:#fff;" runat="server">
                <div style="padding-bottom:10px;text-transform:uppercase;font-size:8pt;">
                    <%=SubTitle %>
                </div>
                <div>
                    <table class="ExistingElements" cellpadding="0" cellspacing="0" width="100%">
                        <tr valign="top" align="left">
                            <td style="width: 220px;">
                                <div class="ElementGrouping">
                                    <div class="ncm_GroupSection x-panel-header x-toolbar x-small-editor">
                                        <div><%= Resources.NCMWebContent.WEBDATA_VK_80 %></div>
                                        <div id="GroupBy" style="padding:2px 0px 3px 0px;"></div>    
                                    </div>
                                    <ul class="GroupItems" style="width:220px;height:448px">
                                    </ul>
                                </div>
                            </td>
                            <td id="gridCell">
                                <div id="Grid" />
                            </td>
                        </tr>
                    </table>
                    <div id="originalQuery">
                    </div>
                    <div id="test">
                    </div>
                    <pre id="stackTrace"></pre>
                </div>
            </asp:Panel>
        </div>
    </asp:Panel>
</asp:Content>

