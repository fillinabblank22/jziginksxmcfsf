<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/NCM/NCMEditMasterPage.master" CodeFile="EWErrorPage.aspx.cs" Inherits="EWErrorPage" %>

<%@ Register Src="~/Orion/NCM/Resources/EnergyWise/EWControls/EWErrors.ascx" TagPrefix="EnergyWise" TagName="EWErrors" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" runat="server"> 
    <div style="padding:10px;font-size:11px;font-family:Arial;">
        <a title="<%=Resources.NCMWebContent.WEBDATA_VK_357 %>" style="text-decoration:underline;" href="/Orion/Admin"><%=Resources.NCMWebContent.WEBDATA_VK_357 %></a>&nbsp;<img alt="" style="vertical-align:middle;" src="/Orion/images/breadcrumb_arrow.gif"/>&nbsp;
        <a title="<%=Resources.NCMWebContent.WEBDATA_VK_358 %>" style="text-decoration:underline;" href="/Orion/Nodes/Default.aspx"><%=Resources.NCMWebContent.WEBDATA_VK_358 %></a>&nbsp;<img alt="" style="vertical-align:middle;" src="/Orion/images/breadcrumb_arrow.gif"/>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmHelpIconPlaceHolder" runat="server"> 
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMWebEWErrors" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" runat="server"> 
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmMainContentPlaceHolder" runat="server"> 
	<link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
	
	<asp:UpdatePanel ID="updatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
	        <div style="padding:10px;">
                <table cellpadding="0" cellspacing="0" width="100%" >
                    <tr>
                        <td style="padding:0px;border-width:1px;border-style:solid;border-color:#DCDBD7;background-color:#FFFFFF">                            
                            <table width="100%">
                                <tr>
                                    <td style="padding:10px;">
                                        <EnergyWise:EWErrors ID="EWErrors" runat="server"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>                    
                </table>        
            </div>            
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content> 
