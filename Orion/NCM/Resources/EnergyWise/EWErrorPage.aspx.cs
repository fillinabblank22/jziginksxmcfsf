using System;

public partial class EWErrorPage : System.Web.UI.Page
{
    #region Page Functions

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString[@"Interfaces"] != null)
        {
            Page.Title = Resources.NCMWebContent.WEBDATA_VK_428;
            EWErrors.IsManageEWNodes = false;
        }

        if (Request.QueryString[@"Nodes"] != null)
        {
            Page.Title = Resources.NCMWebContent.WEBDATA_VK_405;
            EWErrors.IsManageEWNodes = true;
        }
    }    

    #endregion
}
