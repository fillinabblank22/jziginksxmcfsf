using System;
using SolarWinds.Orion.Web.UI;

public partial class EWChartDrilldown : OrionView
{
    protected override void OnInit(EventArgs e)
    {
        PreviewMode();

        this.Title = this.ViewInfo.ViewTitle;
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    public override string ViewType
    {
        get { return @"NCMEWChartDetails"; }
    }

    private void PreviewMode()
    {
        if (string.IsNullOrEmpty(Request.QueryString[@"Drilldown"]))
            Response.Redirect($@"{Page.Request.Url}&Drilldown=True&PreviewMode=True");
    }
}