<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" CodeFile="ManageEWInterfaces.aspx.cs" Inherits="ManageEWInterfaces" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Orion/NCM/Resources/EnergyWise/EWControls/EWInterfaceInfo.ascx" TagPrefix="EnergyWise" TagName="EWInterfaceInfo" %>
<%@ Register Src="~/Orion/NCM/Resources/EnergyWise/EWControls/EWRecurrencePolicy.ascx" TagPrefix="EnergyWise" TagName="EWRecurrencePolicy" %>
<%@ Register Src="~/Orion/NCM/ModalBox.ascx" TagName="ModalBox" TagPrefix="ModalBox" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ncmPageTitlePlaceHolder">
	<%=Page.Title %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmHelpIconPlaceHolder" runat="server">
    <orion:IconHelpButton ID="helpBtn" runat="server" HelpUrlFragment="OrionNCMWebManageEWInterfaces" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" runat="server">
    <div style="padding:10px;font-size:11px;font-family:Arial;">
        <a title="<%=Resources.NCMWebContent.WEBDATA_VK_357 %>" style="text-decoration:underline;" href="/Orion/Admin"><%=Resources.NCMWebContent.WEBDATA_VK_357 %></a>&nbsp;<img alt="" style="vertical-align:middle;" src="/Orion/images/breadcrumb_arrow.gif"/>&nbsp;
        <a title="<%=Resources.NCMWebContent.WEBDATA_VK_358 %>" style="text-decoration:underline;" href="/Orion/Nodes/Default.aspx"><%=Resources.NCMWebContent.WEBDATA_VK_358 %></a>&nbsp;<img alt="" style="vertical-align:middle;" src="/Orion/images/breadcrumb_arrow.gif"/>
    </div>
</asp:Content>

<asp:Content ID="ManageEWInterfacesContent" ContentPlaceHolderID="ncmMainContentPlaceHolder" runat ="server" > 
    <style type="text/css">
        div#container
        {
	        position: static;
        }
    </style>
    
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
    <script type="text/javascript" src="../../../JavaScript/main.js"></script>
	
	<div style="padding:10px;">
	    <asp:UpdatePanel ID="updatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table cellpadding="0" cellspacing="0" width="100%" >
                    <tr>
                        <td style="padding:10px;border-width:1px; border-style:solid; border-color:#DCDBD7; background-color:#FFFFFF">
                            <table>
                                <tr id="trHolder" runat="server"></tr>
                                <tr>
                                    <td>
                                        <%=InfoTitle %>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top:10px;">
                                        <ul>
                                            <%=NodeInterfaceName %>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <EnergyWise:EWInterfaceInfo ID="EWInterfaceInfo" runat="server" />
                                                <table class="blueBox" width="100%">
                                                    <tr>
                                                        <td style="padding-left:10px;padding-bottom:10px;">
                                                            <asp:CheckBox ID="cbRecurrencePolicy" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_435 %>" runat="server" AutoPostBack="true" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-bottom:10px;">
                                                <asp:CheckBox ID="cbWritetoNvRam" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_385 %>" runat="server" AutoPostBack="false" />    
                                            </td>
                                        </tr>
                                        <tr id="ApprovalCommentsHolder" runat="server" visible="false">
                                            <td>
                                                <%=Resources.NCMWebContent.WEBDATA_VK_386 %><br />
                                                <asp:TextBox ID="txtApprovalComments" runat="server" TextMode="MultiLine" Width="300px" Height="45px" spellcheck="false" Font-Name="Arial" Font-Size="9pt" />
                                                <ajaxToolkit:TextBoxWatermarkExtender ID="WatermarkExtender" runat="server" TargetControlID="txtApprovalComments" WatermarkText="<%$ Resources: NCMWebContent, WEBDATA_VK_387 %>" WatermarkCssClass="watermark" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-bottom:10px;">                                    
                                                <EnergyWise:EWRecurrencePolicy ID="EWRecurrencePolicy" runat="server"/>                                         
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td style="padding-top:20px;padding-bottom:20px;">
                                                <orion:LocalizableButton ID="ExecuteConfigActions" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_372 %>" OnClick="ExecuteConfigActions_Click"></orion:LocalizableButton>
                                                <orion:LocalizableButton ID="Cancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" CausesValidation="false" OnClick="Cancel_Click"></orion:LocalizableButton>
                                            </td>
                                        </tr>
                                    </table>
                                </tr>
                            </table>
                        </td>
                    </tr>            
                </table>        
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    
    <ModalBox:ModalBox runat="server" ID="MsgBox" Width="550">
        <DialogContents>
            <div style="border-right: 1px solid; border-top: 1px solid; border-left: 1px solid; border-bottom: 1px solid;">
                <div style="background-image: url('/Orion/NCM/Resources/images/PopupHeader.gif'); background-repeat: repeat-x; background-position: left bottom; margin: 0 0 0 0; padding: 5px 5px 5px 5px;">
                    <table width="100%">
                        <tr>
                            <td class="modalBoxHeaderFont">
                                <%=MsgBoxTitle%>	
                            </td>
                            <td align="right">
                                <asp:ImageButton runat="server" id="btnCrossCancel" OnClick="CancelMsgBox" ImageUrl="~/Orion/NCM/Resources/images/Button.CloseCross.gif" />
                            </td>                              
                        </tr>
                    </table>
                </div>
                <table width="100%">
                    <tr>
                        <td style="padding:5px;">
                            <%=MsgBoxContent%>                            
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <orion:LocalizableButton ID="btnOK" runat="server" DisplayType="Primary" LocalizedText="Ok" OnClick="OKMsgBox"></orion:LocalizableButton>
                            <orion:LocalizableButton ID="btnCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="CancelMsgBox"></orion:LocalizableButton>
                        </td>
                    </tr>
                 </table>
            </div>
        </DialogContents>
    </ModalBox:ModalBox>  
</asp:Content>