using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.HtmlControls;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.EWBusinessLayer;
using SolarWinds.NCM.Contracts.InformationService;

using StringRegistrar = SolarWinds.Orion.Core.Common.i18n.Registrar.ResourceManagerRegistrar;
using SolarWinds.NCM.Contracts;

public partial class ManageEWInterfaces : System.Web.UI.Page
{
    #region Private Members

    private ManageInterfaceMode manageMode;
    private EWInterface currentEWInterface = new EWInterface();
    private SolarWinds.NCMModule.Web.Resources.ISWrapper ISLayer = new SolarWinds.NCMModule.Web.Resources.ISWrapper();
    private string nodeInterfaceName;
    private string nodeName;
    private int ports;
    private const string newLine = "\r\n";
    #endregion

    #region Public Members

    protected string InfoTitle { get; set; }
    protected string MsgBoxContent { get; set; }
    protected string MsgBoxTitle { get; set; }

    protected bool UseUserLevelLoginCreds
    {
        get
        {
            if (ViewState["UseUserLevelLoginCreds"] == null)
            {
                using (var proxy = ISLayer.Proxy)
                {
                    var connLevel = proxy.Cirrus.Settings.GetSetting(Settings.ConnectivityLevel, ConnectivityLevels.Default, null);
                    var useUserLevelLoginCreds = connLevel.Equals(ConnectivityLevels.Level3);
                    ViewState["UseUserLevelLoginCreds"] = useUserLevelLoginCreds;
                    return useUserLevelLoginCreds;
                }
            }
            else
            {
                return Convert.ToBoolean(ViewState["UseUserLevelLoginCreds"]);
            }
        }
    }

    #endregion

    #region Properties

    protected ManageInterfaceMode ManageMode
    {
        get { return manageMode; }
        set { manageMode = value; }
    }

    protected EWInterface CurrentEWInterface
    {
        get { return currentEWInterface; }
    }

    protected string NodeName
    {
        get { return nodeName; }
    }

    public string NodeInterfaceName
    {
        get { return nodeInterfaceName; }
        set { nodeInterfaceName = value; }
    }

    protected int Ports
    {
        get { return ports; }
    }

    #endregion

    #region Page Functions

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString[@"IsErrors"] == null)
        {
            var Url =
                $@"/Orion/NCM/Resources/EnergyWise/EWErrorPage.aspx?Interfaces={Request.QueryString[@"Interfaces"]}";
            Page.Response.Redirect(Url);
        }
        else
        {
            if (!Convert.ToBoolean(Request.QueryString[@"IsErrors"]))
            {
                trHolder.Visible = false;
                if (UseUserLevelLoginCreds)
                {
                    var suggestionWarn = new SuggestionWarning();
                    suggestionWarn.WarningMessage = string.Format(Resources.NCMWebContent.WEBDATA_VK_714, @"<a style='color:#336699;text-decoration:underline;' href='/Orion/NCM/Admin/Settings/UserLevelLoginCreds.aspx'>", @"</a>");

                    var td = new HtmlTableCell();
                    td.Style.Add(@"padding-bottom", @"10px");
                    td.Controls.Add(suggestionWarn);
                    trHolder.Cells.Add(td);
                    trHolder.Visible = true;
                }

                if (ConfigChangeApprovalHelper.ShowSendApprovalRequestButton)
                {
                    ApprovalCommentsHolder.Visible = true;
                    ExecuteConfigActions.Text = StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"LIBCODE_VK_30");
                }

                string[] interfaceIDs;
                if (Session[@"Interfaces"] != null)
                    interfaceIDs = (string[])Session[@"Interfaces"];
                else
                    interfaceIDs = Request.QueryString[@"Interfaces"].Split(',', ';');

                if (interfaceIDs.Length > 1)
                {
                    Page.Title = Resources.NCMWebContent.WEBDATA_VK_428;
                    manageMode = ManageInterfaceMode.Multiple;
                    InfoTitle = Resources.NCMWebContent.WEBDATA_VK_430;

                    LoadMultipleInterfaces(interfaceIDs);
                }
                else
                {
                    Page.Title = Resources.NCMWebContent.WEBDATA_VK_429;
                    manageMode = ManageInterfaceMode.Single;
                    InfoTitle = Resources.NCMWebContent.WEBDATA_VK_431;

                    LoadSingleInterface(interfaceIDs[0]);
                }
            }
        }
    }

    #endregion

    #region Events

    protected void Cancel_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect("/Orion/Nodes/Default.aspx");
    }

    protected void ExecuteConfigActions_Click(object sender, EventArgs e)
    {
        MsgBoxTitle = Resources.NCMWebContent.WEBDATA_VK_372;
        if (manageMode == ManageInterfaceMode.Single)
        {
            MsgBoxContent = string.Format(Resources.NCMWebContent.WEBDATA_VK_432, NodeName, @"<br/>");
        }
        else
        {
            if (cbRecurrencePolicy.Checked)
            {
                if (!EWRecurrencePolicy.MakePoliciesAdditive)
                {
                    MsgBoxContent = string.Format(Resources.NCMWebContent.WEBDATA_VK_433, Ports, @"<br/>");
                }
                else
                {
                    MsgBoxContent = string.Format(Resources.NCMWebContent.WEBDATA_VK_434, Ports, @"<br/>");
                }
            }
            else
            {
                MsgBoxContent = string.Format(Resources.NCMWebContent.WEBDATA_VK_434, Ports, @"<br/>");
            }
        }

        if (ConfigChangeApprovalHelper.ShowSendApprovalRequestButton)
        {
            if (CommonHelper.IsDemoMode())
            {
                Page.Response.Redirect("/Orion/Nodes/Default.aspx");
                return;
            }

            if (manageMode == ManageInterfaceMode.Multiple)
            {
                EcecuteConfigMutipleInterfaces();
                return;
            }
            else
            {
                ExecuteConfigSingleInterface();
                return;
            }
        }

        MsgBox.Show();
    }

    public void CancelMsgBox(object sender, EventArgs e)
    {
        MsgBox.Hide();
    }

    public void OKMsgBox(object sender, EventArgs e)
    {
        MsgBox.Hide();
        if (ConfigChangeApprovalHelper.ShowSendApprovalRequestButton)
        {
            Page.Response.Redirect("/Orion/Nodes/Default.aspx");
        }
        else
        {
            if (manageMode == ManageInterfaceMode.Multiple)
                EcecuteConfigMutipleInterfaces();
            else
                ExecuteConfigSingleInterface();

            Page.Response.Redirect("/Orion/NCM/Resources/EnergyWise/EWTransferStatus.aspx?TransferOn=Interfaces");
        }
    }

    #endregion

    #region Private Functions

    private void LoadSingleInterface(string interfaceID)
    {
        currentEWInterface.Initialize(Convert.ToInt32(interfaceID));
        nodeName = HttpUtility.HtmlEncode(currentEWInterface.NodeCaption);
        nodeInterfaceName +=
            $@"<li><a href=""/Orion/View.aspx?NetObject=I:{HttpUtility.HtmlEncode(interfaceID)}"" style=""text-decoration:underline;"">{HttpUtility.HtmlEncode(currentEWInterface.NodeCaption)} - {HttpUtility.HtmlEncode(currentEWInterface.InterfaceName)}</a></li>";

        EWInterfaceInfo.ManageMode = manageMode;
        if (!Page.IsPostBack)
        {
            EWInterfaceInfo.EnergyWiseName = currentEWInterface.EwName;
            EWInterfaceInfo.EnergyWiseKeywords = currentEWInterface.EwKeywords;
            EWInterfaceInfo.EnergyWiseDefaultPowerLevel = (currentEWInterface.EwLevel).ToString();
            EWInterfaceInfo.EnergyWiseImportance = currentEWInterface.EwImportance.ToString();
            EWInterfaceInfo.EnergyWiseRole = currentEWInterface.EwRole;
            EWInterfaceInfo.EnergyWiseEnabled = currentEWInterface.EwEnabled.ToString();

            EWRecurrencePolicy.ManageMode = ManageInterfaceMode.Single;
            EWRecurrencePolicy.DataSource = currentEWInterface.SerialiseRecurenceToDataTable();
        }

        VisibleRecurrencePolicy();
    }

    private void LoadMultipleInterfaces(string[] interfaceIDs)
    {
        ports = interfaceIDs.Length;

        foreach (var interfaceID in interfaceIDs)
        {
            var ewInterface = new EWInterface();
            ewInterface.Initialize(Convert.ToInt32(interfaceID));
            nodeInterfaceName +=
                $@"<li><a href=""/Orion/View.aspx?NetObject=I:{HttpUtility.HtmlEncode(interfaceID)}"" style=""text-decoration:underline;"">{HttpUtility.HtmlEncode(ewInterface.NodeCaption)} - {HttpUtility.HtmlEncode(ewInterface.InterfaceName)}</a></li>";
        }
        EWRecurrencePolicy.ManageMode = ManageInterfaceMode.Multiple;
        EWInterfaceInfo.ManageMode = manageMode;
        if (!Page.IsPostBack)
            cbRecurrencePolicy.Checked = false;

        VisibleRecurrencePolicy();
    }

    private void ExecuteConfigSingleInterface()
    {
        var NodeIdList = new Dictionary<string, EWNodeTransferStatus>();
        var updatedEWInterface = new EWInterface();

        updatedEWInterface.InterfaceName = CurrentEWInterface.InterfaceName;

        if (EWInterfaceInfo.IsEnergyWiseName)
            updatedEWInterface.EwName = EWInterfaceInfo.EnergyWiseName;
        else
            updatedEWInterface.EwName = null;

        if (EWInterfaceInfo.IsEnergyWiseKeywords)
            updatedEWInterface.EwKeywords = EWInterfaceInfo.EnergyWiseKeywords;
        else
            updatedEWInterface.EwKeywords = null;

        if (EWInterfaceInfo.IsEnergyWiseDefaultPowerLevel)
            updatedEWInterface.EwLevel = Convert.ToInt32(EWInterfaceInfo.EnergyWiseDefaultPowerLevel);
        else
            updatedEWInterface.EwLevel = -1;

        if (EWInterfaceInfo.IsEnergyWiseImportance)
            updatedEWInterface.EwImportance = Convert.ToInt32(EWInterfaceInfo.EnergyWiseImportance);
        else
            updatedEWInterface.EwImportance = -1;

        if (EWInterfaceInfo.IsEnergyWiseRole)
            updatedEWInterface.EwRole = EWInterfaceInfo.EnergyWiseRole;
        else
            updatedEWInterface.EwRole = null;

        updatedEWInterface.EwEnabled = Convert.ToBoolean(EWInterfaceInfo.EnergyWiseEnabled);

        string scriptResults;
        if (cbRecurrencePolicy.Checked)
        {
            updatedEWInterface.DeSerialiseRecurenceFromDataTable(EWRecurrencePolicy.DataSource);
            scriptResults = EWGeneralScriptGenerator.GenerateScript(updatedEWInterface, CurrentEWInterface.EwPolicies, EWInterfaceInfo.IsKeywordsAdditive);
        }
        else
        {
            scriptResults = EWGeneralScriptGenerator.GenerateScript(updatedEWInterface, updatedEWInterface.EwPolicies, EWInterfaceInfo.IsKeywordsAdditive);
        }

        MsgBox.Hide();

        var nodeId = new Guid[1];
        var guid = new Guid(currentEWInterface.NCMNodeID);
        nodeId.SetValue(guid, 0);

        scriptResults =
            $@"{EWGeneralScriptGenerator.CONST_EnterConfigMode}{scriptResults}{EWGeneralScriptGenerator.CONST_ExitConfigMode}";
        if (cbWritetoNvRam.Checked) scriptResults += @"${SaveConfig}";

        if (ConfigChangeApprovalHelper.ShowSendApprovalRequestButton)
        {
            var ticket = new NCMApprovalTicket();
            ticket.ConfigType = string.Empty;
            ticket.Reboot = false;
            ticket.RequestType = NCMApprovalTicket.TYPE_MANAGE_EW;
            ticket.Script = string.Empty;
            ticket.Comments = txtApprovalComments.Text;
            foreach (var id in nodeId)
            {
                ticket.AddNode(id, scriptResults);
            }
            ConfigChangeApprovalHelper.SendRequestForApproval(ticket); 
           
            txtApprovalComments.Text = string.Empty;
        }
        else
        {
            using (var proxy = ISLayer.Proxy)
            {
                var transferIdList = proxy.Cirrus.ConfigArchive.ExecuteScript(nodeId, scriptResults);
                Session[@"TransferIdList"] = transferIdList;
            }
        }

        var ewNodeTransferStatus = new EWNodeTransferStatus();
        ewNodeTransferStatus.NPMNodeID = CurrentEWInterface.NPMNodeID;
        ewNodeTransferStatus.IsPollNode = false;
        NodeIdList.Add(CurrentEWInterface.NCMNodeID.ToUpperInvariant(), ewNodeTransferStatus);

        Session[@"NodeIdList"] = NodeIdList;
    }

    private void EcecuteConfigMutipleInterfaces()
    {
        var scriptResults = new Dictionary<string, string>();
        var NodeIdList = new Dictionary<string, EWNodeTransferStatus>();

        string[] interfaceIDs;
        if (Session[@"Interfaces"] != null)
            interfaceIDs = (string[])Session[@"Interfaces"];
        else
            interfaceIDs = Request.QueryString[@"Interfaces"].Split(',', ';');

        foreach (var interfaceID in interfaceIDs)
        {
            var currentEWInterface = new EWInterface();
            var updatedEWInterface = new EWInterface();

            currentEWInterface.Initialize(Convert.ToInt32(interfaceID.Replace(@"I:", string.Empty).Trim()));

            updatedEWInterface.InterfaceName = currentEWInterface.InterfaceName;

            if (EWInterfaceInfo.IsEnergyWiseName)
                updatedEWInterface.EwName = EWInterfaceInfo.EnergyWiseName;
            else
                updatedEWInterface.EwName = null;

            if (EWInterfaceInfo.IsEnergyWiseKeywords)
                updatedEWInterface.EwKeywords = EWInterfaceInfo.EnergyWiseKeywords;
            else
                updatedEWInterface.EwKeywords = null;

            if (EWInterfaceInfo.IsEnergyWiseDefaultPowerLevel)
                updatedEWInterface.EwLevel = Convert.ToInt32(EWInterfaceInfo.EnergyWiseDefaultPowerLevel);
            else
                updatedEWInterface.EwLevel = -1;

            if (EWInterfaceInfo.IsEnergyWiseImportance)
                updatedEWInterface.EwImportance = Convert.ToInt32(EWInterfaceInfo.EnergyWiseImportance);
            else
                updatedEWInterface.EwImportance = -1;

            if (EWInterfaceInfo.IsEnergyWiseRole)
                updatedEWInterface.EwRole = EWInterfaceInfo.EnergyWiseRole;
            else
                updatedEWInterface.EwRole = null;

            updatedEWInterface.EwEnabled = Convert.ToBoolean(EWInterfaceInfo.EnergyWiseEnabled);

            string script;
            if (cbRecurrencePolicy.Checked)
            {
                updatedEWInterface.DeSerialiseRecurenceFromDataTable(EWRecurrencePolicy.DataSource);
                if (!EWRecurrencePolicy.MakePoliciesAdditive)
                {
                    script = EWGeneralScriptGenerator.GenerateScript(updatedEWInterface, currentEWInterface.EwPolicies, EWInterfaceInfo.IsKeywordsAdditive);
                }
                else
                {
                    var emptyPolicyCollection = new List<EWPolicy>();
                    script = EWGeneralScriptGenerator.GenerateScript(updatedEWInterface, emptyPolicyCollection, EWInterfaceInfo.IsKeywordsAdditive);
                }
            }
            else
            {
                script = EWGeneralScriptGenerator.GenerateScript(updatedEWInterface, updatedEWInterface.EwPolicies, EWInterfaceInfo.IsKeywordsAdditive);
            }

            string outValue;
            if (scriptResults.TryGetValue(currentEWInterface.NCMNodeID, out outValue))
                scriptResults[currentEWInterface.NCMNodeID] += $@"{newLine}{script}";
            else
            {
                scriptResults.Add(currentEWInterface.NCMNodeID, script);

                var ewNodeTransferStatus = new EWNodeTransferStatus();
                ewNodeTransferStatus.NPMNodeID = currentEWInterface.NPMNodeID;
                ewNodeTransferStatus.IsPollNode = false;
                NodeIdList.Add(currentEWInterface.NCMNodeID.ToUpperInvariant(), ewNodeTransferStatus);
            }
        }

        MsgBox.Hide();

        var ticket = new NCMApprovalTicket();
        ticket.ConfigType = string.Empty;
        ticket.Reboot = false;
        ticket.RequestType = NCMApprovalTicket.TYPE_MANAGE_EW;
        ticket.Script = string.Empty;
        ticket.Comments = txtApprovalComments.Text;        

        using (var proxy = ISLayer.Proxy)
        {
            foreach (var result in scriptResults)
            {
                var nodeId = new Guid[1];
                var guid = new Guid(result.Key);
                nodeId.SetValue(guid, 0);
                var script =
                    $@"{EWGeneralScriptGenerator.CONST_EnterConfigMode}{result.Value}{EWGeneralScriptGenerator.CONST_ExitConfigMode}";
                if (cbWritetoNvRam.Checked) script += @"${SaveConfig}";
                if (ConfigChangeApprovalHelper.ShowSendApprovalRequestButton)
                {
                    ticket.AddNode(nodeId[0], script);                   
                }
                else
                {
                    var transferIdList = proxy.Cirrus.ConfigArchive.ExecuteScript(nodeId, script);
                    Session[@"TransferIdList"] = transferIdList;
                }
            }
            if (ConfigChangeApprovalHelper.ShowSendApprovalRequestButton)
            {
                ConfigChangeApprovalHelper.SendRequestForApproval(ticket);
            }
        }

        Session[@"NodeIdList"] = NodeIdList;
    }

    private void VisibleRecurrencePolicy()
    {
        var ewEnabled = EWInterfaceInfo.EnergyWiseEnabled == string.Empty ? true : Convert.ToBoolean(EWInterfaceInfo.EnergyWiseEnabled);
        cbRecurrencePolicy.Enabled = ewEnabled;

        if (ewEnabled)
            EWRecurrencePolicy.Visible = cbRecurrencePolicy.Checked;
        else
            EWRecurrencePolicy.Visible = false;
    }

    #endregion
}
