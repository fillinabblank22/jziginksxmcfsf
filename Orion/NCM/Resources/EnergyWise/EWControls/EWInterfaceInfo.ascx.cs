using System;
using System.Web.UI.WebControls;
using SolarWinds.NCMModule.Web.Resources;

public partial class EWInterfaceInfo : System.Web.UI.UserControl
{
    #region Private Members

    private ManageInterfaceMode _mode = ManageInterfaceMode.Single;

    #endregion

    #region Properties

    public ManageInterfaceMode ManageMode
    {
        get { return _mode; }
        set { _mode = value; }
    }

    public bool IsKeywordsAdditive
    {
        get { return cbKeywordsAdditive.Checked; }
        set { cbKeywordsAdditive.Checked = value; }
    }

    public bool IsEnergyWiseName
    {
        get { return cbEWName.Checked; }
        set { cbEWName.Checked = value; }
    }

    public bool IsEnergyWiseKeywords
    {
        get { return cbEWKeywords.Checked; }
        set { cbEWKeywords.Checked = value; }
    }
    
    public bool IsEnergyWiseDefaultPowerLevel
    {
        get { return cbEWDefaultPowerLevel.Checked; }
        set { cbEWDefaultPowerLevel.Checked = value; }
    }

    public bool IsEnergyWiseImportance
    {
        get { return cbEWImportance.Checked; }
        set { cbEWImportance.Checked = value; }
    }

    public bool IsEnergyWiseRole
    {
        get { return cbEWRole.Checked; }
        set { cbEWRole.Checked = value; }
    }

    public string EnergyWiseName
    {
        get { return EWName.Text; }
        set { EWName.Text = value; }
    }

    public string EnergyWiseKeywords
    {
        get { return EWKeywords.Text; }
        set { EWKeywords.Text = value; }
    }

    public string EnergyWiseDefaultPowerLevel
    {

        get { return EWDefaultPowerLevel.SelectedValue; }
        set
        {
            if (ManageMode == ManageInterfaceMode.Single)
            {
                if (!Page.IsPostBack)
                    LoadEWDefaultPowerLevel();
            }
            EWDefaultPowerLevel.SelectedValue = value; 
        }
    }

    public string EnergyWiseImportance
    {
        get { return EWImportance.Text; }
        set { EWImportance.Text = value; }
    }

    public string EnergyWiseRole
    {
        get { return EWRole.Text; }
        set { EWRole.Text = value; }
    }

    public string EnergyWiseEnabled
    {
        get { return EWEnabled.SelectedValue; }
        set
        {
            if (ManageMode == ManageInterfaceMode.Single)
            {
                if (!Page.IsPostBack)
                    LoadEWEnabled();
            }
            EWEnabled.SelectedValue = value; 
        }
    }

    #endregion

    #region Events

    public void cbEWName_Check(object sender, EventArgs e)
    {
        if (cbEWName.Checked)
        {
            EWName.Enabled = true;
            if (ManageMode == ManageInterfaceMode.Multiple)
                EWName.Text = string.Empty;
        }
        else
        {
            EWName.Enabled = false;
            if (ManageMode == ManageInterfaceMode.Multiple)
                EWName.Text = string.Empty;
        }
    }

    public void cbEWKeywords_Check(object sender, EventArgs e)
    {
        if (cbEWKeywords.Checked)
        {
            EWKeywords.Enabled = true;
            if (ManageMode == ManageInterfaceMode.Multiple)
                EWKeywords.Text = string.Empty;
        }
        else
        {
            EWKeywords.Enabled = false;
            if (ManageMode == ManageInterfaceMode.Multiple)
                EWKeywords.Text = string.Empty;
        }
    }

    public void cbEWDefaultPowerLevel_Check(object sender, EventArgs e)
    {
        if (cbEWDefaultPowerLevel.Checked)
        {
            EWDefaultPowerLevel.Enabled = true;
            if (ManageMode == ManageInterfaceMode.Multiple)
            {
                LoadEWDefaultPowerLevel();
                EWDefaultPowerLevel.SelectedValue = @"10";
            }
        }
        else
        {
            EWDefaultPowerLevel.Enabled = false;
            if (ManageMode == ManageInterfaceMode.Multiple)
                EWDefaultPowerLevel.Items.Clear();
        }
    }

    public void cbEWImportance_Check(object sender, EventArgs e)
    {
        if (cbEWImportance.Checked)
        {
            EWImportance.Enabled = true;
            if (ManageMode == ManageInterfaceMode.Multiple)
                EWImportance.Text = string.Empty;
        }
        else
        {
            EWImportance.Enabled = false;
            if (ManageMode == ManageInterfaceMode.Multiple)
                EWImportance.Text = string.Empty;
        }
    }

    public void cbEWRole_Check(object sender, EventArgs e)
    {
        if (cbEWRole.Checked)
        {
            EWRole.Enabled = true;
            if (ManageMode == ManageInterfaceMode.Multiple)
                EWRole.Text = string.Empty;
        }
        else
        {
            EWRole.Enabled = false;
            if (ManageMode == ManageInterfaceMode.Multiple)
                EWRole.Text = string.Empty;
        }
    }

    #endregion

    #region Page Functions

    protected void Page_Load(object sender, EventArgs e)
    {
        if (ManageMode == ManageInterfaceMode.Multiple)
        {
            if (!Page.IsPostBack)
            {
                RefreshEWInterfaceInfo();
                LoadEWEnabled();

                EWEnabled.SelectedValue = @"True";
            }
        }

        if (!string.IsNullOrEmpty(EWEnabled.SelectedValue))
            EnabledEWProperties();

        cbKeywordsAdditive.Enabled = cbEWKeywords.Checked;
        if (!cbKeywordsAdditive.Enabled)
            cbKeywordsAdditive.Checked = false;
    }

    #endregion

    #region Private Functions

    private void EnabledEWProperties()
    {
        bool enabled = Convert.ToBoolean(EWEnabled.SelectedValue);
        
        cbEWName.Enabled = enabled;
        cbEWKeywords.Enabled = enabled;
        cbEWDefaultPowerLevel.Enabled = enabled;
        cbEWImportance.Enabled = enabled;
        cbEWRole.Enabled = enabled;
        cbKeywordsAdditive.Enabled = enabled;
        
        EWName.Enabled = cbEWName.Enabled && cbEWName.Checked;
        EWKeywords.Enabled = cbEWKeywords.Enabled && cbEWKeywords.Checked;
        EWDefaultPowerLevel.Enabled = cbEWDefaultPowerLevel.Enabled && cbEWDefaultPowerLevel.Checked;
        EWImportance.Enabled = cbEWImportance.Enabled && cbEWImportance.Checked;
        EWRole.Enabled = cbEWRole.Enabled && cbEWRole.Checked;
    }

    private void LoadEWDefaultPowerLevel()
    {
        ListItem item;
        EWDefaultPowerLevel.Items.Clear();
        item = new ListItem(Resources.NCMWebContent.WEBDATA_VK_413, @"0");
        EWDefaultPowerLevel.Items.Add(item);
        item = new ListItem(Resources.NCMWebContent.WEBDATA_VK_414, @"1");
        EWDefaultPowerLevel.Items.Add(item);
        item = new ListItem(Resources.NCMWebContent.WEBDATA_VK_415, @"2");
        EWDefaultPowerLevel.Items.Add(item);
        item = new ListItem(Resources.NCMWebContent.WEBDATA_VK_416, @"3");
        EWDefaultPowerLevel.Items.Add(item);
        item = new ListItem(Resources.NCMWebContent.WEBDATA_VK_417, @"4");
        EWDefaultPowerLevel.Items.Add(item);
        item = new ListItem(Resources.NCMWebContent.WEBDATA_VK_418, @"5");
        EWDefaultPowerLevel.Items.Add(item);
        item = new ListItem(Resources.NCMWebContent.WEBDATA_VK_419, @"6");
        EWDefaultPowerLevel.Items.Add(item);
        item = new ListItem(Resources.NCMWebContent.WEBDATA_VK_420, @"7");
        EWDefaultPowerLevel.Items.Add(item);
        item = new ListItem(Resources.NCMWebContent.WEBDATA_VK_421, @"8");
        EWDefaultPowerLevel.Items.Add(item);
        item = new ListItem(Resources.NCMWebContent.WEBDATA_VK_422, @"9");
        EWDefaultPowerLevel.Items.Add(item);
        item = new ListItem(Resources.NCMWebContent.WEBDATA_VK_423, @"10");
        EWDefaultPowerLevel.Items.Add(item);
    }

    private void LoadEWEnabled()
    {
        ListItem item;
        EWEnabled.Items.Clear();
        item = new ListItem(Resources.NCMWebContent.WEBDATA_VK_388, @"True");
        EWEnabled.Items.Add(item);
        item = new ListItem(Resources.NCMWebContent.WEBDATA_VK_389, @"False");
        EWEnabled.Items.Add(item);
    }

    private void RefreshEWInterfaceInfo()
    {
        cbEWName.Checked = false;
        cbEWKeywords.Checked = false;
        cbEWDefaultPowerLevel.Checked = false;
        cbEWImportance.Checked = false;
        cbEWRole.Checked = false;

        EWName.Enabled = false;
        EWKeywords.Enabled = false;
        EWDefaultPowerLevel.Enabled = false;
        EWImportance.Enabled = false;
        EWRole.Enabled = false;
    }

    #endregion
}
