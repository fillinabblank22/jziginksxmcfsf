using System;
using System.Data;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.EWBusinessLayer;

public partial class EWRecurrencePolicy : System.Web.UI.UserControl
{
    #region Private Members

    private ManageInterfaceMode _mode = ManageInterfaceMode.Single;

    #endregion

    #region Page Functions

    protected void Page_Load(object sender, EventArgs e)
    {
        DisableEditDeleteRecurrencePolicy();
        SetDaysOfWeek();

        if (RecureDD.SelectedValue.Equals(@"Advanced", StringComparison.InvariantCultureIgnoreCase))
        {
            TimeHolder.Visible = false;
            TimeValidatorsHolder.Visible = false;
            DaysHolder.Visible = false;
            AdvancedHolder.Visible = true;
            AdvancedNotesHolder.Visible = true;

            lblAdvMinute.Visible = true;
            lblAdvHour.Visible = true;
            lblAdvMonth.Visible = true;
            lblAdvDayOfMonth.Visible = true;
            lblAdvDayOfWeek.Visible = true;
        }
        else
        {
            TimeHolder.Visible = true;
            TimeValidatorsHolder.Visible = true;
            DaysHolder.Visible = true;
            AdvancedHolder.Visible = false;
            AdvancedNotesHolder.Visible = false;
            DaysErrorHolder.Visible = false;
        }
    }

    #endregion

    #region Private Functions

    private void Initialize()
    {
        if (this.IsPostBack)
        {
            this.ModalBox.Width = 700;
            this.ModalBox.Hide();
        }
    }
    
    private Control FindTemplateControl(string ID, DataListItem item)
    {
        Control findControl = null;
        foreach (Control control in item.Controls)
        {
            if (control.ClientID.Contains(ID))
            {
                findControl = control;
                break;
            }
        }
        return findControl;
    }

    private void DisableEditDeleteRecurrencePolicy()
    {
        int checked_items = 0;
        foreach (DataListItem item in this.DataView.Items)
        {
            if (((CheckBox)this.FindTemplateControl(@"ItemCheck", item)).Checked)
                checked_items++;
        }

        if (checked_items > 1 || checked_items == 0)
        {
            EditRecurrencePolicy.Enabled = false;
            DeleteRecurrencePolicy.Enabled = true;
            if (checked_items == 0)
            {
                DeleteRecurrencePolicy.Enabled = false;
            }
        }
        else
        {
            EditRecurrencePolicy.Enabled = true;
            DeleteRecurrencePolicy.Enabled = true;
        }
    }

    public string GeneratePolicyRecurenceString(string tMinutes, string tHours, string tdayofMonth, string tMonth, string tdayOfWeek, string tlevel, string timportance)
    {
        //check if this is policy is with step date
        if (!(tMinutes.Contains(@"/") || tHours.Contains(@"/") || tdayOfWeek.Contains(@"/") || tdayofMonth.Contains(@"/") || tMonth.Contains(@"/")))
        {
            if (tdayofMonth.Contains(@"*") && tMonth.Contains(@"*") && !(tMinutes.Contains(@"*") || tHours.Contains(@"*")) && !tHours.Contains(@"-") && !tMinutes.Contains(@"-"))
            {
                //this is the Weekly view so let's give a specific answer
                int hours = Int32.Parse(tHours);
                string dayOfTheWeek = "";
                if (tdayOfWeek != @"*")
                {
                    dayOfTheWeek = $@" {Resources.NCMWebContent.WEBDATA_VK_471} " + PolicyParser.Instance.ParceWeekDay(tdayOfWeek).Replace(@"-",
                                       $@" {Resources.NCMWebContent.WEBDATA_VK_472} ");
                }
                
                TimeSpan timeSpan = new TimeSpan(hours, Convert.ToInt32(tMinutes), 0);
                DateTime dateTime = new DateTime(timeSpan.Ticks);

                return string.Format(Resources.NCMWebContent.WEBDATA_VK_470, tlevel, timportance, dateTime.ToShortTimeString(), dayOfTheWeek);
            }
            else
            {
                //simple mode - represent date in user friendly form
                tdayOfWeek = (tdayOfWeek == @"*") ? Resources.NCMWebContent.WEBDATA_VK_473 : PolicyParser.Instance.ParceWeekDay(tdayOfWeek).Replace(@"-",
                    $@" {Resources.NCMWebContent.WEBDATA_VK_472} ");
                tMonth = (tMonth == @"*") ? Resources.NCMWebContent.WEBDATA_VK_473 : PolicyParser.Instance.ParceMonthes(tMonth).Replace(@"-",
                    $@" {Resources.NCMWebContent.WEBDATA_VK_472} ");

                tMinutes = (tMinutes == @"*") ? Resources.NCMWebContent.WEBDATA_VK_473 : tMinutes.Replace(@"-",
                    $@" {Resources.NCMWebContent.WEBDATA_VK_472} ");
                tHours = (tHours == @"*") ? Resources.NCMWebContent.WEBDATA_VK_473 : tHours.Replace(@"-",
                    $@" {Resources.NCMWebContent.WEBDATA_VK_472} ");
                tdayofMonth = (tdayofMonth == @"*") ? Resources.NCMWebContent.WEBDATA_VK_473 : tdayofMonth.Replace(@"-",
                    $@" {Resources.NCMWebContent.WEBDATA_VK_472} ");
            }
        }

        return string.Format(Resources.NCMWebContent.WEBDATA_VK_474, tlevel, timportance, tMinutes, tHours, tdayofMonth, tMonth, tdayOfWeek);
    }

    private void ModalBoxRefresh()
    {
        this.TitleModalBox.Text = string.Empty;
        this.HourBox.Text = @"00";
        this.MinuteBox.Text = @"00";
        this.LevelDD.SelectedIndex = 10;
        this.ImportanceTx.Text = @"100";        
        this.RecureDD.SelectedValue = @"Weekly";
        this.SundayCb.Checked = false;
        this.MondayCb.Checked = false;
        this.TuesdayCb.Checked = false;
        this.WednesdayCb.Checked = false;
        this.ThursdayCb.Checked = false;
        this.FridayCb.Checked = false;
        this.SaturdayCb.Checked = false;
        this.ItemName.Checked = false;
        this.updatePanel.Update();
        ViewState["Operation"] = string.Empty;
        ViewState["Position"] = string.Empty;
    }
    
    #endregion

    #region Events

    public void ItemName_Check(object sender, EventArgs e)
    {
        foreach (DataListItem item in this.DataView.Items)
        {
            ((CheckBox)this.FindTemplateControl(@"ItemCheck", item)).Checked = ItemName.Checked;
        }
        DisableEditDeleteRecurrencePolicy();
    }

    public void Item_Check(object sender, EventArgs e)
    {
        int all_items;
        int checked_items = 0;

        DisableEditDeleteRecurrencePolicy();

        all_items = this.DataView.Items.Count;
        foreach (DataListItem item in this.DataView.Items)
        {
            if (((CheckBox)this.FindTemplateControl(@"ItemCheck", item)).Checked)
                checked_items++;
        }

        if (all_items == checked_items)
            ItemName.Checked = true;
        else
            ItemName.Checked = false;
    }

    public void AddRecurrencePolicy_Click(object sender, EventArgs e)
    {
        this.LevelDD.SelectedIndex = 10;
        this.TitleModalBox.Text = Resources.NCMWebContent.WEBDATA_VK_452;
        this.ImageButtonSubmit.Text = Resources.NCMWebContent.WEBDATA_VK_439;
        this.ModalBox.Show();
        ViewState["Operation"] = @"Add";
    }

    public void EditRecurrencePolicy_Click(object sender, EventArgs e)
    {
        DataRow checkedRow = null;
        int position = -1;

        foreach (DataListItem item in this.DataView.Items)
        {            
            if (((CheckBox)this.FindTemplateControl(@"ItemCheck", item)).Checked)
            {
                position = item.ItemIndex;
                break;
            }
        }
        if (position != -1 )
        {
            checkedRow = this.DataSource.Rows[position];
        }
        if (checkedRow != null)
        {
            this.TitleModalBox.Text = Resources.NCMWebContent.WEBDATA_VK_457;
            this.ImageButtonSubmit.Text = Resources.NCMWebContent.WEBDATA_VK_463;
            //fill-in weekly UI
            if (Convert.ToBoolean(checkedRow["IsWeekly"]))
            {
                this.HourBox.Text = checkedRow["Hours"].ToString();
                this.MinuteBox.Text = checkedRow["Minutes"].ToString();
                this.RecureDD.SelectedValue = @"Weekly";

                string[] days = checkedRow["DayOfWeek"].ToString().Split(',', '.');
                foreach (string day in days)
                {
                    switch (day.Trim())
                    {
                        case "0":
                            this.SundayCb.Checked = true;
                            break;
                        case "1":
                            this.MondayCb.Checked = true;
                            break;
                        case "2":
                            this.TuesdayCb.Checked = true;
                            break;
                        case "3":
                            this.WednesdayCb.Checked = true;
                            break;
                        case "4":
                            this.ThursdayCb.Checked = true;
                            break;
                        case "5":
                            this.FridayCb.Checked = true;
                            break;
                        case "6":
                            this.SaturdayCb.Checked = true;
                            break;
                        case "7":
                            this.SundayCb.Checked = true;
                            break;
                    }
                }
            }
            else
            {
                this.RecureDD.SelectedValue = @"Advanced";
            }
            AdvMinuteBox.Text = checkedRow["Minutes"].ToString();
            AdvHourBox.Text = checkedRow["Hours"].ToString();
            AdvDayOfMonth.Text = checkedRow["DayOfMonth"].ToString();
            AdvDayOfWeek.Text = checkedRow["DayofWeek"].ToString();
            AdvMonth.Text = checkedRow["Month"].ToString();
            this.LevelDD.SelectedIndex = (int)checkedRow["Level"];
            this.ImportanceTx.Text = checkedRow["Importance"].ToString();
            
            if (RecureDD.SelectedValue.Equals(@"Advanced", StringComparison.InvariantCultureIgnoreCase))
            {
                TimeHolder.Visible = false;
                DaysHolder.Visible = false;
                AdvancedHolder.Visible = true;
                AdvancedNotesHolder.Visible = true;
            }
            else
            {
                TimeHolder.Visible = true;
                DaysHolder.Visible = true;
                AdvancedHolder.Visible = false;
                AdvancedNotesHolder.Visible = false;
            }
            
            this.ModalBox.Show();
            ViewState["Operation"] = @"Edit";
            ViewState["Position"] = position;
        }
    }

    public void DeleteRecurrencePolicy_Click(object sender, EventArgs e)
    {
        ArrayList delete_positions = new ArrayList();
        
        foreach (DataListItem item in this.DataView.Items)
        {            
            if (((CheckBox)this.FindTemplateControl(@"ItemCheck", item)).Checked)
            {
                delete_positions.Add(item.ItemIndex);  
            }
        }
        
        delete_positions.Reverse(); 
        foreach (int position in delete_positions)
        {
            DataTable sourceDelete = this.DataSource;
            sourceDelete.Rows.RemoveAt(position);
            this.DataSource = sourceDelete;
        }

        DisableEditDeleteRecurrencePolicy();
    }

    public void HideModalDialog(object sender, EventArgs e)
    {
        this.ModalBox.Hide();
        this.ModalBoxRefresh();
    }

    public void SubmitModalDialog(object sender, EventArgs e)
    {
        bool cronDataValidation = true;
        string comma = @",";
        string operation = string.Empty;

        //validation check
        if (!CronMinutesValidator.IsValid)
        {
            lblAdvMinute.Visible = false;
            cronDataValidation = false;
        }
        if (!CronHoursValidator.IsValid)
        {
            lblAdvHour.Visible = false;
            cronDataValidation = false;
        }
        if (!CronDayOfMonthValidator.IsValid)
        {
            lblAdvDayOfMonth.Visible = false;
            cronDataValidation = false;
        }
        if (!CronMonthValidator.IsValid)
        {
            lblAdvMonth.Visible = false;
            cronDataValidation = false;
        }
        if (!CronDayOfWeekValidator.IsValid)
        {
            lblAdvDayOfWeek.Visible = false;
            cronDataValidation = false;
        }

        if (!cronDataValidation) return;

        DaysErrorHolder.Visible = false;

        if (ViewState["Operation"] != null)
        {
            operation = ViewState["Operation"].ToString();
        }
        switch (operation)
        {
            case "Add":
                string days = string.Empty;
                if (this.SundayCb.Checked)
                {
                    days += @"0";
                }
                if (this.MondayCb.Checked)
                {
                    days += days == string.Empty ? @"1" : $@"{comma}1";
                }
                if (this.TuesdayCb.Checked)
                {
                    days += days == string.Empty ? @"2" : $@"{comma}2";
                }
                if (this.WednesdayCb.Checked)
                {
                    days += days == string.Empty ? @"3" : $@"{comma}3";
                }
                if (this.ThursdayCb.Checked)
                {
                    days += days == string.Empty ? @"4" : $@"{comma}4";
                }
                if (this.FridayCb.Checked)
                {
                    days += days == string.Empty ? @"5" : $@"{comma}5";
                }
                if (this.SaturdayCb.Checked)
                {
                    days += days == string.Empty ? @"6" : $@"{comma}6";
                }

                DataTable sourceAdd = DataSource;

                object[] values;

                if (RecureDD.SelectedValue.Equals(@"Weekly", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (days.Length > 0)
                    {
                        values = new object[8];
                        values[0] = this.HourBox.Text;
                        values[1] = this.MinuteBox.Text;
                        values[2] = @"*";
                        values[3] = @"*";
                        values[4] = days;
                        values[5] = System.Convert.ToInt32(this.LevelDD.SelectedValue);
                        values[6] = System.Convert.ToInt32(this.ImportanceTx.Text);
                        values[7] = true;
                    }
                    else
                    {
                        DaysErrorHolder.Visible = true;
                        break;
                    }
                }
                else
                {
                    values = new object[8];
                    values[0] = this.AdvHourBox.Text;
                    values[1] = this.AdvMinuteBox.Text;
                    values[2] = this.AdvDayOfMonth.Text;
                    values[3] = this.AdvMonth.Text;
                    values[4] = this.AdvDayOfWeek.Text;
                    values[5] = System.Convert.ToInt32(this.LevelDD.SelectedValue);
                    values[6] = System.Convert.ToInt32(this.ImportanceTx.Text);
                    values[7] = false;
                }

                sourceAdd.Rows.Add(values);
                this.DataSource = sourceAdd;
                break;

            case "Edit":
                string daysEdit = string.Empty;
                if (this.SundayCb.Checked)
                {
                    daysEdit += @"0";
                }
                if (this.MondayCb.Checked)
                {
                    daysEdit += daysEdit == string.Empty ? @"1" : $@"{comma}1";
                }
                if (this.TuesdayCb.Checked)
                {
                    daysEdit += daysEdit == string.Empty ? @"2" : $@"{comma}2";
                }
                if (this.WednesdayCb.Checked)
                {
                    daysEdit += daysEdit == string.Empty ? @"3" : $@"{comma}3";
                }
                if (this.ThursdayCb.Checked)
                {
                    daysEdit += daysEdit == string.Empty ? @"4" : $@"{comma}4";
                }
                if (this.FridayCb.Checked)
                {
                    daysEdit += daysEdit == string.Empty ? @"5" : $@"{comma}5";
                }
                if (this.SaturdayCb.Checked)
                {
                    daysEdit += daysEdit == string.Empty ? @"6" : $@"{comma}6";
                }


                DataTable sourceEdit = DataSource;

                if (RecureDD.SelectedValue.Equals(@"Weekly", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (daysEdit.Length > 0)
                    {
                        sourceEdit.Rows[(int)this.ViewState["Position"]]["Hours"] = this.HourBox.Text;
                        sourceEdit.Rows[(int)this.ViewState["Position"]]["Minutes"] = this.MinuteBox.Text;
                        sourceEdit.Rows[(int)this.ViewState["Position"]]["Level"] = System.Convert.ToInt32(this.LevelDD.SelectedValue);
                        sourceEdit.Rows[(int)this.ViewState["Position"]]["Importance"] = System.Convert.ToInt32(this.ImportanceTx.Text);
                        sourceEdit.Rows[(int)this.ViewState["Position"]]["DayOfMonth"] = @"*";
                        sourceEdit.Rows[(int)this.ViewState["Position"]]["Month"] = @"*";
                        sourceEdit.Rows[(int)this.ViewState["Position"]]["DayOfWeek"] = daysEdit;
                        sourceEdit.Rows[(int)this.ViewState["Position"]]["IsWeekly"] = true;
                    }
                    else
                    {
                        DaysErrorHolder.Visible = true;
                        break;
                    }
                }
                else
                {
                    sourceEdit.Rows[(int)this.ViewState["Position"]]["Hours"] = this.AdvHourBox.Text;
                    sourceEdit.Rows[(int)this.ViewState["Position"]]["Minutes"] = this.AdvMinuteBox.Text;
                    sourceEdit.Rows[(int)this.ViewState["Position"]]["Level"] = System.Convert.ToInt32(this.LevelDD.SelectedValue);
                    sourceEdit.Rows[(int)this.ViewState["Position"]]["Importance"] = System.Convert.ToInt32(this.ImportanceTx.Text);
                    sourceEdit.Rows[(int)this.ViewState["Position"]]["DayOfMonth"] = AdvDayOfMonth.Text;
                    sourceEdit.Rows[(int)this.ViewState["Position"]]["Month"] = this.AdvMonth.Text;
                    sourceEdit.Rows[(int)this.ViewState["Position"]]["DayOfWeek"] = this.AdvDayOfWeek.Text;
                    sourceEdit.Rows[(int)this.ViewState["Position"]]["IsWeekly"] = false;
                }

                this.DataSource = sourceEdit;
                break;
        }

        if (DaysErrorHolder.Visible == false)
        {
            this.ModalBox.Hide();
            this.ModalBoxRefresh();
            DisableEditDeleteRecurrencePolicy();
        }
    }

    private void SetDaysOfWeek()
    {        
        SundayCb.Text = CultureInfo.CurrentCulture.DateTimeFormat.DayNames[0];
        MondayCb.Text = CultureInfo.CurrentCulture.DateTimeFormat.DayNames[1];
        TuesdayCb.Text = CultureInfo.CurrentCulture.DateTimeFormat.DayNames[2];
        WednesdayCb.Text = CultureInfo.CurrentCulture.DateTimeFormat.DayNames[3];
        ThursdayCb.Text = CultureInfo.CurrentCulture.DateTimeFormat.DayNames[4];
        FridayCb.Text = CultureInfo.CurrentCulture.DateTimeFormat.DayNames[5];
        SaturdayCb.Text = CultureInfo.CurrentCulture.DateTimeFormat.DayNames[6];
    }

    #endregion

    #region Properties

    public ManageInterfaceMode ManageMode
    {
        get { return _mode; }
        set { _mode = value; }
    }

    public bool MakePoliciesAdditive
    {
        get { return chPoliciesAdditive.Checked; }        
    }

    public DataTable DataSource
    {
        get
        {
            object[] values;
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("Hours", typeof(string));
            dataTable.Columns.Add("Minutes", typeof(string));
            dataTable.Columns.Add("DayOfMonth", typeof(string));
            dataTable.Columns.Add("Month", typeof(string));
            dataTable.Columns.Add("DayOfWeek", typeof(string));
            dataTable.Columns.Add("Level", typeof(int));
            dataTable.Columns.Add("Importance", typeof(int));
            dataTable.Columns.Add("IsWeekly", typeof(bool));
            
            foreach (DataListItem item in DataView.Items)
            {
                values = new object[8];
                values[0] = ((HiddenField)FindTemplateControl(@"HiddenHours", item)).Value;
                values[1] = ((HiddenField)FindTemplateControl(@"HiddenMinutes", item)).Value;
                values[2] = ((HiddenField)FindTemplateControl(@"HiddenDayOfMonth", item)).Value;
                values[3] = ((HiddenField)FindTemplateControl(@"HiddenMonth", item)).Value;
                values[4] = ((HiddenField)FindTemplateControl(@"HiddenDayOfWeek", item)).Value;
                values[5] = Convert.ToInt32 (((HiddenField)FindTemplateControl(@"HiddenLevel", item)).Value);
                values[6] = Convert.ToInt32 (((HiddenField)FindTemplateControl(@"HiddenImportance", item)).Value);
                values[7] = Convert.ToBoolean(((HiddenField)FindTemplateControl(@"HiddenIsWeekly", item)).Value);
                
                
                dataTable.Rows.Add(values);
            }

            return dataTable;
        }
        set
        {
            this.DataView.DataSource = value;
            this.DataView.DataBind();
        }
    }

    #endregion
}
