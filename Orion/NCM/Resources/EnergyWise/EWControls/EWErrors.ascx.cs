using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.EWBusinessLayer;
using SolarWinds.Orion.Common; 

public partial class EWErrors : System.Web.UI.UserControl
{
    #region Private Members

    private bool isManageEWNodes;
    private string interfacesIds;
    private string nodesIds;
    private SolarWinds.NCMModule.Web.Resources.ISWrapper ISLayer = new SolarWinds.NCMModule.Web.Resources.ISWrapper();

    #endregion

    #region Public Members

    public string EWNodesEnabledLink = string.Empty;
    
    #endregion

    #region Properties

    public bool IsManageEWNodes
    {
        get { return isManageEWNodes; }
        set { isManageEWNodes = value; }
    }

    protected string InterfacesIds
    {
        get { return interfacesIds; }
    }

    protected string NodesIds
    {
        get { return nodesIds; }
    }

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        SWISValidator validator = new SWISValidator();
        validator.ContentContainer = ContentContainer;
        ErrorContainer.Controls.Add(validator);

        if (validator.IsValid)
        {
            ISValidationHolder.Visible = false;
            ContinueRedirectHolder.Visible = false;
            BackRedirectHolder.Visible = false;

            NoNPMInstalledHolder.Visible = false;
            EWGoodEntitiesHolder.Visible = false;
            EWNodesNotCapableHolder.Visible = false;
            EWNodesNotExistHolder.Visible = false;
            EWNodesNotEnabledHolder.Visible = false;
            EWNotSupportedHolder.Visible = false;

            if (CommonHelper.IsNpmInstalled())
            {
                EWNodesValidator ewNodesValidator = new EWNodesValidator();
                ewNodesValidator.Initialize();

                if (IsPermissionExist())
                {
                    if (ewNodesValidator.NotEWCapableNodes.Count > 0 ||
                        ewNodesValidator.NotEWEnabledNodes.Count > 0 ||
                        ewNodesValidator.NotExistInNPMNodes.Count > 0 ||
                        ewNodesValidator.NotEWSupported.Count > 0)
                    {
                        ValidateEWNodesCapable(ewNodesValidator);
                        ValidateEWNodesExist(ewNodesValidator);
                        ValidateEWNodesEnabled(ewNodesValidator);
                        ValidateEWGoodEntities(ewNodesValidator);
                        ValidateEWSupportedInterfaces(ewNodesValidator);
                    }
                    else
                    {
                        string Url;
                        if (IsManageEWNodes)
                            Url =
                                $@"/Orion/NCM/Resources/EnergyWise/EWNodes/ManageEWNodes.aspx?Nodes={Request.QueryString[@"Nodes"]}&IsErrors={false}";
                        else
                            Url =
                                $@"/Orion/NCM/Resources/EnergyWise/EWInterfaces/ManageEWInterfaces.aspx?Interfaces={Request.QueryString[@"Interfaces"]}&IsErrors={false}";
                        Page.Response.Redirect(Url);
                    }
                }
            }
            else
            {
                NoNPMInstalledHolder.Visible = true;
                BackRedirectHolder.Visible = true;
            }
        }
    }

    protected void Cancel_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect("/Orion/Nodes/Default.aspx");
    }

    protected void Back_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect("/Orion/Nodes/Default.aspx");
    }

    protected void Continue_Click(object sender, EventArgs e)
    {
        string Url;
        if (IsManageEWNodes)
            Url = $@"/Orion/NCM/Resources/EnergyWise/EWNodes/ManageEWNodes.aspx?Nodes={NodesIds}&IsErrors={false}";
        else
            Url =
                $@"/Orion/NCM/Resources/EnergyWise/EWInterfaces/ManageEWInterfaces.aspx?Interfaces={InterfacesIds}&IsErrors={false}";
        Page.Response.Redirect(Url);
    }

    #endregion

    #region Private Functions

    private TableRow FillTableRow(HyperLink hyperLink)
    {
        TableRow row = new TableRow();        
        
        TableCell cell = new TableCell();
        cell.Text = @"&bull;";
        cell.Width = Unit.Pixel(5);
        row.Cells.Add(cell);

        cell = new TableCell();
        cell.Controls.Add(hyperLink);
        hyperLink.Style.Add(@"text-decoration", @"underline");
        row.Cells.Add(cell);

        return row;
    }

    private bool IsPermissionExist()
    {
        string userSid = SecurityHelper.GetCurrentUserSid(SecurityHelper._canExecuteScript);
        if (userSid.Length > 0 && (Profile.AllowNodeManagement || OrionConfiguration.IsDemoServer))
            return true;

        if (Profile.AllowNodeManagement)
        {
            PermissionExistHolder.Visible = true; //does not have NCM permissions
        }
        else
        {
            AllowNodeManagementPermission.Visible = true; //does not have NPM permissions
        }

        BackRedirectHolder.Visible = true;
        return false;
    }

    private void ValidateEWGoodEntities(EWNodesValidator ewNodesValidator)
    {
        if (ewNodesValidator.GoodEWEntities.Count > 5)
        {
            if (Page.Request.Browser.Browser.Equals(@"IE", StringComparison.InvariantCultureIgnoreCase))
                panelEWGoodEntities.Height = Unit.Pixel(100);
        }
        else
        {
            panelEWGoodEntities.Style.Add(@"border-width", Unit.Pixel(0).ToString());
        }

        if (ewNodesValidator.GoodEWEntities.Count > 0)
        {
            string comma = string.Empty;
            foreach (KeyValuePair<int, HyperLink> hyperLink in ewNodesValidator.GoodEWEntities)
            {                
                EWGoodEntities.Rows.Add(FillTableRow(hyperLink.Value));                    
                if (IsManageEWNodes)
                    nodesIds += $@"{comma}{hyperLink.Key}";
                else
                    interfacesIds += $@"{comma}{hyperLink.Key}";
                comma = @",";                
            }
            BackRedirectHolder.Visible = false;
            ContinueRedirectHolder.Visible = true;
            EWGoodEntitiesHolder.Visible = true;
            return;
        }

        BackRedirectHolder.Visible = true;
        ContinueRedirectHolder.Visible = false;
        EWGoodEntitiesHolder.Visible = false;
    }

    private void ValidateEWNodesCapable(EWNodesValidator ewNodesValidator)
    {
        if (ewNodesValidator.NotEWCapableNodes.Count > 5)
        {
            if (Page.Request.Browser.Browser.Equals(@"IE", StringComparison.InvariantCultureIgnoreCase))
                panelEWNodesNotCapable.Height = Unit.Pixel(100);
        }

        if (ewNodesValidator.NotEWCapableNodes.Count > 0)
        {
            foreach (KeyValuePair<int, HyperLink> hyperLink in ewNodesValidator.NotEWCapableNodes)
            {                
                EWNodesNotCapable.Rows.Add(FillTableRow(hyperLink.Value));
            }
            EWNodesNotCapableHolder.Visible = true;
            return;
        }

        EWNodesNotCapableHolder.Visible = false;
    }

    private void ValidateEWNodesExist(EWNodesValidator ewNodesValidator)
    {
        if (ewNodesValidator.NotExistInNPMNodes.Count > 5)
        {
            if (Page.Request.Browser.Browser.Equals(@"IE", StringComparison.InvariantCultureIgnoreCase))
                panelEWNodesNotExist.Height = Unit.Pixel(100);
        }

        if (ewNodesValidator.NotExistInNPMNodes.Count > 0)
        {
            foreach (KeyValuePair<int, HyperLink> hyperLink in ewNodesValidator.NotExistInNPMNodes)
            {
                EWNodesNotExist.Rows.Add(FillTableRow(hyperLink.Value));
            }

            EWNodesNotExistHolder.Visible = true;
            return;
        }

        EWNodesNotExistHolder.Visible = false;
    }

    private void ValidateEWNodesEnabled(EWNodesValidator ewNodesValidator)
    {
        if (ewNodesValidator.NotEWEnabledNodes.Count > 5)
        {
            if (Page.Request.Browser.Browser.Equals(@"IE", StringComparison.InvariantCultureIgnoreCase))
                panelEWNodesNotEnabled.Height = Unit.Pixel(100);
        }

        if (ewNodesValidator.NotEWEnabledNodes.Count > 0)
        {
            EWNodesEnabledLink = @"/Orion/NCM/Resources/EnergyWise/EWNodes/ManageEWNodes.aspx?Nodes=";
            string comma = string.Empty;
            foreach (KeyValuePair<int, HyperLink> hyperLink in ewNodesValidator.NotEWEnabledNodes)
            {
                EWNodesNotEnabled.Rows.Add(FillTableRow(hyperLink.Value));
                EWNodesEnabledLink += $@"{comma}{hyperLink.Key}";
                comma = @",";
            }
            EWNodesEnabledLink = $@"{EWNodesEnabledLink}&IsErrors=False";
            EWNodesNotEnabledHolder.Visible = true;
            return;
        }

        EWNodesNotEnabledHolder.Visible = false;
    }

    private void ValidateEWSupportedInterfaces(EWNodesValidator ewNodesValidator)
    {
        if (ewNodesValidator.NotEWSupported.Count > 5)
        {
            if (Page.Request.Browser.Browser.Equals(@"IE", StringComparison.InvariantCultureIgnoreCase))
                panelEWNotSupported.Height = Unit.Pixel(100);
        }

        if (ewNodesValidator.NotEWSupported.Count > 0)
        {
            foreach (KeyValuePair<int, HyperLink> hyperLink in ewNodesValidator.NotEWSupported)
            {
                EWNotSupported.Rows.Add(FillTableRow(hyperLink.Value));
            }

            EWNotSupportedHolder.Visible = true;
            return;
        }

        EWNotSupportedHolder.Visible = false;
    }

    #endregion
}
