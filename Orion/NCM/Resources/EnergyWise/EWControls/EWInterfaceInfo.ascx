<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EWInterfaceInfo.ascx.cs" Inherits="EWInterfaceInfo" %>

<link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
<link rel="stylesheet" type="text/css" href="../../styles/EW.css" />

<% if(ManageMode == SolarWinds.NCMModule.Web.Resources.ManageInterfaceMode.Multiple)
   { %>
    <table class="blueBox">
        <tr>
            <td style="padding-left:10px;padding-top:10px">
                <%=string.Format(Resources.NCMWebContent.WEBDATA_VK_436, @"<br/>") %>
            </td>
        </tr>
    </table>
<% } %>
<table class="blueBox">
    <tr>
        <td colspan="2" style="height:10px;"></td>
    </tr>
    <tr>
        <td class="leftLabelColumn" style="padding-left:13px;" nowrap="nowrap">
            <%=Resources.NCMWebContent.WEBDATA_VK_374 %>
        </td>
        <td>
            <asp:DropDownList ID="EWEnabled" runat="server" AutoPostBack="true"></asp:DropDownList>
        </td>
    </tr> 
    <tr>
        <td class="leftLabelColumn" nowrap="nowrap">            
            <asp:CheckBox ID="cbEWName" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_375 %>" runat="server" OnCheckedChanged="cbEWName_Check" AutoPostBack="true" />
        </td>
        <td>
            <asp:TextBox ID="EWName" runat="server" Width="300px" CssClass="clsLgnTxBx"></asp:TextBox>
            <% if(cbEWName.Checked)
               { %>                    
                    <asp:RegularExpressionValidator ID="regexValidatorEWName" runat="server" Display="Dynamic"
                        ControlToValidate="EWName" ValidationExpression="^\S+$"
                        ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_390 %>" >
                    </asp:RegularExpressionValidator>
            <% } %>               
        </td>
    </tr>
    <tr>
        <td class="leftLabelColumn" nowrap="nowrap">            
            <asp:CheckBox ID="cbEWKeywords" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_376 %>" runat="server" OnCheckedChanged="cbEWKeywords_Check" AutoPostBack="true" />
        </td>
        <td>
            <asp:TextBox ID="EWKeywords" runat="server" Width="300px" CssClass="clsLgnTxBx"></asp:TextBox>
            <asp:CheckBox ID="cbKeywordsAdditive" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_391 %>" runat="server"></asp:CheckBox>
            <% if (cbEWKeywords.Checked)
               { %>                    
                    <asp:RegularExpressionValidator ID="regexValidatorEWKeywords" runat="server" Display="Dynamic"
                        ControlToValidate="EWKeywords" ValidationExpression="^\S+$"
                        ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_392 %>" >
                    </asp:RegularExpressionValidator>
            <% } %> 
        </td>
    </tr>
    <tr>
        <td class="leftLabelColumn" nowrap="nowrap">            
            <asp:CheckBox ID="cbEWDefaultPowerLevel" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_377 %>" runat="server" OnCheckedChanged="cbEWDefaultPowerLevel_Check" AutoPostBack="true" />
        </td>
        <td>
            <asp:DropDownList ID="EWDefaultPowerLevel" runat="server" Width="150px"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="leftLabelColumn" nowrap="nowrap">            
            <asp:CheckBox ID="cbEWImportance" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_378 %>" runat="server" OnCheckedChanged="cbEWImportance_Check" AutoPostBack="true" />
        </td>
        <td>
            <asp:TextBox ID="EWImportance" runat="server" Width="30px" CssClass="clsLgnTxBx"></asp:TextBox>
            <% if(cbEWImportance.Checked)
               { %>
                    <asp:RequiredFieldValidator ID="validatorEWImportance1" runat="server" Display="Dynamic" 
                        ControlToValidate="EWImportance" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_393 %>" >
                    </asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="validatorRangeEWImportance1" ControlToValidate="EWImportance" Display="Dynamic"
                        runat="server" MinimumValue="0" MaximumValue="100" Type="Integer" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_394 %>">
                    </asp:RangeValidator>
            <% } %>               
        </td>
    </tr>
    <tr>
        <td class="leftLabelColumn" nowrap="nowrap">            
            <asp:CheckBox ID="cbEWRole" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_379 %>" runat="server" OnCheckedChanged="cbEWRole_Check" AutoPostBack="true" />    
        </td>
        <td>
            <asp:TextBox ID="EWRole" runat="server" Width="200px" CssClass="clsLgnTxBx"></asp:TextBox>
            <% if (cbEWRole.Checked)
               { %>                    
                    <asp:RegularExpressionValidator ID="regexValidatorEWRole" runat="server" Display="Dynamic"
                        ControlToValidate="EWRole" ValidationExpression="^\S+$"
                        ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_395 %>" >
                    </asp:RegularExpressionValidator>
            <% } %>
        </td>
    </tr>
</table>