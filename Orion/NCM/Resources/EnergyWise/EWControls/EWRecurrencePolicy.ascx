<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EWRecurrencePolicy.ascx.cs" Inherits="EWRecurrencePolicy" %>

<%@ Register Assembly="SolarWinds.NCMModule.Web.Resources" Namespace="SolarWinds.NCMModule.Web.Resources.EWBusinessLayer" TagPrefix="cc1" %>
<%@ Register Src="~/Orion/NCM/ModalBox.ascx" TagName="ModalBox" TagPrefix="ctrl" %>

<link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />

<div style="padding-bottom:10px;font-size:medium;font-weight:bold;">
    <%=Resources.NCMWebContent.WEBDATA_VK_437 %>
</div>

<div style="border-width:1px; border-style:solid; border-color:#DCDBD7;">
    <asp:UpdatePanel ID="updatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table style="border-width:medium" width="100%">
                <tr style="background-color:#FBFBFB;">
                    <td nowrap="nowrap" style="font-size:11px;">
                       <asp:LinkButton ID="AddRecurrencePolicy" runat="server" OnClick="AddRecurrencePolicy_Click">
                           <img alt="" style="vertical-align:middle;" src="/Orion/NCM/Resources/images/ConfigSnippets/icon_addsnippet.gif" />&nbsp;<%=Resources.NCMWebContent.WEBDATA_VK_452 %>
                       </asp:LinkButton>                       
                       &nbsp;
                       <asp:LinkButton ID="EditRecurrencePolicy" runat="server" OnClick="EditRecurrencePolicy_Click">
                           <img alt="" style="vertical-align:middle;" src="/Orion/NCM/Resources/images/ConfigSnippets/icon_editsnippet.gif" />&nbsp;<%=Resources.NCMWebContent.WEBDATA_VK_453 %>
                       </asp:LinkButton>
                       &nbsp;
                       <asp:LinkButton ID="DeleteRecurrencePolicy" runat="server" OnClick="DeleteRecurrencePolicy_Click">
                           <img alt="" style="vertical-align:middle;" src="/Orion/NCM/Resources/images/icon_delete.png" />&nbsp;<%=Resources.NCMWebContent.WEBDATA_VK_454 %>
                       </asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="background-color:#FBFBFB;" width="100%" cellpadding="0" cellspacing="0">
                            <% if (ManageMode == SolarWinds.NCMModule.Web.Resources.ManageInterfaceMode.Multiple)
                               { %>
                                <tr>
                                    <td height="10px" style="padding-left:2px;width:25px;border-right:solid 1px #E1E1E0;">
                                        <asp:CheckBox id="chPoliciesAdditive" runat="server" />
                                    </td>
                                    <td style="border-left:solid 1px White;padding-left:3px;">
                                        <%=Resources.NCMWebContent.WEBDATA_VK_391 %>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="height:1px;background-color:White;">
                                    </td>
                                </tr>
                            <% } %>
                            <tr>
                                <td height="10px" style="padding-left:2px;width:25px;border-right:solid 1px #E1E1E0;">
                                    <asp:CheckBox id="ItemName" runat="server" AutoPostBack="true" OnCheckedChanged="ItemName_Check" />
                                </td>
                                <td style="border-left:solid 1px White;padding-left:3px;">
                                    <%=Resources.NCMWebContent.WEBDATA_VK_438 %>
                                </td>
                            </tr>
                        </table>
                        <asp:DataList ID="DataView" Width="100%" RepeatColumns="1" RepeatLayout="Table" RepeatDirection="Horizontal" BackColor="0xE4F1F8" AlternatingItemStyle-BackColor="White" runat="server" EnableViewState="true">                            
                            <ItemTemplate>
                                <table cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td style="width:26px;border-right:solid 1px #E1E1E0;">
                                            &nbsp;
                                        </td>
                                        <td height="10px" style="border-left:solid 1px White;">
                                            <asp:CheckBox id="ItemCheck" runat="server" AutoPostBack="true" OnCheckedChanged="Item_Check" />
                                        </td>
                                        <td>
                                           <asp:Label id="ItemLabel" Font-Size="Smaller" runat="server" Text='<%# GeneratePolicyRecurenceString (DataBinder.Eval(Container.DataItem, @"Minutes").ToString(),DataBinder.Eval(Container.DataItem, @"Hours").ToString(),DataBinder.Eval(Container.DataItem, @"DayOfMonth").ToString(),DataBinder.Eval(Container.DataItem, @"Month").ToString(),DataBinder.Eval(Container.DataItem, @"DayOfWeek").ToString(),DataBinder.Eval(Container.DataItem, @"Level").ToString(),DataBinder.Eval(Container.DataItem, @"Importance").ToString()) %>' />
                                        </td>
                                    </tr>
                                </table>
                                <asp:HiddenField id="HiddenHours" Value='<%# Eval(@"Hours") %>' runat="server" />
                                <asp:HiddenField id="HiddenMinutes" Value='<%# Eval(@"Minutes") %>' runat="server" />
                                <asp:HiddenField id="HiddenDayOfMonth" Value='<%# Eval(@"DayOfMonth") %>' runat="server" />
                                <asp:HiddenField id="HiddenMonth" Value='<%# Eval(@"Month") %>' runat="server" />
                                <asp:HiddenField id="HiddenDayOfWeek" Value='<%# Eval(@"DayOfWeek") %>' runat="server" />
                                <asp:HiddenField id="HiddenLevel" Value='<%# Eval(@"Level") %>' runat="server" />
                                <asp:HiddenField id="HiddenImportance" Value='<%# Eval(@"Importance") %>' runat="server" />
                                <asp:HiddenField id="HiddenIsWeekly" Value='<%# Eval(@"IsWeekly") %>' runat="server" />
                            </ItemTemplate>
                        </asp:DataList>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
 
    <ctrl:ModalBox ID="ModalBox" runat="server">
        <DialogContents>
            <div style="border:1px solid;">
                <div style="background-image: url('/Orion/NCM/Resources/images/PopupHeader.gif'); background-repeat: repeat-x; background-position: left bottom; margin: 0 0 0 0; padding: 5px 5px 5px 5px;">
                    <table width="100%">
                        <tr>
                            <td style="font-weight: normal; font-size: large;">
                                <asp:Label ID="TitleModalBox" runat="server" Font-Names="Arial" />
                            </td>
                            <td align="right">
                                <asp:ImageButton runat="server" id="CancelButton" OnClick="HideModalDialog" CausesValidation="False" ImageUrl="~/Orion/NCM/Resources/images/Button.CloseCross.gif" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="padding-left:10px;padding-right:10px;">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td style="padding-top:10px;padding-bottom:5px;" nowrap="nowrap">
                                <%=Resources.NCMWebContent.WEBDATA_VK_451 %>
                            </td>
                            <td style="padding-top:10px;padding-bottom:5px">
                                <asp:DropDownList id="RecureDD" runat="server" AutoPostBack="true">
                                    <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_455 %>" Value="Weekly" /> 
                                    <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_456 %>" Value="Advanced" /> 
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr id="DaysHolder" class="blueBox" runat="server">
                            <td>
                            </td>
                            <td>
                                <table runat="server" cellspacing="0" cellpadding="0" width="100%">                   
                                    <tr>
                                        <td style="padding-top:10px;">
                                            <asp:CheckBox id="SundayCb" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top:10px;">
                                            <asp:CheckBox id="MondayCb" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top:10px;">
                                            <asp:CheckBox id="TuesdayCb" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top:10px;">
                                            <asp:CheckBox id="WednesdayCb" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top:10px;">
                                            <asp:CheckBox id="ThursdayCb" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top:10px;">
                                            <asp:CheckBox id="FridayCb" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top:10px;padding-bottom:10px;">
                                            <asp:CheckBox id="SaturdayCb" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="DaysErrorHolder" runat="server" visible="false">
                                        <td style="padding-bottom:10px;padding-left:3px;">
                                            <asp:Label runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_450 %>" Font-Size="11px" ForeColor="Red" Font-Bold="true" />
                                        </td>
                                    </tr>                    
                                </table>                                
                            </td>
                        </tr>                    
                        <tr id="AdvancedHolder" runat="server">
                            <td colspan="2" class="blueBox">
                                <div style="padding:5px 5px 5px 5px;">
                                    <table width="100%" runat="server" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td nowrap="nowrap">
                                                <%=Resources.NCMWebContent.WEBDATA_VK_449 %>
                                            </td>
                                            <td>
                                                <asp:TextBox id="AdvMinuteBox" runat="server" Width="50px" TextMode="SingleLine" />
                                                <asp:Label ID="lblAdvMinute" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_458 %>" Font-Size="11px" ForeColor="Gray" />
                                                <cc1:CronDataValidator id="CronMinutesValidator" ControlToValidate="AdvMinuteBox" Display="Static" EnableClientScript="false"
                                                runat="server" MinimumValue="00" MaximumValue="59" Font-Size="11px" Font-Bold="true" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_458 %>" />
                                            </td>
                                        </tr>                                    
                                        <tr>
                                            <td style="padding-top:10px;" nowrap="nowrap">
                                                <%=Resources.NCMWebContent.WEBDATA_VK_448 %>
                                            </td>
                                            <td style="padding-top:10px;">
                                                <asp:TextBox id="AdvHourBox" runat="server" Width="50px" TextMode="SingleLine" />
                                                <asp:Label ID="lblAdvHour" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_459 %>" Font-Size="11px" ForeColor="Gray" />
                                                <cc1:CronDataValidator id="CronHoursValidator" ControlToValidate="AdvHourBox" Display="Dynamic" EnableClientScript="false"
                                                runat="server" MinimumValue="00" MaximumValue="23" Font-Size="11px" Font-Bold="true" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_459 %>"/> 
                                            </td>
                                        </tr>                                    
                                        <tr>
                                            <td style="padding-top:10px;" nowrap="nowrap">
                                                <%=Resources.NCMWebContent.WEBDATA_VK_447 %>
                                            </td>
                                            <td style="padding-top:10px;">
                                                <asp:TextBox id="AdvDayOfMonth" runat="server" Width="50px" TextMode="SingleLine" />
                                                <asp:Label ID="lblAdvDayOfMonth" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_460 %>" Font-Size="11px" ForeColor="Gray" /> 
                                                <cc1:CronDataValidator id="CronDayOfMonthValidator" ControlToValidate="AdvDayOfMonth" Display="Dynamic" EnableClientScript="false"
                                                runat="server" MinimumValue="01" MaximumValue="31" Font-Size="11px" Font-Bold="true" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_460 %>"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top:10px;" nowrap="nowrap">
                                                <%=Resources.NCMWebContent.WEBDATA_VK_446 %>
                                            </td>
                                            <td style="padding-top:10px;">
                                                <asp:TextBox id="AdvMonth" runat="server" Width="50px" TextMode="SingleLine" />
                                                <asp:Label ID="lblAdvMonth" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_461 %>" Font-Size="11px" ForeColor="Gray" /> 
                                                <cc1:CronDataValidator id="CronMonthValidator" ControlToValidate="AdvMonth" Display="Dynamic" EnableClientScript="false"
                                                runat="server" MinimumValue="1" MaximumValue="12" Font-Size="11px" Font-Bold="true" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_461 %>"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top:10px;" nowrap="nowrap">
                                                <%=Resources.NCMWebContent.WEBDATA_VK_445 %>
                                            </td>
                                            <td style="padding-top:10px;">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox id="AdvDayOfWeek" runat="server" Width="50px" TextMode="SingleLine" />
                                                        </td>                                                        
                                                        <td style="padding-left:5px;">
                                                            <asp:Label ID="lblAdvDayOfWeek" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_462 %>" Font-Size="11px" ForeColor="Gray" /> 
                                                            <cc1:CronDataValidator id="CronDayOfWeekValidator" ControlToValidate="AdvDayOfWeek" Display="Dynamic" EnableClientScript="false"
                                                            runat="server" MinimumValue="0" MaximumValue="7" Font-Size="11px" Font-Bold="true" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_462 %>"/>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="AdvancedNotesHolder" runat="server" style="padding-top:5px;padding-right:5px;color:Gray;font-size:8pt;">
                                        <%=Resources.NCMWebContent.WEBDATA_VK_464 %>
                                     </div>
                                </div>
                            </td>
                        </tr>
                        <tr id="TimeHolder" runat="server">
                            <td style="padding-top:5px;" nowrap="nowrap">
                                <%=Resources.NCMWebContent.WEBDATA_VK_444 %>
                            </td>
                            <td align="left" style="padding-top:5px;">
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td nowrap="nowrap">
                                            <asp:TextBox id="HourBox" runat="server" Width="15px" Text="00" MaxLength="2" TextMode="SingleLine" />                        
                                            <asp:Label ID="Label1" runat="server" Text=":" />
                                            <asp:TextBox id="MinuteBox" runat="server" Width="15px" Text="00" MaxLength="2" TextMode="SingleLine" />
                                        </td>
                                        <td style="padding-left:5px;">
                                            <asp:Label ID="Label2" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_465 %>" Font-Size="11px" ForeColor="Gray" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="TimeValidatorsHolder" runat="server">
                            <td>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="validatorHour" runat="server" Display="Dynamic"
                                    ControlToValidate="HourBox" Font-Size="11px" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_466 %>" Font-Bold="true">                                
                                </asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="validatorRangeHours" ControlToValidate="HourBox" Display="Dynamic"
                                    runat="server" MinimumValue="00" Font-Size="11px" MaximumValue="23" Type="Integer" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_467 %>" Font-Bold="true">
                                </asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="validatorMinute" runat="server" Display="Dynamic" 
                                    ControlToValidate="MinuteBox" Font-Size="11px" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_468 %>" Font-Bold="true">
                                </asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="validatorRangeMinutes" ControlToValidate="MinuteBox" Display="Dynamic"
                                    runat="server" MinimumValue="00" Font-Size="11px" MaximumValue="59" Type="Integer" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_469 %>" Font-Bold="true">
                                </asp:RangeValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top:10px;width:25%;">
                                <%=Resources.NCMWebContent.WEBDATA_VK_443 %>
                            </td>
                            <td style="padding-top:10px;">
                                <asp:DropDownList id="LevelDD" runat="server">
                                    <asp:ListItem Value="0" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_413 %>" />
                                    <asp:ListItem Value="1" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_414 %>" />
                                    <asp:ListItem Value="2" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_415 %>" />
                                    <asp:ListItem Value="3" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_416 %>" />
                                    <asp:ListItem Value="4" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_417 %>" />
                                    <asp:ListItem Value="5" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_418 %>" />
                                    <asp:ListItem Value="6" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_419 %>" />
                                    <asp:ListItem Value="7" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_420 %>" />
                                    <asp:ListItem Value="8" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_421 %>" />
                                    <asp:ListItem Value="9" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_422 %>" />
                                    <asp:ListItem Value="10" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_423 %>" />
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top:10px">
                                <%=Resources.NCMWebContent.WEBDATA_VK_442 %>
                            </td>
                            <td style="padding-top:10px" align="left">
                                <asp:TextBox id="ImportanceTx" Width="40px" Text="100" runat="server" />
                                <asp:Label runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_441 %>" Font-Size="11px" ForeColor="Gray"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="validatorImportance" runat="server" 
                                    ControlToValidate="ImportanceTx" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_440 %>" Display="Dynamic" Font-Size="11px" Font-Bold="true">                            
                                </asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="validatorRangeImportance" ControlToValidate="ImportanceTx" Display="Dynamic"
                                    runat="server" MinimumValue="0" MaximumValue="100" Type="Integer" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_394 %>" Font-Size="11px" Font-Bold="true">
                                </asp:RangeValidator>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="padding-right:10px;padding-left:10px;padding-top:10px;padding-bottom:10px;">
                    <table width="100%">
                        <tr>
                            <td align="right" style="">
                                <orion:LocalizableButton ID="ImageButtonSubmit" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_439 %>" OnClick="SubmitModalDialog"></orion:LocalizableButton>
                                <orion:LocalizableButton ID="ImageButtonCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="HideModalDialog" CausesValidation="False"></orion:LocalizableButton>
                            </td>
                            
                        </tr>
                    </table>
                </div>
            </div>
        </DialogContents>
    </ctrl:ModalBox>
</div>

