<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EWErrors.ascx.cs" Inherits="EWErrors" %>

<link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />

<table class="beigeBox" id="ISValidationHolder" cellpadding="0" cellspacing="0" runat="server">
    <tr> 
        <td style="width:5%;padding-left:10px;">
            <img src="/Orion/NCM/Resources/images/warning_32x29.gif" />
        </td>
        <td style="width:95%;padding-top:10px;padding-bottom:10px;">
            <div id="ErrorContainer" runat="server"/>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="background-color:White; width:100%;height:3px;">
        </td>
    </tr>
</table>

<asp:Panel ID="ContentContainer" runat="server">
<table class="beigeBox" id="NoNPMInstalledHolder" cellpadding="0" cellspacing="0" runat="server">
    <tr>
        <td style="width:5%;padding:10px 10px 10px 10px;">
            <img src="/Orion/NCM/Resources/images/warning_32x29.gif" />
        </td>
        <td style="width:95%;padding:10px 10px 10px 0px;">
            <%=string.Format(Resources.NCMWebContent.WEBDATA_VK_369, @"<i>", @"</i>") %>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="background-color:White;width:100%;height:3px;">
        </td>
    </tr>
</table>

<table class="beigeBox" id="EWNodesNotCapableHolder" cellpadding="0" cellspacing="0" runat="server">
    <tr>
        <td style="width:5%;padding-left:10px;">
            <img src="/Orion/NCM/Resources/images/warning_32x29.gif" />
        </td>
        <td style="width:95%;padding-top:10px;">
            <table>
                <tr>
                    <td style="font-weight:bold;">
                        <%=Resources.NCMWebContent.WEBDATA_VK_368 %>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="panelEWNodesNotCapable" runat="server" style="padding-left:10px;overflow:auto;position:relative;max-height:100px;" >
                            <asp:Table ID="EWNodesNotCapable" runat="server"></asp:Table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="background-color:White;width:100%;height:3px;">
        </td>
    </tr>
</table>

<table class="beigeBox" id="EWNodesNotExistHolder" cellpadding="0" cellspacing="0" runat="server">
    <tr>
        <td style="width:5%;padding-left:10px;">
            <img src="/Orion/NCM/Resources/images/warning_32x29.gif" />
        </td>
        <td style="width:95%;padding-top:10px;">
            <table>
                <tr>
                    <td style="font-weight:bold;">
                        <%=Resources.NCMWebContent.WEBDATA_VK_367 %>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="panelEWNodesNotExist" runat="server" style="padding-left:10px;overflow:auto;position:relative;max-height:100px;" >
                            <asp:Table ID="EWNodesNotExist" runat="server"></asp:Table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td style="padding-bottom:10px;">
                        <%=string.Format(Resources.NCMWebContent.WEBDATA_VK_366, @"<br/>", @"<i>", @"</i>") %>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="background-color:White; width:100%;height:3px;">
        </td>
    </tr>
</table>

<table class="beigeBox" id="EWNodesNotEnabledHolder" cellpadding="0" cellspacing="0" runat="server">
    <tr>
        <td style="width:5%;padding-left:10px;">
            <img src="/Orion/NCM/Resources/images/warning_32x29.gif" />
        </td>
        <td style="width:95%;padding-top:10px;">
            <table>
                <tr>
                    <td style="font-weight:bold;">
                        <%=Resources.NCMWebContent.WEBDATA_VK_365 %>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="panelEWNodesNotEnabled" runat="server" style="padding-left:10px;overflow:auto;position:relative;max-height:100px;" >
                            <asp:Table ID="EWNodesNotEnabled" runat="server"></asp:Table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td style="padding-bottom:10px;">
                        <%=string.Format(Resources.NCMWebContent.WEBDATA_VK_371, $@"<a href='{EWNodesEnabledLink}' style='text-decoration:underline;'>", @"</a>")%>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="background-color:White; width:100%;height:3px;">
        </td>
    </tr>
</table>

<table class="beigeBox" id="EWNotSupportedHolder" cellpadding="0" cellspacing="0" runat="server">
    <tr>
        <td style="width:5%;padding-left:10px;">
            <img src="/Orion/NCM/Resources/images/warning_32x29.gif" />
        </td>
        <td style="width:95%;padding-top:10px;">
            <table>
                <tr>
                    <td style="font-weight:bold;">
                        <%=Resources.NCMWebContent.WEBDATA_VK_364 %>
                    </td>
                </tr>
                <tr>
                    <td style="padding-bottom:5px;">
                        <asp:Panel ID="panelEWNotSupported" runat="server" style="padding-left:10px;overflow:auto;position:relative;max-height:100px;" >
                            <asp:Table ID="EWNotSupported" runat="server"></asp:Table>
                        </asp:Panel>
                    </td>
                </tr>                
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="background-color:White; width:100%;height:3px;">
        </td>
    </tr>
</table>

<table class="beigeBox" id="AllowNodeManagementPermission" visible="false" cellpadding="0" cellspacing="0" runat="server">
    <tr>
        <td style="width:5%;padding-left:10px;">
            <img src="/Orion/NCM/Resources/images/warning_32x29.gif" />
        </td>
        <td style="width:95%;">
            <table>
                <tr>
                    <td style="padding-bottom:10px;padding-top:10px;font-weight:bold;color:Red;">
                        <%=Resources.NCMWebContent.WEBDATA_VK_363 %>
                    </td>
                </tr>                
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="background-color:White; width:100%;height:3px;">
        </td>
    </tr>
</table>

<table id="EWGoodEntitiesHolder" cellpadding="0" cellspacing="0" runat="server">
    <tr>
        <td style="padding-bottom:10px;padding-top:10px;">
            <% if (IsManageEWNodes)
               { %>
                <%=Resources.NCMWebContent.WEBDATA_VK_362 %>
            <% }
               else
               { %>
                <%=Resources.NCMWebContent.WEBDATA_VK_361 %>
            <% } %>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Panel ID="panelEWGoodEntities" runat="server" style="padding-left:10px;overflow:auto;position:relative;max-height:100px;border-width:1px; border-style:solid; border-color:#DCDBD7;" >
                <asp:Table ID="EWGoodEntities" runat="server"></asp:Table>
            </asp:Panel>
        </td>
    </tr>    
</table>

<table class="beigeBox" id="PermissionExistHolder" visible="false" cellpadding="0" cellspacing="0" runat="server">
    <tr>
        <td style="width:5%;padding-left:10px;">
            <img src="/Orion/NCM/Resources/images/warning_32x29.gif" />
        </td>
        <td style="width:95%;">
            <table>
                <tr>
                    <td style="padding-bottom:10px;padding-top:10px;font-weight:bold;color:Red;">
                        <%=string.Format(Resources.NCMWebContent.WEBDATA_VK_370, @"<br/>") %>
                    </td>
                </tr>                
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="background-color:White; width:100%;height:3px;">
        </td>
    </tr>
</table>

<table id="BackRedirectHolder" runat="server">
    <tr>
        <td style="padding-bottom:20px;padding-top:10px;">
            <orion:LocalizableButton runat="server" ID="btnBack" DisplayType="Secondary" LocalizedText="Back" OnClick="Back_Click"></orion:LocalizableButton>
        </td>        
    </tr>
</table>

<table id="ContinueRedirectHolder" runat="server">
    <tr>
        <td style="font-weight:bold;padding-top:10px;">            
            <%=Resources.NCMWebContent.WEBDATA_VK_360 %>
        </td>
    </tr>
    <tr>
        <td style="padding-bottom:20px;padding-top:10px;">
            <orion:LocalizableButton runat="server" ID="btnContinue" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_359 %>" OnClick="Continue_Click"></orion:LocalizableButton>
            <orion:LocalizableButton runat="server" ID="btnCancel" DisplayType="Secondary" LocalizedText="Cancel" OnClick="Cancel_Click"></orion:LocalizableButton>
        </td>
    </tr>
</table>
</asp:Panel>