using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Web;
using System.Web.UI;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.EWBusinessLayer;
using SolarWinds.NCM.Contracts.InformationService;

using StringRegistrar = SolarWinds.Orion.Core.Common.i18n.Registrar.ResourceManagerRegistrar;

public partial class EWTransferStatus : System.Web.UI.Page
{
    #region Private Members

    private EWTransferControler ewTransferControler = new EWTransferControler();
    
    #endregion

    #region Page Functions

    protected void Page_Load(object sender, EventArgs e)
    {
        string transferOn = Page.Request.QueryString[@"TransferOn"];
        if (!string.IsNullOrEmpty(transferOn))
        {
            if (transferOn.Equals(@"Nodes", StringComparison.InvariantCultureIgnoreCase))
            {
                Page.Title = Resources.NCMWebContent.WEBDATA_VK_405;
            }
            else
            {
                Page.Title = Resources.NCMWebContent.WEBDATA_VK_428;
            }
        }

        if (Session[@"TransferIdList"] != null)
        {
            Guid[] transferIdList = (Guid[])Session[@"TransferIdList"];
            timerInterfaceStatus.Enabled = true;
            InitGridInterfaceStatus();
            gridInterfaceStatus.DataSource = ewTransferControler.GetTransferStatus(transferIdList);
            gridInterfaceStatus.DataBind();
        }
        else
        {
            Page.Response.Redirect("/Orion/Nodes/Default.aspx");
        }
    }

    #endregion

    #region Events

    protected void timerInterfaceStatus_Tick(object sender, EventArgs e)
    {
    }

    public void btnOK_Click(object sender, EventArgs e)
    {
        Session[@"TransferIdList"] = null;
        Page.Response.Redirect("/Orion/Nodes/Default.aspx");
    }

    public void gridInterfaceStatus_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridViewRow row = e.Row;

        row.Cells[0].Width = Unit.Percentage(20);
        row.Cells[2].Visible = false;
        row.Cells[3].Visible = false;
        row.Cells[4].Visible = false;
        
        if (row.RowType == DataControlRowType.DataRow)
        {
            string cell0 = HttpUtility.HtmlEncode(row.Cells[0].Text);
            row.Cells[0].Text = $@"&bull;&nbsp;{cell0}";

            eTransferState state = (eTransferState)Enum.Parse(typeof(eTransferState), row.Cells[1].Text);
            row.Cells[1].Controls.Add(new Literal { Text =
                $@"<img src='{GetStatusImage(state)}'><span style='padding-left:5px;'>{i18nNCMHelper.LookupTransferStatus(state, true, StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"LIBCODE_IC_55"))}</span>"
            });

            if (state == eTransferState.Complete)
            {
                row.Cells[2].Visible = true;
                LocalizableButton btnScriptResults = new LocalizableButton();
                btnScriptResults.ID = $@"btnScriptResults_{row.DataItemIndex}";
                btnScriptResults.DisplayType = SolarWinds.Orion.Web.UI.ButtonType.Small;
                btnScriptResults.LocalizedText = CommonButtonType.CustomText;
                btnScriptResults.Text = StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"LIBCODE_IC_64");
                btnScriptResults.CommandName = @"ScriptLog";
                btnScriptResults.CommandArgument = row.DataItemIndex.ToString();
                row.Cells[2].Controls.Add(btnScriptResults);

                if (Session[@"NodeIdList"] != null)
                {
                    Dictionary<string, EWNodeTransferStatus> nodeIdList = (Dictionary<string, EWNodeTransferStatus>)Session[@"NodeIdList"];
                    EWNodeTransferStatus ewNodeTransferStatus;
                    string ncmNodeId = row.Cells[4].Text.TrimStart('{').TrimEnd('}').ToUpperInvariant();
                    if (nodeIdList.TryGetValue(ncmNodeId, out ewNodeTransferStatus))
                    {
                        if (!ewNodeTransferStatus.IsPollNode)
                        {
                            ewTransferControler.PollNode(ewNodeTransferStatus.NPMNodeID);
                            nodeIdList[ncmNodeId].IsPollNode = true;
                            Session[@"NodeIdList"] = nodeIdList;
                        }
                    }
                }
            }
            else if (state == eTransferState.Error)
            {
                row.Cells[2].Visible = true;
                row.Cells[2].Controls.Add(new Label() { Text = row.Cells[3].Text, ForeColor = Color.Red });
            }
        }
    }

    public void gridInterfaceStatus_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == @"ScriptLog")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = gridInterfaceStatus.Rows[index];
            string log = row.Cells[2].Text;

            int width = 700;
            scriptResultsText.Text = log;
            scriptResultsText.Wrap = false;
            scriptResultsText.Width = Unit.Pixel(width - 20);
            scriptResultsHeader.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_426, HttpUtility.HtmlEncode(row.Cells[0].Text.Replace(@"&bull;&nbsp;", string.Empty).Trim()));
            scriptResultModalBox.Width = width;
            scriptResultModalBox.Show();
        }
    }

    protected void modalBoxClose_Click(object sender, EventArgs e)
    {
        scriptResultModalBox.Hide();
        scriptResultsText.Text = string.Empty;
    }

    #endregion

    #region Private Functions

    private void InitGridInterfaceStatus()
    {
        gridInterfaceStatus.Width = Unit.Percentage(100);
        gridInterfaceStatus.GridLines = GridLines.None; 
        gridInterfaceStatus.CellPadding = 3;
        gridInterfaceStatus.AutoGenerateColumns = true;
        gridInterfaceStatus.ShowHeader = false;
    }

    private string GetStatusImage(eTransferState state)
    {
        switch (state)
        {
            case eTransferState.Complete:
                return @"/Orion/NCM/Resources/images/Status.Complete.gif";
            case eTransferState.Error:
                return @"/Orion/NCM/Resources/images/Status.Error.gif";
            default:
                return @"/Orion/NCM/Resources/images/Animated.Loading.gif";
        }
    }
    
    #endregion
}
