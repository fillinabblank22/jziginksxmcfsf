<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="EWChartDrilldown.aspx.cs" Inherits="EWChartDrilldown" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="ncm" TagName="IconExportToPdf" Src="~/Orion/NCM/Controls/IconExportToPdf.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ncmPageTitlePlaceHolder">
	<%=Page.Title%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmExportToPDFPlaceHolder" runat="server">
    <ncm:IconExportToPdf ID="IconExportToPdf1" runat="server"></ncm:IconExportToPdf>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmMainContentPlaceHolder" runat="server">
    <script type="text/javascript" src="../../JavaScript/main.js"></script>
	<orion:ResourceHostControl ID="resourceHostControl" runat="server">
		<orion:ResourceContainer runat="server" ID="resContainer" />		
	</orion:ResourceHostControl>
</asp:Content>

