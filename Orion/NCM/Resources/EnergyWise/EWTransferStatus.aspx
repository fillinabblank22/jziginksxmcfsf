<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" CodeFile="EWTransferStatus.aspx.cs" Inherits="EWTransferStatus" %>

<%@ Register Src="~/Orion/NCM/ModalBox.ascx" TagName="ModalBox" TagPrefix="ModalBox" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ncmPageTitlePlaceHolder">
	<%=Page.Title %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmHelpIconPlaceHolder" runat="server">
    <orion:IconHelpButton ID="helpBtn" runat="server" HelpUrlFragment="OrionNCMWebEWInterfaceTransferStatus" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" runat="server">
    <div style="padding:10px;font-size:11px;font-family:Arial;">
        <a title="<%=Resources.NCMWebContent.WEBDATA_VK_357 %>" style="text-decoration:underline;" href="/Orion/Admin"><%=Resources.NCMWebContent.WEBDATA_VK_357 %></a>&nbsp;<img alt="" style="vertical-align:middle;" src="/Orion/images/breadcrumb_arrow.gif"/>&nbsp;
        <a title="<%=Resources.NCMWebContent.WEBDATA_VK_358 %>" style="text-decoration:underline;" href="/Orion/Nodes/Default.aspx"><%=Resources.NCMWebContent.WEBDATA_VK_358 %></a>&nbsp;<img alt="" style="vertical-align:middle;" src="/Orion/images/breadcrumb_arrow.gif"/>
    </div>
</asp:Content>

<asp:Content ID="ManageEWInterfaceContent" ContentPlaceHolderID="ncmMainContentPlaceHolder" runat ="server" > 
	<link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
	<script type="text/javascript" src="../../JavaScript/main.js"></script>
	
	<div style="padding:10px;">
	    <asp:UpdatePanel ID="updatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                    <table cellpadding="0" cellspacing="0" width="100%" >
                        <tr>
                            <table width="100%" style="padding-bottom:10px;border-width:1px; border-style:solid; border-color:#DCDBD7; background-color:#FFFFFF">
                                <tr>
                                    <td style="font-family:Arial;font-size:10pt;font-weight:bold;padding-left:10px;padding-right:10px;padding-top:10px;">
                                        <%=Resources.NCMWebContent.WEBDATA_VK_424 %>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left:10px;padding-right:10px;padding-top:10px;">
                                        <asp:Timer ID="timerInterfaceStatus" runat="server" Interval="5000" OnTick="timerInterfaceStatus_Tick" Enabled="false"></asp:Timer>
                                        <asp:GridView ID="gridInterfaceStatus" runat="server" OnRowDataBound="gridInterfaceStatus_RowDataBound" OnRowCommand="gridInterfaceStatus_RowCommand"></asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left:10px;padding-right:10px;padding-top:20px;padding-bottom:20px;">
                                        <orion:LocalizableButton ID="btnOK" runat="server" DisplayType="Primary" LocalizedText="Ok" OnClick="btnOK_Click"></orion:LocalizableButton>
                                    </td>
                                </tr>
                            </table>
                        </tr>
                    </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    
    <ModalBox:ModalBox ID="scriptResultModalBox" runat="server">
        <DialogContents>
            <table width="100%" style="border:solid 1px black;">
                <tr>
                    <td style="border: 0px; background-image: url('/Orion/NCM/Resources/images/PopupHeader2Rows.gif'); background-repeat:repeat-x;" width="100%">
                        <table id="Table2" style="margin-bottom: 1px;" width="100%">
                            <tr>
                                <td>
                                    <asp:Label ID="scriptResultsHeader" runat="server" Font-Bold="true" Font-Size="medium"></asp:Label>
                                </td>
                                <td align="right">
                                    <asp:ImageButton ID="modalBoxCloseCross" runat="server" ImageUrl="/Orion/NCM/Resources/images/Button.CloseCross.gif" OnClick="modalBoxClose_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="vertical-align:top;width:100%;padding:5px;">
                        <asp:TextBox ID="scriptResultsText" runat="server" TextMode="MultiLine" Rows="20" ReadOnly="true" Width="100%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right" style="padding:5px;">
                        <orion:LocalizableButton ID="modalBoxClose" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_425 %>" OnClick="modalBoxClose_Click"></orion:LocalizableButton>
                    </td>
                </tr>
            </table>
        </DialogContents>
    </ModalBox:ModalBox>
</asp:Content> 
