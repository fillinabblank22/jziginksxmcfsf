﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="ManageEWNodes.aspx.cs" Inherits="Orion_NCM_ManageEWNodes" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Orion/NCM/ModalBox.ascx" TagName="ModalBox" TagPrefix="ModalBox" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ncmPageTitlePlaceHolder">
	<%=Page.Title %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmHelpIconPlaceHolder" runat="server">
    <orion:IconHelpButton ID="helpBtn" runat="server" HelpUrlFragment="OrionNCMWebManageEWNodes" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" runat="server">
    <div style="padding:10px;font-size:11px;font-family:Arial;">
        <a title="<%=Resources.NCMWebContent.WEBDATA_VK_357 %>" style="text-decoration:underline;" href="/Orion/Admin"><%=Resources.NCMWebContent.WEBDATA_VK_357 %></a>&nbsp;<img alt="" style="vertical-align:middle;" src="/Orion/images/breadcrumb_arrow.gif"/>&nbsp;
        <a title="<%=Resources.NCMWebContent.WEBDATA_VK_358 %>" style="text-decoration:underline;" href="/Orion/Nodes/Default.aspx"><%=Resources.NCMWebContent.WEBDATA_VK_358 %></a>&nbsp;<img alt="" style="vertical-align:middle;" src="/Orion/images/breadcrumb_arrow.gif"/>
    </div>
</asp:Content>

<asp:Content ID="ManageEWNodeContent" ContentPlaceHolderID="ncmMainContentPlaceHolder" runat="server">
    <script language="JavaScript" type="text/javascript">
        function trim(string)   
        {   
          return string.replace(/(^\s+)|(\s+$)/g, "");   
        }
        
        function EWNameClientValidate(ctl, args)
        {
            args.IsValid = (trim(args.Value).indexOf(" ", 0) < 0);
        }

        function compareEWSecretsValidate(ctl, args)
        {
            var SecretCtrl = document.getElementById("<%=txtSecret.ClientID %>");
            var SecretVerifyCtrl = document.getElementById("<%=txtSecretVerify.ClientID %>");
            args.IsValid = SecretCtrl.value == SecretVerifyCtrl.value;
        }

        function InitValidators()
        {
            var EWNameCtrl = document.getElementById("<%=txtEWName.ClientID %>");
            ValidatorEnable(document.getElementById('<%=customValidatorEWName.ClientID%>'), !EWNameCtrl.disabled);
            
            var ImportanceCtrl = document.getElementById("<%=txtImportance.ClientID %>");
            ValidatorEnable(document.getElementById('<%=validatorImportance.ClientID%>'), !ImportanceCtrl.disabled);
            ValidatorEnable(document.getElementById('<%=validatorRangeImportance.ClientID%>'), !ImportanceCtrl.disabled);        
            
            var DomainCtrl = document.getElementById("<%=txtDomain.ClientID %>");
            ValidatorEnable(document.getElementById('<%=validatorDomain.ClientID%>'), !DomainCtrl.disabled);
            ValidatorEnable(document.getElementById('<%=customValidatorEWDomain.ClientID%>'), !DomainCtrl.disabled);        
            
            var SecretCtrl = document.getElementById("<%=txtSecret.ClientID %>");
            ValidatorEnable(document.getElementById('<%=validatorSecret.ClientID%>'), !SecretCtrl.disabled  && IsDomainChanged());
            
            var RoleCtrl = document.getElementById("<%=txtRole.ClientID %>");
            ValidatorEnable(document.getElementById('<%=customValidatorEWRole.ClientID%>'), !RoleCtrl.disabled);
            
            var KeywordsCtrl = document.getElementById("<%=txtKeywords.ClientID %>");
            ValidatorEnable(document.getElementById('<%=customValidatorEWKewwords.ClientID%>'), !KeywordsCtrl.disabled);        
            
            ValidatorEnable(document.getElementById('<%=compareEWSecrets.ClientID%>'), true);
            ValidatorValidate(document.getElementById('<%=compareEWSecrets.ClientID%>'));                
        }
        
        function IsDomainChanged()
        {
            var MultiNodes = "<%=MultipleNodes %>";
            var InitDomain = "<%=InitialDomain %>";
            var DomainCtrl = document.getElementById("<%=txtDomain.ClientID %>")
            if (MultiNodes != true)
            {
                return DomainCtrl.value != InitDomain
            }
            return true;
        }
        
    </script>
    <style type="text/css">
        div#container
        {
	        position: static;
        }
    </style>

    <link href="../../styles/EW.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
    <script type="text/javascript" src="../../../JavaScript/main.js"></script>
    
    <div style="padding:10px;">
	    <asp:UpdatePanel ID="updatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="padding:10px;border-width:1px;border-style:solid;border-color:#DCDBD7;background-color:#FFFFFF">
                            <asp:UpdatePanel ID="EWCommonUpdatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table width="100%">
                                        <tr id="trHolder" runat="server"></tr>
                                        <tr>
                                            <td>
                                                <%=Subtitle %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="cells">
	                                            <ul>
		                                            <%=NodeNames %>
	                                            </ul>
                                            </td>
                                        </tr>
                                        <% if (!MultipleNodes)
                                           { %>
                                        <tr>
                                            <td class="cells">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="width: 10px; background-color: #E6F2F7;">
                                                        </td>
                                                        <td style="background-color: #E6F2F7;">
                                                            <%=Resources.NCMWebContent.WEBDATA_VK_373 %>
                                                        </td>
                                                        <td style="background-color: #E6F2F7;">
                                                            <asp:Label ID="lblIOSVersion" runat="server" Text="00.00"></asp:Label>
                                                        </td>
                                                        <td style="background-color: #E6F2F7; vertical-align:middle;">
                                                            &nbsp;&nbsp;
                                                            <asp:Image ID="imgIOSVersion" runat="server" ImageUrl="~/Orion/NCM/Resources/images/EnergyWise.NotSupport.Icon.gif" />
                                                            &nbsp;
                                                        </td>
                                                        <td style="background-color: #E6F2F7; padding-right: 10px;">
                                                            <asp:Label ID="lblIOSMessage" runat="server" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 15px">
                                            </td>
                                        </tr>
                                        <% } %>
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="EWNodeUpdatePanel" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <table id="EWPropTable" runat="server" cellspacing="0" border="0" width="100%" style="background-color:#E6F2F7;">
                                                            <tr>
                                                                <td id="EWFeaturesHolder" runat="server" class="CheckBoxCell" nowrap="nowrap">
                                                                    <%=Resources.NCMWebContent.WEBDATA_VK_374 %>
                                                                </td>
                                                                <td class="cellsInput" style="padding-top:6px;">
                                                                    <asp:DropDownList ID="ddlEWFeatures" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlEWFeatures_SelectedIndexChanged">
                                                                        <asp:ListItem Value="Enabled" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_388 %>" />
                                                                        <asp:ListItem Value="Disabled" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_389 %>" />
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="CheckBoxCell" nowrap="nowrap">
                                                                    <%if (!MultipleNodes)
                                                                      {%>
                                                                        <span>
                                                                            <%=Resources.NCMWebContent.WEBDATA_VK_375 %>
                                                                        </span>
                                                                    <%}
                                                                      else
                                                                      { %>
                                                                        <asp:CheckBox ID="chbEWName" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_375 %>" runat="server" OnCheckedChanged="chbEWName_Check" AutoPostBack="true" />
                                                                    <%} %>
                                                                </td>
                                                                <td class="cellsInput">
                                                                    <asp:TextBox ID="txtEWName" runat="server" Width="300px" CssClass="clsLgnTxBx"></asp:TextBox>
                                                                    <asp:CustomValidator runat="server" ID="customValidatorEWName" ControlToValidate="txtEWName" ValidationGroup="GroupMain"
					                                                    ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_390 %>" Display="dynamic" OnServerValidate="EnergyWiseName_ServerValidate"
					                                                    ClientValidationFunction="EWNameClientValidate">
		                                                            </asp:CustomValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="CheckBoxCell" nowrap="nowrap">
                                                                    <%if (!MultipleNodes)
                                                                      {%>
                                                                        <span>
                                                                            <%=Resources.NCMWebContent.WEBDATA_VK_376 %>
                                                                        </span>
                                                                    <%}
                                                                      else
                                                                      { %>
                                                                        <asp:CheckBox ID="chbEWKeywords" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_376 %>" runat="server" OnCheckedChanged="chbEWKeywords_Check" AutoPostBack="true" />
                                                                    <%} %>
                                                                </td>
                                                                <td class="cellsInput">
                                                                    <asp:TextBox ID="txtKeywords" runat="server" Width="300px" CssClass="clsLgnTxBx"></asp:TextBox>
                                                                    <asp:CheckBox ID="chbEWKeywordsAdd" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_391 %>" runat="server" />
                                                                    <asp:CustomValidator runat="server" ID="customValidatorEWKewwords" ControlToValidate="txtKeywords" ValidationGroup="GroupMain"
					                                                    ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_392 %>" Display="dynamic" OnServerValidate="EnergyWiseName_ServerValidate"
					                                                    ClientValidationFunction="EWNameClientValidate">
		                                                            </asp:CustomValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="CheckBoxCell" nowrap="nowrap">
                                                                    <%if (!MultipleNodes)
                                                                      {%>
                                                                        <span>
                                                                            <%=Resources.NCMWebContent.WEBDATA_VK_377 %>
                                                                        </span>
                                                                    <%}
                                                                      else
                                                                      { %>
                                                                        <asp:CheckBox ID="chbEWDefaultPowerLevel" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_377 %>" runat="server" OnCheckedChanged="chbEWDefaultPowerLevel_Check" AutoPostBack="true" />
                                                                    <%} %>
                                                                </td>
                                                                <td class="cellsInput">
                                                                    <asp:DropDownList ID="ddlDefaultPowerLevel" runat="server" Width="150px">
                                                                        <asp:ListItem Value="0" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_413 %>" />
                                                                        <asp:ListItem Value="1" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_414 %>" />
                                                                        <asp:ListItem Value="2" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_415 %>" />
                                                                        <asp:ListItem Value="3" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_416 %>" />
                                                                        <asp:ListItem Value="4" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_417 %>" />
                                                                        <asp:ListItem Value="5" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_418 %>" />
                                                                        <asp:ListItem Value="6" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_419 %>" />
                                                                        <asp:ListItem Value="7" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_420 %>" />
                                                                        <asp:ListItem Value="8" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_421 %>" />
                                                                        <asp:ListItem Value="9" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_422 %>" />
                                                                        <asp:ListItem Value="10" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_423 %>" />
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="CheckBoxCell" nowrap="nowrap">
                                                                    <%if (!MultipleNodes)
                                                                      {%>
                                                                        <span>
                                                                            <%=Resources.NCMWebContent.WEBDATA_VK_378 %>
                                                                        </span>
                                                                    <%}
                                                                      else
                                                                      { %>
                                                                        <asp:CheckBox ID="chbEWImportance" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_378 %>" runat="server" OnCheckedChanged="chbEWImportance_Check" AutoPostBack="true" />
                                                                    <%} %>
                                                                </td>
                                                                <td class="cellsInput">
                                                                    <asp:TextBox ID="txtImportance" runat="server" Width="30px" CssClass="clsLgnTxBx"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="validatorImportance" runat="server" Display="Dynamic" ValidationGroup="GroupMain" 
                                                                        ControlToValidate="txtImportance" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_393 %>" >
                                                                    </asp:RequiredFieldValidator>
                                                                    <asp:RangeValidator ID="validatorRangeImportance" ControlToValidate="txtImportance" Display="Dynamic" ValidationGroup="GroupMain"
                                                                        runat="server" Type="Integer" MinimumValue="0" MaximumValue="100" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_394 %>">
                                                                    </asp:RangeValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="CheckBoxCell" nowrap="nowrap">
                                                                    <%if (!MultipleNodes)
                                                                      {%>
                                                                        <span>
                                                                            <%=Resources.NCMWebContent.WEBDATA_VK_379 %>
                                                                        </span>
                                                                    <%}
                                                                      else
                                                                      { %>
                                                                        <asp:CheckBox ID="chbEWRole" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_379 %>" runat="server" OnCheckedChanged="chbEWRole_Check" AutoPostBack="true" />
                                                                    <%} %>
                                                                </td>
                                                                <td class="cellsInput">
                                                                    <asp:TextBox ID="txtRole" runat="server" Width="200px" CssClass="clsLgnTxBx"></asp:TextBox>
                                                                    <asp:CustomValidator runat="server" ID="customValidatorEWRole" ControlToValidate="txtRole" ValidationGroup="GroupMain"
					                                                    ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_395 %>" Display="dynamic" OnServerValidate="EnergyWiseName_ServerValidate"
					                                                    ClientValidationFunction="EWNameClientValidate">
		                                                            </asp:CustomValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="CheckBoxCell" nowrap="nowrap">
                                                                    <%if (!MultipleNodes)
                                                                      {%>
                                                                        <span>
                                                                            <%=Resources.NCMWebContent.WEBDATA_VK_380 %>
                                                                        </span>
                                                                    <%}
                                                                      else
                                                                      { %>
                                                                        <asp:CheckBox ID="chbEWDomain" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_380 %>" runat="server" OnCheckedChanged="chbEWDomain_Check" AutoPostBack="true" />
                                                                    <%} %>
                                                                </td>
                                                                <td class="cellsInput">
                                                                    <asp:TextBox ID="txtDomain" runat="server" Width="200px" CssClass="clsLgnTxBx"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="validatorDomain" runat="server" Display="Dynamic" ValidationGroup="GroupMain" 
                                                                        ControlToValidate="txtDomain" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_396 %>" >
                                                                    </asp:RequiredFieldValidator>
                                                                    <asp:CustomValidator runat="server" ID="customValidatorEWDomain" ControlToValidate="txtDomain" ValidationGroup="GroupMain"
					                                                    ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_397 %>" Display="dynamic" OnServerValidate="EnergyWiseName_ServerValidate"
					                                                    ClientValidationFunction="EWNameClientValidate">
		                                                            </asp:CustomValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="CheckBoxCell" colspan="2" nowrap="nowrap">
                                                                    <%if (!MultipleNodes)
                                                                      {%>
                                                                        <span>
                                                                            <%=Resources.NCMWebContent.WEBDATA_VK_381 %>
                                                                        </span>
                                                                    <%}
                                                                      else
                                                                      { %>
                                                                        <asp:CheckBox ID="chbEWSecret" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_381 %>" runat="server" OnCheckedChanged="chbEWSecret_Check" AutoPostBack="true" />
                                                                    <%} %>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="CheckBoxCell" colspan="2" nowrap="nowrap">
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                <%=Resources.NCMWebContent.WEBDATA_VK_382 %>
                                                                            </td>
                                                                            <td>
                                                                               <asp:RadioButtonList ID="rblSecretEncription" runat="server" RepeatDirection="Horizontal">
                                                                                    <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_411 %>"></asp:ListItem>
                                                                                    <asp:ListItem Selected="True" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_412 %>"></asp:ListItem>
                                                                               </asp:RadioButtonList>                          
                                                                           </td>
                                                                        </tr>
                                                                   </table>                  
                                                                </td>                                        
                                                            </tr>
                                                            <tr>
                                                                <td class="CheckBoxCell" nowrap="nowrap">
                                                                    <%=Resources.NCMWebContent.WEBDATA_VK_383 %>
                                                                </td>
                                                                <td class="cellsInput">
                                                                    <asp:TextBox ID="txtSecret" runat="server" Width="200px" TextMode="Password" CssClass="clsLgnTxBx"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="validatorSecret" runat="server" Enable="False" Display="Dynamic" ValidationGroup="GroupMain" 
                                                                        ControlToValidate="txtSecret" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_398 %>" >
                                                                    </asp:RequiredFieldValidator>
                                                                    <asp:RegularExpressionValidator ID="regexValidatorEWsecret" runat="server" Display="Dynamic"
                                                                        ControlToValidate="txtSecret" ValidationExpression="^\S+$"
                                                                        ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_399 %>" >
                                                                    </asp:RegularExpressionValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="CheckBoxCell" nowrap="nowrap">
                                                                    <%=Resources.NCMWebContent.WEBDATA_VK_384 %>
                                                                </td>
                                                                <td class="cellsInput" style="padding-bottom:6px;">
                                                                    <asp:TextBox ID="txtSecretVerify" runat="server" Width="200px" TextMode="Password" CssClass="clsLgnTxBx" ></asp:TextBox>
                                                                    <asp:CustomValidator runat="server" ID="compareEWSecrets" ValidationGroup="GroupMain"
					                                                    ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_400 %>" Display="Dynamic"
					                                                    Enable="True" ClientValidationFunction="compareEWSecretsValidate" OnServerValidate="CompareEWSecrets_ServerValidate">
		                                                            </asp:CustomValidator>
                                                                </td>
                                                            </tr>                                    
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                             <td style="padding-bottom:10px;">
                                                <asp:CheckBox ID="cbWritetoNvRam" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_385 %>" runat="server" AutoPostBack="false" />    
                                             </td>
                                        </tr>
                                        <tr id="ApprovalCommentsHolder" runat="server" visible="false">
                                            <td>
                                                <%=Resources.NCMWebContent.WEBDATA_VK_386 %><br />
                                                <asp:TextBox ID="txtApprovalComments" runat="server" TextMode="MultiLine" Width="300px" Height="45px" spellcheck="false" Font-Name="Arial" Font-Size="9pt" />
                                                <ajaxToolkit:TextBoxWatermarkExtender ID="WatermarkExtender" runat="server" TargetControlID="txtApprovalComments" WatermarkText="<%$ Resources: NCMWebContent, WEBDATA_VK_387 %>" WatermarkCssClass="watermark" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" style="height: 40px; width:auto;padding-top:20px;padding-bottom:20px;">
                                                <orion:LocalizableButton ID="btnSubmit" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_372 %>" ValidationGroup="GroupMain" OnClientClick="InitValidators()" OnClick="ExecuteConfig_Click"></orion:LocalizableButton>
                                                <orion:LocalizableButton ID="btnCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" CausesValidation="false" OnClick="Cancel_Click"></orion:LocalizableButton>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td> 
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>            
    
    <ModalBox:ModalBox runat="server" ID="MsgBox" Width="500">
        <DialogContents>
            <div style="border-right: 1px solid; border-top: 1px solid; border-left: 1px solid; border-bottom: 1px solid;">
                <div style="background-image: url('/Orion/NCM/Resources/images/PopupHeader.gif'); background-repeat: repeat-x; background-position: left bottom; margin: 0 0 0 0; padding: 5px 5px 5px 5px;">
                    <table width="100%">
                        <tr>
                            <td class="modalBoxHeaderFont">
                                <%=MsgBoxTitle%>				
                            </td>
                            <td align="right">
                                <asp:ImageButton runat="server" id="btnCrossCancel" OnClick="CancelMsgBox_Click" ImageUrl="~/Orion/NCM/Resources/images/Button.CloseCross.gif" />
                            </td>                              
                        </tr>
                    </table>
                </div>
                <table width="100%">
                    <tr>
                        <td style="padding:5px;">
                           <%=MsgBoxContent%>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <orion:LocalizableButton ID="btnOKMsgBox" runat="server" DisplayType="Primary" LocalizedText="Ok" OnClick="OKMsgBox_Click"></orion:LocalizableButton>
                            <orion:LocalizableButton ID="btnCancelMsgBox" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="CancelMsgBox_Click"></orion:LocalizableButton>
                        </td>
                    </tr>
                 </table>
            </div>
        </DialogContents>
    </ModalBox:ModalBox>
</asp:Content>