﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.EWBusinessLayer;
using SolarWinds.NCM.Contracts.InformationService;

using StringRegistrar = SolarWinds.Orion.Core.Common.i18n.Registrar.ResourceManagerRegistrar;
using SolarWinds.NCM.Contracts;

public partial class Orion_NCM_ManageEWNodes : System.Web.UI.Page
{
    #region Private Members & Constants

    private SolarWinds.NCMModule.Web.Resources.ISWrapper _isLayer;
    private bool _multipleNodes = false;
    private string _initialDomain = string.Empty;
    private string _subtitle = string.Empty;

    private readonly string IOS_EW_NOT_ENABLED = Resources.NCMWebContent.WEBDATA_VK_401;
    private readonly string IOS_EW_CAPABLE = Resources.NCMWebContent.WEBDATA_VK_402;
    private readonly string IOS_EW_ENABLED = Resources.NCMWebContent.WEBDATA_VK_403;

    #endregion

    #region Protected Members

    protected string MsgBoxContent { get; set; }
    protected string MsgBoxTitle { get; set; }
    
    protected bool UseUserLevelLoginCreds
    {
        get
        {
            if (ViewState["UseUserLevelLoginCreds"] == null)
            {
                using (var proxy = _isLayer.Proxy)
                {
                    var connLevel = proxy.Cirrus.Settings.GetSetting(Settings.ConnectivityLevel, ConnectivityLevels.Default, null);
                    var useUserLevelLoginCreds = connLevel.Equals(ConnectivityLevels.Level3);
                    ViewState["UseUserLevelLoginCreds"] = useUserLevelLoginCreds;
                    return useUserLevelLoginCreds;
                }
            }
            else
            {
                return Convert.ToBoolean(ViewState["UseUserLevelLoginCreds"]);
            }
        }
    }

    #endregion

    #region Overrided Methods

    public override void Validate(string validationGroup)
    {
        customValidatorEWName.Enabled = txtEWName.Enabled;
        customValidatorEWDomain.Enabled = txtDomain.Enabled;
        customValidatorEWKewwords.Enabled = txtKeywords.Enabled;
        customValidatorEWRole.Enabled = txtRole.Enabled;
        validatorImportance.Enabled = txtImportance.Enabled;
        validatorRangeImportance.Enabled = txtImportance.Enabled;
        validatorDomain.Enabled = txtDomain.Enabled;
        validatorSecret.Enabled = txtSecret.Enabled && IsDomainChanged();
        base.Validate(validationGroup);
    }

    #endregion

    #region Event Handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString[@"IsErrors"] == null)
        {
            var Url = $@"/Orion/NCM/Resources/EnergyWise/EWErrorPage.aspx?Nodes={Request.QueryString[@"Nodes"]}";
            Page.Response.Redirect(Url);
        }
        else
        {
            if (!Convert.ToBoolean(Request.QueryString[@"IsErrors"]))
            {
                _isLayer = new SolarWinds.NCMModule.Web.Resources.ISWrapper();

                trHolder.Visible = false;
                if (UseUserLevelLoginCreds)
                {
                    var suggestionWarn = new SuggestionWarning();
                    suggestionWarn.WarningMessage = string.Format(Resources.NCMWebContent.WEBDATA_VK_714, @"<a style='color:#336699;text-decoration:underline;' href='/Orion/NCM/Admin/Settings/UserLevelLoginCreds.aspx'>", @"</a>");

                    var td = new HtmlTableCell();
                    td.Style.Add(@"padding-bottom", @"10px");
                    td.Controls.Add(suggestionWarn);
                    trHolder.Cells.Add(td);
                    trHolder.Visible = true;
                }

                if (ConfigChangeApprovalHelper.ShowSendApprovalRequestButton)
                {
                    ApprovalCommentsHolder.Visible = true;
                    btnSubmit.Text = StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"LIBCODE_VK_30");
                }

                SetDomainSharedSecret();
                if (!Page.IsPostBack)
                {
                    var orionIDsList = GetOrionNodeIDs();
                    if (!_multipleNodes)
                    {
                        var ewNode = new EWNodeObject();
                        if (orionIDsList.Count == 1) ewNode.Initialize(orionIDsList[0]);
                        ViewState["EWNode"] = ewNode;
                        PopulateData(ewNode);
                    }
                    else
                    {
                        var ewNodeList = InitializeEWNodeList(orionIDsList);
                        ViewState["EWNodeList"] = ewNodeList;
                        SetEnableToFields(false);
                    }
                }
                else
                {
                    if (ViewState["IsMultiple"] != null)
                    {
                        _multipleNodes = (bool)ViewState["IsMultiple"];
                    }
                }

                if (!_multipleNodes)
                {
                    Page.Title = Resources.NCMWebContent.WEBDATA_VK_404;
                    _subtitle = Resources.NCMWebContent.WEBDATA_VK_406;
                }
                else
                {
                    Page.Title = Resources.NCMWebContent.WEBDATA_VK_405;
                    _subtitle = Resources.NCMWebContent.WEBDATA_VK_407;
                }
            }
        }
    }

    protected void EnergyWiseName_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = args.Value.Trim().IndexOf(' ') < 0;
    }

    protected void CompareEWSecrets_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = txtSecret.Text == txtSecretVerify.Text;
    }

    #region Check Boxes

    public void chbEWName_Check(object sender, EventArgs e)
    {
        txtEWName.Enabled = chbEWName.Checked;
        txtEWName.Text = string.Empty;
    }

    public void chbEWKeywords_Check(object sender, EventArgs e)
    {
        txtKeywords.Enabled = chbEWKeywords.Checked;
        txtKeywords.Text = string.Empty;
        chbEWKeywordsAdd.Enabled = chbEWKeywords.Checked;
        if (!chbEWKeywords.Checked && chbEWKeywordsAdd.Checked) chbEWKeywordsAdd.Checked = false;
    }

    public void chbEWDefaultPowerLevel_Check(object sender, EventArgs e)
    {
        ddlDefaultPowerLevel.Enabled = chbEWDefaultPowerLevel.Checked;
        if (chbEWDefaultPowerLevel.Checked)
            ddlDefaultPowerLevel.SelectedValue = @"10";
        else
            ddlDefaultPowerLevel.SelectedIndex = -1;
    }

    public void chbEWImportance_Check(object sender, EventArgs e)
    {
        txtImportance.Enabled = chbEWImportance.Checked;
        if (chbEWImportance.Checked)
            txtImportance.Text = @"100";
        else
            txtImportance.Text = string.Empty;
    }

    public void chbEWRole_Check(object sender, EventArgs e)
    {
        txtRole.Enabled = chbEWRole.Checked;
        txtRole.Text = string.Empty;
    }

    public void chbEWDomain_Check(object sender, EventArgs e)
    {
        txtDomain.Enabled = chbEWDomain.Checked;
        txtDomain.Text = string.Empty;
    }

    public void chbEWSecret_Check(object sender, EventArgs e)
    {
        txtSecret.Enabled = chbEWSecret.Checked;
        txtSecret.Text = string.Empty;
        txtSecretVerify.Enabled = chbEWSecret.Checked;
        txtSecretVerify.Text = string.Empty;
        rblSecretEncription.Enabled = chbEWSecret.Checked;
        if (!chbEWSecret.Checked)
        {
            txtSecret.Attributes.Add(@"value", string.Empty);
            txtSecretVerify.Attributes.Add(@"value", string.Empty);
        }
    }

    protected void ddlEWFeatures_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlEWFeatures.SelectedValue == @"Enabled")
        {
            if (_multipleNodes)
            {
                SetEnableToCheckBoxes(true);
                SyncronizeCheckBoxesAndFields();
            }
            else
                SetEnableToFields(true);
        }
        else
        {
            if (_multipleNodes)
            {
                SetEnableToCheckBoxes(false);
            }
            SetEnableToFields(false);
        }
    }

    #endregion

    #region Buttons

    protected void ExecuteConfig_Click(object sender, EventArgs e)
    {
        regexValidatorEWsecret.Validate();
        if (regexValidatorEWsecret.IsValid)
        {
            if (!String.IsNullOrEmpty(this.txtSecret.Text))
                ViewState["secret_word"] = ((rblSecretEncription.SelectedIndex == 0) ? @"7 " : @"0 ") + this.txtSecret.Text;
            else
                ViewState["secret_word"] = string.Empty;

            if (ConfigChangeApprovalHelper.ShowSendApprovalRequestButton)
            {
                if (CommonHelper.IsDemoMode())
                {
                    Page.Response.Redirect("/Orion/Nodes/Default.aspx");
                    return;
                }                
                Execution();
            }
            else
            {
                MsgBoxTitle = Resources.NCMWebContent.WEBDATA_VK_372;
                MsgBoxContent = string.Format(Resources.NCMWebContent.WEBDATA_VK_410, HttpUtility.HtmlEncode(NodeCaption), @"<br/>");
                MsgBox.Show();
            }

            
        }
    }

    protected void Cancel_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect("/Orion/Nodes/Default.aspx");
    }

    public void OKMsgBox_Click(object sender, EventArgs e)
    {
        MsgBox.Hide();
        if (ConfigChangeApprovalHelper.ShowSendApprovalRequestButton)
        {
            Page.Response.Redirect("/Orion/Nodes/Default.aspx");
        }
        else
        {
            Execution();
        }
    }

    public void CancelMsgBox_Click(object sender, EventArgs e)
    {
        MsgBox.Hide();
    }

    #endregion

    #endregion

    #region Public Properties

    public bool MultipleNodes
    {
        get { return _multipleNodes; }
    }

    public string NodeNames
    {
        get { return PopulateNodeCaptionList(); }
    }

    public string NodeCaption
    {
        get { return PopulateNodeCaptionListMsg(); }
    }

    public string InitialDomain
    {
        get
        {
            if (!_multipleNodes)
            {
                var ewNode = GetInitEWNodeObject();
                _initialDomain = ewNode.EnergyWiseDomain;
            }
            return HttpUtility.HtmlEncode(_initialDomain);
        }
    }

    public string Subtitle
    {
        get { return _subtitle; }
    }

    #endregion

    #region Private Methods

    private void PopulateData(EWNodeObject ewNode)
    {
        if (!_multipleNodes)
        {
            var captions = new string[1];
            captions[0] = $@"{ewNode.NodeCaption}:{ewNode.NCMNodeId}";
            ViewState["EWNodeCaptions"] = captions;

            //IOS Version
            lblIOSVersion.Text = ewNode.IOSVersion;
            var iosMsg = string.Empty;
            if (ewNode.IsEnergyWiseSupported && ewNode.IsIOSRight && ewNode.IsEnergyWiseEnabled)
                iosMsg = IOS_EW_ENABLED;
            else
            {
                if (ewNode.IsIOSRight)
                    iosMsg = IOS_EW_CAPABLE;
                else
                    iosMsg = IOS_EW_NOT_ENABLED;
            }

            imgIOSVersion.ImageUrl = ewNode.IsIOSRight ? @"~/Orion/NCM/Resources/images/EnergyWise.Support.Icon.gif" : @"~/Orion/NCM/Resources/images/EnergyWise.NotSupport.Icon.gif";

            lblIOSMessage.Text = iosMsg;
            this.ddlEWFeatures.SelectedValue = ewNode.IsEnergyWiseEnabled ? @"Enabled" : @"Disabled";
            this.txtEWName.Text = ewNode.EnergyWiseName;
            this.txtKeywords.Text = ewNode.EnergyWiseKeywords;
            this.ddlDefaultPowerLevel.SelectedIndex = ewNode.EnergyWisePowerLevel;
            this.txtImportance.Text = ewNode.EnergyWiseImportance.ToString();
            this.txtRole.Text = ewNode.EnergyWiseRole;
            this.txtDomain.Text = ewNode.EnergyWiseDomain;

            SetEnableToFields(ewNode.IsEnergyWiseEnabled);
            SetReadOnlyMode(ewNode);
        }
    }

    private void SetReadOnlyMode(EWNodeObject ewNode)
    {
        if (!ewNode.IsEnergyWiseSupported || !ewNode.IsIOSRight)
        {
            EWPropTable.Disabled = true;
            ddlEWFeatures.Enabled = false;
            btnSubmit.Enabled = false;
            btnCancel.Enabled = false;
        }
    }

    private void ClearControlValues()
    {
        this.ddlEWFeatures.SelectedValue = @"Enabled";
        this.txtEWName.Text = string.Empty;
        this.txtKeywords.Text = string.Empty;
        this.ddlDefaultPowerLevel.SelectedIndex = -1;
        this.txtImportance.Text = string.Empty;
        this.txtRole.Text = string.Empty;
        this.txtDomain.Text = string.Empty;
        this.txtSecret.Text = string.Empty;
        this.txtSecretVerify.Text = string.Empty;

        chbEWName.Checked = false;
        chbEWKeywords.Checked = false;
        chbEWKeywordsAdd.Checked = false;
        chbEWDefaultPowerLevel.Checked = false;
        chbEWImportance.Checked = false;
        chbEWRole.Checked = false;
        chbEWDomain.Checked = false;
        chbEWSecret.Checked = false;
    }

    private void SetEnableToFields(bool isEnable)
    {
        txtEWName.Enabled = isEnable;
        txtKeywords.Enabled = isEnable;
        chbEWKeywordsAdd.Enabled = isEnable;
        ddlDefaultPowerLevel.Enabled = isEnable;
        txtImportance.Enabled = isEnable;
        txtRole.Enabled = isEnable;
        txtDomain.Enabled = isEnable;
        txtSecret.Enabled = isEnable;
        txtSecretVerify.Enabled = isEnable;
        rblSecretEncription.Enabled = isEnable;
    }

    private void SetEnableToCheckBoxes(bool isEnable)
    {
        chbEWDefaultPowerLevel.Enabled = isEnable;
        chbEWDomain.Enabled = isEnable;
        chbEWName.Enabled = isEnable;
        chbEWImportance.Enabled = isEnable;
        chbEWKeywords.Enabled = isEnable;
        chbEWKeywordsAdd.Enabled = isEnable;
        chbEWRole.Enabled = isEnable;
        chbEWDomain.Enabled = isEnable;
        chbEWSecret.Enabled = isEnable;
    }

    private void SyncronizeCheckBoxesAndFields()
    {
        ddlDefaultPowerLevel.Enabled = chbEWDefaultPowerLevel.Checked;
        txtDomain.Enabled = chbEWDomain.Checked;
        txtEWName.Enabled = chbEWName.Checked;
        txtImportance.Enabled = chbEWImportance.Checked;
        txtKeywords.Enabled = chbEWKeywords.Checked;
        chbEWKeywordsAdd.Enabled = chbEWKeywords.Checked;
        txtRole.Enabled = chbEWRole.Checked;
        txtDomain.Enabled = chbEWDomain.Checked;
        txtSecret.Enabled = chbEWSecret.Checked;
        txtSecretVerify.Enabled = chbEWSecret.Checked;
        chbEWSecret.Enabled = chbEWSecret.Checked;
    }

    private string PopulateNodeCaptionList()
    {
        var captions = new string[1];
        var result = new StringBuilder();
        if (ViewState["EWNodeCaptions"] != null)
        {
            captions = (string[])ViewState["EWNodeCaptions"];
        }
       
        foreach (var caption in captions)
        {
            var title = caption.Split(':')[0];
            var id = caption.Split(':')[1];
            result.AppendLine(
                $@"<li><a href='/Orion/NCM/NodeDetails.aspx?NodeID={HttpUtility.HtmlEncode(id)}' style='text-decoration:underline;'>{HttpUtility.HtmlEncode(title)}</a></li>");
        }
        return result.ToString();
    }

    private string PopulateNodeCaptionListMsg()
    {
        var captions = new string[1];
        var result = new StringBuilder();
        var comma = @", ";
        if (ViewState["EWNodeCaptions"] != null)
        {
            captions = (string[])ViewState["EWNodeCaptions"];
        }
        for (var i = 0; i < captions.Length; i++)
        {
            var title = captions[i].Split(':')[0];
            result.Append(title);
            if (i < captions.Length - 1) result.Append(comma);
        }
        return result.ToString();
    }

    private List<EWNodeObject> InitializeEWNodeList(List<int> orionIDsList)
    {
        var ewNodeList = new List<EWNodeObject>();
        EWNodeObject ewNode;
        var captions = new string[orionIDsList.Count];
        for (var i = 0; i < orionIDsList.Count; i++)
        {
            ewNode = new EWNodeObject();
            ewNode.Initialize(orionIDsList[i]);
            ewNodeList.Add(ewNode);
            captions[i] = $@"{ewNode.NodeCaption}:{ewNode.NCMNodeId}";
        }
        ViewState["EWNodeCaptions"] = captions;
        return ewNodeList;
    }

    private List<int> GetOrionNodeIDs()
    {
        var orionIDsList = new List<int>();
        var NodeIDs = string.Empty;
        var ids = new string[1];
        if (Request.QueryString[@"Nodes"] != null)
        {
            NodeIDs = Convert.ToString(Request.QueryString[@"Nodes"]);
        }
        if (NodeIDs != string.Empty)
        {
            var split = ',';
            ids = NodeIDs.Split(split);
        }
        else
        {          
            if (Session[@"OrionNodeIDs"] != null)
            {
                ids = (string[])Session[@"OrionNodeIDs"];
            }
        }
        foreach (var idStr in ids)
        {
            var idInt = 0;
            Int32.TryParse(idStr, out idInt);
            orionIDsList.Add(idInt);
        }
        _multipleNodes = orionIDsList.Count > 1 ? true : false;

        if (_multipleNodes)
            EWFeaturesHolder.Style.Add(@"padding-left", @"9px");

        ViewState["IsMultiple"] = _multipleNodes;
        return orionIDsList;
    }

    private string GenerateScript()
    {
        var result = string.Empty;
        var secret = ViewState["secret_word"].ToString();
        var ewNodeUpd = new EWNodeObject();
        ewNodeUpd.IsEnergyWiseEnabled = this.ddlEWFeatures.SelectedValue == @"Enabled" ? true : false;
        if (!_multipleNodes)
        {
            ewNodeUpd.EnergyWiseName = this.txtEWName.Text.Trim();
            ewNodeUpd.EnergyWiseKeywords = this.txtKeywords.Text.Trim();
            ewNodeUpd.EnergyWisePowerLevel = this.ddlDefaultPowerLevel.SelectedIndex;
            var importance = 100;
            Int32.TryParse(this.txtImportance.Text, out  importance);
            ewNodeUpd.EnergyWiseImportance = importance;
            ewNodeUpd.EnergyWiseRole = this.txtRole.Text.Trim();
            ewNodeUpd.EnergyWiseDomain = this.txtDomain.Text.Trim();
            ewNodeUpd.EnergyWiseSecret = secret;
        }
        else
        {
            ewNodeUpd.EnergyWiseName = chbEWName.Checked ? this.txtEWName.Text.Trim() : null;
            ewNodeUpd.EnergyWiseKeywords = chbEWKeywords.Checked ? this.txtKeywords.Text.Trim() : null;
            ewNodeUpd.EnergyWisePowerLevel = chbEWDefaultPowerLevel.Checked ? this.ddlDefaultPowerLevel.SelectedIndex : -1;
            var importance = -1;
            Int32.TryParse(this.txtImportance.Text, out  importance);
            ewNodeUpd.EnergyWiseImportance = chbEWImportance.Checked ? importance : -1;
            ewNodeUpd.EnergyWiseRole = chbEWRole.Checked ? this.txtRole.Text.Trim() : null;
            ewNodeUpd.EnergyWiseDomain = chbEWDomain.Checked ? this.txtDomain.Text.Trim() : null;
            ewNodeUpd.EnergyWiseSecret = chbEWSecret.Checked ? secret : null;
        }
        var deletedNeighbors = new List<EWNodeObjectNeighbors>();
        var keywordAdditive = chbEWKeywordsAdd.Checked;

        result = EWGeneralScriptGenerator.GenerateScript(ewNodeUpd, deletedNeighbors, keywordAdditive);
        return result;
    }

    private void Execution()
    {
        var NodeIdList = new Dictionary<string, EWNodeTransferStatus>();
        EWNodeTransferStatus ewNodeTransferStatus;
        var nodeIDs = new Guid[1];
        var script = GenerateScript();
        if (_multipleNodes)
        {
            var ewNodeList = new List<EWNodeObject>();
            if (ViewState["EWNodeList"] != null)
            {
                ewNodeList = (List<EWNodeObject>)ViewState["EWNodeList"];
            }
            if (ewNodeList.Count > 0)
            {
                nodeIDs = new Guid[ewNodeList.Count];
                for (var i = 0; i < ewNodeList.Count; i++)
                {
                    nodeIDs[i] = new Guid(ewNodeList[i].NCMNodeId);

                    ewNodeTransferStatus = new EWNodeTransferStatus();
                    ewNodeTransferStatus.NPMNodeID = ewNodeList[i].NPMNodeId;
                    ewNodeTransferStatus.IsPollNode = false;
                    NodeIdList.Add(ewNodeList[i].NCMNodeId.ToUpperInvariant(), ewNodeTransferStatus);
                }
            }
        }
        else
        {
            var ewNode = GetInitEWNodeObject();
            if (!String.IsNullOrEmpty(ewNode.NCMNodeId))
            {
                nodeIDs = new Guid[1];
                nodeIDs[0] = new Guid(ewNode.NCMNodeId);

                ewNodeTransferStatus = new EWNodeTransferStatus();
                ewNodeTransferStatus.NPMNodeID = ewNode.NPMNodeId;
                ewNodeTransferStatus.IsPollNode = false;
                NodeIdList.Add(ewNode.NCMNodeId.ToUpperInvariant(), ewNodeTransferStatus);
            }
        }
        if (cbWritetoNvRam.Checked) script += @"${SaveConfig}";
        if (nodeIDs.Length > 0)
        {
            if (ConfigChangeApprovalHelper.ShowSendApprovalRequestButton)
            {

                var ticket = new NCMApprovalTicket();
                ticket.ConfigType = string.Empty;
                ticket.Reboot = false;
                ticket.RequestType = NCMApprovalTicket.TYPE_MANAGE_EW;
                ticket.Script = string.Empty;
                ticket.Comments = txtApprovalComments.Text;
                foreach (var nodeId in nodeIDs)
                {
                    ticket.AddNode(nodeId, script);
                }
                ConfigChangeApprovalHelper.SendRequestForApproval(ticket);
                txtApprovalComments.Text = string.Empty;
            }
            else
            {
                using (var proxy = _isLayer.Proxy)
                {
                    var transferIdList = proxy.Cirrus.ConfigArchive.ExecuteScript(nodeIDs, script);
                    
                    Session[@"TransferIdList"] = transferIdList;
                    Session[@"NodeIdList"] = NodeIdList;

                    Page.Response.Redirect("/Orion/NCM/Resources/EnergyWise/EWTransferStatus.aspx?TransferOn=Nodes");
                }
            }
        }
    }

    private EWNodeObject GetInitEWNodeObject()
    {
        var ewNode = new EWNodeObject();
        if (ViewState["EWNode"] != null)
        {
            ewNode = (EWNodeObject)ViewState["EWNode"];
        }
        return ewNode;
    }

    private bool IsDomainChanged()
    {
        var result = false;
        if (!_multipleNodes)
        {
            var ewNode = GetInitEWNodeObject();
            result = ewNode.EnergyWiseDomain != txtDomain.Text;
        }
        else
        {
            result = true;
        }
        return result;
    }

    private void SetDomainSharedSecret()
    {
        if (ddlEWFeatures.SelectedValue == @"Disabled")
        {
            txtSecret.Attributes.Add(@"value", string.Empty);
            txtSecretVerify.Attributes.Add(@"value", string.Empty);
            return;
        }

        if (!string.IsNullOrEmpty(txtSecret.Text))
            txtSecret.Attributes.Add(@"value", txtSecret.Text);
        else
            txtSecret.Attributes.Add(@"value", string.Empty);

        if (!string.IsNullOrEmpty(txtSecretVerify.Text))
            txtSecretVerify.Attributes.Add(@"value", txtSecretVerify.Text);
        else
            txtSecretVerify.Attributes.Add(@"value", string.Empty);
    }

    #endregion
}
