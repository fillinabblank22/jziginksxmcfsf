﻿Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.QuickTips.init();

SW.NCM.InventoryStatusViewer = function () {

    var selectorModel;
    var gridStore;
    var grid;
    var pageSize;
    var pageSizeBox;
    var pagingToolbar;

    var getDateTimeOffset = function () {
        var d = new Date();
        var n = d.getTimezoneOffset();
        return n;
    }

    function gridResize() {
        if (grid != null) {
            grid.setWidth(1);
            grid.setWidth($('#gridCell').width());

            var h = getGridHeight();
            grid.setHeight(h);
        }
    }

    function getGridHeight() {
        var top = $('#gridCell').offset().top;
        return $(window).height() - top - $('#footer').height() - 100;
    }

    var refreshObjects = function () {
        grid.store.removeAll();
        var pagingToolbar = grid.getBottomToolbar();
        pagingToolbar.cursor = 0;
        grid.store.baseParams['limit'] = grid.getBottomToolbar().pageSize;
        grid.store.proxy.conn.jsonData = { clientOffset: getDateTimeOffset() };
        pagingToolbar.doLoad(pagingToolbar.cursor);
    }

    var reloadGridStore = function () {
        var currentPageRecordCount = grid.store.getCount();
        var selectedRecordCount = grid.getSelectionModel().getCount();
        if (currentPageRecordCount > selectedRecordCount) {
            grid.store.reload();
        }
        else {
            grid.store.load();
        }
    }

    var cancelInventory = function (ids, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/InventoryStatus.asmx",
                             "CancelInventory", { ids: ids },
                             function (result) {
                                 onSuccess(result);
                             });
    }

    var clearAllInventory = function (ids, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/InventoryStatus.asmx",
                             "ClearAll", { ids: ids },
                             function (result) {
                                 onSuccess(result);
                             });
    }

    var clearCompleteInventory = function (onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/InventoryStatus.asmx",
                             "ClearComplete", {},
                             function (result) {
                                 onSuccess(result);
                             });
    }

    var clearFailedInventory = function (onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/InventoryStatus.asmx",
                             "ClearFailed", {},
                             function (result) {
                                 onSuccess(result);
                             });
    }

    var reExecuteInventory = function (ids, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/InventoryStatus.asmx",
                             "ReExecuteInventory", { ids: ids },
                             function (result) {
                                 onSuccess(result);
                             });
    }

    var cancelAll = function (items) {
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_84;E=js}');
        var ids = items != null && items.length > 0 ? getActiveInventoryIds(items, true) : null;

        cancelInventory(ids, function (result) {
            waitMsg.hide();
            ErrorHandler(result);
            reloadGridStore();
        });
    }

    var clearAll = function (items) {
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_85;E=js}');
        var ids = items != null && items.length > 0 ? getCompleteInventoryIds(items, true) : null;

        clearAllInventory(ids, function (result) {
            waitMsg.hide();
            ErrorHandler(result);
            reloadGridStore();
        });
    }

    var clearComplete = function () {
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_85;E=js}');

        clearCompleteInventory(function (result) {
            waitMsg.hide();
            ErrorHandler(result);
            reloadGridStore();
        });
    }

    var clearFailed = function () {
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_85;E=js}');

        clearFailedInventory(function (result) {
            waitMsg.hide();
            ErrorHandler(result);
            reloadGridStore();
        });
    }

    var reExecute = function (items) {
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=InventoryReports_InventoryStatus_ReExecute;E=js}');
        var ids = items != null && items.length > 0 ? getCompleteNodeIds(items, true) : null;

        reExecuteInventory(ids, function (result) {
            waitMsg.hide();
            ErrorHandler(result);
            reloadGridStore();
        });
    }

    var getCompleteNodeIds = function (items) {
        var ids = [];
        Ext.each(items, function (item) {
            if (item.data.NumStatus == 0) {
                ids.push(item.data.NodeID);
            }
        });
        return ids;
    }

    var getCompleteInventoryIds = function (items) {
        var ids = [];
        Ext.each(items, function (item) {
            if (item.data.NumStatus == 0) {
                ids.push(item.data.InventoryID);
            }
        });
        return ids;
    }

    var getActiveInventoryIds = function (items) {
        var ids = [];
        Ext.each(items, function (item) {
            if (item.data.NumStatus == 1) {
                ids.push(item.data.InventoryID);
            }
        });
        return ids;
    }

    refreshInventoryStatus = function () {
        var inventoryIds = [];
        gridStore.data.each(function (record, index, length) {
            if (record.data.NumStatus == 1) {
                inventoryIds.push(record.data.InventoryID);
            }
        });

        if (inventoryIds.length > 0) {
            var store = new ORION.WebServiceStore(
                "/Orion/NCM/Services/InventoryStatus.asmx/RefreshInventoryStatus",
                [
                    { name: 'InventoryID', mapping: 0 },
                    { name: 'Status', mapping: 1 },
                    { name: 'Error', mapping: 2 },
                    { name: 'UserName', mapping: 3 },
                    { name: 'DateTime', mapping: 4 },
                    { name: 'Cancelling', mapping: 5 },
                    { name: 'Rejected', mapping: 6 },
                    { name: 'NumStatus', mapping: 7 }
                ],
                "SysName");

            store.addListener("exception", function (dataProxy, type, action, options, response, arg) {
                var error = eval("(" + response.responseText + ")");
                Ext.Msg.show({
                    title: 'Error',
                    msg: error.Message,
                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                });
            });

            store.proxy.conn.jsonData = { inventoryIds: inventoryIds, clientOffset: getDateTimeOffset() };

            store.addListener("load", function (store) {
                store.each(function (record) {
                    var value = record.data.InventoryID;
                    var recordIndex = gridStore.findExact("InventoryID", value);
                    if (recordIndex >= 0) {
                        var recordToUpdate = grid.store.getAt(recordIndex);
                        recordToUpdate.set('Status', record.data.Status);
                        recordToUpdate.set('Error', record.data.Error);
                        recordToUpdate.set('UserName', record.data.UserName);
                        recordToUpdate.set('DateTime', record.data.DateTime);
                        recordToUpdate.set('Cancelling', record.data.Cancelling);
                        recordToUpdate.set('Rejected', record.data.Rejected);
                        recordToUpdate.set('NumStatus', record.data.NumStatus);
                        recordToUpdate.commit();
                    }
                });
            });
            store.load();
        }
    }

    var autoUpdateIntervalId = 0;
    var autoUpdate = function () {
        autoUpdateIntervalId = setInterval("refreshInventoryStatus()", 5000);
    }

    var updateToolbarButtons = function () {
        var map = grid.getTopToolbar().items.map;

        var atLeastOneSelected = grid.getSelectionModel().getCount() > 0;
        var hasComplete = getCompleteInventoryIds(grid.getSelectionModel().getSelections()).length > 0;
        var hasActive = getActiveInventoryIds(grid.getSelectionModel().getSelections()).length > 0;

        if (atLeastOneSelected)
            map.CancelAll.setDisabled(!SW.NCM.IsPowerUser || !hasActive);
        else
            map.CancelAll.setDisabled(!SW.NCM.IsPowerUser);

        if (atLeastOneSelected)
            map.ClearAll.setDisabled(!SW.NCM.IsPowerUser || !hasComplete);
        else
            map.ClearAll.setDisabled(!SW.NCM.IsPowerUser);

        map.ClearComplete.setDisabled(!SW.NCM.IsPowerUser || atLeastOneSelected);
        map.ClearFailed.setDisabled(!SW.NCM.IsPowerUser || atLeastOneSelected);
        map.CancelAll.setText(atLeastOneSelected ? '@{R=NCM.Strings;K=WEBJS_VK_87;E=js}' : '@{R=NCM.Strings;K=WEBJS_VK_86;E=js}');
        map.ClearAll.setText(atLeastOneSelected ? '@{R=NCM.Strings;K=WEBJS_VK_326;E=js}' : '@{R=NCM.Strings;K=WEBJS_VK_94;E=js}');
        map.ReExecute.setDisabled(!SW.NCM.IsPowerUser || !hasComplete);
    }

    function ErrorHandler(result) {
        if (result != null && result.Error) {
            Ext.Msg.show({
                title: result.Source,
                msg: result.Msg,
                minWidth: 500,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        }
    }

    function renderNodeNameOrIPAddress(value, meta, record) {
        var nodeid = record.data.NodeID;

        if (nodeid != null && nodeid.length > 2 && nodeid.charAt(0) == '{' && nodeid.charAt(nodeid.length - 1) == '}') {
            nodeid = nodeid.substr(1, nodeid.length - 2);
        }

        return String.format('<a href="/Orion/NCM/NodeDetails.aspx?NodeID={0}">{1}</a>', nodeid, Ext.util.Format.htmlEncode(value));
    }

    function renderStatus(value, meta, record) {
        if (record.data.Cancelling) {
            return String.format(
            '<div style="text-align:left">{0}</div>', value);
        }

        if (record.data.Error || record.data.Rejected) {
            return String.format(
            '<div style="color:red;text-align:left;">{0}</div>', value);
        }
        else {
            var status = '';
            value = value.replace('Inventoring', '');
            value = value.replace('% complete', '');
            if (value == '') value = 0;
            if (value == 'Initializing') {
                status = '@{R=NCM.Strings;K=WEBJS_VK_100;E=js}';
                value = 0;
            }
            if (value == 'Waiting') {
                status = '@{R=NCM.Strings;K=WEBJS_VK_101;E=js}';
                value = 0;
            }
            if (value == 'Complete') {
                status = '@{R=NCM.Strings;K=WEBJS_VK_102;E=js}';
                value = 100;
            }

            return String.format('<div class="sw-progress"><div class="sw-progress-text">{0} {1} %</div><div class="sw-progress-bar" style="width:{1}%;"></div></div>', status, value);
        }
    }

    function renderDateTime(value, meta, record) {
        var date = eval(value.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
        return Ext.util.Format.date(date, ExtDaTimeLocalePattern.DateFull);
    }

    gridColumnChangedHandler = function (component, state) {
        if (component == null)
            return;

        var gridCM = component.getColumnModel();
        var settingName = 'Columns';

        if (gridCM != null && settingName != '') {

            var jsonvalue = SW.NCM.GridColumnsToJson(gridCM);
            ORION.Prefs.save(settingName, jsonvalue);
        }
    }

    ORION.prefix = "NCM_InventoryStatus_";

    return {
        init: function () {

            if ($("[id$='_ContentContainer']").length == 0)
                return false;

            pageSize = parseInt(ORION.Prefs.load('PageSize', '25'));

            gridStore = new ORION.WebServiceStore(
                                "/Orion/NCM/Services/InventoryStatus.asmx/GetInventoryStatusPaged",
                                [
                                    { name: 'InventoryID', mapping: 0 },
                                    { name: 'SysName', mapping: 1 },
                                    { name: 'IPAddress', mapping: 2 },
                                    { name: 'Status', mapping: 3 },
                                    { name: 'Error', mapping: 4 },
                                    { name: 'UserName', mapping: 5 },
                                    { name: 'DateTime', mapping: 6 },
                                    { name: 'NodeID', mapping: 7 },
                                    { name: 'Cancelling', mapping: 8 },
                                    { name: 'Rejected', mapping: 9 },
                                    { name: 'NumStatus', mapping: 10 }
                                ],
                                "SysName"
                                );

            pageSizeBox = new Ext.form.NumberField({
                id: 'pageSizeBox',
                enableKeyEvents: true,
                allowNegative: false,
                width: 30,
                allowBlank: false,
                minValue: 1,
                maxValue: 100,
                value: pageSize,
                listeners: {
                    scope: this,
                    'keydown': function (f, e) {
                        var k = e.getKey();
                        if (k == e.RETURN) {
                            e.stopEvent();
                            var value = parseInt(f.getValue());
                            if (!isNaN(value) && value >= 1 && value <= 100) {
                                pageSize = value;
                                var pagingToolbar = grid.getBottomToolbar();
                                if (pagingToolbar.pageSize != pageSize) { // update page size only if it is different
                                    pagingToolbar.pageSize = pageSize;
                                    ORION.Prefs.save('PageSize', pageSize);
                                    pagingToolbar.doLoad(0);
                                }
                            } else {
                                return;
                            }
                        }
                    },
                    'focus': function (field) {
                        field.el.dom.select();
                    },
                    'change': function (f) {
                        var value = parseInt(f.getValue());
                        if (!isNaN(value) && value >= 1 && value <= 100) {
                            pageSize = value;
                            var pagingToolbar = grid.getBottomToolbar();
                            if (pagingToolbar.pageSize != pageSize) { // update page size only if it is different
                                ORION.Prefs.save('PageSize', pageSize);
                                pagingToolbar.pageSize = pageSize;
                                pagingToolbar.doLoad(0);
                            }
                        } else {
                            return;
                        }
                    }
                }
            });

            pagingToolbar = new Ext.PagingToolbar({
                id: 'pagingToolbar',
                store: gridStore,
                pageSize: pageSize,
                displayInfo: true,
                displayMsg: '@{R=NCM.Strings;K=WEBJS_VK_103;E=js}',
                emptyMsg: '@{R=NCM.Strings;K=WEBJS_VK_104;E=js}',
                items: ['-', new Ext.form.Label({
                    text: '@{R=NCM.Strings;K=WEBJS_VK_300;E=js}',
                    style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;'
                }), pageSizeBox]

            });

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", updateToolbarButtons);

            grid = new Ext.grid.GridPanel({
                id: 'gridControl',
                store: gridStore,
                columns: [selectorModel, {
                    header: 'InventoryID',
                    width: 80,
                    hidden: true,
                    hideable: false,
                    sortable: true,
                    dataIndex: 'InventoryID'
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_88;E=js}',
                    width: 200,
                    sortable: true,
                    dataIndex: 'SysName',
                    renderer: renderNodeNameOrIPAddress
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_89;E=js}',
                    width: 200,
                    sortable: true,
                    dataIndex: 'IPAddress',
                    renderer: renderNodeNameOrIPAddress
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_90;E=js}',
                    width: 350,
                    sortable: true,
                    dataIndex: 'Status',
                    renderer: renderStatus
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_91;E=js}',
                    width: 200,
                    sortable: true,
                    dataIndex: 'UserName'
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_92;E=js}',
                    width: 200,
                    sortable: true,
                    dataIndex: 'DateTime',
                    renderer: renderDateTime
                }, {
                    header: 'NumStatus',
                    width: 80,
                    hidden: true,
                    hideable: false,
                    sortable: true,
                    dataIndex: 'NumStatus'
                }],

                layout: 'fit',
                autoScroll: 'true',
                loadMask: true,
                sm: selectorModel,
                width: 750,
                height: 500,
                stripeRows: true,

                tbar:
					[{
					    id: 'CancelAll',
					    text: '@{R=NCM.Strings;K=WEBJS_VK_86;E=js}',
					    icon: '/Orion/NCM/Resources/images/InventoryIcons/icon_cancel.gif',
					    cls: 'x-btn-text-icon',
					    handler: function () { cancelAll(grid.getSelectionModel().getSelections()); }
					}, '-', {
					    id: 'ClearAll',
					    text: '@{R=NCM.Strings;K=WEBJS_VK_94;E=js}',
					    icon: '/Orion/NCM/Resources/images/InventoryIcons/icon_clear_all.gif',
					    cls: 'x-btn-text-icon',
					    handler: function () { clearAll(grid.getSelectionModel().getSelections()); }
					}, '-', {
					    id: 'ClearComplete',
					    text: '@{R=NCM.Strings;K=WEBJS_VK_95;E=js}',
					    icon: '/Orion/NCM/Resources/images/InventoryIcons/icon_clear_complete.gif',
					    cls: 'x-btn-text-icon',
					    handler: function () { clearComplete(); }
					}, '-', {
					    id: 'ClearFailed',
					    text: '@{R=NCM.Strings;K=WEBJS_VK_96;E=js}',
					    icon: '/Orion/NCM/Resources/images/InventoryIcons/icon_clear_failed.gif',
					    cls: 'x-btn-text-icon',
					    handler: function () { clearFailed(); }
					}, '-', {
					    id: 'ReExecute',
					    text: '@{R=NCM.Strings;K=WEBJS_VK_327;E=js}',
					    icon: '/Orion/NCM/Resources/images/PolicyIcons/update_all.gif',
					    cls: 'x-btn-text-icon',
					    handler: function () { reExecute(grid.getSelectionModel().getSelections()); }
					}],
                bbar: pagingToolbar
            });

            SW.NCM.PersonalizeGrid(grid, columnModel);
            grid.on("statesave", gridColumnChangedHandler);
            grid.render('Grid');

            updateToolbarButtons();

            gridResize();
            refreshObjects();

            $("form").submit(function () { return false; });

            $(window).resize(function () {
                gridResize();
            });

            autoUpdate();
        }
    };

} ();

Ext.onReady(SW.NCM.InventoryStatusViewer.init, SW.NCM.InventoryStatusViewer);