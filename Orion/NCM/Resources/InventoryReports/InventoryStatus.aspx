<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="InventoryStatus.aspx.cs" Inherits="InventoryStatus" %>

<%@ Register Src="~/Orion/NCM/CustomPageLinkHidding.ascx" TagPrefix="ncm" TagName="custPageLinkHidding" %>
<%@ Register TagPrefix="ncm1" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="ncm" Src="~/Orion/NCM/NavigationTabBar.ascx" TagName="NavigationTabBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmHelpIconPlaceHolder" runat="Server">
    <orion:IconHelpButton ID="helpBtn" runat="server" HelpUrlFragment="OrionNCMWebInventoryStatus" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmPageTitlePlaceHolder" runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmHeadPlaceHolder" Runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <ncm:custPageLinkHidding ID="CustPageLinkHidding1"  runat="server" />
    <ncm1:ConfigSnippetUISettings ID="ReportUISetting" runat="server" Name="NCM_InventoryStatus_Columns" DefaultValue="[]" RenderTo="columnModel" />
    <ncm1:ExtLocalDateTimePattern ID="ExtLocalDateTimePattern1" runat="server"/>
    
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/PolicyReports/PolicyReport.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/Progress.css" />
    
    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script type="text/javascript" src="/Orion/NCM/JavaScript/main.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippetsHelper.js"></script>
    <script type="text/javascript" src="InventoryStatusViewer.js"></script>
    
    <script type="text/javascript">
        SW.NCM.IsPowerUser = <%=IsPermissionExist.ToString().ToLowerInvariant() %>;
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
    <input type="hidden" name="NCM_InventoryStatus_PageSize" id="NCM_InventoryStatus_PageSize" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get(@"NCM_InventoryStatus_PageSize")%>' />

    <asp:Panel id="ContentContainer" runat="server">
        <div style="padding:10px;">
            <ncm:NavigationTabBar runat="server" />
            <div id="tabPanel" class="tab-top">
                <table class="ExistingElements" cellpadding="0" cellspacing="0" width="100%">
                    <tr valign="top" align="left">                    
                        <td id="gridCell">
                            <div id="Grid" />
                        </td>
                    </tr>
                </table>
                <div id="originalQuery">
                </div>
                <div id="test">
                </div>
                <pre id="stackTrace"></pre>
            </div>
        </div>
    </asp:Panel>
    <div style="padding:10px;" id="ErrorContainer" runat="server"/>
</asp:Content>

