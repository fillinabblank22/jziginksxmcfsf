using System;
using System.Web.UI;
using SolarWinds.NCMModule.Web.Resources;

public partial class InventoryStatus : Page 
{
    private readonly ICommonHelper commonHelper;

    public InventoryStatus()
    {
        commonHelper = ServiceLocator.Container.Resolve<ICommonHelper>();
    }

    protected override void OnInit(EventArgs e)
    {
        commonHelper.Initialise();

        var validator = new SWISValidator
        {
            ContentContainer = ContentContainer,
            RedirectIfValidationFailed = true
        };
        ErrorContainer.Controls.Add(validator);

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_VK_111;
    }

    protected bool IsPermissionExist
    {
        get
        {
            try
            {
                return SecurityHelper.IsPermissionExist(SecurityHelper._canDownload);
            }
            catch { }

            return false;
        }
    }
}
