﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="Review.aspx.cs" Inherits="Orion_NCM_Resources_Jobs_Review" %>

<%@ Register Src="~/Orion/NCM/Resources/Jobs/Controls/JobWorkflowContainer.ascx" TagName="JobWorkflowContainer" TagPrefix="ncm" %>
<%@ Register Src="~/Orion/NCM/Resources/Jobs/Controls/Review.ascx" TagName="Review" TagPrefix="ncm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmHeadPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" Runat="Server">
<%=Page.Title%>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmExportToPDFPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ncmCustomizePagePlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="ncmHelpIconPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
    <div style="width:80%;padding:10px;">
        <ncm:JobWorkflowContainer ID="jobWorkflowContainer" runat="server"         
            OnNextClick="Next_Click">
            <Content>
                 <div style="padding-top:30px;border-bottom:#e6e6dd 1px solid;padding-bottom:5px;"><%=Resources.NCMWebContent.WEBDATA_IC_152%></div>
                 <ncm:Review ID="ctlReview" runat="server" />
                 <div style="padding-top:10px">
                     <asp:Label style="color:Red" ID="lblError" Visible="false" runat="server" />
                 </div>
            </Content>
        </ncm:JobWorkflowContainer>
    </div>
</asp:Content>

