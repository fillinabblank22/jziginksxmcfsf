﻿using System;
using SolarWinds.NCMModule.Web.Resources.JobManagement;

public partial class Orion_NCM_Resources_Jobs_ConfigChangeReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = jobWorkflowContainer.JobPageTitle;
        if (!Page.IsPostBack)
        {
            ConfigChanegReportCtl.LoadJob(JobWorkflowController.Job);
        }
    }

    
    protected void Next_Click(object sender, NCMJobEventArgs e)
    {
       e.ValidationPass= ConfigChanegReportCtl.UpdateJob(JobWorkflowController.Job);       
    }

}