﻿using System;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources.JobManagement;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Logging;

public partial class Orion_NCM_Resources_Jobs_Default : System.Web.UI.Page
{
    private static Log _log = new Log();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        var redirectUrl = string.Empty;
        try
        {
            if (string.IsNullOrEmpty(Page.Request.QueryString[@"JobID"]))
            {
                //create job
                var jobType = eJobType.DownloadConfigs;
                if (!string.IsNullOrEmpty(Page.Request.QueryString[@"JobType"]))
                    jobType = (eJobType)Enum.Parse(typeof(eJobType), Page.Request.QueryString[@"JobType"]);

                JobWorkflowController.Job = JobHelper.CreateDefaultNCMJob(jobType);
                redirectUrl = JobWorkflowController.GetNextJobStepUrl(true, jobType, -1);
            }
            else
            {
                var jobID = Convert.ToInt32(Page.Request.QueryString[@"JobID"]);
                var newJob = !string.IsNullOrEmpty(Page.Request.QueryString[@"DuplicateJob"]);

                var isLayer = new ISWrapper();
                using (var proxy = isLayer.Proxy)
                {
                    JobWorkflowController.Job = proxy.Cirrus.Jobs.GetJob(jobID);
                }
                if (newJob)
                {
                    //dupilcate job
                    JobWorkflowController.Job.JobName =
                        $@"{JobWorkflowController.Job.JobName} {Resources.NCMWebContent.WEBDATA_IC_202}";
                    JobWorkflowController.Job.JobDefinition.Name =
                        $@"{JobWorkflowController.Job.JobName} {Resources.NCMWebContent.WEBDATA_IC_202}";
                }

                redirectUrl = JobWorkflowController.GetNextJobStepUrl(newJob, JobWorkflowController.Job.JobType, -1);
            }
        }
        catch (Exception ex)
        {
            _log.Error("Add/Edit NCM Job Error", ex);
            lblError.Visible = true;
            lblError.Text = ex.Message;
        }
        if (!string.IsNullOrEmpty (redirectUrl)) Page.Response.Redirect (redirectUrl);

    }
}