﻿using System;
using SolarWinds.NCMModule.Web.Resources.JobManagement;

public partial class Orion_NCM_Resources_Jobs_Schedule : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
      
    }

    protected void Next_Click(object sender, NCMJobEventArgs e)
    {
        JobWorkflowController.Job.JobName = txtJobName.Text;
        JobWorkflowController.Job.JobDefinition.Comments = txtJobComments.Text;
        e.ValidationPass = jobSchedule.UpdateJob(JobWorkflowController.Job);
    }

    protected override void OnInit(EventArgs e)
    {
        if (JobWorkflowController.Job == null)
            throw new Exception(Resources.NCMWebContent.ViewJob_UnableToInitializeJob);

        Page.Title = jobWorkflowContainer.JobPageTitle;
        txtJobName.Text = JobWorkflowController.Job.JobName;
        txtJobComments.Text = JobWorkflowController.Job.JobDefinition.Comments;

        jobSchedule.LoadJob(JobWorkflowController.Job);

        base.OnInit(e);
    }
}