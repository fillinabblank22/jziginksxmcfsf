﻿<%@ Page  Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="ViewJob.aspx.cs" Inherits="Orion_NCM_Resources_Jobs_Schedule" %>

<%@ Register Src="~/Orion/NCM/Resources/Jobs/Controls/JobWorkflowContainer.ascx" TagName="JobWorkflowContainer" TagPrefix="ncm" %>
<%@ Register Src="~/Orion/NCM/Resources/Jobs/Controls/JobAdvancedSchedule.ascx" TagName="JobSchedule" TagPrefix="ncm"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmHeadPlaceHolder" Runat="Server">
    <style type="text/css">
        .ncm-resizable-textbox {
            resize: both;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmExportToPDFPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ncmCustomizePagePlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="ncmHelpIconPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
    <div style="width:80%;padding:10px;">
        <ncm:JobWorkflowContainer ID="jobWorkflowContainer" runat="server" 
            OnNextClick="Next_Click" >           
            <Content>
                <div style="padding-top:30px;font-size:11pt;font-weight:bold;border-bottom:#e6e6dd 1px solid;padding-bottom:5px;"><%=Resources.NCMWebContent.WEBDATA_VM_209%></div>
                      <div style="padding-top:20px"> 
                        <div class="LabelFont" style="padding-bottom:10px;"><%=Resources.NCMWebContent.WEBDATA_VM_192%></div>
                        <asp:TextBox ID="txtJobName" CssClass="TextField" Width="500px" runat="server" />
                        <asp:RequiredFieldValidator id="RequiredFieldValidator2"
                            ControlToValidate="txtJobName"
                            Display="Static"
                            ErrorMessage="*"
                            runat="server"/> 
                      </div>
                     <div style="padding-top:20px"> 
                        <ncm:JobSchedule ID="jobSchedule" runat="server" />
                     </div>
                     <div style="padding-top:70px"> 
                        <div class="LabelFont" style="padding-bottom:10px;padding-top:10px;"><%=Resources.NCMWebContent.WEBDATA_VK_340%></div>
                        <asp:TextBox ID="txtJobComments" runat="server" CssClass="TextField ncm-resizable-textbox" TextMode="MultiLine" Width="700px" Height="50px"/>
                     </div>
            </Content>
        </ncm:JobWorkflowContainer>
    </div>
</asp:Content>

