﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PurgeConfigs.ascx.cs" Inherits="Orion_NCM_Resources_Jobs_Controls_PurgeConfigs" %>

<%@ Register Assembly="Infragistics2.WebUI.WebDataInput.v8.2, Version=8.2.20082.1000, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.WebDataInput" TagPrefix="igtxt" %><%@ Register Assembly="Infragistics2.WebUI.WebDataInput.v8.2, Version=8.2.20082.1000, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.WebDataInput" TagPrefix="igtxt" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources" Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
<link rel="stylesheet" type="text/css" href="/Orion/NCM/Resources/Jobs/Jobs.css" />

<style type="text/css">   
    .ajax__calendar_days table thead tr td
    {
        border: 0px;
        background-color: white;	
        text-transform: none;
    }
    
    .TimeComboBox table
    {
    	top: 0px !important;
    }
    
    .TimeComboBox input 
    {
    	font-size:9pt;
        font-family: Arial,Verdana,Helvetica,sans-serif;
	    border: 1px solid #999999;
	    height:18px;
	}
	
	.Active:hover
	{
		cursor:pointer;
	}
</style>


<script type="text/javascript">
    function CkeckPurgeConfigsType(id) {
        $("[id$='_" + id + "']").click();
    }
</script>

<div>
    <div class="JobHeader"><%=Resources.NCMWebContent.WEBDATA_VK_772 %></div>
    <div style="padding-top:10px;">
        <asp:RadioButton ID="rbPurgeConfigsByDate" runat="server" GroupName="PurgeConfigsGroup" />        
        <span class="Active" onclick="CkeckPurgeConfigsType('rbPurgeConfigsByDate');"><%=string.Format(Resources.NCMWebContent.WEBDATA_VK_744, @"<b>", @"</b>") %></span>
        <div style="padding-top:10px;padding-left:25px;">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td valign="top"><ncm:Calendar ID="txtDateTime" runat="server" CalendarTextBoxCssClass="TextField" AlternativeDrawingMode="false" DisplayTime="false" /></td>
                    <td style="padding-left:40px;" valign="top">
                        <asp:CheckBox ID="chkIncludeTime" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_745 %>" />
                    </td>
                    <td style="padding-left:5px;" valign="top">
                        <ajax:ComboBox ID="ddlTime" runat="server" CssClass="TimeComboBox"
                            AutoPostBack="False"
                            DropDownStyle="DropDownList"
                            AutoCompleteMode="SuggestAppend"
                            CaseSensitive="False"
                            ItemInsertLocation="Append">
                        </ajax:ComboBox>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    
    <div style="padding-top:50px;">
        <asp:RadioButton ID="rbPurgeConfigsByCount" runat="server" GroupName="PurgeConfigsGroup" />
        <span class="Active" onclick="CkeckPurgeConfigsType('rbPurgeConfigsByCount');"><%=string.Format(Resources.NCMWebContent.WEBDATA_VK_746, @"<b>", @"</b>") %> <igtxt:WebNumericEdit ID="txtCount" runat="server" CssClass="TextField" HorizontalAlign="Center" Width="30" MaxValue="999" MinValue="1" MaxLength="3" Nullable="false"></igtxt:WebNumericEdit> <%=Resources.NCMWebContent.WEBDATA_VK_747 %></span>
        <div style="font-size:8pt;padding-top:5px;padding-left:25px;">
            &#0187;&nbsp;<a style="color:#336699;" href="<%=HelpLink %>" target="_blank"><%=Resources.NCMWebContent.WEBDATA_VK_748 %></a>
        </div>
    </div>

    <div style="padding-top:50px;">
        <asp:RadioButton ID="rbPurgeConfigsByTimeRange" runat="server" GroupName="PurgeConfigsGroup" />
        <span class="Active" onclick="CkeckPurgeConfigsType('rbPurgeConfigsByTimeRange');"><%=string.Format(Resources.NCMWebContent.WEBDATA_VK_749, @"<b>", @"</b>") %></span>
        <div style="padding-top:10px;padding-left:25px;">
            <asp:DropDownList ID="ddlTimeRange" runat="server" CssClass="SelectField"></asp:DropDownList>
        </div>
    </div>

    <div style="padding-top:50px;">
        <asp:CheckBox ID="chkPurgeBaselineConfigs" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_750 %>" />
    </div>
</div>
