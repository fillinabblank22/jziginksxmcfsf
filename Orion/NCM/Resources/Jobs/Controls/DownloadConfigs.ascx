﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DownloadConfigs.ascx.cs" Inherits="Orion_NCM_Admin_Jobs_Controls_DownloadConfigs" %>

<%@ Register Src="~/Orion/NCM/Resources/Jobs/Controls/SelectedConfigs.ascx" TagName="SelectedConfigs" TagPrefix="ncm" %>

<link rel="stylesheet" type="text/css" href="/Orion/NCM/Resources/Jobs/Jobs.css" />

<script type="text/javascript">
    $(document).ready(function () {
        ConfigTypeCheckBoxClicked();

        $('span.SelectedConfigType input[type=checkbox]').click(function () {
            if ($('span.SelectedConfigType input:checked').length > 1 && $('span.CfgType input:checked').length > 0) {
                $("#warningNotification").show();
            }
            else {
                $("#warningNotification").hide();
            }
        });
    });

    function ConfigTypeCheckBoxClicked() 
    {
        if ($('span.CfgType input:checked').length == 0) {
            EnableOtherOptionsContent(false);
            $("#warningNotification").hide();
        }
        else {
            EnableOtherOptionsContent(true);

            if ($('span.SelectedConfigType input:checked').length > 1) {
                $("#warningNotification").show();
            }
            else {
                $("#warningNotification").hide();
            }
        }
    }
    
    function EnableOtherOptionsContent(val) 
    {
        $('span.OtherOption input').each(function (i) {
            this.disabled = !val;
            if (!val && this.type == "checkbox")
                this.checked = false;
        });
    }
</script>

<table cellpadding="0" cellspacing="0">
    <tr>
        <td style="padding-top:30px;padding-bottom:10px;" valign="bottom">
            <%=Resources.NCMWebContent.WEBDATA_IC_122%>
        </td>
        <td style="padding-bottom:5px;padding-left:5px;" valign="bottom">
            <span id="warningNotification" class="Warning"><span class="Warning-Icon"></span>
                <%=Resources.NCMWebContent.WEBDATA_VK_900%>
            </span>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <ncm:SelectedConfigs ID="configs" runat="server" />
            <div class="Hint JobConfigTypesHint"><%=Resources.NCMWebContent.WEBDATA_IC_123%></div>
            <asp:Label style="color:Red" ID="lblError" Visible="false" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_IC_154%>" />
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding-top:10px">
            <asp:CheckBox ID="chkSaveChanged" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_IC_131%>" />
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding-top:50px">
            <div style="font-weight:bold;padding-bottom:5px;"><%=Resources.NCMWebContent.WEBDATA_VK_901%></div>
            <%=Resources.NCMWebContent.WEBDATA_IC_124%>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding-top:10px">
            <asp:CheckBox ID="chkLastDownloaded" onclick="ConfigTypeCheckBoxClicked();"  class="CfgType"  runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_IC_125%>" />
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding-top:10px">
            <asp:CheckBox ID="chkBaseline" onclick="ConfigTypeCheckBoxClicked();"  class="CfgType"  runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_IC_126%>" />
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding-top:10px">
            <asp:CheckBox ID="chkStartup"  onclick="ConfigTypeCheckBoxClicked();" class="CfgType" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_IC_127%>" />
        </td>
    </tr>
    <tr id="trEmailResultsDisabled" runat="server" visible="false">
        <td colspan="2" style="color:Red;padding-top:5px;">
            <%=Resources.NCMWebContent.WEBDATA_VK_819%>
        </td>
    </tr>
</table>

<table cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="3" style="padding-bottom:10px;padding-top:10px;">                
            <span class="Hint" style="font-style:italic;"><%=Resources.NCMWebContent.WEBDATA_IC_128%></span>
        </td>
    </tr>
    <tr>
        <td>
            <asp:CheckBox ID="chkSendNotificationDetails" CssClass="OtherOption"  runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_IC_132%>" />
        </td>
        <td style="padding-left:10px">
            <asp:RadioButton ID="rbHTMLEmail" CssClass="OtherOption" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_IC_129%>"  GroupName= "details" />
        </td>
        <td style="padding-left:10px">
            <asp:RadioButton ID="rbTextEmail" CssClass="OtherOption" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_IC_130%>" GroupName= "details" />
        </td>
    </tr>
</table>