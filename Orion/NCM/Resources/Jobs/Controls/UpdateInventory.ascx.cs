﻿using System;
using SolarWinds.NCM.Contracts.InformationService;

public partial class Orion_NCM_Controls_Jobs_JobInventorySetting : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void LoadJob(NCMJob job)
    {
        if (job != null)
        {
            ncmInventorySettings.LoadInventorySpecs(job.JobDefinition.Parameters.InventorySettings);
        }
    }

    public bool UpdateJob(NCMJob job)
    {
        var specs = ncmInventorySettings.GetSelectedInventorySpecs();
        if (string.IsNullOrEmpty(specs))
        {
            ShowError(Resources.NCMWebContent.WEBDATA_VM_210);
            return false; //invalidate control
        }
        if (job != null)
        {
            job.JobDefinition.Parameters.InventorySettings = specs;
            return true;
        }
        return false;
    }

    private void ShowError(string error)
    {
        lblError.Text = error;
        errorContent.Style[@"display"] = @"block";
    }
}