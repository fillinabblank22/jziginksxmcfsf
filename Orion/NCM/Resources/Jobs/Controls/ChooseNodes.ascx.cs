﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.NCM.Common.Help;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;

public partial class Orion_NCM_Resources_Jobs_Controls_ChooseNodes : System.Web.UI.UserControl
{
    protected string MoreInformationLink
    {
        get
        {
            var helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
            return helpHelperWrap.GetHelpUrl("OrionNCMPHNewJobSchedulingTransition");
        }
    }

    protected bool NeedShowNodeSelectionWarningDialog
    {
        get { return Convert.ToBoolean(ViewState["ChooseNodes_ShowNodeSelectionWarning"]); }
        set { ViewState["ChooseNodes_ShowNodeSelectionWarning"] = value; }
    }

    protected List<WebSelectionCriteria> WebSelectionCriteriaList
    {
        get
        {
            if (Session[@"ChooseNodes_WebSelectionCriteriaList"] != null)
                return (List<WebSelectionCriteria>)Session[@"ChooseNodes_WebSelectionCriteriaList"];
            else
            {
                List<WebSelectionCriteria> list = new List<WebSelectionCriteria>();
                if (list.Count == 0) list.Add(new WebSelectionCriteria());
                Session[@"ChooseNodes_WebSelectionCriteriaList"] = list;

                return list;
            }
        }
        set
        {
            List<WebSelectionCriteria> list = value;

            if (list == null) list = new List<WebSelectionCriteria>();
            if (list.Count == 0) list.Add(new WebSelectionCriteria());

            Session[@"ChooseNodes_WebSelectionCriteriaList"] = list;
            DynamicSelection.WebSelectionCriteria = list;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        CheckAccountRights();

        rbDynamicSelection.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_795, @"<span class='Hint'>", @"</span>");
        rbDesktopSelection.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_796, @"<span class='Hint' style='font-style:italic;'>", @"</span>");
        
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        DynamicSelection.WebSelectionCriteria = WebSelectionCriteriaList;
    }

    public void LoadJob(NCMJob job)
    {
        ClearControlState();

        SelectedNodes nodes = job.JobDefinition.Nodes;

        if (nodes.SelectedCriteriaType != eCriteriaType.DesktopCriteria)
            HideDesktopSelection();

        divDynamicSelection.Style.Add(HtmlTextWriterStyle.PaddingBottom, nodes.SelectedCriteriaType == eCriteriaType.DesktopCriteria ? @"50px" : @"100px");

        if (nodes.SelectedCriteriaType == eCriteriaType.AllNodes)
            rbAllNodes.Checked = true;
        else if (nodes.SelectedCriteriaType == eCriteriaType.SelectedNodes)
        {
            rbSelectNodes.Checked = true;
            List<Guid> nodeIDList = ConvertNetworkNodeListToListOfGuids(nodes.NetworkNodes);
            List<Guid> limitationNodeIDList = SecurityHelper.GetLimitationNodeIDList();
            AddRemoveNodesControl.NodeIDList = nodeIDList.Intersect(limitationNodeIDList).ToList();
        }
        else if (nodes.SelectedCriteriaType == eCriteriaType.WebCriteria)
        {
            rbDynamicSelection.Checked = true;
            WebSelectionCriteriaList = nodes.WebNodesSelectionCriterias;
        }
        else
        {
            rbDesktopSelection.Checked = true;
            DesktopSelection.Text = nodes.QueryString;
        }

        NeedShowNodeSelectionWarningDialog = rbDesktopSelection.Checked;

        NodeSelectionChanged(null, EventArgs.Empty);
    }

    public bool UpdateJob(NCMJob job)
    {
        SelectedNodes nodes = job.JobDefinition.Nodes;
        
        if (rbAllNodes.Checked)
        {
            nodes.SelectedCriteriaType = eCriteriaType.AllNodes;            
        }
        else if (rbSelectNodes.Checked)
        {
            if (AddRemoveNodesControl.NodeIDList.Count == 0)
            {
                divAddRemoveNodesValidator.Visible = true;
                return false;
            }

            nodes.SelectedCriteriaType = eCriteriaType.SelectedNodes;
            nodes.NetworkNodes = ConvertListOfGuidsToNetworkNodeList(AddRemoveNodesControl.NodeIDList);
        }
        else if (rbDynamicSelection.Checked)
        {
            nodes.SelectedCriteriaType = eCriteriaType.WebCriteria;
            nodes.WebNodesSelectionCriterias = DynamicSelection.WebSelectionCriteria;
        }
        else
        {
            nodes.SelectedCriteriaType = eCriteriaType.DesktopCriteria;
        }

        nodes.Pack();

        return true;
    }

    protected void NodeSelectionChanged(object sender, EventArgs e)
    {
        if (rbAllNodes.Checked)
        {
            divSelectNodes.Visible = false;
            divDynamicSelection.Visible = false;
            divDesktopSelectionQuery.Visible = false;
            ShowNodeSelectionWarningDialog();
        }
        else if (rbSelectNodes.Checked)
        {
            divSelectNodes.Visible = true;
            divDynamicSelection.Visible = false;
            divDesktopSelectionQuery.Visible = false;
            ShowNodeSelectionWarningDialog();
        }
        else if (rbDynamicSelection.Checked)
        {
            divSelectNodes.Visible = false;
            divDynamicSelection.Visible = true;
            divDesktopSelectionQuery.Visible = false;
            ShowNodeSelectionWarningDialog();
        }
        else
        {
            divSelectNodes.Visible = false;
            divDynamicSelection.Visible = false;
            divDesktopSelectionQuery.Visible = true;
        }
    }

    private List<NetworkNode> ConvertListOfGuidsToNetworkNodeList(List<Guid> nodeIDList)
    {
        List<NetworkNode> networkNodeList = new List<NetworkNode>();
        foreach (Guid nodeID in nodeIDList)
        {
            NetworkNode networkNode = new NetworkNode();
            networkNode.NodeID = nodeID;
            networkNode.NodeCaption = string.Empty;
            networkNodeList.Add(networkNode);
        }
        return networkNodeList;
    }

    private List<Guid> ConvertNetworkNodeListToListOfGuids(List<NetworkNode> networkNodeList)
    {
        if (networkNodeList == null) 
            return new List<Guid>();

        List<Guid> nodeIDList = new List<Guid>();
        foreach (NetworkNode networkNode in networkNodeList)
        {
            nodeIDList.Add(networkNode.NodeID);
        }
        return nodeIDList;
    }

    private void ShowNodeSelectionWarningDialog()
    {
        if (NeedShowNodeSelectionWarningDialog)
        {
            NodeSelectionWarningDialog.Show();
        }
    }

    protected void ChangeNodeSelectionClick(object sender, EventArgs e)
    {
        NeedShowNodeSelectionWarningDialog = false;
        NodeSelectionWarningDialog.Hide();
    }

    protected void CancelNodeSelectionClick(object sender, EventArgs e)
    {
        NodeSelectionWarningDialog.Hide();

        rbAllNodes.Checked = false;
        rbSelectNodes.Checked = false;
        rbDynamicSelection.Checked = false;
        rbDesktopSelection.Checked = true;
        
        NodeSelectionChanged(null, EventArgs.Empty);
        
        ContentUpdatePanel.Update();
    }

    private void HideDesktopSelection()
    {
        divDesktopSelectionLabel.Visible = false;
        divDesktopSelectionQuery.Visible = false;
    }

    private void ClearControlState()
    {
        AddRemoveNodesControl.NodeIDList = null;
        Session[@"ChooseNodes_WebSelectionCriteriaList"] = null;
    }

    private void CheckAccountRights()
    {
        if (!SolarWinds.NCMModule.Web.Resources.CommonHelper.IsDemoMode())
        {
            ControlPanel.Enabled = (SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR) || SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ENGINEER));
        }
    }
}