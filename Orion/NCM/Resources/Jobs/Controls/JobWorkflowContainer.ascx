﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="JobWorkflowContainer.ascx.cs" Inherits="Orion_NCM_Resources_Jobs_Controls_JobWorkflowContainer" %>
<orion:Include runat="server" File="styles/ProgressIndicator.css" />
<div>
    <div id="wizard_header">
        <asp:Literal ID="litWizardHeader" runat="server"></asp:Literal>
    </div>
    <div id="wizard_container">
        <asp:PlaceHolder runat="server" ID="Container" />
    </div>

    <div id="wizard_buttons" style="padding-top:10px;">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td style="text-align:right;">
                    <span style="padding-right:5px;">
                        <orion:LocalizableButton runat="server" ID="btnBack" LocalizedText="Back" DisplayType="Secondary" CausesValidation="false" OnClick="btnBack_Click" />
                    </span>
                    <span style="padding-right:5px;">
                        <orion:LocalizableButton runat="server" ID="btnNext" LocalizedText="Next" DisplayType="Primary" CausesValidation="true" OnClick="btnNext_Click" />
                    </span>
                    <span style="padding-right:5px;">
                        <orion:LocalizableButton runat="server" ID="btnFinish" LocalizedText="Finish" DisplayType="Primary" CausesValidation="true" OnClick="btnFinish_Click" Visible="true" />
                    </span>
                    <span style="padding-right:5px;padding-left:20px;">
                        <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" OnClick="btnCancel_Click" />
                    </span>
                </td>
            </tr>
        </table>
    </div>
</div>