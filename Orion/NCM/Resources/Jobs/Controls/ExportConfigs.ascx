﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExportConfigs.ascx.cs" Inherits="Orion_NCM_Resources_Jobs_Controls_ExportConfigs" %>

<%@ Register Src="~/Orion/NCM/Resources/Jobs/Controls/SelectedConfigs.ascx" TagName="SelectedConfigs" TagPrefix="ncm" %>

<link rel="stylesheet" type="text/css" href="/Orion/NCM/Resources/Jobs/Jobs.css" />

<div>
    <div class="JobHeader"><%=Resources.NCMWebContent.WEBDATA_VK_774 %></div>
    <div style="padding-top:10px;padding-bottom:20px;">
        <%=Resources.NCMWebContent.WEBDATA_VK_775 %>
    </div>

    <div>
        <asp:Repeater runat="server" ID="repeater" OnItemDataBound="repeater_ItemDataBound" OnItemCommand="repeater_ItemCommand">
            <HeaderTemplate>                            
                <table border="0" cellpadding="0" cellspacing="0">
            </HeaderTemplate>
            <ItemTemplate>
                    <tr>
                        <td id="tdDelimiter" runat="server">&nbsp;</td>
                    </tr>
                    <tr id="trException" runat="server" visible="false">
                        <td style="padding-top:10px;">
                            <span class="Exception">
                                <span class="Exception-Icon"></span><asp:Literal ID="litException" runat="server" />
                            </span>
                        </td>
                    </tr>
                    <asp:Literal ID="litServerName" runat="server"></asp:Literal>
                    <tr>
                        <td>
                            <asp:HiddenField ID="txtEngineID" runat="server"></asp:HiddenField>
                            <div style="padding-bottom:5px;"><%=Resources.NCMWebContent.WEBDATA_VK_776 %></div>
                            <asp:TextBox ID="txtPath" runat="server" TextMode="SingleLine" CssClass="TextField" Width="500px" />
                            <orion:LocalizableButton runat="server" ID="btnValidate" LocalizedText="CustomText" DisplayType="Small" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_590 %>" CausesValidation="false" CommandName="Validate"/>
                            <div id="divValidator" runat="server"></div>
                        </td>
                    </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>

    <div style="padding-top:50px;">
        <div style="padding-bottom:5px;"><%=Resources.NCMWebContent.WEBDATA_VK_777 %></div>
        <asp:TextBox ID="txtConfigTemplate" runat="server" TextMode="SingleLine" CssClass="TextField" Width="500px" />
    </div>
    
    <div style="padding-top:50px;">
        <div><%=Resources.NCMWebContent.WEBDATA_VK_778 %></div>
        <div style="padding-top:10px;">
            <ncm:SelectedConfigs ID="SelectedConfigs" runat="server" />
            <div class="Hint JobConfigTypesHint"><%=Resources.NCMWebContent.WEBDATA_IC_123%></div>
            <div id="divConfigTypesEmpty" runat="server" style="color:Red;" visible="false"><%=Resources.NCMWebContent.WEBDATA_VK_779 %></div>
        </div>
    </div>

    <div style="padding-top:50px;">
        <div>
            <asp:RadioButton ID="rbExportAllConfigs" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_780 %>" GroupName="ExportConfigsGroup" />
        </div>
        <div style="padding-top:20px;">
            <asp:RadioButton ID="rbExportLatestConfig" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_781 %>" GroupName="ExportConfigsGroup" />
        </div>
    </div>
</div>
