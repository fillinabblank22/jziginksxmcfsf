﻿using SolarWinds.NCM.Common.Help;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_NCM_Resources_Jobs_Controls_ExecuteScript : System.Web.UI.UserControl
{
    protected string MoreInformationLink
    {
        get
        {
            var helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
            return helpHelperWrap.GetHelpUrl("OrionNCMPHLearnMorePatternsCreateJob");
        }
    }

    public void LoadJob(NCMJob job)
    {
        if (job != null)
        {
            cbShowCmdOutput.Checked = job.JobDefinition.Parameters.ExecuteShowCommands;
            cbFilterRes.Checked = job.JobDefinition.Parameters.ExecuteFilterResults;
            txtFilterPattern.Text = job.JobDefinition.Parameters.ScriptFilterPattern;
            loadScriptCtrl.ScriptText = job.JobDefinition.Parameters.Script;
        }
    }

    public bool UpdateJob(NCMJob job)
    {
        if (job != null)
        {
            job.JobDefinition.Parameters.ScriptFilterPattern = txtFilterPattern.Text;
            job.JobDefinition.Parameters.ExecuteFilterResults = cbFilterRes.Checked;
            job.JobDefinition.Parameters.ExecuteShowCommands = cbShowCmdOutput.Checked;
            job.JobDefinition.Parameters.Script = loadScriptCtrl.ScriptText;
        }
        return true;
    }
}