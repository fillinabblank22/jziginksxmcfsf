﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.NCM.Common.Help;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_Controls_Jobs_RoutineDatabaseMaintenance : System.Web.UI.UserControl
{
    protected string HelpLink
    {
        get
        {
            var helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
            return helpHelperWrap.GetHelpUrl("OrionNCMPHHelpChooseCachePurge");
        }
    }

    private static Dictionary<string,string> RangeTimes = new Dictionary<string,string>() 
    { 
        {Resources.NCMWebContent.WEBDATA_VK_751,@"Last 7 Days"},
        {Resources.NCMWebContent.WEBDATA_VK_752,@"Last Two Weeks"},
        {Resources.NCMWebContent.WEBDATA_VK_753,@"Last 30 Days"},
        {Resources.NCMWebContent.WEBDATA_VK_754,@"Last 3 Months"},
        {Resources.NCMWebContent.WEBDATA_VK_755,@"Last 6 Months"},
        {Resources.NCMWebContent.WEBDATA_VK_756,@"Last 12 Months"}
    };

    private static Dictionary<string, string> Size = new Dictionary<string, string>() 
    { 
        {Resources.NCMWebContent.JobLogSize_1MB,@"1048576"},
        {Resources.NCMWebContent.JobLogSize_2MB,@"2097152"},
        {Resources.NCMWebContent.JobLogSize_5MB,@"5242880"},
        {Resources.NCMWebContent.JobLogSize_10MB,@"10485760"},
        {Resources.NCMWebContent.JobLogSize_50MB,@"52428800"}
    };

    protected override void OnInit(EventArgs e)
    {
        InternalInit();
        base.OnInit(e);
    }

    public void LoadJob(NCMJob job)
    {
        if (job != null)
        {
            SetDropDownSelectedValue(ddlCfgArchiveTimeRange, job.JobDefinition.Parameters.PurgeConfigArchiveTimeRange);
            SetDropDownSelectedValue(ddlCfgCacheTimeRange, job.JobDefinition.Parameters.PurgeConfigCacheTimeRange);
            SetDropDownSelectedValue(ddlApprovalRequestsTimeRange, job.JobDefinition.Parameters.PurgeApprovalRequestsTimeRange);
            SetDropDownSelectedValue(ddlTransferRequestsTimeRange,job.JobDefinition.Parameters.PurgeTransferRequestsTimeRange);
            
            cbCfgArchive.Checked = job.JobDefinition.Parameters.PurgeConfigArchive;
            cbCfgCache.Checked = job.JobDefinition.Parameters.PurgeConfigCache;
            cbApprovalRequests.Checked = job.JobDefinition.Parameters.PurgeApprovalRequests;
            cbTransferRequests.Checked = job.JobDefinition.Parameters.PurgeTransferRequests;

            if (job.JobDefinition.Parameters.PurgeJobLog)
            {
                cbJobLog.Checked = job.JobDefinition.Parameters.PurgeJobLog;
                if (job.JobDefinition.Parameters.PurgeJobLogType.Equals(@"OlderThan", StringComparison.InvariantCultureIgnoreCase))
                {
                    rbJobLogOlderThan.Checked = true;
                    SetDropDownSelectedValue(ddlJobLogTimeRange, job.JobDefinition.Parameters.PurgeJobLogTypeValue);
                }
                else
                {
                    rbJobLogGreaterThan.Checked = true;
                    SetDropDownSelectedValue(ddlJobLogSize, job.JobDefinition.Parameters.PurgeJobLogTypeValue);
                }
            }
            else
            {
                rbJobLogGreaterThan.Checked = true;
            }
        }
    }

    public bool UpdateJob(NCMJob job)
    {
        if (job != null)
        {
            job.JobDefinition.Parameters.PurgeConfigCacheTimeRange = ddlCfgCacheTimeRange.SelectedValue;
            job.JobDefinition.Parameters.PurgeConfigArchiveTimeRange = ddlCfgArchiveTimeRange.SelectedValue;
            job.JobDefinition.Parameters.PurgeApprovalRequestsTimeRange = ddlApprovalRequestsTimeRange.SelectedValue;
            job.JobDefinition.Parameters.PurgeConfigArchive = cbCfgArchive.Checked;
            job.JobDefinition.Parameters.PurgeConfigCache = cbCfgCache.Checked;
            job.JobDefinition.Parameters.PurgeApprovalRequests = cbApprovalRequests.Checked;
            job.JobDefinition.Parameters.PurgeJobLog = cbJobLog.Checked;
            job.JobDefinition.Parameters.PurgeJobLogType = rbJobLogOlderThan.Checked ? @"OlderThan" : @"GreaterThan";
            job.JobDefinition.Parameters.PurgeJobLogTypeValue = rbJobLogOlderThan.Checked ? ddlJobLogTimeRange.SelectedValue : ddlJobLogSize.SelectedValue;
            job.JobDefinition.Parameters.PurgeTransferRequests = cbTransferRequests.Checked;
            job.JobDefinition.Parameters.PurgeTransferRequestsTimeRange = ddlTransferRequestsTimeRange.SelectedValue;
            return true;
        }
        else
            return true;
    }

    private void InternalInit()
    {
        cbCfgArchive.Checked = false;
        cbCfgCache.Checked = false;
        cbApprovalRequests.Checked = false;
        cbJobLog.Checked = false;

        ddlCfgCacheTimeRange.DataSource = RangeTimes;
        ddlCfgCacheTimeRange.DataValueField = @"Value";
        ddlCfgCacheTimeRange.DataTextField = @"Key";
        ddlCfgCacheTimeRange.DataBind();

        ddlCfgArchiveTimeRange.DataSource = RangeTimes;
        ddlCfgArchiveTimeRange.DataValueField = @"Value";
        ddlCfgArchiveTimeRange.DataTextField = @"Key";
        ddlCfgArchiveTimeRange.DataBind();
        ddlCfgArchiveTimeRange.Items.Add(new ListItem(Resources.NCMWebContent.WEBDATA_VM_186, @"Last 2 Years"));

        ddlApprovalRequestsTimeRange.DataSource = RangeTimes;
        ddlApprovalRequestsTimeRange.DataValueField = @"Value";
        ddlApprovalRequestsTimeRange.DataTextField = @"Key";
        ddlApprovalRequestsTimeRange.DataBind();

        ddlJobLogTimeRange.DataSource = RangeTimes;
        ddlJobLogTimeRange.DataValueField = @"Value";
        ddlJobLogTimeRange.DataTextField = @"Key";
        ddlJobLogTimeRange.DataBind();

        ddlJobLogSize.DataSource = Size;
        ddlJobLogSize.DataValueField = @"Value";
        ddlJobLogSize.DataTextField = @"Key";
        ddlJobLogSize.DataBind();

        ddlTransferRequestsTimeRange.DataSource = RangeTimes;
        ddlTransferRequestsTimeRange.DataValueField = @"Value";
        ddlTransferRequestsTimeRange.DataTextField = @"Key";
        ddlTransferRequestsTimeRange.DataBind();
        ddlTransferRequestsTimeRange.Items.Add(new ListItem(Resources.NCMWebContent.WEBDATA_VM_186, @"Last 3 Months"));

    }

    private void SetDropDownSelectedValue(DropDownList ddl, string value)
    {
        ddl.SelectedValue = value;
    }
}