﻿using System;
using System.Web.UI.WebControls;
using System.Globalization;
using SolarWinds.NCM.Common.Help;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_Resources_Jobs_Controls_PurgeConfigs : System.Web.UI.UserControl
{
    protected string HelpLink
    {
        get
        {
            var helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
            return helpHelperWrap.GetHelpUrl("OrionNCMPHPurgeCaveats");
        }
    }

    protected string DateTimeString
    {
        get
        {
            DateTime date = txtDateTime.TimeSpan;
            DateTime time = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            if (chkIncludeTime.Checked)
            {
                time = Convert.ToDateTime(ddlTime.SelectedValue);
                DateTime dateTime = new DateTime(date.Year, date.Month, date.Day, time.Hour, time.Minute, time.Second);
                return dateTime.ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                DateTime dateTime = new DateTime(date.Year, date.Month, date.Day);
                return dateTime.ToString(DateTimeFormatInfo.InvariantInfo.ShortDatePattern, CultureInfo.InvariantCulture);
            }
        }
        set
        {
            DateTime dateTime;
            if (!string.IsNullOrEmpty(value))
            {
                dateTime = DateTimeRoundDown30Min(ConvertValueToDateTimeSafe(value));
            }
            else
            {
                dateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            }
            txtDateTime.TimeSpan = dateTime;
            ddlTime.SelectedValue = dateTime.ToShortTimeString();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void LoadJob(NCMJob job)
    {
        BuildTimeList();
        BuildTimeRangeList();

        JobParameters jobParams = job.JobDefinition.Parameters;

        if (jobParams.PurgeConfigByDate)
        {
            rbPurgeConfigsByDate.Checked = true;
        }
        else if (jobParams.PurgeConfigByTimeRange)
        {
            rbPurgeConfigsByTimeRange.Checked = true;
        }
        else
        {
            rbPurgeConfigsByCount.Checked = true;
        }

        chkIncludeTime.Checked = jobParams.PurgeConfigIncludeTime;
        DateTimeString = jobParams.PurgeConfigDate;
        txtCount.Text = Convert.ToString(jobParams.PurgeConfigCount);
        ddlTimeRange.SelectedValue = jobParams.PurgeConfigTimeRange;
        chkPurgeBaselineConfigs.Checked = !jobParams.PurgeConfigBaseline;
    }

    public bool UpdateJob(NCMJob job)
    {
        JobParameters jobParams = job.JobDefinition.Parameters;

        if (rbPurgeConfigsByDate.Checked)
        {
            if (!txtDateTime.ValidateDate()) return false;

            jobParams.PurgeConfigByDate = true;
            jobParams.PurgeConfigByTimeRange = false;
        }
        else if (rbPurgeConfigsByTimeRange.Checked)
        {
            jobParams.PurgeConfigByDate = false;
            jobParams.PurgeConfigByTimeRange = true;
        }
        else
        {
            jobParams.PurgeConfigByDate = false;
            jobParams.PurgeConfigByTimeRange = false;
        }

        jobParams.PurgeConfigIncludeTime = chkIncludeTime.Checked;
        jobParams.PurgeConfigDate = DateTimeString;
        jobParams.PurgeConfigCount = Convert.ToInt32(txtCount.Text);
        jobParams.PurgeConfigTimeRange = ddlTimeRange.SelectedValue;
        jobParams.PurgeConfigBaseline = !chkPurgeBaselineConfigs.Checked;

        return true;
    }

    private void BuildTimeRangeList()
    {
        ddlTimeRange.Items.Clear();
        ddlTimeRange.Items.Add(new ListItem(Resources.NCMWebContent.WEBDATA_VK_751, @"Last 7 Days"));
        ddlTimeRange.Items.Add(new ListItem(Resources.NCMWebContent.WEBDATA_VK_752, @"Last Two Weeks"));
        ddlTimeRange.Items.Add(new ListItem(Resources.NCMWebContent.WEBDATA_VK_753, @"Last 30 Days"));
        ddlTimeRange.Items.Add(new ListItem(Resources.NCMWebContent.WEBDATA_VK_754, @"Last 3 Months"));
        ddlTimeRange.Items.Add(new ListItem(Resources.NCMWebContent.WEBDATA_VK_755, @"Last 6 Months"));
        ddlTimeRange.Items.Add(new ListItem(Resources.NCMWebContent.WEBDATA_VK_756, @"Last 12 Months"));
    }

    private void BuildTimeList()
    {
        ddlTime.Items.Clear();
        for (int i = 0; i < 24; i++)
        {
            DateTime dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, i, 0, 0);
            string item = dt.ToShortTimeString();
            ddlTime.Items.Add(new ListItem(item, item));

            dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, i, 30, 0);
            item = dt.ToShortTimeString();
            ddlTime.Items.Add(new ListItem(item, item));
        }
    }

    private DateTime ConvertValueToDateTimeSafe(string value)
    {
        DateTime dt;
        if (DateTime.TryParse(value, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt))
            return dt;
        if (DateTime.TryParse(value, out dt))
            return dt;
        else
            return DateTime.MinValue;
    }

    private DateTime DateTimeRoundDown30Min(DateTime dt)
    {
        return new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, (dt.Minute / 30) * 30, 0);
    }
}