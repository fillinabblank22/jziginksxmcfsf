﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/Orion/NCM/Resources/Jobs/Controls/EmailDetails.ascx.cs" Inherits="Orion_NCM_Resources_Jobs_Controls_EmailDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<link rel="stylesheet" type="text/css" href="/Orion/NCM/Resources/Jobs/Jobs.css" />

<style type="text/css">
    .ControlContainer
    {
	    padding-top:10px;
	    height: 500px;
    }
    
    .EnableEmailToolbar
    {
        border-bottom: solid 1px #dcdbd7;
        padding-bottom: 20px;
    }
        
    .LabelFont
    {
	    font-family: Arial,Verdana,Helvetica,sans-serif;
        font-size: 10pt;
        font-weight: bold;
    }
        
    .TextField
    {
        width:50%;
    }
        
    .SelectField
    {
        width:50%;
    }
    
    .TableException
    {
        background:#facece;
        color:#ce0000;
        border:1px solid #ce0000;
        border-radius:5px;            
    }
    
    .SpanException
    {
        display:inline-block;
        position:relative;
        padding:5px 6px 6px 26px;
    }
        
    .SpanException-Icon 
    {
	    position: absolute;
	    left: 4px; 
	    top: 4px; 
	    height: 16px; 
	    width: 16px; 
	    background: url(/Orion/NCM/Resources/images/icon_failed.gif) top left no-repeat; 
    }
    
    .SpanSuccess
    {
	    background: #daf7cc; 
	    color: #00a000;
	    border: 1px solid #00a000; 
	    border-radius: 5px;
	    font-size:10pt;
	    font-family: Arial,Verdana,Helvetica,sans-serif;
	    display: inline-block; 
	    position: relative;
	    padding: 5px 6px 6px 26px; 
    }

    .SpanSuccess-Icon 
    {
	    position: absolute;
	    left: 4px; 
	    top: 4px; 
	    height: 16px; 
	    width: 16px; 
	    background: url(/Orion/NCM/Resources/images/icon_ok.gif) top left no-repeat; 
    }
</style>

<script type="text/javascript">
    function ShowHideSaveResultsToFilename(checked) {
        if (checked) {
            $('#DIVSaveResultsToFilename').show();
            EnableSaveToFilenameValidator(true);
        }
        else {
            $('#DIVSaveResultsToFilename').hide();
            EnableSaveToFilenameValidator(false);
        }
    }

    function AddCCClick() {
        $("#ccBody").show();
        $("#<%=btnAddCC.ClientID%>").hide();
        return false;
    }

    function AddBCCClick() {
        $("#bccBody").show();
        $("[id$='_btnAddBCC']").hide();
        return false;
    }

    function ddlAuthListSelectIndexChange(obj) {
        var sValue = obj.options[obj.selectedIndex].value;
        ShowHideAuthContent(sValue);
    }

    function ShowHideAuthContent(sValue) {
        if (sValue == 'none') {
            EnableAuthValidators(false);
            $('#AuthContent').hide();
        }
        else {
            EnableAuthValidators(true);
            $('#AuthContent').show();
        }
    }

     function ShowHideSSL(value) {
        var rfv = document.getElementById("<%=rfvSSLPortNumber.ClientID %>");
        var rv = document.getElementById("<%=rvSSLPortNumber.ClientID %>");
        if (value) {
            if (rfv != null) ValidatorEnable(rfv, true);
            if (rv != null) ValidatorEnable(rv, true);
            $('#SSLPortContent').show();
        }
        else {
            if (rfv != null) ValidatorEnable(rfv, false);
            if (rv != null) ValidatorEnable(rv, false);
            $('#SSLPortContent').hide();
        }
    }

    function EnableMailValidators(isEnable) {
        var validator1 = $("#<%=rfvSenderName.ClientID%>")[0];
        var validator2 = $("#<%=rfvReplyAddress.ClientID%>")[0];
        var validator3 = $("#<%=rfvTo.ClientID%>")[0];
        var validator4 = $("#<%=rfvServerAddress.ClientID%>")[0];
        var validator5 = $("#<%=rfvPortNumber.ClientID%>")[0];
        var validator6 = $("#<%=rfvUsername.ClientID%>")[0];
        var validator7 = $("#<%=rfvPassword.ClientID%>")[0];
        var validator8 = $("#<%=rfvPassword2.ClientID%>")[0];
        var validator9 = $("#<%=comparePasswords.ClientID%>")[0];
        var validator10 = $("#<%=rfvSSLPortNumber.ClientID%>")[0];
        var validator11 = $("#<%=rvSSLPortNumber.ClientID%>")[0];

        ValidatorEnable(validator1, isEnable);
        ValidatorEnable(validator2, isEnable);
        ValidatorEnable(validator3, isEnable);
        ValidatorEnable(validator4, isEnable);
        ValidatorEnable(validator5, isEnable);
        ValidatorEnable(validator6, isEnable);
        ValidatorEnable(validator7, isEnable);
        ValidatorEnable(validator8, isEnable);
        ValidatorEnable(validator9, isEnable);
        ValidatorEnable(validator10, isEnable);
        ValidatorEnable(validator11, isEnable);
    }

    function EnableSaveToFilenameValidator(enable) {
        var validator = $("#<%=rfvSaveToFileName.ClientID%>")[0];
        ValidatorEnable(validator, enable);
    }

    $(function () {
        var saveResultsToFile = $("#<%=chkSaveResultsToFile.ClientID%>").prop('checked');
        ShowHideSaveResultsToFilename(saveResultsToFile);

        var isMultipleEngines = <%=IsMultipleEngines.ToString().ToLowerInvariant() %>;
        if (isMultipleEngines) {
            $('#DIVWarning').show();
        }
        else {
            $('#DIVWarning').hide();
        }

        var disableEnableEmailInfo = $("#<%=rbDisableEmailInfo.ClientID%>").prop('checked');
        if (disableEnableEmailInfo) {
            $('.ControlContainer').hide();
            EnableMailValidators(false);
            EnableAuthValidators(false);
        }
        else {
            $('.ControlContainer').show();
            EnableMailValidators(true);
            var useSSL = $("#<%=chkUseSSL.ClientID%>").prop('checked');
            ShowHideSSL(useSSL);
            var ddlSelectedValue = $('#<%=ddlAuthList.ClientID %> option:selected').val();
            ShowHideAuthContent(ddlSelectedValue);
        }

        $("#<%=rbEnableEmailInfo.ClientID%>").change(function () {
            if ($(this).is(":checked")) {
                $('.ControlContainer').show();
                EnableMailValidators(true);
                var useSSL = $("#<%=chkUseSSL.ClientID%>").prop('checked');
                ShowHideSSL(useSSL);
                var ddlSelectedValue = $('#<%=ddlAuthList.ClientID %> option:selected').val();
                ShowHideAuthContent(ddlSelectedValue);
            }
        });

        $("#<%=rbDisableEmailInfo.ClientID%>").change(function () {
            if ($(this).is(":checked")) {
                $('.ControlContainer').hide();
                EnableMailValidators(false);
            }
        });

        if ($("#<%= txtRTNEmailCC.ClientID%>").val() != "") {
            AddCCClick();
        }

        if ($("#<%= txtRTNEmailBCC.ClientID%>").val() != "") {
            AddBCCClick();
        }
    });

    function cvPassword_Validate(source, args) {
        var firstPas = $("#<%=txtPassword.ClientID%>").val();
        args.IsValid = (args.Value == firstPas);
    }

    function EnableAuthValidators(enable) {
        var validator;

        validator = document.getElementById("<%=rfvUsername.ClientID %>");
        ValidatorEnable(validator, enable);

        validator = document.getElementById("<%=rfvPassword.ClientID %>");
        ValidatorEnable(validator, enable);

        validator = document.getElementById("<%=rfvPassword2.ClientID %>");
        ValidatorEnable(validator, enable);

        validator = document.getElementById("<%=comparePasswords.ClientID %>");
        ValidatorEnable(validator, enable);
    }
</script>

<div style="padding-top:20px;">
    
    <div style="padding-bottom:20px;">
        <asp:CheckBox ID="chkLogSteps" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_948 %>" />
    </div>

    <div style="padding-bottom:20px;">
        <asp:CheckBox ID="chkSaveResultsToFile" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_949 %>" onclick="ShowHideSaveResultsToFilename(this.checked);"/>
        <div id="DIVSaveResultsToFilename" style="padding-top:10px;">
            <div id="DIVWarning" style="padding-top:10px;">
                <div class="Warning"><div class="Warning-Icon"></div>
                    <%= Resources.NCMWebContent.WEBDATA_VK_943 %>
                </div>
            </div>
            <div style="padding-bottom:5px;padding-top:10px;"><%= Resources.NCMWebContent.WEBDATA_VK_944 %></div>
            <asp:TextBox ID="txtSaveToFileName" runat="server" TextMode="SingleLine" CssClass="TextField" Width="500px" />
            <asp:RequiredFieldValidator ID="rfvSaveToFileName" runat="server" Display="Dynamic" ControlToValidate="txtSaveToFileName" ErrorMessage="*" />
            <orion:LocalizableButton runat="server" ID="btnValidate" LocalizedText="CustomText" DisplayType="Small" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_590 %>" CausesValidation="true" OnClick="btnValidate_Click"/>            
            <span style='font-size:8pt;padding-left:20px;'>&#0187;&nbsp;<a style='color:#336699;' target='_blank' href="<%=LearnMoreOrionConfigNetworkPath %>"><%=Resources.NCMWebContent.WEBDATA_IA_36%></a></span>
            <asp:Literal ID="validator" runat="server"></asp:Literal>
        </div>
    </div>

    <div class="EnableEmailToolbar">
        <asp:RadioButton ID="rbEnableEmailInfo" GroupName="EnableEmail" Checked="true" Text="<%$ Resources: NCMWebContent, WEBDATA_RB_01 %>" runat="server" />
        <div style="display:inline;margin-left:20px;">
        <asp:RadioButton ID="rbDisableEmailInfo" GroupName="EnableEmail" Text="<%$ Resources: NCMWebContent, WEBDATA_RB_02 %>" runat="server" />
        </div>
    </div>

    <div class="ControlContainer">
        <%-- First Column --%>
        <div style="float:left;width:45%;margin-top: 25px;">
            <div style="padding-bottom:5px;">
                <b class="LabelFont"><%= Resources.NCMWebContent.WEBDATA_IC_70 %></b>
            </div>
            <div>
                <asp:TextBox ID="txtSenderName" runat="server" CssClass="TextField" MaxLength="4000"/>
                <asp:RequiredFieldValidator ID="rfvSenderName" runat="server" Display="Dynamic" ControlToValidate="txtSenderName" ErrorMessage="*" />
            </div>
            <div style="padding-top:20px;padding-bottom:5px;">
                <b class="LabelFont"><%= Resources.NCMWebContent.WEBDATA_IC_71 %></b>
            </div>
            <div>
                <asp:TextBox ID="txtReplyAddress" runat="server" CssClass="TextField" MaxLength="4000"/>
                <asp:RequiredFieldValidator ID="rfvReplyAddress" runat="server" Display="Dynamic" ControlToValidate="txtReplyAddress" ErrorMessage="*"></asp:RequiredFieldValidator>
            </div>
            <div style="padding-top:20px;padding-bottom:5px;">
                <b class="LabelFont"><%= Resources.NCMWebContent.WEBDATA_IC_72 %></b>
            </div>
            <div>
                <asp:TextBox ID="txtSubject" runat="server" CssClass="TextField" MaxLength="4000"/>                            
            </div>

            <div style="padding-top:20px;padding-bottom:5px;"><b class="LabelFont"><%= Resources.NCMWebContent.WEBDATA_IC_73 %></b></div>
            <asp:TextBox ID="txtTo" runat="server" CssClass="TextField" MaxLength="4000"/>
            <asp:RequiredFieldValidator ID="rfvTo" runat="server" Display="Dynamic" ControlToValidate="txtTo" ErrorMessage="*"></asp:RequiredFieldValidator>
            <div style="margin-left:10px;display:inline;">
            <orion:LocalizableButton runat="server" ID="btnAddCC" OnClientClick="return AddCCClick();" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_IC_74 %>" DisplayType="Small" CausesValidation="false" />
            <orion:LocalizableButton runat="server" ID="btnAddBCC" OnClientClick="return AddBCCClick();" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_IC_75 %>" DisplayType="Small" CausesValidation="false"/>
            </div>
            <div class="Hint" style="padding-top:10px;">
                <%= Resources.NCMWebContent.WEBDATA_VM_168 %>
            </div>
            <div id="ccBody"style="display:none;">
                <div style="padding-top:20px;padding-bottom:5px;"><b class="LabelFont"><%= Resources.NCMWebContent.WEBDATA_IC_76 %></b></div>
                <div>
                    <asp:TextBox ID="txtRTNEmailCC" runat="server" TextMode="SingleLine" CssClass="TextField" />
                </div>
            </div>

            <div id="bccBody"style="display:none;">
                <div style="padding-top:20px;padding-bottom:5px;"><b class="LabelFont"><%= Resources.NCMWebContent.WEBDATA_IC_77 %></b></div>
                <div>
                    <asp:TextBox ID="txtRTNEmailBCC" runat="server" TextMode="SingleLine" CssClass="TextField" />
                </div>
            </div>
            <div style="margin-top: 20px;">
                <asp:CheckBox ID="checkBoxAttachAsPDF" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_RB_03 %>" />
            </div>

            <div style="margin-top: 20px;">
                <asp:CheckBox ID="checkBoxEmailOnlyOnError" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_797 %>" />
            </div>

            <div style="padding-top:50px;">
                <span class="Suggestion">
                    <span class="Suggestion-Icon"></span> <%=string.Format(Resources.NCMWebContent.WEBDATA_VK_798, @"<a href='/Orion/NCM/Admin/Settings/EmailDefaults.aspx' style='color:#336699;' target='_blank'>", @"</a>")%>
                </span>
            </div>
        </div>
        <%-- End of First Column --%>

        <%-- Second Column --%>
        <div style="float:right;width:47.5%;margin-top:25px;border-left: solid 1px #dcdbd7;padding-left:2.5%;height:90%;">
            <div style="padding-bottom:5px;"><b class="LabelFont"><%= Resources.NCMWebContent.WEBDATA_IC_56 %></b></div>
            <div>
                <asp:TextBox ID="txtServerAddress" runat="server" CssClass="TextField" MaxLength="4000"/>
                <asp:RequiredFieldValidator ID="rfvServerAddress" runat="server" Display="Dynamic" ControlToValidate="txtServerAddress" ErrorMessage="*" />
            </div>

            <div style="padding-top:20px;padding-bottom:5px;"><b class="LabelFont"><%= Resources.NCMWebContent.WEBDATA_IC_117 %></b></div>
            <div>
                <asp:TextBox ID="txtPortNumber" runat="server" CssClass="TextField" MaxLength="4000"/>
                <asp:RequiredFieldValidator ID="rfvPortNumber" runat="server" Display="Dynamic" ControlToValidate="txtPortNumber" ErrorMessage="*"></asp:RequiredFieldValidator>
                <ajaxToolkit:FilteredTextBoxExtender ID="ftbe" runat="server" TargetControlID="txtPortNumber" FilterType="Numbers" />
            </div>

            <div style="padding-top:20px;">
                <asp:CheckBox ID="chkUseSSL" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_IC_61 %>" OnClick="ShowHideSSL(this.checked);"  />
            </div>
            <div id="SSLPortContent" style="background-color:#e4f1f8;width:50%;">
                <table cellpadding="0" cellspacing="0" width="100%" style="padding:10px;">
                    <tr>
                        <td class="LabelFont"><%= Resources.NCMWebContent.WEBDATA_IC_117 %></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txtSSLPortNumber" runat="server" CssClass="TextField"/>
                            <asp:RequiredFieldValidator ID="rfvSSLPortNumber" runat="server" Display="Dynamic" ControlToValidate="txtSSLPortNumber" ErrorMessage="*"></asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="rvSSLPortNumber" runat="server" ControlToValidate="txtSSLPortNumber" ErrorMessage="* Port Number must be between 1 and 65535" MaximumValue="65535" MinimumValue="1" Type="Integer" Display="Dynamic"></asp:RangeValidator>
                            <ajaxToolkit:FilteredTextBoxExtender ID="fteSSLPortNumber" runat="server" TargetControlID="txtSSLPortNumber" FilterType="Numbers" />
                        </td>
                    </tr>
                </table>
            </div>

            <div style="padding-top:20px;padding-bottom:5px;"><b class="LabelFont"><%= Resources.NCMWebContent.WEBDATA_IC_58 %></b></div>
            <div>
                <asp:DropDownList ID="ddlAuthList" CssClass="SelectField" runat="server" onchange="ddlAuthListSelectIndexChange(this);">
                    <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_IC_60 %>" Selected="true" Value="pwd"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_IC_59 %>" Value="none"></asp:ListItem>
                </asp:DropDownList>
            </div>

            <div id="AuthContent">
                <div style="padding-top:20px;padding-bottom:5px;"><b class="LabelFont"><%= Resources.NCMWebContent.WEBDATA_IC_62 %></b></div>

                <div>                        
                    <asp:TextBox ID="txtUsername" runat="server" CssClass="TextField" MaxLength="4000"/>
                    <asp:RequiredFieldValidator ID="rfvUsername" runat="server" Display="Dynamic" ControlToValidate="txtUsername" ErrorMessage="*" />
                </div>

                <div style="padding-top:20px;padding-bottom:5px;"><b class="LabelFont"><%= Resources.NCMWebContent.WEBDATA_IC_63 %></b></div>
                <div> 
                    <ncm:PasswordTextBox ID="txtPassword" runat="server" CssClass="TextField" TextMode="Password" MaxLength="4000"/>
                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" Display="Dynamic" ControlToValidate="txtPassword" ErrorMessage="*" />
                </div>

                <div style="padding-top:20px;padding-bottom:5px;"><b class="LabelFont"><%= Resources.NCMWebContent.WEBDATA_IC_64 %></b></div>
                <div> 
                    <ncm:PasswordTextBox ID="txtPassword2" runat="server" CssClass="TextField" TextMode="Password" MaxLength="4000"/>
                    <asp:RequiredFieldValidator ID="rfvPassword2" runat="server" Display="Dynamic" ControlToValidate="txtPassword2" ErrorMessage="*" />
                </div>
                <div>
                    <asp:CustomValidator id="comparePasswords"
                    EnableClientScript="true" ClientValidationFunction="cvPassword_Validate" 
                    ValidateEmptyText="true" ControlToValidate="txtPassword2" runat="server" 
                    ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_689 %>" 
                    Display="Dynamic" Enabled="true" />
                </div>
            </div>
        </div>
        <%-- End of Second Column --%>
    </div>
</div>