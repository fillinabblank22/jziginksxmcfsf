﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PolicyReports.ascx.cs" Inherits="Orion_NCM_Resources_Jobs_Controls_PolicyReports" %>

<script type="text/javascript">
    var tree;

    function InitializeTree (treeId) {
        tree = igtree_getTreeById(treeId);
    }

    var validateReportSelection = function (sender, args) {
        var node = tree.getSelectedNode();
        if (node != null) {
            var dataPath = node.getDataPath();
            if (dataPath != null && dataPath.substring(0, 2) == "G:") {
                args.IsValid = false;
            } else {
                args.IsValid = true;
            }
        } else {
            args.IsValid = false;
        }
    }
</script>

<table cellpadding="0" cellspacing="0">
	<tr>
	    <td style="padding-top:10px;padding-bottom:5px;"><b><%=Resources.NCMWebContent.WEBDATA_IC_133%></b>
            <div style="font-size:8pt;padding-top:5px;">&#0187;&nbsp;<a style="color:#336699;" href="<%=this.LearnMore %>" target="_blank"><%=Resources.NCMWebContent.WEBDATA_IC_134%></a></div>
        </td>
	</tr>
    <tr>
        <td style="padding-top:20px">
            <asp:Panel ID="treePanel" widht="100%" runat="server"/>
            <asp:CustomValidator ID="cvValidateReportSelection" runat="server" Display="Dynamic" EnableClientScript="true" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_IC_135%>" ClientValidationFunction="validateReportSelection"></asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td style="padding-top:50px">
            <asp:CheckBox ID="chkSendOnError" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_IC_136%>" />
        </td>
    </tr>
</table>