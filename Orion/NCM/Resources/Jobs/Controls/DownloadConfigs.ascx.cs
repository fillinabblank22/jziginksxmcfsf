﻿using System;
using SolarWinds.NCM.Contracts.InformationService;

public partial class Orion_NCM_Admin_Jobs_Controls_DownloadConfigs : System.Web.UI.UserControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        chkSendNotificationDetails.Text = string.Format(Resources.NCMWebContent.WEBDATA_IC_132, @"<b>", @"</b>");
    }

    public void LoadJob(NCMJob job)
    {
        //load selected configs
        configs.LoadConfigs(job.JobDefinition.Parameters.ConfigTypes.Split (','));

        //load notification options
        chkLastDownloaded.Checked = job.JobDefinition.Parameters.NotifyChangedLast;
        chkBaseline.Checked = job.JobDefinition.Parameters.NotifyChangedBaseline;
        chkStartup.Checked = job.JobDefinition.Parameters.NotifyChangedStartup;

        //other options
        if (job.JobDefinition.Parameters.NotifySeparateHtmlEmail || job.JobDefinition.Parameters.NotifySeparateTextEmail)
        {
            chkSendNotificationDetails.Checked = true;
            rbTextEmail.Checked = job.JobDefinition.Parameters.NotifySeparateTextEmail;
            rbHTMLEmail.Checked = job.JobDefinition.Parameters.NotifySeparateHtmlEmail;
        }
        else
        {
            rbHTMLEmail.Checked = true; //by default
        }
        chkSaveChanged.Checked = job.JobDefinition.Parameters.OnlySaveChangedConfigs;
    }

    public bool UpdateJob(NCMJob job)
    {
        //Validation
        lblError.Visible = false;
        string selectedConfigTypes = configs.GetSelectedConfigs(); ;
        if (string.IsNullOrEmpty(selectedConfigTypes))
        {
            lblError.Visible = true;
            return false;
        }
        
        //update config types
        job.JobDefinition.Parameters.ConfigTypes = selectedConfigTypes;

        trEmailResultsDisabled.Visible = false;
        if ((chkLastDownloaded.Checked || chkBaseline.Checked || chkStartup.Checked) && !job.JobDefinition.Notifications.EmailResults)
        {
            trEmailResultsDisabled.Visible = true;
            return false;
        }

        //update notification options
        job.JobDefinition.Parameters.NotifyChangedLast = chkLastDownloaded.Checked;
        job.JobDefinition.Parameters.NotifyChangedBaseline = chkBaseline.Checked;
        job.JobDefinition.Parameters.NotifyChangedStartup = chkStartup.Checked;

        //other options
        if (chkSendNotificationDetails.Checked)
        {
            job.JobDefinition.Parameters.NotifySeparateTextEmail = rbTextEmail.Checked;
            job.JobDefinition.Parameters.NotifySeparateHtmlEmail = rbHTMLEmail.Checked;
        }
        else
        {
            job.JobDefinition.Parameters.NotifySeparateTextEmail = false;
            job.JobDefinition.Parameters.NotifySeparateHtmlEmail = false;
        }
        job.JobDefinition.Parameters.OnlySaveChangedConfigs = chkSaveChanged.Checked;

        return true;
    }
}