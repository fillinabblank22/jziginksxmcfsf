﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectedConfigs.ascx.cs" Inherits="Orion_NCM_Admin_Jobs_Controls_SelectedConfigs" %>
 
 <div>
    <asp:ListView runat="server" ID="ListView">    
        <LayoutTemplate>
            <table id="table" runat="server" cellpadding="0" cellspacing="0">
                <tr id="itemPlaceholder"></tr>
            </table>
        </LayoutTemplate>        
        <ItemTemplate>  
            <tr>
                <td style="padding-bottom:5px;">
                    <asp:CheckBox ID="chkSelectedConfig" runat="server" class="SelectedConfigType" Checked=<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, @"IsSelected")) %> /> 
                </td>
                <td style="padding-bottom:5px;">
                    <asp:Label ID="lConfigName" runat="server" Text=<%# HttpUtility.HtmlEncode(Eval(@"ConfigType")) %> /> 
                </td>
            </tr>    
        </ItemTemplate>
    </asp:ListView>
 </div>