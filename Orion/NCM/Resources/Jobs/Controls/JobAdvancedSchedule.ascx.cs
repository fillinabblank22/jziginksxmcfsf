﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Globalization;
using SolarWinds.NCM.Common.Help;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;
using internalWeekDays = SolarWinds.NCM.Contracts.InformationService.NCMJobSchedule.eTriggerDays;
using internalMonths = SolarWinds.NCM.Contracts.InformationService.NCMJobSchedule.eTriggerMonths;
using internalTriggerType = SolarWinds.NCM.Contracts.InformationService.NCMJobSchedule.eTriggerType;


public partial class Orion_NCM_Resources_Jobs_Controls_JobAdvancedSchedule : System.Web.UI.UserControl
{
    bool IsValid;
    List<string> errors;

    public void LoadJob(NCMJob job)
    {
        if (job != null)
        {
            InitSchedule(job.JobSchedule);
        }
    }

    public bool UpdateJob(NCMJob job)
    {
        Validate();

        if (IsValid)
        {
            if (job != null)
            {
                if (ddlScheduleType.SelectedValue == @"0")//basic schedule
                {
                    SaveBasicSchedule(job.JobSchedule);
                }
                else//1 advanced schedule
                {
                    SaveAdvancedSchedule(job.JobSchedule);
                }

            }

            return true;
        }
        else
        {
            ShowErrorContent(true);
            return false;
        }
            
    }

    protected string LearnMore
    {
        get
        {
            var helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
            return helpHelperWrap.GetHelpUrl("OrionNCMPHCronExpressions");
        }
    }
    #region UserControl override

    protected override void OnInit(EventArgs e)
    {
        InternalWeekDaysListInit();
        InternalMonthsListInit();

        txtStartTimeMonthlyTab.Text = @"12:00";
        txtStartTimeWeeklyTab.Text = @"12:00";
        txtStartTimeDailyTab.Text = @"12:00";

        txtDaysOfTheMonth.Text = @"1";

        ShowAdvancedSchedule(false);
        errorContent.Attributes[@"style"] = @"display:none;";

        ddlScheduleType.SelectedIndex = 0;

        dateTimePicker.TimeSpan = DateTime.Now.AddMinutes(20);
        IsValid = true;
        errors = new List<string>();

        txtDailyStartingOn.TimeSpan = DateTime.Now;
        txtDailyEndingOn.TimeSpan = DateTime.Now.AddYears(10);

        txtWeeklyStartingOn.TimeSpan = DateTime.Now;
        txtWeeklyEndingOn.TimeSpan = DateTime.Now.AddYears(10);

        txtMonthlyStartingOn.TimeSpan = DateTime.Now;
        txtMonthlyEndingOn.TimeSpan = DateTime.Now.AddYears(10);

        txtAdvStartingOn.TimeSpan = DateTime.Now;
        txtAdvEndingOn.TimeSpan = DateTime.Now.AddYears(10);

        base.OnInit(e);
    }

    protected override void OnLoad(EventArgs e)
    {
        ShowAdvancedSchedule(ddlScheduleType.SelectedValue == @"1");

        base.OnLoad(e);
    }
    
    #endregion

    #region private methods

    private void InternalWeekDaysListInit()
    {
        cblWeekDays.Items.Add(new ListItem(DateTimeFormatInfo.CurrentInfo.GetDayName(DayOfWeek.Monday), internalWeekDays.Monday.ToString()));
        cblWeekDays.Items.Add(new ListItem(DateTimeFormatInfo.CurrentInfo.GetDayName(DayOfWeek.Tuesday), internalWeekDays.Tuesday.ToString()));
        cblWeekDays.Items.Add(new ListItem(DateTimeFormatInfo.CurrentInfo.GetDayName(DayOfWeek.Wednesday), internalWeekDays.Wednesday.ToString()));
        cblWeekDays.Items.Add(new ListItem(DateTimeFormatInfo.CurrentInfo.GetDayName(DayOfWeek.Thursday), internalWeekDays.Thursday.ToString()));
        cblWeekDays.Items.Add(new ListItem(DateTimeFormatInfo.CurrentInfo.GetDayName(DayOfWeek.Friday), internalWeekDays.Friday.ToString()));
        cblWeekDays.Items.Add(new ListItem(DateTimeFormatInfo.CurrentInfo.GetDayName(DayOfWeek.Saturday), internalWeekDays.Saturday.ToString()));
        cblWeekDays.Items.Add(new ListItem(DateTimeFormatInfo.CurrentInfo.GetDayName(DayOfWeek.Sunday), internalWeekDays.Sunday.ToString()));
        
    }

    private void InternalMonthsListInit()
    {
        cblMonths.Items.Add(new ListItem(DateTimeFormatInfo.CurrentInfo.GetMonthName(1), internalMonths.January.ToString()));
        cblMonths.Items.Add(new ListItem(DateTimeFormatInfo.CurrentInfo.GetMonthName(2), internalMonths.February.ToString()));
        cblMonths.Items.Add(new ListItem(DateTimeFormatInfo.CurrentInfo.GetMonthName(3), internalMonths.March.ToString()));
        cblMonths.Items.Add(new ListItem(DateTimeFormatInfo.CurrentInfo.GetMonthName(4), internalMonths.April.ToString()));
        cblMonths.Items.Add(new ListItem(DateTimeFormatInfo.CurrentInfo.GetMonthName(5), internalMonths.May.ToString()));
        cblMonths.Items.Add(new ListItem(DateTimeFormatInfo.CurrentInfo.GetMonthName(6), internalMonths.June.ToString()));
        cblMonths.Items.Add(new ListItem(DateTimeFormatInfo.CurrentInfo.GetMonthName(7), internalMonths.July.ToString()));
        cblMonths.Items.Add(new ListItem(DateTimeFormatInfo.CurrentInfo.GetMonthName(8), internalMonths.August.ToString()));
        cblMonths.Items.Add(new ListItem(DateTimeFormatInfo.CurrentInfo.GetMonthName(9), internalMonths.September.ToString()));
        cblMonths.Items.Add(new ListItem(DateTimeFormatInfo.CurrentInfo.GetMonthName(10), internalMonths.October.ToString()));
        cblMonths.Items.Add(new ListItem(DateTimeFormatInfo.CurrentInfo.GetMonthName(11), internalMonths.November.ToString()));
        cblMonths.Items.Add(new ListItem(DateTimeFormatInfo.CurrentInfo.GetMonthName(12), internalMonths.December.ToString()));
    }

    private void SetSelectedDaysOfWeek(List<internalWeekDays> days)
    {
        if (days == null || days.Count == 0)
            return;

        foreach (var day in days)
        {
            ListItem item = cblWeekDays.Items.FindByValue(day.ToString());

            if (item != null)
                item.Selected = true;
        }
    }

    private List<internalWeekDays> GetSelectedDaysOfWeek()
    {
        List<internalWeekDays> days = new List<internalWeekDays>();
        foreach (ListItem item in cblWeekDays.Items)
        {
            if (item.Selected)
            {
                days.Add((internalWeekDays)Enum.Parse(typeof(internalWeekDays), item.Value));
            }
        }
        return days;
    }

    private void SetSelectedMonths(List<internalMonths> months)
    {
        if (months == null || months.Count == 0)
            return;

        foreach (var month in months)
        {
            ListItem item = cblMonths.Items.FindByValue(month.ToString());

            if (item != null)
                item.Selected = true;
        }
       
        cbAllMonths.Checked = months.Count == 12;
    }

    private List<internalMonths> GetSelectedMonths()
    {
        List<internalMonths> months = new List<internalMonths>();
        foreach (ListItem item in cblMonths.Items)
        {
            if (item.Selected)
            {
                months.Add((internalMonths)Enum.Parse(typeof(internalMonths), item.Value));
            }
        }
        
        return months;
    }

    private void InitSchedule(NCMJobSchedule schedule)
    {
        if (schedule.TriggerType == internalTriggerType.Advanced)
        {
            ddlScheduleType.SelectedValue = @"1";
            ShowAdvancedSchedule(true);
            InitAdvancedSchedule(schedule);
        }
        else
        {
            ddlScheduleType.SelectedValue = @"0";
            ShowAdvancedSchedule(false);
            InitBasicSchedule(schedule);
        }
    }

    private void InitAdvancedSchedule(NCMJobSchedule schedule)
    {
        /*
         * * * * * *
        - - - - -
        | | | | |
        | | | | +----- day of week (0 - 6) (Sunday=0)
        | | | +------- month (1 - 12)
        | | +--------- day of month (1 - 31)
        | +----------- hour (0 - 23)
        +------------- min (0 - 59)
        */

        if (string.IsNullOrEmpty(schedule.AdvancedCRONExpression))
            return;

        string[] items = schedule.AdvancedCRONExpression.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

        if (items.Length != 5)// ncrontab format should have 5 words only
            return;

        txtAdvMinutes.Text = items[0];
        txtAdvHours.Text = items[1];
        txtAdvDayOfMonth.Text = items[2];
        txtAdvMonth.Text = items[3];
        txtAdvDayOfWeek.Text = items[4];

        txtAdvStartingOn.TimeSpan = schedule.StartingOn;
        txtAdvEndingOn.TimeSpan = schedule.EndingOn;
    }

    private void InitBasicSchedule(NCMJobSchedule schedule)
    {
        switch (schedule.TriggerType)
        {
            case internalTriggerType.Daily:
                txtStartTimeDailyTab.Text = schedule.StartTime.ToString(@"HH:mm");
                hfActiveSceduleTriggerTab.Value = internalTriggerType.Daily.ToString();
                txtDailyStartingOn.TimeSpan = schedule.StartingOn;
                txtDailyEndingOn.TimeSpan = schedule.EndingOn;
                break;
            case internalTriggerType.Monthly:
                txtStartTimeMonthlyTab.Text = schedule.StartTime.ToString(@"HH:mm");
                txtDaysOfTheMonth.Text = schedule.Day.ToString();
                SetSelectedMonths(schedule.Months);
                hfActiveSceduleTriggerTab.Value = internalTriggerType.Monthly.ToString();
                txtMonthlyStartingOn.TimeSpan = schedule.StartingOn;
                txtMonthlyEndingOn.TimeSpan = schedule.EndingOn;
                break;
            case internalTriggerType.Weekly:
                txtStartTimeWeeklyTab.Text = schedule.StartTime.ToString(@"HH:mm");
                SetSelectedDaysOfWeek(schedule.DaysOfTheWeek);
                hfActiveSceduleTriggerTab.Value = internalTriggerType.Weekly.ToString();
                txtWeeklyStartingOn.TimeSpan = schedule.StartingOn;
                txtWeeklyEndingOn.TimeSpan = schedule.EndingOn;
                break;
            case internalTriggerType.Once:
                dateTimePicker.TimeSpan = schedule.StartDate;
                hfActiveSceduleTriggerTab.Value = internalTriggerType.Once.ToString();
                break;
            default:
                hfActiveSceduleTriggerTab.Value = internalTriggerType.Daily.ToString();
                break;
        }
    }

    private void SaveBasicSchedule(NCMJobSchedule schedule)
    {
        //detect basic schedule trigger type based on hidden field value
        schedule.TriggerType = (internalTriggerType)Enum.Parse(typeof(internalTriggerType), hfActiveSceduleTriggerTab.Value);

        switch (schedule.TriggerType)
        {
            case internalTriggerType.Daily:
                schedule.StartTime = DateTime.ParseExact(txtStartTimeDailyTab.Text, @"HH:mm", null, DateTimeStyles.AllowWhiteSpaces);
                schedule.Day = 1;
                schedule.StartingOn = txtDailyStartingOn.TimeSpan;
                schedule.EndingOn = txtDailyEndingOn.TimeSpan;
                break;
            case internalTriggerType.Monthly:
                schedule.StartTime = DateTime.ParseExact(txtStartTimeMonthlyTab.Text, @"HH:mm", null, DateTimeStyles.AllowWhiteSpaces);
                schedule.Day = int.Parse(txtDaysOfTheMonth.Text);
                schedule.Months = GetSelectedMonths();
                schedule.StartingOn = txtMonthlyStartingOn.TimeSpan;
                schedule.EndingOn = txtMonthlyEndingOn.TimeSpan;
                break;
            case internalTriggerType.Weekly:
                schedule.StartTime = DateTime.ParseExact(txtStartTimeWeeklyTab.Text, @"HH:mm", null, DateTimeStyles.AllowWhiteSpaces);
                schedule.DaysOfTheWeek = GetSelectedDaysOfWeek();
                schedule.StartingOn = txtWeeklyStartingOn.TimeSpan;
                schedule.EndingOn = txtWeeklyEndingOn.TimeSpan;
                break;
            case internalTriggerType.Once:
                schedule.StartDate = dateTimePicker.TimeSpan;
                break;
            default:
                break;
        }
    }

    private void SaveAdvancedSchedule(NCMJobSchedule schedule)
    {
        schedule.TriggerType = internalTriggerType.Advanced;

        schedule.AdvancedCRONExpression =
            $@"{txtAdvMinutes.Text} {txtAdvHours.Text} {txtAdvDayOfMonth.Text} {txtAdvMonth.Text} {txtAdvDayOfWeek.Text}";

        schedule.StartingOn = txtAdvStartingOn.TimeSpan;
        schedule.EndingOn = txtAdvEndingOn.TimeSpan;
    }

    private void ShowAdvancedSchedule(bool val)
    {
        if (val)
        {
            basicScheduleContent.Attributes[@"style"] = @"display:none;";
            advancedScheduleContent.Attributes[@"style"] = @"display:block;";
        }
        else
        {
            basicScheduleContent.Attributes[@"style"] = @"display:block;";
            advancedScheduleContent.Attributes[@"style"] = @"display:none;";
        }
    }

    #endregion

    #region validators
    
    private void Validate()
    {
        if (ddlScheduleType.SelectedValue == @"0")//basic schedule
        {
            ValidateBasicSchedule();
        }
        else
        { 
            ValidateAdvancedSchedule();
        }
    }

    private void ValidateBasicSchedule()
    {
        internalTriggerType basicScheduleType = (internalTriggerType)Enum.Parse(typeof(internalTriggerType),hfActiveSceduleTriggerTab.Value);

        switch (basicScheduleType)
        { 
            case internalTriggerType.Daily:
                ValidateBasicDailySchedule();
                break;
            case internalTriggerType.Monthly:
                ValidateBasicMonthlySchedule();
                break;
            case internalTriggerType.Weekly:
                ValidateBasicWeeklySchedule();
                break;
            case internalTriggerType.Once:
                ValidateOnesSchedule();
                break;
        }
    }

    private void ValidateOnesSchedule()
    {
        if ((dateTimePicker.TimeSpan - DateTime.Now).TotalMinutes < 15)
        {
            IsValid = false;

            int utcOffset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow).Hours;
            string ncmServerTime = string.Format(Resources.NCMWebContent.WEBDATA_VK_878, utcOffset >= 0 ? @"+" : @"-", Math.Abs(utcOffset), DateTime.Now.ToShortTimeString());
            errors.Add($@"{Resources.NCMWebContent.WEBDATA_VM_216} {ncmServerTime}");
        }
        else
        {
            IsValid = true;
        }

    }

    private void ValidateBasicDailySchedule()
    {
        ValidateStartTime(txtStartTimeDailyTab.Text);
        ValidateStartingOnAndEndingOnDateRange(txtDailyStartingOn.TimeSpan, txtDailyEndingOn.TimeSpan);
    }

    private void ValidateBasicWeeklySchedule()
    {
        List<internalWeekDays> res = GetSelectedDaysOfWeek();
        if (res.Count == 0)
        {
            IsValid = false;
            errors.Add(Resources.NCMWebContent.WEBDATA_VM_204);
        }

        ValidateStartTime(txtStartTimeWeeklyTab.Text);
        ValidateStartingOnAndEndingOnDateRange(txtWeeklyStartingOn.TimeSpan, txtWeeklyEndingOn.TimeSpan);
    }

    private void ValidateBasicMonthlySchedule()
    {
        List<internalMonths> res = GetSelectedMonths();
        if (res.Count == 0)
        {
            IsValid = false;
            errors.Add(Resources.NCMWebContent.WEBDATA_VM_205);
        }

        ValidateStartTime(txtStartTimeMonthlyTab.Text);

        int x;
        if (!int.TryParse(txtDaysOfTheMonth.Text, out x) || x < 1 || x > 31)
        {
            IsValid = false;
            errors.Add(Resources.NCMWebContent.WEBDATA_VM_206);
        }
        ValidateStartingOnAndEndingOnDateRange(txtMonthlyStartingOn.TimeSpan, txtMonthlyEndingOn.TimeSpan);
    }

    private void ValidateAdvancedSchedule()
    {
       this.Page.Validate(@"advSceduleValidators");
       IsValid = Page.IsValid;
       ValidateStartingOnAndEndingOnDateRange(txtAdvStartingOn.TimeSpan, txtAdvEndingOn.TimeSpan);
    }

    private void ValidateStartTime(string timeStr)
    {
        DateTime dt;
        if (string.IsNullOrEmpty(timeStr) ||!DateTime.TryParseExact(timeStr, @"HH:mm", null, DateTimeStyles.AllowWhiteSpaces, out dt))
        {
            IsValid = false;
            errors.Add(Resources.NCMWebContent.WEBDATA_VM_207);
        }
    }

    private void ValidateStartingOnAndEndingOnDateRange(DateTime startingOn, DateTime endingOn)
    {
        if (startingOn > endingOn)
        {
            IsValid = false;
            errors.Add(Resources.NCMWebContent.WEBDATA_VK_851);
        }
    }

    private void ShowErrorContent(bool val)
    {
        errorContent.Attributes[@"style"] = val ? @"display:block;" : @"display:none;";
        if (val)
        {
            blErrors.DataSource = errors;
            blErrors.DataBind();
        }
    }
    
    #endregion
}