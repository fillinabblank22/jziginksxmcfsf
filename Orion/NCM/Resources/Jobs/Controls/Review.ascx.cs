﻿using System;
using SolarWinds.NCM.Common.Help;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts;

public partial class Orion_NCM_Resources_Jobs_Controls_Review : System.Web.UI.UserControl
{
    public bool IsValid { get; private set; }

    public string ErrorMsg { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    public void LoadJob(NCMJob Job)
    {
        Validate(Job);

        lblJobname.Text = Job.JobName;
        lblJobType.Text = JobHelper.LookupLocalizedJobType(Job.JobType);
        lblFrequency.Text = JobHelper.LookupLocalisedScheduleDetails(Job.JobSchedule);
        if (Job.JobSchedule.TriggerType == NCMJobSchedule.eTriggerType.Advanced)
        {
            lblRunTime.Text = string.Format(Resources.NCMWebContent.WEBDATA_IC_160, Job.JobSchedule.AdvancedCRONExpression);
            cronHelpLink.Visible = true;
        }
        else if (Job.JobSchedule.TriggerType == NCMJobSchedule.eTriggerType.Once)
        {
            lblRunTime.Text = Job.JobSchedule.StartDate.ToString();
            cronHelpLink.Visible = false;
        }
        else
        {
            lblRunTime.Text = Job.JobSchedule.StartTime.ToShortTimeString();
            cronHelpLink.Visible = false;
        }


        if (!string.IsNullOrEmpty(Job.JobDefinition.Comments))
        {
            lblComments.Text = Job.JobDefinition.Comments;
        }
        else
        {
            lblComments.Text = Resources.NCMWebContent.WEBDATA_IC_148;
        }

        if (Job.JobDefinition.Nodes.SelectedCriteriaType == eCriteriaType.AllNodes)
        {
            lblSelectedNodes.Text = Resources.NCMWebContent.WEBDATA_IC_149;
        }
        else
        {
            lblSelectedNodes.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_814, JobHelper.GetSelectedNodesCount(Job, true));
        }

        if (Job.JobDefinition.Notifications.EmailResults)
        {
            lblEmailNotification.Text = Resources.NCMWebContent.WEBDATA_IC_150;
        }
        else
        {
            lblEmailNotification.Text = Resources.NCMWebContent.WEBDATA_IC_151;
        }


        var isLayer = new ISWrapper();
        var connectivityLevel=string.Empty;
        using (var proxy = isLayer.Proxy)
        {
            connectivityLevel = proxy.Cirrus.Settings.GetSetting(Settings.ConnectivityLevel, ConnectivityLevels.Default, null);
        }

        if (connectivityLevel.Equals(ConnectivityLevels.Level3, StringComparison.InvariantCultureIgnoreCase))
        {
            switch (Job.JobDefinition.JobType)
            {
                case eJobType.CommandScript:
                case eJobType.DownloadConfigs:
                case eJobType.Upload:
                case eJobType.Reboot:
                    lblSecurity.Visible = true;
                    lblSecurity.Text =
                        $@"{Resources.NCMWebContent.WEBDATA_IC_158}<br/><br/>{Resources.NCMWebContent.WEBDATA_IC_159} {Job.JobDefinition.OwnerSID}";                    
                    break;
                default:
                    lblSecurity.Visible = false;
                    break;

            }
        }
    }

    protected string MoreInformationLink
    {
        get
        {
            var helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
            return helpHelperWrap.GetHelpUrl("OrionNCMPHCronExpressions");
        }
    }

    private void Validate(NCMJob job)
    {
        switch (job.JobSchedule.TriggerType)
        {
            case NCMJobSchedule.eTriggerType.Once:
                if ((job.JobSchedule.StartDate - DateTime.Now).TotalMinutes < 15)
                {
                    IsValid = false;

                    var utcOffset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow).Hours;
                    var ncmServerTime = string.Format(Resources.NCMWebContent.WEBDATA_VK_878, utcOffset >= 0 ? @"+" : @"-", Math.Abs(utcOffset), DateTime.Now.ToShortTimeString());
                    ErrorMsg = $@"{Resources.NCMWebContent.WEBDATA_VM_216} {ncmServerTime}";
                }
                else
                {
                    IsValid = true;
                }
                break;
            default:
                IsValid = true;
                break;
        }
    }
}