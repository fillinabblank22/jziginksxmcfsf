﻿using System;
using Infragistics.WebUI.UltraWebNavigator;
using System.Data;
using SolarWinds.NCM.Common.Help;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_Resources_Jobs_Controls_PolicyReports : System.Web.UI.UserControl
{
    private UltraWebTree tree = new UltraWebTree();

    public string LearnMore
    {
        get
        {
            var helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
            return helpHelperWrap.GetHelpUrl("OrionNCMPHHelpChooseReportGenPolicyReport");
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        tree.ExpandImage = ResolveUrl("~/Orion/NCM/Resources/images/Button.Expand.gif");
        tree.CollapseImage = ResolveUrl("~/Orion/NCM/Resources/images/Button.Collapse.gif");
        tree.ClientSideEvents.InitializeTree = @"InitializeTree";
        treePanel.Controls.Add(tree);
    }

    public void LoadJob(NCMJob job)
    {
        BuildTree(job.JobDefinition.Parameters.ObjectID);
        chkSendOnError.Checked = job.JobDefinition.Parameters.NotifyOnlyOnPolicyViolations;
    }

    public bool UpdateJob(NCMJob job)
    {
        //validation
        if (tree.SelectedNode == null || tree.SelectedNode.DataPath.StartsWith(@"G:", StringComparison.CurrentCultureIgnoreCase))
        {
            cvValidateReportSelection.IsValid = false;
            return false;
        }

        //update job
        job.JobDefinition.Parameters.ObjectID = tree.SelectedNode.DataPath;
        job.JobDefinition.Parameters.NotifyOnlyOnPolicyViolations = chkSendOnError.Checked;
        return true;
    }

    private void BuildTree(string selectedReportID)
    {
        var isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            var dt = proxy.Query(@"Select Grouping, Name, PolicyReportID From Cirrus.PolicyReports Order By Grouping, Name");
            foreach (DataRow dr in dt.Rows)
            {
                var currentNode = new Node();

                currentNode.Text = dr["Name"].ToString();
                currentNode.DataPath = dr["PolicyReportID"].ToString();
                currentNode.ImageUrl = ResolveUrl("~/Orion/NCM/Resources/images/PolicyIcons/PolicyReport.Icon.gif");
                currentNode.SelectedImageUrl = ResolveUrl("~/Orion/NCM/Resources/images/PolicyIcons/SelectedPolicyReport.Icon.GIF");

                string groupName = dr["Grouping"].ToString();
                if (string.IsNullOrEmpty(groupName))
                {
                    groupName = Resources.NCMWebContent.NodeGroupText_Unknown;
                }

                Node parentNode = tree.Nodes.Search(groupName, null, null, false);
                if (parentNode == null)
                {
                    parentNode = new Node();
                    parentNode.Text = groupName;
                    parentNode.DataPath = $@"G:{groupName}";
                    parentNode.ImageUrl = ResolveUrl("~/Orion/NCM/Resources/images/PolicyIcons/folder.gif");
                    tree.Nodes.Add(parentNode);
                }

                parentNode.Nodes.Add(currentNode);

                if (currentNode.DataPath.Equals(selectedReportID, StringComparison.CurrentCultureIgnoreCase))
                {
                    parentNode.Expanded = true;
                    currentNode.Selected = true;
                }
            }
        }

        //build reports count in each group
        foreach (Node node in tree.Nodes)
        {
            node.Text = $@"{node.Text} ({node.Nodes.Count})";
        }
    }
}