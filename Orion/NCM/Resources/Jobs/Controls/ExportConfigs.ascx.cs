﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Linq;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;
using NCMContracts = SolarWinds.NCM.Contracts.Constants;
using SolarWinds.NCM.Common.Dal;

public partial class Orion_NCM_Resources_Jobs_Controls_ExportConfigs : System.Web.UI.UserControl
{
    private readonly Log log = new Log();
    private ISWrapper isLayer = new ISWrapper();

    protected List<FilePath> FilePathList { get; set; }

    protected string Path { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void LoadJob(NCMJob job)
    {
        var jobParams = job.JobDefinition.Parameters;

        Path = jobParams.Path;
        FilePathList = jobParams.FilePathList;
        txtConfigTemplate.Text = jobParams.ConfigTemplate;
        SelectedConfigs.LoadConfigs(jobParams.ConfigTypes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
        rbExportAllConfigs.Checked = jobParams.AllConfigs;
        rbExportLatestConfig.Checked = !jobParams.AllConfigs;

        repeater.DataSource = LookupEngines();
        repeater.DataBind();
    }

    public bool UpdateJob(NCMJob job)
    {
        List<FilePath> fpList;
        if (!TryGetFilePathList(out fpList)) return false;

        divConfigTypesEmpty.Visible = false;
        var configTypes = SelectedConfigs.GetSelectedConfigs();
        if (string.IsNullOrEmpty(configTypes))
        {
            divConfigTypesEmpty.Visible = true;
            return false;
        }

        var jobParams = job.JobDefinition.Parameters;

        jobParams.FilePathList = fpList;
        jobParams.ConfigTemplate = txtConfigTemplate.Text;
        if(string.IsNullOrEmpty(jobParams.ConfigTemplate))
            jobParams.ConfigTemplate = NCMContracts.DefaultExportConfigPath;
        jobParams.ConfigTypes = configTypes;
        jobParams.AllConfigs = rbExportAllConfigs.Checked;

        return true;
    }

    protected void repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        var engine = e.Item.DataItem as EngineInfo;
        if (engine != null)
        {
            var engineId = engine.EngineId;
            var txtEngineID = e.Item.FindControl(@"txtEngineID") as HiddenField;
            txtEngineID.Value = engineId.ToString();

            var litServerName = e.Item.FindControl(@"litServerName") as Literal;

            var engines = LookupEngines().ToArray();
            if (engines.Length == 1)
            {
                litServerName.Text = string.Empty;
            }
            else
            {
                var htmlFormat = @"<tr><td style='padding-top:10px;padding-bottom:10px;'><b>{0}</b></td></tr>";
                litServerName.Text = string.Format(htmlFormat, engine.EngineName);

                var tdDelimiter = e.Item.FindControl(@"tdDelimiter") as HtmlTableCell;
                if (e.Item.ItemIndex > 0) tdDelimiter.Style.Add(@"border-bottom", @"dashed 1px #dcdbd7");
            }
            
            var txtPath = e.Item.FindControl(@"txtPath") as TextBox;
            
            try
            {
                using (var proxy = isLayer.Proxy)
                {
                    var appDataPath = proxy.Cirrus.Settings.GetAppDataPath(engineId);
                    appDataPath = System.IO.Path.Combine(appDataPath, "NCM\\Config-Archive");

                    FilePath fp = null;
                    if (FilePathList != null)
                    {
                        if (FilePathList.Count > 0)
                        {
                            fp = FilePathList.Find(item => item.EngineID == engineId);
                            if (fp != null)
                            {
                                if (!string.IsNullOrEmpty(fp.File_Path))
                                    txtPath.Text = fp.File_Path;
                                else
                                    txtPath.Text = appDataPath;
                            }
                            else
                                txtPath.Text = appDataPath;
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(Path))
                                txtPath.Text = Path;
                            else
                                txtPath.Text = appDataPath;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Path))
                            txtPath.Text = Path;
                        else
                            txtPath.Text = appDataPath;
                    }
                }
            }
            catch (Exception ex)
            {
                engineId = -1;
                txtEngineID.Value = engineId.ToString();

                txtPath.Enabled = false;
                txtPath.CssClass = @"TextFieldDisabled";

                var btnValidate = e.Item.FindControl(@"btnValidate") as LocalizableButton;
                btnValidate.Enabled = false;
                btnValidate.CssClass = @"ButtonDisabled";

                var trException = e.Item.FindControl(@"trException") as HtmlTableRow;
                trException.Visible = true;

                var litException = e.Item.FindControl(@"litException") as Literal;
                litException.Text = ex.Message;
            }
        }
    }

    protected void repeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName.Equals(@"Validate"))
        {
            var divValidator = e.Item.FindControl(@"divValidator") as HtmlGenericControl;
            var txtEngineID = e.Item.FindControl(@"txtEngineID") as HiddenField;
            var txtPath = e.Item.FindControl(@"txtPath") as TextBox;

            string error;
            if (!ValidatePath(txtPath.Text, Convert.ToInt16(txtEngineID.Value), out error))
            {
                var literal = new Literal();
                literal.Text = string.IsNullOrEmpty(error) ? Resources.NCMWebContent.WEBDATA_VK_595 : error;
                divValidator.Style.Add(@"color", @"red");
                divValidator.Controls.Add(literal);
            }
            else
            {
                var literal = new Literal();
                literal.Text = Resources.NCMWebContent.WEBDATA_VK_597;
                divValidator.Style.Add(@"color", @"green");
                divValidator.Controls.Add(literal);
            }
        }
    }

    private bool TryGetFilePathList(out List<FilePath> fpList)
    {
        var isValid = true;
        fpList = new List<FilePath>();

        foreach (RepeaterItem item in repeater.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                var txtEngineID = item.FindControl(@"txtEngineID") as HiddenField;
                var engineId = txtEngineID.Value;
                if (Convert.ToInt16(engineId) < 0) continue;

                var txtPath = item.FindControl(@"txtPath") as TextBox;
                var path = txtPath.Text;

                string error;
                if (!ValidatePath(path, Convert.ToInt16(engineId), out error))
                {
                    var literal = new Literal();
                    literal.Text = string.IsNullOrEmpty(error) ? Resources.NCMWebContent.WEBDATA_VK_595 : error;
                    
                    var divValidator = item.FindControl(@"divValidator") as HtmlGenericControl;
                    divValidator.Style.Add(@"color", @"red");
                    divValidator.Controls.Add(literal);
                    
                    isValid = false;
                }

                if (isValid)
                {
                    var fp = new FilePath();
                    fp.EngineID = Convert.ToInt16(engineId);
                    fp.File_Path = path;
                    fpList.Add(fp);
                }
            }
        }

        return isValid;
    }

    private bool ValidatePath(string path, int engineId, out string error)
    {
        try
        {
            using (var proxy = isLayer.Proxy)
            {
                error = string.Empty;
                return proxy.Cirrus.Settings.ValidatePath(path, engineId);
            }
        }
        catch (Exception ex)
        {
            error = ex.Message;
            log.Error(ex);
            return false;
        }
    }

    private IEnumerable<EngineInfo> LookupEngines()
    {
        var engineDal = ServiceLocator.Container.Resolve<ISwisEnginesDal>();
        return engineDal.GetAllPollingEngines();
    }
}