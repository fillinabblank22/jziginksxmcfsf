﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RoutineDatabaseMaintenance.ascx.cs" Inherits="Orion_NCM_Controls_Jobs_RoutineDatabaseMaintenance" %>

<link rel="stylesheet" type="text/css" href="~/orion/ncm/admin/settings/settings.css" />

<script type="text/javascript">
    $(document).ready(function () {
        EnablePurgeJobLogOptions(document.getElementById("<%=cbJobLog.ClientID %>").checked);
    });

    function EnablePurgeJobLogOptions(checked) {
        $('span.PurgeJobLogOption input').each(function () {
            this.disabled = !checked;
        });
    }

    function CkeckPurgeJobLogOption(controlId) {
        $("[id$='_" + controlId + "']").prop("checked", "checked");
    }
</script>

<div class="SettingContainer">
    <div class="SettingBlockDescription" >
        <%=Resources.NCMWebContent.WEBDATA_VM_183%>
    </div>
    <div id="optionsContainer" style="padding-top: 40px">
        <div>
            <asp:CheckBox ID="cbCfgArchive" Text="<%$ Resources: NCMWebContent, WEBDATA_VM_184 %>" runat="server"/>
            <asp:DropDownList ID="ddlCfgArchiveTimeRange" runat="server"></asp:DropDownList>
        </div>
        <div style="padding-top:20px;">
            <asp:CheckBox ID="cbCfgCache" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VM_185 %>"/>
            <asp:DropDownList ID="ddlCfgCacheTimeRange" runat="server"></asp:DropDownList><span style="font-size:8pt;padding-left:25px;">&#0187;&nbsp;<a style="color:#336699;" href="<%=HelpLink %>" target="_blank"><%=Resources.NCMWebContent.WEBDATA_VY_86%></a></span>
        </div>
        <div style="padding-top:20px;">
            <asp:CheckBox ID="cbApprovalRequests" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_844 %>" runat="server"/>
            <asp:DropDownList ID="ddlApprovalRequestsTimeRange" runat="server"></asp:DropDownList>
        </div>
        <div style="padding-top:20px;">
            <asp:CheckBox ID="cbJobLog" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_978 %>" onclick="EnablePurgeJobLogOptions(this.checked);" runat="server" />
            <span style="padding-left:10px;">
                <asp:RadioButton ID="rbJobLogOlderThan" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_979 %>" CssClass="PurgeJobLogOption" runat="server" GroupName="PurgeJobLog" />
                <span style="padding-left:5px;">
                    <asp:DropDownList ID="ddlJobLogTimeRange" runat="server" onclick="CkeckPurgeJobLogOption('rbJobLogOlderThan');"></asp:DropDownList>
                </span>
            </span>
            <span style="padding-left:20px;">
                <asp:RadioButton ID="rbJobLogGreaterThan" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_980 %>" CssClass="PurgeJobLogOption" runat="server" GroupName="PurgeJobLog" />
                <span style="padding-left:5px;">
                    <asp:DropDownList ID="ddlJobLogSize" runat="server" onclick="CkeckPurgeJobLogOption('rbJobLogGreaterThan');"></asp:DropDownList>
                </span>
            </span>
        </div>
        <div style="padding-top:20px;">
            <asp:CheckBox ID="cbTransferRequests" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_603 %>" runat="server"/>
            <asp:DropDownList ID="ddlTransferRequestsTimeRange" runat="server"></asp:DropDownList>
        </div>
    </div>
</div>
