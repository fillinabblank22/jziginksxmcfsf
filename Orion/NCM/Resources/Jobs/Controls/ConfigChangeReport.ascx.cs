﻿using System;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCM.Contracts.Jobs;

public partial class Orion_NCM_Resources_Jobs_Controls_ConfigChangeReport : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void LoadJob(NCMJob job)
    {
        JobParameters jobParams = job.JobDefinition.Parameters;

        switch (jobParams.ChangeReportType)
        {
            case ConfigChangeReportType.Running:
            case ConfigChangeReportType.Startup:
            case ConfigChangeReportType.Baseline:
            case ConfigChangeReportType.Last:
                rbCompareToLast.Checked = true;
                break;
            case ConfigChangeReportType.TargetDate:
                rbCompareToTargetDate.Checked = true;
                break;
            case ConfigChangeReportType.PastXXDays:
                rbCompareToPastXXDays.Checked = true;
                break;
            case ConfigChangeReportType.DateRange:
                rbCompareToDateRange.Checked = true;
                break;
        }

        ddlLastConfigType.SelectedValue = jobParams.LastConfigType;
        txtTargetDate.TimeSpan = jobParams.ChangeReportTargetDate == DateTime.MinValue ? DateTime.Now.AddDays(-7) : jobParams.ChangeReportTargetDate;
        txtPastXXDays.Text = Convert.ToString(jobParams.ChangeReportPastXXDays);
        chkCompareLastDownloaded.Checked = jobParams.CompareLastDownloaded > 0;
        txtStartDate.TimeSpan = jobParams.ChangeReportStartDate == DateTime.MinValue ? DateTime.Now.AddDays(-7) : jobParams.ChangeReportStartDate;
        txtEndDate.TimeSpan = jobParams.ChangeReportEndDate == DateTime.MinValue ? DateTime.Now : jobParams.ChangeReportEndDate;
        chkGenerateReportOnConfigType.Checked = jobParams.GenerateOnConfigType;
        ddlGenerateReportOnConfigType.SelectedValue = jobParams.OnConfigType;
        chkOnlySendDevicesThatHadChanges.Checked = jobParams.OnlySendDevicesThatHadChanges;
    }

    public bool UpdateJob(NCMJob job)
    {
        JobParameters jobParams = job.JobDefinition.Parameters;

        jobParams.CompareLastDownloaded = -1;
        if (rbCompareToLast.Checked)
        {
            jobParams.ChangeReportType = ConfigChangeReportType.Last;
            jobParams.ChangeReportDetails = false;
            jobParams.GenerateOnConfigType = false;
        }
        else if (rbCompareToTargetDate.Checked)
        {
            if (!txtTargetDate.ValidateDate()) return false;

            jobParams.ChangeReportType = ConfigChangeReportType.TargetDate;
            jobParams.ChangeReportDetails = false;
            jobParams.GenerateOnConfigType = chkGenerateReportOnConfigType.Checked;
        }
        else if (rbCompareToPastXXDays.Checked)
        {
            jobParams.ChangeReportType = ConfigChangeReportType.PastXXDays;
            jobParams.ChangeReportDetails = true;
            jobParams.CompareLastDownloaded = chkCompareLastDownloaded.Checked ? 1 : 0;
            jobParams.GenerateOnConfigType = chkGenerateReportOnConfigType.Checked;
        }
        else
        {
            if (!txtStartDate.ValidateDate() || !txtEndDate.ValidateDate()) return false;

            jobParams.ChangeReportType = ConfigChangeReportType.DateRange;
            jobParams.ChangeReportDetails = true;
            jobParams.GenerateOnConfigType = chkGenerateReportOnConfigType.Checked;
        }
        jobParams.LastConfigType = ddlLastConfigType.SelectedValue;
        jobParams.ChangeReportTargetDate = txtTargetDate.TimeSpan;
        jobParams.ChangeReportPastXXDays = Convert.ToInt32(txtPastXXDays.Text);
        jobParams.ChangeReportStartDate = txtStartDate.TimeSpan;
        jobParams.ChangeReportEndDate = txtEndDate.TimeSpan;
        jobParams.OnConfigType = chkGenerateReportOnConfigType.Checked ? ddlGenerateReportOnConfigType.SelectedValue : string.Empty;
        jobParams.OnlySendDevicesThatHadChanges = chkOnlySendDevicesThatHadChanges.Checked;

        return true;
    }
}