﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Web;
using Resources;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCM.Web.Contracts;

public partial class Orion_NCM_Resources_Jobs_Controls_Upload : UserControl
{
    private readonly IIsWrapper isLayer;
    private readonly IMacroParser macroParser;
    private AdvToobarNodes toolBarNodes = new AdvToobarNodes();

    public Orion_NCM_Resources_Jobs_Controls_Upload()
    {
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
        macroParser = ServiceLocator.Container.Resolve<IMacroParser>();
    }

    private string ConfigFileName
    {
        get { return txtConfig.Attributes[@"ConfigFileName"]; }
        set { txtConfig.Attributes[@"ConfigFileName"] = value; }
    }


    protected override void OnInit(EventArgs e)
    {
        var resourceSettings = new ResourceSettings();
        resourceSettings.Prefix = "UploadPicker";
        NPMDefaultSettingsHelper.SetUserSettings("UploadPickerGroupBy", "Vendor");
        
        toolBarNodes.ResourceSettings = resourceSettings;
        toolBarNodes.GroupByChangeEvent = @"RefreshSingleCheckedNode();";
        toolBarNodes.Visible = false;
        toolBarNodes.ShowClearAll = false;
        toolBarNodes.ShowSelectAll = false;
        toolBarNodes.RemoveNodes();
        SelectConfigContainer.Controls.Add(toolBarNodes);

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (btnSelectFile.HasFile)
        {
            var reader = new StreamReader(btnSelectFile.FileContent);
            txtConfig.Text = reader.ReadToEnd(); // XSS safe. TextBoxes already apply encoding
            reader.Close();
            txtConfig.Enabled = true;
            ConfigFileName = string.Empty;
        }

        toolBarNodes.NodesControl.ShowConfigs = true;
        toolBarNodes.NodesControl.ShowConfigCheckBoxes = true;
        toolBarNodes.NodesControl.ExcludeUnmanagedNodes = true;
        toolBarNodes.NodesControl.TreeControl.ClientSideEvents.NodeChecked = @"UltraWebTree_UploadSingleNodeChecked";

        if (!Page.IsPostBack)
        {
            RefreshScriptList();
        }
    }

    public void LoadJob(NCMJob job)
    {
        var jobParams = job.JobDefinition.Parameters;

        var enable = EnableConfigTypeUploadIfNeeded(job);

        if (CommonHelper.IsBinaryConfigByContent(jobParams.ConfigText))
        {
            txtConfig.Text = NCMWebContent.WEBDATA_AP_3;
            txtConfig.Enabled = false;
            ConfigFileName = jobParams.ConfigText;
        }
        else
        {
            txtConfig.Text = jobParams.ConfigText;
            txtConfig.Enabled = true;
            ConfigFileName = string.Empty;
        }


        WriteToNVRAMDIV.Visible = !enable;
        chkWriteToNVRAM.Checked = !enable && jobParams.WriteToNVRAM;
        ddlConfigType.SelectedValue = job.JobDefinition.Parameters.ConfigTypes;
        ConfigTypeUploadDIV.Visible = enable;
    }

    public bool UpdateJob(NCMJob job)
    {
        var jobParams = job.JobDefinition.Parameters;

        var enable = EnableConfigTypeUploadIfNeeded(job);

        var isBinary = !string.IsNullOrEmpty(ConfigFileName);
        jobParams.ConfigText = isBinary ? ConfigFileName : txtConfig.Text;
        
        jobParams.WriteToNVRAM = chkWriteToNVRAM.Checked;
        job.JobDefinition.Parameters.ConfigTypes = enable ? ddlConfigType.SelectedValue : string.Empty;

        return true;
    }

    private bool EnableConfigTypeUploadIfNeeded(NCMJob job)
    {
        var count = JobHelper.GetSelectedNodesCount(job, true);
        if (count == 1)
        {
            var swqlFormat = @"
SELECT 
    CirrusNodes.NodeID, 
    CirrusNodes.CommandProtocol, 
    CirrusNodes.TransferProtocol, 
    CirrusNodes.ConnectionProfile 
FROM Orion.Nodes AS OrionNodes 
INNER JOIN 
    (SELECT 
        NodeID, 
        CoreNodeID, 
        CommandProtocol, 
        TransferProtocol, 
        ConnectionProfile 
    FROM Cirrus.Nodes {0}) AS CirrusNodes 
ON OrionNodes.NodeID=CirrusNodes.CoreNodeID";

            var whereClause = string.Empty;
            switch (job.JobDefinition.Nodes.SelectedCriteriaType)
            {
                case eCriteriaType.AllNodes:
                    whereClause = string.Empty;
                    break;
                case eCriteriaType.DesktopCriteria:
                    whereClause = job.JobDefinition.Nodes.QueryString;
                    break;
                case eCriteriaType.SelectedNodes:
                    whereClause = $@"WHERE NodeID='{job.JobDefinition.Nodes.NetworkNodes[0].NodeID}'";
                    break;
                case eCriteriaType.WebCriteria:
                    whereClause = job.JobDefinition.Nodes.QueryString;
                    break;
            }

            using (var proxy = isLayer.GetProxy())
            {
                var dt = proxy.Query(string.Format(swqlFormat, whereClause));
                if (dt != null && dt.Rows.Count > 0)
                {
                    var nodeId = Guid.Parse(dt.Rows[0]["NodeID"].ToString());
                    var connProfileID = Convert.ToInt32(dt.Rows[0]["ConnectionProfile"].ToString());

                    string commandProtocol;
                    string transferProtocol;

                    if (connProfileID > 0)
                    {
                        var connProfile = proxy.Cirrus.Nodes.GetConnectionProfile(connProfileID);
                        commandProtocol = connProfile.RequestConfigProtocol;
                        transferProtocol = connProfile.TransferConfigProtocol;
                    }
                    else
                    {
                        commandProtocol = dt.Rows[0]["CommandProtocol"].ToString();
                        transferProtocol = dt.Rows[0]["TransferProtocol"].ToString();
                    }

                    var parsedCommandProtocol = macroParser.ParseMacro(nodeId, commandProtocol);
                    var parsedTransferProtocol = macroParser.ParseMacro(nodeId, transferProtocol);
                    if (!parsedCommandProtocol.Equals(@"SNMP", StringComparison.InvariantCultureIgnoreCase) &&
                        (parsedTransferProtocol.Equals(@"TFTP", StringComparison.InvariantCultureIgnoreCase) || parsedTransferProtocol.Equals(@"SCP", StringComparison.InvariantCultureIgnoreCase)))
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private string GetConfigText(string configId, string entityName)
    {
        if (!string.IsNullOrEmpty(configId))
        {
            using (var proxy = isLayer.GetProxy())
            {
                var dt = proxy.Query(string.Format(@"SELECT C.Config FROM {1} AS C WHERE C.ConfigID='{0}'", configId, entityName));
                if (dt != null && dt.Rows.Count > 0)
                {
                    return Convert.ToString(dt.Rows[0][0]);
                }
            }
        }

        return string.Empty;
    }

    private bool IsBinaryConfig(string configId, out string config)
    {
        if (!string.IsNullOrEmpty(configId))
        {
            using (var proxy = isLayer.GetProxy())
            {
                var dt = proxy.Query(
                    $@"SELECT C.Config FROM Cirrus.ConfigArchive AS C WHERE C.ConfigID='{configId}' AND IsBinary=1");
                if (dt != null && dt.Rows.Count > 0)
                {
                    config = Convert.ToString(dt.Rows[0]["Config"]);
                    return true;
                }
            }
        }

        config = null;
        return false;

    }

    private void RefreshScriptList()
    {
        using (var proxy = isLayer.GetProxy())
        {
            var dt = proxy.Query(@"SELECT C.ConfigID, C.ConfigTitle, C.Comments FROM Cirrus.SnippetArchive AS C ORDER BY C.DownloadTime");
            repeater.DataSource = dt;
            repeater.DataBind();
        }
    }

    protected void repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var row = e.Item.DataItem as DataRowView;
            var ctrl = e.Item.FindControl(@"rb");
            if (ctrl != null)
            {
                (ctrl as RadioButton).Text =
                    $@"<span style='padding-left:5px;'><img src='/Orion/NCM/Resources/images/ConfigIcons/Config-Edited.gif' style='vertical-align:middle;'/></span><span style='padding-left:5px;'>{HttpUtility.HtmlEncode(row["ConfigTitle"])}<span/>";
                (ctrl as RadioButton).Attributes.Add(@"value", Convert.ToString(row["ConfigID"]));
            }
        }
    }

    protected void ShowSelectConfigModalPopup(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), @"refresh_single_node_state", @"RefreshSingleCheckedNode();", true);
        toolBarNodes.Visible = true;
        toolBarNodes.RemoveNodes();

        SelectConfigModalPopup.Show();
    }

    protected void SubmitSelectConfigModalPopup(object sender, EventArgs e)
    {
        SelectConfigModalPopup.Hide();

        var configId = toolBarNodes.NodesControl.GetConfigID();

        string config;
        if (IsBinaryConfig(configId, out config))
        {
            txtConfig.Text = NCMWebContent.WEBDATA_AP_3;
            txtConfig.Enabled = false;
            ConfigFileName = config;
        }
        else
        {
            txtConfig.Text = GetConfigText(configId, @"Cirrus.ConfigArchive");
            txtConfig.Enabled = true;
            ConfigFileName = string.Empty;
        }

        ConfigUpdatePanel.Update();
    }

    protected void CloseSelectConfigModalPopup(object sender, EventArgs e)
    {
        SelectConfigModalPopup.Hide();
    }

    protected void ShowSelectScriptModalPopup(object sender, EventArgs e)
    {
        SelectScriptModalPopup.Show();
    }

    protected void SubmitSelectScriptModalPopup(object sender, EventArgs e)
    {
        SelectScriptModalPopup.Hide();

        foreach (RepeaterItem item in repeater.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                var ctrl = item.FindControl(@"rb");
                if (ctrl != null)
                {
                    if ((ctrl as RadioButton).Checked)
                    {
                        var configId = (ctrl as RadioButton).Attributes[@"value"];

                        var isBinary = !string.IsNullOrEmpty(ConfigFileName);

                        if (isBinary)
                        {
                            txtConfig.Text = GetConfigText(configId, @"Cirrus.SnippetArchive");
                            txtConfig.Enabled = true;
                            ConfigFileName = string.Empty;
                        }
                        else
                        {
                            txtConfig.Text += GetConfigText(configId, @"Cirrus.SnippetArchive");
                        }
                        ConfigUpdatePanel.Update();
                        return;
                    }
                }
            }
        }
    }

    protected void CloseSelectScriptModalPopup(object sender, EventArgs e)
    {
        SelectScriptModalPopup.Hide();
    }
}