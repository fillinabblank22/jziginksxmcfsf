﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Review.ascx.cs" Inherits="Orion_NCM_Resources_Jobs_Controls_Review" %>
  <style type="text/css">
      .ReviewValue 
      {
      	padding-top:10px;
      	padding-left:50px;
      	width:90%;
      }
      
      .ReviewName 
      {
      	padding-top:10px;
      	white-space:nowrap;
      	width:10%;
      }
  </style>


  <table style="padding-bottom:5px; padding-bottom:20px" cellpadding="0" cellspacing="0" width="100%">
      <tr>         
          <td class="ReviewName">
            <b><%=Resources.NCMWebContent.WEBDATA_IC_141%></b>
          </td>
          <td class="ReviewValue">
            <asp:Literal ID="lblJobname" Mode="Encode" runat="server" />
          </td>
       </tr>
       <tr>
           <td class="ReviewName">
            <b><%=Resources.NCMWebContent.WEBDATA_IC_142%></b>
          </td>
          <td class="ReviewValue">
            <asp:Label ID="lblJobType" runat="server" />
          </td>
      </tr>

      <tr>
           <td class="ReviewName">
            <b><%=Resources.NCMWebContent.WEBDATA_IC_143%></b>
          </td>
          <td class="ReviewValue">
            <asp:Label ID="lblFrequency" runat="server" />
          </td>
      </tr>

      <tr>
           <td class="ReviewName">
            <b><%=Resources.NCMWebContent.WEBDATA_IC_144%></b>
          </td>
          <td class="ReviewValue">
            <asp:Label ID="lblRunTime" runat="server" />
            <span id="cronHelpLink" runat="server" style="font-size:8pt;"><a style="color:#336699;" href="<%=MoreInformationLink %>" target="_blank">&nbsp;&#0187;&nbsp; <%=Resources.NCMWebContent.WEBDATA_VM_214 %></a></span>
          </td>
      </tr>

      <tr>
           <td class="ReviewName" style="vertical-align:top">
            <b><%=Resources.NCMWebContent.WEBDATA_IC_145%></b>
          </td>
          <td class="ReviewValue">
            <asp:Literal ID="lblComments" Mode="Encode" runat="server"  />
          </td>
      </tr>
      <tr>
         <td style="border-bottom:#e6e6dd 1px solid; padding-top:20px" colspan="2"></td>
      </tr>
       <tr>
           <td class="ReviewName">
              <b><%=Resources.NCMWebContent.WEBDATA_IC_146%></b>
           </td>
           <td class="ReviewValue">
              <asp:Label ID="lblSelectedNodes" runat="server" />
            </td>
        </tr>
        <tr>
              <td style="border-bottom:#e6e6dd 1px solid; padding-top:20px" colspan="2"></td>
        </tr>
         <tr>
           <td class="ReviewName">
              <b><%=Resources.NCMWebContent.WEBDATA_IC_147%></b>
           </td>
           <td class="ReviewValue">
              <asp:Label ID="lblEmailNotification" runat="server" />
            </td>
        </tr>

      </table>

      <div style="padding-top:20px">
            <asp:Label ID="lblSecurity" runat="server" Visible="false" />
      </div>

   