﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ChooseNodes.ascx.cs" Inherits="Orion_NCM_Resources_Jobs_Controls_ChooseNodes" %>

<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.PolicyReportsManagement.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register TagPrefix="ncm" TagName="ModalPopup" Src="~/Orion/NCM/Controls/ModalPopup.ascx" %>
<%@ Register TagPrefix="ncm" TagName="AddRemoveNodesControl" Src="~/Orion/NCM/Controls/AddRemoveNodesControl.ascx"  %>

<script type="text/javascript" src="/Orion/NCM/JavaScript/main.js"></script>

<link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
<link rel="stylesheet" type="text/css" href="/Orion/NCM/Resources/Jobs/Jobs.css" />

<style type="text/css">
    .autocomplete_completionListElement 
    {  
        visibility : hidden;
        margin : 0px !Important;
        padding: 0px !Important;
        background-color : #fff;
        color : #000;
        border : #ccc;
        border-width : 1px;
        border-style : solid;
        cursor : default;
        overflow : auto;
        height : 100px;
        text-align : left;
        font-size : 9pt;
        list-style: none !Important;
        width: 201px !Important;
        z-index: 9999999;
    }
    
    .autocomplete_completionListElement li
    {
        display: block;
        padding: 1px !Important;
        margin: 0px;
    }
    
    .autocomplete_highlightedListItem
    {
        background-color: Highlight;
        color: white;
        padding: 1px;
    }
    
    .autocomplete_listItem 
    {
        background-color: window;
        color: windowtext;
        padding: 1px;
    }
    
    .pagingToolbar { border: 0 none !important; background-color : #e2e1d4 }
    .pagingToolbar td {  border: 0 none !important; padding: 0 2px; }
    .pagingToolbar img { vertical-align: top !important; }
    .pagingToolbar a { white-space: nowrap; font-size: 8pt; }
    .pagingToolbar .disabled { color: silver; }
    .pagingToolbar .spacer { width: 18px; height: 15px; background: url('/Orion/NCM/Resources/images/ConfigChangeApproval/arrows.gif') no-repeat scroll 0 0 transparent; }
    .pagingToolbar .enabled .prev { background-position: 0px 0px !important; }
    .pagingToolbar .enabled .next { background-position: 0px -15px !important; }
    .pagingToolbar .disabled .prev { background-position: -18px 0px !important; }
    .pagingToolbar .disabled .next { background-position: -18px -15px !important; }
</style>

<script type="text/javascript">
    function showTooltip(on, id) {
        var div = document.getElementById(id);
        if (on) {
            div.style.display = 'block';
        }
        else {
            div.style.display = 'none';
        }
    }
</script>

<div>
    <asp:Panel ID="ControlPanel" runat="server">
        <div class="JobHeader"><%=Resources.NCMWebContent.WEBDATA_VK_787 %></div>
        <div style="padding-top:10px;"><%=Resources.NCMWebContent.WEBDATA_VK_788 %></div>
        <div>
            <asp:UpdatePanel ID="ContentUpdatePanel" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <table>
                        <tr>
                            <td style="padding-top:10px;">
                                <asp:RadioButton ID="rbSelectNodes" runat="server" GroupName="NodeSelectionGroup" AutoPostBack="true" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_790 %>" OnCheckedChanged="NodeSelectionChanged" />
                            </td>
                            <td style="padding-top:10px;padding-left:40px;">
                                <asp:RadioButton ID="rbAllNodes" runat="server" GroupName="NodeSelectionGroup" AutoPostBack="true" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_789 %>" OnCheckedChanged="NodeSelectionChanged" />
                            </td>
                            <td style="padding-top:10px;padding-left:40px;">
                                <asp:RadioButton ID="rbDynamicSelection" runat="server" GroupName="NodeSelectionGroup" AutoPostBack="true" OnCheckedChanged="NodeSelectionChanged" />
                            </td>
                            <td style="padding-top:10px;padding-left:40px;">
                                <div id="divDesktopSelectionLabel" runat="server">
                                    <asp:RadioButton ID="rbDesktopSelection" runat="server" GroupName="NodeSelectionGroup" AutoPostBack="true" OnCheckedChanged="NodeSelectionChanged" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="padding-top:20px;">
                                <div id="divSelectNodes" runat="server">
                                    <ncm:AddRemoveNodesControl ID="AddRemoveNodesControl" runat="server" Prefix="NCM_Jobs_ChooseNodes"/>
                                    <div id="divAddRemoveNodesValidator" runat="server" visible="false" style="color:red;padding-top:10px;">
                                        <%=Resources.NCMWebContent.WEBDATA_VK_883 %>
                                    </div>
                                </div>

                                <div id="divDynamicSelection" runat="server">
                                    <ncm:DynamicSelection ID="DynamicSelection" runat="server" HideUnmanageNodes="true" />
                                </div>

                                <div id="divDesktopSelectionQuery" runat="server">
                                    <asp:TextBox ID="DesktopSelection" runat="server" 
                                        TextMode="MultiLine" 
                                        BackColor="Transparent" 
                                        BorderWidth="0px" 
                                        Width="700px" 
                                        Height="200px"
                                        Font-Names="Arial,Verdana,Helvetica,sans-serif"
                                        spellcheck="false"
                                        Wrap="false"
                                        ReadOnly="true"
                                        Font-Size="9pt" style="overflow:auto;resize:none;">
                                    </asp:TextBox>
                                </div>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </asp:Panel>

    <ncm:ModalPopup runat="server" ID="NodeSelectionWarningDialog" Title="<%$ Resources: NCMWebContent, WEBDATA_VK_791 %>" Width="400" BodyStyle="" ButtonAlign="center">
        <DialogContent>
            <table>
                <tr>
                    <td><img src="/Orion/NCM/Resources/images/warning_32x29.gif" /></td>
                    <td style="padding-left:10px;"><%=Resources.NCMWebContent.WEBDATA_VK_793 %></td>
                </tr>
                <tr>
                    <td></td>
                    <td style="padding-top:10px;padding-left:10px;"><a href="<%=MoreInformationLink %>" target="_blank" style="text-decoration:underline"><%=Resources.NCMWebContent.WEBDATA_VK_794 %></a></td>
                </tr>
            </table>
        </DialogContent>        
        <SubmitButtonContent>
            <orion:LocalizableButton ID="btnShowNodeSelectionDlg" runat="server" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_792 %>" DisplayType="Small" OnClick="ChangeNodeSelectionClick"/> 
        </SubmitButtonContent>
        <CancelButtonContent>
            <orion:LocalizableButton ID="btnCancelNodeSelectionDlg" runat="server" LocalizedText="Cancel" DisplayType="Small" OnClick="CancelNodeSelectionClick"/>
        </CancelButtonContent>
    </ncm:ModalPopup>
</div>
