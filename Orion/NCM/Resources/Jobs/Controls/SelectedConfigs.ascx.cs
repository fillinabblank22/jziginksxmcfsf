﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Data;
using SolarWinds.NCM.Common.Dal.ConfigTypes;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_Admin_Jobs_Controls_SelectedConfigs : System.Web.UI.UserControl
{
    private readonly IConfigTypesProvider configTypesProvider;

    public Orion_NCM_Admin_Jobs_Controls_SelectedConfigs()
    {
        configTypesProvider = ServiceLocator.Container.Resolve<IConfigTypesProvider>();
    }

    public void LoadConfigs(string[] selectedConfigTypes)
    {
        var dt = new DataTable();
        dt.Columns.Add("IsSelected");
        dt.Columns.Add("ConfigType");

        foreach (var configType in configTypesProvider.GetAll())
        {
            bool isSelected = selectedConfigTypes.Contains(configType.Name, StringComparer.InvariantCultureIgnoreCase);
            dt.Rows.Add(isSelected, configType.Name);
        }

        ListView.DataSource = dt;
        ListView.DataBind();
    }

    public string GetSelectedConfigs()
    {
        string selectedConfigs = string.Empty;
        foreach (var item in ListView.Items)
        {
            var cb = (CheckBox)item.FindControl(@"chkSelectedConfig");
            var lb = (Label)item.FindControl(@"lConfigName");
            if (cb.Checked)
            {
                selectedConfigs = $@"{selectedConfigs}{lb.Text},";
            }
        }
        return selectedConfigs.TrimEnd(',');
    }
}