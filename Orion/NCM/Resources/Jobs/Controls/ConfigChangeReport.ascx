﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConfigChangeReport.ascx.cs" Inherits="Orion_NCM_Resources_Jobs_Controls_ConfigChangeReport" %>

<%@ Register Assembly="Infragistics2.WebUI.WebDataInput.v8.2, Version=8.2.20082.1000, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.WebDataInput" TagPrefix="igtxt" %><%@ Register Assembly="Infragistics2.WebUI.WebDataInput.v8.2, Version=8.2.20082.1000, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.WebDataInput" TagPrefix="igtxt" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources" Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
<link rel="stylesheet" type="text/css" href="/Orion/NCM/Resources/Jobs/Jobs.css" />

<style type="text/css">   
    .ajax__calendar_days table thead tr td
    {
        border: 0px;
        background-color: white;	
        text-transform: none;
    }
    
    .boldText
    {
    	font-weight: bold;
    }
    
    .Active:hover
	{
		cursor:pointer;
	}
</style>

<script type="text/javascript">
    $(document).ready(function () {
        var checked = document.getElementById("<%=rbCompareToLast.ClientID %>").checked;
        ShowHideGenerateReportOnConfigTypeDIV(checked);
    });

    function ShowHideGenerateReportOnConfigTypeDIV(checked) 
    {
        if (checked) 
        {
            $('#GenerateReportOnConfigTypeDIV').hide();
        }
        else 
        {
            $('#GenerateReportOnConfigTypeDIV').show();
        }
    }

    function CkeckConfigChangeReportType(id, checked) {
        $("[id$='_" + id + "']").click();
        ShowHideGenerateReportOnConfigTypeDIV(checked);
    }
</script>

<div>
    <div class="JobHeader"><%=Resources.NCMWebContent.WEBDATA_VK_773 %></div>
    <div style="padding-top:10px;">
        <asp:RadioButton ID="rbCompareToLast" runat="server" GroupName="ConfigChangeReportGroup" onclick="ShowHideGenerateReportOnConfigTypeDIV(this.checked);" />
        <span class="Active" onclick="CkeckConfigChangeReportType('rbCompareToLast', true);"><%=string.Format(Resources.NCMWebContent.WEBDATA_VK_757, @"<b>", @"</b>") %></span>
        <div style="padding-top:10px;padding-left:25px;">
            <ncm:ConfigTypes ID="ddlLastConfigType" runat="server" ShowBaseline="true" CssClass="SelectField"></ncm:ConfigTypes>
        </div>
        <div class="Hint JobConfigTypesHint" style="padding-left:25px;"><%=Resources.NCMWebContent.WEBDATA_VY_92%></div>
    </div>

    <div style="padding-top:50px;">
        <asp:RadioButton ID="rbCompareToTargetDate" runat="server" GroupName="ConfigChangeReportGroup" onclick="ShowHideGenerateReportOnConfigTypeDIV(false);" />
        <span class="Active" onclick="CkeckConfigChangeReportType('rbCompareToTargetDate', false);"><%=string.Format(Resources.NCMWebContent.WEBDATA_VK_758, @"<b>", @"</b>") %></span>
        <div style="padding-top:10px;padding-left:25px;width:1%;">
            <ncm:Calendar ID="txtTargetDate" runat="server" CalendarTextBoxCssClass="TextField" AlternativeDrawingMode="false" DisplayTime="false" />
        </div>
    </div>

    <div style="padding-top:50px;">
        <asp:RadioButton ID="rbCompareToPastXXDays" runat="server" GroupName="ConfigChangeReportGroup" onclick="ShowHideGenerateReportOnConfigTypeDIV(false);" />
        <span class="Active" onclick="CkeckConfigChangeReportType('rbCompareToPastXXDays', false);"><%=Resources.NCMWebContent.WEBDATA_VK_759 %> <igtxt:WebNumericEdit ID="txtPastXXDays" runat="server" CssClass="TextField" HorizontalAlign="Center" Width="30" MaxValue="999" MinValue="1" MaxLength="3" Nullable="false"></igtxt:WebNumericEdit> <%=Resources.NCMWebContent.WEBDATA_VK_760 %></span>
        <div style="padding-top:10px;padding-left:25px;">
            <asp:CheckBox ID="chkCompareLastDownloaded" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_762 %>" />
        </div>
    </div>

    <div style="padding-top:50px;">
        <asp:RadioButton ID="rbCompareToDateRange" runat="server" GroupName="ConfigChangeReportGroup" onclick="ShowHideGenerateReportOnConfigTypeDIV(false);" />
        <span class="Active" onclick="CkeckConfigChangeReportType('rbCompareToDateRange', false);"><%=Resources.NCMWebContent.WEBDATA_VK_761 %></span>
        <div style="padding-top:10px;padding-left:25px;">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td valign="top">
                        <ncm:Calendar ID="txtStartDate" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_763 %>" CalendarTextBoxCssClass="TextField" TextCssClass="boldText" AlternativeDrawingMode="false" DisplayTime="false" />
                    </td>
                    <td valign="top" style="padding-left:40px;">
                        <ncm:Calendar ID="txtEndDate" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_764 %>" CalendarTextBoxCssClass="TextField" TextCssClass="boldText" AlternativeDrawingMode="false" DisplayTime="false" />
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div style="padding-top:50px;">
        <asp:CheckBox ID="chkOnlySendDevicesThatHadChanges" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_984 %>" />
    </div>

    <div style="padding-top:50px;" id="GenerateReportOnConfigTypeDIV">
        <asp:CheckBox ID="chkGenerateReportOnConfigType" runat="server" />
        <%=Resources.NCMWebContent.WEBDATA_VK_765 %> <ncm:ConfigTypes ID="ddlGenerateReportOnConfigType" runat="server" Width="150px" CssClass="SelectField"></ncm:ConfigTypes> <%=Resources.NCMWebContent.WEBDATA_VK_766 %>
    </div>
</div>