﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;

using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_Resources_Jobs_Controls_LightLoadScriptControl : System.Web.UI.UserControl
{
    public string ScriptText 
    {
        get { return txtConfig.Text; }
        set { txtConfig.Text = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string CssClass 
    {
        set 
        {
            if (!string.IsNullOrEmpty(value))
                mainTable.Attributes.Add(@"class", value);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            RefreshScriptList();
        }

        if (btnFromFile.HasFile)
            txtConfig.Text = ReadScriptFromFile();
    }

    protected void btnSaveScript_Click(object sender, EventArgs e)
    {
        var script = txtConfig.Text;
        if (!string.IsNullOrEmpty(script))
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = @"text/plain";
            HttpContext.Current.Response.AddHeader(@"Content-Disposition", @"attachment; filename=Orion-NCM-script.txt");
            HttpContext.Current.Response.Write(script);
            HttpContext.Current.Response.End();
        }
    }

    private string ReadScriptFromFile()
    {
        using (var reader = new StreamReader(btnFromFile.FileContent))
        {
            return reader.ReadToEnd();
        }
    }

    private void RefreshScriptList()
    {
        var isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            var dt = proxy.Query(@"SELECT C.ConfigID, C.ConfigTitle, C.Comments FROM Cirrus.SnippetArchive AS C ORDER BY C.DownloadTime");
            repeater.DataSource = dt;
            repeater.DataBind();
        }
    }

    protected void repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var row = e.Item.DataItem as DataRowView;
            var ctrl = e.Item.FindControl(@"rb");
            if (ctrl != null)
            {
                (ctrl as RadioButton).Text =
                    $@"<span style='padding-left:5px;'><img src='/Orion/NCM/Resources/images/ConfigIcons/Config-Edited.gif' style='vertical-align:middle;'/></span><span style='padding-left:5px;'>{HttpUtility.HtmlEncode(row["ConfigTitle"])}<span/>";
                (ctrl as RadioButton).Attributes.Add(@"value", Convert.ToString(row["ConfigID"]));
            }
        }
    }

    private string GetConfigText(string configId, string entityName)
    {
        if (!string.IsNullOrEmpty(configId))
        {
            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                var dt = proxy.Query(string.Format(@"SELECT C.Config FROM {1} AS C WHERE C.ConfigID='{0}'", configId, entityName));
                if (dt != null && dt.Rows.Count > 0)
                {
                    return Convert.ToString(dt.Rows[0][0]);
                }
            }
        }

        return string.Empty;
    }

    protected void ShowSelectScriptModalPopup(object sender, EventArgs e)
    {
        SelectScriptModalPopup.Show();
    }

    protected void SubmitSelectScriptModalPopup(object sender, EventArgs e)
    {
        SelectScriptModalPopup.Hide();

        foreach (RepeaterItem item in repeater.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                var ctrl = item.FindControl(@"rb");
                if (ctrl != null)
                {
                    if ((ctrl as RadioButton).Checked)
                    {
                        var configId = (ctrl as RadioButton).Attributes[@"value"];
                        txtConfig.Text += GetConfigText(configId, @"Cirrus.SnippetArchive");
                        ConfigUpdatePanel.Update();
                        return;
                    }
                }
            }
        }
    }

    protected void CloseSelectScriptModalPopup(object sender, EventArgs e)
    {
        SelectScriptModalPopup.Hide();
    }
}