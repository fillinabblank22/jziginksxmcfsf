﻿using System;
using System.Text;
using System.Globalization;
using System.Linq;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Logging;
using SolarWinds.NCM.Common.Dal;
using SolarWinds.NCM.Common.Help;

public partial class Orion_NCM_Resources_Jobs_Controls_EmailDetails : System.Web.UI.UserControl
{
    private Log log = new Log();
    private readonly IIsWrapper isWrapper;
    private readonly IDataDecryptor decryptor;

    public Orion_NCM_Resources_Jobs_Controls_EmailDetails()
    {
        isWrapper = ServiceLocator.Container.Resolve<IIsWrapper>();
        decryptor = ServiceLocator.Container.Resolve<IDataDecryptor>();
    }

    protected bool IsMultipleEngines => LookupEngines().Length > 1;

    public void LoadJob(NCMJob job)
    {
        if (job == null)
            throw new ArgumentNullException(nameof(job));

        chkLogSteps.Checked = job.JobDefinition.Notifications.LogSteps;

        chkSaveResultsToFile.Checked = job.JobDefinition.Notifications.SaveResultsToFile;
        if (string.IsNullOrEmpty(job.JobDefinition.Notifications.SaveToFileName))
        {
            var jobFilename = $@"{ReplaceInvalidFileNameChars(job.JobName, '_')}.htm";
            txtSaveToFileName.Text = System.IO.Path.Combine(GetDefaultPathForPrimaryEngine(), @"Jobs", jobFilename);
        }
        else
        {
            txtSaveToFileName.Text = job.JobDefinition.Notifications.SaveToFileName;
        }

        txtSenderName.Text = job.JobDefinition.Notifications.EmailFrom;
        txtReplyAddress.Text = job.JobDefinition.Notifications.EmailReplyAddress;
        txtSubject.Text = job.JobDefinition.Notifications.EmailSubject;
        txtTo.Text = job.JobDefinition.Notifications.EmailRecipient;
        txtRTNEmailCC.Text = job.JobDefinition.Notifications.EmailCC;
        txtRTNEmailBCC.Text = job.JobDefinition.Notifications.EmailBCC;
        txtServerAddress.Text = job.JobDefinition.Notifications.SMTPServer;
        txtPortNumber.Text = job.JobDefinition.Notifications.SMTPPort.ToString();
        txtUsername.Text = decryptor.Decrypt(job.JobDefinition.Notifications.SMTPAuthUserName);
        chkUseSSL.Checked = job.JobDefinition.Notifications.SMTPUseSSL;
        LoadSSLPortNumber(job.JobDefinition.Notifications.SMTPSSLPortNumber);

        var visible = (job.JobType == eJobType.ConfigChangeReport);
        checkBoxAttachAsPDF.Visible = visible;
        checkBoxAttachAsPDF.Checked = visible ? job.JobDefinition.Notifications.EmailAttachment : false;

        visible = (job.JobType != eJobType.ConfigChangeReport);
        checkBoxEmailOnlyOnError.Visible = visible;
        checkBoxEmailOnlyOnError.Checked = visible ? job.JobDefinition.Notifications.EmailOnlyOnError : false;

        var smtpAuthPassDecr = decryptor.Decrypt(job.JobDefinition.Notifications.SMTPAuthPassword);
        txtPassword.PasswordText = smtpAuthPassDecr;
        txtPassword2.PasswordText = smtpAuthPassDecr;

        if (job.JobDefinition.Notifications.EmailResults)
        {
            rbEnableEmailInfo.Checked = true;
        }
        else
        {
            rbDisableEmailInfo.Checked = true;
        }

        if (job.JobDefinition.Notifications.SMTPUseAuthentication)
            ddlAuthList.SelectedValue = @"pwd";
        else
            ddlAuthList.SelectedValue = @"none";

    }

    public bool UpdateJob(NCMJob job)
    {
        if (job == null)
            throw new ArgumentNullException(nameof(job));

        job.JobDefinition.Notifications.LogSteps = chkLogSteps.Checked;

        job.JobDefinition.Notifications.SaveResultsToFile = chkSaveResultsToFile.Checked;
        if (chkSaveResultsToFile.Checked)
        {
            if (ValidatePathOnAllEngines())
            {
                job.JobDefinition.Notifications.SaveToFileName = txtSaveToFileName.Text;
            }
            else { return false; }
        }

        if (rbEnableEmailInfo.Checked)
        {
            job.JobDefinition.Notifications.EmailFrom = txtSenderName.Text;
            job.JobDefinition.Notifications.EmailReplyAddress = txtReplyAddress.Text;
            job.JobDefinition.Notifications.EmailSubject = txtSubject.Text;
            job.JobDefinition.Notifications.EmailRecipient = txtTo.Text;
            job.JobDefinition.Notifications.EmailCC = txtRTNEmailCC.Text;
            job.JobDefinition.Notifications.EmailBCC = txtRTNEmailBCC.Text;
            job.JobDefinition.Notifications.SMTPServer = txtServerAddress.Text;
            job.JobDefinition.Notifications.SMTPPort = Convert.ToInt32(txtPortNumber.Text);
            job.JobDefinition.Notifications.SMTPAuthUserName = txtUsername.Text;
            job.JobDefinition.Notifications.SMTPAuthPassword = txtPassword.PasswordText;
            job.JobDefinition.Notifications.SMTPUseSSL = chkUseSSL.Checked;
            if (chkUseSSL.Checked) job.JobDefinition.Notifications.SMTPSSLPortNumber = Convert.ToInt32(txtSSLPortNumber.Text, CultureInfo.InvariantCulture);
            job.JobDefinition.Notifications.EmailAttachment = checkBoxAttachAsPDF.Checked;
            job.JobDefinition.Notifications.EmailOnlyOnError = checkBoxEmailOnlyOnError.Checked;
            job.JobDefinition.Notifications.EmailResults = true;
        }
        else
        {
            job.JobDefinition.Notifications.EmailResults = false;
        }

        if (ddlAuthList.SelectedItem.Value == @"pwd")
            job.JobDefinition.Notifications.SMTPUseAuthentication = true;
        else
            job.JobDefinition.Notifications.SMTPUseAuthentication = false;
        return true;
    }

    private EngineInfo[] LookupEngines()
    {
        var engineDal = ServiceLocator.Container.Resolve<ISwisEnginesDal>();
        return engineDal.GetAllPollingEngines().ToArray();
    }

    protected void btnValidate_Click(object sender, EventArgs e)
    {
        ValidatePathOnAllEngines();
    }

    private bool ValidatePathOnAllEngines()
    {
        var engines = LookupEngines();

        var sb = new StringBuilder();
        foreach (var engine in engines)
        {
            string error;
            if (!ValidatePath(txtSaveToFileName.Text, engine.EngineId, out error))
            {
                sb.AppendFormat(@"<tr><td><span class='SpanException'><span class='SpanException-Icon'></span>{0}</span></td></tr>",
                    string.IsNullOrEmpty(error) ?
                        IsMultipleEngines ? string.Format(Resources.NCMWebContent.WEBDATA_VK_945, engine.EngineName) : Resources.NCMWebContent.WEBDATA_VK_946 :
                        error);
            }
        }

        if (sb.Length > 0)
        {
            validator.Text =
                $@"<div style='padding-top:5px;'><table cellpadding='0' cellspacing='0' class='TableException'>{sb}</table></div>";
            return false;
        }
        else
        {
            validator.Text =
                $@"<div style='padding-top:5px;'><span class='SpanSuccess'><span class='SpanSuccess-Icon'></span>{Resources.NCMWebContent.WEBDATA_VK_947}</span></div>";
            return true;
        }
    }

    private bool ValidatePath(string pathWithFilename, int engineId, out string error)
    {
        try
        {
            var path = System.IO.Path.GetDirectoryName(pathWithFilename);

            using (var proxy = isWrapper.GetProxy())
            {
                error = string.Empty;
                return proxy.Cirrus.Settings.ValidatePath(path, engineId);
            }
        }
        catch (Exception ex)
        {
            error = ex.Message;
            log.Error(ex);
            return false;
        }
    }

    private string GetDefaultPathForPrimaryEngine()
    {
        try
        {
            using (var proxy = isWrapper.GetProxy())
            {
                var path = string.Empty;
                var dt = proxy.Query(@"SELECT TOP 1 E.EngineID FROM Orion.Engines AS E WHERE E.ServerType='Primary' ORDER BY E.KeepAlive DESC");
                if (dt != null && dt.Rows.Count > 0)
                {
                    var primaryEngineId = Convert.ToInt32(dt.Rows[0][0]);
                    path = proxy.Cirrus.Settings.GetDefaultPath(primaryEngineId);
                }
                return path;
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            return string.Empty;
        }
    }

    private string ReplaceInvalidFileNameChars(string filename, char replacement)
    {
        var newFilename = string.Empty;

        foreach (var ch in filename)
        {
            if (Array.IndexOf(System.IO.Path.GetInvalidFileNameChars(), ch) >= 0)
            {
                newFilename += replacement;
            }
            else
            {
                newFilename += ch;
            }
        }

        newFilename = newFilename.Replace(';', replacement);

        return newFilename;
    }

    private void LoadSSLPortNumber(int sslPortNumber)
    {
        if (sslPortNumber > 0)
        {
            txtSSLPortNumber.Text = Convert.ToString(sslPortNumber, CultureInfo.InvariantCulture);
        }
        else
        {
            using (var proxy = isWrapper.GetProxy())
            {
                txtSSLPortNumber.Text = proxy.Cirrus.Settings.GetSetting(Settings.SMTPSSLPort, 587, null);
            }
        }
    }

    protected string LearnMoreOrionConfigNetworkPath
    {
        get
        {
            var helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
            return helpHelperWrap.GetHelpUrl("OrionNCMPHHelpConfigNetworkPathSR");
        }
    }

}