﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExecuteScript.ascx.cs" Inherits="Orion_NCM_Resources_Jobs_Controls_ExecuteScript" %>

<%@ Register Src="~/Orion/NCM/Resources/Jobs/Controls/LightLoadScriptControl.ascx" TagName="LoadScript" TagPrefix="ncm" %>
<link rel="stylesheet" type="text/css" href="/orion/ncm/admin/settings/settings.css" />

<div>
    <div class="SettingBlockDescription" style="padding-top: 20px;"></div>
    <div id="pnlExecuteScript">
        <ncm:LoadScript ID="loadScriptCtrl" runat="server" />
    </div>
    <div id="optionsContainer" style="padding-top: 20px">
        <div style="padding-top:10px;">
            <asp:CheckBox ID="cbFilterRes" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VM_188 %>" />
            <asp:TextBox ID="txtFilterPattern" runat="server"/>
            <span style="font-size:8pt;"><a style="color:#336699;" href="<%=MoreInformationLink %>" target="_blank">&nbsp;&#0187;&nbsp; <%=Resources.NCMWebContent.WEBDATA_VM_190 %></a></span>
        </div>
        <div style="padding-top:20px;">
            <asp:CheckBox ID="cbShowCmdOutput" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VM_189 %>" />
        </div>
    </div>
</div>
