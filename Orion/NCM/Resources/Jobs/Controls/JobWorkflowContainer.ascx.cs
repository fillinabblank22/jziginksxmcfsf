﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.JobManagement;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NCM_Resources_Jobs_Controls_JobWorkflowContainer : System.Web.UI.UserControl
{
    public delegate void dlgNavigationClick(object sender, NCMJobEventArgs e);

    public event dlgNavigationClick BackClick;
    public event dlgNavigationClick NextClick;
    public event dlgNavigationClick CancelClick;

    public string JobPageTitle 
    {
        get
        {
            if (Convert.ToBoolean(Request.Params[@"NewJob"]))
            {
                return Resources.NCMWebContent.WEBDATA_IC_137;
            }
            else
            {
                return Server.HtmlEncode(JobWorkflowController.Job.JobName);
            }
        }
    }

    private bool isNewJob;
    private int currentStepIndex;

       
    [PersistenceMode(PersistenceMode.InnerProperty)]
    public PlaceHolder Content
    {
        get { return Container; }
    }

    public void RegisterConfirmationMessageOnNextClick(string message)
    {
        btnNext.OnClientClick = $@"return confirm('{message}');";
    }

    private string SetupWizard()
    {
        SetupFromRequest();

        //still need to get steps from job object instad of URL since Job type may be changed directly on Wizard step
        var steps = JobWorkflowController.GetJobSteps(JobWorkflowController.Job.JobDefinition.JobType, isNewJob);


        if (currentStepIndex == 0)
        {
            btnBack.Visible = false;
            btnFinish.Visible = true;
        }
        else
        {
            btnBack.Visible = true;
            btnFinish.Visible = false;
        }
        if (isNewJob) btnFinish.Visible = false; //do not show finish on the first step when this is new job

        if (currentStepIndex + 1 == steps.Count)
        {
            btnNext.LocalizedText = CommonButtonType.Finish;
        }
        else
        {
            btnNext.LocalizedText = CommonButtonType.Next;
        }

        var count = steps.Count;

        var sb = new StringBuilder();
        sb.Append(@"<div class='ProgressIndicator'>");
        for (var i = 0; i < count; i++)
        {
            bool nextActive;
            if (currentStepIndex==i)
            {
                sb.AppendFormat(@"<div class='PI_on'>&nbsp;{0}</div>", JobWizardHelper.LookupLocalizedJobWizardStep(steps[i]));                
                sb.AppendFormat(@"<img src='/Orion/Images/ProgressIndicator/pi_sep_on_off_sm.gif' />");
            }
            else
            {
                sb.AppendFormat(@"<div class='PI_off'>&nbsp;{0}</div>", JobWizardHelper.LookupLocalizedJobWizardStep(steps[i]));
                nextActive = ((i < count - 1) && steps[currentStepIndex].Equals(steps[i + 1]) ? true : false);
                if (nextActive)
                    sb.AppendFormat(@"<img src='/Orion/Images/ProgressIndicator/pi_sep_off_on_sm.gif' />");
                else
                    sb.AppendFormat(@"<img src='/Orion/Images/ProgressIndicator/pi_sep_off_off_sm.gif' />");
            }
        }
        sb.Append(@"</div>");

        return sb.ToString();
    }

    private void SetupFromRequest()
    {
        isNewJob = Convert.ToBoolean(Request.Params[@"NewJob"]);
        currentStepIndex = Convert.ToInt32(Request.Params[@"StepIndex"]);
    }

    protected override void OnInit(EventArgs e)
    {
        CheckJobValid();
        base.OnInit(e);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);        
        litWizardHeader.Text = SetupWizard();
        CheckJobManagement();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {        
        if (JobWorkflowController.Job == null) //session expired od user gsomehow get to this page directly
        {
            Page.Response.Redirect(JobWorkflowController.GetFinishJobStepUrl()); //redirect to job list to prevent crash
        }
        if (BackClick != null)
        {
            var arg = new NCMJobEventArgs();
            BackClick(sender, arg);
            if (!arg.ValidationPass) return;  //not used now but may requre validation on cancel in the future

        }
        Page.Response.Redirect(JobWorkflowController.GetPreviousJobStepUrl(isNewJob, JobWorkflowController.Job.JobDefinition.JobType, currentStepIndex));
    }

    //Dirty fix to show finsh button on first job step. Current architecture of wizard was not designed for this
    protected void btnFinish_Click(object sender, EventArgs e)
    {

        if (NextClick != null)
        {
            var arg = new NCMJobEventArgs();
            NextClick(sender, arg);

            if (!arg.ValidationPass) return;
        }
        JobWorkflowController.Job.Author = Page.User.Identity.Name;
        JobWorkflowController.Job.JobDefinition.OwnerSID = Page.User.Identity.Name;
        var isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            proxy.Cirrus.Jobs.UpdateJob(JobWorkflowController.Job);
        }

        JobWorkflowController.CleanUpJobFromSesion();
        Page.Response.Redirect(JobWorkflowController.GetFinishJobStepUrl());
       
    }
   

    protected void btnNext_Click(object sender, EventArgs e)
    {

        if (NextClick != null)
        {
            var arg = new NCMJobEventArgs();
            NextClick(sender, arg);

            if (!arg.ValidationPass) return;   
        }
        if ((sender as LocalizableButton).LocalizedText == CommonButtonType.Finish)
        {
            JobWorkflowController.CleanUpJobFromSesion(); 
            Page.Response.Redirect(JobWorkflowController.GetFinishJobStepUrl());
        }
        else
        {
            Page.Response.Redirect(JobWorkflowController.GetNextJobStepUrl(isNewJob, JobWorkflowController.Job.JobDefinition.JobType, currentStepIndex));
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (CancelClick != null)
        {
            var arg = new NCMJobEventArgs();
            CancelClick(sender, arg);
            if (!arg.ValidationPass) return;  //not used now but may requre validation on cancel in the future
        }

        JobWorkflowController.CleanUpJobFromSesion();    
        Page.Response.Redirect(JobWorkflowController.GetCancelJobStepUrl());
    }

    // check if job instance exist in session and reduirect to job list page if now
    private void CheckJobValid()
    {
        if (JobWorkflowController.Job == null) //session expired od user somehow get to this page directly
        {
            Page.Response.Redirect(JobWorkflowController.GetFinishJobStepUrl()); //redirect to job list to prevent crash
        }
    }

    private void CheckJobManagement()
    {
        var mode = GetApprovalMode();
        if (mode == eApprovalMode.TwoLevelAll)
        {
            if (!(Profile.AllowAdmin && (SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR) || SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ENGINEER))))
                Server.Transfer(
                    $@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.Jobs_List_InsufficientPermissionsMessage}");
        }
        else
        {
            if (SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_WEBUPLOADER))
            {
                if (isNewJob && JobWorkflowController.Job.IsPrePolulated == false)
                {
                    Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_VK_800}");
                }
            }
            else if (SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_WEBDOWNLOADER) || SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_WEBVIEWER))
            {
                Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_VK_800}");
            }
        }
    }

    private eApprovalMode GetApprovalMode()
    {
        var isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            return proxy.Cirrus.ConfigChangeApproval.GetApprovalMode();
        }
    }
}