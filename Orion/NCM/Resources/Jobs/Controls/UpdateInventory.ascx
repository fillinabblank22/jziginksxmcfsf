﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UpdateInventory.ascx.cs"  Inherits="Orion_NCM_Controls_Jobs_JobInventorySetting" %>

<%@ Register Src="~/Orion/NCM/Controls/InventorySettings.ascx" TagPrefix="ncm" TagName="InventorySettings" %>

<link rel="stylesheet" type="text/css" href="/Orion/NCM/Resources/Jobs/Jobs.css" />

<div style="padding-top:10px;">
    <div class="SettingBlockDescription" style="padding-top: 0px;">
        <%=Resources.NCMWebContent.WEBDATA_VK_509 %></div>
    <div>
        <div id="errorContent" style="display:none; padding-top:10px;" runat="server" clientidmode="Static">
            <asp:Label ID="lblError"  ForeColor="Red" runat="server"></asp:Label>
        </div>
        <ncm:InventorySettings runat="server" ID="ncmInventorySettings"/>
    </div>
    <div style="border-bottom: #e6e6dd 1px solid; padding-top: 20px;">
    </div>
</div>
