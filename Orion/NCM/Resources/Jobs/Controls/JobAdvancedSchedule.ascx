﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="JobAdvancedSchedule.ascx.cs" Inherits="Orion_NCM_Resources_Jobs_Controls_JobAdvancedSchedule" %>

<%@ Register Assembly="SolarWinds.NCMModule.Web.Resources" Namespace="SolarWinds.NCMModule.Web.Resources.EWBusinessLayer" TagPrefix="cc1" %>
<%@ Register Assembly="SolarWinds.NCMModule.Web.Resources" Namespace="SolarWinds.NCMModule.Web.Resources" TagPrefix="cc2" %>
<%@ Register TagName="UtcOffset" TagPrefix="ncm" Src="~/Orion/NCM/Controls/UtcServerZone.ascx" %>

<orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />

<link rel="stylesheet" type="text/css" href="/orion/ncm/resources/jobs/jobs.css" />
<link rel="stylesheet" type="text/css" href="/orion/ncm/styles/NCMResources.css"  />
<style type="text/css">
        .rbl input
        {
            vertical-align: middle;
        }
        
        .rbl label
        {
           margin-left: 10px;
           margin-right: 1px;
           vertical-align: middle;
        }
        
        .cbl input
        {
            vertical-align: middle;
        }
        
        .cbl label
        {
            margin-left:5px;
            margin-right: 1px;
            vertical-align: middle;
        }
        
        .RightCol
        {
            padding-left:5px;
        }
        
        .LeftCol
        {
            width:115px;
        }
        
        .advTableStyle tr 
        {
            height: 40px;
        }
</style>

<script type="text/javascript">
    Ext.onReady(function () {

        var tabs = new Ext.TabPanel({
            renderTo: 'scheduleTabs',
            width: 660,
            height: 430,
            plain: true,
            defaults: { listeners: { activate: handleActivateScheduleTab} },
            items: [
                    { contentEl: 'oneShotTab', itemId: 'Once', title: '<%=Resources.NCMWebContent.WEBDATA_VM_213%>' },
                    { contentEl: 'dailyTab', itemId: 'Daily', title: '<%=Resources.NCMWebContent.WEBDATA_VK_806%>' },
                    { contentEl: 'weeklyTab', itemId: 'Weekly', title: '<%=Resources.NCMWebContent.WEBDATA_VK_807%>' },
                    { contentEl: 'monthlyTab', bodyStyle: 'position:static', itemId: 'Monthly', title: '<%=Resources.NCMWebContent.WEBDATA_VK_808%>' }
                   ]
        });

        tabs.setActiveTab($('#hfActiveSceduleTriggerTab').val());

        function handleActivateScheduleTab(tab) {
            $('#hfActiveSceduleTriggerTab').val(tab.itemId);
        }


    })

    function handleScheduleTypeChanged(obj) {
        var sValue = obj.options[obj.selectedIndex].value;
        DisplayCorrectScheduleContent(sValue);
    }

    function DisplayCorrectScheduleContent(scheduleType) {

        if (scheduleType == '0') {//basic
            $('#basicScheduleContent').show();
            $('#advancedScheduleContent').hide();
        }
        else {//adv
            $('#basicScheduleContent').hide();
            $('#advancedScheduleContent').show();
        }

    }

    //basic schedule months tab
    
    function SelectAllMonths(val) {
        $("input[id*=cblMonths_]").prop('checked', val);
    }

    function MonthChecked(val) {
        if (val)//checked
        {
            if ($("input[id*=cblMonths_]").filter(':checked').length == 12) 
                $('#cbAllMonths').prop('checked', true);
            else
                $('#cbAllMonths').prop('checked', false);
        }
        else
        {
            $('#cbAllMonths').prop('checked', false);
        }
    }

    $(function () {
        $("input[id*=cblMonths_]").click(function (e) { MonthChecked(e.target.checked) });
        $('#cbAllMonths').click(function (e) { SelectAllMonths(e.target.checked) });
    })

</script>

<asp:HiddenField ID="hfActiveSceduleTriggerTab" Value="Daily" runat="server"  ClientIDMode="Static" />

<div id="container">
    <div>
        <div>
            <b class="LabelFont"><%=Resources.NCMWebContent.WEBDATA_VM_195%></b>
            <div style="display:inline;">
                <asp:DropDownList ID="ddlScheduleType" CssClass="SelectField" runat="server" onchange="handleScheduleTypeChanged(this);" >
                    <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_455 %>" Value="0"/>
                    <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_456 %>" Value="1"/>
                </asp:DropDownList>
            </div>
        </div>

        <div id="basicScheduleContent" runat="server" style="display:none;" clientidmode="Static">
            <div id="scheduleTabs" style="margin-top:20px;">
                <div id="oneShotTab" class="x-hide-display">
                    <div style="padding-top:50px; padding-left:20px;">
                        <cc2:Calendar ID="dateTimePicker" Text="<%$ Resources: NCMWebContent, Jobs_AdvancedSchedule_RunAt %>" Width="100" TimeTextBoxCssClass="TextField" runat="server" />
                    </div> 
                    <div class="Hint" style="padding-left:20px;">
                        <%=Resources.NCMWebContent.WEBDATA_VM_215%>
                        <ncm:UtcOffset ID="UtcOffset2" runat="server"/>
                    </div>
                </div>
                <div id="dailyTab" class="x-hide-display">
                <div style="padding-top:20px;padding-left:15px;">
                    <div style="padding-top:15px;">
                        <%=Resources.NCMWebContent.WEBDATA_VM_196%> <asp:TextBox ID="txtStartTimeDailyTab" Width="40px" CssClass="TextField" runat="server" />
                        <span class="Hint">
                            <%=Resources.NCMWebContent.WEBDATA_VM_197%>
                            <ncm:UtcOffset runat="server"/>
                        </span>
                    </div>
                    <div style="padding-top:25px;">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td valign="top">
                                    <cc2:Calendar ID="txtDailyStartingOn" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_849 %>" CalendarTextBoxCssClass="TextField" AlternativeDrawingMode="false" DisplayTime="false" />
                                </td>
                                <td valign="top" style="padding-left:40px;">
                                    <cc2:Calendar ID="txtDailyEndingOn" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_850 %>" CalendarTextBoxCssClass="TextField" AlternativeDrawingMode="false" DisplayTime="false" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
                <div id="weeklyTab" class="x-hide-display"> 
                <div style="padding-top:20px;padding-left:15px;">
                    <asp:CheckBoxList ID="cblWeekDays" CssClass="cbl" Width="90%" runat="server" RepeatDirection="Horizontal" RepeatColumns="5"/>
                    <div style="padding-top:15px;">
                        <%=Resources.NCMWebContent.WEBDATA_VM_196%> <asp:TextBox ID="txtStartTimeWeeklyTab"  Width="40px" CssClass="TextField"  runat="server" />
                        <span class="Hint"> 
                            <%=Resources.NCMWebContent.WEBDATA_VM_197%>
                            <ncm:UtcOffset runat="server"/>
                        </span>
                    </div>
                    <div style="padding-top:25px;">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td valign="top">
                                    <cc2:Calendar ID="txtWeeklyStartingOn" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_849 %>" CalendarTextBoxCssClass="TextField" AlternativeDrawingMode="false" DisplayTime="false" />
                                </td>
                                <td valign="top" style="padding-left:40px;">
                                    <cc2:Calendar ID="txtWeeklyEndingOn" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_850 %>" CalendarTextBoxCssClass="TextField" AlternativeDrawingMode="false" DisplayTime="false" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
                <div id="monthlyTab" class="x-hide-display">
                <div style="padding-top:20px;padding-left:15px;">
                    <div style="float:left;border-right: solid 1px #dcdbd7;height:205px;padding-right: 20px;margin-top:10px;">
                        <asp:CheckBox ID="cbAllMonths" CssClass="cbl" runat="server" ClientIDMode="Static" Text="<%$ Resources: NCMWebContent, Jobs_AdvancedSchedule_AllMonths %>" />
                    </div>
                    <div style="float:left;margin-left: 20px;">
                        <asp:CheckBoxList ID="cblMonths" CssClass="cbl" ClientIDMode="Static" runat="server" Width="300px" Height="230px" RepeatDirection="Horizontal" RepeatColumns="2"/>
                    </div>
                    <div style="clear:both;">
                        <table>
                            <tr>
                                <td style="padding-right:10px;"><%=Resources.NCMWebContent.WEBDATA_VM_198%></td>
                                <td><asp:TextBox ID="txtDaysOfTheMonth" Width="40px" CssClass="TextField" runat="server"></asp:TextBox></td>
                                <td style="padding-left:10px;"><b class="Hint"><%=Resources.NCMWebContent.WEBDATA_VK_460%></b></td>
                            </tr>
                            <tr>
                                <td style="padding-right:10px;"><%=Resources.NCMWebContent.WEBDATA_VM_196%></td>
                                <td><asp:TextBox ID="txtStartTimeMonthlyTab" Width="40px" CssClass="TextField" runat="server" /></td>
                                <td style="padding-left:10px;">
                                <b class="Hint"> 
                                    <%=Resources.NCMWebContent.WEBDATA_VM_197%>
                                    <ncm:UtcOffset ID="UtcOffset1" runat="server"/>
                                </b></td>
                            </tr>
                        </table>
                    </div>
                    <div style="padding-top:25px;">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td valign="top">
                                    <cc2:Calendar ID="txtMonthlyStartingOn" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_849 %>" CalendarTextBoxCssClass="TextField" AlternativeDrawingMode="false" DisplayTime="false" />
                                </td>
                                <td valign="top" style="padding-left:40px;">
                                    <cc2:Calendar ID="txtMonthlyEndingOn" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_850 %>" CalendarTextBoxCssClass="TextField" AlternativeDrawingMode="false" DisplayTime="false" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            </div>
        </div>

        <div id="advancedScheduleContent" runat="server" style="display: none;" clientidmode="Static">
            <div style="background-color:#e4f1f8;width:75%;margin-top: 15px;">
                <div style="padding-top:5px;padding-left:20px;padding-right:20px;">
                <table class="advTableStyle" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="LeftCol">
                            <%=Resources.NCMWebContent.WEBDATA_VK_449%>
                        </td>
                        <td class="MiddleCol">
                            <asp:TextBox ID="txtAdvMinutes" Width="40px" CssClass="TextField" runat="server" ></asp:TextBox>
                        </td>
                        <td class="RightCol Hint" style="vertical-align: middle;">
                              <%=Resources.NCMWebContent.WEBDATA_VK_469%>
                        </td>
                        <td style="padding-left:10px;">
                            <cc1:CronDataValidator id="CronMinutesValidator" ControlToValidate="txtAdvMinutes" Display="Dynamic" EnableClientScript="false"
                            runat="server" ValidationGroup="advSceduleValidators" MinimumValue="0" MaximumValue="59" Font-Size="11px" Font-Bold="true" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VM_202 %>"/> 
                        </td>
                    </tr>
                    <tr>
                        <td class="LeftCol">
                            <%=Resources.NCMWebContent.WEBDATA_VK_448%>
                        </td>
                        <td class="MiddleCol">
                            <asp:TextBox ID="txtAdvHours" Width="40px" CssClass="TextField" runat="server" ></asp:TextBox>
                        </td>
                        <td class="RightCol Hint" style="vertical-align: middle;">
                            <%=Resources.NCMWebContent.WEBDATA_VK_459%>
                        </td>
                        <td style="padding-left:10px;">
                            <cc1:CronDataValidator id="CronHoursValidator" ControlToValidate="txtAdvHours" Display="Dynamic" EnableClientScript="false"
                            runat="server" ValidationGroup="advSceduleValidators" MinimumValue="0" MaximumValue="23" Font-Size="11px" Font-Bold="true" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VM_202 %>"/> 
                        </td>
                    </tr>
                    <tr>
                        <td class="LeftCol">
                             <%=Resources.NCMWebContent.WEBDATA_VK_447%>
                        </td>
                        <td class="MiddleCol">
                            <asp:TextBox ID="txtAdvDayOfMonth" Width="40px" CssClass="TextField" runat="server" ></asp:TextBox>
                        </td>
                        <td class="RightCol Hint" style="vertical-align: middle;">
                             <%=Resources.NCMWebContent.WEBDATA_VK_460%>
                        </td>
                        <td style="padding-left:10px;">
                            <cc1:CronDataValidator id="CronDayOfMonthValidator" ControlToValidate="txtAdvDayOfMonth" Display="Dynamic" EnableClientScript="false"
                            runat="server" ValidationGroup="advSceduleValidators" MinimumValue="01" MaximumValue="31" Font-Size="11px" Font-Bold="true" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VM_202 %>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="LeftCol">
                            <%=Resources.NCMWebContent.WEBDATA_VK_446%>
                        </td>
                        <td class="MiddleCol">
                            <asp:TextBox ID="txtAdvMonth" Width="40px" CssClass="TextField" runat="server" ></asp:TextBox>
                        </td>
                        <td class="RightCol Hint" style="vertical-align: middle;">
                            <%=Resources.NCMWebContent.WEBDATA_VK_461%>
                        </td>
                        <td style="padding-left:10px;">
                        <cc1:CronDataValidator id="CronMonthValidator" ControlToValidate="txtAdvMonth" Display="Dynamic" EnableClientScript="false"
                        runat="server" ValidationGroup="advSceduleValidators" MinimumValue="1" MaximumValue="12" Font-Size="11px" Font-Bold="true" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VM_202 %>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="LeftCol">
                            <%=Resources.NCMWebContent.WEBDATA_VK_445%>
                        </td>
                        <td class="MiddleCol">
                            <asp:TextBox ID="txtAdvDayOfWeek" Width="40px" CssClass="TextField" runat="server" ></asp:TextBox>
                        </td>
                        <td class="RightCol Hint" style="vertical-align: middle;">
                            <%=Resources.NCMWebContent.WEBDATA_VK_951%>
                        </td>
                        <td style="padding-left:10px;">
                        <cc1:CronDataValidator id="CronDayOfWeekValidator" ControlToValidate="txtAdvDayOfWeek" Display="Dynamic" EnableClientScript="false"
                        runat="server" ValidationGroup="advSceduleValidators" MinimumValue="0" MaximumValue="6" Font-Size="11px" Font-Bold="true" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VM_202 %>"/>
                        </td>
                    </tr>
                </table>
                </div>
                <div class="Hint" style="margin-top:25px;padding-right: 60px;padding-bottom:10px;padding-left:10px;">
                    <%=Resources.NCMWebContent.WEBDATA_VM_200%><%=string.Format(@"{0}{2}{1}", @"<span style='font-size:8pt;padding-left:10px;'>&#0187;&nbsp;<a style='color:#336699;' target='_blank' href=" + this.LearnMore + @">", @"</a></span>", Resources.NCMWebContent.WEBDATA_VM_214)%>
                </div>
                <div style="padding:10px;">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top">
                                <cc2:Calendar ID="txtAdvStartingOn" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_849 %>" CalendarTextBoxCssClass="TextField" AlternativeDrawingMode="false" DisplayTime="false" />
                            </td>
                            <td valign="top" style="padding-left:40px;">
                                <cc2:Calendar ID="txtAdvEndingOn" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_850 %>" CalendarTextBoxCssClass="TextField" AlternativeDrawingMode="false" DisplayTime="false" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div id="errorContent" style="display:none;" runat="server" clientidmode="Static">
            <asp:BulletedList ID="blErrors" ForeColor="Red" runat="server" style="padding-left:20px;padding-top:20px;" BulletStyle="Disc"></asp:BulletedList>
        </div>
    </div>
</div>
