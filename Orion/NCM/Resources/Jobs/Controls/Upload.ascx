﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Upload.ascx.cs" Inherits="Orion_NCM_Resources_Jobs_Controls_Upload" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="SolarWinds.NCMModule.Web.Resources" Namespace="SolarWinds.NCMModule.Web.Resources.Controls" TagPrefix="ncm" %>
<%@ Register TagPrefix="ncm" TagName="ModalPopup" Src="~/Orion/NCM/Controls/ModalPopup.ascx"  %>

<script type="text/javascript" src="/Orion/NCM/JavaScript/main.js"></script>

<link rel="stylesheet" type="text/css" href="/Orion/NCM/JavaScript/Ext_UX/Ext.ux.form.FileUploadField.css" />
<link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
<link rel="stylesheet" type="text/css" href="/Orion/NCM/Resources/Jobs/Jobs.css" />

<style type="text/css">
    .ScriptList tbody tr td
    {
    	padding-top:5px;
    	padding-bottom:5px;
    }
    
    .ImgButton
    {
        font-family: Arial,Verdana,Helvetica,sans-serif;
        font-size: 11px !important;
        font-weight: normal;
        color:Black;
    }
    
    .ImgButton:hover
    {
		cursor:pointer;
		color:Black;
    }
    
    .x-form-file-wrap
    {
        height:auto;
    }
    
    .toolbar
    {
        padding: 1px 0px 1px 3px;
    }
</style>

<script type="text/javascript">
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function () {
        $("input:radio").removeAttr('checked');
        $("input:radio").click(function () {
            $("input:radio").removeAttr('checked');
            $(this).attr('checked', 'checked');
            return true;
        });

        var w = $("#fileUploadTable").width();
        $("[id$='_btnSelectFile']").width(w);
    });
</script>

<div>
    <div class="JobHeader"><%=Resources.NCMWebContent.WEBDATA_VK_782 %></div>    
    <div style="padding-top:10px;">
        <table cellpadding="0" cellspacing="0" style="border: 1px solid #999999;">
            <tr>
                <td style="background-color:rgb(235, 236, 237);">
                    <asp:UpdatePanel ID="UpdatePanelSelectConfig" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="toolbar"><asp:ImageButton ID="btnImgSelectConfigSnippet" runat="server" style="vertical-align:middle;" ImageUrl="/Orion/NCM/Resources/images/import_icon.png" OnClick="ShowSelectScriptModalPopup" /></td>
                                    <td class="toolbar"><asp:LinkButton ID="btnSelectConfigSnippet" CssClass="ImgButton" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_822 %>" runat="server" OnClick="ShowSelectScriptModalPopup" /></td>
                                    <td class="toolbar"><asp:ImageMap ID="imgSeparartor1" runat="server" style="vertical-align:middle;" ImageUrl="/Orion/NCM/Resources/images/Separator.ToolBarNodes.JPG"></asp:ImageMap></td>
                                    <td>
                                        <div class="x-form-field-wrap x-form-file-wrap">
                                            <asp:FileUpload ID="btnSelectFile" runat="server" class="x-form-file ImgButton" onchange="__doPostBack('', '');" />
                                            <table cellpadding="0" cellspacing="0" id="fileUploadTable" class="ImgButton">
                                                <tr>
                                                    <td class="toolbar"><img alt="" style="vertical-align:middle;" src="/Orion/NCM/Resources/images/icon_loadscript.gif" /></td>
                                                    <td class="ImgButton toolbar"><%=Resources.NCMWebContent.WEBDATA_VK_820 %></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                    <td class="toolbar"><asp:ImageMap ID="imgSeparartor2" runat="server" style="vertical-align:middle;" ImageUrl="/Orion/NCM/Resources/images/Separator.ToolBarNodes.JPG"></asp:ImageMap></td>
                                    <td class="toolbar"><asp:ImageButton ID="btnImgSelectConfig" runat="server" style="vertical-align:middle;" ImageUrl="/Orion/NCM/Resources/images/database_icon.png" OnClick="ShowSelectConfigModalPopup" /></td>
                                    <td class="toolbar"><asp:LinkButton ID="btnSelectConfig" CssClass="ImgButton" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_821 %>" runat="server" OnClick="ShowSelectConfigModalPopup" /></td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="ConfigUpdatePanel" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtConfig" runat="server" 
                                TextMode="MultiLine" 
                                BorderColor="White"
                                BackColor="White" 
                                BorderWidth="0px" 
                                Width="700px" 
                                Height="300px"
                                Font-Names="Arial,Verdana,Helvetica,sans-serif"
                                spellcheck="false"
                                Font-Size="9pt">
                            </asp:TextBox>
                            <ajax:TextBoxWatermarkExtender ID="txtWE" runat="server"
                                TargetControlID="txtConfig"
                                WatermarkText="<%$ Resources: NCMWebContent, WEBDATA_VM_187 %>"
                                WatermarkCssClass="WatermarkStyle">
                            </ajax:TextBoxWatermarkExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </div>

    <div id="WriteToNVRAMDIV" runat="server" style="padding-top:50px;">
        <asp:CheckBox ID="chkWriteToNVRAM" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_783 %>" />
    </div>

    <div id="ConfigTypeUploadDIV" runat="server" style="padding-top:50px;">
         <%=Resources.NCMWebContent.WEBDATA_VK_784 %>
         <span style="padding-left:5px;"><ncm:ConfigTypes ID="ddlConfigType" runat="server" Width="150px" CssClass="SelectField"></ncm:ConfigTypes></span>
    </div>

    <ncm:ModalPopup runat="server" ID="SelectConfigModalPopup" Title="<%$ Resources: NCMWebContent, WEBDATA_VK_785 %>" Width="750" Height="300">
        <DialogContent>
            <table id="nodetreecontrol_selectconfig" width="100%">
                <tr>
                    <td id="SelectConfigContainer" runat="server">
                    </td>
                </tr>
            </table>
        </DialogContent>        
        <SubmitButtonContent>
            <orion:LocalizableButton ID="btnSubmitConfig" runat="server" LocalizedText="Ok" DisplayType="Secondary" OnClick="SubmitSelectConfigModalPopup"/> 
        </SubmitButtonContent>
        <CancelButtonContent>
            <orion:LocalizableButton ID="btnCancelConfig" runat="server" LocalizedText="Cancel" DisplayType="Secondary" OnClick="CloseSelectConfigModalPopup"/>
        </CancelButtonContent>
    </ncm:ModalPopup>

    <ncm:ModalPopup runat="server" ID="SelectScriptModalPopup" Title="<%$ Resources: NCMWebContent, WEBDATA_VK_786 %>" Width="750" Height="300">
        <DialogContent>
            <div style="padding:5px 10px 5px 10px;">
                <asp:Repeater runat="server" ID="repeater" OnItemDataBound="repeater_ItemDataBound">
                    <HeaderTemplate>
                        <table id="table" border="0" cellpadding="" cellspacing="0" width="100%" class="ScriptList" style="white-space:nowrap;table-layout:fixed;">
                    </HeaderTemplate>
                    <ItemTemplate>
                            <tr>
			                    <td style="width:30%;overflow:hidden;text-overflow:ellipsis;">
                                    <asp:RadioButton ID="rb" runat="server" GroupName="ScriptGroup" />
                                </td>
			                    <td style="width:70%;overflow:hidden;text-overflow:ellipsis;">
                                    <%#Eval(@"Comments")%>
                                </td>
		                    </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </DialogContent>        
        <SubmitButtonContent>
            <orion:LocalizableButton ID="btnSubmitConfigSnippet" runat="server" LocalizedText="Ok" DisplayType="Secondary" OnClick="SubmitSelectScriptModalPopup"/> 
        </SubmitButtonContent>
        <CancelButtonContent>
            <orion:LocalizableButton ID="btnCancelConfigSnippet" runat="server" LocalizedText="Cancel" DisplayType="Secondary" OnClick="CloseSelectScriptModalPopup"/>
        </CancelButtonContent>
    </ncm:ModalPopup>
</div>
