﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LightLoadScriptControl.ascx.cs" Inherits="Orion_NCM_Resources_Jobs_Controls_LightLoadScriptControl" %>

<%@ Register TagPrefix="ncm" TagName="ModalPopup" Src="~/Orion/NCM/Controls/ModalPopup.ascx"  %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<script type="text/javascript" src="/Orion/NCM/JavaScript/main.js"></script>

<link rel="stylesheet" type="text/css" href="/Orion/NCM/JavaScript/Ext_UX/Ext.ux.form.FileUploadField.css" />
<link rel="stylesheet" type="text/css" href="/Orion/NCM/Resources/Jobs/Jobs.css" />

<style type="text/css">
    .ScriptList tbody tr td
    {
    	padding-top:5px;
    	padding-bottom:5px;
    }
    
    .ImgButton
    {
        font-family: Arial,Verdana,Helvetica,sans-serif;
        font-size: 11px !important;
        font-weight: normal;
        color:Black;
    }
    
    .ImgButton:hover
    {
		cursor:pointer;
		color:Black;
    }
    
    .x-form-file-wrap
    {
        height:auto;
    }
    
    .toolbar
    {
        padding: 1px 0px 1px 3px;
    }
    
    .default-border { border: 1px solid #999999; }
</style>

<script type="text/javascript">
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function () {
        $('.ncm-radio-btn-script-name>input:radio').removeAttr('checked');
        $('.ncm-radio-btn-script-name>input:radio"').click(function () {
            $('.ncm-radio-btn-script-name>input:radio').removeAttr('checked');
            $(this).attr('checked', 'checked');
            return true;
        });

        var w = $("#fileUploadTable").width();
        $("[id$='_btnFromFile']").width(w);
    });

    var LoadScriptControl = {
        getScript: function () {
            return $('#<%=txtConfig.ClientID %>').val();
        },
        setScript: function (script) {
            $('#<%=txtConfig.ClientID %>').val(script);
        }
    }
</script>

<table id="mainTable" runat="server" cellpadding="0" cellspacing="0" class="default-border">
    <tr>
        <td style="background-color:rgb(235, 236, 237);">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanelSelectScript" UpdateMode="Conditional" runat="server">
                            <ContentTemplate>
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="toolbar"><asp:ImageButton ID="btnImgSelectScript" runat="server" style="vertical-align:middle;" ImageUrl="/Orion/NCM/Resources/images/import_icon.png" OnClick="ShowSelectScriptModalPopup" CausesValidation="false" /></td>
                                        <td class="toolbar"><asp:LinkButton ID="btnSelectScript" CssClass="ImgButton" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_822 %>" runat="server" OnClick="ShowSelectScriptModalPopup" CausesValidation="false" /></td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="toolbar"><asp:ImageMap ID="imgSeparartor1" runat="server" style="vertical-align:middle;" ImageUrl="/Orion/NCM/Resources/images/Separator.ToolBarNodes.JPG"></asp:ImageMap></td>
                    <td>
                         <div class="x-form-field-wrap x-form-file-wrap">
                            <asp:FileUpload ID="btnFromFile" runat="server" class="x-form-file ImgButton" onchange="__doPostBack('', '');" />
                            <table cellpadding="0" cellspacing="0" id="fileUploadTable" class="ImgButton">
                                <tr>
                                    <td class="toolbar"><img alt="" style="vertical-align:middle;" src="/Orion/NCM/Resources/images/icon_loadscript.gif" /></td>
                                    <td class="ImgButton toolbar"><%=Resources.NCMWebContent.WEBDATA_VK_820 %></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td class="toolbar"><asp:ImageMap ID="imgSeparartor2" runat="server" style="vertical-align:middle;" ImageUrl="/Orion/NCM/Resources/images/Separator.ToolBarNodes.JPG"></asp:ImageMap></td>
                    <td class="toolbar"><asp:ImageButton ID="btnImgSaveScript" runat="server" style="vertical-align:middle;" ImageUrl="/Orion/NCM/Resources/images/icon_savescript.gif" OnClick="btnSaveScript_Click" CausesValidation="false" /></td>
                    <td class="toolbar"><asp:LinkButton ID="btnSaveScript" CssClass="ImgButton" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_913 %>" runat="server" OnClick="btnSaveScript_Click" CausesValidation="false" /></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <asp:UpdatePanel ID="ConfigUpdatePanel" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <ncm:CrLfTextBox ID="txtConfig" runat="server" 
                        TextMode="MultiLine"
                        BorderColor="White" 
                        BorderWidth="0px" 
                        Width="700px" 
                        Height="300px"
                        Font-Names="Arial,Verdana,Helvetica,sans-serif"
                        Font-Size="9pt"
                        placeholder="<%$ Resources: NCMWebContent, WEBDATA_VM_187 %>">
                    </ncm:CrLfTextBox>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>

<ncm:ModalPopup runat="server" ID="SelectScriptModalPopup" Title="<%$ Resources: NCMWebContent, WEBDATA_VK_786 %>" Width="750" Height="300">
    <DialogContent>
        <div style="padding:5px 10px 5px 10px;">
            <asp:Repeater runat="server" ID="repeater" OnItemDataBound="repeater_ItemDataBound">
                <HeaderTemplate>
                    <table id="table" border="0" cellpadding="" cellspacing="0" width="100%" class="ScriptList" style="white-space:nowrap;table-layout:fixed;">
                </HeaderTemplate>
                <ItemTemplate>
                        <tr>
			                <td style="width:30%;overflow:hidden;text-overflow:ellipsis;">
                                <asp:RadioButton ID="rb" runat="server" GroupName="ScriptGroup" CssClass="ncm-radio-btn-script-name" />
                            </td>
			                <td style="width:70%;overflow:hidden;text-overflow:ellipsis;">
                                <%#Eval(@"Comments")%>
                            </td>
		                </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </DialogContent>        
    <SubmitButtonContent>
        <orion:LocalizableButton ID="btnSubmitScript" runat="server" LocalizedText="Ok" DisplayType="Secondary" OnClick="SubmitSelectScriptModalPopup" CausesValidation="False"/> 
    </SubmitButtonContent>
    <CancelButtonContent>
        <orion:LocalizableButton ID="btnCancelScript" runat="server" LocalizedText="Cancel" DisplayType="Secondary" OnClick="CloseSelectScriptModalPopup" CausesValidation="False"/>
    </CancelButtonContent>
</ncm:ModalPopup>