﻿<%@ WebHandler Language="C#" Class="SaveJobLog" %>

using System;
using System.Web;
using System.Web.SessionState;

public class SaveJobLog : IHttpHandler, IRequiresSessionState
{
    private const int IEMaxFileNameSize = 23;
    
    public void ProcessRequest (HttpContext context) 
    {
        var isLayer = new SolarWinds.NCMModule.Web.Resources.ISWrapper();

        var jobID = Convert.ToInt32(context.Request[@"JobID"]);
        var jobName = context.Request[@"JobName"];
        
        try
        {
            var jobLog = string.Empty;
            using (var proxy = isLayer.Proxy)
            {
                jobLog = proxy.Cirrus.Jobs.GetJobLog(jobID, false);
            }
            
            var fileName = GetFileName(context, jobName);
            context.Response.Clear();
            context.Response.ContentType = @"application/txt";
            context.Response.AddHeader(@"Content-Disposition", $@"attachment; filename={fileName}");
            context.Response.Write(jobLog);
        }
        catch (Exception ex)
        {
            var exString = string.Empty;
            var divFormat = @"<div style=""color:Red;font-size:12px;font-family: Arial,Verdana,Helvetica,sans-serif;"">{0}</div>";
            if (ex.GetType() == typeof(System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>))
            {
                exString = string.Format(divFormat, (ex as System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>).Detail.Message);
            }
            else
            {
                exString = string.Format(divFormat, ex.Message);
            }
            context.Response.Clear();
            context.Response.Write(exString);
        }
        context.ApplicationInstance.CompleteRequest();
    }

    private static string GetFileName(HttpContext context, string jobName)
    {
        return $@"""{EncodeFilenameIfNeeded(context, jobName)}.log""";
    }

    private static string EncodeFilenameIfNeeded(HttpContext context, string filename)
    {
        var browserType = context.Request.Browser.Type.ToUpperInvariant();

        filename = ReplaceInvalidFileNameChars(filename);

        if (browserType.StartsWith(@"IE"))
        {
            //Internet Explorer cuts not ANSI file name from the beginning if it is larger than 23 symbols
            //This code cuts file name from the end.
            if (!IsAnsiString(filename) && (filename.Length > IEMaxFileNameSize))
                filename = filename.Substring(0, IEMaxFileNameSize);

            return HttpUtility.UrlPathEncode(filename);
        }

        return filename;
    }

    private static bool IsAnsiString(string str)
    {
        const char compareCharacter = '\x7F';
        foreach (var c in str)
        {
            if (c > compareCharacter)
                return false;
        }
        return true;
    }

    private static string ReplaceInvalidFileNameChars(string Filename)
    {
        var newFilename = string.Empty;

        foreach (var ch in Filename)
        {
            if (Array.IndexOf(System.IO.Path.GetInvalidFileNameChars(), ch) >= 0)
            {
                newFilename += '_';
            }
            else
            {
                newFilename += ch;
            }
        }

        newFilename = newFilename.Replace(@";", @"_");

        return newFilename;
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}