﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" CodeFile="ExportConfigs.aspx.cs" Inherits="Orion_NCM_Resources_Jobs_ExportConfigs" %>

<%@ Register Src="~/Orion/NCM/Resources/Jobs/Controls/JobWorkflowContainer.ascx" TagName="JobWorkflowContainer" TagPrefix="ncm" %>
<%@ Register Src="~/Orion/NCM/Resources/Jobs/Controls/ExportConfigs.ascx" TagName="ExportConfigs" TagPrefix="ncm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmHeadPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" Runat="Server">
<%=Page.Title%>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmExportToPDFPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ncmCustomizePagePlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="ncmHelpIconPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
     <div style="width:80%;padding:10px;">
        <ncm:JobWorkflowContainer ID="jobWorkflowContainer" runat="server" 
             OnNextClick="Next_Click">
            <Content>
                <ncm:ExportConfigs ID="ExportConfigs" runat="server" />
            </Content>
        </ncm:JobWorkflowContainer>
    </div>
</asp:Content>