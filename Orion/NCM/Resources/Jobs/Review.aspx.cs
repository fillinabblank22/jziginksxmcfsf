﻿using System;
using SolarWinds.NCMModule.Web.Resources.JobManagement;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.Logging;
using SolarWinds.NCM.Contracts;

public partial class Orion_NCM_Resources_Jobs_Review : System.Web.UI.Page
{
    private bool isNewJob;
    private static Log _log = new Log();
    private ISWrapper isLayer = new ISWrapper();

    protected bool LoginCredentialsSupport
    {
        get
        {
            if (ViewState["LoginCredentialsSupport"] == null)
            {
                using (var proxy = isLayer.Proxy)
                {
                    var connectivityLevel = proxy.Cirrus.Settings.GetSetting(Settings.ConnectivityLevel, ConnectivityLevels.Default, null);
                    var on = connectivityLevel.Equals(ConnectivityLevels.Level3, StringComparison.InvariantCultureIgnoreCase);
                    ViewState["LoginCredentialsSupport"] = on;
                    return on;
                }
            }
            else
            {
                return Convert.ToBoolean(ViewState["LoginCredentialsSupport"]);
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        isNewJob = Convert.ToBoolean(Request.Params[@"NewJob"]);

        if (isNewJob)
        {
            Page.Title = Resources.NCMWebContent.WEBDATA_IC_140;
        }
        else
        {
            Page.Title = Resources.NCMWebContent.WEBDATA_IC_139;
        }

        ctlReview.LoadJob(JobWorkflowController.Job);
        if (!Page.User.Identity.Name.Equals(JobWorkflowController.Job.JobDefinition.OwnerSID, StringComparison.InvariantCultureIgnoreCase))
        {
            if (LoginCredentialsSupport)
            {
                switch (JobWorkflowController.Job.JobDefinition.JobType)
                {
                    case eJobType.CommandScript:
                    case eJobType.DownloadConfigs:
                    case eJobType.Upload:
                    case eJobType.Reboot:
                        jobWorkflowContainer.RegisterConfirmationMessageOnNextClick(Resources.NCMWebContent.WEBDATA_IC_157);
                        break;
                    default:
                        jobWorkflowContainer.RegisterConfirmationMessageOnNextClick(Resources.NCMWebContent.WEBDATA_VK_909);
                        break;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(JobWorkflowController.Job.Author))
                {
                    jobWorkflowContainer.RegisterConfirmationMessageOnNextClick(Resources.NCMWebContent.WEBDATA_VK_909);
                }
            }
        }
    }

    protected void Next_Click(object sender, NCMJobEventArgs e)
    {
        if (!ctlReview.IsValid)
        {
            InvalidatePage(ctlReview.ErrorMsg, e);
            return;
        }
        try
        {
            using (var proxy = isLayer.Proxy)
            {
                if (isNewJob)
                {
                    proxy.Cirrus.Jobs.AddJob(JobWorkflowController.Job); //on add job owner Sid is updated on CreateJob step
                }
                else
                {
                    JobWorkflowController.Job.Author = Page.User.Identity.Name;
                    JobWorkflowController.Job.JobDefinition.OwnerSID = Page.User.Identity.Name;
                    proxy.Cirrus.Jobs.UpdateJob(JobWorkflowController.Job);
                }
            }
        }
        catch (Exception ex)
        {
            _log.Error("Submit NCM job Error", ex);
            InvalidatePage(ex.Message, e);
        }
    }

    private void InvalidatePage(string error, NCMJobEventArgs e)
    {
        lblError.Text = error;
        lblError.Visible = true;
        e.ValidationPass = false;
    }

}
