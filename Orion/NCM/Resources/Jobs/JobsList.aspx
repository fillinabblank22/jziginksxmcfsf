﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="JobsList.aspx.cs" Inherits="JobsList" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/NCM/Controls/IconNCMSettings.ascx" TagPrefix="ncm" TagName="IconNCMSettings" %>
<%@ Register TagPrefix="ncm1" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmHeadPlaceHolder" Runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />

    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/PolicyReports/PolicyReport.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Resources/Jobs/Jobs.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/Progress.css" />

    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Resources/Jobs/JobsListViewer.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippetsHelper.js"></script>

    <ncm1:ExtLocalDateTimePattern ID="ExtLocalDateTimePattern1" runat="server"/>    
    <ncm1:ConfigSnippetUISettings ID="JobsListUISettings" runat="server" Name="NCM_JobsList_Columns" DefaultValue="[]" RenderTo="columnModel" />
   
    <style type="text/css">
        .jobReport
        {
            cursor: pointer;
        }
    </style>

    <script type="text/javascript">
        SW.NCM.IsAdministratorOrEngineer = <%=IsAdministratorOrEngineer.ToString().ToLowerInvariant() %>;
        SW.NCM.IsDemoMode = <%=SolarWinds.NCMModule.Web.Resources.CommonHelper.IsDemoMode().ToString().ToLowerInvariant() %>;
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmExportToPDFPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ncmCustomizePagePlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="ncmHelpIconPlaceHolder" Runat="Server">
    <ncm:IconNCMSettings ID="IconNCMSettings1" runat="server"/>
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMPHJobsList" />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
	<asp:Panel id="ContentContainer" runat="server">
        <div style="padding:10px;">
            <table class="ExistingElements" cellpadding="0" cellspacing="0" width="100%">
                <tr id="jobNotificationHolder" runat="server" visible="false">
                    <td style="padding-bottom:10px;">
                        <div class="Suggestion"><div class="Suggestion-Icon"></div>
                            <asp:Label ID="jobNotificationMsg"  runat="server"></asp:Label>
                            <div style="font-size:8pt;padding-top:5px;">&#0187;&nbsp;<a style="color:#336699;" href="<%=this.LearnMore(@"OrionNCMPHLearnMoreNoPurgeMainJobs") %>" target="_blank"><%=Resources.NCMWebContent.WEBDATA_VY_06%></a></div>
                        </div>
                    </td>
                </tr>
                <tr valign="top" align="left">                    
                    <td id="gridCell">
                        <div id="Grid" />
                    </td>                    
                </tr>
            </table>
            <div id="originalQuery">
            </div>
            <div id="test">
            </div>
            <pre id="stackTrace"></pre>
        </div>
        <div id="jobLogDialog" class="x-hidden">
            <div id="jobLogBody" class="x-panel-body" >
                <div id="jobLogTextArea"></div>
            </div>
        </div>
    </asp:Panel>
    <div style="padding:10px;" id="divError" runat="server"/>
</asp:Content>

