﻿using System;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;

public partial class Orion_NCM_Admin_Jobs_Default : System.Web.UI.Page
{
    public static NCMJob downloadjob;
    public static NCMJob changeReportjob;
    public static NCMJob PurgeConfigsjob;
    public static NCMJob updateInventoryJob;
    public static NCMJob routineDatabaseMaintenanceJob;

    public static NCMJob PolicyReportsJob;

    public static NCMJob executeScriptJob;

    private void CreateTestJob()
    {
        if (downloadjob == null)
        {
            changeReportjob = JobHelper.CreateDefaultNCMJob(eJobType.ConfigChangeReport);
            downloadjob = JobHelper.CreateDefaultNCMJob(eJobType.DownloadConfigs);
            PurgeConfigsjob = JobHelper.CreateDefaultNCMJob(eJobType.PurgeConfigs);
            updateInventoryJob = JobHelper.CreateDefaultNCMJob(eJobType.Inventory);
            routineDatabaseMaintenanceJob = JobHelper.CreateDefaultNCMJob(eJobType.DatabaseMaintenance);
            PolicyReportsJob = JobHelper.CreateDefaultNCMJob(eJobType.PolicyReport);
            executeScriptJob = JobHelper.CreateDefaultNCMJob(eJobType.CommandScript);


        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        Button1.Click += new EventHandler(Button1_Click);
        Button2.Click += new EventHandler(Button2_Click);

        CreateTestJob();      
        
        if (!Page.IsPostBack)
        {
            DownloadConfigsCtl.LoadJob(downloadjob);
            ConfigChanegReportCtl.LoadJob(changeReportjob);
            PurgeConfigsCtl.LoadJob(PurgeConfigsjob);
            UpdateInventoryCtl.LoadJob(updateInventoryJob);
            RoutineDatabaseMaintenanceCtl.LoadJob(routineDatabaseMaintenanceJob);
            PolicyReportsCtl.LoadJob(PolicyReportsJob);
            ExecuteScriptCtl.LoadJob(executeScriptJob);
            //TODO add other controls
        }

    }

    void Button2_Click(object sender, EventArgs e)
    {
        
    }

    void Button1_Click(object sender, EventArgs e)
    {
        bool rez = DownloadConfigsCtl.UpdateJob(downloadjob);
        rez = ConfigChanegReportCtl.UpdateJob(changeReportjob);
        rez = PurgeConfigsCtl.UpdateJob(PurgeConfigsjob);
        rez = UpdateInventoryCtl.UpdateJob(updateInventoryJob);
        rez = RoutineDatabaseMaintenanceCtl.UpdateJob(routineDatabaseMaintenanceJob);
        rez= PolicyReportsCtl.UpdateJob(PolicyReportsJob);
        rez = ExecuteScriptCtl.UpdateJob(executeScriptJob);
        //TODO add other controls
        if (rez) Page.Response.Redirect(Page.Request.Url.AbsoluteUri);
    }
}