﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" CodeFile="NodesNotDefined.aspx.cs" Inherits="Orion_NCM_Resources_Jobs_NotDefined" %>

<%@ Register Src="~/Orion/NCM/Resources/Jobs/Controls/JobWorkflowContainer.ascx" TagName="JobWorkflowContainer" TagPrefix="ncm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmHeadPlaceHolder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Resources/Jobs/Jobs.css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" Runat="Server">
<%=Page.Title%>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmExportToPDFPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ncmCustomizePagePlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="ncmHelpIconPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
    <div style="width:80%;padding:10px;">
        <ncm:JobWorkflowContainer ID="jobWorkflowContainer" runat="server">
            <Content>
                <div style="padding-top:30px;font-size:11pt;font-weight:bold;border-bottom:#e6e6dd 1px solid;padding-bottom:5px;"><%=Resources.NCMWebContent.WEBDATA_VK_787%></div>
                <div style="padding-top:10px;"><%=Resources.NCMWebContent.WEBDATA_VK_812%></div>
            </Content>
        </ncm:JobWorkflowContainer>
    </div>
</asp:Content>