﻿using System;
using SolarWinds.NCMModule.Web.Resources.JobManagement;

public partial class Orion_NCM_Resources_Jobs_ConfigChaangeTemplate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = jobWorkflowContainer.JobPageTitle;
        
        ScriptPreview.NetworkNodes = JobWorkflowController.Job.JobDefinition.Nodes.NetworkNodes;
        if (!Page.IsPostBack) ScriptPreview.LoadControl();
    }

    protected void Next_Click(object sender, NCMJobEventArgs e)
    {
        e.ValidationPass = true;
    }
}