﻿using System;
using SolarWinds.NCMModule.Web.Resources.JobManagement;

public partial class Orion_NCM_Resources_Jobs_DatabaseMaintenance : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = jobWorkflowContainer.JobPageTitle;
        if (!Page.IsPostBack)
        {
            RoutineDatabaseMaintenanceCtl.LoadJob(JobWorkflowController.Job);
        }
    }

    protected void Next_Click(object sender, NCMJobEventArgs e)
    {
        e.ValidationPass=  RoutineDatabaseMaintenanceCtl.UpdateJob(JobWorkflowController.Job);
    }
 
}