﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" CodeFile="DatabaseMaintenance.aspx.cs" Inherits="Orion_NCM_Resources_Jobs_DatabaseMaintenance" %>

<%@ Register Src="~/Orion/NCM/Resources/Jobs/Controls/JobWorkflowContainer.ascx" TagName="JobWorkflowContainer" TagPrefix="ncm" %>
<%@ Register Src="~/Orion/NCM/Resources/Jobs/Controls/RoutineDatabaseMaintenance.ascx" TagName="RoutineDatabaseMaintenance" TagPrefix="ncm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmHeadPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" Runat="Server">
<%=Page.Title%>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmExportToPDFPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ncmCustomizePagePlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="ncmHelpIconPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
     <div style="width:80%;padding:10px;">
        <ncm:JobWorkflowContainer ID="jobWorkflowContainer" runat="server" 
           OnNextClick="Next_Click">
            <Content>
                <div style="padding-top:30px;font-size:11pt;font-weight:bold;border-bottom:#e6e6dd 1px solid;padding-bottom:5px;"><%=Resources.NCMWebContent.WEBDATA_VM_182%></div>                
                 <div style="padding-top:30px">
                    <ncm:RoutineDatabaseMaintenance ID="RoutineDatabaseMaintenanceCtl"  runat="server" />
                 </div>
            </Content>
        </ncm:JobWorkflowContainer>
    </div>
</asp:Content>
