﻿<%@ Page  Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="DownloadConfigs.aspx.cs" Inherits="Orion_NCM_Resources_Jobs_CommandScript" %>

<%@ Register Src="~/Orion/NCM/Resources/Jobs/Controls/JobWorkflowContainer.ascx" TagName="JobWorkflowContainer" TagPrefix="ncm" %>
<%@ Register Src="~/Orion/NCM/Resources/Jobs/Controls/DownloadConfigs.ascx" TagName="DownloadConfigs" TagPrefix="ncm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmHeadPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" Runat="Server">
<%=Page.Title%>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmExportToPDFPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ncmCustomizePagePlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="ncmHelpIconPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
     <div style="width:80%;padding:10px;">
        <ncm:JobWorkflowContainer ID="jobWorkflowContainer" runat="server"          
            OnNextClick="Next_Click">
            <Content>
                <div style="padding-top:30px;font-size:11pt;font-weight:bold;border-bottom:#e6e6dd 1px solid;padding-bottom:5px;"><%=Resources.NCMWebContent.Jobs_DownloadConfigs_Title %></div>
                 <ncm:DownloadConfigs ID="DownloadConfigsCtl"  runat="server" />
            </Content>
        </ncm:JobWorkflowContainer>
    </div>
</asp:Content>

