﻿Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.QuickTips.init();

SW.NCM.JobsListViewer = function () {

    function gridResize() {
        if (grid != null) {
            grid.setWidth(1);
            grid.setWidth($('#gridCell').width());

            var h = getGridHeight();
            grid.setHeight(h);
        }
    }

    function getGridHeight() {
        var top = $('#gridCell').offset().top;
        return $(window).height() - top - $('#footer').height() - 75;
    }

    var searchHighlight = function () {
        var search = currentSearchTerm;
        if ($.trim(search).length == 0) return;

        var columnsToHighlight = SW.NCM.ColumnIndexByName(grid.getColumnModel(), ['@{R=NCM.Strings;K=WEBJS_VK_168;E=js}', '@{R=NCM.Strings;K=WEBJS_VK_170;E=js}']);
        Ext.each(columnsToHighlight, function (columnNumber) {
            SW.NCM.highlightSearchText("#Grid .x-grid3-body .x-grid3-td-" + columnNumber, search);
        });
    }

    var timerRunner;
    var autoRefresh = function () {
        var task = {
            run: function () {
                refreshJobStatus();
            },
            interval: 5000
        };
        timerRunner = new Ext.util.TaskRunner();
        timerRunner.start(task);
    }

    refreshJobStatus = function () {
        var jobIDs = [];
        grid.store.data.each(function (record, index, length) {
            if (record.data.Enabled || record.data.Status == 1 || record.data.Status == 3)
                jobIDs.push(record.data.ID);
        });

        if (jobIDs.length > 0) {
            var store = new ORION.WebServiceStore(
                "/Orion/NCM/Services/JobManagement.asmx/GetJobStatusPaged",
                [
                { name: 'ID', mapping: 0 },
                { name: 'Status', mapping: 1 },
                { name: 'DR', mapping: 2 },
                { name: 'NDR', mapping: 3 },
                { name: 'Log', mapping: 4 },
                { name: 'CompletedSubJobs', mapping: 5 },
                { name: 'AllSubJobs', mapping: 6 }
                ],
                "ID");

            store.addListener("exception", function (dataProxy, type, action, options, response, arg) {
                var error = eval("(" + response.responseText + ")");
                Ext.Msg.show({
                    title: 'Error',
                    msg: error.Message,
                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                });
            });

            store.proxy.conn.jsonData = { jobIDs: jobIDs };

            store.addListener("load", function (store) {
                store.each(function (record) {
                    var value = record.data.ID;
                    var recordIndex = grid.store.findExact("ID", value);
                    if (recordIndex >= 0) {
                        var recordToUpdate = grid.store.getAt(recordIndex);
                        recordToUpdate.set('Status', record.data.Status);
                        recordToUpdate.set('DR', record.data.DR);
                        recordToUpdate.set('NDR', record.data.NDR);
                        recordToUpdate.set('Log', record.data.Log);
                        recordToUpdate.set('CompletedSubJobs', record.data.CompletedSubJobs);
                        recordToUpdate.set('AllSubJobs', record.data.AllSubJobs);
                        recordToUpdate.commit();

                        searchHighlight();
                    }
                });
            });
            store.load();
        }
    }

    var currentSearchTerm = null;
    var refreshObjects = function (search, callback) {
        grid.store.removeAll();
        var pagingToolbar = grid.getBottomToolbar();
        pagingToolbar.cursor = 0;
        grid.store.baseParams['limit'] = grid.getBottomToolbar().pageSize;
        grid.store.proxy.conn.jsonData = { search: search };
        currentSearchTerm = search;
        pagingToolbar.doLoad(pagingToolbar.cursor);
    };

    var createNewJob = function (item) {
        window.location = "/Orion/NCM/Resources/Jobs/Default.aspx";
    };

    var editJob = function (item) {
        window.location = "/Orion/NCM/Resources/Jobs/Default.aspx?JobID=" + item.data.ID;
    };

    var duplicateJob = function (item) {
        window.location = "/Orion/NCM/Resources/Jobs/Default.aspx?JobID=" + item.data.ID + "&DuplicateJob=True";
    };

    var enableJobs = function (items) {
        var jobIDs = getDisabledJobIDs(items);
        if (jobIDs.length > 0) {
            var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_185;E=js}');

            internalEnableJobs(jobIDs, function (result) {
                waitMsg.hide();
                ErrorHandler(result);
                if (result == null) {
                    jobIDs.forEach(function (jobID) {
                        var recordIndex = grid.store.findExact("ID", jobID);
                        if (recordIndex >= 0) {
                            var recordToUpdate = grid.store.getAt(recordIndex);
                            if (recordToUpdate.data.Status == 2) {
                                recordToUpdate.set('Status', 3);
                                recordToUpdate.set('Enabled', true);
                                recordToUpdate.commit();
                            }
                        }
                    });
                    updateToolbarButtons();
                }
            });
        }
    };

    var internalEnableJobs = function (jobIDs, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/JobManagement.asmx",
        "EnableOrDisableJobs", { jobIDs: jobIDs, enableOrDisable: true },
        function (result) {
            onSuccess(result);
        });
    };

    var disableJobs = function (items) {
        var jobIDs = getEnabledJobIDs(items);
        if (jobIDs.length > 0) {
            var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_186;E=js}');

            internalDisableJobs(jobIDs, function (result) {
                waitMsg.hide();
                ErrorHandler(result);
                if (result == null) {
                    jobIDs.forEach(function (jobID) {
                        var recordIndex = grid.store.findExact("ID", jobID);
                        if (recordIndex >= 0) {
                            var recordToUpdate = grid.store.getAt(recordIndex);
                            if (recordToUpdate.data.Status == 3) {
                                recordToUpdate.set('Status', 2);
                                recordToUpdate.set('Enabled', false);
                                recordToUpdate.set('NDR', null);
                                recordToUpdate.commit();
                            }
                        }
                    });
                    updateToolbarButtons();
                }
            });
        }
    };

    var internalDisableJobs = function (jobIDs, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/JobManagement.asmx",
        "EnableOrDisableJobs", { jobIDs: jobIDs, enableOrDisable: false },
        function (result) {
            onSuccess(result);
        });
    };

    var deleteJobs = function (items) {
        var jobIDs = getSelectedJobIDs(items);
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_174;E=js}');

        internalDeleteJobs(jobIDs, function (result) {
            waitMsg.hide();
            ErrorHandler(result);
            ReloadGridStore();
        });
    };

    var internalDeleteJobs = function (jobIDs, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/JobManagement.asmx",
        "DeleteJobs", { jobIDs: jobIDs },
        function (result) {
            onSuccess(result);
        });
    };

    var getSelectedJobIDs = function (items) {
        var jobIDs = [];

        Ext.each(items, function (item) {
            jobIDs.push(item.data.ID);
        });

        return jobIDs;
    };

    var getEnabledJobIDs = function (items) {
        var jobIDs = [];

        Ext.each(items, function (item) {
            if (item.data.Enabled)
                jobIDs.push(item.data.ID);
        });

        return jobIDs;
    };

    var getDisabledJobIDs = function (items) {
        var jobIDs = [];

        Ext.each(items, function (item) {
            if (!item.data.Enabled)
                jobIDs.push(item.data.ID);
        });

        return jobIDs;
    };

    var startJob = function (item) {
        var jobID = item.data.ID;
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_175;E=js}');

        internalStartJob(jobID, function (result) {
            waitMsg.hide();
            ErrorHandler(result);
        });
    };

    var internalStartJob = function (jobID, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/JobManagement.asmx",
        "StartJob", { jobID: jobID },
        function (result) {
            onSuccess(result);
        });
    };

    var stopJob = function (item) {
        var jobID = item.data.ID;
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_176;E=js}');

        internalStopJob(jobID, function (result) {
            waitMsg.hide();
            ErrorHandler(result);
        });
    };

    var internalStopJob = function (jobID, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/JobManagement.asmx",
        "StopJob", { jobID: jobID },
        function (result) {
            onSuccess(result);
        });
    };

    function jobNameClick(obj) {
        var jobID = obj.attr("value");
        window.location = "/Orion/NCM/Resources/Jobs/Default.aspx?JobID=" + jobID;
    };

    function jobReportClick(obj) {
        var jobID = obj.attr('jobID');
        var jobName = obj.attr('jobName');
        showJobLog(jobID, jobName);
    };

    function renderJobName(value, meta, record) {
        return String.format('<a href="#" style="font-weight:bold;" class="jobName" value="{0}">{1}</a>', record.data.ID, Ext.util.Format.htmlEncode(value));
    }

    function renderJobStatus(value, meta, record) {
        switch (value) {
            case 1:
                var jobType = record.data.Type;
                if (jobType == 0 || jobType == 2 || jobType == 4 || jobType == 5 || jobType == 8 || jobType == 11 || jobType == 15) {
                    var completed = record.data.CompletedSubJobs;
                    var all = record.data.AllSubJobs;
                    var percentage = (completed || all) ? Math.round(completed / all * 100) : 0;
                    var message = completed === all ? 
                        String.format("@{R=NCM.Strings;K=JOB_STATUS_RUNNING_BACKGROUND_PROCESSING;E=js}") 
                        : String.format("@{R=NCM.Strings;K=JOB_STATUS_RUNNING;E=js} {0} %", percentage);

                    return String.format('<div class="sw-progress"><div class="sw-progress-text">{0}</div><div class="sw-progress-bar" style="width:{1}%;"></div></div>', message, percentage);
                }
                else
                    return "@{R=NCM.Strings;K=JOB_STATUS_RUNNING;E=js}";
            case 2:
                return "@{R=NCM.Strings;K=JOB_STATUS_DISABLED;E=js}";
            case 3:
                return "@{R=NCM.Strings;K=JOB_STATUS_SCHEDULED;E=js}";
            case 4:
                return "@{R=NCM.Strings;K=JOB_STATUS_NOT_SCHEDULED;E=js}";
            default:
                return "";
        }
    }

    function renderJobType(value, meta, record) {
        switch (value) {
            case 0:
                return "@{R=NCM.Strings;K=JOB_TYPE_COMMAND_SCRIPT;E=js}";
            case 2:
                return "@{R=NCM.Strings;K=JOB_TYPE_UPLOAD;E=js}";
            case 3:
                return "@{R=NCM.Strings;K=JOB_TYPE_REPORT;E=js}";
            case 4:
                return "@{R=NCM.Strings;K=JOB_TYPE_REBOOT;E=js}";
            case 5:
                return "@{R=NCM.Strings;K=JOB_TYPE_EXPORT_CONFIGS;E=js}";
            case 6:
                return "@{R=NCM.Strings;K=JOB_TYPE_DATABASE_MAINTENANCE;E=js}";
            case 7:
                return "@{R=NCM.Strings;K=JOB_TYPE_INVENTORY;E=js}";
            case 8:
                return "@{R=NCM.Strings;K=JOB_TYPE_CONFIG_CHANGE_REPORT;E=js}";
            case 9:
                return "@{R=NCM.Strings;K=JOB_TYPE_POLICY_REPORT;E=js}";
            case 10:
                return "@{R=NCM.Strings;K=JOB_TYPE_PURGE_CONFIGS;E=js}";
            case 11:
                return "@{R=NCM.Strings;K=JOB_TYPE_DOWNLOAD_CONFIGS;E=js}";
            case 14:
                return "@{R=NCM.Strings;K=JOB_TYPE_BASELINE_ENTIRE_NETWORK;E=js}";
            case 15:
                return "@{R=NCM.Strings;K=JOB_TYPE_CONFIG_CHANGE_TEMPLATE;E=js}";
            default:
                return "@{R=NCM.Strings;K=JOB_TYPE_UNKNOWN;E=js}";
        }
    }
    
    function renderDateTime(value, meta, record) {
        if (value == null) return '@{R=NCM.Strings;K=WEBJS_VK_184;E=js}';

        var date = eval(value.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
        return date.toLocaleString();
    }

    function renderNextDateRun(value, meta, record) {
        if (value == null) return '';

        var date = eval(value.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
        return date.toLocaleString();
    }

    function renderReport(value, meta, record) {
        if (value == null) //no log data yet
            return '';
        else
            return String.format('<img class="jobReport" jobID="{0}" jobName="{1}" src="/Orion/NCM/Resources/images/InventoryIcons/icon_inventory_report.gif" />', record.data.ID, Ext.util.Format.htmlEncode(record.data.Name));
    }

    var doClean = function () {
        var map = grid.getTopToolbar().items.map;
        map.Clean.setVisible(false);
        map.txtSearch.setValue('');
        refreshObjects("");
    }

    var doSearch = function () {
        var map = grid.getTopToolbar().items.map;
        var search = map.txtSearch.getValue();
        if ($.trim(search).length == 0) return;
        refreshObjects(search);
        map.Clean.setVisible(true);
    }

    var updateToolbarButtons = function () {
        var selCount = grid.getSelectionModel().getCount();
        var map = grid.getTopToolbar().items.map;

        var needsOnlyOneSelected = (selCount != 1);
        var needsAtLeastOneSelected = (selCount === 0);

        var items = grid.getSelectionModel().getSelections();
        var enabledJobs = getEnabledJobIDs(items);
        var disabledJobs = getDisabledJobIDs(items);

        map.EditJob.setDisabled(needsOnlyOneSelected);
        map.DuplicateJob.setDisabled(needsOnlyOneSelected);
        map.StartJob.setDisabled(needsOnlyOneSelected || disabledJobs.length == 1);
        map.StopJob.setDisabled(needsOnlyOneSelected || disabledJobs.length == 1);
        map.EnableJobs.setDisabled(SW.NCM.IsDemoMode ? true : disabledJobs.length == 0);
        map.DisableJobs.setDisabled(SW.NCM.IsDemoMode ? true : enabledJobs.length == 0);
        map.DeleteJobs.setDisabled(SW.NCM.IsDemoMode ? true : needsAtLeastOneSelected);

        var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');
        if (grid.getStore().getCount() > 0 && selCount == grid.getStore().getCount()) {
            hd.addClass('x-grid3-hd-checker-on');
        } else {
            hd.removeClass('x-grid3-hd-checker-on');
        }
    };

    function showJobLog(jobID, jobName) {
        var jobLogDiv = $("#jobLogTextArea");
        jobLogDiv.empty();

        var loadMask = new Ext.LoadMask(Ext.get('Grid'), {
            msg: '@{R=NCM.Strings;K=WEBJS_VK_04;E=js}',
            removeMask: true
        });
        loadMask.show();

        ORION.callWebService("/Orion/NCM/Services/JobManagement.asmx",
                             "GetJobLog", { jobID: jobID },
                             function (result) {
                                 loadMask.hide();

                                 var jobLogTextArea = new Ext.form.TextArea({
                                     id: 'jobLogID',
                                     renderTo: 'jobLogTextArea',
                                     style: 'border: none;',
                                     width: 580,
                                     height: 320,
                                     readOnly: true,
                                     emptyText: '@{R=NCM.Strings;K=WEBJS_VK_187;E=js}',
                                     value: result
                                 });

                                 return ShowJobLogDialog(String.format('@{R=NCM.Strings;K=WEBJS_VK_177;E=js}', jobName));
                             });
    }

    var showJobLogDialog;
    ShowJobLogDialog = function (title) {
        if (!showJobLogDialog) {
            showJobLogDialog = new Ext.Window({
                applyTo: 'jobLogDialog',
                layout: 'fit',
                width: 600,
                height: 400,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                modal: true,
                contentEl: 'jobLogBody',
                buttonAlign: 'left',
                buttons: [{
                    text: '@{R=NCM.Strings;K=WEBJS_VK_189;E=js}',
                    disabled: SW.NCM.IsDemoMode,
                    handler: function () {
                        var item = grid.getSelectionModel().getSelected();
                        ORION.callWebService("/Orion/NCM/Services/JobManagement.asmx",
                             "ClearJobLog", { jobID: item.data.ID },
                             function (result) {
                                 if (result) {
                                     Ext.getCmp('jobLogID').setValue('');
                                     var recordIndex = grid.store.findExact("ID", item.data.ID);
                                     if (recordIndex >= 0) {
                                         var recordToUpdate = grid.store.getAt(recordIndex);
                                         recordToUpdate.set('Log', null);
                                         recordToUpdate.commit();
                                     }
                                 }
                             });
                    }
                }, '->', {
                    text: '@{R=NCM.Strings;K=WEBJS_VK_188;E=js}',
                    handler: function () {
                        var item = grid.getSelectionModel().getSelected();
                        $.download('/Orion/NCM/Resources/Jobs/SaveJobLog.ashx', 'JobID=' + item.data.ID + "&JobName=" + item.data.Name);
                    }
                }, {
                    text: '@{R=NCM.Strings;K=WEBJS_VK_07;E=js}',
                    handler: function () {
                        showJobLogDialog.hide();
                    }
                }]
            });
        }

        showJobLogDialog.setTitle(Ext.util.Format.htmlEncode(title));

        showJobLogDialog.alignTo(document.body, "c-c");
        showJobLogDialog.show();

        return false;
    }

    function ErrorHandler(result) {
        if (result != null && result.Error) {
            Ext.Msg.show({
                title: result.Source,
                msg: result.Msg,
                minWidth: 500,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        }
    }

    function ReloadGridStore() {
        var currentPageRecordCount = grid.store.getCount();
        var selectedRecordCount = grid.getSelectionModel().getCount();
        if (currentPageRecordCount > selectedRecordCount) {
            grid.store.reload();
        }
        else {
            grid.store.load();
        }
    }

    gridColumnChangedHandler = function (component, state) {
        if (component == null)
            return;

        var gridCM = component.getColumnModel();
        var settingName = 'Columns';

        if (gridCM != null && settingName != '') {

            var jsonvalue = SW.NCM.GridColumnsToJson(gridCM);
            ORION.Prefs.save(settingName, jsonvalue);
        }
    }

    ORION.prefix = "NCM_JobsList_";

    var selectorModel;
    var dataStore;
    var grid;

    return {
        init: function () {

            if ($("[id$='_ContentContainer']").length == 0)
                return false;

            $("#Grid").click(function (e) {

                var obj = $(e.target);

                if (obj.hasClass("ncm_searchterm"))
                    obj = obj.parent();

                if (obj.hasClass('jobName')) {
                    jobNameClick(obj);
                    return false;
                }

                if (obj.hasClass('jobReport')) {
                    jobReportClick(obj);
                    return false;
                }
            });

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", updateToolbarButtons);

            dataStore = new ORION.WebServiceStore(
                                "/Orion/NCM/Services/JobManagement.asmx/GetJobsPaged",
                                [
                                    {name: 'ID', mapping: 0 }, //NCMJobID
                                    {name: 'Name', mapping: 1 }, //NCMJobName
                                    {name: 'Type', mapping: 2 }, //NCMJobType
                                    {name: 'Author', mapping: 3 }, //NCMJobCreator
                                    {name: 'DR', mapping: 4 }, //LastDateRun
                                    {name: 'NDR', mapping: 5 }, //NextDateRun
                                    {name: 'DC', mapping: 6 }, //NCMJobDateCreated
                                    {name: 'DM', mapping: 7 }, //NCMJobDateModified
                                    {name: 'Enabled', mapping: 8 },
                                    {name: 'Status', mapping: 9 }, //JobStatus
                                    {name: 'Log', mapping: 10 }, //JobLog
                                    {name: 'CompletedSubJobs', mapping: 11 }, 
                                    {name: 'AllSubJobs', mapping: 12 }
                                ],
                                "Name");

            var comboPS = new Ext.form.ComboBox({
                name: 'perpage',
                width: 50,
                store: new Ext.data.SimpleStore({
                    fields: ['id'],
                    data: [
                                  ['25'],
                                  ['50'],
                                  ['75'],
                                  ['100']
                                ]
                }),
                mode: 'local',
                value: '25',
                listWidth: 50,
                triggerAction: 'all',
                displayField: 'id',
                valueField: 'id',
                editable: false,
                forceSelection: true
            });

            var pagingToolbar = new Ext.PagingToolbar({
                store: dataStore,
                pageSize: 25,
                displayInfo: true,
                displayMsg: '@{R=NCM.Strings;K=WEBJS_VK_166;E=js}',
                emptyMsg: '@{R=NCM.Strings;K=WEBJS_VK_167;E=js}',
                items: ['-', '@{R=NCM.Strings;K=WEBJS_VK_12;E=js}', comboPS]
            });

            comboPS.on('select', function (combo, record) {
                var psize = parseInt(record.get('id'), 10);
                grid.store.baseParams['limit'] = psize;
                pagingToolbar.pageSize = psize;
                pagingToolbar.cursor = 0;
                pagingToolbar.doLoad(pagingToolbar.cursor);
            }, this);

            grid = new Ext.grid.GridPanel({
                id: 'gridControl',
                store: dataStore,

                columns: [selectorModel, {
                    header: 'ID',
                    width: 80,
                    hidden: true,
                    hideable: false,
                    sortable: true,
                    dataIndex: 'ID' //NCMJobID
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_168;E=js}',
                    width: 300,
                    sortable: true,
                    dataIndex: 'Name', //NCMJobName
                    css: 'height:23px;',
                    renderer: renderJobName
                }, {
                    header: '@{R=NCM.Strings;K=Jobs_ListViewer_Status;E=js}',
                    width: 150,
                    sortable: true,
                    dataIndex: 'Status',
                    css: 'height:23px;',
                    renderer: renderJobStatus
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_169;E=js}',
                    width: 300,
                    sortable: true,
                    dataIndex: 'Type', //NCMJobType
                    css: 'height:23px;',
                    renderer: renderJobType
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_170;E=js}',
                    width: 150,
                    sortable: true,
                    dataIndex: 'Author', //NCMJobCreator
                    css: 'height:23px;',
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_182;E=js}',
                    width: 150,
                    dataIndex: 'DR', //LastDateRun
                    sortable: true,
                    css: 'height:23px;',
                    renderer: renderDateTime
                }, {
                    header: '@{R=NCM.Strings;K=Jobs_ListViewer_NextDateRun;E=js}',
                    width: 150,
                    dataIndex: 'NDR', //NextDateRun
                    sortable: true,
                    css: 'height:23px;',
                    renderer: renderNextDateRun
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_171;E=js}',
                    width: 150,
                    hidden: true,
                    sortable: true,
                    dataIndex: 'DC', //NCMJobDateCreated
                    css: 'height:23px;',
                    renderer: renderDateTime
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_183;E=js}',
                    width: 150,
                    hidden: true,
                    sortable: true,
                    dataIndex: 'DM', //NCMJobDateModified
                    css: 'height:23px;',
                    renderer: renderDateTime
                }, {
                    header: '@{R=NCM.Strings;K=Jobs_ListViewer_Enabled;E=js}',
                    width: 80,
                    hidden: true,
                    hideable: false,
                    sortable: true,
                    dataIndex: 'Enabled'
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_173;E=js}',
                    width: 100,
                    sortable: true,
                    dataIndex: 'Log', //JobLog
                    css: 'height:23px;',
                    renderer: renderReport
                }
                ],

                sm: selectorModel,

                layout: 'fit',
                autoScroll: 'true',
                loadMask: true,
                stateful: true,
                stateId: 'gridState',
                width: 750,
                height: 500,
                stripeRows: true,

                tbar: [{
                    id: 'CreateNewJob',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_161;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_60;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_addsnippet.gif',
                    cls: 'x-btn-text-icon',
                    hidden: !SW.NCM.IsAdministratorOrEngineer,
                    handler: function () { createNewJob(); }
                }, !SW.NCM.IsAdministratorOrEngineer ? '' : '-', {
                    id: 'EditJob',
                    text: '@{R=NCM.Strings;K=WEBJS_VY_03;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_61;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_editsnippet.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { editJob(grid.getSelectionModel().getSelected()); }
                }, '-', {
                    id: 'DuplicateJob',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_180;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_181;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_clone.gif',
                    cls: 'x-btn-text-icon',
                    hidden: !SW.NCM.IsAdministratorOrEngineer,
                    handler: function () { duplicateJob(grid.getSelectionModel().getSelected()); }
                }, !SW.NCM.IsAdministratorOrEngineer ? '' : '-', {
                    id: 'StartJob',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_65;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_162;E=js}',
                    icon: '/Orion/NCM/Resources/images/Jobs/icon_start.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        Ext.Msg.show({
                            title: '@{R=NCM.Strings;K=WEBJS_VK_65;E=js}',
                            msg: '@{R=NCM.Strings;K=WEBJS_VM_81;E=js}',
                            minWidth: 500,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO,
                            fn: function () { startJob(grid.getSelectionModel().getSelected()); }
                        });
                    }
                }, '-', {
                    id: 'StopJob',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_163;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_164;E=js}',
                    icon: '/Orion/NCM/Resources/images/Jobs/icon_stop.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { stopJob(grid.getSelectionModel().getSelected()); }
                }, '-', {
                    id: 'EnableJobs',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_140;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_179;E=js}',
                    icon: '/Orion/NCM/Resources/images/icon_ok.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { enableJobs(grid.getSelectionModel().getSelections()); }
                }, '-', {
                    id: 'DisableJobs',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_142;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_178;E=js}',
                    icon: '/Orion/NCM/Resources/images/disable_icon.png',
                    cls: 'x-btn-text-icon',
                    handler: function () { disableJobs(grid.getSelectionModel().getSelections()); }
                }, '-', {
                    id: 'DeleteJobs',
                    text: '@{R=NCM.Strings;K=WEBJS_VY_05;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_62;E=js}',
                    icon: '/Orion/NCM/Resources/images/icon_delete.png',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        Ext.Msg.confirm('@{R=NCM.Strings;K=WEBJS_VK_63;E=js}',
                                '@{R=NCM.Strings;K=WEBJS_VK_64;E=js}',
                                function (btn, text) {
                                    if (btn == 'yes') {
                                        deleteJobs(grid.getSelectionModel().getSelections());
                                    }
                                });
                    }
                }, '->', {
                    id: 'txtSearch',
                    xtype: 'textfield',
                    emptyText: '@{R=NCM.Strings;K=WEBJS_VK_165;E=js}',
                    width: 200,
                    enableKeyEvents: true,
                    listeners: {
                        keypress: function (obj, evnt) {
                            if (evnt.keyCode == 13) {
                                doSearch();
                            }
                        }
                    }
                }, {
                    id: 'Clean',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_30;E=js}',
                    iconCls: 'ncm_clear-btn',
                    cls: 'x-btn-icon',
                    hidden: true,
                    handler: function () { doClean(); }
                }, {
                    id: 'Search',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_31;E=js}',
                    iconCls: 'ncm_search-btn',
                    cls: 'x-btn-icon',
                    handler: function () { doSearch(); }
                }],

                bbar: pagingToolbar

            });

            var gridCM = SW.NCM.GetDefaultColumnModelIfNeeded(columnModel);
            SW.NCM.PersonalizeGrid(grid, gridCM);
            grid.on("statesave", gridColumnChangedHandler);

            grid.render('Grid');

            updateToolbarButtons();

            gridResize();

            $("form").submit(function () { return false; });

            refreshObjects("");

            $(window).resize(function () {
                gridResize();
            });

            autoRefresh();

            grid.getView().on("refresh", function () {
                var search = currentSearchTerm;
                if ($.trim(search).length == 0) return;

                searchHighlight();
            });
        }
    };

} ();

Ext.onReady(SW.NCM.JobsListViewer.init, SW.NCM.JobsListViewer);

//jquery plugin for file downloading
jQuery.download = function (url, data, method) {
    //url and data options required
    if (url && data) {
        //data can be string of parameters or array/object
        data = typeof data == 'string' ? data : jQuery.param(data);
        //split params into form inputs
        var inputs = '';
        jQuery.each(data.split('&'), function () {
            var pair = this.split('=');
            inputs += '<input type="hidden" name="' + pair[0] + '" value="' + pair[1] + '" />';
        });
        //send request
        jQuery('<form action="' + url + '" method="' + (method || 'post') + '">' + inputs + '</form>')
		.appendTo('body').submit().remove();
    };
};