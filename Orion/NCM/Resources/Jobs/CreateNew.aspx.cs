﻿using System;
using System.Web.UI.WebControls;
using SolarWinds.NCM.Common.Help;
using SolarWinds.NCMModule.Web.Resources.JobManagement;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_NCM_Resources_Jobs_CreateNew : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = jobWorkflowContainer.JobPageTitle;

        if (!Page.IsPostBack)
        {
            txtJobName.Text = JobWorkflowController.Job.JobName;
            txtJobComments.Text = JobWorkflowController.Job.JobDefinition.Comments;

            if (JobWorkflowController.Job != null)
            {
                lJobType.SelectedValue = JobWorkflowController.Job.JobDefinition.JobType.ToString();                
            }
            else
            {
                lJobType.SelectedIndex = 1;
            }

            if (JobWorkflowController.Job.IsPrePolulated) //hide job selection for auto-generate jobs (currently only schedule Config Change Template feature)
            {
                JobTypePanel.Visible = false;
            }

            jobSchedule.LoadJob(JobWorkflowController.Job);

        }

    }


    protected void Next_Click(object sender, NCMJobEventArgs e)
    {
                
       
        if (JobWorkflowController.Job.JobID == -1)  //new job 
        {
            if (!JobWorkflowController.Job.IsPrePolulated)
            {
                eJobType selectedJobType = (eJobType)Enum.Parse(typeof(eJobType), lJobType.SelectedValue);
                JobWorkflowController.Job = JobHelper.CreateDefaultNCMJob(selectedJobType);
            }
        }
        else
        {
            //duplicate job
            eJobType selectedJobType = (eJobType)Enum.Parse(typeof(eJobType), lJobType.SelectedValue);
            JobWorkflowController.Job = JobHelper.CreateDuplicateNCMJob(selectedJobType, JobWorkflowController.Job);

        }
        JobWorkflowController.Job.Enabled = true; //anable by default for cloned jobs
        JobWorkflowController.Job.Author = Page.User.Identity.Name;
        JobWorkflowController.Job.JobDefinition.OwnerSID = Page.User.Identity.Name;
        JobWorkflowController.Job.JobName = txtJobName.Text;
        JobWorkflowController.Job.JobDefinition.Comments = txtJobComments.Text;

        e.ValidationPass = jobSchedule.UpdateJob(JobWorkflowController.Job);
    }

    protected string HelpMeDecideLink
    {
        get
        {
            var helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
            return helpHelperWrap.GetHelpUrl("OrionNCMPHHelpMeDecideJobType");
        }
    }

    protected override void OnInit(EventArgs e)
    {
        InitJobTypeList();
        base.OnInit(e);
    }

    private void InitJobTypeList()
    {
        lJobType.Items.Add(new ListItem() { Text = JobHelper.LookupLocalizedJobType(eJobType.CommandScript), Value = eJobType.CommandScript.ToString() });
        lJobType.Items.Add(new ListItem() { Text = JobHelper.LookupLocalizedJobType(eJobType.Upload), Value = eJobType.Upload.ToString() });
        lJobType.Items.Add(new ListItem() { Text = JobHelper.LookupLocalizedJobType(eJobType.ExportConfigs), Value = eJobType.ExportConfigs.ToString() });
        lJobType.Items.Add(new ListItem() { Text = JobHelper.LookupLocalizedJobType(eJobType.DatabaseMaintenance), Value = eJobType.DatabaseMaintenance.ToString() });
        lJobType.Items.Add(new ListItem() { Text = JobHelper.LookupLocalizedJobType(eJobType.Inventory), Value = eJobType.Inventory.ToString() });
        lJobType.Items.Add(new ListItem() { Text = JobHelper.LookupLocalizedJobType(eJobType.ConfigChangeReport), Value = eJobType.ConfigChangeReport.ToString() });
        lJobType.Items.Add(new ListItem() { Text = JobHelper.LookupLocalizedJobType(eJobType.PolicyReport), Value = eJobType.PolicyReport.ToString() });
        lJobType.Items.Add(new ListItem() { Text = JobHelper.LookupLocalizedJobType(eJobType.PurgeConfigs), Value = eJobType.PurgeConfigs.ToString() });
        lJobType.Items.Add(new ListItem() { Text = JobHelper.LookupLocalizedJobType(eJobType.DownloadConfigs), Value = eJobType.DownloadConfigs.ToString() });
        lJobType.Items.Add(new ListItem() { Text = JobHelper.LookupLocalizedJobType(eJobType.Reboot), Value = eJobType.Reboot.ToString() });
    }
}