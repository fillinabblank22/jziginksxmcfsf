﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="Test.aspx.cs" Inherits="Orion_NCM_Admin_Jobs_Default" %>

<%@ Register Src="~/Orion/NCM/Resources/Jobs/Controls/DownloadConfigs.ascx" TagName="DownloadConfigs" TagPrefix="ncm" %>
<%@ Register Src="~/Orion/NCM/Resources/Jobs/Controls/ConfigChangeReport.ascx" TagName="ConfigChangeReport" TagPrefix="ncm" %>
<%@ Register Src="~/Orion/NCM/Resources/Jobs/Controls/PurgeConfigs.ascx" TagName="PurgeConfigs" TagPrefix="ncm" %>
<%@ Register Src="~/Orion/NCM/Resources/Jobs/Controls/UpdateInventory.ascx" TagName="UpdateInventory" TagPrefix="ncm" %>
<%@ Register Src="~/Orion/NCM/Resources/Jobs/Controls/RoutineDatabaseMaintenance.ascx" TagName="RoutineDatabaseMaintenance" TagPrefix="ncm" %>



<%@ Register Src="~/Orion/NCM/Resources/Jobs/Controls/PolicyReports.ascx" TagName="PolicyReports" TagPrefix="ncm" %>


<%@ Register Src="~/Orion/NCM/Resources/Jobs/Controls/ExecuteScript.ascx" TagName="ExecuteScript" TagPrefix="ncm" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="adminPageTitlePlaceHolder">
	Test Jobs
</asp:Content>



<asp:Content ID="Content3" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
<asp:Panel ID="ContentContainer" style="width:80%;padding:10px" runat="server">
    <div style ="border:solid 1px #dcdbd7;padding:10px;background-color:#fff">
     <ncm:DownloadConfigs ID="DownloadConfigsCtl"  runat="server" />
     </div>
     <div style ="border:solid 1px #dcdbd7;padding:10px;background-color:#fff">
     <ncm:ConfigChangeReport ID="ConfigChanegReportCtl"  runat="server" />
     </div>
     <div style ="border:solid 1px #dcdbd7;padding:10px;background-color:#fff">
     <ncm:PurgeConfigs ID="PurgeConfigsCtl"  runat="server" />
     </div>
    <div style ="border:solid 1px #dcdbd7;padding:10px;background-color:#fff">
     <ncm:UpdateInventory ID="UpdateInventoryCtl"  runat="server" />
    </div>
    <div style ="border:solid 1px #dcdbd7;padding:10px;background-color:#fff">
     <ncm:RoutineDatabaseMaintenance ID="RoutineDatabaseMaintenanceCtl"  runat="server" />
    </div>
    <div style ="border:solid 1px #dcdbd7;padding:10px;background-color:#fff">
     <ncm:PolicyReports ID="PolicyReportsCtl"  runat="server" />    
    </div>
    <div style ="border:solid 1px #dcdbd7;padding:10px;background-color:#fff">
     <ncm:ExecuteScript ID="ExecuteScriptCtl" runat="server" />
    </div>
</asp:Panel>
<asp:Button ID="Button1" runat="server" Text="Submit" />
<asp:Button ID="Button2" runat="server" Text="Test PostBack" />

</asp:Content>
