﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.JobManagement;
using SolarWinds.NCM.Contracts.InformationService;

public partial class Orion_NCM_Resources_Jobs_ReviewNodes : System.Web.UI.Page
{
    private List<NetworkNode> networkNodes;

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = jobWorkflowContainer.JobPageTitle;

        networkNodes = JobWorkflowController.Job.JobDefinition.Nodes.NetworkNodes;
        var limitationNodeIDList = SecurityHelper.GetLimitationNodeIDList();
        networkNodes = networkNodes.Where(item => limitationNodeIDList.Contains(item.NodeID)).ToList();
        Nodes.NetworkNodes = networkNodes;
    }

    protected void Next_Click(object sender, NCMJobEventArgs e)
    {
        JobWorkflowController.Job.JobDefinition.Nodes.NetworkNodes = networkNodes;
        e.ValidationPass = true;
    }
}