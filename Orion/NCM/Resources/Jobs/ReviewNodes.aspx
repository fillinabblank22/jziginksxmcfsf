﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="ReviewNodes.aspx.cs" Inherits="Orion_NCM_Resources_Jobs_ReviewNodes" %>

<%@ Register Src="~/Orion/NCM/Resources/Jobs/Controls/JobWorkflowContainer.ascx" TagName="JobWorkflowContainer" TagPrefix="ncm" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.PolicyReportsManagement.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmHeadPlaceHolder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Resources/Jobs/Jobs.css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmExportToPDFPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ncmCustomizePagePlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="ncmHelpIconPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
    <div style="width:80%;padding:10px;">
        <ncm:JobWorkflowContainer ID="jobWorkflowContainer" runat="server" OnNextClick="Next_Click">
            <Content>
                <div class="JobHeader"><%=Resources.NCMWebContent.WEBDATA_VK_845%></div>    
                <div style="padding-top:10px;"><%=Resources.NCMWebContent.WEBDATA_VK_846%></div>
                <div style="padding-top:20px;width:50%;">
                    <asp:UpdatePanel ID="ContentUpdatePanel" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <ncm:SelectNodes ID="Nodes" runat="server" NodeSelectionText="<%$ Resources: NCMWebContent, WEBDATA_VK_842 %>" ShowSelectNodesButton="false" ShowDeleteNodeButton="false" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </Content>
        </ncm:JobWorkflowContainer>
    </div>
</asp:Content>

