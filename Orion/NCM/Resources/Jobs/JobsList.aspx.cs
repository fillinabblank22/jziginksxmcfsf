﻿using System;
using System.Data;
using SolarWinds.NCM.Common.Help;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;

public partial class JobsList : System.Web.UI.Page
{
    private ISWrapper isLayer = new ISWrapper();

    protected override void OnInit(EventArgs e)
    {
        var validator = new SWISValidator();
        validator.ContentContainer = ContentContainer;
        validator.RedirectIfValidationFailed = true;
        divError.Controls.Add(validator);

        if (!SolarWinds.NCMModule.Web.Resources.CommonHelper.IsDemoMode())
        {
            var mode = GetApprovalMode();
            if (mode == eApprovalMode.TwoLevelAll)
            {
                if (!(Profile.AllowAdmin && (SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR) || SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ENGINEER))))
                    Server.Transfer(
                        $@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.Jobs_List_InsufficientPermissionsMessage}");
            }
            else
            {
                if (SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_WEBDOWNLOADER) || SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_WEBVIEWER))
                {
                    Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_VK_800}");
                }
            }
        }

        if (validator.IsValid) ShowJobNotificationMessage();

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_VK_799;
    }

    private void ShowJobNotificationMessage()
    {
        using (var proxy = isLayer.Proxy)
        {
            var jobNotification = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.JobNotification, true, null));
            if (!jobNotification) return;

            DataTable dt;

            dt = proxy.Query(
                $@"SELECT T.NCMJobID FROM Cirrus.NCM_NCMJobs AS T WHERE T.NCMJobType={(int) eJobType.PurgeConfigs} AND T.Enabled=1");
            var needSetupPurgeConfigsJob = (dt.Rows.Count == 0);

            dt = proxy.Query(
                $@"SELECT T.NCMJobID FROM Cirrus.NCM_NCMJobs AS T WHERE T.NCMJobType={(int) eJobType.DatabaseMaintenance} AND T.Enabled=1");
            var needSetupDBMaintenanceJob = (dt.Rows.Count == 0);

            jobNotificationHolder.Visible = true;
            if (needSetupPurgeConfigsJob && needSetupDBMaintenanceJob)
                jobNotificationMsg.Text = Resources.NCMWebContent.WEBDATA_VK_815;
            else if (needSetupPurgeConfigsJob && !needSetupDBMaintenanceJob)
                jobNotificationMsg.Text = Resources.NCMWebContent.WEBDATA_VK_816;
            else if (!needSetupPurgeConfigsJob && needSetupDBMaintenanceJob)
                jobNotificationMsg.Text = Resources.NCMWebContent.WEBDATA_VK_817;
            else
                jobNotificationHolder.Visible = false;
        }
    }

    protected string LearnMore(string fragment)
    {
        var helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
        return helpHelperWrap.GetHelpUrl(fragment);
    }


    protected bool IsAdministratorOrEngineer
    {
        get { return SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR) || SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ENGINEER); }
    }

    private eApprovalMode GetApprovalMode()
    {
        using (var proxy = isLayer.Proxy)
        {
            return proxy.Cirrus.ConfigChangeApproval.GetApprovalMode();
        }
    }
}
