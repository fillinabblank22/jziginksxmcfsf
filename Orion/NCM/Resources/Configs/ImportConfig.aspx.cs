﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.NCM.Common.Dal;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Common;
using CirrusEntities = SolarWinds.InformationService.Linq.Plugins.NCM.Cirrus;
using CoreEntities = SolarWinds.InformationService.Linq.Plugins.Core.Orion;

public partial class Orion_NCM_Resources_Configs_ImportConfig : Page
{
    private readonly IIsWrapper isLayer;
    private readonly ISwisRepositoryFactory swisRepositoryFactory;

    public Orion_NCM_Resources_Configs_ImportConfig()
    {
        swisRepositoryFactory = ServiceLocator.Container.Resolve<ISwisRepositoryFactory>();
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
    }

    protected string ReturnUrl { get; set; }
    protected string NodeCaption { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_VK_823;

        ReturnUrl = UrlHelper.FromSafeUrlParameter(Page.Request.QueryString[@"NCMReturnURL"]);
        if (CommonHelper.IsDemoMode())
        {
            Submit.OnClientClick =
                $@"demoAction('{DemoModeConstants.NCM_MANAGECONFIGS_IMPORT_CONFIG}', this); return false;";
        }

        if (!Page.IsPostBack)
        {
            txtConfigTitle.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_824, FormatDateTime(DateTime.Now));
        }

        Guid nodeId;
        if (Guid.TryParse(Page.Request.QueryString[@"NodeID"], out nodeId))
        {
            using (var swisRepo = swisRepositoryFactory.CreateForUser(HttpContext.Current.Profile.UserName))
            {
                var query = from ncm in swisRepo.Get<CirrusEntities.NodeProperties>()
                    join core in swisRepo.Get<CoreEntities.Nodes>() on ncm.CoreNodeID equals core.NodeID
                    where ncm.NodeID == nodeId
                    select core.Caption;

                NodeCaption = query.FirstOrDefault();
            }
        }
        else
        {
            NodeCaption = string.Empty;
        }
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (CommonHelper.IsDemoMode()) return;

        if (fuConfigFile.HasFile)
        {
            var nodeIdGuid = new Guid(Page.Request.QueryString[@"NodeID"]);

            using (var proxy = isLayer.GetProxy())
            {
                if (checkBoxIsBinary.Checked)
                {
                    var fs = fuConfigFile.PostedFile.InputStream;
                    byte[] bytes;
                    using (var br = new BinaryReader(fs))
                    {
                        bytes = br.ReadBytes((int)fs.Length);
                    }

                    proxy.Cirrus.ConfigArchive.ImportBinaryConfig(nodeIdGuid, txtConfigTitle.Text, txtConfigComments.Text,
                        bytes);
                }
                else
                {
                    var streamReader = new StreamReader(fuConfigFile.FileContent);
                    var configText = streamReader.ReadToEnd();

                    proxy.Cirrus.ConfigArchive.ImportConfig(nodeIdGuid, txtConfigTitle.Text, txtConfigComments.Text,
                        configText);
                }
            }
            Page.Response.Redirect(ReturnUrl);
        }
        else
        {
            cvConfigFile.IsValid = false;
        }
    }

    protected void CancelClick(object sender, EventArgs e)
    {
        Page.Response.Redirect(ReturnUrl);
    }

    private string FormatDateTime(object datetime)
    {
        var t = (DateTime)datetime;
        return $@"{Utils.FormatCurrentCultureDate(t)} {Utils.FormatToLocalTime(t)}";
    }
}