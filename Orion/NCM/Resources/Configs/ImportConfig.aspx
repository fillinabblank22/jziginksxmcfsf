﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="ImportConfig.aspx.cs" Inherits="Orion_NCM_Resources_Configs_ImportConfig" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmHeadPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmExportToPDFPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ncmCustomizePagePlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="ncmHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMPHImportConfigChoose" />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
    <div style="padding:10px;">
        <div style="border:solid 1px #dcdbd7;width:1100px;padding:10px;background-color:#fff;">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td style="font-weight:bold;">
                        <%=NodeCaption %>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:20px;padding-bottom:5px;font-weight:bold;"><%=Resources.NCMWebContent.WEBDATA_IC_162 %></td>
                </tr>     
                <tr>
                    <td>
                        <asp:TextBox ID="txtConfigTitle" runat="server" BorderColor="#89A6C0" Font-Size="9pt" Font-Names="Arial,Verdana,Helvetica,sans-serif" BorderWidth="1px" Width="500px" Height="15px" TextMode="SingleLine" />
                        <asp:RequiredFieldValidator ID="rfvConfigTitle" runat="server" Display="Dynamic" ControlToValidate="txtConfigTitle" ErrorMessage="*" />
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:20px;padding-bottom:5px;font-weight:bold;"><%=Resources.NCMWebContent.WEBDATA_IC_167 %></td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtConfigComments" runat="server" TextMode="MultiLine" Font-Size="9pt" Font-Names="Arial,Verdana,Helvetica,sans-serif" BorderColor="#89A6C0" BorderWidth="1px" Width="500px" Height="45px" style="resize:none;" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox ID="checkBoxIsBinary" runat="server" Text="<%$ Resources: NCMWebContent, ImportConfig_BinaryConfig %>" />
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:20px;padding-bottom:5px;font-weight:bold;"><%=Resources.NCMWebContent.WEBDATA_VK_826 %></td>
                </tr>
                <tr>
                    <td>
                        <asp:FileUpload ID="fuConfigFile" runat="server" CssClass="ConfigFileUpload" size="80" />
                        <div>
                            <asp:CustomValidator ID="cvConfigFile" runat="server" ErrorMessage="<%$ Resources:NCMWebContent, WEBDATA_VK_825 %>" />
                        </div>
                    </td>
                </tr>
            </table>

            <table width="100%">
	            <tr>
	                <td align="left" style="padding-top:20px;">
                        <orion:LocalizableButton runat="server" ID="Submit" LocalizedText="Submit" DisplayType="Primary" OnClick="SubmitClick" />
                        <orion:LocalizableButton runat="server" ID="Cancel" LocalizedText="Cancel" DisplayType="Secondary" OnClick="CancelClick" CausesValidation="false"  />
	                </td>	                        
	            </tr>
            </table>
        </div>
    </div>
</asp:Content>

