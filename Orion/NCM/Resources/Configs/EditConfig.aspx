﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMEditMasterPage.master" AutoEventWireup="true" CodeFile="EditConfig.aspx.cs" Inherits="Orion_NCM_Resources_Configs_EditConfig" %>


<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/NCM/Controls/ErrorControl.ascx" TagPrefix="ncm" TagName="ErrorControl" %>





<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ncmPageTitlePlaceHolder">
	<%=Page.Title %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmHelpIconPlaceHolder" runat="server">
    <orion:IconHelpButton ID="helpBtn" runat="server" HelpUrlFragment="OrionNCMPHEditConfig" />
</asp:Content>


<asp:Content ID="MainContent" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
    <link rel="Stylesheet" type="text/css" href="../../styles/NCMResources.css" />
    <link rel="Stylesheet" type="text/css" href="../ConfigSnippets/jquery-linedtextarea.css" />
	<script type="text/javascript" src="../../JavaScript/main.js"></script>
	<script type="text/javascript" src="../ConfigSnippets/jquery-linedtextarea.js"></script>
     <orion:Include ID="Include1" runat="server" Framework="jQuery" FrameworkVersion="1.7.1" />

    
    <script>

        var textAppended = false;
        function EnableDisableConfigEditArea(checked) {
            if (checked) {
                $('#<%=txtConfigScript.ClientID%>').removeAttr('disabled');

                if (!textAppended) {
                    var currentVal = $('#<%=txtComments.ClientID%>').val();
                    if (currentVal == '') {
                        currentVal = '<%=EditedString%>';
                    }
                    else {
                        currentVal = currentVal + '\n' + '<%=EditedString%>';
                    }

                    $('#<%=txtComments.ClientID%>').val(currentVal);
                    textAppended = true;
                }
            }
            else {
                $('#<%=txtConfigScript.ClientID%>').attr("disabled", "disabled"); ;
            }
        }
    
    </script>
    
    <div style="padding:10px;">       
                <div style="border:solid 1px #dcdbd7;width:1100px;padding:5px;background-color:#fff;">
                    <asp:Panel ID="ContentPanel" runat="server">
                    <table>
                        <tr>
                            <td style="padding-top:10px">
                                <b><asp:Label ID="lblNodeName" runat="server" /></b>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top:10px">
                               <b><%= Resources.NCMWebContent.WEBDATA_IC_162 %></b>
                            </td>
                        </tr>     
                        
                        <tr>
                            <td>
                                <asp:TextBox runat="server" ID="txtTitle" BorderColor="#89A6C0" BorderWidth="1px" MaxLength="200" Width = "500px" Height="15px" TextMode="SingleLine"  />
                                 <asp:RequiredFieldValidator ID="validatorSnippetName" runat="server" Display="Dynamic" ControlToValidate="txtTitle" ErrorMessage="<%$ Resources:NCMWebContent, WEBDATA_IC_161 %>"  > </asp:RequiredFieldValidator>
                            </td>
                        </tr>                        
                    </table>
                    
                    <table>
                        <tr>
                            <td style="padding-top:20px">
                                <b><%= Resources.NCMWebContent.WEBDATA_IC_163 %></b>
                                </td>
                            <td style="padding-top:20px;padding-left:10px">
                                <asp:Label ID="lblConfigId" runat="server" Text="testID"/>
                            </td>
                         </tr>
                         <tr>
                            <td style="padding-top:10px">
                                <b><%= Resources.NCMWebContent.WEBDATA_IC_164 %></b>
                                </td>
                            <td style="padding-top:10px;padding-left:10px">
                                <asp:Label ID="lblDownloadTime" runat="server" Text="testdownloadTime"/>
                            </td>
                         </tr>
                         <tr>
                            <td style="padding-top:10px">
                                <b><%= Resources.NCMWebContent.WEBDATA_IC_165 %></b>
                                </td>
                            <td style="padding-top:10px;padding-left:10px">
                                <asp:Label ID="lblModifiedTime" runat="server" Text="testModifyTime"/>
                            </td>
                         </tr>
                         <tr>
                            <td style="padding-top:10px">
                                <b><%= Resources.NCMWebContent.WEBDATA_IC_166 %></b>
                            </td>
                            <td style="padding-top:10px;padding-left:10px">
                                <asp:Label ID="lblConfigType" runat="server" Text="testConfigTime"/>
                            </td>
                         </tr>
                      </table>

                    
                    <table>
                        <tr>
                            <td style="padding-top:20px">
                                <b><%= Resources.NCMWebContent.WEBDATA_IC_167 %></b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                 <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" BorderColor="#89A6C0" BorderWidth="1px" Width = "500px" Height="45px" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top:30px">
                                <b><asp:CheckBox  runat="server" ID="chkEditConfig" Text="<%$ Resources:NCMWebContent, WEBDATA_IC_168 %>" OnClick="EnableDisableConfigEditArea (this.checked);" /></b> <span style="font-size:smaller;color:Gray" ><%= Resources.NCMWebContent.WEBDATA_IC_169 %></span>                              
                            </td>
                            
                                
                            
                        </tr>
                        <tr>
                            <td>
                               <textarea class="lined" style="width:800px;height:500px;background-color:White" id="txtConfigScript"   runat="server" disabled="disabled"></textarea>
                            </td>
                        </tr>
                    </table>
                    </asp:Panel>
                    <div style="width:800px;">
                        <ncm:ErrorControl ID="ErrorControl" runat="server" Visible="false"  VisiblePermissionLink="false"/>
                    </div>
                    <table width="100%">
	                    <tr>
	                        <td style="padding:5px;width:20%;">
	                            <br/>
                                <orion:LocalizableButton runat="server" ID="Submit" LocalizedText="submit" DisplayType="Primary" OnClick="SubmitClick" />
                                <orion:LocalizableButton runat="server" ID="Cancel" LocalizedText="Cancel" DisplayType="secondary" OnClick="CancelClick" CausesValidation="false"  />
	                        </td>	                        
	                    </tr>
                    </table>
                </div>           
    </div>
</asp:Content>

