﻿using System;
using SolarWinds.NCMModule.Web.Resources;
using System.Linq;
using System.Web;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.InformationService.Linq.Plugins.NCM.Cirrus;
using SolarWinds.NCM.Common.Dal;
using SolarWinds.Orion.Web.Helpers;
using System.Web.UI;
using NodeProperties = SolarWinds.InformationService.Linq.Plugins.NCM.NCM.NodeProperties;
using Nodes = SolarWinds.InformationService.Linq.Plugins.NCM.Orion.Nodes;

public partial class Orion_NCM_Resources_Configs_EditConfig : Page
{
    private readonly IIsWrapper isLayer;
    private readonly ILogger log;
    private readonly ISwisRepositoryFactory swisRepositoryFactory;
    private string returnURL = string.Empty;

    public Orion_NCM_Resources_Configs_EditConfig()
    {
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
        swisRepositoryFactory = ServiceLocator.Container.Resolve<ISwisRepositoryFactory>();
        log = ServiceLocator.Container.Resolve<Func<Type, ILogger>>()(GetType());
    }

    protected string EditedString => string.Format (Resources.NCMWebContent.WEBDATA_IC_171, Page.User.Identity.Name);

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_IC_170;
        returnURL = UrlHelper.FromSafeUrlParameter (Page.Request.QueryString[@"NCMReturnURL"]);
        if (string.IsNullOrEmpty(returnURL))
        {
            returnURL = @"/Orion/NCM/Summary.aspx";
        }

        if (!CommonHelper.IsDemoMode())
        {
            if (!SecurityHelper.IsPermissionExist(SecurityHelper._canExecuteScript))
            {
                ShowError(Resources.NCMWebContent.WEBDATA_IC_173);
                return;
            }
        }

        if (CommonHelper.IsDemoMode()) Submit.OnClientClick =
            $@"demoAction('{DemoModeConstants.NCM_MANAGECONFIGS_SAVE_CONFIG}', this); return false;";
        
        var linedTextArea = @"$(function() { $("".lined"").linedtextarea(	{selectedLine: -1} ); });";
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), @"key_linedtextarea", linedTextArea, true);
        if (!Page.IsPostBack)
        {
            LoadConfigData(HttpContext.Current.Profile.UserName);
        }
    }

    private void LoadConfigData(string userName)
    {
        try
        {
            Guid parsedConfigId;
            var guidParsed = Guid.TryParse(Page.Request.QueryString[@"ConfigID"], out parsedConfigId);
            using (var repo = swisRepositoryFactory.CreateForUser(userName))
            {
                var result = (from archive in repo.Get<ConfigArchive>()
                    join properties in repo.Get<NodeProperties>() on archive.NodeID equals properties.NodeID
                    join nodes in repo.Get<Nodes>() on properties.CoreNodeID equals nodes.NodeID
                    where archive.ConfigID == parsedConfigId
                    select new
                    {
                        NodeCaption = nodes.Caption,
                        archive.BaseConfigID,
                        archive.ConfigID,
                        archive.ConfigTitle,
                        archive.DownloadTime,
                        archive.ModifiedTime,
                        archive.ConfigType,
                        archive.Config,
                        archive.Comments
                    }).FirstOrDefault();

                if (result != null && guidParsed)
                {
                    lblConfigId.Text = result.ConfigID.ToString();
                    txtTitle.Text = result.ConfigTitle;
                    lblDownloadTime.Text = result.DownloadTime.ToString();
                    lblModifiedTime.Text = result.ModifiedTime.ToString();
                    if (lblDownloadTime.Text.Equals(lblModifiedTime.Text))
                    {
                        lblModifiedTime.Text = Resources.NCMWebContent.WEBDATA_IC_174; //never modified
                    }
                    lblConfigType.Text = HttpUtility.HtmlEncode(result.ConfigType);
                    txtConfigScript.InnerText = result.Config;
                    lblNodeName.Text = HttpUtility.HtmlEncode(result.NodeCaption);
                    txtComments.Text = result.Comments;
                    var baseConfigId = result.BaseConfigID.ToString();
                    if (!string.IsNullOrEmpty(baseConfigId))
                    {
                        ViewState.Add("IsRevisionConfig", true);
                    }
                }
                else
                {
                    ContentPanel.Visible = false;
                    ErrorControl.Visible = true;
                    ShowError(string.Format(Resources.NCMWebContent.WEBDATA_IC_172, Page.Request.QueryString[@"ConfigID"]));
                }
            }
        }
        catch (Exception ex)
        {
            ShowError(ex.Message);
            log.Error("NCM edit Config Error", ex);
        }
    }

    private void ShowError(string errorMessage)
    {
        ContentPanel.Visible = false;
        ErrorControl.Visible = true;
        ErrorControl.ErrorMessage = errorMessage;
        Submit.Visible = false;
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (CommonHelper.IsDemoMode()) return;

        var error = false;
        try
        {
            using (var proxy = isLayer.GetProxy())
            {
                if (chkEditConfig.Checked)
                {
                    if (ViewState["IsRevisionConfig"]!=null)
                    {
                        //it is revision config - just updatr it
                        proxy.Cirrus.ConfigArchive.UpdateConfig(new Guid(lblConfigId.Text), txtTitle.Text, txtComments.Text, txtConfigScript.InnerText,true,Page.User.Identity.Name);
                    }
                    else 
                    {
                        //it is original config - save new revision 
                        proxy.Cirrus.ConfigArchive.CloneConfig(new Guid(lblConfigId.Text), txtTitle.Text, txtComments.Text, txtConfigScript.InnerText, Page.User.Identity.Name);
                    }
                }
                else
                {
                    //config text is not changed - just update current config
                    proxy.Cirrus.ConfigArchive.UpdateConfig(new Guid(lblConfigId.Text), txtTitle.Text, txtComments.Text, string.Empty, false, Page.User.Identity.Name);
                }
            }
        }
        catch (Exception ex)
        {
            error = true;
            ShowError(ex.Message);
            log.Error("NCM edit Config Error", ex);
        }

        if(!error)
            Page.Response.Redirect(returnURL);
    }

    protected void CancelClick(object sender, EventArgs e)
    {
        Page.Response.Redirect(returnURL);
    }
}