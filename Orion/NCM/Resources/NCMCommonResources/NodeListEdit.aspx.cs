using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Web;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;
using SolarWinds.NCMModule.Web.Resources.Resources;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources.Wrappers;

public partial class Orion_NCM_Resources_NCMCommonResources_NodeListEdit : Page
{
    private readonly Dictionary<string, string> itemsList = new Dictionary<string, string>();
    private readonly IEditResourcePageService editResourceService;

    public Orion_NCM_Resources_NCMCommonResources_NodeListEdit()
    {
        editResourceService = ServiceLocator.Container.Resolve<IEditResourcePageService>();
    }

    protected ResourceInfo Resource { get; private set; }
    protected string ResourceHeader => editResourceService.GetHeader(Resource);

    protected override void OnInit(EventArgs e)
    {
        Resource = editResourceService.Init(new PageWrap(this), ResourceTitle, null);

        base.OnInit(e);
        Initialize();
    }

    protected void GroupingBox3_SelectedIndexChanged(object sender, EventArgs e)
    {
        List<DropDownList> items = new List<DropDownList>
        {
            GroupingBox2,
            GroupingBox3
        };
        FillInDropDownValues(GroupingBox1, items);

        items.Clear();
        items.Add(GroupingBox1);
        items.Add(GroupingBox3);
        FillInDropDownValues(GroupingBox2, items);
    }

    protected void GroupingBox2_SelectedIndexChanged(object sender, EventArgs e)
    {
        List<DropDownList> items = new List<DropDownList>();
        if (GroupingBox2.SelectedValue.Equals(@"None", StringComparison.InvariantCultureIgnoreCase))
        {
            GroupingBox3.SelectedValue = @"None";
            GroupingPanel3.Visible = false;
            FillInDropDownValues(GroupingBox1, null);

            items.Clear();
            items.Add(GroupingBox1);
            FillInDropDownValues(GroupingBox2, items);
        }
        else
        {
            items.Clear();
            items.Add(GroupingBox1);
            items.Add(GroupingBox2);
            FillInDropDownValues(GroupingBox3, items);

            items.Clear();
            items.Add(GroupingBox2);
            items.Add(GroupingBox3);
            FillInDropDownValues(GroupingBox1, items);
            GroupingPanel3.Visible = true;
        }
    }

    protected void GroupingBox1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (GroupingBox1.SelectedValue.Equals(@"None", StringComparison.InvariantCultureIgnoreCase))
        {
            GroupingBox2.SelectedValue = @"None";
            GroupingBox3.SelectedValue = @"None";
            GroupingPanel2.Visible = false;
            GroupingPanel3.Visible = false;
            FillInDropDownValues(GroupingBox1, null);
        }
        else
        {
            List<DropDownList> items = new List<DropDownList>();
            items.Add(GroupingBox1);
            items.Add(GroupingBox3);
            FillInDropDownValues(GroupingBox2, items);

            items.Clear();
            items.Add(GroupingBox1);
            items.Add(GroupingBox2);
            FillInDropDownValues(GroupingBox3, items);
            GroupingPanel2.Visible = true;
        }
    }

    private void Initialize()
    {
        string[] groupByArray = null;
        if (!string.IsNullOrEmpty(Resource.Properties[@"GroupBy"]))
        {
            groupByArray = Resource.Properties[@"GroupBy"].Split(',');
        }

        SetDropDownList();
        List<DropDownList> skipedItems;
        ListItem lItem; //item to hold selected from settings value
                        //fill in values from settings
        if (groupByArray != null)
        {
            for (int i = 0; i < groupByArray.Length; i++)
            {
                switch (i)
                {
                    case 0:
                        GroupingBox1.SelectedValue = CustomPropertyHelper.CovertGroupingPropertyName(groupByArray[i]);
                        if (!GroupingBox1.SelectedValue.Equals(@"None", StringComparison.InvariantCultureIgnoreCase))
                        {
                            GroupingPanel2.Visible = true;
                            skipedItems = new List<DropDownList>();
                            skipedItems.Add(GroupingBox1);
                            FillInDropDownValues(GroupingBox2, skipedItems);
                        }
                        break;
                    case 1:
                        GroupingBox2.SelectedValue = CustomPropertyHelper.CovertGroupingPropertyName(groupByArray[i]);
                        lItem = GroupingBox2.SelectedItem;
                        GroupingBox1.Items.Remove(lItem);
                        GroupingPanel3.Visible = true;
                        skipedItems = new List<DropDownList>();
                        skipedItems.Add(GroupingBox1);
                        skipedItems.Add(GroupingBox2);
                        FillInDropDownValues(GroupingBox3, skipedItems);
                        break;
                    case 2:
                        GroupingBox3.SelectedValue = CustomPropertyHelper.CovertGroupingPropertyName(groupByArray[i]);
                        lItem = GroupingBox3.SelectedItem;
                        GroupingBox2.Items.Remove(lItem);
                        GroupingBox1.Items.Remove(lItem);
                        break;
                }
            }
        }
    }

    private void SetDropDownList()
    {
        var item = new ListItem(Resources.NCMWebContent.WEBDATA_VK_272, @"None");
        GroupingBox1.Items.Add(item);
        GroupingBox2.Items.Add(item);
        GroupingBox3.Items.Add(item);

        itemsList.Add(@"NodeGroup", Resources.NCMWebContent.WEBDATA_VK_273);
        itemsList.Add(@"Status", Resources.NCMWebContent.WEBDATA_VK_274);
        itemsList.Add(@"Contact", Resources.NCMWebContent.WEBDATA_VK_275);
        itemsList.Add(@"Location", Resources.NCMWebContent.WEBDATA_VK_276);
        itemsList.Add(@"SysObjectID", Resources.NCMWebContent.WEBDATA_VK_277);
        itemsList.Add(@"Vendor", Resources.NCMWebContent.WEBDATA_VK_278);
        itemsList.Add(@"MachineType", Resources.NCMWebContent.WEBDATA_VK_279);
        itemsList.Add(@"IOSImage", Resources.NCMWebContent.WEBDATA_VK_280);
        itemsList.Add(@"IOSVersion", Resources.NCMWebContent.WEBDATA_VK_281);
        itemsList.Add(@"ConfigTypes", Resources.NCMWebContent.WEBDATA_VK_282);
        itemsList.Add(@"EnableLevel", Resources.NCMWebContent.WEBDATA_VK_283);
        itemsList.Add(@"ExecProtocol", Resources.NCMWebContent.WEBDATA_VK_284);
        itemsList.Add(@"CommandProtocol", Resources.NCMWebContent.WEBDATA_VK_285);
        itemsList.Add(@"TransferProtocol", Resources.NCMWebContent.WEBDATA_VK_286);
        itemsList.Add(@"LoginStatus", Resources.NCMWebContent.WEBDATA_VK_287);

        GetCustomProperties();
        GroupingPanel2.Visible = false;
        GroupingPanel3.Visible = false;
        FillInDropDownValues(GroupingBox1, null);
    }

    private void FillInDropDownValues(DropDownList control, List<DropDownList> excludeValues)
    {
        ListItem currentItem = control.SelectedItem;
        control.Items.Clear();
        control.Items.Add(new ListItem(Resources.NCMWebContent.WEBDATA_VK_272, @"None"));

        foreach (KeyValuePair<string, string> key in itemsList)
        {
            var needExclude = false;
            if (excludeValues != null)
            {
                foreach (DropDownList item1 in excludeValues)
                {
                    if (key.Key.Equals(item1.SelectedItem.Value, StringComparison.InvariantCultureIgnoreCase))
                    {
                        needExclude = true;
                        break;
                    }
                }
            }
            if (!needExclude)
            {
                var item = new ListItem(key.Value, key.Key);
                control.Items.Add(item);
            }
        }

        control.SelectedValue = currentItem.Value;
    }

    private void GetCustomProperties()
    {
        List<string> customPropList = CustomPropertyHelper.GetCustomProperties();

        foreach (string prop in customPropList)
        {
            itemsList.Add(prop, prop);
        }
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        editResourceService.SaveChanges(Resource, ResourceTitle, null);

        string groupByValues = string.Empty;
        groupByValues += GroupingBox1.SelectedValue;
        if (!GroupingBox2.SelectedValue.Equals(@"None", StringComparison.InvariantCultureIgnoreCase))
        {
            groupByValues += @"," + GroupingBox2.SelectedValue;
        }

        if (!GroupingBox3.SelectedValue.Equals(@"None", StringComparison.InvariantCultureIgnoreCase))
        {
            groupByValues += @"," + GroupingBox3.SelectedValue;
        }

        Resource.Properties[@"GroupBy"] = groupByValues;

        Response.Redirect(BaseResourceControl.GetEditPageReturnUrl(Request.QueryString, Resource.View.ViewID.ToString()));
    }

    protected void CancelClick(object sender, EventArgs e)
    {
        Response.Redirect(BaseResourceControl.GetEditPageReturnUrl(Request.QueryString, Resource.View.ViewID.ToString()));
    }
}
