using System;
using SolarWinds.Cirrus.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.NCMModule.Web.Resources;

using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, NcmMetadataTypeValue.Policy)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NCM_Resources_NCMSummary_PolicyViolations : BaseResourceControl
{
    private PolicyViolations policy;

    public override string DetachURL
    {
        get
        {
            return
                $@"{base.DetachURL}{(Request.QueryString[@"NodeID"] != null ? @"&NodeID=" + Request.QueryString[@"NodeID"] : string.Empty)}";
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NCMWebContent.WEBDATA_VK_131; }
    }

    public override string HelpLinkFragment
    {
        get { return @"OrionNCMWebPolicyViolations"; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            var viewManagerAdapter = ServiceLocator.Container.Resolve<IViewManager>();
            return viewManagerAdapter.GetSupportedInterfacesForViewType(@"NCMSummary");
        }
    }

    public sealed override string DisplayTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(Resource.Title))
            {
                return Resource.Title;
            }
            return DefaultTitle;
        }
    }

    public sealed override string SubTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(Resource.SubTitle))
            {
                return Resource.SubTitle;
            }
            return string.Empty;
        }
    }

    public override string EditControlLocation
    {
        get
        {
            return @"/Orion/NCM/Controls/EditResourceControls/EditPolicyViolations.ascx";
        }
    }

    protected override void OnInit(EventArgs e)
    {
        policy = new PolicyViolations();
        policy.NodeProvider = GetInterfaceInstance<INodeProvider>();
        policy.ResourceSettings = this.Resource;
        policy.ResourceContainer = Wrapper;
        Wrapper.Content.Controls.Add(policy);

        base.OnInit(e);
        Wrapper.ShowEditButton = policy.ShowEditButton;
    }
}
