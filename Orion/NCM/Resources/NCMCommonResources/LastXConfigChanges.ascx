<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LastXConfigChanges.ascx.cs" Inherits="Orion_NCM_Resources_NCMCommonResources_LastXConfigChanges" %>

<script src="/Orion/NCM/JavaScript/main.js" type="text/javascript"></script>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <content>
        <asp:Panel ID="ContentContainer" runat="server"> 
            <asp:Repeater runat="server" ID="ConfigList">
                <HeaderTemplate>
			        <table cellpadding="0" cellspacing="0" width="100%">
				        <tr style="height:20px;">
					        <td class="ReportHeader" style="font-size:7pt;border-bottom:0px groove transparent;padding-right:0px;width:30%;">
						        <%=Resources.NCMWebContent.WEBDATA_VK_169 %>
					        </td>					
					        <td class="ReportHeader" style="font-size:7pt;border-bottom:0px groove transparent;padding-left:0px;padding-right:0px;width:30%">
						        <%=Resources.NCMWebContent.WEBDATA_VK_170 %>
					        </td>
					        <td class="ReportHeader" style="font-size:7pt;border-bottom:0px groove transparent;padding-left:0px;padding-right:0px;width:40%">
					            &nbsp;
					        </td>
				        </tr>
		        </HeaderTemplate>
                <ItemTemplate>
                        <tr style="height:20px;">
                            <td style="padding-left:3px;width:30%;">
					            <a href="/Orion/NCM/NodeDetails.aspx?NodeID=<%# DataBinder.Eval(Container.DataItem, @"NodeID") %>">
					             <%# DataBinder.Eval(Container.DataItem, @"NodeCaption")%>
					            </a>&nbsp					
					        <td style="width:30%;">
					            <%# DataBinder.Eval(Container.DataItem, @"TimeStamp")%>
					        </td>
				            <td style="width:40%;">
					            <a href="/Orion/NCM/ConfigChangeReport.aspx?ConfigID=<%# DataBinder.Eval(Container.DataItem, @"ConfigID") %>&ComparisonType=<%# DataBinder.Eval(Container.DataItem, @"ComparisonType") %>&ViewChangesOn=<%=ViewChangesOn%>&ConfigType=<%=ConfigType%>&NodeID=<%# DataBinder.Eval(Container.DataItem, @"NodeID") %>" >
						            <img border="0" src="/Orion/NCM/Resources/images/Icon.DisplayChanges.gif">
							        <%=Resources.NCMWebContent.WEBDATA_VK_171 %>
						        </a>
					        </td>
				        </tr>
		        </ItemTemplate>   
		        <FooterTemplate>
                    </table>
		        </FooterTemplate>
            </asp:Repeater>
        </asp:Panel>
        <div id="ErrorContainer" runat="server"/>
    </content>
</orion:resourceWrapper>

  
    