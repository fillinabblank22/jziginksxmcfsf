<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PolicyViolations.ascx.cs" Inherits="Orion_NCM_Resources_NCMSummary_PolicyViolations" %>

<link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
<script src="/Orion/NCM/JavaScript/main.js" type="text/javascript"></script>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <content> 
    </content>
</orion:resourceWrapper>