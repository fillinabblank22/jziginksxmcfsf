using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.Resources;
using SolarWinds.NCMModule.Web.Resources.Wrappers;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NCM_Resources_NCMCommonResources_ConfigurationChangesSnapshotChartEdit : Page
{
    private readonly IEditResourcePageService editResourceService;

    public Orion_NCM_Resources_NCMCommonResources_ConfigurationChangesSnapshotChartEdit()
    {
        editResourceService = ServiceLocator.Container.Resolve<IEditResourcePageService>();
    }

    protected ResourceInfo Resource { get; private set; }
    protected string ResourceHeader => editResourceService.GetHeader(Resource);

    private void SetupControlFromRequest()
    {
        FilterNodeSql.WhereClause = Resource.Properties[@"FilterClause"];

        PeriodListDropDown.SelectedValue = !string.IsNullOrEmpty(Resource.Properties[@"TimeRange"])
            ? Resource.Properties[@"TimeRange"]
            : @"Last 7 Days";

        SetupDateTimePeriod();

        bool visible = Resource.Properties[@"TimeRange"] == @"Custom";
        datePeriodHolder.Visible = visible;

        chkTrackChanges.Checked = !string.IsNullOrEmpty(Resource.Properties[@"TrackChanges"]) &&
                                  Convert.ToBoolean(Resource.Properties[@"TrackChanges"]);
        ddlConfigType.Enabled = chkTrackChanges.Checked;

        if (!string.IsNullOrEmpty(Resource.Properties[@"ConfigType"]))
        {
            ddlConfigType.SelectedValue = Resource.Properties[@"ConfigType"];
        }
    }

    private void SetupDateTimePeriod()
    {
        dateTimeFrom.TimeSpan = string.IsNullOrEmpty(Resource.Properties[@"DateFrom"])
            ? DateTime.Now.AddDays(-7)
            : Convert.ToDateTime(Resource.Properties[@"DateFrom"], CultureInfo.InvariantCulture);
    }

    protected override void OnInit(EventArgs e)
    {
        Resource = editResourceService.Init(new PageWrap(this), ResourceTitle, null);

        OnInitPeriodList();
        SetupControlFromRequest();

        base.OnInit(e);

        ScriptManager scriptManager = ScriptManager.GetCurrent(Page);
        scriptManager.EnableScriptGlobalization = true;
        scriptManager.EnableScriptLocalization = true;
    }

    protected void chkTrackChanges_OnCheckedChanged(object sender, EventArgs e)
    {
        ddlConfigType.Enabled = chkTrackChanges.Checked;
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (!FilterNodeSql.ValidateWhereClause())
            return;

        editResourceService.SaveChanges(Resource, ResourceTitle, null);

        Resource.Properties[@"FilterClause"] = FilterNodeSql.WhereClause;

        if (PeriodListDropDown.SelectedValue == @"Custom")
        {
            if (!dateTimeFrom.ValidateDate()) return;

            Resource.Properties[@"DateFrom"] = dateTimeFrom.TimeSpan.ToString(CultureInfo.InvariantCulture);
            Resource.Properties[@"DateTo"] = string.Empty;
        }
        else
        {
            DateTime dateTimeTo = DateTime.Now;
            string strDateTimeTo = string.Empty;
            if (PeriodListDropDown.SelectedValue.Equals(@"Last Month", StringComparison.InvariantCultureIgnoreCase))
            {
                dateTimeTo = new DateTime(dateTimeTo.Year, dateTimeTo.Month,
                        dateTimeTo.AddDays(-dateTimeTo.Day + 1).Day).AddMilliseconds(-1);
                strDateTimeTo = dateTimeTo.ToString(CultureInfo.InvariantCulture);
            }
            Resource.Properties[@"DateFrom"] = TimeParser.GetTime(PeriodListDropDown.SelectedValue).ToString(CultureInfo.InvariantCulture);
            Resource.Properties[@"DateTo"] = strDateTimeTo;
        }

        Resource.Properties[@"TimeRange"] = PeriodListDropDown.SelectedValue;
        Resource.Properties[@"TrackChanges"] = chkTrackChanges.Checked.ToString();
        Resource.Properties[@"ConfigType"] = ddlConfigType.SelectedValue;

        Response.Redirect(BaseResourceControl.GetEditPageReturnUrl(Request.QueryString, Resource.View.ViewID.ToString()));
    }

    protected void CancelClick(object sender, EventArgs e)
    {
        Response.Redirect(BaseResourceControl.GetEditPageReturnUrl(Request.QueryString, Resource.View.ViewID.ToString()));
    }

    private void OnInitPeriodList()
    {
        PeriodListDropDown.Items.Add(new ListItem(Resources.NCMWebContent.WEBDATA_VK_184, @"As of Last Update"));

        Dictionary<string, string> items = TimeParser.GeneratePeriods();
        foreach (KeyValuePair<string, string> item in items)
        {
            PeriodListDropDown.Items.Add(new ListItem(item.Value, item.Key));
        }
    }

    protected void PeriodListDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool visible = PeriodListDropDown.SelectedValue == @"Custom";
        datePeriodHolder.Visible = visible;
    }
}
