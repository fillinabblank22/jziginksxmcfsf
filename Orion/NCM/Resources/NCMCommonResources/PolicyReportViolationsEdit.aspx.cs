using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Globalization;

using SolarWinds.Orion.Web;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.Resources;
using SolarWinds.Orion.Web.UI;
using System.Web.UI;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.NCMModule.Web.Resources.Wrappers;

public partial class Orion_NCM_Resources_NCMCommonResources_PolicyReportViolationsEdit : Page
{
    private readonly IIsWrapper isLayer;
    private readonly IEditResourcePageService editResourceService;

    public Orion_NCM_Resources_NCMCommonResources_PolicyReportViolationsEdit()
    {
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
        editResourceService = ServiceLocator.Container.Resolve<IEditResourcePageService>();
    }

    protected ResourceInfo Resource { get; set; }
    protected string ResourceHeader => editResourceService.GetHeader(Resource);

    private void SetupControlFromRequest()
    {
        PolicyReportList.SelectedValue = Resource.Properties[@"PolicyReportID"];

        GranularityDropDown.SelectedValue = !string.IsNullOrEmpty(Resource.Properties[@"Granularity"])
            ? Resource.Properties[@"Granularity"]
            : @"Daily";

        var periodListValue = @"Last 7 Days";
        if (!string.IsNullOrEmpty(Resource.Properties[@"TimeRange"]))
        {
            periodListValue = Resource.Properties[@"TimeRange"];
        }

        RenderPeriodListByGranularity(GranularityDropDown.SelectedValue, periodListValue);

        SetupDateTimePeriod();

        var visible = Resource.Properties[@"TimeRange"] == @"Custom";
        datePeriodHolder.Visible = visible;
    }

    private void SetupDateTimePeriod()
    {
        dateTimeFrom.TimeSpan = string.IsNullOrEmpty(Resource.Properties[@"DateFrom"])
            ? DateTime.Now.AddDays(-7)
            : Convert.ToDateTime(Resource.Properties[@"DateFrom"], CultureInfo.InvariantCulture);

        dateTimeTo.TimeSpan = string.IsNullOrEmpty(Resource.Properties[@"DateTo"])
            ? DateTime.Now
            : Convert.ToDateTime(Resource.Properties[@"DateTo"], CultureInfo.InvariantCulture);
    }

    protected override void OnInit(EventArgs e)
    {
        Resource = editResourceService.Init(new PageWrap(this), ResourceTitle, null);

        RenderPolicyReportList();
        RenderGranularityList();
        SetupControlFromRequest();

        base.OnInit(e);

        var scriptManager = ScriptManager.GetCurrent(Page);
        scriptManager.EnableScriptGlobalization = true;
        scriptManager.EnableScriptLocalization = true;
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        editResourceService.SaveChanges(Resource, ResourceTitle, null);

        if (PeriodListDropDown.SelectedValue == @"Custom")
        {
            if (!dateTimeFrom.ValidateDate() || !dateTimeTo.ValidateDate()) return;

            Resource.Properties[@"DateFrom"] = dateTimeFrom.TimeSpan.ToString(CultureInfo.InvariantCulture);
            Resource.Properties[@"DateTo"] = dateTimeTo.TimeSpan.ToString(CultureInfo.InvariantCulture);
        }
        else
        {
            var dateTimeTo = DateTime.Now;
            var strDateTimeTo = string.Empty;
            if (PeriodListDropDown.SelectedValue.Equals(@"Last Month", StringComparison.InvariantCultureIgnoreCase))
            {
                dateTimeTo = new DateTime(dateTimeTo.Year, dateTimeTo.Month,
                        dateTimeTo.AddDays(-dateTimeTo.Day + 1).Day).AddMilliseconds(-1);
                strDateTimeTo = dateTimeTo.ToString(CultureInfo.InvariantCulture);
            }
            Resource.Properties[@"DateFrom"] = TimeParser.GetTime(PeriodListDropDown.SelectedValue).ToString(CultureInfo.InvariantCulture);
            Resource.Properties[@"DateTo"] = strDateTimeTo;
        }

        Resource.Properties[@"PolicyReportID"] = PolicyReportList.SelectedItem.Value;
        Resource.Properties[@"PolicyReportName"] = PolicyReportList.SelectedItem.Text;
        Resource.Properties[@"Granularity"] = GranularityDropDown.Text;
        Resource.Properties[@"TimeRange"] = PeriodListDropDown.SelectedValue;

        Response.Redirect(BaseResourceControl.GetEditPageReturnUrl(Request.QueryString, Resource.View.ViewID.ToString()));
    }

    protected void CancelClick(object sender, EventArgs e)
    {
        Response.Redirect(BaseResourceControl.GetEditPageReturnUrl(Request.QueryString, Resource.View.ViewID.ToString()));
    }

    private void RenderPolicyReportList()
    {
        PolicyReportList.Items.Clear();

        using (var proxy = isLayer.GetProxy())
        {
            var dt = proxy.Query(@"SELECT P.PolicyReportID, P.Name FROM Cirrus.PolicyReports AS P ORDER BY P.Name");

            foreach (DataRow dr in dt.Rows)
            {
                var item = new ListItem();
                item.Text = dr["Name"].ToString();
                item.Value = $@"{{{dr["PolicyReportID"]}}}";

                PolicyReportList.Items.Add(item);
            }
        }
    }

    protected void Granularity_SelectedIndexChanged(object sender, EventArgs e)
    {
        RenderPeriodListByGranularity(GranularityDropDown.SelectedValue, string.Empty);
        datePeriodHolder.Visible = false;
    }

    private void RenderGranularityList()
    {
        var items = TimeParser.GenerateGranularities();
        foreach (var item in items)
        {
            GranularityDropDown.Items.Add(new ListItem(item.Value, item.Key));
        }
    }

    private void RenderPeriodListByGranularity(string granularity, string selectedValue)
    {
        PeriodListDropDown.Items.Clear();

        var items = TimeParser.GeneratePeriods(granularity);
        foreach (var item in items)
        {
            PeriodListDropDown.Items.Add(new ListItem(item.Value, item.Key));
        }

        if (!string.IsNullOrEmpty(selectedValue))
        {
            PeriodListDropDown.SelectedValue = selectedValue;
        }
        else
        {
            PeriodListDropDown.SelectedIndex = 0;
        }
    }

    protected void PeriodListDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
        var visible = PeriodListDropDown.SelectedValue == @"Custom";
        datePeriodHolder.Visible = visible;
    }
}