using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.Resources;
using SolarWinds.NCMModule.Web.Resources.Wrappers;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NCM_Resources_NCMSummary_BackupVsAllDevicesChartEdit : Page
{
    private readonly IEditResourcePageService editResourceService;

    public Orion_NCM_Resources_NCMSummary_BackupVsAllDevicesChartEdit()
    {
        editResourceService = ServiceLocator.Container.Resolve<IEditResourcePageService>();
    }

    protected ResourceInfo Resource { get; private set; }
    protected string ResourceHeader => editResourceService.GetHeader(Resource);

    private void SetupControlFromRequest()
    {
        GranularityDropDown.SelectedValue = !string.IsNullOrEmpty(Resource.Properties[@"Granularity"])
            ? Resource.Properties[@"Granularity"]
            : @"Daily";

        string periodListValue = @"Last 7 Days";
        if (!string.IsNullOrEmpty(Resource.Properties[@"TimeRange"]))
        {
            periodListValue = Resource.Properties[@"TimeRange"];
        }

        RenderPeriodListByGranularity(GranularityDropDown.SelectedValue, periodListValue);

        SetupDateTimePeriod();

        bool visible = Resource.Properties[@"TimeRange"] == @"Custom";
        datePeriodHolder.Visible = visible;
    }

    private void SetupDateTimePeriod()
    {
        dateTimeFrom.TimeSpan = string.IsNullOrEmpty(Resource.Properties[@"DateFrom"])
            ? DateTime.Now.AddDays(-7)
            : Convert.ToDateTime(Resource.Properties[@"DateFrom"], CultureInfo.InvariantCulture);
    }

    protected override void OnInit(EventArgs e)
    {
        Resource = editResourceService.Init(new PageWrap(this), ResourceTitle, null);

        RenderGranularityList();
        SetupControlFromRequest();

        base.OnInit(e);

        ScriptManager scriptManager = ScriptManager.GetCurrent(Page);
        scriptManager.EnableScriptGlobalization = true;
        scriptManager.EnableScriptLocalization = true;
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        editResourceService.SaveChanges(Resource, ResourceTitle, null);

        if (PeriodListDropDown.SelectedValue == @"Custom")
        {
            if (!dateTimeFrom.ValidateDate()) return;

            Resource.Properties[@"DateFrom"] = dateTimeFrom.TimeSpan.ToString(CultureInfo.InvariantCulture);
            Resource.Properties[@"DateTo"] = string.Empty;
        }
        else
        {
            DateTime dateTimeTo = DateTime.Now;
            string strDateTimeTo = string.Empty;
            if (PeriodListDropDown.SelectedValue.Equals(@"Last Month", StringComparison.InvariantCultureIgnoreCase))
            {
                dateTimeTo = new DateTime(dateTimeTo.Year, dateTimeTo.Month,
                        dateTimeTo.AddDays(-dateTimeTo.Day + 1).Day).AddMilliseconds(-1);
                strDateTimeTo = dateTimeTo.ToString(CultureInfo.InvariantCulture);
            }
            Resource.Properties[@"DateFrom"] = TimeParser.GetTime(PeriodListDropDown.SelectedValue).ToString(CultureInfo.InvariantCulture);
            Resource.Properties[@"DateTo"] = strDateTimeTo;
        }

        Resource.Properties[@"Granularity"] = GranularityDropDown.SelectedValue;
        Resource.Properties[@"TimeRange"] = PeriodListDropDown.SelectedValue;

        Response.Redirect(BaseResourceControl.GetEditPageReturnUrl(Request.QueryString, Resource.View.ViewID.ToString()));
    }

    protected void CancelClick(object sender, EventArgs e)
    {
        Response.Redirect(BaseResourceControl.GetEditPageReturnUrl(Request.QueryString, Resource.View.ViewID.ToString()));
    }

    protected void Granularity_SelectedIndexChanged(object sender, EventArgs e)
    {
        RenderPeriodListByGranularity(GranularityDropDown.SelectedValue, string.Empty);
        datePeriodHolder.Visible = false;
    }

    private void RenderGranularityList()
    {
        Dictionary<string, string> items = TimeParser.GenerateGranularities();
        foreach (KeyValuePair<string, string> item in items)
        {
            GranularityDropDown.Items.Add(new ListItem(item.Value, item.Key));
        }
    }

    private void RenderPeriodListByGranularity(string granularity, string selectedValue)
    {
        PeriodListDropDown.Items.Clear();

        Dictionary<string, string> items = TimeParser.GeneratePeriods(granularity);
        foreach (KeyValuePair<string, string> item in items)
        {
            PeriodListDropDown.Items.Add(new ListItem(item.Value, item.Key));
        }

        if (!string.IsNullOrEmpty(selectedValue))
        {
            PeriodListDropDown.SelectedValue = selectedValue;
        }
        else
        {
            PeriodListDropDown.SelectedIndex = 0;
        }
    }

    protected void PeriodListDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool visible = PeriodListDropDown.SelectedValue == @"Custom";
        datePeriodHolder.Visible = visible;
    }
}
