<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DevicesBackedVsNotChartEdit.aspx.cs" Inherits="Orion_NCM_Resources_NCMSummary_DevicesBackedVsNotChartEdit" MasterPageFile="~/Orion/NCM/NCMResourceEditMasterPage.master" ValidateRequest="false" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources" Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register Src="~/Orion/NCM/Resources/NCMCommonResources/FilterNodeSql.ascx" TagPrefix="ncm" TagName="FilterNodeSql" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmMainContent" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../styles/NCMResources.css" />
    <asp:UpdatePanel ID="uPanel" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <table>
                <tr>
                    <td class="PageHeader">
                        <%=ResourceHeader %>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 10px;">
                        <b><%=Resources.NCMWebContent.WEBDATA_VK_128 %></b><br />
                        <asp:TextBox runat="server" ID="ResourceTitle" Width="250"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 5px;">
                        <b><%=Resources.NCMWebContent.WEBDATA_VK_203 %></b>
                        <div style="padding-top: 5px;">
                            <asp:DropDownList ID="PeriodListDropDown" runat="server" AutoPostBack="True" OnSelectedIndexChanged="PeriodListDropDown_SelectedIndexChanged" />
                        </div>
                        <div id="datePeriodHolder" runat="server" style="padding-top: 5px;">
                            <table>
                                <tr>
                                    <td valign="top">
                                        <ncm:Calendar ID="dateTimeFrom" AlternativeDrawingMode="true" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_157 %>" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 5px;">
                        <asp:CheckBox ID="chbIncludeDiscardConfigs" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_220 %>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ncm:FilterNodeSql runat="server" ID="FilterNodeSql" />
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 20px;">
                        <orion:LocalizableButton ID="btnSubmit" runat="server" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitClick"></orion:LocalizableButton>
                        <orion:LocalizableButton ID="btnCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="CancelClick" CausesValidation="false"></orion:LocalizableButton>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
