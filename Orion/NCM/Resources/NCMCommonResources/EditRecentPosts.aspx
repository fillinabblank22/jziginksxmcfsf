<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMResourceEditMasterPage.master" AutoEventWireup="true" CodeFile="EditRecentPosts.aspx.cs" Inherits="EditRecentPosts"%>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmMainContent" Runat="Server">
    <table>
        <tr>
            <td class="PageHeader">
                <%=ResourceHeader %>
            </td>
        </tr>
        <tr>
            <td style="padding-top:10px;">
                <b><%=Resources.NCMWebContent.WEBDATA_VK_182 %></b><br />
                <igtxt:WebNumericEdit ID="PostsCount" runat="server" HorizontalAlign="Center" Width="60" MaxValue="100" MinValue="1" MaxLength="3">
                    <SpinButtons Display="OnRight" />
                </igtxt:WebNumericEdit>
            </td>
        </tr>
        <tr>
            <td style="padding-top:20px;">
                <orion:LocalizableButton ID="btnSubmit" runat="server" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitClick"></orion:LocalizableButton>
            </td>
        </tr>
    </table>
</asp:Content>