﻿<%@ Control Language="C#" ClassName="FilterNodeSql" %>
<%@ Import Namespace="SolarWinds.NCM.Common.Dal" %>
<%@ Import Namespace="SolarWinds.NCMModule.Web.Resources" %>
<%@ Import Namespace="SolarWinds.Cirrus.IS.Client" %>
<script runat="server">        

    public bool ValidateWhereClause()
    {
        if (string.IsNullOrWhiteSpace(WhereClause))
            return true;

        string swqlStatement = $@"SELECT COUNT(*) as ResCount from Orion.Nodes WHERE ({WhereClause})";
        var swisRepositoryFactory = ServiceLocator.Container.Resolve<ISwisRepositoryFactory>();

        try
        {
            using (var repo = swisRepositoryFactory.CreateHttpContextRepository())
            {
                repo.Query(swqlStatement);
            }

            return true;
        }
        catch
        {
            WhereCauseValidationMessage.Visible = true;
            return false;
        }
    }

    public string WhereClause
    {
        get { return whereClauseTxt.Text.Replace(@"""", @"'"); }
        set { whereClauseTxt.Text = string.IsNullOrEmpty(value) ? string.Empty : value.Replace(@"""", @"'"); }
    }

</script>
<table>
    <tr>
        <td>
            <b><%=Resources.NCMWebContent.WEBDATA_IA_49 %></b>
            <div style="padding-top: 5px;">
                <div style="float: left; padding-right: 5px;">
                    <asp:TextBox ID="whereClauseTxt" runat="server" Width="250"></asp:TextBox>
                </div>
                <div style="color: red;" runat="server" id="WhereCauseValidationMessage" visible="false">
                    <b><%=Resources.NCMWebContent.WEBDATA_IA_50 %></b>
                </div>
            </div>

        </td>
    </tr>
    <tr>
        <td>
            <p><%= Resources.NCMWebContent.WEBDATA_IA_52 %></p>
            <orion:CollapsePanel runat="server" Collapsed="true">
                <TitleTemplate><b><%= Resources.NCMWebContent.WEBDATA_IA_56 %></b></TitleTemplate>
                <BlockTemplate>
                    <%= Resources.NCMWebContent.WEBDATA_IA_53 %>
                    <orion:CollapsePanel ID="CollapsePanel1" runat="server" Collapsed="true">
                        <TitleTemplate><b><%= Resources.NCMWebContent.WEBDATA_IA_55 %></b></TitleTemplate>
                        <BlockTemplate>
                            <ul>
                                <li>AgentPort </li>
                                <li>Allow64BitCounters </li>
                                <li>AncestorDetailsUrls</li>
                                <li>AncestorDisplayNames</li>
                                <li>AvgResponseTime </li>
                                <li>BlockUntil </li>
                                <li>BufferBgMissThisHour </li>
                                <li>BufferBgMissToday </li>
                                <li>BufferHgMissThisHour </li>
                                <li>BufferHgMissToday </li>
                                <li>BufferLgMissThisHour </li>
                                <li>BufferLgMissToday </li>
                                <li>BufferMdMissThisHour </li>
                                <li>BufferMdMissToday </li>
                                <li>BufferNoMemThisHour </li>
                                <li>BufferNoMemToday </li>
                                <li>BufferSmMissThisHour </li>
                                <li>BufferSmMissToday </li>
                                <li>Caption</li>
                                <li>ChildStatus </li>
                                <li>CMTS </li>
                                <li>Community </li>
                                <li>Contact </li>
                                <li>CPULoad </li>
                                <li>CustomPollerLastStatisticsPoll </li>
                                <li>CustomPollerLastStatisticsPollSuccess </li>
                                <li>DNS</li>
                                <li>DynamicIP</li>
                                <li>EngineID </li>
                                <li>External </li>
                                <li>GroupStatus</li>
                                <li>Icon </li>
                                <li>IOSImage </li>
                                <li>IOSVersion </li>
                                <li>IP_Address </li>
                                <li>IPAddressGUID </li>
                                <li>IPAddressType</li>
                                <li>LastBoot </li>
                                <li>LastSync </li>
                                <li>LastSystemUpTimePollUtc </li>
                                <li>Location </li>
                                <li>MachineType </li>
                                <li>MaxResponseTime </li>
                                <li>MemoryAvailable </li>
                                <li>MemoryUsed </li>
                                <li>MinResponseTime </li>
                                <li>MinutesSinceLastSync</li>
                                <li>NextPoll </li>
                                <li>NextRediscovery </li>
                                <li>NodeDescription</li>
                                <li>NodeName </li>
                                <li>ObjectSubType </li>
                                <li>OrionIdColumn </li>
                                <li>OrionIdPrefix </li>
                                <li>PercentLoss </li>
                                <li>PercentMemoryAvailable </li>
                                <li>PercentMemoryUsed </li>
                                <li>PollInterval </li>
                                <li>RediscoveryInterval </li>
                                <li>ResponseTime </li>
                                <li>RWCommunity </li>
                                <li>Severity </li>
                                <li>SkippedPollingCycles </li>
                                <li>SNMPVersion </li>
                                <li>StatCollection </li>
                                <li>StatusIcon </li>
                                <li>Status</li>
                                <li>StatusDescription</li>
                                <li>StatusLED</li>
                                <li>SysName</li>
                                <li>SysObjectID </li>
                                <li>SystemUpTime </li>
                                <li>TotalMemory </li>
                                <li>UnManaged</li>
                                <li>UnManageFrom</li>
                                <li>UnManageUntil</li>
                                <li>Vendor</li>
                                <li>VendorIcon </li>
                            </ul>
                        </BlockTemplate>
                    </orion:CollapsePanel>
                </BlockTemplate>
            </orion:CollapsePanel>
        </td>
    </tr>
</table>
