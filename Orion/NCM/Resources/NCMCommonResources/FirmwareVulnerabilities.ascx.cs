﻿using System;
using System.Globalization;

using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;
using Constants = SolarWinds.NCM.Contracts.Constants;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_NCM_Resources_NCMCommonResources_FirmwareVulnerabilities : BaseResourceControl
{
    protected INodeProvider NodeProvider { get; set; }
    protected ResourceSearchControl SearchControl { get; private set; }
    protected bool SupportINodeProvider { get { return NodeProvider != null; } }

    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NCMWebContent.WEBDATA_VK_988; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            var viewManagerAdapter = ServiceLocator.Container.Resolve<IViewManager>();
            return viewManagerAdapter.GetSupportedInterfacesForViewType(Constants.ViewTypeNCMSummary);
        }
    }

    public sealed override string DisplayTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(this.Resource.Title))
                return Resource.Title;
            return Resources.NCMWebContent.WEBDATA_VK_988;
        }
    }

    public sealed override string SubTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(Resource.SubTitle))
                return Resource.SubTitle;
            return string.Empty;
        }
    }

    public override string HelpLinkFragment
    {
        get { return @"OrionNCMPHFWareVResource"; }
    }

    public override string EditControlLocation
    {
        get
        {
            return @"/Orion/NCM/Controls/EditResourceControls/EditFirmwareVulnerabilities.ascx";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return SolarWinds.Orion.Web.UI.ResourceLoadingMode.Ajax; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        NodeProvider = GetInterfaceInstance<INodeProvider>();

        SWISValidator validator = new SWISValidator();
        validator.NodeProvider = NodeProvider;
        validator.ContentContainer = ContentContainer;
        validator.ResourceContainer = Wrapper;
        ErrorContainer.Controls.Add(validator);

        if (validator.IsValid)
        {
            SearchControl = (ResourceSearchControl)LoadControl(@"~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
            Wrapper.HeaderButtons.Controls.Add(SearchControl);
            CustomTable.UniqueClientID = ScriptFriendlyResourceID;
            CustomTable.SWQL = SWQL;
            CustomTable.SearchSWQL = SearchSWQL;
            CustomTable.OrderBy = @"V.Score DESC";
        }
        else
        {
            Wrapper.ShowEditButton = false;
        }
    }

    protected string SWQL
    {
        get
        {
            if (SupportINodeProvider)
                return string.Format(CultureInfo.InvariantCulture, @"
                    SELECT 
                        V.EntryId, 
                        VN.State, 
                        VN.StateChange, 
                        V.Score, 
                        V.Severity,
                        '/Orion/NCM/VulnerabilitySummary.aspx?EntryId=' + V.EntryId + '&CoreNodeID=' + TOSTRING(VN.CoreNodeID) AS [_LinkFor_EntryId]
                    FROM NCM.VulnerabilitiesAnnouncementsNodes AS VN
                    INNER JOIN NCM.VulnerabilitiesAnnouncements AS V ON VN.EntryId=V.EntryId
                    INNER JOIN NCM.NodeProperties AS NCMNodeProperties ON VN.CoreNodeID=NCMNodeProperties.CoreNodeID
                    INNER JOIN Orion.Nodes AS OrionNodes ON OrionNodes.NodeID=NCMNodeProperties.CoreNodeID
                    WHERE 
                        VN.CoreNodeID={0} 
                    AND
                        VN.State IN ({1})",
                    NodeProvider.Node.NodeID,
                    string.IsNullOrEmpty(Resource.Properties[@"FirmwareVulnerabilitiesStates"]) ? string.Join(@",", new int[] { (int)eVulnerabilityState.Potential_vulnerability, (int)eVulnerabilityState.Confirmed_vulnerability }) : Resource.Properties[@"FirmwareVulnerabilitiesStates"]);
            else
                return string.Format(CultureInfo.InvariantCulture, @"
                    SELECT 
                        V.EntryId AS EntryId,
                        V.Score,
                        V.Severity,
                        Count(NCMNodeProperties.CoreNodeID) AS TargetNodes,
                        '/Orion/NCM/VulnerabilitySummary.aspx?EntryId=' + V.EntryId AS [_LinkFor_EntryId],
                        '/Orion/NCM/Vulnerability/TargetNodes.aspx?ResourceID={0}&EntryId=' + V.EntryId AS [_LinkFor_TargetNodes]
                    FROM NCM.VulnerabilitiesAnnouncementsNodes AS VN
                    INNER JOIN NCM.VulnerabilitiesAnnouncements AS V ON VN.EntryId=V.EntryId
                    INNER JOIN NCM.NodeProperties AS NCMNodeProperties ON VN.CoreNodeID=NCMNodeProperties.CoreNodeID
                    INNER JOIN Orion.Nodes AS OrionNodes ON OrionNodes.NodeID=NCMNodeProperties.CoreNodeID
                    WHERE VN.State IN ({1})
                    GROUP BY V.EntryId, V.Score, V.Severity",
                    Resource.ID,
                    string.IsNullOrEmpty(Resource.Properties[@"FirmwareVulnerabilitiesStates"]) ?
                        string.Join(@",", new int[] { (int)eVulnerabilityState.Potential_vulnerability,
                            (int)eVulnerabilityState.Not_applicable,
                            (int)eVulnerabilityState.Remediated,
                            (int)eVulnerabilityState.Remediation_planned,
                            (int)eVulnerabilityState.Waiver,
                            (int)eVulnerabilityState.Confirmed_vulnerability }) : Resource.Properties[@"FirmwareVulnerabilitiesStates"]);
        }
    }

    public string SearchSWQL
    {
        get
        {
            if (SupportINodeProvider)
                return string.Format(CultureInfo.InvariantCulture, @"
                    SELECT 
                        V.EntryId, 
                        VN.State, 
                        VN.StateChange, 
                        V.Score, 
                        V.Severity,
                        '/Orion/NCM/VulnerabilitySummary.aspx?EntryId=' + V.EntryId + '&CoreNodeID=' + TOSTRING(VN.CoreNodeID) AS [_LinkFor_EntryId]
                    FROM NCM.VulnerabilitiesAnnouncementsNodes AS VN
                    INNER JOIN NCM.VulnerabilitiesAnnouncements AS V ON VN.EntryId=V.EntryId
                    INNER JOIN NCM.NodeProperties AS NCMNodeProperties ON VN.CoreNodeID=NCMNodeProperties.CoreNodeID
                    INNER JOIN Orion.Nodes AS OrionNodes ON OrionNodes.NodeID=NCMNodeProperties.CoreNodeID
                    WHERE 
                        VN.CoreNodeID={0} 
                    AND 
                        VN.State IN ({1})
                    AND 
                        V.EntryId LIKE '%${{SEARCH_STRING}}%'",
                    NodeProvider.Node.NodeID,
                    string.IsNullOrEmpty(Resource.Properties[@"FirmwareVulnerabilitiesStates"]) ? string.Join(@",", new int[] { (int)eVulnerabilityState.Potential_vulnerability, (int)eVulnerabilityState.Confirmed_vulnerability }) : Resource.Properties[@"FirmwareVulnerabilitiesStates"]);
            else
                return string.Format(CultureInfo.InvariantCulture, @"
                    SELECT 
                        V.EntryId AS EntryId,
                        V.Score,
                        V.Severity,
                        Count(NCMNodeProperties.CoreNodeID) AS TargetNodes,
                        '/Orion/NCM/VulnerabilitySummary.aspx?EntryId=' + V.EntryId AS [_LinkFor_EntryId],
                        '/Orion/NCM/Vulnerability/TargetNodes.aspx?ResourceID={0}&EntryId=' + V.EntryId AS [_LinkFor_TargetNodes]
                    FROM NCM.VulnerabilitiesAnnouncementsNodes AS VN
                    INNER JOIN NCM.VulnerabilitiesAnnouncements AS V ON VN.EntryId=V.EntryId
                    INNER JOIN NCM.NodeProperties AS NCMNodeProperties ON VN.CoreNodeID=NCMNodeProperties.CoreNodeID
                    INNER JOIN Orion.Nodes AS OrionNodes ON OrionNodes.NodeID=NCMNodeProperties.CoreNodeID
                    WHERE 
                        VN.State IN ({1})
                    AND
                        V.EntryId LIKE '%${{SEARCH_STRING}}%'
                    GROUP BY V.EntryId, V.Score, V.Severity",
                    Resource.ID,
                    string.IsNullOrEmpty(Resource.Properties[@"FirmwareVulnerabilitiesStates"]) ?
                        string.Join(@",", new int[] { (int)eVulnerabilityState.Potential_vulnerability,
                            (int)eVulnerabilityState.Not_applicable,
                            (int)eVulnerabilityState.Remediated,
                            (int)eVulnerabilityState.Remediation_planned,
                            (int)eVulnerabilityState.Waiver,
                            (int)eVulnerabilityState.Confirmed_vulnerability }) : Resource.Properties[@"FirmwareVulnerabilitiesStates"]);
        }
    }
}
