﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FirmwareVulnerabilities.ascx.cs" Inherits="Orion_NCM_Resources_NCMCommonResources_FirmwareVulnerabilities" %>

<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>

<orion:ResourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:Panel ID="ContentContainer" runat="server">
            <orion:CustomQueryTable runat="server" ID="CustomTable"/> 
            <script type="text/javascript">
                var supportINodeProvider = <%=SupportINodeProvider.ToString().ToLowerInvariant()%>;
                var state_Potential_vulnerability = <%=(int)SolarWinds.NCM.Contracts.InformationService.eVulnerabilityState.Potential_vulnerability %>;
                var state_Confirmed_vulnerability = <%=(int)SolarWinds.NCM.Contracts.InformationService.eVulnerabilityState.Confirmed_vulnerability %>;
                var state_Not_applicable = <%=(int)SolarWinds.NCM.Contracts.InformationService.eVulnerabilityState.Not_applicable %>;
                var state_Remediated = <%=(int)SolarWinds.NCM.Contracts.InformationService.eVulnerabilityState.Remediated %>;
                var state_Remediation_planned = <%=(int)SolarWinds.NCM.Contracts.InformationService.eVulnerabilityState.Remediation_planned %>;
                var state_Waiver = <%=(int)SolarWinds.NCM.Contracts.InformationService.eVulnerabilityState.Waiver %>;

                $(function () {
                    function isFloat(v) {
                        return v.toString().indexOf('.') != -1;
                    }

                    function renderState(value) {
                        switch(value)
                        { 
                            case state_Potential_vulnerability: return '<%= Resources.NCMWebContent.WEBDATA_VK_993 %>';
                            case state_Confirmed_vulnerability: return '<%= Resources.NCMWebContent.WEBDATA_VK_994 %>';
                            case state_Not_applicable: return '<%= Resources.NCMWebContent.WEBDATA_VK_996 %>';
                            case state_Remediation_planned: return '<%= Resources.NCMWebContent.WEBDATA_VK_1035 %>';
                            case state_Remediated: return '<%= Resources.NCMWebContent.WEBDATA_VK_995 %>';
                            case state_Waiver: return '<%= Resources.NCMWebContent.WEBDATA_VK_1036 %>';
                            default : return '';
                        }
                    }

                    function renderSeverity(value) {
                        switch (value) {
                        case 'High': return '<%= Resources.NCMWebContent.FirmwareVulnerability_Severity_High %>';
                        case 'Medium' : return '<%= Resources.NCMWebContent.FirmwareVulnerability_Severity_Medium %>';
                        case 'Low' : return '<%= Resources.NCMWebContent.FirmwareVulnerability_Severity_Low %>';
                        default: return '';
                        }
                    }

                    function renderStateChange(value) {
                        if (value == null || value == '') {
                            return '<%= Resources.NCMWebContent.WEBDATA_VK_1002 %>';
                        }
                        return value;
                    }

                    function renderScore(value) {
                        if (isFloat(value)) {
                            return value.toFixed(1);
                        }
                        else {
                            return value;
                        }
                    }

                    function renderTargetNodes(value) {
                        if (value == 1) {
                            return String.format('<%= Resources.NCMWebContent.FirmwareVulnerabilityResource_ColumnValue_Node%>', value);
                        }
                        else {
                            return String.format('<%= Resources.NCMWebContent.FirmwareVulnerabilityResource_ColumnValue_Nodes%>', value);
                        }
                    }

                    function getColumnSettings() {
                        if(supportINodeProvider)
                            return {
                                "EntryId": {
                                    header: '<%= Resources.NCMWebContent.WEBDATA_VK_989 %>'
                                },
                                "State": {
                                    header: '<%= Resources.NCMWebContent.WEBDATA_VK_990 %>',
                                    formatter: function (value, row, cellInfo) {
                                        return renderState(value);
                                    }
                                },
                                "StateChange": {
                                    header: '<%= Resources.NCMWebContent.WEBDATA_VK_991 %>',
                                    formatter: function (value, row, cellInfo) {
                                        return renderStateChange(value);
                                    }
                                },
                                "Score": {
                                    header: '<%= Resources.NCMWebContent.WEBDATA_VK_992 %>',
                                    formatter: function (value, row, cellInfo) {
                                        return renderScore(value);
                                    }
                                },
                                "Severity": {
                                    header: '<%= Resources.NCMWebContent.FirmwareVulnerabilityResource_ColumnHeader_Severity %>',
                                    formatter: function(value, row, cellInfo) {
                                        return renderSeverity(value);
                                    }
                                }
                            };
                        else
                            return  {
                                "EntryId": {
                                    header: '<%= Resources.NCMWebContent.WEBDATA_VK_989 %>'
                                },
                                "Score": {
                                    header: '<%= Resources.NCMWebContent.WEBDATA_VK_992 %>',
                                    formatter: function (value, row, cellInfo) {
                                        return renderScore(value);
                                    }
                                },
                                "Severity": {
                                    header: '<%= Resources.NCMWebContent.FirmwareVulnerabilityResource_ColumnHeader_Severity %>',
                                    formatter: function(value, row, cellInfo) {
                                        return renderSeverity(value);
                                    }
                                },
                                "TargetNodes": {
                                    header: String.format('<span style="text-transform:none;">{0}</span>', '<%= Resources.NCMWebContent.WEBDATA_VK_95 %>'),
                                    formatter: function (value, row, cellInfo) {
                                        return renderTargetNodes(value);
                                    }

                                }
                            };
                    }

                    SW.Core.Resources.CustomQuery.initialize({
                        uniqueId: <%= ScriptFriendlyResourceID %>,
                        initialPage: 0,
                        rowsPerPage: <%= Resource.Properties[@"FirmwareVulnerabilitiesPageSize"] ?? @"5" %>,
                        searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
                        searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
                        allowSort: true,
                        allowSearch: true,
                        columnSettings: getColumnSettings()
                    });

                    var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= Resource.ID %>); };
                    SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                    refresh();
                });	    
            </script>
        </asp:Panel>
        <div id="ErrorContainer" runat="server"/>
    </Content>
</orion:ResourceWrapper>
