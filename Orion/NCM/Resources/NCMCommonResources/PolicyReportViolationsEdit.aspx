<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/NCM/NCMResourceEditMasterPage.master" CodeFile="PolicyReportViolationsEdit.aspx.cs" Inherits="Orion_NCM_Resources_NCMCommonResources_PolicyReportViolationsEdit" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmMainContent" Runat="Server">
    <link rel="stylesheet" type="text/css" href="../../styles/NCMResources.css" />
    <asp:UpdatePanel ID="uPanel" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <table>
                <tr>
                    <td class="PageHeader">
                        <%=ResourceHeader %>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:10px;">
                        <b><%=Resources.NCMWebContent.WEBDATA_VK_128 %></b><br />
                        <asp:TextBox runat="server" ID="ResourceTitle" Width="250"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:5px;">
                        <b><%=Resources.NCMWebContent.WEBDATA_VK_231 %></b>
                        <div style="padding-top:5px;">
                            <asp:DropDownList id="PolicyReportList" runat="server"/>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:5px;">
                        <b><%=Resources.NCMWebContent.WEBDATA_VK_225 %></b>
                        <div style="padding-top:5px;">
                            <asp:DropDownList id="GranularityDropDown" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Granularity_SelectedIndexChanged"/>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:5px;">
                        <b><%=Resources.NCMWebContent.WEBDATA_VK_203 %></b>
                        <div style="padding-top:5px;">
                            <asp:DropDownList id="PeriodListDropDown" runat="server" AutoPostBack="True" OnSelectedIndexChanged="PeriodListDropDown_SelectedIndexChanged"/>
                        </div>
                        <div id="datePeriodHolder" runat="server" style="padding-top:5px;">
                            <table>
                                <tr>                                    
                                    <td valign="top" style="width:1%;"><ncm:Calendar ID="dateTimeFrom" AlternativeDrawingMode="true" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_157 %>" runat="server" /></td>
                                    <td valign="top" style="padding-left:15px;width:1%;"><ncm:Calendar ID="dateTimeTo" AlternativeDrawingMode="true" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_158 %>" runat="server" /></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:20px;">
                        <orion:LocalizableButton ID="btnSubmit" runat="server" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitClick"></orion:LocalizableButton>
                        <orion:LocalizableButton ID="btnCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="CancelClick" CausesValidation="false"></orion:LocalizableButton>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
