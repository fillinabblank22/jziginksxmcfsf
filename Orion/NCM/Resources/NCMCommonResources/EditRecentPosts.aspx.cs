using System;
using System.Web.UI;
using Infragistics.WebUI.WebDataInput;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.Resources;
using SolarWinds.NCMModule.Web.Resources.Wrappers;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class EditRecentPosts : Page
{
    private readonly IEditResourcePageService editResourceService;

    public EditRecentPosts()
    {
        editResourceService = ServiceLocator.Container.Resolve<IEditResourcePageService>();
    }

    protected ResourceInfo Resource { get; private set; }
    protected string ResourceHeader => editResourceService.GetHeader(Resource);

    private void SetupControlFromRequest()
    {
        PostsCount.Text = !string.IsNullOrEmpty(Resource.Properties[@"PostsCount"]) ? Resource.Properties[@"PostsCount"] : @"20";

        PostsCount.DataMode = NumericDataMode.Uint;
    }

    protected override void OnInit(EventArgs e)
    {
        Resource = editResourceService.Init(new PageWrap(this), null, null);

        SetupControlFromRequest();

        base.OnInit(e);
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        Resource.Properties[@"PostsCount"] = PostsCount.Text;

        Response.Redirect(BaseResourceControl.GetEditPageReturnUrl(Request.QueryString, Resource.View.ViewID.ToString()));
    }
}
