using System;
using System.Linq;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.InformationService.Contract2;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using Constants = SolarWinds.NCM.Contracts.Constants;
using StringRegistrar = SolarWinds.Orion.Core.Common.i18n.Registrar.ResourceManagerRegistrar;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Events)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_CirrusResources_EventHistory : BaseResourceControl
{
    private DateTime startTime;
    private DateTime endTime;
    private string timeRange = @"Last 7 Days";
    private Guid? nodeId;
    private int lastX;

    private readonly ICommonHelper commonHelper;
    private readonly IIsWrapper isLayer;

    private static readonly IReadOnlyDictionary<string, string> actionMapping = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase)
    {
        { @"chbConfig downloaded", StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"LIBCODE_IC_133") },
        { @"chbDownload Config requested by user", StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"LIBCODE_VK_211") },
        { @"chbEdit Config", StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"LIBCODE_IC_124") },
        { @"chbDelete Config", StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"LIBCODE_IC_123") },
        { @"chbUpload Config requested by user", StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"LIBCODE_VK_212") },
        { @"chbDownload Config requested by job", StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"BLAuditDownloadConfigRequestedByJob") },
        { @"chbUpload Config requested by job", StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"BLAuditUploadConfigRequestedByJob") },
        { @"chbUpload Scripts requested by user", StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"LIBCODE_VK_213") },
        { @"chbUpload Scripts requested by job", StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"BLAuditUploadScriptsRequestedByJob") },
        { @"chbAssign EOS dates", StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"LIBCODE_VK_215") },
        { @"chbConfig Change Approval", StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"LIBCODE_IC_127") },
        { @"chbVulnerability State Change", StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"BLAuditVulnerabilityStateChange") },
        { @"chbFirmware Upgrade", StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"LIBCODE_VK_217") }
    };

    private static readonly IReadOnlyDictionary<string, EventRowStyle> actionStyleMapping = new Dictionary<string, EventRowStyle>(StringComparer.InvariantCultureIgnoreCase)
    {
        { StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"LIBCODE_IC_133"), new EventRowStyle {BackgroundColor = @"#ffffc0",ImagePath = @"/Orion/NCM/Resources/images/Events/EventConfigChange.gif"} },
        { StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"LIBCODE_VK_211"), new EventRowStyle {BackgroundColor = @"#dcdcdc",ImagePath = @"/Orion/NCM/Resources/images/Events/EventConfigDownload.gif"} },
        { StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"LIBCODE_IC_124"), new EventRowStyle {BackgroundColor = @"#daf4cf",ImagePath = @"/Orion/NCM/Resources/images/Events/EventConfigEdit.gif"} },
        { StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"LIBCODE_IC_123"), new EventRowStyle {BackgroundColor = @"#aaf7e3",ImagePath = @"/Orion/NCM/Resources/images/Events/EventConfigDelete.gif"} },
        { StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"LIBCODE_VK_212"), new EventRowStyle {BackgroundColor = @"#ffffc0",ImagePath = @"/Orion/NCM/Resources/images/Events/EventConfigUpload.gif"} },
        { StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"BLAuditDownloadConfigRequestedByJob"), new EventRowStyle {BackgroundColor = @"#dcdcdc",ImagePath = @"/Orion/NCM/Resources/images/Events/EventConfigDownload.gif" } },
        { StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"BLAuditUploadConfigRequestedByJob"), new EventRowStyle {BackgroundColor = @"#ffffc0",ImagePath = @"/Orion/NCM/Resources/images/Events/EventConfigUpload.gif"} },
        { StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"LIBCODE_VK_213"), new EventRowStyle {BackgroundColor = @"#ffffc0",ImagePath = @"/Orion/NCM/Resources/images/Events/EventConfigUpload.gif"} },
        { StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"BLAuditUploadScriptsRequestedByJob"), new EventRowStyle {BackgroundColor = @"#ffffc0",ImagePath = @"/Orion/NCM/Resources/images/Events/EventConfigUpload.gif"} },
        { StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"LIBCODE_VK_215"), new EventRowStyle {BackgroundColor = @"#C1E9FE",ImagePath = @"/Orion/NCM/Resources/images/Events/EventAssignEOSDates.gif"} },
        { StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"LIBCODE_IC_127"), new EventRowStyle {BackgroundColor = @"#dcdcdc",ImagePath = @"/Orion/NCM/Resources/images/Events/EventConfigChangeApproval.gif"} },
        { StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"BLAuditVulnerabilityStateChange"), new EventRowStyle {BackgroundColor = @"#dcdcdc",ImagePath = @"/Orion/NCM/Resources/images/Events/EventVulnStateChange.png"} },
        { StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"LIBCODE_VK_217"), new EventRowStyle {BackgroundColor = @"#ffffc0",ImagePath = @"/Orion/NCM/Resources/images/Events/EventConfigUpload.gif" } }
    };

    public Orion_NetPerfMon_Resources_CirrusResources_EventHistory()
    {
        commonHelper = ServiceLocator.Container.Resolve<ICommonHelper>();
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
    }

    protected override string DefaultTitle => Resources.NCMWebContent.WEBDATA_VK_141;

    public sealed override string DisplayTitle
    {
        get
        {
            if (string.Equals(this.Resource.Title, DefaultTitle))
            {
                if (!string.IsNullOrEmpty(this.Resource.Properties[@"Title"]))
                {
                    return this.Resource.Properties[@"Title"];
                }
                return Resources.NCMWebContent.WEBDATA_VK_141;
            }
            else
            {
                return this.Resource.Title;
            }
        }
    }

    public override string HelpLinkFragment => @"OrionNCMNodeConfigurationHistory";

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            var viewManagerAdapter = ServiceLocator.Container.Resolve<IViewManager>();
            return viewManagerAdapter.GetSupportedInterfacesForViewType(Constants.ViewTypeNCMSummary);
        }
    }

    public override string EditControlLocation => @"/Orion/NCM/Controls/EditResourceControls/EditEventHistory.ascx";

    protected INodeProvider NodeProvider { get; set; }

    /// <summary>
    /// Set data from database or set default.
    /// </summary>
    private void SetupControlFromRequest()
    {
        if (!string.IsNullOrEmpty(this.Resource.Properties[@"Title"]))
        {
            this.Resource.Title = this.Resource.Properties[@"Title"];
        }

        if (!string.IsNullOrEmpty(this.Resource.Properties[@"Period"]))
        {
            timeRange = this.Resource.Properties[@"Period"];
        }

        lastX = 10;
        if (!string.IsNullOrEmpty(this.Resource.Properties[@"LastX"]))
        {
            lastX = int.Parse(this.Resource.Properties[@"LastX"]);
        }

        SetupDateTimePeriod();

        NodeProvider = GetInterfaceInstance<INodeProvider>();
        if (NodeProvider != null)
        {
            nodeId = commonHelper.GetNodeIdFromNodeProvider(NodeProvider);
        }
    }

    private void SetupDateTimePeriod()
    {
        if (timeRange != @"Custom")
        {
            endTime = DateTime.Now;
            if (timeRange.Equals(@"Last Month", StringComparison.InvariantCultureIgnoreCase))
            {
                endTime = new DateTime(endTime.Year, endTime.Month, endTime.AddDays(-endTime.Day + 1).Day).AddMilliseconds(-1);
            }
            startTime = TimeParser.GetTime(timeRange);

            this.Resource.SubTitle = TimeParser.GetLocalizedTimeRange(timeRange);
        }
        else
        {
            if (string.IsNullOrEmpty(this.Resource.Properties[@"DateFrom"]))
            {
                startTime = DateTime.Now.AddDays(-7);
            }
            else
            {
                startTime = Convert.ToDateTime(this.Resource.Properties[@"DateFrom"], CultureInfo.InvariantCulture);
            }
            if (string.IsNullOrEmpty(this.Resource.Properties[@"DateTo"]))
            {
                endTime = DateTime.Now;
            }
            else
            {
                endTime = Convert.ToDateTime(this.Resource.Properties[@"DateTo"], CultureInfo.InvariantCulture);
            }
            this.Resource.SubTitle = string.Format(Resources.NCMWebContent.WEBDATA_VK_139, startTime, endTime);
        }

        this.Resource.SubTitle += @" ";
        this.Resource.SubTitle += string.Format(Resources.NCMWebContent.WEBDATA_VK_138, lastX);
    }

    /// <summary>
    /// Set data table for report list.
    /// </summary>
    /// <returns></returns>
    private DataTable SetAuditTable()
    {
        var dtAudit = new DataTable();
        var actions = new List<string>();
        var propertiesExists = false;

        foreach (DictionaryEntry property in this.Resource.Properties)
        {
            if (property.Key.ToString().StartsWith(@"chb"))
            {
                if (property.Key.ToString().Equals(@"chbAdd Device", StringComparison.InvariantCultureIgnoreCase) ||
                    property.Key.ToString().Equals(@"chbEdit Device", StringComparison.InvariantCultureIgnoreCase))
                {
                    continue;
                }

                propertiesExists = true;
                string action;
                if (Convert.ToBoolean(property.Value) && actionMapping.TryGetValue(property.Key.ToString(), out action))
                {
                    actions.Add($@"A.Action = '{action}'");
                }
            }
        }

        if (!propertiesExists)
        {
            actions.Add($@"A.Action = '{actionMapping[@"chbConfig downloaded"]}'");
            actions.Add($@"A.Action = '{actionMapping[@"chbUpload Config requested by user"]}'");
            actions.Add($@"A.Action = '{actionMapping[@"chbUpload Config requested by job"]}'");
            actions.Add($@"A.Action = '{actionMapping[@"chbUpload Scripts requested by user"]}'");
            actions.Add($@"A.Action = '{actionMapping[@"chbUpload Scripts requested by job"]}'");
        }

        if (actions.Any())
        {
            if (!nodeId.HasValue)
            {
                using (var proxy = isLayer.GetProxy())
                {
                    dtAudit = proxy.Query($@"SELECT TOP {lastX} A.DateTime, A.UserName, A.Type, A.Action, A.Details FROM Cirrus.Audit AS A WHERE ({string.Join(@" OR ", actions)}) AND ToUtc(A.DateTime) > @datestart AND ToUtc(A.DateTime) < @dateend ORDER BY A.DateTime DESC", new PropertyBag
                    {
                        {@"datestart", startTime.GetAsLocal() },
                        {@"dateend", endTime.GetAsLocal() }
                    });
                }
            }
            else
            {
                using (var proxy = isLayer.GetProxy())
                {
                    var dtNodes = proxy.Query(
                        $@"SELECT OrionNodes.IPAddress FROM Cirrus.NodeProperties AS NcmNodes INNER JOIN Orion.Nodes AS OrionNodes on NcmNodes.CoreNodeID=OrionNodes.NodeID WHERE NcmNodes.NodeID=@nodeId", new PropertyBag{
                        {@"nodeId", nodeId }
                    });

                    var ipAddress = dtNodes.Rows.Count > 0 ? dtNodes.Rows[0]["IPAddress"].ToString() : string.Empty;

                    foreach (DataRow row in dtNodes.Rows)
                    {
                        ipAddress = row["IPAddress"].ToString();
                    }
                    dtAudit = proxy.Query($@"SELECT TOP {lastX} A.DateTime, A.UserName, A.Type, A.Action, A.Details FROM Cirrus.Audit AS A WHERE ({string.Join(@" OR ", actions)}) AND ToUtc(A.DateTime) > @datestart AND ToUtc(A.DateTime) < @dateend AND (A.Details LIKE @ipaddressLike OR A.Details = @ipaddressExactMatch) ORDER BY A.DateTime DESC", new PropertyBag
                    {
                        {@"datestart", startTime.GetAsLocal() },
                        {@"dateend", endTime.GetAsLocal() },
                        {@"ipaddressLike", @"% " + ipAddress},
                        {@"ipaddressExactMatch", ipAddress }
                    });
                }
            }
        }

        dtAudit.Columns.Add(new DataColumn("EventTypeCss", typeof(string)));
        dtAudit.Columns.Add(new DataColumn("EventImage", typeof(string)));

        foreach (DataRow row in dtAudit.Rows)
        {
            if (row["UserName"].ToString().Contains(@"\"))
            {
                var temp = row["UserName"].ToString().Split('\\');
                row["UserName"] = temp[temp.Length - 1];
            }

            EventRowStyle eventRowStyle;
            if(actionStyleMapping.TryGetValue(row["Action"].ToString(), out eventRowStyle))
            {
                row["EventTypeCss"] = $@"background: {eventRowStyle.BackgroundColor};border-bottom: #797979 1px groove;font-family: Arial, Verdana, Helvetica, sans-serif;font-size: 8pt;font-weight: normal;margin-bottom: 0px;margin-top: 0px;";
                row["EventImage"] = $@"<img alt='' src='{eventRowStyle.ImagePath}' border='0'>";
            }
            else
            {
                row["EventTypeCss"] = string.Empty;
                row["EventImage"] = @"&nbsp;";
            }
            
        }

        if (dtAudit.Rows.Count == 0)
        {
            ErrorContainer.InnerText = Resources.NCMWebContent.WEBDATA_VK_140;
        }

        return dtAudit;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        var validator = new SWISValidator
        {
            NodeProvider = NodeProvider,
            ContentContainer = ContentContainer,
            ResourceContainer = Wrapper
        };
        ErrorContainer.Controls.Add(validator);

        if (validator.IsValid)
        {
            SetupControlFromRequest();
            if (!Page.IsPostBack)
            {
                Session[$@"NCM_Audit_{this.ClientID}"] = SetAuditTable();
            }
            else
            {
                if (Session[$@"NCM_Audit_{this.ClientID}"] == null)
                {
                    Session[$@"NCM_Audit_{this.ClientID}"] = SetAuditTable();
                }
            }
            AuditList.DataSource = (DataTable)Session[$@"NCM_Audit_{this.ClientID}"];
            AuditList.DataBind();
        }
        else
        {
            Wrapper.ShowEditButton = false;
        }
    }

    private class EventRowStyle
    {
        public string BackgroundColor { get; set; }
        public string ImagePath { get; set; }
    }
}
