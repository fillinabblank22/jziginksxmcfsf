using System;
using System.Data;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.NCM.Web.Contracts;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.NPM.Web;

using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using Constants = SolarWinds.NCM.Contracts.Constants;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_NCM_Resources_NCMCommonResources_LastXConfigChanges : BaseResourceControl
{
    private readonly IIsWrapper isLayer;
    private readonly IMacroParser macroParser;
    private readonly ICommonHelper commonHelper;

    public Orion_NCM_Resources_NCMCommonResources_LastXConfigChanges()
    {
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
        macroParser = ServiceLocator.Container.Resolve<IMacroParser>();
        commonHelper = ServiceLocator.Container.Resolve<ICommonHelper>();
    }

    protected string LastX { get; set; }
    protected bool ViewChangesOn { get; set; }
    protected string ConfigType { get; set; }

    public override string DetachURL => $@"{base.DetachURL}{(Request.QueryString[@"NodeID"] != null ? @"&NodeID=" + Request.QueryString[@"NodeID"] : string.Empty)}";

    protected override string DefaultTitle => Resources.NCMWebContent.WEBDATA_VK_173;

    public sealed override string DisplayTitle => this.Title.Replace(@"XX", LastX);

    public override string HelpLinkFragment => @"OrionNCMLastXConfigChanges";

    public override string EditURL => ExtendCustomEditUrl(@"/Orion/NCM/Resources/NCMSummary/LastXConfigChangesEdit.aspx");

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get
		{
            var viewManagerAdapter = ServiceLocator.Container.Resolve<IViewManager>();
            return viewManagerAdapter.GetSupportedInterfacesForViewType(Constants.ViewTypeNCMSummary);
		}
	}

    protected INodeProvider NodeProvider { get; set; }

    private void SetupControlFromRequest()
    {
        if (!string.IsNullOrEmpty(this.Resource.Properties[@"Title"]))
        {
            this.Resource.Title = this.Resource.Properties[@"Title"];
        }

        LastX = @"5";
        if (!string.IsNullOrEmpty(this.Resource.Properties[@"LastX"]))
        {
            LastX = this.Resource.Properties[@"LastX"];
        }

        if (this.Resource.Properties[@"SubTitle"] != null)
        {
            this.Resource.SubTitle = this.Resource.Properties[@"SubTitle"];
        }

        ViewChangesOn = false;
        if (!string.IsNullOrEmpty(this.Resource.Properties[@"ViewChangesOn"]))
        {
            ViewChangesOn = Convert.ToBoolean(this.Resource.Properties[@"ViewChangesOn"]);
        }

        if (!string.IsNullOrEmpty(this.Resource.Properties[@"ConfigType"]))
        {
            ConfigType = this.Resource.Properties[@"ConfigType"];
        }
    }

    private DataTable SetConfigsTable()
    {
        var query = string.Empty;
        Guid? nodeID = null;

        NodeProvider = GetInterfaceInstance<INodeProvider>();
        if (NodeProvider != null)
        {
            nodeID = commonHelper.GetNodeIdFromNodeProvider(NodeProvider);
        }

        if (!nodeID.HasValue)
        {
            query =
                $@"Select top {LastX} N.NodeID,O.IPAddress as AgentIP,O.Caption as NodeCaption, D.ConfigTitle, D.ConfigType, K.TimeStamp, D.ConfigID, D.ComparisonType 
               from Cirrus.CacheDiffResults as D left join Cirrus.ComparisonCache as K ON K.CacheID = D.CacheID join Cirrus.NodeProperties as N on N.NodeId= D.NodeId join Orion.Nodes as O on N.CoreNodeId=O.NodeId 
               where D.ComparisonType={(ViewChangesOn ? 4 : 3)} AND D.DiffFlag = 1 {(ViewChangesOn ? $@" AND D.ConfigType='{ConfigType}'" : string.Empty)} order by K.CacheID desc with limitation {this.Resource.View.LimitationID}";
        }
        else
        {
            query =
                $@"Select top {LastX} N.NodeID,O.IPAddress as AgentIP,O.Caption as NodeCaption, D.ConfigTitle, D.ConfigType, K.TimeStamp, D.ConfigID, D.ComparisonType 
               from Cirrus.CacheDiffResults as D left join Cirrus.ComparisonCache as K ON K.CacheID = D.CacheID left join Cirrus.NodeProperties as N on N.NodeId= D.NodeId join Orion.Nodes as O on N.CoreNodeId=O.NodeId 
               where N.NodeID = '{nodeID}' AND D.ComparisonType={(ViewChangesOn ? 4 : 3)} AND D.DiffFlag = 1 {(ViewChangesOn ? $@" AND D.ConfigType='{ConfigType}'" : string.Empty)} order by K.CacheID desc with limitation {this.Resource.View.LimitationID}";
        }

        var dtConfigs = new DataTable();
        using (var proxy = isLayer.GetProxy())
        {
            dtConfigs = proxy.Query(query);
        }

        if (dtConfigs.Rows.Count == 0)
        {
            ErrorContainer.InnerText = Resources.NCMWebContent.WEBDATA_VK_174;
        }

        foreach (DataRow row in dtConfigs.Rows)
        {
            row["NodeCaption"] = macroParser.ParseMacro(Guid.Parse(row["NodeID"].ToString()), row["NodeCaption"].ToString());
            if (string.IsNullOrEmpty(row["NodeCaption"].ToString().Trim()))
            {
                row["NodeCaption"] = row["AgentIP"];
            }
        }

        return dtConfigs;
    }

    protected override void OnInit(EventArgs e)
	{
        base.OnInit(e);

        SetupControlFromRequest();
	}

    protected void Page_Load(object sender, EventArgs e)
    {
        var validator = new SWISValidator();
        validator.NodeProvider = NodeProvider;
        validator.ContentContainer = ContentContainer;
        validator.ResourceContainer = Wrapper;
        ErrorContainer.Controls.Add(validator);

        if (validator.IsValid)
        {
            if (!Page.IsPostBack)
            {
                Session[$@"NCM_ConfigChanges_{this.ClientID}"] = SetConfigsTable();
            }
            else
            {
                if (Session[$@"NCM_ConfigChanges_{this.ClientID}"] == null)
                {
                    Session[$@"NCM_ConfigChanges_{this.ClientID}"] = SetConfigsTable();
                }
            }
            ConfigList.DataSource = (DataTable)Session[$@"NCM_ConfigChanges_{this.ClientID}"];
            ConfigList.DataBind();
        }
        else
        {
            Wrapper.ShowEditButton = false;
        }
    }
}
