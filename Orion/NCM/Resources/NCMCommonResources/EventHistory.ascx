<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EventHistory.ascx.cs" Inherits="Orion_NetPerfMon_Resources_CirrusResources_EventHistory" %>

<script src="/Orion/NCM/JavaScript/main.js" type="text/javascript"></script>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <content> 
        <asp:Panel ID="ContentContainer" runat="server"> 
            <asp:Repeater runat="server" ID="AuditList">
                <HeaderTemplate>
			        <table cellpadding="0" cellspacing="0" width="100%">
				        <tr style="height:20px;">
				            <td class="ReportHeader" style="font-size:7pt;border-bottom:0px groove transparent;padding-left:2px;padding-right:2px;">
				                &nbsp;
					        </td>
					        <td class="ReportHeader" style="font-size:7pt;border-bottom:0px groove transparent;padding-left:0px;padding-right:5px;width:20%;">
						        <%= Resources.NCMWebContent.WEBDATA_VK_133 %>
					        </td>
					        <td class="ReportHeader" style="font-size:7pt;border-bottom:0px groove transparent;padding-left:0px;padding-right:5px;width:20%;">
						        <%= Resources.NCMWebContent.WEBDATA_VK_134 %>
					        </td>
					        <td class="ReportHeader" style="font-size:7pt;border-bottom:0px groove transparent;padding-left:0px;padding-right:5px;width:15%;">
						        <%= Resources.NCMWebContent.WEBDATA_VK_135 %>
					        </td>
					        <td class="ReportHeader" style="font-size:7pt;border-bottom:0px groove transparent;padding-left:0px;padding-right:5px;width:20%;">
						        <%= Resources.NCMWebContent.WEBDATA_VK_136 %>
					        </td>
					        <td class="ReportHeader" style="font-size:7pt;border-bottom:0px groove transparent;padding-left:0px;padding-right:5px;width:25%;">
						        <%= Resources.NCMWebContent.WEBDATA_VK_137 %>
					        </td>
				        </tr>
		        </HeaderTemplate>
                <ItemTemplate>
                        <tr style="height:20px;">
                            <td style="padding-left:2px;padding-right:2px;<%#DataBinder.Eval(Container.DataItem, @"EventTypeCss") %>">
                                <%#DataBinder.Eval(Container.DataItem, @"EventImage") %>
			                </td>
				            <td style="width:20%;<%#DataBinder.Eval(Container.DataItem, @"EventTypeCss") %>">
						        <%# DataBinder.Eval(Container.DataItem,@"DateTime")%>
					        </td>
					        <td style="width:20%;<%#DataBinder.Eval(Container.DataItem, @"EventTypeCss") %>">
						        <%# DataBinder.Eval(Container.DataItem, @"UserName") %>
					        </td>
					        <td style="width:15%;<%#DataBinder.Eval(Container.DataItem, @"EventTypeCss") %>">
						        <%# DataBinder.Eval(Container.DataItem, @"Type") %>
					        </td>
					        <td style="width:20%;<%#DataBinder.Eval(Container.DataItem, @"EventTypeCss") %>">
						        <%# DataBinder.Eval(Container.DataItem, @"Action") %>
					        </td>
					        <td style="width:25%;<%#DataBinder.Eval(Container.DataItem, @"EventTypeCss") %>">
						        <%# DataBinder.Eval(Container.DataItem, @"Details") %>
					        </td>
                        </tr>
		        </ItemTemplate>
		        <FooterTemplate>
		            </table>
		        </FooterTemplate>
            </asp:Repeater>
        </asp:Panel>
        <div id="ErrorContainer" runat="server"/>
    </content>
</orion:resourceWrapper>