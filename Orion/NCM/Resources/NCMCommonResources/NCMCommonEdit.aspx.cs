using System;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.Resources;
using SolarWinds.NCMModule.Web.Resources.Wrappers;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Resources_CirrusResources_NCMCommonEdit : System.Web.UI.Page
{
    private readonly IEditResourcePageService editResourceService;

    public Orion_NetPerfMon_Resources_CirrusResources_NCMCommonEdit()
    {
        editResourceService = ServiceLocator.Container.Resolve<IEditResourcePageService>();
    }

    protected ResourceInfo Resource { get; private set; }
    protected string ResourceHeader => editResourceService.GetHeader(Resource);

    protected override void OnInit(EventArgs e)
    {
        Resource = editResourceService.Init(new PageWrap(this), ResourceTitle, ResourceSubTitle);

        base.OnInit(e);
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        editResourceService.SaveChanges(Resource, ResourceTitle, ResourceSubTitle);

        Response.Redirect(BaseResourceControl.GetEditPageReturnUrl(Request.QueryString, Resource.View.ViewID.ToString()));
    }
}
