<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NodeListEdit.aspx.cs" Inherits="Orion_NCM_Resources_NCMCommonResources_NodeListEdit" MasterPageFile="~/Orion/NCM/NCMResourceEditMasterPage.master" ValidateRequest="false"%>

<asp:Content ID="MainContent" ContentPlaceHolderID="ncmMainContent" Runat="Server">
    <link rel="stylesheet" type="text/css" href="../../styles/NCMResources.css" />
    <asp:UpdatePanel ID="uPanel" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <table>
                <tr>
                    <td class="PageHeader">
                        <%=ResourceHeader %>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:10px;">
                        <b><%=Resources.NCMWebContent.WEBDATA_VK_128 %></b><br />
                        <asp:TextBox runat="server" ID="ResourceTitle" Width="250"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:5px;">
                        <asp:Panel ID="GroupingPanel1" runat="server">
                            <b><%= string.Format(Resources.NCMWebContent.WEBDATA_VK_271, 1) %></b><br/>
                            <asp:DropDownList ID="GroupingBox1" runat="server" Width="250px" AutoPostBack="True" OnSelectedIndexChanged="GroupingBox1_SelectedIndexChanged"></asp:DropDownList>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:5px;">
                        <asp:Panel ID="GroupingPanel2" runat="server">
                            <b><%= string.Format(Resources.NCMWebContent.WEBDATA_VK_271, 2) %></b><br/>
                            <asp:DropDownList ID="GroupingBox2" runat="server" Width="250px" AutoPostBack="True" OnSelectedIndexChanged="GroupingBox2_SelectedIndexChanged"></asp:DropDownList>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:5px;">
                        <asp:Panel ID="GroupingPanel3" runat="server">
                            <b><%= string.Format(Resources.NCMWebContent.WEBDATA_VK_271, 3) %></b><br/>
                            <asp:DropDownList ID="GroupingBox3" runat="server" Width="250px" AutoPostBack="True" OnSelectedIndexChanged="GroupingBox3_SelectedIndexChanged"></asp:DropDownList>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:20px;">
                        <orion:LocalizableButton ID="btnSubmit" runat="server" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitClick"></orion:LocalizableButton>
                        <orion:LocalizableButton ID="btnCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="CancelClick" CausesValidation="false"></orion:LocalizableButton>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
