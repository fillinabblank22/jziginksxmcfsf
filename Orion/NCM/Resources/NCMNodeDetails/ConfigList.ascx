<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConfigList.ascx.cs" Inherits="Orion_NetPerfMon_Resources_CirrusResources_ConfigList" %>
<link rel="stylesheet" type="text/css" href="/orion/ncm/styles/NCMResources.css"  />
<script type="text/javascript" src="/orion/ncm/JavaScript/main.js?v=1.0"></script>

<script type="text/javascript">
    var IsDemoMode =  <%=SolarWinds.NCMModule.Web.Resources.CommonHelper.IsDemoMode().ToString().ToLowerInvariant() %>;
    var DemoMessageId_SetRemoveFavorite = '<%=SolarWinds.NCMModule.Web.Resources.DemoModeConstants.NCM_MANAGECONFIGS_SET_REMOVE_FAVORITE.ToString() %>';
    var DemoMessageId_DeleteConfig = '<%=SolarWinds.NCMModule.Web.Resources.DemoModeConstants.NCM_MANAGECONFIGS_DELETE_CONFIG.ToString() %>';

    function cbClicked(obj) 
    {
        var allIds = getSelectedCfgIds('input:checked');
        var textIds = getSelectedCfgIds('input:checked:not(.Config-IsBinary)');

        var hfAllCfgIds = '<%=this.hfSelectedAllCfgIds.ClientID %>';
        var hfTextCfgIds = '<%=this.hfSelectedTextCfgIds.ClientID %>'

        $('#' + hfAllCfgIds).val(allIds.join(','));
        $('#' + hfTextCfgIds).val(textIds.join(','));

        EnableDisableMgmtButton('<%=this.setFavoriteBtn.ClientID %>', allIds.length != 0, DemoMessageId_SetRemoveFavorite);
        EnableDisableMgmtButton('<%=this.editCfgBtn.ClientID %>', textIds.length == 1);
        EnableDisableMgmtButton('<%=this.deleteCfgBtn.ClientID %>', allIds.length != 0, DemoMessageId_DeleteConfig);
    }

    function getSelectedCfgIds(selector) 
    {
        var contentId = '<%=this.ContentClientId %>';
        var ids = [];
        $('#' + contentId).find(selector).each(function () {
            ids.push($(this).val());
        });
        return ids;
    }

    function EnableDisableMgmtButton(buttonId, enable, demoMessageId) 
    {
        if(enable) {
            NCMEnableButton('#' + buttonId);
        }
        else {
            NCMDisableButton('#' + buttonId);
        }
        
        if (demoMessageId != undefined) {
            AddDemoModeMessage('#' + buttonId, demoMessageId, enable);
        }
    }
</script>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div id="managementContainer" runat="server" style="padding-top:5px;padding-bottom:5px;" >
            <table width="100%" cellpadding="2" cellspacing="0" >
                <tr>
                    <td style="border-bottom:0px;">
                        <span class="PropertyHeader"><asp:Label ID="lbl4" runat="server"></asp:Label></span>
                    </td>
                    <td style="border-bottom:0px;padding-left: 15px;">
                        <asp:HiddenField ID="hfSelectedAllCfgIds" runat="server" Value="" />
                        <asp:HiddenField ID="hfSelectedTextCfgIds" runat="server" Value="" />

                        &nbsp;<a id="setFavoriteBtn" class="disabledbtn" runat="server" onclick="return false;" ><img alt="" style="cursor:default;" src="/Orion/NCM/Resources/images/ConfigIcons/Config-Baseline.gif" /><asp:Label ID="lbl1" runat="server"></asp:Label></a>
                        
                        &nbsp;<a id="editCfgBtn" style="padding-left:10px;" class="disabledbtn" runat="server" onclick="return false;"><img alt="" style="cursor:default;" src="/Orion/NCM/Resources/images/ConfigSnippets/icon_editsnippet.gif" /><asp:Label ID="lbl2" runat="server"></asp:Label></a>
                        
                        &nbsp;<a id="deleteCfgBtn" style="padding-left:10px;" class="disabledbtn" runat="server" onclick="return false;"><img alt="" style="cursor:default;" src="/Orion/NCM/Resources/images/icon_delete.png" /><asp:Label ID="lbl3" runat="server"></asp:Label></a>
                        
                        &nbsp;<a id="importCfgBtn" style="padding-left:10px;" class="importbtn" runat="server"><img alt="" style="cursor:default;" src="/Orion/NCM/Resources/images/ConfigSnippets/icon_import.gif" /><asp:Label ID="lbl5" runat="server"></asp:Label></a>
                    </td>
                </tr>
            </table>
        </div>
    </Content>
</orion:resourceWrapper>

  
    