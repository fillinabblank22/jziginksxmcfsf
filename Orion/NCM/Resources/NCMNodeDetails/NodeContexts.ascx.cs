﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using SolarWinds.NCM.Common.Dal;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Common;
using SolarWinds.NCM.Common.Settings;
using SolarWinds.NCMModule.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_NCM_Resources_NCMNodeDetails_NodeContexts : BaseResourceControl
{
    private const string NcmAsaContextsSessionKey = "NcmAsaContexts";

    protected int StartRowNumber
    {
        get
        {
            if (ViewState["NcmAsaContexts_StartRowNumber"] == null)
                return 0;

            return (int) ViewState["NcmAsaContexts_StartRowNumber"];
        }
        set { ViewState["NcmAsaContexts_StartRowNumber"] = value; }
    }

    protected int TotalRows
    {
        get
        {
            if (ViewState["NcmAsaContexts_TotalRows"] == null)
                return 0;

            return (int) ViewState["NcmAsaContexts_TotalRows"];
        }
        set { ViewState["NcmAsaContexts_TotalRows"] = value; }
    }

    protected override string DefaultTitle => Resources.NCMWebContent.WEBDATA_VK_1058;

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] {typeof(INodeProvider)}; }
    }

    public sealed override string DisplayTitle => !string.IsNullOrEmpty(Resource.Title) ? Resource.Title : Resources.NCMWebContent.WEBDATA_VK_1058;

    public sealed override string SubTitle => !string.IsNullOrEmpty(Resource.SubTitle) ? Resource.SubTitle : string.Empty;

    public override string HelpLinkFragment { get; } = @"OrionNCMContext";

    public override string EditControlLocation { get; } = @"/Orion/NCM/Controls/EditResourceControls/EditNodeContexts.ascx";

    protected int SelectedPage => (StartRowNumber + PageSize) / PageSize;

    protected int TotalPages => (TotalRows - 1) / PageSize + 1;

    protected int PageSize
    {
        get
        {
            int pageSize;
            return int.TryParse(Resource.Properties[@"PageSize"] ?? @"5", out pageSize) ? pageSize : 5;
        }
    }

    protected object RenderLinkIfNeeded(object contextName, object caption, object nodeId)
    {
        if (nodeId == DBNull.Value)
            return contextName;

        var thisNodeId = GetInterfaceInstance<INodeProvider>().Node.NodeID;
        return nodeId.Equals(thisNodeId)
            ? $@"{contextName}, <i style='color:gray;'>this node</i>"
            : $@"<a href='/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{nodeId}'>{contextName}, {caption}</a>";
    }

    protected object RenderAddNodeButtonIfNeeded(object ipAddress, object nodeId)
    {
        if (ipAddress == DBNull.Value)
            return null;

        var ipAddresses = ipAddress.ToString().Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);
        return nodeId == DBNull.Value
            ? $@"<a href='/Orion/Nodes/Add/Default.aspx?IPAddress={ipAddresses[0]}' target='__blank'><img src='/Orion/NCM/Resources/images/ConfigSnippets/icon_addsnippet.gif' title='Add Node as Monitored' /></a>"
            : null;
    }

    protected object RenderIpAddress(object ipAddress)
    {
        if (ipAddress == DBNull.Value)
            return null;

        var ipAddresses = ipAddress.ToString().Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);
        return string.Join(@"<br />", ipAddresses);
    }

    protected void btnNextPage_Click(object sender, EventArgs e)
    {
        if (StartRowNumber + PageSize < TotalRows)
            StartRowNumber += PageSize;

        RefreshContexts(StartRowNumber, StartRowNumber + PageSize);
        EnablePageButtons();
    }

    protected void btnPrevPage_Click(object sender, EventArgs e)
    {
        if (StartRowNumber - PageSize >= 0)
            StartRowNumber -= PageSize;

        RefreshContexts(StartRowNumber, StartRowNumber + PageSize);
        EnablePageButtons();
    }

    protected void btnFirstPage_Click(object sender, EventArgs e)
    {
        StartRowNumber = 0;
        RefreshContexts(StartRowNumber, StartRowNumber + PageSize);
        EnablePageButtons();
    }

    protected void btnLastPage_Click(object sender, EventArgs e)
    {
        StartRowNumber = (TotalPages - 1) * PageSize;
        RefreshContexts(StartRowNumber, StartRowNumber + PageSize);
        EnablePageButtons();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var modulesCollector = ServiceLocator.Container.Resolve<IModulesCollector>();
        var isNpmInstalled = modulesCollector.IsModuleInstalled(SolarWinds.NCM.Contracts.Constants.NpmVersionForContexts.Name,
            SolarWinds.NCM.Contracts.Constants.NpmVersionForContexts.Version);
        var settings = ServiceLocator.Container.Resolve<INCMBLSettings>();
        if (isNpmInstalled || settings.DisableAsaContextsDiscovery)
        {
            Wrapper.Visible = false;
        }
        else
        {
            if (!IsPostBack)
            {
                Session.Remove(NcmAsaContextsSessionKey);
                StartRowNumber = 0;
            }

            RefreshContexts(StartRowNumber, StartRowNumber + PageSize);
            EnablePageButtons();

            Wrapper.Visible = TotalRows > 0;
        }
    }

    private void RefreshContexts(int start, int end)
    {
        var contexts = GetAsaContexts();
        repeater.DataSource = GetAsaContextsPaged(contexts, start, end);
        repeater.DataBind();
    }

    private void EnablePageButtons()
    {
        btnFirstPage.Enabled = StartRowNumber > 0 && TotalPages > 1;
        imgFirstPage.Src = StartRowNumber > 0 && TotalPages > 1
            ? @"/Orion/images/Arrows/button_white_paging_first.gif"
            : @"/Orion/images/Arrows/button_white_paging_first_disabled.gif";

        btnPrevPage.Enabled = StartRowNumber > 0;
        imgPrevPage.Src = StartRowNumber > 0
            ? @"/Orion/images/Arrows/button_paging_previous.gif"
            : @"/Orion/images/Arrows/button_paging_previous_disabled.gif";

        btnNextPage.Enabled = (TotalRows - StartRowNumber) > PageSize;
        imgNextPage.Src = (TotalRows - StartRowNumber) > PageSize
            ? @"/Orion/images/Arrows/button_paging_next.gif"
            : @"/Orion/images/Arrows/button_paging_next_disabled.gif";

        btnLastPage.Enabled = (TotalRows - StartRowNumber) > PageSize && SelectedPage < TotalPages;
        imgLastPage.Src = (TotalRows - StartRowNumber) > PageSize && SelectedPage < TotalPages
            ? @"/Orion/images/Arrows/button_white_paging_last.gif"
            : @"/Orion/images/Arrows/button_white_paging_last_disabled.gif";
    }

    private DataTable GetAsaContextsPaged(DataTable contexts, int start, int end)
    {
        var dt = new DataTable("NCM_AsaContexts") {Locale = CultureInfo.InvariantCulture};
        dt.Columns.Add("ContextName", typeof(string));
        dt.Columns.Add("IpAddress", typeof(string));
        dt.Columns.Add("Caption", typeof(string));
        dt.Columns.Add("NodeID", typeof(int));

        end = Math.Min(end, contexts.Rows.Count);

        for (var i = start; i < end; i++)
        {
            dt.Rows.Add(contexts.Rows[i]["ContextName"],
                contexts.Rows[i]["IpAddress"],
                contexts.Rows[i]["Caption"],
                contexts.Rows[i]["NodeID"]);
        }

        return dt;
    }

    private DataTable GetAsaContexts()
    {
        var dt = Session[NcmAsaContextsSessionKey] as DataTable;
        if (dt != null)
            return dt;

        dt = new DataTable("NCM_ASAContexts") {Locale = CultureInfo.InvariantCulture};
        dt.Columns.Add("ContextName", typeof(string));
        dt.Columns.Add("IpAddress", typeof(string));
        dt.Columns.Add("Caption", typeof(string));
        dt.Columns.Add("NodeID", typeof(int));

        const string sql = @"
        SELECT 
            NcmAsaContexts.ContextName, 
            NcmAsaContexts.IpAddress, 
            OrionNodes.Caption,
            OrionNodes.NodeID
        FROM NCM_ASAContexts AS NcmAsaContexts
        LEFT JOIN Nodes AS OrionNodes
            ON NcmAsaContexts.IpAddress = OrionNodes.IP_Address
        WHERE NcmAsaContexts.NodeId = @nodeId
        ORDER BY
            NcmAsaContexts.ContextName";

        using (var command = SqlHelper.GetTextCommand(sql))
        {
            command.Parameters.AddWithValue("@nodeId", GetInterfaceInstance<INodeProvider>().Node.NodeID);

            using (var reader = SqlHelper.ExecuteReader(command))
            {
                DataRow prevRow = null;
                var prevContextName = string.Empty;

                var colContextName = reader.GetOrdinal("ContextName");
                var colIpAddress = reader.GetOrdinal("IpAddress");
                var colCaption = reader.GetOrdinal("Caption");
                var colNodeId = reader.GetOrdinal("NodeID");

                while (reader.Read())
                {
                    var nextContextName = reader.GetString(colContextName);
                    if (!prevContextName.Equals(nextContextName))
                    {
                        prevRow = dt.Rows.Add(
                            nextContextName,
                            reader.IsDBNull(colIpAddress) ? null : reader.GetString(colIpAddress),
                            reader.IsDBNull(colCaption) ? null : reader.GetString(colCaption),
                            reader.IsDBNull(colNodeId) ? null : (int?) reader.GetInt32(colNodeId)
                        );
                    }
                    else
                    {
                        if (prevRow != null)
                        {
                            if (!reader.IsDBNull(colIpAddress))
                                prevRow["IpAddress"] = $@"{prevRow["IpAddress"]} {reader.GetString(colIpAddress)}";

                            if (!reader.IsDBNull(colCaption))
                                prevRow["Caption"] = reader.GetString(colCaption);

                            if (!reader.IsDBNull(colNodeId))
                                prevRow["NodeID"] = reader.GetInt32(colNodeId);
                        }
                    }

                    prevContextName = nextContextName;
                }
            }
        }
        TotalRows = dt.Rows.Count;
        Session[NcmAsaContextsSessionKey] = dt;
        return dt;
    }
}