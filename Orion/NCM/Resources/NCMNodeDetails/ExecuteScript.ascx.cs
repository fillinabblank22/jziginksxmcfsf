using System;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.NCMModule.Web.Resources;

using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_NCM_Resources_NCMNodeDetails_ExecuteScript : BaseResourceControl
{
    public override string DetachURL
    {
        get
        {
            return
                $@"{base.DetachURL}{(Request.QueryString[@"NodeID"] != null ? $@"&NodeID={Request.QueryString[@"NodeID"]}" : string.Empty)}";
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NCMWebContent.WEBDATA_VK_122; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }

    public sealed override string DisplayTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(this.Resource.Properties[@"Title"]))
                return this.Resource.Properties[@"Title"];
            return Resources.NCMWebContent.WEBDATA_VK_122;
        }
    }

    public sealed override string SubTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(this.Resource.Properties[@"SubTitle"]))
                return this.Resource.Properties[@"SubTitle"];
            return string.Empty;
        }
    }

    public override string HelpLinkFragment
    {
        get { return @"OrionNCMWebExecuteScript"; }
    }

    public override string EditURL
    {
        get { return ExtendCustomEditUrl(@"/Orion/NCM/Resources/NCMCommonResources/NCMCommonEdit.aspx"); }
    }

    protected override void OnInit(EventArgs e)
    {
        ExecuteScript resource = new ExecuteScript();
        resource.NodeProvider = GetInterfaceInstance<INodeProvider>();
        resource.ResourceSettings = this.Resource;
        resource.AllowNodeManagement = Profile.AllowNodeManagement;
        resource.ResourceContainer = Wrapper;
        Wrapper.Content.Controls.Add(resource);
        Wrapper.ShowEditButton = resource.ShowEditButton;
        base.OnInit(e);
    }
}