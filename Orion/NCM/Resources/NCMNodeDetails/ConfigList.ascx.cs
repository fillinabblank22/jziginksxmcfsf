using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.NCMModule.Web.Resources;

using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_NetPerfMon_Resources_CirrusResources_ConfigList : BaseResourceControl
{
    private ConfigList resource;
    private readonly ICommonHelper commonHelper;
    private readonly IIsWrapper isLayer;

    public Orion_NetPerfMon_Resources_CirrusResources_ConfigList()
    {
        commonHelper = ServiceLocator.Container.Resolve<ICommonHelper>();
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
    }

    public override string DetachURL => $@"{base.DetachURL}{(Request.QueryString[@"NodeID"] != null ? $@"&NodeID={Request.QueryString[@"NodeID"]}" : string.Empty)}";

    protected override string DefaultTitle => Resources.NCMWebContent.WEBDATA_VK_125;

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }

    public sealed override string DisplayTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(Resource.Properties[@"Title"]))
                return Resource.Properties[@"Title"];
            return Resources.NCMWebContent.WEBDATA_VK_125;
        }
    }

    public override string HelpLinkFragment => @"OrionNCMPHConfigList";

    public override string EditURL => ExtendCustomEditUrl(@"/Orion/NCM/Resources/NCMNodeDetails/ConfigListEdit.aspx");

    protected INodeProvider NodeProvider { get; set; }

    protected override void OnInit(EventArgs e)
    {
        bool canManageCfg;
        if (CommonHelper.IsDemoMode())
            canManageCfg = true;
        else
            canManageCfg = SecurityHelper.IsPermissionExist(SecurityHelper._canExecuteScript);

        NodeProvider = GetInterfaceInstance<INodeProvider>();

        resource = new ConfigList();
        resource.NodeProvider = NodeProvider;
        resource.ResourceSettings = Resource;
        resource.ResourceContainer = Wrapper;
        resource.EnableMgmtButtons = canManageCfg;

        lbl1.Text = Resources.NCMWebContent.WEBDATA_VM_212;
        lbl2.Text = Resources.NCMWebContent.WEBDATA_VK_154;
        lbl3.Text = Resources.NCMWebContent.WEBDATA_VK_156;
        lbl4.Text = Resources.NCMWebContent.WEBDATA_VM_211;
        lbl5.Text = Resources.NCMWebContent.WEBDATA_VK_823;

        Wrapper.Content.Controls.Add(resource);

        if (canManageCfg && resource.IsAuthorized)
        {
            setFavoriteBtn.ServerClick += new EventHandler(setFavoriteBtn_ServerClick);
            deleteCfgBtn.ServerClick += new EventHandler(deleteCfgBtn_ServerClick);
            editCfgBtn.ServerClick += new EventHandler(editCfgBtn_ServerClick);
            importCfgBtn.ServerClick += new EventHandler(importCfgBtn_ServerClick);
        }
        else
        {
            managementContainer.Visible = false;
            Wrapper.ShowEditButton = false;
        }
        base.OnInit(e);
    }

    void importCfgBtn_ServerClick(object sender, EventArgs e)
    {
        var nodeID  = commonHelper.GetNodeIdFromNodeProvider(NodeProvider);
        var url =
            $@"/Orion/NCM/Resources/Configs/ImportConfig.aspx?NodeID={nodeID}&NCMReturnURL={UrlHelper.ToSafeUrlParameter(Page.Request.Url.AbsoluteUri)}";
        HttpContext.Current.Response.Redirect(url);
    }

    void editCfgBtn_ServerClick(object sender, EventArgs e)
    {
        var idsCfg = GetSelectedConfigIds(hfSelectedTextCfgIds);

        if (idsCfg == null)
            return;

        if (idsCfg.Count == 1)
        {
            var url =
                $@"/Orion/NCM/Resources/Configs/EditConfig.aspx?ConfigID={idsCfg[0]}&NCMReturnURL={UrlHelper.ToSafeUrlParameter(Page.Request.Url.AbsoluteUri)}";
            HttpContext.Current.Response.Redirect(url);
        }
    }

    void deleteCfgBtn_ServerClick(object sender, EventArgs e)
    {
        var idsCfg = GetSelectedConfigIds(hfSelectedAllCfgIds);
        
        if (idsCfg == null)
            return;

        using (var proxy = isLayer.GetProxy())
        {
            proxy.Cirrus.ConfigArchive.DeleteConfigs(idsCfg, Page.User.Identity.Name);
        }

        Response.Redirect(Request.Url.AbsoluteUri);
    }

    void setFavoriteBtn_ServerClick(object sender, EventArgs e)
    {
        var idsCfg = GetSelectedConfigIds(hfSelectedAllCfgIds);

        if (idsCfg == null)
            return;

        using (var proxy = isLayer.GetProxy())
        {
            proxy.Cirrus.ConfigArchive.SetClearBaseline(idsCfg);
        }

        Response.Redirect(Request.Url.AbsoluteUri);
    }

    protected string ContentClientId => resource.ClientID;

    private List<Guid> GetSelectedConfigIds(HiddenField hf)
    {
        if (string.IsNullOrEmpty(hf.Value))
            return null;

        var ids = new List<Guid>();
        var idsStr = hf.Value.Split(new char[] { ',' });

        foreach (var id in idsStr)
        {
            ids.Add(Guid.Parse(id));
        }
        return ids;
    }
}
