﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeContexts.ascx.cs" Inherits="Orion_NCM_Resources_NCMNodeDetails_NodeContexts" %>

<orion:ResourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Repeater runat="server" ID="repeater">
                    <HeaderTemplate>
                        <table border="0" cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td><%# RenderLinkIfNeeded(Eval(@"ContextName"), Eval(@"Caption"), Eval(@"NodeID")) %></td>
                            <td><%# RenderIpAddress(Eval(@"IpAddress"))%></td>
                            <td><%# RenderAddNodeButtonIfNeeded(Eval(@"IpAddress"), Eval(@"NodeID"))%></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <table cellpadding="0" cellspacing="0" width="100%" style="padding-top:10px;">
                    <tr>
                        <td nowrap="nowrap">
                            <asp:LinkButton ID="btnFirstPage" runat="server" OnClick="btnFirstPage_Click">
                                <img id="imgFirstPage" runat="server" src="/Orion/images/Arrows/button_white_paging_first.gif" />
                            </asp:LinkButton>|
                            <asp:LinkButton ID="btnPrevPage" runat="server" OnClick="btnPrevPage_Click">
                                <img id="imgPrevPage" runat="server" src="/Orion/images/Arrows/button_paging_previous.gif" />
                            </asp:LinkButton>|
                            <span>
                                <%= string.Format(Resources.NCMWebContent.WEBDATA_VK_1056, SelectedPage, TotalPages) %>
                            </span>|
                            <asp:LinkButton ID="btnNextPage" runat="server" OnClick="btnNextPage_Click">
                                <img id="imgNextPage" runat="server" src="/Orion/images/Arrows/button_paging_next.gif" />
                            </asp:LinkButton>|
                            <asp:LinkButton ID="btnLastPage" runat="server" OnClick="btnLastPage_Click">
                                <img id="imgLastPage" runat="server" src="/Orion/images/Arrows/button_white_paging_last.gif" />
                            </asp:LinkButton>
                        </td>
                        <td align="right" nowrap="nowrap" style="padding-left:10px;">
                            <%= TotalRows == 0 ? Resources.NCMWebContent.WEBDATA_VK_1059 : string.Format(Resources.NCMWebContent.WEBDATA_VK_1057, StartRowNumber + 1, TotalRows - (StartRowNumber + PageSize) > 0 ? StartRowNumber + PageSize : TotalRows, TotalRows, @"<b>", @"</b>") %>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </Content>
</orion:ResourceWrapper>