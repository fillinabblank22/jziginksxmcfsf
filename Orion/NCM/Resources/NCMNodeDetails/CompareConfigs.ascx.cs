using System;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.NCMModule.Web.Resources;

using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_NetPerfMon_Resources_CirrusResources_CompareConfigs : BaseResourceControl
{

	protected override string DefaultTitle
	{
        get { return Resources.NCMWebContent.WEBDATA_VK_126; }
	}

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }

    public sealed override string DisplayTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(this.Resource.Properties[@"Title"]))
                return this.Resource.Properties[@"Title"];
            return Resources.NCMWebContent.WEBDATA_VK_126;
        }
    }

    public sealed override string SubTitle
    {
        get
        {
            return !string.IsNullOrEmpty(this.Resource.Properties[@"SubTitle"]) ? this.Resource.Properties[@"SubTitle"] : string.Empty;
        }
    }

    public override string HelpLinkFragment
    {
        get { return @"OrionNCMWebCompareConfigurations"; }
    }

    public override string EditURL
    {
        get { return ExtendCustomEditUrl(@"/Orion/NCM/Resources/NCMNodeDetails/CompareConfigsEdit.aspx"); }
    }

    protected override void OnInit(EventArgs e)
	{
	    var resource = new CompareConfigs
	    {
	        NodeProvider = GetInterfaceInstance<INodeProvider>(),
	        ResourceSettings = this.Resource,
	        ResourceContainer = Wrapper
	    };
	    Wrapper.Content.Controls.Add(resource);
        Wrapper.ShowEditButton = resource.ShowEditButton;
        base.OnInit(e);
	}
}
