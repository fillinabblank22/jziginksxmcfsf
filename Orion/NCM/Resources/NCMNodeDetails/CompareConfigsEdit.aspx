<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMResourceEditMasterPage.master" AutoEventWireup="true" CodeFile="CompareConfigsEdit.aspx.cs" Inherits="Orion_NCM_Resources_NCMNodeDetails_CompareConfigsEdit" Title="Untitled Page" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmMainContent" Runat="Server">
<table>
        <tr>
            <td class="PageHeader"><%=ResourceHeader %>
            </td>
        </tr>
        <tr>
            <td style="padding-top:10px;">
                <b><%=Resources.NCMWebContent.WEBDATA_VK_128 %></b><br />
                <asp:TextBox runat="server" ID="ResourceTitle" Width="250"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="padding-top:5px">
                <b><%=Resources.NCMWebContent.WEBDATA_VK_129 %></b><br />
                <igtxt:WebNumericEdit ID="ConfigsNumeric" runat="server" HorizontalAlign="Center" Width="60" MaxValue="999" MinValue="1" MaxLength="3">
                    <SpinButtons Display="OnRight" />
                </igtxt:WebNumericEdit>
            </td>
        </tr>
        <tr>
            <td style="padding-top:20px;">
                <orion:LocalizableButton ID="btnSubmit" runat="server" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitClick"></orion:LocalizableButton>
            </td>
        </tr>
    </table>
</asp:Content>

