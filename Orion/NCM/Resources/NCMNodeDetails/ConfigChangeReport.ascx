<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConfigChangeReport.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NCMResources_ConfigChangeReport"%>

<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources" Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<link rel="stylesheet" type="text/css" href="/orion/ncm/styles/NCMResources.css"  />

<orion:resourceWrapper runat="server" ID="Wrapper">        
    <Content>
        <style type="text/css">
            .ResourceWrapper {
                overflow-x: visible;
            }

            .calendar_container {
                display: inline-block; 
                vertical-align: middle;
                padding: 5px 5px 5px 0px;
            }

            .ajax__calendar_days table thead tr td {
                border: 0px;
                background-color: white;
                text-transform: none;
            }

            .ajax__calendar_container table tbody tr td {
                border: 0px;
            }
        </style>
        <asp:Panel ID="ContentContainer" runat="server">
            <asp:UpdatePanel ID="uPanel" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding-bottom:5px;"><%=Resources.NCMWebContent.WEBDATA_VK_165 %></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="calendar_container">
                                    <ncm:Calendar ID="dateTimeFrom" AlternativeDrawingMode="true" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_157 %>" runat="server" />
                                </div>
                                <div class="calendar_container">
                                    <ncm:Calendar ID="dateTimeTo" AlternativeDrawingMode="true" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_158 %>" runat="server" />
                                </div>
                                <orion:LocalizableButton ID="btnViewChanges" runat="server" DisplayType="Secondary" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_164 %>" OnClick="ShowChangesClick"></orion:LocalizableButton>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top:5px;">
                                <asp:CheckBox ID="chkViewChangesOn" AutoPostBack="true" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_166 %>" OnCheckedChanged="chkViewChangesOn_OnCheckedChanged" />
                                <ncm:ConfigTypes ID="ddlConfigType" runat="server" Width="150px"></ncm:ConfigTypes>
                                <%=Resources.NCMWebContent.WEBDATA_VK_167 %>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <div id="ErrorContainer" runat="server"/>
    </Content> 
</orion:resourceWrapper>