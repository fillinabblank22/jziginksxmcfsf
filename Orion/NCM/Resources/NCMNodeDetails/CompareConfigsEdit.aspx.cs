using System;
using System.Web.UI;
using Infragistics.WebUI.WebDataInput;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.Resources;
using SolarWinds.NCMModule.Web.Resources.Wrappers;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NCM_Resources_NCMNodeDetails_CompareConfigsEdit : Page
{
    private readonly IEditResourcePageService editResourceService;

    public Orion_NCM_Resources_NCMNodeDetails_CompareConfigsEdit()
    {
        editResourceService = ServiceLocator.Container.Resolve<IEditResourcePageService>();
    }

    protected ResourceInfo Resource { get; private set; }
    protected string ResourceHeader => editResourceService.GetHeader(Resource);

    private void SetupControlFromRequest()
    {
        ConfigsNumeric.Text = !string.IsNullOrEmpty(Resource.Properties[@"ConfigsCount"]) ? Resource.Properties[@"ConfigsCount"] : @"10";

        ConfigsNumeric.DataMode = NumericDataMode.Uint;
    }

    protected override void OnInit(EventArgs e)
    {
        Resource = editResourceService.Init(new PageWrap(this), ResourceTitle, null);

        SetupControlFromRequest();

        base.OnInit(e);
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        editResourceService.SaveChanges(Resource, ResourceTitle, null);

        Resource.Properties[@"ConfigsCount"] = ConfigsNumeric.Text;

        Response.Redirect(BaseResourceControl.GetEditPageReturnUrl(Request.QueryString, Resource.View.ViewID.ToString()));
    }
}
