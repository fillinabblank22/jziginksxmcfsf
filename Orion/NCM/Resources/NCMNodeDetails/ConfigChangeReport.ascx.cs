using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.NCM.Common.Dal.ConfigTypes;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_NetPerfMon_Resources_NCMResources_ConfigChangeReport : BaseResourceControl
{
    private readonly IConfigTypesProvider configTypesProvider;
    private readonly ICommonHelper commonHelper;
    private readonly ILogger logger;

    public Orion_NetPerfMon_Resources_NCMResources_ConfigChangeReport()
    {
        configTypesProvider = ServiceLocator.Container.Resolve<IConfigTypesProvider>();
        commonHelper = ServiceLocator.Container.Resolve<ICommonHelper>();
        logger = ServiceLocator.Container.Resolve<Func<Type, ILogger>>()(GetType());
    }

    public override string DetachURL => $@"{base.DetachURL}{(Request.QueryString[@"NodeID"] != null ? $@"&NodeID={Request.QueryString[@"NodeID"]}" : string.Empty)}";

    protected override string DefaultTitle => Resources.NCMWebContent.WEBDATA_VK_168;

    public override string HelpLinkFragment => @"OrionNCMRecentConfigurationChanges";

    public override IEnumerable<Type> RequiredInterfaces => new[] { typeof(INodeProvider) };

    public sealed override string DisplayTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(this.Resource.Properties[@"Title"]))
                return this.Resource.Properties[@"Title"];
            return Resources.NCMWebContent.WEBDATA_VK_168;
        }
    }

    public sealed override string SubTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(this.Resource.Properties[@"SubTitle"]))
                return this.Resource.Properties[@"SubTitle"];
            return string.Empty;
        }
    }

    public override string EditURL => ExtendCustomEditUrl(@"/Orion/NCM/Resources/NCMCommonResources/NCMCommonEdit.aspx");

    protected INodeProvider NodeProvider { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Response.Cache.SetNoStore();

        NodeProvider = GetInterfaceInstance<INodeProvider>();

        SWISValidator validator = new SWISValidator();
        validator.NodeProvider = NodeProvider;
        validator.ContentContainer = ContentContainer;
        validator.ResourceContainer = Wrapper;
        ErrorContainer.Controls.Add(validator);

        if (validator.IsValid)
        {
            if (!Page.IsPostBack)
            {
                dateTimeFrom.TimeSpan = string.IsNullOrEmpty(Resource.Properties[@"DateFrom"])
                    ? DateTime.Now.AddDays(-1)
                    : Convert.ToDateTime(Resource.Properties[@"DateFrom"], CultureInfo.InvariantCulture);
                dateTimeTo.TimeSpan = DateTime.Now;

                chkViewChangesOn.Checked = !string.IsNullOrEmpty(Resource.Properties[@"ViewChangesOn"])
                    ? Convert.ToBoolean(Resource.Properties[@"ViewChangesOn"])
                    : false;

                InitializeConfigTypes();
            }

            Wrapper.Visible = true;
        }
        else
        {
            Wrapper.ShowEditButton = false;
        }
    }

    private void InitializeConfigTypes()
    {
        var nodeId = commonHelper.GetNodeIdFromNodeProvider(NodeProvider);
        var configTypes = nodeId.HasValue
            ? configTypesProvider.GetCommon(new[] { nodeId.Value }).ToArray()
            : configTypesProvider.GetAll().ToArray();

        ddlConfigType.Items.Clear();
        foreach (var confType in configTypes)
        {
            ddlConfigType.Items.Add(new ListItem(confType.Name, confType.Name));
        }

        if (configTypes.Any())
        {
            string preselectedConfigType = Request.QueryString[@"ConfigType"];
            var selectedConfigType = !string.IsNullOrEmpty(preselectedConfigType) ? preselectedConfigType : configTypes.First().Name;
            ddlConfigType.SelectedValue = selectedConfigType;
        }

        ddlConfigType.Enabled = chkViewChangesOn.Checked;
    }

    protected void chkViewChangesOn_OnCheckedChanged(object sender, EventArgs e)
    {
        ddlConfigType.Enabled = chkViewChangesOn.Checked;
    }

    protected void ShowChangesClick(object sender, EventArgs e)
    {
        try
        {
            if (!dateTimeFrom.ValidateDate() || !dateTimeTo.ValidateDate()) return;

            DateTime startTime = dateTimeFrom.TimeSpan;
            DateTime endTime = dateTimeTo.TimeSpan;

            if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {
                Resource.Properties[@"DateFrom"] = startTime.ToString(CultureInfo.InvariantCulture);
                Resource.Properties[@"ViewChangesOn"] = chkViewChangesOn.Checked.ToString();
                Resource.Properties[@"ConfigType"] = ddlConfigType.SelectedValue;
                SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
            }

            var nodeId = commonHelper.GetNodeIdFromNodeProvider(NodeProvider);
            Page.Response.Redirect(
                $@"/Orion/NCM/ConfigChangeReport.aspx?StartTime={startTime.ToString(CultureInfo.InvariantCulture)}&EndTime={endTime.ToString(CultureInfo.InvariantCulture)}&ViewChangesOn={chkViewChangesOn.Checked.ToString()}&ConfigType={ddlConfigType.SelectedValue}&NodeID={nodeId}&ReturnUrl={Page.Server.UrlEncode(Page.Request.Url.PathAndQuery)}&ClientID={Resource.ID}");
        }
        catch (Exception ex)
        {
            logger.Warn($"Error in showing changes.", ex);
        }
    }
}
