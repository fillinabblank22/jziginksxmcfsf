﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterfaceConfigSnippet.ascx.cs" Inherits="Orion_NCM_Resources_NPMInterfaceDetails_InterfaceConfigSnippet" %>

<orion:Include ID="Include" runat="server" File="OrionCore.js" />

<style>
    a.interface-config-tooltip img {
        display: block;
        margin-left: auto;
    }

    a.interface-config-tooltip div {
        border: solid 1px #767676;
        background-color: white;
        color: black;
        z-index: 10;
        padding: 5px;
        margin: -25px 30px 0 0;
        right: 0;
        position: absolute;
        display: none;
        box-shadow: 0 1px 5px 0 #b9b9b9;
    }
    
    a.interface-config-tooltip:hover div {
        display: block;
    }

    div.interface-config-content {
        font-family: Courier, monospace;
        margin-top: -1em;
    }
</style>

<script type="text/javascript">
    $(document).ready(function() {
        var interfaceSnippetResourceId = $('#<%=InterfaceSnippetResource.ClientID%>');
        var nodeId = <%=NodeID%>;
        var interfaceId = <%=InterfaceID%>;
        var configType = '<%=ConfigType%>';
        var resourceId = <%=Resource.ID%>;

        var init = function(nodeId, interfaceId, configType) {
            hideMask(false);
            
            var url = String.format("/api2/ncm/config/interface/{0}/{1}", nodeId, interfaceId);
            if (configType) {
                url +=  String.format("/{0}", configType);
            }

            $.ajax({
                type: "GET",
                url: url,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data, status, xhr) {
                    hideMask(true);
                    onSuccess(data);
                },
                error: function(xhr, status, errorThrown) {
                    hideMask(true);
                    onError(xhr);
                }
            });
        };

        var hideResource = function(resourceId) {
            $("div.ResourceWrapper[resourceid='" + resourceId + "']").hide();
        };
        
        var hideMask = function(hide) {
            if (hide) {
                interfaceSnippetResourceId.find('#LoadingMask').hide();
            } else {
                interfaceSnippetResourceId.find('#LoadingMask').show();
            }
        };
        
        var onSuccess = function(data) {
            if (data) {
                if(data.configSnippet.length > 0) {
                    var content = interfaceSnippetResourceId.find('#Content');
                    content.html(buildMarkup(data));
                } else {
                    hideResource(resourceId);
                }
            } else {
                showDownloadMessage();
            }
        };

        var onError = function(error) {
            showMessage("Unable to get data from the server.", "fail");
        };

        var buildMarkup = function(data) {
            var markup =
                String.format("<a class='interface-config-tooltip'>\
                                   <img src='/Orion/Discovery/images/InfoIcon.png' /> \
                                   <div>{1}</div> \
                               </a> \
                               <div class='interface-config-content'>{0}</div>",
                               getConfigSnippetMarkup(data.configSnippet),
                               getTooltipMarkup(data.configType, data.downloadTime));

            return markup;
        };

        var getTooltipMarkup = function(configType, downloadTime) {
            var markup = "<table cellspacing='0' cellpadding='0' border='0'>";
            
            markup += String.format("<tr> \
                                        <td nowrap='nowrap' style='padding-right: 5px;'><b>Config type:</b></td> \
                                        <td nowrap='nowrap'>{0}</td> \
                                     </tr> \
                                     <tr> \
                                        <td nowrap='nowrap' style='padding-right: 5px;'><b>Download time:</b></td> \
                                        <td nowrap='nowrap'>{1}</td> \
                                     </tr>",
                                    configType, 
                                    getFormattedDate(downloadTime));

            markup += "</table>";
            return markup;
        };

        var getConfigSnippetMarkup = function(configSnippet) {
            return '<pre>' +configSnippet +'</pre>';
        };
        
        var showDownloadMessage = function() {
            var downloadLink = "<a href='/Orion/NCM/ConfigurationManagement.aspx' target='__blank'>download</a>";
            if (configType.toLowerCase() == 'running') {
                showMessage(String.format("Running config file is missing. Please {0} running config file.", downloadLink), "info");
            } else if(configType.toLowerCase() == 'startup') {
                showMessage(String.format("Startup config file is missing. Please {0} startup config file.", downloadLink), "info");
            } else {
                showMessage(String.format("Config file is missing. Please {0} config file.", downloadLink), "info");
            }
        };

        var showMessage = function(msg, type) {
            var content = interfaceSnippetResourceId.find('#Content');
            content.html(String.format("<div class='sw-suggestion sw-suggestion-{1}'> \
                                            <div class='sw-suggestion-icon'></div> \
                                                {0} \
                                        </div>", msg, type));
        };

        var getFormattedDate = function(date) {
            return new Date(new Date(date).getTime() + new Date().getTimezoneOffset() * 60000).toLocaleString();
        };
        
        if (nodeId > 0 && interfaceId > 0) {            
            init(nodeId, interfaceId, configType);
        } else {
            hideMask(true);
            showMessage("Unable to get data from the server.", "fail");
        }
    });
</script>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div id="InterfaceSnippetResource" runat="server">
            <span id="LoadingMask" style="font-weight:bold;color:#777;text-align:center;display:block;margin-left:auto;margin-right:auto;">
                <img  src="/Orion/images/AJAX-Loader.gif" style="margin-right:5px;"/>Loading...
            </span>
            <div id="Content"></div>
        </div>
    </Content>
</orion:resourceWrapper>
