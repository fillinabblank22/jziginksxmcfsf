﻿using System;
using System.Linq;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System.Collections.Generic;
using SolarWinds.InformationService.Linq.Plugins.NCM.NCM;
using SolarWinds.NCM.Common.Dal;
using SolarWinds.NCM.Common.Dal.ConfigTypes;
using SolarWinds.NCM.Contracts.GlobalConfigTypes;
using SolarWinds.NCMModule.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_NCM_Resources_NPMInterfaceDetails_InterfaceConfigSnippet : BaseResourceControl
{
    private readonly IConfigTypesProvider configTypesProvider;
    private readonly ISwisRepositoryFactory swisRepositoryFactory;

    public Orion_NCM_Resources_NPMInterfaceDetails_InterfaceConfigSnippet()
    {
        configTypesProvider = ServiceLocator.Container.Resolve<IConfigTypesProvider>();
        swisRepositoryFactory = ServiceLocator.Container.Resolve<ISwisRepositoryFactory>();
    }

    protected override string DefaultTitle => Resources.NCMWebContent.InterfaceConfig_ResourceTitle;

    protected int NodeID { get; private set; }

    protected string ConfigType { get; private set; }

    protected int InterfaceID { get; private set; }

    public override string HelpLinkFragment => @"OrionNCMInterfaceConfigSnippets";

    public override string EditControlLocation => @"/Orion/NCM/Controls/EditResourceControls/EditInterfaceConfigSnippet.ascx";

    public override IEnumerable<Type> RequiredInterfaces => new[] { typeof(IInterfaceProvider) };

    protected override void OnInit(EventArgs e)
    {
        var interfaceProvider = GetInterfaceInstance<IInterfaceProvider>();
        if (interfaceProvider != null && interfaceProvider.Interface != null)
        {
            NodeID = interfaceProvider.Interface.NodeID;
            InterfaceID = interfaceProvider.Interface.InterfaceID;
        }
        ConfigType = GetSelectedConfigType(Resource.Properties[@"ConfigType"], NodeID);

        Wrapper.Visible = SecurityHelper.IsValidNcmAccountRole && IsMonitoredByNcm(NodeID);

        base.OnInit(e);
    }

    private bool IsMonitoredByNcm(int coreNodeId)
    {
        using (var swisRepo = swisRepositoryFactory.CreateSystemRepository())
        {
            return swisRepo.Get<Nodes>()
                .Any(n => n.CoreNodeID == coreNodeId);
        }
    }

    private string GetSelectedConfigType(string configType, int coreNodeId)
    {
        if (!string.IsNullOrWhiteSpace(configType) && configTypesProvider.IsValid(configType, coreNodeId))
        {
            return configType;
        }

        return GlobalConfigTypes.DefaultConfigType;
    }
}