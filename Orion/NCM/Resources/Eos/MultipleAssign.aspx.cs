﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources.Eos;
using SolarWinds.Orion.Common;

public partial class Orion_NCM_Resources_Eos_MultipleAssign : System.Web.UI.Page
{
    private readonly IIsWrapper _isLayer;

    public Orion_NCM_Resources_Eos_MultipleAssign()
    {
        _isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
    }

    #region Properties

    private List<NodeItem> SelectedEOSNodes
    {
        get
        {
            if (Session[NodeItem.cSessionKey] != null)
                return (List<NodeItem>)Session[NodeItem.cSessionKey];
            Response.Redirect("/Orion/NCM/Resources/Eos/EosMatching.aspx");
            return null;
        }
        set
        {
            Session[NodeItem.cSessionKey] = value;
        }
    }

    protected bool SameModel
    {
        get
        {
            var result = false;
            if (ViewState["SameModel"] != null && bool.TryParse(ViewState["SameModel"].ToString(), out result))
            {
                return result;
            }

            if (SelectedEOSNodes.Count > 1)
            {
                var models = SelectedEOSNodes.Select(item => item.Model).Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();
                if (models.Count == 1 && !string.IsNullOrEmpty(models[0]))
                {
                    result = true;
                }
            }
            else
            {
                result = true;
            }
            ViewState["SameModel"] = result;
            return result;
        }
    }

    protected int RowCount
    {
        get
        {
            int result;
            if (ViewState["RowCount"] != null && int.TryParse(ViewState["RowCount"].ToString(), out result))
            {
                return result;
            }

            using (var proxy = OrionConfiguration.IsDemoServer
                ? _isLayer.GetProxyWithoutImpersonation()
                : _isLayer.GetProxy())
            {
                var rowCount = proxy.Cirrus.Nodes.GetPageableEosRowCount(SelectedEOSNodes.Select(e => e.NodeId).ToArray());
                ViewState["RowCount"] = rowCount;
                return rowCount;
            }
        }
    }

    protected string SelectedNodesTitle
    {
        get
        {
            if (SelectedEOSNodes.Count == 1)
            {
                return Resources.NCMWebContent.WEBDATA_IC_230;
            }

            return string.Format(Resources.NCMWebContent.WEBDATA_IC_205, SelectedEOSNodes.Count);
        }
    }

    private int StartRowNumber
    {
        get { return Convert.ToInt32(ViewState["StartRowNumber"]); }
        set { ViewState["StartRowNumber"] = value; }
    }

    #endregion

    #region Overrides

    protected void Page_Load(object sender, EventArgs e)
    {
        if (OrionConfiguration.IsDemoServer)
        {
            btnAssign.OnClientClick =
                $@"demoAction('{DemoModeConstants.NCM_EOS_ASSIGN_EOS_DATES}', this); return false;";
        }
        else
        {
            var isAdminOrEngineer = SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ENGINEER) || SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR);
            btnAssign.Enabled = isAdminOrEngineer;
        }

        if (SelectedEOSNodes.Count == 1)
        {
            Page.Title = string.Format(Resources.NCMWebContent.WEBDATA_IC_201, SelectedEOSNodes[0].NodeCaption);
        }
        else
        {
            Page.Title = Resources.NCMWebContent.WEBDATA_IC_204;
        }

        NodesListView.DataKeyNames = new string[] { @"GroupName" };

        if (!Page.IsPostBack)
        {
            EnablePagingToolbar();
            EosGridDataBind();
            NodesGridDataBind(ddViewby.SelectedValue);
        }
    }

    #endregion

    #region Events

    protected void OnLayoutCreated(object sender, EventArgs e)
    {
        EosGrid.FindControl(@"reliabilityHeader").Visible = SameModel;
    }

    protected void EosGrid_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        var row = (HtmlTableRow)e.Item.FindControl(@"EosTableRow");        

        if (SameModel)
        {
            var eosUI = (EosUI)e.Item.DataItem;
            row.Style.Value = string.IsNullOrWhiteSpace(row.Style.Value) ? eosUI.CertaintyStyle : $@"{row.Style.Value};{eosUI.CertaintyStyle}";
        }
        else
        {
            if (Convert.ToBoolean(e.Item.DataItemIndex % 2))
            {
                row.Style.Add(@"background-color", @"#e4f1f8");
            }
        }
    }

    protected void ddViewby_SelectedIndexChanged(object sender, EventArgs e)
    {
        NodesGridDataBind(ddViewby.SelectedValue);
    }

    protected void NodesListView_ItemDeleting(object sender, ListViewDeleteEventArgs e)
    {
        foreach (var val in e.Keys.Values)
        {
            switch (ddViewby.SelectedValue.ToUpperInvariant())
            {
                case "MACHINETYPE":
                    SelectedEOSNodes.RemoveAll(x => x.MachineType == val.ToString()); // Remove Nodes
                    break;
                case "OSVERSION":
                    SelectedEOSNodes.RemoveAll(x => x.OSVersion == val.ToString());
                    break;
                case "PARTNUMBER":
                    SelectedEOSNodes.RemoveAll(x => x.Model == val.ToString());
                    break;
                default:
                    SelectedEOSNodes.RemoveAll(x => x.MachineType == val.ToString());
                    break;
            }
        }

        NodesGridDataBind(ddViewby.SelectedValue);
    }

    protected void rb_OnCheckedChanged(object sender, EventArgs e)
    {
        rbAssignManually.Checked = false;

        foreach (var item in EosGrid.Items)
        {
            var rb = (RadioButton)item.FindControl(@"rb");
            rb.Checked = false;
        }
        ((RadioButton)sender).Checked = true;
    }

    protected void rbAssignManually_OnCheckedChanged(object sender, EventArgs e)
    {
        foreach (var item in EosGrid.Items)
        {
            var rb = (RadioButton)item.FindControl(@"rb");
            rb.Checked = false;
        }
    }

    protected void btnNext25_Click(object sender, EventArgs e)
    {
        StartRowNumber += 25;
        EnablePagingToolbar();
        EosGridDataBind();
    }

    protected void btnPrevious25_Click(object sender, EventArgs e)
    {
        StartRowNumber -= 25;
        EnablePagingToolbar();
        EosGridDataBind();
    }

    protected void btnAssign_Click(object sender, EventArgs e)
    {
        using (var proxy = _isLayer.GetProxy())
        {
            int? eosVersion = null;
            Guid? eosEntryID = null;
            DateTime? endOfSupport = null;
            DateTime? endOfSales = null;
            DateTime? endOfSoftware = null;
            string eosLink = null;
            string replacementPartNumber;
            var found = false;

            var nodeGuidList = new List<Guid>();
            foreach (var item in SelectedEOSNodes)
            {
                nodeGuidList.Add(item.NodeId);
            }

            if (rbAssignManually.Checked)
            {
                if (EndofSupport.ValidateDate())
                {
                    endOfSupport = EndofSupport.TimeSpan;
                }

                if (EndofSales.ValidateDate())
                {
                    endOfSales = EndofSales.TimeSpan;
                }

                if (EndOfSoftware.ValidateDate())
                {
                    endOfSoftware = EndOfSoftware.TimeSpan;
                }

                if (!string.IsNullOrEmpty(ExternalLink.Text))
                {
                    eosLink = ExternalLink.Text;
                }

                if ((endOfSupport.HasValue || endOfSales.HasValue || endOfSoftware.HasValue) && nodeGuidList.Count > 0)
                {
                    proxy.Cirrus.Nodes.AssignEOSEntry(nodeGuidList.ToArray(), endOfSupport, endOfSales, endOfSoftware, null, eEoSType.Manual, eosVersion, eosLink, Notes.Text, null);
                    Session.Remove(NodeItem.cSessionKey); //free memory
                    Page.Response.Redirect("EosMatching.aspx");
                }
            }
            else if (nodeGuidList.Count > 0)
            {
                foreach (var item in EosGrid.Items)
                {
                    var rb = (RadioButton)item.FindControl(@"rb");
                    if (rb.Checked)
                    {
                        var EndOfSupport = rb.Attributes[@"EndOfSupport"];
                        if (!string.IsNullOrEmpty(EndOfSupport)) endOfSupport = Convert.ToDateTime(EndOfSupport);

                        var EndOfSales = rb.Attributes[@"EndOfSales"];
                        if (!string.IsNullOrEmpty(EndOfSales)) endOfSales = Convert.ToDateTime(EndOfSales);

                        var EndOfSoftware = rb.Attributes[@"EndOfSoftware"];
                        if (!string.IsNullOrEmpty(EndOfSoftware)) endOfSoftware = Convert.ToDateTime(EndOfSoftware);

                        var EosEntryID = rb.Attributes[@"EosEntryID"];
                        if (!string.IsNullOrEmpty(EosEntryID)) eosEntryID = Guid.Parse(EosEntryID);

                        var EosVersion = rb.Attributes[@"EosVersion"];
                        if (!string.IsNullOrEmpty(EosVersion)) eosVersion = Convert.ToInt32(EosVersion);

                        eosLink = rb.Attributes[@"EosLink"];
                        replacementPartNumber = rb.Attributes[@"ReplacementPartNumber"];

                        var type = (eEoSType)Convert.ToInt32(rb.Attributes[@"EosType"]);
                        if (type == eEoSType.Awaiting) type = eEoSType.User;

                        proxy.Cirrus.Nodes.AssignEOSEntry(nodeGuidList.ToArray(), endOfSupport, endOfSales, endOfSoftware, eosEntryID, type, eosVersion, eosLink, Notes.Text, replacementPartNumber);
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    lblError.Visible = true;
                    return;
                }
                Session.Remove(NodeItem.cSessionKey); //free memory
                Page.Response.Redirect("EosMatching.aspx");
            }
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Session.Remove(NodeItem.cSessionKey); //free memory
        Page.Response.Redirect("EosMatching.aspx");
    }

    #endregion

    #region Private Functions

    private void DisableDeleteButtonifNeded()
    {
        if (NodesListView.Items.Count == 1)
        {
            foreach (var item in NodesListView.Items)
            {
                (item.FindControl(@"btnRemove") as ImageButton).Visible = false;
            }
        }
    }

    private void ShowNotificationBar(DataTable dt)
    {
        var visible = dt.AsEnumerable().Any(row =>
            row.Field<int>("EosType").Equals((int)eEoSType.User) ||
            row.Field<int>("EosType").Equals((int)eEoSType.Manual) ||
            row.Field<int>("EosType").Equals((int)eEoSType.Auto)
            );
        notification.Visible = visible;
    }

    private void EosGridDataBind()
    {
        using (var proxy = OrionConfiguration.IsDemoServer
            ? _isLayer.GetProxyWithoutImpersonation()
            : _isLayer.GetProxy())
        {
            var dt = proxy.Cirrus.Nodes.GetPageableEosDataTable(SelectedEOSNodes.Select(e => e.NodeId).ToArray(), StartRowNumber, 25);
            var eosUIList = CreateEosUIList(dt);
            EosGrid.DataSource = eosUIList;
            EosGrid.DataBind();

            ShowNotificationBar(dt);
        }
    }

    private void NodesGridDataBind(string GroupBy)
    {
        switch (GroupBy.ToUpperInvariant())
        {
            case "MACHINETYPE":
                var grpGroupByMachineType = SelectedEOSNodes.GroupBy(NodeGroup =>
                    NodeGroup.MachineType).Select(nItem => new { GroupName = nItem.Key, NodesCount = nItem.Count() }).OrderBy(P => P.GroupName);
                NodesListView.DataSource = grpGroupByMachineType;
                break;
            case "OSVERSION":
                var grpGroupByOSVersion = SelectedEOSNodes.GroupBy(NodeGroup =>
                    NodeGroup.OSVersion).Select(nItem => new { GroupName = nItem.Key, NodesCount = nItem.Count() }).OrderBy(P => P.GroupName);
                NodesListView.DataSource = grpGroupByOSVersion;
                break;
            case "PARTNUMBER":
                var grpGroupByPartNumber = SelectedEOSNodes.GroupBy(NodeGroup =>
                    NodeGroup.Model).Select(nItem => new { GroupName = nItem.Key, NodesCount = nItem.Count() }).OrderBy(P => P.GroupName);                
                NodesListView.DataSource = grpGroupByPartNumber;
                break;
            default:
                var grpGroupByMachineTypeDefault = SelectedEOSNodes.GroupBy(NodeGroup =>
                   NodeGroup.MachineType).Select(nItem => new { GroupName = nItem.Key, NodesCount = nItem.Count() }).OrderBy(P => P.GroupName);
                NodesListView.DataSource = grpGroupByMachineTypeDefault;
                break;
        }

        NodesListView.DataBind();
        DisableDeleteButtonifNeded();
    }

    private List<EosUI> CreateEosUIList(DataTable dt)
    {
        var eosUIList = new List<EosUI>();
        foreach (DataRow row in dt.Rows)
        {
            var typeObj = row["EosType"];
            var type = 0;
            if (typeObj != DBNull.Value)
            {
                type = Convert.ToInt32(typeObj);
            }

            var eosUI = new EosUI()
            {
                EOSType = type,                
                EndOfSupport = row["EndOfSupport"] == DBNull.Value ? string.Empty : Convert.ToDateTime(row["EndOfSupport"]).ToShortDateString(),
                EndOfSales = row["EndOfSales"] == DBNull.Value ? string.Empty : Convert.ToDateTime(row["EndOfSales"]).ToShortDateString(),
                EndOfSoftware = row["EndOfSoftware"] == DBNull.Value ? string.Empty : Convert.ToDateTime(row["EndOfSoftware"]).ToShortDateString(),
                EosModel = Convert.ToString(row["EosModel"]),
                EosUILink = string.IsNullOrEmpty(Convert.ToString(row["EosLink"])) ? string.Empty : $@"<a style='text-decoration:underline;color:blue' href='{Convert.ToString(row["EosLink"])}' target='_blank'>{Resources.NCMWebContent.WEBDATA_IC_221}</a>",
                EosLink = Convert.ToString(row["EosLink"]),
                EosEntryID = row["EosEntryID"] == DBNull.Value ? string.Empty : Convert.ToString(row["EosEntryID"]),
                EosVersion = row["EosVersion"] == DBNull.Value ? string.Empty : Convert.ToString(row["EosVersion"]),
                Rank = row["Rank"] == DBNull.Value ? 0 : Convert.ToInt32(row["Rank"]),
                PartNumber = row["PartNumber"] == DBNull.Value ? string.Empty : Convert.ToString(row["PartNumber"]),
                ReplacementPartNumber = row["ReplacementPartNumber"] == DBNull.Value ? string.Empty : Convert.ToString(row["ReplacementPartNumber"]),
                CertaintyKey = row["CertaintyKey"] == DBNull.Value ? string.Empty : Convert.ToString(row["CertaintyKey"]),
                Certainty = row["Certainty"] == DBNull.Value ? string.Empty : Convert.ToString(row["Certainty"]),
                CertaintyStyle = row["CertaintyStyle"] == DBNull.Value ? string.Empty : Convert.ToString(row["CertaintyStyle"]),
            };
            eosUIList.Add(eosUI);
        }
        return eosUIList;
    }

    private void EnablePagingToolbar()
    {
        var bEnabled = true;

        bEnabled = !(StartRowNumber == 0);
        btnPrevious25.Enabled = bEnabled;
        btnPrevious25.CssClass = bEnabled ? @"enabled" : @"disabled";

        bEnabled = ((RowCount - StartRowNumber) > 25);
        btnNext25.Enabled = bEnabled;
        btnNext25.CssClass = bEnabled ? @"enabled" : @"disabled";

        if (RowCount > 0)
        {
            pagingMsg.Text = string.Format(Resources.NCMWebContent.WEBDATA_IC_222, StartRowNumber + 1, (RowCount - (StartRowNumber + 25)) > 0 ? StartRowNumber + 25 : RowCount, RowCount);
        }
        else
        {
            pagingMsg.Text = string.Format(Resources.NCMWebContent.WEBDATA_IC_222, 0, 0, 0);
        }
    }

    #endregion

    protected class EosUI
    {
        public string CertaintyKey { get; set; }
        public string Certainty { get; set; }
        public string CertaintyStyle { get; set; }
        public int EOSType { get; set; }        
        public string EndOfSupport { get; set; }
        public string EndOfSales { get; set; }
        public string EndOfSoftware { get; set; }
        public string EosModel { get; set; }
        public string EosLink { get; set; }
        public string EosUILink { get; set; }
        public string EosEntryID { get; set; }
        public string EosVersion { get; set; }
        public int Rank { get; set; }
        public string PartNumber { get; set; }
        public string ReplacementPartNumber { get; set; }

        public static string LookupEosType(object eosType)
        {
            int type;
            if (!int.TryParse(Convert.ToString(eosType), out type))
                type = 0;

            switch (type)
            {
                case 0: return "";
                case 1: return Resources.NCMWebContent.WEBDATA_IC_217;
                case 2: return Resources.NCMWebContent.WEBDATA_IC_218;
                case 3: return Resources.NCMWebContent.WEBDATA_IC_219;
                case 4: return Resources.NCMWebContent.WEBDATA_IC_220;
                default: throw new ArgumentOutOfRangeException(nameof(eosType));
            }
        }
    }
}