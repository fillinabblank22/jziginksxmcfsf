﻿<%@ WebHandler Language="C#" Class="EosDataExporter" %>

using System;
using System.Web;
using System.Data;
using System.Collections.Generic;
using System.Web.SessionState;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;
using SolarWinds.NCMModule.Web.Resources.Eos;

public class EosDataExporter : IHttpHandler, IRequiresSessionState {
    
    #region fields
    private const string FILE_NAME="EosData";
    private Exporter.ExportMethod exportMethod;
    
    private string groupByProperty;
    private string groupByValue;
    private string search;
    private string sortColumn;
    private string sortDirection;
    private bool displayIgnoredDevices;
    private string[] colsToExport;
    
    #endregion
    
        
    public void ProcessRequest (HttpContext context) 
    {
        InternalInitialize(context);
        InternalExport(context);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

    #region private

    private void InternalExport(HttpContext context)
    {
        search = search.Replace(@"'", @"''");
        groupByValue = groupByValue.Replace(@"'", @"''");
        
        EosDataProvider provider = new EosDataProvider()
        {
            GroupByColumn = CustomPropertyHelper.CovertGroupingPropertyName(groupByProperty),
            GroupByValue = groupByValue,
            SearchTerm = search,
            SortColumn = sortColumn,
            SortDirection = sortDirection,
            DisplayIgnoredDevices = displayIgnoredDevices,
            FiltersCollection = context.Request.Params
        };
        
        DataTable dt = provider.GetEosData();
        byte[] buffer = ExportDataToBuffer(dt);
        FinalizeFileTransfering(context, buffer);
    }
    
    private string BuildFileName()
    {
        return $@"{FILE_NAME}.{exportMethod.ToString().ToLowerInvariant()}";
    }
    
    private byte[] ExportDataToBuffer(DataTable dt)
    {
        Exporter exporter = new Exporter(dt);
        exporter.dlgCustomCleanUpDataTable = CleanUpDataTable;
        exporter.dlgXLSHeaderFormate = CreateHeader;
        exporter.dlgCSVHeaderFormate = CreateHeader;
        byte[] res = exporter.DoExport(exportMethod);
        
        return res;
    }

    private void InternalInitialize(HttpContext context)
    {
        string obj = context.Request.Params[@"method"];
        exportMethod = (Exporter.ExportMethod)Enum.Parse(typeof(Exporter.ExportMethod), obj);

        groupByProperty = context.Request.Params[@"groupByProperty"];
        groupByValue = context.Request.Params[@"groupByValue"];
        search = context.Request.Params[@"searchTerm"];
        sortColumn = context.Request.Params[@"sortColumn"];
        sortDirection = context.Request.Params[@"sortDirection"];
        displayIgnoredDevices = Convert.ToBoolean(context.Request.Params[@"displayIgnoredDevices"]);
        colsToExport = context.Request.Params[@"colsToExport"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
    }

    private void FinalizeFileTransfering(HttpContext context, byte[] buffer)
    {
        context.Response.Clear();
        context.Response.ContentType = $@"application/{exportMethod.ToString().ToLowerInvariant()}";
        context.Response.AddHeader(@"Content-Disposition", $@"attachment; filename={BuildFileName()}");
        context.Response.OutputStream.Write(buffer, 0, buffer.Length);
        context.ApplicationInstance.CompleteRequest();
    }

    private DataTable CleanUpDataTable(DataTable dt)
    {
        DataTable dtCopy = dt.Copy();
        dtCopy.Constraints.Clear();

        foreach (DataColumn column in dt.Columns)
        {
            if (!Array.Exists(colsToExport, item => item.Equals(column.ColumnName, StringComparison.InvariantCultureIgnoreCase)))
            {
                string columnToRemove = column.ColumnName;
                dtCopy.Columns.Remove(columnToRemove);
            }
        }

        return dtCopy;
    }

    private List<Exporter.HeaderRowConfig> CreateHeader(DataColumnCollection columns)
    {
        List<Exporter.HeaderRowConfig> headerRowList = new List<Exporter.HeaderRowConfig>();

        foreach (DataColumn column in columns)
        {
            if (Array.Exists(colsToExport, item => item.Equals(column.ColumnName, StringComparison.InvariantCultureIgnoreCase)))
            {
                string caption = column.Caption.Equals(@"EntityPhysical_Model", StringComparison.InvariantCultureIgnoreCase) ? @"PartNumber" : column.Caption;
                headerRowList.Add(new Exporter.HeaderRowConfig(caption, 0));
            }
        }

        return headerRowList;
    }
    #endregion
}