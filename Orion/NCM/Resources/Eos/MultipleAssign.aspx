﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="MultipleAssign.aspx.cs" Inherits="Orion_NCM_Resources_Eos_MultipleAssign" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Assembly="SolarWinds.NCMModule.Web.Resources" Namespace="SolarWinds.NCMModule.Web.Resources" TagPrefix="ncm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmHeadPlaceHolder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
    <style type="text/css">                                     
        .EosTable
        {
            border: solid 1px #dfdfde;
            background-color: white;
            width: 100%;
        }        

        .EosTable tr
        {
            height: 25px;
        }
        
        .EosTable thead tr td
        {
            font-weight: normal;
            background-color: #dddcd0;
            font-size: 11px;
        }
        
        .NodesTable
        {
            background-color: white;
            width: 80%;
        }

        .NodesTable tr
        {
            height: 25px;
        }
        
        .NodesTable thead tr td
        {
            font-weight: normal;
            background-color: #dddcd0;
            font-size: 11px;
        }
        
        .PagingToolbar { border: 0 none !important; }
        .PagingToolbar td   
        {
            border: 0 none !important; 
            padding: 0 2px; 
            background-color: #E2E1D4;
            font-family: Arial,Helvetica,sans-serif;
            font-size: 7pt !important;
            font-weight: normal;
            padding-left: 5px;
            height:25px; }
        .PagingToolbar img { vertical-align: top !important; }
        .PagingToolbar a { white-space: nowrap; font-size: 8pt; }
        .PagingToolbar .disabled { color: silver; }
        .PagingToolbar .spacer { width: 18px; height: 15px; background: url('/Orion/NCM/Resources/images/ConfigChangeApproval/arrows.gif') no-repeat scroll 0 0 transparent; }
        .PagingToolbar .enabled .prev { background-position: 0px 0px !important; }
        .PagingToolbar .enabled .next { background-position: 0px -15px !important; }
        .PagingToolbar .disabled .prev { background-position: -18px 0px !important; }
        .PagingToolbar .disabled .next { background-position: -18px -15px !important; }
        .PagingToolbar .enabled .all { background-position: 0px -30px !important; }
        .PagingToolbar .disabled .all { background-position: -18px -30px !important; }
        
        .MachineTypeList
        {
            border: solid 1px #dfdfde;
            background-color: white;
        }
    </style>

    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />

    <link rel="Stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Resources/Jobs/Jobs.css" />
    <script src="/Orion/NCM/Resources/Eos/MultipleAssign.js" type="text/javascript"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmExportToPDFPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ncmCustomizePagePlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="ncmHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMPHEOSAssignData" />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
    <div style="padding:10px">
        <div style="border:solid 1px #dcdbd7;width:90%;padding:10px;background-color:#fff;">
            <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <div style="padding-top:20px;">
                        <div style="font-weight:bold;padding-bottom:5px;font-size:14px"><%=SelectedNodesTitle%></div>
                    </div>
                    <table>
                        <tr>
                            <td style="margin-left:10px">
                                <%= Resources.NCMWebContent.WEBDATA_IC_41 %>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddViewby" runat="server" Width="200px" CssClass="SelectField" OnSelectedIndexChanged="ddViewby_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Text="<%$ Resources:NCMWebContent,WEBDATA_VK_279 %>" Value="Machinetype" Selected ="True"/>
                                    <asp:ListItem Text="<%$ Resources:NCMWebContent,WEBDATA_VK_281 %>" Value="OSVersion" />
                                    <asp:ListItem Text="<%$ Resources:NCMWebContent,WEBDATA_DP_09 %>" Value="PartNumber" />
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                    <div style="border-bottom:#e6e6dd 1px solid;padding-top:10px;padding-bottom:20px">                      
                         <div style="width:400px">
                             <asp:ListView ID="NodesListView" runat="server" AutoGenerateColumns="False" OnItemDeleting="NodesListView_ItemDeleting">
                                <LayoutTemplate>
                                    <table class="NodesTable" cellspacing="0">
                                        <tbody>
                                            <tr runat="server" id="itemPlaceholder"/>
                                        </tbody>
                                    </table>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr style="background-color:#F0F0F0">
                                        <td  nowrap="nowrap"><%# string.IsNullOrEmpty (Eval(@"GroupName") != null ? Eval(@"GroupName").ToString () : "") ? Resources.NCMWebContent.WEBDATA_IC_232 : Eval(@"GroupName") %> (<%# Convert.ToInt32(Eval(@"NodesCount")) == 1 ? Eval(@"NodesCount") + @" " + Resources.NCMWebContent.WEBDATA_IC_231  : Eval(@"NodesCount") + @" " + Resources.NCMWebContent.WEBDATA_IC_223 %>)</td>                           
                                        <td>                            
                                            <asp:ImageButton ID="btnRemove" runat="server" CommandName="Delete" CausesValidation="false" ImageUrl="/Orion/NCM/Resources/images/icon_delete.png" />
                                        </td>
                                </ItemTemplate>                   
                                <EmptyDataTemplate>
                                    <table class="NodesTable" cellspacing="0">                           
                                        <tbody>
                                            <tr>
                                                <td colspan="6" style="padding-left:10px;">
                                                    <%= Resources.NCMWebContent.WEBDATA_IC_206 %>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </EmptyDataTemplate>
                            </asp:ListView>  
                        </div>                    
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>          
        
            <table width="95%">
                <tr>
                    <td style="padding-top:30px;padding-bottom:10px;padding-right:10px;" valign="bottom">
                        <div style="font-weight:bold;font-size:14px;"><%= Resources.NCMWebContent.WEBDATA_IC_209 %></div>
                        <div style="padding-top:5px;">
                            <%=string.Format(Resources.NCMWebContent.WEBDATA_IC_210, @"<b>", @"</b>") %>
                        </div>
                        <div style="padding-top:5px;">
                            <span class="Suggestion"><span class="Suggestion-Icon"></span>
                                <%=Resources.NCMWebContent.WEBDATA_VK_903 %>
                            </span>
                        </div>
                    </td>
                    <td id="notification" runat="server" style="padding-top:30px;padding-bottom:10px;" align="right" valign="bottom">
                        <div class="Suggestion"><div class="Suggestion-Icon"></div>
                            <div style="text-align:left;">
                                <b><%=Resources.NCMWebContent.WEBDATA_VK_894 %></b>
                                <div><%=Resources.NCMWebContent.WEBDATA_VK_895 %></div>
                            </div>
                       </div>
                    </td>
                </tr>
            </table>

            <asp:UpdatePanel ID="UpdatePanel2" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <table width="95%">
                        <tr>
                            <td>           
                                <asp:ListView ID="EosGrid" runat="server" AutoGenerateColumns="False" OnItemDataBound="EosGrid_ItemDataBound" OnLayoutCreated="OnLayoutCreated">                                   
                                    <LayoutTemplate>
                                        <table class="EosTable" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <td style="width:3%;">&nbsp;</td>                                   
                                                    <td style="width:8%;" nowrap="nowrap"><asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:NCMWebContent,WEBDATA_IC_198 %>" /></td>
                                                    <td style="width:8%;" nowrap="nowrap"><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:NCMWebContent,WEBDATA_IC_199 %>" /></td>
                                                    <td style="width:8%;" nowrap="nowrap"><asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:NCMWebContent,WEBDATA_DP_06 %>" /></td>
                                                    <td style="width:35%;" nowrap="nowrap"><asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:NCMWebContent,WEBDATA_VK_879 %>" /></td>
                                                    <td style="width:8%;" nowrap="nowrap"><asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:NCMWebContent,WEBDATA_IC_200 %>" /></td>                                                    
                                                    <td style="width:13%;" nowrap="nowrap"><asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:NCMWebContent,WEBDATA_DP_09 %>" /></td>
                                                    <td style="width:13%;" nowrap="nowrap"><asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:NCMWebContent,WEBDATA_DP_07 %>" /></td>                                                 
                                                    <td style="width:5%;" ID="reliabilityHeader" nowrap="nowrap" runat="server"><asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:NCMWebContent,WEBDATA_DP_10 %>" /></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr runat="server" id="itemPlaceholder"/>
                                            </tbody>
                                        </table>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr id="EosTableRow" runat="server">
                                            <td style="width:20px;" align="center">
                                                <asp:RadioButton ID="rb" runat="server" OnCheckedChanged="rb_OnCheckedChanged" AutoPostBack="true" EndOfSupport='<%# Eval(@"EndOfSupport") %>' EosEntryID='<%# Eval(@"EosEntryID") %>' EosVersion='<%# Eval(@"EosVersion") %>' EndOfSales='<%# Eval(@"EndOfSales") %>' EndOfSoftware='<%# Eval(@"EndOfSoftware") %>' EosLink='<%# Eval(@"EosLink") %>' EosType='<%# Eval(@"EosType") %>' ReplacementPartNumber='<%# Eval(@"ReplacementPartNumber") %>'/>
                                            </td>                           
                                            <td nowrap="nowrap"><%# Eval(@"EndOfSupport")%></td>
                                            <td nowrap="nowrap"><%# Eval(@"EndOfSales")%></td>
                                            <td nowrap="nowrap"><%# Eval(@"EndOfSoftware")%></td>
                                            <td><%# Eval(@"EosModel")%></td>
                                            <td nowrap="nowrap"><%# Eval(@"EosUILink")%></td>                                            
                                            <td><%# Eval(@"PartNumber")%></td>
                                            <td><%# Eval(@"ReplacementPartNumber")%></td>                                                                                         
                                            <td nowrap="nowrap" Visible="<%# SameModel%>"><p CertaintyKey="<%# Eval(@"CertaintyKey")%>"><%# Eval(@"Certainty")%></p></td>
                                        </tr>
                                    </ItemTemplate>
                                    <EmptyDataTemplate>
                                        <table class="EosTable" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <td style="width:3%;">&nbsp;</td>
                                                    <td style="width:8%;" nowrap="nowrap"><asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:NCMWebContent,WEBDATA_IC_198 %>" /></td>
                                                    <td style="width:8%;" nowrap="nowrap"><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:NCMWebContent,WEBDATA_IC_199 %>" /></td>
                                                    <td style="width:8%;" nowrap="nowrap"><asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:NCMWebContent,WEBDATA_DP_06 %>" /></td>
                                                    <td style="width:35%;" nowrap="nowrap"><asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:NCMWebContent,WEBDATA_VK_879 %>" /></td>
                                                    <td style="width:8%;" nowrap="nowrap"><asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:NCMWebContent,WEBDATA_IC_200 %>" /></td>                                                    
                                                    <td style="width:13%;" nowrap="nowrap"><asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:NCMWebContent,WEBDATA_DP_09 %>" /></td>
                                                    <td style="width:13%;" nowrap="nowrap"><asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:NCMWebContent,WEBDATA_DP_07 %>" /></td>
                                                    <td style="width:5%;" nowrap="nowrap"><asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:NCMWebContent,WEBDATA_DP_10 %>" /></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td colspan="6" style="padding-left:10px;">
                                                        <%= Resources.NCMWebContent.WEBDATA_IC_206 %>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </EmptyDataTemplate>
                                </asp:ListView>
                                <table cellpadding="0" cellspacing="0" width="100%" class="PagingToolbar">
                                    <tr>
                                        <td align="left" valign="middle" style="width:180px;">
                                            <asp:LinkButton ID="btnPrevious25" CssClass="enabled" runat="server" OnClick="btnPrevious25_Click">
                                                <img class="spacer prev" src="/Orion/NCM/Resources/images/ConfigChangeApproval/arrow-spacer.gif" />&nbsp;<%= Resources.NCMWebContent.WEBDATA_IC_212 %>
                                            </asp:LinkButton>
                                        </td>
                                        <td>&nbsp;</td>
                                        <td align="left" valign="middle">
                                            <asp:LinkButton ID="btnNext25" CssClass="enabled" runat="server"  OnClick="btnNext25_Click">
                                                <%= Resources.NCMWebContent.WEBDATA_IC_213 %>&nbsp;<img class="spacer next" src="/Orion/NCM/Resources/images/ConfigChangeApproval/arrow-spacer.gif" />
                                            </asp:LinkButton>
                                        </td>
                                        <td align="right" valign="middle" style="width:100%;">
                                            <asp:Label ID="pagingMsg" runat="server" style="vertical-align:middle;font-size:11px;padding-right:3px;"></asp:Label>                                    
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    
                    <table>
                        <tr>
                            <td style="padding-top:20px;" colspan="4">
                                <%=string.Format(Resources.NCMWebContent.WEBDATA_IC_215, @"<b>", @"</b>") %>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top:10px;">
                                <asp:RadioButton ID="rbAssignManually" runat="server" GroupName="EosGroup" OnCheckedChanged="rbAssignManually_OnCheckedChanged" AutoPostBack="true" />
                            </td>
                            <td style="padding-top:10px;">
                                <ncm:Calendar ID="EndofSupport" Text="<%$ Resources:NCMWebContent, WEBDATA_IC_182 %>" CalendarTextBoxCssClass="TextField" DisplayTime="false" runat="server" />
                            </td>
                            <td style="padding-left:30px;padding-top:10px;">
                                <ncm:Calendar ID="EndofSales" Text="<%$ Resources:NCMWebContent, WEBDATA_IC_183 %>" CalendarTextBoxCssClass="TextField" DisplayTime="false" runat="server" />
                            </td>
                            <td style="padding-left:30px;padding-top:10px;">
                                <ncm:Calendar ID="EndOfSoftware" Text="<%$ Resources:NCMWebContent, WEBDATA_DP_08%>" CalendarTextBoxCssClass="TextField" DisplayTime="false" runat="server" />
                            </td>
                            <td style="padding-left:30px;padding-top:10px;">
                                <span><%= Resources.NCMWebContent.WEBDATA_IC_214 %></span>
                                <asp:TextBox ID="ExternalLink" CssClass="TextField" runat="server" Width="500px" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>

            <div style="padding-top:30px;font-size:14px">
                <b><%= Resources.NCMWebContent.WEBDATA_IC_216 %></b>
            </div>
            <div style="padding-top:10px">
                <asp:TextBox ID="Notes" runat="server" Font-Size="9pt" BorderColor="#999999" BorderWidth="1px" style="resize:none;" spellcheck="false" TextMode="MultiLine" Width="400px" Height="80px"/>
            </div>
            <div style="padding-top:10px">                      
                 <asp:Label ID="lblError" runat="server" Text= "<%$ Resources:NCMWebContent, WEBDATA_IC_184 %>" Visible="false" ForeColor="Red" />
            </div> 
        </div>
        <div style="padding-top:20px;">
            <orion:LocalizableButton runat="server" ID="btnAssign" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_875 %>" OnClick="btnAssign_Click" DisplayType="Primary" CausesValidation="true"/> 
            <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" OnClick="btnCancel_Click" DisplayType="Secondary" CausesValidation="false"/>
        </div>
    </div>
</asp:Content>

