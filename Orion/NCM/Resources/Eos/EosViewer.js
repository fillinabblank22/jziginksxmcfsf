﻿Ext.namespace('SW');
Ext.namespace('SW.NCM');

Ext.QuickTips.init();

SW.NCM.EoLEoS = function () {

    SW.NCM.PageSize = 25;
    SW.NCM.AllSelected = false;
    SW.NCM.ColumnDisplayName = new Object();
    SW.NCM.ShowEosDeleteNotification = false;

    var grid, combo, filters, checkBox, currentSearchTerm = null;

    ORION.prefix = "NCM_Eos_";

    function gridResize() {
        if (grid != null) {
            grid.setWidth(1);
            grid.setWidth($('#gridCell').width());

            var h = getGridHeight()
            grid.setHeight(getGridHeight());

            var groupItems = $(".GroupItems");
            groupItems.height(h - $(".ncm_GroupSection").height() - 14);
        }
    }

    function getGridHeight() {
        var top = $('#gridCell').offset().top;
        return $(window).height() - top - $('#footer').height() - 100;
    }

    var updateToolbarButtons = function () {
        var selCount = grid.getSelectionModel().getCount();
        var map = grid.getTopToolbar().items.map;

        var needsOnlyOneSelected = (selCount != 1);
        var needsAtLeastOneSelected = (selCount === 0);
        var isDisabledMode = !SW.NCM.IsAdminOrEngineer;

        var IsDemoServer = SW.NCM.IsDemoServer;

        if (IsDemoServer) {
            map.AssignBtn.setDisabled(needsAtLeastOneSelected);
        }
        else {
            map.AssignBtn.setDisabled(needsAtLeastOneSelected || isDisabledMode);
        }

        map.IgnoreDevicesBtn.setDisabled(needsAtLeastOneSelected || isDisabledMode);
        map.RemoveFromIgnoredDevicesBtn.setDisabled(needsAtLeastOneSelected || isDisabledMode);
        map.DeleteEosDataBtn.setDisabled(needsAtLeastOneSelected || isDisabledMode);
        map.RenewMatchesBtn.setDisabled(!needsAtLeastOneSelected || isDisabledMode);

        var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');
        if (grid.getStore().getCount() > 0 && selCount == grid.getStore().getCount()) {
            hd.addClass('x-grid3-hd-checker-on');
        } else {
            hd.removeClass('x-grid3-hd-checker-on');
        }

        updateSelectAll();
    }

    var updateIgnoredButtons = function (groupBy, groupByValue) {
        var map = grid.getTopToolbar().items.map;

        var needsIgnoredGroupSelected = (groupBy == 'EosType' && groupByValue == '5');

        map.IgnoreDevicesBtn.setVisible(!needsIgnoredGroupSelected && SW.NCM.IsAdminOrEngineer);
        map.RemoveFromIgnoredDevicesBtn.setVisible(needsIgnoredGroupSelected && SW.NCM.IsAdminOrEngineer);
    }

    var updateSelectAll = function () {
        $("#selectAll").empty();
        $("#selectAll").removeClass("select-all-page");

        var selCount = grid.getSelectionModel().getCount();
        var pageSize = grid.getBottomToolbar().pageSize;
        var totalCount = grid.getStore().getTotalCount();
        var pageCount = grid.getStore().getCount();

        if ((pageCount > 0) && (totalCount > pageSize) && (selCount == pageCount)) {
            var countOfNodesOnPage = pageSize;
            if ((pageCount % pageSize) != 0) {
                countOfNodesOnPage = pageCount % pageSize;
            }

            $("#selectAll").append(String.format("<span>@{R=NCM.Strings;K=WEBJS_VK_215;E=js}</span> <a href='#'>@{R=NCM.Strings;K=WEBJS_VK_216;E=js}</a>", countOfNodesOnPage, totalCount)).addClass("select-all-page");
            $("#selectAll a").click(function () {
                $("#selectAll").empty();
                $("#selectAll").removeClass("select-all-page");
                $("#selectAll").append(String.format("<span>@{R=NCM.Strings;K=WEBJS_VK_217;E=js}</span>"));

                SW.NCM.AllSelected = true;
            });
        }
        else {
            SW.NCM.AllSelected = false;
        }
    }

    var getDateTimeOffset = function () {
        var d = new Date();
        var n = d.getTimezoneOffset();
        return n;
    }

    var getGroupByValues = function (groupBy, displayIgnoredDevices, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/EoLEoS.asmx",
                             "GetValuesAndCountForProperty", { property: groupBy, displayIgnoredDevices: displayIgnoredDevices, clientOffset: getDateTimeOffset() },
                             function (result) {
                                 onSuccess(ORION.objectFromDataTable(result));
                             });
    };

    var loadGroupByValues = function (groupBy, selectValue, displayIgnoredDevices) {
        var groupItems = $(".GroupItems").text('@{R=NCM.Strings;K=WEBJS_VK_04;E=js}');

        getGroupByValues(groupBy, displayIgnoredDevices, function (result) {
            groupItems.empty();
            $(result.Rows).each(function () {
                var value = Ext.util.Format.htmlEncode(this.theValue);
                if (value == null) value = "";
                value = String(value);
                if (value.startsWith('/Date(')) {
                    if (value.match(/\/Date\((\d+)\)\//gi) != null) {
                        var date = eval(value.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
                        value = Ext.util.Format.date(date, ExtDaTimeLocalePattern.DateTimeLong);
                    }
                    else {
                        value = "";
                    }
                }
                var text = groupByItemNameRender(groupBy, $.trim(value));
                var title = text;
                if (text.length > 25) { text = String.format("{0}...", text.substr(0, 25)); };
                var disp = (text) + " (" + this.theCount + ")";
                $("<a href='#'></a>")
					.attr('value', Ext.util.Format.htmlEncode(value)).attr('title', title)
					.text(disp).click(selectGroup).appendTo(".GroupItems").wrap("<li class='group-core-node-item'/>");
            });

            var toSelect = $(".group-core-node-item a[value='" + selectValue + "']");
            if (toSelect.length === 0) {
                toSelect = $(".group-core-node-item a:first");
            }

            if (toSelect.length === 0) {
                //GroupBy list is empty in case no nodes in database or all nodes have ignored eos type
                refreshObjects(groupBy, selectValue, '', displayIgnoredDevices);
            }
            else {
                toSelect.click();
            }
        });
    };

    var selectGroup = function () {
        $(this).parent().addClass("SelectedGroupItem").siblings().removeClass("SelectedGroupItem");
        var obj = getGroupByParams();
        ORION.Prefs.save('GroupByValue', obj.groupByValue);
        refreshObjects(obj.groupBy, obj.groupByValue, '', obj.displayIgnoredDevices);
        updateIgnoredButtons(obj.groupBy, obj.groupByValue);
        return false;
    };

    function getGroupByParams() {
        var obj = {};
        obj.groupBy = combo.getValue();

        if ($(".SelectedGroupItem a").length == 0) {
            obj.groupByValue = '';
        }
        else {
            obj.groupByValue = Ext.util.Format.htmlDecode($(".SelectedGroupItem a").attr('value'));
        }

        if (obj.groupBy == 'EosType') {
            obj.displayIgnoredDevices = true;
        }
        else {
            obj.displayIgnoredDevices = checkBox.getValue();
        }

        return obj;
    }

    function initToolTip() {
        var toolTip = new Ext.ToolTip({
            width: 325,
            anchor: 'top',
            html: ' \
            <div style="padding:5px;"> \
                <table cellpadding="0" cellspacing="0"> \
                    <tr> \
                        <td style="vertical-align:top"> \
                            <img alt="" src="/Orion/NCM/Resources/images/Ninja_32x32.png" /> \
                        </td> \
                        <td style="padding-left:10px;"> \
                            <div style="font-size:12px;color:#00AADD;font-weight:bold;">@{R=NCM.Strings;K=WEBJS_VK_233;E=js}</div> \
                            <div style="font-size:11px;font-weight:bold;padding-top:3px;">@{R=NCM.Strings;K=WEBJS_VK_230;E=js}</div> \
                            <div style="font-size:11px;padding-top:3px;color:#646464;">@{R=NCM.Strings;K=WEBJS_VK_231;E=js}</div> \
                            <div style="font-size:9px;padding-top:3px;color:color:#646464;">@{R=NCM.Strings;K=WEBJS_VK_232;E=js}</div> \
                        </td> \
                    </tr> \
                </table> \
            </div>',
            trackMouse: true
        });
        toolTip.initTarget('filterHelpLink');
    }

    function reconfigureEosDeleteNotification() {
        var notificationTemplate = '\
        <span class="eos-success"> \
            <span class="eos-success-icon"></span> \
            {0} \
        </span>';

        $("#eosDeleteNotification").empty();
        if (SW.NCM.ShowEosDeleteNotification) {
            var notification = String.format(notificationTemplate, '@{R=NCM.Strings;K=WEBJS_VK_227;E=js}');
            $("#eosDeleteNotification").append(notification);

            SW.NCM.ShowEosDeleteNotification = false;
        }
    }

    function reconfigureEosNotification() {
        var notificationTemplate = '\
        <span class="eos-suggestion"> \
            <span class="eos-suggestion-icon"></span> \
            {0} \
        </span>';

        ORION.callWebService("/Orion/NCM/Services/EoLEoS.asmx",
            "ShowEosNotification", {},
            function (result) {
                if (result && result.NodesCountWithNoEosDates > 0) {
                    $("#eosNotification").empty();

                    var notification;
                    if (result.EosSchedulingEnabled) {
                        notification = String.format(notificationTemplate, String.format('@{R=NCM.Strings;K=WEBJS_VK_228;E=js}',
                            '<b>',
                            result.NodesCountWithNoEosDates,
                            '</b>',
                            result.EosStartTime));
                    }
                    else {
                        notification = String.format(notificationTemplate, String.format('@{R=NCM.Strings;K=WEBJS_VK_229;E=js}',
                            '<b>',
                            result.NodesCountWithNoEosDates,
                            '</b>'));
                    }

                    $("#eosNotification").append(notification);
                }
                else {
                    $("#eosNotification").empty();
                }
            });
    }

    function reconfigureFilterNotifications() {
        var filterData = grid.filters.getFilterData();
        var notificationTemplate = '\
        <span id="Hint" style="position: static;display: inline-block;padding-top:2px;padding-bottom:2px;" runat="server"> \
            <span class="ncm-filter"> \
                <table cellpadding="0" cellspacing="0"> \
                    <tr> \
                        <td><span class="ncm-filter-icon ncm-filter-icon-hint"></span></td> \
                        <td><span style="vertical-align: middle; padding-right: 10px">{0}</span></td> \
                        <td><span><a href="javascript:RemoveFilter(\'{1}\',\'{2}\');" ><img id="deleteButton" src="/Orion/NCM/Resources/images/icon_delete.png" style="cursor: pointer;" /></a></td> \
                    </tr> \
                </table> \
            </span> \
        </span>';

        $("#filterNotifications").each(function () {
            $(this).empty();

            for (var i = 0; i < filterData.length; i++) {
                var comparison = (filterData[i].data.comparison == undefined) ? "" : filterData[i].data.comparison;
                var notification = $(String.format(notificationTemplate, getFilterNotificationMessage(filterData[i]), filterData[i].field, comparison));
                $(this).append(notification);
            }
        });

        gridResize();
    }

    ShowFilter = function (evt, name) {
        evt = (evt) ? evt : window.event;
        evt.cancelBubble = true;

        for (var i = 0; i < grid.filters.filters.items.length; i++) {
            if (grid.filters.filters.items[i].dataIndex == name) {
                var menu = grid.filters.filters.items[i].menu;
                var event = Ext.EventObject;
                menu.showAt(event.getXY());
            }
        }
        return false;
    };

    RemoveFilter = function (name, comparison) {
        for (var i = 0; i < grid.filters.filters.items.length; i++) {
            if (grid.filters.filters.items[i].active && grid.filters.filters.items[i].dataIndex == name) {
                switch (grid.filters.filters.items[i].type) {
                    case "eosdate":
                        grid.filters.filters.items[i].disableFilter(comparison);
                        break;
                    case "string":
                        grid.filters.filters.items[i].disableFilter();
                        break;
                    default:
                        grid.filters.filters.items[i].active = false;
                        break;

                }
            }
        }
        grid.getStore().lastOptions.params.start = 0;
        grid.getStore().reload();
    }

    function setFilterData(filterData) {
        var filter = grid.filters.getFilter(Ext.util.Format.htmlDecode (filterData.field));

        switch (filter.type) {
            case "string":
            {
                filter.setActive(true);
                filter.setValue(Ext.util.Format.htmlDecode (filterData.value));
                break;
                }
            case "eosdate":
                {
                    for (key in filter.fields) {
                        if (Ext.util.Format.htmlDecode (filterData.comparison) == filter.compareMap[key]) {
                            filter.fields[key].setChecked(true, false);
                            break;
                        }
                    }
                }
            default: break;
        }

        //cancel reloading grid
        grid.filters.deferredUpdate.cancel();
    }

    function getFilterNotificationMessage(filter) {
        var displayName = SW.NCM.ColumnDisplayName[filter.field] || filter.field;

        if (Ext.isEmpty(filter.data.value)) {
            return String.format('@{R=NCM.Strings;K=WEBJS_VM_82;E=js}', '<b>', displayName, '</b>');
        }

        if (filter.data.type == 'eosdate') {
            switch (filter.data.comparison) {
                case 'lt':
                    return String.format('@{R=NCM.Strings;K=WEBJS_VM_83;E=js}', '<b>', displayName, '</b>', filter.data.value);
                case 'gt':
                    return String.format('@{R=NCM.Strings;K=WEBJS_VM_84;E=js}', '<b>', displayName, '</b>', filter.data.value);
                case 'lt3':
                case 'lt6':
                case 'lt12':
                case 'lt24':
                case 'lt36':
                    return String.format('<b>{0}</b>: {1}', displayName, LookupEosDate(filter.data.value));
                case 'eos0':
                case 'eos4':
                    return String.format('<b>{0}</b>: {1}', displayName, LookupEosType(filter.data.value));
            }
        }

        return String.format('<b>{0}</b>: {1}', displayName, Ext.util.Format.htmlEncode(filter.data.value));
    }

    function clearAllFiltersHandler() {
        var needReloadGridStore = false;

        var filterData = grid.filters.getFilterData();
        for (var i = 0; i < filterData.length; i++) {

            needReloadGridStore = true;

            var dataIndex = filterData[i].field;
            var comparison = (filterData[i].data.comparison == undefined) ? "" : filterData[i].data.comparison;
            var filter = grid.filters.getFilter(dataIndex);

            switch (filter.type) {
                case "eosdate":
                    filter.disableFilter(comparison);
                    break;
                case "string":
                    filter.disableFilter();
                    break;
                default:
                    filter.active = false;
                    break;
            }
        }

        if (needReloadGridStore) {
            grid.getStore().lastOptions.params.start = 0;
            grid.getStore().reload();
        }
    }

    function filtersToQueryString(filterData) {
        var filterQueryString = '';
        var amp = ''
        $.each(filterData, function (index, filter) {
            filterQueryString = filterQueryString + String.format('{5}filter[{0}][field]={1}&filter[{0}][data][type]={2}&filter[{0}][data][comparison]={3}&filter[{0}][data][value]={4}',
            index,
            filter.field,
            filter.data.type,
            filter.data.comparison,
            filter.data.value,
            amp);

            amp = '&';
        });
        return filterQueryString;
    }

    var findGroupByValue = function (value) {
        var store = combo.getStore();
        var index = store.findExact('field1', value);
        if (index == -1) {
            return '';
        }
        else {
            return value;
        }
    }

    var groupByControlInitializer = function () {
        combo = new Ext.form.ComboBox({
            width: 200,
            store: SW.NCM.GroupByData,
            value: '',
            listWidth: 200,
            triggerAction: 'all',
            editable: false,
            forceSelection: true,
            renderTo: 'GroupBy'
        });

        combo.on('select', function (combo, record) {

            var obj = getGroupByParams();

            if (obj.groupBy == '') {
                var groupItems = $(".GroupItems").text('@{R=NCM.Strings;K=WEBJS_VK_04;E=js}');
                updateIgnoredButtons('', '');
                refreshObjects("", "", "", obj.displayIgnoredDevices);
                groupItems.empty();
            }
            else {
                loadGroupByValues(obj.groupBy, '', obj.displayIgnoredDevices);
            }

            if (obj.groupBy == 'EosType') {
                checkBox.hide();
            }
            else {
                checkBox.show();
            }

            ORION.Prefs.save('GroupBy', obj.groupBy);
        });
    }

    var refreshObjects = function (property, value, search, displayIgnoredDevices) {
        grid.store.removeAll();
        var pagingToolbar = grid.getBottomToolbar();
        pagingToolbar.cursor = 0;
        grid.store.baseParams['limit'] = grid.getBottomToolbar().pageSize;
        grid.store.proxy.conn.jsonData = { property: property, value: value, search: search, displayIgnoredDevices: displayIgnoredDevices, clientOffset: getDateTimeOffset() };
        currentSearchTerm = search;
        cleanSearchTextBox(currentSearchTerm);
        pagingToolbar.doLoad(pagingToolbar.cursor);
    };

    var groupByItemNameRender = function (group, value) {
        if (group == "Status") {
            switch (value) {
                case "1": return '@{R=NCM.Strings;K=STATUS_UP;E=js}';
                case "2": return '@{R=NCM.Strings;K=STATUS_DOWN;E=js}';
                case "3": return '@{R=NCM.Strings;K=STATUS_WARNING;E=js}';
                case "9": return '@{R=NCM.Strings;K=STATUS_UNMANAGED;E=js}';
                case "11": return '@{R=NCM.Strings;K=STATUS_EXTERNAL;E=js}';
                case "12": return '@{R=NCM.Strings;K=STATUS_UNREACHABLE;E=js}';
                default: return '@{R=NCM.Strings;K=STATUS_UNKNOWN;E=js}';
            }
        }
        else if (group == "EosType") {
            return LookupEosType(value);
        }
        else {
            if (value == "") {
                return '@{R=NCM.Strings;K=WEBJS_VK_66;E=js}';
            }
        }

        return value;
    }

    function LookupEosType(value) {
        switch (value) {
            case "0": return '@{R=NCM.Strings;K=WEBJS_VM_85;E=js}';
            case "1": return '@{R=NCM.Strings;K=WEBJS_VM_86;E=js}';
            case "2": return '@{R=NCM.Strings;K=WEBJS_VM_87;E=js}';
            case "3": return '@{R=NCM.Strings;K=WEBJS_VM_88;E=js}';
            case "4": return '@{R=NCM.Strings;K=WEBJS_VM_89;E=js}';
            case "5": return '@{R=NCM.Strings;K=WEBJS_VK_207;E=js}';
            default: return '';
        }
    }

    function LookupEosDate(value) {
        switch (value) {
            case '3': return '@{R=NCM.Strings;K=WEBJS_VK_222;E=js}';
            case '6': return '@{R=NCM.Strings;K=WEBJS_VK_223;E=js}';
            case '12': return '@{R=NCM.Strings;K=WEBJS_VK_224;E=js}';
            case '24': return '@{R=NCM.Strings;K=WEBJS_VK_225;E=js}';
            case '36': return '@{R=NCM.Strings;K=WEBJS_VK_226;E=js}';
            default: return '';
        }
    }

    function renderNodeName(value, meta, record) {
        if (SW.NCM.IsAdminOrEngineer) {
            return String.format('<a href="#" value="{0}" class="nodeName">{1}</a>', record.data.NodeID, Ext.util.Format.htmlEncode(value));
        }
        else {
            return Ext.util.Format.htmlEncode(value);
        }
    }

    function renderEoSEoLColumns(value, meta, record) {
        switch (record.data.EosType) {
            case 5: return '@{R=NCM.Strings;K=WEBJS_VK_207;E=js}';
            case 4: return '@{R=NCM.Strings;K=WEBJS_VM_89;E=js}';
            case 0: return '@{R=NCM.Strings;K=WEBJS_VM_85;E=js}';
            default:
                {
                    if (value == null) return '';

                    var date = eval(value.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
                    return Ext.util.Format.date(date, ExtDaTimeLocalePattern.ShortDate);
                }
        }
    }

    function renderDateTime(value, meta, record) {
        if (value == null) return '';

        var date = eval(value.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
        return Ext.util.Format.date(date, ExtDaTimeLocalePattern.DateTimeLong);
    }

    var adjustDataStoreQuery = function (that, options) {
    };

    var cleanSearchTextBox = function (searchTerm) {
        var map = grid.getTopToolbar().items.map;

        var textBoxValue = map.txtSearch.getValue();
        if ((textBoxValue != '') && (searchTerm == '')) {
            map.Clean.setVisible(false);
            map.txtSearch.setValue('');
        }
    }

    var getSearchTerm = function () {
        var map = grid.getTopToolbar().items.map;
        return map.txtSearch.getValue();
    }

    var doClean = function () {
        cleanSearchTextBox('');

        var obj = getGroupByParams();
        refreshObjects(obj.groupBy, obj.groupByValue, '', obj.displayIgnoredDevices);
    }

    var doSearch = function () {
        var search = getSearchTerm();
        if ($.trim(search).length == 0) return;
        var obj = getGroupByParams();
        refreshObjects(obj.groupBy, obj.groupByValue, search, obj.displayIgnoredDevices);
        var map = grid.getTopToolbar().items.map;
        map.Clean.setVisible(true);
    }

    function getSelectedNodeIds(items) {
        var ids = [];
        Ext.each(items, function (item) {
            ids.push(item.data.NodeID);
        });

        return ids;
    }

    function getDataProvider() {
        var dataProvider = {};

        var obj = getGroupByParams();

        dataProvider.groupBy = obj.groupBy;
        dataProvider.groupByValue = obj.groupByValue;
        dataProvider.displayIgnoredDevices = obj.displayIgnoredDevices;
        dataProvider.sortColumn = grid.getStore().sortInfo.field;
        dataProvider.sortDirection = grid.getStore().sortInfo.direction;
        dataProvider.searchTerm = getSearchTerm();
        dataProvider.filters = filtersToQueryString(grid.filters.getFilterData());

        return dataProvider;
    }

    function nodeNameClick(obj) {
        var nodeId = obj.attr("value");
        var nodeIds = [];

        nodeIds.push(nodeId);

        ORION.callWebService("/Orion/NCM/Services/EoLEoS.asmx",
            "BulkEosAssign", { nodeIds: nodeIds, dataProvider: null },
            function (result) {
                if (result) {
                    window.location = 'MultipleAssign.aspx';
                }
            });
    };

    var AssignBtnHandler = function (items) {
        var nodeIds = SW.NCM.AllSelected ? null : getSelectedNodeIds(items);
        var dataProvider = SW.NCM.AllSelected ? getDataProvider() : null;

        ORION.callWebService("/Orion/NCM/Services/EoLEoS.asmx",
        "BulkEosAssign", { nodeIds: nodeIds, dataProvider: dataProvider },
        function (result) {
            if (result && result.success) {
                if (result.hasNodesWithDatesAssigned) {
                    Ext.Msg.show({
                        title: '@{R=NCM.Strings;K=WEBJS_VK_236;E=js}',
                        msg: '@{R=NCM.Strings;K=WEBJS_VK_237;E=js}',
                        width: 400,
                        buttons: {
                            ok: '@{R=NCM.Strings;K=WEBJS_VK_238;E=js}',
                            cancel: '@{R=NCM.Strings;K=WEBJS_VK_38;E=js}'
                        },
                        icon: Ext.MessageBox.QUESTION,
                        fn: function (btn) {
                            if (btn == 'ok') {
                                window.location = 'MultipleAssign.aspx';
                            }
                        }
                    });
                }
                else {
                    window.location = 'MultipleAssign.aspx';
                }
            }
        });
    }

    function RenewMatchesBtnHandler() {
        ORION.callWebService("/Orion/NCM/Services/EoLEoS.asmx",
            "RenewMatches", {},
            function (result) {
                ScheduleRenewMatchesCheck();
            });
    }

    var DeleteEosDataBtnHandler = function (items) {
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_202;E=js}');

        var nodeIds = SW.NCM.AllSelected ? null : getSelectedNodeIds(items);
        var dataProvider = SW.NCM.AllSelected ? getDataProvider() : null;

        internalDeleteEosData(nodeIds, dataProvider, function (result) {
            waitMsg.hide();
            SW.NCM.ShowEosDeleteNotification = true;
            ReloadGridStore();
        });
    }

    var internalDeleteEosData = function (nodeIds, dataProvider, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/EoLEoS.asmx",
            "DeleteEosData", { nodeIds: nodeIds, dataProvider: dataProvider },
            function (result) {
                onSuccess(result);
            });
    };

    var IgnoreDevicesBtnHandler = function (items) {
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_208;E=js}');

        var nodeIds = SW.NCM.AllSelected ? null : getSelectedNodeIds(items);
        var dataProvider = SW.NCM.AllSelected ? getDataProvider() : null;

        internalIgnoreDevices(nodeIds, dataProvider, function (result) {
            waitMsg.hide();
            ReloadGridStore();
        });
    }

    var internalIgnoreDevices = function (nodeIds, dataProvider, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/EoLEoS.asmx",
            "IgnoreDevices", { nodeIds: nodeIds, dataProvider: dataProvider },
            function (result) {
                onSuccess(result);
            });
    };

    var RemoveFromIgnoredDevicesBtnHandler = function (items) {
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_209;E=js}');

        var nodeIds = SW.NCM.AllSelected ? null : getSelectedNodeIds(items);
        var dataProvider = SW.NCM.AllSelected ? getDataProvider() : null;

        internalRemoveFromIgnoredDevices(nodeIds, dataProvider, function (result) {
            waitMsg.hide();
            ReloadGridStore();
        });
    }

    var internalRemoveFromIgnoredDevices = function (nodeIds, dataProvider, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/EoLEoS.asmx",
            "RemoveFromIgnoredDevices", { nodeIds: nodeIds, dataProvider: dataProvider },
            function (result) {
                onSuccess(result);
            });
    };

    var waitInstance;
    function ScheduleRenewMatchesCheck() {
        waitInstance = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_205;E=js}');
        Ext.TaskMgr.start(CheckRenewMatchesStatusTask);
    }

    var CheckRenewMatchesStatusTask = {
        run: function () {
            ORION.callWebService("/Orion/NCM/Services/EoLEoS.asmx",
                             "IsRenewMatchesInProgress", {},
                             function (result) {
                                 if (!result) {
                                     if (CheckRenewMatchesStatusTask != null)
                                         Ext.TaskMgr.stop(CheckRenewMatchesStatusTask);

                                     if (waitInstance != null)
                                         waitInstance.hide();

                                     ReloadGridStore();
                                 }
                             });
        },
        interval: 3000
    }

    function ReloadGridStore() {
        var obj = getGroupByParams();
        var groupBy = obj.groupBy;
        var groupByValue = obj.groupByValue;
        var displayIgnoredDevices = obj.displayIgnoredDevices;
        if (groupBy == '') {
            grid.store.reload();
        }
        else {
            loadGroupByValues(groupBy, groupByValue, displayIgnoredDevices);
        }
    }

    function colsToQueryString(cols) {
        var colsQueryString = '';
        var comma = ''
        Ext.each(cols, function (col) {
            colsQueryString = colsQueryString + String.format('{0}{1}',
            comma,
            col);

            comma = ',';
        });
        return colsQueryString;
    }

    function getColumnsToExport() {
        var colsToExport = [];

        var cols = grid.getColumnModel().getColumnsBy(function (col) {
            return !col.hidden;
        });

        Ext.each(cols, function (col) {
            if (col.dataIndex != '')
                colsToExport.push(col.dataIndex);
        });

        return colsToExport;
    }

    function exportEosData(method) {
        var obj = getDataProvider();

        var colsToExport = getColumnsToExport();
        var colsToExportQueryString = colsToQueryString(colsToExport);

        var params = String.format('method={0}&searchTerm={1}&sortColumn={2}&sortDirection={3}&groupByProperty={4}&groupByValue={5}&displayIgnoredDevices={6}&{7}&colsToExport={8}',
                                    method,
                                    obj.searchTerm,
                                    obj.sortColumn,
                                    obj.sortDirection,
                                    obj.groupBy,
                                    obj.groupByValue,
                                    obj.displayIgnoredDevices,
                                    obj.filters,
                                    colsToExportQueryString);
        $.download('/Orion/NCM/Resources/Eos/EosDataExporter.ashx', params);
    }

    var fireCheckEvent = true;
    function DisplayIgnoredDevices(value) {

        var settingValue = value ? "on" : "off";
        ORION.Prefs.save('DisplayIgnoredDevices', settingValue);

        var obj = getGroupByParams();
        var groupBy = obj.groupBy;
        var groupByValue = obj.groupByValue;
        var displayIgnoredDevices = obj.displayIgnoredDevices;

        LoadGridStore(groupBy, groupByValue, displayIgnoredDevices);
    }

    function LoadGridStore(groupBy, groupByValue, displayIgnoredDevices) {
        if (groupBy == '') {
            refreshObjects("", "", "", displayIgnoredDevices);
        }
        else {
            loadGroupByValues(groupBy, groupByValue, displayIgnoredDevices);
        }
    }

    var clearFiltersForHiddenColumnHandler = function (colModel, colIndex, hidden) {
        if (hidden) {
            var needReloadGridStore = false;

            var dataIndex = colModel.getDataIndex(colIndex);
            var filterData = grid.filters.getFilterData();
            for (var i = 0; i < filterData.length; i++) {
                var field = filterData[i].field;
                if (dataIndex == field) {
                    needReloadGridStore = true;

                    var comparison = (filterData[i].data.comparison == undefined) ? "" : filterData[i].data.comparison;
                    var filter = grid.filters.getFilter(dataIndex);
                    switch (filter.type) {
                        case "eosdate":
                            filter.disableFilter(comparison);
                            break;
                        case "string":
                            filter.disableFilter();
                            break;
                        default:
                            filter.active = false;
                            break;
                    }
                }
            }

            if (needReloadGridStore) {
                grid.getStore().lastOptions.params.start = 0;
                grid.getStore().reload();
            }
        }
    }

    var gridColumnModelChangedHandler = function () {
        var gridCM = grid.getColumnModel();
        var columnModel = SW.NCM.GridColumnsToJson(gridCM);
        ORION.callWebService("/Orion/NCM/Services/EoLEoS.asmx",
                            "SplitGridColumnModel", { columnModel: columnModel },
                            function (list) {
                                var count = 0;
                                Ext.each(list, function (item) {
                                    var settingName = String.format('Columns_{0}', count++);
                                    ORION.Prefs.save(settingName, item);
                                });
                                ORION.Prefs.save('SplitCount', count);
                            });

    }

    var generateConfig = function (cpColumnModel, selectorModel) {
        var config = [];
        // increase this for each new column
        var mappingIndex = 14; //no need to show in grid VendorIcon (mapping index: 9), SystemOID (mapping index: 10) columns
        var header;

        var filterEmptyText = '@{R=NCM.Strings;K=WEBJS_VK_219;E=js}';
        var columnHeaderFormat = '<img class="filter-icon" onclick="ShowFilter(event, \'{0}\');" src="/Orion/js/extjs/resources/images/default/s.gif" alt="" style="vertical-align: middle; padding-top:1px; height: 16px; width:16px"/><span style="vertical-align: middle;">{1}</span>&nbsp;';

        config.columnModel = [];
        config.fieldMapping = [];
        config.filters = [];
        config.orderByColumn = 'NodeCaption';

        config.columnModel.push(selectorModel);

        config.columnModel.push({ header: 'NodeID', width: 10, hidden: true, hideable: false, dataIndex: 'NodeID' });
        config.fieldMapping.push({ name: 'NodeID', mapping: 0 });

        SW.NCM.ColumnDisplayName['NodeCaption'] = '@{R=NCM.Strings;K=WEBJS_VK_43;E=js}';
        header = String.format(columnHeaderFormat, 'NodeCaption', '@{R=NCM.Strings;K=WEBJS_VK_43;E=js}');
        config.columnModel.push({ header: header, width: 200, sortable: true, filterable: true, dataIndex: 'NodeCaption', renderer: renderNodeName });
        config.fieldMapping.push({ name: 'NodeCaption', mapping: 1 });
        config.filters.push({ type: 'string', dataIndex: 'NodeCaption', emptyText: filterEmptyText });

        SW.NCM.ColumnDisplayName['AgentIP'] = '@{R=NCM.Strings;K=WEBJS_VK_44;E=js}';
        header = String.format(columnHeaderFormat, 'AgentIP', '@{R=NCM.Strings;K=WEBJS_VK_44;E=js}');
        config.columnModel.push({ header: header, width: 200, sortable: true, dataIndex: 'AgentIP' });
        config.fieldMapping.push({ name: 'AgentIP', mapping: 2 });
        config.filters.push({ type: 'string', dataIndex: 'AgentIP', emptyText: filterEmptyText });

        SW.NCM.ColumnDisplayName['EndOfSupport'] = '@{R=NCM.Strings;K=WEBJS_VM_94;E=js}';
        header = String.format(columnHeaderFormat, 'EndOfSupport', '@{R=NCM.Strings;K=WEBJS_VM_94;E=js}');
        config.columnModel.push({ header: header, width: 200, sortable: true, dataIndex: 'EndOfSupport', renderer: renderEoSEoLColumns });
        config.fieldMapping.push({ name: 'EndOfSupport', mapping: 3 });
        config.filters.push({
            type: 'eosdate',
            dataIndex: 'EndOfSupport',
            next3monthsText: '@{R=NCM.Strings;K=WEBJS_VK_222;E=js}',
            next6monthsText: '@{R=NCM.Strings;K=WEBJS_VK_223;E=js}',
            next1yearText: '@{R=NCM.Strings;K=WEBJS_VK_224;E=js}',
            next2yearsText: '@{R=NCM.Strings;K=WEBJS_VK_225;E=js}',
            next3yearsText: '@{R=NCM.Strings;K=WEBJS_VK_226;E=js}',
            afterText: '@{R=NCM.Strings;K=WEBJS_VK_220;E=js}',
            beforeText: '@{R=NCM.Strings;K=WEBJS_VK_221;E=js}',
            eostype0Text: '@{R=NCM.Strings;K=WEBJS_VM_85;E=js}',
            eostype4Text: '@{R=NCM.Strings;K=WEBJS_VM_89;E=js}',
            menuItems: ['next3months', 'next6months', 'next1year', 'next2years', 'next3years', '-', 'after', 'before', '-', 'eostype0', 'eostype4']
        });

        SW.NCM.ColumnDisplayName['EndOfSales'] = '@{R=NCM.Strings;K=WEBJS_VM_93;E=js}';
        header = String.format(columnHeaderFormat, 'EndOfSales', '@{R=NCM.Strings;K=WEBJS_VM_93;E=js}');
        config.columnModel.push({ header: header, width: 200, sortable: true, dataIndex: 'EndOfSales', renderer: renderEoSEoLColumns });
        config.fieldMapping.push({ name: 'EndOfSales', mapping: 4 });
        config.filters.push({
            type: 'eosdate',
            dataIndex: 'EndOfSales',
            next3monthsText: '@{R=NCM.Strings;K=WEBJS_VK_222;E=js}',
            next6monthsText: '@{R=NCM.Strings;K=WEBJS_VK_223;E=js}',
            next1yearText: '@{R=NCM.Strings;K=WEBJS_VK_224;E=js}',
            next2yearsText: '@{R=NCM.Strings;K=WEBJS_VK_225;E=js}',
            next3yearsText: '@{R=NCM.Strings;K=WEBJS_VK_226;E=js}',
            afterText: '@{R=NCM.Strings;K=WEBJS_VK_220;E=js}',
            beforeText: '@{R=NCM.Strings;K=WEBJS_VK_221;E=js}',
            eostype0Text: '@{R=NCM.Strings;K=WEBJS_VM_85;E=js}',
            eostype4Text: '@{R=NCM.Strings;K=WEBJS_VM_89;E=js}',
            menuItems: ['next3months', 'next6months', 'next1year', 'next2years', 'next3years', '-', 'after', 'before', '-', 'eostype0', 'eostype4']
        });

        SW.NCM.ColumnDisplayName['EndOfSoftware'] = '@{R=NCM.Strings;K=WEBJS_DP_01;E=js}';
        header = String.format(columnHeaderFormat, 'EndOfSoftware', '@{R=NCM.Strings;K=WEBJS_DP_01;E=js}');
        config.columnModel.push({ header: header, width: 200, sortable: true, dataIndex: 'EndOfSoftware', renderer: renderEoSEoLColumns });
        config.fieldMapping.push({ name: 'EndOfSoftware', mapping: 11 });
        config.filters.push({
            type: 'eosdate',
            dataIndex: 'EndOfSoftware',
            next3monthsText: '@{R=NCM.Strings;K=WEBJS_VK_222;E=js}',
            next6monthsText: '@{R=NCM.Strings;K=WEBJS_VK_223;E=js}',
            next1yearText: '@{R=NCM.Strings;K=WEBJS_VK_224;E=js}',
            next2yearsText: '@{R=NCM.Strings;K=WEBJS_VK_225;E=js}',
            next3yearsText: '@{R=NCM.Strings;K=WEBJS_VK_226;E=js}',
            afterText: '@{R=NCM.Strings;K=WEBJS_VK_220;E=js}',
            beforeText: '@{R=NCM.Strings;K=WEBJS_VK_221;E=js}',
            eostype0Text: '@{R=NCM.Strings;K=WEBJS_VM_85;E=js}',
            eostype4Text: '@{R=NCM.Strings;K=WEBJS_VM_89;E=js}',
            menuItems: ['next3months', 'next6months', 'next1year', 'next2years', 'next3years', '-', 'after', 'before', '-', 'eostype0', 'eostype4']
        });

        config.columnModel.push({ header: 'EosType', width: 10, hidden: true, hideable: false, dataIndex: 'EosType' });
        config.fieldMapping.push({ name: 'EosType', mapping: 5 });

        SW.NCM.ColumnDisplayName['EosComments'] = '@{R=NCM.Strings;K=WEBJS_IC_01;E=js}';
        header = String.format(columnHeaderFormat, 'EosComments', '@{R=NCM.Strings;K=WEBJS_IC_01;E=js}');
        config.columnModel.push({ header: header, width: 300, sortable: false, hidden: false, dataIndex: 'EosComments' });
        config.fieldMapping.push({ name: 'EosComments', mapping: 6 });
        config.filters.push({ type: 'string', dataIndex: 'EosComments', emptyText: filterEmptyText });

        SW.NCM.ColumnDisplayName['MachineType'] = '@{R=NCM.Strings;K=WEBJS_VK_53;E=js}';
        header = String.format(columnHeaderFormat, 'MachineType', '@{R=NCM.Strings;K=WEBJS_VK_53;E=js}');
        config.columnModel.push({ header: header, width: 200, sortable: true, dataIndex: 'MachineType' });
        config.fieldMapping.push({ name: 'MachineType', mapping: 7 });
        config.filters.push({ type: 'string', dataIndex: 'MachineType', emptyText: filterEmptyText });

        SW.NCM.ColumnDisplayName['OSVersion'] = '@{R=NCM.Strings;K=WEBJS_VK_206;E=js}';
        header = String.format(columnHeaderFormat, 'OSVersion', '@{R=NCM.Strings;K=WEBJS_VK_206;E=js}');
        config.columnModel.push({ header: header, width: 300, sortable: true, dataIndex: 'OSVersion' });
        config.fieldMapping.push({ name: 'OSVersion', mapping: 8 });
        config.filters.push({ type: 'string', dataIndex: 'OSVersion', emptyText: filterEmptyText });

        SW.NCM.ColumnDisplayName['EntityPhysical_Model'] = '@{R=NCM.Strings;K=WEBJS_DP_03;E=js}';
        header = String.format(columnHeaderFormat, 'EntityPhysical_Model', '@{R=NCM.Strings;K=WEBJS_DP_03;E=js}');
        config.columnModel.push({ header: header, width: 200, sortable: true, dataIndex: 'EntityPhysical_Model' });
        config.fieldMapping.push({ name: 'EntityPhysical_Model', mapping: 12 });
        config.filters.push({ type: 'string', dataIndex: 'EntityPhysical_Model', emptyText: filterEmptyText });

        SW.NCM.ColumnDisplayName['ReplacementPartNumber'] = '@{R=NCM.Strings;K=WEBJS_DP_02;E=js}';
        header = String.format(columnHeaderFormat, 'ReplacementPartNumber', '@{R=NCM.Strings;K=WEBJS_DP_02;E=js}');
        config.columnModel.push({ header: header, width: 200, sortable: true, dataIndex: 'ReplacementPartNumber' });
        config.fieldMapping.push({ name: 'ReplacementPartNumber', mapping: 13 });
        config.filters.push({ type: 'string', dataIndex: 'ReplacementPartNumber', emptyText: filterEmptyText });

        Ext.each(cpColumnModel, function (column) {
            SW.NCM.ColumnDisplayName[column] = column.PropertyName;
            if (column.PropertyType.toUpperCase() == "SYSTEM.STRING") {
                header = String.format(columnHeaderFormat, column.PropertyName, column.PropertyName);
            }
            else {
                header = column.PropertyName;
            }
            if (column.PropertyType.toUpperCase() == "SYSTEM.DATETIME") {
                config.columnModel.push({ header: header, width: 150, sortable: true, hidden: true, dataIndex: column.PropertyName, renderer: renderDateTime });
            }
            else {
                config.columnModel.push({ header: header, width: 150, sortable: true, hidden: true, dataIndex: column.PropertyName });
            }
            config.fieldMapping.push({ name: column.PropertyName, mapping: mappingIndex++ });
            if (column.PropertyType.toUpperCase() == "SYSTEM.STRING") {
                config.filters.push({ type: 'string', dataIndex: column.PropertyName, emptyText: filterEmptyText });
            }
        });

        return config;
    };

    return {
        init: function () {

            if ($("[id$='_ContentContainer']").length == 0)
                return false;

            $("#Grid").click(function (e) {

                var obj = $(e.target);

                if (obj.hasClass('nodeName')) {
                    nodeNameClick(obj);
                    return false;
                }
            });

            if (SW.NCM.IsEosRunning) {
                ScheduleRenewMatchesCheck()
            }

            var selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", updateToolbarButtons);

            var config = generateConfig(SW.NCM.CPColumnModel, selectorModel);

            var dataStore = new ORION.WebServiceStore(
                            "/Orion/NCM/Services/EoLEoS.asmx/GetEoLEoSData",
                            config.fieldMapping,
                            config.orderByColumn
                            );

            dataStore.on("beforeload", function (that, options) { adjustDataStoreQuery(that, options) });

            checkBox = new Ext.form.Checkbox({
                boxLabel: '@{R=NCM.Strings;K=WEBJS_VK_210;E=js}',
                listeners: {
                    render: function (that) {
                        var displayIgnoredDevices = ORION.Prefs.load("DisplayIgnoredDevices") || "off";
                        fireCheckEvent = false;
                        that.setValue(displayIgnoredDevices);
                    },
                    check: function (that, value) {
                        if (fireCheckEvent) {
                            DisplayIgnoredDevices(value);
                        }
                    }
                }
            });

            filters = new Ext.ux.grid.GridFilters({
                menuFilterText: 'Filters',
                autoReload: true,
                encode: false,
                local: false,
                filters: config.filters
            });

            var pagingToolbar = new Ext.PagingToolbar({
                store: dataStore,
                pageSize: SW.NCM.PageSize,
                plugins: [filters],
                displayInfo: true,
                displayMsg: '@{R=NCM.Strings;K=WEBJS_VK_41;E=js}',
                emptyMsg: '@{R=NCM.Strings;K=WEBJS_VK_42;E=js}',
                items: [
                        '-',
                        {
                            text: '@{R=NCM.Strings;K=WEBJS_VM_92;E=js}',
                            handler: clearAllFiltersHandler
                        }
                        , '-',
                            checkBox
                        ]
            });

            grid = new Ext.grid.GridPanel({
                id: 'gridControl',
                store: dataStore,
                columns: config.columnModel,
                sm: selectorModel,
                layout: 'fit',
                autoScroll: 'true',
                loadMask: true,
                width: 750,
                height: 500,
                stripeRows: true,
                plugins: [filters],
                tbar: [
                {
                    id: 'ExportTo',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_105;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_234;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_export.gif',
                    cls: 'x-btn-text-icon',
                    menu: {
                        items: [{ iconCls: 'mnu-exportXLS', text: '@{R=NCM.Strings;K=WEBJS_VK_107;E=js}', handler: function () { exportEosData('xls') } },
			                    { iconCls: 'mnu-exportCSV', text: '@{R=NCM.Strings;K=WEBJS_VK_108;E=js}', handler: function () { exportEosData('csv') } }]
                    }
                }, '-', {
                    id: 'AssignBtn',
                    text: '@{R=NCM.Strings;K=WEBJS_VM_90;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_203;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_addsnippet.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { AssignBtnHandler(grid.getSelectionModel().getSelections()); }
                }, '-', {
                    id: 'RenewMatchesBtn',
                    text: '@{R=NCM.Strings;K=WEBJS_VM_95;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_204;E=js}',
                    icon: '/Orion/NCM/Resources/images/PolicyIcons/update_all.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        if (SW.NCM.IsDemoServer) {
                            demoAction('NCM_Eos_RefreshSuggestedDates', $('#RenewMatchesBtn'));
                        }
                        else {
                            RenewMatchesBtnHandler();
                        }
                    }
                }, SW.NCM.IsAdminOrEngineer ? '-' : '', {
                    id: 'IgnoreDevicesBtn',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_211;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_212;E=js}',
                    icon: '/Orion/NCM/Resources/images/ignore.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        if (SW.NCM.IsDemoServer) {
                            demoAction('NCM_Eos_IgnoreDevices', $('#IgnoreDevicesBtn'));
                        }
                        else {
                            IgnoreDevicesBtnHandler(grid.getSelectionModel().getSelections());
                        }
                    }
                }, {
                    id: 'RemoveFromIgnoredDevicesBtn',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_213;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_214;E=js}',
                    icon: '/Orion/NCM/Resources/images/icon_delete.png',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        if (SW.NCM.IsDemoServer) {
                            demoAction('NCM_Eos_RemoveFromIgnoredDevices', $('#RemoveFromIgnoredDevicesBtn'));
                        }
                        else {
                            RemoveFromIgnoredDevicesBtnHandler(grid.getSelectionModel().getSelections());
                        }
                    }
                }, '-', {
                    id: 'DeleteEosDataBtn',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_199;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_200;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_delete.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        if (SW.NCM.IsDemoServer) {
                            demoAction('NCM_Eos_DeleteEosData', $('#DeleteEosDataBtn'));
                        }
                        else {
                            Ext.Msg.confirm('@{R=NCM.Strings;K=WEBJS_VK_199;E=js}',
                            '@{R=NCM.Strings;K=WEBJS_VK_201;E=js}',
                            function (btn, text) {
                                if (btn == 'yes') {
                                    DeleteEosDataBtnHandler(grid.getSelectionModel().getSelections());
                                }
                            });
                        }
                    }
                }, '->', {
                    id: 'txtSearch',
                    xtype: 'textfield',
                    emptyText: '@{R=NCM.Strings;K=Resources_EosMatching_SearchPlaceholder;E=js}',
                    width: 200,
                    enableKeyEvents: true,
                    listeners: {
                        keypress: function (obj, evnt) {
                            if (evnt.keyCode == 13) {
                                doSearch();
                            }
                        }
                    }
                }, {
                    id: 'Clean',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_30;E=js}',
                    iconCls: 'ncm_clear-btn',
                    cls: 'x-btn-icon',
                    hidden: true,
                    handler: function () { doClean(); }
                }, {
                    id: 'Search',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_31;E=js}',
                    iconCls: 'ncm_search-btn',
                    cls: 'x-btn-icon',
                    handler: function () { doSearch(); }
                }],

                bbar: pagingToolbar

            });

            grid.getStore().on('beforeload', function (s, params) {
                reconfigureFilterNotifications();
                reconfigureEosDeleteNotification();
                reconfigureEosNotification();
            });

            var gridCM = SW.NCM.GetDefaultColumnModelIfNeeded(SW.NCM.GridColumnModel);
            SW.NCM.PersonalizeGrid(grid, gridCM);

            grid.on("columnresize", gridColumnModelChangedHandler);
            grid.getColumnModel().on("columnmoved", gridColumnModelChangedHandler);
            grid.getColumnModel().on("hiddenchange", function (colModel, colIndex, hidden) {
                gridColumnModelChangedHandler();
                clearFiltersForHiddenColumnHandler(colModel, colIndex, hidden);
            });

            grid.render('Grid');
            gridResize();

            updateToolbarButtons();
            groupByControlInitializer();

            var needToUseFilterData = (SW.NCM.FilterData != null);
            var groupBy = findGroupByValue(needToUseFilterData ? '' : ORION.Prefs.load("GroupBy") || '');
            var groupByValue = needToUseFilterData ? '' : Ext.util.Format.htmlEncode(ORION.Prefs.load("GroupByValue") || '');

            combo.setValue(groupBy);

            var displayIgnoredDevices = checkBox.getValue();
            fireCheckEvent = true;
            if (groupBy == 'EosType') {
                checkBox.hide(); //hide 'Display ignored devices' checkbox in case nodes are grouped by Eos Type
                displayIgnoredDevices = true; //always display ignored devices in case nodes are grouped by Eos Type
            }
            else {
                checkBox.show();
            }

            updateIgnoredButtons(groupBy, groupByValue);

            if (needToUseFilterData) {
                setFilterData(SW.NCM.FilterData);
            }

            LoadGridStore(groupBy, groupByValue, displayIgnoredDevices);

            initToolTip();

            $(window).resize(function() {
                gridResize();
            });

            // highlight the search term (if we have one)            
            grid.getView().on("refresh", function () {
                var search = currentSearchTerm;
                if ($.trim(search).length == 0) return;

                var columnsToHighlight = SW.NCM.ColumnIndexByDataIndex(grid.getColumnModel(), ['NodeCaption', 'AgentIP']);
                Ext.each(columnsToHighlight, function (columnNumber) {
                    SW.NCM.highlightSearchText("#Grid .x-grid3-body .x-grid3-td-" + columnNumber, search);
                });
            });
        }
    }
} ();

Ext.onReady(SW.NCM.EoLEoS.init, SW.NCM.EoSEoL);

jQuery.download = function (url, data, method) {
    //url and data options required
    if (url && data) {
        //data can be string of parameters or array/object
        data = typeof data == 'string' ? data : jQuery.param(data);
        //split params into form inputs
        var inputs = '';
        jQuery.each(data.split('&'), function () {
            var pair = this.split('=');
            inputs += '<input type="hidden" name="' + pair[0] + '" value="' + pair[1] + '" />';
        });
        //send request
        jQuery('<form action="' + url + '" method="' + (method || 'post') + '">' + inputs + '</form>')
		.appendTo('body').submit().remove();
    };
};
