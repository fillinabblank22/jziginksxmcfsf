﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="EosMatching.aspx.cs" Inherits="Orion_NCM_Resources_Eos_EosMatching" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="ncm1" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" Assembly="OrionWeb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmHeadPlaceHolder" Runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
   
    <orion:Include ID="Include5" runat="server" File="OrionCore.js" />

    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/PolicyReports/PolicyReport.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/JavaScript/Ext_UX/gridfilters/css/GridFilters.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/Settings/Settings.css" />

    <script type="text/javascript"  src="/Orion/NCM/JavaScript/Ext_UX/gridfilters/filter/Filter.js"> </script>
    <script type="text/javascript"  src="/Orion/NCM/JavaScript/Ext_UX/gridfilters/filter/EosdateFilter.js"></script>
    <script type="text/javascript"  src="/Orion/NCM/JavaScript/Ext_UX/gridfilters/filter/StringFilter.js"></script>
    <script type="text/javascript"  src="/Orion/NCM/JavaScript/Ext_UX/gridfilters/GridFilters.js"></script>
    
    <script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippetsHelper.js"></script>
    <script type="text/javascript" src="EosViewer.js" ></script>

     <script type="text/javascript">

        SW.NCM.IsDemoServer = <%=SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer.ToString().ToLowerInvariant() %>;

	    SW.NCM.GroupByData = <%=CreateGroupByData() %>;
        SW.NCM.IsAdminOrEngineer = <%=this.IsAdminOrEngineer %>;
        SW.NCM.IsEosRunning = <%=this.IsEosRunning.ToString().ToLowerInvariant()%>;
        SW.NCM.CPColumnModel = <%=CreateCPColumnModel() %>;
        SW.NCM.FilterData = <%=CreateFilterData() %>;
        SW.NCM.GridColumnModel = <%=LoadGridColumnModel() %>;
    </script>

    <style type="text/css">
        #filterHelpLink { color:#336699; text-decoration:underline; font-size:11px; }
        
        #selectAll a { color: Red; text-decoration: underline; }
        .select-all-page { background-color: #ffffcc; }
        
        .ncm-filter { margin-right: 5px;  background: #fffdcc; border: 1px solid #eaca7f; border-radius: 5px; display: inline-block; position: relative; zoom: 1; padding: 5px 6px 6px 26px; color: #000; font-size: 11px; }
        .ncm-filter .ncm-filter-icon { position: absolute; left: 4px; top: 4px; height: 16px; width: 16px; background: url(/Orion/NCM/Resources/images/filter_applied.png) top left no-repeat; }

        .x-menu-list img.filter-icon { display: none; }
        .x-grid3-hd-row td.ux-filtered-column img.filter-icon { background-image: url(/Orion/NCM/Resources/images/filter_applied.png); }
        .x-grid3-hd-row td.ux-filtered-column img.filter-icon:hover { background-image: url(/Orion/NCM/Resources/images/filter_applied_hover.png); }
        .x-grid3-hd-row img.filter-icon { background-image: url(/Orion/NCM/Resources/images/filter.png); }
        .x-grid3-hd-row img.filter-icon:hover { background-image: url(/Orion/NCM/Resources/images/filter_hover.png); }
        
        .eos-suggestion { margin-right: 5px;  background: #fffdcc; border: 1px solid #eaca7f; border-radius: 5px; display: inline-block; position: relative; zoom: 1; padding: 5px 6px 6px 26px; color: #000; font-size: 12px; }
        .eos-suggestion .eos-suggestion-icon { position: absolute; left: 4px; top: 4px; height: 16px; width: 16px; background: url(/Orion/NCM/Resources/images/lightbulb_tip_16x16.gif) top left no-repeat; }
        .eos-success { margin-right: 5px;  background: #daf7cc; border: 1px solid #00a000; border-radius: 5px; display: inline-block; position: relative; zoom: 1; padding: 5px 6px 6px 26px; color: #00a000; font-size: 12px; }
        .eos-success .eos-success-icon { position: absolute; left: 4px; top: 4px; height: 16px; width: 16px; background: url(/Orion/NCM/Resources/images/icon_ok.gif) top left no-repeat; }
    </style>
    <ncm1:ExtLocalDateTimePattern ID="ExtLocalDateTimePattern1" runat="server"/>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMPHEndSupportEndSales" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
    <%foreach (string setting in new string[] { @"GroupByValue", @"GroupBy", @"DisplayIgnoredDevices" }) { %>
		<input type="hidden" name="NCM_Eos_<%=setting%>" id="NCM_Eos_<%=setting%>" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get($@"NCM_Eos_{setting}")%>' />		
	<%}%>
    
    <!-- demoedit -->
    <%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
		<div id="isDemoMode" style="display:none;"></div>
	<%}%>
    <!-- / demoedit -->

    <asp:Panel id="ContentContainer" runat="server">
        <div style="padding:10px;">
            <div style="padding-bottom:10px;">
                <%=string.Format(Resources.NCMWebContent.WEBDATA_VK_885, @"<div>", @"</div>") %>
            </div>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="2" style="padding-top:10px;">
                        <div id="eosDeleteNotification"></div>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:5px;">
                       <div id="eosNotification"></div>
                    </td>
                    <td valign="bottom" align="right" style="padding-top:5px;" nowrap="nowrap">
                        <a id="filterHelpLink" href="<%=FilterHelpUrlFragment %>" target="_blank">
                            <img alt="" style="vertical-align:bottom" src="/Orion/NCM/Resources/images/Ninja_16x16.png" /><span style="padding-left:5px;"><%=Resources.NCMWebContent.WEBDATA_VK_893%></span>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-top:5px;">
                        <div id="filterNotifications" class="ncm-filter-notifications"></div>
                    </td>
                </tr>
            </table>
            <div>&nbsp;</div>
            <div style="border: solid 1px #dcdbd7; padding: 10px; background-color: #fff;">
                <table class="ExistingElements" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="2">
                            <div id="selectAll" style="padding:3px"></div>
                        </td>
                    </tr>
                    <tr valign="top" align="left">
                        <td style="width: 220px;">
                            <div class="ElementGrouping">
                                <div class="ncm_GroupSection x-panel-header x-toolbar x-small-editor">
                                    <div><%= Resources.NCMWebContent.WEBDATA_VK_80 %></div>
                                    <div id="GroupBy" style="padding: 2px 0px 3px 0px;"></div>
                                </div>
                                <ul class="GroupItems" style="width:220px;">
                                </ul>
                            </div>
                        </td>
                        <td id="gridCell">
                            <div id="Grid" />
                        </td>
                    </tr>
                </table>
                <div id="originalQuery">
                </div>
                <div id="test">
                </div>
                <pre id="stackTrace"></pre>
            </div>
        </div>
    </asp:Panel>
    <div style="padding:10px;" id="divError" runat="server"/>
</asp:Content>

