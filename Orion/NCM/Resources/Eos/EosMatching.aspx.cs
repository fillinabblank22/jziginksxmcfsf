﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.Web.Script.Serialization;
using SolarWinds.NCM.Common.Help;
using SolarWinds.Orion.Core.Common;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;
using SolarWinds.NCM.Contracts.InformationService;

public partial class Orion_NCM_Resources_Eos_EosMatching : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_VK_884;
        
        var validator = new SWISValidator();
        validator.ContentContainer = ContentContainer;
        validator.RedirectIfValidationFailed = true;
        divError.Controls.Add(validator);
    }

    public string CreateGroupByData()
    {
        var res = @"[['', '" + Resources.NCMWebContent.WEBDATA_VM_121 + @"'],"
                        + @"['Vendor', '" + Resources.NCMWebContent.WEBDATA_VK_278 + @"'],"
                        + @"['MachineType', '" + Resources.NCMWebContent.WEBDATA_VK_279 + @"'],"
                        + @"['Status','" + Resources.NCMWebContent.WEBDATA_VM_123 + @"'],"
                        + @"['SysLocation','" + Resources.NCMWebContent.Resources_EosMatching_SysLocation + @"'],"
                        + @"['SysContact','" + Resources.NCMWebContent.Resources_EosMatching_SysContact + @"'],"
                        + @"['OSImage','" + Resources.NCMWebContent.Resources_EosMatching_OSImage + @"'],"
                        + @"['OSVersion','" + Resources.NCMWebContent.Resources_EosMatching_OSVersion + @"'],"
                        + @"['EosType','" + Resources.NCMWebContent.Resources_EosMatching_EosType + @"'],"
                        + @"['EntityPhysical_Model','" + Resources.NCMWebContent.WEBDATA_DP_09 + @"']"
                        + @"{0}"
                        + @"]";

        var cpData = new StringBuilder();
        var cps = CustomPropertyHelper.GetCustomProperties().ToArray();
        foreach (var prop in cps)
        {
            cpData.AppendFormat(@",['{0}','{0}']", prop);
        }
        return string.Format(res, cpData.ToString());
    }

    public string CreateCPColumnModel()
    {
        var customProperties = CustomPropertyMgr.GetCustomPropertiesForTable(@"Nodes").ToList();
        if (customProperties.Count > 0)
        {
            var sb = new StringBuilder();
            sb.Append(@"[");
            var comma = string.Empty;
            foreach (var customProperty in customProperties)
            {
                if (!NCMNode.cRegularNCMColumnList.Any(s => s.Equals(customProperty.PropertyName, StringComparison.CurrentCultureIgnoreCase)) && !NCMNode.cSWISRelationNCMColumnList.Any(s => s.Equals(customProperty.PropertyName, StringComparison.CurrentCultureIgnoreCase)))
                {
                    sb.AppendFormat(@"{0}{1}'PropertyName':'{2}','PropertyType':'{3}'{4}", comma, @"{", customProperty.PropertyName, customProperty.PropertyType.ToString(), @"}");
                    comma = @",";
                }
            }
            sb.Append(@"]");
            return sb.ToString();
        }

        return @"[]";
    }

    public string CreateFilterData()
    {
        Dictionary<string, object> result = null;

        if (!string.IsNullOrEmpty(Page.Request.QueryString[@"field"]) &&
            !string.IsNullOrEmpty(Page.Request.QueryString[@"type"]) &&
            (!string.IsNullOrEmpty(Page.Request.QueryString[@"comparison"]) || !string.IsNullOrEmpty(Page.Request.QueryString[@"value"])))
        {
            result = new Dictionary<string, object>();

            result[@"field"] = HttpUtility.HtmlEncode(Page.Request.QueryString[@"field"]);
            result[@"type"] = HttpUtility.HtmlEncode(Page.Request.QueryString[@"type"]);
            result[@"comparison"] = string.IsNullOrEmpty(Page.Request.QueryString[@"comparison"]) ? string.Empty : HttpUtility.HtmlEncode(Page.Request.QueryString[@"comparison"]);
            result[@"value"] = string.IsNullOrEmpty(Page.Request.QueryString[@"value"]) ? string.Empty : HttpUtility.HtmlEncode(Page.Request.QueryString[@"value"]);
        }

        var jsSerializer = new JavaScriptSerializer();
        return jsSerializer.Serialize(result);
    }

    public string LoadGridColumnModel()
    {
        string settingName;
        string settingValue;

        int splitCount = Convert.ToInt16(SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get(@"NCM_Eos_SplitCount"));

        var sb = new StringBuilder();
        for (var i = 0; i < splitCount; i++)
        {
            settingName = $@"NCM_Eos_Columns_{i}";
            settingValue = SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get(settingName);

            sb.Append(settingValue);
        }
        
        return sb.Length > 0 ? sb.ToString() : @"[]";
    }

    public string IsAdminOrEngineer
    {
        get
        {
            return (SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ENGINEER) 
                || SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR)).ToString().ToLowerInvariant();
        }
    }

    public bool IsEosRunning
    {
        get
        {
            try
            {
                var isLayer = new ISWrapper();
                using (var proxy = isLayer.Proxy)
                {
                    return proxy.Cirrus.Eos.IsRefreshingAll();
                }
            }
            catch
            {
                return false;
            }
        }
    }

    public string FilterHelpUrlFragment
    {
        get
        {
            var helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
            return helpHelperWrap.GetHelpUrl("OrionNCMPHFilterTableColumnValues");
        }
    }

    private DataTable ExecuteSWQL(string swql)
    {
        try
        {
            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                var dt = proxy.Query(swql);
                return dt;
            }
        }
        catch
        {

        }

        return null;
    }
}