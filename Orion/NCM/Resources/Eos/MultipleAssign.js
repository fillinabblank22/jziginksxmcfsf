﻿var toolTipHigh;
var toolTipMedium;
var toolTipLow;
var toolTipConfirmed;
var activeToolTip;

function pageLoad() {
    $("p[CertaintyKey]").mouseenter(handlerIn).mouseleave(handlerOut);
    initToolTips();
}

function ShowTooltip(toolTip, el) {    
    activeToolTip = toolTip;
    toolTip.initTarget(el);        
    toolTip.show();
}

function handlerOut() {
    if (activeToolTip != null)
        activeToolTip.hide();
}

function handlerIn() {
    var value = this.getAttribute('CertaintyKey');
    var toolTip;
    switch (value) {
        case "Low": toolTip = toolTipLow; break;
        case "Medium": toolTip = toolTipMedium; break;
        case "High": toolTip = toolTipHigh; break;
        case "Confirmed": toolTip = toolTipConfirmed; break;
        default: return;
    }
    ShowTooltip(toolTip, this);    
}

function initToolTips() {
    toolTipHigh = new Ext.ToolTip({
        width: 255,
        anchor: 'top',           
        html: ' \
            <div style="padding:0px;"> \
                    <table style="width: 100%; border-collapse: collapse; height: 150px;">\
                        <tr style="height: 25px; background-color: #daf7cc; font-weight: bold;">\
                            <td>\
                                <p style="text-align:center">@{R=NCM.Strings;K=WEBJS_VK_316;E=js}</p>\
                            </td>\
                        </tr>\
                        <tr style="vertical-align: top;">\
                            <td><p>@{R=NCM.Strings;K=WEBJS_IA_24;E=js}</p></td>\
                        </tr>\
                </table>\
            </div>',
        trackMouse: true
    });
    toolTipHigh.initTarget('container');
    toolTipHigh.disable();

    toolTipMedium = new Ext.ToolTip({
        width: 255,
        anchor: 'top',       
        html: ' \
            <div style="padding:0px;"> \
                    <table style="width: 100%; border-collapse: collapse; height: 150px;">\
                        <tr style="height: 25px; background-color: #fffdcc; font-weight: bold;">\
                            <td>\
                                <p style="text-align:center">@{R=NCM.Strings;K=WEBJS_VK_317;E=js}</p>\
                            </td>\
                        </tr>\
                        <tr style="vertical-align: top;">\
                            <td><p>@{R=NCM.Strings;K=WEBJS_IA_25;E=js}</p></td>\
                        </tr>\
                </table>\
            </div>',
        trackMouse: true
    });
    toolTipMedium.initTarget('container');
    toolTipMedium.disable();

    toolTipLow = new Ext.ToolTip({
        width: 260,
        anchor: 'top',            
        html: ' \
            <div style="padding:0px;"> \
                    <table style="width: 100%; border-collapse: collapse; height: 150px;">\
                        <tr style="height: 25px; background-color: #ffdcdc; font-weight: bold;">\
                            <td>\
                                <p style="text-align:center">@{R=NCM.Strings;K=WEBJS_VK_318;E=js}</p>\
                            </td>\
                        </tr>\
                        <tr style="vertical-align: top;">\
                            <td><p>@{R=NCM.Strings;K=WEBJS_IA_26;E=js}</p></td>\
                        </tr>\
                </table>\
            </div>',
        trackMouse: true
    });
    toolTipLow.initTarget('container');
    toolTipLow.disable();

    toolTipConfirmed = new Ext.ToolTip({
        width: 260,
        anchor: 'top',
        html: ' \
            <div style="padding:0px;"> \
                    <table style="width: 100%; border-collapse: collapse; height: 150px;">\
                        <tr style="height: 25px; background-color: #b4ffff; font-weight: bold;">\
                            <td>\
                                <p style="text-align:center">@{R=NCM.Strings;K=WEBJS_VK_319;E=js}</p>\
                            </td>\
                        </tr>\
                        <tr style="vertical-align: top;">\
                            <td><p>@{R=NCM.Strings;K=WEBJS_VK_320;E=js}</p></td>\
                        </tr>\
                </table>\
            </div>',
        trackMouse: true
    });
    toolTipConfirmed.initTarget('container');
    toolTipConfirmed.disable();
}