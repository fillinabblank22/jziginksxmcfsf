<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LastXConfigChangesEdit.aspx.cs" Inherits="Orion_NCM_Resources_LastXConfigChangesEdit" MasterPageFile="~/Orion/NCM/NCMResourceEditMasterPage.master" ValidateRequest="false"%>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmMainContent" Runat="Server">
     <table>
        <tr>
            <td class="PageHeader"><%=ResourceHeader %>
            </td>
        </tr>
        <tr>
            <td style="padding-top:10px;">
                <b><%=Resources.NCMWebContent.WEBDATA_VK_128 %></b><br />
                <asp:TextBox runat="server" ID="ResourceTitle" Width="250"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="padding-top:5px;">
                <b><%=Resources.NCMWebContent.WEBDATA_VK_130 %></b><br />
                <asp:TextBox runat="server" ID="ResourceSubTitle" Width="250"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="padding-top:5px;">
                <b><%=Resources.NCMWebContent.WEBDATA_VK_172 %></b>
                <div>
                    <igtxt:WebNumericEdit ID="ConfigsNumeric" runat="server" HorizontalAlign="Center" Width="60" MaxValue="999" MinValue="1" MaxLength="3">
                        <SpinButtons Display="OnRight" />
                    </igtxt:WebNumericEdit>
                </div>
            </td>
        </tr>
        <tr>
            <td style="padding-top:5px;">
		        <asp:UpdatePanel ID="uPanel" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                        <table width="100%" style="padding-top:10px;">
                            <tr>
                                <td nowrap style="border:0px groove transparent;font-weight:bold;">
                                    <asp:CheckBox ID="chkViewChangesOn" AutoPostBack="true" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_166 %>" OnCheckedChanged="chkViewChangesOn_OnCheckedChanged" />
                                    <ncm:ConfigTypes ID="ddlConfigType" runat="server" Width="150px"></ncm:ConfigTypes>
                                    <%=Resources.NCMWebContent.WEBDATA_VK_167 %>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
		    </td>
		</tr>
        <tr>
            <td style="padding-top:20px;">
                <orion:LocalizableButton ID="btnSubmit" runat="server" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitClick"></orion:LocalizableButton>
            </td>
        </tr>
    </table>
</asp:Content>
