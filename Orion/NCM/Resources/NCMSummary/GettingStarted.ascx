﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GettingStarted.ascx.cs" Inherits="Orion_NCM_Resources_NCMSummary_GettingStarted" %>

<orion:resourceWrapper runat="server" ID="Wrapper" CssClass="NcmGettingStarted">
    <Content>
        <style type="text/css">
             .GettingStartedInnerTABLE {
                border: 15px solid #2D5C77;
                padding: 5px;
                margin-bottom: 10px;
                height: 100px;
            }
            
            .GettingStartedInnerTABLE td:first-child
            {
                border-bottom: 0px !important;
                padding: 5px;
                text-align: right;
               
            }

            .GettingStartedInnerTABLE td:last-child
            {
                border-bottom: 0px !important;
                padding: 5px;
                white-space:nowrap;
                text-align: center;
            }

            .GettingStartedButton {
                display: block;
            }
            
            .GettingStartedRemoveButton
            {
                float: right;
                padding: 0px 25px 5px 0px;
            }
        </style>
        <div style="padding: 0px 10px 0px 10px;">
            <table class="GettingStartedInnerTABLE" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <a href="<%=DiscoveryCentralLink%>" title="<%=Resources.NCMWebContent.WEBDATA_VK_352 %>">
                            <span>
                                <%=Resources.NCMWebContent.WEBDATA_VK_352 %></span>
                        </a>
                    </td>
                    <td>
                         <a href="<%=DiscoveryCentralLink%>" class="resList sw-btn-primary sw-btn GettingStartedButton">
                            <%= Resources.NCMWebContent.WEBDATA_VK_352%>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                         <a href="<%=SetupApprovalSystemLink%>" title="<%=Resources.NCMWebContent.WEBDATA_VK_353 %>">
                            <span>
                                <%=Resources.NCMWebContent.WEBDATA_VK_353 %></span>
                        </a>
                         
                    </td>
                    <td>
                        <a href="<%=SetupApprovalSystemLink%>" class="resList sw-btn-primary sw-btn GettingStartedButton">
                            <%= Resources.NCMWebContent.WEBDATA_VK_353%>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="<%=NCMSettingsLink%>" title="<%=Resources.NCMWebContent.WEBDATA_VK_354 %>">
                            <span>
                                <%=Resources.NCMWebContent.WEBDATA_VK_354 %></span>
                        </a>
                    </td>
                    <td>
                        <a href="<%=NCMSettingsLink%>" class="resList sw-btn-primary sw-btn GettingStartedButton">
                            <%= Resources.NCMWebContent.WEBDATA_VK_354%>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                         <a href="<%=NCMGlobalDeviceDefaultsLink%>" title="<%=Resources.NCMWebContent.WEBDATA_IA_39 %>">
                            <span>
                                <%=Resources.NCMWebContent.WEBDATA_IA_39%></span>
                        </a>
                    </td>
                    <td>
                        <a href="<%=NCMGlobalDeviceDefaultsLink%>" class="resList sw-btn-primary sw-btn GettingStartedButton">
                            <%= Resources.NCMWebContent.WEBDATA_IA_39%>
                        </a>
                    </td>
                </tr>
            </table>
        </div>
        <div class="GettingStartedRemoveButton" style="">
           <orion:LocalizableButton runat="server" ID="btnRemoveResource" LocalizedText="CustomText" Text="<%$Resources: NCMWebContent, WEBDATA_VK_355%>" DisplayType="Secondary" OnClick="btnRemoveResource_Click" CssClass="sw-btn"/>
                   
        </div>
    </Content>
</orion:resourceWrapper>


