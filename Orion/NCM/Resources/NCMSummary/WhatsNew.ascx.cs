using System;
using System.Collections.Generic;
using SolarWinds.Coding.Utils.Extensions;
using SolarWinds.NCM.Common;
using SolarWinds.NCM.Common.Help;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Help)]
public partial class Orion_NCM_Resources_NCMSummary_WhatsNew : BaseResourceControl
{
    private readonly IHelpHelperWrap helpHelper;

    public Orion_NCM_Resources_NCMSummary_WhatsNew()
    {
        helpHelper = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
    }

    protected override string DefaultTitle => Resources.NCMWebContent.WEBDATA_VK_707;

    public sealed override string DisplayTitle => $@"{Title} {LicenseInfo.Version.ToString(2)}";

    public override string HelpLinkFragment => string.Empty;

    public string LearnMoreDisplayText => Resources.NCMWebContent.WEBDATA_AP_5 + LicenseInfo.Version.ToString(2);

    protected string LearnMoreLink => helpHelper.GetHelpUrl("OrionNCMWebWhatsNew");

    public override IEnumerable<Type> RequiredInterfaces => 
        ServiceLocator.Container.Resolve<IViewManager>().GetSupportedInterfacesForViewType(@"NCMSummary");

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Page.Request.QueryString[@"CurrentUrl"].IsNullOrEmpty())
        {
            btnRemoveResource.Visible = false;
        }
        btnLearnMore.NavigateUrl = LearnMoreLink;
        btnLearnMore.Text = LearnMoreDisplayText;
        Wrapper.ShowEditButton = false;
        Wrapper.Visible = SecurityHelper.IsValidNcmAccountRole;
    }

    protected void btnRemoveResource_Click(object sender, EventArgs eventArgs)
    {
        ResourceManager.DeleteById(Resource.ID);
        Response.Redirect(Request.Url.ToString());
    }
}
