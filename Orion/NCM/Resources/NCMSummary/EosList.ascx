﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EosList.ascx.cs" Inherits="Orion_NCM_Resources_NCMSummary_EosList" %>

<style type="text/css">
    .ListHeader
    {
        background-color: #E2E1D4;
        font-family: Arial,Helvetica,sans-serif;
        font-size: 7pt !important;
        font-weight: normal;
        padding-left: 5px;
        height:25px;
    }
    
    #table .ListItem
    {
        border-bottom-color: #D8D6D1;
        border-bottom-style: groove;
        border-bottom-width: 1px !important;
        font-family: Arial,Verdana,Helvetica,sans-serif;
        font-size: 8pt;
        margin-bottom: 0;
        margin-top: 0;
        padding-left: 5px;
        height:25px;
    }
    
    .PageButtons { border: 0 none !important; }
    .PageButtons td {  border: 0 none !important; padding: 0 2px; }
    .PageButtons img { vertical-align: top !important; }
    .PageButtons a { white-space: nowrap; font-size: 8pt; }
    .PageButtons .disabled { color: silver; }
    .PageButtons .spacer { width: 18px; height: 15px; background: url('/Orion/NCM/Resources/images/ConfigChangeApproval/arrows.gif') no-repeat scroll 0 0 transparent; }
    .PageButtons .enabled .prev { background-position: 0px 0px !important; }
    .PageButtons .enabled .next { background-position: 0px -15px !important; }
    .PageButtons .disabled .prev { background-position: -18px 0px !important; }
    .PageButtons .disabled .next { background-position: -18px -15px !important; }
    .PageButtons .enabled .all { background-position: 0px -30px !important; }
    .PageButtons .disabled .all { background-position: -18px -30px !important; }
</style>

<orion:ResourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:Panel ID="ContentContainer" runat="server">
            <asp:UpdatePanel ID="updatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Repeater runat="server" ID="repeater" OnItemDataBound="repeater_ItemDataBound">
                        <HeaderTemplate>
                            <table id="table" border="0" cellpadding="2" cellspacing="0" width="100%">
                                <tr>
	                                <td class="ListHeader" nowrap="nowrap"><%= Resources.NCMWebContent.WEBDATA_IC_198%></td>
                                    <td class="ListHeader" nowrap="nowrap"><%= Resources.NCMWebContent.WEBDATA_DP_09 %></td>
	                                <td class="ListHeader" nowrap="nowrap"><%= Resources.NCMWebContent.WEBDATA_VK_95 %></td>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                                <tr>
			                        <td class="ListItem" valign="middle"><%# FormatShortDateTime(Eval(@"EndOfSupport"))%>&nbsp;</td>
                                    <td class="ListItem" valign="middle">                                                      
                                        <a href="<%# RedirectPartNumbertUrl(Convert.ToString(Eval(@"PartNumber"))) %>"><%# Eval(@"PartNumber")%></a>
                                    </td>
			                        <td class="ListItem" valign="middle">
                                        <a href="<%=RedirectUrl %>">
                                            <%# $@"{Eval(@"TargetNodes")} {(Convert.ToInt32(Eval(@"TargetNodes")) > 1 ? Resources.NCMWebContent.WEBDATA_VK_892 : Resources.NCMWebContent.WEBDATA_VK_891)}" %>
                                        </a>
                                    </td>
		                        </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                                <tr id="EmptyData" runat="server" visible="false">
                                    <td colspan="4" class="ListItem"><%= Resources.NCMWebContent.WEBDATA_VK_890 %></td>
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <table cellpadding="0" cellspacing="0" width="100%" class="PageButtons">
                        <tr>
                            <td class="ListHeader" align="left" valign="middle" style="width:180px;">
                                <asp:LinkButton ID="btnPrevious5" CssClass="enabled" runat="server" OnClick="btnPrevious5_Click">
                                    <img class="spacer prev" src="/Orion/NCM/Resources/images/ConfigChangeApproval/arrow-spacer.gif" />&nbsp;<%= Resources.NCMWebContent.WEBDATA_VK_89 %>
                                </asp:LinkButton>
                            </td>
                            <td class="ListHeader">&nbsp;</td>
                            <td class="ListHeader" align="left" valign="middle">
                                <asp:LinkButton ID="btnNext5" CssClass="enabled" runat="server" OnClick="btnNext5_Click">
                                    <%= Resources.NCMWebContent.WEBDATA_VK_90 %>&nbsp;<img class="spacer next" src="/Orion/NCM/Resources/images/ConfigChangeApproval/arrow-spacer.gif" />
                                </asp:LinkButton>
                            </td>
                            <td class="ListHeader" align="left" valign="middle">
                                <img src="/Orion/images/form_prompt_divider.gif" height="20px" width="2px" />
                            </td>
                            <td class="ListHeader" align="left" valign="middle">
                                <asp:LinkButton ID="btnShowAll" CssClass="enabled" runat="server" OnClick="btnShowAll_Click">
                                    <%= Resources.NCMWebContent.WEBDATA_VK_91 %>&nbsp;<img class="spacer all" src="/Orion/NCM/Resources/images/ConfigChangeApproval/arrow-spacer.gif" />
                                </asp:LinkButton>
                            </td>
                            <td class="ListHeader" align="left" valign="middle">
                                <img src="/Orion/images/form_prompt_divider.gif" height="20px" width="2px" />
                            </td>
                            <td class="ListHeader" align="left" valign="middle">
                               <div style="display:inline !Important;">
                                   <asp:UpdateProgress ID="updateProgress" runat="server" AssociatedUpdatePanelID="updatePanel" >
                                        <ProgressTemplate>
                                            <asp:Image ID="imgLoading" ImageAlign="AbsMiddle" runat="server" ImageUrl="/Orion/NCM/Resources/images/ConfigChangeApproval/loading.gif"/>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </div>
                            </td>
                            <td class="ListHeader" align="right" valign="middle" style="width:100%;">
                                <asp:Label ID="lblPagingInfo" runat="server" style="vertical-align:middle;font-size:11px;padding-right:3px;"></asp:Label>                                    
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <div style="border:0px groove transparent;" id="ErrorContainer" runat="server"/>
    </Content>
</orion:ResourceWrapper>