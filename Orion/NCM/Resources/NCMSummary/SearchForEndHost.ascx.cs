using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;

using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Help)]
public partial class SearchForEndHost : BaseResourceControl
{
    protected override string DefaultTitle
    {
        get
        {
            return Resources.NCMWebContent.WEBDATA_VK_234;
        }
    }

    public override string HelpLinkFragment
    {
        get 
        { 
            return @"OrionNCMWebSearchForEndHost";
        }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            var viewManagerAdapter = ServiceLocator.Container.Resolve<IViewManager>();
            return viewManagerAdapter.GetSupportedInterfacesForViewType(@"NCMSummary");
        }
    }

    public sealed override string SubTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(this.Resource.Properties[@"SubTitle"]))
            {
                return this.Resource.Properties[@"SubTitle"];
            }
            return string.Empty;
        }
    }

    public sealed override string DisplayTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(this.Resource.Properties[@"Title"]))
            {
                return this.Resource.Properties[@"Title"];
            }
            return Resources.NCMWebContent.WEBDATA_VK_234;
        }
    }

    public override string EditURL
    {
        get { return ExtendCustomEditUrl(@"/Orion/NCM/Resources/NCMSummary/SearchForEndHostEdit.aspx"); }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (CommonHelper.IsNpmInstalled())
        {
            NPMDefaultSettingsHelper settingHelper = new NPMDefaultSettingsHelper(this.Resource);
            settingHelper.SetSetting("ResourceID", this.Resource.ID.ToString());

            SolarWinds.NCMModule.Web.Resources.SearchForEndHost resource = new SolarWinds.NCMModule.Web.Resources.SearchForEndHost();
            resource.ResourceSettings = this.Resource;
            resource.ResourceContainer = Wrapper;
            Wrapper.Content.Controls.Add(resource);

            Wrapper.ShowEditButton = resource.ShowEditButton;
        }
        else
        {
            Wrapper.Visible = false;
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
