﻿using System;
using System.Collections.Generic;
using System.Data;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.NCMModule.Web.Resources;

using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using System.Globalization;
using SolarWinds.Logging;
using Constants = SolarWinds.NCM.Contracts.Constants;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.PieCharts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsPieChartsValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NCM_Resources_NCMSummary_PolicyReportViolationsChart : StandardChartResource
{
    #region Private members

    private ISWrapper isLayer = new ISWrapper();
    private Log log = new Log();
    private string dateFrom;
    private string dateTo;
    string policyReportName = string.Empty;
    private Dictionary<string, string> parameters;

    #endregion

    #region Resource properties

    protected override string DefaultTitle
    {
        get { return Resources.NCMWebContent.WEBDATA_VK_226; }
    }

    public override string HelpLinkFragment
    {
        get { return @"OrionNCMWebPolicyReportViolations"; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            var viewManagerAdapter = ServiceLocator.Container.Resolve<IViewManager>();
            return viewManagerAdapter.GetSupportedInterfacesForViewType(Constants.ViewTypeNCMSummary);
        }
    }

    public sealed override string DisplayTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(this.Resource.Properties[@"Title"]))
            {
                return this.Resource.Properties[@"Title"];
            }
            return Resources.NCMWebContent.WEBDATA_VK_226;
        }
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        return new string[0];
    }

    protected override string NetObjectPrefix
    {
        get { return @"NCM_SUMMARY"; }
    }

    public override string EditURL
    {
        get { return ExtendCustomEditUrl(@"/Orion/NCM/Resources/NCMCommonResources/PolicyReportViolationsEdit.aspx"); }
    }

    public override ResourceLoadingMode ResourceLoadingMode { get { return SolarWinds.Orion.Web.UI.ResourceLoadingMode.Ajax; } }

    #endregion

    #region Resource life cycle

    protected void Page_Load(object sender, EventArgs e)
    {
        SetupTimeRange();
        InitializeChartDataQuery();

        InitializeChartProperties();
        HandleInit(WrapperContents);
    }

    #endregion

    #region Chart initialization

    private void InitializeChartProperties()
    {
        Resource.Properties[@"ChartName"] = @"PolicyReportViolationsChart";
        Resource.Properties[@"ChartTitle"] = policyReportName;
        Resource.Properties[@"ShowTitle"] = @"true";
    }

    protected override Dictionary<string, object> GenerateDisplayDetails()
    {
        var details = base.GenerateDisplayDetails();
        details[@"ResourceID"] = Resource.ID;
        details[@"NCM_PolicyReportViolationsChartDataParameters"] = parameters;

        return details;
    }

    protected override void AddChartExportControl(System.Web.UI.Control resourceWrapperContents)
    {
        //there is no need for export button
    }

    private void SetupTimeRange()
    {
        dateFrom = Resource.Properties[@"DateFrom"];
        dateTo = Resource.Properties[@"DateTo"];

        if (!string.IsNullOrEmpty(Resource.Properties[@"TimeRange"]) && Resource.Properties[@"TimeRange"] != @"Custom")
        {
            var dateTimeTo = DateTime.Now;
            var strDateTimeTo = string.Empty;
            if (Resource.Properties[@"TimeRange"].Equals(@"Last Month", StringComparison.InvariantCultureIgnoreCase))
            {
                //special fix for last month
                dateTimeTo = new DateTime(dateTimeTo.Year, dateTimeTo.Month,
                    dateTimeTo.AddDays(-dateTimeTo.Day + 1).Day).AddMilliseconds(-1);
                strDateTimeTo = dateTimeTo.ToString(CultureInfo.InvariantCulture);//<--                
            }
            dateFrom = TimeParser.GetTime(Resource.Properties[@"TimeRange"]).ToString(CultureInfo.InvariantCulture);//<--            
            dateTo = strDateTimeTo;
        }

        if (string.IsNullOrEmpty(dateTo))
        {
            if (string.IsNullOrEmpty(dateFrom))
            {
                var helper = new NPMDefaultSettingsHelper(Resource);
                dateFrom = DateTime.Now.AddDays(-7).ToString(CultureInfo.InvariantCulture);//<--                
                helper.SetSetting("DateFrom", dateFrom);
                helper.SetSetting("TimeRange", "Last 7 Days");
                helper.SetSetting("DateTo", string.Empty);
            }
            Resource.SubTitle = string.Format(Resources.NCMWebContent.WEBDATA_VK_185, Convert.ToDateTime(dateFrom, CultureInfo.InvariantCulture).ToString());
        }
        else
            Resource.SubTitle = string.Format(Resources.NCMWebContent.WEBDATA_VK_186, Convert.ToDateTime(dateFrom, CultureInfo.InvariantCulture).ToString(), Convert.ToDateTime(dateTo, CultureInfo.InvariantCulture).ToString());
    }

    private string GetPolicyReportID()
    {
        var reportID = new Guid().ToString();

        try
        {
            using (var proxy = isLayer.Proxy)
            {
                var dt = proxy.Query(@"SELECT TOP 1 P.PolicyReportID FROM Cirrus.PolicyReports As P");

                foreach (DataRow dr in dt.Rows)
                {
                    reportID = $@"{{{dr["PolicyReportID"].ToString()}}}";
                }
            }
        }
        catch { }

        return reportID;
    }

    private string GetPolicyReportName()
    {
        var reportName = string.Empty;

        try
        {
            using (var proxy = isLayer.Proxy)
            {
                var dt = proxy.Query(@"SELECT TOP 1 P.PolicyReportID, P.Name FROM Cirrus.PolicyReports As P");
                foreach (DataRow dr in dt.Rows)
                {
                    reportName = dr["Name"].ToString();
                }
            }
        }
        catch { }

        return reportName;
    }

    private void InitializeChartDataQuery()
    {
        var policyReportID = string.Empty;
        var granularity = string.Empty;

        granularity = Resource.Properties[@"Granularity"];
        if (string.IsNullOrEmpty(granularity))
        {
            Resource.Properties[@"Granularity"] = @"Daily";
            granularity = @"Daily";
        }

        policyReportID = Resource.Properties[@"PolicyReportID"];
        if (string.IsNullOrEmpty(policyReportID))
        {
            policyReportID = GetPolicyReportID();
            Resource.Properties[@"PolicyReportID"] = policyReportID;
        }

        policyReportName = Resource.Properties[@"PolicyReportName"];
        if (string.IsNullOrEmpty(policyReportName))
        {
            policyReportName = GetPolicyReportName();
            Resource.Properties[@"PolicyReportName"] = policyReportName;
        }

        parameters = new System.Collections.Generic.Dictionary<string, string>();
        parameters.Add(@"@startDate", dateFrom);
        parameters.Add(@"@endDate", dateTo);
        parameters.Add(@"@Granularity", granularity);
        parameters.Add(@"@PolicyReportID", policyReportID);
    }
    #endregion
}
