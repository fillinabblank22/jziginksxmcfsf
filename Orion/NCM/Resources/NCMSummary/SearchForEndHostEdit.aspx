<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMResourceEditMasterPage.master" AutoEventWireup="true" CodeFile="SearchForEndHostEdit.aspx.cs" Inherits="SearchForEndHostEdit" %>

<asp:Content ID="SearchForEndHostEditContent" ContentPlaceHolderID="ncmMainContent" Runat="Server">
    <table>
        <tr>
            <td class="PageHeader"><%=ResourceHeader %>
            </td>
        </tr>
        <tr>
            <td style="padding-top:10px;">
                <b><%=Resources.NCMWebContent.WEBDATA_VK_128 %></b><br />
                <asp:TextBox runat="server" ID="ResourceTitle" Width="250"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="padding-top:5px;">
                <b><%=Resources.NCMWebContent.WEBDATA_VK_130 %></b><br />
                <asp:TextBox runat="server" ID="ResourceSubTitle" Width="250"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="padding-top:10px;">
                <b><%=Resources.NCMWebContent.WEBDATA_VK_235 %></b>
                <div><input type="checkbox" id="SourcePortName" runat="server" value="SourcePortName"/><%=Resources.NCMWebContent.WEBDATA_VK_236 %></div>
                <div><input type="checkbox" id="MappedPortName" runat="server" value="MappedPortName"/><%=Resources.NCMWebContent.WEBDATA_VK_237 %></div>
                <div><input type="checkbox" id="MappedPortAlias" runat="server" value="MappedPortAlias"/><%=Resources.NCMWebContent.WEBDATA_VK_238 %></div>
                <div><input type="checkbox" id="MappedPortType" runat="server" value="MappedPortType"/><%=Resources.NCMWebContent.WEBDATA_VK_239 %></div>
                <div><input type="checkbox" id="MappedPortState" runat="server" value="MappedPortState"/><%=Resources.NCMWebContent.WEBDATA_VK_240 %></div>
                <div><input type="checkbox" id="MappedPortAdminStatus" runat="server" value="MappedPortAdminStatus"/><%=Resources.NCMWebContent.WEBDATA_VK_241 %></div>
                <div><input type="checkbox" id="MappedPortSpeed" runat="server" value="MappedPortSpeed"/><%=Resources.NCMWebContent.WEBDATA_VK_242 %></div>
                <div><input type="checkbox" id="MappedPortIndex" runat="server" value="MappedPortIndex"/><%=Resources.NCMWebContent.WEBDATA_VK_243 %></div>
                <div><input type="checkbox" id="LastSeen" runat="server" value="LastSeen"/><%=Resources.NCMWebContent.WEBDATA_VK_244 %></div>
            </td>
        </tr>
        <tr>
            <td style="padding-top:10px;">
                <b><%=Resources.NCMWebContent.WEBDATA_VK_245 %></b>
                <div><input type="checkbox" id="ControllersName" runat="server" value="ControllersName"/><%=Resources.NCMWebContent.WEBDATA_VK_246 %></div>
                <div><input type="checkbox" id="ControllersModelName" runat="server" value="ControllersModelName"/><%=Resources.NCMWebContent.WEBDATA_VK_247 %></div>
                <div><input type="checkbox" id="ControllersRole" runat="server" value="ControllersRole"/><%=Resources.NCMWebContent.WEBDATA_VK_248 %></div>
                <div><input type="checkbox" id="MACAddress" runat="server" value="MACAddress"/><%=Resources.NCMWebContent.WEBDATA_VK_249 %></div>
                <div><input type="checkbox" id="SourceMACAddress" runat="server" value="SourceMACAddress"/><%=Resources.NCMWebContent.WEBDATA_VK_250 %></div>
                <div><input type="checkbox" id="SourceModelName" runat="server" value="SourceModelName"/><%=Resources.NCMWebContent.WEBDATA_VK_251 %></div>
                <div><input type="checkbox" id="SourceSystemName" runat="server" value="SourceSystemName"/><%=Resources.NCMWebContent.WEBDATA_VK_252 %></div>
                <div></div>
            </td>
        </tr>
        <tr>
            <td style="padding-top:15px;">
                <input type="checkbox" id="FuzzyMatch" runat="server"/><%=Resources.NCMWebContent.WEBDATA_VK_253 %>
            </td>
        </tr>
        <tr>
		     <td style="padding-top:15px;">
		        <input type="checkbox" id="DrillDown" runat="server"/><%=Resources.NCMWebContent.WEBDATA_VK_254 %>
		    </td>
		</tr>
        <tr>
            <td style="padding-top:20px;">
                <orion:LocalizableButton ID="btnSubmit" runat="server" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitClick"></orion:LocalizableButton>
                <orion:LocalizableButton ID="btnCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="CancelClick" CausesValidation="false"></orion:LocalizableButton>
            </td>
        </tr>
	</table>
</asp:Content>