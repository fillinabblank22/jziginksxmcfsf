using System;
using System.Web.UI;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.Resources;
using SolarWinds.NCMModule.Web.Resources.Wrappers;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class SearchForEndHostEdit : Page
{
    private readonly IEditResourcePageService editResourceService;

    public SearchForEndHostEdit()
    {
        editResourceService = ServiceLocator.Container.Resolve<IEditResourcePageService>();
    }

    protected ResourceInfo Resource { get; private set; }
    protected string ResourceHeader => editResourceService.GetHeader(Resource);

    protected override void OnInit(EventArgs e)
    {
        Resource = editResourceService.Init(new PageWrap(this), ResourceTitle, ResourceSubTitle);

        SetupControlFromRequest();

        base.OnInit(e);
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        editResourceService.SaveChanges(Resource, ResourceTitle, null);

        Resource.Properties[@"AddedColumns"] = GetAddedColumns();
        Resource.Properties[@"AddedColumnsWireless"] = GetAddedColumnsWireless();

        if (DrillDown.Checked)
        {
            Resource.Properties[@"Drill-down"] = @"NCM";
        }
        else
        {
            Resource.Properties[@"Drill-down"] = @"NPM";
        }

        Resource.Properties[@"FuzzyMatch"] = FuzzyMatch.Checked.ToString();

        Response.Redirect(BaseResourceControl.GetEditPageReturnUrl(Request.QueryString, Resource.View.ViewID.ToString()));
    }

    protected void CancelClick(object sender, EventArgs e)
    {
        Response.Redirect(BaseResourceControl.GetEditPageReturnUrl(Request.QueryString, Resource.View.ViewID.ToString()));
    }

    private void SetupControlFromRequest()
    {
        SetAddedColumns(Resource.Properties[@"AddedColumns"]);
        SetAddedColumnsWireless(Resource.Properties[@"AddedColumnsWireless"]);

        if (!string.IsNullOrEmpty(Resource.Properties[@"Drill-down"]))
        {
            DrillDown.Checked = Resource.Properties[@"Drill-down"] == @"NCM";
        }
        else
        {
            DrillDown.Checked = false;
        }

        FuzzyMatch.Checked = !string.IsNullOrEmpty(Resource.Properties[@"FuzzyMatch"]) &&
                             Convert.ToBoolean(Resource.Properties[@"FuzzyMatch"]);
    }

    private string GetAddedColumns()
    {
        string returnValue = string.Empty;

        if (MappedPortName.Checked)
            returnValue += MappedPortName.Value + @",";
        if (MappedPortAlias.Checked)
            returnValue += MappedPortAlias.Value + @",";
        if (MappedPortType.Checked)
            returnValue += MappedPortType.Value + @",";
        if (MappedPortState.Checked)
            returnValue += MappedPortState.Value + @",";
        if (MappedPortAdminStatus.Checked)
            returnValue += MappedPortAdminStatus.Value + @",";
        if (MappedPortSpeed.Checked)
            returnValue += MappedPortSpeed.Value + @",";
        if (MappedPortIndex.Checked)
            returnValue += MappedPortIndex.Value + @",";
        if (SourcePortName.Checked)
            returnValue += SourcePortName.Value + @",";
        if (LastSeen.Checked)
            returnValue += LastSeen.Value + @",";

        return returnValue.Trim(',');
    }

    private void SetAddedColumns(string addedColumns)
    {
        if (string.IsNullOrEmpty(addedColumns))
            return;

        string[] columns = addedColumns.Split(',');
        foreach (string column in columns)
        {
            if (column == MappedPortName.Value)
                MappedPortName.Checked = true;
            if (column == MappedPortAlias.Value)
                MappedPortAlias.Checked = true;
            if (column == MappedPortType.Value)
                MappedPortType.Checked = true;
            if (column == MappedPortState.Value)
                MappedPortState.Checked = true;
            if (column == MappedPortAdminStatus.Value)
                MappedPortAdminStatus.Checked = true;
            if (column == MappedPortSpeed.Value)
                MappedPortSpeed.Checked = true;
            if (column == MappedPortIndex.Value)
                MappedPortIndex.Checked = true;
            if (column == SourcePortName.Value)
                SourcePortName.Checked = true;
            if (column == LastSeen.Value)
                LastSeen.Checked = true;
        }
    }

    private string GetAddedColumnsWireless()
    {
        string returnValue = string.Empty;

        if (ControllersName.Checked)
            returnValue += ControllersName.Value + @",";
        if (ControllersModelName.Checked)
            returnValue += ControllersModelName.Value + @",";
        if (ControllersRole.Checked)
            returnValue += ControllersRole.Value + @",";
        if (MACAddress.Checked)
            returnValue += MACAddress.Value + @",";
        if (SourceMACAddress.Checked)
            returnValue += SourceMACAddress.Value + @",";
        if (SourceModelName.Checked)
            returnValue += SourceModelName.Value + @",";
        if (SourceSystemName.Checked)
            returnValue += SourceSystemName.Value + @",";

        return returnValue.Trim(',');
    }

    private void SetAddedColumnsWireless(string addedColumns)
    {
        if (string.IsNullOrEmpty(addedColumns))
            return;

        string[] columns = addedColumns.Split(',');
        foreach (string column in columns)
        {
            if (column == ControllersName.Value)
                ControllersName.Checked = true;
            if (column == ControllersModelName.Value)
                ControllersModelName.Checked = true;
            if (column == ControllersRole.Value)
                ControllersRole.Checked = true;
            if (column == MACAddress.Value)
                MACAddress.Checked = true;
            if (column == SourceMACAddress.Value)
                SourceMACAddress.Checked = true;
            if (column == SourceModelName.Value)
                SourceModelName.Checked = true;
            if (column == SourceSystemName.Value)
                SourceSystemName.Checked = true;
        }
    }
}
