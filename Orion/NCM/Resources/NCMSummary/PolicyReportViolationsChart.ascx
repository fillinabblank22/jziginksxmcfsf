﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PolicyReportViolationsChart.ascx.cs" Inherits="Orion_NCM_Resources_NCMSummary_PolicyReportViolationsChart" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div style="overflow: hidden !important;">
            <asp:PlaceHolder runat="server" ID="WrapperContents"></asp:PlaceHolder>
        </div>
    </Content>
</orion:resourceWrapper>
