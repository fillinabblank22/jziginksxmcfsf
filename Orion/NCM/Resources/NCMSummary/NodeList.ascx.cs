using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;

using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_NetPerfMon_Resources_CirrusResources_NodeList : BaseResourceControl
{
    private SolarWinds.NCMModule.Web.Resources.NodeList NCMNodeList;
	
    protected override string DefaultTitle
	{
        get { return Resources.NCMWebContent.WEBDATA_VK_269; }
	}

    public override string HelpLinkFragment
    {
        get { return @"OrionNCMWebNodeList"; }
    }

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get
		{
            var viewManagerAdapter = ServiceLocator.Container.Resolve<IViewManager>();
            return viewManagerAdapter.GetSupportedInterfacesForViewType(@"NCMSummary");
		}
	}

    public sealed override string DisplayTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(this.Resource.Properties[@"Title"]))
            { 
                return this.Resource.Properties[@"Title"]; 
            }
            return Resources.NCMWebContent.WEBDATA_VK_269;
        }
    }

    public override string EditURL
    {
        get { return ExtendCustomEditUrl(@"/Orion/NCM/Resources/NCMCommonResources/NodeListEdit.aspx"); }
    }
	
    protected override void OnLoad(EventArgs e)
	{
        SetDefaultResourceSettings();
        
        Wrapper.ShowManageButton = true;
        Wrapper.ManageButtonText = Resources.NCMWebContent.WEBDATA_VK_270;
        Wrapper.ManageButtonTarget = @"/Orion/Nodes/Default.aspx";
        
        NCMNodeList = new NodeList();
        NCMNodeList.ResourceSettings = this.Resource;
        NCMNodeList.ResourceContainer = Wrapper;
        NCMNodeList.ViewID = this.Resource.View.LimitationID;
        Wrapper.Content.Controls.Add(NCMNodeList);
        Wrapper.ShowEditButton = NCMNodeList.ShowEditButton;
        Wrapper.ShowManageButton = Profile.AllowNodeManagement & NCMNodeList.ShowManageButton;

        base.OnLoad(e);
    }

    private void SetDefaultResourceSettings()
    {
        NPMDefaultSettingsHelper settingHelper = new NPMDefaultSettingsHelper(this.Resource);        
        settingHelper.SetSetting("GroupBy", "Vendor");
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
