<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RecentPosts.ascx.cs" Inherits="Orion_NCM_Resources_NCMCommonResources_RecentPosts" %>
<%@ Register Src="~/Orion/Controls/HelpButton.ascx" TagPrefix="orion" TagName="HelpButton" %>

<asp:ScriptManagerProxy ID="ScriptManagerProxy" runat="server">
    <Services>
			<asp:ServiceReference path="~/Orion/NCM/Services/ThwackNCMRecentPosts.asmx"  />
    </Services>
</asp:ScriptManagerProxy>

<style type="text/css">

    #manageButtons 
    { 
        padding-bottom: 20px;
        padding-right: 5px;
    }
    #manageButtons .HelpButton {float: none !Important;}
    #manageButtons .EditResourceButton {float: none !Important;}

    #manageButtons .HelpButton span {color: white !Important;}
    #manageButtons .EditResourceButton span {color: white !Important;}

    .ThwackTextHeader {
        color: white;
        position: relative;
        text-align: center;
        top: 15px;
        vertical-align: top;
    }
</style> 

<orion:resourceWrapper runat="server" ID="resourceWrapper" ShowHeaderBar="false">
    <Content>
		<div style="border: 1px solid #d5d5d5;">
        <table cellpadding="0" cellspacing="0" border="0" style="background-image: url(/Orion/NCM/Resources/images/ThwackImages/New_ThwackHeader.png);background-repeat:repeat-x;">
			<tr>
				<td style="vertical-align: middle;">
					<div>
						<img class="ThwackHeaderImage" src="/Orion/NCM/Resources/images/ThwackImages/New_ThwackLogo.png" alt="Thwack logo" />
						<span class="ThwackTextHeader"><%= Resources.NCMWebContent.WEBDATA_VY_90 %></span>
					</div>
				</td>
				<td id="manageButtons" align="right">
					<asp:PlaceHolder ID="phEditButton" runat="server">
                        <orion:LocalizableButtonLink ID="btnEdit" runat="server" href="<%# this.EditURL %>" DisplayType="Resource" CssClass="EditResourceButton" LocalizedText="Edit" />
					</asp:PlaceHolder>&nbsp;
					<asp:PlaceHolder ID="phHelpButton" runat="server">
                        <orion:HelpButton runat="server" ID="HelpButton" style="padding-right:5px"/>
					</asp:PlaceHolder>&nbsp;    
				</td>
			</tr>
		</table>
        <div runat="server" id="ThwackRecentPostsList" class="ThwackContent"/>
		<table cellpadding="0" cellspacing="0" style="background-color: #eaeaea; height: 22px;">
            <tr>              
                <td style="border: none; text-align: right; padding-right: 10px;">
                    <a href="http://thwack.com/forums/12.aspx" class="CTA_bottom_resource"><%=Resources.NCMWebContent.WEBDATA_VM_116%> &#0187;</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="http://thwack.com/user/CreateUser.aspx" class="CTA_bottom_resource"><%=Resources.NCMWebContent.WEBDATA_VM_117%> &#0187;</a>
                </td>
            </tr>
        </table>
		</div>
        <script type="text/javascript">
//<![CDATA[

            var resourceId = '<%=this.ResourceID%>';
            SW.Core.VisibilityObserver.registerByResourceId(resourceId, function () {
                Sys.Application.add_init(function () {
                    ThwackNCMRecentPosts.GetRecentPosts(<%= this.Count %>, function (result) {
                        var thwackRecentPostsList = $get('<%=this.ThwackRecentPostsList.ClientID %>');
                        thwackRecentPostsList.innerHTML = result;
                        SW.Core.VisibilityObserver.loadingCompleted(resourceId);
                    }, function () {
                        var thwackRecentPostsList = $get('<%=this.ThwackRecentPostsList.ClientID %>'); //thwackRecentPostsList.innerHTML = 'Unable to contact thwack server.';

                        thwackRecentPostsList.innerHTML = '<%= ControlHelper.EncodeJsString(Resources.NCMWebContent.WEBDATA_VM_118)%>';
                        SW.Core.VisibilityObserver.loadingCompleted(resourceId);
                    });
                });
            }, true);
           
//]]>
        </script>
     </Content>
     
</orion:resourceWrapper>