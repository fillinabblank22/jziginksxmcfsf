﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using Constants = SolarWinds.NCM.Contracts.Constants;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Reports)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_NCM_Resources_NCMSummary_EosList : BaseResourceControl
{
    private const int _pageSize = 5;
    private const string _defaultPeriod = "Next 3 months";

    private ISWrapper _isLayer = new ISWrapper();

    protected override string DefaultTitle
    {
        get { return Resources.NCMWebContent.WEBDATA_VK_888; }
    }

    public sealed override string DisplayTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(Resource.Title))
            {
                return Resource.Title;
            }

            return DefaultTitle;
        }
    }

    public sealed override string SubTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(Resource.SubTitle))
                return Resource.SubTitle;

            var period = Resource.Properties[@"Period"];
            if (string.IsNullOrEmpty(period))
            {
                period = _defaultPeriod;
            }

            return TimeParser.GetLocalizedTimeRange(period);
        }
    }

    public override string EditControlLocation
    {
        get
        {
            return @"/Orion/NCM/Controls/EditResourceControls/EditEosList.ascx";
        }
    }

    public override string HelpLinkFragment
    {
        get { return @"OrionNCMPHEOL"; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            var viewManagerAdapter = ServiceLocator.Container.Resolve<IViewManager>();
            return viewManagerAdapter.GetSupportedInterfacesForViewType(Constants.ViewTypeSummary);
        }
    }

    protected int StartRowNumber
    {
        get { return Convert.ToInt32(ViewState["NCM_Eos_StartRowNumber"]); }
        set { ViewState["NCM_Eos_StartRowNumber"] = value; }
    }

    protected int RowNumbers
    {
        get
        {
            if (ViewState["NCM_Eos_RowNumbers"] == null || Convert.ToInt32(ViewState["NCM_Eos_RowNumbers"]) == 0)
            {
                using (var proxy = _isLayer.Proxy)
                {
                    var swql = $@"
                        SELECT COUNT(NCMNodeProperties.NodeID) AS RowNumbers 
                        FROM Cirrus.NodeProperties AS NCMNodeProperties 
	                    LEFT JOIN (SELECT NodeID, MIN(Model) AS Model FROM Cirrus.EntityPhysical WHERE (EntityClass='3') GROUP BY NodeID) AS EntityPhysical
			                ON EntityPhysical.NodeID=NCMNodeProperties.NodeID
                        INNER JOIN Orion.Nodes AS Nodes ON NCMNodeProperties.CoreNodeID=Nodes.NodeID
                        {GetWhereClause(@"NCMNodeProperties")}
                        GROUP BY NCMNodeProperties.EndOfSupport, EntityPhysical.Model
                        WITH LIMITATION {Resource.View.LimitationID}";

                    var dt = proxy.Query(swql);
                    if (dt != null)
                    {
                        ViewState["NCM_Eos_RowNumbers"] = dt.Rows.Count;
                        return Convert.ToInt32(ViewState["NCM_Eos_RowNumbers"]);
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            else
            {
                return Convert.ToInt32(ViewState["NCM_Eos_RowNumbers"]);
            }
        }
    }

    protected string FormatShortDateTime(object obj)
    {
        DateTime date;
        if (DateTime.TryParse(Convert.ToString(obj), out date))
        {
            return date.ToShortDateString();
        }

        return string.Empty;
    }

    protected string RedirectUrl
    {
        get
        {
            var period = GetPeriod();
            var comparison = GetFilterComparison(period);
            return $@"/Orion/NCM/Resources/Eos/EosMatching.aspx?field=EndOfSupport&type=eosdate&comparison={comparison}";
        }
    }

    protected string RedirectPartNumbertUrl(string partNumber)
    {
        return
            $@"/Orion/NCM/Resources/Eos/EosMatching.aspx?field=EntityPhysical_Model&type=string&value={HttpUtility.UrlEncode(partNumber)}";
    }

    protected override void OnLoad(EventArgs e)
    {
        var validator = new SWISValidator();
        validator.ContentContainer = ContentContainer;
        validator.ResourceContainer = Wrapper;
        ErrorContainer.Controls.Add(validator);

        Wrapper.ManageButtonText = Resources.NCMWebContent.WEBDATA_VK_889;
        Wrapper.ManageButtonTarget = @"/Orion/NCM/Resources/Eos/EosMatching.aspx";
        Wrapper.ShowManageButton = true;

        if (validator.IsValid)
        {
            if (!Page.IsPostBack)
            {
                int startRowNumber;
                int endRowNumber;

                if (IsResourceUsesInOrionReport)
                {
                    StartRowNumber = 0;
                    startRowNumber = StartRowNumber + 1;
                    endRowNumber = (RowNumbers > startRowNumber) ? RowNumbers : startRowNumber;

                    DisablePagingToolbar();
                    EosListDataBind(startRowNumber, endRowNumber);
                }
                else
                {
                    StartRowNumber = 0;
                    startRowNumber = StartRowNumber + 1;
                    endRowNumber = StartRowNumber + _pageSize;

                    EnablePagingToolbar();
                    EosListDataBind(startRowNumber, endRowNumber);
                }
            }
        }
        else
        {
            Wrapper.ShowManageButton = false;
            Wrapper.ShowEditButton = false;
        }

        base.OnLoad(e);
    }

    protected void btnNext5_Click(object sender, EventArgs e)
    {
        StartRowNumber += 5;

        var startRowNumber = StartRowNumber + 1;
        var endRowNumber = StartRowNumber + _pageSize;

        EnablePagingToolbar();
        EosListDataBind(startRowNumber, endRowNumber);
    }

    protected void btnPrevious5_Click(object sender, EventArgs e)
    {
        StartRowNumber -= 5;

        var startRowNumber = StartRowNumber + 1;
        var endRowNumber = StartRowNumber + _pageSize;

        EnablePagingToolbar();
        EosListDataBind(startRowNumber, endRowNumber);
    }

    protected void btnShowAll_Click(object sender, EventArgs e)
    {
        StartRowNumber = 0;

        var startRowNumber = StartRowNumber + 1;
        var endRowNumber = RowNumbers;

        DisablePagingToolbar();
        EosListDataBind(startRowNumber, endRowNumber);
    }

    protected void repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (repeater.Items.Count == 0 && e.Item.ItemType == ListItemType.Footer)
        {
            var ctrl = e.Item.FindControl(@"EmptyData");
            ctrl.Visible = true;
        }
    }

    private void EosListDataBind(int startRowNumber, int endRowNumber)
    {
        var swql =
            $@"SELECT NCMNodeProperties.EndOfSupport, EntityPhysical.Model AS PartNumber, Count(NCMNodeProperties.NodeID) AS TargetNodes
                FROM Cirrus.NodeProperties AS NCMNodeProperties
	            LEFT JOIN (SELECT NodeID, MIN(Model) AS Model FROM Cirrus.EntityPhysical WHERE (EntityClass='3') GROUP BY NodeID) AS EntityPhysical
		            ON EntityPhysical.NodeID=NCMNodeProperties.NodeID
                INNER JOIN Orion.Nodes AS Nodes ON NCMNodeProperties.CoreNodeID=Nodes.NodeID
                {GetWhereClause(@"NCMNodeProperties")}
                GROUP BY NCMNodeProperties.EndOfSupport, EntityPhysical.Model
                ORDER BY NCMNodeProperties.EndOfSupport ASC
                WITH ROWS {startRowNumber} TO {endRowNumber}
                WITH LIMITATION {Resource.View.LimitationID}";

        using (var proxy = _isLayer.Proxy)
        {
            var dt = proxy.Query(swql);
            repeater.DataSource = dt;
            repeater.DataBind();
        }
    }

    private void EnablePagingToolbar()
    {
        var bEnabled = true;

        bEnabled = !(StartRowNumber == 0);
        btnPrevious5.Enabled = bEnabled;
        btnPrevious5.CssClass = bEnabled ? @"enabled" : @"disabled";

        bEnabled = ((RowNumbers - StartRowNumber) > 5);
        btnNext5.Enabled = bEnabled;
        btnNext5.CssClass = bEnabled ? @"enabled" : @"disabled";

        btnShowAll.Enabled = (RowNumbers > 5);
        btnShowAll.CssClass = (RowNumbers > 5) ? @"enabled" : @"disabled";

        if (RowNumbers > 0)
        {
            lblPagingInfo.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_98, StartRowNumber + 1, (RowNumbers - (StartRowNumber + 5)) > 0 ? StartRowNumber + 5 : RowNumbers, RowNumbers);
        }
        else
        {
            lblPagingInfo.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_98, 0, 0, 0);
        }
    }

    private void DisablePagingToolbar()
    {
        StartRowNumber = 0;

        btnPrevious5.Enabled = false;
        btnPrevious5.CssClass = @"disabled";

        btnNext5.Enabled = false;
        btnNext5.CssClass = @"disabled";

        btnShowAll.Enabled = false;
        btnShowAll.CssClass = @"disabled";

        lblPagingInfo.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_98, 1, RowNumbers, RowNumbers);
    }

    private string GetWhereClause(string tableAlias)
    {
        var period = GetPeriod();
        var date = TimeParser.GetTime(period);

        var endOfSupport = $@" {tableAlias}.EndOfSupport < DateTime('{date.Date:o}') ";

        return string.Format(@" WHERE (({0}.EosType={1} OR {0}.EosType={2}) AND ({3}))",
            tableAlias,
            (int)eEoSType.Manual,
            (int)eEoSType.User,
            endOfSupport);
    }

    private bool IsResourceUsesInOrionReport
    {
        get
        {
            var uri = Page.Request.Url;
            var absPath = uri.AbsolutePath;

            if (absPath.StartsWith(@"/Orion/Report", StringComparison.InvariantCultureIgnoreCase))
                return true;

            return false;
        }
    }

    private string GetPeriod()
    {
        var period = Resource.Properties[@"Period"];
        if (string.IsNullOrEmpty(period))
        {
            period = _defaultPeriod;
        }
        return period;
    }

    private string GetFilterComparison(string period)
    {
        switch (period)
        {
            case "Next 3 months":
                return @"lt3";
            case "Next 6 months":
                return @"lt6";
            case "Next year":
                return @"lt12";
            case "Next 2 years":
                return @"lt24";
            case "Next 3 years":
                return @"lt36";
            default:
                return @"lt3";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}