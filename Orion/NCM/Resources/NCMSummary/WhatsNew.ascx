<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WhatsNew.ascx.cs" Inherits="Orion_NCM_Resources_NCMSummary_WhatsNew" %>

<script type="text/javascript" src="/Orion/NCM/JavaScript/main.js"></script>
<link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />

<style type="text/css">
    .whatsNew_Text {
        margin: 0px;
        padding: 0px 0px 0px 10px;
    }

    .whatsNew_Comments {
        font-size: 12px;
        font-family: Arial,Verdana,Helvetica,sans-serif;
        padding: 15px 0px 3px 5px;
    }

    .whatsNew_Text li {
        list-style-image: url('/Orion/NCM/Resources/images/bullet.gif');
        font-size: 11px;
        font-family: Arial,Verdana,Helvetica,sans-serif;
        margin-left: 15px;
        margin-right: 10px;
    }
</style>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <table>
            <tr>
                <td style="border: 0px;" valign="top">
                    <span>
                        <img alt="" style="padding-right: 5px;" src="<%= SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath(@"NCM", @"WhatsNew/Icon_New.gif")%>" />
                    </span>
                </td>
                <td style="border: 0px;">
                    <ul class="whatsNew_Text">
                        <li>
                            <div class="whatsNew_Comments">
                                <b><%=Resources.NCMWebContent.NCMSummary_WhatsNew_Header1_2020_2 %></b>
                                <ul class="whatsNew_Text">
                                    <li>
                                         <div class="whatsNew_Comments">
                                             <b><%=Resources.NCMWebContent.NCMSummary_WhatsNew_Header1_1_2020_2 %></b>
                                         </div>
                                    </li>
                                    <li>
                                        <div class="whatsNew_Comments">
                                            <b><%=Resources.NCMWebContent.NCMSummary_WhatsNew_Header1_2_2020_2 %></b>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="whatsNew_Comments">
                                            <b><%=Resources.NCMWebContent.NCMSummary_WhatsNew_Header1_3_2020_2 %></b>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div class="whatsNew_Comments">
                                <b><%=Resources.NCMWebContent.NCMSummary_WhatsNew_Header2_2020_2 %></b>
                            </div>
                        </li>
                        <li>
                            <div class="whatsNew_Comments">
                                <b><%=Resources.NCMWebContent.NCMSummary_WhatsNew_Header3_2020_2 %></b>
                            </div>
                        </li>
                        <li>
                            <div class="whatsNew_Comments">
                                <b><%=Resources.NCMWebContent.NCMSummary_WhatsNew_Header4_2020_2 %></b>
                            </div>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="border: 0px; text-align: right; padding-top: 10px;">
                    <div class="sw-btn-bar">
                        <orion:LocalizableButtonLink runat="server" ID="btnLearnMore" LocalizedText="CustomText" DisplayType="Primary" Target="_blank" CssClass="sw-btn" />
                        <orion:LocalizableButton runat="server" ID="btnRemoveResource" LocalizedText="CustomText" Text="<%$Resources: NCMWebContent, WEBDATA_AP_4%>" DisplayType="Secondary" OnClick="btnRemoveResource_Click" CssClass="sw-btn" />
                    </div>
                </td>
            </tr>
        </table>
    </Content>
</orion:resourceWrapper>
