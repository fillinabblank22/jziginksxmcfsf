<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GlobalSearch.ascx.cs" Inherits="Orion_NCM_Resources_NCMSummary_GlobalSearch" %>

<orion:Include runat="server" Module="NCM" File="NCMResources.css" />
<orion:Include runat="server" Module="NCM" File="main.js" />

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:Panel ID="ContentContainer" runat="server">
            <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td style="padding-left:5px;border:0px groove transparent;">
                        <asp:TextBox ID="SearchTextBox" runat="server" Width= "200px" ValidationGroup="GroupMaster"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="SearchTextBoxValidator" runat="server" ValidationGroup="GroupMaster" ControlToValidate="SearchTextBox" Display="Dynamic">*</asp:RequiredFieldValidator>
                        <orion:LocalizableButton ID="SearchButton" runat="server" ValidationGroup="GroupMaster" DisplayType="Secondary" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_180 %>" OnClick="SearchButton_Click"></orion:LocalizableButton>
                    </td>
                </tr>
                <tr>
                    <td  style="padding-left:5px;padding-top:10px;border:0px groove transparent;">
                        <asp:LinkButton ID="AdvancedSearch" runat="server" CausesValidation="false" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_179 %>" Font-Underline="True" ForeColor="#BCB619" Font-Size="8pt" OnClick="AdvancedSearch_Click" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <div style="border:0px groove transparent;" id="ErrorContainer" runat="server"/>
    </Content>
</orion:resourceWrapper>

