using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.Charting.v2;
using System.Collections.Generic;
using SolarWinds.NCMModule.Web.Resources;
using Constants = SolarWinds.NCM.Contracts.Constants;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.PieCharts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsPieChartsValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NCM_Resources_NCMSummary_BaselineVsRunningChart : StandardChartResource
{

    #region private Veriables
    private int resourceID, chartID = -1;
    private string limitationID = string.Empty;
    private Dictionary<string, object> parameters;

    #endregion

    #region Resource life cycle
    protected void Page_Init(object sender, EventArgs e)
    {

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetupChart();
        HandleInit(WrapperContents);
    }
    #endregion

    #region Resource properties

    protected override string DefaultTitle
    {
        get { return Resources.NCMWebContent.WEBDATA_VK_210; }
    }

    public override string HelpLinkFragment
    {
        get { return @"OrionNCMWebBaselineVsRunning"; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            var viewManagerAdapter = ServiceLocator.Container.Resolve<IViewManager>();
            return viewManagerAdapter.GetSupportedInterfacesForViewType(Constants.ViewTypeNCMSummary);
        }
    }

    public sealed override string DisplayTitle
    {
        get
        {
            return !string.IsNullOrEmpty(this.Resource.Properties[@"Title"]) ? Resource.Properties[@"Title"] : Resources.NCMWebContent.WEBDATA_VK_210;
        }
    }

    public override string EditURL
    {
        get { return ExtendCustomEditUrl(@"/Orion/NCM/Resources/NCMCommonResources/GeneralChartEdit.aspx?HideTimeRange=true"); }
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        return new string[0];
    }

    protected override string NetObjectPrefix
    {
        get { return @"NCM_SUMMARY"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode { get { return SolarWinds.Orion.Web.UI.ResourceLoadingMode.Ajax; } }

    #endregion

    #region Initialize resource
  

    private void SetupChart()
    {
        Resource.SubTitle = Resources.NCMWebContent.WEBDATA_VK_184;
        limitationID = this.Resource.View.LimitationID.ToString();
        string filterClause = Resource.Properties[@"FilterClause"];
        filterClause = string.IsNullOrEmpty(filterClause) ? @"1=1" : filterClause;
        parameters = new Dictionary<string, object>();

        resourceID = Resource.ID;
        chartID = 2;

        parameters.Add(@"resourceID", resourceID);
        parameters.Add(@"chartID", chartID);
        
        parameters.Add(@"limitationID", limitationID);
      
        parameters.Add(@"FilterClause", filterClause);

        Resource.Properties[@"ChartName"] = @"BaselineVsRunning";
        Resource.Properties[@"TimeRange"] = @"As of Last Update";
    }

    protected override Dictionary<string, object> GenerateDisplayDetails()
    {
        var details = base.GenerateDisplayDetails();
        details[@"ResourceID"] = Resource.ID;
        details[@"NCM_BaselineVsRunningChartDataParameters"] = parameters;
        return details;
    }

    protected override void AddChartExportControl(System.Web.UI.Control resourceWrapperContents)
    {
        //there is no need for export button
    }

    #endregion
}
