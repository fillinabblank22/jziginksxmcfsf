using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.InformationService.Linq.Plugins.NCM.Cirrus;
using SolarWinds.NCM.Common.Dal;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;

using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using Constants = SolarWinds.NCM.Contracts.Constants;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Reports)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_NCM_Resources_NCMSummary_PendingApprovalList : BaseResourceControl
{
    private readonly ISwisRepositoryFactory swisRepositoryFactory;

    public Orion_NCM_Resources_NCMSummary_PendingApprovalList()
    {
        swisRepositoryFactory = ServiceLocator.Container.Resolve<ISwisRepositoryFactory>();
    }

    #region Protected Members
    /// <summary>
    /// Show/hide resource.
    /// </summary>
    protected bool VisibleResource
    {
        get { return Convert.ToBoolean(WebSettingsDAL.Get(ConfigChangeApprovalHelper.NCM_ENABLE_APPROVAL_SYSTEM, @"false")); }
    }
    /// <summary>
    /// Start row number.
    /// </summary>
    protected int StartRowNumber
    {
        get { return Convert.ToInt32(ViewState["NCM_StartRowNumber"]); }
        set { ViewState["NCM_StartRowNumber"] = value; }
    }

    /// <summary>
    /// Total row numbers of pendig approval requests list.
    /// </summary>
    protected int RowNumbers
    {
        get 
        {
            if (ViewState["NCM_RowNumbers"] == null || Convert.ToInt32(ViewState["NCM_RowNumbers"]) == 0)
            {
                using (var repo = swisRepositoryFactory.CreateHttpContextRepository())
                {
                    string swql =
                        $@"SELECT COUNT(A.ID) AS RowNumbers FROM Cirrus.ApproveQueue AS A {GetWhereClause(@"A")}";

                    DataTable dt = repo.Query(swql);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        ViewState["NCM_RowNumbers"] = dt.Rows[0][0];
                        return Convert.ToInt32(ViewState["NCM_RowNumbers"]);
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            else
            {
                return Convert.ToInt32(ViewState["NCM_RowNumbers"]);
            }
        }
        set { ViewState["NCM_RowNumbers"] = value; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NCMWebContent.WEBDATA_VK_97; }
    }
    
    protected string FormatTime(object time)
    {
        DateTime t = (DateTime)time;
        return $@"{Utils.FormatCurrentCultureDate(t)} {Utils.FormatToLocalTime(t)}";
    }

    protected string LookupRequestStatus(object status)
    {
        return ConfigChangeApprovalHelper.LookupLocalizedRequestStatus((eApprovalStatus)status);
    }

    protected string LookupRequestType(object type)
    {
        return ConfigChangeApprovalHelper.LookupLocalizedRequestType(Convert.ToString(type));
    }

    protected string LookupRequestStatusImageSrc(object approvalStatus)
    {
        switch ((eApprovalStatus)approvalStatus)
        {
            case eApprovalStatus.PendingApproval: return @"/Orion/NCM/Resources/images/ConfigChangeApproval/execute_icon_pending.png";
            case eApprovalStatus.NeedConfirmation: return @"/Orion/NCM/Resources/images/ConfigChangeApproval/execute_icon_pending.png";
            case eApprovalStatus.Declined: return @"/Orion/NCM/Resources/images/ConfigChangeApproval/execute_icon_declined.png";
            case eApprovalStatus.WaitingForExecution: return @"/Orion/NCM/Resources/images/ConfigChangeApproval/execute_icon_waiting_for_execution.png";
            case eApprovalStatus.Scheduled: return @"/Orion/NCM/Resources/images/ConfigChangeApproval/execute_icon_scheduled.png";
            case eApprovalStatus.Complete: return @"/Orion/NCM/Resources/images/ConfigChangeApproval/execute_icon_complete.png";
            case eApprovalStatus.Executing: return @"/Orion/NCM/Resources/images/ConfigChangeApproval/execute_icon_executing.png";
            default: throw new ArgumentOutOfRangeException(nameof(approvalStatus));
        }
    }

    #endregion

    #region Public Properties

    public sealed override string DisplayTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(Resource.Properties[@"Title"]))
            {
                return Resource.Properties[@"Title"];
            }

            return Resources.NCMWebContent.WEBDATA_VK_97;
        }
    }

    public sealed override string SubTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(Resource.Properties[@"SubTitle"]))
            {
                return Resource.Properties[@"SubTitle"];
            }

            return string.Empty;
        }
    }

    public override string EditURL
    {
        get { return ExtendCustomEditUrl(@"/Orion/NCM/Resources/NCMCommonResources/NCMCommonEdit.aspx"); }
    }

    public override string HelpLinkFragment
    {
        get { return @"OrionNCMPHPendingApprovalList"; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            var viewManagerAdapter = ServiceLocator.Container.Resolve<IViewManager>();
            return viewManagerAdapter.GetSupportedInterfacesForViewType(Constants.ViewTypeSummary);
        }
    }

    #endregion

    #region Event Handlers

    protected override void OnLoad(EventArgs e)
    {
        SWISValidator validator = new SWISValidator();
        validator.ContentContainer = ContentContainer;
        validator.ResourceContainer = Wrapper;
        ErrorContainer.Controls.Add(validator);

        Wrapper.ManageButtonText = Resources.NCMWebContent.WEBDATA_VK_88;
        Wrapper.ManageButtonTarget = @"/Orion/NCM/Admin/ConfigChangeApproval/ApprovalRequests.aspx";
        Wrapper.ShowManageButton = true;

        if (validator.IsValid)
        {
            Wrapper.Visible = VisibleResource;
            if (!Page.IsPostBack)
            {
                if (VisibleResource)
                {
                    EnablePaging();
                    FillPendingApprovalList(false);
                }
            }
        }
        else
        {
            Wrapper.ShowManageButton = false;
            Wrapper.ShowEditButton = false;
        }

        base.OnLoad(e);
    }

    protected void btnNext5_Click(object sender, EventArgs e)
    {
        StartRowNumber += 5;
        EnablePaging();
        FillPendingApprovalList(false);
    }

    protected void btnPrevious5_Click(object sender, EventArgs e)
    {
        StartRowNumber -= 5;
        EnablePaging();
        FillPendingApprovalList(false);
    }

    protected void btnShowAll_Click(object sender, EventArgs e)
    {
        StartRowNumber = 0;
        DisablePaging();
        FillPendingApprovalList(true);
    }

    protected void repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (repeater.Items.Count < 1)
        {
            if (e.Item.ItemType == ListItemType.Footer)
            {
                Control footer = e.Item.FindControl(@"Footer");
                footer.Visible = true;
            }
        }
    }

    #endregion

    #region Private Methods
    /// <summary>
    /// Fill approval requests list by specified data source.
    /// </summary>
    private void FillPendingApprovalList(bool showAll)
    {
        string swql;

        if (showAll)
        {
            swql = $@"SELECT  
                    A.ID,
                    A.UserName,
                    A.RequestType,
                    A.RequestStatus,
                    0 AS TargetNodes,
                    A.DateTime
                FROM Cirrus.NCM_ApproveQueueView AS A
                {GetWhereClause(@"A")}
                ORDER BY A.DateTime ASC";
        }
        else
        {
            int pageSize = 5;

            int startRowNumber = StartRowNumber + 1;
            int endRowNumber = StartRowNumber + pageSize;

            swql = string.Format(
                @"SELECT  
                    A.ID,
                    A.UserName,
                    A.RequestType,
                    A.RequestStatus,
                    0 AS TargetNodes,
                    A.DateTime
                FROM Cirrus.NCM_ApproveQueueView AS A
                {2}
                ORDER BY A.DateTime ASC
                WITH ROWS {0} TO {1}",
                startRowNumber,
                endRowNumber,
                GetWhereClause(@"A"));
        }

        using (var repo = swisRepositoryFactory.CreateHttpContextRepository())
        {
            DataTable dt = repo.Query(swql);
            PopulateTargetNodesCount(dt, repo);
            repeater.DataSource = dt;
            repeater.DataBind();
        }
    }

    private void PopulateTargetNodesCount(DataTable dt, ISwisRepository repo)
    {
        foreach (DataRow row in dt.Rows)
        {
            var requestId = new Guid(row["ID"].ToString());
            row["TargetNodes"] = repo.Get<ApproveQueueNodes>().Count(n => n.ApproveQueueID == requestId);
        }
    }

    /// <summary>
    /// Enable/disable paging buttons.
    /// </summary>
    private void EnablePaging()
    {
        bool bEnabled = true;

        bEnabled = !(StartRowNumber == 0);
        btnPrevious5.Enabled = bEnabled;
        btnPrevious5.CssClass = bEnabled ? @"enabled" : @"disabled";

        bEnabled = ((RowNumbers - StartRowNumber) > 5);
        btnNext5.Enabled = bEnabled;
        btnNext5.CssClass = bEnabled ? @"enabled" : @"disabled";

        if (RowNumbers > 0)
        {
            lblInfoPaging.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_98, StartRowNumber + 1, (RowNumbers - (StartRowNumber + 5)) > 0 ? StartRowNumber + 5 : RowNumbers, RowNumbers);
        }
        else
        {
            btnShowAll.Enabled = false;
            btnShowAll.CssClass = @"disabled";

            lblInfoPaging.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_98, 0, 0, 0);
        }
    }

    /// <summary>
    /// Disable paging and hide paging text.
    /// </summary>
    private void DisablePaging()
    {
        StartRowNumber = 0;

        btnPrevious5.Enabled = false;
        btnPrevious5.CssClass = @"disabled";
        
        btnNext5.Enabled = false;
        btnNext5.CssClass = @"disabled";

        btnShowAll.Enabled = false;
        btnShowAll.CssClass = @"disabled";

        lblInfoPaging.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_98, 1, RowNumbers, RowNumbers);
    }

    /// <summary>
    /// Gets where clause.
    /// </summary>
    /// <param name="tableAlias">The table alias.</param>
    /// <returns></returns>
    private string GetWhereClause(string tableAlias)
    {
        if (CommonHelper.IsDemoMode())
        {
            return $@" WHERE {tableAlias}.RequestStatus = 0";
        }

        eUserApproveRole role = ConfigChangeApprovalHelper.GetCurrentUserApprovalRole();

        switch (role)
        {
            case eUserApproveRole.Requestor:
                return $@" WHERE {tableAlias}.UserName='{HttpContext.Current.Profile.UserName}' "; //only his requests in any state  
            case eUserApproveRole.SingleApprover:
            case eUserApproveRole.ApproverGroup1:
            case eUserApproveRole.RequestorAndApproverGroup1:
                return string.Format(@" WHERE {0}.RequestStatus = {1} or {0}.UserName='{2}'", tableAlias, (int)eApprovalStatus.PendingApproval, HttpContext.Current.Profile.UserName);  //only pending approval requests from all users plus own requests in any state
            case eUserApproveRole.ApproverGroup2:
            case eUserApproveRole.RequestorAndApproverGroup2:
                return string.Format(@" WHERE {0}.RequestStatus = {1} or {0}.UserName='{2}'", tableAlias, (int)eApprovalStatus.NeedConfirmation, HttpContext.Current.Profile.UserName);  //only need confirmation requests from all users plus own requests in any state
            default:
                return $@" WHERE {tableAlias}.UserName='{HttpContext.Current.Profile.UserName}' "; //for all other users just show only their requests
        }        
    }

    #endregion

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
