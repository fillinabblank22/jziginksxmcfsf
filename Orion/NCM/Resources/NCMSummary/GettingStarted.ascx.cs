﻿using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;

using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Help)]
public partial class Orion_NCM_Resources_NCMSummary_GettingStarted : BaseResourceControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        Wrapper.Visible = Profile.AllowAdmin && SecurityHelper.IsValidNcmAccountRole;
        Wrapper.ShowEditButton = false;
    }

    protected string DiscoveryCentralLink
    {
        get { return @"/Orion/Admin/DiscoveryCentral.aspx"; }
    }

    protected string SetupApprovalSystemLink
    {
        get { return @"/Orion/NCM/Admin/ConfigChangeApproval/ApprovalSetup.aspx"; }
    }

    protected string NCMSettingsLink
    {
        get { return @"/Orion/NCM/Admin/Default.aspx"; }
    }

    protected string NCMGlobalDeviceDefaultsLink
    {
        get { return @"/Orion/NCM/Admin/Settings/GlobalDeviceDefaults.aspx"; }
    }    

    protected override string DefaultTitle
    {
        get { return Resources.NCMWebContent.WEBDATA_VK_351; }
    }

    protected void btnRemoveResource_Click(object sender, EventArgs eventArgs)
    {
        ResourceManager.DeleteById(Resource.ID);
        Page.Response.Redirect(Request.Url.ToString());
    }
}