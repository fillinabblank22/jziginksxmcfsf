﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using System.Globalization;
using Constants = SolarWinds.NCM.Contracts.Constants;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.PieCharts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsPieChartsValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NCM_Resources_NCMSummary_BackupVsAllDevices : StandardChartResource
{
    #region Private members

    private ISWrapper isLayer = new ISWrapper();
    private Log log = new Log();
    private Dictionary<string, string> parameters;
    private string dateFrom = string.Empty;
    private string dateTo = string.Empty;
    private string granularity = string.Empty;

    #endregion

    #region Resource life cycle

    protected void Page_Load(object sender, EventArgs e)
    {
        SetupChart();
        InitializeChartDataQuery();

        InitializeChartProperties();
        HandleInit(WrapperContents);
    }

    #endregion

    #region Resource properties

    protected override string DefaultTitle
    {
        get { return Resources.NCMWebContent.WEBDATA_VK_221; }
    }

    public override string HelpLinkFragment
    {
        get { return @"OrionNCMWebBackupVsAllDevices"; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            var viewManagerAdapter = ServiceLocator.Container.Resolve<IViewManager>();
            return viewManagerAdapter.GetSupportedInterfacesForViewType(Constants.ViewTypeNCMSummary);
        }
    }

    public sealed override string DisplayTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(this.Resource.Properties[@"Title"]))
            {
                return this.Resource.Properties[@"Title"];
            }
            return Resources.NCMWebContent.WEBDATA_VK_221;
        }
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        return new string[0];
    }

    protected override string NetObjectPrefix
    {
        get { return @"NCM_SUMMARY"; }
    }

    public override string EditURL
    {
        get { return ExtendCustomEditUrl(@"/Orion/NCM/Resources/NCMCommonResources/BackupVsAllDevicesChartEdit.aspx"); }
    }

    public override ResourceLoadingMode ResourceLoadingMode { get { return SolarWinds.Orion.Web.UI.ResourceLoadingMode.Ajax; } }

    #endregion

    #region Chart initialization

    private void InitializeChartProperties()
    {
        Resource.Properties[@"ChartName"] = @"BackupVsAllDevices";
    }

    protected override Dictionary<string, object> GenerateDisplayDetails()
    {
        var details = base.GenerateDisplayDetails();
        details[@"NCM_BackupVsAllDevicesNewChartDataParameters"] = parameters;

        return details;
    }

    protected override void AddChartExportControl(System.Web.UI.Control resourceWrapperContents)
    {
        //there is no need for export button
    }

    private void SetupTimeRange()
    {
        dateFrom = Resource.Properties[@"DateFrom"];
        dateTo = Resource.Properties[@"DateTo"];

        if (!string.IsNullOrEmpty(Resource.Properties[@"TimeRange"]) && Resource.Properties[@"TimeRange"] != @"Custom")
        {
            DateTime dateTimeTo = DateTime.Now;
            string strDateTimeTo = string.Empty;
            if (Resource.Properties[@"TimeRange"].Equals(@"Last Month", StringComparison.InvariantCultureIgnoreCase))
            {
                //special fix for last month
                dateTimeTo = new DateTime(dateTimeTo.Year, dateTimeTo.Month,
                        dateTimeTo.AddDays(-dateTimeTo.Day + 1).Day).AddMilliseconds(-1);
                strDateTimeTo = dateTimeTo.ToString(CultureInfo.InvariantCulture);
            }
            dateFrom = TimeParser.GetTime(Resource.Properties[@"TimeRange"]).ToString(CultureInfo.InvariantCulture);
            dateTo = strDateTimeTo;
        }

        if (string.IsNullOrEmpty(dateTo))
        {
            if (string.IsNullOrEmpty(dateFrom))
            {
                NPMDefaultSettingsHelper helper = new NPMDefaultSettingsHelper(Resource);
                dateFrom = DateTime.Now.AddDays(-7).ToString(CultureInfo.InvariantCulture);
                helper.SetSetting("DateFrom", dateFrom);
                helper.SetSetting("TimeRange", "Last 7 Days");
                helper.SetSetting("DateTo", string.Empty);
            }
            Resource.SubTitle = string.Format(Resources.NCMWebContent.WEBDATA_VK_185, Convert.ToDateTime(dateFrom, CultureInfo.InvariantCulture).ToString());
        }
        else
            Resource.SubTitle = string.Format(Resources.NCMWebContent.WEBDATA_VK_186, Convert.ToDateTime(dateFrom, CultureInfo.InvariantCulture).ToString(), Convert.ToDateTime(dateTo, CultureInfo.InvariantCulture).ToString());
    }

    private void SetupChart()
    {
        granularity = string.IsNullOrEmpty(Resource.Properties[@"Granularity"]) ? @"Daily" : Resource.Properties[@"Granularity"].ToString();

        if (string.IsNullOrEmpty(Resource.Properties[@"Granularities"])) Resource.Properties[@"Granularities"] = @"Daily;Weekly;Monthly";

        SetupTimeRange();
    }

    private void InitializeChartDataQuery()
    {
        parameters = new System.Collections.Generic.Dictionary<string, string>();
        parameters.Add(@"@startDate", dateFrom);        
        parameters.Add(@"@endDate", dateTo);
        parameters.Add(@"@granularity", granularity);
    }

    #endregion
}