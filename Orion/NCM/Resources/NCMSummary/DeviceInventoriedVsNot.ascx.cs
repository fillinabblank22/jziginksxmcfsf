using System;
using System.Globalization;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;

using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.Charting.v2;
using System.Collections.Generic;
using Constants = SolarWinds.NCM.Contracts.Constants;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.PieCharts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsPieChartsValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NCM_Resources_NCMCommonResources_DeviceInventoriedVsNot : StandardChartResource
{
    #region private Veriables

    private int resourceID, chartID = -1;
    private string dateFrom;
    private string dateTo;
    private Dictionary<string, object> parameters;
    private string limitationID = string.Empty;

    #endregion

    #region Resource life cycle

    protected void Page_Init(object sender, EventArgs e)
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetupTimeRange();
        SetupChart();
        HandleInit(WrapperContents);
    }

    #endregion

    #region Resource properties

    protected override string DefaultTitle
    {
        get { return Resources.NCMWebContent.WEBDATA_VK_183; }
    }

    public override string HelpLinkFragment
    {
        get { return @"OrionNCMWebInventoriedVsNot"; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            var viewManagerAdapter = ServiceLocator.Container.Resolve<IViewManager>();
            return viewManagerAdapter.GetSupportedInterfacesForViewType(Constants.ViewTypeNCMSummary);
        }
    }

    public sealed override string DisplayTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(this.Resource.Properties[@"Title"]))
                return this.Resource.Properties[@"Title"];
            return Resources.NCMWebContent.WEBDATA_VK_183;
        }
    }

    public override string EditURL
    {
        get { return ExtendCustomEditUrl(@"/Orion/NCM/Resources/NCMCommonResources/GeneralChartEdit.aspx"); }
    }

    public override ResourceLoadingMode ResourceLoadingMode { get { return SolarWinds.Orion.Web.UI.ResourceLoadingMode.Ajax; } }

    #endregion

    #region Initialize resource

    private void SetupTimeRange()
    {
        dateFrom = Resource.Properties[@"DateFrom"];
        dateTo = Resource.Properties[@"DateTo"];

        if (!string.IsNullOrEmpty(Resource.Properties[@"TimeRange"]) && Resource.Properties[@"TimeRange"] != @"Custom")
        {
            DateTime dateTimeTo = DateTime.Now;
            string strDateTimeTo = string.Empty;
            if (Resource.Properties[@"TimeRange"].Equals(@"Last Month", StringComparison.InvariantCultureIgnoreCase))
            {
                //special fix for last month
                dateTimeTo = new DateTime(dateTimeTo.Year, dateTimeTo.Month,
                    dateTimeTo.AddDays(-dateTimeTo.Day + 1).Day).AddMilliseconds(-1);
                strDateTimeTo = dateTimeTo.ToString(CultureInfo.InvariantCulture);
            }
            dateFrom = TimeParser.GetTime(Resource.Properties[@"TimeRange"]).ToString(CultureInfo.InvariantCulture);
            dateTo = strDateTimeTo;
        }

        if (string.IsNullOrEmpty(dateTo))
        {
            if (string.IsNullOrEmpty(dateFrom))
            {
                NPMDefaultSettingsHelper helper = new NPMDefaultSettingsHelper(Resource);

                helper.SetSetting("DateFrom", DateTime.Now.ToString(CultureInfo.InvariantCulture));
                helper.SetSetting("TimeRange", "As of Last Update");
                helper.SetSetting("DateTo", string.Empty);
            }
            if (!string.IsNullOrEmpty(Resource.Properties[@"TimeRange"]) && Resource.Properties[@"TimeRange"] == @"As of Last Update")
                Resource.SubTitle = Resources.NCMWebContent.WEBDATA_VK_184;
            else
                Resource.SubTitle = string.Format(Resources.NCMWebContent.WEBDATA_VK_185, Convert.ToDateTime(dateFrom, CultureInfo.InvariantCulture).ToString());
        }
        else
            Resource.SubTitle = string.Format(Resources.NCMWebContent.WEBDATA_VK_186, Convert.ToDateTime(dateFrom, CultureInfo.InvariantCulture).ToString(), Convert.ToDateTime(dateTo, CultureInfo.InvariantCulture).ToString());

    }

    private void SetupChart()
    {
        bool isLastUpdate = !string.IsNullOrEmpty(Resource.Properties[@"TimeRange"]) && Resource.Properties[@"TimeRange"] == @"As of Last Update" ? true : false;
        limitationID = this.Resource.View.LimitationID.ToString();
        string filterClause = Resource.Properties[@"FilterClause"];
        filterClause = string.IsNullOrEmpty(filterClause) ? @"1=1" : filterClause;

        Resource.Properties[@"ChartName"] = @"DeviceInventoriedVsNot";

        resourceID = Resource.ID;
        chartID = 5;

        parameters = new Dictionary<string, object>();
        parameters.Add(@"resourceID", resourceID);
        parameters.Add(@"chartID", chartID);
        parameters.Add(@"isLastUpdate", isLastUpdate);
        parameters.Add(@"limitationID", limitationID);
        parameters.Add(@"FilterClause", filterClause);

        parameters.Add(@"@startDate", dateFrom);
        parameters.Add(@"@endDate", dateTo);
    }
    protected override Dictionary<string, object> GenerateDisplayDetails()
    {
        var details = base.GenerateDisplayDetails();
        details[@"NCM_DeviceInventoriedVsNoChartDataParameters"] = parameters;

        return details;
    }

    protected override void AddChartExportControl(System.Web.UI.Control resourceWrapperContents)
    {
        //there is no need for export button
    }

    #endregion

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        return new string[0];
    }

    protected override string NetObjectPrefix
    {
        get { return @"NCM_SUMMARY"; }
    }
}
