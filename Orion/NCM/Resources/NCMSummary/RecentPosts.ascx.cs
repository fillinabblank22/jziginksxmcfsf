using System;
using System.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Reports)]
public partial class Orion_NCM_Resources_NCMCommonResources_RecentPosts : BaseResourceControl
{
    private const int DefaultNumberOfPosts = 20;


    protected override void OnInit(EventArgs e)
    {
        this.resourceWrapper.Visible = SecurityHelper.IsValidNcmAccountRole;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.AddStylesheet(@"/Orion/styles/Thwack.css");
        this.HelpButton.HelpUrlFragment = this.HelpLinkFragment;
    }

    public string ResourceID
    {
        get { return Resource.ID.ToString(); }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public int Count
    {
        get
        {
            int count;
            if (int.TryParse(Resource.Properties[@"PostsCount"], out count))
            {
                return count;
            }
            return DefaultNumberOfPosts;
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NCMWebContent.WEBDATA_VK_181; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            var viewManagerAdapter = ServiceLocator.Container.Resolve<IViewManager>();
            return viewManagerAdapter.GetSupportedInterfacesForViewType(@"NCMSummary");
        }
    }

    public sealed override string DisplayTitle
    {
        get { return String.Empty; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return @"OrionNCMWebRecentPosts";
        }
    }

    public override string EditURL
    {
        get { return ExtendCustomEditUrl(@"/Orion/NCM/Resources/NCMCommonResources/EditRecentPosts.aspx"); }
    }
}
