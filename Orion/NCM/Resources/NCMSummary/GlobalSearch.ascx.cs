using System;
using SolarWinds.NCM.Web.Contracts.Search;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;

using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Help)]
public partial class Orion_NCM_Resources_NCMSummary_GlobalSearch : BaseResourceControl
{
    protected override string DefaultTitle
    {
        get { return Resources.NCMWebContent.WEBDATA_VK_178; }
    }

    public override string HelpLinkFragment
    {
        get { return @"OrionNCMWebGlobalSearch"; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            var viewManagerAdapter = ServiceLocator.Container.Resolve<IViewManager>();
            return viewManagerAdapter.GetSupportedInterfacesForViewType(@"NCMSummary");
        }
    }

    public sealed override string DisplayTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(this.Resource.Properties[@"Title"]))
            { 
                return this.Resource.Properties[@"Title"]; 
            }
            return Resources.NCMWebContent.WEBDATA_VK_178;
        }
    }

    public sealed override string SubTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(this.Resource.Properties[@"SubTitle"]))
            {
                return this.Resource.Properties[@"SubTitle"];
            }
            return string.Empty;
        }
    }
    
    public override string EditURL
    {
        get { return ExtendCustomEditUrl(@"/Orion/NCM/Resources/NCMCommonResources/NCMCommonEdit.aspx"); }
    }

    protected override void OnLoad(EventArgs e)
    {
        SWISValidator validator = new SWISValidator();
        validator.ContentContainer = ContentContainer;
        validator.ResourceContainer = Wrapper;
        ErrorContainer.Controls.Add(validator);

        if (!validator.IsValid)
        {
            Wrapper.ShowEditButton = false;
        }

        base.OnLoad(e);
    }

    protected void SearchButton_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(SearchTextBox.Text.Trim()))
        {
            ShowSearchResultPage(SearchMode.Result);
        }
    }

    protected void AdvancedSearch_Click(object sender, EventArgs e)
    {
        ShowSearchResultPage(SearchMode.Search);
    }

    private void ShowSearchResultPage(SearchMode mode)
    {
        Session[@"SearchParam"] = SearchTextBox.Text.Trim();
        Session[@"SearchMode"] = mode;
        Session[@"SearchType"] = SearchType.Everything;
        Session[@"NodeSearchField"] = NodeSearchFields.Everything;
        Session[@"SearchedConfigs"] = SearchedConfigs.LastDownloadedConfigs;
        Session[@"ConfigTypes"] = @"AllConfigTypes";

        Page.Response.Redirect("/Orion/NCM/SearchResult.aspx");
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
