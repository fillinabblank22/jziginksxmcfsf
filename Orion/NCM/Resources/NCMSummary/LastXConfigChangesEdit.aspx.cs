using System;
using System.Web.UI;
using Infragistics.WebUI.WebDataInput;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.Resources;
using SolarWinds.NCMModule.Web.Resources.Wrappers;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NCM_Resources_LastXConfigChangesEdit : Page
{
    private readonly IEditResourcePageService editResourceService;

    public Orion_NCM_Resources_LastXConfigChangesEdit()
    {
        editResourceService = ServiceLocator.Container.Resolve<IEditResourcePageService>();
    }

    protected ResourceInfo Resource { get; private set; }
    protected string ResourceHeader => editResourceService.GetHeader(Resource);

    protected override void OnInit(EventArgs e)
    {
        Resource = editResourceService.Init(new PageWrap(this), ResourceTitle, ResourceSubTitle);

        base.OnInit(e);

        SetupControlFromRequest();
    }

    protected void chkViewChangesOn_OnCheckedChanged(object sender, EventArgs e)
    {
        ddlConfigType.Enabled = chkViewChangesOn.Checked;
    }

    private void SetupControlFromRequest()
    {
        ConfigsNumeric.Text =
            !string.IsNullOrEmpty(Resource.Properties[@"LastX"]) ? Resource.Properties[@"LastX"] : @"5";
        chkViewChangesOn.Checked = !string.IsNullOrEmpty(Resource.Properties[@"ViewChangesOn"]) &&
                                   Convert.ToBoolean(Resource.Properties[@"ViewChangesOn"]);
        ddlConfigType.Enabled = chkViewChangesOn.Checked;

        if (!string.IsNullOrEmpty(Resource.Properties[@"ConfigType"]))
        {
            ddlConfigType.SelectedValue = Resource.Properties[@"ConfigType"];
        }

        ConfigsNumeric.DataMode = NumericDataMode.Uint;
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        editResourceService.SaveChanges(Resource, ResourceTitle, ResourceSubTitle);

        Resource.Properties[@"LastX"] = ConfigsNumeric.Text;
        Resource.Properties[@"ViewChangesOn"] = chkViewChangesOn.Checked.ToString();
        Resource.Properties[@"ConfigType"] = ddlConfigType.SelectedValue;

        Response.Redirect(BaseResourceControl.GetEditPageReturnUrl(Request.QueryString, Resource.View.ViewID.ToString()));
    }
}
