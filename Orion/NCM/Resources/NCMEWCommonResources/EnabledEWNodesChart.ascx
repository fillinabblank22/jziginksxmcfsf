<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EnabledEWNodesChart.ascx.cs" Inherits="EnabledEWNodesChart" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div style="margin-bottom:10px;">
            <%= Resources.NCMWebContent.WEBDATA_VK_260%>
        </div>
        <div style="overflow: hidden !important;">
            <asp:PlaceHolder runat="server" ID="WrapperContents"></asp:PlaceHolder>
        </div>
    </Content>
</orion:resourceWrapper>
