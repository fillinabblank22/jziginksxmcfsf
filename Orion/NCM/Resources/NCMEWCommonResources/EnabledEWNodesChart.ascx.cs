using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;

using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.Charting.v2;
using System.Collections.Generic;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.PieCharts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsPieChartsValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class EnabledEWNodesChart : StandardChartResource
{
    #region Private members

    private bool isNPMExist = false;
    private int resourceID = -1;
    private string subtitle = string.Empty;
    #endregion

    #region Resource life cycle

    protected void Page_Init(object sender, EventArgs e)
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (CommonHelper.IsNpmInstalled() && SecurityHelper.IsValidNcmAccountRole)
        {
            InitializeChartProperties();
            HandleInit(WrapperContents);
            isNPMExist = true;
        }
        else
        {
            HandleInit(WrapperContents);
            Wrapper.Visible = false;
        }
    }

    #endregion

    #region Resource properties

    protected override string DefaultTitle
    {
        get { return Resources.NCMWebContent.WEBDATA_VK_259; }
    }

    public override string HelpLinkFragment
    {
        get { return @"OrionNCMWebEnabledEnergyWisePorts"; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            var viewManagerAdapter = ServiceLocator.Container.Resolve<IViewManager>();
            return viewManagerAdapter.GetSupportedInterfacesForViewType(@"Summary");
        }
    }

    public sealed override string DisplayTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(this.Resource.Properties[@"Title"]))
            {
                return this.Resource.Properties[@"Title"];
            }

            return Resources.NCMWebContent.WEBDATA_VK_259;
        }
    }

    public sealed override string SubTitle
    {
        get
        {
            subtitle = this.Resource.Properties[@"SubTitle"];
            if (!string.IsNullOrEmpty(subtitle))
            {
                return subtitle;
            }

            return string.Empty;
        }
    }


    public override string EditURL
    {
        get { return ExtendCustomEditUrl(@"/Orion/NCM/Resources/NCMCommonResources/NCMCommonEdit.aspx"); }
    }

    public override ResourceLoadingMode ResourceLoadingMode { get { return SolarWinds.Orion.Web.UI.ResourceLoadingMode.Ajax; } }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        return new string[0];
    }

    protected override string NetObjectPrefix
    {
        get { return @"NCM_SUMMARY"; }
    }

    #endregion

    #region Chart initialization

    private void InitializeChartProperties()
    {
        Resource.Properties[@"ChartName"] = @"EnabledEWNodesChart";
        Resource.SubTitle = SubTitle;
        resourceID = this.Resource.ID;   
    }

    protected override Dictionary<string, object> GenerateDisplayDetails()
    {
        var details = base.GenerateDisplayDetails();
        details[@"isNPMExist"] = isNPMExist;
        details[@"resourceID"] = resourceID;

        return details;
    }

    protected override void AddChartExportControl(System.Web.UI.Control resourceWrapperContents)
    {
        //there is no need for export button
    }

    #endregion
}