using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class EWNodeGridConflict : BaseResourceControl
{
    private readonly EWNodeGrid nodeGrid = new EWNodeGrid();
    private ResourceInfo resource = new ResourceInfo();

    private static readonly string ewNotEnabledQuery = @"SELECT Nodes.NodeID, Nodes.IP_Address, 
    Nodes.Caption, Nodes.VendorIcon, Nodes.GroupStatus
    FROM Orion.NPM.EW.Readiness AS NpmEwReadiness INNER JOIN Orion.Nodes AS Nodes
    ON NpmEwReadiness.NodeID = Nodes.NodeID
    WHERE NpmEwReadiness.HasEnergyWise = 0 
    AND NpmEwReadiness.HasRightIOS = 1 
    AND NpmEwReadiness.IsEnergyWiseCapable = 1";

    public override string DetachURL => string.IsNullOrEmpty(Request.QueryString[@"PreviewMode"])
        ? $@"{base.DetachURL}&ChartResourceID={Request.QueryString[@"ChartResourceID"] ?? Request.QueryString[@"ResourceID"]}"
        : $@"{base.DetachURL}{(Request.QueryString[@"PreviewMode"] != null ? $@"&PreviewMode={Request.QueryString[@"PreviewMode"]}" : string.Empty)}";

    protected override string DefaultTitle => Resources.NCMWebContent.WEBDATA_VK_266;

    public sealed override string DisplayTitle => Resources.NCMWebContent.WEBDATA_VK_263;

    public override string HelpLinkFragment => @"OrionNCMWebEWCapable";

    public override string SubTitle => string.Format(Resources.NCMWebContent.WEBDATA_VK_267, nodeGrid.CountNodes);

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces =>
        ServiceLocator.Container.Resolve<IViewManager>().GetSupportedInterfacesForViewType(@"NCMEWChartDetails");

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        Wrapper.ShowEditButton = false;
        Wrapper.ShowManageButton = false;

        if (CommonHelper.IsNpmInstalled() && SecurityHelper.IsValidNcmAccountRole)
        {
            SetupNodeGrid();
        }
        else
        {
            Wrapper.Visible = false;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    private void SetupNodeGrid()
    {
        if (string.IsNullOrEmpty(Request.QueryString[@"PreviewMode"]))
        {
            if (!string.IsNullOrEmpty(Request.QueryString[@"ResourceID"]))
            {
                var resourceId = !string.IsNullOrEmpty(Request.QueryString[@"ChartResourceID"])
                    ? this.Request[@"ChartResourceID"]
                    : this.Request[@"ResourceID"];
                resource = ResourceManager.GetResourceByID(Convert.ToInt32(resourceId));
                resource.ID = Convert.ToInt32(resourceId);
            }
        }
        else
        {
            // Preview Mode
            resource.Title = Resources.NCMWebContent.WEBDATA_VK_263;
        }

        Resource.Title = resource.Title;
        nodeGrid.SwqlDataSource = ewNotEnabledQuery;
        nodeGrid.LegendColors = @"252,217,40";
        nodeGrid.ResourceContainer = Wrapper;
        Wrapper.Content.Controls.Add(nodeGrid);
    }
}
