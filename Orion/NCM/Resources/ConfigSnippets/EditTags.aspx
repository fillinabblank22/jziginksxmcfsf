<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMEditMasterPage.master" AutoEventWireup="true" CodeFile="EditTags.aspx.cs" Inherits="Orion_NCM_Resources_ConfigSnippets_EditTags" Title="Untitled Page" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ncmPageTitlePlaceHolder">
	<%=Page.Title %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmHelpIconPlaceHolder" runat="server">
    <orion:IconHelpButton ID="helpBtn" runat="server" HelpUrlFragment="OrionNCMAGTagConfigTemplate" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" runat="server">
    <ncm:DropDownMapPath ID="NodeSiteMapPath" Provider="NCMSitemapProvider" runat="server"  OnInit="SiteMapPath_OnInit">
        <RootNodeTemplate>
            <a href="<%# Eval(@"url") %>" ><u> <%# Eval(@"title") %></u> </a>
        </RootNodeTemplate>
        <CurrentNodeTemplate>
	        <a href="<%# Eval(@"url") %>" ><%# Eval(@"title") %> </a>
        </CurrentNodeTemplate>
    </ncm:DropDownMapPath>
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
    <link rel="Stylesheet" type="text/css" href="../../styles/NCMResources.css" />
    <script type="text/javascript" src="../../JavaScript/main.js"></script>
    
    <div style="padding:10px;">

        <table>
		    <tbody>			    
			    <tr id="TagsHolder" runat="server">
				    <td>
					    <asp:UpdatePanel ID="upTags" UpdateMode="Conditional" runat="server">
						    <ContentTemplate>
							    <table>
								    <tr>
									    <td>
										    <asp:RadioButton ID="radioButtonExistingTags" GroupName="TagRB" Text="<%$ Resources:NCMWebContent, WEBDATA_VM_24%>" Checked="true" OnCheckedChanged="OnRadioButtonTags_CheckedChanged" AutoPostBack="true" runat="server" />
									    </td>
								    </tr>
								    <tr>
									    <td id="contentExistingTags" runat="server">
										    <div style="padding-left:20px;width:600px">
										        <table class="blueBox">
										            <tr>
										                <td>
											                <asp:Repeater ID="repeaterExistingTags" runat="server">
												                <HeaderTemplate>
													                <table cellspacing="0" cellpadding="0">
												                </HeaderTemplate>
												                <ItemTemplate>
													                <tr>
														                <td style="width:30px;">
															                <asp:CheckBox ID="tagSelected" runat="server" />
															                <input id="tagName" type="hidden" value='<%# Container.DataItem %>' runat="server" />
														                </td>
														                <td align="left"><%# SolarWinds.NCMModule.Web.Resources.CommonHelper.EncodeXmlToString(Container.DataItem.ToString()) %></td>
													                </tr>
												                </ItemTemplate> 
												                <FooterTemplate>
													                </table>
												                </FooterTemplate>
											                </asp:Repeater>
											            </td>
											         </tr>
											     </table>
										    </div>
									    </td>
								    </tr>
								    <tr>
									    <td>
		        						    <asp:RadioButton ID="radioButtonAddTags" GroupName="TagRB" Text="<%$ Resources:NCMWebContent, WEBDATA_VM_25%>" OnCheckedChanged="OnRadioButtonTags_CheckedChanged" AutoPostBack="true" runat="server" />
									    </td>
								    </tr>
								    <tr>
									    <td id="contentAddTags" runat="server">
										    <div style="padding-left:20px;width:600px">
											    <table cellspacing="0" cellpadding="0" class="blueBox">
												    <tr>
													    <td style="padding:5px 5px 0px 5px;">
														    <asp:TextBox ID="txtAddTags" BorderColor="#89A6C0" BorderWidth="1px" Width="400" runat="server"/>
														</td>
														<td class="Property" align="left" style="padding:5px 5px 0px 5px;">
												            <asp:CustomValidator runat="server" ID="validatorTags" ControlToValidate="txtAddTags" 
                                                                ErrorMessage="<%$ Resources:NCMWebContent, WEBDATA_VM_17%>" Display="Dynamic" Enabled="true" ClientValidationFunction="ValidateTags">
                                                            </asp:CustomValidator>                                                          
												        </td>
													</tr>
													<tr>
													    <td colspan="2" style="padding-left:5px;padding-bottom:5px;">
	                                                        <span style="font-size:7pt;"><%=Resources.NCMWebContent.WEBDATA_VM_26 %></span>   
													    </td>
												    </tr>												    
											    </table>
										    </div>
									    </td>
								    </tr>
								    <tr>
									    <td>
		        						    <asp:RadioButton ID="radioButtonRemoveTags" GroupName="TagRB" Text="<%$ Resources:NCMWebContent, WEBDATA_VM_27%>" OnCheckedChanged="OnRadioButtonTags_CheckedChanged" AutoPostBack="true" runat="server" />
									    </td>
								    </tr>
								    <tr>
									    <td id="contentRemoveTags" runat="server">
										    <div style="padding-left:20px;width:600px">
										        <table class="blueBox">
										            <tr>
										                <td>
											                <asp:Repeater ID="repeaterRemoveTags" runat="server">
												                <HeaderTemplate>
													                <table cellspacing="0" cellpadding="0">
												                </HeaderTemplate>
												                <ItemTemplate>
													                <tr>
														                <td style="width:30px;">
															                <asp:CheckBox ID="tagSelected" runat="server"/>
															                <input id="tagName" type="hidden" value='<%# Container.DataItem %>' runat="server" />
														                </td>
														                <td align="left"><%# SolarWinds.NCMModule.Web.Resources.CommonHelper.EncodeXmlToString(Container.DataItem.ToString()) %></td>
													                </tr>
												                </ItemTemplate> 
												                <FooterTemplate>
													                </table>
												                </FooterTemplate>
											                </asp:Repeater>
											                <span id="contentRemoveTagsNA" visible="false" runat="server"><%= Resources.NCMWebContent.WEBDATA_VM_28 %></span>
											            </td>
											         </tr>
											     </table>
										    </div>
									    </td>
								    </tr>
							    </table>
						    </ContentTemplate>
					    </asp:UpdatePanel>
				    </td>
			    </tr>
			    <tr>
			        <td class="Property">
                        <asp:Label ID="lblError" runat="server" ForeColor="Red" Visible="false"></asp:Label>
                    </td>
			    </tr>
		    </tbody>
		    <tfoot>
		        <tr>
		            <td>
		                <br/>
                        <orion:LocalizableButton runat="server" ID="Submit" LocalizedText="submit" DisplayType="Primary" OnClick="SubmitClick"  />
                        <orion:LocalizableButton runat="server" ID="Cancel" LocalizedText="Cancel" DisplayType="secondary" OnClick="CancelClick" CausesValidation="false"  />
		            </td>
		        </tr>
		    </tfoot>
	    </table>
	    
    </div>
</asp:Content>

