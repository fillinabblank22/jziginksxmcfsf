<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMEditMasterPage.master" AutoEventWireup="true" CodeFile="ExScriptPreview.aspx.cs" Inherits="Orion_NCM_Resources_ConfigSnippets_ExScriptPreview" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
    <div style="padding:10px;">
        <asp:Label ID="snippetName" runat="server"></asp:Label>
        <br />
        <asp:TextBox ID="scriptContent" runat="server" 
                     Width="800px" Height="700px" ReadOnly="true"
                     TextMode="MultiLine" 
                     BorderStyle="solid" 
                     BorderColor="#dcdbd7" 
                     BorderWidth="1px">
        </asp:TextBox>
    </div>
</asp:Content>

