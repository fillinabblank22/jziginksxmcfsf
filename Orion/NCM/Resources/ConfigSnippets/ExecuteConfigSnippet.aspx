<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="ExecuteConfigSnippet.aspx.cs" Inherits="Orion_NCM_Resources_ConfigSnippets_ExecuteConfigSnippet" %>

<%@ Register TagPrefix="ncmModal" TagName="ModalBox" Src="~/Orion/NCM/ModalBox.ascx"  %>
<%@ Register Src="~/Orion/NCM/CustomPageLinkHidding.ascx" TagPrefix="ncm" TagName="custPageLinkHidding" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder">
    <ncm:DropDownMapPath ID="NodeSiteMapPath" Provider="NCMSitemapProvider" runat="server"  OnInit="SiteMapPath_OnInit">
        <RootNodeTemplate>
            <a href="<%# Eval(@"url") %>" ><u> <%# Eval(@"title") %></u> </a>
        </RootNodeTemplate>
        <CurrentNodeTemplate>
	        <a href="<%# Eval(@"url") %>" ><%# Eval(@"title") %> </a>
        </CurrentNodeTemplate>
    </ncm:DropDownMapPath>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ncmPageTitlePlaceHolder">
    <%=PageTitle %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmMainContentPlaceHolder" runat="Server">
    <ncm:custPageLinkHidding  runat="server" /> 
    <link rel="Stylesheet" type="text/css" href="../../styles/NCMResources.css" />
	<script type="text/javascript" src="../../JavaScript/main.js"></script>
    <orion:Include runat="server" File="styles/ProgressIndicator.css" />

    <div id="ncmResourceWrapper" style="padding:10px;">	    
        <table>
            <tr>
                <td>
                    <asp:UpdatePanel id="ContentUpdatePanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel ID="ExecuteConfigSnippet" runat="server">
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel> 
                </td>
            </tr>
        </table>
        <asp:UpdateProgress ID="Progress" runat="server">
        </asp:UpdateProgress>
    </div>
    
    <ncmModal:ModalBox runat="server" ID="DataPickerModalBox">
        <DialogContents>
            <div style="border-right: 1px solid; border-top: 1px solid; border-left: 1px solid; border-bottom: 1px solid;">
                <div style="background-image: url('/Orion/NCM/Resources/images/PopupHeader.gif'); background-repeat: repeat-x; background-position: left bottom; margin: 0 0 0 0; padding: 5px 5px 5px 5px;">
                    <table width="100%">
                        <tr>
                            <td style="font-weight:bold;">
                                <asp:Label ID="exDataPickerTitle" runat="server" Font-Bold="true" EnableViewState="true"></asp:Label>
                            </td>
                            <td align="right">
                                <asp:ImageButton runat="server" OnClick="DataPickerCancelHandler" id="CloseMsgBox1" ImageUrl="/Orion/NCM/Resources/images/Button.CloseCross.gif" />
                            </td>                                
                        </tr>
                    </table>
                </div>
                <table width="100%">
                    <tr>
                        <td id="DataPickerContainer" runat="server" style="padding-top:5px;padding-bottom:5px;">
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <orion:LocalizableButton ID="DataPickerOk" runat="server" DisplayType="Primary" LocalizedText="Ok" OnClick="DataPickerOkHandler"></orion:LocalizableButton>
                            <orion:LocalizableButton ID="DataPickerCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="DataPickerCancelHandler"></orion:LocalizableButton>
                        </td>
                    </tr>
                 </table>
            </div>
        </DialogContents>
    </ncmModal:ModalBox>
    
    <ncmModal:ModalBox runat="server" ID="MsgBox">
        <DialogContents>
            <div style="border-right: 1px solid; border-top: 1px solid; border-left: 1px solid; border-bottom: 1px solid;">
                <div style="background-image: url('/Orion/NCM/Resources/images/PopupHeader.gif'); background-repeat: repeat-x; background-position: left bottom; margin: 0 0 0 0; padding: 5px 5px 5px 5px;">
                    <table width="100%">
                        <tr>
                            <td class="modalBoxHeaderFont">
                                <%=titleMsgBox%>
                            </td>
                            <td align="right">
                                <asp:ImageButton runat="server" OnClick="CancelHandler" id="CloseMsgBox" ImageUrl="/Orion/NCM/Resources/images/Button.CloseCross.gif" />
                            </td>                                
                        </tr>
                    </table>
                </div>
                <table width="100%">
                    <tr>
                        <td style="padding:5px;">
                            <%=contentMsgBox%>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <orion:LocalizableButton ID="Ok" runat="server" DisplayType="Primary" LocalizedText="Ok" OnClick="TransferHandler"></orion:LocalizableButton>
                            <orion:LocalizableButton ID="Cancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="CancelHandler"></orion:LocalizableButton>
                        </td>
                    </tr>
                 </table>
            </div>
        </DialogContents>
    </ncmModal:ModalBox>
    
    <ncmModal:ModalBox runat="server" ID="ProgressModalBox">
        <DialogContents>
            <div style="border-right: 1px solid; border-top: 1px solid; border-left: 1px solid; border-bottom: 1px solid;">
                <div style="background-image: url('/Orion/NCM/Resources/images/PopupHeader.gif'); background-repeat: repeat-x; background-position: left bottom; margin: 0 0 0 0; padding: 5px 5px 5px 5px;">
                    <table width="100%">
                        <tr>
                            <td style="font-size:12pt;font-weight:bold;">
                                <%=Resources.NCMWebContent.WEBDATA_VK_491 %>
                            </td>
                        </tr>
                    </table>
                </div>
                <asp:UpdatePanel id="ContentUpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="background-image: url('/Orion/NCM/Resources/images/bg_popup.jpg'); background-repeat: no-repeat; background-position:top; margin: 0 0 0 0;">
                            <table width="100%">
                                <tr>
                                    <td colspan="2" style="padding:5px">
                                       <b><%=Resources.NCMWebContent.WEBDATA_VK_492 %></b>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td align="right"   style="padding-top:10px;padding-bottom:5px;width:30%">
                                        <img src="../images/animated_loading_sm3_whbg.gif" />
                                        
                                    </td>
                                    <td align="left"  style="padding-top:10px;padding-bottom:5px;width:70%">
                                        <asp:label ID="ProgressLabel" runat="server" text="Label"></asp:label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" colspan="2" style="width:50%;padding:10px">
                                        <orion:LocalizableButton ID="GenCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="GenCancelHandler"></orion:LocalizableButton>
                                    </td>
                                </tr>
                             </table>
                        </div>
                        <asp:timer runat="server" ID="ProgressTimer" Enabled="false"></asp:timer>
                    </ContentTemplate>
                </asp:UpdatePanel> 
            </div>
        </DialogContents>
    </ncmModal:ModalBox>
</asp:Content>
