﻿/**
 * NOTE: This function was taking from the ExtJs library.  I didn't want to pull in all of ExtJs just to define namespaces.
 * Creates namespaces to be used for scoping variables and classes so that they are not global.  Usage:
 * <pre><code>
        Ext.namespace('Company', 'Company.data');
        Company.Widget = function() { ... }
        Company.data.CustomStore = function(config) { ... }
   </code></pre>
 * @param {String} namespace1
 * @param {String} namespace2
 * @param {String} etc
 * @method namespace
 */
MakeNamespace = function(){
    var a=arguments, o=null, i, j, d, rt;
    for (i=0; i<a.length; ++i) {
        d=a[i].split(".");
        rt = d[0];
        eval('if (typeof ' + rt + ' == "undefined"){' + rt + ' = {};} o = ' + rt + ';');
        for (j=1; j<d.length; ++j) {
            o[d[j]]=o[d[j]] || {};
            o=o[d[j]];
        }
    }
},

MakeNamespace("SW", "SW.NCM");

//thwack stuff
SW.NCM.logInToThwack = function(callback, params, showWarning) {

    function val(id) { return Ext.getCmp(id).getValue().trim(); }

    function onValidate(ctrIndex, text) {
        dlg.buttons[0].setDisabled(val("tliUN") == "");
        return true;
    }

    function onLogIn() {
        ORION.callWebService(
            "/Orion/NCM/Services/Thwack.asmx",
            "LogIn",
            { userName: val("tliUN"), password: val("tliPSW") },
            function(result) {
                thwackUserInfo = { name: val("tliUN"), pass: val("tliPSW"), valid: false };
                onCancel();
                if (callback) {
                    callback(params);
                }
            }
        );
    }

    function onCancel() {
        dlg.hide();
        dlg.destroy();
        dlg = null;
    }

    var dlg = new Ext.Window({
        title: '@{R=NCM.Strings;K=WEBJS_VM_47;E=js}',
        width: 300,
        border: false,
        region: "center",
        layout: "fit",
        modal: true,
        resizable: false,
        bodyStyle: "padding:5px;",
        items: [
            new Ext.form.FormPanel({
                frame: true,
                labelWidth: 110,
                autoHeight: true,
                defaultType: "textfield",
                labelAlign: "right",
                items: showWarning
                    ? [
                        {
                            xtype: "label",
                            html: '@{R=NCM.Strings;K=WEBJS_VM_48;E=js}',
                            style: "margin-left:20px;color:#666;"
                        },
                        { xtype: "label", html: "<hr style='width:300px;color:#666;'/>" },
                        {
                            id: "tliUN",
                            fieldLabel: '@{R=NCM.Strings;K=WEBJS_VM_49;E=js}',
                            anchor: "90%",
                            selectOnFocus: true,
                            validator: onValidate,
                            value: thwackUserInfo.name
                        },
                        {
                            id: "tliPSW",
                            inputType: "password",
                            fieldLabel: '@{R=NCM.Strings;K=WEBJS_VM_50;E=js}',
                            anchor: "90%",
                            selectOnFocus: true,
                            validator: onValidate,
                            value: thwackUserInfo.pass
                        }
                    ]
                    : [
                        {
                            id: "tliUN",
                            fieldLabel: '@{R=NCM.Strings;K=WEBJS_VM_49;E=js}',
                            anchor: "90%",
                            selectOnFocus: true,
                            validator: onValidate,
                            value: thwackUserInfo.name
                        },
                        {
                            id: "tliPSW",
                            inputType: "password",
                            fieldLabel: '@{R=NCM.Strings;K=WEBJS_VM_50;E=js}',
                            anchor: "90%",
                            selectOnFocus: true,
                            validator: onValidate,
                            value: thwackUserInfo.pass
                        }
                    ]
            })
        ],
        buttons: [
            { text: '@{R=NCM.Strings;K=WEBJS_VM_51;E=js}', disabled: true, handler: onLogIn },
            { text: '@{R=NCM.Strings;K=WEBJS_VK_38;E=js}', handler: onCancel }
        ]
    });

    if (thwackUserInfo.valid != true)
        dlg.show();
    else
        callback(params);
};

SW.NCM.validateThwackCred = function(state) {

    ORION.callWebService("/Orion/NCM/Services/Thwack.asmx",
        "ValidateCred",
        { state: state },
        function(result) {
            thwackUserInfo.valid = state;
        });
};
//config snippets stuff

SW.NCM.highlightSearchText = function (selector, searchTerm) {

    var quoteForRegExp = function (str) {
        var toQuote = '\\/[](){}?+*|.^$'; // note: backslash must be first
        for (var i = 0; i < toQuote.length; ++i) {
            str = replaceAll(str, '\\' + toQuote.charAt(i), '\\' + toQuote.charAt(i));
        }
        return str;
    };


    // search term highlighting adapted from http://dossy.org/archives/000338.html
    var re = new RegExp('(' + quoteForRegExp(searchTerm) + ')', 'ig');
    $(selector + " *").each(function () {
        if ($(this).children().size() > 0) return;
        var html = $(this).html();
        var newhtml = html.replace(re, '<span class="ncm_searchterm">$1</span>');
        $(this).html(newhtml);
    });
};
function replaceAll(txt, replace, with_this) {
  return txt.replace(new RegExp(replace, 'g'),with_this);
}
SW.NCM.ColumnIndexByName = function(columnModel, columnsName){
    var ids =[];
    if(!columnModel || !columnModel.config || !columnsName )
        return [];
    
    Ext.each(columnsName, function(columnName){
        
        var res = columnModel.getColumnsBy(function(c)
                                            {
                                               if(c)
                                                return c.header == columnName;
                                               else
                                                return false;
                                            }
                                         );
      if(res[0])
        ids.push(res[0].id);
    });
    
    return ids;
}

SW.NCM.ColumnIndexByDataIndex = function (columnModel, columnsDataIndex) {
    var ids = [];
    if (!columnModel || !columnModel.config || !columnsDataIndex)
        return [];

    Ext.each(columnsDataIndex, function (columnDataIndex) {

        var res = columnModel.getColumnsBy(function (c) {
            if (c)
                return c.dataIndex == columnDataIndex;
            else
                return false;
        });

        if (res[0])
            ids.push(res[0].id);
    });

    return ids;
}
// demo mode

SW.NCM.IsDemoMode = function()
{
  return $("#isDemoMode").length > 0
}

SW.NCM.ShowDemoModeExtMsg = function () {
    Ext.Msg.show({
        title: '@{R=NCM.Strings;K=DemoMode_DialogTitle;E=js}',
        msg: '@{R=NCM.Strings;K=DemoMode_DialogMessage;E=js}',
        buttons: Ext.Msg.OK,
        icon: Ext.MessageBox.INFO
    });

    return false;
}

// grid personalization

 SW.NCM.PersonalizeGrid  = function( grid, columns ){
        if(!grid && !colums) return;
        
        
        var cmconfig = grid.getColumnModel().config;
        var cm = grid.getColumnModel();
        
        // ignore checkbox and id columns
        var position = 2;
        if( columns )
        {
             for (var i = 0; i < columns.length; i++)
             {
                var res = cm.getColumnsBy(function(c)
                                            {
                                               if(c && c.dataIndex)
                                                return c.dataIndex == columns[i].id;
                                               else
                                                return false;
                                            }
                                         );
                if(res[0])
                {
                    var cc = res[0];
                    cc.width = columns[i].w;
                    cc.hidden = columns[i].v ==1?false:true;
                    
                    cmconfig.splice(cc.id,1,cc);
                    cm.moveColumn(cc.id,position);
                    reviceColumnPosition();
                    position++;
                }
             }
         }
         
        function reviceColumnPosition  ()
        {
            for( var j = 2; j <cmconfig.length; j++)
            {
                cmconfig[j].id = j;
            }
        }
    };

SW.NCM.GetDefaultColumnModelIfNeeded = function(columnmodel) {
    for (var i = 0; i < columnmodel.length; i++) {
        if (columnmodel[i].v == 1) {
            return columnmodel;
        }
    }
    //return [] column model if all columns are hidden
    //in this case grid use default(own) column model
    return [];
};

SW.NCM.GridColumnsToJson = function(columnmodel) {
    var cmconfig = columnmodel.config;
    var list = [];
    // skip checkbox and id columns
    for (var i = 2; i < cmconfig.length; i++) {
        var obj = {};
        obj.id = cmconfig[i].dataIndex;
        obj.w = cmconfig[i].width;
        obj.v = cmconfig[i].hidden == true ? 0 : 1;
        list.push(obj);
    }

    var jsonstate = Ext.util.JSON.encode(list);
    return jsonstate;
};
 
 var showDescriptionDialog;
SW.NCM.ShowDescription = function(obj) {

    if (!showDescriptionDialog) {
        showDescriptionDialog = new Ext.Window({
            applyTo: 'snippetDescriptionDialog',
            layout: 'fit',
            width: 500,
            height: 300,
            closeAction: 'hide',
            plain: true,
            resizable: false,
            modal: true,
            contentEl: 'snippetDescriptionBody',
            buttons: [
                {
                    text: '@{R=NCM.Strings;K=WEBJS_VY_86;E=js}',
                    handler: function() { showDescriptionDialog.hide(); }
                }
            ]
        });
    }

    // Set the window caption
    showDescriptionDialog.setTitle(Ext.util.Format.htmlEncode(obj.text()));

    // Set the location
    showDescriptionDialog.alignTo(document.body, "c-c");
    showDescriptionDialog.show();

    return false;
};

Ext.Button.override({
    setTooltip: function(qtipText) {
        var btnEl = this.getEl().child(this.buttonSelector)
        Ext.QuickTips.register({
            target: btnEl.id,
            text: qtipText
        });
    }
});

Ext.getUrlParam = function(param) {
    var params = Ext.urlDecode(location.search.substring(1));
    return param ? params[param] : params;
};

SW.NCM.checkDemoMode = function(name) {
    if (SW.NCM.IsDemoServer) {
        demoAction(name, this);
        return true;
    } else {
        return false;
    }
};
