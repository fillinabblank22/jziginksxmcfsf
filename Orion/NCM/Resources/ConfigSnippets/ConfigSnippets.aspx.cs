using System;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_ConfigSnippets : OrionView
{
    public bool IsDemoMode;
    private const string NCM_VIEW_TYPE = "NCMConfigSnippets";

    protected String _thwackUserInfo = @"{name:'',pass:'',valid:false}";

    private readonly ICommonHelper commonHelper;

    public Orion_NCM_ConfigSnippets()
    {
        commonHelper = ServiceLocator.Container.Resolve<ICommonHelper>();
    }

    protected override void OnInit(EventArgs e)
    {
        commonHelper.Initialise();
        IsDemoMode = SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;

        var validator = new SWISValidator
        {
            ContentContainer = ContentContainer,
            RedirectIfValidationFailed = true
        };
        ErrorContainer.Controls.Add(validator);

        base.OnInit(e);
    }

    public override string ViewType => NCM_VIEW_TYPE;

    protected void Page_Load(object sender, EventArgs e)
    {
        System.Net.NetworkCredential cred = Session[@"NCM_ThwackCredential"] as System.Net.NetworkCredential;
        
        bool credValid;
        object vc = Session[@"NCM_ThwackCredValidation"] ;
        if (vc != null)
            credValid = (bool)vc;
        else
            credValid = false;

        if (cred != null)
        {
            this._thwackUserInfo = String.Format(@"{0}name:'{1}',pass:'{2}', valid:{4}{3}", 
                                     @"{", 
                                     cred.UserName.Replace(@"\", @"&#92;"), 
                                     cred.Password.Replace(@"\", @"&#92;"), 
                                     @"}", 
                                     credValid.ToString().ToLower());
        }

    }
}
