using System;
using SolarWinds.Orion.Web.UI;

using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_Resources_ConfigSnippets_Thwack_Default : OrionView
{
    private const string NCM_VIEW_TYPE = "NCMSharedConfigSnippets";
    protected String _thwackUserInfo = @"{name:'',pass:'', valid: false}";
    private static readonly string THWACK_SESSION_KEY = @"thwack_snippets_list";

    private SolarWinds.NCMModule.Web.Resources.ISWrapper _isLayer;
    public bool IsDemoMode;

    protected override void OnInit(EventArgs e)
    {
        _isLayer = new SolarWinds.NCMModule.Web.Resources.ISWrapper();
        
        IsDemoMode = SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;

        SWISValidator validator = new SWISValidator();
        validator.ContentContainer = ContentContainer;
        validator.RedirectIfValidationFailed = true;
        ErrorContainer.Controls.Add(validator);

        if (!IsPostBack)
            Session.Remove(THWACK_SESSION_KEY);

        base.OnInit(e);
    }

    public override string ViewType
    {
        get { return NCM_VIEW_TYPE; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        System.Net.NetworkCredential cred = Session[@"NCM_ThwackCredential"] as System.Net.NetworkCredential;

        bool credValid;
        object vc = Session[@"NCM_ThwackCredValidation"];

        if (vc != null)
            credValid = (bool)vc;
        else
            credValid = false;

        if (cred != null)
        {
            this._thwackUserInfo = String.Format(@"{0}name:'{1}',pass:'{2}', valid:{4}{3}",
                                     @"{",
                                     cred.UserName.Replace(@"\", @"&#92;"),
                                     cred.Password.Replace(@"\", @"&#92;"),
                                     @"}",
                                     credValid.ToString().ToLower());
        }
    }
}
