﻿Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.QuickTips.init();

SW.NCM.ConfigSnippets = function () {

    function gridResize() {
        if (grid != null) {
            grid.setWidth(1);
            grid.setWidth($('#gridCell').width());

            var h = getGridHeight()
            grid.setHeight(getGridHeight());

            var groupItems = $(".GroupItems");
            groupItems.height(h - $(".ncm_GroupSection").height() - 14);

            $('#footer').css({ position: 'absolute' });
        }
    }

    function getGridHeight() {
        var top = $('#gridCell').offset().top;
        return $(window).height() - top - $('#footer').height() - 100;
    }

    var getGroupByValues = function (groupBy, onSuccess) {
        var param = Ext.util.Format.htmlDecode(groupBy);
        ORION.callWebService("/Orion/NCM/Services/Thwack.asmx",
                             "GetValuesAndCountForProperty", { property: param },
                             function (result) {
                                 onSuccess(ORION.objectFromDataTable(result));
                             });
    };

    var importSnippets = function (ids, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/Thwack.asmx",
                             "ImportSnippets", { Ids: ids },
                             function (result) {
                                 onSuccess(result);
                             });
    };


    var loadGroupByValues = function () {
        var groupItems = $(".GroupItems").text("Loading...");

        getGroupByValues("tags", function (result) {
            groupItems.empty();
            $(result.Rows).each(function () {
                var value = Ext.util.Format.htmlEncode(this.theValue);
                var text = $.trim(String(this.theValue));
                if (text.length > 22) { text = String.format("{0}...", text.substr(0, 22)); };
                var disp = (text || "All") + " (" + this.theCount + ")";
                if (value === null) { disp = "All" + " (" + this.theCount + ")"; }
                $("<a href='#'></a>")
					.attr('value', value).attr('title', value)
					.text(disp).click(selectGroup).appendTo(".GroupItems").wrap("<li class='GroupItem'/>");
            });

            var toSelect = $(".GroupItem a[value='" + Ext.util.Format.htmlEncode(ORION.Prefs.load("GroupByValue")) + "']");
            if (toSelect.length === 0) {
                toSelect = $(".GroupItem a:first");
            }

            toSelect.click();
        });
    };

    var selectGroup = function () {
        $(this).parent().addClass("SelectedGroupItem").siblings().removeClass("SelectedGroupItem");

        var value = $(".SelectedGroupItem a").attr('value');
        value = Ext.util.Format.htmlDecode(value);
        ORION.Prefs.save('GroupByValue', value);

        refreshObjects("tags", value, "");
        return false;
    };

    var currentSearchTerm = null;
    var refreshObjects = function (property, value, search, callback) {
        grid.store.removeAll();
        grid.store.proxy.conn.jsonData = { property: property, value: value || "", search: search };
        currentSearchTerm = search;
        grid.store.load({ callback: callback });
    };

    var getSelectedSnippetIds = function (items) {
        var ids = [];

        Ext.each(items, function (item) {
            ids.push(item.data.Id);
        });

        return ids;
    };

    var importSelectedSnippets = function (items) {
        var selected = getSelectedSnippetIds(items);
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VM_35;E=js}');

        importSnippets(selected, function (result) {
            waitMsg.hide();

            var dlg, obj = Ext.decode(result);
            function onClose() { dlg.hide(); dlg.destroy(); dlg = null; }

            if (obj.isSuccess) {
                SW.NCM.validateThwackCred(true);
                function onView() {
                    onClose(); Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VM_36;E=js}');
                    ORION.callWebService("/Orion/Services/NodeManagement.asmx",
						"SaveUserSetting", { name: "NCM_ConfigSnippets_GroupByValue", value: "Imported From thwack" },
						function (result) { window.location = "/Orion/NCM/Resources/ConfigSnippets/ConfigSnippets.aspx"; }
					);
                }

                dlg = new Ext.Window({
                    title: '@{R=NCM.Strings;K=WEBJS_VM_37;E=js}', html: obj.msg,
                    width: 400, height: 90, border: false, region: "center", layout: "fit", modal: true, resizable: false, plain: true, bodyStyle: "padding:5px 0px 0px 10px;",
                    buttons: [
						{ text: '@{R=NCM.Strings;K=WEBJS_VM_38;E=js}', handler: onView }, { text: '@{R=NCM.Strings;K=WEBJS_VM_34;E=js}', handler: onClose }
					]
                });
                dlg.show();
            } else {
                SW.NCM.validateThwackCred(false);
                var dlg = new Ext.Window({
                    title: '@{R=NCM.Strings;K=WEBJS_VM_37;E=js}', html: String.format("<span style='color:#f00;'>{0}</span>", obj.msg),
                    width: 350, border: false, region: "center", layout: "fit", modal: true, resizable: false, plain: true, bodyStyle: "padding:5px 0px 0px 10px;",
                    buttons: [{ text: '@{R=NCM.Strings;K=WEBJS_VM_34;E=js}', handler: onClose}]
                });
                dlg.show();
            }
        });
    };

    var updateToolbarButtons = function () {
        var selCount = grid.getSelectionModel().getCount();
        var map = grid.getTopToolbar().items.map;

        var needsOnlyOneSelected = (selCount != 1);
        var needsAtLeastOneSelected = (selCount === 0);

        map.ImportButton.setDisabled(SW.NCM.IsPowerUser ? needsAtLeastOneSelected : true);

        // revice 'Select All' checkbox state
        var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');
        if (grid.getStore().getCount() > 0 && selCount == grid.getStore().getCount()) {
            hd.addClass('x-grid3-hd-checker-on');
        } else {
            hd.removeClass('x-grid3-hd-checker-on');
        }
    };

    // Column rendering functions

    function renderName(value, meta, record) {
        return String.format('<img src="/Orion/NCM/Resources/images/ConfigSnippets/icon_config_snippet.gif" />&nbsp;<a href="#" class="snippetName" value="{0}">{1}</a>', record.data.Id, Ext.util.Format.htmlEncode(value));
    }

    function renderTags(value, meta, record) {
        var tags;

        try {
            tags = Ext.decode(value);
        } catch (e) { }

        tags = tags || [];

        if (tags.length === 0) {
            return '@{R=NCM.Strings;K=WEBJS_VM_39;E=js}';
        }

        var links = [];
        for (var i = 0; i < tags.length; ++i) {
            links.push(String.format('<a href="#" class="tagLink">{0}</a>', Ext.util.Format.htmlEncode(tags[i])));
        }

        return links.join(', ');
    }


    function renderPublishDate(value, meta, record) {
        return value;
    }

    function renderRating(value, meta, record) {
        if (value == 0)
            return '@{R=NCM.Strings;K=WEBJS_VM_40;E=js}';

        // 1. start with 3.8333. 
        // 2. multiple by 10 and truncate (38) and then double (76)
        // 3. drop the ones digit (/ 10 and then * 10) => 70
        // 4. divide by 2 =  35
        var rating = parseInt(value * 10) * 2;
        rating = (parseInt(rating / 10) * 10) / 2;
        rating = "000" + rating;
        rating = rating.substr(rating.length - 2);

        return String.format('<img src="/Orion/NCM/Resources/images/ConfigSnippets/stars/stars_{0}.gif" />', rating);
    }

    function jumpToTag(tagName) {
        var tn = Ext.util.Format.htmlEncode(tagName);
        $(".GroupItem a[value='" + tn + "']").click();
    }

    var showDescriptionDialog;

    function showDescription(obj) {

        var snippetId = obj.attr("value");
        var body = $("#snippetDescriptionBody").text('@{R=NCM.Strings;K=WEBJS_VM_29;E=js}');

        ORION.callWebService("/Orion/NCM/Services/Thwack.asmx",
                             "GetSnippetDescription", { snippetId: snippetId },
                             function (result) {
                                 var desc = $.trim(result.description);
                                 desc = desc.length != 0 ? desc : "No description";
                                 desc = Ext.util.Format.htmlEncode(desc);
                                 body.empty().html('<div style="font-weight: bold;">Description:</div>' + desc);
                             });

        return SW.NCM.ShowDescription(obj);

    }


    gridColumnChangedHandler = function (component, state) {
        if (component == null)
            return;

        var gridCM = component.getColumnModel();
        var settingName = 'Columns';

        if (gridCM != null && settingName != '') {

            var jsonvalue = SW.NCM.GridColumnsToJson(gridCM);
            ORION.Prefs.save(settingName, jsonvalue);
        }
    }
    ORION.prefix = "NCM_ConfigSnippets_Thwack_";

    var selectorModel;
    var dataStore;
    var grid;

    return {
        init: function () {

            if ($("[id$='_ContentContainer']").length == 0)
                return false;

            function onImportClick() {
                if (SW.NCM.IsDemoMode()) return SW.NCM.ShowDemoModeExtMsg();
                SW.NCM.logInToThwack(importSelectedSnippets, grid.getSelectionModel().getSelections(), true);
            }

            $("#Grid").click(function (e) {

                var obj = $(e.target);
                if (obj.hasClass("ncm_searchterm"))
                    obj = obj.parent();

                if (obj.hasClass('tagLink')) {
                    jumpToTag(obj.text());
                    return false;
                }
                if (obj.hasClass('snippetName')) {
                    showDescription(obj);
                    return false;
                }
            });

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", updateToolbarButtons);

            dataStore = new ORION.WebServiceStore(
                                "/Orion/NCM/Services/Thwack.asmx/GetSnippetsPaged",
                                [
                                    { name: 'Id', mapping: 0 },
                                    { name: 'Title', mapping: 1 },
                                    { name: 'Link', mapping: 2 },
                                    { name: 'PubDate', mapping: 3 },
                                    { name: 'DownloadCount', mapping: 4 },
                                    { name: 'ViewCount', mapping: 5 },
                                    { name: 'Rating', mapping: 6 },
                                    { name: 'Owner', mapping: 7 },
                                    { name: 'Tags', mapping: 8 }
                                ],
                                "Title");

            var pagingToolbar = new Ext.PagingToolbar({
                store: dataStore,
                pageSize: 25,
                displayInfo: true,
                displayMsg: '@{R=NCM.Strings;K=WEBJS_VM_25;E=js}',
                emptyMsg: '@{R=NCM.Strings;K=WEBJS_VM_26;E=js}'
            });

            grid = new Ext.grid.GridPanel({
                id: 'gridControl',
                store: dataStore,

                columns: [selectorModel, {
                    header: 'ID',
                    width: 80,
                    hidden: true,
                    sortable: true,
                    hideable: false,
                    dataIndex: 'Id'
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VM_20;E=js}',
                    width: 300,
                    sortable: true,
                    hideable: false,
                    dataIndex: 'Title',
                    renderer: renderName
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VM_41;E=js}',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    dataIndex: 'Owner'
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VM_42;E=js}',
                    width: 90,
                    sortable: true,
                    dataIndex: 'PubDate',
                    renderer: renderPublishDate
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VM_43;E=js}',
                    width: 90,
                    sortable: true,
                    dataIndex: 'Rating',
                    renderer: renderRating
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VM_44;E=js}',
                    width: 90,
                    sortable: true,
                    dataIndex: 'DownloadCount'
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VM_45;E=js}',
                    width: 100,
                    sortable: true,
                    hidden: true,
                    dataIndex: 'ViewCount'
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VM_21;E=js}',
                    width: 220,
                    sortable: false,
                    dataIndex: 'Tags',
                    renderer: renderTags
                }],

                sm: selectorModel,

                viewConfig: {
                    forceFit: false,
                    deferEmptyText: true,
                    emptyText: " "
                },

                height: 500,
                loadMask: true,
                layout: 'fit',
                autoScroll: 'true',
                stripeRows: true,

                tbar: [{
                    id: 'ImportButton',
                    text: '@{R=NCM.Strings;K=WEBJS_VM_12;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VM_46;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_import.gif',
                    cls: 'x-btn-text-icon',
                    handler: onImportClick
                }],

                bbar: pagingToolbar

            });
            SW.NCM.PersonalizeGrid(grid, columnModel);
            grid.on("statesave", gridColumnChangedHandler);

            grid.render('Grid');
            
            pagingToolbar.refresh.hideParent = true;
            pagingToolbar.refresh.hide();

            updateToolbarButtons();

            loadGroupByValues();

            $("form").submit(function () { return false; });

            gridResize();
            $(window).resize(function () {
                gridResize();
            });

            $("#search").keyup(function (e) {
                if (e.keyCode == 13) {
                    $("#doSearch").click();
                }
            });

            $("#doSearch").click(function () {
                var search = $("#search").val();
                if ($.trim(search).length == 0) return;

                $(".GroupItem").removeClass("SelectedGroupItem");

                refreshObjects("", "", search);
            });

            // highlight the search term (if we have one)            
            grid.getView().on("refresh", function () {
                var search = currentSearchTerm;
                if ($.trim(search).length == 0) return;

                var columnsToHighlight = SW.NCM.ColumnIndexByName(grid.getColumnModel(), ["Config Change Template Name", "Submitter", "Tags"]);
                Ext.each(columnsToHighlight, function (columnNumber) {
                    SW.NCM.highlightSearchText("#Grid .x-grid3-body .x-grid3-td-" + columnNumber, search);
                });
            });

            // Set the grid's empty text based on the data returned from the server.
            dataStore.on("load", function (store, records, options) {
                var t = dataStore.reader.jsonData.d.EmptyText;
                $("#Grid div.x-grid-empty").text(t);
            });
        }
    };

} ();

Ext.onReady(SW.NCM.ConfigSnippets.init, SW.NCM.ConfigSnippets);
