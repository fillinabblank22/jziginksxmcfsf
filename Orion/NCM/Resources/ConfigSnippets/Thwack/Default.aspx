<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_NCM_Resources_ConfigSnippets_Thwack_Default" Title="Untitled Page" %>

<%@ Register Src="~/Orion/NCM/NavigationTabBar.ascx" TagPrefix="ncm" TagName="NavigationTabBar" %>

<%@ Register Src="~/Orion/NCM/CustomPageLinkHidding.ascx" TagPrefix="ncm" TagName="custPageLinkHidding" %>
<%@ Register TagPrefix="ncm1" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmHeadPlaceHolder" Runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
	<ncm:custPageLinkHidding runat="server" />

    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script type="text/javascript">
        var thwackUserInfo = <%=_thwackUserInfo%>;
    </script>

    <script type="text/javascript" src="ViewThwackConfigSnippets.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippetsHelper.js"></script>

    <script type="text/javascript">
        SW.NCM.IsPowerUser = <%=SolarWinds.NCMModule.Web.Resources.SecurityHelper.IsPermissionExist(SolarWinds.NCMModule.Web.Resources.SecurityHelper._canExecuteScript).ToString().ToLower() %>;
    </script>
    <ncm1:ConfigSnippetUISettings ID="ConfigSnippetUISettings1" runat="server" Name="NCM_ConfigSnippets_Thwack_Columns" DefaultValue="[]" RenderTo="columnModel" />

    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippets.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />

    <style type="text/css">
		#snippetName
        {
	        font-weight: bold;
        }
	</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">

    <%if (IsDemoMode) {%>
		<div id="isDemoMode" style="display:none;"></div>
	<%}%>

    <%foreach (string setting in new string[] { @"GroupByValue"}) { %>
		    <input type="hidden" name="NCM_ConfigSnippets_Thwack_<%=setting%>" id="NCM_ConfigSnippets_Thwack_<%=setting%>" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get($@"NCM_ConfigSnippets_Thwack_{setting}")%>' />
    <%}%>

    <asp:Panel id="ContentContainer" runat="server">
        <div style="padding:10px;">
            <ncm:NavigationTabBar ID="NavigationTabBar" runat="server" Visible="true" />

            <div id="tabPanel" class="tab-top">

                <table class="ExistingElements" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="2">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="font-size: 11px; padding-bottom: 10px;">
                                        <%=string.Format(Resources.NCMWebContent.WEBDATA_VM_47, @"<a href='https://thwack.com/ncm-config-change-scripts' style='text-decoration:underline;color:#336699;' target='_blank'>", @"</a>")%>
                                    </td>
                                    <td align="right" style="padding-bottom:5px;">
                                        <label for="search"><%=Resources.NCMWebContent.WEBDATA_VM_13%></label>
                                        <input id="search" type="text" size="20" />
                                        <!-- need to think about localization button here-->
                                        <%= $@"<input id=""doSearch"" type=""button"" value=""{Resources.NCMWebContent.WEBDATA_VY_19}"" />" %>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr valign="top" align="left">
			            <td style="padding-right: 10px;width: 15%;">
		                    <div class="ElementGrouping">
			                    <div class="ncm_GroupSection x-panel-header x-toolbar x-small-editor">
				                    <div><%=Resources.NCMWebContent.WEBDATA_VM_10 %></div>
			                    </div>
			                    <ul class="GroupItems" style="width:100%;"></ul>
		                    </div>
	                    </td>
	                    <td id="gridCell">
                            <div id="Grid"/>
	                    </td>
                    </tr>
                </table>

                <div id="originalQuery"></div>
                <div id="test"></div>
                <pre id="stackTrace"></pre>
            </div>

            <div id="snippetDescriptionDialog" class="x-hidden">
                <div id="snippetDescriptionBody" class="x-panel-body" style="padding: 10px; height: 210px; overflow: auto;" />
            </div>
        </div>
    </asp:Panel>
    <div style="padding:10px;" id="ErrorContainer" runat="server"/>
</asp:Content>

