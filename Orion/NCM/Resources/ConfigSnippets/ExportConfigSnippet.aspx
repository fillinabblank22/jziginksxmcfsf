<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMEditMasterPage.master" AutoEventWireup="true" CodeFile="ExportConfigSnippet.aspx.cs" Inherits="Orion_NCM_Resources_ConfigSnippets_ExportConfigSnippet" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" Runat="Server">
    <div style="padding:10px;font-size:11px;font-family:Arial;">
        <a title="<%=Resources.NCMWebContent.WEBDATA_VM_36 %>" style="text-decoration:underline;" href="ConfigSnippets.aspx"><%=Resources.NCMWebContent.WEBDATA_VM_36 %></a>&nbsp;<img alt="" style="vertical-align:middle;" src="/Orion/images/breadcrumb_arrow.gif"/>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
    <link rel="Stylesheet" type="text/css" href="../../styles/NCMResources.css" />
    <script type="text/javascript" src="../../JavaScript/main.js"></script>
    
    <script language="javascript" type="text/javascript">
        function $id(id)
        {
	        return document.getElementById(id);
        }

        function ToggleVisibility(id, displayStyle)
        {
	        var item = $id(id);
            if (item.style.display == "none")
                item.style.display = displayStyle;
            else
                item.style.display = "none";
        }

        function Hide(id)
        {
            var item;
            if (id.style)
	            item = id;
            else
    	        item = $id(id);
            item.style.display = "none";
        }
    </script>
    
    <div style="padding:10px;">
        <div class="ncm_ThwackExport">
            <div>
                <div id="ncm_ShareTitle"><%=Resources.NCMWebContent.WEBDATA_VM_39 %></div>
                <%= string.Format(Resources.NCMWebContent.WEBDATA_VM_40, string.Format(Resources.NCMWebContent.WEBDATA_VM_41, ThwackUrl))%>
                <br />
                <br />
                
                <a href="" onclick="ToggleVisibility('DetailedInstructions',''); Hide(this); return false;" style="text-decoration:underline;"><%= Resources.NCMWebContent.WEBDATA_VM_42 %></a>
                <div id="DetailedInstructions" style="display:none;padding-left:0px;padding-bottom:0px;">
                    <%=string.Format(Resources.NCMWebContent.WEBDATA_VM_43, ThwackUrl)%>
                </div>
                
                <br />
                <br />
                <orion:LocalizableButtonLink ID="lbl1" runat="server"  DisplayType="Primary" LocalizedText="CustomText" Text="<%$ Resources:NCMWebContent, WEBDATA_VM_45%>"  />
                <orion:LocalizableButtonLink ID="lbl2" runat="server"  NavigateUrl="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippets.aspx" DisplayType="Secondary" LocalizedText="CustomText" Text="<%$ Resources:NCMWebContent, WEBDATA_VM_46%>" />
            </div> 
        </div>
        <iframe runat="server" id="DownloadIFrame" width="100%" height="100%" scrolling="no" frameborder="0" src=""></iframe>
    </div>
</asp:Content>

