using System;
using System.IO;
using System.Web.UI;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.NCM.Web.Contracts.SiteMap;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_Resources_ConfigSnippets_ImportConfigSnippet : Page
{
    private readonly IIsWrapper isLayer;
    private readonly ILogger logger;
    private readonly Func<INcmSiteMapRenderer> rendererFactory;

    public Orion_NCM_Resources_ConfigSnippets_ImportConfigSnippet()
    {
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
        logger = ServiceLocator.Container.Resolve<Func<Type, ILogger>>()(GetType());
        rendererFactory = ServiceLocator.Container.Resolve<Func<INcmSiteMapRenderer>>();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_VM_37;
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (uploadConfigSnippet.HasFile)
        {
            var reader = new StreamReader(uploadConfigSnippet.FileContent);
            var fileName = uploadConfigSnippet.FileName;

            var fileContent = reader.ReadToEnd();
            try
            {
                var toImportSnippet = ImportExportConfigSnippets.ConfigSnippetImportFromString(fileContent);
                toImportSnippet.Name = Path.GetFileNameWithoutExtension(fileName);

                using (var proxy = isLayer.GetProxy())
                {
                    var snippetId  = proxy.Cirrus.Snippets.AddSnippet(toImportSnippet);
                    Response.Redirect($"EditConfigSnippet.aspx?SnippetID={snippetId}");
                }
            }
            catch
            {
                validatorConfigSnippetFile.IsValid = false;
            }
        }
        else
        {
            validatorConfigSnippetFile.IsValid = false;
        }
    }

    protected void CancelClick(object sender, EventArgs e)
    {
        Response.Redirect("ConfigSnippets.aspx");
    }

    protected void SiteMapPath_OnInit(object sender, EventArgs e)
    {
        try
        {
            var renderer = rendererFactory();
            renderer.SetUpData(null);
            NodeSiteMapPath.SetUpRenderer(renderer);
        }
        catch (Exception ex)
        {
            logger.Error("Error initializing the site map.", ex);
            throw;
        }
    }
}
