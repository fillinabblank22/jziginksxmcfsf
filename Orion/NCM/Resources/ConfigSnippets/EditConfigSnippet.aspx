<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMEditMasterPage.master" AutoEventWireup="true" CodeFile="EditConfigSnippet.aspx.cs" Inherits="Orion_NCM_Resources_ConfigSnippets_EditConfigSnippet" Title="Untitled Page" %>

<%@ Register TagPrefix="ncm" TagName="ConfigSnippetSettings" Src="~/Orion/NCM/Resources/ConfigSnippets/Controls/ConfigSnippetSettings.ascx" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/NCM/Controls/ErrorControl.ascx" TagPrefix="ncm" TagName="ErrorControl" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ncmPageTitlePlaceHolder">
	<%=Page.Title %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmHelpIconPlaceHolder" runat="server">
    <orion:IconHelpButton ID="helpBtn" runat="server"/>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" runat="server">
    <ncm:DropDownMapPath ID="NodeSiteMapPath" Provider="NCMSitemapProvider" runat="server"  OnInit="SiteMapPath_OnInit">
        <RootNodeTemplate>
            <a href="<%# Eval(@"url") %>" ><u> <%# Eval(@"title") %></u> </a>
        </RootNodeTemplate>
        <CurrentNodeTemplate>
	        <a href="<%# Eval(@"url") %>" ><%# Eval(@"title") %> </a>
        </CurrentNodeTemplate>
    </ncm:DropDownMapPath>
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
    <link rel="Stylesheet" type="text/css" href="../../styles/NCMResources.css" />
    <link rel="Stylesheet" type="text/css" href="jquery-linedtextarea.css" />
	<script type="text/javascript" src="../../JavaScript/main.js"></script>
	<script type="text/javascript" src="jquery-linedtextarea.js"></script>
    
    <div style="padding:10px;">
        <asp:UpdatePanel ID="uPanel" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <div style="border:solid 1px #dcdbd7;width:1100px;padding:5px;background-color:#fff;">
                    <table id="SnippetSettingsHolder" runat="server">
                        <tr>
                            <td>
                                <ncm:ConfigSnippetSettings ID="snippetSettings" runat="server" />
                            </td>
                        </tr>
                    </table>
                    
                    <div style="width:800px;">
                        <ncm:ErrorControl ID="ErrorControl" runat="server" Visible="false" />
                    </div>
                                        
                    <table id="OkHolder" visible="false" runat="server" width="800px">
                        <tr>
                            <td class="Property" style="padding:10px;">
                                <table style="background-color:#daf7cc;vertical-align:middle;">
                                    <tr>
                                        <td>
                                            <asp:Image ID="OkImage" ImageUrl="~/Orion/NCM/Resources/images/icon_ok.gif" runat="server" />
                                        </td>
                                        <td>
                                            <asp:Label ID="OkText" runat="server" ForeColor="#00a000" Font-Bold="true"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    
                    <table width="100%">
	                    <tr>
	                        <td style="padding:5px;width:20%;">
	                            <br/>
                                <orion:LocalizableButton runat="server" ID="Submit" LocalizedText="submit" DisplayType="Primary" OnClick="SubmitClick"  />
                                <orion:LocalizableButton runat="server" ID="Cancel" LocalizedText="Cancel" DisplayType="secondary" OnClick="CancelClick" CausesValidation="false"  />
	                        </td>
	                        <td style="padding:5px; width:80%">
	                            <br/>
                                <orion:LocalizableButton runat="server" ID="ValidateBtn" LocalizedText="customtext" Text="<%$ Resources:NCMWebContent, WEBDATA_VM_20%>" DisplayType="secondary" OnClick="ValidateClick"  />
                                <orion:LocalizableButton runat="server" ID="SaveAsCopy" LocalizedText="customtext" Text="<%$ Resources:NCMWebContent, WEBDATA_VM_21%>" DisplayType="secondary" OnClick="SaveAsCopyClick" />
                                <orion:LocalizableButton runat="server" ID="Execute" LocalizedText="customtext" Text="<%$ Resources:NCMWebContent, WEBDATA_VM_22%>" DisplayType="secondary" OnClick="ExecuteClick"  />
	                        </td>
	                    </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

