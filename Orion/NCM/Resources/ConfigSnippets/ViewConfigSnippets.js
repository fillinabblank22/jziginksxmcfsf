﻿Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.QuickTips.init();

SW.NCM.Snippets = function() {

    var getGroupByValues = function(groupBy, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/ConfigSnippets.asmx",
            "GetValuesAndCountForProperty",
            { property: groupBy },
            function(result) {
                onSuccess(ORION.objectFromDataTable(result));
            });
    };

    var deleteSnippets = function(ids, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/ConfigSnippets.asmx",
            "DeleteSnippets",
            { snippetIds: ids },
            function(result) {
                onSuccess(result);
            });
    };

    var createNewSnippet = function() {

        window.location = "/Orion/NCM/Resources/ConfigSnippets/EditConfigSnippet.aspx";
    };

    var copySnippet = function(ids, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/ConfigSnippets.asmx",
            "CopySnippets",
            { snippetIds: ids },
            function(result) {
                onSuccess(result);
            });
    };

    var uploadSnippets = function(ids, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/Thwack.asmx",
            "UploadSnippets",
            { Ids: ids },
            function(result) {
                onSuccess(result);
            });
    };

    var uploadSelectedSnippets = function(items) {
        var selected = getSelectedSnippetIds(items);
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VM_27;E=js}');

        uploadSnippets(selected,
            function(result) {
                waitMsg.hide();

                var obj = Ext.decode(result);
                if (!obj.isSuccess) {
                    obj.msg = String.format("<span style='color:#f00;'>{0}</span>", obj.msg);
                    SW.NCM.validateThwackCred(false);
                } else {
                    SW.NCM.validateThwackCred(true);
                }

                function onClose() {
                    dlg.hide();
                    dlg.destroy();
                    dlg = null;
                }

                var dlg = new Ext.Window({
                    title: '@{R=NCM.Strings;K=WEBJS_VM_28;E=js}',
                    html: obj.msg,
                    width: 350,
                    autoHeight: true,
                    border: false,
                    region: "center",
                    layout: "fit",
                    modal: true,
                    resizable: false,
                    plain: true,
                    bodyStyle: "padding:5px 0px 0px 10px;",
                    buttons: [{ text: "@{R=NCM.Strings;K=ConfigChangeTemplates_ButtonClose;E=js}", handler: onClose }]
                });
                dlg.show();
            });
    };

    var loadGroupByValues = function() {
        var groupItems = $(".GroupItems").text('@{R=NCM.Strings;K=WEBJS_VM_29;E=js}');

        getGroupByValues("tags",
            function(result) {
                groupItems.empty();
                $(result.Rows).each(function() {
                    var value = Ext.util.Format.htmlEncode(this.theValue);
                    var text = $.trim(String(this.theValue));
                    if (text.length > 22) {
                        text = String.format("{0}...", text.substr(0, 22));
                    }
                    var allLabel = "@{R=NCM.Strings;K=ConfigChangeTemplates_All;E=js}";
                    var disp = (text || allLabel) + " (" + this.theCount + ")";
                    if (value === null) {
                        disp = allLabel + " (" + this.theCount + ")";
                    }
                    $("<a href='#'></a>")
                        .attr('value', value).attr('title', value)
                        .text(disp).click(selectGroup).appendTo(".GroupItems").wrap("<li class='GroupItem'/>");
                });

                var toSelect = $(".GroupItem a[value='" +
                    Ext.util.Format.htmlEncode(ORION.Prefs.load("GroupByValue")) +
                    "']");
                if (toSelect.length === 0) {
                    toSelect = $(".GroupItem a:first");
                }

                toSelect.click();
            });
    };

    var selectGroup = function() {
        $(this).parent().addClass("SelectedGroupItem").siblings().removeClass("SelectedGroupItem");

        var value = $(".SelectedGroupItem a").attr('value');
        value = Ext.util.Format.htmlDecode(value);
        ORION.Prefs.save('GroupByValue', value);

        refreshObjects("tags", value, "");
        return false;
    };

    var currentSearchTerm = null;
    var refreshObjects = function(property, value, search, callback) {
        grid.store.removeAll();
        var pagingToolbar = grid.getBottomToolbar();
        pagingToolbar.cursor = gridCursor;
        gridCursor = 0;
        grid.store.proxy.conn.jsonData = { property: property, value: value || "", search: search };
        currentSearchTerm = search;
        pagingToolbar.doLoad(pagingToolbar.cursor);
    };

    var getSelectedSnippetIds = function(items) {
        var ids = [];

        Ext.each(items,
            function(item) {
                ids.push(item.data.SnippetID);
            });

        return ids;
    };

    var gridCursor = 0;
    var deleteSelectedItems = function(items) {
        var toDelete = getSelectedSnippetIds(items);
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VM_30;E=js}');

        deleteSnippets(toDelete,
            function(result) {
                waitMsg.hide();
                ErrorHandler(result);
                var currentPageRecordCount = grid.store.getCount();
                var selectedRecordCount = grid.getSelectionModel().getCount();
                if (currentPageRecordCount > selectedRecordCount) {
                    var pagingToolbar = grid.getBottomToolbar();
                    gridCursor = pagingToolbar.cursor;
                } else {
                    gridCursor = 0;
                }
                loadGroupByValues();
            });
    };

    var editSnippet = function(item) {
        var url = "/Orion/NCM/Resources/ConfigSnippets/EditConfigSnippet.aspx?SnippetId=" + item.data.SnippetID;
        window.location = url;
    };


    var copySelectedSnippets = function(items) {

        var selectedIds = getSelectedSnippetIds(items);

        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VM_31;E=js}');

        copySnippet(selectedIds,
            function(result) {
                waitMsg.hide();
                ErrorHandler(result);
                loadGroupByValues();
                grid.store.reload();
            });

    };

    var exportSelectedSnippets = function(items) {
        var selectedIds = getSelectedSnippetIds(items);
        ORION.postToTarget("/Orion/NCM/Resources/ConfigSnippets/ExportConfigSnippet.aspx", { postData: selectedIds });
    };


    var editTags = function(items) {
        var selectedIds = getSelectedSnippetIds(items);
        ORION.postToTarget("/Orion/NCM/Resources/ConfigSnippets/EditTags.aspx", { postData: selectedIds });
    };

    var showScriptErrorDialog;

    function showScriptError(msg, obj) {
        var body = $("#snippetScriptErrorBody").text('@{R=NCM.Strings;K=WEBJS_VM_29;E=js}');
        var desc = $.trim(msg);
        desc = desc.length !== 0 ? desc : "@{R=NCM.Strings;K=ConfigChangeTemplates_UnexpectedError;E=js}";
        body.empty()
            .html(
                '<div style="font-weight: bold;">@{R=NCM.Strings;K=ConfigChangeTemplates_ExecutionErrorsHeader;E=js}</div>' +
                desc);

        if (!showScriptErrorDialog) {
            showScriptErrorDialog = new Ext.Window({
                applyTo: 'snippetScriptErrorDialog',
                layout: 'fit',
                width: 580,
                height: 300,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                modal: true,
                contentEl: 'snippetScriptErrorBody',
                buttons: [
                    {
                        text: '@{R=NCM.Strings;K=WEBJS_VM_33;E=js}',
                        handler: function() {
                            window.location =
                                "/Orion/NCM/Resources/ConfigSnippets/EditConfigSnippet.aspx?validate=true&SnippetId=" +
                                obj.data.SnippetID;
                        }
                    },
                    {
                        text: '@{R=NCM.Strings;K=WEBJS_VM_34;E=js}',
                        handler: function() { showScriptErrorDialog.hide(); }
                    }
                ]
            });
        }

        // Set the window caption
        showScriptErrorDialog.setTitle(
            '@{R=NCM.Strings;K=WEBJS_VM_32;E=js}' + Ext.util.Format.htmlEncode(obj.data.Name));

        // Set the location
        showScriptErrorDialog.alignTo(document.body, "c-c");
        showScriptErrorDialog.show();

        return false;
    }

    var executeSnippet = function(item) {
        var snippetId = item.data.SnippetID;

        ORION.callWebService("/Orion/NCM/Services/ConfigSnippets.asmx",
            "ValidateConfigSnippetScript",
            { snippetId: snippetId },
            function(result) {
                var jsonValue = Ext.util.JSON.decode(result);
                if (!jsonValue.success)
                    return showScriptError(jsonValue.message, item);
                else
                    window.location = "/Orion/NCM/Resources/ConfigSnippets/ExecuteConfigSnippet.aspx?SnippetId=" +
                        snippetId;
            });
    };

    var updateToolbarButtons = function() {
        var selCount = grid.getSelectionModel().getCount();
        var map = grid.getTopToolbar().items.map;

        var needsOnlyOneSelected = selCount !== 1;
        var needsAtLeastOneSelected = selCount === 0;

        map.EditButton.setDisabled(SW.NCM.IsPowerUser ? needsOnlyOneSelected : true);
        map.Execute.setDisabled(SW.NCM.IsPowerUser ? needsOnlyOneSelected : true);
        map.EditTagsButton.setDisabled(SW.NCM.IsPowerUser ? needsAtLeastOneSelected : true);
        map.CopyButton.setDisabled(SW.NCM.IsPowerUser ? needsAtLeastOneSelected : true);
        map.ExportButton.setDisabled(SW.NCM.IsPowerUser ? needsOnlyOneSelected : true);
        map.DeleteButton.setDisabled(SW.NCM.IsPowerUser ? needsAtLeastOneSelected : true);
        map.UploadButton.setDisabled(SW.NCM.IsPowerUser ? needsAtLeastOneSelected : true);
        map.CreateNewSnippet.setDisabled(!SW.NCM.IsPowerUser);
        map.ImportButton.setDisabled(!SW.NCM.IsPowerUser);

        // revice 'Select All' checkbox state
        var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');
        if (grid.getStore().getCount() > 0 && selCount == grid.getStore().getCount()) {
            hd.addClass('x-grid3-hd-checker-on');
        } else {
            hd.removeClass('x-grid3-hd-checker-on');
        }
    };

    //Error handler
    function ErrorHandler(result) {
        if (result !== null && result.Error) {
            Ext.Msg.show({
                title: result.Source,
                msg: result.Msg,
                minWidth: 500,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        }
    }

    // Column rendering functions
    function renderName(value, meta, record) {
        return String.format(
            '<img src="/Orion/NCM/Resources/images/ConfigSnippets/icon_config_snippet.gif" />&nbsp;<a href="#" class="snippetName" value="{0}">{1}</a>',
            record.data.SnippetID,
            Ext.util.Format.htmlEncode(value));
    }


    function renderDateTime(value, meta, record) {
        var date = eval(value.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
        return Ext.util.Format.date(date, ExtDaTimeLocalePattern.DateFull);
    }

    function renderTags(value, meta, record) {
        var tags;

        try {
            tags = Ext.decode(value);
        } catch (e) {
        }

        tags = tags || [];

        if (tags.length === 0) {
            return '@{R=NCM.Strings;K=WEBJS_VM_39;E=js}';
        }

        var links = [];
        for (var i = 0; i < tags.length; ++i) {
            links.push(String.format('<a href="#" class="tagLink">{0}</a>', Ext.util.Format.htmlEncode(tags[i])));
        }

        return links.join(', ');
    }

    function renderDescription(value, meta, record) {
        return Ext.util.Format.htmlEncode(value);
    }

    function jumpToTag(tagName) {
        var tn = Ext.util.Format.htmlEncode(tagName);
        $(".GroupItem a[value='" + tn + "']").click();
    }

    gridColumnChangedHandler = function(component, state) {
        if (component == null)
            return;

        var gridCM = component.getColumnModel();
        var settingName = 'Columns';

        if (gridCM != null && settingName != '') {

            var jsonvalue = SW.NCM.GridColumnsToJson(gridCM);
            ORION.Prefs.save(settingName, jsonvalue);
        }
    };

    function gridResize() {
        if (grid != null) {
            grid.setWidth(1);
            grid.setWidth($('#gridCell').width());

            var h = getGridHeight();
            grid.setHeight(getGridHeight());

            var groupItems = $(".GroupItems");
            groupItems.height(h - $(".ncm_GroupSection").height() - $("#ncm_ThwackAd").height() - 51);
        }
    }

    function getGridHeight() {
        var top = $('#gridCell').offset().top;
        return $(window).height() - top - $('#footer').height() - 100;
    }

    function showDescription(obj) {
        var snippetId = obj.attr("value");
        var body = $("#snippetDescriptionBody").text('@{R=NCM.Strings;K=LIBCODE_VK_05;E=js}');

        ORION.callWebService("/Orion/NCM/Services/ConfigSnippets.asmx",
            "GetSnippetDescription",
            { snippetId: snippetId },
            function(result) {
                var desc = $.trim(result.description);
                desc = desc.length !== 0 ? desc : '@{R=NCM.Strings;K=WEBJS_VY_72;E=js}';
                desc = Ext.util.Format.htmlEncode(desc);
                body.empty().html('<div style="font-weight: bold;">@{R=NCM.Strings;K=WEBJS_VY_90;E=js}</div>' + desc);
            });

        return SW.NCM.ShowDescription(obj);
    }


    ORION.prefix = "NCM_ConfigSnippets_";

    var selectorModel;
    var dataStore;
    var grid;


    return {
        init: function() {

            if ($("[id$='_ContentContainer']").length === 0)
                return false;

            $("#Grid").click(function(e) {

                var obj = $(e.target);

                if (obj.hasClass("ncm_searchterm"))
                    obj = obj.parent();

                if (obj.hasClass('tagLink')) {
                    jumpToTag(obj.text());
                    return false;
                }
                if (obj.hasClass('snippetName')) {
                    showDescription(obj);
                    return false;
                }
            });

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", updateToolbarButtons);

            dataStore = new ORION.WebServiceStore(
                "/Orion/NCM/Services/ConfigSnippets.asmx/GetSnippetsPaged",
                [
                    { name: 'SnippetID', mapping: 0 },
                    { name: 'Name', mapping: 1 },
                    { name: 'Description', mapping: 2 },
                    { name: 'Created', mapping: 3 },
                    { name: 'LastModified', mapping: 4 },
                    { name: 'Tags', mapping: 5 }
                ],
                "Name");

            var pagingToolbar = new Ext.PagingToolbar({
                store: dataStore,
                pageSize: 25,
                displayInfo: true,
                displayMsg: '@{R=NCM.Strings;K=WEBJS_VM_25;E=js}',
                emptyMsg: '@{R=NCM.Strings;K=WEBJS_VM_26;E=js}'
            });


            grid = new Ext.grid.GridPanel({
                id: 'gridControl',
                store: dataStore,

                columns: [
                    selectorModel, {
                        header: 'ID',
                        width: 80,
                        hidden: true,
                        hideable: false,
                        sortable: true,
                        dataIndex: 'SnippetID'
                    }, {
                        header: '@{R=NCM.Strings;K=WEBJS_VM_20;E=js}',
                        width: 330,
                        sortable: true,
                        dataIndex: 'Name',
                        renderer: renderName
                    }, {
                        header: '@{R=NCM.Strings;K=WEBJS_VM_21;E=js}',
                        width: 400,
                        sortable: false,
                        dataIndex: 'Tags',
                        renderer: renderTags
                    }, {
                        header: '@{R=NCM.Strings;K=WEBJS_VM_22;E=js}',
                        width: 200,
                        sortable: false,
                        hidden: true,
                        dataIndex: 'Description',
                        renderer: renderDescription
                    }, {
                        header: '@{R=NCM.Strings;K=WEBJS_VM_23;E=js}',
                        width: 150,
                        sortable: true,
                        dataIndex: 'Created',
                        hidden: true,
                        renderer: renderDateTime
                    }, {
                        header: '@{R=NCM.Strings;K=WEBJS_VM_24;E=js}',
                        width: 150,
                        sortable: true,
                        dataIndex: 'LastModified',
                        hidden: true,
                        renderer: renderDateTime
                    }
                ],

                sm: selectorModel,

                layout: 'fit',
                autoScroll: 'true',
                loadMask: true,

                height: 500,
                stripeRows: true,

                tbar: [
                    {
                        id: 'CreateNewSnippet',
                        text: '@{R=NCM.Strings;K=WEBJS_VM_02;E=js}',
                        tooltip: '@{R=NCM.Strings;K=WEBJS_VM_03;E=js}',
                        icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_addsnippet.gif',
                        cls: 'x-btn-text-icon',
                        handler: function() {
                            //if (SW.NCM.IsDemoMode()) return SW.NCM.ShowDemoModeExtMsg();
                            createNewSnippet();
                        }
                    }, '-', {
                        id: 'Execute',
                        text: '@{R=NCM.Strings;K=WEBJS_VM_04;E=js}',
                        tooltip: '@{R=NCM.Strings;K=WEBJS_VM_05;E=js}',
                        icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_execute.gif',
                        cls: 'x-btn-text-icon',
                        handler: function() {
                            //if (SW.NCM.IsDemoMode()) return SW.NCM.ShowDemoModeExtMsg();
                            executeSnippet(grid.getSelectionModel().getSelected());
                        }
                    }, '-', {
                        id: 'EditButton',
                        text: '@{R=NCM.Strings;K=WEBJS_VM_06;E=js}',
                        tooltip: '@{R=NCM.Strings;K=WEBJS_VM_07;E=js}',
                        icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_editsnippet.gif',
                        cls: 'x-btn-text-icon',
                        handler: function() {
                            //	if (SW.NCM.IsDemoMode()) return SW.NCM.ShowDemoModeExtMsg();
                            editSnippet(grid.getSelectionModel().getSelected());
                        }
                    }, '-', {
                        id: 'EditTagsButton',
                        text: '@{R=NCM.Strings;K=WEBJS_VM_08;E=js}',
                        tooltip: '@{R=NCM.Strings;K=WEBJS_VM_09;E=js}',
                        icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_tag.gif',
                        cls: 'x-btn-text-icon',
                        handler: function() {
                            //if (SW.NCM.IsDemoMode()) return SW.NCM.ShowDemoModeExtMsg();
                            editTags(grid.getSelectionModel().getSelections());
                        }
                    }, '-', {
                        id: 'CopyButton',
                        text: '@{R=NCM.Strings;K=WEBJS_VM_10;E=js}',
                        tooltip: '@{R=NCM.Strings;K=WEBJS_VM_11;E=js}',
                        icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_clone.gif',
                        cls: 'x-btn-text-icon',
                        handler: function() {
                            if (SW.NCM.IsDemoMode()) return SW.NCM.ShowDemoModeExtMsg();
                            copySelectedSnippets(grid.getSelectionModel().getSelections());
                        }
                    }, '-', {
                        id: 'ImportButton',
                        text: '@{R=NCM.Strings;K=WEBJS_VM_12;E=js}',
                        tooltip: '@{R=NCM.Strings;K=WEBJS_VM_13;E=js}',
                        icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_import.gif',
                        cls: 'x-btn-text-icon',
                        handler: function() {
                            if (SW.NCM.IsDemoMode()) return SW.NCM.ShowDemoModeExtMsg();
                            window.location = "/Orion/NCM/Resources/ConfigSnippets/ImportConfigSnippet.aspx";
                        }
                    }, '-', {
                        id: 'UploadButton',
                        text: '@{R=NCM.Strings;K=WEBJS_VM_14;E=js}',
                        tooltip: '@{R=NCM.Strings;K=WEBJS_VM_15;E=js}',
                        icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_thwack.gif',
                        cls: 'x-btn-text-icon',
                        handler: function() {
                            if (SW.NCM.IsDemoMode()) return SW.NCM.ShowDemoModeExtMsg();
                            SW.NCM.logInToThwack(uploadSelectedSnippets,
                                grid.getSelectionModel().getSelections(),
                                false);
                        }
                    }, '-', {
                        id: 'ExportButton',
                        text: '@{R=NCM.Strings;K=WEBJS_VM_16;E=js}',
                        tooltip: '@{R=NCM.Strings;K=WEBJS_VM_17;E=js}',
                        icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_export.gif',
                        cls: 'x-btn-text-icon',
                        handler: function() {
                            if (SW.NCM.IsDemoMode()) return SW.NCM.ShowDemoModeExtMsg();
                            var selmodel = grid.getSelectionModel();
                            exportSelectedSnippets(selmodel.getSelections());
                        }
                    }, '-', {
                        id: 'DeleteButton',
                        text: '@{R=NCM.Strings;K=WEBJS_VM_18;E=js}',
                        tooltip: '@{R=NCM.Strings;K=WEBJS_VM_19;E=js}',
                        icon: '/Orion/NCM/Resources/images/icon_delete.png',
                        cls: 'x-btn-text-icon',
                        handler: function() {
                            if (SW.NCM.IsDemoMode()) return SW.NCM.ShowDemoModeExtMsg();
                            Ext.Msg.confirm('@{R=NCM.Strings;K=ConfigSnippets_DeleteDlg_Title;E=js}',
                                '@{R=NCM.Strings;K=ConfigSnippets_DeleteDlg_Msg;E=js}',
                                function(btn, text) {
                                    if (btn == 'yes') {
                                        deleteSelectedItems(grid.getSelectionModel().getSelections());
                                    }
                                });
                        }
                    }
                ],

                bbar: pagingToolbar

            });

            SW.NCM.PersonalizeGrid(grid, columnModel);
            grid.on("statesave", gridColumnChangedHandler);
            grid.render('Grid');

            updateToolbarButtons();

            loadGroupByValues();

            $("form").submit(function() { return false; });

            gridResize();
            $(window).resize(function() {
                gridResize();
            });

            $("#search").keyup(function(e) {
                if (e.keyCode == 13) {
                    $("#doSearch").click();
                }
            });

            $("#doSearch").click(function() {
                var search = $("#search").val();
                if ($.trim(search).length == 0) return;

                $(".GroupItem").removeClass("SelectedGroupItem");

                refreshObjects("", "", search);
            });

            // highlight the search term (if we have one)            
            grid.getView().on("refresh",
                function() {
                    var search = currentSearchTerm;
                    if ($.trim(search).length == 0) return;

                    var columnsToHighlight = SW.NCM.ColumnIndexByName(grid.getColumnModel(),
                        ['@{R=NCM.Strings;K=WEBJS_VM_20;E=js}', '@{R=NCM.Strings;K=WEBJS_VM_21;E=js}']);
                    Ext.each(columnsToHighlight,
                        function(columnNumber) {
                            SW.NCM.highlightSearchText("#Grid .x-grid3-body .x-grid3-td-" + columnNumber, search);
                        });
                });
        }
    };

}();

Ext.onReady(SW.NCM.Snippets.init, SW.NCM.Snippets);
