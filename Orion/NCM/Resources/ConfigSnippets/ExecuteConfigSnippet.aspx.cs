using System;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.NCM.Web.Contracts.SiteMap;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.ExtensibilityFramework;

public partial class Orion_NCM_Resources_ConfigSnippets_ExecuteConfigSnippet : OrionView
{
    private const string NCM_VIEW_TYPE = "NCMExecuteConfigSnippet";
    
    private readonly ExDataPicker exNodes = new ExDataPicker();
    private readonly ExDataValidator exDataValidator = new ExDataValidator();
    private readonly ExecuteConfigSnippet executeConfigSnippet = new ExecuteConfigSnippet();
    private TransferCallBackDelegate callBack;
    private readonly ProgressTemplate progressTemplate = new ProgressTemplate();

    private readonly ILogger logger;
    private readonly Func<INcmSiteMapRenderer> rendererFactory;

    public string contentMsgBox;
    public string titleMsgBox = Resources.NCMWebContent.WEBDATA_VM_52;

    public Orion_NCM_Resources_ConfigSnippets_ExecuteConfigSnippet()
    {
        logger = ServiceLocator.Container.Resolve<Func<Type, ILogger>>()(GetType());
        rendererFactory = ServiceLocator.Container.Resolve<Func<INcmSiteMapRenderer>>();
    }

    public override string ViewType => NCM_VIEW_TYPE;

    protected string PageTitle { get; set; }

    #region Page Methods

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        callBack = TransferCallBack;
        
        ExecuteConfigSnippet.Controls.Add(executeConfigSnippet);
        
        executeConfigSnippet.LoadControl();
        executeConfigSnippet.DataPickerClick += executeConfigSnippet_DataPickerClick;
        executeConfigSnippet.CallBackPtr = callBack;
        executeConfigSnippet.ProgressShow += executeConfigSnippet_ProgressShow;
          
        ProgressTimer.Tick += ProgressTimer_Tick;
        ProgressTimer.Interval=5000;

        ResourceSettings resourceSettings = new ResourceSettings {Prefix = "ConfSnippetPicker"};
        NPMDefaultSettingsHelper.SetUserSettings("ConfSnippetPickerGroupBy", "Vendor");
        
        exNodes.ResourceSettings = resourceSettings;
        exNodes.Visible = false;
        exNodes.ConfigMgmt = false;

        DataPickerContainer.Controls.Add(exNodes);
        DataPickerContainer.Controls.Add(exDataValidator);

        System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), @"key_igtree", @"function igtree_mouseout(){} function igtree_mouseover(){}", true);
    }

    void ProgressTimer_Tick(object sender, EventArgs e)
    {
        lock (ExSync.SyncObject)
        {
            if (Page.Session[@"NCMExScriptProgress"] != null)
                ProgressLabel.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_493, Page.Session[@"NCMExScriptProgress"].ToString());

            if (Page.Session[@"NCMExScriptGenComplete"] != null)
            {
                if (Convert.ToBoolean(Page.Session[@"NCMExScriptGenComplete"]))
                {
                    ProgressTimer.Enabled = false;
                    ProgressModalBox.Hide();
                     
                }
            }
        }
    }

    void executeConfigSnippet_ProgressShow()
    {
        ProgressTimer.Interval = ExNodes.Instance.Nodes.Count > 100 ? 30000 : 5000;
        ProgressModalBox.Show();
        ProgressModalBox.Width = 450;
        ProgressLabel.Text = Resources.NCMWebContent.WEBDATA_VM_53;
        ProgressTimer.Enabled = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = string.Format(Resources.NCMWebContent.WEBDATA_VM_54, executeConfigSnippet.ConfigSnippet.Item1);
        PageTitle = string.Format(Resources.NCMWebContent.WEBDATA_VM_54, !string.IsNullOrEmpty(Request[@"SnippetID"]) ? $@"<a href='EditConfigSnippet.aspx?SnippetID={Page.Request.QueryString[@"SnippetID"]}'>{CommonHelper.EncodeXmlToString(executeConfigSnippet.ConfigSnippet.Item1)}</a>" : CommonHelper.EncodeXmlToString(executeConfigSnippet.ConfigSnippet.Item1));

        if (Page.Request.Browser.Browser.Equals(@"FIREFOX", StringComparison.InvariantCultureIgnoreCase))
            progressTemplate.IsFireFoxBrowser = true;
        Progress.ProgressTemplate = progressTemplate;
        Progress.DisplayAfter = 3000;

        exNodes.SetupConrol();
    }

    #endregion

    #region Events

    void executeConfigSnippet_DataPickerClick(string argname, string entityName)
    {
        var exHelper = ServiceLocator.Container.Resolve<IExHelper>();
        exDataPickerTitle.Text = exHelper.GetEntityDisplayName(entityName);

        exNodes.EntityType = entityName;
        exNodes.ArgumentName = argname;
        exNodes.Visible = true;
        exNodes.RemoveExDataPickerNodes();
        exNodes.ExDataPickerHeight = 350;
        exNodes.ExDataPickerWidth = 635;

        DataPickerModalBox.Width = 650;
        DataPickerModalBox.Show();
    }

    protected void DataPickerOkHandler(object sender, EventArgs e)
    {
        ExNodes oldNodes = (ExNodes) ExNodes.Instance.Clone();

        var exHelper = ServiceLocator.Container.Resolve<IExHelper>();
        string entityType = exHelper.DecodeEntityName(exNodes.EntityType);
        switch (entityType.ToUpperInvariant())
        {
            case "NCM.NODES":
                var selectedNodes = exNodes.GetSelectedNodes();
                foreach (ExNode node in ExNodes.Instance.Nodes.Values)
                {
                    node.Arguments[exNodes.ArgumentName].Values.Clear();
                    foreach (var entityValue in selectedNodes)
                    {
                        node.Arguments[exNodes.ArgumentName].Values.Add(entityValue.ToString());
                    }
                }
                break;
            default:
                var selectedEntities = exNodes.GetSelectedEntities();
                //clear values and repopulate
                foreach (ExNode node in ExNodes.Instance.Nodes.Values)
                {
                    node.Arguments[exNodes.ArgumentName].Values.Clear();
                }

                foreach (var entity in selectedEntities)
                { 
                    foreach (string entityValue in entity.Value)
                    {
                        ExNodes.Instance.Nodes[entity.Key].Arguments[exNodes.ArgumentName].Values.Add(entityValue);
                    }
                }
                break;
        }

        exDataValidator.ArgName = exNodes.ArgumentName;
        exDataValidator.ArgType = exNodes.EntityType;
        exDataValidator.ValidationMode = ValidationType.Entity;
        exDataValidator.UseForValidateAllVariables = false;
        exDataValidator.Text = Resources.NCMWebContent.WEBDATA_VM_55;

        if (exDataValidator.IsValid())
        {
            DataPickerModalBox.Hide();
        }
        else
        {
            //revert everything back
            ExNodes.Instance = oldNodes;
        }

        ContentUpdatePanel.Update();
    }

    protected void DataPickerCancelHandler(object sender, EventArgs e)
    {
        DataPickerModalBox.Hide();
        ContentUpdatePanel.Update();
    }

    protected void TransferHandler(object sender, EventArgs e)
    {
        MsgBox.Hide();

        if (MsgBox.Session[@"TransferType"] != null &&
            (TransferType)MsgBox.Session[@"TransferType"] == TransferType.ExecuteConfigSnippet)
        {
            executeConfigSnippet.ExecuteConfigSnippetAction();
            MsgBox.Session[@"TransferType"] = null;

            Response.Redirect("/Orion/NCM/TransferStatus.aspx");
        }

        if (MsgBox.Session[@"TransferType"] != null &&
           (TransferType)MsgBox.Session[@"TransferType"] == TransferType.SendRequestForApproval)
        {
            executeConfigSnippet.ExecuteConfigSnippetAction();
            MsgBox.Session[@"TransferType"] = null;
        }
    }

    protected void CancelHandler(object sender, EventArgs e)
    {
        MsgBox.Hide();
    }

    protected void GenCancelHandler(object sender, EventArgs e)
    {
        executeConfigSnippet.CancelScriptGeneration();
        ProgressTimer.Enabled = false;
        ProgressModalBox.Hide();
        ContentUpdatePanel.Update();
    }

    #endregion

    private void TransferCallBack(TransferType type)
    {
        contentMsgBox = string.Format(Resources.NCMWebContent.WEBDATA_VM_56, executeConfigSnippet.Count.ToString());
        MsgBox.Width = 500;
        MsgBox.Show();
        MsgBox.Session[@"TransferType"] = type;
    }

    protected void SiteMapPath_OnInit(object sender, EventArgs e)
    {
        try
        {
            var renderer = rendererFactory();
            renderer.SetUpData(null);
            NodeSiteMapPath.SetUpRenderer(renderer);
        }
        catch (Exception ex)
        {
            logger.Error("Error initializing the site map.", ex);
            throw;
        }
    }
}
