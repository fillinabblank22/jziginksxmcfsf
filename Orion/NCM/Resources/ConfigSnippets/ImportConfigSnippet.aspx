<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMEditMasterPage.master" AutoEventWireup="true" CodeFile="ImportConfigSnippet.aspx.cs" Inherits="Orion_NCM_Resources_ConfigSnippets_ImportConfigSnippet" Title="Untitled Page" %>

<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmHelpIconPlaceHolder" runat="server">
    &nbsp;
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" Runat="Server">
    <ncm:DropDownMapPath ID="NodeSiteMapPath" Provider="NCMSitemapProvider" runat="server"  OnInit="SiteMapPath_OnInit">
        <RootNodeTemplate>
            <a href="<%# Eval(@"url") %>" ><u> <%# Eval(@"title") %></u> </a>
        </RootNodeTemplate>
        <CurrentNodeTemplate>
	        <a href="<%# Eval(@"url") %>" ><%# Eval(@"title") %> </a>
        </CurrentNodeTemplate>
    </ncm:DropDownMapPath>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
    <link rel="Stylesheet" type="text/css" href="../../styles/NCMResources.css" />    
    
    <div style="padding:10px;">
        <br />
        <asp:FileUpload ID="uploadConfigSnippet" runat="server" Width="50em" size="100" />
        <br />
        <br />
        <asp:CustomValidator ID="validatorConfigSnippetFile" runat="server" Font-Size="8pt" ErrorMessage="<%$ Resources:NCMWebContent, WEBDATA_VM_38%>" />
        <div>
            <orion:LocalizableButton runat="server" ID="Submit" LocalizedText="submit" DisplayType="Primary" OnClick="SubmitClick"  />
            <orion:LocalizableButton runat="server" ID="Cancel" LocalizedText="Cancel" DisplayType="secondary" OnClick="CancelClick"  />
        </div>
    </div>
</asp:Content>

