using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.ServiceModel;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Script.Serialization;
using System.Web.UI;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.InformationService.Contract2;
using SolarWinds.NCM.Web.Contracts.SiteMap;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Common;

public partial class Orion_NCM_Resources_ConfigSnippets_EditTags : Page
{
    private readonly bool _isDemoMode = OrionConfiguration.IsDemoServer;

    private readonly IIsWrapper isLayer;
    private readonly ILogger logger;
    private readonly Func<INcmSiteMapRenderer> rendererFactory;

    public Orion_NCM_Resources_ConfigSnippets_EditTags()
    {
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
        logger = ServiceLocator.Container.Resolve<Func<Type, ILogger>>()(GetType());
        rendererFactory = ServiceLocator.Container.Resolve<Func<INcmSiteMapRenderer>>();
    }

    private int[] SnippetIDs
    {
        get
        {
            if (ViewState["SnippetIDs"] == null)
            {
                if (string.IsNullOrEmpty(Request[@"postData"]))
                {
                    return new int[0];
                }
                ViewState["SnippetIDs"] = new JavaScriptSerializer().Deserialize<int[]>(Server.UrlDecode(Request[@"postData"]));
            }
            return ViewState["SnippetIDs"] as int[];
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        Submit.Visible = !_isDemoMode;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        InitPageTitle();

        if (!Page.IsPostBack)
        {
            BindControls();
            OnRadioButtonTags_CheckedChanged(null, null);
        }
    }

    protected void OnRadioButtonTags_CheckedChanged(Object sender, EventArgs e)
    {
        contentExistingTags.Visible = radioButtonExistingTags.Checked;
        contentAddTags.Visible = radioButtonAddTags.Checked;
        contentRemoveTags.Visible = radioButtonRemoveTags.Checked;
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (_isDemoMode)
            return;
        try
        {
            using (var proxy = isLayer.GetProxy())
            {
                var removeTags = GetSelectedTags(repeaterRemoveTags);
                var addTags = GetSelectedTags(repeaterExistingTags);
                var newTags = GetNewTags();

                if (removeTags.Count > 0)
                    proxy.Cirrus.Snippets.DeleteTags(SnippetIDs, removeTags);
                if (addTags.Count > 0)
                    proxy.Cirrus.Snippets.AddTags(SnippetIDs, addTags);
                if (newTags.Count > 0)
                    proxy.Cirrus.Snippets.AddTags(SnippetIDs, newTags);
            }
            CancelClick(null, null);
        }
        catch (Exception ex)
        {
            if (ex.GetType() == typeof(FaultException<InfoServiceFaultContract>))
            {
                lblError.Visible = true;
                lblError.Text = (ex as FaultException<InfoServiceFaultContract>).Detail.Message;
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ex.Message;
            }
        }
    }

    protected void CancelClick(object sender, EventArgs e)
    {
        Response.Redirect("ConfigSnippets.aspx");
    }

    private void BindControls()
    {
        using (var proxy = isLayer.GetProxy())
        {
            var existTags = proxy.Cirrus.Snippets.GetTagsList();
            List<string> removeTags;

            if (SnippetIDs.Length > 0)
                removeTags = proxy.Cirrus.Snippets.GetTagsListForSnippets(SnippetIDs);
            else
                removeTags = new List<string>();

            if (existTags != null)
            {
                repeaterExistingTags.DataSource = existTags;
                repeaterExistingTags.DataBind();
            }

            if (removeTags != null)
            {
                contentRemoveTagsNA.Visible = removeTags.Count < 1;
                repeaterRemoveTags.DataSource = removeTags;
                repeaterRemoveTags.DataBind();
            }
        }
    }

    private List<string> GetSelectedTags(Repeater repeater)
    {
        var selTags = new List<string>();
        foreach (RepeaterItem item in repeater.Items)
        {
            var cbSelected = item.FindControl(@"tagSelected") as CheckBox;
            if (cbSelected != null && cbSelected.Checked)
            {
                var ihName = item.FindControl(@"tagName") as HtmlInputHidden;
                if (ihName != null)
                {
                    selTags.Add(ihName.Value);
                }
            }
        }
        return selTags;
    }

    private List<string> GetNewTags()
    {
        var newTags = new List<string>();
        foreach (var name in txtAddTags.Text.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries))
        {
            var tagName = name.Trim();
            if (tagName.Length > 0)
            {
                newTags.Add(tagName);
            }
        }
        return newTags;
    }

    private void InitPageTitle()
    {
        var defaultPageTitle = Resources.NCMWebContent.WEBDATA_VM_33;
        var comma = string.Empty;
        var snippetNames = new StringBuilder();

        var strIds = new StringBuilder();
        foreach (var Id in SnippetIDs)
        {
            strIds.AppendFormat(@"'{0}',", Id.ToString());
        }

        if (strIds.Length > 0)
        {
            using (var proxy = isLayer.GetProxy())
            {
                var dt = proxy.Query(
                    $@"Select C.Name From Cirrus.ConfigSnippets As C Where C.Id In ({strIds.ToString().Trim(',')})");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        snippetNames.AppendFormat(@"{0}{1}", comma, dr["Name"]);
                        comma = @", ";
                    }
                    Page.Title = string.Format(Resources.NCMWebContent.WEBDATA_VM_34, CommonHelper.EncodeXmlToString(snippetNames.ToString()));
                }
                else
                {
                    TagsHolder.Visible = false;
                    Submit.Visible = false;

                    lblError.Visible = true;
                    lblError.Text = Resources.NCMWebContent.WEBDATA_VM_35;

                    Page.Title = defaultPageTitle;
                }
            }
        }
        else
            Page.Title = defaultPageTitle;
    }

    protected void SiteMapPath_OnInit(object sender, EventArgs e)
    {
        try
        {
            var renderer = rendererFactory();
            renderer.SetUpData(null);
            NodeSiteMapPath.SetUpRenderer(renderer);
        }
        catch (Exception ex)
        {
            logger.Error("Error initializing the site map.", ex);
            throw;
        }
    }
}
