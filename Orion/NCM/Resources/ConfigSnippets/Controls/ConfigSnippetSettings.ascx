<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConfigSnippetSettings.ascx.cs" Inherits="Orion_NCM_Resources_ConfigSnippets_Controls_ConfigSnippetSettings" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<style type="text/css">
    .autocomplete_completionListElement 
    {  
        visibility : hidden;
        margin : 0px !Important;
        padding: 0px !Important;
        background-color : #fff;
        color : #000;
        border : #ccc;
        border-width : 1px;
        border-style : solid;
        cursor : 'default';
        overflow : auto;
        height : 100px;
        text-align : left; 
        list-style: none !Important;
        width: 501px !Important;
        z-index: 9999999;
    }
    
    .autocomplete_completionListElement li
    {
        display: block;
        padding: 1px !Important;
        margin: 0px;
    }
    
    .autocomplete_highlightedListItem
    {
        background-color: Highlight;
        color: white;
        padding: 1px;
    }
    
    .autocomplete_listItem 
    {
        background-color: window;
        color: windowtext;
        padding: 1px;
    }
</style>

<div>
    <table>
        <tr>
            <td colspan="2" class="Property" valign="top" style="border-bottom:0px groove transparent;">
                <b><%= Resources.NCMWebContent.WEBDATA_VM_14 %></b>
            </td>
        </tr>
        <tr>
            <td class="Property">
                <asp:TextBox ID="txtSnippetName" MaxLength="250" runat="server" TextMode="SingleLine" Font-Size="12px" Font-Names="Arial" BorderColor="#89A6C0" BorderWidth="1px" Width = "500px" Height="15px" />
            </td>
            <td class="Property" style="width:300px;font-weight:bold;">
                <asp:RequiredFieldValidator ID="validatorSnippetName" runat="server" Display="Dynamic" 
                    ControlToValidate="txtSnippetName" ErrorMessage="<%$ Resources:NCMWebContent, WEBDATA_VM_15 %>" >
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="height:10px"></td>
        </tr>
        <tr>
            <td colspan="2" class="Property" valign="top" style="border-bottom:0px groove transparent;">
                <b><%= Resources.NCMWebContent.WEBDATA_VM_16 %></b>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="Property">
                <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Font-Size="12px" Font-Names="Arial" BorderColor="#89A6C0" BorderWidth="1px" Width = "500px" Height="45px" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="height:10px"></td>
        </tr>
        <tr>
            <td colspan="2" class="Property" valign="top" style="border-bottom:0px groove transparent;">
                <b><%=Resources.NCMWebContent.WEBDATA_VK_497 %></b>
                <br/>
		        <span style="font-size:7pt;"><%=Resources.NCMWebContent.WEBDATA_VM_23 %></span>           
            </td>
        </tr>
        <tr>
            <td class="Property" valign="top">
                <asp:TextBox ID="txtTags" AutoCompleteType="Disabled" runat="server" TextMode="SingleLine" Font-Size="12px" Font-Names="Arial" BorderColor="#89A6C0" BorderWidth="1px" Width = "500px" Height="15px" />
                <ajaxToolkit:AutoCompleteExtender 
                    runat="server" 
                    ID="tagsCompleteExtender" 
                    TargetControlID="txtTags"
                    ServiceMethod="GetTagsList"
                    ServicePath="../../../Services/ConfigSnippets.asmx"
                    MinimumPrefixLength="1" 
                    CompletionInterval="1000"
                    EnableCaching="true"
                    CompletionSetCount="20" 
                    DelimiterCharacters=", "
                    CompletionListCssClass="autocomplete_completionListElement" 
                    CompletionListItemCssClass="autocomplete_listItem" 
                    CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                    />
            </td>
            <td class="Property" style="width:300px;vertical-align:middle;font-weight:bold;" valign="top">
                <asp:CustomValidator runat="server" ID="validatorTags" ControlToValidate="txtTags" 
                    ErrorMessage="<%$ Resources:NCMWebContent, WEBDATA_VM_17%>" Display="Dynamic" Enabled="true" ClientValidationFunction="ValidateTags">
                </asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="height:10px"></td>
        </tr>
        <tr>
            <td colspan="2" class="Property" valign="top">
                <b><asp:CheckBox ID="chkPreserveWhiteSpace" runat="server" Text="<%$ Resources:NCMWebContent, WEBDATA_VK_986%>" /></b>
                <br />
		        <span style="font-size:7pt;"><%=Resources.NCMWebContent.WEBDATA_VK_987 %></span>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="height:10px"></td>
        </tr>
    </table>
    <table>        
        <tr>
            <td class="Property" valign="top" style="border-bottom:0px groove transparent;">
                <b><%= Resources.NCMWebContent.WEBDATA_VM_18 %></b>
            </td>
        </tr>
        <tr>
            <td class="Property">
                <textarea class="lined" style="width:800px;height:500px;" id="txtSnippetScript" runat="server"></textarea>
            </td>
            <td class="Property" style="width:300px;font-weight:bold;" valign="top">
                <asp:RequiredFieldValidator ID="validatorSnippetScript" runat="server" Display="Dynamic" 
                    ControlToValidate="txtSnippetScript" ErrorMessage="<%$ Resources:NCMWebContent, WEBDATA_VM_19%>" >
                </asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
</div>