using System;
using System.Collections.Generic;
using System.Linq;

public partial class Orion_NCM_Resources_ConfigSnippets_Controls_ConfigSnippetSettings : System.Web.UI.UserControl
{
    public string SnippetName
    {
        get { return txtSnippetName.Text; }
        set { txtSnippetName.Text = value; }
    }

    public string SnippetDescription
    {
        get { return txtDescription.Text; }
        set { txtDescription.Text = value; }
    }

    public IEnumerable<string> SnippetTags
    {
        get 
        {
            return txtTags.Text
                .Split(@",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                .Where(t => !string.IsNullOrWhiteSpace(t))
                .Select(t => t.Trim());
        }
        set 
        {
            string comma = string.Empty;
            foreach (string tag in value)
            {
                txtTags.Text += comma + tag;
                comma = @", ";
            }
        }
    }

    public string SnippetScript
    {
        get { return txtSnippetScript.Value; }
        set { txtSnippetScript.Value = value; }
    }

    public bool PreserveWhiteSpace
    {
        get { return chkPreserveWhiteSpace.Checked; }
        set { chkPreserveWhiteSpace.Checked = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string linedTextArea = @"$(function() { $("".lined"").linedtextarea(	{selectedLine: -1} ); });";
        System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), @"key_linedtextarea", linedTextArea, true);
    }
}
