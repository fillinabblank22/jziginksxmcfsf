<%@ WebHandler Language="C#" Class="DownloadConfigSnippet" %>

using System;
using System.Web;

using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;

public class DownloadConfigSnippet : IHttpHandler 
{
    private const int IEMaxFileNameSize = 23;
    
    public void ProcessRequest(HttpContext context) 
    {
        var isLayer = new ISWrapper();
        
        var exportIdLookup = ExportConfigSnippetIds(context);

        if (exportIdLookup.Length == 1)
        {
            try
            {
                ConfigSnippet configSnippet;
                using (var proxy = isLayer.Proxy)
                {
                    configSnippet = proxy.Cirrus.Snippets.GetSnippet(exportIdLookup[0]);
                }
                var toExportStr = ImportExportConfigSnippets.ConfigSnippetExportToString(configSnippet);

                var fileName = GetExportFileName(context, configSnippet);
                context.Response.Clear();
                context.Response.AddHeader(@"Content-Disposition", @"attachment; filename=" + fileName);
                context.Response.ContentType = @"application/ncm-template";
                context.Response.Write(toExportStr);
            }
            catch (Exception ex)
            {
                var exStr = string.Empty;
                var divStr = @"<div style=""color:Red;font-size:12px;"">{0}</div>";
                if (ex.GetType() == typeof(System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>))
                {
                    exStr = string.Format(divStr, (ex as System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>).Detail.Message);
                }
                else
                {
                    exStr = string.Format(divStr, ex.Message);
                }
                context.Response.Clear();
                context.Response.Write(exStr);
            }
            context.ApplicationInstance.CompleteRequest();
        }
    }

    public bool IsReusable
    {
        get { return false; }
    }

    private static string GetExportFileName(HttpContext context, ConfigSnippet snippet)
    {
        return $@"""{EncodeFilenameIfNeeded(context, snippet.Name)}.ncm-template""";
    }

    private static string EncodeFilenameIfNeeded(HttpContext context, string filename)
    {
        var browserType = context.Request.Browser.Type.ToUpperInvariant();

        filename = ReplaceInvalidFileNameChars(filename);

        if (browserType.StartsWith(@"IE"))
        {
            //Internet Explorer cuts not ANSI file name from the beginning if it is larger than 23 symbols
            //This code cuts file name from the end.
            if (!IsAnsiString(filename) && (filename.Length > IEMaxFileNameSize))
                filename = filename.Substring(0, IEMaxFileNameSize);

            return HttpUtility.UrlPathEncode(filename);
        }
        
        return filename;
    }

    private static bool IsAnsiString(string str)
    {
        const char compareCharacter = '\x7F';
        foreach (var c in str)
        {
            if (c > compareCharacter)
                return false;
        }
        return true;
    }

    private static string ReplaceInvalidFileNameChars(string Filename)
    {
        var newFilename = string.Empty;

        foreach (var ch in Filename)
        {
            if (Array.IndexOf(System.IO.Path.GetInvalidFileNameChars(), ch) >= 0)
            {
                newFilename += '_';
            }
            else
            {
                newFilename += ch;
            }
        }

        newFilename = newFilename.Replace(@";", @"_");

        return newFilename;
    }
    
    private int[] ExportConfigSnippetIds(HttpContext context)
    {
        var queryString = string.Empty;

        if (!string.IsNullOrEmpty(context.Request[@"SnippetIds"]))
            queryString = context.Request[@"SnippetIds"];

        var exportIdStrings = queryString.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        var exportIdLookup = new int[exportIdStrings.Length];

        for (var i = 0; i < exportIdStrings.Length; i++)
        {
            exportIdLookup.SetValue(Convert.ToInt32(exportIdStrings[i]), i);
        }

        return exportIdLookup;
    }
}