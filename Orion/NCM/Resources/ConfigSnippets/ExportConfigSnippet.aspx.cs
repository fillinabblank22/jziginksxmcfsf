using System;
using System.Web.Script.Serialization;
using System.Text;

public partial class Orion_NCM_Resources_ConfigSnippets_ExportConfigSnippet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_VM_44;

        lbl1.NavigateUrl = ThwackUrl;

        if (!IsPostBack)
        {
            string postData = Server.UrlDecode(Request.Form[@"postData"]);
            
            if (!string.IsNullOrEmpty(postData))
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                int[] SnippetsIds = serializer.Deserialize<int[]>(postData);

                DownloadIFrame.Attributes[@"src"] = GenerateExportUrl(SnippetsIds);
            }
        }
    }

    protected static string ThwackUrl
    {
        get
        {
            return @"http://thwack.com/media/g/config-change-templates/default.aspx";
        }
    }

    private string GenerateExportUrl(int[] SnippetsIds)
    {
        StringBuilder sb = new StringBuilder(@"/Orion/NCM/Resources/ConfigSnippets/DownloadConfigSnippet.ashx?SnippetIds=");

        foreach (int SnippetId in SnippetsIds)
        {
            sb.AppendFormat(@"{0},", SnippetId);
        }
        return sb.ToString();
    }
}
