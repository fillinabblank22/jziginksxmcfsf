using System;
using System.Web;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.NCM.Web.Contracts;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.JobManagement;

public partial class Orion_NCM_Resources_ConfigSnippets_ExScriptPreview : System.Web.UI.Page
{
    private readonly IIsWrapper isWrapper;
    private readonly IMacroParser macroParser;

    public Orion_NCM_Resources_ConfigSnippets_ExScriptPreview()
    {
        isWrapper = ServiceLocator.Container.Resolve<IIsWrapper>();
        macroParser = ServiceLocator.Container.Resolve<IMacroParser>();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var nodeId = Request.QueryString[@"NodeID"];
        if (!string.IsNullOrEmpty(nodeId))
        {
            Initializer(new Guid(nodeId));
        }
        else
        {
            scriptContent.Text = Resources.NCMWebContent.WEBDATA_VK_494;
        }
    }

    private void Initializer(Guid nodeId)
    {
        var nodeName = HttpUtility.HtmlEncode(macroParser.ParseMacro(nodeId, @"${NodeCaption}"));
        Page.Title = string.Format(Resources.NCMWebContent.WEBDATA_VK_495, nodeName);

        if (!string.IsNullOrEmpty(Request.QueryString[@"NewJob"]))
        {
            if (JobWorkflowController.Job == null)
            {
                Page.Response.Redirect("/Orion/NCM/Resources/Jobs/JobsList.aspx");
            }
            else
            {
                var networkNode = JobWorkflowController.Job.JobDefinition.Nodes.NetworkNodes.Find(item => item.NodeID == nodeId);
                if (networkNode != null)
                    scriptContent.Text = networkNode.Script;
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(Request.QueryString[@"SnippetId"]))
            {
                int snippetId;
                if (int.TryParse(Request.QueryString[@"SnippetId"], out snippetId))
                {
                    using (var proxy = isWrapper.GetProxy())
                    {
                        var configSnippet = proxy.Cirrus.Snippets.GetSnippet(snippetId);
                        if (configSnippet != null)
                        {
                            snippetName.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_496, HttpUtility.HtmlEncode(configSnippet.Name));
                        }
                    }
                }
            }

            if (ExNodes.Instance.Nodes == null)
            {
                Page.Response.Redirect("/Orion/NCM/Resources/ConfigSnippets/ConfigSnippets.aspx");
            }
            else
            {
                ExNode node = null;
                if (ExNodes.Instance.Nodes.TryGetValue(nodeId, out node))
                {
                    scriptContent.Text = node.Script;
                }
                else
                {
                    scriptContent.Text = Resources.NCMWebContent.WEBDATA_VK_494;
                }
            }
        }
    }
}
