<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="ConfigSnippets.aspx.cs" Inherits="Orion_NCM_ConfigSnippets" Title="Untitled Page" %>

<%@ Register Src="~/Orion/NCM/NavigationTabBar.ascx" TagPrefix="ncm" TagName="NavigationTabBar" %>
<%@ Register Src="~/Orion/NCM/CustomPageLinkHidding.ascx" TagPrefix="ncm" TagName="custPageLinkHidding" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="ncm" Src="~/Orion/NCM/Controls/IconNCMSettings.ascx" TagName="IconNCMSettings" %>
<%@ Register TagPrefix="ncm1" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Conten1" ContentPlaceHolderID="ncmHeadPlaceHolder" runat="server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <ncm:custPageLinkHidding ID="CustPageLinkHidding1"  runat="server" />
    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/ViewConfigSnippets.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippetsHelper.js"></script>
    
    <script type="text/javascript">
        var thwackUserInfo = <%=_thwackUserInfo%>;
    </script>
    
    <script type="text/javascript">
        SW.NCM.IsPowerUser = <%=SolarWinds.NCMModule.Web.Resources.SecurityHelper.IsPermissionExist(SolarWinds.NCMModule.Web.Resources.SecurityHelper._canExecuteScript).ToString().ToLower() %>;
    </script>

    <ncm1:ExtLocalDateTimePattern ID="ExtLocalDateTimePattern1" runat="server"/>
    <ncm1:ConfigSnippetUISettings ID="ConfigSnippetUISettings1" runat="server" Name="NCM_ConfigSnippets_Columns" DefaultValue="[]" RenderTo="columnModel" />
    
    
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippets.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmCustomizePagePlaceHolder" runat="server">
    <ncm:IconNCMSettings ID="IconNCMSettings1" runat="server"/>
    <orion:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionNCMConfigChangeTemplates" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmHelpIconPlaceHolder" runat="server">
   
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" runat="server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmMainContentPlaceHolder" runat="server">
    
    <%if (IsDemoMode) {%>
		<div id="isDemoMode" style="display:none;"></div>
	<%}%>
	
	
    <%foreach (string setting in new string[] { @"GroupByValue"}) { %>
		<input type="hidden" name="NCM_ConfigSnippets_<%=setting%>" id="NCM_ConfigSnippets_<%=setting%>" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get($@"NCM_ConfigSnippets_{setting}")%>' />
	<%}%>
	
    <asp:Panel id="ContentContainer" runat="server">
        <div style="padding:10px;">
            <ncm:NavigationTabBar runat="server"/>
        
            <div id="tabPanel" class="tab-top">

	                <table class="ExistingElements" cellpadding="0" cellspacing="0" width="100%">
		                <tr>
                            <td colspan="2">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td style="font-size: 11px; padding-bottom: 10px;width:75%;">
                                        <%= Resources.NCMWebContent.WEBDATA_VM_08 %>
                                            <a href="/Orion/NCM/Resources/ConfigSnippets/Thwack/Default.aspx" style="text-decoration:underline;color:#336699;"> <%= Resources.NCMWebContent.WEBDATA_VM_09 %> &#0187; </a>
                                        </td>
                                        <td align="right" style="padding-bottom:5px;">
                                            <label for="search"><%=Resources.NCMWebContent.WEBDATA_VM_13%></label>
                                            <input id="search" type="text" size="20" />
                                            <!-- need to think about localization button here-->
                                            <%= $@"<input id=""doSearch"" type=""button"" value=""{Resources.NCMWebContent.WEBDATA_VY_19}"" />" %>			    
                                        </td>
                                    </tr>                    
                                </table>
                            </td>		        
		                </tr>
		                <tr valign="top" align="left">

			                <td style="padding-right: 10px;width: 15%;">
				                <div class="ElementGrouping">
					                <div class="ncm_GroupSection x-panel-header x-toolbar x-small-editor">
						                <div><%=Resources.NCMWebContent.WEBDATA_VM_10%></div>
					                </div>
					                <ul class="GroupItems" style="width:100%;"></ul>
				                </div>

                                <!-- The Thwack Advertisement -->
                                <div id="ncm_ThwackAd">
                                    <div>
                                        <a target="_blank" href="http://thwack.com/media/g/config-change-templates/default.aspx"><img src="<%=SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath(@"NCM", @"ConfigSnippets/thwack_share.gif") %>" alt="" /></a>                        
                                    </div>
                                    <p><%=Resources.NCMWebContent.WEBDATA_VM_11 %></p>
                                   
                                    <span class="LinkArrow">&#0187;</span> <a target="_blank" href="http://thwack.com/media/g/config-change-templates/default.aspx"><%=Resources.NCMWebContent.WEBDATA_VM_12 %></a>
                                </div>

			                </td>
			                <td id="gridCell">
                                <div id="Grid"/>
			                </td>
		                </tr>
	                </table>
                	
	                <div id="originalQuery"></div>	
	                <div id="test"></div>
	                <pre id="stackTrace"></pre>
	            </div>
        
            <div id="snippetDescriptionDialog" class="x-hidden">
                <div id="snippetDescriptionBody" class="x-panel-body" style="padding: 10px; height: 210px; overflow: auto;"></div>
            </div>
         
            <div id="snippetScriptErrorDialog" class="x-hidden">
                <div id="snippetScriptErrorBody" class="x-panel-body" style="padding: 10px; height: 210px; overflow: auto;"></div>
            </div>
        </div>
    </asp:Panel>
	<div style="padding:10px;" id="ErrorContainer" runat="server"/>
</asp:Content>

