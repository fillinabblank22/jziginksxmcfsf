using System;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;
using System.Linq;
using System.ServiceModel;
using System.Web.UI;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.InformationService.Contract2;
using SolarWinds.NCM.Web.Contracts.SiteMap;
using SolarWinds.Orion.Common;

public partial class Orion_NCM_Resources_ConfigSnippets_EditConfigSnippet : Page
{
    private ConfigSnippet configSnippet;
    private int snippetId;
    private bool _isDemoMode = OrionConfiguration.IsDemoServer;

    private readonly IIsWrapper isLayer;
    private readonly ILogger logger;
    private readonly Func<INcmSiteMapRenderer> rendererFactory;

    public Orion_NCM_Resources_ConfigSnippets_EditConfigSnippet()
    {
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
        logger = ServiceLocator.Container.Resolve<Func<Type, ILogger>>()(GetType());
        rendererFactory = ServiceLocator.Container.Resolve<Func<INcmSiteMapRenderer>>();
    }

    private string HelpUrlFragment
    {
        get
        {
            if (string.IsNullOrEmpty(Request[@"SnippetID"]))
            {
                return @"OrionNCMPHCreatingConfigTemplate";
            }

            return @"OrionNCMAGEditingConfigTemplate";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        helpBtn.HelpUrlFragment = HelpUrlFragment;

        Submit.Visible = !_isDemoMode;
        SaveAsCopy.Visible = !_isDemoMode;
        Execute.Visible = !_isDemoMode;

        try
        {
            ValidatePermissions();

            if (string.IsNullOrEmpty(Request[@"SnippetID"]))
            {
                Page.Title = Resources.NCMWebContent.WEBDATA_VM_29;
            }
            else
            {
                snippetId = Convert.ToInt32(Request[@"SnippetID"]);

                using (var proxy = isLayer.GetProxy())
                {
                    configSnippet = proxy.Cirrus.Snippets.GetSnippet(snippetId);

                    if (!Page.IsPostBack)
                    {
                        snippetSettings.SnippetName = configSnippet.Name;
                        snippetSettings.SnippetDescription = configSnippet.Description;
                        snippetSettings.SnippetTags = configSnippet.Tags;
                        snippetSettings.SnippetScript = configSnippet.Script;
                        snippetSettings.PreserveWhiteSpace = configSnippet.PreserveWhiteSpace;
                    }

                    Page.Title = string.Format(Resources.NCMWebContent.WEBDATA_VM_69, CommonHelper.EncodeXmlToString(configSnippet.Name));
                    
                    var validate = false;
                    if (Request[@"validate"] != null)
                        bool.TryParse(Request[@"validate"], out validate);

                    if (validate == true && configSnippet != null)
                        ValidateConfigSnippet();
                }
            }
        }
        catch (Exception ex)
        {
            if (string.IsNullOrEmpty(Request[@"SnippetID"]))
            {
                Page.Title = Resources.NCMWebContent.WEBDATA_VM_29;
            }
            else
            {
                Page.Title = Resources.NCMWebContent.WEBDATA_VM_30;
            }

            DisplayException(ex);
        }
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (_isDemoMode)
            return;

        if(SaveConfigSnippet())
            Response.Redirect("ConfigSnippets.aspx");
    }

    protected void ValidateClick(object sender, EventArgs e)
    {
        ValidateConfigSnippet();
    }

    protected void SaveAsCopyClick(object sender, EventArgs e)
    {
        if (_isDemoMode)
            return;

        try
        {
            using (var proxy = isLayer.GetProxy())
            {
                configSnippet = new ConfigSnippet
                {
                    Name = snippetSettings.SnippetName,
                    Description = snippetSettings.SnippetDescription,
                    Tags = snippetSettings.SnippetTags.ToArray(),
                    Script = snippetSettings.SnippetScript,
                    PreserveWhiteSpace = snippetSettings.PreserveWhiteSpace
                };
                proxy.Cirrus.Snippets.SaveSnippetAsCopy(configSnippet);
            }

            Response.Redirect("ConfigSnippets.aspx");
        }
        catch (Exception ex)
        {
            DisplayException(ex);
        }
    }

    protected void ExecuteClick(object sender, EventArgs e)
    {
        if (_isDemoMode)
            return;

        if (!ValidateConfigSnippet())
            return;

        if (SaveConfigSnippet())
            Response.Redirect($"ExecuteConfigSnippet.aspx?SnippetID={snippetId}");
    }

    protected void CancelClick(object sender, EventArgs e)
    {
        Response.Redirect("ConfigSnippets.aspx");
    }

    private bool ValidateConfigSnippet()
    {
        var exHelper = ServiceLocator.Container.Resolve<IExHelper>();
        var errorString = exHelper.ValidateConfigSnippetScript(snippetSettings.SnippetScript);

        OkHolder.Visible = string.IsNullOrEmpty(errorString);
        OkText.Text = Resources.NCMWebContent.WEBDATA_VM_59;

        ErrorControl.Visible = !string.IsNullOrEmpty(errorString);
        ErrorControl.VisiblePermissionLink = false;
        ErrorControl.ErrorMessage = string.Format(Resources.NCMWebContent.WEBDATA_VM_60, errorString);

        return string.IsNullOrEmpty(errorString) ? true : false;
    }

    private bool SaveConfigSnippet()
    {
        try
        {
            using (var proxy = isLayer.GetProxy())
            {
                configSnippet = new ConfigSnippet();

                if (string.IsNullOrEmpty(Request[@"SnippetID"]))
                {
                    configSnippet.Name = snippetSettings.SnippetName;
                    configSnippet.Description = snippetSettings.SnippetDescription;
                    configSnippet.Tags = snippetSettings.SnippetTags.ToArray();
                    configSnippet.Script = snippetSettings.SnippetScript;
                    configSnippet.PreserveWhiteSpace = snippetSettings.PreserveWhiteSpace;
                    snippetId = proxy.Cirrus.Snippets.AddSnippet(configSnippet);
                }
                else
                {
                    configSnippet.Id = snippetId;
                    configSnippet.Name = snippetSettings.SnippetName;
                    configSnippet.Description = snippetSettings.SnippetDescription;
                    configSnippet.Tags = snippetSettings.SnippetTags.ToArray();
                    configSnippet.Script = snippetSettings.SnippetScript;
                    configSnippet.PreserveWhiteSpace = snippetSettings.PreserveWhiteSpace;
                    proxy.Cirrus.Snippets.UpdateSnippet(configSnippet);
                }
            }

            return true;
        }
        catch (Exception ex)
        {
            DisplayException(ex);
        }

        return false;
    }

    private void DisplayException(Exception ex)
    {
        SnippetSettingsHolder.Visible = false;

        Submit.Visible = false;
        ValidateBtn.Visible = false;
        SaveAsCopy.Visible = false;
        Execute.Visible = false;

        OkHolder.Visible = false;
        ErrorControl.Visible = true;

        if (ex.GetType() == typeof(FaultException<InfoServiceFaultContract>))
        {
            ErrorControl.VisiblePermissionLink = false;
            ErrorControl.ErrorMessage = (ex as FaultException<InfoServiceFaultContract>).Detail.Message;
        }
        else
        {
            if (ex.GetType() != typeof(InvalidOperationException))
            {
                ErrorControl.VisiblePermissionLink = false;
            }

            ErrorControl.ErrorMessage = ex.Message;
        }
    }

    private void ValidatePermissions()
    {
        if (!SecurityHelper.IsPermissionExist(SecurityHelper._canExecuteScript))
        {
            if (string.IsNullOrEmpty(Page.Request.QueryString[@"SnippetID"]))
            {
                throw new InvalidOperationException(Resources.NCMWebContent.WEBDATA_VM_31);
            }

            throw new InvalidOperationException(Resources.NCMWebContent.WEBDATA_VM_32);
        }
    }

    protected void SiteMapPath_OnInit(object sender, EventArgs e)
    {
        try
        {
            var renderer = rendererFactory();
            renderer.SetUpData(null);
            NodeSiteMapPath.SetUpRenderer(renderer);
        }
        catch (Exception ex)
        {
            logger.Error("Error initializing the site map.", ex);
            throw;
        }
    }
}
