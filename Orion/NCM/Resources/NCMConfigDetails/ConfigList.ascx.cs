using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;

using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_NetPerfMon_Resources_CirrusResources_ConfigList : BaseResourceControl
{
	protected override string DefaultTitle
	{
        get { return Resources.NCMWebContent.WEBDATA_VK_125; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get
		{
            var viewManagerAdapter = ServiceLocator.Container.Resolve<IViewManager>();
            return viewManagerAdapter.GetSupportedInterfacesForViewType(@"NCMConfigDetails");
		}
	}

    public sealed override string DisplayTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(this.Resource.Properties[@"Title"]))
                return this.Resource.Properties[@"Title"];
            return Resources.NCMWebContent.WEBDATA_VK_125;
        }
    }

    public override string HelpLinkFragment
    {
        get { return @"OrionNCMWebConfigList"; }
    }

    public override string EditURL
    {
        get { return ExtendCustomEditUrl(@"/Orion/NCM/Resources/NCMNodeDetails/ConfigListEdit.aspx"); }
    }

    protected override void OnInit(EventArgs e)
    {
        ConfigList resource = new ConfigList();
        resource.ResourceSettings = this.Resource;
        resource.ResourceContainer = Wrapper;
        Wrapper.Content.Controls.Add(resource);

        base.OnInit(e);
    }
}
