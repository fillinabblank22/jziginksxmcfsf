<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMResourceEditMasterPage.master" AutoEventWireup="true" CodeFile="ConfigDetailsEdit.aspx.cs" Inherits="Orion_NCM_Resources_NCMConfigDetails_ConfigDetailsEdit" ValidateRequest="false"%>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmMainContent" Runat="Server">
     <table>
        <tr>
            <td class="PageHeader">
                <%=ResourceHeader %>
            </td>
        </tr>
        <tr>
            <td style="padding-top:10px;">
                <b><%=Resources.NCMWebContent.WEBDATA_VK_128 %></b><br />
                <asp:TextBox runat="server" ID="ResourceTitle" Width="250"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td nowrap style="padding-top:5px">
                <b><%=Resources.NCMWebContent.WEBDATA_VK_177 %></b><asp:CheckBox ID="chbLineCheck" runat="server"/>
            </td>
        </tr>
        <tr>
            <td style="padding-top:20px;">
                <orion:LocalizableButton ID="btnSubmit" runat="server" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitClick"></orion:LocalizableButton>
            </td>
        </tr>
    </table>
</asp:Content>
