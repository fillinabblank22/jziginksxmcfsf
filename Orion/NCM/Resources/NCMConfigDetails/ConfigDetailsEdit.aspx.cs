using System;
using System.Web.UI;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.Resources;
using SolarWinds.NCMModule.Web.Resources.Wrappers;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NCM_Resources_NCMConfigDetails_ConfigDetailsEdit : Page
{
    private readonly IEditResourcePageService editResourceService;

    public Orion_NCM_Resources_NCMConfigDetails_ConfigDetailsEdit()
    {
        editResourceService = ServiceLocator.Container.Resolve<IEditResourcePageService>();
    }

    protected ResourceInfo Resource { get; private set; }
    protected string ResourceHeader => editResourceService.GetHeader(Resource);

    protected override void OnInit(EventArgs e)
    {
        Resource = editResourceService.Init(new PageWrap(this), ResourceTitle, null);

        SetupControlFromRequest();

        base.OnInit(e);
    }

    private void SetupControlFromRequest()
    {
        chbLineCheck.Checked = string.IsNullOrEmpty(Resource.Properties[@"LineCheck"]) ||
                               Convert.ToBoolean(Resource.Properties[@"LineCheck"]);
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        editResourceService.SaveChanges(Resource, ResourceTitle, null);

        Resource.Properties[@"LineCheck"] = chbLineCheck.Checked.ToString();

        Response.Redirect(BaseResourceControl.GetEditPageReturnUrl(Request.QueryString, Resource.View.ViewID.ToString()));
    }
}
