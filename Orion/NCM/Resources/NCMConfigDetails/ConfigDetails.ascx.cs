using System;
using System.Collections.Generic;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.NCM.Web.Contracts.Search;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using System.Linq;
using System.Text;
using System.Web;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_NetPerfMon_Resources_CirrusResources_ConfigDetails : BaseResourceControl
{
    private readonly IViewManager viewManagerAdapter;
    private readonly IIsWrapper isWrapper;
    private readonly ILogger logger;
    private readonly ISearchEngine searchEngine;

    public Orion_NetPerfMon_Resources_CirrusResources_ConfigDetails()
    {
        viewManagerAdapter = ServiceLocator.Container.Resolve<IViewManager>();
        isWrapper = ServiceLocator.Container.Resolve<IIsWrapper>();
        searchEngine = ServiceLocator.Container.Resolve<ISearchEngine>();
        logger = ServiceLocator.Container.Resolve<Func<Type, ILogger>>()(GetType());
    }

	protected override string DefaultTitle => Resources.NCMWebContent.WEBDATA_VK_176;

    public override IEnumerable<Type> RequiredInterfaces => viewManagerAdapter.GetSupportedInterfacesForViewType(@"NCMConfigDetails");

    public sealed override string DisplayTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(Resource.Properties[@"Title"]))
                return Resource.Properties[@"Title"];
            return Resources.NCMWebContent.WEBDATA_VK_176;
        }
    }

    public override string HelpLinkFragment => @"OrionNCMWebConfigDetails";

    public override string EditURL => ExtendCustomEditUrl(@"/Orion/NCM/Resources/NCMConfigDetails/ConfigDetailsEdit.aspx");

    public string SaveURL => $@"/Orion/NCM/Resources/NCMConfigDetails/ConfigExporter.ashx{(ConfigId.HasValue ? $@"?configID={ConfigId.Value}" : string.Empty)}";

    private Guid? ConfigId => Request.QueryNullableGuid("configID");

    protected override void OnInit(EventArgs e)
    {
        try
        {
            SetDefaultResourceSettings();

            Wrapper.ShowManageButton = !IsBinaryConfig();
            Wrapper.ManageButtonText = Resources.NCMWebContent.WEBDATA_VM_218;
            Wrapper.ManageButtonTarget = SaveURL;

            ConfigDetails resource = new ConfigDetails
            {
                ID = @"ConfigDetailsResource",
                ResourceSettings = Resource,
                ResourceContainer = Wrapper,
                LinesToHighlight = FindLinesToHighlight()
            };
            Wrapper.Content.Controls.Add(resource);

            base.OnInit(e);
        }
        catch (Exception ex)
        {
            logger.Error("Error initializing the Config Details resource.", ex);
            throw;
        }
    }

    private int[] FindLinesToHighlight()
    {
        var wholeWordSearch = Request.QueryNullableBoolean("WholeWordSearch");
        var searchStringRaw = Request.QueryString[@"SearchString"];

        if (ConfigId.HasValue && wholeWordSearch.HasValue && !string.IsNullOrWhiteSpace(searchStringRaw))
        {
            var searchString = Encoding.UTF8.GetString(Convert.FromBase64String(searchStringRaw));

            return searchEngine.FindLineNumbersOfMatches(ConfigId.Value, searchString, wholeWordSearch.Value, HttpContext.Current.Profile.UserName).ToArray();
        }

        return null;
    }

    private void SetDefaultResourceSettings()
    {
        NPMDefaultSettingsHelper settingHelper = new NPMDefaultSettingsHelper(Resource);
        if (string.IsNullOrEmpty(Resource.Properties[@"LineCheck"]))
        {
            settingHelper.SetSetting(@"LineCheck", @"True");
        }
    }

    private bool IsBinaryConfig()
    {
        if (ConfigId != null)
        {
            using (var proxy = isWrapper.GetProxy())
            {
                var dt = proxy.Query($@"SELECT IsBinary FROM Cirrus.ConfigArchive WHERE ConfigID='{ConfigId}'");
                if (dt != null && dt.Rows.Count > 0)
                {
                    return Convert.ToBoolean(dt.Rows[0]["IsBinary"]);
                }
            }
        }
        return false;
    }
}
