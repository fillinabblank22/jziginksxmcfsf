using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;

using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_NetPerfMon_Resources_CirrusResources_CompareConfigs : BaseResourceControl
{
	protected override string DefaultTitle
	{
        get { return Resources.NCMWebContent.WEBDATA_VK_126; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get
		{
            var viewManagerAdapter = ServiceLocator.Container.Resolve<IViewManager>();
            return viewManagerAdapter.GetSupportedInterfacesForViewType(@"NCMConfigDetails");
		}
	}

    public sealed override string DisplayTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(this.Resource.Properties[@"Title"]))
                return this.Resource.Properties[@"Title"];
            return Resources.NCMWebContent.WEBDATA_VK_126;
        }
    }

    public sealed override string SubTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(this.Resource.Properties[@"SubTitle"]))
                return this.Resource.Properties[@"SubTitle"];
            return string.Empty;
        }
    }

    public override string HelpLinkFragment
    {
        get { return @"OrionNCMWebCompareConfigurations"; }
    }

    public override string EditURL
    {
        get { return ExtendCustomEditUrl(@"/Orion/NCM/Resources/NCMCommonResources/NCMCommonEdit.aspx"); }
    }

    protected override void OnInit(EventArgs e)
    {
        SetDefaultResourceSettings();

        CompareConfigs resource = new CompareConfigs();
        resource.ResourceSettings = this.Resource;
        resource.ResourceContainer = Wrapper;
        Wrapper.Content.Controls.Add(resource);

        base.OnInit(e);
    }

    private void SetDefaultResourceSettings()
    {
        NPMDefaultSettingsHelper settingHelper = new NPMDefaultSettingsHelper(this.Resource);
        settingHelper.SetSetting("SWQLGetConfigTitle", "SELECT C.ConfigTitle, C.Baseline FROM Cirrus.ConfigArchive AS C WHERE C.ConfigID='{ConfigID}'");
        settingHelper.SetSetting("SWQLCompareConfigs", "Cirrus.Reports,Compare,{ConfigID1},{ConfigID2}");
        settingHelper.SetSetting("SWQLConfigs", "SELECT TOP 10 C.ConfigID, C.BaseConfigID, C.ConfigTitle, C.ModifiedTime, C.ConfigType, C.Baseline FROM Cirrus.ConfigArchive AS C WHERE C.NodeID='{NodeID}' ORDER BY C.ModifiedTime DESC");
        settingHelper.SetSetting("SWQLGetNodeID", "SELECT C.NodeID FROM Cirrus.ConfigArchive AS C WHERE C.ConfigID='{ConfigID}'");
    }
}
