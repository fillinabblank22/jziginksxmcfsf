﻿<%@ WebHandler Language="C#" Class="ConfigExporter" %>

using System;
using System.ServiceModel;
using System.Web;
using System.Web.SessionState;
using SolarWinds.InformationService.Contract2;
using SolarWinds.NCMModule.Web.Resources;

public class ConfigExporter : IHttpHandler, IRequiresSessionState
{
    private readonly ISWrapper _isLayer = new ISWrapper();

    public void ProcessRequest(HttpContext context)
    {
        try
        {
            var configId = GetConfigId(context);
            var configTuple = GetConfigFromDB(configId);
            
            var time = configTuple.Item1;
            var configType = configTuple.Item2;
            var configText = configTuple.Item3;
            var fileName = $@"{time}_{configType}.config".Replace(@":", @"-").Replace('/', '.').Replace(' ', '_');
            
            
            context.Response.Clear();
            context.Response.ContentType = @"text/plain";
            context.Response.AddHeader(@"Content-Disposition", $@"attachment; filename={fileName}");
            context.Response.Write(configText);
        }
        catch (Exception ex)
        {
            var exStr = string.Empty;
            var divStr = @"<div style=""color:Red;font-size:12px;"">{0}</div>";
            if (ex.GetType() == typeof (FaultException<InfoServiceFaultContract>))
            {
                exStr = string.Format(divStr, (ex as FaultException<InfoServiceFaultContract>).Detail.Message);
            }
            else
            {
                exStr = string.Format(divStr, ex.Message);
            }
            context.Response.Clear();
            context.Response.Write(exStr);
        }
    }

    public bool IsReusable
    {
        get { return false; }
    }

    private string GetConfigId(HttpContext context)
    {
        if (context.Request[@"configID"] == null)
            throw new ArgumentNullException("Unable to get configId");

        return context.Request[@"configID"];
    }

    private Tuple<string, string, string> GetConfigFromDB(string configID)
    {
        using (var proxy = _isLayer.Proxy)
        {
            var dt =
                proxy.Query($@"
                    SELECT        CA.DownloadTime, CA.ConfigType, CA.Config
                    FROM	Cirrus.ConfigArchive AS CA 
			        WHERE CA.ConfigID = '{configID}'");
            if (dt != null && dt.Rows.Count > 0)
            {
                var config = dt.Rows[0]["Config"].ToString();
                var configType = dt.Rows[0]["ConfigType"].ToString();
                var downloadTime = dt.Rows[0]["DownloadTime"].ToString();

                return Tuple.Create(downloadTime, configType, config);
            }
        }
        return null;
    }
}