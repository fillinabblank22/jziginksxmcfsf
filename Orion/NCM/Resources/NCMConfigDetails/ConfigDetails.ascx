<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConfigDetails.ascx.cs" Inherits="Orion_NetPerfMon_Resources_CirrusResources_ConfigDetails" %>

<orion:resourceWrapper runat="server" ID="Wrapper" >
    <content> 
        <style type="text/css">   
            div#ncmResourceWrapper div.ResourceWrapper
            {
                overflow-x: visible;
            }
            [id*="_ConfigDetailsResource"] .Property
            {
                margin: 0px;
                padding: 0px;
            }
        </style>
    </content>
</orion:resourceWrapper>
  
    