using System;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_NCMResourceEditMasterPage : System.Web.UI.MasterPage
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!SecurityHelper.IsValidNcmAccountRole)
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_VK_356}");
        }
    }
}
