﻿using System;
using SolarWinds.Orion.Web.UI;
using System.Collections.Generic;
using System.Globalization;
using System.Web;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.NCM.Web.Contracts.SiteMap;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.NodeManagement;

public partial class Orion_NCM_SecurityPolicyDetails : OrionView
{
    private readonly ILogger logger;
    private readonly Func<INcmSiteMapRenderer> rendererFactory;

    public Orion_NCM_SecurityPolicyDetails()
    {
        logger = ServiceLocator.Container.Resolve<Func<Type, ILogger>>()(GetType());
        rendererFactory = ServiceLocator.Container.Resolve<Func<INcmSiteMapRenderer>>();
    }

    public override string ViewType => @"NCMSecurityPolicyDetails";

    protected void Page_Load(object sender, EventArgs e)
    {
        var bytes = Convert.FromBase64String(Page.Request.QueryString[@"PolicyName"]);
        Page.Title = string.Format(Resources.NCMWebContent.WEBDATA_VK_503, System.Text.Encoding.UTF8.GetString(bytes));

        resourceContainer.DataSource = ViewInfo;
        resourceContainer.DataBind();
    }

    protected void SiteMapPath_OnInit(object sender, EventArgs e)
    {
        try
        {
            var renderer = rendererFactory();
            var coreNodeId = Convert.ToInt32(Page.Request.QueryString[@"NodeId"], CultureInfo.InvariantCulture);

            renderer.SetUpData(
                new KeyValuePair<string, string>(@"NodeID", NcmNodeHelper.GetNcmNodeId(coreNodeId, HttpContext.Current.Profile.UserName).ToString()),
                new KeyValuePair<string, string>(@"CoreNodeID", Convert.ToString(coreNodeId)));

            NodeSiteMapPath.SetUpRenderer(renderer);
        }
        catch (Exception ex)
        {
            logger.Error("Error initializing the site map.", ex);
            throw;
        }
    }
}