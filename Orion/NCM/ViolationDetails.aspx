<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViolationDetails.aspx.cs" Inherits="Orion_NCM_Default" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head2" runat="server">
    <%@ Register Src="~/Orion/NCM/Controls/ViolationDetailsTree.ascx" TagName="ViolationDetailsTree" TagPrefix="tcl" %>
    <title>Untitled Page</title>
</head>

<body>
    <form id ="test" runat="server">
        <asp:ScriptManager id="sss" runat="server"/>
        <script type="text/javascript" src="JavaScript/main.js?v=1.0"></script>
        <link rel="stylesheet" type="text/css" href="/WebEngine/Resources/common/globalStyles.css" />
        <link rel="stylesheet" type="text/css" href="/WebEngine/Resources/SlateGray.css" />
        <link rel="stylesheet" type="text/css" href="styles/NCMResources.css" />
        <tcl:ViolationDetailsTree ID="violationDetailsTree" runat="server"></tcl:ViolationDetailsTree>
    </form>	
</body> 
</html>
    
    

      
             
               
