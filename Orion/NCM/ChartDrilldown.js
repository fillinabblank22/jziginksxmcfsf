﻿Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.QuickTips.init();

SW.NCM.ChartDrilldown = function () {

    function gridResize() {
        if (grid != null) {
            grid.setWidth(1);
            grid.setWidth($('#gridCell').width());

            var h = getGridHeight()
            grid.setHeight(getGridHeight());

            var groupItems = $(".GroupItems");
            groupItems.height(h - $(".ncm_GroupSection").height() - 14);
        }
    }

    function getGridHeight() {
        var top = $('#gridCell').offset().top;
        return $(window).height() - top - $('#footer').height() - 100;
    }

    var getGroupByValues = function (groupBy, onSuccess) {

        var param = Ext.util.Format.htmlDecode(groupBy);
        ORION.callWebService("/Orion/NCM/Services/ChartDrilldown.asmx",
            "GetValuesAndCountForProperty", { property: groupBy },
            function (result) {
                onSuccess(ORION.objectFromDataTable(result));
            });
    };

    var loadGroupByValues = function (groupBy, selectValue) {
        var groupItems = $(".GroupItems").text('@{R=NCM.Strings;K=WEBJS_VK_04;E=js}');

        getGroupByValues(groupBy, function (result) {
            groupItems.empty();
            $(result.Rows).each(function () {
                var value = Ext.util.Format.htmlEncode(this.theValue);
                if (value == null) value = "";
                value = String(value);
                if (value.startsWith('/Date(')) {
                    if (value.match(/\/Date\((\d+)\)\//gi) != null) {
                        var date = eval(value.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
                        value = Ext.util.Format.date(date, ExtDaTimeLocalePattern.DateTimeLong);
                    }
                    else {
                        value = "";
                    }
                }
                var text = groupByItemNameRender(groupBy, $.trim(value));
                var title = text;
                if (text.length > 25) { text = String.format("{0}...", text.substr(0, 25)); };
                var disp = (text) + " (" + this.theCount + ")";
                $("<a href='#'></a>")
                    .attr('value', Ext.util.Format.htmlEncode(value)).attr('title', title)
                    .text(disp).click(selectGroup).appendTo(".GroupItems").wrap("<li class='group-core-node-item'/>");
            });

            var toSelect = $(".group-core-node-item a[value='" + selectValue + "']");
            if (toSelect.length === 0) {
                toSelect = $(".group-core-node-item a:first");
            }

            toSelect.click();
        });
    };

    var selectGroup = function () {
        $(this).parent().addClass("SelectedGroupItem").siblings().removeClass("SelectedGroupItem");
        var value = $(".SelectedGroupItem a").attr('value');
        value = Ext.util.Format.htmlDecode(value);
        var groupBy = combo.getValue();
        refreshObjects(groupBy, value, '');
        return false;
    };

    var groupByItemNameRender = function (group, value) {
        if (value == "")
            return '@{R=NCM.Strings;K=WEBJS_VK_66;E=js}';

        if (group == "Status") {
            switch (value) {
                case "1": return '@{R=NCM.Strings;K=STATUS_UP;E=js}';
                case "2": return '@{R=NCM.Strings;K=STATUS_DOWN;E=js}';
                case "3": return '@{R=NCM.Strings;K=STATUS_WARNING;E=js}';
                case "9": return '@{R=NCM.Strings;K=STATUS_UNMANAGED;E=js}';
                case "11": return '@{R=NCM.Strings;K=STATUS_EXTERNAL;E=js}';
                case "12": return '@{R=NCM.Strings;K=STATUS_UNREACHABLE;E=js}';
                default: return '@{R=NCM.Strings;K=STATUS_UNKNOWN;E=js}';
            }
        }

        if (group == "NodeStatus") {
            var chartId = Ext.getUrlParam('ChartID');
            switch (chartId) {
                case "1": // config changes snapshot chart
                    switch (value.toLowerCase()) {
                        case "conflict":
                            return '@{R=NCM.Strings;K=WEBJS_VK_154;E=js}';
                        case "noconflict":
                            return '@{R=NCM.Strings;K=WEBJS_VK_153;E=js}';
                        case "unknown":
                            return '@{R=NCM.Strings;K=WEBJS_VK_150;E=js}';
                    }
                    break;
                case "2": // baseline vs running chart
                case "3": // running vs startup chart
                    switch (value.toLowerCase()) {
                        case "conflict":
                            return '@{R=NCM.Strings;K=WEBJS_VK_152;E=js}';
                        case "noconflict":
                            return '@{R=NCM.Strings;K=WEBJS_VK_151;E=js}';
                        case "unknown":
                            return '@{R=NCM.Strings;K=WEBJS_VK_150;E=js}';
                    }
                    break;
                case "4": // backed up vs not backed up chart
                    switch (value.toLowerCase()) {
                        case "conflict":
                            return '@{R=NCM.Strings;K=WEBJS_VK_148;E=js}';
                        case "noconflict":
                            return '@{R=NCM.Strings;K=WEBJS_VK_149;E=js}';
                    }
                    break;
                case "5": // inventoried vs not inventoried chart
                    switch (value.toLowerCase()) {
                        case "conflict":
                            return '@{R=NCM.Strings;K=WEBJS_VK_146;E=js}';
                        case "noconflict":
                            return '@{R=NCM.Strings;K=WEBJS_VK_147;E=js}';
                    }
                    break;
            }
        }

        return value;
    }

    var currentSearchTerm = null;
    var refreshObjects = function (property, value, search, callback) {
        grid.store.removeAll();
        var pagingToolbar = grid.getBottomToolbar();
        pagingToolbar.cursor = 0;
        grid.store.baseParams['limit'] = grid.getBottomToolbar().pageSize;
        grid.store.proxy.conn.jsonData = { property: property, value: value, search: search };
        currentSearchTerm = search;
        pagingToolbar.doLoad(pagingToolbar.cursor);
    };

    var callWebMethod = function (url, data, success, failure) {
        $.ajax({
            type: "POST",
            url: url,
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            async: true,
            success: function (result) {
                if (success)
                    success(result);
            },
            error: function (response) {
                if (failure)
                    failure(response);
                else {
                    alert(response);
                }
            }
        });
    }

    var updateDownloadButtonMenu = function (menu, configTypes) {
        menu.removeAll();
        menu.add(new Ext.menu.TextItem({ text: '@{R=NCM.Strings;K=WEBJS_VK_265;E=js}', cls: 'menu-item-text' }));
        var haveCustomConfigs = false;
        Ext.iterate(configTypes, function (configType) {
            if (!configType.IsCustom) {
                menu.add({ iconCls: 'menu-item-no-icon', text: configType.Name, handler: function () { downloadConfigs(configType.Name); } });
            } else {
                haveCustomConfigs = true;
            }
        });
        if (haveCustomConfigs && configTypes.length > 0) {
            menu.add(new Ext.menu.Separator());
            menu.add(new Ext.menu.TextItem({ text: '@{R=NCM.Strings;K=WEBJS_VK_266;E=js}', cls: 'menu-item-text' }));
            Ext.iterate(configTypes, function (configType) {
                if (configType.IsCustom) {
                    menu.add({ iconCls: 'menu-item-no-icon', text: configType.Name, handler: function () { downloadConfigs(configType.Name); } });
                }
            });
        }
    }

    var updateToolbarButtons = function () {
        var selCount = grid.getSelectionModel().getCount();
        var map = grid.getTopToolbar().items.map;

        var needsAtLeastOneSelected = (selCount === 0);
        map.EditProperties.setDisabled(SW.NCM.IsDemoMode ? true : needsAtLeastOneSelected);
        map.Download.setDisabled(SW.NCM.IsDemoMode ? true : needsAtLeastOneSelected);
        map.UpdateInventory.setDisabled(SW.NCM.IsDemoMode ? true : needsAtLeastOneSelected);

        var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');
        if (grid.getStore().getCount() > 0 && selCount == grid.getStore().getCount()) {
            hd.addClass('x-grid3-hd-checker-on');
        } else {
            hd.removeClass('x-grid3-hd-checker-on');
        }
    };

    var EditNodesPropertiesHandler = function (items) {
        var ids = getSelectedNodesToEdit(items);
        window.location = '/Orion/Nodes/NodeProperties.aspx?Nodes=' + ids.join(',') + SW.NCM.ReturnUrl;
    }

    var UpdateInventory = function (items) {
        var ncmNodeIds = getSelectedNcmNodesIds(items);
        Ext.Msg.confirm('@{R=NCM.Strings;K=WEBJS_VK_80;E=js}', String.format('@{R=NCM.Strings;K=WEBJS_VK_325;E=js}', ncmNodeIds.length),
            function (btn, text) {
                if (btn == 'yes') {
                    internalUpdateInventory(ncmNodeIds);
                }
            });

    }

    var internalUpdateInventory = function (ncmNodeIds) {
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_323;E=js}');
        ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
            "UpdateInventory", { nodeIds: ncmNodeIds },
            function (result) {
                waitMsg.hide();
                errorHandler('@{R=NCM.Strings;K=WEBJS_VK_324;E=js}', result);
                if (result == null) {
                    window.location = "/Orion/NCM/Resources/InventoryReports/InventoryStatus.aspx";
                }
            }
        );
    }

    var downloadConfigs = function (configType) {
        var items = grid.getSelectionModel().getSelections();
        var nodeIds = getSelectedNcmNodesIds(items);
        internalDownloadConfigs(nodeIds, configType);
    }

    var internalDownloadConfigs = function (ncmNodeIds, configType) {
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_240;E=js}');
        ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
            "DownloadConfigs", { nodeIds: ncmNodeIds, configType: configType },
            function (result) {
                waitMsg.hide();
                errorHandler('@{R=NCM.Strings;K=WEBJS_VK_241;E=js}', result);
                if (result == null) {
                    window.location = "/Orion/NCM/TransferStatus.aspx";
                }
            }
        );
    }

    var errorHandler = function (source, result, func) {
        if (result != null && result.Error) {
            Ext.Msg.show({
                title: source,
                msg: result.Msg,
                minWidth: 500,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR,
                fn: func
            });
        }
    }
    function gridReload() {
        var groupBy = combo.getValue();
        if (groupBy == "") {
            refreshObjects("", "", "");
        }
        else {
            loadGroupByValues(groupBy, '');
        }
    }

    function getSelectedNodesToEdit(items) {
        var ids = [];
        Ext.each(items, function (item) {
            ids.push(item.data.CoreNodeID);
        });

        return ids;
    }

    function getSelectedNcmNodesIds(items) {
        var ids = [];
        Ext.each(items, function (item) {
            ids.push(item.data.NodeID);
        });

        return ids;
    }

    var groupByControlInitializer = function () {
        combo = new Ext.form.ComboBox({
            width: 200,
            store: SW.NCM.GroupByData,
            value: '',
            listWidth: 200,
            triggerAction: 'all',
            editable: false,
            forceSelection: true,
            renderTo: 'GroupBy'
        });

        combo.on('select', function (combo, record) {
            if (record.data.field1 == '') {
                var groupItems = $(".GroupItems").text('@{R=NCM.Strings;K=WEBJS_VK_04;E=js}');
                refreshObjects("", "", "");
                groupItems.empty();
            }
            else {
                loadGroupByValues(record.data.field1, '');
            }
        });

    }

    function getGroupByParams() {
        var obj = {};
        obj.groupBy = Ext.getUrlParam('groupBy');
        obj.groupByValue = Ext.getUrlParam('groupByValue');

        if (obj.groupBy == undefined)
            obj.groupBy = 'NodeStatus';

        if (obj.groupByValue == undefined)
            obj.groupByValue = '';

        return obj;
    }

    function renderNodeStatus(value, meta, record) {
        var nodeStatus = Ext.util.Format.htmlEncode(value)
        var chartId = Ext.getUrlParam('ChartID');
        switch (chartId) {
            case "1": // config changes snapshot chart
                switch (nodeStatus.toLowerCase()) {
                    case "conflict":
                        return '@{R=NCM.Strings;K=WEBJS_VK_154;E=js}';
                    case "noconflict":
                        return '@{R=NCM.Strings;K=WEBJS_VK_153;E=js}';
                    case "unknown":
                        return '@{R=NCM.Strings;K=WEBJS_VK_150;E=js}';
                }
                break;
            case "2": // baseline vs running chart
            case "3": // running vs startup chart
                switch (nodeStatus.toLowerCase()) {
                    case "conflict":
                        return '@{R=NCM.Strings;K=WEBJS_VK_152;E=js}';
                    case "noconflict":
                        return '@{R=NCM.Strings;K=WEBJS_VK_151;E=js}';
                    case "unknown":
                        return '@{R=NCM.Strings;K=WEBJS_VK_150;E=js}';
                }
                break;
            case "4": // backed up vs not backed up chart
                switch (nodeStatus.toLowerCase()) {
                    case "conflict":
                        return '@{R=NCM.Strings;K=WEBJS_VK_148;E=js}';
                    case "noconflict":
                        return '@{R=NCM.Strings;K=WEBJS_VK_149;E=js}';
                }
                break;
            case "5": // inventoried vs not inventoried chart
                switch (nodeStatus.toLowerCase()) {
                    case "conflict":
                        return '@{R=NCM.Strings;K=WEBJS_VK_146;E=js}';
                    case "noconflict":
                        return '@{R=NCM.Strings;K=WEBJS_VK_147;E=js}';
                }
                break;
        }

        return Ext.util.Format.htmlEncode(value);
    }

    function renderIPAddress(value, meta, record) {
        return record.data.AgentIP;
    }

    function UnknownVendor(img) {
        img.src = '/NetPerfMon/images/Vendors/Unknown.gif';
    }

    function renderNodeName(value, meta, record) {
        return String.format('<img src="/NetPerfMon/images/Vendors/{0}" onerror="UnknownVendor(this);" />&nbsp;<a href="/Orion/NCM/NodeDetails.aspx?NodeID={1}">{2}</a>', record.data.VendorIcon, record.data.NodeID, Ext.util.Format.htmlEncode(value));
    }

    var doClean = function () {
        var map = grid.getTopToolbar().items.map;

        map.Clean.setVisible(false);
        map.txtSearch.setValue('');

        gridReload();
    }

    var doSearch = function () {
        var map = grid.getTopToolbar().items.map;
        var searchCondition = map.txtSearch.getValue();

        if ($.trim(searchCondition).length == 0) return;

        $(".group-core-node-item").removeClass("SelectedGroupItem");

        refreshObjects("", "", searchCondition);
        map.Clean.setVisible(true);
    }

    var grid;
    var combo;

    return {
        init: function () {
            if ($("[id$='_GridContainer']").length == 0)
                return false;

            var dataStore = new ORION.WebServiceStore(
                "/Orion/NCM/Services/ChartDrilldown.asmx/GetGridPaged",
                [
                    { name: 'NodeID', mapping: 0 },
                    { name: 'CoreNodeID', mapping: 1 },
                    { name: 'VendorIcon', mapping: 2 },
                    { name: 'NodeCaption', mapping: 3 },
                    { name: 'AgentIP', mapping: 4 },
                    { name: 'IPAddressType', mapping: 5 },
                    { name: 'NodeStatus', mapping: 7 }
                ],
                "NodeCaption");

            var comboPS = new Ext.form.ComboBox({
                name: 'perpage',
                width: 50,
                store: new Ext.data.SimpleStore({
                    fields: ['id'],
                    data: [
                        ['25'],
                        ['50'],
                        ['75'],
                        ['100']
                    ]
                }),
                mode: 'local',
                value: '25',
                listWidth: 50,
                triggerAction: 'all',
                displayField: 'id',
                valueField: 'id',
                editable: false,
                forceSelection: true
            });

            var pagingToolbar = new Ext.PagingToolbar({
                store: dataStore,
                pageSize: 25,
                displayInfo: true,
                displayMsg: '@{R=NCM.Strings;K=WEBJS_VK_41;E=js}',
                emptyMsg: '@{R=NCM.Strings;K=WEBJS_VK_42;E=js}',
                items: ['-', '@{R=NCM.Strings;K=WEBJS_VK_12;E=js}', comboPS]
            });

            comboPS.on('select', function (combo, record) {
                var psize = parseInt(record.get('id'), 10);
                grid.store.baseParams['limit'] = psize;
                pagingToolbar.pageSize = psize;
                pagingToolbar.cursor = 0;
                pagingToolbar.doLoad(pagingToolbar.cursor);
            }, this);

            var selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", updateToolbarButtons);

            grid = new Ext.grid.GridPanel({
                id: 'gridControl',
                store: dataStore,
                columns: [selectorModel,
                    {
                        header: 'NodeID',
                        width: 20,
                        hidden: true,
                        hideable: false,
                        sortable: true,
                        dataIndex: 'NodeID'
                    }, {
                        header: 'CoreNodeID',
                        width: 20,
                        hidden: true,
                        hideable: false,
                        sortable: true,
                        dataIndex: 'CoreNodeID'
                    }, {
                        header: '@{R=NCM.Strings;K=WEBJS_VK_43;E=js}',
                        width: 350,
                        sortable: true,
                        dataIndex: 'NodeCaption',
                        renderer: renderNodeName
                    }, {
                        header: '@{R=NCM.Strings;K=WEBJS_VK_44;E=js}',
                        width: 350,
                        sortable: true,
                        dataIndex: 'AgentIP',
                        renderer: renderIPAddress
                    }, {
                        header: '@{R=NCM.Strings;K=WEBJS_VK_145;E=js}',
                        width: 250,
                        sortable: true,
                        dataIndex: 'NodeStatus',
                        renderer: renderNodeStatus
                    }],

                sm: selectorModel,
                layout: 'fit',
                autoScroll: 'true',
                loadMask: true,
                width: 750,
                height: 500,
                stripeRows: true,
                tbar: [{
                    id: 'Download',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_263;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_264;E=js}',
                    icon: '/Orion/NCM/Resources/images/Events/EventConfigDownload.gif',
                    cls: 'x-btn-text-icon',
                    hidden: (SW.NCM.NCMAccountRole == 'WebViewer') && !SW.NCM.IsDemoServer,
                    listeners: {
                        beforerender: function (that) {
                            updateDownloadButtonMenu(that.menu, SW.NCM.ConfigTypesStore);
                        }
                    },
                    menu: {
                        showSeparator: false,
                        cls: 'menu-download',
                        listeners: {
                            show: function (that) {
                                var items = grid.getSelectionModel().getSelections();
                                var nodeIds = getSelectedNcmNodesIds(items);

                                callWebMethod('/Orion/NCM/Services/ConfigManagement.asmx/GetConfigTypes',
                                    { nodeIds: nodeIds },
                                    function (result) {
                                        var configTypes = result.d;
                                        updateDownloadButtonMenu(that, configTypes);
                                    });
                            }
                        }
                    }
                }, SW.NCM.NCMAccountRole == 'WebViewer' && !SW.NCM.IsDemoServer ? '' : '-', {
                    id: 'UpdateInventory',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_80;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_81;E=js}',
                    icon: '/Orion/NCM/Resources/images/InventoryIcons/icon_update_inventory.gif',
                    cls: 'x-btn-text-icon',
                    hidden: SW.NCM.NCMAccountRole == 'WebViewer' && !SW.NCM.IsDemoServer,
                    handler: function () { UpdateInventory(grid.getSelectionModel().getSelections()); }
                }, '-', {
                    id: 'EditProperties',
                    text: '@{R=NCM.Strings;K=WEBJS_VM_54;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VM_54;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_editsnippet.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        EditNodesPropertiesHandler(grid.getSelectionModel().getSelections());
                    }
                }, '->', {
                    id: 'txtSearch',
                    xtype: 'textfield',
                    emptyText: '@{R=NCM.Strings;K=WEBJS_VK_144;E=js}',
                    width: 200,
                    enableKeyEvents: true,
                    listeners: {
                        keypress: function (obj, evnt) {
                            if (evnt.keyCode == 13) {
                                doSearch();
                            }
                        }
                    }
                }, {
                    id: 'Clean',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_30;E=js}',
                    iconCls: 'ncm_clear-btn',
                    cls: 'x-btn-icon',
                    hidden: true,
                    handler: function () { doClean(); }
                }, {
                    id: 'Search',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_31;E=js}',
                    iconCls: 'ncm_search-btn',
                    cls: 'x-btn-icon',
                    handler: function () { doSearch(); }
                }],

                bbar: pagingToolbar

            });

            grid.render('Grid');



            updateToolbarButtons();
            groupByControlInitializer();

            var obj = getGroupByParams();
            combo.setValue(obj.groupBy);

            loadGroupByValues(obj.groupBy, obj.groupByValue);

            if (obj.groupBy == '')
                refreshObjects(obj.groupBy, obj.groupByValue);

            gridResize();
            $(window).resize(function () {
                gridResize();
            });

            grid.getView().on("refresh", function () {
                var search = currentSearchTerm;
                if ($.trim(search).length == 0) return;

                var columnsToHighlight = SW.NCM.ColumnIndexByName(grid.getColumnModel(), ['@{R=NCM.Strings;K=WEBJS_VK_43;E=js}', '@{R=NCM.Strings;K=WEBJS_VK_44;E=js}']);
                Ext.each(columnsToHighlight, function (columnNumber) {
                    SW.NCM.highlightSearchText("#Grid .x-grid3-body .x-grid3-td-" + columnNumber, search);
                });
            });

        }
    }
}();

Ext.onReady(SW.NCM.ChartDrilldown.init, SW.NCM.ChartDrilldown);

