<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NavigationTabBar.ascx.cs" Inherits="Orion_NCM_Controls_NavigationTabBar" %>

<%   
    this.AddTab(Resources.NCMWebContent.WEBDATA_VK_719, VirtualPathUtility.ToAbsolute(@"~/Orion/NCM/ConfigurationManagement.aspx"), IsPageRegexMatch(@"(^|/)(ConfigurationManagement.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(ConfigurationManagement)"));
    if (ShowTransferStatusTab) this.AddTab(Resources.NCMWebContent.WEBDATA_IA_15, VirtualPathUtility.ToAbsolute(@"~/Orion/NCM/TransferStatus.aspx"), IsPageRegexMatch(@"(^|/)(TransferStatus.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(ConfigurationManagement)"));
    if (ShowInventoryStatusTab) this.AddTab(Resources.NCMWebContent.WEBDATA_VK_111, VirtualPathUtility.ToAbsolute(@"~/Orion/NCM/Resources/InventoryReports/InventoryStatus.aspx"), IsPageRegexMatch(@"(^|/)(InventoryStatus.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(ConfigurationManagement)"));
    if (ShowScriptManagementTab) this.AddTab(Resources.NCMWebContent.WEBDATA_IA_22, VirtualPathUtility.ToAbsolute(@"~/Orion/NCM/SnippetManagement.aspx"), IsPageRegexMatch(@"(^|/)(SnippetManagement.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(ConfigurationManagement)"));
    this.AddTab(Resources.NCMWebContent.NavigationTab_BaselineManagement, VirtualPathUtility.ToAbsolute(@"~/ui/ncm/baselineManagement"), IsPageRegexMatch(@"(^|/)(baselineManagement)$"), true, !IsPageRegexMatch(@"(^|/)(ConfigurationManagement)"));

    this.AddTab(Resources.NCMWebContent.WEBDATA_VK_719, VirtualPathUtility.ToAbsolute(@"~/Orion/NCM/ConfigurationManagement.aspx"), IsPageRegexMatch(@"(^|/)(ConfigurationManagement.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(TransferStatus)"));
    if (ShowTransferStatusTab) this.AddTab(Resources.NCMWebContent.WEBDATA_IA_15, VirtualPathUtility.ToAbsolute(@"~/Orion/NCM/TransferStatus.aspx"), IsPageRegexMatch(@"(^|/)(TransferStatus.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(TransferStatus)"));
    if (ShowInventoryStatusTab) this.AddTab(Resources.NCMWebContent.WEBDATA_VK_111, VirtualPathUtility.ToAbsolute(@"~/Orion/NCM/Resources/InventoryReports/InventoryStatus.aspx"), IsPageRegexMatch(@"(^|/)(InventoryStatus.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(TransferStatus)"));
    if (ShowScriptManagementTab) this.AddTab(Resources.NCMWebContent.WEBDATA_IA_22, VirtualPathUtility.ToAbsolute(@"~/Orion/NCM/SnippetManagement.aspx"), IsPageRegexMatch(@"(^|/)(SnippetManagement.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(TransferStatus)"));
    this.AddTab(Resources.NCMWebContent.NavigationTab_BaselineManagement, VirtualPathUtility.ToAbsolute(@"~/ui/ncm/baselineManagement"), IsPageRegexMatch(@"(^|/)(baselineManagement)$"), true, !IsPageRegexMatch(@"(^|/)(TransferStatus)"));

    if (ShowScriptManagementTab)
    {
        this.AddTab(Resources.NCMWebContent.WEBDATA_VK_719, VirtualPathUtility.ToAbsolute(@"~/Orion/NCM/ConfigurationManagement.aspx"), IsPageRegexMatch(@"(^|/)(ConfigurationManagement.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(SnippetManagement)"));
        this.AddTab(Resources.NCMWebContent.WEBDATA_IA_15, VirtualPathUtility.ToAbsolute(@"~/Orion/NCM/TransferStatus.aspx"), IsPageRegexMatch(@"(^|/)(TransferStatus.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(SnippetManagement)"));
        this.AddTab(Resources.NCMWebContent.WEBDATA_VK_111, VirtualPathUtility.ToAbsolute(@"~/Orion/NCM/Resources/InventoryReports/InventoryStatus.aspx"), IsPageRegexMatch(@"(^|/)(InventoryStatus.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(SnippetManagement)"));
        this.AddTab(Resources.NCMWebContent.WEBDATA_IA_22, VirtualPathUtility.ToAbsolute(@"~/Orion/NCM/SnippetManagement.aspx"), IsPageRegexMatch(@"(^|/)(SnippetManagement.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(SnippetManagement)"));
        this.AddTab(Resources.NCMWebContent.NavigationTab_BaselineManagement, VirtualPathUtility.ToAbsolute(@"~/ui/ncm/baselineManagement"), IsPageRegexMatch(@"(^|/)(baselineManagement)$"), true, !IsPageRegexMatch(@"(^|/)(SnippetManagement)"));
    }

    if (ShowInventoryStatusTab)
    {
        this.AddTab(Resources.NCMWebContent.WEBDATA_VK_719, VirtualPathUtility.ToAbsolute(@"~/Orion/NCM/ConfigurationManagement.aspx"), IsPageRegexMatch(@"(^|/)(ConfigurationManagement.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(InventoryStatus)"));
        this.AddTab(Resources.NCMWebContent.WEBDATA_IA_15, VirtualPathUtility.ToAbsolute(@"~/Orion/NCM/TransferStatus.aspx"), IsPageRegexMatch(@"(^|/)(TransferStatus.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(InventoryStatus)"));
        this.AddTab(Resources.NCMWebContent.WEBDATA_VK_111, VirtualPathUtility.ToAbsolute(@"~/Orion/NCM/Resources/InventoryReports/InventoryStatus.aspx"), IsPageRegexMatch(@"(^|/)(InventoryStatus.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(InventoryStatus)"));
        if (ShowScriptManagementTab) this.AddTab(Resources.NCMWebContent.WEBDATA_IA_22, VirtualPathUtility.ToAbsolute(@"~/Orion/NCM/SnippetManagement.aspx"), IsPageRegexMatch(@"(^|/)(SnippetManagement.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(InventoryStatus)"));
        this.AddTab(Resources.NCMWebContent.NavigationTab_BaselineManagement, VirtualPathUtility.ToAbsolute(@"~/ui/ncm/baselineManagement"), IsPageRegexMatch(@"(^|/)(baselineManagement)$"), true, !IsPageRegexMatch(@"(^|/)(InventoryStatus)"));
    }

    this.AddTab(Resources.NCMWebContent.WEBDATA_VK_104, VirtualPathUtility.ToAbsolute(@"~/Orion/NCM/Resources/ConfigSnippets/ConfigSnippets.aspx"), IsPageRegexMatch(@"(^|/)(ConfigSnippets.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(Resources/ConfigSnippets)"));
    this.AddTab(Resources.NCMWebContent.WEBDATA_VK_105, VirtualPathUtility.ToAbsolute(@"~/Orion/NCM/Resources/ConfigSnippets/Thwack/Default.aspx"), IsPageRegexMatch(@"(^|/)(Thwack/Default.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(Resources/ConfigSnippets)"));

    this.AddTab(Resources.NCMWebContent.WEBDATA_IA_43, VirtualPathUtility.ToAbsolute(@"~/Orion/CLI/Admin/DeviceTemplate/DeviceTemplatesManagement.aspx"), IsPageRegexMatch(@"(^|/)(DeviceTemplatesManagement.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(DeviceTemplatesManagement)"));
    this.AddTab(Resources.NCMWebContent.WEBDATA_IA_57, VirtualPathUtility.ToAbsolute(@"~/Orion/NCM/Admin/DeviceTemplate/Thwack/ThwackDeviceTemplates.aspx"), IsPageRegexMatch(@"(^|/)(Thwack/ThwackDeviceTemplates.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(DeviceTemplatesManagement)"));

    this.AddTab(Resources.NCMWebContent.WEBDATA_IA_43, VirtualPathUtility.ToAbsolute(@"~/Orion/CLI/Admin/DeviceTemplate/DeviceTemplatesManagement.aspx"), IsPageRegexMatch(@"(^|/)(DeviceTemplatesManagement.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(ThwackDeviceTemplates)"));
    this.AddTab(Resources.NCMWebContent.WEBDATA_IA_57, VirtualPathUtility.ToAbsolute(@"~/Orion/NCM/Admin/DeviceTemplate/Thwack/ThwackDeviceTemplates.aspx"), IsPageRegexMatch(@"(^|/)(Thwack/ThwackDeviceTemplates.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(ThwackDeviceTemplates)"));

    this.AddTab(Resources.NCMWebContent.WEBDATA_VK_106, VirtualPathUtility.ToAbsolute(@"~/Orion/NCM/Admin/PolicyReports/PolicyReportManagement.aspx"), IsPageRegexMatch(@"(^|/)(PolicyReportManagement.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(Admin/PolicyReports)"));
    this.AddTab(Resources.NCMWebContent.WEBDATA_VK_107, VirtualPathUtility.ToAbsolute(@"~/Orion/NCM/Admin/PolicyReports/PolicyManagement.aspx"), IsPageRegexMatch(@"(^|/)(PolicyManagement.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(Admin/PolicyReports)"));
    this.AddTab(Resources.NCMWebContent.WEBDATA_VK_108, VirtualPathUtility.ToAbsolute(@"~/Orion/NCM/Admin/PolicyReports/PolicyRuleManagement.aspx"), IsPageRegexMatch(@"(^|/)(PolicyRuleManagement.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(Admin/PolicyReports)"));
    this.AddTab(Resources.NCMWebContent.WEBDATA_VK_109, VirtualPathUtility.ToAbsolute(@"~/Orion/NCM/Admin/PolicyReports/Thwack/Default.aspx"), IsPageRegexMatch(@"(^|/)(Thwack/Default.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(Admin/PolicyReports)"));

    this.AddTab(Resources.NCMWebContent.FirmwareDefinition_PageTitle, VirtualPathUtility.ToAbsolute(@"~/Orion/NCM/Admin/Firmware/FirmwareDefinitions.aspx"), IsPageRegexMatch(@"(^|/)(FirmwareDefinitions.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(NCM/Admin/Firmware)"));
    this.AddTab(Resources.NCMWebContent.SharedFirmwareDefinitions_PageTitle, VirtualPathUtility.ToAbsolute(@"~/Orion/NCM/Admin/Firmware/Thwack/ThwackFirmwareDefinitions.aspx"), IsPageRegexMatch(@"(^|/)(ThwackFirmwareDefinitions.aspx)$"), true, !IsPageRegexMatch(@"(^|/)(NCM/Admin/Firmware)"));    

    %>

<style type="text/css">
.sw-tabs .x-tab-strip-text { font-size: 13px !important; }
.tab-top 
{
    border: solid 1px #DFDFDE;
    border-top:medium none !important;
    padding:1em;
    background:white url('/Orion/NCM/Resources/images/tab.top.gif') repeat-x scroll left top !important;
}
</style>

<script type="text/javascript">
    $(document).ready(function () {
        $(".sw-tabs li.sw-tab").bind('mouseover', function () { $(this).addClass('x-tab-strip-over'); });
        $(".sw-tabs li.sw-tab").bind('mouseout', function () { $(this).removeClass('x-tab-strip-over'); });
    });
</script>

<table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <div class="sw-tabs">
                <div class="x-tab-panel-header x-unselectable x-tab-panel-header-plain" style="width: 100%;">
                    <div class="x-tab-strip-wrap">

                        <ul class="x-tab-strip x-tab-strip-top" style="width:100%;">

                            <asp:Literal ID="TabPH" EnableViewState="False" runat="server" />

                            <li class="x-tab-edge"></li>
                            <div class="x-clear"><!-- --></div>
                        </ul>
                    </div>

                </div>
            </div>
        </td>
        <td align="right" style="border-bottom: 1px solid #D0D0D0;">
            <asp:PlaceHolder ID="phSearch" runat="server"></asp:PlaceHolder>            
        </td>
    </tr>
</table>


