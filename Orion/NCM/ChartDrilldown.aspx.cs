using System;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Web.UI;
using Newtonsoft.Json;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.NCM.Common.Dal.ConfigTypes;
using SolarWinds.NCM.Contracts.ConfigTypes;
using SolarWinds.NCM.Web.Contracts.SiteMap;
using SolarWinds.Orion.Web;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;

public partial class Orion_NCM_Resources_ChartDrilldown : Page
{
    private readonly IConfigTypesProvider configTypesProvider;
    private readonly Func<INcmSiteMapRenderer> rendererFactory;
    private readonly ILogger logger;

    public Orion_NCM_Resources_ChartDrilldown()
    {
        configTypesProvider = ServiceLocator.Container.Resolve<IConfigTypesProvider>();
        rendererFactory = ServiceLocator.Container.Resolve<Func<INcmSiteMapRenderer>>();
        logger = ServiceLocator.Container.Resolve<Func<Type, ILogger>>()(GetType());
    }

    protected string ReturnUrl =>
        $@"&GuidID={Guid.NewGuid().ToString()}&ReturnTo={ReferrerRedirectorBase.GetReturnUrl()}";

    protected string GetConfigTypesStore()
    {
        try
        {
            var configTypes = configTypesProvider.GetAll().ToArray();
            return JsonConvert.SerializeObject(configTypes);
        }
        catch
        {
            return JsonConvert.SerializeObject(new ConfigType[0]);
        }
    }

    protected string CreateGroupByData()
    {
        string res = @"[['', '" + Resources.NCMWebContent.WEBDATA_VM_121 + @"'],"
                     + @"['NodeStatus', '" + Resources.NCMWebContent.WEBDATA_VK_715 + @"'],"
                     + @"['Vendor', '" + Resources.NCMWebContent.WEBDATA_VK_278 + @"'],"
                     + @"['MachineType', '" + Resources.NCMWebContent.WEBDATA_VK_279 + @"'],"
                     + @"['Status','" + Resources.NCMWebContent.WEBDATA_VM_123 + @"'],"
                     + @"['IPAddressType','" + Resources.NCMWebContent.WEBDATA_VM_124 + @"'],"
                     + @"['Location','" + Resources.NCMWebContent.WEBDATA_VM_125 + @"'],"
                     + @"['SNMPVersion','" + Resources.NCMWebContent.WEBDATA_VM_126 + @"'],"
                     + @"['Community','" + Resources.NCMWebContent.WEBDATA_VM_127 + @"'],"
                     + @"['RWCommunity','" + Resources.NCMWebContent.WEBDATA_VM_128 + @"']"
                     + @"{0}"
                     + @"]";

        StringBuilder cpData = new StringBuilder();
        string[] cps = GetNCMCustomProperties();
        foreach (string prop in cps)
        {
            cpData.AppendFormat(@",['{0}','{0}']", prop);
        }

        return string.Format(res, cpData);
    }

    protected string SubTitle { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        var resource = ResourceManager.GetResourceByID(Convert.ToInt32(Request.QueryString[@"ResourceID"]));
        Page.Title = !string.IsNullOrEmpty(resource.Properties[@"Title"])
            ? resource.Properties[@"Title"]
            : resource.Title;

        string timeRange = string.IsNullOrEmpty(resource.Properties[@"TimeRange"])
            ? @"As of Last Update"
            : resource.Properties[@"TimeRange"];
        string dateFrom = resource.Properties[@"DateFrom"];
        string dateTo = resource.Properties[@"DateTo"];

        if (!timeRange.Equals(@"Custom", StringComparison.InvariantCultureIgnoreCase))
        {
            DateTime dateTimeTo = DateTime.Now;
            string strDateTimeTo = string.Empty;
            if (timeRange.Equals(@"Last Month", StringComparison.InvariantCultureIgnoreCase))
            {
                dateTimeTo = new DateTime(dateTimeTo.Year, dateTimeTo.Month,
                    dateTimeTo.AddDays(-dateTimeTo.Day + 1).Day).AddMilliseconds(-1);
                strDateTimeTo = dateTimeTo.ToString(CultureInfo.InvariantCulture);
            }

            dateFrom = TimeParser.GetTime(timeRange).ToString(CultureInfo.InvariantCulture);
            dateTo = strDateTimeTo;
        }

        if (string.IsNullOrEmpty(dateTo))
        {
            SubTitle = timeRange.Equals(@"As of Last Update", StringComparison.InvariantCultureIgnoreCase)
                ? Resources.NCMWebContent.WEBDATA_VK_184
                : string.Format(Resources.NCMWebContent.WEBDATA_VK_185,
                    Convert.ToDateTime(dateFrom, CultureInfo.InvariantCulture).ToString(CultureInfo.InvariantCulture));
        }
        else
        {
            SubTitle = string.Format(Resources.NCMWebContent.WEBDATA_VK_186,
                Convert.ToDateTime(dateFrom, CultureInfo.InvariantCulture).ToString(CultureInfo.InvariantCulture),
                Convert.ToDateTime(dateTo, CultureInfo.InvariantCulture).ToString(CultureInfo.InvariantCulture));
        }
    }

    private string[] GetNCMCustomProperties()
    {
        return CustomPropertyHelper.GetCustomProperties().ToArray();
    }

    #region SiteMap

    protected void SiteMapPath_OnInit(object sender, EventArgs e)
    {
        try
        {
            var renderer = rendererFactory();
            NodeSiteMapPath.SetUpRenderer(renderer);
        }
        catch (Exception ex)
        {
            logger.Error("Error initializing site map.", ex);
            throw;
        }
    }

    #endregion
}
