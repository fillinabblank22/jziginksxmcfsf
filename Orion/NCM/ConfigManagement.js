﻿Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.QuickTips.init();

SW.NCM.ConfigManagement = function () {

    ORION.prefix = "NCM_ConfigManagement_";

    var config;
    var showSelectedOnly = false;
    var selectionCount = 0;

    var searchTerm = '';
    var allGroupsToSearch = true;
    var colToDisplay = '@{R=NCM.Strings;K=WEBJS_VK_43;E=js}';
    var colToSearch = 'Nodes.Caption';
    var colToHighlight = 'Caption;IPAddress';

    var searchBox;

    var groupingStore;
    var tree;
    var combo1;
    var combo2;
    var combo3;
    var lastSelectedNode;

    var toolbar;
    var grid;
    var blockRowSelectionEvent;
    var gridStore;
    var expandedRows = new Array();

    var pageSize;
    var topPageSizeBox;
    var bottomPageSizeBox;
    var activeBaselinesStatusTooltip;

    var comboSelect = function(combo1, combo2, combo3) {
        var value1 = combo1.getValue();
        var value2 = combo2.getValue();
        var value3 = combo3.getValue();

        if (value1 == '') {
            value2 = '';
            combo2.setValue(value2);
            value3 = '';
            combo3.setValue(value3);
        }

        if (value2 == '') {
            value3 = '';
            combo3.setValue(value3);
        }

        combo2.setVisible(value1 != '');
        combo3.setVisible(value2 != '');

        combo1.getStore().loadData(groupingStore);
        combo1.collapse();

        combo2.getStore().loadData(groupingStore);
        combo2.collapse();

        combo3.getStore().loadData(groupingStore);
        combo3.collapse();

        var index;
        index = combo2.getStore().find('field1', value1);
        if (index >= 0) {
            combo2.getStore().removeAt(index);
        }
        index = combo3.getStore().find('field1', value1);
        if (index >= 0) {
            combo3.getStore().removeAt(index);
        }

        index = combo1.getStore().find('field1', value2);
        if (index >= 0) {
            combo1.getStore().removeAt(index);
        }
        index = combo3.getStore().find('field1', value2);
        if (index >= 0) {
            combo3.getStore().removeAt(index);
        }

        index = combo1.getStore().find('field1', value3);
        if (index >= 0) {
            combo1.getStore().removeAt(index);
        }
        index = combo2.getStore().find('field1', value3);
        if (index >= 0) {
            combo2.getStore().removeAt(index);
        }
    };

    var getGrouping = function() {
        return String.format('{0}|{1}|{2}', combo1.getValue(), combo2.getValue(), combo3.getValue());
    };

    var saveGrouping = function() {
        ORION.Prefs.save('Grouping', getGrouping());
    };

    var findComboGroupValue = function(combo, value) {
        var store = combo.getStore();
        var index = store.findExact('field1', value);
        if (index == -1) {
            return '';
        } else {
            return value;
        }
    };

    var setNodeIcon = function(node, src, className) {
        if ((node.ui) && (node.ui.iconNode)) {
            node.ui.iconNode.src = src;
            node.ui.iconNode.className = className;
        }
    };

    var getGroupingQueryString = function(queryString, node) {
        if (node != null) {
            if (node.attributes.level != 0) {
                queryString += String.format("{0}={1}&", node.attributes.groupBy, node.attributes.groupByValue);
                return getGroupingQueryString(queryString, node.parentNode);
            }
        }
        return queryString;
    };

    var getGroupValueObject = function(groupValueObject, node) {
        if (node != null) {
            if (node.attributes.level != 0) {
                groupValueObject.push(node.attributes.group);
                getGroupValueObject(groupValueObject, node.parentNode);
            }
        }
    };

    var initGrouping = function() {
        groupingStore = SW.NCM.GroupingStore;

        combo1 = new Ext.form.ComboBox({
            typeAhead: true,
            renderTo: 'combo1',
            store: SW.NCM.GroupingStore,
            width: 300,
            forceSelection: true,
            mode: 'local',
            editable: false,
            triggerAction: 'all',
            listeners:
            {
                select: function(combo, record, index) {
                    comboSelect(combo1, combo2, combo3);
                    saveGrouping();
                    clearTree();
                    searchTerm = '';
                    loadTree();
                    treeResize();
                }
            }
        });

        combo2 = new Ext.form.ComboBox({
            typeAhead: true,
            renderTo: 'combo2',
            store: SW.NCM.GroupingStore,
            width: 300,
            forceSelection: true,
            mode: 'local',
            editable: false,
            triggerAction: 'all',
            listeners:
            {
                select: function(combo, record, index) {
                    comboSelect(combo1, combo2, combo3);
                    saveGrouping();
                    clearTree();
                    searchTerm = '';
                    loadTree();
                    treeResize();
                }
            }
        });

        combo3 = new Ext.form.ComboBox({
            typeAhead: true,
            renderTo: 'combo3',
            store: SW.NCM.GroupingStore,
            width: 300,
            forceSelection: true,
            mode: 'local',
            editable: false,
            triggerAction: 'all',
            listeners:
            {
                select: function(combo, record, index) {
                    comboSelect(combo1, combo2, combo3);
                    saveGrouping();
                    clearTree();
                    searchTerm = '';
                    loadTree();
                    treeResize();
                }
            }
        });

        var grouping = (ORION.Prefs.load("Grouping") || 'Vendor||').split('|');
        var value1 = findComboGroupValue(combo1, grouping[0] || '');
        var value2 = findComboGroupValue(combo2, grouping[1] || '');
        var value3 = findComboGroupValue(combo3, grouping[2] || '');
        combo1.setValue(value1);
        combo2.setValue(value2);
        combo3.setValue(value3);
        comboSelect(combo1, combo2, combo3);
    };

    var initTree = function() {
        var treeLoader = new Ext.tree.TreeLoader({
            dataUrl: '/Orion/NCM/ConfigManagementTreeProvider.ashx',
            listeners: {
                beforeload: function(treeLoader, node) {
                    this.baseParams.level = node.attributes.level;
                    this.baseParams.grouping = getGrouping();
                    this.baseParams.groupingQueryString = getGroupingQueryString('', node);
                    setNodeIcon(node, '/Orion/NCM/Resources/images/spinner.gif', '');
                },
                load: function(treeLoader, node, response) {
                    setNodeIcon(node, node.attributes.icon, node.attributes.iconCls);
                    if (searchTerm.length == 0) {
                        selectFirstChildOrRootNode();
                    }
                }
            }
        });

        var treeSelectionModel = new Ext.tree.DefaultSelectionModel({
            listeners: {
                beforeselect: function(selectionModel, newNode, oldNode) {
                    if ((oldNode) && (oldNode.ui)) {
                        oldNode.ui.removeClass('tree-node-selected');
                    }
                },
                selectionchange: function(selectionModel, node) {
                    if ((node) && (node.ui)) {
                        node.ui.addClass('tree-node-selected');
                        changeSelectionIfNeeded();
                        var groupingQueryString = getGroupingQueryString('', node);
                        loadGrid(groupingQueryString, '');
                        lastSelectedNode = node;
                    }
                }
            }
        });

        tree = new Ext.tree.TreePanel({
            id: 'treeControl',
            useArrows: true,
            renderTo: 'treePanel',
            selModel: treeSelectionModel,
            autoScroll: true,
            animate: true,
            enableDD: false,
            containerScroll: true,
            rootVisible: false,
            width: 320,
            border: true,
            loader: treeLoader,
            root: {
                text: '',
                level: 0,
                groupBy: 'Vendor',
                groupByValue: '',
                id: 'root'
            }
        });
    };

    var loadTree = function() {
        tree.getLoader().load(tree.getRootNode(), null, null);
    };

    var clearTree = function() {
        while (tree.root.firstChild) {
            tree.root.removeChild(tree.root.firstChild);
        }
        tree.getSelectionModel().clearSelections(true);
    };

    var clearTreeSelection = function() {
        var node = tree.getSelectionModel().getSelectedNode();
        if ((node) && (node.ui)) {
            node.ui.removeClass('tree-node-selected');
        }
        tree.getSelectionModel().clearSelections(true);
    };

    var selectFirstChildOrRootNode = function() {
        var selectionModel = tree.getSelectionModel();
        if (tree.root.firstChild) {
            if (tree.getRootNode().isSelected()) {
                selectionModel.select(tree.root.firstChild);
            } else {
                if (!selectionModel.getSelectedNode()) {
                    selectionModel.select(tree.root.firstChild);
                }
            }
        } else {
            selectionModel.select(tree.getRootNode());
        }
    };

    var selectLastSelectedNode = function(node) {
        var selectionModel = tree.getSelectionModel();
        if (!node) {
            node = tree.getRootNode();
            if (node.firstChild) {
                node = node.firstChild;
            }
        } else {
            if (node.isRoot) {
                if (node.firstChild) {
                    node = node.firstChild;
                }
            }
        }
        if (tree.getNodeById(node.id)) {
            selectionModel.unselect(node, true);
            selectionModel.select(node);
        }
    };

    var windowResize = function() {
        var w = Ext.getCmp('window');
        if (w != null && !w.maximized) {
            w.setSize(Ext.getBody().getViewSize().width * 0.7, Ext.getBody().getViewSize().height * 0.9);
            w.alignTo(document.body, "c-c");
        }
    };

    var showWindow = function(title, iframeContainerId, callback) {
        var window = new Ext.Window({
            id: 'window',
            layout: 'anchor',
            width: Ext.getBody().getViewSize().width * 0.7,
            minWidth: 800,
            height: Ext.getBody().getViewSize().height * 0.9,
            minHeight: 400,
            closeAction: 'close',
            title: title,
            modal: true,
            resizable: true,
            maximizable: true,
            plain: true,
            stateful: false,
            html: String.format(
                '<iframe name="{0}" id="{0}" frameborder="0" style="overflow:auto;" class="x-panel-body" width="100%" height="100%" />',
                iframeContainerId),
            buttons: [
                {
                    text: '@{R=NCM.Strings;K=WEBJS_VK_07;E=js}',
                    handler: function() {
                        window.close();
                    }
                }
            ],
            listeners: {
                restore: function(that) {
                    that.setSize(Ext.getBody().getViewSize().width * 0.7, Ext.getBody().getViewSize().height * 0.9);
                    that.alignTo(document.body, "c-c");
                },
                show: function(that) {
                    that.alignTo(document.body, "c-c");
                },
                close: function(that) {
                    if (that.reloadGridStore && that.reloadGridStore == 'yes') {
                        grid.store.reload();
                    }
                }
            }
        }).show(null, callback);
    };

    var manageDeviceTemplateClick = function(obj) {
        var ncmNodeID = obj.attr("value");
        showWindow('@{R=NCM.Strings;K=WEBJS_VK_330;E=js}',
            'iframeContainer',
            function() {
                window.frames.iframeContainer.location.href = String.format(
                    '/Orion/NCM/Admin/DeviceTemplate/DeviceTemplateLinkHandler.ashx?printable=true&NodeID={0}&ReturnUrl={1}',
                    ncmNodeID,
                    SW.NCM.CloseWindowHandlerUrl);

                $('#iframeContainer').load(function() {
                    $('#iframeContainer').contents().find("head").append($(
                        "<style type='text/css'> #DeviceTemplateContainer {width:98% !important; padding: 8px !important;} #content { padding-bottom: 10px !important;} </style>"));
                });
            });
    };

    var downloadConfigs = function(configType) {
        internalGetNCMNodeIdsForTransfer(function(results) {
            var ncmNodeIdsForTransfer = results.ncmNodeIdsForTransfer;
            var result = results.error;
            if (result != null && result.Error) {
                errorHandler('@{R=NCM.Strings;K=WEBJS_VK_239;E=js}',
                    result,
                    function() {
                        if (ncmNodeIdsForTransfer.length > 0) {
                            var msg = String.format('@{R=NCM.Strings;K=WEBJS_VK_309;E=js}',
                                ncmNodeIdsForTransfer.length);

                            Ext.Msg.confirm('@{R=NCM.Strings;K=WEBJS_VK_239;E=js}',
                                msg,
                                function(btn, text) {
                                    if (btn == 'yes') {
                                        internalDownloadConfigs(ncmNodeIdsForTransfer, configType);
                                    }
                                });
                        }
                    });
            } else {
                var msg = String.format('@{R=NCM.Strings;K=WEBJS_VK_309;E=js}', ncmNodeIdsForTransfer.length);

                Ext.Msg.confirm('@{R=NCM.Strings;K=WEBJS_VK_239;E=js}',
                    msg,
                    function(btn, text) {
                        if (btn == 'yes') {
                            internalDownloadConfigs(ncmNodeIdsForTransfer, configType);
                        }
                    });
            }
        });
    };

    var internalDownloadConfigs = function(ncmNodeIds, configType) {
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_240;E=js}');
        ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
            "DownloadConfigs",
            { nodeIds: ncmNodeIds, configType: configType },
            function(result) {
                waitMsg.hide();
                errorHandler('@{R=NCM.Strings;K=WEBJS_VK_241;E=js}', result);
                if (result == null) {
                    grid.store.reload();
                }
            }
        );
    };

    var compareConfigs = function() {
        window.open('/ui/ncm/configDiff');
    };

    var runConfigChangeReport = function() {
        internalGetNCMNodeIds(function(ncmNodeIds) {
            ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
                "RunConfigChangeReport",
                {},
                function(result) {
                    if (result != null) {
                        showWindow('@{R=NCM.Strings;K=WEBJS_VK_269;E=js}',
                            'iframeContainer',
                            function() {
                                SW.NCM.postToIFrame('iframeContainer',
                                    String.format(
                                        '/Orion/NCM/ConfigChangeReport.aspx?printable=true&StartTime={0}&EndTime={1}&ViewChangesOn={2}&ConfigType={3}&OnlyShowDevicesThatHadChanges={4}&Prefix={5}&ClientID={6}',
                                        result.starttime,
                                        result.endtime,
                                        result.viewchangeson,
                                        result.configtype,
                                        result.onlyshowdevicesthathadchanges,
                                        result.prefix,
                                        result.clientid),
                                    { ncmNodeIds: ncmNodeIds });
                            });
                    }
                }
            );
        });
    };

    var uploadConfig = function () {
        internalGetNCMNodeIds(function (ncmNodeIds) {
            showWindow('@{R=NCM.Strings;K=WEBJS_IA_15;E=js}', 'iframeContainer', function () {
                SW.NCM.postToIFrame('iframeContainer', '/Orion/NCM/UploadConfig.aspx?printable=true', { ncmNodeIds: ncmNodeIds });
            });
        });
    };

    var executeScript = function() {
        internalGetNCMNodeIds(function(ncmNodeIds) {
            showWindow(SW.NCM.ShowApprovalRequestButton
                ? '@{R=NCM.Strings;K=WEBJS_VK_291;E=js}'
                : '@{R=NCM.Strings;K=WEBJS_VK_272;E=js}',
                'iframeContainer',
                function() {
                    window.frames.iframeContainer.location.href = '/Orion/NCM/ExecuteScript.aspx?printable=true';
                });
        });
    };

    var internalExecuteScript = function(ncmNodeIds, script) {
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_242;E=js}');
        ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
            "ExecuteScript",
            { nodeIds: ncmNodeIds, script: script },
            function(result) {
                waitMsg.hide();
                errorHandler('@{R=NCM.Strings;K=WEBJS_VK_243;E=js}', result);
                if (result == null) {
                    grid.store.reload();
                }
            }
        );
    };

    var sendRequestForApproval = function(script) {
        internalGetNCMNodeIds(function(ncmNodeIds) {
            var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_244;E=js}');
            var guid = newGuid();
            ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
                "SendRequestForApproval",
                {
                    nodeIds: ncmNodeIds,
                    configType: '',
                    reboot: false,
                    requestType: SW.NCM.RequestType,
                    script: script,
                    guidId: guid,
                    configId: null
                },
                function(result) {
                    waitMsg.hide();
                    errorHandler('@{R=NCM.Strings;K=WEBJS_VK_245;E=js}', result);
                    if (result == null) {
                        showWindow('@{R=NCM.Strings;K=WEBJS_VK_291;E=js}',
                            'iframeContainer',
                            function() {
                                window.frames.iframeContainer.location.href =
                                    String.format(
                                        '/Orion/NCM/Admin/ConfigChangeApproval/ViewApprovalRequest.aspx?printable=true&GuidID={0}&NCMReturnURL={1}',
                                        guid,
                                        SW.NCM.CloseWindowHandlerUrl);
                            });
                    }
                }
            );
        });
    };

    var editProperties = function() {
        internalGetOrionNodeIds(function(orionNodeIds) {
            showWindow('@{R=NCM.Strings;K=WEBJS_VK_274;E=js}',
                'iframeContainer',
                function() {
                    window.frames.iframeContainer.location.href = String.format(
                        '/Orion/Nodes/NodeProperties.aspx?printable=true&Nodes={0}&GuidID={1}&ReturnTo={2}',
                        orionNodeIds.join(','),
                        newGuid(),
                        SW.NCM.CloseWindowHandlerUrl);
                });
        });
    };

    var updateInventory = function() {
        internalGetNCMNodeIds(function(ncmNodeIds) {
            Ext.Msg.confirm('@{R=NCM.Strings;K=WEBJS_VK_80;E=js}',
                String.format('@{R=NCM.Strings;K=WEBJS_VK_325;E=js}', ncmNodeIds.length),
                function(btn, text) {
                    if (btn == 'yes') {
                        internalUpdateInventory(ncmNodeIds);
                    }
                });

        });
    };

    var internalUpdateInventory = function(ncmNodeIds) {
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_323;E=js}');
        ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
            "UpdateInventory",
            { nodeIds: ncmNodeIds },
            function(result) {
                waitMsg.hide();
                errorHandler('@{R=NCM.Strings;K=WEBJS_VK_324;E=js}', result);
                if (result == null) {
                    window.location = "/Orion/NCM/Resources/InventoryReports/InventoryStatus.aspx";
                }
            }
        );
    };

    var internalGetOrionNodeIds = function(onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
            "GetSelectedOrionNodeIds",
            {},
            function(orionNodeIds) {
                onSuccess(orionNodeIds);
            }
        );
    };

    var internalGetNCMNodeIds = function(onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
            "GetSelectedNCMNodeIds",
            {},
            function(ncmNodeIds) {
                onSuccess(ncmNodeIds);
            }
        );
    };

    var internalGetNCMNodeIdsForTransfer = function(onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
            "GetNCMNodeIdsForTransfer",
            {},
            function(result) {
                onSuccess(result);
            }
        );
    };

    var newGuid = function () {
        try {
            return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, function (c) {
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            });
        } catch (e) {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        }
    };

    var loadGrid = function(groupingQueryString, search, callback) {
        grid.store.removeAll();
        var topPagingToolbar = grid.getTopToolbar();
        topPagingToolbar.cursor = 0;
        grid.store.baseParams['limit'] = topPagingToolbar.pageSize;
        grid.store.proxy.conn.jsonData = {
            groupingQueryString: groupingQueryString,
            showSelectedOnly: showSelectedOnly,
            colToSearch: colToSearch,
            searchTerm: search,
            clientOffset: getDateTimeOffset()
        };
        searchTerm = search;
        cleanSearch();
        grid.store.on('load',
            function() {
                UpdateGridCheckboxState();
                UpdateGridSearchHighlight();
                clearSelectAll();
                setTimeout(initBaselinesStatusTips, 500);
            });
        topPagingToolbar.doLoad(0);
    };

    var getDateTimeOffset = function() {
        var d = new Date();
        var n = d.getTimezoneOffset();
        return n;
    };

    var updateDownloadButton = function(downloadButton, selCount) {
        if (selCount === 0) {
            downloadButton.setDisabled(true);
        } else {
            downloadButton.setDisabled(false);
            internalGetNCMNodeIds(function(ncmNodes) {
                ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
                    "GetConfigTypes",
                    { nodeIds: ncmNodes },
                    function(configTypes) {
                        downloadButton.menu.removeAll();
                        downloadButton.menu.add(new Ext.menu.TextItem({
                            text: '@{R=NCM.Strings;K=WEBJS_VK_265;E=js}',
                            cls: 'menu-item-text'
                        }));
                        var haveCustomConfigs = false;
                        Ext.iterate(configTypes,
                            function(configType) {
                                if (!configType.IsCustom) {
                                    downloadButton.menu.add({
                                        iconCls: 'menu-item-no-icon',
                                        text: configType.Name,
                                        handler: function() { downloadConfigs(configType.Name); }
                                    });
                                } else {
                                    haveCustomConfigs = true;
                                }
                            });
                        if (haveCustomConfigs && configTypes.length > 0) {
                            downloadButton.menu.add(new Ext.menu.Separator());
                            downloadButton.menu.add(new Ext.menu.TextItem({
                                text: '@{R=NCM.Strings;K=WEBJS_VK_266;E=js}',
                                cls: 'menu-item-text'
                            }));
                            Ext.iterate(configTypes,
                                function(configType) {
                                    if (configType.IsCustom) {
                                        downloadButton.menu.add({
                                            iconCls: 'menu-item-no-icon',
                                            text: configType.Name,
                                            handler: function() { downloadConfigs(configType.Name); }
                                        });
                                    }
                                });
                        }
                    }
                );
            });
        }
    };

    var updateToolbar = function(selCount) {
        var map = toolbar.items.map;
        updateDownloadButton(map.Download, selCount);

        var needsAtLeastOneSelected = (selCount === 0);
        map.CompareConfigs.setDisabled(needsAtLeastOneSelected);
        map.Upload.setDisabled(needsAtLeastOneSelected);
        map.ExecuteScript.setDisabled(needsAtLeastOneSelected);
        map.EditProperties.setDisabled(needsAtLeastOneSelected);
        map.UpdateInventory.setDisabled(needsAtLeastOneSelected);
    };

    var updateGridCheckboxState = function(count) {
        if (grid.getStore().getCount() > 0 && count == grid.getStore().getCount()) {
            $("#selectAllRows").attr("checked", "checked");
        } else {
            $("#selectAllRows").removeAttr("checked");
        }
    };

    var clearSelectAll = function() {
        $("#selectAll").empty();
        $("#selectAll").removeClass("select-all-page");
    };

    var updateSelectAll = function() {
        $("#selectAll").empty();
        $("#selectAll").removeClass("select-all-page");

        var selCount = $('[orionNodeId]:checked').length;
        var totalCount = grid.getStore().getTotalCount();
        var pageCount = grid.getStore().query('Level', 1).length;

        if ((pageCount > 0) && (totalCount > pageSize) && (selCount == pageCount)) {
            var countOfNodesOnPage = pageSize;
            if ((pageCount % pageSize) != 0) {
                countOfNodesOnPage = pageCount % pageSize;
            }

            $("#selectAll")
                .append(String.format(
                    "<span>@{R=NCM.Strings;K=WEBJS_VK_215;E=js}</span> <a href='#'>@{R=NCM.Strings;K=WEBJS_VK_216;E=js}</a>",
                    countOfNodesOnPage,
                    totalCount)).addClass("select-all-page");
            $("#selectAll a").click(function() {
                $("#selectAll").empty();
                $("#selectAll").removeClass("select-all-page");
                $("#selectAll").append(String.format("<span>@{R=NCM.Strings;K=WEBJS_VK_217;E=js}</span>"));

                var node = tree.getSelectionModel().getSelectedNode();
                var groupingQueryString = getGroupingQueryString('', node);

                ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
                    "ChangeGridAllSelectionState",
                    { groupingQueryString: groupingQueryString, colToSearch: colToSearch, searchTerm: searchTerm },
                    function(result) {
                        updateSelection(result);
                    }
                );
            });
        }
    };

    ToggleSelectAllRows = function(that) {
        var check = that.checked;
        $('input:checkbox[orionNodeId^]').each(function() {
            this.checked = check;
        });

        var count = 0;
        var nodeIds = new Object();
        grid.store.data.each(function(record, index, length) {
            if (record.data.Level == 1) {
                record.set('Checked', check ? 1 : 0);
                record.commit();
                nodeIds[record.data.OrionNodeID] = record.data.NCMNodeID;
                count++;
            }
        });

        if (count > 0) {
            ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
                "ChangeGridMultipleSelectionState",
                { nodeIds: nodeIds, check: check },
                function(result) {
                    updateSelection(result);
                    updateSelectAll();
                }
            );
        }

        UpdateGridSearchHighlight();
    };

    ToggleSelectRow = function(that, recordId) {
        var check = that.checked;
        if ($('[orionNodeId]:checked').length != $('[orionNodeId]').length) {
            $("#selectAllRows").removeAttr("checked");
        }

        var recordIndex = gridStore.indexOfId(recordId);
        if (recordIndex >= 0) {
            var record = gridStore.getAt(recordIndex);
            if (record) {
                record.set('Checked', check ? 1 : 0);
                record.commit();
            }
        }

        var orionNodeId = that.getAttribute("orionNodeId");
        var ncmNodeId = that.getAttribute("ncmNodeId");
        ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
            "ChangeGridSingleSelectionState",
            { orionNodeId: orionNodeId, ncmNodeId: ncmNodeId, check: check },
            function(result) {
                updateSelection(result);
                updateSelectAll();
            }
        );

        UpdateGridSearchHighlight();
    };

    var updateSelection = function(result) {
        $("#gridSelection").empty();
        if (result > 0) {
            if (showSelectedOnly) {
                $("#gridSelection").append(String.format(
                    "<span>@{R=NCM.Strings;K=WEBJS_VK_247;E=js}</span><span id='showSelectedGridRowsOnly' style='padding-left:10px;'><b>@{R=NCM.Strings;K=WEBJS_VK_248;E=js}</b></span><span style='padding-left:10px;'><a href='#' id='clearSelection' style='color: #4779C4;text-decoration: underline;'>@{R=NCM.Strings;K=WEBJS_VK_250;E=js}</a></span>",
                    "<b>",
                    result,
                    "</b>"));
                ShowSelectedRowsOnly();
            } else {
                $("#gridSelection").append(String.format(
                    "<span>@{R=NCM.Strings;K=WEBJS_VK_247;E=js}</span><span id='showSelectedGridRowsOnly' style='padding-left:10px;'><a href='#' id='showSelectedOnly' style='color: #4779C4;text-decoration: underline;'>@{R=NCM.Strings;K=WEBJS_VK_249;E=js}</a></span><span style='padding-left:10px;'><a href='#' id='clearSelection' style='color: #4779C4;text-decoration: underline;'>@{R=NCM.Strings;K=WEBJS_VK_250;E=js}</a></span>",
                    "<b>",
                    result,
                    "</b>"));
            }

            $("#showSelectedOnly").click(function() {
                ToggleShowSelectedOnly();
            });

            $("#clearSelection").click(function() {
                ToggleClearSelection();
            });
        } else {
            $("#gridSelection").append("@{R=NCM.Strings;K=WEBJS_VK_246;E=js}");
            if (showSelectedOnly) {
                ClearSelection();
            }
        }
        updateToolbar(result);
        selectionCount = result;
    };

    var changeSelectionIfNeeded = function() {
        if (showSelectedOnly) {
            showSelectedOnly = false;
            $("#showSelectedGridRowsOnly").empty();
            $("#showSelectedGridRowsOnly")
                .append(String.format(
                    "<a href='#' id='showSelectedOnly' style='color: #4779C4;text-decoration: underline;'>@{R=NCM.Strings;K=WEBJS_VK_249;E=js}</a>"));
            $("#showSelectedOnly").click(function() {
                ToggleShowSelectedOnly();
            });
        }
    };

    ToggleShowSelectedOnly = function() {
        $("#showSelectedGridRowsOnly").empty();
        $("#showSelectedGridRowsOnly").append(String.format("<b>@{R=NCM.Strings;K=WEBJS_VK_248;E=js}</b>"));
        ShowSelectedRowsOnly();
    };

    ToggleClearSelection = function() {
        SW.NCM.Rows = new Object();
        ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
            "ClearGridSelectionState",
            {},
            function() {
                ClearSelection();
                updateSelection(0);
                clearSelectAll();
            }
        );
    };

    ShowSelectedRowsOnly = function() {
        showSelectedOnly = true;
        clearTreeSelection();
        var node = tree.getSelectionModel().getSelectedNode();
        var groupingQueryString = getGroupingQueryString('', node);
        loadGrid(groupingQueryString, '');
    };

    ClearSelection = function() {
        showSelectedOnly = false;
        clearTreeSelection();
        selectLastSelectedNode(lastSelectedNode);
    };

    UpdateGridCheckboxState = function() {
        var count = 0;
        grid.store.data.each(function(record, index, length) {
            if (record != null && record.data.Checked == 1) {
                count++;
            }
        });
        updateGridCheckboxState(count);
    };

    UpdateGridSearchHighlight = function() {
        var search = searchTerm;
        if ($.trim(search).length == 0) return;

        var columnsToHighlight = SW.NCM.ColumnIndexByDataIndex(grid.getColumnModel(), colToHighlight.split(';'));
        Ext.each(columnsToHighlight,
            function(columnNumber) {
                SW.NCM.highlightSearchText("#Grid .x-grid3-body .x-grid3-td-" + columnNumber, search);
            });
        updateSearchResults(grid.getStore().getTotalCount());
    };

    var updateGridSelectionState = function() {
        SW.NCM.callWebMethod('/Orion/NCM/Services/ConfigManagement.asmx/GetGridSelectionState',
            {},
            function(result) {
                var count = result.d;
                updateToolbar(count);
                updateSelection(count);
                return;
            });
    };

    RemoveRow = function (expander, recordId) {
        var recordIndex = gridStore.indexOfId(recordId);
        var recordToRemove = gridStore.getAt(recordIndex);
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_251;E=js}');
        Ext.Msg.confirm('@{R=NCM.Strings;K=WEBJS_IA_21;E=js}',
            String.format('@{R=NCM.Strings;K=WEBJS_IA_22;E=js}', recordToRemove.data.NCMNodeID),
            function (btn) {
                if (btn == 'yes') {
                    ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
                        "DeleteConfig", { configId: recordToRemove.data.NCMNodeID },
                        function (result) {
                            waitMsg.hide();
                            if (result == null) {
                                var beforeRecord = gridStore.getAt(recordIndex - 1);
                                var afterRecord = gridStore.getAt(recordIndex + 1);
                                gridStore.removeAt(recordIndex); //remove record from dataStore
                                if (beforeRecord.data.Level == 1) {
                                    if (afterRecord == null) {
                                        gridStore.insert(recordIndex, NoConfigsRecord(2));
                                    } else if (afterRecord.data.Level == 1) {
                                        gridStore.insert(recordIndex, NoConfigsRecord(2));
                                    }
                                }
                            }
                            errorHandler('@{R=NCM.Strings;K=WEBJS_VK_252;E=js}', result);
                        });
                }
            });
    };

    SetAsFavorite = function (expander, recordId) {
        var rowRecord = gridStore.getById(recordId);
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_253;E=js}');
        ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
            "SetRemoveFavorite", { configId: rowRecord.data.NCMNodeID },
            function (result) {
                waitMsg.hide();
                if (result == null) {
                    rowRecord.set('IPAddress', 'Favorite');
                    rowRecord.commit();
                }
                errorHandler('@{R=NCM.Strings;K=WEBJS_VK_254;E=js}', result);
            });
    };

    RemoveFavorite = function (expander, recordId) {
        var rowRecord = gridStore.getById(recordId);
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_255;E=js}');
        ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
            "SetRemoveFavorite", { configId: rowRecord.data.NCMNodeID },
            function (result) {
                waitMsg.hide();
                if (result == null) {
                    rowRecord.set('IPAddress', rowRecord.data.OriginConfigType == 'Edited' ? rowRecord.data.ParentConfigType : rowRecord.data.OriginConfigType);
                    rowRecord.commit();
                }
                errorHandler('@{R=NCM.Strings;K=WEBJS_VK_256;E=js}', result);
            });
    };

    ShowAllRow = function (expander, recordId) {
        var rowRecord = gridStore.getById(recordId);
        ExpandRow(expander, recordId);
    };

    ToggleRow = function (expander, recordId) {
        if (expandedRows[recordId]) {
            CollapseRow(expander, recordId);
        }
        else {
            ExpandRow(expander, recordId);
        }
    };

    CollapseRow = function (expander, recordId) {
        var rowRecord = gridStore.getById(recordId);
        var index = gridStore.indexOfId(recordId);
        var limit = gridStore.getCount();
        var toRemove = new Array();

        //remove all records in a branch under current record, use Level to check this
        for (var i = index + 1; i < limit; i++) {
            var record = gridStore.getAt(i);
            if (record.data.Level > rowRecord.data.Level) {
                toRemove.push(record);
            } else {
                break;
            }
        }

        gridStore.remove(toRemove);
        expandedRows[recordId] = false;
        expander.className = 'tree-grid-expander-expand';
    };

    GetStartRowNumber = function(recordIndex) {
        var start = 0;
        var record = gridStore.getAt(recordIndex);
        if (record) {
            while (record.data.Level != 1) {
                start++;
                recordIndex--;
                record = gridStore.getAt(recordIndex);
            }
        }
        return start;
    };

    ExpandRow = function(expander, recordId) {
        var rowRecord = gridStore.getById(recordId);
        var store = new ORION.WebServiceStore(
            "/Orion/NCM/Services/ConfigManagement.asmx/GetConfigsPaged",
            config.fieldMapping,
            config.orderByColumn
        );

        store.addListener("exception",
            function(dataProxy, type, action, options, response, arg) {
                var error = eval("(" + response.responseText + ")");
                Ext.Msg.show({
                    title: 'Error',
                    msg: error.Message,
                    icon: Ext.Msg.ERROR,
                    buttons: Ext.Msg.OK
                });
            });

        var id = rowRecord.data.NCMNodeID;
        var level = 2;
        var start = 1;
        var showAllConfigs = false;

        var recordIndex = gridStore.indexOfId(recordId);

        //parse "show all" record id
        if (id.startsWith('ShowAll:')) {
            id = id.substring('ShowAll:'.length);
            gridStore.removeAt(recordIndex); //remove "show all" record
            gridStore.insert(recordIndex, LoadingRecord(level)); //insert "loading" record
            showAllConfigs = true;
            start = GetStartRowNumber(recordIndex);
        } else {
            recordIndex++;
            gridStore.insert(recordIndex, LoadingRecord(level)); //insert "loading" record
        }

        store.proxy.conn.jsonData = {
            nodeId: id,
            start: start,
            showAllConfigs: showAllConfigs,
            clientOffset: getDateTimeOffset()
        };

        store.addListener("load",
            function(store) {
                expandedRows[recordId] = true;
                expander.className = 'tree-grid-expander-collapse';

                var toAdd = new Array();

                store.each(function(record) {
                    toAdd.push(record);
                });

                gridStore.removeAt(recordIndex); //remove "loading" record

                if (toAdd.length > 0) {
                    gridStore.insert(recordIndex, toAdd);
                    recordIndex = recordIndex + toAdd.length
                }

                if (!showAllConfigs) {
                    //insert "show all" record
                    ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
                        "GetConfigsTotalRows",
                        { nodeId: id },
                        function(totalRows) {
                            if (totalRows > 3) {
                                //show only first three configs
                                gridStore.insert(recordIndex, ShowAllRecord(id, level));
                            } else if (totalRows == 0) {
                                gridStore.insert(recordIndex, NoConfigsRecord(level));
                            }
                        }
                    );
                }
            });
        store.load();
    };

    LoadingRecord = function(level) {
        var record = Ext.data.Record.create(gridStore.fields);
        var loadingRecord = new record({
            NCMNodeID: 'LoadingRecord',
            Caption: '@{R=NCM.Strings;K=WEBJS_VK_04;E=js}',
            Level: level
        });
        return loadingRecord;
    };

    ShowAllRecord = function(id, level) {
        var record = Ext.data.Record.create(gridStore.fields);
        var showAllRecord = new record({
            NCMNodeID: String.format('ShowAll:{0}', id),
            Caption: '@{R=NCM.Strings;K=WEBJS_VK_257;E=js}',
            Level: level
        });
        return showAllRecord;
    };

    NoConfigsRecord = function(level) {
        var record = Ext.data.Record.create(gridStore.fields);
        var showAllRecord = new record({
            NCMNodeID: 'NoConfigs',
            Caption: '@{R=NCM.Strings;K=WEBJS_VK_258;E=js}',
            Level: level
        });
        return showAllRecord;
    };

    EditConfig = function(configId) {
        showWindow('@{R=NCM.Strings;K=WEBJS_VK_303;E=js}',
            'iframeContainer',
            function() {
                window.frames.iframeContainer.location.href = String.format(
                    '/Orion/NCM/Resources/Configs/EditConfig.aspx?printable=true&ConfigID={0}&NCMReturnURL={1}',
                    configId,
                    SW.NCM.CloseWindowHandlerUrl);
            });
    };

    function renderErrorDetails(value, meta, record) {
        if (record.data.IsActiveTransfer != null && record.data.IsActiveTransfer == true) {
            return '';
        }
        else {
            if (record.data.IsTransferError != null && record.data.IsTransferError == true) {
                return String.format('<span style="color:red;">{0}</span>', value);
            }
            else {
                return '';
            }
        }
    }

    function renderIPAddress(value, meta, record) {
        if (record.data.Level == 1) {
            return value;
        }
        else {
            if (record.data.Level == 2 && (record.data.NCMNodeID == 'LoadingRecord' || record.data.NCMNodeID == 'NoConfigs' || record.data.NCMNodeID.startsWith('ShowAll:'))) {
                return '';
            }
            else {
                if (record.data.IPAddress === 'Favorite') {
                    value = String.format('@{R=NCM.Strings;K=WEBJS_VK_340;E=js}', record.data.OriginConfigType === "Edited" ? String.format('@{R=NCM.Strings;K=WEBJS_VK_341;E=js}', record.data.ParentConfigType) : record.data.OriginConfigType);
                } else {
                    if (record.data.OriginConfigType === 'Edited') {
                        value = String.format('@{R=NCM.Strings;K=WEBJS_VK_341;E=js}', record.data.IPAddress);
                    }
                }
                return String.format('<span style="color:gray;">{0}</span>', value);
            }
        }
    }

    function renderName(value, meta, record) {
        
        if (record.data.Level == 1) {
            return String.format('<a target="__blank" href="/Orion/NCM/NodeDetails.aspx?NodeID={0}"><img src="/Orion/images/StatusIcons/{1}" /><span style="padding-left:5px;">{2}</span></a>', record.data.NCMNodeID, getStatusIcon(record.data.Icon), value);
        }
        else {
            if (record.data.Level == 2 && record.data.NCMNodeID == 'LoadingRecord') {
                return String.format('<span class="tree-grid-loading-img"><span class="tree-grid-loading-text">{0}</span></span>', value);
            }
            else if (record.data.Level == 2 && record.data.NCMNodeID == 'NoConfigs') {
                return String.format('<span class="tree-grid-expander-no-expand">{0}</span>', value);
            }
            else if (record.data.Level == 2 && record.data.NCMNodeID.startsWith('ShowAll:')) {
                return String.format('<span class="tree-grid-expander-no-expand"><a href="#" onclick="ShowAllRow(this, \'{0}\');">{1}</a></span>', record.id, value);
            }
            else {
                if (value.startsWith('/Date(')) {
                    if (value.match(/\/Date\((\d+)\)\//gi) != null) {
                        var date = eval(value.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
                        value = Ext.util.Format.date(date, ExtDaTimeLocalePattern.DateTimeLong);
                        return String.format('<span class="tree-grid-expander-no-expand"><a target="__blank" href="/Orion/NCM/ConfigDetails.aspx?ConfigID={{{0}}}"><img src="/Orion/NCM/Resources/images/ConfigIcons/Config-{1}.png" />{2}</a></span>',
                            record.data.NCMNodeID,record.data.Icon, value);
                    }
                    else {
                        return value;
                    }
                }
                else {
                    return value;
                }
            }
        }
    }

    function renderCheckbox(value, meta, record) {
        if (record.data.Level == 1) {
            var checked = record.data.Checked ? 'checked="checked"' : ''
            return String.format('<input id="coreNodeId_{3}" type="checkbox" {0} onclick="ToggleSelectRow(this, \'{1}\');" recordId="{1}" ncmNodeId="{2}" orionNodeId="{3}" />', checked, record.id, record.data.NCMNodeID, record.data.OrionNodeID);
        }
        else {
            return '';
        }
    }

    function renderExpand(value, meta, record) {
        if (record.data.Level == 1 && record.data.IsExpandable == 1) {
            var expanded = expandedRows[record.id] || false;
            if (expanded) {
                return String.format('<img src="/Orion/NCM/Resources/images/Pixel.gif" class="tree-grid-expander-collapse" onclick="ToggleRow(this, \'{0}\');" />', record.id);
            }
            else {
                return String.format('<img src="/Orion/NCM/Resources/images/Pixel.gif" class="tree-grid-expander-expand" onclick="ToggleRow(this, \'{0}\');" />', record.id);
            }
        }
        else {
            return '';
        }
    }

    function renderLastActionType(value, meta, record) {
        if (record.data.Level == 1) {
            var src = getActionTypeIconSrc(value);
            if (src != '') {
                return String.format('<img src="{0}" />', src);
            }
        }
        return '';
    }


    function renderBaselineStatus(value, meta, record) {
        if (record.data.Level === 1) {
            return getBaselineStateSrc(value, record.data.OrionNodeID);
        }
        else {
            if (record.data.Level === 2 && (record.data.NCMNodeID === 'LoadingRecord' || record.data.NCMNodeID === 'NoConfigs' || record.data.NCMNodeID.startsWith('ShowAll:'))) {
                return '';
            }
            if (((SW.NCM.NCMAccountRole === 'WebViewer') || (SW.NCM.NCMAccountRole === 'WebDownloader')) && !SW.NCM.IsDemoServer) {
                return '';
            }

            if (record.data.IsBinary) {
                return '';
            }
            else {
                var parameters = String.format('configId={0}', record.data.NCMNodeID);
                var baselinePageAddress = String.format('/ui/ncm/editBaseline?{0}', parameters);
                var linkValue = SW.NCM.IsDemoServer ? '#' : baselinePageAddress;
                var action = SW.NCM.IsDemoServer ? 'onclick=\"demoAction(\'NCM_Promote_To_Baseline\', this);\"' : '';
                var linkCaption = '@{R=NCM.Strings;K=WEBJS_PROMOTE_TO_BASELINE;E=js}';
                var link = String.format('<a href="{0}" {1}>{2}</a>', linkValue, action, linkCaption);
                return String.format('<div style="text-align:left;">{0}</div>', link);
            }
        }
    }

    function renderLastActionDate(value, meta, record) {
        if (record.data.Level == 1) {
            if (record.data.IsActiveTransfer != null && record.data.IsActiveTransfer == true) {
                return '<span class="tree-grid-inprogress"><span class="tree-grid-loading-text">@{R=NCM.Strings;K=WEBJS_VK_262;E=js}</span></span>';
            }
            if (value) {
                if (value.startsWith('/Date(')) {
                    if (value.match(/\/Date\((\d+)\)\//gi) != null) {
                        var date = eval(value.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
                        value = Ext.util.Format.date(date, ExtDaTimeLocalePattern.DateTimeLong);
                        if (record.data.IsTransferError != null && record.data.IsTransferError == true) {
                            return String.format('<span style="color:red;">{0}</span>', value);
                        }
                        else {
                            return String.format('<span style="color:black;">{0}</span>', value);
                        }
                    }
                }
            }
        }
        return '';
    }

    function renderDateTime(value, meta, record) {
        if (value == null) return '';

        if (value.startsWith('/Date(')) {
            if (value.match(/\/Date\((\d+)\)\//gi) != null) {
                var date = eval(value.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
                return Ext.util.Format.date(date, ExtDaTimeLocalePattern.DateTimeLong);
            }
        }
        return '';
    }

    function renderDeviceTemplateLink(value, meta, record) {
        if (record.data.Level == 1) {
            if (record.data.NCMNodeID == null) {
                return '';
            }

            if (!SW.NCM.IsDemoServer) { //in demo mode we don't check any rights
                if (!(SW.NCM.NCMAccountRole == 'Administrator') || !SW.NCM.AllowNodeManagement) {
                    return '';
                }
            }

            if (record.data.IsActiveTransfer != null && record.data.IsActiveTransfer == true) {
                return '';
            }
            else {
                if (record.data.IsTransferError != null && record.data.IsTransferError == true) {
                    return String.format('<a href="#" class="ncm_SuggestedAction" value="{0}">@{R=NCM.Strings;K=WEBJS_VK_330;E=js}</a>', record.data.NCMNodeID);
                }
                else {
                    return '';
                }
            }
        }
        else {
            if (record.data.Level == 2 && (record.data.NCMNodeID == 'LoadingRecord' || record.data.NCMNodeID == 'NoConfigs' || record.data.NCMNodeID.startsWith('ShowAll:'))) {
                return '';
            }
            else {
                if (((SW.NCM.NCMAccountRole == 'WebViewer') || (SW.NCM.NCMAccountRole == 'WebDownloader')) && !SW.NCM.IsDemoServer) {
                    return '';
                }
                else {
                    var html = String.format('<a href="#" onclick="{0}">@{R=NCM.Strings;K=WEBJS_VK_259;E=js}</a>', SW.NCM.IsDemoServer ? "demoAction('NCM_Set_As_Favorite', this);" : String.format("SetAsFavorite(this, \'{0}\');", record.id));
                    if (record.data.IPAddress == 'Favorite') {
                        html = String.format('<a href="#" onclick="{0}">@{R=NCM.Strings;K=WEBJS_VK_260;E=js}</a>', SW.NCM.IsDemoServer ? "demoAction('NCM_Remove_Favorite', this);" : String.format("RemoveFavorite(this, \'{0}\');", record.id));
                    }
                    return String.format(
                        '<div style="text-align:right;">{0}<span style="display:{3};"><span style="color:gray;padding:0px 5px 0px 5px;">|</span><a href="#" onclick="EditConfig(\'{1}\');" >@{R=NCM.Strings;K=WEBJS_VK_301;E=js}</a></span><span style="color:gray;padding:0px 5px 0px 5px;">|</span><a href="#" onclick="{2}">@{R=NCM.Strings;K=WEBJS_VK_261;E=js}</a></div>',
                        html,
                        record.data.NCMNodeID,
                        SW.NCM.IsDemoServer ? "demoAction('NCM_Delete_Config', this);" : String.format("RemoveRow(this, \'{0}\');", record.id),
                        record.data.IsBinary ? "none" : "inline");
                }
            }
        }
    }

    var getActionTypeIconSrc = function(type) {
        switch (type) {
        case 1:
            return '/Orion/NCM/Resources/images/Events/EventConfigDownload.gif';
        case 2:
            return '/Orion/NCM/Resources/images/Events/EventConfigUpload.gif';
        case 3:
            return '/Orion/NCM/Resources/images/ConfigSnippets/icon_execute.gif';
        default:
            return '';
        }
    };

    var getRecordByNcmNodeId = function(ncmNodeId) {
        var recordIndex = gridStore.findExact("NCMNodeID", ncmNodeId);
        if (recordIndex >= 0) {
            return gridStore.getAt(recordIndex);
        }
    };

    var getBaselinesStatusTooltipHtml = function(ncmNodeId, callback) {
        var record = getRecordByNcmNodeId(ncmNodeId);
        if (!record) {
            callback(buildErrorTooltipHtml());
            return;
        }

        $.ajax({
            type: "GET",
            async: false,
            url: String.format("/api2/ncm/baselines/violations/node/{0}", ncmNodeId),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(baselineViolations, status, xhr) {
                callback(buildBaselinesStatusTooltipHtml(record.data.OrionNodeID,
                    record.data.BaselineStatus,
                    baselineViolations));
            },
            error: function(xhr, status, errorThrown) {
                callback(buildErrorTooltipHtml());
            }
        });
    };

    var buildBaselinesStatusTooltipHtml = function(orionNodeId, parentBaselineStatus, baselineViolations) {
        var html = String.format(
            '<table id="tipTable_{0}" onmouseenter="showBaselinesStatusTooltip();" onmouseleave="tooltipIsHidden = false; hideBaselinesStatusTooltip();" class="baselines-status-tip-table" cellpadding="0" cellspacing="0">',
            orionNodeId);

        html += String.format(' \
                <tr> \
                    <td>{0}</td> \
                    <td colspan="2"><b>@{R=NCM.Strings;K=WEBJS_IC_02;E=js}</b></td> \
                </tr>',
            getBaselineStatusIcon(parentBaselineStatus));

        var hasErrors = baselineViolations.filter(function(baselineViolation) {
                if (baselineViolation.violationStatus === SW.NCM.BaselineViolationStateError) {
                    return baselineViolation;
                }
            }).length >
            0;

        if (hasErrors) {
            html += ' \
                <tr> \
                    <td style="white-space:nowrap;" colspan="3"> \
                        <div class="sw-suggestion sw-suggestion-fail" style="margin-top:5px;"> \
                            <div class="sw-suggestion-icon"></div> \
                            @{R=NCM.Strings;K=WEBJS_VK_342;E=js} &raquo; <a target="__blank" class="sw-suggestion-link" href="https://solarwinds.com/documentation/kbloader.aspx?kb=MT126991">@{R=NCM.Strings;K=WEBJS_VK_343;E=js}</a> \
                        </div> \
                    </td > \
                </tr>';
        }

        baselineViolations.forEach(function(baselineViolation) {
            html += String.format(' \
                <tr> \
                    <td>{0}</td> \
                    <td style="text-overflow:ellipsis;overflow:hidden;max-width:300px;">{1}</td> \
                    <td>{2}</td> \
                </tr>',
                getBaselineStatusIcon(baselineViolation.violationStatus),
                getBaselineName(baselineViolation),
                baselineViolation.configType);
        });

        html += "</table>";
        return html;
    };

    var buildErrorTooltipHtml = function() {
        return "<div style='padding:5px;color:red;'>Unable to get data from the server.</div>";
    };

    var getBaselineDiffUrl = function(baselineId, configId) {
        return String.format("/ui/ncm/baselineDiff/{0}/{1}", baselineId, configId);
    };

    var getBaselineName = function (baselineViolation) {
        var baselineName = Ext.util.Format.htmlEncode(baselineViolation.baselineName);
        var validStates = [1, 2];
        if (validStates.indexOf(baselineViolation.violationStatus) >= 0) {
            return String.format("<a href='{0}' target='__blank'>{1}</a>",
                getBaselineDiffUrl(baselineViolation.baselineId, baselineViolation.configId),
                baselineName);
        } else {
            return baselineName;
        }
    };

    var initBaselinesStatusTip = function(record) {
        var minWidth = 300;
        var target = $("#orionNodeId_" + record.data.OrionNodeID);
        if (target.length > 0) {
            var baselinesStatusTip = Ext.getCmp('baselinesStatusTip_' + record.data.OrionNodeID);
            if (!baselinesStatusTip) {
                baselinesStatusTip = new Ext.ToolTip({
                    width: minWidth,
                    id: 'baselinesStatusTip_' + record.data.OrionNodeID,
                    ncmNodeId: record.data.NCMNodeID,
                    orionNodeId: record.data.OrionNodeID,
                    baseCls: 'baselines-status-tip',
                    anchor: 'left',
                    displayed: false,
                    autoHide: false,
                    listeners: {
                        show: function(tooltip) {
                            if (!tooltip.displayed) {
                                activeBaselinesStatusTooltip = tooltip;
                                getBaselinesStatusTooltipHtml(tooltip.ncmNodeId,
                                    function(html) {
                                        tooltip.update(html);
                                        tooltip.displayed = true;

                                        var width = $("#tipTable_" + tooltip.orionNodeId).width() + 10;
                                        tooltip.setWidth(width < minWidth ? minWidth : width);
                                    });
                            }
                        },
                        hide: function(tooltip) {
                            tooltip.displayed = false;
                            tooltip.setWidth(minWidth);
                        }
                    }
                });
            }
            baselinesStatusTip.initTarget("orionNodeId_" + record.data.OrionNodeID);
        }
    };

    var initBaselinesStatusTips = function() {
        gridStore.each(function(record) {
            initBaselinesStatusTip(record);
        });
    };

    tooltipIsHidden = false;
    delayHideBaselinesStatusTooltip = function() {
        setTimeout(hideBaselinesStatusTooltip, 200);
    };

    hideBaselinesStatusTooltip = function() {
        if (!tooltipIsHidden) {
            if (activeBaselinesStatusTooltip) {
                activeBaselinesStatusTooltip.hide();
            }
        }
    };

    showBaselinesStatusTooltip = function() {
        tooltipIsHidden = true;
        if (activeBaselinesStatusTooltip) {
            activeBaselinesStatusTooltip.show();
        }
    };

    var getBaselineStatusIcon = function(status) {
        switch (status) {
        case 1:
            return '<img src="/Orion/NCM/Resources/images/severity_ok.png" style="padding-right:5px;"/>';
        case 2:
            return '<img src="/Orion/NCM/Resources/images/severity_critical.png" style="padding-right:5px;"/>';
        case 3:
            return '<img src="/Orion/NCM/Resources/images/spinner.gif" style="padding-right:5px;"/>';
        case 4:
            return '<img src="/Orion/NCM/Resources/images/error_icon_16x16.png" style="padding-right:5px;"/>';
        default:
            return '';
        }
    };

    var getBaselineStatusText = function(status) {
        switch (status) {
        case -1:
            return '@{R=NCM.Strings;K=WEBJS_IC_03;E=js}';
        case 0:
            return '@{R=NCM.Strings;K=WEBJS_VK_258;E=js}';
        case 1:
            return '@{R=NCM.Strings;K=WEBJS_IC_04;E=js}';
        case 2:
            return '@{R=NCM.Strings;K=WEBJS_IC_05;E=js}';
        case 3:
            return '@{R=NCM.Strings;K=WEBJS_IC_06;E=js}';
        case 4:
            return '@{R=NCM.Strings;K=WEBJS_VM_74;E=js}';
        default:
            return '';
        }
    };

    var getBaselineStateSrc = function(status, orionNodeId) {
        switch (status) {
        case -1:
        case 0:
            return getBaselineStatusText(status);
        case 1:
        case 2:
        case 3:
            return String.format(
                '<a onmouseleave="tooltipIsHidden = false; delayHideBaselinesStatusTooltip();" href="#" id="orionNodeId_{0}">{1}{2}</a>',
                orionNodeId,
                getBaselineStatusIcon(status),
                getBaselineStatusText(status));
        case 4:
            return String.format(
                '<a onmouseleave="tooltipIsHidden = false; delayHideBaselinesStatusTooltip();" href="#" id="orionNodeId_{0}" style="color:red;">{1}{2}</a>',
                orionNodeId,
                getBaselineStatusIcon(status),
                getBaselineStatusText(status));
        default:
            return '';
        }
    };

    var getStatusIcon = function(status) {
        switch (status) {
        case 1:
            return "Small-Up.gif";
        case 2:
            return "Small-Down.gif";
        case 3:
            return "Small-Warning.gif";
        case 9:
            return "Small-Unmanaged.gif";
        case 11:
            return "Small-External.gif";
        case 12:
            return "Small-Unreachable.gif";
        default:
            return "Small-Unknown.gif";
        }
    };
    
    function gridResize() {
        if (grid != null) {
            grid.setWidth(1);
            grid.setWidth($('#gridCell').width());

            grid.setHeight(getGridHeight());
        }
    };

    function getGridHeight() {
        var top = $('#gridCell').offset().top;
        return $(window).height() - top - $('#footer').height() - 160;
    };

    var treeResize = function() {
        tree.setWidth(1);
        tree.setWidth($('#treeCell').width());
        tree.setHeight(1);
        tree.setHeight($('#gridCell').height() - $('.ncm_GroupSection').height() - 12);
    };

    var initToolbar = function() {
        toolbar = new Ext.Toolbar({
            renderTo: 'toolbar',
            items: [
                {
                    id: 'Download',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_263;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_264;E=js}',
                    icon: '/Orion/NCM/Resources/images/Events/EventConfigDownload.gif',
                    cls: 'x-btn-text-icon',
                    hidden: (SW.NCM.NCMAccountRole == 'WebViewer') && !SW.NCM.IsDemoServer,
                    listeners: {
                        beforerender: function(that) {
                            var menu = that.menu;
                            menu.add(new Ext.menu.TextItem({
                                text: '@{R=NCM.Strings;K=WEBJS_VK_265;E=js}',
                                cls: 'menu-item-text'
                            }));
                            var haveCustomConfigs = false;
                            Ext.iterate(SW.NCM.ConfigTypesStore,
                                function(configType) {
                                    if (!configType.IsCustom) {
                                        menu.add({
                                            iconCls: 'menu-item-no-icon',
                                            text: configType.Name,
                                            handler: function() { downloadConfigs(configType.Name); }
                                        });
                                    } else {
                                        haveCustomConfigs = true;
                                    }
                                });
                            if (haveCustomConfigs) {
                                menu.add(new Ext.menu.Separator());
                                menu.add(new Ext.menu.TextItem({
                                    text: '@{R=NCM.Strings;K=WEBJS_VK_266;E=js}',
                                    cls: 'menu-item-text'
                                }));
                                Ext.iterate(SW.NCM.ConfigTypesStore,
                                    function(configType) {
                                        if (configType.IsCustom) {
                                            menu.add({
                                                iconCls: 'menu-item-no-icon',
                                                text: configType.Name,
                                                handler: function() { downloadConfigs(configType.Name); }
                                            });
                                        }
                                    });
                            }
                        }
                    },
                    menu: {
                        showSeparator: false,
                        cls: 'menu-download'
                    }
                }, (SW.NCM.NCMAccountRole == 'WebViewer') && !SW.NCM.IsDemoServer ? '' : '-', {
                    id: 'CompareConfigs',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_267;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_268;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigIcons/Config-Compare.gif',
                    cls: 'x-btn-text-icon',
                    menu: {
                        showSeparator: false,
                        cls: 'menu-compare-configs',
                        items: [
                            {
                                iconCls: 'item-no-icon',
                                text: '@{R=NCM.Strings;K=WEBJS_VK_267;E=js}',
                                handler: function() { compareConfigs(); }
                            },
                            {
                                iconCls: 'item-no-icon',
                                text: '@{R=NCM.Strings;K=WEBJS_VK_269;E=js}',
                                handler: function() { runConfigChangeReport(); }
                            }
                        ]
                    }
                },
                ((SW.NCM.NCMAccountRole == 'WebViewer') || (SW.NCM.NCMAccountRole == 'WebDownloader')) &&
                !SW.NCM.IsDemoServer
                ? ''
                : '-', {
                    id: 'Upload',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_270;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_IA_16;E=js}',
                    icon: '/Orion/NCM/Resources/images/Events/EventConfigUpload.gif',
                    cls: 'x-btn-text-icon',
                    hidden: ((SW.NCM.NCMAccountRole == 'WebViewer') || (SW.NCM.NCMAccountRole == 'WebDownloader')) &&
                        !SW.NCM.IsDemoServer,
                    handler: function() { uploadConfig(); }
                },
                ((SW.NCM.NCMAccountRole == 'WebViewer') || (SW.NCM.NCMAccountRole == 'WebDownloader')) &&
                !SW.NCM.IsDemoServer
                ? ''
                : '-', {
                    id: 'ExecuteScript',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_272;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_273;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_execute.gif',
                    cls: 'x-btn-text-icon',
                    hidden: ((SW.NCM.NCMAccountRole == 'WebViewer') || (SW.NCM.NCMAccountRole == 'WebDownloader')) &&
                        !SW.NCM.IsDemoServer,
                    handler: function() { executeScript(); }
                }, !SW.NCM.AllowNodeManagement && !SW.NCM.IsDemoServer ? '' : '-', {
                    id: 'EditProperties',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_274;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_275;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_editsnippet.gif',
                    cls: 'x-btn-text-icon',
                    hidden: !SW.NCM.AllowNodeManagement && !SW.NCM.IsDemoServer,
                    handler: function() { editProperties(); }
                }, SW.NCM.NCMAccountRole == 'WebViewer' && !SW.NCM.IsDemoServer ? '' : '-', {
                    id: 'UpdateInventory',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_80;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_81;E=js}',
                    icon: '/Orion/NCM/Resources/images/InventoryIcons/icon_update_inventory.gif',
                    cls: 'x-btn-text-icon',
                    hidden: SW.NCM.NCMAccountRole == 'WebViewer' && !SW.NCM.IsDemoServer,
                    handler: function() { updateInventory(); }
                }
            ]
        });
    };

    var pagingToolbarChangeHandler = function(that) {
        if (that.displayItem) {
            var count = 0;
            that.store.each(function(record) {
                if (record.data.Level == 1)
                    count++;
            });
            var msg = count == 0
                ? that.emptyMsg
                : String.format(that.displayMsg, that.cursor + 1, that.cursor + count, that.store.getTotalCount());
            that.displayItem.setText(msg);
        }
    };

    var errorHandler = function(source, result, func) {
        if (result != null && result.Error) {
            Ext.Msg.show({
                title: source,
                msg: result.Msg,
                minWidth: 500,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR,
                fn: func
            });
        }
    };

    refreshTransferStatus = function() {
        var orionIds = [];
        gridStore.data.each(function(record, index, length) {
            if (record.data.Level == 1 &&
                record.data.IsActiveTransfer != null &&
                record.data.IsActiveTransfer == true) {
                orionIds.push(record.data.OrionNodeID);
            }
        });

        if (orionIds.length > 0) {
            var store = new ORION.WebServiceStore(
                "/Orion/NCM/Services/ConfigManagement.asmx/GetTransferStatusPaged",
                [
                    { name: 'NCMNodeID', mapping: 0 },
                    { name: 'LastTransferActionType', mapping: 1 },
                    { name: 'LastTransferDate', mapping: 2 },
                    { name: 'LastTransferMessage', mapping: 3 },
                    { name: 'IsTransferError', mapping: 4 },
                    { name: 'IsActiveTransfer', mapping: 5 }
                ],
                "NCMNodeID");

            store.addListener("exception",
                function(dataProxy, type, action, options, response, arg) {
                    var error = eval("(" + response.responseText + ")");
                    Ext.Msg.show({
                        title: 'Error',
                        msg: error.Message,
                        icon: Ext.Msg.ERROR,
                        buttons: Ext.Msg.OK
                    });
                });

            store.proxy.conn.jsonData = { orionIds: orionIds, clientOffset: getDateTimeOffset() };

            store.addListener("load",
                function(store) {
                    store.each(function(record) {
                        var value = record.data.NCMNodeID;
                        var recordIndex = gridStore.findExact("NCMNodeID", value);
                        if (recordIndex >= 0) {
                            var recordToUpdate = grid.store.getAt(recordIndex);
                            recordToUpdate.set('LastTransferActionType', record.data.LastTransferActionType);
                            recordToUpdate.set('LastTransferDate', record.data.LastTransferDate);
                            recordToUpdate.set('LastTransferMessage', record.data.LastTransferMessage);
                            recordToUpdate.set('IsTransferError', record.data.IsTransferError);
                            recordToUpdate.set('IsActiveTransfer', record.data.IsActiveTransfer);
                            recordToUpdate.commit();
                        }
                    });
                });
            store.load();
        }
    };

    refreshBaselineStatus = function() {
        var orionNodeIds = [];
        gridStore.data.each(function(record) {
            orionNodeIds.push(record.data.OrionNodeID);
        });

        if (orionNodeIds.length > 0) {
            var store = new ORION.WebServiceStore(
                "/Orion/NCM/Services/ConfigManagement.asmx/GetBaselineStatusPaged",
                [
                    { name: 'OrionNodeID', mapping: 0 },
                    { name: 'BaselineStatus', mapping: 1 }
                ],
                "OrionNodeID");

            store.addListener("exception",
                function(dataProxy, type, action, options, response, arg) {
                    var error = eval("(" + response.responseText + ")");
                    Ext.Msg.show({
                        title: 'Error',
                        msg: error.Message,
                        icon: Ext.Msg.ERROR,
                        buttons: Ext.Msg.OK
                    });
                });

            store.proxy.conn.jsonData = { orionNodeIds: orionNodeIds };

            store.addListener("load",
                function(store) {
                    store.each(function(record) {
                        var recordIndex = gridStore.findExact("OrionNodeID", record.data.OrionNodeID);
                        if (recordIndex >= 0) {
                            var recordToUpdate = grid.store.getAt(recordIndex);
                            recordToUpdate.set('BaselineStatus', record.data.BaselineStatus);
                            recordToUpdate.commit();
                            initBaselinesStatusTip(recordToUpdate);
                        }
                    });
                });
            store.load();
        }
    };

    var refreshBaselinesId = 0;
    var refreshBaselines = function() {
        refreshBaselinesId = setInterval("refreshBaselineStatus()", 5000);
    };

    var refreshTransfersId = 0;
    var refreshTransfers = function() {
        refreshTransfersId = setInterval("refreshTransferStatus()", 5000);
    };

    var initSearch = function() {
        searchBox = new Ext.form.TwinTriggerField({
            renderTo: 'searchBox',
            width: 200,
            twin: true,
            trigger2Class: "x-form-search-trigger",
            onTrigger1Click: searchMenuClick,
            onTrigger2Click: doSearchClick,
            enableKeyEvents: true,
            listeners: {
                beforerender: function(that) {
                    var menu = that.menu;
                    menu.add(new Ext.menu.TextItem({
                        text: '@{R=NCM.Strings;K=WEBJS_VK_276;E=js}',
                        style: 'font-weight:bold;padding-left:5px;'
                    }));
                    menu.add(new Ext.menu.CheckItem({
                        text: '@{R=NCM.Strings;K=WEBJS_VK_277;E=js}',
                        group: 'GroupsToSearch',
                        checked: true,
                        hideOnClick: false,
                        allGroupsToSearch: true
                    }));
                    menu.add(new Ext.menu.CheckItem({
                        text: '@{R=NCM.Strings;K=WEBJS_VK_278;E=js}',
                        group: 'GroupsToSearch',
                        checked: false,
                        hideOnClick: false,
                        currentGroupToSearch: false
                    }));
                    menu.add(new Ext.menu.Separator());
                    menu.add(new Ext.menu.TextItem({
                        text: '@{R=NCM.Strings;K=WEBJS_VK_279;E=js}',
                        style: 'font-weight:bold;padding-left:5px;'
                    }));
                    Ext.each(config.searchMenuItems,
                        function(item) {
                            menu.add(item);
                        });
                },
                keypress: function(that, evnt) {
                    if (evnt.keyCode == 13) {
                        doSearchClick();
                    }
                }
            },
            menu: new Ext.menu.Menu({
                id: 'searchMenu',
                width: 200,
                showSeparator: false
            })
        });
    };

    var searchMenuClick = function() {
        var xy = searchBox.getPosition();
        xy[1] += searchBox.getHeight();
        searchBox.menu.showAt(xy);
    };

    var doSearchClick = function() {
        var searchValue = searchBox.getValue();
        if (searchValue == '') {
            Ext.Msg.show({
                title: '@{R=NCM.Strings;K=WEBJS_VK_280;E=js}',
                msg: '@{R=NCM.Strings;K=WEBJS_VK_281;E=js}',
                buttons: Ext.Msg.OK,
                animEl: searchBox,
                icon: Ext.MessageBox.ERROR
            });
            return;
        }

        var menu = searchBox.menu.items;
        Ext.each(menu.items,
            function(item) {
                if (item.allGroupsToSearch != undefined) {
                    allGroupsToSearch = item.checked;
                }
                if (item.colToSearch && item.checked) {
                    colToDisplay = item.text;
                    colToSearch = item.colToSearch;
                    colToHighlight = item.colToHighlight;
                }
            });

        var grouping = $.grep(getGrouping().split('|'), function(item) { return item.length != 0 });
        if (allGroupsToSearch) {
            groupingQueryString = '';
            if (grouping.length > 0) {
                clearTreeSelection();
            }
        } else {
            var node = tree.getSelectionModel().getSelectedNode();
            var groupingQueryString = getGroupingQueryString('', node);
            if (!node || grouping.length == 0) {
                Ext.Msg.show({
                    title: '@{R=NCM.Strings;K=WEBJS_VK_280;E=js}',
                    msg: '@{R=NCM.Strings;K=WEBJS_VK_282;E=js}',
                    buttons: Ext.Msg.OK,
                    animEl: searchBox,
                    icon: Ext.MessageBox.ERROR
                });
                return;
            }
        }
        changeSelectionIfNeeded();
        loadGrid(groupingQueryString, Ext.util.Format.htmlEncode(searchValue));
    };

    var updateSearchResults = function(result) {
        $("#searchResults").empty();
        if (searchTerm.length > 0) {
            if (allGroupsToSearch) {
                $("#searchResults").append(String.format(
                    '<div style="padding-bottom: 10px;">@{R=NCM.Strings;K=WEBJS_VK_284;E=js} <a href="#" style="color: #4779C4;text-decoration: underline;">@{R=NCM.Strings;K=WEBJS_VK_283;E=js}</a></div>',
                    "<b>",
                    result,
                    "</b>",
                    searchTerm,
                    colToDisplay));
            } else {
                $("#searchResults").append(String.format(
                    '<div style="padding-bottom: 10px;">@{R=NCM.Strings;K=WEBJS_VK_285;E=js} <a href="#" style="color: #4779C4;text-decoration: underline;">Clear results</a></div>',
                    "<b>",
                    result,
                    "</b>",
                    searchTerm,
                    getFoundForString(),
                    colToDisplay));
            }
            $("#searchResults a").click(function() {
                ToggleClearSearchResults();
            });
        }
        treeResize();
    };

    var getFoundForString = function () {
        var node = tree.getSelectionModel().getSelectedNode();

        var groupValueObject = new Array();
        getGroupValueObject(groupValueObject, node);
        var groups = groupValueObject.reverse();

        var foundFor = '';
        var first = true;
        Ext.each(groups, function (group) {
            if (first) {
                first = false;
            }
            else {
                foundFor += " &rarr; ";
            }
            foundFor += group;
        });
        return foundFor;
    }

    ToggleClearSearchResults = function () {
        searchTerm = '';
        cleanSearch();
        selectLastSelectedNode(lastSelectedNode);
    }

    var cleanSearch = function () {
        if (searchTerm == '') {
            searchBox.setValue(searchTerm);
            updateSearchResults(0);
        }
    }

    var createConfig = function (cpColumnModel) {
        var config = [];

        config.columnModel = [];
        config.fieldMapping = [];
        config.searchMenuItems = [];
        config.orderByColumn = 'Caption';

        config.columnModel.push({ header: 'NCMNodeID', width: 20, hidden: true, hideable: false, sortable: true, dataIndex: 'NCMNodeID' });
        config.columnModel.push({ header: 'OrionNodeID', width: 20, hidden: true, hideable: false, sortable: true, dataIndex: 'OrionNodeID' });
        config.columnModel.push({ header: '', width: 30, resizable: false, sortable: false, menuDisabled: true, hideable: false, align: 'center', dataIndex: 'Expand', renderer: renderExpand });
        config.columnModel.push({ header: '<input id="selectAllRows" onclick="ToggleSelectAllRows(this);" type="checkbox" name="test" />', width: 25, resizable: false, sortable: false, menuDisabled: true, hideable: false, align: 'center', dataIndex: 'Checkbox', renderer: renderCheckbox });
        config.columnModel.push({ header: '@{R=NCM.Strings;K=WEBJS_VK_43;E=js}', width: 300, sortable: true, dataIndex: 'Caption', renderer: renderName });
        config.columnModel.push({ header: '@{R=NCM.Strings;K=WEBJS_VK_44;E=js}', width: 200, sortable: true, dataIndex: 'IPAddress', renderer: renderIPAddress });
        config.columnModel.push({ header: '@{R=NCM.Strings;K=WEBJS_IC_02;E=js}', width: 200, resizable: true, sortable: true, align: 'left', dataIndex: 'BaselineStatus', renderer: renderBaselineStatus });
        config.columnModel.push({ header: '@{R=NCM.Strings;K=WEBJS_VK_286;E=js}', width: 50, resizable: true, sortable: true, align: 'center', dataIndex: 'LastTransferActionType', renderer: renderLastActionType });
        config.columnModel.push({ header: '@{R=NCM.Strings;K=WEBJS_VK_287;E=js}', width: 200, sortable: true, dataIndex: 'LastTransferDate', renderer: renderLastActionDate });
        config.columnModel.push({ header: '@{R=NCM.Strings;K=WEBJS_VK_288;E=js}', width: 400, sortable: false, dataIndex: 'LastTransferMessage', renderer: renderErrorDetails });
        config.columnModel.push({ header: '@{R=NCM.Strings;K=WEBJS_VK_289;E=js}', width: 200, sortable: true, hidden: true, dataIndex: 'NodeGroup' });
        config.columnModel.push({ header: '@{R=NCM.Strings;K=WEBJS_VK_53;E=js}', width: 200, sortable: true, hidden: true, dataIndex: 'MachineType' });
        config.columnModel.push({ header: '@{R=NCM.Strings;K=WEBJS_VK_52;E=js}', width: 200, sortable: true, hidden: true, dataIndex: 'Vendor' });
        config.columnModel.push({ header: '@{R=NCM.Strings;K=WEBJS_VK_206;E=js}', width: 200, sortable: true, hidden: true, dataIndex: 'IOSVersion' });
        config.columnModel.push({ header: '@{R=NCM.Strings;K=ConfigurationManagement_Column_SuggestedAction;E=js}', width: 350, sortable: false, dataIndex: 'DeviceTemplateLink', renderer: renderDeviceTemplateLink });
        config.columnModel.push({ header: '@{R=NCM.Strings;K=ComplianceReportResult_ConfigTitle_ColumnHeader;E=js}', width: 200, sortable: false, hidden: true, dataIndex: 'ConfigTitle' });
       
        config.fieldMapping.push({ name: 'NCMNodeID', mapping: 0 });
        config.fieldMapping.push({ name: 'OrionNodeID', mapping: 1 });
        config.fieldMapping.push({ name: 'Caption', mapping: 2 });
        config.fieldMapping.push({ name: 'IPAddress', mapping: 3 });
        config.fieldMapping.push({ name: 'Level', mapping: 4 });
        config.fieldMapping.push({ name: 'IsExpandable', mapping: 5 });
        config.fieldMapping.push({ name: 'ShowCheckbox', mapping: 6 });
        config.fieldMapping.push({ name: 'Checked', mapping: 7 });
        config.fieldMapping.push({ name: 'Icon', mapping: 8 });
        config.fieldMapping.push({ name: 'OriginConfigType', mapping: 9 });
        config.fieldMapping.push({ name: 'ParentConfigType', mapping: 10 });
        config.fieldMapping.push({ name: 'IsBinary', mapping: 11 });
        config.fieldMapping.push({ name: 'BaselineStatus', mapping: 12 });
        config.fieldMapping.push({ name: 'LastTransferActionType', mapping: 13 });
        config.fieldMapping.push({ name: 'LastTransferDate', mapping: 14 });
        config.fieldMapping.push({ name: 'LastTransferMessage', mapping: 15 });
        config.fieldMapping.push({ name: 'IsTransferError', mapping: 16 });
        config.fieldMapping.push({ name: 'IsActiveTransfer', mapping: 17 });
        config.fieldMapping.push({ name: 'NodeGroup', mapping: 18 });
        config.fieldMapping.push({ name: 'MachineType', mapping: 19 });
        config.fieldMapping.push({ name: 'Vendor', mapping: 20 });
        config.fieldMapping.push({ name: 'IOSVersion', mapping: 21 });
        config.fieldMapping.push({ name: 'DeviceTemplateLink', mapping: 22 });
        config.fieldMapping.push({ name: 'ConfigTitle', mapping: 23});

        config.searchMenuItems.push(new Ext.menu.CheckItem({ text: '@{R=NCM.Strings;K=WEBJS_IA_23;E=js}', group: 'TableToSearch', checked: true, hideOnClick: false, colToSearch: 'Nodes.Caption;Nodes.IP_Address', colToHighlight: 'Caption;IPAddress' }));
        config.searchMenuItems.push(new Ext.menu.CheckItem({ text: '@{R=NCM.Strings;K=WEBJS_VK_43;E=js}', group: 'TableToSearch', checked: false, hideOnClick: false, colToSearch: 'Nodes.Caption', colToHighlight: 'Caption' }));
        config.searchMenuItems.push(new Ext.menu.CheckItem({ text: '@{R=NCM.Strings;K=WEBJS_VK_44;E=js}', group: 'TableToSearch', checked: false, hideOnClick: false, colToSearch: 'Nodes.IP_Address', colToHighlight: 'IPAddress' }));
        config.searchMenuItems.push(new Ext.menu.CheckItem({ text: '@{R=NCM.Strings;K=WEBJS_VK_289;E=js}', group: 'TableToSearch', checked: false, hideOnClick: false, colToSearch: 'NCMNodeProperties.NodeGroup', colToHighlight: 'NodeGroup' }));
        config.searchMenuItems.push(new Ext.menu.CheckItem({ text: '@{R=NCM.Strings;K=WEBJS_VK_53;E=js}', group: 'TableToSearch', checked: false, hideOnClick: false, colToSearch: 'Nodes.MachineType', colToHighlight: 'MachineType' }));
        config.searchMenuItems.push(new Ext.menu.CheckItem({ text: '@{R=NCM.Strings;K=WEBJS_VK_52;E=js}', group: 'TableToSearch', checked: false, hideOnClick: false, colToSearch: 'Nodes.Vendor', colToHighlight: 'Vendor' }));
        config.searchMenuItems.push(new Ext.menu.CheckItem({ text: '@{R=NCM.Strings;K=WEBJS_VK_206;E=js}', group: 'TableToSearch', checked: false, hideOnClick: false, colToSearch: 'Nodes.IOSVersion', colToHighlight: 'IOSVersion' }));

        var mappingIndex = config.fieldMapping.length;

        Ext.each(cpColumnModel, function (column) {
            if (column.PropertyType.toUpperCase() == "SYSTEM.DATETIME") {
                config.columnModel.push({ header: column.PropertyName, width: 150, sortable: true, hidden: true, dataIndex: column.PropertyName, renderer: renderDateTime });
            }
            else {
                config.columnModel.push({ header: column.PropertyName, width: 150, sortable: true, hidden: true, dataIndex: column.PropertyName });
            }
            config.fieldMapping.push({ name: column.PropertyName, mapping: mappingIndex++ });
            if (column.PropertyType.toUpperCase() != "SYSTEM.DATETIME" && column.PropertyType.toUpperCase() != "SYSTEM.BOOLEAN") {
                config.searchMenuItems.push(new Ext.menu.CheckItem({ text: column.PropertyName, group: 'TableToSearch', checked: false, hideOnClick: false, colToSearch: String.format('OrionCustomProperties.{0}', column.PropertyName), colToHighlight: column.PropertyName }));
            }
        });
        return config;
    };

    var gridStateSaveHandler = function () {
        var columnModel = grid.getColumnModel();
        var sortInfo = grid.getStore().getSortState();

        var gridState = {};
        gridState.columnModel = SW.NCM.GridColumnsToJson(columnModel);
        gridState.sortColumn = sortInfo.field;
        gridState.sortDirection = sortInfo.direction;

        SW.NCM.callWebMethod('/Orion/NCM/Services/ConfigManagement.asmx/PersonalizeGridState', { prefix: ORION.prefix, gridState: gridState }, function () { });
    }

    var getDefaultSortInfoIfNeeded = function (sortColumn, sortDirection, fieldMapping) {
        var sortInfo = {};
        for (var i = 0; i < fieldMapping.length; i++) {
            if (fieldMapping[i].name == sortColumn) {
                sortInfo.sortColumn = sortColumn;
                sortInfo.sortDirection = sortDirection;
                return sortInfo;
            }
        }
        sortInfo.sortColumn = "Caption";
        sortInfo.sortDirection = "ASC";
        return sortInfo;
    }

    var replaceColumnNameIfNeeded = function (columnModel, cpColumnModel, oldColumnName, newColumnName) {
        var cpName = cpColumnModel.filter(function (cp) {
            if (cp.PropertyName == oldColumnName)
                return cp;
        });
        
        if (cpName.length == 0) {
            Ext.each(columnModel, function (column) {
                if (column.id == oldColumnName) {
                    column.id = newColumnName;
                }
            });
        }
    }

    return {
        init: function() {

            if ($("[id$='_ContentContainer']").length == 0)
                return false;

            $("#Grid").click(function(e) {
                var obj = $(e.target);

                if (obj.hasClass('ncm_SuggestedAction')) {
                    manageDeviceTemplateClick(obj);
                    return false;
                }
            });

            config = createConfig(SW.NCM.CPColumnModel);

            initSearch();

            initGrouping();

            initToolbar();

            gridStore = new ORION.WebServiceStore(
                "/Orion/NCM/Services/ConfigManagement.asmx/GetNodesPaged",
                config.fieldMapping,
                config.orderByColumn
            );
            var sortInfo = getDefaultSortInfoIfNeeded(
                SW.NCM.GridPersonalizationState.sortColumn || config.orderByColumn,
                SW.NCM.GridPersonalizationState.sortDirection || "ASC",
                config.fieldMapping);
            gridStore.setDefaultSort(sortInfo.sortColumn, sortInfo.sortDirection);

            pageSize = parseInt(ORION.Prefs.load('PageSize', '25'));

            topPageSizeBox = new Ext.form.NumberField({
                id: 'topPageSizeBox',
                enableKeyEvents: true,
                allowNegative: false,
                width: 30,
                allowBlank: false,
                minValue: 1,
                maxValue: 100,
                value: pageSize,
                listeners: {
                    scope: this,
                    'keydown': function(f, e) {
                        var k = e.getKey();
                        if (k == e.RETURN) {
                            e.stopEvent();
                            var value = parseInt(f.getValue());
                            if (!isNaN(value) && value > 0 && value <= 100) {
                                pageSize = value;
                                var topPagingToolbar = grid.getTopToolbar();
                                var bottomPagingToolbar = grid.getBottomToolbar();
                                if (topPagingToolbar.pageSize != pageSize) { // update page size only if it is different
                                    ORION.Prefs.save('PageSize', pageSize);
                                    topPagingToolbar.pageSize = pageSize;
                                    bottomPagingToolbar.pageSize = pageSize;
                                    bottomPageSizeBox.setValue(pageSize);

                                    var node = tree.getSelectionModel().getSelectedNode();
                                    var groupingQueryString = getGroupingQueryString('', node);
                                    loadGrid(groupingQueryString, searchTerm);
                                }
                            } else {
                                return;
                            }
                        }
                    },
                    'focus': function(field) {
                        field.el.dom.select();
                    },
                    'change': function(f, numbox, o) {
                        var value = parseInt(f.getValue());
                        if (!isNaN(value) && value > 0 && value <= 100) {
                            pageSize = value;
                            var topPagingToolbar = grid.getTopToolbar();
                            var bottomPagingToolbar = grid.getBottomToolbar();
                            if (topPagingToolbar.pageSize != pageSize) { // update page size only if it is different
                                ORION.Prefs.save('PageSize', pageSize);
                                topPagingToolbar.pageSize = pageSize;
                                bottomPagingToolbar.pageSize = pageSize;
                                bottomPageSizeBox.setValue(pageSize);

                                var node = tree.getSelectionModel().getSelectedNode();
                                var groupingQueryString = getGroupingQueryString('', node);
                                loadGrid(groupingQueryString, searchTerm);
                            }
                        } else {
                            return;
                        }
                    }
                }
            });

            bottomPageSizeBox = new Ext.form.NumberField({
                id: 'bottomPageSizeBox',
                enableKeyEvents: true,
                allowNegative: false,
                width: 30,
                allowBlank: false,
                minValue: 1,
                maxValue: 100,
                value: pageSize,
                listeners: {
                    scope: this,
                    'keydown': function(f, e) {
                        var k = e.getKey();
                        if (k == e.RETURN) {
                            e.stopEvent();
                            var value = parseInt(f.getValue());
                            if (!isNaN(value) && value > 0 && value <= 100) {
                                pageSize = value;
                                var topPagingToolbar = grid.getTopToolbar();
                                var bottomPagingToolbar = grid.getBottomToolbar();
                                if (bottomPagingToolbar.pageSize != pageSize
                                ) { // update page size only if it is different
                                    ORION.Prefs.save('PageSize', pageSize);
                                    topPagingToolbar.pageSize = pageSize;
                                    bottomPagingToolbar.pageSize = pageSize;
                                    topPageSizeBox.setValue(pageSize);

                                    var node = tree.getSelectionModel().getSelectedNode();
                                    var groupingQueryString = getGroupingQueryString('', node);
                                    loadGrid(groupingQueryString, searchTerm);
                                }
                            } else {
                                return;
                            }
                        }
                    },
                    'focus': function(field) {
                        field.el.dom.select();
                    },
                    'change': function(f, numbox, o) {
                        var value = parseInt(f.getValue());
                        if (!isNaN(value) && value > 0 && value <= 100) {
                            pageSize = value;
                            var topPagingToolbar = grid.getTopToolbar();
                            var bottomPagingToolbar = grid.getBottomToolbar();
                            if (bottomPagingToolbar.pageSize != pageSize) { // update page size only if it is different
                                ORION.Prefs.save('PageSize', pageSize);
                                topPagingToolbar.pageSize = pageSize;
                                bottomPagingToolbar.pageSize = pageSize;
                                topPageSizeBox.setValue(pageSize);

                                var node = tree.getSelectionModel().getSelectedNode();
                                var groupingQueryString = getGroupingQueryString('', node);
                                loadGrid(groupingQueryString, searchTerm);
                            }
                        } else {
                            return;
                        }
                    }
                }
            });

            var topPagingToolbar = new Ext.PagingToolbar({
                store: gridStore,
                pageSize: pageSize,
                displayInfo: true,
                displayMsg: '@{R=NCM.Strings;K=WEBJS_VK_41;E=js}',
                emptyMsg: '@{R=NCM.Strings;K=WEBJS_VK_42;E=js}',
                items: [
                    '-',
                    new Ext.form.Label({
                        text: '@{R=NCM.Strings;K=WEBJS_VK_300;E=js}',
                        style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;'
                    }), topPageSizeBox
                ],
                listeners: {
                    change: function() {
                        pagingToolbarChangeHandler(this);
                    }
                }
            });

            var bottomPagingToolbar = new Ext.PagingToolbar({
                store: gridStore,
                pageSize: pageSize,
                displayInfo: true,
                displayMsg: '@{R=NCM.Strings;K=WEBJS_VK_41;E=js}',
                emptyMsg: '@{R=NCM.Strings;K=WEBJS_VK_42;E=js}',
                items: [
                    '-',
                    new Ext.form.Label({
                        text: '@{R=NCM.Strings;K=WEBJS_VK_300;E=js}',
                        style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;'
                    }), bottomPageSizeBox
                ],
                listeners: {
                    change: function() {
                        pagingToolbarChangeHandler(this);
                    }
                }
            });

            var sm = new Ext.grid.RowSelectionModel({
                singleSelect: false,
                listeners: {
                    beforerowselect: function(sm, rowIndex, keepExisting, record) {
                        return !blockRowSelectionEvent;
                    },
                    rowselect: function(sm, index, record) {
                        var checkbox = $("input[recordId=" + record.id + "]");
                        if (checkbox.length > 0 && checkbox.is(":not(:checked)")) {
                            checkbox.attr("checked", "checked");
                            ToggleSelectRow(checkbox.get(0), record.id);
                        }
                        initBaselinesStatusTip(record);
                    }
                }
            });


            grid = new Ext.grid.GridPanel({
                id: 'gridControl',
                store: gridStore,
                selModel: sm,
                columns: config.columnModel,
                disableSelection: true,
                layout: 'fit',
                autoScroll: 'true',
                stateful: true,
                stateId: 'gridStateId',
                loadMask: true,
                height: 500,
                stripeRows: true,
                tbar: topPagingToolbar,
                bbar: bottomPagingToolbar
            });

            var gridColumnModel =
                SW.NCM.GetDefaultColumnModelIfNeeded(JSON.parse(SW.NCM.GridPersonalizationState.columnModel));
            replaceColumnNameIfNeeded(gridColumnModel, SW.NCM.CPColumnModel, "Name", "Caption");
            SW.NCM.PersonalizeGrid(grid, gridColumnModel);

            grid.on("statesave",
                function() {
                    gridStateSaveHandler();
                    initBaselinesStatusTips();
                });

            grid.on("cellmousedown",
                function(gr, rowIndex, columnIndex, e) {
                    if (columnIndex === 2) //Expand column index
                        blockRowSelectionEvent = true;
                    else
                        blockRowSelectionEvent = false;
                });

            grid.render('Grid');

            initTree();

            updateGridSelectionState();

            gridResize();
            treeResize();
            $(window).resize(function() {
                gridResize();
                treeResize();
                windowResize();
            });

            refreshTransfers();
            refreshBaselines();
        }
    };

}();

Ext.onReady(SW.NCM.ConfigManagement.init, SW.NCM.ConfigManagement);

SW.NCM.postToTarget = function(targetPage, data) {
    var formTag = String.format('<form method="post" target="_blank" action="{0}"></form>', targetPage);
    var form = $(formTag);

    for (var prop in data) {
        var inputTag = String.format('<input type="hidden" name="{0}" id="{0}" />', prop);
        var jsonData = JSON.stringify(data[prop]);
        form.append($(inputTag).val(jsonData));
    }

    $(form).appendTo('body').submit();
};

SW.NCM.postToIFrame = function(targetIFrame, targetPage, data) {
    var formTag = String.format('<form method="post" target="{0}" action="{1}"></form>', targetIFrame, targetPage);
    var form = $(formTag);

    for (var prop in data) {
        var inputTag = String.format('<input type="hidden" name="{0}" id="{0}" />', prop);
        var jsonData = JSON.stringify(data[prop]);
        form.append($(inputTag).val(jsonData));
    }

    $(form).appendTo('body').submit();
};

SW.NCM.callWebMethod = function(url, data, success, failure) {
    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: true,
        success: function(result) {
            if (success)
                success(result);
        },
        error: function(response) {
            if (failure)
                failure(response);
            else {
                alert(response);
            }
        }
    });
};
