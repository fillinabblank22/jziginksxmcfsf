if (!window.SW) window.SW = {};
if (!SW.vol) SW.vol = {};
if (!SW.CONST) SW.CONST = {};

SW.CONST.TAB = 9;
SW.CONST.CR = 13;

// -- footer
var uploadMultiNodeChecked = '';
var uploadSingleNodeChecked = '';
var editconfigNodeChecked = '';
var baselineconfigNodeChecked = '';
var configOldNodeChecked = '';
var skipRefresh = false;

var lastCheckedExEntity = '';
var lastCheckedExNode = '';

var selectedConfigName = '';
var isConfigEdited = false;

SW.ReflowFooter = function (e) {

    var winHeight = $(window).height();
    var scrollVis = document.body.scrollHeight > winHeight;
    var that = SW.ReflowFooter;

    if ((typeof (that.last) != "undefined") && that.last == scrollVis)
        return;

    that.last = scrollVis;

    var cp2 = $('#copyrightBar');

    if (that.last)
        cp2.css({ position: 'static', bottom: '' });
    else
        cp2.css({ position: 'absolute', bottom: '0px' });
};

function RefreshMultiCheckedNode() {
    uploadMultiNodeChecked = '';
}

function RefreshSingleCheckedNode() {
    uploadSingleNodeChecked = '';
}

function RefreshBaselineCheckedNode() {
    baselineconfigNodeChecked = '';
}

function DisableEnablebtnUploadBtn() {
    var txtArea = $(".lined")[0].value;
    if (!(/\S/.test(txtArea))) {
        $("#btnUpload").attr("hrefDisabled", $("#btnUpload").attr("href"));
        $("#btnUpload").removeAttr("href");
        $("#btnUpload").addClass("aspNetDisabled sw-btn-disabled");
    }
    else {
        $("#btnUpload").attr("href", $("#btnUpload").attr("hrefDisabled"));
        $("#btnUpload").removeAttr("hrefDisabled");
        $("#btnUpload").removeClass("aspNetDisabled sw-btn-disabled");
    }
}


var selectedNodeId = null;
function UltraWebTree_NodeSelectionChange(treeId, nodeId, newNodeId) {
    var node = igtree_getNodeById(newNodeId);
    var dataPath = node.getDataPath();
    if (dataPath != null && dataPath.substring(0, 1) == "C") {
        $(".tree-node-selected").removeClass();

        var newId = node.Id;
        $("#" + newId).addClass("tree-node-selected");
        selectedNodeId = newId;

        var loadingMask = new Ext.LoadMask(Ext.getBody(), { msg: "Loading..." });
        loadingMask.show();

        var configId = dataPath.replace("C:", "");

        var parentNode = node.getParent();
        if (parentNode != null) {
            parentNodeId = parentNode.getDataPath().replace("N:", "");
        }
        parentConfigId = configId;
        parentConfigTitle = node.getText();
        
        ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
            "GetConfig", { configID: configId },
            function (result) {
                
                isConfigBinary = result.IsBinary;

                if (isConfigBinary) {
                    $("#txtConfig").text("Binary Config");
                    $("#hfIsBinary").val(true);
                    DisableConfigTextTxt(true);
                    HideSaveToFileSection(true);

                } else {
                    $("#txtConfig").text(result.Config);
                    DisableConfigTextTxt(false);
                    HideSaveToFileSection(false);
                    EnableSaveToFileChk();
                }

                $("#lblConfigLabel").text(node.__getText());
                selectedConfigName = node.__getText();
                isConfigEdited = false;
                SetConfigStatus();
                DisableEnablebtnUploadBtn();

                loadingMask.hide();
            }
        );
    } 
}

function DisableConfigTextTxt(value) {
    $("#txtConfig").prop('disabled', value);
}

function HideSaveToFileSection(value) {
    $("#SaveToFileSection").prop('hidden', value);
}

function UltraWebTree_PreserveSelection() {
    if (selectedNodeId != null) {
        $("#" + selectedNodeId).addClass("tree-node-selected");
    }
}

function SetConfigStatus() {
    if (isConfigEdited) {
        $("#lblEditNotif").css("display", "inline");
    }
    else {
        $("#lblEditNotif").css("display", "none");
    }
}

function EnableSaveToFileChk() {
    $("#chkSaveToFile").prop('disabled', false);
}

function SetSelectedNodeName() {
    $("#lblConfigLabel").text(selectedConfigName);
}

function UltraWebTree_NodeChecked(treeId, nodeId, bChecked) {
    var selectedNode = igtree_getNodeById(nodeId);
    var childNodes = selectedNode.getChildNodes();
    var childCount = childNodes.length;
    var parentNode;

    if (!childNodeChecked) {
        for (var n = 0; n < childCount; n++) {
            if (childNodes[n].getChecked() != bChecked) {
                childNodes[n].setChecked(bChecked);
            }
        }
    }

    childNodeChecked = false;
    if (childNodes[0] == null && bChecked == false && selectedNode.getParent() != null) {
        parentNode = selectedNode.getParent();
        if (parentNode.getChecked() != false) {
            childNodeChecked = true;
            parentNode.setChecked(false);
        }
    }
}

function UltraWebTree_NodeCheckedDownloadConfig(treeId, nodeId, bChecked) {
    var tmpSkip = false;
    if (!skipRefresh) {
        skipRefresh = true;
        tmpSkip = true;
    }
    var selectedNode = igtree_getNodeById(nodeId);
    var childNodes = selectedNode.getChildNodes();
    var childCount = childNodes.length;
    var parentNode;

    if (!childNodeChecked) {
        for (var n = 0; n < childCount; n++) {
            if (childNodes[n].getChecked() != bChecked) {
                childNodes[n].setChecked(bChecked);
            }
        }
    }

    childNodeChecked = false;
    if (childNodes[0] == null && bChecked == false && selectedNode.getParent() != null) {
        parentNode = selectedNode.getParent();
        if (parentNode.getChecked() != false) {
            childNodeChecked = true;
            parentNode.setChecked(false);
        }
    }
    if (tmpSkip) {
        skipRefresh = false;
    }
}

//do not allow Orion to delete Array.prototype.indexOf
UltraWebTree_NodeCheckedDownloadConfig['fixed'] = 1;

function UltraWebTree_UploadMultiNodeChecked(treeId, nodeId, bChecked) {
    var node = igtree_getNodeById(nodeId);

    if (node.getDataPath().substring(0, 1) == "C") {
        if (bChecked == false) {
            uploadMultiNodeChecked = '';
            return;
        }
    }
    if (node.getDataPath().substring(0, 1) == "C") {
        if (uploadMultiNodeChecked != '') {
            var configNode = igtree_getNodeById(uploadMultiNodeChecked);
            configNode.setChecked(false);
        }
        uploadMultiNodeChecked = nodeId;
    }
}

function UltraWebTree_UploadSingleNodeChecked(treeId, nodeId, bChecked) {
    var node = igtree_getNodeById(nodeId);

    if (node.getDataPath().substring(0, 1) == "C") {
        if (bChecked == false) {
            uploadSingleNodeChecked = '';
            return;
        }
    }
    if (node.getDataPath().substring(0, 1) == "C") {
        if (uploadSingleNodeChecked != '') {
            var configNode = igtree_getNodeById(uploadSingleNodeChecked);
            configNode.setChecked(false);
        }
        uploadSingleNodeChecked = nodeId;
    }
}

function UltraWebTree_EditConfigsNodeChecked(treeId, nodeId, bChecked) {
    var node = igtree_getNodeById(nodeId);

    if (node.getDataPath().substring(0, 1) == "C") {
        if (bChecked == false) {
            editconfigNodeChecked = '';
            return;
        }
    }
    if (node.getDataPath().substring(0, 1) == "C") {
        if (editconfigNodeChecked != '') {
            var configNode = igtree_getNodeById(editconfigNodeChecked);
            if (configNode != null) {
                configOldNodeChecked = editconfigNodeChecked;
                configNode.setChecked(false);
            }
        }
        if (configOldNodeChecked != nodeId) {
            editconfigNodeChecked = nodeId;
        }
        configOldNodeChecked = '';
    }
}

function UltraWebTree_CompareToBaselineNodeChecked(treeId, nodeId, bChecked) {
    var node = igtree_getNodeById(nodeId);

    if (node.getDataPath().substring(0, 1) == "C") {
        if (bChecked == false) {
            baselineconfigNodeChecked = '';
            return;
        }
    }
    if (node.getDataPath().substring(0, 1) == "C") {
        if (baselineconfigNodeChecked != '') {
            var configNode = igtree_getNodeById(baselineconfigNodeChecked);
            configNode.setChecked(false);
        }
        baselineconfigNodeChecked = nodeId;
    }
}


//----------------config management buttons
function UltraWebTree_ConfigManagementNodeChecked(treeId, nodeId, bChecked) {

    var tree = igtree_getTreeById(treeId);
    var Nodes = tree.getNodes('all');
    var checkedNodesCount = 0;
    var childCount = Nodes.length;
    for (var n = 0; n < childCount; n++) {

        if (Nodes[n].hasCheckbox()) {
            if (Nodes[n].getChecked()) {
                checkedNodesCount = checkedNodesCount + 1;
            }
        }
    }

    var editButtonId = '#' + $('[id*="_EditConfig"]')[0].id;
    var deleteButtonId = '#' + $('[id*="_DeleteConfig"]')[0].id;
    var baselineButtonId = '#' + $('[id*="_setBaselineConfig"]')[0].id;

    if (checkedNodesCount == 0) {
        //disable all buttons              
        NCMDisableButton(editButtonId);
        NCMDisableButton(deleteButtonId);
        NCMDisableButton(baselineButtonId);

        AddDemoModeMessage(baselineButtonId, DemoMessageId_SetClearBaseline, false);
        AddDemoModeMessage(deleteButtonId, DemoMessageId_DeleteConfig, false);
    }
    if (checkedNodesCount == 1) {
        //enable all buttons         
        NCMEnableButton(editButtonId);
        NCMEnableButton(deleteButtonId);
        NCMEnableButton(baselineButtonId);

        AddDemoModeMessage(baselineButtonId, DemoMessageId_SetClearBaseline, true);
        AddDemoModeMessage(deleteButtonId, DemoMessageId_DeleteConfig, true);
    }

    if (checkedNodesCount > 1) {
        //disable edit    
        NCMDisableButton(editButtonId);
        NCMEnableButton(deleteButtonId);
        NCMEnableButton(baselineButtonId);

        AddDemoModeMessage(baselineButtonId, DemoMessageId_SetClearBaseline, true);
        AddDemoModeMessage(deleteButtonId, DemoMessageId_DeleteConfig, true);
    }
}

function NCMEnableButton(id) {
    $(id).removeClass('disabledbtn');
    $(id).addClass('enablebtn');
    $(id).removeAttr('onclick');
    $(id).removeAttr('disabled');
}

function NCMDisableButton(id) {
    $(id).removeClass('enablebtn');
    $(id).addClass('disabledbtn');
    $(id).attr('onclick', 'return false');
}

function AddDemoModeMessage(id, msgId, add) {
    if (IsDemoMode) {
        if (add) {
            $(id).attr('href', String.format('javascript:demoAction("{0}", this);', msgId));
        }
        else {
            $(id).removeAttr('href');
        }
    }
}

//-------------------

function CheckWriteToNVRAM(checked, checkNVRAMId) {
    if (checked) {
        var checkNVRAM = document.getElementById(checkNVRAMId);
        if (checkNVRAM) {
            checkNVRAM.checked = true;
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////
var childNodeChecked;
var ie = (document.all) ? true : false;


function RenderFileUpload(fileUploadId, imagePath) {
    $("#" + fileUploadId).change(function () {
        __doPostBack('', '');
    });

    $("#" + fileUploadId).filestyle({
        image: imagePath,
        imageheight: 22,
        imagewidth: 70,
        bgPosition: "right"
    });
}

function RenderFileUploadImage(fileUploadId, imagePath) {
    $("#" + fileUploadId).change(function () {
        __doPostBack('', '');
    });

    $("#" + fileUploadId).filestyle({
        image: imagePath,
        imageheight: 16,
        imagewidth: 70,
        bgPosition: "left"
    });
}

function UltraWebTree_Zebra(treeId, nodeId) {
    var items = $('table').filter(function () { return this.id.match(/nodetreecontrol/) }).find("div").filter(function () { if ($(this).parent().css('display') != 'none' && this.id.match(/^ctl*/)) return this; });
    var j = 0;
    $(items).each(function () { (j++ & 1) ? 0 : $(this).addClass("ZebraStripe"); });

}


function UltraWebTree_ConfigManagementInit(treeId, nodeId) {
    var items = $('table').filter(function () { return this.id.match(/nodetreecontrol/) }).find("div").filter(function () { if ($(this).parent().css('display') != 'none' && this.id.match(/^ctl*/)) return this; });
    var j = 0;
    $(items).each(function () { (j++ & 1) ? 0 : $(this).addClass("ZebraStripe"); });

    UltraWebTree_ConfigManagementNodeChecked(treeId, null, null);

}

function UltraWebTree_NodeDemandLoad(treeId, nodeId) {
    var node = igtree_getNodeById(nodeId);
    var tree = igtree_getTreeById(treeId);
    tree.Enabled = false;
    var currentNodeHtml = node.getText(true).trim();
    var margin_left = 20 * node.getLevel();

    if (currentNodeHtml.search('@{R=NCM.Strings;K=WEBJS_VK_04;E=js}') < 0) {
        node.setHtml(String.format("{0}<div class='Property' style='margin-left:{1}px;'>{2}</div>", currentNodeHtml, margin_left, '@{R=NCM.Strings;K=WEBJS_VK_04;E=js}'));
    }
}

function Redirect(url) {
    window.location = url;
}

// such approach used in cause of
// simple switch omits in IE
var t = '&"<>';
function EncodeHtml(s) {
    if (!s)
        return "";
    s += '';
    var es = '';
    for (var i = 0; i < s.length; i++) {
        switch (s.substring(i, i + 1)) {
            case t.substring(0, 1):
                es += '&amp;';
                break;
            case t.substring(1, 2):
                es += '&quot;';
                break;
            case t.substring(2, 3):
                es += '&lt;';
                break;
            case t.substring(3, 4):
                es += '&gt;';
                break;
            default:
                es += s.substring(i, i + 1);
                break;
        }
    }
    return es;
}

function EncodeValue(id) {
    var el = $get(id);
    if (el && el.value) {
        el.value = EncodeHtml(el.value);
    }
}

function getWindowHeight() {
    var windowHeight = 0;
    if (typeof (window.innerHeight) == 'number') {
        windowHeight = window.innerHeight;
    }
    else {
        if (document.documentElement && document.documentElement.clientHeight) {
            windowHeight = document.documentElement.clientHeight;
        }
        else {
            if (document.body && document.body.clientHeight) {
                windowHeight = document.body.clientHeight;
            }
        }
    }
    return windowHeight;
}

function SetHandCursor() {
    document.documentElement.style.cursor = 'pointer';
}

function SetDefaultCursor() {
    document.documentElement.style.cursor = 'default';
}

function UltraChart_MouseOver(this_ref, Row, Column, Value, RowLabel, ColumnLabel, EventName) {
    SetHandCursor();
}

function UltraChart_MouseOut(this_ref, Row, Column, Value, RowLabel, ColumnLabel, EventName) {

    SetDefaultCursor();
}

function UltraWebTree_ExEntityChecked(treeId, nodeId, bChecked) {
    if (bChecked == true) {
        var node = igtree_getNodeById(nodeId);
        if (node.getDataPath() == lastCheckedExEntity) {
            return;
        }

        lastCheckedExEntity = node.getDataPath();
        var parent = node.getParent();
        var childes = parent.getChildNodes();
        for (var childNumbers = 0; childNumbers < childes.length; childNumbers++) {
            if (childes[childNumbers].getDataPath().substring(0, 1) == "E" && childes[childNumbers].getDataPath() != node.getDataPath()) {
                childes[childNumbers].setChecked(false);
            }
        }
    }
}

function UltraWebTree_ExNodesChecked(treeId, nodeId, bChecked) {
    var tmpSkip = false;
    if (!skipRefresh) {
        skipRefresh = true;
        tmpSkip = true;
    }
    var selectedNode = igtree_getNodeById(nodeId);
    var childNodes = selectedNode.getChildNodes();
    var childCount = childNodes.length;
    var parentNode;

    if (!childNodeChecked) {
        for (var n = 0; n < childCount; n++) {
            if (childNodes[n].getChecked() != bChecked) {
                childNodes[n].setChecked(bChecked);
            }
        }
    }

    childNodeChecked = false;
    if (childNodes[0] == null && bChecked == false && selectedNode.getParent() != null) {
        parentNode = selectedNode.getParent();
        if (parentNode.getChecked() != false) {
            childNodeChecked = true;
            parentNode.setChecked(false);
        }
    }
    if (tmpSkip) {
        skipRefresh = false;
    }
}

function UltraWebTree_ExEntitiesChecked(treeId, nodeId, bChecked) {

    if (!skipRefresh) {
        var selectedNode = igtree_getNodeById(nodeId);
        var childNodes = selectedNode.getChildNodes();
        var childCount = childNodes.length;
        var parentNode;
        if (!childNodeChecked) {
            for (var n = 0; n < childCount; n++) {
                if (childNodes[n].hasCheckbox()) {
                    if (childNodes[n].getChecked() != bChecked) {
                        childNodes[n].setChecked(bChecked);
                    }
                }
            }

        }
        childNodeChecked = false;

        var hasChilds = false;

        if (childNodes[0] == null) {
            hasChilds = false;
        }
        else {
            if (childNodes[0].getDataPath() == null) {
                hasChilds = false;
            }
            else {
                hasChilds = true;
            }

        }
        if (hasChilds == false && bChecked == false && selectedNode.getParent() != null) {
            parentNode = selectedNode.getParent();
            if (parentNode.hasCheckbox()) {
                if (parentNode.getChecked() != false) {
                    childNodeChecked = true;
                    parentNode.setChecked(false);
                }
                parentNode = parentNode.getParent(); //uncheck on group name level if exist
                if (parentNode != null) {

                    if (parentNode.hasCheckbox()) {
                        if (parentNode.getChecked() != false) {
                            skipRefresh = true;
                            parentNode.setChecked(false);
                            skipRefresh = false;
                        }
                    }
                }

            }
        }
    }
}

function UltraWebTree_ExNodeChecked(treeId, nodeId, bChecked) {
    var node = igtree_getNodeById(nodeId);

    if (node.getDataPath().substring(0, 1) == "N") {
        if (bChecked == false) {
            lastCheckedExNode = '';
            return;
        }
    }
    if (node.getDataPath().substring(0, 1) == "N") {
        if (lastCheckedExNode != '') {
            var checkedNode = igtree_getNodeById(lastCheckedExNode);
            checkedNode.setChecked(false);
        }
        lastCheckedExNode = nodeId;
    }
}

function RefreshExNodeChecked() {
    lastCheckedExNode = '';
}

function ValidateTags(ctl, args) {
    var valid = true;
    var arr = args.Value.split(",");

    for (var i = 0; i < arr.length; i++) {
        var tag = arr[i].replace(/(^\s+)|(\s+$)/g, "");
        if (tag.length > 250) {
            valid = false;
        }
    }
    args.IsValid = valid;
}

//--------------- Three-level grouping Scripts --------------------------------------------------

//<Check un check DownloadConfigsControl>
var SkipEvent = false;
function UltraWebTree_TheeLevelNodeCheckedDownloadConfig(treeId, nodeId, bChecked) {
    var selectedNode = igtree_getNodeById(nodeId);
    if (SkipEvent == false) {
        UpdateChilds(selectedNode, bChecked);
    }
    if (bChecked == false) {
        UpdateParent(selectedNode);
    }
}

function UpdateParent(node) {
    if (node.getParent() != null && node.getParent().getChecked() != false) {
        SkipEvent = true;
        node.getParent().setChecked(false);
    }
    else {
        SkipEvent = false;
    }
}

function UpdateChilds(node, bcheck) {
    var childNodes = node.getChildNodes();
    var childCount = childNodes.length;
    for (var n = 0; n < childCount; n++) {
        childNodes[n].setChecked(bcheck);
    }
}

function UltraWebTree_DemandLoad(treeId, nodeId) {
    var tree = igtree_getTreeById(treeId);
    tree.Enabled = false;
    var node = igtree_getNodeById(nodeId);
    var nodeText = node.getText();
    var marginLeft = 25 * (node.getLevel() + 1);
    if (nodeText.search('@{R=NCM.Strings;K=WEBJS_VK_04;E=js}') < 0) {
        node.setHtml(String.format("{0}<div class='tree-node-loading-img' style='margin-left:{1}px;'><span class='tree-node-loading-text'>{2}</span></div>", nodeText, marginLeft, '@{R=NCM.Strings;K=WEBJS_VK_04;E=js}'));
    }
}