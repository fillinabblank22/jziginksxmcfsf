﻿$(function() {
    if(window.isBreadCrumbInitialized)
    {
       // if these events are handled, probably it is done by PluggableDropDownSiteMap.js
       return;
    }
    
    var isClicked = false;
    var submenuHeight = 0;
    var $iframe = $('<iframe src="" class="tooltip-iframe" scroll="none" frameborder="0"></iframe>');
    var $customizeDiv = $('<div id="customizeDialog" style="display:none;"> \
									<div id="inside"> <table width="100%"> <tr><td id="customizeCaption"> </td> </tr> <tr> <td> <select id="groupByProperty" style="width: 60%;"> \
														 </td> </tr> </table> </div> \
									<div class="sw-btn-bar-wizard" style="margin-right:5px;">' +
										SW.Core.Widgets.Button('@{R=Core.Strings;K=CommonButtonType_Submit; E=js}', { type: 'primary', id: 'customizeSubmit' }) +
								    SW.Core.Widgets.Button('@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}', { type: 'secondary', id: 'customizeCancel' }) +
									'</div> \
								</div>');
    $('body').append($customizeDiv);
    var $cancelButton = $customizeDiv.find("#customizeCancel");
    var $submitButton = $customizeDiv.find("#customizeSubmit");
    var $dropDownProp = $customizeDiv.find("#groupByProperty");
    var $caption = $customizeDiv.find("#customizeCaption");
    var customizeMode = "Nodes";

    $("img.bc_itemimage").hover(function() {
        $(this).css("cursor", "pointer");
        $(this).css("_cursor", "hand");

        $(this).attr({ src: "/Orion/images/breadcrumb_dd_bg.gif" });
        $(this).parent().addClass("bc_itemselected");
        $(this).prev("a").addClass("bc_refselected");
    },
    function() {
        $(this).css("cursor", "default");
        $(this).css("_cursor", "default");

        if (!isClicked) {
            $(this).attr({ src: "/Orion/images/breadcrumb_arrow.gif" });
            $(this).parent().removeClass("bc_itemselected");
            $(this).prev("a").removeClass("bc_refselected");
        }
    });

    $("img.bc_itemimage").click(function(e) {
        e.stopPropagation();

        var theImage = $(this);

        var left = theImage.parent().position().left + 20;
        var top = theImage.parent().position().top + 21;
        var submenu = theImage.next("ul.bc_submenu");
        var defaultHeight = 305;

        $("ul.bc_submenu").each(function(idx) {
            if (this.id != submenu.attr("id")) {
                $(this).hide();
                $(this).prev("img.bc_itemimage").attr({ src: "/Orion/images/breadcrumb_arrow.gif" });
                $(this).css({ overflow: 'auto' }).css("height", "auto");
            }
        });

        if (submenu.is(":visible")) {
            theImage.attr({ src: "/Orion/images/breadcrumb_arrow.gif" });
            theImage.parent().removeClass("bc_itemselected");
            theImage.prev("a").removeClass("bc_refselected");
            submenu.hide();
            if ($.browser.msie && ($.browser.version == "6.0"))
                $iframe.css('display', 'none');
            submenu.css({ overflow: 'auto' }).css("height", "auto");
            isClicked = false;
        }
        else {
            theImage.attr({ src: "/Orion/images/breadcrumb_dd_bg.gif" });
            theImage.parent().addClass("bc_itemselected");
            theImage.prev("a").addClass("bc_refselected");
            isClicked = true;
            submenuHeight = submenu.height();
            if (submenu.height() > defaultHeight) {
                submenu.css({ overflow: 'hidden' }).css("height", defaultHeight);

                submenu.mousemove(function(e) {
                    //var top = (e.pageY - submenu.offset().top) * (submenuHeight - defaultHeight) / defaultHeight;
                    var delta = (submenuHeight - defaultHeight);
                    var topY = (e.pageY - submenu.offset().top);
                    var top = delta / (1 + delta * Math.exp((-1 / 18) * topY));
                    submenu.scrollTop(top);
                });
            }

            if ($.browser.msie && ($.browser.version == "6.0")) {
                submenu.after($iframe);
                $iframe.css('display', 'block');
                $iframe.css({
                    width: submenu.width() + 5,
                    height: submenu.height() + 5,
                    top: top,
                    left: left
                });
            }
            submenu.css("position", "absolute").css("top", top).css("left", left).show();
        }
    });

    $(this).click(function() {
        $("li.bc_item").removeClass("bc_itemselected");
        $("a").removeClass("bc_refselected");
        if ($.browser.msie && ($.browser.version == "6.0"))
            $iframe.css('display', 'none');
        $("ul.bc_submenu").hide();
        $("ul.bc_submenu").css({ overflow: 'auto' }).css("height", "auto");
        $("img.bc_itemimage").attr({ src: "/Orion/images/breadcrumb_arrow.gif" });

        isClicked = false;
    });

    setStyles = function() {
        $("li.bc_item").removeClass("bc_itemselected");
        $("a").removeClass("bc_refselected");
        if ($.browser.msie && ($.browser.version == "6.0"))
            $iframe.css('display', 'none');
        $("ul.bc_submenu").hide();
        $("ul.bc_submenu").css({ overflow: 'auto' }).css("height", "auto");
        $("img.bc_itemimage").attr({ src: "/Orion/images/breadcrumb_arrow.gif" });

        isClicked = false;
    };
    showCustomizeDialog = function(objectName) {
        $dropDownProp.children().remove();
        customizeMode = objectName;
        var objectTitle;
        switch (objectName) {
            case 'CONFIG':
                $caption[0].innerHTML = "<b>" + "@{R=NCM.Strings;K=WEBJS_VK_155; E=js}" + "</b> <br>";
                objectTitle = "@{R=NCM.Strings;K=WEBJS_VK_156; E=js}";
                break;
            case 'POLICYREPORT':
                $caption[0].innerHTML = "<b>" + "@{R=NCM.Strings;K=WEBJS_VK_157; E=js}" + "</b> <br>";
                objectTitle = "@{R=NCM.Strings;K=WEBJS_VK_158; E=js}";
                break;
            case 'INVENTORYREPORT':
                $caption[0].innerHTML = "<b>" + "@{R=NCM.Strings;K=WEBJS_VK_159; E=js}" + "</b> <br>";
                objectTitle = "@{R=NCM.Strings;K=WEBJS_VK_160; E=js}";
                break;
            default:
                objectTitle = "@{R=NCM.Strings;K=WEBJS_VK_150; E=js}";
                break;
        }

        $.ajax({
            type: 'Post',
            url: '/Orion/NCM/Services/CustomizeBreadcrumb.asmx/GetListOfOptions',
            data: "{'objectName': '" + objectName + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(result) {
                $dropDownProp.append(result.d);
                var dialog = $customizeDiv.dialog({
                    width: 480, modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: objectTitle
                });
                dialog.show();
            },
            error: function(error) {
                $dropDownProp.append("<option value=''>[All " + objectName + "]</option>");
                var dialog = $customizeDiv.dialog({
                    width: 480, modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: objectTitle
                });
                dialog.show();
            }
        });
    };

    $("#customizeCONFIG").click(function() {
        showCustomizeDialog("CONFIG");
        setStyles();
        return false;
    });

    $("#customizePOLICYREPORT").click(function() {
        showCustomizeDialog("POLICYREPORT");
        setStyles();
        return false;
    });

    $("#customizeINVENTORYREPORT").click(function() {
        showCustomizeDialog("INVENTORYREPORT");
        setStyles();
        return false;
    });

    $cancelButton.click(function() {
        $customizeDiv.dialog("close");
    });

    $submitButton.click(function() {
        $.ajax({
            type: 'Post',
            url: '/Orion/NCM/Services/CustomizeBreadcrumb.asmx/SaveBcCustomization',
            data: "{'customizationValue': '" + $dropDownProp.val() + "', 'objectName': '" + customizeMode + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(){
                     location.reload();
                     $customizeDiv.dialog("close");
                    }
        });
        
        
    });
});
