﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true"
    CodeFile="TransferStatus.aspx.cs" Inherits="Orion_NCM_TransferStatus" %>

<%@ Register TagPrefix="ncm" Src="~/Orion/NCM/Controls/IconNCMSettings.ascx" TagName="IconNCMSettings" %>
<%@ Register TagPrefix="orion" Src="~/Orion/Controls/IconHelpButton.ascx" TagName="IconHelpButton" %>
<%@ Register TagPrefix="ncm1" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register TagPrefix="ncm" Src="~/Orion/NCM/NavigationTabBar.ascx" TagName="NavigationTabBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmHeadPlaceHolder" runat="Server">
    
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/PolicyReports/PolicyReport.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />

    <ncm1:ExtLocalDateTimePattern ID="ExtLocalDateTimePattern1" runat="server"/>  
    <orion:Include ID="Include1" runat="server"  Framework="Ext" FrameworkVersion="3.4" />
     
    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script type="text/javascript" src="/Orion/NCM/TransferStatusViewer.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippetsHelper.js"></script>
     <script type="text/javascript">
         SW.NCM.NCMAccountRole = '<%=NCMAccountRole %>';
         SW.NCM.AllowNodeManagement = <%=AllowNodeManagement.ToString().ToLowerInvariant() %>;
         SW.NCM.IsDemoServer = <%=SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer.ToString().ToLowerInvariant() %>;
         SW.NCM.CloseWindowHandlerUrl = '<%=CloseWindowHandlerUrl %>';
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="ncmHelpIconPlaceHolder" runat="Server">
    <ncm:IconNCMSettings ID="IconNCMSettings1" runat="server" />
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMWebCMTransferStatus" />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ncmMainContentPlaceHolder" runat="Server">
    <asp:Panel id="ContentContainer" runat="server">
        <div style="padding: 10px;">
            <ncm:NavigationTabBar ID="NavigationTabBar1" runat="server" />
            <div id="tabPanel" class="tab-top">
                <table cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td id="gridPH">
                            <div id="Grid" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="scriptResultsDlg" class="x-hidden">
               <div id="scriptResultsBody" class="x-panel-body" style="height: 96%; height: calc(100% - 20px); padding: 10px;">
                   <textarea id="scriptResultsTextArea" style="width: 100%; height: 100%; padding: 5px;" readonly="true"></textarea>
               </div>
            </div>
        </div>
    </asp:Panel>
    <div style="padding: 10px;" id="divError" runat="server" />
</asp:Content>
