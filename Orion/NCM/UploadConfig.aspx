﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true"
    CodeFile="UploadConfig.aspx.cs" Inherits="Orion_NCM_UploadConfig" EnableViewState="true" %>

<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Controls"
    Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.PolicyReportsManagement.Controls"
    Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" Assembly="OrionWeb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmHeadPlaceHolder" runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
    <script src="JavaScript/main.js" type="text/javascript"></script>
    <link rel="Stylesheet" type="text/css" href="/Orion/NCM/Resources/ConfigSnippets/jquery-linedtextarea.css" />
    <script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/jquery-linedtextarea.js"></script>
    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <link href="styles/NCMResources.css" rel="stylesheet" type="text/css" />
    <link href="Admin/PolicyReports/PolicyReport.css" rel="stylesheet" />

    <style type="text/css">
        .ParentNode
        {
            background-color: transparent !important;
        }
        
        .linedwrap
        {
            border: 1px solid #DFDED7 !important;
        }
        
        .x-form-field-wrap
        {
            margin-bottom: 15px;
            margin-left: 15px;
        }
        
        
        #lblConfigLabel
        {
            font-weight: bold;
        }
        
        .FilterMenu select
        {
            width: 100%;
        }
        .x-combo-list-item
        {
            min-height: 16px;
        }
        
        .FilterMenu
        {
            width: 100%;
            margin-bottom: 10px;
        }
        
        div.FilterMenu
        {
        }
        
        .FilterMenu .GroupItem
        {
            margin-left: 10px;
            margin-right: 0%;
            padding-top: 10px;
            padding-bottom: 10px;
            width: 95%;
        }
        .FilterMenu table
        {
            table-layout: fixed;
            width: 100%;
        }
        
        .node-config:hover
        {
            cursor: pointer;
        }
        
        .tree-node-selected span
        {
            font-weight: bold;
        }
        
        .tree-node-selected
        {
            background-image: url('/Orion/Nodes/images/background/left_selection_gradient.gif');
            background-color: #97d6ff !important;
            background-repeat: repeat-x;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" runat="Server">
    <%=Page.Title %>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ncmExportToPDFPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ncmCustomizePagePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="ncmHelpIconPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="ncmMainContentPlaceHolder" runat="Server">
    <asp:UpdatePanel runat="server" ID="updatePanelApplNodes" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="width: 98%; padding-left: 1%; padding-right: 1%; padding-bottom: 30px;">
                <%--Page Header--%>
                <div>
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <div id="selectedNodes" style="width: 160px; margin-top: 10px; margin-bottom: 10px;">
                                    <ncm:SelectNodes ID="selectNodesCntrl" runat="server" NodeSelectionText="<%$ Resources: NCMWebContent, WEBDATA_IA_07 %>"
                                        ShowSelectNodesButton="false" ShowDeleteNodeButton="false" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span>
                                    <%= Resources.NCMWebContent.WEBDATA_IA_05 %></span>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </div>
                <%--Main Content--%>
                <div>
                    <table style="width: 100%">
                        <tr>
                            <td style="vertical-align: top; width: 350px;">
                                <%--Select config to upload--%>
                                <div id="leftSide" style="border: 1px solid #DFDED7; height: 758px; vertical-align: top;">
                                    <%--Filter panel--%>
                                    <table cellpadding="0" cellspacing="0" style="height: 100%; vertical-align: top;">
                                        <tr style="vertical-align: top; height: 12%;">
                                            <td>
                                                <div id="filterContent" class="ncm_GroupSection x-panel-header x-toolbar x-small-editor">
                                                    <div>
                                                        <label>
                                                            <%= Resources.NCMWebContent.WEBDATA_VK_80%>
                                                        </label>
                                                    </div>
                                                    <div>
                                                        <asp:DropDownList runat="server" ID="ddlGroupFirst" ClientIDMode="Static" AutoPostBack="true"
                                                            OnSelectedIndexChanged="ddlGroup_SelectedIndexChanged" CssClass="FilterMenu">
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div>
                                                        <asp:DropDownList runat="server" ID="ddlGroupSecond" ClientIDMode="Static" AutoPostBack="true"
                                                            OnSelectedIndexChanged="ddlGroup_SelectedIndexChanged" CssClass="FilterMenu"
                                                            Visible="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div style="white-space: 100%">
                                                        <asp:DropDownList runat="server" ID="ddlGroupThird" ClientIDMode="Static" AutoPostBack="true"
                                                            OnSelectedIndexChanged="ddlGroup_SelectedIndexChanged" CssClass="FilterMenu"
                                                            Visible="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="vertical-align: top; height: 88%;">
                                            <td>
                                                <div id="treeBody" style="overflow: auto; padding: 5px; width: 340px;">
                                                    <asp:Panel ID="panelConfigTree" runat="server">
                                                    </asp:Panel>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td id="tdRightSide" style="padding-left: 15px; width: 100%; height: 750px; ">
                                <%--Config table--%>
                                <div>
                                    <div id="tableHeader" style="height: 40px; background-color: rgb(235, 236, 237);
                                        width: 100%; margin-bottom: 0px" class="x-panel-header">
                                        <div style="margin-top: 0px; padding-top: 11px; padding-left: 11px;">
                                            <label>
                                                <%=Resources.NCMWebContent.WEBDATA_VM_85%>
                                            </label>
                                            <asp:Label ID="lblConfigLabel" ClientIDMode="Static" runat="server"></asp:Label>
                                            <label id="lblEditNotif" style="color: Red; font-weight: bold; display: none;">
                                                - <%=Resources.NCMWebContent.WEBDATA_IA_30%>
                                            </label>
                                        </div>
                                    </div>
                                    <div style="width: 100%; height: 100%;">
                                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="hfIsBinary" Value="false"/>
                                        <textarea class="lined"  ClientIDMode="Static" cols="1" rows="50" runat="server" style="width: 100%;
                                            height: 702px;" id="txtConfig" runat="server"></textarea>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td style="padding-left:15px;">
                                <table style="width: 100%; margin-top: 5px;">
                                    <tr>
                                        <td>
                                            <div id="AdvancedPanelControl">
                                                <div id="AdvancedPanelContentLink">
                                                    <a style="display:block;cursor:pointer;">
                                                        <image id="imgExpand" src="/Orion/NCM/Resources/images/Button.Expand.gif" />
                                                        <image id="imgCollapse" style="display: none" src="/Orion/NCM/Resources/images/Button.Collapse.gif" />
                                                        <span style="text-decoration: underline; margin-left:-3px;"><%= Resources.NCMWebContent.WEBDATA_IA_02 %></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                        <td style="float:right;padding-right: 6%;">
                                            <div>
                                                <orion:LocalizableButton runat="server" ID="btnUpload" ClientIDMode="Static" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_IA_01 %>" DisplayType="Primary" OnClientClick="uploadClick(); return false;" CausesValidation="false" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <div>
                                    <table id="AdvancedPanelContent" style="margin-top:10px;display:none;">
                                        <tr>
                                            <td id="SaveToFileSection" style="padding-right: 20px;white-space:nowrap;">
                                                <asp:CheckBox ID="chkSaveToFile" ClientIDMode="Static" Enabled="false" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_IA_04 %>" />
                                            </td>
                                            <td style="padding-right:20px;white-space:nowrap;">
                                                <asp:Panel ID="pnlAdvancedConfig" runat="server">
                                                    <asp:Label ID="lblConfigType" Text="Config types:" runat="server" Visible="false"></asp:Label>
                                                </asp:Panel>
                                                <asp:CheckBox ID="chkWritetoNVRAM" runat="server" onclick="writetoNVRAMClick(this);" Text="<%$ Resources: NCMWebContent, WEBDATA_IA_03 %>" />
                                            </td>
                                            <td style="white-space:nowrap;">
                                                <asp:CheckBox ID="chkRebootDevice" runat="server" onclick="rebootDeviceClick(this);" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_47 %>" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlGroupFirst" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlGroupSecond" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlGroupThird" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript" src="JavaScript/main.js?v=1.0"></script>
    <script type="text/javascript">

        NcmNodeIds = <%=NcmNodeIds %>;
        ShowApprovalRequestButton = <%=SolarWinds.NCMModule.Web.Resources.ConfigChangeApprovalHelper.ShowSendApprovalRequestButton.ToString().ToLowerInvariant() %>;
        CloseWindowHandlerUrl = '<%=CloseWindowHandlerUrl %>';
        IsMultipleConfigTypeUpload = <%=EnableMultipleConfigTypeUpload.ToString().ToLowerInvariant() %>;
        IsIndirectTransferProtocol = <%=EnableConfigTypeDropdownForIndirectTransferProtocol.ToString().ToLowerInvariant() %>;

        var isConfigBinary; 

        var parentNodeId;
        var parentConfigId;
        var parentConfigTitle;

        $(document).ready(function() {
            if (!$(".linedwrap").length) {
                $(".lined").linedtextarea(
                {
                    selectedLine: -1
                });
            }
        });

        var uploadClick = function() {
            var config =  isConfigBinary ? "" : $("#txtConfig").val();

            if(ShowApprovalRequestButton) {
                var reboot = $("[id$='_chkRebootDevice']").prop("checked");
                var requestType = '<%=SolarWinds.NCM.Contracts.InformationService.NCMApprovalTicket.TYPE_UPLOAD_CONFIG %>';
                var guid = newGuid();
                
                if (!isConfigBinary) {
                    var cloneConfig = $("#chkSaveToFile").prop("checked");
                    if (cloneConfig) {
                        internalCloneConfig(parentConfigId, parentConfigTitle, config);
                    }
                }
                
                internalSendRequestForApproval(NcmNodeIds, parentConfigId, getConfigType(), reboot, requestType, config, guid);
            }
            else {
                internalGetNCMNodeIdsForTransfer(NcmNodeIds, function (results) {
                    var ncmNodeIdsForTransfer = results.ncmNodeIdsForTransfer;
                    var result = results.error;
                    if (result != null && result.Error) {
                        errorHandler('<%= Resources.NCMWebContent.WEBDATA_VK_121 %>', result, function () {
                            if (ncmNodeIdsForTransfer.length > 0) {
                                var msg = String.format('<%= Resources.NCMWebContent.WEBDATA_IC_55 %>', ncmNodeIdsForTransfer.length);
                                
                                var isDifferentVendors = differentVendors(parentNodeId, ncmNodeIdsForTransfer);
                                if(isDifferentVendors) {
                                    msg += '</br></br>&bull;&nbsp;' + '<%= Resources.NCMWebContent.WEBDATA_IC_49 %>';
                                }
                                var isDifferentTransferTypes = differentTransferTypes(ncmNodeIdsForTransfer);
                                if(isDifferentTransferTypes) {
                                    msg += '</br></br>&bull;&nbsp;' + '<%= Resources.NCMWebContent.WEBDATA_IC_50 %>';
                                }
                                Ext.Msg.confirm('<%= Resources.NCMWebContent.WEBDATA_VK_121 %>',
                                    msg,
                                    function (btn, text) {
                                        if (btn == 'yes') {
                                            var reboot = $("[id$='_chkRebootDevice']").prop("checked");

                                            if (!isConfigBinary) {
                                               
                                                var cloneConfig = $("#chkSaveToFile").prop("checked");
                                                if (cloneConfig) {
                                                    internalCloneConfig(parentConfigId, parentConfigTitle, config);
                                                }
                                            } 

                                            internalUploadConfig(ncmNodeIdsForTransfer, parentConfigId, getConfigType(), config, reboot);
                                        }
                                    });
                            }
                        });
                    }
                    else {
                        var msg = String.format('<%= Resources.NCMWebContent.WEBDATA_IC_55 %>', ncmNodeIdsForTransfer.length);
                                              
                        var isDifferentVendors = differentVendors(parentNodeId, ncmNodeIdsForTransfer);
                        if(isDifferentVendors) {
                            msg += '</br></br>&bull;&nbsp;' + '<%= Resources.NCMWebContent.WEBDATA_IC_49 %>';
                        }
                        var isDifferentTransferTypes = differentTransferTypes(ncmNodeIdsForTransfer);
                        if(isDifferentTransferTypes) {
                            msg += '</br></br>&bull;&nbsp;' + '<%= Resources.NCMWebContent.WEBDATA_IC_50 %>';
                        }
                        Ext.Msg.confirm('<%= Resources.NCMWebContent.WEBDATA_VK_121 %>',
                            msg,
                            function (btn, text) {
                                if (btn == 'yes') {
                                    var reboot = $("[id$='_chkRebootDevice']").prop("checked");

                                    if (!isConfigBinary) {
                                        var cloneConfig = $("#chkSaveToFile").prop("checked");
                                        if (cloneConfig) {
                                            internalCloneConfig(parentConfigId, parentConfigTitle, config);
                                        }
                                    }

                                    internalUploadConfig(ncmNodeIdsForTransfer, parentConfigId, getConfigType(), config, reboot);
                                }
                            });
                    }
                });
            }
        }

        var getConfigType = function() {
            var configType;
            if(IsMultipleConfigTypeUpload || IsIndirectTransferProtocol) {
                configType = $("[id$='_ddlConfigTypes']").val();
            }
            
            else {
                configType = $("[id$='_chkWritetoNVRAM']").prop("checked") ? "Startup" : "Running";
            }
            return configType;
        }

        var internalGetNCMNodeIdsForTransfer = function (ncmNodeIds, onSuccess) {
            ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
                "GetNCMNodeIdsForTransferFromList", { ncmNodeIds : ncmNodeIds },
                function (result) {
                    onSuccess(result);
                }
            );
        }

        var internalSendRequestForApproval = function (nodeIds, configId, configType, reboot, requestType, config, guid) {
            if (!configId)
                configId = null;
            var waitMsg = Ext.Msg.wait('<%= Resources.NCMWebContent.WEBDATA_VK_941 %>');
            ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
                "SendRequestForApproval", { nodeIds: nodeIds, configType: configType, reboot: reboot, requestType: requestType, script: config, guidId: guid, configId: configId },
                function (result) {
                    waitMsg.hide();
                    errorHandler('<%= Resources.NCMWebContent.WEBDATA_VK_942 %>', result);
                    if (result == null) {
                        window.parent.iframeContainer.location.href = String.format('/Orion/NCM/Admin/ConfigChangeApproval/ViewApprovalRequest.aspx?printable=true&GuidID={0}&NCMReturnURL={1}', guid, CloseWindowHandlerUrl);
                        window.parent.Ext.getCmp('window').setTitle('<%= Resources.NCMWebContent.WEBDATA_IC_233 %>');
                    }
                }
            );
        }

        var internalUploadConfig = function (nodeIds, configId, configType, config, reboot) {
            debugger;
            if (!configId)
                configId = '';
            var waitMsg = Ext.Msg.wait('<%= Resources.NCMWebContent.WEBDATA_VK_939 %>');

            ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
                "UploadConfig", { nodeIds: nodeIds, configType: configType, config: config, reboot: reboot, configId: configId },
                function (result) {
                    waitMsg.hide();
                    errorHandler('<%= Resources.NCMWebContent.WEBDATA_VK_940 %>', result);
                    if (result == null) {
                        window.location = "/Orion/NCM/CloseWindowHandler.aspx?reloadGridStore=yes";
                    }
                }
            );
        }

        var internalCloneConfig = function (configId, title, config) {
            ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
                "CloneConfig", { configId: configId, title: title, config: config },
                function () { }
            );
        }

        var errorHandler = function (source, result, func) {
            if (result != null && result.Error) {
                Ext.Msg.show({
                    title: source,
                    msg: result.Msg,
                    minWidth: 500,
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    fn: func
                });
            }
        }

        var newGuid = function () {
            try {
                return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, function (c) {
                    (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
                });
            } catch (e) {
                return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                    var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
                    return v.toString(16);
                });
            }
        }

        var differentVendors = function (parentNodeId, ncmNodeIds) {
            if(parentNodeId) {
                var value;
                callWebMethod('/Orion/NCM/Services/ConfigManagement.asmx/IsDifferentVendors', { parentNodeId: parentNodeId, ncmNodeIds: ncmNodeIds }, function (result) { value = result.d; return; });
                return value;
            }
            else {
                return false;
            }
        }

        var differentTransferTypes = function (ncmNodeIds) {
            if(ncmNodeIds.length == 1) {
                return false;
            }
            else {
                var value;
                callWebMethod('/Orion/NCM/Services/ConfigManagement.asmx/IsDifferentTransferTypes', { ncmNodeIds: ncmNodeIds }, function (result) { value = result.d; return; });
                return value;
            }
        }

        var callWebMethod = function (url, data, success, failure) {
            $.ajax({
                type: "POST",
                url: url,
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: "json",
                async: true,
                success: function (result) {
                    if (success)
                        success(result);
                },
                error: function (response) {
                    if (failure)
                        failure(response);
                    else {
                        alert(response);
                    }
                }
            });
        }

        var writetoNVRAMClick = function (that) {
            if (that.checked) {
                Ext.Msg.show({
                    title: '<%= Resources.NCMWebContent.WEBDATA_VK_935 %>',
                    msg: '<%= Resources.NCMWebContent.WEBDATA_IC_47 %>',
                    minWidth: 500,
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.WARNING
                });
            }
        }

        var rebootDeviceClick = function (that) {
            if (that.checked) {
                Ext.Msg.show({
                    title: '<%= Resources.NCMWebContent.WEBDATA_VK_935 %>',
                    msg: IsMultipleConfigTypeUpload || IsIndirectTransferProtocol ?
                        '<%= Resources.NCMWebContent.WEBDATA_VK_936 %>' :
                        $("[id$='_chkWritetoNVRAM']").prop("checked") ? '<%= Resources.NCMWebContent.WEBDATA_VK_936 %>' : '<%= Resources.NCMWebContent.WEBDATA_IC_48 %>',
                    minWidth: 500,
                    buttons: {
                        ok: '<%= Resources.NCMWebContent.WEBDATA_VK_937 %>',
                        cancel: '<%= Resources.NCMWebContent.WEBDATA_VK_24 %>',
                    },
                    icon: Ext.MessageBox.WARNING,
                    fn: function (btn) {
                        if (btn == "ok") {
                            if(!IsMultipleConfigTypeUpload || !IsIndirectTransferProtocol) {
                                $("[id$='_chkWritetoNVRAM']").prop("checked", true);
                            }
                        }
                        if (btn == "cancel") {
                            that.checked = false;
                            if(!IsMultipleConfigTypeUpload || !IsIndirectTransferProtocol) {
                                $("[id$='_chkWritetoNVRAM']").prop("checked", false);
                            }
                        }
                    }
                });
            }
        }

        function pageLoad(sender, args) {
            if (!$(".linedwrap").length) {                
                $(".lined").linedtextarea(
                {                    
                    selectedLine: -1
                });
            }

            $("#btnCloseDialog, #BtnSRClose").click(function () {
                $("#MessageBoxTable").hide();
            });

            $("#AdvancedPanelContentLink").click(function () {

                $("#imgCollapse").toggle();
                $("#imgExpand").toggle();
                $("#AdvancedPanelContent").toggle();
            });


            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequest);


            Resize();
            RemoveTreeScroll();
            DisableEnablebtnUploadBtn();

            function RemoveTreeScroll() {
                $("div[title]").css("overflow", "hidden");
            }

            function Resize() {
                var tableHeaderWidth = $("#tableHeader").width();
                var tdRightSideWidth = $("#tdRightSide").width();

                var linedwrapWidth = tdRightSideWidth - 30;
                var txtConfigWidth = tdRightSideWidth - 90;

                $(".linedwrap").width(linedwrapWidth);
                $("#txtConfig").width(txtConfigWidth);

                $("#tableHeader").width($(".linedtextarea").width() - 8);
            }


            window.onresize = function(event) {
                Resize();
            };

            $(".lined").keyup(function () {
                isConfigEdited = true;

                DisableEnablebtnUploadBtn();
                var lblEditNotif = $("#lblEditNotif");
                lblEditNotif.css("display", "inline");
            });

            function beginRequest(sender, args) {
                var loadingMask = new Ext.LoadMask(Ext.getBody(), { msg: "<%= Resources.NCMWebContent.UploadConfig_LoadingMessage %>" });
                loadingMask.show();
            }

            function ResizeTree() {
                var totalHight = $("#leftSide").height()
                var filterHight = $("#filterContent").height();
                var treeContainer = $("#treeBody");
                var newSize = totalHight - filterHight - 29;
                treeContainer.height(newSize);
            }

            function endRequest(sender, args) {
                ResizeTree();
                var loadingMask = new Ext.LoadMask(Ext.getBody(), { msg: "<%= Resources.NCMWebContent.UploadConfig_LoadingMessage %>" });
                SetSelectedNodeName();
                SetConfigStatus();
                loadingMask.hide();
            }
        }

    </script>
</asp:Content>
