﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMEditMasterPage.master" AutoEventWireup="true" CodeFile="PreviewRemediationScript.aspx.cs" Inherits="Orion_NCM_PreviewRemediationScript" %>

<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmHeadPlaceHolder" Runat="Server">
    <script type="text/javascript" src="/Orion/NCM/JavaScript/main.js"></script>
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title %>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
    <div style="padding:0px 10px 0px 10px;">
        <asp:UpdatePanel runat="server" ID="upPanel" UpdateMode="Conditional">
            <ContentTemplate>
                <ncm:ExScriptPreview ID="ScriptPreview" runat="server" VisibleWriteConfigToNVRAM="false" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

