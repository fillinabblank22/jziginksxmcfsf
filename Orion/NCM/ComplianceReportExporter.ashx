<%@ WebHandler Language="C#" Class="ComplianceReportExporter" %>

using System;
using System.Text;
using System.Globalization;
using System.Xml;
using System.Web;
using System.Data;
using System.Web.SessionState;

using SolarWinds.InformationService.Contract2;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;
using StringRegistrar = SolarWinds.Orion.Core.Common.i18n.Registrar.ResourceManagerRegistrar;


public class ComplianceReportExporter : IHttpHandler, IRequiresSessionState
{
    private ISWrapper isLayer = new ISWrapper();

    private const string QUERY_PARAM_ACTION = "action";
    private const string QUERY_PARAM_OBJECTID = "objid";
    private const string QUERY_PARAM_INCLUDEVIOLATIONDETAILS = "includeViolationDetails";
    private const string QUERY_PARAM_LIIMITATIONID = "limitationId";

    private enum ActionType
    {
        unknown = 0,
        xls = 1,
        csv = 2
    }

    public void ProcessRequest(HttpContext context)
    {
        Export(context);
    }

    public bool IsReusable
    {
        get { return false; }
    }

    #region private functions

    private ActionType StrToActionType(string value)
    {
        if (string.IsNullOrEmpty(value))
            return ActionType.unknown;

        ActionType res;
        try
        {
            res = (ActionType)Enum.Parse(typeof(ActionType), value);

        }
        catch
        {
            res = ActionType.unknown;
        }

        return res;
    }

    private void Export(HttpContext context)
    {
        var action = context.Request[QUERY_PARAM_ACTION];
        var reportId = context.Request[QUERY_PARAM_OBJECTID];
        
        var includeViolationDetails = false;
        if(!string.IsNullOrEmpty(context.Request[QUERY_PARAM_INCLUDEVIOLATIONDETAILS]))
        {
            includeViolationDetails = bool.Parse(context.Request[QUERY_PARAM_INCLUDEVIOLATIONDETAILS]);
        }
        var limitationId = context.Request[QUERY_PARAM_LIIMITATIONID];

        var atype = StrToActionType(action);

        switch (atype)
        {
            case ActionType.csv:
                ExportToCSV(context, reportId, includeViolationDetails, limitationId);
                break;
            case ActionType.xls:
                ExportToXLS(context, reportId, includeViolationDetails, limitationId);
                break;
            default:
                return;
        }
    }

    private void ExportToXLS(HttpContext context, string reportId, bool includeViolationDetails, string limitationId)
    {
        var dt = GetComplianceDataTableForExport(reportId, includeViolationDetails, limitationId);
        
        var appType = GetRespContentType(ActionType.xls);

        var exporter = new Exporter(dt);

        exporter.dlgCellFormate = ComplianceReportResultExport.CellFormation;
        exporter.dlgColumnLookUp = ComplianceReportResultExport.ColumnLookup;
        exporter.dlgXLSHeaderFormate = ComplianceReportResultExport.CreateHeader;
        exporter.dlgCustomCleanUpDataTable = ComplianceReportResultExport.CleanupReportData;

        var buffer = exporter.DoExport(Exporter.ExportMethod.xls);
        var fname = GetFileName(Resources.NCMWebContent.ComplianceReportExporter_ComplianceReport, ActionType.xls);
        SubmitResult(context, buffer, fname, appType);
    }

    private void ExportToCSV(HttpContext context, string reportId, bool includeViolationDetails, string limitationId)
    {
        var dt = GetComplianceDataTableForExport(reportId, includeViolationDetails, limitationId);

        var appType = GetRespContentType(ActionType.csv);

        var exporter = new Exporter(dt);
        exporter.dlgCellFormate = ComplianceReportResultExport.CellFormation;
        exporter.dlgColumnLookUp = ComplianceReportResultExport.ColumnLookup;
        exporter.dlgCustomCleanUpDataTable = ComplianceReportResultExport.CleanupReportData;
        exporter.dlgCSVHeaderFormate = ComplianceReportResultExport.CreateHeader;

        var buffer = exporter.DoExport(Exporter.ExportMethod.csv);
        var fname = GetFileName(Resources.NCMWebContent.ComplianceReportExporter_ComplianceReport, ActionType.csv);
        SubmitResult(context, buffer, fname, appType);
    }

    private DataTable GetComplianceDataTableForExport(string reportId, bool includeViolationDetails, string limitationId)
    {
        DataTable results = null;
        try
        {
            using (var proxy = isLayer.Proxy)
            {
                var swql = $@"
SELECT Nodes.Caption 
    ,Nodes.IP_Address AS NodeIpAddress
    ,PolicyCacheResults.NodeID
    ,PolicyCacheResults.PolicyID
    ,PolicyCacheResults.PolicyName
    ,PolicyCacheResults.RuleID
    ,PolicyCacheResults.RuleName
    ,PolicyCacheResults.IsViolation
    ,PolicyCacheResults.ErrorLevel
    ,PolicyCacheResults.ConfigType
    ,PolicyCacheResults.ConfigTitle
    {(includeViolationDetails ? @",PolicyCacheResults.XmlResults" : string.Empty)}
FROM NCM.NodeProperties AS NCMNodeProperties
INNER JOIN Orion.Nodes AS Nodes ON NCMNodeProperties.CoreNodeID=Nodes.NodeID
INNER JOIN Cirrus.PolicyCacheResults AS PolicyCacheResults ON NCMNodeProperties.NodeID=PolicyCacheResults.NodeID
WHERE PolicyCacheResults.ReportID=@reportId AND {(ExportWithoutViolations(reportId) ? @"(1=1)" : @"PolicyCacheResults.IsViolation=1")}
ORDER BY PolicyCacheResults.PolicyName, PolicyCacheResults.PolicyID, PolicyCacheResults.RuleName,PolicyCacheResults.RuleID, PolicyCacheResults.NodeID
WITH LIMITATION {limitationId}";

                var dt = proxy.Query(swql, new PropertyBag() { { @"reportId", reportId } });

                var violations = new[]
                    {
                        proxy.Cirrus.Settings.GetSetting(Settings.PolicyViolationLevel1, StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"PolicyViolationsLevel_Informational"), null),
                        proxy.Cirrus.Settings.GetSetting(Settings.PolicyViolationLevel2, StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"PolicyViolationsLevel_Warning"), null),
                        proxy.Cirrus.Settings.GetSetting(Settings.PolicyViolationLevel3, StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"PolicyViolationsLevel_Critical"), null)
                    };

                results = new DataTable("ComplianceResultsForExport") { Locale = CultureInfo.InvariantCulture };
                var dc = new DataColumn("NodeID");
                var primaryKeyCols = new DataColumn[1];
                primaryKeyCols[0] = dc;
                results.Columns.Add(dc);
                results.Columns.Add(Resources.NCMWebContent.ComplianceReportExporter_NodeName).DataType = typeof(string);
                results.Columns.Add(Resources.NCMWebContent.ComplianceReportExporter_NodeIpAddress).DataType = typeof(string);
                results.Columns.Add(Resources.NCMWebContent.ComplianceReportExporter_ConfigType).DataType = typeof(string);
                results.Columns.Add(Resources.NCMWebContent.ComplianceReportExporter_ConfigTitle).DataType = typeof(string);
                results.PrimaryKey = primaryKeyCols;
                
                var colCount = 5;

                var currentPolicyID = string.Empty;
                var currentRuleID = string.Empty;

                foreach (DataRow row in dt.Rows)
                {
                    if (!currentPolicyID.Equals(Convert.ToString(row["PolicyID"], CultureInfo.InvariantCulture)))
                    {
                        var column = new DataColumn(string.Format(CultureInfo.InvariantCulture, @"P:{0}", colCount));
                        column.Caption = Convert.ToString(row["PolicyName"], CultureInfo.InvariantCulture);
                        results.Columns.Add(column);
                        colCount++;
                    }

                    if (!currentRuleID.Equals(Convert.ToString(row["RuleID"], CultureInfo.InvariantCulture)) ||
                        !currentPolicyID.Equals(Convert.ToString(row["PolicyID"], CultureInfo.InvariantCulture)))
                    {
                        currentRuleID = Convert.ToString(row["RuleID"], CultureInfo.InvariantCulture);
                        currentPolicyID = Convert.ToString(row["PolicyID"], CultureInfo.InvariantCulture);
                        var column = new DataColumn(string.Format(CultureInfo.InvariantCulture, @"R:{0}", colCount));
                        column.Caption = row["RuleName"].ToString();
                        results.Columns.Add(column);
                        colCount++;
                    }

                    var errorLevel = Convert.ToInt32(row["ErrorLevel"], CultureInfo.InvariantCulture);
                    var isViolation = Convert.ToBoolean(row["IsViolation"], CultureInfo.InvariantCulture);
                    
                    var node_row = results.Rows.Find(row["NodeID"]);
                    if (node_row == null)
                    {
                        var values = new object[colCount];
                        values[0] = row["NodeID"];
                        values[1] = row["Caption"];
                        values[2] = row["NodeIpAddress"];
                        values[3] = row["ConfigType"];
                        values[4] = row["ConfigTitle"];

                        if (isViolation)
                        {
                            if (includeViolationDetails)
                            {
                                values[colCount - 1] = string.Format(CultureInfo.InvariantCulture, @"{0} - {1}", violations[errorLevel], ParseComplianceViolationDetails(Convert.ToString(row["XmlResults"], CultureInfo.InvariantCulture)));
                            }
                            else
                            {
                                values[colCount - 1] = violations[errorLevel];
                            }
                        }

                        results.Rows.Add(values);
                    }
                    else
                    {
                        if (isViolation)
                        {
                            if (includeViolationDetails)
                            {
                                node_row[colCount - 1] = string.Format(CultureInfo.InvariantCulture, @"{0} - {1}", violations[errorLevel], ParseComplianceViolationDetails(Convert.ToString(row["XmlResults"], CultureInfo.InvariantCulture)));
                            }
                            else
                            {
                                node_row[colCount - 1] = violations[errorLevel];
                            }
                        }
                    }
                }
                return results;
            }
        }
        catch
        {
            return null;
        }
    }

    private string ParseComplianceViolationDetails(string xml)
    {
        try
        {
            var sb = new StringBuilder();
            if (!string.IsNullOrEmpty(xml))
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xml);

                var blocks = xmlDoc.GetElementsByTagName("CB");
                foreach (XmlNode block in blocks)
                {
                    var ln = Convert.ToInt32(block.Attributes["LN"].InnerText, CultureInfo.InvariantCulture);
                    if (!(ln <= 1 && blocks.Count == 1))
                    {
                        sb.AppendFormat(@"{0}{1} {2}, {3} {4}",
                            Environment.NewLine,
                            Resources.NCMWebContent.WEBDATA_VM_102,
                            block.Attributes["L"].InnerText.Trim(Environment.NewLine.ToCharArray()),
                            Resources.NCMWebContent.WEBDATA_VM_103,
                            ln);
                    }

                    var patterns = block.SelectNodes("Ps/P");
                    foreach (XmlNode pattern in patterns)
                    {
                        sb.AppendFormat(@"{0}{1}{2} '{3}' {4}",
                            Environment.NewLine,
                            ln <= 1 && blocks.Count == 1 ? string.Empty : new String(' ', 3),
                            Resources.NCMWebContent.WEBDATA_VM_101,
                            pattern.Attributes["PT"].InnerText,
                            Convert.ToBoolean(pattern.Attributes["FM"].InnerText) ? 
                                Resources.NCMWebContent.WEBDATA_VM_98 : Resources.NCMWebContent.WEBDATA_VM_99);

                        foreach (XmlNode line in pattern.SelectNodes("L"))
                        {
                            sb.AppendFormat(@"{0}{1}{2} {3} '{4}'",
                                Environment.NewLine,
                                ln <= 1 && blocks.Count == 1 ? new String(' ', 3) : new String(' ', 6),
                                Resources.NCMWebContent.WEBDATA_VM_100,
                                line.Attributes["FLN"].InnerText,
                                line.Attributes["FL"].InnerText.Trim(Environment.NewLine.ToCharArray()));
                        }
                    }
                }
            }
            return sb.ToString();
        }
        catch { return string.Empty; }
    }

    private bool ExportWithoutViolations(string reportId)
    {
        using (var proxy = isLayer.Proxy)
        {
            var swql = @"SELECT TOP 1 ShowDetails FROM Cirrus.PolicyReports WHERE PolicyReportID=@reportId";
            var dt = proxy.Query(swql, new PropertyBag() { { @"reportId", reportId } });
            if (dt != null && dt.Rows.Count > 0)
            {
                return Convert.ToBoolean(dt.Rows[0][0], CultureInfo.InvariantCulture);
            }
        }
        return true;
    }

    private void SubmitResult(HttpContext context, byte[] buffer, string filename, string applType)
    {
        if (buffer == null)
            return;
        context.Response.Clear();
        context.Response.ContentType = applType;
        context.Response.AddHeader(@"Content-Disposition", $@"attachment; filename={filename}");
        context.Response.OutputStream.Write(buffer, 0, buffer.Length);
        context.ApplicationInstance.CompleteRequest();
    }

    private string GetRespContentType(ActionType type)
    {
        switch (type)
        {
            case ActionType.csv:
                return @"application/csv";
            case ActionType.xls:
                return @"application/excel";
            default:
                return @"application/octet-stream";
        }
    }

    private string GetFileName(string filename, ActionType type)
    {
        switch (type)
        {
            case ActionType.csv:
                return $@"{filename}.csv";
            case ActionType.xls:
                return $@"{filename}.xls";

            default:
                return @"UnknownExportType";
        }
    }

    #endregion
}