﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Script.Serialization;
using SolarWinds.NCM.Common.Help;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.NCMModule.Web.Resources.NCMCore;

public partial class Orion_NCM_ExecuteScript : System.Web.UI.Page
{
    private string gridSelectionStateId = @"85D46102-3FF7-44B6-BDA0-6953C41B881B";

    protected string NcmNodeIdsJSON { get; set; }

    private IEnumerable<Guid> GetNcmNodeIds()
    {
        GridStateManager gridStateMng = new GridStateManager(this.Session, new Guid(gridSelectionStateId));
        return gridStateMng.GetValues().Select(Guid.Parse);
    }

    protected string CloseWindowHandlerUrl => UrlHelper.ToSafeUrlParameter(@"/Orion/NCM/CloseWindowHandler.aspx");

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_VM_05;

        Initialize();

        if (btnFromFile.HasFile)
            scriptBox.Value = ReadScriptFromFile();
    }

    protected void btnSaveScript_Click(object sender, EventArgs e)
    {
        string script = scriptBox.Value;
        if (!string.IsNullOrEmpty(script))
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = @"text/plain";
            HttpContext.Current.Response.AddHeader(@"Content-Disposition", @"attachment; filename=Orion-NCM-script.txt");
            HttpContext.Current.Response.Write(script);
            HttpContext.Current.Response.End();
        }
    }

    private string ReadScriptFromFile()
    {
        using (StreamReader reader = new StreamReader(btnFromFile.FileContent))
        {
            return reader.ReadToEnd();
        }
    }

    private void Initialize()
    {
        var nodeIds = GetNcmNodeIds().ToArray();

        if (nodeIds.Length > 0)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            NcmNodeIdsJSON = js.Serialize(nodeIds);
            selectNodes.AssignedNodes = nodeIds.ToList();
        }
        else
        {
            Page.Response.Redirect("/Orion/NCM/CloseWindowHandler.aspx");
        }
    }

    protected string LearnMoreConfigModeMeaning
    {
        get
        {
            var helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
            return helpHelperWrap.GetHelpUrl("OrionNCMPHConfigModeMeaning");
        }
    }

}