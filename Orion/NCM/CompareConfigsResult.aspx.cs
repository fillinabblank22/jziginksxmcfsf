using System;
using System.Data;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Logging;
using SolarWinds.InformationService.Contract2;

public partial class CompareConfigsResult : System.Web.UI.Page
{
    #region Private Members

    private readonly ISWrapper isWrapper = new ISWrapper();
    private static readonly Log log = new Log();

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_IC_28;

        var nodeId = Request.QueryString[@"NodeID"];
        CheckNodeAvailability(nodeId);

        var configId1 = Request.QueryString[@"ConfigID1"];
        CheckConfigAvailability(configId1);

        var configId2 = Request.QueryString[@"ConfigID2"];
        CheckConfigAvailability(configId2);

        Response.Redirect($"/ui/ncm/configDiff?leftConfigId={configId1}&rightConfigId={configId2}");
    }

    #region Private Functions

    private void CheckConfigAvailability(string configId)
    {
        DataTable dt = null;
        try
        {
            using (var proxy = isWrapper.Proxy)
            {
                dt = proxy.Query(@"SELECT C.ConfigID FROM Cirrus.ConfigArchive AS C where C.ConfigID=@configId",
                    new PropertyBag {{@"configId", Guid.Parse(configId)}});
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
        }

        if (dt != null && dt.Rows.Count == 0)
        {
            throw new ApplicationException(
                string.Format(Resources.NCMWebContent.CompareConfigsResult_NoDataAvailableForConfigID, configId));
        }
    }

    private void CheckNodeAvailability(string nodeId)
    {
        DataTable dt = null;
        try
        {
            using (var proxy = isWrapper.Proxy)
            {
                dt = proxy.Query(@"SELECT NodeID FROM Cirrus.NodeProperties WHERE NodeID=@nodeId",
                    new PropertyBag {{@"nodeId", Guid.Parse(nodeId)}});
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
        }

        if (dt != null && dt.Rows.Count == 0)
        {
            throw new ApplicationException(
                string.Format(Resources.NCMWebContent.CompareConfigsResult_NoDataAvailableForNodeID, nodeId));
        }
    }

    #endregion
}
