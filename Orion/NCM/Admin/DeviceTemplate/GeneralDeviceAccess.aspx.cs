﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Web.UI.HtmlControls;
using System.Data;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.Controls;
using SolarWinds.NCMModule.Web.Resources.DeviceTemplateManagement;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.InformationService.Contract2;
using SolarWinds.NCM.Common.Help;
using SolarWinds.NCM.Common.Transfer;
using SolarWinds.Orion.Cli.Contracts.DataModel;
using SolarWinds.NCM.Contracts;
using System.Linq;
using Castle.Core.Internal;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.NCM.Common.BusinessLayer.Interfaces;

public partial class Orion_NCM_Admin_DeviceTemplate_GeneralDeviceAccess : Page
{
    private readonly IIsWrapper _isWrapper;
    private readonly IDataDecryptor decryptor;
    private readonly INcmBusinessLayerProxyHelper blProxy;
    private string[] executeScriptProtocols;
    private string[] requestConfigProtocols;

    public Orion_NCM_Admin_DeviceTemplate_GeneralDeviceAccess()
    {
        _isWrapper = ServiceLocator.Container.Resolve<IIsWrapper>();
        decryptor = ServiceLocator.Container.Resolve<IDataDecryptor>();
        blProxy = ServiceLocator.Container.Resolve<INcmBusinessLayerProxyHelper>();
    }

    protected bool UseDeviceLoginCredentials
    {
        get
        {
            if (ViewState["NCM_UseDeviceLoginCredentials"] == null)
            {
                using (var proxy = _isWrapper.GetProxy())
                {
                    var connLevel = GetSetting(Settings.ConnectivityLevel, ConnectivityLevels.Default, null);
                    if (connLevel.Equals(ConnectivityLevels.Level1))
                        ViewState["NCM_UseDeviceLoginCredentials"] = true;
                    else
                        ViewState["NCM_UseDeviceLoginCredentials"] = false;
                }
            }
            return Convert.ToBoolean(ViewState["NCM_UseDeviceLoginCredentials"], CultureInfo.InvariantCulture);
        }
    }

    protected bool HideUsernames
    {
        get
        {
            if (ViewState["NCM_HideUsernames"] == null)
            {
                using (var proxy = _isWrapper.GetProxy())
                {
                    ViewState["NCM_HideUsernames"] = GetSetting(Settings.HideUsernames, false, null);
                }
            }
            return Convert.ToBoolean(ViewState["NCM_HideUsernames"], CultureInfo.InvariantCulture);
        }
    }

    protected string HiddenUsername
    {
        get { return Convert.ToString(ViewState["NCM_HiddenUsername"]); }
        set { ViewState["NCM_HiddenUsername"] = value; }
    }

    protected string HiddenPassword
    {
        get { return Convert.ToString(ViewState["NCM_HiddenPassword"]); }
        set { ViewState["NCM_HiddenPassword"] = value; }
    }

    protected string HiddenEnablePassword
    {
        get { return Convert.ToString(ViewState["NCM_HiddenEnablePassword"]); }
        set { ViewState["NCM_HiddenEnablePassword"] = value; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        var results = blProxy.CallMain(x => x.GetCommandProtocolsWithoutGlobalProtocols());
        executeScriptProtocols = results.ExecuteScriptProtocols;
        requestConfigProtocols = results.RequestConfigProtocols;

        CommonHelper.PopulateDropDownList(executeScriptProtocols, ExecProtocol);
        CommonHelper.PopulateDropDownList(requestConfigProtocols, CommandProtocol);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = DeviceTemplateWizardController.GetPageTitle();

        GlobalUsername.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_313, @"${GlobalUsername}");
        GlobalPassword.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_313, @"${GlobalPassword}");
        GlobalEnablePassword.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_313, @"${GlobalEnablePassword}");
        GlobalTelnetPort.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_313, @"${GlobalTelnetPort}");
        GlobalSSHPort.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_313, @"${GlobalSSHPort}");

        if (HideUsernames)
            Username.TextMode = TextBoxMode.Password;
        Password.TextMode = TextBoxMode.Password;
        EnablePassword.TextMode = TextBoxMode.Password;

        if (!Page.IsPostBack)
        {
            CommandProtocol.SelectedValue = requestConfigProtocols[0];
            ExecProtocol.SelectedValue = executeScriptProtocols[0];
            CommandProtocol_SelectedIndexChanged(TransferProtocol, EventArgs.Empty);

            LoadGlobalConnectionProfiles();

            LoadNodeProperty(GlobalConnectionProfile, DeviceTemplateWizardController.NCMNode.ConnectionProfile);
            LoadNodeProperty(UseUserDeviceCredentials, DeviceTemplateWizardController.NCMNode.UseUserDeviceCredentials);
            LoadNodeProperty(Username, DeviceTemplateWizardController.NCMNode.Username, @"Username");
            LoadNodeProperty(Password, DeviceTemplateWizardController.NCMNode.Password, @"Password");
            LoadNodeProperty(EnableLevel, DeviceTemplateWizardController.NCMNode.EnableLevel, @"EnableLevel");
            LoadNodeProperty(EnablePassword, DeviceTemplateWizardController.NCMNode.EnablePassword, @"EnablePassword");
            LoadNodeProperty(ExecProtocol, DeviceTemplateWizardController.NCMNode.ExecProtocol);
            LoadNodeProperty(CommandProtocol, DeviceTemplateWizardController.NCMNode.CommandProtocol);
            LoadNodeProperty(TransferProtocol, DeviceTemplateWizardController.NCMNode.TransferProtocol);
            LoadNodeProperty(TelnetPort, DeviceTemplateWizardController.NCMNode.TelnetPort);
            LoadNodeProperty(SSHPort, DeviceTemplateWizardController.NCMNode.SSHPort);

            if (UseDeviceLoginCredentials)
            {
                UseUserDeviceCredentials.Enabled = false;
                UseUserDeviceCredentials.SelectedValue = @"False";
            }

            GlobalConnectionProfile_SelectedIndexChanged(GlobalConnectionProfile, EventArgs.Empty);

            SetMacroColumnVisibility(Convert.ToInt32(GlobalConnectionProfile.SelectedValue, CultureInfo.InvariantCulture) < 0);

            LoadDeviceCommand(@"CustomUserNamePrompt", UserNamePrompt, AutoDetectUserNamePrompt, CustomUserNamePrompt);
            LoadDeviceCommand(@"CustomPasswordPrompt", PasswordPrompt, AutoDetectPasswordPrompt, CustomPasswordPrompt);
            LoadDeviceCommand(@"EnableIdentifier", EnableIdentifier, AutoDetectEnableIdentifier, CustomEnableIdentifier);
            LoadDeviceCommand(@"EnableCommand", EnableCommand, AutoDetectEnableCommand, CustomEnableCommand);
            LoadDeviceCommand(@"VirtualPrompt", VirtualPrompt, AutoDetectVirtualPrompt, CustomVirtualPrompt);
            LoadDeviceCommand(@"More", More, AutoDetectMore, CustomMore);
            LoadDeviceCommand(@"Reset", Reset);
            LoadDeviceCommand(@"Version", Version);

            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
                ValidateLogin.OnClientClick =
                    $@"demoAction('{DemoModeConstants.NCM_SETTINGS_ADD_OR_MANAGE_DEVICE_TEMPLATES}', this); return false;";
        }

    }

    protected void Next_Click(object sender, DeviceTemplateEventArgs e)
    {
        DeviceTemplateWizardController.NCMNode.ConnectionProfile = GetNodeProperty<int>(GlobalConnectionProfile);
        if (DeviceTemplateWizardController.NCMNode.ConnectionProfile == -1) //no profile
        {
            DeviceTemplateWizardController.NCMNode.UseUserDeviceCredentials = GetNodeProperty<bool>(UseUserDeviceCredentials);
            if (!DeviceTemplateWizardController.NCMNode.UseUserDeviceCredentials)
            {
                DeviceTemplateWizardController.NCMNode.Username = GetNodeProperty<string>(Username);
                DeviceTemplateWizardController.NCMNode.Password = GetNodeProperty<string>(Password);
                DeviceTemplateWizardController.NCMNode.EnableLevel = GetNodeProperty<string>(EnableLevel);
                DeviceTemplateWizardController.NCMNode.EnablePassword = GetNodeProperty<string>(EnablePassword);
            }
            DeviceTemplateWizardController.NCMNode.ExecProtocol = GetNodeProperty<string>(ExecProtocol);
            DeviceTemplateWizardController.NCMNode.CommandProtocol = GetNodeProperty<string>(CommandProtocol);
            DeviceTemplateWizardController.NCMNode.TransferProtocol = GetNodeProperty<string>(TransferProtocol);
            DeviceTemplateWizardController.NCMNode.TelnetPort = GetNodeProperty<string>(TelnetPort);
            DeviceTemplateWizardController.NCMNode.SSHPort = GetNodeProperty<string>(SSHPort);
        }

        UpdateDeviceCommand(DeviceTemplateWizardController.DeviceCommands, @"CustomUserNamePrompt", UserNamePrompt, AutoDetectUserNamePrompt, CustomUserNamePrompt);
        UpdateDeviceCommand(DeviceTemplateWizardController.DeviceCommands, @"CustomPasswordPrompt", PasswordPrompt, AutoDetectPasswordPrompt, CustomPasswordPrompt);
        UpdateDeviceCommand(DeviceTemplateWizardController.DeviceCommands, @"EnableIdentifier", EnableIdentifier, AutoDetectEnableIdentifier, CustomEnableIdentifier);
        UpdateDeviceCommand(DeviceTemplateWizardController.DeviceCommands, @"EnableCommand", EnableCommand, AutoDetectEnableCommand, CustomEnableCommand);
        UpdateDeviceCommand(DeviceTemplateWizardController.DeviceCommands, @"VirtualPrompt", VirtualPrompt, AutoDetectVirtualPrompt, CustomVirtualPrompt);
        UpdateDeviceCommand(DeviceTemplateWizardController.DeviceCommands, @"More", More, AutoDetectMore, CustomMore);
        UpdateDeviceCommand(DeviceTemplateWizardController.DeviceCommands, @"Reset", Reset);
        UpdateDeviceCommand(DeviceTemplateWizardController.DeviceCommands, @"Version", Version);

        AddMenuBasedCommand(DeviceTemplateWizardController.DeviceCommands);
    }

    protected void GlobalConnectionProfile_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetMacroColumnVisibility(Convert.ToInt32(GlobalConnectionProfile.SelectedValue, CultureInfo.InvariantCulture) < 0);

        var profileId = Convert.ToInt32(GlobalConnectionProfile.SelectedValue, CultureInfo.InvariantCulture);

        switch (profileId)
        {
            case -1://no profile set
                if (!UseDeviceLoginCredentials) EnableControl(UseUserDeviceCredentials, DeviceTemplateWizardController.NCMNode.UseUserDeviceCredentials);
                if (Convert.ToBoolean(UseUserDeviceCredentials.SelectedValue, CultureInfo.InvariantCulture))
                {
                    UseUserDeviceCredentials_SelectedIndexChanged(UseUserDeviceCredentials, EventArgs.Empty);
                }
                else
                {
                    EnableControl(Username, DeviceTemplateWizardController.NCMNode.Username, @"Username", GlobalUsername);
                    EnableControl(Password, DeviceTemplateWizardController.NCMNode.Password, @"Password", GlobalPassword);
                    EnableControl(EnableLevel, DeviceTemplateWizardController.NCMNode.EnableLevel, @"EnableLevel");
                    EnableControl(EnablePassword, DeviceTemplateWizardController.NCMNode.EnablePassword, @"EnablePassword", GlobalEnablePassword);
                }
                EnableControl(ExecProtocol, DeviceTemplateWizardController.NCMNode.ExecProtocol);
                EnableControl(CommandProtocol, DeviceTemplateWizardController.NCMNode.CommandProtocol);
                CommandProtocol_SelectedIndexChanged(CommandProtocol, EventArgs.Empty);
                EnableControl(TransferProtocol, DeviceTemplateWizardController.NCMNode.TransferProtocol);
                EnableControl(TelnetPort, DeviceTemplateWizardController.NCMNode.TelnetPort, string.Empty, GlobalTelnetPort);
                EnableControl(SSHPort, DeviceTemplateWizardController.NCMNode.SSHPort, string.Empty, GlobalSSHPort);
                break;
            case 0: //auto detect
                DisableControl(UseUserDeviceCredentials, false);
                DisableControl(Username, Resources.NCMWebContent.WEBDATA_VK_865, @"Username", GlobalUsername);
                DisableControl(Password, Resources.NCMWebContent.WEBDATA_VK_865, @"Password", GlobalPassword);
                DisableControl(EnableLevel, Resources.NCMWebContent.WEBDATA_VK_865);
                DisableControl(EnablePassword, Resources.NCMWebContent.WEBDATA_VK_865, @"EnablePassword", GlobalEnablePassword);
                DisableControl(ExecProtocol, Resources.NCMWebContent.WEBDATA_VK_865);
                DisableControl(CommandProtocol, Resources.NCMWebContent.WEBDATA_VK_865);
                CommandProtocol_SelectedIndexChanged(CommandProtocol, EventArgs.Empty);
                DisableControl(TransferProtocol, Resources.NCMWebContent.WEBDATA_VK_865);
                DisableControl(TelnetPort, Resources.NCMWebContent.WEBDATA_VK_865, string.Empty, GlobalTelnetPort);
                DisableControl(SSHPort, Resources.NCMWebContent.WEBDATA_VK_865, string.Empty, GlobalSSHPort);
                break;
            default: //use existing connection profile
                using (var proxy = _isWrapper.GetProxy())
                {
                    var profile = proxy.Cirrus.Nodes.GetConnectionProfile(profileId);

                    DisableControl(UseUserDeviceCredentials, false);
                    DisableControl(Username, profile.UserName, @"Username", GlobalUsername);
                    DisableControl(Password, profile.Password, @"Password", GlobalPassword);
                    DisableControl(EnableLevel, profile.EnableLevel);
                    DisableControl(EnablePassword, profile.EnablePassword, @"EnablePassword", GlobalEnablePassword);
                    DisableControl(ExecProtocol, profile.ExecuteScriptProtocol);
                    DisableControl(CommandProtocol, profile.RequestConfigProtocol);
                    CommandProtocol_SelectedIndexChanged(CommandProtocol, EventArgs.Empty);
                    DisableControl(TransferProtocol, profile.TransferConfigProtocol);
                    DisableControl(TelnetPort, profile.TelnetPort, string.Empty, GlobalTelnetPort);
                    DisableControl(SSHPort, profile.SSHPort, string.Empty, GlobalSSHPort);
                }
                break;
        }
    }

    protected void UseUserDeviceCredentials_SelectedIndexChanged(object sender, EventArgs e)
    {
        var useUserDeviceLoginCreds = Convert.ToBoolean(UseUserDeviceCredentials.SelectedValue, CultureInfo.InvariantCulture);

        Username.TextMode = !useUserDeviceLoginCreds && HideUsernames ? TextBoxMode.Password : TextBoxMode.SingleLine;
        EnableOrDisableNodeProperty(Username, HiddenUsername, GlobalUsername, !useUserDeviceLoginCreds);

        Password.TextMode = !useUserDeviceLoginCreds ? TextBoxMode.Password : TextBoxMode.SingleLine;
        EnableOrDisableNodeProperty(Password, HiddenPassword, GlobalPassword, !useUserDeviceLoginCreds);

        EnableOrDisableNodeProperty(EnableLevel, HiddenEnableLevel.Value, null, !useUserDeviceLoginCreds);

        EnablePassword.TextMode = !useUserDeviceLoginCreds ? TextBoxMode.Password : TextBoxMode.SingleLine;
        EnableOrDisableNodeProperty(EnablePassword, HiddenEnablePassword, GlobalEnablePassword, !useUserDeviceLoginCreds);

        SetMacroValueVisibility(UsernameValue, !useUserDeviceLoginCreds);
        SetMacroValueVisibility(EnableLevelValue, !useUserDeviceLoginCreds);
    }

    protected void ResetToGlobalMacro_Click(object sender, EventArgs e)
    {
        var btn = sender as LinkButton;

        Control ctrlPropertyValue = null;
        var controlId = btn.Attributes[@"controlId"];
        if (!string.IsNullOrEmpty(controlId))
            ctrlPropertyValue = connectionProfileTable.FindControl(controlId);

        if (ctrlPropertyValue != null)
        {
            if (ctrlPropertyValue is PasswordTextBox)
            {
                var pwdPropertyValue = (PasswordTextBox)ctrlPropertyValue;
                if (controlId.Equals(@"Username", StringComparison.InvariantCultureIgnoreCase))
                {
                    pwdPropertyValue.TextMode = HideUsernames ? TextBoxMode.Password : TextBoxMode.SingleLine;
                    pwdPropertyValue.PasswordText = btn.Attributes[@"macroValue"];
                }
                else if (controlId.Equals(@"Password", StringComparison.InvariantCultureIgnoreCase))
                {
                    pwdPropertyValue.TextMode = TextBoxMode.Password;
                    pwdPropertyValue.PasswordText = btn.Attributes[@"macroValue"];
                }
                else if (controlId.Equals(@"EnablePassword", StringComparison.InvariantCultureIgnoreCase))
                {
                    pwdPropertyValue.TextMode = TextBoxMode.Password;
                    pwdPropertyValue.PasswordText = btn.Attributes[@"macroValue"];
                }
            }
            else if (ctrlPropertyValue is TextBox)
            {
                var txtPropertyValue = (TextBox)ctrlPropertyValue;
                txtPropertyValue.Text = btn.Attributes[@"macroValue"];
            }
        }
    }

    [Localizable(false)]
    protected void CommandProtocol_SelectedIndexChanged(object sender, EventArgs e)
    {
        CommonHelper.InitializeTransferConfigProtocol(TransferProtocol, CommandProtocol.SelectedItem, false);
    }

    protected void ValidateLogin_Click(object sender, EventArgs e)
    {
        TraceOutput.Visible = false;
        DeviceValidationInfo.Visible = false;
        DeviceValidationDetailsSPAN.Visible = false;
        DeviceLoginValidation();
    }

    protected void DeviceValidationDetailsBTN_Click(object sender, EventArgs e)
    {
        var btn = (sender as LinkButton);
        TraceOutput.Visible = !TraceOutput.Visible;
        DeviceValidationDetailsLBL.Text = TraceOutput.Visible ? Resources.NCMWebContent.WEBDATA_VK_741 : Resources.NCMWebContent.WEBDATA_VK_742;
        DeviceValidationDetailsIMG.Src = TraceOutput.Visible ? @"/Orion/NCM/Resources/images/arrowdown.gif" : @"/Orion/NCM/Resources/images/arrowright.gif";
    }

    protected void ShowMacroValues_Click(object sender, EventArgs e)
    {
        var useUserDeviceLoginCreds = Convert.ToBoolean(UseUserDeviceCredentials.SelectedValue, CultureInfo.InvariantCulture);
        var nodeId = DeviceTemplateWizardController.NCMNode.NodeID;

        using (var proxy = _isWrapper.GetProxy())
        {
            if (!HideUsernames && !useUserDeviceLoginCreds)
                ShowMacroValue(UsernameValue, proxy.Cirrus.Nodes.ParseMacros(nodeId, Username.Text), Username.Text);
            if (!useUserDeviceLoginCreds)
                ShowMacroValue(EnableLevelValue, proxy.Cirrus.Nodes.ParseMacros(nodeId, EnableLevel.SelectedValue), EnableLevel.SelectedValue);
            ShowMacroValue(ExecProtocolValue, proxy.Cirrus.Nodes.ParseMacros(nodeId, ExecProtocol.SelectedValue), ExecProtocol.SelectedValue);
            ShowMacroValue(CommandProtocolValue, proxy.Cirrus.Nodes.ParseMacros(nodeId, CommandProtocol.SelectedValue), CommandProtocol.SelectedValue);
            ShowMacroValue(TransferProtocolValue, proxy.Cirrus.Nodes.ParseMacros(nodeId, TransferProtocol.SelectedValue), TransferProtocol.SelectedValue);
            ShowMacroValue(TelnetPortValue, proxy.Cirrus.Nodes.ParseMacros(nodeId, TelnetPort.Text), TelnetPort.Text);
            ShowMacroValue(SSHPortValue, proxy.Cirrus.Nodes.ParseMacros(nodeId, SSHPort.Text), SSHPort.Text);

            Password.TextMode = useUserDeviceLoginCreds ? TextBoxMode.SingleLine : TextBoxMode.Password;
            EnablePassword.TextMode = useUserDeviceLoginCreds ? TextBoxMode.SingleLine : TextBoxMode.Password;
        }
    }

    private void DisableControl<T>(Control ctrlPropertyValue, T propertyValue, string propertyName = "", Control lnk = null)
    {
        var value = HttpUtility.HtmlEncode(decryptor.Decrypt(Convert.ToString(propertyValue, CultureInfo.InvariantCulture)));

        if (ctrlPropertyValue is PasswordTextBox)
        {
            var pwdBox = (PasswordTextBox)ctrlPropertyValue;
            if (propertyName.Equals(@"Username", StringComparison.InvariantCultureIgnoreCase) && HideUsernames)
            {
                if (propertyValue.Equals(Resources.NCMWebContent.WEBDATA_VK_865))
                {
                    pwdBox.TextMode = TextBoxMode.SingleLine;
                }
                else
                {
                    pwdBox.TextMode = TextBoxMode.Password;
                }
            }
            if (propertyName.Equals(@"Password", StringComparison.InvariantCultureIgnoreCase))
            {
                if (propertyValue.Equals(Resources.NCMWebContent.WEBDATA_VK_865))
                {
                    pwdBox.TextMode = TextBoxMode.SingleLine;
                }
                else
                {
                    pwdBox.TextMode = TextBoxMode.Password;
                }
            }
            if (propertyName.Equals(@"EnablePassword", StringComparison.InvariantCultureIgnoreCase))
            {
                if (propertyValue.Equals(Resources.NCMWebContent.WEBDATA_VK_865))
                {
                    pwdBox.TextMode = TextBoxMode.SingleLine;
                }
                else
                {
                    pwdBox.TextMode = TextBoxMode.Password;
                }
            }
            pwdBox.PasswordText = value;
            pwdBox.Enabled = false;
        }
        else if (ctrlPropertyValue is TextBox)
        {
            var txtBox = (TextBox)ctrlPropertyValue;
            txtBox.Text = value;
            txtBox.Enabled = false;
        }
        else if (ctrlPropertyValue is DropDownList)
        {
            var ddList = (DropDownList)ctrlPropertyValue;
            if (propertyValue.Equals(Resources.NCMWebContent.WEBDATA_VK_865))
            {
                var autoDetect = ddList.Items.FindByValue(@"Auto Detect");
                if (autoDetect == null)
                {
                    autoDetect = new ListItem(Resources.NCMWebContent.WEBDATA_VK_865, @"Auto Detect");
                    ddList.Items.Add(autoDetect);
                    ddList.SelectedValue = autoDetect.Value;
                }
                else
                {
                    ddList.SelectedValue = autoDetect.Value;
                }
            }
            else
            {
                ddList.SelectedValue = value;
            }
            ddList.Enabled = false;
        }

        if (lnk != null)
        {
            if (lnk is LinkButton)
            {
                var lnkBtn = (LinkButton)lnk;
                lnkBtn.Visible = false;
            }
        }
    }

    private void EnableControl<T>(Control ctrlPropertyValue, T propertyValue, string propertyName = "", Control lnk = null)
    {
        if (ctrlPropertyValue is PasswordTextBox)
        {
            var pwdBox = (PasswordTextBox)ctrlPropertyValue;
            LoadNodeProperty(ctrlPropertyValue, propertyValue, propertyName);
            pwdBox.Enabled = true;
        }
        else if (ctrlPropertyValue is TextBox)
        {
            var txtBox = (TextBox)ctrlPropertyValue;
            LoadNodeProperty(ctrlPropertyValue, propertyValue);
            txtBox.Enabled = true;
        }
        else if (ctrlPropertyValue is DropDownList)
        {
            var ddList = (DropDownList)ctrlPropertyValue;
            var autoDetect = ddList.Items.FindByValue(@"Auto Detect");
            if (autoDetect != null) ddList.Items.Remove(autoDetect);
            var autoDetermine = ddList.Items.FindByValue(@"Determined by User Account");
            if (autoDetermine != null) ddList.Items.Remove(autoDetermine);
            LoadNodeProperty(ctrlPropertyValue, propertyValue, propertyName);
            ddList.Enabled = true;
        }

        if (lnk != null)
        {
            if (lnk is LinkButton)
            {
                var lnkBtn = (LinkButton)lnk;
                lnkBtn.Visible = true;
            }
        }
    }

    private void EnableOrDisableNodeProperty(Control ctrlPropertyValue, string value, LinkButton lbResetToGlobalMacro, bool bEnabled)
    {
        var encodedValue = HttpUtility.HtmlEncode(value);

        if (ctrlPropertyValue is PasswordTextBox)
        {
            var pwdPropertyValue = (PasswordTextBox)ctrlPropertyValue;
            pwdPropertyValue.PasswordText = bEnabled ? encodedValue : Resources.NCMWebContent.WEBDATA_VK_330;
            pwdPropertyValue.Enabled = bEnabled;
        }
        else if (ctrlPropertyValue is TextBox)
        {
            var txtPropertyValue = (TextBox)ctrlPropertyValue;
            txtPropertyValue.Text = bEnabled ? encodedValue : Resources.NCMWebContent.WEBDATA_VK_330;
            txtPropertyValue.Enabled = bEnabled;
        }
        else if (ctrlPropertyValue is DropDownList)
        {
            var ddlPropertyValue = (DropDownList)ctrlPropertyValue;
            var item = new ListItem(Resources.NCMWebContent.WEBDATA_VK_330, @"Determined by User Account");
            if (bEnabled)
            {
                if (ddlPropertyValue.Items.FindByValue(@"Determined by User Account") != null)
                {
                    ddlPropertyValue.Items.Remove(item);
                }

                var autoDetect = ddlPropertyValue.Items.FindByValue(@"Auto Detect");
                if (autoDetect != null)
                {
                    ddlPropertyValue.Items.Remove(autoDetect);
                }
            }
            else
            {
                if (ddlPropertyValue.Items.FindByValue(@"Determined by User Account") == null)
                {
                    ddlPropertyValue.Items.Add(item);
                }
            }
            ddlPropertyValue.SelectedValue = bEnabled ? encodedValue : Resources.NCMWebContent.WEBDATA_VK_330;
            ddlPropertyValue.Enabled = bEnabled;
        }

        if (lbResetToGlobalMacro != null) lbResetToGlobalMacro.Visible = bEnabled;
    }

    private void SetMacroValueVisibility(Literal literal, bool visible)
    {
        literal.Visible = visible;
    }

    private void SetMacroColumnVisibility(bool visible)
    {
        ShowMacroValuesButtonHolder.Visible = visible;
        foreach (HtmlTableRow row in connectionProfileTable.Rows)
        {
            if (!string.IsNullOrEmpty(row.Attributes[@"visible-col-index"]))
            {
                var index = Convert.ToInt32(row.Attributes[@"visible-col-index"], CultureInfo.InvariantCulture);
                row.Cells[index].Visible = visible;
            }
        }
    }

    private void ShowMacroValue(Literal literal, string parsedMacroValue, string originalValue)
    {
        if (!string.IsNullOrEmpty(parsedMacroValue) && originalValue.StartsWith(@"${"))
            literal.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_965, @"<span style='font-style:italic;'>", HttpUtility.HtmlEncode(parsedMacroValue), @"<span>");
        else
            literal.Text = string.Empty;
    }

    private void LoadGlobalConnectionProfiles()
    {
        GlobalConnectionProfile.Items.Clear();
        GlobalConnectionProfile.Items.Add(new ListItem(Resources.NCMWebContent.WEBDATA_VK_862, @"-1"));
        using (var proxy = _isWrapper.GetProxy())
        {
            var profiles = proxy.Cirrus.Nodes.GetAllConnectionProfiles();
            var autoDetectProfiles = profiles.Where(item => item.UseForAutoDetect);

            if (autoDetectProfiles.Any())
            {
                GlobalConnectionProfile.Items.Add(new ListItem(Resources.NCMWebContent.WEBDATA_VK_864, @"0"));
            }

            foreach (var profile in profiles)
            {
                GlobalConnectionProfile.Items.Add(new ListItem(profile.Name, profile.ID.ToString()));
            }
        }
    }

    private void LoadNodeProperty<T>(Control ctrlPropertyValue, T propertyValue, string propertyName = "")
    {
        var value = HttpUtility.HtmlEncode(decryptor.Decrypt(Convert.ToString(propertyValue, CultureInfo.InvariantCulture)));

        if (ctrlPropertyValue is PasswordTextBox)
        {
            var pwdPropertyValue = (PasswordTextBox)ctrlPropertyValue;
            if (propertyName.Equals(@"Username", StringComparison.InvariantCultureIgnoreCase))
            {
                pwdPropertyValue.TextMode = HideUsernames ? TextBoxMode.Password : TextBoxMode.SingleLine;
                HiddenUsername = value;
            }
            if (propertyName.Equals(@"Password", StringComparison.InvariantCultureIgnoreCase))
            {
                pwdPropertyValue.TextMode = TextBoxMode.Password;
                HiddenPassword = value;
            }
            if (propertyName.Equals(@"EnablePassword", StringComparison.InvariantCultureIgnoreCase))
            {
                pwdPropertyValue.TextMode = TextBoxMode.Password;
                HiddenEnablePassword = value;
            }
            pwdPropertyValue.PasswordText = value;
        }
        else if (ctrlPropertyValue is TextBox)
        {
            var txtPropertyValue = (TextBox)ctrlPropertyValue;
            txtPropertyValue.Text = value;
        }
        else if (ctrlPropertyValue is DropDownList)
        {
            if (propertyName.Equals(@"EnableLevel", StringComparison.InvariantCultureIgnoreCase))
                HiddenEnableLevel.Value = value;

            var ddlPropertyValue = (DropDownList)ctrlPropertyValue;
            if (ddlPropertyValue.Items.FindByValue(value) != null)
                ddlPropertyValue.SelectedValue = value;
            else
                ddlPropertyValue.SelectedIndex = 0;
        }
    }

    private T GetNodeProperty<T>(Control ctrlPropertyValue)
    {
        if (ctrlPropertyValue is PasswordTextBox)
        {
            var pwdPropertyValue = (PasswordTextBox)ctrlPropertyValue;
            return (T)Convert.ChangeType(pwdPropertyValue.PasswordText, typeof(T));
        }
        else if (ctrlPropertyValue is TextBox)
        {
            var txtPropertyValue = (TextBox)ctrlPropertyValue;
            return (T)Convert.ChangeType(txtPropertyValue.Text, typeof(T));
        }
        else if (ctrlPropertyValue is DropDownList)
        {
            var ddlPropertyValue = (DropDownList)ctrlPropertyValue;
            return (T)Convert.ChangeType(ddlPropertyValue.SelectedValue, typeof(T));
        }
        else
        {
            return default(T);
        }
    }

    private void ShowDeviceValidationBar(LoginValidationResults loginResults)
    {
        DeviceValidationBar.Attributes[@"style"] = @"display:block;";
        DeviceValidationInfo.Visible = true;
        DeviceValidationInfo.Style.Add(@"background-color", (loginResults.ValidationResult == eLoginResults.Success) ? @"#D6F6C6" : @"#FEEE90");
        DeviceValidationDetailsSPAN.Visible = (loginResults.DeviceOutput.Length > 0);
        DeviceValidationDetailsLBL.Text = Resources.NCMWebContent.WEBDATA_VK_742;
        DeviceValidationDetailsIMG.Src = @"/Orion/NCM/Resources/images/arrowright.gif";
        DeviceValidationIMG.Src = (loginResults.ValidationResult == eLoginResults.Success) ? @"~/Orion/images/nodemgmt_art/icons/icon_OK.gif" : @"~/Orion/images/nodemgmt_art/icons/icon_warning.gif";
        DeviceValidationResult.Text = HttpUtility.HtmlEncode(loginResults.LoginMessage);
        DeviceValidationResult.Style.Add(@"color", (loginResults.ValidationResult == eLoginResults.Success) ? @"black" : @"red");
        DeviceValidationOutput.Text = HttpUtility.HtmlEncode(loginResults.DeviceOutput);
        UpdatePogressBar.Attributes[@"style"] = @"display:none;";
    }

    private void DeviceLoginValidation()
    {
        var ncmNode = GetNcmNodeForValidateLogin();
        var coreNodeProperties = GetCoreNodeProperties(ncmNode.CoreNodeID);
        var deviceCommands = GetDeviceCommandsForValidateLogin();

        using (var proxy = _isWrapper.GetProxy())
        {
            var deviceTemplateSerializer = ServiceLocator.Container.Resolve<IDeviceTemplateSerializer>();
            var deviceTemplate = 
                new DeviceTemplate { TemplateXml = deviceTemplateSerializer.SerializeXml(deviceCommands, false) };

            var loginResults = proxy.Cirrus.Nodes.ValidateLogin(
                Convert.ToInt32(coreNodeProperties["EngineID"], CultureInfo.InvariantCulture),
                ncmNode,
                Convert.ToString(coreNodeProperties["IPAddress"], CultureInfo.InvariantCulture),
                deviceTemplate);

            ShowDeviceValidationBar(loginResults);

            if (loginResults.ValidationResult == eLoginResults.Success && ncmNode.ConnectionProfile >= 0)
            {
                GlobalConnectionProfile.SelectedValue = HttpUtility.HtmlEncode(loginResults.ConnectionProfileId.ToString());
                GlobalConnectionProfile_SelectedIndexChanged(GlobalConnectionProfile, EventArgs.Empty);
            }

            var connProfileId = Convert.ToInt32(GlobalConnectionProfile.SelectedValue, CultureInfo.InvariantCulture);
            if (connProfileId > 0)
            {
                var connProfile = proxy.Cirrus.Nodes.GetConnectionProfile(connProfileId);
                wizard.EnabledNextButton = connProfile.RequestConfigProtocol.Equals(@"SNMP", StringComparison.InvariantCultureIgnoreCase) ? true : (loginResults.ValidationResult == eLoginResults.Success);
            }
            else if (connProfileId == 0)
            {
                wizard.EnabledNextButton = (loginResults.ValidationResult == eLoginResults.Success);
            }
            else
            {
                var commandProtocol = proxy.Cirrus.Nodes.ParseMacros(ncmNode.NodeID, ncmNode.CommandProtocol);
                wizard.EnabledNextButton = commandProtocol.Equals(@"SNMP", StringComparison.InvariantCultureIgnoreCase) ? true : (loginResults.ValidationResult == eLoginResults.Success);
            }
        }
    }

    private DataRow GetCoreNodeProperties(int coreNodeId)
    {
        using (var proxy = _isWrapper.GetProxy())
        {
            var dt = proxy.Query(@"
                SELECT 
                    EngineID, 
                    IPAddress
                FROM Orion.Nodes WHERE NodeID=@nodeId",
                new PropertyBag { { @"nodeId", coreNodeId } });

            if (dt != null && dt.Rows.Count > 0)
                return dt.Rows[0];
            else
                return null;
        }
    }

    private Tuple<string, string, string, string> GetUserLevelLoginCredentials()
    {
        using (var proxy = _isWrapper.GetProxy())
        {
            var dt = proxy.Query(@"
                SELECT UserLevelName, UserLevelPassword, UserLevelEnableLevel, UserLevelEnablePassword 
                FROM Cirrus.UserLevelLoginCredentials 
                WHERE AccountID=@accountId",
                new PropertyBag{ { @"accountId", HttpContext.Current.Profile.UserName } });

            if (dt != null && dt.Rows.Count > 0)
            {
                return new Tuple<string, string, string, string>(
                    Convert.ToString(dt.Rows[0]["UserLevelName"], CultureInfo.InvariantCulture),
                    Convert.ToString(dt.Rows[0]["UserLevelPassword"], CultureInfo.InvariantCulture),
                    Convert.ToString(dt.Rows[0]["UserLevelEnableLevel"], CultureInfo.InvariantCulture),
                    Convert.ToString(dt.Rows[0]["UserLevelEnablePassword"], CultureInfo.InvariantCulture));
            }
            else
            {
                return new Tuple<string, string, string, string>(
                    string.Empty,
                    string.Empty,
                    string.Empty,
                    string.Empty);
            }
        }
    }

    private T GetSetting<T>(Settings settingName, T defaultValue, int? engineId)
    {
        using (var proxy = _isWrapper.GetProxy())
        {
            var result = proxy.Cirrus.Settings.GetSetting(settingName, null, engineId);
            return string.IsNullOrEmpty(result) ? defaultValue : (T)Convert.ChangeType(result, typeof(T));
        }
    }

    private NCMNode GetNcmNodeForValidateLogin()
    {
        var ncmNode = NCMNode.InitializeNewNode();
        ncmNode.NodeID = DeviceTemplateWizardController.NCMNode.NodeID;
        ncmNode.CoreNodeID = DeviceTemplateWizardController.NCMNode.CoreNodeID;
        ncmNode.ConnectionProfile = GetNodeProperty<int>(GlobalConnectionProfile);
        if (ncmNode.ConnectionProfile == -1)
        {
            ncmNode.UseUserDeviceCredentials = GetNodeProperty<bool>(UseUserDeviceCredentials);
            if (!ncmNode.UseUserDeviceCredentials)
            {
                ncmNode.Username = GetNodeProperty<string>(Username);
                ncmNode.Password = GetNodeProperty<string>(Password);
                ncmNode.EnableLevel = GetNodeProperty<string>(EnableLevel);
                ncmNode.EnablePassword = GetNodeProperty<string>(EnablePassword);
            }
            else
            {
                using (var proxy = _isWrapper.GetProxy())
                {
                    var userLevelLoginCreds = GetUserLevelLoginCredentials();
                    ncmNode.Username = userLevelLoginCreds.Item1;
                    ncmNode.Password = userLevelLoginCreds.Item2;
                    ncmNode.EnableLevel = userLevelLoginCreds.Item3;
                    ncmNode.EnablePassword = userLevelLoginCreds.Item3;
                }
            }
            ncmNode.ExecProtocol = GetNodeProperty<string>(ExecProtocol);
            ncmNode.CommandProtocol = GetNodeProperty<string>(CommandProtocol);
            ncmNode.TransferProtocol = GetNodeProperty<string>(TransferProtocol);
            ncmNode.TelnetPort = GetNodeProperty<string>(TelnetPort);
            ncmNode.SSHPort = GetNodeProperty<string>(SSHPort);
        }
        return ncmNode;
    }

    private DeviceCommands GetDeviceCommandsForValidateLogin()
    {
        var deviceCommands = new DeviceCommands();
        foreach (var deviceCommand in DeviceTemplateWizardController.DeviceCommands.Commands)
        {
            deviceCommands.Commands.Add(deviceCommand);
        }

        UpdateDeviceCommand(deviceCommands, @"CustomUserNamePrompt", UserNamePrompt, AutoDetectUserNamePrompt, CustomUserNamePrompt);
        UpdateDeviceCommand(deviceCommands, @"CustomPasswordPrompt", PasswordPrompt, AutoDetectPasswordPrompt, CustomPasswordPrompt);
        UpdateDeviceCommand(deviceCommands, @"EnableIdentifier", EnableIdentifier, AutoDetectEnableIdentifier, CustomEnableIdentifier);
        UpdateDeviceCommand(deviceCommands, @"EnableCommand", EnableCommand, AutoDetectEnableCommand, CustomEnableCommand);
        UpdateDeviceCommand(deviceCommands, @"VirtualPrompt", VirtualPrompt, AutoDetectVirtualPrompt, CustomVirtualPrompt);
        UpdateDeviceCommand(deviceCommands, @"More", More, AutoDetectMore, CustomMore);
        UpdateDeviceCommand(deviceCommands, @"Reset", Reset);
        UpdateDeviceCommand(deviceCommands, @"Version", Version);

        return deviceCommands;
    }

    private void LoadDeviceCommand(string commandName, TextBox txtDeviceCommand, RadioButton rbAutoDetect = null, RadioButton rbCustom = null)
    {
        var deviceCommand = DeviceTemplateWizardController.DeviceCommands.GetCommand(commandName);
        if (deviceCommand != null)
        {
            txtDeviceCommand.Text = HttpUtility.HtmlEncode(deviceCommand.OriginalValue);
            if (rbAutoDetect != null && rbCustom != null)
            {
                rbAutoDetect.Checked = false;
                rbCustom.Checked = true;
                txtDeviceCommand.Enabled = true;
            }
        }
        else
        {
            txtDeviceCommand.Text = string.Empty;
            if (rbAutoDetect != null && rbCustom != null)
            {
                rbAutoDetect.Checked = true;
                rbCustom.Checked = false;
                txtDeviceCommand.Enabled = false;
            }
        }
    }

    private void UpdateDeviceCommand(DeviceCommands deviceCommands, string commandName, TextBox txtDeviceCommand, RadioButton rbAutoDetect = null, RadioButton rbCustom = null)
    {
        var deviceCommand = deviceCommands.GetCommand(commandName);
        if (rbAutoDetect != null && rbAutoDetect.Checked)
        {
            if (deviceCommand != null)
            {
                deviceCommands.Commands.Remove(deviceCommand);
            }
        }
        else
        {
            var commandValue = txtDeviceCommand.Text;
            if (deviceCommand == null)
            {
                deviceCommand = new DeviceCommand { Name = commandName, OriginalValue = commandValue };
                deviceCommands.Commands.Add(deviceCommand);
            }
            else
            {
                deviceCommand.OriginalValue = commandValue;
            }
        }
    }

    private void AddMenuBasedCommand(DeviceCommands deviceCommands)
    {
        var menuBasedCommand = deviceCommands.GetCommand(@"MenuBased");
        if (menuBasedCommand != null)
        {
            return;
        }
        else
        {
            var virtualPromptCommand = deviceCommands.GetCommand(@"VirtualPrompt");
            if (virtualPromptCommand != null)
            {
                menuBasedCommand = new DeviceCommand { Name = @"MenuBased", OriginalValue = @"False" };
                deviceCommands.Commands.Add(menuBasedCommand);
            }
        }
    }

    protected string LearnMore(string fragment)
    {
        var helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
        return helpHelperWrap.GetHelpUrl(fragment);
    }

}