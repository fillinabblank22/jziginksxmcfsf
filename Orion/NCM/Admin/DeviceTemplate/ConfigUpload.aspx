﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="ConfigUpload.aspx.cs" Inherits="Orion_NCM_Admin_DeviceTemplate_ConfigUpload" %>

<%@ Register Src="~/Orion/NCM/Admin/DeviceTemplate/Controls/DeviceTemplateWizardContainer.ascx" TagName="WizardContainer" TagPrefix="ncm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/Settings/Settings.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/DeviceTemplate/DeviceTemplate.css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=SolarWinds.NCMModule.Web.Resources.DeviceTemplateManagement.DeviceTemplateWizardController.GetPageTitle(true) %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <ncm:WizardContainer ID="wizard" runat="server" OnNextClick="Next_Click">
        <Content>
            <div class="ncm-main-content">
                <div id="UploadOptionsHolder" runat="server">
                    <div class="ncm-block-header">
                        <%=Resources.NCMWebContent.DeviceTemplateWizard_UploadOptionsLabel %>
                    </div>
                    <table id="UploadOptionsTable" runat="server" class="ncm-blue-box">
                        <tr transfer-type="Direct">
                            <td class="ncm-left-label-column">
                                <%=Resources.NCMWebContent.DeviceTemplateWizard_EnterConfigModeCommandLabel %>
                            </td>
                            <td class="ncm-right-input-column">
                                <asp:TextBox ID="txtEnterConfig" runat="server" CssClass="ncm-text-field"></asp:TextBox>
                            </td>
                        </tr>
                        <tr transfer-type="Direct">
                            <td class="ncm-left-label-column">
                                <%=Resources.NCMWebContent.DeviceTemplateWizard_ExitConfigModeCommandLabel %>
                            </td>
                            <td class="ncm-right-input-column">
                                <asp:TextBox ID="txtExitConfig" runat="server" CssClass="ncm-text-field"></asp:TextBox>
                            </td>
                        </tr>
                        <tr transfer-type="Direct">
                            <td class="ncm-left-label-column">
                               <%=Resources.NCMWebContent.DeviceTemplateWizard_WriteToNVRAMCommandLabel %>
                            </td>
                            <td class="ncm-right-input-column">
                                <asp:TextBox ID="txtWriteToNVRAM" runat="server" CssClass="ncm-text-field"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="ncm-left-label-column">
                                <%=Resources.NCMWebContent.DeviceTemplateWizard_RebootCommandLabel %>
                            </td>
                            <td class="ncm-right-input-column">
                                <asp:TextBox ID="txtReboot" runat="server" CssClass="ncm-text-field"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>

                <div id="UploadCommandHolder" runat="server">
                    <div class="ncm-block-header">
                        <%=Resources.NCMWebContent.DeviceTemplateWizard_UploadCommandLabel %>
                    </div>
                    <table class="ncm-blue-box">
                        <tr>
                            <td class="ncm-left-label-column">
                                <%=Resources.NCMWebContent.DeviceTemplateWizard_UploadConfigCommandLabel %>
                            </td>
                            <td class="ncm-right-input-column">
                                <asp:TextBox ID="txtUpload" runat="server" CssClass="ncm-text-field ncm-text-wide-field"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="ncm-left-label-column"></td>
                            <td class="ncm-right-input-column ncm-hint">
                                <asp:Label ID="lblUploadCommandExample" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="ncm-left-label-column"></td>
                            <td class="ncm-right-input-column">
                                <asp:Label CssClass="SettingSuggestion" ID="lblUploadCommandDescription" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </Content>
    </ncm:WizardContainer>
</asp:Content>
