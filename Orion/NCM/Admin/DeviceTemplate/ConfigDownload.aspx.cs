﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.NCM.Common.Dal;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.InformationService.Contract2;
using SolarWinds.NCM.Common.Dal.ConfigTypes;
using SolarWinds.NCM.Common.Transfer;
using SolarWinds.NCM.Contracts.GlobalConfigTypes;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.DeviceTemplateManagement;

public partial class Orion_NCM_Admin_DeviceTemplate_ConfigDownload : Page
{
    private const string ScpDownloadCommand = "DownloadConfigIndirectSCP";
    private const string TftpDownloadCommand = "DownloadConfigIndirect";
    private const string DirectDownloadCommand = "DownloadConfig";

    private ISWrapper _isLayer = new ISWrapper();
    private readonly IConfigTypesProvider configTypesProvider;

    public Orion_NCM_Admin_DeviceTemplate_ConfigDownload()
    {
        configTypesProvider = ServiceLocator.Container.Resolve<IConfigTypesProvider>();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = DeviceTemplateWizardController.GetPageTitle();

        if (!Page.IsPostBack)
        {
            var transferType = GetTransferType();
            if (IsDownloadStepRequired(transferType))
            {
                InitConfigTypeFields();
                CheckTransferType(transferType);
                InitConfigDownload();
            }
            else
            {
                lblNoDownload.Visible = true;
                DownloadSection.Visible = false;
            }

            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
                DownloadConfigButton.OnClientClick =
                    $@"demoAction('{DemoModeConstants.NCM_SETTINGS_ADD_OR_MANAGE_DEVICE_TEMPLATES}', this); return false;";
        }
    }

    private bool IsDownloadStepRequired(eTransferMethod transferType)
    {
        return transferType != eTransferMethod.IndirectSNMP;
    }

    #region Config Types Section

    private void InitConfigTypeFields()
    {
        var configTypeCommandList = new List<ConfigTypeCommand>();

        var configTypes = GetConfigTypes();
        foreach (var configType in configTypes)
        {
            var originalValue = string.Empty;
            var isBinary = false;
            if (DeviceTemplateWizardController.DeviceCommands != null)
            {
                var command = DeviceTemplateWizardController.DeviceCommands.GetCommand(configType);
                originalValue = (command != null && command.OriginalValue != null)
                    ? command.OriginalValue
                    : string.Empty;

                isBinary = command != null ? command.IsBinary : false;
            }
            configTypeCommandList.Add(new ConfigTypeCommand(configType, originalValue, isBinary));
        }

        ConfigTypesRepeater.DataSource = configTypeCommandList;
        ConfigTypesRepeater.DataBind();
    }

    private IEnumerable<string> GetConfigTypes()
    {
        var nodeId = DeviceTemplateWizardController.NCMNode.NodeID;
        foreach (var configType in configTypesProvider.GetCommon(new [] {nodeId}))
        {
            yield return configType.Name;
        }
    }

    #endregion

    #region Download Command Section

    #region Resources

    private static readonly string macroConfigType = @"${ConfigType}";
    private static readonly string macroStorageAddress = @"${StorageAddress}";
    private static readonly string macroStorageFilename = @"${StorageFilename}";
    private static readonly string macroTransferProtocol = @"${TransferProtocol}";
    private static readonly string macroCRLF = @"${CRLF}";
    private static readonly string macroSCPStorageAddress = @"${SCPStorageAddress}";
    private static readonly string macroSCPServerUserName = @"${SCPServerUserName}";
    private static readonly string macroSCPServerPassword = @"${SCPServerPassword}";
    private static readonly string downloadConfigCommand = @"show ${ConfigType}";
    private static readonly string scpDownloadConfigCommand = @"copy ${TransferProtocol}://${SCPServerUserName}@${SCPStorageAddress}/${StorageFilename}  ${ConfigType}${CRLF}${CRLF}${SCPServerPassword}";
    private static readonly string tftpDownloadConfigCommand = @"copy ${TransferProtocol}://${StorageAddress}/${StorageFilename}  ${ConfigType}${CRLF}${CRLF}";

    private static readonly string scpDownloadCommandExampleText = $@"{Resources.NCMWebContent.DeviceTemplateWizard_ForExampleLabel} {scpDownloadConfigCommand}";
    private static readonly string scpDonwloadCommandDescriptionText = $@"<b>{Resources.NCMWebContent.DeviceTemplateWizard_AvailableMacrosLabel}</b></br>
                                            {string.Format(Resources.NCMWebContent.DeviceTemplateWizard_DetermineConfigTypeLabel, macroConfigType)}</br>
                                            {string.Format(Resources.NCMWebContent.DeviceTemplateWizard_IPAddressOfSCPServerLabel, macroSCPStorageAddress)}</br>
                                            {string.Format(Resources.NCMWebContent.DeviceTemplateWizard_UsernameUsedToConnectSCPServerLabel, macroSCPServerUserName)}</br>
                                            {string.Format(Resources.NCMWebContent.DeviceTemplateWizard_PasswordUsedToConnectSCPServerLabel, macroSCPServerPassword)}</br>
                                            {string.Format(Resources.NCMWebContent.DeviceTemplateWizard_AutoGeneratedFilenameOnSCPServerLabel, macroStorageFilename)}</br>
                                            {string.Format(Resources.NCMWebContent.DeviceTemplateWizard_TransferProtocolToDownloadConfigLabel, macroTransferProtocol)}</br>
                                            {string.Format(Resources.NCMWebContent.DeviceTemplateWizard_CRLFLabel, macroCRLF)}</br>";

    private static readonly string tftpDownloadCommandExampleText = $@"{Resources.NCMWebContent.DeviceTemplateWizard_ForExampleLabel} {tftpDownloadConfigCommand}";
    private static readonly string tftpDownloadCommandDescriptionText = $@"<b>{Resources.NCMWebContent.DeviceTemplateWizard_AvailableMacrosLabel}</b></br>
                                            {string.Format(Resources.NCMWebContent.DeviceTemplateWizard_DetermineConfigTypeLabel, macroConfigType)}</br>
                                            {string.Format(Resources.NCMWebContent.DeviceTemplateWizard_IPAddressOfTFTPServerLabel, macroStorageAddress)}</br>
                                            {string.Format(Resources.NCMWebContent.DeviceTemplateWizard_AutoGeneratedFilenameOnTFTPServerLabel, macroStorageFilename)}</br>
                                            {string.Format(Resources.NCMWebContent.DeviceTemplateWizard_TransferProtocolToDownloadConfigLabel, macroTransferProtocol)}</br>
                                            {string.Format(Resources.NCMWebContent.DeviceTemplateWizard_CRLFLabel, macroCRLF)}</br>";

    private static readonly string directDownloadCommandExampleText = $@"{Resources.NCMWebContent.DeviceTemplateWizard_ForExampleLabel} {downloadConfigCommand}";
    private static readonly string directDownloadCommandDescriptionText = $@"<b>{Resources.NCMWebContent.DeviceTemplateWizard_AvailableMacrosLabel}</b></br>
                                            {string.Format(Resources.NCMWebContent.DeviceTemplateWizard_DetermineConfigTypeLabel, macroConfigType)}</br>";

    #endregion

    private void CheckTransferType(eTransferMethod transferType)
    {
        var downloadCommandName = GetDownloadCommandName(transferType);
        if (DeviceTemplateWizardController.DeviceCommands != null)
        {
            var command = DeviceTemplateWizardController.DeviceCommands.GetCommand(downloadCommandName);
            if (command != null)
                txtDownloadConfigCommand.Text = command.OriginalValue;
        }

        InitDescriptionBox(transferType);
    }

    private void InitDescriptionBox(eTransferMethod transferProtocol)
    {
        const string descriptionIcon = @"<span class='SettingSuggestion-Icon'></span>";
        switch (transferProtocol)
        {
            case eTransferMethod.IndirectSCP:
                lblDownloadCommandExample.Text = scpDownloadCommandExampleText;
                lblDownloadCommandDescription.Text = descriptionIcon + scpDonwloadCommandDescriptionText;
                break;
            case eTransferMethod.IndirectTFTP:
                lblDownloadCommandExample.Text = tftpDownloadCommandExampleText;
                lblDownloadCommandDescription.Text = descriptionIcon + tftpDownloadCommandDescriptionText;
                break;
            default:
                lblDownloadCommandExample.Text = directDownloadCommandExampleText;
                lblDownloadCommandDescription.Text = descriptionIcon + directDownloadCommandDescriptionText;
                break;
        }
    }

    private string GetDownloadCommandName(eTransferMethod transferProtocol)
    {
        switch (transferProtocol)
        {
            case eTransferMethod.IndirectSCP:
                return ScpDownloadCommand;
            case eTransferMethod.IndirectTFTP:
                return TftpDownloadCommand;
            default:
                return DirectDownloadCommand;
        }
    }

    protected void Next_Click(object sender, DeviceTemplateEventArgs e)
    {
        SaveCommandsFromInput();
    }

    private void SaveCommandsFromInput()
    {
        var transferProtocol = GetTransferType();
        if (transferProtocol == eTransferMethod.IndirectSNMP)
            return;

        var downloadCommandName = GetDownloadCommandName(transferProtocol);
        UpdateOrAddCommand(downloadCommandName, txtDownloadConfigCommand.Text);

        for (var i = 0; i < ConfigTypesRepeater.Items.Count; i++)
        {
            var lblConfigTypeName = (Label)ConfigTypesRepeater.Items[i].FindControl(@"lblConfigTypeName");
            var txtConfigFileName = (TextBox)ConfigTypesRepeater.Items[i].FindControl(@"txtConfigFileName");
            var cbIsBinary = (CheckBox)ConfigTypesRepeater.Items[i].FindControl(@"cbIsBinary");

            UpdateOrAddCommand(lblConfigTypeName.Text, txtConfigFileName.Text, cbIsBinary.Checked);
        }
    }

    private void UpdateOrAddCommand(string commandName, string commandValue, bool isBinary = false)
    {
        var command = DeviceTemplateWizardController.DeviceCommands.GetCommand(commandName);
        if (command != null)
        {
            command.OriginalValue = commandValue;
            command.IsBinary = isBinary;
        }
        else
        {
            DeviceTemplateWizardController.DeviceCommands.Commands.Add(new DeviceCommand
            {
                Name = commandName,
                OriginalValue = commandValue,
                IsBinary = isBinary
            });
        }
    }

    private eTransferMethod GetTransferType()
    {
        return DeviceTemplateHelper.GetTransferType(
            transferProtocol: ParseMacros(DeviceTemplateWizardController.NCMNode.TransferProtocol),
            commandProtocol: ParseMacros(DeviceTemplateWizardController.NCMNode.CommandProtocol));
    }

    protected void ShowMacroValues_Click(object sender, EventArgs e)
    {
        var value = ParseMacros(txtDownloadConfigCommand.Text);
        lblParsedDownloadConfigValue.Text = string.IsNullOrEmpty(value) ? string.Empty : string.Format(Resources.NCMWebContent.WEBDATA_VK_965, @"<span style='font-style:italic;'>", HttpUtility.HtmlEncode(value), @"</span>");
    }

    private string ParseMacros(string macro)
    {
        var nodeID = DeviceTemplateWizardController.NCMNode.NodeID;
        using (var proxy = _isLayer.Proxy)
        {
            return proxy.Cirrus.Nodes.ParseMacros(nodeID, macro);
        }
    }

    #endregion

    #region Test Download Section

    private string ConfigType
    {
        get { return ddlConfigTypes.SelectedItem.Value; }
    }

    private Guid TransferID
    {
        get
        {
            if (ViewState[ClientID] != null)
                return (Guid)ViewState[ClientID];
            else
                return Guid.Empty;
        }
        set { ViewState[ClientID] = value; }
    }

    private void InitConfigDownload()
    {
        ToggleDownloadButton(true);
        InitConfigTypesList();
        InitTimer();
    }

    private void InitConfigTypesList()
    {
        var types = GetConfigTypes();
        foreach (var type in types)
        {
            ddlConfigTypes.Items.Add(new ListItem(type, type));
        }
    }

    private void InitTimer()
    {
        DownloadTimer.Interval = 3000;
        DownloadTimer.Enabled = false;
    }

    private void RefreshStatus()
    {
        var transferIsFinished = true;
        var dt = GetTransferResults(TransferID);
        if (dt != null)
        {
            lblStatus.ForeColor = Color.Black;
            if (dt.Rows.Count > 0)
            {
                var state = (eTransferState)Enum.Parse(typeof(eTransferState), dt.Rows[0]["Status"].ToString());

                if (state == eTransferState.Error)
                {
                    lblStatus.Text = ParseStatus(state, Convert.ToString(dt.Rows[0]["ErrorMessage"], CultureInfo.InvariantCulture));
                }
                else
                {
                    if (state == eTransferState.Complete)
                    {
                        lblStatus.Text = ParseStatus(state);
                        SetLinkToLatestConfig(Guid.Parse(dt.Rows[0]["ConfigID"].ToString()));
                        ConfigRow.Visible = true;
                    }
                    else
                    {
                        lblStatus.Text = ParseStatus(state);
                        ConfigRow.Visible = false;
                        transferIsFinished = false;
                    }
                }
            }
            else
            {
                lblStatus.Text = Resources.NCMWebContent.DeviceTemplateWizard_NotAvailableLabel;
            }
        }

        ToggleTimer(!transferIsFinished);
        ToggleDownloadButton(transferIsFinished);
    }

    private string ParseStatus(eTransferState state, string error = "")
    {
        switch (state)
        {
            case eTransferState.Complete:
                return string.Format(CultureInfo.InvariantCulture, @"<img src='/Orion/NCM/Resources/images/Status.Complete.gif'/>&nbsp;{0}", Resources.NCMWebContent.DeviceTemplateWizard_ConfigDownloadedMessage);
            case eTransferState.Transfering:
                return string.Format(CultureInfo.InvariantCulture, @"<img src='/Orion/NCM/Resources/images/spinner.gif'/>&nbsp;{0}...", i18nNCMHelper.LookupTransferStatus(state, true, Resources.NCMWebContent.DeviceTemplateWizard_ConfigDownloadingMessage));
            case eTransferState.Queued:
                return string.Format(CultureInfo.InvariantCulture, @"<img src='/Orion/NCM/Resources/images/spinner.gif'/>&nbsp;{0}...", i18nNCMHelper.LookupTransferStatus(state, false, string.Empty));
            case eTransferState.Error:
                return string.Format(CultureInfo.InvariantCulture, @"<img src='/Orion/NCM/Resources/images/icon_failed.gif'/>&nbsp;{0}<br />{1}", Resources.NCMWebContent.DeviceTemplateWizard_ConfigNotDownloadedMessage, error);
            default:
                return null;
        }
    }

    private void SetLinkToLatestConfig(Guid configId)
    {
        using (var proxy = _isLayer.Proxy)
        {
            var dt = proxy.Query(@"SELECT ConfigID, ConfigTitle, IsBinary 
                FROM Cirrus.ConfigArchive
                WHERE ConfigID=@configId",
                new PropertyBag { { @"configId", configId } });

            if (dt != null && dt.Rows.Count > 0)
            {
                var _configId = (Guid)dt.Rows[0]["ConfigID"];
                var configTitle = (string)dt.Rows[0]["ConfigTitle"];
                var isBinary = (bool)dt.Rows[0]["IsBinary"];
 
                var imageUrl = isBinary ? @"/Orion/NCM/Resources/images/ConfigIcons/Config-Binary.png" : @"/Orion/NCM/Resources/images/ConfigIcons/Config-Text.png";
                
                hlConfigImage.ImageUrl = imageUrl;
                lblConfigText.InnerText = configTitle;

                hlConfig.NavigateUrl = $@"/Orion/NCM/ConfigDetails.aspx?configID={{{_configId}}}";
                hlConfig.Target = @"_blank";
            }
        }
    }

    private DataTable GetTransferResults(Guid transferId)
    {
        using (var proxy = _isLayer.Proxy)
        {
            var dt = proxy.Query(@" SELECT Status, ErrorMessage, ConfigID
                FROM NCM.TransferResults                
                WHERE TransferID=@transferId",
                new PropertyBag { { @"transferId", transferId } });
            return dt;
        }
    }

    private void ToggleDownloadButton(bool enable)
    {
        DownloadConfigButton.Enabled = enable;
    }

    private void ToggleTimer(bool enable)
    {
        DownloadTimer.Enabled = enable;
    }

    protected void DownloadConfig_Click(object sender, EventArgs e)
    {
        SaveCommandsFromInput();

        lblStatus.Text = string.Empty;
        lblConfigText.InnerText = string.Empty;

        var deviceTemplateSerializer = ServiceLocator.Container.Resolve<IDeviceTemplateSerializer>();

        using (var proxy = _isLayer.Proxy)
        {
            var IDs = proxy.Cirrus.ConfigArchive.DownloadConfigOnNodes(
                new List<NCMNode>(new[] { DeviceTemplateWizardController.NCMNode }),
                deviceTemplateSerializer.SerializeXml(DeviceTemplateWizardController.DeviceCommands, false),
                ConfigType);
            TransferID = IDs.First();
        }

        RefreshStatus();
    }

    protected void DownloadTimer_OnTick(object sender, EventArgs e)
    {
        RefreshStatus();
    }

    #endregion

    private class ConfigTypeCommand
    {
        public ConfigTypeCommand(string configType, string originalConfigType, bool isBinary)
        {
            ConfigType = configType;
            OriginalConfigType = originalConfigType;
            IsBinary = isBinary;
        }

        public string ConfigType { get; set; }
        public string OriginalConfigType { get; set; }
        public bool IsBinary { get; set; }
    }
}