﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="ConfigDownload.aspx.cs" Inherits="Orion_NCM_Admin_DeviceTemplate_ConfigDownload" %>

<%@ Register Src="~/Orion/NCM/Admin/DeviceTemplate/Controls/DeviceTemplateWizardContainer.ascx" TagName="WizardContainer" TagPrefix="ncm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
    <link href="/Orion/NCM/Admin/Settings/Settings.css" rel="stylesheet" />
    <link href="/Orion/NCM/Admin/DeviceTemplate/DeviceTemplate.css" rel="stylesheet" />

    <style type="text/css">
        .ncm-description-column
        {
            padding-bottom: 20px;
        }
        
        .inline-display
        {
            display: inline !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=SolarWinds.NCMModule.Web.Resources.DeviceTemplateManagement.DeviceTemplateWizardController.GetPageTitle(true) %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <ncm:WizardContainer ID="wizard" runat="server" OnNextClick="Next_Click">
        <Content>
            <div class="ncm-main-content">
                <asp:Label ID="lblNoDownload" runat="server" Text="<%$ Resources: NCMWebContent, DeviceTemplateWizard_NothingToSelectOnThisStepLabel %>" Visible="False" />
                <div id="DownloadSection" runat="server">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div class="ncm-block-header">
                                <%=Resources.NCMWebContent.DeviceTemplateWizard_DownloadCommandLabel %>
                            </div>
                            <table id="tblDownloadCommandTable" class="ncm-blue-box" runat="server">
                                <tr>
                                    <td class="ncm-description-column" colspan="2">
                                        <%=Resources.NCMWebContent.DeviceTemplateWizard_DownloadCommandNote %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ncm-left-label-column">
                                        <%=Resources.NCMWebContent.DeviceTemplateWizard_DownloadConfigCommandLabel %>
                                    </td>
                                    <td class="ncm-right-input-column">
                                        <asp:TextBox ID="txtDownloadConfigCommand" runat="server" CssClass="ncm-text-field ncm-text-wide-field"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td class="ncm-right-input-column">
                                        <table cellpadding="0" cellspacing="0" class="ncm-text-field ncm-text-wide-field">
                                            <tr>
                                                <td><asp:Label ID="lblParsedDownloadConfigValue" runat="server"></asp:Label></td>
                                                <td style="text-align:right">
                                                    <orion:LocalizableButton runat="server" ID="ShowMacroValues" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_964 %>" DisplayType="Small" OnClick="ShowMacroValues_Click"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td class="ncm-right-input-column ncm-hint">
                                        <asp:Label ID="lblDownloadCommandExample" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td class="ncm-right-input-column">
                                        <asp:Label CssClass="SettingSuggestion" ID="lblDownloadCommandDescription" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:Repeater ID="ConfigTypesRepeater" runat="server">
                        <HeaderTemplate>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="ncm-block-header">
                                        <%=Resources.NCMWebContent.DeviceTemplateWizard_ConfigTypesLabel %>
                                    </td>
                                    <td style="text-align: right;">
                                        <img src="/Orion/NCM/Resources/images/ConfigSnippets/icon_editsnippet.gif" alt="" />
                                        <a href="/ui/ncm/configTypes" class="ncm-link" target="_blank">
                                            <%=Resources.NCMWebContent.DeviceTemplateWizard_ManageConfigTypesLink %>
                                        </a>
                                    </td>
                                </tr>
                            </table>
                            <table class="ncm-blue-box">
                                <tr>
                                    <td class="ncm-description-column" colspan="2">
                                        <%=Resources.NCMWebContent.DeviceTemplateWizard_ConfigTypesNote %>
                                    </td>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                                <tr>
                                    <td class="ncm-left-label-column">
                                        <asp:Label id="lblConfigTypeName" CssClass="inline-display" runat="server" Text='<%# Eval(@"ConfigType") %>'></asp:Label>
                                        <span> <%=Resources.NCMWebContent.DeviceTemplateWizard_ConfigFileNameLabel %> </span>
                                    </td>
                                    <td class="ncm-right-input-column" style="white-space:nowrap;">
                                        <asp:TextBox ID="txtConfigFileName" runat="server" Text='<%# Eval(@"OriginalConfigType") %>' CssClass="ncm-text-field"></asp:TextBox>
                                        <span style="padding-left:10px;">
                                            <asp:CheckBox ID="cbIsBinary" runat="server" Text="<%$ Resources: NCMWebContent, DeviceTemplateWizard_IsBinaryConfigCheckboxText %>" Checked='<%# Eval(@"IsBinary") %>'></asp:CheckBox>
                                        </span>
                                    </td>
                                </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>

                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" style="padding-top:20px;">
                        <ContentTemplate>
                            <div style="border-top: 1px solid #e6e6dd;padding:10px 50px 0px 50px;">
                                <div class="ncm-block-header">
                                    <%=Resources.NCMWebContent.DeviceTemplateWizard_DownloadTestLabel %>
                                </div>
                                <table id="tblTestDownload" class="ncm-blue-box" runat="server">
                                    <tr>
                                        <td class="ncm-description-column" colspan="3">
                                            <%=Resources.NCMWebContent.DeviceTemplateWizard_DownloadTestNote %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ncm-left-label-column">
                                            <%=Resources.NCMWebContent.DeviceTemplateWizard_SelectConfigTypeToDownloadLabel %>
                                        </td>
                                        <td class="ncm-right-input-column" style="width:1%;white-space:nowrap;">
                                            <asp:DropDownList ID="ddlConfigTypes" runat="server"></asp:DropDownList>
                                            <orion:LocalizableButton runat="server" ID="DownloadConfigButton" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, DeviceTemplateWizard_PerformDownloadTestButtonText %>" OnClick="DownloadConfig_Click" Enabled="False"/>
                                        </td>
                                        <td class="ncm-right-input-column">
                                            <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr runat="server" ID="ConfigRow"  Visible="False" class="ncm-blue-box">
                                        <td  class="ncm-left-label-column">
                                            <%=Resources.NCMWebContent.DeviceTemplateWizard_ConfigLabel %>
                                        </td>
                                        <td class="ncm-right-input-column" colspan="2">
                                            <asp:HyperLink ID="hlConfig" runat="server">
                                                <asp:Image runat="server" ID="hlConfigImage"/>
                                                <span runat="server" ID="lblConfigText"></span>
                                            </asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                                <asp:Timer ID="DownloadTimer" runat="server" OnTick="DownloadTimer_OnTick"></asp:Timer>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </Content>
    </ncm:WizardContainer>
</asp:Content>

