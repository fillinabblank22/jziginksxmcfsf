﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using SolarWinds.InformationService.Contract2;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Logging;

public partial class Orion_NCM_Admin_DeviceTemplate_DeviceTemplatesMigration : System.Web.UI.Page
{
    private static readonly Log _log = new Log();

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.DeviceTemplate_DeviceTemplatesMigration_PageTitle;

        if (!Page.IsPostBack)
        {
            repeater.DataSource = GetConflicts();
            repeater.DataBind();
        }
    }

    private List<object> GetConflicts()
    {
        try
        {
            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                var dt = proxy.Query(@"SELECT OptionValue FROM Cirrus.Options WHERE OptionName=@optionName",
                    new PropertyBag() {{@"optionName", @"NCM_DeviceTemplatesMigrationConflicts"}});
                if (dt != null && dt.Rows.Count > 0)
                {
                    string conflicts = dt.Rows[0][0] as string;
                    return JsonConvert.DeserializeObject<List<object>>(conflicts);
                }
            }
        }
        catch (Exception ex)
        {
            _log.Error("Unable to Get NCM device templates migration conflicts.", ex);
        }
        return null;
    }
}