﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="SelectNode.aspx.cs" Inherits="Orion_NCM_Admin_DeviceTemplate_SelectNode" %>

<%@ Register Src="~/Orion/NCM/Admin/DeviceTemplate/Controls/DeviceTemplateWizardContainer.ascx" TagName="WizardContainer" TagPrefix="ncm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
    <script type="text/javascript" src="/Orion/NCM/JavaScript/main.js"></script>
    
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/DeviceTemplate/DeviceTemplate.css" />
    
    <script type="text/javascript">
        var checkedNodeId = '';

        function UltraWebTree_SingleNodeCheckedRefresh() {
            checkedNodeId = '';
        }

        function UltraWebTree_SingleNodeChecked(treeId, nodeId, bChecked) {
            var node = igtree_getNodeById(nodeId);

            if (node.getDataPath().substring(0, 1) == "N") {
                if (bChecked == false) {
                    checkedNodeId = '';
                    return;
                }
            }
            if (node.getDataPath().substring(0, 1) == "N") {
                if (checkedNodeId != '') {
                    var configNode = igtree_getNodeById(checkedNodeId);
                    configNode.setChecked(false);
                }
                checkedNodeId = nodeId;
            }
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=SolarWinds.NCMModule.Web.Resources.DeviceTemplateManagement.DeviceTemplateWizardController.GetPageTitle(true, false) %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <ncm:WizardContainer ID="wizard" runat="server" OnNextClick="Next_Click">
        <Content>
            <asp:UpdatePanel ID="updatePanel" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <div class="ncm-main-content">
                        <div style="max-height:500px;min-width:500px;overflow:auto;">
                            <table id="nodetreecontrol_device_template_datapicker" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td id="nodes_picker_container" runat="server">                                    
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="padding-top:10px;">
                            <asp:CustomValidator ID="customValidator" runat="server" OnServerValidate="NodeSelectionValidate" ErrorMessage="<%$ Resources: NCMWebContent, DeviceTemplateWizard_YouMustSelectNodeMessage %>" EnableClientScript="false" Display="Dynamic"></asp:CustomValidator>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </Content>
    </ncm:WizardContainer>
</asp:Content>

