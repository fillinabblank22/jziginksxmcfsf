﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="GeneralDeviceAccess.aspx.cs" Inherits="Orion_NCM_Admin_DeviceTemplate_GeneralDeviceAccess" %>

<%@ Register Src="~/Orion/NCM/Admin/DeviceTemplate/Controls/DeviceTemplateWizardContainer.ascx" TagName="WizardContainer" TagPrefix="ncm" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/DeviceTemplate/DeviceTemplate.css" />

    <style type="text/css">
        .ncm-input-value-column
        {
            width: 225px;
        }
    
        .ncm-macro-value-column
        {
            width: 150px;
            white-space: nowrap;
            padding-right: 20px;
        }

        .ncm-deviceTemplateTable-container 
        {
            padding: 10px 0; 
        }

        .ncm-deviceTemplateTable-container table 
        { 
            width: 70%;
            padding: 0 10px;
        }

        .ncm-deviceTemplateTable-container .ncm-prompt-radio-button-column 
        {
            white-space: nowrap;
            width: 200px;
        }

        .ncm-deviceTemplateTable-container .ncm-prompt-textbox-column 
        {
             white-space: nowrap;
        }

        .ncm-deviceTemplateTable-container .ncm-prompt-textbox-column input 
        {
            -ms-min-width: 150px;
            min-width: 150px;
        }
    </style>

    <script type="text/javascript">
        //Initializing Additional Parameters validators
        function initializeValidators()
        {
            ToggleField('CustomUserNamePrompt', 'UserNamePrompt', 'rfvUserNamePrompt');
            ToggleField('CustomPasswordPrompt', 'PasswordPrompt', 'rfvPasswordPrompt');
            ToggleField('CustomEnableIdentifier', 'EnableIdentifier', 'rfvEnableIdentifier');
            ToggleField('CustomEnableCommand', 'EnableCommand', 'rfvEnableCommand');
            ToggleField('CustomVirtualPrompt', 'VirtualPrompt', 'rfvVirtualPrompt');
            ToggleField('CustomMore', 'More', 'rfvMore');
        }

        function HideDeviceValidationBar() {
            $("[id$='_UpdatePogressBar']").show();
            $("[id$='_DeviceValidationBar']").hide();
        }

        function ToggleField(source, target, fieldValidator) {
            var radioBtn = $("#" + source);
            var textBox = $("#" + target);
            var validator = $("#" + fieldValidator);

            if (radioBtn.is(':checked'))
            {
                textBox.prop('disabled', false);
                ValidatorEnable(validator[0], true);
            } else {
                textBox.prop('disabled', true);
                ValidatorEnable(validator[0], false);
            }
        }

        function ToggleAddvancedParams()
        {
            var container = $('#AdvancedParametersConteiner');
            var label = $('#lblAdvacnedParameters');
            var img = $('#imgAdvacnedParameters');

            var isVisible = container.is(':visible');

            if(isVisible)
            {
                container.hide();
                label.text("<%=Resources.NCMWebContent.DeviceTemplateWizard_ShowAdvancedOptionsButtonText %>");
                img.attr("src", "/Orion/NCM/Resources/images/arrowright.gif");               
            }
            else
            {
                container.show();
                label.text("<%=Resources.NCMWebContent.DeviceTemplateWizard_HideAdvancedOptionsButtonText %>");
                img.attr("src", "/Orion/NCM/Resources/images/arrowdown.gif");
            }
        }

        $(document).ready(function () {
            initializeValidators();
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=SolarWinds.NCMModule.Web.Resources.DeviceTemplateManagement.DeviceTemplateWizardController.GetPageTitle(true) %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <ncm:WizardContainer ID="wizard" runat="server" OnNextClick="Next_Click">
        <Content>
            <div class="ncm-main-content">
                <asp:UpdatePanel ID="ConnectionProfileUpdatePanel" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="ncm-block-header">
                                        <%=Resources.NCMWebContent.WEBDATA_VK_859 %>
                                    </td>
                                    <td style="text-align:right;">
                                        <img src="/Orion/NCM/Resources/images/ConfigSnippets/icon_editsnippet.gif" alt="" />
                                        <a href="/Orion/NCM/Admin/ConnectionProfiles/ProfileManagement.aspx" class="ncm-link" target="_blank">
                                            <%=Resources.NCMWebContent.WEBDATA_VK_857 %>
                                        </a>
                                    </td>
                                </tr>
                            </table>
                            <table id="connectionProfileTable" runat="server" class="ncm-blue-box">
                                <tr>
                                    <td class="ncm-left-label-column">
                                    <%=Resources.NCMWebContent.WEBDATA_VK_860 %>
                                    </td>
                                    <td class="ncm-right-input-column ncm-input-value-column">
                                        <asp:DropDownList ID="GlobalConnectionProfile" runat="server" AutoPostBack="true" OnSelectedIndexChanged="GlobalConnectionProfile_SelectedIndexChanged" Width="225px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="ncm-right-input-column" colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="ncm-left-label-column">
                                        <%=Resources.NCMWebContent.WEBDATA_VK_295 %>
                                    </td>
                                    <td class="ncm-right-input-column ncm-input-value-column">
                                        <asp:DropDownList ID="UseUserDeviceCredentials" runat="server" AutoPostBack="true" OnSelectedIndexChanged="UseUserDeviceCredentials_SelectedIndexChanged" Width="225px">
                                            <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_317 %>" Value="True"></asp:ListItem>
                                            <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_318 %>" Value="False" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="ncm-right-input-column" colspan="2">&nbsp;</td>
                                </tr>
                                <tr visible-col-index="2">
                                    <td class="ncm-left-label-column">
                                        <%=Resources.NCMWebContent.WEBDATA_VK_296 %>
                                    </td>
                                    <td class="ncm-right-input-column ncm-input-value-column">
                                        <ncm:PasswordTextBox ID="Username" runat="server" Width="220px"/>
                                    </td>
                                    <td class="ncm-right-input-column ncm-macro-value-column">
                                        <asp:Literal ID="UsernameValue" runat="server"></asp:Literal>
                                    </td>
                                    <td class="ncm-right-input-column">
                                        <asp:LinkButton runat="server" ID="GlobalUsername" controlId="Username" macroValue="${GlobalUsername}" Font-Underline="true" ForeColor="blue" OnClick="ResetToGlobalMacro_Click"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr visible-col-index="2">
                                    <td class="ncm-left-label-column">
                                        <%=Resources.NCMWebContent.WEBDATA_VK_297 %>
                                    </td>
                                    <td class="ncm-right-input-column ncm-input-value-column">
                                        <ncm:PasswordTextBox ID="Password" runat="server" Width="220px"/>
                                    </td>
                                    <td class="ncm-right-input-column ncm-macro-value-column">&nbsp;</td>
                                    <td class="ncm-right-input-column">
                                        <asp:LinkButton runat="server" ID="GlobalPassword" controlId="Password" macroValue="${GlobalPassword}" Font-Underline="true" ForeColor="blue" OnClick="ResetToGlobalMacro_Click"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr visible-col-index="2">
                                    <td class="ncm-left-label-column">
                                        <%=Resources.NCMWebContent.WEBDATA_VK_298 %>
                                    </td>
                                    <td class="ncm-right-input-column ncm-input-value-column">
                                        <asp:DropDownList ID="EnableLevel" runat="server" Width="225px">
                                            <asp:ListItem Text="${GlobalEnableLevel}" Value="${GlobalEnableLevel}" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_314 %>" Value="<No Enable Login>"></asp:ListItem>
                                            <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_315 %>" Value="enable"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:HiddenField runat="server" ID="HiddenEnableLevel" />
                                    </td>
                                    <td class="ncm-right-input-column ncm-macro-value-column">
                                        <asp:Literal ID="EnableLevelValue" runat="server"></asp:Literal>
                                    </td>
                                    <td class="ncm-right-input-column">&nbsp;</td>
                                </tr>
                                <tr visible-col-index="2">
                                    <td class="ncm-left-label-column">
                                        <%=Resources.NCMWebContent.WEBDATA_VK_299 %>
                                    </td>
                                    <td class="ncm-right-input-column ncm-input-value-column">
                                        <ncm:PasswordTextBox ID="EnablePassword" runat="server" Width="220px"/>
                                    </td>
                                    <td class="ncm-right-input-column ncm-macro-value-column">&nbsp;</td>
                                    <td class="ncm-right-input-column">
                                        <asp:LinkButton runat="server" ID="GlobalEnablePassword" controlId="EnablePassword" macroValue="${GlobalEnablePassword}" Font-Underline="true" ForeColor="blue" OnClick="ResetToGlobalMacro_Click"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr visible-col-index="2">
                                    <td class="ncm-left-label-column">
                                        <%=Resources.NCMWebContent.WEBDATA_VK_302 %>
                                    </td>
                                    <td class="ncm-right-input-column ncm-input-value-column">
                                        <asp:DropDownList ID="ExecProtocol" runat="server" Width="225px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="ncm-right-input-column ncm-macro-value-column">
                                        <asp:Literal ID="ExecProtocolValue" runat="server"></asp:Literal>
                                    </td>
                                    <td class="ncm-right-input-column">&nbsp;</td>
                                </tr>
                                <tr visible-col-index="2">
                                    <td class="ncm-left-label-column">
                                        <%=Resources.NCMWebContent.WEBDATA_VK_303 %>
                                    </td>
                                    <td class="ncm-right-input-column ncm-input-value-column">
                                        <asp:DropDownList ID="CommandProtocol" runat="server" AutoPostBack="true" OnSelectedIndexChanged="CommandProtocol_SelectedIndexChanged" Width="225px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="ncm-right-input-column ncm-macro-value-column">
                                        <asp:Literal ID="CommandProtocolValue" runat="server"></asp:Literal>
                                    </td>
                                    <td class="ncm-right-input-column">&nbsp;</td>
                                </tr>
                                <tr visible-col-index="2">
                                    <td class="ncm-left-label-column">
                                        <%=Resources.NCMWebContent.WEBDATA_VK_304 %>
                                    </td>
                                    <td class="ncm-right-input-column ncm-input-value-column">
                                        <asp:DropDownList ID="TransferProtocol" runat="server" Width="225px"/>
                                    </td>
                                    <td class="ncm-right-input-column ncm-macro-value-column">
                                        <asp:Literal ID="TransferProtocolValue" runat="server"></asp:Literal>
                                    </td>
                                    <td class="ncm-right-input-column">&nbsp;</td>
                                </tr>
                                <tr visible-col-index="2">
                                    <td class="ncm-left-label-column">
                                        <%=Resources.NCMWebContent.WEBDATA_VK_305 %>
                                    </td>
                                    <td class="ncm-right-input-column ncm-input-value-column">
                                        <asp:TextBox runat="server" ID="TelnetPort" Width="220px"></asp:TextBox>
                                    </td>
                                    <td class="ncm-right-input-column ncm-macro-value-column">
                                        <asp:Literal ID="TelnetPortValue" runat="server"></asp:Literal>
                                    </td>
                                    <td class="ncm-right-input-column">
                                        <asp:LinkButton runat="server" ID="GlobalTelnetPort" controlId="TelnetPort" macroValue="${GlobalTelnetPort}" Font-Underline="true" ForeColor="blue" OnClick="ResetToGlobalMacro_Click"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr visible-col-index="2">
                                    <td class="ncm-left-label-column">
                                        <%=Resources.NCMWebContent.WEBDATA_VK_306 %>
                                    </td>
                                    <td class="ncm-right-input-column ncm-input-value-column">
                                        <asp:TextBox runat="server" ID="SSHPort" Width="220px"></asp:TextBox>
                                    </td>
                                    <td class="ncm-right-input-column ncm-macro-value-column">
                                        <asp:Literal ID="SSHPortValue" runat="server"></asp:Literal>
                                    </td>
                                    <td class="ncm-right-input-column">
                                        <asp:LinkButton runat="server" ID="GlobalSSHPort" controlId="SSHPort" macroValue="${GlobalSSHPort}" Font-Underline="true" ForeColor="blue" OnClick="ResetToGlobalMacro_Click"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr id="ShowMacroValuesButtonHolder" runat="server">
                                    <td class="ncm-left-label-column">&nbsp;</td>
                                    <td class="ncm-right-input-column">
                                        <orion:LocalizableButton runat="server" ID="ShowMacroValues" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_964 %>" OnClick="ShowMacroValues_Click" DisplayType="Small" CausesValidation="false" />
                                    </td>
                                    <td class="ncm-right-input-column">&nbsp;</td>
                                </tr>
                            </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="ncm-block-header">
                    <%=Resources.NCMWebContent.DeviceTemplateWizard_DeviceTemplateLabel %>
                </div>
                <div class="ncm-blue-box ncm-deviceTemplateTable-container">
                    <table id="deviceTemplateTable" runat="server">
                        <tr>
                            <td class="ncm-left-label-column">
                                <%=Resources.NCMWebContent.DeviceTemplateWizard_ResetTerminalSizeCommandLabel %>
                            </td>
                            <td colspan="2" class="ncm-right-input-column">
                                <asp:TextBox runat="server" ID="Reset" Width="100%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="ncm-left-label-column"></td>
                            <td colspan="2" class="ncm-right-input-column ncm-hint">
                                <%=Resources.NCMWebContent.DeviceTemplateWizard_ResetTerminalSizeCommandNote %>
                            </td>
                        </tr>
                        <tr>
                            <td class="ncm-left-label-column">
                                <%=Resources.NCMWebContent.DeviceTemplateWizard_ShowVersionCommandLabel %>
                            </td>
                            <td colspan="2" class="ncm-right-input-column">
                                <asp:TextBox runat="server" ID="Version" Width="100%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="ncm-left-label-column"></td>
                            <td colspan="2" class="ncm-right-input-column ncm-hint">
                                <%=Resources.NCMWebContent.DeviceTemplateWizard_ShowVersionCommandNote %>
                            </td>
                        </tr>
                            <tr>
                            <td class="ncm-left-label-column"></td>
                            <td colspan="2" class="ncm-right-input-column" style="padding-top: 5px;">
                                <asp:LinkButton runat="server" ID="lnkAdvacnedParameters" ClientIDMode="Static" OnClientClick="ToggleAddvancedParams(); return false;" style="vertical-align:middle;" Font-Underline="false" Font-Size="8pt" ForeColor="blue">
                                    <img alt="" id="imgAdvacnedParameters" ClientIDMode="Static" style="vertical-align:middle;" runat="server" src="/Orion/NCM/Resources/images/arrowright.gif" />
                                    <asp:Label ID="lblAdvacnedParameters" ClientIDMode="Static" runat="server" style="vertical-align:middle;padding-left:5px;" Text="<%$ Resources: NCMWebContent, DeviceTemplateWizard_ShowAdvancedOptionsButtonText %>"></asp:Label>
                                </asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                    <div id="AdvancedParametersConteiner" ClientIDMode="Static" runat="server" style="display: none">
                        <table id="AdvancedParameters" runat="server">
                            <tr>
                                <td class="ncm-left-label-column">
                                    <%=Resources.NCMWebContent.DeviceTemplateWizard_UserNamePromptLabel %>
                                </td>
                                <td class="ncm-prompt-radio-button-column">
                                    <asp:RadioButton ID="AutoDetectUserNamePrompt" onchange="ToggleField('CustomUserNamePrompt','UserNamePrompt','rfvUserNamePrompt')" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_865 %>" promptType="AutoDetect" childControlId="UserNamePrompt" GroupName="UserNamePromptGroup"/>
                                    <asp:RadioButton ClientIDMode="Static" ID="CustomUserNamePrompt" onchange="ToggleField('CustomUserNamePrompt','UserNamePrompt', 'rfvUserNamePrompt')" runat="server" Text="<%$ Resources: NCMWebContent, DeviceTemplateWizard_CustomRadioButtonText %>" promptType="Custom" childControlId="UserNamePrompt" GroupName="UserNamePromptGroup"/>
                                </td>
                                <td class="ncm-right-input-column ncm-prompt-textbox-column">
                                    <asp:TextBox ClientIDMode="Static" runat="server" ID="UserNamePrompt" Width="100%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ClientIDMode="Static" ID="rfvUserNamePrompt" ControlToValidate="UserNamePrompt" Display="Static" ErrorMessage="<%$ Resources: NCMWebContent, DeviceTemplateWizard_ValueIsRequiredMessage %>" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="ncm-left-label-column">
                                    <%=Resources.NCMWebContent.DeviceTemplateWizard_PasswordPromptLabel %>
                                </td>
                                <td class="ncm-prompt-radio-button-column">
                                    <asp:RadioButton ID="AutoDetectPasswordPrompt" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_865 %>" onchange="ToggleField('CustomPasswordPrompt','PasswordPrompt', 'rfvPasswordPrompt')" promptType="AutoDetect" childControlId="PasswordPrompt" fieldValidatorControlId="rfvPasswordPrompt" GroupName="PasswordPromptGroup"/>
                                    <asp:RadioButton ID="CustomPasswordPrompt" ClientIDMode="Static" runat="server" Text="<%$ Resources: NCMWebContent, DeviceTemplateWizard_CustomRadioButtonText %>" onchange="ToggleField('CustomPasswordPrompt','PasswordPrompt', 'rfvPasswordPrompt')" promptType="Custom" childControlId="PasswordPrompt" fieldValidatorControlId="rfvPasswordPrompt" GroupName="PasswordPromptGroup"/>
                                </td>
                                <td class="ncm-right-input-column ncm-prompt-textbox-column">
                                    <asp:TextBox runat="server" ID="PasswordPrompt" ClientIDMode="Static" Width="100%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvPasswordPrompt" ClientIDMode="Static" ControlToValidate="PasswordPrompt" Display="Dynamic" ErrorMessage="<%$ Resources: NCMWebContent, DeviceTemplateWizard_ValueIsRequiredMessage %>" runat="server"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="ncm-left-label-column">
                                    <%=Resources.NCMWebContent.DeviceTemplateWizard_EnableIdentifierLabel %>
                                </td>
                                <td class="ncm-prompt-radio-button-column">
                                    <asp:RadioButton ID="AutoDetectEnableIdentifier" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_865 %>" onchange="ToggleField('CustomEnableIdentifier','EnableIdentifier', 'rfvEnableIdentifier')" promptType="AutoDetect" childControlId="EnableIdentifier" fieldValidatorControlId="rfvEnableIdentifier" GroupName="EnableIdentifierGroup"/>
                                    <asp:RadioButton ID="CustomEnableIdentifier" ClientIDMode="Static" runat="server" Text="<%$ Resources: NCMWebContent, DeviceTemplateWizard_CustomRadioButtonText %>" onchange="ToggleField('CustomEnableIdentifier','EnableIdentifier', 'rfvEnableIdentifier')" promptType="Custom" childControlId="EnableIdentifier" fieldValidatorControlId="rfvEnableIdentifier" GroupName="EnableIdentifierGroup"/>
                                </td>
                                <td class="ncm-right-input-column ncm-prompt-textbox-column">
                                    <asp:TextBox runat="server" ID="EnableIdentifier" ClientIDMode="Static" Width="100%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvEnableIdentifier" ClientIDMode="Static" ControlToValidate="EnableIdentifier" Display="Dynamic" ErrorMessage="<%$ Resources: NCMWebContent, DeviceTemplateWizard_ValueIsRequiredMessage %>" runat="server"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="ncm-left-label-column">
                                    <%=Resources.NCMWebContent.DeviceTemplateWizard_EnableCommandLabel %>
                                </td>
                                <td class="ncm-prompt-radio-button-column">
                                    <asp:RadioButton ID="AutoDetectEnableCommand" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_865 %>" onchange="ToggleField('CustomEnableCommand','EnableCommand', 'rfvEnableCommand')" promptType="AutoDetect" childControlId="EnableCommand" fieldValidatorControlId="rfvEnableCommand" GroupName="EnableCommandGroup"/>
                                    <asp:RadioButton ID="CustomEnableCommand" ClientIDMode="Static" runat="server" Text="<%$ Resources: NCMWebContent, DeviceTemplateWizard_CustomRadioButtonText %>" onchange="ToggleField('CustomEnableCommand','EnableCommand', 'rfvEnableCommand')" promptType="Custom" childControlId="EnableCommand" fieldValidatorControlId="rfvEnableCommand" GroupName="EnableCommandGroup"/>
                                </td>
                                <td class="ncm-right-input-column ncm-prompt-textbox-column">
                                    <asp:TextBox runat="server" ID="EnableCommand" ClientIDMode="Static" Width="100%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvEnableCommand" ClientIDMode="Static" ControlToValidate="EnableCommand" Display="Dynamic" ErrorMessage="<%$ Resources: NCMWebContent, DeviceTemplateWizard_ValueIsRequiredMessage %>" runat="server"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="ncm-left-label-column">
                                    <%=Resources.NCMWebContent.DeviceTemplateWizard_CommandModePromptLabel %>
                                </td>
                                <td class="ncm-prompt-radio-button-column">
                                    <asp:RadioButton ID="AutoDetectVirtualPrompt" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_865 %>" onchange="ToggleField('CustomVirtualPrompt','VirtualPrompt', 'rfvVirtualPrompt')" promptType="AutoDetect" childControlId="VirtualPrompt" fieldValidatorControlId="rfvVirtualPrompt" GroupName="VirtualPromptGroup"/>
                                    <asp:RadioButton ID="CustomVirtualPrompt" ClientIDMode="Static" runat="server" Text="<%$ Resources: NCMWebContent, DeviceTemplateWizard_CustomRadioButtonText %>" onchange="ToggleField('CustomVirtualPrompt','VirtualPrompt', 'rfvVirtualPrompt')" promptType="Custom" childControlId="VirtualPrompt" fieldValidatorControlId="rfvVirtualPrompt" GroupName="VirtualPromptGroup"/>
                                </td>
                                <td class="ncm-right-input-column ncm-prompt-textbox-column">
                                    <asp:TextBox runat="server" ID="VirtualPrompt" ClientIDMode="Static" Width="100%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvVirtualPrompt" ClientIDMode="Static" ControlToValidate="VirtualPrompt" Display="Dynamic" ErrorMessage="<%$ Resources: NCMWebContent, DeviceTemplateWizard_ValueIsRequiredMessage %>" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="ncm-left-label-column">
                                    <%=Resources.NCMWebContent.DeviceTemplateWizard_MorePromptLabel %>
                                </td>
                                <td class="ncm-prompt-radio-button-column">
                                    <asp:RadioButton ID="AutoDetectMore" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_865 %>" onchange="ToggleField('CustomMore','More', 'rfvMore')" promptType="AutoDetect" childControlId="More" fieldValidatorControlId="rfvMore" GroupName="MoreGroup"/>
                                    <asp:RadioButton ID="CustomMore" ClientIDMode="Static" runat="server" Text="<%$ Resources: NCMWebContent, DeviceTemplateWizard_CustomRadioButtonText %>" onchange="ToggleField('CustomMore','More', 'rfvMore')" promptType="Custom" childControlId="More" fieldValidatorControlId="rfvMore" GroupName="MoreGroup"/>
                                </td>
                                <td class="ncm-right-input-column ncm-prompt-textbox-column">
                                    <asp:TextBox runat="server" ID="More" ClientIDMode="Static" Width="100%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvMore" ClientIDMode="Static" ControlToValidate="More" Display="Dynamic" ErrorMessage="<%$ Resources: NCMWebContent, DeviceTemplateWizard_ValueIsRequiredMessage %>" runat="server"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="ncm-left-label-column">&nbsp;</td>
                                <td colspan="2" class="ncm-right-input-column">
                                    &#0187;&nbsp;<a class="ncm-helplink" href="<%= LearnMore(@"OrionNCMPHDeviceTemplateParamsHelpMe") %>" target="_blank"><%=Resources.NCMWebContent.DeviceTemplateWizard_DeviceTemplateParamsHelpMeLink %></a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <br/>
                <asp:UpdatePanel ID="TestAccessUpdatePanel" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                        <div class="ncm-blue-box ncm-deviceTemplateTable-container">
                            <table id="Table1" runat="server">
                                <tr>
                                    <td class="ncm-left-label-column"><%=Resources.NCMWebContent.DeviceTemplateWizard_TestDeviceAccessLabel %></td>
                                    <td class="ncm-right-input-column">
                                        <orion:LocalizableButton runat="server" ID="ValidateLogin" LocalizedText="Test" OnClientClick="HideDeviceValidationBar();" OnClick="ValidateLogin_Click" DisplayType="Small" />
                                        <span style="padding-left:5px;">
                                            &#0187;&nbsp;<a class="ncm-helplink" href="<%=LearnMore(@"OrionNCMPHDeviceTemplateTestWhatValidate")%>" target="_blank"><%=Resources.NCMWebContent.DeviceTemplateWizard_WhatDoesThisTestValidateLink %></a>
                                        </span>
                                    </td>
                                </tr>
                                <tr id="deviceValidationHolder" runat="server">
	                                <td class="ncm-left-label-column">&nbsp;</td>
	                                <td class="ncm-right-input-column">
                                        <div id="UpdatePogressBar" runat="server" style="display:none;">
                                            <asp:UpdateProgress runat="server" ID="UpdateProgress1" DynamicLayout="false">
			                                    <ProgressTemplate>
				                                    <img src="/Orion/NCM/Resources/images/Animated.Loading.gif" />
				                                    <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_11 %>" />
			                                    </ProgressTemplate>
		                                    </asp:UpdateProgress>
                                        </div>

                                        <div id="DeviceValidationBar" runat="server" style="display:none;width:100%">
                                            <div runat="server" id="DeviceValidationInfo" style="padding:3px; text-align:left; margin:5px 0px 5px 0px;" visible="false">
                                                <img alt="" runat="server" id="DeviceValidationIMG" style="vertical-align:middle;" />
                                                <asp:Label ID="DeviceValidationResult" runat="server" style="vertical-align:middle;" />
                                            </div>

                                            <div id="DeviceValidationDetailsSPAN" runat="server" style="padding-bottom:5px;" visible="false">
                                                <asp:LinkButton runat="server" ID="DeviceValidationDetailsBTN" style="vertical-align:middle;" Font-Underline="false" Font-Size="8pt" ForeColor="blue" OnClick="DeviceValidationDetailsBTN_Click">
                                                    <img alt="" id="DeviceValidationDetailsIMG" style="vertical-align:middle;" runat="server" src="/Orion/NCM/Resources/images/arrowright.gif" />
                                                    <asp:Label ID="DeviceValidationDetailsLBL" runat="server" style="vertical-align:middle;padding-left:5px;"></asp:Label>
                                                </asp:LinkButton>
                                            </div>
                    
                                            <div id="TraceOutput" runat="server" style="padding-bottom:5px;">
		                                        <asp:TextBox runat="server" BackColor="#E6E6DD" ForeColor="#5D5C5E" ID="DeviceValidationOutput" TextMode="MultiLine" Font-Names="Arial,Verdana,Helvetica,sans-serif" Font-Size="9pt" ReadOnly="True" Height="150px" Width="400px" spellcheck="false"></asp:TextBox>
	                                        </div>
                                        </div>
	                                </td>
                                </tr>
                            </table>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </Content>
    </ncm:WizardContainer>
</asp:Content>

