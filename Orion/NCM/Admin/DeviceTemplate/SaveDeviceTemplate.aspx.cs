﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;

using SolarWinds.InformationService.Contract2;
using SolarWinds.NCM.Common.Help;
using SolarWinds.NCM.Common.Transfer;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.DeviceTemplateManagement;
using SolarWinds.Orion.Cli.IoC;
using SolarWinds.Orion.Cli.Contracts;
using SolarWinds.Orion.Cli.Contracts.DataModel;

public partial class Orion_NCM_Admin_DeviceTemplate_SaveDeviceTemplate : System.Web.UI.Page
{
    private AdvToobarNodes toolBarNodes = new AdvToobarNodes();

    protected bool ApplyToBasicSelectedNode
    {
        get { return Convert.ToBoolean(ViewState["NCM_ApplyToBasicSelectedNode"]); }
        set { ViewState["NCM_ApplyToBasicSelectedNode"] = value; }
    }

    protected bool IsCreateMode
    {
        get { return DeviceTemplateWizardController.NewDeviceTemplate; }
    }

    protected override void OnInit(EventArgs e)
    {
        var resourceSettings = new ResourceSettings();
        resourceSettings.Prefix = "SaveDeviceTemplatePicker";
        NPMDefaultSettingsHelper.SetUserSettings("SaveDeviceTemplatePickerGroupBy", "Vendor");

        toolBarNodes.ResourceSettings = resourceSettings;
        toolBarNodes.Visible = true;
        toolBarNodes.ShowClearAll = true;
        toolBarNodes.ShowSelectAll = true;
        toolBarNodes.NodesControl.ShowConfigs = false;
        toolBarNodes.NodesControl.ShowGroupCheckBoxes = true;
        toolBarNodes.NodesControl.ShowNodeCheckBoxes = true;
        toolBarNodes.NodesControl.ExcludeUnmanagedNodes = false;
        toolBarNodes.NodesControl.TreeControl.ClientSideEvents.NodeChecked = @"UltraWebTree_TheeLevelNodeCheckedDownloadConfig";
        nodes_picker_container.Controls.Add(toolBarNodes);

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = DeviceTemplateWizardController.GetPageTitle();

        if (!Page.IsPostBack)
        {
            var deviceTemplate = GetDeviceTemplate();

            DeviceTemplateName.Text = DeviceTemplateWizardController.IsDefaultTemplate
                ? String.Format(Resources.NCMWebContent.DeviceTemplateWizard_CopyOf, deviceTemplate.Name)
                : deviceTemplate.Name;
            SystemOID.Text = deviceTemplate.SystemOID;

           
            SystemDescriptionRegex.Text = deviceTemplate.SystemDescriptionRegex;
            ddlUseForAutoDetect.SelectedValue = (DeviceTemplateWizardController.IsDefaultTemplate || !deviceTemplate.UseForAutoDetect)
                ? @"No"
                : @"Yes";
            rbAutoDetectType.SelectedValue = Convert.ToString((int)deviceTemplate.AutoDetectType);
            Comments.Text = deviceTemplate.Comments;

            toolBarNodes.RemoveNodes();
            DownloadConfigs_CheckedChanged(DownloadConfigs, EventArgs.Empty);
            ApplyToBasicSelectedNode = true;
        }
    }

    protected void Next_Click(object sender, DeviceTemplateEventArgs e)
    {
        var deviceTemplateId = 0;
        if (IsCreateMode)
            deviceTemplateId = CreateNewDeviceTemplate();
        else
        {
            if (DeviceTemplateWizardController.IsDefaultTemplate)
                deviceTemplateId = CreateNewDeviceTemplate();
            else
                deviceTemplateId = UpdateDeviceTemplate();
        }

        UpdateSelectedNodes(AssignConnectionProfile.Checked, deviceTemplateId);

        if (DownloadConfigs.Checked)
            DownloadConfigsForSelectNodes();
    }

    protected void DownloadConfigs_CheckedChanged(object sender, EventArgs e)
    {
        if (DownloadConfigs.Checked)
            ConfigTypes.Enabled = true;
        else
            ConfigTypes.Enabled = false;
    }

    protected void RemoveNodeButton_Click(object sender, EventArgs e)
    {
        rowAssignedNode.Visible = false;
        ApplyToBasicSelectedNode = false;
    }

    protected string GetNodeName()
    {
        var isWrapper = new ISWrapper();
        using (var proxy = isWrapper.Proxy)
        {
            var dt = proxy.Query(@"SELECT Caption FROM Orion.Nodes WHERE NodeID=@nodeId", new PropertyBag() { { @"nodeId", DeviceTemplateWizardController.NCMNode.CoreNodeID } });
            if (dt != null && dt.Rows.Count > 0)
                return Convert.ToString(dt.Rows[0][0], CultureInfo.InvariantCulture);
            return string.Empty;
        }
    }

    private DeviceTemplate CreateDeviceTemplate(bool createNew)
    {
        var deviceTemplateSerializer = ServiceLocator.Container.Resolve<IDeviceTemplateSerializer>();

        var newDeviceTemplate = createNew ? new DeviceTemplate() : GetDeviceTemplate();
        newDeviceTemplate.Name = DeviceTemplateName.Text;
        newDeviceTemplate.SystemOID = SystemOID.Text;
        newDeviceTemplate.SystemDescriptionRegex = SystemDescriptionRegex.Text;
        newDeviceTemplate.TemplateXml =
            deviceTemplateSerializer.SerializeXml(DeviceTemplateWizardController.DeviceCommands, false);
        newDeviceTemplate.Comments = Comments.Text;
        newDeviceTemplate.UseForAutoDetect = ddlUseForAutoDetect.SelectedValue.Equals(@"Yes", StringComparison.InvariantCultureIgnoreCase) ? true : false;
        newDeviceTemplate.AutoDetectType = (eAutoDetectType)Enum.Parse(typeof(eAutoDetectType), rbAutoDetectType.SelectedValue);
        newDeviceTemplate.Author = HttpContext.Current.Profile.UserName;
        newDeviceTemplate.IsDefault = false;
        return newDeviceTemplate;
    }

    private int CreateNewDeviceTemplate()
    {
        var mng = ServiceLocator.Container.Resolve<IDeviceTemplatesManager>();

        var deviceTemplate = CreateDeviceTemplate(true);
        return mng.AddDeviceTemplate(deviceTemplate);
    }

    private int UpdateDeviceTemplate()
    {
        var mng = ServiceLocator.Container.Resolve<IDeviceTemplatesManager>();

        var deviceTemplate = CreateDeviceTemplate(false);
        mng.UpdateDeviceTemplate(deviceTemplate);
        return deviceTemplate.Id;
    }

    private void UpdateSelectedNodes(bool assignConnectionProfile, int deviceTemplateId)
    {
        var isWrapper = new ISWrapper();
        using (var proxy = isWrapper.Proxy)
        {
            var mng = ServiceLocator.Container.Resolve<IDeviceTemplatesManager>();

            if (ApplyToBasicSelectedNode)
            {
                proxy.Cirrus.Nodes.UpdateNode(DeviceTemplateWizardController.NCMNode);

                if (deviceTemplateId > 0)
                    mng.SetTemplateForNode(DeviceTemplateWizardController.NCMNode.CoreNodeID, deviceTemplateId);
            }

            var nodeIds = GetSelectedNodeIds();
            if (nodeIds.Count() > 0)
            {
                foreach (var nodeId in nodeIds)
                {
                    var ncmNode = proxy.Cirrus.Nodes.GetNode(nodeId);

                    if (deviceTemplateId > 0)
                        mng.SetTemplateForNode(ncmNode.CoreNodeID, deviceTemplateId);

                    if (assignConnectionProfile)
                    {
                        ncmNode.ConnectionProfile = DeviceTemplateWizardController.NCMNode.ConnectionProfile;
                        ncmNode.UseUserDeviceCredentials = DeviceTemplateWizardController.NCMNode.UseUserDeviceCredentials;
                        ncmNode.Username = DeviceTemplateWizardController.NCMNode.Username;
                        ncmNode.Password = DeviceTemplateWizardController.NCMNode.Password;
                        ncmNode.EnableLevel = DeviceTemplateWizardController.NCMNode.EnableLevel;
                        ncmNode.EnablePassword = DeviceTemplateWizardController.NCMNode.EnablePassword;
                        ncmNode.ExecProtocol = DeviceTemplateWizardController.NCMNode.ExecProtocol;
                        ncmNode.CommandProtocol = DeviceTemplateWizardController.NCMNode.CommandProtocol;
                        ncmNode.TransferProtocol = DeviceTemplateWizardController.NCMNode.TransferProtocol;
                        ncmNode.TelnetPort = DeviceTemplateWizardController.NCMNode.TelnetPort;
                        ncmNode.SSHPort = DeviceTemplateWizardController.NCMNode.SSHPort;
                        ncmNode.AllowIntermediary = DeviceTemplateWizardController.NCMNode.AllowIntermediary;
                        ncmNode.UseKeybInteractiveAuth = DeviceTemplateWizardController.NCMNode.UseKeybInteractiveAuth;
                    }
                    proxy.Cirrus.Nodes.UpdateNode(ncmNode);
                }
            }
        }
    }

    private void DownloadConfigsForSelectNodes()
    {
        var nodeIds = GetAllNodeIds();
        if (nodeIds.Any())
        {
            var isWrapper = new ISWrapper();
            using (var proxy = isWrapper.Proxy)
            {
                proxy.Cirrus.ConfigArchive.DownloadConfig(nodeIds.ToArray(), ConfigTypes.SelectedValue);
            }
        }
    }

    private IEnumerable<Guid> GetAllNodeIds()
    {
        var nodeIds = GetSelectedNodeIds().ToList();
        nodeIds.Add(DeviceTemplateWizardController.NCMNode.NodeID);
        return nodeIds.Distinct();
    }

    private IEnumerable<Guid> GetSelectedNodeIds()
    {
        var nodeIds = toolBarNodes.NodesControl.GetCheckedNodeIds();
        return nodeIds;
    }

    private DeviceTemplate GetDeviceTemplate()
    {
        if (IsCreateMode)
        {
            return new DeviceTemplate()
            {
                UseForAutoDetect = false,
                Comments = string.Empty,
                SystemDescriptionRegex = string.Empty,
                AutoDetectType = eAutoDetectType.BySystemOid
            };
        }

        var mng = ServiceLocator.Container.Resolve<IDeviceTemplatesManager>();
        var id = Convert.ToInt32(Page.Request.QueryString[@"Id"], CultureInfo.InvariantCulture);
        var deviceTemplate = mng.GetDeviceTemplate(id);
        return deviceTemplate;
    }

    protected string LearnMoreAssignDeviceTemplate
    {
        get
        {
            var helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
            return helpHelperWrap.GetHelpUrl("OrionNCMPHHelpAssignDeviceTemplate");
        }
    }
}