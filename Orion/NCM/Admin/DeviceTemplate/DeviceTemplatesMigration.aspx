﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="DeviceTemplatesMigration.aspx.cs" Inherits="Orion_NCM_Admin_DeviceTemplate_DeviceTemplatesMigration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/Settings/Settings.css" />
    <style type="text/css">
        .cell {
            padding: 10px;
        }
        .cell a:hover {
            text-decoration: underline;
        }
     </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <div class="ContentContainer">
        <div class="SettingContainer">
            <div>
                <%=Resources.NCMWebContent.DeviceTemplate_DeviceTemplatesMigration_Heading1 %>
            </div>
            <div>
                <%=Resources.NCMWebContent.DeviceTemplate_DeviceTemplatesMigration_Heading2 %>
            </div>
            <div style="padding-top:10px;">
                <asp:Repeater ID="repeater" runat="server">
                    <HeaderTemplate>                            
                        <table class="NeedsZebraStripes" border="0" cellpadding="0" cellspacing="0" width="100%">
                    </HeaderTemplate>
                    <ItemTemplate>
                            <tr>
                                <td class="cell">
                                    <%=String.Format(Resources.NCMWebContent.DeviceTemplate_DeviceTemplatesMigration_ItemTemplate,
                                           String.Concat(@"<a href='", 
                                               string.Format(@"/Orion/Nodes/NodeProperties.aspx?Nodes={0}&GuidID={1}", 
                                                   Eval(@"nodeId"), 
                                                   Guid.NewGuid()),
                                               @"' target='_blank'>",
                                               Eval(@"nodeCaption"),
                                               @"</a>"),
                                           Eval(@"cliTemplate"),
                                           Eval(@"ncmTemplate")) %>
                                </td>
                            </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>                            
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
</asp:Content>

