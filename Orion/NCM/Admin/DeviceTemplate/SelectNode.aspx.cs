﻿using System;
using System.Linq;
using System.Web.UI.WebControls;

using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.DeviceTemplateManagement;

public partial class Orion_NCM_Admin_DeviceTemplate_SelectNode : System.Web.UI.Page
{
    private AdvToobarNodes toolBarNodes = new AdvToobarNodes();

    protected override void OnInit(EventArgs e)
    {
        var resourceSettings = new ResourceSettings();
        resourceSettings.Prefix = "DeviceTemplatePicker";
        NPMDefaultSettingsHelper.SetUserSettings("DeviceTemplatePickerGroupBy", "Vendor");

        toolBarNodes.ResourceSettings = resourceSettings;
        toolBarNodes.Visible = true;
        toolBarNodes.GroupByChangeEvent = @"UltraWebTree_SingleNodeCheckedRefresh()";
        toolBarNodes.ShowClearAll = false;
        toolBarNodes.ShowSelectAll = false;
        toolBarNodes.NodesControl.ShowConfigs = false;
        toolBarNodes.NodesControl.ShowGroupCheckBoxes = false;
        toolBarNodes.NodesControl.ShowNodeCheckBoxes = true;
        toolBarNodes.NodesControl.ExcludeUnmanagedNodes = false;
        toolBarNodes.NodesControl.TreeControl.ClientSideEvents.NodeChecked = @"UltraWebTree_SingleNodeChecked";
        nodes_picker_container.Controls.Add(toolBarNodes);

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = DeviceTemplateWizardController.GetPageTitle(false, false);

        if (!Page.IsPostBack)
            toolBarNodes.RemoveNodes();
    }

    protected void NodeSelectionValidate(object source, ServerValidateEventArgs args)
    {
        var nodeIds = toolBarNodes.NodesControl.GetCheckedNodeIds();
        args.IsValid = nodeIds.Any();
    }

    protected void Next_Click(object sender, DeviceTemplateEventArgs e)
    {
        var nodeIds = toolBarNodes.NodesControl.GetCheckedNodeIds();
        if (nodeIds.Any())
        {
            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                var ncmNode = proxy.Cirrus.Nodes.GetNode(nodeIds.First());
                DeviceTemplateWizardController.NCMNode = ncmNode;
            }
            DeviceTemplateWizardController.ClearSessionNodeName();
            e.IsValid = true;
        }
        else
        {
            e.IsValid = false;
        }
    }
}