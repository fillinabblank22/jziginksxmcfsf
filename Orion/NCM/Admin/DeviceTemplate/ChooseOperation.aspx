﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="ChooseOperation.aspx.cs" Inherits="Orion_NCM_Admin_DeviceTemplate_ChooseOperation" %>

<%@ Register Src="~/Orion/NCM/Admin/DeviceTemplate/Controls/DeviceTemplateWizardContainer.ascx" TagName="WizardContainer" TagPrefix="ncm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/DeviceTemplate/DeviceTemplate.css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=SolarWinds.NCMModule.Web.Resources.DeviceTemplateManagement.DeviceTemplateWizardController.GetPageTitle(true) %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <ncm:WizardContainer ID="wizard" runat="server" OnNextClick="Next_Click">
        <Content>
            <div class="ncm-main-content">
                <div class="ncm-block-header">
                    <%= Resources.NCMWebContent.DeviceTemplateWizard_SelectOperationLabel %>
                </div>
                <div class="ncm-blue-box" style="padding: 0">
                    <div style="padding: 10px;">
                        <div>
                            <asp:RadioButton  runat="server" ID="rbExecute"  Text="<%$Resources:NCMWebContent, DeviceTemplateWizard_ExecuteScriptsCheckboxText%>" GroupName="Operations"/>
                        </div>
                        <div style="padding-top:10px;">
                            <asp:RadioButton runat="server" ID="rbExecuteAndDownload" Text="<%$Resources:NCMWebContent, DeviceTemplateWizard_ExecuteScriptsAndDownloadConfigCheckboxText%>" GroupName="Operations"/>
                        </div>
                        <div style="padding-top:10px;">
                            <asp:RadioButton runat="server" ID="rbExecuteAndDownloadAndUpload" Text="<%$Resources:NCMWebContent, DeviceTemplateWizard_ExecuteScriptsDownloadAndUploadConfigCheckboxText%>" GroupName="Operations"/>
                        </div>
                        <div style="padding-top:5px;">
                           <span style="padding-left:5px;">
                                                &#0187;&nbsp;<a class="ncm-helplink" href="<%=HelpMeChoose%>" target="_blank"><%= Resources.NCMWebContent.DeviceTemplateWizard_HelpMeChooseLink %></a>
                           </span>
                        </div>
                     </div>
                </div>
                <div style="padding-top: 10px;">
                   <%= string.Format(Resources.NCMWebContent.DeviceTemplateWizard_ChooseOperationStepNote, @"<br/>") %>
                </div>
            </div>
        </Content>
    </ncm:WizardContainer>
</asp:Content>

