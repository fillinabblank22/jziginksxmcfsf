﻿using System;
using Resources;
using SolarWinds.NCM.Common.Help;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.DeviceTemplateManagement;

public partial class Orion_NCM_Admin_DeviceTemplate_ChooseOperation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = DeviceTemplateWizardController.GetPageTitle();

        if (!Page.IsPostBack)
        {
            InitOperation();
        }
    }

    private void InitOperation()
    {
        DeviceTemplateWizardController.Operations value = DeviceTemplateWizardController.Operation;
        switch (value)
        {
            case DeviceTemplateWizardController.Operations.Execute:
                {
                    rbExecute.Checked = true;
                    break;
                }
            case DeviceTemplateWizardController.Operations.ExecuteAndDownload:
                {
                    rbExecuteAndDownload.Checked = true;
                    break;
                }
            case DeviceTemplateWizardController.Operations.ExecuteAndDownloadAndUpload:
                {
                    rbExecuteAndDownloadAndUpload.Checked = true;
                    break;
                }
        }
    }

    protected void Next_Click(object sender, DeviceTemplateEventArgs e)
    {
        SetOperation();
    }

    protected string HelpMeChoose
    {
        get
        {
            var helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
            return helpHelperWrap.GetHelpUrl("OrionNCMPHDeviceTemplateOpsHelpChoose");
        }
    }

    private void SetOperation()
    {
        if (rbExecute.Checked)
            DeviceTemplateWizardController.Operation = DeviceTemplateWizardController.Operations.Execute;
        else if (rbExecuteAndDownload.Checked)
            DeviceTemplateWizardController.Operation = DeviceTemplateWizardController.Operations.ExecuteAndDownload;
        else if (rbExecuteAndDownloadAndUpload.Checked)
            DeviceTemplateWizardController.Operation = DeviceTemplateWizardController.Operations.ExecuteAndDownloadAndUpload;
    }
}
