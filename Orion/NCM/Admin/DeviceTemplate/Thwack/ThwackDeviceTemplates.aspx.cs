using System;
using System.Web.UI;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_Admin_DeviceTemplates_Thwack_Default : Page
{
    protected string _thwackUserInfo = @"{name:'',pass:'', valid: false}";
    public bool IsDemoMode;
    private static readonly string THWACK_DEVICE_TEMPLATES_LIST_SESSION_KEY = @"thwack_devicetemplate_list";

    private readonly ICommonHelper commonHelper;

    public Orion_NCM_Admin_DeviceTemplates_Thwack_Default()
    {
        commonHelper = ServiceLocator.Container.Resolve<ICommonHelper>();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        SWISValidator validator = new SWISValidator();
        validator.ContentContainer = ContentContainer;
        validator.RedirectIfValidationFailed = true;
        ErrorContainer.Controls.Add(validator);

        if (validator.IsValid)
        {
            commonHelper.Initialise();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            Session.Remove(THWACK_DEVICE_TEMPLATES_LIST_SESSION_KEY);

        SetupCred();
    }

    #region private methods
    
    private void SetupCred()
    {
        System.Net.NetworkCredential cred = Session[@"NCM_ThwackCredential"] as System.Net.NetworkCredential;

        bool credValid;
        object vc = Session[@"NCM_ThwackCredValidation"];

        if (vc != null)
            credValid = (bool)vc;
        else
            credValid = false;

        if (cred != null)
        {
            this._thwackUserInfo = String.Format(@"{0}name:'{1}',pass:'{2}', valid:{4}{3}",
                                     @"{",
                                     cred.UserName.Replace(@"\", @"&#92;"),
                                     cred.Password.Replace(@"\", @"&#92;"),
                                     @"}",
                                     credValid.ToString().ToLower());
        }
    }

    #endregion
}
