﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="SaveDeviceTemplate.aspx.cs" Inherits="Orion_NCM_Admin_DeviceTemplate_SaveDeviceTemplate" %>

<%@ Register Src="~/Orion/NCM/Admin/DeviceTemplate/Controls/DeviceTemplateWizardContainer.ascx" TagName="WizardContainer" TagPrefix="ncm" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
    <script type="text/javascript" src="/Orion/NCM/JavaScript/main.js"></script>
    
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/DeviceTemplate/DeviceTemplate.css" />

    <style type="text/css">
        .ncm-node-caption-link
        {
            color: #336699 !important;
            cursor: pointer;
            font-family: Arial;
            text-decoration: underline;
        }
    
        .ncm-node-caption-link:hover
        {
            color: #f99d1c !important;
            text-decoration: none;
        }

        .ncm-resizable-comments-field {
            resize: vertical;
        }
        input[readonly] { 
            color: gray;
        }
    </style>

     <script type="text/javascript">
         $(document).ready(function () {
             var useForAutoDetect = $('#<%=ddlUseForAutoDetect.ClientID%>').val();
             showHideAutoDetectTypeHolder(useForAutoDetect);
         });

         function UseForAutoDetectChange(obj) {
             var value = obj.options[obj.selectedIndex].value;
             showHideAutoDetectTypeHolder(value);
         };

         var enableDisableValidator = function(enable) {
             var validator = document.getElementById("<%=rfvDeviceTemplateName.ClientID %>");
             ValidatorEnable(validator, enable);
         };

         var showHideAutoDetectTypeHolder = function(useForAutoDetect) {
             if (useForAutoDetect === "Yes")
                 $("#autoDetectTypeHolder").show();
             else
                 $("#autoDetectTypeHolder").hide();
         };

         var validateSystemDescriptionRegex = function (sender, args) {
             var useForAutoDetect = $('#<%=ddlUseForAutoDetect.ClientID%>').val();
             var autoDetectType = $("#rbAutoDetectType").find(":checked").val();
             var sysDescrRegexValue = $("#SystemDescriptionRegex").val();
             if(useForAutoDetect === "Yes" && autoDetectType === "1") {
                 args.IsValid = sysDescrRegexValue.trim().length > 0;
             }
             else {
                 args.IsValid = true;
             }
         };
     </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=SolarWinds.NCMModule.Web.Resources.DeviceTemplateManagement.DeviceTemplateWizardController.GetPageTitle(true) %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <ncm:WizardContainer ID="wizard" runat="server" OnNextClick="Next_Click">
        <Content>
            <div class="ncm-main-content">
                <div class="ncm-block-header">
                    <%=Resources.NCMWebContent.DeviceTemplateWizard_SaveDeviceTemplateLabel %>
                </div>
                <table class="ncm-blue-box">
                    <tr>
                        <td class="ncm-left-label-column">
                            <%=Resources.NCMWebContent.DeviceTemplateWizard_SaveDeviceTemplateLabel %>
                        </td>
                        <td class="ncm-right-input-column">
                            <asp:TextBox runat="server" ID="DeviceTemplateName" CssClass="ncm-text-field"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvDeviceTemplateName" ControlToValidate="DeviceTemplateName" Display="Dynamic" ErrorMessage="<%$ Resources: NCMWebContent, DeviceTemplateWizard_ValueIsRequiredMessage %>" runat="server"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="ncm-left-label-column">
                            <%=Resources.NCMWebContent.DeviceTemplateWizard_SystemOIDLabel %>
                        </td>
                        <td class="ncm-right-input-column">
                            <asp:TextBox runat="server" ID="SystemOID" CssClass="ncm-text-field"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvSystemOID" ControlToValidate="SystemOID" Display="Dynamic" ErrorMessage="<%$ Resources: NCMWebContent, DeviceTemplateWizard_ValueIsRequiredMessage %>" runat="server"/>
                            <asp:RegularExpressionValidator ID="revSystemOID" ControlToValidate="SystemOID" Display="Dynamic" ValidationExpression="\d([.]\d{1,9}){4,}" ErrorMessage="<%$ Resources: NCMWebContent, DeviceTemplateWizard_IncorrectSystemOIDFormatMessage %>"  runat="server"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="ncm-left-label-column">
                            <%=Resources.NCMWebContent.DeviceTemplateWizard_SystemDescriptionPatternLabel %>
                        </td>
                        <td class="ncm-right-input-column">
                            <asp:TextBox runat="server" ID="SystemDescriptionRegex" ClientIDMode="Static" CssClass="ncm-text-field"></asp:TextBox>
                            <asp:CustomValidator ID="rfvSystemDescriptionRegex" Display="Dynamic" ClientValidationFunction="validateSystemDescriptionRegex" ErrorMessage="<%$ Resources: NCMWebContent, DeviceTemplateWizard_ValueIsRequiredMessage %>" runat="server"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="ncm-left-label-column">
                            <%=Resources.NCMWebContent.WEBDATA_VK_1031%>
                        </td>
                        <td class="ncm-right-input-column">
                            <asp:DropDownList runat="server" ID="ddlUseForAutoDetect" onchange="UseForAutoDetectChange(this);">
                                <asp:ListItem Value="Yes" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_311 %>"></asp:ListItem>
                                <asp:ListItem Value="No" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_312 %>"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="autoDetectTypeHolder">
                        <td class="ncm-left-label-column">
                            &nbsp;
                        </td>
                        <td class="ncm-right-input-column">
                            <asp:RadioButtonList runat="server" ID="rbAutoDetectType" ClientIDMode="Static">
                                <asp:ListItem Text="<%$ Resources: NCMWebContent, DeviceTemplateWizard_MatchBySysOIDRadioButtonText %>" Value="0" Selected="true"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: NCMWebContent, DeviceTemplateWizard_MatchBySysDescriptionRadioButtonText %>" Value="1"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td class="ncm-left-label-column" style="vertical-align:top;">
                            <%=Resources.NCMWebContent.DeviceTemplateWizard_CommentsLabel %>
                        </td>
                        <td class="ncm-right-input-column">
                            <asp:TextBox runat="server" ID="Comments" CssClass="ncm-text-field ncm-resizable-comments-field" Height="50px" TextMode="MultiLine"></asp:TextBox>
                            <div>  
                                <span>&#0187;&nbsp;
                                    <a class="ncm-helplink" 
                                        href="<%= LearnMoreAssignDeviceTemplate %>" 
                                        target="_blank"><%=Resources.NCMWebContent.DeviceTemplateWizard_AssignDeviceTemplatesHelpMeLink %>
                                    </a>
                                </span>
                            </div>  
                        </td>
                    </tr>
                </table>

                <asp:UpdatePanel ID="updatePanel" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                        <div class="ncm-block-header">
                            <%=Resources.NCMWebContent.DeviceTemplateWizard_AssignDeviceTemplateLabel %>
                        </div>
                        <table class="ncm-blue-box">
                            <tr runat="server" ID="rowAssignedNode">
                                <td class="ncm-left-label-column"></td>
                                <td class="ncm-right-input-column">
                                    <div style="padding-bottom: 5px;">
                                        <%=Resources.NCMWebContent.DeviceTemplateWizard_AssignDeviceTemplateNote %> <a href="/Orion/NCM/NodeDetails.aspx?NodeID=<%=SolarWinds.NCMModule.Web.Resources.DeviceTemplateManagement.DeviceTemplateWizardController.NCMNode.NodeID %>" target="_blank" class="ncm-node-caption-link"><%=GetNodeName() %></a>
                                        <a id="removeNodeBtn" style="padding-left:10px;" class="enablebtn" OnServerClick="RemoveNodeButton_Click" CausesValidation="False" runat="server">
                                            <img alt="" style="vertical-align:sub;" src="/Orion/NCM/Resources/images/icon_delete.png">
                                            <span><%=Resources.NCMWebContent.WEBDATA_VK_567 %></span>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="ncm-left-label-column"></td>
                                <td class="ncm-right-input-column">
                                    <div style="padding-bottom: 5px;">
                                        <%=Resources.NCMWebContent.DeviceTemplateWizard_SelectAdditionalNodesLabel %>
                                    </div>
                                    <div style="width:50%;min-width:500px;max-height:500px;overflow:auto;background-color:white;">
                                        <table id="nodetreecontrol_device_template_datapicker" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td id="nodes_picker_container" runat="server">                                    
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="ncm-left-label-column"></td>
                                <td class="ncm-right-input-column">
                                    <asp:CheckBox ID="AssignConnectionProfile" runat="server" Text="<%$ Resources: NCMWebContent, DeviceTemplateWizard_AssignTheSameConnProfileCheckboxText %>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="ncm-left-label-column"></td>
                                <td class="ncm-right-input-column">
                                    <asp:CheckBox ID="DownloadConfigs" runat="server" Text="<%$ Resources: NCMWebContent, DeviceTemplateWizard_DownloadConfigsCheckboxText %>" AutoPostBack="true" OnCheckedChanged="DownloadConfigs_CheckedChanged" />
                                    <div style="padding:5px 0px 0px 20px;">
                                        <ncm:ConfigTypes ID="ConfigTypes" runat="server" Width="150px"></ncm:ConfigTypes>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </Content>
    </ncm:WizardContainer>
</asp:Content>

