﻿<%@ WebHandler Language="C#" Class="DeviceTemplateLinkHandler" %>

using System;
using System.Text;
using System.Web;
using System.Web.SessionState;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.NCM.Common.Transfer;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.DeviceTemplateManagement;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Cli.Contracts;

public class DeviceTemplateLinkHandler : IHttpHandler, IRequiresSessionState
{
    public bool IsReusable
    {
        get { return false; }
    }

    public void ProcessRequest(HttpContext context)
    {
        bool isError = false;
        string errorMessage = string.Empty;

        try
        {
            DeviceTemplateWizardController.ClearSessionData();

            var nodeId = Guid.Parse(context.Request[@"NodeID"]);
            NCMNode ncmNode = GetNcmNode(nodeId);

            var mng = ServiceLocator.Container.Resolve<IDeviceTemplatesManager>();
            var deviceTemplate = mng.GetTemplateForNode(ncmNode.CoreNodeID, true);

            var deviceTemplateSerializer = ServiceLocator.Container.Resolve<IDeviceTemplateSerializer>();

            DeviceTemplateWizardController.DeviceCommands = deviceTemplateSerializer.DeserializeXml(deviceTemplate.TemplateXml);
            DeviceTemplateWizardController.DeviceTemplateName = deviceTemplate.Name;
            DeviceTemplateWizardController.IsDefaultTemplate = deviceTemplate.IsDefault;
            DeviceTemplateWizardController.NCMNode = ncmNode;

            var url = new StringBuilder();
            url.AppendFormat(@"/Orion/NCM/Admin/DeviceTemplate/ChooseOperation.aspx?StepIndex=0&Id={0}&IsSuggestedAction=true", deviceTemplate.Id);
            if (!string.IsNullOrEmpty(context.Request[@"printable"]))
                url.Append(@"&printable=true");
            if (!string.IsNullOrEmpty(context.Request[@"ReturnUrl"]))
                url.AppendFormat(@"&ReturnUrl={0}", context.Request[@"ReturnUrl"]);

            context.Response.Redirect(url.ToString());
        }
        catch (Exception ex)
        {
            isError = true;
            errorMessage = ex.Message;
        }

        if (isError)
        {
            var url = new StringBuilder();
            url.Append(@"/Orion/NCM/Admin/DeviceTemplate/SelectNode.aspx?StepIndex=0&SetSessionData=true");
            if (!string.IsNullOrEmpty(context.Request[@"printable"]))
                url.Append(@"&printable=true");
            if (!string.IsNullOrEmpty(context.Request[@"ReturnUrl"]))
                url.AppendFormat(@"&ReturnUrl={0}", context.Request[@"ReturnUrl"]);

            OrionErrorPageBase.TransferToErrorPage(
                context,
                new ErrorInspectorDetails
                {
                    Error = new Exception(errorMessage),
                    HtmlHelpfulHints = $@"<a href='{url}'>{Resources.NCMWebContent.DeviceTemplate_Error_CreateNew}</a>",
                    Title = Resources.NCMWebContent.DeviceTemplate_Error_PageTitle
                });
        }
    }

    private NCMNode GetNcmNode(Guid ncmNodeId)
    {
        var isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            return proxy.Cirrus.Nodes.GetNode(ncmNodeId);
        }
    }
}