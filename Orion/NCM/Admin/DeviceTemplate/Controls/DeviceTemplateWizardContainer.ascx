﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DeviceTemplateWizardContainer.ascx.cs" Inherits="Orion_NCM_Admin_DeviceTemplate_Controls_DeviceTemplateWizardContainer" %>
<orion:Include runat="server" File="styles/ProgressIndicator.css" />
<script type="text/javascript">
    SW = window.SW || {};
    SW.NCM = SW.NCM || {};

    SW.NCM.DeviceTemplateWizardContainer = {
        fnEnabledNextButton: function (enabled) {
            var btnNext = $("#<%=btnNext.ClientID%>");
            if (enabled) {
                btnNext.removeClass('sw-btn-disabled');
            }
            else {
                btnNext.addClass('sw-btn-disabled');
            }
        }
    }

    $(function () {
        $("#<%=btnNext.ClientID%>").click(function (e) {
            if ($(this).is('.sw-btn-disabled')) {
                e.preventDefault();
            }
        });
    });
</script>

<div id="DeviceTemplateContainer" style="width:80%;padding:10px;">
    <div>
        <asp:Literal ID="wizardHeader" runat="server"></asp:Literal>
    </div>

    <div style="padding-top:20px;font-size:11pt;font-weight:bold;border-bottom:#e6e6dd 1px solid;padding-bottom:5px;">
        <asp:Literal ID="stepTitle" runat="server"></asp:Literal>
    </div>

    <div id="stepSubTitleHolder" runat="server" style="padding-top:10px;">
        <asp:Literal ID="stepSubTitle" runat="server"></asp:Literal>
    </div>

    <div style="padding:10px 0px 10px 0px;border-bottom:#e6e6dd 1px solid;">
        <asp:PlaceHolder runat="server" ID="Container" />
    </div>

    <div style="padding-top:10px;">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td style="text-align:right;">
                    <span style="padding-right:5px;">
                        <orion:LocalizableButton runat="server" ID="btnBack" LocalizedText="Back" DisplayType="Secondary" CausesValidation="false" OnClick="btnBack_Click" />
                    </span>
                    <span style="padding-right:5px;">
                        <orion:LocalizableButton runat="server" ID="btnNext" LocalizedText="Next" DisplayType="Primary" CausesValidation="true" OnClick="btnNext_Click" />
                    </span>
                    <span style="padding-right:5px;padding-left:20px;">
                        <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" OnClick="btnCancel_Click" />
                    </span>
                </td>
            </tr>
        </table>
    </div>
</div>