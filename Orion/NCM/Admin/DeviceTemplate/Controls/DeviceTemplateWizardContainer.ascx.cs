﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.DeviceTemplateManagement;
using SolarWinds.Orion.Common;

public partial class Orion_NCM_Admin_DeviceTemplate_Controls_DeviceTemplateWizardContainer : System.Web.UI.UserControl
{
    private const string key = "D453A4B4-4FA8-4056-8126-724132C671E2";
    private string script = @"SW.NCM.DeviceTemplateWizardContainer.fnEnabledNextButton({0});";

    public delegate void dlgWizardClick(object sender, DeviceTemplateEventArgs e);

    public event dlgWizardClick BackClick;
    public event dlgWizardClick NextClick;
    public event dlgWizardClick CancelClick;

    [PersistenceMode(PersistenceMode.InnerProperty)]
    public PlaceHolder Content
    {
        get { return Container; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool EnabledNextButton
    {
        set
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), key, string.Format(script, value.ToString().ToLowerInvariant()), true);
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (!IsPostBack)
        {
            if (DeviceTemplateWizardController.NeedSetSessionData)
                DeviceTemplateWizardController.SetSessionData();
        }

        ValidateSessionData();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetupWizard();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (BackClick != null)
        {
            var args = new DeviceTemplateEventArgs();
            BackClick.Invoke(sender, args);
            if (!args.IsValid) return;
        }

        Page.Response.Redirect(DeviceTemplateWizardController.GetBackStepUrl());
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        var steps = DeviceTemplateWizardController.GetWizardSteps();

        if (NextClick != null)
        {
            var args = new DeviceTemplateEventArgs();
            NextClick.Invoke(sender, args);
            if (!args.IsValid) return;
        }

        if (DeviceTemplateWizardController.CurrentStepIndex + 1 == steps.Count)
            Page.Response.Redirect(DeviceTemplateWizardController.GetFinishStepUrl());
        else
            Page.Response.Redirect(DeviceTemplateWizardController.GetNextStepUrl());
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (CancelClick != null)
        {
            var args = new DeviceTemplateEventArgs();
            CancelClick.Invoke(sender, args);
            if (!args.IsValid) return;
        }
        Page.Response.Redirect(DeviceTemplateWizardController.GetCancelStepUrl());
    }

    private void SetupWizard()
    {
        var steps = DeviceTemplateWizardController.GetWizardSteps();

        if (DeviceTemplateWizardController.CurrentStepIndex == 0)
        {
            btnBack.Visible = false;
            btnNext.LocalizedText = SolarWinds.Orion.Web.UI.CommonButtonType.Next;
        }
        else if (DeviceTemplateWizardController.CurrentStepIndex + 1 == steps.Count)
        {
            btnBack.Visible = true;
            btnNext.LocalizedText = SolarWinds.Orion.Web.UI.CommonButtonType.Finish;

            if (OrionConfiguration.IsDemoServer)
                btnNext.OnClientClick =
                    $@"demoAction('{DemoModeConstants.NCM_SETTINGS_ADD_OR_MANAGE_DEVICE_TEMPLATES}', this); return false;";
        }
        else
        {
            btnBack.Visible = true;
            btnNext.LocalizedText = SolarWinds.Orion.Web.UI.CommonButtonType.Next;
        }

        StringBuilder header = new StringBuilder();
        header.Append(@"<div class='ProgressIndicator'>");
        for (int i = 0; i < steps.Count; i++)
        {
            bool isActive;
            if (DeviceTemplateWizardController.CurrentStepIndex == i)
            {
                header.AppendFormat(@"<div class='PI_on'>&nbsp;{0}</div>", steps[i].Title);
                header.AppendFormat(@"<img src='/Orion/Images/ProgressIndicator/pi_sep_on_off_sm.gif' />");
            }
            else
            {
                header.AppendFormat(@"<div class='PI_off'>&nbsp;{0}</div>", steps[i].Title);
                isActive = ((i < steps.Count - 1) && steps[DeviceTemplateWizardController.CurrentStepIndex].Equals(steps[i + 1]) ? true : false);
                if (isActive)
                    header.AppendFormat(@"<img src='/Orion/Images/ProgressIndicator/pi_sep_off_on_sm.gif' />");
                else
                    header.AppendFormat(@"<img src='/Orion/Images/ProgressIndicator/pi_sep_off_off_sm.gif' />");
            }
        }
        header.Append(@"</div>");
        wizardHeader.Text = header.ToString();

        stepTitle.Text = steps[DeviceTemplateWizardController.CurrentStepIndex].Title;
        string subTitle = steps[DeviceTemplateWizardController.CurrentStepIndex].SubTitle;
        if (!string.IsNullOrEmpty(subTitle))
            stepSubTitle.Text = subTitle;
        else
            stepSubTitleHolder.Style.Add(@"display", @"none");
    }

    private void ValidateSessionData()
    {
        if (DeviceTemplateWizardController.DeviceCommands == null || DeviceTemplateWizardController.NCMNode == null)
        {
            DeviceTemplateWizardController.ClearSessionData();
            Page.Response.Redirect(DeviceTemplateWizardController.GetReturnUrl());
        }
    }
}