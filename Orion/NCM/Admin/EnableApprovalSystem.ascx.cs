using System;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;

public partial class Orion_NCM_Admin_EnableApprovalSystem : System.Web.UI.UserControl
{
    protected string EnableApprovalSystem => WebSettingsDAL.Get(ConfigChangeApprovalHelper.NCM_ENABLE_APPROVAL_SYSTEM, @"false");

    protected string BypassApproval
    {
        get
        {
            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                return proxy.Cirrus.Settings.GetSetting(Settings.BypassApprovalForAutoRemediationScript, @"false", null).ToLowerInvariant ();
            }
        }
    }

    protected string EnableApprovalSystemLink
    {
        get
        {
            if (Convert.ToBoolean(WebSettingsDAL.Get(ConfigChangeApprovalHelper.NCM_ENABLE_APPROVAL_SYSTEM, @"false")))
            {
                return Resources.NCMWebContent.WEBDATA_VK_21;
            }

            return Resources.NCMWebContent.WEBDATA_VK_22;
        }
    }
	
	protected string DemoActionKey => DemoModeConstants.NCM_SETTINGS_ENABLE_APPROVAL_SYSTEM;

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}
