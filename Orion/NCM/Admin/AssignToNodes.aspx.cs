﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_AssignToNodes : System.Web.UI.Page
{
    private string ConnectionProfileId => HttpUtility.HtmlEncode(Request.QueryString[@"profileID"]);

    private string ConnectionProfileName => HttpUtility.HtmlEncode(Request.QueryString[@"profileName"]);

    private static readonly string SettingKey = @"AssignToNodesCP";

    private string ReturnUrl
    {
        get
        {
            return Convert.ToString(ViewState["ReturnUrl"]);
        } 
        set
        {
            if (ViewState["ReturnUrl"] == null)
                ViewState["ReturnUrl"] = value;
        }
    }

    private readonly AdvToobarNodes _toolBarNodes = new AdvToobarNodes();
    private ISWrapper isLayer = new ISWrapper();

    protected override void OnInit(EventArgs e)
    {
        var resourceSettings = new ResourceSettings();
        resourceSettings.Prefix = SettingKey;
        NPMDefaultSettingsHelper.SetUserSettings($"{SettingKey}GroupBy", "Vendor");

        _toolBarNodes.ResourceSettings = resourceSettings;
        _toolBarNodes.Visible = true;
        _toolBarNodes.ShowClearAll = true;
        _toolBarNodes.ShowSelectAll = true;
        _toolBarNodes.NodesControl.ShowConfigs = false;
        _toolBarNodes.NodesControl.ShowGroupCheckBoxes = true;
        _toolBarNodes.NodesControl.ShowNodeCheckBoxes = true;
        _toolBarNodes.NodesControl.ExcludeUnmanagedNodes = false;
        _toolBarNodes.NodesControl.TreeControl.ClientSideEvents.NodeChecked = @"UltraWebTree_TheeLevelNodeCheckedDownloadConfig";
        nodes_picker_container.Controls.Add(_toolBarNodes);

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Page.Request.UrlReferrer != null)
                ReturnUrl = Page.Request.UrlReferrer.ToString();
            else
                ReturnUrl = @"~/Orion/NCM/Admin/Default.aspx";
        }

        if (string.IsNullOrEmpty(ConnectionProfileId) || string.IsNullOrEmpty(ConnectionProfileName))
            throw new HttpRequestValidationException(Resources.NCMWebContent.AssignToNodes_IncorrectURLParameter);

        Page.Title = string.Format(Resources.NCMWebContent.AssignToNodes_PageTitle, ConnectionProfileName);
    }

    protected void NodeSelectionValidate(object source, ServerValidateEventArgs args)
    {
        var nodeIds = _toolBarNodes.NodesControl.GetCheckedNodeIds();
        args.IsValid = nodeIds.Any();
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        var nodeIds = _toolBarNodes.NodesControl.GetCheckedNodeIds();
        if (nodeIds.Any())
        {
            customValidator.IsValid = true;
            UpdateNodeDetails(nodeIds);
            Page.Response.Redirect(ReturnUrl);
        }
        else
            customValidator.IsValid = false;
    }

    private void UpdateNodeDetails(Guid[] nodeIds)
    {
        using (var proxy = isLayer.Proxy)
        {
            foreach (var node in nodeIds)
            {
                var ncmNode = proxy.Cirrus.Nodes.GetNode(node);
                if (ncmNode != null)
                {
                    if (string.IsNullOrEmpty(ConnectionProfileId))
                        throw new HttpRequestValidationException(Resources.NCMWebContent.AssignToNodes_IncorrectURLParameter);

                    ncmNode.ConnectionProfile = Convert.ToInt32(ConnectionProfileId);
                    proxy.Cirrus.Nodes.UpdateNode(ncmNode);
                }
            }
        }
    }

    protected void CancelClick(object sender, EventArgs e)
    {
        Page.Response.Redirect(ReturnUrl);
    }
}
