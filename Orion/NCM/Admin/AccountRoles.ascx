<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AccountRoles.ascx.cs" Inherits="Orion_NCM_Admin_AccountRoles" %>

<style type="text/css">
    #NCMAccountRolesTABLE
    {
        height: <%=CSSHeight%>;
    }
    
    #LearnMoreLink a
    {
        color: #336699;
    }
    
	#LearnMoreLink a:hover 
	{ 
	    color: #ffa500;
	}
</style>

<table id="NCMAccountRolesTABLE">
    <tr>
        <td>
            <div style="position:absolute;">
                <asp:Table ID="table" CellPadding="0" CellSpacing="0" runat="server"></asp:Table>
            </div>
        </td>
    </tr>
</table>