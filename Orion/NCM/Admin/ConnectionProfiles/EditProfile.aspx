﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="EditProfile.aspx.cs" Inherits="Orion_NCM_Admin_ConnectionProfiles_EditProfile" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="../Settings/Settings.css" />
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <style type="text/css">
        .ncm_infoIcon {
            background: transparent url(/Orion/NCM/Resources/images/Icon.Info.gif) scroll no-repeat center;
            padding: 2px 0px 2px 20px;
        }
    </style>

    <script type="text/javascript">
        function initTooltip(targetEl, msg) {
            var toolTip = new Ext.ToolTip({
                anchor: 'top',
                html: String.format('<table><tr><td style="padding:5px;font-size:8pt;">{0}</td></tr></table>', msg),
                trackMouse: true
            });
            toolTip.initTarget(targetEl);
        }

        function pageLoad() {
            initTooltip('LabelInfo', '<%=Resources.NCMWebContent.EditConnectionProfile_Tooltip_EnableLevel %>');
            initTooltip('CheckboxInfo', '<%=Resources.NCMWebContent.EditConnectionProfile_Tooltip_ApplyProfile %>');
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMPHCreateNewConnectionProfile" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
     <asp:Panel ID="ContentContainer" CssClass="ContentContainer" runat="server">
        <div class="SettingContainer">
            <div>
                <div style="font-weight:bold;padding-top:10px;"><%=Resources.NCMWebContent.WEBDATA_VK_854 %></div>
                <div style="padding-top:5px" automation="profileNameInput">
                    <asp:TextBox ID="txtProfileName" MaxLength="256" runat="server" TextMode="SingleLine" CssClass="SettingTextField" Width="200px" />
                    <div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                            ControlToValidate="txtProfileName"
                            Display="Dynamic"
                            ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_855 %>"
                            runat="server"/>
                    </div>
                </div>

                <div style="font-weight:bold;padding-top:10px;"><%=Resources.NCMWebContent.EditConnectionProfile_Lable_CliLogin %></div>
                <div style="padding-top:5px" automation="cliLoginInput"><ncm:PasswordTextBox ID="txtUserName" runat="server" TextMode="SingleLine" CssClass="SettingTextField" Width="200px" /></div>
                
                <div style="font-weight:bold;padding-top:20px;"><%=Resources.NCMWebContent.EditConnectionProfile_Lable_CliPassword %></div>
                <div style="padding-top:5px" automation="cliPasswordInput"><ncm:PasswordTextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="SettingTextField" Width="200px" /></div>

                <div style="font-weight: bold; padding-top: 20px;"><%=Resources.NCMWebContent.EditConnectionProfile_Lable_EnableLevel %><span id='LabelInfo' class="ncm_infoIcon"></span></div>
                <div style="padding-top:5px" automation="enableLevelSelect">
                    <asp:DropDownList ID="ddlEnableLevel" runat="server" CssClass="SettingSelectField" Width="200px">
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_314 %>" Value="<No Enable Login>"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_315 %>" Value="enable"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                
                <div style="font-weight:bold;padding-top:20px;"><%=Resources.NCMWebContent.WEBDATA_VM_137 %></div>
                <div style="padding-top:5px" automation="enablePasswordInput"><ncm:PasswordTextBox ID="txtEnablePassword" runat="server" TextMode="Password" CssClass="SettingTextField" Width="200px" /></div>

                <div style="font-weight:bold;padding-top:20px;"><%=Resources.NCMWebContent.WEBDATA_VM_139 %></div>
                <div style="padding-top:5px" automation="executeCommandsSelect">
                    <asp:DropDownList ID="ddlExecProtocol" runat="server" CssClass="SettingSelectField" Width="200px">
                    </asp:DropDownList>
                </div>
                
                <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>                        
                        <div style="font-weight:bold;padding-top:20px;"><%=Resources.NCMWebContent.WEBDATA_VM_140 %></div>
                        <div style="padding-top:5px" automation="configRequestSelect">
                            <asp:DropDownList ID="ddlRequestProtocol" runat="server" Width="200px" CssClass="SettingSelectField" AutoPostBack="true" OnSelectedIndexChanged="ddlRequestProtocol_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div> 
                                        
                        <div style="font-weight:bold;padding-top:20px;"><%=Resources.NCMWebContent.WEBDATA_VM_141 %></div>
                        <div style="padding-top:5px" automation="transferConfigsSelect">
                            <asp:DropDownList ID="ddlTransferProtocol" runat="server" CssClass="SettingSelectField" Width="200px">
                            </asp:DropDownList>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <div style="font-weight:bold;padding-top:20px;"><%=Resources.NCMWebContent.WEBDATA_VM_143 %></div>
                <div style="padding-top:5px" automation="telnetPortInput"><asp:TextBox runat="server" ID="txtTelnetPort" CssClass="SettingTextField" Width="200px" ></asp:TextBox></div>
                <div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                        ControlToValidate="txtTelnetPort"
                        Display="Dynamic"
                        ErrorMessage="<%$ Resources: NCMWebContent,WEBDATA_VM_144 %>"
                        runat="server"/> 
                    <asp:CompareValidator runat="server" ID="CompareValidator1" 
                        ControlToValidate="txtTelnetPort" 
                        ValueToCompare="0"
                        ErrorMessage="<%$ Resources: NCMWebContent,WEBDATA_VM_145 %>"
                        Operator="GreaterThan" type="Integer" Display="Dynamic" />
                </div>

                <div style="font-weight:bold;padding-top:20px;"><%=Resources.NCMWebContent.WEBDATA_VM_146 %></div>
                <div style="padding-top:5px" automation="sshPortInput"><asp:TextBox runat="server" ID="txtSSHPort" CssClass="SettingTextField" Width="200px"></asp:TextBox></div>
                <div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                        ControlToValidate="txtSSHPort"
                        Display="Dynamic"
                        ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VM_147 %>"
                        runat="server" /> 
                    <asp:CompareValidator runat="server" ID="CompareValidator2" 
                        ControlToValidate="txtSSHPort" 
                        ValueToCompare="0"
                        ErrorMessage="<%$ Resources: NCMWebContent,WEBDATA_VM_148 %>"
                        Operator="GreaterThan" Type="Integer" Display="Dynamic" />
                </div>
               
                <div style="padding-top:20px;padding-bottom:10px;" automation="autoTestProfileCheckbox"><asp:CheckBox ID="cbAutoDetectConnectionProfile" runat="server" Text="<%$ Resources: NCMWebContent, EditConnectionProfile_Checkbox_AutoTest %>" /> <span id='CheckboxInfo' class='ncm_infoIcon'></span></div>
            </div>
        </div>
        
        <div style="padding-top:20px;">
            <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" 
            OnClick="btnSubmit_Click" DisplayType="Primary" CausesValidation="true"/> 
            <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" 
            OnClick="btnCancel_Click" DisplayType="Secondary" CausesValidation="false"/>
        </div>
     </asp:Panel>
</asp:Content>

