﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="ProfileManagement.aspx.cs" Inherits="Orion_NCM_Admin_ConnectionProfiles_ProfileManagement" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/PolicyReports/PolicyReport.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
    
    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippetsHelper.js"></script>
    <script type="text/javascript" src="ProfileManagement.js" ></script>
    <script type="text/javascript">
        SW.NCM.AllowNodeManagement = <%=Profile.AllowNodeManagement.ToString().ToLowerInvariant()%>;
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMPHManageConnectionProfile" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <!-- demoedit -->
    <%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
		<div id="isDemoMode" style="display:none;"></div>
	<%}%>
    <!-- / demoedit -->

    <asp:Panel style="padding:10px 10px 10px 10px;" id="ContentContainer" runat="server">
        <div id="tabPanel">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr valign="top" align="left">                    
                    <td id="gridCell">
                        <div id="Grid" />
                    </td>                    
                </tr>
            </table>
            <div id="originalQuery">
            </div>
            <div id="test">
            </div>
            <pre id="stackTrace"></pre>
        </div>
    </asp:Panel>
    <div style="padding:10px;" id="ErrorContainer" runat="server"/>
</asp:Content>

