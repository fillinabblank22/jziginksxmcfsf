﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.NCM.Common.BusinessLayer.Interfaces;
using SolarWinds.Orion.Web;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.Orion.Common;

public partial class Orion_NCM_Admin_ConnectionProfiles_EditProfile : Page
{
    private readonly IIsWrapper isLayer;
    private readonly INcmBusinessLayerProxyHelper blProxy;
    private string[] ExecuteScriptProtocols;
    private string[] RequestConfigProtocols;

    protected bool HideUsernames
    {
        get
        {
            if (ViewState["HideUsernames"] == null)
            {
                using (var proxy = isLayer.GetProxy())
                {
                    var hideUsernames = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.HideUsernames, false, null));
                    ViewState["HideUsernames"] = hideUsernames;
                    return hideUsernames;
                }
            }

            return Convert.ToBoolean(ViewState["HideUsernames"]);
        }
    }
    public Orion_NCM_Admin_ConnectionProfiles_EditProfile()
    {
        blProxy = ServiceLocator.Container.Resolve<INcmBusinessLayerProxyHelper>();
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        var denyAccess = !OrionConfiguration.IsDemoServer && !Profile.AllowNodeManagement && !(Page is IBypassAccessLimitation);
        if (denyAccess)
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_VK_363}");
        }

        var results = blProxy.CallMain(x => x.GetCommandProtocolsWithoutGlobalProtocols());
        ExecuteScriptProtocols = results.ExecuteScriptProtocols;
        RequestConfigProtocols = results.RequestConfigProtocols;

        CommonHelper.PopulateDropDownList(ExecuteScriptProtocols, ddlExecProtocol);
        CommonHelper.PopulateDropDownList(RequestConfigProtocols, ddlRequestProtocol);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (HideUsernames)
        {
            txtUserName.TextMode = TextBoxMode.Password;
        }

        if (OrionConfiguration.IsDemoServer)
        {
            btnSubmit.OnClientClick =
                $@"demoAction('{DemoModeConstants.NCM_CONNECTION_PROFILES_ADD_EDIT_PROFILE}', this); return false;";
        }
        else
        {
            btnSubmit.Enabled = Profile.AllowNodeManagement;
        }

        if (!Page.IsPostBack)
        {
            LoadProfile();
        }
    }

    protected void ddlRequestProtocol_SelectedIndexChanged(object sender, EventArgs e)
    {
        CommonHelper.InitializeTransferConfigProtocol(ddlTransferProtocol, ddlRequestProtocol.SelectedItem, false);
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        SubmitProfile();
        Page.Response.Redirect("/Orion/NCM/Admin/ConnectionProfiles/ProfileManagement.aspx");
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect("/Orion/NCM/Admin/ConnectionProfiles/ProfileManagement.aspx");
    }

    private void LoadProfile()
    {
        var profileId = Page.Request.QueryNullableInt("ProfileID");
        var profile = LoadProfile(profileId);

        Page.Title = profileId == null ? Resources.NCMWebContent.WEBDATA_VK_856 : string.Format(Resources.NCMWebContent.WEBDATA_VM_69, HttpUtility.HtmlEncode(profile.Name));

        txtProfileName.Text = ConvertValueToStringSafe(profile.Name);
        txtUserName.PasswordText = ConvertValueToStringSafe(profile.UserName);
        txtPassword.PasswordText = ConvertValueToStringSafe(profile.Password);
        ddlEnableLevel.SelectedValue = profile.EnableLevel;
        txtEnablePassword.PasswordText = ConvertValueToStringSafe(profile.EnablePassword);
        txtTelnetPort.Text = ConvertValueToStringSafe(profile.TelnetPort);
        txtSSHPort.Text = ConvertValueToStringSafe(profile.SSHPort);
        cbAutoDetectConnectionProfile.Checked = profile.UseForAutoDetect;

        ddlExecProtocol.SelectedValue = string.IsNullOrEmpty(profile.ExecuteScriptProtocol) ? ExecuteScriptProtocols[0] : profile.ExecuteScriptProtocol;
        ddlRequestProtocol.SelectedValue = string.IsNullOrEmpty(profile.RequestConfigProtocol) ? RequestConfigProtocols[0] : profile.RequestConfigProtocol;
        ddlRequestProtocol_SelectedIndexChanged(ddlTransferProtocol, EventArgs.Empty);
        if (!string.IsNullOrEmpty(profile.TransferConfigProtocol))
        {
            ddlTransferProtocol.SelectedValue = profile.TransferConfigProtocol;
        }
    }

    private ConnectionProfile LoadProfile(int? profileId)
    {
        if (profileId == null)
        {
            return new ConnectionProfile();
        }

        using (var proxy = isLayer.GetProxy())
        {
            return proxy.Cirrus.Nodes.GetConnectionProfile(profileId.Value);
        }
    }

    private void SubmitProfile()
    {
        var profileId = Page.Request.QueryNullableInt("ProfileID");
        var profile = LoadProfile(profileId);

        profile.Name = txtProfileName.Text;
        profile.UserName = txtUserName.PasswordText;
        profile.Password = txtPassword.PasswordText;
        profile.EnableLevel = ddlEnableLevel.SelectedValue;
        profile.EnablePassword = txtEnablePassword.PasswordText;
        profile.ExecuteScriptProtocol = ddlExecProtocol.SelectedValue;
        profile.RequestConfigProtocol = ddlRequestProtocol.SelectedValue;
        profile.TransferConfigProtocol = ddlTransferProtocol.SelectedValue;
        profile.TelnetPort = Convert.ToInt32(txtTelnetPort.Text);
        profile.SSHPort = Convert.ToInt32(txtSSHPort.Text);
        profile.UseForAutoDetect = cbAutoDetectConnectionProfile.Checked;
        
        if (profileId == null)
        {
            using (var proxy = isLayer.GetProxy())
            {
                proxy.Cirrus.Nodes.AddConnectionProfile(profile);
            }
        }
        else
        {
            using (var proxy = isLayer.GetProxy())
            {
                proxy.Cirrus.Nodes.UpdateConnectionProfile(profile);
            }
        }
    }

    private string ConvertValueToStringSafe(object value)
    {
        return value == null ? string.Empty : value.ToString();
    }
}