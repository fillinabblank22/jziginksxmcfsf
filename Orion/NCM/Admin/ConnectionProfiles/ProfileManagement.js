﻿Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.QuickTips.init();

SW.NCM.ProfileManagement = function () {

    var refreshObjects = function (callback) {
        grid.store.removeAll();
        grid.store.proxy.conn.jsonData = {};
        grid.store.load({ callback: callback });
    };

    var createNewProfile = function (item) {
        window.location = "/Orion/NCM/Admin/ConnectionProfiles/EditProfile.aspx";
    };

    var editProfile = function (item) {
        window.location = "/Orion/NCM/Admin/ConnectionProfiles/EditProfile.aspx?ProfileID=" + item.data.ProfileID;
    };

    var deleteProfile = function (item) {
        var profileID = item.data.ProfileID;
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_197;E=js}');

        internalDeleteProfile(profileID, function (result) {
            waitMsg.hide();
            ErrorHandler(result);
            ReloadGridStore();
        });
    };

    var AssignProfileToNodes = function (item) {
        window.location = String.format("/Orion/NCM/Admin/AssignToNodes.aspx?profileID={0}&profileName={1}", item.data.ProfileID, encodeURIComponent(item.data.ProfileName));
    };

    var internalDeleteProfile = function (profileID, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/ProfileManagement.asmx",
        "DeleteProfile", { profileID: profileID },
        function (result) {
            onSuccess(result);
        });
    };

    function profileNameClick(obj) {
        var profileID = obj.attr("value");
        window.location = "/Orion/NCM/Admin/ConnectionProfiles/EditProfile.aspx?ProfileID=" + profileID;
    };

    function renderProfileName(value, meta, record) {
        return String.format('<a href="#" style="font-weight:bold;" class="profileName" value="{0}">{1}</a>', record.data.ProfileID, Ext.util.Format.htmlEncode(value));
    }

    function renderUseForAutoDetect(value, meta, record) {
        if (value) {
            return '@{R=NCM.Strings;K=WEBJS_VK_57;E=js}';
        }
        else {
            return '@{R=NCM.Strings;K=WEBJS_VK_58;E=js}';
        }
    }

    function ErrorHandler(result) {
        if (result != null && result.Error) {
            Ext.Msg.show({
                title: result.Source,
                msg: result.Msg,
                minWidth: 500,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        }
    }

    function ReloadGridStore() {
        var currentPageRecordCount = grid.store.getCount();
        var selectedRecordCount = grid.getSelectionModel().getCount();
        if (currentPageRecordCount > selectedRecordCount) {
            grid.store.reload();
        }
        else {
            grid.store.load();
        }
    }

    var updateToolbarButtons = function () {
        var selCount = grid.getSelectionModel().getCount();
        var map = grid.getTopToolbar().items.map;

        var needsOnlyOneSelected = (selCount != 1);

        map.EditProfile.setDisabled(needsOnlyOneSelected);
        map.DeleteProfile.setDisabled(needsOnlyOneSelected);
        map.AssignProfileBtn.setDisabled(needsOnlyOneSelected);

        var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');
        if (grid.getStore().getCount() > 0 && selCount == grid.getStore().getCount()) {
            hd.addClass('x-grid3-hd-checker-on');
        } else {
            hd.removeClass('x-grid3-hd-checker-on');
        }
    };

    var selectorModel;
    var dataStore;
    var grid;

    return {
        init: function () {

            if ($("[id$='_ContentContainer']").length == 0)
                return false;

            $("#Grid").click(function (e) {

                var obj = $(e.target);

                if (obj.hasClass('profileName')) {
                    profileNameClick(obj);
                    return false;
                }
            });

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", updateToolbarButtons);

            dataStore = new ORION.WebServiceStore(
                                "/Orion/NCM/Services/ProfileManagement.asmx/GetProfiles",
                                [
                                    { name: 'ProfileID', mapping: 0 },
                                    { name: 'ProfileName', mapping: 1 },
                                    { name: 'UseForAutoDetect', mapping: 2 }
                                ],
                                "ProfileName");

            grid = new Ext.grid.GridPanel({
                id: 'gridControl',
                store: dataStore,

                columns: [selectorModel, {
                    header: 'ProfileID',
                    width: 80,
                    hidden: true,
                    hideable: false,
                    sortable: true,
                    dataIndex: 'ProfileID'
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_196;E=js}',
                    width: 400,
                    sortable: true,
                    dataIndex: 'ProfileName',
                    renderer: renderProfileName
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_198;E=js}',
                    width: 200,
                    sortable: true,
                    dataIndex: 'UseForAutoDetect',
                    renderer: renderUseForAutoDetect
                }
                ],

                sm: selectorModel,

                layout: 'fit',
                autoScroll: 'true',
                loadMask: true,
                width: 1000,
                height: 500,
                stripeRows: true,

                tbar: [{
                    id: 'CreateNewProfile',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_190;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_191;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_addsnippet.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { createNewProfile(); }
                }, '-', {
                    id: 'EditProfile',
                    text: '@{R=NCM.Strings;K=WEBJS_VY_03;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_192;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_editsnippet.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { editProfile(grid.getSelectionModel().getSelected()); }
                }, '-', {
                    id: 'DeleteProfile',
                    text: '@{R=NCM.Strings;K=WEBJS_VY_05;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_193;E=js}',
                    icon: '/Orion/NCM/Resources/images/icon_delete.png',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        var showDemo = SW.NCM.IsDemoMode();

                        if (showDemo) {
                            demoAction('NCM_ConnectionProfile_DeleteProfile', $('#DeleteProfile'));
                        } else {
                            Ext.Msg.confirm('@{R=NCM.Strings;K=WEBJS_VK_194;E=js}',
                                '@{R=NCM.Strings;K=WEBJS_VK_195;E=js}',
                                function (btn, text) {
                                    if (btn == 'yes') {
                                        deleteProfile(grid.getSelectionModel().getSelected());
                                    }
                                });
                        }
                    }
                }, '-', {
                    id: 'AssignProfileBtn',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_329;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_328;E=js}',
                    icon: '/Orion/NCM/Resources/images/assign_node.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        AssignProfileToNodes(grid.getSelectionModel().getSelected());
                    }
                }
                ]
            });

            grid.render('Grid');

            updateToolbarButtons();

            $("form").submit(function () { return false; });

            refreshObjects();
        }
    };

}();
Ext.onReady(SW.NCM.ProfileManagement.init, SW.NCM.ProfileManagement);
