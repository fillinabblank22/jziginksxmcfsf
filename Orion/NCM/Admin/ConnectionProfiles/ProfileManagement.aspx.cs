﻿using System;
using SolarWinds.Orion.Web;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_Admin_ConnectionProfiles_ProfileManagement : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        bool denyAccess = !CommonHelper.IsDemoMode() && (!Profile.AllowNodeManagement && !(this.Page is IBypassAccessLimitation));

        if (denyAccess)
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_VK_363}");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_VK_857;

        SWISValidator validator = new SWISValidator();
        validator.ContentContainer = ContentContainer;
        validator.RedirectIfValidationFailed = true;
        ErrorContainer.Controls.Add(validator);
    }

    protected string AllowNodeManagement
    {
        get { return Profile.AllowNodeManagement.ToString().ToLowerInvariant(); }
    }
}