<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SMTPServer.ascx.cs" Inherits="Orion_Admin_SMTPServer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<style type="text/css">
    #AuthContent td
    {
        padding-top:20px;
    }
</style>

<div>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div style="padding-bottom:5px;"><b><%= Resources.NCMWebContent.WEBDATA_IC_56 %></b></div>
                <div>
                    <asp:TextBox ID="txtServerAddress" runat="server" CssClass="SettingTextField" Width="200px" Height="18px" MaxLength="4000"/>
                    <asp:RequiredFieldValidator ID="rfvServerAddress" runat="server" Display="Dynamic" ControlToValidate="txtServerAddress" ErrorMessage="*" />
                </div>
            </td>
        </tr>
        <tr>
            <td style="padding-top:20px;">
                <div style="padding-bottom:5px;"><b><%= Resources.NCMWebContent.WEBDATA_IC_57 %></b></div>
                <div>
                    <asp:TextBox ID="txtPortNumber" runat="server" CssClass="SettingTextField" Width="200px" Height="18px" MaxLength="4000"/>
                    <asp:RequiredFieldValidator ID="rfvPortNumber" runat="server" Display="Dynamic" ControlToValidate="txtPortNumber" ErrorMessage="*"></asp:RequiredFieldValidator>
                    <ajaxToolkit:FilteredTextBoxExtender ID="ftbe" runat="server" TargetControlID="txtPortNumber" FilterType="Numbers" />
                </div>
            </td>
        </tr>
        <tr>
            <td style="padding-top:20px;">
                <asp:CheckBox ID="chUseSSL" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_IC_61 %>" OnClick="chkShowHideSSL(this);"  />
            </td>
        </tr>
        <tr style="padding-top:10px;">
              <td>
                <table id="SSLPortContent" cellpadding="0" cellspacing="0" width="205px" style="padding-top:5px;background-color:#e4f1f8;">
                    <tr>
                        <td style="padding-left:10px">
                        <b><%= Resources.NCMWebContent.WEBDATA_IC_57 %></b>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left:10px;padding-bottom:10px">
                        <asp:TextBox ID="txtSSLPortNumber" runat="server" CssClass="SettingTextField" Width="180px" Height="18px" MaxLength="4000"/>
                            <asp:RequiredFieldValidator ID="txtSSLPortNumberValidator" runat="server" Display="Dynamic" ControlToValidate="txtSSLPortNumber" ErrorMessage="*"></asp:RequiredFieldValidator>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtSSLPortNumber" FilterType="Numbers" />
                        </td>
                    </tr>
                </table>
             </td>
        </tr>
        <tr>
            <td style="padding-top:20px;">
                <div style="padding-bottom:5px;"><b><%= Resources.NCMWebContent.WEBDATA_IC_58 %></b></div>
                <div>
                    <asp:DropDownList ID="ddlAuthList" runat="server" onchange="ddlAuthListSelectIndexChange(this);" Width="200px" Height="20px" CssClass="SettingTextField">
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_IC_59 %>" Value="false"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_IC_60 %>" Value="true"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <table id="AuthContent" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <div style="padding-bottom:5px;"><b><%= Resources.NCMWebContent.WEBDATA_IC_62 %></b></div>
                            <div>                        
                                <asp:TextBox ID="txtUsername" runat="server" CssClass="SettingTextField" Width="200px" Height="18px" MaxLength="4000"/>
                                <asp:RequiredFieldValidator ID="rfvUsername" runat="server" Display="Dynamic" ControlToValidate="txtUsername" ErrorMessage="*" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td> 
                            <div style="padding-bottom:5px;"><b><%= Resources.NCMWebContent.WEBDATA_IC_63 %></b></div>
                            <div> 
                                <asp:TextBox ID="txtPassword" runat="server" CssClass="SettingTextField" Width="200px" Height="18px" TextMode="Password" MaxLength="4000"/>
                                <asp:RequiredFieldValidator ID="rfvPassword" runat="server" Display="Dynamic" ControlToValidate="txtPassword" ErrorMessage="*" />
                            </div>
                        </td>  
                    </tr>
                    <tr>
                        <td>
                            <div style="padding-bottom:5px;"><b><%= Resources.NCMWebContent.WEBDATA_IC_64 %></b></div>
                            <div> 
                                <asp:TextBox ID="txtPassword2" runat="server" CssClass="SettingTextField" Width="200px" Height="18px" TextMode="Password" MaxLength="4000"/>
                                <asp:RequiredFieldValidator ID="rfvPassword2" runat="server" Display="Dynamic" ControlToValidate="txtPassword2" ErrorMessage="*" />
                            </div>
                            <div>
                                <asp:CustomValidator id="comparePasswords" EnableClientScript="true" ClientValidationFunction="cvPassword_Validate" ValidateEmptyText="true" ControlToValidate="txtPassword2" runat="server" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_689 %>" Display="Dynamic" Enabled="true" />
                            </div>
                        </td>  
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>        