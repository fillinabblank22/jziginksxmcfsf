<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EnableApprovalSystem.ascx.cs" Inherits="Orion_NCM_Admin_EnableApprovalSystem" %>

<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>

<orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />

<style type="text/css">
    #ncmMsgBoxContent a
    {
        color: #336699;
        text-decoration: underline;
    }
    
	#ncmMsgBoxContent a:hover 
	{ 
	    color:#ffa500;
	    text-decoration: underline;
	}
</style>

<script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
<%if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
<script type="text/javascript">
    Ext.namespace('SW');
    Ext.namespace('SW.NCM');
    Ext.QuickTips.init();

    SW.NCM.EnableApprovalSystem = function() {
    
        return {    
            init: function() {
                
                var enableApprovalSystem = <%=EnableApprovalSystem%>;
                var bypassApproval = <%=BypassApproval%>;
                var remediationMessage= bypassApproval ? '<%= Resources.NCMWebContent.WEBDATA_IC_235 %>' : '<%= Resources.NCMWebContent.WEBDATA_IC_236 %>';
                                        

                
                var dlg;
                $("#enableApprovalSystem").click(function() {                    
                    var dlg = new Ext.Window({
		                title: enableApprovalSystem ?  '<%= Resources.NCMWebContent.WEBDATA_VK_17 %>' : '<%= Resources.NCMWebContent.WEBDATA_VK_18 %>', 
		                html: String.format("<div style='border:solid 1px #dcdbd7;background-color:white;'>{0}</div>", 
		                        enableApprovalSystem ?
                                String.format('<%= Resources.NCMWebContent.WEBDATA_VK_25 %>', 
                                    "<div id='ncmMsgBoxContent' style='padding:10px;'>",
                                    "<div>",
                                    "<b>",
                                    "</b>",
                                    "</br></br>") :
                                String.format('<%= Resources.NCMWebContent.WEBDATA_VK_26 %>', 
                                    "<div id='ncmMsgBoxContent' style='padding:10px;'>",
                                    "<div>",
                                    "<b>",
                                    "</b>",
                                    "</br></br>",
                                    "<a href='/Orion/NCM/Admin/ConfigChangeApproval/ApprovalSetup.aspx'>",
                                    "</a>",remediationMessage )),
		                width: 400, 
		                autoHeight: true, 
		                border: false, 
		                region: 'center', 
		                layout: 'fit',
		                closable: false, 
		                modal: true, 
		                resizable: false, 
		                plain: true, 
		                bodyStyle: 'padding:5px 5px 5px 5px;',
		                buttonAlign : 'right',
		                buttons: [{ 
		                    text: enableApprovalSystem ? '<%= Resources.NCMWebContent.WEBDATA_VK_19 %>' :  '<%= Resources.NCMWebContent.WEBDATA_VK_20 %>', 
		                    handler: function() 
		                    {
		                        enableApprovalSystem = !enableApprovalSystem;
		                        ORION.callWebService("/Orion/NCM/Services/ApprovalRequests.asmx",
                                    "SetEnableApprovalSystem", { enableApprovalSystem: enableApprovalSystem },
                                    function(result) {
                                        if(result)
                                        {
                             	            $("#enableApprovalSystem").html(enableApprovalSystem ? '<%= Resources.NCMWebContent.WEBDATA_VK_21 %>' : '<%= Resources.NCMWebContent.WEBDATA_VK_22 %>');
                             	        }
                                });
		                        dlg.hide();
		                    }
		                    },{
		                    text: enableApprovalSystem ? '<%= Resources.NCMWebContent.WEBDATA_VK_23 %>' : '<%= Resources.NCMWebContent.WEBDATA_VK_24 %>', 
		                    handler: function() 
		                    {
		                        dlg.hide();
		                    }
		               }]
	                });
	                dlg.show();            
                });
            }
        }
    }(); 
    Ext.onReady(SW.NCM.EnableApprovalSystem.init, SW.NCM.EnableApprovalSystem);
</script>
<%}%>

<span class="LinkArrow">&#0187;</span>
  <%if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) 
  {%>
    <a href="#" style="" id="enableApprovalSystem"><%=EnableApprovalSystemLink%></a>
  <%}else{%>
	<a href="javascript:demoAction('<%=DemoActionKey%>', this);" style="" id="enableApprovalSystem"><%=EnableApprovalSystemLink%></a>
  <%}%>

