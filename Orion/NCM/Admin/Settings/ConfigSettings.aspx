<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="ConfigSettings.aspx.cs" Inherits="Orion_NCM_Admin_Settings_ConfigSettings"%>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/Controls/Slider.ascx" TagPrefix="orion" TagName="Slider" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
    <orion:Include ID="Include1" runat="server" File="Admin.css" />
    <orion:Include ID="Include2" runat="server" File="Discovery.css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="Settings.css" />
    <style type="text/css">
        tr.slider > td
        {
            text-align:left;
            white-space:nowrap;
            padding:10px;
            background-color:#e4f1f8;
        }
        
        .ButtonRemove
        {
	        font-size:10pt;
	        font-family: Arial,Verdana,Helvetica,sans-serif;
	        display: inline-block; 
	        position: relative;
	        padding: 7px 5px 7px 26px; 
	        color: #000;
        }
        
        .ButtonRemove-Icon 
        {
	        position: absolute;
	        left: 5px; 
	        top: 7px; 
	        height: 16px; 
	        width: 16px; 
	        background: url(/Orion/NCM/Resources/images/icon_delete.png) top left no-repeat; 
        }
	</style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMPHConfigSettings" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <asp:Panel ID="ContentContainer" CssClass="ContentContainer" runat="server">
        <div class="SettingContainer">
            <div class="SettingBlockHeader"><%=Resources.NCMWebContent.WEBDATA_VK_569 %></div>
            <div style="padding-top:20px;">
                <table cellpadding="0" cellspacing="0">
                    <tr class="slider">
                        <orion:Slider runat="server" ID="slConcurrentDownloads" TableCellCount="3" Label="<%$ Resources: NCMWebContent, WEBDATA_VK_570 %>" RangeFrom="1" RangeTo="100" Unit="<%$ Resources: NCMWebContent, WEBDATA_VK_571 %>" ValidateRange="true" Step="1" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_572 %>" />
                        <td><span class="SettingNote"><%=string.Format(Resources.NCMWebContent.WEBDATA_VK_699, 25) %></span></td>
                    </tr>
                    <tr class="slider">
                        <orion:Slider runat="server" ID="slSNMPConfigTransferTimeout" TableCellCount="3" Label="<%$ Resources: NCMWebContent, WEBDATA_VK_573 %>" RangeFrom="2" RangeTo="14" Unit="<%$ Resources: NCMWebContent, WEBDATA_VK_574 %>" ValidateRange="true" Step="1" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_575 %>" />
                        <td><span class="SettingNote"><%=string.Format(Resources.NCMWebContent.WEBDATA_VK_701, 4) %></span></td>
                    </tr>
                    <tr class="slider">
                        <orion:Slider runat="server" ID="slMinimumConfigLength" TableCellCount="3" Label="<%$ Resources: NCMWebContent, WEBDATA_VK_576 %>" RangeFrom="1" RangeTo="20" Unit="<%$ Resources: NCMWebContent, WEBDATA_VK_577 %>" ValidateRange="true" Step="1" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_578 %>" />
                        <td><span class="SettingNote"><%=string.Format(Resources.NCMWebContent.WEBDATA_VK_700, 11) %></span></td>
                    </tr>
                </table>
            </div>
            
            <div class="SettingBlockHeader" style="padding-top:50px;"><%=Resources.NCMWebContent.WEBDATA_VK_579 %></div>
            <div class="SettingBlockDescription"><%=Resources.NCMWebContent.WEBDATA_VK_580 %></div>
            <div style="padding-top:20px;">
                <table cellpadding="0" cellspacing="0" style="background-color:#e4f1f8;">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0">
                                <tr class="slider">
                                        <orion:Slider runat="server" ID="slComparisonOutputWidth" TableCellCount="3" Label="<%$ Resources: NCMWebContent, WEBDATA_VK_581 %>" RangeFrom="100" RangeTo="1000" Unit="" ValidateRange="true" Step="2" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_582 %>" />
                                     <td style="color:Red">
                                        <asp:Label ID="lblCompOutWidthNotif" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_IA_29 %>"
                                            Visible="false"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top:10px;">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="padding-top:10px;padding-left:10px;" nowrap="nowrap" valign="top">
                                        <asp:RadioButton ID="rbFullSideBySideComparison" GroupName="Comparison" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_583 %>" runat="server"/>
                                    </td>
                                    <td style="padding-top:10px;padding-left:20px;" nowrap="nowrap" valign="top">
                                        <asp:RadioButton ID="rbNumberOfLinesComparison" GroupName="Comparison" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_584 %>" runat="server"></asp:RadioButton>
                                    </td>
                                    <td nowrap="nowrap">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr class="slider">
                                                <orion:Slider runat="server" ID="slNumberOfLines" TableCellCount="3" Label="" RangeFrom="0" RangeTo="100" Unit="<%$ Resources: NCMWebContent, WEBDATA_VK_577 %>" ValidateRange="true" Step="1" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_585 %>" />
                                                <td><span class="SettingNote"><%=string.Format(Resources.NCMWebContent.WEBDATA_VK_700, 5) %></span></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>                    
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            
            <asp:UpdatePanel ID="uPanel2" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <div class="SettingBlockHeader" style="padding-top:50px;"><%=Resources.NCMWebContent.WEBDATA_VK_586 %></div>
                    <div style="padding-top:20px;">
                        <asp:CheckBox ID="chkEnableConfigMirror" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_587 %>" AutoPostBack="true" OnCheckedChanged="chkEnableConfigMirror_CheckedChanged" />
                    </div>
                    <div style="padding-top:20px;">
                        <asp:CheckBox ID="chkKeepMultipleRevisions" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_588 %>" />
                    </div>
                    <div id="ConfigMirrorSettings" runat="server">
                        <div>
                            <asp:Repeater runat="server" ID="repeater" OnItemDataBound="repeater_ItemDataBound" OnItemCommand="repeater_ItemCommand">
                                <HeaderTemplate>                            
                                    <table border="0" cellpadding="0" cellspacing="0">
                                </HeaderTemplate>
                                <ItemTemplate>
                                        <tr>
                                            <td id="tdHolder" runat="server">&nbsp;</td>
                                        </tr>
                                        <tr id="exHolder" runat="server" visible="false">
                                            <td style="padding-top:10px;">
                                                <span class="SettingException">
                                                    <span class="SettingException-Icon"></span><asp:Literal ID="exWarn" runat="server" />
                                                </span>
                                            </td>
                                        </tr>
                                        <asp:Literal ID="litServerName" runat="server"></asp:Literal>
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="hiddenEngineID" runat="server"></asp:HiddenField>
                                                <div style="padding-top:20px;">
                                                    <div style="padding-bottom:5px;"><%=Resources.NCMWebContent.WEBDATA_VK_589 %></div>
                                                    <asp:TextBox ID="txtConfigArchiveDirectory" runat="server" TextMode="SingleLine" CssClass="SettingTextField" Width="500px" />
                                                    <orion:LocalizableButton runat="server" ID="btnValidate" LocalizedText="CustomText" DisplayType="Small" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_590 %>" CausesValidation="false" CommandName="Validate"/>
                                                    <span style="padding-left:10px;"><asp:CheckBox ID="chkUseFirstPoller" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_591 %>" AutoPostBack="true" OnCheckedChanged="chkUseFirstPoller_CheckedChanged" /></span>
                                                </div>
                                                <div id="validatorHolder" runat="server"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="padding-top:20px;padding-bottom:10px;">
                                                    <div style="padding-bottom:5px;"><%=Resources.NCMWebContent.WEBDATA_VK_592 %></div>
                                                    <asp:TextBox ID="txtConfigMirrorTemplate" runat="server" TextMode="SingleLine" CssClass="SettingTextField" Width="500px" />
                                                </div>
                                            </td>
                                        </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>                            
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        
        <div style="padding-top:20px;">
            <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" 
            OnClick="btnSubmit_Click" DisplayType="Primary"
            CommandArgument="~/Orion/NCM/Admin/Default.aspx" CausesValidation="true"/> 
            <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" 
            OnClick="btnCancel_Click" DisplayType="Secondary"
            CommandArgument="~/Orion/NCM/Admin/Default.aspx" CausesValidation="false"/>
        </div>
    </asp:Panel>
</asp:Content>


