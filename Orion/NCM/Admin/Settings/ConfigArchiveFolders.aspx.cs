﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.Orion.Web;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCM.Common.Dal;

public partial class Orion_NCM_Admin_Settings_ConfigArchiveFolders : System.Web.UI.Page
{
    private ISWrapper isLayer;
    private static readonly SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();

    protected bool IsMultipleEngines => LookupEngines().Any();

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ValidatePageAccess();
    }

    private void ValidatePageAccess()
    {
        if (CommonHelper.IsDemoMode() ||
            !(this.Page is IBypassAccessLimitation) && !Profile.AllowAdmin && !SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR))
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_VK_554}");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_VK_975;

        isLayer = new ISWrapper();

        if (!Page.IsPostBack)
        {
            var list = new List<ConfigArchive>();
            list.AddRange(GetConfigArchivePathFromSettings());
            if (SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_WEBVIEWER))
                _log.Warn("WebViewer doesn't have permission to get Job info");
            else
                list.AddRange(GetConfigArchivePathFromJobs());

            repeater.DataSource = list;
            repeater.DataBind();
        }
    }

    private List<ConfigArchive> GetConfigArchivePathFromSettings()
    {
        var exHtml = string.Empty;
        var list = new List<ConfigArchive>();

        isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            foreach (var engine in LookupEngines())
            {
                try
                {
                    var appDataPath = proxy.Cirrus.Settings.GetAppDataPath(engine.EngineId);
                    var configArchivePath = proxy.Cirrus.Settings.GetSetting(Settings.ConfigArchiveDirectory, System.IO.Path.Combine(appDataPath, "NCM\\Config-Archive"), engine.EngineId);
                    var coreInstallPath = proxy.Cirrus.Settings.GetCoreInstallPath(engine.EngineId);

                    list.Add(new ConfigArchive
                    {
                        Source = string.Format(Resources.NCMWebContent.WEBDATA_VK_976, SettingAttribute.Read(Settings.ConfigArchiveDirectory).Name),
                        Engine = engine.EngineName,
                        Path = configArchivePath,
                        HRef = @"/Orion/NCM/Admin/Settings/ConfigSettings.aspx",
                        CssClass = configArchivePath.StartsWith(coreInstallPath) ? @"highlightRow" : string.Empty
                    });
                }
                catch (Exception ex)
                {
                    exHtml +=
                        $@"<tr><td><span class='SpanException'><span class='SpanException-Icon'></span>{ex.Message}</span></td></tr>";
                }
            }
        }
        if (!string.IsNullOrEmpty(exHtml))
        {
            exLiteral.Text =
                $@"<div style='padding-top:5px;padding-bottom:10px;'><table cellpadding='0' cellspacing='0' class='TableException'>{exHtml}</table></div>";
        }
        return list;
    }

    private List<ConfigArchive> GetConfigArchivePathFromJobs()
    {
        var list = new List<ConfigArchive>();

        isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            IEnumerable<int> jobIds = proxy.Query(
                $@"SELECT NCMJobID FROM Cirrus.NCM_NCMJobs WHERE NCMJobType={(int) eJobType.ExportConfigs}").AsEnumerable().Select(row => row.Field<int>("NCMJobID"));
            foreach (var jobId in jobIds)
            {
                var job = proxy.Cirrus.Jobs.GetJob(jobId);
                if (job.JobDefinition.Parameters.FilePathList != null)
                {
                    foreach (var engine in LookupEngines())
                    {
                        try
                        {
                            var filePath = job.JobDefinition.Parameters.FilePathList.Find(item => item.EngineID == engine.EngineId);
                            if (filePath != null)
                            {
                                var coreInstallPath = proxy.Cirrus.Settings.GetCoreInstallPath(engine.EngineId);

                                list.Add(new ConfigArchive
                                {
                                    Source = string.Format(Resources.NCMWebContent.WEBDATA_VK_977, job.JobName),
                                    Engine = engine.EngineName,
                                    Path = filePath.File_Path,
                                    HRef = $@"/Orion/NCM/Resources/Jobs/Default.aspx?JobID={jobId}",
                                    CssClass = filePath.File_Path.StartsWith(coreInstallPath) ? @"highlightRow" : string.Empty
                                });
                            }
                        }
                        catch { }
                    }
                }
            }
        }
        return list;
    }

    private IEnumerable<EngineInfo> LookupEngines()
    {
        var enginesDal = ServiceLocator.Container.Resolve<ISwisEnginesDal>();
        return enginesDal.GetAllPollingEngines();
    }

    private class ConfigArchive
    {
        public string Source { get; set; }
        public string Engine { get; set; }
        public string Path { get; set; }
        public string HRef { get; set; }
        public string CssClass { get; set; }
    }
   
}