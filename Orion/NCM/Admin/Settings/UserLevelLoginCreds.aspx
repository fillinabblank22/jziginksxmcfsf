<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="UserLevelLoginCreds.aspx.cs" Inherits="Orion_NCM_Admin_Settings_UserLevelLoginCreds" Title="Untitled Page" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
    <script type="text/javascript">
        
        function cvPassword_Validate(source, args)
        {
            var id = source.attributes.getNamedItem('controltocompareid').value;
            args.IsValid = (args.Value == document.getElementById(id).value);
        }
        
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="Settings.css" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMPHUserLoginCreds" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <asp:Panel ID="ContentContainer" CssClass="ContentContainer" runat="server">
        <div class="SettingContainer">
            <div>
                <%= string.Format(Resources.NCMWebContent.WEBDATA_VK_710, @"<a style='color:#336699;' href='/Orion/NCM/Admin/Settings/Security.aspx'>", @"</a>") %>
            </div>
        
            <div>
                <div style="font-weight:bold;padding-top:20px;"><%=Resources.NCMWebContent.WEBDATA_IC_62 %></div>
                <div style="padding-top:5px"><ncm:PasswordTextBox ID="txtUserName" runat="server" TextMode="SingleLine" CssClass="SettingTextField" Width="200px" /></div>
                
                <div style="font-weight:bold;padding-top:20px;"><%=Resources.NCMWebContent.WEBDATA_IC_63 %></div>
                <div style="padding-top:5px"><ncm:PasswordTextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="SettingTextField" Width="200px" /></div>
                
                <div style="font-weight:bold;padding-top:20px;"><%=Resources.NCMWebContent.WEBDATA_IC_64 %></div>
                <div style="padding-top:5px"><ncm:PasswordTextBox ID="txtConfirmPassword" runat="server" TextMode="Password" CssClass="SettingTextField" Width="200px" /></div>
                <div>
                    <asp:CustomValidator id="compareValidator" EnableClientScript="true" ClientValidationFunction="cvPassword_Validate" ValidateEmptyText="true" ControlToValidate="txtConfirmPassword" runat="server" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_689 %>" Display="Dynamic" Enabled="true" />
                </div>
                
                <div style="font-weight:bold;padding-top:20px;"><%=Resources.NCMWebContent.WEBDATA_VK_283 %></div>
                <div style="padding-top:5px">
                    <asp:DropDownList ID="ddlEnableLevel" runat="server" CssClass="SettingSelectField" Width="200px">
                        <asp:ListItem Text="" Value=""></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_314 %>" Value="<No Enable Login>"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_315 %>" Value="enable"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                
                <div style="font-weight:bold;padding-top:20px;"><%=Resources.NCMWebContent.WEBDATA_VM_137 %></div>
                <div style="padding-top:5px"><ncm:PasswordTextBox ID="txtEnablePassword" runat="server" TextMode="Password" CssClass="SettingTextField" Width="200px" /></div>
            </div>
        </div>
        
        <div style="padding-top:20px;">
            <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" 
            OnClick="btnSubmit_Click" DisplayType="Primary" CausesValidation="true"/> 
            <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" 
            OnClick="btnCancel_Click" DisplayType="Secondary" CausesValidation="false"/>
        </div>
    </asp:Panel>
</asp:Content>

 