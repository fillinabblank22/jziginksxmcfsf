<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="EmailDefaults.aspx.cs" Inherits="Orion_NCM_Admin_Settings_EmailDefaults" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="Settings.css" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMPHEmailNotificationDefaults" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <asp:Panel ID="ContentContainer" CssClass="ContentContainer" runat="server">
        <div class="SettingContainer">
            <div class="SettingDescription"><%=Resources.NCMWebContent.WEBDATA_IC_68 %></div>
            <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding-top:20px;">
                                <div style="padding-bottom:5px;">
                                    <b><%=Resources.NCMWebContent.WEBDATA_IC_70 %></b>
                                </div>
                                <div>
                                    <asp:TextBox ID="txtSenderName" runat="server" CssClass="SettingTextField" Width="300px" Height="18px" MaxLength="4000"/>
                                    <asp:RequiredFieldValidator ID="rfvSenderName" runat="server" Display="Dynamic" ControlToValidate="txtSenderName" ErrorMessage="*" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top:20px;">
                                <div style="padding-bottom:5px;">
                                    <b><%=Resources.NCMWebContent.WEBDATA_IC_71 %></b>
                                </div>
                                <div>
                                    <asp:TextBox ID="txtReplyAddress" runat="server" CssClass="SettingTextField" Width="300px" Height="18px" MaxLength="4000"/>
                                    <asp:RequiredFieldValidator ID="rfvReplyAddress" runat="server" Display="Dynamic" ControlToValidate="txtReplyAddress" ErrorMessage="*"></asp:RequiredFieldValidator>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top:20px;">
                                <div style="padding-bottom:5px;">
                                    <b><%=Resources.NCMWebContent.WEBDATA_IC_72 %></b>
                                </div>
                                <div>
                                    <asp:TextBox ID="txtSubject" runat="server" CssClass="SettingTextField" Width="300px" Height="18px" MaxLength="4000"/>                            
                                </div>
                            </td>
                        </tr>
                        <tr>
                             <td style="padding-top:20px;padding-bottom:5px;"> 
                                <b><%=Resources.NCMWebContent.WEBDATA_IC_73 %></b>
                             </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtTo" runat="server" CssClass="SettingTextField" Width="300px" Height="18px" MaxLength="4000"/>
                                <asp:RequiredFieldValidator ID="rfvTo" runat="server" Display="Dynamic" ControlToValidate="txtTo" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <td style="padding-left:5px"> 
                                <orion:LocalizableButton runat="server" ID="btnAddCC" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_IC_74 %>" OnClick="btnAddCC_Click" DisplayType="Small" CausesValidation="false" />
                            </td>
                            <td style="padding-left:5px"> 
                                <orion:LocalizableButton runat="server" ID="btnAddBCC" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_IC_75 %>" OnClick="btnAddBCC_Click" DisplayType="Small" CausesValidation="false"/>
                            </td>
                        </tr>
                        <tr id="CCPanel" runat="server" style="display:none;">
                            <td style="padding-top:20px;">
                                 <table cellpadding="0" cellspacing="0">
                                     <tr>
                                         <td style="padding-bottom:5px;"> 
                                            <b><%=Resources.NCMWebContent.WEBDATA_IC_76 %></b>
                                         </td>
                                     </tr>
                                     <tr>
                                        <td>
                                            <asp:TextBox ID="txtCC" runat="server" CssClass="SettingTextField" Width="300px" Height="18px" MaxLength="4000"/>                        
                                        </td>
                                     </tr>
                                 </table>
                            </td>
                        </tr>
                        <tr id="BCCPanel" runat="server" style="display:none;">
                            <td style="padding-top:20px;">
                                 <table cellpadding="0" cellspacing="0">
                                     <tr>
                                         <td style="padding-bottom:5px;"> 
                                            <b><%=Resources.NCMWebContent.WEBDATA_IC_77 %></b>
                                         </td>
                                     </tr>
                                     <tr>
                                        <td>
                                            <asp:TextBox ID="txtBCC" runat="server" CssClass="SettingTextField" Width="300px" Height="18px" MaxLength="4000"/>                        
                                        </td>
                                     </tr>
                                 </table>
                            </td>
                        </tr> 
                    </table>
                    <div class="SettingHint" style="padding-top:15px;"><%=Resources.NCMWebContent.WEBDATA_IC_78 %></div>
                    <div style="padding-top:30px;">
                        <span class="SettingSuggestion"><span class="SettingSuggestion-Icon"></span>
                            <%=string.Format(Resources.NCMWebContent.WEBDATA_IC_79,  @"<a style='color:#336699;text-decoration:underline;' href='SMTPServer.aspx'>", @"</a>") %>
                        </span>
                    </div>
                 </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        
        <div style="padding-top:20px;">
            <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" 
            OnClick="btnSubmit_Click" DisplayType="Primary"
            CommandArgument="~/Orion/NCM/Admin/Default.aspx"/> 
            <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" 
            OnClick="btnCancel_Click" DisplayType="Secondary"
            CommandArgument="~/Orion/NCM/Admin/Default.aspx" CausesValidation="false"/>
        </div>
    </asp:Panel>
</asp:Content>