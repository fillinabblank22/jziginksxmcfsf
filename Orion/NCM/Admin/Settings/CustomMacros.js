Ext.namespace("SW");
Ext.namespace("SW.NCM");
Ext.QuickTips.init();

SW.NCM.CustomMacros = function() {

    function gridResize() {
        if (grid != null) {
            grid.setWidth(1);
            grid.setWidth($("#gridCell").width());

            grid.setHeight(getGridHeight());
            grid.doLayout();
        }
    }

    function getGridHeight() {
        var top = $("#gridCell").offset().top;
        return $(window).height() - top - $("#footer").height() - 100;
    }

    var updateToolbarButtons = function() {
        var selCount = grid.getSelectionModel().getCount();
        var map = grid.getTopToolbar().items.map;

        var needsOnlyOneSelected = (selCount !== 1);
        var needsAtLeastOneSelected = (selCount === 0);

        map.EditMacroBtn.setDisabled(needsOnlyOneSelected);
        map.DeleteMacrosBtn.setDisabled(needsAtLeastOneSelected);

        // revice 'Select All' checkbox state
        var hd = Ext.fly(grid.getView().innerHd).child("div.x-grid3-hd-checker");
        if (grid.getStore().getCount() > 0 && selCount == grid.getStore().getCount()) {
            hd.addClass("x-grid3-hd-checker-on");
        } else {
            hd.removeClass("x-grid3-hd-checker-on");
        }
    };

    var doSearch = function() {

        var map = grid.getTopToolbar().items.map;
        var search = map.txtSearch.getValue();
        var searchBtn = map.Search;
        if ($.trim(search).length === 0) return;
        refreshObjects(search);
        map.Clean.setVisible(true);
    };

    var doClean = function() {

        var map = grid.getTopToolbar().items.map;
        map.Clean.setVisible(false);
        map.txtSearch.setValue("");
        refreshObjects("");
    };

    var refreshObjects = function(search, callback) {
        grid.store.removeAll();
        var pagingToolbar = grid.getBottomToolbar();
        pagingToolbar.cursor = gridCursor;
        gridCursor = 0;
        grid.store.baseParams["limit"] = grid.getBottomToolbar().pageSize;
        grid.store.proxy.conn.jsonData = { search: search };
        currentSearchTerm = search;
        grid.store.load({ callback: callback });
    };

    var isMacroNameInUseAlready = function(newname, callback) {
        ORION.callWebService("/Orion/NCM/Services/Settings.asmx",
            "ValidateCustomMacroName",
            { name: newname },
            function(result) {
                callback(result);
            });
    };

    var internalCreateEditCustomMacro = function(mName, mValue) {
        ORION.callWebService("/Orion/NCM/Services/Settings.asmx",
            "CreateEditCustomMacros",
            { name: mName, value: mValue },
            function(result) {
                refreshObjects("");
            });
    };

    function createCustomMacro() {
        ShowCreateEditMacroDlg(null, null, internalCreateEditCustomMacro);

    };

    function editCustomMacro(item) {
        ShowCreateEditMacroDlg(item.data.SettingName, item.data.SettingValue, internalCreateEditCustomMacro);

    };

    var internalDeleteMacros = function(ids, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/Settings.asmx",
            "DeleteCustomMacros",
            { names: ids },
            function(result) {
                onSuccess(result);
            });
    };

    var gridCursor = 0;

    function deleteMacros(items) {
        var toDelete = getSelectedMacrosIds(items, false);
        var waitMsg = Ext.Msg.wait("@{R=NCM.Strings;K=WEBJS_VM_56;E=js}");
        internalDeleteMacros(toDelete,
            function(result) {
                waitMsg.hide();
                errorHandler(result);
                var pagingToolbar = grid.getBottomToolbar();

                var currentPageRecordCount = grid.store.getCount();
                var selectedRecordCount = grid.getSelectionModel().getCount();
                if (currentPageRecordCount > selectedRecordCount) {
                    gridCursor = pagingToolbar.cursor;
                } else {
                    gridCursor = 0;
                }

                refreshObjects("");
            });
    };

    var getSelectedMacrosIds = function(items, all) {
        var ids = [];

        Ext.each(items,
            function(item) {
                ids.push(item.data.SettingName);
            });

        return ids;
    };

    function renderMacroName(value) {
        var formattedValue = formatMacroName(Ext.util.Format.htmlEncode(value));
        return formattedValue;
    };

    function formatMacroName(value) {
        return "${" + value + "}";
    };

    function renderMacroValue(value) {
        return Ext.util.Format.htmlEncode(value);
    };

    var dlgInstance;
    var isEditMode;

    ShowCreateEditMacroDlg = function(name, value, submitFnc) {
        isEditMode = name != null;
        var dlgTitle;
        if (isEditMode) {
            dlgTitle = "@{R=NCM.Strings;K=WEBJS_VM_59;E=js}";
        } else {
            dlgTitle = "@{R=NCM.Strings;K=WEBJS_VM_60;E=js}";
        }

        if (!dlgInstance) {

            var nameCmp = new Ext.form.TextField({
                id: "txtMacroName",
                renderTo: "txtNameDiv",
                emptyText: "@{R=NCM.Strings;K=WEBJS_VM_61;E=js}",
                value: name,
                disabled: false,
                allowBlank: false,
                regex: new RegExp("^[^\$].*"),
                regexText: "@{R=NCM.Strings;K=WEBJS_VM_73;E=js}",
                width: 200
            });

            var valueCmp = new Ext.form.TextField({
                id: "txtMacroValue",
                renderTo: "txtValueDiv",
                emptyText: "@{R=NCM.Strings;K=WEBJS_VM_62;E=js}",
                value: value,
                width: 200
            });

            dlgInstance = new Ext.Window({
                applyTo: "AddEditMacrosDlg",
                layout: "fit",
                width: 370,
                autoHeight: true,
                closeAction: "hide",
                plain: true,
                resizable: false,
                modal: true,
                contentEl: "AddEditMacrosDlgBody",
                buttons: [
                    {
                        text: "@{R=NCM.Strings;K=WEBJS_VM_63;E=js}",
                        handler: function() {

                            var mName = nameCmp.getValue();
                            var mVal = valueCmp.getValue();

                            if (!nameCmp.validate())
                                return;

                            if (!valueCmp.validate())
                                return;

                            if (!isEditMode) {
                                var waitMsg = Ext.Msg.wait("@{R=NCM.Strings;K=WEBJS_VM_64;E=js}");
                                isMacroNameInUseAlready(mName,
                                    function(res) {
                                        waitMsg.hide();
                                        if (res) {
                                            submitFnc(mName, mVal);
                                            dlgInstance.hide();

                                        } else {
                                            Ext.Msg.show({
                                                title: "@{R=NCM.Strings;K=WEBJS_VM_74;E=js}",
                                                msg: "@{R=NCM.Strings;K=WEBJS_VM_65;E=js}",
                                                minWidth: 300,
                                                buttons: Ext.Msg.OK,
                                                icon: Ext.MessageBox.ERROR
                                            });
                                        }
                                    }
                                );
                            } else {
                                submitFnc(mName, mVal);
                                dlgInstance.hide();
                            }

                        }
                    },
                    {
                        text: "@{R=NCM.Strings;K=WEBJS_VK_38;E=js}",
                        handler: function() { dlgInstance.hide(); }
                    }
                ]
            });
        }

        var cmpMValue = Ext.getCmp("txtMacroValue");
        var cmpMName = Ext.getCmp("txtMacroName");

        if (isEditMode) {

            cmpMValue.setValue(value);
            cmpMName.setValue(name);
            cmpMName.setDisabled(true);
        } else {
            cmpMValue.setValue(null);
            cmpMName.setValue(null);
            cmpMName.setDisabled(false);
        }

        dlgInstance.setTitle(dlgTitle);
        dlgInstance.alignTo(document.body, "c-c");
        dlgInstance.show();
    };

    function errorHandler(result) {
        if (result != null && result.Error) {
            Ext.Msg.show({
                title: result.Source,
                msg: result.Msg,
                minWidth: 500,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        }
        if (result != null && result.Error == false) {
            Ext.Msg.show({
                title: result.Source,
                msg: result.Msg,
                minWidth: 500,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.INFO
            });
        }
    }

    var selectorModel;
    var dataStore;
    var grid;

    return {
        init: function() {

            dataStore = new ORION.WebServiceStore(
                "/Orion/NCM/Services/Settings.asmx/GetCustomMacrosPaged",
                [
                    { name: "SettingName", mapping: 0 },
                    { name: "SettingValue", mapping: 1 }
                ],
                "SettingName");

            var comboPS = new Ext.form.ComboBox({
                name: "perpage",
                width: 50,
                store: new Ext.data.SimpleStore({
                    fields: ["id"],
                    data: [
                        ["25"],
                        ["50"],
                        ["75"],
                        ["100"]
                    ]
                }),
                mode: "local",
                value: "25",

                listWidth: 50,
                triggerAction: "all",
                displayField: "id",
                valueField: "id",
                editable: false,
                forceSelection: true
            });

            var pagingToolbar = new Ext.PagingToolbar({
                store: dataStore,
                pageSize: 25,
                displayInfo: true,
                emptyMsg: "@{R=NCM.Strings;K=WEBJS_VM_66;E=js}",
                items: ["-", "@{R=NCM.Strings;K=WEBJS_VY_12;E=js} ", comboPS]
            });

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", updateToolbarButtons);

            comboPS.on("select",
                function(combo, record) {
                    var psize = parseInt(record.get("id"), 10);
                    grid.store.baseParams["limit"] = psize;
                    pagingToolbar.pageSize = psize;
                    pagingToolbar.cursor = 0;
                    pagingToolbar.doLoad(pagingToolbar.cursor);
                },
                this);

            grid = new Ext.grid.GridPanel({
                store: dataStore,

                columns: [
                    selectorModel, {
                        header: "@{R=NCM.Strings;K=WEBJS_VM_67;E=js}",
                        width: 350,
                        sortable: true,
                        dataIndex: "SettingName",
                        renderer: renderMacroName
                    }, {
                        header: "@{R=NCM.Strings;K=WEBJS_VM_68;E=js}",
                        width: 350,
                        sortable: false,
                        dataIndex: "SettingValue",
                        renderer: renderMacroValue
                    }
                ],

                sm: selectorModel,

                layout: "fit",
                autoScroll: "true",
                loadMask: true,

                width: 750,
                height: 500,
                stripeRows: true,

                tbar: [
                    {
                        id: "AddNewMacroBtn",
                        text: "@{R=NCM.Strings;K=WEBJS_VK_131;E=js}",
                        tooltip: "@{R=NCM.Strings;K=WEBJS_VM_60;E=js}",
                        icon: "/Orion/NCM/Resources/images/ConfigSnippets/icon_addsnippet.gif",
                        cls: "x-btn-text-icon",
                        handler: function() { createCustomMacro(); }
                    }, "-", {
                        id: "EditMacroBtn",
                        text: "@{R=NCM.Strings;K=WEBJS_VY_03;E=js}",
                        tooltip: "@{R=NCM.Strings;K=WEBJS_VM_69;E=js}",
                        icon: "/Orion/NCM/Resources/images/ConfigSnippets/icon_editsnippet.gif",
                        cls: "x-btn-text-icon",
                        handler: function() { editCustomMacro(grid.getSelectionModel().getSelected()); }
                    }, "-", {
                        id: "DeleteMacrosBtn",
                        text: "@{R=NCM.Strings;K=WEBJS_VY_05;E=js}",
                        tooltip: "@{R=NCM.Strings;K=WEBJS_VM_70;E=js}",
                        icon: "/Orion/NCM/Resources/images/icon_delete.png",
                        cls: "x-btn-text-icon",
                        handler: function() { deleteMacros(grid.getSelectionModel().getSelections()); }
                    }, "->", {
                        id: "txtSearch",
                        xtype: "textfield",
                        emptyText: "@{R=NCM.Strings;K=WEBJS_VM_72;E=js}",
                        width: 200,
                        enableKeyEvents: true,
                        listeners: {
                            keypress: function(obj, evnt) {
                                if (evnt.keyCode == 13) {
                                    doSearch();
                                }
                            }
                        }
                    }, {
                        id: "Clean",
                        tooltip: "@{R=NCM.Strings;K=WEBJS_VK_30;E=js}",
                        iconCls: "ncm_clear-btn",
                        cls: "x-btn-icon",
                        hidden: true,
                        handler: function() { doClean(); }
                    }, {
                        id: "Search",
                        tooltip: "@{R=NCM.Strings;K=WEBJS_VK_31;E=js}",
                        iconCls: "ncm_search-btn",
                        cls: "x-btn-icon",
                        handler: function() { doSearch(); }
                    }
                ],

                bbar: pagingToolbar

            });

            grid.render("Grid");
            updateToolbarButtons();
            gridResize();
            refreshObjects("");
            $(window).resize(gridResize);

            //higlight search results
            grid.getView().on("refresh",
                function() {
                    var search = currentSearchTerm;
                    if ($.trim(search).length === 0) return;

                    var columnsToHighlight = SW.NCM.ColumnIndexByName(grid.getColumnModel(),
                        ["@{R=NCM.Strings;K=WEBJS_VM_67;E=js}", "@{R=NCM.Strings;K=WEBJS_VM_68;E=js}"]);
                    Ext.each(columnsToHighlight,
                        function(columnNumber) {
                            SW.NCM.highlightSearchText("#Grid .x-grid3-body .x-grid3-td-" + columnNumber, search);
                        });
                });
        }
    };

}();

Ext.onReady(SW.NCM.CustomMacros.init, SW.NCM.CustomMacros);