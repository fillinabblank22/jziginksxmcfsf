<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="SCPServer.aspx.cs" Inherits="Orion_NCM_Admin_Settings_SCPServer"%>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
    <script type="text/javascript">
        function cvPassword_Validate(source, args)
        {
            var id = source.attributes.getNamedItem('controltocompareid').value;
            args.IsValid = (args.Value == document.getElementById(id).value);
        }
    </script>
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="Settings.css" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMPHSCPServerSettings" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <asp:Panel ID="ContentContainer" CssClass="ContentContainer" runat="server">
         <asp:UpdatePanel ID="uPanel" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <div class="SettingContainer">
                    <div class="SettingBlockDescription">
                        <%=Resources.NCMWebContent.WEBDATA_VK_654 %>
                        <div style="padding-top:20px;"><asp:CheckBox ID="chkUseThirdPartySCPServer" runat="server" /></div>
                        <div style="padding-top:20px;"><asp:CheckBox ID="chkSCPAutoRetriveIP" runat="server" Text="" AutoPostBack="true" OnCheckedChanged="chkSCPAutoRetriveIP_OnCheckedChanged"/></div>
                        <div style="padding-top:40px;">
                            <asp:Repeater runat="server" ID="repeater" OnItemDataBound="repeater_ItemDataBound" OnItemCommand="repeater_ItemCommand">
                                <HeaderTemplate>                            
                                    <table border="0" cellpadding="0" cellspacing="0">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr id="exHolder" runat="server" visible="false">
                                        <td colspan="3" style="padding-top:10px;">
                                            <span class="SettingException">
                                                <span class="SettingException-Icon"></span><asp:Literal ID="exWarn" runat="server" />
                                            </span>
                                        </td>
                                    </tr>
                                    <asp:Literal ID="litServerName" runat="server"></asp:Literal>
                                    <tr>
                                        <td valign="top" style="border-right:1px solid #dcdbd7;padding-right:20px;" nowrap="nowrap">
                                            <asp:HiddenField ID="hiddenEngineID" runat="server"></asp:HiddenField>
                                            
                                            <div style="font-weight:bold;padding-bottom:5px;"><%=Resources.NCMWebContent.WEBDATA_IC_62 %></div>
                                            <div>
                                                <ncm:PasswordTextBox ID="txtSCPUsername" runat="server" TextMode="SingleLine" CssClass="SettingTextField" Width="200px" />
                                            </div>
                                            
                                            <div style="font-weight:bold;padding-bottom:5px;padding-top:20px;"><%=Resources.NCMWebContent.WEBDATA_IC_63 %></div>
                                            <div>
                                                <ncm:PasswordTextBox ID="txtSCPPassword" runat="server" TextMode="Password" CssClass="SettingTextField" Width="200px" />
                                            </div>
                                            
                                            <div style="font-weight:bold;padding-bottom:5px;padding-top:20px;"><%=Resources.NCMWebContent.WEBDATA_IC_64 %></div>
                                            <div>
                                                <ncm:PasswordTextBox ID="txtSCPConfirmPassword" runat="server" TextMode="Password" CssClass="SettingTextField" Width="200px" />
                                            </div>
                                        </td>
                                        <td valign="top" align="left" style="padding-left:20px;" nowrap="nowrap">
                                            <div style="font-weight:bold;padding-bottom:5px;"><%=Resources.NCMWebContent.WEBDATA_VK_655 %></div>
                                            <asp:TextBox ID="txtSCPServerAddress" runat="server" TextMode="SingleLine" CssClass="SettingTextField" Width="200px" />
                                            <div style="padding-top:5px;">
                                                <orion:LocalizableButton runat="server" ID="btnRetrieveIP" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_650 %>" DisplayType="Small" CommandName="RetrieveIP" CausesValidation="false" />
                                            </div>
                                        </td>
                                        <td valign="top" align="left" style="padding-left:20px;" nowrap="nowrap">
                                            <div style="font-weight:bold;padding-bottom:5px;"><%=Resources.NCMWebContent.WEBDATA_VK_649 %></div>
                                            <asp:TextBox ID="txtSCPRootDir" runat="server" TextMode="SingleLine" CssClass="SettingTextField" Width="400px" />
                                            <div style="padding-top:5px;">
                                                <orion:LocalizableButton runat="server" ID="btnValidate" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_590 %>" DisplayType="Small" CommandName="Validate" CausesValidation="false" />
                                                <span id="pathValidatorHolder" runat="server" style="padding-left:10px;"></span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="3" id="tdHolder" runat="server" style="padding-bottom:10px;">
                                            <asp:CustomValidator id="compareValidator" EnableClientScript="true" ClientValidationFunction="cvPassword_Validate" ValidateEmptyText="true" ControlToValidate="txtSCPConfirmPassword"  runat="server" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_689 %>" Display="Dynamic" Enabled="true" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
                <div style="padding-top:20px;">
                    <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" 
                    OnClick="btnSubmit_Click" DisplayType="Primary"
                    CommandArgument="~/Orion/NCM/Admin/Default.aspx" CausesValidation="true"/> 
                    <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" 
                    OnClick="btnCancel_Click" DisplayType="Secondary"
                    CommandArgument="~/Orion/NCM/Admin/Default.aspx" CausesValidation="false"/>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

