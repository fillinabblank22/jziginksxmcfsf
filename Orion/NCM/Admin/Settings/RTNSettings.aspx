<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="RTNSettings.aspx.cs" Inherits="Orion_NCM_Admin_Settings_RTNSettings" %>
<%@ Import Namespace="SolarWinds.NCMModule.Web.Resources" %>
<%@ Import Namespace="SolarWinds.Orion.Common" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers"%>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="Settings.css" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMPHRTCDInfo" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <asp:Panel ID="ContentContainer" CssClass="ContentContainer" runat="server">
        <div class="SettingContainer">
            <div><%=Resources.NCMWebContent.WEBDATA_IC_81 %></div>
            <div class="SettingBlockHeader" style="padding-top:20px;"><%= Resources.NCMWebContent.WEBDATA_IC_82 %></div>
            
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td nowrap="nowrap" valign="top" style="padding-top:5px;">
                        <b><%=Resources.NCMWebContent.WEBDATA_IC_83 %></b>
                    </td>
                    <td nowrap="nowrap" valign="top" style="padding-top:5px;padding-left:5px;">
                        <%=Resources.NCMWebContent.WEBDATA_IC_84 %>
                        <div style="font-size:8pt;padding-top:5px;">&#0187;&nbsp;<a style="color:#336699;" href="<%=this.LearnMore %>" target="_blank"><%= Resources.NCMWebContent.WEBDATA_IC_85 %></a></div>
                    </td>
                </tr>
            </table>
             
            <div class="SettingBlockHeader" style="padding-top:50px;"><%=Resources.NCMWebContent.WEBDATA_IC_94 %></div>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td nowrap="nowrap" valign="top" style="padding-top:5px;">
                        <b><%=Resources.NCMWebContent.WEBDATA_IC_87 %></b>
                    </td>
                    <td nowrap="nowrap" valign="top" style="padding-top:40px;padding-left:5px;" >
                         <%=string.Format(Resources.NCMWebContent.WEBDATA_IC_86, @"<a target='_blank' rel='noopener noreferrer' style='color:#336699;' text-decoration:underline; href='http://www.solarwinds.com/documentation/helpLoader.aspx?lang=en&topic=OrionNCMPHRTCDDeviceConfigLearnMore.htm'>", @"</a>") %>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:40px;" >
                        <b><%=Resources.NCMWebContent.WEBDATA_IC_95 %></b>
                    </td>
                    <td style="padding-top:40px;padding-left:5px;" >
                        <%=string.Format(Resources.NCMWebContent.WEBDATA_IC_96, OrionConfiguration.IsDemoServer ? $@"<a style='color:#336699' href=""javascript:demoAction('{DemoModeConstants.NCM_RTNSETTINGS_CONFIG_CHANGES}', this);"">" : @"<a style='color:#336699;' href='ConfigChangeSettings.aspx'>", @"</a>") %>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="padding-top:10px;padding-left:5px;">
                        <b>&bull;&nbsp;</b><%=Resources.NCMWebContent.WEBDATA_IC_100 %>
                    </td>
                </tr>

                <tr>
                    <td style="padding-top:40px;" >
                        <b><%=Resources.NCMWebContent.WEBDATA_IC_101 %></b>
                    </td>
                    <td style="padding-top:40px;padding-left:5px;" >
                        <%=string.Format(Resources.NCMWebContent.WEBDATA_IC_97, OrionConfiguration.IsDemoServer ? $@"<a style='color:#336699' href=""javascript:demoAction('{DemoModeConstants.NCM_RTNSETTINGS_CONFIG_DOWNLOADS_NOTS}', this);"">" :  @"<a style='color:#336699;' href='ConfigDownloadsNotifications.aspx'>", @"</a>") %>
                    </td>
                </tr>

                <tr>
                    <td></td>
                    <td style="padding-top:10px;padding-left:5px;">
                        <b>&bull;&nbsp;</b><%=Resources.NCMWebContent.WEBDATA_IC_103 %>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="padding-top:10px;padding-left:5px;">
                    <b>&bull;&nbsp;</b><%=Resources.NCMWebContent.WEBDATA_IC_104 %>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="padding-top:10px;padding-left:5px;">
                        <b>&bull;&nbsp;</b><%=Resources.NCMWebContent.WEBDATA_IC_105 %>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:40px;" >
                        <b><%=Resources.NCMWebContent.WEBDATA_IC_106 %></b>
                    </td>
                    <td style="padding-top:40px;padding-left:5px;">
                        <%=string.Format(Resources.NCMWebContent.WEBDATA_IC_107, OrionConfiguration.IsDemoServer ? $@"<a style='color:#336699' href=""javascript:demoAction('{DemoModeConstants.NCM_RTNSETTINGS_SMTP_SERVER}', this);"">" :  @"<a style='color:#336699;' href='SMTPServer.aspx'>", @"</a>") %>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:40px;" >
                        <b><%=Resources.NCMWebContent.WEBDATA_IC_110 %></b>
                    </td>
                    <td style="padding-top:40px;padding-left:5px;" >
                        <%=Resources.NCMWebContent.WEBDATA_IC_111 %>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="padding-top:20px;">
                                    <asp:RadioButton id="chkEnable" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_IC_112 %>" GroupName="RTN" />
                                </td>
                                <td style="padding-top:20px;padding-left:50px;">
                                    <asp:RadioButton id="chkDisable" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_IC_113 %>" GroupName="RTN" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding-top:20px;">
            <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" 
            OnClick="btnSubmit_Click" DisplayType="Primary"
            CommandArgument="~/Orion/NCM/Admin/Default.aspx"/> 
            <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" 
            OnClick="btnCancel_Click" DisplayType="Secondary"
            CommandArgument="~/Orion/NCM/Admin/Default.aspx" CausesValidation="false"/>
        </div>
    </asp:Panel>
</asp:Content>