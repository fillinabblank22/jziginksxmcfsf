﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.NCM.Common.Help;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCM.Web.Contracts.Search;

public partial class Orion_NCM_Admin_Settings_SearchSettings : Page
{
    private readonly ILogger log;
    private readonly IIsWrapper isLayer;
    private readonly ISearchHelper searchHelper;

    public Orion_NCM_Admin_Settings_SearchSettings()
    {
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
        searchHelper = ServiceLocator.Container.Resolve<ISearchHelper>();
        log = ServiceLocator.Container.Resolve<Func<Type, ILogger>>()(GetType());
    }

    protected string LearnMoreLink
    {
        get
        {
            var helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
            return helpHelperWrap.GetHelpUrl("OrionNCMPHDisableSearchPerformanceInfo");
        }
    }

    protected string FTSIndexCatalogPath
    {
        get { return Convert.ToString(ViewState["FTSIndexCatalogPath"]); }
        set { ViewState["FTSIndexCatalogPath"] = value; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ValidatePageAccess();
    }

    private void ValidatePageAccess()
    {
        if (CommonHelper.IsDemoMode() ||
            !(this.Page is IBypassAccessLimitation) && !Profile.AllowAdmin && !SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR))
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_VK_554}");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Title = Resources.NCMWebContent.WEBDATA_VK_724;

            var status = searchHelper.GetIndexingStatus();

            var numberFormatInfo = new System.Globalization.NumberFormatInfo();
            numberFormatInfo.NumberGroupSeparator = "";
            sDelayTime.NumberFormat = numberFormatInfo;

            if (!Page.IsPostBack)
            {
                using (var proxy = isLayer.GetProxy())
                {
                    rbEnableSearch.Checked = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.FTSIsEnabled, true, null));
                    rbDisableSearch.Checked = !rbEnableSearch.Checked;
                    sRefreshPeriod.Text = Convert.ToString(Convert.ToInt32(proxy.Cirrus.Settings.GetSetting(Settings.FTSConfigScanPeriod, 600000, null)) / 1000 / 60);
                    sDelayTime.Text = proxy.Cirrus.Settings.GetSetting(Settings.FTSIndexingDelay, 0, null);
                    FTSIndexCatalogPath = proxy.Cirrus.Settings.GetSetting(Settings.FTSIndexCatalogPath, string.Empty, null);
                    txtFTSIndexCatalogPath.Text = FTSIndexCatalogPath;
                }
                RefreshStatus(status);
            }
        }
        catch (Exception ex)
        {
            log.Error("Error loading the Search Settings page.", ex);
            throw;
        }
    }

    protected void btnApply_Click(object sender, EventArgs e)
    {
        string error;
        if (!ValidatePath(txtFTSIndexCatalogPath.Text, out error))
        {
            txtFTSIndexCatalogPath.CssClass = @"SettingTextFieldError";
            var literal = new Literal();
            literal.Text = string.IsNullOrEmpty(error) ? Resources.NCMWebContent.WEBDATA_VK_739 : error;
            validatorHolder.Style.Add(@"color", @"red");
            validatorHolder.Controls.Add(literal);
        }
        else
        {
            if (searchHelper.GetIndexingStatus() != IndexingStatus.Indexed)
            {
                txtFTSIndexCatalogPath.CssClass = @"SettingTextFieldError";
                var literal = new Literal();
                literal.Text = Resources.NCMWebContent.WEBDATA_VK_740;
                validatorHolder.Style.Add(@"color", @"red");
                validatorHolder.Controls.Add(literal);
            }
            else
            {
                txtFTSIndexCatalogPath.CssClass = @"SettingTextField";
                using (var proxy = isLayer.GetProxy())
                {
                    proxy.Cirrus.Settings.SaveSetting(Settings.FTSIsEnabled, rbEnableSearch.Checked, null);
                    proxy.Cirrus.Settings.SaveSetting(Settings.FTSIndexCatalogPath, txtFTSIndexCatalogPath.Text, null);
                }
                searchHelper.SetIndexingStatus(IndexingStatus.Waiting);
                Page.Response.Redirect(Request.Url.AbsoluteUri);
            }
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string error;
        if (!ValidatePath(txtFTSIndexCatalogPath.Text, out error))
        {
            txtFTSIndexCatalogPath.CssClass = @"SettingTextFieldError";
            var literal = new Literal();
            literal.Text = string.IsNullOrEmpty(error) ? Resources.NCMWebContent.WEBDATA_VK_739 : error;
            validatorHolder.Style.Add(@"color", @"red");
            validatorHolder.Controls.Add(literal);
            return;
        }

        if (searchHelper.GetIndexingStatus() != IndexingStatus.Indexed && !FTSIndexCatalogPath.Equals(txtFTSIndexCatalogPath.Text, StringComparison.InvariantCultureIgnoreCase))
        {
            txtFTSIndexCatalogPath.CssClass = @"SettingTextFieldError";
            var literal = new Literal();
            literal.Text = Resources.NCMWebContent.WEBDATA_VK_740;
            validatorHolder.Style.Add(@"color", @"red");
            validatorHolder.Controls.Add(literal);
            return;
        }
        txtFTSIndexCatalogPath.CssClass = @"SettingTextField";

        using (var proxy = isLayer.GetProxy())
        {
            proxy.Cirrus.Settings.SaveSetting(Settings.FTSIsEnabled, rbEnableSearch.Checked, null);
            proxy.Cirrus.Settings.SaveSetting(Settings.FTSConfigScanPeriod, Convert.ToInt32(sRefreshPeriod.Text) * 1000 * 60, null);
            proxy.Cirrus.Settings.SaveSetting(Settings.FTSIndexingDelay, sDelayTime.Text, null);

            if (!FTSIndexCatalogPath.Equals(txtFTSIndexCatalogPath.Text, StringComparison.InvariantCultureIgnoreCase))
            {
                if (searchHelper.GetIndexingStatus() == IndexingStatus.Indexed)
                {
                    proxy.Cirrus.Settings.SaveSetting(Settings.FTSIndexCatalogPath, txtFTSIndexCatalogPath.Text, null);
                    searchHelper.SetIndexingStatus(IndexingStatus.Waiting);
                }
            }
        }

        var btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        var btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }

    private void RefreshStatus(IndexingStatus status)
    {
        switch (status)
        {
            case IndexingStatus.Waiting:
            case IndexingStatus.NotIndexed:
                lblIndexStatus.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_722, 0);
                lblConfigsCount.Text = @"0";
                break;
            case IndexingStatus.InProgress:
                lblIndexStatus.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_722, searchHelper.GetPercentageComplete());
                lblConfigsCount.Text = searchHelper.GetIndexedConfigs().ToString();
                break;
            case IndexingStatus.Indexed:
                lblIndexStatus.Text = Resources.NCMWebContent.WEBDATA_VK_723;
                lblConfigsCount.Text = searchHelper.GetIndexedConfigs().ToString();
                break;
        }
    }

    private bool ValidatePath(string path, out string error)
    {
        try
        {
            using (var proxy = isLayer.GetProxy())
            {
                error = string.Empty;
                var primaryEngineId = GetPrimaryEngineID();
                return proxy.Cirrus.Settings.ValidatePath(path, primaryEngineId);
            }
        }
        catch (Exception ex)
        {
            error = ex.Message;
            log.Error("Error validating path", ex);
            return false;
        }
    }

    private int GetPrimaryEngineID()
    {
        var primaryEngineId = -1;
        if (ViewState["PrimaryEngineID"] == null)
        {
            using (var proxy = isLayer.GetProxy())
            {
                var dt = proxy.Query(@"SELECT TOP 1 E.EngineID FROM Orion.Engines AS E WHERE E.ServerType='Primary' ORDER BY E.KeepAlive DESC");
                if (dt != null && dt.Rows.Count > 0)
                {
                    primaryEngineId = Convert.ToInt32(dt.Rows[0][0]);
                    ViewState["PrimaryEngineID"] = primaryEngineId;
                }
            }
        }
        else
        {
            primaryEngineId = Convert.ToInt32(ViewState["PrimaryEngineID"]);
        }
        return primaryEngineId;
    }
}