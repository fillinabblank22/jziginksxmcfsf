﻿Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.QuickTips.init();

SW.NCM.ComparisonCriteriaViewer = function () {
    function gridResize() {
        if (grid != null) {
            grid.setWidth(1);
            grid.setWidth($('#gridCell').width());

            grid.setHeight(getGridHeight());
            grid.doLayout();
        }
    }

    function getGridHeight() {
        var top = $('#gridCell').offset().top;
        return $(window).height() - top - $('#footer').height() - 125;
    }

    var currentSearchTerm = null;
    var refreshObjects = function (search, callback) {
        grid.store.removeAll();
        var pagingToolbar = grid.getBottomToolbar();
        pagingToolbar.cursor = 0;
        grid.store.baseParams['limit'] = grid.getBottomToolbar().pageSize;
        grid.store.proxy.conn.jsonData = { search: search };
        currentSearchTerm = search;
        pagingToolbar.doLoad(pagingToolbar.cursor);
    };

    var doClean = function () {
        var map = grid.getTopToolbar().items.map;
        map.Clean.setVisible(false);
        map.txtSearch.setValue('');
        refreshObjects("");
    }

    var doSearch = function () {

        var map = grid.getTopToolbar().items.map;
        var search = map.txtSearch.getValue();
        if ($.trim(search).length == 0) return;
        refreshObjects(search);
        map.Clean.setVisible(true);
    }

    var getSelectedRegExPatternIDs = function (items) {
        var ids = [];

        Ext.each(items, function (item) {
            ids.push(item.data.RegExID);
        });

        return ids;
    };

    var getEnabledRegExPatternIDs = function (items) {
        var ids = [];

        Ext.each(items, function (item) {
            if (item.data.Enabled)
                ids.push(item.data.RegExID);
        });

        return ids;
    };

    var getDisabledRegExPatternIDs = function (items) {
        var ids = [];

        Ext.each(items, function (item) {
            if (!item.data.Enabled)
                ids.push(item.data.RegExID);
        });

        return ids;
    };

    var AddRegExPattern = function () {
        showRegExPatternDialog('', '@{R=NCM.Strings;K=WEBJS_VK_118;E=js}');
    };

    var EditRegExPattern = function (item) {
        showRegExPatternDialog(item.data.RegExID, '@{R=NCM.Strings;K=WEBJS_VK_119;E=js}');
    };

    var SaveRegExPattern = function (regExID, title, enabled, regExPattern, comment, isBlock, blockEndRegEx) {
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_120;E=js}');

        saveRegExPattern(regExID, title, enabled, regExPattern, comment, isBlock, blockEndRegEx, function (result) {
            waitMsg.hide();
            ErrorHandler(result);
            ReloadGridStore();
        });
    };

    var EnableRegExPatterns = function (items) {
        var toEnable = getDisabledRegExPatternIDs(items);
        if (toEnable.length > 0) {
            var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_138;E=js}');

            enableOrDisableRegExPatterns(toEnable, true, function (result) {
                waitMsg.hide();
                ErrorHandler(result);
                ReloadGridStore();
            });
        }
    };

    var DisableRegExPatterns = function (items) {
        var toDisable = getEnabledRegExPatternIDs(items);
        if (toDisable.length > 0) {
            var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_139;E=js}');

            enableOrDisableRegExPatterns(toDisable, false, function (result) {
                waitMsg.hide();
                ErrorHandler(result);
                ReloadGridStore();
            });
        }
    };

    var DeleteRegExPatterns = function (items) {
        var toDelete = getSelectedRegExPatternIDs(items);
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_121;E=js}');

        deleteRegExPatterns(toDelete, function (result) {
            waitMsg.hide();
            ErrorHandler(result);
            ReloadGridStore();
        });
    };

    var enableOrDisableRegExPatterns = function (ids, enableOrDisable, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/ComparisonCriteria.asmx",
                             "EnableOrDisableRegExPatterns", { regExIDs: ids, enableOrDisable: enableOrDisable },
                             function (result) {
                                 onSuccess(result);
                             });
    };

    var deleteRegExPatterns = function (ids, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/ComparisonCriteria.asmx",
                             "DeleteRegExPatterns", { regExIDs: ids },
                             function (result) {
                                 onSuccess(result);
                             });
    };

    function validateSaveRegExPattern(regExPattern, isBlock, blockEndRegEx) {
        if (regExPattern == null || regExPattern.trim() === "") {
            showWarningMessage('@{R=NCM.Strings;K=WEBJS_BZ_01;E=js}', '@{R=NCM.Strings;K=WEBJS_BZ_02;E=js}');
            return false;
        }
        if (isBlock === true && (blockEndRegEx == null || blockEndRegEx.trim() === "")) {
            showWarningMessage('@{R=NCM.Strings;K=WEBJS_BZ_01;E=js}', '@{R=NCM.Strings;K=WEBJS_BZ_03;E=js}');
            return false;
        }

        return true;
    }

    var saveRegExPattern = function (regExID, title, enabled, regExPattern, comment, isBlock, blockEndRegEx, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/ComparisonCriteria.asmx",
                             "SaveRegExPattern", { regExID: regExID, title: title, enabled: enabled, regExPattern: regExPattern, comment: comment, isBlock: isBlock, blockEndRegEx: blockEndRegEx },
                             function (result) {
                                 onSuccess(result);
                             });
    };

    function ReloadGridStore() {
        var currentPageRecordCount = grid.store.getCount();
        var selectedRecordCount = grid.getSelectionModel().getCount();
        if (currentPageRecordCount > selectedRecordCount) {
            grid.store.reload();
        }
        else {
            grid.store.load();
        }
    }

    var updateToolbarButtons = function () {
        var selCount = grid.getSelectionModel().getCount();
        var map = grid.getTopToolbar().items.map;

        var needsOnlyOneSelected = (selCount != 1);
        var needsAtLeastOneSelected = (selCount === 0);

        map.Edit.setDisabled(needsOnlyOneSelected);
        map.Enable.setDisabled(needsAtLeastOneSelected);
        map.Disable.setDisabled(needsAtLeastOneSelected);
        map.Delete.setDisabled(needsAtLeastOneSelected);

        var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');
        if (grid.getStore().getCount() > 0 && selCount == grid.getStore().getCount()) {
            hd.addClass('x-grid3-hd-checker-on');
        } else {
            hd.removeClass('x-grid3-hd-checker-on');
        }
    };

    function showWarningMessage(title, message) {
        Ext.Msg.show({
            title: title,
            msg: message,
            minWidth: 500,
            buttons: Ext.Msg.OK,
            icon: Ext.MessageBox.WARNING
        });
    }

    function ErrorHandler(result) {
        if (result != null && result.error) {
            Ext.Msg.show({
                title: result.source,
                msg: result.message,
                minWidth: 500,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        }
    }

    function renderEnabled(value, meta, record) {
        return value ? '@{R=NCM.Strings;K=WEBJS_VK_57;E=js}' : '@{R=NCM.Strings;K=WEBJS_VK_58;E=js}';
    }

    function renderTitle(value, meta, record) {
        value = value || '';
        if (value.length === 0) {
            return '@{R=NCM.Strings;K=WEBJS_VK_123;E=js}';
        }
        return Ext.util.Format.htmlEncode(value);
    }

    function renderIsBlock(value, meta, record) {
        return value ? '@{R=NCM.Strings;K=WEBJS_VK_57;E=js}' : '@{R=NCM.Strings;K=WEBJS_VK_58;E=js}';
    }

    function renderBlockEndRegEx(value, meta, record) {
        value = value || '';
        if (value.length === 0) {
            return  '@{R=NCM.Strings;K=WEBJS_DP_04;E=js}';
        }
        return Ext.util.Format.htmlEncode(value);
    }

    function renderRegExPattern(value, meta, record) {
        value = value || '';
        if (value.length === 0) {
            return '@{R=NCM.Strings;K=WEBJS_VK_124;E=js}';
        }
        return Ext.util.Format.htmlEncode(value);
    }

    function renderComment(value, meta, record) {
        value = value || '';
        if (value.length === 0) {
            return '@{R=NCM.Strings;K=WEBJS_VK_125;E=js}';
        }
        return Ext.util.Format.htmlEncode(value);
    }

    function showRegExPatternDialog(regExID, dlgTitle) {

        var divRegExID = $("#divRegExID");
        divRegExID.empty();

        var divEnabled = $("#divEnabled");
        divEnabled.empty();

        var divTitle = $("#divTitle");
        divTitle.empty();

        var divRegExPattern = $("#divRegExPattern");
        divRegExPattern.empty();

        document.getElementById("isBlockCheckbox").checked = false;

        var divBlockEndRegEx = $("#divBlockEndRegEx");
        divBlockEndRegEx.empty();

        var divComment = $("#divComment");
        divComment.empty();

        ORION.callWebService("/Orion/NCM/Services/ComparisonCriteria.asmx",
                             "GetRegExPattern", { regExID: regExID },
                             function (result) {

                                 var regExID = new Ext.form.TextField({
                                     id: 'regExID',
                                     renderTo: 'divRegExID',
                                     value: result.regExID,
                                     hidden: true
                                 });

                                 var enabled = new Ext.form.Checkbox({
                                     id: 'enabled',
                                     renderTo: 'divEnabled',
                                     boxLabel: '@{R=NCM.Strings;K=WEBJS_VK_122;E=js}',
                                     checked: result.enabled
                                 });

                                 var title = new Ext.form.TextField({
                                     id: 'title',
                                     renderTo: 'divTitle',
                                     emptyText: '@{R=NCM.Strings;K=WEBJS_VK_123;E=js}',
                                     value: result.title,
                                     width: 545
                                 });

                                 var regExPattern = new Ext.form.TextField({
                                     id: 'regExPattern',
                                     renderTo: 'divRegExPattern',
                                     emptyText: '@{R=NCM.Strings;K=WEBJS_VK_124;E=js}',
                                     value: result.regExPattern,
                                     width: 545
                                 });

                                 document.getElementById('isBlockCheckbox').checked = result.isBlock;

                                 var blockEndRegEx = new Ext.form.TextField({
                                     id: 'blockEndRegEx',
                                     renderTo: 'divBlockEndRegEx',
                                     emptyText: '@{R=NCM.Strings;K=WEBJS_DP_04;E=js}',
                                     value: result.blockEndRegEx,
                                     width: 545
                                 });

                                 var comment = new Ext.form.TextArea({
                                     id: 'comment',
                                     renderTo: 'divComment',
                                     emptyText: '@{R=NCM.Strings;K=WEBJS_VK_125;E=js}',
                                     value: result.comment,
                                     width: 545,
                                     height: 100
                                 });

                                 return ShowRegExPatternDialog(dlgTitle);
                             });
    }

    toggleIsBlock = function () {
        var checkbox = document.getElementById("isBlockCheckbox");
        var label = document.getElementById("blockEndRegExLabel");
        var textBox = document.getElementById("blockEndRegExTextBox");

        if (checkbox.checked == true) {
            label.style.display = "block";
            textBox.style.display = "block";
        } else {
            label.style.display = "none";
            textBox.style.display = "none";
        }
    }

    var showDialog;
    ShowRegExPatternDialog = function (dlgTitle) {

        if (!showDialog) {
            showDialog = new Ext.Window({
                applyTo: 'regExPatternDialog',
                layout: 'fit',
                width: 600,
                height: 500,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                modal: true,
                contentEl: 'regExPatternDialogBody',
                buttons: [
                            {
                                text: '@{R=NCM.Strings;K=WEBJS_VK_111;E=js}',
                                handler: function () {
                                    var regExID = Ext.getCmp('regExID').getValue();
                                    var title = Ext.getCmp('title').getValue();
                                    var enabled = Ext.getCmp('enabled').getValue();
                                    var regExPattern = Ext.getCmp('regExPattern').getValue();
                                    var isBlock = document.getElementById('isBlockCheckbox').checked;
                                    var blockEndRegEx = '';
                                    if (isBlock) {
                                        blockEndRegEx = Ext.getCmp('blockEndRegEx').getValue();
                                    }
                                    var comment = Ext.getCmp('comment').getValue();

                                    if (validateSaveRegExPattern(regExPattern, isBlock, blockEndRegEx)) {
                                        SaveRegExPattern(regExID, title, enabled, regExPattern, comment, isBlock, blockEndRegEx);
                                        showDialog.hide();
                                    }
                                }
                            }, {
                                text: '@{R=NCM.Strings;K=WEBJS_VK_38;E=js}',
                                handler: function () {
                                    showDialog.hide();
                                }
                            }
                ]
            });
        }

        showDialog.setTitle(Ext.util.Format.htmlEncode(dlgTitle));
        showDialog.alignTo(document.body, "c-c");
        showDialog.show();
        toggleIsBlock();

        return false;
    }

    var selectorModel;
    var dataStore;
    var grid;

    return {
        init: function () {
            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", updateToolbarButtons);

            dataStore = new ORION.WebServiceStore(
                "/Orion/NCM/Services/ComparisonCriteria.asmx/GetRegExPatternsPaged",
                [
                    { name: 'RegExID', mapping: 0 },
                    { name: 'Title', mapping: 1 },
                    { name: 'Enabled', mapping: 2 },
                    { name: 'RegEx', mapping: 3 },
                    { name: 'IsBlock', mapping: 4 },
                    { name: 'BlockEndRegEx', mapping: 5 },
                    { name: 'Comment', mapping: 6 }
                ],
                "Title");

            var comboPS = new Ext.form.ComboBox({
                name: 'perpage',
                width: 50,
                store: new Ext.data.SimpleStore({
                    fields: ['id'],
                    data: [
                      ['25'],
                      ['50'],
                      ['75'],
                      ['100']
                    ]
                }),
                mode: 'local',
                value: '25',
                listWidth: 50,
                triggerAction: 'all',
                displayField: 'id',
                valueField: 'id',
                editable: false,
                forceSelection: true
            });

            var pagingToolbar = new Ext.PagingToolbar({
                store: dataStore,
                pageSize: 25,
                displayInfo: true,
                displayMsg: '@{R=NCM.Strings;K=WEBJS_VK_126;E=js}',
                emptyMsg: '@{R=NCM.Strings;K=WEBJS_VK_127;E=js}',
                items: ['-', '@{R=NCM.Strings;K=WEBJS_VK_12;E=js}', comboPS]
            });

            comboPS.on('select', function (combo, record) {
                var psize = parseInt(record.get('id'), 10);
                grid.store.baseParams['limit'] = psize;
                pagingToolbar.pageSize = psize;
                pagingToolbar.cursor = 0;
                pagingToolbar.doLoad(pagingToolbar.cursor);
            }, this);


            grid = new Ext.grid.GridPanel({
                id: 'gridControl',
                store: dataStore,

                columns: [selectorModel, {
                        header: 'RegExID',
                        width: 80,
                        hidden: true,
                        hideable: false,
                        sortable: true,
                        dataIndex: 'RegExID'
                    }, {
                        header: '@{R=NCM.Strings;K=WEBJS_VK_75;E=js}',
                        width: 300,
                        sortable: true,
                        dataIndex: 'Title',
                        renderer: renderTitle
                    }, {
                        header: '@{R=NCM.Strings;K=WEBJS_VK_128;E=js}',
                        width: 100,
                        sortable: true,
                        dataIndex: 'Enabled',
                        renderer: renderEnabled
                    }, {
                        header: '@{R=NCM.Strings;K=WEBJS_VK_129;E=js}',
                        width: 300,
                        sortable: false,
                        dataIndex: 'RegEx',
                        renderer: renderRegExPattern
                    }, {
                        header: '@{R=NCM.Strings;K=ComparisonCriteria_IsBlock_ColumnHeader;E=js}',
                        width: 100,
                        sortable: true,
                        dataIndex: 'IsBlock',
                        renderer: renderIsBlock
                    }, {
                        header: '@{R=NCM.Strings;K=ComparisonCriteria_BlockEndRegEx_ColumnHeader;E=js}',
                        width: 300,
                        sortable: false,
                        dataIndex: 'BlockEndRegEx',
                        renderer: renderBlockEndRegEx
                    }, {
                        header: '@{R=NCM.Strings;K=WEBJS_VK_130;E=js}',
                        width: 300,
                        sortable: false,
                        dataIndex: 'Comment',
                        renderer: renderComment
                    }
                ],
                sm: selectorModel,

                layout: 'fit',
                autoScroll: 'true',
                loadMask: true,

                width: 750,
                height: 500,
                stripeRows: true,

                tbar: [{
                    id: 'AddNew',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_131;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_132;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_addsnippet.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        AddRegExPattern();
                    }
                }, '-', {
                    id: 'Edit',
                    text: '@{R=NCM.Strings;K=WEBJS_VY_03;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_133;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_editsnippet.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        EditRegExPattern(grid.getSelectionModel().getSelected());
                    }
                }, '-', {
                    id: 'Enable',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_140;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_141;E=js}',
                    icon: '/Orion/NCM/Resources/images/icon_ok.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        EnableRegExPatterns(grid.getSelectionModel().getSelections());
                    }
                }, '-', {
                    id: 'Disable',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_142;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_143;E=js}',
                    icon: '/Orion/NCM/Resources/images/disable_icon.png',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        DisableRegExPatterns(grid.getSelectionModel().getSelections());
                    }
                }, '-', {
                    id: 'Delete',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_25;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_134;E=js}',
                    icon: '/Orion/NCM/Resources/images/icon_delete.png',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        Ext.Msg.confirm('@{R=NCM.Strings;K=WEBJS_VK_135;E=js}',
                        '@{R=NCM.Strings;K=WEBJS_VK_136;E=js}',
                        function (btn, text) {
                            if (btn == 'yes') {
                                DeleteRegExPatterns(grid.getSelectionModel().getSelections());
                            }
                        });
                    }
                }, '->', {
                    id: 'txtSearch',
                    xtype: 'textfield',
                    emptyText: '@{R=NCM.Strings;K=WEBJS_VK_137;E=js}',
                    width: 250,
                    enableKeyEvents: true,
                    listeners: {
                        keypress: function (obj, evnt) {
                            if (evnt.keyCode == 13) {
                                doSearch();
                            }
                        }
                    }
                }, {
                    id: 'Clean',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VY_08;E=js}',
                    iconCls: 'ncm_clear-btn',
                    cls: 'x-btn-icon',
                    hidden: true,
                    handler: function () { doClean(); }
                }, {
                    id: 'Search',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VY_09;E=js}',
                    iconCls: 'ncm_search-btn',
                    cls: 'x-btn-icon',
                    handler: function () { doSearch(); }
                }],

                bbar: pagingToolbar

            });

            grid.render('Grid');

            updateToolbarButtons();

            gridResize();

            $("form").submit(function () { return false; });

            refreshObjects("");

            $(window).resize(function () {
                gridResize();
            });

            grid.getView().on("refresh", function () {
                var search = currentSearchTerm;
                if ($.trim(search).length == 0) return;

                var columnsToHighlight = SW.NCM.ColumnIndexByName(grid.getColumnModel(), ['@{R=NCM.Strings;K=WEBJS_VK_75;E=js}', '@{R=NCM.Strings;K=WEBJS_VK_129;E=js}', '@{R=NCM.Strings;K=WEBJS_VK_130;E=js}']);
                Ext.each(columnsToHighlight, function (columnNumber) {
                    SW.NCM.highlightSearchText("#Grid .x-grid3-body .x-grid3-td-" + columnNumber, search);
                });
            });
        }
    };
}();

Ext.onReady(SW.NCM.ComparisonCriteriaViewer.init, SW.NCM.ComparisonCriteriaViewer);







