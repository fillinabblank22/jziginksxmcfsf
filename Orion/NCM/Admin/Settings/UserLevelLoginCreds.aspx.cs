using System;
using System.Web;
using System.Web.UI.WebControls;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.InformationService.Contract2;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCM.Contracts;

public partial class Orion_NCM_Admin_Settings_UserLevelLoginCreds : System.Web.UI.Page
{
    private readonly IIsWrapper isLayer;
    private readonly IDataDecryptor dataDecryptor;

    public Orion_NCM_Admin_Settings_UserLevelLoginCreds()
    {
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
        dataDecryptor = ServiceLocator.Container.Resolve<IDataDecryptor>();
    }

    protected bool HideUsernames
    {
        get
        {
            if (ViewState["HideUsernames"] == null)
            {
                using (var proxy = isLayer.GetProxy())
                {
                    var hideUsernames = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.HideUsernames, false, null));
                    ViewState["HideUsernames"] = hideUsernames;
                    return hideUsernames;
                }
            }
            else
            {
                return Convert.ToBoolean(ViewState["HideUsernames"]);
            }
        }
    }

    protected bool UseUserLevelLoginCreds
    {
        get
        {
            if (ViewState["UseUserLevelLoginCreds"] == null)
            {
                using (var proxy = isLayer.GetProxy())
                {
                    var connLevel = proxy.Cirrus.Settings.GetSetting(Settings.ConnectivityLevel, ConnectivityLevels.Default, null);
                    var useUserLevelLoginCreds = connLevel.Equals(ConnectivityLevels.Level3);
                    ViewState["UseUserLevelLoginCreds"] = useUserLevelLoginCreds;
                    return useUserLevelLoginCreds;
                }
            }
            else
            {
                return Convert.ToBoolean(ViewState["UseUserLevelLoginCreds"]);
            }
        }
    }

    protected string ReturnUrl
    {
        set
        {
            if (ViewState["ReturnUrl"] == null)
            {
                ViewState["ReturnUrl"] = value;
            }
        }
        get { return Convert.ToString(ViewState["ReturnUrl"]); }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ValidatePageAccess();
    }

    private void ValidatePageAccess()
    {
        if (CommonHelper.IsDemoMode() || !SecurityHelper.IsPermissionExist(SecurityHelper._canDownload))
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_VK_712}");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_VK_711;
        
        if (Page.Request.UrlReferrer != null)
            ReturnUrl = Page.Request.UrlReferrer.ToString();
        else
            ReturnUrl = @"~/Orion/NCM/Admin/Default.aspx";

        if (HideUsernames)
        {
            txtUserName.TextMode = TextBoxMode.Password;
        }

        txtUserName.Enabled = UseUserLevelLoginCreds;
        txtUserName.CssClass = UseUserLevelLoginCreds ? @"SettingTextField" : @"TextFieldDisabled";
        txtPassword.Enabled = UseUserLevelLoginCreds;
        txtPassword.CssClass = UseUserLevelLoginCreds ? @"SettingTextField" : @"TextFieldDisabled";
        txtConfirmPassword.Enabled = UseUserLevelLoginCreds;
        txtConfirmPassword.CssClass = UseUserLevelLoginCreds ? @"SettingTextField" : @"TextFieldDisabled";
        ddlEnableLevel.Enabled = UseUserLevelLoginCreds;
        ddlEnableLevel.CssClass = UseUserLevelLoginCreds ? @"SettingSelectField" : @"TextFieldDisabled";
        txtEnablePassword.Enabled = UseUserLevelLoginCreds;
        txtEnablePassword.CssClass = UseUserLevelLoginCreds ? @"SettingTextField" : @"TextFieldDisabled";

        if (!Page.IsPostBack)
        {
            compareValidator.Attributes[@"ControlToCompareId"] = txtPassword.ClientID;

            using (var proxy = isLayer.GetProxy())
            {
                var swql = @"SELECT C.UserLevelName, C.UserLevelPassword, C.UserLevelEnableLevel, C.UserLevelEnablePassword 
                    FROM Cirrus.UserLevelLoginCredentials AS C 
                    WHERE C.AccountID=@userName";

                var dt = proxy.Query(swql, new PropertyBag() {{@"userName", HttpContext.Current.Profile.UserName}});

                txtUserName.PasswordText = dt.Rows.Count > 0 ? dataDecryptor.Decrypt(dt.Rows[0]["UserLevelName"].ToString()) : string.Empty;
                txtPassword.PasswordText = dt.Rows.Count > 0 ? dataDecryptor.Decrypt(dt.Rows[0]["UserLevelPassword"].ToString()) : string.Empty;
                txtConfirmPassword.PasswordText = dt.Rows.Count > 0 ? dataDecryptor.Decrypt(dt.Rows[0]["UserLevelPassword"].ToString()) : string.Empty;
                ddlEnableLevel.SelectedValue = dt.Rows.Count > 0 ? dt.Rows[0]["UserLevelEnableLevel"].ToString() : string.Empty;
                txtEnablePassword.PasswordText = dt.Rows.Count > 0 ? dataDecryptor.Decrypt(dt.Rows[0]["UserLevelEnablePassword"].ToString()) : string.Empty;
            }
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (UseUserLevelLoginCreds)
        {
            using (var proxy = isLayer.GetProxy())
            {
                var userName = txtUserName.PasswordText;
                var password = txtPassword.PasswordText;
                var enableLevel = ddlEnableLevel.SelectedValue;
                var enablePassword = txtEnablePassword.PasswordText;

                proxy.Cirrus.Settings.SaveUserLevelLoginCreds(HttpContext.Current.Profile.UserName, userName, password, enableLevel, enablePassword);
            }
        }

        Page.Response.Redirect(ReturnUrl);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect(ReturnUrl);
    }
}
