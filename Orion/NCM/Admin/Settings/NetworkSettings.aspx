<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true"
    CodeFile="NetworkSettings.aspx.cs" Inherits="Orion_NCM_Admin_Settings_NetworkSettings"
    EnableViewState="true" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/Controls/Slider.ascx" TagPrefix="orion" TagName="Slider" %>
<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" runat="Server">
    <orion:Include ID="Include1" runat="server" File="Admin.css" />
    <orion:Include ID="Include2" runat="server" File="Discovery.css" />
    <script type="text/javascript">
        $(document).ready(function () {
            var icmpData = document.getElementById("<%=txtICMPData.ClientID %>").value;
            ICMPDataChange(icmpData.length);
        });

        function ICMPDataChange(bytes) {
            $('#icmpDataHolder').html(bytes + '&nbsp;' + '<%=Resources.NCMWebContent.WEBDATA_VK_646 %>');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" runat="Server">
    <link rel="stylesheet" type="text/css" href="Settings.css" />
    <style type="text/css">
        #ICMPTimeoutSlider tbody tr td
        {
            padding: 10px;
            background-color: #e4f1f8;
        }
        
        #SNMPAdvSettingsHolder tbody tr td
        {
            padding: 10px;
            background-color: #e4f1f8;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMPHNetworkSettings" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" runat="Server">
    <%=Page.Title%>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <asp:Panel ID="ContentContainer" CssClass="ContentContainer" runat="server" EnableViewState="true">
        <div class="SettingContainer">
            <div class="SettingBlockHeader">
                <%=Resources.NCMWebContent.WEBDATA_VK_625 %></div>
            <div class="SettingBlockDescription">
                <%=Resources.NCMWebContent.WEBDATA_VK_626 %></div>
            <div style="padding-top: 20px;">
                <table id="ICMPTimeoutSlider" cellpadding="0" cellspacing="0">
                    <tr>
                        <orion:Slider runat="server" ID="slICMPTimeout" TableCellCount="3" Label="" RangeFrom="100"
                            RangeTo="5000" Unit="<%$ Resources: NCMWebContent, WEBDATA_VK_627 %>" ValidateRange="true"
                            Step="1" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_628 %>" />
                        <td>
                            <span class="SettingNote">
                                <%=string.Format(Resources.NCMWebContent.WEBDATA_VK_697, 2500) %></span>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="padding-top: 20px;">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td nowrap="nowrap" style="text-align: left; padding-bottom: 5px;">
                            <%=Resources.NCMWebContent.WEBDATA_VK_629 %>&nbsp;<span id="icmpDataHolder"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txtICMPData" runat="server" TextMode="MultiLine" CssClass="SettingTextField"
                                Width="400px" Height="50px" onchange="ICMPDataChange(this.value.length);" />
                        </td>
                    </tr>
                </table>
            </div>
            <div class="SettingBlockHeader" style="padding-top: 50px;">
                <%=Resources.NCMWebContent.WEBDATA_VK_630 %></div>
            <div class="SettingBlockDescription">
                <%=Resources.NCMWebContent.WEBDATA_VK_631 %></div>
            <div style="padding-top: 20px;">
                <table id="SNMPAdvSettingsHolder" cellpadding="0" cellspacing="0">
                    <tr>
                        <orion:Slider runat="server" ID="slSNMPTimeout" TableCellCount="3" Label="" RangeFrom="100"
                            RangeTo="5000" Unit="<%$ Resources: NCMWebContent, WEBDATA_VK_627 %>" ValidateRange="true"
                            Step="1" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_628 %>" />
                        <td>
                            <span class="SettingNote">
                                <%=string.Format(Resources.NCMWebContent.WEBDATA_VK_697, 1000) %></span>
                        </td>
                    </tr>
                    <tr>
                        <orion:Slider runat="server" ID="slSNMPRetries" TableCellCount="3" Label="" RangeFrom="1"
                            RangeTo="5" Unit="<%$ Resources: NCMWebContent, WEBDATA_VK_632 %>" ValidateRange="true"
                            Step="1" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_633 %>" />
                        <td>
                            <span class="SettingNote">
                                <%=string.Format(Resources.NCMWebContent.WEBDATA_VK_698, 1) %></span>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div style="padding-top: 20px;">
            <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" OnClick="btnSubmit_Click"
                DisplayType="Primary" CommandArgument="~/Orion/NCM/Admin/Default.aspx" CausesValidation="true" />
            <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" OnClick="btnCancel_Click"
                DisplayType="Secondary" CommandArgument="~/Orion/NCM/Admin/Default.aspx" CausesValidation="false" />
        </div>
    </asp:Panel>
</asp:Content>
