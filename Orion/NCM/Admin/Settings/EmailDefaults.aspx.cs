using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_Admin_Settings_EmailDefaults : System.Web.UI.Page
{
    private ISWrapper _isLayer;

    private void LoadNCMSettings()
    {
        if (!Page.IsPostBack)
        {
            _isLayer = new ISWrapper();
            using (var proxy = _isLayer.Proxy)
            {
                txtSenderName.Text = proxy.Cirrus.Settings.GetSetting(SolarWinds.NCM.Contracts.InformationService.Settings.DefaultEmailFromName, Resources.NCMWebContent.Settings_DefaultEmailSenderName, null);
                txtReplyAddress.Text = proxy.Cirrus.Settings.GetSetting(SolarWinds.NCM.Contracts.InformationService.Settings.DefaultEmailReplyAddress, @"SolarWinds@yourdomain.name", null);
                txtSubject.Text = proxy.Cirrus.Settings.GetSetting(SolarWinds.NCM.Contracts.InformationService.Settings.DefaultEmailSubject, Resources.NCMWebContent.Settings_DefaultEmailSubject, null);
                txtTo.Text = proxy.Cirrus.Settings.GetSetting(SolarWinds.NCM.Contracts.InformationService.Settings.DefaultEmailTo, string.Empty, null);
                txtCC.Text = proxy.Cirrus.Settings.GetSetting(SolarWinds.NCM.Contracts.InformationService.Settings.DefaultEmailCC, string.Empty, null);
                txtBCC.Text = proxy.Cirrus.Settings.GetSetting(SolarWinds.NCM.Contracts.InformationService.Settings.DefaultEmailBCC, string.Empty, null);
            }
            if (!string.IsNullOrEmpty(txtCC.Text))
            {
                CCPanel.Style.Add(@"display", @"block");
                btnAddCC.Visible = false;
            }

            if (!string.IsNullOrEmpty(txtBCC.Text))
            {
                BCCPanel.Style.Add(@"display", @"block");
                btnAddBCC.Visible = false;
            }
        }
    }

    private void SaveNCMSettings()
    {
         _isLayer = new ISWrapper();
         using (var proxy = _isLayer.Proxy)
         {
             proxy.Cirrus.Settings.SaveSetting(SolarWinds.NCM.Contracts.InformationService.Settings.DefaultEmailFromName, txtSenderName.Text, null);
             proxy.Cirrus.Settings.SaveSetting(SolarWinds.NCM.Contracts.InformationService.Settings.DefaultEmailReplyAddress, txtReplyAddress.Text, null);
             proxy.Cirrus.Settings.SaveSetting(SolarWinds.NCM.Contracts.InformationService.Settings.DefaultEmailSubject, txtSubject.Text, null);
             proxy.Cirrus.Settings.SaveSetting(SolarWinds.NCM.Contracts.InformationService.Settings.DefaultEmailTo, txtTo.Text, null);
             proxy.Cirrus.Settings.SaveSetting(SolarWinds.NCM.Contracts.InformationService.Settings.DefaultEmailCC, txtCC.Text, null);
             proxy.Cirrus.Settings.SaveSetting(SolarWinds.NCM.Contracts.InformationService.Settings.DefaultEmailBCC, txtBCC.Text, null);
         }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ValidatePageAccess();
    }

    private void ValidatePageAccess()
    {
        if (CommonHelper.IsDemoMode() ||
            !(this.Page is IBypassAccessLimitation) && !Profile.AllowAdmin && !SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR))
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_VK_554}");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_IC_69;
        LoadNCMSettings();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        SaveNCMSettings();
        var btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        var btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }

    protected void btnAddCC_Click(object sender, EventArgs e)
    {
        CCPanel.Style.Add(@"display", @"block");
        btnAddCC.Visible = false;
    }

    protected void btnAddBCC_Click(object sender, EventArgs e)
    {
        BCCPanel.Style.Add(@"display", @"block");
        btnAddBCC.Visible = false;
    }
}
