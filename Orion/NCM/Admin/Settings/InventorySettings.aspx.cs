using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;

public partial class Orion_NCM_Admin_Settings_InventorySettings : System.Web.UI.Page
{
    private ISWrapper isLayer;

    protected string InventorySpecList
    {
        get { return Convert.ToString(ViewState["InventorySpecList"]); }
        set { ViewState["InventorySpecList"] = value; }
    }

    protected int ConcurrentNodes
    {
        get { return Convert.ToInt16(ViewState["ConcurrentNodes"]); }
        set { ViewState["ConcurrentNodes"] = value; }
    }

    protected bool ExtendedVLANsInventory
    {
        get { return Convert.ToBoolean(ViewState["ExtendedVLANsInventory"]); }
        set { ViewState["ExtendedVLANsInventory"] = value; }
    }

    protected bool DisableInventoryLookup
    {
        get { return Convert.ToBoolean(ViewState["DisableInventoryLookup"]); }
        set { ViewState["DisableInventoryLookup"] = value; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ValidatePageAccess();
    }

    private void ValidatePageAccess()
    {
        if (CommonHelper.IsDemoMode() ||
            !(this.Page is IBypassAccessLimitation) && !Profile.AllowAdmin && !SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR))
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_VK_554}");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_VK_508;

        isLayer = new ISWrapper();

        chkExtendedVLANsInventory.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_552, @"<span class='SettingNote'>", @"</span>");

        if (!Page.IsPostBack)
        {
            using (var proxy = isLayer.Proxy)
            {
                InventorySpecList = proxy.Cirrus.Settings.GetSetting(Settings.IncludeInInventory, @"Standard-ARP,Standard-Bridge Table,Cisco-Cisco Chassis,Cisco-Cisco,Cisco-Cisco Cards,Cisco-Cisco Catalyst Cards,Cisco-Cisco Image,Cisco-Cisco Memory Pools,Cisco-Cisco CDP,Cisco-Cisco Flash,Cisco-Cisco FRU,Standard-Entity MIB,Standard-Interfaces,Standard-System Information,Standard-Reverse DNS,Standard-IP Addresses,Standard-Active Ports,Windows-Windows Users,Windows-Windows,Windows-Windows Services,Windows-Windows Software,Route Tables-IP CIDR Route Table,Standard-Juniper Entity MIB,F5-System,F5-Local TM,F5-Global TM", null);
                ncmInventorySettings.LoadInventorySpecs(InventorySpecList);
                ConcurrentNodes = Convert.ToInt16(proxy.Cirrus.Settings.GetSetting(Settings.ConcurrentNodes, 5, null));
                slConcurrentNodes.Value = ConcurrentNodes;
                ExtendedVLANsInventory = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.ExtendedVLANsInventory, false, null));
                chkExtendedVLANsInventory.Checked = ExtendedVLANsInventory;
                DisableInventoryLookup = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.DisableInventoryLookup, false, null));
                chkDisableInventoryLookup.Checked = DisableInventoryLookup;
            }
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        using (var proxy = isLayer.Proxy)
        {
            var inventorySpecList = ncmInventorySettings.GetSelectedInventorySpecs();
            if (!InventorySpecList.Equals(inventorySpecList))
            {
                proxy.Cirrus.Settings.SaveSetting(Settings.IncludeInInventory, inventorySpecList, null);
            }
            if (ConcurrentNodes != slConcurrentNodes.Value)
            {
                proxy.Cirrus.Settings.SaveSetting(Settings.ConcurrentNodes, slConcurrentNodes.Value, null);
            }
            if (ExtendedVLANsInventory != chkExtendedVLANsInventory.Checked)
            {
                proxy.Cirrus.Settings.SaveSetting(Settings.ExtendedVLANsInventory, chkExtendedVLANsInventory.Checked,
                    null);
            }
            if (DisableInventoryLookup != chkDisableInventoryLookup.Checked)
            {
                proxy.Cirrus.Settings.SaveSetting(Settings.DisableInventoryLookup, chkDisableInventoryLookup.Checked,
                    null);
            }
        }

        LocalizableButton btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        LocalizableButton btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }
}
