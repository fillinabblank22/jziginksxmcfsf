<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="ConfigDownloadsNotifications.aspx.cs" Inherits="Orion_NCM_Admin_Settings_ConfigDownloadsNotifications" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" runat="Server">
    <link rel="stylesheet" type="text/css" href="Settings.css" />
	
	<script type="text/javascript">
        $(document).ready(function() 
        {
            var checked = $("[id$='_cbDisableRTNEmail']").attr("checked");
            DisableRTNEmailClick(checked);
            
            checked = $("[id$='_cbRTNWithDiffs']").attr("checked");
            RTNWithDiffsClick(checked);
            
            if($("[id$='_btnAddCC']").length == 0)
                $("#ccBody").show();
            
            if($("[id$='_btnAddBCC']").length == 0)
                $("#bccBody").show();
                
        });
        
        function DisableRTNEmailClick(checked)
        {
            if(checked)
            {
                $("[id$='_cbRTNWithDiffs']").attr("disabled", "disabled");
                $("[id$='_cbRTNWithDiffs']").removeAttr("checked");
                $("[id$='_cbRTNInHTMLFormat']").attr("disabled", "disabled");
                $("[id$='_cbRTNInHTMLFormat']").removeAttr("checked");
                $("[id$='_txtRTNEmailFromName']").attr("disabled", "disabled");
                $("[id$='_txtRTNEmailReplyAddress']").attr("disabled", "disabled");
                $("[id$='_txtRTNEmailSubject']").attr("disabled", "disabled");
                $("[id$='_txtRTNEmailTo']").attr("disabled", "disabled");
                $("[id$='_txtRTNEmailCC']").attr("disabled", "disabled");
                $("[id$='_txtRTNEmailBCC']").attr("disabled", "disabled");
                EnableMailValidators(false);
            }
            else
            {
                $("[id$='_cbRTNWithDiffs']").removeAttr("disabled");
                $("[id$='_cbRTNInHTMLFormat']").removeAttr("disabled");
                $("[id$='_txtRTNEmailFromName']").removeAttr("disabled");
                $("[id$='_txtRTNEmailReplyAddress']").removeAttr("disabled");
                $("[id$='_txtRTNEmailSubject']").removeAttr("disabled");
                $("[id$='_txtRTNEmailTo']").removeAttr("disabled");
                $("[id$='_txtRTNEmailCC']").removeAttr("disabled");
                $("[id$='_txtRTNEmailBCC']").removeAttr("disabled");
                EnableMailValidators(true);
            }
        }
        
        function RTNWithDiffsClick(checked)
        {
            if(checked)
            {
                $("[id$='_cbRTNInHTMLFormat']").removeAttr("disabled");
            }
            else
            {
                $("[id$='_cbRTNInHTMLFormat']").attr("disabled", "disabled");
                $("[id$='_cbRTNInHTMLFormat']").removeAttr("checked");
            }
        }
        
        function AddCCClick()
        {
            $("#ccBody").show();
            $("[id$='_btnAddCC']").hide();
            return false;
        }
       
        function AddBCCClick()
        {
            $("#bccBody").show();
            $("[id$='_btnAddBCC']").hide();
            return false;
        }

        function EnableMailValidators(sValue)
        {
            var validator1 = document.getElementById("<%=rfvSenderName.ClientID %>");
            var validator2 = document.getElementById("<%=rfvReplyAddr.ClientID %>");
            var validator3 = document.getElementById("<%=rfvTo.ClientID %>");

            ValidatorEnable(validator1, sValue);
            ValidatorEnable(validator2, sValue);
            ValidatorEnable(validator3, sValue);
        }

    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMPHRTCDConfigDownloadsNotifications" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
	<asp:Panel ID="ContentContainer" CssClass="ContentContainer" runat="server">
         <div class="SettingContainer">
	        <div><%=Resources.NCMWebContent.WEBDATA_VM_155 %></div>

            <div id="DownloadCfgFileBody">
	            <div class="SettingBlockHeader" style="padding-top:20px;"><%=Resources.NCMWebContent.WEBDATA_VM_156 %></div>
	            <table cellpadding="0" cellspacing="0">
	                <tr>
	                    <td colspan="2" style="padding-top:5px;padding-bottom:5px;"><%=Resources.NCMWebContent.WEBDATA_VK_693 %></td>
	                </tr>
	                <tr>
		                <td>
			                <ncm:ConfigTypes ID="RTNConfigType" runat="server" CssClass="SettingSelectField" Width="200px"></ncm:ConfigTypes>
		                </td>
	                </tr>
	            </table>
            </div>
            
            <div id="BaselineCfgFileBody">
                <div class="SettingBlockHeader" style="padding-top:50px;"><%=Resources.NCMWebContent.WEBDATA_VM_158 %></div>
                <div class="SettingBlockDescription"><%=Resources.NCMWebContent.WEBDATA_VM_159 %></div>
                
                <table cellpadding="0" cellspacing="0">
	                <tr>
	                    <td style="padding-top:20px;"><asp:RadioButton ID="rbRTNLastDownloadConfig" GroupName="RTN" Text="<%$ Resources: NCMWebContent, WEBDATA_VM_160 %>" runat="server" /></td>
	                    <td style="padding-top:20px;padding-left:20px;"><asp:RadioButton ID="rbRTNBaselineConfig" GroupName="RTN" Text="<%$ Resources: NCMWebContent, WEBDATA_VM_161 %>" runat="server" /></td>
	                </tr>
	            </table>
            </div>
            
            <div id="EmailNotificationBody">
                <div class="SettingBlockHeader" style="padding-top:50px;"><%=Resources.NCMWebContent.WEBDATA_VM_162 %></div>
                <div style="padding-top:10px;"><asp:CheckBox ID="cbDisableRTNEmail" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VM_163 %>" onclick="DisableRTNEmailClick(this.checked);" /></div>
                <div style="padding-top:10px;"><asp:CheckBox ID="cbRTNWithDiffs" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VM_164 %>" onclick="RTNWithDiffsClick(this.checked);" /></div>
                <div style="padding-top:10px;"><asp:CheckBox ID="cbRTNInHTMLFormat" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VM_165 %>" /></div>
                <div style="padding-top:30px;"><%=Resources.NCMWebContent.WEBDATA_VM_167%></div>
                <div id="mailSettingsBody">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding-top:20px;">
                                <div style="padding-bottom:5px;"><b><%=Resources.NCMWebContent.WEBDATA_IC_70 %></b></div>
                                <div>
                                    <asp:TextBox ID="txtRTNEmailFromName" runat="server" TextMode="SingleLine" Height="18px" CssClass="SettingTextField" Width="300px" />
                                    <asp:RequiredFieldValidator ID="rfvSenderName" runat="server" Display="Dynamic" ControlToValidate="txtRTNEmailFromName" ErrorMessage="*" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top:20px;">
                                <div style="padding-bottom:5px;"><b><%=Resources.NCMWebContent.WEBDATA_IC_71 %></b></div>
                                <div>
                                    <asp:TextBox ID="txtRTNEmailReplyAddress" runat="server" TextMode="SingleLine" Height="18px" CssClass="SettingTextField" Width="300px" />
                                    <asp:RequiredFieldValidator ID="rfvReplyAddr" runat="server" Display="Dynamic" ControlToValidate="txtRTNEmailReplyAddress" ErrorMessage="*" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top:20px;">
                                <div style="padding-bottom:5px;"><b><%=Resources.NCMWebContent.WEBDATA_IC_72 %></b></div>
                                <div>
                                    <asp:TextBox ID="txtRTNEmailSubject" runat="server" TextMode="SingleLine" Height="18px" CssClass="SettingTextField" Width="300px" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                             <td style="padding-top:20px;padding-bottom:5px;"> 
                                <b><%=Resources.NCMWebContent.WEBDATA_IC_73 %></b>
                             </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtRTNEmailTo" runat="server" CssClass="SettingTextField" Width="300px" Height="18px" MaxLength="4000"/>
                                <asp:RequiredFieldValidator ID="rfvTo" runat="server" Display="Dynamic" ControlToValidate="txtRTNEmailTo" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <td style="padding-left:5px"> 
                                <orion:LocalizableButton runat="server" ID="btnAddCC" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_IC_74 %>" DisplayType="Small" CausesValidation="false" OnClientClick="return AddCCClick();"  />
                            </td>
                            <td style="padding-left:5px"> 
                                <orion:LocalizableButton runat="server" ID="btnAddBCC" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_IC_75 %>" DisplayType="Small" CausesValidation="false" OnClientClick="return AddBCCClick();" />
                            </td>
                        </tr>
                        <tr id="ccBody"style="display:none;">
                            <td colspan="3" style="padding-top:20px;">
                                <div style="padding-bottom:5px;"><b><%=Resources.NCMWebContent.WEBDATA_IC_76 %></b></div>
                                <div>
                                    <asp:TextBox ID="txtRTNEmailCC" runat="server" TextMode="SingleLine" Height="18px" CssClass="SettingTextField" Width="300px" />
                                </div>
                            </td>
                        </tr>
                        <tr id="bccBody" style="display:none;">
                            <td colspan="3" style="padding-top:20px;">
                                <div style="padding-bottom:5px;"><b><%=Resources.NCMWebContent.WEBDATA_IC_77 %></b></div>
                                <div>
                                    <asp:TextBox ID="txtRTNEmailBCC" runat="server" TextMode="SingleLine" Height="18px" CssClass="SettingTextField" Width="300px" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div class="SettingHint" style="padding-top:10px;"><%=Resources.NCMWebContent.WEBDATA_IC_78 %></div>
                </div>
            </div>
        </div>
        <div style="padding-top:20px;">
            <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" OnClick="btnSubmit_Click" DisplayType="Primary" CausesValidation="true" CommandArgument="~/Orion/NCM/Admin/Settings/RTNSettings.aspx" />
            <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" OnClick="btnCancel_Click" DisplayType="Secondary" CausesValidation="false" CommandArgument="~/Orion/NCM/Admin/Settings/RTNSettings.aspx" />
        </div>
    </asp:Panel>
</asp:Content>
