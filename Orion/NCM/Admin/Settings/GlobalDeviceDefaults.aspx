<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="GlobalDeviceDefaults.aspx.cs" Inherits="Orion_NCM_Admin_Settings_GlobalDeviceDefaults" Title="Untitled Page" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content" ContentPlaceHolderID="adminHeadPlaceHolder" runat="server">
    <link rel="stylesheet" type="text/css" href="Settings.css" />
    <script type="text/javascript">
        function cvPassword_Validate(source, args)
        {
            args.IsValid = (args.Value == document.getElementById("<%=txtPasswordNew.ClientID %>").value);
        }
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
  <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMPHGlobalDeviceDefaults" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <asp:Panel ID="ContentContainer" CssClass="ContentContainer" runat="server">
        <div class="SettingContainer">
            <div class="SettingBlockDescription" style="padding-top:0px;"><%=Resources.NCMWebContent.WEBDATA_VM_131 %></div>
            <div style="padding-top:10px;">
                <span class="SettingSuggestion"><span class="SettingSuggestion-Icon"></span>
                    <%=Resources.NCMWebContent.WEBDATA_VM_132 %>
                </span>
            </div>
            
            <div id="loginContainer">
                <div class="SettingBlockHeader" style="padding-top:50px;"><%=Resources.NCMWebContent.WEBDATA_VM_133 %></div>
                <div class="SettingBlockDescription"><%=Resources.NCMWebContent.WEBDATA_VM_134 %>&nbsp;<span style='color:gray;'><%=Resources.NCMWebContent.WEBDATA_VM_135 %></span></div>
                <div style="padding-top:5px;"><span style="font-size:8pt;">&nbsp;&#0187;&nbsp;<%= this.LearnMore %></span></div>
                
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td nowrap="nowrap" style="padding-top:40px;">
                            <div style="padding-bottom:5px;"><b><%=Resources.NCMWebContent.WEBDATA_IC_62%></b></div>
                            <div>
                                <ncm:PasswordTextBox ID="txtUsername" runat="server" CssClass="SettingTextField" Width="200px" MaxLength="4000"></ncm:PasswordTextBox>
                                <span style="padding-left:20px;"><asp:CheckBox ID="cbResetToGlobalLogin" runat="server" Text ="<%$ Resources: NCMWebContent, WEBDATA_VM_136 %>"></asp:CheckBox></span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top:20px;">
                            <div style="padding-bottom:5px;"><b><%=Resources.NCMWebContent.WEBDATA_IC_60%></b></div>
                            <div>
                                <asp:TextBox ID="txtPasswordNew" runat="server" TextMode="Password" CssClass="SettingTextField" Width="200px" MaxLength="4000"></asp:TextBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top:20px;">
                            <div style="padding-bottom:5px;"><b><%=Resources.NCMWebContent.WEBDATA_IC_64 %></b></div>
                            <div>
                                <asp:TextBox ID="txtPasswordConfirmNew" runat="server" TextMode="Password" CssClass="SettingTextField" Width="200px" MaxLength="4000"></asp:TextBox>
                            </div>
                            <div>
                                <asp:CustomValidator id="comparePasswords" EnableClientScript="true" ClientValidationFunction="cvPassword_Validate" ValidateEmptyText="true" ControlToValidate="txtPasswordConfirmNew" runat="server" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_689 %>" Display="Dynamic" Enabled="true" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top:20px;">
                            <div style="padding-bottom:5px;"><b><%=Resources.NCMWebContent.WEBDATA_VK_283 %></b></div>
                            <div>
                                <asp:DropDownList ID="ddlEnableLevel" runat="server" CssClass="SettingSelectField" Width="200px">
                                    <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_314 %>" Value="<No Enable Login>"></asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_315 %>" Value="enable"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top:20px;">
                            <div style="padding-bottom:5px;"><b><%=Resources.NCMWebContent.WEBDATA_VM_137 %></b></div>
                            <div>
                                <asp:TextBox ID="txtEnablePasswordNew" runat="server" TextMode="Password" CssClass="SettingTextField" Width="200px" MaxLength="4000"></asp:TextBox>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
   
            <div id="communicationContainer">
                <div class="SettingBlockHeader" style="padding-top:50px;"><%=Resources.NCMWebContent.WEBDATA_VM_138 %></div>
                <div class="SettingBlockDescription"><%=Resources.NCMWebContent.WEBDATA_VM_134 %>&nbsp;<span style='color:gray;'><%=Resources.NCMWebContent.WEBDATA_VK_694 %></span></div>
            
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td nowrap="nowrap" style="padding-top:40px;">
                            <div style="padding-bottom:5px;"><b><%=Resources.NCMWebContent.WEBDATA_VM_139 %></b></div>
                            <div>
                                <asp:DropDownList ID="ddlExCfgScrProtocol" runat="server" CssClass="SettingSelectField" Width="200px">
                                </asp:DropDownList>
                                <span style="padding-left:20px;"><asp:CheckBox ID="cbResetToGlobalCommunication" runat="server" Text ="<% $Resources: NCMWebContent, WEBDATA_VM_136 %>"></asp:CheckBox></span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top:20px;">
                            <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                                <ContentTemplate>
                                    <div style="padding-bottom:5px;"><b><%=Resources.NCMWebContent.WEBDATA_VM_140 %></b></div>
                                    <div>
                                        <asp:DropDownList ID="ddlCfgReqProtocol" runat="server" Width="200px" CssClass="SettingSelectField" AutoPostBack="true" OnSelectedIndexChanged="ddlCfgReqProtocol_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div> 
                                    <div style="padding-top:20px;">
                                        <div style="padding-bottom:5px;"><b><%=Resources.NCMWebContent.WEBDATA_VM_141 %></b></div>
                                        <div>
                                            <asp:DropDownList ID="ddlTransfCfgProtocol" runat="server" CssClass="SettingSelectField" Width="200px">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </div>
   
            <div id="transferContainer">
                <div class="SettingBlockHeader" style="padding-top:50px;"><%=Resources.NCMWebContent.WEBDATA_VM_142 %></div>
                <div class="SettingBlockDescription"><%=Resources.NCMWebContent.WEBDATA_VM_134 %>&nbsp;<span style='color:gray;'><%=Resources.NCMWebContent.WEBDATA_VK_695 %></span></div>
            
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td nowrap="nowrap" style="padding-top:40px;">
                            <div style="padding-bottom:5px;"><b><%=Resources.NCMWebContent.WEBDATA_VM_143 %></b></div>
                            <div>
                                <asp:TextBox runat="server" ID="txtTelnetPort" CssClass="SettingTextField" Width="200px" ></asp:TextBox>
                                <span style="padding-left:20px;"><asp:CheckBox ID="cbResetToGlobalTransfer" runat="server" Text ="<%$ Resources: NCMWebContent, WEBDATA_VM_136 %>"></asp:CheckBox></span>
                            </div>
                            <div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                    ControlToValidate="txtTelnetPort"
                                    Display="Dynamic"
                                    ErrorMessage="<%$ Resources: NCMWebContent,WEBDATA_VM_144 %>"
                                    runat="server"/> 
                                <asp:CompareValidator runat="server" ID="cmpNumbers1" 
                                    ControlToValidate="txtTelnetPort" 
                                    ValueToCompare="0"
                                    ErrorMessage="<%$ Resources: NCMWebContent,WEBDATA_VM_145 %>"
                                    Operator="GreaterThan" type="Integer" Display="Dynamic" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top:20px;">
                            <div style="padding-bottom:5px;"><b><%=Resources.NCMWebContent.WEBDATA_VM_146 %></b></div>
                            <div>
                                <asp:TextBox runat="server" ID="txtSSHPort" CssClass="SettingTextField" Width="200px"></asp:TextBox>
                            </div>
                            <div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                    ControlToValidate="txtSSHPort"
                                    Display="Dynamic"
                                    ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VM_147 %>"
                                    runat="server" /> 
                                <asp:CompareValidator runat="server" ID="cmpNumbers2" 
                                    ControlToValidate="txtSSHPort" 
                                    ValueToCompare="0"
                                    ErrorMessage="<%$ Resources: NCMWebContent,WEBDATA_VM_148 %>"
                                    Operator="GreaterThan" Type="Integer" Display="Dynamic" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div style="padding-top:20px;">
            <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" OnClick="btnSubmit_Click" DisplayType="Primary" CausesValidation="true" CommandArgument="~/Orion/NCM/Admin/Default.aspx" />
            <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" OnClick="btnCancel_Click" DisplayType="Secondary" CausesValidation="false" CommandArgument="~/Orion/NCM/Admin/Default.aspx" />
        </div>
    </asp:Panel>
</asp:Content>

