using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCM.Common.Dal;

public partial class Orion_NCM_Admin_Settings_TFTPServer : System.Web.UI.Page
{
    private readonly Log log = new Log();

    private ISWrapper isLayer;

    protected bool TFTPAutoRetriveIP
    {
        get { return Convert.ToBoolean(ViewState["TFTPAutoRetriveIP"]); }
        set { ViewState["TFTPAutoRetriveIP"] = value; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ValidatePageAccess();
    }

    private void ValidatePageAccess()
    {
        if (CommonHelper.IsDemoMode() ||
            !(this.Page is IBypassAccessLimitation) && !Profile.AllowAdmin && !SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR))
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_VK_554}");
        }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_VK_653;

        isLayer = new ISWrapper();

        chkTFTPAutoRetriveIP.Text = $@"<b>{Resources.NCMWebContent.WEBDATA_VM_166}</b>";

        if (!Page.IsPostBack)
        {
            using (var proxy = isLayer.Proxy)
            {
                TFTPAutoRetriveIP = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.TFTPAutoRetriveIP, false, null));
                chkTFTPAutoRetriveIP.Checked = TFTPAutoRetriveIP;
            }
            
            var engines = LookupEngines();
            repeater.DataSource = engines;
            repeater.DataBind();

            EnableTFTPServerAddress(chkTFTPAutoRetriveIP.Checked);
        }
    }

    protected void repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        var engine = e.Item.DataItem as EngineInfo;
        if (engine != null)
        {
            int engineId = Convert.ToInt16(engine.EngineId);
            var hiddenEngineID = e.Item.FindControl(@"hiddenEngineID") as HiddenField;
            hiddenEngineID.Value = engineId.ToString();

            var engines = LookupEngines().ToArray();
            if (engines.Length > 1)
            {
                var litServerName = e.Item.FindControl(@"litServerName") as Literal;
                litServerName.Text =
                    $@"<div style='font-weight:bold;padding-bottom:20px;{(e.Item.ItemIndex == 0 ? string.Empty : @"padding-top:20px;")}'>{engine.EngineName}</div>";

                var td = e.Item.FindControl(@"tdHolder") as HtmlTableCell;
                if (engines.Length != (e.Item.ItemIndex + 1)) td.Style.Add(@"border-bottom", @"dashed 1px #dcdbd7");
            }

            var txtTFTPServerAddress = e.Item.FindControl(@"txtTFTPServerAddress") as TextBox;
            var txtTFTPRootDir = e.Item.FindControl(@"txtTFTPRootDir") as TextBox;
            try
            {
                using (var proxy = isLayer.Proxy)
                {
                    var defaultIP = proxy.Cirrus.Settings.RetrieveIP(engineId);
                    var tftpServerAddress = proxy.Cirrus.Settings.GetSetting(Settings.TFTPServerAddress, string.Empty, engineId);
                    if (string.IsNullOrEmpty(tftpServerAddress))
                    {
                        tftpServerAddress = defaultIP;
                    }
                    txtTFTPServerAddress.Text = tftpServerAddress;
                    txtTFTPRootDir.Text = proxy.Cirrus.Settings.GetSetting(Settings.TFTPRootDir, @"C:\TFTP-Root", engineId);
                }
            }
            catch (Exception ex)
            {
                engineId = -1;
                hiddenEngineID.Value = engineId.ToString();

                txtTFTPServerAddress.Enabled = false;
                txtTFTPServerAddress.CssClass = @"TextFieldDisabled";
                txtTFTPRootDir.Enabled = false;
                txtTFTPRootDir.CssClass = @"TextFieldDisabled";

                var btnRetrieveIP = e.Item.FindControl(@"btnRetrieveIP") as LocalizableButton;
                btnRetrieveIP.Enabled = false;
                btnRetrieveIP.CssClass = @"ButtonDisabled";
                var btnValidate = e.Item.FindControl(@"btnValidate") as LocalizableButton;
                btnValidate.Enabled = false;
                btnValidate.CssClass = @"ButtonDisabled";

                var tr = e.Item.FindControl(@"exHolder") as HtmlTableRow;
                tr.Visible = true;

                var exWarn = e.Item.FindControl(@"exWarn") as Literal;
                exWarn.Text = ex.Message;
            }
        }
    }

    protected void repeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        var hiddenEngineID = e.Item.FindControl(@"hiddenEngineID") as HiddenField;
        int engineId = Convert.ToInt16(hiddenEngineID.Value);
        if (e.CommandName.Equals(@"Validate"))
        {
            var txtTFTPRootDir = e.Item.FindControl(@"txtTFTPRootDir") as TextBox;
            var validatorHolder = e.Item.FindControl(@"validatorHolder") as HtmlGenericControl;
            string error;
            if (!ValidatePath(txtTFTPRootDir.Text, engineId, out error))
            {
                var literal = new Literal();
                literal.Text = string.IsNullOrEmpty(error) ? Resources.NCMWebContent.WEBDATA_VK_651 : error;
                validatorHolder.Style.Add(@"color", @"red");
                validatorHolder.Controls.Add(literal);
            }
            else
            {
                var literal = new Literal();
                literal.Text = Resources.NCMWebContent.WEBDATA_VK_597;
                validatorHolder.Style.Add(@"color", @"green");
                validatorHolder.Controls.Add(literal);
            }
        }

        if (e.CommandName.Equals(@"RetrieveIP"))
        {
            var txtTFTPServerAddress = e.Item.FindControl(@"txtTFTPServerAddress") as TextBox;
            using (var proxy = isLayer.Proxy)
            {
                txtTFTPServerAddress.Text = proxy.Cirrus.Settings.RetrieveIP(engineId);
            }
        }
    }

    protected void chkTFTPAutoRetriveIP_OnCheckedChanged(object sender, EventArgs e)
    {
        EnableTFTPServerAddress(chkTFTPAutoRetriveIP.Checked);
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        using (var proxy = isLayer.Proxy)
        {
            Dictionary<int, TFTPServerSettings> results;
            if (TryGetTFTPServerSettings(out results))
            {
                foreach (var kvp in results)
                {
                    if (chkTFTPAutoRetriveIP.Checked) proxy.Cirrus.Settings.SaveSetting(Settings.TFTPServerAddress, kvp.Value.TFTPServerAddress, kvp.Key);
                    proxy.Cirrus.Settings.SaveSetting(Settings.TFTPRootDir, kvp.Value.TFTPRootDir, kvp.Key);
                }

                if (TFTPAutoRetriveIP != chkTFTPAutoRetriveIP.Checked)
                    proxy.Cirrus.Settings.SaveSetting(Settings.TFTPAutoRetriveIP, chkTFTPAutoRetriveIP.Checked, null);
            }
            else
            {
                return;
            }
        }

        var btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        var btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }

    private void EnableTFTPServerAddress(bool bEnable)
    {
        foreach (RepeaterItem item in repeater.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                var hiddenEngineID = item.FindControl(@"hiddenEngineID") as HiddenField;
                if (Convert.ToInt16(hiddenEngineID.Value) < 0) continue;

                var txtTFTPServerAddress = item.FindControl(@"txtTFTPServerAddress") as TextBox;
                txtTFTPServerAddress.Enabled = bEnable;
                var btnRetrieveIP = item.FindControl(@"btnRetrieveIP") as LocalizableButton;
                btnRetrieveIP.Enabled = bEnable;
                btnRetrieveIP.CssClass = bEnable ? @"ButtonEnabled" : @"ButtonDisabled";
            }
        }
    }

    private bool TryGetTFTPServerSettings(out Dictionary<int, TFTPServerSettings> results)
    {
        var isValid = true;

        results = new Dictionary<int, TFTPServerSettings>();

        foreach (RepeaterItem item in repeater.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                var engineId = string.Empty;
                var tftpServerAddress = string.Empty;
                var tftpRootDir = string.Empty;

                var hiddenEngineID = item.FindControl(@"hiddenEngineID") as HiddenField;
                engineId = hiddenEngineID.Value;
                if (Convert.ToInt16(engineId) < 0) continue;

                var txtTFTPServerAddress = item.FindControl(@"txtTFTPServerAddress") as TextBox;
                tftpServerAddress = txtTFTPServerAddress.Text;

                var txtTFTPRootDir = item.FindControl(@"txtTFTPRootDir") as TextBox;
                tftpRootDir = txtTFTPRootDir.Text;

                var validatorHolder = item.FindControl(@"validatorHolder") as HtmlGenericControl;
                string error;
                if (!ValidatePath(tftpRootDir, Convert.ToInt16(engineId), out error))
                {
                    var literal = new Literal();
                    literal.Text = string.IsNullOrEmpty(error) ? Resources.NCMWebContent.WEBDATA_VK_651 : error;
                    validatorHolder.Style.Add(@"color", @"red");
                    validatorHolder.Controls.Add(literal);
                    isValid = false;
                }

                if (isValid)
                {
                    var tftServerSettings = new TFTPServerSettings(tftpServerAddress, tftpRootDir);
                    results.Add(Convert.ToInt16(engineId), tftServerSettings);
                }
            }
        }

        return isValid;
    }

    private bool ValidatePath(string path, int engineId, out string error)
    {
        try
        {
            using (var proxy = isLayer.Proxy)
            {
                error = string.Empty;
                return proxy.Cirrus.Settings.ValidatePathWithoutImpersonation(path, engineId);
            }
        }
        catch (Exception ex)
        {
            error = ex.Message;
            log.Error(ex);
            return false;
        }
    }

    private IEnumerable<EngineInfo> LookupEngines()
    {
        var enginesDal = ServiceLocator.Container.Resolve<ISwisEnginesDal>();
        return enginesDal.GetAllPollingEngines();
    }

    private class TFTPServerSettings
    {
        private string _tftpServerAddress;
        private string _tftpRootDir;

        public TFTPServerSettings(string tftpServerAddress, string tftpRootDir)
        {
            _tftpServerAddress = tftpServerAddress;
            _tftpRootDir = tftpRootDir;
        }

        public string TFTPServerAddress
        {
            get { return _tftpServerAddress; }
            set { _tftpServerAddress = value; }
        }

        public string TFTPRootDir
        {
            get { return _tftpRootDir; }
            set { _tftpRootDir = value; }
        }
    }
}
