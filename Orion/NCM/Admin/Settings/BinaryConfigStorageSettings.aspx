﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="BinaryConfigStorageSettings.aspx.cs" Inherits="Orion_NCM_Admin_Settings_BinaryConfigStorageSettings" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/Settings/Settings.css" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMPHBinaryConfigStorageSettings" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <asp:Panel ID="ContentContainer" CssClass="ContentContainer" runat="server">
        <div class="SettingContainer">
            <table cellpadding="0" cellspacing="0" width="80%">
                <tr>
                    <td colspan="2">
                        <div style="padding-bottom:5px;"><%= Resources.NCMWebContent.WEBDATA_VK_1046 %></div>
                        <asp:TextBox ID="txtPath" runat="server" TextMode="SingleLine" CssClass="SettingTextField" Width="60%"/>
                        <asp:RequiredFieldValidator ID="rfvLocation" runat="server" ControlToValidate="txtPath" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                    <tr>
                    <td style="padding-top:30px;padding-right:5px;" colspan="2">
                        <%= Resources.NCMWebContent.WEBDATA_VK_1047 %>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:5px;padding-right:5px;width:5%;white-space:nowrap;">
                        <%= Resources.NCMWebContent.WEBDATA_VK_1048 %>
                    </td>
                    <td style="padding-top:5px;width:95%;">
                        <asp:TextBox ID="txtUserName" autocomplete="off" runat="server" CssClass="SettingTextField" Width="20%"/>
                        <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ControlToValidate="txtUserName" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="padding-right:5px;width:5%;">&nbsp;</td>
                    <td colspan="2" style="color:#5F5F5F;font-size:10px;width:95%;">
                        <%= Resources.NCMWebContent.WEBDATA_VK_1049 %>
                    </td>
                </tr>
                <tr>
                    <td style="padding-right:5px;padding-top:5px;width:5%;white-space:nowrap;">
                        <%= Resources.NCMWebContent.WEBDATA_VK_297 %>
                    </td>
                    <td style="padding-top:5px;width:95%;">
                        <ncm:PasswordTextBox ID="txtPassword" TextMode="Password" runat="server" CssClass="SettingTextField" Width="20%"></ncm:PasswordTextBox>
                        <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
            <div style="padding-top:10px;">
                <asp:Literal ID="lValidator" runat="server"></asp:Literal>
            </div>
        </div>
        <div style="padding-top:20px;">
            <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" OnClick="btnSubmit_Click" DisplayType="Primary" CommandArgument="~/Orion/NCM/Admin/Default.aspx" CausesValidation="true"/>
            <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" OnClick="btnCancel_Click" DisplayType="Secondary" CommandArgument="~/Orion/NCM/Admin/Default.aspx" CausesValidation="false"/>
            <span style="padding-left:20px;">
                <orion:LocalizableButton runat="server" ID="btnValidate" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_590 %>" OnClick="btnValidate_Click" CausesValidation="true" />
            </span>
        </div>
    </asp:Panel>
</asp:Content>

