using System;
using System.Web.UI.WebControls;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCM.Contracts;

public partial class Orion_NCM_Admin_Settings_ConfigChangeSettings : System.Web.UI.Page
{
    private readonly IIsWrapper isLayer;
    private readonly IDataDecryptor dataDecryptor;

    public Orion_NCM_Admin_Settings_ConfigChangeSettings()
    {
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
        dataDecryptor = ServiceLocator.Container.Resolve<IDataDecryptor>();
    }

    protected bool HideUsernames
    {
        get
        {
            if (ViewState["HideUsernames"] == null)
            {
                using (var proxy = isLayer.GetProxy())
                {
                    var hideUsernames = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.HideUsernames, false, null));
                    ViewState["HideUsernames"] = hideUsernames;
                    return hideUsernames;
                }
            }
            else
            {
                return Convert.ToBoolean(ViewState["HideUsernames"]);
            }
        }
    }

    protected string ConnectivityLevel
    {
        get { return Convert.ToString(ViewState["ConnectivityLevel"]); }
        set { ViewState["ConnectivityLevel"] = value; }
    }

    protected bool RTNUseUserDevice
    {
        get { return Convert.ToBoolean(ViewState["RTNUseUserDevice"]); }
        set { ViewState["RTNUseUserDevice"] = value; }
    }

    protected string RTNUserLevelName
    {
        get { return Convert.ToString(ViewState["RTNUserLevelName"]); }
        set { ViewState["RTNUserLevelName"] = value; }
    }

    protected string RTNUserLevelPassword
    {
        get { return Convert.ToString(ViewState["RTNUserLevelPassword"]); }
        set { ViewState["RTNUserLevelPassword"] = value; }
    }

    protected string RTNUserLevelEnableLevel
    {
        get { return Convert.ToString(ViewState["RTNUserLevelEnableLevel"]); }
        set { ViewState["RTNUserLevelEnableLevel"] = value; }
    }

    protected string RTNUserLevelEnablePassword
    {
        get { return Convert.ToString(ViewState["RTNUserLevelEnablePassword"]); }
        set { ViewState["RTNUserLevelEnablePassword"] = value; }
    }

    protected bool RTNLimitDownloadOperations
    {
        get { return Convert.ToBoolean(ViewState["RTNLimitDownloadOperations"]); }
        set { ViewState["RTNLimitDownloadOperations"] = value; }
    }

    protected bool RTNIncludeNotification
    {
        get { return Convert.ToBoolean(ViewState["RTNIncludeNotification"]); }
        set { ViewState["RTNIncludeNotification"] = value; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ValidatePageAccess();
    }

    private void ValidatePageAccess()
    {
        if (CommonHelper.IsDemoMode() ||
            !(this.Page is IBypassAccessLimitation) && !Profile.AllowAdmin && !SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR))
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_VK_554}");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_VK_690;
        
        chkRTNUseUserDevice.Text = Resources.NCMWebContent.WEBDATA_VK_691;       
        if (HideUsernames)
        {
            txtRTNUserLevelName.TextMode = TextBoxMode.Password;
        }
        
        if (!Page.IsPostBack)
        {
            compareValidator2.Attributes[@"ControlToCompareId"] = txtRTNUserLevelPassword.ClientID;

            using (var proxy = isLayer.GetProxy())
            {
                ConnectivityLevel = proxy.Cirrus.Settings.GetSetting(Settings.ConnectivityLevel, ConnectivityLevels.Default, null);
                var rtnUseUserDevice = ConnectivityLevel.Equals(ConnectivityLevels.Level3, StringComparison.CurrentCultureIgnoreCase);
                chkRTNUseUserDevice.Enabled = rtnUseUserDevice;
                if (rtnUseUserDevice)
                {
                    rtnUseUserDevice = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.RTNUseUserDevice, false, null));
                }
                RTNUseUserDevice = rtnUseUserDevice;
                chkRTNUseUserDevice.Checked = RTNUseUserDevice;

                RTNUserLevelName = dataDecryptor.Decrypt(proxy.Cirrus.Settings.GetSetting(Settings.RTNUserLevelName, string.Empty, null));
                txtRTNUserLevelName.PasswordText = RTNUserLevelName;
                RTNUserLevelPassword = dataDecryptor.Decrypt(proxy.Cirrus.Settings.GetSetting(Settings.RTNUserLevelPassword, string.Empty, null));
                txtRTNUserLevelPassword.PasswordText = RTNUserLevelPassword;
                txtRTNUserLevelConfirmPassword.PasswordText = RTNUserLevelPassword;
                RTNUserLevelEnableLevel = proxy.Cirrus.Settings.GetSetting(Settings.RTNUserLevelEnableLevel, @"<No Enable Login>", null);
                ddlRTNUserLevelEnableLevel.SelectedValue = RTNUserLevelEnableLevel;
                RTNUserLevelEnablePassword = dataDecryptor.Decrypt(proxy.Cirrus.Settings.GetSetting(Settings.RTNUserLevelEnablePassword, string.Empty, null));
                txtRTNUserLevelEnablePassword.PasswordText = RTNUserLevelEnablePassword;
              
                RTNIncludeNotification = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.RTNIncludeNotification, false, null));
                chkRTNIncludeNotification.Checked = RTNIncludeNotification;
            }
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        using (var proxy = isLayer.GetProxy())
        {
            if (RTNUseUserDevice != chkRTNUseUserDevice.Checked)
                proxy.Cirrus.Settings.SaveSetting(Settings.RTNUseUserDevice, chkRTNUseUserDevice.Checked, null);

            if (chkRTNUseUserDevice.Checked)
            {
                if (RTNUserLevelName != txtRTNUserLevelName.PasswordText)
                    proxy.Cirrus.Settings.SaveSetting(Settings.RTNUserLevelName, txtRTNUserLevelName.PasswordText, null);
                if (RTNUserLevelPassword != txtRTNUserLevelPassword.PasswordText)
                    proxy.Cirrus.Settings.SaveSetting(Settings.RTNUserLevelPassword, txtRTNUserLevelPassword.PasswordText, null);
                if (!RTNUserLevelEnableLevel.Equals(ddlRTNUserLevelEnableLevel.SelectedValue, StringComparison.CurrentCultureIgnoreCase))
                    proxy.Cirrus.Settings.SaveSetting(Settings.RTNUserLevelEnableLevel, ddlRTNUserLevelEnableLevel.SelectedValue, null);
                if (RTNUserLevelEnablePassword != txtRTNUserLevelEnablePassword.PasswordText)
                    proxy.Cirrus.Settings.SaveSetting(Settings.RTNUserLevelEnablePassword, txtRTNUserLevelEnablePassword.PasswordText, null);
            }
           
            if (RTNIncludeNotification != chkRTNIncludeNotification.Checked)
                proxy.Cirrus.Settings.SaveSetting(Settings.RTNIncludeNotification, chkRTNIncludeNotification.Checked, null);
        }

        var btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        var btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }
}
