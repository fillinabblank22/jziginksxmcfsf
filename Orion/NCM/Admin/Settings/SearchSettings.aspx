﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="SearchSettings.aspx.cs" Inherits="Orion_NCM_Admin_Settings_SearchSettings" %>

<%@ Register Assembly="Infragistics2.WebUI.WebDataInput.v8.2, Version=8.2.20082.1000, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.WebDataInput" TagPrefix="igtxt" %><%@ Register Assembly="Infragistics2.WebUI.WebDataInput.v8.2, Version=8.2.20082.1000, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.WebDataInput" TagPrefix="igtxt" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            var checked = document.getElementById("<%=rbEnableSearch.ClientID %>").checked;
            ShowHideSearchSettings(checked);
        });

        function ShowHideSearchSettings(checked) {
            if (checked) {
                $('#SearchSettingsDIV').show();
            }
            else {
                $('#SearchSettingsDIV').hide();
            }
        }
    </script>

    <style type="text/css">
        .rb input
        {
            vertical-align: middle;
        }
        
        .rb label
        {
            vertical-align: middle;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="Settings.css" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMPHSearchSettings" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <asp:Panel ID="ContentContainer" CssClass="ContentContainer" runat="server">
        <div class="SettingContainer">
            <div class="SettingBlockHeader"><%=Resources.NCMWebContent.WEBDATA_VK_725 %></div>

            <div style='padding-top:20px'>
                <asp:RadioButton ID="rbEnableSearch" runat="server" CssClass="rb" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_726 %>" GroupName="FTSEnabled" onclick="ShowHideSearchSettings(this.checked);" />
                <span style="padding-left:40px;">
                    <asp:RadioButton ID="rbDisableSearch" runat="server" CssClass="rb" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_721 %>" GroupName="FTSEnabled" onclick="ShowHideSearchSettings(false);" />
                    <span style="font-size:8pt;padding-left:5px;vertical-align:middle;">
                        &#0187;&nbsp;<a style="color:#336699;" href="<%=LearnMoreLink %>" target="_blank"><%=Resources.NCMWebContent.WEBDATA_VK_833%></a>
                    </span>
                </span>
            </div>

            <div id="SearchSettingsDIV">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style='padding-top:40px'>
                            <b><%=Resources.NCMWebContent.WEBDATA_VK_727 %></b>
                            <span style="padding-left:5px;font-style:italic;">
                                <asp:Label ID="lblIndexStatus" runat="server" />
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td style='padding-top:10px'>
                            <b><%=Resources.NCMWebContent.WEBDATA_VK_732 %></b>
                            <span style="padding-left:5px;">
                                <asp:Label ID="lblConfigsCount" runat="server" Text="test" />
                            </span>
                        </td>
                    </tr>
                </table>

                <div style="padding-top:20px;"><%=Resources.NCMWebContent.WEBDATA_VK_736 %></div>
                <div style="padding-top:5px;">
                <asp:UpdatePanel ID="pnlUpdate" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtFTSIndexCatalogPath" runat="server" TextMode="SingleLine" CssClass="SettingTextField" Width="500px" />
                        <orion:LocalizableButton runat="server" ID="btnApply" LocalizedText="CustomText" DisplayType="Small" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_738 %>" CausesValidation="true" OnClick="btnApply_Click"/>
                        <span class='SettingHint'><%=Resources.NCMWebContent.WEBDATA_VK_737 %></span>
                        <div id="validatorHolder" runat="server"></div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                </div>

                <div class="SettingBlockHeader" style='padding-top:50px'><%=Resources.NCMWebContent.WEBDATA_VK_730 %></div>
                <div style="padding-top:20px;">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding-right:30px;"><%=Resources.NCMWebContent.WEBDATA_VK_731 %></td>
                            <td>
                                <igtxt:WebNumericEdit ID="sRefreshPeriod" runat="server" HorizontalAlign="Right" Width="40" MaxValue="60" MinValue="1" MaxLength="2" Nullable="false">
                                </igtxt:WebNumericEdit>
                                <span style="padding-left:5px;"><%=Resources.NCMWebContent.WEBDATA_VK_574%></span>
                            </td>
                            <td style="padding-left:30px;"><span class="SettingNote"><%=string.Format(Resources.NCMWebContent.WEBDATA_VK_701, 10) %></span></td>
                        </tr>
                        <tr>
                            <td style="padding-top:20px;padding-right:30px;"><%=string.Format(Resources.NCMWebContent.WEBDATA_VK_734, @"<div><span class='SettingHint'>", @"</span></div>")%></td>
                            <td style="padding-top:20px;">
                                <igtxt:WebNumericEdit ID="sDelayTime" runat="server" HorizontalAlign="Right" Width="40" MaxValue="1000" MinValue="0" MaxLength="4" Nullable="false">
                                </igtxt:WebNumericEdit>
                                <span style="padding-left:5px;"><%=Resources.NCMWebContent.WEBDATA_VK_627%></span>
                            </td>
                            <td style="padding-top:20px;padding-left:30px;"><span class="SettingNote"><%=string.Format(Resources.NCMWebContent.WEBDATA_VK_697, 0) %></span></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div style="padding-top:20px;">
            <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" 
            OnClick="btnSubmit_Click" DisplayType="Primary"
            CommandArgument="~/Orion/NCM/Admin/Default.aspx" CausesValidation="true"/> 
            <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" 
            OnClick="btnCancel_Click" DisplayType="Secondary"
            CommandArgument="~/Orion/NCM/Admin/Default.aspx" CausesValidation="false"/>
        </div>
    </asp:Panel>
</asp:Content>


