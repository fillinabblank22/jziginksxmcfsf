﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="ConfigArchiveFolders.aspx.cs" Inherits="Orion_NCM_Admin_Settings_ConfigArchiveFolders" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="Settings.css" />
    <style type="text/css">
        .header
        {
            background-color: #E2E1D4;
            font-family: Arial,Helvetica,sans-serif;
            font-weight: normal;
            padding-left: 5px;
            height:25px;
            font-style:italic;
            padding:5px 10px 5px 10px;
        }
        
        .item
        {
            padding:5px 10px 5px 10px;
        }
        
        .item a
        {
            color: #336699;
        }
        
        .highlightRow
        {
            background: #fffdcc;
        }
        
        .TableException
        {
            background: #facece;
            color: #ce0000;
            border: 1px solid #ce0000;
            border-radius: 5px;
        }
        
        .SpanException
        {
            display: inline-block;
            position: relative;
            padding: 5px 6px 6px 26px;
        }
        
        .SpanException-Icon
        {
            position: absolute;
            left: 4px;
            top: 4px;
            height: 16px;
            width: 16px;
            background: url(/Orion/NCM/Resources/images/icon_failed.gif) top left no-repeat;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMPHChangeArchive" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
   <%=Page.Title %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <asp:Panel ID="ContentContainer" CssClass="ContentContainer" runat="server">
        <div class="SettingContainer">
            <div style="padding-bottom:10px;">
                <%=Resources.NCMWebContent.WEBDATA_VK_969 %>
            </div>
            <div style="padding-bottom:10px;width:80%;">
                <span class="SettingWarning"><span class="SettingWarning-Icon"></span>
                    <%=Resources.NCMWebContent.WEBDATA_VK_970 %>
                </span>
            </div>
            <div style="padding-bottom:5px;">
                <%=Resources.NCMWebContent.WEBDATA_VK_971 %>
            </div>
            <asp:Literal ID="exLiteral" runat="server"></asp:Literal>
            <div>
                <asp:Repeater runat="server" ID="repeater">
                    <HeaderTemplate>                            
                        <table border="0" cellpadding="0" cellspacing="0" width="80%">
                         <tr>
	                        <td class="header"><%=Resources.NCMWebContent.WEBDATA_VK_972 %></td>
                            <%if (IsMultipleEngines)
                              { %>
                            <td class="header"><%=Resources.NCMWebContent.WEBDATA_VK_973 %></td>
                            <%} %>
	                        <td class="header"><%=Resources.NCMWebContent.WEBDATA_VK_974 %></td>
                        </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                            <tr class="<%# Eval(@"CssClass")%>">
                                <td class="item">
                                    <a target="_blank" href="<%# Eval(@"HRef")%>"><%# Eval(@"Source")%></a>
                                </td>
                                <%if (IsMultipleEngines)
                                  { %>
                                <td class="item">
                                    <%# Eval(@"Engine")%>
                                </td>
                                <% } %>
                                <td class="item">
                                    <%# Eval(@"Path")%>
                                </td>
                            </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>                            
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </asp:Panel>
</asp:Content>

