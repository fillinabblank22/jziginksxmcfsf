using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.Controls;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCM.Common.Dal;

public partial class Orion_NCM_Admin_Settings_SCPServer : System.Web.UI.Page
{
    private readonly Log log = new Log();

    private readonly IIsWrapper isLayer;
    private readonly IDataDecryptor dataDecryptor;

    public Orion_NCM_Admin_Settings_SCPServer()
    {
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
        dataDecryptor = ServiceLocator.Container.Resolve<IDataDecryptor>();
    }

    protected bool UseThirdPartySCPServer
    {
        get { return Convert.ToBoolean(ViewState["UseThirdPartySCPServer"]); }
        set { ViewState["UseThirdPartySCPServer"] = value; }
    }

    protected bool SCPAutoRetriveIP
    {
        get { return Convert.ToBoolean(ViewState["SCPAutoRetriveIP"]); }
        set { ViewState["SCPAutoRetriveIP"] = value; }
    }

    protected bool HideUsernames
    {
        get { return Convert.ToBoolean(ViewState["HideUsernames"]); }
        set { ViewState["HideUsernames"] = value; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ValidatePageAccess();
    }

    private void ValidatePageAccess()
    {
        if (CommonHelper.IsDemoMode() ||
            !(this.Page is IBypassAccessLimitation) && !Profile.AllowAdmin && !SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR))
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_VK_554}");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_VK_657;

        chkUseThirdPartySCPServer.Text = $@"<b>{Resources.NCMWebContent.WEBDATA_VK_658}</b>";
        chkSCPAutoRetriveIP.Text = $@"<b>{Resources.NCMWebContent.WEBDATA_VK_652}</b>";

        if (!Page.IsPostBack)
        {
            using (var proxy = isLayer.GetProxy())
            {
                HideUsernames = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.HideUsernames, false, null));

                UseThirdPartySCPServer = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.UseThirdPartySCPServer, false, null));
                chkUseThirdPartySCPServer.Checked = UseThirdPartySCPServer;
                SCPAutoRetriveIP = !Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.SCPAutoRetriveIP, true, null));
                chkSCPAutoRetriveIP.Checked = SCPAutoRetriveIP;
            }

            var engines = LookupEngines();
            repeater.DataSource = engines;
            repeater.DataBind();

            EnableSCPServerAddress(chkSCPAutoRetriveIP.Checked);
        }
    }

    protected void repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        var engine = e.Item.DataItem as EngineInfo;
        if (engine != null)
        {
            var engineId = engine.EngineId;
            var hiddenEngineID = e.Item.FindControl(@"hiddenEngineID") as HiddenField;
            hiddenEngineID.Value = engineId.ToString();

            var engines = LookupEngines().ToArray();
            if (engines.Length > 1)
            {
                var litServerName = e.Item.FindControl(@"litServerName") as Literal;
                litServerName.Text =
                    $@"<tr><td colspan='3'><div style='font-weight:bold;padding-bottom:20px;{(e.Item.ItemIndex == 0 ? string.Empty : @"padding-top:20px;")}'>{engine.EngineName}</div></td></tr>";

                var td = e.Item.FindControl(@"tdHolder") as HtmlTableCell;
                if (engines.Length != (e.Item.ItemIndex + 1)) td.Style.Add(@"border-bottom", @"dashed 1px #dcdbd7");
            }

            var txtSCPUsername = e.Item.FindControl(@"txtSCPUsername") as PasswordTextBox;
            var txtSCPPassword = e.Item.FindControl(@"txtSCPPassword") as PasswordTextBox;
            var txtSCPConfirmPassword = e.Item.FindControl(@"txtSCPConfirmPassword") as PasswordTextBox;
            var txtSCPServerAddress = e.Item.FindControl(@"txtSCPServerAddress") as TextBox;
            var txtSCPRootDir = e.Item.FindControl(@"txtSCPRootDir") as TextBox;
            var customValidator = e.Item.FindControl(@"compareValidator") as CustomValidator;

            try
            {

                using (var proxy = isLayer.GetProxy())
                {
                    var defaultIP = proxy.Cirrus.Settings.RetrieveIP(engineId);
                    var scpServerAddress = proxy.Cirrus.Settings.GetSetting(Settings.SCPServerAddress, string.Empty, engineId);
                    if (string.IsNullOrEmpty(scpServerAddress))
                    {
                        scpServerAddress = defaultIP;
                    }
                    txtSCPServerAddress.Text = scpServerAddress;
                    txtSCPRootDir.Text = proxy.Cirrus.Settings.GetSetting(Settings.SCPRootDir, @"C:\SFTP_Root", engineId);

                    if (HideUsernames) txtSCPUsername.TextMode = TextBoxMode.Password;
                    txtSCPUsername.PasswordText = dataDecryptor.Decrypt(proxy.Cirrus.Settings.GetSetting(Settings.SCPUserName, @"Guest", engineId));
                    txtSCPPassword.PasswordText = dataDecryptor.Decrypt(proxy.Cirrus.Settings.GetSetting(Settings.SCPPassword, @"Guest", engineId));
                    txtSCPConfirmPassword.PasswordText = dataDecryptor.Decrypt(proxy.Cirrus.Settings.GetSetting(Settings.SCPPassword, @"Guest", engineId));
                    
                    customValidator.Attributes[@"ControlToCompareId"] = txtSCPPassword.ClientID;
                }
            }
            catch (Exception ex)
            {
                engineId = -1;
                hiddenEngineID.Value = engineId.ToString();

                txtSCPUsername.Enabled = false;
                txtSCPUsername.CssClass = @"TextFieldDisabled";
                txtSCPPassword.Enabled = false;
                txtSCPPassword.CssClass = @"TextFieldDisabled";
                txtSCPConfirmPassword.Enabled = false;
                txtSCPConfirmPassword.CssClass = @"TextFieldDisabled";
                txtSCPServerAddress.Enabled = false;
                txtSCPServerAddress.CssClass = @"TextFieldDisabled";
                txtSCPRootDir.Enabled = false;
                txtSCPRootDir.CssClass = @"TextFieldDisabled";
                customValidator.Enabled = false;

                var btnRetrieveIP = e.Item.FindControl(@"btnRetrieveIP") as LocalizableButton;
                btnRetrieveIP.Enabled = false;
                btnRetrieveIP.CssClass = @"ButtonDisabled";
                var btnValidate = e.Item.FindControl(@"btnValidate") as LocalizableButton;
                btnValidate.Enabled = false;
                btnValidate.CssClass = @"ButtonDisabled";

                var tr = e.Item.FindControl(@"exHolder") as HtmlTableRow;
                tr.Visible = true;

                var exWarn = e.Item.FindControl(@"exWarn") as Literal;
                exWarn.Text = ex.Message;
            }
        }
    }

    protected void repeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        var hiddenEngineID = e.Item.FindControl(@"hiddenEngineID") as HiddenField;
        int engineId = Convert.ToInt16(hiddenEngineID.Value);
        if (e.CommandName.Equals(@"Validate"))
        {
            var txtSCPRootDir = e.Item.FindControl(@"txtSCPRootDir") as TextBox;
            var pathValidatorHolder = e.Item.FindControl(@"pathValidatorHolder") as HtmlGenericControl;
            string error;
            if (!ValidatePath(txtSCPRootDir.Text, engineId, out error))
            {
                var literal = new Literal();
                literal.Text = string.IsNullOrEmpty(error) ? Resources.NCMWebContent.WEBDATA_VK_651 : error;
                pathValidatorHolder.Style.Add(@"color", @"red");
                pathValidatorHolder.Controls.Add(literal);
            }
            else
            {
                var literal = new Literal();
                literal.Text = Resources.NCMWebContent.WEBDATA_VK_597;
                pathValidatorHolder.Style.Add(@"color", @"green");
                pathValidatorHolder.Controls.Add(literal);
            }
        }

        if (e.CommandName.Equals(@"RetrieveIP"))
        {
            var txtSCPServerAddress = e.Item.FindControl(@"txtSCPServerAddress") as TextBox;
            using (var proxy = isLayer.GetProxy())
            {
                txtSCPServerAddress.Text = proxy.Cirrus.Settings.RetrieveIP(engineId);
            }
        }
    }

    protected void chkSCPAutoRetriveIP_OnCheckedChanged(object sender, EventArgs e)
    {
        EnableSCPServerAddress(chkSCPAutoRetriveIP.Checked);
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        using (var proxy = isLayer.GetProxy())
        {
            Dictionary<int, SCPServerSettings> results;
            if (TryGetSCPServerSettings(out results))
            {
                foreach (var result in results)
                {
                    proxy.Cirrus.Settings.SaveSetting(Settings.SCPUserName, result.Value.SCPUsername, result.Key);
                    proxy.Cirrus.Settings.SaveSetting(Settings.SCPPassword, result.Value.SCPPassword, result.Key);
                    if (chkSCPAutoRetriveIP.Checked) proxy.Cirrus.Settings.SaveSetting(Settings.SCPServerAddress, result.Value.SCPServerAddress, result.Key);
                    proxy.Cirrus.Settings.SaveSetting(Settings.SCPRootDir, result.Value.SCPRootDir, result.Key);
                }

                if (UseThirdPartySCPServer != chkUseThirdPartySCPServer.Checked)
                    proxy.Cirrus.Settings.SaveSetting(Settings.UseThirdPartySCPServer, chkUseThirdPartySCPServer.Checked, null);
                if (SCPAutoRetriveIP != chkSCPAutoRetriveIP.Checked)
                    proxy.Cirrus.Settings.SaveSetting(Settings.SCPAutoRetriveIP, !chkSCPAutoRetriveIP.Checked, null);
            }
            else
            {
                return;
            }
        }

        var btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        var btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }

    private bool TryGetSCPServerSettings(out Dictionary<int, SCPServerSettings> results)
    {
        var isValid = true;

        results = new Dictionary<int, SCPServerSettings>();

        foreach (RepeaterItem item in repeater.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                var engineId = string.Empty;
                var scpUsername = string.Empty;
                var scpPassword = string.Empty;
                var scpServerAddress = string.Empty;
                var scpRootDir = string.Empty;

                var hiddenEngineID = item.FindControl(@"hiddenEngineID") as HiddenField;
                engineId = hiddenEngineID.Value;
                if (Convert.ToInt16(engineId) < 0) continue;

                var txtSCPUsername = item.FindControl(@"txtSCPUsername") as PasswordTextBox;
                scpUsername = txtSCPUsername.PasswordText;

                var txtSCPPassword = item.FindControl(@"txtSCPPassword") as PasswordTextBox;
                scpPassword = txtSCPPassword.PasswordText;

                var txtSCPServerAddress = item.FindControl(@"txtSCPServerAddress") as TextBox;
                scpServerAddress = txtSCPServerAddress.Text;

                var txtSCPRootDir = item.FindControl(@"txtSCPRootDir") as TextBox;
                scpRootDir = txtSCPRootDir.Text;

                var pathValidatorHolder = item.FindControl(@"pathValidatorHolder") as HtmlGenericControl;
                string error;
                if (!ValidatePath(scpRootDir, Convert.ToInt16(engineId), out error))
                {
                    var literal = new Literal();
                    literal.Text = string.IsNullOrEmpty(error) ? Resources.NCMWebContent.WEBDATA_VK_651 : error;
                    pathValidatorHolder.Style.Add(@"color", @"red");
                    pathValidatorHolder.Controls.Add(literal);
                    isValid = false;
                }

                if (isValid)
                {
                    var scpServerSettings = new SCPServerSettings(scpUsername, scpPassword, scpServerAddress, scpRootDir);
                    results.Add(Convert.ToInt16(engineId), scpServerSettings);
                }
            }
        }

        return isValid;
    }

    private void EnableSCPServerAddress(bool bEnable)
    {
        foreach (RepeaterItem item in repeater.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                var hiddenEngineID = item.FindControl(@"hiddenEngineID") as HiddenField;
                if (Convert.ToInt16(hiddenEngineID.Value) < 0) continue;

                var txtSCPServerAddress = item.FindControl(@"txtSCPServerAddress") as TextBox;
                txtSCPServerAddress.Enabled = bEnable;
                var btnRetrieveIP = item.FindControl(@"btnRetrieveIP") as LocalizableButton;
                btnRetrieveIP.Enabled = bEnable;
                btnRetrieveIP.CssClass = bEnable ? @"ButtonEnabled" : @"ButtonDisabled";
            }
        }
    }

    private bool ValidatePath(string path, int engineId, out string error)
    {
        try
        {
            using (var proxy = isLayer.GetProxy())
            {
                error = string.Empty;
                return proxy.Cirrus.Settings.ValidatePathWithoutImpersonation(path, engineId);
            }
        }
        catch (Exception ex)
        {
            error = ex.Message;
            log.Error(ex);
            return false;
        }
    }

    private IEnumerable<EngineInfo> LookupEngines()
    {
        var enginesDal = ServiceLocator.Container.Resolve<ISwisEnginesDal>();
        return enginesDal.GetAllPollingEngines();
    }

    private class SCPServerSettings
    {
        public SCPServerSettings(string scpUsername, string scpPassword, string scpServerAddress, string scpRootDir)
        {
            SCPUsername = scpUsername;
            SCPPassword = scpPassword;
            SCPServerAddress = scpServerAddress;
            SCPRootDir = scpRootDir;
        }

        public string SCPUsername { get; }

        public string SCPPassword { get; }

        public string SCPServerAddress { get; }

        public string SCPRootDir { get; }
    }
}
