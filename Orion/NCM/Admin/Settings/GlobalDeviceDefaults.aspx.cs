using System;
using System.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI.WebControls;
using System.Text;
using System.Web;
using System.Web.UI;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.NCM.Common.BusinessLayer.Interfaces;
using SolarWinds.NCM.Common.Help;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web;

public partial class Orion_NCM_Admin_Settings_GlobalDeviceDefaults : Page
{
    private readonly IIsWrapper isLayer;
    private readonly IDataDecryptor dataDecryptor;
    private readonly INcmBusinessLayerProxyHelper blProxy;

    [Localizable(false)] private static string[] globalSettings = {
        "GlobalConfigRequestProtocol",
        "GlobalConfigTransferProtocol",
        "GlobalEnableLevel",
        "GlobalEnablePassword",
        "GlobalExecProtocol",
        "GlobalPassword",
        "GlobalSSHPort",
        "GlobalTelnetPort",
        "GlobalUsername"
    };

    public Orion_NCM_Admin_Settings_GlobalDeviceDefaults()
    {
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
        dataDecryptor = ServiceLocator.Container.Resolve<IDataDecryptor>();
        blProxy = ServiceLocator.Container.Resolve<INcmBusinessLayerProxyHelper>();
    }

    protected bool HideUsernames
    {
        get
        {
            using (var proxy = isLayer.GetProxy())
            {
                var hideUsernames = proxy.Cirrus.Settings.GetSetting(Settings.HideUsernames, @"False",null);
                return Convert.ToBoolean(hideUsernames);
            }
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ValidatePageAccess();

        var results = blProxy.CallMain(x => x.GetCommandProtocolsWithoutGlobalProtocols());
        CommonHelper.PopulateDropDownList(results.ExecuteScriptProtocols, ddlExCfgScrProtocol);
        CommonHelper.PopulateDropDownList(results.RequestConfigProtocols, ddlCfgReqProtocol);
    }

    private void ValidatePageAccess()
    {
        if (CommonHelper.IsDemoMode() ||
            !(Page is IBypassAccessLimitation) && !Profile.AllowAdmin && !SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR))
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_VK_554}");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_VM_149;

        if (!Page.IsPostBack)
            Initializer();
    }

    protected void ddlCfgReqProtocol_SelectedIndexChanged(object sender, EventArgs e)
    {
        CommonHelper.InitializeTransferConfigProtocol(ddlTransfCfgProtocol, ddlCfgReqProtocol.SelectedItem);
    }
    
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        SubmitSettings();
        var btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }
    
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        var btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }

    protected string LearnMore
    {
        get
        {
            var helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
            return
                $@"<a href=""{helpHelperWrap.GetHelpUrl("OrionNCMPHGlobalMacrosLearnMore")}"" target=""_blank"" style=""color:#336699;"">{Resources.NCMWebContent.WEBDATA_VK_692}</a>";
        }
    }

    #region private methods

    private void SubmitSettings()
    {
        SaveSettings();

        if (cbResetToGlobalCommunication.Checked ||
            cbResetToGlobalLogin.Checked ||
            cbResetToGlobalTransfer.Checked)
        {
            ResetToGlobal();
        }
    }

    private void SaveSettings()
    {
        using (var proxy = isLayer.GetProxy())
        {
            //device login info
            proxy.Cirrus.Settings.SaveSetting(Settings.GlobalUsername, txtUsername.PasswordText, null);

            if (!txtPasswordNew.Text.Equals(GetDecryptedValue(proxy.Cirrus.Settings.GetSetting(Settings.GlobalPassword,"",null),true)))
                proxy.Cirrus.Settings.SaveSetting(Settings.GlobalPassword, txtPasswordNew.Text, null); ;

            proxy.Cirrus.Settings.SaveSetting(Settings.GlobalEnableLevel, ddlEnableLevel.SelectedValue, null);
            
            if (!txtEnablePasswordNew.Text.Equals(GetDecryptedValue(proxy.Cirrus.Settings.GetSetting(Settings.GlobalEnablePassword, "", null), true)))
                proxy.Cirrus.Settings.SaveSetting(Settings.GlobalEnablePassword, txtEnablePasswordNew.Text, null); 
            
            //communication transfer protocol
            proxy.Cirrus.Settings.SaveSetting(Settings.GlobalExecProtocol, ddlExCfgScrProtocol.SelectedValue, null);
            proxy.Cirrus.Settings.SaveSetting(Settings.GlobalConfigRequestProtocol, ddlCfgReqProtocol.SelectedValue, null);
            proxy.Cirrus.Settings.SaveSetting(Settings.GlobalConfigTransferProtocol, ddlTransfCfgProtocol.SelectedValue, null);
            
            //transfer ports
            proxy.Cirrus.Settings.SaveSetting(Settings.GlobalTelnetPort, txtTelnetPort.Text, null);
            proxy.Cirrus.Settings.SaveSetting(Settings.GlobalSSHPort, txtSSHPort.Text, null);
        }
    }

    private void ResetToGlobal()
    { 
        using (var proxy = isLayer.GetProxy())
        {
            if (cbResetToGlobalTransfer.Checked)
            {
                proxy.Cirrus.Settings.SetGlobalMacroForAllNodes(@"SSHPort", @"${GlobalSSHPort}");
                proxy.Cirrus.Settings.SetGlobalMacroForAllNodes(@"TelnetPort", @"${GlobalTelnetPort}");
            }

            if (cbResetToGlobalCommunication.Checked)
            {
                proxy.Cirrus.Settings.SetGlobalMacroForAllNodes(@"ExecProtocol", @"${GlobalExecProtocol}");
                proxy.Cirrus.Settings.SetGlobalMacroForAllNodes(@"CommandProtocol", @"${GlobalConfigRequestProtocol}");
                proxy.Cirrus.Settings.SetGlobalMacroForAllNodes(@"TransferProtocol", @"${GlobalConfigTransferProtocol}");
            }

            if (cbResetToGlobalLogin.Checked)
            {
                proxy.Cirrus.Settings.SetGlobalMacroForAllNodes(@"Username", @"${GlobalUserName}");
                proxy.Cirrus.Settings.SetGlobalMacroForAllNodes(@"Password", @"${GlobalPassword}");
                proxy.Cirrus.Settings.SetGlobalMacroForAllNodes(@"EnableLevel", @"${GlobalEnableLevel}");
                proxy.Cirrus.Settings.SetGlobalMacroForAllNodes(@"EnablePassword", @"${GlobalEnablePassword}");
            }

        }
    }

    private void Initializer()
    {
        var items =  GetGlobalSettingsValue();

        if (items != null)
        {
            var hideSensetiveData = HideUsernames;

            //device login info
            txtUsername.TextMode = hideSensetiveData ? TextBoxMode.Password : TextBoxMode.SingleLine;
            txtUsername.PasswordText = items.ContainsKey(@"GlobalUsername") ? dataDecryptor.Decrypt(items[@"GlobalUsername"]) : string.Empty;

            txtPasswordNew.TextMode = TextBoxMode.Password;
            txtPasswordNew.Attributes[@"value"] = items.ContainsKey(@"GlobalPassword") ? GetDecryptedValue(items[@"GlobalPassword"], true) : string.Empty;

            txtPasswordConfirmNew.TextMode = TextBoxMode.Password;
            txtPasswordConfirmNew.Attributes[@"value"] = items.ContainsKey(@"GlobalPassword") ? GetDecryptedValue(items[@"GlobalPassword"], true) : string.Empty;

            txtEnablePasswordNew.TextMode = TextBoxMode.Password;
            txtEnablePasswordNew.Attributes[@"value"] = items.ContainsKey(@"GlobalEnablePassword") ? GetDecryptedValue(items[@"GlobalEnablePassword"], true) : string.Empty;

            ddlEnableLevel.SelectedValue = items.ContainsKey(@"GlobalEnableLevel") ? items[@"GlobalEnableLevel"] : string.Empty;

            //communication transfer protocol
            ddlExCfgScrProtocol.SelectedValue = items.ContainsKey(@"GlobalExecProtocol") ? items[@"GlobalExecProtocol"] : string.Empty;
            ddlCfgReqProtocol.SelectedValue = items.ContainsKey(@"GlobalConfigRequestProtocol") ? items[@"GlobalConfigRequestProtocol"] : string.Empty;
            CommonHelper.InitializeTransferConfigProtocol(ddlTransfCfgProtocol, ddlCfgReqProtocol.SelectedItem);
            ddlTransfCfgProtocol.SelectedValue = items.ContainsKey(@"GlobalConfigTransferProtocol") ? items[@"GlobalConfigTransferProtocol"] : string.Empty;

            //transfer ports
            txtTelnetPort.Text = items.ContainsKey(@"GlobalTelnetPort") ? items[@"GlobalTelnetPort"] : string.Empty;
            txtSSHPort.Text = items.ContainsKey(@"GlobalSSHPort") ? items[@"GlobalSSHPort"] : string.Empty;
        }
        else
        {
            CommonHelper.InitializeTransferConfigProtocol(ddlTransfCfgProtocol, ddlCfgReqProtocol.SelectedItem);
        }

        cbResetToGlobalCommunication.Checked = false;
        cbResetToGlobalLogin.Checked = false;
        cbResetToGlobalTransfer.Checked = false;
        
    }

    private IReadOnlyDictionary<string, string> GetGlobalSettingsValue()
    {
        DataTable dt;
        var res = new Dictionary<string, string>();
        using (var proxy = isLayer.GetProxy())
        {
            var swql = CreateSwqlSelect();
            dt = proxy.Query(swql);
        }

        if (dt != null)
        {
            foreach (DataRow row in dt.Rows)
            {
                var key = row[0]?.ToString();
                if (!string.IsNullOrEmpty(key))
                {
                    var value = row[1] == null ? string.Empty : row[1].ToString();

                    res[key] = value;
                }
            }
        }

        return res;
    }

    private string CreateSwqlSelect()
    {
        var swql = @"select S.SettingName, S.SettingValue from Cirrus.GlobalSettings S where {0}";

        var sb = new StringBuilder(); 
        var first = true;
        foreach (var item in globalSettings)
        {
            if (first)
                first = false;
            else
                sb.Append(@" or ");

            
            sb.AppendFormat(@"(S.SettingName = '{0}')", item);
        }

        if (sb.Length == 0)
            sb.Append(@"1=2");

        return string.Format(swql, sb);
    }

    private string GetDecryptedValue(string value, bool secure)
    {
        var val = dataDecryptor.Decrypt(value);
        return secure ? new string('?', val.Length) : val;
    }

    #endregion
}
