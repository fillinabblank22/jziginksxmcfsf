using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCM.Contracts;

public partial class Orion_NCM_Admin_Settings_Security : System.Web.UI.Page
{
    private ISWrapper isLayer;

    protected bool HideCommunityStrings
    {
        get { return Convert.ToBoolean(ViewState["HideCommunityStrings"]); }
        set { ViewState["HideCommunityStrings"] = value; }
    }

    protected bool HideUsernames
    {
        get { return Convert.ToBoolean(ViewState["HideUsernames"]); }
        set { ViewState["HideUsernames"] = value; }
    }

    protected string ConnectivityLevel
    {
        get { return Convert.ToString(ViewState["ConnectivityLevel"]); }
        set { ViewState["ConnectivityLevel"] = value; }
    }

    protected bool ComplianceMgmtOnlyForAdmins
    {
        get { return Convert.ToBoolean(ViewState["ComplianceMgmtOnlyForAdmins"]); }
        set { ViewState["ComplianceMgmtOnlyForAdmins"] = value; }
    }

    protected bool BypassApprovalForAutoRemediationScript
    {
        get { return Convert.ToBoolean(ViewState["BypassApprovalForAutoRemediationScript"]); }
        set { ViewState["BypassApprovalForAutoRemediationScript"] = value; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ValidatePageAccess();
    }

    private void ValidatePageAccess()
    {
        if (CommonHelper.IsDemoMode() || 
            !(this.Page is IBypassAccessLimitation) && !Profile.AllowAdmin && !SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR))
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_VK_554}");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_VK_498;
        rbLevelThree.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_507, @"<span style='color:Gray;font-style:italic;'>", @"</span>");

        isLayer = new ISWrapper();

        if (!Page.IsPostBack)
        {
            using (var proxy = isLayer.Proxy)
            {
                HideCommunityStrings = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.HideCommunityStrings, false, null));
                chkHideCommunityStrings.Checked = HideCommunityStrings;
                HideUsernames = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.HideUsernames, false, null));
                chkHideUsernames.Checked = HideUsernames;

                ConnectivityLevel = proxy.Cirrus.Settings.GetSetting(Settings.ConnectivityLevel, ConnectivityLevels.Default, null);
                if (ConnectivityLevel.Equals(ConnectivityLevels.Level1))
                {
                    rbLevelOne.Checked = true;
                }
                else
                {
                    rbLevelThree.Checked = true;
                }

                ComplianceMgmtOnlyForAdmins = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.ComplianceMgmtOnlyForAdmins, false, null));
                chkComplianceMgmtOnlyForAdmins.Checked = ComplianceMgmtOnlyForAdmins;

                BypassApprovalForAutoRemediationScript= Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.BypassApprovalForAutoRemediationScript, false, null));
                chkBypassApprovalForAutoRemediationScript.Checked = BypassApprovalForAutoRemediationScript;
            }
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        using (var proxy = isLayer.Proxy)
        {
            if (HideCommunityStrings != chkHideCommunityStrings.Checked)
                proxy.Cirrus.Settings.SaveSetting(Settings.HideCommunityStrings, chkHideCommunityStrings.Checked, null);
            if (HideUsernames != chkHideUsernames.Checked)
                proxy.Cirrus.Settings.SaveSetting(Settings.HideUsernames, chkHideUsernames.Checked, null);

            var connLevel = ConnectivityLevels.Level1;
            if (rbLevelThree.Checked)
            {
                connLevel = ConnectivityLevels.Level3;
            }
            if (!ConnectivityLevel.Equals(connLevel))
            {
                proxy.Cirrus.Settings.SaveSetting(Settings.ConnectivityLevel, connLevel, null);
            }

            if (ComplianceMgmtOnlyForAdmins != chkComplianceMgmtOnlyForAdmins.Checked)
                proxy.Cirrus.Settings.SaveSetting(Settings.ComplianceMgmtOnlyForAdmins, chkComplianceMgmtOnlyForAdmins.Checked, null);

            if (BypassApprovalForAutoRemediationScript != chkBypassApprovalForAutoRemediationScript.Checked)
                proxy.Cirrus.Settings.SaveSetting(Settings.BypassApprovalForAutoRemediationScript, chkBypassApprovalForAutoRemediationScript.Checked, null);
        }

        var btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        var btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }
}
