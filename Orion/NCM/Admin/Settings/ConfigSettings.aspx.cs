using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;
using NCMContracts = SolarWinds.NCM.Contracts.Constants;
using SolarWinds.NCM.Common.Dal;

public partial class Orion_NCM_Admin_Settings_ConfigSettings : System.Web.UI.Page
{
    private readonly Log log = new Log();

    private ISWrapper isLayer;

    protected int ConcurrentDownloads
    {
        get { return Convert.ToInt16(ViewState["ConcurrentDownloads"]); }
        set { ViewState["ConcurrentDownloads"] = value; }
    }

    protected int SNMPConfigTransferTimeout
    {
        get { return Convert.ToInt16(ViewState["SNMPConfigTransferTimeout"]); }
        set { ViewState["SNMPConfigTransferTimeout"] = value; }
    }

    protected int MinimumConfigLength
    {
        get { return Convert.ToInt16(ViewState["MinimumConfigLength"]); }
        set { ViewState["MinimumConfigLength"] = value; }
    }

    protected int ComparisonOutputWidth
    {
        get { return Convert.ToInt16(ViewState["ComparisonOutputWidth"]); }
        set { ViewState["ComparisonOutputWidth"] = value; }
    }

    protected bool FullSideBySideComparison
    {
        get { return Convert.ToBoolean(ViewState["FullSideBySideComparison"]); }
        set { ViewState["FullSideBySideComparison"] = value; }
    }

    protected bool NumberOfLinesComparison
    {
        get { return Convert.ToBoolean(ViewState["NumberOfLinesComparison"]); }
        set { ViewState["NumberOfLinesComparison"] = value; }
    }

    protected int NumberOfLines
    {
        get { return Convert.ToInt16(ViewState["NumberOfLines"]); }
        set { ViewState["NumberOfLines"] = value; }
    }

    protected bool EnableConfigMirror
    {
        get { return Convert.ToBoolean(ViewState["EnableConfigMirror"]); }
        set { ViewState["EnableConfigMirror"] = value; }
    }

    protected bool KeepMultipleRevisions
    {
        get { return Convert.ToBoolean(ViewState["KeepMultipleRevisions"]); }
        set { ViewState["KeepMultipleRevisions"] = value; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ValidatePageAccess();
    }

    private void ValidatePageAccess()
    {
        if (CommonHelper.IsDemoMode() ||
            !(Page is IBypassAccessLimitation) && !Profile.AllowAdmin && !SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR))
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_VK_554}");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_VK_564;

        isLayer = new ISWrapper();

        if (!Page.IsPostBack)
        {
            using (var proxy = isLayer.Proxy)
            {
                ConcurrentDownloads = Convert.ToInt16(proxy.Cirrus.Settings.GetSetting(Settings.ConcurrentDownloads, 25, null));
                slConcurrentDownloads.Value = ConcurrentDownloads;
                SNMPConfigTransferTimeout = Convert.ToInt16(proxy.Cirrus.Settings.GetSetting(Settings.SNMPConfigTransferTimeout, 4, null));
                slSNMPConfigTransferTimeout.Value = SNMPConfigTransferTimeout;
                MinimumConfigLength = Convert.ToInt16(proxy.Cirrus.Settings.GetSetting(Settings.MinimumConfigLength, 11, null));
                slMinimumConfigLength.Value = MinimumConfigLength;

                ComparisonOutputWidth = Convert.ToInt16(proxy.Cirrus.Settings.GetSetting(Settings.ComparisonOutputWidth, 250, null));
                slComparisonOutputWidth.Value = ComparisonOutputWidth;
                FullSideBySideComparison = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.FullSideBySideComparison, false, null));
                rbFullSideBySideComparison.Checked = FullSideBySideComparison;
                NumberOfLinesComparison = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.NumberOfLinesComparison, true, null));
                rbNumberOfLinesComparison.Checked = NumberOfLinesComparison;
                NumberOfLines = Convert.ToInt16(proxy.Cirrus.Settings.GetSetting(Settings.NumberOfLines, 5, null));
                slNumberOfLines.Value = NumberOfLines;

                EnableConfigMirror = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.EnableConfigMirror, true, null));
                chkEnableConfigMirror.Checked = EnableConfigMirror;
                ConfigMirrorSettings.Visible = EnableConfigMirror;

                KeepMultipleRevisions = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.KeepMultipleRevisions, true, null));
                chkKeepMultipleRevisions.Checked = KeepMultipleRevisions;

                repeater.DataSource = LookupEngines();
                repeater.DataBind();
            }
        }
    }

    protected void chkEnableConfigMirror_CheckedChanged(object sender, EventArgs e)
    {
        ConfigMirrorSettings.Visible = chkEnableConfigMirror.Checked;
    }

    protected void repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        var engine = e.Item.DataItem as EngineInfo;
        if (engine != null)
        {
            var engineId = engine.EngineId;
            var hiddenEngineID = e.Item.FindControl(@"hiddenEngineID") as HiddenField;
            hiddenEngineID.Value = engineId.ToString();

            var litServerName = e.Item.FindControl(@"litServerName") as Literal;
            var chkUseFirstPoller = e.Item.FindControl(@"chkUseFirstPoller") as CheckBox;

            var engines = LookupEngines().ToArray();
            if (engines.Length == 1)
            {
                litServerName.Text = string.Empty;
                chkUseFirstPoller.Visible = false;
            }
            else
            {
                var htmlFormat = @"<tr><td style='padding-top:20px;'><b>{0}</b></td></tr>";
                litServerName.Text = string.Format(htmlFormat,engine.EngineName);

                var tdHolder = e.Item.FindControl(@"tdHolder") as HtmlTableCell;
                if (e.Item.ItemIndex > 0) tdHolder.Style.Add(@"border-bottom", @"dashed 1px #dcdbd7");

                if (e.Item.ItemIndex > 0 && engines.Length > 1)
                {
                    chkUseFirstPoller.Visible = false;
                }
            }

            var txtConfigArchiveDirectory = e.Item.FindControl(@"txtConfigArchiveDirectory") as TextBox;
            var txtConfigMirrorTemplate = e.Item.FindControl(@"txtConfigMirrorTemplate") as TextBox;
            try
            {
                using (var proxy = isLayer.Proxy)
                {
                    var defaultPath = proxy.Cirrus.Settings.GetAppDataPath(engineId);
                    txtConfigArchiveDirectory.Text = Convert.ToString(proxy.Cirrus.Settings.GetSetting(Settings.ConfigArchiveDirectory, System.IO.Path.Combine(defaultPath, "NCM\\Config-Archive"), engineId));
                    txtConfigMirrorTemplate.Text = Convert.ToString(proxy.Cirrus.Settings.GetSetting(Settings.ConfigMirrorTemplate, NCMContracts.DefaultExportConfigPath, engineId));
                }
            }
            catch (Exception ex)
            {
                engineId = -1;
                hiddenEngineID.Value = engineId.ToString();

                txtConfigArchiveDirectory.Enabled = false;
                txtConfigArchiveDirectory.CssClass = @"TextFieldDisabled";
                txtConfigMirrorTemplate.Enabled = false;
                txtConfigMirrorTemplate.CssClass = @"TextFieldDisabled";

                var btnValidate = e.Item.FindControl(@"btnValidate") as LocalizableButton;
                btnValidate.Enabled = false;
                btnValidate.CssClass = @"ButtonDisabled";

                var tr = e.Item.FindControl(@"exHolder") as HtmlTableRow;
                tr.Visible = true;

                var exWarn = e.Item.FindControl(@"exWarn") as Literal;
                exWarn.Text = ex.Message;
            }
        }
    }

    protected void chkUseFirstPoller_CheckedChanged(object sender, EventArgs e)
    {
        var index = 0;
        foreach (RepeaterItem item in repeater.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                if (index > 0)
                    item.Visible = !(sender as CheckBox).Checked;

                index++;
            }
        }
    }

    private bool UseFirstPollerInformationForAll()
    {
        foreach (RepeaterItem item in repeater.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                var chkUseFirstPoller = item.FindControl(@"chkUseFirstPoller") as CheckBox;
                return chkUseFirstPoller.Checked;
            }
        }

        return false;
    }

    protected void repeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName.Equals(@"Validate"))
        {
            var validatorHolder = e.Item.FindControl(@"validatorHolder") as HtmlGenericControl;
            if (UseFirstPollerInformationForAll())
            {
                Dictionary<int, ConfigArchiveSettings> results;
                if (TryGetConfigArchiveSettings(out results, true))
                {
                    var literal = new Literal();
                    literal.Text = Resources.NCMWebContent.WEBDATA_VK_597;
                    validatorHolder.Style.Add(@"color", @"green");
                    validatorHolder.Controls.Add(literal);
                }
            }
            else
            {
                var hiddenEngineID = e.Item.FindControl(@"hiddenEngineID") as HiddenField;
                var txtConfigArchiveDirectory = e.Item.FindControl(@"txtConfigArchiveDirectory") as TextBox;

                string error;
                if (!ValidatePath(txtConfigArchiveDirectory.Text, Convert.ToInt16(hiddenEngineID.Value), out error))
                {
                    var literal = new Literal();
                    literal.Text = string.IsNullOrEmpty(error) ? Resources.NCMWebContent.WEBDATA_VK_595 : error;
                    validatorHolder.Style.Add(@"color", @"red");
                    validatorHolder.Controls.Add(literal);
                }
                else
                {
                    var literal = new Literal();
                    literal.Text = Resources.NCMWebContent.WEBDATA_VK_597;
                    validatorHolder.Style.Add(@"color", @"green");
                    validatorHolder.Controls.Add(literal);
                }
            }
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (slComparisonOutputWidth.Value % 2 != 0)
        {
            lblCompOutWidthNotif.Visible = true;
        }
        else
        {
            using (var proxy = isLayer.Proxy)
            {
                if (EnableConfigMirror != chkEnableConfigMirror.Checked)
                    proxy.Cirrus.Settings.SaveSetting(Settings.EnableConfigMirror, chkEnableConfigMirror.Checked, null);
                if (KeepMultipleRevisions != chkKeepMultipleRevisions.Checked)
                    proxy.Cirrus.Settings.SaveSetting(Settings.KeepMultipleRevisions, chkKeepMultipleRevisions.Checked, null);
                if (chkEnableConfigMirror.Checked)
                {
                    var useFirstPoller = UseFirstPollerInformationForAll();

                    Dictionary<int, ConfigArchiveSettings> results;
                    if (TryGetConfigArchiveSettings(out results, useFirstPoller))
                    {
                        foreach (var kvp in results)
                        {
                            proxy.Cirrus.Settings.SaveSetting(Settings.ConfigArchiveDirectory, kvp.Value.ConfigArchiveDir, kvp.Key);
                            proxy.Cirrus.Settings.SaveSetting(Settings.ConfigMirrorTemplate, kvp.Value.ConfigMirrorTemplate, kvp.Key);
                        }
                    }
                    else { return; }
                }

                if (ConcurrentDownloads != slConcurrentDownloads.Value)
                    proxy.Cirrus.Settings.SaveSetting(Settings.ConcurrentDownloads, slConcurrentDownloads.Value, null);
                if (SNMPConfigTransferTimeout != slSNMPConfigTransferTimeout.Value)
                    proxy.Cirrus.Settings.SaveSetting(Settings.SNMPConfigTransferTimeout, slSNMPConfigTransferTimeout.Value, null);
                if (MinimumConfigLength != slMinimumConfigLength.Value)
                    proxy.Cirrus.Settings.SaveSetting(Settings.MinimumConfigLength, slMinimumConfigLength.Value, null);

                if (ComparisonOutputWidth != slComparisonOutputWidth.Value)
                    proxy.Cirrus.Settings.SaveSetting(Settings.ComparisonOutputWidth, slComparisonOutputWidth.Value, null);
                if (FullSideBySideComparison != rbFullSideBySideComparison.Checked)
                    proxy.Cirrus.Settings.SaveSetting(Settings.FullSideBySideComparison, rbFullSideBySideComparison.Checked, null);
                if (NumberOfLinesComparison != rbNumberOfLinesComparison.Checked)
                    proxy.Cirrus.Settings.SaveSetting(Settings.NumberOfLinesComparison, rbNumberOfLinesComparison.Checked, null);
                if (NumberOfLines != slNumberOfLines.Value)
                    proxy.Cirrus.Settings.SaveSetting(Settings.NumberOfLines, slNumberOfLines.Value, null);
            }

            var btn = sender as LocalizableButton;
            Page.Response.Redirect(btn.CommandArgument);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        var btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }

    private bool ValidatePath(string path, int engineId, out string error)
    {
        try
        {
            using (var proxy = isLayer.Proxy)
            {
                error = string.Empty;
                return proxy.Cirrus.Settings.ValidatePath(path, engineId);
            }
        }
        catch (Exception ex)
        {
            error = ex.Message;
            log.Error(ex);
            return false;
        }
    }

    private bool TryGetConfigArchiveSettings(out Dictionary<int, ConfigArchiveSettings> results, bool useFirstPoller)
    {
        var isValid = true;
        results = new Dictionary<int, ConfigArchiveSettings>();

        var engineId = string.Empty;
        var configArchiveDirectory = string.Empty;
        var configMirrorTemplate = string.Empty;

        HtmlGenericControl validatorHolder = null;

        var index = 0;
        foreach (RepeaterItem item in repeater.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                var hiddenEngineID = item.FindControl(@"hiddenEngineID") as HiddenField;
                engineId = hiddenEngineID.Value;
                if (Convert.ToInt16(engineId) < 0) continue;

                TextBox txtConfigMirrorTemplate;
                TextBox txtConfigArchiveDirectory;
                if (index == 0 && useFirstPoller)
                {
                    txtConfigArchiveDirectory = item.FindControl(@"txtConfigArchiveDirectory") as TextBox;
                    configArchiveDirectory = txtConfigArchiveDirectory.Text;

                    txtConfigMirrorTemplate = item.FindControl(@"txtConfigMirrorTemplate") as TextBox;
                    configMirrorTemplate = txtConfigMirrorTemplate.Text;

                    validatorHolder = item.FindControl(@"validatorHolder") as HtmlGenericControl;
                }
                else if (!useFirstPoller)
                {
                    txtConfigArchiveDirectory = item.FindControl(@"txtConfigArchiveDirectory") as TextBox;
                    configArchiveDirectory = txtConfigArchiveDirectory.Text;

                    txtConfigMirrorTemplate = item.FindControl(@"txtConfigMirrorTemplate") as TextBox;
                    configMirrorTemplate = txtConfigMirrorTemplate.Text;

                    validatorHolder = item.FindControl(@"validatorHolder") as HtmlGenericControl;
                }
                index++;

                string error;
                if (!ValidatePath(configArchiveDirectory, Convert.ToInt16(engineId), out error))
                {
                    var engines = LookupEngines();
                    var engine = engines.FirstOrDefault(x => x.EngineId.ToString() == engineId);
                    var literal = new Literal();
                    if (useFirstPoller)
                        literal.Text = string.IsNullOrEmpty(error) ? string.Format(@"<div>" + Resources.NCMWebContent.WEBDATA_VK_596 + @"</div>", engine.EngineName) : error;
                    else
                        literal.Text = string.IsNullOrEmpty(error) ? Resources.NCMWebContent.WEBDATA_VK_595 : error;
                    if (validatorHolder != null)
                    {
                        validatorHolder.Style.Add(@"color", @"red");
                        validatorHolder.Controls.Add(literal);
                    }
                    isValid = false;
                }

                if (isValid)
                {
                    var configArchiveSettings = new ConfigArchiveSettings(configArchiveDirectory, configMirrorTemplate);
                    results.Add(Convert.ToInt16(engineId), configArchiveSettings);
                }
            }
        }

        return isValid;
    }

    private IEnumerable<EngineInfo> LookupEngines()
    {
        var enginesDal = ServiceLocator.Container.Resolve<ISwisEnginesDal>();
        return enginesDal.GetAllPollingEngines();
    }

    private class ConfigArchiveSettings
    {
        public ConfigArchiveSettings(string configArchiveDir, string configMirrorTemplate)
        {
            ConfigArchiveDir = configArchiveDir;
            ConfigMirrorTemplate = configMirrorTemplate;
        }

        public string ConfigArchiveDir { get; }

        public string ConfigMirrorTemplate { get; }
    }
}
