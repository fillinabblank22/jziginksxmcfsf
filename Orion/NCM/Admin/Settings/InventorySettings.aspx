<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="InventorySettings.aspx.cs" Inherits="Orion_NCM_Admin_Settings_InventorySettings" Title="Untitled Page" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/Controls/Slider.ascx" TagPrefix="orion" TagName="Slider" %>
<%@ Register Src="~/Orion/NCM/Controls/InventorySettings.ascx" TagPrefix="ncm" TagName="InventorySettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
    <orion:Include ID="Include1" runat="server" File="Admin.css" />
    <orion:Include ID="Include2" runat="server" File="Discovery.css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="Settings.css" />
    <style type="text/css">
        tr.slider > td {
            text-align: left;
            white-space: nowrap;
            padding: 10px;
        }
	</style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMPHNodeInventorySettings" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <asp:Panel ID="ContentContainer" CssClass="ContentContainer" runat="server">
        <div class="SettingContainer">
            <div class="SettingBlockDescription" style="padding-top:0px;"><%=Resources.NCMWebContent.WEBDATA_VK_509 %></div>
            <div>
                <ncm:InventorySettings runat="server" ID="ncmInventorySettings"/>
            </div>
            
            <div style="border-bottom:#e6e6dd 1px solid;padding-top:20px;"></div>
            
            <div style="padding-top:20px;">
                <table cellpadding="0" cellspacing="0" style="background-color:#e4f1f8;">
                    <tr class="slider">
                        <orion:Slider runat="server" ID="slConcurrentNodes" nowrap="nowrap" TableCellCount="3" Label="<%$ Resources: NCMWebContent, WEBDATA_VK_549 %>" RangeFrom="1" RangeTo="100" Unit="" ValidateRange="true" Step="1" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_550 %>"/>
                    </tr>
                </table>
            </div>
            <div style="padding-top:20px;">
                <span class="SettingSuggestion"><span class="SettingSuggestion-Icon"></span>
                    <%=Resources.NCMWebContent.WEBDATA_VK_551 %>
                </span>
            </div>
                        
            <div style="border-bottom:#e6e6dd 1px solid;padding-top:20px;"></div>
            
            <table cellpadding="0" cellspacing="0" style="width:100%">
                <tr>
                    <td class="column1" nowrap="nowrap"><asp:CheckBox ID="chkExtendedVLANsInventory" runat="server" /></td>
                </tr>
                <tr>
                    <td class="column1" nowrap="nowrap"><asp:CheckBox ID="chkDisableInventoryLookup" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_553 %>" /></td>
                </tr>
            </table>
        </div>
        
        <div style="padding-top:20px;">
            <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" 
            OnClick="btnSubmit_Click" DisplayType="Primary"
            CommandArgument="~/Orion/NCM/Admin/Default.aspx" CausesValidation="true"/> 
            <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" 
            OnClick="btnCancel_Click" DisplayType="Secondary"
            CommandArgument="~/Orion/NCM/Admin/Default.aspx" CausesValidation="false"/>
        </div>
    </asp:Panel>
</asp:Content>
