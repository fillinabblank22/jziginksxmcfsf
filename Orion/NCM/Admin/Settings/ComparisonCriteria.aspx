<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="ComparisonCriteria.aspx.cs" Inherits="Orion_NCM_Admin_Settings_ComparisonCriteria" Title="Untitled Page" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    
    <link rel="stylesheet" type="text/css" href="Settings.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/PolicyReports/PolicyReport.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
    
    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Admin/Settings/ComparisonCriteriaViewer.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippetsHelper.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMPHCompCrit" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <asp:Panel ID="ContentContainer" CssClass="ContentContainer"  runat="server">
        <div class="SettingContainer">
            <div>                
                <%=Resources.NCMWebContent.WEBDATA_VK_556 %>
            </div>
          
            <table class="ExistingElements" cellpadding="0" cellspacing="0" width="100%" width="100%">
                <tr valign="top" align="left">
                    <td id="gridCell">
                        <div id="Grid" />
                    </td>
                </tr>
            </table>
                        
            <div id="regExPatternDialog" class="x-hidden">                
                <div id="regExPatternDialogBody" class="x-panel-body" style="padding:10px;height:450px;overflow:auto;">
                    <table>
                        <tr>
                            <td style="padding:5px 0px 15px 0px;"><%=Resources.NCMWebContent.WEBDATA_VK_558 %></td>
                        </tr>
                        <tr>
                            <td style="padding-top:5px;"><%=Resources.NCMWebContent.WEBDATA_VK_559 %></td>
                        </tr>
                        <tr>
                            <td><div id="divTitle"></div></td>
                        </tr>
                        <tr>
                            <td style="padding-top:5px;"><%=Resources.NCMWebContent.WEBDATA_VK_560 %></td>
                        </tr>
                        <tr>
                            <td><div id="divRegExPattern"></div></td>
                        </tr>
                        <tr>
                            <td style="padding-top:10px;"><input type="checkbox" id="isBlockCheckbox" onchange="toggleIsBlock()" style="margin-right: 5px;"/><%=Resources.NCMWebContent.WEBDATA_DP_11 %></td>
                        </tr>
                        <tr id="blockEndRegExLabel" style="display:none">
                            <td style="padding-top:5px;"><%=Resources.NCMWebContent.WEBDATA_DP_12 %></td>
                        </tr>
                        <tr id="blockEndRegExTextBox" style="display:none">
                            <td><div id="divBlockEndRegEx"></div></td>
                        </tr>
                        <tr>
                            <td style="padding-top:5px;"><%=Resources.NCMWebContent.WEBDATA_VK_561 %></td>
                        </tr>
                        <tr>
                            <td><div id="divComment"></div></td>
                        </tr>
                        <tr>
                            <td style="padding-top:5px;"><div id="divEnabled"></td>
                        </tr>
                    </table>
                    <div id="divRegExID"></div>
                </div>
            </div>
        </div>
        
        <div style="padding-top:20px;">
            <orion:LocalizableButton runat="server" ID="btnBack" LocalizedText="Back" 
            OnClick="btnBack_Click" DisplayType="Primary"
            CommandArgument="~/Orion/NCM/Admin/Default.aspx"/>
        </div>
    </asp:Panel>
</asp:Content>

