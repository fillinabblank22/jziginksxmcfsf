using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_Admin_Settings_SMTPServer : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ValidatePageAccess();
    }

    private void ValidatePageAccess()
    {
        if (CommonHelper.IsDemoMode() ||
            !(this.Page is IBypassAccessLimitation) && !Profile.AllowAdmin && !SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR))
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_VK_554}");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
        Page.Title = Resources.NCMWebContent.WEBDATA_IC_67;        
        if (!Page.IsPostBack)
        {
            ncmSMTPServer.Load();
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        ncmSMTPServer.Save();
        LocalizableButton btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        LocalizableButton btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }
}
