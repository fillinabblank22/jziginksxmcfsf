using System;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;

public partial class Orion_NCM_Admin_Settings_NetworkSettings : System.Web.UI.Page
{
    private readonly IIsWrapper isLayer;
    private readonly ICommonHelper commonHelper;

    public Orion_NCM_Admin_Settings_NetworkSettings()
    {
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
        commonHelper = ServiceLocator.Container.Resolve<ICommonHelper>();
    }

    protected int ICMPTimeout
    {
        get { return Convert.ToInt16(ViewState["ICMPTimeout"]); }
        set { ViewState["ICMPTimeout"] = value; }
    }

    protected string ICMPData
    {
        get { return Convert.ToString(ViewState["ICMPData"]); }
        set { ViewState["ICMPData"] = value; }
    }

    protected int SNMPTimeout
    {
        get { return Convert.ToInt16(ViewState["SNMPTimeout"]); }
        set { ViewState["SNMPTimeout"] = value; }
    }

    protected int SNMPRetries
    {
        get { return Convert.ToInt16(ViewState["SNMPRetries"]); }
        set { ViewState["SNMPRetries"] = value; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ValidatePageAccess();
    }

    private void ValidatePageAccess()
    {
        if (CommonHelper.IsDemoMode() ||
            !(this.Page is IBypassAccessLimitation) && !Profile.AllowAdmin && !SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR))
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_VK_554}");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_VK_614;

        slICMPTimeout.Label = $@"<b>{Resources.NCMWebContent.WEBDATA_VK_640}</b>";
        slSNMPTimeout.Label = $@"<b>{Resources.NCMWebContent.WEBDATA_VK_641}</b>";
        slSNMPRetries.Label = $@"<b>{Resources.NCMWebContent.WEBDATA_VK_642}</b>";

        if (!Page.IsPostBack)
        {
            using (var proxy = isLayer.GetProxy())
            {
                ICMPTimeout = Convert.ToInt16(proxy.Cirrus.Settings.GetSetting(Settings.ICMPTimeout, 2500, null));
                slICMPTimeout.Value = ICMPTimeout;
                ICMPData = proxy.Cirrus.Settings.GetSetting(Settings.ICMPData,
                    $@"SolarWinds Network Configuration Manager Version {commonHelper.GetProductVersion()}", null);
                txtICMPData.Text = ICMPData;

                SNMPTimeout = Convert.ToInt16(proxy.Cirrus.Settings.GetSetting(Settings.SNMPTimeout, 1000, null));
                slSNMPTimeout.Value = SNMPTimeout;
                SNMPRetries = Convert.ToInt16(proxy.Cirrus.Settings.GetSetting(Settings.SNMPRetries, 1, null));
                slSNMPRetries.Value = SNMPRetries;
            }
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        using (var proxy = isLayer.GetProxy())
        {
            if (ICMPTimeout != slICMPTimeout.Value)
                proxy.Cirrus.Settings.SaveSetting(Settings.ICMPTimeout, slICMPTimeout.Value, null);
            if (!ICMPData.Equals(txtICMPData.Text))
                proxy.Cirrus.Settings.SaveSetting(Settings.ICMPData, txtICMPData.Text, null);

            if (SNMPTimeout != slSNMPTimeout.Value)
                proxy.Cirrus.Settings.SaveSetting(Settings.SNMPTimeout, slSNMPTimeout.Value, null);
            if (SNMPRetries != slSNMPRetries.Value)
                proxy.Cirrus.Settings.SaveSetting(Settings.SNMPRetries, slSNMPRetries.Value, null);

            var btn = sender as LocalizableButton;
            Page.Response.Redirect(btn.CommandArgument);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        var btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }
}
