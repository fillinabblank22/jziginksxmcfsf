﻿using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using System;
using System.Web;
using SolarWinds.Cirrus.IS.Client;

public partial class Orion_NCM_Admin_Settings_BinaryConfigStorageSettings : System.Web.UI.Page
{
    private readonly IIsWrapper isWrapper;
    private readonly IDataDecryptor dataDecryptor;

    private static readonly string SpanSuccess =
        @"<span class='sw-suggestion sw-suggestion-pass'><span class='sw-suggestion-icon'></span>";
    private static readonly string SpanFail =
        @"<span class='sw-suggestion sw-suggestion-fail'><span class='sw-suggestion-icon'></span>";
    private static readonly string SpanEndTag = @"</span>";

    public Orion_NCM_Admin_Settings_BinaryConfigStorageSettings()
    {
        isWrapper = ServiceLocator.Container.Resolve<IIsWrapper>();
        dataDecryptor = ServiceLocator.Container.Resolve<IDataDecryptor>();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ValidatePageAccess();
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_VK_1045;

        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            const string demoAction = "demoAction('NCM_BinaryConfigStorageSettings_Submit', this); return false;";
            btnSubmit.OnClientClick = demoAction;
            btnValidate.OnClientClick = demoAction;
        }

        if (!Page.IsPostBack)
        {
            using (var proxy = isWrapper.GetProxy())
            {
                txtPath.Text = proxy.Cirrus.Settings.GetSetting(Settings.BinaryConfigStorageFolder, string.Empty, null);
                txtUserName.Text = dataDecryptor.Decrypt(proxy.Cirrus.Settings.GetSetting(Settings.BinaryConfigStorageUserName, string.Empty, null));
                txtPassword.PasswordText = dataDecryptor.Decrypt(proxy.Cirrus.Settings.GetSetting(Settings.BinaryConfigStoragePassword, string.Empty, null));
            }
        }
    }

    protected void btnValidate_Click(object sender, EventArgs e)
    {
        if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            ValidateBinaryConfigStorageSettings();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer || !ValidateBinaryConfigStorageSettings())
            return;

        using (var proxy = isWrapper.GetProxy())
        {
            proxy.Cirrus.Settings.SaveSetting(Settings.BinaryConfigStorageFolder, txtPath.Text, null);
            proxy.Cirrus.Settings.SaveSetting(Settings.BinaryConfigStorageUserName, txtUserName.Text, null);
            proxy.Cirrus.Settings.SaveSetting(Settings.BinaryConfigStoragePassword, txtPassword.PasswordText, null);
        }

        LocalizableButton btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        LocalizableButton btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }

    private void ValidatePageAccess()
    {
        if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer && !(this.Page is IBypassAccessLimitation) && !Profile.AllowAdmin && !SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR))
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_VK_554}");
        }
    }

    private bool ValidateBinaryConfigStorageSettings()
    {
        try
        {
            using (var proxy = isWrapper.GetProxy())
            {
                var result = proxy.Cirrus.ConfigArchive.ValidateBinaryConfigStorage(txtPath.Text, txtUserName.Text, txtPassword.PasswordText);

                if (result.IsValid)
                {
                    lValidator.Text = string.Concat(SpanSuccess, HttpUtility.HtmlEncode(Resources.NCMWebContent.WEBDATA_VK_947), SpanEndTag);
                }

                else
                {
                    lValidator.Text = string.Concat(SpanFail, HttpUtility.HtmlEncode(result.Message), SpanEndTag);
                }

                return result.IsValid;
            }
        }
        catch (Exception ex)
        {
            lValidator.Text = string.Concat(SpanFail, HttpUtility.HtmlEncode(ex.Message), SpanEndTag);
            return false;
        }
    }
}