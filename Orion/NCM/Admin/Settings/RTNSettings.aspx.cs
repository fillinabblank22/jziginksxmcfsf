using System;
using SolarWinds.NCM.Common.Help;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_NCM_Admin_Settings_RTNSettings : System.Web.UI.Page
{
    private ISWrapper _isLayer;
    bool isDemo = false;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ValidatePageAccess();
    }

    private void ValidatePageAccess()
    {
        if (!CommonHelper.IsDemoMode() && !(this.Page is IBypassAccessLimitation) && !Profile.AllowAdmin && !SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR))
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_VK_554}");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_IC_114;
        isDemo = OrionConfiguration.IsDemoServer;
        btnSubmit.Visible = !isDemo;

        _isLayer = new ISWrapper();
        bool isRTNEnabled;
        if (!Page.IsPostBack)
        {
            using (var proxy = _isLayer.Proxy)
            {
                isRTNEnabled = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(SolarWinds.NCM.Contracts.InformationService.Settings.RTNEnabled, false, null));
            }
            if (isRTNEnabled)
            {
                chkEnable.Checked = true;
            }
            else
            {
                chkDisable.Checked = true;
            }
        }

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        if (isDemo) return;
        
        _isLayer = new ISWrapper();
        using (var proxy = _isLayer.Proxy)
        {
            proxy.Cirrus.Settings.SaveSetting(SolarWinds.NCM.Contracts.InformationService.Settings.RTNEnabled, chkEnable.Checked, null);
        }

        
        var btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        var btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }

    protected string LearnMore
    {
        get
        {
            var helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
            return helpHelperWrap.GetHelpUrl("OrionNCMPHRTCDDeviceConfigLearnMore");
        }
    }
}
