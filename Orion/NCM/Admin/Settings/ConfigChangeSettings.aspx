<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="ConfigChangeSettings.aspx.cs" Inherits="Orion_NCM_Admin_Settings_ConfigChangeSettings"%>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
    <script type="text/javascript">
        $(document).ready(function() 
        {
            RTNUseUserDeviceClick();
        });
        
        function RTNUseUserDeviceClick()
        {
            var checked = document.getElementById("<%=chkRTNUseUserDevice.ClientID %>").checked;
            if(checked)
            {
                $('#deviceCredsHolder').show();
            }
            else
            {
                $('#deviceCredsHolder').hide();
            }
            EnableValidator(checked);
        }
        
        function EnableValidator(bEnable)
        {       
            var validator = document.getElementById("<%=compareValidator2.ClientID %>");
            ValidatorEnable(validator, bEnable);
        }
        
        function cvPassword_Validate(source, args)
        {
            var id = source.attributes.getNamedItem('controltocompareid').value;
            args.IsValid = (args.Value == document.getElementById(id).value);
        }
        
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="Settings.css" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMPHRTCDConfigChangesPage" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <asp:Panel ID="ContentContainer" CssClass="ContentContainer" runat="server">
        <div class="SettingContainer">
            <div class="SettingBlockHeader"><%=Resources.NCMWebContent.WEBDATA_VK_686 %></div>
            <div style="padding-top:10px;">
                <asp:CheckBox ID="chkRTNUseUserDevice" runat="server" onclick="RTNUseUserDeviceClick();" />                
            </div>
            <div style="color:Gray;padding-top:5px">
                <i><%=string.Format(Resources.NCMWebContent.WEBDATA_IC_115, @"<a style='color:#336699;' href='Security.aspx'>", @"</a>")%></i>
            </div>
            <div id="deviceCredsHolder">
                <div>
                    <div style="font-weight:bold;padding-top:20px;"><%=Resources.NCMWebContent.WEBDATA_IC_62 %></div>
                    <div style="padding-top:5px"><ncm:PasswordTextBox ID="txtRTNUserLevelName" runat="server" TextMode="SingleLine" CssClass="SettingTextField" Width="200px" /></div>
                    
                    <div style="font-weight:bold;padding-top:20px;"><%=Resources.NCMWebContent.WEBDATA_IC_63 %></div>
                    <div style="padding-top:5px"><ncm:PasswordTextBox ID="txtRTNUserLevelPassword" runat="server" TextMode="Password" CssClass="SettingTextField" Width="200px" /></div>
                    
                    <div style="font-weight:bold;padding-top:20px;"><%=Resources.NCMWebContent.WEBDATA_IC_64 %></div>
                    <div style="padding-top:5px"><ncm:PasswordTextBox ID="txtRTNUserLevelConfirmPassword" runat="server" TextMode="Password" CssClass="SettingTextField" Width="200px" /></div>
                    <div>
                        <asp:CustomValidator id="compareValidator2" EnableClientScript="true" ClientValidationFunction="cvPassword_Validate" ValidateEmptyText="true"  ControlToValidate="txtRTNUserLevelConfirmPassword"  runat="server" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_689 %>" Display="Dynamic" Enabled="true" />
                    </div>
                    
                    <div style="font-weight:bold;padding-top:20px;"><%=Resources.NCMWebContent.WEBDATA_VK_283 %></div>
                    <div style="padding-top:5px">
                        <asp:DropDownList ID="ddlRTNUserLevelEnableLevel" runat="server" CssClass="SettingSelectField" Width="200px">
                            <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_314 %>" Value="<No Enable Login>"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_315 %>" Value="enable"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    
                    <div style="font-weight:bold;padding-top:20px;"><%=Resources.NCMWebContent.WEBDATA_VM_137 %></div>
                    <div style="padding-top:5px"><ncm:PasswordTextBox ID="txtRTNUserLevelEnablePassword" runat="server" TextMode="Password" CssClass="SettingTextField" Width="200px" /></div>
                </div>
            </div>
            
            <div class="SettingBlockHeader" style="padding-top:50px;"><%=Resources.NCMWebContent.WEBDATA_IC_116 %></div>
            <div style="padding-top:20px;">
                <%=Resources.NCMWebContent.WEBDATA_VK_687 %>               
            </div>
            <div style="padding-top:20px;">
                <asp:CheckBox ID="chkRTNIncludeNotification" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_688 %>" />
            </div>
        </div>
        <div style="padding-top:20px;">
            <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" 
            OnClick="btnSubmit_Click" DisplayType="Primary"
            CommandArgument="~/Orion/NCM/Admin/Settings/RTNSettings.aspx" CausesValidation="true"/> 
            <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" 
            OnClick="btnCancel_Click" DisplayType="Secondary"
            CommandArgument="~/Orion/NCM/Admin/Settings/RTNSettings.aspx" CausesValidation="false"/>
        </div>
    </asp:Panel>
</asp:Content>

