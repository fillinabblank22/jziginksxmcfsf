<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true"
    CodeFile="AdvancedSettings.aspx.cs" Inherits="Orion_NCM_Admin_Settings_AdvancedSettings" %>

<%@ Register Assembly="Infragistics2.WebUI.WebDataInput.v8.2, Version=8.2.20082.1000, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.WebDataInput" TagPrefix="ig" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            var checked = document.getElementById("<%=chkEnableCache.ClientID %>").checked;
            ShowHideSetting(checked, 'cacheTimeHolder');

            checked = document.getElementById("<%=cbEnableEOSAutorun.ClientID %>").checked;
            ShowHideSetting(checked, 'EOSTimeHolder');
            ConfigureCredentialsPanel();            
        });

        $("#<%= chkUseImpersonation.ClientID %>").live("change", function () {
            ConfigureCredentialsPanel();
        });

        function ConfigureCredentialsPanel() {
            var validatorStatus = $("#<%= chkUseImpersonation.ClientID %>")[0].checked;
            var userNameValidator = $("#<%= userNameValidator.ClientID %>")[0];
            var userPassValidator = $("#<%= userPassValidator.ClientID %>")[0];

            ValidatorEnable(userNameValidator, validatorStatus)
            ValidatorEnable(userPassValidator, validatorStatus)

            $("#credentialsPanel").toggle(validatorStatus);
        }

        function ShowHideSetting(checked, controlId) {
            if (checked) {
                $('#' + controlId).show();
            }
            else {
                $('#' + controlId).hide();
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" runat="Server">
    <link rel="stylesheet" type="text/css" href="Settings.css" />

    <style type="text/css">
        .TableHeader
        {
            background-color: #E2E1D4;
            font-family: Arial,Helvetica,sans-serif;
            font-weight: bold;
            font-size: 11pt;
            height: 25px;
        }
        
        .ListHeader
        {
            font-family: Arial,Helvetica,sans-serif;
            font-weight: bold;
            list-style-type: decimal;
        }
        
        .TableException
        {
            background: #facece;
            color: #ce0000;
            border: 1px solid #ce0000;
            border-radius: 5px;
        }
        
        .SpanException
        {
            display: inline-block;
            position: relative;
            padding: 5px 6px 6px 26px;
        }
        
        .SpanException-Icon
        {
            position: absolute;
            left: 4px;
            top: 4px;
            height: 16px;
            width: 16px;
            background: url(/Orion/NCM/Resources/images/icon_failed.gif) top left no-repeat;
        }
        
        .SpanSuccess
        {
            background: #daf7cc;
            color: #00a000;
            border: 1px solid #00a000;
            border-radius: 5px;
            font-size: 10pt;
            font-family: Arial,Verdana,Helvetica,sans-serif;
            display: inline-block;
            position: relative;
            padding: 5px 6px 6px 26px;
        }
        
        .SpanSuccess-Icon
        {
            position: absolute;
            left: 4px;
            top: 4px;
            height: 16px;
            width: 16px;
            background: url(/Orion/NCM/Resources/images/icon_ok.gif) top left no-repeat;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMPHAdvancedSettingsPage" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" runat="Server">
    <%=Page.Title%>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <asp:Panel ID="ContentContainer" CssClass="ContentContainer" runat="server">
        <div class="SettingContainer">
            <div class="SettingBlockHeader">
                <%=Resources.NCMWebContent.WEBDATA_VK_661 %></div>
            <div class="SettingBlockDescription">
                <%=Resources.NCMWebContent.WEBDATA_VK_662 %>
            </div>
            <asp:Literal ID="exLiteral" runat="server"></asp:Literal>
            <div style="padding-top: 20px;">
                <asp:Table ID="tableLogging" runat="server" CellPadding="0" CellSpacing="0" Width="80%">
                    <asp:TableRow ID="name" runat="server">
                        <asp:TableCell Style="width: 30%;">&nbsp;</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="header" runat="server">
                        <asp:TableCell class="TableHeader">
                            <ul style="margin-bottom:0;margin-top:0;">
                                <li style="list-style-type:none;"><%=Resources.NCMWebContent.WEBDATA_VK_623 %></li>
                            </ul>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="jobs" runat="server">
                        <asp:TableCell class="SettingDelimiter" Style="width: 30%;">
                            <ul style="margin-bottom:0;margin-top:0;">
                                <li class="ListHeader" value="1">
                                    <%=Resources.NCMWebContent.WEBDATA_VK_663 %>
                                    <div class="SettingHint"><%=Resources.NCMWebContent.WEBDATA_VK_664 %></div>
                                </li>
                            </ul>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="inventory" runat="server">
                        <asp:TableCell class="SettingDelimiter" Style="width: 30%;">
                            <ul style="margin-bottom:0;margin-top:0;">
                                <li class="ListHeader" value="2">
                                    <%=Resources.NCMWebContent.WEBDATA_VK_665 %>
                                    <div class="SettingHint"><%=Resources.NCMWebContent.WEBDATA_VK_666 %></div>
                                </li>
                            </ul>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
            <div class="SettingBlockHeader" style="padding-top: 50px;">
                <%=Resources.NCMWebContent.WEBDATA_VK_673 %></div>
            <div class="SettingBlockDescription">
                <%=Resources.NCMWebContent.WEBDATA_VK_674 %>
            </div>
            <div style="padding-top: 20px;">
                <asp:CheckBox ID="chkEnableCache" runat="server" onclick="ShowHideSetting(this.checked, 'cacheTimeHolder');" />
            </div>
            <div style="padding-top: 20px;" id="cacheTimeHolder">
                <%=Resources.NCMWebContent.WEBDATA_VK_675 %>&nbsp;<ig:WebDateTimeEdit ID="txtCachingTime"
                    runat="server" EditModeFormat="T" Width="70px" />
            </div>
            <div class="SettingBlockHeader" style="padding-top: 50px;">
                <%=Resources.NCMWebContent.WEBDATA_VK_676 %></div>
            <div class="SettingBlockDescription">
                <%=Resources.NCMWebContent.WEBDATA_VK_677 %>
            </div>
            <div style="padding-top: 20px;">
                <span>
                    <asp:RadioButton ID="rbAllColumns" GroupName="Sorting" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_678 %>"
                        runat="server" /></span> <span style="padding-left: 20px;">
                            <asp:RadioButton ID="rbSingleColumn" GroupName="Sorting" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_679 %>"
                                runat="server" /></span>
            </div>
            <div class="SettingBlockHeader" style="padding-top: 50px;">
                <%=Resources.NCMWebContent.WEBDATA_VK_867 %></div>
            <div style="padding-top: 20px;">
                <asp:CheckBox ID="cbIsFallBackEnabled" runat="server" />
            </div>
            <div class="SettingBlockHeader" style="padding-top: 50px;">
                <%=Resources.NCMWebContent.WEBDATA_VK_880 %></div>
            <div style="padding-top: 20px;">
                <asp:CheckBox ID="cbEnableEOSAutorun" runat="server" onclick="ShowHideSetting(this.checked, 'EOSTimeHolder');" />
            </div>
            <div style="padding-top: 20px;" id="EOSTimeHolder">
                <%=Resources.NCMWebContent.WEBDATA_VK_882 %>&nbsp;<ig:WebDateTimeEdit ID="txtEOSTime"
                    runat="server" EditModeFormat="T" Width="70px" />
            </div>
            <div class="SettingBlockHeader" style="padding-top: 50px;"><%=Resources.NCMWebContent.AdvancedSettings_NetworkShareSettingsLabel %></div>
            <div style="padding-top: 20px;">
                <asp:CheckBox ID="chkUseImpersonation" runat="server" Text="<%$ Resources: NCMWebContent, AdvancedSetting_UseImpersonationCheckboxText %>" />
            </div>
            <div id="credentialsPanel">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="padding-top:20px;padding-right:5px;">
                            <%=Resources.NCMWebContent.WEBDATA_VK_1048 %>
                        </td>
                        <td style="padding-top:20px;">
                            <asp:TextBox ID="txtUserName" autocomplete="off" runat="server" />
                            <asp:RequiredFieldValidator ID="userNameValidator" runat="server" ControlToValidate="txtUserName" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-right:5px;">&nbsp;</td>
                        <td colspan="2" style="color:#5F5F5F;font-size:10px;">
                            <%=Resources.NCMWebContent.WEBDATA_VK_1049 %>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-right:5px;padding-top:5px;">
                            <%=Resources.NCMWebContent.WEBDATA_VK_297 %>
                        </td>
                        <td style="padding-top:5px;">
                            <asp:TextBox ID="txtPassword" type="password" autocomplete="off" runat="server" />
                            <asp:RequiredFieldValidator ID="userPassValidator" runat="server" ControlToValidate="txtPassword" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-right:5px;">&nbsp;</td>
                        <td style="padding-top:5px;">
                            <div>
                                <orion:LocalizableButton runat="server" ID="btnValidate" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, AdvancedSetting_ValidateCredentialsButtonText %>" OnClick="btnValidateCredentials_Click" DisplayType="Small" CausesValidation="true" />
                                <span class="SettingHint"><%=Resources.NCMWebContent.AdvancedSetting_ValidateCredentialsHint %></span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <asp:Literal ID="credentialsValidator" runat="server"></asp:Literal>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="SettingBlockHeader" style="padding-top: 50px;"><%=Resources.NCMWebContent.AdvancedSettings_NetworkDiscoverySettingsLabel %></div>
            <div style="padding-top: 20px;">
                <asp:CheckBox ID="chkSkipAddingNodes" runat="server" Text="<%$ Resources: NCMWebContent, AdvancedSettings_SkipAddingNodesCheckboxText %>" />
            </div>
        </div>
        <div style="padding-top: 20px;">
            <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" OnClick="btnSubmit_Click"
                DisplayType="Primary" CommandArgument="~/Orion/NCM/Admin/Default.aspx" CausesValidation="true" />
            <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" OnClick="btnCancel_Click"
                DisplayType="Secondary" CommandArgument="~/Orion/NCM/Admin/Default.aspx" CausesValidation="false" />
        </div>
    </asp:Panel>
</asp:Content>
