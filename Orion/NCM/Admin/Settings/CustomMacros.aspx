<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="CustomMacros.aspx.cs" Inherits="Orion_NCM_Admin_Settings_CustomMacros" Title="Untitled Page" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    
    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Admin/Settings/CustomMacros.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippetsHelper.js"></script>
    
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/PolicyReports/PolicyReport.css" />
    <link rel="stylesheet" type="text/css" href="Settings.css" />
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="helpBtn" runat="server" HelpUrlFragment="OrionNCMPHCustomMacros" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <asp:Panel ID="ContentContainer" CssClass="ContentContainer" runat="server">
        <div class="SettingContainer">
            <div id="descrContent" style="padding-bottom:20px;">
                <%=string.Format(Resources.NCMWebContent.WEBDATA_VM_150, @"<span style='font-size:8pt;'>&#0187;&nbsp;<a style='color:#336699;' target='_blank' href=" + this.LearnMore +@">", @"</a></span>")%>
            </div>
    
            <table class="ExistingElements" cellpadding="0" cellspacing="0" width="100%" width="100%">
                <tr valign="top" align="left">
                    <td id="gridCell">
                        <div id="Grid" />
                    </td>
                </tr>
            </table>
            
            <div id="AddEditMacrosDlg" class="x-hidden">                
                <div id="AddEditMacrosDlgBody" class="x-panel-body" style="height:100%;overflow:auto;">
                    <div style="padding:10px;">
			            <table height="70%;">
                            <tr>
                                <td><%= Resources.NCMWebContent.WEBDATA_VM_152%></td>
                                <td><div id="txtNameDiv"></div></td>
                            </tr>
                            <tr>
                                <td><%= Resources.NCMWebContent.WEBDATA_VM_153%></td>
                                <td><div id="txtValueDiv"></div></td>
                            </tr>
                        </table>     
		            </div>
                </div>
            </div>
        </div>
    </asp:Panel>  
</asp:Content>

