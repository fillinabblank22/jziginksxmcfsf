using System;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.Orion.Web;

public partial class Orion_NCM_Admin_Settings_ConfigDownloadsNotifications : System.Web.UI.Page
{
    private ISWrapper isLayer;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ValidatePageAccess();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        isLayer = new ISWrapper();
        Page.Title = Resources.NCMWebContent.WEBDATA_VM_154;        

        if (!IsPostBack)
        {
            Initialize();
        }
    }

    private void ValidatePageAccess()
    {
        if (CommonHelper.IsDemoMode() ||
            !(this.Page is IBypassAccessLimitation) && !Profile.AllowAdmin && !SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR))
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_VK_554}");
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        SubmitSettings();
        var btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        var btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }

    #region private methods
    
    private void Initialize()
    {
        using (var proxy = isLayer.Proxy)
        {
            //Previously Downloded Config File
            RTNConfigType.SelectedValue = proxy.Cirrus.Settings.GetSetting(Settings.RTNConfigType, @"Running", null);
            
            //Baseline Config File
            rbRTNLastDownloadConfig.Checked = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.RTNLastConfig, @"False", null));
            rbRTNBaselineConfig.Checked = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.RTNBaselineConfig, @"True", null));

            //Email Notification Options and Defaults
            cbDisableRTNEmail.Checked = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.DisableRTNEmail, @"False", null));
            cbRTNInHTMLFormat.Checked = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.RTNInHTMLFormat, @"False", null));
            cbRTNWithDiffs.Checked = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.RTNWithDiffs, @"True", null));
           
            txtRTNEmailFromName.Text = proxy.Cirrus.Settings.GetSetting(Settings.RTNEmailFromName, Resources.NCMWebContent.Settings_DefaultEmailSenderName, null);
            txtRTNEmailReplyAddress.Text = proxy.Cirrus.Settings.GetSetting(Settings.RTNEmailReplyAddress, @"SolarWinds@yourdomain.name", null);
            txtRTNEmailSubject.Text = proxy.Cirrus.Settings.GetSetting(Settings.RTNEmailSubject, Resources.NCMWebContent.Settings_RTNDefaultEmailSubject, null);
            
            txtRTNEmailTo.Text = proxy.Cirrus.Settings.GetSetting(Settings.RTNEmailTo, string.Empty, null);
            txtRTNEmailCC.Text = proxy.Cirrus.Settings.GetSetting(Settings.RTNEmailCC, string.Empty, null);
            txtRTNEmailBCC.Text = proxy.Cirrus.Settings.GetSetting(Settings.RTNEmailBCC, string.Empty, null);

            if (!string.IsNullOrEmpty(txtRTNEmailCC.Text))
            {
                btnAddCC.Visible = false;
            }

            if (!string.IsNullOrEmpty(txtRTNEmailBCC.Text))
            {
                btnAddBCC.Visible = false;
            }
        }
    }
    
    private void SubmitSettings()
    {
        using (var proxy = isLayer.Proxy)
        {
            //Previously Downloded Config File
            proxy.Cirrus.Settings.SaveSetting(Settings.RTNConfigType, RTNConfigType.SelectedValue, null);

            //Baseline Config File
            proxy.Cirrus.Settings.SaveSetting(Settings.RTNLastConfig, rbRTNLastDownloadConfig.Checked, null);
            proxy.Cirrus.Settings.SaveSetting(Settings.RTNBaselineConfig, rbRTNBaselineConfig.Checked, null);

            //Email Notification Options and Defaults
            proxy.Cirrus.Settings.SaveSetting(Settings.DisableRTNEmail, cbDisableRTNEmail.Checked, null);

            if (!cbDisableRTNEmail.Checked)
            {
                proxy.Cirrus.Settings.SaveSetting(Settings.RTNInHTMLFormat, cbRTNInHTMLFormat.Checked, null);
                proxy.Cirrus.Settings.SaveSetting(Settings.RTNWithDiffs, cbRTNWithDiffs.Checked, null);
                proxy.Cirrus.Settings.SaveSetting(Settings.RTNEmailFromName, txtRTNEmailFromName.Text, null);
                proxy.Cirrus.Settings.SaveSetting(Settings.RTNEmailReplyAddress, txtRTNEmailReplyAddress.Text, null);
                proxy.Cirrus.Settings.SaveSetting(Settings.RTNEmailSubject, txtRTNEmailSubject.Text, null);
                proxy.Cirrus.Settings.SaveSetting(Settings.RTNEmailTo, txtRTNEmailTo.Text, null);
                proxy.Cirrus.Settings.SaveSetting(Settings.RTNEmailCC, txtRTNEmailCC.Text, null);
                proxy.Cirrus.Settings.SaveSetting(Settings.RTNEmailBCC, txtRTNEmailBCC.Text, null);
            }
        }
    }
    
    #endregion

}
