<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="TFTPServer.aspx.cs" Inherits="Orion_NCM_Admin_Settings_TFTPServer"%>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="Settings.css" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMTFTPServer" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <asp:Panel ID="ContentContainer" CssClass="ContentContainer" runat="server">
        <asp:UpdatePanel ID="uPanel" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <div class="SettingContainer">
                    <div class="SettingBlockDescription">
                        <%=Resources.NCMWebContent.WEBDATA_VK_647 %>
                        <div style="padding-top:40px;">
                            <asp:CheckBox ID="chkTFTPAutoRetriveIP" runat="server" Text="" AutoPostBack="true" OnCheckedChanged="chkTFTPAutoRetriveIP_OnCheckedChanged"/>
                        </div>
                        <div style="padding-top:40px;">
                            <asp:Repeater runat="server" ID="repeater" OnItemDataBound="repeater_ItemDataBound" OnItemCommand="repeater_ItemCommand">
                                <HeaderTemplate>                            
                                    <table border="0" cellpadding="0" cellspacing="0">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr id="exHolder" runat="server" visible="false">
                                        <td style="padding-top:10px;">
                                            <span class="SettingException">
                                                <span class="SettingException-Icon"></span><asp:Literal ID="exWarn" runat="server" />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="tdHolder" runat="server" style="padding-bottom:20px;">
                                            <asp:HiddenField ID="hiddenEngineID" runat="server"></asp:HiddenField>
                                            <asp:Literal ID="litServerName" runat="server"></asp:Literal>
                                            
                                            <div style="padding-bottom:5px;"><%=Resources.NCMWebContent.WEBDATA_VK_648 %></div>
                                            <div>
                                                <asp:TextBox ID="txtTFTPServerAddress" runat="server" TextMode="SingleLine" CssClass="SettingTextField" Width="500px" />
                                                <orion:LocalizableButton runat="server" ID="btnRetrieveIP" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_650 %>" DisplayType="Small" CommandName="RetrieveIP" />
                                            </div>
                                            
                                            <div style="padding-bottom:5px;padding-top:20px;"><%=Resources.NCMWebContent.WEBDATA_VK_649 %></div>
                                            <div>
                                                <asp:TextBox ID="txtTFTPRootDir" runat="server" TextMode="SingleLine" CssClass="SettingTextField" Width="500px" />
                                                <orion:LocalizableButton runat="server" ID="btnValidate" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_590 %>" DisplayType="Small" CommandName="Validate" />
                                            </div>
                                            <div id="validatorHolder" runat="server"></div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
                <div style="padding-top:20px;">
                    <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" 
                    OnClick="btnSubmit_Click" DisplayType="Primary"
                    CommandArgument="~/Orion/NCM/Admin/Default.aspx" CausesValidation="true"/> 
                    <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" 
                    OnClick="btnCancel_Click" DisplayType="Secondary"
                    CommandArgument="~/Orion/NCM/Admin/Default.aspx" CausesValidation="false"/>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

