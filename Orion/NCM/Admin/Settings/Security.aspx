<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="Security.aspx.cs" Inherits="Orion_NCM_Admin_Settings_Security"%>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="Settings.css" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMPHSecurity" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <asp:Panel ID="ContentContainer" CssClass="ContentContainer" runat="server">
        <div class="SettingContainer">
            <div class="SettingBlockHeader" style="border-bottom:0px;padding-bottom:0px;"><%=Resources.NCMWebContent.WEBDATA_VK_499 %></div>
            <div style="padding-top:10px;"><asp:CheckBox ID="chkHideCommunityStrings" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_500 %>" /></div>
            <div style="padding-top:10px;"><asp:CheckBox ID="chkHideUsernames" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_501 %>" /></div>
            
            <div class="SettingBlockHeader" style="border-bottom:0px;padding-top:50px;padding-bottom:0px;"><%=Resources.NCMWebContent.WEBDATA_VK_504 %></div>
            <div class="SettingBlockDescription" style="padding-top:10px;padding-bottom:0px;"><%=Resources.NCMWebContent.WEBDATA_VK_505 %></div>
            <div style="padding-top:10px;"><asp:RadioButton ID="rbLevelOne" GroupName="Levels" Value="Level 1" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_506 %>" runat="server" /></div>
            <div style="padding-top:10px;"><asp:RadioButton ID="rbLevelThree" GroupName="Levels" Value="Level 3" runat="server" /></div>
            <div style="padding-top:10px; padding-left:20px;">
                <span class="SettingSuggestion"><span class="SettingSuggestion-Icon"></span>
                      <%=Resources.NCMWebContent.WEBDATA_VY_91 %>
                </span>
            </div>

            <div class="SettingBlockHeader" style="border-bottom:0px;padding-top:50px;padding-bottom:0px;"><b><%=Resources.NCMWebContent.Security_CompliancePolicyReportManagementLabel %></b></div>
            <div style="padding-top:10px;"><asp:CheckBox ID="chkComplianceMgmtOnlyForAdmins" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_897 %>" /></div>
            <div style="padding-top:10px;"><asp:CheckBox ID="chkBypassApprovalForAutoRemediationScript" runat="server" Text="<%$ Resources: NCMWebContent, Security_BypassApprovalForAutoRemediationScriptCheckboxText %>" /></div>
        </div>
        
        <div style="padding-top:20px;">
            <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" 
            OnClick="btnSubmit_Click" DisplayType="Primary"
            CommandArgument="~/Orion/NCM/Admin/Default.aspx"/> 
            <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" 
            OnClick="btnCancel_Click" DisplayType="Secondary"
            CommandArgument="~/Orion/NCM/Admin/Default.aspx"/>
        </div>
    </asp:Panel>
</asp:Content>

