using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Text;
using System.Web;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Logging;
using SolarWinds.NCM.Common.Dal;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using CoreModels = SolarWinds.Orion.Core.Common.Models;

public partial class Orion_NCM_Admin_Settings_AdvancedSettings : System.Web.UI.Page
{
    private static readonly Log log = new Log();

    private readonly IIsWrapper isLayer;
    private readonly IDataDecryptor decr;

    private string notificationTypeId = @"223EF793-105F-4A3B-89EF-7664411D629D";

    public Orion_NCM_Admin_Settings_AdvancedSettings()
    {
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
        decr = ServiceLocator.Container.Resolve<IDataDecryptor>();
    }

    protected bool EnableCache
    {
        get { return Convert.ToBoolean(ViewState["EnableCache"]); }
        set { ViewState["EnableCache"] = value; }
    }

    protected DateTime CachingTime
    {
        get { return Convert.ToDateTime(ViewState["CachingTime"]); }
        set { ViewState["CachingTime"] = value; }
    }

    protected bool SortOnAllColumns
    {
        get { return Convert.ToBoolean(ViewState["SortOnAllColumns"]); }
        set { ViewState["SortOnAllColumns"] = value; }
    }

    protected bool SortOnSingleColumn
    {
        get { return Convert.ToBoolean(ViewState["SortOnSingleColumn"]); }
        set { ViewState["SortOnSingleColumn"] = value; }
    }

    protected bool IsFallBackEnabled
    {
        get { return Convert.ToBoolean(ViewState["IsFallBackEnabled"]); }
        set { ViewState["IsFallBackEnabled"] = value; }
    }

    protected bool EnableEOSAutorun
    {
        get { return Convert.ToBoolean(ViewState["EnableEOSAutorun"]); }
        set { ViewState["EnableEOSAutorun"] = value; }
    }

    protected DateTime EOSTime
    {
        get { return Convert.ToDateTime(ViewState["EOSTime"]); }
        set { ViewState["EOSTime"] = value; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ValidatePageAccess();
    }

    private void ValidatePageAccess()
    {
        if (CommonHelper.IsDemoMode() ||
            !(this.Page is IBypassAccessLimitation) && !Profile.AllowAdmin && !SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR))
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_VK_554}");
        }
    }

    private ICoreBusinessLayerProxyCreator coreBusinessLayerProxyCreator
    {
        get { return ServiceLocator.Container.Resolve<ICoreBusinessLayerProxyCreator>(); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_VK_680;

        chkEnableCache.Text = $@"<b>{Resources.NCMWebContent.WEBDATA_VK_682}</b>";
        cbIsFallBackEnabled.Text = $@"<b>{Resources.NCMWebContent.WEBDATA_VK_876}</b>";
        cbEnableEOSAutorun.Text = $@"<b>{Resources.NCMWebContent.WEBDATA_VK_881}</b>";

        if (!Page.IsPostBack)
        {
            using (var proxy = isLayer.GetProxy())
            {
                EnableCache = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.EnableCache, true, null));
                chkEnableCache.Checked = EnableCache;
                CachingTime = Convert.ToDateTime(proxy.Cirrus.Settings.GetSetting(Settings.CachingTime, @"11:55 PM", null));
                txtCachingTime.Value = CachingTime;

                SortOnAllColumns = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.SortOnAllColumns, true, null));
                rbAllColumns.Checked = SortOnAllColumns;
                SortOnSingleColumn = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.SortOnSingleColumn, false, null));
                rbSingleColumn.Checked = SortOnSingleColumn;

                IsFallBackEnabled = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.IsFallBackEnabled, false, null));
                cbIsFallBackEnabled.Checked = IsFallBackEnabled;

                EnableEOSAutorun = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.NCMEosSchedulingEnabled, true, null));
                cbEnableEOSAutorun.Checked = EnableEOSAutorun;
                EOSTime = Convert.ToDateTime(proxy.Cirrus.Settings.GetSetting(Settings.NCMEosStartTime, @"2:15", null), System.Globalization.CultureInfo.InvariantCulture);
                txtEOSTime.Value = EOSTime;

                chkUseImpersonation.Checked = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.NetworkShareUseImpersonation, false, null));
                chkSkipAddingNodes.Checked = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(Settings.SkipAddingNodes, false, null));
                txtUserName.Text = decr.Decrypt(proxy.Cirrus.Settings.GetSetting(Settings.NetworkShareUserName, "", null));
                txtPassword.Text = decr.Decrypt(proxy.Cirrus.Settings.GetSetting(Settings.NetworkShareUserPassword, "", null));
            }
        }

        DataBindLogging();
    }

    protected void btnValidateCredentials_Click(object sender, EventArgs e)
    {
        ValidateUserOnAllEngines();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        using (var proxy = isLayer.GetProxy())
        {
            if (chkUseImpersonation.Checked)
            {
                if (ValidateUserOnAllEngines())
                {
                    proxy.Cirrus.Settings.SaveSetting(Settings.NetworkShareUserName, txtUserName.Text, null);
                    proxy.Cirrus.Settings.SaveSetting(Settings.NetworkShareUserPassword, txtPassword.Text, null);
                }
                else return;
            }

            var logging = GetLoggingData();
            foreach (var kvp in logging)
            {
                try
                {
                    proxy.Cirrus.Settings.SaveLogging(kvp.Value, kvp.Key);
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                }
            }

            if (LoggingIsEnabled(logging))
                InsertUpdateNcmLoggingIsEnabledNotification();
            else
                AcknowledgeNcmLoggingIsEnabledNotification();

            if (EnableCache != chkEnableCache.Checked)
                proxy.Cirrus.Settings.SaveSetting(Settings.EnableCache, chkEnableCache.Checked, null);
            if (chkEnableCache.Checked)
            {
                var dateTime = Convert.ToDateTime(txtCachingTime.Value);
                if (!CachingTime.Equals(dateTime))
                {
                    proxy.Cirrus.Settings.SaveSetting(Settings.CachingTime, dateTime.ToString(@"hh:mm:ss tt", System.Globalization.DateTimeFormatInfo.InvariantInfo), null);
                }
            }

            if (SortOnAllColumns != rbAllColumns.Checked)
                proxy.Cirrus.Settings.SaveSetting(Settings.SortOnAllColumns, rbAllColumns.Checked, null);
            if (SortOnSingleColumn != rbSingleColumn.Checked)
                proxy.Cirrus.Settings.SaveSetting(Settings.SortOnSingleColumn, rbSingleColumn.Checked, null);

            if (IsFallBackEnabled != cbIsFallBackEnabled.Checked)
                proxy.Cirrus.Settings.SaveSetting(Settings.IsFallBackEnabled, cbIsFallBackEnabled.Checked, null);

            if (EnableEOSAutorun != cbEnableEOSAutorun.Checked)
                proxy.Cirrus.Settings.SaveSetting(Settings.NCMEosSchedulingEnabled, cbEnableEOSAutorun.Checked, null);
            if (cbEnableEOSAutorun.Checked)
            {
                var dateTime = (DateTime)txtEOSTime.Value;
                if (!EOSTime.Equals(dateTime))
                {
                    proxy.Cirrus.Settings.SaveSetting(Settings.NCMEosStartTime, dateTime.ToString(@"t", System.Globalization.CultureInfo.InvariantCulture), null);
                }
            }
            proxy.Invoke(@"NCM.Eos", @"InitSchedule");

            proxy.Cirrus.Settings.SaveSetting(Settings.NetworkShareUseImpersonation, chkUseImpersonation.Checked, null);
            proxy.Cirrus.Settings.SaveSetting(Settings.SkipAddingNodes, chkSkipAddingNodes.Checked, null);
        }

        var btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        var btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }

    private void DataBindLogging()
    {
        var engines = LookupEngines().ToArray();

        TableCell tCell;
        CheckBox chk;

        var countEngines = engines.Length;

        int tableCellWidth;
        int emptyTableCellWidth;
        if (countEngines > 7)
        {
            tableCellWidth = 70 / countEngines;
            emptyTableCellWidth = 0;
        }
        else
        {
            tableCellWidth = 10;
            emptyTableCellWidth = 70 - countEngines * 10;
        }

        var htmlTemplate = string.Empty;

        foreach (var engine in engines)
        {
            var engineId = engine.EngineId.ToString();
            int engineIdValue = Convert.ToInt16(engineId);
            using (var proxy = isLayer.GetProxy())
            {
                var enabled = true;
                var logging = string.Empty;
                try
                {
                    logging = proxy.Cirrus.Settings.GetLogging(Convert.ToInt16(engineId));
                }
                catch (Exception ex)
                {
                    htmlTemplate +=
                        $@"<tr><td><span class='SpanException'><span class='SpanException-Icon'></span>{ex.Message}</span></td></tr>";

                    engineIdValue = -1;
                    enabled = false;
                }

                foreach (TableRow tRow in tableLogging.Rows)
                {
                    if (tRow.ID.Equals(@"name", StringComparison.CurrentCultureIgnoreCase))
                    {
                        var hiddenField = new HiddenField();
                        hiddenField.ID = $@"hiddenField_{engineId}";
                        hiddenField.Value = engineIdValue.ToString();

                        var literal = new Literal();
                        literal.Text = $@"&nbsp;{engine.EngineName}&nbsp;";

                        tCell = new TableCell();
                        tCell.CssClass = @"ListHeader";
                        tCell.Style.Add(@"padding-bottom", @"10px");
                        tCell.HorizontalAlign = HorizontalAlign.Center;
                        tCell.Width = Unit.Percentage(tableCellWidth);
                        tCell.Controls.Add(hiddenField);
                        tCell.Controls.Add(literal);
                        tRow.Cells.Add(tCell);

                        if (countEngines == 1) tRow.Style.Add(@"display", @"none");
                    }

                    if (tRow.ID.Equals(@"jobs", StringComparison.CurrentCultureIgnoreCase))
                    {
                        chk = new CheckBox();
                        chk.ID = $@"jobs_{engineId}";
                        chk.Checked = logging.IndexOf(@"Jobs", StringComparison.InvariantCultureIgnoreCase) >= 0;
                        chk.Enabled = enabled;

                        tCell = new TableCell();
                        tCell.Controls.Add(chk);
                        tCell.CssClass = @"SettingDelimiter";
                        tCell.HorizontalAlign = HorizontalAlign.Center;
                        tCell.Width = Unit.Percentage(tableCellWidth);
                        tRow.Cells.Add(tCell);
                    }

                    if (tRow.ID.Equals(@"inventory", StringComparison.CurrentCultureIgnoreCase))
                    {
                        chk = new CheckBox();
                        chk.ID = $@"inventory_{engineId}";
                        chk.Checked = logging.IndexOf(@"Inventory", StringComparison.InvariantCultureIgnoreCase) >= 0;
                        chk.Enabled = enabled;

                        tCell = new TableCell();
                        tCell.CssClass = @"SettingDelimiter";
                        tCell.HorizontalAlign = HorizontalAlign.Center;
                        tCell.Width = Unit.Percentage(tableCellWidth);
                        tCell.Controls.Add(chk);
                        tRow.Cells.Add(tCell);
                    }

                    if (tRow.ID.Equals(@"header", StringComparison.CurrentCultureIgnoreCase))
                    {
                        tCell = new TableCell();
                        tCell.CssClass = @"TableHeader";
                        tCell.Text = @"&nbsp;";
                        tCell.HorizontalAlign = HorizontalAlign.Center;
                        tCell.Width = Unit.Percentage(tableCellWidth);
                        tRow.Cells.Add(tCell);
                    }
                }
            }
        }

        foreach (TableRow tRow in tableLogging.Rows)
        {
            tCell = new TableCell();
            tCell.Text = @"&nbsp;";
            tCell.Width = Unit.Percentage(emptyTableCellWidth);
            tRow.Cells.Add(tCell);
        }

        if (!string.IsNullOrEmpty(htmlTemplate)) exLiteral.Text =
            $@"<div style='padding-top:10px;'><table cellpadding='0' cellspacing='0' class='TableException'>{htmlTemplate}</table></div>";
    }

    private Dictionary<int, string> GetLoggingData()
    {
        var engines = LookupEngines();

        CheckBox chk;

        var logging = new Dictionary<int, string>();
        foreach (var engine in engines)
        {
            var engineId = engine.EngineId.ToString();

            var hiddenField = tableLogging.FindControl($@"hiddenField_{engineId}") as HiddenField;
            if (Convert.ToInt16(hiddenField.Value) < 0) continue;

            var sb = new StringBuilder();

            chk = tableLogging.FindControl($@"jobs_{engineId}") as CheckBox;
            if (chk.Checked) sb.Append(@"Jobs,");

            chk = tableLogging.FindControl($@"inventory_{engineId}") as CheckBox;
            if (chk.Checked)
            {
                sb.Append(@"Inventory,");
                sb.Append(@"ConfigMgmtEngine8.NodeMonitor,");
            }
            
            sb.Append(@"General");

            logging.Add(Convert.ToInt32(engineId), sb.ToString());
        }

        return logging;
    }

    private IEnumerable<EngineInfo> LookupEngines()
    {
        var engineDal = ServiceLocator.Container.Resolve<ISwisEnginesDal>();
        return engineDal.GetAllPollingEngines();
    }

    private bool ValidateUserOnAllEngines()
    {
        var engines = LookupEngines();

        var sb = new StringBuilder();
        foreach (var engine in engines)
        {
            var engineId = engine.EngineId;

            string error;
            if (!ValidateUser(txtUserName.Text, txtPassword.Text, engineId, out error))
            {
                sb.AppendFormat(@"<tr><td><span class='SpanException'><span class='SpanException-Icon'></span>{0}</span></td></tr>",
                    string.IsNullOrEmpty(error) ?
                        IsMultipleEngines ? string.Format(Resources.NCMWebContent.AdvancedSettings_IncorrectCredentialsOnMessage, engine.EngineName) : Resources.NCMWebContent.AdvancedSettings_IncorrectCredentialsMessage :
                        error);
            }
        }

        if (sb.Length > 0)
        {
            credentialsValidator.Text =
                $@"<div style='padding-top:5px;'><table cellpadding='0' cellspacing='0' class='TableException'>{sb}</table></div>";
            return false;
        }
        else
        {
            credentialsValidator.Text =
                $@"<div style='padding-top:5px;'><span class='SpanSuccess'><span class='SpanSuccess-Icon'></span>{Resources.NCMWebContent.WEBDATA_VK_947}</span></div>";
            return true;
        }
    }

    private bool ValidateUser(string UserName, string Password, int engineId, out string error)
    {
        try
        {

            using (var proxy = isLayer.GetProxy())
            {
                error = string.Empty;
                return proxy.Cirrus.Settings.ValidateADUser(UserName, Password, engineId);
            }
        }
        catch (Exception ex)
        {
            error = ex.Message;
            log.Error(ex);
            return false;
        }
    }

    private bool LoggingIsEnabled(Dictionary<int, string> logging)
    {
        foreach (var kvp in logging)
        {
            if (!kvp.Value.Equals(@"General", StringComparison.InvariantCultureIgnoreCase))
                return true;
        }
        return false;
    }

    private void InsertUpdateNcmLoggingIsEnabledNotification()
    {
        var typeId = Guid.Parse(notificationTypeId);
        using (var coreProxy = coreBusinessLayerProxyCreator.Create(BusinessLayerExceptionHandler))
        {
            var notification = coreProxy.GetNotificationItemsByType(typeId, true).FirstOrDefault();
            if (notification != null)
            {
                if (notification.IsAcknowledged)
                {
                    notification.SetNotAcknowledged();
                    coreProxy.UpdateNotificationItem(notification);
                }
            }
            else
            {
                notification = new CoreModels.NotificationItem(
                    Guid.NewGuid(),
                    $@"{Resources.NCMWebContent.AdvancedSettings_NCMLoggingIsEnabled}<span style='font-weight:normal;'> &raquo; <a href='/Orion/NCM/Admin/Settings/AdvancedSettings.aspx'>{Resources.NCMWebContent.AdvancedSettings_MoreDetailsLink}</a></span>",
                    null,
                    DateTime.Now,
                    false,
                    typeId,
                    null,
                    null,
                    null);
                coreProxy.InsertNotificationItem(notification);
            }
        }
    }

    private void AcknowledgeNcmLoggingIsEnabledNotification()
    {
        var typeId = Guid.Parse(notificationTypeId);
        using (var coreProxy = coreBusinessLayerProxyCreator.Create(BusinessLayerExceptionHandler))
        {
            var notification = coreProxy.GetNotificationItemsByType(typeId, true).FirstOrDefault();
            if (notification != null)
                coreProxy.AcknowledgeNotificationItem(notification.Id, HttpContext.Current.Profile.UserName, notification.CreatedAt);
        }
    }

    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

    protected bool IsMultipleEngines => LookupEngines().Any();
}
