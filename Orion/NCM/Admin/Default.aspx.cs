using System;
using System.Web.UI;
using SolarWinds.NCM.Common.Help;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web;
using SolarWinds.NCMModule.Web.Resources;
using NcmWebStrings = Resources.NCMWebContent;

public partial class Orion_NCM_Admin_Default : Page
{
    private readonly IHelpHelperWrap helpHelperWrap;
    private readonly ICommonHelper commonHelper;

    public Orion_NCM_Admin_Default()
    {
        helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
        commonHelper = ServiceLocator.Container.Resolve<ICommonHelper>();
    }

    protected string ProductNameAndVersion
    {
        get
        {
            string productVersion = commonHelper.GetProductVersion();
            if (!string.IsNullOrEmpty(productVersion))
            {
                return string.Format(NcmWebStrings.WEBDATA_VK_16, productVersion);
            }
            return string.Empty;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ValidatePageAccess();
    }

    private void ValidatePageAccess()
    {
        if (!CommonHelper.IsDemoMode() && !(this.Page is IBypassAccessLimitation) && !Profile.AllowAdmin && !SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR))
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={NcmWebStrings.WEBDATA_VK_554}");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = NcmWebStrings.WEBDATA_VK_01;

        SWISValidator validator = new SWISValidator();
        validator.ContentContainer = ContentContainer;
        validator.RedirectIfValidationFailed = true;
        divError.Controls.Add(validator);

        if (validator.IsValid)
        {
            //column 1
            nodeManagement.AddLink(OrionConfiguration.IsDemoServer ? $@"javascript:demoAction('{DemoModeConstants.NCM_SETTINGS_ADD_OR_MANAGE_NODES}', this);"
                : @"/Orion/Nodes/Default.aspx", NcmWebStrings.WEBDATA_VK_09, 10);

            if (!SolarWinds.Orion.Core.Common.RegistrySettings.IsUnifiedITManager())
            {
                nodeManagement.AddLink(OrionConfiguration.IsDemoServer ? $@"javascript:demoAction('{DemoModeConstants.NCM_SETTINGS_NODE_LICENSING}', this);"
                    : @"/Orion/Admin/Details/ModulesDetailsHost.aspx", NcmWebStrings.WEBDATA_VK_601, 15);
            }

            nodeManagement.AddLink(OrionConfiguration.IsDemoServer ? $@"javascript:demoAction('{DemoModeConstants.NCM_SETTINGS_NODE_INVENTORY}', this);"
                : @"/Orion/NCM/Admin/Settings/InventorySettings.aspx", NcmWebStrings.WEBDATA_VK_624, 20);

            configs.AddLink(OrionConfiguration.IsDemoServer ? $@"javascript:demoAction('{DemoModeConstants.NCM_SETTINGS_CONFIGS}', this);"
                : @"/Orion/NCM/Admin/Settings/ConfigSettings.aspx", NcmWebStrings.WEBDATA_VK_598, 10);
            configs.AddLink(OrionConfiguration.IsDemoServer ? $@"javascript:demoAction('{DemoModeConstants.NCM_SETTINGS_CONFIG_TYPES}', this);"
                : @"/ui/ncm/configTypes", NcmWebStrings.SettingsConfigTypes, 15);
            configs.AddLink(OrionConfiguration.IsDemoServer ? $@"javascript:demoAction('{DemoModeConstants.NCM_SETTINGS_COMPARISON_CRITERIA}', this);"
                : @"/Orion/NCM/Admin/Settings/ComparisonCriteria.aspx", NcmWebStrings.WEBDATA_VK_555, 20);
            configs.AddLink(OrionConfiguration.IsDemoServer ? $@"javascript:demoAction('{DemoModeConstants.NCM_SETTINGS_CONFIG_ARCHIVE_FOLDER_LOCATIONS}', this);"
                : @"/Orion/NCM/Admin/Settings/ConfigArchiveFolders.aspx", NcmWebStrings.WEBDATA_VK_975, 25);
            configs.AddLink(@"/Orion/NCM/Admin/Settings/BinaryConfigStorageSettings.aspx", NcmWebStrings.WEBDATA_VK_1045, 30);

            network.AddLink(OrionConfiguration.IsDemoServer ? $@"javascript:demoAction('{DemoModeConstants.NCM_SETTINGS_PROTOCOL_SETTINGS}', this);"
                : @"/Orion/NCM/Admin/Settings/NetworkSettings.aspx", NcmWebStrings.WEBDATA_VK_614, 10);
            network.AddLink(OrionConfiguration.IsDemoServer ? $@"javascript:demoAction('{DemoModeConstants.NCM_SETTINGS_SCP}', this);"
                : @"/Orion/NCM/Admin/Settings/SCPServer.aspx", NcmWebStrings.WEBDATA_VK_615, 15);
            network.AddLink(OrionConfiguration.IsDemoServer ? $@"javascript:demoAction('{DemoModeConstants.NCM_SETTINGS_TFTP}', this);"
                : @"/Orion/NCM/Admin/Settings/TFTPServer.aspx", NcmWebStrings.WEBDATA_VK_616, 20);

            notifications.AddLink(OrionConfiguration.IsDemoServer ? $@"javascript:demoAction('{DemoModeConstants.NCM_SETTINGS_EMAIL}', this);"
                : @"/Orion/NCM/Admin/Settings/EmailDefaults.aspx", NcmWebStrings.WEBDATA_VK_606, 10);
            notifications.AddLink(OrionConfiguration.IsDemoServer ? $@"javascript:demoAction('{DemoModeConstants.NCM_SETTINGS_SMTP}', this);"
                : @"/Orion/NCM/Admin/Settings/SMTPServer.aspx", NcmWebStrings.WEBDATA_VK_607, 15);

            //RTCD is available for demo
            rtnSettings.AddLink(@"/Orion/NCM/Admin/Settings/RTNSettings.aspx", NcmWebStrings.WEBDATA_VK_619, 10);

            //column 2
            policyManagement.AddLink(OrionConfiguration.IsDemoServer ? $@"javascript:demoAction('{DemoModeConstants.NCM_SETTINGS_POLICY_REPORTS}', this);"
                : @"/Orion/NCM/Admin/PolicyReports/PolicyReportManagement.aspx", NcmWebStrings.WEBDATA_VK_12, 10);
            policyManagement.AddLink(OrionConfiguration.IsDemoServer ? $@"javascript:demoAction('{DemoModeConstants.NCM_SETTINGS_MANAGE_POLICIES}', this);"
                : @"/Orion/NCM/Admin/PolicyReports/PolicyManagement.aspx", NcmWebStrings.WEBDATA_VK_13, 15);
            policyManagement.AddLink(OrionConfiguration.IsDemoServer ? $@"javascript:demoAction('{DemoModeConstants.NCM_SETTINGS_REPORTS_ON_THWACK}', this);"
                : @"/Orion/NCM/Admin/PolicyReports/Thwack/Default.aspx", NcmWebStrings.WEBDATA_VK_15, 20);
            policyManagement.AddLink(OrionConfiguration.IsDemoServer ? $@"javascript:demoAction('{DemoModeConstants.NCM_SETTINGS_MANAGE_RULES}', this);"
                : @"/Orion/NCM/Admin/PolicyReports/PolicyRuleManagement.aspx", NcmWebStrings.WEBDATA_VK_14, 25);
            policyManagement.AddLink(OrionConfiguration.IsDemoServer ? $@"javascript:demoAction('{DemoModeConstants.NCM_SETTINGS_MANAGE_VIOLATION_LEVELS}', this);"
                : @"/Orion/NCM/Admin/PolicyReports/PolicyRuleViolation.aspx", NcmWebStrings.Compliance_ManageViolationLevels, 30);

            security.AddLink(OrionConfiguration.IsDemoServer ? $@"javascript:demoAction('{DemoModeConstants.NCM_SETTINGS_SECURITY}', this);"
                : @"/Orion/NCM/Admin/Settings/Security.aspx", NcmWebStrings.WEBDATA_VK_498, 10);
            security.AddLink(OrionConfiguration.IsDemoServer ? $@"javascript:demoAction('{DemoModeConstants.NCM_SETTINGS_USER_LEVEL_CREDS}', this);"
                : @"/Orion/NCM/Admin/Settings/UserLevelLoginCreds.aspx", NcmWebStrings.WEBDATA_VK_713, 15);

            globalDeviceDefaults.AddLink(OrionConfiguration.IsDemoServer ? $@"javascript:demoAction('{DemoModeConstants.NCM_SETTINGS_GLOBAL_DEVICE_DEFAULTS}', this);"
                : @"/Orion/NCM/Admin/Settings/GlobalDeviceDefaults.aspx", NcmWebStrings.WEBDATA_VK_621, 10);
            //Manage Connection Profiles is available in demo
            globalDeviceDefaults.AddLink(@"/Orion/NCM/Admin/ConnectionProfiles/ProfileManagement.aspx", NcmWebStrings.WEBDATA_VM_217, 15);

            advanced.AddLink(OrionConfiguration.IsDemoServer ? $@"javascript:demoAction('{DemoModeConstants.NCM_SETTINGS_ADVANCED_SETTINGS}', this);"
                : @"/Orion/NCM/Admin/Settings/AdvancedSettings.aspx", NcmWebStrings.WEBDATA_VK_610, 10);
            advanced.AddLink(OrionConfiguration.IsDemoServer ? $@"javascript:demoAction('{DemoModeConstants.NCM_SETTINGS_MANAGE_MACROS}', this);"
                : @"/Orion/NCM/Admin/Settings/CustomMacros.aspx", NcmWebStrings.WEBDATA_VK_611, 15);
            advanced.AddLink(OrionConfiguration.IsDemoServer ? $@"javascript:demoAction('{DemoModeConstants.NCM_SETTINGS_SEARCH_SETTINGS}', this);"
                : @"/Orion/NCM/Admin/Settings/SearchSettings.aspx", NcmWebStrings.WEBDATA_VK_724, 20);
            advanced.AddLink(OrionConfiguration.IsDemoServer ? $@"javascript:demoAction('{DemoModeConstants.NCM_SETTINGS_VULNERABILITY}', this);"
                : @"/Orion/NCM/Admin/Settings/VulnerabilitySettings.aspx", NcmWebStrings.WEBDATA_VK_1014, 25);

            //Manage NCM Approval Requests is available in demo
            approval.AddLink(@"/Orion/NCM/Admin/ConfigChangeApproval/ApprovalRequests.aspx", NcmWebStrings.WEBDATA_VK_04, 10);
            approval.AddLink(OrionConfiguration.IsDemoServer ? $@"javascript:demoAction('{DemoModeConstants.NCM_SETTINGS_SETUP_WIZARD}', this);"
                : @"/Orion/NCM/Admin/ConfigChangeApproval/ApprovalSetup.aspx", NcmWebStrings.WEBDATA_VK_05, 15);
            //Accounts management is available in demo
            approval.AddLink(@"/Orion/Admin/Accounts/Accounts.aspx", NcmWebStrings.WEBDATA_VK_06, 20);

            firmwareUpgrade.AddLink(@"/Orion/NCM/Admin/Firmware/FirmwareStorageLocation.aspx", NcmWebStrings.FirmwareUpgradeSettings_Label_UpgradeSettings, 10);
            firmwareUpgrade.AddLink(@"/Orion/NCM/Admin/Firmware/FirmwareDefinitions.aspx", NcmWebStrings.FirmwareUpgradeSetting_FirmwareUpgradeTemplates, 15);
            firmwareUpgrade.AddLink(@"/Orion/NCM/Admin/Firmware/FirmwareImagesStorage.aspx", NcmWebStrings.FirmwareUpgradeSetting_FirmwareRepository, 20);
            firmwareUpgrade.AddLink(@"/Orion/NCM/Admin/Firmware/Thwack/ThwackFirmwareDefinitions.aspx", NcmWebStrings.FirmwareUpgradeSetting_FirmwareUpgradeTemplatesOnThwack, 25);
        }
    }

    private string learnMoreTemplate = @"<span style='font-size:8pt;'>&nbsp;&#0187;&nbsp;<a href='{0}' target='__blank'>{1}</a></span>";

    protected string LearnMore(string fragment, string text)
    {
        return string.Format(learnMoreTemplate, helpHelperWrap.GetHelpUrl(fragment), text);
    }
}
