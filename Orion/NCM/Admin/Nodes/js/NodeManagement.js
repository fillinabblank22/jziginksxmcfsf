﻿var addNodesEnabled = function (grid) {
    var nodeIds = grid.GetSelectedNodeIDs();

    if (nodeIds.length == 0) {
        return false;
    }
    else {
        var enabled = false;
        callWebMethod('/Orion/NCM/Services/NodeManagement.asmx/AddNodesEnabled', false, { coreNodeIds: nodeIds, where: grid.GetWhere("N"), allSelected: grid.allSelected }, function (result) { enabled = result.d; return; });
        return enabled;
    }
}

var removeNodesEnabled = function (grid) {
    var nodeIds = grid.GetSelectedNodeIDs();

    if (nodeIds.length == 0) {
        return false;
    }
    else {
        var enabled = false;
        callWebMethod('/Orion/NCM/Services/NodeManagement.asmx/RemoveNodesEnabled', false, { coreNodeIds: nodeIds, where: grid.GetWhere("N"), allSelected: grid.allSelected }, function (result) { enabled = result.d; return; });
        return enabled;
    }
}

var addNodesCallback = function (grid) {
    if ($("#isDemoMode").length != 0) {
        demoAction('NCM_addNodesCallback');
    }
    else {
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_32;E=js}');
        callWebMethod('/Orion/NCM/Services/NodeManagement.asmx/AddNodes', true, { coreNodeIds: grid.GetSelectedNodeIDs(), where: grid.GetWhere("N"), allSelected: grid.allSelected }, function (result) {
            waitMsg.hide();
            errorHandler(result.d);
            if (result.d == null) {
                Ext.Msg.show({
                    title: '@{R=NCM.Strings;K=WEBJS_VK_33;E=js}',
                    msg: '@{R=NCM.Strings;K=WEBJS_VK_34;E=js}',
                    minWidth: 500,
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO
                });
                var groupBy = Utils.SelectedProperty();
                if (groupBy == 'NCMLicenseStatus.LicensedByNCM') {
                    grid.LoadGroupByValues();
                }
                else {
                    grid.RefreshObjects();
                }
            }
        });
    }
}

var removeNodesCallback = function (grid) {
    if ($("#isDemoMode").length != 0) {
        demoAction('NCM_removeNodesCallback');
    }
    else {
        Ext.Msg.show({
            title: '@{R=NCM.Strings;K=WEBJS_VK_35;E=js}',
            msg: '@{R=NCM.Strings;K=WEBJS_VK_36;E=js}',
            buttons: {
                ok: '@{R=NCM.Strings;K=WEBJS_VK_37;E=js}',
                cancel: '@{R=NCM.Strings;K=WEBJS_VK_38;E=js}'
            },
            icon: Ext.MessageBox.QUESTION,
            fn: function (btn) {
                if (btn == 'ok') {
                    var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_39;E=js}');
                    callWebMethod('/Orion/NCM/Services/NodeManagement.asmx/RemoveNodes', true, { coreNodeIds: grid.GetSelectedNodeIDs(), where: grid.GetWhere("N"), allSelected: grid.allSelected }, function (result) {
                        waitMsg.hide();
                        errorHandler(result.d);
                        if (result.d == null) {
                            var groupBy = Utils.SelectedProperty();
                            if (groupBy == 'NCMLicenseStatus.LicensedByNCM') {
                                grid.LoadGroupByValues();
                            }
                            else {
                                grid.RefreshObjects();
                            }
                        }
                    });
                }
            }
        });
    }
}

var errorHandler = function (result) {
    if (result != null && result.Error) {
        Ext.Msg.show({
            title: result.Source,
            msg: result.Msg,
            minWidth: 500,
            buttons: Ext.Msg.OK,
            icon: Ext.MessageBox.ERROR
        });
    }
}

var callWebMethod = function (url, async, data, success, failure) {
    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: async,
        success: function (result) {
            if (success)
                success(result);
        },
        error: function (response) {
            if (failure)
                failure(response);
            else {
                alert(response);
            }
        }
    });
}