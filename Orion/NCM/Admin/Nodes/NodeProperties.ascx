<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeProperties.ascx.cs" Inherits="Orion_NCM_Nodes_NodeProperties" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<script type="text/javascript">
    var isNcmModuleOnly = <%=IsNcmModuleOnly.ToString().ToLowerInvariant()%>;
    var actualNodesCount = <%=ActualNodesCount%>;
    var licenseNodesCount = <%=LicenseNodesCount%>;
    var pluginState = '<%=PluginState%>';
    var pluginMode = '<%=PluginMode%>';
    var isMultiselected = <%=IsMultiselected.ToString().ToLowerInvariant()%>;

    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function() {
        if ((pluginMode == 'EditProperies') || (!isNcmModuleOnly && pluginMode == 'WizChangeProperties')) {
            var value = $('#<%=ManageNodesWithNCM.ClientID%>').val();
            ShowHideNodeProperties(value);
        }
    });

    function ManageNodesWithNCMChange(obj) {
        var value = obj.options[obj.selectedIndex].value;
        if (!isNcmModuleOnly && value == 'Add') {
            if (pluginMode == 'WizChangeProperties') {
                var nodesCountToAdd = <%=NodesCountToAdd%>;
                if (IsLicenseOverLimit(nodesCountToAdd, actualNodesCount, licenseNodesCount))
                {
                    value = 'Remove';
                    obj.options[1].selected = true;
                }
            }
            else if (pluginMode == 'EditProperies' && (pluginState == 'Remove' || pluginState == 'KeepUnchanged' || pluginState == 'Never'))
            {
                var nodesCountToAdd = <%=NodesCountToAdd%>;
                if (IsLicenseOverLimit(nodesCountToAdd, actualNodesCount, licenseNodesCount))
                {
                    value = pluginState;
                    if (pluginState == 'KeepUnchanged')
                        obj.options[0].selected = true;
                    else if (pluginState == 'Remove')
                        obj.options[1].selected = true;
                    else if (pluginState == 'Never')
                        obj.options[3].selected = true;
                }
            }
        }
        ShowHideNodeProperties(value);
    }

    function ShowHideNodeProperties(value) {
        if (value == 'Add' || value == 'KeepUnchanged') {
            $("[id$='_propertiesHolder']").show();
        }
        else {
            $("[id$='_propertiesHolder']").hide();
        }
    }

    function IsLicenseOverLimit(nodesCountToAdd, actualNcmNodesCount, licenseNcmNodesCount)
    {
        if ((actualNcmNodesCount + nodesCountToAdd) > licenseNcmNodesCount) {
            var licenseOverLimitMsg = String.format('<%=Resources.NCMWebContent.WEBDATA_VK_720 %>',
            '\n',
            licenseNcmNodesCount,
            actualNcmNodesCount);

            if (licenseNcmNodesCount - actualNcmNodesCount > 0) {
                licenseOverLimitMsg = String.format('{0}\n{1}', licenseOverLimitMsg, String.format('<%=Resources.NCMWebContent.WEBDATA_VK_733 %>', licenseNcmNodesCount - actualNcmNodesCount));
            }

            alert(licenseOverLimitMsg);

            return true;
        }

        return false;
    }

    function HideDeviceValidationBar() {
        $("[id$='_DeviceValidationBar']").hide();
    }

    function ValidateNcmNodeProperties() {
        var rfvProfileName = $("[id$='_rfvProfileName']")[0];

        if (rfvProfileName) {
            ValidatorEnable(rfvProfileName, true);
            return rfvProfileName.isvalid;
        } else {
            return true;
        }
    }

    function ValidateLoginButtonClick() {
        HideDeviceValidationBar();
        return ValidateNcmNodeProperties();
    }
</script>

<style type="text/css">
    .pluginHeader
    {
        font-size:14px;
        font-weight:bold;
    }
        
    .inputValueColumn
    {
        width: 225px;
    }
    
    .macroValueColumn
    {
        width: 150px;
        white-space: nowrap;
        padding-right: 20px;
    }
</style>

<div class="contentBlock" id="mainHolder" runat="server">
    <div id="manageNodesWithNCMHolder" runat="server" style="padding-top:10px;">
        <table width="100%">
            <tr>
                <td class="leftLabelColumn">&nbsp;</td>
                <td class="rightInputColumn">
                    <asp:DropDownList ID="ManageNodesWithNCM" runat="server" onchange="ManageNodesWithNCMChange(this);" AutoPostBack="true" OnSelectedIndexChanged="ManageNodesWithNCM_SelectedIndexChanged">
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_985 %>" Value="KeepUnchanged" Enabled="false"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_311 %>" Value="Add"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_312 %>" Value="Remove"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_1002 %>" Value="Never"></asp:ListItem>
                    </asp:DropDownList>
                    <span><%=Resources.NCMWebContent.WEBDATA_VK_620 %></span>
                </td>
            </tr>
        </table>
    </div>

    <div id="propertiesHolder" runat="server">
        <div class="pluginHeader">
            <%=Resources.NCMWebContent.WEBDATA_VK_289 %>
        </div>
        
        <div class="contentBlockHeader" style="padding-top:10px;">
            <asp:CheckBox runat="server" id="NodeDetails" AutoPostBack="true" OnCheckedChanged="NodeDetails_CheckedChanged" /> <%=Resources.NCMWebContent.WEBDATA_VK_291 %>
        </div>
        <table class="blueBox" width="100%">
            <tr>
                <td class="leftLabelColumn">
                    <%=Resources.NCMWebContent.WEBDATA_VK_292 %>
                </td>
                <td class="rightInputColumn">
                    <asp:TextBox runat="server" ID="NodeGroup" Width="185px"></asp:TextBox>
                    <ajaxToolkit:AutoCompleteExtender ID="ace" 
                        runat="server"
                        TargetControlID="NodeGroup"
                        ServiceMethod="GetNodeGroupValueList"
                        ServicePath="~/Orion/NCM/Services/ConfigManagement.asmx"
                        MinimumPrefixLength="1"
                        CompletionInterval="1000"
                        EnableCaching="true"
                        CompletionSetCount="20"
                        UseContextKey="false">
                    </ajaxToolkit:AutoCompleteExtender>
                </td>
            </tr>
            <tr>
                <td class="leftLabelColumn">
                    <%=Resources.NCMWebContent.WEBDATA_VK_293 %>
                </td>
                <td class="rightInputColumn">
                    <asp:TextBox runat="server" ID="Comments" TextMode="MultiLine" Font-Names="Arial,Verdana,Helvetica,sans-serif" Font-Size="9pt" Height="50px" Width="185px" spellcheck="false"></asp:TextBox>
                </td>
            </tr>
        </table>    
        
        <div>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="contentBlockHeader" style="padding-top:10px;">
                        <asp:CheckBox runat="server" ID="ConnectionProfile" AutoPostBack="true" OnCheckedChanged="ConnectionProfile_CheckedChanged" /> <%=Resources.NCMWebContent.WEBDATA_VK_859 %>
                    </td>
                    <td style="text-align:right;">
                        <img src="/Orion/NCM/Resources/images/ConfigSnippets/icon_editsnippet.gif" alt="" />
                        <a href="/Orion/NCM/Admin/ConnectionProfiles/ProfileManagement.aspx" class="RenewalsLink" target="_blank">
                            <%=Resources.NCMWebContent.WEBDATA_VK_857 %>
                        </a> 
                    </td>
                </tr>
            </table>
        </div>
        <table id="connectionProfileTable" runat="server" class="blueBox" width="100%">
            <tr>
                <td class="leftLabelColumn">
                    <%=Resources.NCMWebContent.WEBDATA_VK_860 %>
                </td>
                <td class="rightInputColumn inputValueColumn">
                    <asp:DropDownList ID="GlobalConnectionProfile" runat="server" AutoPostBack="true" OnSelectedIndexChanged="GlobalConnectionProfile_SelectedIndexChanged" Width="225px">
                    </asp:DropDownList>
                </td>
                <td class="rightInputColumn" colspan="2">&nbsp;</td>
            </tr>
            <tr id="NewProfileHolder" runat="server" visible="false">
                <td class="leftLabelColumn">
                    <%=Resources.NCMWebContent.WEBDATA_VK_861 %>
                </td>
                <td class="rightInputColumn inputValueColumn">
                    <asp:TextBox runat="server" ID="ProfileName" Width="220px"></asp:TextBox>
                </td>
                <td class="rightInputColumn" colspan="2">
                    <asp:RequiredFieldValidator ID="rfvProfileName"
                            ControlToValidate="ProfileName"
                            Display="Dynamic"
                            ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_855 %>"
                            runat="server"/>
                </td>
            </tr>
            <tr>
                <td class="leftLabelColumn">
                    <%=Resources.NCMWebContent.WEBDATA_VK_295 %>
                </td>
                <td class="rightInputColumn inputValueColumn">
                    <asp:DropDownList ID="UseUserDeviceCredentials" runat="server" AutoPostBack="true" OnSelectedIndexChanged="UseUserDeviceCredentials_SelectedIndexChanged" Width="225px">
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_317 %>" Value="True"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_318 %>" Value="False" Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="rightInputColumn" colspan="2">&nbsp;</td>
            </tr>
            <tr visible-col-index="2">
                <td class="leftLabelColumn">
                    <%=Resources.NCMWebContent.WEBDATA_VK_296 %>
                </td>
                <td class="rightInputColumn inputValueColumn">
                    <ncm:PasswordTextBox ID="Username" runat="server" Width="220px"/>
                </td>
                <td class="rightInputColumn macroValueColumn">
                    <asp:Literal ID="UsernameValue" runat="server"></asp:Literal>
                </td>
                <td class="rightInputColumn">
                    <asp:LinkButton runat="server" ID="GlobalUsername" Font-Underline="true" OnClick="ResetToGlobalMacro_Click"></asp:LinkButton>
                </td>
            </tr>
            <tr visible-col-index="2">
                <td class="leftLabelColumn">
                    <%=Resources.NCMWebContent.WEBDATA_VK_297 %>
                </td>
                <td class="rightInputColumn inputValueColumn">
                    <ncm:PasswordTextBox ID="Password" runat="server" Width="220px"/>
                </td>
                <td class="rightInputColumn macroValueColumn">&nbsp;</td>
                <td class="rightInputColumn">
                    <asp:LinkButton runat="server" ID="GlobalPassword" Font-Underline="true" OnClick="ResetToGlobalMacro_Click"></asp:LinkButton>
                </td>
            </tr>
            <tr visible-col-index="2">
                <td class="leftLabelColumn">
                    <%=Resources.NCMWebContent.WEBDATA_VK_298 %>
                </td>
                <td class="rightInputColumn inputValueColumn">
                    <asp:DropDownList ID="EnableLevel" runat="server" Width="225px">
                        <asp:ListItem Text="${GlobalEnableLevel}" Value="${GlobalEnableLevel}" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_314 %>" Value="<No Enable Login>"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_315 %>" Value="enable"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:HiddenField runat="server" ID="HiddenEnableLevel" />
                </td>
                <td class="rightInputColumn macroValueColumn">
                    <asp:Literal ID="EnableLevelValue" runat="server"></asp:Literal>
                </td>
                <td class="rightInputColumn">&nbsp;</td>
            </tr>
            <tr visible-col-index="2">
                <td class="leftLabelColumn">
                    <%=Resources.NCMWebContent.WEBDATA_VK_299 %>
                </td>
                <td class="rightInputColumn inputValueColumn">
                    <ncm:PasswordTextBox ID="EnablePassword" runat="server" Width="220px"/>
                </td>
                <td class="rightInputColumn macroValueColumn">&nbsp;</td>
                <td class="rightInputColumn">
                    <asp:LinkButton runat="server" ID="GlobalEnablePassword" Font-Underline="true" OnClick="ResetToGlobalMacro_Click"></asp:LinkButton>
                </td>
            </tr>
            <tr visible-col-index="2">
                <td class="leftLabelColumn">
                    <%=Resources.NCMWebContent.WEBDATA_VK_302 %>
                </td>
                <td class="rightInputColumn inputValueColumn">
                    <asp:DropDownList ID="ExecProtocol" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ExecProtocol_SelectedIndexChanged" Width="225px">
                    </asp:DropDownList>
                </td>
                <td class="rightInputColumn macroValueColumn">
                    <asp:Literal ID="ExecProtocolValue" runat="server"></asp:Literal>
                </td>
                <td class="rightInputColumn">&nbsp;</td>
            </tr>
            <tr visible-col-index="2">
                <td class="leftLabelColumn">
                    <%=Resources.NCMWebContent.WEBDATA_VK_303 %>
                </td>
                <td class="rightInputColumn inputValueColumn">
                    <asp:DropDownList ID="CommandProtocol" runat="server" AutoPostBack="true" OnSelectedIndexChanged="CommandProtocol_SelectedIndexChanged" Width="225px">
                    </asp:DropDownList>
                </td>
                <td class="rightInputColumn macroValueColumn">
                    <asp:Literal ID="CommandProtocolValue" runat="server"></asp:Literal>
                </td>
                <td class="rightInputColumn">&nbsp;</td>
            </tr>
            <tr visible-col-index="2">
                <td class="leftLabelColumn">
                    <%=Resources.NCMWebContent.WEBDATA_VK_304 %>
                </td>
                <td class="rightInputColumn inputValueColumn">
                    <asp:DropDownList ID="TransferProtocol" runat="server" Width="225px"/>
                </td>
                <td class="rightInputColumn macroValueColumn">
                    <asp:Literal ID="TransferProtocolValue" runat="server"></asp:Literal>
                </td>
                <td class="rightInputColumn">&nbsp;</td>
            </tr>
            <tr visible-col-index="2">
                <td class="leftLabelColumn">
                    <%=Resources.NCMWebContent.WEBDATA_VK_305 %>
                </td>
                <td class="rightInputColumn inputValueColumn">
                    <asp:TextBox runat="server" ID="TelnetPort" Width="220px"></asp:TextBox>
                </td>
                <td class="rightInputColumn macroValueColumn">
                    <asp:Literal ID="TelnetPortValue" runat="server"></asp:Literal>
                </td>
                <td class="rightInputColumn">
                    <asp:LinkButton runat="server" ID="GlobalTelnetPort" Font-Underline="true" OnClick="ResetToGlobalMacro_Click"></asp:LinkButton>
                </td>
            </tr>
            <tr visible-col-index="2">
                <td class="leftLabelColumn">
                    <%=Resources.NCMWebContent.WEBDATA_VK_306 %>
                </td>
                <td class="rightInputColumn inputValueColumn">
                    <asp:TextBox runat="server" ID="SSHPort" Width="220px"></asp:TextBox>
                </td>
                <td class="rightInputColumn macroValueColumn">
                    <asp:Literal ID="SSHPortValue" runat="server"></asp:Literal>
                </td>
                <td class="rightInputColumn">
                    <asp:LinkButton runat="server" ID="GlobalSSHPort" Font-Underline="true" OnClick="ResetToGlobalMacro_Click"></asp:LinkButton>
                </td>
            </tr>
            <tr id="deviceValidationHolder" runat="server">
	            <td class="leftLabelColumn">&nbsp;</td>
	            <td class="rightInputColumn" colspan="3">
                    <div id="DeviceValidationBar" runat="server" style="display:none;width:100%">
                        <div runat="server" id="DeviceValidationInfo" style="padding:3px; text-align:left; margin:5px 0px 5px 0px;" visible="false">
                            <img alt="" runat="server" id="DeviceValidationIMG" style="vertical-align:middle;" />
                            <asp:Label ID="DeviceValidationResult" runat="server" style="vertical-align:middle;" />
                        </div>
                    
                        <div id="DeviceValidationDetailsSPAN" runat="server" style="padding-bottom:5px;" visible="false">
                            <asp:LinkButton runat="server" ID="DeviceValidationDetailsBTN" style="vertical-align:middle;" Font-Underline="false" Font-Size="8pt" OnClick="DeviceValidationDetailsBTN_Click" CausesValidation="false">
                                <img alt="" id="DeviceValidationDetailsIMG" style="vertical-align:middle;" runat="server" src="/Orion/NCM/Resources/images/arrowright.gif" />
                                <asp:Label ID="DeviceValidationDetailsLBL" runat="server" style="vertical-align:middle;padding-left:5px;"></asp:Label>
                            </asp:LinkButton>
                        </div>
                    
                        <div id="TraceOutput" runat="server" style="padding-bottom:5px;">
		                    <asp:TextBox runat="server" BackColor="#E6E6DD" ForeColor="#5D5C5E" ID="DeviceValidationOutput" TextMode="MultiLine" Font-Names="Arial,Verdana,Helvetica,sans-serif" Font-Size="9pt" ReadOnly="True" Height="150px" Width="400px" spellcheck="false"></asp:TextBox>
	                    </div>
                    </div>
	            </td>
            </tr>
            <tr id="buttonsHolder" runat="server" visible-col-index="2">
                <td class="leftLabelColumn">&nbsp;</td>
                <td class="rightInputColumn">
                    <orion:LocalizableButton runat="server" ID="btnValidateLogin" LocalizedText="Test" OnClientClick="return ValidateLoginButtonClick();" OnClick="btnValidateLogin_Click" DisplayType="Small" CausesValidation="false" />
                </td>
                <td>
                    <orion:LocalizableButton runat="server" ID="btnShowMacroValues" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_964 %>" OnClick="btnShowMacroValues_Click" DisplayType="Small" CausesValidation="false" />
                </td>
                <td class="rightInputColumn">&nbsp;</td>
            </tr>
        </table>

        <div class="contentBlockHeader" style="padding-top:10px;">
            <asp:CheckBox runat="server" id="Communication" AutoPostBack="true" OnCheckedChanged="Communication_CheckedChanged" /> <%=Resources.NCMWebContent.WEBDATA_VK_300 %>
        </div>
        <table class="blueBox" width="100%">
            <tr>
                <td class="leftLabelColumn">                
                    <%=Resources.NCMWebContent.WEBDATA_VK_308 %>
                </td>
                <td class="rightInputColumn">
                    <asp:DropDownList ID="AllowIntermediary" runat="server">
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_311 %>" Value="True"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_312 %>" Value="False" Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="leftLabelColumn">
                    <%=Resources.NCMWebContent.WEBDATA_VK_309 %>
                </td>
                <td class="rightInputColumn">
                    <asp:DropDownList ID="UseKeyboardInteractiveAuthentication" runat="server">
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_311 %>" Value="True"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, WEBDATA_VK_312 %>" Value="False" Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr id="EncryptionAlgorithmHolder" runat="server">
                <td class="leftLabelColumn">
                    <%=Resources.NCMWebContent.WEBDATA_IC_238 %>
                </td>
                <td class="rightInputColumn">
                    <asp:DropDownList ID="EncryptionAlgorithm" runat="server">
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, EncryptionAlgorithm_Any %>" Value="0" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, EncryptionAlgorithm_AES %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, EncryptionAlgorithm_AES128 %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, EncryptionAlgorithm_AES192 %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, EncryptionAlgorithm_AES256 %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, EncryptionAlgorithm_DES %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, EncryptionAlgorithm_3DES %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, EncryptionAlgorithm_BLOWFISH %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources: NCMWebContent, EncryptionAlgorithm_ChaCha20Poly1305 %>" Value="8"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </div>
</div>
