using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net;
using System.Globalization;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.Controls;
using SolarWinds.NCMModule.Web.Resources.NodeManagement;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Orion.Cli.Contracts;
using SolarWinds.Orion.Cli.Contracts.DataModel;
using SolarWinds.Logging;
using SolarWinds.NCM.Common.BusinessLayer.Interfaces;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.NCM.Contracts;
using SolarWinds.NCM.Web.Contracts;
using SolarWinds.NCMModule.Web.Resources.NCMCore;
using CommonHelper = SolarWinds.NCMModule.Web.Resources.CommonHelper;

public partial class Orion_NCM_Nodes_NodeProperties : UserControl, INodePropertyPlugin
{
    private readonly IIsWrapper _isLayer;
    private readonly INcmBusinessLayerProxyHelper blProxy;
    private static readonly Log _log = new Log();

    protected IList<Node> _nodes;
    private NodePropertyPluginExecutionMode _mode;
    private Dictionary<string, object> _pluginState;
    private string pageGuid;

    private const string __nodeIp = "__nodeIp";
    private const string Oid_System = "1.3.6.1.2.1.1";
    private const string sysObjectID = "2.0";
    private const string sysDescr = "1.0";
    private readonly string[] productTags = new[] { @"Orion", @"OLM", @"NCM" }; //products to ignore to assume NCM is standalone
    private readonly IDataDecryptor decryptor;
    private readonly IMacroParser macroParser;
    private readonly ILicenseWrapper licenseWrapper;
    private readonly ICommonHelper commonHelper;

    public Orion_NCM_Nodes_NodeProperties()
    {
        _isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
        decryptor = ServiceLocator.Container.Resolve<IDataDecryptor>();
        macroParser = ServiceLocator.Container.Resolve<IMacroParser>();
        licenseWrapper = ServiceLocator.Container.Resolve<ILicenseWrapper>();
        blProxy = ServiceLocator.Container.Resolve<INcmBusinessLayerProxyHelper>();
        commonHelper = ServiceLocator.Container.Resolve<ICommonHelper>();
    }

    #region Properties

    protected bool IsNcmModuleOnly
    {
        get
        {
            if (SecurityHelper.IsValidNcmAccountRole)
            {
                if (ViewState["NCM_IsNcmModuleOnly"] == null)
                {
                    var modulesCount = SolarWinds.Orion.Web.Helpers.ModuleDetailsHelper.LoadModuleInfoForEngines(@"Primary", false)
                        .Count(moduleInfo => !productTags.Contains(moduleInfo.ProductTag, StringComparer.InvariantCultureIgnoreCase));
                    var isNcmModuleOnly = modulesCount == 0;

                    if (isNcmModuleOnly && _mode == NodePropertyPluginExecutionMode.EditProperies)
                    {
                        isNcmModuleOnly = NcmNodeHelper.GetSelectedNcmNodesCount(_nodes) == _nodes.Count;
                    }
                    ViewState["NCM_IsNcmModuleOnly"] = isNcmModuleOnly;
                }
                return Convert.ToBoolean(ViewState["NCM_IsNcmModuleOnly"]);
            }
            else
            {
                return true;
            }
        }
    }

    protected string PluginState
    {
        get { return  HttpUtility.HtmlEncode(Convert.ToString(ViewState["NCM_PluginState"])); }
        set { ViewState["NCM_PluginState"] = value; }
    }

    protected int ConnectionProfileId
    {
        get { return Convert.ToInt32(ViewState["NCM_ConnectionProfileId"]); }
        set { ViewState["NCM_ConnectionProfileId"] = value; }
    }

    public bool IsMultiselected
    {
        get
        {
            IList<int> Ids = null;
            Ids = NodeWorkflowHelper.GetMultiSelectedNodeIds(pageGuid);
            if (Ids.Count == 0 && NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(pageGuid) != null)
                return NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(pageGuid).Count > 1;

            if (Ids == null) return false;

            return (Ids.Count > 1);
        }
    }

    protected new bool Visible { get; private set; } = true;

    protected bool HideUsernames
    {
        get
        {
            if (SecurityHelper.IsValidNcmAccountRole)
            {
                var hideUsernames = ViewState["NCM_HideUsernames"] as string;
                if (string.IsNullOrEmpty(hideUsernames))
                {
                    using (var proxy = _isLayer.GetProxy())
                    {
                        hideUsernames = proxy.Cirrus.Settings.GetSetting(Settings.HideUsernames, false, null);
                        ViewState["NCM_HideUsernames"] = hideUsernames;
                    }
                }
                return Convert.ToBoolean(hideUsernames);
            }
            else
            {
                return false;
            }
        }
    }

    protected bool UseDeviceLoginCredentials
    {
        get
        {
            if (SecurityHelper.IsValidNcmAccountRole)
            {
                var useDeviceLoginCreds = ViewState["NCM_UseDeviceLoginCredentials"] as string;
                if (string.IsNullOrEmpty(useDeviceLoginCreds))
                {
                    using (var proxy = _isLayer.GetProxy())
                    {
                        useDeviceLoginCreds = proxy.Cirrus.Settings.GetSetting(Settings.ConnectivityLevel, ConnectivityLevels.Default, null);
                        ViewState["NCM_UseDeviceLoginCredentials"] = useDeviceLoginCreds;
                    }
                }

                if (useDeviceLoginCreds.Equals(ConnectivityLevels.Level1))
                    return true;

                return false;
            }
            else
            {
                return true;
            }
        }
    }

    protected string HiddenUsername
    {
        get { return Convert.ToString(ViewState["NCM_HiddenUsername"]); }
        set { ViewState["NCM_HiddenUsername"] = value; }
    }

    protected string HiddenPassword
    {
        get { return Convert.ToString(ViewState["NCM_HiddenPassword"]); }
        set { ViewState["NCM_HiddenPassword"] = value; }
    }

    protected string HiddenEnablePassword
    {
        get { return Convert.ToString(ViewState["NCM_HiddenEnablePassword"]); }
        set { ViewState["NCM_HiddenEnablePassword"] = value; }
    }

    protected int NodesCountToAdd
    {
        get { return Convert.ToInt32(ViewState["NCM_NodesCountToAdd"]); }
        set { ViewState["NCM_NodesCountToAdd"] = value; }
    }

    protected int ActualNodesCount
    {
        get
        {
            if (SecurityHelper.IsValidNcmAccountRole)
            {
                return NcmNodeHelper.GetNcmActualNodesCount();
            }
            else
            {
                return 0;
            }
        }
    }

    protected long LicenseNodesCount
    {
        get
        {
            if (SecurityHelper.IsValidNcmAccountRole)
            {
                return commonHelper.GetLicenseNodesLimit();
            }

            return int.MaxValue;
        }
    }

    protected string PluginMode => _mode.ToString();

    protected bool IsLicenseExpired => licenseWrapper.GetEvaluationDaysLeft() < 0;

    private bool ManageNodesWithNcmEnabled
    {
        set
        {
            _pluginState[@"Ncm_ManageNodesWithNcmEnabled"] = value;
            ShowCliDeviceTemplatePlugin(value);
        }
    }

    private ICoreBusinessLayerProxyCreator coreBusinessLayerProxyCreator
    {
        get { return ServiceLocator.Container.Resolve<ICoreBusinessLayerProxyCreator>(); }
    }

    private bool IsNcmNodeCredentialsPrefilled
    {
        get
        {
            if (!_pluginState.ContainsKey(@"Ncm_IsNodeCredentialsPrefilled"))
            {
                _pluginState[@"Ncm_IsNodeCredentialsPrefilled"] = false;
            }
            return bool.Parse(_pluginState[@"Ncm_IsNodeCredentialsPrefilled"].ToString());
        }
        set
        {
            _pluginState[@"Ncm_IsNodeCredentialsPrefilled"] = value;
        }
    }

    #endregion

    #region Events

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        var results = blProxy.CallMain(x => x.GetCommandProtocols());
        CommonHelper.PopulateDropDownList(results.ExecuteScriptProtocols, ExecProtocol);
        CommonHelper.PopulateDropDownList(results.RequestConfigProtocols, CommandProtocol);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Visible)
        {
            GlobalUsername.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_313, @"${GlobalUsername}");
            GlobalPassword.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_313, @"${GlobalPassword}");
            GlobalEnablePassword.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_313, @"${GlobalEnablePassword}");
            GlobalTelnetPort.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_313, @"${GlobalTelnetPort}");
            GlobalSSHPort.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_313, @"${GlobalSSHPort}");

            if (HideUsernames)
            {
                Username.TextMode = TextBoxMode.Password;
            }
            Password.TextMode = TextBoxMode.Password;
            EnablePassword.TextMode = TextBoxMode.Password;

            var hf = (ControlHelper.FindControlRecursive(Page, @"HiddenFieldGuid") as HiddenField);
            if (hf != null)
            {
                pageGuid = hf.Value;
            }

            if (!Page.IsPostBack)
            {
                LoadGlobalConnectionProfiles();

                //node details
                LoadNodeProperty(@"NodeGroup", NodeGroup, string.Empty);
                LoadNodeProperty(@"NodeComments", Comments, string.Empty);

                //connection profile
                LoadNodeProperty(@"ConnectionProfile", GlobalConnectionProfile, @"-1");
                LoadNodeProperty(@"UseUserDeviceCredentials", UseUserDeviceCredentials, @"False");
                LoadNodeProperty(@"Username", Username, @"${GlobalUsername}");
                LoadNodeProperty(@"Password", Password, @"${GlobalPassword}");
                LoadNodeProperty(@"EnableLevel", EnableLevel, @"${GlobalEnableLevel}");
                LoadNodeProperty(@"EnablePassword", EnablePassword, @"${GlobalEnablePassword}");
                LoadNodeProperty(@"ExecProtocol", ExecProtocol, @"${GlobalExecProtocol}");
                ExecProtocol_SelectedIndexChanged(ExecProtocol, EventArgs.Empty);
                LoadNodeProperty(@"CommandProtocol", CommandProtocol, @"${GlobalConfigRequestProtocol}");
                CommandProtocol_SelectedIndexChanged(CommandProtocol, EventArgs.Empty);
                LoadNodeProperty(@"TransferProtocol", TransferProtocol, @"${GlobalConfigTransferProtocol}");
                LoadNodeProperty(@"TelnetPort", TelnetPort, @"${GlobalTelnetPort}");
                LoadNodeProperty(@"SSHPort", SSHPort, @"${GlobalSSHPort}");

                HiddenEnableLevel.Value = EnableLevel.SelectedValue;

                if (UseDeviceLoginCredentials)
                {
                    UseUserDeviceCredentials.Enabled = false;
                    UseUserDeviceCredentials.SelectedValue = @"False";
                    UseUserDeviceCredentials.CssClass = @"disabled";
                }

                GlobalConnectionProfile_SelectedIndexChanged(GlobalConnectionProfile, EventArgs.Empty);

                //communication
                LoadNodeProperty(@"AllowIntermediary", AllowIntermediary, @"False");
                LoadNodeProperty(@"UseKeybInteractiveAuth", UseKeyboardInteractiveAuthentication, @"False");
                LoadNodeProperty(@"EncryptionAlgorithm", EncryptionAlgorithm, @"0");

                if (IsMultiselected)
                {
                    NodeDetails.Visible = true;
                    NodeDetails.Checked = false;
                    ConnectionProfile.Visible = true;
                    ConnectionProfile.Checked = false;
                    Communication.Visible = true;
                    Communication.Checked = false;

                    NodeDetails_CheckedChanged(NodeDetails, EventArgs.Empty);
                    ConnectionProfile_CheckedChanged(ConnectionProfile, EventArgs.Empty);
                    Communication_CheckedChanged(Communication, EventArgs.Empty);
                }
                else
                {
                    NodeDetails.Visible = false;
                    ConnectionProfile.Visible = false;
                    Communication.Visible = false;
                }

                deviceValidationHolder.Visible = !IsMultiselected;
                buttonsHolder.Visible = !IsMultiselected;

                SetMacroColumnVisibility(!IsMultiselected && (Convert.ToInt32(GlobalConnectionProfile.SelectedValue, CultureInfo.InvariantCulture) < 0));
            }

            PrefillNCMNodeCredentials();
        }
    }

    protected void ManageNodesWithNCM_SelectedIndexChanged(object sender, EventArgs e)
    {
        var manageNodesWithNcmEnabled = (ManageNodesWithNCM.SelectedValue.Equals(@"KeepUnchanged") || ManageNodesWithNCM.SelectedValue.Equals(@"Add"));
        ManageNodesWithNcmEnabled = manageNodesWithNcmEnabled;
    }

    protected void ResetToGlobalMacro_Click(object sender, EventArgs e)
    {
        var btnResetToGlobalMacro = (LinkButton)sender;
        var id = btnResetToGlobalMacro.ID.Replace(@"Global", string.Empty);

        var ctrlPropertyValue = btnResetToGlobalMacro.Parent.FindControl(id);
        if (ctrlPropertyValue is PasswordTextBox)
        {
            var pwdPropertyValue = (PasswordTextBox)ctrlPropertyValue;
            if (id.Equals(@"Username", StringComparison.InvariantCultureIgnoreCase))
            {
                pwdPropertyValue.TextMode = HideUsernames ? TextBoxMode.Password : TextBoxMode.SingleLine;
                pwdPropertyValue.PasswordText = $@"${{Global{id}}}";
            }
            else if (id.Equals(@"Password", StringComparison.InvariantCultureIgnoreCase))
            {
                pwdPropertyValue.TextMode = TextBoxMode.Password;
                pwdPropertyValue.PasswordText = $@"${{Global{id}}}";
            }
            else if (id.Equals(@"EnablePassword", StringComparison.InvariantCultureIgnoreCase))
            {
                pwdPropertyValue.TextMode = TextBoxMode.Password;
                pwdPropertyValue.PasswordText = $@"${{Global{id}}}";
            }
        }
        else if (ctrlPropertyValue is TextBox)
        {
            var txtPropertyValue = (TextBox)ctrlPropertyValue;
            txtPropertyValue.Text = $@"${{Global{id}}}";
        }
    }

    protected void UseUserDeviceCredentials_SelectedIndexChanged(object sender, EventArgs e)
    {
        var useUserDeviceLoginCreds = Convert.ToBoolean(UseUserDeviceCredentials.SelectedValue, CultureInfo.InvariantCulture);

        Username.TextMode = !useUserDeviceLoginCreds && HideUsernames ? TextBoxMode.Password : TextBoxMode.SingleLine;
        EnableOrDisableNodeProperty(Username, HiddenUsername, GlobalUsername, !useUserDeviceLoginCreds);

        Password.TextMode = !useUserDeviceLoginCreds ? TextBoxMode.Password : TextBoxMode.SingleLine;
        EnableOrDisableNodeProperty(Password, HiddenPassword, GlobalPassword, !useUserDeviceLoginCreds);

        EnableOrDisableNodeProperty(EnableLevel, HiddenEnableLevel.Value, null, !useUserDeviceLoginCreds);

        EnablePassword.TextMode = !useUserDeviceLoginCreds ? TextBoxMode.Password : TextBoxMode.SingleLine;
        EnableOrDisableNodeProperty(EnablePassword, HiddenEnablePassword, GlobalEnablePassword, !useUserDeviceLoginCreds);

        SetMacroValueVisibility(UsernameValue, !IsMultiselected && !useUserDeviceLoginCreds);
        SetMacroValueVisibility(EnableLevelValue, !IsMultiselected && !useUserDeviceLoginCreds);
    }

    protected void ExecProtocol_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHideEncryptionAlgorithm(HttpContext.Current.Profile.UserName);
    }

    [Localizable(false)]
    protected void CommandProtocol_SelectedIndexChanged(object sender, EventArgs e)
    {
        CommonHelper.InitializeTransferConfigProtocol(TransferProtocol, CommandProtocol.SelectedItem, shouldAddGlobalConfigTransferProtocol: true);

        ShowHideEncryptionAlgorithm(HttpContext.Current.Profile.UserName);
    }

    protected void NodeDetails_CheckedChanged(object sender, EventArgs e)
    {
        NodeGroup.Enabled = NodeDetails.Checked;
        NodeGroup.CssClass = NodeDetails.Checked ? string.Empty : @"disabled";
        Comments.Enabled = NodeDetails.Checked;
        Comments.CssClass = NodeDetails.Checked ? string.Empty : @"disabled";
    }

    protected void ConnectionProfile_CheckedChanged(object sender, EventArgs e)
    {
        var profileId = Convert.ToInt32(GlobalConnectionProfile.SelectedValue);
        var noProfile = (profileId == -1);
        var newProfile = (profileId == -2);
        var bChecked = ConnectionProfile.Checked;

        GlobalConnectionProfile.Enabled = bChecked;
        GlobalConnectionProfile.CssClass = bChecked ? string.Empty : @"disabled";
        ProfileName.Enabled = bChecked;
        ProfileName.CssClass = bChecked ? string.Empty : @"disabled";

        UseUserDeviceCredentials.Enabled = UseDeviceLoginCredentials ? false : bChecked && noProfile;
        UseUserDeviceCredentials.CssClass = UseDeviceLoginCredentials ? @"disabled" : bChecked && noProfile ? string.Empty : @"disabled";

        Username.Enabled = !Convert.ToBoolean(UseUserDeviceCredentials.SelectedValue) && bChecked && (noProfile || newProfile);
        Username.CssClass = !Convert.ToBoolean(UseUserDeviceCredentials.SelectedValue) && bChecked && (noProfile || newProfile) ? string.Empty : @"disabled";
        Password.Enabled = !Convert.ToBoolean(UseUserDeviceCredentials.SelectedValue) && bChecked && (noProfile || newProfile);
        Password.CssClass = !Convert.ToBoolean(UseUserDeviceCredentials.SelectedValue) && bChecked && (noProfile || newProfile) ? string.Empty : @"disabled";
        EnableLevel.Enabled = !Convert.ToBoolean(UseUserDeviceCredentials.SelectedValue) && bChecked && (noProfile || newProfile);
        EnableLevel.CssClass = !Convert.ToBoolean(UseUserDeviceCredentials.SelectedValue) && bChecked && (noProfile || newProfile) ? string.Empty : @"disabled";
        EnablePassword.Enabled = !Convert.ToBoolean(UseUserDeviceCredentials.SelectedValue) && bChecked && (noProfile || newProfile);
        EnablePassword.CssClass = !Convert.ToBoolean(UseUserDeviceCredentials.SelectedValue) && bChecked && (noProfile || newProfile) ? string.Empty : @"disabled";

        GlobalUsername.Visible = !Convert.ToBoolean(UseUserDeviceCredentials.SelectedValue) && bChecked && (noProfile || newProfile);
        GlobalPassword.Visible = !Convert.ToBoolean(UseUserDeviceCredentials.SelectedValue) && bChecked && (noProfile || newProfile);
        GlobalEnablePassword.Visible = !Convert.ToBoolean(UseUserDeviceCredentials.SelectedValue) && bChecked && (noProfile || newProfile);

        ExecProtocol.Enabled = bChecked && (noProfile || newProfile);
        ExecProtocol.CssClass = bChecked && (noProfile || newProfile) ? string.Empty : @"disabled";
        CommandProtocol.Enabled = bChecked && (noProfile || newProfile);
        CommandProtocol.CssClass = bChecked && (noProfile || newProfile) ? string.Empty : @"disabled";
        TransferProtocol.Enabled = bChecked && (noProfile || newProfile);
        TransferProtocol.CssClass = bChecked && (noProfile || newProfile) ? string.Empty : @"disabled";
        TelnetPort.Enabled = bChecked && (noProfile || newProfile);
        TelnetPort.CssClass = bChecked && (noProfile || newProfile) ? string.Empty : @"disabled";
        SSHPort.Enabled = bChecked && (noProfile || newProfile);
        SSHPort.CssClass = bChecked && (noProfile || newProfile) ? string.Empty : @"disabled";

        GlobalTelnetPort.Visible = bChecked && (noProfile || newProfile);
        GlobalSSHPort.Visible = bChecked && (noProfile || newProfile);
    }

    protected void GlobalConnectionProfile_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetMacroColumnVisibility(!IsMultiselected && (Convert.ToInt32(GlobalConnectionProfile.SelectedValue, CultureInfo.InvariantCulture) < 0));

        var profileId = Convert.ToInt32(GlobalConnectionProfile.SelectedValue, CultureInfo.InvariantCulture);
        NewProfileHolder.Visible = (profileId == -2);

        switch (profileId)
        {
            case -2: //create new connection profile
                DisableControl(@"UseUserDeviceCredentials", UseUserDeviceCredentials, @"False");
                EnableControl(@"Username", Username, @"${GlobalUsername}", GlobalUsername);
                EnableControl(@"Password", Password, @"${GlobalPassword}", GlobalPassword);
                EnableControl(@"EnableLevel", EnableLevel, @"${GlobalEnableLevel}");
                EnableControl(@"EnablePassword", EnablePassword, @"${GlobalEnablePassword}", GlobalEnablePassword);
                EnableControl(@"ExecProtocol", ExecProtocol, @"${GlobalExecProtocol}");
                ExecProtocol_SelectedIndexChanged(ExecProtocol, EventArgs.Empty);
                EnableControl(@"CommandProtocol", CommandProtocol, @"${GlobalConfigRequestProtocol}");
                CommandProtocol_SelectedIndexChanged(CommandProtocol, EventArgs.Empty);
                EnableControl(@"TransferProtocol", TransferProtocol, @"${GlobalConfigTransferProtocol}");
                EnableControl(@"TelnetPort", TelnetPort, @"${GlobalTelnetPort}", GlobalTelnetPort);
                EnableControl(@"SSHPort", SSHPort, @"${GlobalSSHPort}", GlobalSSHPort);
                break;
            case -1://no profile set
                if (!UseDeviceLoginCredentials) EnableControl(@"UseUserDeviceCredentials", UseUserDeviceCredentials, @"False");
                if (Convert.ToBoolean(UseUserDeviceCredentials.SelectedValue))
                {
                    UseUserDeviceCredentials_SelectedIndexChanged(UseUserDeviceCredentials, EventArgs.Empty);
                }
                else
                {
                    EnableControl(@"Username", Username, @"${GlobalUsername}", GlobalUsername);
                    EnableControl(@"Password", Password, @"${GlobalPassword}", GlobalPassword);
                    EnableControl(@"EnableLevel", EnableLevel, @"${GlobalEnableLevel}");
                    EnableControl(@"EnablePassword", EnablePassword, @"${GlobalEnablePassword}", GlobalEnablePassword);
                }
                EnableControl(@"ExecProtocol", ExecProtocol, @"${GlobalExecProtocol}");
                ExecProtocol_SelectedIndexChanged(ExecProtocol, EventArgs.Empty);
                EnableControl(@"CommandProtocol", CommandProtocol, @"${GlobalConfigRequestProtocol}");
                CommandProtocol_SelectedIndexChanged(CommandProtocol, EventArgs.Empty);
                EnableControl(@"TransferProtocol", TransferProtocol, @"${GlobalConfigTransferProtocol}");
                EnableControl(@"TelnetPort", TelnetPort, @"${GlobalTelnetPort}", GlobalTelnetPort);
                EnableControl(@"SSHPort", SSHPort, @"${GlobalSSHPort}", GlobalSSHPort);
                break;
            case 0: //auto detect
                DisableControl(@"UseUserDeviceCredentials", UseUserDeviceCredentials, @"False");
                DisableControl(@"Username", Username, Resources.NCMWebContent.WEBDATA_VK_865, GlobalUsername);
                DisableControl(@"Password", Password, Resources.NCMWebContent.WEBDATA_VK_865, GlobalPassword);
                DisableControl(@"EnableLevel", EnableLevel, Resources.NCMWebContent.WEBDATA_VK_865);
                DisableControl(@"EnablePassword", EnablePassword, Resources.NCMWebContent.WEBDATA_VK_865, GlobalEnablePassword);
                DisableControl(@"ExecProtocol", ExecProtocol, Resources.NCMWebContent.WEBDATA_VK_865);
                ExecProtocol_SelectedIndexChanged(ExecProtocol, EventArgs.Empty);
                DisableControl(@"CommandProtocol", CommandProtocol, Resources.NCMWebContent.WEBDATA_VK_865);
                CommandProtocol_SelectedIndexChanged(CommandProtocol, EventArgs.Empty);
                DisableControl(@"TransferProtocol", TransferProtocol, Resources.NCMWebContent.WEBDATA_VK_865);
                DisableControl(@"TelnetPort", TelnetPort, Resources.NCMWebContent.WEBDATA_VK_865, GlobalTelnetPort);
                DisableControl(@"SSHPort", SSHPort, Resources.NCMWebContent.WEBDATA_VK_865, GlobalSSHPort);
                break;
            default: //use existing connection profile
                using (var proxy = _isLayer.GetProxy())
                {
                    var profile = proxy.Cirrus.Nodes.GetConnectionProfile(profileId);

                    DisableControl(@"UseUserDeviceCredentials", UseUserDeviceCredentials, @"False");
                    DisableControl(@"Username", Username, profile.UserName, GlobalUsername);
                    DisableControl(@"Password", Password, profile.Password, GlobalPassword);
                    DisableControl(@"EnableLevel", EnableLevel, profile.EnableLevel);
                    DisableControl(@"EnablePassword", EnablePassword, profile.EnablePassword, GlobalEnablePassword);
                    DisableControl(@"ExecProtocol", ExecProtocol, profile.ExecuteScriptProtocol);
                    ExecProtocol_SelectedIndexChanged(ExecProtocol, EventArgs.Empty);
                    DisableControl(@"CommandProtocol", CommandProtocol, profile.RequestConfigProtocol);
                    CommandProtocol_SelectedIndexChanged(CommandProtocol, EventArgs.Empty);
                    DisableControl(@"TransferProtocol", TransferProtocol, profile.TransferConfigProtocol);
                    DisableControl(@"TelnetPort", TelnetPort, profile.TelnetPort.ToString(), GlobalTelnetPort);
                    DisableControl(@"SSHPort", SSHPort, profile.SSHPort.ToString(), GlobalSSHPort);
                }
                break;
        }
    }

    protected void Communication_CheckedChanged(object sender, EventArgs e)
    {
        AllowIntermediary.Enabled = Communication.Checked;
        AllowIntermediary.CssClass = Communication.Checked ? string.Empty : @"disabled";
        UseKeyboardInteractiveAuthentication.Enabled = Communication.Checked;
        UseKeyboardInteractiveAuthentication.CssClass = Communication.Checked ? string.Empty : @"disabled";
        EncryptionAlgorithm.Enabled = Communication.Checked;
        EncryptionAlgorithm.CssClass = Communication.Checked ? string.Empty : @"disabled";
       
    }

    #endregion

    #region Private Methods

    private bool AdvancedCiscoAsaMonitoringEnabled()
    {
        var advancedCiscoAsaMonitoringEnabled = true;
        var cliAdvancedCiscoAsaMonitoringPlugin = NodePropertyPluginManager.Plugins.FirstOrDefault(p => p.Name == @"CliAdvancedCiscoAsaMonitoring");
        object value;
        if (cliAdvancedCiscoAsaMonitoringPlugin != null && cliAdvancedCiscoAsaMonitoringPlugin.State.TryGetValue(@"Cli_AdvancedCiscoAsaMonitoringEnabled", out value))
        {
            advancedCiscoAsaMonitoringEnabled = bool.Parse(value.ToString());
        }
        return advancedCiscoAsaMonitoringEnabled;
    }

    private void PrefillNCMNodeCredentials()
    {
        if (IsMultiselected) return; //skip prefill NCM credentials in multi-edit mode

        if (IsNcmNodeCredentialsPrefilled) return; //NCM credentails already prefilled

        var advancedCiscoAsaMonitoringEnabled = AdvancedCiscoAsaMonitoringEnabled();
        if (!advancedCiscoAsaMonitoringEnabled &&
            ManageNodesWithNCM.SelectedValue.Equals(@"Add", StringComparison.InvariantCultureIgnoreCase))
        {
            //skip prefill NCM credentials when add node to NCM already selected, but Cisco ASA Monitoring is still disabled
            IsNcmNodeCredentialsPrefilled = true;
            return;
        }
        else if (!advancedCiscoAsaMonitoringEnabled) return; //skip prefill NCM credentials when Advanced Cisco ASA Monitoring is disabled

        if ((PluginState.Equals(@"Remove", StringComparison.InvariantCultureIgnoreCase) || PluginState.Equals(@"Never", StringComparison.InvariantCultureIgnoreCase)) &&
            ManageNodesWithNCM.SelectedValue.Equals(@"Add", StringComparison.InvariantCultureIgnoreCase))
        {
            //need prefill NCM credentials from CliAdvancedCiscoAsaMonitoring plugin
            IsNcmNodeCredentialsPrefilled = true;

            var cliAdvancedCiscoAsaMonitoringPlugin = NodePropertyPluginManager.Plugins.FirstOrDefault(p => p.Name == @"CliAdvancedCiscoAsaMonitoring");
            if (cliAdvancedCiscoAsaMonitoringPlugin != null)
            {
                object value;
                if (cliAdvancedCiscoAsaMonitoringPlugin.State.TryGetValue(@"Cli_Username", out value))
                    PrefillNodeProperty(Username, @"Username", value != null ? value.ToString() : @"${GlobalUsername}");

                if (cliAdvancedCiscoAsaMonitoringPlugin.State.TryGetValue(@"Cli_Password", out value))
                    PrefillNodeProperty(Password, @"Password", value != null ? value.ToString() : @"${GlobalPassword}");

                if (cliAdvancedCiscoAsaMonitoringPlugin.State.TryGetValue(@"Cli_EnablePassword", out value))
                    PrefillNodeProperty(EnablePassword, @"EnablePassword", value != null ? value.ToString() : @"${GlobalEnablePassword}");

                if (cliAdvancedCiscoAsaMonitoringPlugin.State.TryGetValue(@"Cli_SSHPort", out value))
                    PrefillNodeProperty(SSHPort, @"SSHPort", value != null ? value.ToString() : @"${GlobalSSHPort}");
            }
        }
    }

    private void ShowCliDeviceTemplatePlugin(bool manageNodesWithNcmEnabled)
    {
        var cliDeviceTemplatePlugin = NodePropertyPluginManager.Plugins.FirstOrDefault(p => p.Name == @"CliDeviceTemplate");
        if (cliDeviceTemplatePlugin == null) return;

        try
        {
            var advancedCiscoAsaMonitoringEnabled = AdvancedCiscoAsaMonitoringEnabled();
            cliDeviceTemplatePlugin.Visible = manageNodesWithNcmEnabled || advancedCiscoAsaMonitoringEnabled;
        }
        catch (Exception ex)
        {
            cliDeviceTemplatePlugin.Visible = true;
            _log.ErrorFormat("NCMNodeProperties: {0}", ex);
        }
    }

    private void DisableControl(string propertyName, Control ctrl, string propertyValue, Control lnk = null)
    {
        if (ctrl is PasswordTextBox)
        {
            var pwdBox = (PasswordTextBox)ctrl;
            if (propertyName.Equals(@"Username", StringComparison.InvariantCultureIgnoreCase) && HideUsernames)
            {
                if (propertyValue.Equals(Resources.NCMWebContent.WEBDATA_VK_865))
                {
                    pwdBox.TextMode = TextBoxMode.SingleLine;
                }
                else
                {
                    pwdBox.TextMode = TextBoxMode.Password;
                }
            }
            if (propertyName.Equals(@"Password", StringComparison.InvariantCultureIgnoreCase))
            {
                if (propertyValue.Equals(Resources.NCMWebContent.WEBDATA_VK_865))
                {
                    pwdBox.TextMode = TextBoxMode.SingleLine;
                }
                else
                {
                    pwdBox.TextMode = TextBoxMode.Password;
                }
            }
            if (propertyName.Equals(@"EnablePassword", StringComparison.InvariantCultureIgnoreCase))
            {
                if (propertyValue.Equals(Resources.NCMWebContent.WEBDATA_VK_865))
                {
                    pwdBox.TextMode = TextBoxMode.SingleLine;
                }
                else
                {
                    pwdBox.TextMode = TextBoxMode.Password;
                }
            }
            pwdBox.PasswordText = propertyValue;
            pwdBox.Enabled = false;
            pwdBox.CssClass = @"disabled";
        }
        else if (ctrl is TextBox)
        {
            var txtBox = (TextBox)ctrl;
            txtBox.Text = propertyValue;
            txtBox.Enabled = false;
            txtBox.CssClass = @"disabled";
        }
        else if (ctrl is DropDownList)
        {
            var ddList = (DropDownList)ctrl;
            if (propertyValue.Equals(Resources.NCMWebContent.WEBDATA_VK_865))
            {
                var autoDetect = ddList.Items.FindByValue(@"Auto Detect");
                if (autoDetect == null)
                {
                    autoDetect = new ListItem(Resources.NCMWebContent.WEBDATA_VK_865, @"Auto Detect");
                    ddList.Items.Add(autoDetect);
                    ddList.SelectedValue = autoDetect.Value;
                }
                else
                {
                    ddList.SelectedValue = autoDetect.Value;
                }
            }
            else
            {
                ddList.SelectedValue = propertyValue;
            }
            ddList.Enabled = false;
        }

        if (lnk != null)
        {
            if (lnk is LinkButton)
            {
                var lnkBtn = (LinkButton)lnk;
                lnkBtn.Visible = false;
            }
        }
    }

    private void EnableControl(string propertyName, Control ctrl, string propertyValue, Control lnk = null)
    {
        if (ctrl is PasswordTextBox)
        {
            var pwdBox = (PasswordTextBox)ctrl;
            LoadNodeProperty(propertyName, ctrl, propertyValue);
            pwdBox.Enabled = true;
            pwdBox.CssClass = string.Empty;
        }
        else if (ctrl is TextBox)
        {
            var txtBox = (TextBox)ctrl;
            LoadNodeProperty(propertyName, ctrl, propertyValue);
            txtBox.Enabled = true;
            txtBox.CssClass = string.Empty;
        }
        else if (ctrl is DropDownList)
        {
            var ddList = (DropDownList)ctrl;
            var autoDetect = ddList.Items.FindByValue(@"Auto Detect");
            if (autoDetect != null) ddList.Items.Remove(autoDetect);
            var autoDetermine = ddList.Items.FindByValue(@"Determined by User Account");
            if (autoDetermine != null) ddList.Items.Remove(autoDetermine);
            LoadNodeProperty(propertyName, ctrl, propertyValue);
            ddList.Enabled = true;
        }

        if (lnk != null)
        {
            if (lnk is LinkButton)
            {
                var lnkBtn = (LinkButton)lnk;
                lnkBtn.Visible = true;
            }
        }
    }

    private void PrefillNodeProperty(Control ctrlPropertyValue, string propertyName, string propertyValue)
    {
        if (ctrlPropertyValue is PasswordTextBox)
        {
            var pwdPropertyValue = (PasswordTextBox)ctrlPropertyValue;
            if (propertyName.Equals(@"Username", StringComparison.InvariantCultureIgnoreCase))
            {
                pwdPropertyValue.TextMode = HideUsernames ? TextBoxMode.Password : TextBoxMode.SingleLine;
                HiddenUsername = propertyValue;
            }
            else if (propertyName.Equals(@"Password", StringComparison.InvariantCultureIgnoreCase))
            {
                pwdPropertyValue.TextMode = TextBoxMode.Password;
                HiddenPassword = propertyValue;
            }
            else if (propertyName.Equals(@"EnablePassword", StringComparison.InvariantCultureIgnoreCase))
            {
                pwdPropertyValue.TextMode = TextBoxMode.Password;
                HiddenEnablePassword = propertyValue;
            }
            pwdPropertyValue.Text = propertyValue;
            pwdPropertyValue.PasswordText = propertyValue;
        }
        else if (ctrlPropertyValue is TextBox)
        {
            var txtPropertyValue = (TextBox)ctrlPropertyValue;
            txtPropertyValue.Text = propertyValue;
        }
    }

    private void LoadNodeProperty(string PropertyName, Control ctrlPropertyValue, string DefaultPropertyValue)
    {
        var currentPropertyValue = string.Empty;

        var oldPropertyValue = string.Empty;
        var newPropertyValue = string.Empty;

        var bEnabled = true;
        var CssClass = string.Empty;

        IList<int> Ids = new List<int>();
        if (NodeWorkflowHelper.GetMultiSelectedNodeIds(pageGuid).Count > 0)
        {
            Ids = NodeWorkflowHelper.GetMultiSelectedNodeIds(pageGuid);
        }
        else if (NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(pageGuid).Count > 0)
        {
            foreach (var id in NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(pageGuid))
                Ids.Add(NodeWorkflowHelper.GetObjectId(id));
        }

        if (Ids.Count > 0)
        {
            oldPropertyValue = GetNodeProperty(Ids[0], PropertyName, DefaultPropertyValue);
            currentPropertyValue = oldPropertyValue;
        }
        else
        {
            currentPropertyValue = DefaultPropertyValue;
        }

        for (var i = 1; i < Ids.Count; i++)
        {
            newPropertyValue = GetNodeProperty(Ids[i], PropertyName, DefaultPropertyValue);
            if (!oldPropertyValue.Equals(newPropertyValue, StringComparison.InvariantCulture))
            {
                currentPropertyValue = string.Empty;
                bEnabled = false;
                CssClass = @"disabled";
                break;
            }
            newPropertyValue = oldPropertyValue;
        }

        if (ctrlPropertyValue is PasswordTextBox)
        {
            var pwdPropertyValue = (PasswordTextBox)ctrlPropertyValue;
            if (PropertyName.Equals(@"Username", StringComparison.InvariantCultureIgnoreCase))
            {
                pwdPropertyValue.TextMode = HideUsernames ? TextBoxMode.Password : TextBoxMode.SingleLine;
                HiddenUsername = currentPropertyValue;
            }
            if (PropertyName.Equals(@"Password", StringComparison.InvariantCultureIgnoreCase))
            {
                pwdPropertyValue.TextMode = TextBoxMode.Password;
                HiddenPassword = currentPropertyValue;
            }
            if (PropertyName.Equals(@"EnablePassword", StringComparison.InvariantCultureIgnoreCase))
            {
                pwdPropertyValue.TextMode = TextBoxMode.Password;
                HiddenEnablePassword = currentPropertyValue;
            }
            pwdPropertyValue.PasswordText = currentPropertyValue;
            pwdPropertyValue.Enabled = bEnabled;
            pwdPropertyValue.CssClass = CssClass;
        }
        else if (ctrlPropertyValue is TextBox)
        {
            var txtPropertyValue = (TextBox)ctrlPropertyValue;
            txtPropertyValue.Text = currentPropertyValue;
            txtPropertyValue.Enabled = bEnabled;
            txtPropertyValue.CssClass = CssClass;
        }
        else if (ctrlPropertyValue is DropDownList)
        {
            var ddlPropertyValue = (DropDownList)ctrlPropertyValue;
            if (ddlPropertyValue.Items.FindByValue(currentPropertyValue) != null)
                ddlPropertyValue.SelectedValue = currentPropertyValue;
            else
                ddlPropertyValue.SelectedIndex = 0;
            ddlPropertyValue.Enabled = bEnabled;
        }
    }

    private string SetNodeProperty(string PropertyName, object DefaultValue, Control ctrlPropertyValue, CheckBox cbPropertyValue)
    {
        if (_mode == NodePropertyPluginExecutionMode.WizChangeProperties && !IsMultiselected)
        {
            if (ctrlPropertyValue is PasswordTextBox)
            {
                var pwdPropertyValue = (PasswordTextBox)ctrlPropertyValue;
                return pwdPropertyValue.PasswordText;
            }
            else if (ctrlPropertyValue is TextBox)
            {
                var txtPropertyValue = (TextBox)ctrlPropertyValue;
                return txtPropertyValue.Text;
            }
            else if (ctrlPropertyValue is DropDownList)
            {
                var ddlPropertyValue = (DropDownList)ctrlPropertyValue;
                return ddlPropertyValue.SelectedValue;
            }
        }
        else if (_mode == NodePropertyPluginExecutionMode.EditProperies && !IsMultiselected)
        {
            if (ctrlPropertyValue is PasswordTextBox)
            {
                var pwdPropertyValue = (PasswordTextBox)ctrlPropertyValue;
                return pwdPropertyValue.PasswordText;
            }
            else if (ctrlPropertyValue is TextBox)
            {
                var txtPropertyValue = (TextBox)ctrlPropertyValue;
                return txtPropertyValue.Text;
            }
            else if (ctrlPropertyValue is DropDownList)
            {
                var ddlPropertyValue = (DropDownList)ctrlPropertyValue;
                return ddlPropertyValue.SelectedValue;
            }
        }
        else
        {
            if (cbPropertyValue.Checked)
            {
                if (ctrlPropertyValue is PasswordTextBox)
                {
                    var pwdPropertyValue = (PasswordTextBox)ctrlPropertyValue;
                    return pwdPropertyValue.PasswordText;
                }
                else if (ctrlPropertyValue is TextBox)
                {
                    var txtPropertyValue = (TextBox)ctrlPropertyValue;
                    return txtPropertyValue.Text;
                }
                else if (ctrlPropertyValue is DropDownList)
                {
                    var ddlPropertyValue = (DropDownList)ctrlPropertyValue;
                    return ddlPropertyValue.SelectedValue;
                }
            }
            else
            {
                return Convert.ToString(DefaultValue);
            }
        }

        return string.Empty;
    }

    private void LoadGlobalConnectionProfiles()
    {
        GlobalConnectionProfile.Items.Clear();
        GlobalConnectionProfile.Items.Add(new ListItem(Resources.NCMWebContent.WEBDATA_VK_862, @"-1"));
        GlobalConnectionProfile.Items.Add(new ListItem(Resources.NCMWebContent.WEBDATA_VK_863, @"-2")); //'-2' only used to check if new profile selected (not save in NCM databse)

        using (var proxy = _isLayer.GetProxy())
        {
            var profiles = proxy.Cirrus.Nodes.GetAllConnectionProfiles();

            var autoDetectProfiles = profiles.Where(item => item.UseForAutoDetect);
            if (autoDetectProfiles.Any())
            {
                GlobalConnectionProfile.Items.Add(new ListItem(Resources.NCMWebContent.WEBDATA_VK_864, @"0"));
            }

            foreach (var profile in profiles)
            {
                GlobalConnectionProfile.Items.Add(new ListItem(profile.Name, profile.ID.ToString()));
            }
        }
    }

    private void EnableOrDisableNodeProperty(Control ctrlPropertyValue, string value, LinkButton lbResetToGlobalMacro, bool bEnabled)
    {
        if (ctrlPropertyValue is PasswordTextBox)
        {
            var pwdPropertyValue = (PasswordTextBox)ctrlPropertyValue;
            pwdPropertyValue.PasswordText = bEnabled ? value : Resources.NCMWebContent.WEBDATA_VK_330;
            pwdPropertyValue.Enabled = bEnabled;
            pwdPropertyValue.CssClass = bEnabled ? string.Empty : @"disabled";
        }
        else if (ctrlPropertyValue is TextBox)
        {
            var txtPropertyValue = (TextBox)ctrlPropertyValue;
            txtPropertyValue.Text = bEnabled ? value : Resources.NCMWebContent.WEBDATA_VK_330;
            txtPropertyValue.Enabled = bEnabled;
            txtPropertyValue.CssClass = bEnabled ? string.Empty : @"disabled";
        }
        else if (ctrlPropertyValue is DropDownList)
        {
            var ddlPropertyValue = (DropDownList)ctrlPropertyValue;
            var item = new ListItem(Resources.NCMWebContent.WEBDATA_VK_330, @"Determined by User Account");
            if (bEnabled)
            {
                if (ddlPropertyValue.Items.FindByValue(@"Determined by User Account") != null)
                {
                    ddlPropertyValue.Items.Remove(item);
                }

                var autoDetect = ddlPropertyValue.Items.FindByValue(@"Auto Detect");
                if (autoDetect != null)
                {
                    ddlPropertyValue.Items.Remove(autoDetect);
                }
            }
            else
            {
                if (ddlPropertyValue.Items.FindByValue(@"Determined by User Account") == null)
                {
                    ddlPropertyValue.Items.Add(item);
                }
            }
            ddlPropertyValue.SelectedValue = bEnabled ? value : @"Determined by User Account";
            ddlPropertyValue.Enabled = bEnabled;
            ddlPropertyValue.CssClass = bEnabled ? string.Empty : @"disabled";
        }

        if (lbResetToGlobalMacro != null) lbResetToGlobalMacro.Visible = bEnabled;
    }

    /// <summary>
    /// Gets Ncm Node property value by defined propertyName.
    /// </summary>
    /// <param name="coreNodeId">The NodeID of Core Node object.</param>
    /// <param name="propertyName">The property name.</param>
    /// <param name="coreNodeId">The default value.</param>
    /// <returns></returns>
    private string GetNodeProperty(int coreNodeId, string propertyName, string defaultValue)
    {
        var dt = ViewState["NCM_NodeProperties"] as DataTable;

        if (dt != null)
        {
            return NcmNodeHelper.GetNodeProperty(dt, coreNodeId, propertyName, defaultValue);
        }
        return defaultValue;
    }

    /// <summary>
    /// Loads Ncm Node Properties table.
    /// </summary>
    /// <param name="nodes">The list of Core Nodes.</param>
    private void LoadNcmNodePropertiesTable(IList<Node> nodes)
    {
        var dt = ViewState["NCM_NodeProperties"] as DataTable;

        if (dt == null)
        {
            ViewState["NCM_NodeProperties"] = NcmNodeHelper.LoadNcmNodePropertiesTable(nodes);
        }
    }

    private void ShowHideEncryptionAlgorithm(string userName)
    {
        var nodeId = GetNcmNodeId(userName);
        var execProtocol = ParseProtocolValue(ExecProtocol.SelectedValue, nodeId);
        var commandProtocol = ParseProtocolValue(CommandProtocol.SelectedValue, nodeId);

        EncryptionAlgorithmHolder.Visible =
            !(execProtocol.Equals(@"TELNET", StringComparison.InvariantCultureIgnoreCase) &&
              (commandProtocol.Equals(@"TELNET", StringComparison.InvariantCultureIgnoreCase) ||
               commandProtocol.Equals(@"SNMP", StringComparison.InvariantCultureIgnoreCase)));
    }

    #endregion

    #region Core Nodes Helper Methods

    private List<Node> GetNodes(string guid)
    {
        var nodes = new List<Node>();

        using (var proxy = coreBusinessLayerProxyCreator.Create(BusinessLayerExceptionHandler))
        {
            if (NodeWorkflowHelper.GetMultiSelectedNodeIds(guid).Count > 0)
            {
                foreach (var nodeId in NodeWorkflowHelper.GetMultiSelectedNodeIds(guid))
                {
                    var engineId = GetEngineIdForNodeId(proxy, nodeId);
                    var node = GetNodeById(nodeId, engineId);
                    nodes.Add(node);
                }
            }
            else if (NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(guid).Count > 0)
            {
                foreach (var coreNodeIdWithEngineId in NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(guid))
                {
                    var nodeId = Int32.Parse(coreNodeIdWithEngineId.Split(':')[1]);
                    var engineId = Int32.Parse(coreNodeIdWithEngineId.Split(':')[2]);
                    var node = GetNodeById(nodeId, engineId);
                    nodes.Add(node);
                }
            }
        }

        return nodes;
    }

    private int GetEngineIdForNodeId(ICoreBusinessLayer proxy, int nodeId)
    {
        var engineId = proxy.GetEngineIdForNetObject($@"N:{nodeId}");
        return engineId;
    }

    private Node GetNodeById(int nodeId, int engineId)
    {
        using (var coreProxy = coreBusinessLayerProxyCreator.Create(BusinessLayerExceptionHandler, engineId, true))
        {
            return coreProxy.GetNodeWithOptions(nodeId, false, false);
        }
    }

    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        throw new Exception(@"BusinessLayerException:NCM Node Properties");
    }

    #endregion

    #region INodePropertyPlugin Members

    public void Initialize(IList<Node> nodes, NodePropertyPluginExecutionMode mode, Dictionary<string, object> pluginState)
    {
        _nodes = new List<Node>(nodes);
        _mode = mode;
        _pluginState = pluginState;

        if (!SecurityHelper.IsValidNcmAccountRole || IsLicenseExpired)
        {
            Visible = false;
            mainHolder.Visible = false;
            ManageNodesWithNcmEnabled = false;
            return;
        }

        if (mode == NodePropertyPluginExecutionMode.EditProperies)
        {
            var selectedCoreNodesCount = nodes.Count;
            var selectedNcmNodesCount = NcmNodeHelper.GetSelectedNcmNodesCount(nodes);
            var ignoredNcmNodesCount = NcmNodeHelper.GetIgnoredNcmNodesCount(nodes);
            if (selectedCoreNodesCount == selectedNcmNodesCount)
            {
                NodesCountToAdd = 0;

                Visible = true;
                LoadNcmNodePropertiesTable(nodes);
                manageNodesWithNCMHolder.Visible = true;
                if (!Page.IsPostBack)
                {
                    ManageNodesWithNCM.SelectedValue = @"Add";
                    PluginState = @"Add";
                    ManageNodesWithNcmEnabled = true;
                }
            }
            else if (selectedNcmNodesCount == 0 && ignoredNcmNodesCount == 0)
            {
                NodesCountToAdd = selectedCoreNodesCount;

                Visible = true;
                manageNodesWithNCMHolder.Visible = true;
                if (!Page.IsPostBack)
                {
                    ManageNodesWithNCM.SelectedValue = @"Remove";
                    PluginState = @"Remove";
                    ManageNodesWithNcmEnabled = false;
                }
            }
            else if (selectedCoreNodesCount == ignoredNcmNodesCount)
            {
                NodesCountToAdd = selectedCoreNodesCount;

                Visible = true;
                manageNodesWithNCMHolder.Visible = true;
                if (!Page.IsPostBack)
                {
                    ManageNodesWithNCM.SelectedValue = @"Never";
                    PluginState = @"Never";
                    ManageNodesWithNcmEnabled = false;
                }
            }
            else if (selectedCoreNodesCount >= (selectedNcmNodesCount + ignoredNcmNodesCount))
            {
                NodesCountToAdd = selectedCoreNodesCount - selectedNcmNodesCount;

                Visible = true;
                manageNodesWithNCMHolder.Visible = true;
                if (!Page.IsPostBack)
                {
                    ManageNodesWithNCM.Items.FindByValue(@"KeepUnchanged").Enabled = true;
                    ManageNodesWithNCM.SelectedValue = @"KeepUnchanged";
                    PluginState = @"KeepUnchanged";
                    ManageNodesWithNcmEnabled = true;
                }
            }
            else
            {
                Visible = false;
                mainHolder.Visible = false;
                ManageNodesWithNcmEnabled = false;
            }
        }
        else if (mode == NodePropertyPluginExecutionMode.WizChangeProperties)
        {
            NodesCountToAdd = _nodes.Count;

            if (!Page.IsPostBack)
            {
                manageNodesWithNCMHolder.Visible = !IsNcmModuleOnly;
                ManageNodesWithNCM.SelectedValue = !IsNcmModuleOnly ? @"Remove" : @"Add";
                PluginState = !IsNcmModuleOnly ? @"Remove" : @"Add";
                ManageNodesWithNcmEnabled = IsNcmModuleOnly;
            }
        }
    }

    public bool Update()
    {
        if (!SecurityHelper.IsValidNcmAccountRole || IsLicenseExpired)
        {
            mainHolder.Visible = false;
            return true;
        }

        if (_mode == NodePropertyPluginExecutionMode.WizChangeProperties && !IsNcmModuleOnly && ManageNodesWithNCM.SelectedValue.Equals(@"Remove", StringComparison.InvariantCultureIgnoreCase))
        {
            //user does not want add node to NCM
            propertiesHolder.Visible = false;
            return true;
        }

        if (_mode == NodePropertyPluginExecutionMode.WizChangeProperties && !IsNcmModuleOnly && ManageNodesWithNCM.SelectedValue.Equals(@"Never", StringComparison.InvariantCultureIgnoreCase))
        {
            propertiesHolder.Visible = false;
        }

        if (_mode == NodePropertyPluginExecutionMode.EditProperies && !Visible) return true;

        if (_nodes == null) return true;

        if (pageGuid != null)
        {
            //clear nodes retrieved from session without guid
            _nodes.Clear();

            //get nodes using guid
            _nodes = GetNodes(pageGuid);
        }

        var userName = HttpContext.Current.Profile.UserName;

        using (var proxy = _isLayer.GetProxy())
        {
            foreach (var node in _nodes)
            {
                var coreNode = node;
                if (coreNode.ID <= 0)
                {
                    coreNode = NodeWorkflowHelper.Node;
                    if (coreNode.ID <= 0) return true;

                    if (!IsNcmModuleOnly && ManageNodesWithNCM.SelectedValue.Equals(@"Never", StringComparison.InvariantCultureIgnoreCase))
                        proxy.Cirrus.Nodes.AddToIgnoreList(coreNode.ID);
                    else
                        AddNcmNode(proxy, coreNode, userName);
                }
                else if (coreNode.ID > 0)
                {
                    if (_mode == NodePropertyPluginExecutionMode.EditProperies && Visible)
                    {
                        if (ManageNodesWithNCM.SelectedValue.Equals(@"Add", StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (PluginState.Equals(@"Remove", StringComparison.InvariantCultureIgnoreCase) ||
                                PluginState.Equals(@"KeepUnchanged", StringComparison.InvariantCultureIgnoreCase) ||
                                PluginState.Equals(@"Never", StringComparison.InvariantCultureIgnoreCase))
                            {
                                var ncmNodeId = NcmNodeHelper.GetNcmNodeId(coreNode.ID, userName);
                                if (ncmNodeId == null)
                                {
                                    AddNcmNode(proxy, coreNode, userName);
                                }
                                else
                                {
                                    UpdateNcmNode(proxy, ncmNodeId.Value);
                                }
                            }
                            else if (PluginState.Equals(@"Add", StringComparison.InvariantCultureIgnoreCase))
                            {
                                UpdateNcmNode(proxy, coreNode, userName);
                            }
                        }
                        else if (ManageNodesWithNCM.SelectedValue.Equals(@"Remove", StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (PluginState.Equals(@"Add", StringComparison.InvariantCultureIgnoreCase) ||
                                PluginState.Equals(@"KeepUnchanged", StringComparison.InvariantCultureIgnoreCase))
                            {
                                RemoveNcmNode(proxy, coreNode, userName);
                            }
                            else if (PluginState.Equals(@"Never", StringComparison.InvariantCultureIgnoreCase))
                            {
                                proxy.Cirrus.Nodes.RemoveFromIgnoreList(coreNode.ID);
                            }
                        }
                        else if (ManageNodesWithNCM.SelectedValue.Equals(@"KeepUnchanged", StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (PluginState.Equals(@"KeepUnchanged", StringComparison.InvariantCultureIgnoreCase))
                            {
                                UpdateNcmNode(proxy, coreNode, userName);
                            }
                        }
                        else if (ManageNodesWithNCM.SelectedValue.Equals(@"Never", StringComparison.InvariantCultureIgnoreCase))
                        {
                            RemoveNcmNode(proxy, coreNode, userName);
                            proxy.Cirrus.Nodes.AddToIgnoreList(coreNode.ID);
                        }
                    }
                }
            }
        }

        return true;
    }

    public bool Validate()
    {
        return true;
    }

    #endregion

    #region Ncm Node Management

    /// <summary>
    /// Adds Ncm Node in database.
    /// <param name="proxy"></param>
    /// <param name="coreNode">The Core Node object.</param>
    private void AddNcmNode(ICirrusInfoProxy proxy, Node coreNode, string userName)
    {
        var ncmNodeId = NcmNodeHelper.GetNcmNodeId(coreNode.ID, userName);
        if (ncmNodeId != null)
        {
            return;
        }

        var ncmNode = NCMNode.InitializeNewNode();
        ncmNode.CoreNodeID = coreNode.ID;
        FillNcmNodeProperties(ncmNode);
        proxy.Cirrus.Nodes.AddNode(ncmNode);
    }

    /// <summary>
    /// Updates Ncm Node in database.
    /// </summary>
    /// <param name="proxy"></param>
    /// <param name="coreNode">The Core Node object.</param>
    private void UpdateNcmNode(ICirrusInfoProxy proxy, Node coreNode, string userName)
    {
        var ncmNodeId = NcmNodeHelper.GetNcmNodeId(coreNode.ID, userName);
        if (ncmNodeId == null)
        {
            return;
        }

        UpdateNcmNode(proxy, ncmNodeId.Value);
    }

    private void UpdateNcmNode(ICirrusInfoProxy proxy, Guid ncmNodeId)
    {
        var ncmNode = proxy.Cirrus.Nodes.GetNode(ncmNodeId);
        FillNcmNodeProperties(ncmNode);
        proxy.Cirrus.Nodes.UpdateNode(ncmNode);
    }

    /// <summary>
    /// Removes Ncm Node from database.
    /// </summary>
    /// <param name="proxy"></param>
    /// <param name="coreNode">The Core Node object.</param>
    private void RemoveNcmNode(ICirrusInfoProxy proxy, Node coreNode, string userName)
    {
        var ncmNodeId = NcmNodeHelper.GetNcmNodeId(coreNode.ID, userName);
        if (ncmNodeId == null)
        {
            return;
        }

        proxy.Cirrus.Nodes.RemoveNode(ncmNodeId.Value);
    }

    /// <summary>
    /// Fill Ncm Node object by all UI properties.
    /// </summary>
    /// <param name="ncmNode">The Ncm Node object.</param>
    private void FillNcmNodeProperties(NCMNode ncmNode)
    {
        //node details
        ncmNode.NodeGroup = SetNodeProperty(@"NodeGroup", ncmNode.NodeGroup, NodeGroup, NodeDetails);
        ncmNode.NodeComments = SetNodeProperty(@"NodeComments", ncmNode.NodeComments, Comments, NodeDetails);

        //global connection profile
        var globalConnectionProfile = SetNodeProperty(@"ConnectionProfile", ncmNode.ConnectionProfile, GlobalConnectionProfile, ConnectionProfile);
        if (!IsMultiselected && string.IsNullOrEmpty(globalConnectionProfile))
        {
            //for non multi-edit mode we need to set all device login properties
            globalConnectionProfile = GlobalConnectionProfile.SelectedValue;
        }

        if (!string.IsNullOrEmpty(globalConnectionProfile)) //ConnectionProfile checkbox for multi-edit mode should be checked to save device login properties
        {
            var profileId = Convert.ToInt32(globalConnectionProfile);
            switch (profileId)
            {
                case -2:
                    //create new profile selected
                    Guid? ncmNodeId = null;
                    if (!ncmNode.NodeID.Equals(Guid.Empty))
                    {
                        ncmNodeId = ncmNode.NodeID;
                    }

                    //for multi-edit mode need to create connection profile only one time
                    var newProfileId = ConnectionProfileId;
                    if (newProfileId == 0)
                    {
                        using (var proxy = _isLayer.GetProxy())
                        {
                            var profile = new ConnectionProfile();
                            profile.Name = ProfileName.Text;
                            profile.UserName = DecryptAndParseNcmNodeProperty(ncmNodeId, Username.PasswordText);
                            profile.Password = DecryptAndParseNcmNodeProperty(ncmNodeId, Password.PasswordText);
                            profile.EnableLevel = DecryptAndParseNcmNodeProperty(ncmNodeId, EnableLevel.SelectedValue);
                            profile.EnablePassword = DecryptAndParseNcmNodeProperty(ncmNodeId, EnablePassword.PasswordText);
                            profile.ExecuteScriptProtocol = DecryptAndParseNcmNodeProperty(ncmNodeId, ExecProtocol.SelectedValue);
                            profile.RequestConfigProtocol = DecryptAndParseNcmNodeProperty(ncmNodeId, CommandProtocol.SelectedValue);
                            profile.TransferConfigProtocol = DecryptAndParseNcmNodeProperty(ncmNodeId, TransferProtocol.SelectedValue);
                            int telnetPort;
                            if (!int.TryParse(DecryptAndParseNcmNodeProperty(ncmNodeId, TelnetPort.Text), out telnetPort))
                                telnetPort = 23;
                            profile.TelnetPort = telnetPort;
                            int sshPort;
                            if (!int.TryParse(DecryptAndParseNcmNodeProperty(ncmNodeId, SSHPort.Text), out sshPort))
                                sshPort = 22;
                            profile.SSHPort = sshPort;

                            newProfileId = proxy.Cirrus.Nodes.AddConnectionProfile(profile);
                            ConnectionProfileId = newProfileId;
                        }
                    }
                    ncmNode.ConnectionProfile = newProfileId; //update connection profile id for NCM node
                    break;
                case -1:
                    //no profile selected, fill NCM node by all UI devile login properties
                    ncmNode.ConnectionProfile = -1;
                    bool useUserDeviceCredentials;
                    if (Boolean.TryParse(SetNodeProperty(@"UseUserDeviceCredentials", ncmNode.UseUserDeviceCredentials, UseUserDeviceCredentials, ConnectionProfile), out useUserDeviceCredentials))
                    {
                        ncmNode.UseUserDeviceCredentials = useUserDeviceCredentials;
                    }
                    if (!useUserDeviceCredentials)
                    {
                        ncmNode.Username = SetNodeProperty(@"Username", ncmNode.Username, Username, ConnectionProfile);
                        ncmNode.Password = SetNodeProperty(@"Password", ncmNode.Password, Password, ConnectionProfile);
                        ncmNode.EnableLevel = SetNodeProperty(@"EnableLevel", ncmNode.EnableLevel, EnableLevel, ConnectionProfile);
                        ncmNode.EnablePassword = SetNodeProperty(@"EnablePassword", ncmNode.EnablePassword, EnablePassword, ConnectionProfile);
                    }
                    ncmNode.ExecProtocol = SetNodeProperty(@"ExecProtocol", ncmNode.ExecProtocol, ExecProtocol, ConnectionProfile);
                    ncmNode.CommandProtocol = SetNodeProperty(@"CommandProtocol", ncmNode.CommandProtocol, CommandProtocol, ConnectionProfile);
                    ncmNode.TransferProtocol = SetNodeProperty(@"TransferProtocol", ncmNode.TransferProtocol, TransferProtocol, ConnectionProfile);
                    ncmNode.TelnetPort = SetNodeProperty(@"TelnetPort", ncmNode.TelnetPort, TelnetPort, ConnectionProfile);
                    ncmNode.SSHPort = SetNodeProperty(@"SSHPort", ncmNode.SSHPort, SSHPort, ConnectionProfile);
                    break;
                case 0:
                    //auto-detect selected, only save connection profileId - 0
                    ncmNode.ConnectionProfile = 0;
                    break;
                default:
                    //use existing connection profile, only save selected connection profileId
                    ncmNode.ConnectionProfile = profileId;
                    break;
            }
        }

        //communication
        bool allowIntermediary;
        if (Boolean.TryParse(SetNodeProperty(@"AllowIntermediary", ncmNode.AllowIntermediary, AllowIntermediary, Communication), out allowIntermediary))
        {
            ncmNode.AllowIntermediary = allowIntermediary;
        }

        bool useKeyboardInteractiveAuthentication;
        if (Boolean.TryParse(SetNodeProperty(@"UseKeybInteractiveAuth", ncmNode.UseKeybInteractiveAuth, UseKeyboardInteractiveAuthentication, Communication), out useKeyboardInteractiveAuthentication))
        {
            ncmNode.UseKeybInteractiveAuth = useKeyboardInteractiveAuthentication;
        }

        int encryptionAlgprithm;
        if (int.TryParse(SetNodeProperty(@"EncryptionAlgorithm", (int)ncmNode.EncryptionAlgorithm, EncryptionAlgorithm, Communication), out encryptionAlgprithm))
        {
            ncmNode.EncryptionAlgorithm = (SSHEncryption)encryptionAlgprithm;
        }
    }

    private string GetChangedNodeIpAddress(Node coreNode, Dictionary<string, object> pluginState)
    {
        var ipAddress = coreNode.IpAddress;

        object ipObject;
        if (_pluginState.TryGetValue(__nodeIp, out ipObject))
        {
            var ipString = (ipObject == null) ? string.Empty : ipObject.ToString();
            if (!string.IsNullOrEmpty(ipString))
                ipAddress = ipString;
        }

        return ipAddress;
    }

    #endregion

    #region Ncm Macro Value Visibility

    protected void btnShowMacroValues_Click(object sender, EventArgs e)
    {
        var useUserDeviceLoginCreds = Convert.ToBoolean(UseUserDeviceCredentials.SelectedValue, CultureInfo.InvariantCulture);
        var nodeId = GetNcmNodeId(HttpContext.Current.Profile.UserName);

        if (!HideUsernames && !useUserDeviceLoginCreds) ShowMacroValue(Username, UsernameValue, nodeId);
        if (!useUserDeviceLoginCreds) ShowMacroValue(EnableLevel, EnableLevelValue, nodeId);
        ShowMacroValue(ExecProtocol, ExecProtocolValue, nodeId);
        ShowMacroValue(CommandProtocol, CommandProtocolValue, nodeId);
        ShowMacroValue(TransferProtocol, TransferProtocolValue, nodeId);
        ShowMacroValue(TelnetPort, TelnetPortValue, nodeId);
        ShowMacroValue(SSHPort, SSHPortValue, nodeId);

        Password.TextMode = useUserDeviceLoginCreds ? TextBoxMode.SingleLine : TextBoxMode.Password;
        EnablePassword.TextMode = useUserDeviceLoginCreds ? TextBoxMode.SingleLine : TextBoxMode.Password;
    }

    private void SetMacroValueVisibility(Literal literal, bool visible)
    {
        literal.Visible = visible;
    }

    private void SetMacroColumnVisibility(bool visible)
    {
        foreach (HtmlTableRow row in connectionProfileTable.Rows)
        {
            if (!string.IsNullOrEmpty(row.Attributes[@"visible-col-index"]))
            {
                var index = Convert.ToInt32(row.Attributes[@"visible-col-index"], CultureInfo.InvariantCulture);
                row.Cells[index].Visible = visible;
            }
        }
    }

    private void ShowMacroValue(Control ctrl, Literal literal, Guid? nodeId)
    {
        if (ctrl is TextBox)
        {
            var txt = (TextBox)ctrl;
            if (txt.Text.StartsWith(@"${"))
            {
                var value = DecryptAndParseNcmNodeProperty(nodeId, txt.Text);
                literal.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_965, @"<span style='font-style:italic;'>", HttpUtility.HtmlEncode(value), @"</span>");
            }
            else
            {
                literal.Text = string.Empty;
            }
        }
        else if (ctrl is DropDownList)
        {
            var ddl = (DropDownList)ctrl;
            if (ddl.SelectedValue.StartsWith(@"${"))
            {
                var value = DecryptAndParseNcmNodeProperty(nodeId, ddl.SelectedValue);
                literal.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_965, @"<span style='font-style:italic;'>", HttpUtility.HtmlEncode(value), @"</span>");
            }
            else
            {
                literal.Text = string.Empty;
            }
        }
    }

    private Guid? GetNcmNodeId(string userName)
    {
        Guid? nodeId = null;

        var coreNode = _nodes[0];
        if (coreNode.ID > 0)
        {
            nodeId = NcmNodeHelper.GetNcmNodeId(coreNode.ID, userName);
        }
        return nodeId;
    }

    private string ParseProtocolValue(string protocolValue, Guid? nodeId)
    {
        return protocolValue.StartsWith(@"${") ?
            DecryptAndParseNcmNodeProperty(nodeId, protocolValue) :
            protocolValue;
    }

    #endregion

    #region Ncm Device Login Validation

    protected void btnValidateLogin_Click(object sender, EventArgs e)
    {
        TraceOutput.Visible = false;
        DeviceValidationInfo.Visible = false;
        DeviceValidationDetailsSPAN.Visible = false;
        DeviceLoginValidation(HttpContext.Current.Profile.UserName);
    }

    protected void DeviceValidationDetailsBTN_Click(object sender, EventArgs e)
    {
        var btn = (sender as LinkButton);
        TraceOutput.Visible = !TraceOutput.Visible;
        DeviceValidationDetailsLBL.Text = TraceOutput.Visible ? Resources.NCMWebContent.WEBDATA_VK_741 : Resources.NCMWebContent.WEBDATA_VK_742;
        DeviceValidationDetailsIMG.Src = TraceOutput.Visible ? @"/Orion/NCM/Resources/images/arrowdown.gif" : @"/Orion/NCM/Resources/images/arrowright.gif";
    }

    private void ShowDeviceValidationBar(LoginValidationResults loginResults)
    {
        DeviceValidationBar.Attributes[@"style"] = @"display:block;";
        DeviceValidationInfo.Visible = true;
        DeviceValidationInfo.Style.Add(@"background-color", (loginResults.ValidationResult == eLoginResults.Success) ? @"#D6F6C6" : @"#FEEE90");
        DeviceValidationDetailsSPAN.Visible = (loginResults.DeviceOutput.Length > 0);
        DeviceValidationDetailsLBL.Text = Resources.NCMWebContent.WEBDATA_VK_742;
        DeviceValidationDetailsIMG.Src = @"/Orion/NCM/Resources/images/arrowright.gif";
        DeviceValidationIMG.Src = (loginResults.ValidationResult == eLoginResults.Success) ? @"~/Orion/images/nodemgmt_art/icons/icon_OK.gif" : @"~/Orion/images/nodemgmt_art/icons/icon_warning.gif";
        DeviceValidationResult.Text = HttpUtility.HtmlEncode(loginResults.LoginMessage);
        DeviceValidationResult.Style.Add(@"color", (loginResults.ValidationResult == eLoginResults.Success) ? @"black" : @"red");
        DeviceValidationOutput.Text = HttpUtility.HtmlEncode(loginResults.DeviceOutput);
    }

    private void DeviceLoginValidation(string userName)
    {
        var coreNode = _nodes[0];

        var deviceTemplateId = GetSelectedDeviceTemplateId();
        var autoDetect = !(deviceTemplateId > 0);

        LoginValidationResults loginResults;
        if (!coreNode.ObjectSubType.Equals(@"SNMP", StringComparison.InvariantCultureIgnoreCase) && autoDetect)
        {
            loginResults = CreateFailedLoginValidationResults();
            ShowDeviceValidationBar(loginResults);
        }
        else
        {
            var ncmNode = GetNcmNode(coreNode, userName);
            using (var proxy = _isLayer.GetProxy())
            {

                DeviceTemplate deviceTemplate = null;
                var changedIpAddress = GetChangedNodeIpAddress(coreNode, _pluginState);
                if (coreNode.ObjectSubType.Equals(@"SNMP", StringComparison.InvariantCultureIgnoreCase))
                {
                    var nodeForSnmpQuery = CreateCoreNodeForSNMPQuery(coreNode, changedIpAddress);
                    var response = SNMPQuery(nodeForSnmpQuery);

                    if (response != null)
                    {
                        //use data we polled
                        _log.InfoFormat(
                            "Login Validation:  Use fresh SNMP data . SysObjectID:{0}, SysDescription:{1}",
                            response[sysObjectID], response[sysDescr]);
                        deviceTemplate = autoDetect
                            ? GetDeviceTemplateForLoginValidation(response[sysObjectID], response[sysDescr])
                            : GetDeviceTemplateForLoginValidation(deviceTemplateId);
                    }
                    else
                    {
                        _log.WarnFormat(
                            "Login Validation:  Fallback - Use Core SNMP data. SysObjectID:{0}, SysDescription:{1}",
                            coreNode.SysObjectID, coreNode.Description);
                        //try use data from Core as fallback
                        deviceTemplate = autoDetect ?
                            GetDeviceTemplateForLoginValidation(coreNode.SysObjectID, coreNode.Description) :
                            GetDeviceTemplateForLoginValidation(deviceTemplateId);
                    }
                }
                else
                {
                    deviceTemplate = autoDetect ?
                        GetDeviceTemplateForLoginValidation(coreNode.SysObjectID, coreNode.Description) :
                        GetDeviceTemplateForLoginValidation(deviceTemplateId);
                }
                if (deviceTemplate != null)
                    loginResults = proxy.Cirrus.Nodes.ValidateLogin(coreNode.EngineID, ncmNode, changedIpAddress, deviceTemplate);
                else
                    loginResults = CreateFailedLoginValidationResults();
                ShowDeviceValidationBar(loginResults);

                if (loginResults.ValidationResult == eLoginResults.Success && ncmNode.ConnectionProfile >= 0)
                {
                    //set valid connection profile id
                    GlobalConnectionProfile.SelectedValue = loginResults.ConnectionProfileId.ToString();
                    //update all UI device login credentials
                    GlobalConnectionProfile_SelectedIndexChanged(GlobalConnectionProfile, EventArgs.Empty);
                }
            }
        }
    }

    private LoginValidationResults CreateFailedLoginValidationResults()
    {
        var loginResults = new LoginValidationResults();
        loginResults.ValidationResult = eLoginResults.Fail;
        loginResults.LoginMessage = Resources.NCMWebContent.WEBDATA_VK_743;
        loginResults.DeviceOutput = string.Empty;
        return loginResults;
    }

    private int GetSelectedDeviceTemplateId()
    {
        var selectedDeviceTemplateId = 0;
        var cliDeviceTemplatePlugin = NodePropertyPluginManager.Plugins.FirstOrDefault(p => p.Name == @"CliDeviceTemplate");
        object value;
        if (cliDeviceTemplatePlugin != null && cliDeviceTemplatePlugin.State.TryGetValue(@"Cli_SelectedDeviceTemplateId", out value))
        {
            if (!int.TryParse(value.ToString(), out selectedDeviceTemplateId))
                selectedDeviceTemplateId = 0;
        }
        return selectedDeviceTemplateId;
    }

    private DeviceTemplate GetDeviceTemplateForLoginValidation(int deviceTemplateId)
    {
        try
        {
            var mng = ServiceLocator.Container.Resolve<IDeviceTemplatesManager>();
            return mng.GetDeviceTemplate(deviceTemplateId);
        }
        catch (Exception ex)
        {
            _log.Error(ex);
        }
        return null;
    }

    private DeviceTemplate GetDeviceTemplateForLoginValidation(string systemOid, string systemDescription)
    {
        try
        {
            var mng = ServiceLocator.Container.Resolve<IDeviceTemplatesManager>();
            return mng.AutoDetectTemplate(systemOid, systemDescription);
        }
        catch (Exception ex)
        {
            _log.Error(ex);
        }
        return null;
    }

    private NCMNode GetNcmNode(Node coreNode, string userName)
    {
        var ncmNode = NCMNode.InitializeNewNode();

        //node datails
        Guid? ncmNodeId = null;
        if (coreNode.ID > 0)
        {
            ncmNodeId = NcmNodeHelper.GetNcmNodeId(coreNode.ID, userName);
            if (ncmNodeId != null)
            {
                ncmNode.NodeID = ncmNodeId.Value;
            }
        }
        ncmNode.NodeGroup = DecryptAndParseNcmNodeProperty(ncmNodeId, NodeGroup.Text);
        ncmNode.NodeComments = DecryptAndParseNcmNodeProperty(ncmNodeId, Comments.Text);

        //global connection profile
        var profileId = Convert.ToInt32(GlobalConnectionProfile.SelectedValue);
        switch (profileId)
        {
            case -2:
                //new profile selected, use UI node properties to validate device login credentials
                ncmNode.ConnectionProfile = -1; //use no profile selected mode
                ncmNode.UseUserDeviceCredentials = false;
                ncmNode.Username = DecryptAndParseNcmNodeProperty(ncmNodeId, Username.PasswordText);
                ncmNode.Password = DecryptAndParseNcmNodeProperty(ncmNodeId, Password.PasswordText);
                ncmNode.EnableLevel = DecryptAndParseNcmNodeProperty(ncmNodeId, EnableLevel.SelectedValue);
                ncmNode.EnablePassword = DecryptAndParseNcmNodeProperty(ncmNodeId, EnablePassword.PasswordText);
                ncmNode.ExecProtocol = DecryptAndParseNcmNodeProperty(ncmNodeId, ExecProtocol.SelectedValue);
                ncmNode.CommandProtocol = DecryptAndParseNcmNodeProperty(ncmNodeId, CommandProtocol.SelectedValue);
                ncmNode.TransferProtocol = DecryptAndParseNcmNodeProperty(ncmNodeId, TransferProtocol.SelectedValue);
                ncmNode.TelnetPort = DecryptAndParseNcmNodeProperty(ncmNodeId, TelnetPort.Text);
                ncmNode.SSHPort = DecryptAndParseNcmNodeProperty(ncmNodeId, SSHPort.Text);
                break;
            case -1:
                //no profile selected, use UI node properties to validate device login credentials
                ncmNode.ConnectionProfile = -1;
                bool useUserDeviceCredentials;
                if (Boolean.TryParse(UseUserDeviceCredentials.SelectedValue, out useUserDeviceCredentials))
                {
                    ncmNode.UseUserDeviceCredentials = false;
                }
                if (!useUserDeviceCredentials)
                {
                    ncmNode.Username = DecryptAndParseNcmNodeProperty(ncmNodeId, Username.PasswordText);
                    ncmNode.Password = DecryptAndParseNcmNodeProperty(ncmNodeId, Password.PasswordText);
                    ncmNode.EnableLevel = DecryptAndParseNcmNodeProperty(ncmNodeId, EnableLevel.SelectedValue);
                    ncmNode.EnablePassword = DecryptAndParseNcmNodeProperty(ncmNodeId, EnablePassword.PasswordText);
                }
                else
                {
                    using (var proxy = _isLayer.GetProxy())
                    {
                        var swqlFormat = @"SELECT C.UserLevelName, C.UserLevelPassword, C.UserLevelEnableLevel, C.UserLevelEnablePassword 
                                FROM Cirrus.UserLevelLoginCredentials AS C 
                                WHERE C.AccountID='{0}'";

                        var dt = proxy.Query(string.Format(swqlFormat, userName));

                        ncmNode.Username = dt.Rows.Count > 0 ? DecryptAndParseNcmNodeProperty(ncmNodeId, dt.Rows[0]["UserLevelName"].ToString()) : string.Empty;
                        ncmNode.Password = dt.Rows.Count > 0 ? DecryptAndParseNcmNodeProperty(ncmNodeId, dt.Rows[0]["UserLevelPassword"].ToString()) : string.Empty;
                        ncmNode.EnableLevel = dt.Rows.Count > 0 ? dt.Rows[0]["UserLevelEnableLevel"].ToString() : string.Empty;
                        ncmNode.EnablePassword = dt.Rows.Count > 0 ? DecryptAndParseNcmNodeProperty(ncmNodeId, dt.Rows[0]["UserLevelEnablePassword"].ToString()) : string.Empty;
                    }
                }
                ncmNode.ExecProtocol = DecryptAndParseNcmNodeProperty(ncmNodeId, ExecProtocol.SelectedValue);
                ncmNode.CommandProtocol = DecryptAndParseNcmNodeProperty(ncmNodeId, CommandProtocol.SelectedValue);
                ncmNode.TransferProtocol = DecryptAndParseNcmNodeProperty(ncmNodeId, TransferProtocol.SelectedValue);
                ncmNode.TelnetPort = DecryptAndParseNcmNodeProperty(ncmNodeId, TelnetPort.Text);
                ncmNode.SSHPort = DecryptAndParseNcmNodeProperty(ncmNodeId, SSHPort.Text);
                break;
            case 0:
                //auto-detect selected, set only connection profile to validate device login credentials
                ncmNode.ConnectionProfile = 0;
                break;
            default:
                //use existing connection profile to validate device login credentials
                ncmNode.ConnectionProfile = profileId;
                break;
        }

        //communication
        bool allowIntermediary;
        if (Boolean.TryParse(AllowIntermediary.SelectedValue, out allowIntermediary))
        {
            ncmNode.AllowIntermediary = allowIntermediary;
        }

        bool useKeyboardInteractiveAuthentication;
        if (Boolean.TryParse(UseKeyboardInteractiveAuthentication.SelectedValue, out useKeyboardInteractiveAuthentication))
        {
            ncmNode.UseKeybInteractiveAuth = useKeyboardInteractiveAuthentication;
        }

        int encryptionAlgorithm;
        if (int.TryParse(EncryptionAlgorithm.SelectedValue, out encryptionAlgorithm))
        {
            ncmNode.EncryptionAlgorithm = (SSHEncryption)encryptionAlgorithm;
        }

        return ncmNode;
    }

    private Node CreateCoreNodeForSNMPQuery(Node coreNode, string changedIpAddress)
    {
        Node node;
        var ipAddress = IPAddress.Parse(coreNode.IpAddress);
        if (ipAddress.Equals(IPAddress.Parse(changedIpAddress)))
        {
            node = new Node() { IpAddress = coreNode.IpAddress };
        }
        else
        {
            node = new Node() { IpAddress = changedIpAddress };
        }

        SnmpCredentials credentials;
        if (coreNode.SNMPVersion == SNMPVersion.SNMP3)
        {
            var snmpV3UserName = coreNode.ReadWriteCredentials.SNMPv3UserName;
            if (snmpV3UserName != null) snmpV3UserName = snmpV3UserName.Trim();

            if (string.IsNullOrEmpty(snmpV3UserName))
            {
                credentials = coreNode.ReadOnlyCredentials;
            }
            else
            {
                credentials = coreNode.ReadWriteCredentials;
            }
        }
        else
        {
            credentials = new SnmpCredentials() { CommunityString = coreNode.ReadOnlyCredentials.CommunityString };
        }
        node.ReadOnlyCredentials = credentials;
        node.SNMPVersion = coreNode.SNMPVersion;
        node.SNMPPort = coreNode.SNMPPort;
        return node;
    }

    private bool SNMPQuery(Node coreNode, string oid, string snmpGetType, out Dictionary<string, string> response)
    {
        using (var coreProxy = coreBusinessLayerProxyCreator.Create(BusinessLayerExceptionHandler))
        {
            coreProxy.NodeSNMPQuery(coreNode, oid, snmpGetType, out response);
            if (response!=null && response.ContainsKey(sysObjectID) && response.ContainsKey(sysDescr))
            {
                return true;
            }
            return false;
        }
    }

    private Dictionary<string, string> SNMPQuery(Node nodeForSnmpQuery)
    {
        Dictionary<string, string> response;
        if (SNMPQuery(nodeForSnmpQuery, Oid_System, @"getsubtree", out response))
            return response;

        return null;
    }

    private string DecryptAndParseNcmNodeProperty(Guid? nodeId, string property)
    {
        if (string.IsNullOrEmpty(property))
        {
            return property;
        }
        
        var val = decryptor.Decrypt(property);

        val = nodeId.HasValue ? 
            macroParser.ParseMacro(nodeId.Value, val) : macroParser.ParseMacro(null, val);

        return decryptor.Decrypt(val);
    }

    #endregion
}
