﻿using System;
using System.Web.UI;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_Admin_NCMAdmin : MasterPage
{
    private readonly ICommonHelper commonHelper;

    public Orion_NCM_Admin_NCMAdmin()
    {
        commonHelper = ServiceLocator.Container.Resolve<ICommonHelper>();
    }
    
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!SecurityHelper.IsValidNcmAccountRole && !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_VK_356}");
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        commonHelper.Initialise();
    }
}
