using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.NCM.Common.Help;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_Admin_AccountRoles : ProfilePropEditUserControl
{
    public override string PropertyValue
    {
        get
        {
            foreach (KeyValuePair<string, NcmAccount> pair in SecurityHelper.NCMAccountRoles)
            {
                RadioButton radioButton = (RadioButton)table.FindControl(pair.Key);
                if (radioButton != null && radioButton.Checked)
                {
                    return radioButton.ID;
                }
            }
            
            return string.Empty;
        }
        set
        {
            RadioButton radioButton;
            if (string.IsNullOrEmpty(value))
            {
                radioButton = (RadioButton)table.FindControl(SecurityHelper.NCM_ROLE_NONE);
            }
            else
            {
                radioButton = (RadioButton)table.FindControl(value);
            }

            if (radioButton != null) radioButton.Checked = true;
        }
    }

    protected override void OnInit(EventArgs e)
   	{
        table.Rows.Clear();

        TableRow row;
        TableCell cell;

        foreach (KeyValuePair<string, NcmAccount> pair in SecurityHelper.NCMAccountRoles)
        {
            row = new TableRow();
            cell = new TableCell();

            NcmAccount ncmAccount = pair.Value;

            RadioButton radioButton = new RadioButton();
            radioButton.GroupName = @"NCMAccountRole";
            radioButton.ID = pair.Key;
            radioButton.Text = $@"&nbsp;<b>{ncmAccount.RoleName}</b> {ncmAccount.RoleDescription}";

            cell.Style.Add(HtmlTextWriterStyle.PaddingBottom, @"5px");

            cell.Controls.Add(radioButton);
            row.Cells.Add(cell);
            table.Rows.Add(row);
        }

        Literal learnMoreLink = new Literal();
        learnMoreLink.Text =
            $@"<span id='LearnMoreLink' style='font-size:9pt;'>&#0187;&nbsp;<a href='{LearnMoreLink}' target='_blank'>{Resources.NCMWebContent.WEBDATA_VK_288}</a></span>";

        row = new TableRow();
        cell = new TableCell();
        cell.Style.Add(HtmlTextWriterStyle.PaddingLeft, @"5px");
        cell.Controls.Add(learnMoreLink);
        row.Cells.Add(cell);
        table.Rows.Add(row);
   	}

    protected string LearnMoreLink
    {
        get
        {
            var helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
            return helpHelperWrap.GetHelpUrl("OrionNCMPHLearnMoreRoles");
        }
    }

    protected string CSSHeight
    {
        get
        {
            if (Page.Request.Browser.Browser.Equals(@"FIREFOX", StringComparison.InvariantCultureIgnoreCase))
            {
                return @"150px";
            }
            else
            {
                return @"170px;";
            }
        }
    }
}
