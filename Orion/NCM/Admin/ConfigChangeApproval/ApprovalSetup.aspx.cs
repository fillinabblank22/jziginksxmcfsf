using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Common;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.InformationService.Contract2;

public partial class Orion_NCM_Admin_ApprovalSetup : System.Web.UI.Page
{
    private int _previousStepIndex = 0;

    protected List<string> OrionUsers
    {
        get
        {
            if (ViewState["Orion_Users"] != null)
                return (List<string>)ViewState["Orion_Users"];

            var isWrapper = new ISWrapper();
            using (var proxy = isWrapper.Proxy)
            {
                List<string> users = proxy.Query(
                    @"SELECT 
	                    A.AccountID 
                    FROM Orion.Accounts A
                    INNER JOIN Orion.WebUserSettings W 
	                    ON A.AccountID=W.AccountID
                    WHERE 
	                    A.AccountType<>@accountType AND W.SettingName='NCM.NCMAccountRole' AND (W.SettingValue='Administrator' OR W.SettingValue='Engineer')
                    ORDER BY 
                        A.AccountID",
                    new PropertyBag() 
                    {
                        {@"accountType", (int)SolarWinds.Orion.Web.DAL.GroupAccountType.WindowsGroup },
                    }
                    ).AsEnumerable().Select(row => Convert.ToString(row["AccountID"], CultureInfo.InvariantCulture)).Except(NCMApprovalUsers.Select(user => user.UserId)).ToList();

                ViewState["Orion_Users"] = users;
                return users;
            }
        }
        set { ViewState["Orion_Users"] = value; }
    }

    protected List<NCMApprovalUser> NCMApprovalUsers
    {
        get
        {
            if (ViewState["NCM_ApprovalUsers"] != null)
                return (List<NCMApprovalUser>)ViewState["NCM_ApprovalUsers"];

            var isWrapper = new ISWrapper();
            using (var proxy = isWrapper.Proxy)
            {
                var users = proxy.Cirrus.ConfigChangeApproval.GetApprovalUsers();
                ViewState["NCM_ApprovalUsers"] = users;
                return users;
            }
        }
        set { ViewState["NCM_ApprovalUsers"] = value; }
    }

    protected string SetupWizardHeader
    {
        get
        {
            switch (wizard.ActiveStepIndex)
            {
                case 0:
                    return
                        $@"<div class=""ProgressIndicator""><div class=""PI_on"">&nbsp;{Resources.NCMWebContent.ConfigChangeApproval_Mode_WizardStep}</div><img src=""/Orion/Images/ProgressIndicator/pi_sep_on_off_sm.gif"" /><div class=""PI_off"">&nbsp;SMTP</div><img src=""/Orion/Images/ProgressIndicator/pi_sep_off_off_sm.gif"" /><div class=""PI_off"">&nbsp;{Resources.NCMWebContent.WEBDATA_VK_51}</div><img src=""/Orion/Images/ProgressIndicator/pi_sep_off_off_sm.gif"" /><div class=""PI_off"">&nbsp;{Resources.NCMWebContent.WEBDATA_VK_52}</div><img src=""/Orion/Images/ProgressIndicator/pi_sep_off_off_sm.gif"" /></div>";
                case 1:
                    return
                        $@"<div class=""ProgressIndicator""><div class=""PI_off"">&nbsp;{Resources.NCMWebContent.ConfigChangeApproval_Mode_WizardStep}</div><img src=""/Orion/Images/ProgressIndicator/pi_sep_off_on_sm.gif"" /><div class=""PI_on"">&nbsp;SMTP</div><img src=""/Orion/Images/ProgressIndicator/pi_sep_on_off_sm.gif"" /><div class=""PI_off"">&nbsp;{Resources.NCMWebContent.WEBDATA_VK_51}</div><img src=""/Orion/Images/ProgressIndicator/pi_sep_off_off_sm.gif"" /><div class=""PI_off"">&nbsp;{Resources.NCMWebContent.WEBDATA_VK_52}</div><img src=""/Orion/Images/ProgressIndicator/pi_sep_off_off_sm.gif"" /></div>";
                case 2:
                    return
                        $@"<div class=""ProgressIndicator""><div class=""PI_off"">&nbsp;{Resources.NCMWebContent.ConfigChangeApproval_Mode_WizardStep}</div><img src=""/Orion/Images/ProgressIndicator/pi_sep_off_off_sm.gif"" /><div class=""PI_off"">&nbsp;SMTP</div><img src=""/Orion/Images/ProgressIndicator/pi_sep_off_on_sm.gif"" /><div class=""PI_on"">&nbsp;{Resources.NCMWebContent.WEBDATA_VK_51}</div><img src=""/Orion/Images/ProgressIndicator/pi_sep_on_off_sm.gif"" /><div class=""PI_off"">&nbsp;{Resources.NCMWebContent.WEBDATA_VK_52}</div><img src=""/Orion/Images/ProgressIndicator/pi_sep_off_off_sm.gif"" /></div>";
                case 3:
                    return
                        $@"<div class=""ProgressIndicator""><div class=""PI_off"">&nbsp;{Resources.NCMWebContent.ConfigChangeApproval_Mode_WizardStep}</div><img src=""/Orion/Images/ProgressIndicator/pi_sep_off_off_sm.gif"" /><div class=""PI_off"">&nbsp;SMTP</div><img src=""/Orion/Images/ProgressIndicator/pi_sep_off_off_sm.gif"" /><div class=""PI_off"">&nbsp;{Resources.NCMWebContent.WEBDATA_VK_51}</div><img src=""/Orion/Images/ProgressIndicator/pi_sep_off_on_sm.gif"" /><div class=""PI_on"">&nbsp;{Resources.NCMWebContent.WEBDATA_VK_52}</div><img src=""/Orion/Images/ProgressIndicator/pi_sep_on_off_sm.gif"" /></div>";
                default:
                    return string.Empty;
            }
        }
    }

    protected string SetupWizardTitle
    {
        get
        {
            switch (wizard.ActiveStepIndex)
            {
                case 0:
                    return Resources.NCMWebContent.WEBDATA_VK_1023;
                case 1:
                    return Resources.NCMWebContent.WEBDATA_VK_54;
                case 2:
                    return Resources.NCMWebContent.WEBDATA_VK_56;
                case 3:
                    if (approvalMode.Mode == eApprovalMode.OneLevel)
                        return Resources.NCMWebContent.WEBDATA_VK_58;
                    else
                        return Resources.NCMWebContent.WEBDATA_VK_1025;
                default:
                    return string.Empty;
            }
        }
    }

    protected string SetupWizardSubtitle
    {
        get
        {
            switch (wizard.ActiveStepIndex)
            {
                case 0:
                    return Resources.NCMWebContent.WEBDATA_VK_1016;
                case 1:
                    return Resources.NCMWebContent.WEBDATA_VK_55;
                case 2:
                    return Resources.NCMWebContent.WEBDATA_VK_57;
                case 3:
                    return string.Empty;
                default:
                    return string.Empty;
            }
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (SolarWinds.NCMModule.Web.Resources.CommonHelper.IsDemoMode()
           || (!(Profile.AllowAdmin && SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR)) && !(this.Page is IBypassAccessLimitation)))
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_VK_554}");
        }

        SWISValidator validator = new SWISValidator();
        validator.ContentContainer = ContentContainer;
        validator.RedirectIfValidationFailed = true;
        ErrorContainer.Controls.Add(validator);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_VK_49;

        if (!Page.IsPostBack)
        {
            if (wizard.ActiveStepIndex == 0)
            {
                approvalMode.Mode = GetApprovalMode();
                smtp.Load();
            }
        }

        _previousStepIndex = wizard.ActiveStepIndex;
    }

    protected void wizard_OnActiveStepChanged(object sender, EventArgs e)
    {
        switch (wizard.ActiveStepIndex)
        {
            case 0:
                break;
            case 1:
                if (_previousStepIndex == 0)
                    SetApprovalMode(approvalMode.Mode);

                if (_previousStepIndex == 2)
                {
                    ClearValueAttribute(smtp);
                    smtp.Load();
                }
                break;
            case 2:
                if (_previousStepIndex == 1)
                    smtp.Save();

                if (_previousStepIndex == 3 && (approvalMode.Mode == eApprovalMode.TwoLevelAll || approvalMode.Mode == eApprovalMode.TwoLevelWebUploader))
                {
                    OrionUsers = null;
                    NCMApprovalUsers = null;
                }

                email.VisibleEmailTo = (approvalMode.Mode == eApprovalMode.OneLevel);
                email.Load();
                break;
            case 3:
                email.VisibleEmailTo = (approvalMode.Mode == eApprovalMode.OneLevel);
                email.Save();

                if (approvalMode.Mode == eApprovalMode.OneLevel)
                {
                    btnFinish.LocalizedText = SolarWinds.Orion.Web.UI.CommonButtonType.CustomText;
                    btnFinish.Text = Resources.NCMWebContent.WEBDATA_VK_53;
                    bntCancelFromStep4.Visible = true;

                    UserRolesHolder.Visible = true;
                    ApprovalGroupsHolder.Visible = false;
                }
                else
                {
                    btnFinish.LocalizedText = SolarWinds.Orion.Web.UI.CommonButtonType.CustomText;
                    btnFinish.Text = Resources.NCMWebContent.ConfigChangeApproval_ButtonText_Finish;
                    bntCancelFromStep4.Visible = false;

                    UserRolesHolder.Visible = false;
                    ApprovalGroupsHolder.Visible = true;

                    approvalGroup1.DataBind(eApprovalGroup.Group1, OrionUsers, NCMApprovalUsers);
                    approvalGroup2.DataBind(eApprovalGroup.Group2, OrionUsers, NCMApprovalUsers);
                }
                break;
        }
    }

    protected void wizard_FinishButtonClick(object sender, WizardNavigationEventArgs e)
    {
        if (approvalMode.Mode == eApprovalMode.TwoLevelAll || approvalMode.Mode == eApprovalMode.TwoLevelWebUploader)
        {
            if (!Page.IsValid)
                return;

            UpdateApprovalUsers(NCMApprovalUsers);
        }

        WebSettingsDAL.Set(ConfigChangeApprovalHelper.NCM_ENABLE_APPROVAL_SYSTEM, @"true");

        if (approvalMode.Mode == eApprovalMode.OneLevel)
            Page.Response.Redirect("/Orion/Admin/Accounts/Accounts.aspx");
        else
            Page.Response.Redirect("/Orion/NCM/Admin/Default.aspx");
    }

    protected void wizard_CancelButtonClick(object sender, EventArgs e)
    {
        if (wizard.ActiveStepIndex == 3) //last step
            WebSettingsDAL.Set(ConfigChangeApprovalHelper.NCM_ENABLE_APPROVAL_SYSTEM, @"true");

        Page.Response.Redirect("/Orion/NCM/Admin/Default.aspx");
    }

    protected void ValidateNCMApprovalUsers(object source, ServerValidateEventArgs args)
    {
        var group1 = NCMApprovalUsers.Where(user => user.GroupID == eApprovalGroup.Group1);
        var group2 = NCMApprovalUsers.Where(user => user.GroupID == eApprovalGroup.Group2);
        args.IsValid = group1.Any() && group2.Any();
    }

    protected void AddUser_Click(NCMApprovalUser userToAdd)
    {
        var approvalUser = NCMApprovalUsers.Find(user => user.UserId.Equals(userToAdd.UserId, StringComparison.InvariantCultureIgnoreCase));
        if (approvalUser == null)
        {
            AddUser(NCMApprovalUsers, userToAdd);
            RemoveUser(OrionUsers, userToAdd.UserId);

            approvalGroup1.DataBind(eApprovalGroup.Group1, OrionUsers, NCMApprovalUsers);
            approvalGroup2.DataBind(eApprovalGroup.Group2, OrionUsers, NCMApprovalUsers);
        }
    }

    protected void RemoveUser_Click(string userId)
    {
        var userToRemove = NCMApprovalUsers.Find(user => user.UserId.Equals(userId, StringComparison.InvariantCultureIgnoreCase));
        if (userToRemove != null)
        {
            RemoveUser(NCMApprovalUsers, userToRemove);
            AddUser(OrionUsers, userToRemove.UserId);

            approvalGroup1.DataBind(eApprovalGroup.Group1, OrionUsers, NCMApprovalUsers);
            approvalGroup2.DataBind(eApprovalGroup.Group2, OrionUsers, NCMApprovalUsers);
        }
    }

    /// <summary>
    /// Fix to clear password on PostBack in SMTP Server control while used by wizard.
    /// Implemented to avoid changes in Core control it selves.
    /// </summary>
    /// <param name="source">The parent control.</param>
    private void ClearValueAttribute(Control parent)
    {
        foreach (Control ctrl in parent.Controls)
        {
            if (ctrl is TextBox)
            {
                TextBox textBox = ctrl as TextBox;
                if (textBox.TextMode == TextBoxMode.Password)
                {
                    textBox.Attributes[@"value"] = string.Empty;
                }
            }
        }
    }

    private eApprovalMode GetApprovalMode()
    {
        var isWrapper = new ISWrapper();
        using (var proxy = isWrapper.Proxy)
        {
            return proxy.Cirrus.ConfigChangeApproval.GetApprovalMode();
        }
    }

    private void SetApprovalMode(eApprovalMode mode)
    {
        var isWrapper = new ISWrapper();
        using (var proxy = isWrapper.Proxy)
        {
            proxy.Cirrus.ConfigChangeApproval.SetApprovalMode(mode);
        }
    }

    private void UpdateApprovalUsers(List<NCMApprovalUser> approvalUsers)
    {
        var isWrapper = new ISWrapper();
        using (var proxy = isWrapper.Proxy)
        {
            proxy.Cirrus.ConfigChangeApproval.UpdateApprovalUsers(approvalUsers);
        }
    }

    private void AddUser<T>(object list, T item)
    {
        if (list.GetType() == typeof(List<string>))
        {
            var temp = list as List<string>;
            temp.Add(item as string);
            temp.Sort(StringComparer.InvariantCultureIgnoreCase);
            list = temp;
        }
        else if (list.GetType() == typeof(List<NCMApprovalUser>))
        {
            var temp = list as List<NCMApprovalUser>;
            temp.Add(item as NCMApprovalUser);
            list = temp;
        }
    }

    private void RemoveUser<T>(object list, T item)
    {
        if (list.GetType() == typeof(List<string>))
        {
            var temp = list as List<string>;
            temp.Remove(item as string);
            temp.Sort(StringComparer.InvariantCultureIgnoreCase);
            list = temp;
        }
        else if (list.GetType() == typeof(List<NCMApprovalUser>))
        {
            var temp = list as List<NCMApprovalUser>;
            temp.Remove(item as NCMApprovalUser);
            list = temp;
        }
    }
}
