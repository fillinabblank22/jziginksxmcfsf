using System;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_Admin_ApprovalRequests : System.Web.UI.Page
{
    private bool EnableApprovalSystem
    {
        get
        {
            try
            {
                return Convert.ToBoolean(SolarWinds.Orion.Core.Common.WebSettingsDAL.Get(ConfigChangeApprovalHelper.NCM_ENABLE_APPROVAL_SYSTEM));
            }
            catch { return false; }
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        Page.Title = ConfigChangeApprovalHelper.IsApproverUser || CommonHelper.IsDemoMode() ? Resources.NCMWebContent.WEBDATA_VK_27 : Resources.NCMWebContent.WEBDATA_VK_28;

        SWISValidator validator = new SWISValidator();
        validator.ContentContainer = ContentContainer;
        validator.RedirectIfValidationFailed = true;
        ErrorContainer.Controls.Add(validator);

        GridContainer.Visible = EnableApprovalSystem;
        approval_disabled.Visible = !EnableApprovalSystem;
    }
}
