<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="ApprovalScriptView.aspx.cs" Inherits="Orion_NCM_Admin_ConfigChangeApproval_ApprovalScriptView" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <div style="padding:10px;">
        <asp:TextBox ID="txtScript" runat="server" Font-Size="9pt" Font-Names="Arial,Verdana,Helvetica,sans-serif" Width="800px" Height="700px" ReadOnly="true" TextMode="MultiLine" BorderStyle="solid" BorderColor="#dcdbd7" BackColor="White" BorderWidth="1px"/>
        <ncm:ExceptionWarning ID="ExWarn" runat="server" Visible="false" />
    </div>
</asp:Content>

