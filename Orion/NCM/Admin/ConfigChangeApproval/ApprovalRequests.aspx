<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="ApprovalRequests.aspx.cs" Inherits="Orion_NCM_Admin_ApprovalRequests" %>

<%@ Register Src="~/Orion/NCM/CustomPageLinkHidding.ascx" TagPrefix="ncm" TagName="custPageLinkHidding" %>
<%@ Register TagPrefix="ncm1" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMPHPendingApprovalList" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <ncm:custPageLinkHidding ID="CustPageLinkHidding1"  runat="server" />
    <ncm1:ConfigSnippetUISettings ID="ReportUISetting" runat="server" Name="NCM_ApprovalRequests_Columns" DefaultValue="[]" RenderTo="columnModel" />
    <ncm1:ExtLocalDateTimePattern ID="ExtLocalDateTimePattern1" runat="server"/>
    
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/PolicyReports/PolicyReport.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
    
    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script type="text/javascript" src="../../JavaScript/main.js"></script>
    <script type="text/javascript" src="ApprovalRequestsViewer.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippetsHelper.js"></script>
    
    <script type="text/javascript">
        SW.NCM.IsApproverUser = <%=SolarWinds.NCMModule.Web.Resources.ConfigChangeApprovalHelper.IsApproverUser.ToString().ToLowerInvariant()%>;
        SW.NCM.IsDemoMode = <%=SolarWinds.NCMModule.Web.Resources.CommonHelper.IsDemoMode().ToString().ToLowerInvariant() %>;
        SW.NCM.CurrentUserApprovalRole = <%=SolarWinds.NCMModule.Web.Resources.ConfigChangeApprovalHelper.GetCurrentUserApprovalRole().ToString(@"D") %>;
    </script>
    
    <style type="text/css">
        .approvalRequest,.approvalHistory
        {
            cursor: pointer;
        }
	</style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
	<asp:Panel id="ContentContainer" runat="server">
        <div style="padding:10px;">
            <table id="approval_disabled" runat="server">
                <tr>
                    <td>
                        <div class='sw-suggestion sw-suggestion-fail'><span class='sw-suggestion-icon'></span>
                            <%= Resources.NCMWebContent.WEBDATA_VK_29 %> &raquo; <a class='sw-suggestion-link' href='/Orion/NCM/Admin/Default.aspx'><%= Resources.NCMWebContent.WEBDATA_VK_22 %></a>
                        </div>
                    </td>
                </tr>
            </table>
            <asp:Panel ID="GridContainer" runat="server" >
	            <div id="InfoContainer" style="padding:10px 0px 10px 0px;"></div>
                <div style="border:solid 1px #dcdbd7;padding:10px;background-color:#fff;">
                    <table class="ExistingElements" cellpadding="0" cellspacing="0" width="100%">
                        <tr valign="top" align="left">                    
                            <td id="gridCell">
                                <div id="Grid" />
                            </td>                    
                        </tr>
                    </table>
                    <div id="originalQuery">
                    </div>
                    <div id="test">
                    </div>
                    <pre id="stackTrace"></pre>
                </div>
                <div id="requestCommentsDialog" class="x-hidden">
                    <div id="requestCommentsBody" class="x-panel-body" style="padding: 10px; height: 210px; overflow: auto;"></div>
                </div>
                <div id="jobLogDialog" class="x-hidden">
                    <div id="jobLogBody" class="x-panel-body" >
                        <div id="jobLogTextArea"></div>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </asp:Panel>    
    <div style="padding:10px;" id="ErrorContainer" runat="server"/>
</asp:Content>

