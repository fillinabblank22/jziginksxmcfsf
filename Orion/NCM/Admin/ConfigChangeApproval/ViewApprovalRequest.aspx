<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="ViewApprovalRequest.aspx.cs" Inherits="Orion_NCM_Admin_ViewApprovalRequest" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.ConfigChangeApproval" Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMPHViewConfigChangeRequest" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
    <style type="text/css">
        .TreeItem 
        {
            border-bottom: 0px;
            font-family: Arial,Verdana,Helvetica,sans-serif;
            font-size: 8pt;
            font-weight: normal;
            margin-bottom: 0;
            margin-top: 0;
        }
        
        .LabelBold
        {
            font-family: Arial,Verdana,Helvetica,sans-serif;
            font-weight: bold;
            padding-bottom: 5px;
        }
        
        .LabelNormal
        {
            font-family: Arial,Verdana,Helvetica,sans-serif;
            font-weight: normal;
            padding-bottom: 20px;
        }
        
        .ActiveLink:hover
	    {
		    cursor: pointer;		    
	    }
	    
        .ActiveLink
	    {
		    color:#336699 !important;
	    }
	    
	    .TargetNodesHeader 
	    {
            background-color: #E1E1E0;
            background-image: url("/Orion/NCM/Resources/images/ConfigChangeApproval/bg_groupby.gif");
            background-repeat: repeat-x;
            height: 25px;
        }
    </style>

    <script type="text/javascript" src="../../JavaScript/main.js"></script>
    <script type="text/javascript">
        var show = false;
        $(document).ready(function () {
            show = false;
            ShowHideScriptDetails();

            var list = document.getElementById("<%= StatusList.ClientID %>");
            if (list != null) {
                var value = list.options[list.selectedIndex].value;
                ShowHideNotesValidator(value);
            }

            var rb = document.getElementById("<%=rbSpecifiedTime.ClientID %>");
            if (rb != null) {
                var checked = rb.checked;
                ShowHideCalendar(checked);
            }
        });

        function ShowHideScriptDetails() {
            if (show) {
                $('#ScriptDetailsDIV').show();
                show = false;
            }
            else {
                $('#ScriptDetailsDIV').hide();
                show = true;
            }
        }

        function ShowHideCalendar(checked) {
            if (checked) {
                $('#CalendarTR').show();
            }
            else {
                $('#CalendarTR').hide();
            }
        }

        function ddlStatusListIndexChange(obj) {
            var value = obj.options[obj.selectedIndex].value;
            ShowHideNotesValidator(value);
        }

        function ShowHideNotesValidator(value) {
            var declineStatus = 1;
            var notesValidator = document.getElementById("<%= NotesValidator.ClientID %>");
            if (value == declineStatus) {
                if (notesValidator != null) ValidatorEnable(notesValidator, true);
            } else {
                if (notesValidator != null) ValidatorEnable(notesValidator, false);
            }
        }
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <div style="padding:10px;">
        <div id="MainContent" runat="server" style="border:solid 1px #dcdbd7;background-color:#fff;width:80%;padding:10px;">
            <div class="LabelBold">
                <%=Resources.NCMWebContent.WEBDATA_VM_123%>
            </div>
            <div class="LabelNormal">
                <asp:Label ID="Status" runat="server"></asp:Label>
                <asp:DropDownList ID="StatusList" runat="server" Font-Size="9pt" Font-Names="Arial,Verdana,Helvetica,sans-serif" BorderColor="#999999" BorderWidth="1px" onchange="ddlStatusListIndexChange(this);"></asp:DropDownList>
            </div>

            <div class="LabelBold">
                <%=Resources.NCMWebContent.WEBDATA_VK_42 %>
            </div>
            <div class="LabelNormal">
                <asp:Label ID="RequestType" runat="server"></asp:Label>
            </div>

            <div class="LabelBold">
                <%=Resources.NCMWebContent.WEBDATA_VK_835 %>
            </div>
            <div class="LabelNormal" style="font-size:8pt;">
                &#0187;&nbsp;<span class="ActiveLink" style="color:#336699" onclick="ShowHideScriptDetails();"><%=Resources.NCMWebContent.WEBDATA_VK_836 %></span>
                <div style="padding-left:10px;" id="ScriptDetailsDIV">
                    <div id="ConfigTypeDIV" runat="server" style="padding-top:5px;">
                        <span class="LabelBold"><%=Resources.NCMWebContent.WEBDATA_VK_43 %></span>&nbsp;<span class="LabelNormal"><asp:Label ID="ConfigType" runat="server"></asp:Label></span>
                    </div>
                    <div id="ScriptDIV" runat="server" style="padding-top:5px;">
                        <div class="LabelBold"><%=Resources.NCMWebContent.WEBDATA_VK_45 %></div>
                        <asp:TextBox ID="Script" runat="server" Font-Size="8pt" Font-Names="Arial,Verdana,Helvetica,sans-serif" BorderColor="#999999" BorderWidth="1px" style="resize: none;" spellcheck="false" TextMode="MultiLine" Width="50%" Height="200px" ReadOnly="true" />
                        <asp:RequiredFieldValidator 
                            ID="rfvScript" 
                            runat="server"
                            ControlToValidate="Script" 
                            Display="None"
                            ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VM_199 %>"  />
                    </div>
                    <div id="RebootDIV" runat="server" style="padding-top:5px;">
                        <asp:CheckBox ID="Reboot" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_47 %>"/>
                    </div>
                    <div id="ApprovalScriptViewerDIV" runat="server" style="padding-top:5px;width:50%">
                        <asp:UpdatePanel ID="ApprovalScriptViewerUpdatePanel" UpdateMode="Conditional" runat="server">
                            <ContentTemplate>
                                <div style="padding-bottom:5px;"><%=Resources.NCMWebContent.WEBDATA_VK_46 %></div>
                                <ncm:ApprovalScriptViewer ID="ApprovalScriptViewer" runat="server"/>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
            
            <div class="LabelBold">
                <%=Resources.NCMWebContent.WEBDATA_VK_837 %>
            </div>
            <div class="LabelNormal">
                <asp:Label ID="ExecutionMethod" runat="server"></asp:Label>
                <div id="ExecutionMethodRadioButtonList" runat="server">
                    <table style="width:40%">
                        <tr>
                            <td nowrap="nowrap">
                                <asp:RadioButton ID="rbRunImmediately" runat="server" onclick="ShowHideCalendar(false);" GroupName="ExecutionMethod" />
                            </td>
                            <td style="padding-left:40px;" nowrap="nowrap">
                                <asp:RadioButton ID="rbSpecifiedTime" runat="server" onclick="ShowHideCalendar(this.checked);" GroupName="ExecutionMethod" />
                            </td>
                            <td style="padding-left:40px;" nowrap="nowrap">
                                <asp:RadioButton ID="rbReturnToRequestor" runat="server" onclick="ShowHideCalendar(false);" GroupName="ExecutionMethod" />
                            </td>
                        </tr>
                        <tr id="CalendarTR">
                            <td>&nbsp;</td>
                            <td style="padding-left:60px;width:1%;">
                                <ncm:Calendar ID="RunAt" runat="server" Text="" AlternativeDrawingMode="false" DisplayTime="true" />
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </div>                                    
                        
            <div id="LimitationHelpLinkHolder" runat="server" style="font-size:8pt;padding-bottom:5px;">
                &#0187;&nbsp;<a href="<%=HelpLink %>" target="_blank" class="ActiveLink"><%=Resources.NCMWebContent.WEBDATA_IA_37 %></a>
            </div>
            <table cellpadding="0" cellspacing="0" width="50%" style="border: solid 1px #dfdfde;">            
                <tr class="TargetNodesHeader">
                    <td style="padding-left:5px;font-family: Arial,Verdana,Helvetica,sans-serif;font-weight: bold;">
                        <%=Resources.NCMWebContent.WEBDATA_VK_838%>
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <asp:Label ID="Actions" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_852 %>"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:UpdatePanel ID="TargetNodesUpdatePanel" UpdateMode="Conditional" runat="server">
                            <ContentTemplate>
                                <div id="TargetNodesDIV" runat="server"></div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>

            <div class="LabelBold" style="padding-top:20px;">
                <%=Resources.NCMWebContent.WEBDATA_VK_40 %>
            </div>
            <div class="LabelNormal">
                <asp:Label ID="SubmittedBy" runat="server"></asp:Label>
            </div>

            <div class="LabelBold">
                <%=Resources.NCMWebContent.WEBDATA_VK_839 %>
            </div>
            <div class="LabelNormal">
                <asp:Label ID="StatusChangeTime" runat="server"></asp:Label>
            </div>

            <div class="LabelBold">
                <%=Resources.NCMWebContent.WEBDATA_VK_44 %>
            </div>
            <div class="LabelNormal">
                <asp:Label ID="RequestTime" runat="server"></asp:Label>
            </div>
            <div class="LabelBold">
                <%=Resources.NCMWebContent.ConfigChangeApproval_Label_RequestorEmail %>
            </div>
            <div class="LabelNormal">
                <asp:TextBox ID="RequestorEmail" runat="server" Width="300px" BorderColor="#999999" BorderWidth="1px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="RequestorEmail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            </div>
            <div class="LabelBold">
                <%=Resources.NCMWebContent.WEBDATA_VK_840 %>
            </div>
            <div class="LabelNormal">
                <asp:Label ID="ApprovedBy" runat="server"></asp:Label>
            </div>

            <div class="LabelBold">
                <%=Resources.NCMWebContent.WEBDATA_VK_41 %>
            </div>
            <div class="LabelNormal">
                <asp:TextBox ID="Notes" runat="server" Font-Size="9pt" Font-Names="Arial,Verdana,Helvetica,sans-serif" BorderColor="#999999" BorderWidth="1px" style="resize: none;" spellcheck="false" TextMode="MultiLine" Width="50%" Height="50px"/>
                <asp:RequiredFieldValidator 
                            ID="NotesValidator" 
                            runat="server"
                            ControlToValidate="Notes" 
                            Display="Dynamic"
                            Text="*"
                            ErrorMessage="*You cannot decline request with empty notes."  Enabled="False" />
            </div>
        </div>
        <ncm:ExceptionWarning ID="ExWarn" runat="server" Visible="false" />
        
        <asp:ValidationSummary ID="validationErrors" ShowSummary="false" DisplayMode="SingleParagraph" runat="server" ShowMessageBox="true" />
    </div>
    <div style="padding:10px;">
        <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Save" OnClick="btnSubmit_Click" DisplayType="Primary" CausesValidation="true"/> 
        <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" OnClick="btnCancel_Click" DisplayType="Secondary" CausesValidation="false"/>
    </div>
</asp:Content>

