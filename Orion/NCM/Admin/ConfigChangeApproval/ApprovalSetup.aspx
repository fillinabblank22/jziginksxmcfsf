<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="ApprovalSetup.aspx.cs" Inherits="Orion_NCM_Admin_ApprovalSetup" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/NCM/Admin/SMTPServer.ascx" TagPrefix="ncm" TagName="SMTPServer" %>
<%@ Register Src="~/Orion/NCM/Controls/EmailSettings.ascx" TagPrefix="ncm" TagName="EmailSettings" %>
<%@ Register Src="~/Orion/NCM/Admin/ConfigChangeApproval/Controls/ApprovalMode.ascx" TagPrefix="ncm" TagName="ApprovalMode" %>
<%@ Register Src="~/Orion/NCM/Admin/ConfigChangeApproval/Controls/ApprovalGroup.ascx" TagPrefix="ncm" TagName="ApprovalGroup" %>
<%@ Register Src="~/Orion/NCM/Admin/ConfigChangeApproval/Controls/UserRoles.ascx" TagPrefix="ncm" TagName="UserRoles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
<orion:Include runat="server" File="styles/ProgressIndicator.css" />
    <style type="text/css">
        .WizardStyle td
        {
	        margin: 0;
	        padding: 0;
        }

        .WizardStartNextButtonStyle
        {
            padding-right: 20px;
        }

        .WizardStepPreviousButtonStyle
        {
	        padding-right: 5px;
        }

        .WizardFinishCompleteButtonStyle
        {
            padding-right: 20px;
        }

        .WizardCancelButtonStyle
        {	
            padding-right: 5px;
        }
        
        .WizardTitle
        {
            font-size: 14px;
            font-weight: bold;
        }
        
        .HideWizardNavigationButtons 
        {
            display: none;
        }
        
        .SettingTextField
        {
            border:1px solid #89A6C0;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMConfigApprovalSMTP" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <asp:Panel ID="ContentContainer" runat="server">
    <asp:UpdatePanel ID="uPanel" UpdateMode="Conditional" runat="server">        
        <ContentTemplate>
            <div style="padding:10px">
                <div style="border:solid 1px #dcdbd7;;width:850px;background-color:#fff;">
                    <%=SetupWizardHeader%>
                    <div style="padding:10px;">
                        <asp:Wizard ID="wizard" runat="server" DisplaySideBar="false" DisplayCancelButton="true" CssClass="WizardStyle" Width="100%"
                            OnActiveStepChanged="wizard_OnActiveStepChanged"
                            OnCancelButtonClick="wizard_CancelButtonClick"
                            OnFinishButtonClick="wizard_FinishButtonClick"
                            StartNextButtonStyle-CssClass="HideWizardNavigationButtons"
                            StepNextButtonStyle-CssClass="HideWizardNavigationButtons"
                            StepPreviousButtonStyle-CssClass="HideWizardNavigationButtons"
                            FinishPreviousButtonStyle-CssClass="HideWizardNavigationButtons"
                            FinishCompleteButtonStyle-CssClass="HideWizardNavigationButtons"
                            CancelButtonStyle-CssClass="HideWizardNavigationButtons">
                            <WizardSteps>
                                <asp:WizardStep ID="ApprovalModeStep">
                                    <table width="100%">
                                        <tr>
                                            <td class="WizardTitle"><%=SetupWizardTitle %></td>
                                        </tr>
                                        <tr>
                                            <td><%=SetupWizardSubtitle %></td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top:10px;"><ncm:ApprovalMode ID="approvalMode" runat="server" /></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:right;padding-top:20px;">
                                                <span class="WizardStartNextButtonStyle"><orion:LocalizableButton runat="server" ID="btnMoveToStep2" LocalizedText="Submit" 
                                                DisplayType="Primary" CommandName="MoveNext" CausesValidation="true"/></span>
                                                <span class="WizardCancelButtonStyle"><orion:LocalizableButton runat="server" ID="bntCancelFromStep1" LocalizedText="Cancel" 
                                                DisplayType="Secondary" CommandName="Cancel" CausesValidation="false"/></span>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:WizardStep>
                                <asp:WizardStep ID="SmtpStep">
                                    <table width="100%">
                                        <tr>
                                            <td class="WizardTitle"><%=SetupWizardTitle %></td>
                                        </tr>
                                        <tr>
                                            <td><%=SetupWizardSubtitle %></td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top:10px;"><ncm:SMTPServer ID="smtp" runat="server"/></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:right;padding-top:20px;">
                                                <span class="WizardStepPreviousButtonStyle"><orion:LocalizableButton runat="server" ID="btnBackToStep1" LocalizedText="Back" 
                                                DisplayType="Secondary" CommandName="MovePrevious" CausesValidation="false"/></span>
                                                <span class="WizardStartNextButtonStyle"><orion:LocalizableButton runat="server" ID="btnMoveToStep3" LocalizedText="Submit" 
                                                DisplayType="Primary" CommandName="MoveNext" CausesValidation="true"/></span>
                                                <span class="WizardCancelButtonStyle"><orion:LocalizableButton runat="server" ID="bntCancelFromStep2" LocalizedText="Cancel" 
                                                DisplayType="Secondary" CommandName="Cancel" CausesValidation="false"/></span>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:WizardStep>
                                <asp:WizardStep ID="EmailStep">
                                    <table width="100%">
                                        <tr>
                                            <td class="WizardTitle"><%=SetupWizardTitle %></td>
                                        </tr>
                                        <tr>
                                            <td><%=SetupWizardSubtitle %></td>
                                        </tr>
                                        <tr>
                                            <td><ncm:EmailSettings ID="email" runat="server"/></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:right;padding-top:20px;">
                                                <span class="WizardStepPreviousButtonStyle"><orion:LocalizableButton runat="server" ID="btnBackToStep2" LocalizedText="Back" 
                                                DisplayType="Secondary" CommandName="MovePrevious" CausesValidation="false"/></span>
                                                <span class="WizardStartNextButtonStyle"><orion:LocalizableButton runat="server" ID="btnMoveToStep4" LocalizedText="Submit" 
                                                DisplayType="Primary" CommandName="MoveNext" CausesValidation="true"/></span>
                                                <span class="WizardCancelButtonStyle"><orion:LocalizableButton runat="server" ID="bntCancelFromStep3" LocalizedText="Cancel" 
                                                DisplayType="Secondary" CommandName="Cancel" CausesValidation="false"/></span>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:WizardStep>
                                <asp:WizardStep ID="UserRolesStep">
                                    <table width="100%">
                                        <tr>
                                            <td class="WizardTitle"><%=SetupWizardTitle %></td>
                                        </tr>
                                        <tr>
                                            <td><%=SetupWizardSubtitle %></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div id="UserRolesHolder" runat="server">
                                                    <ncm:UserRoles ID="userRoles" runat="server" />
                                                </div>
                                                <div id="ApprovalGroupsHolder" runat="server">
                                                    <div>
                                                        <%=Resources.NCMWebContent.WEBDATA_VK_1024 %>
                                                    </div>
                                                    <div style="padding-top:10px;">
                                                        <%=Resources.NCMWebContent.WEBDATA_VK_1026 %>&nbsp;<a style="color:#336699;" href="/Orion/Admin/Accounts/Accounts.aspx" target="_blank"><%=Resources.NCMWebContent.WEBDATA_VK_53 %>&nbsp;&#0187;</a>
                                                    </div>
                                                    <div style="padding-top:30px;">
                                                        <ncm:ApprovalGroup ID="approvalGroup1" runat="server" GroupName="<%$ Resources: NCMWebContent, Admin_ApprovalSetup_Roles_ApprovalLevel_1 %>" GroupSubtitle="<%$ Resources: NCMWebContent, WEBDATA_VK_1027 %>" OnAddUserClick="AddUser_Click" OnRemoveUserClick="RemoveUser_Click" />
                                                    </div>
                                                    <div style="padding-top:50px;">
                                                        <ncm:ApprovalGroup ID="approvalGroup2" runat="server" GroupName="<%$ Resources: NCMWebContent, Admin_ApprovalSetup_Roles_ApprovalLevel_2 %>" GroupSubtitle="<%$ Resources: NCMWebContent, WEBDATA_VK_1028 %>" OnAddUserClick="AddUser_Click" OnRemoveUserClick="RemoveUser_Click" />
                                                    </div>
                                                    <div style="padding-top:20px;">
                                                        <asp:CustomValidator ID="customValidator" runat="server" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_1029 %>" OnServerValidate="ValidateNCMApprovalUsers" Display="Dynamic"></asp:CustomValidator>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:right;padding-top:20px;">
                                                <span class="WizardStepPreviousButtonStyle"><orion:LocalizableButton runat="server" ID="btnBackToStep3" LocalizedText="Back" 
                                                DisplayType="Secondary" CommandName="MovePrevious" CausesValidation="false"/></span>
                                                <span class="WizardStartNextButtonStyle"><orion:LocalizableButton runat="server" ID="btnFinish" 
                                                DisplayType="Primary" CommandName="MoveComplete" CausesValidation="true"/></span>
                                                <span class="WizardCancelButtonStyle"><orion:LocalizableButton runat="server" ID="bntCancelFromStep4" LocalizedText="Finish" 
                                                DisplayType="Secondary" CommandName="Cancel" CausesValidation="true"/></span>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:WizardStep>
                            </WizardSteps>
                        </asp:Wizard>
                    </div>
                </div>
            </div>
       </ContentTemplate>
    </asp:UpdatePanel>
    </asp:Panel>
    <div style="padding:10px;" id="ErrorContainer" runat="server"/>
</asp:Content>

