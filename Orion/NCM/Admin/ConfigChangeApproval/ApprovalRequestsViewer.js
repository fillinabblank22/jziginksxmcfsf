﻿Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.QuickTips.init();

SW.NCM.ApprovalRequestsViewer = function () {

    function gridResize() {
        if (grid != null) {
            grid.setWidth(1);
            grid.setWidth($('#gridCell').width());

            grid.setHeight(getGridHeight());
        }
    }

    function getGridHeight() {
        var top = $('#gridCell').offset().top;
        return $(window).height() - top - $('#footer').height() - 100;
    }

    var currentSearchTerm = null;
    var refreshObjects = function (search, callback, requestStatus) {
        grid.store.removeAll();
        grid.store.baseParams['limit'] = grid.getBottomToolbar().pageSize;
        grid.store.proxy.conn.jsonData = { search: search, rqsStatus: requestStatus };
        currentSearchTerm = search;
        grid.store.load({ callback: callback });
    };

    var getColumnNumber = function (columnDataIndex) {
        for (var i = 0; i < grid.getColumnModel().config.length; i++) {
            var col = grid.getColumnModel().config[i];
            if (col.dataIndex == columnDataIndex)
                return i;
        }
    };

    var viewEditRequest = function (item) {
        window.location = String.format("/Orion/NCM/Admin/ConfigChangeApproval/ViewApprovalRequest.aspx?Id={0}", item.data.a);
    };

    var newGuid = function () {
        try {
            return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, function (c) {
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            });
        } catch (e) {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        }
    };

    var getSelectedRequestIds = function (items) {
        var ids = [];

        Ext.each(items, function (item) {
            ids.push(item.data.a);
        });

        return ids;
    };

    var approveSelectedRequest = function (item) {
        var toApprove = item.data.a;
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_01;E=js}');

        approveRequest(toApprove, function (result) {
            waitMsg.hide();
            ErrorHandler(result);
            ReloadGridStore();
        });
    };

    var declineSelectedRequests = function (item) {
        var action = 'decline';
        window.location = String.format("/Orion/NCM/Admin/ConfigChangeApproval/ViewApprovalRequest.aspx?Id={0}&Action={1}", item.data.a, action);
    };

    var deleteSelectedRequests = function (items) {
        var toDelete = getSelectedRequestIds(items);
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_03;E=js}');

        deleteRequests(toDelete, function (result) {
            waitMsg.hide();
            ErrorHandler(result);
            ReloadGridStore();
        });
    };

    var approveRequest = function (id, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/ApprovalRequests.asmx",
                             "ApproveRequest", { requestId: id },
                             function (result) {
                                 onSuccess(result);
                             });
    };

    var deleteRequests = function (ids, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/ApprovalRequests.asmx",
                             "DeleteRequests", { requestIds: ids },
                             function (result) {
                                 onSuccess(result);
                             });
    };

    var updateToolbarButtons = function () {
        var selCount = grid.getSelectionModel().getCount();
        var map = grid.getTopToolbar().items.map;

        var needsOnlyOneSelected = (selCount != 1);
        var needsAtLeastOneSelected = (selCount === 0);

        var pendingRequestSelected = false;
        var needConfirmationRequestSelected = false;
        if (selCount == 1) {
            pendingRequestSelected = grid.getSelectionModel().getSelected().data.b == 0 && ((SW.NCM.CurrentUserApprovalRole == 2) || (SW.NCM.CurrentUserApprovalRole == 3) || (SW.NCM.CurrentUserApprovalRole == 5));
            needConfirmationRequestSelected = grid.getSelectionModel().getSelected().data.b == 6 && ((SW.NCM.CurrentUserApprovalRole == 4) || (SW.NCM.CurrentUserApprovalRole == 6));
        }

        map.ViewEdit.setDisabled(needsOnlyOneSelected);
        map.Approve.setDisabled(SW.NCM.IsDemoMode ? true : !pendingRequestSelected && !needConfirmationRequestSelected);
        map.Decline.setDisabled(SW.NCM.IsDemoMode ? true : !pendingRequestSelected && !needConfirmationRequestSelected);
        map.Delete.setDisabled(needsAtLeastOneSelected);

        var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');
        if (grid.getStore().getCount() > 0 && selCount == grid.getStore().getCount()) {
            hd.addClass('x-grid3-hd-checker-on');
        } else {
            hd.removeClass('x-grid3-hd-checker-on');
        }
    };

    function ReloadGridStore() {
        var currentPageRecordCount = grid.store.getCount();
        var selectedRecordCount = grid.getSelectionModel().getCount();
        if (currentPageRecordCount > selectedRecordCount) {
            grid.store.reload();
        }
        else {
            grid.store.load();
        }
    }

    function ErrorHandler(result) {
        if (result != null && result.Error) {
            Ext.Msg.show({
                title: result.Source,
                msg: result.Msg,
                minWidth: 500,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        }
    }

    function renderDateTime(value, meta, record) {
        if (value == null)
            return;
        var date = eval(value.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
        return String.format('<span class="approvalRequest" value="{0}">{1}</span>', record.data.a, Ext.util.Format.date(date, ExtDaTimeLocalePattern.DateFull));
    }

    function renderHistoryCell(value, meta, record) {

        if (record.data.b == 4) {
            return String.format('<img class="approvalHistory" jobId="{0}" src="/Orion/NCM/Resources/images/InventoryIcons/icon_inventory_report.gif" />', record.data.j);
        }
        else
            return '';
    }
   
    function renderTargetNodes(value, meta, record) {
        var n = parseInt(value, 10);
        if (n == 1) {
            return String.format('<span class="approvalRequest" value="{0}">{1}</span>', record.data.a, Ext.util.Format.htmlEncode(String.format("@{R=NCM.Strings;K=ConfigChangeApproval_CellValue_Node;E=js}", value)));
        }
        else {
            return String.format('<span class="approvalRequest" value="{0}">{1}</span>', record.data.a, Ext.util.Format.htmlEncode(String.format("@{R=NCM.Strings;K=ConfigChangeApproval_CellValue_Nodes;E=js}", value)));
        }
    }

    function renderStatus(value, meta, record) {

        var statusStr = '';
        var icon = '';
        switch (value) {
            case 0: statusStr = '@{R=NCM.Strings;K=REQUEST_STATUS_PENDING;E=js}'; icon = '/Orion/NCM/Resources/images/ConfigChangeApproval/execute_icon_pending.png'; break;
            case 1: statusStr = '@{R=NCM.Strings;K=REQUEST_STATUS_DECLINED;E=js}'; icon = '/Orion/NCM/Resources/images/ConfigChangeApproval/execute_icon_declined.png'; break;
            case 2: statusStr = '@{R=NCM.Strings;K=REQUEST_STATUS_WAITING_FOR_EXECUTION;E=js}'; icon = '/Orion/NCM/Resources/images/ConfigChangeApproval/execute_icon_waiting_for_execution.png'; break;
            case 3: statusStr = '@{R=NCM.Strings;K=REQUEST_STATUS_SCHEDULED;E=js}'; icon = '/Orion/NCM/Resources/images/ConfigChangeApproval/execute_icon_scheduled.png'; break;
            case 4: statusStr = '@{R=NCM.Strings;K=REQUEST_STATUS_COMPLETE;E=js}'; icon = '/Orion/NCM/Resources/images/ConfigChangeApproval/execute_icon_complete.png'; break;
            case 5: statusStr = '@{R=NCM.Strings;K=REQUEST_STATUS_EXECUTING;E=js}'; icon = '/Orion/NCM/Resources/images/ConfigChangeApproval/execute_icon_executing.png'; break;
            case 6: statusStr = '@{R=NCM.Strings;K=REQUEST_STATUS_NEEDCONFIRMATION;E=js}'; icon = '/Orion/NCM/Resources/images/ConfigChangeApproval/execute_icon_pending.png'; break;
        }
        return String.format('<span class="approvalRequest" value="{1}"><img src="{0}" class="approvalRequest" value="{1}" style="vertical-align: middle;" />&nbsp;{2}</span>', icon, record.data.a, statusStr);
    }

    function renderStringValue(value, meta, record) {
        return String.format('<span class="approvalRequest" value="{0}">{1}</span>', record.data.a, Ext.util.Format.htmlEncode(value));
    }

    var doClean = function() {
        var map = grid.getTopToolbar().items.map;
        map.Clean.setVisible(false);
        map.txtSearch.setValue('');
        refreshObjects("", null, map.StatusFilter.getValue());
    };

    var doSearch = function() {
        var map = grid.getTopToolbar().items.map;
        var search = map.txtSearch.getValue();
        var searchBtn = map.Search;
        var statusFilterValue = map.StatusFilter.getValue();

        if ($.trim(search).length == 0) return;

        refreshObjects(search, null, statusFilterValue);
        map.Clean.setVisible(true);
    };

    function showComments(obj) {
        var requestId = obj.attr("value");
        var body = $("#requestCommentsBody").text('@{R=NCM.Strings;K=WEBJS_VK_04;E=js}');

        ORION.callWebService("/Orion/NCM/Services/ApprovalRequests.asmx",
                             "GetComments", { requestId: requestId },
                             function (result) {
                                 var comments = $.trim(result.comments);
                                 comments = comments.length != 0 ? comments : '@{R=NCM.Strings;K=WEBJS_VK_06;E=js}';
                                 comments = Ext.util.Format.htmlEncode(comments);
                                 body.empty().html('<div style="font-weight: bold;">' + '@{R=NCM.Strings;K=WEBJS_VK_05;E=js}' + '</div>' + comments);
                                 return ShowCommentsDialog(result.title);
                             });
    }

    function showHistory(obj) {
        var jobId = obj.attr("jobId");
        var title = '@{R=NCM.Strings;K=ConfigChangeApproval_DialogTitle_ApprovalRequestHistory;E=js}';
        var jobLogDiv = $("#jobLogTextArea");
        jobLogDiv.empty();
        ORION.callWebService("/Orion/NCM/Services/JobManagement.asmx",
                             "GetJobLog", { jobID: jobId },
                             function (result) {
                                 var jobLogTextArea = new Ext.form.TextArea({
                                     id: 'jobLogID',
                                     renderTo: 'jobLogTextArea',
                                     style: 'border: none;',
                                     width: 580,
                                     height: 320,
                                     readOnly: true,
                                     emptyText: '@{R=NCM.Strings;K=WEBJS_VK_187;E=js}',
                                     value: result
                                 });
                                 return ShowJobLogDialog(title);
                             });
    }

    var showCommentsDialog;
    ShowCommentsDialog = function(title) {

        if (!showCommentsDialog) {
            showCommentsDialog = new Ext.Window({
                applyTo: 'requestCommentsDialog',
                layout: 'fit',
                width: 500,
                height: 300,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                modal: true,
                contentEl: 'requestCommentsBody',
                buttons: [
                    {
                        text: '@{R=NCM.Strings;K=WEBJS_VK_07;E=js}',
                        handler: function() { showCommentsDialog.hide(); }
                    }
                ]
            });
        }

        showCommentsDialog.setTitle(Ext.util.Format.htmlEncode(title));

        showCommentsDialog.alignTo(document.body, "c-c");
        showCommentsDialog.show();

        return false;
    };

    var showJobLogDialog;
    ShowJobLogDialog = function(title) {
        if (!showJobLogDialog) {
            showJobLogDialog = new Ext.Window({
                applyTo: 'jobLogDialog',
                layout: 'fit',
                width: 600,
                height: 400,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                modal: true,
                contentEl: 'jobLogBody',
                buttonAlign: 'left',
                buttons: [
                    {
                        text: '@{R=NCM.Strings;K=WEBJS_VK_07;E=js}',
                        handler: function() {
                            showJobLogDialog.hide();
                        }
                    }
                ]
            });
        }

        showJobLogDialog.setTitle(Ext.util.Format.htmlEncode(title));

        showJobLogDialog.alignTo(document.body, "c-c");
        showJobLogDialog.show();

        return false;
    };

    gridColumnChangedHandler = function(component, state) {
        if (component == null)
            return;

        var gridCM = component.getColumnModel();
        var settingName = 'Columns';

        if (gridCM != null && settingName != '') {

            var jsonvalue = SW.NCM.GridColumnsToJson(gridCM);
            ORION.Prefs.save(settingName, jsonvalue);
        }
    };

    ORION.prefix = "NCM_ApprovalRequests_";

    var selectorModel;
    var dataStore;
    var grid;


    return {
        init: function () {

            if ($("[id$='_GridContainer']").length == 0)
                return false;

            $("#Grid").click(function (e) {

                var obj = $(e.target);

                if (obj.hasClass("ncm_searchterm"))
                    obj = obj.parent();

                if (obj.hasClass('approvalRequest')) {
                    showComments(obj);
                    return false;
                }

                if (obj.hasClass('approvalHistory')) {
                    showHistory(obj);
                    return false;
                }

            });

            if (SW.NCM.IsApproverUser || SW.NCM.IsDemoMode) {
                $("#InfoContainer").html('@{R=NCM.Strings;K=WEBJS_VK_08;E=js}');
            }
            else {
                $("#InfoContainer").html('@{R=NCM.Strings;K=WEBJS_VK_09;E=js}');
            }

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", updateToolbarButtons);

            dataStore = new ORION.WebServiceStore(
                                "/Orion/NCM/Services/ApprovalRequests.asmx/GetApprovalRequestsPaged",
                                [
                                    { name: 'a', mapping: 0 }, //ID
                                    { name: 'b', mapping: 1 }, //RequestStatus
                                    { name: 'c', mapping: 2 }, //RequestType
                                    { name: 'd', mapping: 3 }, //TargetNodes
                                    { name: 'e', mapping: 4 }, //UserName
                                    { name: 'f', mapping: 5 }, //StatusChangeTime
                                    { name: 'g', mapping: 6 }, //DateTime
                                    { name: 'h', mapping: 7 }, //ApprovedBy
                                    { name: 'i', mapping: 8 }, //Comments
                                    { name: 'j', mapping: 9 } //History
                                ],
                                "g");

            var comboPS = new Ext.form.ComboBox({
                name: 'perpage',
                width: 50,
                store: new Ext.data.SimpleStore({
                    fields: ['id'],
                    data: [
                                  ['25'],
                                  ['50'],
                                  ['75'],
                                  ['100']
                    ]
                }),
                mode: 'local',
                value: '25',
                listWidth: 50,
                triggerAction: 'all',
                displayField: 'id',
                valueField: 'id',
                editable: false,
                forceSelection: true
            });

            var pagingToolbar = new Ext.PagingToolbar({
                store: dataStore,
                pageSize: 25,
                displayInfo: true,
                displayMsg: '@{R=NCM.Strings;K=WEBJS_VK_10;E=js}',
                emptyMsg: '@{R=NCM.Strings;K=WEBJS_VK_11;E=js}',
                items: ['-', '@{R=NCM.Strings;K=WEBJS_VK_12;E=js}', comboPS]
            });

            comboPS.on('select', function (combo, record) {
                var psize = parseInt(record.get('id'), 10);
                grid.store.baseParams['limit'] = psize;
                pagingToolbar.pageSize = psize;
                pagingToolbar.cursor = 0;
                pagingToolbar.doLoad(pagingToolbar.cursor);
            }, this);


            grid = new Ext.grid.GridPanel({
                id: 'gridControl',
                store: dataStore,

                columns: [selectorModel, {
                    header: 'ID',
                    width: 5,
                    hidden: true,
                    hideable: false,
                    sortable: true,
                    dataIndex: 'a'
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_172;E=js}',
                    width: 200,
                    sortable: true,
                    dataIndex: 'b',
                    renderer: renderStatus
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_14;E=js}',
                    width: 150,
                    sortable: true,
                    dataIndex: 'c',
                    renderer: renderStringValue
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_15;E=js}',
                    width: 150,
                    sortable: false,
                    dataIndex: 'd',
                    renderer: renderTargetNodes
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VM_77;E=js}',
                    width: 150,
                    sortable: true,
                    dataIndex: 'e',
                    renderer: renderStringValue
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VM_78;E=js}', //request time
                    width: 150,
                    sortable: true,
                    dataIndex: 'f',
                    renderer: renderDateTime
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_16;E=js}', //request time
                    width: 250,
                    sortable: true,
                    dataIndex: 'g',
                    renderer: renderDateTime
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VM_79;E=js}',
                    width: 150,
                    sortable: true,
                    dataIndex: 'h',
                    renderer: renderStringValue
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VM_80;E=js}',
                    width: 200,
                    sortable: false,
                    dataIndex: 'i',
                    renderer: renderStringValue
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_173;E=js}',
                    width: 100,
                    sortable: false,
                    dataIndex: 'j',
                    renderer: renderHistoryCell
                }

                ],

                sm: selectorModel,

                layout: 'fit',
                autoScroll: 'true',
                loadMask: true,

                width: 750,
                height: 500,
                stripeRows: true,

                tbar: [{
                    text: '@{R=NCM.Strings;K=WEBJS_VM_76;E=js}',
                    xtype: 'label'

                }, {
                    id: 'StatusFilter',
                    xtype: 'combo',
                    editable: false,
                    triggerAction: 'all',
                    store: [['-1', '@{R=NCM.Strings;K=WEBJS_VM_75;E=js}'],
                            ['0', '@{R=NCM.Strings;K=REQUEST_STATUS_PENDING;E=js}'],
                            ['6', '@{R=NCM.Strings;K=REQUEST_STATUS_NEEDCONFIRMATION;E=js}'],
                            ['1', '@{R=NCM.Strings;K=REQUEST_STATUS_DECLINED;E=js}'],
                            ['2', '@{R=NCM.Strings;K=REQUEST_STATUS_WAITING_FOR_EXECUTION;E=js}'],
                            ['3', '@{R=NCM.Strings;K=REQUEST_STATUS_SCHEDULED;E=js}'],
                            ['4', '@{R=NCM.Strings;K=REQUEST_STATUS_COMPLETE;E=js}'],
                            ['5', '@{R=NCM.Strings;K=REQUEST_STATUS_EXECUTING;E=js}']],
                    value: '-1',
                    selectOnFocus: true,
                    listeners: {
                        select: function (that, record) {
                            refreshObjects('', null, that.getValue());
                        }
                    }
                }, '-', {
                    id: 'ViewEdit',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_17;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_18;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigChangeApproval/edit_icon.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { viewEditRequest(grid.getSelectionModel().getSelected()); }
                }, SW.NCM.IsApproverUser || SW.NCM.IsDemoMode ? '-' : '', {
                    id: 'Approve',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_19;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_20;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigChangeApproval/approve_icon.gif',
                    cls: 'x-btn-text-icon',
                    hidden: !SW.NCM.IsApproverUser && !SW.NCM.IsDemoMode,
                    handler: function () { approveSelectedRequest(grid.getSelectionModel().getSelected()); }
                }, SW.NCM.IsApproverUser || SW.NCM.IsDemoMode ? '-' : '', {
                    id: 'Decline',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_21;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_22;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigChangeApproval/decline_icon.gif',
                    cls: 'x-btn-text-icon',
                    hidden: !SW.NCM.IsApproverUser && !SW.NCM.IsDemoMode,
                    handler: function () { declineSelectedRequests(grid.getSelectionModel().getSelected()); }
                }, !SW.NCM.IsDemoMode ? '-' : '', {
                    id: 'Delete',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_25;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_26;E=js}',
                    icon: '/Orion/NCM/Resources/images/icon_delete.png',
                    cls: 'x-btn-text-icon',
                    hidden: SW.NCM.IsDemoMode,
                    handler: function () {
                        Ext.Msg.confirm(
							            '@{R=NCM.Strings;K=WEBJS_VK_27;E=js}',
                                        '@{R=NCM.Strings;K=WEBJS_VK_28;E=js}',
                                        function (btn, text) {
                                            if (btn == 'yes') {
                                                deleteSelectedRequests(grid.getSelectionModel().getSelections());
                                            }
                                        });
                    }
                }, '->', {
                    id: 'txtSearch',
                    xtype: 'textfield',
                    emptyText: '@{R=NCM.Strings;K=WEBJS_VK_29;E=js}',
                    width: 200,
                    enableKeyEvents: true,
                    listeners: {
                        keypress: function (obj, evnt) {
                            if (evnt.keyCode == 13) {
                                doSearch();
                            }
                        }
                    }
                }, {
                    id: 'Clean',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_30;E=js}',
                    iconCls: 'ncm_clear-btn',
                    cls: 'x-btn-icon',
                    hidden: true,
                    handler: function () { doClean(); }
                }, {
                    id: 'Search',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_31;E=js}',
                    iconCls: 'ncm_search-btn',
                    cls: 'x-btn-icon',
                    handler: function () { doSearch(); }
                }],

                bbar: pagingToolbar

            });

            SW.NCM.PersonalizeGrid(grid, columnModel);
            grid.on("statesave", gridColumnChangedHandler);

            grid.render('Grid');

            updateToolbarButtons();

            gridResize(pagingToolbar.pageSize);

            $("form").submit(function () { return false; });

            refreshObjects("", null, "-1");

            $(window).resize(function () {
                gridResize(pagingToolbar.pageSize);
            });

            grid.getView().on("refresh", function () {
                var search = currentSearchTerm;
                if ($.trim(search).length == 0) return;

                var columnsToHighlight = SW.NCM.ColumnIndexByName(grid.getColumnModel(), ['@{R=NCM.Strings;K=WEBJS_VK_13;E=js}', '@{R=NCM.Strings;K=WEBJS_VK_14;E=js}', '@{R=NCM.Strings;K=WEBJS_VM_79;E=js}', '@{R=NCM.Strings;K=WEBJS_VM_80;E=js}']);
                Ext.each(columnsToHighlight, function (columnNumber) {
                    SW.NCM.highlightSearchText("#Grid .x-grid3-body .x-grid3-td-" + columnNumber, search);
                });
            });
        }
    };

}();

Ext.onReady(SW.NCM.ApprovalRequestsViewer.init, SW.NCM.ApprovalRequestsViewer);

SW.NCM.callWebMethod = function (url, data, success, failure) {
    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: true,
        success: function (result) {
            if (success)
                success(result);
        },
        error: function (response) {
            if (failure)
                failure(response);
            else {
                alert(response);
            }
        }
    });
};
