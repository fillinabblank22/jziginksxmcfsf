﻿using System;
using System.Web.UI.WebControls;

using SolarWinds.NCM.Contracts.InformationService;

public partial class Orion_NCM_Admin_ConfigChangeApproval_Controls_ApprovalMode : System.Web.UI.UserControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        DataBindApprovalMode();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public eApprovalMode Mode
    {
        get
        {
            eApprovalMode mode;
            if (Enum.TryParse<eApprovalMode>(rbApprovalMode.SelectedValue, out mode))
            {
                if (mode == eApprovalMode.ApprovalDisabled)
                    mode = eApprovalMode.OneLevel;

                return mode;
            }
            else
                return eApprovalMode.OneLevel;
        }
        set
        {
            eApprovalMode mode = value;
            if (mode == eApprovalMode.ApprovalDisabled)
                mode = eApprovalMode.OneLevel;

            rbApprovalMode.SelectedValue = mode.ToString(@"D");
        }
    }

    private void DataBindApprovalMode()
    {
        rbApprovalMode.Items.Clear();
        rbApprovalMode.Items.Add(new ListItem(
            $@"<b>{Resources.NCMWebContent.WEBDATA_VK_1017}</b><div style='padding-left:20px;padding-bottom:10px;'>{Resources.NCMWebContent.WEBDATA_VK_1020}</div>", eApprovalMode.OneLevel.ToString(@"D")));
        rbApprovalMode.Items.Add(new ListItem(
            $@"<b>{Resources.NCMWebContent.WEBDATA_VK_1018}</b><div style='padding-left:20px;padding-bottom:10px;'>{Resources.NCMWebContent.WEBDATA_VK_1021}</div>", eApprovalMode.TwoLevelWebUploader.ToString(@"D")));
        rbApprovalMode.Items.Add(new ListItem(
            $@"<b>{Resources.NCMWebContent.WEBDATA_VK_1019}</b><div style='padding-left:20px;'>{Resources.NCMWebContent.WEBDATA_VK_1022}</div>", eApprovalMode.TwoLevelAll.ToString(@"D")));
    }
}