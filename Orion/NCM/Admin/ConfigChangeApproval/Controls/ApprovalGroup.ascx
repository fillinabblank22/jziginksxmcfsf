﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApprovalGroup.ascx.cs" Inherits="Orion_NCM_Admin_ConfigChangeApproval_Controls_ApprovalGroup" %>

<style type="text/css">
    .ApprovalGroupGrid 
    {
        border-bottom:solid 1px #dcdbd7;
        border-top:solid 1px #dcdbd7;
    }
    
    .ApprovalGroupGrid tbody tr td
    {
        border:none;
    }
    
    .ButtonRemove
    {
	    font-size:10pt;
	    font-family: Arial,Verdana,Helvetica,sans-serif;
	    display: inline-block; 
	    position: relative;
	    padding: 7px 5px 7px 26px; 
	    color: #000;
    }
        
    .ButtonRemove-Icon 
    {
	    position: absolute;
	    left: 5px; 
	    top: 7px; 
	    height: 16px; 
	    width: 16px; 
	    background: url(/Orion/NCM/Resources/images/icon_delete.png) top left no-repeat; 
    }
</style>

<div>
    <asp:HiddenField ID="hfGroupID" runat="server" />

    <div style="font-weight:bold;"><%=GroupName %></div>
    <div><%=GroupSubtitle %></div>

    <div style="padding-top:20px;">
        <asp:UpdatePanel ID="updatePanel" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <asp:GridView ID="gridView" runat="server" CssClass="ApprovalGroupGrid" CellPadding="0" ColSpacing="0" BackColor="white" Width="600px" AutoGenerateColumns="False" ShowHeader="False" OnRowDeleting="gridView_RowDeleting">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Label ID="lblUser" runat="server" Text='<%# $@"{DataBinder.Eval(Container.DataItem, @"UserId")} ({DataBinder.Eval(Container.DataItem, @"EmailAddress")})" %>' userId='<%# DataBinder.Eval(Container.DataItem, @"UserId") %>' Width="500px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="btnRemove" runat="server" CommandName="Delete" CausesValidation="false" CssClass="ButtonRemove">
                                    <span class="ButtonRemove-Icon"></span><%=Resources.NCMWebContent.WEBDATA_VK_567 %>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <div style="padding-top:20px;">
                    <%= Resources.NCMWebContent.ApprovalGroup_SelectUser %>:
                    <asp:DropDownList ID="ddlOrionUsers" runat="server" Width="200px" BorderColor="#89A6C0" BorderWidth="1px"></asp:DropDownList>
                    <span style="padding-left:20px;"><%= Resources.NCMWebContent.ApprovalGroup_UserEmail %>:</span>
                    <asp:TextBox ID="txtEmailTo" runat="server" Width="200px" BorderColor="#89A6C0" BorderWidth="1px"></asp:TextBox>
                    <span id="rfvEmailTo" runat="server" style="color:red;">*</span>
                    <orion:LocalizableButton runat="server" ID="btnAddUser" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, ApprovalGroup_AddUser %>" OnClick="btnAddUser_Click" DisplayType="Small" CausesValidation="false"/>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>