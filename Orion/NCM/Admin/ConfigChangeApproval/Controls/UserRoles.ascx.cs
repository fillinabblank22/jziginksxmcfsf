﻿using System;
using SolarWinds.NCM.Common.Help;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_Admin_ConfigChangeApproval_Controls_UserRoles : System.Web.UI.UserControl
{
    protected string LearnMoreLink
    {
        get
        {
            var helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
            return helpHelperWrap.GetHelpUrl("OrionNCMPHLearnMoreRoles");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}