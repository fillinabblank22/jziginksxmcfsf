﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

using SolarWinds.NCM.Contracts.InformationService;

public partial class Orion_NCM_Admin_ConfigChangeApproval_Controls_ApprovalGroup : System.Web.UI.UserControl
{
    public delegate void dlgAddUserClick(NCMApprovalUser user);
    public event dlgAddUserClick AddUserClick;

    public delegate void dlgRemoveUserClick(string userId);
    public event dlgRemoveUserClick RemoveUserClick;

    [PersistenceMode(PersistenceMode.Attribute)]
    public string GroupName { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string GroupSubtitle { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        rfvEmailTo.Visible = false;
    }

    protected void gridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        GridViewRow row = gridView.Rows[e.RowIndex];
        if (row != null)
        {
            Label lbl = row.FindControl(@"lblUser") as Label;
            if (lbl != null)
            {
                if (RemoveUserClick != null)
                    RemoveUserClick.Invoke(lbl.Attributes[@"userId"]);
            }
        }
    }

    protected void btnAddUser_Click(object sender, EventArgs e)
    {
        if (AddUserClick != null)
        {
            if (!string.IsNullOrEmpty(txtEmailTo.Text))
            {
                rfvEmailTo.Visible = false;

                NCMApprovalUser approvalUser = new NCMApprovalUser();
                approvalUser.UserId = ddlOrionUsers.SelectedValue;
                approvalUser.GroupID = (eApprovalGroup)Enum.Parse(typeof(eApprovalGroup), hfGroupID.Value);
                approvalUser.GroupName = GroupName;
                approvalUser.EmailAddress = txtEmailTo.Text;

                AddUserClick.Invoke(approvalUser);

                txtEmailTo.Text = string.Empty;
            }
            else 
            {
                rfvEmailTo.Visible = true;
            }
        }
    }

    public void DataBind(eApprovalGroup groupId, List<string> orionUsers, List<NCMApprovalUser> approvalUsers)
    {
        hfGroupID.Value = groupId.ToString(@"D");

        ddlOrionUsers.DataSource = orionUsers;
        ddlOrionUsers.DataBind();

        gridView.DataSource = approvalUsers.Where(user => user.GroupID == groupId);
        gridView.DataBind();

        if (orionUsers.Count == 0)
        {
            btnAddUser.Enabled = false;

            ddlOrionUsers.Style.Add(@"color", @"gray");
            ddlOrionUsers.Style.Add(@"font-style", @"italic");

            ListItem item = new ListItem(Resources.NCMWebContent.ConfigChangeApproval_NoUsers_WatermarkText);
            item.Attributes.Add(@"style", @"color:gray;font-style:italic;");
            ddlOrionUsers.Items.Add(item);
        }
        else
        {
            btnAddUser.Enabled = true;

            ddlOrionUsers.Style.Remove(@"color");
            ddlOrionUsers.Style.Remove(@"font-style");
        }

        updatePanel.Update();
    }
}