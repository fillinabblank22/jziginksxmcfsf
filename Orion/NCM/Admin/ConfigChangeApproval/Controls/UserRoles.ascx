﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserRoles.ascx.cs" Inherits="Orion_NCM_Admin_ConfigChangeApproval_Controls_UserRoles" %>

<div>
    <table width="100%">
        <tr>
            <td style="padding: 0px 10px 0px 10px;">
                <img alt="" src="/Orion/NCM/Resources/images/check_32x32.gif" />
            </td>
            <td>
                <%= string.Format(Resources.NCMWebContent.WEBDATA_VK_59, @"<b>", @"</b>") %>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding:10px 0px 10px 0px;">
                <div style="padding:10px 0px 10px 0px;background-color:#FFFDCC;">
                    <img style="padding:0px 5px 0px 5px;" src="/Orion/NCM/Resources/images/lightbulb_tip_16x16.gif"/><b><%= Resources.NCMWebContent.WEBDATA_VK_60 %></b>&nbsp;<span style="font-size:8pt;">&#0187;&nbsp;<a style="color:#336699;" href="<%=LearnMoreLink%>" target="_blank"><%= Resources.NCMWebContent.WEBDATA_VK_61 %></a></span>
                </div>
            </td>
        </tr>
    </table>
</div>