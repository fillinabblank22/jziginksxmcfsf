using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Linq;
using System.Web;
using Resources;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Logging;
using SolarWinds.NCM.Common.Help;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.PolicyReportsManagement.Controls;
using SolarWinds.NCM.Contracts.InformationService;

using StringRegistrar = SolarWinds.Orion.Core.Common.i18n.Registrar.ResourceManagerRegistrar;

public partial class Orion_NCM_Admin_ViewApprovalRequest : System.Web.UI.Page
{
    #region Private Members

    private const string SETTING_PREFIX = "ApprovalScriptViewer";

    private IIsWrapper isLayer;
    private SelectNodes TargetNodes = new SelectNodes();
    private Log log = new Log();

    #endregion

    #region Protected Members

    protected string HelpLink
    {
        get
        {
            var helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
            return helpHelperWrap.GetHelpUrl("OrionNCMPHWhyNotSeeAllNodes");
        }
    }

    protected bool TargetNodesReadOnly
    {
        get { return Convert.ToBoolean(ViewState["TargetNodesReadOnly"]); }
        set { ViewState["TargetNodesReadOnly"] = value; }
    }

    protected string PageGuid
    {
        get
        {
            if (!string.IsNullOrEmpty(Page.Request.QueryString[@"Id"]))
            {
                return Page.Request.QueryString[@"Id"];
            }
            else if (!string.IsNullOrEmpty(Page.Request.QueryString[@"GuidID"]))
            {
                return Page.Request.QueryString[@"GuidID"];
            }
            else
            {
                return Guid.Empty.ToString();
            }
        }
    }

    protected NCMApprovalTicket NCMApprovalTicket
    {
        get
        {
            try
            {
                NCMApprovalTicket ncmApprovalTicket;
                var requestId = Page.Request.QueryString[@"Id"];
                if (!string.IsNullOrEmpty(requestId))
                {
                    using (var proxy = isLayer.GetProxy())
                    {
                        if (Page.Session[PageGuid] == null)
                        {
                            ncmApprovalTicket = proxy.Cirrus.ConfigChangeApproval.GetRequest(new Guid(requestId));
                            Page.Session[PageGuid] = ncmApprovalTicket;
                        }
                        else
                        {
                            ncmApprovalTicket = (NCMApprovalTicket)Page.Session[PageGuid];
                        }
                    }
                }
                else
                {
                    ncmApprovalTicket = (NCMApprovalTicket)Page.Session[PageGuid];
                    if (ncmApprovalTicket == null)
                    {
                        Page.Response.Redirect("~/Orion/NCM/Admin/ConfigChangeApproval/ApprovalRequests.aspx");
                    }
                }
                return ncmApprovalTicket;
            }
            catch { throw; }
        }
    }

    protected bool IsOwner => NCMApprovalTicket.SubmittedBy.Equals(HttpContext.Current.Profile.UserName, StringComparison.InvariantCultureIgnoreCase);

    protected bool IsNewApprovalRequest => !string.IsNullOrEmpty(Page.Request.QueryString[@"GuidID"]);

    #endregion

    #region Event Handlers

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();

        ValidatePageAccess();

        rbRunImmediately.Text = StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"EXECUTION_TYPE_IMMEDIATE");
        rbSpecifiedTime.Text = StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"EXECUTION_TYPE_SPECIFIED_TIME");
        rbReturnToRequestor.Text = StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"EXECUTION_TYPE_RETURN_TO_REQUESTOR");

        ApprovalScriptViewer.RequestId = Page.Request.QueryString[@"Id"];
        ApprovalScriptViewer.PageGuid = PageGuid;
        var settings = new ResourceSettings();
        settings.Prefix = SETTING_PREFIX;
        NPMDefaultSettingsHelper.SetUserSettings("ApprovalScriptViewerGroupBy", "Vendor");
        ApprovalScriptViewer.ResourceSettings = settings;
        ApprovalScriptViewer.ApprovalScriptViewerHeight = 300;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = NCMWebContent.WEBDATA_VK_37;

        try
        {
            if (!Page.IsPostBack)
            {
                LoadStatusList(NCMApprovalTicket.RequestStatus);

                if (!string.IsNullOrEmpty(Page.Request.QueryString[@"Id"]))
                    ClearSessionState();

                Status.Text = ConfigChangeApprovalHelper.LookupLocalizedRequestStatus(NCMApprovalTicket.RequestStatus);

                if (!string.IsNullOrEmpty(Page.Request.QueryString[@"Action"]) && string.Equals(Page.Request.QueryString[@"Action"].ToLowerInvariant(), @"decline"))
                    StatusList.SelectedValue = eApprovalStatus.Declined.ToString(@"D");
                else
                    StatusList.SelectedValue = NCMApprovalTicket.RequestStatus.ToString(@"D");

                RequestType.Text = ConfigChangeApprovalHelper.LookupLocalizedRequestType(NCMApprovalTicket.RequestType);

                switch (NCMApprovalTicket.RequestType)
                {
                    case NCMApprovalTicket.TYPE_EXECUTE_SCRIPTS:
                        if (NCMApprovalTicket.UseScriptPerNode)
                        {
                            ConfigTypeDIV.Visible = false;
                            ScriptDIV.Visible = false;
                            RebootDIV.Visible = false;
                            ApprovalScriptViewerDIV.Visible = true;
                        }
                        else
                        {
                            ConfigTypeDIV.Visible = false;
                            ScriptDIV.Visible = true;
                            RebootDIV.Visible = false;
                            ApprovalScriptViewerDIV.Visible = false;
                            Script.Text = NCMApprovalTicket.Script;
                        }
                        break;
                    case NCMApprovalTicket.TYPE_UPLOAD_CONFIG:
                        if (NCMApprovalTicket.UseScriptPerNode)
                        {
                            ConfigTypeDIV.Visible = false;
                            ScriptDIV.Visible = false;
                            RebootDIV.Visible = false;
                            ApprovalScriptViewerDIV.Visible = true;
                        }
                        else
                        {
                            ConfigTypeDIV.Visible = true;
                            ScriptDIV.Visible = true;
                            RebootDIV.Visible = true;
                            ApprovalScriptViewerDIV.Visible = false;
                            ConfigType.Text = NCMApprovalTicket.ConfigType;
                            Script.Text = CommonHelper.IsBinaryConfigByContent(NCMApprovalTicket.Script) ? NCMWebContent.WEBDATA_AP_3 : NCMApprovalTicket.Script;
                            Reboot.Checked = NCMApprovalTicket.Reboot;
                        }
                        break;
                    case NCMApprovalTicket.TYPE_CONFIG_CHANGE_TEMPLATE:
                    case NCMApprovalTicket.TYPE_MANAGE_EW:
                        ConfigTypeDIV.Visible = false;
                        ScriptDIV.Visible = false;
                        RebootDIV.Visible = false;
                        ApprovalScriptViewerDIV.Visible = true;
                        break;
                }

                SetExecutionMethod(NCMApprovalTicket.ExecutionType);
                ExecutionMethod.Text = ConfigChangeApprovalHelper.LookupLocalizedExecutionMethod(NCMApprovalTicket.ExecutionType, NCMApprovalTicket.RunAt);
                RunAt.TimeSpan = NCMApprovalTicket.RunAt == DateTime.MinValue ? DateTime.Now : NCMApprovalTicket.RunAt;
                SubmittedBy.Text = NCMApprovalTicket.SubmittedBy;
                StatusChangeTime.Text = FormatDateTime(NCMApprovalTicket.StatusChangeTime);
                RequestTime.Text = FormatDateTime(NCMApprovalTicket.RequestTime);
                ApprovedBy.Text = string.IsNullOrEmpty(NCMApprovalTicket.ApprovedBy) ? NCMWebContent.WEBDATA_VK_841 : NCMApprovalTicket.ApprovedBy;
                Notes.Text = NCMApprovalTicket.Comments;
                RequestorEmail.Text = NCMApprovalTicket.RequestorEmail;

                var role = ConfigChangeApprovalHelper.GetCurrentUserApprovalRole();
                bool isApprover;
                switch (NCMApprovalTicket.RequestStatus)
                {
                    case eApprovalStatus.Executing:
                    case eApprovalStatus.Scheduled:
                    case eApprovalStatus.Complete:
                        Status.Visible = true;
                        StatusList.Visible = false;
                        ExecutionMethod.Visible = true;
                        ExecutionMethodRadioButtonList.Visible = false;
                        Reboot.Enabled = false;
                        TargetNodesReadOnly = true;
                        RequestorEmail.ReadOnly = true;
                        Notes.ReadOnly = true;
                        btnSubmit.Visible = false;
                        btnCancel.Visible = false;
                        break;
                    case eApprovalStatus.WaitingForExecution:
                        Status.Visible = true;
                        StatusList.Visible = false;
                        ExecutionMethod.Visible = true;
                        ExecutionMethodRadioButtonList.Visible = false;
                        Reboot.Enabled = false;
                        RequestorEmail.ReadOnly = true;
                        TargetNodesReadOnly = true;
                        Notes.ReadOnly = true;
                        if (IsOwner)
                        {
                            btnSubmit.LocalizedText = SolarWinds.Orion.Web.UI.CommonButtonType.CustomText;
                            btnSubmit.Text = NCMWebContent.WEBDATA_VK_877;
                            btnSubmit.Visible = true;
                            btnCancel.Visible = true;
                        }
                        else
                        {
                            btnSubmit.Visible = false;
                            btnCancel.Visible = false;
                        }
                        break;
                    case eApprovalStatus.Declined:
                        Status.Visible = !IsOwner;
                        StatusList.Visible = IsOwner;
                        ExecutionMethod.Visible = !IsOwner;
                        ExecutionMethodRadioButtonList.Visible = IsOwner;
                        Reboot.Enabled = IsOwner;
                        RequestorEmail.ReadOnly = !IsOwner;
                        TargetNodesReadOnly = !IsOwner;
                        Notes.ReadOnly = !IsOwner;
                        btnSubmit.Visible = IsOwner;
                        btnCancel.Visible = IsOwner;
                        break;
                    case eApprovalStatus.PendingApproval:
                        if (IsNewApprovalRequest)
                        {
                            Status.Visible = true;
                            StatusList.Visible = false;
                            ExecutionMethod.Visible = false;
                            ExecutionMethodRadioButtonList.Visible = true;
                            Reboot.Enabled = true;
                            RequestorEmail.ReadOnly = false;
                            TargetNodesReadOnly = false;
                            Notes.ReadOnly = false;
                            btnSubmit.Visible = true;
                            btnCancel.Visible = true;
                        }
                        else
                        {
                            isApprover = role == eUserApproveRole.SingleApprover || role == eUserApproveRole.ApproverGroup1 || role == eUserApproveRole.RequestorAndApproverGroup1;
                            Status.Visible = !isApprover;
                            StatusList.Visible = isApprover;
                            ExecutionMethod.Visible = !IsOwner;
                            ExecutionMethodRadioButtonList.Visible = IsOwner;
                            Reboot.Enabled = IsOwner;
                            RequestorEmail.ReadOnly = !IsOwner;
                            TargetNodesReadOnly = !isApprover && !IsOwner;
                            Notes.ReadOnly = !isApprover && !IsOwner;
                            btnSubmit.Visible = isApprover || IsOwner;
                            btnCancel.Visible = isApprover || IsOwner;
                        }
                        break;
                    case eApprovalStatus.NeedConfirmation:
                        isApprover = role == eUserApproveRole.ApproverGroup2 || role == eUserApproveRole.RequestorAndApproverGroup2;
                        Status.Visible = !isApprover;
                        StatusList.Visible = isApprover;
                        ExecutionMethod.Visible = true;
                        ExecutionMethodRadioButtonList.Visible = false;
                        Reboot.Enabled = false;
                        RequestorEmail.ReadOnly = true;
                        TargetNodesReadOnly = !isApprover;
                        Notes.ReadOnly = !isApprover;
                        btnSubmit.Visible = isApprover;
                        btnCancel.Visible = isApprover;
                        break;
                }

                // for "Execute Config Change Template" or "Manage EnergyWise" or cases where we have script per node (remediation script) request type Target Nodes are read only
                if (NCMApprovalTicket.RequestType.Equals(NCMApprovalTicket.TYPE_CONFIG_CHANGE_TEMPLATE, StringComparison.InvariantCultureIgnoreCase) ||
                    NCMApprovalTicket.RequestType.Equals(NCMApprovalTicket.TYPE_MANAGE_EW, StringComparison.InvariantCultureIgnoreCase) || NCMApprovalTicket.UseScriptPerNode)
                    TargetNodesReadOnly = true;
            }

            if (CommonHelper.IsDemoMode())
            {
                // if demo mode - hide Submit and Cancel buttons
                btnSubmit.Visible = false;
                btnCancel.Visible = false;
            }

            Actions.Visible = !TargetNodesReadOnly;
            TargetNodes.NodeSelectionText = NCMWebContent.WEBDATA_VK_842;
            TargetNodes.ShowSelectNodesButton = false;
            TargetNodes.ShowDeleteNodeButton = !TargetNodesReadOnly;
            TargetNodes.NetworkNodes = NCMApprovalTicket.NetworkNodes;
            TargetNodesDIV.Controls.Add(TargetNodes);

            CheckLimitation();

            ApprovalScriptViewer.NetworkNodes = NCMApprovalTicket.NetworkNodes;
            ApprovalScriptViewer.LoadConrol();
        }
        catch (Exception ex)
        {
            ShowExceptionBar(ex);
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (TargetNodes.NetworkNodes.Count == 0)
        {
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), @"key_no_nodes",
                $@"alert('{NCMWebContent.WEBDATA_VK_848}');", true);
            return;
        }

        try
        {
            if (!String.IsNullOrWhiteSpace(RequestorEmail.Text))
                NCMApprovalTicket.RequestorEmail = RequestorEmail.Text;

            using (var proxy = isLayer.GetProxy())
            {
                var status = (eApprovalStatus)Enum.Parse(typeof(eApprovalStatus), StatusList.SelectedValue);

                if (ConfigChangeApprovalHelper.IsApproverUser && !IsNewApprovalRequest)
                {
                    if (NCMApprovalTicket.RequestStatus == eApprovalStatus.PendingApproval || NCMApprovalTicket.RequestStatus == eApprovalStatus.NeedConfirmation)
                    {
                        if (NCMApprovalTicket.RequestStatus == eApprovalStatus.PendingApproval && IsOwner)
                        {
                            NCMApprovalTicket.ExecutionType = GetExecutionType();
                            if (NCMApprovalTicket.ExecutionType == eExecutionType.SpecifiedTime)
                            {
                                if (!RunAt.ValidateDate()) return;
                                NCMApprovalTicket.RunAt = RunAt.TimeSpan;
                            }
                            if (NCMApprovalTicket.RequestType.Equals(NCMApprovalTicket.TYPE_UPLOAD_CONFIG, StringComparison.InvariantCultureIgnoreCase))
                                NCMApprovalTicket.Reboot = Reboot.Checked;
                        }
                        // update Network Nodes only for "Execute Script" and "Upload Config" Request Types
                        if ((NCMApprovalTicket.RequestType.Equals(NCMApprovalTicket.TYPE_EXECUTE_SCRIPTS, StringComparison.InvariantCultureIgnoreCase) ||
                            NCMApprovalTicket.RequestType.Equals(NCMApprovalTicket.TYPE_UPLOAD_CONFIG, StringComparison.InvariantCultureIgnoreCase)) && !NCMApprovalTicket.UseScriptPerNode)
                            NCMApprovalTicket.NetworkNodes = TargetNodes.NetworkNodes;
                        NCMApprovalTicket.Comments = Notes.Text;

                        if (status == eApprovalStatus.PendingApproval || status == eApprovalStatus.NeedConfirmation)
                        {
                            // clear ApprovedBy for Approval Ticket with selected Pending Approval status
                            NCMApprovalTicket.ApprovedBy = string.Empty;
                            // update Approval Ticket
                            proxy.Cirrus.ConfigChangeApproval.UpdateRequest(NCMApprovalTicket);
                        }
                        else if (status == eApprovalStatus.Declined)
                        {
                            NCMApprovalTicket.ApprovedBy = HttpContext.Current.Profile.UserName;
                            // decline Approval Ticket
                            proxy.Cirrus.ConfigChangeApproval.DeclineRequest(NCMApprovalTicket);
                        }
                        else
                        {
                            if (NCMApprovalTicket.ExecutionType == eExecutionType.SpecifiedTime)
                            {
                                if (NCMApprovalTicket.RunAt < DateTime.Now)
                                {
                                    System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), @"key_run_at_in_the_past",
                                        $@"alert('{NCMWebContent.WEBDATA_VK_853}');", true);
                                    return;
                                }
                            }

                            NCMApprovalTicket.ApprovedBy = HttpContext.Current.Profile.UserName;
                            // aprove Approval Ticket
                            proxy.Cirrus.ConfigChangeApproval.ApproveRequest(NCMApprovalTicket, HttpContext.Current.Profile.UserName);
                        }
                    }
                    else if (NCMApprovalTicket.RequestStatus == eApprovalStatus.Declined && IsOwner) //request owner reask for approval for declined request
                    {
                        NCMApprovalTicket.ExecutionType = GetExecutionType();
                        if (NCMApprovalTicket.ExecutionType == eExecutionType.SpecifiedTime)
                        {
                            if (!RunAt.ValidateDate()) return;
                            NCMApprovalTicket.RunAt = RunAt.TimeSpan;
                        }
                        if (NCMApprovalTicket.RequestType.Equals(NCMApprovalTicket.TYPE_UPLOAD_CONFIG, StringComparison.InvariantCultureIgnoreCase))
                            NCMApprovalTicket.Reboot = Reboot.Checked;
                        // update Network Nodes only for "Execute Script" and "Upload Config" Request Types
                        if ((NCMApprovalTicket.RequestType.Equals(NCMApprovalTicket.TYPE_EXECUTE_SCRIPTS, StringComparison.InvariantCultureIgnoreCase) ||
                            NCMApprovalTicket.RequestType.Equals(NCMApprovalTicket.TYPE_UPLOAD_CONFIG, StringComparison.InvariantCultureIgnoreCase)) && !NCMApprovalTicket.UseScriptPerNode)
                            NCMApprovalTicket.NetworkNodes = TargetNodes.NetworkNodes;
                        NCMApprovalTicket.Comments = Notes.Text;

                        if (NCMApprovalTicket.RequestStatus != status)
                            NCMApprovalTicket.StatusChangeTime = DateTime.Now;

                        NCMApprovalTicket.RequestStatus = status;
                        // clear ApprovedBy for Approval Ticket with Pending Approval status
                        if (NCMApprovalTicket.RequestStatus == eApprovalStatus.PendingApproval) NCMApprovalTicket.ApprovedBy = string.Empty;

                        proxy.Cirrus.ConfigChangeApproval.UpdateRequest(NCMApprovalTicket);
                    }
                    else if (NCMApprovalTicket.RequestStatus == eApprovalStatus.WaitingForExecution && IsOwner)
                    {
                        // aprove Approval Ticket
                        proxy.Cirrus.ConfigChangeApproval.ApproveRequest(NCMApprovalTicket, HttpContext.Current.Profile.UserName);
                    }
                    else { /*do nothing*/ }
                }
                else
                {
                    if (NCMApprovalTicket.RequestStatus == eApprovalStatus.PendingApproval)
                    {
                        NCMApprovalTicket.ExecutionType = GetExecutionType();
                        if (NCMApprovalTicket.ExecutionType == eExecutionType.SpecifiedTime)
                        {
                            if (!RunAt.ValidateDate()) return;
                            NCMApprovalTicket.RunAt = RunAt.TimeSpan;
                        }
                        if (NCMApprovalTicket.RequestType.Equals(NCMApprovalTicket.TYPE_UPLOAD_CONFIG, StringComparison.InvariantCultureIgnoreCase))
                            NCMApprovalTicket.Reboot = Reboot.Checked;
                        // update Network Nodes only for "Execute Script" and "Upload Config" Request Types
                        if ((NCMApprovalTicket.RequestType.Equals(NCMApprovalTicket.TYPE_EXECUTE_SCRIPTS, StringComparison.InvariantCultureIgnoreCase) ||
                            NCMApprovalTicket.RequestType.Equals(NCMApprovalTicket.TYPE_UPLOAD_CONFIG, StringComparison.InvariantCultureIgnoreCase)) && !NCMApprovalTicket.UseScriptPerNode)
                            NCMApprovalTicket.NetworkNodes = TargetNodes.NetworkNodes;
                        NCMApprovalTicket.Comments = Notes.Text;

                        if (IsNewApprovalRequest)
                            proxy.Cirrus.ConfigChangeApproval.AddRequest(NCMApprovalTicket);
                        else
                            proxy.Cirrus.ConfigChangeApproval.UpdateRequest(NCMApprovalTicket);
                    }
                    else if (NCMApprovalTicket.RequestStatus == eApprovalStatus.Declined)
                    {
                        NCMApprovalTicket.ExecutionType = GetExecutionType();
                        if (NCMApprovalTicket.ExecutionType == eExecutionType.SpecifiedTime)
                        {
                            if (!RunAt.ValidateDate()) return;
                            NCMApprovalTicket.RunAt = RunAt.TimeSpan;
                        }
                        if (NCMApprovalTicket.RequestType.Equals(NCMApprovalTicket.TYPE_UPLOAD_CONFIG, StringComparison.InvariantCultureIgnoreCase))
                            NCMApprovalTicket.Reboot = Reboot.Checked;
                        // update Network Nodes only for "Execute Script" and "Upload Config" Request Types
                        if ((NCMApprovalTicket.RequestType.Equals(NCMApprovalTicket.TYPE_EXECUTE_SCRIPTS, StringComparison.InvariantCultureIgnoreCase) ||
                            NCMApprovalTicket.RequestType.Equals(NCMApprovalTicket.TYPE_UPLOAD_CONFIG, StringComparison.InvariantCultureIgnoreCase)) && !NCMApprovalTicket.UseScriptPerNode)
                            NCMApprovalTicket.NetworkNodes = TargetNodes.NetworkNodes;
                        NCMApprovalTicket.Comments = Notes.Text;

                        if (NCMApprovalTicket.RequestStatus != status)
                            NCMApprovalTicket.StatusChangeTime = DateTime.Now;

                        NCMApprovalTicket.RequestStatus = status;
                        // clear ApprovedBy for Approval Ticket with Pending Approval status
                        if (NCMApprovalTicket.RequestStatus == eApprovalStatus.PendingApproval) NCMApprovalTicket.ApprovedBy = string.Empty;

                        proxy.Cirrus.ConfigChangeApproval.UpdateRequest(NCMApprovalTicket);
                    }
                    else if (NCMApprovalTicket.RequestStatus == eApprovalStatus.WaitingForExecution)
                    {
                        // aprove Approval Ticket
                        proxy.Cirrus.ConfigChangeApproval.ApproveRequest(NCMApprovalTicket, HttpContext.Current.Profile.UserName);
                    }
                    else { /*do nothing*/ }
                }
            }

            ClearSessionState();
            Redirect();
        }
        catch (Exception ex)
        {
            ShowExceptionBar(ex);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ClearSessionState();
        Redirect();
    }

    #endregion

    #region Private Members

    private void Redirect()
    {
        var returnUrl = UrlHelper.FromSafeUrlParameter(Page.Request.QueryString[@"NCMReturnURL"]);
        if (string.IsNullOrEmpty(returnUrl))
        {
            returnUrl = @"/Orion/NCM/Admin/ConfigChangeApproval/ApprovalRequests.aspx";
        }
        Page.Response.Redirect(returnUrl);
    }

    private void ShowExceptionBar(Exception ex)
    {
        MainContent.Visible = false;
        btnSubmit.Visible = false;
        btnCancel.Visible = false;
        ExWarn.Visible = true;

        if (ex is System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>)
        {
            ExWarn.ExceptionMessage = (ex as System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>).Detail.Message;
        }
        else
        {
            if (ex.InnerException != null)
            {
                ExWarn.ExceptionMessage = ex.InnerException.Message;
            }
            else
            {
                ExWarn.ExceptionMessage = ex.Message;
            }
        }
    }

    private string FormatDateTime(object datetime)
    {
        var t = (DateTime)datetime;
        if (t == DateTime.MinValue) return NCMWebContent.WEBDATA_VK_118;
        return $@"{Utils.FormatCurrentCultureDate(t)} {Utils.FormatToLocalTime(t)}";
    }

    private eExecutionType GetExecutionType()
    {
        if (rbRunImmediately.Checked) return eExecutionType.Immediate;
        else if (rbSpecifiedTime.Checked) return eExecutionType.SpecifiedTime;
        else return eExecutionType.ReturnToRequestor;
    }

    private void SetExecutionMethod(eExecutionType type)
    {
        switch (type)
        {
            case eExecutionType.Immediate:
                rbRunImmediately.Checked = true;
                break;
            case eExecutionType.SpecifiedTime:
                rbSpecifiedTime.Checked = true;
                break;
            case eExecutionType.ReturnToRequestor:
                rbReturnToRequestor.Checked = true;
                break;
            default:
                rbRunImmediately.Checked = true;
                break;
        }
    }

    private void LoadStatusList(eApprovalStatus status)
    {
        switch (status)
        {
            case eApprovalStatus.PendingApproval:
                StatusList.Items.Add(new ListItem() { Text = ConfigChangeApprovalHelper.LookupLocalizedRequestStatus(eApprovalStatus.PendingApproval), Value = eApprovalStatus.PendingApproval.ToString(@"D") });
                StatusList.Items.Add(new ListItem() { Text = NCMWebContent.WEBDATA_VK_843, Value = @"-1" });
                break;
            case eApprovalStatus.NeedConfirmation:
                StatusList.Items.Add(new ListItem() { Text = ConfigChangeApprovalHelper.LookupLocalizedRequestStatus(eApprovalStatus.NeedConfirmation), Value = eApprovalStatus.NeedConfirmation.ToString(@"D") });
                StatusList.Items.Add(new ListItem() { Text = NCMWebContent.WEBDATA_VK_843, Value = @"-1" });
                break;
            case eApprovalStatus.Declined:
                StatusList.Items.Add(new ListItem() { Text = ConfigChangeApprovalHelper.LookupLocalizedRequestStatus(eApprovalStatus.PendingApproval), Value = eApprovalStatus.PendingApproval.ToString(@"D") });
                break;
        }
        StatusList.Items.Add(new ListItem() { Text = ConfigChangeApprovalHelper.LookupLocalizedRequestStatus(eApprovalStatus.Declined), Value = eApprovalStatus.Declined.ToString(@"D") });
    }

    private void ClearSessionState()
    {
        Page.Session.Remove(PageGuid);
    }

    private void ValidatePageAccess()
    {
        var role = ConfigChangeApprovalHelper.GetCurrentUserApprovalRole();
        if (!string.IsNullOrEmpty(Page.Request.QueryString[@"GuidID"])) //new approval request
        {
            if (role == eUserApproveRole.None || role == eUserApproveRole.SingleApprover || role == eUserApproveRole.ApproverGroup1 || role == eUserApproveRole.ApproverGroup2)
                Server.Transfer($@"~/Orion/Error.aspx?Message={NCMWebContent.WEBDATA_IC_173}");
        }
        else
        {
            if ((role == eUserApproveRole.None || role == eUserApproveRole.Requestor) && !IsOwner)
                Server.Transfer($@"~/Orion/Error.aspx?Message={NCMWebContent.WEBDATA_IC_173}");
        }
    }

    private void CheckLimitation()
    {
        try
        {
            int nodesCountWithLimitation;
            int nodesCountWithoutLimitation;

            var swql_Format = @"
                SELECT 
                    COUNT(NCMNodeProperties.NodeID) AS NodesCount 
                FROM Cirrus.NodeProperties AS NCMNodeProperties
                INNER JOIN Orion.Nodes AS Nodes 
                    ON NCMNodeProperties.CoreNodeID=Nodes.NodeID
                WHERE NCMNodeProperties.NodeID IN ({0})";

            DataTable dt;
            using (var proxy = isLayer.GetProxy())
            {
                dt = proxy.Query(string.Format(CultureInfo.InvariantCulture, swql_Format, string.Join(@",", NCMApprovalTicket.NetworkNodes.Select(node =>
                    $@"'{node.NodeID}'"))));
                nodesCountWithLimitation = Convert.ToInt32(dt.Rows[0][0]);
            }

            using (var proxy = isLayer.GetProxyWithoutImpersonation())
            {
                dt = proxy.Query(string.Format(CultureInfo.InvariantCulture, swql_Format, string.Join(@",", NCMApprovalTicket.NetworkNodes.Select(node =>
                    $@"'{node.NodeID}'"))));
                nodesCountWithoutLimitation = Convert.ToInt32(dt.Rows[0][0]);
            }

            LimitationHelpLinkHolder.Visible = (nodesCountWithLimitation != nodesCountWithoutLimitation);
        }
        catch (Exception ex)
        {
            LimitationHelpLinkHolder.Visible = false;
            log.Error(ex.Message);
        }
    }

    #endregion
}
