using System;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCM.Web.Contracts;

public partial class Orion_NCM_Admin_ConfigChangeApproval_ApprovalScriptView : System.Web.UI.Page
{
    private readonly IMacroParser macroParser;
    private readonly IIsWrapper isLayer;

    public Orion_NCM_Admin_ConfigChangeApproval_ApprovalScriptView()
    {
        macroParser = ServiceLocator.Container.Resolve<IMacroParser>();
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var nodeId = new Guid(Page.Request.QueryString[@"NodeId"]);
        var requestId = Page.Request.QueryString[@"RequestId"];

        Page.Title = string.Format(Resources.NCMWebContent.WEBDATA_VK_48, macroParser.ParseMacro(nodeId, @"${NodeCaption}"));

        try
        {
            NCMApprovalTicket ncmApprovalTicket = null;
            if (!string.IsNullOrEmpty(requestId))
            {
                using (var proxy = isLayer.GetProxy())
                {
                    ncmApprovalTicket = proxy.Cirrus.ConfigChangeApproval.GetRequest(new Guid(requestId));
                }
            }
            else
            {
                var pageGuid = Page.Request.QueryString[@"GuidID"];
                if (!string.IsNullOrEmpty(pageGuid))
                {
                    ncmApprovalTicket = (NCMApprovalTicket)Page.Session[pageGuid];
                }
                if (ncmApprovalTicket == null)
                {
                    Page.Response.Redirect("~/Orion/NCM/Admin/ConfigChangeApproval/ApprovalRequests.aspx");
                }
            }

            var networkNode = ncmApprovalTicket.NetworkNodes.Find(item => item.NodeID == nodeId);
            if (networkNode != null) txtScript.Text = networkNode.Script;
        }
        catch (Exception ex)
        {
            txtScript.Visible = false;
            ExWarn.Visible = true;

            if (ex is System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>)
            {
                ExWarn.ExceptionMessage = (ex as System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>).Detail.Message;
            }
            else
            {
                if (ex.InnerException != null)
                {
                    ExWarn.ExceptionMessage = ex.InnerException.Message;
                }
                else
                {
                    ExWarn.ExceptionMessage = ex.Message;
                }
            }
        }
    }
}
