using System;
using System.Web.UI;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_Admin_SMTPServer : UserControl
{
    private readonly IIsWrapper _isLayer;
    private readonly IDataDecryptor decryptor;

    public Orion_Admin_SMTPServer()
    {
        _isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
        decryptor = ServiceLocator.Container.Resolve<IDataDecryptor>();
    }

    private string jsScriptKey = @"B36BF6FC-23F9-4E0C-B089-8A9C53255F0B";
    private string jsScriptFormat = @"
    function pageLoad() {{
        var value = $('#{0}').val();
        ShowHideAuthContent(value);
        chkShowHideSSL(document.getElementById('{1}'));
    }}

    function ddlAuthListSelectIndexChange(obj) {{
        var value = obj.options[obj.selectedIndex].value;
        ShowHideAuthContent(value);
    }}

    function chkShowHideSSL(obj) {{
        if (obj == null) return;
        var value = obj.checked;
        var sslPortNumberValidator = document.getElementById('{2}');
        if (value) {{
            if (sslPortNumberValidator != null) ValidatorEnable(sslPortNumberValidator, true);
            $('#SSLPortContent').show();
        }}
        else {{
            if (sslPortNumberValidator != null) ValidatorEnable(sslPortNumberValidator, false);
            $('#SSLPortContent').hide();
        }}
    }}

    function ShowHideAuthContent(value) {{
        var comparePasswordsValidator = document.getElementById('{3}');
        var rfvUsernameValidator = document.getElementById('{4}');
        var rfvPasswordValidator = document.getElementById('{5}');
        var rfvPassword2Validator = document.getElementById('{6}');
        if (value == 'false') {{
            if (comparePasswordsValidator != null) ValidatorEnable(comparePasswordsValidator, false);
            if (rfvUsernameValidator != null) ValidatorEnable(rfvUsernameValidator, false);
            if (rfvPasswordValidator != null) ValidatorEnable(rfvPasswordValidator, false);
            if (rfvPassword2Validator != null) ValidatorEnable(rfvPassword2Validator, false);
            $('#AuthContent').hide();
        }}
        else {{
            $('#AuthContent').show();
            if (comparePasswordsValidator != null) ValidatorEnable(comparePasswordsValidator, true);
            if (rfvUsernameValidator != null) ValidatorEnable(rfvUsernameValidator, true);
            if (rfvPasswordValidator != null) ValidatorEnable(rfvPasswordValidator, true);
            if (rfvPassword2Validator != null) ValidatorEnable(rfvPassword2Validator, true);
        }}
    }}

    function cvPassword_Validate(source, args) {{
        args.IsValid = (args.Value == document.getElementById('{7}').value);
    }}";

    protected void Page_Load(object sender, EventArgs e)
    {
        var jsScript = string.Format(jsScriptFormat,
            ddlAuthList.ClientID,
            chUseSSL.ClientID,
            txtSSLPortNumberValidator.ClientID,
            comparePasswords.ClientID,
            rfvUsername.ClientID,
            rfvPassword.ClientID,
            rfvPassword2.ClientID,
            txtPassword.ClientID
            );

        ScriptManager.RegisterClientScriptBlock(Page,
                GetType(),
                jsScriptKey,
                jsScript,
                true);
    }

    public new void Load()
    {
        using (var proxy = _isLayer.GetProxy())
        {
            txtServerAddress.Text = proxy.Cirrus.Settings.GetSetting(SolarWinds.NCM.Contracts.InformationService.Settings.SMTPServer, @"SMTP.MyDomain.Com", null);
            txtPortNumber.Text = proxy.Cirrus.Settings.GetSetting(SolarWinds.NCM.Contracts.InformationService.Settings.SMTPPort, 25, null);
            chUseSSL.Checked = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(SolarWinds.NCM.Contracts.InformationService.Settings.SMTPUseSSL, false, null));
            txtSSLPortNumber.Text = proxy.Cirrus.Settings.GetSetting(SolarWinds.NCM.Contracts.InformationService.Settings.SMTPSSLPort, 587, null);
            ddlAuthList.SelectedValue = proxy.Cirrus.Settings.GetSetting(SolarWinds.NCM.Contracts.InformationService.Settings.SMTPUseAuth, @"true", null).ToLowerInvariant();
            txtUsername.Text = decryptor.Decrypt(proxy.Cirrus.Settings.GetSetting(SolarWinds.NCM.Contracts.InformationService.Settings.SMTPAuthUserName, @"SMTPUsername", null));
            txtPassword.Attributes[@"value"] = GetHiddenPassword(proxy.Cirrus.Settings.GetSetting(SolarWinds.NCM.Contracts.InformationService.Settings.SMTPAuthPassword, @"SMTPPassword", null));
            txtPassword2.Attributes[@"value"] = GetHiddenPassword(proxy.Cirrus.Settings.GetSetting(SolarWinds.NCM.Contracts.InformationService.Settings.SMTPAuthPassword, @"SMTPPassword", null));
        }
    }

    /// <summary>
    /// Save SMTP server settings into database.
    /// </summary>
    public void Save()
    {
        using (var proxy = _isLayer.GetProxy())
        {
            proxy.Cirrus.Settings.SaveSetting(SolarWinds.NCM.Contracts.InformationService.Settings.SMTPServer, txtServerAddress.Text, null);
            proxy.Cirrus.Settings.SaveSetting(SolarWinds.NCM.Contracts.InformationService.Settings.SMTPPort, txtPortNumber.Text, null);
            proxy.Cirrus.Settings.SaveSetting(SolarWinds.NCM.Contracts.InformationService.Settings.SMTPUseSSL, chUseSSL.Checked.ToString().ToLowerInvariant(), null);
            proxy.Cirrus.Settings.SaveSetting(SolarWinds.NCM.Contracts.InformationService.Settings.SMTPUseAuth, ddlAuthList.SelectedValue, null);

            if (chUseSSL.Checked)
            {
                proxy.Cirrus.Settings.SaveSetting(SolarWinds.NCM.Contracts.InformationService.Settings.SMTPSSLPort, txtSSLPortNumber.Text, null);
            }

            if (Convert.ToBoolean(ddlAuthList.SelectedValue))
            {
                proxy.Cirrus.Settings.SaveSetting(SolarWinds.NCM.Contracts.InformationService.Settings.SMTPAuthUserName, txtUsername.Text, null);

                if (!txtPassword.Text.Equals(GetHiddenPassword(proxy.Cirrus.Settings.GetSetting(SolarWinds.NCM.Contracts.InformationService.Settings.SMTPAuthPassword, @"SMTPPassword", null))))
                {
                    proxy.Cirrus.Settings.SaveSetting(SolarWinds.NCM.Contracts.InformationService.Settings.SMTPAuthPassword, txtPassword.Text, null);
                }
            }
        }
    }

    /// <summary>
    /// Use to hide password in html response.
    /// </summary>
    private string GetHiddenPassword(string value)
    {
        return new String('?', decryptor.Decrypt(value).Length);
    }
}
