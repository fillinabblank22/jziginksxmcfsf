﻿
<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="AssignToNodes.aspx.cs" Inherits="Orion_NCM_AssignToNodes" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="adminPageTitlePlaceHolder">
	<%=Page.Title%>
</asp:Content>

<asp:Content ID ="Content2" runat="server" ContentPlaceHolderID="adminHelpIconPlaceHolder">
    <orion:IconHelpButton ID="helpBtn" runat="server" HelpUrlFragment="OrionNCMWebPolicyReportImport" />
    <script type="text/javascript" src="/Orion/NCM/JavaScript/main.js"></script>
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
</asp:Content>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="adminContentPlaceholder">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />

    <script type="text/javascript">
    </script>
    
    <div style="padding: 10px;">
        <br />
        <asp:UpdatePanel ID="updatePanel" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <div class="ncm-main-content">
                    <div style="max-height: 500px; min-width: 500px; overflow: auto;">
                        <table id="nodetreecontrol_device_template_datapicker" cellpadding="0" cellspacing="0" width="50%">
                            <tr>
                                <td id="nodes_picker_container" runat="server">                                    
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="padding-top: 10px;">
                        <asp:CustomValidator ID="customValidator" runat="server" OnServerValidate="NodeSelectionValidate" ErrorMessage="* You must select a node" EnableClientScript="false" Display="Dynamic"></asp:CustomValidator>
                    </div>
                </div>
                <br />
                <orion:LocalizableButton runat="server" ID="Submit" LocalizedText="submit" DisplayType="Primary" OnClick="SubmitClick"  CausesValidation="True" />
                <orion:LocalizableButton runat="server" ID="Cancel" LocalizedText="Cancel" DisplayType="secondary" OnClick="CancelClick" CausesValidation="false"  /> 
            </ContentTemplate>
        </asp:UpdatePanel>    
    </div>
</asp:Content>


