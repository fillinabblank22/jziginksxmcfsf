using System;
using System.Collections.Generic;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_Admin_NcmModuleDetails : System.Web.UI.UserControl
{
    private static Log _log;
    private static Log _logger { get { return _log ?? (_log = new Log()); } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                GetModuleAndLicenseInfo(@"NCM");
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("Error while displaying details for NCM module. Details: {0}", ex.ToString());
            }
        }
    }

    private void GetModuleAndLicenseInfo(string moduleName)
    {
        foreach (var module in ModuleDetailsHelper.LoadModuleInfoForEngines(@"Primary", false))
        {
            if (!module.ProductDisplayName.StartsWith(moduleName, StringComparison.OrdinalIgnoreCase))
                continue;

            var values = new Dictionary<string, string>();

            NcmDetails.Name = module.ProductDisplayName;

            values.Add(Resources.NCMWebContent.WEBDATA_VK_112, module.ProductName);
            values.Add(Resources.NCMWebContent.WEBDATA_VK_113, module.Version);
            values.Add(Resources.NCMWebContent.WEBDATA_VK_114, string.IsNullOrEmpty(module.HotfixVersion) ? Resources.NCMWebContent.WEBDATA_VK_118 : module.HotfixVersion);
            if (!string.IsNullOrEmpty(module.LicenseInfo))
                values.Add(Resources.NCMWebContent.WEBDATA_VK_115, module.LicenseInfo);

            AddNcmInfo(values);

            NcmDetails.DataSource = values;
            break;
        }
    }

    private void AddNcmInfo(Dictionary<string, string> values)
    {
        try
        {
            var featureManager = new FeatureManager();
            var maxCountOfNodes = featureManager.GetMaxElementCount(@"NCM.Nodes");

            var currentCountOfNodes = 0;
            var isLayer = new ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                var dt = proxy.Query(@"SELECT COUNT(NCMNodeProperties.NodeID) AS NodesCount FROM Cirrus.NodeProperties AS NCMNodeProperties INNER JOIN Orion.Nodes AS Nodes ON NCMNodeProperties.CoreNodeID=Nodes.NodeID");
                if (dt != null && dt.Rows.Count > 0)
                {
                    currentCountOfNodes = Convert.ToInt32(dt.Rows[0][0]);
                }
            }

            values.Add(Resources.NCMWebContent.WEBDATA_VK_116, maxCountOfNodes > 1000000 ? Resources.NCMWebContent.WEBDATA_VK_119 : maxCountOfNodes.ToString());
            values.Add(Resources.NCMWebContent.WEBDATA_VK_117, currentCountOfNodes.ToString());
        }
        catch (Exception ex)
        {
            _logger.Error("Cannot get number of NCM licensed elements.", ex);
        }
    }
}
