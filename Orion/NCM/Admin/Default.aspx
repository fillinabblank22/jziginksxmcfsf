<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" CodeFile="Default.aspx.cs" Inherits="Orion_NCM_Admin_Default" %>
<%@ Import Namespace="Resources" %>

<%@ Register Src="~/Orion/NCM/Admin/EnableApprovalSystem.ascx" TagPrefix="ncm" TagName="EnableApprovalSystem" %>
<%@ Register Src="~/Orion/Admin/AdminBucket.ascx" TagPrefix="orion" TagName="AdminBucket" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="adminHeadPlaceHolder">
    <style type="text/css">
        #ncmSettings {
            padding: 15px 20px 20px;
            margin-top: -5px;
            background: #DFE0E1;
        }
        #ncmSettings a { color: #336699; }
	    #ncmSettings a:hover { color:orange; }
        #ncmSettings .NewIconContentBucket{ margin: 15px 0 0 0;} 
        #ncmSettings .NewBucketIcon { width: 75px; max-height: 75px;}
        #ncmSettings .NewBucketHeader { font-size: 16px;}
        #ncmSettings .sw-hdr-title{font-weight: 700;font-size: 18px;text-transform: capitalize;line-height: 15px;padding: 15px;background: #ffffff;box-shadow: 0 1px 5px 0 #b9b9b9;}
        .titleTable, .breadcrumb, .dateArea { display: none;}
	 </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
<asp:Panel id="ContentContainer" runat="server">
    <div id="ncmSettings">
        <div class="sw-hdr-title">
            <h1><%=Page.Title %></h1>
        </div>

        <orion:AdminBucket ID="nodeManagement" runat="server" NewStyle="True"
        Icon="../Resources/images/NcmSettings/management.svg" ColumnCount="3"
        Header="<%$ Resources: NCMWebContent, WEBDATA_VK_07 %>">
        <Description><%= Resources.NCMWebContent.WEBDATA_VK_08 %></Description>
        </orion:AdminBucket>
        
        <orion:AdminBucket ID="configs" runat="server" NewStyle="True"
        Icon="../Resources/images/NcmSettings/gears.svg" ColumnCount="3"
        Header="<%$ Resources: NCMWebContent, WEBDATA_VK_598 %>">
        <Description><%= Resources.NCMWebContent.WEBDATA_VK_599 %></Description>
        </orion:AdminBucket>
        
        <orion:AdminBucket ID="network" runat="server" NewStyle="True"
        Icon="../Resources/images/NcmSettings/network.svg" ColumnCount="3"
        Header="<%$ Resources: NCMWebContent, WEBDATA_VK_612 %>">
        <Description><%= Resources.NCMWebContent.WEBDATA_VK_613 %></Description>
        </orion:AdminBucket>
        
        <orion:AdminBucket ID="notifications" runat="server" NewStyle="True"
        Icon="../Resources/images/NcmSettings/notifications.svg" ColumnCount="3"
        Header="<%$ Resources: NCMWebContent, WEBDATA_VK_604 %>">
        <Description><%= Resources.NCMWebContent.WEBDATA_VK_605 %></Description>
        </orion:AdminBucket>
        
        <orion:AdminBucket ID="rtnSettings" runat="server" NewStyle="True"
        Icon="../Resources/images/NcmSettings/real-time.svg" ColumnCount="3" 
        Header="<%$ Resources: NCMWebContent, WEBDATA_VK_617 %>">
        <Description><%= string.Format(Resources.NCMWebContent.WEBDATA_VK_618, this.LearnMore(@"OrionNCMPHSettingsRTCDLearnMore",NCMWebContent.WEBDATA_VY_06))%></Description>
        </orion:AdminBucket>

        <orion:AdminBucket ID="firmwareUpgrade" runat="server" NewStyle="True"
        Icon="../Resources/images/NcmSettings/firmware-upgrade.svg" ColumnCount="3"
        Header="<%$ Resources: NCMWebContent, NCMSettings_FirmwareUpgrade_Header %>">
        <Description><%= Resources.NCMWebContent.NCMSettings_FirmwareUpgrade_Description %></Description>                        
        </orion:AdminBucket>

        <orion:AdminBucket ID="policyManagement" runat="server" NewStyle="True"
        Icon="../Resources/images/NcmSettings/compliance-policy.svg" ColumnCount="3"
        Header="<%$ Resources: NCMWebContent, WEBDATA_VK_10 %>">
        <Description><%= string.Format(Resources.NCMWebContent.WEBDATA_VK_11, this.LearnMore(@"OrionNCMPHSettingsComplianceLearnMore",NCMWebContent.WEBDATA_VY_06)) %></Description>
        </orion:AdminBucket>
        
        <orion:AdminBucket ID="security" runat="server" NewStyle="True"
        Icon="../Resources/images/NcmSettings/lock.svg" ColumnCount="3"
        Header="<%$ Resources: NCMWebContent, WEBDATA_VK_498 %>">
        <Description><%= Resources.NCMWebContent.WEBDATA_VK_602 %></Description>
        </orion:AdminBucket>
        
        <orion:AdminBucket ID="globalDeviceDefaults" runat="server" NewStyle="True"
        Icon="../Resources/images/NcmSettings/globe.svg" ColumnCount="3"
        Header="<%$ Resources: NCMWebContent, WEBDATA_VK_621 %>">
        <Description><%= Resources.NCMWebContent.WEBDATA_VK_622 %></Description>
        </orion:AdminBucket>
        
        <orion:AdminBucket ID="advanced" runat="server" NewStyle="True"
        Icon="../Resources/images/NcmSettings/advanced.svg" ColumnCount="3"
        Header="<%$ Resources: NCMWebContent, WEBDATA_VK_608 %>">
        <Description><%= Resources.NCMWebContent.WEBDATA_VK_609 %><br><%=ProductNameAndVersion%></Description>
        </orion:AdminBucket>
        
        <orion:AdminBucket ID="approval" runat="server" NewStyle="True"
        Icon="../Resources/images/NcmSettings/config-approval.svg" ColumnCount="3"
        Header="<%$ Resources: NCMWebContent, WEBDATA_VK_02 %>"
        CustomColumnIndex="1">
        <Description><%= string.Format(Resources.NCMWebContent.WEBDATA_VK_03, this.LearnMore(@"OrionNCMPHSettingsChangeApprovalLearnMore",NCMWebContent.WEBDATA_VY_06)) %></Description>
        <CustomColumn>
			<p><ncm:EnableApprovalSystem ID="EnableApprovalSystem" runat="server" /></p>
		</CustomColumn>
        </orion:AdminBucket>
    </div>
</asp:Panel>
<div id="divError" runat="server"/>
</asp:Content>
