﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="AddEditFirmwareDefinition.aspx.cs" Inherits="Orion_NCM_Firmware_AddEditFirmwareDefinition" %>
<%@ Import Namespace="SolarWinds.NCMModule.Web.Resources" %>
<%@ Import Namespace="SolarWinds.NCM.Common.Dal" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/NCM/Firmware/Controls/SelectFirmwareImages.ascx" TagPrefix="ncm" TagName="SelectFirmwareImages" %>
<%@ Register Src="~/Orion/Controls/Slider.ascx" TagPrefix="orion" TagName="Slider" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" runat="Server">
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/PolicyReports/PolicyReport.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
    <orion:Include runat="server" File="Discovery.css" />
    <style type="text/css">
        #NodeUpCheckDelayAfterReboot tbody tr td
        {
            padding: 10px;
            background-color: #e4f1f8;
        }
        .SettingNote
        {
            color:gray;
            font-style:italic;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            toggleCommandCheckbox(document.getElementById("<%=chkGetCurrentFirmwareImageCommand.ClientID %>").checked, 'GetCurrentFirmwareImageCommandHolder', 'rfvShowCurrentFirmwareImageCommand');
            toggleCaptureResultsPatternCheckbox(document.getElementById("<%=chkCaptureSysImageFilePattern.ClientID %>").checked, 'CaptureSysImageFilePatternHolder', 'rfvCaptureSysImageFilePattern', 'revCaptureSysImageFilePattern');
            toggleCommandCheckbox(document.getElementById("<%=chkCollectBootVariableInfoCommand.ClientID %>").checked, 'CollectBootVariableInfoCommandHolder', 'rfvCollectBootVariableInfoCommand');
            toggleCommandCheckbox(document.getElementById("<%=chkGetInfoOnConfigRegisterCommand.ClientID %>").checked, 'GetInfoOnConfigRegisterCommandHolder', 'rfvShowVersionCommand');
            toggleCaptureResultsPatternCheckbox(document.getElementById("<%=chkCaptureConfigRegisterPattern.ClientID %>").checked, 'CaptureConfigRegisterPatternHolder', 'rfvCaptureConfigRegisterPattern', 'revCaptureConfigRegisterPattern', 'rfvExpectedConfigRegisterValue');
            toggleCommandCheckbox(document.getElementById("<%=chkGetFreeSpaceCommand.ClientID %>").checked, 'GetFreeSpaceCommandHolder', 'rfvShowFreeSpaceCommand');
            toggleCommandCheckbox(document.getElementById("<%=chkOtherCommands.ClientID %>").checked, 'OtherCommandsHolder', 'rfvOtherCommands');
            toggleCommandCheckbox(document.getElementById("<%=chkDeleteFirmwareImageCommand.ClientID %>").checked, 'DeleteFirmwareImageCommandHolder', 'rfvDeleteFirmwareImageCommand');
            toggleCommandCheckbox(document.getElementById("<%=chkBackupFirmwareImageCommand.ClientID %>").checked, 'BackupFirmwareImageCommandHolder', 'rfvBackupFirmwareImageCommand');
            toggleCommandCheckbox(document.getElementById("<%=chkUpdateConfigRegisterCommand.ClientID %>").checked, 'UpdateConfigRegisterCommandHolder', 'rfvUpdateConfigRegisterCommand');
            toggleCommandCheckbox(document.getElementById("<%=chkUpdateBootVariableCommand.ClientID %>").checked, 'UpdateBootVariableCommandHolder', 'rfvUpdateBootVariableCommand');
            toggleOption(document.getElementById("<%=chkUpdateBootVariableCommand.ClientID %>").checked, 'UpdateBootVariableHolder');
            toggleCommandCheckbox(document.getElementById("<%=chkRebootCommand.ClientID %>").checked, 'RebootCommandHolder', 'rfvRebootCommand');
            toggleOption(document.getElementById("<%=chkRebootCommand.ClientID %>").checked, 'RebootHolder');
            toggleReboot(document.getElementById("<%=chkReboot.ClientID %>").checked, 'UnmanageHolder');

            toggleCommandCheckbox(document.getElementById("<%=chkVerifyUploadCommand.ClientID %>").checked, 'VerifyUploadCommandHolder', 'rfvVerifyUploadCommand');
            toggleOption(document.getElementById("<%=chkVerifyUploadCommand.ClientID %>").checked, 'VerifyUploadHolder');

            toggleCommandCheckbox(document.getElementById("<%=chkVerifyBackupCommand.ClientID %>").checked, 'VerifyBackupCommandHolder', 'rfvVerifyBackupCommand');
            toggleOption(document.getElementById("<%=chkVerifyBackupCommand.ClientID %>").checked && document.getElementById("<%=chkBackupFirmwareImageCommand.ClientID %>").checked, 'VerifyBackupHolder');

            toggleCommandCheckbox(document.getElementById("<%=chkVerifyUpgradeCommand.ClientID %>").checked, 'VerifyUpgradeCommandHolder', 'rfvVerifyUpgradeCommand');
            toggleOption(document.getElementById("<%=chkVerifyUpgradeCommand.ClientID %>").checked, 'VerifyUpgradeHolder');

            var isFipsEnabled = <%=ServiceLocator.Container.Resolve<IFipsConfiguration>().FipsEnabledInOrion().ToString().ToLowerInvariant() %>;
            if (isFipsEnabled) {
                $('#trVerifyUploadCommand').hide();
                $('#trVerifyBackupCommand').hide();
                $('#VerifyBackupHolder').hide();
                $('#VerifyUploadHolder').hide();
            } else {
                $('#trVerifyUploadCommand').show();
                $('#trVerifyBackupCommand').show();
                $('#VerifyBackupHolder').show();
                $('#VerifyUploadHolder').show();
            }
        });

        var toggleCommandCheckbox = function (checked, controlHolderId, validatorId) {
            if (checked) {
                $('#' + controlHolderId).show();
                ValidatorEnable($("[id$=_" + validatorId + "]")[0], true);
            }
            else {
                $('#' + controlHolderId).hide();
                ValidatorEnable($("[id$=_" + validatorId + "]")[0], false);
            }
        }

        var toggleCaptureResultsPatternCheckbox = function (checked, controlHolderId, fieldValidatorId, regExValidatorId, expectedValuValidatorId) {
            if (checked) {
                $('#' + controlHolderId).show();
                ValidatorEnable($("[id$=_" + fieldValidatorId + "]")[0], true);
                ValidatorEnable($("[id$=_" + regExValidatorId + "]")[0], true);
                if (expectedValuValidatorId) {
                    ValidatorEnable($("[id$=_" + expectedValuValidatorId + "]")[0], true);
                }
            }
            else {
                $('#' + controlHolderId).hide();
                ValidatorEnable($("[id$=_" + fieldValidatorId + "]")[0], false);
                ValidatorEnable($("[id$=_" + regExValidatorId + "]")[0], false);
                if (expectedValuValidatorId) {
                    ValidatorEnable($("[id$=_" + expectedValuValidatorId + "]")[0], false);
                }
            }
        }

        var toggleBackupOption = function (checked, controlHolderId) {
            if (checked && document.getElementById("<%=chkBackupFirmwareImageCommand.ClientID %>").checked) {
                $('#' + controlHolderId).show();
            }
            else {
                $('#' + controlHolderId).hide();
            }
        }

        var toggleOption = function (checked, controlHolderId) {
            if (checked) {
                $('#' + controlHolderId).show();
            }
            else {
                $('#' + controlHolderId).hide();
            }
        }

        var toggleReboot = function (checked, controlHolderId) {
            if (checked) {
                $('#' + controlHolderId).show();
            }
            else {
                $('#' + controlHolderId).hide();
            }
        }

        var toggleWarning = function (checked, controlHolderId) {
            if (!checked) {
                $('#' + controlHolderId).css('display', 'table-cell');
            }
            else {
                $('#' + controlHolderId).css('display', 'none');
            }
        }
    </script>
</asp:Content>

<asp:content id="Content3" contentplaceholderid="adminPageTitlePlaceHolder" runat="Server">
    <%=HtmlPageTitle %>
</asp:content>

<asp:content id="Content6" contentplaceholderid="adminHelpIconPlaceHolder" runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMAddFirmwareUpgradeTemplates" />
</asp:content>

<asp:content id="Content7" contentplaceholderid="adminContentPlaceholder" runat="Server">
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />

    <div style="padding:10px;">
        <div style="border:solid 1px #dcdbd7;width:90%;min-width:1350px;padding:10px;background-color:#fff;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="white-space:nowrap;width:10%;">
                        <%=Resources.NCMWebContent.AddEditFirmwareDefinition_Label_Name %>
                    </td>
                    <td style="width:90%;">
                        <asp:TextBox ID="txtName" runat="server" TextMode="SingleLine" Font-Size="12px" Font-Names="Arial" BorderColor="#999999" BorderWidth="1px" Width="30%" Height="18px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvName" runat="server" Display="Dynamic" ControlToValidate="txtName" ErrorMessage="*" />
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:10px;white-space:nowrap;width:10%;">
                        <%=Resources.NCMWebContent.AddEditFirmwareDefinition_Label_Description %>
                    </td>
                    <td style="padding-top:10px;width:90%;">
                        <asp:TextBox ID="txtDescription" runat="server" TextMode="SingleLine" Font-Size="12px" Font-Names="Arial" BorderColor="#999999" BorderWidth="1px" Width="60%" Height="18px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div style="padding:50px 0px 5px 0px;text-transform:uppercase;"><b><%=Resources.NCMWebContent.AddEditFirmwareDefinition_Label_CollectInfoCommands%></b></div>
                        <div style="border-bottom:solid 1px #dcdbd7;"></div>

                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td style="padding-top:10px;width:100%;" colspan="3">
                                    <asp:CheckBox ID="chkGetCurrentFirmwareImageCommand" runat="server" Text="<%$ Resources:NCMWebContent, AddEditFirmwareDefinition_Label_GetImageCommand %>" onclick="toggleCommandCheckbox(this.checked, 'GetCurrentFirmwareImageCommandHolder', 'rfvShowCurrentFirmwareImageCommand');" />
                                </td>
                            </tr>
                            <tr id="GetCurrentFirmwareImageCommandHolder">
                                <td style="padding-top:10px;padding-left:20px;width:40%;">
                                    <asp:TextBox ID="txtShowCurrentFirmwareImageCommand" runat="server" TextMode="SingleLine" Font-Size="12px" Font-Names="Arial" BorderColor="#999999" BorderWidth="1px" Width="100%" Height="18px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvShowCurrentFirmwareImageCommand" runat="server" Display="Dynamic" ControlToValidate="txtShowCurrentFirmwareImageCommand" ErrorMessage="*" />
                                </td>
                                <td style="padding-top:10px;padding-left:20px;padding-right:5px;white-space:nowrap;width:10%;">
                                    <asp:CheckBox ID="chkCaptureSysImageFilePattern" runat="server" Text="<%$ Resources:NCMWebContent, AddEditFirmwareDefinition_Checkbox_UseCaptureResultsPattern %>" onclick="toggleCaptureResultsPatternCheckbox(this.checked, 'CaptureSysImageFilePatternHolder', 'rfvCaptureSysImageFilePattern', 'revCaptureSysImageFilePattern');" />
                                </td>
                                <td style="padding-top:10px;width:50%;" id="CaptureSysImageFilePatternHolder">
                                    <asp:TextBox ID="txtCaptureSysImageFilePattern" runat="server" TextMode="SingleLine" Font-Size="12px" Font-Names="Arial" BorderColor="#999999" BorderWidth="1px" Width="60%" Height="18px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvCaptureSysImageFilePattern" runat="server" Display="Dynamic" ControlToValidate="txtCaptureSysImageFilePattern" ErrorMessage="*"/>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>
                                    <asp:RegularExpressionValidator ID="revCaptureSysImageFilePattern" runat="server" Display="Dynamic" ErrorMessage="<%$ Resources:NCMWebContent, AddEditFirmwareDefinition_ErrorMessage_PatternShouldContainMacro %>" ControlToValidate="txtCaptureSysImageFilePattern" ValidationExpression=".*\${CaptureData}.*" />
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top:10px;width:100%;" colspan="3">
                                    <asp:CheckBox ID="chkGetInfoOnConfigRegisterCommand" runat="server" Text="<%$ Resources:NCMWebContent, AddEditFirmwareDefinition_Label_ConfigRegisterCommand %>"  onclick="toggleCommandCheckbox(this.checked, 'GetInfoOnConfigRegisterCommandHolder', 'rfvShowVersionCommand'); "/>
                                </td>
                            </tr>
                            <tr id="GetInfoOnConfigRegisterCommandHolder">
                                <td style="padding-top:10px;padding-left:20px;white-space:nowrap;width:40%;">
                                    <asp:TextBox ID="txtShowVersionCommand" runat="server" TextMode="SingleLine" Font-Size="12px" Font-Names="Arial" BorderColor="#999999" BorderWidth="1px" Width="100%" Height="18px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvShowVersionCommand" runat="server" Display="Dynamic" ControlToValidate="txtShowVersionCommand" ErrorMessage="*"/>
                                </td>
                                <td style="padding-top:10px;padding-left:20px;padding-right:5px;width:10%;">
                                    <asp:CheckBox ID="chkCaptureConfigRegisterPattern" runat="server" Text="<%$ Resources:NCMWebContent, AddEditFirmwareDefinition_Checkbox_UseCaptureResultsPattern %>" onclick="toggleCaptureResultsPatternCheckbox(this.checked, 'CaptureConfigRegisterPatternHolder', 'rfvCaptureConfigRegisterPattern', 'revCaptureConfigRegisterPattern','rfvExpectedConfigRegisterValue');" />
                                </td>
                                <td style="padding-top:10px;width:50%;"id="CaptureConfigRegisterPatternHolder">
                                    <span style="padding-right:20px;">
                                        <asp:TextBox ID="txtCaptureConfigRegisterPattern" runat="server" TextMode="SingleLine" Font-Size="12px" Font-Names="Arial" BorderColor="#999999" BorderWidth="1px" Width="30%" Height="18px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvCaptureConfigRegisterPattern" runat="server" Display="Dynamic" ControlToValidate="txtCaptureConfigRegisterPattern" ErrorMessage="*" />                                        
                                    </span>
                                    <span>
                                        <asp:Label Text="<%$ Resources:NCMWebContent, AddEditFirmwareDefinition_Label_ExpectedValue %>" runat="server"></asp:Label>
                                        <asp:TextBox ID="txtExpectedConfigRegisterValue" runat="server" TextMode="SingleLine" Font-Size="12px" Font-Names="Arial" BorderColor="#999999" BorderWidth="1px" Width="10%" Height="18px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvExpectedConfigRegisterValue" runat="server" Display="Dynamic" ControlToValidate="txtExpectedConfigRegisterValue" ErrorMessage="*" />
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>
                                    <asp:RegularExpressionValidator ID="revCaptureConfigRegisterPattern" runat="server" Display="Dynamic" ErrorMessage="<%$ Resources:NCMWebContent, AddEditFirmwareDefinition_ErrorMessage_PatternShouldContainMacro %>" ControlToValidate="txtCaptureConfigRegisterPattern" ValidationExpression=".*\${CaptureData}.*" />
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top:10px;width:100%;" colspan="3">
                                    <asp:CheckBox ID="chkCollectBootVariableInfoCommand" runat="server" Text="<%$ Resources:NCMWebContent, AddEditFirmwareDefinition_Label_CollectBootVariableInfo %>"  onclick="toggleCommandCheckbox(this.checked, 'CollectBootVariableInfoCommandHolder', 'rfvCollectBootVariableInfoCommand');" />
                                </td>
                            </tr>
                            <tr id="CollectBootVariableInfoCommandHolder">
                                <td style="padding-top:10px;padding-left:20px;white-space:nowrap;width:40%;">
                                    <asp:TextBox ID="txtCollectBootVariableInfoCommand" runat="server" TextMode="SingleLine" Font-Size="12px" Font-Names="Arial" BorderColor="#999999" BorderWidth="1px" Width="100%" Height="18px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvCollectBootVariableInfoCommand" runat="server" Display="Dynamic" ControlToValidate="txtCollectBootVariableInfoCommand" ErrorMessage="*" />
                                </td>
                                <td style="padding-top:10px;width:60%;" colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding-top:10px;width:100%;" colspan="3">
                                    <asp:CheckBox ID="chkGetFreeSpaceCommand" runat="server" Text="<%$ Resources:NCMWebContent, AddEditFirmwareDefinition_Label_GetFreeSpaceCommand %>"  onclick="toggleCommandCheckbox(this.checked, 'GetFreeSpaceCommandHolder', 'rfvShowFreeSpaceCommand');" />
                                </td>
                            </tr>
                            <tr id="GetFreeSpaceCommandHolder">
                                <td style="padding-top:10px;padding-left:20px;white-space:nowrap;width:40%;">
                                    <asp:TextBox ID="txtShowFreeSpaceCommand" runat="server" TextMode="SingleLine" Font-Size="12px" Font-Names="Arial" BorderColor="#999999" BorderWidth="1px" Width="100%" Height="18px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvShowFreeSpaceCommand" runat="server" Display="Dynamic" ControlToValidate="txtShowFreeSpaceCommand" ErrorMessage="*" />
                                </td>
                                <td style="padding-top:10px;width:60%;" colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding-top:10px;width:100%;" colspan="3">
                                    <asp:CheckBox ID="chkOtherCommands" runat="server" Text="<%$Resources:NCMWebContent, AddEditFirmwareDefinition_Label_OtherCommands%>"  onclick="toggleCommandCheckbox(this.checked, 'OtherCommandsHolder', 'rfvOtherCommands');" />
                                </td>
                            </tr>
                            <tr id="OtherCommandsHolder">
                                <td style="padding-top:10px;padding-left:20px;white-space:nowrap;width:40%;">
                                    <asp:TextBox ID="txtOtherCommands" runat="server" TextMode="MultiLine" Font-Size="12px" Font-Names="Arial" BorderColor="#999999" BorderWidth="1px" Width="100%" Height="45px" style="resize:none;"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvOtherCommands" runat="server" Display="Dynamic" ControlToValidate="txtOtherCommands" ErrorMessage="*" style="vertical-align:top;" />
                                </td>
                                <td style="padding-top:10px;width:60%;" colspan="2">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div style="padding:50px 0px 5px 0px;text-transform:uppercase;"><b><%=Resources.NCMWebContent.AddEditFirmwareDefinition_Label_UpgradeCommands%></b></div>
                        <div style="border-bottom:solid 1px #dcdbd7;"></div> 

                        <table cellpadding="0" cellspacing="0" width="80%">
                            <tr>
                                <td style="padding-top:10px;white-space:nowrap;width:15%;">
                                    <%=Resources.NCMWebContent.AddEditFirmwareDefinition_Label_FirmwareImages%>
                                </td>
                                <td style="padding-top:10px;padding-left:10px;white-space:nowrap;width:85%;">
                                    <ncm:SelectFirmwareImages runat="server" ID="ncmSelectFirmwareImages" EnableValidateFirmwareImage="False" ReadOnly="False" />
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top:10px;white-space:nowrap;width:15%;">
                                    <%=Resources.NCMWebContent.AddEditFirmwareDefinition_Label_UpgradeImageCommand%>
                                </td>
                                <td style="padding-top:10px;padding-left:10px;white-space:nowrap;width:85%;">
                                    <asp:TextBox ID="txtUpgradeFirmwareImageCommand" runat="server" TextMode="SingleLine" Font-Size="12px" Font-Names="Arial" BorderColor="#999999" BorderWidth="1px" Width="100%" Height="18px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvUpgradeFirmwareImageCommand" runat="server" Display="Dynamic" ControlToValidate="txtUpgradeFirmwareImageCommand" ErrorMessage="*" />
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top:10px;white-space:nowrap;width:15%;">
                                    <%=Resources.NCMWebContent.AddEditFirmwareDefinition_Label_TransferProtocol%>
                                </td>
                                <td style="padding-top:10px;padding-left:10px;width:85%;">
                                    <asp:DropDownList ID="ddlTransferProtocol" runat="server" BorderColor="#999999" BorderWidth="1px"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top:30px;white-space:nowrap;width:15%;">
                                    <asp:CheckBox ID="chkDeleteFirmwareImageCommand" runat="server" Text="<%$Resources:NCMWebContent, AddEditFirmwareDefinition_Label_DeleteImageCommand%>"  onclick="toggleCommandCheckbox(this.checked, 'DeleteFirmwareImageCommandHolder', 'rfvDeleteFirmwareImageCommand');" />
                                </td>
                                <td id="DeleteFirmwareImageCommandHolder" style="padding-top:30px;padding-left:10px;white-space:nowrap;width:85%;">
                                    <asp:TextBox ID="txtDeleteFirmwareImageCommand" runat="server" TextMode="SingleLine" Font-Size="12px" Font-Names="Arial" BorderColor="#999999" BorderWidth="1px" Width="100%" Height="18px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvDeleteFirmwareImageCommand" runat="server" Display="Dynamic" ControlToValidate="txtDeleteFirmwareImageCommand" ErrorMessage="*" />
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top:10px;white-space:nowrap;width:15%;">
                                    <asp:CheckBox ID="chkBackupFirmwareImageCommand" runat="server" Text="<%$Resources:NCMWebContent, AddEditFirmwareDefinition_Label_BackupImageCommand%>"  onclick="toggleCommandCheckbox(this.checked, 'BackupFirmwareImageCommandHolder', 'rfvBackupFirmwareImageCommand'); toggleOption(this.checked, 'VerifyBackupHolder');" />
                                </td>
                                <td id="BackupFirmwareImageCommandHolder" style="padding-top:10px;padding-left:10px;white-space:nowrap;width:85%;">
                                    <asp:TextBox ID="txtBackupFirmwareImageCommand" runat="server" TextMode="SingleLine" Font-Size="12px" Font-Names="Arial" BorderColor="#999999" BorderWidth="1px" Width="100%" Height="18px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvBackupFirmwareImageCommand" runat="server" Display="Dynamic" ControlToValidate="txtBackupFirmwareImageCommand" ErrorMessage="*" />
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top:10px;white-space:nowrap;width:15%;">
                                    <asp:CheckBox ID="chkUpdateConfigRegisterCommand" runat="server" Text="<%$Resources:NCMWebContent, AddEditFirmwareDefinition_Label_UpdateConfigRegisterCommand%>"  onclick="toggleCommandCheckbox(this.checked, 'UpdateConfigRegisterCommandHolder', 'rfvUpdateConfigRegisterCommand');" />
                                </td>
                                <td id="UpdateConfigRegisterCommandHolder" style="padding-top:10px;padding-left:10px;white-space:nowrap;width:85%;">
                                    <asp:TextBox ID="txtUpdateConfigRegisterCommand" runat="server" TextMode="SingleLine" Font-Size="12px" Font-Names="Arial" BorderColor="#999999" BorderWidth="1px" Width="100%" Height="18px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvUpdateConfigRegisterCommand" runat="server" Display="Dynamic" ControlToValidate="txtUpdateConfigRegisterCommand" ErrorMessage="*" />
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top:10px;white-space:nowrap;width:15%;">
                                    <asp:CheckBox ID="chkUpdateBootVariableCommand" runat="server" Text="<%$Resources:NCMWebContent, AddEditFirmwareDefinition_Label_UpdateBootVariableCommand%>"  onclick="toggleCommandCheckbox(this.checked, 'UpdateBootVariableCommandHolder', 'rfvUpdateBootVariableCommand'); toggleOption(this.checked, 'UpdateBootVariableHolder');" />
                                </td>
                                <td id="UpdateBootVariableCommandHolder" style="padding-top:10px;padding-left:10px;white-space:nowrap;width:85%;">
                                    <asp:TextBox ID="txtUpdateBootVariableCommand" runat="server" TextMode="SingleLine" Font-Size="12px" Font-Names="Arial" BorderColor="#999999" BorderWidth="1px" Width="100%" Height="18px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvUpdateBootVariableCommand" runat="server" Display="Dynamic" ControlToValidate="txtUpdateBootVariableCommand" ErrorMessage="*" />
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top:10px;white-space:nowrap;width:15%;">
                                    <asp:CheckBox ID="chkRebootCommand" runat="server" Text="<%$Resources:NCMWebContent, AddEditFirmwareDefinition_Label_RebootCommand%>"  onclick="toggleCommandCheckbox(this.checked, 'RebootCommandHolder', 'rfvRebootCommand'); toggleOption(this.checked, 'RebootHolder');" />
                                </td>
                                <td id="RebootCommandHolder" style="padding-top:10px;padding-left:10px;white-space:nowrap;width:85%;">
                                    <asp:TextBox ID="txtRebootCommand" runat="server" TextMode="SingleLine" Font-Size="12px" Font-Names="Arial" BorderColor="#999999" BorderWidth="1px" Width="100%" Height="18px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvRebootCommand" runat="server" Display="Dynamic" ControlToValidate="txtRebootCommand" ErrorMessage="*" />
                                </td>
                            </tr>
                            <tr id="trVerifyUploadCommand">
                                <td style="padding-top:10px;white-space:nowrap;width:15%;">
                                    <asp:CheckBox ID="chkVerifyUploadCommand" runat="server" Text="<%$Resources:NCMWebContent, AddEditFirmwareDefinition_Label_VerifyUploadedImageCommand%>"  onclick="toggleCommandCheckbox(this.checked, 'VerifyUploadCommandHolder', 'rfvVerifyUploadCommand'); toggleOption(this.checked, 'VerifyUploadHolder');" />
                                </td>
                                <td id="VerifyUploadCommandHolder" style="padding-top:10px;padding-left:10px;white-space:nowrap;width:85%;">
                                    <asp:TextBox ID="txtVerifyUploadCommand" runat="server" TextMode="SingleLine" Font-Size="12px" Font-Names="Arial" BorderColor="#999999" BorderWidth="1px" Width="100%" Height="18px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvVerifyUploadCommand" runat="server" Display="Dynamic" ControlToValidate="txtVerifyUploadCommand" ErrorMessage="*" />
                                </td>
                            </tr>
                            <tr id="trVerifyBackupCommand">
                                <td style="padding-top:10px;white-space:nowrap;width:15%;">
                                    <asp:CheckBox ID="chkVerifyBackupCommand" runat="server" Text="<%$Resources:NCMWebContent, AddEditFirmwareDefinition_Label_VerifyBackupImageCommand%>"  onclick="toggleCommandCheckbox(this.checked, 'VerifyBackupCommandHolder', 'rfvVerifyBackupCommand'); toggleBackupOption(this.checked, 'VerifyBackupHolder');" />
                                </td>
                                <td id="VerifyBackupCommandHolder" style="padding-top:10px;padding-left:10px;white-space:nowrap;width:85%;">
                                    <asp:TextBox ID="txtVerifyBackupCommand" runat="server" TextMode="SingleLine" Font-Size="12px" Font-Names="Arial" BorderColor="#999999" BorderWidth="1px" Width="100%" Height="18px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvVerifyBackupCommand" runat="server" Display="Dynamic" ControlToValidate="txtVerifyBackupCommand" ErrorMessage="*" />
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top:10px;white-space:nowrap;width:15%;">
                                    <asp:CheckBox ID="chkVerifyUpgradeCommand" runat="server" Text="<%$Resources:NCMWebContent, AddEditFirmwareDefinition_Label_VerifyUpgradeCommand%>"  onclick="toggleCommandCheckbox(this.checked, 'VerifyUpgradeCommandHolder', 'rfvVerifyUpgradeCommand'); toggleOption(this.checked, 'VerifyUpgradeHolder');" />
                                </td>
                                <td id="VerifyUpgradeCommandHolder" style="padding-top:10px;padding-left:10px;white-space:nowrap;width:85%;">
                                    <asp:TextBox ID="txtVerifyUpgradeCommand" runat="server" TextMode="SingleLine" Font-Size="12px" Font-Names="Arial" BorderColor="#999999" BorderWidth="1px" Width="100%" Height="18px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvVerifyUpgradeCommand" runat="server" Display="Dynamic" ControlToValidate="txtVerifyUpgradeCommand" ErrorMessage="*" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div style="padding:50px 0px 5px 0px;text-transform:uppercase;"><b><%=Resources.NCMWebContent.AddEditFirmwareDefinition_Label_UpgradeOptions%></b></div>
                        <div style="border-bottom:solid 1px #dcdbd7;"></div>
                        
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="padding-top:10px;">
                                    <asp:CheckBox ID="chkSaveConfigToNVRAM" runat="server" Text="<%$Resources:NCMWebContent, AddEditFirmwareDefinition_Checkbox_SaveToNvRam%>"></asp:CheckBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top:10px;">
                                    <asp:CheckBox ID="chkBackupRunningAndStartupConfigsBeforeUpgrade" runat="server" Text="<%$Resources:NCMWebContent, AddEditFirmwareDefinition_Checkbox_BackupConfigsBeforeUpgrade%>"></asp:CheckBox>
                                </td>
                            </tr>
                            <tr id="VerifyBackupHolder">
                                <td style="padding-top:10px;">
                                    <asp:CheckBox ID="chkVerifyBackup" runat="server" Text="<%$Resources:NCMWebContent, AddEditFirmwareDefinition_Checkbox_VerifyBackupImage%>" onclick="toggleWarning(this.checked, 'verifyBackupImageWarning');"></asp:CheckBox>
                                    <div id="verifyBackupImageWarning" class="sw-suggestion sw-suggestion-warn" style="display:none; margin-top: 5px;">
                                        <div class="sw-suggestion-icon"></div>
                                        <span style="color:black;"><%=Resources.NCMWebContent.AddEditFirmwareDefinition_label_VerifyImageWarning%></span>
                                    </div>
                                </td>
                            </tr>
                            <tr id="VerifyUploadHolder">
                                <td style="padding-top:10px;">
                                    <asp:CheckBox ID="chkVerifyUpload" runat="server" Text="<%$Resources:NCMWebContent, AddEditFirmwareDefinition_Checkbox_VerifyUploadedImage%>" onclick="toggleWarning(this.checked, 'verifyUploadImageWarning');"></asp:CheckBox>
                                    <div id="verifyUploadImageWarning" class="sw-suggestion sw-suggestion-warn" style="display:none; margin-top: 5px;">
                                        <div class="sw-suggestion-icon"></div>
                                        <span style="color:black;"><%=Resources.NCMWebContent.AddEditFirmwareDefinition_label_VerifyImageWarning%></span>
                                    </div>
                                </td>
                            </tr>
                            <tr id="UpdateBootVariableHolder">
                                <td style="padding-top:10px;">
                                    <asp:CheckBox ID="chkUpdateBootVariable" runat="server" Text="<%$Resources:NCMWebContent, AddEditFirmwareDefinition_Checkbox_UpdateBootVariable%>"></asp:CheckBox>
                                </td>
                            </tr>
                            <tr id="RebootHolder">
                                <td style="padding-top:10px;">
                                    <asp:CheckBox ID="chkReboot" runat="server" Text="<%$Resources:NCMWebContent, AddEditFirmwareDefinition_Checkbox_RebootDevice%>" onclick="toggleReboot(this.checked, 'UnmanageHolder');"></asp:CheckBox>
                                    <div id="UnmanageHolder" style="padding-left:20px;padding-top:10px;">
                                        <asp:CheckBox ID="chkUnmanage" runat="server" Text="<%$Resources:NCMWebContent, AddEditFirmwareDefinition_Checkbox_UnmanageDevice%>"></asp:CheckBox>
                                        <div style="padding-top:10px;">
                                            <asp:CheckBox ID="chkRebootRequired" runat="server" Text="<%$Resources:NCMWebContent, AddEditFirmwareDefinition_Checkbox_ShowRebootWarning%>"></asp:CheckBox>
                                        </div>
                                        <table id="NodeUpCheckDelayAfterReboot" style="padding-top:10px;" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <orion:Slider runat="server" ID="slNodeUpCheckDelayAfterReboot" TableCellCount="3" Label="<%$ Resources:NCMWebContent, AddEditFirmwareDefinition_Label_DelayNodeUpCheckAfterReboot%>" RangeFrom="0"
                                                    RangeTo="10000" Unit="<%$ Resources:NCMWebContent, WEBDATA_VK_627 %>" ValidateRange="true"
                                                    Step="1" ErrorMessage="<%$ Resources:NCMWebContent, AddEditFirmwareDefinition_ErrorMessage_DelayNodeUpCheckAfterReboot %>" />
                                                <td>
                                                    <span class="SettingNote">
                                                        <%=string.Format(Resources.NCMWebContent.WEBDATA_VK_697, 0) %></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr id="VerifyUpgradeHolder">
                                <td style="padding-top:10px;">
                                    <asp:CheckBox ID="chkVerifyUpgrade" runat="server" Text="<%$Resources:NCMWebContent, AddEditFirmwareDefinition_Checkbox_VerifyUpgrade%>"></asp:CheckBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top:10px;">
                                    <asp:CheckBox ID="chkBackupRunningAndStartupConfigsAfterUpgrade" runat="server" Text="<%$Resources:NCMWebContent, AddEditFirmwareDefinition_Checkbox_BackupConfigsAfterUpgrade%>"></asp:CheckBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding-top:20px;">
	        <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Save" DisplayType="Primary" OnClick="btnSubmit_Click" />
	        <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" DisplayType="Secondary" OnClick="btnCancel_Click" CausesValidation="false" />
	    </div>
    </div>
</asp:content>