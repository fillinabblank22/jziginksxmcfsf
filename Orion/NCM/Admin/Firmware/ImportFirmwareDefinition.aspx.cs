﻿using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Common;
using System;
using System.IO;
using Resources;

public partial class Orion_NCM_Firmware_ImportFirmwareDefinition : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (!OrionConfiguration.IsDemoServer && (!Profile.AllowAdmin || !SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR)))
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={NCMWebContent.WEBDATA_AP_13}");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = NCMWebContent.ImportFirmwareDefinition_PageTitle;

        if (OrionConfiguration.IsDemoServer) 
            Submit.OnClientClick = @"demoAction('NCM_Firmware_Import_Firmware_Definition', this); return false;";
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (OrionConfiguration.IsDemoServer)
            return;

        if (fileUpload.HasFile)
        {
            try
            {
                using (var streamReader = new StreamReader(fileUpload.FileContent))
                {
                    var firmwareDefinition = FirmwareDefinition.Deserialize(streamReader.ReadToEnd());
                    
                    var isWrapper = new ISWrapper();
                    using (var proxy = isWrapper.Proxy)
                    {
                        proxy.Cirrus.FirmwareDefinitions.AddFirmwareDefinition(firmwareDefinition);
                    }
                    
                    Response.Redirect("/Orion/NCM/Admin/Firmware/FirmwareDefinitions.aspx");
                }
            }
            catch (Exception ex)
            {
                validator.ErrorMessage = ex.Message;
                validator.IsValid = false;
            }
        }
        else
        {
            validator.ErrorMessage = NCMWebContent.ImportFirmwareDefinition_ValidationError_BadFormat;
            validator.IsValid = false;
        }
    }

    protected void CancelClick(object sender, EventArgs e)
    {
        Response.Redirect("/Orion/NCM/Admin/Firmware/FirmwareDefinitions.aspx");
    }
}