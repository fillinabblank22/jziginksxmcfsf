﻿using SolarWinds.Logging;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Common;
using System;
using System.ComponentModel;
using System.Globalization;
using System.Web;
using System.Web.UI.WebControls;
using Resources;

public partial class Orion_NCM_Firmware_AddEditFirmwareDefinition : System.Web.UI.Page
{
    private const string DefaultConfigRegisterCommandExpectedValue = "0x2102";
    private static readonly Log _log = new Log();
    private ISWrapper _isWrapper = new ISWrapper();
    private bool _addMode = false;

    protected string HtmlPageTitle { get; set; }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (!OrionConfiguration.IsDemoServer && (!Profile.AllowAdmin || !SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR)))
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={NCMWebContent.WEBDATA_AP_13}");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        _addMode = string.IsNullOrEmpty(Page.Request.QueryString[@"ID"]);
        
        FirmwareDefinition firmwareDefinition = LoadFirmwareDefinition();

        if (_addMode)
        {
            Page.Title = NCMWebContent.AddEditFirmwareDefinition_PageTitle_AddNewDefinition;
            HtmlPageTitle = NCMWebContent.AddEditFirmwareDefinition_PageTitle_AddNewDefinition;
        }
        else
        {
            Page.Title =
                $@"{NCMWebContent.AddEditFirmwareDefinition_PageTitle_EditTemplateDefinition} {HttpUtility.HtmlEncode(firmwareDefinition.Name)}";
            string htmlTemplateName = $@"<i>{HttpUtility.HtmlEncode(firmwareDefinition.Name)}</i>";
            HtmlPageTitle =
                $@"{NCMWebContent.AddEditFirmwareDefinition_PageTitle_EditTemplateDefinition} {htmlTemplateName}";
        }

        if (OrionConfiguration.IsDemoServer)
            btnSubmit.OnClientClick = @"demoAction('NCM_Firmware_Import_Firmware_Definition', this); return false;";

        if (!Page.IsPostBack)
        {
            InitializeTransferProtocolDropDownList();

            txtName.Text = firmwareDefinition.Name;
            txtDescription.Text = firmwareDefinition.Description;

            LoadCommand(firmwareDefinition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.GetCurrentFirmwareImageCommand), chkGetCurrentFirmwareImageCommand, txtShowCurrentFirmwareImageCommand, chkCaptureSysImageFilePattern, txtCaptureSysImageFilePattern);
            LoadGetInfoOnConfigRegisterCommand(firmwareDefinition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.GetInfoOnConfigRegisterCommand));
            LoadCommand(firmwareDefinition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.CollectBootPathList), chkCollectBootVariableInfoCommand, txtCollectBootVariableInfoCommand);
            LoadCommand(firmwareDefinition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.GetFreeSpaceCommand), chkGetFreeSpaceCommand, txtShowFreeSpaceCommand);
            LoadCommand(firmwareDefinition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.OtherCommands), chkOtherCommands, txtOtherCommands);

            ncmSelectFirmwareImages.LoadFirmwareImages(firmwareDefinition.FirmwareImages);

            txtUpgradeFirmwareImageCommand.Text = firmwareDefinition.UpgradeFirmwareImageCommand;
            ddlTransferProtocol.SelectedValue = firmwareDefinition.TransferProtocol.ToString(@"D");

            LoadCommand(firmwareDefinition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.DeleteFirmwareImageCommand), chkDeleteFirmwareImageCommand, txtDeleteFirmwareImageCommand);
            LoadCommand(firmwareDefinition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.BackupFirmwareImageCommand), chkBackupFirmwareImageCommand, txtBackupFirmwareImageCommand);
            LoadCommand(firmwareDefinition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.UpdateConfigRegisterCommand), chkUpdateConfigRegisterCommand, txtUpdateConfigRegisterCommand);
            LoadCommand(firmwareDefinition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.UpdateBootVariableCommand), chkUpdateBootVariableCommand, txtUpdateBootVariableCommand);
            LoadCommand(firmwareDefinition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.RebootCommand), chkRebootCommand, txtRebootCommand);
            LoadCommand(firmwareDefinition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.VerifyUploadCommand), chkVerifyUploadCommand, txtVerifyUploadCommand);
            LoadCommand(firmwareDefinition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.VerifyBackupCommand), chkVerifyBackupCommand, txtVerifyBackupCommand);
            LoadCommand(firmwareDefinition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.VerifyUpgradeCommand), chkVerifyUpgradeCommand, txtVerifyUpgradeCommand);

            chkSaveConfigToNVRAM.Checked = firmwareDefinition.SaveConfigToNVRAM;
            chkBackupRunningAndStartupConfigsBeforeUpgrade.Checked = firmwareDefinition.BackupRunningAndStartupConfigsBeforeUpgrade;
            chkBackupRunningAndStartupConfigsAfterUpgrade.Checked = firmwareDefinition.BackupRunningAndStartupConfigsAfterUpgrade;
            chkUpdateBootVariable.Checked = firmwareDefinition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.UpdateBootVariableCommand) != null && firmwareDefinition.UpdateBootVariable;
            chkReboot.Checked = firmwareDefinition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.RebootCommand) != null && firmwareDefinition.Reboot;
            chkUnmanage.Checked = firmwareDefinition.Unmanage;
            chkRebootRequired.Checked = firmwareDefinition.RebootRequired;
            slNodeUpCheckDelayAfterReboot.Value = firmwareDefinition.NodeUpCheckDelayAfterReboot;
            chkVerifyUpload.Checked = firmwareDefinition.VerifyUploadedImageIntegrity;
            chkVerifyBackup.Checked = firmwareDefinition.VerifyBackedUpImageIntegrity;
            chkVerifyUpgrade.Checked = firmwareDefinition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.VerifyUpgradeCommand) != null && firmwareDefinition.VerifyUpgrade;
        }
    }

    private void LoadGetInfoOnConfigRegisterCommand(FirmwareUpgradeCommand captureConfigRegisterCommand)
    {
        if (captureConfigRegisterCommand?.CaptureResultsPattern != null && captureConfigRegisterCommand.ExpectedResultValue == null)
        {
            captureConfigRegisterCommand.ExpectedResultValue = DefaultConfigRegisterCommandExpectedValue;
        }

        LoadCommand(captureConfigRegisterCommand, chkGetInfoOnConfigRegisterCommand, txtShowVersionCommand,
            chkCaptureConfigRegisterPattern, txtCaptureConfigRegisterPattern, txtExpectedConfigRegisterValue);
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (OrionConfiguration.IsDemoServer)
            return;

        SaveFirmwareDefinition();
        Response.Redirect("/Orion/NCM/Admin/Firmware/FirmwareDefinitions.aspx");
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Orion/NCM/Admin/Firmware/FirmwareDefinitions.aspx");
    }

    private FirmwareDefinition LoadFirmwareDefinition()
    {
        if (_addMode)
            return new FirmwareDefinition();
        else
        {
            using (var proxy = _isWrapper.Proxy)
            {
                return proxy.Cirrus.FirmwareDefinitions.GetFirmwareDefinition(Convert.ToInt32(Page.Request.QueryString[@"ID"], CultureInfo.InvariantCulture));
            }
        }
    }

    private void SaveFirmwareDefinition()
    {
        FirmwareDefinition firmwareDefinition = LoadFirmwareDefinition();

        firmwareDefinition.Name = txtName.Text;
        firmwareDefinition.Description = txtDescription.Text;

        SaveCommand(firmwareDefinition, eFirmwareUpgradeCommandID.GetCurrentFirmwareImageCommand, chkGetCurrentFirmwareImageCommand, txtShowCurrentFirmwareImageCommand, chkCaptureSysImageFilePattern, txtCaptureSysImageFilePattern);
        SaveCommand(firmwareDefinition, eFirmwareUpgradeCommandID.GetInfoOnConfigRegisterCommand, chkGetInfoOnConfigRegisterCommand, txtShowVersionCommand, chkCaptureConfigRegisterPattern, txtCaptureConfigRegisterPattern, txtExpectedConfigRegisterValue);
        SaveCommand(firmwareDefinition, eFirmwareUpgradeCommandID.GetFreeSpaceCommand, chkGetFreeSpaceCommand, txtShowFreeSpaceCommand);
        SaveCommand(firmwareDefinition, eFirmwareUpgradeCommandID.CollectBootPathList, chkCollectBootVariableInfoCommand, txtCollectBootVariableInfoCommand);
        SaveCommand(firmwareDefinition, eFirmwareUpgradeCommandID.OtherCommands, chkOtherCommands, txtOtherCommands);

        firmwareDefinition.FirmwareImages = ncmSelectFirmwareImages.GetSelectedFirmwareImages();
        
        firmwareDefinition.UpgradeFirmwareImageCommand = txtUpgradeFirmwareImageCommand.Text;
        firmwareDefinition.TransferProtocol = (eCommandProtocol)Enum.Parse(typeof(eCommandProtocol), ddlTransferProtocol.SelectedValue);

        SaveCommand(firmwareDefinition, eFirmwareUpgradeCommandID.DeleteFirmwareImageCommand, chkDeleteFirmwareImageCommand, txtDeleteFirmwareImageCommand);
        SaveCommand(firmwareDefinition, eFirmwareUpgradeCommandID.BackupFirmwareImageCommand, chkBackupFirmwareImageCommand, txtBackupFirmwareImageCommand);
        SaveCommand(firmwareDefinition, eFirmwareUpgradeCommandID.UpdateConfigRegisterCommand, chkUpdateConfigRegisterCommand, txtUpdateConfigRegisterCommand);
        SaveCommand(firmwareDefinition, eFirmwareUpgradeCommandID.UpdateBootVariableCommand, chkUpdateBootVariableCommand, txtUpdateBootVariableCommand);
        SaveCommand(firmwareDefinition, eFirmwareUpgradeCommandID.RebootCommand, chkRebootCommand, txtRebootCommand);
        SaveCommand(firmwareDefinition, eFirmwareUpgradeCommandID.VerifyUploadCommand, chkVerifyUploadCommand, txtVerifyUploadCommand);
        SaveCommand(firmwareDefinition, eFirmwareUpgradeCommandID.VerifyBackupCommand, chkVerifyBackupCommand, txtVerifyBackupCommand);
        SaveCommand(firmwareDefinition, eFirmwareUpgradeCommandID.VerifyUpgradeCommand, chkVerifyUpgradeCommand, txtVerifyUpgradeCommand);

        firmwareDefinition.SaveConfigToNVRAM = chkSaveConfigToNVRAM.Checked;
        firmwareDefinition.BackupRunningAndStartupConfigsBeforeUpgrade = chkBackupRunningAndStartupConfigsBeforeUpgrade.Checked;
        firmwareDefinition.BackupRunningAndStartupConfigsAfterUpgrade = chkBackupRunningAndStartupConfigsAfterUpgrade.Checked;
        firmwareDefinition.UpdateBootVariable = firmwareDefinition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.UpdateBootVariableCommand) != null && chkUpdateBootVariable.Checked;
        firmwareDefinition.Reboot = firmwareDefinition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.RebootCommand) != null && chkReboot.Checked;
        firmwareDefinition.Unmanage = firmwareDefinition.Reboot && chkUnmanage.Checked;
        firmwareDefinition.RebootRequired = firmwareDefinition.Reboot && chkRebootRequired.Checked;
        firmwareDefinition.NodeUpCheckDelayAfterReboot = firmwareDefinition.Reboot ? slNodeUpCheckDelayAfterReboot.Value : 0;

        firmwareDefinition.VerifyUploadedImageIntegrity = firmwareDefinition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.VerifyUploadCommand) != null && chkVerifyUpload.Checked;
        firmwareDefinition.VerifyBackedUpImageIntegrity = firmwareDefinition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.BackupFirmwareImageCommand) != null && firmwareDefinition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.VerifyBackupCommand) != null && chkVerifyBackup.Checked;

        firmwareDefinition.VerifyUpgrade = firmwareDefinition.GetFirmwareUpgradeCommand(eFirmwareUpgradeCommandID.VerifyUpgradeCommand) != null && chkVerifyUpgrade.Checked;

        using (var proxy = _isWrapper.Proxy)
        {
            if (_addMode)
                proxy.Cirrus.FirmwareDefinitions.AddFirmwareDefinition(firmwareDefinition);
            else
                proxy.Cirrus.FirmwareDefinitions.UpdateFirmwareDefinition(firmwareDefinition);
        }
    }

    private void LoadCommand(FirmwareUpgradeCommand command, CheckBox commandCheckbox, TextBox commandTextbox, CheckBox captureResultsPatternCheckbox = null, TextBox captureResultsPatternTextbox = null, TextBox expectedValue = null)
    {
        if (command != null)
        {
            commandCheckbox.Checked = true;
            commandTextbox.Text = command.Command;

            if (captureResultsPatternCheckbox != null)
                captureResultsPatternCheckbox.Checked = !string.IsNullOrEmpty(command.CaptureResultsPattern);
            if (captureResultsPatternTextbox != null)
                captureResultsPatternTextbox.Text = !string.IsNullOrEmpty(command.CaptureResultsPattern) ? command.CaptureResultsPattern : string.Empty;
            if (expectedValue != null)
                expectedValue.Text = !string.IsNullOrEmpty(command.ExpectedResultValue) ? command.ExpectedResultValue : string.Empty;
        }
        else
        {
            ClearCommandFields(commandCheckbox, commandTextbox, captureResultsPatternCheckbox, captureResultsPatternTextbox);
        }
    }

    private static void ClearCommandFields(CheckBox commandCheckbox, TextBox commandTextbox,
        CheckBox captureResultsPatternCheckbox, TextBox captureResultsPatternTextbox)
    {
        commandCheckbox.Checked = false;
        commandTextbox.Text = string.Empty;

        if (captureResultsPatternCheckbox != null)
            captureResultsPatternCheckbox.Checked = false;
        if (captureResultsPatternTextbox != null)
            captureResultsPatternTextbox.Text = string.Empty;
    }

    private void SaveCommand(FirmwareDefinition firmwareDefinition, eFirmwareUpgradeCommandID commandId, CheckBox commandCheckbox, TextBox commandTextbox, CheckBox captureResultsPatternCheckbox = null, TextBox captureResultsPatternTextbox = null, TextBox expectedValue = null)
    {
        if (commandCheckbox.Checked)
        {
            FirmwareUpgradeCommand command;
            if (captureResultsPatternCheckbox != null && captureResultsPatternCheckbox.Checked)
                command = new FirmwareUpgradeCommand(commandTextbox.Text, captureResultsPatternTextbox.Text, expectedValue?.Text);
            else
                command = new FirmwareUpgradeCommand(commandTextbox.Text);

            firmwareDefinition.AddFirmwareUpgradeCommand(commandId, command);
        }
        else
        {
            firmwareDefinition.RemoveFirmwareUpgradeCommand(commandId);
        }
    }
    
    [Localizable(false)]
    private void InitializeTransferProtocolDropDownList()
    {
        ddlTransferProtocol.Items.Clear();
        ddlTransferProtocol.Items.Add(new ListItem("TFTP", eCommandProtocol.CommandProtocol_TFTP.ToString("D")));
        ddlTransferProtocol.Items.Add(new ListItem("SCP", eCommandProtocol.CommandProtocol_SCP.ToString("D")));
    }
}