﻿using SolarWinds.NCM.Common.Settings;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Web.UI;
using System;
using System.Web;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.NCM.Common.BusinessLayer.Interfaces;

public partial class Orion_NCM_Admin_Firmware_FirmwareStorageLocation : System.Web.UI.Page
{
    private readonly IIsWrapper isWrapper;
    private readonly IDataDecryptor dataDecryptor;

    private static readonly string SpanSuccess =
        @"<span class='SpanSuccess' automationid='lValidator'><span class='SpanSuccess-Icon'></span>";
    private static readonly string SpanFail =
        @"<span class='SpanException' automationid='lValidator'><span class='SpanException-Icon'></span>";
    private static readonly string SpanEndTag = @"</span>";

    public Orion_NCM_Admin_Firmware_FirmwareStorageLocation()
    {
        isWrapper = ServiceLocator.Container.Resolve<IIsWrapper>();
        dataDecryptor = ServiceLocator.Container.Resolve<IDataDecryptor>();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (!CommonHelper.IsDemoMode() && (!Profile.AllowAdmin || !SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR)))
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_AP_13}");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.FirmwareUpgradeStorageSettings_Title;

        if (!Page.IsPostBack)
        {
            using (var proxy = isWrapper.GetProxy())
            {
                txtLocation.Text = proxy.Cirrus.Settings.GetSetting(Settings.FirmwareStorageLocation, string.Empty, null);
                txtUserName.Text = dataDecryptor.Decrypt(proxy.Cirrus.Settings.GetSetting(Settings.FirmwareNetworkShareUserName, string.Empty, null));
                txtPassword.PasswordText = dataDecryptor.Decrypt(proxy.Cirrus.Settings.GetSetting(Settings.FirmwareNetworkShareUserPassword, string.Empty, null));
                slFirmwareOperations.Value = Convert.ToInt16(proxy.Cirrus.Settings.GetSetting(Settings.MaxNumberOfFirmwareOperations, 5, null));
            }
        }
    }

    protected void btnValidate_Click(object sender, EventArgs e)
    {
        if (CommonHelper.IsDemoMode())
        {
            return;
        }

        ValidateFirmwareStorageLocation();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (CommonHelper.IsDemoMode())
        {
            return;
        }

        if (!ValidateFirmwareStorageLocation())
            return;

        using (var proxy = isWrapper.GetProxy())
        {
            proxy.Cirrus.Settings.SaveSetting(Settings.FirmwareStorageLocation, txtLocation.Text, null);
            proxy.Cirrus.Settings.SaveSetting(Settings.FirmwareNetworkShareUserName, txtUserName.Text, null);
            proxy.Cirrus.Settings.SaveSetting(Settings.FirmwareNetworkShareUserPassword, txtPassword.PasswordText, null);
            proxy.Cirrus.Settings.SaveSetting(Settings.MaxNumberOfFirmwareOperations, slFirmwareOperations.Value, null);
        }

        var settings = ServiceLocator.Container.Resolve<INCMBLSettings>();
        var proxyHelper = ServiceLocator.Container.Resolve<INcmBusinessLayerProxyHelper>();
        proxyHelper.CallMain(p => p.StartScanFirmwareRepository(settings.FirmwareRepositoryFileExtensions.Split(';'), TimeSpan.FromMinutes(settings.FirmwareRepositoryScanTimeout), settings.FirmwareRepositoryFileWatcherEnabled));

        LocalizableButton btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        LocalizableButton btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }

    private void ShowValidationSuccess(string message)
    {
        lValidator.Text = string.Concat(SpanSuccess, HttpUtility.HtmlEncode(message), SpanEndTag);
    }

    private void ShowValidationError(string message)
    {
        lValidator.Text = string.Concat(SpanFail, HttpUtility.HtmlEncode(message), SpanEndTag);
    }

    private bool ValidateFirmwareStorageLocation()
    {
        try
        {
            using (var proxy = isWrapper.GetProxy())
            {
                var result = proxy.Cirrus.FirmwareStorage.ValidateFirmwareStorage(txtLocation.Text, txtUserName.Text, txtPassword.PasswordText);

                if (result.IsValid)
                {
                    ShowValidationSuccess(Resources.NCMWebContent.WEBDATA_VK_947);
                }
                else
                {
                    ShowValidationError(result.Message);
                }

                return result.IsValid;
            }
        }
        catch (Exception ex)
        {
            ShowValidationError(ex.Message);
            return false;
        }
    }
}