﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="FirmwareDefinitions.aspx.cs" Inherits="Orion_NCM_Firmware_FirmwareDefinitions" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/NCM/NavigationTabBar.ascx" TagPrefix="ncm" TagName="NavigationTabBar" %>
<%@ Register TagPrefix="ncm1" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />

    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/PolicyReports/PolicyReport.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />

    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Admin/Firmware/js/FirmwareDefinitions.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippetsHelper.js"></script>

    <ncm1:ExtLocalDateTimePattern ID="ExtLocalDateTimePattern1" runat="server"/> 

    <style type="text/css">
        #Grid td {
            padding: 0px 0px 0px 0px;
            border: 0px;
        }

        .author-icon-certified {
            background: url(/Orion/images/orion_generic_icon_orange.ico) no-repeat;
            padding-left: 20px;
            vertical-align: bottom;
        }

        .author-icon-normal {
            background: url(/Orion/images/users.gif) no-repeat;
            padding-left: 20px;
            vertical-align: bottom;
        }
    </style>

    <script type="text/javascript">
        SW.NCM.IsDemoServer = <%=SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer.ToString().ToLowerInvariant() %>;
        var thwackUserInfo = <%=_thwackUserInfo%>;
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title %>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMFirmwareUpgradeTemplates" />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <%foreach (string setting in new string[] { @"PageSize" })
      { %>
    <input type="hidden" name="NCM_FirmwareDefinitions_<%=setting%>" id="NCM_FirmwareDefinitions_<%=setting%>"
        value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get($@"NCM_FirmwareDefinitions_{setting}")%>' />
    <%}%>
    <asp:Panel id="ContentContainer" runat="server">
        <div style="padding:10px;">
            <ncm:NavigationTabBar ID="NavigationTabBar1" runat="server" />
            <div id="gridCell" class="tab-top">
                <div id="Grid" />
            </div>
        </div>
    </asp:Panel>
    <div id="divError" runat="server"/>
</asp:Content>