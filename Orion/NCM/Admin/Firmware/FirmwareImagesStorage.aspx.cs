﻿using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Common;
using System;
using System.Data;
using SolarWinds.NCM.Contracts.InformationService;

public partial class Orion_NCM_Firmware_ImagesStorage : System.Web.UI.Page
{
    private bool IsStorageLocationSet
    {
        get
        {
            try
            {
                var isWrapper = new ISWrapper();
                using (var proxy = isWrapper.Proxy)
                {
                    var isSet = proxy.Cirrus.Settings.GetSetting(Settings.FirmwareStorageLocation, null, null);
                    return !string.IsNullOrEmpty(isSet);
                }

            }
            catch { return false; }
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (!OrionConfiguration.IsDemoServer && (!Profile.AllowAdmin || !SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR)))
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_AP_13}");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.FirmwareRepository_PageTitle;
        ContentContainer.Visible = IsStorageLocationSet;
        StorageLocationBox.Visible = !IsStorageLocationSet;

    }

    private void InitFields(bool isNew)
    {
        if (!isNew) //TODO
        {

        }

        ddlMachineTypes.DataSource = GetAllMachineTypesList();
        ddlMachineTypes.DataValueField = @"MachineType";
        ddlMachineTypes.DataTextField = @"MachineType";
        ddlMachineTypes.DataBind();
    }

    private DataTable GetAllMachineTypesList()
    {
        string swql = @"SELECT DISTINCT MachineType FROM Orion.Nodes";
        var isWrapper = new ISWrapper();
        using (var proxy = isWrapper.Proxy)
        {
            return proxy.Query(swql);
        }
    }
}