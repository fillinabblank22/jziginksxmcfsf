﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="FirmwareStorageLocation.aspx.cs" Inherits="Orion_NCM_Admin_Firmware_FirmwareStorageLocation" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register Src="~/Orion/Controls/Slider.ascx" TagPrefix="orion" TagName="Slider" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
    <orion:Include ID="Include1" runat="server" File="Admin.css" />
    <orion:Include ID="Include2" runat="server" File="Discovery.css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/Settings/Settings.css" />

    <style type="text/css">
        tr.slider > td
        {
            text-align:left;
            white-space:nowrap;
            padding:10px;
            background-color:#e4f1f8;
        }
        .SpanException
        {
            background: #facece;
            color: #ce0000;
            border: 1px solid #ce0000;
            border-radius: 5px;
            font-size:10pt;
            font-family: Arial,Verdana,Helvetica,sans-serif;
            display: inline-block; 
            position: relative;
            padding: 5px 6px 6px 26px; 
        }

        .SpanException-Icon 
        {
            position: absolute;
            left: 4px; 
            top: 4px; 
            height: 16px; 
            width: 16px; 
            background: url(/Orion/NCM/Resources/images/icon_failed.gif) top left no-repeat;
        }

        .SpanSuccess {
            background: #daf7cc;
            color: #00a000;
            border: 1px solid #00a000;
            border-radius: 5px;
            font-size: 10pt;
            font-family: Arial,Verdana,Helvetica,sans-serif;
            display: inline-block;
            position: relative;
            padding: 5px 6px 6px 26px;
        }
        
        .SpanSuccess-Icon
        {
            position: absolute;
            left: 4px;
            top: 4px;
            height: 16px;
            width: 16px;
            background: url(/Orion/NCM/Resources/images/icon_ok.gif) top left no-repeat;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMFimwareStorageLocation" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <script language="javascript" type="text/javascript">    
	    var isDemoMode = <%=SolarWinds.NCMModule.Web.Resources.CommonHelper.IsDemoMode().ToString().ToLowerInvariant() %>;

	    function checkSubmitDemoMode() {
	        if (isDemoMode) {
	            demoAction("NCM_FirmwareStorageLocation_Submit", this);
	            return false;
	        }
	        return true;
	    }

	    function checkValidateDemoMode() {
            if (isDemoMode) {
                demoAction("NCM_FirmwareStorageLocation_Validate", this);
                return false;
            }
            return true;
        }
    </script>

    <asp:Panel ID="ContentContainer" CssClass="ContentContainer" runat="server">
        <div class="SettingContainer">
            <div class="SettingBlockHeader"><%=Resources.NCMWebContent.FirmwareUpgradeStorageSettings_StorageLocationSettings%></div>
            
            <table cellpadding="0" cellspacing="0" width="80%">
                <tr>
                    <td style="padding-top:10px;" colspan="2">
                        <div style="padding-bottom:5px;"><%=Resources.NCMWebContent.FirmwareUpgradeStorageSettings_PathSettingLabel%></div>
                        <asp:TextBox ID="txtLocation" runat="server" TextMode="SingleLine" CssClass="SettingTextField" Width="60%" automationId="txtLocation"/>
                        <asp:RequiredFieldValidator ID="rfvLocation" runat="server" ControlToValidate="txtLocation" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                    <tr>
                    <td style="padding-top:20px;padding-right:5px;" colspan="2">
                        <%=Resources.NCMWebContent.WEBDATA_VK_1047%>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:10px;padding-right:5px;width:5%;white-space:nowrap;">
                        <%=Resources.NCMWebContent.WEBDATA_VK_1048%>
                    </td>
                    <td style="padding-top:10px;width:95%;">
                        <asp:TextBox ID="txtUserName" autocomplete="off" runat="server" CssClass="SettingTextField" Width="20%" automationId="txtUserName"/>
                        <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ControlToValidate="txtUserName" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="padding-right:5px;width:5%;">&nbsp;</td>
                    <td colspan="2" style="color:#5F5F5F;font-size:10px;width:95%;">
                        <%=Resources.NCMWebContent.WEBDATA_VK_1049%>
                    </td>
                </tr>
                <tr>
                    <td style="padding-right:5px;padding-top:5px;width:5%;white-space:nowrap;">
                         <%=Resources.NCMWebContent.WEBDATA_VK_297%>
                    </td>
                    <td style="padding-top:5px;width:95%;">
                        <ncm:PasswordTextBox ID="txtPassword" TextMode="Password" runat="server" CssClass="SettingTextField" Width="20%" automationId="txtPassword"></ncm:PasswordTextBox>
                        <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="padding-right:5px;width:5%;">&nbsp;</td>
                    <td style="padding-top:10px;width:95%;">
                        <orion:LocalizableButton runat="server" ID="btnValidate" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_590 %>" OnClientClick="return checkValidateDemoMode();" OnClick="btnValidate_Click" CausesValidation="true" automationId="btnValidate" />
                    </td>
                </tr>
                <tr>
                    <td style="padding-right:5px;width:5%;">&nbsp;</td>
                    <td colspan="2" style="color:#5F5F5F;font-size:10px;width:95%;">
                        <%=Resources.NCMWebContent.FirmwareUpgradeSettings_Label_ValidateCredentials%>
                    </td>
                </tr>
            </table>
            <div style="padding-top:10px;">
                <asp:Literal ID="lValidator" runat="server"></asp:Literal>
            </div>
        </div>
        
        <div style="padding-top:20px;">
            <div class="SettingContainer" style="padding-top:20px;">
                <div class="SettingBlockHeader">
                    <%= Resources.NCMWebContent.FirmwareUpgradeSettings_Label_UpgradeSettings %>
                </div>
                <table cellpadding="0" cellspacing="0" style="padding-top: 20px">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0">
                                <tr class="slider">
                                    <orion:Slider runat="server" ID="slFirmwareOperations" 
                                                  TableCellCount="3" 
                                                  Label="<%$ Resources: NCMWebContent, FirmwareUpgradeSettings_Slider_MaxNumberOfOperations %>" 
                                                  RangeFrom="1" 
                                                  RangeTo="100" Unit="<%$ Resources: NCMWebContent, FirmwareUpgradeSettings_Slider_Units %>" 
                                                  ValidateRange="true" Step="1" 
                                                  ErrorMessage="<%$ Resources: NCMWebContent, FirmwareUpgradeSettings_Slider_Error %>" />
                                    <td>
                                        <span class="SettingNote">
                                            <%= Resources.NCMWebContent.FirmwareUpgradeSettings_Slider_Defaults %>
                                        </span>
                                    </td>
                            
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="color:#5F5F5F;background-color:#e4f1f8;font-size:10px;padding-left:10px;padding-bottom:10px">
                            <%= Resources.NCMWebContent.FirmwareUpgradeSettings_Label_PollerResources %>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div style="padding-top:20px;">
            <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" OnClick="btnSubmit_Click" OnClientClick="return checkSubmitDemoMode();" DisplayType="Primary" CommandArgument="~/Orion/NCM/Admin/Default.aspx" CausesValidation="true" automationId="btnSubmit"/>
            <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" OnClick="btnCancel_Click" DisplayType="Secondary" CommandArgument="~/Orion/NCM/Admin/Default.aspx" CausesValidation="false" automationId="btnCancel"/>
        </div>
    </asp:Panel>
</asp:Content>

