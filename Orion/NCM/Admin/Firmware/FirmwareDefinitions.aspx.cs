﻿using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Common;
using System;
using Resources;

public partial class Orion_NCM_Firmware_FirmwareDefinitions : System.Web.UI.Page
{
    protected string _thwackUserInfo = @"{name:'',pass:'', valid: false}";
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (!OrionConfiguration.IsDemoServer && (!Profile.AllowAdmin || !SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR)))
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={NCMWebContent.WEBDATA_AP_13}");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = NCMWebContent.FirmwareDefinition_PageTitle;

        SWISValidator validator = new SWISValidator();
        validator.ContentContainer = ContentContainer;
        validator.RedirectIfValidationFailed = true;
        divError.Controls.Add(validator);
    }
}