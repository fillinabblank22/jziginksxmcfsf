﻿Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.QuickTips.init();

var newVariable = SW.NCM.AddEditFirmwareImage = function () {

    var onSaveCallback;
    var imageId;

    function errorHandler(result) {
        if (result != null) {
            Ext.Msg.show({
                title: result.Source,
                msg: result.Msg,
                minWidth: 500,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        }
    }

    //Init drop down list #ddlMachineTypes
    var initMachineTypeDropDownList = function (id) {
        ORION.callWebService("/Orion/NCM/Services/FirmwareImages.asmx",
            "GetAllMachineTypesList", { imageId: id },
            function (result) {
                if (result !== null) {
                    var ddl = $('#ddlMachineTypes');
                    ddl.empty();
                    var list = result.Rows;
                    $.each(list, function (key, value) {
                        ddl.append($('<option></option>').attr("value", value[0]).text(value[0]));
                    });
                }
            });
    }

    //init bullet list #MachineTypeList
    var initMachineTypesList = function (id) {
        var listItems = $("#MachineTypeList");

        ORION.callWebService("/Orion/NCM/Services/FirmwareImages.asmx",
            "GetMachineTypes", { imageId: id },
            function (result) {
                if (result !== null) {
                    listItems.empty();
                    var items = JSON.parse(result);
                    items.forEach(appendMachineTypeList);
                }
            });
    }

    var appendMachineTypeList = function (item) {
        $("#MachineTypeList").append($("<tr style='height:25px;'>")
            .append("<td class='td-node'>&#8226;</td>")
            .append($("<td class='td-node'>")
                .append($("<label>").text(item))
            )
            .append($("<td class='td-node'>")
                .append($("<img>")
                    .attr("src", "/Orion/NCM/Resources/images/icon_delete.png")
                    .attr("align", "right")
                    .attr("style", "cursor:pointer;")
                    .attr("machineType", item).click(function () { removeMachineTypeFromList($(this), item); })
                )
            )
        );
    }

    var initImageFields = function (id) {
        ORION.callWebService("/Orion/NCM/Services/FirmwareImages.asmx",
            "GetFirmareImage", { imageId: id },
            function (result) {
                if (result !== null) {
                    var item = JSON.parse(result)[0];
                    $("#lblFileName").text(item["FileName"]);
                    $("#lblRelativePath").text(item["RelativePath"]);
                    $("#txtDescription").val(item["Description"]);
                }
            });
    }

    //removes item from #ddlMachineTypes
    var removeMachineTypeFromDropDown = function (item) {
        var dropdownElement = $("#ddlMachineTypes");
        var valueToFind = String.format("option[value=\"{0}\"]", item);
        dropdownElement.find(valueToFind).remove();
    }

    //removes item from #MachineTypeList and add to #ddlMachineTypes
    var removeMachineTypeFromList = function (obj, item) {
        obj.closest("tr").remove();
        var dropdownElement = $("#ddlMachineTypes");
        dropdownElement.append(new Option(item, item));
    }

    var SaveImage = function () {
        var description = $("#txtDescription").val();
        var machineTypes = getMachineTypes();

        ORION.callWebService("/Orion/NCM/Services/FirmwareImages.asmx",
            "SaveImage", { imageId: imageId, description: description, machineTypes: machineTypes },
            function (result) {
                if (result == null) {
                    showAddEditImageDialog.hide();
                    onSaveCallback();
                }
                else
                    errorHandler(result);
            });
    }

    var getMachineTypes = function () {
        var listOfTypes = [];
        $("#MachineTypeList").find("td [machineType]").each(function () {
            var keyValue = this.getAttribute("machineType");
            listOfTypes.push(keyValue);
        });
        return listOfTypes;
    };

    var cleanFields = function () {
        $("#lblFileName").text("");
        $("#lblRelativePath").text("");
        $("#txtDescription").val("");
        $("#MachineTypeList").empty();
        $("#ddlMachineTypes").empty();
    }

    var showAddEditImageDialog;
    var ShowAddEditImageDialog = function () {

        if (!showAddEditImageDialog) {
            showAddEditImageDialog = new Ext.Window({
                applyTo: 'firmwareImageDialog',
                layout: 'fit',
                width: 600,
                height: 525,
                boxMinWidth: 600,
                boxMinHeight: 500,
                boxMaxWidth: 600,
                boxMaxHeight: 800,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                modal: true,
                contentEl: 'firmwareImageBody',
                buttonAlign: 'left',
                buttons: [
                    '->', {
                        id: 'SaveFirmwareImage',
                        text: '@{R=NCM.Strings;K=WEBJS_VK_188;E=js}',
                        cls: 'sw-btn-primary',
                        handler: function () {
                            if (SW.NCM.IsDemoServer) {
                                demoAction('NCM_SaveFirmwareImage', $('#SaveFirmwareImage'));
                            }
                            else {
                                SaveImage();
                            }
                        }
                    }, {
                        text: '@{R=NCM.Strings;K=WEBJS_VK_07;E=js}',
                        handler: function () {
                            showAddEditImageDialog.hide();
                        }
                    }
                ]
            });
        }

        showAddEditImageDialog.setTitle(Ext.util.Format.htmlEncode("@{R=NCM.Strings;K=FirmwareUpgrade_EditFirmwareImage_PopupTitle;E=js}"));
        showAddEditImageDialog.alignTo(document.body, "c-c");
        showAddEditImageDialog.show();

        return false;
    }

    return {
        showAddEditImageWindow: function (id, callback) {
            imageId = id;
            onSaveCallback = callback;

            cleanFields();
            initMachineTypeDropDownList(id);
            initImageFields(id);
            initMachineTypesList(id);

            ShowAddEditImageDialog();
        },

        //Add item to #MachineTypeList and removes from #ddlMachineTypes
        addItemToMachineTypeList: function (item) {
            if (item != null) {
                appendMachineTypeList(item);
                removeMachineTypeFromDropDown(item);
            }
        },

        alignAddEditImageWindow: function () {
            if (showAddEditImageDialog != null) {
                showAddEditImageDialog.alignTo(Ext.getBody(), "c-c");
            }
        }
    }
}();