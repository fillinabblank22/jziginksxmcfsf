﻿Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.QuickTips.init();

SW.NCM.FirmwareDefinitions = function () {

    ORION.prefix = "NCM_FirmwareDefinitions_";

    function gridResize() {
        if (grid != null) {
            grid.setWidth(1);
            grid.setWidth($('#gridCell').width());

            grid.setHeight(getGridHeight());
        }
    }

    function getGridHeight() {
        var top = $('#gridCell').offset().top;
        return $(window).height() - top - $('#footer').height() - 100;
    }

    var getDateTimeOffset = function () {
        var date = new Date();
        var offset = date.getTimezoneOffset();
        return offset;
    }

    var currentSearchTerm = null;
    var refreshObjects = function (search, callback) {
        grid.store.removeAll();
        var pagingToolbar = grid.getBottomToolbar();
        pagingToolbar.cursor = 0;
        grid.store.baseParams['limit'] = grid.getBottomToolbar().pageSize;
        grid.store.proxy.conn.jsonData = { search: search, clientOffset: getDateTimeOffset() };
        currentSearchTerm = search;
        pagingToolbar.doLoad(pagingToolbar.cursor);
    }

    var reloadGridStore = function () {
        var currentPageRecordCount = grid.store.getCount();
        var selectedRecordCount = grid.getSelectionModel().getCount();
        if (currentPageRecordCount > selectedRecordCount) {
            grid.store.reload();
        }
        else {
            grid.store.load();
        }
    }

    function renderName(value, meta, record) {
        return String.format('<a href="#" style="font-weight:bold;" class="firmwareDefinitionName" id="{0}" canned="{1}">{2}</a>', record.data.ID, record.data.Canned, Ext.util.Format.htmlEncode(value));
    }

    function renderDateTime(value, meta, record) {
        if (value == null) return '';

        var date = eval(value.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
        return Ext.util.Format.date(date, ExtDaTimeLocalePattern.DateFull);
    }

    function renderAuthor(value, meta, record) {
        if (!value || 0 === value.length)
            return '<span></span>';
        else if (record.data.Canned)
            return '<span class="author-icon-certified">' + value + '</span>';
        else
            return '<span class="author-icon-normal">' + value + '</span>';
    }

    var updateToolbarButtons = function () {
        var selCount = grid.getSelectionModel().getCount();
        var selItems = grid.getSelectionModel().getSelections();
        var map = grid.getTopToolbar().items.map;

        var needsOnlyOneSelected = (selCount != 1);
        var needsAtLeastOneSelected = (getSelectedIds(selItems).length === 0);

        map.Edit.setDisabled(needsOnlyOneSelected);
        map.Export.setDisabled(needsOnlyOneSelected);
        map.Upload.setDisabled(needsOnlyOneSelected);
        map.Delete.setDisabled(needsAtLeastOneSelected);

        var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');
        if (grid.getStore().getCount() > 0 && selCount == grid.getStore().getCount()) {
            hd.addClass('x-grid3-hd-checker-on');
        } else {
            hd.removeClass('x-grid3-hd-checker-on');
        }
    }

    var addNewFirmwareDefinition = function () {
        window.location = "/Orion/NCM/Admin/Firmware/AddEditFirmwareDefinition.aspx";
    }

    var editFirmwareDefinition = function (item) {
        if (item.data.Canned) {
            showOutOfTheBoxWindow();
        }
        else {
            redirectToEdit(item.data.ID);
        }
    }

    var redirectToEdit = function (id) {
        window.location = "/Orion/NCM/Admin/Firmware/AddEditFirmwareDefinition.aspx?ID=" + id;
    }

    var showOutOfTheBoxWindow = function () {
        var html = '\
        <div style="padding:10px;"> \
            <div> \
                @{R=NCM.Strings;K=WEBJS_VK_334;E=js} \
            </div> \
            <br/><br/> \
            <span>@{R=NCM.Strings;K=WEBJS_VK_335;E=js}</span><br/> \
            <label id="lblOrigionalName" style="font-weight:bold;"></label><br/><br/> \
            <div>@{R=NCM.Strings;K=WEBJS_VK_336;E=js}</div> \
            <input id="txtNewName" type="text" style="width:250px;"></input> \
        </div>';

        var item = grid.getSelectionModel().getSelected();

        var window = new Ext.Window({
            id: 'ootbWindow',
            layout: 'fit',
            width: 600,
            autoHeight: true,
            closeAction: 'close',
            title: '@{R=NCM.Strings;K=WEBJS_VK_332;E=js}',
            modal: true,
            resizable: false,
            stateful: false,
            html: html,
            buttons: [{
                text: '@{R=NCM.Strings;K=WEBJS_VK_333;E=js}',
                handler: function () {
                    if (!SW.NCM.IsDemoServer) {
                        var id = item.data.ID;
                        var newName = $("#txtNewName").val();

                        ORION.callWebService("/Orion/NCM/Services/FirmwareDefinitions.asmx",
                           "DuplicateFirmwareDefinition", { id: id, newName: newName },
                           function (newId) {
                               if (newId > 0)
                                   redirectToEdit(newId);
                           });
                    }
                    else
                        demoAction('NCM_FirmwareDefinitions_Duplicate', this);
                }
            }, {
                text: '@{R=NCM.Strings;K=WEBJS_VK_38;E=js}',
                handler: function () {
                    window.close();
                }
            }],
            listeners: {
                show: function (that) {
                    $("#lblOrigionalName").text(item.data.Name);
                    $("#txtNewName").val(String.format("@{R=NCM.Strings;K=WEBJS_VK_331;E=js}", item.data.Name));

                    that.alignTo(Ext.getBody(), "c-c");
                }
            }

        }).show();
    }

    var deleteFirmwareDefinitions = function (items) {
        var ids = getSelectedIds(items);
        if (ids.length > 0) {
            var waitMsg = Ext.Msg.wait('Delete firmware upgrade templates...');

            internalDeleteFirmwareDefinitions(ids, function (result) {
                waitMsg.hide();
                errorHandler(result);
                reloadGridStore();
            });
        }
    }

    var internalDeleteFirmwareDefinitions = function (ids, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/FirmwareDefinitions.asmx",
        "DeleteFirmwareDefinitions", { ids: ids },
        function (result) {
            onSuccess(result);
        });
    };

    var importFirmwareDefinition = function () {
        window.location = "/Orion/NCM/Admin/Firmware/ImportFirmwareDefinition.aspx";
    }

    var exportFirmwareDefinitionAsFile = function (item) {
        $.download('/Orion/NCM/Admin/Firmware/DownloadFirmwareDefinition.ashx', 'ID=' + item.data.ID);
    }

    function firmwareDefinitionNameClick(obj) {
        var canned = JSON.parse(obj.attr("canned"));
        if (canned) {
            showOutOfTheBoxWindow();
        }
        else {
            var id = obj.attr("id");
            redirectToEdit(id);
        }
    }

    var getSelectedIds = function (items) {
        var ids = [];

        Ext.each(items, function (item) {
            if (!item.data.Canned)
                ids.push(item.data.ID);
        });

        return ids;
    }

    var pagingToolbarChangeHandler = function (that) {
        if (that.displayItem) {
            var count = 0;
            that.store.each(function (record) {
                count++;
            });
            var msg = count == 0 ? that.emptyMsg : String.format(that.displayMsg, that.cursor + 1, that.cursor + count, that.store.getTotalCount());
            that.displayItem.setText(msg);
        }
    }

    var doClean = function () {
        var map = grid.getTopToolbar().items.map;
        map.Clean.setVisible(false);
        map.txtSearch.setValue('');
        refreshObjects("");
    }

    var doSearch = function () {
        var map = grid.getTopToolbar().items.map;
        var search = map.txtSearch.getValue();
        if ($.trim(search).length == 0) return;
        refreshObjects(search);
        map.Clean.setVisible(true);
    }

    function errorHandler(result) {
        if (result != null && result.Error) {
            Ext.Msg.show({
                title: result.Source,
                msg: result.Msg,
                minWidth: 500,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        }
    }

    var uploadFirmwareDefinition = function (ids, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/ThwackFirmwareDefinitions.asmx",
                             "UploadFirmwareDefinitionsToTwack", { ids: ids },
                             function (result) {
                                 onSuccess(result);
                             });
    };

    var uploadSelectedFirmwareDefinitionsToThwack = function (items) {
        var selectedIds = new Array();

        Ext.each(items, function (item) {
            selectedIds.push(item.data.ID);
        });

        var waitMsg = Ext.Msg.wait("@{R=NCM.Strings;K=WEBJS_AP_02;E=js}");

        uploadFirmwareDefinition(selectedIds, function (result) {
            waitMsg.hide();

            var obj = Ext.decode(result);
            if (!obj.isSuccess) {
                obj.msg = String.format("<span style='color:#f00;'>{0}</span>", obj.msg);
                SW.NCM.validateThwackCred(false);
            } else {
                SW.NCM.validateThwackCred(true);
            }

            function onClose() { dlg.hide(); dlg.destroy(); dlg = null; }
            var dlg = new Ext.Window({
                title: "@{R=NCM.Strings;K=WEBJS_AP_03;E=js}", html: obj.msg,
                width: 400, autoHeight: true, border: false, region: "center", layout: "fit", modal: true, resizable: false, plain: true, bodyStyle: "padding:5px 0px 0px 10px;",
                buttons: [{ text: "@{R=NCM.Strings;K=WEBJS_VK_07;E=js}", handler: onClose }]
            });
            dlg.show();
        });
    };

    var selectorModel;
    var dataStore;
    var grid;
    var pagingToolbar;

    return {
        init: function () {

            if ($("[id$='_ContentContainer']").length == 0)
                return false;

            $("#Grid").click(function (e) {

                var obj = $(e.target);

                if (obj.hasClass('firmwareDefinitionName')) {
                    firmwareDefinitionNameClick(obj);
                    return false;
                }
            });

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", updateToolbarButtons);

            dataStore = new ORION.WebServiceStore(
                                "/Orion/NCM/Services/FirmwareDefinitions.asmx/GetFirmwareDefinitions",
                                [
                                    { name: 'ID', mapping: 0 },
                                    { name: 'Name', mapping: 1 },
                                    { name: 'LastModified', mapping: 2 },
                                    { name: 'Description', mapping: 3 },
                                    { name: 'Author', mapping: 4 },
                                    { name: 'Canned', mapping: 5 }
                                ],
                                "Name");

            var pageSize = parseInt(ORION.Prefs.load('PageSize', '25'));

            var pageSizeBox = new Ext.form.NumberField({
                id: 'pageSizeBox',
                enableKeyEvents: true,
                allowNegative: false,
                width: 30,
                allowBlank: false,
                minValue: 1,
                maxValue: 100,
                value: pageSize,
                listeners: {
                    scope: this,
                    'keydown': function (f, e) {
                        var k = e.getKey();
                        if (k == e.RETURN) {
                            e.stopEvent();
                            var value = parseInt(f.getValue());
                            if (!isNaN(value) && value > 0 && value <= 100) {
                                pageSize = value;
                                var pagingToolbar = grid.getBottomToolbar();
                                if (pagingToolbar.pageSize != pageSize) { // update page size only if it is different
                                    ORION.Prefs.save('PageSize', pageSize);
                                    pagingToolbar.pageSize = pageSize;
                                    pagingToolbar.doLoad(0);
                                }
                            }
                            else {
                                return;
                            }
                        }
                    },
                    'focus': function (field) {
                        field.el.dom.select();
                    },
                    'change': function (f, numbox, o) {
                        var value = parseInt(f.getValue());
                        if (!isNaN(value) && value > 0 && value <= 100) {
                            pageSize = value;
                            var pagingToolbar = grid.getBottomToolbar();
                            if (pagingToolbar.pageSize != pageSize) { // update page size only if it is different
                                ORION.Prefs.save('PageSize', pageSize);
                                pagingToolbar.pageSize = pageSize;
                                pagingToolbar.doLoad(0);
                            }
                        }
                        else {
                            return;
                        }
                    }
                }
            });

            pagingToolbar = new Ext.PagingToolbar({
                store: dataStore,
                pageSize: pageSize,
                displayInfo: true,
                displayMsg: '@{R=NCM.Strings;K=FirmwareDefinitions_Label_TemplatesCount;E=js}',
                emptyMsg: '@{R=NCM.Strings;K=FirmwareDefinitions_Label_NoTemplates;E=js}',
                items: ['-', new Ext.form.Label({ text: '@{R=NCM.Strings;K=WEBJS_VK_300;E=js}', style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;' }), pageSizeBox],
                listeners: {
                    change: function () {
                        pagingToolbarChangeHandler(this);
                    }
                }
            });
            
            grid = new Ext.grid.GridPanel({
                id: 'gridControl',
                store: dataStore,

                columns: [selectorModel, {
                    header: 'ID',
                    width: 100,
                    hidden: true,
                    hideable: false,
                    sortable: true,
                    dataIndex: 'ID'
                }, {
                    header: '@{R=NCM.Strings;K=FirmwareDefinitions_ColumnHeader_Name;E=js}',
                    width: 300,
                    sortable: true,
                    dataIndex: 'Name',
                    css: 'height:23px;',
                    renderer: renderName
                }, {
                    header: '@{R=NCM.Strings;K=FirmwareDefinitions_ColumnHeader_LastModified;E=js}',
                    width: 200,
                    dataIndex: 'LastModified',
                    sortable: true,
                    css: 'height:23px;',
                    renderer: renderDateTime
                }, {
                    header: '@{R=NCM.Strings;K=FirmwareDefinitions_ColumnHeader_Description;E=js}',
                    width: 500,
                    sortable: false,
                    css: 'height:23px;',
                    dataIndex: 'Description'
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_170;E=js}',
                    width: 150,
                    sortable: false,
                    css: 'height:23px;',
                    dataIndex: 'Author',
                    renderer: renderAuthor
                }
                ],

                sm: selectorModel,

                layout: 'fit',
                autoScroll: 'true',
                loadMask: true,
                stateful: true,
                stateId: 'gridState',
                width: 750,
                height: 500,
                stripeRows: true,

                tbar: [{
                    id: 'AddNew',
                    text: '@{R=NCM.Strings;K=FirmwareDefinitions_Button_Add;E=js}',
                    tooltip: '@{R=NCM.Strings;K=FirmwareDefinitions_ButtonTooltip_Add;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_addsnippet.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { addNewFirmwareDefinition(); }
                }, '-', {
                    id: 'Edit',
                    text: '@{R=NCM.Strings;K=FirmwareDefinitions_Button_Edit;E=js}',
                    tooltip: '@{R=NCM.Strings;K=FirmwareDefinitions_ButtonTooltip_Edit;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_editsnippet.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { editFirmwareDefinition(grid.getSelectionModel().getSelected()); }                
                }, '-', {
                    id: 'Import',
                    text: '@{R=NCM.Strings;K=WEBJS_VY_28;E=js}',
                    tooltip: '@{R=NCM.Strings;K=FirmwareDefinitions_ButtonTooltip_Import;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_import.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { importFirmwareDefinition(); }
                }, '-', {
                    id: 'Export',
                    text: '@{R=NCM.Strings;K=WEBJS_VY_30;E=js}',
                    tooltip: '@{R=NCM.Strings;K=FirmwareDefinitions_ButtonTooltip_Export;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_export.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { exportFirmwareDefinitionAsFile(grid.getSelectionModel().getSelected()); }
                }, '-', {
                    id: 'Upload',
                    text: '@{R=NCM.Strings;K=WEBJS_VY_32;E=js}',
                    tooltip: '@{R=NCM.Strings;K=FirmwareDefinitions_ButtonTooltip_Upload;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_thwack.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        if (SW.NCM.checkDemoMode("NCM_FirmwareDefinitions_UploadToThwack")) {
                            return;
                        }

                        SW.NCM.logInToThwack(uploadSelectedFirmwareDefinitionsToThwack, grid.getSelectionModel().getSelections(), false);
                    }
                }, '-', {
                    id: 'Delete',
                    text: '@{R=NCM.Strings;K=FirmwareDefinitions_Button_Delete;E=js}',
                    tooltip: '@{R=NCM.Strings;K=FirmwareDefinitions_ButtonTooltip_Delete;E=js}',
                    icon: '/Orion/NCM/Resources/images/icon_delete.png',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        if (SW.NCM.checkDemoMode("NCM_FirmwareDefinitions_Delete")) {
                            return;
                        }

                        Ext.Msg.confirm('@{R=NCM.Strings;K=FirmwareDefinitions_MessageBoxTitle_Delete;E=js}',
                                '@{R=NCM.Strings;K=FirmwareDefinitions_MessageBoxBody_Delete;E=js}',
                                function (btn, text) {
                                    if (btn == 'yes') {
                                        deleteFirmwareDefinitions(grid.getSelectionModel().getSelections());
                                    }
                                });
                    }
                }, '->', {
                    id: 'txtSearch',
                    xtype: 'textfield',
                    emptyText: '@{R=NCM.Strings;K=FirmwareDefinitions_SearchBoxWatermark_SearchTemplates;E=js}',
                    width: 200,
                    enableKeyEvents: true,
                    listeners: {
                        keypress: function (obj, evnt) {
                            if (evnt.keyCode == 13) {
                                doSearch();
                            }
                        }
                    }
                }, {
                    id: 'Clean',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_30;E=js}',
                    iconCls: 'ncm_clear-btn',
                    cls: 'x-btn-icon',
                    hidden: true,
                    handler: function () { doClean(); }
                }, {
                    id: 'Search',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_31;E=js}',
                    iconCls: 'ncm_search-btn',
                    cls: 'x-btn-icon',
                    handler: function () { doSearch(); }
                }],

                bbar: pagingToolbar

            });

            grid.render('Grid');

            updateToolbarButtons();

            gridResize();

            $("form").submit(function () { return false; });

            refreshObjects("");

            $(window).resize(function () {
                gridResize();
            });

            grid.getView().on("refresh", function () {
                var search = currentSearchTerm;
                if ($.trim(search).length == 0) return;

                var columnsToHighlight = SW.NCM.ColumnIndexByName(grid.getColumnModel(), ['Name']);
                Ext.each(columnsToHighlight, function (columnNumber) {
                    SW.NCM.highlightSearchText("#Grid .x-grid3-body .x-grid3-td-" + columnNumber, search);
                });
            });
        }
    };

}();
Ext.onReady(SW.NCM.FirmwareDefinitions.init, SW.NCM.FirmwareDefinitions);

jQuery.download = function (url, data, method) {
    //url and data options required
    if (url && data) {
        //data can be string of parameters or array/object
        data = typeof data == 'string' ? data : jQuery.param(data);
        //split params into form inputs
        var inputs = '';
        jQuery.each(data.split('&'), function () {
            var pair = this.split('=');
            inputs += '<input type="hidden" name="' + pair[0] + '" value="' + pair[1] + '" />';
        });
        //send request
        jQuery('<form action="' + url + '" method="' + (method || 'post') + '">' + inputs + '</form>')
		.appendTo('body').submit().remove();
    };
}
