﻿Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.QuickTips.init();

SW.NCM.FirmwareDefinitions = function () {

    ORION.prefix = "NCM_FirmwareImagesStorage_";

    function gridResize() {
        if (grid != null) {
            grid.setWidth(1);
            grid.setWidth($('#gridCell').width());

            grid.setHeight(getGridHeight());
            grid.doLayout();
        }
    }

    function alignWindow() {
        SW.NCM.AddEditFirmwareImage.alignAddEditImageWindow();
    }

    function getGridHeight() {
        var top = $('#gridCell').offset().top;
        return $(window).height() - top - $('#footer').height() - 75;
    }

    var getDateTimeOffset = function () {
        var date = new Date();
        var offset = date.getTimezoneOffset();
        return offset;
    }

    var currentSearchTerm = null;
    var refreshObjects = function (search, callback) {
        grid.store.removeAll();
        var pagingToolbar = grid.getBottomToolbar();
        pagingToolbar.cursor = 0;
        grid.store.baseParams['limit'] = grid.getBottomToolbar().pageSize;
        grid.store.proxy.conn.jsonData = { search: search, machineType: null, clientOffset: getDateTimeOffset() };
        currentSearchTerm = search;
        pagingToolbar.doLoad(pagingToolbar.cursor);
    }

    var reloadGridStore = function () {
        var currentPageRecordCount = grid.store.getCount();
        var selectedRecordCount = grid.getSelectionModel().getCount();
        if (currentPageRecordCount > selectedRecordCount) {
            grid.store.reload();
        }
        else {
            grid.store.load();
        }
    }
    
    function renderNodeName(value, meta, record) {
        return String.format("<a href=\"/Orion/NCM/NodeDetails.aspx?NodeID={0}\">{1}</a>", record.data.NodeID, value);
    }

    function renderDateTime(value, meta, record) {
        if (value == null) return '';

        var date = eval(value.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
        return Ext.util.Format.date(date, ExtDaTimeLocalePattern.DateFull);
    }

    function renderSize(value, meta, record) {
        if (value >= 1073741824)
            return String.format("@{R=NCM.Strings;K=FirmwareUpgradeWizard_FirmwareImageSizeInGBLabel;E=js}", (value / 1073741824).toFixed(2), value.toLocaleString());
        else if (value >= 1048576)
            return String.format("@{R=NCM.Strings;K=FirmwareUpgradeWizard_FirmwareImageSizeInMBLabel;E=js}", (value / 1048576).toFixed(2), value.toLocaleString());
        else if (value >= 1024)
            return String.format("@{R=NCM.Strings;K=FirmwareUpgradeWizard_FirmwareImageSizeInKBLabel;E=js}", (value / 1024).toFixed(2), value.toLocaleString());
        else
            return String.format("@{R=NCM.Strings;K=FirmwareUpgradeWizard_FirmwareImageSizeInBytesLabel;E=js}", value.toLocaleString());
    }

    var updateToolbarButtons = function () {
        var selCount = grid.getSelectionModel().getCount();
        var map = grid.getTopToolbar().items.map;

        var needsOnlyOneSelected = (selCount != 1);
        var needsAtLeastOneSelected = (selCount === 0);

        map.EditFirmwareImage.setDisabled(needsOnlyOneSelected);
        map.DeleteFirmwareImages.setDisabled(needsAtLeastOneSelected);

        var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');
        if (grid.getStore().getCount() > 0 && selCount == grid.getStore().getCount()) {
            hd.addClass('x-grid3-hd-checker-on');
        } else {
            hd.removeClass('x-grid3-hd-checker-on');
        }
    }
    
    var deleteFirmwareImages = function (items) {
        var ids = geSelectedIds(items);
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=FirmwareRepository_DeleteWaitDialogMessage;E=js}');

        internalDeleteFirmwareImages(ids, function (result) {
            waitMsg.hide();
            errorHandler(result);
            reloadGridStore();
        });
    }

    var internalDeleteFirmwareImages = function (ids, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/FirmwareImages.asmx",
        "DeleteFirmwareImages", { ids: ids },
        function (result) {
            onSuccess(result);
        });
    };

    var editNewFirmwareImage = function (item) { SW.NCM.AddEditFirmwareImage.showAddEditImageWindow(item.data.ID, function () { grid.store.reload(); }) };
    
    var startScanFirmwareRepository = function () {
        ORION.callWebService("/Orion/NCM/Services/FirmwareImages.asmx",
            "StartScanFirmwareRepository", {},
            function () {
                isScanFirmwareRepositoryRunning();
            });
    }

    var waitMsg;
    var isScanFirmwareRepositoryRunning = function () {
        waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=FirmwareRepository_ScanWaitDialogMessage;E=js}');
        Ext.TaskMgr.start(IsScanFirmwareRepositoryRunningTask);
    }

    var IsScanFirmwareRepositoryRunningTask = {
        run: function () {
            ORION.callWebService("/Orion/NCM/Services/FirmwareImages.asmx",
                             "IsScanFirmwareRepositoryRunning", {},
                             function (result) {
                                 var d = JSON.parse(result);
                                 if (!d.IsRunning) {
                                     if (IsScanFirmwareRepositoryRunningTask != null)
                                         Ext.TaskMgr.stop(IsScanFirmwareRepositoryRunningTask);

                                     if (waitMsg != null)
                                         waitMsg.hide();

                                     reloadGridStore();
                                 }

                                 lastErrorHandler(d.LastError);
                             });
        },
        interval: 5000
    }

    var lastErrorHandler = function (lastError) {
        $("#divLastError").empty();
        if (lastError != null) {
            $("#divLastError").html(String.format(" \
                <div class='sw-suggestion sw-suggestion-fail'> \
                    <div class='sw-suggestion-icon'></div> \
                    {0} \
                </div>", lastError));
        }
    }

    var geSelectedIds = function (items) {
        var ids = [];

        Ext.each(items, function (item) {
            ids.push(item.data.ID);
        });

        return ids;
    }

    var pagingToolbarChangeHandler = function (that) {
        if (that.displayItem) {
            var count = 0;
            that.store.each(function (record) {
                count++;
            });
            var msg = count == 0 ? that.emptyMsg : String.format(that.displayMsg, that.cursor + 1, that.cursor + count, that.store.getTotalCount());
            that.displayItem.setText(msg);
        }
    }

    var doClean = function () {
        var map = grid.getTopToolbar().items.map;
        map.Clean.setVisible(false);
        map.txtSearch.setValue('');
        refreshObjects("");
    }

    var doSearch = function () {
        var map = grid.getTopToolbar().items.map;
        var search = map.txtSearch.getValue();
        if ($.trim(search).length == 0) return;
        refreshObjects(search);
        map.Clean.setVisible(true);
    }

    function errorHandler(result) {
        if (result != null && result.Error) {
            Ext.Msg.show({
                title: result.Source,
                msg: result.Msg,
                minWidth: 500,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        }
    }

    var selectorModel;
    var dataStore;
    var grid;
    var pagingToolbar;

    return {
        init: function () {

            if ($("[id$='_ContentContainer']").length == 0)
                return false;

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", updateToolbarButtons);
            
            dataStore = new ORION.WebServiceStore(
                                "/Orion/NCM/Services/FirmwareImages.asmx/GetFirmwareImages",
                                [
                                    { name: 'ID', mapping: 0 },
                                    { name: 'FileName', mapping: 1 },
                                    { name: 'Description', mapping: 2 },
                                    { name: 'NodeID', mapping: 3 },
                                    { name: 'Caption', mapping: 4 },
                                    { name: 'IP_Address', mapping: 5 },
                                    { name: 'DateTimeUtc', mapping: 6 },
                                    { name: 'MD5Hash', mapping: 7 },
                                    { name: 'Size', mapping: 8 }
                                ],
                                "FileName");

            var pageSize = parseInt(ORION.Prefs.load('PageSize', '25'));

            var pageSizeBox = new Ext.form.NumberField({
                id: 'pageSizeBox',
                enableKeyEvents: true,
                allowNegative: false,
                width: 30,
                allowBlank: false,
                minValue: 1,
                maxValue: 100,
                value: pageSize,
                listeners: {
                    scope: this,
                    'keydown': function (f, e) {
                        var k = e.getKey();
                        if (k == e.RETURN) {
                            e.stopEvent();
                            var value = parseInt(f.getValue());
                            if (!isNaN(value) && value > 0 && value <= 100) {
                                pageSize = value;
                                var pagingToolbar = grid.getBottomToolbar();
                                if (pagingToolbar.pageSize != pageSize) { // update page size only if it is different
                                    ORION.Prefs.save('PageSize', pageSize);
                                    pagingToolbar.pageSize = pageSize;
                                    pagingToolbar.doLoad(0);
                                }
                            }
                            else {
                                return;
                            }
                        }
                    },
                    'focus': function (field) {
                        field.el.dom.select();
                    },
                    'change': function (f, numbox, o) {
                        var value = parseInt(f.getValue());
                        if (!isNaN(value) && value > 0 && value <= 100) {
                            pageSize = value;
                            var pagingToolbar = grid.getBottomToolbar();
                            if (pagingToolbar.pageSize != pageSize) { // update page size only if it is different
                                ORION.Prefs.save('PageSize', pageSize);
                                pagingToolbar.pageSize = pageSize;
                                pagingToolbar.doLoad(0);
                            }
                        }
                        else {
                            return;
                        }
                    }
                }
            });

            pagingToolbar = new Ext.PagingToolbar({
                store: dataStore,
                pageSize: pageSize,
                displayInfo: true,
                displayMsg: '@{R=NCM.Strings;K=FirmwareRepository_PagingMessage;E=js}',
                emptyMsg: '@{R=NCM.Strings;K=FirmwareRepository_PagingEmptyMessage;E=js}',
                items: ['-', new Ext.form.Label({ text: '@{R=NCM.Strings;K=WEBJS_VK_300;E=js}', style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;' }), pageSizeBox],
                listeners: {
                    change: function () {
                        pagingToolbarChangeHandler(this);
                    }
                }
            });

            grid = new Ext.grid.GridPanel({
                id: 'gridControl',
                store: dataStore,

                columns: [selectorModel, {
                    header: 'ID',
                    width: 100,
                    hidden: true,
                    hideable: false,
                    dataIndex: 'ID'
                }, {
                    header: '@{R=NCM.Strings;K=FirmwareRepository_FileNameColumnHeader;E=js}',
                    width: 300,
                    sortable: true,
                    dataIndex: 'FileName',
                    css: 'height:23px;'
                }, {
                    header: '@{R=NCM.Strings;K=FirmwareRepository_DescriptionColumnHeader;E=js}',
                    width: 450,
                    sortable: true,
                    dataIndex: 'Description',
                    css: 'height:23px;'
                }, {
                    header: '@{R=NCM.Strings;K=FirmwareRepository_NodeColumnHeader;E=js}',
                    width: 200,
                    sortable: true,
                    dataIndex: 'Caption',
                    css: 'height:23px;',
                    renderer: renderNodeName
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_44;E=js}',
                    width: 150,
                    sortable: true,
                    dataIndex: 'IP_Address',
                    css: 'height:23px;'
                }, {
                    header: '@{R=NCM.Strings;K=FirmwareRepository_MD5HashColumnHeader;E=js}',
                    width: 250,
                    sortable: true,
                    dataIndex: 'MD5Hash',
                    css: 'height:23px;',
                    hideable: !SW.NCM.IsFipsEnabled,
                    hidden: SW.NCM.IsFipsEnabled
                }, {
                    header: '@{R=NCM.Strings;K=FirmwareRepository_SizeColumnHeader;E=js}',
                    width: 200,
                    sortable: true,
                    dataIndex: 'Size',
                    css: 'height:23px;',
                    renderer: renderSize
                }, {
                    header: '@{R=NCM.Strings;K=FirmwareRepository_TimeColumnHeader;E=js}',
                    width: 200,
                    sortable: true,
                    dataIndex: 'DateTimeUtc',
                    css: 'height:23px;',
                    renderer: renderDateTime
                }
                ],

                sm: selectorModel,

                layout: 'fit',
                autoScroll: 'true',
                loadMask: true,
                stateful: true,
                stateId: 'gridState',
                width: 750,
                height: 500,
                stripeRows: true,

                tbar: [{
                    id: 'EditFirmwareImage',
                    text: '@{R=NCM.Strings;K=FirmwareRepository_EditFirmwareImageButtonText;E=js}',
                    tooltip: '@{R=NCM.Strings;K=FirmwareRepository_EditFirmwareImageButtonTooltip;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_editsnippet.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { editNewFirmwareImage(grid.getSelectionModel().getSelected()); }
                }, '-', {
                    id: 'DeleteFirmwareImages',
                    text: '@{R=NCM.Strings;K=FirmwareRepository_DeleteFirmwareImagesButtonText;E=js}',
                    tooltip: '@{R=NCM.Strings;K=FirmwareRepository_DeleteFirmwareImagesButtonTooltip;E=js}',
                    icon: '/Orion/NCM/Resources/images/icon_delete.png',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        if (SW.NCM.IsDemoServer) {
                            demoAction('NCM_DeleteFirmwareImages', $('#DeleteFirmwareImages'));
                        }
                        else {
                            Ext.Msg.confirm('@{R=NCM.Strings;K=FirmwareRepository_DeleteFirmwareImagesButtonText;E=js}',
                                    '@{R=NCM.Strings;K=FirmwareRepository_DeleteConfirmDialogMessage;E=js}',
                                    function (btn, text) {
                                        if (btn == 'yes') {
                                            deleteFirmwareImages(grid.getSelectionModel().getSelections());
                                        }
                                    });
                        }
                    }
                }, '-', {
                    id: 'ScanFirmwareRepository',
                    text: '@{R=NCM.Strings;K=FirmwareRepository_ScanFirmwareRepositoryButtonText;E=js}',
                    tooltip: '@{R=NCM.Strings;K=FirmwareRepository_ScanFirmwareRepositoryButtonTooltip;E=js}',
                    icon: '/Orion/NCM/Resources/images/InventoryIcons/icon_update_inventory.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        if (SW.NCM.IsDemoServer) {
                            demoAction('NCM_ScanFirmwareRepository', $('#ScanFirmwareRepository'));
                        }
                        else {
                            startScanFirmwareRepository();
                        }
                    }
                }, '->', {
                    id: 'txtSearch',
                    xtype: 'textfield',
                    emptyText: '@{R=NCM.Strings;K=FirmwareRepository_SearchEmptyText;E=js}',
                    width: 200,
                    enableKeyEvents: true,
                    listeners: {
                        keypress: function (obj, evnt) {
                            if (evnt.keyCode == 13) {
                                doSearch();
                            }
                        }
                    }
                }, {
                    id: 'Clean',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_30;E=js}',
                    iconCls: 'ncm_clear-btn',
                    cls: 'x-btn-icon',
                    hidden: true,
                    handler: function () { doClean(); }
                }, {
                    id: 'Search',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_31;E=js}',
                    iconCls: 'ncm_search-btn',
                    cls: 'x-btn-icon',
                    handler: function () { doSearch(); }
                }],

                bbar: pagingToolbar

            });

            grid.render('Grid');
            updateToolbarButtons();
            gridResize();

            $("form").submit(function () { return false; });

            refreshObjects("");
            isScanFirmwareRepositoryRunning();

            $(window).resize(function () {
                gridResize();
                alignWindow();
            });

            grid.getView().on("refresh", function () {
                var search = currentSearchTerm;
                if ($.trim(search).length == 0) return;

                var columnsToHighlight = SW.NCM.ColumnIndexByDataIndex(grid.getColumnModel(), ['FileName', 'Description', 'Caption']);
                Ext.each(columnsToHighlight, function (columnNumber) {
                    SW.NCM.highlightSearchText("#Grid .x-grid3-body .x-grid3-td-" + columnNumber, search);
                });
            });
        }
    };

}();
Ext.onReady(SW.NCM.FirmwareDefinitions.init, SW.NCM.FirmwareDefinitions);