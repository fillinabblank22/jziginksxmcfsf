﻿Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.QuickTips.init();

SW.NCM.FirmwareImagesRadioGrid = function () {

    function gridResize(w, h) {
        if (grid != null) {
            grid.setWidth(w);
            grid.setHeight(h);
        }
    }

    var getDateTimeOffset = function () {
        var date = new Date();
        var offset = date.getTimezoneOffset();
        return offset;
    }

    var refreshGrid = function (search, callback) {
        grid.store.removeAll();
        var pagingToolbar = grid.getBottomToolbar();
        pagingToolbar.cursor = 0;
        grid.store.baseParams['limit'] = grid.getBottomToolbar().pageSize;
        grid.store.proxy.conn.jsonData = { search: search, machineType: currentMachineType, clientOffset: getDateTimeOffset() };
        currentSearchTerm = search;
        pagingToolbar.doLoad(pagingToolbar.cursor);
    }

    var doClean = function () {
        var map = grid.getTopToolbar().items.map;
        map.Clean.setVisible(false);
        map.txtSearch.setValue('');
        refreshGrid("");
    }

    var doSearch = function () {
        var map = grid.getTopToolbar().items.map;
        var search = map.txtSearch.getValue();
        if ($.trim(search).length == 0) return;
        refreshGrid(search);
        map.Clean.setVisible(true);
    }

    function renderRadioButton(value, meta, record) {
        return String.format('<input type="radio" name="firmware" imageid="{0}" />', record.data.ID);
    }

    function renderCaption(value, meta, record) {
        return String.format('<a target="__blank" href="/Orion/NCM/NodeDetails.aspx?NodeID={0}">{1}</a>', record.data.NodeID, Ext.util.Format.htmlEncode(value));
    }

    function renderDateTime(value, meta, record) {
        if (value == null) return '';

        var date = eval(value.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
        return Ext.util.Format.date(date, ExtDaTimeLocalePattern.DateFull);
    }

    function renderSize(value, meta, record) {
        if (value >= 1073741824)
            return String.format("@{R=NCM.Strings;K=FirmwareUpgradeWizard_FirmwareImageSizeInGBLabel;E=js}", (value / 1073741824).toFixed(2), value.toLocaleString());
        else if (value >= 1048576)
            return String.format("@{R=NCM.Strings;K=FirmwareUpgradeWizard_FirmwareImageSizeInMBLabel;E=js}", (value / 1048576).toFixed(2), value.toLocaleString());
        else if (value >= 1024)
            return String.format("@{R=NCM.Strings;K=FirmwareUpgradeWizard_FirmwareImageSizeInKBLabel;E=js}", (value / 1024).toFixed(2), value.toLocaleString());
        else
            return String.format("@{R=NCM.Strings;K=FirmwareUpgradeWizard_FirmwareImageSizeInBytesLabel;E=js}", value.toLocaleString());
    }

    var currentMachineType = null;
    var currentSearchTerm = null;
    var grid;
    var window;

    return {
        show: function (machineType, onSuccess) {

            currentMachineType = machineType;

            window = new Ext.Window({
                id: 'window',
                layout: 'anchor',
                width: Ext.getBody().getViewSize().width * 0.8,
                minWidth: 800,
                height: Ext.getBody().getViewSize().height * 0.7,
                minHeight: 400,
                closeAction: 'close',
                title: '@{R=NCM.Strings;K=FirmwareUpgradeWizard_SelectFirmwareImageDialogTitle;E=js}',
                modal: true,
                resizable: false,
                stateful: false,
                plain: true,
                html: '<div id="Grid"/>',
                buttons: [{
                    text: '@{R=NCM.Strings;K=WEBJS_VK_111;E=js}',
                    cls: 'sw-btn-primary',
                    handler: function () {
                        var record = grid.getSelectionModel().getSelected();
                        if (record != undefined) {
                            onSuccess(record.data.ID, record.data.FileName, record.data.Size);
                            window.close();
                        }
                        else {
                            Ext.Msg.show({
                                title: '@{R=NCM.Strings;K=FirmwareUpgradeWizard_SelectFirmwareImageDialogTitle;E=js}',
                                msg: '@{R=NCM.Strings;K=FirmwareUpgradeWizard_SelectFirmwareImageDialogMessage;E=js}',
                                minWidth: 500,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.WARNING
                            });
                        }
                    }
                }, {
                        text: '@{R=NCM.Strings;K=WEBJS_VK_38;E=js}',
                    handler: function () {
                        window.close();
                    }
                }],
                listeners: {
                    resize: function (that, w, h) {
                        var upgradeStorageLinkPadding = 10;
                        gridResize(that.getInnerWidth(), that.getInnerHeight() - $('#UpgradeStorageLink').height() - upgradeStorageLinkPadding);
                    },
                    show: function (that) {

                        that.alignTo(Ext.getBody(), "c-c");

                        var dataStore = new ORION.WebServiceStore(
                                "/Orion/NCM/Services/FirmwareImages.asmx/GetFirmwareImages",
                                [
                                    { name: 'ID', mapping: 0 },
                                    { name: 'FileName', mapping: 1 },
                                    { name: 'Description', mapping: 2 },
                                    { name: 'NodeID', mapping: 3 },
                                    { name: 'Caption', mapping: 4 },
                                    { name: 'IP_Address', mapping: 5 },
                                    { name: 'DateTimeUtc', mapping: 6 },
                                    { name: 'Size', mapping: 8 }
                                ],
                                "FileName");

                        var pagingToolbar = new Ext.PagingToolbar({
                            store: dataStore,
                            pageSize: 25,
                            displayInfo: true,
                            displayMsg: '@{R=NCM.Strings;K=FirmwareRepository_PagingMessage;E=js}',
                            emptyMsg: '@{R=NCM.Strings;K=FirmwareRepository_PagingEmptyMessage;E=js}'
                        });

                        grid = new Ext.grid.GridPanel({
                            id: 'gridControl',
                            store: dataStore,
                            columns: [{
                                width: 30,
                                resizable: false,
                                sortable: false,
                                menuDisabled: true,
                                hideable: false,
                                align: 'center',
                                dataIndex: 'ID',
                                css: 'height:23px;',
                                renderer: renderRadioButton
                            }, {
                                header: '@{R=NCM.Strings;K=FirmwareRepository_FileNameColumnHeader;E=js}',
                                width: 300,
                                sortable: true,
                                dataIndex: 'FileName',
                                css: 'height:23px;'
                            }, {
                                header: '@{R=NCM.Strings;K=FirmwareRepository_DescriptionColumnHeader;E=js}',
                                width: 500,
                                sortable: true,
                                css: 'height:23px;',
                                dataIndex: 'Description'
                            }, {
                                header: '@{R=NCM.Strings;K=FirmwareRepository_NodeColumnHeader;E=js}',
                                width: 200,
                                sortable: true,
                                css: 'height:23px;',
                                dataIndex: 'Caption',
                                renderer: renderCaption
                            }, {
                                header: '@{R=NCM.Strings;K=WEBJS_VK_44;E=js}',
                                width: 150,
                                sortable: true,
                                dataIndex: 'IP_Address',
                                css: 'height:23px;'
                            }, {
                                header: '@{R=NCM.Strings;K=FirmwareRepository_SizeColumnHeader;E=js}',
                                width: 200,
                                sortable: false,
                                dataIndex: 'Size',
                                css: 'height:23px;',
                                renderer: renderSize
                            }, {
                                header: '@{R=NCM.Strings;K=FirmwareRepository_TimeColumnHeader;E=js}',
                                width: 200,
                                dataIndex: 'DateTimeUtc',
                                sortable: true,
                                css: 'height:23px;',
                                renderer: renderDateTime
                            }
                            ],

                            layout: 'fit',
                            stateful: false,
                            autoScroll: 'true',
                            loadMask: true,
                            width: 750,
                            height: 500,
                            stripeRows: true,

                            tbar: ['->',
                            {
                                id: 'txtSearch',
                                xtype: 'textfield',
                                emptyText: '@{R=NCM.Strings;K=FirmwareRepository_SearchEmptyText;E=js}',
                                width: 200,
                                enableKeyEvents: true,
                                listeners: {
                                    keypress: function (obj, evnt) {
                                        if (evnt.keyCode == 13) {
                                            doSearch();
                                        }
                                    }
                                }
                            }, {
                                id: 'Clean',
                                tooltip: '@{R=NCM.Strings;K=WEBJS_VK_30;E=js}',
                                iconCls: 'ncm_clear-btn',
                                cls: 'x-btn-icon',
                                hidden: true,
                                handler: function () { doClean(); }
                            }, {
                                id: 'Search',
                                tooltip: '@{R=NCM.Strings;K=WEBJS_VK_31;E=js}',
                                iconCls: 'ncm_search-btn',
                                cls: 'x-btn-icon',
                                handler: function () { doSearch(); }
                            }],

                            bbar: pagingToolbar

                        });
                        grid.on("rowclick", function (that, rowIndex, e) {
                            var record = that.getSelectionModel().getSelected();
                            if (record != undefined) {
                                $(String.format("input[imageid={0}]", record.data.ID)).prop('checked', true);
                            }
                        });
                        grid.render('Grid');

                        //add "Manage Firmware Upgrade Storage" link on top of grid
                        $('#Grid').prepend('<div align="right" id="UpgradeStorageLink" style="padding: 5px 0px;">' +
                            '<img src="/Orion/Nodes/images/icons/icon_edit.gif" alt="" style="vertical-align: text-bottom;"/>' +
                            '<a href="/Orion/NCM/Admin/Firmware/FirmwareImagesStorage.aspx" class="RenewalsLink" target="_blank">@{R=NCM.Strings;K=FirmwareUpgradeWizard_ManageFirmwareRepositoryLink;E=js}</a>' +
                            '</div>');

                        var upgradeStorageLinkPadding = 10;
                        gridResize(that.getInnerWidth(), that.getInnerHeight() - $('#UpgradeStorageLink').height() - upgradeStorageLinkPadding);

                        refreshGrid("");

                        grid.getView().on("refresh", function () {
                            var search = currentSearchTerm;
                            if ($.trim(search).length == 0) return;

                            var columnsToHighlight = SW.NCM.ColumnIndexByDataIndex(grid.getColumnModel(), ['FileName', 'Description', 'Caption']);
                            Ext.each(columnsToHighlight, function (columnNumber) {
                                SW.NCM.highlightSearchText("#Grid .x-grid3-body .x-grid3-td-" + columnNumber, search);
                            });
                        });
                    },
                    close: function (that) {
                        grid = null;
                        window = null;
                    }
                }

            }).show();
        },

        resize: function () {
            if (window != null) {
                window.setSize(Ext.getBody().getViewSize().width * 0.8, Ext.getBody().getViewSize().height * 0.7);
                window.alignTo(Ext.getBody(), "c-c");
            }
        }
    }

}();