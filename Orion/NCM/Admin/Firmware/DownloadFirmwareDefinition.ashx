﻿<%@ WebHandler Language="C#" Class="DownloadFirmwareDefinition" %>

using System;
using System.Globalization;
using System.Web;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;

public class DownloadFirmwareDefinition : IHttpHandler 
{
    public void ProcessRequest(HttpContext context)
    {
        try
        {
            var isWrapper = new ISWrapper();
            using (var proxy = isWrapper.Proxy)
            {
                int id = Convert.ToInt32(context.Request[@"ID"], CultureInfo.InvariantCulture);
                FirmwareDefinition firmwareDefinition = proxy.Cirrus.FirmwareDefinitions.GetFirmwareDefinition(id);

                string fileName = $@"""{CommonHelper.EncodeFilenameIfNeeded(context, firmwareDefinition.Name)}.xml""";
                context.Response.Clear();
                context.Response.ContentType = @"application/xml";
                context.Response.AddHeader(@"Content-Disposition", $@"attachment; filename={fileName}");
                context.Response.Write(FirmwareDefinition.Serialize(firmwareDefinition));
            }
        }
        catch (Exception ex)
        {
            context.Response.Clear();
            if (ex.GetType() == typeof(System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>))
                context.Response.Write(
                    $@"<div style=""color:Red;font-size:12px;"">{(ex as System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>).Detail.Message}</div>");
            else
                context.Response.Write($@"<div style=""color:Red;font-size:12px;"">{ex.Message}</div>");
        }
        context.ApplicationInstance.CompleteRequest();
    }

    public bool IsReusable
    {
        get { return false; }
    }
}