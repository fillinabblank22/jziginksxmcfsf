﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="FirmwareImagesStorage.aspx.cs" Inherits="Orion_NCM_Firmware_ImagesStorage" %>
<%@ Import Namespace="SolarWinds.NCMModule.Web.Resources" %>
<%@ Import Namespace="SolarWinds.NCM.Common.Dal" %>

<%@ Register TagPrefix="ncm1" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />

    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/PolicyReports/PolicyReport.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippetsHelper.js"></script>
    <ncm1:ExtLocalDateTimePattern ID="ExtLocalDateTimePattern1" runat="server"/> 
    <script src="js/FirmwareImagesStorage.js"></script>
    <script src="js/AddEditFirmwareImages.js"></script>

    <style type="text/css">
        #Grid td
        {
            padding: 0px 0px 0px 0px;
            border: 0px;
        }

        .x-window-body {
            overflow: auto;
        }
        
        #DescriptionBox {
            padding-top: 10px;
        }

        #ddlMachineTypes {
            margin-top: 5px;
            margin-right: 10px;
        }

        #MachineTypeList td {
            white-space: nowrap;
        }

        #divLastError {
            padding-bottom: 10px;
        }
    </style>

    <script type="text/javascript">
        SW.NCM.IsDemoServer = <%=SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer.ToString().ToLowerInvariant() %>;
        SW.NCM.IsFipsEnabled = <%=ServiceLocator.Container.Resolve<IFipsConfiguration>().FipsEnabledInOrion().ToString().ToLowerInvariant() %>;

        var CustomValidation = function(sender, args) {
            if ($('#cvFileName').val() == "")
                args.IsValid = false;
        }
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title %>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMFirmwareRepository" />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <%foreach (string setting in new string[] { @"PageSize" })
      { %>
    <input type="hidden" name="NCM_FirmwareDefinitions_<%=setting%>" id="NCM_FirmwareDefinitions_<%=setting%>"
        value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get($@"NCM_FirmwareDefinitions_{setting}")%>' />
    <%}%>
    
    <asp:Panel id="StorageLocationBox" runat="server" style="padding:10px;">
        <div class='sw-suggestion sw-suggestion-fail'><span class='sw-suggestion-icon'></span>
            <%= Resources.NCMWebContent.WEBDATA_AP_1 %> &raquo; <a class='sw-suggestion-link' href='/Orion/NCM/Admin/Firmware/FirmwareStorageLocation.aspx'><%= Resources.NCMWebContent.WEBDATA_AP_2 %></a>
        </div>
    </asp:Panel>
    <asp:Panel id="ContentContainer" runat="server">
        <div style="padding:10px;">
            <div id="divLastError"></div>
            <div id="gridCell" class="tab-top">
                <div id="Grid" />
            </div>
        </div>
        <div id="firmwareImageDialog" class="x-hidden">
            <div id="firmwareImageBody" class="x-panel-body" style="padding: 10px;">
                <div>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <b><%= Resources.NCMWebContent.FirmwareRepository_FileNameLabel %></b>
                            </td>
                            <td style="padding-left:10px;text-align:left;">
                                <asp:Label ID="lblFileName" runat="server" ClientIDMode="Static"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top:10px;">
                                <b><%= Resources.NCMWebContent.FirmwareRepository_RelativePathLabel %></b>
                            </td>
                            <td colspan="2" style="padding-top:10px;padding-left:10px;text-align:left;">
                                <asp:Label ID="lblRelativePath" runat="server" ClientIDMode="Static"></asp:Label>
                            </td>
                        </tr>
                    </table>
                        
                    <div id="DescriptionBox">
                        <div>
                            <b><%= Resources.NCMWebContent.WEBDATA_VM_16 %></b>
                        </div>
                        <div>
                            <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Width="500" Height="100" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div id="MachineTypeBox">
                        <div>
                            <table id="MachineTypeList" class="GroupItems NeedsZebraStripes" style="width: 250px; margin: 10px 0;" cellpadding="0" cellspacing="0" runat="server" ClientIDMode="Static"></table>
                        </div>
                        <div>
                            <div>
                                <b><%= Resources.NCMWebContent.FirmwareRepository_MachineTypeLabel %></b>
                            </div>
                            <div>
                                <asp:DropDownList ID="ddlMachineTypes" runat="server" ClientIDMode="Static" Width="250"></asp:DropDownList>
                                <orion:LocalizableButton runat="server" ID="btnAdd" Text="<%$Resources:NCMWebContent, WEBDATA_VK_904%>" OnClientClick="SW.NCM.AddEditFirmwareImage.addItemToMachineTypeList($('#ddlMachineTypes').val()); return false;" DisplayType="Small"/>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </asp:Panel>
    <div id="divError" runat="server"/>
</asp:Content>