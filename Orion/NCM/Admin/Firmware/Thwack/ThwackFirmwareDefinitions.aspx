﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="ThwackFirmwareDefinitions.aspx.cs" Inherits="Orion_NCM_Firmware_ThwackFirmwareDefinitions" %>

<%@ Register Src="~/Orion/NCM/NavigationTabBar.ascx" TagPrefix="ncm" TagName="NavigationTabBar" %>
<%@ Register TagPrefix="ncm1" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippets.css" />

    <ncm1:ConfigSnippetUISettings ID="ConfigSnippetUISettings1" runat="server" Name="NCM_FirmwareDefinitions_Thwack_Columns" DefaultValue="[]" RenderTo="columnModel" />
    <script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippetsHelper.js"></script>

    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script src="ThwackFirmwareDefinition.js"></script>

    <script type="text/javascript">
        var IsDemoMode = <%=SolarWinds.NCMModule.Web.Resources.CommonHelper.IsDemoMode().ToString().ToLowerInvariant() %>;
        var thwackUserInfo = <%=_thwackUserInfo%>;
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminContentPlaceholder" runat="server">
	<input type="hidden" name="NCM_FirmwareDefinitions_Thwack_GroupByValue" id="NCM_FirmwareDefinitions_Thwack_GroupByValue" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get(@"NCM_FirmwareDefinitions_Thwack_GroupByValue")%>' />
    <asp:Panel ID="ContentContainer" runat="server">
        <div style="padding:10px;">
            <ncm:NavigationTabBar ID="NavigationTabBar" runat="server" Visible="true"></ncm:NavigationTabBar>

            <div id="tabPanel" class="tab-top">

                <table class="ExistingElements" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="2">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="font-size: 11px; padding-bottom: 10px;">
                                        <%=Resources.NCMWebContent.WEBDATA_AP_11%>
                                        <a href='https://thwack.com/ncm-firmware-upgrade-templates'
                                            style="text-decoration:underline;color:#336699;"><%=Resources.NCMWebContent.WEBDATA_VY_16%>
                                        </a>.
                                    </td>
                                    <td align="right" style="padding-bottom:5px;">
                                        <label for="search"><%=Resources.NCMWebContent.WEBDATA_VY_17%></label>
                                        <input id="search" type="text" size="20" />
                                        <%= $@"<input id=""doSearch"" type=""button"" value=""{Resources.NCMWebContent.WEBDATA_VY_19}"" />" %>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr valign="top" align="left">
			            <td style="padding-right:10px;width:15%;">
		                    <div class="ElementGrouping">
			                    <div class="ncm_GroupSection x-panel-header x-toolbar x-small-editor">
				                    <div><%=Resources.NCMWebContent.WEBDATA_AP_12%></div>
			                    </div>
			                        <ul class="GroupItems" style="width:100%;"></ul>
		                    </div>
	                    </td>
	                    <td id="gridCell">
                            <div id="Grid"/>
	                    </td>
                    </tr>

                </table>

                <div id="originalQuery"></div>
                <div id="test"></div>
                <pre id="stackTrace"></pre>
            </div>
            <div id="snippetDescriptionDialog" class="x-hidden">
                <div id="snippetDescriptionBody" class="x-panel-body" style="padding: 10px; height: 210px; overflow: auto;" />
            </div>
        </div>
    </asp:Panel>
    <div style="padding:10px;" id="ErrorContainer" runat="server"/>
</asp:Content>