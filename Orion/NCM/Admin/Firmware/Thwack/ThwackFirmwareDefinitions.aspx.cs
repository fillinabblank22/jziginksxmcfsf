﻿using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Common;
using System;

public partial class Orion_NCM_Firmware_ThwackFirmwareDefinitions : System.Web.UI.Page
{
    protected string _thwackUserInfo = @"{name:'',pass:'', valid: false}";
    public bool IsDemoMode;
    private static readonly string THWACK_FIRMWARE_DEFINITIONS_LIST_SESSION_KEY = @"thwack_firmwaredefinition_list";

    private readonly ICommonHelper commonHelper;

    public Orion_NCM_Firmware_ThwackFirmwareDefinitions()
    {
        commonHelper = ServiceLocator.Container.Resolve<ICommonHelper>();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (!OrionConfiguration.IsDemoServer && (!Profile.AllowAdmin || !SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR)))
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_AP_13}");
        }

        var validator = new SWISValidator
        {
            ContentContainer = ContentContainer,
            RedirectIfValidationFailed = true
        };

        ErrorContainer.Controls.Add(validator);

        if (validator.IsValid)
        {
            commonHelper.Initialise();

            if (!IsPostBack)
                Session.Remove(THWACK_FIRMWARE_DEFINITIONS_LIST_SESSION_KEY);

            SetupCred();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.FirmwareUpgradeThwack_PageTitle;
    }

    private void SetupCred()
    {
        System.Net.NetworkCredential cred = Session[@"NCM_ThwackCredential"] as System.Net.NetworkCredential;

        bool credValid;
        object vc = Session[@"NCM_ThwackCredValidation"];

        if (vc != null)
            credValid = (bool)vc;
        else
            credValid = false;

        if (cred != null)
        {
            this._thwackUserInfo =
                $@"{{name:'{cred.UserName.Replace(@"\", @"&#92;")}',pass:'{cred.Password.Replace(@"\", @"&#92;")}', valid:{credValid.ToString().ToLower()}}}";
        }
    }
}