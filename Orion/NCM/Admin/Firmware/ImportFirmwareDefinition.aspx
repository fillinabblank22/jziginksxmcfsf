﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="ImportFirmwareDefinition.aspx.cs" Inherits="Orion_NCM_Firmware_ImportFirmwareDefinition" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMImportFirmwareTemplate" />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />

    <div style="padding:10px;">
        <br />
        <asp:FileUpload ID="fileUpload" runat="server" Width="50em" size="100"  />
        <br />
        <br />
        <asp:CustomValidator ID="validator" runat="server" Display="Dynamic" Font-Size="9pt" />
        <br />
        <br />
        <orion:LocalizableButton runat="server" ID="Submit" LocalizedText="Submit" DisplayType="Primary" OnClick="SubmitClick" />
        <orion:LocalizableButton runat="server" ID="Cancel" LocalizedText="Cancel" DisplayType="Secondary" OnClick="CancelClick" CausesValidation="false" />
    </div>
</asp:Content>