using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;
using System.Linq;

public partial class Orion_NCM_Resources_PolicyReports_EditPolicyReport : System.Web.UI.Page
{
    private const string QUERY_ReportID_PARAM = "ReportID";
    private const string QUERY_FOLDER_PARAM = "Folder";

    private ISWrapper isLayer = new ISWrapper();
    private Guid? _reportId;
    private string _folder = string.Empty;
    private bool _addMode;
    private bool hasPerm = true;
    private PolicyReport report= null;

    protected override void OnInit(EventArgs e)
    {
        btnSubmit.Visible = !CommonHelper.IsDemoMode();

        try
        {
            var reportIdParamValue = Page.Request.QueryString[QUERY_ReportID_PARAM];
            Guid tempId;
            if (!string.IsNullOrWhiteSpace(reportIdParamValue) && Guid.TryParse(reportIdParamValue, out tempId))
            {
                _reportId = tempId;
            }

            _folder = Page.Request.QueryString[QUERY_FOLDER_PARAM];

            if (!_reportId.HasValue)
            {
                _addMode = true;
                btnHelp.HelpUrlFragment = @"OrionNCMWebManagePolicyReportsCreateReport";
            }
            else
            {
                _addMode = false;
                btnHelp.HelpUrlFragment = @"OrionNCMWebManagePolicyReportsEditReport";
            }

            ValidatePermissions();

            var folders = GetAllFolders();
            txtFolder.DataSource = folders;
            base.OnInit(e);
        }
        catch (InvalidOperationException ex)
        {
            hasPerm = false;
            DisplayException(ex);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (hasPerm)
            SetReportFields();        
    }

    protected void btnAddPolicies_Click(object sender, EventArgs e)
    {
        var nodes = treeAllPolicies.GetSelectedNodes();
        if (nodes != null)
            treeAssignedPolicies.AddNodes(nodes);
    }

    protected void btnRemovePolicies_Click(object sender, EventArgs e)
    {
        treeAssignedPolicies.RemoveSelectedNodes();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (CommonHelper.IsDemoMode()) return;

        try
        {
            var id = SaveReport();
            Response.Redirect("PolicyReportManagement.aspx?opento=" + id);
        }
        catch (Exception ex)
        {
            DisplayException(ex);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("PolicyReportManagement.aspx");
    }

    #region Private Methods    

    private DataTable LoadPolicyByReport(PolicyReport report)
    {
        if (_addMode)
            return null;

        var policyIDs = report.AssignedPoliciesList;

        if (policyIDs == null || policyIDs.Count == 0)
            return null;

        var swql = @"SELECT P.PolicyID AS ID, P.Name, P.Grouping FROM Cirrus.Policies AS P WHERE PolicyID IN ({0})";

        var ids = new StringBuilder();

        var isFirst = true;
        foreach(var id in policyIDs)
        {
            if(isFirst)
                isFirst = false;
            else           
                ids.Append(@",");

            ids.AppendFormat(@"'{0}'",id);
        }

        swql = string.Format(swql, ids);

        return ExecuteSWQL(swql);
    }

    private PolicyReport LoadReport()
    {
        if (_addMode)
        {
            var report = new PolicyReport();
            return report;
        }

        return LoadReport(_reportId.Value);
    }

    private PolicyReport LoadReport(Guid id)
    {
        using (var proxy = isLayer.Proxy)
        {
            return proxy.Cirrus.PolicyReports.GetPolicyReport(id, false);
        }
    }
  
    private Guid SaveReport()
    {
        var report = LoadReport();
        report.Name = txtReportName.Text;
        report.Comments = txtDescription.Text;
        report.Group = txtFolder.FolderName;
        report.ShowRulesWithoutViolationFlag = cbRulesWithoutViolation.Checked;
        report.ShowSummaryFlag = cbSummaryOnReport.Checked;
        report.AssignedPoliciesList = GetAssignedPolicyIds().ToList();

        using (var proxy = isLayer.Proxy)
        {
            if (_addMode)
            {
                return proxy.Cirrus.PolicyReports.AddPolicyReport(report, false);
            }

            proxy.Cirrus.PolicyReports.UpdatePolicyReport(report);
        }

        return report.ID;
    }

    private void SetReportFields()
    {
        
        txtFolder.FolderName = _folder;
        try
        {
            report = LoadReport();

            if (!_addMode)
            {
                Page.Title = string.Format(Resources.NCMWebContent.WEBDATA_VM_69, CommonHelper.EncodeXmlToString (report.Name) );
            }
            else
            {
                Page.Title = Resources.NCMWebContent.WEBDATA_VM_62;
            }

            if (!Page.IsPostBack)
            {
                txtReportName.Text = report.Name;
                txtDescription.Text = report.Comments;
                
                txtFolder.FolderName = _addMode ? _folder : report.Group;

                cbRulesWithoutViolation.Checked = report.ShowRulesWithoutViolationFlag;
                cbSummaryOnReport.Checked = report.ShowSummaryFlag;

                treeAssignedPolicies.DataSource = LoadPolicyByReport(report);
                treeAllPolicies.DataSource = ExecuteSWQL(@"SELECT P.PolicyID AS ID, P.Name, P.Grouping FROM Cirrus.Policies AS P");
            }
        }
        catch (Exception ex)
        {
            DisplayException(ex);
        }
    }

    private DataTable ExecuteSWQL(string swql)
    {
        using (var proxy = isLayer.Proxy)
        {
            var dt = proxy.Query(swql);
            return dt;
        }
    }

    private IEnumerable<Guid> GetAssignedPolicyIds()
    {
        var nodes = treeAssignedPolicies.GetAllNodes();
        if (nodes != null)
        {
            foreach (var node in nodes)
            {
                yield return node.ID;
            }
        }
    }

    private void DisplayException(Exception ex)
    {
        MainContent.Visible = false;
        ErrorControl.Visible = true;

        if (string.IsNullOrEmpty(Page.Request.QueryString[QUERY_ReportID_PARAM]))
        {
            Page.Title = Resources.NCMWebContent.CreateNewReport_PageTitle;
        }
        else
        {
            Page.Title = Resources.NCMWebContent.EditReport_PageTitle;
        }
        if (ex.GetType() == typeof(System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>))
        {
            btnSubmit.Visible = false;

            ErrorControl.VisiblePermissionLink = false;
            ErrorControl.ErrorMessage = (ex as System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>).Detail.Message;
        }
        else
        {
            btnSubmit.Visible = false;
            btnCancel.Visible = false;

            ErrorControl.ErrorMessage = ex.Message;
        }
    }

    private void ValidatePermissions()
    {
        if (!SecurityHelper.IsPermissionExist(SecurityHelper._canDownload))
        {
            if (string.IsNullOrEmpty(Page.Request.QueryString[QUERY_ReportID_PARAM]))
            {
                throw new InvalidOperationException(Resources.NCMWebContent.CreateNewReport_PermissionRequired);
            }
            else
            {
                throw new InvalidOperationException(Resources.NCMWebContent.EditReport_PermissionRequired);
            }
        }
    }

    #endregion

    public static List<string> GetAllFolders()
    {
        var isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            var dtFolders = proxy.Query(@"SELECT DISTINCT R.Grouping FROM Cirrus.PolicyReports AS R ORDER BY R.Grouping");

            var folders = new List<string>();
            foreach (DataRow row in dtFolders.Rows)
            {
                folders.Add(row["Grouping"].ToString());
            }

            return folders;
        }
    }

}
