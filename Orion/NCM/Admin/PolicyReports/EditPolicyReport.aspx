<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="EditPolicyReport.aspx.cs" Inherits="Orion_NCM_Resources_PolicyReports_EditPolicyReport" Title="<%$ Resources: NCMWebContent, WEBDATA_VY_21%>" %>

<%@ Register Src="~/Orion/NCM/Admin/PolicyReports/Controls/TreeViewer.ascx" TagName="PoliciesTree" TagPrefix="ncm" %>
<%@ Register Src="~/Orion/NCM/Admin/PolicyReports/Controls/Folder.ascx" TagName="Folder" TagPrefix="ncm" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/NCM/Controls/ErrorControl.ascx" TagPrefix="ncm" TagName="ErrorControl" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="adminPageTitlePlaceHolder">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="adminHelpIconPlaceHolder">
    <orion:IconHelpButton ID="btnHelp" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <style type="text/css">
        input[id$="txtReportName"] {
            padding: 2px;
        }
    </style>
    
    <ncm:ErrorControl ID="ErrorControl" runat="server" Visible="false" />
    <div style="padding: 10px;">
        <div id="MainContent" runat="server" style="border:solid 1px #dcdbd7;width:1100px;padding:10px;">
            <asp:UpdatePanel ID="uPanel" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <table id="reportHolder" runat="server" width="100%">
                        <tr>
                            <td style="padding: 0px 20px 5px 0px; width: 20%">
                                <%=Resources.NCMWebContent.WEBDATA_VY_22%>
                            </td>
                            <td style="padding: 0px 0px 5px 0px; width: 80%">
                                <asp:TextBox ID="txtReportName" runat="server" TextMode="SingleLine" Font-Size="12px" Font-Names="Arial" BorderColor="#89A6C0" BorderWidth="1px" Width="500px" Height="17px" MaxLength="200"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="validatorReportName" runat="server" Display="Dynamic"
                                    ControlToValidate="txtReportName" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VY_27%>">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px 20px 5px 0px; width: 20%">
                                <%=Resources.NCMWebContent.WEBDATA_VY_24%>
                            </td>
                            <td style="padding: 5px 0px 5px 0px; width: 80%">
                                <asp:TextBox ID="txtDescription" style="resize: vertical;" runat="server" TextMode="MultiLine" Font-Size="12px" Font-Names="Arial" BorderColor="#89A6C0" BorderWidth="1px" Width="500px" Height="50px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <ncm:Folder ID="txtFolder" runat="server" MaxLength="200" />
                
                    <table width="100%">
                        <tr>
                            <td style="padding: 5px 20px 5px 0px; width: 20%" valign="top">
                                <%=Resources.NCMWebContent.WEBDATA_VY_23%>
                            </td>
                            <td style="padding: 5px 0px 5px 0px; width: 80%">
                                <asp:CheckBox ID="cbSummaryOnReport" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VY_28%>" />
                                <br />
                                <asp:CheckBox ID="cbRulesWithoutViolation" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VY_29%>" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table style="background-color: #e4f1f8;">
                                    <tr>
                                        <td colspan="3">
                                            <div id="LinkHolder" runat="server" style="padding: 5px 10px 0px 10px;">
                                                <table width="100%" style="background-color: #FFFDCC;">
                                                    <tr>
                                                        <td style="padding: 15px 15px 15px 0px;">
                                                            <img style="padding:0px 5px 0px 5px;" src="/Orion/NCM/Resources/images/lightbulb_tip_16x16.gif"/><b><%=Resources.NCMWebContent.WEBDATA_VY_30%></b> &#0187; <a style="color:#336699;" href="/Orion/NCM/Admin/PolicyReports/EditPolicy.aspx" ><%=Resources.NCMWebContent.WEBDATA_VY_31%></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    
                                    </tr>
                                    
                                    <tr>
                                        <td style="padding: 10px 10px 10px 10px;">
                                            <div style="padding-bottom: 5px;">
                                                <%=Resources.NCMWebContent.WEBDATA_VY_25%>
                                            </div>
                                          
                                            <div>
                                                <ncm:PoliciesTree ID="treeAllPolicies" NodeImageUrl="/Orion/NCM/Resources/images/PolicyIcons/Policy.Icon.gif" runat="server" />
                                            </div>
                                        </td>
                                        <td style="padding: 10px 0px 10px 0px;">
                                            <div>
                                                <orion:LocalizableButton ID="btnAddPolicies" runat="server" LocalizedText="customtext"   Text="<%$ Resources: NCMWebContent, WEBDATA_VM_63%>" OnClick="btnAddPolicies_Click" CausesValidation="false"/>
                                            </div>
                                            <div>
                                                <orion:LocalizableButton ID="btnRemovePolicies" runat="server" LocalizedText="customtext"  Text="<%$ Resources: NCMWebContent, WEBDATA_VM_64%>" OnClick="btnRemovePolicies_Click" CausesValidation="false"/>
                                            </div>
                                        </td>
                                        <td style="padding: 10px 10px 10px 10px;">
                                            <div style="padding-bottom: 5px;">
                                                <%=Resources.NCMWebContent.WEBDATA_VY_26%>
                                            </div>
                                            <div>
                                                <ncm:PoliciesTree ID="treeAssignedPolicies" NodeImageUrl="/Orion/NCM/Resources/images/PolicyIcons/Policy.Icon.gif" runat="server" Expanded="true" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <table width="100%">
            <tr>
                <td style="padding-top: 10px;">
                    
                    <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="submit" DisplayType="Primary" OnClick="btnSubmit_Click"  />
                    <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" DisplayType="secondary" OnClick="btnCancel_Click" CausesValidation="false"  />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
