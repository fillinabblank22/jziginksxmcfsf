﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="PolicyRuleViolation.aspx.cs" Inherits="Orion_NCM_Admin_PolicyReports_PolicyRuleViolation" %>


<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminBodyPlaceholder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/Settings/Settings.css" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminHelpIconPlaceHolder" Runat="Server">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMPHCustomizingViolationLevels" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="adminPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <asp:Panel ID="ContentContainer" CssClass="ContentContainer" runat="server">
        <div class="SettingContainer">
            <div class="SettingBlockHeader" style="border-bottom:0px;padding-bottom:0px;"><%=Resources.NCMWebContent.ManageViolationLevels_CustomizeLabel %></div>
            <div style="padding-top:10px;">
             <img src="/Orion/NCM/Resources/images/PolicyIcons/Policy.Info.gif"><span style="padding-left: 5px;padding-right: 5px;"><%=Resources.NCMWebContent.ManageViolationLevels_ViolationLevel1 %></span><asp:TextBox runat="server" ID="txtLevel1"></asp:TextBox>
                <asp:RequiredFieldValidator Display="Dynamic" ID="rfv1" runat="server" ControlToValidate="txtLevel1" ErrorMessage="*" />
            </div>
             <div style="padding-top:10px;">
             <img src="/Orion/NCM/Resources/images/PolicyIcons/Policy.Warning.gif"><span style="padding-left: 5px;padding-right: 5px;"><%=Resources.NCMWebContent.ManageViolationLevels_ViolationLevel2 %></span><asp:TextBox runat="server" ID="txtLevel2"></asp:TextBox>
                 <asp:RequiredFieldValidator Display="Dynamic" ID="rfv2" runat="server" ControlToValidate="txtLevel2" ErrorMessage="*" />
            </div>
             <div style="padding-top:10px;">
             <img src="/Orion/NCM/Resources/images/PolicyIcons/Policy.Critical.gif"><span style="padding-left: 5px;padding-right: 5px;"><%=Resources.NCMWebContent.ManageViolationLevels_ViolationLevel3 %></span><asp:TextBox runat="server" ID="txtLevel3"></asp:TextBox>
                 <asp:RequiredFieldValidator Display="Dynamic" ID="rfv3" runat="server" ControlToValidate="txtLevel3" ErrorMessage="*" />
            </div>
        </div>
        
        <div style="padding-top:20px;">
            <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" 
            OnClick="btnSubmit_Click" DisplayType="Primary"
            CommandArgument="~/Orion/NCM/Admin/Default.aspx"/> 
            <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" 
            OnClick="btnCancel_Click" DisplayType="Secondary"
            CommandArgument="~/Orion/NCM/Admin/Default.aspx"/>
        </div>
    </asp:Panel>
</asp:Content>

