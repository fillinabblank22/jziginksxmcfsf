using System;
using System.IO;
using System.Text.RegularExpressions;

using SolarWinds.NCMModule.Web.Resources.PolicyReportsManagement;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_Resources_PolicyReports_ImportPolicyReport : System.Web.UI.Page
{
    private SolarWinds.NCMModule.Web.Resources.ISWrapper isLayer;
    private string policyNames;

    public string PolicyNames
    {
        get { return policyNames; }
        set { policyNames = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_VM_67;

        isLayer = new SolarWinds.NCMModule.Web.Resources.ISWrapper();

        Submit.Visible = !CommonHelper.IsDemoMode();
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (CommonHelper.IsDemoMode()) return;

        var display_warning = false;

        if (uploadPolicyReport.HasFile)
        {
            var streamReader = new StreamReader(uploadPolicyReport.FileContent);
            var fileContent = streamReader.ReadToEnd();

            try
            {
                var toImportPolicyReport = ImportExportPolicyReport.ImportPolicyReport(fileContent);
                Session[@"NCM_ImportPolicyReport"] = toImportPolicyReport;

                var comma = string.Empty;
                foreach (var policy in toImportPolicyReport.AssignedPolicies)
                {
                    if (policy.NodeSelectionString.StartsWith(@"Nodes:", StringComparison.InvariantCultureIgnoreCase) ||
                        policy.NodeSelectionString.StartsWith(@"NCM_Nodes:", StringComparison.InvariantCultureIgnoreCase))
                    {
                        display_warning = true;
                        policyNames += $@"{comma}{policy.PolicyName}";
                        comma = @", ";
                    }
                }

                if (display_warning)
                {
                    System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), @"key_display_warning_dialog", @"self.setTimeout('$(""#display_warning_dialog"").click()', 1500);", true);
                }
                else
                {
                    ImportClick(null, null);
                }
            }
            catch
            {
                validatorPolicyReportFile.IsValid = false;
            }
        }
        else
        {
            validatorPolicyReportFile.IsValid = false;
        }
    }

    protected void ImportClick(object sender, EventArgs e)
    {
        var toImportPolicyReport = (PolicyReport)Session[@"NCM_ImportPolicyReport"];
        foreach (var policy in toImportPolicyReport.AssignedPolicies)
        {
            if (policy.NodeSelectionString.StartsWith(@"Nodes:", StringComparison.InvariantCultureIgnoreCase) ||
                policy.NodeSelectionString.StartsWith(@"NCM_Nodes:", StringComparison.InvariantCultureIgnoreCase))
            {
                policy.NodeSelectionString = @"*:";
            }
            else if (policy.NodeSelectionString.StartsWith(@"Criteria:", StringComparison.InvariantCultureIgnoreCase))
            {
                policy.NodeSelectionString = Regex.Replace(policy.NodeSelectionString, @"\bNodes\b", "NCM_Nodes", RegexOptions.IgnoreCase);
            }
        }
        Session[@"NCM_ImportPolicyReport"] = null;

        using (var proxy = isLayer.Proxy)
        {
            proxy.Cirrus.PolicyReports.AddPolicyReport(toImportPolicyReport, true);
        }

        Response.Redirect("PolicyReportManagement.aspx");
    }

    protected void CancelClick(object sender, EventArgs e)
    {
        Response.Redirect("PolicyReportManagement.aspx");
    }
}
