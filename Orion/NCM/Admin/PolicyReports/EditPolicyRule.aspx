<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="EditPolicyRule.aspx.cs" Inherits="Orion_NCM_Resources_PolicyReports_EditPolicyRule" Title="<%$ Resources: NCMWebContent, WEBDATA_VY_21%>" %>

<%@ Register Src="~/Orion/NCM/Admin/PolicyReports/Controls/Folder.ascx" TagName="Folder" TagPrefix="ncm" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/NCM/Controls/ErrorControl.ascx" TagPrefix="ncm" TagName="ErrorControl" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.PolicyReportsManagement.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register TagPrefix="ncm" TagName="LoadScript" Src="~/Orion/NCM/Resources/Jobs/Controls/LightLoadScriptControl.ascx" %>

<%@ Import Namespace="SolarWinds.Orion.Web.Helpers"%>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="adminPageTitlePlaceHolder">
	<%=Page.Title%>
</asp:Content>

<asp:Content ID ="Content2" runat="server" ContentPlaceHolderID="adminHelpIconPlaceHolder">
    <orion:IconHelpButton ID="btnHelp" runat="server" />
</asp:Content>

<asp:Content ID="Conten3" ContentPlaceHolderID="adminContentPlaceholder" runat="server">    
	<orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="styles/ProgressIndicator.css" />

	<link rel="stylesheet" type="text/css" href="../../styles/NCMResources.css" />
    
    <script type="text/javascript" src="../../JavaScript/main.js"></script> 
	<script type="text/javascript" src="TestPolicyRule.js"></script> 
	
    <script type="text/javascript">
	    $(document).ready(function () {
	        var scriptType = $('#<%=rbCLI.ClientID%>').is(':checked') ? 'cli' : 'cct';
	        remediateScriptTypeClick(scriptType);
	    });

	    function showTooltip(on, id) {
	        var div = document.getElementById(id);
	        if (on) {
	            div.style.display = 'block';
	        }
	        else {
	            div.style.display = 'none';
	        }
	    }

	    var remediateScriptTypeClick = function (scriptType) {
	        if (scriptType == 'cct') {
	            $('#<%=chkExecuteScriptAutomatically.ClientID %>').removeAttr('checked');
	            $('#<%=chkExecuteScriptInConfigMode.ClientID %>').removeAttr('checked');
	            $('#ExecuteScriptAutomaticallyHolder').hide();
	        } else {
	            $('#ExecuteScriptAutomaticallyHolder').show();
	        }

	        toogleExecuteRemediationScriptPerBlock();
	    }

	    var toogleExecuteRemediationScriptPerBlock = function () {
	        var rbConfigBlockIsCh = $('#<%=rbConfigBlock.ClientID %>').is(':checked');
	        var rbCliIsCh = $('#<%=rbCLI.ClientID %>').is(':checked');
	        var chkAdvMode = $('#<%=chkUseAdvancedMode.ClientID %>').is(':checked');
	        var show = (chkAdvMode && rbConfigBlockIsCh && rbCliIsCh);
	        $('#ExecuteRemediationScriptPerBlock').toggle(show);
	        if (!show)
	            $('#<%= chkExecuteRemediationScriptPerBlock.ClientID %>').removeAttr('checked');
	    }

	    var executeRemediationScriptPerBlockClick = function (checked) {
	        if (checked) {
	            var script = LoadScriptControl.getScript();
	            LoadScriptControl.setScript('${ConfigBlockStartLine}\n' + script);
	        }
	        else {
	            var script = LoadScriptControl.getScript();
	            LoadScriptControl.setScript(script.replace(/\${ConfigBlockStartLine}(\r\n|\n|)/gi, ''));
	        }
	    }
	</script>
    
    <style type="text/css">
        .HiddenFinishCompleteButtonStyle { display: none; }
        .load-script-border { border: 1px solid #89A6C0; }
        input[id$="txtPolicyRuleName"] {
            padding: 2px;
        }
    </style>

    <![if !IE]>
    <style type="text/css">        
        .Space label { padding-left: 3px; }
    </style>
    <![endif]>
    
	<ncm:ErrorControl ID="ErrorControl" runat="server" Visible="false" />
	<div style="padding:10px;">
        <div id="MainContent" runat="server">
            <div id="infoHolder" runat="server" style="padding:0px 0px 5px 0px;">
                <%=Resources.NCMWebContent.WEBDATA_VY_42%>
            </div>
            <div id="policyRuleHolder" runat="server" style="border:solid 1px #dcdbd7;width:1100px;padding:10px;background-color:#fff;">
                <div style="padding:10px 0px 5px 0px;"><b><%=Resources.NCMWebContent.WEBDATA_VY_43%></b></div>
                <div style="border-bottom:solid 1px #dcdbd7;"></div>
                <table cellpadding="0" cellspacing="0" width="100%" style="">
                    <tr>
                        <td style="padding:10px 0px 5px 10px;width:20%;">
                            <%=Resources.NCMWebContent.WEBDATA_VY_44%>
                        </td>
                        <td style="padding:10px 0px 5px 7px;width:80%;">
                            <asp:TextBox ID="txtPolicyRuleName" runat="server" TextMode="SingleLine" Font-Size="12px" Font-Names="Arial" BorderColor="#89A6C0" BorderWidth="1px" Width="500px" Height="18px" MaxLength="200"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="validatorPolicyRuleName" runat="server" Display="Dynamic" ControlToValidate="txtPolicyRuleName" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VY_69%>" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:5px 0px 5px 10px;width:20%;">
                            <%=Resources.NCMWebContent.WEBDATA_VY_24%>
                        </td>
                        <td style="padding:10px 0px 5px 7px;width:80%;">
                            <asp:TextBox style="resize: vertical;" ID="txtDescription" runat="server" TextMode="MultiLine" Font-Size="12px" Font-Names="Arial" BorderColor="#89A6C0" BorderWidth="1px" Width="500px" Height="50px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:5px 0px 5px 10px;width:20%;">
                            <%=Resources.NCMWebContent.WEBDATA_VY_45%>
                        </td>
                        <td>
                            <table width="500px;">
                                <tr>
                                    <td style="width:30%;padding:5px 0px 5px 5px">
                                        <asp:RadioButton ID="rbInformational" CssClass="Space" GroupName="ErrorLevel" runat="server" />
                                    </td>
                                    <td style="width:40%;padding:5px 0px 5px 0px">
                                        <asp:RadioButton ID="rbWarning" CssClass="Space" GroupName="ErrorLevel" runat="server" />
                                    </td>
                                    <td style="width:30%;padding:5px 0px 5px 0px">
                                        <asp:RadioButton ID="rbCritical" CssClass="Space" GroupName="ErrorLevel" runat="server" />
                                    </td>
                                </tr>    
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left:7px;">
                            <ncm:Folder ID="txtFolder" runat="server" MaxLength="200" />        
                        </td>
                    </tr>
                </table>
                
                <div style="padding:20px 0px 5px 0px;"><b><%=Resources.NCMWebContent.WEBDATA_VY_46%></b></div>
                <div style="border-bottom:solid 1px #dcdbd7;"></div> 
                    
                <asp:UpdatePanel ID="Panel_PolicyRule" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                        <div style="padding-top: 5px;"></div> <%--empty div--%>
                        
                        <div style="padding: 5px 0px 5px 0px;">
                            <%= Resources.NCMWebContent.WEBDATA_VY_47 %>
                            <span style="padding-left: 10px;"></span><asp:RadioButton ID="rbPatternIsFound" CssClass="Space" GroupName="Alert" Text="<%$ Resources: NCMWebContent, WEBDATA_VY_74%>" runat="server" />
                            <span style="padding-left: 10px;"></span><asp:RadioButton ID="rbPatternIsNOTFound" CssClass="Space" GroupName="Alert" Text="<%$ Resources: NCMWebContent, WEBDATA_VY_73%>" runat="server" />
                        </div>

                        <div style="padding: 5px 0px 5px 0px;">
                            <asp:CheckBox ID="chkUseAdvancedMode" AutoPostBack="true" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VY_75%>" OnCheckedChanged="chkUseAdvancedMode_OnCheckedChanged" onchange="toogleExecuteRemediationScriptPerBlock()"/>
                        </div>
                        
                        <table>
                            <tr>
                                <td id="AdvancedModeHolder1" runat="server" style="padding: 10px 0px 5px 10px; background-color: white;">
                                    <ncm:MultiLinePolicyRulePatterns ID="multiLineRulePatterns" runat="server" />
                                    <asp:CustomValidator ID="validatorMultiLineRulePatterns" runat="server" Display="Dynamic" />
                                </td>
                            </tr>
                        </table>
                        
                        <div id="AdvancedModeHolder2" runat="server">
                            <div style="padding: 20px 0px 5px 0px;"><b><%= Resources.NCMWebContent.WEBDATA_VY_48 %></b></div>
                            <div style="border-bottom: solid 1px #dcdbd7;"></div> 
                            
                            <table>
                                <tr>
                                    <td style="padding: 10px 0px 5px 10px;">
                                        <%= Resources.NCMWebContent.WEBDATA_VY_49 %>
                                        <span style="padding-left: 10px;"></span><asp:RadioButton ID="rbEntireConfigFile" CssClass="Space" AutoPostBack="true" GroupName="SerachIn" Text="<%$ Resources: NCMWebContent, WEBDATA_VY_78%>" runat="server" OnCheckedChanged="rbEntireConfigFile_CheckedChanged" onchange="toogleExecuteRemediationScriptPerBlock()"/>
                                        <span style="padding-left: 10px;"></span><asp:RadioButton ID="rbConfigBlock" CssClass="Space" AutoPostBack="true" GroupName="SerachIn" Text="<%$ Resources: NCMWebContent, WEBDATA_VY_77%>" runat="server" OnCheckedChanged="rbConfigBlock_CheckedChanged" onchange="toogleExecuteRemediationScriptPerBlock()"/>
                                    </td>
                                </tr>
                            </table>
                            
                            <div id="ConfigBlockHolder" runat="server" style="padding: 5px 10px 0px 20px;">
                                <div style="padding: 0px 0px 5px 0px;">
                                    <%= Resources.NCMWebContent.WEBDATA_VY_50 %>
                                </div>
                                <table cellpadding="0" cellspacing="0" width="100%" style="background-color: #e4f1f8;">
                                    <tr>                    
                                        <td style="padding: 10px 10px 10px 10px; width: 140px;" valign="top">
                                            <%= Resources.NCMWebContent.WEBDATA_VY_51 %>
                                        </td>
                                        <td align="left" style="padding: 10px 10px 0px 10px; width: 500px;">
                                            <ncm:CrLfTextBox ID="txtConfigBlockStart" runat="server" TextMode="MultiLine" Font-Size="12px" Font-Names="Arial" BorderColor="#89A6C0" BorderWidth="1px" Width="500px" Height="50px" onfocus="showTooltip(true, 'blockStartTooltip');" onblur="showTooltip(false, 'blockStartTooltip');"></ncm:CrLfTextBox>
                                            <asp:RequiredFieldValidator ID="validatorConfigBlockStart" runat="server" Display="Dynamic" ControlToValidate="txtConfigBlockStart" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VY_79%>" />
                                        </td>
                                        <td valign="top" style="padding: 10px 10px 10px 0px;">
                                            <div id="blockStartTooltip" style="display: none; text-align: left; z-index: 1000; position: static; color: gray; background-color: transparent; vertical-align: top;">
                                                <%= Resources.NCMWebContent.WEBDATA_VY_52 %>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td style="padding: 0px 10px 10px 10px;"><span style="font-size: 7pt; color: Gray;"><%= Resources.NCMWebContent.WEBDATA_VY_53 %> <a href="<%= LearnMore(@"OrionNCMWebManagePolicyReportsCreateRule") %>" target="_blank" style="color: Gray; text-decoration: underline;"><%= Resources.NCMWebContent.WEBDATA_VY_54 %></a></span></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px 10px 10px 10px; width: 140px;" valign="top" align="left">
                                            <%= Resources.NCMWebContent.WEBDATA_VY_55 %>
                                        </td>
                                        <td align="left" style="padding: 0px 10px 0px 10px; width: 500px;">
                                            <ncm:CrLfTextBox ID="txtConfigBlockEnd" runat="server" TextMode="MultiLine" Font-Size="12px" Font-Names="Arial" BorderColor="#89A6C0" BorderWidth="1px" Width="500px" Height="50px" onfocus="showTooltip(true, 'blockEndTooltip');" onblur="showTooltip(false, 'blockEndTooltip');"></ncm:CrLfTextBox>                                            
                                            <asp:RequiredFieldValidator ID="validatorConfigBlockEnd" runat="server" Display="Dynamic" ControlToValidate="txtConfigBlockEnd" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VY_80%>" />
                                        </td>
                                        <td valign="top" style="padding: 0px 10px 0px 0px;">
                                            <div id="blockEndTooltip" style="display: none; text-align: left; z-index: 1000; position: static; color: gray; background-color: transparent; vertical-align: top;">
                                                <%= Resources.NCMWebContent.WEBDATA_VY_56 %>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td style="padding: 0px 10px 10px 10px;"><span style="font-size: 7pt; color: Gray;"><%= Resources.NCMWebContent.WEBDATA_VY_57 %> <a href="<%= LearnMore(@"OrionNCMWebManagePolicyReportsCreateRule") %>" target="_blank" style="color: Gray; text-decoration: underline;"><%= Resources.NCMWebContent.WEBDATA_VY_54 %></a></span></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px 10px 10px 10px; width: 140px;" valign="top" align="left">
                                            <%= Resources.NCMWebContent.WEBDATA_VY_58 %>
                                        </td>
                                        <td align="left" style="padding: 0px 10px 10px 10px;" colspan="2">
                                            <asp:RadioButton ID="rbCBRegExExpression" CssClass="Space" GroupName="ConfigBlockStringType" Text="<%$ Resources: NCMWebContent, WEBDATA_VY_82%>" runat="server" />
                                            <span style="padding-left: 10px;"><asp:RadioButton ID="rbCBFindString" CssClass="Space" GroupName="ConfigBlockStringType" Text="<%$ Resources: NCMWebContent, WEBDATA_VY_81%>" runat="server" /></span>
                                            <span style="padding-left: 100px;"><asp:CheckBox ID="chkConfigBlockMustExist" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_966%>" /></span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        
                        <div id="SimpleModeHolder" runat="server" style="padding: 10px 10px 10px 20px;">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>                   
                                    <td style="padding: 10px 10px 10px 10px; width: 140px;" valign="top">
                                        <%= Resources.NCMWebContent.WEBDATA_VY_59 %>
                                    </td>
                                    <td align="left" style="padding: 10px 10px 10px 10px;">
                                        <ncm:CrLfTextBox ID="txtPattern" runat="server" TextMode="MultiLine" Font-Size="12px" Font-Names="Arial" BorderColor="#89A6C0" BorderWidth="1px" Width="500px" Height="50px"></ncm:CrLfTextBox>
                                        <br />
                                        <asp:RequiredFieldValidator ID="validatorPattern" runat="server" Display="Dynamic" ControlToValidate="txtPattern" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VY_83%>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0px 10px 10px 10px; width: 140px;" valign="top">
                                        <%= Resources.NCMWebContent.WEBDATA_VY_60 %>
                                    </td>
                                    <td style="padding: 0px 10px 10px 10px;" align="left">
                                        <asp:RadioButton ID="rbRegExExpression" CssClass="Space" GroupName="StringType" Text="<%$ Resources: NCMWebContent, WEBDATA_VY_82%>" runat="server" />
                                        <span style="padding-left: 10px;"></span><asp:RadioButton ID="rbFindString" CssClass="Space" GroupName="StringType" Text="<%$ Resources: NCMWebContent, WEBDATA_VY_81%>" runat="server" />
                                        <br /><span style="font-size: 7pt; color: Gray;"><%= Resources.NCMWebContent.WEBDATA_VY_61 %> <a href="<%= LearnMore(@"OrionNCMWebRegularExpressions") %>" target="_blank" style="color: Gray; text-decoration: underline;"><%= Resources.NCMWebContent.WEBDATA_VY_62 %></a><%= Resources.NCMWebContent.WEBDATA_VY_63 %><br /> <%= Resources.NCMWebContent.WEBDATA_VY_64 %></span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        
                        <div style="padding: 20px 0px 5px 0px;"><b><%= Resources.NCMWebContent.WEBDATA_VY_65 %></b></div>
                        <div style="border-bottom: solid 1px #dcdbd7;"></div> 
                        
                    </ContentTemplate>
                </asp:UpdatePanel>

                <table cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td style="padding: 10px 10px 10px 10px; width: 150px;" valign="top">
                                <%= Resources.NCMWebContent.WEBDATA_VY_66 %>
                            </td>
                            <td style="padding: 10px 10px 0px 10px;" align="left">
                                <ncm:LoadScript ID="loadScriptCtrl" runat="server" CssClass="load-script-border" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="padding: 0px 10px 10px 10px;">
                                <span style="font-size: 7pt; color: Gray;"><%= Resources.NCMWebContent.WEBDATA_VY_67 %> <a href="<%= LearnMore(@"OrionNCMWebRemediation")%>" target="_blank" style="color: Gray; text-decoration: underline;"><%= Resources.NCMWebContent.WEBDATA_VY_68 %></a></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 0px 10px 10px 10px; width: 150px;">
                                <%= Resources.NCMWebContent.EditRule_RemediateScriptType_Label %>
                            </td>
                            <td style="padding: 0px 10px 10px 10px;" align="left">
                                <asp:RadioButton ID="rbCLI" CssClass="Space" GroupName="RemediateScriptType" Text="<%$ Resources: NCMWebContent, EditRule_CLI_RadioButtonText%>" runat="server" onclick="remediateScriptTypeClick('cli');"/>
                            </td>
                        </tr>
                    </tbody>
                    <tbody id="ExecuteScriptAutomaticallyHolder">
                        <tr>
                            <td></td>
                            <td style="padding: 0px 10px 10px 30px;">
                                <asp:CheckBox ID="chkExecuteScriptAutomatically" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_1015%>" />
                            </td>
                        </tr>
                        <tr id="ExecuteRemediationScriptPerBlock">
                            <td></td>
                            <td style="padding: 0px 10px 10px 30px;">
                                <asp:CheckBox ID="chkExecuteRemediationScriptPerBlock" runat="server" Text="<%$ Resources: NCMWebContent, EditRule_RunScriptOnEachConfigBlock_CheckboxText%>" onclick="executeRemediationScriptPerBlockClick(this.checked);" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="padding: 0px 10px 10px 30px;">
                                <asp:CheckBox ID="chkExecuteScriptInConfigMode" runat="server" Text="<%$ Resources: NCMWebContent, EditRule_ExecuteScriptInConfigMode_CheckboxText %>" />
                            </td>
                        </tr>
                    </tbody>
                    <tbody>
                        <tr>
                            <td></td>
                            <td>
                                <span style="padding-left: 10px;"></span><asp:RadioButton ID="rbCCT" CssClass="Space" GroupName="RemediateScriptType" Text="<%$ Resources: NCMWebContent, EditRule_ConfigChangeTemplate_RadioButtonText%>" runat="server" onclick="remediateScriptTypeClick('cct');" />
                            </td> 
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
	    <asp:UpdatePanel ID="Panel_Buttons" UpdateMode="Conditional" runat="server">
	        <ContentTemplate>
	            <table>
	                <tr>
	                    <td style="padding-top: 10px;">
	                        <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="submit" DisplayType="Primary" OnClick="btnSubmit_Click"  />
	                        <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" DisplayType="secondary" OnClick="btnCancel_Click" CausesValidation="false"  />
	                    </td>
	                    <td style="padding-top: 10px; padding-left: 20px;">
	                        <orion:LocalizableButton runat="server" ID="btnTestPolicyRule" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_VM_65 %>" OnClick="btnTestPolicyRule_Click" CausesValidation="true"/>
	                    </td>
	                </tr>
	            </table>
	        </ContentTemplate>
	    </asp:UpdatePanel>
    </div>
    
    <div id="dialogContainer" class="x-hidden">
        <input id="display_test_policy_rule_dialog" onclick="return TestPolicyRule.TestPolicyRule_Click();" type="button" style="display:none;" />
        <div id="mainContainer" class="x-panel-body" style="height:460px;width:680px;overflow:auto;">
            <asp:UpdatePanel ID="Panel_TestPolicyRule" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <table width="660px;">
                        <tr>
                            <td style="padding:5px;">
                                <ncm:TestPolicyRule ID="TestPolicyRule" runat="server" />                                
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    
</asp:Content>

