<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="PolicyRuleManagement.aspx.cs" Inherits="Orion_NCM_Resources_PolicyReports_PolicyRuleManagement" Title="<%$ Resources: NCMWebContent, WEBDATA_VY_12%>" %>

<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register Src="~/Orion/NCM/CustomPageLinkHidding.ascx" TagPrefix="ncm" TagName="custPageLinkHidding" %>
<%@ Register TagPrefix="ncm1" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register TagPrefix="ncm" Src="~/Orion/NCM/NavigationTabBar.ascx"  TagName="NavigationTabBar" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/NCM/Controls/ErrorControl.ascx" TagPrefix="ncm" TagName="ErrorControl" %>

<asp:Content ID="PageTitleContent" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
    
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <ncm:custPageLinkHidding ID="CustPageLinkHidding2"  runat="server" />
    <ncm1:ConfigSnippetUISettings ID="PolicyRuleUISetting" runat="server" Name="NCM_PolicyRuleMgmt_Columns" DefaultValue="[]" RenderTo="columnModel" />
    <ncm1:ExtLocalDateTimePattern ID="ExtLocalDateTimePattern1" runat="server"/>
    
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/PolicyReports/PolicyReport.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
    
    <script type="text/javascript">
        var IsDemoMode = <%=SolarWinds.NCMModule.Web.Resources.CommonHelper.IsDemoMode().ToString().ToLowerInvariant() %>;
    </script>
    
    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Admin/PolicyReports/PolicyRuleManagement.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippetsHelper.js"></script>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="adminPageTitlePlaceHolder">
	<%=Page.Title%>
</asp:Content>

<asp:Content ID ="Content2" runat="server" ContentPlaceHolderID="adminHelpIconPlaceHolder">
    <orion:IconHelpButton ID="helpBtn" runat="server" HelpUrlFragment="OrionNCMWebManagePolicyReportsRules" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <%foreach (string setting in new string[] { @"GroupByValue", @"GroupBy" }) { %>
		<input type="hidden" name="NCM_PolicyRuleMgmt_<%=setting%>" id="NCM_PolicyRuleMgmt_<%=setting%>" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get($@"NCM_PolicyRuleMgmt_{setting}")%>' />		
	<%}%>
	
    <asp:Panel id="ContentContainer" runat="server">
        <div style="padding:10px;">
            <ncm:NavigationTabBar ID="NavigationTabBar1" runat="server"/>
            <div id="tabPanel" class="tab-top">
                <table class="ExistingElements" cellpadding="0" cellspacing="0" width="100%">
                    <tr valign="top" align="left">
                        <td style="width: 220px;">
                            <div class="ElementGrouping">
                                <div class="ncm_GroupSection x-panel-header x-toolbar x-small-editor">
                                    <div><%=Resources.NCMWebContent.WEBDATA_VY_11%></div>
                                    <div id="GroupBy" style="padding:2px 0px 3px 0px;"></div>    
                                </div>
                                <div>
                                    <ul class="GroupItems" style="width:220px;"></ul>
                                </div>
                            </div>
                        </td>
                        <td id="gridCell">
                            <div id="Grid" />
                        </td>
                    </tr>
                </table>
                <div id="originalQuery">
                </div>
                <div id="test">
                </div>
                <pre id="stackTrace"></pre>
            </div>
            <div id="snippetDescriptionDialog" class="x-hidden">
                <div id="snippetDescriptionBody" class="x-panel-body" style="padding: 10px; height: 210px; overflow: auto;"></div>
            </div>
        </div>
    </asp:Panel>
    <div style="padding:10px;" id="ErrorContainer" runat="server"/>
    <ncm:ErrorControl ID="ErrorControl" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VY_13%>" runat="server" Visible="false" />
</asp:Content>

