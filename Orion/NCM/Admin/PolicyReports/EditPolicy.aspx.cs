using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.HtmlControls;

using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Cirrus.IS.Client;

public partial class Orion_NCM_Resources_PolicyReports_EditPolicy : System.Web.UI.Page
{
    private const string QUERY_POLICYID_PARAM = "PolicyID";
    private const string QUERY_FOLDER_PARAM = "Folder";
    private const string QUERY_ISCOPY_PARAM = "IsCopy";

    private Guid? _policyID = null;
    private string _folder = string.Empty;
    private bool _addMode;
    private bool hasPerm = true;
    private ISWrapper isLayer = new ISWrapper();

    private bool IsCopy
    {
        get
        {
            var isCopy = false;
            if (!string.IsNullOrEmpty(Page.Request.QueryString[QUERY_ISCOPY_PARAM]))
                isCopy = string.Equals(Page.Request.QueryString[QUERY_ISCOPY_PARAM].ToLowerInvariant(), @"true");
            return isCopy;
        }
    }

    private Guid? GetPolicyIdParameterValue()
    {
        var value = Page.Request.QueryString[QUERY_POLICYID_PARAM];
        Guid id;
        if (!string.IsNullOrWhiteSpace(value) && Guid.TryParse(value, out id))
        {
            return id;
        }

        return null;
    }

    protected override void OnInit(EventArgs e)
    {
        btnSubmit.Visible = !CommonHelper.IsDemoMode();

        try
        {
            _policyID = GetPolicyIdParameterValue();
            _folder = Page.Request.QueryString[QUERY_FOLDER_PARAM];

            if (!_policyID.HasValue)
            {
                _addMode = true;
                btnHelp.HelpUrlFragment = @"OrionNCMWebManagePolicyReportsCreatePolicy";
            }
            else
            {
                _addMode = false;
                btnHelp.HelpUrlFragment = @"OrionNCMWebManagePolicyReportsEditPolicy";
            }

            ValidatePermissions();

            var folders = GetAllFolders();
            txtFolder.DataSource = folders;
            base.OnInit(e);

        }
        catch (InvalidOperationException ex)
        {
            hasPerm = false;
            DisplayException(ex);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (hasPerm)
            LoadPolicy();
    }

    protected void btnAddPolicyRules_Click(object sender, EventArgs e)
    {
        var nodes = treeAllPolicyRules.GetSelectedNodes();
        if (nodes != null)
            treeAssignedPolicyRules.AddNodes(nodes);
    }

    protected void btnRemovePolicyRules_Click(object sender, EventArgs e)
    {
        treeAssignedPolicyRules.RemoveSelectedNodes();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (CommonHelper.IsDemoMode()) return;

        try
        {
            var id = SavePolicy();
            Response.Redirect("PolicyManagement.aspx?opento=" + id);
        }
        catch (Exception ex)
        {
            DisplayException(ex);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        var paramValue = GetPolicyIdParameterValue();
        if (IsCopy && paramValue.HasValue)
        {
            using (var proxy = isLayer.Proxy)
            {
                proxy.Cirrus.PolicyReports.DeletePolicies(
                    new[] { paramValue.Value }, true);
            }
        }
        Response.Redirect("PolicyManagement.aspx");
    }

    protected void btnResetToAllNodes_Click(object sender, EventArgs e)
    {
        nodeSelectionCriteria.ResetToAllNodes();
        nodeSelectionCriteria.Visible = true;
        nodeSelectionErrorControl.Visible = false;
        btnSubmit.Visible = true;
    }

    #region Private Members

    private void ValidatePermissions()
    {
        if (!SecurityHelper.IsPermissionExist(SecurityHelper._canDownload))
        {
            if (string.IsNullOrEmpty(Page.Request.QueryString[QUERY_POLICYID_PARAM]))
            {
                throw new InvalidOperationException(Resources.NCMWebContent.WEBDATA_VM_75);
            }
            else
            {
                throw new InvalidOperationException(Resources.NCMWebContent.WEBDATA_VM_76);
            }
        }
    }

    private void LoadPolicy()
    {
        try
        {
            var policy = LoadPolicy(_policyID);
            if (_addMode)
            {
                Page.Title = Resources.NCMWebContent.CreateNewPolicy_PageTitle;
            }
            else
            {
                Page.Title = string.Format(Resources.NCMWebContent.EditPolicyFormat_PageTitle, CommonHelper.EncodeXmlToString(policy.PolicyName));
            }

            if (!Page.IsPostBack)
            {
                txtPolicyName.Text = policy.PolicyName;
                txtDescription.Text = policy.Comments;
                txtFolder.FolderName = _addMode ? _folder : policy.Grouping;
                ddlConfigType.SelectedValue = string.IsNullOrEmpty(policy.ConfigTypes) ? @"Any" : policy.ConfigTypes;
                nodeSelectionCriteria.SelectionCriteria = GetSelectionCriteriaWithLimitation(policy.NodeSelectionCriterias);

                IEnumerable<string> exceptColList = null;
                var isValid = policy.NodeSelectionCriterias.SelectionCriteriaType == eCriteriaType.WebCriteria ? IsValidWebSelectionCriteria(policy.NodeSelectionCriterias.WebSelectionCriterias, out exceptColList) : true;
                nodeSelectionCriteria.Visible = isValid;
                nodeSelectionErrorControl.Visible = !isValid;
                btnSubmit.Visible = isValid;

                if (exceptColList != null && exceptColList.Count() > 0)
                    exceptColList.ToList().ForEach(item => exceptColListControl.Controls.Add(new HtmlGenericControl() { TagName = @"li", InnerText = item }));

                if (policy.AssignedRulesList.Count > 0)
                {
                    treeAssignedPolicyRules.DataSource = ExecuteSWQL(
                        $@"SELECT R.PolicyRuleID AS ID, R.Name, R.Grouping FROM Cirrus.PolicyRules R WHERE R.PolicyRuleID IN ({string.Join(@",", policy.AssignedRulesList.Select(rule => $@"'{rule}'"))}) ORDER BY R.Grouping, R.Name");
                }

                treeAllPolicyRules.DataSource = ExecuteSWQL(@"SELECT R.PolicyRuleID AS ID, R.Name, R.Grouping FROM Cirrus.PolicyRules R ORDER BY R.Grouping, R.Name");
            }
        }
        catch (Exception ex)
        {
            DisplayException(ex);
        }
    }

    private Policy LoadPolicy(Guid? policyId)
    {
        if (!policyId.HasValue)
        {
            return new Policy();
        }

        using (var proxy = isLayer.Proxy)
        {
            return proxy.Cirrus.PolicyReports.GetPolicy(policyId.Value, false);
        }
    }

    private Guid SavePolicy()
    {
        var policy = LoadPolicy(GetPolicyIdParameterValue());
        policy.PolicyName = txtPolicyName.Text;
        policy.Comments = txtDescription.Text;
        policy.Grouping = txtFolder.FolderName;
        policy.ConfigTypes = ddlConfigType.SelectedValue;
        policy.NodeSelectionCriterias = nodeSelectionCriteria.SelectionCriteria;
        policy.AssignedRulesList = GetAssignedPolicyRules().ToList();

        using (var proxy = isLayer.Proxy)
        {
            if (_addMode)
            {
                return proxy.Cirrus.PolicyReports.AddPolicy(policy, false);
            }

            proxy.Cirrus.PolicyReports.UpdatePolicy(policy);
        }
        return policy.PolicyId;
    }

    private IEnumerable<Guid> GetAssignedPolicyRules()
    {
        var nodes = treeAssignedPolicyRules.GetAllNodes();
        if (nodes != null)
        {
            foreach (var node in nodes)
            {
                yield return node.ID;
            }
        }
    }

    private SelectionCriterias GetSelectionCriteriaWithLimitation(SelectionCriterias criteria)
    {
        var nodeArrayList = criteria.AssignedNodesList;
        if (nodeArrayList != null && nodeArrayList.Count > 0)
        {
            var limitationNodeIDList = SecurityHelper.GetLimitationNodeIDList();
            criteria.AssignedNodesList = nodeArrayList.Intersect(limitationNodeIDList).ToList();
        }
        return criteria;
    }

    private DataTable ExecuteSWQL(string swql)
    {
        using (var proxy = isLayer.Proxy)
        {
            return proxy.Query(swql);
        }
    }

    private void DisplayException(Exception ex)
    {
        MainContent.Visible = false;
        ErrorControl.Visible = true;

        if (!string.IsNullOrEmpty(Page.Request.QueryString[@"PolicyID"]))
        {
            Page.Title = Resources.NCMWebContent.EditPolicy_PageTitle;
        }
        else
        {
            Page.Title = Resources.NCMWebContent.CreateNewPolicy_PageTitle;
        }
        if (ex.GetType() == typeof(System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>))
        {
            btnSubmit.Visible = false;

            ErrorControl.VisiblePermissionLink = false;
            ErrorControl.ErrorMessage = (ex as System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>).Detail.Message;
        }
        else
        {
            btnSubmit.Visible = false;
            btnCancel.Visible = false;

            ErrorControl.ErrorMessage = ex.Message;
        }
    }

    private bool IsValidWebSelectionCriteria(List<WebSelectionCriteria> criteria, out IEnumerable<string> exceptColList)
    {
        var criteriaColList = criteria.Select(item => item.SelectedColumn);
        var allColList = CommonHelper.GetColumnListForDynamicSelection().Select(item => item.Key);

        exceptColList = criteriaColList.Except(allColList);
        return !exceptColList.Any();
    }

    #endregion

    public static List<string> GetAllFolders()
    {
        var isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            var dtFolders = proxy.Query(@"SELECT DISTINCT P.Grouping FROM Cirrus.Policies AS P ORDER BY P.Grouping");

            var folders = new List<string>();
            foreach (DataRow row in dtFolders.Rows)
            {
                folders.Add(row["Grouping"].ToString());
            }

            return folders;
        }
    }
}
