<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="ImportPolicyReport.aspx.cs" Inherits="Orion_NCM_Resources_PolicyReports_ImportPolicyReport" Title="Untitled Page" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="adminPageTitlePlaceHolder">
	<%=Page.Title%>
</asp:Content>

<asp:Content ID ="Content2" runat="server" ContentPlaceHolderID="adminHelpIconPlaceHolder">
    <orion:IconHelpButton ID="helpBtn" runat="server" HelpUrlFragment="OrionNCMWebPolicyReportImport" />
</asp:Content>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="adminContentPlaceholder">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />

    <script type="text/javascript">
        Ext.namespace('SW');
        Ext.namespace('SW.NCM');
        Ext.QuickTips.init();

        var hideDlg = false;
        SW.NCM.ImportPolicyReport = function() {

            return {    
                init: function() {
                    
                    var dlg;
                    $("#display_warning_dialog").click(function() {
                        var dlg = new Ext.Window({
			                title: '<%= Resources.NCMWebContent.WEBDATA_VM_67%>', 
			                html: "<table><tr><td valign='top'><img src=\"/Orion/NCM/Resources/images/warning_32x29.gif\" /></td><td style=\"padding-left:10px;\">" + "<%=string.Format(Resources.NCMWebContent.WEBDATA_VM_68,this.PolicyNames) %>" + "</td></tr></table>",
			                width: 400, 
			                autoHeight: true, 
			                border: false, 
			                region: 'center', 
			                layout: 'fit', 
			                modal: true, 
			                resizable: false, 
			                plain: true, 
			                bodyStyle: 'padding:5px 0px 15px 5px;',
			                buttonAlign : 'center',
			                buttons: [{ 
			                    text:'<%= Resources.NCMWebContent.WEBDATA_VM_66%>', 
			                    handler: function() 
			                    {
			                        hideDlg = true;
			                        dlg.hide();
			                    }
			                    },{
			                    text: '<%= Resources.CoreWebContent.WEBDATA_PCC_28%>', 
			                    handler: function() 
			                    {
			                        dlg.hide();
			                    }
			               }],
			               listeners: { 
		                        hide : function(e) {
		                            if(hideDlg)
		                            {
		                                $("[id$='_Import']").click();
		                            }
		                        }
		                    }				       
		                });
		                dlg.show();            
                    });
                }
            }
        }(); 

        Ext.onReady(SW.NCM.ImportPolicyReport.init, SW.NCM.ImportPolicyReport);
    </script>
    
    <div style="padding:10px;">
        <br />
        <asp:FileUpload ID="uploadPolicyReport" runat="server" Width="50em" size="100" />
        <br />
        <br />
        <asp:CustomValidator ID="validatorPolicyReportFile" runat="server" Font-Size="8pt" ErrorMessage="<%$ Resources: NCMWebContent, ImportFirmwareDefinition_ValidationError_BadFormat%>" />
        <br />
        <br />
        <orion:LocalizableButton runat="server" ID="Submit" LocalizedText="submit" DisplayType="Primary" OnClick="SubmitClick"  />
        <orion:LocalizableButton runat="server" ID="Cancel" LocalizedText="Cancel" DisplayType="secondary" OnClick="CancelClick" CausesValidation="false"  />
        <asp:Button runat="server" ID="Import" OnClick="ImportClick" style="display:none;"/>
    </div>
    
    <div class="x-hidden">
        <input id="display_warning_dialog" type="button" style="display:none;" />
    </div>
</asp:Content>


