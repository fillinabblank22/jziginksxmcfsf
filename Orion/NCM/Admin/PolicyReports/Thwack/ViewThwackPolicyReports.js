﻿// JScript File

Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.QuickTips.init();

SW.NCM.PolicyReports = function () {

    var getGroupByValues = function (groupBy, onSuccess) {
        var param = Ext.util.Format.htmlDecode(groupBy);
        ORION.callWebService("/Orion/NCM/Services/ThwackPolicyReports.asmx",
                             "GetValuesAndCountForProperty", { property: param },
                             function (result) {
                                 onSuccess(ORION.objectFromDataTable(result));
                             });
    };

    var importPolicyReports = function (ids, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/ThwackPolicyReports.asmx",
                             "ImportPolicyReports", { Ids: ids },
                             function (result) {
                                 onSuccess(result);
                             });
    };


    var loadGroupByValues = function () {
        var groupItems = $(".GroupItems").text("@{R=NCM.Strings;K=WEBJS_VY_19;E=js}");

        getGroupByValues("tags", function (result) {
            groupItems.empty();
            $(result.Rows).each(function () {
                var value = Ext.util.Format.htmlEncode(this.theValue);
                var text = $.trim(String(this.theValue));
                if (text.length > 22) { text = String.format("{0}...", text.substr(0, 22)); };
                var disp = (text || "@{R=NCM.Strings;K=LIBCODE_VM_116;E=js}") + " (" + this.theCount + ")";
                if (value === null) { disp = "@{R=NCM.Strings;K=LIBCODE_VM_116;E=js}" + " (" + this.theCount + ")"; }
                $("<a href='#'></a>")
					.attr('value', value).attr('title', value)
					.text(disp).click(selectGroup).appendTo(".GroupItems").wrap("<li class='GroupItem'/>");
            });

            var toSelect = $(".GroupItem a[value='" + Ext.util.Format.htmlEncode(ORION.Prefs.load("GroupByValue")) + "']");
            if (toSelect.length === 0) {
                toSelect = $(".GroupItem a:first");
            }

            toSelect.click();
        });
    };

    var selectGroup = function () {
        $(this).parent().addClass("SelectedGroupItem").siblings().removeClass("SelectedGroupItem");

        var value = $(".SelectedGroupItem a").attr('value');
        value = Ext.util.Format.htmlDecode(value);
        ORION.Prefs.save('GroupByValue', value);

        refreshObjects("tags", value, "");
        return false;
    };

    var currentSearchTerm = null;
    var refreshObjects = function (property, value, search, callback) {
        grid.store.removeAll();
        grid.store.proxy.conn.jsonData = { property: property, value: value || "", search: search };
        currentSearchTerm = search;
        grid.store.load({ callback: callback });
    };

    var getSelectedReportIds = function (items) {
        var ids = [];

        Ext.each(items, function (item) {
            ids.push(item.data.Id);
        });

        return ids;
    };

    var importSelectedPolicyReports = function (items) {
        var selected = getSelectedReportIds(items);
        var waitMsg = Ext.Msg.wait("@{R=NCM.Strings;K=WEBJS_VY_82;E=js}");

        importPolicyReports(selected, function (result) {
            waitMsg.hide();

            var dlg, obj = Ext.decode(result);
            function onClose() { dlg.hide(); dlg.destroy(); dlg = null; }

            if (obj.isSuccess) {
                SW.NCM.validateThwackCred(true);
                function onView() {
                    onClose(); Ext.Msg.wait("@{R=NCM.Strings;K=WEBJS_VY_83;E=js}");
                    ORION.callWebService("/Orion/Services/NodeManagement.asmx",
						"SaveUserSetting", { name: "NCM_PolicyReports_GroupByValue", value: "@{R=NCM.Strings;K=WEBJS_VY_87;E=js}" },
						function (result) { window.location = "/Orion/NCM/Admin/PolicyReports/PolicyReportManagement.aspx"; }
					);
                }

                dlg = new Ext.Window({
                    title: "@{R=NCM.Strings;K=WEBJS_VY_84;E=js}", html: obj.msg,
                    width: 400, height: 90, border: false, region: "center", layout: "fit", modal: true, resizable: false, plain: true, bodyStyle: "padding:5px 0px 0px 10px;",
                    buttons: [
						{ text: "@{R=NCM.Strings;K=WEBJS_VY_85;E=js}", handler: onView }, { text: "@{R=NCM.Strings;K=WEBJS_VY_86;E=js}", handler: onClose }
                    ]
                });
                dlg.show();
            } else {
                SW.NCM.validateThwackCred(false);
                var dlg = new Ext.Window({
                    title: "@{R=NCM.Strings;K=WEBJS_VY_84;E=js}", html: String.format("<span style='color:#f00;'>{0}</span>", obj.msg),
                    width: 350, border: false, region: "center", layout: "fit", modal: true, resizable: false, plain: true, bodyStyle: "padding:5px 0px 0px 10px;",
                    buttons: [{ text: "@{R=NCM.Strings;K=WEBJS_VY_86;E=js}", handler: onClose }]
                });
                dlg.show();
            }
        });
    };

    var updateToolbarButtons = function () {
        var selCount = grid.getSelectionModel().getCount();
        var map = grid.getTopToolbar().items.map;

        var needsOnlyOneSelected = (selCount != 1);
        var needsAtLeastOneSelected = (selCount === 0);

        map.ImportButton.setDisabled(IsDemoMode ? true : needsAtLeastOneSelected);

        // revice 'Select All' checkbox state
        var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');
        if (grid.getStore().getCount() > 0 && selCount == grid.getStore().getCount()) {
            hd.addClass('x-grid3-hd-checker-on');
        } else {
            hd.removeClass('x-grid3-hd-checker-on');
        }
    };

    // Column rendering functions

    function renderName(value, meta, record) {
        return String.format('<img src="/Orion/NCM/Resources/images/PolicyIcons/PolicyReport.Icon.gif" />&nbsp;<a href="#" class="policyReportName" value="{0}">{1}</a>', record.data.Id, Ext.util.Format.htmlEncode(value));
    }

    function renderTags(value, meta, record) {
        var tags;

        try {
            tags = Ext.decode(value);
        } catch (e) { }

        tags = tags || [];

        if (tags.length === 0) {
            return "@{R=NCM.Strings;K=WEBJS_VY_88;E=js}";
        }

        var links = [];
        for (var i = 0; i < tags.length; ++i) {
            links.push(String.format('<a href="#" class="tagLink">{0}</a>', Ext.util.Format.htmlEncode(tags[i])));
        }

        return links.join(', ');
    }


    function renderPublishDate(value, meta, record) {
        return value;
    }

    function renderRating(value, meta, record) {
        if (value == 0)
            return "@{R=NCM.Strings;K=WEBJS_VY_89;E=js}";

        // 1. start with 3.8333. 
        // 2. multiple by 10 and truncate (38) and then double (76)
        // 3. drop the ones digit (/ 10 and then * 10) => 70
        // 4. divide by 2 =  35
        var rating = parseInt(value * 10) * 2;
        rating = (parseInt(rating / 10) * 10) / 2;
        rating = "000" + rating;
        rating = rating.substr(rating.length - 2);

        return String.format('<img src="/Orion/NCM/Resources/images/ConfigSnippets/stars/stars_{0}.gif" />', rating);
    }

    function jumpToTag(tagName) {
        var tn = Ext.util.Format.htmlEncode(tagName);
        $(".GroupItem a[value='" + tn + "']").click();
    }

    var showDescriptionDialog;

    function showDescription(obj) {

        var reportId = obj.attr("value");
        var body = $("#snippetDescriptionBody").text("@{R=NCM.Strings;K=WEBJS_VY_19;E=js}");

        ORION.callWebService("/Orion/NCM/Services/ThwackPolicyReports.asmx",
                             "GetPolicyReportDescription", { reportId: reportId },
                             function (result) {
                                 var desc = $.trim(result.description);
                                 desc = desc.length != 0 ? desc : "@{R=NCM.Strings;K=WEBJS_VY_72;E=js}";
                                 desc = Ext.util.Format.htmlEncode(desc);
                                 body.empty().html('<div style="font-weight: bold;">@{R=NCM.Strings;K=WEBJS_VY_90;E=js}</div>' + desc);
                             });

        return SW.NCM.ShowDescription(obj);

    }


    gridColumnChangedHandler = function (component, state) {
        if (component == null)
            return;

        var gridCM = component.getColumnModel();
        var settingName = 'Columns';

        if (gridCM != null && settingName != '') {

            var jsonvalue = SW.NCM.GridColumnsToJson(gridCM);
            ORION.Prefs.save(settingName, jsonvalue);
        }
    }

    function gridResize() {
        if (grid != null) {
            grid.setWidth(1);
            grid.setWidth($('#gridCell').width());

            var h = getGridHeight()
            grid.setHeight(getGridHeight());

            var groupItems = $(".GroupItems");
            groupItems.height(h - $(".ncm_GroupSection").height() - 14);
        }
    }

    function getGridHeight() {
        var top = $('#gridCell').offset().top;
        return $(window).height() - top - $('#footer').height() - 100;
    }

    ORION.prefix = "NCM_PolicyReports_Thwack_";

    var selectorModel;
    var dataStore;
    var grid;


    return {
        init: function () {

            if ($("[id$='_ContentContainer']").length == 0)
                return false;

            function onImportClick() {
                if (IsDemoMode) return SW.NCM.ShowDemoModeExtMsg();
                SW.NCM.logInToThwack(importSelectedPolicyReports, grid.getSelectionModel().getSelections(), true);
            }

            $("#Grid").click(function (e) {

                var obj = $(e.target);
                if (obj.hasClass("ncm_searchterm"))
                    obj = obj.parent();

                if (obj.hasClass('tagLink')) {
                    jumpToTag(obj.text());
                    return false;
                }
                if (obj.hasClass('policyReportName')) {
                    showDescription(obj);
                    return false;
                }
            });
            //$("#btnImport").click(onImportClick);

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", updateToolbarButtons);

            dataStore = new ORION.WebServiceStore(
                            "/Orion/NCM/Services/ThwackPolicyReports.asmx/GetPolicyReportsPaged",
                            [
                                { name: 'Id', mapping: 0 },
                                { name: 'Title', mapping: 1 },
                                { name: 'Link', mapping: 2 },
                                { name: 'PubDate', mapping: 3 },
                                { name: 'DownloadCount', mapping: 4 },
                                { name: 'ViewCount', mapping: 5 },
                                { name: 'Rating', mapping: 6 },
                                { name: 'Owner', mapping: 7 },
                                { name: 'Tags', mapping: 8 }
                            ],
                            "Title");

            var pagingToolbar = new Ext.PagingToolbar({
                store: dataStore,
                pageSize: 25,
                displayInfo: true,
                displayMsg: '@{R=NCM.Strings;K=WEBJS_VY_46;E=js}',
                emptyMsg: "@{R=NCM.Strings;K=WEBJS_VY_45;E=js}"
            });

            grid = new Ext.grid.GridPanel({
                id: 'gridControl',
                store: dataStore,

                columns: [selectorModel, {
                    header: '@{R=NCM.Strings;K=WEBJS_VY_13;E=js}',
                    width: 80,
                    hidden: true,
                    sortable: true,
                    hideable: false,
                    dataIndex: 'Id'
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VY_91;E=js}',
                    width: 300,
                    sortable: true,
                    hideable: false,
                    dataIndex: 'Title',
                    renderer: renderName
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VY_92;E=js}',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    dataIndex: 'Owner'
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VY_93;E=js}',
                    width: 90,
                    sortable: true,
                    dataIndex: 'PubDate',
                    renderer: renderPublishDate
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VY_94;E=js}',
                    width: 90,
                    sortable: true,
                    dataIndex: 'Rating',
                    renderer: renderRating
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VY_95;E=js}',
                    width: 90,
                    sortable: true,
                    dataIndex: 'DownloadCount'
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VY_96;E=js}',
                    width: 100,
                    sortable: true,
                    hidden: true,
                    dataIndex: 'ViewCount'
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VY_97;E=js}',
                    width: 220,
                    sortable: false,
                    dataIndex: 'Tags',
                    renderer: renderTags
                }],

                sm: selectorModel,

                viewConfig: {
                    forceFit: false,
                    deferEmptyText: true,
                    emptyText: " "
                },

                //width: 600,
                height: 500,
                loadMask: true,
                layout: 'fit',
                autoScroll: 'true',
                stripeRows: true,

                tbar: [{
                    id: 'ImportButton',
                    text: '@{R=NCM.Strings;K=WEBJS_VY_28;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VY_98;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_import.gif',
                    cls: 'x-btn-text-icon',
                    handler: onImportClick
                }],

                bbar: pagingToolbar

            });
            SW.NCM.PersonalizeGrid(grid, columnModel);
            grid.on("statesave", gridColumnChangedHandler);

            grid.render('Grid');
            pagingToolbar.refresh.hideParent = true;
            pagingToolbar.refresh.hide();

            updateToolbarButtons();

            // Set the height of the Group Items list
            var fudgeFactor = 12;
            var a = $("#Grid").height();
            var b = $(".ncm_GroupSection").height();
            var groupItemsHeight = $("#Grid").height() - $(".ncm_GroupSection").height() - fudgeFactor;
            $(".GroupItems").height(groupItemsHeight);

            // Set the width and height of the grid
            gridResize();
            $(window).resize(function () {
                gridResize();
            });

            loadGroupByValues();

            $("form").submit(function () { return false; });

            $("#search").keyup(function (e) {
                if (e.keyCode == 13) {
                    $("#doSearch").click();
                }
            });

            $("#doSearch").click(function () {
                var search = $("#search").val();
                if ($.trim(search).length == 0) return;

                $(".GroupItem").removeClass("SelectedGroupItem");

                refreshObjects("", "", search);
            });

            // highlight the search term (if we have one)            
            grid.getView().on("refresh", function () {
                var search = currentSearchTerm;
                if ($.trim(search).length == 0) return;

                var columnsToHighlight = SW.NCM.ColumnIndexByName(grid.getColumnModel(), ['@{R=NCM.Strings;K=WEBJS_VY_91;E=js}', '@{R=NCM.Strings;K=WEBJS_VY_92;E=js}', '@{R=NCM.Strings;K=WEBJS_VY_97;E=js}']);
                Ext.each(columnsToHighlight, function (columnNumber) {
                    SW.NCM.highlightSearchText("#Grid .x-grid3-body .x-grid3-td-" + columnNumber, search);
                });
            });

            // Set the grid's empty text based on the data returned from the server.
            dataStore.on("load", function (store, records, options) {
                var t = dataStore.reader.jsonData.d.EmptyText;
                $("#Grid div.x-grid-empty").text(t);
            });
        }
    };

}();

Ext.onReady(SW.NCM.PolicyReports.init, SW.NCM.PolicyReports);