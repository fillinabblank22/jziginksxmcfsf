<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="PolicyReportManagement.aspx.cs" Inherits="Orion_NCM_Resources_PolicyReports_PolicyReportManagement" Title="<%$ Resources: NCMWebContent, WEBDATA_VY_02%>" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register Src="~/Orion/NCM/CustomPageLinkHidding.ascx" TagPrefix="ncm" TagName="custPageLinkHidding" %>
<%@ Register TagPrefix="ncm1" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register TagPrefix="ncm" Src="~/Orion/NCM/NavigationTabBar.ascx"  TagName="NavigationTabBar" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/NCM/Controls/ErrorControl.ascx" TagPrefix="ncm" TagName="ErrorControl" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers"%>

<asp:Content ID="PageTitleContent" ContentPlaceHolderID="adminHeadPlaceHolder" Runat="Server">
    
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <ncm:custPageLinkHidding ID="CustPageLinkHidding1"  runat="server" />
    <ncm1:ConfigSnippetUISettings ID="ReportUISetting" runat="server" Name="NCM_PolicyReportMgmt_Columns" DefaultValue="[]" RenderTo="columnModel" />
    <ncm1:ExtLocalDateTimePattern ID="ExtLocalDateTimePattern1" runat="server"/>
    
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/PolicyReports/PolicyReport.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
    
    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Admin/PolicyReports/PolicyReportManagement.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippetsHelper.js"></script>
    
    <script type="text/javascript">
        var thwackUserInfo = <%=_thwackUserInfo%>;
        var IsDemoMode = <%=SolarWinds.NCMModule.Web.Resources.CommonHelper.IsDemoMode().ToString().ToLowerInvariant() %>;
        var disabledReportValue = <%= disabledReportValue%>;
        var enabledReportValue = <%= enabledReportValue%>;
    </script>
    
    <style type="text/css">
        .mngPolicyReportLink
        {
            background-image:url("/Orion/images/Page.Icon.CustomPg.gif");
            background-position:left center;
            background-repeat:no-repeat;
            padding:2px 0 2px 20px;
            text-align:right;
        }
        
        .ncmDataArea
        {
            border-style:none;
            float:right;
            font-family:Arial,Helvetica,sans-serif;
            font-size:8pt;
            margin-right:5px;
            margin-top:0;
            padding-bottom:0px;
            padding-top:7px;
            text-align:right;
        }

        #selectAll a { color: Red; text-decoration: underline; }
        .select-all-page { background-color: #ffffcc; }
    </style>
    
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="adminPageTitlePlaceHolder">
	<%=Page.Title%>
</asp:Content>

<asp:Content ID ="Content2" runat="server" ContentPlaceHolderID="adminHelpIconPlaceHolder">
    <orion:IconHelpButton ID="helpBtn" runat="server" HelpUrlFragment="OrionNCMWebManagePolicyReports" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    
    <%foreach (string setting in new string[] { @"GroupByValue"}) { %>
		<input type="hidden" name="NCM_PolicyReportMgmt_<%=setting%>" id="NCM_PolicyReportMgmt_<%=setting%>" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get($@"NCM_PolicyReportMgmt_{setting}")%>' />
	<%}%>
    
    <asp:Panel id="ContentContainer" runat="server">
        <div style="padding:10px;">
             <div id="LinkHolder" runat="server" style="padding:0px 0px 10px 0px;">
                <table width="100%" style="background-color:#FFFDCC;">
                    <tr>
                        <td style="padding:5px 0px 5px 0px;">
                            <img style="padding:0px 5px 0px 5px;" src="/Orion/NCM/Resources/images/lightbulb_tip_16x16.gif"/><b><%=Resources.NCMWebContent.WEBDATA_VY_04%></b><%=Resources.NCMWebContent.WEBDATA_VY_05%>&nbsp;&#0187;&nbsp;<a style="color:#336699;" target="_blank" href="<%=LearnMoreManagePolicyReports %>" ><%=Resources.NCMWebContent.WEBDATA_VY_06%></a>
                        </td>
                    </tr>
                </table>
            </div>
            <ncm:NavigationTabBar runat="server"/>
            <div id="tabPanel" class="tab-top">
                <table class="ExistingElements" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="2" align="right">
                            <div id="refreshContainer"style="margin-top:6px;font-size:8pt;display:none;">
                                <%=Resources.NCMWebContent.WEBDATA_VY_07%><span id='refreshtime' style="font-size:8pt;">30s</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div id="selectAll" style="padding:3px"></div>
                        </td>
                    </tr>
                    <tr valign="top" align="left">
                        <td style="width: 220px;">
                            <div class="ElementGrouping">
                                <div class="ncm_GroupSection x-panel-header x-toolbar x-small-editor">
                                    <div><%=Resources.NCMWebContent.WEBDATA_VY_01%></div>
                                </div>
                                <div>
                                    <ul class="GroupItems" style="width:220px;"></ul>
                                 </div>                                
                            </div>
                        </td>
                        <td id="gridCell">
                            <div id="Grid" />
                        </td>
                    </tr>
                </table>
                <div id="originalQuery">
                </div>
                <div id="test">
                </div>
                <pre id="stackTrace"></pre>
            </div>
        </div>
    </asp:Panel>
    <div style="padding:10px;" id="ErrorContainer" runat="server"/>
    <ncm:ErrorControl ID="ErrorControl" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VY_03%>" runat="server" Visible="false" />
</asp:Content>

