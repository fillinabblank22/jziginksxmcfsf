using System;
using System.Web.UI;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Orion.Web;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_Resources_PolicyReports_PolicyRuleManagement : Page
{
    private readonly ICommonHelper commonHelper;
    private readonly IIsWrapper isLayer;

    public Orion_NCM_Resources_PolicyReports_PolicyRuleManagement()
    {
        commonHelper = ServiceLocator.Container.Resolve<ICommonHelper>();
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
    }

    protected override void OnInit(EventArgs e)
    {
        var validator = new SWISValidator
        {
            ContentContainer = ContentContainer,
            RedirectIfValidationFailed = true
        };

        ErrorContainer.Controls.Add(validator);

        if (validator.IsValid)
        {
            commonHelper.Initialise();
            IsPermissionExist();
        }

        base.OnInit(e);
    }

    protected static readonly string HelpLink = string.Empty;

    private void IsPermissionExist()
    {
        using (var proxy = isLayer.GetProxy())
        {
            var complianceMgmtOnlyForAdmins = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(SolarWinds.NCM.Contracts.InformationService.Settings.ComplianceMgmtOnlyForAdmins, false, null));
            if (complianceMgmtOnlyForAdmins)
            {
                if (CommonHelper.IsDemoMode()
                     || (!Profile.AllowAdmin && !(this.Page is IBypassAccessLimitation)))
                {
                    ErrorControl.Visible = true;
                    ErrorControl.VisiblePermissionLink = false;
                    ErrorControl.ErrorMessage = Resources.NCMWebContent.WEBDATA_VK_898;
                    ContentContainer.Visible = false;
                }
            }
            else
            {
                if (!SecurityHelper.IsPermissionExist(SecurityHelper._canDownload))
                {
                    ErrorControl.Visible = true;
                    ErrorControl.VisiblePermissionLink = true;
                    ErrorControl.ErrorMessage = Resources.NCMWebContent.WEBDATA_VK_899;
                    ContentContainer.Visible = false;
                }
            }
        }
    }
}
