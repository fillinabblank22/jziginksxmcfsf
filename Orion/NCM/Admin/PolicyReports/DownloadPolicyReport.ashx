<%@ WebHandler Language="C#" Class="DownloadPolicyReport" %>

using System;
using System.Web;
using System.Text.RegularExpressions;

using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.PolicyReportsManagement;
using SolarWinds.NCM.Contracts.InformationService;

public class DownloadPolicyReport : IHttpHandler 
{
    public void ProcessRequest(HttpContext context) 
    {
        var isLayer = new ISWrapper();

        var exportId = Guid.Parse(context.Request[@"PolicyReportID"]);

        try
        {
            PolicyReport policyReport = null;
            using (var proxy = isLayer.Proxy)
            {
                policyReport = proxy.Cirrus.PolicyReports.GetPolicyReport(exportId, true);
                foreach (var policy in policyReport.AssignedPolicies)
                {
                    if (policy.NodeSelectionString.StartsWith(@"Criteria:", StringComparison.InvariantCultureIgnoreCase))
                    {
                        policy.NodeSelectionString = Regex.Replace(policy.NodeSelectionString, @"\bNCM_Nodes\b", "Nodes", RegexOptions.IgnoreCase);
                    }
                }
            }

            var toExportStr = ImportExportPolicyReport.ExportPolicyReport(policyReport);
            var fileName = GetExportFileName(context, policyReport);
            context.Response.Clear();
            context.Response.ContentType = @"application/xml";
            context.Response.AddHeader(@"Content-Disposition", @"attachment; filename=" + fileName);
            context.Response.Write(toExportStr);
        }
        catch (Exception ex)
        {
            var exStr = string.Empty;
            var divStr = @"<div style=""color:Red;font-size:12px;"">{0}</div>";
            if (ex.GetType() == typeof(System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>))
            {
                exStr = string.Format(divStr, (ex as System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>).Detail.Message);
            }
            else
            {
                exStr = string.Format(divStr, ex.Message);
            }
            context.Response.Clear();
            context.Response.Write(exStr);
        }
        context.ApplicationInstance.CompleteRequest();
    }

    public bool IsReusable
    {
        get { return false; }
    }

    private static string GetExportFileName(HttpContext context, PolicyReport policyReport)
    {
        var encodedName = CommonHelper.EncodeFilenameIfNeeded(context, policyReport.Name);
        return $@"""{encodedName}.xml""";
    }

    
}