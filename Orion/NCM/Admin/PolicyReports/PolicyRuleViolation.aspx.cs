﻿using System;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Web.UI;
using StringRegistrar = SolarWinds.Orion.Core.Common.i18n.Registrar.ResourceManagerRegistrar;

public partial class Orion_NCM_Admin_PolicyReports_PolicyRuleViolation : System.Web.UI.Page
{
    private ISWrapper isLayer;

    protected string Level_1
    {
        get { return txtLevel1.Text; }
        set { txtLevel1.Text = value; }
    }

    protected string Level_2
    {
        get { return txtLevel2.Text; }
        set { txtLevel2.Text = value; }
    }

    protected string Level_3
    {
        get { return txtLevel3.Text; }
        set { txtLevel3.Text = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.Compliance_ManageViolationLevels;
        isLayer= new ISWrapper();
       
        if (!Page.IsPostBack)
        {
            InitInternal();
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        using (var proxy = isLayer.Proxy)
        {
            proxy.Cirrus.Settings.SaveSetting(Settings.PolicyViolationLevel1, Level_1, null);
            proxy.Cirrus.Settings.SaveSetting(Settings.PolicyViolationLevel2, Level_2, null);
            proxy.Cirrus.Settings.SaveSetting(Settings.PolicyViolationLevel3, Level_3, null);
        }

        var btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        var btn = sender as LocalizableButton;
        Page.Response.Redirect(btn.CommandArgument);
    }

    private void InitInternal()
    {
        using (var proxy = isLayer.Proxy)
        {
            Level_1 = proxy.Cirrus.Settings.GetSetting(Settings.PolicyViolationLevel1, StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"PolicyViolationsLevel_Informational"), null);
            Level_2 = proxy.Cirrus.Settings.GetSetting(Settings.PolicyViolationLevel2, StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"PolicyViolationsLevel_Warning"), null);
            Level_3 = proxy.Cirrus.Settings.GetSetting(Settings.PolicyViolationLevel3, StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"PolicyViolationsLevel_Critical"), null);
        }
    }
}