﻿Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.QuickTips.init();


SW.NCM.PolicyReport = function () {

    SW.NCM.AllSelected = false;

    function gridResize() {
        if (grid != null) {
            grid.setWidth(1);
            grid.setWidth($('#gridCell').width());

            var h = getGridHeight()
            grid.setHeight(getGridHeight());

            var groupItems = $(".GroupItems");
            groupItems.height(h - $(".ncm_GroupSection").height() - 14);
        }
    }

    function getGridHeight() {
        var top = $('#gridCell').offset().top;
        return $(window).height() - top - $('#footer').height() - 75;
    }

    var getGroupByValues = function (groupBy, onSuccess) {

        ORION.callWebService("/Orion/NCM/Services/PolicyReportManagement.asmx",
                             "GetValuesAndCountForProperty", { property: groupBy },
                             function (result) {
                                 onSuccess(ORION.objectFromDataTable(result));
                             });
    };

    var getGroupDisplayText = function (text) {
        switch (text) {
            case 'No Folder':
                return '@{R=NCM.Strings;K=WEBJS_VY_78;E=js}';
            default:
                return text;
        }
    };

    var loadGroupByValues = function () {
        var groupItems = $(".GroupItems").text("@{R=NCM.Strings;K=WEBJS_VY_19;E=js}");

        getGroupByValues("groups", function (result) {
            groupItems.empty();
            $(result.Rows).each(function () {
                var value = Ext.util.Format.htmlEncode(this.theValue);
                var text = $.trim(String(this.theValue));
                if (text.length > 22) { text = String.format("{0}...", text.substr(0, 22)); };
                var disp = (getGroupDisplayText(text) || "@{R=NCM.Strings;K=LIBCODE_VM_116;E=js}") + " (" + this.theCount + ")";
                if (value === null) { disp = "@{R=NCM.Strings;K=LIBCODE_VM_116;E=js}" + " (" + this.theCount + ")"; }
                $("<a href='#'></a>")
                    .attr('value', value).attr('title', getGroupDisplayText(value))
					.text(disp).click(selectGroup).appendTo(".GroupItems").wrap("<li class='group-policy-report-item'/>");
            });

            var toSelect = $(".group-policy-report-item a[value='" + Ext.util.Format.htmlEncode(ORION.Prefs.load("GroupByValue")) + "']");
            if (toSelect.length === 0) {
                toSelect = $(".group-policy-report-item a:first");
            }

            toSelect.click();
        });
    };

    var selectGroup = function () {
        $(this).parent().addClass("SelectedGroupItem").siblings().removeClass("SelectedGroupItem");

        var value = $(".SelectedGroupItem a").attr('value');
        value = Ext.util.Format.htmlDecode(value);
        ORION.Prefs.save('GroupByValue', value);

        refreshObjects("groups", value, "");
        return false;
    };

    var currentSearchTerm = null;

    var refreshObjects = function (property, value, search, callback) {
        grid.store.removeAll();
        grid.store.baseParams['limit'] = grid.getBottomToolbar().pageSize;
        grid.store.proxy.conn.jsonData = { property: property, value: value || "", search: search };
        currentSearchTerm = search;
        grid.store.load({ callback: callback });
    };

    var managePolicyReport = function () {

        var url = "/Orion/NCM/Admin/PolicyReports/PolicyReportManagement.aspx";
        window.location = url;
    };

    var viewPolicyReport = function (item) {
        if (item.data.ReportStatus == disabledReportValue) {
            Ext.Msg.show({
                title: '@{R=NCM.Strings;K=WEBJS_VY_49;E=js}',
                msg: '@{R=NCM.Strings;K=WEBJS_IA_61;E=js}',
                minWidth: 500,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.INFO
            });
        }
        else {
            if (item.data.CacheStatus == 3) {
                var url = "/Orion/NCM/ComplianceReportResult.aspx?ReportID=" + item.data.ReportID;
                window.location = url;
            } else {
                var msgText = '@{R=NCM.Strings;K=WEBJS_VY_50;E=js}';
                if (item.data.CacheStatus == 0)
                    msgText = '@{R=NCM.Strings;K=WEBJS_VY_55;E=js}';

                if (item.data.CacheStatus == 4)
                    msgText = '@{R=NCM.Strings;K=WEBJS_VY_54;E=js}';

                Ext.Msg.show({
                    title: '@{R=NCM.Strings;K=WEBJS_VY_49;E=js}',
                    msg: msgText,
                    minWidth: 500,
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO
                });
            }
        }
    };

    var updateToolbarButtons = function () {
        var selCount = grid.getSelectionModel().getCount();
        var map = grid.getTopToolbar().items.map;

        var needsOnlyOneSelected = (selCount != 1);
        var needsAtLeastOneSelected = (selCount === 0);

        map.ViewReportBtn.setDisabled(needsOnlyOneSelected);
        map.UpdateAllBtn.setDisabled(IsDemoMode);

        if (selCount > 0) {
            map.UpdateAllBtn.setText('@{R=NCM.Strings;K=WEBJS_IA_27;E=js}');
            map.UpdateAllBtn.setTooltip('@{R=NCM.Strings;K=WEBJS_IA_28;E=js}');
        }
        else {
            map.UpdateAllBtn.setText('@{R=NCM.Strings;K=WEBJS_VY_26;E=js}');
            map.UpdateAllBtn.setTooltip('@{R=NCM.Strings;K=WEBJS_VY_27;E=js}');
        }

        // revice 'Select All' checkbox state
        var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');
        if (grid.getStore().getCount() > 0 && selCount == grid.getStore().getCount()) {
            hd.addClass('x-grid3-hd-checker-on');
        } else {
            hd.removeClass('x-grid3-hd-checker-on');
        }

        updateSelectAll();
    };

    var updateSelectAll = function () {
        $("#selectAll").empty();
        $("#selectAll").removeClass("select-all-page");

        var selCount = grid.getSelectionModel().getCount();
        var pageSize = grid.getBottomToolbar().pageSize;
        var totalCount = grid.getStore().getTotalCount();
        var pageCount = grid.getStore().getCount();

        if ((pageCount > 0) && (totalCount > pageSize) && (selCount == pageCount)) {
            var countOfNodesOnPage = pageSize;
            if ((pageCount % pageSize) != 0) {
                countOfNodesOnPage = pageCount % pageSize;
            }

            $("#selectAll").append(String.format("<span>@{R=NCM.Strings;K=CompliancePolicyReports_AllPolicyReportsOnPageSelected;E=js}</span> <a href='#'>@{R=NCM.Strings;K=CompliancePolicyReports_SelectAllPolicyReports;E=js}</a>", countOfNodesOnPage, totalCount)).addClass("select-all-page");
            $("#selectAll a").click(function () {
                $("#selectAll").empty();
                $("#selectAll").removeClass("select-all-page");
                $("#selectAll").append(String.format("<span>@{R=NCM.Strings;K=CompliancePolicyReports_AllPolicyReportsSelected;E=js}</span>"));

                SW.NCM.AllSelected = true;
            });
        }
        else {
            SW.NCM.AllSelected = false;
        }
    };

    var doClean = function () {
        var map = grid.getTopToolbar().items.map;
        map.Clean.setVisible(false);
        map.txtSearch.setValue('');
        loadGroupByValues();
    };

    var doSearch = function () {
        var map = grid.getTopToolbar().items.map;
        var search = map.txtSearch.getValue();
        if ($.trim(search).length == 0) return;
        $(".group-policy-report-item").removeClass("SelectedGroupItem");
        refreshObjects("", "", search);
        map.Clean.setVisible(true);
    }

    // Column rendering functions
    function renderName(value, meta, record) {
        return String.format('<img src="/Orion/NCM/Resources/images/PolicyIcons/PolicyReport.Icon.gif" />&nbsp;<a href="#" class="policyReportName" value="{0}">{1}</a>', record.data.ReportID, Ext.util.Format.htmlEncode(value));
    }

    function renderReportStatus(value, meta, record) {
        if (value == 1)
            return '@{R=NCM.Strings;K=WEBJS_VK_128;E=js}';
        return '@{R=NCM.Strings;K=WEBJS_IA_32;E=js}';
    }

    function renderDateTime(value, meta, record) {
        if (value == null)
            return '';
        var date = eval(value.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
        return Ext.util.Format.date(date, ExtDaTimeLocalePattern.DateFull);
    }

    function renderLastUpdate(value, meta, record) {
        var status = record.data.CacheStatus;
        if (record.data.ReportStatus == disabledReportValue)
            return '@{R=NCM.Strings;K=WEBJS_VY_75;E=js}';
        var msg;
        switch (status) {
            case 0:
                return '@{R=NCM.Strings;K=WEBJS_VY_75;E=js}';
            case 1:
                return '<img src="/Orion/NCM/Resources/images/spinner.gif"/>&nbsp;@{R=NCM.Strings;K=WEBJS_VY_81;E=js}';
            case 2:
                return '<img src="/Orion/NCM/Resources/images/spinner.gif"/>&nbsp;@{R=NCM.Strings;K=WEBJS_VY_80;E=js}';
            case 3:
                return renderDateTime(value, meta, record);
            case 4:
                return '<font color="#FF0000">@{R=NCM.Strings;K=WEBJS_VM_74;E=js}</font>';
            case 5:
                return '<img src="/Orion/NCM/Resources/images/spinner.gif"/>&nbsp;@{R=NCM.Strings;K=WEBJS_VY_79;E=js}';
            default:
                return '@{R=NCM.Strings;K=WEBJS_VY_76;E=js}';
        }
    }

    function renderGroups(value, meta, record) {
        var grouping = value;
        grouping = grouping || '';

        if (grouping.length === 0) {
            return "@{R=NCM.Strings;K=WEBJS_VY_78;E=js}";
        }

        res = String.format('<a href="#" class="groupLink">{0}</a>', Ext.util.Format.htmlEncode(grouping));

        return res;
    }

    function renderComments(value, meta, record) {
        return Ext.util.Format.htmlEncode(value);
    }

    function jumpToGroup(groupName) {
        var gn = Ext.util.Format.htmlEncode(groupName);
        $(".group-policy-report-item a[value='" + gn + "']").click();
    }

    gridColumnChangedHandler = function (component, state) {
        if (component == null)
            return;

        var gridCM = component.getColumnModel();
        var settingName = 'Columns';

        if (gridCM != null && settingName != '') {

            var jsonvalue = SW.NCM.GridColumnsToJson(gridCM);
            ORION.Prefs.save(settingName, jsonvalue);
        }
    }

    function reportNameClick(obj) {
        var reportId = obj.attr("value");
        var selection = grid.getSelectionModel().getSelections();
        var index = Ext.each(selection, function (item) {
            if (item.data.ReportID == reportId)
                return false;
        });
        if (typeof (index) !== 'undefined' && index != null) {
            viewPolicyReport(selection[index]);
        }
        else {
            ErrorHandler({ "Source": "@{R=NCM.Strings;K=WEBJS_VK_68;E=js}", "Error": true, "Msg": "@{R=NCM.Strings;K=CompliancePolicyReports_UnableToFindReportDialogMessage;E=js}" });
        }
    }

    var firstLoad = true;
    var lastLoad = new Date();
    var skipCachingStatusCheck = false;
    var loadRate = 15000;
    var isCacheRunning = false;
    var grid_loadlock = false;
    function CheckCachingStatus(o) {
        lastLoad = new Date();
        grid_loadlock = false;
        skipCachingStatusCheck = false;
        isCacheRunning = false;

        if (firstLoad) {
            firstLoad = false;
            Ext.TaskMgr.start({
                run: function () {
                    var txt;

                    if (skipCachingStatusCheck == false) {
                        ORION.callWebService("/Orion/NCM/Services/PolicyReportManagement.asmx",
                                 "GetCacheUpdatingStatus", {},
                                 function (result) {
                                     var obj = Ext.util.JSON.decode(result);
                                     if (obj.success) {
                                         if (obj.running) {
                                             $('#refreshContainer').show();
                                             $('#refreshtime').show();
                                             isCacheRunning = true;
                                         }
                                         else {
                                             $('#refreshContainer').hide();
                                             $('#refreshtime').hide();
                                             isCacheRunning = false;
                                         }

                                     }
                                 });
                        skipCachingStatusCheck = true;
                    }

                    if (grid_loadlock) {
                        txt = 'now';
                    } else {
                        if (isCacheRunning) {
                            var now = new Date();
                            var diff = (now - lastLoad.getTime());
                            if (diff >= loadRate) {
                                grid_loadlock = true;
                                var tb = grid.getBottomToolbar();
                                tb.doLoad(tb.cursor);
                                txt = 'now';
                            } else {
                                txt = Math.round((loadRate - diff) / 1000);
                            }
                            $('#refreshtime').text(txt);
                        }
                    }
                },
                interval: 1000
            });
        }
    }

    //Error handler
    function ErrorHandler(result) {
        if (result != null && result.Error) {
            Ext.Msg.show({
                title: result.Source,
                msg: result.Msg,
                minWidth: 500,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        }
        if (result != null && result.Error == false) {
            Ext.Msg.show({
                title: result.Source,
                msg: result.Msg,
                minWidth: 500,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.INFO
            });
        }
    }

    var getSelectedPolicyReportIds = function (items) {
        var ids = [];

        Ext.each(items, function (item) {
            ids.push(item.data.ReportID);
        });

        return ids;
    };

    function getGridParams() {
        var gridParams = {};

        var selectedItem = $(".SelectedGroupItem a");
        var map = grid.getTopToolbar().items.map;

        gridParams.groupByValue = Ext.util.Format.htmlDecode(selectedItem.length == 0 ? '' : selectedItem.attr('value'));
        gridParams.searchTerm = map.txtSearch.getValue();

        return gridParams;
    }

    function VerifySelectedReportsStatus(selectedItems) {
        var enabledReportsCount = 0;
        var disabledReportsCount = 0;
        selectedItems = selectedItems.length > 0 ? selectedItems : grid.store.data.items;

        Ext.each(selectedItems, function (item) {
            if (!item.data.ReportStatus) {
                disabledReportsCount++;
            }
            else {
                enabledReportsCount++;
            }
        });

        if (enabledReportsCount === 0 && disabledReportsCount > 0)
            return -1;
        if (disabledReportsCount === 0)
            return 1;
        else {
            return 0;
        }
    }

    function ShowNotification(reportStatus, fn) {
        var title = '@{R=NCM.Strings;K=WEBJS_IA_29;E=js}';
        var msg = '@{R=NCM.Strings;K=WEBJS_IA_31;E=js}';
        var icon = Ext.MessageBox.INFO;

        if (reportStatus < 0) {
            fn = null;
            icon = Ext.MessageBox.ERROR;
            msg = '@{R=NCM.Strings;K=WEBJS_IA_30;E=js}';
        }
        else if (reportStatus > 0) {
            fn();
            return;
        }

        Ext.Msg.show({
            title: title,
            msg: msg,
            minWidth: 300,
            buttons: Ext.Msg.OK,
            icon: icon,
            fn: fn
        });
    }

    //toolbar button handlers
    function UpdateAll() {
        var selectedReportsIDs = getSelectedPolicyReportIds(grid.getSelectionModel().getSelections());
        var reportsStatus = VerifySelectedReportsStatus(grid.getSelectionModel().getSelections());
        var fn = RunUpdateAll;

        if (selectedReportsIDs.length !== 0) {
            fn = function() {
                RunUpdateSelected(selectedReportsIDs);
            };
        }

        ShowNotification(reportsStatus, fn);
    }

    function RunUpdateAll() {
        ORION.callWebService("/Orion/NCM/Services/PolicyReportManagement.asmx",
                                 "RunUpdateAll", {},
                                 function (result) {
                                     //refresh gridload
                                     skipCachingStatusCheck = false;
                                     ErrorHandler(result);
                                     var tb = grid.getBottomToolbar();
                                     tb.doLoad(tb.cursor);
                                 });
    }

    function RunUpdateSelected(selectedReportsIDs) {
        ORION.callWebService("/Orion/NCM/Services/PolicyReportManagement.asmx",
                                 "RunUpdateSelected", { selectedReportsIDs: SW.NCM.AllSelected ? null : selectedReportsIDs, gridParams: SW.NCM.AllSelected ? getGridParams() : null },
                                 function (result) {
                                     //refresh gridload
                                     skipCachingStatusCheck = false;
                                     ErrorHandler(result);
                                     var tb = grid.getBottomToolbar();
                                     tb.doLoad(tb.cursor);
                                 });
    }

    ORION.prefix = "NCM_PolicyReportView_";

    var selectorModel;
    var dataStore;
    var grid;

    return {
        init: function () {

            if ($("[id$='_ContentContainer']").length == 0)
                return false;

            $("#Grid").click(function (e) {

                var obj = $(e.target);

                if (obj.hasClass("ncm_searchterm"))
                    obj = obj.parent();

                if (obj.hasClass('groupLink')) {
                    jumpToGroup(obj.text());
                    return false;
                }

                if (obj.hasClass('policyReportName')) {
                    reportNameClick(obj);
                    return false;
                }
            });

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", updateToolbarButtons);

            dataStore = new ORION.WebServiceStore(
                                "/Orion/NCM/Services/PolicyReportManagement.asmx/GetPolicyReportsPaged",
                                [
                                    { name: 'ReportID', mapping: 0 },
                                    { name: 'Name', mapping: 1 },
                                    { name: 'Comments', mapping: 2 },
                                    { name: 'Grouping', mapping: 3 },
                                    { name: 'LastModified', mapping: 4 },
                                    { name: 'LastUpdated', mapping: 5 },
                                    { name: 'CacheStatus', mapping: 6 },
                                    { name: 'ReportStatus', mapping: 7 },
                                    { name: 'LastError', mapping: 8 }

                                ],
                                "Name");
            dataStore.on('load', CheckCachingStatus);

            var combo = new Ext.form.ComboBox({
                name: 'perpage',
                width: 50,
                store: new Ext.data.SimpleStore({
                    fields: ['id'],
                    data: [
                                  ['25'],
                                  ['50'],
                                  ['75'],
                                  ['100']
                                ]
                }),
                mode: 'local',
                value: '25',

                listWidth: 50,
                triggerAction: 'all',
                displayField: 'id',
                valueField: 'id',
                editable: false,
                forceSelection: true
            });


            var pagingToolbar = new Ext.PagingToolbar({
                store: dataStore,
                pageSize: 25,
                displayInfo: true,
                displayMsg: '@{R=NCM.Strings;K=WEBJS_VY_46;E=js}',
                emptyMsg: "@{R=NCM.Strings;K=WEBJS_VY_45;E=js}",
                items: ['-', '@{R=NCM.Strings;K=WEBJS_VY_12;E=js} ', combo]
            });

            combo.on('select', function (combo, record) {
                var psize = parseInt(record.get('id'), 10);
                grid.store.baseParams['limit'] = psize;
                pagingToolbar.pageSize = psize;
                pagingToolbar.cursor = 0;
                pagingToolbar.doLoad(pagingToolbar.cursor);
            }, this);

            //CheckCachingStatus();
            grid = new Ext.grid.GridPanel({
                id: 'gridControl',
                store: dataStore,

                columns: [selectorModel, {
                    header: '@{R=NCM.Strings;K=WEBJS_VY_13;E=js}',
                    width: 80,
                    hidden: true,
                    hideable: false,
                    sortable: true,
                    dataIndex: 'ReportID'
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VY_41;E=js}',
                    width: 330,
                    sortable: true,
                    dataIndex: 'Name',
                    renderer: renderName
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VY_42;E=js}',
                    width: 150,
                    sortable: true,
                    dataIndex: 'LastUpdated',
                    renderer: renderLastUpdate
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VY_16;E=js}',
                    width: 150,
                    sortable: false,
                    dataIndex: 'Grouping',
                    renderer: renderGroups
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VY_43;E=js}',
                    width: 400,
                    sortable: false,
                    dataIndex: 'Comments',
                    renderer: renderComments
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VY_15;E=js}',
                    width: 150,
                    sortable: true,
                    dataIndex: 'LastModified',
                    hidden: true,
                    renderer: renderDateTime
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VY_44;E=js}',
                    width: 150,
                    sortable: false,
                    dataIndex: 'LastError',
                    hidden: true
                },
                 {
                     header: '@{R=NCM.Strings;K=WEBJS_VK_172;E=js}',
                     width: 150,
                     sortable: true,
                     dataIndex: 'ReportStatus',
                     renderer: renderReportStatus
                 }
                ],

                sm: selectorModel,

                layout: 'fit',
                autoScroll: 'true',
                loadMask: true,

                width: 750,
                height: 500,
                stripeRows: true,

                tbar: [{
                    id: 'ViewReportBtn',
                    text: '@{R=NCM.Strings;K=WEBJS_VY_24;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VY_25;E=js}',
                    icon: '/Orion/NCM/Resources/images/PolicyIcons/view_report.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        viewPolicyReport(grid.getSelectionModel().getSelected());
                    }
                }, '-', {
                    id: 'UpdateAllBtn',
                    text: '@{R=NCM.Strings;K=WEBJS_VY_26;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VY_27;E=js}',
                    icon: '/Orion/NCM/Resources/images/PolicyIcons/update_all.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { UpdateAll(); }

                }, '-', {
                    id: 'ManageBtn',
                    text: '@{R=NCM.Strings;K=WEBJS_VY_99;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VY_100;E=js}',
                    icon: '/Orion/NCM/Resources/images/PolicyIcons/manage_policy_reports_icon.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { managePolicyReport(); }

                }, '->', {
                    id: 'txtSearch',
                    xtype: 'textfield',
                    emptyText: '@{R=NCM.Strings;K=WEBJS_VY_37;E=js}',
                    width: 200,
                    enableKeyEvents: true,
                    listeners: {
                        keypress: function (obj, evnt) {
                            if (evnt.keyCode == 13) {
                                doSearch();
                            }
                        }
                    }
                }, {
                    id: 'Clean',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VY_08;E=js}',
                    iconCls: 'ncm_clear-btn',
                    cls: 'x-btn-icon',
                    hidden: true,
                    handler: function () { doClean(); }
                }, {
                    id: 'Search',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VY_09;E=js}',
                    iconCls: 'ncm_search-btn',
                    cls: 'x-btn-icon',
                    handler: function () { doSearch(); }
                }],

                bbar: pagingToolbar

            });

            SW.NCM.PersonalizeGrid(grid, columnModel);
            grid.on("statesave", gridColumnChangedHandler);
            grid.render('Grid');

            updateToolbarButtons();

            gridResize();

            loadGroupByValues();

            $("form").submit(function () { return false; });

            $(window).resize(function () {
                gridResize();
            });

            // highlight the search term (if we have one)            
            grid.getView().on("refresh", function () {
                var search = currentSearchTerm;
                if ($.trim(search).length == 0) return;

                var columnsToHighlight = SW.NCM.ColumnIndexByName(grid.getColumnModel(), ["@{R=NCM.Strings;K=WEBJS_VY_41;E=js}", "@{R=NCM.Strings;K=WEBJS_VY_16;E=js}"]);
                Ext.each(columnsToHighlight, function (columnNumber) {
                    SW.NCM.highlightSearchText("#Grid .x-grid3-body .x-grid3-td-" + columnNumber, search);
                });
            });


        }
    };

} ();

Ext.onReady(SW.NCM.PolicyReport.init, SW.NCM.PolicyReport);

