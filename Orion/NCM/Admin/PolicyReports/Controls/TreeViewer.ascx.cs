using System;
using System.Data;
using System.Collections;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebNavigator;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_Resources_PolicyReports_TreeViewer : System.Web.UI.UserControl
{
    public DataTable DataSource { get; set; }

    public bool Expanded { get; set; }

    public string NodeImageUrl
    {
        set { tree.Levels[1].LevelImage = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.Request.Browser.Browser.Equals(@"FIREFOX", StringComparison.InvariantCultureIgnoreCase))
        {
            tree.Width = Unit.Pixel(300);
            tree.Height = Unit.Pixel(400);
        }

        if (!Page.IsPostBack)
        {
            InitializeTree();
        }
    }

    private void InitializeTree()
    {
        DataTable dt = DataSource;

        if (dt == null)
        {
            return;
        }

        var groupingsCache = new Dictionary<string, Node>();
        foreach (DataRow row in dt.Rows)
        {
            var id = (Guid) row["ID"];
            string name = row["Name"] as string;
            string grouping = row["Grouping"] as string;

            AddInitialNode(id, name, grouping, groupingsCache);
        }
    }

    private void AddInitialNode(Guid id, string name, string grouping, IDictionary<string, Node> groupingsCache)
    {
        if (string.IsNullOrEmpty(grouping))
        {
            grouping = Resources.NCMWebContent.Compliance_NoFolder;
        }

        name = CommonHelper.EncodeXmlToString(name);
        grouping = CommonHelper.EncodeXmlToString(grouping);

        if (!groupingsCache.ContainsKey(grouping))
        {
            groupingsCache[grouping] = tree.Nodes.Add(grouping, @"L1");
        }

        Node groupingNode = groupingsCache[grouping];
        groupingNode.Nodes.Add(name, id);
    }

    private void AddNode(Guid id, string name, string grouping, IDictionary<string, Node> groupingsCache)
    {
        if (string.IsNullOrEmpty(grouping))
        {
            grouping = Resources.NCMWebContent.Compliance_NoFolder;
        }

        name = CommonHelper.EncodeXmlToString(name);
        grouping = CommonHelper.EncodeXmlToString(grouping);

        if (!groupingsCache.ContainsKey(grouping))
        {
            groupingsCache[grouping] = tree.Nodes.Search(grouping, @"L1", null, false) ?? tree.Nodes.Add(grouping, @"L1");
        }

        Node groupingNode = groupingsCache[grouping];
        Node subNode = groupingNode.Nodes.Search(name, id, groupingNode, false);
        if (subNode == null)
        {
            groupingNode.Expanded = Expanded;
            groupingNode.Nodes.Add(name, id);
        }

        tree.Nodes.Sort(true, false);
    }

    public void AddNodes(List<NodeEntry> nodes)
    {
        if (nodes == null)
            return;

        var groupingsCache = new Dictionary<string, Node>();
        foreach (NodeEntry node in nodes)
        {
            AddNode(node.ID, node.Name, node.Grouping, groupingsCache);
        }
    }

    public void RemoveSelectedNodes()
    {
        ArrayList selectedNodes = tree.CheckedNodes;

        if (selectedNodes == null)
            return;

        foreach (Node node in selectedNodes)
        {
            tree.Nodes[node.Parent.Index].Nodes.Remove(node);
            if (node.Parent.Nodes.Count == 0)
                tree.Nodes.Remove(node.Parent);
        }
    }

    public List<NodeEntry> GetAllNodes()
    {
        List<NodeEntry> nodes = new List<NodeEntry>();

        foreach (Node nodeL1 in tree.Nodes)
        {
            foreach (Node nodeL2 in nodeL1.Nodes)
                nodes.Add(new NodeEntry((Guid)nodeL2.Tag, nodeL2.Text, nodeL2.Parent.Text));
        }

        return nodes;
    }

    public List<NodeEntry> GetSelectedNodes()
    {
        if (tree.CheckedNodes.Count == 0)
            return null;

        List<NodeEntry> nodes = new List<NodeEntry>();

        foreach (Node node in tree.CheckedNodes)
        {
            nodes.Add(new NodeEntry((Guid)node.Tag, node.Text, node.Parent.Text));
        }

        return nodes;
    }
}

public class NodeEntry
{
    public Guid ID { get; set; }

    public string Name { get; set; }

    public string Grouping { get; set; }

    public NodeEntry(Guid id, string name, string grouping)
    {
        ID = id;
        Name = name;
        Grouping = grouping;
    }
}