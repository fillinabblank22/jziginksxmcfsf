using System;
using System.Linq;
using System.Web;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;

public partial class Orion_NCM_Resources_PolicyReports_Controls_NodeSelectionCriteria : System.Web.UI.UserControl
{
    private AdvToobarNodes toolBarNodes = new AdvToobarNodes();

    protected string MoreInformationLink
    {
        get
        {
            return
                $@"{SolarWinds.Orion.Web.DAL.WebSettingsDAL.HelpServer}/NetPerfMon/SolarWinds/default.htm?context=SolarWinds&file=OrionNCMPHPolicyReporterNodeChange.htm";
        }
    }

    public SelectionCriterias SelectionCriteria
    {
        get { return nodeSelectionCriteria.GenerateSelectionCriteria(); }
        set { nodeSelectionCriteria.SelectionCriteria = value; }
    }

    public void ResetToAllNodes()
    {
        nodeSelectionCriteria.ResetToAllNodes();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        ResourceSettings resourceSettings = new ResourceSettings();
        resourceSettings.Prefix = "PolicyPicker";
        NPMDefaultSettingsHelper.SetUserSettings("PolicyPickerGroupBy", "Vendor");

        toolBarNodes.ResourceSettings = resourceSettings;
        toolBarNodes.Visible = false;
        toolBarNodes.NodesControl.ExcludeUnmanagedNodes = true;

        toolBarNodes.ShowClearAll = true;
        toolBarNodes.ShowSelectAll = true;

        toolBarNodes.RemoveNodes();
        
        nodes_picker_container.Controls.Add(toolBarNodes);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        nodeSelectionCriteria.SelectNodes += new SolarWinds.NCMModule.Web.Resources.PolicyReportsManagement.Controls.dlgSelectNodes(nodeSelectionCriteria_SelectNodes);
        toolBarNodes.NodesControl.ShowConfigs = false;
        toolBarNodes.NodesControl.ShowGroupCheckBoxes = true;
        toolBarNodes.NodesControl.ShowNodeCheckBoxes = true;
        toolBarNodes.NodesControl.TreeControl.ClientSideEvents.NodeChecked = @"UltraWebTree_TheeLevelNodeCheckedDownloadConfig";

        uPanel_criteria.Update();
    }

    void nodeSelectionCriteria_SelectNodes()
    {
        System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), @"key_display_nodes_dialog", @"$(""#display_nodes_dialog"").click();", true);
    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        var selectedNodes = toolBarNodes.GetCheckedNodesIdsArrayList().ToList();

        SelectionCriterias criteria = nodeSelectionCriteria.SelectionCriteria;
        criteria.AssignedNodesList = selectedNodes;
        nodeSelectionCriteria.SelectionCriteria = criteria;
    }

    protected void btnRefreshNodes_Click(object sender, EventArgs e)
    {
        HttpContext.Current.Session[@"SelectClearAllNodes"] = false;
        toolBarNodes.Visible = true;
        toolBarNodes.RemoveNodes();

        uPanel_nodes.Update();
    }

    protected void btnRemoveNodes_Click(object sender, EventArgs e)
    {
        toolBarNodes.Visible = false;
        toolBarNodes.RemoveNodes();
        
        uPanel_nodes.Update();
    }
}
