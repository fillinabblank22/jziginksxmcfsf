<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Folder.ascx.cs" Inherits="Orion_NCM_Resources_PolicyReports_Controls_Folder" %>

<script type="text/javascript" src="/Orion/js/jquery.js"></script>

<script type="text/javascript">
    $(document).ready(function() 
    {
        var sValue = $('#<%=ddlFolderList.ClientID%>').val();
        ShowHideNewFolderContent(sValue);
    });
    
    function ddlFolderListSelectIndexChange(obj)
    {
        var sValue = obj.options[obj.selectedIndex].value;
        ShowHideNewFolderContent(sValue);
    }
    
    function ShowHideNewFolderContent(sValue)
    {
        if(sValue =='new_folder')
        {
            $('#newFolderContent').show();
        }
        else
        {
            $('#newFolderContent').hide();
        }
    }
</script>

<style type="text/css">
    .FolderList
    {
        width:250px;
        border:1px solid #89A6C0
    }
</style>

<table width="100%">
    <tr>
        <td style="width:20%;">
            <%=Resources.NCMWebContent.WEBDATA_VY_32%>
        </td>
        <td style="width:80%">
            <asp:DropDownList ID="ddlFolderList" runat="server" CssClass="FolderList" />
        </td>
    </tr>
</table>
<div id="newFolderContent">
    <table width="100%">
        <tr>
            <td style="width:20%;">
                <%=Resources.NCMWebContent.WEBDATA_VY_33%>
            </td>
            <td style="width:80%;">
                <asp:TextBox ID="txtNewFolder" runat="server" TextMode="SingleLine" Font-Size="12px"
                    Font-Names="Arial" BorderColor="#89A6C0" BorderWidth="1px" Width="250px" Height="17px" />
            </td>
        </tr>
    </table>
</div>
