<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeSelectionCriteria.ascx.cs" Inherits="Orion_NCM_Resources_PolicyReports_Controls_NodeSelectionCriteria" %>

<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.PolicyReportsManagement.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<style type="text/css">
    .autocomplete_completionListElement 
    {  
        visibility : hidden;
        margin : 0px !Important;
        padding: 0px !Important;
        background-color : #fff;
        color : #000;
        border : #ccc;
        border-width : 1px;
        border-style : solid;
        cursor : 'default';
        overflow : auto;
        height : 100px;
        text-align : left;
        font-size : 9pt;
        list-style: none !Important;
        width: 201px !Important;
        z-index: 9999999;
    }
    
    .autocomplete_completionListElement li
    {
        display: block;
        padding: 1px !Important;
        margin: 0px;
    }
    
    .autocomplete_highlightedListItem
    {
        background-color: Highlight;
        color: white;
        padding: 1px;
    }
    
    .autocomplete_listItem 
    {
        background-color: window;
        color: windowtext;
        padding: 1px;
    }
    
    .pagingToolbar { border: 0 none !important; background-color : #e2e1d4 }
    .pagingToolbar td {  border: 0 none !important; padding: 0 2px; }
    .pagingToolbar img { vertical-align: top !important; }
    .pagingToolbar a { white-space: nowrap; font-size: 8pt; }
    .pagingToolbar .disabled { color: silver; }
    .pagingToolbar .spacer { width: 18px; height: 15px; background: url('/Orion/NCM/Resources/images/ConfigChangeApproval/arrows.gif') no-repeat scroll 0 0 transparent; }
    .pagingToolbar .enabled .prev { background-position: 0px 0px !important; }
    .pagingToolbar .enabled .next { background-position: 0px -15px !important; }
    .pagingToolbar .disabled .prev { background-position: -18px 0px !important; }
    .pagingToolbar .disabled .next { background-position: -18px -15px !important; }
</style>

<script language="javascript">
    function showTooltip(on, id)
    {
        var div = document.getElementById(id);
        if(on)
        {
            div.style.display = 'block';
        } 
        else 
        {
            div.style.display = 'none';
        }
    }
    
    var moreInformationLink = "<%=MoreInformationLink%>";
</script> 

<div>
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <script type="text/javascript" src="/Orion/NCM/Admin/PolicyReports/Controls/NodeSelectionCriteria.js"></script>
    
    <asp:UpdatePanel ID="uPanel_criteria" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <ncm:NodeSelectionCriteria ID="nodeSelectionCriteria" runat="server"></ncm:NodeSelectionCriteria>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <div id="nodesDialog" class="x-hidden">
        <div id="nodesBody" class="x-panel-body" style="height:340px;width:580px;overflow:auto;">
            <asp:UpdatePanel ID="uPanel_nodes" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <table id="nodetreecontrol_editpolicy_picker" width="560px;">
                        <tr>
                            <td id="nodes_picker_container" runat="server">                                    
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    
    <div class="x-hidden">
        <asp:UpdatePanel ID="uPanel_buttons" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <asp:Button runat="server" ID="btnOk" OnClick="btnOk_Click" style="display:none;" CausesValidation="false" />
                <asp:Button runat="server" ID="btnRefreshNodes" OnClick="btnRefreshNodes_Click" style="display:none;" CausesValidation="false" />
                <asp:Button runat="server" ID="btnRemoveNodes" OnClick="btnRemoveNodes_Click" style="display:none;" CausesValidation="false" />
            </ContentTemplate>
        </asp:UpdatePanel>
        
        <input id="display_nodes_dialog" type="button" style="display:none;" />
        <input id="display_warning_dialog" type="button" style="display:none;" />
    </div>
</div>