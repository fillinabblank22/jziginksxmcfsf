<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TreeViewer.ascx.cs" Inherits="Orion_NCM_Resources_PolicyReports_TreeViewer" %>

<%@ Register Assembly="Infragistics2.WebUI.Shared.v8.2, Version=8.2.20082.1000, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Shared" TagPrefix="ignav" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebNavigator.v8.2, Version=8.2.20082.1000, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.UltraWebNavigator" TagPrefix="ignav" %>

<script type="text/javascript">
function NodeClick(treeId,nodeId)
{
    var node = igtree_getTreeById(treeId).getSelectedNode();
    var parentNode = node.getParent();
    if(parentNode == null)
    {
      var bExp = node.getExpanded();
      node.setExpanded(!bExp);
    }else
    {
     var bChecked = node.getChecked();
     node.setChecked(!bChecked);
    }
}
</script>
<ignav:UltraWebTree ID="tree" runat="server" Width="300px" Height="400px" BorderColor="Gray" BorderWidth="1px" BorderStyle="Solid" BackColor="White" Visible="true">
    <Levels>
        <ignav:Level Index="0" LevelCheckBoxes="False" LevelImage="../../../Resources/images/Folder.Icon.gif" />
        <ignav:Level  Index="1" LevelCheckBoxes="True"  LevelImage="../../../Resources/images/PolicyIcons/Policy.Icon.gif" />
    </Levels>
    <Images>
        <ExpandImage Url="../../../Resources/images/Button.Expand.gif" />
        <CollapseImage Url="../../../Resources/images/Button.Collapse.gif" />
    </Images>
    <ClientSideEvents NodeClick="NodeClick" />
</ignav:UltraWebTree>