using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

public partial class Orion_NCM_Resources_PolicyReports_Controls_Folder : System.Web.UI.UserControl
{

    private string _folderName;
    public string FolderName
    {
        get 
        {
            if (ddlFolderList.SelectedValue == @"new_folder")
                return txtNewFolder.Text;
            else
                return ddlFolderList.SelectedValue;
        }
        set 
        {
            _folderName = value; 
        }
    }

    private List<string> _dataSource;
    public List<string> DataSource
    {
        get { return _dataSource;  }
        set { _dataSource = value; }
    }
    
    public int MaxLength
    {
        set { txtNewFolder.MaxLength = value; }
    }

    protected override void OnLoad(EventArgs e)
    {
        if (!Page.IsPostBack)
            Initialize();

        base.OnLoad(e);
    }
    
    #region private methods

    private void Initialize()
    { 
        // add client side selected index handler
        ddlFolderList.Attributes.Add(@"onchange", @"ddlFolderListSelectIndexChange(this)");
        //populate dropdownlist

        ddlFolderList.Items.Clear();
        if (DataSource != null)
        {
            foreach (string item in DataSource)
            {
                if (string.IsNullOrEmpty(item))
                    continue;
                ddlFolderList.Items.Add(new ListItem(item, item));
            }
        }
        ddlFolderList.Items.Add(new ListItem(Resources.NCMWebContent.WEBDATA_VM_61, @"new_folder"));

        SelectFolder();
    }

    private void SelectFolder()
    {
        ddlFolderList.ClearSelection();
        if (!string.IsNullOrEmpty(_folderName))
        {
            int sIndex = -1;
            foreach (ListItem item in ddlFolderList.Items)
            {
                sIndex++;
                if (string.Equals(item.Value, _folderName, StringComparison.OrdinalIgnoreCase))
                {
                    ddlFolderList.Items[sIndex].Selected = true;
                    return;
                }
            }
            ddlFolderList.SelectedIndex = 0;
        }
        else
            ddlFolderList.SelectedIndex = ddlFolderList.Items.Count - 1;

    }
    #endregion
}
