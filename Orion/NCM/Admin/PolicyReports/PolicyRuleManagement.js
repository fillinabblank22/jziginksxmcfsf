﻿Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.QuickTips.init();

SW.NCM.PolicyRule = function () {

    var getGroupByValues = function (groupBy, onSuccess) {

        var param = Ext.util.Format.htmlDecode(groupBy);
        ORION.callWebService("/Orion/NCM/Services/PolicyRuleManagement.asmx",
                             "GetValuesAndCountForProperty", { property: groupBy },
                             function (result) {
                                 onSuccess(ORION.objectFromDataTable(result));
                             });
    };

    var getGroupDisplayText = function (text) {
        switch (text) {
            case 'No Folder':
                return '@{R=NCM.Strings;K=WEBJS_VY_78;E=js}';
            case 'No Report':
                return '@{R=NCM.Strings;K=WEBJS_VY_73;E=js}';
            case 'No Policy':
                return '@{R=NCM.Strings;K=WEBJS_VY_77;E=js}';
            case 'Informational':
                return '@{R=NCM.Strings;K=PolicyRuleManagement_InformationalErrorLevel;E=js}';
            case 'Warning':
                return '@{R=NCM.Strings;K=PolicyRuleManagement_WarningErrorLevel;E=js}';
            case 'Critical':
                return '@{R=NCM.Strings;K=PolicyRuleManagement_CriticalErrorLevel;E=js}';
            default:
                return text;
        }
    };

    var loadGroupByValues = function (groupBy) {
        var groupItems = $(".GroupItems").text("@{R=NCM.Strings;K=WEBJS_VY_19;E=js}");

        getGroupByValues(groupBy, function (result) {
            groupItems.empty();
            $(result.Rows).each(function () {
                var value = Ext.util.Format.htmlEncode(this.theValue);
                var text = $.trim(String(this.theValue));
                if (text.length > 20) { text = String.format("{0}...", text.substr(0, 20)); };
                var disp = (getGroupDisplayText(text)) + " (" + this.theCount + ")";
                var prefix = groupBy == "ErrorLevel" ? "-" + value.toLowerCase() : "";
                $("<a href='#'></a>")
                    .attr('value', Ext.util.Format.htmlEncode(this.theID || this.theValue)).attr('title', getGroupDisplayText(value))
					.text(disp).click(selectGroup).appendTo(".GroupItems").wrap("<li class='group-policyrule-" + groupBy.toLowerCase() + prefix + "'/>");
            });

            var stylePrefix = renderStylePrefix((ORION.Prefs.load("GroupBy") || groupBy), Ext.util.Format.htmlEncode(ORION.Prefs.load("GroupByValue") || ""));
            var toSelect = $(".group-policyrule-" + (ORION.Prefs.load("GroupBy") || groupBy).toLowerCase() + stylePrefix + " a[value='" + Ext.util.Format.htmlEncode(ORION.Prefs.load("GroupByValue")) + "']");
            if (toSelect.length === 0) {
                toSelect = $(".group-policyrule-" + groupBy.toLowerCase() + stylePrefix + " a:first");
            }

            toSelect.click();
        });
    };

    function renderStylePrefix(groupBy, groupByValue) {
        switch (groupBy) {
            case "ErrorLevel":
                switch (groupByValue) {
                    case "Informational":
                    case "Warning":
                    case "Critical":
                        return "-" + groupByValue.toLowerCase();
                    default:
                        return "-informational";
                }
            default:
                return "";
        }
    }

    var selectGroup = function () {
        $(this).parent().addClass("SelectedGroupItem").siblings().removeClass("SelectedGroupItem");

        var value = $(".SelectedGroupItem a").attr('value');
        value = Ext.util.Format.htmlDecode(value);
        ORION.Prefs.save('GroupByValue', value);
        var groupBy = combo.getValue();
        refreshObjects(groupBy, value, "");
        return false;
    };

    var currentSearchTerm = null;

    var refreshObjects = function (property, value, search, callback) {
        grid.store.removeAll();
        var pagingToolbar = grid.getBottomToolbar();
        pagingToolbar.cursor = gridCursor;
        grid.store.baseParams['limit'] = grid.getBottomToolbar().pageSize;
        grid.store.proxy.conn.jsonData = { property: property, value: value || "", search: search };
        currentSearchTerm = search;
        pagingToolbar.doLoad(pagingToolbar.cursor);
        gridCursor = 0;
    };

    var getSelectedRuleIds = function (items) {
        var ids = [];

        Ext.each(items, function (item) {
            ids.push(item.data.PolicyRuleID);
        });

        return ids;
    };

    var getColumnNumber = function (columnDataIndex) {
        for (var i = 0; i < grid.getColumnModel().config.length; i++) {
            var col = grid.getColumnModel().config[i];
            if (col.dataIndex == columnDataIndex)
                return i;
        }
    };

    var addNewRule = function () {
        var group = combo.getValue();
        var folder;
        if (group == 'Folder')
            folder = Ext.util.Format.htmlDecode($(".SelectedGroupItem >a").attr('value'));
        else
            folder = '';
        window.location = "/Orion/NCM/Admin/PolicyReports/EditPolicyRule.aspx?folder=" + folder;
    };

    var gridCursor = 0;
    var deleteSelectedRules = function (items) {
        var toDelete = getSelectedRuleIds(items);
        var waitMsg = Ext.Msg.wait("@{R=NCM.Strings;K=WEBJS_VY_59;E=js}");

        deleteRules(toDelete, function (result) {
            waitMsg.hide();
            ErrorHandler(result);
            var groupBy = combo.getValue();
            var currentPageRecordCount = grid.store.getCount();
            var selectedRecordCount = grid.getSelectionModel().getCount();
            if (groupBy == "NoGrouping") {
                if (currentPageRecordCount > selectedRecordCount) {
                    grid.store.reload();
                }
                else {
                    grid.store.load();
                }
            }
            else {
                if (currentPageRecordCount > selectedRecordCount) {
                    var pagingToolbar = grid.getBottomToolbar();
                    gridCursor = pagingToolbar.cursor;
                }
                else {
                    gridCursor = 0;
                }
                loadGroupByValues(groupBy);
            }
        });
    };

    var deleteRules = function (ids, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/PolicyRuleManagement.asmx",
                             "DeletePolicyRules", { policyRuleIds: ids },
                             function (result) {
                                 onSuccess(result);
                             });
    };

    var editRule = function (item) {
        editRuleInternal(item.data.PolicyRuleID, false);
    };

    var editRuleInternal = function (policyRuleId, isCopy) {
        var url = "/Orion/NCM/Admin/PolicyReports/EditPolicyRule.aspx?PolicyRuleID=" + policyRuleId;
        if (isCopy)
            url = String.format("{0}&IsCopy=true", url);
        window.location = url;
    }

    var copyRule = function (item) {
        var waitMsg = Ext.Msg.wait("@{R=NCM.Strings;K=WEBJS_IA_65;E=js}");
        ORION.callWebService("/Orion/NCM/Services/PolicyRuleManagement.asmx",
                             "CopyPolicyRule", { policyRuleId: item.data.PolicyRuleID },
                             function (result) {
                                 editRuleInternal(result, true);
                             },
                             function (result) {
                                 waitMsg.hide();
                                 var error = new Object();
                                 error.Error = true;
                                 error.Source = "@{R=NCM.Strings;K=PolicyRuleManagement_ErrorCopyRuleDialogTitle;E=js}";
                                 error.Msg = result;
                                 ErrorHandler(error);
                             });
    };

    var updateToolbarButtons = function () {
        var selCount = grid.getSelectionModel().getCount();
        var map = grid.getTopToolbar().items.map;

        var needsOnlyOneSelected = (selCount != 1);
        var needsAtLeastOneSelected = (selCount === 0);

        map.EditRule.setDisabled(needsOnlyOneSelected);
        map.CopyRule.setDisabled(needsOnlyOneSelected);
        map.DeleteRules.setDisabled(IsDemoMode ? true : needsAtLeastOneSelected);

        var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');
        if (grid.getStore().getCount() > 0 && selCount == grid.getStore().getCount()) {
            hd.addClass('x-grid3-hd-checker-on');
        } else {
            hd.removeClass('x-grid3-hd-checker-on');
        }
    };

    function gridStoreLoadHandler(o) {
        selectRowInGrid();
    }

    function ErrorHandler(result) {
        if (result != null && result.Error) {
            Ext.Msg.show({
                title: result.Source,
                msg: result.Msg,
                minWidth: 500,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        }
    }

    function renderName(value, meta, record) {
        return String.format('<img src="/Orion/NCM/Resources/images/PolicyIcons/Policy.Rule.gif" />&nbsp;<a href="#" class="policyRuleName" value="{0}">{1}</a>', record.data.PolicyRuleID, Ext.util.Format.htmlEncode(value));
    }

    function renderRemediation(value, meta, record)
    {
        switch (value) {
            case "A":
                return "@{R=NCM.Strings;K=PolicyRuleManagement_AutomaticRemediation;E=js}";
            case "N":
                return "@{R=NCM.Strings;K=WEBJS_VK_58;E=js}";
            case "M":
                return "@{R=NCM.Strings;K=PolicyRuleManagement_ManualRemediation;E=js}";
        }
    }

    function renderDateTime(value, meta, record) {
        var date = eval(value.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
        return Ext.util.Format.date(date, ExtDaTimeLocalePattern.DateFull);
    }

    function renderFolder(value, meta, record) {
        var grouping = value;
        grouping = grouping || '';

        if (grouping.length === 0) {
            return "@{R=NCM.Strings;K=WEBJS_VY_78;E=js}";
        }

        res = String.format('<a href="#" class="groupLink">{0}</a>', Ext.util.Format.htmlEncode(grouping));

        return res;
    }

    function renderAppearsInReport(value, meta, record) {
        if (value != null) {
            var arr = value.split(",");

            var comma = "";
            var res = "";
            for (var i = 0; i < arr.length; i++) {
                var reports = arr[i].split(":");
                res += String.format('{0}<a href="/Orion/NCM/ComplianceReportResult.aspx?ReportID={2}">{1}</a>', comma, reports[0], reports[1]);
                comma = ", ";
            }

            return res;
        }

        return "@{R=NCM.Strings;K=WEBJS_VY_73;E=js}";
    }

    function renderAppearsInPolicy(value, meta, record) {
        if (value != null)
            return Ext.util.Format.htmlEncode(value);
        else
            return "@{R=NCM.Strings;K=WEBJS_VY_77;E=js}";
    }

    function jumpToGroup(groupName) {
        var groupBy = combo.getValue();
        if (groupBy != "Folder") {
            ORION.Prefs.save('GroupBy', "Folder");
            ORION.Prefs.save('GroupByValue', groupName);

            combo.setValue("Folder");
            loadGroupByValues("Folder");
            refreshObjects("Folder", groupName, "");
        }
        else {
            $(".group-policyrule-" + groupBy.toLowerCase() + " a[value='" + Ext.util.Format.htmlEncode(groupName) + "']").click();
        }
    }

    function showDescription(obj) {
        var policyRuleId = obj.attr("value");
        var body = $("#snippetDescriptionBody").text("@{R=NCM.Strings;K=WEBJS_VY_19;E=js}");

        ORION.callWebService("/Orion/NCM/Services/PolicyRuleManagement.asmx",
                             "GetPolicyRuleDescription", { policyRuleId: policyRuleId },
                             function (result) {
                                 var desc = $.trim(result.description);
                                 desc = desc.length != 0 ? desc : "@{R=NCM.Strings;K=WEBJS_VY_72;E=js}";
                                 desc = Ext.util.Format.htmlEncode(desc);
                                 body.empty().html('<div style="font-weight: bold;">Description:</div>' + desc);
                             });

        return SW.NCM.ShowDescription(obj);
    }

    function PolicyRuleNameClickHandler(obj) {
        var policyRuleId = obj.attr("value");
        var url = "/Orion/NCM/Admin/PolicyReports/EditPolicyRule.aspx?PolicyRuleID=" + policyRuleId;
        window.location = url;

    };

    var doClean = function () {
        var map = grid.getTopToolbar().items.map;
        var groupBy = combo.getValue();

        map.Clean.setVisible(false);
        map.txtSearch.setValue('');
        if (groupBy == 'NoGrouping')
            refreshObjects("", "", "");
        else
            loadGroupByValues(groupBy);
    }

    var doSearch = function () {

        var map = grid.getTopToolbar().items.map;
        var groupBy = combo.getValue();
        var search = map.txtSearch.getValue();

        if ($.trim(search).length == 0) return;
        var value = $(".SelectedGroupItem a").attr('value');
        value = Ext.util.Format.htmlDecode(value);
        var prefix = groupBy == "ErrorLevel" ? "-" + value.toLowerCase() : "";
        $(".group-policyrule-" + groupBy.toLowerCase() + prefix).removeClass("SelectedGroupItem");
        refreshObjects("", "", search);

        map.Clean.setVisible(true);

    }


    gridColumnChangedHandler = function (component, state) {
        if (component == null)
            return;

        var gridCM = component.getColumnModel();
        var settingName = 'Columns';

        if (gridCM != null && settingName != '') {

            var jsonvalue = SW.NCM.GridColumnsToJson(gridCM);
            ORION.Prefs.save(settingName, jsonvalue);
        }
    }
    var doReportSelection = false;
    var openToVal = null;
    NavToPage = function (component) {
        openToVal = Ext.getUrlParam('opento');
        if (openToVal == null)
            return;


        var params = {};
        params.dir = grid.store.sortInfo.direction;
        params.sort = grid.store.sortInfo.field;
        params.limit = grid.getBottomToolbar().pageSize;
        params.policyId = openToVal;

        //var strPrms ="sort="+ params.sort + "&dir="+ params.dir+"&reportId="+params.reportId +"&limit="+params.limit;
        var strPrms = Ext.encode(params);
        //string sort, string dir, string reportId, int limit

        var start = 0;
        $.ajax({
            async: false,
            type: "POST",
            url: "/Orion/NCM/Services/PolicyRuleManagement.asmx/FindPolicyRuleInGrid",
            contentType: "application/json; charset=utf-8",
            data: strPrms,
            dataType: "json",
            success: function (res) {
                doReportSelection = true;
                var obj = Ext.util.JSON.decode(res.d);
                if (obj.success == true) {
                    $("#" + ORION.prefix + 'GroupByValue').val('');
                    $("#" + ORION.prefix + 'GroupBy').val('NoGrouping');
                    start = obj.data.start;
                } else {
                    start = 0;
                    doReportSelection = false;
                }

            },
            error: function (xhr, status, errorThrown) {
                doReportSelection = false;
                start = 0;
            }
        });

        if (start > 0) {
            gridCursor = start;
            grid.store.baseParams['start'] = start;
            grid.getBottomToolbar().cursor = start / grid.getBottomToolbar().pageSize;
        }
    }

    function selectRowInGrid() {
        if (doReportSelection == true) {
            grid.store.baseParams = {};
            doReportSelection = false;
            if (openToVal != null) {
                var nm = getRuleIndex(openToVal);
                if (nm != null && nm >= 0) {
                    grid.getView().focusRow(nm);
                    grid.getSelectionModel().selectRow(nm);
                }
            }
        }
    }

    function getRuleIndex(ruleId) {
        var items = grid.store.data.items;
        var index = Ext.each(items, function (item) {
            if (item.data.PolicyRuleID == ruleId)
                return false;
        });

        return index;
    }

    function gridResize() {
        if (grid != null) {
            grid.setWidth(1);
            grid.setWidth($('#gridCell').width());

            var h = getGridHeight()
            grid.setHeight(getGridHeight());

            var groupItems = $(".GroupItems");
            groupItems.height(h - $(".ncm_GroupSection").height() - 14);
        }
    }

    function getGridHeight() {
        var top = $('#gridCell').offset().top;
        return $(window).height() - top - $('#footer').height() - 100;
    }

    ORION.prefix = "NCM_PolicyRuleMgmt_";

    var selectorModel;
    var dataStore;
    var grid;
    var combo;

    return {
        init: function () {

            if ($("[id$='_ContentContainer']").length == 0)
                return false;

            $("#Grid").click(function (e) {

                var obj = $(e.target);

                if (obj.hasClass("ncm_searchterm"))
                    obj = obj.parent();

                if (obj.hasClass('groupLink')) {
                    jumpToGroup($(e.target).text());
                    return false;
                }

                if (obj.hasClass('policyRuleName')) {
                    PolicyRuleNameClickHandler(obj);
                    return false;
                }
            });

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", updateToolbarButtons);

            dataStore = new ORION.WebServiceStore(
                                "/Orion/NCM/Services/PolicyRuleManagement.asmx/GetPolicyRulesPaged",
                                [
                                    { name: 'PolicyRuleID', mapping: 0 },
                                    { name: 'Name', mapping: 1 },
                                    { name: 'LastModified', mapping: 2 },
                                    { name: 'Grouping', mapping: 3 },
                                    { name: 'AppearsInReport', mapping: 4 },
                                    { name: 'AppearsInPolicy', mapping: 5 },
                                    { name: 'Remediation', mapping: 6 }
                                ],
                                "Name");
            dataStore.on('load', gridStoreLoadHandler);

            var comboPS = new Ext.form.ComboBox({
                name: 'perpage',
                width: 50,
                store: new Ext.data.SimpleStore({
                    fields: ['id'],
                    data: [
                                  ['25'],
                                  ['50'],
                                  ['75'],
                                  ['100']
                    ]
                }),
                mode: 'local',
                value: '25',

                listWidth: 50,
                triggerAction: 'all',
                displayField: 'id',
                valueField: 'id',
                editable: false,
                forceSelection: true
            });

            var pagingToolbar = new Ext.PagingToolbar({
                store: dataStore,
                pageSize: 25,
                displayInfo: true,
                displayMsg: '@{R=NCM.Strings;K=WEBJS_VY_60;E=js}',
                emptyMsg: "@{R=NCM.Strings;K=WEBJS_VY_61;E=js}",
                items: ['-', '@{R=NCM.Strings;K=WEBJS_VY_12;E=js} ', comboPS]
            });

            comboPS.on('select', function (combo, record) {
                var psize = parseInt(record.get('id'), 10);
                grid.store.baseParams['limit'] = psize;
                pagingToolbar.pageSize = psize;
                pagingToolbar.cursor = 0;
                pagingToolbar.doLoad(pagingToolbar.cursor);
            }, this);

            grid = new Ext.grid.GridPanel({
                id: 'gridControl',
                store: dataStore,

                columns: [selectorModel, {
                    header: '@{R=NCM.Strings;K=WEBJS_VY_13;E=js}',
                    width: 80,
                    hidden: true,
                    hideable: false,
                    sortable: true,
                    dataIndex: 'PolicyRuleID'
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VY_62;E=js}',
                    width: 250,
                    sortable: true,
                    dataIndex: 'Name',
                    renderer: renderName
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VY_15;E=js}',
                    width: 150,
                    sortable: true,
                    dataIndex: 'LastModified',
                    renderer: renderDateTime
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VY_16;E=js}',
                    width: 200,
                    sortable: true,
                    dataIndex: 'Grouping',
                    renderer: renderFolder
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VY_17;E=js}',
                    width: 300,
                    sortable: false,
                    dataIndex: 'AppearsInReport',
                    renderer: renderAppearsInReport
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VY_63;E=js}',
                    width: 300,
                    sortable: false,
                    dataIndex: 'AppearsInPolicy',
                    renderer: renderAppearsInPolicy
                },
                {
                    header: '@{R=NCM.Strings;K=PolicyRuleManagement_RemediationDefined_ColumnHeader;E=js}',
                    width: 150,
                    sortable: true,
                    dataIndex: 'Remediation',
                    renderer: renderRemediation
                }
                ],

                sm: selectorModel,

                layout: 'fit',
                autoScroll: 'true',
                loadMask: true,

                //width: 750,
                height: 500,
                stripeRows: true,

                tbar: [{
                    id: 'AddNewRule',
                    text: '@{R=NCM.Strings;K=WEBJS_VY_64;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VY_65;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_addsnippet.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { addNewRule(); }
                }, '-', {
                    id: 'EditRule',
                    text: '@{R=NCM.Strings;K=WEBJS_VY_03;E=js}',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VY_66;E=js}',
                    icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_editsnippet.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { editRule(grid.getSelectionModel().getSelected()); }
                },
                    '-', {
                        id: 'CopyRule',
                        text: '@{R=NCM.Strings;K=WEBJS_VM_10;E=js}',
                        tooltip: '@{R=NCM.Strings;K=WEBJS_IA_62;E=js}',
                        icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_clone.gif',
                        cls: 'x-btn-text-icon',
                        handler: function () { copyRule(grid.getSelectionModel().getSelected()); }
                    },
                     '-', {
                         id: 'DeleteRules',
                         text: '@{R=NCM.Strings;K=WEBJS_VY_05;E=js}',
                         tooltip: '@{R=NCM.Strings;K=WEBJS_VY_67;E=js}',
                         icon: '/Orion/NCM/Resources/images/icon_delete.png',
                         cls: 'x-btn-text-icon',
                         handler: function () {
                             Ext.Msg.confirm("@{R=NCM.Strings;K=WEBJS_VY_68;E=js}",
                                "@{R=NCM.Strings;K=WEBJS_VY_69;E=js}",
                                function (btn, text) {
                                    if (btn == 'yes') {
                                        deleteSelectedRules(grid.getSelectionModel().getSelections());
                                    }
                                });
                         }
                     }, '->', {
                         id: 'txtSearch',
                         xtype: 'textfield',
                         emptyText: '@{R=NCM.Strings;K=WEBJS_VY_70;E=js}',
                         width: 200,
                         enableKeyEvents: true,
                         listeners: {
                             keypress: function (obj, evnt) {
                                 if (evnt.keyCode == 13) {
                                     doSearch();
                                 }
                             }
                         }
                     }, {
                         id: 'Clean',
                         tooltip: '@{R=NCM.Strings;K=WEBJS_VY_08;E=js}',
                         iconCls: 'ncm_clear-btn',
                         cls: 'x-btn-icon',
                         hidden: true,
                         handler: function () { doClean(); }
                     }, {
                         id: 'Search',
                         tooltip: '@{R=NCM.Strings;K=WEBJS_VY_09;E=js}',
                         iconCls: 'ncm_search-btn',
                         cls: 'x-btn-icon',
                         handler: function () { doSearch(); }
                     }],

                bbar: pagingToolbar

            });

            SW.NCM.PersonalizeGrid(grid, columnModel);
            grid.on("statesave", gridColumnChangedHandler);
            grid.on("render", NavToPage);

            grid.render('Grid');

            combo = new Ext.form.ComboBox({
                width: 200,
                store: [['Folder', '@{R=NCM.Strings;K=WEBJS_VY_16;E=js}'], ['ErrorLevel', '@{R=NCM.Strings;K=WEBJS_VY_71;E=js}'], ['AppearsInReport', '@{R=NCM.Strings;K=WEBJS_VY_17;E=js}'], ['AppearsInPolicy', '@{R=NCM.Strings;K=WEBJS_VY_63;E=js}'], ['NoGrouping', '@{R=NCM.Strings;K=WEBJS_VY_20;E=js}']],
                value: 'NoGrouping',
                listWidth: 200,
                triggerAction: 'all',
                editable: false,
                forceSelection: true,
                renderTo: 'GroupBy'
            });

            updateToolbarButtons();

            gridResize();
            $(window).resize(function () {
                gridResize();
            });

            var groupBy = ORION.Prefs.load("GroupBy") || "NoGrouping";
            combo.setValue(groupBy);
            if (groupBy != "NoGrouping") {
                loadGroupByValues(groupBy);
            }
            else {
                var groupItems = $(".GroupItems").text("@{R=NCM.Strings;K=WEBJS_VY_19;E=js}");
                refreshObjects("", "", "");
                groupItems.empty();
            }

            $("form").submit(function () { return false; });

            combo.on('select', function (combo, record) {
                if (combo.getValue() == "NoGrouping") {
                    ORION.Prefs.save('GroupByValue', "");

                    var groupItems = $(".GroupItems").text("@{R=NCM.Strings;K=WEBJS_VY_19;E=js}");
                    refreshObjects("", "", "");
                    groupItems.empty();
                }
                else {
                    loadGroupByValues(combo.getValue());
                }
                ORION.Prefs.save('GroupBy', combo.getValue());
            });

            grid.getView().on("refresh", function () {
                var search = currentSearchTerm;
                if ($.trim(search).length == 0) return;

                var columnsToHighlight = SW.NCM.ColumnIndexByName(grid.getColumnModel(), ["@{R=NCM.Strings;K=WEBJS_VY_62;E=js}", "@{R=NCM.Strings;K=WEBJS_VY_16;E=js}"]);
                Ext.each(columnsToHighlight, function (columnNumber) {
                    SW.NCM.highlightSearchText("#Grid .x-grid3-body .x-grid3-td-" + columnNumber, search);
                });
            });
        }
    };

}();


Ext.onReady(SW.NCM.PolicyRule.init, SW.NCM.PolicyRule);
