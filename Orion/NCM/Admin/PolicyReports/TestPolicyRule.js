﻿Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.QuickTips.init();

SW.NCM.TestPolicyRule = function() {

    var winDlg;
    
    this.TestPolicyRule_Click = function()    
    {
        if(!winDlg)
        {
            winDlg = new Ext.Window({
	            applyTo: 'dialogContainer',
                layout: 'fit',
                width: 700,
                height: 550,
                closeAction: 'hide',
                plain: true,
                autoScroll: false,
	            contentEl: 'mainContainer',
                title: '@{R=NCM.Strings;K=WEBJS_VK_59;E=js}',
                resizable: false,
                modal: true,
                buttons: [{
                    text: '@{R=NCM.Strings;K=WEBJS_VK_07;E=js}',
                    handler: function(){
                        winDlg.hide();
                    }
                }]
            });
        }
       
        winDlg.alignTo(document.body, "c-c");
        winDlg.show(this);

        return false;
    }
    
}

Ext.onReady(function(){
    TestPolicyRule = new SW.NCM.TestPolicyRule();
});