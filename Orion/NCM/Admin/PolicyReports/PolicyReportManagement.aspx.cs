using System;
using System.Web.UI;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.NCM.Common.Help;
using SolarWinds.Orion.Web;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;

public partial class Orion_NCM_Resources_PolicyReports_PolicyReportManagement : Page
{
    protected String _thwackUserInfo = @"{name:'',pass:'',valid:false}";

    protected int disabledReportValue = (int)ReportStatus.Disabled;
    protected int enabledReportValue = (int)ReportStatus.Enabled;

    private readonly ICommonHelper commonHelper;
    private readonly IIsWrapper isLayer;

    public Orion_NCM_Resources_PolicyReports_PolicyReportManagement()
    {
        commonHelper = ServiceLocator.Container.Resolve<ICommonHelper>();
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
    }

    protected override void OnInit(EventArgs e)
    {
        var validator = new SWISValidator();
        validator.ContentContainer = ContentContainer;
        validator.RedirectIfValidationFailed = true;
        ErrorContainer.Controls.Add(validator);

        if (validator.IsValid)
        {
            commonHelper.Initialise();
            IsPermissionExist();
        }

        base.OnInit(e);
    }

    private void IsPermissionExist()
    {
        using (var proxy = isLayer.GetProxy())
        {
            var complianceMgmtOnlyForAdmins = Convert.ToBoolean(proxy.Cirrus.Settings.GetSetting(SolarWinds.NCM.Contracts.InformationService.Settings.ComplianceMgmtOnlyForAdmins, false, null));
            if (complianceMgmtOnlyForAdmins)
            {
                if (CommonHelper.IsDemoMode()
                     || (!Profile.AllowAdmin && !(this.Page is IBypassAccessLimitation)))
                {
                    ErrorControl.Visible = true;
                    ErrorControl.VisiblePermissionLink = false;
                    ErrorControl.ErrorMessage = Resources.NCMWebContent.WEBDATA_VK_898;
                    ContentContainer.Visible = false;
                }
            }
            else
            {
                if (!SecurityHelper.IsPermissionExist(SecurityHelper._canDownload))
                {
                    ErrorControl.Visible = true;
                    ErrorControl.VisiblePermissionLink = true;
                    ErrorControl.ErrorMessage = Resources.NCMWebContent.WEBDATA_VK_899;
                    ContentContainer.Visible = false;
                }
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var cred = Session[@"NCM_ThwackCredential"] as System.Net.NetworkCredential;

        bool credValid;
        var vc = Session[@"NCM_ThwackCredValidation"];
        if (vc != null)
            credValid = (bool)vc;
        else
            credValid = false;

        if (cred != null)
        {
            this._thwackUserInfo = String.Format(@"{0}name:'{1}',pass:'{2}', valid:{4}{3}",
                                     @"{",
                                     cred.UserName.Replace(@"\", @"&#92;"),
                                     cred.Password.Replace(@"\", @"&#92;"),
                                     @"}",
                                     credValid.ToString().ToLower());
        }

    }

    protected string LearnMoreManagePolicyReports
    {
        get
        {
            var helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
            return helpHelperWrap.GetHelpUrl("OrionNCMWebManagePolicyReports");
        }
    }


}
