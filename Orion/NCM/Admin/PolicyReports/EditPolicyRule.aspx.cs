using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.NCM.Common.Help;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;
using StringRegistrar = SolarWinds.Orion.Core.Common.i18n.Registrar.ResourceManagerRegistrar;


public partial class Orion_NCM_Resources_PolicyReports_EditPolicyRule : System.Web.UI.Page
{
    #region Members

    private const string QUERY_RULEID_PARAM = "PolicyRuleID";
    private const string QUERY_FOLDER_PARAM = "Folder";
    private const string QUERY_ISCOPY_PARAM = "IsCopy";

    private Guid? _policyRuleID;
    private string _folder = string.Empty;
    private bool _addMode;
    private bool _hasPerm = true;
    private ISWrapper _isLayer = new ISWrapper();

    #endregion

    #region Properties

    private bool IsCopy
    {
        get
        {
            var isCopy = false;
            if (!string.IsNullOrEmpty(Page.Request.QueryString[QUERY_ISCOPY_PARAM]))
                isCopy = string.Equals(Page.Request.QueryString[QUERY_ISCOPY_PARAM].ToLowerInvariant(), @"true");
            return isCopy;
        }
    }

    #endregion

    #region Override Handlers

    protected override void OnInit(EventArgs e)
    {
        btnSubmit.Visible = !CommonHelper.IsDemoMode();
        InitializeCommponent();
        try
        {
            _policyRuleID = GetRuleIdParameterValue();
            _folder = Page.Request.QueryString[QUERY_FOLDER_PARAM];

            if (!_policyRuleID.HasValue)
            {
                _addMode = true;
                btnHelp.HelpUrlFragment = @"OrionNCMWebManagePolicyReportsCreateRule";
            }
            else
            {
                _addMode = false;
                btnHelp.HelpUrlFragment = @"OrionNCMWebManagePolicyReportsEditRule";
            }

            ValidatePermissions();

            var folders = GetAllFolders();
            txtFolder.DataSource = folders;
            base.OnInit(e);
        }
        catch (InvalidOperationException ex)
        {
            _hasPerm = false;
            DisplayException(ex);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (_hasPerm)
        {
            try
            {
                LoadPolicyRule();
            }
            catch (Exception ex)
            {
                DisplayException(ex);
            }
        }
    }

    protected void chkUseAdvancedMode_OnCheckedChanged(object sender, EventArgs e)
    {
        SetAdvancedMode(chkUseAdvancedMode.Checked);
    }

    protected void rbEntireConfigFile_CheckedChanged(object sender, EventArgs e)
    {
        if (rbEntireConfigFile.Checked)
        {
            ConfigBlockHolder.Visible = false;
        }
    }

    protected void rbConfigBlock_CheckedChanged(object sender, EventArgs e)
    {
        if (rbConfigBlock.Checked)
        {
            ConfigBlockHolder.Visible = true;
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (CommonHelper.IsDemoMode()) return;

        try
        {
            if (ValidatePatterns())
            {
                var id = SavePolicyRule();
                Response.Redirect("PolicyRuleManagement.aspx?opento=" + id);
            }
        }
        catch (Exception ex)
        {
            DisplayException(ex);
        }
    }

    private Guid? GetRuleIdParameterValue()
    {
        var value = Page.Request.QueryString[QUERY_RULEID_PARAM];
        Guid id;
        if (!string.IsNullOrWhiteSpace(value) && Guid.TryParse(value, out id))
        {
            return id;
        }

        return null;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        var id = GetRuleIdParameterValue();

        if (IsCopy && id.HasValue)
        {
            using (var proxy = _isLayer.Proxy)
            {
                proxy.Cirrus.PolicyReports.DeletePolicyRules(new []
                {
                    id.Value
                });
            }
        }
        Response.Redirect("PolicyRuleManagement.aspx");
    }

    protected void btnTestPolicyRule_Click(object sender, EventArgs e)
    {
        if (ValidatePatterns())
        {
            Panel_TestPolicyRule.Update();

            TestPolicyRule.Visible = true;
            TestPolicyRule.PolicyRule = PolicyRuleForTest();
            TestPolicyRule.LoadControl();

            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), @"key_display_test_policy_rule_dialog", @"$(""#display_test_policy_rule_dialog"").click();", true);
        }
    }

    #endregion

    #region Private Functions

    private void InitializeCommponent()
    {
        using (var proxy = _isLayer.Proxy)
        {
            var level_1 = proxy.Cirrus.Settings.GetSetting(Settings.PolicyViolationLevel1, StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"PolicyViolationsLevel_Informational"), null);
            var level_2 = proxy.Cirrus.Settings.GetSetting(Settings.PolicyViolationLevel2, StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"PolicyViolationsLevel_Warning"), null);
            var level_3 = proxy.Cirrus.Settings.GetSetting(Settings.PolicyViolationLevel3, StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"PolicyViolationsLevel_Critical"), null);

            rbInformational.Text =
                $@"<img src='/Orion/NCM/Resources/images/PolicyIcons/Policy.Info.gif'/>&nbsp;{level_1}";
            rbWarning.Text = $@"<img src='/Orion/NCM/Resources/images/PolicyIcons/Policy.Warning.gif'/>&nbsp;{level_2}";
            rbCritical.Text =
                $@"<img src='/Orion/NCM/Resources/images/PolicyIcons/Policy.Critical.gif'/>&nbsp;{level_3}";
        }

    }
    /// <summary>
    /// Validate multi-line policy rule patterns.
    /// </summary>
    /// <returns></returns>
    private bool ValidatePatterns()
    {
        if (chkUseAdvancedMode.Checked && multiLineRulePatterns.Patterns.Count > 0)
        {
            var policyRule = new PolicyRule();
            policyRule.MultiLineRulePatterns = multiLineRulePatterns.Patterns;
            if (!policyRule.EvaluateExpression())
            {
                validatorMultiLineRulePatterns.ErrorMessage = Resources.NCMWebContent.WEBDATA_VY_76;
                validatorMultiLineRulePatterns.IsValid = false;

                Panel_PolicyRule.Update();

                return false;
            }
        }

        return true;
    }

    /// <summary>
    /// Policy Rule for testing.
    /// </summary>
    /// <returns></returns>
    private PolicyRule PolicyRuleForTest()
    {
        PolicyRule policyRule;
        string blockStart;
        string blockEnd;

        policyRule = LoadPolicyRule(GetRuleIdParameterValue());
        policyRule.RuleName = txtPolicyRuleName.Text;
        policyRule.Comments = txtDescription.Text;
        policyRule.RemediateScript = loadScriptCtrl.ScriptText;
        policyRule.AdvancedMode = chkUseAdvancedMode.Checked;
        policyRule.SimplePatternText = txtPattern.Text;
        policyRule.PatternType = GetPatternType();
        policyRule.MultiLineRulePatterns = multiLineRulePatterns.Patterns;
        GetSearchIn(out blockStart, out blockEnd);
        policyRule.ConfigBlockStart = blockStart;
        policyRule.ConfigBlockEnd = blockEnd;
        policyRule.ConfigBlockPatternType = GetConfigBlockPatternType();
        policyRule.ConfigBlockMustExist = chkConfigBlockMustExist.Checked;
        policyRule.PatternMustExist = GetPatternMustExist();
        policyRule.ErrorLevel = GetErrorLevel();
        policyRule.Grouping = txtFolder.FolderName;
        policyRule.ExecuteScriptAutomatically = false;
        policyRule.Owner = HttpContext.Current.Profile.UserName;
        policyRule.RemediateScriptType = GetRemediateScriptType();
        policyRule.ExecuteRemediationScriptPerBlock = chkExecuteRemediationScriptPerBlock.Checked;
        policyRule.ExecuteScriptInConfigMode = chkExecuteScriptInConfigMode.Checked;

        return policyRule;
    }

    /// <summary>
    /// Load policy rule properties.
    /// </summary>
    private void LoadPolicyRule()
    {
        var policyRule = LoadPolicyRule(_policyRuleID);

        if (_addMode)
        {
            Page.Title = Resources.NCMWebContent.WEBDATA_VM_70;
        }
        else
        {
            Page.Title = string.Format(Resources.NCMWebContent.WEBDATA_VM_69, CommonHelper.EncodeXmlToString(policyRule.RuleName));
        }

        if (!Page.IsPostBack)
        {
            txtPolicyRuleName.Text = policyRule.RuleName;
            txtDescription.Text = policyRule.Comments;
            loadScriptCtrl.ScriptText = policyRule.RemediateScript;
            chkUseAdvancedMode.Checked = policyRule.AdvancedMode;
            SetAdvancedMode(policyRule.AdvancedMode);
            txtPattern.Text = policyRule.SimplePatternText;
            SetPatternType(policyRule.PatternType);
            multiLineRulePatterns.Patterns = policyRule.MultiLineRulePatterns;
            SetSearchIn(policyRule.ConfigBlockStart, policyRule.ConfigBlockEnd);
            SetConfigBlockPatternType(policyRule.ConfigBlockPatternType);
            chkConfigBlockMustExist.Checked = policyRule.ConfigBlockMustExist;
            SetPatternMustExist(policyRule.PatternMustExist);
            SetErrorLevel(policyRule.ErrorLevel);
            txtFolder.FolderName = _addMode ? _folder : policyRule.Grouping;
            chkExecuteScriptAutomatically.Checked = policyRule.ExecuteScriptAutomatically;
            SetRemediateScriptType(policyRule.RemediateScriptType);
            chkExecuteRemediationScriptPerBlock.Checked = policyRule.ExecuteRemediationScriptPerBlock;
            chkExecuteScriptInConfigMode.Checked = policyRule.ExecuteScriptInConfigMode;
        }
    }

    /// <summary>
    /// Load policy rule.
    /// </summary>
    /// <param name="policyRuleId"></param>
    /// <returns></returns>
    private PolicyRule LoadPolicyRule(Guid? policyRuleId)
    {
        if (!policyRuleId.HasValue)
        {
            return new PolicyRule() {ExecuteScriptInConfigMode = true};
        }

        using (var proxy = _isLayer.Proxy)
        {
            return proxy.Cirrus.PolicyReports.GetPolicyRule(policyRuleId.Value);
        }
    }

    /// <summary>
    /// Save policy rule.
    /// </summary>
    /// <returns></returns>
    private Guid SavePolicyRule()
    {
        PolicyRule policyRule;
        string blockStart;
        string blockEnd;

        policyRule = LoadPolicyRule(GetRuleIdParameterValue());
        policyRule.RuleName = txtPolicyRuleName.Text;
        policyRule.Comments = txtDescription.Text;
        policyRule.RemediateScript = loadScriptCtrl.ScriptText;
        policyRule.AdvancedMode = chkUseAdvancedMode.Checked;
        policyRule.SimplePatternText = txtPattern.Text;
        policyRule.PatternType = GetPatternType();
        policyRule.MultiLineRulePatterns = multiLineRulePatterns.Patterns;
        GetSearchIn(out blockStart, out blockEnd);
        policyRule.ConfigBlockStart = blockStart;
        policyRule.ConfigBlockEnd = blockEnd;
        policyRule.ConfigBlockPatternType = GetConfigBlockPatternType();
        policyRule.ConfigBlockMustExist = chkConfigBlockMustExist.Checked;
        policyRule.PatternMustExist = GetPatternMustExist();
        policyRule.ErrorLevel = GetErrorLevel();
        policyRule.Grouping = txtFolder.FolderName;
        policyRule.ExecuteScriptAutomatically = chkExecuteScriptAutomatically.Checked;
        policyRule.Owner = HttpContext.Current.Profile.UserName;
        policyRule.RemediateScriptType = GetRemediateScriptType();
        policyRule.ExecuteRemediationScriptPerBlock = chkExecuteRemediationScriptPerBlock.Checked;
        policyRule.ExecuteScriptInConfigMode = chkExecuteScriptInConfigMode.Checked;

        using (var proxy = _isLayer.Proxy)
        {
            if (_addMode)
            {
                return proxy.Cirrus.PolicyReports.AddPolicyRule(policyRule);
            }

            proxy.Cirrus.PolicyReports.UpdatePolicyRule(policyRule);
        }

        return policyRule.RuleId;
    }

    /// <summary>
    /// Set mode property for policy rule.
    /// </summary>
    /// <param name="advMode"></param>
    private void SetAdvancedMode(bool advMode)
    {
        if (advMode)
        {
            AdvancedModeHolder1.Visible = true;
            AdvancedModeHolder2.Visible = true;
            SimpleModeHolder.Visible = false;
        }
        else
        {
            AdvancedModeHolder1.Visible = false;
            AdvancedModeHolder2.Visible = false;
            SimpleModeHolder.Visible = true;
        }
    }

    /// <summary>
    /// Set pattern type property for simple mode.
    /// </summary>
    /// <param name="patternType"></param>
    private void SetPatternType(string patternType)
    {
        if (!string.IsNullOrEmpty(patternType) && patternType.Equals(@"Regex"))
        {
            rbRegExExpression.Checked = true;
        }
        else
        {
            rbFindString.Checked = true;
        }
    }

    /// <summary>
    /// Get pattern type property for simple mode.
    /// </summary>
    /// <returns></returns>
    private string GetPatternType()
    {
        if (rbRegExExpression.Checked)
        {
            return @"Regex";
        }

        return @"Like";
    }

    private void SetRemediateScriptType(string scriptType)
    {
        if (string.IsNullOrEmpty(scriptType) || scriptType.Equals(@"CLI"))
        {
            rbCLI.Checked = true;
        }
        else
        {
            rbCCT.Checked = true;
        }
    }

    private string GetRemediateScriptType()
    {
        if (rbCLI.Checked)
        {
            return @"CLI";
        }

        return @"CCT";
    }

    /// <summary>
    /// Set pattern type property for config blocks.
    /// </summary>
    /// <param name="patternType"></param>
    private void SetConfigBlockPatternType(string patternType)
    {
        if (!string.IsNullOrEmpty(patternType) && patternType.Equals(@"Regex"))
        {
            rbCBRegExExpression.Checked = true;
        }
        else
        {
            rbCBFindString.Checked = true;
        }
    }

    /// <summary>
    /// Get pattern type property for config blocks.
    /// </summary>
    /// <returns></returns>
    private string GetConfigBlockPatternType()
    {
        if (rbCBRegExExpression.Checked)
        {
            return @"Regex";
        }

        return @"Like";
    }

    /// <summary>
    /// Set pattern must exist property.
    /// </summary>
    /// <param name="mustExist"></param>
    private void SetPatternMustExist(bool mustExist)
    {
        if (mustExist)
        {
            rbPatternIsNOTFound.Checked = true;
        }
        else
        {
            rbPatternIsFound.Checked = true;
        }
    }

    /// <summary>
    /// Get pattern must exist property.
    /// </summary>
    /// <returns></returns>
    private bool GetPatternMustExist()
    {
        if (rbPatternIsNOTFound.Checked)
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// Set search in config file/block property.
    /// </summary>
    /// <param name="blockStart"></param>
    /// <param name="blockEnd"></param>
    private void SetSearchIn(string blockStart, string blockEnd)
    {
        if (!string.IsNullOrEmpty(blockStart) && !string.IsNullOrEmpty(blockEnd) &&
            blockStart.Length > 0 && blockEnd.Length > 0)
        {
            rbConfigBlock.Checked = true;
            ConfigBlockHolder.Visible = true;

            txtConfigBlockStart.Text = blockStart;
            txtConfigBlockEnd.Text = blockEnd;
        }
        else
        {
            rbEntireConfigFile.Checked = true;
            ConfigBlockHolder.Visible = false;

            txtConfigBlockStart.Text = string.Empty;
            txtConfigBlockEnd.Text = string.Empty;
        }
    }

    /// <summary>
    /// Get search in config file/block property.
    /// </summary>
    private void GetSearchIn(out string blockStart, out string blockEnd)
    {
        if (rbEntireConfigFile.Checked)
        {
            blockStart = string.Empty;
            blockEnd = string.Empty;
        }
        else
        {
            blockStart = txtConfigBlockStart.Text;
            blockEnd = txtConfigBlockEnd.Text;
        }
    }

    /// <summary>
    /// Set alert level property for policy rule.
    /// </summary>
    /// <param name="level"></param>
    private void SetErrorLevel(int level)
    {
        switch (level)
        {
            case 0:
                rbInformational.Checked = true;
                break;
            case 1:
                rbWarning.Checked = true;
                break;
            case 2:
                rbCritical.Checked = true;
                break;
        }
    }

    /// <summary>
    /// Get alert level property for policy rule.
    /// </summary>
    /// <returns></returns>
    private int GetErrorLevel()
    {
        if (rbInformational.Checked)
            return 0;
        if (rbWarning.Checked)
            return 1;
        return 2;
    }

    /// <summary>
    /// Display exception.
    /// </summary>
    /// <param name="ex"></param>
    private void DisplayException(Exception ex)
    {
        MainContent.Visible = false;
        ErrorControl.Visible = true;

        if (!string.IsNullOrEmpty(Page.Request.QueryString[QUERY_RULEID_PARAM]))
        {
            Page.Title = Resources.NCMWebContent.WEBDATA_VM_73;
        }
        else
        {
            Page.Title = Resources.NCMWebContent.WEBDATA_VM_74;
        }
        if (ex.GetType() == typeof(System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>))
        {
            btnSubmit.Visible = false;
            btnTestPolicyRule.Visible = false;

            ErrorControl.VisiblePermissionLink = false;
            ErrorControl.ErrorMessage = (ex as System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>).Detail.Message;
        }
        else
        {
            btnSubmit.Visible = false;
            btnCancel.Visible = false;
            btnTestPolicyRule.Visible = false;

            if (ex.GetType() != typeof(InvalidOperationException))
            {
                ErrorControl.VisiblePermissionLink = false;
            }

            ErrorControl.ErrorMessage = ex.Message;
        }
    }

    /// <summary>
    /// Get all folders for policy rule.
    /// </summary>
    /// <returns></returns>
    private static List<string> GetAllFolders()
    {
        var _isLayer = new ISWrapper();
        using (var proxy = _isLayer.Proxy)
        {
            var dtFolders = proxy.Query(@"SELECT DISTINCT P.Grouping FROM Cirrus.PolicyRules AS P ORDER BY P.Grouping");

            var folders = new List<string>();
            foreach (DataRow row in dtFolders.Rows)
            {
                folders.Add(row["Grouping"].ToString());
            }

            return folders;
        }
    }

    /// <summary>
    /// Validate permissions to edit policy rule.
    /// </summary>
    private void ValidatePermissions()
    {
        if (!SecurityHelper.IsPermissionExist(SecurityHelper._canDownload))
        {
            if (string.IsNullOrEmpty(Page.Request.QueryString[QUERY_RULEID_PARAM]))
            {
                throw new InvalidOperationException(Resources.NCMWebContent.WEBDATA_VM_71);
            }

            throw new InvalidOperationException(Resources.NCMWebContent.WEBDATA_VM_72);
        }
    }

    #endregion

    protected string LearnMore(string fragment)
    {
        var helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
        return helpHelperWrap.GetHelpUrl(fragment);

    }
}
