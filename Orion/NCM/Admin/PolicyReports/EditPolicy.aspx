<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/Admin/NCMAdmin.master" AutoEventWireup="true" CodeFile="EditPolicy.aspx.cs" Inherits="Orion_NCM_Resources_PolicyReports_EditPolicy" Title="<%$ Resources: NCMWebContent, WEBDATA_VY_21%>" %>

<%@ Register Src="~/Orion/NCM/Admin/PolicyReports/Controls/TreeViewer.ascx" TagName="PolicyRulesTree" TagPrefix="ncm" %>
<%@ Register Src="~/Orion/NCM/Admin/PolicyReports/Controls/Folder.ascx" TagName="Folder" TagPrefix="ncm" %>
<%@ Register Src="~/Orion/NCM/Admin/PolicyReports/Controls/NodeSelectionCriteria.ascx" TagName="NodeSelectionCriteria" TagPrefix="ncm" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register Src="~/Orion/NCM/Controls/ErrorControl.ascx" TagPrefix="ncm" TagName="ErrorControl" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="adminPageTitlePlaceHolder">
	<%=Page.Title%>
</asp:Content>

<asp:Content ID ="Content2" runat="server" ContentPlaceHolderID="adminHelpIconPlaceHolder">
    <orion:IconHelpButton ID="btnHelp" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
	<script type="text/javascript" src="../../JavaScript/main.js"></script>
    <link rel="stylesheet" type="text/css" href="../../styles/NCMResources.css" />
    
    <style type="text/css">
        input[id$="txtPolicyName"] {
            padding: 2px;
        }

        .sw-suggestion-content {
            color: black;
            font-weight: normal;
        }

        .sw-suggestion-content a {
            text-decoration: underline;
        }

        .sw-suggestion-content li {
            padding-left: 10px;
        }
    </style>
    
    <ncm:ErrorControl ID="ErrorControl" runat="server" Visible="false" />
    <div style="padding:10px;">
        <div id="MainContent" runat="server" style="border:solid 1px #dcdbd7;width:1100px;padding:10px;">
            <table id="policyHolder" runat="server" width="100%">
                <tr>
                    <td style="padding:10px 20px 5px 0px; width:20%;">
                        <%=Resources.NCMWebContent.WEBDATA_VY_34%>
                    </td>
                    <td style="padding:10px 0px 5px 0px; width:80%;">
                        <asp:TextBox ID="txtPolicyName" runat="server" TextMode="SingleLine" Font-Size="12px" Font-Names="Arial" BorderColor="#89A6C0" BorderWidth="1px" Width="500px" Height="18px" MaxLength="200"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="validatorPolicyName" runat="server" Display="Dynamic" 
                            ControlToValidate="txtPolicyName" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VY_41%>" >
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="padding:5px 20px 5px 0px; width:20%;">
                        <%=Resources.NCMWebContent.WEBDATA_VY_24%>
                    </td>
                    <td style="padding:5px 0px 5px 0px; width:80%;">
                        <asp:TextBox ID="txtDescription" style="resize: vertical;" runat="server" TextMode="MultiLine" Font-Size="12px" Font-Names="Arial" BorderColor="#89A6C0" BorderWidth="1px" Width="500px" Height="50px"></asp:TextBox>
                    </td>
                </tr>
            </table>
            
            <ncm:Folder ID="txtFolder" runat="server" MaxLength="200" />
            
            <table width="100%">
                <tr>
                    <td style="padding:5px 20px 5px 0px; width:20%;" valign="top">
                        <%=Resources.NCMWebContent.WEBDATA_VY_35%>
                    </td>
                    <td style="padding:5px 0px 5px 0px;width:80%;">
                        <ncm:NodeSelectionCriteria ID="nodeSelectionCriteria" runat="server"></ncm:NodeSelectionCriteria>
                        <div id="nodeSelectionErrorControl" runat="server" class="sw-suggestion sw-suggestion-fail">
                            <div class="sw-suggestion-icon"></div>
                            <div class="sw-suggestion-content">
                                The following custom properties defined in node selection criteria does not exist:
                                <ul id="exceptColListControl" runat="server"></ul>
                                Please <a href="/Orion/Admin/CPE/Default.aspx" target="_blank">create these properties</a> or reset node selection criteria to <asp:LinkButton ID="btnResetToAllNodes" runat="server" Text="All nodes" OnClick="btnResetToAllNodes_Click" ></asp:LinkButton>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding:5px 20px 5px 0px; width:20%;">
                        <%=Resources.NCMWebContent.WEBDATA_VY_36%>
                    </td>
                    <td style="padding:5px 0px 5px 0px; width:80%;">
                        <ncm:ConfigTypes ID="ddlConfigType" runat="server" ShowBaseline="true" BaselineItemText="<%$ Resources: NCMWebContent, WEBDATA_VK_103%>" ShowAny="true" Width="200px"></ncm:ConfigTypes>
                    </td>
                </tr>
 		    </table>
            <table width="100%">
                <tr>
                    <td>
                        <asp:UpdatePanel ID="uPanel" UpdateMode="Conditional" runat="server">
                            <ContentTemplate>
                                <table style="background-color:#e4f1f8;">
                                     <tr>
                                        <td colspan="3">
                                            <div id="LinkHolder" runat="server" style="padding:5px 10px 0px 10px;">
                                                <table width="100%" style="background-color:#FFFDCC;">
                                                    <tr>
                                                        <td style="padding:15px 15px 15px 0px;">
                                                            <img style="padding:0px 5px 0px 5px;" src="/Orion/NCM/Resources/images/lightbulb_tip_16x16.gif"/><b><%=Resources.NCMWebContent.WEBDATA_VY_38%></b> &#0187; <a style="color:#336699;" href="/Orion/NCM/Admin/PolicyReports/EditPolicyRule.aspx" ><%=Resources.NCMWebContent.WEBDATA_VY_37%></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:10px 10px 10px 10px;">
                                            <div style="padding-bottom:5px;">
                                                <%=Resources.NCMWebContent.WEBDATA_VY_39%>
                                            </div>
                                            <div>
                                                <ncm:PolicyRulesTree ID="treeAllPolicyRules" NodeImageUrl="/Orion/NCM/Resources/images/PolicyIcons/Policy.Rule.gif" runat="server" />
                                            </div>
                                        </td>
                                        <td style="padding:10px 0px 10px 0px;">
                                            <div>
                                                <orion:LocalizableButton ID="btnAddPolicyRules" runat="server" LocalizedText="customtext"   Text="<%$ Resources: NCMWebContent, WEBDATA_VM_63%>" OnClick="btnAddPolicyRules_Click" CausesValidation="false"/>
                                            </div>
                                            <div>
                                                <orion:LocalizableButton ID="btnRemovePolicyRules" runat="server" LocalizedText="customtext"  Text="<%$ Resources: NCMWebContent, WEBDATA_VM_64%>" OnClick="btnRemovePolicyRules_Click" CausesValidation="false"/>
                                            </div>
                                        </td>
                                        <td style="padding:10px 10px 10px 10px;">
                                            <div style="padding-bottom:5px;">
                                                <%=Resources.NCMWebContent.WEBDATA_VY_40%>
                                            </div>
                                            <div>
                                                <ncm:PolicyRulesTree ID="treeAssignedPolicyRules" NodeImageUrl="/Orion/NCM/Resources/images/PolicyIcons/Policy.Rule.gif" runat="server" Expanded="true" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </div>
        <table width="100%">
            <tr>
                <td style="padding-top:10px;">
                    <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="submit" DisplayType="Primary" OnClick="btnSubmit_Click"  />
                    <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" DisplayType="secondary" OnClick="btnCancel_Click" CausesValidation="false"  />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
