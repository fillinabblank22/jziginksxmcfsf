﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Orion.NPM.Web.UI;

public partial class Orion_NCM_BreadCrumb : UserControl
{
    public string Provider
    {
        get { return ctrSiteMapPath.Provider; }
        set { ctrSiteMapPath.Provider = value; }
    }

    protected void OnSiteMapPath_OnInit(object sender, EventArgs e)
    {
        if (!CommonWebHelper.IsBreadcrumbsDisabled)
        {
            string viewID = Request[@"ViewID"];
            string returnTo = Request[@"ReturnTo"];
            string accountID = Request[@"AccountID"];
            var renderer = new AdminSiteMapRenderer();
            renderer.SetUpData(new KeyValuePair<string, string>(@"ViewID", viewID),
                new KeyValuePair<string, string>(@"ReturnTo", returnTo),
                new KeyValuePair<string, string>(@"AccountID", accountID));
            ctrSiteMapPath.SetUpRenderer(renderer);
        }
    }
}
