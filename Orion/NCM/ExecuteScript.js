﻿Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.QuickTips.init();

SW.NCM.ExecuteScript = function () {

    var scriptName;

    var reSizeTextArea = function () {
        window.setTimeout(function () {
            setHeight();
            setWidth();
        });
    };

    var setWidth = function () {
        $("#scriptBox").width(1);
        var w = $("#scriptPanel").width() - 4;
        $("#scriptBox").width(w);
    };

    var setHeight = function () {
        var textArea = $("#scriptBox");
        var groupItems = $(".GroupItems");

        var maxHeight = getHeight(textArea);
        textArea.height(maxHeight);
        groupItems.height(maxHeight + 6);
    };

    var getHeight = function (gridPanel) {
        var gridTop = gridPanel.offset().top;
        return $(window).height() - gridTop - 150;
    };

    DoNCMTransfer = function() {
        var script = $("#scriptBox").val();
        if (script.length == 0) {
            Ext.Msg.show({
                title: $("#chkConfigMode").prop("checked")
                    ? '@{R=NCM.Strings;K=WEBJS_IA_14;E=js}'
                    : '@{R=NCM.Strings;K=WEBJS_VK_293;E=js}',
                msg: '@{R=NCM.Strings;K=WEBJS_VK_298;E=js}',
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.ERROR
            });
        } else {
            var saveScript = $('#chkSaveScript').is(':checked');
            if (saveScript) {
                if (!scriptName.validate()) {
                    return;
                }
            }

            if ($("#chkConfigMode").prop("checked")) {
                if (SW.NCM.ShowApprovalRequestButton) {
                    var reboot = $('#chkRebootDevice').is(':checked');
                    var guid = newGuid();
                    if (saveScript) internalSaveSnippet(scriptName.getValue(), script, '');
                    internalSendRequestForApproval(SW.NCM.NcmNodeIds,
                        getConfigType(),
                        reboot,
                        SW.NCM.RequestTypeUploadConfig,
                        script,
                        guid);
                } else {
                    internalGetNCMNodeIdsForTransfer(SW.NCM.NcmNodeIds,
                        function(results) {
                            var ncmNodeIdsForTransfer = results.ncmNodeIdsForTransfer;
                            var error = results.error;
                            var reboot = $('#chkRebootDevice').is(':checked');
                            if (results != null && results.Error) {
                                errorHandler('@{R=NCM.Strings;K=WEBJS_IA_14;E=js}',
                                    error,
                                    function() {
                                        if (ncmNodeIdsForTransfer.length > 0) {
                                            differentTransferTypes(ncmNodeIdsForTransfer, function(isDifferentTransferTypes) {
                                                var msg = String.format('@{R=NCM.Strings;K=WEBJS_IA_17;E=js}',
                                                    ncmNodeIdsForTransfer.length);

                                                if (isDifferentTransferTypes) {
                                                    msg += '</br></br>&bull;&nbsp;' +
                                                        '@{R=NCM.Strings;K=WEBJS_VK_311;E=js}';
                                                }

                                                Ext.Msg.confirm('@{R=NCM.Strings;K=WEBJS_IA_14;E=js}',
                                                    msg,
                                                    function (btn, text) {
                                                        if (btn == 'yes') {
                                                            if (saveScript) {
                                                                internalSaveSnippet(scriptName.getValue(), script, '');
                                                            }
                                                            internalUploadSnippet(ncmNodeIdsForTransfer,
                                                                getConfigType(),
                                                                script,
                                                                reboot);
                                                        }
                                                    });
                                            });
                                        }
                                    }
                                );
                            } else {
                                if (ncmNodeIdsForTransfer.length > 0)

                                    differentTransferTypes(ncmNodeIdsForTransfer, function (isDifferentTransferTypes) {
                                    var msg = String.format('@{R=NCM.Strings;K=WEBJS_IA_17;E=js}',
                                        ncmNodeIdsForTransfer.length);

                                    if (isDifferentTransferTypes) {
                                        msg += '</br></br>&bull;&nbsp;' + '@{R=NCM.Strings;K=WEBJS_VK_311;E=js}';
                                    }

                                    Ext.Msg.confirm('@{R=NCM.Strings;K=WEBJS_IA_14;E=js}',
                                        msg,
                                        function (btn, text) {
                                            if (btn == 'yes') {
                                                if (saveScript) {
                                                    internalSaveSnippet(scriptName.getValue(), script, '');
                                                }
                                                internalUploadSnippet(ncmNodeIdsForTransfer,
                                                    getConfigType(),
                                                    script,
                                                    reboot);
                                            }
                                        });
                                });
                            }
                        });
                }
            } else {
                if (SW.NCM.ShowApprovalRequestButton) {
                    var guid = newGuid();
                    if (saveScript) internalSaveSnippet(scriptName.getValue(), script, '');
                    internalSendRequestForApproval(SW.NCM.NcmNodeIds,
                        '',
                        false,
                        SW.NCM.RequestTypeExecuteScript,
                        script,
                        guid);
                } else {
                    internalGetNCMNodeIdsForTransfer(SW.NCM.NcmNodeIds,
                        function(results) {
                            var ncmNodeIdsForTransfer = results.ncmNodeIdsForTransfer;
                            var result = results.error;
                            if (result != null && result.Error) {
                                errorHandler('@{R=NCM.Strings;K=WEBJS_VK_293;E=js}',
                                    result,
                                    function() {
                                        if (ncmNodeIdsForTransfer.length > 0) {
                                            var msg = String.format('@{R=NCM.Strings;K=WEBJS_VK_310;E=js}',
                                                ncmNodeIdsForTransfer.length);

                                            Ext.Msg.confirm('@{R=NCM.Strings;K=WEBJS_VK_293;E=js}',
                                                msg,
                                                function(btn, text) {
                                                    if (btn == 'yes') {
                                                        if (saveScript)
                                                            internalSaveSnippet(scriptName.getValue(), script, '');
                                                        internalExecuteScript(ncmNodeIdsForTransfer, script);
                                                    }
                                                });
                                        }
                                    });
                            } else {
                                var msg = String.format('@{R=NCM.Strings;K=WEBJS_VK_310;E=js}',
                                    ncmNodeIdsForTransfer.length);

                                Ext.Msg.confirm('@{R=NCM.Strings;K=WEBJS_VK_293;E=js}',
                                    msg,
                                    function(btn, text) {
                                        if (btn == 'yes') {
                                            if (saveScript) internalSaveSnippet(scriptName.getValue(), script, '');
                                            internalExecuteScript(ncmNodeIdsForTransfer, script);
                                        }
                                    });
                            }
                        });
                }
            }
        }
    };

    saveScriptClick = function(checked) {
        if (checked) {
            $("#saveScriptPanel").show();
        } else {
            $("#saveScriptPanel").hide();
        }
    };

    configModeClick = function(checked) {
        if (checked) {
            $("#uploadAdvPanel").show();
            if (!SW.NCM.ShowApprovalRequestButton) $("#btn>span>span").text('@{R=NCM.Strings;K=WEBJS_VK_270;E=js}');
        } else {
            $("#uploadAdvPanel").hide();
            if (!SW.NCM.ShowApprovalRequestButton) $("#btn>span>span").text('@{R=NCM.Strings;K=WEBJS_VK_292;E=js}');
        }
    };

    rebootDeviceClick = function(that) {
        if (that.checked) {
            Ext.Msg.show({
                title: '@{R=NCM.Strings;K=WEBJS_VK_304;E=js}',
                msg: '@{R=NCM.Strings;K=WEBJS_VK_306;E=js}',
                minWidth: 500,
                buttons: {
                    ok: '@{R=NCM.Strings;K=WEBJS_VK_308;E=js}',
                    cancel: '@{R=NCM.Strings;K=WEBJS_VK_38;E=js}',
                },
                icon: Ext.MessageBox.WARNING,
                fn: function(btn) {
                    if (btn == "cancel") {
                        that.checked = false;
                    }
                }
            });
        }
    };

    var internalSaveSnippet = function(title, config, comments) {
        SW.NCM.callWebMethod('/Orion/NCM/Services/SnippetManagement.asmx/AddNewSnippet',
            { configTitle: title, config: config, comments: comments },
            function() {});
    };

    var internalGetNCMNodeIdsForTransfer = function(ncmNodeIds, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
            "GetNCMNodeIdsForTransferFromList",
            { ncmNodeIds: ncmNodeIds },
            function(result) {
                onSuccess(result);
            }
        );
    };

    var internalExecuteScript = function(ncmNodeIds, script) {
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_242;E=js}');
        ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
            "ExecuteScript",
            { nodeIds: ncmNodeIds, script: script },
            function(result) {
                waitMsg.hide();
                errorHandler('@{R=NCM.Strings;K=WEBJS_VK_243;E=js}', result);
                if (result == null) {
                    window.location = "/Orion/NCM/CloseWindowHandler.aspx?reloadGridStore=yes";
                }
            }
        );
    };

    var internalUploadSnippet = function(nodeIds, configType, config, reboot) {
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_IA_07;E=js}');
        ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
            "UploadConfig",
            { nodeIds: nodeIds, configType: configType, config: config, reboot: reboot, configId: null },
            function(result) {
                waitMsg.hide();
                errorHandler('@{R=NCM.Strings;K=WEBJS_IA_19;E=js}', result);
                if (result == null) {
                    window.location = "/Orion/NCM/CloseWindowHandler.aspx?reloadGridStore=yes";
                }
            }
        );
    };

    var internalSendRequestForApproval = function(nodeIds, configType, reboot, requestType, config, guid) {
        var waitMsg = Ext.Msg.wait('@{R=NCM.Strings;K=WEBJS_VK_244;E=js}');
        ORION.callWebService("/Orion/NCM/Services/ConfigManagement.asmx",
            "SendRequestForApproval",
            {
                nodeIds: nodeIds,
                configType: configType,
                reboot: reboot,
                requestType: requestType,
                script: config,
                guidId: guid,
                configId: null
            },
            function(result) {
                waitMsg.hide();
                errorHandler('@{R=NCM.Strings;K=WEBJS_VK_245;E=js}', result);
                if (result == null) {
                    window.parent.frames.iframeContainer.location.href = String.format(
                        '/Orion/NCM/Admin/ConfigChangeApproval/ViewApprovalRequest.aspx?printable=true&GuidID={0}&NCMReturnURL={1}',
                        guid,
                        SW.NCM.CloseWindowHandlerUrl);
                }
            }
        );
    };

    var differentTransferTypes = function(ncmNodeIds, callback) {
        if (ncmNodeIds.length === 1) {
            callback(false);
        } else {
            SW.NCM.callWebMethod('/Orion/NCM/Services/ConfigManagement.asmx/IsDifferentTransferTypes',
                { ncmNodeIds: ncmNodeIds },
                function (result) {
                    callback(result.d);
                });
        }
    };

    var getConfigType = function() {
        return "Running";
    };

    var errorHandler = function(source, result, func) {
        if (result != null && result.Error) {
            Ext.Msg.show({
                title: source,
                msg: result.Msg,
                minWidth: 500,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR,
                fn: func
            });
        }
    };

    var selectSnippet = function() {
        $(this).parent().addClass("SelectedItem").siblings().removeClass("SelectedItem");

        var value = $(".SelectedItem a").attr('value');
        value = Ext.util.Format.htmlDecode(value);
        getSnippetConfig(value, function (snippetConfig) {
            $("#scriptBox").val(snippetConfig);
        });
    };

    var getSnippetConfig = function(snippetId, callback) {
        SW.NCM.callWebMethod('/Orion/NCM/Services/SnippetManagement.asmx/GetSnippetConfig',
            { snippetId: snippetId },
            function(result) {
                if (result != null) {
                    callback(result.d);
                }
            });
    };

    var initSnippetList = function() {
        var groupItems = $(".GroupItems").text('@{R=NCM.Strings;K=WEBJS_VM_29;E=js}');
        getSnippetsByValues("tags",
            function(result) {
                groupItems.empty();
                $(result.Rows).each(function() {
                    var value = Ext.util.Format.htmlEncode(this.ConfigID);
                    var text = $.trim(String(this.ConfigTitle));
                    $("<a href='#'></a>")
                        .draggable({ helper: "clone" })
                        .attr('value', value).attr('title', value)
                        .text(text).click(selectSnippet).appendTo(".GroupItems").wrap("<li class='GroupItem'/>");
                });
            });
    };

    var getSnippetsByValues = function(groupBy, onSuccess) {
        ORION.callWebService("/Orion/NCM/Services/SnippetManagement.asmx",
            "GetSnippetsList",
            {},
            function(result) {
                onSuccess(ORION.objectFromDataTable(result));
            });
    };

    var newGuid = function () {
        try {
            return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, function (c) {
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            });
        } catch (e) {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        }
    };

    return {
        init: function() {

            initSnippetList();

            var w = $("#fileUploadTable").width();
            $("[id$='_btnFromFile']").width(w);

            if (SW.NCM.ShowApprovalRequestButton) {
                $("#btn>span>span").text('@{R=NCM.Strings;K=WEBJS_VK_291;E=js}');
            } else {
                $("#btn>span>span").text('@{R=NCM.Strings;K=WEBJS_VK_292;E=js}');
            }

            $('#chkConfigMode').attr('checked', false);
            $('#chkRebootDevice').attr('checked', false);
            configModeClick(false);

            scriptName = new Ext.form.TextField({
                renderTo: 'scriptName',
                emptyText: '@{R=NCM.Strings;K=WEBJS_IA_11;E=js}',
                disabled: false,
                allowBlank: false,
                width: 200
            });

            reSizeTextArea();

            $(window).resize(function() {
                reSizeTextArea();
            });

            $('#chkSaveScript').attr('checked', false);
            saveScriptClick(false);

            $("#scriptBox").droppable({
                drop: function(event, ui) {
                    $(this).val($(this).val().concat(getSnippetConfig(ui.draggable.attr("value"))));
                    ui.draggable.parent().addClass("SelectedItem").siblings().removeClass("SelectedItem");
                }
            });
        }
    };

} ();
Ext.onReady(SW.NCM.ExecuteScript.init, SW.NCM.ExecuteScript);

SW.NCM.callWebMethod = function (url, data, success, failure) {
    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: true,
        success: function (result) {
            if (success)
                success(result);
        },
        error: function (response) {
            if (failure)
                failure(response);
            else {
                alert(response);
            }
        }
    });
};
