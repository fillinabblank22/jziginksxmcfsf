using System;
using System.Web.UI;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_LicenseError : Page
{
    private readonly ICommonHelper commonHelper;

    public Orion_NCM_LicenseError()
    {
        commonHelper = ServiceLocator.Container.Resolve<ICommonHelper>();
    }

    protected string ActualNodes => commonHelper.GetActualNodesCount().ToString();

    protected string LicenseNodes => commonHelper.GetLicenseNodesLimit().ToString();

    public string OverLicenseNodes
    {
        get
        {
            long overLicenseNodes = commonHelper.GetActualNodesCount() - commonHelper.GetLicenseNodesLimit();
            return overLicenseNodes.ToString();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CommonHelper.ReInitialise();
    }

    protected void btnOKDelete_Click(object sender, EventArgs e)
    {
        var returnUrl = !string.IsNullOrEmpty(Page.Request[@"NCMReturnUrl"])
            ? Page.Request[@"NCMReturnUrl"]
            : @"/Orion/NCM/Summary.aspx";
        commonHelper.DeleteUnlicensedNodes();

        Page.Response.Redirect(returnUrl);
    }
}
