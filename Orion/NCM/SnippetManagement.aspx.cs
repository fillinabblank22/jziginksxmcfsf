﻿using System;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_SnippetManagment : System.Web.UI.Page
{
    #region Page Life Cycle

    protected override void OnInit(EventArgs e)
    {
        ISWrapper ISLayer = new SolarWinds.NCMModule.Web.Resources.ISWrapper();

        if (!SecurityHelper.IsPermissionExist(SecurityHelper._canUpload) && !CommonHelper.IsDemoMode())
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_IC_173}");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_IA_22;

        SWISValidator validator = new SWISValidator();
        validator.ContentContainer = ContentContainer;
        validator.RedirectIfValidationFailed = true;
        divError.Controls.Add(validator);
    }

    #endregion
}