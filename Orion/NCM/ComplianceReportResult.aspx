<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="ComplianceReportResult.aspx.cs" Inherits="Orion_NCM_ComplianceReportResult" Title="Compliance Report Result" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register TagPrefix="ncm" Src="~/Orion/NCM/Controls/IconExportToPdf.ascx" TagName="IconExportToPdf" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" runat="server">
    <ncm:DropDownMapPath ID="NodeSiteMapPath" Provider="NCMSitemapProvider" runat="server"  OnInit="SiteMapPath_OnInit">
                    <RootNodeTemplate>
                        <a href="<%# Eval(@"url") %>" ><u> <%# Eval(@"title") %></u> </a>
                    </RootNodeTemplate>
                    <CurrentNodeTemplate>
				        <a href="<%# Eval(@"url") %>" ><%# Eval(@"title") %> </a>
				    </CurrentNodeTemplate>
   </ncm:DropDownMapPath>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmPageTitlePlaceHolder" Runat="Server">
    <%=ReportTitle%>
</asp:Content>

<asp:Content ID="Content9" ContentPlaceHolderID="ncmHelpIconPlaceHolder" runat="server">
    <a href='/Orion/NCM/Admin/PolicyReports/EditPolicyReport.aspx?ReportID=<%=PolicyReportID%>' class='mngPolicyReportLink'><%=Resources.NCMWebContent.WEBDATA_VM_83%></a> 
    <orion:IconHelpButton ID="helpBtn" runat="server" HelpUrlFragment="OrionNCMWebPolicyViolations" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmHeadPlaceHolder" Runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <link rel="stylesheet" type="text/css" href="JavaScript/Ext_UX/ColumnHeaderGroup.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
    
    <script type="text/javascript">
        var IsDemoMode = <%=SolarWinds.NCMModule.Web.Resources.CommonHelper.IsDemoMode().ToString().ToLowerInvariant() %>;
        var LimitationID = <%=this.ViewInfo.LimitationID %>;
    </script>

    <script type="text/javascript" src="JavaScript/Ext_UX/ColumnHeaderGroup.js"></script>
    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippetsHelper.js"></script>
    <script type="text/javascript" src="/Orion/NCM/ComplianceReportResultView.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Controls/ViolationDetailsTree.js"></script> 

    <style type="text/css">
        .x-grid3-hd-inner 
        {
            white-space : normal;
        }

        .x-tree-node-icon.ncm_policy_icon
        {
            background-image:url('/Orion/NCM/Resources/images/PolicyIcons/Policy.Icon.gif');
        }

        .x-tree-node-icon.ncm_rule_icon
        {
            background-image:url('/Orion/NCM/Resources/images/PolicyIcons/Policy.Rule.gif');
        }

        .mngPolicyReportLink
        {
            background-image:url("/Orion/NCM/Resources/images/PolicyIcons/manage_policy_reports.gif");
            background-position:left center;
            background-repeat:no-repeat;
            padding:2px 0 2px 20px;
            text-align:right;
        }
        
        a.x-menu-item
        {   
            padding: 3px 21px 3px 27px !important;
        }
        
        .x-menu-item-active 
        {
            background-repeat: repeat-x;
            background-position: left bottom;
            border-style:solid;
            border-width: 1px 0;
            margin:0 1px;
	        padding: 0;
        }
    </style>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
    <%foreach (string setting in new string[] { @"PageSize" }) { %>
		<input type="hidden" name="NCM_ComplianceReportResultViewer_<%=setting%>" id="NCM_ComplianceReportResultViewer_<%=setting%>" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get($@"NCM_ComplianceReportResultViewer_{setting}")%>' />		
	<%}%>

    <link rel="stylesheet" type="text/css" href="styles/NCMResources.css" />

    <div style="width:100%;">
        <div style="padding:10px;">
            <div style="padding-bottom:5px;font-size: 11px;"> <%=LastUpdate%></div>
            <table id="MainContent" runat="server" cellpadding="0" cellspacing="0" width="100%" style="border:solid 1px #dcdbd7;padding:10px;background-color:white;">
                <tr>
                    <td style="padding-bottom:10px;display:none" id="ReportSummaryContainer" visible="false" runat="server">
                        <div style="padding-bottom:5px;" ><%=Resources.NCMWebContent.WEBDATA_VM_77%></div>
                        <div>
                            <div id="Tree"></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="display:none;" id="ReportDetailsContainer" runat="server">
                        <div style="padding-bottom:5px;"><%=Resources.NCMWebContent.WEBDATA_VM_78%></div>
                        <div id="gridCell">
                            <div id="Grid"></div>
                        </div>
                    </td>
                </tr>
                <tr id="infoContainer" runat="server" style="display:none">
                    <td><%=this.InfoMsg%></td>
                </tr>
            </table>
        </div>
    </div>
    <div id="nodesDialogContainer" class="x-hidden" style="overflow:hidden;">
        <div id="treecontrolpageFrameContainer" style="overflow:hidden;">
             <iframe name="treeControlIframe" src="" id="treeControlIframe" class="x-panel-body" style="overflow: hidden; padding-right:5px;" width="99%"; height="450px";></iframe>
        </div> 
    </div>
</asp:Content>

<asp:Content ID="ExportToPdfContent" ContentPlaceHolderID="ncmExportToPDFPlaceHolder" runat="server">
    <ncm:IconExportToPdf ID="IconExportToPdf1" runat="server" />
</asp:Content> 