<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="SearchForEndHostResult.aspx.cs" Inherits="SearchForEndHostResult" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register Src="~/Orion/NCM/CustomPageLinkHidding.ascx" TagPrefix="ncm" TagName="custPageLinkHidding" %>  
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="ncm1" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmPageTitlePlaceHolder" runat="server">
	<%=Page.Title%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmHelpIconPlaceHolder" runat="server">
    <orion:IconHelpButton ID="helpBtn" runat="server" HelpUrlFragment="OrionNCMWebSearchForEndHostResult" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" runat="server">
    <ncm1:DropDownMapPath ID="NodeSiteMapPath" Provider="NCMSitemapProvider" runat="server"  OnInit="SiteMapPath_OnInit">
        <RootNodeTemplate>
            <a href="<%# Eval(@"url") %>" ><u> <%# Eval(@"displayTitle") %></u> </a>
        </RootNodeTemplate>
        <CurrentNodeTemplate>
	        <a href="<%# Eval(@"url") %>" ><%# Eval(@"displayTitle") %> </a>
	    </CurrentNodeTemplate>
   </ncm1:DropDownMapPath>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmMainContentPlaceHolder" runat="server">
    <ncm:custPageLinkHidding  runat="server" /> 
    <link rel="stylesheet" type="text/css" href="styles/NCMResources.css" />
    <script type="text/javascript" src="JavaScript/main.js?v=1.0"></script>
    
	<div style="padding:0px 10px 0px 10px;">
        <table width="100%">
            <tr>                    
                <td valign="top" style="width:100%">
                    <div style="padding:0px 0px 5px 0px;">
                        <asp:Label ID="AsOfLastDiscovery" runat="server" Font-Size="8pt"></asp:Label>
                    </div>
                    <asp:DataList runat="server" ID="table"></asp:DataList>
                    <asp:Label ID="errorLabel" runat="server" ForeColor="Red" Font-Size="8pt" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>                    
                <td valign="top" style="width:100%; padding-top:15px;">
                    <asp:DataList runat="server" ID="tableWireless"></asp:DataList>
                    <asp:Label ID="errorLabelWireless" runat="server" ForeColor="Red" Font-Size="8pt" Visible="false"></asp:Label>
                </td>
            </tr>
        </table>
        <orion:ResourceHostControl ID="resHostControl" runat="server">
            <orion:ResourceContainer runat="server" ID="resContainer" />		
        </orion:ResourceHostControl>
	</div>
</asp:Content>
