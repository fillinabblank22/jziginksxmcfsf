<%@ Page Language="C#"  MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="ConfigDetails.aspx.cs" Inherits="Orion_NCM_ConfigDetails" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="orion" TagName="Refresher" Src="~/Orion/Controls/Refresher.ascx" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register TagPrefix="ncm" Src="~/Orion/NCM/Controls/IconNCMSettings.ascx" TagName="IconNCMSettings" %>
<%@ Register TagPrefix="ncm" Src="~/Orion/NCM/Controls/IconCustomizePage.ascx" TagName="IconCustomizePage" %>
<%@ Register TagPrefix="ncm1" Namespace="SolarWinds.NCMModule.Web.Resources" Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register TagPrefix="ncm" Src="~/Orion/NCM/Controls/IconExportToPdf.ascx" TagName="IconExportToPdf" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ncmPageTitlePlaceHolder">
	<%=PageTitle %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" runat="server">
    <ncm:DropDownMapPath ID="NodeSiteMapPath" Provider="NCMSitemapProvider" runat="server"  OnInit="SiteMapPath_OnInit">
        <RootNodeTemplate>
            <a href="<%# Eval(@"url") %>" ><u> <%# Eval(@"title") %></u> </a>
        </RootNodeTemplate>
        <CurrentNodeTemplate>
	        <a href="<%# Eval(@"url") %>" ><%# Eval(@"title") %> </a>
	    </CurrentNodeTemplate>
   </ncm:DropDownMapPath>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmMainContentPlaceHolder" runat="server">
	<link rel="stylesheet" type="text/css" href="styles/NCMResources.css" />
	<script type="text/javascript" src="JavaScript/main.js?v=1.0"></script>
	
	<orion:Refresher ID="Refresher" runat="server" />
	
    <asp:Panel ID="ErrorPannel" runat="server">
	    <table>
	        <tr>
	            <td style="padding:10px;"><ncm1:ExceptionWarning ID="exMsg" runat="server" ></ncm1:ExceptionWarning></td>
	        </tr>
	    </table>
    </asp:Panel>
    
	<asp:Panel ID="resourcePanel" runat="server">
	    <div id="LinkHolder" runat="server" style="padding:0px 10px 3px 10px;">
            <table width="100%" style="background-color:#FFFDCC;">
                <tr>
                    <td style="padding:5px 0px 5px 0px;">
                        <img style="padding:0px 5px 0px 5px;" src="/Orion/NCM/Resources/images/lightbulb_tip_16x16.gif"/>Return to <a style="color:#336699;" href="/Orion/NCM/ComplianceReportResult.aspx?ReportID=<%=ReportId%>" ><%=ReportName%></a>
                    </td>
                </tr>
            </table>
        </div>
	    <div id="ncmResourceWrapper">
            <orion:ResourceHostControl ID="ResourceHostControl2" runat="server">
	            <orion:ResourceContainer runat="server" ID="resContainer" />
            </orion:ResourceHostControl>
	    </div>
	</asp:Panel>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmCustomizePagePlaceHolder" runat="server">
    <ncm:IconCustomizePage ID="IconCustomizePage1" runat="server" />
    <ncm:IconNCMSettings ID="IconNCMSettings1" runat="server"/>
</asp:Content>

<asp:Content ID="ExportToPdfContent" ContentPlaceHolderID="ncmExportToPDFPlaceHolder" runat="server">
    <ncm:IconExportToPdf ID="IconExportToPdf1" runat="server" />
</asp:Content> 