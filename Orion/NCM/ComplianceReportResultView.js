﻿Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.QuickTips.init();

SW.NCM.ComplianceReportResultViewer = function () {

    ORION.prefix = "NCM_ComplianceReportResultViewer_";
    var pageSizeBox;
    var pageSize;

    function gridResize() {
        if (grid != null) {
            grid.setWidth(1);
            grid.setWidth($('#gridCell').width());

            grid.setHeight(getGridHeight());
        }
    }

    function getGridHeight() {
        var top = $('#gridCell').offset().top;
        return $(window).height() - top - $('#footer').height() - 100;
    }

    //ext grid column and row renders
    function renderNodeNameField(value, meta, record) {
        var nodeId = record.data.NodeID;
        var isBinary = record.data.IsBinary;
        var res = NodeNameParser(nodeId, value, isBinary);
        return res;
    }

    function renderRuleField(value, meta, record) {
        if (value == null)
            return "<img src=\"/Orion/NCM/Resources/images/PolicyIcons/Policy.Passed.gif\" />";;

        var items = value.split("|");
        if (items.length < 3)
            return '';

        var errorLevel = items[0],
            policyId = items[1],
            ruleId = items[2];
        nodeId = record.data.NodeID;

        var iconUrl = ErrorLevelParser(errorLevel, nodeId, policyId, ruleId);
        return iconUrl;
    }

    function NodeNameParser(nodeId, nodeName, isBinary) {
        var configIcon = isBinary ? "Config-Binary.png" : "Config-Text.png";
        var id = nodeId.replace('{', '').replace('}', '');
        var res = String.format('<img src="/Orion/NCM/Resources/images/ConfigIcons/{0}" />&nbsp;<a href="/Orion/NCM/NodeDetails.aspx?NodeID={1}">{2}</a>', configIcon, id, nodeName);
        return res;
    }

    function ErrorLevelParser(value, nodeid, policyid, ruleid) {
        var icon = "<img onclick=\"return TreeViewer.imageClick('{0}', '{1}', '{2}', '{3}', {4})\" src=\"/Orion/NCM/Resources/images/PolicyIcons/Policy.{5}.gif\" style=\"cursor: pointer; cursor: hand;\"/>";
        switch (value.toUpperCase()) {
            case "C":
                return String.format(icon, nodeid, policyid, ruleid, _reportId, LimitationID, "Critical");
            case "W":
                return String.format(icon, nodeid, policyid, ruleid, _reportId, LimitationID, "Warning");
            case "I":
                return String.format(icon, nodeid, policyid, ruleid, _reportId, LimitationID, "Info");
            default:
                return '';
        }
    }

    var loadMask;
    var grid;
    var _reportId;

    var refreshObjects = function (reportId) {
        grid.store.removeAll();
        grid.store.proxy.conn.jsonData = { reportId: reportId };
        grid.store.load();
    };

    var displayReportResult = function () {
        ORION.callWebService("/Orion/NCM/Services/PolicyReportResult.asmx",
                 "GenerateColumnModel", { reportId: _reportId },
                 function (result) {
                     loadMask.hide();
                     var data;
                     try {
                         data = Ext.util.JSON.decode(result);
                     } catch (ex) {
                         data = null;
                     }

                     if (data == null || data == "") {
                         $("[id$='_ReportDetailsContainer']").hide();
                         $("[id$='_ReportSummaryContainer']").hide();
                         $("[id$='_infoContainer']").show();
                     } else {
                         $("[id$='_ReportDetailsContainer']").show();

                         if ($("[id$='_ReportSummaryContainer']").length != 0) {
                             $("[id$='_ReportSummaryContainer']").show();
                             loadReportSummary(data);
                         }

                         loadReportResult(data);
                     }
                 });
    };


    var generateConfig = function (structure) {
        var res = [];
        res.columns = [];
        res.policyGroupRow = [];
        res.fields = [];
        var mappingIndex = 5; // 5 columns reserved for our needs

        res.columns.push({ header: 'NodeID', width: 80, hidden: true, hideable: false, dataIndex: 'NodeID' });
        res.fields.push({ name: 'NodeID', mapping: 0 });

        res.columns.push({ header: '@{R=NCM.Strings;K=WEBJS_VK_88;E=js}', width: 200, hidden: false, hideable: false, dataIndex: 'NodeName', sortable: true, renderer: renderNodeNameField });
        res.fields.push({ name: 'NodeName', mapping: 1 });

        res.columns.push({ header: '@{R=NCM.Strings;K=WEBJS_VK_89;E=js}', width: 120, hidden: false, hideable: false, dataIndex: 'NodeIpAddress', sortable: true });
        res.fields.push({ name: 'NodeIpAddress', mapping: 2 });

        res.columns.push({ header: '@{R=NCM.Strings;K=ComplianceReportResult_ConfigType_ColumnHeader;E=js}', width: 80, hidden: true, hideable: false, dataIndex: 'ConfigType' });
        res.fields.push({ name: 'ConfigType', mapping: 3 });

        res.columns.push({ header: '@{R=NCM.Strings;K=ComplianceReportResult_ConfigTitle_ColumnHeader;E=js}', width: 80, hidden: true, hideable: false, dataIndex: 'ConfigTitle' });
        res.fields.push({ name: 'ConfigTitle', mapping: 4 });

        res.policyGroupRow.push({ header: '', align: 'center', colspan: 5 });

        Ext.iterate(structure, function (item) {
            res.policyGroupRow.push({
                header: '<img src=\"/Orion/NCM/Resources/images/PolicyIcons/Policy.Icon.gif\"/>&nbsp;' + item.Policy,
                align: 'center',
                colspan: item.Rules.length
            });

            Ext.each(item.Rules, function (rule) {
                res.fields.push({
                    name: rule.Name,
                    mapping: mappingIndex++
                });
                res.columns.push({
                    dataIndex: rule.Name,
                    header: '<img src=\"/Orion/NCM/Resources/images/PolicyIcons/Policy.Rule.gif\"/>&nbsp;' + rule.DisplayName,
                    align: 'center',
                    width: 120,
                    sortable: true,
                    renderer: renderRuleField
                });
            });
        });

        return res;
    };

    var ExportTo = function (type) {
        Ext.Msg.confirm(type == 'xls' ? '@{R=NCM.Strings;K=WEBJS_VK_107;E=js}' : '@{R=NCM.Strings;K=WEBJS_VK_108;E=js}', '@{R=NCM.Strings;K=ComplianceReportResultView_ExportAlert;E=js}', function (btn, text) {
            if (btn == 'yes') {
                $.download('/Orion/NCM/ComplianceReportExporter.ashx', 'objid=' + _reportId + '&action=' + type + '&includeViolationDetails=true' + '&limitationId=' + LimitationID);
            }
            else {
                $.download('/Orion/NCM/ComplianceReportExporter.ashx', 'objid=' + _reportId + '&action=' + type + '&limitationId=' + LimitationID);
            }
        });
    }

    function createTreeNodes(items) {
        var rootNode = new Ext.tree.TreeNode({ text: 'root ' });

        Ext.iterate(items, function (item) {
            var l1 = new Ext.tree.TreeNode({ text: item.Policy, iconCls: 'ncm_policy_icon' });
            rootNode.appendChild(l1);
            Ext.each(item.Rules, function (rule) {
                var l2 = new Ext.tree.TreeNode({ text: rule.DisplayName, iconCls: 'ncm_rule_icon' });
                l1.appendChild(l2);
            });
        });

        return rootNode;
    }

    var loadReportSummary = function (res) {
        var nodes = createTreeNodes(res);

        var tree = new Ext.tree.TreePanel({
            renderTo: 'Tree',
            useArrows: false,
            autoScroll: true,
            animate: true,
            enableDD: false,
            containerScroll: true,
            rootVisible: false,
            width: 500,
            border: false,
            loader: new Ext.tree.TreeLoader(),
            root: nodes
        });

        tree.getRootNode().expand();
    }

    var doReportSelection = false;
    var openToVal = null;

    NavToPage = function (component) {

        openToVal = Ext.getUrlParam('NodeID');

        if (openToVal == null)
            return;


        var params = {};
        params.dir = grid.store.sortInfo.direction;
        params.sort = grid.store.sortInfo.field;
        params.limit = grid.getBottomToolbar().pageSize;
        params.nodeId = openToVal;
        params.reportId = _reportId;

        var strPrms = Ext.encode(params);

        var start = 0;
        $.ajax({
            async: false,
            type: "POST",
            url: "/Orion/NCM/Services/PolicyReportResult.asmx/FindNodeInGrid",
            contentType: "application/json; charset=utf-8",
            data: strPrms,
            dataType: "json",
            success: function (res) {

                var obj = Ext.util.JSON.decode(res.d);
                if (obj.success == true) {
                    doReportSelection = true;
                    start = obj.data.start;
                } else {
                    start = 0;
                    doReportSelection = false;
                }

            },
            error: function (xhr, status, errorThrown) {
                doReportSelection = false;
                start = 0;
            }
        });

        if (start > 0) {

            grid.store.baseParams['start'] = start;
            grid.getBottomToolbar().cursor = start / pageSize;

        }
    }
    function gridStoreLoadHandler(o) {
        selectRowInGrid();
    }

    function selectRowInGrid() {

        if (doReportSelection == true) {
            grid.store.baseParams = {};
            doReportSelection = false;
            if (openToVal != null) {
                var nm = getNodeIndex(openToVal);
                if (nm != null && nm >= 0) {
                    grid.getView().focusRow(nm);
                    grid.getSelectionModel().selectRow(nm);
                }
            }
        }
    }

    function getNodeIndex(nodeId) {
        var items = grid.store.data.items;
        var index = Ext.each(items, function (item) {
            if (item.data.NodeID.toLowerCase() == nodeId.toLowerCase())
                return false;
        });

        return index;
    }

    var loadReportResult = function (res) {
        var structure = res;

        var config = generateConfig(structure);
        var group = new Ext.ux.grid.ColumnHeaderGroup({
            rows: [config.policyGroupRow]
        });

        var dataStore = new ORION.WebServiceStore(
                "/Orion/NCM/Services/PolicyReportResult.asmx/GenerateReportResults",
                config.fields,
                'NodeName'
                );
        dataStore.on('load', gridStoreLoadHandler);


        pageSizeBox = new Ext.form.NumberField({
            id: 'pageSizeBox',
            enableKeyEvents: true,
            allowNegative: false,
            width: 30,
            allowBlank: false,
            minValue: 1,
            maxValue: 500,
            value: pageSize,
            listeners: {
                scope: this,
                'keydown': function (f, e) {
                    var k = e.getKey();
                    if (k == e.RETURN) {
                        e.stopEvent();
                        var value = parseInt(f.getValue());
                        if (!isNaN(value) && value > 0 && value <= 500) {
                            pageSize = value;
                            var PagingToolbar = grid.getBottomToolbar();
                            if (PagingToolbar.pageSize != pageSize) { // update page size only if it is different
                                ORION.Prefs.save('PageSize', pageSize);
                                PagingToolbar.pageSize = pageSize;
                                PagingToolbar.doLoad(0);
                            }

                        }
                        else {
                            return;
                        }
                    }
                },
                'focus': function (field) {
                    field.el.dom.select();
                },
                'change': function (f, numbox, o) {
                    var value = parseInt(f.getValue());
                    if (!isNaN(value) && value > 0 && value <= 100) {
                        pageSize = value;
                        if (PagingToolbar.pageSize != pageSize) { // update page size only if it is different
                            ORION.Prefs.save('PageSize', pageSize);
                            PagingToolbar.pageSize = pageSize;

                            PagingToolbar.doLoad(0);
                        }
                    }
                    else {
                        return;
                    }
                }
            }
        });

        var PagingToolbar = new Ext.PagingToolbar({
            store: dataStore,
            pageSize: pageSize,
            displayInfo: true,
            displayMsg: '@{R=NCM.Strings;K=WEBJS_VK_41;E=js}',
            emptyMsg: '@{R=NCM.Strings;K=WEBJS_VK_42;E=js}',
            items: ['-', new Ext.form.Label({ text: '@{R=NCM.Strings;K=WEBJS_VK_300;E=js}', style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;' }), pageSizeBox]
        });

        grid = new Ext.grid.GridPanel({
            height: 500,
            width: 1090,
            store: dataStore,
            columns: config.columns,
            layout: 'fit',
            autoScroll: 'true',
            loadMask: true,
            stripeRows: true,
            viewConfig: {
            },
            bbar: PagingToolbar,
            plugins: group,
            tbar: [{
                id: 'ExportTo',
                text: '@{R=NCM.Strings;K=WEBJS_VK_105;E=js}',
                tooltip: '@{R=NCM.Strings;K=WEBJS_VM_52;E=js}',
                icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_export.gif',
                cls: 'x-btn-text-icon',
                disabled: IsDemoMode,
                menu: {
                    items: [{ iconCls: 'mnu-exportXLS', text: '@{R=NCM.Strings;K=WEBJS_VK_107;E=js}', handler: function () { ExportTo('xls'); } },
                            { iconCls: 'mnu-exportCSV', text: '@{R=NCM.Strings;K=WEBJS_VK_108;E=js}', handler: function () { ExportTo('csv'); } }]
                }
            }]

        });

        grid.on("render", NavToPage);
        grid.render('Grid');
        gridResize();
        refreshObjects(_reportId);
        $(window).resize(gridResize);
    };
    return {
        init: function () {
            pageSize = parseInt(ORION.Prefs.load('PageSize', '250'));

            _reportId = Ext.getUrlParam('ReportID');
            _nodeId = Ext.getUrlParam('NodeId');
            loadMask = new Ext.LoadMask(Ext.getBody(), {
                msg: '@{R=NCM.Strings;K=WEBJS_VM_29;E=js}',
                removeMask: true
            });
            loadMask.show();
            displayReportResult();
        }
    }
} ();


Ext.onReady(SW.NCM.ComplianceReportResultViewer.init, SW.NCM.ComplianceReportResultViewer);

//this extension for JQuery should be moved to common js resource
jQuery.download = function(url, data, method){
	//url and data options required
	if( url && data ){ 
		//data can be string of parameters or array/object
		data = typeof data == 'string' ? data : jQuery.param(data);
		//split params into form inputs
		var inputs = '';
		jQuery.each(data.split('&'), function(){ 
			var pair = this.split('=');
			inputs+='<input type="hidden" name="'+ pair[0] +'" value="'+ pair[1] +'" />'; 
		});
		//send request
		jQuery('<form action="'+ url +'" method="'+ (method||'post') +'">'+inputs+'</form>')
		.appendTo('body').submit().remove();
	};
};
