using System;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Infragistics.WebUI.UltraWebNavigator;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.NCM.Common.Help;
using SolarWinds.NCMModule.Web.Search;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCM.Web.Contracts.Search;

public partial class SearchResult : System.Web.UI.Page
{
    #region Private Members

    string _searchParam = string.Empty;
    private SearchMode _mode;
    private SearchType _type;
    private NodeSearchFields _field;
    private SearchedConfigs _searchedConfigs;
    private DateRangeConfigs _dateRangeConfigs;
    private AdvToobarNodes nodes = new AdvToobarNodes();
    private UltraWebTree _searchedConfigsTree = new UltraWebTree();
    private UltraWebTree _searchedNodesTree = new UltraWebTree();
    private Label _waitLabel = new Label();
    private Label _noResultsConfigs = new Label();
    private Label _noResultsNodes = new Label();
    private Table calendarTable = new Table();
    private Label startTimeLabel = new Label();
    private Label endTimeLabel = new Label();
    private SolarWinds.NCMModule.Web.Resources.Calendar calendarFrom = new SolarWinds.NCMModule.Web.Resources.Calendar();
    private SolarWinds.NCMModule.Web.Resources.Calendar calendarTo = new SolarWinds.NCMModule.Web.Resources.Calendar();
    private readonly IIsWrapper isLayer;
    private readonly ISearchHelper searchHelper;
    private readonly ISearchResultsBuilder searchResultsBuilder;
    private readonly ISearchForSearch searchForSearch;
    private readonly ILogger logger;

    #endregion

    public SearchResult()
    {
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
        searchHelper = ServiceLocator.Container.Resolve<ISearchHelper>();
        searchResultsBuilder = ServiceLocator.Container.Resolve<ISearchResultsBuilder>();
        searchForSearch = ServiceLocator.Container.Resolve<ISearchForSearch>();
        logger = ServiceLocator.Container.Resolve<Func<Type, ILogger>>()(GetType());
    }

    #region Public Properties

    public string SearchTitle { get; set; } = string.Empty;

    #endregion

    protected string LearnMoreLink 
    {
        get
        {
            var helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
            return helpHelperWrap.GetHelpUrl("OrionNCMPHDisableSearchPerformanceInfo");
        } 
    }

    private bool SearchIsDisabled()
    {
        using (var proxy = isLayer.GetProxy())
        {
            var searchIsEnabled = proxy.Cirrus.Settings.GetSetting(Settings.FTSIsEnabled, true, null);
            return !Convert.ToBoolean(searchIsEnabled);
        }
    }

    private void RefreshConfigIndexingStatus(IndexingStatus status)
    {
        switch (status)
        {
            case IndexingStatus.Waiting:
            case IndexingStatus.NotIndexed:
                lblIndexingStatus.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_722, 0);
                break;
            case IndexingStatus.InProgress:
                lblIndexingStatus.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_722, searchHelper.GetPercentageComplete());
                break;
        }
    }

    private IndexingStatus GetConfigIndexingStatus()
    {
        using (var proxy = isLayer.GetProxy())
        {
            var value = proxy.Cirrus.Settings.GetSetting(Settings.FTSIndexStatus, 0, null);
            var status = (IndexingStatus)Enum.Parse(typeof(IndexingStatus), value);
            return status;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Page.Title = Resources.NCMWebContent.WEBDATA_VM_109;
            var searchIsDisabled = SearchIsDisabled();

            SearchPanel.Enabled = !searchIsDisabled;
            SearchIsDisabledHolder.Visible = searchIsDisabled;

            if (!searchIsDisabled)
            {
                // Check, if config indexing in progress...
                var indexingStatus = GetConfigIndexingStatus();

                IndexingStatusHolder.Visible = (indexingStatus != IndexingStatus.Indexed);
                MainSearchTable.Visible = (indexingStatus == IndexingStatus.Indexed);

                if (indexingStatus != IndexingStatus.Indexed)
                {
                    RefreshConfigIndexingStatus(indexingStatus);
                    return;
                }
            }

            Session[@"BackPageName"] = @"Search";
            HttpContext.Current.Session[@"SelectClearAllNodes"] = false;
            nodes.NodesControl.ShowConfigs = false;
            nodes.NodesControl.ShowGroupCheckBoxes = true;
            nodes.NodesControl.ShowNodeCheckBoxes = true;
            nodes.NodesControl.TreeControl.ClientSideEvents.NodeChecked = @"UltraWebTree_TheeLevelNodeCheckedDownloadConfig";
            if (!Page.IsPostBack)
            {
                GetDataFromSession();
                AdvSearchTextBox.Text = _searchParam == null ? string.Empty : _searchParam;
                PopulateSearchTypeDropDown(searchIsDisabled);
                PopulateSearchFieldsDropDown(searchIsDisabled);
                PopulateRadioButtonListDateRangeConfigs();
                PopulateDateRangeConfigsDropDown();

                if (_mode != SearchMode.Search)
                {
                    var t = new Table();
                    var img = new Image();
                    var tr = new TableRow();
                    var tdimg = new TableCell();
                    var tdlbl = new TableCell();

                    tr.Cells.Add(tdimg);
                    tr.Cells.Add(tdlbl);

                    img.ImageUrl = @"~/Orion/NCM/Resources/images/Animated.Loading.gif";
                    tr.Cells[0].Controls.Add(img);

                    _waitLabel.ID = SearchConstants.WAIT_LABEL_ID;
                    _waitLabel.Text = SearchConstants.WAIT_LABEL_TEXT;
                    tr.Cells[1].Controls.Add(_waitLabel);
                    tr.Visible = !searchIsDisabled;

                    t.Rows.Add(tr);
                    ConfigResultHolder.Controls.Add(t);
                    AjaxTimer.Enabled = !searchIsDisabled;
                }
                DisablePageCaching();
            }

            nodes.NodesControl.TreeControl.DemandLoad += TreviewControl_DemandLoad;
            nodes.NodesControl.TreeControl.NodeCollapsed += TreviewControl_NodeCollapsed;
            nodes.NodesControl.TreeControl.NodeExpanded += TreviewControl_NodeExpanded;
        }
        catch (Exception ex)
        {
            logger.Error("Error loading the Search Results page.", ex);
            throw;
        }
    }

    void TreviewControl_NodeExpanded(object sender, WebTreeNodeEventArgs e)
    {
        SearchResultsUpdatePanel.Update();
    }

    void TreviewControl_NodeCollapsed(object sender, WebTreeNodeEventArgs e)
    {
        SearchResultsUpdatePanel.Update();
    }

    void TreviewControl_DemandLoad(object sender, WebTreeNodeEventArgs e)
    {
        SearchResultsUpdatePanel.Update();
    }

    protected override void OnInit(EventArgs e)
    {
        try
        {
            base.OnInit(e);
            _searchedConfigsTree.NodeCollapsed += _searchedConfigsTree_NodeCollapsed;
            _searchedConfigsTree.LoadOnDemand = LoadOnDemand.Manual;
            _searchedConfigsTree.DemandLoad += (s, arg) => OnTreeDemandLoad(arg.Node, HttpContext.Current.Profile.UserName);
            _searchedNodesTree.NodeCollapsed += _searchedNodesTree_NodeCollapsed;
            AjaxTimer.Tick += (s, arg) => OnAjaxTimer(HttpContext.Current.Profile.UserName);
            ConfigResultHolder.Controls.Add(_searchedConfigsTree);
            NodeResultHolder.Controls.Add(_searchedNodesTree);
            SetDefaultNodesSettings();
            NodesPanel.Controls.Add(nodes);
            nodes.Visible = false;
            RenderCalendarTable();
            DateTimePikerPlace.Controls.Add(calendarTable);
        }
        catch (Exception ex)
        {
            logger.Error("Error initializing the Search Results page.", ex);
            throw;
        }
    }

    private void OnTreeDemandLoad(Node node, string userName)
    {
        try
        {
            searchResultsBuilder.PopulateConfigText(Session[@"SearchParameter"].ToString(), node, (bool)Session[@"WholeWordSearch"], userName);
        }
        catch (Exception ex)
        {
            logger.Error("Error in demand loading", ex);
            throw;
        }
    }

    void _searchedConfigsTree_NodeCollapsed(object sender, WebTreeNodeEventArgs e)
    {
        NodesUpdatePanel.Update();
    }

    void _searchedNodesTree_NodeCollapsed(object sender, WebTreeNodeEventArgs e)
    {
        NodesUpdatePanel.Update();
    }

    public void DisablePageCaching()
    {
        Page.Response.Cache.SetExpires(DateTime.Now.AddDays(-30));
        Page.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Page.Response.Cache.SetNoServerCaching();
        Page.Response.Cache.SetNoStore();
    }

    protected void OnAjaxTimer(string userName)
    {
        try
        {
            BuildSearchedResults(userName);
            AjaxTimer.Enabled = false;
            ConfigResultHolder.Controls.Remove(_waitLabel);
            NodesUpdatePanel.Update();
        }
        catch (Exception ex)
        {
            logger.Error("Error in ajax tick.", ex);
            throw;
        }
    }

    protected void Nodes_Tick(object sender, EventArgs e)
    {
        if (SearchTypeDropDown.SelectedValue != SearchType.SelectedNodes.ToString())
        {
            nodes.RemoveNodes();
        }
        else
        {
            if (nodes.NodesControl.NodeCount == 0) nodes.NodesControl.Refresh();
        }
        NodesTimer.Enabled = false;
    }

    protected void AdvancedConfig_Tick(object sender, EventArgs e)
    {
        AdvancedConfigPanel.Visible = SearchTypeDropDown.SelectedValue != SearchType.Nodes.ToString() ? true : false;
        AdvancedConfigTimer.Enabled = false;
    }

    private void BuildSearchedResults(string userName)
    {
        Session[@"WholeWordSearch"] = this.cbWholeWord.Checked;

        SearchTitle = SearchConstants.SEARCH_RESULT_TITLE;
        var type = (SearchType)Enum.Parse(typeof(SearchType), SearchTypeDropDown.SelectedValue);
        switch (type)
        {
            case SearchType.Everything:
                AddSearchedConfigsTree(false, userName);
                AddSearchedNodesTree();
                break;
            case SearchType.Configs:
                AddSearchedConfigsTree(false, userName);
                break;
            case SearchType.Nodes:
                AddSearchedNodesTree();
                break;
            case SearchType.SelectedNodes:
                AddSearchedConfigsTree(true, userName); 
                break;
        }
    }

    private void AddSearchedConfigsTree(bool useSelectedNodes, string userName)
    {
        if (useSelectedNodes)
        {
            
            Session[@"SearchNodeIDs"] = nodes.GetCheckedNodesIdsArrayList();
            //[vm]
            Session[@"SearchCoreNodeIDs"] = nodes.GetCheckedCoreNodeIDs();
        }

        var path = Page.Request.PhysicalApplicationPath;
        searchResultsBuilder.PopulateConfigsTree(Session[@"SearchParameter"].ToString(), (bool)Session[@"WholeWordSearch"], path, _searchedConfigsTree, userName);
        ConfigResultTitle.InnerText = SearchConstants.SELECTEDNODES_SEARCH_RESULTS;
        if (_searchedConfigsTree.Nodes.Count > 0)
        {
            _searchedConfigsTree.ExpandAll();
        }
        else
        {
            _noResultsConfigs.Text = SearchConstants.NO_RESULTS_MESSAGE;
            ConfigResultHolder.Controls.Add(_noResultsConfigs);
        }
    }

    private void AddSearchedNodesTree()
    {
        var path = Page.Request.PhysicalApplicationPath;
        searchResultsBuilder.PopulateNodesTree(Session[@"SearchParameter"].ToString(),
            (NodeSearchFields)Enum.Parse(typeof(NodeSearchFields), SearchFieldsDropDown.SelectedValue),
            path, _searchedNodesTree);
        NodeResultTitle.InnerText = SearchConstants.NODES_SEARCH_RESULTS;
        if (_searchedNodesTree.Nodes.Count > 0)
        {
            _searchedNodesTree.ExpandAll();
        }
        else
        {
            if ((NodeSearchFields)Enum.Parse(typeof(NodeSearchFields), SearchFieldsDropDown.SelectedValue) == NodeSearchFields.NodeName)
            {
                _noResultsNodes.Text = SearchConstants.NO_NODECAPTION_MESSAGE;
            }
            else
            {
                _noResultsNodes.Text = SearchConstants.NO_RESULTS_MESSAGE;
            }
            NodeResultHolder.Controls.Add(_noResultsNodes);
        }
    }

    private void PopulateSearchTypeDropDown(bool searchIsDisabled)
    {
        foreach (var kvp in SearchTypesDictionary.SearchTypeDictionary)
        {
            var item = new ListItem(kvp.Value, kvp.Key.ToString());
            SearchTypeDropDown.Items.Add(item);
        }
        SearchTypeDropDown.SelectedIndex = (int)_type;
        AdvancedConfigTimer.Enabled = !searchIsDisabled;
    }

    private void PopulateSearchFieldsDropDown(bool searchIsDisabled)
    {
        foreach (var kvp in SearchFieldsDictionary.SearchFieldDictionary)
        {
            var item = new ListItem(kvp.Value, kvp.Key.ToString());
            SearchFieldsDropDown.Items.Add(item);
        }
        SearchFieldsDropDown.SelectedIndex = (int)_field;
        SearchFieldsDropDown.Visible = SearchTypeDropDown.SelectedValue == SearchType.Nodes.ToString() ? true : false;
        NodesLabel.Visible = SearchTypeDropDown.SelectedValue == SearchType.SelectedNodes.ToString() ? true : false;
        NodesTimer.Enabled = !searchIsDisabled;
    }

    private void PopulateRadioButtonListDateRangeConfigs()
    {
        foreach (var kvp in SearchedConfigsDictionary.SearchedConfigDictionary)
        {
            var item = new ListItem(kvp.Value, kvp.Key.ToString());
            RadioButtonListDateRangeConfigs.Items.Add(item);
        }
        RadioButtonListDateRangeConfigs.SelectedIndex = (int)_searchedConfigs;
        SpecifyDateRangePanelEnabling();
    }

    private void PopulateDateRangeConfigsDropDown()
    {
        foreach (var kvp in DateRangeConfigsDictionary.DateRangeConfigDictionary)
        {
            var item = new ListItem(kvp.Value, kvp.Key.ToString());
            DateRangeConfigsDropDown.Items.Add(item);
        }
        DateRangeConfigsDropDown.SelectedIndex = (int)_dateRangeConfigs;
        DateTimePikerEnabling();
    }

    private void SpecifyDateRangePanelEnabling()
    {
        SpecifyDateRangePanel.Enabled = 
            RadioButtonListDateRangeConfigs.SelectedValue == SearchedConfigs.SpecifyDownloadedConfigs.ToString();
        DateTimePikerEnabling();
    }

    private void DateTimePikerEnabling()
    {
        var enable = false;
        if (DateRangeConfigsDropDown.SelectedValue.Equals(DateRangeConfigs.SpecifyDates.ToString(), StringComparison.InvariantCultureIgnoreCase )
            && RadioButtonListDateRangeConfigs.SelectedValue.Equals(SearchedConfigs.SpecifyDownloadedConfigs.ToString(), StringComparison.InvariantCultureIgnoreCase)
            )
        {
            enable = true;
        }
        calendarTable.Enabled = enable;
        calendarFrom.Enabled = enable;
        calendarTo.Enabled = enable;
    }

    protected void SearchTypeDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            SearchFieldsDropDown.Visible =
                SearchTypeDropDown.SelectedValue == SearchType.Nodes.ToString() ? true : false;
            if (SearchTypeDropDown.SelectedValue == SearchType.Everything.ToString())
            {
                SearchFieldsDropDown.SelectedIndex = 0;
            }

            if (SearchTypeDropDown.SelectedValue == SearchType.SelectedNodes.ToString())
            {
                NodesLabel.Visible = true;
                nodes.Visible = true;
                NodesUpdatePanel.Update();
            }
            else
            {
                NodesLabel.Visible = false;
                nodes.Visible = false;
            }

            HttpContext.Current.Session[@"SelectClearAllNodes"] = false;
            NodesTimer.Enabled = true;
            AdvancedConfigTimer.Enabled = true;
        }
        catch (Exception ex)
        {
            logger.Error("Error in search type selection.", ex);
            throw;
        }
    }

    protected void RadioButtonListDateRangeConfigs_IndexChanged(Object sender, EventArgs e)
    {
        try
        {
            SpecifyDateRangePanelEnabling();
        }
        catch (Exception ex)
        {
            logger.Error("Error in date range selection.", ex);
            throw;
        }
    }

    protected void DateRangeConfigsDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DateTimePikerEnabling();
            var currDate = DateTime.Now;
            switch ((DateRangeConfigs)Enum.Parse(typeof(DateRangeConfigs), DateRangeConfigsDropDown.SelectedValue))
            {
                case DateRangeConfigs.WithinPastWeek:
                    calendarFrom.TimeSpan = currDate.AddDays(-6);
                    calendarTo.TimeSpan = currDate;
                    break;
                case DateRangeConfigs.ThisMonth:
                    calendarFrom.TimeSpan = new DateTime(currDate.Year, currDate.Month, 1);
                    calendarTo.TimeSpan = currDate;
                    break;
                case DateRangeConfigs.LastMonth:
                    var lastMonthDate = new DateTime(currDate.Year, currDate.Month, currDate.AddDays(-currDate.Day + 1).Day).AddMilliseconds(-1);
                    calendarFrom.TimeSpan = new DateTime(lastMonthDate.Year, lastMonthDate.Month, 1);
                    calendarTo.TimeSpan = lastMonthDate;
                    break;
                case DateRangeConfigs.SpecifyDates:
                    calendarFrom.TimeSpan = currDate.AddDays(-1);
                    calendarTo.TimeSpan = currDate;
                    break;
            }
        }
        catch (Exception ex)
        {
            logger.Error("Error in date range selection.", ex);
            throw;
        }
    }

    protected void AdvSearchButton_Click(object sender, EventArgs e)
    {
        try
        {
            NoNodesLabel.InnerText = string.Empty;
            if (AdvSearchTextBox.Text.Trim().Length > 0)
            {
                Session[@"SearchParameter"] = AdvSearchTextBox.Text.Trim();
                if (!CheckBoxSearchForSearch.Checked)
                {
                    ShowAdvancedSearchResult();
                }
                else
                {
                    ShowSearchForSearchResult();
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error("Error in searching.", ex);
            throw;
        }
    }

    private void ShowSearchForSearchResult()
    {
        HttpContext.Current.Session[@"WholeWordSearch"] = cbWholeWord.Checked;

        searchForSearch.FilterNodesByStringByRecursion(_searchedConfigsTree, (string)Session[@"SearchParameter"], SearchOnSearchType.Config);
        searchForSearch.FilterNodesByStringByRecursion(_searchedNodesTree, (string)Session[@"SearchParameter"], SearchOnSearchType.Node);
    }

    private void ShowAdvancedSearchResult()
    {
        SetDataToSession();
        if (SearchTypeDropDown.SelectedValue == SearchType.SelectedNodes.ToString())
        {
            if (!nodes.GetCheckedNodesIdsArrayList().Any())
            {
                NoNodesLabel.InnerText = Resources.NCMWebContent.WEBDATA_VM_110;
                ConfigResultTitle.InnerText = string.Empty;
                ConfigResultHolder.Controls.Clear();
                NodeResultTitle.InnerText = string.Empty;
                NodeResultHolder.Controls.Clear();
                return;
            }
        }
        else
        {
            Session[@"SearchGroups"] = null;
            Session[@"SearchNodeIDs"] = null;
            Session[@"SearchCoreNodeIDs"] = null;
        }
        PreparePageBeforeSearch();
    }

    private void PreparePageBeforeSearch()
    {
        ConfigResultTitle.InnerText = string.Empty;
        NodeResultTitle.InnerText = string.Empty;
        var t = new Table();
        var img = new Image();
        var tr = new TableRow();
        var tdimg = new TableCell();
        var tdlbl = new TableCell();

        tr.Cells.Add(tdimg);
        tr.Cells.Add(tdlbl);

        img.ImageUrl = @"~/Orion/NCM/Resources/images/Animated.Loading.gif";
        tr.Cells[0].Controls.Add(img);

        _waitLabel.ID = SearchConstants.WAIT_LABEL_ID;
        _waitLabel.Text = SearchConstants.WAIT_LABEL_TEXT;
        tr.Cells[1].Controls.Add(_waitLabel);

        t.Rows.Add(tr);
        ConfigResultHolder.Controls.Add(t);

        if (ConfigResultHolder.Controls.Contains(_noResultsConfigs))
        {
            ConfigResultHolder.Controls.Remove(_noResultsConfigs);
        }
        if (NodeResultHolder.Controls.Contains(_noResultsNodes))
        {
            NodeResultHolder.Controls.Remove(_noResultsNodes);
        }
        _searchedConfigsTree.Nodes.Clear();
        _searchedNodesTree.Nodes.Clear();
        AjaxTimer.Enabled = true;
    }

    private void GetDataFromSession()
    {
        if (Session[@"SearchParam"] != null)
        {
            _searchParam = HttpContext.Current.Session[@"SearchParam"].ToString();
            Session[@"SearchParameter"] = _searchParam;
        }

        _mode = (Session[@"SearchMode"] != null) ?
            (SearchMode)Enum.Parse(typeof(SearchMode), HttpContext.Current.Session[@"SearchMode"].ToString()) :
            SearchMode.Search;

        _type = (Session[@"SearchType"] != null) ?
            (SearchType)Enum.Parse(typeof(SearchType), HttpContext.Current.Session[@"SearchType"].ToString()) :
            SearchType.Everything;
        
        _field = (Session[@"NodeSearchField"] != null) ?
            (NodeSearchFields)Enum.Parse(typeof(NodeSearchFields), HttpContext.Current.Session[@"NodeSearchField"].ToString()):
            NodeSearchFields.Everything;
        
        _searchedConfigs = (Session[@"SearchedConfigs"] != null) ?
            (SearchedConfigs)Enum.Parse(typeof(SearchedConfigs), HttpContext.Current.Session[@"SearchedConfigs"].ToString()):
            SearchedConfigs.LastDownloadedConfigs;
        
        _dateRangeConfigs = (Session[@"DateRangeConfigs"] != null) ?
            (DateRangeConfigs)Enum.Parse(typeof(DateRangeConfigs), HttpContext.Current.Session[@"DateRangeConfigs"].ToString()):
            DateRangeConfigs.WithinPastWeek;
        
        calendarFrom.TimeSpan = (Session[@"DateFrom"] != null) ?
            Convert.ToDateTime(HttpContext.Current.Session[@"DateFrom"]):
            DateTime.Now.AddDays(-6);
        
        calendarTo.TimeSpan = (Session[@"DateTo"] != null) ? 
            Convert.ToDateTime(HttpContext.Current.Session[@"DateTo"]):
            DateTime.Now;
    }

    private void SetDataToSession()
    {
        Session[@"SearchParam"] = AdvSearchTextBox.Text.Trim();
        Session[@"SearchMode"] = SearchMode.Result;
        Session[@"SearchType"] = SearchTypeDropDown.SelectedValue;
        Session[@"NodeSearchField"] = SearchFieldsDropDown.SelectedValue;
        Session[@"SearchedConfigs"] = RadioButtonListDateRangeConfigs.SelectedValue;
        Session[@"DateRangeConfigs"] = DateRangeConfigsDropDown.SelectedValue;
        Session[@"ConfigTypes"] = ConfigTypesDropDown.SelectedValue;
        Session[@"DateFrom"] = calendarFrom.TimeSpan;
        Session[@"DateTo"] = calendarTo.TimeSpan;
    }

    private void SetDefaultResourceSettings()
    {
        NPMDefaultSettingsHelper.SetUserSettings("GroupBy", "Vendor");
    }

    private void SetDefaultNodesSettings()
    {
        var resourceSettings = new ResourceSettings();
        SetDefaultResourceSettings();
        nodes.ResourceSettings = resourceSettings;

        nodes.ID = @"Nodes";
    }

    private void RenderCalendarTable()
    {
        calendarFrom.ID = @"calendarFrom";
        calendarFrom.Text = Resources.NCMWebContent.WEBDATA_VM_111;
        
        
        calendarTo.ID = @"calendarTo";
        calendarTo.Text = Resources.NCMWebContent.WEBDATA_VM_112;

        startTimeLabel.ForeColor = System.Drawing.Color.Red;
        endTimeLabel.ForeColor = System.Drawing.Color.Red;

        // initialize calendar table
        calendarTable.CellPadding = 0;
        calendarTable.CellSpacing = 0;
        calendarTable.BorderWidth = 0;
        var calendarTableRows = new TableRow[] {new TableRow(), new TableRow() };

        // fill row[0]
        var tableCell = new TableCell();        
        tableCell.Controls.Add(calendarFrom);
        tableCell.HorizontalAlign = HorizontalAlign.Left;
        tableCell.Width = Unit.Pixel(210);
        calendarTableRows[0].Cells.Add(tableCell);

        tableCell = new TableCell();        
        tableCell.Controls.Add(calendarTo);
        tableCell.Style.Add(@"padding-left", @"15px");
        tableCell.Width = Unit.Pixel(210);
        tableCell.HorizontalAlign = HorizontalAlign.Left;
        calendarTableRows[0].Cells.Add(tableCell);

        // fill row[1]            
        tableCell = new TableCell();
        tableCell.Controls.Add(startTimeLabel);
        tableCell.HorizontalAlign = HorizontalAlign.Left;
        tableCell.Width = Unit.Pixel(210);
        calendarTableRows[1].Cells.Add(tableCell);

        tableCell = new TableCell();
        tableCell.Controls.Add(endTimeLabel);
        tableCell.Width = Unit.Pixel(210);
        tableCell.HorizontalAlign = HorizontalAlign.Left;
        calendarTableRows[1].Cells.Add(tableCell);

        calendarTable.Rows.Add(calendarTableRows[0]);
        calendarTable.Rows.Add(calendarTableRows[1]);

        calendarTable.Width = Unit.Pixel(540);
    }
}
