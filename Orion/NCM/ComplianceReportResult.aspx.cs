using System;
using System.Web;
using System.Collections.Generic;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.InformationService.Contract2;
using SolarWinds.NCM.Web.Contracts.SiteMap;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NCM_ComplianceReportResult : OrionView
{
    private static readonly string REPORTS_RESULT_SESSION_KEY = @"complicance_report_result";
    private static readonly string REPORTS_RESULT_SORTDIRECTION_KEY = @"complicance_report_sortdirection";
    private const string QUERY_PARAM_REPORTID = "ReportID";
    private const string NCM_VIEW_TYPE = "NCMComplianceReportResult";
    private bool bShowSummary = true;

    private readonly IIsWrapper isLayer;
    private readonly ILogger logger;
    private readonly Func<INcmSiteMapRenderer> rendererFactory;

    public Orion_NCM_ComplianceReportResult()
    {
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
        logger = ServiceLocator.Container.Resolve<Func<Type, ILogger>>()(GetType());
        rendererFactory = ServiceLocator.Container.Resolve<Func<INcmSiteMapRenderer>>();
    }

    public string ReportTitle { get; private set; } = string.Empty;

    public string LastUpdate { get; private set; } = string.Empty;

    public Guid? PolicyReportID { get; private set; }

    public string InfoMsg { get; private set; } = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            InfoMsg = Resources.NCMWebContent.WEBDATA_VM_79;
            if (!IsPostBack)
            {
                Session[REPORTS_RESULT_SESSION_KEY] = null;
                Session[REPORTS_RESULT_SORTDIRECTION_KEY] = null;
            }

            PolicyReportID = Request.QueryNullableGuid(QUERY_PARAM_REPORTID);

            if (PolicyReportID == null)
            {
                throw new ArgumentException(Resources.NCMWebContent.WEBDATA_VM_80);
            }

            InitializeReportFields();
            PreparyingPageLayout();
        }
        catch (Exception ex)
        {
            logger.Error("Error loading the compliance report result page.", ex);
            throw;
        }
    }

    public override string ViewType => NCM_VIEW_TYPE;

    #region private methods

    private void InitializeReportFields()
    {
        try
        {
            using (var proxy = isLayer.GetProxy())
            {
                var dt = proxy.Query(
                    @"SELECT P.Name, P.ShowSummary, P.LastUpdated FROM Cirrus.PolicyReports AS P WHERE P.PolicyReportID=@reportId",
                    new PropertyBag {{@"reportId", PolicyReportID.Value}});

                if (dt != null && dt.Rows.Count > 0)
                {
                    var name = dt.Rows[0]["Name"] as string;
                    ReportTitle = HttpContext.Current.Server.HtmlEncode(name);
                    Page.Title = ReportTitle;
                    bShowSummary = Convert.ToBoolean(dt.Rows[0]["ShowSummary"]);
                    var lastUpdateDate = Convert.ToDateTime(dt.Rows[0]["LastUpdated"]);
                    LastUpdate = dt.Rows[0]["LastUpdated"] is DBNull || dt.Rows[0]["LastUpdated"] == null
                        ? Resources.NCMWebContent.WEBDATA_VM_81
                        : string.Format(Resources.NCMWebContent.WEBDATA_VM_82 + lastUpdateDate.ToLongDateString() + @" " + lastUpdateDate.ToLongTimeString());
                }
                else
                {
                    ReportTitle = Resources.NCMWebContent.WEBDATA_VK_197;
                    LastUpdate = Resources.NCMWebContent.WEBDATA_VM_81;
                    bShowSummary = false;
                }
            }

        }
        catch (Exception ex)
        {
            InfoMsg = ex.Message;
            infoContainer.Attributes[@"style"] = @"display:block";

            bShowSummary = false;
        }
    }

    private void PreparyingPageLayout()
    {
        ReportSummaryContainer.Visible = bShowSummary;
    }

    #endregion

    protected void SiteMapPath_OnInit(object sender, EventArgs e)
    {
        try
        {
            var id = string.Empty;
            if (!string.IsNullOrEmpty(Request.QueryString[@"ReportID"]))
                id = Request.QueryString[@"ReportID"];

            var renderer = rendererFactory();
            renderer.SetUpData(new KeyValuePair<string, string>(@"ReportID", id));
            NodeSiteMapPath.SetUpRenderer(renderer);
        }
        catch (Exception ex)
        {
            logger.Error("Error initializing the site map.", ex);
            throw;
        }
    }
}
