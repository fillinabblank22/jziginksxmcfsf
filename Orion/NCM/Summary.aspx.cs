using System;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_Cirrus_Summary : OrionView
{
    private const string NCM_VIEW_TYPE = "NCMSummary";

    private readonly ICommonHelper commonHelper;

    public Orion_Cirrus_Summary()
    {
        commonHelper = ServiceLocator.Container.Resolve<ICommonHelper>();
    }

    public override string ViewType => NCM_VIEW_TYPE;

    protected override void OnInit(EventArgs e)
    {
        commonHelper.Initialise();
        
        Title = ViewInfo.ViewTitle;
        resContainer.DataSource = ViewInfo;
        resContainer.DataBind();
        
        base.OnInit(e);
    }
}
