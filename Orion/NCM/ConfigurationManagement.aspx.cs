﻿using System;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using SolarWinds.NCM.Common.Dal.ConfigTypes;
using SolarWinds.NCM.Contracts.ConfigTypes;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Core.Common;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.NCMCore;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;
using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCM.Contracts;

public partial class Orion_NCM_ConfigurationManagement : System.Web.UI.Page
{
    private readonly IConfigTypesProvider configTypesProvider;

    public Orion_NCM_ConfigurationManagement()
    {
        configTypesProvider = ServiceLocator.Container.Resolve<IConfigTypesProvider>();
    }

    protected string CloseWindowHandlerUrl
    {
        get
        {
            return UrlHelper.ToSafeUrlParameter(@"/Orion/NCM/CloseWindowHandler.aspx");
        }
    }

    protected bool ShowApprovalRequestButton
    {
        get { return ConfigChangeApprovalHelper.ShowSendApprovalRequestButton; }
    }

    protected string NCMAccountRole
    {
        get
        {
            return SecurityHelper.GetNCMAccountRole();
        }
    }

    protected bool AllowNodeManagement
    {
        get { return Profile.AllowNodeManagement; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.WEBDATA_VK_911;

        var validator = new SWISValidator();
        validator.ContentContainer = ContentContainer;
        validator.RedirectIfValidationFailed = true;
        divError.Controls.Add(validator);

        if (validator.IsValid)
        {
            ShowUseUserLevelLoginSuggestion();
        }
    }

    public string GetGroupingStore()
    {
        var store = new StringBuilder();
        store.Append(@"[");
        store.AppendFormat(@"['', '{0}'],", Resources.NCMWebContent.WEBDATA_IA_17);
        store.AppendFormat(@"['NodeGroup','{0}'],", Resources.NCMWebContent.WEBDATA_VK_273);
        store.AppendFormat(@"['Status','{0}'],", Resources.NCMWebContent.WEBDATA_VK_274);
        store.AppendFormat(@"['Contact','{0}'],", Resources.NCMWebContent.WEBDATA_VK_275);
        store.AppendFormat(@"['Location','{0}'],", Resources.NCMWebContent.WEBDATA_VK_276);
        store.AppendFormat(@"['SysObjectID','{0}'],", Resources.NCMWebContent.WEBDATA_VK_277);
        store.AppendFormat(@"['Vendor','{0}'],", Resources.NCMWebContent.WEBDATA_VK_278);
        store.AppendFormat(@"['MachineType','{0}'],", Resources.NCMWebContent.WEBDATA_VK_279);
        store.AppendFormat(@"['IOSImage','{0}'],", Resources.NCMWebContent.WEBDATA_VK_280);
        store.AppendFormat(@"['IOSVersion','{0}'],", Resources.NCMWebContent.WEBDATA_VK_281);
        store.AppendFormat(@"['ConfigTypes','{0}'],", Resources.NCMWebContent.WEBDATA_VK_282);
        store.AppendFormat(@"['EnableLevel','{0}'],", Resources.NCMWebContent.WEBDATA_VK_283);
        store.AppendFormat(@"['ExecProtocol','{0}'],", Resources.NCMWebContent.WEBDATA_VK_284);
        store.AppendFormat(@"['CommandProtocol','{0}'],", Resources.NCMWebContent.WEBDATA_VK_285);
        store.AppendFormat(@"['TransferProtocol','{0}'],", Resources.NCMWebContent.WEBDATA_VK_286);
        store.AppendFormat(@"['LoginStatus','{0}']", Resources.NCMWebContent.WEBDATA_VK_287);
        foreach (var prop in CustomPropertyHelper.GetCustomProperties())
        {
            store.AppendFormat(@",['{0}','{0}']", prop);
        }
        store.Append(@"]");
        return store.ToString();
    }

    public string GetConfigTypesStore()
    {
        try
        {
            var configTypes = configTypesProvider.GetAll().ToArray();
            return JsonConvert.SerializeObject(configTypes);
        }
        catch
        {
            return JsonConvert.SerializeObject(new ConfigType[0]);
        }
    }

    public string GetCPColumnModel()
    {
        var customProperties = CustomPropertyMgr.GetCustomPropertiesForTable(@"Nodes").ToList();
        if (customProperties.Count > 0)
        {
            var store = new StringBuilder();
            store.Append(@"[");
            var comma = string.Empty;
            foreach (var customProperty in customProperties)
            {
                if (!NCMNode.cRegularNCMColumnList.Any(s => s.Equals(customProperty.PropertyName, StringComparison.CurrentCultureIgnoreCase)) && !NCMNode.cSWISRelationNCMColumnList.Any(s => s.Equals(customProperty.PropertyName, StringComparison.CurrentCultureIgnoreCase)))
                {
                    store.AppendFormat(@"{0}{1}'PropertyName':'{2}','PropertyType':'{3}'{4}", comma, @"{", customProperty.PropertyName, customProperty.PropertyType.ToString(), @"}");
                    comma = @",";
                }
            }
            store.Append(@"]");
            return store.ToString();
        }

        return @"[]";
    }

    public string GetGridPersonalizationState()
    {
        var gridPersonalizationHelper = new GridPersonalizationHelper("NCM_ConfigManagement_");
        var state = gridPersonalizationHelper.GetGridPersonalizationState();
        return state;
    }

    private void ShowUseUserLevelLoginSuggestion()
    {
        var isWrapper = new ISWrapper();
        using (var proxy = isWrapper.Proxy)
        {
            var connLevel = proxy.Cirrus.Settings.GetSetting(Settings.ConnectivityLevel, ConnectivityLevels.Default, null);
            var useUserLevelLoginCreds = connLevel.Equals(ConnectivityLevels.Level3);
            ViewState["UseUserLevelLoginCreds"] = useUserLevelLoginCreds;

            divShowUseUserLevelLoginSuggestion.Visible = useUserLevelLoginCreds && SecurityHelper.IsPermissionExist(SecurityHelper._canDownload);
        }
    }
}