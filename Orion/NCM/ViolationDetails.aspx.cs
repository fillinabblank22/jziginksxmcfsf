using System;
using System.Web.UI;

public partial class Orion_NCM_Default : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            violationDetailsTree.Show(new Guid(Page.Request.QueryString[@"currentNodeID"]),
                Page.Request.QueryString[@"currentPolicyID"], Page.Request.QueryString[@"currentRuleID"],
                Page.Request.QueryString[@"currentReportID"],
                Convert.ToInt32(Page.Request.QueryString[@"currentLimitationID"]));
        }
    }
}
