﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="SecurityPolicyDetails.aspx.cs" Inherits="Orion_NCM_SecurityPolicyDetails" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.Cirrus.Web.UI" Assembly="SolarWinds.Cirrus.Web" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="ncmHeadPlaceHolder" runat="server">
    <style type="text/css">
        body {
            background: #ecedee;
        }
    </style>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ncmPageTitlePlaceHolder">
    <% =Page.Title %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" runat="server">
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
    <ncm:DropDownMapPath ID="NodeSiteMapPath" Provider="NCMSitemapProvider" runat="server"  OnInit="SiteMapPath_OnInit">
        <RootNodeTemplate>
            <a href="<%# Eval(@"url") %>" ><u> <%# Eval(@"displayTitle") %></u> </a>
        </RootNodeTemplate>
        <CurrentNodeTemplate>
            <a href="<%# Eval(@"url") %>" ><%# Eval(@"displayTitle") %> </a>
        </CurrentNodeTemplate>
    </ncm:DropDownMapPath>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmMainContentPlaceHolder" runat="server">
    <ncm:NCMSecurityPolicyDetailsResourceHost ID="resourceHostControl" runat="server">
        <orion:ResourceContainer runat="server" ID="resourceContainer" />
    </ncm:NCMSecurityPolicyDetailsResourceHost>
</asp:Content>