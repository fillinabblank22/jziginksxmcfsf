using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Drawing;
using System.Web;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources.SearchBusinessLayer;
using SolarWinds.InformationService.Contract2;
using SolarWinds.NCM.Web.Contracts.SiteMap;
using SolarWinds.NCMModule.Web.Resources;

public partial class SearchForEndHostResult : OrionView
{
    private static readonly string ncmViewType = @"NCMSearchForEndHostResult";

    private readonly IIsWrapper isLayer;
    private readonly ILogger logger;
    private readonly Func<INcmSiteMapRenderer> rendererFactory;

    public SearchForEndHostResult()
    {
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
        logger = ServiceLocator.Container.Resolve<Func<Type, ILogger>>()(GetType());
        rendererFactory = ServiceLocator.Container.Resolve<Func<INcmSiteMapRenderer>>();
    }

    public override string ViewType => ncmViewType;

    #region Event Handlers

    protected override void OnInit(EventArgs e)
    {
        Title = ViewInfo.ViewTitle;
        resContainer.DataSource = ViewInfo;
        resContainer.DataBind();

        base.OnInit(e);

        table.ItemCommand += table_ItemCommand;
        tableWireless.ItemCommand += tableWireless_ItemCommand;
    }

    private void tableWireless_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName != @"Drill-down")
        {
            return;
        }

        string objectName;
        var nodeId = GetNcmNodeId(e.CommandArgument.ToString(), out objectName);
        if (!string.IsNullOrEmpty(nodeId))
        {
            Response.Redirect($"/Orion/NCM/NodeDetails.aspx?NodeID={nodeId}");
        }
        else
        {
            var redirectUrl = Request.Url.ToString();
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), @"key_drill_down", string.Format(@"alert('" + Resources.NCMWebContent.WEBDATA_VK_257 + @"');window.location=""{1}"";", objectName, redirectUrl), true);
        }
    }

    private void table_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName != @"Drill-down")
        {
            return;
        }

        string objectName;
        var nodeId = GetNcmNodeId(e.CommandArgument.ToString(), out objectName);
        if (!string.IsNullOrEmpty(nodeId))
        {
            Response.Redirect($"/Orion/NCM/NodeDetails.aspx?NodeID={nodeId}");
        }
        else
        {
            var redirectUrl = Request.Url.ToString();
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), @"key_drill_down", string.Format(@"alert('" + Resources.NCMWebContent.WEBDATA_VK_257 + @"');window.location=""{1}"";", objectName, redirectUrl), true);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!CommonHelper.IsNpmInstalled())
        {
            return;
        }

        var searchEngine = SearchEngine.Instance;

        var resource = new ResourceInfo();
        if (!string.IsNullOrEmpty(Request[@"ResourceID"]))
        {
            var resourceId = Request[@"ResourceID"];
            resource = ResourceManager.GetResourceByID(Convert.ToInt32(resourceId));
            resource.ID = Convert.ToInt32(resourceId);
        }

        var searchType = !string.IsNullOrEmpty(Request[@"SearchType"])
            ? (SearchType) Enum.Parse(typeof(SearchType), Request[@"SearchType"])
            : SearchType.MACAddress;

        var searchValue = !string.IsNullOrEmpty(Request[@"SearchValue"]) ? HttpUtility.HtmlEncode(Request[@"SearchValue"]) : string.Empty;

        var addedColumns =
            !string.IsNullOrEmpty(resource.Properties[@"AddedColumns"]) && resource.Properties[@"AddedColumns"].Length > 0
                ? searchEngine.StringToList<AddedColumns>(resource.Properties[@"AddedColumns"])
                : new List<AddedColumns>();

        var addedColumnsWireless =
            !string.IsNullOrEmpty(resource.Properties[@"AddedColumnsWireless"]) &&
            resource.Properties[@"AddedColumnsWireless"].Length > 0
                ? searchEngine.StringToList<WirelessAddedColumns>(resource.Properties[@"AddedColumnsWireless"])
                : new List<WirelessAddedColumns>();

        var drilldown = !string.IsNullOrEmpty(resource.Properties[@"Drill-down"]) ? resource.Properties[@"Drill-down"] : @"NPM";
        var fuzzyMatch = !string.IsNullOrEmpty(resource.Properties[@"FuzzyMatch"]) && Convert.ToBoolean(resource.Properties[@"FuzzyMatch"]);

        var dtResults = searchEngine.PerformSearch(searchType, addedColumns, searchValue, fuzzyMatch, ViewInfo.LimitationID);
        var dtResultsWireless = searchEngine.PerformSearchWireless(searchType, addedColumnsWireless, searchValue, fuzzyMatch, ViewInfo.LimitationID);

        var itemTemplate = new SearchForEndHostTemplate(ListItemType.Item, addedColumns.Count == 0 ? string.Empty : resource.Properties[@"AddedColumns"], drilldown);
        var headerTemplate = new SearchForEndHostTemplate(ListItemType.Header, addedColumns.Count == 0 ? string.Empty : resource.Properties[@"AddedColumns"], drilldown);

        table.HeaderTemplate = headerTemplate;
        table.ItemTemplate = itemTemplate;
        table.RepeatColumns = 1;
        table.RepeatLayout = RepeatLayout.Table;
        table.RepeatDirection = RepeatDirection.Vertical;

        table.HeaderStyle.Height = Unit.Pixel(25);
        table.HeaderStyle.BorderStyle = BorderStyle.None;
        table.HeaderStyle.BackColor = Color.FromArgb(229, 228, 216);

        table.ItemStyle.BackColor = Color.FromArgb(0xE4F1F8);
        table.ItemStyle.Height = Unit.Pixel(20);
        table.ItemStyle.BorderStyle = BorderStyle.None;

        table.AlternatingItemStyle.BackColor = Color.White;
        table.AlternatingItemStyle.Height = Unit.Pixel(20);
        table.AlternatingItemStyle.BorderStyle = BorderStyle.None;

        table.DataSource = dtResults;
        table.DataBind();

        if (dtResults.Rows.Count == 0)
        {
            errorLabel.Visible = true;

            using (var proxy = isLayer.GetProxy())
            {
                var dt = proxy.Query(@"SELECT SourceNodeID FROM Orion.NPM.OrionSwitchPortMapping");
                errorLabel.Text = dt.Rows.Count == 0
                    ? Resources.NCMWebContent.WEBDATA_VK_256
                    : Resources.NCMWebContent.WEBDATA_VK_255;
            }
        }
        else
        {
            AsOfLastDiscovery.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_258, dtResults.Compute("MAX(LastSeen)", string.Empty));
        }

        var itemTemplateWireless = new WirelessSearchForEndHostTemplate(ListItemType.Item, addedColumnsWireless.Count == 0 ? string.Empty : resource.Properties[@"AddedColumnsWireless"], drilldown);
        var headerTemplateWireless = new WirelessSearchForEndHostTemplate(ListItemType.Header, addedColumnsWireless.Count == 0 ? string.Empty : resource.Properties[@"AddedColumnsWireless"], drilldown);

        tableWireless.HeaderTemplate = headerTemplateWireless;
        tableWireless.ItemTemplate = itemTemplateWireless;
        tableWireless.RepeatColumns = 1;
        tableWireless.RepeatLayout = RepeatLayout.Table;
        tableWireless.RepeatDirection = RepeatDirection.Vertical;

        tableWireless.HeaderStyle.Height = Unit.Pixel(25);
        tableWireless.HeaderStyle.BorderStyle = BorderStyle.None;
        tableWireless.HeaderStyle.BackColor = Color.FromArgb(229, 228, 216);

        tableWireless.ItemStyle.BackColor = Color.FromArgb(0xE4F1F8);
        tableWireless.ItemStyle.Height = Unit.Pixel(20);
        tableWireless.ItemStyle.BorderStyle = BorderStyle.None;

        tableWireless.AlternatingItemStyle.BackColor = Color.White;
        tableWireless.AlternatingItemStyle.Height = Unit.Pixel(20);
        tableWireless.AlternatingItemStyle.BorderStyle = BorderStyle.None;

        tableWireless.DataSource = dtResultsWireless;
        tableWireless.DataBind();

        if (dtResultsWireless.Rows.Count == 0)
        {
            errorLabelWireless.Visible = true;
            errorLabelWireless.Text = Resources.NCMWebContent.WEBDATA_VK_255;
        }
    }

    #endregion

    #region Private Methods

    private string GetNcmNodeId(string netObjectId, out string objectName)
    {
        var nodeId = string.Empty;
        
        var node = new SolarWinds.Orion.NPM.Web.Node(netObjectId);
        using (var proxy = isLayer.GetProxy())
        {
            var dt = proxy.Query(@"SELECT TOP 1 NodeID FROM Cirrus.NodeProperties WHERE CoreNodeID=@coreNodeId",
                new PropertyBag {{@"coreNodeId", node.NodeID}});

            if (dt.Rows.Count == 0)
            {
                objectName = node.Name; 
                return nodeId; // NPM node does not exist in NCM
            }

            foreach (DataRow dataRow in dt.Rows)
            {
                nodeId = dataRow["NodeID"].ToString();
            }
        }

        objectName = string.Empty;
        return nodeId;
    }

    #endregion

    protected void SiteMapPath_OnInit(object sender, EventArgs e)
    {
        try
        {
            var renderer = rendererFactory();
            NodeSiteMapPath.SetUpRenderer(renderer);
        }
        catch (Exception ex)
        {
            logger.Error("Error initializing the site map.", ex);
            throw;
        }
    }
}
