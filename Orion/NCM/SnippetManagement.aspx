﻿<%@  Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true"
    CodeFile="SnippetManagement.aspx.cs" Inherits="Orion_NCM_SnippetManagment" %>

<%@ Register TagPrefix="ncm1" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls"
    Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register TagPrefix="ncm" Src="~/Orion/NCM/NavigationTabBar.ascx" TagName="NavigationTabBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmHeadPlaceHolder" runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script src="/Orion/NCM/SnippetManagment.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippetsHelper.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/jquery-linedtextarea.js"></script>
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/PolicyReports/PolicyReport.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
    <link href="/Orion/NCM/Resources/Jobs/Jobs.css" rel="stylesheet" type="text/css" />
    <link href="/Orion/NCM/styles/NCMResources.css" rel="stylesheet" type="text/css" />
    <link rel="Stylesheet" type="text/css" href="/Orion/NCM/Resources/ConfigSnippets/jquery-linedtextarea.css" />
    <ncm1:ExtLocalDateTimePattern ID="ExtLocalDateTimePattern1" runat="server" />
    <style type="text/css">
        #txtSnippetComments, .linedwrap
        {
            border: 1px solid #D0D0D0 !important;
            overflow: auto;
        }
        #txtConfigContent
        {
            overflow: auto;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" runat="Server">
    <%= Page.Title %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmExportToPDFPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ncmCustomizePagePlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="ncmHelpIconPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ncmMainContentPlaceHolder" runat="Server">
    <input type="hidden" name="NCM_SnippetManagement_PageSize" id="NCM_SnippetManagement_PageSize"
        value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get(@"NCM_SnippetManagement_PageSize")%>' />
    <asp:Panel ID="ContentContainer" runat="server">
        <div style="padding: 10px;">
            <ncm:NavigationTabBar ID="NavigationTabBar1" runat="server" />
            <div id="tabPanel" class="tab-top">
                <table id="mainContent" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td>
                            <div style="margin-left: 0px; margin-bottom: 10px;">
                                <%= Resources.NCMWebContent.WEBDATA_IA_25%>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="Suggestion" style="margin-left: 0px; margin-bottom: 10px;">
                                <div class="Suggestion-Icon">
                                </div>
                                <%= Resources.NCMWebContent.WEBDATA_IA_24%>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td id="gridCell">
                            <div id="mainGrid">
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
    <div style="padding: 10px;" id="divError" runat="server" />
</asp:Content>

<asp:Content ID="Content8" ContentPlaceHolderID="ncmOutsideFormPlaceHolder" runat="Server">
    <div id="createEditSnippetDialog" class="x-hidden">
        <div id="createEditSnippetDialogBody" class="x-panel-body" style="padding: 10px;
            height: 535; overflow: auto;">
            <div>
            </div>
            <div style="padding-top: 5px;">
                <table>
                    <tr>
                        <td>
                            <div id="lblSnippetName">
                                <%= Resources.NCMWebContent.WEBDATA_IA_23%>:</div>
                        </td>
                        <td>
                            <div id="snippetContentBody">
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="padding: 20px 0px 20px 0px;">
                <textarea class="lined" style="width: 740px; height: 300px;" id="txtConfigContent"></textarea>
            </div>
            <div>
                <%= Resources.NCMWebContent.WEBDATA_IC_145%></div>
            <div style="padding: 10px 0px 20px 0px;">
                <textarea style="width: 740px; height: 100px;" id="txtSnippetComments"></textarea>
            </div>
        </div>
    </div>
</asp:Content>
