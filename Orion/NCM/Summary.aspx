<%@ Page Language="C#"  MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="Summary.aspx.cs" Inherits="Orion_Cirrus_Summary" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="orion" TagName="Refresher" Src="~/Orion/Controls/Refresher.ascx" %>
<%@ Register TagPrefix="ncm" Src="~/Orion/NCM/Controls/IconNCMSettings.ascx" TagName="IconNCMSettings" %>
<%@ Register TagPrefix="ncm" Src="~/Orion/NCM/Controls/IconCustomizePage.ascx" TagName="IconCustomizePage" %>
<%@ Register TagPrefix="ncm" Src="~/Orion/NCM/Controls/IconExportToPdf.ascx" TagName="IconExportToPdf" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="ncmHeadPlaceHolder" runat="server">
    <style type="text/css">
        body {
            background: #ecedee;
        }
    </style>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ncmPageTitlePlaceHolder">
    <link rel="stylesheet" type="text/css" href="styles/NCMResources.css" />
    <script type="text/javascript" src="JavaScript/main.js?v=1.0"></script>
    <script type="text/javascript" src="JavaScript/jquery.filestyle.js"></script>
	<%=Page.Title %>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ncmMainContentPlaceHolder" runat="server">	
    <orion:Refresher ID="Refresher" runat="server" />
  
   	<div id="ncmResourceWrapper">
	    <orion:ResourceHostControl ID="resourceHostControl" runat="server">
		    <orion:ResourceContainer runat="server" ID="resContainer" />
	    </orion:ResourceHostControl>
	</div>

	<script type="text/javascript">
		function igtree_mouseout() {}
		function igtree_mouseover(){}
	</script>	    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmCustomizePagePlaceHolder" runat="server">
    <ncm:IconCustomizePage ID="IconCustomizePage1" runat="server" />
    <ncm:IconNCMSettings ID="IconNCMSettings1" runat="server"/>
</asp:Content>

<asp:Content ID="ExportToPdfContent" ContentPlaceHolderID="ncmExportToPDFPlaceHolder" runat="server">
    <ncm:IconExportToPdf ID="IconExportToPdf1" runat="server" />
</asp:Content> 
