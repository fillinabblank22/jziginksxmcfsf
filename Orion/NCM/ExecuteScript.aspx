﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="ExecuteScript.aspx.cs" Inherits="Orion_NCM_ExecuteScript" %>

<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.PolicyReportsManagement.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmHeadPlaceHolder" Runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script type="text/javascript" src="/Orion/NCM/ExecuteScript.js"></script>
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/PolicyReports/PolicyReport.css" />

    <link rel="stylesheet" type="text/css" href="/Orion/NCM/JavaScript/Ext_UX/Ext.ux.form.FileUploadField.css" />
    <link rel="Stylesheet" type="text/css" href="/Orion/NCM/Resources/ConfigSnippets/jquery-linedtextarea.css" />
	<script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/jquery-linedtextarea.js"></script>

    <style type="text/css">
        .ImgButton
        {
            font-family: Arial,Verdana,Helvetica,sans-serif;
            font-size: 11px !important;
            font-weight: normal;
            color:Black;
        }
    
        .ImgButton:hover
        {
		    cursor:pointer;
		    color:Black;
        }
    
        .x-form-file-wrap
        {
            height:auto;
        }
        
        .GroupItem
        {
            padding: 2px 3px;
        }
        
        .SelectedItem
        {
            background-image: url('/Orion/Nodes/images/background/left_selection_gradient.gif') !important;
            background-color: #97d6ff !important;
            background-repeat: repeat-x !important;
        }
        
        
    </style>

    <script type="text/javascript">
        SW.NCM.NcmNodeIds = <%= NcmNodeIdsJSON %>;
        SW.NCM.ShowApprovalRequestButton = <%=SolarWinds.NCMModule.Web.Resources.ConfigChangeApprovalHelper.ShowSendApprovalRequestButton.ToString().ToLowerInvariant() %>;
        SW.NCM.RequestTypeUploadConfig = '<%=SolarWinds.NCM.Contracts.InformationService.NCMApprovalTicket.TYPE_UPLOAD_CONFIG %>';
        SW.NCM.RequestTypeExecuteScript = '<%=SolarWinds.NCM.Contracts.InformationService.NCMApprovalTicket.TYPE_EXECUTE_SCRIPTS %>';
        SW.NCM.CloseWindowHandlerUrl = '<%=CloseWindowHandlerUrl %>';
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" Runat="Server">
    <%=Page.Title %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ncmExportToPDFPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ncmCustomizePagePlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="ncmHelpIconPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
    <div style="padding:10px;">
        <asp:UpdatePanel runat="server" ID="updatePanel" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="width:320px;padding-bottom:10px;">
                    <ncm:SelectNodes ID="selectNodes" runat="server" NodeSelectionText="<%$ Resources: NCMWebContent, WEBDATA_IA_07 %>" ShowSelectNodesButton="false" ShowDeleteNodeButton="false" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div style="border: #dcdbd7 1px solid; padding: 10px; background-color: #fff;">
            <table cellpadding="0" cellspacing="0" width="98%">
                <tr valign="top" align="left">
                    <td style="padding-bottom:5px;"><b><%= Resources.NCMWebContent.WEBDATA_IA_27 %></b></td>
                    <td></td>
                </tr>
                <tr valign="top" align="left">
                    <td id="groupingPanel" style="width:300px; height: 100%; min-width:320px;">
                        <div class="ElementGrouping">
                            <div class="ncm_GroupSection x-panel-header x-toolbar x-small-editor">
                                <div><%=Resources.NCMWebContent.WEBDATA_IA_31%></div>
                            </div>
                        </div>
                        <ul class="GroupItems" style="min-width:318px; min-height: 256px; border: 1px solid #E1E1E0; overflow:auto;"></ul>
                    </td>
                    <td id="scriptPanel" style="padding-left:10px; ">
                        <table cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid #e1e1e0;">
                            <tr>
                                <td style="background-color:rgb(235, 236, 237);">
                                    <table cellpadding="0" cellspacing="0" id="toolbar">
                                        <tr>
                                            <td>
                                                <div class="x-form-field-wrap x-form-file-wrap">
                                                    <asp:FileUpload ID="btnFromFile" runat="server" class="x-form-file ImgButton" onchange="__doPostBack('', '');" />
                                                    <table id="fileUploadTable" class="ImgButton">
                                                        <tr>
                                                            <td><img alt="" style="vertical-align:middle;" src="/Orion/NCM/Resources/images/icon_loadscript.gif" /></td>
                                                            <td class="ImgButton"><%=Resources.NCMWebContent.WEBDATA_VK_820 %></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td><asp:ImageMap ID="imgSeparartor1" runat="server" style="vertical-align:middle;" ImageUrl="/Orion/NCM/Resources/images/Separator.ToolBarNodes.JPG"></asp:ImageMap></td>
                                                        <td><asp:ImageButton ID="btnImgSaveScript" runat="server" style="vertical-align:middle;" ImageUrl="/Orion/NCM/Resources/images/icon_savescript.gif" OnClick="btnSaveScript_Click"  /></td>
                                                        <td><asp:LinkButton ID="btnSaveScript" CssClass="ImgButton" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_913 %>" runat="server" OnClick="btnSaveScript_Click" /></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <textarea class="lined" style="width:100%; min-height: 250px;border:none;" id="scriptBox" runat="server" clientidmode="Static"></textarea>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="padding-top:10px;padding-left:10px;">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <div style="padding-bottom:10px;">
                                        <asp:CheckBox ID="chkSaveScript" runat="server" ClientIDMode="Static" Text="<%$ Resources: NCMWebContent, WEBDATA_IA_28 %>" onclick="saveScriptClick(this.checked);" />
                                        <div id="saveScriptPanel" style="padding-left:20px;padding-top:10px;">
                                            <span id="scriptName"></span>
                                        </div>
                                    </div>

                                    <asp:CheckBox ID="chkConfigMode" runat="server" ClientIDMode="Static" Text="<%$ Resources: NCMWebContent, WEBDATA_IA_26 %>" onclick="configModeClick(this.checked);" />
                                    <span style="font-size:8pt;padding-left:20px;"><a style="color:#336699;" href="<%=LearnMoreConfigModeMeaning %>" target="_blank">&nbsp;&#0187;&nbsp;<%=Resources.NCMWebContent.WEBDATA_VK_950 %></a></span>

                                    <div id="uploadAdvPanel" style="padding-top:10px;">
                                        <asp:CheckBox ID="chkRebootDevice" runat="server" ClientIDMode="Static" onclick="rebootDeviceClick(this);" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_47 %>" />
                                    </div>
                                </td>
                                <td align="right" valign="top">
                                    <orion:LocalizableButton ID="btn" runat="server" LocalizedText="CustomText" ClientIDMode="Static" DisplayType="Primary" OnClientClick="DoNCMTransfer(); return false;" Text="<%$ Resources: NCMWebContent, WEBDATA_VM_22 %>" />
                                </td>
                            </tr>
                        </table>
                        
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content8" ContentPlaceHolderID="ncmOutsideFormPlaceHolder" Runat="Server">
</asp:Content>

