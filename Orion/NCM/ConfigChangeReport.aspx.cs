using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Globalization;
using System.Web.Script.Serialization;
using System.Text;
using System.Web;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.NCMCore;
using SolarWinds.InformationService.Contract2;
using SolarWinds.NCM.Common.Dal.ConfigTypes;
using SolarWinds.NCM.Web.Contracts;
using SolarWinds.NCM.Web.Contracts.SiteMap;

public partial class ConfigChangeReport : Page
{
    private readonly IIsWrapper isLayer;
    private readonly IMacroParser macroParser;

    private readonly SolarWinds.NCMModule.Web.Resources.Calendar calendarFrom =
        new SolarWinds.NCMModule.Web.Resources.Calendar();

    private readonly SolarWinds.NCMModule.Web.Resources.Calendar calendarTo =
        new SolarWinds.NCMModule.Web.Resources.Calendar();

    private readonly JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
    private readonly IConfigTypesProvider configTypesProvider;

    private ResourceSettings resourceSettings;
    private DateTime startTime;
    private DateTime endTime;
    private bool viewChangesOn;
    private bool showDevicesThatHadChanges;
    private Guid? nodeId = null;
    private string nodeName = string.Empty;
    private string resourceClientId = string.Empty;
    private string returnUrl;
    private IList<Guid> nodeIds = new List<Guid>();
    private readonly ILogger logger;
    private readonly Func<INcmSiteMapRenderer> rendererFactory;

    public ConfigChangeReport()
    {
        configTypesProvider = ServiceLocator.Container.Resolve<IConfigTypesProvider>();
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
        macroParser = ServiceLocator.Container.Resolve<IMacroParser>();
        logger = ServiceLocator.Container.Resolve<Func<Type, ILogger>>()(GetType());
        rendererFactory = ServiceLocator.Container.Resolve<Func<INcmSiteMapRenderer>>();
    }

    private Guid[] NcmNodeIds
    {
        get { return (Guid[]) ViewState["NCM_NodeIds"]; }
        set { ViewState["NCM_NodeIds"] = value; }
    }

    #region Override Functions

    protected override void OnInit(EventArgs e)
    {
        Page.Response.Cache.SetNoStore();

        if (Page.Form != null && ScriptManager.GetCurrent(Page) == null)
        {
            Page.Form.Controls.AddAt(0, new ScriptManager());
        }

        base.OnInit(e);
        nodeId = Request.QueryNullableGuid("NodeID");

        if (!string.IsNullOrEmpty(Request.QueryString[@"StartTime"]))
        {
            startTime = Convert.ToDateTime(Request.QueryString[@"StartTime"], CultureInfo.InvariantCulture);
            calendarFrom.TimeSpan = startTime;
        }

        if (!string.IsNullOrEmpty(Request.QueryString[@"EndTime"]))
        {
            endTime = Convert.ToDateTime(Request.QueryString[@"EndTime"], CultureInfo.InvariantCulture);
            calendarTo.TimeSpan = endTime;
        }

        if (!string.IsNullOrEmpty(Request.QueryString[@"ViewChangesOn"]))
        {
            viewChangesOn = Convert.ToBoolean(Request.QueryString[@"ViewChangesOn"]);
            chkViewChangesOn.Checked = viewChangesOn;
            ddlConfigType.Enabled = viewChangesOn;
        }

        InitializeConfigTypes(nodeId);

        var onlyShowDeviceWithChanges = Request.QueryString[@"OnlyShowDevicesThatHadChanges"];
        if (!string.IsNullOrEmpty(onlyShowDeviceWithChanges))
        {
            showDevicesThatHadChanges = Convert.ToBoolean(onlyShowDeviceWithChanges);
            chkOnlyShowDevicesThatHadChanges.Checked = showDevicesThatHadChanges;
            onlyShowDevicesThatHadChangesHolder.Visible = true;
        }
        else
        {
            onlyShowDevicesThatHadChangesHolder.Visible = false;
        }

        if (!string.IsNullOrEmpty(Request.QueryString[@"ClientID"]))
        {
            resourceClientId = Request.QueryString[@"ClientID"];
        }

        calendarFrom.ID = @"calendarFrom";
        calendarTo.ID = @"calendarTo";
        CalendarTableFrom.Controls.Add(calendarFrom);
        CalendarTableTo.Controls.Add(calendarTo);
    }

    private void InitializeConfigTypes(Guid? nodeId)
    {
        var configTypes = nodeId.HasValue
            ? configTypesProvider.GetCommon(new[] { nodeId.Value }).ToArray()
            : configTypesProvider.GetAll().ToArray();

        ddlConfigType.Items.Clear();
        foreach (var confType in configTypes)
        {
            ddlConfigType.Items.Add(new ListItem(confType.Name, confType.Name));
        }

        if (configTypes.Any())
        {
            string preselectedConfigType = Request.QueryString[@"ConfigType"];
            var selectedConfigType = !string.IsNullOrEmpty(preselectedConfigType) ? preselectedConfigType : configTypes.First().Name;
            ddlConfigType.SelectedValue = selectedConfigType; 
        }

        ddlConfigType.Enabled = chkViewChangesOn.Checked;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var prefix = !string.IsNullOrEmpty(Request.QueryString[@"Prefix"]) ? Request.QueryString[@"Prefix"] : string.Empty;
        var configId = !string.IsNullOrEmpty(Request.QueryString[@"ConfigID"]) ? Request.QueryString[@"ConfigID"] : string.Empty;
        var comparisonType = !string.IsNullOrEmpty(Request.QueryString[@"ComparisonType"]) ? Request.QueryString[@"ComparisonType"] : string.Empty;
        var configType = !string.IsNullOrEmpty(Request.QueryString[@"ConfigType"]) ? Request.QueryString[@"ConfigType"] : string.Empty;

        if (!string.IsNullOrEmpty(Request.QueryString[@"ReturnUrl"]))
        {
            returnUrl = Request.QueryString[@"ReturnUrl"];
        }

        if (!string.IsNullOrEmpty(prefix))
        {
            resourceSettings = new ResourceSettings {Prefix = prefix};
        }

        if (!string.IsNullOrEmpty(Request.QueryString[@"ConfigID"]))
        {
            tableTimeFrame.Visible = false;
            tableConfigType.Visible = false;

            if (nodeId.HasValue)
            {
                nodeIds.Add(nodeId.Value);
                nodeName = GetNodeName(nodeId.Value);
            }

            linkNodeName.Text = nodeName;
            linkNodeName.NavigateUrl = $@"/Orion/NCM/NodeDetails.aspx?NodeID={nodeId}";

            var propertyBag = new PropertyBag {{@"configId", configId}, {@"comparisonType", comparisonType}};

            if (viewChangesOn)
            {
                propertyBag.Add(@"configType", configType);
            }

            ExecuteSwqlQuery(
                $@"SELECT 
                    C.TimeStamp, 
                    S.DiffList, 
                    S.NodeID, 
                    S.ConfigTitleBefore, 
                    S.ConfigTitle AS ConfigTitleAfter, 
                    S.ConfigTypeBefore, 
                    S.ConfigType AS ConfigTypeAfter, 
                    S.DiffWidth, 
                    S.RtnMessageBefore, 
                    S.RtnMessageAfter
            FROM Cirrus.ComparisonCache AS C JOIN Cirrus.CacheDiffResults AS S ON C.CacheID=S.CacheID 
            WHERE S.ConfigID=@configId 
                AND S.ComparisonType=@comparisonType {(viewChangesOn ? $@"AND S.ConfigType=@configType" : string.Empty)}",
                propertyBag);
        }
        else
        {
            labelReportTitle.Text = string.Format(Resources.NCMWebContent.WEBDATA_IC_03, startTime, endTime);

            if (nodeId.HasValue)
            {
                nodeIds.Add(nodeId.Value);
                nodeName = GetNodeName(nodeId.Value);

                linkNodeName.Text = nodeName;
                linkNodeName.NavigateUrl = returnUrl;

                var query =
                $@"SELECT 
                    C.TimeStamp, 
                    S.DiffList, 
                    S.NodeID, 
                    S.ConfigTitleBefore, 
                    S.ConfigTitle AS ConfigTitleAfter, 
                    S.ConfigTypeBefore, 
                    S.ConfigType AS ConfigTypeAfter, 
                    S.DiffWidth, 
                    S.RtnMessageBefore, 
                    S.RtnMessageAfter 
                FROM Cirrus.ComparisonCache AS C JOIN Cirrus.CacheDiffResults AS S ON C.CacheID=S.CacheID 
                WHERE ToUtc(C.TimeStamp)>=@startTime_ 
                    AND ToUtc(C.TimeStamp)<=@endTime_ 
                    AND S.ComparisonType=@comparisonType {(viewChangesOn ? $@"AND S.ConfigType=@configType" : string.Empty)} 
                    AND S.NodeID=@nodeId 
                ORDER BY C.CacheID DESC";

                using (var proxy = isLayer.GetProxy())
                {
                    var propertyBag = new PropertyBag
                    {
                        {@"startTime_", startTime.GetAsLocal()},
                        {@"endTime_", endTime.GetAsLocal()},
                        {@"nodeId", nodeId},
                        {@"comparisonType", viewChangesOn ? 4 : 3}
                    };

                    if (viewChangesOn)
                    {
                        propertyBag.Add(@"configType", configType);
                    }

                    var dt = proxy.Query(query, propertyBag);

                    if (dt.Rows.Count >= 0)
                    {
                        FillTable(dt);
                    }
                }
            }
            else
            {
                tableNodeName.Visible = false;

                var postData = Server.UrlDecode(Page.Request.Form[@"ncmNodeIds"]);
                if (!string.IsNullOrEmpty(postData))
                {
                    var ids = new List<Guid>();
                    var idsRaw = jsSerializer.Deserialize<string[]>(Server.UrlDecode(postData));
                    foreach (var idRaw in idsRaw)
                    {
                        Guid id;
                        if (Guid.TryParse(idRaw, out id))
                        {
                            ids.Add(id);
                        }
                    }

                    NcmNodeIds = ids.ToArray();
                }

                if (NcmNodeIds?.Length > 0)
                {
                    nodeIds = new List<Guid>(NcmNodeIds);
                    var swqlNodeIDs = string.Join(@",", NcmNodeIds.ToList().Select(item => $@"'{item}'"));

                    var query =
                    $@"SELECT 
                        C.TimeStamp, 
                        S.DiffList, 
                        S.NodeID, 
                        S.ConfigTitleBefore, 
                        S.ConfigTitle AS ConfigTitleAfter, 
                        S.ConfigTypeBefore, 
                        S.ConfigType AS ConfigTypeAfter, 
                        S.DiffWidth, 
                        S.RtnMessageBefore, 
                        S.RtnMessageAfter
                    FROM Cirrus.ComparisonCache AS C JOIN Cirrus.CacheDiffResults AS S ON C.CacheID=S.CacheID 
                    WHERE ToUtc(C.TimeStamp)>=@startTime_ 
                        AND ToUtc(C.TimeStamp)<=@endTime_ 
                        AND S.ComparisonType={(viewChangesOn ? 4 : 3)} {(viewChangesOn ? $@"AND S.ConfigType='{configType}'" : string.Empty)} 
                        AND S.NodeID IN ({swqlNodeIDs}) 
                    ORDER BY C.CacheID DESC";

                    using (var proxy = isLayer.GetProxy())
                    {
                        var dt = proxy.Query(query, new PropertyBag
                        {
                            {@"startTime_", startTime.GetAsLocal()},
                            {@"endTime_", endTime.GetAsLocal()}
                        });

                        if (dt.Rows.Count >= 0)
                        {
                            FillTable(dt);
                        }
                    }
                }
            }
        }
    }

    protected void chkViewChangesOn_OnCheckedChanged(object sender, EventArgs e)
    {
        ddlConfigType.Enabled = chkViewChangesOn.Checked;
    }

    protected void ViewChangesClick(object sender, EventArgs e)
    {
        var error = false;
        string errorMsg;

        try
        {
            errorMsg = ValidateDate(calendarFrom.TimeSpan);
            if (errorMsg == string.Empty)
            {
                DateFromValidatorMsg.InnerText = string.Empty;
            }
            else
            {
                DateFromValidatorMsg.InnerText = errorMsg;
                error = true;
            }
        }
        catch
        {
            DateFromValidatorMsg.InnerText = Resources.NCMWebContent.WEBDATA_IC_04;
            error = true;
        }

        try
        {
            errorMsg = ValidateDate(calendarTo.TimeSpan);
            if (errorMsg == string.Empty)
            {
                DateToValidatorMsg.InnerText = string.Empty;
            }
            else
            {
                DateToValidatorMsg.InnerText = errorMsg;
                error = true;
            }
        }
        catch
        {
            DateToValidatorMsg.InnerText = Resources.NCMWebContent.WEBDATA_IC_04;
            error = true;
        }

        if (!error)
        {
            if (!string.IsNullOrEmpty(Request.QueryString[@"Prefix"]))
            {
                if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
                {
                    if (resourceSettings == null)
                    {
                        resourceSettings = new ResourceSettings {Prefix = Request.QueryString[@"Prefix"]};
                    }

                    resourceSettings.SetCustomProperty("DateTimeFrom",
                        calendarFrom.TimeSpan.ToString(CultureInfo.InvariantCulture));
                    resourceSettings.SetCustomProperty("ViewChangesOn", chkViewChangesOn.Checked.ToString());
                    resourceSettings.SetCustomProperty("ConfigType", ddlConfigType.SelectedValue);
                    resourceSettings.SetCustomProperty("OnlyShowDevicesThatHadChanges",
                        chkOnlyShowDevicesThatHadChanges.Checked.ToString());
                }
            }

            var queryParams= new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(Request.QueryString[@"printable"]))
            {
                queryParams.Add(@"printable",true.ToString(CultureInfo.InvariantCulture));
            }

            queryParams.Add(@"StartTime", calendarFrom.TimeSpan.ToString(CultureInfo.InvariantCulture));
            queryParams.Add(@"EndTime",calendarTo.TimeSpan.ToString(CultureInfo.InvariantCulture));
            queryParams.Add(@"ViewChangesOn", chkViewChangesOn.Checked.ToString(CultureInfo.InvariantCulture));
            queryParams.Add(@"ConfigType", ddlConfigType.SelectedValue);
            if (!string.IsNullOrEmpty(Request.QueryString[@"OnlyShowDevicesThatHadChanges"]))
            {
                queryParams.Add(@"OnlyShowDevicesThatHadChanges", chkOnlyShowDevicesThatHadChanges.Checked.ToString(CultureInfo.InvariantCulture));
            }
            if (nodeId.HasValue)
            {
                queryParams.Add(@"NodeID", nodeId.ToString());
            }
            queryParams.Add(@"ReturnUrl", returnUrl);
            if (!string.IsNullOrEmpty(Request.QueryString[@"Prefix"]))
            {
                queryParams.Add(@"Prefix", Request.QueryString[@"Prefix"]);
            }
            if (!string.IsNullOrEmpty(resourceClientId))
            {
                queryParams.Add(@"ClientID", resourceClientId);
            }

            var url =
                $@"/Orion/NCM/ConfigChangeReport.aspx{QueryStringBuilder.ToQueryString(queryParams)}";

            if (NcmNodeIds != null)
            {
                Response.Clear();
                var sb = new StringBuilder();
                sb.Append(@"<html>");
                sb.Append(@"<body onload='document.forms[""form""].submit()'>");
                sb.AppendFormat(@"<form name='form' action='{0}' method='post'>", url);
                sb.AppendFormat(@"<input type='hidden' name='ncmNodeIds' value='{0}'>",
                    jsSerializer.Serialize(NcmNodeIds));
                sb.Append(@"</form>");
                sb.Append(@"</body>");
                sb.Append(@"</html>");
                Response.Write(sb.ToString());
                Response.End();
            }
            else
            {
                Response.Redirect(url);
            }
        }
    }

    #endregion

    #region Private Functions

    /// <summary>
    /// Executes SWQL query
    /// </summary>
    /// <param name="swqlQuery"></param>
    private void ExecuteSwqlQuery(string swqlQuery, PropertyBag propertyBag)
    {
        using (var proxy = isLayer.GetProxy())
        {
            var dt = proxy.Query(swqlQuery, propertyBag);
            if (dt.Rows.Count >= 0)
            {
                FillTable(dt);
            }
        }
    }

    private string GetNodeName(Guid nodeId)
    {
        return macroParser.ParseMacro(nodeId, @"${NodeCaption}");
    }

    private void FillTable(DataTable diffListTable)
    {
        TableRow tableRow;
        TableCell tableCell;

        var hasChanges = false;
        var hasCachedConfigs = false;

        for (var nodesCount = 0; nodesCount < nodeIds.Count; nodesCount++)
        {
            var dataRows = diffListTable.Select($"NodeID='{nodeIds[nodesCount]}'");
            var deviceTableRows = new List<TableRow>();
            var deviceHasChanges = false;
            if (!nodeId.HasValue)
            {
                // added node name in table
                nodeName = GetNodeName(nodeIds[nodesCount]);
                tableRow = FillTableRow(nodeName, string.Empty, Color.White, Color.White, string.Empty, true, true);
                deviceTableRows.Add(tableRow);
            }

            if (dataRows.Length > 0)
            {
                hasCachedConfigs = true;
                // added 'BEFORE' and 'AFTER' header
                tableRow = FillTableRow(Resources.NCMWebContent.WEBDATA_IC_12, Resources.NCMWebContent.WEBDATA_IC_13,
                    Color.FromArgb(226, 225, 212), Color.FromArgb(226, 225, 212), string.Empty, true, false);
                deviceTableRows.Add(tableRow);

                for (var row = 0; row < dataRows.Length; row++)
                {
                    var diffList = dataRows[row]["DiffList"].ToString();
                    if (!string.IsNullOrEmpty(diffList))
                    {
                        hasChanges = true;
                        deviceHasChanges = true;

                        var adds = 0;
                        var deletes = 0;
                        var changes = 0;

                        var width = Convert.ToInt32(dataRows[row]["DiffWidth"]);

                        // added TimeStamp header
                        tableRow = FillTableRow(string.Empty,
                            DetermineAgeGroup(Convert.ToDateTime(dataRows[row]["TimeStamp"])),
                            Color.FromArgb(226, 225, 212), Color.FromArgb(226, 225, 212), string.Empty, false, false);
                        tableRow.ID = $@"tableRow_{nodesCount}_{row}";
                        deviceTableRows.Add(tableRow);

                        // added before and after config titles
                        var configTitleBefore = Convert.ToString(dataRows[row]["ConfigTitleBefore"]);
                        var configTitleAfter = Convert.ToString(dataRows[row]["ConfigTitleAfter"]);
                        if (!string.IsNullOrEmpty(configTitleBefore) && !string.IsNullOrEmpty(configTitleAfter))
                        {
                            tableRow = FillTableRow(
                                string.Format(Resources.NCMWebContent.WEBDATA_VK_912, configTitleBefore),
                                string.Format(Resources.NCMWebContent.WEBDATA_VK_912, configTitleAfter),
                                Color.FromArgb(226, 225, 212), Color.FromArgb(226, 225, 212), string.Empty, false,
                                false);
                            deviceTableRows.Add(tableRow);
                        }

                        // added before and after rtn messages
                        var rtnMessageBefore = Convert.ToString(dataRows[row]["RtnMessageBefore"],
                            CultureInfo.InvariantCulture);
                        var rtnMessageAfter = Convert.ToString(dataRows[row]["RtnMessageAfter"],
                            CultureInfo.InvariantCulture);
                        if (!string.IsNullOrEmpty(rtnMessageBefore) || !string.IsNullOrEmpty(rtnMessageAfter))
                        {
                            tableRow = FillTableRow(rtnMessageBefore, rtnMessageAfter, Color.FromArgb(226, 225, 212),
                                Color.FromArgb(226, 225, 212), string.Empty, false, false);
                            deviceTableRows.Add(tableRow);
                        }

                        var arrDiffList = diffList.Split('\n');

                        for (var i = 0; i < arrDiffList.Length; i++)
                        {
                            if (arrDiffList[i].Trim().Length > 0 && arrDiffList[i].Trim() != @"N/A")
                            {
                                var diffChar = arrDiffList[i].Substring(width / 2 - 2, 2).Trim();
                                var containsInterface = arrDiffList[i + 1].Contains(@"Interface");


                                var leftText = arrDiffList[i].Substring(0, width / 2 - 2).Trim();
                                var rightText = arrDiffList[i].Substring(width / 2).Trim();

                                switch (diffChar)
                                {
                                    case "|":
                                        if (containsInterface)
                                        {
                                            tableRow = FillTableRow(arrDiffList[i + 1].Trim(),
                                                arrDiffList[i + 1].Trim(), Color.Khaki, Color.Khaki, diffChar, false,
                                                false);
                                            deviceTableRows.Add(tableRow);
                                        }

                                        changes++;
                                        tableRow = FillTableRow(leftText, rightText, Color.Khaki, Color.Khaki, diffChar,
                                            false, false);
                                        deviceTableRows.Add(tableRow);
                                        break;
                                    case "<":
                                        if (containsInterface)
                                        {
                                            tableRow = FillTableRow(arrDiffList[i + 1].Trim(), string.Empty,
                                                Color.FromName(@"#a0eda0"), Color.FromName(@"#f7adaf"), diffChar, false,
                                                false);
                                            deviceTableRows.Add(tableRow);
                                        }

                                        deletes++;
                                        tableRow = FillTableRow(leftText, rightText, Color.FromName(@"#a0eda0"),
                                            Color.FromName(@"#f7adaf"), diffChar, false, false);
                                        deviceTableRows.Add(tableRow);
                                        break;
                                    case ">":
                                        if (containsInterface)
                                        {
                                            tableRow = FillTableRow(string.Empty, arrDiffList[i + 1].Trim(),
                                                Color.FromName(@"#f7adaf"), Color.FromName(@"#a0eda0"), diffChar, false,
                                                false);
                                            deviceTableRows.Add(tableRow);
                                        }

                                        adds++;
                                        tableRow = FillTableRow(leftText, rightText, Color.FromName(@"#f7adaf"),
                                            Color.FromName(@"#a0eda0"), diffChar, false, false);
                                        deviceTableRows.Add(tableRow);
                                        break;
                                    case "=":
                                        tableRow = FillTableRow(leftText, rightText, Color.White, Color.White, diffChar,
                                            false, false);
                                        deviceTableRows.Add(tableRow);
                                        break;
                                    case "*":
                                        tableRow = FillUnchangedLinesRow(leftText);
                                        deviceTableRows.Add(tableRow);
                                        break;
                                    default:
                                        //do nothing
                                        break;
                                }

                                i++;
                            }
                        }

                        // added in 'adds, deletes and changes' results 
                        var ctrl = tableDiffResults.FindControl($@"tableRow_{nodesCount}_{row}") as TableRow;
                        if (ctrl != null)
                        {
                            ctrl.Cells[0].Text = string.Format(Resources.NCMWebContent.WEBDATA_IC_14, adds, deletes,
                                changes);
                        }
                    }
                    else
                    {
                        if (!showDevicesThatHadChanges)
                        {
                            // added TimeStamp header
                            tableRow = FillTableRow(string.Empty,
                                DetermineAgeGroup(Convert.ToDateTime(dataRows[row]["TimeStamp"])),
                                Color.FromArgb(226, 225, 212), Color.FromArgb(226, 225, 212), string.Empty, false,
                                false);
                            tableRow.ID = $@"tableRow_{nodesCount}_{row}";
                            deviceTableRows.Add(tableRow);

                            // added before and after config titles
                            var configTitleBefore = Convert.ToString(dataRows[row]["ConfigTitleBefore"]);
                            var configTitleAfter = Convert.ToString(dataRows[row]["ConfigTitleAfter"]);
                            if (!string.IsNullOrEmpty(configTitleBefore) && !string.IsNullOrEmpty(configTitleAfter))
                            {
                                tableRow = FillTableRow(
                                    string.Format(Resources.NCMWebContent.WEBDATA_VK_912, configTitleBefore),
                                    string.Format(Resources.NCMWebContent.WEBDATA_VK_912, configTitleAfter),
                                    Color.FromArgb(226, 225, 212), Color.FromArgb(226, 225, 212), string.Empty, false,
                                    false);
                                deviceTableRows.Add(tableRow);
                            }

                            // no changes in given TimeStamp perion
                            tableRow = FillTableRow(Resources.NCMWebContent.WEBDATA_IC_17, string.Empty, Color.White,
                                Color.White, string.Empty, false, false);
                            deviceTableRows.Add(tableRow);
                        }
                    }
                }
            }
            else
            {
                // no cached config changes in TimeStamp period
                tableRow = new TableRow {Width = Unit.Percentage(100)};

                tableCell = new TableCell
                {
                    Text = Resources.NCMWebContent.WEBDATA_IC_18,
                    CssClass = @"ReportDetails",
                    HorizontalAlign = HorizontalAlign.Left,
                    ForeColor = Color.Red,
                    BackColor = Color.White,
                    Height = Unit.Pixel(20),
                    Width = Unit.Percentage(100),
                    ColumnSpan = 3
                };

                tableCell.Style.Add(@"font-weight", @"bold");
                tableCell.Style.Add(@"padding-left", Unit.Pixel(10).ToString());
                tableCell.Style.Add(@"border-color", @"dfded7");
                tableRow.Cells.Add(tableCell);
                deviceTableRows.Add(tableRow);
            }

            if (deviceHasChanges || !showDevicesThatHadChanges)
            {
                tableDiffResults.Rows.AddRange(deviceTableRows.ToArray());
            }
        }

        if (nodeIds.Count == 0)
        {
            // no cached config changes in TimeStamp period
            tableRow = new TableRow {Width = Unit.Percentage(100)};

            tableCell = new TableCell
            {
                Text = Resources.NCMWebContent.WEBDATA_IC_18,
                CssClass = @"ReportDetails",
                HorizontalAlign = HorizontalAlign.Left,
                ForeColor = Color.Red,
                BackColor = Color.White,
                Height = Unit.Pixel(20),
                Width = Unit.Percentage(100),
                ColumnSpan = 3
            };

            tableCell.Style.Add(@"font-weight", @"bold");
            tableCell.Style.Add(@"padding-left", Unit.Pixel(10).ToString());
            tableCell.Style.Add(@"border-color", @"dfded7");
            tableRow.Cells.Add(tableCell);
            tableDiffResults.Rows.Add(tableRow);
        }

        // If there are no cached configs or there is no changed configuration
        if (!hasChanges && !hasCachedConfigs && !showDevicesThatHadChanges)
        {
            tableDiffResults.Rows.Clear();

            tableRow = new TableRow {Width = Unit.Percentage(100)};

            tableCell = new TableCell
            {
                ID = @"NoChanges",
                Text = Resources.NCMWebContent.WEBDATA_IC_17,
                CssClass = @"ReportDetails",
                HorizontalAlign = HorizontalAlign.Left,
                ForeColor = Color.Black,
                BackColor = Color.White,
                Height = Unit.Pixel(20),
                Width = Unit.Percentage(100),
                ColumnSpan = 3
            };
            tableCell.Style.Add(@"font-weight", @"bold");
            tableCell.Style.Add(@"padding-left", Unit.Pixel(10).ToString());
            tableCell.Style.Add(@"border-color", @"dfded7");
            tableRow.Cells.Add(tableCell);
            tableDiffResults.Rows.Add(tableRow);
        }

        SetTableBorders();
    }

    private static TableRow FillTableRow(string leftText, string rightText, Color leftColor, Color rightColor,
        string diffChar, bool isCaption, bool isNodeName)
    {
        var image = new System.Web.UI.WebControls.Image();
        var tableRow = new TableRow {Width = Unit.Percentage(100)};
        TableCell tableCell;

        switch (diffChar)
        {
            case "|":
                image.ImageUrl = @"/Orion/NCM/Resources/images/ConfigChangeReport.Change.gif";
                image.ImageAlign = ImageAlign.Middle;

                tableCell = new TableCell
                {
                    HorizontalAlign = HorizontalAlign.Center,
                    BackColor = leftColor,
                    Height = Unit.Pixel(20),
                    Width = Unit.Pixel(30)
                };
                tableCell.Style.Add(@"padding-left", Unit.Pixel(10).ToString());
                tableCell.Style.Add(@"border-left", @"#dfded7 1px solid");
                tableCell.Controls.Add(image);
                tableRow.Cells.Add(tableCell);
                break;
            case "<":
                image.ImageUrl = @"/Orion/NCM/Resources/images/icon_delete.png";
                image.ImageAlign = ImageAlign.Middle;

                tableCell = new TableCell
                {
                    HorizontalAlign = HorizontalAlign.Center,
                    BackColor = leftColor,
                    Height = Unit.Pixel(20),
                    Width = Unit.Pixel(30)
                };
                tableCell.Style.Add(@"padding-left", Unit.Pixel(10).ToString());
                tableCell.Style.Add(@"border-left", @"#dfded7 1px solid");
                tableCell.Controls.Add(image);
                tableRow.Cells.Add(tableCell);
                break;
            case ">":
                image.ImageUrl = @"/Orion/NCM/Resources/images/ConfigChangeReport.Add.gif";
                image.ImageAlign = ImageAlign.Middle;

                tableCell = new TableCell
                {
                    HorizontalAlign = HorizontalAlign.Center,
                    BackColor = leftColor,
                    Height = Unit.Pixel(20),
                    Width = Unit.Pixel(30)
                };
                tableCell.Style.Add(@"padding-left", Unit.Pixel(10).ToString());
                tableCell.Style.Add(@"border-left", @"#dfded7 1px solid");
                tableCell.Controls.Add(image);
                tableRow.Cells.Add(tableCell);
                break;
            case "=":
                tableCell = new TableCell
                {
                    HorizontalAlign = HorizontalAlign.Center,
                    BackColor = leftColor,
                    Height = Unit.Pixel(20),
                    Width = Unit.Pixel(30)
                };
                tableCell.Style.Add(@"padding-left", Unit.Pixel(10).ToString());
                tableCell.Style.Add(@"border-left", @"#dfded7 1px solid");
                tableRow.Cells.Add(tableCell);
                break;
            default:
                if (isNodeName)
                {
                    tableCell = new TableCell
                    {
                        Font =
                        {
                            Size = FontUnit.Point(12),
                            Bold = true
                        },
                        Text = HttpUtility.HtmlEncode(leftText),
                        CssClass = @"ReportDetails",
                        HorizontalAlign = HorizontalAlign.Left,
                        BackColor = leftColor,
                        Height = Unit.Pixel(20),
                        Width = Unit.Percentage(100),
                        ColumnSpan = 3
                    };

                    tableCell.Style.Add(@"padding-top", Unit.Pixel(15).ToString());
                    tableRow.Cells.Add(tableCell);
                }

                break;
        }

        if (!isNodeName)
        {
            tableCell = new TableCell
            {
                Text = HttpUtility.HtmlEncode(leftText),
                CssClass = @"ReportDetails",
                HorizontalAlign = HorizontalAlign.Left,
                BackColor = leftColor,
                Height = Unit.Pixel(20),
                Width = Unit.Percentage(50)
            };

            if (isCaption)
            {
                tableCell.Style.Add(@"font-weight", @"bold");
                tableCell.Style.Add(@"padding-left", Unit.Pixel(10).ToString());
                tableCell.Style.Add(@"border-top", @"#dfded7 1px solid");
                tableCell.Style.Add(@"border-left", @"#dfded7 1px solid");
                tableCell.ColumnSpan = 2;
            }

            if (leftColor == Color.FromArgb(226, 225, 212))
            {
                tableCell.Style.Add(@"border-right", @"white 1px solid");
                tableCell.Style.Add(@"padding-left", Unit.Pixel(10).ToString());
                tableCell.Style.Add(@"border-left", @"#dfded7 1px solid");
                tableCell.ColumnSpan = 2;
            }

            if (leftText == Resources.NCMWebContent.WEBDATA_IC_17)
            {
                tableCell.Style.Add(@"padding-left", Unit.Pixel(10).ToString());
                tableCell.Style.Add(@"border-left", @"#dfded7 1px solid");
                tableCell.ColumnSpan = 2;
            }

            tableRow.Cells.Add(tableCell);

            tableCell = new TableCell
            {
                Text = HttpUtility.HtmlEncode(rightText),
                CssClass = @"ReportDetails",
                HorizontalAlign = HorizontalAlign.Left,
                BackColor = rightColor,
                Height = Unit.Pixel(20),
                Width = Unit.Percentage(50)
            };
            if (isCaption)
            {
                tableCell.Style.Add(@"border-top", @"#dfded7 1px solid");
                tableCell.Style.Add(@"font-weight", @"bold");
            }

            tableCell.Style.Add(@"padding-left", Unit.Pixel(10).ToString());
            tableCell.Style.Add(@"border-right", @"#dfded7 1px solid");
            tableRow.Cells.Add(tableCell);
        }
        else
        {
            // must be the last. Used for drawing top and bottom borders
            tableCell = new TableCell
            {
                Text = @"node",
                Visible = false
            };
            tableRow.Cells.Add(tableCell);
        }

        return tableRow;
    }

    private static TableRow FillUnchangedLinesRow(string text)
    {
        var tableRow = new TableRow {Width = Unit.Percentage(100)};
        var tableCell = new TableCell
        {
            Text = text,
            HorizontalAlign = HorizontalAlign.Center,
            BackColor = Color.White,
            Height = Unit.Pixel(20),
            Width = Unit.Pixel(30),
            ColumnSpan = 3
        };
        tableCell.Style.Add(@"border-left", @"#dfded7 1px solid");
        tableCell.Style.Add(@"border-right", @"#dfded7 1px solid");
        tableRow.Cells.Add(tableCell);
        return tableRow;
    }

    private void SetTableBorders()
    {
        if (tableDiffResults.Rows.Count <= 0)
        {
            return;
        }

        int i;
        for (i = 1; i < tableDiffResults.Rows.Count; i++)
        {
            if (tableDiffResults.Rows[i].Cells[tableDiffResults.Rows[i].Cells.Count - 1].Text
                    .Equals(@"node", StringComparison.InvariantCultureIgnoreCase) &&
                !tableDiffResults.Rows[i - 1].Cells[tableDiffResults.Rows[i - 1].Cells.Count - 1].Text
                    .Equals(Resources.NCMWebContent.WEBDATA_IC_18, StringComparison.InvariantCultureIgnoreCase) &&
                !tableDiffResults.Rows[i - 1].Cells[tableDiffResults.Rows[i - 1].Cells.Count - 1].Text
                    .Equals(@"node", StringComparison.InvariantCultureIgnoreCase))
            {
                foreach (TableCell cell in tableDiffResults.Rows[i - 1].Cells)
                {
                    cell.Style.Add(@"border-bottom", @"#dfded7 1px solid");
                }
            }

            if (i + 1 < tableDiffResults.Rows.Count)
            {
                if (tableDiffResults.Rows[i].Cells[tableDiffResults.Rows[i].Cells.Count - 1].Text
                        .Equals(@"node", StringComparison.InvariantCultureIgnoreCase) &&
                    !tableDiffResults.Rows[i + 1].Cells[tableDiffResults.Rows[i + 1].Cells.Count - 1].Text
                        .Equals(Resources.NCMWebContent.WEBDATA_IC_18, StringComparison.InvariantCultureIgnoreCase) &&
                    !tableDiffResults.Rows[i + 1].Cells[tableDiffResults.Rows[i + 1].Cells.Count - 1].Text
                        .Equals(@"node", StringComparison.InvariantCultureIgnoreCase))
                {
                    tableDiffResults.Rows[i].Cells[0].Style.Add(@"border-bottom", @"#dfded 1px solid");
                }
            }
        }

        foreach (TableCell cell in tableDiffResults.Rows[i - 1].Cells)
        {
            if (cell.Text.Equals(Resources.NCMWebContent.WEBDATA_IC_17, StringComparison.InvariantCultureIgnoreCase) &&
                !string.IsNullOrEmpty(cell.ID))
            {
                continue;
            }

            if (!cell.Text.Equals(Resources.NCMWebContent.WEBDATA_IC_18, StringComparison.InvariantCultureIgnoreCase))
            {
                cell.Style.Add(@"border-bottom", @"#dfded7 1px solid");
            }      
        }
    }

    /// <summary>
    /// Convert TimeStamp into date format 'Day - MM/dd/yyyy'
    /// </summary>
    /// <param name="date"></param>
    /// <returns></returns>
    private static string DetermineAgeGroup(DateTime date)
    {
        var days = DateTimeManager.DateDiff(DateTimeManager.DateInterval.Day, date, DateTime.Now);
        switch (days)
        {
            case 0:
                return $@"{Resources.NCMWebContent.WEBDATA_IC_20} - {date:g}";
            case 1:
                return $@"{Resources.NCMWebContent.WEBDATA_IC_21} - {date:g}";
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                return $@"{DateTimeManager.GetLocalizedDayName(date)} - {date:g}";
            default:
                var weeks = DateTimeManager.DateDiff(DateTimeManager.DateInterval.Week, date, DateTime.Now);
                switch (weeks)
                {
                    case 0:
                        return $@"{Resources.NCMWebContent.WEBDATA_IC_22} - {date:g}";
                    case 1:
                        return $@"{Resources.NCMWebContent.WEBDATA_IC_23} - {date:g}";
                    case 2:
                        return $@"{Resources.NCMWebContent.WEBDATA_IC_24} - {date:g}";
                    default:
                        var months = DateTimeManager.DateDiff(DateTimeManager.DateInterval.Month, date, DateTime.Now);
                        switch (months)
                        {
                            case 0:
                                return $@"{Resources.NCMWebContent.WEBDATA_IC_25} - {date:g}";
                            case 1:
                                return $@"{Resources.NCMWebContent.WEBDATA_IC_26} - {date:g}";
                            case 2:
                                return $@"{Resources.NCMWebContent.WEBDATA_IC_27} - {date:g}";
                            default:
                                return
                                    $@"{CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(date.Month)} {date.Year} - {date:g}";
                        }
                }
        }
    }

    public string ValidateDate(DateTime date)
    {
        if (date > new DateTime(1979, 12, 31) && date < new DateTime(2101, 1, 1))
        {
            return string.Empty;
        }

        return Resources.NCMWebContent.WEBDATA_IC_04;
    }

    protected void SiteMapPath_OnInit(object sender, EventArgs e)
    {
        try
        {
            var renderer = rendererFactory();

            if (!string.IsNullOrEmpty(Request.QueryString[@"NodeID"]))
            {
                nodeId = Guid.Parse(Request.QueryString[@"NodeID"]);
                renderer.SetUpData(new KeyValuePair<string, string>(@"NodeID", Request.QueryString[@"NodeID"]));
            }
            else
            {
                renderer.SetUpData(null);
            }

            NodeSiteMapPath.SetUpRenderer(renderer);
        }
        catch (Exception ex)
        {
            logger.Error("Error initializing the site map.", ex);
            throw;
        }
    }

    #endregion

    public class DateTimeManager
    {
        public enum DateInterval
        {
            Second,
            Minute,
            Hour,
            Day,
            Week,
            Month,
            Year
        }

        public static long DateDiff(DateInterval interval, DateTime startDate, DateTime endDate)
        {
            long dateDiffValue = 0;
            TimeSpan ts;

            if (interval == DateInterval.Year || interval == DateInterval.Week ||
                interval == DateInterval.Month || interval == DateInterval.Day)
            {
                var startDateString = startDate.ToShortDateString();
                var endDateString = endDate.ToShortDateString();
                ts = Convert.ToDateTime(endDateString) - Convert.ToDateTime(startDateString);
            }
            else
            {
                ts = endDate - startDate;
            }

            switch (interval)
            {
                case DateInterval.Year:
                    dateDiffValue = endDate.Year - startDate.Year;
                    break;
                case DateInterval.Month:
                    dateDiffValue = (endDate.Month - startDate.Month) + (12 * (endDate.Year - startDate.Year));
                    break;
                case DateInterval.Week:
                    dateDiffValue = Fix(ts.TotalDays) / 7;
                    break;
                case DateInterval.Day:
                    dateDiffValue = Fix(ts.TotalDays);
                    break;
                case DateInterval.Hour:
                    dateDiffValue = Fix(ts.TotalHours);
                    break;
                case DateInterval.Minute:
                    dateDiffValue = Fix(ts.TotalMinutes);
                    break;
                case DateInterval.Second:
                    dateDiffValue = Fix(ts.TotalSeconds);
                    break;
                default:
                    //do nothing
                    break;
            }

            return dateDiffValue;
        }

        /// <summary>
        /// Gets localized day name.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        public static string GetLocalizedDayName(DateTime date)
        {
            int dayOfWeek = Convert.ToInt16(date.DayOfWeek);
            return CultureInfo.CurrentCulture.DateTimeFormat.DayNames[dayOfWeek];
        }

        private static long Fix(double number)
        {
            if (number >= 0)
            {
                return (long) Math.Floor(number);
            }

            return (long) Math.Ceiling(number);
        }
    }
}