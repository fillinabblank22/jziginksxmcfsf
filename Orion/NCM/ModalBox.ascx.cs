using System;
using System.Web.UI;

public partial class ModalBox : System.Web.UI.UserControl
{
    public override string ClientID
    {
        get
        {
            return this.divContainer.ClientID;
        }
    }

    private ITemplate _dialogContents;

    [PersistenceMode(PersistenceMode.InnerProperty)]
    [TemplateInstance(TemplateInstance.Single)]
    public ITemplate DialogContents
    {
        get
        {
            return _dialogContents;
        }
        set
        {
            this._dialogContents = value;
        }
    }


    public int Width
    {
        get
        {
            string widthStr = this.divDialog.Style[HtmlTextWriterStyle.Width];
            if (string.IsNullOrEmpty(widthStr))
            {
                return 350;
            }
            else
            {
                return Convert.ToInt32(widthStr.Substring(0, widthStr.Length - 2));
            }
        }
        set
        {
            this.divDialog.Style[HtmlTextWriterStyle.Width] = value.ToString() + @"px";
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.iFrame.Attributes[@"style"] = MASK_STYLE;

        if (null != this.DialogContents)
        {
            this.DialogContents.InstantiateIn(this.phContents);
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), @"key_setModalBox", @"if(typeof(setModalBox) != 'undefined'){ setModalBox();}", true);
    }

    private void CreateContents()
    {

    }

    public void Hide()
    {
        this.divContainer.Attributes[@"style"] = @"display: none;";
        this.upModalBox.Update();
    }

    public void Show()
    {
        this.divContainer.Attributes[@"style"] = "";
        this.upModalBox.Update();
    }

    private const string MASK_STYLE = @"
    position: fixed;
    width: 100%;
 	height: 100%; 
    top:0px; 
	bottom:0px; 
	left:0px;
	right:0px;
	overflow:hidden; 
	padding:0; 
	margin:0; 
    background-color: Black;
    opacity: 0.5;
    -moz-opacity: 0.5;
    filter: alpha(opacity=50);
    z-index: 1000;
    ";
}
