<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMEditMasterPage.master" AutoEventWireup="true" CodeFile="ExecuteRemediationScript.aspx.cs" Inherits="Orion_NCM_Resources_PolicyReports_ExecuteRemediationScript" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Orion/NCM/Controls/UniversalNodesPicker.ascx" TagName="UniversalNodesPicker" TagPrefix="ncm" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/NCM/Resources/Jobs/Controls/LightLoadScriptControl.ascx" TagName="LoadScript" TagPrefix="ncm" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ncmPageTitlePlaceHolder">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ncmHelpIconPlaceHolder">
    <orion:IconHelpButton ID="helpBtn" runat="server" HelpUrlFragment="OrionNCMWebPolicyReportsExecuteScript" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" runat="server">
    <ncm:DropDownMapPath ID="NodeSiteMapPath" Provider="NCMSitemapProvider" runat="server"  OnInit="SiteMapPath_OnInit">
        <RootNodeTemplate>
            <a href="<%# Eval(@"url") %>" ><u> <%# Eval(@"title") %></u> </a>
        </RootNodeTemplate>
        <CurrentNodeTemplate>
	        <a href="<%# Eval(@"url") %>" ><%# Eval(@"title") %> </a>
        </CurrentNodeTemplate>
    </ncm:DropDownMapPath>
</asp:Content>

<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="ncmHeadPlaceHolder">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <script type="text/javascript" src="/Orion/NCM/JavaScript/main.js"></script>
    <script type="text/javascript">
        var showWindow = function (title, iframeContainerId, callback) {
            var window = new Ext.Window({
                id: 'window',
                layout: 'anchor',
                width: '60%',
                minWidth: 800,
                height: 600,
                minHeight: 400,
                closeAction: 'close',
                title: title,
                modal: true,
                resizable: true,
                maximizable: true,
                plain: true,
                html: String.format('<iframe name="{0}" id="{0}" frameborder="0" style="overflow:auto;" class="x-panel-body" width="100%" height="100%" />', iframeContainerId),
                buttons: [{
                    text: '<%= Resources.NCMWebContent.WEBDATA_VK_425%>',
                    handler: function () {
                        window.close();
                    }
                }],
                listeners: {
                    show: function (that) {
                        that.alignTo(document.body, "c-c");
                    }
                }
            }).show(null, callback);
        }

        var postToIFrame = function (targetIFrame, targetPage, data) {
            var formTag = String.format('<form method="post" target="{0}" action="{1}"></form>', targetIFrame, targetPage);
            var form = $(formTag);

            for (var prop in data) {
                var inputTag = String.format('<input type="hidden" name="{0}" id="{0}" />', prop);
                var jsonData = JSON.stringify(data[prop]);
                form.append($(inputTag).val(jsonData));
            }

            $(form).appendTo('body').submit();
        }
    </script>
    
    <style type="text/css">
        .load-script-border { border: 1px solid #89A6C0; }
        
        .SettingException
        {
	        background: #facece; 
	        color: #ce0000;
	        border: 1px solid #ce0000; 
	        border-radius: 5px;
	        font-size:10pt;
	        font-family: Arial,Verdana,Helvetica,sans-serif;
	        display: inline-block; 
	        position: relative;
	        padding: 5px 6px 6px 26px; 
        }

        .SettingException-Icon 
        {
	        position: absolute;
	        left: 4px; 
	        top: 4px; 
	        height: 16px; 
	        width: 16px; 
	        background: url(/Orion/NCM/Resources/images/icon_failed.gif) top left no-repeat; 
        }
        
        .SettingOk
        {
	        background: #daf7cc; 
	        color: #00a000;
	        border: 1px solid #00a000; 
	        border-radius: 5px;
	        font-size:10pt;
	        font-family: Arial,Verdana,Helvetica,sans-serif;
	        display: inline-block; 
	        position: relative;
	        padding: 5px 6px 6px 26px; 
        }

        .SettingOk-Icon 
        {
	        position: absolute;
	        left: 4px; 
	        top: 4px; 
	        height: 16px; 
	        width: 16px; 
	        background: url(/Orion/NCM/Resources/images/icon_ok.gif) top left no-repeat; 
        }
    </style>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ncmMainContentPlaceHolder" runat="Server">
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
    <div style="padding:10px;width:80%">
        <div style="border:solid 1px #dcdbd7;width:100%;background-color:#fff;width:99%;padding:10px;">
            <table width="100%">
                <tr id="UseUserLevelLoginCredsHolder" runat="server">
                    <td style="padding-bottom:10px;">
                        <span class="SuggestionWarning">
                            <span class="SuggestionWarning-Icon"></span>
                            <%=string.Format(Resources.NCMWebContent.WEBDATA_VK_714, @"<a style='color:#336699;text-decoration:underline;' href='/Orion/NCM/Admin/Settings/UserLevelLoginCreds.aspx'>", @"</a>") %>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div>
                            <ncm:LoadScript ID="loadScriptCtrl" runat="server" CssClass="load-script-border" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:30px;">
                        <div style="font-weight:bold">
                            <%= Resources.NCMWebContent.WEBDATA_VM_108%>
                        </div>
                        <div style="width:50%">
                            <ncm:UniversalNodesPicker ID="universalNodesPicker" runat="server" />
                        </div>
                    </td>
                </tr>
                <tr id="ApprovalCommentsHolder" runat="server" visible="false">
                    <td style="padding-top:10px;">
                        <b><%= Resources.NCMWebContent.WEBDATA_VK_386%></b><br />
                        <asp:TextBox ID="txtApprovalComments" runat="server" TextMode="MultiLine" Width="300px" Height="45px" spellcheck="false" Font-Name="Arial" Font-Size="9pt" BorderColor="#89A6C0" BorderWidth="1px" />
                        <ajaxToolkit:TextBoxWatermarkExtender ID="WatermarkExtender" runat="server" TargetControlID="txtApprovalComments" WatermarkText="<%$ Resources: NCMWebContent, WEBDATA_VK_387 %>" WatermarkCssClass="watermark" />
                    </td>
                </tr>
            </table>
        </div>

        <asp:UpdatePanel runat="server" ID="upPanel" UpdateMode="Conditional">
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td style="padding-top:10px;" colspan="2">
                            <asp:Label ID="ValidatorLabel" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top:10px;width:20%;">
                            <orion:LocalizableButton ID="OKButton" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_122 %>" OnClick="OKButton_Click"></orion:LocalizableButton>
                            <orion:LocalizableButton ID="CancelButton" runat="server" DisplayType="Secondary" LocalizedText="Cancel" CausesValidation="false" OnClick="CancelButton_Click"></orion:LocalizableButton>
                        </td>
                        <td style="padding-top:10px;width:80%;">
                            <orion:LocalizableButton runat="server" ID="ValidateButton" LocalizedText="CustomText" Text="<%$ Resources:NCMWebContent, WEBDATA_VM_20%>" DisplayType="Secondary" OnClick="ValidateButton_Click"></orion:LocalizableButton>
                            <orion:LocalizableButton runat="server" ID="PreviewScriptButton" LocalizedText="CustomText" Text="<%$ Resources:NCMWebContent, ExecuteRemediationScript_PreviewScript_ButtonText%>" DisplayType="Secondary" OnClick="PreviewScriptButton_Click"></orion:LocalizableButton>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
