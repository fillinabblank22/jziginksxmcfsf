<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ConfigChangeReport.aspx.cs" Inherits="ConfigChangeReport" MasterPageFile="~/Orion/NCM/NCMEditMasterPage.master" Title="Config Change Report" ValidateRequest="false"%>

<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register TagPrefix="ncm" Src="~/Orion/NCM/Controls/IconExportToPdf.ascx" TagName="IconExportToPdf" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ncmPageTitlePlaceHolder">
	<table id="tableNodeName" runat="server" border="0">
            <tr>
                <td style="padding-bottom:5px;padding-top:5px;">
                    <h1 style="padding-left:0px;padding-bottom:0px;padding-top:5px;">
                        <asp:Label ID="labelNode" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_IC_19 %>"></asp:Label>&nbsp;
                        <asp:HyperLink ID="linkNodeName" runat="server"></asp:HyperLink>
                    </h1>
                </td>
            </tr>
        </table>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" runat="server">
    <ncm:DropDownMapPath ID="NodeSiteMapPath" Provider="NCMSitemapProvider" runat="server"  OnInit="SiteMapPath_OnInit">
        <RootNodeTemplate>
            <a href="<%# Eval(@"url") %>" ><u> <%# Eval(@"title") %></u> </a>
        </RootNodeTemplate>
        <CurrentNodeTemplate>
	        <a href="<%# Eval(@"url") %>" ><%# Eval(@"title") %> </a>
	    </CurrentNodeTemplate>
   </ncm:DropDownMapPath>
</asp:Content>

<asp:Content ID="ConfigChangeReportContent" ContentPlaceHolderID="ncmMainContentPlaceHolder" runat ="server" >
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
    <script type="text/javascript" src="/Orion/NCM/JavaScript/main.js"></script>
    
    <div style="padding-left:10px;padding-right:10px;"> 
        <table id="tableTimeFrame" width="800px" runat="server" border="0"> 
            <tr>
                <td style="padding-bottom:10px;padding-top:10px">
                    <asp:Label ID="labelReportTitle" runat="server"></asp:Label>
                </td>
            </tr> 
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0"  border="0">                    
                        <tr> 
                            <td colspan="6">
                                <table>
                                    <tr>
                                        <td style="border:0px;padding-bottom:5px;">
                                            <asp:Image ID="imageDisplayChanges" runat="server" ImageUrl="/Orion/NCM/Resources/images/Icon.DisplayChanges.gif" ImageAlign="AbsMiddle"/>
                                        </td>
                                        <td class="ReportDetailsBold" colspan="" align="left" style="padding-bottom:5px;">
                                            <%=Resources.NCMWebContent.WEBDATA_IC_01 %>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-right:5px;" align="right"><%=Resources.NCMWebContent.WEBDATA_IC_05 %></td>
                            <td id="CalendarTableFrom" runat="server" style="border:0px;"></td>
                            <td style="border:0px;"></td>
                            <td style="padding-right:5px;padding-left:15px;" align="right"><%=Resources.NCMWebContent.WEBDATA_IC_06 %></td>
                            <td id="CalendarTableTo" runat="server" style="border:0px;"></td>
                            <td style="border:0px;padding-left:5px;" align="left">
                               <orion:LocalizableButton ID="ViewChanges" runat="server"  LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_IC_02 %>" DisplayType="Primary" OnClick="ViewChangesClick"/> 
                            </td>
                        </tr>                       
                        <tr>
                            <td align="right"></td>
                            <td id="DateFromValidatorMsg" runat="server" class="ReportDetails" style="border:0px;color:Red;"></td>
                            <td style="border:0px; "></td>
                            <td align="right"></td>
                            <td id="DateToValidatorMsg" runat="server" class="ReportDetails" style="border:0px;color:Red;"></td>
                            <td style="border:0px;">&nbsp;</td>
                        </tr>
                    </table>  
                </td>
            </tr>
        </table>
        
        <asp:UpdatePanel ID="uPanel" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <table id="tableConfigType" runat="server" width="100%" style="padding-left:5px;">
                    <tr>
                        <td style="border:0px groove transparent;padding-bottom:15px;">
                            <asp:CheckBox ID="chkViewChangesOn" AutoPostBack="true" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_IC_07 %>" OnCheckedChanged="chkViewChangesOn_OnCheckedChanged" />
                            <ncm:ConfigTypes ID="ddlConfigType" runat="server" Width="150px"></ncm:ConfigTypes>
                            <%=Resources.NCMWebContent.WEBDATA_IC_08 %>
                        </td>
                    </tr>
                    <tr id="onlyShowDevicesThatHadChangesHolder" runat="server">
                        <td style="border:0px groove transparent;padding-bottom:15px;">
                            <asp:CheckBox ID="chkOnlyShowDevicesThatHadChanges" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_983 %>" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
                       
        <table id="tableLegend" runat="server" cellspacing ="5" border="0">
            <tr>                
                <td style="background-color:Khaki;height:18px;width:16px;">
                </td>
                <td style="font-weight:bold;font-size:8pt;padding-right:10px;" ><%=Resources.NCMWebContent.WEBDATA_IC_09 %></td> 
                <td style="background-color:#a0eda0;height:18px;width:16px;">
                </td>  
                <td style="font-weight:bold;font-size:8pt;padding-right:10px;" ><%=Resources.NCMWebContent.WEBDATA_IC_10 %></td>          
                <td style="background-color:#f7adaf;height:18px;width:16px;">
                </td>
                <td style="font-weight:bold;font-size:8pt;padding-right:10px;" ><%=Resources.NCMWebContent.WEBDATA_IC_11 %></td>            
            </tr>
        </table>
        <br />
        <asp:Table ID="tableDiffResults" runat="server" CellPadding="0" CellSpacing="0" Width="100%">        
        </asp:Table>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmHelpIconPlaceHolder" runat="server">
    <ncm:IconExportToPdf ID="IconExportToPdf1" runat="server" />
</asp:Content>         