﻿<%@ WebHandler Language="C#" Class="LoadSaveScriptHandler" %>

using System;
using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
using System.IO;
using System.Web.Script.Serialization;

public class LoadSaveScriptHandler : IHttpHandler, IRequiresSessionState 
{
    public enum Action
    {
        loadscript,
        savescript 
    }
    
    public void ProcessRequest (HttpContext context) 
    {
        string objAction = context.Request.Params[@"action"];
        Action action = (Action)Enum.Parse(typeof(Action), objAction);

        context.Response.Clear();
        switch (action)
        {
            case Action.loadscript:
                if (context.Request.Files.Count > 0)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    Dictionary<string, object> obj = new Dictionary<string, object>();
                    
                    context.Response.ContentType = @"text/html";

                    HttpPostedFile file = context.Request.Files[0];
                    if (Path.GetExtension(file.FileName).Equals(@".txt", StringComparison.InvariantCultureIgnoreCase))
                    {
                        StreamReader sr = new StreamReader(file.InputStream);
                        obj[@"success"] = true;
                        obj[@"script"] = sr.ReadToEnd();
                        context.Response.Write(serializer.Serialize(obj));
                    }
                    else
                    {
                        obj[@"success"] = false;
                        obj[@"error"] = Resources.NCMWebContent.WEBDATA_VK_910;
                        context.Response.Write(serializer.Serialize(obj));
                    }
                }
                break;
            case Action.savescript:
                string script = HttpContext.Current.Server.UrlDecode(context.Request.Params[@"script"]);
                context.Response.ContentType = @"text/plain";
                context.Response.AppendHeader(@"Content-Disposition", @"attachment; filename=Orion-NCM-script.txt");
                context.Response.Write(script);
                break;
            default:
                break;
        }
        context.Response.End();
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }
}