using System;
using System.Text;
using System.Linq;
using System.Web.UI;
using System.Collections.Generic;
using System.Globalization;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCM.Contracts.InformationService;
using StringRegistrar = SolarWinds.Orion.Core.Common.i18n.Registrar.ResourceManagerRegistrar;
using SolarWinds.NCM.Contracts;
using SolarWinds.NCM.Contracts.GlobalConfigTypes;
using SolarWinds.NCM.Web.Contracts.SiteMap;

public partial class Orion_NCM_Resources_PolicyReports_ExecuteRemediationScript : Page
{
    private const string jsScriptKey = "584C9B1D-3DE5-4183-BD68-1D4AC8C30AE7";
    private IIsWrapper isLayer;
    private readonly ILogger logger;
    private readonly Func<INcmSiteMapRenderer> rendererFactory;

    public Orion_NCM_Resources_PolicyReports_ExecuteRemediationScript()
    {
        isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
        logger = ServiceLocator.Container.Resolve<Func<Type, ILogger>>()(GetType());
        rendererFactory = ServiceLocator.Container.Resolve<Func<INcmSiteMapRenderer>>();
    }

    protected bool UseUserLevelLoginCreds
    {
        get
        {
            if (ViewState["UseUserLevelLoginCreds"] == null)
            {
                using (var proxy = isLayer.GetProxy())
                {
                    var connectivityLevel = proxy.Cirrus.Settings.GetSetting(Settings.ConnectivityLevel, ConnectivityLevels.Default, null);
                    var useUserLevelLoginCreds = connectivityLevel.Equals(ConnectivityLevels.Level3, StringComparison.InvariantCultureIgnoreCase);
                    ViewState["UseUserLevelLoginCreds"] = useUserLevelLoginCreds;
                    return useUserLevelLoginCreds;
                }
            }

            return Convert.ToBoolean(ViewState["UseUserLevelLoginCreds"]);
        }
    }

    protected PolicyRule PolicyRule
    {
        get
        {
            if (Session[@"NCM_PolicyRule"] != null)
                return (PolicyRule)Session[@"NCM_PolicyRule"];

            using (var proxy = isLayer.GetProxy())
            {
                var rule = proxy.Cirrus.PolicyReports.GetPolicyRule(Guid.Parse(Page.Request.QueryString[@"RuleID"]));
                Session[@"NCM_PolicyRule"] = rule;
                return rule;
            }
        }
        set
        {
            Session[@"NCM_PolicyRule"] = value;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!Page.IsPostBack)
            PolicyRule = null;

        if (!string.IsNullOrEmpty(Page.Request.QueryString[@"LimitationID"]))
        {
            var currentLimitationID = Convert.ToInt32(Page.Request.QueryString[@"LimitationID"], CultureInfo.InvariantCulture);
            universalNodesPicker.LimitationID = currentLimitationID;
        }

        UseUserLevelLoginCredsHolder.Visible = UseUserLevelLoginCreds;
        ValidateButton.Visible = PolicyRule.RemediateScriptType.Equals(@"CCT", StringComparison.InvariantCultureIgnoreCase);
        PreviewScriptButton.Visible = PolicyRule.RemediateScriptType.Equals(@"CLI", StringComparison.InvariantCultureIgnoreCase) && PolicyRule.ExecuteRemediationScriptPerBlock;

        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            OKButton.OnClientClick =
                $@"demoAction('{DemoModeConstants.NCM_EXECUTE_REMEDIATION_SCRIPT}', this); return false;";
        else
        {
            var isPermissionExist = SecurityHelper.IsPermissionExist(SecurityHelper._canExecuteScript);
            OKButton.Visible = isPermissionExist;
            if (ConfigChangeApprovalHelper.ShowSendApprovalRequestButton && isPermissionExist)
            {
                ApprovalCommentsHolder.Visible = PolicyRule.RemediateScriptType.Equals(@"CLI", StringComparison.InvariantCultureIgnoreCase);
                if (PolicyRule.RemediateScriptType.Equals(@"CLI", StringComparison.InvariantCultureIgnoreCase))
                    OKButton.Text = StringRegistrar.Instance.GetResourceString(@"NCM.Strings", @"LIBCODE_VK_30");
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = string.Format(Resources.NCMWebContent.ExecuteRemediationScript_PageTitle, PolicyRule.RuleName);
        if (!Page.IsPostBack)
        {
            if (Session[@"NCM_Orion_NodesList"] != null)
                universalNodesPicker.AssignedNodes = (List<Guid>)Session[@"NCM_Orion_NodesList"];

            loadScriptCtrl.ScriptText = PolicyRule.RemediateScript;
        }
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        PolicyRule = null;
        Response.Redirect(GetReturnUrl());
    }

    protected void OKButton_Click(object sender, EventArgs e)
    {
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) return;

        ClearErrorBar();

        if (universalNodesPicker.AssignedNodes.Count == 0)
            ShowErrorBar(Resources.NCMWebContent.FirmwareUpgradeWizard_SelectNodesDialogMessage);
        else if (string.IsNullOrEmpty(loadScriptCtrl.ScriptText))
            ShowErrorBar(Resources.NCMWebContent.WEBDATA_VK_38);
        else
        {
            if (PolicyRule.RemediateScriptType.Equals(@"CCT", StringComparison.InvariantCultureIgnoreCase))
            {
                if (!ValidateScript(loadScriptCtrl.ScriptText))
                    return;
            }

            var nodeIds = universalNodesPicker.AssignedNodes.ToArray().Select(item => Guid.Parse(item.ToString())).ToArray();
            if (ConfigChangeApprovalHelper.ShowSendApprovalRequestButton && PolicyRule.RemediateScriptType.Equals(@"CLI", StringComparison.InvariantCultureIgnoreCase))
            {
                var ticket = new NCMApprovalTicket
                {
                    ConfigType = PolicyRule.ExecuteScriptInConfigMode ? GlobalConfigTypes.Running : string.Empty,
                    Reboot = false,
                    RequestType = PolicyRule.ExecuteScriptInConfigMode
                        ? NCMApprovalTicket.TYPE_UPLOAD_CONFIG
                        : NCMApprovalTicket.TYPE_EXECUTE_SCRIPTS,
                    Script = PolicyRule.ExecuteRemediationScriptPerBlock ? string.Empty : loadScriptCtrl.ScriptText,
                    Comments = txtApprovalComments.Text,
                    UseScriptPerNode = PolicyRule.ExecuteRemediationScriptPerBlock
                };
                if (PolicyRule.ExecuteRemediationScriptPerBlock)
                {
                    using (var proxy = isLayer.GetProxy())
                    {
                        var networkNodes = proxy.Cirrus.PolicyReports.GenerateRemediationScriptForNodes(nodeIds, Guid.Parse(Page.Request.QueryString[@"ReportID"]), Guid.Parse(Page.Request.QueryString[@"PolicyID"]), Guid.Parse(Page.Request.QueryString[@"RuleID"]), loadScriptCtrl.ScriptText);
                        foreach (var node in networkNodes)
                        {
                            ticket.AddNode(node.NodeID, node.Script);
                        }
                    }
                }
                else
                {
                    foreach (var nodeId in nodeIds)
                    {
                        ticket.AddNode(nodeId);
                    }
                }
                PolicyRule = null;
                ConfigChangeApprovalHelper.SendRequestForApproval(ticket);
            }
            else
            {
                if (PolicyRule.RemediateScriptType.Equals(@"CLI", StringComparison.InvariantCultureIgnoreCase))
                {
                    using (var proxy = isLayer.GetProxy())
                    {
                        if (PolicyRule.ExecuteRemediationScriptPerBlock)
                        {
                            var networkNodes = proxy.Cirrus.PolicyReports.GenerateRemediationScriptForNodes(nodeIds, Guid.Parse(Page.Request.QueryString[@"ReportID"]), Guid.Parse(Page.Request.QueryString[@"PolicyID"]), Guid.Parse(Page.Request.QueryString[@"RuleID"]), loadScriptCtrl.ScriptText);
                            var ncmNodes = networkNodes.Select(x => new NCMNodeScript
                            {
                                NodeId = x.NodeID,
                                Script = x.Script
                            }).ToArray();

                            if (PolicyRule.ExecuteScriptInConfigMode)
                            {
                                proxy.Cirrus.ConfigArchive.UploadConfigPerNode(ncmNodes, @"Running", false);
                            }
                            else
                            {
                                proxy.Cirrus.ConfigArchive.ExecuteScriptPerNode(ncmNodes);
                            }
                        }
                        else
                        {
                            if (PolicyRule.ExecuteScriptInConfigMode)
                                proxy.Cirrus.ConfigArchive.UploadConfig(nodeIds, @"Running", loadScriptCtrl.ScriptText, false);
                            else
                                proxy.Cirrus.ConfigArchive.ExecuteScript(nodeIds, loadScriptCtrl.ScriptText);
                        }
                    }
                    PolicyRule = null;
                    Response.Redirect("/Orion/NCM/TransferStatus.aspx");
                }
                else
                {
                    var executeConfigSnippetHelper = ServiceLocator.Container.Resolve<IExecuteConfigSnippetHelper>();

                    string error;
                    if (executeConfigSnippetHelper.TryLoadConfigSnippet(loadScriptCtrl.ScriptText, out error))
                    {
                        ExNodes.Clear();
                        if (executeConfigSnippetHelper.TryPopulateExDataModel(nodeIds, out error))
                        {
                            Session[@"NCM_ConfigSnippet"] = Tuple.Create(string.Format(Resources.NCMWebContent.ExecuteRemediationScript_Title, PolicyRule.RuleName), loadScriptCtrl.ScriptText, false);
                            PolicyRule = null;
                            Response.Redirect(
                                $"/Orion/NCM/Resources/ConfigSnippets/ExecuteConfigSnippet.aspx?ReturnUrl={UrlHelper.ToSafeUrlParameter(GetReturnUrl())}");
                        }
                        else
                            ShowErrorBar(error);
                    }
                    else
                        ShowErrorBar(error);
                }
            }
        }
    }

    protected void ValidateButton_Click(object sender, EventArgs e)
    {
        ValidateScript(loadScriptCtrl.ScriptText);
    }

    protected void PreviewScriptButton_Click(object sender, EventArgs e)
    {
        ClearErrorBar();

        if (universalNodesPicker.AssignedNodes.Count == 0)
            ShowErrorBar(Resources.NCMWebContent.FirmwareUpgradeWizard_SelectNodesDialogMessage);
        else if (string.IsNullOrEmpty(loadScriptCtrl.ScriptText))
            ShowErrorBar(Resources.NCMWebContent.WEBDATA_VK_38);
        else
        {
            var jsScript = new StringBuilder();
            jsScript.AppendFormat(@"showWindow('{0}', 'iframeContainer', function () {1}", Resources.NCMWebContent.RemediationScriptPreview_PageTitle, @"{");
            jsScript.Append(@"postToIFrame('iframeContainer', '/Orion/NCM/PreviewRemediationScript.aspx?printable=true', {");
            jsScript.AppendFormat(@"nodeIds : [{0}],", string.Join(@",", universalNodesPicker.AssignedNodes.ToArray().Select(item =>
                $@"'{item}'")));
            jsScript.AppendFormat(@"reportId : '{0}',", Page.Request.QueryString[@"ReportID"]);
            jsScript.AppendFormat(@"policyId : '{0}',", Page.Request.QueryString[@"PolicyID"]);
            jsScript.AppendFormat(@"ruleId : '{0}',", Page.Request.QueryString[@"RuleID"]);
            jsScript.Append(@"script : LoadScriptControl.getScript()");
            jsScript.Append(@"});");
            jsScript.Append(@"});");

            ScriptManager.RegisterClientScriptBlock(Page,
                GetType(),
                jsScriptKey,
                jsScript.ToString(),
                true);
        }
    }

    private bool ValidateScript(string script)
    {
        var exHelper = ServiceLocator.Container.Resolve<IExHelper>();
        var errorString = exHelper.ValidateConfigSnippetScript(script);
        var isValid = string.IsNullOrEmpty(errorString);
        ValidatorLabel.Text = isValid ? $@"<span class='SettingOk-Icon'></span>{Resources.NCMWebContent.WEBDATA_VM_59}" : string.Format(Resources.NCMWebContent.WEBDATA_VM_60, @"<span class='SettingException-Icon'></span>" + errorString);
        ValidatorLabel.CssClass = isValid ? @"SettingOk" : @"SettingException";
        return isValid;
    }

    private void ClearErrorBar()
    {
        ValidatorLabel.CssClass = string.Empty;
        ValidatorLabel.Text = string.Empty;
    }

    private void ShowErrorBar(string error)
    {
        ValidatorLabel.CssClass = @"SettingException";
        ValidatorLabel.Text = $@"<span class='SettingException-Icon'></span>{error}";
    }

    private string GetReturnUrl()
    {
        if (!string.IsNullOrEmpty(Page.Request.QueryString[@"ReportID"]))
            return
                $@"/Orion/NCM/ComplianceReportResult.aspx?ReportID={Page.Request.QueryString[@"ReportID"].Trim('{', '}').ToLower()}";
        return @"/Orion/NCM/ComplianceReports.aspx";
    }

    protected void SiteMapPath_OnInit(object sender, EventArgs e)
    {
        try
        {
            var id = string.Empty;
            if (!string.IsNullOrEmpty(Request.QueryString[@"ReportID"]))
                id = Page.Request.QueryString[@"ReportID"].Trim('{', '}').ToLower();

            var renderer = rendererFactory();
            renderer.SetUpData(new KeyValuePair<string, string>(@"ReportID", id));
            NodeSiteMapPath.SetUpRenderer(renderer);
        }
        catch (Exception ex)
        {
            logger.Error("Error initializing the site map.", ex);
            throw;
        }
    }
}
