using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_Controls_NavigationTabBar : System.Web.UI.UserControl
{
    private Dictionary<string, string> _added = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
    private bool _alreadyactive = false;
    private string _pageTitleOverride = null;
    private readonly ICirrusEntityHelper cirrusEntityHelper = new CirrusEntityHelper();

    public bool ShowScriptManagementTab
    {
        get { return SecurityHelper.IsPermissionExist(SecurityHelper._canUpload) || cirrusEntityHelper.IsDemoMode(); }
    }

    public bool ShowTransferStatusTab
    {
        get { return SecurityHelper.IsPermissionExist(SecurityHelper._canDownload) || cirrusEntityHelper.IsDemoMode(); }
    }

    public bool ShowInventoryStatusTab
    {
        get { return SecurityHelper.IsPermissionExist(SecurityHelper._canDownload) || cirrusEntityHelper.IsDemoMode(); }
    }

    public string PageTitleOverride
    {
        get
        {
            return _pageTitleOverride;
        }
        set
        {
            _pageTitleOverride = value;
        }
    }

    [PersistenceMode(PersistenceMode.InnerProperty)]
    public PlaceHolder SearchPlaceholder
    {
        get { return phSearch; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void AddTab(string displayName, string url, bool Active, bool Enabled, bool Ignore)
    {
        if (Ignore)
            return;

        if (_added.ContainsKey(url))
            return;

        if (_alreadyactive)
            Active = false;

        _added.Add(url, displayName);

        const string innerTabFormat = "<em class=\"x-tab-left\"><span class=\"x-tab-strip-inner\"><span class=\"x-tab-strip-text\">{0}</span></span></em>";
        const string NonClickingFormat = "<li class=\"sw-tab {0}\"><a href=\"{1}\" onclick=\"return false;\" class=\"x-tab-right\">{2}</a></li>";
        const string ClickingFormat = "<li class=\"sw-tab {0}\"><a href=\"{1}\" class=\"x-tab-right\">{2}</a></li>";

        string fullpath = VirtualPathUtility.ToAbsolute(url);
        bool isurl = fullpath.Equals(Request.Url.AbsolutePath, StringComparison.OrdinalIgnoreCase);

        string fmt = NonClickingFormat;
        string p1;
        string p2;

        if (!Enabled)
        {
            p1 = @"x-item-disabled";
            p2 = @"#";
        }
        else if (Active)
        {
            _alreadyactive = true;
            p1 = @"x-tab-strip-active";

            if (isurl)
            {
                p2 = @"#";
            }
            else
            {
                p2 = HttpUtility.HtmlAttributeEncode(url);
                fmt = ClickingFormat;
            }
        }
        else
        {
            p1 = "";
            p2 = HttpUtility.HtmlAttributeEncode(url);
            fmt = ClickingFormat;
        }

        string inner = string.Format(innerTabFormat, HttpUtility.HtmlEncode(displayName));
        string newTab = string.Format(fmt, p1, p2, inner);
        TabPH.Text += newTab;
    }

    public void AddTab(string displayName, string url, bool Active)
    {
        AddTab(displayName, url, Active, true, false);
    }

    public bool IsPageRegexMatch(string pattern)
    {
        return Regex.IsMatch(Request.Url.AbsolutePath, pattern, RegexOptions.IgnoreCase);
    }
}
