﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;

using SolarWinds.NCM.Contracts.InformationService;

public partial class Orion_NCM_PreviewRemediationScript : System.Web.UI.Page
{
    protected List<NetworkNode> NetworkNodes
    {
        get
        {
            if (ViewState["NCM_NetworkNodes"] != null)
                return (List<NetworkNode>)ViewState["NCM_NetworkNodes"];

            Guid[] nodeIds = GetPostData<Guid[]>(@"nodeIds");
            Guid reportId = GetPostData<Guid>(@"reportId");
            Guid policyId = GetPostData<Guid>(@"policyId");
            Guid ruleId = GetPostData<Guid>(@"ruleId");
            string script = GetPostData<string>(@"script");

            var isLayer = new SolarWinds.NCMModule.Web.Resources.ISWrapper();
            using (var proxy = isLayer.Proxy)
            {
                var networkNodes = proxy.Cirrus.PolicyReports.GenerateRemediationScriptForNodes(nodeIds, reportId, policyId, ruleId, script);
                ViewState["NCM_NetworkNodes"] = networkNodes;
                return networkNodes;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.NCMWebContent.RemediationScriptPreview_PageTitle;

        ScriptPreview.NetworkNodes = NetworkNodes;
        if (!Page.IsPostBack)
            ScriptPreview.LoadControl();
    }

    private T GetPostData<T>(string key)
    {
        string postData = Server.UrlDecode(Request.Form[key]);
        if (!string.IsNullOrEmpty(postData))
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Deserialize<T>(Server.UrlDecode(postData));
        }
        else
            return default(T);
    }
}