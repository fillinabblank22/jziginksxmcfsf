﻿using System;

public partial class Orion_NCM_Controls_UtcServerZone : System.Web.UI.UserControl
{
    protected override void OnInit(EventArgs e)
    {
        int utcOffset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow).Hours;
        lbl1.Text = string.Format(Resources.NCMWebContent.WEBDATA_VK_878, utcOffset >= 0 ? @"+" : @"-", Math.Abs(utcOffset), DateTime.Now.ToString(@"HH:mm"));
        base.OnInit(e);
    }
}