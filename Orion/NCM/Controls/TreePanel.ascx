﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TreePanel.ascx.cs" Inherits="Orion_NCM_Controls_TreePanel" %>

<%@ Register Assembly="Infragistics2.WebUI.Shared.v8.2, Version=8.2.20082.1000, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Shared" TagPrefix="ignav" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebNavigator.v8.2, Version=8.2.20082.1000, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.UltraWebNavigator" TagPrefix="ignav" %>

<script type="text/javascript" src="/Orion/NCM/JavaScript/main.js"></script>

<style type="text/css">
    .tree-node-loading-img
    {
        width: 16px !important;
        height: 16px !important;
        background-image: url(/Orion/NCM/Resources/images/spinner.gif) !important;
        background-position: left;
        background-repeat:no-repeat;
        margin-left:35px;
    }
    
    .tree-node-loading-text
    {
        padding-left: 18px;
        display: inline-block;
    }
</style>

<div>
    <table cellpadding="0" cellspacing="0" style="background-color:white;border:#999999 1px solid;">
        <tr>
            <td style="border-bottom:#999999 1px solid;padding:5px;">
                <%=Title %>
            </td>
        </tr>
        <tr>
            <td style="padding:5px 5px 5px 10px;background-color:#ebeced;">
                <span>
                    <asp:LinkButton ID="btnSelectAll" runat="server" OnClick="btnSelectAll_Click">
                        <img src="/Orion/NCM/Resources/images/Toolbar.SelectAll.gif" /><span style="padding-left:5px;"><%=Resources.NCMWebContent.WEBDATA_VK_146 %></span>
                    </asp:LinkButton>
                </span>
                <span style="padding-left:10px;">
                    <asp:LinkButton ID="btnSelectNone" runat="server" OnClick="btnSelectNone_Click">
                        <img src="/Orion/NCM/Resources/images/disable_icon.png" /><span style="padding-left:5px;"><%=Resources.NCMWebContent.WEBDATA_VK_908 %></span>
                    </asp:LinkButton>
                </span>
            </td>
        </tr>
        <tr>
            <td style="padding:5px;">
                <ignav:UltraWebTree ID="tree"
                    runat="server" 
                    Width="300px" 
                    Height="450px" 
                    BackColor="White" 
                    Visible="true" 
                    LoadOnDemand="Manual"
                    OnDemandLoad="tree_OnDemandLoad" 
                    CssClass="ncm_UltraWebTree">
                    <Images>
                        <ExpandImage Url="/Orion/NCM/Resources/images/Button.Expand.gif" />
                        <CollapseImage Url="/Orion/NCM/Resources/images/Button.Collapse.gif" />
                    </Images>
                    <SelectedNodeStyle BackColor="Transparent" />
                    <ClientSideEvents NodeChecked="UltraWebTree_NodeChecked" DemandLoad="UltraWebTree_DemandLoad" />
                </ignav:UltraWebTree>
            </td>
        </tr>
    </table>
</div>