﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ModalPopup.ascx.cs" Inherits="Orion_NCM_Controls_ModalPopup" %>

<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:UpdatePanel runat="server" ID="UpdatePanelModalPopup" UpdateMode="Conditional" ChildrenAsTriggers="true">
    <ContentTemplate>
        <div id="divContainer" runat="server" style="display: none;">
            <iframe frameborder="0" src="/Orion/NCM/Controls/modal_popup.html" id="iFrame" runat="server">               
            </iframe>
            <div id="divCover" runat="server" style="position: fixed; z-index: 1000; width: 100%;left: 0px;">
                <div id="divDialog" runat="server" style="margin-left: auto; margin-right: auto; width: 800px; height: 100%; background-color: White;">
                    <div style="border:Gray 1px solid;box-shadow:10px 10px 5px #888888;">
                        <div style="background-color:#F4F4F4;">
                            <div style="padding:10px;">
                                <table width="100%">
                                    <tr>
                                        <td style="font: bold 14px Arial,Verdana,Helvetica,sans-serif;"><%=Title %></td>
                                        <td align="right"><asp:PlaceHolder runat="server" ID="phCloseButton" /></td>
                                    </tr>
                                </table>
                            </div>
                            <div style="padding:0px 10px 0px 10px;">
                                <div id="divContent" runat="server" style="background-color:White;border:#D8D8D8 1px solid;overflow:auto;">
                                    <asp:PlaceHolder runat="server" ID="phContent" />
                                </div>
                            </div>
                            <table width="100%">
                                <tr>
                                    <td id="buttonHolder" runat="server" style="padding:10px;" align="right">
                                        <asp:PlaceHolder runat="server" ID="phSublitButton" />
                                        <asp:PlaceHolder runat="server" ID="phCancelButton" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script language="javascript" type="text/javascript" >
            window.onresize = function () { setModalPopup(); }

            function setModalPopup() {
                var outY = getWindowHeight();

                var divs = $("[id$='_divCover']");
                var inY;
                var nextTop;
                for (var i = 0; i < divs.length; i++) {
                    inY = divs[i].offsetHeight;
                    nextTop = (parseInt(outY) - parseInt(inY)) / 2;
                    divs[i].style.top = nextTop + "px";
                }
            }
        </script>
    </ContentTemplate>
</asp:UpdatePanel>