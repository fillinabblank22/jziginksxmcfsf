<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IconCustomizePage.ascx.cs" Inherits="Orion_NCM_Controls_IconCustomizePage" %>

<%if (Profile.AllowCustomize)
{%>
    <a href="/Orion/Admin/CustomizeView.aspx?ViewID=<%=((OrionView)Page).ViewInfo.ViewID %>&ReturnTo=<%= ((OrionView)Page).ReturnUrl %>" class="custPageLink"><%= Resources.NCMWebContent.WEBDATA_VK_159 %></a>
<%}%>