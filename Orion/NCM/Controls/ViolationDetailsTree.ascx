<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ViolationDetailsTree.ascx.cs" Inherits="Orion_NCM_Resources_PolicyReports_Controls_ViolationDetailsTree" %>

<style type="text/css">
    .configTableName
    {
        font-weight: bold;
        font-size:12px;
    }

    .configTableValue
    {
        color: #878787;
        font-size: 12px;
    }

    #treeControl_remediationScriptText a
    {
        color: #547DA8;
        text-decoration: none;
    }

    #treeControl_configNameText a
    {
        color: #547DA8;
        text-decoration: none;
    }

    #treeControl_remediationScriptText a:hover
    {
        color: #F99D1C;
        text-decoration: underline;
    }

    #treeControl_configNameText a:hover
    {
        color: #F99D1C;
        text-decoration: underline;
    }

    #violationDetailsTree_executeNodeButton
    {
        text-decoration: none;
        color:#000;
    }

    #violationDetailsTree_executeAllNodesButton
    {
        text-decoration: none;
        color:#000;
    }

    #violationDetailsTree_remediationScriptText a
    {
        text-decoration: underline;
        color:#326598;
    }

    #violationDetailsTree_configNameText a
    {
        text-decoration: underline;
        color:#326598;
    }

    #violationDetailsTree_executeNodeButton:hover
    {
        text-decoration: underline;
        color: #F99D1C;
    }

    #violationDetailsTree_executeAllNodesButton:hover
    {
        text-decoration: underline;
        color: #F99D1C;
    }

    #violationDetailsTree_remediationScriptText a:hover
    {
        text-decoration: underline;
        color: #F99D1C;
    }

    #violationDetailsTree_configNameText a:hover
    {
        text-decoration: underline;
        color: #F99D1C;
    }

    #treeViewContainer
    {
        border: 1px solid #ccc; 
        margin-top: 10px;
        overflow-x: hidden;
        overflow-y: hidden;
        height: 275px;
    }
</style>

<asp:Panel ID="conentPanel" runat="server">
    <div style="margin-left:5px;">
         <asp:Panel ID="textContentPanel" runat="server" >
             <table style="margin-bottom:10px; margin-left: 2px;" >
                <tr>
                    <td align="right" class="configTableName"><%=Resources.NCMWebContent.WEBDATA_VM_85%></td>
                    <td align="left" style="padding-left:10px;" class="configTableValue"><div id="configNameText" runat="server"></div></td>
                </tr>
                <tr>
                    <td align="right" class="configTableName"><%=Resources.NCMWebContent.WEBDATA_VM_86%></td>
                    <td align="left" style="padding-left:10px;" class="configTableValue"><div id="ruleNameText" runat="server"></div></td>
                </tr>
                <tr>
                    <td align="right" class="configTableName"><%=Resources.NCMWebContent.WEBDATA_VM_87%></td>
                    <td align="left" style="padding-left:10px;" class="configTableValue"><div id="violationsNumber" runat="server"></div></td>
                </tr>
                <tr>
                    <td align="right" class="configTableName"><nobr><%=Resources.NCMWebContent.WEBDATA_VM_88%></nobr></td>
                    <td align="left" style="padding-left:10px;" class="configTableValue"><div id="remediationScriptText" runat="server"></div></td>
                </tr> 
                <tr>
                    <td align="right" class="configTableName"><%=Resources.NCMWebContent.WEBDATA_VM_89%></td>
                    <td align="left" style="padding-left:10px;" class="configTableValue">
                        <table>
                            <tr>
                                <td style="padding-right:30px;"><img style="vertical-align:middle; padding-right: 8px;" alt="" src="/Orion/NCM/Resources/images/ConfigSnippets/icon_execute.gif" /><asp:LinkButton runat="server" ID="executeNodeButton"  Text="<%$ Resources: NCMWebContent, WEBDATA_VM_90 %>" Font-Size="12px" /></td>
                                <td><img style="vertical-align:middle; padding-right: 8px;" alt="" src="/Orion/NCM/Resources/images/ConfigSnippets/icon_execute.gif" /><asp:LinkButton runat="server" ID="executeAllNodesButton" Text="<%$ Resources: NCMWebContent,WEBDATA_VM_91 %>" Font-Size="12px" /></td>
                            </tr>
                        </table>
                    </td>
                </tr> 
             </table>
         </asp:Panel>
            <asp:UpdatePanel ID="uPanel_nodes" UpdateMode="Always" runat="server">
                <ContentTemplate>
                <div id="treeViewContainer" style="">
                    <table id="violation_details_picker" width="360px;">
                        <tr>
                            <td id="violation_details_container" runat="server">                                    
                            </td>
                        </tr>
                    </table>
                </div>
                </ContentTemplate>
            </asp:UpdatePanel>
     </div>
 </asp:Panel>
