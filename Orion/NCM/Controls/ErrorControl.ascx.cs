using System;
using SolarWinds.NCM.Common.Help;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_NCM_Controls_PermissionError : System.Web.UI.UserControl
{
    protected string PermissionLink
    {
        get
        {
            var helpHelperWrap = ServiceLocator.Container.Resolve<IHelpHelperWrap>();
            return helpHelperWrap.GetHelpUrl("OrionNCMWebUserPermissions");
        }
    }

    public bool VisiblePermissionLink
    {
        set { span_permissions_link.Visible = value; }
    }

    public string ErrorMessage
    {
        set { span_error_message.InnerText = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}
