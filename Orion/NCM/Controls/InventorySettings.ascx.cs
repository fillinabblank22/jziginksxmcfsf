﻿using SolarWinds.NCM.Contracts.InformationService;
using SolarWinds.NCMModule.Web.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web.UI.WebControls;

public partial class Orion_NCM_Controls_InventorySettings : System.Web.UI.UserControl
{
    [Localizable(false)] private readonly Dictionary<string, string> defaultInventorySpecList = new Dictionary<string, string>()
    {
        {"Standard-Interfaces", "Interfaces"},
        {"Standard-ARP", "ARP"},
        {"Standard-Bridge Table", "BridgeTable"},
        {"Standard-Entity MIB", "EntityMIB"},
        {"Standard-IP Addresses", "IPAddresses"},
        {"Standard-Active Ports", "ActivePorts"},
        {"Route Tables-IP CIDR Route Table", "IPCIDRRouteTable"},
        {"Route Tables-IP Route Table", "IPRouteTable"},
        {"Windows-Windows Services", "WindowsServices"},
        {"Windows-Windows Software", "WindowsSoftware"},
        {"Windows-Windows Users", "WindowsUsers"},
        {"Cisco-Cisco Chassis", "CiscoChassis"},
        {"Cisco-Cisco Memory Pools", "CiscoMemoryPools"},
        {"Cisco-Cisco Cards", "CiscoCards"},
        {"Cisco-Cisco Image", "CiscoImage"},
        {"Cisco-Cisco Catalyst Cards", "CiscoCatalystCards"},
        {"Cisco-Cisco Flash", "CiscoFlash"},
        {"Cisco-Cisco CDP", "CiscoCDP"},
        {"Cisco-Cisco FRU", "CiscoFRU"},
        {"Standard-Juniper Entity MIB", "JuniperEntity"},
        {"Standard-Brocade Serial Numbers", "BrocadeSerialNumbers"},
        {"F5-System", "cbSystem"},
        {"F5-Local TM", "cbLocalTM"},
        {"F5-Global TM", "cbGlobalTM"}
    };

    public void LoadInventorySpecs(string inventorySpecList)
    {
        string specs;
        if (string.IsNullOrEmpty(inventorySpecList))
        {
            specs = GetGlobalInventorySettings();
        }
        else
        {
            specs = inventorySpecList;
        }

        if (string.IsNullOrEmpty(specs))
        {
            return;
        }

        foreach (var inventorySpec in specs.Split(new [] { ',' }, StringSplitOptions.RemoveEmptyEntries))
        {
            string inventorySpecId;
            if (defaultInventorySpecList.TryGetValue(inventorySpec, out inventorySpecId))
            {
                var chk = SpecTable.FindControl(inventorySpecId) as CheckBox;
                if (chk != null)
                {
                    chk.Checked = true;
                }
            }
        }
    }

    public string GetSelectedInventorySpecs()
    {
        var inventorySpecList = new StringBuilder();
        foreach (var inventorySpec in defaultInventorySpecList)
        {
            var chk = SpecTable.FindControl(inventorySpec.Value) as CheckBox;
            if (chk != null)
            {
                if (chk.Checked)
                {
                    inventorySpecList.AppendFormat(@"{0},", inventorySpec.Key);
                }
            }
        }

        return inventorySpecList.ToString().TrimEnd(',');
    }

    private string GetGlobalInventorySettings()
    {
        var isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            return proxy.Cirrus.Settings.GetSetting(Settings.IncludeInInventory, @"Standard-ARP,Standard-Bridge Table,Cisco-Cisco Chassis,Cisco-Cisco,Cisco-Cisco Cards,Cisco-Cisco Catalyst Cards,Cisco-Cisco Image,Cisco-Cisco Memory Pools,Cisco-Cisco CDP,Cisco-Cisco Flash,Cisco-Cisco FRU,Standard-Entity MIB,Standard-Interfaces,Standard-System Information,Standard-Reverse DNS,Standard-IP Addresses,Standard-Active Ports,Windows-Windows Users,Windows-Windows,Windows-Windows Services,Windows-Windows Software,Route Tables-IP CIDR Route Table,Standard-Juniper Entity MIB,F5-System,F5-Local TM,F5-Global TM", null);
        }
    }
}