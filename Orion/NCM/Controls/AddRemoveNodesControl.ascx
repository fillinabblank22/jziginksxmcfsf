﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddRemoveNodesControl.ascx.cs" Inherits="Orion_NCM_Controls_AddRemoveNodesControl" %>

<%@ Register Src="~/Orion/NCM/Controls/TreePanel.ascx" TagName="TreePanel" TagPrefix="ncm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="SolarWinds.NCMModule.Web.Resources" Namespace="SolarWinds.NCMModule.Web.Resources" TagPrefix="ncm" %>

<style type="text/css">
    .SearchTermField
    {
        font-size:9pt;
        font-family: Arial,Verdana,Helvetica,sans-serif;
        border: 1px solid #999999;
        height:19px;
    }
    
    .GroupByField
    {
        font-size:9pt;
        font-family: Arial,Verdana,Helvetica,sans-serif;
        border: 1px solid #999999;
        height:22px;
    }
    
    .WatermarkSearchTerm 
    {
        color:Gray;
        font-size:9pt;
        font-family: Arial,Verdana,Helvetica,sans-serif;
        font-style:italic;
        border: 1px solid #999999;
        height:19px;
    }
</style>

<div>
    <asp:UpdatePanel ID="addRemoveNodesUpdatePanel" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <asp:Panel ID="addRemoveNodesPanel" runat="server" DefaultButton="btnSearch">
                <div id="ErrorControlHolder" runat="server" visible="false" style="padding:10px 0px 10px 0px;">
                    <ncm:ExceptionWarning ID="errorControl" runat="server" />
                </div>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td colspan="3" style="padding:10px;background-color:#ebeced;">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" style="width:2%;padding-right:10px;" nowrap="nowrap">
                                        <%=Resources.NCMWebContent.WEBDATA_VK_80 %>
                                    </td>
                                    <td align="left" style="width:48%;">
                                        <asp:DropDownList ID="ddlGroupBy" runat="server" CssClass="GroupByField" AutoPostBack="true" OnSelectedIndexChanged="ddlGroupBy_OnSelectedIndexChanged"></asp:DropDownList>
                                    </td>
                                    <td align="right" style="width:48%;">
                                        <asp:TextBox ID="txtSearchTerm" runat="server" CssClass="SearchTermField" Width="200px"></asp:TextBox>
                                        <ajaxToolkit:TextBoxWatermarkExtender ID="txtWE" runat="server" TargetControlID="txtSearchTerm" WatermarkText="<%$ Resources: NCMWebContent, WEBDATA_VK_907 %>" WatermarkCssClass="WatermarkSearchTerm"></ajaxToolkit:TextBoxWatermarkExtender>
                                    </td>
                                    <td align="left" style="width:1%;" id="ClearSearchButtonsHolder" runat="server">
                                        <asp:ImageButton ID="btnClearSearch" runat="server" ImageUrl="/Orion/NCM/Resources/images/PolicyIcons/clear_button.gif" ImageAlign="AbsMiddle" OnClick="btnClearSearch_Click" />
                                    </td>
                                    <td align="left" style="width:1%;">
                                        <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="/Orion/NCM/Resources/images/PolicyIcons/search_button.gif" ImageAlign="AbsMiddle" OnClick="btnSearch_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:10px 10px 0px 0px;">
                            <ncm:TreePanel ID="leftTreePanel" runat="server" IncludeNodeIDList="false" Title="<%$ Resources: NCMWebContent, WEBDATA_VK_905 %>" />
                        </td>
                        <td align="center" valign="middle">
                            <div>
                                <asp:LinkButton ID="btnAdd" runat="server" OnClick="btnAdd_Click">
                                    <div><img src="/Orion/NCM/Resources/images/arrows_add_32x32.gif" /></div>
                                    <div><%=Resources.NCMWebContent.WEBDATA_VK_904 %></div>
                                </asp:LinkButton>
                            </div>
                            <div style="padding-top:20px;">
                                <asp:LinkButton ID="btnRemove" runat="server" OnClick="btnRemove_Click">
                                    <div><img src="/Orion/NCM/Resources/images/arrows_remove_32x32.gif" /></div>
                                    <div><%=Resources.NCMWebContent.WEBDATA_VK_567 %></div>
                                </asp:LinkButton>
                            </div>
                        </td>
                        <td style="padding:10px 0px 0px 10px;">
                            <ncm:TreePanel ID="rightTreePanel" runat="server" IncludeNodeIDList="true" Title="<%$ Resources: NCMWebContent, WEBDATA_VK_906 %>" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>