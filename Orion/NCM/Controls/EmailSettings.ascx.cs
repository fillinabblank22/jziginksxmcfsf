using System;
using Resources;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.NCM.Contracts.InformationService;

public partial class Orion_NCM_Controls_EmailSettings : System.Web.UI.UserControl
{
    private bool _visibleEmailTo = true;
    public bool VisibleEmailTo
    {
        set
        {
            _visibleEmailTo = value;
            EmailToHolder.Visible = _visibleEmailTo;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    /// <summary>
    /// Load E-mail settings from database.
    /// </summary>
    public new void Load()
    {
        if (_visibleEmailTo)
            txtTo.Text = WebSettingsDAL.Get(NCMApprovalTicket.NCM_EMAIL_TO, string.Empty);
        txtFrom.Text = WebSettingsDAL.Get(NCMApprovalTicket.NCM_EMAIL_FROM, string.Empty);
        txtSubject.Text = WebSettingsDAL.Get(NCMApprovalTicket.NCM_EMAIL_SUBJECT, NCMWebContent.ConfigChangeApproval_MailSubject_ApprovalNeeded);
        txtMessage.Text = WebSettingsDAL.Get(NCMApprovalTicket.NCM_EMAIL_BODY, NCMWebContent.ConfigChangeApproval_MailBody_ApprovalRequestSubmitted);
    }

    /// <summary>
    /// Save E-mail settings into database.
    /// </summary>
    public void Save()
    {
        if (_visibleEmailTo)
            WebSettingsDAL.Set(NCMApprovalTicket.NCM_EMAIL_TO, txtTo.Text);
        WebSettingsDAL.Set(NCMApprovalTicket.NCM_EMAIL_FROM, txtFrom.Text);
        WebSettingsDAL.Set(NCMApprovalTicket.NCM_EMAIL_SUBJECT, txtSubject.Text);
        WebSettingsDAL.Set(NCMApprovalTicket.NCM_EMAIL_BODY, txtMessage.Text);
    }
}
