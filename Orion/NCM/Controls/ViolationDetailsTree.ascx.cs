using System;
using System.Data;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Infragistics.WebUI.UltraWebNavigator;
using System.Xml;
using System.Drawing;
using SolarWinds.NCM.Web.Contracts;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_Resources_PolicyReports_Controls_ViolationDetailsTree : System.Web.UI.UserControl
{
    #region private members

    private string xmlCode = string.Empty;
    private UltraWebTree tree = new UltraWebTree();
    private XmlNodeList blocks;
    private Guid? currentNodeID = null;
    private string currentPolicyID = string.Empty;
    private string currentRuleID = string.Empty;
    private string currentReportID = string.Empty;
    private string currentConfigID = string.Empty;
    private int currentLimitationID = 0;
    private List<Guid> nodesIdList = new List<Guid>();
    private bool dispalyAll = true;
    private IMacroParser macroParser;

    #endregion

    #region override methods

    protected override void OnInit(EventArgs e)
    {
        Session.Timeout = 30;
        violation_details_container.Controls.Add(tree);
        executeNodeButton.Click += new EventHandler(executeNodeButton_Click);
        executeAllNodesButton.Click += new EventHandler(executeAllNodesButton_Click);
        var a = new Node();
        tree.LoadOnDemand = LoadOnDemand.Manual;
        tree.DemandLoad += new DemandLoadEventHandler(tree_DemandLoad);
        tree.ExpandImage = @"/Orion/NCM/Resources/images/Button.Expand.gif";
        tree.CollapseImage = @"/Orion/NCM/Resources/images/Button.Collapse.gif";
        tree.Font.Size = FontUnit.Parse(@"14px");

        macroParser = ServiceLocator.Container.Resolve<IMacroParser>();

        tree.Width = Unit.Pixel(750);
        tree.Height = Unit.Pixel(270);
        tree.RootNodeStyle.CssClass = @"Property";
        tree.RenderAnchors = true;
        tree.SelectedNodeStyle.BackColor = Color.Transparent;

        var linkHref = string.Empty;
        base.OnInit(e);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        currentNodeID = (Guid?)ViewState["Orion_NCM_currentNodeID"];
        currentPolicyID = (string)ViewState["Orion_NCM_currentPolicyID"];
        currentRuleID = (string)ViewState["Orion_NCM_currentRuleID"];
        currentReportID = (string)ViewState["Orion_NCM_currentReportID"];
        currentConfigID = (string)ViewState["Orion_NCM_currentConfigID"];
        currentLimitationID = (int)ViewState["Orion_NCM_currentLimitationID"];
    }

    #endregion

    #region public methods

    public void Show(Guid currentNodeID, string currentPolicyID, string currentRuleID, string currentReportID, int limitationID)
    {
        if (string.IsNullOrEmpty(currentPolicyID))
        {
            throw new ArgumentException(Resources.NCMWebContent.WEBDATA_VM_92, @"currentPolicyID");
        }
        if (string.IsNullOrEmpty(currentRuleID))
        {
            throw new ArgumentException(Resources.NCMWebContent.WEBDATA_VM_92, @"currentRuleID");
        }
        if (string.IsNullOrEmpty(currentReportID))
        {
            throw new ArgumentException(Resources.NCMWebContent.WEBDATA_VM_92, @"currentReportID");
        }

        ViewState["Orion_NCM_currentNodeID"] = this.currentNodeID = currentNodeID;
        ViewState["Orion_NCM_currentPolicyID"] = this.currentPolicyID = currentPolicyID;
        ViewState["Orion_NCM_currentRuleID"] = this.currentRuleID = currentRuleID;
        ViewState["Orion_NCM_currentReportID"] = this.currentReportID = currentReportID;
        ViewState["Orion_NCM_currentLimitationID"] = currentLimitationID = limitationID;

        var nodeName = macroParser.ParseMacro(currentNodeID, @"${NodeCaption}");
        if (nodeName == string.Empty)
        {
            nodeName = macroParser.ParseMacro(currentNodeID, @"${AgentIP}");
        }

        var xmlResults = GetPolicyResultTable(currentNodeID, currentPolicyID, currentRuleID, currentReportID);
        xmlCode = xmlResults.Rows[0]["XMLResults"].ToString();
        currentConfigID = xmlResults.Rows[0]["ConfigID"].ToString();
        ViewState["Orion_NCM_currentConfigID"] = currentConfigID;
        var linkHref = $@"/Orion/NCM/ConfigDetails.aspx?ConfigID={xmlResults.Rows[0]["ConfigID"]}";
        configNameText.InnerHtml =
            $@"{nodeName}&nbsp;&nbsp;<a style='color'  onclick=""window.open('{linkHref}');return false;"" href='{linkHref}'>{Resources.NCMWebContent.WEBDATA_VM_93}</a>";
        ruleNameText.InnerHtml = xmlResults.Rows[0]["RuleName"].ToString();

        var scriptText = string.Empty;
        if ((string.IsNullOrEmpty(xmlResults.Rows[0]["RemediateScript"].ToString())) || (xmlResults.Rows[0]["RemediateScript"].ToString() == @"NULL"))
        {
            scriptText = Resources.NCMWebContent.WEBDATA_VM_94;
        }
        else
        {
            linkHref = $@"/Orion/NCM/Admin/PolicyReports/EditPolicyRule.aspx?PolicyRuleID={currentRuleID}";
            scriptText =
                $@"{Resources.NCMWebContent.WEBDATA_VM_96}&nbsp;&nbsp;<a href='{linkHref}' onclick=""{{parent.location.replace('{linkHref}'); return false;}}"">{Resources.NCMWebContent.WEBDATA_VM_95}</a>";
        }

        remediationScriptText.InnerHtml = scriptText;
        violationsNumber.InnerHtml = GetViolationCount(currentPolicyID, currentRuleID, currentReportID, currentLimitationID);
        BuildTree();
    }

    #endregion

    #region private methods

    #region gui action methods

    void executeAllNodesButton_Click(object sender, EventArgs e)
    {
        nodesIdList = GetNodesList(currentPolicyID, currentRuleID, currentReportID, currentLimitationID);
        Session[@"NCM_Orion_NodesList"] = nodesIdList;
        var redirectionURl =
            $@"/Orion/NCM/ExecuteRemediationScript.aspx?ReportID={currentReportID}&PolicyID={currentPolicyID}&RuleID={currentRuleID}&LimitationID={currentLimitationID}";
        var script = $@"window.open('{redirectionURl}', '_blank');";        
        System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), @"key_redirect_page", script, true);
    }

    void executeNodeButton_Click(object sender, EventArgs e)
    {
        nodesIdList.Clear();
        if (currentNodeID.HasValue)
        {
            nodesIdList.Add(currentNodeID.Value);
        }
        Session[@"NCM_Orion_NodesList"] = nodesIdList;

        var redirectionURl =
            $@"/Orion/NCM/ExecuteRemediationScript.aspx?ReportID={currentReportID}&PolicyID={currentPolicyID}&RuleID={currentRuleID}&LimitationID={currentLimitationID}";
        var script = $@"window.open('{redirectionURl}', '_blank');";                
        System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), @"key_redirect_page", script, true);
    }

    private void tree_DemandLoad(object sender, WebTreeNodeEventArgs e)
    {
        currentNodeID = (Guid?)ViewState["Orion_NCM_currentNodeID"];
        currentPolicyID = (string)ViewState["Orion_NCM_currentPolicyID"];
        currentRuleID = (string)ViewState["Orion_NCM_currentRuleID"];
        currentReportID = (string)ViewState["Orion_NCM_currentReportID"];
        currentConfigID = (string)ViewState["Orion_NCM_currentConfigID"];
        currentLimitationID = (int)ViewState["Orion_NCM_currentLimitationID"];

        if (e.Node.Tag != null)
        {
            blocks = (XmlNodeList)Session[@"NCMConfigBlocks"];

            if ((e.Node.Tag) is int)
            {
                var index = Convert.ToInt32(e.Node.Tag);
                var block = blocks[index];
                var patterns = block.ChildNodes.Item(0).ChildNodes;
                var counter = 0;
                foreach (XmlNode pattern in patterns)
                {
                    e.Node.Nodes.Add(GetConfigPattern(pattern, counter));
                    counter++;
                }
            }
            else if ((e.Node.Tag) is string)
            {
                var index = 0;
                if (e.Node.Parent != null && e.Node.Parent.Tag != null)
                {
                    index = Convert.ToInt32(e.Node.Parent.Tag);
                }
                var block = blocks[index];
                var patterns = block.ChildNodes.Item(0).ChildNodes;

                var patternIndex = Convert.ToInt32(((string)e.Node.Tag).Substring(8));
                var pattern = patterns[patternIndex];
                var lines = pattern.ChildNodes;
                var counter = 0;
                foreach (XmlNode line in lines)
                {
                    e.Node.Nodes.Add(GetConfigLine(line));
                    counter++;
                }
                dispalyAll = Convert.ToBoolean(e.Node.DataPath);
                if (!dispalyAll)
                {
                    var tempNode = new Node();
                    tempNode.Text = Resources.NCMWebContent.WEBDATA_VM_97;
                    tempNode.CssClass = @"Property";
                    e.Node.Nodes.Add(tempNode);
                    tempNode.CssClass = @"Property";
                }
            }
        }
    }

    #endregion

    private DataTable GetPolicyResultTable(Guid currentNodeID, string currentPolicyID, string currentRuleID, string currentReportID)
    {
        var isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            var query = $@"
SELECT TOP 1 PCR.XmlResults, PCR.ConfigTitle, PCR.ConfigID, PCR.RuleName, PR.RemediateScript FROM Cirrus.PolicyCacheResults AS PCR 
RIGHT OUTER JOIN Cirrus.PolicyRules AS PR ON PR.PolicyRuleID = pcr.RuleID 
WHERE (PCR.NodeID='{currentNodeID}') AND (PCR.PolicyID ='{currentPolicyID}') AND (PCR.RuleID='{currentRuleID}') AND (PCR.ReportID='{currentReportID}')";
            var dt = proxy.Query(query);
            return dt;
        }
    }

    private string GetViolationCount(string currentPolicyID, string currentRuleID, string currentReportID, int limitationID)
    {
        var isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            var query = $@"
SELECT COUNT(P.RuleID) AS ViolationCount FROM Cirrus.PolicyCacheResults AS P 
INNER JOIN Cirrus.NodeProperties AS NCMNodeProperties on NCMNodeProperties.NodeID=P.NodeID
INNER JOIN Orion.Nodes AS Nodes ON NCMNodeProperties.CoreNodeID=Nodes.NodeID
WHERE P.PolicyID='{currentPolicyID}' AND P.RuleID='{currentRuleID}' AND P.ReportID='{currentReportID}' AND P.IsViolation=1 WITH LIMITATION {limitationID}";
            var dt = proxy.Query(query);
            return dt.Rows[0]["ViolationCount"].ToString();
        }
    }

    private List<Guid> GetNodesList(string currentPolicyID, string currentRuleID, string currentReportID, int limitationID)
    {
        var isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            var query = $@"
SELECT P.NodeID AS ViolationCount FROM Cirrus.PolicyCacheResults AS P 
INNER JOIN Cirrus.NodeProperties AS NCMNodeProperties on NCMNodeProperties.NodeID=P.NodeID
INNER JOIN Orion.Nodes AS Nodes ON NCMNodeProperties.CoreNodeID=Nodes.NodeID
WHERE P.PolicyID='{currentPolicyID}' AND P.RuleID='{currentRuleID}' AND P.ReportID='{currentReportID}' AND P.IsViolation=1 WITH LIMITATION {limitationID}";
            var dt = proxy.Query(query);
            var nodeList = new List<Guid>();
            foreach (DataRow dr in dt.Rows)
            {
                nodeList.Add(new Guid(dr["ViolationCount"].ToString()));
            }

            return nodeList;
        }
    }

    private void BuildTree()
    {
        if (blocks == null)
        {
            blocks = GetAllBlocks();
        }
        tree.Nodes.Clear();
        var counter = 0;
        foreach (XmlNode block in blocks)
        {
            var resultBlock = GetBlock(block, counter, blocks.Count);
            if (resultBlock != null)
            {
                tree.Nodes.Add(resultBlock);
            }
            counter++;
        }
    }

    private XmlNodeList GetAllBlocks()
    {
        var xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(xmlCode);

        var retResult = xmlDoc.GetElementsByTagName("CB");
        Session[@"NCMConfigBlocks"] = retResult;
        return retResult;
    }

    private Node GetBlock(XmlNode block, int index, int blocksCount)
    {
        var configBlock = new Node();
        configBlock.ImageUrl = ResolveUrl("/Orion/NCM/Resources/images/config_picture.jpg");
        var startLine = string.Empty;
        var lineNumber = 0;
        if (block.Attributes["L"] != null)
        {
            startLine = block.Attributes["L"].InnerText;
        }
        if (block.Attributes["LN"] != null)
        {
            lineNumber = Convert.ToInt32(block.Attributes["LN"].InnerText);
        }

        if (lineNumber <= 1 && blocksCount == 1)
        {
            var patterns = block.ChildNodes.Item(0).ChildNodes;
            var counter = 0;
            foreach (XmlNode pattern in patterns)
            {
                tree.Nodes.Add(GetConfigPattern(pattern, counter));
                counter++;
            }
            return null;
        }
        configBlock.Text =
            $@"<b>{Resources.NCMWebContent.WEBDATA_VM_102}{startLine}&nbsp;&nbsp;&nbsp;{Resources.NCMWebContent.WEBDATA_VM_103} {lineNumber}</b>";
        configBlock.Tag = index;
        configBlock.ShowExpand = true;
        configBlock.TargetUrl =
            $@"javascript:parent.location.replace('/Orion/NCM/ConfigDetails.aspx?ConfigID={currentConfigID}&ReportID={currentReportID.Trim('{', '}')}&FoundLineNumber={lineNumber}')";
        configBlock.CssClass = @"Property";
        return configBlock;
    }

    private Node GetConfigPattern(XmlNode pattern, int index)
    {
        var patternBlock = new Node();
        patternBlock.Tag = $@"pattern_{index}";
        patternBlock.ImageUrl = ResolveUrl("/Orion/NCM/Resources/images/pattern_picture.jpg");
        var foundMatch = Convert.ToBoolean(pattern.Attributes["FM"].InnerText);
        if (pattern.Attributes["ALL"] != null)
        {
            patternBlock.DataPath = pattern.Attributes["ALL"].InnerText;
        }
        string foundText;
        if (foundMatch)
        {
            foundText = Resources.NCMWebContent.WEBDATA_VM_98;
        }
        else
        {
            foundText = Resources.NCMWebContent.WEBDATA_VM_99;
        }
        var patternText = pattern.Attributes["PT"].InnerText;
        patternBlock.Text = $@"{Resources.NCMWebContent.WEBDATA_VM_101} '{patternText}' {foundText}";
        var lines = pattern.ChildNodes;
        if (lines.Count > 0)
        {
            patternBlock.ShowExpand = true;
        }
        patternBlock.CssClass = @"Property";
        return patternBlock;
    }

    private Node GetConfigLine(XmlNode line)
    {
        var configLine = new Node();
        var onlineNumber = Convert.ToInt32(line.Attributes["FLN"].InnerText);
        var onlineText = line.Attributes["FL"].InnerText;
        configLine.Text = $@"{Resources.NCMWebContent.WEBDATA_VM_100}{onlineNumber} '{onlineText}'";
        configLine.TargetUrl =
            $@"javascript:parent.location.replace('/Orion/NCM/ConfigDetails.aspx?ConfigID={currentConfigID}&ReportID={currentReportID.Trim('{', '}')}&FoundLineNumber={onlineNumber}')";
        configLine.CssClass = @"Property";
        return configLine;
    }

    #endregion
}