﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Data;
using System.Text;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;

public partial class Orion_NCM_Controls_AddRemoveNodesControl : System.Web.UI.UserControl
{
    [PersistenceMode(PersistenceMode.Attribute)]
    public string Prefix { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool HideUnmanageNodes { get; set; }

    public List<Guid> NodeIDList
    {
        get
        {
            var nodeIDList = (List<Guid>)Session[$@"{Prefix}_NodeIDList"];
            if (nodeIDList == null)
            {
                nodeIDList = new List<Guid>();
            }
            return nodeIDList;
        }
        set
        {
            var nodeIDList = value;
            if (nodeIDList == null)
            {
                nodeIDList = new List<Guid>();
            }
            Session[$@"{Prefix}_NodeIDList"] = nodeIDList;
        }
    }

    private string GroupBy
    {
        get
        {
            var groupBy = WebUserSettingsDAL.Get($@"{Prefix}_GroupBy");
            if (string.IsNullOrEmpty(groupBy))
            {
                groupBy = @"Vendor";
            }
            return groupBy;
        }
        set
        {
            WebUserSettingsDAL.Set($@"{Prefix}_GroupBy", value);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ErrorControlHolder.Visible = false;
        ClearSearchButtonsHolder.Visible = (!string.IsNullOrEmpty(leftTreePanel.SearchTerm) && !string.IsNullOrEmpty(rightTreePanel.SearchTerm));

        leftTreePanel.HideUnmanageNodes = HideUnmanageNodes;
        rightTreePanel.HideUnmanageNodes = HideUnmanageNodes;

        if (!Page.IsPostBack)
        {
            InitilizeGroupByList();
        }

        leftTreePanel.GroupBy = GroupBy;
        rightTreePanel.GroupBy = GroupBy;

        leftTreePanel.NodeIDList = NodeIDList;
        rightTreePanel.NodeIDList = NodeIDList;

        if (!Page.IsPostBack)
        {
            try
            {
                leftTreePanel.Refresh();
                rightTreePanel.Refresh();
            }
            catch (Exception ex)
            {
                ShowErrorBar(ex);

                NodeIDList = null;

                leftTreePanel.NodeIDList = NodeIDList;
                rightTreePanel.NodeIDList = NodeIDList;

                leftTreePanel.Refresh();
                rightTreePanel.Refresh();
            }
        }
    }

    protected void ddlGroupBy_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        GroupBy = ddlGroupBy.SelectedValue;
        leftTreePanel.GroupBy = GroupBy;
        rightTreePanel.GroupBy = GroupBy;

        leftTreePanel.Refresh();
        rightTreePanel.Refresh();
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        var groups = leftTreePanel.GetCheckedGroups();
        rightTreePanel.AddGroups(groups);

        var nodes = leftTreePanel.GetCheckedNodes();
        rightTreePanel.AddNodes(nodes);

        leftTreePanel.RemoveNodes(nodes);
        leftTreePanel.RemoveGroups(groups);

        var searchTerm = leftTreePanel.SearchTerm;
        UpdateNodeIDList(nodes, true, searchTerm);
        UpdateNodeIDList(groups, true, searchTerm);
    }

    protected void btnRemove_Click(object sender, EventArgs e)
    {
        var groups = rightTreePanel.GetCheckedGroups();
        leftTreePanel.AddGroups(groups);

        var nodes = rightTreePanel.GetCheckedNodes();
        leftTreePanel.AddNodes(nodes);

        rightTreePanel.RemoveNodes(nodes);
        rightTreePanel.RemoveGroups(groups);

        var searchTerm = rightTreePanel.SearchTerm;
        UpdateNodeIDList(nodes, false, searchTerm);
        UpdateNodeIDList(groups, false, searchTerm);
    }

    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        DoSearch();
    }

    protected void btnClearSearch_Click(object sender, ImageClickEventArgs e)
    {
        CleanSearch();
    }

    private void ShowErrorBar(Exception ex)
    {
        ErrorControlHolder.Visible = true;
        if (ex is System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>)
        {
            errorControl.ExceptionMessage = (ex as System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>).Detail.Message;
        }
        else
        {
            if (ex.InnerException != null)
            {
                errorControl.ExceptionMessage = ex.InnerException.Message;
            }
            else
            {
                errorControl.ExceptionMessage = ex.Message;
            }
        }
    }

    private void DoSearch()
    {
        var searchTerm = txtSearchTerm.Text;
        if (!string.IsNullOrEmpty(searchTerm))
        {
            leftTreePanel.SearchTerm = searchTerm;
            rightTreePanel.SearchTerm = searchTerm;

            leftTreePanel.Refresh();
            rightTreePanel.Refresh();
        }
        ClearSearchButtonsHolder.Visible = !string.IsNullOrEmpty(searchTerm);
    }

    private void CleanSearch()
    {
        var searchTerm = string.Empty;
        txtSearchTerm.Text = searchTerm;

        leftTreePanel.SearchTerm = searchTerm;
        rightTreePanel.SearchTerm = searchTerm;

        leftTreePanel.Refresh();
        rightTreePanel.Refresh();

        ClearSearchButtonsHolder.Visible = !string.IsNullOrEmpty(searchTerm);
    }

    private void UpdateNodeIDList(List<NodeUIEntry> nodes, bool add, string searchTerm)
    {
        if (nodes != null && nodes.Count > 0)
        {
            var whereClause = new StringBuilder();
            var or = string.Empty;
            foreach (var node in nodes)
            {
                if (node.DataPath.StartsWith(@"G:"))
                {
                    var group = node.DataPath.Replace(@"G:", string.Empty).Replace(@"'", @"''");
                    if (string.IsNullOrEmpty(group))
                    {
                        SolarWinds.Orion.Core.Common.Models.CustomProperty cp;
                        if (CustomPropertyHelper.IsCustomProperty(GroupBy, out cp))
                        {
                            if (cp.PropertyType == typeof(String))
                            {
                                whereClause.AppendFormat(@" {1} ({0} = '' OR {0} IS NULL) ", CustomPropertyHelper.DecodePropertyName(CustomPropertyHelper.CovertGroupingPropertyName(GroupBy)), or);
                            }
                            else
                            {
                                whereClause.AppendFormat(@" {1} {0} IS NULL ", CustomPropertyHelper.DecodePropertyName(CustomPropertyHelper.CovertGroupingPropertyName(GroupBy)), or);
                            }
                        }
                        else
                        {
                            whereClause.AppendFormat(@" {1} ({0} = '' OR {0} IS NULL) ", CustomPropertyHelper.DecodePropertyName(CustomPropertyHelper.CovertGroupingPropertyName(GroupBy)), or);
                        }
                    }
                    else
                    {
                        whereClause.AppendFormat(@" {1} {0} = '{2}'  ", CustomPropertyHelper.DecodePropertyName(CustomPropertyHelper.CovertGroupingPropertyName(GroupBy)), or, group);
                    }
                    or = @" OR ";
                }

                if (node.DataPath.StartsWith(@"N:"))
                {
                    var nodeIDGuid = new Guid(node.DataPath.Substring(2));
                    if (add)
                    {
                        NodeIDList.Add(nodeIDGuid);
                    }
                    else
                    {
                        NodeIDList.Remove(nodeIDGuid);
                    }
                }
            }

            if (whereClause.Length > 0)
            {
                var query = new StringBuilder();
                query.AppendFormat(@"SELECT NCMNodeProperties.NodeID FROM Cirrus.NodeProperties AS NCMNodeProperties
INNER JOIN Orion.Nodes AS Nodes ON NCMNodeProperties.CoreNodeID=Nodes.NodeID
INNER JOIN Orion.NodesCustomProperties AS OrionCustomProperties ON NCMNodeProperties.CoreNodeID=OrionCustomProperties.NodeID WHERE ({0})",
                    whereClause);
                if (HideUnmanageNodes)
                {
                    query.Append(@" AND Nodes.Status<>9 ");
                }

                if (string.IsNullOrEmpty(searchTerm))
                {
                    query.Append(@" AND (1=1) ");
                }
                else
                {
                    query.AppendFormat(@" AND (Nodes.IPAddress LIKE '%{0}%' OR Nodes.Caption LIKE '%{0}%') ", SearchHelper.WildcardToLike(searchTerm.Replace(@"'", @"''")));
                }

                var isWrapper = new ISWrapper();
                using (var proxy = isWrapper.Proxy)
                {
                    var dt = proxy.Query(query.ToString());
                    if (add)
                        NodeIDList.AddRange(dt.AsEnumerable().Select(row => row.Field<Guid>("NodeID")).ToList());
                    else
                        NodeIDList = NodeIDList.Except(dt.AsEnumerable().Select(row => row.Field<Guid>("NodeID")).ToList()).ToList();
                }
            }

            NodeIDList = NodeIDList.Distinct().ToList();
        }
    }

    private void InitilizeGroupByList()
    {
        ddlGroupBy.DataSource = GroupByDataSource();
        ddlGroupBy.DataTextField = @"Value";
        ddlGroupBy.DataValueField = @"Key";
        ddlGroupBy.DataBind();

        if (ddlGroupBy.Items.FindByValue(GroupBy) == null)
        {
            GroupBy = @"Vendor";
        }
        ddlGroupBy.SelectedValue = GroupBy;
    }

    private Dictionary<string, string> GroupByDataSource()
    {
        var dataSource = new Dictionary<string, string>
        {
            { @"None", Resources.NCMWebContent.WEBDATA_VK_272 },
            { @"NodeGroup", Resources.NCMWebContent.WEBDATA_VK_273 },
            { @"Status", Resources.NCMWebContent.WEBDATA_VK_274 },
            { @"SysContact", Resources.NCMWebContent.WEBDATA_VK_275 },
            { @"SysLocation", Resources.NCMWebContent.WEBDATA_VK_276 },
            { @"SystemOID", Resources.NCMWebContent.WEBDATA_VK_277 },
            { @"Vendor", Resources.NCMWebContent.WEBDATA_VK_278 },
            { @"MachineType", Resources.NCMWebContent.WEBDATA_VK_279 },
            { @"OSImage", Resources.NCMWebContent.WEBDATA_VK_280 },
            { @"OSVersion", Resources.NCMWebContent.WEBDATA_VK_281 },
            { @"ConfigTypes", Resources.NCMWebContent.WEBDATA_VK_282 },
            { @"EnableLevel", Resources.NCMWebContent.WEBDATA_VK_283 },
            { @"ExecProtocol", Resources.NCMWebContent.WEBDATA_VK_284 },
            { @"CommandProtocol", Resources.NCMWebContent.WEBDATA_VK_285 },
            { @"TransferProtocol", Resources.NCMWebContent.WEBDATA_VK_286 },
            { @"LoginStatus", Resources.NCMWebContent.WEBDATA_VK_287 }
        };
        foreach (var customProperty in CustomPropertyHelper.GetCustomProperties())
        {
            dataSource.Add(customProperty, customProperty);
        }

        return dataSource;
    }
}