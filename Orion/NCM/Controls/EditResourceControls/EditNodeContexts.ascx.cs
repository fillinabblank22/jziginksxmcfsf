﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NCM_Controls_EditResourceControls_EditNodeContexts : BaseResourceEditControl
{
    public override Dictionary<string, object> Properties
    {
        get
        {
            var properties = new Dictionary<string, object> {{@"PageSize", txtPageSize.Text}};
            return properties;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        SetupControl();
        base.OnInit(e);
    }

    private void SetupControl()
    {
        string pageSize = @"5";
        if (!string.IsNullOrEmpty(Resource.Properties[@"PageSize"]))
        {
            pageSize = Resource.Properties[@"PageSize"];
        }
        txtPageSize.Text = pageSize;
    }
}