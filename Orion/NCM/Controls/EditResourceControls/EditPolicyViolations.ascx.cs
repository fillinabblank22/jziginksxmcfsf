﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;

using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_Controls_EditResourceControls_EditPolicyViolations : BaseResourceEditControl
{
    public override System.Collections.Generic.Dictionary<string, object> Properties
    {
        get
        {
            var policyReportIDs = new List<string>();
            foreach (ListItem item in chkList.Items)
            {
                if (item.Selected)
                {
                    policyReportIDs.Add(string.Format(CultureInfo.InvariantCulture, @"'{0}'", item.Value));
                }
            }
            var properties = new Dictionary<string, object>();
            properties.Add(@"PolicyReportIDs", string.Join(@",", policyReportIDs));
            return properties;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        SetupControl();
        base.OnInit(e);
    }

    protected void ValidateSelection(object source, ServerValidateEventArgs args)
    {
        if (chkList.Items.Count == 0)
        {
            args.IsValid = true;
            return;
        }

        foreach (ListItem item in chkList.Items)
        {
            if (item.Selected)
            {
                args.IsValid = true;
                return;
            }
        }
        args.IsValid = false;
    }

    private void SetupControl()
    {
        IEnumerable<string> policyReportIDs = null;
        if (Resource.Properties[@"PolicyReportIDs"] == null)
        {
            policyReportIDs = GetTop4PolicyReportIDs();
        }
        else
        {
            policyReportIDs = Resource.Properties[@"PolicyReportIDs"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(item => item.Trim(new char[] { '\'' }));
        }

        var isWrapper = new ISWrapper();
        using (var proxy = isWrapper.Proxy)
        {
            var dt = proxy.Query(@"SELECT PolicyReportID, Name FROM Cirrus.PolicyReports ORDER BY Name");
            if (dt != null && dt.Rows.Count > 0)
            {
                emptyMsg.Visible = false;
                foreach (DataRow row in dt.Rows)
                {
                    var policyReportName = Convert.ToString(row["Name"], CultureInfo.InvariantCulture);
                    var policyReportID = Convert.ToString(row["PolicyReportID"], CultureInfo.InvariantCulture);
                    var item = new ListItem(policyReportName, policyReportID);
                    item.Selected = policyReportIDs.Contains(policyReportID, StringComparer.InvariantCultureIgnoreCase);
                    chkList.Items.Add(item);
                }
            }
            else
            {
                emptyMsg.Visible = true;
            }
        }
    }

    private IEnumerable<string> GetTop4PolicyReportIDs()
    {
        var isWrapper = new ISWrapper();
        using (var proxy = isWrapper.Proxy)
        {
            IEnumerable<string> policyReportIDs = proxy.Query(string.Format(CultureInfo.InvariantCulture, @"SELECT TOP 4 PolicyReportID
            FROM Cirrus.PolicyReports
            ORDER BY Name")).AsEnumerable().Select(row => Convert.ToString(row.Field<Guid>("PolicyReportID"), CultureInfo.InvariantCulture));
            return policyReportIDs;
        }
    }
}