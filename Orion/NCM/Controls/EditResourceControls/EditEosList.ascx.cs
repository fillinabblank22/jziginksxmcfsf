﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_Controls_EditResourceControls_EditEosList : BaseResourceEditControl
{
    private const string _defaultPeriod = "Next 3 months";

    public override bool ShowSubTitle
    {
        get
        {
            return false;
        }
    }

    public override System.Collections.Generic.Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            properties.Add(@"Period", ddlPeriod.SelectedValue);
            return properties;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        InitPeriodList();

        string period = Resource.Properties[@"Period"];
        if (string.IsNullOrEmpty(period))
        {
            ddlPeriod.SelectedValue = _defaultPeriod;
        }
        else
        {
            ddlPeriod.SelectedValue = period;
        }

        base.OnInit(e);
    }

    private void InitPeriodList()
    {
        ddlPeriod.Items.Clear();
        Dictionary<string, string> items = TimeParser.GenerateTimePeriods();
        foreach (KeyValuePair<string, string> item in items)
        {
            ddlPeriod.Items.Add(new ListItem(item.Value, item.Key));
        }
    }
}