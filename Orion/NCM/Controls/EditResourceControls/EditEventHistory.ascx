<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditEventHistory.ascx.cs" Inherits="Orion_NCM_Controls_EditResourceControls_EditEventHistory" %>

<%@ Register Assembly="Infragistics2.WebUI.WebDataInput.v8.2, Version=8.2.20082.1000, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.WebDataInput" TagPrefix="igtxt" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
    <asp:UpdatePanel ID="uPanel" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <table>
                <tr>
                    <td style="padding-top:5px;">
                        <b><%=Resources.NCMWebContent.WEBDATA_VK_142 %></b>
                        <div style="padding-top:5px;">
                            <asp:DropDownList id="PeriodListDropDown" runat="server" AutoPostBack="True" OnSelectedIndexChanged="PeriodListDropDown_SelectedIndexChanged"/>
                        </div>
                        <div id="datePeriodHolder" runat="server" style="padding-top:5px;">
                            <table>
                                <tr>
                                    <td valign="top"><ncm:Calendar ID="dateTimeFrom" AlternativeDrawingMode="true" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_157 %>" runat="server" /></td>
                                    <td valign="top" style="padding-left:10px;"><ncm:Calendar ID="dateTimeTo" AlternativeDrawingMode="true" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_158 %>" runat="server" /></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                     <td style="padding-top:5px;">
                        <b><%=Resources.NCMWebContent.WEBDATA_VK_143 %></b>
                        <div style="padding-top:5px;">
                            <asp:CheckBox ID="chbLastXEvents" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_144 %>" AutoPostBack="true" OnCheckedChanged="chbLastXEvents_CheckedChanged"/>
                        </div>
                        <div style="padding-top:5px;">
                            <igtxt:WebNumericEdit ID="lastXNumeric" runat="server" HorizontalAlign="Center" Width="60" MaxValue="100" MinValue="1" MaxLength="3">
                                <SpinButtons Display="OnRight" />
                            </igtxt:WebNumericEdit>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:5px;">
                        <b><%=Resources.NCMWebContent.WEBDATA_VK_145 %></b>
                        <div style="padding-top:5px;">
                            <span><orion:LocalizableButton ID="btnSelectAll" runat="server" DisplayType="Small" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_146 %>"  OnClientClick="SelectAllClick(true); return false;" CausesValidation="false"></orion:LocalizableButton></span>
                            <span><orion:LocalizableButton ID="btnClearAll" runat="server" DisplayType="Small" LocalizedText="CustomText" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_147 %>" OnClientClick="SelectAllClick(false); return false;" CausesValidation="false"></orion:LocalizableButton></span>
                        </div>
                        <div style="padding-top:5px;">
                            <table>
                                <tr>
                                    <td><asp:CheckBox ID="chbConfigDownload" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_148 %>" Checked="true"/></td>
                                    <td><asp:CheckBox ID="chbUploadConfigByUser" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_149 %>" Checked="true"/></td>
                                </tr>
                                <tr>
                                    <td><asp:CheckBox ID="chbDownloadConfigByJob" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_150 %>"/></td>
                                    <td><asp:CheckBox ID="chbUploadConfigByJob" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_151 %>" Checked="true"/></td>
                                </tr>
                                <tr>
                                    <td><asp:CheckBox ID="chbDownloadConfigByUser" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_152 %>"/></td>
                                    <td><asp:CheckBox ID="chbUploadScripts" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_153 %>" Checked="true"/></td>
                                </tr>
                                <tr>
                                    <td><asp:CheckBox ID="chbEditConfig" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_154 %>"/></td>
                                    <td><asp:CheckBox ID="chbUploadScriptsByJob" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_155 %>" Checked="true"/></td>
                                </tr>
                                <tr>
                                    <td><asp:CheckBox ID="chbDeleteConfig" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_156 %>"/></td>
                                    <td><asp:CheckBox ID="chbEndOfSupport" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_IC_198 %>"/></td>
                                </tr>
                                <tr>
                                    <td><asp:CheckBox ID="chbConfigChangeApproval" runat="server" Text="<%$ Resources: NCMWebContent, EditResourceControls_EditEventHistory_ConfigChangeApproval %>"/></td>
                                    <td><asp:CheckBox ID="chbVulnerabilityStateChange" runat="server" Text="<%$ Resources: NCMWebContent, EditResourceControls_EditEventHistory_VulnerabilityStateChange %>"/></td>
                                </tr>
                                <tr>
                                    <td><asp:CheckBox ID="chbFirmwareUpgrade" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_1044 %>"/></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <script type="text/javascript">
        function SelectAllClick(checked) {
            document.getElementById("<%=chbDeleteConfig.ClientID%>").checked = checked;
            document.getElementById("<%=chbUploadConfigByUser.ClientID%>").checked = checked;
            document.getElementById("<%=chbConfigDownload.ClientID%>").checked = checked;
            document.getElementById("<%=chbDownloadConfigByJob.ClientID%>").checked = checked;
            document.getElementById("<%=chbDownloadConfigByUser.ClientID%>").checked = checked;
            document.getElementById("<%=chbUploadConfigByJob.ClientID%>").checked = checked;
            document.getElementById("<%=chbEditConfig.ClientID%>").checked = checked;
            document.getElementById("<%=chbUploadScripts.ClientID%>").checked = checked;
            document.getElementById("<%=chbUploadScriptsByJob.ClientID%>").checked = checked;
            document.getElementById("<%=chbEndOfSupport.ClientID%>").checked = checked;
            document.getElementById("<%=chbConfigChangeApproval.ClientID%>").checked = checked;
            document.getElementById("<%=chbVulnerabilityStateChange.ClientID%>").checked = checked;
            document.getElementById("<%=chbFirmwareUpgrade.ClientID%>").checked = checked;
        }
    </script>
