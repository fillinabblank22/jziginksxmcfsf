﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditPolicyViolations.ascx.cs" Inherits="Orion_NCM_Controls_EditResourceControls_EditPolicyViolations" %>

<div>
    <div style="font-weight:bold;">
        <%=Resources.NCMWebContent.WEBDATA_VK_967 %>
    </div>
    <asp:CheckBoxList ID="chkList" runat="server" RepeatColumns="2" CssClass="policy-report-list">
    </asp:CheckBoxList>
    <div id="emptyMsg" runat="server" style="color:Red;padding:5px 0px 0px 5px;">
        <%=Resources.NCMWebContent.WEBDATA_VK_981 %>
    </div>
    <div style="padding-top:5px;">
        <asp:CustomValidator ID="validator" runat="server" OnServerValidate="ValidateSelection" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_968 %>" EnableClientScript="false" Display="Dynamic"></asp:CustomValidator>
    </div>
</div>

<style type="text/css">
   .policy-report-list tr > td:last-of-type { padding-left:20px; }
</style>