﻿using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.NCM.Common.Dal.ConfigTypes;

public partial class Orion_NCM_Controls_EditResourceControls_EditInterfaceConfigSnippet : BaseResourceEditControl
{
    private readonly IConfigTypesProvider configTypesProvider;

    public Orion_NCM_Controls_EditResourceControls_EditInterfaceConfigSnippet()
    {
        configTypesProvider = ServiceLocator.Container.Resolve<IConfigTypesProvider>();
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            var properties = new Dictionary<string, object> { { @"ConfigType", rbConfigType.SelectedValue } };
            return properties;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        SetupControl();
        base.OnInit(e);
    }

    private void SetupControl()
    {
        rbConfigType.Items.Clear();
        foreach (var configType in configTypesProvider.GetAll())
        {
            rbConfigType.Items.Add(new ListItem(configType.Name, configType.Name));
        }

        var selectedValue = Resource.Properties[@"ConfigType"] ?? @"Running";
        rbConfigType.SelectedValue = rbConfigType.Items.FindByValue(selectedValue) != null ? selectedValue : @"Running";
    }
}