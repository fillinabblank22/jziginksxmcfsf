﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditInterfaceConfigSnippet.ascx.cs" Inherits="Orion_NCM_Controls_EditResourceControls_EditInterfaceConfigSnippet" %>

<div>
    <b><%=Resources.NCMWebContent.WEBDATA_VK_348%></b>
    <div style="padding-top:5px;">
        <asp:RadioButtonList runat="server" ID="rbConfigType">
        </asp:RadioButtonList>
    </div>
</div>
