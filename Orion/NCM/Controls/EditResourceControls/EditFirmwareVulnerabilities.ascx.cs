﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCM.Contracts.InformationService;

public partial class Orion_NCM_Controls_EditResourceControls_EditFirmwareVulnerabilities : BaseResourceEditControl
{
    private bool SupportNetObjectType { get { return Resource.View.NetObjectType != null; } }

    [Localizable(false)]
    public override System.Collections.Generic.Dictionary<string, object> Properties
    {
        get
        {
            List<string> list = new List<string>();
            foreach (ListItem item in chkList.Items)
            {
                if (item.Selected)
                {
                    list.Add(item.Value);
                }
            }
            Dictionary<string, object> properties = new Dictionary<string, object>();           
            properties.Add("FirmwareVulnerabilitiesStates", string.Join(",", list)); ;
            properties.Add("FirmwareVulnerabilitiesPageSize", txtPageSize.Text);
            return properties;
        }
    }

    protected override void OnInit(EventArgs e)
    {      
        SetupControl();
        base.OnInit(e);
    }

    protected void ValidateVulnStateSelection(object source, ServerValidateEventArgs args)
    {
        foreach (ListItem item in chkList.Items)
        {
            if (item.Selected)
            {
                args.IsValid = true;
                return;
            }
        }
        args.IsValid = false;
    }

    private void SetupControl()
    {      
        {
            chkList.Items.Clear();
            chkList.Items.Add(new ListItem(Resources.NCMWebContent.WEBDATA_VK_993, ((int)eVulnerabilityState.Potential_vulnerability).ToString()));
            chkList.Items.Add(new ListItem(Resources.NCMWebContent.WEBDATA_VK_994, ((int)eVulnerabilityState.Confirmed_vulnerability).ToString()));
            chkList.Items.Add(new ListItem(Resources.NCMWebContent.WEBDATA_VK_996, ((int)eVulnerabilityState.Not_applicable).ToString()));
            chkList.Items.Add(new ListItem(Resources.NCMWebContent.WEBDATA_VK_1035, ((int)eVulnerabilityState.Remediation_planned).ToString()));
            chkList.Items.Add(new ListItem(Resources.NCMWebContent.WEBDATA_VK_995, ((int)eVulnerabilityState.Remediated).ToString()));
            chkList.Items.Add(new ListItem(Resources.NCMWebContent.WEBDATA_VK_1036, ((int)eVulnerabilityState.Waiver).ToString()));

            List<eVulnerabilityState> vulnStates = new List<eVulnerabilityState>();
            if (string.IsNullOrEmpty(Resource.Properties[@"FirmwareVulnerabilitiesStates"]))
            {
                if (SupportNetObjectType)
                {
                    
                    //node details default
                    vulnStates.Add(eVulnerabilityState.Potential_vulnerability);
                    vulnStates.Add(eVulnerabilityState.Confirmed_vulnerability);
                }
                else
                {
                    //summary default
                    vulnStates.Add(eVulnerabilityState.Potential_vulnerability);
                    vulnStates.Add(eVulnerabilityState.Confirmed_vulnerability);
                    vulnStates.Add(eVulnerabilityState.Not_applicable);                   
                    vulnStates.Add(eVulnerabilityState.Remediation_planned);
                    vulnStates.Add(eVulnerabilityState.Remediated);
                    vulnStates.Add(eVulnerabilityState.Waiver);
                }

            }
            else
            {
                vulnStates = Resource.Properties[@"FirmwareVulnerabilitiesStates"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(item => (eVulnerabilityState)Enum.Parse(typeof(eVulnerabilityState), item)).ToList();
            }

            foreach (ListItem item in chkList.Items)
            {
                item.Selected = vulnStates.Contains((eVulnerabilityState)Enum.Parse(typeof(eVulnerabilityState), item.Value));
            }
        }

        string pageSize = @"5";
        if (!string.IsNullOrEmpty(Resource.Properties[@"FirmwareVulnerabilitiesPageSize"]))
        {
            pageSize = Resource.Properties[@"FirmwareVulnerabilitiesPageSize"];
        }
        txtPageSize.Text = pageSize;
    }
}