using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Text;

public partial class Orion_NCM_Controls_EditResourceControls_EditEventHistory : BaseResourceEditControl
{
    private const string selectionScriptKey = "F2A265D1-DE50-407F-BDF0-E4C7586741B2";
    private const string selectionScriptBodyTemplate = @"
            document.getElementById('{0}').checked = checked; 
            document.getElementById('{1}').checked = checked; 
            document.getElementById('{2}').checked = checked;
            document.getElementById('{3}').checked = checked;
            document.getElementById('{4}').checked = checked;
            document.getElementById('{5}').checked = checked;
            document.getElementById('{6}').checked = checked;
            document.getElementById('{7}').checked = checked;
            document.getElementById('{8}').checked = checked;
            document.getElementById('{9}').checked = checked;
            document.getElementById('{10}').checked = checked;
            document.getElementById('{11}').checked = checked;
            document.getElementById('{12}').checked = checked;";

    protected override void OnInit(EventArgs e)
    {
        OnInitPeriodList();
        SetupControlFromRequest();

        base.OnInit(e);

        //System.Web.UI.ScriptManager scriptManager = System.Web.UI.ScriptManager.GetCurrent(this.Page);
        //scriptManager.EnableScriptGlobalization = true;
        //scriptManager.EnableScriptLocalization = true;
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        RegisterSelectAllScript(); //Register SelectAllClick client function using ClientScriptManager to correctly initialize client's DOM
    }

    public override bool ShowSubTitle
    {
        get { return false; }
    }

    [Localizable(false)]
    public override System.Collections.Generic.Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            if (PeriodListDropDown.SelectedValue == "Custom")
            {
                if (!dateTimeFrom.ValidateDate() || !dateTimeTo.ValidateDate()) return null;

                properties.Add("DateFrom", dateTimeFrom.TimeSpan.ToString(CultureInfo.InvariantCulture));
                properties.Add("DateTo", dateTimeTo.TimeSpan.ToString(CultureInfo.InvariantCulture));
            }
            else
            {
                properties.Add(@"DateFrom", string.Empty);
                properties.Add(@"DateTo", string.Empty);
            }
            properties.Add("Period", PeriodListDropDown.SelectedValue);

            properties.Add("LastX", lastXNumeric.Text);
            properties.Add("chbDisplay LastX Events", chbLastXEvents.Checked.ToString());

            properties.Add("chbConfig downloaded", chbConfigDownload.Checked.ToString());
            properties.Add("chbDownload Config requested by user", chbDownloadConfigByUser.Checked.ToString());
            properties.Add("chbEdit Config", chbEditConfig.Checked.ToString());
            properties.Add("chbDelete Config", chbDeleteConfig.Checked.ToString());
            properties.Add("chbUpload Config requested by user", chbUploadConfigByUser.Checked.ToString());
            properties.Add("chbDownload Config requested by job", chbDownloadConfigByJob.Checked.ToString());
            properties.Add("chbUpload Config requested by job", chbUploadConfigByJob.Checked.ToString());
            properties.Add("chbUpload Scripts requested by user", chbUploadScripts.Checked.ToString());
            properties.Add("chbUpload Scripts requested by job", chbUploadScriptsByJob.Checked.ToString());
            properties.Add("chbAssign EOS dates", chbEndOfSupport.Checked.ToString());
            properties.Add("chbConfig Change Approval", chbConfigChangeApproval.Checked.ToString());
            properties.Add("chbVulnerability State Change", chbVulnerabilityStateChange.Checked.ToString());
            properties.Add("chbFirmware Upgrade", chbFirmwareUpgrade.Checked.ToString());
            return properties;
        }
    }

    private void SetupControlFromRequest()
    {
        Page.Title = $@"{Resources.NCMWebContent.WEBDATA_VK_127} {Resource.Title}";

        if (!string.IsNullOrEmpty(Resource.Properties[@"Period"]))
        {
            PeriodListDropDown.SelectedValue = Resource.Properties[@"Period"];
        }
        else
        {
            PeriodListDropDown.SelectedValue = @"Last 7 Days";
        }

        SetupDateTimePeriod();

        SetUpLastXEvents();
        lastXNumeric.Enabled = chbLastXEvents.Checked;
        bool visible = Resource.Properties[@"Period"] == @"Custom" ? true : false;
        datePeriodHolder.Visible = visible;

        SetupCheckboxes();

    }

    [Localizable(false)]
    private void SetupDateTimePeriod()
    {
        if (string.IsNullOrEmpty(Resource.Properties["DateFrom"]))
        {
            dateTimeFrom.TimeSpan = DateTime.Now.AddDays(-7);
        }
        else
        {
            dateTimeFrom.TimeSpan = Convert.ToDateTime(Resource.Properties["DateFrom"], CultureInfo.InvariantCulture);
        }

        if (string.IsNullOrEmpty(Resource.Properties["DateTo"]))
        {
            dateTimeTo.TimeSpan = DateTime.Now;
        }
        else
        {
            dateTimeTo.TimeSpan = Convert.ToDateTime(Resource.Properties["DateTo"], CultureInfo.InvariantCulture);
        }
    }

    [Localizable(false)]
    private void SetUpLastXEvents()
    {
        if (!string.IsNullOrEmpty(Resource.Properties["LastX"]))
            lastXNumeric.Text = int.Parse(Resource.Properties["LastX"]).ToString();
        else
            lastXNumeric.Text = "10";

        if (!string.IsNullOrEmpty(Resource.Properties["chbDisplay LastX Events"]))
            chbLastXEvents.Checked = Convert.ToBoolean(Resource.Properties["chbDisplay LastX Events"]);
        else
            chbLastXEvents.Checked = false;
    }

    [Localizable(false)]
    private void SetupCheckboxes()
    {
        if (!string.IsNullOrEmpty(Resource.Properties["chbConfig downloaded"]))
            chbConfigDownload.Checked = Convert.ToBoolean(Resource.Properties["chbConfig downloaded"]);

        if (!string.IsNullOrEmpty(Resource.Properties["chbDownload Config requested by user"]))
            chbDownloadConfigByUser.Checked = Convert.ToBoolean(Resource.Properties["chbDownload Config requested by user"]);

        if (!string.IsNullOrEmpty(Resource.Properties["chbEdit Config"]))
            chbEditConfig.Checked = Convert.ToBoolean(Resource.Properties["chbEdit Config"]);

        if (!string.IsNullOrEmpty(Resource.Properties["chbDelete Config"]))
            chbDeleteConfig.Checked = Convert.ToBoolean(Resource.Properties["chbDelete Config"]);

        if (!string.IsNullOrEmpty(Resource.Properties["chbUpload Config requested by user"]))
            chbUploadConfigByUser.Checked = Convert.ToBoolean(Resource.Properties["chbUpload Config requested by user"]);

        if (!string.IsNullOrEmpty(Resource.Properties["chbDownload Config requested by job"]))
            chbDownloadConfigByJob.Checked = Convert.ToBoolean(Resource.Properties["chbDownload Config requested by job"]);

        if (!string.IsNullOrEmpty(Resource.Properties["chbUpload Config requested by job"]))
            chbUploadConfigByJob.Checked = Convert.ToBoolean(Resource.Properties["chbUpload Config requested by job"]);

        if (!string.IsNullOrEmpty(Resource.Properties["chbUpload Scripts requested by user"]))
            chbUploadScripts.Checked = Convert.ToBoolean(Resource.Properties["chbUpload Scripts requested by user"]);

        if (!string.IsNullOrEmpty(Resource.Properties["chbUpload Scripts requested by job"]))
            chbUploadScriptsByJob.Checked = Convert.ToBoolean(Resource.Properties["chbUpload Scripts requested by job"]);

        if (!string.IsNullOrEmpty(Resource.Properties["chbAssign EOS dates"]))
            chbEndOfSupport.Checked = Convert.ToBoolean(Resource.Properties["chbAssign EOS dates"]);

        if (!string.IsNullOrEmpty(Resource.Properties["chbConfig Change Approval"]))
            chbConfigChangeApproval.Checked = Convert.ToBoolean(Resource.Properties["chbConfig Change Approval"]);

        if (!string.IsNullOrEmpty(Resource.Properties["chbVulnerability State Change"]))
            chbVulnerabilityStateChange.Checked = Convert.ToBoolean(Resource.Properties["chbVulnerability State Change"]);

        if (!string.IsNullOrEmpty(Resource.Properties["chbFirmware Upgrade"]))
            chbFirmwareUpgrade.Checked = Convert.ToBoolean(Resource.Properties["chbFirmware Upgrade"]);

    }

    private void OnInitPeriodList()
    {
        Dictionary<string, string> items = SolarWinds.NCMModule.Web.Resources.TimeParser.GeneratePeriods();
        foreach (KeyValuePair<string, string> item in items)
        {
            PeriodListDropDown.Items.Add(new ListItem(item.Value, item.Key));
        }
    }

    private string BuildSelectAllScript()
    {
        StringBuilder sb = new StringBuilder();

        sb.Append(@"function SelectAllClick(checked){");
        sb.AppendFormat(selectionScriptBodyTemplate, 
                        this.chbDeleteConfig.ClientID, this.chbUploadConfigByUser.ClientID,
                        this.chbConfigDownload.ClientID, this.chbDownloadConfigByJob.ClientID, 
                        this.chbDownloadConfigByUser.ClientID, this.chbUploadConfigByJob.ClientID, 
                        this.chbEditConfig.ClientID, this.chbUploadScripts.ClientID, 
                        this.chbUploadScriptsByJob.ClientID, this.chbEndOfSupport.ClientID, 
                        this.chbConfigChangeApproval.ClientID, this.chbVulnerabilityStateChange.ClientID,
                        this.chbFirmwareUpgrade.ClientID);
        sb.Append(@"}");

        return sb.ToString();
    }

    private void RegisterSelectAllScript()
    {
        ScriptManager.RegisterClientScriptBlock(this.Page,
            this.GetType(),
            selectionScriptKey,
            BuildSelectAllScript(),
            true);
    }

    protected void PeriodListDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool visible = PeriodListDropDown.SelectedValue == @"Custom" ? true : false;
        datePeriodHolder.Visible = visible;
    }

    protected void chbLastXEvents_CheckedChanged(object sender, EventArgs e)
    {
        lastXNumeric.Enabled = chbLastXEvents.Checked;
    }
}
