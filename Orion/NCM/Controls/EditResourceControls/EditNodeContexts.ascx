﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditNodeContexts.ascx.cs" Inherits="Orion_NCM_Controls_EditResourceControls_EditNodeContexts" %>

<div>
    <b><%=Resources.NCMWebContent.WEBDATA_VK_997%></b>
    <div style="padding-top:2px;">
        <asp:TextBox runat="server" ID="txtPageSize" Width="50px" />
        <asp:RangeValidator ID="rangeValidator" runat="server" ControlToValidate="txtPageSize" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_998 %>" MaximumValue="100" MinimumValue="5" Type="Integer" Display="Dynamic"></asp:RangeValidator>
        <asp:RequiredFieldValidator ID="requiredFieldValidator" runat="server" ControlToValidate="txtPageSize" ErrorMessage="<%$ Resources: NCMWebContent, WEBDATA_VK_999 %>" Display="Dynamic"></asp:RequiredFieldValidator>
    </div>
</div>