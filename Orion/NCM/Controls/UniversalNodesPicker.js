﻿Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.QuickTips.init();

var hideDlg = false;
SW.NCM.NodeSelectionCriteria = function() {

    var showNodesDialog;
    function ShowNodesDialog() {
        $("[id$='_nodes_picker_container']").text("@{R=NCM.Strings;K=WEBJS_VY_19;E=js}");

        if (!showNodesDialog) {
            showNodesDialog = new Ext.Window({
                applyTo: 'nodesDialog',
                layout: 'fit',
                width: 600,
                height: 400,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                modal: true,
                contentEl: 'nodesBody',
                buttons: [{
                    text: '@{R=NCM.Strings;K=WEBJS_VK_111;E=js}',
                    handler: function () {
                        $("[id$='_btnOk']").click();
                        showNodesDialog.hide();
                    }
                },
                {
                    text: '@{R=NCM.Strings;K=WEBJS_VK_38;E=js}',
                    handler: function () {
                        $("[id$='_btnRemoveNodes']").click();
                        showNodesDialog.hide();
                    }
                }]
            });
        }

        showNodesDialog.setTitle("@{R=NCM.Strings;K=WEBJS_VK_72;E=js}");

        showNodesDialog.alignTo(document.body, "c-c");
        showNodesDialog.show();

        return false;
    }


    return {    
        init: function() {
        
            $("#display_nodes_dialog").click(function() {
                $("[id$='_btnRefreshNodes']").click();
                ShowNodesDialog();
            });
            
            var dlg;
            $("#display_warning_dialog").click(function () {

                if (hideDlg == false) {
                    var dlg = new Ext.Window({
                        title: '@{R=NCM.Strings;K=WEBJS_VY_104;E=js}',
                        html: '<table><tr><td><img src="/Orion/NCM/Resources/images/warning_32x29.gif" /></td><td style="padding-left:10px;">@{R=NCM.Strings;K=WEBJS_VY_105;E=js}<br />@{R=NCM.Strings;K=WEBJS_VY_106;E=js} <a href="" style="text-decoration:underline">@{R=NCM.Strings;K=WEBJS_VY_107;E=js}</a></td></tr></table>',
                        width: 400,
                        autoHeight: true,
                        border: false,
                        region: 'center',
                        layout: 'fit',
                        modal: true,
                        resizable: false,
                        plain: true,
                        bodyStyle: 'padding:5px 0px 15px 5px;',
                        buttonAlign: 'center',
                        buttons: [{
                            text: '@{R=NCM.Strings;K=WEBJS_VY_108;E=js}',
                            handler: function () {
                                hideDlg = true;
                                dlg.hide();
                            }
                        }, {
                            text: '@{R=NCM.Strings;K=WEBJS_VK_38;E=js}',
                            handler: function () {
                                dlg.hide();
                            }
                        }],
                        listeners: {
                            hide: function (e) {
                                if (!hideDlg) {
                                    $("[id$='_desktop_selection_criteria']").click();
                                }
                            }
                        }
                    });
                    dlg.show();
                }
            });
        }
    }
}(); 

Ext.onReady(SW.NCM.NodeSelectionCriteria.init, SW.NCM.NodeSelectionCriteria);
