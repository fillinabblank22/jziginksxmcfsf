<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IconNCMSettings.ascx.cs" Inherits="Orion_NCM_Controls_IconNCMSettings" %>
<%@ Import Namespace="SolarWinds.Orion.Common" %>
<%@ Import Namespace="SolarWinds.NCMModule.Web.Resources" %>
<%if (Profile.AllowAdmin || OrionConfiguration.IsDemoServer || SecurityHelper.IsInNCMRole(SecurityHelper.NCM_ROLE_ADMINISTARTOR))  
  {%>
    <a href="/Orion/NCM/Admin/Default.aspx" style="background-image:url('/Orion/NCM/Resources/images/Page.Icon.MdlSettings.gif');background-position:left center;background-repeat:no-repeat;padding:2px 0 2px 20px;"><%= Resources.NCMWebContent.WEBDATA_VK_100 %></a>
<%}%>

