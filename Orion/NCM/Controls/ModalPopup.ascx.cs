﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_NCM_Controls_ModalPopup : System.Web.UI.UserControl
{
    public override string ClientID
    {
        get
        {
            return this.divContainer.ClientID;
        }
    }

    private ITemplate _dialogContent;

    [PersistenceMode(PersistenceMode.InnerProperty)]
    [TemplateInstance(TemplateInstance.Single)]
    public ITemplate DialogContent
    {
        get
        {
            return _dialogContent;
        }
        set
        {
            this._dialogContent = value;
        }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string Title { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public int Width
    {
        set
        {
            this.divContent.Style[HtmlTextWriterStyle.Width] = (value - 25).ToString() + @"px";
            this.divDialog.Style[HtmlTextWriterStyle.Width] = value.ToString() + @"px";
        }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public int Height
    {
        set
        {
            this.divContent.Style[HtmlTextWriterStyle.Height] = (value).ToString() + @"px";
        }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string ButtonAlign 
    {
        set
        {
            buttonHolder.Attributes[@"align"] = value;
        }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string BodyStyle
    {
        set
        {
            divContent.Attributes[@"style"] = value;
        }
    }

    [PersistenceMode(PersistenceMode.InnerProperty)]
    public PlaceHolder CloseButtonContent
    {
        get { return phCloseButton; }
    }

    [PersistenceMode(PersistenceMode.InnerProperty)]
    public PlaceHolder SubmitButtonContent
    {
        get { return phSublitButton; }
    }

    [PersistenceMode(PersistenceMode.InnerProperty)]
    public PlaceHolder CancelButtonContent
    {
        get { return phCancelButton; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.iFrame.Attributes[@"style"] = I_FRAME_STYLE;

        if (null != this.DialogContent)
        {
            this.DialogContent.InstantiateIn(this.phContent);
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), @"key_setModalPopup", @"if(typeof(setModalPopup) != 'undefined'){ setModalPopup();}", true);
    }

    public void Hide()
    {
        this.divContainer.Attributes[@"style"] = @"display: none;";
        this.UpdatePanelModalPopup.Update();
    }

    public void Show()
    {
        this.divContainer.Attributes[@"style"] = "";
        this.UpdatePanelModalPopup.Update();
    }

    private const string I_FRAME_STYLE = @"
    position: fixed;
    width: 100%;
 	height: 100%; 
    top:0px; 
	bottom:0px; 
	left:0px;
	right:0px;
	overflow:hidden; 
	padding:0; 
	margin:0; 
    background-color: Gray;
    opacity: 0.5;
    -moz-opacity: 0.5;
    filter: alpha(opacity=50);
    z-index: 1000;
    ";
}