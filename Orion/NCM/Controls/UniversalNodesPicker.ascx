<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UniversalNodesPicker.ascx.cs" Inherits="Orion_NCM_Resources_NCM_Controls_UniversalNodesPicker" %>

<div>
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <script type="text/javascript" src="/Orion/NCM/JavaScript/main.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Controls/UniversalNodesPicker.js"></script>
    
    <div id="nodesDialog" class="x-hidden">
        <div id="nodesBody" class="x-panel-body" style="height:330px;width:580px;overflow:auto;">
            <asp:UpdatePanel ID="uPanel_nodes" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <table id="nodetreecontrol_editpolicy_picker" width="560px;">
                        <tr>
                            <td id="nodes_picker_container" runat="server">                                    
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    
    <div class="x-hidden">
        <asp:UpdatePanel ID="uPanel_buttons" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <asp:Button runat="server" ID="btnOk" style="display:none;" />
                <asp:Button runat="server" ID="btnRefreshNodes"  style="display:none;" />
                <asp:Button runat="server" ID="btnRemoveNodes"  style="display:none;" />
            </ContentTemplate>
        </asp:UpdatePanel>
        
        <input id="display_nodes_dialog" type="button" style="display:none;" />
        <input id="display_warning_dialog" type="button" style="display:none;" />
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
          <ContentTemplate>        
                <asp:Panel runat="server" ID="SelectNodesContainer">
                </asp:Panel>    
        </ContentTemplate>
    </asp:UpdatePanel>
</div>