﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InventorySettings.ascx.cs" Inherits="Orion_NCM_Controls_InventorySettings" %>

<link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/Settings/Settings.css" />

<style type="text/css">
    .column1 {
        width: 10%;
        padding-top: 10px;
    }

    .column2 {
        color: gray;
        width: 90%;
        padding-left: 50px;
        padding-top: 10px;
    }
</style>

<script type="text/javascript">
	function autoCheckForInterfaces(checked) {
	    var interfacesChecked = $("[id$='_Interfaces']").prop("checked");
	    if (checked && !interfacesChecked) {

	        alert("Selecting Interfaces is required with the option(s) you selected.");
	        $("[id$='_Interfaces']").prop("checked", true);
	    }
	}
</script>

<table id="SpecTable" runat="server" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="2" class="SettingBlockHeader" style="padding-top:20px;"><%=Resources.NCMWebContent.WEBDATA_VK_510 %></td>
    </tr>
    <tr>
        <td class="column1" nowrap="nowrap"><asp:CheckBox ID="Interfaces" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_511 %>"/></td>
        <td class="column2" nowrap="nowrap"><%=Resources.NCMWebContent.WEBDATA_VK_512 %></td>
    </tr>
    <tr>
        <td class="column1" nowrap="nowrap"><asp:CheckBox ID="ARP" onclick="autoCheckForInterfaces(this.checked);" runat="server" Text="ARP"/></td>
        <td class="column2" nowrap="nowrap"><%=Resources.NCMWebContent.WEBDATA_VK_514 %></td>
    </tr>
    <tr>
        <td class="column1" nowrap="nowrap"><asp:CheckBox ID="BridgeTable" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_515 %>"/></td>
        <td class="column2" nowrap="nowrap"><%=Resources.NCMWebContent.WEBDATA_VK_516 %></td>
    </tr>
    <tr>
        <td class="column1" nowrap="nowrap"><asp:CheckBox ID="EntityMIB" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_517 %>"/></td>
        <td class="column2" nowrap="nowrap"><%=Resources.NCMWebContent.WEBDATA_VK_518 %></td>
    </tr>
        <tr>
        <td class="column1" nowrap="nowrap"><asp:CheckBox ID="JuniperEntity" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_IC_118 %>"/></td>
        <td class="column2" nowrap="nowrap"><%=Resources.NCMWebContent.WEBDATA_IC_119 %></td>
    </tr>
    <tr>
        <td class="column1" nowrap="nowrap"><asp:CheckBox ID="IPAddresses" onclick="autoCheckForInterfaces(this.checked);" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_519 %>"/></td>
        <td class="column2" nowrap="nowrap"><%=Resources.NCMWebContent.WEBDATA_VK_520 %></td>
    </tr>
    <tr>
        <td class="column1" nowrap="nowrap"><asp:CheckBox ID="ActivePorts" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_521 %>"/></td>
        <td class="column2" nowrap="nowrap"><%=Resources.NCMWebContent.WEBDATA_VK_522 %></td>
    </tr>
    <tr>
        <td class="column1" nowrap="nowrap"><asp:CheckBox ID="BrocadeSerialNumbers" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_896 %>"/></td>
        <td class="column2" nowrap="nowrap"><%=Resources.NCMWebContent.WEBDATA_VK_896 %></td>
    </tr>
    <tr>
        <td colspan="2" class="SettingBlockHeader" style="padding-top:50px;"><%=Resources.NCMWebContent.WEBDATA_VK_523 %></td>
    </tr>
    <tr>
        <td class="column1" nowrap="nowrap"><asp:CheckBox ID="IPCIDRRouteTable" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_524 %>"/></td>
        <td class="column2" nowrap="nowrap"><%=Resources.NCMWebContent.WEBDATA_VK_525 %></td>
    </tr>
    <tr>
        <td class="column1" nowrap="nowrap"><asp:CheckBox ID="IPRouteTable" runat="server" onclick="autoCheckForInterfaces(this.checked);" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_526 %>"/></td>
        <td class="column2" nowrap="nowrap"><%=Resources.NCMWebContent.WEBDATA_VK_527 %></td>
    </tr>
    <tr>
        <td colspan="2" class="SettingBlockHeader" style="padding-top:50px;"><%=Resources.NCMWebContent.WEBDATA_VK_528 %></td>
    </tr>
    <tr>
        <td class="column1" nowrap="nowrap"><asp:CheckBox ID="WindowsServices" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_529 %>"/></td>
        <td class="column2" nowrap="nowrap"><%=Resources.NCMWebContent.WEBDATA_VK_530 %></td>
    </tr>
    <tr>
        <td class="column1" nowrap="nowrap"><asp:CheckBox ID="WindowsSoftware" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_531 %>"/></td>
        <td class="column2" nowrap="nowrap"><%=Resources.NCMWebContent.WEBDATA_VK_532 %></td>
    </tr>
    <tr>
        <td class="column1" nowrap="nowrap"><asp:CheckBox ID="WindowsUsers" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_533 %>"/></td>
        <td class="column2" nowrap="nowrap"><%=Resources.NCMWebContent.WEBDATA_VK_534 %></td>
    </tr>
    <tr>
        <td colspan="2" class="SettingBlockHeader" style="padding-top:50px;">Cisco</td>
    </tr>
    <tr>
        <td class="column1" nowrap="nowrap"><asp:CheckBox ID="CiscoChassis" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_536 %>"/></td>
        <td class="column2" nowrap="nowrap"><%=Resources.NCMWebContent.WEBDATA_VK_537 %></td>
    </tr>
    <tr>
        <td class="column1" nowrap="nowrap"><asp:CheckBox ID="CiscoMemoryPools" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_538 %>"/></td>
        <td class="column2" nowrap="nowrap"><%=Resources.NCMWebContent.WEBDATA_VK_539 %></td>
    </tr>
    <tr>
        <td class="column1" nowrap="nowrap"><asp:CheckBox ID="CiscoCards" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_540 %>"/></td>
        <td class="column2" nowrap="nowrap"><%=Resources.NCMWebContent.WEBDATA_VK_541 %></td>
    </tr>
    <tr>
        <td class="column1" nowrap="nowrap"><asp:CheckBox ID="CiscoImage" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_542 %>"/></td>
        <td class="column2" nowrap="nowrap"><%=Resources.NCMWebContent.WEBDATA_VK_543 %></td>
    </tr>
    <tr>
        <td class="column1" nowrap="nowrap"><asp:CheckBox ID="CiscoCatalystCards" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_544 %>"/></td>
        <td class="column2" nowrap="nowrap"><%=Resources.NCMWebContent.WEBDATA_VK_541 %></td>
    </tr>
    <tr>
        <td class="column1" nowrap="nowrap"><asp:CheckBox ID="CiscoFlash" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_545 %>"/></td>
        <td class="column2" nowrap="nowrap"><%=Resources.NCMWebContent.WEBDATA_VK_546 %></td>
    </tr>
    <tr>
        <td class="column1" nowrap="nowrap"><asp:CheckBox ID="CiscoCDP" runat="server" Text="Cisco CDP"/></td>
        <td class="column2" nowrap="nowrap"><%=Resources.NCMWebContent.WEBDATA_VK_548 %></td>
    </tr>
    <tr>
        <td class="column1" nowrap="nowrap"><asp:CheckBox ID="CiscoFRU" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_1060 %>"/></td>
        <td class="column2" nowrap="nowrap"><%=Resources.NCMWebContent.WEBDATA_VK_1061 %></td>
    </tr>
    <tr>
        <td colspan="2" class="SettingBlockHeader" style="padding-top:50px;"><%=Resources.NCMWebContent.InventorySettings_F5_Header %></td>
    </tr>
    <tr>
        <td class="column1" nowrap="nowrap"><asp:CheckBox ID="cbSystem" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_1050 %>"/></td>
        <td class="column2" nowrap="nowrap"><%=Resources.NCMWebContent.WEBDATA_VK_1051 %></td>
    </tr>
    <tr>
        <td class="column1" nowrap="nowrap"><asp:CheckBox ID="cbLocalTM" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_1052 %>"/></td>
        <td class="column2" nowrap="nowrap"><%=Resources.NCMWebContent.WEBDATA_VK_1053 %></td>
    </tr>
    <tr>
        <td class="column1" nowrap="nowrap"><asp:CheckBox ID="cbGlobalTM" runat="server" Text="<%$ Resources: NCMWebContent, WEBDATA_VK_1054 %>"/></td>
        <td class="column2" nowrap="nowrap"><%=Resources.NCMWebContent.WEBDATA_VK_1055 %></td>
    </tr>
</table>