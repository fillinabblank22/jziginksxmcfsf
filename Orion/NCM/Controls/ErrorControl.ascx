<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ErrorControl.ascx.cs" Inherits="Orion_NCM_Controls_PermissionError" %>

<style type="text/css">
    .div_error
    {
        padding: 10px;
    }
        
    .SpanError
    {
	    background: #facece; 
	    color: #ce0000;
	    border: 1px solid #ce0000; 
	    border-radius: 5px;
	    font-size:10pt;
	    font-family: Arial,Verdana,Helvetica,sans-serif;
	    font-weight: bold;
	    display: inline-block; 
	    position: relative;
	    padding: 5px 6px 6px 26px; 
    }

    .SpanError-Icon 
    {
	    position: absolute;
	    left: 4px; 
	    top: 4px; 
	    height: 16px; 
	    width: 16px; 
	    background: url(/Orion/NCM/Resources/images/icon_failed.gif) top left no-repeat; 
    }
</style>

<div id="div_error" class="div_error" runat="server">
    <span class="SpanError"><span class="SpanError-Icon"></span>
        <span id="span_error_message" runat="server"></span><span id="span_permissions_link" runat="server" valign="bottom" style="color:Black;font-weight:normal;font-size:8pt;">&nbsp;&nbsp;&#0187;&nbsp;<a href="<%=PermissionLink%>" target="_blank" style="color:#336699;"><%= Resources.NCMWebContent.ErrorControl_LearnMoreAboutPermissions %></a></span>
    </span>
</div>