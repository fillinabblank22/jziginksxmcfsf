Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.QuickTips.init();

SW.NCM.ViolationDetailsTree = function () {
    var nodesDialogContainerWin;
    var oldPath = "";

    this.imageClick = function (currentNodeID, currentPolicyID, currentRuleID, currentReportID, currentLimitationID) {
        if (!nodesDialogContainerWin) {
            nodesDialogContainerWin = new Ext.Window({
                applyTo: 'nodesDialogContainer',
                layout: 'fit',
                width: 800,
                height: 525,
                closeAction: 'hide',
                plain: true,
                modal: true,
                autoScroll: false,
                contentEl: 'treecontrolpageFrameContainer',
                title: '@{R=NCM.Strings;K=WEBJS_VM_53;E=js}',
                resizable: false,

                buttons: [{
                    text: '@{R=NCM.Strings;K=WEBJS_VK_07;E=js}',
                    handler: function () {
                        nodesDialogContainerWin.hide();
                    }
                }]
            });
            nodesDialogContainerWin.on('resize', function (sender, width, height) {
            });
        }

        nodesDialogContainerWin.alignTo(document.body, "c-c");
        nodesDialogContainerWin.show(this);

        if (oldPath != String.format('ViolationDetails.aspx?currentNodeID={0}&currentPolicyID={1}&currentRuleID={2}&currentReportID={3}&currentLimitationID={4}', currentNodeID, currentPolicyID, currentRuleID, currentReportID, currentLimitationID)) {
            window.frames.treeControlIframe.location.href = oldPath = String.format('ViolationDetails.aspx?currentNodeID={0}&currentPolicyID={1}&currentRuleID={2}&currentReportID={3}&currentLimitationID={4}', currentNodeID, currentPolicyID, currentRuleID, currentReportID, currentLimitationID);
        }
        return false;
    }
}

Ext.onReady(function () {
    TreeViewer = new SW.NCM.ViolationDetailsTree();
});