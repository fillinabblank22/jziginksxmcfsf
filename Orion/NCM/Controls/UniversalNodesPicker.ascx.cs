using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.PolicyReportsManagement.Controls;

public partial class Orion_NCM_Resources_NCM_Controls_UniversalNodesPicker : System.Web.UI.UserControl
{
    SelectNodes selectNodesControl = new SelectNodes();
    private ToolBarNodes toolBarNodes = new ToolBarNodes();

    public List<Guid> AssignedNodes
    {
        get
        {
            if (ViewState["selectNodesControl.AssignedNodes"] == null)
            {
                return new List<Guid>();
            }
            else
            {
                return (List<Guid>)ViewState["selectNodesControl.AssignedNodes"];
            }
        }
        set
        {
            ViewState["selectNodesControl.AssignedNodes"] = value;
            selectNodesControl.AssignedNodes = value;
        }
    }

    public bool ShowSelectNodesButton
    {
        set { selectNodesControl.ShowSelectNodesButton = value; }
    }

    public int LimitationID { get; set; }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        selectNodesControl.SelectNodesClick += new dlgSelectNodesClick(selectNodes_SelectNodesClick);
        SelectNodesContainer.Controls.Add(selectNodesControl);
        btnRefreshNodes.Click += new EventHandler(btnRefreshNodes_Click);
        ResourceSettings resourceSettings = new ResourceSettings();
        resourceSettings.Prefix = "UniversalNodesPicker";
        NPMDefaultSettingsHelper.SetUserSettings("UniversalNodesPickerGroupBy", "Vendor");

        toolBarNodes.ResourceSettings = resourceSettings;
        toolBarNodes.ConfigMgmt = false;
        toolBarNodes.Visible = false;
        toolBarNodes.RemoveNodes();

        nodes_picker_container.Controls.Add(toolBarNodes);
        btnOk.Click += new EventHandler(btnOk_Click);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Visible)
        {
            selectNodesControl.AssignedNodes = AssignedNodes;
            toolBarNodes.LimitationID = LimitationID;
            toolBarNodes.SetupConrol(RenderType.Upload_MultiNode_SelectNode);
        }
    }

    void btnOk_Click(object sender, EventArgs e)
    {
        AssignedNodes = toolBarNodes.GetNodeIDsCollection().ToList();
        UpdatePanel1.Update();
    }

    void btnRefreshNodes_Click(object sender, EventArgs e)
    {
        HttpContext.Current.Session[@"SelectClearAllNodes"] = false;
        toolBarNodes.Visible = true;
        toolBarNodes.RemoveNodes();
        uPanel_nodes.Update();
    }

    public void ShowDialog()
    {
        System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), @"key_display_nodes_dialog", @"$(""#display_nodes_dialog"").click();", true);
    }

    void selectNodes_SelectNodesClick()
    {
        ShowDialog();
    }
}
