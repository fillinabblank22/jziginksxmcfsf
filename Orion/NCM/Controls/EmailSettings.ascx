<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EmailSettings.ascx.cs" Inherits="Orion_NCM_Controls_EmailSettings" %>

<style type="text/css">
    .column1
    {
        width:20%;
        padding:5px 0px 5px 0px !important;
    }
    
    .column2
    {
        width:80%;
        padding:5px 0px 5px 0px !important;
    }
    
    .tooltip
    {
        font-size:8pt;
        color:Gray;
    }
</style>

<div>
    <table width="100%">
        <tr id="EmailToHolder" runat="server">
            <td valign="top" class="column1"><b><%= Resources.NCMWebContent.WEBDATA_VK_62 %></b></td>
            <td class="column2">
                <asp:TextBox ID="txtTo" runat="server" TextMode="MultiLine" Font-Size="small" Font-Names="Arial,Verdana,Helvetica,Sans-Serif" BorderColor="#89A6C0" BorderWidth="1px" Width="350px" Height="100px" spellcheck="false" style="overflow-y:scroll;" MaxLength="4000"/>
                <asp:RequiredFieldValidator style="vertical-align:top;" ID="rfvTo" runat="server" Display="Dynamic" ControlToValidate="txtTo" ErrorMessage="*" />  <br /> 
                <asp:RegularExpressionValidator  ID="revTo" runat="server" Display="Dynamic" ControlToValidate="txtTo" ValidationExpression="^([\w.!#$%&'*+/=?^_`{|}~-]+@[\w.-]+\.[\w]+[\s]*[;,]{0,1}[\s]*[\n]{0,1})+$" ErrorMessage="<%$Resources: NCMWebContent, WEBDATA_VK_1063 %>" />
                <div class="tooltip"><%= Resources.NCMWebContent.WEBDATA_VK_66 %></div>
            </td>
        </tr>
        <tr>
            <td valign="top" class="column1"><b><%= Resources.NCMWebContent.WEBDATA_VK_63 %></b></td>
            <td class="column2">
                <asp:TextBox ID="txtFrom" runat="server" TextMode="SingleLine" Font-Size="small" Font-Names="Arial,Verdana,Helvetica,Sans-Serif" BorderColor="#89A6C0" BorderWidth="1px" Width="350px" Height="18px" spellcheck="false" MaxLength="4000"/>
                <asp:RequiredFieldValidator ID="rfvFrom" runat="server" Display="Dynamic" ControlToValidate="txtFrom" ErrorMessage="*" />  <br /> 
                <asp:RegularExpressionValidator ID="revFrom" runat="server" Display="Dynamic" ControlToValidate="txtFrom" ValidationExpression="^[\w.!#$%&'*+/=?^_`{|}~-]+@[\w.-]+\.[\w]+$" ErrorMessage="<%$Resources: NCMWebContent, WEBDATA_VK_1063 %>" />
                <div class="tooltip"><%= Resources.NCMWebContent.WEBDATA_VK_67 %></div>
            </td>
        </tr>
        <tr>
            <td valign="middle" class="column1"><b><%= Resources.NCMWebContent.WEBDATA_VK_64 %></b></td>
            <td class="column2">
                <asp:TextBox ID="txtSubject" runat="server" TextMode="SingleLine" Font-Size="small" Font-Names="Arial,Verdana,Helvetica,Sans-Serif" BorderColor="#89A6C0" BorderWidth="1px" Width="350px" Height="18px" spellcheck="false" MaxLength="4000"/>
                <asp:RequiredFieldValidator ID="rfvSubject" runat="server" Display="Dynamic" ControlToValidate="txtSubject" ErrorMessage="*" />
            </td>
        </tr>
        <tr>
            <td valign="top" class="column1"><b><%= Resources.NCMWebContent.WEBDATA_VK_65 %></b></td>
            <td class="column2">
                <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Font-Size="small" Font-Names="Arial,Verdana,Helvetica,Sans-Serif" BorderColor="#89A6C0" BorderWidth="1px" Width="350px" Height="100px" spellcheck="false" style="overflow-y:scroll;" MaxLength="4000"/>
                <asp:RequiredFieldValidator style="vertical-align:top;" ID="rfvMessage" runat="server" Display="Dynamic" ControlToValidate="txtMessage" ErrorMessage="*" />
                <div class="tooltip"><%= string.Format(Resources.NCMWebContent.WEBDATA_VK_68, @"<br/>") %></div>
            </td>
        </tr>
    </table>
</div>