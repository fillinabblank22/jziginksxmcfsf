﻿using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Data;
using System.IO;
using Infragistics.WebUI.UltraWebNavigator;

using SolarWinds.NCMModule.Web.Resources;
using SolarWinds.NCMModule.Web.Resources.NCMCore.Helpers;

public partial class Orion_NCM_Controls_TreePanel : System.Web.UI.UserControl
{
    private ISWrapper isWrapper = new ISWrapper();
    
    private const string imagePathTemplate = "/NetPerfMon/images/Vendors/{0}";
    private const string swql_BuildGroups = @"SELECT {0} FROM Cirrus.NodeProperties AS NCMNodeProperties
INNER JOIN Orion.Nodes AS Nodes ON NCMNodeProperties.CoreNodeID=Nodes.NodeID
INNER JOIN Orion.NodesCustomProperties AS OrionCustomProperties ON NCMNodeProperties.CoreNodeID=OrionCustomProperties.NodeID {1} {2} GROUP BY {0} ORDER BY {0}";
    private const string swql_BuildGroupsByVendor = @"SELECT {0}, MAX(Nodes.VendorIcon) AS VendorIcon FROM Cirrus.NodeProperties AS NCMNodeProperties
INNER JOIN Orion.Nodes AS Nodes ON NCMNodeProperties.CoreNodeID=Nodes.NodeID
INNER JOIN Orion.NodesCustomProperties AS OrionCustomProperties ON NCMNodeProperties.CoreNodeID=OrionCustomProperties.NodeID {1} {2} GROUP BY {0} ORDER BY {0}";
    private const string swql_BuildNodes = @"SELECT Nodes.Caption AS NodeCaption, Nodes.SysName, NCMNodeProperties.NodeID, Nodes.IPAddress AS AgentIP, Nodes.Status, Nodes.VendorIcon FROM Cirrus.NodeProperties AS NCMNodeProperties
INNER JOIN Orion.Nodes AS Nodes ON NCMNodeProperties.CoreNodeID=Nodes.NodeID
INNER JOIN Orion.NodesCustomProperties AS OrionCustomProperties ON NCMNodeProperties.CoreNodeID=OrionCustomProperties.NodeID {0} {1} ORDER BY Nodes.Caption";
    private string physicalPath;

    [PersistenceMode(PersistenceMode.Attribute)]
    public string Title { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool IncludeNodeIDList { get; set; }

    public bool HideUnmanageNodes { get; set; }

    public List<Guid> NodeIDList { get; set; }

    public string GroupBy { get; set; }

    public string SearchTerm 
    {
        get
        {
            var searchTerm = Convert.ToString(ViewState["NCM_TreePanel_SearchTerm"]);
            return searchTerm;
        }
        set
        {
            ViewState["NCM_TreePanel_SearchTerm"] = value;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        physicalPath = string.Concat($@"{Page.Request.PhysicalApplicationPath}NetPerfMon/images/Vendors/", @"{0}");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void tree_OnDemandLoad(object sender, WebTreeNodeEventArgs e)
    {
        BuildNodes(e.Node);
    }

    protected void btnSelectAll_Click(object sender, EventArgs e)
    {
        SelectAllSelectNoneNodes(tree.Nodes, true);
    }

    protected void btnSelectNone_Click(object sender, EventArgs e)
    {
        SelectAllSelectNoneNodes(tree.Nodes, false);
    }

    private void SelectAllSelectNoneNodes(Nodes nodes, bool select)
    {
        foreach (Node node in nodes)
        {
            if (node.CheckBox == CheckBoxes.True)
            {
                if (select)
                {
                    if (!node.Checked)
                    {
                        node.Checked = select;
                    }
                }
                else
                {
                    if (node.Checked)
                    {
                        node.Checked = select;
                    }
                }
            }

            if (node.Nodes.Count > 0)
            {
                SelectAllSelectNoneNodes(node.Nodes, select);
            }
        }
    }

    private void BuildGroups()
    {
        using (var proxy = isWrapper.Proxy)
        {
            var dt = proxy.Query(string.Format(GroupBy.Equals(@"Vendor", StringComparison.InvariantCultureIgnoreCase) ? swql_BuildGroupsByVendor : swql_BuildGroups,
                CustomPropertyHelper.DecodePropertyName(CustomPropertyHelper.CovertGroupingPropertyName(GroupBy)),
                BuildWhereClause(null),
                BuildSearchClause()
                ));

            var nodes = new Nodes();
            foreach (DataRow row in dt.Rows)
            {
                var icon = GroupBy.Equals(@"Vendor", StringComparison.InvariantCultureIgnoreCase) ? Convert.ToString(row["VendorIcon"]) : string.Empty;
                var value = row[CustomPropertyHelper.CovertGroupingPropertyName(GroupBy)];
                var text = ParseNodeGroupText(value);

                var node = new Node();
                node.CheckBox = CheckBoxes.True;
                node.ImageUrl = GroupBy.Equals(@"Vendor", StringComparison.InvariantCultureIgnoreCase) ? ResolveUrl(string.Format(imagePathTemplate, File.Exists(string.Format(physicalPath, icon)) ? icon : @"Unknown.gif")) : string.Empty;
                node.Text = HttpUtility.HtmlEncode(text);
                node.DataPath = $@"G:{value}";
                node.ShowExpand = true;
                node.Tag = @"Group";
                nodes.Add(node);
            }

            if (nodes.Count > 0)
            {
                tree.Nodes.AddRange(nodes);
            }
        }
    }

    private void BuildNodes(Node parentNode)
    {
        using (var proxy = isWrapper.Proxy)
        {
            var dt = proxy.Query(string.Format(swql_BuildNodes, BuildWhereClause(parentNode), BuildSearchClause()));

            var nodes = new Nodes();
            foreach (DataRow row in dt.Rows)
            {
                var icon = Convert.ToString(row["VendorIcon"]);
                var value = row["NodeID"];
                var text = Convert.ToString(row["NodeCaption"]);
                var ipAddress = Convert.ToString(row["AgentIP"]);

                if (string.IsNullOrEmpty(text))
                {
                    text = string.IsNullOrEmpty(Convert.ToString(row["SysName"])) ? ipAddress : Convert.ToString(row["SysName"]);
                }

                var node = new Node {CheckBox = CheckBoxes.True};
                if (parentNode != null)
                {
                    node.Checked = parentNode.Checked;
                }
                node.ImageUrl = ResolveUrl(string.Format(imagePathTemplate, File.Exists(string.Format(physicalPath, icon)) ? icon : @"Unknown.gif"));
                node.Text = $@"<a target='_blank' href='/Orion/NCM/NodeDetails.aspx?NodeID={value}'>{HttpUtility.HtmlEncode(text)}</a>";
                node.DataPath = $@"N:{value}";
                node.ShowExpand = false;
                node.Tag = @"Node";
                nodes.Add(node);
            }

            if (nodes.Count > 0)
            {
                if (parentNode != null)
                {
                    parentNode.Nodes.AddRange(nodes);

                    var pos = parentNode.Text.IndexOf(@"<div class='tree-node-loading-img'", StringComparison.Ordinal);
                    parentNode.Text = parentNode.Text.Substring(0, pos);
                }
                else
                {
                    tree.Nodes.AddRange(nodes);
                }
            }
        }
    }

    public void Refresh()
    {
        tree.Nodes.Clear();
        if (GroupBy.Equals(@"None", StringComparison.InvariantCultureIgnoreCase))
        {
            BuildNodes(null);
        }
        else
        {
            BuildGroups();
        }
    }

    public List<NodeUIEntry> GetCheckedGroups()
    {
        if (tree.CheckedNodes.Count == 0)
            return null;

        var nodes = new List<NodeUIEntry>();
        foreach (Node node in tree.CheckedNodes)
        {
            if (Convert.ToString(node.Tag).Equals(@"Group", StringComparison.InvariantCultureIgnoreCase))
            {
                nodes.Add(new NodeUIEntry(node.DataPath, node.Text, node.Tag, node.ImageUrl, node.ShowExpand, node.Expanded, null));
            }
        }

        return nodes;
    }

    public List<NodeUIEntry> GetCheckedNodes()
    {
        if (tree.CheckedNodes.Count == 0)
            return null;

        var nodes = new List<NodeUIEntry>();
        foreach (Node node in tree.CheckedNodes)
        {
            if (Convert.ToString(node.Tag).Equals(@"Node", StringComparison.InvariantCultureIgnoreCase))
            {
                NodeUIEntry parentNode = null;
                if (node.Parent != null)
                {
                    parentNode = new NodeUIEntry(node.Parent.DataPath, node.Parent.Text, node.Parent.Tag, node.Parent.ImageUrl, node.Parent.ShowExpand, node.Parent.Expanded, null);
                }
                nodes.Add(new NodeUIEntry(node.DataPath, node.Text, node.Tag, node.ImageUrl, node.ShowExpand, node.Expanded, parentNode));
            }
        }

        return nodes;
    }

    public void AddGroups(List<NodeUIEntry> nodes)
    {
        if (nodes != null && nodes.Count > 0)
        {
            foreach (var node in nodes)
            {
                AddGroup(node);
            }
        }
    }

    private void AddGroup(NodeUIEntry nodeEntry)
    {
        var groupNode = tree.Nodes.Search(nodeEntry.Text, @"Group", null, false);
        if (groupNode == null)
        {
            var nodeToAdd = new Node();
            nodeToAdd.DataPath = nodeEntry.DataPath;
            nodeToAdd.Text = nodeEntry.Text;
            nodeToAdd.ImageUrl = nodeEntry.ImageUrl;
            nodeToAdd.Tag = nodeEntry.Tag;
            nodeToAdd.CheckBox = CheckBoxes.True;
            nodeToAdd.ShowExpand = nodeEntry.ShowExpand;
            nodeToAdd.Expanded = nodeEntry.Expanded;

            groupNode = tree.Nodes.Add(nodeToAdd);
        }

        tree.Nodes.Sort(true, false);
    }

    public void AddNodes(List<NodeUIEntry> nodes)
    {
        if (nodes != null && nodes.Count > 0)
        {
            foreach (var node in nodes)
            {
                AddNode(node);
            }
        }
    }

    private void AddNode(NodeUIEntry nodeEntry)
    {
        Node nodeToAdd = null;
        Node parentNode = null;

        if (nodeEntry.ParentNode != null)
        {
            parentNode = tree.Nodes.Search(nodeEntry.ParentNode.Text, @"Group", null, false);
            if (parentNode == null)
            {
                nodeToAdd = new Node();
                nodeToAdd.DataPath = nodeEntry.ParentNode.DataPath;
                nodeToAdd.Text = nodeEntry.ParentNode.Text;
                nodeToAdd.ImageUrl = nodeEntry.ParentNode.ImageUrl;
                nodeToAdd.Tag = nodeEntry.ParentNode.Tag;
                nodeToAdd.CheckBox = CheckBoxes.True;
                nodeToAdd.ShowExpand = nodeEntry.ParentNode.ShowExpand;
                nodeToAdd.Expanded = nodeEntry.ParentNode.Expanded;

                parentNode = tree.Nodes.Add(nodeToAdd);
            }
        }

        var childNode = tree.Nodes.Search(nodeEntry.Text, @"Node", null, false);
        if (childNode == null)
        {
            nodeToAdd = new Node();
            nodeToAdd.DataPath = nodeEntry.DataPath;
            nodeToAdd.Text = nodeEntry.Text;
            nodeToAdd.ImageUrl = nodeEntry.ImageUrl;
            nodeToAdd.Tag = nodeEntry.Tag;
            nodeToAdd.CheckBox = CheckBoxes.True;
            nodeToAdd.ShowExpand = nodeEntry.ShowExpand;
            nodeToAdd.Expanded = nodeEntry.Expanded;

            if (parentNode != null)
            {
                if (parentNode.Expanded) childNode = parentNode.Nodes.Add(nodeToAdd);
            }
            else
            {
                childNode = tree.Nodes.Add(nodeToAdd);
            }
        }

        tree.Nodes.Sort(true, false);
    }

    public void RemoveGroups(List<NodeUIEntry> nodes)
    {
        if (nodes != null && nodes.Count > 0)
        {
            foreach (var node in nodes)
            {
                RemoveGroup(node);
            }
        }
    }

    private void RemoveGroup(NodeUIEntry nodeEntry)
    {
        var nodeToRemove = tree.Nodes.Search(nodeEntry.Text, @"Group", null, false);
        if (nodeToRemove != null)
        {
            tree.Nodes.Remove(nodeToRemove);
        }

        tree.Nodes.Sort(true, false);
    }

    public void RemoveNodes(List<NodeUIEntry> nodes)
    {
        if (nodes != null && nodes.Count > 0)
        {
            foreach (var node in nodes)
            {
                RemoveNode(node);
            }
        }
    }

    private void RemoveNode(NodeUIEntry nodeEntry)
    {
        Node nodeToRemove = null;
        Node parentNode = null;

        if (nodeEntry.ParentNode != null)
        {
            parentNode = tree.Nodes.Search(nodeEntry.ParentNode.Text, @"Group", null, false);
        }

        if (parentNode != null)
        {
            nodeToRemove = parentNode.Nodes.Search(nodeEntry.Text, @"Node", null, false);
            if (nodeToRemove != null)
            {
                parentNode.Nodes.Remove(nodeToRemove);
            }
        }
        else
        {
            nodeToRemove = tree.Nodes.Search(nodeEntry.Text, @"Node", null, false);
            if (nodeToRemove != null)
            {
                tree.Nodes.Remove(nodeToRemove);
            }
        }

        if (parentNode!= null && parentNode.Nodes.Count == 0)
        {
            tree.Nodes.Remove(parentNode);
        }

        tree.Nodes.Sort(true, false);
    }

    private string ParseNodeGroupText(object value)
    {
        switch (GroupBy.ToUpperInvariant())
        {
            case "STATUS":
                return CommonHelper.LookupNodeStatusText(Convert.ToString(value));
            default:
                return string.IsNullOrEmpty(Convert.ToString(value)) ? Resources.NCMWebContent.NodeGroupText_Unknown : Convert.ToString(value);
        }
    }

    [Localizable(false)]
    private string BuildWhereClause(Node parentNode)
    {
        var whereClause = new StringBuilder();
        if (parentNode != null)
        {
            var group = parentNode.DataPath.Replace("G:", string.Empty).Replace("'", "''");
            if (string.IsNullOrEmpty(group))
            {
                SolarWinds.Orion.Core.Common.Models.CustomProperty cp;
                if (CustomPropertyHelper.IsCustomProperty(GroupBy, out cp))
                {
                    if (cp.PropertyType == typeof(String))
                    {
                        whereClause.AppendFormat(" WHERE ({0}='' OR {0} IS NULL) ", CustomPropertyHelper.DecodePropertyName(CustomPropertyHelper.CovertGroupingPropertyName(GroupBy)));
                    }
                    else
                    {
                        whereClause.AppendFormat(" WHERE ({0} IS NULL) ", CustomPropertyHelper.DecodePropertyName(CustomPropertyHelper.CovertGroupingPropertyName(GroupBy)));
                    }
                }
                else
                {
                    whereClause.AppendFormat(" WHERE ({0}='' OR {0} IS NULL) ", CustomPropertyHelper.DecodePropertyName(CustomPropertyHelper.CovertGroupingPropertyName(GroupBy)));
                }
            }
            else
            {
                whereClause.AppendFormat(" WHERE {0}='{1}' ", CustomPropertyHelper.DecodePropertyName(CustomPropertyHelper.CovertGroupingPropertyName(GroupBy)), group);
            }
        }
        else
        {
            whereClause.Append(" WHERE (1=1) ");
        }

        if (NodeIDList.Count > 0)
        {
            if (IncludeNodeIDList)
            {
                whereClause.AppendFormat(" AND NCMNodeProperties.NodeID IN ({0}) ", JoinNodeIDList());
            }
            else
            {
                whereClause.AppendFormat(" AND NCMNodeProperties.NodeID NOT IN ({0}) ", JoinNodeIDList());
            }
        }
        else
        {
            if (IncludeNodeIDList)
            {
                whereClause.Append(" AND (1=0) ");
            }
            else
            {
                whereClause.Append(" AND (1=1) ");
            }
        }

        if (HideUnmanageNodes)
        {
            whereClause.Append(" AND Nodes.Status<>9 ");
        }

        return whereClause.ToString();
    }

    private string BuildSearchClause()
    {
        if (string.IsNullOrEmpty(SearchTerm))
        {
            return @" AND (1=1) ";
        }
        else
        {
            return string.Format(@" AND (Nodes.IPAddress LIKE '%{0}%' OR Nodes.Caption LIKE '%{0}%') ", SearchHelper.WildcardToLike(SearchTerm.Replace(@"'", @"''")));
        }
    }

    private string JoinNodeIDList()
    {
        var joinString = string.Join(@"','", NodeIDList);
        return $@"'{joinString}'";
    }
}

public class NodeUIEntry
{
    public string DataPath { get; set; }
    public string Text { get; set; }
    public object Tag { get; set; }
    public string ImageUrl { get; set; }
    public bool ShowExpand { get; set; }
    public bool Expanded { get; set; }
    public NodeUIEntry ParentNode { get; set; }

    public NodeUIEntry(string dataPath, string text, object tag, string imageUrl, bool showExpand, bool expanded, NodeUIEntry parentNode)
    {
        DataPath = dataPath;
        Text = text;
        Tag = tag;
        ImageUrl = imageUrl;
        ShowExpand = showExpand;
        Expanded = expanded;
        ParentNode = parentNode;
    }
}