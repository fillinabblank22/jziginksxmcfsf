using System;
using System.Data;
using SolarWinds.InformationService.Contract2;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;
public partial class Orion_NCM_NodeDetails : System.Web.UI.Page
{

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        Redirect();
    }
    
    private void Redirect()
    {
        var nodeId = Page.Request.QueryString[@"NodeID"];
        var coreNodeId = string.IsNullOrEmpty(nodeId) ? 0 : GetCoreNodeID(nodeId);

        if (coreNodeId > 0)
        {
            var viewManagerAdapter = ServiceLocator.Container.Resolve<IViewManager>();
            var viewId = Page.Request.QueryString[@"ViewID"];
            var url = $@"{viewManagerAdapter.GetPagePathByViewType(@"NodeDetails")}?NetObject=N:{coreNodeId}";
            var urlSuffix = !string.IsNullOrEmpty(viewId) ? $@"&ViewID={viewId}" : string.Empty;

            Page.Response.Redirect(url + urlSuffix);
        }
        else
        {
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new Exception(Resources.NCMWebContent.WEBDATA_VK_163) });
        }
    }

    private int GetCoreNodeID(string ncmNodeId)
    {
        var isLayer = new ISWrapper();
        using (var proxy = isLayer.Proxy)
        {
            var swql = @"SELECT CoreNodeID FROM Cirrus.NodeProperties WHERE NodeID=@NodeId";
            var propertyBag = new PropertyBag{{@"NodeId", ncmNodeId}};
            var dt = proxy.Query(swql, propertyBag);
            
            if (dt != null && dt.Rows.Count > 0)
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }
        return 0;
    }
}
