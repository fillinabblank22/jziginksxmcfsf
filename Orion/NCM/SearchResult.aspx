<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="SearchResult.aspx.cs" Inherits="SearchResult" ValidateRequest="false"  %>
<%@ Import Namespace="SolarWinds.Orion.Common" %>
<%@ Import Namespace="SolarWinds.NCMModule.Web.Resources" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="ncm" Namespace="SolarWinds.NCMModule.Web.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ncmPageTitlePlaceHolder">
    <%=Page.Title %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmCustomizePagePlaceHolder" runat="Server">
    <orion:IconHelpButton ID="helpBtn" runat="server" HelpUrlFragment="OrionNCMWebGlobalSearch" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmHeadPlaceHolder" runat="Server">
    <style type="text/css">
        .Exception
        {
	        background: #facece; 
	        color: #ce0000;
	        border: 1px solid #ce0000; 
	        border-radius: 5px;
	        font-size:10pt;
	        font-family: Arial,Verdana,Helvetica,sans-serif;
	        display: inline-block; 
	        position: relative;
	        padding: 5px 6px 6px 26px; 
        }

        .Exception-Icon 
        {
	        position: absolute;
	        left: 4px; 
	        top: 4px; 
	        height: 16px; 
	        width: 16px; 
	        background: url(/Orion/NCM/Resources/images/icon_failed.gif) top left no-repeat; 
        }
    </style>
</asp:Content>

<asp:Content ID="SearchResultContent" ContentPlaceHolderID="ncmMainContentPlaceHolder" runat="Server">
   <link rel="stylesheet" type="text/css" href="/Orion/NCM/styles/NCMResources.css" />
   <script type="text/javascript" src="/Orion/NCM/JavaScript/main.js"></script>
   
   <div align="left" style="text-align:left;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;height:20%;">
        <div id="IndexingStatusHolder" style="padding:10px 0px 10px 0px;" runat="server" visible="false">
            <span class="Exception">
                <span class="Exception-Icon"></span><asp:Label ID="lblIndexingStatus" runat="server"></asp:Label>
            </span>
        </div>

        <table id="MainSearchTable" runat="server" visible="true" cellpadding="0" cellspacing="0" width="100%" style="border-width:1px; border-style:solid; border-color:#DCDBD7; background-color:#FFFFFF">
            <tr id="SearchIsDisabledHolder" runat="server" visible="false">
                <td style="padding:10px;">
                    <%=string.Format(Resources.NCMWebContent.WEBDATA_VK_735, OrionConfiguration.IsDemoServer ? $@"<a style=""color:#336699;"" href=""javascript:demoAction('{DemoModeConstants.NCM_SETTINGS_SEARCH_SETTINGS}', this);"">" :  @"<a style=""color:#336699;"" href=""/Orion/NCM/Admin/Settings/SearchSettings.aspx"">", @"</a>")%>
                    <div style="font-size:8pt;padding-top:5px;">
                        &#0187;&nbsp;<a style="color:#336699;" href="<%=LearnMoreLink %>" target="_blank"><%=Resources.NCMWebContent.WEBDATA_VK_833%></a>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="padding-top:10px;padding-bottom:10px;">
                <asp:Panel ID="SearchPanel" runat="server">
                    <!-- Adwanced Search -->
                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>
                                <asp:Panel runat="server" ID="AdvSearchPanel" DefaultButton="AdvSearchButton">
                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                    <tr>
                                        <td align="left" style="padding-left:10px; width:1%">
                                            <nobr><div style="font-size: 12px"><%= Resources.NCMWebContent.WEBDATA_VM_104 %></div></nobr>
                                        </td>
                                        <td align="left" style="padding-left:10px;width:1%">
                                            <nobr>
                                            <asp:TextBox id="AdvSearchTextBox" runat="server" ValidationGroup="GroupSearch" Width="300px" ToolTip="<%$ Resources: NCMWebContent, WEBDATA_VM_115 %>"></asp:TextBox>
                                            <asp:RequiredFieldValidator id="AdvSearchTextBoxValidator" runat="server" ValidationGroup="GroupSearch" Display="Dynamic" ControlToValidate="AdvSearchTextBox">*</asp:RequiredFieldValidator>
                                            </nobr>
                                        </td>
                                        <td align="left" style="padding-left:10px;width:1%" nowrap="nowrap">
                                            <div style="font-size:12px;"><%= Resources.NCMWebContent.WEBDATA_VM_105 %></div>
                                        </td>
                                        <td align="right" style="width:1%">
                                            <asp:UpdatePanel ID="AdvancedSearchUpdatePanel" UpdateMode="Conditional" runat="server">
                                            <ContentTemplate>
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td align="right" style="padding-left:10px">
                                                        <asp:DropDownList id="SearchTypeDropDown" runat="server" AutoPostBack="True" OnSelectedIndexChanged="SearchTypeDropDown_SelectedIndexChanged" />
                                                    </td>
                                                    <td align="left" style="padding-left:10px">
                                                        <asp:DropDownList id="SearchFieldsDropDown" runat="server" AutoPostBack="False" />
                                                    </td>
                                                </tr>
                                            </table>
                                            </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td align="left" style="padding-left:10px; width:90% " >
                                            <orion:LocalizableButton  ID="AdvSearchButton" LocalizedText="CustomText" Text="<%$ Resources: NCMWebcontent, WEBDATA_VK_180 %>" runat="server" ValidationGroup="GroupSearch" OnClick = "AdvSearchButton_Click"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="5" style="padding-left:10px">
                                             <nobr><asp:CheckBox id="CheckBoxSearchForSearch" runat="server" Text = "<%$ Resources: NCMWebContent, WEBDATA_VM_113%>"></asp:CheckBox></nobr>
                                        </td>
                                    </tr>
                                    <tr>
                                         <td align="left" colspan="5" style="padding-left:10px">
                                             <nobr><asp:CheckBox runat="server" id="cbWholeWord" Text="<%$ Resources: NCMWebContent, WEBDATA_VM_114%>"></asp:CheckBox></nobr>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="padding:10px; size :auto;" colspan="5" >
                                            <asp:UpdatePanel ID="AdvancedConfigUpdatePanel" UpdateMode="Conditional" runat="server"><ContentTemplate>
                                            <asp:Panel ID="AdvancedConfigPanel" runat="server" BorderStyle="Solid" BorderWidth="1" BorderColor="#DCDBD7">
                                            <table border="0">
                                                <tr>
                                                    <td>
                                                        <asp:RadioButtonList id="RadioButtonListDateRangeConfigs" runat="server" AutoPostBack="True" RepeatLayout="Table" OnSelectedIndexChanged="RadioButtonListDateRangeConfigs_IndexChanged" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Panel id="SpecifyDateRangePanel" runat="server">
                                                        <table>
                                                            <tr>
                                                                <td align="left" style="padding-left:24px;width:30%;">
                                                                    <nobr><div style="font-size: 12px"><%= Resources.NCMWebContent.WEBDATA_VM_106 %></div></nobr>
                                                                </td>
                                                                <td style="padding-left:25px;">
                                                                    <asp:DropDownList id="DateRangeConfigsDropDown" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DateRangeConfigsDropDown_SelectedIndexChanged"/>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table>
                                                            <tr>
                                                                <td id="DateTimePikerPlace" runat="server" colspan="2" style="padding-left:24px;width:600px"></td>
                                                            </tr>
                                                        </table>
                                                        </asp:Panel> 
                                                    </td>
                                                </tr>
                                            </table>
                                            <table>
                                                <tr>
                                                    <td align="left" style="padding-left:10px;width:30%;">
                                                        <nobr><div style="font-size: 12px"><%= Resources.NCMWebContent.WEBDATA_VM_107 %></div></nobr>
                                                    </td>
                                                    <td style="padding-left:10px;">
                                                        <ncm:ConfigTypes ID="ConfigTypesDropDown" runat="server" ShowAll="true" ShowEdited="true" AutoPostBack="False"></ncm:ConfigTypes>
                                                    </td>
                                                </tr>
                                            </table>
                                            </asp:Panel>
                                            <asp:Timer id="AdvancedConfigTimer" runat="server" Interval="100" Enabled="False" OnTick="AdvancedConfig_Tick"/>
                                            </ContentTemplate></asp:UpdatePanel> 
                                        </td>
                                    </tr>
                                </table>
                                </asp:Panel>
                            </td>                        
                        </tr>
                        <tr>
                            <td align="left" style="padding-left:10px;">
                                <asp:UpdatePanel ID="NodesUpdatePanel" UpdateMode="Conditional" runat="server">
                                    <ContentTemplate>
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td id="NodesLabel" runat="server" style="padding-top:20px;padding-bottom:10px;" visible="false"><%=Resources.NCMWebContent.WEBDATA_VM_108 %></td>
                                            </tr>
                                        </table> 
                                        <asp:Timer id="NodesTimer" runat="server" Interval="100" Enabled="False" OnTick="Nodes_Tick"/>
                                        <asp:Panel id="NodesPanel" runat="server" Visible="true" Width="700px" ></asp:Panel> 
                                    </ContentTemplate>
                                </asp:UpdatePanel> 
                            </td>                         
                        </tr> 
                    </table>
                
                    <!-- Search Results -->
                    <asp:UpdatePanel ID="SearchResultsUpdatePanel" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="AjaxTimer" EventName="Tick" />
                        </Triggers>
                            <asp:Timer ID="AjaxTimer" runat="server" Enabled="False" Interval="5000"/>
                            <table >
                                <tr>
                                    <td id="NoNodesLabel" runat="server" align="left" valign="bottom" style="padding-left:10px;padding-right:10px;padding-top:10px;padding-bottom:4px;color:Red;">
                                    </td>
                                </tr>
                                <tr>
                                    <td id="ConfigResultTitle" runat="server" align="left" valign="bottom" class="clsSearchResultHeader" style="padding-left:10px;padding-right:10px;padding-top:10px;padding-bottom:4px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td id="ConfigResultHolder" runat="server" style="padding-left:10px;padding-right:10px">
                                    </td>
                                </tr>
                                <tr>
                                    <td id="NodeResultTitle" runat="server" align="left" valign="bottom" class="clsSearchResultHeader" style="padding-left:10px;padding-right:10px;padding-top:10px;padding-bottom:4px;">
                                    </td>
                                </tr> 
                                <tr>
                                    <td id="NodeResultHolder" runat="server" style="padding-left:10px;padding-right:10px">
                                    </td>
                                </tr>                   
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
                </td>
            </tr>
        </table>
   </div>
</asp:Content>
