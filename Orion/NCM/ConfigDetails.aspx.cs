using System;
using System.Collections.Generic;
using System.Web;
using SolarWinds.Cirrus.IS.Client;
using SolarWinds.Coding.Utils.Logger;
using SolarWinds.InformationService.Contract2;
using SolarWinds.NCM.Web.Contracts;
using SolarWinds.NCM.Web.Contracts.SiteMap;
using SolarWinds.Orion.Web.UI;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_ConfigDetails : OrionView
{
    private readonly IIsWrapper _isLayer;
    private readonly IMacroParser macroParser;
    private readonly ILogger logger;
    private readonly Func<INcmSiteMapRenderer> rendererFactory;

    public Orion_NCM_ConfigDetails()
    {
        _isLayer = ServiceLocator.Container.Resolve<IIsWrapper>();
        macroParser = ServiceLocator.Container.Resolve<IMacroParser>();
        logger = ServiceLocator.Container.Resolve<Func<Type, ILogger>>()(GetType());
        rendererFactory = ServiceLocator.Container.Resolve<Func<INcmSiteMapRenderer>>();
    }

    public override string ViewType { get; } = @"NCMConfigDetails";

    protected string PageTitle { get; set; }

    protected override void OnInit(EventArgs e)
    {
        if (string.IsNullOrEmpty(Page.Request.QueryString[@"ConfigID"]))
        {
            PreviewMode();
        }
        else
        {
            bool notExist;
            bool permNotExist;
            string nodeName = HttpUtility.HtmlEncode(GetNodeName(out notExist, out permNotExist));

            if (permNotExist)
            {
                PageTitle = ViewInfo.ViewTitle;
                exMsg.ExceptionMessage = string.Format(Resources.NCMWebContent.WEBDATA_VK_161, nodeName);
                resourcePanel.Visible = false;
            }
            else if (notExist)
            {
                PageTitle = ViewInfo.ViewTitle;
                exMsg.ExceptionMessage = string.Format(Resources.NCMWebContent.WEBDATA_VK_162, @"ConfigID", Page.Request.QueryString[@"ConfigID"]);
                resourcePanel.Visible = false;
            }
            else
            {
                PageTitle = string.Format(Resources.NCMWebContent.WEBDATA_VK_175, ViewInfo.ViewTitle,
                    $@"<a href='/Orion/NCM/NodeDetails.aspx?NodeID={GetNodeId()}'>{nodeName}</a>");
                ErrorPannel.Visible = false;
                resContainer.DataSource = ViewInfo;
                resContainer.DataBind();
            }

            if (string.IsNullOrEmpty(Page.Request.QueryString[@"ReportID"]))
            {
                LinkHolder.Visible = false;
            }
        }

        base.OnInit(e);
    }

    protected string ReportId => HttpUtility.HtmlEncode(Page.Request.QueryString[@"ReportID"]);

    protected string ReportName
    {
        get
        {
            if (!string.IsNullOrEmpty(Page.Request.QueryString[@"ReportID"]))
            {
                using (var proxy = _isLayer.GetProxy())
                {
                    var dt =
                        proxy.Query(
                            @"SELECT R.Name FROM Cirrus.PolicyReports AS R WHERE R.PolicyReportID=@policyReportId",
                            new PropertyBag {{@"policyReportId", Page.Request.QueryString[@"ReportID"]}});
                    if (dt.Rows.Count > 0)
                    {
                        return dt.Rows[0]["Name"].ToString();
                    }
                }
            }

            return string.Empty;
        }
    }

    private string GetNodeName(out bool nodeNotExists, out bool permissionNotExist)
    {
        nodeNotExists = false;
        permissionNotExist = false;

        var nodeName = string.Empty;
        var nodeID = GetNodeId();

        if (nodeID.HasValue)
        {
            using (var proxy = _isLayer.GetProxy())
            {
                var node = proxy.Query(
                    $@"SELECT OrionNodes.Caption, OrionNodes.IPAddress FROM Cirrus.NodeProperties AS NcmNodes INNER JOIN Orion.Nodes AS OrionNodes ON NcmNodes.CoreNodeID=OrionNodes.NodeID WHERE NcmNodes.NodeID=@nodeID WITH LIMITATION {ViewInfo.LimitationID}"
                    , new PropertyBag() {{@"nodeID", nodeID}});
                if (node.Rows.Count > 0)
                {
                    nodeName = node.Rows[0]["Caption"].ToString();

                    nodeName = macroParser.ParseMacro(nodeID.Value, nodeName);

                    if (string.IsNullOrEmpty(nodeName))
                        nodeName = node.Rows[0]["IPAddress"].ToString();
                }
                else
                {
                    permissionNotExist = true;
                    node = proxy.Query(
                        @"SELECT OrionNodes.Caption FROM Cirrus.NodeProperties AS NcmNodes INNER JOIN Orion.Nodes AS OrionNodes ON NcmNodes.CoreNodeID=OrionNodes.NodeID WHERE NcmNodes.NodeID=@nodeID",
                        new PropertyBag() {{@"nodeID", nodeID}});
                    nodeName = node.Rows[0]["Caption"].ToString();
                }
            }
        }
        else
        {
            nodeNotExists = true;
        }

        return nodeName;
    }

    private Guid? GetNodeId()
    {
        Guid? nodeId = null;

        if (string.IsNullOrEmpty(Page.Request.QueryString[@"ConfigID"]))
        {
            PreviewMode();
        }
        else
        {
            using (var proxy = _isLayer.GetProxy())
            {
                var node = proxy.Query(@"SELECT C.NodeID FROM Cirrus.ConfigArchive AS C WHERE C.ConfigID=@configId",
                    new PropertyBag { { @"configId", Page.Request.QueryString[@"ConfigID"] } });

                if (node.Rows.Count > 0)
                {
                    nodeId = Guid.Parse(node.Rows[0]["NodeID"].ToString());
                }
            }
        }

        return nodeId;
    }

    private void PreviewMode()
    {
        using (var proxy = _isLayer.GetProxy())
        {
            var swql =
@"SELECT TOP 1 NcmNodes.NodeID, ConfigArchive.ConfigID 
FROM Cirrus.NodeProperties AS NcmNodes 
INNER JOIN Orion.Nodes AS OrionNodes ON NcmNodes.CoreNodeID=OrionNodes.NodeID 
INNER JOIN Cirrus.ConfigArchive AS ConfigArchive ON NcmNodes.NodeID=ConfigArchive.NodeID";

            var dataTable = proxy.Query(swql);

            if (dataTable.Rows.Count > 0)
            {
                Page.Response.Redirect($"{Page.Request.Url}&ConfigID={dataTable.Rows[0]["ConfigID"]}");
            }
            else
            {
                PageTitle = ViewInfo.ViewTitle;
                exMsg.ExceptionMessage = Resources.NCMWebContent.WEBDATA_VK_163;
                resourcePanel.Visible = false;
            }
        }
    }

    protected void SiteMapPath_OnInit(object sender, EventArgs e)
    {
        try
        {
            var renderer = rendererFactory();
            var nodeId = GetNodeId();

            if (nodeId.HasValue)
            {
                renderer.SetUpData(new KeyValuePair<string, string>(@"NodeID", nodeId.Value.ToString()));

                NodeSiteMapPath.SetUpRenderer(renderer);
            }
            else
            {
                NodeSiteMapPath.Visible = false;
            }
        }
        catch (Exception ex)
        {
            logger.Error("Error initializing the site map.", ex);
            throw;
        }
    }
}
