﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true"
    CodeFile="ConfigurationManagement.aspx.cs" Inherits="Orion_NCM_ConfigurationManagement" %>

<%@ Register TagPrefix="ncm" Src="~/Orion/NCM/Controls/IconNCMSettings.ascx" TagName="IconNCMSettings" %>
<%@ Register TagPrefix="ncm" Src="~/Orion/NCM/NavigationTabBar.ascx" TagName="NavigationTabBar" %>
<%@ Register TagPrefix="ncm1" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls"
    Assembly="SolarWinds.NCMModule.Web.Resources" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ncmHeadPlaceHolder" runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/PolicyReports/PolicyReport.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
    <link href="/Orion/NCM/Resources/Jobs/Jobs.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script type="text/javascript" src="ConfigManagement.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippetsHelper.js"></script>
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/JavaScript/Ext_UX/Ext.ux.form.FileUploadField.css" />
    <script type="text/javascript" src="/Orion/NCM/JavaScript/Ext_UX/Ext.ux.form.FileUploadField.js"></script>
    <script type="text/javascript">
        SW.NCM.CloseWindowHandlerUrl = '<%=CloseWindowHandlerUrl %>';
	    SW.NCM.GroupingStore = <%=GetGroupingStore() %>;
        SW.NCM.ConfigTypesStore = <%=GetConfigTypesStore() %>;
        SW.NCM.CPColumnModel = <%=GetCPColumnModel() %>;
        SW.NCM.GridPersonalizationState = <%=GetGridPersonalizationState() %>;
        SW.NCM.ShowApprovalRequestButton = <%=ShowApprovalRequestButton.ToString().ToLowerInvariant() %>;
        SW.NCM.NCMAccountRole = '<%=NCMAccountRole %>';
        SW.NCM.AllowNodeManagement = <%=AllowNodeManagement.ToString().ToLowerInvariant() %>;
        SW.NCM.IsDemoServer = <%=SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer.ToString().ToLowerInvariant() %>;
        SW.NCM.BaselineViolationStateError = <%=(int)SolarWinds.NCM.Contracts.Baseline.ViolationState.Error %>;
    </script>
    <style type="text/css">
        .baselines-status-tip {
            background-color: #F4F4F4;
            border: 1px solid #a6a6a6;
            color: black;
        }
        .baselines-status-tip-table {
            width: 100%;
            padding:0 5px;
        }
        .baselines-status-tip-table td
        {
            height: 25px;
            padding: 5px 5px 5px 0;
        }
        .baselines-status-tip-table tr:first-child td {
            border-bottom: 1px solid #a6a6a6;
        }
        .baselines-status-tip-table tr td:first-child {
            width: 20px;
        }

        #Grid td
        {
            padding: 0px 0px 0px 0px;
            border: 0px;
        }
        
        .x-tree-arrows .x-tree-elbow-plus
        {
            background-position: left top !important;
            background-image: url(/Orion/NCM/Resources/images/Button.Expand.gif) !important;
            vertical-align: top !important;
        }
        
        .x-tree-arrows .x-tree-elbow-minus
        {
            background-position: left top !important;
            background-image: url(/Orion/NCM/Resources/images/Button.Collapse.gif) !important;
            vertical-align: top !important;
        }
        
        .x-tree-arrows .x-tree-elbow-end-plus
        {
            background-position: left top !important;
            background-image: url(/Orion/NCM/Resources/images/Button.Expand.gif) !important;
            vertical-align: top !important;
        }
        
        .x-tree-arrows .x-tree-elbow-end-minus
        {
            background-position: left top !important;
            background-image: url(/Orion/NCM/Resources/images/Button.Collapse.gif) !important;
            vertical-align: top !important;
        }
        
        .tree-node-no-icon
        {
            background-image: none !important;
            height: 0px !important;
            width: 0px !important;
        }
        
        .tree-node-selected
        {
            font-weight: bold;
            background-image: url('/Orion/Nodes/images/background/left_selection_gradient.gif');
            background-color: #97d6ff !important;
            background-repeat: repeat-x;
        }
        
        .tree-grid-expander-expand
        {
            width: 16px !important;
            height: 16px !important;
            background-image: url(/Orion/NCM/Resources/images/Button.Expand.gif) !important;
        }
        
        .tree-grid-expander-expand:hover
        {
            cursor: pointer;
        }
        
        .tree-grid-expander-collapse
        {
            width: 16px;
            height: 16px;
            background-image: url(/Orion/NCM/Resources/images/Button.Collapse.gif) !important;
        }
        
        .tree-grid-expander-collapse:hover
        {
            cursor: pointer;
        }
        
        .tree-grid-expander-no-expand
        {
            padding-left: 22px;
        }
        
        .tree-grid-loading-img
        {
            width: 16px !important;
            height: 16px !important;
            background-image: url(/Orion/NCM/Resources/images/spinner.gif) !important;
            background-position: left;
            background-repeat: no-repeat;
            margin-left: 22px;
        }
        
        .tree-grid-inprogress
        {
            width: 16px !important;
            height: 16px !important;
            background-image: url(/Orion/NCM/Resources/images/spinner.gif) !important;
            background-position: left;
            background-repeat: no-repeat;
            border: 0px solid; 
        }
        
        .tree-grid-loading-text
        {
            padding-left: 18px;
        }
        
        .menu-item-text
        {
            line-height: 16px;
            padding-left: 5px;
            padding-right: 21px;
        }
        
        .menu-download a.x-menu-item
        {
            padding-left: 20px;
        }
        
        .menu-compare-configs a.x-menu-item
        {
            padding-left: 10px;
        }
        
        .menu-item-no-icon .x-menu-item
        {
            display: none;
        }
        
        .x-grid3-row td, .x-grid3-summary-row td
        {
            vertical-align: middle;
        }
        
        #selectAll a
        {
            color: Red;
            text-decoration: underline;
        }
        
        .select-all-page
        {
            background-color: #ffffcc;
        }
    </style>
    <ncm1:ExtLocalDateTimePattern ID="ExtLocalDateTimePattern1" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" runat="Server">
    <%=Page.Title %>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ncmExportToPDFPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ncmCustomizePagePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="ncmHelpIconPlaceHolder" runat="Server">
    <ncm:IconNCMSettings ID="IconNCMSettings1" runat="server" />
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="ncmMainContentPlaceHolder" runat="Server">
    <%foreach (string setting in new string[] { @"Grouping", @"PageSize" })
      { %>
    <input type="hidden" name="NCM_ConfigManagement_<%=setting%>" id="NCM_ConfigManagement_<%=setting%>"
        value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get($@"NCM_ConfigManagement_{setting}")%>' />
    <%}%>
    <asp:Panel ID="ContentContainer" runat="server">
        <div style="padding: 10px;">
            <ncm:NavigationTabBar ID="NavigationTabBar1" runat="server">
                <SearchPlaceholder>
                    <span id="searchBox"></span>
                </SearchPlaceholder>
            </ncm:NavigationTabBar>
            <div id="tabPanel" class="tab-top">
                <div>
                    <table class="ExistingElements" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td colspan="2">
                                <div id="selectAll" style="padding: 3px">
                                </div>
                            </td>
                        </tr>
                        <tr valign="top" align="left">
                            <td id="treeCell" style="width: 320px;">
                                <div class="ElementGrouping" style="border: 0px;">
                                    <div class="ncm_GroupSection x-panel-header x-toolbar x-small-editor">
                                        <div style="padding-left: 5px;">
                                            <%=Resources.NCMWebContent.WEBDATA_VK_80 %></div>
                                        <div id="combo1" style="padding: 5px;">
                                        </div>
                                        <div id="combo2" style="padding: 5px;">
                                        </div>
                                        <div id="combo3" style="padding: 5px;">
                                        </div>
                                    </div>
                                    <div id="treePanel">
                                    </div>
                                </div>
                            </td>
                            <td id="gridCell" style="padding-left: 10px;">
                                <div class="Suggestion" style="margin-left: 0px; margin-bottom: 10px;" runat="server" id="divShowUseUserLevelLoginSuggestion">
                                    <div class="Suggestion-Icon">
                                    </div>                       
                                    <%=string.Format(Resources.NCMWebContent.WEBDATA_VK_714, @"<a style='color:#336699;text-decoration:underline;' target='_blank' href='/Orion/NCM/Admin/Settings/UserLevelLoginCreds.aspx'>", @"</a>")%>
                                </div>
                                <span id="searchResults"></span>
                                <div id="toolbar">
                                </div>
                                <div id="gridSelection" style="padding: 10px 0px 10px 0px;">
                                </div>
                                <div id="Grid" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </asp:Panel>
    <div style="padding: 10px;" id="divError" runat="server" />
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="ncmOutsideFormPlaceHolder" runat="Server">
</asp:Content>
