<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" AutoEventWireup="true" CodeFile="ComplianceReports.aspx.cs" Inherits="ComplianceReports"  %>

<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register Src="~/Orion/NCM/CustomPageLinkHidding.ascx" TagPrefix="ncm" TagName="custPageLinkHidding" %>
<%@ Register TagPrefix="ncm1" Namespace="SolarWinds.NCMModule.Web.Resources.Resources.Controls" Assembly="SolarWinds.NCMModule.Web.Resources" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="ncm" Src="~/Orion/NCM/Controls/IconNCMSettings.ascx" TagName="IconNCMSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmCustomizePagePlaceHolder" runat="server">
    <a href="/Orion/NCM/Admin/PolicyReports/PolicyReportManagement.aspx" class="mngPolicyReportLink"><%=Resources.NCMWebContent.WEBDATA_VY_02%></a>
    <ncm:IconNCMSettings ID="IconNCMSettings1" runat="server"/>
    <orion:IconHelpButton ID="helpBtn" runat="server" HelpUrlFragment="OrionNCMWebCompPolicyReports" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" runat="server">
    <%=Page.Title%>
</asp:Content>

<asp:Content ID="PageTitleContent" ContentPlaceHolderID="ncmHeadPlaceHolder" Runat="Server">
    <script type="text/javascript">
        var IsDemoMode = <%=SolarWinds.NCMModule.Web.Resources.CommonHelper.IsDemoMode().ToString().ToLowerInvariant() %>;
        var disabledReportValue = <%= disabledReportValue%>;
        var enabledReportValue = <%= enabledReportValue%>;
    </script>
    
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <ncm:custPageLinkHidding ID="CustPageLinkHidding1"  runat="server" />
    <ncm1:ConfigSnippetUISettings ID="ReportUISetting" runat="server" Name="NCM_PolicyReportView_Columns" DefaultValue="[]" RenderTo="columnModel" />
    <ncm1:ExtLocalDateTimePattern ID="ExtLocalDateTimePattern1" runat="server"/>
    
    <link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/PolicyReports/PolicyReport.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
    
    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Admin/PolicyReports/PolicyReportViewer.js"></script>
    <script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippetsHelper.js"></script>
    
      <style type="text/css">
        .mngPolicyReportLink
        {
            background-image:url("/Orion/NCM/Resources/images/PolicyIcons/manage_policy_reports.gif");
            background-position:left center;
            background-repeat:no-repeat;
            padding:2px 0 2px 20px;
            text-align:right;
        }
        
        .ncmDataArea
        {
            border-style:none;
            float:right;
            font-family:Arial,Helvetica,sans-serif;
            font-size:8pt;
            margin-right:5px;
            margin-top:0;
            padding-bottom:0px;
            padding-top:0px;
            text-align:right;
        }

        #selectAll a { color: Red; text-decoration: underline; }
        .select-all-page { background-color: #ffffcc; }
    </style>
    
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
    
    <%foreach (string setting in new string[] { @"GroupByValue"}) { %>
		<input type="hidden" name="NCM_PolicyReportView_<%=setting%>" id="NCM_PolicyReportView_<%=setting%>" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get($@"NCM_PolicyReportView_{setting}")%>' />
	<%}%>
    
    <asp:Panel id="ContentContainer" runat="server">
        <div style="padding:10px;">
            <table class="ExistingElements" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td colspan="2" align="right">
                        <div id="refreshContainer"style="margin-top:6px;font-size:8pt;display:none;">
                            <%=Resources.NCMWebContent.WEBDATA_VY_07%><span id='refreshtime' style="font-size:8pt;">30s</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div id="selectAll" style="padding:3px"></div>
                    </td>
                </tr>
                <tr valign="top" align="left">
                    <td style="width: 220px;">
                        <div class="ElementGrouping">
                            <div class="ncm_GroupSection x-panel-header x-toolbar x-small-editor">
                                <div><%=Resources.NCMWebContent.WEBDATA_VY_01%></div>
                            </div>
                            <div>
                                <ul class="GroupItems" style="width:220px;"></ul>
                            </div>
                        </div>
                    </td>
                    <td id="gridCell">
                        <div id="Grid" />
                    </td>
                </tr>
            </table>
            <div id="originalQuery">
            </div>
            <div id="test">
            </div>
            <pre id="stackTrace"></pre>
        </div>
    </asp:Panel>
    <div style="padding:10px;" id="ErrorContainer" runat="server"/>
</asp:Content>


