﻿Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.QuickTips.init();

SW.NCM.TransferStatusViewer = function () {

    var selectorModel;
    var dataStore;
    var grid;

    function gridResize() {
        if (grid != null) {
            grid.setWidth(1);
            grid.setWidth($('#gridPH').width());

            var h = getGridHeight();
            grid.setHeight(h);
        }
    }

    function getGridHeight() {
        var top = $('#gridPH').offset().top;
        return $(window).height() - top - $('#footer').height() - 100;
    }

    var getDateTimeOffset = function () {
        var d = new Date();
        var n = d.getTimezoneOffset();
        return n;
    }

    var currentSearchTerm = null;
    var refreshObjects = function (search) {
        grid.store.removeAll();
        var pagingToolbar = grid.getBottomToolbar();
        pagingToolbar.cursor = 0;
        grid.store.baseParams['limit'] = grid.getBottomToolbar().pageSize;
        grid.store.proxy.conn.jsonData = { search: search, clientOffset: getDateTimeOffset() };
        currentSearchTerm = search;
        pagingToolbar.doLoad(pagingToolbar.cursor);
    };

    //search
    var doClean = function () {
        var map = grid.getTopToolbar().items.map;
        map.Clean.setVisible(false);
        map.txtSearch.setValue('');
        refreshObjects("");
    };

    var doSearch = function () {
        var map = grid.getTopToolbar().items.map;
        var search = map.txtSearch.getValue();
        if ($.trim(search).length == 0) return;
        refreshObjects(search);
        map.Clean.setVisible(true);
    };

    // btn handlers
    var updateToolbarButtons = function () {
        var selCount = grid.getSelectionModel().getCount();
        var map = grid.getTopToolbar().items.map;

        var atLeastOneSelected = (selCount > 0);
        var hasActiveSelected = getSelectedIDs(grid.getSelectionModel().getSelections(), true).length > 0;
        var hasInactiveSelected = getInactiveSelectedIDs(grid.getSelectionModel().getSelections()).length > 0;
        var hasScriptResultSelected = getScriptResultIds(grid.getSelectionModel().getSelections()).length > 0;

        map.ClearComplete.setDisabled(atLeastOneSelected);
        map.ClearFailed.setDisabled(atLeastOneSelected);
        map.ReExecute.setDisabled(!hasInactiveSelected);
        map.ShowAllResults.setDisabled(!hasScriptResultSelected);

        if (atLeastOneSelected)
            map.CancelAll.setDisabled(!hasActiveSelected);
        else
            map.CancelAll.setDisabled(false);

        map.CancelAll.setText(atLeastOneSelected ? '@{R=NCM.Strings;K=WEBJS_VK_87;E=js}' : '@{R=NCM.Strings;K=WEBJS_VK_86;E=js}');
        map.ClearAll.setText(atLeastOneSelected ? '@{R=NCM.Strings;K=WEBJS_VK_326;E=js}' : '@{R=NCM.Strings;K=WEBJS_VK_94;E=js}');
    };

    var clearAll = function (items) {
        var transferIds = items != null && items.length ? getSelectedIDs(items) : null;

        ORION.callWebService("/Orion/NCM/Services/TransferStatus.asmx",
            "ClearTransfers", { ids: transferIds },
            function (result) { errorHandler(result); refreshObjects(currentSearchTerm); });
    };

    var cancellAll = function (items) {
        var transferIds = items != null && items.length > 0 ? getSelectedIDs(items, true) : null;

        ORION.callWebService("/Orion/NCM/Services/TransferStatus.asmx",
            "CancelTransfers", { ids: transferIds },
            function (result) { errorHandler(result); refreshObjects(currentSearchTerm); });
    };

    var clearComplete = function () {
        ORION.callWebService("/Orion/NCM/Services/TransferStatus.asmx",
            "ClearCompletedOrFailedTransfers", { failedOnly: false },
            function (result) { errorHandler(result); refreshObjects(currentSearchTerm); });
    };

    var clearFailed = function () {
        ORION.callWebService("/Orion/NCM/Services/TransferStatus.asmx",
            "ClearCompletedOrFailedTransfers", { failedOnly: true },
            function (result) { errorHandler(result); refreshObjects(currentSearchTerm); });
    };

    var reExecute = function (items) {
        var transferIds = items != null && items.length > 0 ? getSelectedIDs(items, false) : null;
        ORION.callWebService("/Orion/NCM/Services/TransferStatus.asmx",
            "RetryScript", { ids: transferIds },
            function (result) {
                errorHandler(result);
                refreshObjects(currentSearchTerm);
            });
    };

    var showScriptResultsBtnHandler = function(items) {
        var ids = getScriptResultIds(items);
        showScriptResults(ids);
    };

    var showScriptResults = function (ids) {

        var body = $("#scriptResultsTextArea").text("Loading...");

        ORION.callWebService("/Orion/NCM/Services/TransferStatus.asmx",
            "GetScriptResults", { transferIds: ids },
            function (result) {
                var desc = $.trim(result);
                desc = desc.replace(/(?:\r\n|\r|\n)/g, "\r\n");
                body.empty().val(desc);
                return showDlg();
            });
    };

    //auto update status column
    var timerRunner;

    function AutoRefresh() {
        var task = {
            run: function () {
                var ids = getTransInProgress();
                if (ids.length > 0)
                    requestStatuses(ids);
            },
            interval: 5000
        };

        timerRunner = new Ext.util.TaskRunner();
        timerRunner.start(task);

    };

    var updateStatusDetailsColumns = function (transfers) {

        transfers.forEach(function (transfer) {

            var recordIndex = grid.store.findExact("TransferId", transfer.id);
            if (recordIndex >= 0) {
                var recordToUpdate = grid.store.getAt(recordIndex);
                recordToUpdate.set('StatusDetails', transfer.statusDetails);
                recordToUpdate.set('Status', transfer.status);

                if (transfer.action == 1 && transfer.status == 3) {
                    recordToUpdate.set('NodeID', transfer.nodeId);
                }

                recordToUpdate.commit();
            }
        });
    };

    var requestStatuses = function (transferIds) {

        ORION.callWebService("/Orion/NCM/Services/TransferStatus.asmx",
           "GetTransStatusDetails", { ids: transferIds },
            function (result) {
                var obj = JSON.parse(result);
                if (obj != null) {
                    updateStatusDetailsColumns(obj);
                }
            });
    };

    var getTransInProgress = function () {
        var ids = [];
        grid.store.data.each(function (record, index, length) {
            if (record.data.Status == 0 || record.data.Status == 1)
                ids.push(record.data.TransferId);
        });
        return ids;
    };

    var manageDeviceTemplateClick = function (obj) {
        var ncmNodeID = obj.attr("value");
        showWindow('@{R=NCM.Strings;K=WEBJS_VK_330;E=js}', 'iframeContainer', function () {
            window.frames.iframeContainer.location.href = String.format('/Orion/NCM/Admin/DeviceTemplate/DeviceTemplateLinkHandler.ashx?printable=true&NodeID={0}&ReturnUrl={1}', ncmNodeID, SW.NCM.CloseWindowHandlerUrl);

            $('#iframeContainer').load(function () {
                $('#iframeContainer').contents().find("head").append($("<style type='text/css'> #DeviceTemplateContainer {width:98% !important; padding: 8px !important;} #content { padding-bottom: 10px !important;}  </style>"));
            });
        });
    }

    //helpers
    function DateTimeConverter(strDateTime) {
        var date = eval(strDateTime.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
        return Ext.util.Format.date(date, ExtDaTimeLocalePattern.DateFull);
    }

    var showWindow = function (title, iframeContainerId, callback) {
        var window = new Ext.Window({
            id: 'window',
            layout: 'anchor',
            width: '60%',
            minWidth: 800,
            height: 910,
            minHeight: 400,
            closeAction: 'close',
            title: title,
            modal: true,
            resizable: true,
            maximizable: true,
            plain: true,
            html: String.format('<iframe name="{0}" id="{0}" frameborder="0" style="overflow:auto;" class="x-panel-body" width="100%" height="100%" />', iframeContainerId),
            buttons: [{
                text: '@{R=NCM.Strings;K=WEBJS_VK_07;E=js}',
                handler: function () {
                    window.close();
                }
            }],
            listeners: {
                show: function (that) {
                    that.alignTo(document.body, "c-c");
                }
            }
        }).show(null, callback);
    }

    var dlgInstance;
    var showDlg = function (subtitle) {
        if (!dlgInstance) {
            dlgInstance = new Ext.Window({
                applyTo: 'scriptResultsDlg',
                layout: 'fit',
                width: 960,
                height: 600,
                closeAction: 'hide',
                plain: true,
                resizable: true,
                modal: true,
                contentEl: 'scriptResultsBody',
                buttons: [{
                    text: '@{R=NCM.Strings;K=WEBJS_VK_07;E=js}',
                    handler: function () { dlgInstance.hide(); }
                }]
            });
        }

        // Set the window caption
        dlgInstance.setTitle(String.format("@{R=NCM.Strings;K=TransferStatus_Label_ActionResults;E=js}<br>{0}", subtitle));

        // Set the location
        dlgInstance.alignTo(document.body, "c-c");
        dlgInstance.show();

        return false;
    };

    var getSelectedIDs = function (items, activeOnly) {
        var ids = [];
        Ext.each(items, function (item) {
            if (typeof activeOnly === "undefined" || activeOnly == false) {
                ids.push(item.data.TransferId);
            } else {
                if (item.data.Status == 0 || item.data.Status == 1)
                    ids.push(item.data.TransferId);
            }

        });
        return ids;
    };

    var getScriptResultIds = function (items) {
        var ids = [];
        Ext.each(items, function (item) {
            if ((item.data.Action === 2 || item.data.Action === 3) && (item.data.Status == 2 || item.data.Status == 3))
                ids.push(item.data.TransferId);
        });

        return ids;
    };

    var getInactiveSelectedIDs = function (items) {
        var ids = [];
        Ext.each(items, function (item) {
            if (item.data.Status == 2 || item.data.Status == 3)
                ids.push(item.data.TransferId);
        });
        return ids;
    };

    function errorHandler(result) {
        if (result != null && result.Error) {
            Ext.Msg.show({
                title: result.Source,
                msg: result.Msg,
                minWidth: 500,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        }
    }

    //columns render
    function renderDateTime(value, meta, record) {
        if (value == null) return '@{R=NCM.Strings;K=WEBJS_VK_184;E=js}';

        return DateTimeConverter(value);
    }

    function renderActionName(value, meta, record) {
        if (value != null) {
            switch (value) {
                case 1:
                    return '@{R=NCM.Strings;K=TransferStatus_ActionLabel_Downlaod;E=js}';
                case 2:
                    return '@{R=NCM.Strings;K=TransferStatus_ActionLabel_Upload;E=js}';
                case 3:
                    return '@{R=NCM.Strings;K=TransferStatus_ActionLabel_ExecuteScript;E=js}';
            }
        }
        return '';
    }

    function renderActionIcon(value, meta, record) {
        if (record.data.Action != null) {
            switch (record.data.Action) {
                case 1:
                    return '<img src="/Orion/NCM/Resources/images/Events/EventConfigDownload.gif">';
                case 2:
                    return '<img src="/Orion/NCM/Resources/images/Events/EventConfigUpload.gif">';
                case 3:
                    return '<img src="/Orion/NCM/Resources/images/Events/EventUploadScripts.gif">';
            }
        }
        return '';
    }

    function renderDeviceTemplateLink(value, meta, record) {
        if (value == null) return '';
        if (!(SW.NCM.NCMAccountRole == 'Administrator') || !SW.NCM.AllowNodeManagement) return '';
        if (record.data.Status == 3)
            return String.format('<a href="#" class="ncm_SuggestedAction" value="{0}">@{R=NCM.Strings;K=WEBJS_VK_330;E=js}</a>', value);
        else
            return '';
    }

    function renderStatus(value, meta, record) {
        return value;
    }

    function renderNodeName(value, meta, record) {
        return String.format("<a href=\"/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}\">{1}</a>", record.data.CoreNodeID, value);
    }

    return {
        init: function () {

            if ($("[id$='_ContentContainer']").length == 0)
                return false;

            $("#Grid").click(function (e) {

                var obj = $(e.target);
                if (obj.hasClass('scriptResults')) {

                    var id = obj.attr("value");
                    showScriptResults([id]);
                    return false;
                }

                if (obj.hasClass('ncm_SuggestedAction')) {
                    manageDeviceTemplateClick(obj);
                    return false;
                }
            });

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", updateToolbarButtons);

            dataStore = new ORION.WebServiceStore(
                                "/Orion/NCM/Services/TransferStatus.asmx/GetTransfersPaged",
                                [
                                    { name: 'TransferId', mapping: 0 },
                                    { name: 'DateTime', mapping: 1 },
                                    { name: 'Action', mapping: 2 },
                                    { name: 'NodeName', mapping: 3 },
                                    { name: 'IpAddress', mapping: 4 },
                                    { name: 'UserName', mapping: 5 },
                                    { name: 'StatusDetails', mapping: 6 },
                                    { name: 'ActionIcon', mapping: 7 },
                                    { name: 'CoreNodeID', mapping: 8 },
                                    { name: 'Status', mapping: 9 },
                                    { name: 'NodeID', mapping: 12 }
                                ],
                                "DateTime");

            dataStore.setDefaultSort("DateTime", "DESC");

            var comboPS = new Ext.form.ComboBox({
                name: 'perpage',
                width: 50,
                store: new Ext.data.SimpleStore({
                    fields: ['id'],
                    data: [
                                  ['25'],
                                  ['50'],
                                  ['75'],
                                  ['100']
                    ]
                }),
                mode: 'local',
                value: '25',
                listWidth: 50,
                triggerAction: 'all',
                displayField: 'id',
                valueField: 'id',
                editable: false,
                forceSelection: true
            });

            var pagingToolbar = new Ext.PagingToolbar({
                store: dataStore,
                pageSize: 25,
                displayInfo: true,
                displayMsg: '@{R=NCM.Strings;K=TransferStatus_Label_DisplayingRows;E=js}',
                emptyMsg: '@{R=NCM.Strings;K=TransferStatus_Label_NoTransfers;E=js}',
                items: ['-', '@{R=NCM.Strings;K=WEBJS_VK_12;E=js}', comboPS]
            });

            comboPS.on('select', function (combo, record) {
                var psize = parseInt(record.get('id'), 10);
                grid.store.baseParams['limit'] = psize;
                pagingToolbar.pageSize = psize;
                pagingToolbar.cursor = 0;
                pagingToolbar.doLoad(pagingToolbar.cursor);
            }, this);

            grid = new Ext.grid.GridPanel({
                id: 'gridControl',
                store: dataStore,
                columns: [selectorModel, {
                    header: 'TransferId',
                    width: 80,
                    hidden: true,
                    hideable: false,
                    sortable: true,
                    dataIndex: 'TransferId'
                }, {
                    header: '',
                    width: 24,
                    dataIndex: 'ActionIcon',
                    sortable: false,
                    hideable: false,
                    resizable: false,
                    menuDisabled: true,
                    renderer: renderActionIcon
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_92;E=js}',
                    width: 200,
                    sortable: true,
                    dataIndex: 'DateTime',
                    css: 'height:23px;',
                    renderer: renderDateTime
                }, {
                    header: '@{R=NCM.Strings;K=TransferStatus_ColumnHeader_Action;E=js}',
                    width: 150,
                    sortable: true,
                    dataIndex: 'Action',
                    css: 'height:23px;',
                    renderer: renderActionName
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_88;E=js}',
                    width: 200,
                    sortable: true,
                    dataIndex: 'NodeName',
                    css: 'height:23px;',
                    renderer: renderNodeName
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_89;E=js}',
                    width: 150,
                    sortable: true,
                    dataIndex: 'IpAddress',
                    css: 'height:23px;'
                }, {
                    header: '@{R=NCM.Strings;K=WEBJS_VK_91;E=js}',
                    width: 150,
                    dataIndex: 'UserName',
                    sortable: true,
                    css: 'height:23px;'
                }, {
                    header: '@{R=NCM.Strings;K=TransferStatus_ColumnHeader_StatusDetails;E=js}',
                    width: 500,
                    sortable: true,
                    dataIndex: 'StatusDetails',
                    css: 'height:23px;',
                    renderer: renderStatus
                }, {
                    header: '@{R=NCM.Strings;K=TransferStatus_ColumnHeader_SuggestedAction;E=js}',
                    width: 250,
                    sortable: false,
                    dataIndex: 'NodeID',
                    css: 'height:23px;',
                    renderer: renderDeviceTemplateLink
                }],

                sm: selectorModel,
                layout: 'fit',
                autoScroll: 'true',
                loadMask: true,
                stateful: true,
                stateId: 'gridState',
                width: 1250,
                height: 500,
                stripeRows: true,

                tbar: [{
                    id: 'CancelAll',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_86;E=js}',
                    icon: '/Orion/NCM/Resources/images/Toolbar.Cancel.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { cancellAll(grid.getSelectionModel().getSelections()); }
                }, '-', {
                    id: 'ClearAll',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_94;E=js}',
                    icon: '/Orion/NCM/Resources/images/Toolbar.ClearAll.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { clearAll(grid.getSelectionModel().getSelections()); }
                }, '-', {
                    id: 'ClearComplete',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_95;E=js}',
                    icon: '/Orion/NCM/Resources/images/Toolbar.ClearComplete.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { clearComplete(); }
                }, '-', {
                    id: 'ClearFailed',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_96;E=js}',
                    icon: '/Orion/NCM/Resources/images/Toolbar.ClearFailed.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { clearFailed(); }
                }, '-', {
                    id: 'ReExecute',
                    text: '@{R=NCM.Strings;K=WEBJS_VK_327;E=js}',
                    icon: '/Orion/NCM/Resources/images/PolicyIcons/update_all.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { reExecute(grid.getSelectionModel().getSelections()); }
                }, '-', {
                    id: 'ShowAllResults',
                    text: '@{R=NCM.Strings;K=TransferStatus_Button_ShowScriptResults;E=js}',
                    icon: '/Orion/NCM/Resources/images/InventoryIcons/icon_inventory_report.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { showScriptResultsBtnHandler(grid.getSelectionModel().getSelections()); }
                }, '->', {
                    id: 'txtSearch',
                    xtype: 'textfield',
                        emptyText: '@{R=NCM.Strings;K=TransferStatus_Label_SearchTransfers;E=js}',
                    width: 200,
                    enableKeyEvents: true,
                    listeners: {
                        keypress: function (obj, evnt) {
                            if (evnt.keyCode == 13) {
                                doSearch();
                            }
                        }
                    }
                }, {
                    id: 'Clean',
                    tooltip: '@{R=NCM.Strings;K=WEBJS_VK_30;E=js}',
                    iconCls: 'ncm_clear-btn',
                    cls: 'x-btn-icon',
                    hidden: true,
                    handler: function () { doClean(); }
                }, {
                    id: 'Search',
                        tooltip: '@{R=NCM.Strings;K=TransferStatus_Label_SearchTransfers;E=js}',
                    iconCls: 'ncm_search-btn',
                    cls: 'x-btn-icon',
                    handler: function () { doSearch(); }
                }],
                bbar: pagingToolbar
            });

            grid.render('Grid');
            updateToolbarButtons();

            gridResize();
            $(window).resize(function () {
                gridResize();
            });

            refreshObjects('');
            AutoRefresh();
            // highlight the search term (if we have one)            
            grid.getView().on("refresh", function () {
                var search = currentSearchTerm;
                if ($.trim(search).length == 0) return;

                var columnsToHighlight = SW.NCM.ColumnIndexByName(grid.getColumnModel(), ['Node Name', 'Ip Address']);
                Ext.each(columnsToHighlight, function (columnNumber) {
                    SW.NCM.highlightSearchText("#Grid .x-grid3-body .x-grid3-td-" + columnNumber, search);
                });
            });
        }
    };
}();
Ext.onReady(SW.NCM.TransferStatusViewer.init, SW.NCM.TransferStatusViewer);
