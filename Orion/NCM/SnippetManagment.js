﻿Ext.namespace('SW');
Ext.namespace('SW.NCM');
Ext.QuickTips.init();

SW.NCM.SnippetManagment = function () {

    ORION.prefix = "NCM_SnippetManagement_";

    var grid;
    var gridStore;
    var selectorModel;
    var pageSize;
    var bottomPageSizeBox;
    var eventType;
    var currentSearchTerm = '';

    function RenderDateTime(value) {
        if (value == null) return '@{R=NCM.Strings;K=WEBJS_VK_184;E=js}';

        var date = eval(value.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
        return Ext.util.Format.date(date, ExtDaTimeLocalePattern.DateFull);
    }

    function CreateNewSnippet(configId, snippetConfig, snippetTitle, snippetComments) {

        ORION.callWebService("/Orion/NCM/Services/SnippetManagement.asmx",
                "AddNewSnippet", { configTitle: snippetTitle, config: snippetConfig, comments: snippetComments },
                function (result) {
                    errorHandler('@{R=NCM.Strings;K=WEBJS_VK_312;E=js}', result);
                    ReloadGrid();
                }
            );        
    }

    function EditSnippet(configId, snippetConfig, snippetTitle, snippetComments) {
        ORION.callWebService("/Orion/NCM/Services/SnippetManagement.asmx",
                "EditSnippet", { configId: configId, configTitle: snippetTitle, config: snippetConfig, comments: snippetComments },
                function (result) {
                    errorHandler('@{R=NCM.Strings;K=WEBJS_VK_313;E=js}', result);
                    ReloadGrid();
                }
            );        
    }

    function RemoveSnippet(configId) {
        ORION.callWebService("/Orion/NCM/Services/SnippetManagement.asmx",
                "DeleteSnippet", { configId: configId },
                function (result) {
                    errorHandler('@{R=NCM.Strings;K=WEBJS_VK_314;E=js}', result);
                    ReloadGrid();
                }
            );        
    }

    function ReloadGrid() {
        var bottomPagingToolbar = grid.getBottomToolbar();
        var currentPageRecordCount = grid.store.getCount();

        var selectedRecordCount = grid.getSelectionModel().getCount();
        if (currentPageRecordCount > selectedRecordCount) {
            bottomPagingToolbar.doLoad(bottomPagingToolbar.cursor);
        }
        else {            
            bottomPagingToolbar.doLoad(0);
        }        
    }

    var doClean = function () {
        var map = grid.getTopToolbar().items.map;

        map.Clean.setVisible(false);
        map.txtSearch.setValue('');
        currentSearchTerm = '';
        ReloadStore('');
    };

    var doSearch = function () {

        var map = grid.getTopToolbar().items.map;
        var search = map.txtSearch.getValue();

        if ($.trim(search).length == 0) return;
        $(".group-inventory-report-item").removeClass("SelectedGroupItem");
        currentSearchTerm = search;

        ReloadStore(search);

        map.Clean.setVisible(true);
    };

    var errorHandler = function (source, result, func) {
        if (result != null && result.Error) {
            Ext.Msg.show({
                title: source,
                msg: result.Msg,
                minWidth: 500,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR,
                fn: func
            });
        }
    };

    function gridResize() {
        if (grid != null) {
            grid.setWidth(1);
            grid.setWidth($('#gridCell').width());

            var h = getGridHeight();
            grid.setHeight(h);
        }
    }

    function alignWindow() {
        if (createEditSnippetDialog != null) {
            createEditSnippetDialog.alignTo(Ext.getBody(), "c-c");
        }
    }

    function getGridHeight() {
        var top = $('#gridCell').offset().top;
        return $(window).height() - top - $('#footer').height() - 100;
    }

    function ReloadStore(searchTerm) {
        gridStore.proxy.conn.jsonData = { searchTerm: searchTerm };
        gridStore.load();
    }

    function UpdateTopToolBar() {
        var selCount = grid.getSelectionModel().getCount();
        var map = grid.getTopToolbar().items.map;
        var needsOnlyOneSelected = (selCount != 1);
        var needsOnlyManySelection = !(selCount >= 1);

        map.DeleteSnippet.setDisabled(needsOnlyManySelection);
        map.EditSnippet.setDisabled(needsOnlyOneSelected);

        var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');
        if (grid.getStore().getCount() > 0 && selCount == grid.getStore().getCount()) {
            hd.addClass('x-grid3-hd-checker-on');
        } else {
            hd.removeClass('x-grid3-hd-checker-on');
        }
    }

    ShowEitDialog = function () {
        eventType = 'Edit';
        $(".x-grid3-hd-checker-on").removeClass('x-grid3-hd-checker-on');
        ShowCreateEditSnippetDialog();
    };

    function RenderSnippetName(value, meta, record) {
        return String.format('<span onclick="ShowEitDialog();" style="font-weight:bold;color:#4779C4; cursor:pointer" value="{0}">{1}</span>', record.data.ID, Ext.util.Format.htmlEncode(value));
    }

    function RenderComments(value) {
        return Ext.util.Format.htmlEncode(value);
    }

    function VerifySnippetName() {
        var result = true;
        if (!(/\S/.test(snippetNameField.getValue())))
            result = false;

        return result;
    }

    var createEditSnippetDialog;
    var snippetNameField;
    ShowCreateEditSnippetDialog = function () {
        if (!snippetNameField) {
            snippetNameField = new Ext.form.TextField({
                id: 'snippetNameField',
                renderTo: 'snippetContentBody',
                emptyText: '@{R=NCM.Strings;K=WEBJS_IA_11;E=js}',                
                disabled: false,
                allowBlank: false,
                width: 200
            });
        }

        if (!createEditSnippetDialog) {
            createEditSnippetDialog = new Ext.Window({
                id: 'createEditSnippetDialog',
                applyTo: 'createEditSnippetDialog',
                layout: 'fit',
                width: 780,
                height: 625,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                modal: true,
                contentEl: 'createEditSnippetDialogBody',
                buttons: [
                    {
                        id: 'btnSave',
                        cls: 'sw-btn-primary',
                        text: '@{R=NCM.Strings;K=WEBJS_VK_188;E=js}',
                        handler: function () {
                            var snippetTitle = snippetNameField.getValue();
                            var config = $("#txtConfigContent").val();
                            var comments = $("#txtSnippetComments").val();
                            if (VerifySnippetName()) {
                                if (eventType === 'create') {
                                    CreateNewSnippet(-1, config, snippetTitle, comments);
                                } else {
                                    var snippetConfigId = grid.getSelectionModel().getSelected().data.ConfigID;
                                    EditSnippet(snippetConfigId, config, snippetTitle, comments);
                                }

                                createEditSnippetDialog.hide();
                            } else {
                                $("#snippetNameField").addClass("x-form-empty-field");
                                $("#snippetNameField").addClass("x-form-invalid");
                            }

                        }
                    }, {
                        text: '@{R=NCM.Strings;K=WEBJS_VK_38;E=js}',
                        handler: function () {
                            createEditSnippetDialog.hide();
                        }
                    }
                ]
            });
        }

        $("#txtConfigContent").val('');
        $("#txtSnippetName").val('');
        $("#txtSnippetComments").val('');

        if (eventType === 'create') {
            createEditSnippetDialog.setTitle('@{R=NCM.Strings;K=WEBJS_IA_05;E=js}');
            snippetNameField.setValue('');
        } else {
            createEditSnippetDialog.setTitle('@{R=NCM.Strings;K=WEBJS_IA_06;E=js}');

            var snippetTitle = grid.getSelectionModel().getSelected().data.ConfigTitle;
            var snippetConfig = grid.getSelectionModel().getSelected().data.Config;
            var snippetComments = grid.getSelectionModel().getSelected().data.Comments;

            $("#txtConfigContent").val(snippetConfig);            
            $("#txtSnippetComments").val(snippetComments);
            snippetNameField.setValue(snippetTitle);
        }

        createEditSnippetDialog.alignTo(document.body, "c-c");
        createEditSnippetDialog.show();


        return false;
    };

    return {
        init: function () {
            if ($("[id$='_ContentContainer']").length == 0)
                return false;

            gridStore = new ORION.WebServiceStore(
                "/Orion/NCM/Services/SnippetManagement.asmx/GetSnippetPaged",
                [
                    { name: 'ConfigID', mapping: 0 },
                    { name: 'ConfigTitle', mapping: 1 },
                    { name: 'DownloadTime', mapping: 2 },
                    { name: 'ModifiedTime', mapping: 3 },
                    { name: 'Config', mapping: 4 },
                    { name: 'Comments', mapping: 5 }
                ],
                "ConfigTitle");
            
            pageSize = parseInt(ORION.Prefs.load('PageSize', '25'));

            bottomPageSizeBox = new Ext.form.NumberField({
                id: 'bottomPageSizeBox',
                enableKeyEvents: true,
                allowNegative: false,
                width: 30,
                allowBlank: false,
                minValue: 1,
                maxValue: 100,
                value: pageSize,
                listeners: {
                    scope: this,
                    'keydown': function (f, e) {
                        var k = e.getKey();
                        if (k == e.RETURN) {
                            e.stopEvent();
                            var value = parseInt(f.getValue());
                            if (!isNaN(value) && value > 0 && value <= 100) {
                                pageSize = value;
                                var bottomPagingToolbar = grid.getBottomToolbar();
                                if (bottomPagingToolbar.pageSize != pageSize) { // update page size only if it is different                                

                                    bottomPagingToolbar.pageSize = pageSize;
                                    ORION.Prefs.save('PageSize', pageSize);
                                    bottomPagingToolbar.doLoad(0);
                                }
                            } else {
                                return;
                            }
                        }
                    },
                    'focus': function (field) {
                        field.el.dom.select();
                    },
                    'change': function (f) {
                        var value = parseInt(f.getValue());
                        if (!isNaN(value) && value > 0 && value <= 100) {
                            pageSize = value;
                            var bottomPagingToolbar = grid.getBottomToolbar();
                            if (bottomPagingToolbar.pageSize != pageSize) { // update page size only if it is different
                                ORION.Prefs.save('PageSize', pageSize);
                                bottomPagingToolbar.pageSize = pageSize;
                                bottomPagingToolbar.doLoad(0);
                            }
                        } else {
                            return;
                        }
                    }
                }
            });


            var bottomPagingToolbar = new Ext.PagingToolbar({
                id: 'bottomPagingToolbar',
                store: gridStore,
                pageSize: pageSize,
                displayInfo: true,
                displayMsg: '@{R=NCM.Strings;K=WEBJS_IA_12;E=js}',
                emptyMsg: '@{R=NCM.Strings;K=WEBJS_IA_13;E=js}',
                items: ['-', new Ext.form.Label({ text: '@{R=NCM.Strings;K=WEBJS_VK_300;E=js}', style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;' }), bottomPageSizeBox]

            });

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.addListener("selectionchange", UpdateTopToolBar);

            ReloadStore('');

            grid = new Ext.grid.GridPanel({
                id: 'gridControl',
                store: gridStore,
                renderTo: 'mainGrid',
                sm: selectorModel,
                xtype: grid,
                disableSelection: true,
                layout: 'fit',
                autoScroll: 'true',
                loadMask: true,
                height: 500,
                stripeRows: true,
                columns: [
                    selectorModel,
                    {
                        id: 'ConfigTitle',
                        header: '@{R=NCM.Strings;K=WEBJS_IA_04;E=js}',
                        width: 250,
                        hideable: false,
                        sortable: true,
                        dataIndex: 'ConfigTitle',
                        renderer: RenderSnippetName
                    },
                    {
                        id: 'DownloadTime',
                        header: '@{R=NCM.Strings;K=WEBJS_IA_18;E=js}',
                        sortable: true,
                        width: 150,
                        dataIndex: 'DownloadTime',
                        renderer: RenderDateTime
                    },
                    {
                        id: 'ModifiedTime',
                        header: '@{R=NCM.Strings;K=WEBJS_IA_02;E=js}',
                        sortable: true,
                        width: 150,
                        dataIndex: 'ModifiedTime',
                        renderer: RenderDateTime
                    },
                    {
                        id: 'Comments',
                        header: '@{R=NCM.Strings;K=LIBCODE_VK_76;E=js}',
                        sortable: true,
                        width: 400,
                        dataIndex: 'Comments',
                        renderer: RenderComments
                    }
                ],

                tbar:
                [
                    {
                        id: 'CreateSnippet',
                        text: '@{R=NCM.Strings;K=WEBJS_IA_05;E=js}',
                        icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_addsnippet.gif',
                        cls: 'x-btn-text-icon',
                        handler: function () {
                            eventType = 'create';
                            ShowCreateEditSnippetDialog();
                        }
                    }, '-',
                    {
                        id: 'EditSnippet',
                        text: '@{R=NCM.Strings;K=WEBJS_IA_06;E=js}',
                        icon: '/Orion/NCM/Resources/images/ConfigSnippets/icon_editsnippet.gif',
                        cls: 'x-btn-text-icon',
                        handler: function () {
                            eventType = 'edit';
                            ShowCreateEditSnippetDialog();
                        }
                    }, '-',
                    {
                        id: 'DeleteSnippet',
                        text: '@{R=NCM.Strings;K=WEBJS_IA_08;E=js}',
                        icon: '/Orion/NCM/Resources/images/icon_delete.png',
                        cls: 'x-btn-text-icon',
                        handler: function () {
                            Ext.Msg.confirm('',
                                '@{R=NCM.Strings;K=WEBJS_IA_09;E=js}',
                                function (btn) {
                                    if (btn == 'yes') {
                                        var selectedSnippets = new Array();
                                        var selectedItems = grid.getSelectionModel().selections.items;

                                        for (var i = 0; i < selectedItems.length; i++) {
                                            selectedSnippets.push(selectedItems[i].data.ConfigID)
                                        }

                                        RemoveSnippet(selectedSnippets);
                                    }
                                });
                        }
                    },
                    '->', {
                        id: 'txtSearch',
                        xtype: 'textfield',
                        emptyText: '@{R=NCM.Strings;K=WEBJS_IA_10;E=js}',
                        width: 200,
                        enableKeyEvents: true,
                        listeners: {
                            keypress: function (obj, evnt) {
                                if (evnt.keyCode == 13) {
                                    doSearch();
                                }
                            }
                        }
                    }, {
                        id: 'Clean',
                        tooltip: '@{R=NCM.Strings;K=WEBJS_VK_30;E=js}',
                        iconCls: 'ncm_clear-btn',
                        cls: 'x-btn-icon',
                        hidden: true,
                        handler: function () { doClean(); }
                    }, {
                        id: 'Search',
                        tooltip: '@{R=NCM.Strings;K=WEBJS_VK_31;E=js}',
                        iconCls: 'ncm_search-btn',
                        cls: 'x-btn-icon',
                        handler: function () { doSearch(); }
                    }
                ],
                bbar: bottomPagingToolbar
            });

            grid.getView().on("refresh", function () {
                var search = currentSearchTerm;
                if ($.trim(search).length == 0) return;

                var columnsToHighlight = SW.NCM.ColumnIndexByDataIndex(grid.getColumnModel(), 'ConfigTitle');
                Ext.each(columnsToHighlight, function (columnNumber) {
                    SW.NCM.highlightSearchText("#mainGrid .x-grid3-body .x-grid3-td-" + columnNumber, search);
                });
            });

            gridResize();
            $(window).resize(function () {
                gridResize();
                alignWindow();
            });

            $(".lined").linedtextarea({ selectedLine: -1 });
            UpdateTopToolBar();
        }
    };

} ();

Ext.onReady(SW.NCM.SnippetManagment.init, SW.NCM.SnippetManagment);
