﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NCM/NCMMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ncmHeadPlaceHolder" Runat="Server">
    <script type="text/javascript">
        function getUrlParam(param) {
            var url = window.location.search.substring(1);
            var urlParams = url.split('&');
            for (var i = 0; i < urlParams.length; i++) {
                var urlParam = urlParams[i].split('=');
                if (urlParam[0] == param) {
                    return urlParam[1];
                }
            }
        }

        var win = window.parent.Ext.getCmp('window');
        if (win) {
            var param = getUrlParam('reloadGridStore');
            if (param) {
                win.reloadGridStore = param;
            }
            win[win.closeAction]();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ncmBreadCrumbPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ncmPageTitlePlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ncmExportToPDFPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ncmCustomizePagePlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="ncmHelpIconPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="ncmMainContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="ncmOutsideFormPlaceHolder" Runat="Server">
</asp:Content>