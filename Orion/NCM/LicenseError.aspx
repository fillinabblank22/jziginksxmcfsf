<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true" CodeFile="LicenseError.aspx.cs" Inherits="Orion_NCM_LicenseError" %>
<%@ Import Namespace="SolarWinds.InformationService.Linq.Plugins.NCM.Orion.Web" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" Assembly="OrionWeb" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="menuBarPlaceholder">
	<!-- no menubar on the license error page -->
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <style type="text/css">
        #userName
        {
            display: none;
        }
        
        #licContent
        {
	        margin-top: 0px;
	        padding-top: 40px;
	        width: 100%;
	        border-top: 1px solid black;
        }

        .licTable
        {	        
	        width: 500px;
        }

        #licBackground
        {
	        border: solid #dcdbd7 2px;
	        background-color: white;
        }
        
        .licTable td
        {
            font-size: 10pt;
            font-family: Arial;
            padding: 10px;
        }
    </style>
    <div align="center" id="licContent">
        <table class="licTable" id="licBackground">
            <tr>
                <td>
                    <img src="/Orion/NCM/Resources/images/Icon.Warning.32x32.gif" />
                </td>
                <td style="font-size:13pt;font-weight:bold;text-align:left;">
                    <%=Resources.NCMWebContent.NCM_LicenseError_Heading1 %><br />
                    <%=Resources.NCMWebContent.NCM_LicenseError_Heading2 %>
                </td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align:left;" >
                    <%=String.Format(Resources.NCMWebContent.NCM_LicenseError_ErrorMessage1, LicenseNodes, ActualNodes) %>
                </td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align:left;">
                    <%=String.Format(Resources.NCMWebContent.NCM_LicenseError_ErrorMessage2, OverLicenseNodes) %>

                </td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align:left;">
                    <%=String.Concat(
                           String.Format(
                               Resources.NCMWebContent.NCM_LicenseError_ErrorMessage3, 
                               @"<a href='http://www.solarwinds.com/embedded_in_products/productLink.aspx?id=NCM_upgradelicense' style='text-decoration:underline;color:blue;'>"),
                           @"</a>.") %>
                </td>
            </tr>
            <tr>
                <td></td>
                <td align="right" style="padding:20px 10px 10px 10px;">
                    <orion:LocalizableButton runat="server" Text="<%$ Resources: NCMWebContent, NCM_LicenseError_OKButton %>" OnClick="btnOKDelete_Click"  DisplayType="Primary" Target="_blank" CssClass="sw-btn" />
                    <button type="button" class="sw-btn" onclick="javascript:alert('<%=Resources.NCMWebContent.NCM_LicenseError_UpgradeLicenseAlert%>');"><%=Resources.NCMWebContent.NCM_LicenseError_CancelButton %></button>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>