﻿using System;
using System.Web.UI;
using SolarWinds.NCMModule.Web.Resources;

public partial class Orion_NCM_NCMMasterPage : MasterPage
{
    private readonly ICommonHelper commonHelper;

    public Orion_NCM_NCMMasterPage()
    {
        commonHelper = ServiceLocator.Container.Resolve<ICommonHelper>();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!SecurityHelper.IsValidNcmAccountRole)
        {
            Server.Transfer($@"~/Orion/Error.aspx?Message={Resources.NCMWebContent.WEBDATA_VK_356}");
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        commonHelper.Initialise();
    }
}
