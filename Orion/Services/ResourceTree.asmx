﻿<%@ WebService Language="C#" Class="ResourceTree" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Linq;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Core.Models.Discovery;
using SolarWinds.Orion.Core.Models.DiscoveredObjects;
using SolarWinds.Orion.Core.Models.Interfaces;
using System.Net;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web.Discovery;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ResourceTree : WebService
{
    private const string VOLUMES_DISCOVERED_OBJECT_NAME = "Volumes";
    private const string INTERFACES_DISCOVERED_OBJECT_NAME = "Interfaces";

    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        throw ex;
    }

    public sealed class RenderedTree
    {
        public RenderedTree(string html, bool isFromCache = false, DateTime cachedTime = default(DateTime))
        {
            Html = html;
            IsFromCache = isFromCache;
            CachedTime = cachedTime;
        }

        public string Html { get; private set; }
        public DateTime CachedTime { get; private set; }
        public bool IsFromCache { get; private set; }
    }

    public class TreeNode : ITreeNode
    {
        public string ID { get; set; }
        public string Caption { get; set; }
        public bool Selected { get; set; }

        public string ObjectType { get; set; }

        public List<ITreeNode> Children { get; set; }

        public bool IsGroup { get; set; }

        /// <summary>
        /// True if node is down, unreachable or provided credentials are incorrect; otherwise false
        /// </summary>
        public bool NodeIsNotAvailable { get; set; }
        public DateTime CachedTime { get; set; }
        public bool IsFromCache { get; set; }


        public TreeNode()
        {
            this.Children = new List<ITreeNode>();
            this.ID = Guid.NewGuid().ToString();
            this.Caption = String.Empty;
            this.Selected = true;
            this.ObjectType = String.Empty;
            this.IsGroup = false;
            this.SelectionMode = SolarWinds.Orion.Core.Models.Enums.GroupSelectionMode.Many;
        }

        public SolarWinds.Orion.Core.Models.Enums.GroupSelectionMode SelectionMode
        {
            get;
            set;
        }

        public bool SelectionDisabled
        {
            get;
            set;
        }
    }

    public class SelectionMapping
    {
        public string MappingIdString { get; set; }
        public bool State { get; set; }
    }

    public class SelectionTypeInfo
    {
        public string Caption { get; set; }
        public string TypeName { get; set; }
        public string ActionType { get; set; }
    }

    public class Progress
    {
        public int DiscoveredVolumes { get; set; }
        public int DiscoveredInterfaces { get; set; }
    }

    private class AsyncTreeRenderingParams
    {
        public TreeNode TreeNode { get; set; }
        public string InfoId { get; set; }
        public System.Web.SessionState.HttpSessionState Session { get; set; }
    }

    [WebMethod(true)]
    public void UpdateSelection(string infoIdString, List<SelectionMapping> mappings)
    {
        try
        {
            Guid infoId = new Guid(infoIdString);
            ResourceTreeInfo info = ResourceTreeHelper.GetResourceTreeInfo(infoId);

            if (info == null)
            {
                log.ErrorFormat("Unable to get resource tree info with ID [{0}]", infoId);
                return;
            }

            foreach (SelectionMapping mapping in mappings)
            {
                Guid mappingId = new Guid(mapping.MappingIdString);

                if (!info.TreeMapping.ContainsKey(mappingId))
                {
                    log.ErrorFormat("Unable to find mapping with ID [{0}]", mappingId);
                    continue;
                }

                info.TreeMapping[mappingId].IsSelected = mapping.State;
            }
        }
        catch (Exception ex)
        {
            log.Error("Unhandled exception occured when updating selection for resource tree.", ex);
            throw;
        }
    }

    [WebMethod(true)]
    public string UpdateResourcesManagedState(string infoIdString, int nodeId)
    {
        Guid infoId = new Guid(infoIdString);
        ResourceTreeInfo info = ResourceTreeHelper.GetResourceTreeInfo(infoId);

        if (info == null)
        {
            log.ErrorFormat("Unable to get resource tree info with ID [{0}]", infoId);
            return "Unable to update resources";
        }

        try
        {
            using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, info.EngineId))
            {
                var licenseExceedements = proxy.ImportOneTimeJobResult(new DiscoveredObjectTreeWcfWrapper(info.resultTree), nodeId);

                if (licenseExceedements != null && licenseExceedements.Count > 0)
                {
                    return LicensingHelper.ComposeExceedementMessage(licenseExceedements,
                        Resources.CoreWebContent.LIBCODE_TP0_2); //Node resources were updated. Additional objects may not be managed due to license limit: {0}
                }
            }
        }
        catch (Exception ex)
        {
            log.Error("Unhandled exception occured when updating resources managed state.", ex);
        }

        return null;
    }

    // only one level deep but that's enough for list resources
    protected void CheckTreeConsistency(IDiscoveredObject obj)
    {
        bool allChildrenSelected = true;
        bool anyChildSelected = false;

        if (obj.Children != null)
        {
            obj.Children.ForEach((c) =>
            {
                if (!c.IsSelected)
                {
                    allChildrenSelected = false;

                    if (obj is IDiscoveredObjectGroup)
                        obj.IsSelected = false;
                }
                else
                {
                    anyChildSelected = true;
                }

                CheckTreeConsistency(c);
            });

            if (obj is IDiscoveredObjectGroup && obj.Children.Count > 0 && !obj.IsSelected)
            {
                if( allChildrenSelected )
                    obj.IsSelected = true;

                var selectionMode = obj as IDiscoveredObjectGroupWithSelectionMode;
                if( selectionMode != null && selectionMode.SelectionMode == SolarWinds.Orion.Core.Models.Enums.GroupSelectionMode.Single && anyChildSelected )
                    obj.IsSelected = true;
            }

            if (!(obj is IDiscoveredObjectGroup) && obj.Children.Count > 0 && anyChildSelected && !obj.IsSelected)
                obj.IsSelected = true;
        }
    }

    [WebMethod(true)]
    public TreeNode GetTreeInfo(string infoId)
    {
        Guid jobId = new Guid(infoId);

        ResourceTreeInfo info = ResourceTreeHelper.GetResourceTreeInfo(jobId);

        if (info == null)
            throw new NullReferenceException("Unable to find resource tree info");

        // there already is a tree in session (postback)
        if (info.resultTree != null)
        {
            return BuildTree(info.resultTree, jobId);
        }
        else
        {
            try
            {
                DiscoveredObjectTreeWcfWrapper wrapper = null;

                // Try get tree info from BL
                using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, info.EngineId))
                {
                    wrapper = businessProxy.GetOneTimeJobResult(jobId);
                }

                if (wrapper != null)
                {
                    info.resultTree = wrapper.Tree;
                    info.IsCached = wrapper.IsCached;
                    info.Created = wrapper.Created;

                    if (info.resultTree.RootObjects.Count > 0)
                    {
                        CheckTreeConsistency(info.resultTree.RootObjects.First());
                    }

                    return BuildTree(info.resultTree, jobId);
                }
                else
                {
                    // no results so far, null is expected outcome
                    return null;
                }
            }
            catch (Exception ex)
            {
                log.Error("Unhandled exception occured when getting resource tree.", ex);
                throw;
            }
        }
    }

    /// <summary>
    /// Method is responsible for creating proper html code to client. Return null if rendering isn't finished yet otherwise return html code.
    /// </summary>
    /// <param name="infoId">Unique identifier of resource tree</param>
    /// <param name="treeNode"></param>
    /// <returns>Null if rendering isn't finished otherwise html code representing ResourceTree</returns>
    [WebMethod(true)]
    public RenderedTree RenderTree(string infoId)
    {
        TreeNode treeNode = GetTreeInfo(infoId);
        if (treeNode == null)
        {
            return null;
        }

        Dictionary<string, string> renderedData = null;
        if (System.Web.HttpContext.Current.Session["treeRendering"] == null)
        {
            // Queued rendering task
            renderedData = new Dictionary<string,string>();
            // renderedData.Add(infoId, null);
            System.Web.HttpContext.Current.Session["treeRendering"] = renderedData;
        }
        else
        {
            renderedData = System.Web.HttpContext.Current.Session["treeRendering"] as Dictionary<string, string>;
            if (renderedData != null)
            {
                if (renderedData.ContainsKey(infoId))
                {
                    var myTree = ResourceTreeHelper.GetResourceTreeInfo(new Guid(infoId));

                    string strRenderedData = renderedData[infoId];
                    renderedData.Remove(infoId);
                    return new RenderedTree(strRenderedData,
                        myTree.IsCached,
                        myTree.Created);
                }
            }
        }

        // here start rendering
        renderedData.Add(infoId, null);

        System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(AsyncRenderTreeP));
        t.IsBackground = true;
        t.Start(new AsyncTreeRenderingParams { InfoId = infoId, TreeNode = treeNode, Session = System.Web.HttpContext.Current.Session });

        return null;
    }

    /// <summary>
    /// Method executed in background thread and is responsible for storing rendered ResourceTree into Session
    /// </summary>
    /// <param name="treeNode">TreeNode from which html code will be created</param>
    private void AsyncRenderTreeP(object treeNode)
    {
        AsyncTreeRenderingParams asyncRenderingParams = treeNode as AsyncTreeRenderingParams;
        if (asyncRenderingParams != null)
        {
            // var session = System.Web.HttpContext.Current.Session;
            var session = asyncRenderingParams.Session;
            if (session["treeRendering"] != null)
            {
                var renderedData = session["treeRendering"] as Dictionary<string, string>;
                if (renderedData != null)
                {
                    if (renderedData.ContainsKey(asyncRenderingParams.InfoId))
                    {
                        renderedData[asyncRenderingParams.InfoId] = RenderTreeP(asyncRenderingParams.TreeNode);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Method responsible for rendering all html elements represents passed TreeNode
    /// </summary>
    /// <param name="treeNode">Represent tree which I want to render.</param>
    /// <returns>Rendered HTML code which I want do display on client</returns>
    private string RenderTreeP(TreeNode treeNode)
    {
        SWTreeList treeList = new SWTreeList();
        treeList.HideOrphanedRadioItems = true;
        treeList.TreeNode = treeNode;
        treeList.Page = new Page();

        StringWriter stringWriter = new StringWriter();
        HtmlTextWriter htmlTextWriter = new HtmlTextWriter(stringWriter);
        treeList.RenderControl(htmlTextWriter);
        return htmlTextWriter.InnerWriter.ToString();
    }

    [WebMethod(true)]
    public Progress GetProgress(string infoId)
    {
        Guid jobId = new Guid(infoId);

        ResourceTreeInfo info = ResourceTreeHelper.GetResourceTreeInfo(jobId);

        Progress result = new Progress() { DiscoveredInterfaces = 0, DiscoveredVolumes = 0 };

        if (info == null)
            throw new NullReferenceException("Unable to find resource tree info");

        try
        {
            OrionDiscoveryJobProgressInfo progress = null;

            // Try get tree info from BL
            using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, info.EngineId))
            {
                progress = businessProxy.GetOneTimeJobProgress(jobId);
            }

            // TODO: make discovered elements counts pluggable
            if (progress != null)
            {
                if (progress.DiscoveredNetObjects.ContainsKey(VOLUMES_DISCOVERED_OBJECT_NAME))
                    result.DiscoveredVolumes = progress.DiscoveredNetObjects[VOLUMES_DISCOVERED_OBJECT_NAME];

                if (progress.DiscoveredNetObjects.ContainsKey(INTERFACES_DISCOVERED_OBJECT_NAME))
                    result.DiscoveredInterfaces = progress.DiscoveredNetObjects[INTERFACES_DISCOVERED_OBJECT_NAME];

                return result;
            }
        }
        catch (Exception ex)
        {
            log.Error("Unhandled exception occured when getting job progress.", ex);
            throw;
        }

        return result;
    }

    private TreeNode BuildTree(DiscoveredObjectTree sourceTree, Guid infoId)
    {
        ResourceTreeInfo info = ResourceTreeHelper.GetResourceTreeInfo(infoId);

        TreeNode root = new TreeNode();
        root.IsFromCache = info.IsCached;
        root.CachedTime = info.Created;
        if (sourceTree.GetAllTreeObjectsOfType<DiscoveredPoller>().Any(HasNodeDetails) == false)
        {
            root.NodeIsNotAvailable = true;
            if (!info.IsCached)
            {
                root.Caption = String.Format("alert(nodeName + '{0}'); window.location = returnUrl;",
                    Resources.CoreWebContent.WEBCODE_PCC_3);
            }
            else
            {
                root.Caption = String.Format("alert(nodeName + '{0}');", Resources.CoreWebContent.WEBCODE_PCC_3);
            }

            return root;
        }

        if (sourceTree.RootObjects.Count == 0)
            return root;

        if (sourceTree.RootObjects.Count > 1)
        {
            log.ErrorFormat("Detected more than one discovered node for job [{0}], picking first one", infoId);
        }

        AddChildNodes(sourceTree.RootObjects.First(), root, info.TreeMapping);

        return root;
    }

    private static bool HasNodeDetails(DiscoveredPoller p)
    {
        return p.PollerType.StartsWith("N.Details", StringComparison.InvariantCultureIgnoreCase);
    }

    private void AddChildNodes(IDiscoveredObject discoveredObject, TreeNode parentNode, Dictionary<Guid, IDiscoveredObject> treeMapping)
    {
        SortedDictionary<int, List<TreeNode>> children = new SortedDictionary<int, List<TreeNode>>();
        List<TreeNode> notSortedChildren = new List<TreeNode>();

        if (discoveredObject.Children != null)
        {
            foreach (IDiscoveredObject child in discoveredObject.Children)
            {
                // get icon if provided
                string icon = String.Empty;

                if (child is IDiscoveredObjectWithIcon)
                    icon = ((IDiscoveredObjectWithIcon)child).Icon;

                // we need to bind node model for ext js tree with existing IDiscoveredObject instance
                Guid bindingId = Guid.NewGuid();

				var safeDisplayName = HttpUtility.HtmlEncode(child.DisplayName);

                TreeNode newNode = new TreeNode()
                {
                    Caption = string.IsNullOrEmpty(icon) ? safeDisplayName : String.Format("<img src='{0}' />&nbsp;{1}", icon, safeDisplayName),
                    ID = bindingId.ToString(),
                    Selected = child.IsSelected,
                    IsGroup = child is IDiscoveredObjectGroup
                };

                var withSelectionMode = child as IDiscoveredObjectGroupWithSelectionMode;
                if (withSelectionMode != null)
                {
                    newNode.SelectionMode = withSelectionMode.SelectionMode;
                    newNode.SelectionDisabled = withSelectionMode.SelectionDisabled;
                }

                if (child is IDisoveredObjectSelectionTag)
                {
                    newNode.ObjectType = String.Format("{0};{1}", child.GetType().Name, ((IDisoveredObjectSelectionTag)child).SelectionTag);
                }
                else
                {
                    newNode.ObjectType = child.GetType().Name;
                }

                if (child is IDiscoveredObjectTreeOrder)
                {
                    IDiscoveredObjectTreeOrder childWithOrder = (IDiscoveredObjectTreeOrder)child;

                    // add new model for web tree
                    if (!children.ContainsKey(childWithOrder.DisplayOrder))
                        children[childWithOrder.DisplayOrder] = new List<TreeNode>();

                    children[childWithOrder.DisplayOrder].Add(newNode);
                }
                else
                {
                    notSortedChildren.Add(newNode);
                }

                // store the mapping
                treeMapping.Add(bindingId, child);

                // dig deeper
                AddChildNodes(child, newNode, treeMapping);
            }
        }

        foreach (int order in children.Keys)
            foreach (var node in children[order])
                parentNode.Children.Add(node);

        notSortedChildren.ForEach((node) => parentNode.Children.Add(node));
    }

    [WebMethod(true)]
    public List<SelectionTypeInfo> GetSelectionTypes(string infoId)
    {
        try
        {
            ResourceTreeInfo info = ResourceTreeHelper.GetResourceTreeInfo(new Guid(infoId));

            List<SelectionTypeInfo> selectionInfos = new List<SelectionTypeInfo>();

            if (info == null)
            {
                log.ErrorFormat("Unable to get resource tree info with ID [{0}]", infoId);
                return selectionInfos;
            }

            info.resultTree.SelectionTypes.ForEach((st) => selectionInfos.Add(
                new SelectionTypeInfo()
                {
                    Caption = st.DisplayName,
                    TypeName = st.TypeName,
                    ActionType = st.ActionType.ToString()
                }));

            return selectionInfos;
        }
        catch (Exception ex)
        {
            log.Error("Unhandled exception occured when getting selection types for resource tree.", ex);
            throw;
        }
    }
}

