﻿<%@ WebService Language="C#" Class="Containers" %>
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web;
using Newtonsoft.Json;
using Resources;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Shared;

/// <summary>
/// Summary description for Containers
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class Containers : WebService
{
    // unique identifier for grid expanded state in session
    private const string GridStateID = "5187b3cb-b5ca-4a74-a6a2-fe5555a201c8";
    private const string GridStateDefinitionsSessionKey = "5187b3cb-b5ca-4a74-a6a2-fe5555a201c8-Deffinitions";

    private void CheckContainerManagement()
    {
		if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
		{
			return;
		}
	
        AuthorizationChecker.AllowNodeManagement();
    }

    [WebMethod(EnableSession = true)]
	public PageableDataTable GetItemsPaged(string groupBy, string viewMode)
    {
		return GetContainersPaged(groupBy, viewMode, string.Empty);
    }

    [WebMethod]
    public PageableDataTable GetContainerForGrid(string property, string type, string value, string search)
    {
        CheckContainerManagement();

        int pageSize = 0;
        int startRowNumber = 0;
        search = (string.IsNullOrEmpty(search)) ? string.Empty : CommonHelper.FormatFilter(search);

        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        using (ContainersDAL dal = new ContainersDAL())
        {
            return dal.GetContainersForGrid(property, type, value, search, pageSize, startRowNumber, sortColumn, sortDirection);
        }
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetPagedContainersForGrid(string property, string type, string value, string search, string prefix)
    {
        CheckContainerManagement();

        int pageSize = 0;
        int startRowNumber = 0;
        search = (string.IsNullOrEmpty(search)) ? string.Empty : CommonHelper.FormatFilter(search);

        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        pageSize = Math.Max(pageSize, 20);
        using (ContainersDAL dal = new ContainersDAL())
        {
            return ExpandFromSession(dal.GetContainersForGrid(property, new ContainersMetadataDAL().GetEntityProperty(type, property)?.Type, value,
                $"%{search}%", pageSize, startRowNumber, sortColumn, sortDirection), 0, string.Empty, prefix, null);
        }
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetContainersChildren(string itemId, int level, string parentPath)
    {
        ContainersDAL dal = new ContainersDAL();

        DataTable pageData = new DataTable();
        int containerId;
        if (Int32.TryParse(itemId, out containerId))
        {
            int maxCount = ContainerHelper.GetManageGroupsMaxChildrenCount();
            int count = dal.GetContainersChildrenCount(containerId);

            pageData = dal.GetContainersChildren(containerId, maxCount);

            if (count > maxCount)
            {
                DataRow lastRow = pageData.NewRow();
                lastRow["Name"] = String.Format(Resources.CoreWebContent.WEBCODE_TM0_96, count - maxCount, maxCount);
                lastRow["ID"] = -2;
                lastRow["ObjectType"] = String.Empty;
                lastRow["Description"] = String.Empty;
                pageData.Rows.Add(lastRow);
            }
        }

        foreach (DataRow row in pageData.Rows)
        {
            row["Description"] = (row["Description"] is String) ? UIHelper.Escape((string)row["Description"]) : string.Empty;
        }

		return ExpandFromSession(new PageableDataTable(pageData, 0), level, parentPath, String.Empty, null);
    }

    private PageableDataTable GetContainersPaged(string groupBy, string viewMode, string search)
    {
        CheckContainerManagement();

        int pageSize = 0;
        int startRowNumber = 0;
        search = (string.IsNullOrEmpty(search)) ? string.Empty : CommonHelper.FormatFilter(search);

        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);


        ContainersDAL dal = new ContainersDAL();
        var cpDataTable = GetContainerCustomProperties();

        string cpList = groupBy.Equals("none") ? string.Join(",", (from DataRow row in cpDataTable.Rows select string.Format(" c.CustomProperties.{0}", row[0])))
                            : string.Join(",", (from DataRow row in cpDataTable.Rows select string.Format(" '' AS {0}", row[0])));
        PageableDataTable pageData = dal.GetItemsForGrid(pageSize, startRowNumber, sortColumn, sortDirection, groupBy, search, "root".Equals(viewMode, StringComparison.InvariantCultureIgnoreCase), cpList);
        foreach (DataRow row in pageData.DataTable.Rows)
        {
            row["Description"] = (row["Description"] is String) ? UIHelper.Escape((string)row["Description"]) : string.Empty;
        }
        ExpandFromSession(pageData, 0, String.Empty, String.Empty, viewMode);
        return pageData;
    }

    private PageableDataTable ExpandFromSession(PageableDataTable pageData, int level, string parentPath, string prefix, string viewMode)
    {
        if (!pageData.DataTable.Columns.Contains("Definition"))
        {
            pageData.DataTable.Columns.Add("Definition", typeof(string));
        }

        if (!pageData.DataTable.Columns.Contains("PlainName"))
        {
            pageData.DataTable.Columns.Add("PlainName", typeof(string));
        }

        if (!pageData.DataTable.Columns.Contains("DefinitionID"))
        {
            pageData.DataTable.Columns.Add("DefinitionID", typeof(Int32));
        }

        if (!pageData.DataTable.Columns.Contains("DefinitionName"))
        {
            pageData.DataTable.Columns.Add("DefinitionName", typeof(string));
        }

        if (!pageData.DataTable.Columns.Contains("AncestorNames"))
        {
            pageData.DataTable.Columns.Add("AncestorNames", typeof(string[]));
        }

        if (!pageData.DataTable.Columns.Contains("AncestorUrls"))
        {
            pageData.DataTable.Columns.Add("AncestorUrls", typeof(string[]));
        }

        if (!pageData.DataTable.Columns.Contains("ObjectTypeBackup"))
        {
            pageData.DataTable.Columns.Add("ObjectTypeBackup", typeof(string));
        }

        pageData.DataTable.Columns.Add("Level", typeof(int));
        pageData.DataTable.Columns.Add("Path", typeof(string));
        pageData.DataTable.Columns.Add("ParentPath", typeof(string));

        int i = 0;

        while (i < pageData.DataTable.Rows.Count)
        {
            DataRow row = pageData.DataTable.Rows[i];

            row["Level"] = level;
            row["ParentPath"] = parentPath;

            int id;

            if (!Int32.TryParse(row["ID"].ToString(), out id))
            {
                if (String.IsNullOrEmpty(parentPath))
                {
                    row["Path"] = row["ID"];
                }
                else
                {
                    row["Path"] = String.Format("{0};{1}", row["ParentPath"], row["ID"]);
                }
            }
            else
            {
                if (String.IsNullOrEmpty(parentPath))
                {
                    row["Path"] = String.Format("{0}{1}-{2}", prefix, row["Entity"], row["ID"]);
                }
                else
                {
                    row["Path"] = String.Format("{0};{1}{2}-{3}", row["ParentPath"], prefix, row["Entity"], row["ID"]);
                }
            }

            string path = (string)row["Path"];

            if (IsExpanded(path))
            {
                string itemID = String.Empty;

                if (path.IndexOf(';') == -1)
                {
                    itemID = path;
                }
                else
                {
                    itemID = path.Split(';').Last();
                }

                if (itemID.StartsWith("Orion.Groups"))
                {
                    itemID = itemID.Substring("Orion.Groups".Length + 1);
                }

				PageableDataTable children = GetChildren(itemID, level + 1, path, viewMode);                
                
                foreach (DataRow childRow in children.DataTable.Rows)
                {
                    DataRow newRow = pageData.DataTable.NewRow();

                    foreach (DataColumn dc in pageData.DataTable.Columns)
                    {
                        newRow[dc.ColumnName] = childRow[dc.ColumnName];
                    }

                    pageData.DataTable.Rows.InsertAt(newRow, ++i);
                }
            }
            
            i++;
        }
        
        return pageData;
    }

    [WebMethod]
    public PageableDataTable GetGroupsByProperty(string property, string type)
    {
        using (ContainersDAL dal = new ContainersDAL())
        {
            return dal.GetGroupsByProperty(property, type);
        }
    }
    
    [WebMethod]
    public int GetContainerNestedLevel(string memberIds, int grouId)
    {
        if (string.IsNullOrEmpty(memberIds)) throw new ArgumentNullException("memberIds");
        var members = JsonConvert.DeserializeObject<Dictionary<int, IList<int>>>(memberIds);

        if (members == null) throw new ArgumentNullException("Could not deserialize memberIds!");
        
        var nestedLevel = 0;
        ContainerHelper.GetNestedLevelUp(grouId, members, ref nestedLevel);
        return nestedLevel;
    }

    [WebMethod(EnableSession = true)]
	public PageableDataTable GetChildren(string itemId, int level, string parentPath, string viewMode)
    {
        CheckContainerManagement();

        ContainersDAL dal = new ContainersDAL();

        DataTable pageData;
        int containerId;
        if (Int32.TryParse(itemId, out containerId))
        {
			string filter = null;
			if ("flat".Equals(viewMode, StringComparison.InvariantCultureIgnoreCase)) 
			{
				filter = "md.Entity != 'Orion.Groups'";
			}
			
            int maxCount = ContainerHelper.GetManageGroupsMaxChildrenCount();
            int count = dal.GetChildrenCount(containerId);

			pageData = dal.GetChildren(containerId, maxCount, filter);

            if (count > maxCount)
            {
                DataRow lastRow = pageData.NewRow();
                lastRow["Name"] = String.Format(Resources.CoreWebContent.WEBCODE_TM0_96, count - maxCount, maxCount);
                lastRow["ID"] = -2;
                lastRow["ObjectType"] = String.Empty;
                lastRow["Description"] = String.Empty;
                pageData.Rows.Add(lastRow);
            }
        }
        else
        {
            pageData = dal.GetContainersGroupedUnderItem(itemId);
        }

        if(pageData != null)
            pageData = GetOrderDataTable(pageData);

        var resultDT = ExpandFromSession(new PageableDataTable(pageData, 0), level, parentPath, String.Empty, viewMode);
        AddCustomProperties(resultDT);
        return resultDT;
    }

    [WebMethod]
    public Dictionary<string, string> GetGroupByProperties(string entityType)
    {
        ContainersMetadataDAL dal = new ContainersMetadataDAL();
        Dictionary<string, string> properties = new Dictionary<string, string>();
        properties.Add(string.Empty, Resources.CoreWebContent.WEBCODE_AK0_70);
        foreach (EntityProperty property in dal.GetEntityProperties(entityType, ContainersMetadataDAL.PropertyType.GroupBy))
        {
            properties.Add(property.FullyQualifiedName, UIHelper.Escape(property.DisplayName));
        }
        return properties;
    }

    [WebMethod(EnableSession = true, MessageName = "GetEntityGroupsAppendErrors")]
    public ContainersMetadataDAL.GroupEntityResult GetEntityGroupsAppendErrors(string entityType, string groupByProperty, string searchValue, string[] excludeDefinitions)
    {
        return GetEntityGroupsAppendErrors(entityType, groupByProperty, searchValue, excludeDefinitions, string.Empty);
    }


    [WebMethod(EnableSession = true, MessageName = "GetEntityGroupsAppendErrorsWithLimitation")]
    public ContainersMetadataDAL.GroupEntityResult GetEntityGroupsAppendErrors(string entityType, string groupByProperty, string searchValue, string[] excludeDefinitions, string viewId)
    {
        var result = new ContainersMetadataDAL.GroupEntityResult();
        using (var swisErrorContext = new SwisErrorsContext())
        {
            result.Groups = GetEntityGroups(entityType, groupByProperty, searchValue, excludeDefinitions, viewId);

            var swisErrorMessages = swisErrorContext.GetErrorMessages();
            if (swisErrorMessages.Any())
            {
                result.IsError = true;

                var control = new System.Web.UI.UserControl();
                var swisfErrorControl = control.LoadControl("~/Orion/SwisfErrorControl.ascx") as SolarWinds.Orion.Web.BaseSwisfErrorControl;
                string swisError = BaseSwisfErrorControl.GetSwisfErrorControlHtmlString(swisErrorMessages.Select(error => new SolarWinds.Orion.Web.Federation.SwisErrorMessage(error)).ToList(), swisfErrorControl);

		result.ErrorControl = swisError;
            }
        }
        return result;
    }    


    [WebMethod(EnableSession = true, MessageName = "GetEntityGroups")]
    public IEnumerable<ContainersMetadataDAL.EntityGroup> GetEntityGroups(string entityType, string groupByProperty, string searchValue, string[] excludeDefinitions)
    {
        return GetEntityGroups(entityType, groupByProperty, searchValue, excludeDefinitions, string.Empty);
    }

    [WebMethod(EnableSession = true, MessageName = "GetEntityGroupsWithLimitation")]
    public IEnumerable<ContainersMetadataDAL.EntityGroup> GetEntityGroups(string entityType, string groupByProperty, string searchValue, string[] excludeDefinitions, string viewId)
    {
        int view;
        var viewLimitationId = 0;
        if (int.TryParse(viewId, out view))
        {
            viewLimitationId = ViewManager.GetViewById(view).LimitationID;
        }
        



        var groups = (new ContainersMetadataDAL()).GetEntityGroups(entityType, groupByProperty, searchValue, excludeDefinitions.ToList(), true, viewLimitationId);
        var maxCount = ContainerHelper.GetManageGroupsMaxObjectTreeItems();

        if (groups != null && groups.Count() > maxCount)
        {
            var remainingCount = groups.Count() - maxCount;
            var newGroups = groups.Take(maxCount);
            var entityGroup = new List<ContainersMetadataDAL.EntityGroup>()
            {
                new ContainersMetadataDAL.EntityGroup()
                    {
                        Name = (remainingCount > 1) ? String.Format(CoreWebContent.WEBCODE_VB1_20, remainingCount) : String.Format(CoreWebContent.WEBCODE_VB1_21, remainingCount)
                    }
            };
            return newGroups.Concat(entityGroup);
        }

        return groups;
    }
    
    [WebMethod]
    public void DeleteContainers(IEnumerable<Int32> containerIds)
    {
        // The deleting of container is disabled in demo mode but if user has right, he can do it
		if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
		{
			AuthorizationChecker.AllowNodeManagement();
		}
        
        ContainersDAL dal = new ContainersDAL();
        foreach (Int32 id in containerIds)
        {
            dal.DeleteContainer(id);
        }
    }

    [WebMethod]
    public PageableDataTable GetQueryResults(string definition)
    {
        CheckContainerManagement();

        int maxRecords = ContainerHelper.GetContainerPreviewMaxCount();

        //"ContainerID", "MemberPrimaryID", "DisplayName","MemberEntityType", "Status", 
        //"MemberUri", "MemberAncestorDisplayNames", "MemberAncestorDetailsUrls", "Description"
        DataTable dt = SWISVerbsHelper.InvokeV3<DataTable>(
            "Orion.ContainerMemberDefinition",
            "GetFirstNMembers",
            definition,
            maxRecords + 1);

        DataTable results = new DataTable();
        results.Columns.Add("Name", typeof(String));
        results.Columns.Add("Entity", typeof(String));
        results.Columns.Add("EntityName", typeof(String));
        results.Columns.Add("Status", typeof(Int32));
        results.Columns.Add("StatusName", typeof(String));
        results.Columns.Add("MemberPrimaryID", typeof(int));

        if (dt != null)
        {
            StatusInfo.Init(new StatusInfoProvider(), new SolarWinds.Logging.Log());
            foreach (DataRow row in dt.Rows)
            {
                string entity = row["MemberEntityType"].ToString();
                int status = Convert.ToInt32(row["Status"]);
                int memberPrimaryID = EntityHelper.GetIdFromUri((string)row["MemberPrimaryIDUri"]);
                StatusInfo info = StatusInfo.GetStatus(status);

                results.Rows.Add(FullNameHelper.GenerateFullName((string[])row["MemberAncestorDisplayNames"], (string[])row["MemberAncestorDetailsUrls"]),
                        entity,
                        EntityHelper.GetEntityName(entity),
                        status,
                        info.ShortDescription,
                        memberPrimaryID);
            }

            if (dt.Rows.Count > maxRecords)
            {
                results.Rows[results.Rows.Count - 1].Delete();
                results.Rows.Add(String.Format(Resources.CoreWebContent.WEBCODE_TM0_86, dt.Rows.Count - 1), String.Empty, String.Empty, -1, String.Empty);
            }
        }

        return new PageableDataTable(results, results.Rows.Count);
    }

    [WebMethod(EnableSession = true)]
    public void ChangeTree(string path, bool expand)
    {
        TreeStateManager tm = new TreeStateManager(this.Session, new Guid(GridStateID));

        if (expand)
        {
            tm.Expand(path);
        }
        else
        {
            tm.Collapse(path);
        }
    }

    [WebMethod(EnableSession = true)]
    public bool IsExpanded(string path)
    {
        TreeStateManager tm = new TreeStateManager(this.Session, new Guid(GridStateID));
        return tm.IsExpanded(path);
    }

    [WebMethod(EnableSession = true)]
    public void ClearTreeState()
    {
        TreeStateManager tm = new TreeStateManager(this.Session, new Guid(GridStateID));
        tm.Clear();
    }

    [WebMethod]
    public DataTable GetContainerCustomProperties()
    {
        return CustomPropertyHelper.GetEntityProperties("Orion.GroupCustomProperties");
    }
    
    private DataTable GetOrderDataTable(DataTable data)
    {
        var sortedRows = data.Select().OrderByDescending(r => (r["Entity"] as string) != null && (r["Entity"] as string).Contains("Orion.Groups")).ToList();

        DataTable dt = data.Clone();

        foreach (DataRow dr in sortedRows)
        {
            DataRow row = dt.NewRow();
            for (int i = 0; i < dr.ItemArray.Count(); i++)
                row[i] = dr[i];
            
            dt.Rows.Add(row);            
        }        
        
        return dt;
    }

    private void AddCustomProperties(PageableDataTable dataTable)
    {
        var cpDataTable = GetContainerCustomProperties();
        var cpList = (from DataRow row in cpDataTable.Rows select row[0].ToString()).ToList();
        var ids = (from DataRow row in dataTable.DataTable.Rows where row["ObjectType"].ToString().Equals("Group") select row["ID"].ToString()).ToList();

        if (cpList.Count > 0)
        {
            foreach (DataRow row in cpDataTable.Rows)
            {
                dataTable.DataTable.Columns.Add(row["ColumnName"].ToString(), Type.GetType(row["Type"].ToString()));
            }

            DataTable cpValues;
            string whereCondition = ids.Count > 0 ? string.Format("WHERE ContainerID in ({0})", string.Join(",", ids)) : string.Empty;

            using (var swis = InformationServiceProxy.CreateV3())
            {
                cpValues = swis.Query(string.Format("SELECT ContainerID, {0} FROM Orion.GroupCustomProperties {1}", string.Join(",", cpList), whereCondition));
            }
            foreach (DataRow row in dataTable.DataTable.Rows)
            {
                row["Description"] = (row["Description"] is String) ? UIHelper.Escape((string)row["Description"]) : string.Empty;
                bool isGroup;
                if ((bool.TryParse(row["IsGroup"].ToString(), out isGroup) && isGroup) || row["IsGroup"].Equals(1))
                {
                    var cpRow = cpValues.Select(string.Format("ContainerID = {0}", row["ID"])).FirstOrDefault();
                    if (cpRow != null)
                    {
                        foreach (var colName in cpList)
                        {
                            row[colName] = cpRow[colName];
                        }
                    }
                }
            }
        }
    }
}
