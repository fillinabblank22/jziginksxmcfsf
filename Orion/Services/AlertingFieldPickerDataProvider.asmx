﻿<%@ WebService Language="C#" Class="AlertingFieldPickerDataProviderWebService" %>
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using Newtonsoft.Json;
using Resources;
using SolarWinds.Orion.Core.Alerting.Plugins.Conditions.Dynamic;
using SolarWinds.Orion.Core.Alerting.Plugins.Conditions.Models;
using SolarWinds.Orion.Core.Models.Alerting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Alerts;
using SolarWinds.Orion.Web.Enums;
using SolarWinds.Orion.Web.Model;

/// <summary>
/// Summary description for AlertingFieldPickerDataProvider
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class AlertingFieldPickerDataProviderWebService : System.Web.Services.WebService
{
    public const string SessionContextKey = "AlertingFieldProviderActiveNetObjectUri";
    public const string SessionEntityKey = "AlertingFieldProviderEntity";
    public const string UpdateEventPrefix = "[updateEvent]";
    
    private readonly AlertingFieldPickerDataProvider _dataProvider;
    
    public AlertingFieldPickerDataProviderWebService()
    {
        _dataProvider = new AlertingFieldPickerDataProvider();
    }

    [WebMethod]
    public PageableDataTable GetGroups(GetGroupsRequest request)
    {
        var table = new DataTable();

        table.Columns.Add("GroupID", typeof(string));
        table.Columns.Add("GroupCaption", typeof(string));
        table.Columns.Add("NavigationPath", typeof(string));
        table.Columns.Add("IsManaged", typeof(bool));

        foreach (FieldsGroup group in _dataProvider.GetGroups(request.ObjectType))
        {
            table.Rows.Add(group.GroupID, group.DisplayName, group.NavigationPath, SwisEntityHelper.IsManagedEntity(group.GroupID));
        }

        return new PageableDataTable(table, table.Rows.Count);
    }

    [WebMethod]
    public PageableDataTable GetCategories(GetCategoriesRequest request)
    {
        var table = new DataTable();

        table.Columns.Add("CategoryID", typeof(string));
        table.Columns.Add("CategoryCaption", typeof(string));
        table.Columns.Add("CategoryTooltip", typeof(string));
        table.Columns.Add("NavigationPath", typeof(string));
        table.Columns.Add("IndentationLevel", typeof(int));
        table.Columns.Add("HasEntirePathWithSingleCardinality", typeof(bool));
        table.Columns.Add("IsLastInLevel", typeof(bool));

        if (!string.IsNullOrEmpty(request.GroupID))
        {
            if (!string.IsNullOrEmpty(request.FavoriteID) && request.FavoriteID.Equals("favorites"))
            {
                table.Rows.Add("favorites", CoreWebContent.WEBCODE_TM0_111, string.Empty, string.Empty, 0);
                table.Rows.Add("recomended", CoreWebContent.CoreMetadataReportingValues_RecommendedForReporting,
                    string.Empty, string.Empty, 0);
                table.Rows.Add("customProperties", CoreWebContent.WEBDATA_IB0_25, string.Empty, string.Empty, 0);
            }
            else
            {
                var fieldsCategories = _dataProvider.GetCategories(request.ObjectType, request.GroupID).ToList();
                for (int i = 0; i < fieldsCategories.Count; i++)
                {
                    FieldsCategory cat = fieldsCategories[i];
                    if (IsFiltered(cat, request))
                    {
                        continue;
                    }

                    var isLastInLevel = fieldsCategories.ElementAtOrDefault(i + 1) == null || fieldsCategories[i + 1].Level != cat.Level;
                    table.Rows.Add(cat.CategoryID, cat.DisplayName, string.Empty, cat.NavigationPath, cat.Level, false, isLastInLevel);
                }
            }
        }

        return new PageableDataTable(table, table.Rows.Count);
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetColumns(GetColumnsRequest request)
    {
        if (HttpContext.Current.Session != null)
        {
            HttpContext.Current.Session[SessionContextKey] = request.Uri;
            HttpContext.Current.Session[SessionEntityKey] = request.GroupID;
        }
        var table = new DataTable();
        
        table.Columns.Add("ItemID", typeof(string));
        table.Columns.Add("ItemCaption", typeof(string));
        table.Columns.Add("Field", typeof(string));
        table.Columns.Add("ItemTooltip", typeof(string));
        table.Columns.Add("NavigationPath", typeof(string));
        table.Columns.Add("HasEntirePathWithSingleCardinality", typeof(bool));
        table.Columns.Add("Description", typeof(string));
        table.Columns.Add("Units", typeof(string));
        table.Columns.Add("IsFavorite", typeof (bool));
        table.Columns.Add("PreviewValue", typeof (string));
        IEnumerable<Field> fields = Enumerable.Empty<Field>();

        if (!string.IsNullOrEmpty(request.SearchTerm))
        {
            fields = _dataProvider.SearchFields(request.ObjectType, request.SearchTerm);
        }
        else if (!string.IsNullOrEmpty(request.CategoryID) && (string.IsNullOrEmpty(request.FavoriteID) || request.FavoriteID.Equals("classic")))
        {
            fields = _dataProvider.GetFields(request.ObjectType, request.CategoryID);
        }
        else if (!string.IsNullOrEmpty(request.CategoryID) && !string.IsNullOrEmpty(request.FavoriteID))
        {
            if (request.CategoryID.Equals("recomended", StringComparison.OrdinalIgnoreCase))
                fields = _dataProvider.GetFields(request.ObjectType, string.Empty).Where(f => ((f.RefID.EntityName.Equals(request.GroupID, StringComparison.OrdinalIgnoreCase)) 
                    ||(f.RefID.EntityName.Equals(CreateUpdateEventRefId(request.GroupID), StringComparison.OrdinalIgnoreCase))) && f.DataTypeInfo.IsRecommendedForAlerting);
            else if (request.CategoryID.Equals("favorites", StringComparison.OrdinalIgnoreCase))
                fields = _dataProvider.GetFields(request.ObjectType, string.Empty).Where(f => f.DataTypeInfo.IsFavorite);
            else
                fields = _dataProvider.GetFields(request.ObjectType, string.Empty).Where(f => (!string.IsNullOrEmpty(f.RefID.NavigationPath) && f.RefID.NavigationPath.IndexOf("CustomProperties", StringComparison.OrdinalIgnoreCase) > -1) && HasCurrentGroupID(f.NavigationPathItems.Where(l => l.Name.Equals("CustomProperties", StringComparison.OrdinalIgnoreCase)).ToArray(), request.GroupID));
        }
       
        if (!string.IsNullOrEmpty(request.FieldType))
        {
            FieldType fieldType;
            Enum.TryParse(request.FieldType, out fieldType);
            fields = fields.Where(x => x.FieldType == fieldType);
        }

        foreach (Field field in SortFields(fields))
        {
            if (IsFilteredBasedOnDataType(field)) continue;
            if (IsFiltered(field, request)) continue;
            
            string fieldJson = new JavaScriptSerializer().Serialize(field);

            table.Rows.Add(field.RefID,
                field.DisplayName,
                fieldJson,
                field.OwnerDisplayName,
               string.Empty, false,
               (field.DataTypeInfo is PropertyDataDeclaration) ? ((PropertyDataDeclaration)field.DataTypeInfo).Description : string.Empty,
               (field.DataTypeInfo is PropertyDataDeclaration) ? ((PropertyDataDeclaration)field.DataTypeInfo).Units : string.Empty,
               field.DataTypeInfo.IsFavorite,
               (field.DataTypeInfo is PropertyDataDeclaration) ? ((PropertyDataDeclaration)field.DataTypeInfo).PreviewValue : string.Empty);
        }

        return new PageableDataTable(table, table.Rows.Count);
    }

    private bool HasCurrentGroupID(NavigationItemLink[] navigationPathItems, string groupID)
    {
        bool result = false;

        if (navigationPathItems != null && navigationPathItems.Length > 0)
        {
            result = navigationPathItems.All(i => i.SourceEntityName.Equals(groupID, StringComparison.OrdinalIgnoreCase));
        }

        return result;
    }
    
    [WebMethod]
    public Field GetField(string fieldRefId, AlertingFieldPickerExtraRequestParams extraRequestParams)
    {
        return _dataProvider.GetField(extraRequestParams.ObjectType, fieldRefId);
    }

    [WebMethod]
    public string[] GetFieldAutocompleteValues(string fieldRefId, string searchTerm, AlertingFieldPickerExtraRequestParams extraRequestParams)
    {
        if (string.IsNullOrEmpty(fieldRefId))
        {
            return new string[0];
        }
        
        return _dataProvider.GetFieldAutocompleteValues(extraRequestParams.ObjectType, fieldRefId, searchTerm);
    }

    [WebMethod]
    public AlertingFieldPickerDataProvider.EnumValue[] GetFieldEnumValues(string fieldRefId, AlertingFieldPickerExtraRequestParams extraRequestParams)
    {
        return _dataProvider.GetFieldEnumValues(extraRequestParams.ObjectType, fieldRefId);
    }

    public static FieldRef CreateUpdateEventRefId(string entityName)
    {
        return string.Format("{0}.{1}", UpdateEventPrefix, entityName);
    }
    
    private IEnumerable<Field> SortFields(IEnumerable<Field> fields)
    {
        var pagingParams = new PagingRequestParams(Context);

        Func<Field, object> sorter;
        switch (pagingParams.SortColumn)
        {
            case "ItemTooltip" :
                sorter = (item) => item.OwnerDisplayName;
                break;
            case "Description" :
                sorter = (item) => (item.DataTypeInfo is PropertyDataDeclaration) ? ((PropertyDataDeclaration)item.DataTypeInfo).Description : item.DisplayName;
                break;
            case "IsFavorite":
                sorter = (item) => item.DataTypeInfo.IsFavorite;
                break;
            default:
                sorter = (item) => item.DisplayName;
                break;
        }

        if (pagingParams.SortDirection == SortDataDirection.Asc)
        {
            fields = fields.OrderBy(sorter);
        }
        else
        {
            fields = fields.OrderByDescending(sorter);
        }

        return fields;
    }

    /// <summary>
    /// There are some data types that dont make sense for field picker.  This method filters them.
    /// </summary>
    /// <param name="field"></param>
    /// <returns></returns>
    private static bool IsFilteredBasedOnDataType(Field field)
    {
        if (field.DataTypeInfo != null && field.FieldType == FieldType.Property && (field.DataTypeInfo as PropertyDataDeclaration).DataType == DataType.ByteArray)
            return true;
        else
            return false;
    }

    /// <summary>
    /// Apply filter on a given field.
    /// </summary>
    /// <param name="field"></param>
    /// <param name="request"></param>
    /// <returns>Returns TRUE if field is filtered and should NOT be in a result.</returns>
    private static bool IsFiltered(Field field, AlertingFieldPickerBaseRequest request)
    {
        if (!HasFilter(request) || field.DataTypeInfo == null || field.FieldType != FieldType.Property)
        {
            // If we haven't datatype we do not aplly filtering. Likely we have custom data source (SQL/SWQL).  
            return false;
        }

        var isFiltered = false;
        PropertyDataDeclaration propDataDeclaration = field.DataTypeInfo as PropertyDataDeclaration;

        if (request.DataTypeFilter != null && propDataDeclaration.DeclType != DataDeclarationType.NotSpecified)
        {
            var dataTypesFilter = request.DataTypeFilter.Where(f => f != DataDeclarationType.NotSpecified).ToArray();
            if (dataTypesFilter.Length > 0)
            {
                // Field is filtered if we have datatype info and field is not what we want.
                isFiltered = !dataTypesFilter.Contains(propDataDeclaration.DeclType);
            }
        }

        if (!string.IsNullOrEmpty(request.ApplicationTypeFilter))
        {
            isFiltered |= propDataDeclaration.ApplicationType != request.ApplicationTypeFilter;
        }

        return isFiltered;
    }

    private bool IsFiltered(FieldsCategory category, AlertingFieldPickerBaseRequest request)
    {
        if (!HasFilter(request))
        {
            return false;
        }

        // Category is filtered if contains no fields. It means that all fields in category are filtered. 
        foreach (var categoryField in _dataProvider.GetFields(request.ObjectType, category.CategoryID))
        {
            if (!IsFiltered(categoryField, request))
            {
                return false;
            }
        }

        return true;
    }
    
    private static bool HasFilter(AlertingFieldPickerBaseRequest request)
    {
        return request.DataTypeFilter != null && request.DataTypeFilter.Length > 0
            || !string.IsNullOrEmpty(request.ApplicationTypeFilter);
    }

    public class AlertingFieldPickerExtraRequestParams
    {
        public string ObjectType { get; set; }
    }

    public class AlertingFieldPickerBaseRequest
    {
        public AlertingFieldPickerExtraRequestParams ExtraRequestParams { get; set; }

        internal string ObjectType
        {
            get { return ExtraRequestParams.ObjectType; }
        }

        public DataDeclarationType[] DataTypeFilter { get; set; }

        public string ApplicationTypeFilter { get; set; }
    }

    public class GetGroupsRequest : AlertingFieldPickerBaseRequest
    {

    }

    public class GetCategoriesRequest : AlertingFieldPickerBaseRequest
    {
        public string GroupID { get; set; }
        public string FavoriteID { get; set; }
    }

    public class GetColumnsRequest : AlertingFieldPickerBaseRequest
    {
        public string GroupID { get; set; }
        public string CategoryID { get; set; }
        public string SearchTerm { get; set; }
        public string FieldType { get; set; }
        public string FavoriteID { get; set; }
        public string Uri { get; set; }
    }

    [WebMethod]
    public bool ValidateExpressionString(string expr)
    {
        bool isValid = true;

        StringBuilder sb = new StringBuilder();
        var expression = JsonConvert.DeserializeObject<Expr>(expr);
        ValidateExpression(expression, sb);
        
        if (sb.ToString().Contains("False"))
        {
            isValid = false;
        }
        return isValid;
    }

    private void ValidateExpression(Expr expr, StringBuilder sb, int level = 0)
    {
        if (expr == null)
            return;

        switch (expr.NodeType)
        {
            case ExprType.Constant:
                {
                }
                break;

            case ExprType.Event:
            case ExprType.Field:
                {
                     if (expr.Value == null)
                     {
                         sb.Append(false);
                     }
                }
                break;

            case ExprType.Operator:
                {
                    if (level != 0)
                        sb.Append("\r\n");
                    
                    var operatorProvider = new OperatorProvider();
                    var oper = operatorProvider[expr.Value];

                    if (oper.Type == OperatorType.Binary)
                    {
                        ValidateExpression(expr[0], sb, level + 1);
                        ValidateExpression(expr[1], sb, level + 1);
                    }
                    else if (oper.Type == OperatorType.Unary)
                    {
                        ValidateExpression(expr[0], sb, level + 1);
                    }
                    else if (oper.Type == OperatorType.Nary)
                    {

                        foreach (var child in expr.Child)
                        {
                            ValidateExpression(child, sb, level + 1);
                        }
                      }
                    else
                    {
                        throw new NotImplementedException(String.Format("Unknown operator {0} type {1} ", oper.RefID, oper.Type));
                    }
                }
                break;

            default:
                throw new NotImplementedException(String.Format("Unknown node type {0}", expr.NodeType));
        }
    }
}

