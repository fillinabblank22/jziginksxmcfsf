<%@ WebService Language="C#" Class="NewAlertingService" %>
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using Resources;

using SolarWinds.Logging;
using SolarWinds.Orion.Core.Actions.DAL;
using SolarWinds.Orion.Core.Alerting;
using SolarWinds.Orion.Core.Alerting.DAL;
using SolarWinds.Orion.Core.Common.Alerting;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.Core.Models.Actions;
using SolarWinds.Orion.Common.Blacklists;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.InformationService;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class NewAlertingService : WebService {

    private static Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    [WebMethod]
    public bool AreSensitiveDataPresentInAlertConfiguration(string alertId)
    {
        var alertDal = AlertDefinitionsDAL.Create(ConditionTypeProvider.Create(AppDomainCatalog.Instance, InformationServiceProxy.CreateV3Creator()), InformationServiceProxy.CreateV3Creator());
        try
        {
            int alertIdInt32 = Convert.ToInt32(alertId);
            if (alertIdInt32 == 0)
                return false;

            return alertDal.AreSensitiveDataPresentInAlertConfiguration(Convert.ToInt32(alertId));
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }
    }

    [WebMethod]
    public AlertImportResult ImportAlert(string fileName, string alertConfigurationData, bool stripSensitiveData, string protectionPassword)
    {
        using (var proxy = _blProxyCreator.Create((ex) => log.Error(ex)))
        {
            AlertImportResult result = proxy.ImportAlertConfiguration(alertConfigurationData, HttpContext.Current.Profile.UserName, true, true, true, stripSensitiveData, protectionPassword);
            result.Name = !string.IsNullOrEmpty(result.Name)
                ? HttpUtility.HtmlEncode(result.Name.Replace(":hightlight=true", ""))
                : result.Name;
            result.MigrationMessage = !string.IsNullOrEmpty(result.MigrationMessage)
                ? HttpUtility.HtmlEncode(result.MigrationMessage)
                : result.MigrationMessage;
            if (!result.AlertId.HasValue && !string.IsNullOrEmpty(result.MigrationMessage))
            {
                log.ErrorFormat("Failed to import alert from {0}! Migration message: {1}", fileName,HttpUtility.HtmlDecode(result.MigrationMessage));
            }

            return result;
        }
    }

    [WebMethod]
    public bool DeleteAlerts(string[] ids)
    {
        var alertDal = AlertDefinitionsDAL.Create(ConditionTypeProvider.Create(AppDomainCatalog.Instance, InformationServiceProxy.CreateV3Creator()), InformationServiceProxy.CreateV3Creator());

        try
        {
            foreach (var id in ids.Where(id => Convert.ToInt32(id) != 0))
            {
                alertDal.Delete(Convert.ToInt32(id));
            }

            return true;
        }
        catch (Exception ex)
        {
            log.Error("Couldn't delete alert.", ex);
            throw;
        }
    }

    [WebMethod]
    public bool EnableDisableAlerts(string[] ids, bool enable)
    {
        if ((bool) HttpContext.Current.Profile.GetPropertyValue("AllowDisableAlert"))
        {
            var alertDal = AlertDefinitionsDAL.Create(ConditionTypeProvider.Create(AppDomainCatalog.Instance, InformationServiceProxy.CreateV3Creator()), InformationServiceProxy.CreateV3Creator());
            try
            {
                //ToDo: needs just one method
                foreach (var id in ids.Where(id => Convert.ToInt32(id) != 0))
                {
                    var alert = alertDal.Get(Convert.ToInt32(id));
                    alert.Enabled = enable;
                    alertDal.Update(alert);
                }

                return true;
            }
            catch (Exception ex)
            {
                log.Error(ex);
                throw;
            }
        }
        return false;
    }

    [WebMethod]
    public PageableDataTable GetAlerts(string property, string type, string value, string search)
    {
        int pageSize = 0;
        int startRowNumber = 0;

        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        if (pageSize == 0)
            pageSize = 20;

        string sortOrder = "Name";
        if (!string.IsNullOrEmpty(sortColumn))
        {
            sortOrder = string.Format("{0} {1}", sortColumn.Replace("CustomProperties.", string.Empty), sortDirection);
        }

        var cpDataTable = GetAlertCustomProperties();
        var cpList = (from DataRow row in cpDataTable.Rows select row[0].ToString()).ToList();

        var parameters = GetQueryConditionsParameters(property, type, value, search);
        string whereCondition = parameters.Item1;
        string timeofdayJoins = string.Empty;
        if (whereCondition.Contains("TimeOfDay"))
        {
            timeofdayJoins += @"LEFT JOIN Orion.AlertSchedules als on als.AlertConfigurationID = ac.AlertID
                                 LEFT JOIN orion.Frequencies fr on fr.FrequencyID=als.FrequencyID";
        }

        Dictionary<string, object> queryParams = parameters.Item2;

        string queryString = string.Format(@"
SELECT DISTINCT ac.AlertID, ac.Name, ac.Description, ac.ObjectType, ac.Enabled,
a.ActionTypeID, aa.EnvironmentType, aa.CategoryType, a.ActionID,a.Description as ActionDescription,a.Title, '' AS ObjectTypeName, ac.CreatedBy, 
Canned as Type, ac.Category {1}
FROM Orion.AlertConfigurations ac
LEFT JOIN Orion.ActionsAssignments aa on ac.AlertID = aa.ParentID AND (aa.EnvironmentType = 'Alerting' OR aa.EnvironmentType IS NULL) AND (aa.CategoryType = 'Trigger' OR aa.CategoryType = 'Reset')
LEFT Join Orion.Actions (nolock=true) a ON a.ActionID=aa.ActionID
{2}
Where {0}
", whereCondition, (cpList.Count > 0) ? string.Format(", ac.CustomProperties.[{0}]", string.Join("], ac.CustomProperties.[", cpList.ToArray())) : string.Empty, timeofdayJoins);

        queryString = queryString.Replace("ac.TriggerActions", "a.Title");
        queryString = queryString.Replace("ac.TimeOfDay", "fr.DisplayName");

        string countQuery = null;

        if (property.Equals("triggeractions", StringComparison.OrdinalIgnoreCase) && !string.IsNullOrEmpty(value))
            countQuery = string.Format(@"SELECT Count(AlertID) as alertsCount FROM Orion.AlertConfigurations ac
                                                  LEFT JOIN Orion.ActionsAssignments aa on ac.AlertID = aa.ParentID AND (aa.EnvironmentType = 'Alerting' OR aa.EnvironmentType IS NULL) AND aa.CategoryType = 'Trigger'
                                                  LEFT Join Orion.Actions (nolock=true) a ON a.ActionID = aa.ActionID
                                                  {1}
                                                  Where {0}", whereCondition, timeofdayJoins);
        else if (property.Equals("assignedactions", StringComparison.OrdinalIgnoreCase) && !string.IsNullOrEmpty(value))
        {
            countQuery = string.Format(@"SELECT Count(AlertID) as alertsCount FROM Orion.AlertConfigurations ac
                                                  LEFT JOIN Orion.ActionsAssignments aa on ac.AlertID = aa.ParentID AND (aa.EnvironmentType = 'Alerting' OR aa.EnvironmentType IS NULL) 
                                                  LEFT Join Orion.Actions (nolock=true) a ON a.ActionID = aa.ActionID
                                                  WHERE {0} ", whereCondition);
        }
        else if (property.Equals("type", StringComparison.OrdinalIgnoreCase) && !string.IsNullOrEmpty(value))
            countQuery = string.Format(@"SELECT Count(ac.AlertID) AS alertsCount
                              FROM Orion.AlertConfigurations ac 
                                {1}
                                {0}",
                                                string.IsNullOrEmpty(whereCondition) ? string.Empty : string.Format("WHERE {0}", whereCondition), timeofdayJoins);
        else
            countQuery = string.Format(@"SELECT Count(ac.AlertID) AS alertsCount
                              FROM Orion.AlertConfigurations ac 
                                {1}
                                {0}",
                                                string.IsNullOrEmpty(whereCondition) ? string.Empty : string.Format("WHERE {0}", whereCondition), timeofdayJoins);

        countQuery = countQuery.Replace("ac.TriggerActions", "ac.Name");
        countQuery = countQuery.Replace("ac.TimeOfDay", "fr.DisplayName");

        var frequenciesName = new FrequenciesDAL(InformationServiceProxy.CreateV3Creator()).GetAlertFrequencyDisplayNames();

        string objectTypesNamesQuery = @"
SELECT ac.ObjectType, e.DisplayName as ObjectTypeName 
FROM   (SELECT DISTINCT ObjectType FROM Orion.AlertConfigurations) ac
LEFT   JOIN Metadata.EntityMetadata EntityMetadata ON EntityMetadata.Value = ac.ObjectType AND EntityMetadata.Name='netObjectName'
JOIN   Metadata.Entity e ON e.FullName = EntityMetadata.EntityName OR (e.FullName = ac.ObjectType AND EntityMetadata.EntityName IS NULL)";

        DataTable countTable;

        var finalDataTable = new DataTable();
        finalDataTable.Columns.Add("AlertID", typeof(int));
        finalDataTable.Columns.Add("Name", typeof(string));
        finalDataTable.Columns.Add("Description", typeof(string));
        finalDataTable.Columns.Add("ObjectType", typeof(string));
        finalDataTable.Columns.Add("Enabled", typeof(bool));
        finalDataTable.Columns.Add("TriggerActions", typeof(object));
        finalDataTable.Columns.Add("ResetActions", typeof(string));
        finalDataTable.Columns.Add("TriggerActionsData", typeof(object));
        finalDataTable.Columns.Add("TriggerActionsIds", typeof(string));
        finalDataTable.Columns.Add("ObjectTypeName", typeof(string));
        finalDataTable.Columns.Add("TimeOfDay", typeof (string));
        finalDataTable.Columns.Add("CreatedBy", typeof(string));
        finalDataTable.Columns.Add("Type", typeof(bool));
        finalDataTable.Columns.Add("Category", typeof(string));
        finalDataTable.Columns.Add("ResetActionsIds", typeof(string));

        try
        {
            using (var service = InformationServiceProxy.CreateV3())
            {
                var acDataTable = service.Query(queryString, queryParams);

                foreach (var cp in cpList)
                {
                    try
                    {
                        finalDataTable.Columns.Add(cp, acDataTable.Columns[cp].DataType);
                    }
                    catch (Exception ex)
                    {
                        log.Error("Failed adding custom properties.", ex);
                    }
                }

                ILookup<string, string> objectTypesNamesLookup = service.Query(objectTypesNamesQuery).Rows.Cast<DataRow>().ToLookup(ks => ks["ObjectType"].ToString(), es => es["ObjectTypeName"].ToString());

                var result = (from row in acDataTable.AsEnumerable()
                              group row by row.Field<int>("AlertID") into grp
                              select new
                              {
                                  AlertID = grp.Key,
                                  Rows = grp.ToList()
                              }).ToDictionary(v=>v.AlertID, v=>v.Rows);

                foreach (var alert in result)
                {
                    try
                    {
                        var alertRow = alert.Value.First();

                        if (log.IsDebugEnabled)
                            log.DebugFormat("adding alert {0}", alertRow[1].ToString());

                        var triggerActions =
                            alert.Value.Where(
                                x =>
                                    x.Field<string>("EnvironmentType") == "Alerting" && x.Field<string>("CategoryType") == "Trigger" && x["ActionID"] != DBNull.Value);
                        var resetActions =
                            alert.Value.Where(
                                x =>
                                    x.Field<string>("EnvironmentType") == "Alerting" && x.Field<string>("CategoryType") == "Reset" && x["ActionID"] != DBNull.Value);
                        var resetActionsCount = resetActions.Count();
                        var triggerActionData = new List<object>();
                        foreach (var action in triggerActions)
                        {
                            try
                            {
                                triggerActionData.Add(
                                    new
                                    {
                                        Title = action.Field<string>("Title"),
                                        ActionTypeID = action.Field<string>("ActionTypeID"),
                                        Description = action.Field<string>("ActionDescription")
                                    });
                            }
                            catch (Exception ex)
                            {
                                log.Error("Error occurred while adding action", ex);
                            }
                        }
                        var triggerActionIds = string.Join(",", triggerActions.Select(x => x.Field<int>("ActionID")));
                        var resetActionsIds = string.Join(",", resetActions.Select(x => x.Field<int>("ActionID")));

                        var row = finalDataTable.NewRow();
                        row[0] = int.Parse(alertRow[0].ToString());
                        row[1] = alertRow[1].ToString();
                        row[2] = alertRow[2].ToString();

                        string objectType = alertRow[3].ToString();
                        row[3] = objectType;
                        row[4] = bool.Parse(alertRow[4].ToString());
                        row[5] = (triggerActionData.Count == 1)
                            ? triggerActionData.First()
                            : triggerActionData.Count.ToString();
                        row[6] = resetActionsCount;
                        row[7] = triggerActionData;
                        row[8] = triggerActionIds;
                        row[9] = objectTypesNamesLookup[objectType].FirstOrDefault() ?? objectType;
                        row[10] = frequenciesName.Count >= 0
                            ? frequenciesName.FirstOrDefault(x => x.Key == alert.Key).Value
                            : null;
                        row[11] = alertRow[12].ToString();
                        row[12] = bool.Parse(alertRow[13].ToString());
                        row[13] = alertRow[14].ToString();
                        row[14] = resetActionsIds;
                        foreach (var cp in cpList)
                        {
                            row[cp] = alertRow[cp];
                        }
                        finalDataTable.Rows.Add(row);
                    }
                    catch (Exception ex)
                    {
                        log.Error("Error occurred adding alert to the final table.", ex);
                    }
                }

                countTable = service.Query(countQuery, queryParams);
            }
        }
        catch (Exception ex)
        {
            log.Error("Failed to retrieve alerts.", ex);
            throw;
        }

        var dv = finalDataTable.DefaultView;
        dv.Sort = sortOrder;
        var sortedTable = dv.ToTable();
        var pageableDataTable = (sortedTable.Rows.Count == 0 || sortedTable.Rows.Count < startRowNumber) ? sortedTable : sortedTable.AsEnumerable().Skip(startRowNumber).Take(pageSize).CopyToDataTable();

        return new PageableDataTable(pageableDataTable, (countTable.Rows == null || countTable.Rows.Count == 0 ? 0 : (int)(countTable.Rows[0][0])));
    }

    [WebMethod]
    public int[] GetAlertNumber(string alertData, string property, string type, string value, string sort,
        string direction, bool isId)
    {
        var sortColumn = sort;
        var sortDirection = direction;

        var sortOrder = "Name Asc";
        if (!string.IsNullOrEmpty(sortColumn))
        {
            sortOrder = string.Format("{0} {1}", sortColumn.Replace("CustomProperties.", string.Empty), sortDirection);
        }

        var parameters = GetQueryConditionsParameters(property, type, value, string.Empty);
        var whereCondition = parameters.Item1;
        var queryParams = parameters.Item2;

        var cpDataTable = GetAlertCustomProperties();
        var cpList = (from DataRow row in cpDataTable.Rows select row[0].ToString()).ToList();

        var queryClause = string.Empty;

        if (property.Equals("triggeractions", StringComparison.OrdinalIgnoreCase))
        {
            queryClause = @"LEFT JOIN Orion.ActionsAssignments aa on ac.AlertID = aa.ParentID AND (aa.EnvironmentType = 'Alerting' OR aa.EnvironmentType IS NULL) AND aa.CategoryType = 'Trigger'
                                                                      LEFT Join Orion.Actions (nolock=true) a ON a.ActionID=aa.ActionID";
        }
        else if (property.Equals("assignedactions", StringComparison.OrdinalIgnoreCase))
        {
            queryClause = @" LEFT JOIN Orion.ActionsAssignments aa on ac.AlertID = aa.ParentID AND (aa.EnvironmentType = 'Alerting' OR aa.EnvironmentType IS NULL)
                                                                      LEFT Join Orion.Actions (nolock=true) a ON a.ActionID=aa.ActionID";
        }

        string queryString = string.Format(@"
SELECT ac.AlertID, ac.Name {2} {3}
FROM Orion.AlertConfigurations ac
{1}
Where {0}
", whereCondition, queryClause,
            string.IsNullOrEmpty(sort) || sort.Equals("Name") || sort.Equals("AlertID") || sort.Equals("TimeOfDay") || sort.Equals("Type")
                ? string.Empty
                : ",ac." + sort,
            (cpList.Count > 0)
                ? string.Format(", ac.CustomProperties.{0}", string.Join(", ac.CustomProperties.", cpList.ToArray()))
                : string.Empty);

        log.Debug("Alerts main grid query :" + queryString);
        DataTable alertDataTable;

        try
        {
            using (var service = InformationServiceProxy.CreateV3())
            {
                alertDataTable = service.Query(queryString, queryParams);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

        if (alertDataTable == null || alertDataTable.Rows == null || alertDataTable.Rows.Count == 0)
            return new[] {-1};

        if (!string.IsNullOrEmpty(sort) && sort.Equals("TimeOfDay"))
        {
            alertDataTable.Columns.Add("TimeOfDay", typeof (string));
            var frequenciesName =
                new FrequenciesDAL(InformationServiceProxy.CreateV3Creator()).GetAlertFrequencyDisplayNames();
            foreach (DataRow row in alertDataTable.Rows)
            {
                row["TimeOfDay"] = frequenciesName.Count >= 0
                    ? frequenciesName.FirstOrDefault(x => x.Key == int.Parse(row["AlertID"].ToString())).Value
                    : null;
            }
        }

        try
        {
            var dv = alertDataTable.DefaultView;
            dv.Sort = sortOrder;
            var sortedTable = dv.ToTable(true, "AlertID", "Name");
            var condition = isId ? string.Format("AlertID IN ({0})", alertData) : string.Format("Name = '{0}'", alertData);
            var rowsArray = sortedTable.Select(condition);
            return rowsArray.Select(row => sortedTable.Rows.IndexOf(row)).ToArray();
        }
        catch (Exception ex)
        {
            log.Error(ex);
            return new[] {-1};
        }
    }

    private Tuple<string, Dictionary<string, object>> GetQueryConditionsParameters(string property, string type, string value, string search)
    {
        var queryParams = new Dictionary<string, object>();
        string whereCondition = "1=1 ";

        double dValue;
        DateTime dtValue;
        if (type.Equals("real") && Double.TryParse(value, NumberStyles.Float, CultureInfo.CurrentCulture, out dValue) || Double.TryParse(value, NumberStyles.Float, CultureInfo.InvariantCulture, out dValue))
        {
            value = dValue.ToString(CultureInfo.InvariantCulture);
        }
        else if (type.Equals("datetime") && DateTime.TryParse(value, CultureInfo.CurrentCulture, DateTimeStyles.None, out dtValue) || DateTime.TryParse(value, CultureInfo.InvariantCulture, DateTimeStyles.None, out dtValue))
        {
            value = dtValue.ToString("s", CultureInfo.InvariantCulture);
        }

        if (!string.IsNullOrEmpty(property) && !string.IsNullOrEmpty(value))
        {
            if (property.Equals("triggeractions", StringComparison.OrdinalIgnoreCase) && !string.IsNullOrEmpty(value))
            {
                if (value.Equals("null", StringComparison.OrdinalIgnoreCase))
                {
                    whereCondition += "AND (a.ActionTypeID IS NULL)";
                }
                else
                {
                    whereCondition += " AND (a.ActionTypeID=@value)";
                    queryParams[@"value"] = value;
                }
            }
            else if (property.Equals("assignedactions", StringComparison.OrdinalIgnoreCase) && !string.IsNullOrEmpty(value))
            {
                whereCondition += "AND ac.AlertID IN (SELECT ParentID FROM Orion.ActionsAssignments WHERE ActionID=@value)";
                queryParams[@"value"] = value;
            }
            else if (property.Equals("type", StringComparison.OrdinalIgnoreCase) && !string.IsNullOrEmpty(value))
            {
                if (value.Equals("User-Defined", StringComparison.OrdinalIgnoreCase))
                {
                    whereCondition += "AND (ac.Canned = 0) ";
                }
                else
                {
                    whereCondition += " AND (ac.Canned = 1) ";
                }
            }
            else
            {
                if (value.Equals("null", StringComparison.OrdinalIgnoreCase) ||
                    value.Equals("{empty}", StringComparison.OrdinalIgnoreCase))
                {
                    var sqlConditionPart = type.Equals("datetime") ? string.Empty : $"{"ac." + property}='' or";
                    whereCondition += $" AND ({sqlConditionPart} {"ac." + property} IS NULL)";
                }
                else
                {
                    whereCondition += String.Format(" AND ({0}=@value)", "ac." + property);
                    queryParams[@"value"] = value;
                }
            }
        }

        //getting selected columns to search on all selected columns
        var selectedColumns = WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, "AlertManager_SelectedColumns", WebConstants.AlertDafultSelectedColumns).Split(',').ToList();
        if (!string.IsNullOrEmpty(search) && selectedColumns.Count > 0)
        {
            var searchCondition = new StringBuilder();
            var separator = string.Empty;
            for (int i = 0; i < selectedColumns.Count; i++)
            {
                if (selectedColumns[i].Equals("Type")) // no real column type for alert configuration
                    continue;

                searchCondition.AppendFormat(" {0} ac.{1} LIKE @search", separator, selectedColumns[i]);
                separator = "OR";
            }
            if (searchCondition.Length > 0)
                whereCondition += String.Format(" AND ({0})", searchCondition);

            queryParams[@"search"] = search.Replace("[", "[[]");
        }
        return new Tuple<string, Dictionary<string, object>>(whereCondition, queryParams);
    }

    [WebMethod]
    public bool EnableDisableAlert(string alertId, string value)
    {
        if ((bool) HttpContext.Current.Profile.GetPropertyValue("AllowDisableAlert"))
        {
            bool enabled;
            int id;
            if (bool.TryParse(value, out enabled) && int.TryParse(alertId, out id))
            {
                var dal =
                    AlertDefinitionsDAL.Create(
                        ConditionTypeProvider.Create(AppDomainCatalog.Instance,
                            InformationServiceProxy.CreateV3Creator()), InformationServiceProxy.CreateV3Creator());

                try
                {
                    //ToDo: needs just one method
                    var alert = dal.Get(id);
                    alert.Enabled = enabled;
                    dal.Update(alert);
                    return true;
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                    throw;
                }
            }
        }
        return false;
    }

    [WebMethod]
    public int? DuplicateAlert(string alertId, string alertName)
    {
        int id;
        if (int.TryParse(alertId, out id))
        {
            var creator = SwisConnectionProxyPool.GetCreator();
            ActionsDAL actionDal = new ActionsDAL(creator);

            var dal = AlertDefinitionsDAL.Create(ConditionTypeProvider.Create(AppDomainCatalog.Instance, InformationServiceProxy.CreateV3Creator()), InformationServiceProxy.CreateV3Creator());
            try
            {
                //ToDo: needs just one method
                var alert = dal.Get(id);
                if (string.IsNullOrEmpty(alertName))
                    alert.Name = string.Format(CoreWebContent.WEBDATA_TM0_299, alert.Name);
                else
                    alert.Name = alertName;
                if (alert.ExecutionTimePeriods != null)
                {
                    foreach (var item in alert.ExecutionTimePeriods)
                    {
                        // we should copy frequencies, not re-use
                        item.FrequencyId = 0;
                    }
                }

                foreach (var actionDefinitionTrigger in alert.TriggerActions)
                {
                    actionDefinitionTrigger.Title = actionDal.GetUniqueNameForAction(actionDefinitionTrigger.Title);
                    actionDefinitionTrigger.IsShared = false;
                    actionDefinitionTrigger.TimePeriods.ForEach(y => y.FrequencyId = 0);
                }

                foreach (var actionDefinitionReset in alert.ResetActions)
                {
                    actionDefinitionReset.Title = actionDal.GetUniqueNameForAction(actionDefinitionReset.Title);
                    actionDefinitionReset.IsShared = false;
                    actionDefinitionReset.TimePeriods.ForEach(y => y.FrequencyId = 0);
                }

                alert.CreatedBy = HttpContext.Current.Profile.UserName;
                alert.AlertRefID = Guid.NewGuid();
                alert.Enabled = false; // no need to clone and trig copy of alert
                alert.Canned = false;
                dal.Create(alert);

                // assign custom properties
                CustomPropertyHelper.AssignCpValues(new[] {alert.Uri}, alert.CustomProperties);
                // no need to update restricted values here due to we duplicate already existing values
                return alert.AlertID;
            }
            catch (Exception ex)
            {
                log.Error(ex);
                throw;
            }
        }
        return 0;
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetObjectGroupProperties()
    {
        try
        {
            const string query = @"SELECT Table, Field, DataType, MaxLength, StorageMethod, Description, TargetEntity
                             FROM Orion.CustomProperty (nolock=true)
                             WHERE TargetEntity='Orion.AlertConfigurationsCustomProperties'";

            DataTable cpDataTable;
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
                cpDataTable = service.Query(query);


            var dt = new DataTable();
            dt.Columns.Add("Value");
            dt.Columns.Add("Name");
            dt.Columns.Add("Type");

            dt.Rows.Add("", CoreWebContent.WEBCODE_AK0_70, "");
            dt.Rows.Add("ObjectType", CoreWebContent.WEBDATA_AK0_210, "objecttype");
            dt.Rows.Add("Enabled", CoreWebContent.WEBDATA_LK0_6, "");
            dt.Rows.Add("Frequency", CoreWebContent.WEBDATA_AK0_46, "");
            dt.Rows.Add("TriggerActions", CoreWebContent.WEBDATA_VM0_5, "triggeractions");
            dt.Rows.Add("CreatedBy", CoreWebContent.WEBDATA_SO0_189, "");
            dt.Rows.Add("Category", CoreWebContent.EditAccount_AlertLimitationCategory, "");
            dt.Rows.Add("Type", CoreWebContent.WEBDATA_AK0_379, "type");
            dt.Rows.Add("AssignedActions", CoreWebContent.WEBDATA_AY0_103, "assignedactions");
            foreach (DataRow row in cpDataTable.Rows)
            {
                dt.Rows.Add("CustomProperties." + row["Field"], row["Field"], row["DataType"]);
            }

            return new PageableDataTable(dt.DefaultView.ToTable(), cpDataTable.Rows.Count);
        }
        catch (Exception ex)
        {
            log.ErrorFormat("GetEntityProperties: Error during getting entity properties - {0}", ex.ToString());
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetAlertGroupValues(string property, string type)
    {
        if (string.IsNullOrEmpty(property))
            return null;
        string queryString = "";
        string countQuery = "";
        var queryParams = new Dictionary<string, object>();

        countQuery = @"SELECT  COUNT(AlertID) as Cnt FROM Orion.AlertConfigurations (nolock=true)";

        Dictionary<string, string> displayNamePluralToDisplayName = new Dictionary<string, string>();
        string swqlQuery = "SELECT FullName, DisplayName FROM Metadata.Entity";
        using(InformationServiceProxy proxy = InformationServiceProxy.CreateV3())
        {
            DataTable tblDisplayNamePluralToDisplayName = proxy.Query(swqlQuery);
            foreach(DataRow row in tblDisplayNamePluralToDisplayName.Rows)
            {
                string entityFullName = row["FullName"] != DBNull.Value ? Convert.ToString(row["FullName"]) : string.Empty;
                string displayName = row["DisplayName"] != DBNull.Value ? Convert.ToString(row["DisplayName"]) : string.Empty;
                if (!string.IsNullOrEmpty(entityFullName) && !displayNamePluralToDisplayName.ContainsKey(entityFullName))
                {
                    displayNamePluralToDisplayName.Add(entityFullName, displayName);
                }
            }
        }

        if (type.Equals("triggeractions", StringComparison.OrdinalIgnoreCase))
        {
            var plugins = SolarWinds.Orion.Web.Actions.ActionManager.Instance.GetPlugins(ActionEnviromentType.Alerting).ToList();

            var sb = new StringBuilder();
            if (plugins.Count > 0)
            {
                sb.Append("case ");
                foreach (var actionPlugin in plugins)
                {
                    sb.Append(string.Format("when t.Value = '{0}' then '{1}'", actionPlugin.ActionTypeID, actionPlugin.Title.Replace("'", "''")));
                }
                sb.Append("end");
            }
            else
            {
                sb.Append("t.Value");
            }

            queryString = string.Format(
                @"Select t.Value as Value, {0} as DisplayNamePlural, COUNT(t.AlertID) as Cnt From
(
Select Distinct ac.AlertID,a.ActionTypeID as Value
From Orion.Actions (nolock=true) a Right Join Orion.AlertConfigurations (nolock=true) ac ON ac.AlertID=a.Assignments.ParentID AND a.Assignments.EnvironmentType = 'Alerting' AND a.Assignments.CategoryType = 'Trigger'
ORDER BY a.ActionTypeID
) t
GROUP BY t.Value", sb);
        }
        else if (type.Equals("assignedactions", StringComparison.OrdinalIgnoreCase))
        {
            queryString = @"SELECT DISTINCT ActionID as Value, Title as DisplayNamePlural,(SELECT Count(a.ParentID) as Cnt FROM Orion.ActionsAssignments (nolock=true) a INNER JOIN Orion.AlertConfigurations (nolock=true) ac ON a.ParentID=ac.AlertID
                            WHERE a.ActionID=Actions.ActionID) as Cnt FROM Orion.Actions AS Actions
		                    INNER JOIN Orion.AlertConfigurations ac on ac.AlertID = Actions.Assignments.ParentID
                            WHERE Actions.Assignments.EnvironmentType = 'Alerting' AND (SELECT Count(a.ParentID) as Cnt FROM Orion.ActionsAssignments a WHERE a.ActionID=Actions.ActionID) > 1";
        }
        else if (type.Equals("type", StringComparison.OrdinalIgnoreCase))
        {
            queryString = @"SELECT 'User-Defined' as Value, NULL as DisplayNamePlural, COUNT(*) as Cnt 
            FROM Orion.AlertConfigurations (nolock=true) ac 
            Where ac.Canned = 0
       UNION ALL (
            SELECT 'Out-of-the-box' as Value, NULL as DisplayNamePlural, COUNT(*) as Cnt 
            FROM Orion.AlertConfigurations (nolock=true) ac 
            Where ac.Canned = 1)
";
        }
        else if (type.Equals("string", StringComparison.OrdinalIgnoreCase) ||
                type.Equals("nvarchar", StringComparison.OrdinalIgnoreCase) ||
                type.Equals("varchar", StringComparison.OrdinalIgnoreCase) ||
                string.IsNullOrEmpty(type) || property.Equals("Category", StringComparison.OrdinalIgnoreCase) || property.Equals("CreatedBy", StringComparison.OrdinalIgnoreCase))
        {
            queryString = string.Format(@"SELECT r.[{0}] as Value, r.[{0}] as DisplayNamePlural, COUNT(*) as Cnt
                                          FROM (
                                            SELECT (CASE WHEN b.{0} IS NULL THEN '' ELSE b.{0} END) as [{0}] 
                                            FROM Orion.AlertConfigurations (nolock=true) b
                                          ) as r
                                          GROUP BY r.[{0}]
                                          ORDER BY r.[{0}]", property);
        }
        else if (type.Equals("objecttype", StringComparison.OrdinalIgnoreCase))
        {
            queryString =
                string.Format(@"SELECT r.{0} as Value, IsNull(em.EntityName, r.{0}) as DisplayNamePlural, COUNT(*) as Cnt 
            FROM Orion.AlertConfigurations (nolock=true) r 
            LEFT JOIN Metadata.EntityMetadata em ON r.ObjectType=em.Value AND em.Name='netObjectName'
            GROUP BY r.{0} 
            ORDER BY r.{0}", property);
        }
        else
        {
            queryString = string.Format(@"SELECT r.{0} as Value, r.{0} as DisplayNamePlural, COUNT(*) as Cnt 
            FROM Orion.AlertConfigurations (nolock=true) r 
            GROUP BY r.{0} 
            ORDER BY r.{0}", property);
        }

        log.Debug("Manage Alerts main grid group query: " + queryString);
        DataTable cpDataTable;
        DataTable countTable;

        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                cpDataTable = service.Query(queryString, queryParams);
                countTable = service.Query(countQuery, queryParams);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

        DataTable dt = new DataTable();
        dt.Columns.Add("Value"); //combovalue
        dt.Columns.Add("DisplayNamePlural"); // combodisplay
        dt.Columns.Add("Cnt");

        dt.Rows.Add("[All]", CoreWebContent.WEBDATA_VM0_2, countTable.Rows[0][0]);

        foreach (DataRow row in cpDataTable.Rows)
        {
            object dispalyName = row["DisplayNamePlural"];
            object displayValue = dispalyName;

            if (dispalyName is DBNull && type.Equals("triggeractions", StringComparison.OrdinalIgnoreCase))
                displayValue = CoreWebContent.WEBCODE_YK0_31;
            else if (type.Equals("type", StringComparison.OrdinalIgnoreCase))
            {
                if ( Convert.ToInt32(row["Cnt"]) == 0)
                    continue;

                if (row["Value"].ToString().Equals("User-Defined", StringComparison.OrdinalIgnoreCase))
                    displayValue = CoreWebContent.WEBDATA_SO0_194;
                if (row["Value"].ToString().Equals("Out-of-the-box", StringComparison.OrdinalIgnoreCase))
                    displayValue = CoreWebContent.WEBDATA_SO0_195;
            }
            else
            {
                string strDisplayValue = displayValue != null && displayValue != DBNull.Value ? Convert.ToString(displayValue) : string.Empty;
                if (displayNamePluralToDisplayName.ContainsKey(strDisplayValue))
                {
                    displayValue = displayNamePluralToDisplayName[strDisplayValue];
                }
                if (string.IsNullOrEmpty(row["Value"].ToString()))
                {
                    displayValue = "[Unknown]";
                    row["Value"] = DBNull.Value;
                }
            }

            dt.Rows.Add(row["Value"], displayValue, row["Cnt"]);
        }

        dt.DefaultView.Sort = "DisplayNamePlural";
        return new PageableDataTable(dt.DefaultView.ToTable(), dt.Rows.Count);
    }

    [WebMethod]
    public DataTable GetAlertCustomProperties()
    {
        return CustomPropertyHelper.GetEntityProperties("Orion.AlertConfigurationsCustomProperties");
    }

    [WebMethod]
    public void SaveUserSetting(string name, string value)
    {
        var blacklist = new WebUserSettingsBlacklist();

        if (!AuthorizationChecker.IsAdmin && blacklist.IsSettingBlacklisted(name))
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.StatusCode = 403;
            return;
        }

        WebUserSettingsDAL.Set(name, value);
    }

    [WebMethod(EnableSession = true)]
    public void SetSessionData(string name, string value)
    {
        Session[name] = value;
    }

    [WebMethod(EnableSession = true)]
    public string GetSetting(string name)
    {
        var setting = SettingsDAL.GetSetting(name);
        if (setting != null)
            return setting.SettingValue.ToString();
        return string.Empty;
    }
}
