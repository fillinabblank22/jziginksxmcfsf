﻿<%@ WebService Language="C#" Class="AccountManagement" %>

using System;
using System.Collections;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Indications;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Principal;
using SolarWinds.Orion.Web.Model.LDAPAuthentication;
using Newtonsoft.Json;
using SolarWinds.Orion.Core.Common.Notification;
using SolarWinds.Orion.Swis.PubSub;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class AccountManagement : System.Web.Services.WebService
{

    private static SolarWinds.Logging.Log _myLog = new SolarWinds.Logging.Log();

    [WebMethod]
    public PageableDataTable GetAccountGroups()
    {
        AuthorizationChecker.AllowAdmin();

        int pageSize;
        int startRowNumber;
        int groupAccountType;

        string sortColumn = this.Context.Request.QueryString["sort"];
        string sortDirection = this.Context.Request.QueryString["dir"];

        Int32.TryParse(this.Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(this.Context.Request.QueryString["limit"], out pageSize);
        Int32.TryParse(this.Context.Request.QueryString["groupType"], out groupAccountType);

        if (pageSize < 10)
        {
            if (!Int32.TryParse(WebUserSettingsDAL.Get("AccountGroups_PageSize"), out pageSize))
                pageSize = 10;
        }

        AccountManagementDAL accountMgmtDAL = new AccountManagementDAL();
        PageableDataTable gridData = accountMgmtDAL.GetAccountGroups(sortColumn, sortDirection, startRowNumber, pageSize, (GroupAccountType)groupAccountType);
        return gridData;

    }

    [WebMethod]
    public PageableDataTable GetAccounts(string accountId)
    {
        AuthorizationChecker.AllowAdmin();

        int pageSize;
        int startRowNumber;

        string sortColumn = this.Context.Request.QueryString["sort"];
        string sortDirection = this.Context.Request.QueryString["dir"];

        Int32.TryParse(this.Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(this.Context.Request.QueryString["limit"], out pageSize);

        if (pageSize < 10)
        {
            if (!Int32.TryParse(WebUserSettingsDAL.Get("Accounts_PageSize"), out pageSize))
                pageSize = 10;
        }

        AccountManagementDAL accountMgmtDAL = new AccountManagementDAL();
        PageableDataTable gridData = accountMgmtDAL.GetAccounts(accountId, sortColumn, sortDirection, startRowNumber, pageSize);
        return gridData;
    }

    private static void DeleteNotificationSend(string subjectAccountId)
    {
        var notification = new AccountNotification(IndicationHelper.GetIndicationType(IndicationType.System_InstanceDeleted), subjectAccountId);
        PublisherClient.Instance.Publish(notification);
    }

    [WebMethod]
    public void DeleteAccount(IEnumerable<string> accountIDs)
    {
        AuthorizationChecker.AllowAdmin();
        AccountManagementDAL accountMgmtDAL = new AccountManagementDAL();
        accountMgmtDAL.DeleteAccounts(accountIDs, DeleteNotificationSend);
    }

    [WebMethod]
    public void DeleteGroupAccount(IEnumerable<string> accountIDs, int groupAccountType)
    {
        AuthorizationChecker.AllowAdmin();
        AccountManagementDAL accountMgmtDAL = new AccountManagementDAL();
        accountMgmtDAL.DeleteAccountGroups(accountIDs, (ids) => ids.ToList().ForEach(DeleteNotificationSend), (GroupAccountType)groupAccountType);
    }

    [WebMethod(EnableSession = true)]
    public string EditGroupAccount(List<string> accountIDs)
    {
        AuthorizationChecker.AllowAdmin();

        try
        {
            Context.Session["AccountType"] = "group"; // this query string allows AccountWorkflowManager to define correct return address
            AccountWorkflowManager.CurrentStep = AccountWorkflowManager.AddAccountStep.SelectAccounts;
            AccountWorkflowManager.Accounts = accountIDs;
            string result = AccountWorkflowManager.Next(AccountWorkflowManager.AddAccountStep.SetProperties);
            result = result.TrimStart('~');
            Context.Session.Remove("AccountType");
            return result;
        }
        catch (Exception ex)
        {
            Context.Session.Remove("AccountType");
            _myLog.ErrorFormat("EditGroupAccounts failed.  Details: {0}", ex);
            return null;
        }
    }

    [WebMethod(EnableSession = true)]
    public string EditWindowsAccount(List<string> accountIDs)
    {
        AuthorizationChecker.AllowAdmin();

        try
        {
            AccountWorkflowManager.CurrentStep = AccountWorkflowManager.AddAccountStep.SelectAccounts;
            AccountWorkflowManager.Accounts = accountIDs;
            string result = AccountWorkflowManager.Next(AccountWorkflowManager.AddAccountStep.SetProperties);
            result = result.TrimStart('~');
            return result;
        }
        catch (Exception ex)
        {
            _myLog.ErrorFormat("EditWindowsAccounts failed.  Details: {0}", ex);
            return null;
        }
    }


    [WebMethod]
    public bool MoveUpGroupAccount(IEnumerable<string> accountIDs, int groupAccountType)
    {

        AuthorizationChecker.AllowAdmin();

        AccountManagementDAL accountMgmtDAL = new AccountManagementDAL();
        return accountMgmtDAL.MoveUpGroupAccount(accountIDs, (GroupAccountType)groupAccountType);
    }

    [WebMethod]
    public bool MoveDownGroupAccount(IEnumerable<string> accountIDs, int groupAccountType)
    {
        AuthorizationChecker.AllowAdmin();

        AccountManagementDAL accountMgmtDAL = new AccountManagementDAL();
        return accountMgmtDAL.MoveDownGroupAccount(accountIDs, (GroupAccountType)groupAccountType);
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetWorkflowManagerAccounts()
    {

        try
        {
            //get the list of account from the workflow manager and build a table 
            List<string> AccountList = new List<string>();
            AccountList = AccountWorkflowManager.Accounts;
            //todo uncomment above comment out test data below

            //AccountList.Add("Steve");
            //AccountList.Add("Steve2");
            //AccountList.Add("Steve3");
            //AccountList.Add("Steve4");
            //AccountList.Add("Steve5");
            //AccountList.Add("Steve6");
            //AccountList.Add("Steve7");
            //AccountList.Add("Steve8");

            //if (numberOfRecords == 1)
            //{ numberOfRecs.Text = numberOfRecords.ToString().Trim() + " account "; }
            //else
            //{ numberOfRecs.Text = numberOfRecords.ToString().Trim() + " accounts "; }
            DataTable tmpTable = new DataTable();
            DataColumn dc = new DataColumn("account");
            tmpTable.Columns.Add(dc);
            DataColumn dc1 = new DataColumn("delete");
            tmpTable.Columns.Add(dc1);

            int numberOfRecords = 0;
            if (AccountList != null)
            {
                numberOfRecords = AccountList.Count;

                foreach (string accountID in AccountList)
                {
                    DataRow tmpRow = tmpTable.NewRow();
                    tmpRow["account"] = accountID;
                    tmpRow["delete"] = 0;
                    tmpTable.Rows.Add(tmpRow);
                }
            }

            PageableDataTable gridData = new PageableDataTable(tmpTable, numberOfRecords);
            return gridData;
        }
        catch (Exception ex)
        {
            _myLog.ErrorFormat("GetWorkFlow accounts failed.  Details: {0}", ex);
        }
        return null;

    }

    [WebMethod(EnableSession = true)]
    public void WorkflowManagerDeleteAccount(String accountID)
    {
        AuthorizationChecker.AllowAdmin();

        try
        {
            bool success = AccountWorkflowManager.Accounts != null ? AccountWorkflowManager.Accounts.Remove(accountID) : false;
        }
        catch (Exception ex)
        {
            _myLog.ErrorFormat("WorkflowManagerDeleteAccount failed.  Details: {0}", ex);
        }
    }


    [WebMethod(EnableSession = true)]
    public int WorkflowManagerAddAccounts(string[] accountIds)
    {
        AuthorizationChecker.AllowAdmin();

        if (accountIds == null) return 0;
        int addedCount = 0;
        try
        {
            if (AccountWorkflowManager.Accounts == null)
            {
                AccountWorkflowManager.Accounts = new List<string>(accountIds);
                return accountIds.Length;
            }

            foreach (String accountId in accountIds)
            {
                if (AccountWorkflowManager.Accounts.Contains(accountId))
                    continue;

                AccountWorkflowManager.Accounts.Add(accountId);
                addedCount++;
            }
            return addedCount;
        }
        catch (Exception ex)
        {
            _myLog.ErrorFormat("WorkflowManagerAddAccounts failed.  Details: {0}", ex);
            return addedCount;
        }
    }


    [WebMethod(EnableSession = true)]
    public void WorkflowManagerAddDeleteAccounts(string[] addAccountIds, string[] deleteAccountIds)
    {
        AuthorizationChecker.AllowAdmin();

        try
        {

            // add (list of) accounts
            if (addAccountIds != null)
            {
                WorkflowManagerAddAccounts(addAccountIds);
            }

            // delete accounts (loop)
            if (deleteAccountIds != null)
            {
                foreach (String accountId in deleteAccountIds)
                {
                    WorkflowManagerDeleteAccount(accountId);
                }
            }
        }

        catch (Exception e)
        {
            string err = e.Message;
            _myLog.ErrorFormat("Error occurred updating AccountList control.  Details: {0}", err);
        }
        return;

    }


    [WebMethod(EnableSession = true)]
    public PageableDataTable GetWindowsAccounts(string searchstring, string username, string password, string accounttype)
    {
        AuthorizationChecker.AllowAdmin();

        // get sort direction from the querystring
        string sortDirection = this.Context.Request.QueryString["dir"];

        DataTable tmpTable = new DataTable();
        DataColumn dc = new DataColumn("account");
        tmpTable.Columns.Add(dc);

        try
        {

            List<AccountInfo> accountList;
            string retMsgText = "";  //reset to default

            // go get the accounts             
            accountList = AccountSearchHelper.SearchFor(searchstring, username, password, accounttype, sortDirection);

            int numberOfRecords = 0;
            if (accountList != null)
            {
                numberOfRecords = accountList.Count;
            }

            // convert the accountInfo list into a pageable datatable for the Ext grid store
            foreach (AccountInfo accountInfo in accountList)
            {
                // check if we've reached 250 rows and if so, then jump out of loop (to limit results to 250 max).
                if (tmpTable.Rows.Count == 250)
                {
                    _myLog.Warn("Search returned more than 250 results.  LDAP search on AD domain is limited to 250 results.");
                    retMsgText = Resources.CoreWebContent.WEBCODE_AK0_147;
                    break;
                }

                // add a row to the data table
                DataRow tmpRow = tmpTable.NewRow();
                tmpRow["account"] = accountInfo.NetBiosUserName;
                tmpTable.Rows.Add(tmpRow);
            }


            PageableDataTable gridData = new PageableDataTable(tmpTable, numberOfRecords, retMsgText, "warning", "search");
            return gridData;
        }


        // catch user defined DomainSearchException exceptions    
        catch (DomainSearchException dsEx)
        {
            PageableDataTable gridData = new PageableDataTable(tmpTable, 1, dsEx.LocalizedMessage ?? dsEx.Message, dsEx.MessagePriority, dsEx.MessageType);
            return gridData;
        }


        // catch COM exceptions    
        catch (COMException comException)
        {
            string errMsg = GetFriendlyComErrorMessage(accounttype, comException);

            _myLog.ErrorFormat("Search string entered is invalid: '{0}'. Details:\r\n{1}", errMsg, comException);

            PageableDataTable gridData = new PageableDataTable(tmpTable, 1, errMsg, "error", "search");

            return gridData;
        }

        // catch other general exceptions
        catch (Exception e)
        {
            _myLog.ErrorFormat("Could not find account details on the specified domain.  Details:\r\n{0}", e);

            PageableDataTable gridData = new PageableDataTable(tmpTable, 1, e.Message, "error", "credentials");
            return gridData;
        }

    }

    private static string GetFriendlyComErrorMessage(string accountType, COMException exception)
    {
        string errMsg;

        string accType = accountType.ToLower();


        // refactor error messages based on the error code
        switch (exception.ErrorCode)
        {
            case -2147016646: // "The server is not operational".  This is caused when you enter an invalid domain.
                errMsg = Resources.CoreWebContent.WEBCODE_AK0_146;
                break;

            case -2147463168:
            // "Unknown error (0x80005000)".  This is caused when you enter search string with no domain e.g. '\blah'
            default:
                errMsg = string.Format(Resources.CoreWebContent.WEBCODE_AK0_143,
                    ((accType == "group")
                        ? Resources.CoreWebContent.WEBCODE_AK0_144
                        : Resources.CoreWebContent.WEBCODE_AK0_145));
                break;
        }
        return errMsg;
    }

    [WebMethod(EnableSession = true)]
    public object[] TestProcessAuthentication(string accounttype)
    {
        if (string.IsNullOrWhiteSpace(accounttype))
            throw new ArgumentNullException("accounttype");

        AuthorizationChecker.AllowAdmin();

        try
        {
            _myLog.DebugFormat("Testing connection to AD or Local with current Identity for account type '{0}'.", accounttype);

            TrySearchResult result = AccountSearchHelper.TrySearchWithCurrentIdentity(accounttype);

            string principalName = WindowsIdentity.GetCurrent().Name;
            _myLog.DebugFormat("Tested Domain authentication for current identity '{1}' with following resuts. {0}", result, principalName);

            AccountName name = AccountSearchHelper.ParseAccountName(principalName);

            var messages = new ArrayList();

            AddADDomainInfo(result, messages, name);
            AddSearchResultsInfo(result, messages, name);

            return messages.ToArray();
        }
        // catch user defined DomainSearchException exceptions    
        catch (DomainSearchException dsEx)
        {
            return new object[]
            {
                new
                {
                    MessageType = dsEx.MessagePriority,
                    Message = dsEx.LocalizedMessage ?? dsEx.Message
                }
            };
        }
        catch (COMException comException)
        {
            string errMsg = GetFriendlyComErrorMessage(accounttype, comException);

            _myLog.ErrorFormat("Search string entered is invalid: '{0}'. Details:\r\n{1}", errMsg, comException);

            return new object[]
            {
                new
                {
                    MessageType = "error",
                    Message = errMsg
                }
            };
        }
        catch (Exception e)
        {
            _myLog.ErrorFormat("Could not find account details on the specified domain. Details:\r\n{0}", e);

            return new object[]
            {
                new
                {
                    MessageType = "error",
                    Message = e.Message
                }
            };
        }
    }

    [WebMethod(EnableSession = true)]
    public object TestLDAPServerConnection(string ldapServerSettigs, string userName, string password)
    {
        var ldapAuthSettings = JsonConvert.DeserializeObject<LDAPAuthSettings>(ldapServerSettigs);

        using (var auth = new LdapAuthentication(ldapAuthSettings))
        {
            try
            {
                return new
                {
                    result = auth.TryConnect(userName, password, true)
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    result = false,
                    message = ex.Message
                };
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetDomainDN(string ldapServer)
    {
        var domainDn = string.Empty;
        using (var auth = new LdapAuthentication(new LDAPAuthSettings { Host = ldapServer }))
        {
            try
            {
                domainDn = auth.GetDomainDN(ldapServer);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        return domainDn;
    }

    private static void AddSearchResultsInfo(TrySearchResult result, ArrayList messages, AccountName name)
    {
        if (result.IsSuccessfull)
        {
            string message = result.IsJoinedToADDomain
                ? string.Format(Resources.CoreWebContent.WEDDATA_VK0_06,
                    name.GetFullName(), result.ADDomainName)
                : string.Format(Resources.CoreWebContent.WEDDATA_VK0_07,
                    name.GetFullName());

            messages.Add(new
            {
                MessageType = "success",
                Message = message
            });
        }
        else
        {
            string message = result.IsJoinedToADDomain
                ? string.Format(Resources.CoreWebContent.WEDDATA_VK0_08,
                    name.GetFullName(), result.ADDomainName)
                : string.Format(Resources.CoreWebContent.WEDDATA_VK0_09,
                    name.GetFullName());

            messages.Add(new
            {
                MessageType = "warning",
                Message = message
            });
        }
    }

    private static void AddADDomainInfo(TrySearchResult result, ArrayList messages, AccountName name)
    {
        if (result.IsJoinedToADDomain)
        {
            string message = string.Format(Resources.CoreWebContent.WEDDATA_VK0_10,
                Environment.MachineName, result.ADDomainName);

            messages.Add(new
            {
                MessageType = "success",
                Message = message
            });

            return;
        }

        if (result.IsADAuthFailure)
        {
            string message = string.Format(Resources.CoreWebContent.WEDDATA_VK0_11,
                Environment.MachineName, name.GetFullName());

            messages.Add(new
            {
                MessageType = "warning",
                Message = message
            });
        }
        else if (result.IsADUnknownError)
        {
            string message = string.Format(Resources.CoreWebContent.WEDDATA_VK0_12,
                Environment.MachineName);

            messages.Add(new
            {
                MessageType = "error",
                Message = message
            });
        }
        else
        {
            string message = string.Format(Resources.CoreWebContent.WEDDATA_VK0_13,
                Environment.MachineName);

            messages.Add(new
            {
                MessageType = "success",
                Message = message
            });
        }
    }
}

