﻿<%@ WebService Language="C#" Class="NotificationPanel" %>

using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class NotificationPanel : WebService
{
	[WebMethod]
    public void AcknowledgeNotificationItemsByType(string type, string createdBefore)
	{
        NotificationItemsManager.DismissMessageByType(HttpContext.Current.Profile.UserName, new Guid(type), DateTime.Parse(createdBefore));
	}

    [WebMethod]
    public void AcknowledgeNotificationItemById(string notificationId, string createdBefore)
    {
        NotificationItemsManager.DismissMessageById(HttpContext.Current.Profile.UserName, new Guid(notificationId), DateTime.Parse(createdBefore));
    }

    [WebMethod]
    public void AcknowledgeAllNotificationItems(string createdBefore)
    {
        NotificationItemsManager.DismissAllMessages(HttpContext.Current.Profile.UserName, DateTime.Parse(createdBefore));
    }

    [WebMethod]
    public IEnumerable<NotificationMessage> GetNotifications()
    {
        var roles = GetUserRoles();
        return NotificationItemsManager.GetNotificationMessagesV2(roles);
    }

    private ICollection<NotificationItemType.Roles> GetUserRoles()
    {
        var profile = HttpContext.Current.Profile;
        var roles = new List<NotificationItemType.Roles>();

        if (Convert.ToBoolean(profile.GetPropertyValue("AllowAdmin")))
            roles.Add(NotificationItemType.Roles.Administrator);

        if (Convert.ToBoolean(profile.GetPropertyValue("AllowNodeManagement")))
            roles.Add(NotificationItemType.Roles.NodeManagement);

        if (Convert.ToBoolean(profile.GetPropertyValue("AllowCustomize")))
            roles.Add(NotificationItemType.Roles.Customization);

        if (Convert.ToBoolean(profile.GetPropertyValue("AllowEventClear")))
            roles.Add(NotificationItemType.Roles.EventClear);

        if (Convert.ToBoolean(profile.GetPropertyValue("AllowReportManagement")))
            roles.Add(NotificationItemType.Roles.ManageReports);

        return roles;
    }
}