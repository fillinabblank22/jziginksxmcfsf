<%@ WebService Language="C#" Class="WebAdmin" %>
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.Common.Blacklists;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.Repository;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class WebAdmin : WebService
{
    private static readonly Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
    private static Dictionary<String, String> entities;

    private static void BLExceptionHandler(Exception ex)
    {
        log.Error(ex);
        throw ex;
    }

    private static Dictionary<String, String> AvailableEntities
    {
        get
        {
            if (entities == null)
            {
                ContainersMetadataDAL dal = new ContainersMetadataDAL();
                entities = dal.GetAvailableContainerMemberEntities(true);
            }

            return entities;
        }
    }

    [WebMethod(EnableSession = true)]
    public ExternalWebsite[] GetExternalWebsites()
    {
        AuthorizationChecker.AllowAdmin();
        using (var proxy = _blProxyCreator.Create(BLExceptionHandler))
        {
            return proxy.GetExternalWebsites().ToArray();
        }
    }

    [WebMethod(EnableSession = true)]
    public void DeleteExternalWebsite(int id)
    {
        AuthorizationChecker.AllowAdmin();
        using (var proxy = _blProxyCreator.Create(BLExceptionHandler))
        {
            proxy.DeleteExternalWebsite(id);
            proxy.DeleteWebMenuItemByLink(GetLinkForID(id));
        }
    }

    private static string GetLinkForID(int id)
    {
        return string.Format("/Orion/External.aspx?Site=" + id);
    }

    private static string Truncate(string source, int maxLength)
    {
        return source.Substring(0, Math.Min(source.Length, maxLength));
    }

    private static string GetMenuItemDescriptionForSite(ExternalWebsite site)
    {
        return string.IsNullOrEmpty(site.FullTitle) ? string.Format(Resources.CoreWebContent.WEBCODE_AK0_89, site.ShortTitle) : site.FullTitle;
    }

    [WebMethod(EnableSession = true)]
    public int CreateExternalWebsite(ExternalWebsite site, string menuBar)
    {
        AuthorizationChecker.AllowAdmin();
        using (var proxy = _blProxyCreator.Create(BLExceptionHandler))
        {
            // Database structure is limited [ShortTitle(50)] [Fultitle(500)] [URL(500)] in externalWebsites table
            // But an External web referece is added into MenuItems table that is limited evem more to [Title(200)] [Description(255)] [Link(255)]
            // Link is not the same as URL it is reference to link for ID
            site.ShortTitle = Truncate(site.ShortTitle, 50);
            site.FullTitle = Truncate(site.FullTitle, 255);
            site.URL = Truncate(site.URL, 500);

            int id = proxy.CreateExternalWebsite(site);
            WebMenuItem item = new WebMenuItem();
            item.Title = Truncate(site.ShortTitle, 50);
            item.Description = Truncate(GetMenuItemDescriptionForSite(site), 255);
            item.Link = GetLinkForID(id);
            item.NewWindow = false;
            proxy.AddNewWebMenuItemToMenubar(item, menuBar);
            return id;
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetMenuBarByExternalWebsite(int siteId)
    {
        return MenuBar.GetMenuBarByExternalWebSite(siteId);
    }

    [WebMethod(EnableSession = true)]
    public void UpdateExternalWebsite(ExternalWebsite site, string menuBar)
    {
        AuthorizationChecker.AllowAdmin();
        using (var proxy = _blProxyCreator.Create(BLExceptionHandler))
        {
            // Database structure is limited [ShortTitle(50)] [Fultitle(500)] [URL(500)] in externalWebsites table
            // But an External web referece is added into MenuItems table that is limited evem more to [Title(200)] [Description(255)] [Link(255)]
            // Link is not the same as URL it is reference to link for ID
            site.ShortTitle = Truncate(site.ShortTitle, 50);
            site.FullTitle = Truncate(site.FullTitle, 255);
            site.URL = Truncate(site.URL, 500);

            proxy.UpdateExternalWebsite(site);
            proxy.RenameWebMenuItemByLink(site.ShortTitle, GetMenuItemDescriptionForSite(site), menuBar, GetLinkForID(site.ID));
        }
    }

    [WebMethod(EnableSession = true)]
    public int EnableAdvancedAlert(string alertDefId, bool enable)
    {
        using (var proxy = _blProxyCreator.Create(BLExceptionHandler))
        {
            try
            {
                return proxy.EnableAdvancedAlert(new Guid(alertDefId), enable);
            }
            catch (Exception ex)
            {
                log.ErrorFormat("EnableAdvancedAlert: Error during enabling advanced alert - {0}", ex.ToString());
                return -1;
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetDateTimeLimits(string periodName)
    {
        DateTime fromDate = new DateTime();
        DateTime toDate = new DateTime();
        Periods.Parse(ref periodName, ref fromDate, ref toDate);

        return string.Format("{0}~{1}", fromDate.ToUniversalTime().ToString("s"), toDate.ToUniversalTime().ToString("s"));
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetGridObjects(string property, string type, string value, string search)
    {
        int pageSize = 0;
        int startRowNumber = 0;

        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];
        search = (string.IsNullOrEmpty(search)) ? string.Empty : CommonHelper.FormatFilter(search);

        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        startRowNumber = Math.Max(startRowNumber + 1, 1);
        pageSize = Math.Max(pageSize, 20);
        value = value.Equals(EntityPropertyHelper.GetPropertyUnknownValue()) ? "" : value;

        using (var swisErrorsContext = new SwisErrorsContext())
        {
            var entitiesForGrid = new ContainersMetadataDAL().GetEntitiesForGrid(type, property, value, sortColumn, sortDirection,
                startRowNumber, startRowNumber + pageSize - 1, search);
            var entitiesCount = new ContainersMetadataDAL().GetEntitiesCountForGrid(type, property, value, search);

            var errorMessages = swisErrorsContext.GetErrorMessages();
            string swisError = string.Empty;
            if (errorMessages != null && errorMessages.Any())
            {
                var control = new System.Web.UI.UserControl();
                var swisfErrorControl =
                    control.LoadControl("~/Orion/SwisfErrorControl.ascx") as BaseSwisfErrorControl;
                swisError = BaseSwisfErrorControl.GetSwisfErrorControlHtmlString(
                    errorMessages.Select(
                        error => new SolarWinds.Orion.Web.Federation.SwisErrorMessage(error)).ToList(),
                    swisfErrorControl);
            }

            return new PageableDataTable(entitiesForGrid, entitiesCount)
            {
                Metadata =
                    new
                    {
                        ErrorHtml = swisError
                    }
            };

        }
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetObjectPropertyValues(string property, string type)
    {
        try
        {
            ContainersMetadataDAL cmdal = new ContainersMetadataDAL();
            Dictionary<string, int> items = new Dictionary<string, int>(cmdal.GetEntityGroupsCount(type, property));

            DataTable dt = new DataTable();
            dt.Columns.Add("Value");
            dt.Columns.Add("Cnt");

            foreach (KeyValuePair<string, int> item in items)
                dt.Rows.Add(item.Key, item.Value);

            return new PageableDataTable(dt, items.Count);
        }
        catch (Exception ex)
        {
            log.WarnFormat("GetObjectPropertyValues: Error during getting entity property values - {0}", ex.ToString());
            return null;
        }
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetObjectPropertyValuesCpe(string property, string type)
    {
        try
        {
            ContainersMetadataDAL cmdal = new ContainersMetadataDAL();
            var dt = cmdal.GetEntityGroupsCountCpe(type, property);
            return new PageableDataTable(dt, dt.Rows.Count);
        }
        catch (Exception ex)
        {
            log.WarnFormat("GetObjectPropertyValuesCpe: Error during getting entity property values - {0}", ex.ToString());
            return null;
        }
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetAvailableObjectTypes()
    {
        try
        {
            ContainersMetadataDAL cmdal = new ContainersMetadataDAL();
            Dictionary<string, string> items = cmdal.GetAvailableContainerMemberEntities(true);
            DataTable dt = new DataTable();
            dt.Columns.Add("Key");
            dt.Columns.Add("Value");

            foreach (KeyValuePair<string, string> item in items)
            {
                // block net objects if its module "resourceHost" attribute isn't defined within the .config files
                string detailsView = NetObjectFactory.GetDetailsViewNameBySWISType(item.Key);
                if (string.IsNullOrEmpty(detailsView))
                    continue;
                if (string.IsNullOrEmpty(ViewManager.GetResourceHostByViewType(detailsView)))
                    continue;

                dt.Rows.Add(item.Key, item.Value);
            }

            return new PageableDataTable(dt, dt.Rows.Count);
        }
        catch (Exception ex)
        {
            log.ErrorFormat("GetAvailableObjectTypes: Error during available objects - {0}", ex.ToString());
            return null;
        }
    }

    private void SetViewInfoByResourceId(string resourceId)
    {
        int resId;
        if (int.TryParse(resourceId, out resId))
        {
            var resource = ResourceManager.GetResourceByID(resId);
            if (resource != null)
                if (resource.View != null)
                    HttpContext.Current.Items[resource.View.GetType().Name] = resource.View;
        }
    }

    [WebMethod(EnableSession = true)]
    public int GetViewLimitationID(string resourceId)
    {
        int resId;
        if (int.TryParse(resourceId, out resId))
        {
            var resource = ResourceManager.GetResourceByID(resId);
            if (resource != null)
                if (resource.View != null)
                    return resource.View.LimitationID;
        }
        return 0;
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetEntityProperties(string property)
    {
        try
        {
            ContainersMetadataDAL cmdal = new ContainersMetadataDAL();
            List<EntityProperty> items  = new List<EntityProperty>(cmdal.GetEntityProperties(property, ContainersMetadataDAL.PropertyType.GroupBy));

            DataTable dt = new DataTable();
            dt.Columns.Add("Key");
            dt.Columns.Add("Value");

            foreach (EntityProperty item in items)
            {
                dt.Rows.Add(item.FullyQualifiedName, item.DisplayName);
            }

            return new PageableDataTable(dt, items.Count);
        }
        catch (Exception ex)
        {
            log.ErrorFormat("GetEntityProperties: Error during getting entity properties - {0}", ex.ToString());
            return null;
        }
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetFilteredEntityProperties(string property)
    {
        try
        {
            ContainersMetadataDAL cmdal = new ContainersMetadataDAL();
            List<EntityProperty> items = new List<EntityProperty>(cmdal.GetEntityProperties(property, ContainersMetadataDAL.PropertyType.GroupBy));

            DataTable dt = new DataTable();
            dt.Columns.Add("Key");
            dt.Columns.Add("Value");

            foreach (EntityProperty item in items)
            {
                if (!Blocked(item))
                    dt.Rows.Add(item.FullyQualifiedName, item.DisplayName);
            }

            dt.DefaultView.Sort = "Value ASC";

            return new PageableDataTable(dt.DefaultView.ToTable(), items.Count);
        }
        catch (Exception ex)
        {
            log.ErrorFormat("GetEntityProperties: Error during getting entity properties - {0}", ex.ToString());
            return null;
        }
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetEntityPropertyValues(string property, string type, string resourceId)
    {
        try
        {
            SetViewInfoByResourceId(resourceId);
            ContainersMetadataDAL cmdal = new ContainersMetadataDAL();
            DataTable data = cmdal.GetEntityGroupsWithCompaund(type, property);
            Dictionary<string, int> items = cmdal.GetEntityGroupsResult(data, type, property);

            DataTable dt = new DataTable();
            dt.Columns.Add("Value");
            dt.Columns.Add("Status");
            dt.Columns.Add("GroupStatus");

            dt.Rows.Add(Resources.CoreWebContent.WEBDATA_PS0_13, string.Empty, string.Empty);

            foreach (KeyValuePair<string, int> item in items)
            {
                var grStatus = string.Empty;
                foreach (DataRow row in data.Rows)
                {
                    if (row[0].Equals(item.Key))
                    {
                        grStatus = row[2].ToString();
                        break;
                    }
                }
                dt.Rows.Add(item.Key, item.Value, grStatus);
            }

            return new PageableDataTable(dt, items.Count);
        }
        catch (Exception ex)
        {
            log.ErrorFormat("GetEntityPropertyValues: Error during getting entity property values - {0}", ex.ToString());
            return null;
        }
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetObjectProperties(string property, string type)
    {
        try
        {
            ContainersMetadataDAL cmdal = new ContainersMetadataDAL();
            List<EntityProperty> items = new List<EntityProperty>(cmdal.GetEntityProperties(type, ContainersMetadataDAL.PropertyType.GroupBy));

            DataTable dt = new DataTable();
            dt.Columns.Add("Value");
            dt.Columns.Add("Name");
            dt.Columns.Add("Type");

            dt.Rows.Add("", Resources.CoreWebContent.WEBCODE_AK0_70, "");
            foreach (EntityProperty item in items)
            {
                dt.Rows.Add(item.FullyQualifiedName, item.DisplayName, string.Empty);
            }

            return new PageableDataTable(dt, items.Count);
        }
        catch (Exception ex)
        {
            log.ErrorFormat("GetEntityProperties: Error during getting entity properties - {0}", ex.ToString());
            return null;
        }
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetFilteredObjectPropertiesForCpe(string property, string type)
    {
        return GetFilteredObjectPropertiesAll(property, type, true);
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetFilteredObjectProperties(string property, string type)
    {
        return GetFilteredObjectPropertiesAll(property, type);
    }

    private PageableDataTable GetFilteredObjectPropertiesAll(string property, string type, bool isAddGroupParam = false)
    {
        try
        {
            ContainersMetadataDAL cmdal = new ContainersMetadataDAL();
            List<EntityProperty> items = new List<EntityProperty>(cmdal.GetEntityProperties(type, ContainersMetadataDAL.PropertyType.GroupBy));

            //Add only if we can group entities by Group( Container )
            if (isAddGroupParam && IsEntityAvailable(type))
            {
                EntityProperty propertyGroup = new EntityProperty
                {
                    EntityName = "Orion.Container",
                    Name = "ContainerID",
                    DisplayName = Resources.CoreWebContent.WEBCODE_CPE_GroupByGroup,
                    Type = "System.String",
                    GroupBy = true,
                    FilterBy = true,
                    IsNavigable = false,
                    IsCustom = false
                };
                items.Add(propertyGroup);
            }

            DataTable dt = new DataTable();
            dt.Columns.Add("Value");
            dt.Columns.Add("Name");
            dt.Columns.Add("Type");

            dt.Rows.Add("", Resources.CoreWebContent.WEBCODE_AK0_70, "");
            foreach (EntityProperty item in items)
            {
                if (!Blocked(item))
                    dt.Rows.Add(item.FullyQualifiedName, item.DisplayName, item.Type);
            }

            dt.DefaultView.Sort = "Name ASC";

            return new PageableDataTable(dt.DefaultView.ToTable(), items.Count);
        }
        catch (Exception ex)
        {
            log.ErrorFormat("GetEntityProperties: Error during getting entity properties - {0}", ex.ToString());
            return null;
        }
    }

    private static readonly IList<string> _blockedProperties = new List<string>
    {
        "Orion.Nodes.IPAddress",
        "Orion.Nodes.DynamicIP",
        "Orion.Nodes.Caption",
        "Orion.Nodes.DNS",
        "Orion.Nodes.SysName"
    };

    public static bool IsEntityAvailable(string entity)
    {
        if (entity.Equals("Orion.Container") && !AvailableEntities.ContainsKey(entity))
            entity = "Orion.Groups";

        if (entity.Equals("Orion.APM.Application") && !AvailableEntities.ContainsKey(entity))
            entity = "Orion.APM.IIS.Application";

        return AvailableEntities.ContainsKey(entity);
    }

    private static bool Blocked(EntityProperty item)
    {
        return (_blockedProperties.Contains(item.FullyQualifiedName));
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetDependencyObjects(bool isParent)
    {
        try
        {
            var items = new ContainersMetadataDAL().GetAvailableContainerMemberEntities(true, true, isParent);
            using (var dt = new DataTable())
            {
                dt.Columns.Add("Key");
                dt.Columns.Add("Value");

                foreach (var item in items)
                {
                    dt.Rows.Add(item.Key, item.Value);
                }

                return new PageableDataTable(dt, dt.Rows.Count);
            }
        }
        catch (Exception ex)
        {
            log.ErrorFormat("GetDependencyObjects: Error during populating dependency objects - {0}", ex.ToString());
            return null;
        }
    }

    [WebMethod]
    public void SaveUserSetting(string name, string value)
    {
        var blacklist = new WebUserSettingsBlacklist();

        if (!AuthorizationChecker.IsAdmin && blacklist.IsSettingBlacklisted(name))
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.StatusCode = 403;
            return;
        }

        WebUserSettingsDAL.Set(name, value);
    }

    [WebMethod]
    public string GetUserSetting(string name)
    {
        return WebUserSettingsDAL.Get(name);
    }

    [WebMethod]
    public void SaveWebSetting(string name, string value)
    {
        AuthorizationChecker.AllowAdmin();
        SolarWinds.Orion.Web.DAL.WebSettingsDAL.SetValue(name, value);
    }

    [WebMethod]
    public string GetWebSetting(string name, string defaultValue)
    {
        return SolarWinds.Orion.Web.DAL.WebSettingsDAL.Get(name, defaultValue);
    }

    [WebMethod(EnableSession = true)]
    public string GetBreakableHtml(string value)
    {
        return SolarWinds.Orion.Core.Web.FormatHelper.MakeBreakableString(value);
    }

    [WebMethod(EnableSession = true)]
    public Guid GetNewGuid()
    {
        return Guid.NewGuid();
    }

    [WebMethod(EnableSession = true)]
    public void UpdateResourcePosition(int viewId, int resourceId, int column, int position)
    {
        AuthorizationChecker.AllowCustomize();
        ResourceManager.UpdateResourcePosition(viewId, resourceId, column, position);
    }

    [WebMethod(EnableSession = true)]
    public int MoveLastResourceFromColumn(int viewId, int resourceId, int column, int position)
    {
        var resource = ResourceManager.GetResourceByID(resourceId);
        resource.Column = column;
        resource.Position = position;

        int newResourceId = 0;
        ResourceManager.Insert(resource, out newResourceId);

        new ResourceRepository().DeleteColumn(resourceId);
        ResourceManager.UpdateResourcePosition(viewId, newResourceId, column, position);
        return newResourceId;
    }

    [WebMethod(EnableSession = true)]
    public void UpdateColumnWidth(int viewId, int column, int width)
    {
        AuthorizationChecker.AllowCustomize();
        ViewManager.UpdateColumnWidth(viewId, column, width);
    }

    [WebMethod]
    public void RemoveResource(int resourceId)
    {
        AuthorizationChecker.AllowCustomize();

        var resource = ResourceManager.GetResourceByID(resourceId);
        if (resource != null)
        {
            ResourceManager.DeleteById(resourceId);

            var resourcesOnView = ResourceManager.GetResourcesForView(resource.View.ViewID);
            var resourcesOnColumn = resourcesOnView.GetResourcesForColumn(resource.Column);
            int indexPos = 1;

            foreach (var res in resourcesOnColumn)
            {
                ResourceManager.SetPositionById(indexPos++, resource.Column, res.ID);
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetEntityType(string uri)
    {
        return SolarWinds.Orion.Web.Helpers.UriHelper.GetUriType(uri);
    }
}
