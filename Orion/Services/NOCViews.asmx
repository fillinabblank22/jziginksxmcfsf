﻿<%@ WebService Language="C#" Class="NOCViews" %>
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.InformationService;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class NOCViews : WebService
{
    private static Log log = new Log();
	
    [WebMethod]
    public PageableDataTable GetNOCViews(string property, string type, string value, string search)
    {
        int pageSize = 0;
        int startRowNumber = 0;
        
        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);
        if (pageSize == 0)
            pageSize = 10;

        var queryParams = new Dictionary<string, object>();
        string sortOrder = "ViewTitle ASC";
        if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortDirection))
        {
            if ((sortColumn.Equals("ViewType", StringComparison.OrdinalIgnoreCase) || sortColumn.Equals("ViewTitle", StringComparison.OrdinalIgnoreCase)) && (sortOrder.Equals("ASC", StringComparison.OrdinalIgnoreCase) || sortOrder.Equals("DESC", StringComparison.OrdinalIgnoreCase)))
                sortOrder = string.Format("{0} {1}", sortColumn, sortDirection);
        }
        string queryString = String.Format(@"SELECT  ViewID AS ViewID, CASE WHEN ViewGroup IS NULL THEN ViewTitle ELSE ViewGroupName END AS ViewTitle, ViewType, ViewGroup
FROM Orion.Views (nolock=true) WHERE NOCView = true AND ViewID IN (SELECT MAX(ViewID) AS ViewID FROM Orion.Views (nolock=true) GROUP BY IsNull(ViewGroup, ViewID), ViewGroup) ORDER BY {0} WITH ROWS @startRowNumber TO @endRowNumber", sortOrder);
        queryParams[@"startRowNumber"] = startRowNumber + 1;
        queryParams[@"endRowNumber"] = startRowNumber + pageSize;

        string countQuery = @"SELECT Count(ViewID) AS ViewsCount
                              FROM Orion.Views (nolock=true)
where NOCView = true AND ViewID IN 
(SELECT MAX(ViewID) AS ViewID FROM Orion.Views (nolock=true) GROUP BY IsNull(ViewGroup, ViewID), ViewGroup)";
        DataTable countTable;
        DataTable vDataTable;
        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                vDataTable = service.Query(queryString, queryParams);
                countTable = service.Query(countQuery, queryParams);
            }

            foreach (DataRow row in vDataTable.Rows)
            {
                row["ViewType"] = GetViewTypeName(row["ViewType"].ToString());
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

        return new PageableDataTable(vDataTable, (countTable.Rows == null || countTable.Rows.Count == 0 ? 0 : (int)(countTable.Rows[0][0])));
    }

    private string GetViewTypeName(string viewType)
    {
        try
        {
            return ViewManager.GetFriendlyViewTypeName(viewType);
        }
        catch (Exception ex)
        {
            log.WarnFormat("Unable to fing friendly view type name for '{0}'. {1}", viewType, ex);
            return viewType;
        }
    }
    
    [WebMethod]
    public bool DeleteViews(string[] ids)
    {
        AuthorizationChecker.AllowCustomize();

        bool result = true;
        foreach (var id in ids)
        {
            if (Convert.ToInt32(id) != 0)
            {
                ViewManager.DeleteView(Convert.ToInt32(id));
            }
            else
            {
                result = false;
                log.ErrorFormat("Error occured during getting viewID from [{0}]", id);
            }
        }
        return result;
    }

    [WebMethod]
    public bool DisableNOCViews(string[] ids, string[] groups)
    {
        AuthorizationChecker.AllowCustomize();

        if (ids == null || ids.Length == 0)
        {
            log.Debug("Nothing to enable/disable.");
            return true;
        }

        foreach (string id in ids)
        {
            if (string.IsNullOrEmpty(id))
                continue;

            var view = ViewManager.GetViewById(Convert.ToInt32(id));
            view.NOCView = false;
            view.NOCViewRotationInterval = 0;
            ViewManager.UpdateViewGroupNocData(view);
        }
        
        ViewsDAL.DisableNOCView(ids, groups);
        return true;
    }
}


