﻿<%@ WebService Language="C#" Class="OidPicker" %>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models.Mib;
using System.Web.Script.Services;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

/// <summary>
/// Summary description for OidPicker
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class OidPicker : System.Web.Services.WebService
{
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator(); 
    
    /// <summary>
    /// Method for returning the children of a given OID. The node parameter is supplied
    /// by ExtJs, and it can't be easily changed, thus the name.
    /// </summary>
    /// <param name="node"></param>
    /// <returns></returns>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<TreeNode> GetChildren(string node)
    {
        using (var proxy = _blProxyCreator.Create(handleError))
        {
            return convertToTreeNodes(proxy.GetChildOids(node));
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void AbortQuery()
    {
        using (var proxy = _blProxyCreator.Create(handleError))
        {
            proxy.CancelRunningCommand();
        }
    }

    /// <summary>
    /// Search method supplying multiple modes based on the input from the client.
    /// </summary>
    /// <param name="searchCriteria"></param>
    /// <returns></returns>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<TreeNode> Search(string searchCriteria)
    {
        var result = new List<TreeNode>();
        using (var proxy = _blProxyCreator.Create(handleError))
        {
            if(string.IsNullOrEmpty(searchCriteria) == false)
            {
                searchCriteria = searchCriteria.Trim();
            }
            //If the criteria is an OID, search for the oid.
            if (isOid(searchCriteria))
            {
                var oid = proxy.GetOid(searchCriteria);
                if (oid != null)
                {
                    result.Add(convertToTreeNode(proxy.GetOid(searchCriteria)));
                }
            }
            //If it isn't an OID, try to search by name.
            else
            {
                result.AddRange(convertToTreeNodes(proxy.GetSearchingOidsByName(searchCriteria), true));
            }
        }
        return result.OrderBy(x => x.id).ToList();
    }

    private void handleError(Exception ex)
    {

    }

    /// <summary>
    /// Helper method to convert an Oids collection into a list of
    /// treenode entities expected by the ExtJs treeview.
    /// </summary>
    /// <param name="oids"></param>
    /// <returns></returns>
    private List<TreeNode> convertToTreeNodes(Oids oids)
    {
        return oids.Select(x => convertToTreeNode(x)).ToList();
    }

    private List<TreeNode> convertToTreeNodes(Oids oids, bool isSearchResult)
    {
        return oids.Select(x => convertToTreeNode(x, isSearchResult)).OrderBy(x => x.id).ToList();
    }

    /// <summary>
    /// Helper method to convert a single Oid entity into a
    /// treenode entity expected by the ExtJs treeview.
    /// </summary>
    /// <param name="oid"></param>
    /// <returns></returns>
    private TreeNode convertToTreeNode(Oid oid)
    {
        TreeNode result = null;
        if (isSelectable(oid.StringType))
        {
            result = new SelectableTreeNode();
        }
        else
        {
            result = new TreeNode();
        }

        result.id = oid.ID;
        result.text = string.Format("{0}({1})", oid.Name, oid.TreeIndex);
        result.leaf = !oid.HasChildren;
        result.description = oid.Description;
        result.accessType = oid.Access.ToString();
        result.mib = oid.MIB;
        result.status = oid.Status.ToString();
        result.variableType = oid.VariableType.ToString();
        //stringType is used to determine which image to use, but ExtJS passes it in CSS as background-image.
        //It uses the url() CSS function, and parenthses would interfere with them.
        string oidStringType = oid.StringType;
        if (oidStringType.Contains('('))
        {
            int stripStart = oidStringType.IndexOf('(');
            oidStringType = oidStringType.Remove(stripStart, oidStringType.Length - stripStart);
        }
        result.stringType = oid.StringType;
        result.isTabular = oid.StringType == "SEQUENCE";
        result.name = oid.Name;
        //Open into the internet branch on load, just like in the UnDP app
        result.expanded = (oid.ID == "1.3" || oid.ID == "1.3.6");

        determineOidToUse(result);
        result.icon = string.Format("/Orion/OidIcons.ashx?oid={0}&stringtype={1}&istabular={2}&isExpanded={3}", oid.ID, oidStringType, result.isTabular, result.expanded);
        return result;
    }

    private void determineOidToUse(TreeNode node)
    {
        node.calculatedOid = node.id;
        node.calculatedName = node.name;
        node.calculatedIsTabular = node.isTabular;

        if (string.IsNullOrEmpty(node.stringType) == false)
        {
            if (node.stringType.ToUpper().Contains("ENTRY"))
            {
                //If the current item is entry, return the parent (table oid).
                string parentOid = getParentOid(node.id, 1);
                var parent = Search(parentOid).FirstOrDefault();
                if (parent != null)
                {
                    node.calculatedOid = parent.id;
                    node.calculatedName = parent.name;
                    node.calculatedIsTabular = parent.calculatedIsTabular;

                    node.tableName = parent.name;
                    node.isTabular = parent.calculatedIsTabular;
                }
            }
            else
            {
                string parentOid = getParentOid(node.id, 2);
                var parent = Search(parentOid).FirstOrDefault();
                if (parent != null && parent.isTabular)
                {
                    node.calculatedOid = parent.id;
                    node.calculatedName = parent.name;
                    node.calculatedIsTabular = parent.calculatedIsTabular;

                    node.tableName = parent.name;
                    node.isTabular = parent.calculatedIsTabular;
                }
            }
        }
    }

    /// <summary>
    /// Returns the nth parent of the given oid.
    /// </summary>
    /// <param name="oid">The oid to work with.</param>
    /// <param name="levels">The level of the parent.</param>
    /// <returns></returns>
    private string getParentOid(string oid, int targetLevel)
    {
        if (string.IsNullOrEmpty(oid) == false)
        {
            //Get the total number of levels.
            int numberOfLevels = oid.Count(x => x == '.');
            //Only do work if the level can be found.
            if (numberOfLevels > targetLevel)
            {
                int numberOfLevelsFound = 0;
                int targetIndex = -1;

                //Loop through the oid from behind and try to find the nth level.
                for (int i = oid.Length - 1; i > 0; i--)
                {
                    if (oid[i] == '.')
                    {
                        numberOfLevelsFound++;
                        if (numberOfLevelsFound == targetLevel)
                        {
                            targetIndex = i;
                            break;
                        }
                    }
                }
                //Target level found, return
                if (targetIndex != -1)
                {
                    return oid.Substring(0, targetIndex);
                }
            }
        }
        return string.Empty;
    }

    private TreeNode convertToTreeNode(Oid oid, bool isSearchResult)
    {
        TreeNode result = convertToTreeNode(oid);
        result.text = string.Format("{0} ({1}: {2})", oid.ID, oid.MIB, oid.Name);
        return result;
    }

    /// <summary>
    /// Helper method which determines if a given string is an OID. It doesn't
    /// checks validity, only if that the string is consisted of digits and dots.
    /// </summary>
    /// <param name="criteria"></param>
    /// <returns></returns>
    private bool isOid(string criteria)
    {
        return criteria.All(x => char.IsDigit(x) || x == '.');
    }

    private bool isSelectable(string stringType)
    {
        bool result = false;
        if (string.IsNullOrEmpty(stringType) == false)
        {
            switch (stringType.ToUpperInvariant())
            {
                case "SEQUENCE":
                    result = false;
                    break;
                case "TRAP":
                    result = true;
                    break;
                default:
                    if ((stringType.Length > 0) && (!stringType.Equals("unknown")))
                    {
                        result = true;
                    }
                    break;
            }

            if (stringType.ToUpperInvariant().Contains("ENTRY"))
            {
                result = false;
            }
        }
        return result;
    }

    /// <summary>
    /// A class for representing data in a format the ExtJs TreePanel can consume. This one is used
    /// for parent nodes, it lacks the checked property to make it unselectable on the client side.
    /// </summary>
    public class TreeNode
    {
        public string id { get; set; }
        public string text { get; set; }
        public bool expanded { get; set; }
        public bool leaf { get; set; }
        public string description { get; set; }
        public string mib { get; set; }
        public string mibPath { get; set; }
        public string accessType { get; set; }
        public string status { get; set; }
        public string variableType { get; set; }
        public string iconCls { get; set; }
        public string icon { get; set; }
        public string stringType { get; set; }
        public bool isTabular { get; set; }
        public string name { get; set; }
        public string tableName { get; set; }
        public string calculatedOid { get; set; }
        public string calculatedName { get; set; }
        public bool calculatedIsTabular { get; set; }
    }

    public class SelectableTreeNode : TreeNode
    {
        public bool @checked { get; set; }
    }
}
