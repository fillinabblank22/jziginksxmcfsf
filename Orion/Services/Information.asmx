<%@ WebService Language="C#" Class="Information" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Web.Services;
using System.ServiceModel;
using System.Web.Script.Services;
using System.Web.UI;
using System.Xml;
using SolarWinds.InformationService.Contract2;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Federation;
using SolarWinds.Orion.Web.InformationService;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class Information : System.Web.Services.WebService
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    [WebMethod]
    public DataTable Query(string query)
    {
        return DoQueryWithRetries(query, null);
    }

    [Serializable]
    public class QueryResult
    {
        public DataTable Data { get; set; }
        public string ErrorHtml { get; set; }
        public SwisQueryError QueryError { get; set; }
    }

    [Serializable]
    public class SwisQueryError
    {
        public string LocalizedMessage { get;  }
        public string StackTrace { get;  }
        public string InnerException { get;  }
        public string InnerExceptionMessage { get;  }

        public SwisQueryError(string localizedMessage, string stackTrace)
        {
            LocalizedMessage = localizedMessage;
            StackTrace = stackTrace;
        }
            
        public SwisQueryError(SwisQueryException ex): this (ex.LocalizedMessage, ex.StackTrace)
        {
            if (ex.InnerException != null)
            {
                InnerException = ex.InnerException.ToString();
                InnerExceptionMessage = ex.InnerException.Message;
            }
        }
    }

    [WebMethod]
    public QueryResult QueryWithPartialErrors(string query)
    {
        using (new SwisSettingsContext { AppendErrors = true })
        {
            DataTable dt = DoQueryWithRetries(query, null);
            List<SwisErrorMessage> errors = SwisDataTableParser.GetDataTableErrors(dt);
            UserControl control = new UserControl();
            BaseSwisfErrorControl swisfErrorControl = control.LoadControl("~/Orion/SwisfErrorControl.ascx") as BaseSwisfErrorControl;
            string errorHtml = BaseSwisfErrorControl.GetSwisfErrorControlHtmlString(errors, swisfErrorControl);
            return new QueryResult { Data = dt, ErrorHtml = errorHtml };
        }
    }

    [WebMethod]
    public QueryResult QueryWithServerDetailedError(string query, string server)
    {
        return DoQueryWithRetries(() => DoQueryWithDetailedError(query, server, null));
    }

    [WebMethod]
    public DataTable QueryWithServer(string query, string server)
    {
        return DoQueryWithRetries(query, server, null);
    }

    [WebMethod]
    public DataTable QueryWithParameters(string query, IDictionary<string, object> parameters)
    {
        return DoQueryWithRetries(query, parameters);
    }

    [WebMethod]
    public string Create(IDictionary<string, object> properties)
    {
        var instanceType = (string)properties["InstanceType"];
        properties.Remove("InstanceType");
        properties.Remove("Uri");
        foreach (var key in properties.Where(y => y.Value == null).Select(y => y.Key).ToList())
            properties.Remove(key);
        return InformationServiceProxy.CreateV3().Create(instanceType, properties);
    }

    [WebMethod]
    public IDictionary<string, object> Read(string uri)
    {
        var y = InformationServiceProxy.CreateV3().Read(uri).ToDictionary(x => x.Key, x => x.Value is DBNull ? null : x.Value);
        return y;
    }

    [WebMethod]
    public void Update(string uri, IDictionary<string, object> propertiesToUpdate)
    {
        foreach (var key in propertiesToUpdate.Where(y => y.Value == null).Select(y => y.Key).ToList())
            propertiesToUpdate[key] = DBNull.Value;

        InformationServiceProxy.CreateV3().Update(uri, propertiesToUpdate);
    }

    [WebMethod]
    public void Delete(string uri)
    {
        InformationServiceProxy.CreateV3().Delete(uri);
    }

    private DataTable DoQueryWithRetries(string query, IDictionary<string, object> parameters)
    {
        return DoQueryWithRetries(query, null, parameters);
    }

    private DataTable DoQueryWithRetries(string query, string serverName, IDictionary<string, object> parameters)
    {
        return DoQueryWithRetries(() => DoQuery(query, serverName, parameters));
    }

    private T DoQueryWithRetries<T>(Func<T> doQuery, int retries = 1)
    {
        while (true)
        {
            --retries;
            try
            {
                return doQuery();
            }
            catch (FaultException)
            {
                // FaultException is a subclass of CommunicationException. We want to retry on 
                // CommunicationException but not FaultException, so we need to catch and rethrow here.
                throw;
            }
            catch (CommunicationException ex)
            {
                if (retries >= 0)
                    log.Warn("CommunicationException caught. Retrying...", ex);
                else
                    throw;
            }
        }
    }

    public DataTable DoQuery(string query, IDictionary<string, object> parameters)
    {
        return DoQuery(query, null, parameters);
    }

    public DataTable DoQuery(string query, string serverName, IDictionary<string, object> parameters)
    {
        try
        {
            return DoQueryInternal(query, serverName, parameters);
        }
        catch (SwisQueryException ex)
        {
            throw new Exception(ex.LocalizedMessage);
        }
    }

    private QueryResult DoQueryWithDetailedError(string query, string serverName, IDictionary<string, object> parameters)
    {
        try
        {
            return new QueryResult {Data = DoQueryInternal(query, serverName, parameters)};
        }
        catch (SwisQueryException ex)
        {
            return new QueryResult {QueryError = new SwisQueryError(ex)};
        }
    }

    private DataTable DoQueryInternal(string query, string serverName, IDictionary<string, object> parameters)
    {
        using (InformationServiceProxy service = String.IsNullOrEmpty(serverName) ? InformationServiceProxy.CreateV3() : InformationServiceProxy.CreateV3(serverName))
        {
            return service.Query(query, parameters);
        }
    }

    [WebMethod]
    public string[] GetEntitiesWithVerbs()
    {
        using(var proxy = InformationServiceProxy.CreateV3())
        {
            var entities = proxy.Query("SELECT distinct EntityName FROM Metadata.Verb");
            var rv = new string[entities.Rows.Count];
            int i = 0;
            foreach (DataRow row in entities.Rows)
                rv[i++] = row[0].ToString();

            return rv;
        }
    }

    [WebMethod]
    public string[] GetEntityVerbs(string entity)
    {
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            var verbs = proxy.Query("SELECT Name FROM Metadata.Verb WHERE EntityName=@entity", new Dictionary<string, object>() { { "entity", entity } });
            var rv = new string[verbs.Rows.Count];
            int i = 0;
            foreach (DataRow row in verbs.Rows)
                rv[i++] = row[0].ToString();

            return rv;
        }
    }

    [WebMethod]
    public Dictionary<string, string>[] GetVerbParams(string entity, string verb)
    {
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            var verbs = proxy.Query("SELECT Position, Name, Type FROM Metadata.VerbArgument WHERE EntityName=@entity AND VerbName=@verb", new Dictionary<string, object>() { { "entity", entity }, {"verb", verb} });
            var rv = new Dictionary<string, string>[verbs.Rows.Count];
            int i = 0;
            foreach (DataRow row in verbs.Rows)
                rv[i++] = new Dictionary<string, string>
                {
                    { "position", row[0].ToString() },
                    { "name", row[1].ToString() },
                    { "type", row[2].ToString() }
                };


            return rv;
        }
    }

    private static object ConvertToType(string type, string param)
    {
        var t = Type.GetType(type);
        if (t.IsArray)
        {
            var ps = param.Split(';');
            var rv = (Array)Activator.CreateInstance(t, ps.Length);
            int i = 0;
            foreach(var p in ps)
            {
                rv.SetValue(ConvertToType(t.GetElementType().FullName, p), i++);
            }
            return rv;
        }
        else
        {
            return Convert.ChangeType(param, t);
        }
    }

    // ugly, but working.
    private static XmlElement SerializeObjectForSWIS(object o)
    {
        var dcs = new DataContractSerializer(o.GetType());
        var sb = new StringBuilder();
        using (var xw = XmlWriter.Create(sb))
        {
            dcs.WriteObject(xw, o);
        }
        var xdoc = new XmlDocument();
        xdoc.Load(new StringReader(sb.ToString()));
        return xdoc.DocumentElement;
    }

    [WebMethod]
    public string Invoke(string entity, string verb, string[] param)
    {
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            var verbs = proxy.Query("SELECT Position, Name, Type FROM Metadata.VerbArgument WHERE EntityName=@entity AND VerbName=@verb", new Dictionary<string, object>() { { "entity", entity }, { "verb", verb } });
            int i = 0;
            var paramParsed = new List<object>();
            if (param.Length != verbs.Rows.Count)
                throw new InvalidOperationException("invalid parameter count");

            foreach (DataRow row in verbs.Rows)
            {
                paramParsed.Add(ConvertToType(row[2].ToString(), param[i++]));
            }

            try
            {
                var rv = proxy.Invoke(entity, verb, paramParsed.Select(SerializeObjectForSWIS));
                var sb = new StringBuilder();
                var set = new XmlWriterSettings()
                {
                    OmitXmlDeclaration = true,
                    Indent = true,
                    NewLineOnAttributes = true
                };
                using (var xw = XmlWriter.Create(sb, set))
                {
                    rv.WriteTo(xw);
                }

                return (sb.ToString());
            }
            catch (FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract> ex)
            {
                throw new Exception(ex.Detail.Message);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
    }

}
