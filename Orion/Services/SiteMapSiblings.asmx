﻿<%@ WebService Language="C#" Class="SiteMapSiblings" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using SolarWinds.Orion.Web.SiteMaps;

/// <summary>
/// Provides data for on-demand sibling dropdowns loading process
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class SiteMapSiblings : System.Web.Services.WebService 
{
    /// <summary>
    /// Returns siblings of child node identified by childNodeDataId which will be shown in drop down 
    /// by parent node.
    /// siteMap identifiers are here for direct access to appropriate siteMap configuration,
    /// because we don't have here HttpContext content which allows SiteMapConfigurationManager 
    /// to choose right siteMap configuration
    /// </summary>
    /// <param name="siteMapKey">used for direct access to appropriate siteMap configuration</param>
    /// <param name="siteMapIndex">used for direct access to appropriate siteMap configuration</param>
    /// <param name="siteMapProviderName">used for direct access to appropriate siteMap configuration</param>
    /// <param name="currentPageNodeKey">key of site map node currently presented by the page</param>
    /// <param name="currentPageNodeDataId">data identifier of site map node currently presented by the page</param>
    /// <param name="parentNodeKey">key of site map node, by which required sibling list should be rendered</param>
    /// <param name="parentNodeDataId">data identifier of parent site map node, by which required sibling list should be rendered</param>
    /// <param name="childNodeDataId">identifier of data, presented by child siteMap node (in breadcrumbs hierarchy) of siteMap node, by which siblings list should be eventually rendered</param>
    /// <returns></returns>
    [WebMethod]
    public List<SiteMapSiblingInfo> GetSiblings(string siteMapKey, int siteMapIndex, string siteMapProviderName,
        string currentPageNodeKey, string currentPageNodeDataId, string parentNodeKey, string parentNodeDataId, string childNodeDataId)
    {
        var siteMapProvider =
            !string.IsNullOrEmpty(siteMapProviderName)
            ? SiteMap.Providers[siteMapProviderName]
            : SiteMap.Provider;

        var decodedSiteMapKey = HttpUtility.HtmlDecode(siteMapKey);
        var siteMapInfo = SiteMapConfigurationManager.Instance.GetSiteMapConfiguration(decodedSiteMapKey, siteMapIndex);
        var provider = siteMapInfo.GetSiblingsProvider(currentPageNodeKey, currentPageNodeDataId, siteMapProvider);
        if (provider != null)
        {
            return provider.GetSiblings(parentNodeKey, parentNodeDataId, childNodeDataId).ToList();
        }
        return new List<SiteMapSiblingInfo>();
    }
}