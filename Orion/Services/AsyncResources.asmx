<%@ WebService Language="C#" Class="AsyncResources" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.ModuleManager;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.Web.Plugins;
using SolarWinds.Orion.Web.Helpers;
using Node = SolarWinds.Orion.NPM.Web.Node;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class AsyncResources : System.Web.Services.WebService
{
    private static readonly Log log = new Log();


    [WebMethod]
    public DataTable GetAvailabilityStats(int nodeId)
    {
        try
        {
            DataTable nodeAvailabilityByDays = SqlDAL.GetNodeAvailability(nodeId);
            AvailabilityStatisticsCalculator availabilityCalculator = new AvailabilityStatisticsCalculator();
            var availabilityStatistics = availabilityCalculator.GetNodeAvailabilityStatisticsTable(nodeAvailabilityByDays);
            return availabilityStatistics;
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }
    }

    [WebMethod]
    public Object GetAllDependencies(int resourceId, string parentFilter, string childFilter, string dependencyFilter)
    {
        ResourceInfo resource = ResourceManager.GetResourceByID(resourceId);
        if (resource != null)
        {
            Context.Items[typeof(ViewInfo).Name] = resource.View;
        }

        DataTable sourceDataTable = null;
        string swisError = string.Empty;

        parentFilter = parentFilter.Trim();
        childFilter = childFilter.Trim();
        dependencyFilter = dependencyFilter.Trim();

        DataTable resultTable = new DataTable();
        resultTable.Columns.Add(new DataColumn("DEPENDENCY NAME", typeof(string)));
        resultTable.Columns.Add(new DataColumn("_IconFor_DEPENDENCY NAME", typeof(string)));
        resultTable.Columns.Add(new DataColumn("PARENT", typeof(string)));
        resultTable.Columns.Add(new DataColumn("_IconFor_PARENT", typeof(string)));
        resultTable.Columns.Add(new DataColumn(" ", typeof(string)));
        resultTable.Columns.Add(new DataColumn("_IconFor_ ", typeof(string)));
        resultTable.Columns.Add(new DataColumn("CHILD", typeof(string)));
        resultTable.Columns.Add(new DataColumn("_IconFor_CHILD", typeof(string)));
        try
        {
            using (DependenciesDAL dal = new DependenciesDAL())
            using (var swisErrorsContext = new SwisErrorsContext())
            {
                sourceDataTable = dal.GetAllDependenciesFilteredLinq(parentFilter, childFilter, dependencyFilter);

                var errorMessages = swisErrorsContext.GetErrorMessages();
                if (errorMessages != null && errorMessages.Any())
                {
                    var control = new System.Web.UI.UserControl();
                    var swisfErrorControl =
                        control.LoadControl("~/Orion/SwisfErrorControl.ascx") as BaseSwisfErrorControl;
                    swisError = BaseSwisfErrorControl.GetSwisfErrorControlHtmlString(
                        errorMessages.Select(error => new SolarWinds.Orion.Web.Federation.SwisErrorMessage(error)).ToList(),
                        swisfErrorControl);
                }
            }
            foreach (DataRow row in sourceDataTable.Rows)
            {
                DataRow newRow = resultTable.NewRow();
                newRow["DEPENDENCY NAME"] = SolarWinds.Orion.Core.Web.FormatHelper.MakeBreakableString(HttpUtility.HtmlEncode(row["Name"].ToString()));
                newRow["_IconFor_DEPENDENCY NAME"] = "/Orion/images/dependency_16x16.gif";
                newRow["PARENT"] = CommonWebHelper.RenderObjectName(row["ParentUri"].ToString(), HttpUtility.HtmlEncode(row["ParentDisplayName"].ToString()));
                newRow["_IconFor_PARENT"] = CommonWebHelper.GetStatusIconPath(row["ParentUri"].ToString(),
                                                                              row["ParentStatus"].ToString());
                newRow[" "] = string.Empty;
                newRow["_IconFor_ "] = "/Orion/images/arrow_forward.gif";
                newRow["CHILD"] = CommonWebHelper.RenderObjectName(row["ChildUri"].ToString(), HttpUtility.HtmlEncode(row["ChildDisplayName"].ToString()));
                newRow["_IconFor_CHILD"] = CommonWebHelper.GetStatusIconPath(row["ChildUri"].ToString(),
                                                                             row["ChildStatus"].ToString());
                resultTable.Rows.Add(newRow);
            }
        }
        catch (FaultException ex)
        {
            if (!string.IsNullOrEmpty(parentFilter) || !string.IsNullOrEmpty(childFilter) || !string.IsNullOrEmpty(dependencyFilter))
                throw new Exception(string.Format(Resources.CoreWebContent.WEBCODE_VB0_32, ex.Message, string.Format("{0} {1} {2}", parentFilter, childFilter, dependencyFilter)));
            else
                throw;
        }

        return new
        {
            ResultTable = resultTable,
            ErrorHtml = swisError
        };
    }

    [WebMethod(EnableSession = false)]
    public object GetNodeDetails(int nodeId, int viewLimitationId)
    {
        // add view info with limitation id to apply limitations when NetObjectFactory.Create is called
        if (viewLimitationId > 0 && !HttpContext.Current.Items.Contains(typeof(ViewInfo).Name))
        {
            var viewInfo = new ViewInfo("", "", "", false, false, typeof(Node));
            viewInfo.LimitationID = viewLimitationId;
            HttpContext.Current.Items[typeof(ViewInfo).Name] = viewInfo;
        }
        
        var node = (Node) NetObjectFactory.Create(string.Format("N:{0}", nodeId));
        
        node.SetCustomProperty("HardwareType", GetNodeHardwareTypeStringRepresentation(nodeId));
        if (string.IsNullOrEmpty(node.IOSImage) || node.IOSImage.Equals("unknown", StringComparison.OrdinalIgnoreCase))
            node.SetCustomProperty("IOSImage", Resources.CoreWebContent.WEBCODE_AK0_6);
        
        int severity;
        int childstatus;
        string statusDescription;
        string childStatusDescription;
        if (NodeChildStatusDAL.GetChildStatusDetails(node.NodeID, true, out childstatus, out severity, out childStatusDescription))
        {
            // this status description reflects limitations.
            statusDescription = childStatusDescription;
        }
        else
        {
            statusDescription = node.StatusDescription ?? string.Empty;
        }

        int cpuNo;
        using (var dal = new WebDAL())
            cpuNo = dal.GetNodeCPUIndexCount(node.NodeID);
            
        string customPollerAdminButtonDisplayStyle = "display:none;";
        if (node.IsSNMP && ModuleManager.InstanceWithCache.IsThereModule("NPM"))
            customPollerAdminButtonDisplayStyle = string.Empty;
           
        // status image alt text
        string nodeStatusAltText;
        string localizedParentEnum = Resources.CoreWebContent.ResourceManager.GetString(string.Format("{0}_{1}", typeof(Status).Name, node.Status.ParentStatus));
        if (node.Status.ChildStatus.Value == OBJECT_STATUS.Up)
        {
            nodeStatusAltText = string.Format(Resources.CoreWebContent.WEBCODE_VB0_43, localizedParentEnum);
        }
        else
        {
            string localizedChildEnum = Resources.CoreWebContent.ResourceManager.GetString(string.Format("{0}_{1}", typeof(Status).Name, node.Status.ChildStatus));
            nodeStatusAltText = string.Format(Resources.CoreWebContent.WEBCODE_VB0_44, localizedParentEnum, localizedChildEnum);
        }
        
        // Info will be displayed just for outages that will occur in the future.
        // If node is unmanaged already, there us re-manage button instead.
        var unmanageInfo = new UnmanageInfoData
        {
            NodeId = nodeId,
            UnmanageFromLocal = node.UnManageFrom.ToLocalTime(),
            UnmanageToLocal = node.UnManageUntil.ToLocalTime(),
            DisplayUnmanageInfo = false,
            UnmanageInfo = String.Empty,
            UnmanageCancelStyle = string.Empty,
            UnmanageReason = string.Empty,
            UnmanageInfoStyle = string.Empty
        };

        if (MaintenanceModeDAL.IsMaintenanceModeEnabled)
        {
            GetNewUnmanageInfo(unmanageInfo, ref statusDescription);
        }
        else
        {
            GetOldUnmanageInfo(unmanageInfo);
        }

        return new {
                       node.NodeID,
                       Name = CommonWebHelper.JQueryEncode(node.Name),
                       ListResourcesDisplayStyle = node.IsSNMP || node.ObjectSubType.Equals("wmi", StringComparison.OrdinalIgnoreCase)  ? string.Empty : "display:none;",
                       CustomPollerAdminButtonDisplayStyle = customPollerAdminButtonDisplayStyle,
                       node.UnManaged,
                       node.External,
                       NodeStatusImagePath = node.Status.ToString("imgpath", null),
                       NodeStatusAltText = nodeStatusAltText,
                       NodeStatusDescription = MakeBreakableString(statusDescription).Replace(Resources.CoreWebContent.String_Separator_1_VB0_59, (Resources.CoreWebContent.String_Separator_1_VB1_1 + "<br/>")),
                       node.IPAddressString,
                       DynamicIPString = node.DynamicIP ? Resources.CoreWebContent.WEBDATA_VB0_124  :  Resources.CoreWebContent.WEBDATA_VB0_125,
                       node.VendorIcon,
                       MachineType = HttpUtility.HtmlEncode(MakeBreakableString(node.MachineType)),
                       SysName = HttpUtility.HtmlEncode(node.SysName),
                       DNSName = HttpUtility.HtmlEncode(node.DNSName),
                       Description = HttpUtility.HtmlEncode(MakeBreakableString(node.Description)),
                       Location = HttpUtility.HtmlEncode(MakeBreakableString(node.Location)),
                       Contact = HttpUtility.HtmlEncode(MakeBreakableString(node.Contact)),
                       node.SysObjectID,
                       LastBoot = node.LastBoot > System.Data.SqlTypes.SqlDateTime.MinValue.Value ? MakeBreakableString(node.LastBoot.ToString("f")) : String.Empty,
                       IOSVersion = HttpUtility.HtmlEncode(MakeBreakableString(node.IOSVersion)),
                       IOSImage = HttpUtility.HtmlEncode(MakeBreakableString(node.IOSImage)),
                       HardwareType = MakeBreakableString(node["HardwareType"]),
                       CpuNo = cpuNo,
                       node.HrefIPAddress,
                       UnmanageInfoStyle = unmanageInfo.UnmanageInfoStyle + (unmanageInfo.DisplayUnmanageInfo ? String.Empty : "display:none"),
                       unmanageInfo.UnmanageInfo,
                       unmanageInfo.UnmanageCancel,
                       unmanageInfo.UnmanageCancelStyle,
                       unmanageInfo.UnmanageReason,
                       UnmanageReasonStyle = string.IsNullOrEmpty(unmanageInfo.UnmanageReason) ? "display:none" : String.Empty,
                       node.CustomStatus,
                       StatusReasonStyle = node.CustomStatus ? String.Empty : "display:none",
                       EffectiveCategory = NodeDetailHelper.GetNodeCategoryDisplayText(node.EffectiveCategory)
                   };
    }

    private static string GetAgoString(TimeSpan timeDiff)
    {
        if (timeDiff > TimeSpan.FromDays(1))
        {
            return string.Format(Resources.CoreWebContent.WEBCODE_PS1_8, timeDiff.Days,
                timeDiff.Hours, timeDiff.Minutes);
        }
        if (timeDiff > TimeSpan.FromHours(1))
        {
            return string.Format(Resources.CoreWebContent.WEBCODE_PS1_6, timeDiff.Hours,
                timeDiff.Minutes);
        }
        
        return string.Format(Resources.CoreWebContent.WEBCODE_PS1_7, timeDiff.Minutes);
    }

    private static void GetNewUnmanageInfo(UnmanageInfoData unmanageInfo, ref string statusDescription)
    {
        unmanageInfo.UnmanageCancel = Resources.CoreWebContent.WEBCODE_PS1_5;
        unmanageInfo.UnmanageInfoStyle = "color: #888;";
        if (unmanageInfo.UnmanageToLocal > DateTime.Now) // re-manage sets just unmanageuntil to DateTime.UtcNow
        {
            unmanageInfo.DisplayUnmanageInfo = true;

            var maintenancePlans = MaintenanceModeDAL.GetMaintenancePlansForEntity("Orion.Nodes", unmanageInfo.NodeId)
                .Where(p => p.Enabled);

            if (unmanageInfo.UnmanageFromLocal > DateTime.Now) // future plan
            {
                var nearestFuturePlan = maintenancePlans
                    .OrderBy(p => p.UnmanageDate)
                    .FirstOrDefault();

                var timeDiff = unmanageInfo.UnmanageFromLocal - DateTime.Now;
                var timeDiffStr = GetAgoString(timeDiff);

                if (nearestFuturePlan != null)
                {
                    unmanageInfo.UnmanageInfo = string.Format(Resources.CoreWebContent.WEBCODE_PS1_4,
                        nearestFuturePlan.AccountID, timeDiffStr, nearestFuturePlan.Reason);
                }
                else
                {
                    unmanageInfo.UnmanageInfo = string.Format(Resources.CoreWebContent.WEBCODE_PS1_9,
                        timeDiffStr, Resources.CoreWebContent.WEBCODE_PS1_10);
                }
            }
            else // active plan
            {
                var timeDiff = DateTime.Now - unmanageInfo.UnmanageFromLocal;
                var timeDiffStr = GetAgoString(timeDiff);

                unmanageInfo.UnmanageInfo = string.Format(Resources.CoreWebContent.WEBCODE_PS1_14,
                    timeDiffStr, unmanageInfo.UnmanageFromLocal.ToShortDateString() + " " + unmanageInfo.UnmanageFromLocal.ToShortTimeString());

                var latestPlan = maintenancePlans
                    .OrderByDescending(p => p.UnmanageDate)
                    .FirstOrDefault();

                statusDescription = Resources.CoreWebContent.WEBCODE_PS1_11;

                if (latestPlan != null)
                {
                    statusDescription += string.Format(Resources.CoreWebContent.WEBCODE_PS1_12, latestPlan.AccountID);
                    unmanageInfo.UnmanageReason = string.Format(Resources.CoreWebContent.WEBCODE_PS1_13, latestPlan.Reason);
                }

                // can't cancel active plan
                unmanageInfo.UnmanageCancelStyle = "display:none";
            }
        }
    }

    private static void GetOldUnmanageInfo(UnmanageInfoData unmanageInfo)
    {
        unmanageInfo.UnmanageCancel = Resources.CoreWebContent.WEBCODE_VS1_7;
        if (unmanageInfo.UnmanageFromLocal > DateTime.Now &&
            unmanageInfo.UnmanageToLocal > DateTime.Now) // re-manage sets just unmanageuntil to DateTime.UtcNow
        {
            unmanageInfo.DisplayUnmanageInfo = true;

            const string dtlFormat = "{0}&nbsp;{1}";

            var unmanageInfoFrom = String.Format(dtlFormat,
                                                 unmanageInfo.UnmanageFromLocal.ToShortDateString(),
                                                 unmanageInfo.UnmanageFromLocal.ToShortTimeString());

            var unmanageInfoTo = String.Format(dtlFormat,
                                               unmanageInfo.UnmanageToLocal.ToShortDateString(),
                                               unmanageInfo.UnmanageToLocal.ToShortTimeString());

            unmanageInfo.UnmanageInfo = String.Format(Resources.CoreWebContent.WEBCODE_VS1_6, unmanageInfoFrom, unmanageInfoTo);
        }
    }
    
    private class UnmanageInfoData
    {
        public DateTime UnmanageFromLocal; 
        public DateTime UnmanageToLocal; 
        public int NodeId; 
        public bool DisplayUnmanageInfo;
        public string UnmanageInfo;
        public string UnmanageReason;
        public string UnmanageCancelStyle;
        public string UnmanageCancel;
        public string UnmanageInfoStyle;
    }
    
    private static string MakeBreakableString(object val)
    {
        return FormatHelper.MakeBreakableString(val.ToString(), HttpContext.Current.Request.Browser.Browser == "IE" && HttpContext.Current.Request.Browser.MajorVersion == 6);
    }

    private static string GetNodeHardwareTypeStringRepresentation(int nodeId)
    {
        if (VimHelper.VMwarePollingAvailable)
        {
            int hostNodeID;
            string hostCaption;
            VmNodeType nt = (new VMWareSwisDAL()).GetNodeHarwareType(nodeId, out hostNodeID, out hostCaption);

            switch (nt)
            {
                case VmNodeType.NotHosted: return Resources.CoreWebContent.WEBCODE_VB0_36;
                case VmNodeType.HostUnknown: return Resources.CoreWebContent.WEBCODE_VB0_37;
                case VmNodeType.Hosted:
                    return string.Format(Resources.CoreWebContent.WEBCODE_VB0_38, String.Format("<a href='/Orion/View.aspx?NetObject=N:{0}'>{1}</a>", hostNodeID, hostCaption));
                default: return Resources.CoreWebContent.WEBCODE_AK0_6;
            }
        }
        return Resources.CoreWebContent.WEBCODE_VB0_36;
    }
        
    [WebMethod(EnableSession = false)]
    public object GetOrionManagementTasks(string[] plugins, string netObjectId, string returnUrl)
    {
        if (plugins == null || plugins.Length == 0)
        {
            return string.Empty;
        }

        var node = NetObjectFactory.Create(netObjectId);
        
        var pageHolder = new Page();
        var managementTasksItems = new List<ManagementTaskItem>();
        
        // get management tasks from plugins
        foreach (var plugin in plugins)
        {
            try
            {
                var taskPlugin = (IManagementTasksPlugin)pageHolder.LoadControl(plugin);
                var tasks = taskPlugin.GetManagementTasks(node, returnUrl);
                managementTasksItems.AddRange(tasks);
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Error when loading plugin '{0}'. Exception: {1}", plugin, ex);
            }
        }
        
        var tasksBySections = managementTasksItems.GroupBy(task => task.Section).Select(g => new {Title = g.Key.ToUpper(), Tasks = g.ToList()});
        return tasksBySections;
    }

    [WebMethod(EnableSession = false)]
    public string GetDowntimeData(string netObject, DateTime startDate, DateTime finalDate, int hoursPerBlock)
    {
        return new NetObjectDowntimeProvider().GetJsonDowntimeData(netObject, startDate, finalDate, hoursPerBlock);
    }

    [WebMethod(EnableSession = false)]
    public string GetDowntimeStatusLegend(string objectType)
    {
        return new NetObjectDowntimeProvider().GetJSONStatusData(objectType);
    }
}
    
