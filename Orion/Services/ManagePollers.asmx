﻿<%@ WebService Language="C#" Class="ManagePollers" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Enums;
using SolarWinds.Orion.Core.Common.Interfaces.ManagePollers;
using SolarWinds.Orion.Core.Common.Models.ManagePollers;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.Models.Actions;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.Model;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ManagePollers  : WebService
{
    private static readonly Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    /// <summary>
    /// Gets the list of local device pollers.
    /// </summary>
    [WebMethod]
    public PageableDataTable GetLocalPollers(string property, string value, string search)
    {
        AuthorizationChecker.AllowAdmin();

        int totalRows;
        var pagingParams = new PagingRequestParams(Context);
        var helper = new ManagePollersHelper();
        var gridParams = new GridParameters
        {
            GroupProperty = property,
            GroupValue = value,
            PagingParams = pagingParams,
            SearchValue = search
        };
        var technologies = helper.GetTechnologiesFiltered(gridParams, out totalRows);

        var data = ConvertTechnologiesToDataTable(technologies);

        return new PageableDataTable(data, totalRows);
    }

    [WebMethod]
    public PageableDataTable GetLocalPollersGroupValues(string property, string value, string search)
    {
        AuthorizationChecker.AllowAdmin();

        if (string.IsNullOrEmpty(property))
            return null;

        var helper = new ManagePollersHelper();
        var groupByValues = helper.GetTechnologiesGroupBy(property, search);
        var table = new DataTable();
        table.Columns.Add("Value");
        table.Columns.Add("Cnt");

        foreach (var groupByValue in groupByValues)
        {
            table.Rows.Add(groupByValue.Key, groupByValue.Value);
        }

        return new PageableDataTable(table, table.Rows.Count);
    }

    [WebMethod(EnableSession = true)]
    public ActionResult PerformAction(string action, IEnumerable<string> pollerIds, Dictionary<string,object> parameters)
    {
        AuthorizationChecker.AllowAdmin();

        if (action == null)
            throw new ArgumentNullException("action");
        if (pollerIds == null)
            throw new ArgumentNullException("pollerIds");
        if (parameters == null)
            throw new ArgumentNullException("parameters");

        var pollersArray = pollerIds.ToArray();
        if (pollersArray.Length == 0)
        {
            return new ActionResult {Success = true};
        }
        var plugins = pollersArray
            .Select(p => new KeyValuePair<IManagePollersToolbarPlugin, string>(ManagePollersHelper.GetManagePollersPluginWithType<IManagePollersToolbarPlugin>(p), p))
            .Where(p => p.Key != null)
            .GroupBy(k => k.Key, v => v.Value)
            .ToDictionary(k => k.Key, v => v);

        var helper = new ManagePollersHelper();
        string redirectUrl = string.Empty;
        bool success;
        if (plugins.Count() > 1)
        {
            log.InfoFormat("More that one plugin fit! Poller ids: {0}, plugin count: {1}", string.Join(",", pollersArray), plugins.Count);
            success = plugins.Aggregate(true, (current, kvp) => current & helper.PerformAction(action, kvp.Key, parameters, kvp.Value.ToArray(), out redirectUrl));
        }
        else if (plugins.Count == 1)
        {
            success = helper.PerformAction(action, plugins.First().Key, parameters, pollersArray, out redirectUrl);
        }
        else
        {
            success = false;
        }
        return new ActionResult { Success = success, RedirectUrl = redirectUrl };

    }

    [WebMethod(EnableSession = true)]
    public ActionResult FinishImport(Dictionary<string, object> parameters)
    {
        AuthorizationChecker.AllowAdmin();

        if (parameters == null)
            throw new ArgumentNullException("parameters");

        var redirectUrl = string.Empty;
        var suitablePlugins = ManagePollersHelper.GetManagePollersPluginsWithType<IManagePollersToolbarPlugin>()
            .Where(p => p.SupportedActions.Contains(ToolbarButtonEnum.Import))
            .ToArray();
        if (suitablePlugins.Length == 0)
        {
            return new ActionResult { Success = false, RedirectUrl = redirectUrl };
        }
        foreach (var suitablePlugin in suitablePlugins)
        {
            var result = suitablePlugin. PerformAction(ToolbarButtonEnum.Import, null, parameters, out redirectUrl);
            if (result)
            {
                return new ActionResult { Success = false, RedirectUrl = redirectUrl };
            }
        }

        return new ActionResult { Success = false, RedirectUrl = redirectUrl };
    }

    #region assign page services
    [WebMethod]
    public int[] EnableDisableTechnology(string technologyPollingId, bool status, string property, string value, string search)
    {
        AuthorizationChecker.AllowAdmin();

        return EnableDisableTechnologyPollingAssignment(technologyPollingId, status, pollerId =>
        {
            var pagingParams = new PagingRequestParams(Context);
            var helper = new ManagePollersHelper();
            var gridParams = new GridParameters
            {
                GroupProperty = property,
                GroupValue = value,
                PagingParams = pagingParams,
                SearchValue = search
            };
            return helper.GetNetObjectIdsWithPollerStatus(pollerId, gridParams, false).ToArray();
        });
    }

    [WebMethod]
    public int[] EnableDisableTechnologyForNetObjects(string technologyPollingId, bool status, int[] objectIds)
    {
        AuthorizationChecker.AllowAdmin();

        return EnableDisableTechnologyPollingAssignment(technologyPollingId, status, _ => objectIds);
    }

    private int[] EnableDisableTechnologyPollingAssignment(string technologyPollingId, bool status, Func<string, int[]> objectIds)
    {
        using (var proxy = _blProxyCreator.Create(HandleError))
        {
            return proxy.EnableDisableTechnologyPollingAssignmentOnNetObjects(technologyPollingId, status, objectIds(technologyPollingId));
        }
    }

    [WebMethod]
    public PageableDataTable GetNodesWithPollerStatus(string property, string value, string search)
    {
        AuthorizationChecker.AllowAdmin();

        var queryString = Context.Request.QueryString;
        var pollerId = queryString["pollerId"];
        if (string.IsNullOrEmpty(pollerId))
        {
            throw new InvalidOperationException("Poller ID was not provided to the method. Operation cannot continue.");
        }

        var pagingParams = new PagingRequestParams(Context);
        var helper = new ManagePollersHelper();
        var gridParams = new GridParameters
        {
            GroupProperty = property,
            GroupValue = value,
            PagingParams = pagingParams,
            SearchValue = search
        };
        var data = helper.GetNodesWithPollerStatus(pollerId, gridParams);

        int totalRows = (data != null && data.ExtendedProperties.ContainsKey("TotalRows")) ? Convert.ToInt32(data.ExtendedProperties["TotalRows"]) : 0;
        return new PageableDataTable(data, totalRows);
    }

    [WebMethod]
    public PageableDataTable GetNodesWithPollerStatusGroupValues(string property, string value, string search)
    {
        AuthorizationChecker.AllowAdmin();

        //[No grouping] option was probably selected, client should not send such request, but if it happens we return empty list
        if (string.IsNullOrWhiteSpace(property))
        {
            return new PageableDataTable(new DataTable(), 0);
        }

        var queryString = Context.Request.QueryString;
        var pollerId = queryString["pollerId"];

        var helper = new ManagePollersHelper();
        DataTable data;

        // we can query Nodes entities directly as the property which we need to get group by values comes from this table only
        if (property.StartsWith("n."))
        {
            var where = helper.BuildWhereClause(search, false);
            if (property.Equals("n.SNMPVersion"))
            {
                if (string.IsNullOrEmpty(where))
                {
                    where = string.Format("WHERE {0} > 0", property);
                }
                else
                {
                    where = string.Format("{0} AND {1} > 0", where, property);
                }
            }

            var query = string.Format(@"
                SELECT {2} as Value, COUNT(n.nodeID) as Cnt
                FROM Orion.Nodes n
                {1}
                GROUP BY {0}
                ORDER BY {0}
                ",
                 property, where, property.StartsWith("n.CustomProperties.") ? String.Format("ISNULL({0},'')", property) : property);

            using (var swis = InformationServiceProxy.CreateV3())
            {
                data = swis.Query(query, new Dictionary<string, object> {
                    {"Search", (object) search ?? DBNull.Value},
                    {"PollerId", (object) pollerId ?? DBNull.Value}
                });
            }
        }
        else
        {
            data = helper.GetNodesGroupingValuesAndCounts(property, pollerId, search);
        }

        return new PageableDataTable(data, data.Rows.Count);
    }

    [WebMethod]
    public dynamic GetScanStatus(string technologyPollingId)
    {
        AuthorizationChecker.AllowAdmin();

        var plugin = ManagePollersHelper.GetManagePollersPluginWithType<IAssignmentsPagePlugin>(technologyPollingId);
        if (plugin == null)
        {
            log.ErrorFormat("There is no plugin for poller with ID {0}", technologyPollingId);
            return null;
        }

        var s = plugin.GetScanStatus(technologyPollingId);

        if (s != null)
        {
            return new
            {
                s.PollerId,
                PollerName = string.Empty,
                s.ScanStatus,
                Processed = s.NotEnabledNetObjects.Count + s.NotSupportedNetObjects + s.SupportedNetObjects,
                Total = s.TotalNetObjects,
                ConflictNetObjects = s.NotEnabledNetObjects,
                s.NetObjectType,
                Disabled = s.NotEnabledNetObjects.Count,
                Enabled = s.SupportedNetObjects,
                s.CancelPending,
                s.StatusAcknowledged
            };
        }

        return null;
    }

    [WebMethod]
    public dynamic GetPollerScanStatus()
    {
        AuthorizationChecker.AllowAdmin();

        var pollerNames = ManagePollersHelper.PollerNames;
        var plugins = OrionModuleManager.GetManagePollersPlugins().OfType<IAssignmentsPagePlugin>().Where(p => p.PollersScanSupported);

        var statuses = plugins.SelectMany(p => p.GetScanStatuses(false)).ToList();

        // return statuses just for pollers which exists in the system
        return statuses
            .Where(s => pollerNames.ContainsKey(s.PollerId))
            .Select(s => new
            {
                s.PollerId,
                PollerName = pollerNames[s.PollerId],
                s.ScanStatus,
                Processed = s.NotEnabledNetObjects.Count + s.NotSupportedNetObjects + s.SupportedNetObjects,
                Total = s.TotalNetObjects,
                ConflictNetObjects = s.NotEnabledNetObjects,
                s.NetObjectType,
                Disabled = s.NotEnabledNetObjects.Count,
                Enabled = s.SupportedNetObjects,
                s.CancelPending
            });
    }

    [WebMethod]
    public string ScanTechnology(string technologyPollingId, string property, string value, string search)
    {
        AuthorizationChecker.AllowAdmin();

        return ScanTechnologyPolling(technologyPollingId, pollerId =>
        {
            var pagingParams = new PagingRequestParams(Context);
            var helper = new ManagePollersHelper();
            var gridParams = new GridParameters
            {
                GroupProperty = property,
                GroupValue = value,
                PagingParams = pagingParams,
                SearchValue = search
            };
            return helper.GetNetObjectIdsWithPollerStatus(pollerId, gridParams, true).ToArray();
        });
    }

    [WebMethod]
    public string ScanTechnologyForNetObjects(string technologyPollingId, int[] objectIds)
    {
        AuthorizationChecker.AllowAdmin();

        return ScanTechnologyPolling(technologyPollingId, _ => objectIds);
    }

    [WebMethod]
    public string CancelPollerScan(string technologyPollingId)
    {
        AuthorizationChecker.AllowAdmin();

        var plugin = ManagePollersHelper.GetManagePollersPluginWithType<IAssignmentsPagePlugin>(technologyPollingId);
        if (plugin == null)
        {
            log.ErrorFormat("There is no plugin for poller with ID {0}", technologyPollingId);
            return BuildSerializedJsonResponse(false, string.Format(CoreWebContent.WEBDATA_PS1_10, technologyPollingId));
        }

        plugin.ControlScan(technologyPollingId, ScanOperationEnum.Cancel);
        return BuildSerializedJsonResponse(true, string.Empty);
    }

    [WebMethod]
    public string AcknowledgeScanStatus(string technologyPollingId)
    {
        AuthorizationChecker.AllowAdmin();

        var plugin = ManagePollersHelper.GetManagePollersPluginWithType<IAssignmentsPagePlugin>(technologyPollingId);
        if (plugin == null)
        {
            log.ErrorFormat("There is no plugin for poller with ID {0}", technologyPollingId);
            return BuildSerializedJsonResponse(false, string.Format(CoreWebContent.WEBDATA_PS1_10, technologyPollingId));
        }

        plugin.ControlScan(technologyPollingId, ScanOperationEnum.Acknowledge);
        return BuildSerializedJsonResponse(true, string.Empty);
    }

    #endregion

    private DataTable ConvertTechnologiesToDataTable(IEnumerable<ManagePollersGridLine> lines)
    {
        var table = new DataTable();
        table.Columns.Add("Technology");
        table.Columns.Add("Id");
        table.Columns.Add("Name");
        table.Columns.Add("Assignments");
        table.Columns.Add("Author");
        table.Columns.Add("Vendor");
        table.Columns.Add("Enabled");
        table.Columns.Add("VendorIcon");
        table.Columns.Add("AuthorCls");
        table.Columns.Add("AvailableOperations");
        table.Columns.Add("AssignmentNetOjectDisplayName");

        var serializer = new JavaScriptSerializer();

        foreach (var line in lines)
        {
            var availableOperationsStr = serializer.Serialize(line.AvailableOperations.Select(p => p.ToString()));
            table.Rows.Add(line.Technology, line.PollerId, Server.HtmlDecode(line.PollerName), line.AssignmentsCount, Server.HtmlDecode(line.Author),
                Server.HtmlDecode(line.Vendor), line.ScanNewNodes, "", line.AuthorCls, availableOperationsStr,
                line.AssignmentNetOjectDisplayName);
        }

        return table;
    }

    private void HandleError(Exception ex)
    {
        log.Error(ex);
        throw ex;
    }

    /// <summary>
    /// Performs discovery on netobjects retrieved with function <paramref name="netObjectIdsBytechnologyPollingId"/> for given <paramref name="technologyPollingId"/>.
    /// </summary>
    /// <param name="technologyPollingId">The device poller ID.</param>
    /// <param name="netObjectIdsBytechnologyPollingId">Function which takes poller ID and returns list of netobjects IDs.</param>
    /// <returns>Result of device poller enqueue attempt.</returns>
    private string ScanTechnologyPolling(string technologyPollingId, Func<string, IEnumerable<int>> netObjectIdsBytechnologyPollingId)
    {
        var plugin = ManagePollersHelper.GetManagePollersPluginWithType<IAssignmentsPagePlugin>(technologyPollingId);
        if (plugin == null)
        {
            log.ErrorFormat("There is no plugin for poller with ID {0}", technologyPollingId);
            return BuildSerializedJsonResponse(false, string.Format(CoreWebContent.WEBDATA_PS1_10, technologyPollingId));
        }

        if (!plugin.CanEnqueue(technologyPollingId))
        {
            log.InfoFormat("Scan for poller with ID='{0}' cannot start. Cannot run more than one scan at the time.", technologyPollingId);
            // scan already in progress or pending
            return BuildSerializedJsonResponse(false, CoreWebContent.WEBDATA_PS1_11);
        }

        string netObjectPrefix = ManagePollersHelper.GetNeobjectPrefix(technologyPollingId);

        var netObjectIds = netObjectIdsBytechnologyPollingId(technologyPollingId).ToList();
        if (netObjectIds.Count == 0)
        {
            // no nodes to scan
            log.InfoFormat("Scan for device poller '{0}' cannot be scheduled because there are no netobjects to scan on. This may come from fact that netobjects are SNMP nodes, down or unmanaged.", technologyPollingId);
            return BuildSerializedJsonResponse(false, CoreWebContent.WEBDATA_PS1_12);
        }

        var success = plugin.EnqueueScanJob(technologyPollingId, netObjectPrefix, netObjectIds);
        return BuildSerializedJsonResponse(success, string.Empty);
    }

    private static string BuildSerializedJsonResponse(bool success, string message)
    {
        var response = new { Success = success, Message = message };
        return new JavaScriptSerializer().Serialize(response);
    }

    public class ActionResult
    {
        public string RedirectUrl { get; set; }
        public bool Success { get; set; }

    }
}
