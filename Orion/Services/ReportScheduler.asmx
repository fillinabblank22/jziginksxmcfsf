﻿<%@ WebService Language="C#" Class="ReportScheduler" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Security;
using System.Web.Services;
using Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ReportScheduler : WebService
{
    private static readonly Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator(); 
    private static readonly int primaryEngineId = EnginesDAL.GetPrimaryEngineId();

    [WebMethod]
    public PageableDataTable GetReportJobs(string property, string type, string value, string search)
    {
        int pageSize = 0;
        int startRowNumber = 0;

        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        if (pageSize == 0)
            pageSize = 20;

        string sortOrder = "Name";
        if (!string.IsNullOrEmpty(sortColumn))
        {
            sortOrder = string.Format("{0} {1}",
                                      (sortColumn.Equals("reportsdata", StringComparison.OrdinalIgnoreCase)) ? "ReportTitles" : sortColumn,
                                      sortDirection);
        }
        var parameters = GetQueryConditionsParameters(property, type, value, search);
        string whereCondition = parameters.Item1;
        Dictionary<string, object> queryParams = parameters.Item2;
        string queryString = String.Format(@"SELECT  rj.ReportJobID, 
	                                              rj.Name, 
	                                              rj.Description, 
	                                              rj.Enabled,
                                                  ToLocal(rj.LastRun) as LastRun,
                                                  rj.FrequencyTitle,
                                                  rj.ActionsData,
                                                  rj.ActionTitles,
                                                  rj.ReportsData,
                                                  rj.UrlData,
                                                  COUNT(rjd.ReportID) as AssignedReports
                                          FROM orion.reportjobs (nolock=true) rj
                                          LEFT JOIN Orion.ReportJobDefinitions (nolock=true) rjd ON rj.ReportJobID = rjd.ReportJobID  
                                          LEFT JOIN Orion.ReportJobUrls (nolock=true) rju ON rj.ReportJobID = rju.ReportJobID                                        
                                          {0}
                                          GROUP BY rj.ReportJobID, rj.Name, rj.Description, rj.Enabled, rj.LastRun, rj.FrequencyTitle, rj.ActionsData, rj.ActionTitles, rj.ReportsData, rj.UrlData
                                          ORDER BY {1}
                                          WITH ROWS @startRowNumber TO @endRowNumber",
                                        whereCondition, sortOrder);
        queryParams[@"startRowNumber"] = startRowNumber + 1;
        queryParams[@"endRowNumber"] = startRowNumber + pageSize;

        string countQuery = string.Format(@"SELECT Count(rj.ReportJobId) AS jobsCount
                              FROM orion.reportjobs rj {0}", whereCondition);

        DataTable rjDataTable;
        DataTable countTable;

        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                rjDataTable = service.Query(queryString, queryParams);
                countTable = service.Query(countQuery, queryParams);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

        return new PageableDataTable(rjDataTable, (countTable.Rows == null || countTable.Rows.Count == 0 ? 0 : (int)(countTable.Rows[0][0])));
    }

    
    private Tuple<string, Dictionary<string, object>> GetWhereConditionWithParameters(string property, string type, string value)
    {
        var queryParams = new Dictionary<string, object>();
        string whereCondition = "WHERE 1=1";

        if (!string.IsNullOrEmpty(property) && !string.IsNullOrEmpty(value))
        {
            if (property.Equals("reportsdata", StringComparison.OrdinalIgnoreCase))
            {
                if (value.Equals("0", StringComparison.OrdinalIgnoreCase))
                {
                    whereCondition += String.Format(" AND (rj.{0} = '[]')", property);
                }
                else
                {
                    whereCondition += String.Format(" AND (rj.ReportsData LIKE '%ID\":'+ @value +'%')");
                    queryParams[@"value"] = value;
                }
            }
            else if (property.Equals("action", StringComparison.OrdinalIgnoreCase))
            {
                whereCondition += String.Format(" AND (rj.ActionsData LIKE '%ActionTypeID\":\"' + @value +'\"%')");
                queryParams[@"value"] = value;
            }
            else if (value.Equals("null", StringComparison.OrdinalIgnoreCase))
                whereCondition += String.Format(" AND (rj.{0} IS NULL)", property);
            else
            {
                whereCondition += String.Format(" AND (rj.{0}=@value)", property);
                queryParams[@"value"] = value;
            }
        }
        return new Tuple<string, Dictionary<string, object>>(whereCondition, queryParams);
    }

    private Tuple<string, Dictionary<string, object>> GetQueryConditionsParameters(string property, string type, string value, string search)
    {
        var whereClause = GetWhereConditionWithParameters(property, type, value);
        var whereCondition = whereClause.Item1;
        var queryParams = whereClause.Item2;
        
        var selectedColumns = WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName,
                                                          "ReportScheduler_SelectedColumns",
                                                          WebConstants.ReportSchedulerDafultSelectedColumns)
                                                          .Split(',').ToList();

        if (!string.IsNullOrEmpty(search) && selectedColumns.Count > 0)
        {
            var searchCondition = new StringBuilder();
            searchCondition.AppendFormat("rj.{0} LIKE @search", selectedColumns[0]);
            for (int i = 1; i < selectedColumns.Count; i++)
            {
                if (selectedColumns[i].Equals("reportsdata", StringComparison.OrdinalIgnoreCase))
                {
                    searchCondition.AppendFormat(" OR rj.ReportTitles LIKE @search");
                }
                else
                {
                    searchCondition.AppendFormat(" OR rj.{0} LIKE @search", selectedColumns[i]);
                }

            }
            if (searchCondition.Length > 0)
                whereCondition += String.Format(" AND ({0})", searchCondition);

            queryParams[@"search"] = search.Replace("[", "[[]");
        }
        return new Tuple<string, Dictionary<string, object>>(whereCondition, queryParams);
    }

    [WebMethod]
    public int GetReportJobNumber(string reportJobName, string property, string type, string value, string sort, string direction)
    {
        string sortColumn = sort;
        string sortDirection = direction;

        string sortOrder = "Name";
        if (!string.IsNullOrEmpty(sortColumn))
        {
            sortOrder = string.Format("{0} {1}", (sortColumn.ToLower().Equals("reportsdata")) ? "ReportTitles" : sortColumn, sortDirection);
        }

        var whereClause = GetWhereConditionWithParameters(property, type, value);
        var whereCondition = whereClause.Item1;
        var queryParams = whereClause.Item2;
        
        string queryString = String.Format(@"SELECT rj.Name FROM orion.reportjobs (nolock=true) rj 
                                          {0} ORDER BY {1}",
                                        whereCondition, sortOrder);

        DataTable rjDataTable;

        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                rjDataTable = service.Query(queryString, queryParams);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

        if (rjDataTable == null || rjDataTable.Rows == null || rjDataTable.Rows.Count == 0)
            return -1;

        int rowId = 0;
        foreach (DataRow row in rjDataTable.Rows)
        {
            if (Convert.ToString(row["Name"]) == reportJobName)
                return rowId;
            rowId++;
        }
        return -1;
    }

    [WebMethod]
    public PageableDataTable GetSchedulesGroupValues(string property, string type)
    {
        if (String.IsNullOrWhiteSpace(property))
            return null;

        // 3 possible properties:
        // Action(print, mail, saveToDisk), ReportsAssigned(list of reprots used in this schedule), Status(on, off)

        string groupingQuery;
        string countQuery;

        if (property.Equals("ReportsData"))
        {
            groupingQuery = string.Format(@"SELECT IsNull(rjd.ReportID, 0) as Value, IsNull(r.Title, '{0}') as DisplayNamePlural, COUNT(rj.ReportJobId) as Cnt 
                              FROM Orion.ReportJobs (nolock=true) rj 
                              LEFT JOIN Orion.ReportJobDefinitions (nolock=true) rjd ON rj.ReportJobID = rjd.ReportJobID
                              LEFT JOIN Orion.Report (nolock=true) r ON r.ReportID = rjd.ReportId
                              GROUP BY IsNull(rjd.ReportID, 0), r.Title
                              ORDER BY r.Title", CoreWebContent.WEBCODE_IB0_12);
        }
        else if (property.Equals("Action"))
        {
            groupingQuery = @"SELECT a.ActionTypeID as Value, a.ActionTypeID as DisplayNamePlural, COUNT(DISTINCT rj.ReportJobID) as Cnt 
                              FROM Orion.ActionsAssignments (nolock=true) aa
                              INNER JOIN Orion.Actions (nolock=true) a ON aa.ActionID = a.ActionID AND aa.EnvironmentType='Reporting'
                              INNER JOIN Orion.ReportJobs (nolock=true) rj ON aa.ParentID = rj.ReportJobID
                              GROUP BY a.ActionTypeID
                              ORDER BY a.ActionTypeID";
        }
        else
        {
            groupingQuery =
                String.Format(@"SELECT r.{0} as Value, r.{0} as DisplayNamePlural, COUNT(r.ReportJobId) as Cnt 
                                            FROM Orion.ReportJobs (nolock=true) r 
                                            GROUP BY r.{0}
                                            ORDER BY r.{0}
                                            ", property);
        }
        countQuery = "SELECT  COUNT(ReportJobId) as Cnt FROM Orion.ReportJobs (nolock=true)";

        DataTable cpDataTable;
        DataTable countTable;

        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                cpDataTable = service.Query(groupingQuery);
                countTable = service.Query(countQuery);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

        DataTable dt = new DataTable();

        dt.Columns.Add("Value");
        dt.Columns.Add("DisplayNamePlural");
        dt.Columns.Add("Cnt");

        dt.Rows.Add("[All]", "[All]", countTable.Rows[0][0]);

        foreach (DataRow row in cpDataTable.Rows)
        {
            if (property.Equals("Action", StringComparison.OrdinalIgnoreCase))
                row["DisplayNamePlural"] = GetLocalizedProperty("ActionType", row["DisplayNamePlural"].ToString());

            dt.Rows.Add(row["Value"], row["DisplayNamePlural"], row["Cnt"]);
        }

        return new PageableDataTable(dt, dt.Rows.Count);
    }

    [WebMethod]
    public bool EnableDisableJob(string jobId, string value)
    {
        var valid = true;
        if (Convert.ToBoolean(value))
        {
            valid = ReportJobDAL.ValidateJob(jobId);
        }
        if (valid)
        {
            using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, primaryEngineId))
            {
                businessProxy.ChangeReportJobStatus(Convert.ToInt16(jobId), Convert.ToBoolean(value));
            }
            return true;
        }

        return false;
    }

    [WebMethod]
    public PageableDataTable GetAllSelectedJobs(string property, string type, string value, string search)
    {
        var parameters = GetQueryConditionsParameters(property, type, value, search);
        string whereCondition = parameters.Item1;
        Dictionary<string, object> queryParams = parameters.Item2;

        string selectQuery = string.Format(@"SELECT rj.ReportJobId, rj.Name FROM orion.reportjobs (nolock=true) rj {0}", whereCondition);

        DataTable rjDataTable;

        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                rjDataTable = service.Query(selectQuery, queryParams);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }
        return new PageableDataTable(rjDataTable, rjDataTable.Rows.Count);
    }

    [WebMethod]
    public string RemoveReportJobs(List<string> jobsIds)
    {
        AuthorizationChecker.AllowReportManagement();

        using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, primaryEngineId))
        {
            businessProxy.DeleteReportJobs(jobsIds.Select(int.Parse).ToList());
        }

        return String.Empty;
    }

    [WebMethod]
    public ReportTuple DublicateReportJob(string jobsId, string jobName)
    {
        AuthorizationChecker.AllowReportManagement();
        int result;
        string newJobName = string.Empty;
        try
        {
            newJobName = GetUniqueNameForReportJob(jobName);
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

        using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, primaryEngineId))
        {
            result = businessProxy.DublicateReportJob(Convert.ToInt32(jobsId), newJobName, ReportJobDAL.GetLimitedReportIds());
        }

        return new ReportTuple { ID = result, Title = newJobName };
    }

    [WebMethod]
    public List<ReportJobConfiguration> GetReportAssignedSchedules(string reportId)
    {
        using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, primaryEngineId))
        {
            return businessProxy.GetSchedulesWithReport(Convert.ToInt16(reportId));
        }
    }

    [WebMethod]
    public bool UpdateScheduleAssignments(List<string> schedulesIds, List<string> oldReportIds, List<string> newReportIds)
    {
        try
        {
            using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, primaryEngineId))
            {
                businessProxy.AssignJobsToReports(newReportIds.Select(int.Parse).ToList(),
                                                  schedulesIds.Select(int.Parse).ToList());

                // in case we have only one schedule selected than we're also removing old reports
                if (schedulesIds.Count == 1)
                {
                    var itemsToRemove = oldReportIds.Where(item => !newReportIds.Contains(item)).ToList();
                    if (itemsToRemove.Count() > 0)
                    {
                        businessProxy.UnAssignReportsFromJob(int.Parse(schedulesIds[0]),
                                                             itemsToRemove.Select(int.Parse).ToList());
                    }
                }
            }
            return true;

        }
        catch (Exception ex)
        {
            log.Error("Update schedule assignments failed.", ex);
            throw;
        }
    }

    [WebMethod]
    public bool AssignSchedules(List<string> reportIds, List<string> schedulesIds)
    {
        if (reportIds.Count == 1)
        {
            using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, primaryEngineId))
            {
                businessProxy.AssignJobsToReport(Convert.ToInt32(reportIds[0]),
                                                schedulesIds.Select(int.Parse).ToList());
            }
        }
        else
        {
            using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, primaryEngineId))
            {
                businessProxy.AssignJobsToReports(reportIds.Select(int.Parse).ToList(),
                                                schedulesIds.Select(int.Parse).ToList());
            }
        }
        return true;
    }

    [WebMethod]
    public bool UnassignReport(List<string> jobIds, string reportId)
    {
        using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, primaryEngineId))
        {
            businessProxy.AssignJobsToReport(Convert.ToInt32(reportId), jobIds.Select(int.Parse).ToList());
        }
        return true;
    }

    [WebMethod]
    public bool TestCredentials(string name, string password)
    {
        try
        {
            if (!string.IsNullOrEmpty(name))
            {
                //forms login
                if ((name.Contains("\\")) || (name.Contains("@"))) //domain login (*forms)
                {
                    try
                    {
                        //Get Windows identity (from username/password combo)
                        WindowsIdentity logonIdentity = OrionMixedModeAuth.GetIdentity(name, password);
                        //Check the Identity with AuthorizationManager
                        return (AuthorizationManager.AuthorizeUser(logonIdentity));
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }

                }
                else //normal forms login
                {
                    return (Membership.ValidateUser(name, password));
                }

            }
            return false;
        }
        catch (Exception ex)
        {
            log.Debug("Test user credentials failed", ex);
            return false;
        }
    }

    private string GetUniqueNameForReportJob(string oldName)
    {
        var allNames = ReportHelper.GetSimilarExistingReportJobNames(oldName);
        string newName = string.Format(CoreWebContent.WEBDATA_TM0_299, oldName);
        int counter = 2;
        while (allNames.Any(name => name.Equals(newName)))
        {
            newName = string.Format(CoreWebContent.WEBDATA_TM0_300, counter, oldName);
            counter++;
        }

        return newName;
    }

    private string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }

    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }
}
