﻿<%@ WebService Language="C#" Class="ReportChartDataWebService" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.Federation;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.Reporting;
using SolarWinds.Orion.Web.Reporting.ReportCharts;

/// <summary>
/// Summary description for ReportChartData
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class ReportChartDataWebService : WebService
{
    private const int DefaultLimitOfSeries = 10;
    private readonly ReportChartDataSeriesProvider _seriesProvider;
    private readonly ReportChartDataProvider _dataProvider;
    private readonly Log _log = new Log(); 
    
    
    public ReportChartDataWebService()
    {
        _seriesProvider = new ReportChartDataSeriesProvider();
        _dataProvider = new ReportChartDataProvider(InformationServiceProxy.CreateV3Creator());
    }

    [WebMethod]
    public ChartDataResults LoadData(ReportChartDataRequest request)
    {
        var resultDataSeries = Enumerable.Empty<DataSeries>();
        
        try
        {
            var ctx = new ReportChartContext
            {
                ChartConfiguration = request.ChartConfiguration,
                DataSource = request.DataSource.ApplyQueryContextIfPresent(),
                TimeFrame = request.TimeFrame
            };

            if (request.ViewLimitationId > 0 && !HttpContext.Current.Items.Contains(typeof(ViewInfo).Name))
            {
                var viewInfo = new ViewInfo("", "", "", false, false, typeof (Node))
                                   {LimitationID = request.ViewLimitationId};
                HttpContext.Current.Items[typeof(ViewInfo).Name] = viewInfo;
            }
            
            _dataProvider.Initialize(ctx);
            resultDataSeries = _seriesProvider.CreateDataSeries(request.ChartConfiguration, _dataProvider);
        }
        catch (Exception e)
        {
            _log.Error("Unexpected error occurs on loading data for report chart", e);
        }
        
        var result = new ChartDataResults(resultDataSeries);
        
        result = ApplyLimitOnNumberOfSeries(result);

        result.SwisfErrorControlHtml = GenerateSwisfErrorControlHtml();
        
        return result;
    }

    private string GenerateSwisfErrorControlHtml()
    {
        return BaseSwisfErrorControl.GetSwisfErrorControlHtmlIfFederationEnabledOrEmptyString(_dataProvider.GetSwisfErrors(), new Page());
    }
    
    private ChartDataResults ApplyLimitOnNumberOfSeries(ChartDataResults result)
    {
        var allSeries = result.DataSeries.ToArray();
        
        var limit = GetLimitOfSeries();
        
        if (limit >= allSeries.Length)
        {
            result.AppliedLimitation = false;
        }
        else
        {
            result.AppliedLimitation = true;
            result.DataSeries = allSeries.Take(limit).ToArray();
        }

        return result;
    }

    private int GetLimitOfSeries()
    {
        return GetSetting("Web-MaximalNumberOfSeriesInChart", DefaultLimitOfSeries);
    }

    private static int GetSetting(string name, int defaultValue)
    {
        int value;
        try
        {
            value = (int)(SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting(name).SettingValue);
        }
        catch (Exception)
        {
            value = defaultValue;
        }

        return value;
    }
}