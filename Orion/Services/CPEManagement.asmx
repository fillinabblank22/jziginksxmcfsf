﻿<%@ WebService Language="C#" Class="CPEManagement" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Alerting.Plugins.Conditions.Dynamic;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Core.Common.InformationService;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;
using InformationServiceProxy = SolarWinds.Orion.Web.InformationService.InformationServiceProxy;
using SwisEntityHelper = SolarWinds.Orion.Web.SwisEntityHelper;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class CPEManagement : WebService
{
    private static readonly Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    private const string AllValue = "[All]";
    private const string UsagePropertyName = "Usage";
    private static readonly string[] RequaredProperties = new[] {"Uri", "Status", "DisplayName"};

    public class CPEManagementError : Exception
    {
        protected CPEManagementError(string message, Exception inner) : base(message, inner) { }
        protected CPEManagementError(string message) : base(message) { }
    }

    public class BadFilterError : CPEManagementError
    {
        internal BadFilterError(string message, Exception inner) : base(message, inner) { }
    }

    public class IncorrectNetObjectIdError : CPEManagementError
    {
        internal IncorrectNetObjectIdError(string message) : base(message) { }
    }

    private static string GetLimitationDescription(string table, string field, string limitationDescription)
    {
        //o	City for nodes is used to define an account limitation.
        return string.Format(Resources.CoreWebContent.WEBCODE_SO0_40, field, table, GetDescriptionForLimitation(limitationDescription));
    }

    private static string GetDescriptionForLimitation(string type)
    {
        //description of limitation
        switch (type)
        {
            case "AccountLimitation":
                return Resources.CoreWebContent.WEBCODE_SO0_35;
            case "ViewLimitation":
                return Resources.CoreWebContent.WEBCODE_SO0_36;
            case "GroupLimitation":
                return Resources.CoreWebContent.WEBCODE_SO0_37;
            default:
                return string.Format(Resources.CoreWebContent.WEBCODE_SO0_38);
        }
    }

    [WebMethod(EnableSession = true)]
    public string RemoveCP(List<string> ids)
    {
        //group by entities
        if (ids == null)
            return string.Empty;

        Dictionary<string, List<string>> dict = new Dictionary<string, List<string>>();

        foreach (string id in ids)
        {
            string[] pair = id.Split(':');
            if (pair.Length != 2)
            {
                log.Error("Cannot delete property " + id);
                continue;
            }

            if (dict.ContainsKey(pair[0]))
                dict[pair[0]].Add(pair[1]);
            else
            {
                dict[pair[0]] = new List<string> { pair[1] };
            }
        }

        int i = 0;
        Dictionary<string, List<string>> blockedList;
        StringBuilder sb = new StringBuilder(Resources.CoreWebContent.WEBCODE_SO0_39);
        bool anyblocked = false;

        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                foreach (KeyValuePair<string, List<string>> delCPs in dict)
                {
                    var targetEntity = CustomPropertyHelper.GetCustomPropertyTarget(delCPs.Key);

                    blockedList = CustomPropertyHelper.CheckForUsageByTarget(targetEntity, "'" + string.Join("','", delCPs.Value.ToArray()) + "'");

                    foreach (string el in delCPs.Value)
                    {
                        if (blockedList != null && blockedList.ContainsKey(el))
                        {
                            foreach (string desc in blockedList[el])
                                sb.AppendLine(GetLimitationDescription(delCPs.Key, el, desc));

                            anyblocked = true;
                            continue;
                        }

                        log.Debug("Deleting " + el + " custom properties for " + delCPs.Key + " entity");

                        service.Invoke(targetEntity, "DeleteCustomProperty", CustomPropertyHelper.GetArgElements(new[] { el }));
                        i++;
                    }
                }
            }

            if (anyblocked)
                return sb.ToString();

            return string.Empty;
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public bool AreCustomPropertiesUsedForActions(List<string> ids)
    {
        if (ids == null)
            return false;

        var query = new StringBuilder(@"SELECT TOP 1 PropertyValue FROM Orion.Actions AS Actions
INNER JOIN Orion.ActionsProperties ActionsProperties on ActionsProperties.ActionID = Actions.ActionID
WHERE Actions.ActionTypeID = 'CustomProperty' and ActionsProperties.PropertyName = 'CustomPropertyName' and Actions.Properties.PropertyName = 'ActiveObjectType' and ");
        string whereFormat = "Actions.Properties.PropertyValue='{0}' and ActionsProperties.PropertyValue in ({1})";

        var swisSchemaProvider = new SwisSchemaProvider(InformationServiceProxy.CreateV3Creator());
        var entityProvider = new EntityProviderDynamic(swisSchemaProvider);
        try
        {
            var dict = new Dictionary<string, List<string>>();
            foreach (var id in ids)
            {
                var pair = id.Split(':');
                if (pair.Length != 2)
                {
                    log.ErrorFormat("Cannot check property {0} for action", id);
                    continue;
                }
                var objectType = entityProvider.GetObjectTypeByEntityFullName(pair[0]);
                if (string.IsNullOrEmpty(objectType)) continue;

                if (dict.ContainsKey(objectType))
                    dict[objectType].Add(pair[1]);
                else
                {
                    dict[objectType] = new List<string> {pair[1]};
                }
            }

            if (dict.Count == 0)
                return false;

            var firstKey = dict.First().Key;

            foreach (var cps in dict)
            {
                if (!firstKey.Equals(cps.Key))
                    query.Append(" OR ");

                var cpIds = cps.Value.Select(id => string.Format("'{0}'", id));
                query.Append(string.Format(whereFormat, cps.Key, string.Join(",", cpIds)));
            }

            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                var cpDataTable = service.Query(query.ToString());
                if (cpDataTable != null && cpDataTable.Rows.Count > 0)
                    return true;
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
        }

        return false;
    }

    private string GetWhereConditionForManageCPGrid(string property, string type, string value, string search)
    {
        string whereCondition = string.Empty;
        if (!string.IsNullOrEmpty(value))
        {
            if (property == UsagePropertyName)
            {
                whereCondition = "WHERE cp.Usage." + value + "='True'";
            }
            else if (!string.IsNullOrEmpty(property))
            {
                whereCondition = "WHERE cp." + property + "='" + value + "'";
            }
        }

        if (!string.IsNullOrEmpty(search))
        {
            if (!string.IsNullOrEmpty(whereCondition))
            {
                whereCondition += " AND (ISNULL(e.DisplayNamePlural, e.Name) LIKE '" + search + "' OR cp.Field LIKE '" + search + "' OR cp.Description LIKE '" + search + "' OR cp.Default LIKE '" + search + "')";
            }
            else
                whereCondition = "WHERE (ISNULL(e.DisplayNamePlural, e.Name) LIKE '" + search + "' OR cp.Field LIKE '" + search + "' OR cp.Description LIKE '" + search + "' OR cp.Default LIKE '" + search + "')";
        }
        return whereCondition;
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetAllSelectedCustomProperties(string property, string type, string value, string search)
    {
        string whereCondition = GetWhereConditionForManageCPGrid(property, type, value, search);
        List<CustomPropertyUsage> cpUsages = CustomPropertyHelper.GetCustomPropertyUsages().ToList();
        StringBuilder usagesQuery = new StringBuilder();
        foreach (CustomPropertyUsage usage in cpUsages)
        {
            usagesQuery.AppendFormat(", cp.Usage.{0}", usage.Name);
        }

        string selectString = string.Format(@"SELECT e.FullName + ':' +cp.Field as CPID, cp.Table, cp.TargetEntity
FROM Orion.CustomProperty (nolock=true) cp
INNER JOIN Metadata.Relationship (nolock=true) re ON re.TargetType=cp.TargetEntity AND re.SourcePropertyName='CustomProperties' AND re.IsInjected=0 AND re.BaseType = 'System.Hosting'
INNER JOIN Metadata.Entity (nolock=true) e ON e.FullName = re.SourceType 
{0}", whereCondition);

        log.Debug("Custom properties main grid query :" + selectString);
        DataTable cpDataTable;
        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                cpDataTable = service.Query(selectString);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

        return new PageableDataTable(cpDataTable, cpDataTable.Rows.Count);
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetCustomProperties(string property, string type, string value, string search)
    {
        int pageSize = 0;
        int startRowNumber = 0;

        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        if (pageSize == 0)
            pageSize = 20;

        string sortOrder = "cp.Table ASC ";
        if (!string.IsNullOrEmpty(sortColumn))
        {
            if (sortColumn.Equals("DisplayNamePlural", StringComparison.OrdinalIgnoreCase))
                sortOrder = string.Format("ISNULL(e.DisplayNamePlural, e.Name) {0}", sortDirection);
            else
                sortOrder = string.Format("cp.{0} {1}", sortColumn, sortDirection);
        }

        string whereCondition = GetWhereConditionForManageCPGrid(property, type, value, search);

        List<CustomPropertyUsage> cpUsages = CustomPropertyHelper.GetCustomPropertyUsages().ToList();
        StringBuilder usagesQuery = new StringBuilder();
        foreach (CustomPropertyUsage usage in cpUsages)
        {
            usagesQuery.AppendFormat(", cp.Usage.{0}", usage.Name);
        }

        string queryString = string.Format(@"SELECT e.FullName + ':' +cp.Field as CPID, cp.Table, cp.Field, cp.DataType, cp.MaxLength, cp.Description, 
       ISNULL(e.DisplayNamePlural, cp.Table) as DisplayNamePlural, cp.TargetEntity, e.FullName, cp.Mandatory, cp.Default, '' AS Usage {4} 
FROM Orion.CustomProperty (nolock=true) cp
INNER JOIN Metadata.Relationship (nolock=true) re ON re.TargetType=cp.TargetEntity AND re.SourcePropertyName='CustomProperties' AND re.IsInjected=0 AND re.BaseType = 'System.Hosting'
INNER JOIN Metadata.Entity (nolock=true) e ON e.FullName = re.SourceType 
{0} 
ORDER BY {1}
WITH ROWS {2} TO {3}
", whereCondition, sortOrder,
                startRowNumber + 1,
                startRowNumber + pageSize,
                usagesQuery
                );

        string countString = string.Format(@"SELECT COUNT(cp.Field) as Cnt 
FROM Orion.CustomProperty (nolock=true) cp
INNER JOIN Metadata.Relationship (nolock=true) re ON re.TargetType=cp.TargetEntity AND re.SourcePropertyName='CustomProperties' AND re.IsInjected=0 AND re.BaseType = 'System.Hosting'
INNER JOIN Metadata.Entity (nolock=true) e ON e.FullName = re.SourceType 
{0}", whereCondition);

        log.Debug("Custom properties main grid query :" + queryString);
        log.Debug("Custom properties main grid count query :" + countString);

        DataTable cpDataTable;
        DataTable countTable;

        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                cpDataTable = service.Query(queryString);
                countTable = service.Query(countString);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

        foreach (DataRow row in cpDataTable.Rows)
        {
            StringBuilder usagesStr = new StringBuilder();

            foreach (CustomPropertyUsage usage in cpUsages)
            {
                if ((bool) row[usage.Name])
                {
                    if (usagesStr.Length > 0)
                    {
                        usagesStr.Append(", ");
                    }
                    usagesStr.Append(usage.Label);
                }
            }

            row["Usage"] = usagesStr.ToString();
        }

        return new PageableDataTable(cpDataTable, (countTable.Rows == null || countTable.Rows.Count == 0 || countTable.Rows[0][0] == DBNull.Value ? 0 : (int)(countTable.Rows[0][0])));
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetCustomPropertiesGroupValues(string property, string type, string value, string search)
    {
        if (string.IsNullOrEmpty(property))
            return null;

        DataTable cpDataTable;

        if (property == UsagePropertyName)
        {
            cpDataTable = GetUsageGroups(search);
        }
        else
        {
            string whereCondition = string.Empty;

            if (!string.IsNullOrEmpty(property) && !string.IsNullOrEmpty(value))
                whereCondition = "Where cp." + property + "='" + value + "'";

            if (!string.IsNullOrEmpty(search))
            {
                if (!string.IsNullOrEmpty(whereCondition))
                {
                    whereCondition += " AND (ISNULL(e.DisplayNamePlural, cp.Table) LIKE '" + search +
                                      "' OR cp.Field LIKE '" + search + "')";
                }
                else
                    whereCondition = "Where (ISNULL(e.DisplayNamePlural, cp.Table) LIKE '" + search +
                                     "' OR cp.Field LIKE '" + search + "')";
            }

            string queryString = string.Format(@"
       Select '[All]' as Value, '[All]' as DisplayNamePlural, COUNT({0}) as Cnt FROM Orion.CustomProperty (nolock=true) cp
        INNER JOIN Metadata.Relationship (nolock=true) r ON cp.TargetEntity = r.TargetType AND r.IsInjected=0 AND r.BaseType = 'System.Hosting'
        LEFT JOIN Metadata.Entity (nolock=true) e ON e.Type = r.SourceType 
        {1}
        Union ALL
       (SELECT cp.{0} as Value, {2} as DisplayNamePlural, COUNT(cp.{0}) as Cnt FROM Orion.CustomProperty (nolock=true) cp
        INNER JOIN Metadata.Relationship (nolock=true) r ON cp.TargetEntity = r.TargetType AND r.IsInjected=0 AND r.BaseType = 'System.Hosting'
        LEFT JOIN Metadata.Entity (nolock=true) e ON e.Type = r.SourceType 
                {1} Group by cp.{0})",
                                     property,
                                     whereCondition,
                                    (property.Equals("Table", StringComparison.OrdinalIgnoreCase))
                                        ? "ISNULL(e.DisplayNamePlural, cp.Table)"
                                        : string.Format("cp.{0}", property));

            log.Debug("Custom properties main grid group query: " + queryString);

            try
            {
                InformationServiceProxy service = InformationServiceProxy.CreateV3();
                cpDataTable = service.Query(queryString);
            }
            catch (Exception ex)
            {
                log.Error(ex);
                throw;
            }
        }

        return new PageableDataTable(cpDataTable, cpDataTable.Rows.Count);
    }

    private static DataTable GetUsageGroups(string search)
    {
        string whereCondition = string.Empty;
        if (!string.IsNullOrEmpty(search))
        {
            whereCondition = "AND (ISNULL(e.DisplayNamePlural, cp.Table) LIKE '" + search +
                                "' OR cp.Field LIKE '" + search + "')";
        }

        DataTable cpDataTable = new DataTable();
        cpDataTable.Columns.Add("Value", typeof (string));
        cpDataTable.Columns.Add("DisplayNamePlural", typeof (string));
        cpDataTable.Columns.Add("Cnt", typeof (int));

        StringBuilder countQuery = new StringBuilder();
        countQuery.AppendFormat(@"SELECT COUNT(Table) AS UsageCount, '{0}' AS UsageName FROM Orion.CustomProperty (nolock=true) cp
        INNER JOIN Metadata.Relationship (nolock=true) r ON cp.TargetEntity = r.TargetType
        LEFT JOIN Metadata.Entity (nolock=true) e ON e.Type = r.SourceType 
        WHERE 1=1 {1}", AllValue, whereCondition);

        foreach (CustomPropertyUsage usage in CustomPropertyHelper.GetCustomPropertyUsages())
        {
            countQuery.AppendFormat(@" UNION ALL
                    (SELECT COUNT(Table) AS UsageCount, '{0}' AS UsageName FROM Orion.CustomProperty (nolock=true) cp
                    INNER JOIN Metadata.Relationship (nolock=true) r ON cp.TargetEntity = r.TargetType
                    LEFT JOIN Metadata.Entity (nolock=true) e ON e.Type = r.SourceType 
                    WHERE cp.Usage.{0} = 'True' {1})",
                                    usage.Name, whereCondition);
        }

        try
        {
            Dictionary<string, int> counts = new Dictionary<string, int>();
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                DataTable countDt = service.Query(countQuery.ToString());
                foreach (DataRow row in countDt.Rows)
                {
                    counts[(string) row["UsageName"]] = (int) row["UsageCount"];
                }
            }

            int count;
            counts.TryGetValue(AllValue, out count);
            cpDataTable.Rows.Add(AllValue, "[All]", count);

            foreach (CustomPropertyUsage usage in CustomPropertyHelper.GetCustomPropertyUsages())
            {
                counts.TryGetValue(usage.Name, out count);
                cpDataTable.Rows.Add(usage.Name, usage.Label, count);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

        return cpDataTable;
    }

    [WebMethod(EnableSession = true)]
    public IList<PredefinedCustomProperty> GetCustomPropertyTemplates(string targetEntity, bool includeAdvanced)
    {
        var list = new List<PredefinedCustomProperty> { new PredefinedCustomProperty { Name = Resources.CoreWebContent.WEBDATA_IB0_128, Table = "Custom" } };
        using (var proxy = _blProxyCreator.Create(delegate(Exception ex) { throw ex; }))
        {
            list.AddRange(proxy.GetPredefinedCustomProperties(targetEntity, includeAdvanced));
        }

        return list;
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetCpAvailableEntities(string property, string type)
    {
        DataTable table;
        using (var dal = new SolarWinds.Orion.Web.DAL.CustomPropertyDAL())
        {
            table = dal.GetAvailableEntities();
        }

        return table == null ? null : new PageableDataTable(table, table.Rows.Count);
    }

    [WebMethod(EnableSession = true)]
    public DataTable GetEntityColumns(string entity)
    {
        // common system property list
        var columnslist = CustomPropertyHelper.GetCPExportColumns(entity);
        // all system properties
        var systemColumns = CustomPropertyHelper.GetEntityProperties(entity);

        var resultDt = new DataTable();
        resultDt.Columns.Add("ColumnName", typeof(string));
        resultDt.Columns.Add("ColumnDisplayName", typeof(string));
        resultDt.Columns.Add("Type", typeof(string));
        resultDt.Columns.Add("System", typeof(bool));
        resultDt.Columns.Add("Visible", typeof(bool));
        resultDt.Columns.Add("Editable", typeof(bool));
        resultDt.Columns.Add("Values", typeof(string));
        resultDt.Columns.Add("Description", typeof(string));
        resultDt.Columns.Add("PropertySize", typeof(int));
        resultDt.Columns.Add("Mandatory", typeof(bool));
        resultDt.Columns.Add("Default", typeof(string));

        var visibleSystemColumnsCount = 0;

        if (systemColumns != null)
        {
            foreach (DataRow row in systemColumns.Rows)
            {
                var visible = columnslist.Count <= 0 || columnslist.Contains(row["ColumnName"].ToString());
                resultDt.Rows.Add(row["ColumnName"], row["ColumnDisplayName"], row["Type"], true,
                                  visible, false, string.Empty, string.Empty, -1);
                if (visible) visibleSystemColumnsCount++;
            }
        }

        // add display name column if there isn't any visible system columns available
        if (visibleSystemColumnsCount==0)
        {
            // DisplayName is defined in System.Entity 
            resultDt.Rows.Add("DisplayName", Resources.CoreWebContent.WEBDATA_VB0_12, "System.String", true, true, false, string.Empty, string.Empty, -1, false, string.Empty);
        }

        // add one more column - Uri to identity entity
        resultDt.Rows.Add("Uri", Resources.CoreWebContent.WEBDATA_CPE_Uri_Column, "System.String", true, false, false, string.Empty, string.Empty, -1, false, string.Empty);

        if (SwisEntityHelper.IsManagedEntity(entity))
            resultDt.Rows.Add("Status", Resources.CoreWebContent.WEBDATA_CPE_Status_Column, "System.String", true, false, false, string.Empty, string.Empty, -1, false, string.Empty);

        // in case entity support world map then add world map fields 
        if (WorldMapDAL.isEntityAvailable(entity))
        {
            resultDt.Rows.Add("WorldMapPoint.Latitude", Resources.CoreWebContent.WEBDATA_PCC_WM_LATITIDE, "System.Single", true, columnslist.Count <= 0 || columnslist.Contains("WorldMapPoint.Latitude"), true, string.Empty, string.Empty, -1, false, string.Empty);
            resultDt.Rows.Add("WorldMapPoint.Longitude", Resources.CoreWebContent.WEBDATA_PCC_WM_LONGITUDE, "System.Single", true, columnslist.Count <= 0 || columnslist.Contains("WorldMapPoint.Longitude"), true, string.Empty, string.Empty, -1, false, string.Empty);
        }

        var targetEntity = CustomPropertyHelper.GetCustomPropertyTarget(entity, true);

        // list of custom properties
        var customColumns = CustomPropertyHelper.GetEntityProperties(targetEntity);
        if (customColumns != null)
        {
            foreach (DataRow row in customColumns.Rows)
            {
                var prop = CustomPropertyMgr.GetCustomPropertyByEntity(targetEntity, row["ColumnName"].ToString());
                if (prop == null) continue;

                var values = string.Empty;
                if (prop.Values.Length > 0)
                {
                    values = string.Join("::", prop.Values);
                }

                string description = SolarWinds.Orion.Core.Web.FormatHelper.MakeBreakableString(prop.Description);

                resultDt.Rows.Add(row["ColumnName"], row["ColumnDisplayName"], row["Type"], false, true, "true".Equals(row["CanUpdate"].ToString(), StringComparison.OrdinalIgnoreCase), values, description, prop.DatabaseColumnLength, prop.Mandatory, prop.Default);
            }
        }
        return resultDt;
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetEntityCustomPropertyTable(string property, string type, string value, string search, string extendedParams)
    {
        int pageSize;
        int startRowNumber;
        var queryString = HttpUtility.ParseQueryString(extendedParams);

        // retrieve paging parameters
        var sortColumn = queryString["sort"];
        var sortDirection = queryString["dir"];

        Int32.TryParse(queryString["start"], out startRowNumber);
        Int32.TryParse(queryString["limit"], out pageSize);

        if (pageSize == 0)
            pageSize = 100;

        var entityColumns = GetEntityColumns(type);

        var query = new StringBuilder(" SELECT ");
        var comma = string.Empty;

        // getting property name and property type
        var param = property.Split('$');

        var propertyName = string.Empty;
        var propertyType = string.Empty;

        if (param.Length == 2)
        {
            propertyName = EntityPropertyHelper.GetPropertyShortName(param[0], type);
            propertyType = param[1];
        }
        var selectedColumns = WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName,string.Format("CPIEditor_{0}", type.Replace('.', '_')), string.Empty).Split(',');

        foreach (DataRow column in entityColumns.Rows)
        {
            var isSystem = (bool)column["System"];
            var isVisible = (bool)column["Visible"];
            var columnName = column["ColumnName"].ToString();
            if (isSystem && (isVisible || RequaredProperties.Contains(columnName) || selectedColumns.Contains(columnName)))
            {
                query.AppendFormat("{0} e.{1}", comma, columnName);
                comma = ",";
            }
            else if (!isSystem)
            {
                query.AppendFormat("{0} e.CustomProperties.{1}", comma, columnName);
                // in case we have sorting by custom property we need include "CustomProperties" prefix into sort column
                if (!string.IsNullOrEmpty(sortColumn) && sortColumn.Equals(columnName))
                {
                    sortColumn = string.Format("CustomProperties.{0}", sortColumn);
                }
                // if we're trying to get filtered data for some custom property we need to get property type
                if (!string.IsNullOrEmpty(value) && !string.IsNullOrEmpty(propertyName) && string.IsNullOrEmpty(propertyType))
                {
                    if (propertyName.Equals(columnName, StringComparison.OrdinalIgnoreCase))
                    {
                        propertyType = column["Type"].ToString();
                    }
                }
                comma = ",";
            }

            if (string.IsNullOrEmpty(sortColumn)) sortColumn = columnName;
        }

        query.AppendFormat(" FROM {0} (nolock=true) e ", type);

        string whereClause = GetWhereConditionForCPInlineGrid(property, type, value, search);

        var filtersCondition = GetFiltersCondition(queryString, entityColumns, type, "e");
        if (!string.IsNullOrEmpty(filtersCondition))
        {
            whereClause = string.Format(" {0} {1} ({2})", whereClause, string.IsNullOrEmpty(whereClause) ? string.Empty : "AND", filtersCondition);
        }

        string countString;
        if (property.Contains(ContainersMetadataDAL.GroupByProperty))
        {
            query.AppendFormat("JOIN Orion.ContainerMemberSnapshots cms ON cms.MemberUri = e.Uri");

            countString = string.Format(@"SELECT COUNT(e.Uri) AS Cnt 
                FROM {0} (nolock=true) e LEFT JOIN Orion.ContainerMemberSnapshots cms ON cms.MemberUri = e.Uri  {1} ", type, !string.IsNullOrEmpty(whereClause)
                                       ? string.Format(" WHERE {0} ", whereClause)
                                       : string.Empty);
        }
        else
        {
            countString = string.Format(@"SELECT COUNT(e.Uri) AS Cnt 
                FROM {0} (nolock=true) e {1}", type, !string.IsNullOrEmpty(whereClause)
                                       ? string.Format(" WHERE {0} ", whereClause)
                                       : string.Empty);
        }

        if (!string.IsNullOrEmpty(whereClause))
        {
            query.AppendFormat(" WHERE {0} ", whereClause);
        }

        query.AppendFormat(" ORDER BY e.{0} {1} WITH ROWS {2} to {3}", sortColumn, sortDirection, startRowNumber + 1, startRowNumber + pageSize);

        DataTable res;
        DataTable countTable;

        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                res = service.Query(query.ToString());
                countTable = service.Query(countString);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

        return new PageableDataTable(res, (countTable.Rows == null || countTable.Rows.Count == 0 ? res.Rows.Count : (int)(countTable.Rows[0][0])));
    }

    private string GetWhereConditionForCPInlineGrid(string property, string type, string value, string search)
    {
        var entityColumns = GetEntityColumns(type);
        // getting property name and property type
        var param = property.Split('$');

        var propertyName = string.Empty;
        var propertyType = string.Empty;
        var isCustom = false;

        if (param.Length == 2)
        {
            isCustom = EntityPropertyHelper.IsCustomProperty(param[0]);
            propertyName = EntityPropertyHelper.GetPropertyShortName(param[0], type);

            propertyType = param[1];
        }

        if (property.Contains(ContainersMetadataDAL.GroupByProperty))
        {
            return String.Format("( cms.ContainerID = '{0}' )", value);
        }
        var andCondition = string.Empty;
        var whereClause = new StringBuilder();
        if (!string.IsNullOrEmpty(propertyName) && !string.IsNullOrEmpty(value))
        {
            var eProp = new EntityProperty
            {
                EntityAlias = "e",
                EntityName = type,
                Name = propertyName,
                Type = propertyType,
                IsCustom = isCustom,
                CustomPropertyNavigationName = EntityPropertyHelper.CustomPropertiesProperty
            };

            whereClause.AppendFormat(" ({0}) ", EntityPropertyHelper.GetWhere(eProp,value.Equals(EntityPropertyHelper.GetPropertyUnknownValue()) ? "" : value));

            andCondition = " AND ";
        }

        if (!string.IsNullOrEmpty((search)))
        {
            whereClause.AppendFormat(" {0} (", andCondition);
            var orCondition = string.Empty;

            foreach (DataRow column in entityColumns.Rows)
            {
                // add conditions only for visible 
                if ((bool)column["Visible"])
                {
                    var isSystem = (bool)column["System"];
                    whereClause.AppendFormat(
                        isSystem ? "{0} (e.{1} LIKE '{2}')" : "{0} (e.CustomProperties.{1} LIKE '{2}')", orCondition,
                        column["ColumnName"], search.Replace("'", "''").Replace("[", "[[]"));
                    orCondition = "OR";
                }
            }
            whereClause.Append(" ) ");
        }

        return whereClause.ToString();
    }

    public DataTable GetEntityCustomPropertyUriNoLimit(string property, string type, string value, string search, string extendedParams = null)
    {
        var query = new StringBuilder(" SELECT ");

        query.Append("e.Uri");

        query.AppendFormat(" FROM {0} (nolock=true) e ", type);

        string whereClause = GetWhereConditionForCPInlineGrid(property, type, value, search);
        if (!string.IsNullOrEmpty(extendedParams))
        {
            var queryString = HttpUtility.ParseQueryString(extendedParams);
            var entityColumns = GetEntityColumns(type);
            var filtersCondition = GetFiltersCondition(queryString, entityColumns, type, "e");
            if (!string.IsNullOrEmpty(filtersCondition))
            {
                whereClause = string.Format(" {0} {1} ({2})", whereClause,
                    string.IsNullOrEmpty(whereClause) ? string.Empty : "AND", filtersCondition);
            }
        }

        if (!string.IsNullOrEmpty(whereClause))
        {
            query.AppendFormat(" WHERE {0} ", whereClause);
        }

        DataTable res;
        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                res = service.Query(query.ToString());
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

        return res;
    }

    public string GetFiltersCondition(System.Collections.Specialized.NameValueCollection collection, DataTable entityColumns, string entityName, string entityAlias)
    {
        if (collection == null) return string.Empty;

        var filters = FilterConditionHelper.GetFiltersInfo(collection);
        var filterCondition = new StringBuilder();
        var andConj = string.Empty;

        if (filters.Count == 0) return string.Empty;

        foreach (DataRow row in entityColumns.Rows)
        {
            var colName = row["ColumnName"].ToString();
            foreach (var filter in filters)
            {
                if (filter.Value.FilterColumn.Equals(colName, StringComparison.OrdinalIgnoreCase ) && filter.Value.FilterValue != null)
                {
                    filterCondition.AppendFormat(" {0} {1} ", andConj, FilterConditionHelper.GetFilterCondition(filter.Value, entityName, entityAlias, !(bool)row["System"]));
                    andConj = " AND ";
                }
            }
        }

        return filterCondition.ToString();
    }

    [WebMethod(EnableSession = true)]
    public void AssignAllCustomPropertyValues(object[] propertiesToUpdate, string entity, string pr, string type,
                                              string val, string search, string extendedParams)
    {
        List<string> uris = new List<string>();
        var uriTable = GetEntityCustomPropertyUriNoLimit(pr, type, val, search, extendedParams);
        for (int i = 0; i < uriTable.Rows.Count; i++)
        {
            uris.Add(uriTable.Rows[i][0].ToString());
        }

        var properties = new Dictionary<string, object>();
        var modifiedCustomProperties = new Dictionary<string, CustomProperty>();
        var targetEntity = CustomPropertyHelper.GetCustomPropertyTargetBySourceName(entity);
        var values = new Dictionary<string, List<string>>();
        foreach (object prop in propertiesToUpdate)
        {
            var dicValues = (Dictionary<string, object>) prop;

            var property = dicValues["property"].ToString();
            var value = dicValues["value"].ToString();

            var customProp = CustomPropertyMgr.GetCustomPropertyByEntity(targetEntity, property);

            if (customProp != null && customProp.Values.Length > 0)
            {
                modifiedCustomProperties[customProp.PropertyName] = customProp;
                value = CustomPropertyHelper.GetInvariantCultureString(value, customProp.PropertyType);
            }
            if (!values.ContainsKey(property))
            {
                values[property] = new List<string>();
            }
            if (!values[property].Contains(value))
            {
                values[property].Add(value);
            }

            if (!properties.ContainsKey(property))
            {
                if (string.IsNullOrEmpty(value))
                    properties.Add(property, DBNull.Value);
                else
                    properties.Add(property, value);
            }
        }
        try
        {
            CustomPropertyHelper.AssignCpValues(uris.ToArray(), properties);
            foreach (KeyValuePair<string, CustomProperty> propAssignment in modifiedCustomProperties)
            {
                CustomPropertyHelper.UpdateRestrictedValuesIfNeeded(modifiedCustomProperties[propAssignment.Key], values[propAssignment.Key].ToArray());
            }
        }
        catch (Exception ex)
        {
            log.Error("AssignCustomPropertyValues: failed to assign values", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public void AssignCustomPropertyValues(object[] propertiesToUpdate, string entity)
    {
        var properties = new Dictionary<string, Dictionary<string, List<string>>>();
        var modifiedCustomProperties = new Dictionary<string, CustomProperty>();
        var values = new Dictionary<string, List<string>>();
        var targetEntity = CustomPropertyHelper.GetCustomPropertyTargetBySourceName(entity);

        foreach (object prop in propertiesToUpdate)
        {
            var dicValues = (Dictionary<string, object>)prop;

            var uri = dicValues["uri"].ToString();
            var property = dicValues["property"].ToString();
            var value = dicValues["value"].ToString();

            var customProp = CustomPropertyMgr.GetCustomPropertyByEntity(targetEntity, property);

            if (customProp!=null)
            {
                modifiedCustomProperties[customProp.PropertyName] = customProp;
                value = CustomPropertyHelper.GetInvariantCultureString(value, customProp.PropertyType);
            }

            if (!values.ContainsKey(property))
            {
                values[property] = new List<string>();
            }

            if (!values[property].Contains(value))
            {
                values[property].Add(value);
            }

            if (!properties.ContainsKey(property))
            {
                properties.Add(property, new Dictionary<string, List<string>>());
            }

            if (!properties[property].ContainsKey(value))
            {
                properties[property].Add(value, new List<string>());
            }

            if (!properties[property][value].Contains(uri))
            {
                properties[property][value].Add(uri);
            }
        }

        foreach (KeyValuePair<string, Dictionary<string, List<string>>> propAssignment in properties)
        {
            try
            {
                CustomPropertyHelper.AssignCpValuesForEntity(propAssignment.Key, propAssignment.Value, targetEntity);
                if (modifiedCustomProperties.ContainsKey(propAssignment.Key))
                {
                    CustomPropertyHelper.UpdateRestrictedValuesIfNeeded(modifiedCustomProperties[propAssignment.Key], values[propAssignment.Key].ToArray());
                }
            }
            catch (Exception ex)
            {
                log.Error("AssignCustomPropertyValues: failed to assign values", ex);
                throw;
            }
        }
    }

    [WebMethod(EnableSession = true, MessageName = "GetCustomPropertyEntityGroups")]
    public IEnumerable<SolarWinds.Orion.Web.DAL.ContainersMetadataDAL.EntityGroup> GetCustomPropertyEntityGroups(string entityType, string groupByProperty, string searchValue, string[] excludeDefinitions)
    {
        IEnumerable<SolarWinds.Orion.Web.DAL.ContainersMetadataDAL.EntityGroup> groups = null;
        using (var dal = new SolarWinds.Orion.Web.DAL.CustomPropertyDAL())
        {
            groups = dal.GetCustomPropertyEntityGroups(entityType, groupByProperty, searchValue, excludeDefinitions.ToList(), 0);
        }
        var maxCount = ContainerHelper.GetManageGroupsMaxObjectTreeItems();

        if (groups != null && groups.Count() > maxCount)
        {
            var remainingCount = groups.Count() - maxCount + 1;
            var newGroups = groups.Take(maxCount);
            var entityGroup = new List<SolarWinds.Orion.Web.DAL.ContainersMetadataDAL.EntityGroup>()
            {
                new SolarWinds.Orion.Web.DAL.ContainersMetadataDAL.EntityGroup()
                    {
                        Name = String.Format("There {2} {0} more item{1}...", remainingCount, (remainingCount > 1) ? "s" : "", (remainingCount > 1) ? "are" : "is")
                    }
            };
            return newGroups.Concat(entityGroup);
        }

        return groups;
    }
}
