<%@ WebService Language="C#" Class="ViewConditions" %>

using System;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public sealed class ViewConditions : WebService
{
    private static readonly Log _log = new Log();
    
    [WebMethod]
    public object GetPossibleConditionsForView(int viewId)
    {
        if (viewId == 0)
            return new object[] { };  
        AuthorizationChecker.AllowCustomize();
        try {
          var view = ViewManager.GetViewById(viewId);
          var rv = ViewManager.GetPossibleViewConditions(view);
          var selected = ViewManager.GetViewConditions(view);
          return rv.Select(x => new
          {
              Type = ViewConditionsDAL.GetStoredNameForType(x.GetType()),
              Name = x.DisplayName,
              Selected = selected.Contains(ViewConditionsDAL.GetStoredNameForType(x.GetType()))
          });
        } 
        catch (Exception ex)
        {
          _log.Error("Error while running GetPossibleConditionsForView", ex);
          throw;
        }
    }

    [WebMethod]
    public void SetViewCondition(int viewId, string condition, bool enable)
    {
        AuthorizationChecker.AllowCustomize();
        try {
          var dal = new ViewConditionsDAL();
          if (enable)
              dal.AddCondition(viewId, condition);
          else
              dal.RemoveCondition(viewId, condition);
          
        } 
        catch (Exception ex)
        {
          _log.Error("Error while running SetViewCondition", ex);
          throw;
        }
    }

    [WebMethod]
    public void ShowViewAlways(int viewId)
    {
        AuthorizationChecker.AllowCustomize();
        try {
          var dal = new ViewConditionsDAL();
          dal.RemoveAllConditions(viewId);
        } 
        catch (Exception ex)
        {
          _log.Error("Error while running ShowViewAlways", ex);
          throw;
        }
    }
}