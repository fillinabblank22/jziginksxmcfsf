﻿<%@ WebService Language="C#" Class="EditCustomPropertyValue" %>

using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Web.Services;
using System.Web.Script.Services;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class EditCustomPropertyValue : WebService
{
    [WebMethod]
    public Dictionary<string, bool> ValidateDate(string date, string time)
    {
        var result = new Dictionary<string, bool>();
        DateTime dateTime;
        result.Add("date", DateTime.TryParse(date, out dateTime) && (dateTime > SqlDateTime.MinValue).Value);
        result.Add("time", DateTime.TryParse(time, out dateTime));
        return result;
    }
}

