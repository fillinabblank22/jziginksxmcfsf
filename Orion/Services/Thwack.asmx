<%@ WebService Language="C#" Class="Thwack" %>

using System;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.NPM.Common;
using SolarWinds.NPM.Common.Models;
                            
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class Thwack : System.Web.Services.WebService
{
    private static SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();

    private void BLExceptionHandler(Exception ex)
    {
        _log.Error(ex);
        throw ex;
    }
    
    [WebMethod]
    public string GetRecentPosts(int postsCount)
    {
        PostItemList posts = GetPostItems("http://thwackfeeds.solarwinds.com/products/community/NPM.aspx");
        if (posts != null && posts.ItemList != null)
        {
            return FormatRecentPostsTable(posts.ItemList, postsCount);
        }
        return GetErrorMessage();
    }

    [WebMethod]
    public string GetRecentlyPostedUniversalPollers(int postsCount, DateTime oldestAllowed, string sortBy)
    {
        PostItemList posts = GetPostItems("http://thwackfeeds.solarwinds.com/productfeeds/xmlfeed.aspx?showfolder=udpollers");
        if (posts != null && posts.ItemList != null)
        {
            // filter older items than allowed
            posts.ItemList.RemoveAll(delegate(PostItem item) { return item.PubDate < oldestAllowed; });
            // sort items
            if (String.Equals(sortBy, "Title", StringComparison.OrdinalIgnoreCase))
            {
                posts.ItemList.Sort(SortByTitle);
            }
            else if (String.Equals(sortBy, "Views", StringComparison.OrdinalIgnoreCase))
            {
                posts.ItemList.Sort(SortByViews);
            }
            else if (String.Equals(sortBy, "Downloads", StringComparison.OrdinalIgnoreCase))
            {
                posts.ItemList.Sort(SortByDownloads);
            }
            else
            {
                posts.ItemList.Sort(SortByDateTime);
            }
            // format the result table
            return FormatRecentlyPostedUniversalPollersTable(posts.ItemList, postsCount);
        }
        return GetErrorMessage();
    }


    #region Query methods
    private static PostItemList GetPostItems(string url)
    {
        try
        {
            return PostsManagerGeneric<PostItemList>.GetPostItemsList(url);
        }
        catch
        {
            return null;
        }
    }

    #endregion

    #region Formatting methods
    private static string GetErrorMessage()
    {
        return string.Format("<div class=\"ThwackConnectError\">{0}</div>", Resources.CoreWebContent.WEBCODE_AK0_171);
    }

    
    private static string FormatRecentPostsTable(List<PostItem> posts, int count)
    {
        StringWriter content = new StringWriter();
        HtmlTextWriter writer = new HtmlTextWriter(content);

        writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding, "0");
        writer.AddAttribute(HtmlTextWriterAttribute.Cellspacing, "0");
        writer.RenderBeginTag(HtmlTextWriterTag.Table);
        
        int realCount = Math.Min(count, posts.Count);
        for (int i = 0; i < realCount; i++)
        {
            if (i % 2 != 0)
            {
                writer.AddAttribute("class", "ZebraStripe");
            }
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");
            writer.RenderBeginTag(HtmlTextWriterTag.Td);

            writer.RenderBeginTag(HtmlTextWriterTag.Ul);

           
            writer.RenderBeginTag(HtmlTextWriterTag.Li);

        
            writer.AddAttribute(HtmlTextWriterAttribute.Href, posts[i].Link);
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.WriteEncodedText(posts[i].Title);
            
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();
        }
        writer.RenderEndTag();
        return content.ToString();
    }

    private static string FormatRecentlyPostedUniversalPollersTable(List<PostItem> posts, int count)
    {
        StringWriter content = new StringWriter();
        HtmlTextWriter writer = new HtmlTextWriter(content);

        writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding, "0");
        writer.AddAttribute(HtmlTextWriterAttribute.Cellspacing, "0");
        writer.RenderBeginTag(HtmlTextWriterTag.Table);

        writer.AddAttribute("class", "ReportHeader");
        writer.RenderBeginTag(HtmlTextWriterTag.Tr);

        writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");
        writer.AddStyleAttribute(HtmlTextWriterStyle.FontSize, "7pt");
        writer.RenderBeginTag(HtmlTextWriterTag.Td);
        writer.RenderBeginTag(HtmlTextWriterTag.Ul);
        writer.Write(string.Format("{0}&nbsp;", Resources.CoreWebContent.WEBCODE_AK0_172));
        writer.RenderEndTag();   
        writer.RenderEndTag();

        writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");
        writer.AddStyleAttribute(HtmlTextWriterStyle.FontSize, "7pt");
        writer.AddStyleAttribute(HtmlTextWriterStyle.WhiteSpace, "nowrap");
        writer.RenderBeginTag(HtmlTextWriterTag.Td);
        writer.Write(string.Format("{0}&nbsp;", Resources.CoreWebContent.WEBCODE_AK0_173));
        writer.RenderEndTag();

        writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");
        writer.AddStyleAttribute(HtmlTextWriterStyle.FontSize, "7pt");
        writer.AddStyleAttribute(HtmlTextWriterStyle.WhiteSpace, "nowrap");
        writer.RenderBeginTag(HtmlTextWriterTag.Td);
        writer.Write(string.Format("{0}&nbsp;", Resources.CoreWebContent.WEBCODE_AK0_174));
        writer.RenderEndTag();

        writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");
        writer.AddStyleAttribute(HtmlTextWriterStyle.FontSize, "7pt");
        writer.AddStyleAttribute(HtmlTextWriterStyle.WhiteSpace, "nowrap");
        writer.RenderBeginTag(HtmlTextWriterTag.Td);
        writer.Write(string.Format("{0}&nbsp;", Resources.CoreWebContent.WEBCODE_AK0_175));
        writer.RenderEndTag();   

        writer.RenderEndTag();   
        
        int realCount = Math.Min(count, posts.Count);
        for (int i = 0; i < realCount; i++)
        {
            if (i % 2 != 0)
            {
                writer.AddAttribute("class", "ZebraStripe");
            }
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderBeginTag(HtmlTextWriterTag.Ul);


            writer.RenderBeginTag(HtmlTextWriterTag.Li);


            writer.AddAttribute(HtmlTextWriterAttribute.Href, posts[i].Link);
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.WriteEncodedText(!String.IsNullOrEmpty(posts[i].Title) ? posts[i].Title : "-");
            writer.RenderEndTag();
            writer.Write("&nbsp;");
            writer.RenderEndTag();

            writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");
    
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.WriteEncodedText(posts[i].PubDate.ToShortDateString());
            writer.Write("&nbsp;");
            writer.RenderEndTag();

            writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");
 
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write(posts[i].DownloadCount);
            writer.Write("&nbsp;");
            writer.RenderEndTag();

            writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");

            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write(posts[i].ViewCount);
            writer.Write("&nbsp;");
            writer.RenderEndTag();

            writer.RenderEndTag();
        }
        writer.RenderEndTag();
        return content.ToString();
    }
    #endregion   
    
    #region Posts sorting methods
    private static int SortByDateTime(PostItem x, PostItem y)
    {
        return y.PubDate.CompareTo(x.PubDate);
    }

    private static int SortByTitle(PostItem x, PostItem y)
    {
        return x.Title.CompareTo(y.Title);
    }

    private static int SortByViews(PostItem x, PostItem y)
    {
        return y.ViewCount.CompareTo(x.ViewCount);
    }

    private static int SortByDownloads(PostItem x, PostItem y)
    {
        return y.DownloadCount.CompareTo(x.DownloadCount);
    } 
    #endregion
 
}