﻿<%@ WebService Language="C#" Class="ActionManager" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Resources;

using SolarWinds.Logging;
using SolarWinds.Orion.Core.Actions.DAL;
using SolarWinds.Orion.Core.Alerting;
using SolarWinds.Orion.Core.Alerting.DAL;
using SolarWinds.Orion.Core.Alerting.Models;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Core.Models;
using SolarWinds.Orion.Core.Models.Actions;
using SolarWinds.Orion.Core.Models.Actions.Contexts;
using SolarWinds.Orion.Core.Models.MacroParsing;
using SolarWinds.Orion.Core.Models.Schedules;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.InformationService;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ActionManager  : WebService {
    private static readonly Log log = new Log();

    [WebMethod]
    public PageableDataTable GetActions(string property, string type, string value, string search)
    {
        int pageSize, startRowNumber;
        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];
        bool isAssignedAlert = property.ToLower().Equals("assignedalert");

        string sortOrder = "Title";
        if (!string.IsNullOrEmpty(sortColumn))
        {
            sortOrder = string.Format("{0} {1}", sortColumn, sortDirection);
        }

        var parameters = GetQueryConditionsParameters(property, type, isAssignedAlert ? string.Empty : value, search);
        string whereCondition = parameters.Item1;

        if (pageSize == 0)
            pageSize = 20;

        string queryString =
            string.Format(
                @"SELECT a.ActionID, a.ActionTypeID, a.Title, a.Enabled, a.Assignments.EnvironmentType, ac.Name as AssignedAlert, ac.AlertID, a.Description,
                 a.Assignments.CategoryType, SplitStringToArray('') AS TimeSchedule, a.Assignments.ActionAssignmentID, ac.Description as AlertDescription
FROM Orion.Actions (nolock=true) a
 Inner Join Orion.AlertConfigurations (nolock=true) ac ON ac.AlertID=a.Assignments.ParentID And a.Assignments.EnvironmentType = 'Alerting'
Where {0}", whereCondition);

        if (isAssignedAlert)
        {
            //Get where clause for count query
            parameters = GetQueryConditionsParameters(property, type, value, search);
            whereCondition = parameters.Item1;
        }
		
        string countString =
            string.Format(
                @"SELECT Count(T.ActionID) AS Cnt FROM 
(SELECT Distinct ActionID FROM Orion.Actions (nolock=true) a Inner Join Orion.AlertConfigurations (nolock=true) ac ON ac.AlertID=a.Assignments.ParentID And a.Assignments.EnvironmentType = 'Alerting' {0}) as T",
                string.IsNullOrEmpty(whereCondition) ?  string.Empty : string.Format("WHERE {0}", whereCondition));

        log.Debug("Actions main grid query :" + queryString);

        DataTable actionDataTable;
        DataTable countTable;
        Dictionary<string, object> queryParams = parameters.Item2;
        queryParams[@"accountID"] = HttpContext.Current.Profile.UserName;
		
        try
        {
            using (var service = InformationServiceProxy.CreateV3())
            {
                actionDataTable = FillActionsDataWithSchedules(service.Query(queryString, queryParams), service);
                countTable = service.Query(countString, queryParams);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

        var deduplicatedDataTable = new DataTable();
        deduplicatedDataTable.Columns.Add("ActionID", typeof(int));
        deduplicatedDataTable.Columns.Add("ActionTypeID", typeof(string));
        deduplicatedDataTable.Columns.Add("Title", typeof(string));
        deduplicatedDataTable.Columns.Add("Enabled", typeof(bool));
        deduplicatedDataTable.Columns.Add("EnvironmentType", typeof(string));
        deduplicatedDataTable.Columns.Add("AssignedAlert", typeof(string));
        deduplicatedDataTable.Columns.Add("AlertID", typeof(string));
        deduplicatedDataTable.Columns.Add("Description", typeof(string));
        deduplicatedDataTable.Columns.Add("CategoryType", typeof(string));
        deduplicatedDataTable.Columns.Add("TimeSchedule", typeof(object));
        deduplicatedDataTable.Columns.Add("ActionAssignmentID", typeof(string));
        deduplicatedDataTable.Columns.Add("AssignedAlertsData", typeof(object));

        var groupedActions = (from row in actionDataTable.AsEnumerable() group row by row.Field<int>("ActionID") into grp select new {ActionID = grp.Key, Rows = grp.ToList()}).ToDictionary(v => v.ActionID, v => v.Rows);
        foreach (var action in groupedActions)
        {
            var actiontRow = action.Value.First();
            var assignedAlerts = (action.Value.Count == 1) ? actiontRow["AssignedAlert"].ToString() : string.Format("{0} {1}", action.Value.Count, CoreWebContent.WEBCODE_VB0_241.ToLower());
            var assignedAlertsData = new List<object>();
            bool isSelectedAlert = string.IsNullOrEmpty(value); 
            
            foreach (var actionData in action.Value)
            {
                var alertName = actionData.Field<string>("AssignedAlert");
                isSelectedAlert = isSelectedAlert || alertName.Equals(value);
                assignedAlertsData.Add(
                    new
                    {
                        AlertID = actionData.Field<int>("AlertID"),
                        AlertName = alertName,
                        AlertDescription = actionData.Field<string>("AlertDescription")
                    });
            }

            if (isAssignedAlert && !isSelectedAlert) continue;

            var alertIds = (action.Value.Count == 1) ? actiontRow["AlertID"].ToString() : string.Join(",", action.Value.Select(x => (int) x["AlertID"]).ToArray());
            var actionAssignmentIds = (action.Value.Count == 1) ? actiontRow["ActionAssignmentID"].ToString() : string.Join(",", action.Value.Select(x => (int)x["ActionAssignmentID"]).ToArray());
            
            var row = deduplicatedDataTable.NewRow();
            row["ActionID"] = int.Parse(actiontRow["ActionID"].ToString());
            row["ActionTypeID"] = GetLocalizedProperty("ActionTypeID", actiontRow["ActionTypeID"].ToString());
            row["Title"] = actiontRow["Title"].ToString();
            row["Enabled"] = bool.Parse(actiontRow["Enabled"].ToString());
            row["EnvironmentType"] = GetLocalizedProperty("ActionEnviromentType", actiontRow["EnvironmentType"].ToString());
            row["AssignedAlert"] = assignedAlerts;
            row["AlertID"] = alertIds;
            row["Description"] = actiontRow["Description"].ToString();
            row["CategoryType"] = actiontRow["CategoryType"].ToString();
            row["TimeSchedule"] = actiontRow["TimeSchedule"];
            row["ActionAssignmentID"] = actionAssignmentIds;
            row["AssignedAlertsData"] = assignedAlertsData;

            deduplicatedDataTable.Rows.Add(row);
        }

        var dv = deduplicatedDataTable.DefaultView;
        dv.Sort = sortOrder;
        var sortedTable = dv.ToTable();
        var pageableDataTable = (sortedTable.Rows.Count == 0 || sortedTable.Rows.Count < startRowNumber) ? sortedTable : sortedTable.AsEnumerable().Skip(startRowNumber).Take(pageSize).CopyToDataTable();

        return new PageableDataTable(pageableDataTable, (countTable.Rows == null || countTable.Rows.Count == 0 ? 0 : (int)((countTable.Rows[0][0] is DBNull) ? 0 : countTable.Rows[0][0])));
    }

    private DataTable FillActionsDataWithSchedules(DataTable dataTable, InformationServiceProxy proxy)
    {
        if (dataTable == null || dataTable.Rows.Count == 0) return dataTable;
        var ids = dataTable.Rows.Cast<DataRow>().Select(x => (int)x["ActionID"]).ToArray();
        var schedules = proxy.Query(
                String.Format(@"Select f.DisplayName, s.ActionID 
FROM Orion.Frequencies f INNER JOIN Orion.ActionSchedules s ON f.FrequencyID = s.FrequencyID
Where s.ActionID IN ({0})",
                    string.Join(",", ids)));

        if (schedules == null || schedules.Rows.Count==0) return dataTable;

        foreach (DataRow row in dataTable.Rows)
        {
            row["TimeSchedule"] = schedules.Rows.Cast<DataRow>().Where(x => (int)x["ActionID"] == (int)row["ActionID"]).Select(x => x["DisplayName"].ToString()).ToArray();
        }
        return dataTable;
    }

    private Tuple<string, Dictionary<string, object>> GetQueryConditionsParameters(string property, string type, string value, string search)
    {
        var queryParams = new Dictionary<string, object>();
        string whereCondition = "1=1";

        if (!string.IsNullOrEmpty(property) && !string.IsNullOrEmpty(value))
        {

            if (value.ToLower().Equals("null"))
                whereCondition += String.Format(" AND ({0} IS NULL)", property.ToLower().Equals("assignedalert") ? "ac.Name" : "a." + property);
            else if (value.ToLower().Equals("{empty}"))
                whereCondition += String.Format(" AND ({0}='')", property.ToLower().Equals("assignedalert") ? "ac.Name" : "a." + property);
            else
            {
                whereCondition += String.Format(" AND ({0}=@value)", property.ToLower().Equals("assignedalert") ? "ac.Name" : "a." + property);
                queryParams[@"value"] = value;
            }
        }

        //getting selected columns to search on all selected columns
        var selectedColumns = SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, "ActionManager_SelectedColumns", WebConstants.ActionDafultSelectedColumns).Split(',').ToList();
        if (!string.IsNullOrEmpty(search) && selectedColumns.Count > 0)
        {
            var searchCondition = new StringBuilder();
            var orCond = string.Empty;
            foreach (var selCol in selectedColumns)
            {
                if (selCol.Equals("TimeSchedule")) continue; //ToDo: will be done when action schedule is ready
                searchCondition.AppendFormat("{1}{0} LIKE @search", mapColumn(selCol), orCond);
                orCond = " OR ";
            }
            if (searchCondition.Length > 0)
                whereCondition += String.Format(" AND ({0})", searchCondition);

            queryParams[@"search"] = search.Replace("[", "[[]");
        }
        return new Tuple<string, Dictionary<string, object>>(whereCondition, queryParams);
    }

    private static string mapColumn(string col)
    {
        switch (col)
        {
            case "EnvironmentType" :
                return "a.Assignments.EnvironmentType";
            case "AssignedAlert":
                return "ac.Name";
            case "CategoryType":
                return "a.Assignments.CategoryType";
            default:
                return "a." + col;
        }
    }

    [WebMethod]
    public int[] GetActionNumber(string actionIds, string property, string type, string value, string sort, string direction, string search)
    {
        var sortColumn = sort;
        var sortDirection = direction;

        var sortOrder = "Title,Asc";
        if (!string.IsNullOrEmpty(sortColumn))
        {
            sortOrder = string.Format("{0} {1}", sortColumn, sortDirection);
        }

        var parameters = GetQueryConditionsParameters(property, type, value, search);
        var whereCondition = parameters.Item1;
        var queryParams = parameters.Item2;
        
        string queryString = string.Format(@"
SELECT a.ActionID, ac.Name as AssignedAlert, a.Assignments.EnvironmentType as EnvironmentType 
FROM Orion.Actions a
Inner Join Orion.AlertConfigurations (nolock=true) ac ON ac.AlertID=a.Assignments.ParentID And a.Assignments.EnvironmentType = 'Alerting'
Where {0}
ORDER BY {1}", whereCondition, sortOrder);

        log.Debug("Actions main grid query :" + queryString);
        DataTable actionDataTable;
        try
        {
            using (var service = InformationServiceProxy.CreateV3())
            {
                actionDataTable = service.Query(queryString, queryParams);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

        if (actionDataTable == null || actionDataTable.Rows == null || actionDataTable.Rows.Count == 0)
            return new[] {-1};

        try
        {
            var rowsArray = actionDataTable.Select(string.Format("ActionID in ({0})", actionIds));
            var indexes = rowsArray.Select(row => actionDataTable.Rows.IndexOf(row)).ToArray();
            return indexes;
        }
        catch (Exception ex)
        {
            log.Error(ex);
            return new[] {-1};
        }
    }

    [WebMethod]
    public bool EnableDisableAction(string actionId, string value)
    {
        if ((bool) HttpContext.Current.Profile.GetPropertyValue("AllowDisableAction"))
        {
            bool enabled;
            int id;
            if (bool.TryParse(value, out enabled) && int.TryParse(actionId, out id))
            {
                var creator = InformationServiceProxy.CreateV3Creator();
                var dal = new SolarWinds.Orion.Core.Actions.DAL.ActionsDAL(creator);
                dal.EnableAction(id, enabled);

            }
            return true;
        }
        return false;
    }

    [WebMethod]
    public bool EnableDisableActions(string[] ids, string enable)
    {
        try
        {
            foreach (var id in ids.Where(id => Convert.ToInt32(id) != 0))
            {
                EnableDisableAction(id, enable);
            }
            return true;
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }
    }

    [WebMethod]
    public bool DeleteActions(string[] ids)
    {
        try
        {
            var creator = InformationServiceProxy.CreateV3Creator();
            var dal = new ActionsDAL(creator);
            foreach (var id in ids.Where(id => Convert.ToInt32(id) != 0))
            {
                dal.DeleteAction(Convert.ToInt32(id));
            }

            return true;
        }
        catch (Exception ex)
        {
            log.Error("Couldn't delete action.", ex);
            throw;
        }
    }

    [WebMethod]
    public bool CheckForMultipleAssignments(string[] ids)
    {
        if (ids == null || ids.Length == 0)
            return false;

        string query = string.Format(@"Select Count(*) as cnt FROM Orion.ActionsAssignments aa
where aa.ActionID in ({0})
Group by aa.ActionID", string.Join(",", ids));
        
        using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
        {
            var actions = service.Query(query);
            if (actions != null && actions.Rows.Count > 0)
            {
                var result = actions.Rows.Cast<DataRow>()
                   .Where(x => x.Field<int>("cnt") > 1)
                   .ToArray();

                if (result.Length > 0)
                    return true;
            }
        }

        return false;
    }
    
    [WebMethod]
    public bool DeleteActionsbyAssignmentID(string[] ids)
    {
        if (ids == null || ids.Length == 0)
        {
            log.Debug("No actions to delete");
            return true;
        }
        
        log.DebugFormat("Deleting actions ids: {0}", string.Join(",", ids));

        try
        {
            DataTable grouped, uries;
            string query = string.Format(@"SELECT a.Uri, aa.ActionID, Count(aa.ActionID) as cnt
FROM Orion.ActionsAssignments aa
inner join Orion.Actions a on a.ActionID = aa.ActionID
Where aa.ActionID in
(select ActionID from Orion.ActionsAssignments aa where aa.ActionAssignmentID in ({0}))
Group by Uri, ActionID", string.Join(",", ids));

            string assignmentUriesQuery = string.Format(@"SELECT ActionID, ActionAssignmentID, Uri
FROM Orion.ActionsAssignments
Where ActionAssignmentID in ({0})",string.Join(",", ids));
            
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                grouped = service.Query(query);
                uries = service.Query(assignmentUriesQuery);
            }

            if (grouped == null || grouped.Rows.Count == 0)
            {
                log.Error("Actions manager: nothing to delete from query:" + query);
                return false;
            }

            var actions =
                grouped.Rows.Cast<DataRow>()
                    .Select(r => (string) r["Uri"])
                    .ToArray(); // we can drop here whole action
            var creator = InformationServiceProxy.CreateV3Creator();

            var uriesToDelete = uries.Rows.Cast<DataRow>().Select(x => (string)x["Uri"]).ToArray();
            
            // getting the rest uries
            using (IInformationServiceProxy2 proxy = creator.Create())
            {
                proxy.BulkDelete(uriesToDelete);
                proxy.BulkDelete(actions);
            }

            return true;
        }
        catch (Exception ex)
        {
            log.Error("Couldn't delete action.", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetObjectGroupProperties()
    {
        try
        {
            var dt = new DataTable();
            dt.Columns.Add("Value");
            dt.Columns.Add("Name");
            dt.Columns.Add("Type");

            dt.Rows.Add("", CoreWebContent.WEBCODE_AK0_70, "");
            dt.Rows.Add("ActionTypeID", CoreWebContent.WEBDATA_VM0_6, "objecttype");
            dt.Rows.Add("Enabled", CoreWebContent.WEBDATA_LK0_6, "");
            dt.Rows.Add("AssignedAlert", CoreWebContent.WEBDATA_VM0_7, "assignedalert");

            return new PageableDataTable(dt.DefaultView.ToTable(), dt.Rows.Count);
        }
        catch (Exception ex)
        {
            log.ErrorFormat("GetEntityProperties: Error during getting entity properties - {0}", ex.ToString());
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetGroupValues(string property, string type)
    {
        if (string.IsNullOrEmpty(property))
            return null;

        string queryString;
        var queryParams = new Dictionary<string, object>();

        if (type.Equals("assignedalert", StringComparison.OrdinalIgnoreCase))
        {
            queryString =
                @"SELECT ac.Name as Value, ac.Name as DisplayNamePlural, COUNT(*) as Cnt 
            FROM Orion.Actions (nolock=true) a Inner Join Orion.AlertConfigurations (nolock=true) ac ON ac.AlertID=a.Assignments.ParentID And a.Assignments.EnvironmentType = 'Alerting'
            GROUP BY ac.Name 
            ORDER BY ac.Name";
        }
        else
        {
            var sb = new StringBuilder();
            var value = string.Format("T.{0}",property);
            
            if (property.Equals("ActionTypeID", StringComparison.OrdinalIgnoreCase))
            {
                var plugins = SolarWinds.Orion.Web.Actions.ActionManager.Instance.GetPlugins(ActionEnviromentType.Alerting).ToList();
                if (plugins.Count > 0)
                {
                    sb.Append("case ");
                    foreach (var actionPlugin in plugins)
                    {
                        sb.Append(string.Format("when {2} = '{0}' then '{1}'", actionPlugin.ActionTypeID, actionPlugin.Title.Replace("'", "''"), value));
                    }
                    sb.Append("end");
                }
                else
                {
                    sb.Append(value);
                }
            }
            else
            {
                sb.Append(value);
            }

            queryString = string.Format(@"SELECT T.{0} as Value, {1} as DisplayNamePlural, COUNT(T.ActionID) as Cnt FROM 
            (SELECT Distinct a.{0}, a.ActionID FROM Orion.Actions (nolock=true) a Inner Join Orion.AlertConfigurations (nolock=true) ac ON ac.AlertID=a.Assignments.ParentID And a.Assignments.EnvironmentType = 'Alerting') AS T
            GROUP BY T.{0} 
            ORDER BY T.{0}", property, sb);
        }

        const string countQuery = @"SELECT COUNT(T.ActionID) as Cnt FROM 
            (SELECT Distinct a.ActionID FROM Orion.Actions (nolock=true) a Inner Join Orion.AlertConfigurations (nolock=true) ac ON ac.AlertID=a.Assignments.ParentID And a.Assignments.EnvironmentType = 'Alerting'
            Where a.Assignments.EnvironmentType = 'Alerting') AS T";

        log.Debug("Manage Actions main grid group query: " + queryString);
        DataTable cpDataTable;
        DataTable countTable;

        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                cpDataTable = service.Query(queryString, queryParams);
                countTable = service.Query(countQuery, queryParams);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

        var dt = new DataTable();
        dt.Columns.Add("Value");
        dt.Columns.Add("DisplayNamePlural");
        dt.Columns.Add("Cnt");

        dt.Rows.Add("[All]", CoreWebContent.WEBDATA_VM0_2, countTable.Rows[0][0]);

        foreach (DataRow row in cpDataTable.Rows)
            dt.Rows.Add(row["Value"], row["DisplayNamePlural"], row["Cnt"]);

        return new PageableDataTable(dt, dt.Rows.Count);
    }

    [WebMethod(EnableSession = true)]
    public string GetActionDefinition(string id)
    {
        ActionDefinition action = new ActionDefinition();
        var creator = InformationServiceProxy.CreateV3Creator();
        try
        {
            var dal = new ActionsDAL(creator);
            action = dal.LoadAction(Convert.ToInt32(id));
        }
        catch (Exception ex)
        {
            log.ErrorFormat("Couldn't export action by using {0} actionId. Error - {1}", id, ex);
        }
        return ToJson(action);
    }

    private AlertDefinition GetAlertDefinition(int alertId)
    {
        var dal = AlertDefinitionsDAL.Create(ConditionTypeProvider.Create(AppDomainCatalog.Instance, InformationServiceProxy.CreateV3Creator()), InformationServiceProxy.CreateV3Creator());
        var alert = dal.Get(alertId);
        return alert;
    }
    
    [WebMethod(EnableSession = true)]
    public string GetAlertingActionContext(string alertDefinitionId)
    {
        var alert = GetAlertDefinition(Convert.ToInt16(alertDefinitionId));
        var alertingActionContext = new AlertingActionContext
        {
            EntityType = alert.ObjectType,
            AlertContext = new AlertContext()
            {
                AlertName = alert.Name
            }
        };
        var contexts = SolarWinds.Orion.Web.Alerts.VariablePickerFactory.GetAlertingMacroContexts(alert);
        foreach (var context in contexts)
        {
            alertingActionContext.MacroContext.Add(context);
        }
        var swisEntityContext = alertingActionContext.MacroContext.GetData<SwisEntityContext>();
        if (swisEntityContext != null)
            alertingActionContext.EntityType = swisEntityContext.EntityType;
        return ToJson(alertingActionContext);
    }

    [WebMethod(EnableSession = true)]
    public void UpdateAction(string actionDefinition)
    {
        ActionDefinition action = JsonConvert.DeserializeObject<ActionDefinition>(actionDefinition);
        var creator = InformationServiceProxy.CreateV3Creator();
        try
        {
            var dal = new ActionsDAL(creator);
            dal.UpdateAction(action);
        }
        catch (Exception ex)
        {
            log.ErrorFormat("Couldn't save action with actionId - {0}. Error - {1}", action.ID, ex);
        }
    }
    [WebMethod(EnableSession = true)]
    public void UpdateActionsProperties(string actionsProperties, List<int> actionsIds)
    {
        var properties = (ActionProperties)OrionSerializationHelper.FromJSON(actionsProperties, typeof(ActionProperties));
        var creator = InformationServiceProxy.CreateV3Creator();
        try
        {
            ActionsDAL dal = new ActionsDAL(creator);
            dal.UpdateActionsProperties(properties, actionsIds);
            IEnumerable<ActionDefinition> actionsDefinitions = dal.LoadActions(actionsIds);
            UpdateActionsDescriptions(actionsDefinitions, dal);
        }
        catch (Exception ex)
        {
            log.ErrorFormat("Couldn't save actions properties with actionId - {0}. Error - {1}", string.Join(",",actionsIds), ex);
        }
    }
    
    private void UpdateActionsDescriptions(IEnumerable<ActionDefinition> actionsDefinitions, ActionsDAL dal) 
    {
        if (actionsDefinitions == null) throw new ArgumentNullException("actionsDefinitions");

        Dictionary<int?, string> actionsDescriptions = new Dictionary<int?, string>();
        foreach(ActionDefinition actionDefinition in actionsDefinitions)
        {
            ActionConfiguration actionConfiguration = SolarWinds.Orion.Web.Actions.ActionManager.Instance.GetActionConfiguration(actionDefinition.ActionTypeID);
            actionConfiguration.Load(actionDefinition);
            actionsDescriptions.Add(actionDefinition.ID, actionConfiguration.GetDescription());
        }
        try
        {
            dal.UpdateActionsDescriptions(actionsDescriptions);
        }
        catch (Exception ex)
        {
            log.ErrorFormat("Couldn't save actions descriptions. Error - {0}", ex);
        }
    }

    [WebMethod]
    public void UpdateActionsTitle(string actionsTitle, List<int> actionsIds)
    {
        if (actionsTitle == null) throw new ArgumentNullException("actionsTitle");        
        if (actionsIds == null) throw new ArgumentNullException("actionsIds");

        string query = string.Format("SELECT Uri From Orion.Actions WHERE ActionID IN ({0})", string.Join(",", actionsIds));
        using (var service = InformationServiceProxy.CreateV3())
        {
            try
            {
                var uris = service.Query(query).AsEnumerable().Select(x => x.Field<string>(0)).ToArray();
                service.BulkUpdate(uris, new Dictionary<string, object> {{"Title", actionsTitle}});
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Couldn't save actions title with actionId - {0}. Error - {1}",
                    string.Join(",", actionsIds), ex);
            }
        }
    }

    [WebMethod]
    public void UpdateActionsFrequencies(string timePeriods, List<int> actionsIds)
    {
        var timePeriodSchedules = JsonConvert.DeserializeObject<List<TimePeriodSchedule>>(timePeriods);
        var creator = InformationServiceProxy.CreateV3Creator();       
        try
        {
            var dal = new ActionsDAL(creator);
            dal.UpdateActionsFrequencies(timePeriodSchedules, actionsIds);
        }
        catch (Exception ex)
        {
            log.ErrorFormat("Couldn't save actions frequencies with actionId - {0}. Error - {1}",
                string.Join(",", actionsIds), ex);
        }        
    }

    [WebMethod]
    public string GetMacroContexts(int actionId)
    {
        string query = string.Format(@"SELECT a.ParentID FROM Orion.ActionsAssignments a                                       
                                       WHERE a.ActionID = @actionId");

        using (var service = InformationServiceProxy.CreateV3())
        {
            var dt = service.Query(query, new Dictionary<string, object>{{"actionId", actionId}});
            var alertId = dt.AsEnumerable().Select(x => x.Field<int>(0)).FirstOrDefault();
            var alertDal = AlertDefinitionsDAL.Create(ConditionTypeProvider.Create(AppDomainCatalog.Instance, InformationServiceProxy.CreateV3Creator()),
                    InformationServiceProxy.CreateV3Creator());
            var alert = alertDal.Get(alertId);

            var contexts = SolarWinds.Orion.Web.Alerts.VariablePickerFactory.GetAlertingMacroContexts(alert);

            return ToJson(contexts);
        }
    }


    
    [WebMethod]
    public bool UpdateActionsAssignments(string actionsDataJson, List<int> oldAlertIds, List<int> newAlertIds)
    {
        var actionsDataList = JsonConvert.DeserializeObject<List<ActionData>>(actionsDataJson);
        try
        {
            var creator = InformationServiceProxy.CreateV3Creator();
            var dal = new ActionsDAL(creator);

            dal.SaveActionsForAssignmentsWithCategory(newAlertIds, ActionEnviromentType.Alerting.ToString(), actionsDataList); 
              
            if (actionsDataList.Count == 1)
            {
                var itemsToRemove = oldAlertIds.Where(item => !newAlertIds.Contains(item)).ToList();
                
                 if (itemsToRemove.Any())
                    {
                        dal.UnAssignAlertsFromAction(actionsDataList[0].ActionID, itemsToRemove);
                    }
            }
            return true;

        }
        catch (Exception ex)
        {
            log.Error("Update schedule assignments failed.", ex);
            throw;
        }
       
    }

    [WebMethod]
    public string GetAssignedAlertsToAction(int actionId)
    {
        string query = string.Format(@"SELECT ac.AlertID, ac.Name FROM Orion.ActionsAssignments aa
                                        INNER JOIN Orion.AlertConfigurations ac ON ac.AlertID = aa.ParentID
                                        WHERE ActionID IN ({0}) AND EnvironmentType = 'Alerting'", actionId);
        
        DataTable alertsData = new DataTable();
        using (var service = InformationServiceProxy.CreateV3())
        {
            try
            {
                alertsData = service.Query(query);
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Couldn't get action asignments data", ex);
            }
        }
        return ToJson(alertsData);
    }
    
    public string ToJson(object objectGraph)
    {
        var settings = new JsonSerializerSettings();
        settings.TypeNameHandling = TypeNameHandling.Objects;

        return JsonConvert.SerializeObject(objectGraph, Formatting.Indented, settings);
    }

    private string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
}
