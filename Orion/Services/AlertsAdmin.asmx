<%@ WebService Language="C#" Class="AlertsAdmin" %>
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Models.Alerts;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Controllers.Alerts.Resources;
using SolarWinds.Orion.Web.Helpers;
using WebLimitation = SolarWinds.Orion.Web.Limitation;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class AlertsAdmin : WebService
{
    private static readonly Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    private static void BLExceptionHandler(Exception ex)
    {
        log.Error(ex);
        throw ex;
    }

    [WebMethod(EnableSession = true)]
    public int EnableAdvancedAlert(string alertDefId, bool enable)
    {
        AuthorizationChecker.AllowAdmin();

        using (var proxy = _blProxyCreator.Create(BLExceptionHandler))
        {
            try
            {
                return proxy.EnableAdvancedAlert(new Guid(alertDefId), enable);
            }
            catch (Exception ex)
            {
                log.ErrorFormat("EnableAdvancedAlert: Error during enabling advanced alert - {0}", ex.ToString());
                return -1;
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public int EnableAdvancedAlerts(List<string> alertDefIds, bool enable, bool enableAll)
    {
        AuthorizationChecker.AllowAdmin();
        using (var proxy = _blProxyCreator.Create(BLExceptionHandler))
        {
            try
            {
                return proxy.EnableAdvancedAlerts(alertDefIds, enable, enableAll);
            }
            catch (Exception ex)
            {
                log.ErrorFormat("EnableAdvancedAlert: Error during enabling advanced alert - {0}", ex.ToString());
                return -1;
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public int EnableAdvancedAlert(string alertDefId)
    {
        return EnableAdvancedAlert(alertDefId, true);
    }

    [WebMethod(EnableSession = true)]
    public int DisableAdvancedAlert(string alertDefId)
    {
        AuthorizationChecker.AllowAdmin();
        return EnableAdvancedAlert(alertDefId, false);
    }

    [WebMethod(EnableSession = true)]
    public int RemoveAdvancedAlert(string alertDefId)
    {
        AuthorizationChecker.AllowAdmin();

        using (var proxy = _blProxyCreator.Create(BLExceptionHandler))
        {
            try
            {
                return proxy.RemoveAdvancedAlert(new Guid(alertDefId));
            }
            catch (Exception ex)
            {
                log.ErrorFormat("RemoveAdvancedAlert: Error during deleting advanced alert - {0}", ex.ToString());
                return -1;
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public int RemoveAdvancedAlerts(List<string> alertDefIds, bool deleteAll)
    {
        AuthorizationChecker.AllowAdmin();

        //string[] ids = alertDefIds.Split(':');
        using (var proxy = _blProxyCreator.Create(BLExceptionHandler))
        {
            try
            {
                return proxy.RemoveAdvancedAlerts(alertDefIds, deleteAll);
            }
            catch (Exception ex)
            {
                log.ErrorFormat("RemoveAdvancedAlert: Error during deleting advanced alert - {0}", ex.ToString());
                return -1;
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public DataTable GetAdvAlerts()
    {
        using (var proxy = _blProxyCreator.Create(BLExceptionHandler))
        {
            try
            {
                return proxy.GetAdvancedAlerts();
            }
            catch (Exception ex)
            {
                log.ErrorFormat("GetAdvAlerts: Error during getting advanced alerts - {0}", ex.ToString());
                return null;
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public DataTable GetAdvAlert(string alertDefID)
    {
        using (var proxy = _blProxyCreator.Create(BLExceptionHandler))
        {
            DataTable dt;
            try
            {
                dt = proxy.GetAdvancedAlert(new Guid(alertDefID));
            }
            catch (Exception ex)
            {
                log.ErrorFormat("GetAdvAlerts: Error during getting advanced alerts - {0}", ex.ToString());
                throw;
            }
            return dt;
        }
    }

    [WebMethod(EnableSession = true)]
    public int UpdateAdvAlertDefinition(string alertDefId, string alertName, string alertDescr, bool enabled, int evInterval, string dow, string startTime, string endTime, bool ignoreTimeout)
    {
        AuthorizationChecker.AllowAdmin();

        var defYear = "12/30/1899 ";
        DateTime sTime = DateTime.Parse(defYear + startTime, System.Globalization.CultureInfo.InvariantCulture);
        DateTime eTime = DateTime.Parse(defYear + endTime, System.Globalization.CultureInfo.InvariantCulture);

        using (var proxy = _blProxyCreator.Create(BLExceptionHandler))
        {
            try
            {
                return proxy.UpdateAlertDef(new Guid(alertDefId), alertName, alertDescr, enabled, evInterval, dow, sTime, eTime, ignoreTimeout);
            }
            catch (Exception ex)
            {
                log.ErrorFormat("UpdateAlertDef: Error during updating advanced alerts - {0}", ex.ToString());
                throw;
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public void AcknowledgeAlerts(string[] alertIds)
    {
        using (var proxy = _blProxyCreator.Create(BLExceptionHandler))
        {
            try
            {
                proxy.AcknowledgeAlertsAction(new List<string>(alertIds), HttpContext.Current.Profile.UserName);
            }
            catch (Exception ex)
            {
                log.ErrorFormat("AcknowledgeAlerts: Error during Acknowledge advanced alerts - {0}", ex.ToString());
                throw;
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetAdvancedAlerts(string property)
    {
        int pageSize = 0;
        int startRowNumber = 0;

        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        using (var proxy = _blProxyCreator.Create(BLExceptionHandler))
        {
            try
            {
                DataTable dt = proxy.GetPagebleAdvancedAlerts(sortColumn, sortDirection, startRowNumber, pageSize);
                return new PageableDataTable(dt, proxy.AdvAlertsCount());
            }
            catch (Exception ex)
            {
                log.ErrorFormat("GetAdvAlerts: Error during getting advanced alerts - {0}", ex.ToString());
                return null;
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetAlertsForSWISObject(string property, string type)
    {
        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        using (var proxy = _blProxyCreator.Create(BLExceptionHandler))
        {
            try
            {
                var blRequest = new PageableActiveAlertRequest
                {
                    LimitationIDs = new int[] { WebLimitation.GetCurrentViewLimitationID() },
                    LoadTriggeringObjectEntityUriForLegacyAlerts = true,
                    PageSize = 2000,
                    OrderByClause = string.Format("{0} {1}", sortColumn, sortDirection),
                    FilterStatement = "(AcknowledgedBy IS NULL OR AcknowledgedBy = '')",
                    SecondaryFilters = new List<ActiveAlertFilter>()
	                {
		                new ActiveAlertFilter()
		                {
			                Comparison = ComparisonType.Equal,
			                FieldName = "TriggeringObjectEntityUri",
			                FieldDataType = "string",
			                Value = property
		                }
	                }
                };

                var res = proxy.GetPageableActiveAlerts(blRequest);
                var dt = new DataTable();
                dt.Columns.Add("AlertName", typeof(string));
                dt.Columns.Add("ActiveAlertType", typeof(ActiveAlertType));
                foreach (ActiveAlert alert in res.ActiveAlerts)
                {
                    DataRow row = dt.NewRow();
                    row["AlertName"] = alert.Name;
                    row["ActiveAlertType"] = alert.AlertType;

                    dt.Rows.Add(row);
                }
                return new PageableDataTable(dt, res.TotalRow);
            }
            catch (Exception ex)
            {
                log.ErrorFormat("GetAdvAlerts: Error during getting advanced alerts - {0}", ex.ToString());
                return null;
            }
        }
    }
    [WebMethod(EnableSession = true)]
    public void UpdateAdvancedAlertNote(string alertId, string netObjectId, string objectType, string notes)
    {
        using (var proxy = _blProxyCreator.Create(BLExceptionHandler))
        {
            proxy.UpdateAdvancedAlertNote(alertId, netObjectId.Split(':')[1], objectType, notes);
        }
    }

    [WebMethod(EnableSession = true)]
    public DataTable GetAlertDetailsById(string alertId, string netObjectId)
    {
        using (var proxy = _blProxyCreator.Create(BLExceptionHandler))
        {
            try
            {
                DataTable dt = proxy.GetSortableAlertTable(netObjectId, string.Empty, alertId,
                    " AlertTime DESC ", 1, true, WebLimitation.GetCurrentListOfLimitationIDs(), true);

                if (dt != null && dt.Rows.Count > 0)
                {
                    string activeObject = netObjectId.Split(':')[1];
                    dt.Columns.Add("Notes", typeof(string));
                    dt.Rows[0]["Notes"] = string.Empty;

                    DataTable notes = null;
                    if (dt.Rows[0]["MonitoredProperty"].ToString().Equals(
                        OrionMessagesHelper.GetMessageTypeString(OrionMessageType.ADVANCED_ALERT), StringComparison.OrdinalIgnoreCase))
                    {
                        dt.Rows[0]["MonitoredProperty"] =
                            OrionMessagesHelper.GetLocalizedMessageTypeLabel(OrionMessageType.ADVANCED_ALERT, true, false);
                        try
                        {
                            notes = SolarWinds.Orion.Web.DAL.SqlDAL.GetAlertStatusNotes(alertId, activeObject);
                        }
                        catch (Exception ex)
                        {
                            log.Error(ex);
                            return null;
                        }
                    }
                    else
                    {
                        dt.Rows[0]["MonitoredProperty"] =
                            OrionMessagesHelper.GetLocalizedMessageTypeLabel(OrionMessageType.BASIC_ALERT, true, false);
                    }

                    if (notes != null && notes.Rows.Count > 0)
                        foreach (DataRow note in notes.Rows)
                            dt.Rows[0]["Notes"] = note["Notes"].ToString();
                }

                return dt;

            }
            catch (Exception ex)
            {
                log.ErrorFormat("GetAlertDetailsById: Error during getting alert details - {0}", ex.ToString());
                return null;
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public void UpdateAcknowledgeAlerts(string[] alertKeys, string accountID, string acknowledgeNote)
    {
        using (var proxy = _blProxyCreator.Create(BLExceptionHandler))
        {
            try
            {
                List<string> alertKeysList = new List<string>();
                alertKeysList.AddRange(alertKeys);
                proxy.AcknowledgeAlertsWithNotes(alertKeysList, accountID, acknowledgeNote);
            }
            catch (Exception ex)
            {
                log.ErrorFormat("UpdateAcknowledgeAlerts: Error during updating alerts - {0}", ex.ToString());
                throw;
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public DataTable GetPageableAlerts(string period, int fromRow, int toRow, string type, string alertId, bool showAcknAlerts)
    {
        DataTable alertTable = null;
        using (var proxy = _blProxyCreator.Create(BLExceptionHandler))
        {
            try
            {
                alertTable = proxy.GetPageableAlerts(WebLimitation.GetCurrentListOfLimitationIDs(), period, fromRow, toRow, type, alertId, showAcknAlerts);
            }
            catch (Exception ex)
            {
                log.ErrorFormat("GetPageableAllAlerts: Error during loading alerts - {0}", ex.ToString());
                throw;
            }
        }
        return alertTable;
    }
}
