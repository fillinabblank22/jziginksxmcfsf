﻿<%@ WebService Language="C#" Class="WSAsyncExecuteTasks" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.ServiceModel;
using System.Web.Script.Services;
using System.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Model;

/// <summary>
/// Class responsible for asynchronous executing tasks on server.
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class WSAsyncExecuteTasks : WebService
{
    [WebMethod(true)]
    public object GetTaskResult(String delegateServerMethod, Object[] parameters)
    {
        try
        {
            var methodSubscription = delegateServerMethod.Split(';');
            string typeString = methodSubscription[0];
            string methodString = methodSubscription[1];

            Type type = Type.GetType(typeString);
            object instance = Activator.CreateInstance(type);
            System.Reflection.MethodInfo method = type.GetMethod(methodString);

            if (!(instance is SolarWinds.Orion.Web.UI.BaseResourceControl))
            {
                return new Dictionary<String, object>()
                    {
                        { "Result", null },
                        { "DataResultState", "JustMessage" },
                        { "Message", GetErrorMessageHtml(new ErrorInspectorDetails() { Title = "Unable to execute code from another class than BaseResourceControl for security reason.", Error = new Exception("Security Exception.") }) },
                    };
            }
            
            bool secureMethod = false;
            foreach (var attr in method.GetCustomAttributes(false))
            {
                if (attr is WSAsyncExecuteMethodAttribute)
                {
                    WSAsyncExecuteMethodAttribute attribute = attr as WSAsyncExecuteMethodAttribute;
                    if (attribute.IsSecureMethod)
                    {
                        secureMethod = true;
                        break;
                    }
                }
            }

            if (!secureMethod)
            {
                return new Dictionary<String, object>()
                    {
                        { "Result", null },
                        { "DataResultState", "JustMessage" },
                        { "Message", GetErrorMessageHtml(new ErrorInspectorDetails() { Title = "Unable to execute method, because it is not secure.", Error = new Exception("Security Exception.") }) },
                    };
            }

            System.Reflection.PropertyInfo propDataResultState = type.GetProperty("DataResultState");
            System.Reflection.PropertyInfo propImmediateLoading = type.GetProperty("ImmediateLoading");
            if (propImmediateLoading != null)
            {
                propImmediateLoading.SetValue(instance, false, null);
            }

            object result = null;
            String errorMessage = String.Empty;
            try
            {
                result = method.Invoke(instance, new object[] { parameters });

                if (result is DataTable)
                    EscapingChars(result as DataTable);

            }
            catch (Exception ex)
            {
                errorMessage = ex.InnerException != null ? ex.InnerException.Message : String.Empty;
                log.Error(String.Format("Exception occured when invoking [{0}] \r\nMessage: {1}", delegateServerMethod, errorMessage), ex);
            }

            TaskResult task = new TaskResult() { Outcome = TaskOutcome.Success };
            if (propDataResultState != null)
                task = (TaskResult)propDataResultState.GetValue(instance, null);
            if (task == null)
            {
                task = new TaskResult() { Outcome = TaskOutcome.Unknown };
            }
            
            switch (task.Outcome)
            {
                case TaskOutcome.Error:
                    return new Dictionary<String, object>()
                    {
                        { "Result", null },
                        { "DataResultState", "JustMessage" },
                        { "Message", GetErrorMessageHtml(new ErrorInspectorDetails() { Title = String.Concat(errorMessage, " ", result is DataTable ? null : result).Trim(), Error = new Exception(task.ErrorMessage) }) },
                    };
                case TaskOutcome.Unreachable:
                    return new Dictionary<String, object>()
                    {
                        { "Result", null },
                        { "DataResultState", "JustMessage" },
                        { "Message", GetErrorMessageHtml(new ErrorInspectorDetails() { Title = "All WSUS servers are down. No data to display.", Error = new Exception(task.ErrorMessage) }) },
                    };
                case TaskOutcome.Unauthorized:
                    return new Dictionary<String, object>()
                    {
                        { "Result", null },
                        { "DataResultState", "JustMessage" },
                        { "Message", String.Empty},
                    };
                case TaskOutcome.PartialSuccess:
                    return new Dictionary<String, object>()
                    {
                        { "Result", result },
                        { "DataResultState", "Message" },
                        { "Message", GetErrorMessageHtml(new ErrorInspectorDetails() { Title = "Resource displays just partial information", Error = new Exception(String.Concat("Some of WSUS servers are down.\n\n", task.ErrorMessage)) }) },
                    };
                case TaskOutcome.Unknown:
                case TaskOutcome.Success:
                default:
                    return new Dictionary<String, object>()
                    {
                        { "Result", result },
                        { "DataResultState", null },
                        { "Message", null },
                    };
            }
        }
        catch (Exception ex)
        {
            string message = ex.InnerException != null ? ex.InnerException.Message : String.Empty;
            log.Error(String.Format("Exception occured when tried to reflexing method [{0}] \r\nMessage: {1}", delegateServerMethod, message), ex);

            return new Dictionary<String, object>()
                {
                    { "Result", null },
                    { "DataResultState", "JustMessage" },
                    { "Message", GetErrorMessageHtml(new ErrorInspectorDetails() { Title = "Exception occured when tried to reflexing method", Error = ex.InnerException }) },
                };
        }
    }

    /// <summary>
    /// Method produces ErrorMessageWhich will be displayed to user if error happened during asynchronous executing task.
    /// </summary>
    /// <param name="detail"></param>
    /// <returns>Formatted html message which will be displayed to user</returns>
    private object GetErrorMessageHtml(ErrorInspectorDetails detail)
    {
        using (StringWriter stringWriter = new StringWriter())
        {
            using (HtmlTextWriter writer = new HtmlTextWriter(stringWriter))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "sw-suggestion sw-suggestion-fail");
                writer.AddStyleAttribute(HtmlTextWriterStyle.Width, "100%");
                writer.AddStyleAttribute(HtmlTextWriterStyle.PaddingRight, "0px");
                writer.AddStyleAttribute(HtmlTextWriterStyle.PaddingLeft, "0px");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "sw-suggestion-icon");
                    writer.RenderBeginTag(HtmlTextWriterTag.Span);
                    writer.RenderEndTag(); //Span

                    writer.AddStyleAttribute(HtmlTextWriterStyle.PaddingLeft, "26px");
                    writer.RenderBeginTag(HtmlTextWriterTag.Span);
                    {
                        writer.Write(detail.Title);
                        writer.Write(" &raquo; ");
                    }
                    writer.RenderEndTag(); //Span

                    writer.AddAttribute(HtmlTextWriterAttribute.Href, detail.ErrorPageResolved);
                    writer.AddAttribute(HtmlTextWriterAttribute.Alt, "Warning");
                    writer.AddAttribute(HtmlTextWriterAttribute.Target, "swerror");
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "sw-suggestion-link");
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    {
                        writer.Write("View Details");
                    }
                    writer.RenderEndTag(); //A
                }
                writer.RenderEndTag(); //div

                HttpContext.Current.Cache["sw.error." + detail.Id] = detail;
                return stringWriter.ToString();
            }
        } 
    }

    private void EscapingChars(DataSet dataSet)
    {
        foreach (DataTable table in dataSet.Tables)
        {
            EscapingChars(table);
        }
    }

    private void EscapingChars(DataTable table)
    {
        for (Int32 i = 0; i < table.Rows.Count; i++)
        {
            for (Int32 j = 0; j < table.Columns.Count; j++)
            {
                if (table.Rows[i][j] is DataSet)
                {
                    EscapingChars(table.Rows[i][j] as DataSet);
                }
                else if (table.Rows[i][j] is DataTable)
                {
                    EscapingChars(table.Rows[i][j] as DataTable);
                }
                else
                {
                    try
                    {
                        table.Rows[i][j] = System.Web.HttpUtility.HtmlEncode(table.Rows[i][j].ToString());
                    }
                    catch (Exception ex)
                    {
                        throw new ArgumentException(String.Format("Data Type '{0}' is not implemented in /Orion/PM/Services/WSAsyncExecuteTasks.asmx/EscapingChars. Error: {1}", table.Rows[i][j].GetType().Name, ex));
                    }
                }
            }
        }
    }

    private static readonly Log log = new Log();
}

