﻿<%@ WebService Language="C#" Class="WebIntegration" %>

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.Services;
using Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.Models.WebIntegration;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class WebIntegration  : WebService
{
    private static readonly Log Log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    [WebMethod]
    public dynamic EnableWebIntegration(string userIdentification, string userPassword)
    {
        AuthorizationChecker.AllowAdmin();

        string error = "";
        var failureReason = "Integration has failed with unknown error"; // do not localize
        
        using (var proxy = _blProxyCreator.Create(CoreBLExceptionHandler))
        {
            try
            {
                var token = proxy.ConfigureUserWebIntegration(HttpContext.Current.Profile.UserName, userIdentification, userPassword);

                if (!String.IsNullOrEmpty(token.Token))
                {
                    return new
                        {
                            success = true,
                            token = token
                        };
                }
                error = CoreWebContent.WEBDATA_JD0_12;
            }
            catch (Exception ex)
            {
                // process possible error here
                var coreFault = ex as FaultException<CoreFaultContract>;

                if (coreFault != null && coreFault.Detail != null)
                {
                    failureReason = coreFault.Detail.Message ?? failureReason;
                    error = !String.IsNullOrEmpty(coreFault.Detail.LocalizedMessage) ? coreFault.Detail.LocalizedMessage : CoreWebContent.WEBDATA_JD0_11;
                }
            }
        }

        return new
            {
                success = false,
                message = error,
                failureReason = failureReason
            };
    }

    [WebMethod]
    public dynamic GetUserToken()
    {
        AuthorizationChecker.AllowAdmin();
        
        using (var proxy = _blProxyCreator.Create(CoreBLExceptionHandler))
        {
            try
            {
                var username = HttpContext.Current.Profile.UserName;
                
                if (!proxy.IsUserWebIntegrationAvailable(username))
                {
                    return new
                        {
                            enabled = false
                        };
                }

                var token = proxy.GetUserWebIntegrationToken(username);

                return new
                    {
                        enabled = true,
                        token = token
                    };
            }
            catch (Exception ex)
            {
                Log.Error("Unable to get user token.", ex);
                return new
                {
                    enabled = false
                };
            }
        }
    }
    
    [WebMethod]
    public void DisableWebIntegration()
    {
        using (var proxy = _blProxyCreator.Create(CoreBLExceptionHandler))
        {
            proxy.DisableUserWebIntegration(HttpContext.Current.Profile.UserName);
        }
    }

    [WebMethod]
    public dynamic GetLAMInfo()
    {
        using (var proxy = _blProxyCreator.Create(CoreBLExceptionHandler))
        {
            LicenseAndManagementInfo info;
            try
            {
                info = proxy.GetLicenseAndMaintenanceSummary(HttpContext.Current.Profile.UserName);
            }
            catch (Exception ex)
            {
                Log.Error("Unable to get license and maintenance summary.", ex);
                return new {success = false};
            }
            return new
            {
                maintenanceExpiredCount = info.MaintenanceExpiredCount,
                maintenanceExpiringCount = info.MaintenanceExpiringCount,
                maintenanceActiveCount = info.MaintenanceActiveCount,
                evaluationCount = info.EvaluationExpiringCount,
                licenseLimitCount = info.LicenseLimitReachedCount,
                updatesCount = info.UpdatesAvailableCount,
                success = true
            };
        }
    }



    [WebMethod]
    public dynamic GetSupportCases()
    {
        using (var proxy = _blProxyCreator.Create(CoreBLExceptionHandler))
        {
            IEnumerable<SupportCase> cases;
            try
            {
                cases = proxy.GetSupportCases(HttpContext.Current.Profile.UserName);
            }
            catch (Exception ex)
            {
                Log.Error("Unable to get support cases.", ex);
                cases = null;
            }

            if (cases == null)
            {
                return new
                {
                    success = false
                };
            }
            
            var casesForJs = cases
                .OrderByDescending(i => i.LastUpdated)
                .Select(i => new
                {
                    title = i.Title,
                    url = i.CaseURL,
                    caseNumber = i.CaseNumber,
                    lastUpdate = i.LastUpdated.ToString("MMM d, yyyy", CultureInfo.InvariantCulture)
                })
                .ToArray();
            return new
            {
                success = true,
                cases = casesForJs
            };
        }
    }
    
    private static void CoreBLExceptionHandler(Exception ex)
    {
        Log.Error("Call to business layer failed.", ex);
    }
}