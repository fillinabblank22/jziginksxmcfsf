﻿<%@ WebService Language="C#" Class="CustomizeBreadcrumb" %>

using System.Text;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.EntityManager;
using SolarWinds.Orion.Web;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class CustomizeBreadcrumb : WebService
{
	[WebMethod]
    public void SaveBcCustomization(string customizationValue, string objectName)
	{
        switch (objectName.ToUpper())
        {
            case "NODES":
                SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.BreadcrumbNodeCustomization = customizationValue;
                break;
            case "INTERFACES":
                SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.BreadcrumbInterfaceCustomization = customizationValue;
                break;
            case "VOLUMES":
                SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.BreadcrumbVolumeCustomization = customizationValue;
                break;
            case "ACTIVEALERTS":
                SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.BreadcrumbActiveAlertsCustomization = customizationValue;
                break;
        }
	}

    [WebMethod]
    public string GetBcCustomization()
    {
        return SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.BreadcrumbNodeCustomization;
    }

    [WebMethod]
    public string GetListOfOptions(string objectName)
    {
        var options = string.Empty;   
        var custOption= string.Empty;  
        var customProperties = new StringBuilder();
        
        
        switch (objectName.ToUpper())
        {
            case "NODES":
                custOption = SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.BreadcrumbNodeCustomization;

                foreach (var prop in CustomPropertyMgr.GetPropNamesForTable("NodesCustomProperties", false))
				{
                    customProperties.AppendFormat("<option value='{0}' propertyType='{1}'>{0}</option>", prop, CustomPropertyMgr.GetTypeForProp("NodesCustomProperties", prop));
				}
                options = string.Format(@"<option value=''>{2}</option>{0}
							<option value='Vendor'>{3}</option>
							<option value='MachineType'>{4}</option>
							<option value='SNMPVersion'>{5}</option>
							<option value='Status'>{6}</option>
							<option value='Location'>{7}</option>
							<option value='Contact'>{8}</option>
							<option value='Community'>{9}</option>
							<option value='RWCommunity'>{10}</option>
							{1}", (EntityManager.InstanceWithCache.IsThereEntity("Orion.NPM.EW.Entity")) ? string.Format("<option value='EnergyWise'>{0}</option>",Resources.CoreWebContent.WEBCODE_VB0_338)
							: string.Empty, customProperties,Resources.CoreWebContent.WEBCODE_VB0_337,Resources.CoreWebContent.WEBCODE_VB0_124,Resources.CoreWebContent.WEBDATA_VB0_16,
							Resources.CoreWebContent.WEBCODE_VB0_340,Resources.CoreWebContent.WEBDATA_VB0_14,Resources.CoreWebContent.WEBDATA_VB0_128,Resources.CoreWebContent.WEBDATA_VB0_129,Resources.CoreWebContent.WEBCODE_VB0_190,Resources.CoreWebContent.WEBCODE_VB0_339);
                break;
            case "INTERFACES":
                custOption = SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.BreadcrumbInterfaceCustomization;
                
                foreach (var prop in CustomPropertyMgr.GetPropNamesForTable("Interfaces", false))
                {
                    customProperties.AppendFormat("<option value='{0}' propertyType='{1}'>{0}</option>", prop, CustomPropertyMgr.GetTypeForProp("Interfaces", prop));
                }
                options = string.Format(@"<option value=''>{1}</option>
							<option value='InterfaceType'>{2}</option>
							<option value='Status'>{3}</option>
							<option value='ObjectSubType'>{4}</option>
							<option value='PollInterval'>{5}</option>
                            {0}", customProperties,Resources.CoreWebContent.WEBCODE_VB0_343,Resources.CoreWebContent.WEBDATA_AK0_160,Resources.CoreWebContent.WEBDATA_VB0_14,Resources.CoreWebContent.WEBCODE_VB0_342,Resources.CoreWebContent.WEBCODE_VB0_341);
                break;
            case "VOLUMES":
                custOption = SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.BreadcrumbVolumeCustomization;
                
                foreach (var prop in CustomPropertyMgr.GetPropNamesForTable("Volumes", false))
                {
                    customProperties.AppendFormat("<option value='{0}' propertyType='{1}'>{0}</option>", prop, CustomPropertyMgr.GetTypeForProp("Volumes", prop));
                }
                options = string.Format(@"<option value=''>{1}</option>
							<option value='VolumeType'>{2}</option>
							<option value='Status'>{3}</option>
							<option value='PollInterval'>{4}</option>
                            {0}", customProperties,Resources.CoreWebContent.WEBCODE_VB0_344,Resources.CoreWebContent.WEBDATA_VB0_41,Resources.CoreWebContent.WEBDATA_VB0_14,Resources.CoreWebContent.WEBCODE_VB0_341);
                break;
            case "ACTIVEALERTS":
                custOption = SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.BreadcrumbActiveAlertsCustomization;

                foreach (var prop in CustomPropertyMgr.GetPropNamesForTable("AlertConfigurationsCustomProperties", false))
                {
                    customProperties.AppendFormat("<option value='CP_{0}' propertyType='{1}'>{0}</option>", prop, CustomPropertyMgr.GetTypeForProp("AlertConfigurationsCustomProperties", prop));
                }

                options = string.Format(@"<option value=''>{0}</option>
                                          <option value='AlertName'>{1}</option>
                                          <option value='RelatedNode'>{2}</option>
                                          <option value='AcknowledgedBy'>{3}</option>
                                          <option value='Severity'>{4}</option>",
                                          Resources.CoreWebContent.WEBCODE_PS0_18, Resources.CoreWebContent.WEBCODE_PS0_19, Resources.CoreWebContent.WEBCODE_PS0_20, Resources.CoreWebContent.WEBCODE_PS0_21, Resources.CoreWebContent.WEBCODE_PS0_22);
                options += customProperties.ToString();
                break;
            default:
                options = string.Format(@"<option value=''>{0}</option>", string.Format(Resources.CoreWebContent.WEBCODE_VB0_345,objectName));
                break;
        }
        
        if (!string.IsNullOrEmpty(custOption))
        {
            options = options.Replace(string.Format("value='{0}'", custOption), string.Format("value='{0}' selected='true'", custOption));
        }
        return options;
    }
}