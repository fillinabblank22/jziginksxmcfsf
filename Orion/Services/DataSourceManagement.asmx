﻿<%@ WebService Language="C#" Class="DataSourceManagement" %>

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Linq;
using System.Text;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Orion.Core.Reporting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Reporting;
using SolarWinds.Reporting.Models.Selection;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Logging;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class DataSourceManagement : WebService
{

    private static Log _log = new Log();

    private DataSource ParseDataSourceFromString(string dataSource)
    {
        var serializer = new DataContractJsonSerializer(typeof(DataSource));
        return  ((DataSource)serializer.ReadObject(new MemoryStream(Encoding.UTF8.GetBytes(dataSource)))).ApplyQueryContextIfPresent();
    }

    [Serializable]
    public struct ColumnDataInfo
    {
        public string Name;
        public string Type;
    }

    [Serializable]
    public struct PreviewData
    {
        public ColumnDataInfo[] ColumnInfo;
        public PageableDataTable Data;
    }
    
    [WebMethod]
    public PreviewData? GetQueryResults(string dataSource)
    {
        try
        {
            var source = ParseDataSourceFromString(dataSource);            
            var dataTableQuery = DataSourceFactory.GetDataTableQuery(source, InformationServiceProxy.CreateV3Creator());
            if (dataTableQuery.Filter == null)
                dataTableQuery.Filter = new Filter { Limit = new Limit { Count = WebsiteSettings.ReportingTablePreviewMaxRowCount, Mode = LimitMode.TopEntries } };
            else if (dataTableQuery.Filter.Limit == null)
                dataTableQuery.Filter.Limit = new Limit { Count = WebsiteSettings.ReportingTablePreviewMaxRowCount, Mode = LimitMode.TopEntries };

            var dt = dataTableQuery.Query();
            var columnInfo = new List<ColumnDataInfo>();
                
            for (var i = 0; i < dt.Columns.Count; i++)
            {
                columnInfo.Add(new ColumnDataInfo
                                    {Name = dt.Columns[i].Caption, Type = dt.Columns[i].DataType.FullName});
            }

            var previewData = new PreviewData
                                    {
                                        ColumnInfo = columnInfo.ToArray(),
                                        Data = new PageableDataTable(dt, dt.Rows.Count)
                                    };
            return previewData;
        }
        catch (Exception ex)
        {
            if (_log.IsDebugEnabled)
                _log.Debug(string.Format("GetQueryResults failed for dataSource {0} ", dataSource), ex);
            return null;
        }
    }

    [WebMethod]
    public bool ValidateQuery(string dataSource)
    {
        try
        {
            var source = ParseDataSourceFromString(dataSource);            
            var dataTableQuery = DataSourceFactory.GetDataTableQuery(source, InformationServiceProxy.CreateV3Creator());
            dataTableQuery.Validate();
            return true;
        }
        catch (Exception ex)
        {
            if (_log.IsDebugEnabled)
                _log.Debug(string.Format("ValidateQuery failed for dataSource {0} ", dataSource), ex);
            return false;
        }

    }
[WebMethod]
    public string GetReportQuery(string dataSource)
    {
        try
        {
            var source = ParseDataSourceFromString(dataSource);            
            var dataTableQuery = DataSourceFactory.GetDataTableQuery(source, InformationServiceProxy.CreateV3Creator());
            dataTableQuery.Validate();
            return dataTableQuery.QueryCommandText();
        }
        catch (Exception ex)
        {
            _log.Error(string.Format("Getting query failed for dataSource {0} ", dataSource), ex);
            return string.Format("ValidateQuery failed for dataSource {0} with exception {1} ", dataSource, ex.Message);
        }
    }
}