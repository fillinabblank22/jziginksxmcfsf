﻿<%@ WebService Language="C#" Class="ExpressionBuilderDataProvider" %>

using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Runtime.Serialization;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Core.Reporting;
using SolarWinds.Orion.Web.Model.ExpressionBuilder;
using SolarWinds.Reporting.Models.Selection;
using System.Linq;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class ExpressionBuilderDataProvider  : System.Web.Services.WebService {

    [DataContract]
    public class EnumValue
    {
        [DataMember]
        public string RefId { get; set; }
        [DataMember]
        public string DisplayName { get; set; }
    }


    [WebMethod]
    public EnumValue[] GetFieldEnumValues(DataSource dataSource, string fieldRefId) {

        var fields = DataSourceFactory.GetFieldProvider(dataSource, InformationServiceProxy.CreateV3Creator());
        var provider = DataSourceFactory.GetApplicationTypeRepository();
        var helper = new ApplicationTypeHelper(provider);

        // get proper type
        var type = helper.GetEnumerated(fields.GetField(fieldRefId));

        // transform into serializable 
        return type.GetValueDisplayName().Select(
            n => new EnumValue()
            {
                RefId = n.Key,
                DisplayName = n.Value
            }).ToArray();
    }

    [WebMethod]
    public string[] GetFieldAutocompleteValues(DataSource dataSource, string fieldRefId, string searchTerm) 
    {

        var fetcher = DataSourceFactory.GetDataTableQuery(dataSource, InformationServiceProxy.CreateV3Creator());

        // set up projection
        fetcher.Fields = new QField[] 
        { 
            new QField{ FieldRef = fieldRefId, RefID = Guid.NewGuid(), Aggregation = Aggregate.NotSpecified }
        };
        
        // 
        fetcher.Filter = new Filter()
        {
            Limit = new Limit() { Mode = LimitMode.TopEntries, Count = 20, Distinct = true },
            Expression = new Expr()
            {
                NodeType = ExprType.Operator,
                Value = (new OpStartWith(false)).RefID,
                Child = new Expr[] {
                    new Expr() { NodeType = ExprType.Field, Value = fieldRefId },
                    new Expr() { NodeType = ExprType.Constant, Value = searchTerm }         
                }
            }
        };
        
        // query
        var dt = fetcher.Query();
                
        // just first column
        return dt.Rows.OfType<System.Data.DataRow>().Select(n => n[0].ToString()).Distinct(StringComparer.OrdinalIgnoreCase).ToArray();
    }
    
    [WebMethod]
    public GetMasterEntitesResponse GetMasterEntities()
    {
        var fetcher = DataSourceFactory.GetMasterEntityProvider(InformationServiceProxy.CreateV3Creator());
        
        var entities = fetcher.GetDynamicSourceItems().OrderBy(item => item.DisplayName);
      
        var response = new GetMasterEntitesResponse
        {
            Entities = entities.ToArray()
        };

        return response;
    }

  
    [WebMethod]
    public Field GetField(DataSource dataSource, string fieldRefId)
    {
        var fields = DataSourceFactory.GetFieldProvider(dataSource, InformationServiceProxy.CreateV3Creator());
        return fields.GetField(fieldRefId);
    }

    [WebMethod]
    public string GetDatePickerRegionalSettings()
    {
        return SolarWinds.Orion.Web.DatePickerRegionalSettings.GetDatePickerRegionalSettings();
    }

}
