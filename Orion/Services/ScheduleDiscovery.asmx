﻿<%@ WebService Language="C#" Class="ScheduleDiscovery" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Linq;
using SolarWinds.Orion.Web.Discovery;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class ScheduleDiscovery  : System.Web.Services.WebService {

    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();        

    [WebMethod(EnableSession = true)]
    public void SelectInterface(int nodeID, int profileID, int interfaceID, bool isSelected)
    {
        try
        {
            DiscoveryNodeUI discoveryNodeUI = ScheduleDiscoveryHelper.ScheduleDiscoveryResults.SingleOrDefault(n => n.DiscoveryNode.NodeID == nodeID && n.DiscoveryNode.ProfileID == profileID);
            if (discoveryNodeUI == null)
            {
                log.ErrorFormat("Discovery Node NodeID={0},ProfileID={0} isn't in session", nodeID, profileID);
                throw new ApplicationException("Interface could not be selected");
            }
            discoveryNodeUI.DiscoveryNode.Interfaces.SingleOrDefault(i => i.InterfaceID == interfaceID).IsSelected = isSelected;
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }
                
    }

    [WebMethod(EnableSession = true)]
    public void SelectVolume(int nodeID, int profileID, int volumeID, bool isSelected)
    {
        try
        {
            DiscoveryNodeUI discoveryNodeUI = ScheduleDiscoveryHelper.ScheduleDiscoveryResults.SingleOrDefault(n => n.DiscoveryNode.NodeID == nodeID && n.DiscoveryNode.ProfileID == profileID);
            if (discoveryNodeUI == null)
            {
                log.ErrorFormat("Discovery Node NodeID={0},ProfileID={0} isn't in session", nodeID, profileID);
                throw new ApplicationException("Volume could not be selected");
            }
            discoveryNodeUI.DiscoveryNode.Volumes.SingleOrDefault(i => i.VolumeID == volumeID).IsSelected = isSelected;
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public void SelectGroupItem(int nodeID, int profileID, Guid groupID, int itemID, bool isSelected)
    {
        try
        {
            DiscoveryNodeUI discoveryNodeUI = ScheduleDiscoveryHelper.ScheduleDiscoveryResults.SingleOrDefault(n => n.DiscoveryNode.NodeID == nodeID && n.DiscoveryNode.ProfileID == profileID);
            if (discoveryNodeUI == null)
            {
                log.ErrorFormat("Discovery Node NodeID={0},ProfileID={0} isn't in session", nodeID, profileID);
                throw new ApplicationException("Item could not be selected");
            }

            var group = discoveryNodeUI.GetGroupDefinition(groupID);
            var item = discoveryNodeUI.DiscoveryNode.NodeResult.PluginResults.SelectMany(n => n.GetDiscoveredObjects()).Where(group.Group.IsMyGroupedObjectType).ElementAt(itemID);
            item.IsSelected = isSelected;
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;        
        }
    }

    [WebMethod(EnableSession = true)]
    public void SelectIgnoredInterface(int interfaceID, bool isSelected)
    {
        try
        {            
            var ignoredInterfaces =
                from n in ScheduleDiscoveryHelper.IgnoredNodes
                from i in n.DiscoveryIgnoredNode.IgnoredInterfaces
                where i.ID == interfaceID
                select i;
            ignoredInterfaces.First().IsSelected = isSelected;
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

    }

    [WebMethod(EnableSession = true)]
    public void SelectIgnoredVolume(int volumeID, bool isSelected)
    {
        try
        {            
            var ignoredVolumes =
                from n in ScheduleDiscoveryHelper.IgnoredNodes
                from v in n.DiscoveryIgnoredNode.IgnoredVolumes
                where v.ID == volumeID
                select v;
            ignoredVolumes.First().IsSelected = isSelected;
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

    } 
    
    
    [WebMethod(EnableSession = true)]
    public void SelectIgnoredItem(int ignoredNodeID, Guid groupID, int itemIndex, bool isSelected)
    {
        try
        {
            var node = ScheduleDiscoveryHelper.IgnoredNodes.Where(n => n.DiscoveryIgnoredNode.ID == ignoredNodeID).FirstOrDefault();
            if( node == null)
            {
                log.ErrorFormat("Discovery Ignored Node NodeID={0}, isn't in session", ignoredNodeID);
                throw new ApplicationException("Item could not be selected");            
            }

            var group = node.GetGroupDefinition(groupID);
            
            var item = node.DiscoveryIgnoredNode.NodeResult.PluginResults
                    .SelectMany(n => n.GetDiscoveredObjects())
                    .Where(n => group.Group.IsMyGroupedObjectType(n))
                    .ElementAt(itemIndex);

            item.IsSelected = isSelected;            
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }
    }         
}

