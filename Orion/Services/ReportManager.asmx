﻿<%@ WebService Language="C#" Class="ReportManager" %>

using System;
using System.Configuration;
using System.Globalization;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Xml;
using System.Xml.Linq;
using Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Reporting.DAL;
using SolarWinds.Orion.Core.Reporting.Import;
using SolarWinds.Orion.Core.Reporting.Models.WebResource;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Reporting.Models;
using SolarWinds.Reporting.Models.Charts;
using SolarWinds.Reporting.Models.Data;
using SolarWinds.Reporting.Models.Tables;
using Serializer = SolarWinds.Orion.Core.Common.Serializer;
using SolarWinds.Reporting.Models.Layout;
using SolarWinds.Orion.Core.Common.Swis;
using ID = System.Guid;

[Serializable]
public class ResourceConfigOptions
{
    [DataMember] public Guid RefId;
    [DataMember] public Guid ConfigId;
    [DataMember] public string Title;
    [DataMember] public string Category;
    [DataMember] public string IconPath { get; set; }
    [DataMember] public string EditButtonText { get; set; }
    [DataMember] public string Path;
    [DataMember] public string RenderProvider;
    [DataMember] public bool SupportDtFilter;
    [DataMember] public bool RequiresEditOnAdd;
    [DataMember] public ProviderInterfaceOptions[] SupportedInterfaces;
}

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ReportManager : WebService
{
    private static Log log = new Log();

    [WebMethod]
    public PageableDataTable GetReports(string property, string type, string value, string search, string resourceFilter, Boolean swisBasedReportsOnly)
    {
        int pageSize = 0;
        int startRowNumber = 0;

        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        if (pageSize == 0)
            pageSize = 20;

        string sortOrder = WebConstants.AllReportsDefaultSortOrder;
        if (!string.IsNullOrEmpty(sortColumn))
        {
            sortOrder = string.Format("{0} {1}",
                                      sortColumn.StartsWith("CustomProperties.")
                                          ? string.Format("r.{0}", sortColumn)
                                          : sortColumn, sortDirection);
        }

        var parameters = GetQueryConditions(property, type, value, search, resourceFilter, swisBasedReportsOnly);
        string whereCondition = parameters.Item1;
        Dictionary<string, object> queryParams = parameters.Item2;
        var cpDataTable = GetReportCustomProperties();
        var cpList = new List<string>();
        foreach (DataRow row in cpDataTable.Rows)
        {
            cpList.Add(row[0].ToString());
        }
        string queryString =
            string.Format(
                @"SELECT r.ReportID, r.Name, 
                                                    CASE WHEN f.AccountID=@accountID THEN 'true' 
                                                    ELSE 'false' 
                                                    END AS Favorite, 
                                                    r.Title, r.Description, r.LegacyPath, r.ModuleTitle, r.LimitationCategory, r.Uri, r.Category, jd.JobsData,
                                                    CASE WHEN r.LegacyPath IS NULL THEN '{2}' ELSE '{3}' END AS Type
                                                    {4}
                                             FROM Orion.Report (nolock=true) r
                                             LEFT JOIN Orion.ReportFavorites (nolock=true) f ON (f.ReportID=r.ReportID AND f.AccountID=@accountID)
                                             LEFT JOIN Orion.ReportJobData (nolock=true) jd ON (jd.ReportID=r.ReportID) 
                                             {5}
                                             {0}
                                             ORDER BY {1}
                                             WITH ROWS @startRowNumber TO @endRowNumber",
                whereCondition, sortOrder, CoreWebContent.WEBDATA_IB0_166, CoreWebContent.WEBDATA_SO0_108,
                (cpList.Count > 0) ? string.Format(", r.CustomProperties.{0}", string.Join(", r.CustomProperties.", cpList.ToArray())) : string.Empty,
                (property.ToLower().Equals("jobsdisplayname") && !string.IsNullOrEmpty(value)) ?
                                                 @"LEFT JOIN Orion.ReportJobDefinitions (nolock=true) rjd ON (rjd.ReportID=r.ReportID)   
					           LEFT JOIN Orion.ReportJobs (nolock=true) rj ON (rjd.ReportJobID=rj.ReportJobID)" : string.Empty);

        queryParams[@"accountID"] = HttpContext.Current.Profile.UserName;
        queryParams[@"startRowNumber"] = startRowNumber + 1;
        queryParams[@"endRowNumber"] = startRowNumber + pageSize;

        string countString = string.Format(@"SELECT COUNT(r.ReportID) as Cnt 
                                             FROM Orion.Report (nolock=true) r 
                                             LEFT JOIN Orion.ReportFavorites (nolock=true) f ON (f.ReportID=r.ReportID AND f.AccountID=@accountID)
                                             LEFT JOIN Orion.ReportJobData (nolock=true) jd ON (jd.ReportID=r.ReportID)
                                             {1}
                                             {0}", whereCondition, (property.ToLower().Equals("jobsdisplayname") && !string.IsNullOrEmpty(value)) ?
                                                 @"LEFT JOIN Orion.ReportJobDefinitions (nolock=true) rjd ON (rjd.ReportID=r.ReportID)   
					           LEFT JOIN Orion.ReportJobs (nolock=true) rj ON (rjd.ReportJobID=rj.ReportJobID)" : string.Empty);

        log.Debug("Reports main grid query :" + queryString);

        DataTable rjDataTable;
        DataTable countTable;

        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                rjDataTable = service.Query(queryString, queryParams);
                countTable = service.Query(countString, queryParams);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

        return new PageableDataTable(rjDataTable, (countTable.Rows == null || countTable.Rows.Count == 0 ? 0 : (int)((countTable.Rows[0][0] is System.DBNull)? 0 : countTable.Rows[0][0])));
    }

    [WebMethod]
    public PageableDataTable GetReportsFromRemoteOrion(Int32 serverID, string search)
    {
        Int32 pageSize;
        Int32 startRowNumber;

        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        if (pageSize == 0) pageSize = 20;

        String sortColumn = Context.Request.QueryString["sort"];
        String sortDirection = Context.Request.QueryString["dir"];

        return DoGetReportsFromRemoteOrion(serverID, pageSize, startRowNumber, sortColumn, sortDirection, search);
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetAllReportsFromRemoteOrion(Int32 serverID, string search)
    {
        const Int32 PageSize = Int32.MaxValue;
        const Int32 StartRowNumber = 0;

        String sortColumn = Context.Request.QueryString["sort"];
        String sortDirection = Context.Request.QueryString["dir"];

        return DoGetReportsFromRemoteOrion(serverID, PageSize, StartRowNumber, sortColumn, sortDirection, search);
    }

    public int GetReportCountFromRemoteOrion(Int32 serverID, string search)
    {
        const String ReportsCountQueryTemplate = @"
            SELECT COUNT(r.ReportID) as Cnt 
            FROM Orion.Report (nolock=true) r 
            WHERE {0} {1}";
        try
        {
            var queryParams = new Dictionary<String, Object>();
            var searchCondition = String.Empty;
            if (!String.IsNullOrEmpty(search))
            {
                searchCondition = "and ((r.Title like @search) or (r.Description like @search))";
                queryParams[@"search"] = search.Replace("[", "[[]");
            }
            var reportsCountQuery = String.Format(ReportsCountQueryTemplate, GetReportQueryLimitation(false), searchCondition);
            var reportsCountTable = QueryRemoteOrion(serverID, reportsCountQuery, queryParams);

            var reportsCount = ((reportsCountTable.Rows == null || reportsCountTable.Rows.Count == 0) ? 0 : (int)((reportsCountTable.Rows[0][0] is DBNull) ? 0 : reportsCountTable.Rows[0][0]));
            return reportsCount;
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }
    }

    private PageableDataTable DoGetReportsFromRemoteOrion(Int32 serverID, int pageSize, int startRowNumber,
        string sortColumn, string sortDirection, string search)
    {
        const String ReportsQueryTemplate = @"
            SELECT r.ReportID, r.Name, 'false' as Favorite, r.Title, r.Description
            FROM Orion.Report (nolock=true) r
            WHERE Definition NOT LIKE '%<d2p1:Type>CustomSWQL</d2p1:Type>%' AND Definition NOT LIKE '%<d2p1:Type>CustomSWQL</d2p1:Type>%' AND {0} {1}
            ORDER BY {2}";

        const String ReportsCountQueryTemplate = @"
            SELECT COUNT(r.ReportID) as Cnt 
            FROM Orion.Report (nolock=true) r 
            WHERE Definition NOT LIKE '%<d2p1:Type>CustomSWQL</d2p1:Type>%' AND Definition NOT LIKE '%<d2p1:Type>CustomSWQL</d2p1:Type>%' AND {0} {1}";

        try
        {
            var queryParams = new Dictionary<String, Object>();

            var searchCondition = String.Empty;
            if (!String.IsNullOrEmpty(search))
            {
                searchCondition = "and ((r.Title like @search) or (r.Description like @search))";
                queryParams[@"search"] = search.Replace("[", "[[]");
            }

            var sortOrder = "r.Title, r.Description Asc";
            // if the sorting is using the Custom properties, then it shall be done in the SWQL query itself
            if ((!String.IsNullOrEmpty(sortColumn)) && (sortColumn.StartsWith("CustomProperties.")))
            {
                sortOrder = string.Format("{0} {1}", string.Format("r.{0}", sortColumn),sortDirection);
            }

            var reportsQuery = String.Format(ReportsQueryTemplate, GetReportQueryLimitation(false), searchCondition, sortOrder);
            var reportsTable = QueryRemoteOrion(serverID, reportsQuery, queryParams);

            // rest of the sorting is moved here since "LastImported" column is added only in the AddLastImportedDateDataToRemoteReportsList function [Refer EOCO-901]
            if ((!String.IsNullOrEmpty(sortColumn)) && (!sortColumn.StartsWith("CustomProperties.")))
            {
                sortOrder = string.Format("{0} {1}", sortColumn, sortDirection);
                reportsTable.DefaultView.Sort = sortOrder;
                reportsTable = reportsTable.DefaultView.ToTable();
            }

            // paging is done here, since "lastImported" column is added seperately
            // and if paging is done within the query then Sorting on "lastImported" will give invalid result while paging is done [Refer EOCO-901]
            if (reportsTable.Rows.Count > 0)
                reportsTable = reportsTable.AsEnumerable().Skip(startRowNumber).Take(pageSize).CopyToDataTable();

            var reportsCountQuery = String.Format(ReportsCountQueryTemplate, GetReportQueryLimitation(false), searchCondition);
            var reportsCountTable = QueryRemoteOrion(serverID, reportsCountQuery, queryParams);

            var reportsCount = (reportsCountTable.Rows == null || reportsCountTable.Rows.Count == 0 ? 0
                : (int) ((reportsCountTable.Rows[0][0] is DBNull) ? 0 : reportsCountTable.Rows[0][0]));
            return new PageableDataTable(reportsTable, reportsCount);
        }
        catch (EndpointNotFoundException enf)
        {
            // Log original crash and provide user-friendly error message which can be shown to user
            log.Error(enf);
            throw new Exception(CoreWebContent.RemoteReportsImport_UnableToContactServer, enf);
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }
    }

    [WebMethod]
    public PageableDataTable GetImportingReportsConflicts(Int32 serverID, Int32[] reportIDs)
    {
        const String RemoteReportsQueryTemplate = @"
            SELECT ReportID, Name, Definition
            FROM Orion.Report 
            WHERE ReportID in ({0})
            ORDER BY Title";

        try
        {
            if (serverID <= 0) throw new ArgumentException("serverID");
            if (reportIDs.Length == 0) throw new ArgumentException("reportIDs");


            string reportIDsCsv = String.Join(", ", reportIDs);

            string query = String.Format(RemoteReportsQueryTemplate, reportIDsCsv);
            DataTable reports = QueryRemoteOrion(serverID, query);

            Queue<Tuple<Int32, Guid>> remoteReports = new Queue<Tuple<Int32, Guid>>();
            foreach (DataRow row in reports.Rows)
            {
                Int32 reportID = Convert.ToInt32(row["ReportID"]);

                String reportDefinition = Convert.ToString(row["Definition"]);
                Guid reportGuid = GetReportGuid(reportDefinition);

                Tuple<int, Guid> remoteReport = Tuple.Create(reportID, reportGuid);
                remoteReports.Enqueue(remoteReport);
            }

            const String ConflictedReportsQueryTemplate = @"
            SELECT 
                {0} AS RemoteReportID, 
                Report.Title
            FROM Orion.Report as Report
                WHERE ([Report].[Definition] LIKE ('%' + '{1}' + '%'))";

            DataTable conflictsTable = null;

            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                while (remoteReports.Any())
                {
                    Tuple<int, Guid> remoteReportNext = remoteReports.Dequeue();

                    String conflictQuery = String.Format(CultureInfo.InvariantCulture, ConflictedReportsQueryTemplate, remoteReportNext.Item1, remoteReportNext.Item2);
                    DataTable currentConflictTable = service.Query(conflictQuery);

                    if (conflictsTable == null)
                    {
                        conflictsTable = currentConflictTable;
                    }
                    else
                    {
                        conflictsTable.Merge(currentConflictTable);
                    }

                }

                return new PageableDataTable(conflictsTable, conflictsTable.Rows.Count);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }
    }

    private Guid GetReportGuid(String reportDefinition)
    {
        using (var reader = new StringReader(reportDefinition))
        {
            var element = XElement.Load(reader);
            Report report = Loader.Deserialize(element);

            return report.ReportGuid;
        }
    }


    [WebMethod]
    public DataTable GetReportCustomProperties()
    {
        return CustomPropertyHelper.GetEntityProperties("Orion.ReportsCustomProperties");
    }

    [WebMethod]
    public int GetReportNumber(int reportId, string property, string type, string value, string sort, string direction, string resourceFilter, Boolean swisBasedReportsOnly)
    {
        string sortColumn = sort;
        string sortDirection = direction;

        string sortOrder = WebConstants.AllReportsDefaultSortOrder;
        if (!string.IsNullOrEmpty(sortColumn))
        {
            sortOrder = string.Format("{0} {1}",
                                      sortColumn.StartsWith("CustomProperties.")
                                          ? string.Format("r.{0}", sortColumn)
                                          : sortColumn, sortDirection);
        }

        var parameters = GetQueryConditions(property, type, value, string.Empty, resourceFilter, swisBasedReportsOnly);
        string whereCondition = parameters.Item1;
        Dictionary<string, object> queryParams = parameters.Item2;

        string queryString = string.Format(@"SELECT r.ReportID, 
                                                    CASE WHEN f.AccountID=@accountID THEN 'true' 
                                                    ELSE 'false' 
                                                    END AS Favorite, r.Title, r.Description, r.LegacyPath, r.ModuleTitle, r.LimitationCategory, r.Uri, r.Category,
                                                    CASE WHEN r.LegacyPath IS NULL THEN '{2}' ELSE '{3}' END AS Type
                                             FROM Orion.Report (nolock=true) r
                                             LEFT JOIN Orion.ReportFavorites (nolock=true) f ON (f.ReportID=r.ReportID AND f.AccountID=@accountID)
                                             LEFT JOIN Orion.ReportJobData (nolock=true) jd ON (jd.ReportID=r.ReportID)   
                                             {4}
                                             {0}
                                             ORDER BY {1}",
                                  whereCondition, sortOrder, CoreWebContent.WEBDATA_IB0_166, CoreWebContent.WEBDATA_SO0_108,
                                  (property.ToLower().Equals("jobsdisplayname") && !string.IsNullOrEmpty(value)) ?
                                                 @"LEFT JOIN Orion.ReportJobDefinitions (nolock=true) rjd ON (rjd.ReportID=r.ReportID)   
					           LEFT JOIN Orion.ReportJobs (nolock=true) rj ON (rjd.ReportJobID=rj.ReportJobID)" : string.Empty);

        queryParams[@"accountID"] = HttpContext.Current.Profile.UserName;
        log.Debug("Reports main grid query :" + queryString);
        DataTable rjDataTable;

        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                rjDataTable = service.Query(queryString, queryParams);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

        if (rjDataTable == null || rjDataTable.Rows == null || rjDataTable.Rows.Count == 0)
            return -1;

        int rowId = 0;
        foreach (DataRow row in rjDataTable.Rows)
        {
            if (Convert.ToUInt32(row["ReportID"]) == reportId)
                return rowId;

            rowId++;
        }

        return -1;
    }


    [WebMethod]
    public bool ChangeFavoriteState(string reportId, string value)
    {
        int id = Convert.ToInt32(reportId);
        var argList = new List<object> { id, HttpContext.Current.Profile.UserName };

        bool result = true;
        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
                service.Invoke("Orion.ReportFavorites",
                               Convert.ToBoolean(value) ? "AddReportFavoriteMark" : "DeleteReportFavoriteMark",
                               CustomPropertyHelper.GetArgElements(argList));
        }
        catch (Exception ex)
        {
            result = false;
            log.Error(ex);
            throw;
        }

        return result;
    }

    [WebMethod]
    public int DuplicateReport(string reportId)
    {
        int id = Convert.ToInt32(reportId);
        const string tableName = "ReportDefinitions";

        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                // emptying ReportGuid for duplication and
                // setting unique name for duplicated report e.g. Copy of Weekly Report
                var doc = XDocument.Parse(OrionReportHelper.GetReportbyID(id).Definition);
                var ns = (doc.Root == null) ? string.Empty : doc.Root.Name.Namespace;
                var el = doc.Descendants(ns + "ReportGuid").FirstOrDefault();
                if (el != null)
                    el.SetValue(Guid.Empty.ToString());

                var webReport = Loader.Deserialize(doc.Root);
                var newTitle = GetUniqueNameForReport(webReport.Header.Title, webReport.LimitationCategory);
                webReport.Name = newTitle;
                webReport.Header.Title = newTitle;
                var definition = Loader.Serialize(webReport);

                var argList = new List<object> { webReport.Name, webReport.Description, webReport.LimitationCategory, webReport.Category, webReport.Header.Title,
                    webReport.Header.SubTitle, definition, webReport.IsFavorite.ToString(), HttpContext.Current.Profile.UserName, HttpContext.Current.Profile.UserName };

                var resultXml = service.Invoke("Orion.Report", "CreateReport", CustomPropertyHelper.GetArgElements(argList));
                var newReportId = Convert.ToInt32(resultXml.InnerText);
                var cpNames = CustomPropertyMgr.GetPropNamesForTable(tableName, false);
                foreach (var cpName in cpNames)
                {
                    var cpVal = CustomPropertyMgr.GetCustomProp(tableName, cpName, id);
                    if (!string.IsNullOrEmpty(cpVal))
                    {
                        CustomPropertyMgr.SetCustomProp(tableName, cpName, newReportId, cpVal);
                    }
                }

                return newReportId;
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }
    }

    private string GetUniqueNameForReport(string oldName, string limitationCategory)
    {
        var allNames = ReportHelper.GetSimilarExistingReportNames(oldName, limitationCategory);
        string newName = string.Format(CoreWebContent.WEBDATA_TM0_299, oldName);
        int counter = 2;
        while (allNames.Any(name => name.Equals(newName)))
        {
            newName = string.Format(CoreWebContent.WEBDATA_TM0_300, counter, oldName);
            counter++;
        }

        return newName;
    }

    private string GetReportQueryLimitation(bool includeResourceBased, bool swisBasedReportsOnly = true, string resourceFilter = null)
    {
        var queryLimitation = new StringBuilder();
        var resourceFilters = new[] { "tableconfiguration", "chartconfiguration" };

        if (swisBasedReportsOnly || !includeResourceBased || resourceFilters.Contains(resourceFilter))
        {
            // 1. legacy path is not null for legacy reports
            queryLimitation.Append(" (r.LegacyPath is null) ");
        }

        if (swisBasedReportsOnly)
        {
            // 2. CustomSql - to not include sql based reports
            queryLimitation.Append("and (r.Definition not like '%:Type>CustomSQL<%') ");
        }

        if (!includeResourceBased)
        {
            // 3. WebResourceConfiguration - to not include resource-based reports
            queryLimitation.Append(" and (r.Definition not like '%:WebResourceConfiguration\"%') ");
        }

        if (!string.IsNullOrEmpty(queryLimitation.ToString()))
            return string.Format("( {0} )", queryLimitation.ToString());

        return string.Empty;
    }


    [WebMethod(EnableSession = true)]
    public PageableDataTable GetReportGroupValues(string property, string type, string resourceFilter, Boolean swisBasedReportsOnly)
    {
        if (string.IsNullOrEmpty(property))
            return null;

        if (property.Equals("legacy"))
            property = "LegacyPath";

        string queryString = "";
        string countQuery    = "";
        var queryParams = new Dictionary<string, object>();

        String whereCondition = GetReportQueryLimitation(true, swisBasedReportsOnly, resourceFilter);

        if (type.Equals("favorites", StringComparison.OrdinalIgnoreCase))
        {
            if (whereCondition != String.Empty)
            {
                whereCondition = " AND " + whereCondition;
            }

            queryString = @"
                        SELECT f.AccountID AS Value,
				               f.AccountID AS DisplayNamePlural,
				               COUNT(r.ReportID) AS Cnt
                        FROM Orion.Report (nolock=true) r 
                        LEFT JOIN Orion.ReportFavorites (nolock=true) f ON (f.ReportID=r.ReportID AND f.AccountID=@accountID)
			            WHERE ( f.AccountID=@accountID OR f.AccountID IS NULL ) " + whereCondition +
                        @"Group by f.AccountID
                        ORDER BY f.AccountID";

            countQuery = @"
                        SELECT  COUNT(r.ReportID) as Cnt FROM Orion.Report (nolock=true) r
                        LEFT JOIN Orion.ReportFavorites (nolock=true) f ON (f.ReportID=r.ReportID AND f.AccountID=@accountID)
			            WHERE ( f.AccountID=@accountID OR f.AccountID IS NULL ) " + whereCondition;

            queryParams[@"accountID"] = HttpContext.Current.Profile.UserName;
        }
        else if (type.Equals("jobsdisplayname", StringComparison.OrdinalIgnoreCase))
        {
            if (whereCondition != String.Empty)
            {
                whereCondition = "WHERE " + whereCondition;
            }

            queryString = string.Format(@"SELECT rj.Name as Value, IsNull(rj.Name, '{0}') as DisplayNamePlural, COUNT(r.ReportID) as Cnt
                        FROM Orion.Report (nolock=true) r
                        LEFT JOIN Orion.ReportJobDefinitions (nolock=true) rjd ON r.ReportID = rjd.ReportID
                        LEFT JOIN Orion.ReportJobs (nolock=true) rj ON rj.ReportJobID = rjd.ReportJobID
                        {1}
                        GROUP BY rj.Name
                        ORDER BY rj.Name", CoreWebContent.WEBCODE_IB0_13, whereCondition);

            countQuery = String.Format(@"SELECT  COUNT(ReportID) as Cnt FROM Orion.Report r {0}", whereCondition);
        }
        else
        {
            if (whereCondition != String.Empty)
            {
                whereCondition = "WHERE " + whereCondition;
            }

            queryString = string.Format(@"SELECT r.{0} as Value, r.{0} as DisplayNamePlural, COUNT(*) as Cnt 
            FROM Orion.Report (nolock=true) r 
            {1}
            GROUP BY r.{0} 
            ORDER BY r.{0}", property, whereCondition);

            countQuery = String.Format(@"SELECT  COUNT(ReportID) as Cnt FROM Orion.Report (nolock=true) r {0}",
                whereCondition);
        }

        log.Debug("Manage Reports main grid group query: " + queryString);
        DataTable cpDataTable;
        DataTable countTable;

        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                cpDataTable = service.Query(queryString, queryParams);
                countTable = service.Query(countQuery, queryParams);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

        DataTable dt = new DataTable();
        dt.Columns.Add("Value");
        dt.Columns.Add("DisplayNamePlural");
        dt.Columns.Add("Cnt");

        dt.Rows.Add("[All]", "[All]", countTable.Rows[0][0]);

        foreach (DataRow row in cpDataTable.Rows)
            dt.Rows.Add(row["Value"], row["DisplayNamePlural"], row["Cnt"]);

        return new PageableDataTable(dt, dt.Rows.Count);
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetObjectGroupProperties()
    {
        try
        {
            String query = @"SELECT Table, Field, DataType, MaxLength, StorageMethod, Description, TargetEntity
                             FROM Orion.CustomProperty (nolock=true)
                             WHERE TargetEntity='Orion.ReportsCustomProperties'";

            DataTable cpDataTable;
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
                cpDataTable = service.Query(query);


            DataTable dt = new DataTable();
            dt.Columns.Add("Value");
            dt.Columns.Add("Name");
            dt.Columns.Add("Type");

            dt.Rows.Add("", CoreWebContent.WEBCODE_AK0_70, "");
            dt.Rows.Add("favorites", CoreWebContent.WEBCODE_TM0_111, "favorites");
            dt.Rows.Add("ModuleTitle", CoreWebContent.WEBCODE_TM0_112, "");
            dt.Rows.Add("LimitationCategory", CoreWebContent.WEBCODE_RB0_1, "");
            dt.Rows.Add("legacy", CoreWebContent.WEBDATA_SO0_107, "");
            dt.Rows.Add("Category", CoreWebContent.WEBDATA_SO0_105, "");
            dt.Rows.Add("JobsDisplayName", CoreWebContent.WEBDATA_YK0_65, "jobsdisplayname");
            foreach (DataRow row in cpDataTable.Rows)
            {
                dt.Rows.Add("CustomProperties." + row["Field"], row["Field"], row["DataType"]);
            }

            return new PageableDataTable(dt.DefaultView.ToTable(), cpDataTable.Rows.Count);
        }
        catch (Exception ex)
        {
            log.ErrorFormat("GetEntityProperties: Error during getting entity properties - {0}", ex.ToString());
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public string RemoveReports(List<string> ids)
    {
        AuthorizationChecker.AllowReportManagement();

        StringBuilder returnMessage = new StringBuilder();
        try
        {
            OrionReportHelper.DeleteReports(ids.ConvertAll(Convert.ToInt32));
            OrionReportHelper.TurnOffReportJobsIfNoReports();
        }
        catch (SqlException sqlEx)
        {
            foreach (SqlError sqlError in sqlEx.Errors)
            {
                log.DebugFormat("Caught SQL exception in Report Manager while deleting ReportDefinition: \"{0}\". Exception details: {1}", sqlError.Message, sqlError);
            }
        }
        catch (Exception e)
        {
            log.DebugFormat("Caught exception in Report Manager while deleting ReportDefinition: \"{0}\". Exception details: {1}", e.Message, e);
        }
        return returnMessage.ToString();
    }

    private Tuple<string,Dictionary<string,object>> GetQueryConditions(string property, string type, string value, string search, string resourceFilter, Boolean swisBasedReportsOnly)
    {
        var queryParams = new Dictionary<string, object>();
        string whereCondition = "WHERE 1=1";

        if (property.Equals("legacy"))
        {
            property = "LegacyPath";
        }

        var querylimitation = GetReportQueryLimitation(true, swisBasedReportsOnly, resourceFilter);
        if (!string.IsNullOrEmpty(querylimitation))
        {
            whereCondition += String.Format(" AND {0}", querylimitation);
        }

        double dValue;
        DateTime dtValue;
        if (type.Equals("real") && Double.TryParse(value, NumberStyles.Float, CultureInfo.CurrentCulture, out dValue) || Double.TryParse(value, NumberStyles.Float, CultureInfo.InvariantCulture, out dValue))
        {
            value = dValue.ToString(CultureInfo.InvariantCulture);
        }
        else if (type.Equals("datetime") && (DateTime.TryParse(value, CultureInfo.CurrentCulture, DateTimeStyles.None, out dtValue) || DateTime.TryParse(value, CultureInfo.InvariantCulture, DateTimeStyles.None, out dtValue)))
        {
            value = dtValue.ToString(CultureInfo.InvariantCulture);
        }

        if (!string.IsNullOrEmpty(property) && !string.IsNullOrEmpty(value))
        {
            if (property.ToLower().Equals("favorites"))
            {
                if (value.ToLower().Equals("null") || value.ToLower().Equals("{empty}"))
                    whereCondition += " AND ( (f.AccountID<>@value) OR (f.AccountID IS NULL) )";
                else
                    whereCondition += " AND (f.AccountID=@value)";

                queryParams[@"value"] = HttpContext.Current.Profile.UserName;
            }
            else
            {
                if (value.ToLower().Equals("null"))
                    whereCondition += String.Format(" AND ({0} IS NULL)", (property.ToLower().Equals("jobsdisplayname")) ? "rj.Name" : "r." + property);
                else if (value.ToLower().Equals("{empty}"))
                    whereCondition += String.Format(" AND ({0}='')", (property.ToLower().Equals("jobsdisplayname")) ? "rj.Name" : "r." + property);
                else
                {
                    whereCondition += String.Format(" AND ({0}=@value)", (property.ToLower().Equals("jobsdisplayname")) ? "rj.Name" : "r." + property);
                    queryParams[@"value"] = value;
                }
            }
        }
        //getting selected columns to search on all selected columns
        var selectedColumns = WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName,
                                                          "ReportManager_SelectedColumns",
                                                          WebConstants.AllReportsDefaultSelectedColumns)
                                                          .Split(',').ToList();

        selectedColumns.Remove("Favorite"); // we don't need to search by this field
        selectedColumns.Remove("Type");

        var cpDataTable = GetReportCustomProperties();
        var cpList = new List<string>();
        foreach (DataRow row in cpDataTable.Rows)
        {
            cpList.Add(row[0].ToString());
        }

        if (!string.IsNullOrEmpty(search) && selectedColumns.Count > 0)
        {
            var searchCondition = new StringBuilder();
            var separator = string.Empty;

            for (int i = 0; i < selectedColumns.Count; i++)
            {
                // check if custom property still exists
                if (selectedColumns[i].StartsWith("CustomProperties.") && !cpList.Contains(selectedColumns[i].Replace("CustomProperties.", string.Empty)))
                    continue;
                if (selectedColumns[i].ToLower() == "jobsdisplayname")
                {
                    searchCondition.AppendFormat(" {0} jd.{1} LIKE @search", separator, selectedColumns[i]);
                }
                else
                {
                    searchCondition.AppendFormat(" {0} r.{1} LIKE @search", separator, selectedColumns[i]);
                }
                separator = "OR";
            }
            if (searchCondition.Length > 0)
                whereCondition += String.Format(" AND ({0})", searchCondition);

            queryParams[@"search"] = search.Replace("[", "[[]");
        }
        queryParams[@"accountID"] = HttpContext.Current.Profile.UserName;

        return new Tuple<string, Dictionary<string, object>>(whereCondition, queryParams);
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetAllSelectedReports(string property, string type, string value, string search, string resourceFilter, Boolean swisBasedReportsOnly)
    {
        var parameters = GetQueryConditions(property, type, value, search, resourceFilter, swisBasedReportsOnly);
        string whereCondition = parameters.Item1;
        Dictionary<string, object> queryParams = parameters.Item2;


        string selectString = string.Format(@"SELECT r.ReportID, r.Title, r.LegacyPath         
                                             FROM Orion.Report (nolock=true) r 
                                             LEFT JOIN Orion.ReportFavorites (nolock=true) f ON (f.ReportID=r.ReportID AND f.AccountID=@accountID)
                                             LEFT JOIN Orion.ReportJobData (nolock=true) jd ON (jd.ReportID=r.ReportID)
                                             {1}
                                             {0}", whereCondition, (property.ToLower().Equals("jobsdisplayname") && !string.IsNullOrEmpty(value)) ?
                                                 @"LEFT JOIN Orion.ReportJobDefinitions (nolock=true) rjd ON (rjd.ReportID=r.ReportID)   
					           LEFT JOIN Orion.ReportJobs (nolock=true) rj ON (rjd.ReportJobID=rj.ReportJobID)" : string.Empty);

        log.Debug("Reports main grid query :" + selectString);

        DataTable rjDataTable;

        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                rjDataTable = service.Query(selectString, queryParams);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }
        return new PageableDataTable(rjDataTable, rjDataTable.Rows.Count);
    }

    [WebMethod(EnableSession = true)]
    public OrionReportBase GetOrionReport(int reportId)
    {
        //AuthorizationChecker.AllowReportManagement();
        try
        {
            return OrionReportHelper.GetReportbyID(reportId);
        }
        catch (Exception e)
        {
            log.ErrorFormat("Caught exception in Report Manager while getting Report ID:{0}. Exception details: {1}", reportId, e);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public Report GetOrionReportDefinition(OrionReportBase report)
    {
        if (string.IsNullOrEmpty(report.Definition))
            throw new ArgumentNullException("Report definition cannot be null");

        return GetOrionReportDefinition(report.Definition);
    }

    [WebMethod(EnableSession = true)]
    public Report GetOrionReportDefinition(string definition)
    {
        try
        {
            return Serializer.FromXmlString<Report>(definition);
        }
        catch (Exception e)
        {
            log.ErrorFormat("Exception occured truing to get Report definition from string [{0}]. Exception details: {1}", definition, e);
            throw;
        }
    }

    // OrionReportBase.Definition = GetReportDefinitionString(definition);
    [WebMethod(EnableSession = true)]
    public string GetReportDefinitionString(Report definition)
    {
        try
        {
            return Serializer.ToXmlString(definition);
        }
        catch (Exception e)
        {
            log.ErrorFormat("Exception occured truing to get string from Report definition. Exception details: {0}", e);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public void UpdateOrionReport(OrionReportBase report)
    {
        AuthorizationChecker.AllowReportManagement();
        try
        {
            OrionReportHelper.UpdateReport(report);
        }
        catch (Exception e)
        {
            log.ErrorFormat("Caught exception in Report Manager while updating Report ID:{0}. Exception details: {1}", report.Id, e);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public void SetSessionData(string name, string value)
    {
        Session[name] = value;
    }

    [WebMethod(EnableSession = true)]
    public object GetSessionData(string name)
    {
        return Session[name];
    }

    [WebMethod(EnableSession = true)]
    public ResourceConfigOptions LoadResourceConfigOptions(string location)
    {
        try
        {
            var template = ResourceTemplateManager.CreateFromVirtualPath(location);
            var interfaces = new List<Type>();
            // combine requared and supported interfaces
            foreach (
                var requaredInterface in
                    template.RequiredInterfaces.Where(requaredInterface => !interfaces.Contains(requaredInterface)))
            {
                interfaces.Add(requaredInterface);
            }
            foreach (
                var supportedInterface in
                    template.SupportedInterfaces.Where(supportedInterface => !interfaces.Contains(supportedInterface)))
            {
                interfaces.Add(supportedInterface);
            }

            var configId = Guid.NewGuid();
            bool requiresEditOnAdd = false;
            if (template.ReportRenderProvider.Equals(SolarWinds.Reporting.Impl.Rendering.TableRenderer.Moniker, StringComparison.OrdinalIgnoreCase)
                || template.ReportRenderProvider.Equals( SolarWinds.Orion.Web.Reporting.ReportCharts.ReportChartRenderer.Moniker, StringComparison.OrdinalIgnoreCase))
            {
                requiresEditOnAdd = true;
            }

            var currentCulture = Thread.CurrentThread.CurrentUICulture.LCID;
            var resource = ResourceTemplate.TemplateCache.Where(item => item.Key.Item2.LCID == currentCulture && item.Key.Item1.Equals(location, StringComparison.OrdinalIgnoreCase)).Select(item => item.Value).FirstOrDefault();
            string categoryTitle = String.Empty;
            var classicCategoryPropertyName = StandardMetadataPropertyName.ClassicCategory.ToString();

            // Tooltip is composed of classic category title and subtitle.
            if (resource.MetadataProperties.Contains(classicCategoryPropertyName))
            {
                var classicCategoryMetadata = resource.MetadataProperties[classicCategoryPropertyName];

                categoryTitle = classicCategoryMetadata.PropertyValue;
            }

            return new ResourceConfigOptions
            {
                RefId = Guid.NewGuid(),
                ConfigId = configId,
                Title = template.Title,
                Category = categoryTitle,
                IconPath = String.Format("/Orion/images/Reports/{0}.png", template.LayoutIconPath),
                EditButtonText = template.EditButtonText,
                Path = template.ControlPath,
                RenderProvider = template.ReportRenderProvider,
                SupportDtFilter = template.SupportDateTimeFilter,
                RequiresEditOnAdd = requiresEditOnAdd,
                SupportedInterfaces = ObjectProvidersManager.GetProviderInterfacesOptions(interfaces)
            };
        }
        catch (ArgumentException e)
        {
            log.ErrorFormat(
                "Caught exception in while loading resource properties for '{0}'. Exception details: {1}",
                location, e);
            throw new ArgumentException(CoreWebContent.WEBDATA_TM0_285);
        }
        catch (Exception e)
        {
            log.ErrorFormat(
                "Caught exception in while loading resource properties for '{0}'. Exception details: {1}",
                location, e);
            throw;
        }
    }

    [Serializable]
    public class LayoutInfo
    {
        public Section[] Sections;
        public ResourceConfigOptions[] ResourceOptions;
    }

    [WebMethod(EnableSession = true)]
    public void DuplicateConfig(string configsSessionName, string configId, string newConfigId)
    {
        var configsData = GetSessionData(configsSessionName);
        if (configsData == null) return;

        var configs = (ConfigurationData[]) GetSessionData(configsSessionName);
        if (configs != null)
        {
            var list = new List<ConfigurationData>(configs);
            foreach (var configurationData in configs)
            {
                if (configurationData.RefId == new Guid(configId))
                {
                    var newConfigData = configurationData.CloneConfigurationData();
                    newConfigData.RefId = new Guid(newConfigId);
                    list.Add(newConfigData);
                    break;
                }

            }
            Session[configsSessionName] = list.ToArray();
        }
    }
    [WebMethod(EnableSession = true)]
    public void CreateConfig(string location, string configsSessionName, string configId)
    {
        try
        {
            var configsList = new List<ConfigurationData>();
            if (!string.IsNullOrEmpty(configsSessionName) && Session[configsSessionName] != null)
            {
                var configs = (ConfigurationData[]) Session[configsSessionName];
                configsList = new List<ConfigurationData>(configs);
            }

            var refId = new Guid(configId);
            var template = ResourceTemplateManager.CreateFromVirtualPath(location);
            var settings =
                template.InitialProperties.Select(
                    property => new ContextValue {Name = property.Key, Value = property.Value}).ToList();


            ConfigurationData configData;
            if (template.ReportRenderProvider.Equals(SolarWinds.Reporting.Impl.Rendering.TableRenderer.Moniker,
                                                     StringComparison.OrdinalIgnoreCase))
            {
                configData = new TableConfiguration {RefId = refId, DisplayTitle = template.Title};
            }
            else if (
                template.ReportRenderProvider.Equals(
                    SolarWinds.Orion.Web.Reporting.ReportCharts.ReportChartRenderer.Moniker,
                    StringComparison.OrdinalIgnoreCase))
            {
                configData = new ChartConfiguration {RefId = refId, DisplayTitle = template.Title};
            }
            else
            {
                // standard web resource
                configData = new WebResourceConfiguration
                {
                    RefId = refId,
                    DisplayTitle = template.Title,
                    Title = template.Title,
                    DisplaySubTitle = string.Empty,
                    SubTitle = string.Empty,
                    Settings = settings.ToArray(),
                    ResourceFile = template.ControlPath
                };
            }
            configsList.Add(configData);

            // update configuration date with new points
            if (!string.IsNullOrEmpty(configsSessionName))
                Session[configsSessionName] = configsList.ToArray();
        }
        catch(Exception ex)
        {
            log.ErrorFormat( "Caught exception in while creating Configuration Data for '{0}'. Exception details: {1}", location, ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public void RemoveConfigs(string configsSessionName, string[] configIds)
    {
        var configsData = GetSessionData(configsSessionName);
        if (configsData == null) return;

        var configs = configsData as ConfigurationData[];
        if (configs != null)
        {
            foreach (var configId in configIds)
            {
                configs = configs.Where(configurationData => configurationData.RefId != new Guid(configId)).ToArray();
            }
            Session[configsSessionName] = configs;
        }
    }

    [WebMethod(EnableSession = true)]
    public void RemoveConfig(string configsSessionName, string configId)
    {
        RemoveConfigs(configsSessionName, new[] { configId });
    }

    [WebMethod(EnableSession = true)]
    public LayoutInfo GetLayoutInfo(string sectionsSessionName, string configsSessionName)
    {
        var layoutInfo = new LayoutInfo();
        try
        {
            var configsData = GetSessionData(configsSessionName);
            if (configsData == null) return layoutInfo;

            var configs = (ConfigurationData[]) GetSessionData(configsSessionName);
            if (configs == null) return layoutInfo;

            var resourceOptions = new List<ResourceConfigOptions>();
            foreach (var config in configs)
            {
                var resPath = string.Empty;
                if (config is TableConfiguration)
                {
                    resPath = WebConstants.ReportTableWrapperPath;
                }
                else if (config is ChartConfiguration)
                {
                    resPath = WebConstants.ReportChartWrapperPath;
                }
                else if (config is WebResourceConfiguration)
                {
                    resPath = ((WebResourceConfiguration)config).ResourceFile;
                }

                var resOptions = LoadResourceConfigOptions(resPath);
                resOptions.ConfigId = config.RefId;
                if (!string.IsNullOrEmpty(config.DisplayTitle))
                    resOptions.Title = config.DisplayTitle;
                resourceOptions.Add(resOptions);
            }

            layoutInfo.ResourceOptions = resourceOptions.ToArray();
            layoutInfo.Sections = (Section[])OrionSerializationHelper.FromJSON(GetSessionData(sectionsSessionName).ToString(), typeof(Section[]));

            return layoutInfo;
        }
        catch (Exception e)
        {
            log.Error("Caught exception in while loading layout configuration. Exception details:", e);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public IList<object> GetReportPrintSizes()
    {
        var list = new List<object>();

        var reportSettings = new XmlDocument();
        reportSettings.Load(Path.Combine(OrionConfiguration.WebPath, @"Orion\Reports\ReportSettings.config"));

        XmlNode dpi = reportSettings.SelectSingleNode("root/dpi");

        if (dpi == null || dpi.Attributes == null)
            return list;

        list.Add(Convert.ToInt16(dpi.Attributes["value"].Value));

        XmlNodeList paperSizes = reportSettings.SelectNodes("root/paperSizes/paperSize");



        if (paperSizes == null)
            return list;

        foreach (XmlNode node in paperSizes)
            if (node.Attributes != null)
                list.Add(new {
                    DisplayName = node.Attributes["displayName"].Value,
                    Name = node.Attributes["name"].Value,
                    Width = Convert.ToInt16(node.Attributes["width"].Value),
                    Height = Convert.ToInt16(node.Attributes["height"].Value)
                });
        return list;
    }

    [WebMethod(EnableSession = true)]
    public string CreateReportInStorage()
    {
        Guid reportGuid = ReportStorage.Instance.SetReport(new Report());
        return reportGuid.ToString();
    }

    [WebMethod]
    public void KeepAlive() { }

    /// <summary>
    /// Method set state, whethet to show again dialog "What is new ..." in next access to page for creating report
    /// </summary>
    /// <param name="show">If true, dialog will be shown again, otherwise will not be shown.</param>
    [WebMethod]
    public void SetShowWhatIsNewInWebBasedReportDialog(bool show)
    {
        WebUserSettingsDAL.Set(HttpContext.Current.Profile.UserName, "DontShowWhatIsNewInReportDialog", Convert.ToString(!show));
    }

    [WebMethod]
    public ID ImportReports(Int32 serverID, ImportingReportParams[] importingReports)
    {
        ReportsImportManager importManager = new ReportsImportManager();
        ID sessionID = importManager.Import(serverID, importingReports);

        return sessionID;
    }

    [WebMethod]
    public ImportReportStatus GetImportReportsProgress(ID sessionID)
    {
        ReportsImportManager importManager = new ReportsImportManager();
        ImportReportStatus importReportStatus = importManager.GetImportStatus(sessionID);

        return importReportStatus;
    }

    [WebMethod]
    public Boolean CancelReportsImport(ID sessionID)
    {
        ReportsImportManager importManager = new ReportsImportManager();
        return importManager.CancelImport(sessionID);
    }

    [WebMethod]
    public PageableDataTable GetReportCells(int reportId, string resourceType)
    {
        try
        {
            var table = new DataTable();
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Value", typeof(Guid));
            var report = ReportDAL.GetReportById(reportId);
            foreach (var config in report.Configs)
            {
                switch (resourceType.ToLower())
                {
                    case "tableconfiguration":
                        var tableConfiguration = config as TableConfiguration;
                        if (tableConfiguration != null)
                        {
                            table.Rows.Add(string.IsNullOrEmpty(tableConfiguration.DisplayTitle) ?
                                CoreWebContent.Misc_ReportTableWrapper_Title_1 : tableConfiguration.DisplayTitle,
                                tableConfiguration.RefId);
                        }
                        break;
                    case "chartconfiguration":
                        var chartConfiguration = config as ChartConfiguration;
                        if (chartConfiguration != null)
                        {
                            table.Rows.Add(string.IsNullOrEmpty(chartConfiguration.DisplayTitle) ?
                                CoreWebContent.Misc_ReportChartWrapper_Title_1 : chartConfiguration.DisplayTitle, chartConfiguration.RefId);
                        }
                        break;
                }
            }

            return new PageableDataTable(table, table.Rows.Count);
        }
        catch (Exception ex)
        {
            log.Error(string.Format("Error while getting config data for report {0}.", reportId), ex);

        }
        return null;
    }

    private DataTable QueryRemoteOrion(int siteId, string query, Dictionary<string, object> parameters = null)
    {
        var proxyProvider = new OrionSiteSwisProxyProvider();
        using (SwisConnectionProxy remoteProxy = proxyProvider.CreateProxy(siteId))
        {
            return remoteProxy.Query(query, parameters);
        }
    }
}
