﻿<%@ WebService Language="C#" Class="TabsManager" %>



using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using Models = SolarWinds.Orion.Content.Models;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class TabsManager : WebService
{
    [WebMethod(EnableSession = true)]
    public void SetActiveTabId(string tabId)
    {
        SWTab.SetActiveTabId(tabId);
    }

    [WebMethod(EnableSession = true)]
    public Models.ViewGroup[] GetTabs()
    {
        var tabs = new TabManager().GetTabs(User.Identity.Name, true);
        var tabsObj = tabs.Select(tab => new Models.ViewGroup
        {
            Id = tab.TabId,
            Name = tab.TabName,
            DefaultTitle = GetTabText(tab.TabName),
            Views = GetViews(tab).ToList()
        });

        return tabsObj.ToArray();
    }

    private IEnumerable<Models.View> GetViews(Tab tab)
    {
        return tab.TabMenuBar.Select(menuBar => new Models.View
        {
            Id = menuBar.MenuItemID,
            Type = "legacy",
            DefaultTitle = menuBar.Title,
            Url = menuBar.LinkTarget.Trim(),
            Description = menuBar.Description,
            OpenInNewWindow = menuBar.OpenInNewWindow,
            IsCustom = !menuBar.IsSystem
        });
    }

    private string GetTabText(string key)
    {
        const string managerId = "Core.Strings";
        const string prefix = "NavigationTab";

        var registrar = ResourceManagerRegistrar.Instance;
        string lookupkey = registrar.CleanResxKey(prefix, key);

        // look up core last, and default to the incoming key.
        return registrar.SearchAll(lookupkey, new[] { managerId }) ?? key;
    }
}
