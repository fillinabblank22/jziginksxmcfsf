﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/OrionMinReqs.master" AutoEventWireup="true" Inherits="SolarWinds.Orion.Web.WorldMap.Service" EnableSessionState="ReadOnly" %>

<asp:Content ContentPlaceHolderID="BodyContent" Runat="Server">

<%-- todo: upscale this to asmx --%>

<pre>
    Ops:
        get-poi
            params: op=get-poi, longitude, latitude, zoom, width, height, [ clause, resourceid ]
            return: { success: true, data: array(GeoPOI) }
    
        get-bounded-poi
            params: op=get-bounded-poi,  neLat,neLng,swLat,swLng,includeGroups,includeNodes,filterGroups,filterNodes});
            return: { success: true, data: array(GeoPOI) }

        set-poi
            params: op=set-poi
            post: { changes: { Longitude: xx, Longitude: xx, Items: arrayof(GeoItem) } }
            return: { success: true }

</pre>
</asp:Content>
