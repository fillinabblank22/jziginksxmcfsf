﻿<%@ WebService Language="C#" Class="DependenciesTree" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.PackageManager;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Dependencies;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using Node=SolarWinds.Orion.NPM.Web.Node;
using Container=SolarWinds.Orion.Web.Containers.Container;
using System.Linq;
using SolarWinds.Orion.Core.Common.Swis;
using SettingsDAL = SolarWinds.Orion.Web.DAL.SettingsDAL;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class DependenciesTree : WebService
{

    private static readonly Log log = new Log();

    public class DependenciesTreeError : Exception
    {
        protected DependenciesTreeError(string message, Exception inner) : base(message, inner) { }
        protected DependenciesTreeError(string message) : base(message) { }        
    }
    
    public class BadFilterError : DependenciesTreeError
    {
        internal BadFilterError(string message, Exception inner) : base(message, inner) { }        
    } 
    
    public class IncorrectNetObjectIdError : DependenciesTreeError
    {
        internal IncorrectNetObjectIdError(string message) : base(message) { }
    }

    [WebMethod(EnableSession = true)]
    public string BuildTree(int resourceId, string rootId, object[] keys, string netObj)
    {
        ResourceInfo resource = ResourceManager.GetResourceByID(resourceId);
        Context.Items[typeof(ViewInfo).Name] = resource.View;
        var parentFilter = string.IsNullOrEmpty(resource.Properties["ParentFilter"]) ? string.Empty : resource.Properties["ParentFilter"];
        var childFilter = string.IsNullOrEmpty(resource.Properties["ChildFilter"]) ? string.Empty : resource.Properties["ChildFilter"];
        var dependencyFilter = string.IsNullOrEmpty(resource.Properties["DependencyFilter"]) ? string.Empty : resource.Properties["DependencyFilter"];

        try
        {
            StringWriter stringWriter = new StringWriter();
            HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);

            BuildTreeSection(htmlWriter, resource, rootId, keys.Length + 1, keys, netObj, parentFilter, childFilter, dependencyFilter);

            return stringWriter.ToString();
        }
        catch (FaultException ex)
        {
            if (string.IsNullOrEmpty(parentFilter) && string.IsNullOrEmpty(childFilter) && string.IsNullOrEmpty(dependencyFilter))
            {
                log.ErrorFormat("Exception in BuildTree: {0}", ex);
                throw;
            }
            log.ErrorFormat("Exception in BuildTree: Bad filter '{1}' specified - {0}", ex, string.Format("{0} {1} {2}", parentFilter, childFilter, dependencyFilter));
            throw new BadFilterError(CoreWebContent.DependenciesTree_BadFilterError + string.Format("{0} {1} {2}", parentFilter, childFilter, dependencyFilter), ex);
        }
        catch (Exception ex)
        {
            log.ErrorFormat("Exception in BuildTree: {0}", ex);
            throw;
        }
    }

    private void BuildTreeSection(HtmlTextWriter writer, ResourceInfo resource, string rootId, int treeLevel, object[] keys, string netObj, string parentFilter, string childFilter, string dependencyFilter)
    {
        TreeStateManager manager = new TreeStateManager(Context.Session, resource.ID);

        if (treeLevel > 1)
        {
            int id;
            if (keys.Length > 0 && int.TryParse(keys[0].ToString(), out id))
            {
                writer.AddStyleAttribute(HtmlTextWriterStyle.PaddingLeft, "16px");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);

                BuildNodeList(writer, id);

                writer.RenderEndTag();
            }
        }
        else
        {
            DataTable dependencies = null;
            string uri = string.Empty;

            if (!string.IsNullOrEmpty(netObj))
            {
                int nId;
                if (int.TryParse(NetObjectHelper.GetObjectID(netObj), out nId))
                {
                    var type = NetObjectHelper.GetNetObjectEntity(netObj);
                    using (var dal = new DependenciesDAL())
                    {
                        uri = dal.GetUriForEntity(type, nId, false);
                        dependencies = dal.GetAllDependenciesFiltered(nId, type, parentFilter, childFilter, dependencyFilter);
                    }

                    foreach (DependencyCacheItem depItem in DependencyCache.GetDependencyCacheItems(netObj, parentFilter, childFilter, dependencyFilter))
                    {
                        DataRow row = dependencies.NewRow();
                        if (dependencies.Columns.Contains("DependencyId")) row["DependencyId"] = depItem.DependencyId;
                        if (dependencies.Columns.Contains("Name")) row["Name"] = depItem.DependencyName;
                        if (dependencies.Columns.Contains("ParentUri")) row["ParentUri"] = depItem.DependencyParentUri;
                        if (dependencies.Columns.Contains("ChildUri")) row["ChildUri"] = depItem.DependencyChildUri;
                        if (dependencies.Columns.Contains("Status")) row["Status"] = depItem.DependencyStatus;
                        if (dependencies.Columns.Contains("Type")) row["Type"] = depItem.DependencyType;
                        if (dependencies.Columns.Contains("AutoManaged")) row["AutoManaged"] = depItem.DependencyAutoManaget;
                        
                        try
                        {
                            dependencies.Rows.Add(row);
                        }
                        catch (Exception ex)
                        {
                            log.ErrorFormat("Exception in BuildTreeSection: {0}", ex);
                            throw;
                        }
                    }
                }
                else
                {
                    throw new IncorrectNetObjectIdError(CoreWebContent.DependenciesTree_IncorrectNetObjectIdError);
                }
            }
            else
            {
                using (var dal = new DependenciesDAL())
                using (var swisErrorsContext = new SwisErrorsContext())
                {
                    dependencies = dal.GetAllDependenciesFiltered(parentFilter, childFilter, dependencyFilter);

                    var errorMessages = swisErrorsContext.GetErrorMessages();
                    string swisError = string.Empty;
                    if (errorMessages != null && errorMessages.Any())
                    {
                        var control = new System.Web.UI.UserControl();
                        var swisfErrorControl =
                            control.LoadControl("~/Orion/SwisfErrorControl.ascx") as BaseSwisfErrorControl;
                        swisError = BaseSwisfErrorControl.GetSwisfErrorControlHtmlString(
                            errorMessages.Select(
                                error => new SolarWinds.Orion.Web.Federation.SwisErrorMessage(error)).ToList(),
                            swisfErrorControl);
                    }

                    writer.Write(swisError);
                }
            }

            var index = 0;
            var isTypeExists = dependencies.Columns.Contains("Type");
            var isStripe = false;

            if (isTypeExists)
            {
                IEnumerable<DataRow> dependenciesRows = dependencies.AsEnumerable();

                IEnumerable<DataRow> dataRows =
                    dependenciesRows.Where(
                        row =>
                        SwisUriComparer.SwisUriEquals(row.Field<string>("ParentUri"), uri) ||
                        row.Field<string>("Type") == "Parent").ToList();
                foreach (var row in dataRows)
                {
                    string childId = string.Format("{0}-{1}", rootId, index++);
                    string displayKey = string.Format(Resources.CoreWebContent.WEBCODE_AK0_68, HttpUtility.HtmlEncode(row["Name"].ToString().Trim()));
                    var key = row["DependencyId"].ToString();
                    var parentObjectId = UriHelper.GetNetObjectId(row["ParentUri"].ToString());
                    string groupStatus = (dependencies.Columns.Contains("Status")) ? row["Status"].ToString() : null;

                    RenderGroup(writer, resource, childId, displayKey, ArrayAppend(keys, key), manager, netObj, parentObjectId, isStripe, groupStatus);
                    isStripe = !isStripe;
                }

                dataRows =
                    dependenciesRows.Where(
                        row =>
                        SwisUriComparer.SwisUriEquals(row.Field<string>("ChildUri"), uri) ||
                        row.Field<string>("Type") == "Child").ToList();
                foreach (var row in dataRows)
                {
                    string childId = string.Format("{0}-{1}", rootId, index++);
                    string displayKey = string.Format(Resources.CoreWebContent.WEBCODE_AK0_69, HttpUtility.HtmlEncode(row["Name"].ToString().Trim()));
                    var key = row["DependencyId"].ToString();
                    var parentObjectId = UriHelper.GetNetObjectId(row["ParentUri"].ToString());
                    string groupStatus = (dependencies.Columns.Contains("Status")) ? row["Status"].ToString() : null;

                    RenderGroup(writer, resource, childId, displayKey, ArrayAppend(keys, key), manager, netObj, parentObjectId, isStripe, groupStatus);
                    isStripe = !isStripe;
                }
            }
            else
            {
                foreach (DataRow row in dependencies.Rows)
                {
                    string childId = string.Format("{0}-{1}", rootId, index++);
                    string displayKey = HttpUtility.HtmlEncode(row["Name"].ToString().Trim());
                    var key = row["DependencyId"].ToString();
                    var parentObjectId = UriHelper.GetNetObjectId(row["ParentUri"].ToString());
                    string groupStatus = (dependencies.Columns.Contains("Status")) ? row["Status"].ToString() : null;

                    RenderGroup(writer, resource, childId, displayKey, ArrayAppend(keys, key), manager, netObj, parentObjectId, isStripe, groupStatus);
                    isStripe = !isStripe;
                }
            }
        }
    }

    private void BuildNodeList(HtmlTextWriter writer, int dependencyId)
    {
        DataTable dependencies;
        using (var dal = new DependenciesDAL())
        {
            dependencies = dal.GetDependency(dependencyId);
        }

        foreach (DataRow row in dependencies.Rows)
        {
            RenderNode(writer, row, true);
            writer.WriteBreak();
            RenderNode(writer, row, false);
            writer.WriteBreak();
        }
    }

    private string GetDefaultPropertyName(string netObjectType)
    {
        var propertyName = "Caption";
        switch (netObjectType)
        {
            case "Orion.Groups":
            case "Orion.Container":
            case "Orion.APM.Application":
            case "Orion.APM.IIS.ApplicationPool":
                propertyName = "Name";
                break;
        }
        return propertyName;
    }
    
    private void RenderNode(HtmlTextWriter writer, DataRow row, bool isParent)
    {
       
        var uri = (isParent) ? row["ParentUri"].ToString() : row["ChildUri"].ToString();

        var netObjectType = UriHelper.GetUriType(uri);
        var netObjectId = UriHelper.GetNetObjectId(uri);

        NetObject netObj = NetObjectFactory.Create(netObjectId);

        var propertyName = GetDefaultPropertyName(netObjectType);
        
        if (netObj != null)
        {
            writer.BeginRender();
            writer.AddAttribute(HtmlTextWriterAttribute.Href, BaseResourceControl.GetViewLink(netObj));

            if (((ProfileCommon)Context.Profile).ToolsetIntegration && netObj is Node)
            {
                Node node = (netObj as Node);
                writer.AddAttribute("IP", node.IPAddress.ToString());
                writer.AddAttribute("NodeHostname", node.Hostname);

                if (SolarWinds.Orion.Core.Common.RegistrySettings.AllowSecureDataOnWeb())
                {
                    writer.AddAttribute("Community", node.CommunityString);
                }
                else
                {
                    string attrValue = FormatHelper.GetGuidParamString(WebCommunityStringsDAL.GetCommunityStringGuid(node.CommunityString));
                    writer.AddAttribute("Community", attrValue);
                }
            }

            writer.RenderBeginTag(HtmlTextWriterTag.A);            
            var text = string.Format("{0} - {1}", (isParent) ? Resources.CoreWebContent.WEBCODE_AK0_66 : Resources.CoreWebContent.WEBCODE_AK0_67, netObj.ObjectProperties[propertyName]);
            RenderStatus(writer, GetStatusIconPath(netObj), string.Empty);
            writer.WriteEncodedText(text);
            writer.RenderEndTag(); // a
            writer.EndRender();
        }
    }

    private static void RenderStatus(HtmlTextWriter writer, string iconPath, string alt)
    {
        if (string.IsNullOrEmpty(iconPath)) return;
        writer.BeginRender();

        writer.AddAttribute(HtmlTextWriterAttribute.Class, "StatusIcon");

        writer.AddAttribute(HtmlTextWriterAttribute.Alt, alt);
        writer.AddAttribute(HtmlTextWriterAttribute.Src, iconPath);
        writer.RenderBeginTag(HtmlTextWriterTag.Img);
        writer.RenderEndTag();

        writer.EndRender();
    }

    private static void RenderGroup(HtmlTextWriter writer, ResourceInfo resource, string rootId, string key, object[] keys, TreeStateManager manager, string netObj, string parentNetObjectId, bool isStripe, string groupStatus=null)
    {
        string toggleId = rootId + "-toggle";
        string contentsId = rootId + "-contents";

        writer.BeginRender();

        string keysArray = FormatArray(keys);
        bool needsExpanding = manager.IsExpanded(keysArray);

        if (isStripe)
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "ZebraStripe");
        writer.RenderBeginTag(HtmlTextWriterTag.Div);

        writer.AddAttribute(HtmlTextWriterAttribute.Onclick,
            string.Format("ORIONDependencies.DependenciesTree.Click('{0}', {1}, {2}, '{3}'); return false;", rootId, resource.ID, FormatArray(keys), netObj));
        writer.AddAttribute(HtmlTextWriterAttribute.Href, "#");

        if (needsExpanding)
            writer.AddAttribute("expand", "1");

        writer.RenderBeginTag(HtmlTextWriterTag.A);

        writer.AddAttribute(HtmlTextWriterAttribute.Src, "/Orion/images/Button.Expand.gif");
        writer.AddAttribute(HtmlTextWriterAttribute.Id, toggleId);
        writer.AddAttribute(HtmlTextWriterAttribute.Class, "Toggle");
        writer.RenderBeginTag(HtmlTextWriterTag.Img);
        writer.RenderEndTag(); // img

        NetObject parentNetObject = NetObjectFactory.Create(parentNetObjectId);

        RenderStatus(writer, GetStatusIconPath(parentNetObject, groupStatus), string.Empty);

        writer.Write(key);
        writer.RenderEndTag(); // a

        writer.WriteBreak();

        writer.AddAttribute(HtmlTextWriterAttribute.Id, contentsId);
        writer.AddStyleAttribute(HtmlTextWriterStyle.Display, "none");
        writer.AddStyleAttribute(HtmlTextWriterStyle.PaddingLeft, "12px");
        writer.RenderBeginTag(HtmlTextWriterTag.Div);
        writer.RenderEndTag(); // div
        writer.RenderEndTag(); // div

        writer.EndRender();
    }

    private static string FormatArray(object[] values)
    {
        var serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(values.GetType());
        using (MemoryStream ms = new MemoryStream())
        {
            serializer.WriteObject(ms, values);
            return Encoding.UTF8.GetString(ms.ToArray());
        }
    }

    private static object[] ArrayAppend(object[] array, object toAppend)
    {
        object[] newArray = new object[array.Length + 1];
        array.CopyTo(newArray, 0);
        newArray[array.Length] = toAppend;
        return newArray;
    }

    private static void BLExceptionHandler(Exception ex)
    {
        log.Error(ex);
        throw ex;
    }

    [WebMethod(EnableSession = true)]
    public void IgnoreDependencies(List<int> ids)
    {
        AuthorizationChecker.AllowNodeManagement();

        using (var proxy = new DependencyProxy(BLExceptionHandler))
        {
            try
            {
                if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
                {
                    // don't allow user to change dependencies
                }
                else
                {
                    proxy.DeleteDependencies(ids);
                }
            }
            catch (Exception ex)
            {
                log.ErrorFormat("IgnoreDependencies: Error during ignore dependencies - {0}", ex.ToString());
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public void RestoreDependencies(List<int> ids)
    {
        using (var proxy = new DependencyProxy(BLExceptionHandler))
        {
            try
            {
                if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
                {
                    // don't allow user to change dependencies
                }
                else
                {
                    proxy.DeleteIgnoredAutoDependencies(ids);
                }
            }
            catch (Exception ex)
            {
                log.ErrorFormat("RestoreDependencies: Error during restore dependencies - {0}", ex.ToString());
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public void SetAutoDependencySetting(bool enabled)
    {
        AuthorizationChecker.AllowNodeManagement();

        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            // Don't allow user to change dependencies.
            return;
        }

        SettingsDAL.SetValue("SWNetPerfMon-Settings-AutoDependency Enabled", enabled);
    }

    [WebMethod(EnableSession = true)]
    public bool GetAutoDependencySetting()
    {
        var setting = SettingsDAL.GetSetting("SWNetPerfMon-Settings-AutoDependency Enabled");
        return Convert.ToBoolean(setting.SettingValue);
    }
        
    [WebMethod(EnableSession = true)]
    public int RemoveDependencies(List<int> ids)
    {
        using (IDependencyProxy proxy = new DependencyProxy(BLExceptionHandler))
        {
            try
            {
                if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
                {
                    // don't allow user to change dependencies
                    return 0;
                }
                else
                {
                    return proxy.DeleteDependencies(ids);
                }
            }
            catch (Exception ex)
            {
                log.ErrorFormat("RemoveDependencies: Error during deleting dependencies - {0}", ex.ToString());
                return -1;
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetDependencies(string property, string type, string value)
    {
        return GetDependencies(property, type, value, false);
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetIgnoredDependencies(string property, string type, string value)
    {
        return GetDependencies(property, type, value, true);
    }

    private PageableDataTable GetDependencies(string property, string type, string value, bool ignored)
    {
        int pageSize = 0;
        int startRowNumber = 0;

        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        int.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        int.TryParse(Context.Request.QueryString["limit"], out pageSize);

        var allowInterfaces = PackageManager.InstanceWithCache.IsPackageInstalled("Orion.Interfaces");

        using (var dal = new DependenciesDAL())
        using (var swisErrorsContext = new SwisErrorsContext())
        {
            DataTable dataTable = null;

            int totalCount = 0;

            if (ignored)
            {
                dataTable = dal.GetPagedIgnoredDependencies(sortColumn, sortDirection, startRowNumber, pageSize,
                    property, type, value, out totalCount);
            }
            else
            {
                dataTable = dal.GetPagedDependencies(sortColumn, sortDirection, startRowNumber, pageSize, property, type,
                    value, out totalCount);
            }

            var errorMessages = swisErrorsContext.GetErrorMessages();
            string swisError = string.Empty;
            if (errorMessages != null && errorMessages.Any())
            {
                var control = new System.Web.UI.UserControl();
                var swisfErrorControl =
                    control.LoadControl("~/Orion/SwisfErrorControl.ascx") as BaseSwisfErrorControl;
                swisError = BaseSwisfErrorControl.GetSwisfErrorControlHtmlString(
                    errorMessages.Select(error => new SolarWinds.Orion.Web.Federation.SwisErrorMessage(error)).ToList(),
                    swisfErrorControl);
            }

            return new PageableDataTable(dataTable, totalCount)
            {
                Metadata =
                    new
                        {
                            ErrorHtml = swisError
                        }
            };
        }
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetIgnoredDependencyGroupingValues()
    {
        var dt = new DataTable();
        dt.Columns.Add("Value");
        dt.Columns.Add("Name");
        dt.Columns.Add("Type");

        dt.Rows.Add("", CoreWebContent.WEBCODE_AK0_70, "");
        dt.Rows.Add("DisplayName", CoreWebContent.WEBDATA_AK0_203, "System.String");
        dt.Rows.Add("ParentObjects", CoreWebContent.WEBCODE_AK0_65, "System.String");

        return new PageableDataTable(dt, dt.Rows.Count);
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetIgnoredDependencyGroupValues(string property, string type)
    {
        using (var dal = new DependenciesDAL())
        {
            try
            {
                if (string.IsNullOrEmpty(property))
                    return null;

                var dt = dal.GetIgnoredDependencyGroupValues(property, type);
                return new PageableDataTable(dt, dt.Rows.Count);
            }
            catch (Exception ex)
            {
                log.ErrorFormat("GetIgnoredDependencyGroupValues: Error during getting group values - {0}", ex.ToString());
                return null;
            }
        }
    }
    
    [WebMethod(EnableSession = true)]
    public PageableDataTable GetDependencyGroupingValues()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Value");
        dt.Columns.Add("Name");
        dt.Columns.Add("Type");

        dt.Rows.Add("", Resources.CoreWebContent.WEBCODE_AK0_70, "");
        dt.Rows.Add("ParentStatus", Resources.CoreWebContent.WEBCODE_AK0_63, "System.Int32");
        dt.Rows.Add("ChildStatus", Resources.CoreWebContent.WEBCODE_AK0_64, "System.Int32");
        dt.Rows.Add("DisplayName", Resources.CoreWebContent.WEBDATA_AK0_203, "System.String");
        dt.Rows.Add("ParentObjects", Resources.CoreWebContent.WEBCODE_AK0_65, "System.String");
        dt.Rows.Add("AutoManaged", Resources.CoreWebContent.Dependencies_GroupByOrigin, "System.Boolean");

        return new PageableDataTable(dt, dt.Rows.Count);
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetDependencyGroupValues(string property, string type)
    {
        using (DependenciesDAL dal = new DependenciesDAL())
        {
            try
            {
                if (string.IsNullOrEmpty(property))
                    return null;
                
                DataTable dt = dal.GetDependencyGroupValues(property, type);
                return new PageableDataTable(dt, dt.Rows.Count);
            }
            catch (Exception ex)
            {
                log.ErrorFormat("GetDependencyGroupValues: Error during getting group values - {0}", ex.ToString());
                return null;
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public Dependency GetDependency(int id)
    {
        using (IDependencyProxy proxy = new DependencyProxy(BLExceptionHandler))
        {
            try
            {
                return proxy.GetDependency(id);
            }
            catch (Exception ex)
            {
                log.ErrorFormat("GetDependency: Error during getting dependency - {0}", ex.ToString());
                return null;
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public IList<ContainerMember> GetContainerMembers(int id)
    {
        using (ContainersDAL cdal = new ContainersDAL())
        {
            try
            {
                return new List<ContainerMember>(cdal.GetMembers(id));
            }
            catch (Exception ex)
            {
                log.ErrorFormat("GetContainerMembers: Error during getting container members - {0}", ex.ToString());
                return new List<ContainerMember>();
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public IList<ContainerMember> GetContainerMembersByUri(string uri)
    {
        return GetContainerMembers(UriHelper.GetId(uri));
    }


    [WebMethod(EnableSession = true)]
    public void UpdateDependency(Dependency dependency)
    {
        using (IDependencyProxy proxy = new DependencyProxy(BLExceptionHandler))
        {
            try
            {
                if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
                {
                    // don't allow user to change dependencies at demo server
                }
                else
                {
                    proxy.UpdateDependency(dependency);
                }
            }
            catch (Exception ex)
            {
                log.ErrorFormat("UpdateDependency: Error during updating dependency - {0}", ex.ToString());
            }
        }
    }

    private static string GetStatusIconPath(NetObject netObj, string objectStatus =null)
    {
        string path = null;
        string swisType = NetObjectFactory.GetSWISTypeByNetObject(netObj.NetObjectID);
        
        if (netObj is Node)
        {
            var status = (netObj as Node).Status;
            path = status.ToString("smallimgpath", null);
        }
        else if (netObj is Container)
        {
            path = string.Format("/Orion/StatusIcon.ashx?entity={0}&status={1}&size=small", "Orion.Groups", (int)(netObj as Container).Status.StatusId);
        }
        else
        {
            int statusNumber;
            var statusValue = netObj.GetProperty<object>("Status");
            if (statusValue!= null && int.TryParse(statusValue.ToString(), out statusNumber))
            {
                path = string.Format("/Orion/StatusIcon.ashx?entity={0}&status={1}&size=small", swisType, statusNumber);
            }
            else if (!string.IsNullOrEmpty(objectStatus))
            {
                path = string.Format("/Orion/StatusIcon.ashx?entity={0}&status={1}&size=small", swisType, objectStatus);
            }
        }

        return path;
    }
}

