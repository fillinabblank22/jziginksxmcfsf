﻿<%@ WebService Language="C#" Class="DiscoveryWizard" %>


using System;
using System.Globalization;
using System.Linq;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Threading.Tasks;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.SessionState;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.Core.Models.Credentials;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Models.Discovery;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Orion.Core.Models;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.Web.DAL;
using WebSettingsDAL = SolarWinds.Orion.Core.Common.WebSettingsDAL;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class DiscoveryWizard : WebService
{
    private const string ShowDiscoveryHintSettingName = "ShowDiscoveryHint";
    private const string ShowIncludeFutureOuHintSettingName = "ShowIncludeFutureOuHint";
    private const string CountOfComputersSessionTaskKey = "AdDiscoveryCntOfComputers_{0}";
    
    static readonly Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
    public enum ResultType
    {
        Success,
        ValidationError,
        ProcessingError
    }

    public enum TaskStatusResult
    {
        StillRunning = 0,
        SuccessfullyCompleted = 1,
        Failed = 2
    }

    public class Result
    {
        public ResultType Type { get; set; }
        public KeyValuePair<String, String>[] ValidationErrors { get; set; }
        public KeyValuePair<String, String>[] ProcessingErrors { get; set; }
        public string DC { get; set; }
        public List<OrganizationalUnit> OrganizationalUnits { get; set; }
        public List<OrganizationalUnitInfo> OrganizationalUnitsToDisplayInGrid { get; set; }
        public int NumberOfSelectedOUs { get; set; }
        public int WarningLimit { get; set; }
        public bool WarningByLicense { get; set; }
    }

    public class ActiveDirectoryEntry
    {


        // Index in list
        public int Index { get; set; }

        // Existing Windows Credential ID
        public int CredentialID { get; set; }

        // Existing Creds in profile from which Name, Username And Password was copied
        public int ProfileIndex { get; set; }

        // Reference to credential in Shared Credentials
        public int CredentialIndex { get; set; }
        public string HostName { get; set; }

        public List<OrganizationalUnitInfo> OrganizationalUnits { get; set; }

        public List<OrganizationalUnitInfo> OrganizationalUnitsToDisplayInGrid { get; set; }

        public int NumberOfSelectedOUs { get; set; }

        public bool NameChanged { get; set; }
        public string Name { get; set; }
        public bool UserNameChanged { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool PasswordChanged { get; set; }
        public bool ServersOnly { get; set; }
        public int DeviceCount { get; set; }
    }

    public class Credential
    {
        // Index in list
        public int Index { get; set; }
        // Existing Windows Credential ID
        public int ID { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }

    }

    private Result ValidateNewCredentials(ActiveDirectoryEntry entry, SerializableCredentialList profileCredentials)
    {
        // get windows accounts from storage
        CredentialManagerService credentialService = new CredentialManagerService();
        var allWindowsCreds = credentialService.GetWindowsCredentials();
        var profileCreds = profileCredentials.OfType<UsernamePasswordCredential>();

        if ((entry.CredentialID == 0) && (entry.ProfileIndex < 0) &&
            (profileCreds.Any(p => p.Name == entry.Name) || allWindowsCreds.Any(p => p.Name == entry.Name)))
        {
            return new Result
            {
                Type = ResultType.ValidationError,
                ValidationErrors = new KeyValuePair<string, string>[] {
                    new KeyValuePair<String, String>("Name", Resources.CoreWebContent.WEBDATA_AF0_82)
                }
            };
        };
        return new Result() { Type = ResultType.Success };
    }

    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    [WebMethod(EnableSession = true)]
    public List<Credential> ADGetCredentials(int self)
    {
        AuthorizationChecker.AllowNodeManagement();

        var configurationInfo = ConfigWorkflowHelper.DiscoveryConfigurationInfo;
        var coreConfiguration = configurationInfo.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>();

        CredentialManagerService service = new CredentialManagerService();

        // get windows accounts from storage
        List<CredentialManagerService.WindowsCredential> creds = service.GetWindowsCredentials();

        var list = coreConfiguration.Credentials.OfType<UsernamePasswordCredential>().ToList();
        var lookup = list.ToLookup(p => p.Name);

        var existingCreds = creds.Where(c => lookup[c.Name].FirstOrDefault() == null).Select(n => new Credential
        {
            ID = n.ID,
            Index = -1,
            Name = n.Name,
            UserName = n.Username,
        }).ToList();


        var profile = list.Select((n, i) => new Credential
        {
            ID = n.ID.HasValue ? n.ID.Value : 0,
            Index = i,
            Name = n.Name,
            UserName = n.Username,

        }).ToList();

        // merge it with what's in current profile        
        existingCreds.AddRange(profile);
        return existingCreds;
    }


    private ActiveDirectoryAccess GetAccessToken(ActiveDirectoryEntry entry)
    {
        DiscoveryConfiguration discovery = ConfigWorkflowHelper.DiscoveryConfigurationInfo;
        var config = discovery.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>();
        var list = config.ActiveDirectoryList;

        var creds = config.Credentials.OfType<UsernamePasswordCredential>().ToList();

        ActiveDirectoryAccess access = new ActiveDirectoryAccess
        {
            ProfileIndex = -1,
            HostName = entry.HostName,
            OrganizationalUnits = entry.OrganizationalUnits,
            OrganizationalUnitsToDisplayInGrid = entry.OrganizationalUnitsToDisplayInGrid,
            NumberOfSelectedOUs = entry.NumberOfSelectedOUs ,
            ServersOnly = entry.ServersOnly,
            DeviceCount = entry.DeviceCount
        };

        if (entry.CredentialID == 0)
        {
            access.CredentialID = 0;

            if (entry.ProfileIndex >= 0 && entry.ProfileIndex < creds.Count)
            {
                var existing = creds[entry.ProfileIndex];
                access.ProfileIndex = entry.ProfileIndex;
                access.Credential = existing;

            }
            else if (entry.CredentialIndex >= 0 && entry.CredentialIndex < creds.Count && !entry.PasswordChanged)
            {
                // Find by credential id

                var existing = creds[entry.CredentialIndex];

                access.Credential = existing;

            }
            else
            {
                // New

                access.Credential = new UsernamePasswordCredential
                {
                    Name = entry.Name,
                    Username = entry.UserName,
                    Password = entry.Password,
                    Description = string.Empty
                };

            }

        }
        else
        {
            access.Credential = null;
            access.CredentialID = entry.CredentialID;
        }

        return access;
    }

    private ActiveDirectoryAccess GetTestAccessToken(ActiveDirectoryEntry entry, DiscoveryConfiguration discovery)
    {
        var config = discovery.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>();
        var creds = config.Credentials.OfType<UsernamePasswordCredential>().ToList();

        ActiveDirectoryAccess access = new ActiveDirectoryAccess
        {
            ProfileIndex = -1,
            HostName = entry.HostName,
        };

        if (entry.CredentialID == 0 || (entry.PasswordChanged || entry.UserNameChanged))
        {
            access.CredentialID = 0;

            if (entry.ProfileIndex >= 0 && entry.ProfileIndex < creds.Count)
            {
                var existing = creds[entry.ProfileIndex];
                access.ProfileIndex = entry.ProfileIndex;
                access.Credential = existing;

            }
            else if (entry.CredentialIndex >= 0 && entry.CredentialIndex < creds.Count)
            {
                // Find by credential id

                var existing = creds[entry.CredentialIndex];

                var testCreds = new UsernamePasswordCredential
                {
                    Name = existing.Name,
                    Username = existing.Username,
                    Password = existing.Password
                };

                if (entry.PasswordChanged)
                {
                    testCreds.Password = entry.Password;
                }

                if (entry.UserNameChanged)
                {
                    testCreds.Username = entry.UserName;
                }

                if (entry.NameChanged)
                {
                    testCreds.Name = entry.Name;
                }

                access.Credential = testCreds;

            }
            else
            {
                // New

                access.Credential = new UsernamePasswordCredential
                {
                    Name = entry.Name,
                    Username = entry.UserName,
                    Password = entry.Password,
                    Description = string.Empty
                };

            }

        }
        else
        {
            access.Credential = null;
            access.CredentialID = entry.CredentialID;
        }

        return access;
    }



    [ScriptMethod]
    [WebMethod(EnableSession = true)]
    public Result ADTestConnection(ActiveDirectoryEntry entry)
    {
        ActiveDirectoryAccess access = GetTestAccessToken(entry, ConfigWorkflowHelper.DiscoveryConfigurationInfo);
        return _ValidateActiveDirectoryAccessWithOrgUnits(access);
    }

    private Result _ValidateActiveDirectoryAccess(ActiveDirectoryAccess access)
    {
        DiscoveryConfiguration discovery = ConfigWorkflowHelper.DiscoveryConfigurationInfo;
        using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, discovery.EngineID))
        {
            var result = businessProxy.ValidateActiveDirectoryAccess(access);
            return new Result
            {
                Type = result.IsValid ? ResultType.Success : ResultType.ProcessingError,
                ProcessingErrors = result.IsValid ? null : result.ErrorMessages.Select((e, i) => new KeyValuePair<String, String>(i.ToString(), e)).ToArray(),
                ValidationErrors = new KeyValuePair<string, string>[] { },
                DC = result.DC,
            };
        }
    }

    private Result _ValidateActiveDirectoryAccessWithOrgUnits(ActiveDirectoryAccess access)
    {
        DiscoveryConfiguration discovery = ConfigWorkflowHelper.DiscoveryConfigurationInfo;
        var config = discovery.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>();
        try
        {
            Result result = null;
            using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, discovery.EngineID))
            {
                var blResult = businessProxy.ValidateActiveDirectoryAccess(access);
                result = new Result
                {
                    Type = blResult.IsValid ? ResultType.Success : ResultType.ProcessingError,
                    ProcessingErrors = blResult.IsValid ? null : blResult.ErrorMessages.Select((e, i) => new KeyValuePair<String, String>(i.ToString(), e)).ToArray(),
                    ValidationErrors = new KeyValuePair<string, string>[] { },
                    DC = blResult.DC,
                    OrganizationalUnits = blResult.IsValid ? businessProxy.GetActiveDirectoryOrganizationUnits(access).OrderBy(ou => ou.Depth).ToList() : null,
                };
            }

            using (var swis = SwisConnectionProxyPool.GetCreator().CreateSwisConnection())
            {
                const string query = "SELECT AvailableCount FROM Orion.LicenseSaturation WHERE ElementType = @elementType";
                var adLimit = 1000;
                
                string  elementType = WellKnownElementTypes.Nodes;

                using (var data = swis.Query(query, new Dictionary<string, object>() { { "elementType", elementType } }))
                {
                    if (data.Rows.Count == 1)
                    {
                        adLimit = Convert.ToInt32(data.Rows[0][0]);
                        log.InfoFormat("Detected {0} available {1}", adLimit, elementType);
                    }
                    else
                    {
                        log.ErrorFormat("Cannot read number of available licenses fore {0} from SWIS. The query did return {1} result(s)", 
                                        elementType, data.Rows.Count);
                    }
                }
                result.WarningLimit = Math.Min(adLimit,
                                               SolarWinds.Orion.Web.DAL.WebSettingsDAL.GetValue("ADDiscovery_DeviceCountLimit", 1000));
                result.WarningByLicense = adLimit == result.WarningLimit;
            }
            return result;

        }
        catch (Exception e)
        {
            log.WarnFormat("Caught exception while validating AD credentials: {0}", e);

            // treat any errors as validation failures
            return new Result
            {
                Type = ResultType.ValidationError
            };

        }

    }

    [ScriptMethod]
    [WebMethod(EnableSession = true)]
    public int StartCountingComputersInAD(ActiveDirectoryEntry entry)
    {
        Task<List<OrganizationalUnitCountOfComputers>> task = Task.Factory.StartNew(discoveryConfigurationInfoObject =>
        {
            var discoveryConfigurationInfo = discoveryConfigurationInfoObject as DiscoveryConfiguration;
            if (discoveryConfigurationInfo == null)
            {
                return new List<OrganizationalUnitCountOfComputers>();
            }
            
            ActiveDirectoryAccess access = GetTestAccessToken(entry, discoveryConfigurationInfo);
            
            try
            {
                using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, discoveryConfigurationInfo.EngineID))
                {
                    return businessProxy.GetCountOfComputers(access);
                }
            }
            catch (Exception e)
            {
                log.WarnFormat("Caught exception while validating getting numbers of computers/servers: {0}", e);

                // treat any errors as validation failures
                return new List<OrganizationalUnitCountOfComputers>();
            }
        }, ConfigWorkflowHelper.DiscoveryConfigurationInfo);

        Session[String.Format(CountOfComputersSessionTaskKey, task.Id)] = task;
        return task.Id;
    }

    [ScriptMethod]
    [WebMethod(EnableSession = true)]
    public TaskStatusResult GetCountOfComputersTaskResult(int taskId)
    {
        var task = (Task<List<OrganizationalUnitCountOfComputers>>)Session[String.Format(CountOfComputersSessionTaskKey, taskId)];

        if (task != null)
        {
            if (task.Status == TaskStatus.Running)
            {
                return TaskStatusResult.StillRunning;
            }

            if (task.Status == TaskStatus.RanToCompletion)
            {
                return TaskStatusResult.SuccessfullyCompleted;
            }
        }

        return TaskStatusResult.Failed;
    }

    [ScriptMethod]
    [WebMethod(EnableSession = true)]
    public List<OrganizationalUnitCountOfComputers> GetCountOfComputersInAD(int taskId)
    {
        var task = (Task<List<OrganizationalUnitCountOfComputers>>)Session[String.Format(CountOfComputersSessionTaskKey, taskId)];
        return task != null ? task.Result : new List<OrganizationalUnitCountOfComputers>();
    }

    [ScriptMethod]
    [WebMethod(EnableSession = true)]
    public Result ADAddEntry(ActiveDirectoryEntry entry)
    {
        DiscoveryConfiguration discovery = ConfigWorkflowHelper.DiscoveryConfigurationInfo;
        var config = discovery.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>();

        var result = ValidateNewCredentials(entry, config.Credentials);
        if (result.Type != ResultType.Success)
            return result;


        ActiveDirectoryAccess access = GetAccessToken(entry);
        access.OrganizationalUnits = entry.OrganizationalUnits;
        access.OrganizationalUnitsToDisplayInGrid = entry.OrganizationalUnitsToDisplayInGrid;
        access.NumberOfSelectedOUs = entry.NumberOfSelectedOUs;
        access.ServersOnly = entry.ServersOnly;
        access.DeviceCount = entry.DeviceCount;

        result = _ValidateActiveDirectoryAccess(access);
        if (result.Type != ResultType.Success)
        {
            return result;
        }

        config.ActiveDirectoryList.Add(access);

        if (access.Credential != null &&
            !config.Credentials.OfType<UsernamePasswordCredential>().Any(c => c.Name.Equals(access.Credential.Name)))
        {
            //new credentials
            config.Credentials.Add(access.Credential);

            //also add for WMI probing
            config.WMICredentials.Add(new WMIAccess {Credential = access.Credential});

        }
        else if (access.CredentialID > 0 && access.Credential == null)
        {
            //existing credentials, we can use from DB
            if (config.WMICredentials.FirstOrDefault(c => c.CredentialID == access.CredentialID) == null)
            {
                var cred = new CredentialManager().GetCredential<UsernamePasswordCredential>(access.CredentialID);
                if (cred != null)
                {
                    config.WMICredentials.Add(new WMIAccess {Credential = cred, CredentialID = access.CredentialID});
                }
            }

        }
        return new Result
        {
            Type = ResultType.Success
        };
    }


    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    [WebMethod(EnableSession = true)]
    public List<ActiveDirectoryEntry> ADGetEntries()
    {
        CredentialManagerService service = new CredentialManagerService();
        var credsLookup = service.GetWindowsCredentials().ToLookup(p => p.ID);
        DiscoveryConfiguration discovery = ConfigWorkflowHelper.DiscoveryConfigurationInfo;
        var config = discovery.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>();

        // profile creds
        var creds = config.Credentials.OfType<UsernamePasswordCredential>().ToList();

        return config.ActiveDirectoryList.Select((e, index) =>
        {
            var entry = new ActiveDirectoryEntry
        {
            CredentialID = e.CredentialID,
            HostName = e.HostName,
            OrganizationalUnits = e.OrganizationalUnits,
            OrganizationalUnitsToDisplayInGrid = e.OrganizationalUnitsToDisplayInGrid,
            NumberOfSelectedOUs = e.NumberOfSelectedOUs,
            Index = index,
            ProfileIndex = e.ProfileIndex,
            CredentialIndex = -1,
            ServersOnly = e.ServersOnly,
            DeviceCount = e.DeviceCount
        };
            if (e.Credential != null)
            {
                entry.Name = e.Credential.Name;
                entry.UserName = e.Credential.Username;
                entry.CredentialIndex = creds.IndexOf(e.Credential);
            }
            else
            {
                var cred = credsLookup[e.CredentialID].FirstOrDefault();

                if (cred != null)
                {
                    entry.Name = cred.Name;
                    entry.UserName = cred.Username;
                }
            }
            return entry;
        }).ToList();

    }

    [WebMethod(EnableSession = true)]
    public Result ADSaveEntry(ActiveDirectoryEntry entry)
    {
        DiscoveryConfiguration discovery = ConfigWorkflowHelper.DiscoveryConfigurationInfo;
        var config = discovery.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>();
        var list = config.ActiveDirectoryList;

        if (entry.Index >= 0 && entry.Index < list.Count)
        {
            var validationResult = ValidateNewCredentials(entry, config.Credentials);
            if (validationResult.Type != ResultType.Success)
                return validationResult;

            validationResult = _ValidateActiveDirectoryAccess(GetTestAccessToken(entry, discovery));
            if (validationResult.Type != ResultType.Success)
            {
                return validationResult;
            }

            var access = GetAccessToken(entry);

            if (access.Credential != null &&
                !config.Credentials.OfType<UsernamePasswordCredential>().Any(c => c.Name.Equals(access.Credential.Name)))
            {
                // Add new credential
                config.Credentials.Add(access.Credential);
                
                // also add it to WMI probing creds
                config.WMICredentials.Add(new WMIAccess { Credential = access.Credential});

            }
            else if (access.CredentialID > 0 && access.Credential == null)
            {
                //existing credentials, we can use from DB
                if (config.WMICredentials.FirstOrDefault(c => c.CredentialID == access.CredentialID) == null)
                {
                    var cred = new CredentialManager().GetCredential<UsernamePasswordCredential>(access.CredentialID);
                    config.WMICredentials.Add(new WMIAccess { Credential = cred, CredentialID = access.CredentialID });
                }
            }
            
            list[entry.Index] = access;

        }
        return new Result
        {
            Type = ResultType.Success
        };
    }


    [WebMethod(EnableSession = true)]
    public void ADDeleteEntry(ActiveDirectoryEntry entry)
    {

        DiscoveryConfiguration discovery = ConfigWorkflowHelper.DiscoveryConfigurationInfo;
        var config = discovery.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>();
        var list = config.ActiveDirectoryList;

        if (entry.Index >= 0 && entry.Index < list.Count)
        {
            list.RemoveAt(entry.Index);
        }

    }

    [WebMethod(EnableSession = true)]
    public void ToggleDiscoveryHint(string settingName)
    {
        switch (settingName)
        {
            case ShowDiscoveryHintSettingName:
            case ShowIncludeFutureOuHintSettingName: break;
            default:
                return;
        }
        
        var value = WebUserSettingsDAL.Get(settingName);

        if (!string.IsNullOrEmpty(value))
        {
            bool show = Convert.ToBoolean(value);
            WebUserSettingsDAL.Set(settingName, Convert.ToString((!show)));
        }
        else
        {
            WebUserSettingsDAL.Set(settingName, Convert.ToString(false));
        }
    }


    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        // this needs to be exposed to client             
        log.Error(ex);
    }
}
