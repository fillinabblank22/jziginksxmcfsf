﻿<%@ WebService Language="C#" Class="AgentManagerService" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Agent;
using SolarWinds.Orion.Web.Agent;
using SolarWinds.AgentManagement.Contract;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class AgentManagerService : WebService
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
    
    [WebMethod(true)]
    public int StartDeployingAgent(string deploymentSettingsId) 
    {
        var settings = AgentsDeployment.Instance.GetDeploymentSettings(new Guid(deploymentSettingsId));
        if (settings == null)
        {
            log.ErrorFormat("Deployment settings with id {0} not found", deploymentSettingsId);
            return -1;
        }

        using (var proxy = _blProxyCreator.Create(ex => log.Error(ex)))
        {
            return proxy.DeployAgent(settings);
        }        
    }

    [WebMethod(true)]
    public void StartDeployingPlugins(int agentId)
    {
        using (var proxy = _blProxyCreator.Create(ex => log.Error(ex)))
        {
            var requiredPlugins = proxy.GetRequiredAgentDiscoveryPlugins();
            proxy.DeployAgentPlugins(agentId, requiredPlugins);
        }        
    }

    [WebMethod(true)]
    public void ApproveReboot(int agentId)
    {
        new AgentManager().ApproveReboot(agentId);
    }    
    
    [WebMethod(true)]
    public AgentDeploymentInfo GetDeployingStatus(int agentId)
    {
        using (var proxy = _blProxyCreator.Create(ex => log.Error(ex)))
        {
            return proxy.GetAgentDeploymentInfo(agentId);
        }
    }

    [WebMethod]
    public void UninstallAgent(int nodeId)
    {
        new AgentManager().UninstallAgentByNodeId(nodeId);
    }
}