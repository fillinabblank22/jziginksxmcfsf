<%@ WebService Language="C#" Class="HubbleService" %>

using System;
using System.Collections;
using System.Data.SqlClient;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web.UI;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class HubbleService : WebService
{
    [WebMethod]
    [ScriptMethod]
    public Hashtable GetResourceBasicData(int resId)
    {
        using (SqlCommand cmd = SqlHelper.GetTextCommand("SELECT ResourceFile FROM Resources WHERE ResourceID=@id"))
        {
            cmd.Parameters.AddWithValue("id", resId);
            var path = SqlHelper.ExecuteScalar(cmd).ToString();
            var realpath = Server.MapPath(path);
            var loadingMode = "unknown";
            try {
              // get rendering mode from the control
              Page pageholder = new Page();
              var ctrl = (BaseResourceControl)pageholder.LoadControl(path);
              loadingMode = ctrl.ResourceLoadingMode.ToString();
            }
            catch {}

          object viewstatecount = 0;
          return new Hashtable()
          {
            {"path", path},
            {"realPath", realpath},
            {"viewBagCount", viewstatecount},
            {"loadingMode", loadingMode}
          };
        }
    }

    [WebMethod]
    [ScriptMethod]
    public Hashtable GetResourceProperties(int resId)
    {
        using (SqlCommand cmd = SqlHelper.GetTextCommand("SELECT PropertyName, PropertyValue FROM ResourceProperties WHERE ResourceID=@id ORDER BY PropertyName ASC"))
        {
            cmd.Parameters.AddWithValue("id", resId);
            var rv = new Hashtable();

            using (var reader = SqlHelper.ExecuteReader(cmd))
            {
                while (reader.Read())
                {
                    rv.Add(reader.GetString(0), reader.GetString(1));
                }
            }
            return rv;
        }
    }
}