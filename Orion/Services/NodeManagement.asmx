<%@ WebService Language="C#" Class="NodeManagement" %>

using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using System.Linq;
using System.Text;
using SolarWinds.InformationService.Contract2;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.Web.NpmAdapters;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.InformationService.Contract;
using Node = SolarWinds.Orion.Core.Common.Models.Node;
using Interface = SolarWinds.Orion.Core.Common.Models.Interface;
using SolarWinds.Orion.Core.Common.Indications;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.PackageManager;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Common.Blacklists;
using SolarWinds.Orion.Web.Agent;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.Plugins;
using EngineServerType = SolarWinds.Orion.Models.EngineServerType;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class NodeManagement : WebService
{
    Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    private static readonly DateTime unmanageInfiniteDate = new DateTime(9999, 1, 1, 0, 0, 0, DateTimeKind.Utc);

    private const string NodePrefix = "N";
    private const string InterfacePrefix = "I";
    private const string VolumePrefix = "V";
    private const string SupportedEntities = NodePrefix + InterfacePrefix + VolumePrefix;

    [WebMethod]
    public void SaveUserSetting(string name, string value)
    {
        if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            try
            {
                var blacklist = new WebUserSettingsBlacklist();

                if (!AuthorizationChecker.IsAdmin && blacklist.IsSettingBlacklisted(name))
                {
                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.StatusCode = 403;
                    return;
                }

                WebUserSettingsDAL.Set(name, value);
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }
    }

    [WebMethod]
    public bool CheckIfEnginesAreEqual(int[] ids, int id)
    {
        AuthorizationChecker.AllowNodeManagement();
        return new ModuleGlobalDAL().AreEqualEngines(ids, id, true);
    }

    [WebMethod]
    public void SaveBoolSetting(string name, string value)
    {
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            return;
        }

        AuthorizationChecker.AllowNodeManagement();
        bool val;
        SolarWinds.Orion.Web.DAL.SettingsDAL.SetValue(name, Boolean.TryParse(value, out val) && val);
    }

    private void ProcessItems<T>(IEnumerable<T> items, Action<T, INpmNodeManagementAdapter> itemProcessor, bool needToCheckPermissions = true)
    {
        if (needToCheckPermissions)
            AuthorizationChecker.AllowNodeManagement();
        using (INpmNodeManagementAdapter adapter = NpmAdapterFactory.CreateNodeManagementAdapter(delegate (Exception ex) { throw ex; }))
        {
            foreach (T item in items)
            {
                itemProcessor(item, adapter);
            }
        }
    }

    private void ProcessItems<T>(IEnumerable<T> items, Action<T, ICoreBusinessLayer> itemProcessor, bool needToCheckPermission = true)
    {
        if (needToCheckPermission)
            AuthorizationChecker.AllowNodeManagement();

        using (var proxy = _blProxyCreator.Create(delegate (Exception ex) { throw ex; }))
        {
            foreach (T item in items)
            {
                itemProcessor(item, proxy);
            }
        }
    }

    private void ProcessItemsOnEngine(IEnumerable<string> netObjectIds, Action<string, ICoreBusinessLayer> itemProcessor)
    {
        ProcessItems(netObjectIds, delegate (string netObjectId, ICoreBusinessLayer proxy)
        {
            using (var engineProxy = _blProxyCreator.Create(ex => { throw ex; }, GetEngineIdForNetObject(netObjectId), false))
            {
                itemProcessor(GetNetObjectIdString(netObjectId), engineProxy);
            }
        });
    }

    private void ProcessItemsOnEngine(IEnumerable<string> netObjectIds, Action<string, INpmNodeManagementAdapter> itemProcessor)
    {
        ProcessItems(netObjectIds, delegate (string netObjectId, INpmNodeManagementAdapter adapter)
        {
            using (INpmNodeManagementAdapter engineAdapter = NpmAdapterFactory.CreateNodeManagementAdapter(ex => { throw ex; }, GetEngineIdForNetObject(netObjectId), false))
            {
                itemProcessor(netObjectId, engineAdapter);
            }
        });
    }

    private void UpdateNodes(IEnumerable<int> nodeIds, Action<Node, ICoreBusinessLayer> nodeProcessor, bool needToCheckPermissions = true)
    {
        ProcessItems(nodeIds, delegate (int nodeId, ICoreBusinessLayer proxy)
        {
            EnsureNodeAccess(nodeId);

            Node node = proxy.GetNodeWithOptions(nodeId, false, false);
            nodeProcessor(node, proxy);
            proxy.UpdateNode(node);

            using (INpmNodeManagementAdapter adapter = NpmAdapterFactory.CreateNodeManagementAdapter(delegate (Exception ex) { throw ex; }))
                adapter.UpdateNpmNode(node);
        }, needToCheckPermissions);
    }

    private void UpdateInterfaces(IEnumerable<int> interfaceIds, Action<Interface, INpmNodeManagementAdapter> interfaceProcessor, bool needToCheckPermissions = true)
    {
        ProcessItems(interfaceIds, delegate (int interfaceId, INpmNodeManagementAdapter adapter)
        {
            EnsureAccess(InterfacePrefix, interfaceId);

            Interface _interface = adapter.GetInterface(interfaceId);
            interfaceProcessor(_interface, adapter);
            adapter.UpdateInterface(_interface);
        }, needToCheckPermissions);
    }

    private void UpdateVolumes(IEnumerable<int> volumeIds, Action<Volume, ICoreBusinessLayer> volumeProcessor, bool needToCheckPermissions = true)
    {
        ProcessItems(volumeIds, delegate (int volumeId, ICoreBusinessLayer adapter)
        {
            EnsureAccess(VolumePrefix, volumeId);

            Volume _volume = adapter.GetVolume(volumeId);
            volumeProcessor(_volume, adapter);
            adapter.UpdateVolume(_volume);
        }, needToCheckPermissions);
    }

    private void DeleteNetObjectsFromDb(string[] netObjectIds)
    {
        AuthorizationChecker.AllowNodeManagement();
        foreach (string netObjectId in netObjectIds)
        {
            DeleteNetObjectsFromDb(netObjectId);
        }
    }

    private void DeleteNetObjectsFromDb(string netObjectId)
    {
        AuthorizationChecker.AllowNodeManagement();

        string prefix;
        int id, engineId;

        ParseNetObject(netObjectId, out prefix, out id, out engineId);
        EnsureAccess(prefix, id);

        switch (prefix)
        {
            case NodePrefix:
                Node node = null;
                try
                {
                    using (var proxy = _blProxyCreator.Create(delegate (Exception ex) { throw ex; }))
                    {
                        node = proxy.GetNode(id);
                        proxy.DeleteNode(id);
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                    throw;
                }

                using (INpmNodeManagementAdapter adapter = NpmAdapterFactory.CreateNodeManagementAdapter(delegate (Exception ex) { throw ex; }))
                    adapter.DeleteNpmNode(node);
                break;
            case InterfacePrefix:
                using (INpmNodeManagementAdapter adapter = NpmAdapterFactory.CreateNodeManagementAdapter(delegate (Exception ex) { throw ex; }))
                    adapter.DeleteInterface(id);
                break;
            case VolumePrefix:
                using (var proxy = _blProxyCreator.Create(delegate (Exception ex) { throw ex; }))
                    proxy.DeleteVolume(proxy.GetVolume(id));
                break;
        }
    }

    private void PerformDeleting(int engineId, List<string> objects)
    {
        AuthorizationChecker.AllowNodeManagement();

        try
        {
            List<int> nodeIds = new List<int>();
            objects.ForEach((s) => { if (NetObjectHelper.IsNodeNetObject(s)) nodeIds.Add(Int32.Parse(NetObjectHelper.GetObjectID(s))); });

            using (var proxy = _blProxyCreator.Create((ex) => { throw ex; }, engineId, true))
            {
                proxy.RemoveNetObjects(objects);
            }

            using (INpmNodeManagementAdapter adapter = NpmAdapterFactory.CreateNodeManagementAdapter((ex) => { throw ex; }, engineId, true))
                adapter.DeleteNpmNodes(nodeIds.ToArray());

        }
        catch (System.ServiceModel.FaultException)
        {
            DeleteNetObjectsFromDb(objects.ToArray());
        }
        catch (System.ServiceModel.EndpointNotFoundException ex)
        {
            throw new System.ServiceModel.EndpointNotFoundException(ex.Message);
        }
    }

    [WebMethod]
    public bool IsNodeUnmanaged(int nodeId)
    {
        AuthorizationChecker.AllowUnmanage();
        EnsureAccess(NodePrefix, nodeId);

        using (var proxy = _blProxyCreator.Create(delegate (Exception ex) { throw ex; }))
        {
            Node node = proxy.GetNodeWithOptions(nodeId, false, false);
            return node.UnManaged;
        }
    }

    [WebMethod(EnableSession = true)]
    public void RemanageNodes(int[] nodeIds)
    {
        AuthorizationChecker.AllowUnmanage();
        var utcNow = DateTime.UtcNow;

        UpdateNodes(nodeIds, delegate (Node node, ICoreBusinessLayer proxy)
        {
            if (!String.IsNullOrEmpty(node.Status) && !node.Status.Trim().Equals(((int)OBJECT_STATUS.External).ToString(), StringComparison.InvariantCultureIgnoreCase))
            {
                node.UnManageUntil = utcNow;
                node.LastBoot = DateTime.MinValue; //FB #10608
            }
        }, false);
    }

    [WebMethod(EnableSession = true)]
    public void RemanageInterfaces(int[] interfaceIds)
    {
        AuthorizationChecker.AllowUnmanage();
        var utcNow = DateTime.UtcNow;

        UpdateInterfaces(interfaceIds, delegate (Interface _interface, INpmNodeManagementAdapter adapter)
        {
            _interface.UnManageUntil = utcNow;
        }, false);
    }

    [WebMethod(EnableSession = true)]
    public void RemanageVolumes(int[] volumeIds)
    {
        AuthorizationChecker.AllowUnmanage();
        var utcNow = DateTime.UtcNow;

        UpdateVolumes(volumeIds, delegate (Volume volume, ICoreBusinessLayer adapter)
        {
            volume.UnManageUntil = utcNow;
        }, false);
    }

    [WebMethod(EnableSession = true)]
    public void UnmanageNodes(int[] nodeIds, DateTime? unmanageFrom, DateTime? unmanageUntil)
    {
        AuthorizationChecker.AllowUnmanage();
        var utcNow = DateTime.UtcNow;

        UpdateNodes(nodeIds, delegate (Node node, ICoreBusinessLayer proxy)
        {
            if (log.IsDebugEnabled)
            {
                log.DebugFormat("UnmanageNodes NodeId='{0}', unmanageFrom='{1}', unmanageUntil='{2}'", node.ID, unmanageFrom, unmanageUntil);
            }

            if (!String.IsNullOrEmpty(node.Status) && !node.Status.Trim().Equals(((int)OBJECT_STATUS.External).ToString(), StringComparison.InvariantCultureIgnoreCase))
            {
                node.UnManageFrom = unmanageFrom ?? utcNow;
                node.UnManageUntil = unmanageUntil ?? unmanageInfiniteDate;
            }
            else
            {
                if (log.IsDebugEnabled)
                {
                    log.DebugFormat("UnmanageNodes failed for NodeId='{0}' node.Status is '{1}'", node.ID, string.IsNullOrEmpty(node.Status) ? string.Empty : node.Status);
                }
            }
        }, false);
    }

    [WebMethod(EnableSession = true)]
    public void UnmanageInterfaces(int[] interfaceIds, DateTime? unmanageFrom, DateTime? unmanageUntil)
    {
        AuthorizationChecker.AllowUnmanage();
        var utcNow = DateTime.UtcNow;

        UpdateInterfaces(interfaceIds, delegate (Interface _interface, INpmNodeManagementAdapter adapter)
        {
            if (log.IsDebugEnabled)
            {
                log.DebugFormat("UnmanageInterfaces InteraceId='{0}', unmanageFrom='{1}', unmanageUntil='{2}'", _interface.ID, unmanageFrom, unmanageUntil);
            }

            _interface.UnManageFrom = unmanageFrom ?? utcNow;
            _interface.UnManageUntil = unmanageUntil ?? unmanageInfiniteDate;
        }, false);
    }

    [WebMethod(EnableSession = true)]
    public void UnmanageVolumes(int[] volumeIds, DateTime? unmanageFrom, DateTime? unmanageUntil)
    {
        AuthorizationChecker.AllowUnmanage(); //moving out of try catch due to permission check throws exception

        try
        {
            var utcNow = DateTime.UtcNow;
            UpdateVolumes(volumeIds, delegate (Volume volume, ICoreBusinessLayer adapter)
            {
                if (log.IsDebugEnabled)
                {
                    log.DebugFormat("UnmanageVolumes VolumeId='{0}', unmanageFrom='{1}', unmanageUntil='{2}'", volume.ID, unmanageFrom, unmanageUntil);
                }

                volume.UnManageFrom = unmanageFrom ?? utcNow;
                volume.UnManageUntil = unmanageUntil ?? unmanageInfiniteDate;
            }, false);
        }
        catch (Exception e)
        {
            log.ErrorFormat("An error has occurred when unmanaging volumes: {0}", e.ToString());
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public void DeleteNodes(int[] nodeIds)
    {
        AuthorizationChecker.AllowNodeManagement();

        ProcessItems(nodeIds, delegate (int nodeId, ICoreBusinessLayer proxy)
        {
            EnsureNodeAccess(nodeId);

            Node node = proxy.GetNode(nodeId);
            proxy.DeleteNode(nodeId);

            using (INpmNodeManagementAdapter adapter = NpmAdapterFactory.CreateNodeManagementAdapter(delegate (Exception ex) { throw ex; }))
                adapter.DeleteNpmNode(node);
        });
    }

    private static readonly object _deleteObjNowSyncRoot = new object();

    [WebMethod(EnableSession = true)]
    public void DeleteObjNow(string netObjectIds)
    {
        AuthorizationChecker.AllowNodeManagement();

        string prefix;
        int id, engineId;

        ParseNetObject(netObjectIds, out prefix, out id, out engineId);
        EnsureAccess(prefix, id);

        string objId = GetNetObjectId(prefix, id);

        lock (_deleteObjNowSyncRoot)
        {
            try
            {
                if (HandleInterfaceRemoval(netObjectIds))
                    return;

                using (var engineProxy = _blProxyCreator.Create(ex => { throw ex; }, engineId, true))
                {
                    engineProxy.RemoveNetObject(objId);
                }

                if (NetObjectHelper.IsNodeNetObject(objId))
                    using (
                        INpmNodeManagementAdapter adapter =
                            NpmAdapterFactory.CreateNodeManagementAdapter((ex) => { throw ex; }, engineId, true))
                        adapter.DeleteNpmNodes(new int[] { id });

            }
            catch (System.ServiceModel.FaultException)
            {
                //can't connect to standard poller?
                DeleteNetObjectsFromDb(objId);
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public void DeleteNetObjects(string[] netObjectIds)
    {
        AuthorizationChecker.AllowNodeManagement();

        var objectCollection = new Dictionary<int, IList<string>>();
        var nodeCollection = new Dictionary<int, IList<string>>();
        var engines = new List<int>();

        foreach (var netObjectId in netObjectIds)
        {
            if (HandleInterfaceRemoval(netObjectId))
                continue;

            string prefix;
            int id, engineID;

            ParseNetObject(netObjectId, out prefix, out id, out engineID);
            EnsureAccess(prefix, id);

            string objId = GetNetObjectId(prefix, id);

            if (!engines.Contains(engineID))
            {
                engines.Add(engineID);
            }

            if (prefix.Equals(NodePrefix, StringComparison.OrdinalIgnoreCase))
            {
                if (!nodeCollection.ContainsKey(engineID))
                {
                    nodeCollection[engineID] = new List<string>();
                }

                nodeCollection[engineID].Add(objId);

            }
            else
            {
                if (!objectCollection.ContainsKey(engineID))
                {
                    objectCollection[engineID] = new List<string>();
                }

                objectCollection[engineID].Add(objId);
            }
        }

        foreach (var engine in engines)
        {
            var listOfObjects = new List<string>();

            if (nodeCollection.ContainsKey(engine))
            {
                listOfObjects.InsertRange(0, nodeCollection[engine]);
            }

            if (objectCollection.ContainsKey(engine))
            {
                listOfObjects.AddRange(objectCollection[engine]);
            }

            PerformDeleting(engine, listOfObjects);
        }
    }

    [WebMethod(EnableSession = true)]
    public void DeleteObjects(string[] netObjectIds)
    {
        try
        {
            ProcessItemsOnEngine(netObjectIds, delegate (string netObjectId, ICoreBusinessLayer proxy)
            {
                if (HandleInterfaceRemoval(netObjectId)) return;

                string prefix;
                int id;

                ParseNetObject(netObjectId, out prefix, out id);
                EnsureAccess(prefix, id);

                proxy.RemoveNetObject(GetNetObjectId(prefix, id));
            });
        }
        catch (System.ServiceModel.FaultException)
        {
            //can't connect to standard poller?
            DeleteNetObjectsFromDb(netObjectIds);
        }
    }

    [WebMethod(EnableSession = true)]
    public void PollNow(string[] netObjectIds)
    {
        AuthorizationChecker.AllowNodeManagement();

        foreach (var id in netObjectIds)
        {
            EnsureAccess(id);
        }

        try
        {
            // try to split NPM and Core functionality
            ProcessItemsOnEngine(netObjectIds, delegate (string netObjectId, INpmNodeManagementAdapter adapter)
            {
                adapter.NpmPollerForcePollNode(netObjectId);
            });

            ProcessItemsOnEngine(netObjectIds, delegate (string netObjectId, ICoreBusinessLayer proxy)
            {
                proxy.PollNow(netObjectId);
            });
            var publisher = IndicationPublisher.CreateV3();
            // Publish event that PollNow is needed
            foreach (string netobject in netObjectIds)
            {
                string modified = FixInterfaceEntity(netobject);
                publisher.ReportIndication(new NetObjectOperationIndication(IndicationType.Orion_PollNow, modified, SolarWinds.Orion.Web.NetObjectFactory.GetSWISTypeByNetObject(modified)));
            }
        }
        catch (Exception ex)
        {
            log.Error("Error in PollNow function.", ex);
            throw new Exception(String.Format("Poll now failed for netobject {0}.", netObjectIds));
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string PollNetObjNow(string netObjectIds)
    {
        AuthorizationChecker.AllowNodeManagement();
        EnsureAccess(netObjectIds);

        ModuleGlobalDAL moduleglobal = new ModuleGlobalDAL();
        var res = string.Empty;
        try
        {
            var objectId = netObjectIds.Split(':');
            var netObjEngineId = GetEngineIdForNetObject(netObjectIds);

            if (objectId.Length > 0)
            {
                if (objectId[0].Equals("N", StringComparison.OrdinalIgnoreCase))
                {
                    // try to split NPM and Core functionality
                    if (moduleglobal.IsModuleActiveOnEngine(netObjEngineId, "NPM"))
                    {
                        using (INpmNodeManagementAdapter adapter = NpmAdapterFactory.CreateNodeManagementAdapter(ex => { throw ex; }, netObjEngineId, false))
                            adapter.NpmPollerForcePollNode(GetNetObjectIdString(netObjectIds));
                    }

                    using (var proxy = _blProxyCreator.Create(ex => { throw ex; }, netObjEngineId, false))
                        res = proxy.PollNodeNow(GetNetObjectIdString(netObjectIds));
                }
                else if (objectId[0].Equals("I", StringComparison.OrdinalIgnoreCase) || objectId[0].Equals("IW", StringComparison.OrdinalIgnoreCase))
                {
                    if (moduleglobal.IsModuleActiveOnEngine(netObjEngineId, "NPM"))
                    {
                        using (INpmNodeManagementAdapter adapter = NpmAdapterFactory.CreateNodeManagementAdapter(ex => { throw ex; }, netObjEngineId, false))
                            res = adapter.PollInterfaceNow(GetNetObjectIdString(netObjectIds));
                    }
                    using (var proxy = _blProxyCreator.Create(ex => { throw ex; }, netObjEngineId, false))
                        proxy.PollNow(GetNetObjectIdString(netObjectIds));

                    netObjectIds = FixInterfaceEntity(netObjectIds);
                }
                else
                {
                    using (var proxy = _blProxyCreator.Create(ex => { throw ex; }, netObjEngineId, false))
                        proxy.PollNow(GetNetObjectIdString(netObjectIds));
                }
                // Publish event that PollNow is needed
                IndicationPublisher.CreateV3().ReportIndication(new NetObjectOperationIndication(IndicationType.Orion_PollNow, GetNetObjectIdString(netObjectIds), SolarWinds.Orion.Web.NetObjectFactory.GetSWISTypeByNetObject(GetNetObjectIdString(netObjectIds))));
            }
        }
        catch (Exception ex)
        {
            log.Error("Error in PollNow function.", ex);
            throw new Exception(String.Format("Poll now failed for netobject {0}.", GetNetObjectIdString(netObjectIds)));
        }
        return res;
    }

    [WebMethod(EnableSession = true)]
    public void RediscoverNow(string netObjectIds)
    {
        AuthorizationChecker.AllowNodeManagement();
        EnsureAccess(netObjectIds);

        try
        {
            ProcessItemsOnEngine(new[] { netObjectIds }, delegate (string netObjectId, ICoreBusinessLayer proxy)
            {
                proxy.Rediscover(netObjectId);
            });
            // Publish event that Rediscovery is needed
            string modified = FixInterfaceEntity(netObjectIds);
            IndicationPublisher.CreateV3().ReportIndication(new NetObjectOperationIndication(IndicationType.Orion_Rediscovery, GetNetObjectIdString(modified), SolarWinds.Orion.Web.NetObjectFactory.GetSWISTypeByNetObject(GetNetObjectIdString(modified))));
        }
        catch (Exception ex)
        {
            log.Error("Error in Rediscover function.", ex);
            throw new Exception(String.Format("Rediscover failed for netobject {0}.", netObjectIds));
        }
    }

    [WebMethod(EnableSession = true)]
    public void Rediscover(string[] netObjectIds)
    {
        AuthorizationChecker.AllowNodeManagement();

        foreach (var id in netObjectIds)
        {
            EnsureAccess(id);
        }

        try
        {
            ProcessItemsOnEngine(netObjectIds, delegate (string netObjectId, ICoreBusinessLayer proxy)
            {
                proxy.Rediscover(netObjectId);
            });
            var publisher = IndicationPublisher.CreateV3();
            // Publish event that Rediscovery is needed
            foreach (string netobject in netObjectIds)
            {
                string modified = FixInterfaceEntity(netobject);
                publisher.ReportIndication(new NetObjectOperationIndication(IndicationType.Orion_Rediscovery, GetNetObjectIdString(modified), SolarWinds.Orion.Web.NetObjectFactory.GetSWISTypeByNetObject(GetNetObjectIdString(modified))));
            }
        }
        catch (Exception ex)
        {
            log.Error("Error in Rediscover function.", ex);
            throw new Exception(String.Format("Rediscover failed for netobject {0}.", netObjectIds));
        }
    }

    [WebMethod(EnableSession = true)]
    public void AdminstrativelyShutDown(string[] netObjectIds)
    {
        ProcessItemsOnEngine(netObjectIds, delegate (string netObjectId, INpmNodeManagementAdapter adapter)
        {
            EnsureAccess(netObjectId);
            adapter.AdministrativelyShutDown(netObjectId);
        });
    }

    [WebMethod(EnableSession = true)]
    public void AdministrativelyEnable(string[] netObjectIds)
    {
        ProcessItemsOnEngine(netObjectIds, delegate (string netObjectId, INpmNodeManagementAdapter adapter)
        {
            EnsureAccess(netObjectId);
            adapter.AdministrativelyEnable(netObjectId);
        });
    }

    [WebMethod(EnableSession = true)]
    public void EnergyWiseSetInterfacePowerLevel(string[] netObjectIds, int level)
    {
        AuthorizationChecker.AllowNodeManagement();
        using (INpmNodeManagementAdapter adapter = NpmAdapterFactory.CreateNodeManagementAdapter(delegate (Exception ex) { throw ex; }))
        {
            foreach (var id in netObjectIds)
            {
                EnsureAccess(id);
                adapter.EnergyWiseSetInterfacePowerLevel(id, level);
            }
        }
    }

    private string GetNetObjectIdString(string engineNetObjectId)
    {
        var parts = engineNetObjectId.Split(':');
        return (parts.Length == 3) ? string.Format("{0}:{1}", parts[0], parts[1]) : engineNetObjectId;
    }

    private int GetEngineIdForNetObject(string netObjectId)
    {
        int engineId;
        var parts = netObjectId.Split(':');

        if (parts.Length == 3 && int.TryParse(parts[2], out engineId))
            return engineId;

        int id;
        List<string> allowed = new List<string>() { "N", "I", "IW", "V" };
        if (parts.Length != 2 || !allowed.Contains(parts[0]) || !int.TryParse(parts[1], out id))
            throw new ArgumentException(string.Format("'{0}' is not a valid net object id", netObjectId));

        int nodeId;
        switch (parts[0].ToUpperInvariant())
        {
            case "N":
                nodeId = id;
                break;
            case "I":
            case "IW":
                using (INpmNodeManagementAdapter adapter = NpmAdapterFactory.CreateNodeManagementAdapter(delegate (Exception ex) { throw ex; }))
                    nodeId = adapter.GetInterface(id).NodeID;
                break;
            case "V":
                using (var volumeProxy = _blProxyCreator.Create(delegate (Exception ex) { throw ex; }))
                    nodeId = volumeProxy.GetVolume(id).NodeID;
                break;
            default:
                throw new ArgumentException(string.Format("'{0}' is not a valid net object id", netObjectId));
        }

        using (var proxy = _blProxyCreator.Create(delegate (Exception ex) { throw ex; }))
            return proxy.GetNode(nodeId).EngineID;
    }

    [WebMethod(EnableSession = true)]
    public void StoreSelectedIDs(string[] ids, string netObjectType)
    {
        AuthorizationChecker.AllowNodeManagement();

        try
        {
            NodeWorkflowHelper.FillNetObjectIds(netObjectType, ids);
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public void StoreAllIDs(string queryString, string netObjectType)
    {
        AuthorizationChecker.AllowNodeManagement();

        try
        {
            InformationServiceProxy service = InformationServiceProxy.CreateV3();
            QueryOptions options = new QueryOptions(QueryOutputFormat.DataTable);
            DataTable idsDataTable = service.Query(queryString);

            int[] ids = new int[idsDataTable.Rows.Count];
            for (int i = 0; i < idsDataTable.Rows.Count; i++)
            {
                ids[i] = (int)idsDataTable.Rows[i][0];
            }

            NodeWorkflowHelper.FillNetObjectIds(netObjectType, ids);
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public object GetNodesToEngineAssignmentInfo(int targetEngineId, int[] nodesToReassignIds)
    {
        AuthorizationChecker.AllowNodeManagement();

        const string baseMsg = "Cannot get nodes to engine assignment info. {0}";

        if (nodesToReassignIds == null || nodesToReassignIds.Length == 0)
        {
            var errMsg = string.Format(baseMsg, "Input nodes to reassign do not contain any node");
            log.Error(errMsg);
            throw new InvalidOperationException(errMsg);
        }

        try
        {
            using (var proxy = InformationServiceProxy.CreateV3())
            {
                var qParams = new Dictionary<string, object>();
                qParams["engineId"] = targetEngineId;
                var dt = proxy.Query("SELECT ServerType FROM Orion.Engines WHERE EngineID=@engineId", qParams);

                if (dt.Rows.Count == 0 || dt.Rows[0].ItemArray.Length == 0 || dt.Rows[0].ItemArray[0] is DBNull)
                {
                    var errMsg = string.Format(baseMsg, $"Engine with ID:{targetEngineId} not found or cannot determine server type");
                    log.Error(errMsg);
                    throw new InvalidOperationException(errMsg);
                }

                if (!dt.Rows[0].ItemArray[0].ToString().Equals(EngineServerType.RemoteCollector.ToString(),
                    StringComparison.OrdinalIgnoreCase))
                {
                    return new
                    {
                        isPollerToRemoteCollectorAssignment = false,
                        warningHelpLink = string.Empty,
                        unsupportedAgentNodeSelected = false,
                        filteredNodesToReassignIds = nodesToReassignIds
                    };
                }

                qParams.Clear();
                qParams["primaryType"] = EngineServerType.Primary.ToString();
                qParams["additionalType"] = EngineServerType.Additional.ToString();
                qParams["nodeIdList"] = nodesToReassignIds;

                dt = proxy.Query(
                    "SELECT TOP 1 NodeID FROM Orion.Nodes N INNER JOIN Orion.Engines E ON N.EngineID=E.EngineID " +
                    "WHERE N.NodeID IN @nodeIdList AND (E.ServerType=@primaryType OR E.ServerType=@additionalType)", qParams);

                // Remove all Agent nodes as those are not supported on Remote Collector
                var agentNodes = proxy.Query(
                    "SELECT NodeID FROM Orion.Nodes N " +
                    "WHERE N.ObjectSubType = 'Agent' AND N.NodeID IN @nodeIdList", qParams);

                var filteredNodeIds = new HashSet<int>(nodesToReassignIds);
                foreach (DataRow row in agentNodes.Rows)
                {
                    filteredNodeIds.Remove((int)row[0]);
                }

                return new
                {
                    isPollerToRemoteCollectorAssignment = dt.Rows.Count > 0,
                    warningHelpLink = HelpHelper.GetHelpUrl("Orion", "orioncoreorcsupport"),
                    unsupportedAgentNodeSelected = agentNodes.Rows.Count > 0,
                    filteredNodesToReassignIds = filteredNodeIds.ToArray()
                };
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetEnginesTable()
    {
        AuthorizationChecker.AllowNodeManagement();
        return GetEnginesTableForSupportedPollingEngineTypes(Enum.GetValues(typeof(EngineServerType)).OfType<EngineServerType>().ToArray());
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetEnginesTableForSupportedPollingEngineTypes(EngineServerType[] supportedPollingEngineTypes)
    {
        AuthorizationChecker.AllowNodeManagement();
        int pageSize = 0;
        int startRowNumber = 0;
        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        return EnginesDAL.GetPagebleEngines(
            Context.Request.QueryString["sort"], Context.Request.QueryString["dir"],
            startRowNumber + 1, Math.Max(startRowNumber + pageSize, 15), supportedPollingEngineTypes);
    }

    [WebMethod(EnableSession = true)]
    public EngineServerType[] GetSupportedEngineTypesAccordingSelectedNodes(int[] nodeIds)
    {
        AuthorizationChecker.AllowNodeManagement();
        var supportedEngineTypeProvider = new SupportedEngineTypesNodePropertiesProvider();
        return supportedEngineTypeProvider.GetSupportedEngineTypes(nodeIds, OrionModuleManager.GetAddNodeMethodPlugins().Select(item => item.Value)).ToArray();
    }

    [WebMethod(EnableSession = true)]
    public DataTable GetEngineById(int engineId)
    {
        AuthorizationChecker.AllowNodeManagement();
        return EnginesDAL.GetEngineById(engineId);
    }

    [WebMethod(EnableSession = true)]
    public int UpdateNodesPollingEngine(int engineId, string nodeIds)
    {
        AuthorizationChecker.AllowNodeManagement();

        if (!string.IsNullOrEmpty(nodeIds))
        {
            var ids = Array.ConvertAll<String, int>(nodeIds.Split(','), Convert.ToInt32);

            foreach (var nodeId in ids)
            {
                EnsureNodeAccess(nodeId);
            }

            int updatedCount = 0;
            using (var proxy = _blProxyCreator.Create(delegate (Exception ex) { throw ex; }))
                updatedCount = proxy.UpdateNodesPollingEngine(engineId, ids);
            if (updatedCount > 0)
            {
                //TODO RH performance improvement might be needed to sync only Agent nodes
                new AgentManager().SyncPollingEngine(ids, engineId);
            }
            return updatedCount;
        }
        throw new InvalidParamaterValueException("nodeIds parameter is empty");
    }

    [WebMethod(EnableSession = true)]
    public bool DoesAnyRemoteCollectorExist()
    {
        AuthorizationChecker.AllowNodeManagement();

        const string anyRemoteCollectorExistsSwql = @"
SELECT TOP 1 EngineID 
FROM Orion.Engines 
WHERE ServerType = 'RemoteCollector'";

        var anyRemoteCollectorExists = false;

        try
        {
            using (var proxy = InformationServiceProxy.CreateV3())
            {
                anyRemoteCollectorExists = proxy.Query(anyRemoteCollectorExistsSwql).Rows.Count > 0;
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
        }

        return anyRemoteCollectorExists;
    }

    [WebMethod(EnableSession = true)]
    public string GetIPAddressString(string ipAddress)
    {
        return IPAddressHelper.ToStringIp(ipAddress);
    }

    [WebMethod(EnableSession = true)]
    public void CalculateSystemTopology()
    {
        var allowTopology = PackageManager.InstanceWithCache.IsPackageInstalled("Orion.Topology");

        if (allowTopology)
        {
            AuthorizationChecker.AllowNodeManagement();
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {
                // don't calculate topology connections and auto dependencies at Demo server
            }
            else
            {
                using (ITopologyAdapter adapter = NpmAdapterFactory.CreateTopologyAdapter(delegate (Exception ex) { throw ex; }))
                {
                    adapter.CalculateSystemTopology();
                }
            }
        }
        else
        {
            log.Info("Topology package is not installed!");
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void UsePolledStatus(string netObjectIds)
    {
        AuthorizationChecker.AllowNodeManagement();
        EnsureAccess(netObjectIds);

        try
        {
            if (!string.IsNullOrEmpty(netObjectIds))
            {
                var objectId = netObjectIds.Split(':');

                if (objectId.Length > 0)
                {
                    if (objectId[0].Equals("N", StringComparison.OrdinalIgnoreCase))
                    {
                        int nodeId = Convert.ToInt32(objectId[1]);
                        NodeDAL.UpdateCustomStatus(nodeId, false);
                        AuditChangedStatus(nodeId);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            log.Error("Error in Use Polled status function.", ex);
        }

    }

    private void AuditChangedStatus(int nodeId)
    {
        Node node;
        using (var proxy = _blProxyCreator.Create(delegate (Exception ex) { throw ex; }))
        {
            node = proxy.GetNodeWithOptions(nodeId, false, false);
        }



        var originalNodeProperties = new PropertyBag
        {
            {"NodeId", nodeId},
            {"IP_Address", node.IpAddress},
            {"Caption", node.Caption},
            {"Status", node.Status},
            {"CustomStatus", true}

        };

        var currentNodeProperties = new PropertyBag
        {
            {"NodeId", nodeId},
            {"IP_Address", node.IpAddress},
            {"Caption", node.Caption},
            {"Status", node.Status},
            {"CustomStatus", false}
        };

        var indication = new NodeIndication(IndicationType.System_InstanceModified, currentNodeProperties,
            originalNodeProperties);


        var publisher = IndicationPublisher.CreateV3();
        if (publisher != null)
        {
            publisher.ReportIndication(indication);
        }
    }

    private string FixInterfaceEntity(string netobject)
    {
        // The WMI interfaces uses "IW" for Collector entity, but SWIS entity is common "I" as interface
        if (netobject.StartsWith("IW"))
            return netobject.Replace("IW", "I");
        else
            return netobject;
    }

    private bool HandleInterfaceRemoval(string netObjectId)
    {
        try
        {
            int id = 0;
            var parts = FixInterfaceEntity(netObjectId).ToUpperInvariant().Split(':');

            if (parts[0] == "I" && int.TryParse(parts[1], out id))
            {
                EnsureInterfaceAccess(id);

                using (var adapter = NpmAdapterFactory.CreateNodeManagementAdapter((Exception ex) => { throw ex; }))
                {
                    adapter.DeleteInterface(id);
                }

                return true;
            }
        }
        catch (Exception ex)
        {
            log.Error($"Error trying to delete interface of ID '{netObjectId}'.", ex);
        }

        return false;
    }

    private void ParseNetObject(string netObjectId, out string prefix, out int id, out int engineId)
    {
        string[] parts = FixInterfaceEntity(netObjectId).Split(':');

        if (parts.Length >= 3
            && IsSupportedEntity(parts[0])
            && int.TryParse(parts[1], out id)
            && int.TryParse(parts[2], out engineId))
        {
            prefix = parts[0];
            return;
        }

        throw new ArgumentException($"'{netObjectId}' is not a valid net object id");
    }

    private void ParseNetObject(string netObjectId, out string prefix, out int id)
    {
        string[] parts = FixInterfaceEntity(netObjectId).Split(':');

        if (parts.Length >= 2
            && IsSupportedEntity(parts[0])
            && int.TryParse(parts[1], out id))
        {
            prefix = parts[0];
            return;
        }

        throw new ArgumentException($"'{netObjectId}' is not a valid net object id");
    }

    private bool IsSupportedEntity(string prefix)
        => SupportedEntities.Contains(prefix);
    
    private string GetNetObjectId(string prefix, int id)
        => $"{prefix}:{id}";

    /// <summary> Ensures the node with given Id exists and is not limited out for the caller context. </summary>
    private void EnsureNodeAccess(int nodeId)
        => EnsureAccess(NodePrefix, nodeId);

    /// <summary> Ensures the interface with given Id exists and is not limited out for the caller context. </summary>
    private void EnsureInterfaceAccess(int interfaceId)
        => EnsureAccess(InterfacePrefix, interfaceId);

    /// <summary> Ensures the volume with given Id exists and is not limited out for the caller context. </summary>
    private void EnsureVolumeAccess(int volumeId)
        => EnsureAccess(VolumePrefix, volumeId);

    /// <summary> Ensures the net object with given Id exists and is not limited out for the caller context. </summary>
    private void EnsureAccess(string netObjectId)
    {
        if (string.IsNullOrEmpty(netObjectId))
            throw new ArgumentException(nameof(netObjectId));
        
        string prefix;
        int id, engineId = 0;

        if (netObjectId.Count(c => c.Equals(':')) < 2)
        {
            ParseNetObject(netObjectId, out prefix, out id);
        }
        else
        {
            ParseNetObject(netObjectId, out prefix, out id, out engineId);
        }

        EnsureAccess(prefix, id);
    }

    /// <summary>
    /// Ensures the net object with given Id exists and is not limited out for the caller context. 
    /// </summary>
    /// <param name="netObjectPrefix">Net object prefix e.g. N, I, V</param>
    /// <param name="netObjectId">Net object Id</param>
    private void EnsureAccess(string netObjectPrefix, int netObjectId)
    {
        try
        {
            var netObjectType = NetObjectFactory.GetSWISTypeByNetObject(GetNetObjectId(netObjectPrefix, netObjectId));

            var query = string.Format("SELECT Uri FROM {0} WHERE {1} = {2}",
                netObjectType,
                NetObjectFactory.GetPrimaryKeyForSWISType(netObjectType),
                netObjectId);

            using (var swis = InformationServiceProxy.CreateV3())
            {
                var result = swis.Query(query);

                if (result.Rows.Count == 0)
                    throw new InvalidOperationException($"Net object {netObjectPrefix}:{netObjectId} not found");
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }
    }
}
