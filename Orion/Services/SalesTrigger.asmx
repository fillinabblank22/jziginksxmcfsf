﻿<%@ WebService Language="C#" Class="SalesTrigger" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Logging;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.Models.WebIntegration;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using SettingsDAL = SolarWinds.Orion.Core.Common.SettingsDAL;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class SalesTrigger : System.Web.Services.WebService
{
    private static readonly Log Log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator(); 

    [WebMethod]
    public object GetEvalModuleInfo()
    {
        ILicensingDAL licensing = new LicensingDAL();
        var licenses = licensing.GetLicenses();
        List<LicenseInfoModel> evalModules = licenses.Where(x => x.IsEvaluation && !x.IsHidden).ToList();
        licensing.FilterHiddenEvalLicenses(evalModules);
        return SalesTriggerHelper.FormatEvalInfoForJs(evalModules);
    }

    [WebMethod]
    public object GetModuleInfo()
    {
        ILicensingDAL licensing = new LicensingDAL();
        List<LicenseInfoModel> evalModules = licensing.GetLicenses().Where(x => x.DaysRemainingCount<90 && !x.IsEvaluation && !x.IsHidden).ToList();
        return SalesTriggerHelper.FormatInfoForJs(evalModules);
    }

    [WebMethod]
    public object GetMaintenanceModuleInfo()
    {
        AuthorizationChecker.AllowAdmin();
        using (var proxy = _blProxyCreator.Create(ex => Log.Error(ex)))
        {
            string username = HttpContext.Current.Profile.UserName;

            if (!String.IsNullOrEmpty(username) && proxy.IsUserWebIntegrationAvailable(username))
            {
                //if web integration enabled, get data from customer portal
                IEnumerable<MaintenanceStatus> statuses =
                    proxy.GetMaintenanceInfoFromCustomerPortal(username)
                        .Where(s => (s.ExpirationDate - DateTime.UtcNow).Days < 90);

                return SalesTriggerHelper.FormatMaintenanceInfoForJs(statuses);
            }
        }
        return GetModuleInfo();
    }

    [WebMethod]
    public IEnumerable<object> GetPollerLimitInfo()
    {
        AuthorizationChecker.AllowAdmin();

        var result = new List<object>();
        var engineProperties = EngineDAL.GetEngineProperty(EngineDAL.ScaleFactorPropertyType);
        if (engineProperties == null)
        {
            return result;
        }

        int limitReachedScaleFactor;
        if (!int.TryParse(SettingsDAL.Get("PollerLimitReachedScaleFactor"), out limitReachedScaleFactor))
        {
            limitReachedScaleFactor = 100;
        }

        int limitWarningScaleFactor;
        if (!int.TryParse(SettingsDAL.Get("PollerLimitWarningScaleFactor"), out limitWarningScaleFactor))
        {
            limitWarningScaleFactor = 85;
        }

        var pollingDetails = new Dictionary<string, dynamic>();

        foreach (DataRow row in engineProperties.Rows)
        {
            int scaleFactor;
            if (int.TryParse(row["PropertyValue"] as string, out scaleFactor))
            {
                var isPollerLimitReached = (scaleFactor >= limitReachedScaleFactor);
                if (scaleFactor >= limitWarningScaleFactor || isPollerLimitReached)
                {
                    var key = row["ServerName"] as String ?? String.Empty;
                    var engineId = (int)row["EngineID"];

                    if (pollingDetails.ContainsKey(key))
                    {
                        if (scaleFactor > pollingDetails[key].ScaleFactor)
                        {
                           pollingDetails[key] = new { ScaleFactor = scaleFactor, IsPollerLimitReached = isPollerLimitReached, EngineID = engineId};
                        }
                    }
                    else
                    {
                        pollingDetails[key] = new { ScaleFactor = scaleFactor, IsPollerLimitReached = isPollerLimitReached, EngineID = engineId};
                    }
                }
            }
        }

        pollingDetails.ToList().ForEach(d => result.Add(new
        {
            EngineName = d.Key,
            CurrentUsage = d.Value.ScaleFactor,
            d.Value.IsPollerLimitReached,
            d.Value.EngineID
        }));

        return result;
    }    
    
    [WebMethod]
    public object GetUpgradeModuleInfo()
    {
        AuthorizationChecker.AllowAdmin();
        var upgradeRequest = new UpgradeRequest(){Products = new List<ProductContract>()};
        using (var proxy = _blProxyCreator.Create(ex => Log.Error(ex)))
        {
            var items = proxy.GetMaintenanceRenewalNotificationItems(false);
            string notificationMessage = String.Format(Resources.CoreWebContent.WEBDATA_ZS0_001, items.Count);
            List<ModuleLicenseInfo> allModules = proxy.GetModuleLicenseInformation();
            var ugradeItemsList = new List<dynamic>();

            foreach (var item in items)
            {
                var moduleInfo = ModulesCollector.GetModuleInfo(item.ProductTag);

                if (items.Count == 1)
                {
                    notificationMessage = moduleInfo != null ? string.Format(Resources.CoreWebContent.WEBDATA_ZS0_002, moduleInfo.ProductName) : Resources.CoreWebContent.WEBDATA_ZS0_003;
                }

                var licenseInfo = allModules.FirstOrDefault(p => p.ModuleName == moduleInfo.ProductShortName);
                bool isVersionDifference = VersionDifference(moduleInfo.Version, item.NewVersion);
                ugradeItemsList.Add(new
                {
                    Version = item.NewVersion,
                    ProductDisplayName = moduleInfo.ProductName,
                    IsMaintenanceExpired = licenseInfo != null && licenseInfo.MaintenanceExpiration < DateTime.UtcNow,
                    ModuleName = moduleInfo.ProductTag,
                    IsVersionDifference = isVersionDifference,
                    ReleaseNotes = item.Description,
                    DownloadLink = item.Url
                });
            }

            upgradeRequest.Database = SqlDAL.GetSQLEngineDBContract();
            upgradeRequest.OperatingSystem = new OperatingSystemContract
            {
                Name = OSVersionHelper.GetOSName(),
                Version = Environment.OSVersion.Version.ToString(),
                ServicePack = Environment.OSVersion.ServicePack,
                Edition = OSVersionHelper.GetOSEdition(),
                Bits = Environment.Is64BitOperatingSystem ? "64" : "32"
            };

            foreach (var module in ModulesCollector.GetInstalledModules())
            {
                upgradeRequest.Products.Add(new ProductContract
                {
                    ProductName = module.ProductName,
                    ProductShortName = module.ProductShortName,
                    PublicVersion = module.Version
                });
            }

            return new
            {
                title = notificationMessage,
                ugradeItems = ugradeItemsList.ToArray(),
                upgradeRequestData = upgradeRequest
            };
        }
    }

    [WebMethod]
    public object GetLicenseModuleInfo()
    {
        using (var proxy = _blProxyCreator.Create(ex => Log.Error(ex)))
        {
            return proxy.GetModuleSaturationInformation();
        }
    }
 
    [WebMethod]
    public void DontRemindMeAgain(string notificationType)
    {
        var type = (NotificationType)Enum.Parse(typeof(NotificationType), notificationType);
        var typeGuid = new Guid();
        var notificationId = new Guid();
            
        switch (type)
        {
            case NotificationType.Eval:
                {
                    typeGuid = new Guid("6EE3D05F-7555-4E3E-9338-AA338834FE36");
                    SolarWinds.Orion.Web.DAL.SettingsDAL.SetValue("EvaluationExpiration-Check", 0);
                    break;
                }
            case NotificationType.Maintenance:
                {
                    // Unique identifier of the Maintenance Expiration notification item used as NotificationItem ID (don't change the GUID value).
                    notificationId = new Guid("561BE782-187F-4977-B5C4-B8666E73E582");
                    SolarWinds.Orion.Web.DAL.SettingsDAL.SetValue("MaintenanceExpiration-Check", 0);
                    break;
                }
            case NotificationType.Licensing:
                {
                    SolarWinds.Orion.Web.DAL.SettingsDAL.SetValue("LicenseSaturation-Disable", 1);
                    typeGuid = new Guid("3604B78F-77F9-429B-B5E1-03462A563E66");
                    break;
                }
            case NotificationType.Polling:
                {
                    // Unique identifier of the Poller Limit notification item used as NotificationItem ID (don't change the GUID value).
                    notificationId = new Guid("C7070869-B2B8-42ED-8472-7F24056435D9");
                    SolarWinds.Orion.Web.DAL.SettingsDAL.SetValue("PollerLimit-Check", 0);
                    break;
                }
            case NotificationType.Upgrade:
                {
                    SolarWinds.Orion.Web.DAL.SettingsDAL.SetValue("MaintenanceRenewals-Check", 1);
                    typeGuid = new Guid("60FB0695-15BC-4F16-9DCA-A844AE17E714");
                    break;
                }
        }
        
        if (type == NotificationType.Maintenance || type == NotificationType.Polling)
        {
            using (var proxy = _blProxyCreator.Create(ex => Log.Error(ex)))
            {
                proxy.AcknowledgeNotificationItem(notificationId, HttpContext.Current.Profile.UserName, DateTime.UtcNow);
            }
        }
        else
        {
            using (var proxy = _blProxyCreator.Create(ex => Log.Error(ex)))
            {
                proxy.AcknowledgeNotificationItemsByType(typeGuid, (HttpContext.Current.Profile).UserName, DateTime.UtcNow);
            }
        }
    }

    public enum NotificationType
    {
        Eval,
        Maintenance,
        Licensing,
        Polling,
        Upgrade  
    }

    private bool VersionDifference(string currentVersion, string newVersion)
    {
        Version parsedCurrentVersion;
        Version parsedNewVersion;
        if (!Version.TryParse(currentVersion, out parsedCurrentVersion))
            return false;
        if (!Version.TryParse(newVersion, out parsedNewVersion))
            return false;
        if (parsedNewVersion.Major != parsedCurrentVersion.Major)
            return true;
        return false;
    }
}
