﻿<%@ WebService Language="C#" Class="SMTPServers" %>
using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Newtonsoft.Json;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Security;
using SolarWinds.Orion.Web;
using System.Collections.Generic;
using System.Data;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class SMTPServers : System.Web.Services.WebService {

    private static Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    [WebMethod]
    public PageableDataTable GetSMTPServers(string property, string type, string value, string search)
    {
        int pageSize = 0;
        int startRowNumber = 0;

        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        if (pageSize == 0)
            pageSize = 20;

        string sortOrder = "s.Address";
        if (!string.IsNullOrEmpty(sortColumn))
        {
            sortOrder = string.Format("s.{0} {1}", sortColumn, sortDirection);
        }
        Dictionary<string, object> queryParams = new Dictionary<string, object>();
        string queryString =
            String.Format(
                @"SELECT s.SMTPServerId, s.Address, s.Port, s.CredentialID, s.IsDefault, s.EnabledSSL, s.BackupServerID, sb.Address as BackupServerAddress
FROM Orion.SMTPServers (nolock=true) s 
LEFT JOIN Orion.SMTPServers (nolock=true) sb ON s.BackupServerID = sb.SMTPServerId
               ORDER BY {0}  
               WITH ROWS @startRowNumber TO @endRowNumber",
                sortOrder);

        queryParams[@"startRowNumber"] = startRowNumber + 1;
        queryParams[@"endRowNumber"] = startRowNumber + pageSize;

        string countQuery =
            string.Format(
                @"SELECT Count(s.SMTPServerID) AS serversCount FROM Orion.SMTPServers (nolock=true) s");
   
        DataTable ssDataTable;
        DataTable countTable;

        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                ssDataTable = service.Query(queryString, queryParams);
                countTable = service.Query(countQuery);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

        return new PageableDataTable(ssDataTable,
                                     (countTable.Rows == null || countTable.Rows.Count == 0
                                          ? 0
                                          : (int) ((countTable.Rows[0][0] is System.DBNull) ? 0 : countTable.Rows[0][0])));
    }

    [WebMethod]
    public int GetSMTPServerNumber(string serverId, string direction, string sort)
    {
        string sortOrder = "s.Address";
        if (!string.IsNullOrEmpty(sort))
        {
            sortOrder = string.Format("s.{0} {1}", sort, direction);
        }
        string queryString =
            String.Format(
                @"SELECT s.SMTPServerID FROM Orion.SMTPServers (nolock=true) s 
                                            ORDER BY {0}",
                sortOrder);

        DataTable ssDataTable;
        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                ssDataTable = service.Query(queryString);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

        if (ssDataTable == null || ssDataTable.Rows == null || ssDataTable.Rows.Count == 0)
            return -1;

        int rowId = 0;
        foreach (DataRow row in ssDataTable.Rows)
        {
            if (Convert.ToString(row["SMTPServerID"]) == serverId)
                return rowId;
            rowId++;
        }
        return -1;
    }

    [WebMethod]
    public int AddSmtpServer(SmtpServer smtpServer)
    {
        AuthorizationChecker.AllowAdmin();
        try
        {
            using (var proxy = _blProxyCreator.Create(ex => log.Error(ex)))
            {
                return proxy.InsertSmtpServer(smtpServer); 
            }

        }
        catch (Exception ex)
        {
            log.Error("Couldn't update SMTPServer.",ex);
            throw;
        }
    }

    [WebMethod]
    public bool UpdateSmtpServer(SmtpServer smtpServer)
    {
        AuthorizationChecker.AllowAdmin();
        try
        {
            using (var proxy = _blProxyCreator.Create(ex => log.Error(ex)))
            {
                var result = proxy.UpdateSmtpServer(smtpServer);
                if (smtpServer.IsDefault)
                {
                    SaveDefaultSmtpServer(smtpServer);
                }
                return result;
            }

        }
        catch (Exception ex)
        {
            log.Error("Couldn't update SMTPServer.",ex);
            throw;
        }
    }

    [WebMethod]
    public bool DeleteSmtpServer(string[] ids)
    {
        AuthorizationChecker.AllowAdmin();
        try
        {

            foreach (var id in ids.Where(id => Convert.ToInt32(id) != 0))
            {
                using (var proxy = _blProxyCreator.Create(ex => log.Error(ex)))
                {
                    proxy.DeleteSmtpServer(Convert.ToInt32(id));
                }
            }

            return true;
        }
        catch (Exception ex)
        {
            log.Error("Couldn't delete SMTPServer.", ex);
            throw;
        }
    }

    [WebMethod]
    public bool SetSmtpServerAsDefault(string id)
    {
        AuthorizationChecker.AllowAdmin();
        try
        {
            using (var proxy = _blProxyCreator.Create(ex => log.Error(ex)))
            {
                proxy.SetSmtpServerAsDefault(Convert.ToInt32(id));
                var smtpServer = proxy.GetSmtpServer(Convert.ToInt32(id));
                SaveDefaultSmtpServer(smtpServer);
            }

            return true;
        }
        catch (Exception ex)
        {
            log.Error("Couldn't set as default SMTPServer.", ex);
            throw;
        }
    }

    [WebMethod]
    public string GetUsedSmtpServers(string[] ids)
    {
        string smtpServersIds = string.Join(",", ids);
        string sqlQuery = String.Format(@"SELECT DISTINCT Address FROM Orion.ActionsProperties (nolock=true) ap
                                          LEFT JOIN Orion.SMTPServers (nolock=true) ss ON ap.PropertyValue = ss.SMTPServerID
                                          WHERE PropertyValue IN({0}) AND PropertyName = 'SmtpServerID'", smtpServersIds);
        DataTable ssDataTable;

        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                ssDataTable = service.Query(sqlQuery);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }
        List<string> addressList = (from DataRow row in ssDataTable.Rows select row.ItemArray[0].ToString()).ToList();

        return JsonConvert.SerializeObject(addressList);
    }
    
    [WebMethod]
    public List<object> GetSmtpServersForDropDown(bool isPrimarySmtpServerDd)
    {
        string queryString = @"SELECT SMTPServerID, Address FROM Orion.SMTPServers";
        DataTable ssDataTable;

        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                ssDataTable = service.Query(queryString);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

        var smtpServersList = new List<object>();
        bool defaultSmtpServerExists = DefaultSmtpServerExists();
        if (isPrimarySmtpServerDd && defaultSmtpServerExists)
        {
            smtpServersList.Add(new { id = "", name = Resources.CoreWebContent.WEBDATA_VL0_77 });
        }
        
        smtpServersList.AddRange((from DataRow dtRow in ssDataTable.Rows select new { id = dtRow.ItemArray[0], name = dtRow.ItemArray[1] }).Cast<object>().ToList());

        if (isPrimarySmtpServerDd)
        {
            smtpServersList.Add(new { id = "-1", name = Resources.CoreWebContent.WEBDATA_YK0_74 });
            smtpServersList.Add(new { id = "-2", name = Resources.CoreWebContent.WEBDATA_AY0_75 });
        }
        else
        {
            smtpServersList.Add(new { id = "0", name = Resources.CoreWebContent.WEBDATA_VL0_37 });
            smtpServersList.Add(new { id = "-2", name = Resources.CoreWebContent.WEBDATA_AY0_75 });
        }

        return smtpServersList;
    }

    [WebMethod]
    public Dictionary<string, bool> ValidateSmtpServer(SmtpServer config)
    {
        Dictionary<string, bool> validationResults = new Dictionary<string, bool>();
        validationResults.Add("smtpServerIP", CommonHelper.IsValidHostNameOrIpAddress(config.Address));
        validationResults.Add("smtpServerPort", config.Port != 0);

        string queryString =String.Format(@"SELECT s.Address FROM Orion.SMTPServers (nolock=true) s 
                                            WHERE s.Address=@address",config.Address);

        Dictionary<string, object> queryParams = new Dictionary<string, object>();
        queryParams[@"address"] = config.Address;
        DataTable ssDataTable;
        try
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                ssDataTable = service.Query(queryString,queryParams);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            throw;
        }

        if (ssDataTable == null || ssDataTable.Rows == null || ssDataTable.Rows.Count == 0)
            validationResults.Add("smtpServerExist", false);
        else
            validationResults.Add("smtpServerExist", true);


        return validationResults;
    }

    [WebMethod]
    public bool DefaultSmtpServerExists() 
    {
        bool defaultSmtpServerExists = false;
        try
        {
            using (var proxy = _blProxyCreator.Create(ex => log.Error(ex)))
            {
                defaultSmtpServerExists = proxy.DefaultSmtpServerExists();
                
            }
        }
        catch (Exception ex)
        {
            log.Error("Couldn't check if default SMTPServer exists.", ex);
            throw;
        }
        return defaultSmtpServerExists;
    }

    private void SaveDefaultSmtpServer(SmtpServer smtpServer)
    {
        if (smtpServer.Credentials != null && !string.IsNullOrEmpty(smtpServer.Credentials.Username) &&
            !string.IsNullOrEmpty(smtpServer.Credentials.Password))
        {
            var cryptoHelper = new CryptoHelper();
            smtpServer.Credentials.Password = cryptoHelper.Encrypt(smtpServer.Credentials.Password);
        }
        SolarWinds.Orion.Web.DAL.WebSettingsDAL.SetSmtpServerAsDefault(smtpServer);
    }

}

