﻿<%@ WebService Language="C#" Class="AddRemoveObjects" %>
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using Resources;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Shared;
using SolarWinds.Orion.Core.Common.Enums;
using SolarWinds.Orion.Core.Common.InformationService;
using InformationServiceProxy = SolarWinds.Orion.Web.InformationService.InformationServiceProxy;
using SwisEntityHelper = SolarWinds.Orion.Web.SwisEntityHelper;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class AddRemoveObjects : WebService
{
    private const string CustomProperties = "CustomProperties";
    private const string StatusProperty = "Status";
    private const string CategoryProperty = "Category";
    private readonly string UnknownValue = Resources.CoreWebContent.WEBCODE_AK0_121;
    private readonly string NoGroupingValue = Resources.CoreWebContent.WEBCODE_AK0_70;


    public class GroupByItem
    {
        public string Column { get; set; }
        public string DisplayName { get; set; }
        public string Type { get; set; }

        public GroupByItem()
        {
            Column = String.Empty;
            DisplayName = String.Empty;
            Type = String.Empty;
        }

        public GroupByItem(string column, string displayName, string type)
        {
            Column = column;
            DisplayName = displayName;
            Type = type;
        }
    }


    [WebMethod]
    public IEnumerable<GroupByItem> GetGroupByProperties(string entityType)
    {
        var properties = new List<GroupByItem>();

        properties.Add(new GroupByItem(String.Empty, NoGroupingValue, String.Empty));

        string query = String.Format(@"SELECT Name,
                                              DisplayName,
                                              Type,
                                              IsNavigable
                                       FROM Metadata.Property
                                       WHERE EntityName = '{0}' AND GroupBy = 'true'", entityType);

        using (var swis = InformationServiceProxy.CreateV3())
        {
            DataTable table = swis.QueryWithAppendedErrors(query);

            foreach (DataRow row in table.Rows)
            {
                string name = row["Name"].ToString();
                string displayName = !(row["DisplayName"] == DBNull.Value || String.IsNullOrEmpty((string)row["DisplayName"])) ? row["DisplayName"].ToString() : name;
                string type = row["Type"].ToString();
                bool isNavigable = (bool)row["IsNavigable"];

                // select custom properties for this entity type
                if (name.Equals(CustomProperties, StringComparison.OrdinalIgnoreCase))
                {
                    String customQuery = String.Format(@"SELECT Name,
                                                                DisplayName,
                                                                Type
                                                         FROM Metadata.Property
                                                         WHERE EntityName = '{0}'
                                                           AND IsNavigable = 'false'
                                                           AND IsInherited = 'false'
                                                           AND IsInjected = 'false'
                                                           AND GroupBy = 'true'
                                                           AND (Type<>'System.String' OR IsSortable='true')", type);

                    DataTable customTable = swis.QueryWithAppendedErrors(customQuery);

                    foreach (DataRow customRow in customTable.Rows)
                    {
                        name = customRow["Name"].ToString();
                        displayName = !(row["DisplayName"] == DBNull.Value || String.IsNullOrEmpty((string)row["DisplayName"])) ? customRow["DisplayName"].ToString() : name;
                        type = customRow["Type"].ToString();

                        properties.Add(new GroupByItem()
                        {
                            Column = String.Format("{0}.{1}.{2}", entityType, CustomProperties, name),
                            DisplayName = UIHelper.Escape(displayName),
                            Type = type
                        });
                    }
                }
                else
                {
                    if (isNavigable)
                    {
                        properties.Add(new GroupByItem()
                        {
                            Column = String.Format("{0}.{1}.DisplayName", entityType, name),
                            DisplayName = UIHelper.Escape(displayName),
                            Type = "System.String"
                        });
                    }
                    else
                    {
                        properties.Add(new GroupByItem()
                        {
                            Column = String.Format("{0}.{1}", entityType, name),
                            DisplayName = UIHelper.Escape(displayName),
                            Type = type
                        });
                    }
                }
            }
        }

        return properties;
    }



    public class EntityGroup
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public int Count { get; set; }
        public List<int> ChildStatuses { get; set; }

        public int Status
        {
            get
            {
                if (ChildStatuses.Count > 1)
                {
                    return StatusInfo.RollupStatus(ChildStatuses, EvaluationMethod.Worst);
                }
                else
                {
                    return ChildStatuses[0];
                }
            }
        }

        public EntityGroup()
        {
            ChildStatuses = new List<int>();
        }
    }

    [WebMethod(EnableSession = true, MessageName = "GetDefaultGroupByProperties")]
    public Dictionary<string,string> GetDefaultGroupByProperties()
    {
        return SwisEntityHelper.GetEntityDefaultGroupingData();
    }

    [WebMethod(EnableSession = true, MessageName = "GetObjectPickerEntityGroups")]
    public IEnumerable<ContainersMetadataDAL.EntityGroup> GetObjectPickerEntityGroups(string entityType, string groupByProperty, string searchValue, string viewId)
    {
        return GetObjectPickerFilteredEntityGroups(entityType, groupByProperty, searchValue, viewId, null);
    }

    [WebMethod(EnableSession = true, MessageName = "GetObjectPickerFilteredEntityGroups")]
    public IEnumerable<ContainersMetadataDAL.EntityGroup> GetObjectPickerFilteredEntityGroups(string entityType, string groupByProperty, string searchValue, string viewId, string filter)
    {
        int view;
        var viewLimitationId = 0;
        if (int.TryParse(viewId, out view))
        {
            viewLimitationId = ViewManager.GetViewById(view).LimitationID;
        }

        var groups = (new ContainersMetadataDAL()).GetEntityGroups(entityType, groupByProperty, searchValue, new List<string>(), true, viewLimitationId, filter, entityType.Equals("Orion.ContainerMembers"));
        var maxCount = ContainerHelper.GetManageGroupsMaxObjectTreeItems();

        if (groups != null && groups.Count() > maxCount)
        {
            var remainingCount = groups.Count() - maxCount;
            var newGroups = groups.Take(maxCount);
            var entityGroup = new List<ContainersMetadataDAL.EntityGroup>()
            {
                new ContainersMetadataDAL.EntityGroup()
                    {
                        Name = (remainingCount > 1) ? String.Format(CoreWebContent.WEBCODE_VB1_20, remainingCount) : String.Format(CoreWebContent.WEBCODE_VB1_21, remainingCount)
                    }
            };
            return newGroups.Concat(entityGroup);
        }

        return groups;
    }

    [WebMethod(EnableSession = true, MessageName = "GetEntityGroups")]
    public IEnumerable<EntityGroup> GetEntityGroups(string entityType, string groupByProperty, string searchValue, string[] excludeDefinitions, string filter)
    {
        return GetEntityGroups(entityType, groupByProperty, searchValue, excludeDefinitions, filter, "");
    }

    [WebMethod(EnableSession = true, MessageName = "GetEntityGroupsWithLimitation")]
    public IEnumerable<EntityGroup> GetEntityGroups(string entityType, string groupByProperty, string searchValue, string[] excludeDefinitions, string filter, string viewId)
    {
        int? viewLimitationId = GetLimitationIdFromViewIdString(viewId);
        var displayNameField = SwisEntityHelper.IsManagedEntity(entityType) ? "AncestorDisplayNames" : "DisplayName";
        var whereList = new List<String>();
        string whereQuery = String.Empty;

        if (!String.IsNullOrEmpty(filter))
        {
            whereList.Add(String.Format(" {0} ", filter));
        }

        if (!String.IsNullOrEmpty(searchValue))
        {
            whereList.Add(String.Format(" e.{1} LIKE '%{0}%' ", searchValue.Replace("'", "''"), displayNameField));

        }

        if (excludeDefinitions != null && excludeDefinitions.Length > 0)
        {
            // convert values like 10_0, 9_0, 5_0 to 10, 9, 5
            var ids = excludeDefinitions.Select(ed => ed.Split('_')[0]).ToList();
            whereList.Add(String.Format(" e.{0} NOT IN ({1}) ", NetObjectTypeToSWISEntityMapper.GetIdColumn(entityType), string.Join(",", ids)));
        }

        if (whereList.Count > 0)
        {
            whereQuery = String.Format(" WHERE {0} ", string.Join(" AND ", whereList.ToArray()));
        }


        // remove entity type prefix
        groupByProperty = groupByProperty.Replace(entityType + ".", string.Empty);

        string query = String.Format(@"SELECT {1} e.Status, Count(e.Status) AS Cnt
                                       FROM {0} e
                                       {2}
                                       GROUP BY {1} e.Status
                                       ORDER BY {3} {4}",
                                        entityType,
                                        groupByProperty.Equals(StatusProperty, StringComparison.OrdinalIgnoreCase) ? String.Empty : String.Format("e.{0},", groupByProperty),
                                        whereQuery,
                                        String.Format("e.{0}", groupByProperty),

                                        viewLimitationId != null ? String.Format("WITH LIMITATION {0}", viewLimitationId) : String.Empty
                                        );

        var entityGroups = new Dictionary<String, EntityGroup>();

        using (var swis = InformationServiceProxy.CreateV3())
        {
            DataTable table = swis.QueryWithAppendedErrors(query);

            foreach (DataRow row in table.Rows)
            {
                string property = row[0].ToString();
                string value = row[0].ToString();
                int count = Convert.ToInt32(row["Cnt"]);
                int status = Convert.ToInt32(row["Status"]);

                // show statuses instead of numbers
                if (groupByProperty.Equals(StatusProperty, StringComparison.OrdinalIgnoreCase))
                {
                    property = StatusInfo.GetStatus(Convert.ToInt32(property)).ShortDescription;
                }

                // show node category instead of numbers
                if (groupByProperty.Equals(CategoryProperty, StringComparison.OrdinalIgnoreCase))
                {
                    int nc;

                    if (Int32.TryParse(property, out nc))
                    {
                        property = NodeCategoryHelper.GetLocalizedName((NodeCategory)nc);
                    }
                }

                // show [Unknown] for empty strings
                if (String.IsNullOrEmpty(property))
                {
                    property = UnknownValue;
                }

                if (entityGroups.ContainsKey(property))
                {
                    EntityGroup group = entityGroups[property];
                    group.Count += count;
                    group.ChildStatuses.Add(status);
                }
                else
                {
                    EntityGroup group = new EntityGroup()
                    {
                        Name = property,
                        Value = value,
                        Count = count
                    };

                    group.ChildStatuses.Add(status);
                    entityGroups[property] = group;
                }
            }
        }

        return entityGroups.Values;
    }

    public class Entity
    {
        public string ID { get; set; }
        public string Uri { get; set; }
        public string FullName { get; set; }
        public string Status { get; set; }
        public string StatusIconHint { get; set; }
        public string ItemType { get; set;}
        public int InstanceSiteId { get; set; }
    }

    private string GenerateFullName(IList<string> names)
    {
        if (names == null)
            throw new ArgumentNullException("names");

        return System.Web.HttpUtility.HtmlEncode(string.Join(Resources.CoreWebContent.WEBCODE_VB1_9, names.ToArray()));
    }


    [WebMethod(EnableSession = true, MessageName = "LoadEntities")]
    public IEnumerable<Entity> LoadEntities(string entityType, string entityIds)
    {
        return LoadEntities(entityType, entityIds, "");
    }

    [WebMethod(EnableSession = true, MessageName = "LoadEntitiesWithLimitation")]
    public IEnumerable<Entity> LoadEntities(string entityType, string entityIds, string viewId)
    {
        var isManagedEntity = SwisEntityHelper.IsManagedEntity(entityType);
        var displayNameField = isManagedEntity ? "AncestorDisplayNames" : "DisplayName";
        int? viewLimitationId = GetLimitationIdFromViewIdString(viewId);
        var entities = new List<Entity>();

        if (String.IsNullOrEmpty(entityType) || String.IsNullOrEmpty(entityIds))
        {
            return entities;
        }

        string query = String.Format(@"
SELECT e.{1}, 
       e.Status, 
       e.{4} as AncestorDisplayNames 
FROM {0} e
WHERE e.{1} IN ({2}) 
ORDER BY e.DisplayName {3}", entityType, NetObjectTypeToSWISEntityMapper.GetIdColumn(entityType), entityIds,
                       viewLimitationId != null ? String.Format("WITH LIMITATION {0}", viewLimitationId) : String.Empty, displayNameField);

        using (var swis = InformationServiceProxy.CreateV3())
        {
            DataTable table = swis.QueryWithAppendedErrors(query);

            if (table != null && table.Rows.Count > 0)
            {
                foreach (DataRow row in table.Rows)
                {
                    entities.Add(new Entity()
                    {
                        ID = row[0].ToString(),
                        Status = row[1].ToString(),
                        FullName = isManagedEntity? GenerateFullName((string[])row[2]): row[2].ToString()
                    });
                }
            }
        }

        return entities;
    }

    [Serializable]
    public struct EntityInfo
    {
        public string Entity;
        public string DisplayName;
        public string DisplayNameSingular;
    }

    [Serializable]
    public struct ServerInfo
    {
        public int ServerID;
        public string Name;
        public bool Down;
    }

    [WebMethod(EnableSession = true, MessageName = "GetAvailableEntityNames")]
    public EntityInfo[] GetAvailableEntityNames(string[] entities)
    {
        var entitiesInfo = new List<EntityInfo>();
        if (entities == null || entities.Length == 0) return entitiesInfo.ToArray();
        var query =
       String.Format(@"SELECT DISTINCT(FullName), ISNULL(DisplayNamePlural, Entity.DisplayName) AS DisplayNamePlural, Entity.DisplayName
                            FROM Metadata.Entity
                            LEFT JOIN Orion.NetObjectTypes ON EntityType = Fullname
                            WHERE FullName IN ('{0}') OR NetObjectTypes.Name IN ('{0}')",
                     String.Join("','", entities));

        using (var swis = InformationServiceProxy.CreateV3())
        {
            var table = swis.QueryWithAppendedErrors(query);

            if (table != null && table.Rows.Count > 0)
            {
                foreach (DataRow row in table.Rows)
                {
                    entitiesInfo.Add(new EntityInfo
                    {
                        Entity = row[0].ToString(),
                        DisplayName = row[1].ToString(),
                        DisplayNameSingular = row[2].ToString()
                    });
                }
            }
        }

        return entitiesInfo.ToArray();
    }

    [WebMethod(EnableSession = true, MessageName = "GetSolarwindsServers")]
    public ServerInfo[] GetSolarwindsServers(bool enabledServersFilter)
    {
        List<ServerInfo> serverInfo = new List<ServerInfo>();
        string query = @"SELECT SiteID as ServerID, Name, (CASE WHEN Status = 1 THEN 'false' ELSE 'true' END) AS Down 
                        FROM Orion.Sites
                        WHERE IsLocal=0 AND Enabled=@enabled";
        using (var swis = InformationServiceProxy.CreateV3())
        {
            var table = swis.QueryWithAppendedErrors(query, new Dictionary<string, object> { { "enabled", enabledServersFilter } });

            if (table != null && table.Rows.Count > 0)
            {
                serverInfo.AddRange(from DataRow row in table.Rows select new ServerInfo { ServerID = Convert.ToInt32(row[0]), Name = row[1].ToString(), Down = Convert.ToBoolean(row[2])});
            }
        }

        return serverInfo.ToArray();
    }

    [WebMethod(EnableSession = true, MessageName = "LoadEntitiesByUris")]
    public IEnumerable<Entity> LoadEntitiesByUris(string entityType, string[] uris)
    {
        var entities = new List<Entity>();

        if (uris == null || uris.Length == 0) return entities;

        var entityIds = uris.Select(uri => UriHelper.GetStringId(uri)).ToList();
        var isManagedEntity = SwisEntityHelper.IsManagedEntity(entityType);
        var displayNameField = isManagedEntity ? "AncestorDisplayNames" : "DisplayName";

        var idColumn = NetObjectTypeToSWISEntityMapper.GetIdColumn(entityType, false);
        var isKnownIdColumn = !string.IsNullOrEmpty(idColumn);
        bool statusColumnExists = SwisEntityHelper.StatusColumnExists(entityType);
        bool statusHintIconColumnExists = SwisEntityHelper.StatusIconHintColumnExists(entityType);

        //Fix for FB359640 
        var uriColumn = "Uri";
        bool isContainerMembers = false;
        if (entityType.Equals("Orion.ContainerMembers"))
        {
            uriColumn = "MemberUri";
            isContainerMembers = true;
        }

        string query =
            String.Format(
                @"
SELECT e.{1}, 
       {4} AS Status,
       e.{6},
       e.{3} as AncestorDisplayNames,
       {5} AS StatusIconHint
       {7}
       {8}
FROM {0} (nolock=true) e 
WHERE e.{1} IN ({2}) 
ORDER BY e.DisplayName ",
                entityType,
                (isKnownIdColumn) ? idColumn : uriColumn,
                (isKnownIdColumn)
                    ? string.Join(",", entityIds.ToArray())
                    : string.Format("'{0}'", string.Join("','", uris)),
                displayNameField,
                statusColumnExists ? "e.Status" : "-1",
                statusHintIconColumnExists ? "e.StatusIconHint" : "''",
                uriColumn,
                isContainerMembers ? ", e.MemberEntityType" : string.Empty,
                ", e.InstanceSiteID AS InstanceSiteID");

        using (var swis = InformationServiceProxy.CreateV3())
        {
            DataTable table = swis.QueryWithAppendedErrors(query);

            if (table != null && table.Rows.Count > 0)
            {
                foreach (DataRow row in table.Rows)
                {
                    entities.Add(new Entity()
                    {
                        ID = (isKnownIdColumn) ? row[0].ToString() : UriHelper.GetStringId(row[0].ToString()),
                        Status = row[1].ToString(),
                        StatusIconHint = row[4].ToString(),
                        Uri = row[2].ToString(),
                        FullName =
                                             (isManagedEntity) ? GenerateFullName((string[]) row[3]) : row[3].ToString(),
                        ItemType = isContainerMembers ? row.Field<string>("MemberEntityType") : entityType,
                        InstanceSiteId = row.Field<int>("InstanceSiteID")
                    });
                }
            }
        }

        return entities;
    }

    [WebMethod(EnableSession = true, MessageName = "LoadTopOneEntityByEntityType")]
    public Entity LoadTopOneEntityByEntityType(string entityType)
    {
        var entity = new Entity();

        var isManagedEntity = SwisEntityHelper.IsManagedEntity(entityType);
        var displayNameField = isManagedEntity ? "AncestorDisplayNames" : "DisplayName";

        var idColumn = NetObjectTypeToSWISEntityMapper.GetIdColumn(entityType, false);
        var isKnownIdColumn = !string.IsNullOrEmpty(idColumn);
        bool statusColumnExists = SwisEntityHelper.StatusColumnExists(entityType);
        bool statusHintIconColumnExists = SwisEntityHelper.StatusIconHintColumnExists(entityType);

        var uriColumn = "Uri";
        bool isContainerMembers = false;
        if (entityType.Equals("Orion.ContainerMembers"))
        {
            uriColumn = "MemberUri";
            isContainerMembers = true;
        }

        string query =
            String.Format(
                @"
SELECT TOP 1 e.{1}, 
       {3} AS Status,
       e.{6},
       e.{2} as AncestorDisplayNames,
       {4} AS StatusIconHint
       {5}
FROM {0} (nolock=true) e",
                entityType, (isKnownIdColumn) ? idColumn : "Uri",
                displayNameField,
                statusColumnExists ? "e.Status" : "-1",
                statusHintIconColumnExists ? "e.StatusIconHint" : "''", isContainerMembers ? ", e.MemberEntityType" : string.Empty, uriColumn);

        using (var swis = InformationServiceProxy.CreateV3())
        {
            DataTable table = swis.QueryWithAppendedErrors(query);

            if (table != null && table.Rows.Count > 0)
            {
                foreach (DataRow row in table.Rows)
                {
                    entity = new Entity()
                    {
                        ID = (isKnownIdColumn) ? row[0].ToString() : UriHelper.GetStringId(row[0].ToString()),
                        Status = row[1].ToString(),
                        StatusIconHint = row[4].ToString(),
                        Uri = row[2].ToString(),
                        FullName =
                             (isManagedEntity) ? GenerateFullName((string[])row[3]) : row[3].ToString(),
                        ItemType = isContainerMembers ? row[5].ToString() : entityType
                    };
                }
            }
        }
        return entity;
    }

    [WebMethod(EnableSession = true, MessageName = "LoadMultipleEntitiesByUris")]
    public IEnumerable<Entity> LoadMultipleEntitiesByUris(string [] uris)
    {
        var objectsList = new List<dynamic>();
        var entities = new List<Entity>();

        foreach (var uri in uris)
        {
            objectsList.Add(new
            {
                entity = UriHelper.GetUriType(uri),
                objUri = uri
            });
        }

        var groupedObjectsList = objectsList.GroupBy(x => x.entity, x => x.objUri, (key, g) => new { entity = key, uris = g.ToArray() }).ToList();

        foreach (var obj in groupedObjectsList)
        {
            entities.AddRange(LoadEntitiesByUris(obj.entity, obj.uris.Cast<string>().ToArray()));
        }
        return entities;
    }

    [WebMethod(EnableSession = true, MessageName = "LoadEntitiesByIds")]
    public IEnumerable<Entity> LoadEntitiesByIds(string entityType, string[] uris)
    {
        var entities = new List<Entity>();

        if (uris == null || uris.Length == 0) return entities;
        var entityIds = string.Join(",", uris);
        string query =
            String.Format(
                @"
SELECT e.Uri       
FROM {0} (nolock=true) e 
WHERE e.NodeId IN ({1}) 
ORDER BY e.DisplayName ",
                entityType, entityIds);

        using (var swis = InformationServiceProxy.CreateV3())
        {
            DataTable table = swis.QueryWithAppendedErrors(query);

            if (table != null && table.Rows.Count > 0)
            {
                foreach (DataRow row in table.Rows)
                {
                    entities.Add(new Entity()
                    {
                        ID = "0",
                        Status = "",
                        StatusIconHint = "",
                        Uri = row[0].ToString(),
                        FullName = ""
                    });
                }
            }
        }

        return entities;
    }

    [WebMethod(EnableSession = true, MessageName = "GetContainerMemberUris")]
    public List<string> GetContainerMemberUris(string[] memberUris)
    {
        var uris = new List<string>();

        if (memberUris == null || memberUris.Length == 0) return uris;
        var joinedMemberUris = string.Join("','", memberUris);

        string query = String.Format(@"SELECT Uri FROM Orion.ContainerMembers WHERE MemberUri IN('{0}')", joinedMemberUris);

        using (var swis = InformationServiceProxy.CreateV3())
        {
            DataTable table = swis.QueryWithAppendedErrors(query);

            if (table != null && table.Rows.Count > 0)
            {
                foreach (DataRow row in table.Rows)
                {
                    uris.Add(row[0].ToString());
                }
            }
        }

        return uris;
    }

    private int? GetLimitationIdFromViewIdString(string viewId)
    {
        int? viewLimitationId = null;
        if (!string.IsNullOrEmpty(viewId))
        {
            int viewIdInt;
            if (Int32.TryParse(viewId, out viewIdInt))
            {
                try
                {
                    viewLimitationId = ViewManager.GetViewById(viewIdInt).LimitationID;
                }
                catch
                {
                    return null;
                }
            }
        }

        return viewLimitationId;
    }
}
