﻿<%@ WebService Language="C#" Class="ContainersTree" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Linq;
using Resources;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ContainersTree  : WebService
{
    public struct TreeNode
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
        public string Entity { get; set; }
        public bool IsExpandable { get; set; }
        public string ExpandedState { get; set; }
        public string Description { get; set; }
        public string MyContainerID { get; set; }
        public int InstanceSiteId { get; set; }
        public string StatusIconHint { get; set; }
    }

    [WebMethod]
    public void ChangeTree(int resourceId, string path)
    {
        WebUserSettingsDAL.Set("GroupTreeState." + resourceId, path);
    }

    [WebMethod]
    ///<param name="resourceId">ID of resource with tree. This is used to get resource settings.</param>
    public List<TreeNode> GetContainerNodes(int resourceId, int containerID, string status, int fromIndex, int limit, string treeStateString, int instanceSiteId)
    {
        ResourceInfo resource = ResourceManager.GetResourceByID(resourceId);
        Context.Items[typeof(ViewInfo).Name] = resource.View;

        string groupings = resource.Properties["Grouping"] ?? String.Empty;

        DataTable dt = null;
        ContainersTreeDAL dal = new ContainersTreeDAL();

        string orderBy = null;
        if ("Status".Equals(resource.Properties["Ordering"], StringComparison.InvariantCultureIgnoreCase))
        {
            orderBy = "Status";
        }

        string viewMode = resource.Properties["ViewMode"];
        bool onlyRoot = string.IsNullOrEmpty(viewMode) || "Root".Equals(viewMode, StringComparison.InvariantCultureIgnoreCase);

        // load containers
        if (String.IsNullOrEmpty(status) && containerID == 0)
        {
            if (groupings.Equals("status", StringComparison.InvariantCultureIgnoreCase))
            {
                dt = dal.GetContainerStatusNodes(orderBy, onlyRoot);
            }
            else
            {
                dt = dal.GetContainerNodesByStatus(String.Empty, orderBy, onlyRoot);
            }
        }
        else
        {
            if (containerID == 0)
            {
                dt = dal.GetContainerNodesByStatus(status.Split('-')[2], orderBy, onlyRoot);
            }
            else
            {
                dt = dal.GetContainerMembersNodes(containerID, instanceSiteId, orderBy);
            }
        }

        List<TreeNode> nodes = new List<TreeNode>();

        if (dt != null && dt.Rows.Count > 0 )
        {
            Dictionary<string, bool> dict = new Dictionary<string, bool>();

            if (!string.IsNullOrEmpty(treeStateString))
            {
                foreach (var val in treeStateString.Split(';'))
                {
                    if (string.IsNullOrEmpty(val))
                        continue;

                    dict[val] = true;
                }
            }
            nodes = new List<TreeNode>(limit);

            if (limit == 0) limit = Int32.MaxValue;

            bool showGroups = (containerID == 0) || (!"Flat".Equals(viewMode, StringComparison.InvariantCultureIgnoreCase));

            var all = GetOrderData(showGroups, dt);
            foreach (DataRow row in all.Skip(fromIndex).Take(limit))
            {
                nodes.Add(new TreeNode()
                {
                    Id = row["ID"].ToString(),
                    Name = CoreWebContent.ResourceManager.GetString(String.Format("Status_{0}", row["Name"].ToString())) ?? row["Name"].ToString(),
                    Entity = row["Entity"].ToString(),
                    Status = Convert.ToInt32(row["Status"]),
                    IsExpandable = (row["IsExpandable"].ToString() == "1"),
                    ExpandedState = GetExpandedState(dict, resourceId, row["ID"].ToString()),
                    Description = "",
                    MyContainerID = Convert.ToString(row["MyContainerID"]),
                    InstanceSiteId =  Convert.ToInt32(row["InstanceSiteId"]),
                    StatusIconHint = Convert.ToString(row.Table.Columns.Contains("StatusIconHint") ? row["StatusIconHint"]: String.Empty)
                });
            }

            int lastIndex = fromIndex + nodes.Count;
            if (lastIndex < all.Count)
            {
                nodes.Add(new TreeNode()
                {
                    Id = "",
                    Name = String.Format("{0}", all.Count - lastIndex)
                });
            }
        }

        return nodes;
    }

    private string GetExpandedState(IDictionary<string, bool> sessionManager, int resourceId, string id)
    {
        string path = id.Replace(".", string.Empty);
        if (path.IndexOf(string.Format(CultureInfo.InvariantCulture, "{0}-", resourceId), StringComparison.InvariantCultureIgnoreCase) == -1)
        {
            path = string.Format(CultureInfo.InvariantCulture, "{0}-{1}", resourceId, path);
        }

        return sessionManager.ContainsKey(path) ? path : String.Empty;
    }


    private List<DataRow> GetOrderData(bool showGroups, DataTable data)
    {
        var rows = data.Select().ToList();
        var groupRows = rows
            .Where(item => item["MyContainerID"].Equals(0) == false && item["ID"].ToString().StartsWith(ContainersDAL.Entity))
            .ToList();

        var result = rows.Except(groupRows).ToList();

        if (showGroups)
            result.InsertRange(0, groupRows);

        return result;
    }
}
