<%@ WebHandler Language="C#" Class="DetachResourceAsImage" %>

using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Security;
using EO.WebBrowser;
using EO.WebEngine;

using SolarWinds.Logging;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Settings;


public class DetachResourceAsImage : IHttpHandler
{
    private static readonly Log _log = new Log();
    private static string XSRFCookieName = "XSRF-TOKEN";
    private static string SessionCookieName = "ASP.NET_SessionId";

    public void ProcessRequest(HttpContext context)
    {
        if (EnginesDAL.GetCachedFIPSModeOnPrimaryEngine())
        {
            _log.Warn("Rendering as image is disabled in case FIPS is enabled!");
            return;
        }

        var targetUrl = GetResourceUrl(context);
        var engineName = $"Engine_{Guid.NewGuid()}";
        var resourceTimeout = WebSettingsDAL.GetValue("DetachResourceAsImageResourceTimeOut", 30000);
        var windowTimeout = WebSettingsDAL.GetValue("DetachResourceAsImageWindowTimeOut", 2000);
        var ignoreCertCNInvalid = WebSettingsDAL.GetValue("DetachResourceAsImageIgnoreCertCNInvalid", true);
        var ignoreCertErrors = WebSettingsDAL.GetValue("DetachResourceAsImageIgnoreCertErrors", true);

        var engine = Engine.Create(engineName);
        var threadRunner = new ThreadRunner(engineName, engine);
        var webView = threadRunner.CreateWebView();
        Image pageImage = null;

        try
        {
            SetBrowserCookies(webView, context, targetUrl);

            webView.JSInitCode = @"
                        var isPageloading = true;
                        window.onload = function() {
                            window.isPageloading = false;
                        }";

            threadRunner.Send(() =>
            {
                webView.CertificateError += (sender, arg) =>
                {
                    if (ignoreCertErrors ||
                        (ignoreCertCNInvalid && arg.ErrorCode == ErrorCode.CertificateCommonNameInvalid))
                    {
                        // Ignore cert error in case IP address or localhost is used
                        _log.Debug($"Certificate error '{arg.ErrorCode}' ignored for '{arg.Url}'");
                        arg.Continue();
                    }
                    else
                    {
                        _log.Error($"Certificate error '{arg.ErrorCode}' occured for '{arg.Url}'");
                    }
                };

                if (!webView.LoadUrl(targetUrl).WaitOne(resourceTimeout))
                {
                    _log.Error($"Couldn't load '{targetUrl}' in {resourceTimeout} ms ...");
                    return;
                }

                // wait while the internally working of the WebView continues
                WebView.DoEvents(windowTimeout);

                // wait for page load
                EvalLoadingScript(webView, "window.isPageloading;", 100, resourceTimeout);
                // wait for chart V2 is loaded
                EvalLoadingScript(webView, "$('.highcharts-loading').is(':visible');", 100, resourceTimeout);
                // wait for chart V3 is loaded
                EvalLoadingScript(webView, "$('.xui-busy').is(':visible');", 100, resourceTimeout);

                pageImage = webView.Capture(GetPageSize(webView));
            });
        }
        catch (Exception ex)
        {
            _log.Error($"Capturing screen of '{targetUrl}' failed!", ex);
            throw;
        }
        finally
        {
            StopWebObjects(webView, threadRunner, engine);
        }

        if (pageImage != null)
        {
            using (var memoryStream = new MemoryStream())
            {
                pageImage.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Png);
                context.Response.ContentType = "image/png";
                context.Response.BinaryWrite(memoryStream.ToArray());
                context.Response.Flush();
            }
        }
        else
        {
            _log.Error($"Couldn't get an image from '{targetUrl}'!");
            context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
        }
    }

    private string GetResourceUrl(HttpContext context)
    {
        var queryString = HttpUtility.ParseQueryString(context.Request.Url.Query);
        queryString["ResourceOnly"] = "1";

        var builder = new UriBuilder(context.Request.Url)
        {
            Query = queryString.ToString(),
            Path = "/Orion/DetachResourceAsImage.aspx"
        };

        _log.DebugFormat("Target Url: {0}", builder);
        return builder.ToString();
    }

    private static void SetBrowserCookies(WebView webView, HttpContext context, string targetUrl)
    {
        SetBrowserCookie(webView, context, targetUrl, FormsAuthentication.FormsCookieName);
        SetBrowserCookie(webView, context, targetUrl, SessionCookieName);

        if (GeneralSettings.Instance.EnableXsrfProtection)
        {
            SetBrowserCookie(webView, context, targetUrl, XSRFCookieName);
        }
    }

    private static void SetBrowserCookie(WebView webView, HttpContext context, string targetUrl, string cookieName)
    {
        var cookie = context.Request.Cookies[cookieName];
        var cookieValue = cookie != null ? cookie.Value : String.Empty;

        webView.Engine.CookieManager.SetCookie(targetUrl,
            new EO.WebEngine.Cookie(cookieName,
                cookieValue)
            {
                Domain = context.Request.Url.Host
            });
    }

    private static void EvalLoadingScript(WebView webView, string script, int delay, int timeOut)
    {
        var sw = new Stopwatch();
        var loading = true;

        sw.Start();

        while (loading)
        {
            WebView.DoEvents(delay);
            var loadingObj = webView.EvalScript(script);
            if (sw.ElapsedMilliseconds > timeOut || loadingObj == null || !bool.TryParse(loadingObj.ToString(), out loading))
            {
                break;
            }
        }

        sw.Stop();
    }

    private void StopWebObjects(WebView webView, ThreadRunner threadRunner, Engine engine)
    {
        webView?.Close(true);
        webView?.Destroy();
        webView?.Dispose();
        threadRunner?.Stop();
        threadRunner?.Dispose();
        engine?.Stop(true);
    }

    private static Rectangle GetPageSize(WebView webView)
    {
        int height, width;
        var heightObj = webView.EvalScript("$('.ResourceWrapper').height();");
        var widthObj = webView.EvalScript("$('.ResourceWrapper').width();");
        var size = webView.GetPageSize();

        return heightObj != null && widthObj != null && int.TryParse(heightObj.ToString(), out height) &&
                   int.TryParse(widthObj.ToString(), out width)
            ? new Rectangle(0, 0, width, height)
            : new Rectangle(0, 0, size.Width, size.Height);
    }

    public bool IsReusable
    {
        get { return false; }
    }
}