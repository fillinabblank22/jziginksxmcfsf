﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/VIMCloudMonitoring/VimClmView.master" AutoEventWireup="true"
    CodeFile="VolumeDetails.aspx.cs" Inherits="Orion_VIMCloudMonitoring_VolumeDetails" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>

<asp:Content ID="PageTitleContent" ContentPlaceHolderID="VimPageTitle" runat="Server">
    <h1>       
        <%= ViewInfo.IsSubView ? ViewInfo.ViewGroupTitle : ViewInfo.ViewHtmlTitle%> 
        - 
        <%= HttpUtility.HtmlEncode(CloudVolume.CloudInstanceName)%>
        - 
        <%= HttpUtility.HtmlEncode(CloudVolume.Name)%>
        <%= ViewInfo.IsSubView ? ViewInfo.ViewHtmlTitle : string.Empty%>
    </h1>
</asp:Content>


<asp:Content ID="MainContent" ContentPlaceHolderID="VimMainContentPlaceHolder" runat="server">
    <orion:ResourceHostControl ID="CloudVolumeResourceHostControl" runat="server">
        <orion:ResourceContainer runat="server" ID="resourceContainer" />
    </orion:ResourceHostControl>
</asp:Content>
