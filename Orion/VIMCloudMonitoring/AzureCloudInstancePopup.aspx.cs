﻿using SolarWinds.VIM.CloudMonitoring.Web.CloudAccounts;
using SolarWinds.VIM.CloudMonitoring.Web.CloudAccounts.Azure;
using SolarWinds.VIM.CloudMonitoring.Web.CloudInstances;
using SolarWinds.VIM.CloudMonitoring.Web.CloudInstances.Azure;
using SolarWinds.VIM.CloudMonitoring.Web.CloudVolumes;
using System;
using System.Web.UI;
using SolarWinds.VIM.CloudMonitoring.Web.DAL;
using SolarWinds.VIM.CloudMonitoring.Web.Views;

public partial class Orion_VIMCloudMonitoring_AzureCloudInstancePopup : CloudInstancePopup<AzureCloudInstanceNetObject>
{
    private readonly ICloudAccountsDal<AzureCloudAccountViewModel> _azureCloudAccountsDal;
    private AzureCloudAccountViewModel _azureCloudAccount;

    public Orion_VIMCloudMonitoring_AzureCloudInstancePopup():this(new CloudInstanceDal(), new NodesDAL(), new CloudVolumeDal(), new AzureCloudAccountsDal())
    {
            
    }

    public Orion_VIMCloudMonitoring_AzureCloudInstancePopup(ICloudInstanceDal cloudInstanceDal, INodeDal nodeDal,
        ICloudVolumeDal cloudVolumeDal, ICloudAccountsDal<AzureCloudAccountViewModel> azureCloudAccountsDal) :base(cloudInstanceDal, nodeDal, cloudVolumeDal)
    {
        if (azureCloudAccountsDal == null)
            throw new ArgumentNullException(nameof(azureCloudAccountsDal));
        _azureCloudAccountsDal = azureCloudAccountsDal;
    }

    protected override void OnInit(EventArgs e)
    {
        DivWithProgress = div_with_progress;
        base.OnInit(e);
    }

    protected AzureCloudAccountViewModel AzureCloudAccount
    {
        get
        {
            if (CloudInstanceNetObject!=null && CloudInstanceNetObject.CloudInstance != null)
            {
                _azureCloudAccount = _azureCloudAccountsDal.GetCloudAccount(CloudInstanceNetObject.CloudInstance.CloudAccountId);
            }
            return _azureCloudAccount;
        }
    }
}