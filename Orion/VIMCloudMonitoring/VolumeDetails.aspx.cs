﻿using System;
using SolarWinds.VIM.CloudMonitoring.Web.CloudVolumes;
using SolarWinds.VIM.CloudMonitoring.Web.Views;

public partial class Orion_VIMCloudMonitoring_VolumeDetails : VimViewPage, ICloudVolumeProvider
{
    public override string ViewType
    {
        get { return "CloudMonitoring CloudVolume Details"; }
    }

    public override string HelpFragment
    {
        get { return string.Empty; }
    }

    protected string PageTitle { get; set; }

    public CloudVolumeNetObject CloudVolume
    {
        get { return (CloudVolumeNetObject)NetObject; }
    }

    protected override void OnInit(EventArgs e)
    {
        resourceContainer.DataSource = ViewInfo;
        resourceContainer.DataBind();

        base.OnInit(e);
    }
}