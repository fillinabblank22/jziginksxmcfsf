﻿<%@ Page Language="C#" Title="<%$ Resources: VIMWebContent, VIMWEBCODE_CLM_7 %>" AutoEventWireup="true" CodeFile="EditChosenCloudInstances.aspx.cs" Inherits="Orion_VIMCloudMonitoring_Admin_EditChosenCloudInstances" MasterPageFile="~/Orion/VIMCloudMonitoring/Admin/CLMAdminPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:Include runat="server" Module="VIMCloudMonitoring" File="Common/FilterTreeNodeValueFactory.js" Section="Top"/>
    <orion:Include runat="server" Module="VIMCloudMonitoring" File="Common/FilterTreeNodeValueCollectionFactory.js" Section="Top"/>
    <orion:Include runat="server" Module="VIMCloudMonitoring" File="Common/FilterTreeNodeRetriever.js" Section="Top"/>
    <orion:Include runat="server" Module="VIMCloudMonitoring" File="Common/FilterTreeLeafStatusRetriever.js" Section="Top"/>
    <orion:Include runat="server" Module="VIMCloudMonitoring" File="vim.CloudStatusNameProvider.js" Section="Top"/>
    <orion:Include runat="server" Module="VIMCloudMonitoring" File="vim.CloudStatusIconProvider.js" Section="Top"/>
    <orion:Include runat="server" Module="VIMCloudMonitoring" File="vim.AddAccount.ChooseCloudInstances.Renderers.js" Section="Top"/>
    <orion:Include runat="server" Module="VIMCloudMonitoring" File="vim.AddAccount.ChooseCloudInstances.js" Section="Top"/>
    <orion:Include runat="server" Module="VIMCloudMonitoring" File="Common/ErrorPresenter.js" Section="Top"/>
    <orion:Include runat="server" Module="VIMCloudMonitoring" File="Common/QueryStringParser.js" Section="Top"/>
    <orion:Include runat="server" Module="VIMCloudMonitoring" File="Controls/ProgressBarDialogController.js" Section="Top"/>
    <orion:Include runat="server" Module="VIMCloudMonitoring" File="vim.EditChosenCloudInstances.js" Section="Top"/>
    <orion:Include runat="server" Module="CloudMonitoring" File="AddAccount.css"/>
    <orion:Include runat="server" Module="VIMCloudMonitoring" File="ChooseCloudInstances.css"/>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <div class="row mainRow">
        <div class="col-md-10">
            <h4 id="pageTitle"><%= Page.Title %></h4>
        </div>
    </div>

    <div class="row mainRow" id="ChooseCloudInstances">
        <div class="left-section-margin bottom-section-margin right-section-margin chooseCloudInstancesGrid">
            <div id="ChooseInstancesPlaceholder"></div>
        </div>
    </div>

    <div class="row mainRow">
        <div class="col-md-6 col-md-offset-1">
            <div id="editChosenInstancesError" class="alert alert-danger hiddenElement wrapWord" style="margin-top: 20px;"></div>
        </div>
    </div>

    <div class="row mainRow">
        <div class="col-md-5 col-md-offset-7 wizard-buttons">
            <div>
                <input type="button" id="SaveButton" class="sw-btn wizard-button wizard-button-primary" value="Save"/>
                <input type="button" id="CancelButton" class="sw-btn wizard-button" value="Cancel"/>
            </div>
        </div>
    </div>

    <div id="ProgressbarDialog" class="hiddenElement">
        <div class="progress-content">
            <div class="progress">
                <div id="Progressbar" class="progress-bar progress-bar-striped active" role="progressbar"></div>
            </div>
            <div id="ProgressInformation" class="progress-label"></div>
        </div>
    </div>
</asp:Content>