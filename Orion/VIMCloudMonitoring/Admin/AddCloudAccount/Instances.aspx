﻿<%@ Page  MasterPageFile="~/Orion/VIMCloudMonitoring/NoHeader.master" Language="C#" %>

<%@ Register TagPrefix="wizardSteps" TagName="ChooseCloudInstances" Src="~/Orion/VIMCloudMonitoring/Controls/ChooseInstances.ascx" %>

<asp:Content ID="AdminContent" ContentPlaceHolderID="MainContentPlaceHolder" runat="Server" class="container">
    <wizardSteps:ChooseCloudInstances runat="server" />
</asp:Content>
