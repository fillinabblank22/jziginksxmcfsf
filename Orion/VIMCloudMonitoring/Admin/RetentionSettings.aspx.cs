﻿using System;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

namespace Orion.VIM.Admin
{
    public partial class VIMCloudMonitoring_Admin_RetentionSettings : System.Web.UI.Page, IBypassAccessLimitation
    {
        private const string SettingIDDetailedRetention = "CLM_Setting_Detailed_Retain";
        private const string SettingIDHourlyRetention = "CLM_Setting_Hourly_Retain";
        private const string SettingIDDailyRetention = "CLM_Setting_Daily_Retain";

        private const int DefaultDetailedRetention = 7;
        private const int DefaultHourlyRetention = 30;
        private const int DefaultDailyRetention = 365;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                detailedRetention.Text = SettingsDAL.GetCurrentInt(SettingIDDetailedRetention, DefaultDetailedRetention).ToString();
                hourlyRetention.Text = SettingsDAL.GetCurrentInt(SettingIDHourlyRetention, DefaultHourlyRetention).ToString();
                dailyRetention.Text = SettingsDAL.GetCurrentInt(SettingIDDailyRetention, DefaultDailyRetention).ToString();
            }
        }

        protected void Submit_Click(object source, EventArgs e)
        {
            var detailed = (float)Convert.ToDouble(detailedRetention.Text);
            var hourly = (float)Convert.ToDouble(hourlyRetention.Text);
            var daily = (float)Convert.ToDouble(dailyRetention.Text);

            SettingsDAL.Set(SettingIDDetailedRetention, detailed);
            SettingsDAL.Set(SettingIDHourlyRetention, hourly);
            SettingsDAL.Set(SettingIDDailyRetention, daily);

            ReferrerRedirectorBase.Return("/Orion/CloudMonitoring/Admin/CloudMonitoringSettings.aspx");
        }

        protected void Cancel_Click(object source, EventArgs e)
        {
            ReferrerRedirectorBase.Return("/Orion/CloudMonitoring/Admin/CloudMonitoringSettings.aspx");
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (!Profile.AllowAdmin && !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {

                OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_VB0_323), Title = Resources.CoreWebContent.WEBDATA_VB0_567 });

            }
        }

    }
}
