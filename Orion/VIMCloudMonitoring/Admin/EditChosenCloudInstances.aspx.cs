﻿using System;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_VIMCloudMonitoring_Admin_EditChosenCloudInstances : System.Web.UI.Page, IBypassAccessLimitation
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!Profile.AllowAdmin && !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {

            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_VB0_323), Title = Resources.CoreWebContent.WEBDATA_VB0_567 });

        }
    }

}