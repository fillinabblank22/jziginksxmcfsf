﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VolumePopup.aspx.cs" Inherits="Orion_VIMCloudMonitoring_VolumePopup" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>

<h3 data-automation="HeaderValue" class="Status<%= CloudVolume.Status %>">
    <b><%= UIHelper.Escape(CloudVolume.DiskIdentifier) %></b>
</h3>
<div class="NetObjectTipBody">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><%= Resources.VIMWebContent.Web_CloudVolume_Popup_Status %>:</th>
            <td colspan="2" data-automation="StatusValue">
                <%= CloudVolume.Status %>
                <% if (!string.IsNullOrWhiteSpace(CloudVolume.Status))
                   { %>
                    <br/><span><%= CloudVolume.StatusDescription %></span>
                <% } %>
            </td>
        </tr>
        <tr>
            <th><%= Resources.VIMWebContent.Web_CloudVolume_Popup_Type %>:</th>
            <td colspan="2" data-automation="TypeValue">
                <% if (string.IsNullOrEmpty(CloudVolume.Type)) { %>
                    <span>N/A</span>
                <% } else { %>
                <%= CloudVolume.Type %>
                <% } %>
            </td>
        </tr>
        <tr>
            <th><%= Resources.VIMWebContent.Web_CloudVolume_Popup_State %>:</th>
            <td colspan="2" data-automation="StateValue">
                <span class="cv vim-clm-<%= StateStatusCssClass %>"><%= CloudVolume.CloudState %></span>
            </td>
        </tr>
        <tr>
            <th><%= Resources.VIMWebContent.Web_CloudVolume_Popup_Size %>:</th>
            <td colspan="2" data-automation="SizeValue">
                
                <% if (CloudVolume.Size == null || CloudVolume.Size == 0) { %>
                    <span>N/A</span>
                <% } else { %>
                <%= SolarWinds.VIM.CloudMonitoring.Web.Helpers.FormatHelper.FormatCloudVolumeBytes(CloudVolume.Size.Value) %>
                <% } %>
            </td>
        </tr>
        <tr>
            <th><%= Resources.VIMWebContent.Web_CloudVolume_Popup_CloudInstance %>:</th>
            <td colspan="2" data-automation="CloudInstanceName">
                <div style="vertical-align: middle;" data-automation="CloudInstanceIcon" class="instanceName instanceName-icon <%= StatusIconCssClass %>">&nbsp;</div>
                <%= CloudVolume.CloudInstanceName %>
            </td>
        </tr>
    </table>
</div>