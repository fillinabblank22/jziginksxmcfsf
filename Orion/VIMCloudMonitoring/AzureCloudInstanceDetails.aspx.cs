﻿using System;
using SolarWinds.CloudMonitoring.Shared.Enums;
using SolarWinds.VIM.CloudMonitoring.Web.CloudInstances;
using SolarWinds.VIM.CloudMonitoring.Web.CloudInstances.Azure;
using SolarWinds.VIM.CloudMonitoring.Web.Views;

public partial class Orion_VIMCloudMonitoring_AzureCloudInstanceDetails : CloudInstanceViewPage,
    IAzureCloudInstanceProvider, IAzureInstanceDetailPage
{
    public override string ViewType => "CloudMonitoring CloudInstance Details Azure";

    protected override CloudProvider CloudProvider => CloudProvider.Azure;

    public AzureCloudInstanceNetObject CloudInstanceNetObject => (AzureCloudInstanceNetObject) NetObject;

    public Orion_VIMCloudMonitoring_AzureCloudInstanceDetails() : this(new CloudInstanceStatusIconHelper())
    {
    }

    public Orion_VIMCloudMonitoring_AzureCloudInstanceDetails(
        ICloudInstanceStatusIconHelper cloudInstanceStatusIconHelper) : base(cloudInstanceStatusIconHelper)
    {
    }

    protected override void OnInit(EventArgs e)
    {
        resContainer.DataSource = ViewInfo;
        resContainer.DataBind();
        base.OnInit(e);
    }
}