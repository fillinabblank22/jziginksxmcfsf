﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AzureCloudInstancePopup.aspx.cs" Inherits="Orion_VIMCloudMonitoring_AzureCloudInstancePopup" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>

<h3 data-automation="HeaderValue" class="Status<%=CloudInstanceNetObject.CloudInstance.Status%>"><b><%:CloudInstanceNetObject.CloudInstance.Name%></b></h3>
<div class="NetObjectTipBody" style="width: 300px">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><%= Resources.VIMWebContent.Web_CloudInstance_Popup_Status%>:</th>
            <td colspan="2" data-automation="StatusValue"><%= CloudInstanceNetObject.CloudInstance.Status%>
                <% if (!string.IsNullOrWhiteSpace(CloudInstanceNetObject.CloudInstance.StatusDescription))
                    { %>
                        <br /><span><%= CloudInstanceNetObject.CloudInstance.StatusDescription %></span>
                <%  } %>  
            </td>
        </tr>
        <tr>
            <th><%= Resources.VIMWebContent.Web_CloudInstance_Popup_ManagedAsNode%>:</th>
            <td colspan="2" data-automation="ManagedAsNodeValue">
                <%= IsInstanceMonitoredAsNode ? Resources.VIMWebContent.Web_CloudInstance_Popup_Yes : Resources.VIMWebContent.Web_CloudInstance_Popup_No %>

                <% if (IsInstanceMonitoredAsNode)
                    { %>
                        &nbsp;<img src="<%= Node.Status.ToString("smallled", null) %>" />&nbsp;<%= Node.Status.ToString("parentstatus", null) %>
                <% } %>
            </td>
        </tr>

        <% if (IsInstanceMonitoredAsNode)
            { %>
        <tr>
            <th><%= Resources.VIMWebContent.Web_CloudInstance_Popup_PollingIPAddress %>:</th>
            <td colspan="2" data-automation="PollingIPAddressValue"><%= Node.IPAddress%></td>
        </tr>
        <tr>
            <th><%= Resources.VIMWebContent.Web_CloudInstance_Popup_MachineType %>:</th>
            <td colspan="2" data-automation="MachineTypeValue">
                <img src="/NetPerfMon/Images/Vendors/<%= Node.VendorIcon %>" onerror="this.src='/NetPerfMon/images/Vendors/Unknown.gif'; return true;" />
                <%= Node.MachineType %></td>
        </tr>
        <% } %>

        <tr>
            <th><%= Resources.VIMWebContent.Web_CloudInstance_Popup_State%>:</th>
            <td colspan="2" data-automation="StateValue"><%= CloudInstanceNetObject.CloudInstance.State%></td>
        </tr>
        <tr>
            <th>Provider:</th>
            <td colspan="2" data-automation="ProviderValue">
                <img src="/Orion/VIMCloudMonitoring/images/CloudIcons/Azure.png" />
                <%= Resources.VIMWebContent.Web_CloudInstance_Popup_AzureProvider%>
            </td>
        </tr>
        <tr>
            <th><%= Resources.VIMWebContent.Web_CloudInstance_Popup_Account%>:</th>
            <td colspan="2" data-automation="AccountValue" style="word-break: break-all;"><%:CloudInstanceNetObject.CloudInstance.AccountName%></td>
        </tr>
        <tr>
            <th><%= Resources.VIMWebContent.Web_CloudInstance_Popup_SubscriptionName %>:</th>
            <td colspan="2" data-automation="SubscriptionValue"><%= AzureCloudAccount?.SubscriptionName??string.Empty %></td>
        </tr>
        <tr>
            <th><%= Resources.VIMWebContent.Web_CloudInstance_Popup_Location %>:</th>
            <td colspan="2" data-automation="LocationValue"><%= CloudInstanceNetObject.CloudInstance.Location %></td>
        </tr>

        <div runat="server" id="div_with_progress"></div>

        <% if (CloudVolumesWithProblems.Any())
           {%>
        <tr>
            <th class="textVerticalTop"><%= Resources.VIMWebContent.Web_CloudVolume_Popup_VolumesWithProblems %>:</th>
            <td colspan="2" data-automation="VolumesWithProblems" style="word-break: break-all;">
                <% foreach (var cloudVolume in CloudVolumesWithProblems)
                   { %>
                        <div class="spaceBetweenRows clm-icon clm-icon-aws-volume-<%= cloudVolume.Status.ToLower() %>">&nbsp;</div>
                        <%= cloudVolume.DiskIdentifier %><br />
                <% } %>
            </td>
        </tr>
        <%} %>
    </table>
</div>
