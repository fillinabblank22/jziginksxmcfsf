using System;
using System.Web.UI;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.CloudMonitoring.Web.Views;

public partial class Orion_VIMCloudMonitoring_VimClmView : MasterPage
{
    private bool _allowExportPDF = true;
    private bool _allowViewTimestamp = true;

    public bool RefresherEnabled
    {
        get { return PageRefresher.Visible; }
        set { PageRefresher.Visible = value; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        AllowViewTimestamp = !OrionMinReqsMaster.IsMobileView;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(HelpFragment))
        {
            btnHelp.HelpUrlFragment = HelpFragment;
            btnHelp.Visible = true;
        }

        A1.Visible = !OrionMinReqsMaster.IsMobileView && Profile.AllowCustomize && !String.IsNullOrEmpty(CustomizeViewHref);
        viewTimestamp.Visible = AllowViewTimestamp;
    }

    public bool AllowViewTimestamp
    {
        get { return _allowViewTimestamp; }
        set { _allowViewTimestamp = value; }
    }

    public string CustomizeViewHref
    {
        get
        {
            if (!(Page is VimViewPage))
                return String.Empty;

            return ((VimViewPage)Page).CustomizeViewHref;
        }
    }

    public string HelpFragment
    {
        get
        {
            if (!(Page is VimViewPage))
                return String.Empty;

            return ((VimViewPage)Page).HelpFragment;
        }
    }
}
