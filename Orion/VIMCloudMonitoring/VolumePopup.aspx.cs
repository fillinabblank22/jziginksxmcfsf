﻿using SolarWinds.Orion.Web;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.CloudMonitoring.Web.CloudInstances;
using SolarWinds.VIM.CloudMonitoring.Web.CloudVolumes;
using SolarWinds.VIM.CloudMonitoring.Web.Helpers;
using System;
using System.Web;

public partial class Orion_VIMCloudMonitoring_VolumePopup : System.Web.UI.Page
{
    private readonly ICloudInstanceDal<CloudInstance> _cloudInstanceDal;
    private readonly ICloudInstanceStatusIconHelper _cloudInstanceStatusIconHelper;

    protected CloudVolume CloudVolume;
    protected VimManagedStatus CloudInstanceStatus;

    public Orion_VIMCloudMonitoring_VolumePopup() : this(new CloudInstanceDal(), new CloudInstanceStatusIconHelper())
    {
    }

    private Orion_VIMCloudMonitoring_VolumePopup(ICloudInstanceDal<CloudInstance> cloudInstanceDal,
        ICloudInstanceStatusIconHelper cloudInstanceStatusIconHelper)
    {
        if (cloudInstanceDal == null)
            throw new ArgumentNullException(nameof(cloudInstanceDal));
        if (cloudInstanceStatusIconHelper == null)
            throw new ArgumentNullException(nameof(cloudInstanceStatusIconHelper));
        _cloudInstanceDal = cloudInstanceDal;
        _cloudInstanceStatusIconHelper = cloudInstanceStatusIconHelper;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var cloudVolumeNetObject = NetObjectFactory.Create(Request["NetObject"]) as CloudVolumeNetObject;
        if (cloudVolumeNetObject?.CloudVolume != null)
        {
            CloudVolume = cloudVolumeNetObject.CloudVolume;
            CloudInstanceStatus = _cloudInstanceDal.GetCloudInstance(CloudVolume.VirtualMachineId).Status;
        }
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
    }

    protected string StateStatusCssClass => CssHelper.GetVimManagedStatusCssClass(CloudVolume
        .StateStatus);

    protected string StatusIconCssClass => _cloudInstanceStatusIconHelper.GetIconCssClassElementForStatus(
        CloudInstanceStatus, CloudVolume.CloudProvider);
}