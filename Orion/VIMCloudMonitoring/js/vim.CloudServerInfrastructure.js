SW.Core.namespace("SW.VIM.CloudServerInfrastructure").Resource = function () {

    function createSingleSection(cssClass, count) {
        if (count > 0) {
            return "<div class='" + cssClass + "'></div><span>" + count + "</span>";
        }
        return "";
    }

    function createSummarySectionHtml(critical, warning, unknown) {
        var summary = "<div class='cloudServerInfrastructureSummarySection'>";

        summary += createSingleSection("critical", critical);
        summary += createSingleSection("warning", warning);
        summary += createSingleSection("unknown", unknown);
        summary += "</div>";

        return summary;
    }

    function nodeHasSummarySection(node) {
        return node.children("div").length > 0;
    }

    function appendSummarySection(node) {
        if (nodeHasSummarySection(node)) return;

        var critical = node.attr("data-critical") ? node.attr("data-critical") : 0;
        var warning = node.attr("data-warning") ? node.attr("data-warning") : 0;
        var unknown = node.attr("data-unknown") ? node.attr("data-unknown") : 0;

        node.children("a").after(createSummarySectionHtml(critical, warning, unknown));
    }

    function appendStatusToNodes(nodes, containerId) {
        if (!nodes) { return; }

        for (var i = 0; i < nodes.length; i++) {
            var node = $.jstree._reference(containerId)._get_node(nodes[i]);
            var isLeaf = $.jstree._reference(containerId).is_leaf(node);

            if (isLeaf) { continue; }

            appendSummarySection(node);

            var children = $.jstree._reference(containerId)._get_children(node);
            appendStatusToNodes(children, containerId);
        }
    }

    function appendStatusToRoot(containerId) {
        var roots = $.jstree._reference(containerId)._get_children(-1);
        appendStatusToNodes(roots, containerId);
    }

    function appendStatusToChildren(node, containerId) {
        var children = $.jstree._reference(containerId)._get_children(node.rslt.obj);
        appendStatusToNodes(children, containerId);
    }

    function hideNodeSummarySection(node) {
        node.rslt.obj.children("div").hide();
    }

    function showNodeSummarySection(node) {
        node.rslt.obj.children("div").show();
    }

    function openNode(node, containerId) {
        appendStatusToChildren(node, containerId);
        hideNodeSummarySection(node);
    }

    function treeNodeClick(event) {
        if (event.target.className === "jstree-icon") {
            return;
        }

        var action = "open_node";

        if ($(event.target).closest("li").hasClass("jstree-open")) {
            action = "close_node";
        }

        $(this).jstree(action, event.target);
    };

    function getNodeFilter(containerId, node, result) {
        if (node === -1) { return; }

        var nodePath = $.jstree._reference(containerId).get_path(node);
        var nodeLevel = nodePath.length;
        var parent = $.jstree._reference(containerId)._get_parent(node);

        result["filterId" + nodeLevel] = node.attr("data-filter-id");

        getNodeFilter(containerId, parent, result);
    }

    function requestedNodeFilters(containerId, node) {
        var result = { "filterId1": "", "filterId2": "", "filterId3": "" };
        getNodeFilter(containerId, node, result);
        return result;
    }

    function showResource(resourceId, containerId) {
        var treeHasAnyNodes = $.jstree._reference(containerId)._get_children(-1);

        if (treeHasAnyNodes) {
            $("div[resourceid=\"" + resourceId + "\"]").show();
        } else {
            $("div[resourceid=\"" + this.resourceId + "\"]").hide();
        }
    }

    function createTreee(containerId, groupingLevel1, groupingLevel2, groupingLevel3, resourceId, cookieId, viewLimitationId) {
        var serviceUrl = "/api/CloudInstance/GetTreeNodes?groupingLevel1=" + groupingLevel1 + "&groupingLevel2=" + groupingLevel2 + "&groupingLevel3=" + groupingLevel3;
        if (viewLimitationId != null) {
            serviceUrl += "&viewLimitationId=" + viewLimitationId;
        }
        $(containerId).jstree({
            "json_data": {
                "ajax": {
                    "url": function (node) {
                        if (node === -1) {
                            return serviceUrl;
                        }
                        return serviceUrl + "&provider=" + node.attr("data-cloud-provider");

                    },
                    "data": function (node) {
                        return requestedNodeFilters(containerId, node);
                    }
                }
            },
            "themes": {
                "url": "/Orion/VIM/js/jsTree/themes/customizable/style.css",
                "theme": "default",
                "icons": true,
                "dots": true
            },
            "core": {
                "html_titles": true
            },
            "cookies": {
                "save_opened": cookieId,
                "save_selected": false
            },
            "plugins": ["themes", "json_data", "cookies"]
        })
        .bind("loaded.jstree", function () { showResource(resourceId, containerId); appendStatusToRoot(containerId); })
        .bind("open_node.jstree", function (event, node) { openNode(node, containerId); })
        .bind("close_node.jstree", function (event, node) { showNodeSummarySection(node); })
        .bind("click.jstree", treeNodeClick);
    }

    function Resource(resourceId, containerId, groupingLevel1, groupingLevel2, groupingLevel3, cookieId, viewLimitationId) {
        this.resourceId = resourceId;
        this.containerId = containerId;
        this.groupingLevel1 = groupingLevel1;
        this.groupingLevel2 = groupingLevel2;
        this.groupingLevel3 = groupingLevel3;
        this.cookieId = encodeURIComponent(cookieId);
        this.viewLimitationId = viewLimitationId;
    }

    Resource.prototype = {
        createResource: function () {
            createTreee(this.containerId, encodeURIComponent(this.groupingLevel1), encodeURIComponent(this.groupingLevel2),
                encodeURIComponent(this.groupingLevel3), this.resourceId, this.cookieId, this.viewLimitationId);
        },
        refresh: function () {
            var self = this;
            self.createResource();
        }
    }

    return Resource;
}();