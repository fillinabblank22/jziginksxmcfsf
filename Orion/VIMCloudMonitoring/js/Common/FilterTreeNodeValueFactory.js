﻿/*jslint browser: true*/
/*global SW: false, $: false*/
(function () {
    "use strict";

    var vimCommon = SW.Core.namespace("SW.VIM.Common");

    vimCommon.FilterTreeNodeValueFactory = function () {
        this.create = function (text, name, statistics, checked) {
            if (!text) {
                throw "Text cannot be null.";
            }
            if (!name) {
                throw "Name cannot be null.";
            }
            if (!statistics) {
                throw "Statistics cannot be null.";
            }
            if (isNaN(statistics)) {
                throw "Statistics has to be a number.";
            }
            if (typeof checked !== "boolean") {
                throw "Checked has to be a boolean.";
            }

            return {
                text: text.toString(),
                data: {
                    name: name.toString(),
                    statistics: statistics
                },
                leaf: true,
                checked: checked,
                iconCls: "noIconNode"
            };
        };
    };
}());
