﻿/*jslint browser: true*/
/*global SW: false, $: false*/
(function () {
    "use strict";

    var vimCommon = SW.Core.namespace("SW.VIM.Common");

    vimCommon.FilterTreeNodeRetriever = function () {
        this.retrieve = function (filterNodes, internalId) {
            if (!filterNodes) {
                throw "Filter nodes cannot be null.";
            }

            if (filterNodes.constructor !== Array) {
                throw "Filter nodes has to be an array.";
            }

            return filterNodes.filter(function (node) {
                return node.internalId === internalId;
            })[0];
        };
    };
}());
