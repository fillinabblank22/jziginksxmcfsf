﻿/*jslint browser: true*/
/*global SW: false, $: false*/
(function () {
    "use strict";

    var vimCommon = SW.Core.namespace("SW.VIM.Common");

    vimCommon.ErrorPresenter = function (htmlPlaceholderElementId) {
        this.show = function (message) {
            $(htmlPlaceholderElementId).html(message);
            $(htmlPlaceholderElementId).show();
        };
        this.hide = function () {
            $(htmlPlaceholderElementId).hide();
            $(htmlPlaceholderElementId).html("");
        };
    };
}());
