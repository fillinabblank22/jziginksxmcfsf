﻿/* eslint-env browser */
/* eslint no-undef: "error" */
/* global SW */
(function () {
    "use strict";

    var vimCommon = SW.Core.namespace("SW.VIM.Common");

    function notSet(value) {
        return value === undefined || value === null;
    }

    vimCommon.FilterTreeLeafStatusRetriever = function () {
        this.retrieve = function (nodeValues, value) {
            if (!nodeValues) {
                throw "Node values cannot be null.";
            }
            if (nodeValues.constructor !== Array) {
                throw "Node values has to be an array.";
            }
            if (notSet(value)) {
                throw "Value cannot be null or undefined.";
            }

            var text = value.toString();

            var matchingNodes = nodeValues.filter(function (nodeValue) {
                return nodeValue.data.text === text;
            });

            if (notSet(matchingNodes) || matchingNodes.count === 0) {
                return false;
            }

            var matchingNode = matchingNodes[0];

            if (notSet(matchingNode) || notSet(matchingNode.data)) {
                return false;
            }

            var isNodeChecked = matchingNode.data.checked;
            return isNodeChecked;
        };
    };
}());
