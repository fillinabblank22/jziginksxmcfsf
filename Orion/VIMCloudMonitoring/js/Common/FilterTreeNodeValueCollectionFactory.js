﻿/*jslint browser: true, vars: true*/
/*global SW: false, $: false*/
(function () {
    "use strict";

    var vimCommon = SW.Core.namespace("SW.VIM.Common");

    function isObject(value) {
        return value && value.constructor === Object;
    }

    vimCommon.FilterTreeNodeValueCollectionFactory = function () {
        var filterTreeNodeValueFactory = new vimCommon.FilterTreeNodeValueFactory();

        this.create = function (nodeValues) {
            if (!nodeValues) {
                throw "Node values cannot be null.";
            }

            if (!isObject(nodeValues)) {
                throw "Node values has to be an object.";
            }

            var treeModel = [];
            $.each(nodeValues, function (optionText, optionData) {
                var nodeValue = filterTreeNodeValueFactory.create(
                    optionText,
                    optionText,
                    optionData.count,
                    optionData.checked || false
                );
                treeModel = treeModel.concat(nodeValue);
            });
            return treeModel;
        };

        this.createWithMappedNames = function (nodeValues, nameMap) {
            if (!nodeValues) {
                throw "Node values cannot be null.";
            }

            if (!isObject(nodeValues)) {
                throw "Node values has to be an object.";
            }

            if (!nameMap) {
                throw "Name map cannot be null.";
            }

            if (nameMap.constructor !== Object) {
                throw "Name map has to be an object.";
            }

            var treeModel = [];
            $.each(nodeValues, function (optionText, optionData) {
                var optionName = nameMap[optionText] || optionText;
                var nodeValue = filterTreeNodeValueFactory.create(
                    optionText,
                    optionName,
                    optionData.count,
                    optionData.checked || false
                );
                treeModel = treeModel.concat(nodeValue);
            });
            return treeModel;
        };
    };
}());
