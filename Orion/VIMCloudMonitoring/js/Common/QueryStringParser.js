﻿/*jslint browser: true, vars: true*/
/*global SW: false, $: false*/
(function () {
    "use strict";

    var vimCommon = SW.Core.namespace("SW.VIM.Common");

    // Parses query strings in following format: "?key1=value1&key2=value2[..]"
    vimCommon.QueryStringParser = function () {

        this.parseValue = function (queryString, key) {
            if (!key || !queryString) {
                return null;
            }

            var queryStringClean = queryString.trim();
            if (queryStringClean[0] === "?") {
                queryStringClean = queryStringClean.substring(1);
            }

            if (!queryStringClean) {
                return null;
            }

            var result = null;

            queryStringClean
                .split("&")
                .some(function (parameter) {
                    var keyValuePair = parameter.split("=");
                    if (keyValuePair[0] === key) {
                        result = decodeURIComponent(keyValuePair[1]);
                        return true;
                    }
                });

            return result;
        };
    };
}());
