﻿/// <reference path="vim.CloudStatusCellCssClassProvider.js" />
/// <reference path="vim.CloudStatusNameProvider.js" />
/// <reference path="Controls/ManageNodeDialog.js" />
/* global SW */
(function (cloudVolumePerformance) {

    function areEmptyStatePreconditionsMet(searchBoxClientSelector, rows) {
        var resourceStateChecker = new SW.VIM.Resources.Shared.ResourceStateChecker(searchBoxClientSelector);
        return rows.length === 0
            && !resourceStateChecker.isFilterApplied(searchBoxClientSelector)
            && !resourceStateChecker.isSearchApplied(searchBoxClientSelector);
    }

    function onLoadCustomQueryTable(searchBoxClientSelector, rows) {
        if (areEmptyStatePreconditionsMet(searchBoxClientSelector, rows)) {
            var resourceContentElement = $(searchBoxClientSelector).parents(".ResourceContent");
            var resourceEmptyStatePresenter = new SW.VIM.Resources.Shared.ResourceEmptyStatePresenter(resourceContentElement);
            resourceEmptyStatePresenter.show();
        }
    }

    cloudVolumePerformance.InitializeCustomQueryTable = function (resourceId, bodyId, rowsPerPage, searchBoxClientId, searchButtonClientId) {

        var searchBoxClientSelector = "#" + searchBoxClientId;
        var CLM = SW.VIM.CloudMonitoring;


        SW.Core.Resources.CustomQuery.initialize({
            uniqueId: resourceId,
            searchTextBoxId: searchBoxClientId,
            searchButtonId: searchButtonClientId,
            initialPage: 0,
            allowSort: true,
            allowPaging: true,
            rowsPerPage: rowsPerPage,
            columnSettings: {
                "Disk": {
                    header: "@{R=VIM.Strings.CloudMonitoring;K=CLM_CloudVolume_Disk;E=js}",
                    formatter: function (value, row) {
                        var status = row[2];
                        var statusIconClass = CLM.CloudStatusIconProvider.getVolumeStatusIconClass(status);
                        return String.format("<span><div class=\"volumeName volumeName-icon clm-icon {0}\">&nbsp;</div><div class=\"volumeName volumeName-text\">{1}</div></span>", statusIconClass, value);
                    },
                    isHtml: true
                },
                "ReadLatencyInMilliseconds": {
                    header: "@{R=VIM.Strings.CloudMonitoring;K=CLM_CloudVolume_ReadLatency;E=js}",
                    isHtml: true,
                    formatter: function (value, row) {
                        return cloudVolumePerformance.dataUnavailableBaseFormatter(value, cloudVolumePerformance.millisecondsFormatter, row[10] === cloudVolumePerformance.ProviderIds.Azure, row[11]);
                    }
                },
                "WriteLatencyInMilliseconds": {
                    header: "@{R=VIM.Strings.CloudMonitoring;K=CLM_CloudVolume_WriteLatency;E=js}",
                    isHtml: true,
                    formatter: function (value, row) {
                        return cloudVolumePerformance.dataUnavailableBaseFormatter(value, cloudVolumePerformance.millisecondsFormatter, row[10] === cloudVolumePerformance.ProviderIds.Azure, row[11]);
                    }
                },
                "ReadOperationsPerSecond": {
                    header: "@{R=VIM.Strings.CloudMonitoring;K=CLM_CloudVolume_ReadIOPS;E=js}",
                    isHtml: true,
                    formatter: function (value, row) {
                        return cloudVolumePerformance.dataUnavailableBaseFormatter(value, cloudVolumePerformance.twoDecimalPlacesFormatter, row[10] === cloudVolumePerformance.ProviderIds.Azure, row[11]);
                    }
                },
                "WriteOperationsPerSecond": {
                    header: "@{R=VIM.Strings.CloudMonitoring;K=CLM_CloudVolume_WriteIOPS;E=js}",
                    isHtml: true,
                    formatter: function (value, row) {
                        return cloudVolumePerformance.dataUnavailableBaseFormatter(value, cloudVolumePerformance.twoDecimalPlacesFormatter, row[10] === cloudVolumePerformance.ProviderIds.Azure, row[11]);
                    }
                },
                "QueuedOperations": {
                    header: "@{R=VIM.Strings.CloudMonitoring;K=CLM_CloudVolume_QueuedIO;E=js}",
                    isHtml: true,
                    formatter: function (value, row) {
                        return cloudVolumePerformance.dataUnavailableBaseFormatter(value, cloudVolumePerformance.twoDecimalPlacesFormatter, row[10] === cloudVolumePerformance.ProviderIds.Azure, row[11]);
                    }
                },
                "Size": {
                    header: "@{R=VIM.Strings.CloudMonitoring;K=CLM_CloudVolume_Size;E=js}",
                    isHtml: true,
                    formatter: function (value) {
                        return cloudVolumePerformance.dataUnavailableBaseFormatter(value, cloudVolumePerformance.gigabytesFormatter, false);
                    }
                },
                "Type": {
                    header: "@{R=VIM.Strings.CloudMonitoring;K=CLM_CloudVolume_Type;E=js}",
                    isHtml: true,
                    formatter: function (value) {
                        return cloudVolumePerformance.stringBaseFormatter(value, cloudVolumePerformance.stringFormatter, false);
                    }
                },
                "Managed": {
                    header: "@{R=VIM.Strings.CloudMonitoring;K=CLM_CloudVolume_Managed;E=js}",
                    isHtml: true,
                    formatter: function (value) {
                        return cloudVolumePerformance.stringBaseFormatter(value, cloudVolumePerformance.stringFormatter, false);
                    }
                }
            },
            onLoad: function (rows) {
                onLoadCustomQueryTable(searchBoxClientSelector, rows);
                $(".HintForVolumes").setHint({
                    leftOffset: -8
                });
            }
        });

        // bind filter dropdown to custom query resource
        SW.Core.Resources.CustomQuery.FilterDropdown.initialize(
            resourceId,
            {
                setResultQueryCallback: SW.Core.Resources.CustomQuery.setQuery,
                setResultSearchQueryCallback: SW.Core.Resources.CustomQuery.setSearchQuery,
                filterFinishedCallback: SW.Core.Resources.CustomQuery.refresh
            },
            $(searchBoxClientSelector).parents(".searchControl").siblings(".filterControl").children("select").val()
        );

        function refreshResource() {
            SW.Core.Resources.CustomQuery.refresh(resourceId);
        }

        function manageInstanceAsNode() {
            try {
                var element = $("#cluetip").data("currentHoverElement");
                var instanceId = $(element).attr("data-instanceid");
                // TODO: enclose in namespace
                ManageInstanceAsNodeDialog(instanceId);
            } catch (ex) {
                window.console.log("Could not find InstanceId");
            }
        }

        SW.Core.View.AddOnRefresh(refreshResource, bodyId);

        $(".hidden[id*='Pager'").removeClass("hidden");
        $(searchBoxClientSelector).attr("placeholder", "@{R=VIM.Strings.CloudMonitoring;K=CLM_CloudVolume_SearchVolumes;E=js}");

        $(".manageAsNode")
            .unbind("click")
            .click(function () {
                manageInstanceAsNode();
                return false;
            });

    };

    function formatSpace(text) {
        return (text || "").replace(" ", "&nbsp;");
    }

    function getDataUnavailableContent(suggestAgentInstall, instanceId) {
        var $span = $("<span>")
            .text("@{R=VIM.Strings.CloudMonitoring;K=CLM_CloudVolume_DataNotAvailable;E=js}")
            .addClass("CloudNotAvailable");

        if (suggestAgentInstall) {
            $span.attr({
                rel: "#InstallAgentHintForVolumes",
                title: "@{R=VIM.Strings.CloudMonitoring;K=CLM_CloudVolume_DataNA_ForAzureHover_Title;E=js}",
                "data-instanceid": instanceId,
            })
            .addClass("HintForVolumes");
        }

        return $span.prop("outerHTML");
    }

    cloudVolumePerformance.millisecondsFormatter = function (value) {
        return formatSpace(cloudVolumePerformance.twoDecimalPlacesFormatter(value) + " ms");
    };

    cloudVolumePerformance.gigabytesFormatter = function (value) {
        return formatSpace(SW.Core.Charts.dataFormatters.byte(value, { customPrecision: 2 }, 0, null, 3));
    };

    cloudVolumePerformance.stringFormatter = function (value) {
        return formatSpace(value);
    };

    cloudVolumePerformance.twoDecimalPlacesFormatter = function (value) {
        return (+value).toFixed(2);
    };

    cloudVolumePerformance.stringBaseFormatter = function (value, formatter, suggestAgentInstall, instanceId) {

        if (value) {
            return formatter(value);
        }

        return getDataUnavailableContent(suggestAgentInstall, instanceId);
    };

    cloudVolumePerformance.dataUnavailableBaseFormatter = function (value, formatter, suggestAgentInstall, instanceId) {

        if (value.length !== 0 && value >= 0) {
            return formatter(value);
        }

        return getDataUnavailableContent(suggestAgentInstall, instanceId);
    };

    cloudVolumePerformance.ProviderIds = {
        Aws: 1,
        Azure: 2
    }

}(SW.Core.namespace("SW.VIM.CloudMonitoring.CloudVolumePerformance")));
