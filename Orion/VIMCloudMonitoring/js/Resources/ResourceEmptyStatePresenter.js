﻿
(function () {
    var shared = SW.Core.namespace("SW.VIM.Resources.Shared");

    var helpUrl = 'http://www.solarwinds.com/documentation/kbLoader.aspx?kb=4114&lang=@{R=Core.Strings;K=CurrentHelpLanguage;E=js}';

    function createEmptyStateElement() {
        var dataNotAvailableArea = $("<div></div>").addClass("dataNotAvailableArea");
        var dataNotAvailableTextArea = $("<div></div>").addClass("dataNotAvailableTextArea");

        dataNotAvailableArea.append(dataNotAvailableTextArea);

        var textAreaContainer = $("<div></div>").addClass("textAreaContainer");
        var textElement = $("<div></div>").text("@{R=VIM.Strings.CloudMonitoring;K=ResourceEmptyState_DataNotAvailable;E=js}");
        var helpLinkElement = "<a href=\"" + helpUrl + "\" target=\"_blank\">@{R=VIM.Strings.CloudMonitoring;K=ResourceEmptyState_WhyNot;E=js}</a>";

        textAreaContainer.append(textElement, helpLinkElement);
        dataNotAvailableTextArea.append(textAreaContainer);

        return dataNotAvailableArea;
    }

    shared.ResourceEmptyStatePresenter = function (placeholderElement) {
        this.show = function () {

            var emptyStateElement = createEmptyStateElement();

            $(placeholderElement)
                .empty()
                .append(emptyStateElement);
        };
    };
}())
