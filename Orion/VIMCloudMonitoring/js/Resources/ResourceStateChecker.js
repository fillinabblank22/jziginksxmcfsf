﻿
(function () {
    var shared = SW.Core.namespace("SW.VIM.Resources.Shared");

    shared.ResourceStateChecker = function (searchBoxClientSelector) {

        this.isFilterApplied = function () {
            var filterValue = $(searchBoxClientSelector)
                .parents(".searchControl")
                .siblings(".filterControl")
                .children("select")
                .val();

            return filterValue !== "";
        }

        this.isSearchApplied = function () {
            var searchString = $(searchBoxClientSelector).val();

            return searchString !== "";
        }
    };
}())
