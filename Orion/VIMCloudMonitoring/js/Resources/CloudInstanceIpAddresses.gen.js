/// <reference path="../../../typescripts/typings/jquery.d.ts"/>
/// <reference path="../../../typescripts/typings/OrionMinRegs.d.ts"/>
/// <reference path="../../../typescripts/typings/OrionCoreResources.d.ts"/>
/// <reference path="../../../typescripts/typings/OrionCoreView.d.ts"/>
var SW;
(function (SW) {
    var VIM;
    (function (VIM) {
        var Resources;
        (function (Resources) {
            var CloudInstanceIpAddresses;
            (function (CloudInstanceIpAddresses) {
                var resources = {
                    IpAddress: {
                        title: '@{R=VIM.CloudMonitoring.Strings;K=VIM_WEB_CloudInstanceIpAddresses_IpAddress;E=js}'
                    },
                    IpType: {
                        title: '@{R=VIM.CloudMonitoring.Strings;K=VIM_WEB_CloudInstanceIpAddresses_Type;E=js}',
                        values: [
                            '',
                            '@{R=VIM.CloudMonitoring.Strings;K=VIM_WEB_CloudInstanceIpAddressType_Public;E=js}',
                            '@{R=VIM.CloudMonitoring.Strings;K=VIM_WEB_CloudInstanceIpAddressType_PrivatePrimary;E=js}',
                            '@{R=VIM.CloudMonitoring.Strings;K=VIM_WEB_CloudInstanceIpAddressType_PrivateSecondary;E=js}',
                            '@{R=VIM.CloudMonitoring.Strings;K=VIM_WEB_CloudInstanceIpAddressType_Elastic;E=js}'
                        ]
                    },
                    IpVersion: {
                        title: '@{R=VIM.CloudMonitoring.Strings;K=VIM_WEB_CloudInstanceIpAddresses_IpVersion}',
                        values: [
                            '',
                            '@{R=VIM.Strings;K=VIM_WEB_IPv4;E=js}',
                            '@{R=VIM.Strings;K=VIM_WEB_IPv6;E=js}'
                        ]
                    }
                };
                var colIndexIpType = 2;
                var enumIpType = {
                    'undefined': 0,
                    'public': 1,
                    'private': 2,
                    'secondaryPrivate': 3,
                    'elastic': 4
                };
                function initialize(opts) {
                    // resource values - display values for enum fields
                    // This reloads data for this resource (see the same ID as we used for uniqueId in initialize method)
                    function refresh() {
                        SW.Core.Resources.CustomQuery.refresh(opts.resourceId);
                    }
                    // CustomQueryTable does not allow for row styling, style all cells according to specific cell value
                    function classProvider(value, row) {
                        var className = '';
                        switch (Number(row[colIndexIpType])) {
                            case enumIpType.public:
                            case enumIpType.elastic:
                                className = 'hilite';
                                break;
                        }
                        return className;
                    }
                    function formatIpVersion(value) {
                        return resources.IpVersion.values[Number(value)];
                    }
                    function formatIpType(value) {
                        return resources.IpType.values[Number(value)];
                    }
                    // initialize custom query
                    SW.Core.Resources.CustomQuery.initialize({
                        uniqueId: opts.resourceId,
                        rowsPerPage: opts.rowsPerPage,
                        columnSettings: {
                            'Version': {
                                allowSort: false,
                                cellCssClassProvider: classProvider,
                                header: resources.IpVersion.title,
                                formatter: formatIpVersion
                            },
                            'IPAddress': {
                                allowSort: false,
                                cellCssClassProvider: classProvider,
                                header: resources.IpAddress.title
                            },
                            'Type': {
                                allowSort: false,
                                cellCssClassProvider: classProvider,
                                header: resources.IpType.title,
                                formatter: formatIpType
                            }
                        }
                    });
                    // This registers resource in view refresh routine
                    SW.Core.View.AddOnRefresh(refresh, opts.tableId);
                    // And finally loads initial data
                    refresh();
                }
                CloudInstanceIpAddresses.initialize = initialize;
            })(CloudInstanceIpAddresses = Resources.CloudInstanceIpAddresses || (Resources.CloudInstanceIpAddresses = {}));
        })(Resources = VIM.Resources || (VIM.Resources = {}));
    })(VIM = SW.VIM || (SW.VIM = {}));
})(SW || (SW = {}));
//# sourceMappingURL=CloudInstanceIpAddresses.js.map