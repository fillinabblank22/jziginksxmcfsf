﻿(function ($, utils) {
    /*
    Helper method for initializing resources. It prevents resource from being initialized before it is rendered.
    Such a situation could happen when resource is addded via apollo widget drawer.
    */
    utils.initResource = utils.initResource || function(resourceInitializer) {
        if (!resourceInitializer || typeof resourceInitializer !== "function") {
            throw new Error("Resource initializer action is not defined.");
        }
        
        var widgetDrawer = $(".sw-widget-drawer__load-placeholder"), 
            renderByWidgetDrawer = widgetDrawer.length > 0;

        // if widget is rendered on server side than just invoke initialize method
        if (!renderByWidgetDrawer) {
            resourceInitializer();
            return;
        }

        // This code happens if resource will be rendered via apollo widget drawer
        // Rendering or resource is followed by element sw-widget-drawer__load-placeholder disappears
        widgetDrawer.on("remove",
            function() {
                $(document).on("DOMNodeInserted", ".ResourceWrapper",
                    function (event) {

                        // wait for render finshed and initialize
                        setTimeout(resourceInitializer, 500);

                        //unsubscribe from events;
                        $(document).off("DOMNodeInserted");
                        widgetDrawer.off("remove");
                    });
            });        
    };

}(jQuery, SW.Core.namespace("SW.VIM.CloudMonitoring.Utils")));
