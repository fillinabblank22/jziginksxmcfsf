﻿/* global SW */
(function (CloudStatusIconProvider) {

    function getStatusSuffix(statusName) {
        return statusName
            ? "-" + statusName
            : "";
    }

    function getStatusIconClass(managedStatus, classNameInfix) {
        var statusName = SW.VIM.CloudMonitoring.CloudStatusNameProvider.getStatusName(managedStatus);
        return "clm-icon-aws-" + classNameInfix + getStatusSuffix(statusName);
    }

    CloudStatusIconProvider.getInstanceStatusIconClass = function (monitored, managedStatus) {
        if (!monitored) {
            return "clm-icon-aws-instance-not-monitored";
        }
        return getStatusIconClass(managedStatus, "instance");
    };

    CloudStatusIconProvider.getVolumeStatusIconClass = function (managedStatus) {
        return getStatusIconClass(managedStatus, "volume");
    };

}(SW.Core.namespace("SW.VIM.CloudMonitoring.CloudStatusIconProvider")));
