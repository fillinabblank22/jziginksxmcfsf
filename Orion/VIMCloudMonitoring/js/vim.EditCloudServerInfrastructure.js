﻿var editCloudServerInfrastructure = SW.Core.namespace("SW.VIM.EditCloudServerInfrastructure");

(function (ns) {
    var editErrorAllert = undefined;

    ns.getErrorElement = function () {
        editErrorAllert = editErrorAllert || $("#EditErrorAlert");
        return editErrorAllert;
    };

    ns.showErrorAlert = function (alertMessage) {
        ns.getErrorElement()
            .css("visibility", "visible")
            .html(alertMessage);
    };
    ns.hideErrorAlert = function () {
        
        ns.getErrorElement()
            .css("visibility", "hidden");
    };

    ns.validateTitle = function() {
        var title = $("[name*='txtTitle']")[0].value;

        if (title.length === 0) {
            ns.showErrorAlert("@{R=VIM.Strings;K=VIMCFGDATA_CLM_3;E=js}");
            return false;
        }

        return true;
    };

    ns.validateGroupingLevels = function() {
        ns.groupingLevels = $("[id*=GroupingLevel] option:selected");

        var isAnyGroupingLevelSelected = ns.checkIfAnyGroupingLevelIsSelected();
        if (!isAnyGroupingLevelSelected) {
            ns.showErrorAlert("@{R=VIM.Strings;K=VIMCFGDATA_CLM_4;E=js}");
            return false;
        }

        var selectedGroupingLevelsAreUnique = ns.checkIfSelectedGroupingLevelsAreUnique();
        if (!selectedGroupingLevelsAreUnique) {
            ns.showErrorAlert("@{R=VIM.Strings;K=VIMCFGDATA_CLM_5;E=js}");
            return false;
        }

        return true;
    };
    ns.checkIfAnyGroupingLevelIsSelected = function() {
        var isAnySelected = false;
        $.each(ns.groupingLevels, function(index, value) {
            if (value.value !== "None") {
                isAnySelected = true;
            }
        });

        return isAnySelected;
    };
    ns.checkIfSelectedGroupingLevelsAreUnique = function() {
        var groupingLevels = [];
        var groupingLevelsAreUnique = true;
        $.each(ns.groupingLevels, function(index, value) {
            if ($.inArray(value.value, groupingLevels) !== -1) {
                groupingLevelsAreUnique = false;
            } else {
                if (value.value !== "None") {
                    groupingLevels.push(value.value);
                }
            }
        });

        return groupingLevelsAreUnique;
    };
    ns.validateData = function() {
        ns.hideErrorAlert();

        var isTitleValid = ns.validateTitle();
        if (!isTitleValid) return false;

        var areGroupingLevelsValid = ns.validateGroupingLevels();
        return areGroupingLevelsValid;
    };

}(editCloudServerInfrastructure));

$(function() {
    $("#EditErrorAlert").css("visibility", "hidden");
    $("[automation='Submit']").click(editCloudServerInfrastructure.validateData);
});