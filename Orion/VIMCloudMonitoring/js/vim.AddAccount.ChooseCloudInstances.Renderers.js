﻿/* eslint-env browser */
/* eslint no-undef: "error" */
/* global SW */
(function (renderers) {
    renderers.renderMonitoredCellTemplate = function (value, metaData, record) {
        var monitoredBy = record.get("MonitoredByAccount");
        if (monitoredBy.length > 0) {
            return "<div class=\"monitoredBy\">Monitored by:<div class=\"accountName\" title=\"" + monitoredBy + "\">" + monitoredBy + "</div>";
        }
        var switchStyleName = (value === true) ? "clm-icon-on" : "clm-icon-off";
        return "<div class=\"clm-icon-in-cell " + switchStyleName + " chooseCloudInstancesMonitoredCell\"></div>";
    };

    renderers.renderNameWithManagedStatusIconFactory = function (mapper) {
        return function(value, metaData, record) { //metaData is required by ExtJs contract
            return "<span class=\"clm-icon " + mapper(record.data.Monitored, record.data.ManagedStatus) +
                "\"></span><span class=\"chooseCloudInstancesNameCell\">" + Ext42.util.Format.htmlEncode(value) + "</span>";
        };
    };
}(SW.Core.namespace("SW.CloudMonitoring.AddCloudAccount.ChooseInstances.Renderers")));
