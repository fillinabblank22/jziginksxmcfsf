﻿/* global SW, Ext42 */
(function (chooseInstances) {

    var cloudAccountId;
    var cloudAccount;
    var gridReady = false;

    var config = { isDemo: false };

    function getCloudAccountsPageUrl() {
        return "/Orion/CloudMonitoring/Admin/CloudAccounts/";
    }

    function resourceFactory() {
        this.getCloudAccount = function (cloudAccountId) {
            return $.ajax({
                url: "/api/CloudAccount/GetAwsCloudAccount",
                type: "get",
                dataType: "json",
                data: { cloudAccountId: cloudAccountId }
            });
        };
        this.getVirtualMachines = function (cloudAccountId) {
            return $.ajax({
                url: "/api/CloudInstanceAdmin/GetAwsInstancesForAccount",
                type: "get",
                dataType: "json",
                data: { cloudAccountId: cloudAccountId }
            });
        };
        this.updateVirtualMachines = function(virtualMachines) {
            return $.ajax({
                url: "/api/CloudInstanceAdmin/AddOrUpdateAwsInstances",
                type: "post",
                dataType: "json",
                data: virtualMachines
            });
        };
    }

    var resourceFactoryObject = new resourceFactory();
    var errorPresenterObject = new SW.VIM.Common.ErrorPresenter("#editChosenInstancesError");
    var queryStringParserObject = new SW.VIM.Common.QueryStringParser();
    var progressBarDialogControllerObject =
        new SW.VIM.Controls.ProgressBarDialogController("#ProgressbarDialog", "#ProgressInformation");

    function hideProgressBarIfAllReady() {
        if (gridReady && cloudAccount) {
            progressBarDialogControllerObject.hide();
        }
    }

    function setDynamicPageTitle(suffix) {
        $("#pageTitle").html("@{R=VIM.Strings;K=VIMWEBDATA_CLM_33;E=js}" + "<strong>" + suffix + "</strong>" );
    }

    function initializeGrid(cloudAccountId) {
        resourceFactoryObject.getVirtualMachines(cloudAccountId)
            .done(function (cloudInstancesResult) {
                config.isDemo = cloudInstancesResult.IsDemo === true; 
                SW.CloudMonitoring.AddCloudAccount.ChooseInstances.initializeConfig(config);
                SW.CloudMonitoring.AddCloudAccount.ChooseInstances.initializeChooseCloudInstances(cloudInstancesResult.CloudInstances);
                gridReady = true;
                hideProgressBarIfAllReady();
            })
            .fail(function (jqXhr) {
                var errorMessage = "@{R=VIM.Strings;K=VIMWEBDATA_CLM_29;E=js}" + "<br/>" + jqXhr.responseText;
                errorPresenterObject.show(errorMessage);
                progressBarDialogControllerObject.hide();
            });
    }

    function retrieveCloudAccount(cloudAccountId) {
        resourceFactoryObject.getCloudAccount(cloudAccountId)
            .done(function(cloudAccountResult) {
                cloudAccount = cloudAccountResult;
                setDynamicPageTitle(cloudAccount.Name);
                hideProgressBarIfAllReady();
            })
            .fail(function (jqXhr) {
                var errorMessage = "@{R=VIM.Strings;K=VIMWEBDATA_CLM_32;E=js}" + "<br/>" + jqXhr.responseText;
                errorPresenterObject.show(errorMessage);
                progressBarDialogControllerObject.hide();
            });
    }

    chooseInstances.initializePage = function () {
        progressBarDialogControllerObject.init();
        progressBarDialogControllerObject.setTitle("@{R=VIM.Strings;K=VIMWEBDATA_CLM_22;E=js}");
        progressBarDialogControllerObject.setProgressBarInformation("@{R=VIM.Strings;K=VIMWEBDATA_CLM_22;E=js}");
        progressBarDialogControllerObject.show();

        cloudAccountId = queryStringParserObject.parseValue(window.location.search, "CloudAccountId");

        if (cloudAccountId) {
            retrieveCloudAccount(cloudAccountId);
            initializeGrid(cloudAccountId);
        } else {
            errorPresenterObject.show("@{R=VIM.Strings;K=VIMWEBDATA_CLM_30;E=js}");
            progressBarDialogControllerObject.hide();
        }
    };

    chooseInstances.onSave = function () {
        if (config.isDemo) {
            demoAction();
            return;
        }

        if (!SW.CloudMonitoring.AddCloudAccount.ChooseInstances.instancesGrid) {
            return;
        }
        progressBarDialogControllerObject.setTitle("@{R=VIM.Strings;K=VIMWEBDATA_CLM_31;E=js}");
        progressBarDialogControllerObject.setProgressBarInformation("@{R=VIM.Strings;K=VIMWEBDATA_CLM_31;E=js}");
        progressBarDialogControllerObject.show();

        function virtualMachines() {
            this.CloudAccountId = cloudAccountId;
            var store = SW.CloudMonitoring.AddCloudAccount.ChooseInstances.instancesGrid.store;
            this.VirtualMachines = $.map(store.proxy.data, function (item) {
                return Ext42.create(store.proxy.model, item).data;
            });
        }

        resourceFactoryObject.updateVirtualMachines(new virtualMachines())
            .done(function() {
                errorPresenterObject.hide();
                window.location.href = getCloudAccountsPageUrl();
            })
            .fail(function (response) {
                progressBarDialogControllerObject.hide();
                errorPresenterObject.show(response.responseText);
            });
    };

    chooseInstances.onCancel = function () {
        window.location.href = getCloudAccountsPageUrl();
    };

    // TODO: move this out to place of usage
    $(function() {
        chooseInstances.initializePage();
        $("#SaveButton").click(chooseInstances.onSave);
        $("#CancelButton").click(chooseInstances.onCancel);
    });

}(SW.Core.namespace("SW.CloudMonitoring.EditChosenCloudInstances")));
