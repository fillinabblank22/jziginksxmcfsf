﻿/* global SW */
(function (CloudStatusNameProvider) {
    CloudStatusNameProvider.getStatusName = function (managedStatus) {
        if (managedStatus == null) {
            return "";
        }
        switch (Number(managedStatus)) {
        case 0:
            return "unknown";
        case 1:
            return "up";
        case 2:
            return "down";
        case 3:
            return "warning";
        case 4:
            return "shutdown";
        case 9:
            return "unmanaged";
        case 12:
            return "unreachable";
        case 14:
            return "critical";
        default:
            return "";
        }
    };
}(SW.Core.namespace("SW.VIM.CloudMonitoring.CloudStatusNameProvider")));
