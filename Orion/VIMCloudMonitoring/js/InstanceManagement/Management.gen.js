var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var SW;
(function (SW) {
    var VIM;
    (function (VIM) {
        var CloudMonitoring;
        (function (CloudMonitoring) {
            var CloudInstance;
            (function (CloudInstance) {
                var Management;
                (function (Management) {
                    var CloudInstanceState;
                    (function (CloudInstanceState) {
                        CloudInstanceState[CloudInstanceState["Unknown"] = -1] = "Unknown";
                        CloudInstanceState[CloudInstanceState["Pending"] = 0] = "Pending";
                        CloudInstanceState[CloudInstanceState["Running"] = 16] = "Running";
                        CloudInstanceState[CloudInstanceState["ShuttingDown"] = 32] = "ShuttingDown";
                        CloudInstanceState[CloudInstanceState["Terminated"] = 48] = "Terminated";
                        CloudInstanceState[CloudInstanceState["Stopping"] = 64] = "Stopping";
                        CloudInstanceState[CloudInstanceState["Stopped"] = 80] = "Stopped";
                    })(CloudInstanceState = Management.CloudInstanceState || (Management.CloudInstanceState = {}));
                    var CloudProvider;
                    (function (CloudProvider) {
                        CloudProvider[CloudProvider["Aws"] = 1] = "Aws";
                        CloudProvider[CloudProvider["Azure"] = 2] = "Azure";
                    })(CloudProvider = Management.CloudProvider || (Management.CloudProvider = {}));
                    var rxEncodedMessage = / Encoded authorization failure message\:.*$/;
                    function trimError(error) {
                        return error && error.replace(rxEncodedMessage, "") || "";
                    }
                    var CloudInstanceManagementApi = (function () {
                        function CloudInstanceManagementApi() {
                            this.webServiceUrl = "/Orion/VIMCloudMonitoring/Services/InstanceManagementService.asmx";
                        }
                        Object.defineProperty(CloudInstanceManagementApi.prototype, "Url", {
                            get: function () {
                                return this.webServiceUrl;
                            },
                            enumerable: true,
                            configurable: true
                        });
                        CloudInstanceManagementApi.prototype.callAction = function (method, data) {
                            var dfd = $.Deferred();
                            SW.Core.Services.callWebService(this.webServiceUrl, method, data, function (res) {
                                if (res && res.Success) {
                                    dfd.resolve({
                                        InstanceId: res.InstanceId,
                                        PreviousState: res.PreviousState,
                                        CurrentState: res.CurrentState,
                                    });
                                }
                                else {
                                    dfd.reject(trimError(res && res.ErrorMessage));
                                }
                            }, function (err) {
                                dfd.reject(trimError(err));
                            });
                            return dfd.promise();
                        };
                        CloudInstanceManagementApi.prototype.start = function (virtualMachineId) {
                            var data = {
                                virtualMachineId: virtualMachineId
                            };
                            return this.callAction("Start", data);
                        };
                        CloudInstanceManagementApi.prototype.stop = function (virtualMachineId, force) {
                            var data = {
                                virtualMachineId: virtualMachineId,
                                force: force
                            };
                            return this.callAction("Stop", data);
                        };
                        CloudInstanceManagementApi.prototype.reboot = function (virtualMachineId) {
                            var data = {
                                virtualMachineId: virtualMachineId
                            };
                            return this.callAction("Reboot", data);
                        };
                        CloudInstanceManagementApi.prototype.delete = function (virtualMachineId, removeNode) {
                            var data = {
                                virtualMachineId: virtualMachineId,
                                removeNode: removeNode
                            };
                            return this.callAction("Delete", data);
                        };
                        CloudInstanceManagementApi.prototype.unmanage = function (virtualMachineId) {
                            var data = {
                                virtualMachineId: virtualMachineId
                            };
                            return this.callAction("Unmanage", data);
                        };
                        CloudInstanceManagementApi.prototype.remanage = function (virtualMachineId) {
                            var data = {
                                virtualMachineId: virtualMachineId
                            };
                            return this.callAction("Remanage", data);
                        };
                        CloudInstanceManagementApi.prototype.pollNow = function (virtualMachineId) {
                            var data = {
                                virtualMachineId: virtualMachineId
                            };
                            return this.callAction("PollNow", data);
                        };
                        return CloudInstanceManagementApi;
                    }());
                    Management.CloudInstanceManagementApi = CloudInstanceManagementApi;
                })(Management = CloudInstance.Management || (CloudInstance.Management = {}));
            })(CloudInstance = CloudMonitoring.CloudInstance || (CloudMonitoring.CloudInstance = {}));
        })(CloudMonitoring = VIM.CloudMonitoring || (VIM.CloudMonitoring = {}));
    })(VIM = SW.VIM || (SW.VIM = {}));
})(SW || (SW = {}));
var SW;
(function (SW) {
    var VIM;
    (function (VIM) {
        var ManagementResource;
        (function (ManagementResource) {
            var htmlDialog = "\n<div class=\"mgmtConfirmDialog dialog-border\">\n    <div class=\"mgmtBody\">\n    </div>\n    <div class=\"dialog-buttons\">\n        <a class=\"btn-yes sw-btn-primary sw-btn\" automation=\"Yes\" href=\"#\"><span class=\"sw-btn-c\"><span class=\"sw-btn-t\">Yes</span></span></a>\n        <a class=\"btn-no sw-btn\" automation=\"No\" href=\"#\"><span class=\"sw-btn-c\"><span class=\"sw-btn-t\">No</span></span></a>\n        <a class=\"btn-close sw-btn\" automation=\"Close\" href=\"#\"><span class=\"sw-btn-c\"><span class=\"sw-btn-t\">Close</span></span></a>\n    </div>\n</div>\n";
            var htmlSuccess = "<span class=\"sw-suggestion sw-suggestion-pass mgmtSuccess\"><span class=\"sw-suggestion-icon\"></span><label class=\"message\"></label></span>";
            var htmlError = "<span class=\"sw-suggestion sw-suggestion-fail mgmtError\"><span class=\"sw-suggestion-icon\"></span><label class=\"message\"></label></span>";
            var htmlWarning = "<table><tr><td><img src=\"/Orion/images/NotificationImages/notification_warning.gif\" /></td><td><label class=\"message\"></label></td></tr></table>";
            var htmlProgress = "<span class=\"sw-suggestion sw-suggestion-progress mgmtProgress\"><span class=\"sw-suggestion-icon\" /><label class=\"message\"></label></span>";
            function setMessage(container, message) {
                container.find(".message").html(message);
                return container;
            }
            function setBtnText($btn, text) {
                $btn.find(Selectors.DlgBtnText).text(text);
                return $btn;
            }
            function createMsgSuccess(message) {
                return setMessage($(htmlSuccess), message);
            }
            function createMsgError(message) {
                return setMessage($(htmlError), message);
            }
            function createWarning(message) {
                return setMessage($(htmlWarning), message);
            }
            function createProgress(message) {
                return setMessage($(htmlProgress), message);
            }
            var Texts = (function () {
                function Texts() {
                }
                Texts.DlgBtnYes = "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ConfirmDialogYes;E=js}";
                Texts.DlgBtnNo = "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ConfirmDialogNo;E=js}";
                Texts.DlgBtnClose = "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ConfirmDialogClose;E=js}";
                return Texts;
            }());
            var Selectors = (function () {
                function Selectors() {
                }
                Selectors.ResContent = ".ResourceContent";
                Selectors.ResWrapper = ".ResourceWrapper";
                Selectors.MgmtButtons = ".mgmtButtons";
                Selectors.DialogBody = ".mgmtBody";
                Selectors.DialogButtons = ".dialog-buttons";
                Selectors.DlgBtnYes = ".btn-yes";
                Selectors.DlgBtnNo = ".btn-no";
                Selectors.DlgBtnClose = ".btn-close";
                Selectors.DlgBtnText = ".sw-btn-t";
                Selectors.DialogUI = ".ui-dialog";
                Selectors.DlgCrossBtn = ".ui-dialog-titlebar .ui-dialog-titlebar-close";
                Selectors.RemoveNodeCheck = "div.removeNode input[type=\"checkbox\"]";
                return Selectors;
            }());
            function ensureResource(container) {
                if (!container) {
                    throw new ReferenceError("container");
                }
                if (!container.is(Selectors.ResWrapper + " " + Selectors.ResContent)) {
                    container = container.closest(Selectors.ResWrapper).find(Selectors.ResContent);
                }
                if (!container.length) {
                    throw new RangeError("container");
                }
                return container;
            }
            ManagementResource.ensureResource = ensureResource;
            var ManagementResourceView = (function () {
                function ManagementResourceView(location) {
                    this.location = location;
                }
                ManagementResourceView.prototype.initialize = function (container) {
                    this.$resource = ensureResource(container);
                };
                ManagementResourceView.prototype.getResource = function () {
                    return this.$resource;
                };
                ManagementResourceView.prototype.getDialog = function () {
                    return this.$dialog;
                };
                Object.defineProperty(ManagementResourceView.prototype, "$dlgBody", {
                    get: function () {
                        return this.$dialog.find(Selectors.DialogBody);
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(ManagementResourceView.prototype, "$dlgButtons", {
                    get: function () {
                        return this.$dialog.find(Selectors.DialogButtons);
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(ManagementResourceView.prototype, "$dlgBtnYes", {
                    get: function () {
                        return this.$dlgButtons.find(Selectors.DlgBtnYes);
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(ManagementResourceView.prototype, "$dlgBtnNo", {
                    get: function () {
                        return this.$dlgButtons.find(Selectors.DlgBtnNo);
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(ManagementResourceView.prototype, "$dlgBtnClose", {
                    get: function () {
                        return this.$dlgButtons.find(Selectors.DlgBtnClose);
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(ManagementResourceView.prototype, "$dlgUI", {
                    get: function () {
                        return this.$dialog.closest(Selectors.DialogUI);
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(ManagementResourceView.prototype, "$dlgCrossButton", {
                    get: function () {
                        return this.$dlgUI.find(Selectors.DlgCrossBtn);
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(ManagementResourceView.prototype, "$mgmtButtons", {
                    get: function () {
                        return this.$resource.find(Selectors.MgmtButtons);
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(ManagementResourceView.prototype, "$wrapper", {
                    get: function () {
                        return this.$resource.closest(Selectors.ResWrapper);
                    },
                    enumerable: true,
                    configurable: true
                });
                ManagementResourceView.prototype.getButton = function (button) {
                    var $bn = this.$mgmtButtons.find(".management-button." + button.name);
                    return $bn.length
                        ? $bn
                        : this.createButton(button).appendTo(this.$mgmtButtons);
                };
                ManagementResourceView.prototype.bindButton = function (button, action) {
                    var $bn = this.getButton(button);
                    $bn.unbind('click').bind('click', function () { return action(); });
                };
                ManagementResourceView.prototype.updateButtonVisibility = function (button, show) {
                    var $bn = this.getButton(button);
                    if (show) {
                        $bn.show();
                    }
                    else {
                        $bn.hide();
                    }
                };
                ManagementResourceView.prototype.updateButtonDisabled = function (button, isDisabled, action) {
                    var $bn = this.getButton(button);
                    if (isDisabled) {
                        $bn.unbind("click");
                        $bn.bind("click", action);
                    }
                };
                ManagementResourceView.prototype.createButton = function (button) {
                    return $("<span class=\"management-button " + button.name + "\"><a href=\"#\">" + button.title + "</a></span>");
                };
                ManagementResourceView.prototype.setBody = function (content) {
                    this.$dlgBody.empty().append(content);
                };
                ManagementResourceView.prototype.createDialog = function (title) {
                    if (!this.$dialog) {
                        this.$dialog = $(htmlDialog);
                        this.$dialog.dialog({
                            width: 500,
                            modal: true,
                            position: [null, 100],
                            autoOpen: false
                        });
                        setBtnText(this.$dlgBtnYes, Texts.DlgBtnYes);
                        setBtnText(this.$dlgBtnNo, Texts.DlgBtnNo);
                        setBtnText(this.$dlgBtnClose, Texts.DlgBtnClose);
                    }
                    this.$dlgBody.empty();
                    this.$dlgButtons.show();
                    this.$dialog.dialog({
                        title: title
                    });
                    return this.$dialog;
                };
                ManagementResourceView.prototype.invokeCloseAction = function (onClose) {
                    if (onClose) {
                        onClose();
                    }
                    return false;
                };
                ManagementResourceView.prototype.showActionDialog = function (texts, action, onClose) {
                    var _this = this;
                    function wrapError(error) {
                        return error && " (" + error + ")" || "";
                    }
                    var removeNode = this.getRemoveNodeCheck();
                    this.createDialog(texts.actionTitle);
                    this.setBody(createProgress(texts.loading));
                    this.$dlgBtnYes.hide();
                    this.$dlgBtnNo.hide();
                    this.$dlgBtnClose.hide();
                    this.$dlgCrossButton
                        .bind("click", function () { return _this.invokeCloseAction(onClose); });
                    this.$dlgBtnClose
                        .unbind("click")
                        .bind("click", function () {
                        _this.$dialog.dialog("close");
                        _this.invokeCloseAction(onClose);
                    });
                    this.$dialog.dialog("open");
                    return action(removeNode)
                        .done(function (res) {
                        _this.setBody(createMsgSuccess(texts.success));
                        _this.$dlgBtnClose.show();
                    })
                        .fail(function (error) {
                        _this.setBody(createMsgError("" + texts.error + wrapError(error)));
                        _this.$dlgBtnClose.show();
                    });
                };
                ManagementResourceView.prototype.showConfirmDialog = function (texts, action, onClose) {
                    var _this = this;
                    this.createDialog(texts.confirmTitle || texts.actionTitle);
                    this.setBody(createWarning(texts.confirmMessage));
                    this.$dlgBtnYes.show();
                    this.$dlgBtnNo.show();
                    this.$dlgBtnClose.hide();
                    this.$dlgBtnYes
                        .unbind("click")
                        .bind("click", function () {
                        setTimeout(function () { return _this.showActionDialog(texts, action, onClose); });
                        return false;
                    });
                    this.$dlgBtnNo
                        .unbind("click")
                        .bind("click", function () {
                        _this.$dialog.dialog("close");
                        return false;
                    });
                    this.$dialog.dialog("open");
                    return this.getDialog();
                };
                ManagementResourceView.prototype.showDialog = function (texts, action, onClose, skipConfirmation) {
                    if (skipConfirmation) {
                        this.showActionDialog(texts, action, onClose);
                    }
                    else {
                        this.showConfirmDialog(texts, action, onClose);
                    }
                    return this.getDialog();
                };
                ManagementResourceView.prototype.showResource = function (shouldBeVisible) {
                    if (shouldBeVisible) {
                        this.$wrapper.show();
                    }
                    else {
                        this.$wrapper.hide();
                    }
                };
                ManagementResourceView.prototype.reload = function (url) {
                    if (url) {
                        this.location.href = url;
                    }
                    else {
                        this.location.reload();
                    }
                };
                ManagementResourceView.prototype.getRemoveNodeCheck = function () {
                    if (this.$dialog) {
                        return this.$dlgBody.find(Selectors.RemoveNodeCheck).is(":checked");
                    }
                    return false;
                };
                return ManagementResourceView;
            }());
            ManagementResource.ManagementResourceView = ManagementResourceView;
        })(ManagementResource = VIM.ManagementResource || (VIM.ManagementResource = {}));
    })(VIM = SW.VIM || (SW.VIM = {}));
})(SW || (SW = {}));
var SW;
(function (SW) {
    var VIM;
    (function (VIM) {
        var ManagementResource;
        (function (ManagementResource) {
            var ManagementResourceController = (function () {
                function ManagementResourceController(data, view) {
                    this.config = data;
                    this.view = view;
                    if (typeof this.config.reloadTimeout !== "number") {
                        this.config.reloadTimeout = 2000;
                    }
                }
                ManagementResourceController.prototype.initialize = function () {
                    var _this = this;
                    this.buttons.forEach(function (button) {
                        _this.view.bindButton(button, function () { return _this.showButtonDialog(button); });
                    });
                    this.updateButtonsVisibility();
                    this.updateButtonsDisabled();
                };
                ManagementResourceController.prototype.reload = function (instantly, url) {
                    var _this = this;
                    if (typeof instantly !== "boolean") {
                        instantly = false;
                    }
                    var timeout = instantly ? 0 : this.config.reloadTimeout;
                    setTimeout(function () { return _this.view.reload(url); }, timeout);
                };
                ManagementResourceController.prototype.updateButtonsVisibility = function () {
                    var _this = this;
                    this.buttons.forEach(function (button) { return _this.view.updateButtonVisibility(button, button.visibility()); });
                    this.updateResourceVisibility();
                };
                ManagementResourceController.prototype.updateButtonsDisabled = function () {
                    var _this = this;
                    if (this.config.demo.IsDemoMode()) {
                        this.buttons.forEach(function (button) { return _this.view.updateButtonDisabled(button, button.disabled(), demoAction); });
                    }
                    else {
                        this.buttons.forEach(function (button) { return _this.view.updateButtonDisabled(button, button.disabled(), function () { }); });
                    }
                };
                ManagementResourceController.prototype.updateResourceVisibility = function () {
                    var shouldBeHidden = this.buttons.every(function (button) { return !button.visibility(); });
                    this.view.showResource(!this.config.hideWhenNoButtons || !shouldBeHidden);
                };
                ManagementResourceController.prototype.formatMessage = function (format) {
                    return SW.Core.String.Format(format, this.getMessageData());
                };
                ManagementResourceController.prototype.formatTexts = function (texts) {
                    var _this = this;
                    var formatted = {};
                    Object.keys(texts).forEach(function (k) { return formatted[k] = _this.formatMessage(texts[k]); });
                    return formatted;
                };
                ManagementResourceController.prototype.showButtonDialog = function (button) {
                    var texts = this.formatTexts(button.texts);
                    return this.view.showDialog(texts, button.action, button.onClose, button.skipConfirmation);
                };
                return ManagementResourceController;
            }());
            ManagementResource.ManagementResourceController = ManagementResourceController;
        })(ManagementResource = VIM.ManagementResource || (VIM.ManagementResource = {}));
    })(VIM = SW.VIM || (SW.VIM = {}));
})(SW || (SW = {}));
var SW;
(function (SW) {
    var VIM;
    (function (VIM) {
        var CloudMonitoring;
        (function (CloudMonitoring) {
            var CloudInstance;
            (function (CloudInstance) {
                var Management;
                (function (Management) {
                    var Selectors = (function () {
                        function Selectors() {
                        }
                        Selectors.ForceCheck = "div.force input[type=\"checkbox\"]";
                        return Selectors;
                    }());
                    var ManagementResourceView = (function (_super) {
                        __extends(ManagementResourceView, _super);
                        function ManagementResourceView() {
                            return _super !== null && _super.apply(this, arguments) || this;
                        }
                        ManagementResourceView.prototype.getForceCheck = function () {
                            return this.$dlgBody.find(Selectors.ForceCheck).is(":checked");
                        };
                        return ManagementResourceView;
                    }(VIM.ManagementResource.ManagementResourceView));
                    Management.ManagementResourceView = ManagementResourceView;
                })(Management = CloudInstance.Management || (CloudInstance.Management = {}));
            })(CloudInstance = CloudMonitoring.CloudInstance || (CloudMonitoring.CloudInstance = {}));
        })(CloudMonitoring = VIM.CloudMonitoring || (VIM.CloudMonitoring = {}));
    })(VIM = SW.VIM || (SW.VIM = {}));
})(SW || (SW = {}));
var SW;
(function (SW) {
    var VIM;
    (function (VIM) {
        var CloudMonitoring;
        (function (CloudMonitoring) {
            var CloudInstance;
            (function (CloudInstance) {
                var Management;
                (function (Management) {
                    var ManagementResourceController = (function (_super) {
                        __extends(ManagementResourceController, _super);
                        function ManagementResourceController(data, api, view) {
                            var _this = _super.call(this, data, view) || this;
                            _this.api = api;
                            _this.bnStart = {
                                name: "start",
                                title: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionStart_ButtonTitle;E=js}",
                                visibility: function () { return _this.visibleStart(); },
                                action: function () { return _this.performStart(); },
                                texts: {
                                    actionTitle: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionStart_Title;E=js}",
                                    confirmMessage: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionStart_ConfirmMessage;E=js}",
                                    loading: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionStart_Progress;E=js}",
                                    success: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionStart_Success;E=js}",
                                    error: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionStart_Error;E=js}"
                                },
                                disabled: function () { return _this.disabledStart(); }
                            };
                            _this.bnStop = {
                                name: "stop",
                                title: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionStop_ButtonTitle;E=js}",
                                visibility: function () { return _this.visibleStop(); },
                                action: function () { return _this.performStop(); },
                                texts: {
                                    actionTitle: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionStop_Title;E=js}",
                                    confirmMessage: _this.getStopActionConfirmationMessage(data),
                                    loading: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionStop_Progress;E=js}",
                                    success: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionStop_Success;E=js}",
                                    error: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionStop_Error;E=js}"
                                },
                                disabled: function () { return _this.disabledStop(); }
                            };
                            _this.bnReboot = {
                                name: "reboot",
                                title: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionReboot_ButtonTitle;E=js}",
                                visibility: function () { return _this.visibleReboot(); },
                                action: function () { return _this.performReboot(); },
                                texts: {
                                    actionTitle: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionReboot_Title;E=js}",
                                    confirmMessage: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionReboot_ConfirmMessage;E=js}",
                                    loading: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionReboot_Progress;E=js}",
                                    success: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionReboot_Success;E=js}",
                                    error: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionReboot_Error;E=js}"
                                },
                                disabled: function () { return _this.disabledReboot(); }
                            };
                            _this.bnDelete = {
                                name: "delete",
                                title: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionDelete_ButtonTitle;E=js}",
                                visibility: function () { return _this.visibleDelete(); },
                                action: function (removeNode) { return _this.performDelete(removeNode); },
                                onClose: function () {
                                    if (_this.wasTerminationSuccessful) {
                                        _this.reload(true, "/Orion/CloudMonitoring/Summary.aspx");
                                    }
                                },
                                texts: {
                                    actionTitle: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionDelete_Title;E=js}",
                                    confirmMessage: data.nodeMapped
                                        ? "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionDelete_ConfirmMessageNode;E=js}"
                                        : "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionDelete_ConfirmMessage;E=js}",
                                    loading: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionDelete_Progress;E=js}",
                                    success: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionDelete_Success;E=js}",
                                    error: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionDelete_Error;E=js}"
                                },
                                disabled: function () { return _this.disabledDelete(); }
                            };
                            _this.bnUnmanage = {
                                name: "unmanage",
                                title: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionUnmanage_ButtonTitle;E=js}",
                                visibility: function () { return _this.visibleUnmanage(); },
                                action: function () { return _this.performUnmanage(); },
                                onClose: function () { return _this.reload(true); },
                                texts: {
                                    actionTitle: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionUnmanage_Title;E=js}",
                                    confirmMessage: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionUnmanage_ConfirmMessage;E=js}",
                                    loading: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionUnmanage_Progress;E=js}",
                                    success: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionUnmanage_Success;E=js}",
                                    error: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionUnmanage_Error;E=js}"
                                },
                                disabled: function () { return _this.disabledUnmanage(); }
                            };
                            _this.bnRemanage = {
                                name: "remanage",
                                title: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionRemanage_ButtonTitle;E=js}",
                                visibility: function () { return _this.visibleRemanage(); },
                                action: function () { return _this.performRemanage(); },
                                onClose: function () { return _this.reload(true); },
                                texts: {
                                    actionTitle: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionRemanage_Title;E=js}",
                                    confirmMessage: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionRemanage_ConfirmMessage;E=js}",
                                    loading: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionRemanage_Progress;E=js}",
                                    success: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionRemanage_Success;E=js}",
                                    error: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionRemanage_Error;E=js}"
                                },
                                disabled: function () { return _this.disabledRemanage(); }
                            };
                            _this.bnPollNow = {
                                name: "pollnow",
                                title: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionPollNow_ButtonTitle;E=js}",
                                visibility: function () { return _this.visiblePollNow(); },
                                action: function () { return _this.performPollNow(); },
                                onClose: function () { return _this.reload(true); },
                                skipConfirmation: true,
                                texts: {
                                    actionTitle: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionPollNow_Title;E=js}",
                                    loading: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionPollNow_Progress;E=js}",
                                    success: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionPollNow_Success;E=js}",
                                    error: "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionPollNow_Error;E=js}"
                                },
                                disabled: function () { return _this.disabledPollNow(); }
                            };
                            _this.buttons = [
                                _this.bnStart,
                                _this.bnStop,
                                _this.bnReboot,
                                _this.bnDelete,
                                _this.bnUnmanage,
                                _this.bnRemanage,
                                _this.bnPollNow
                            ];
                            return _this;
                        }
                        ManagementResourceController.prototype.getStopActionConfirmationMessage = function (data) {
                            if (data.provider === Management.CloudProvider.Aws) {
                                return "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionStop_ConfirmMessage_Aws;E=js}";
                            }
                            return "@{R=VIM.Strings.CloudMonitoring;K=WEBDATA_CLM_CloudInstanceManagement_ActionStop_ConfirmMessage_Azure;E=js}";
                        };
                        ManagementResourceController.prototype.getMessageData = function () {
                            return [this.config.instanceName];
                        };
                        ManagementResourceController.prototype.updateState = function (res) {
                            this.config.state = res.CurrentState;
                            this.updateButtonsVisibility();
                        };
                        ManagementResourceController.prototype.updateManage = function () {
                            this.reload();
                        };
                        ManagementResourceController.prototype.visibleStart = function () {
                            return this.allowManagement() && !this.isUnkownState(this.config) && !this.isRunningState(this.config);
                        };
                        ManagementResourceController.prototype.disabledStart = function () {
                            return this.config.demo.IsDemoMode();
                        };
                        ManagementResourceController.prototype.visibleStop = function () {
                            return this.visibleStopAndReboot();
                        };
                        ManagementResourceController.prototype.disabledStop = function () {
                            return this.config.demo.IsDemoMode();
                        };
                        ManagementResourceController.prototype.visibleReboot = function () {
                            return this.visibleStopAndReboot();
                        };
                        ManagementResourceController.prototype.disabledReboot = function () {
                            return this.config.demo.IsDemoMode();
                        };
                        ManagementResourceController.prototype.visibleDelete = function () {
                            return this.allowManagement();
                        };
                        ManagementResourceController.prototype.disabledDelete = function () {
                            return this.config.demo.IsDemoMode();
                        };
                        ManagementResourceController.prototype.visibleUnmanage = function () {
                            return this.allowManagement() && !this.config.nodeMapped;
                        };
                        ManagementResourceController.prototype.disabledUnmanage = function () {
                            return this.config.demo.IsDemoMode();
                        };
                        ManagementResourceController.prototype.visibleRemanage = function () {
                            return (this.config.allowManagement || (this.config.demo && this.config.demo.IsDemoMode())) && this.config.unmanaged && !this.config.nodeMapped;
                        };
                        ManagementResourceController.prototype.disabledRemanage = function () {
                            return this.config.demo.IsDemoMode();
                        };
                        ManagementResourceController.prototype.visiblePollNow = function () {
                            return !this.config.unmanaged && !this.config.nodeMapped && (this.config.allowManagement || (this.config.demo && this.config.demo.IsDemoMode()));
                        };
                        ManagementResourceController.prototype.disabledPollNow = function () {
                            return this.config.demo.IsDemoMode();
                        };
                        ManagementResourceController.prototype.visibleStopAndReboot = function () {
                            return this.allowManagement() && !this.isUnkownState(this.config) && this.isRunningState(this.config);
                        };
                        ManagementResourceController.prototype.allowManagement = function () {
                            return (this.config.allowManagement || (this.config.demo && this.config.demo.IsDemoMode())) && !this.config.unmanaged;
                        };
                        ManagementResourceController.prototype.performStart = function () {
                            var _this = this;
                            return this.api.start(this.config.virtualMachineId)
                                .done(function (res) { return _this.updateState(res); });
                        };
                        ManagementResourceController.prototype.performStop = function () {
                            var _this = this;
                            var force = this.view.getForceCheck();
                            return this.api.stop(this.config.virtualMachineId, force)
                                .done(function (res) { return _this.updateState(res); });
                        };
                        ManagementResourceController.prototype.performReboot = function () {
                            var _this = this;
                            return this.api.reboot(this.config.virtualMachineId)
                                .done(function (res) { return _this.updateState(res); });
                        };
                        ManagementResourceController.prototype.performDelete = function (removeNode) {
                            var _this = this;
                            return this.api.delete(this.config.virtualMachineId, removeNode)
                                .done(function (res) {
                                _this.updateState(res);
                                _this.wasTerminationSuccessful = true;
                            })
                                .fail(function () { return _this.wasTerminationSuccessful = false; });
                        };
                        ManagementResourceController.prototype.performUnmanage = function () {
                            var _this = this;
                            return this.api.unmanage(this.config.virtualMachineId)
                                .done(function () { return _this.updateManage(); });
                        };
                        ManagementResourceController.prototype.performRemanage = function () {
                            var _this = this;
                            return this.api.remanage(this.config.virtualMachineId)
                                .done(function () { return _this.updateManage(); });
                        };
                        ManagementResourceController.prototype.performPollNow = function () {
                            var _this = this;
                            return this.api.pollNow(this.config.virtualMachineId)
                                .done(function () { return _this.updateManage(); });
                        };
                        ManagementResourceController.prototype.isUnkownState = function (data) {
                            return data.state === Management.CloudInstanceState.Unknown;
                        };
                        ManagementResourceController.prototype.isRunningState = function (data) {
                            return data.state === Management.CloudInstanceState.Running;
                        };
                        return ManagementResourceController;
                    }(VIM.ManagementResource.ManagementResourceController));
                    Management.ManagementResourceController = ManagementResourceController;
                })(Management = CloudInstance.Management || (CloudInstance.Management = {}));
            })(CloudInstance = CloudMonitoring.CloudInstance || (CloudMonitoring.CloudInstance = {}));
        })(CloudMonitoring = VIM.CloudMonitoring || (VIM.CloudMonitoring = {}));
    })(VIM = SW.VIM || (SW.VIM = {}));
})(SW || (SW = {}));
var SW;
(function (SW) {
    var VIM;
    (function (VIM) {
        var CloudMonitoring;
        (function (CloudMonitoring) {
            var CloudInstance;
            (function (CloudInstance) {
                var Management;
                (function (Management) {
                    function assertOptions(options) {
                        if (!options) {
                            throw new RangeError("options");
                        }
                        if (!options.container || !options.container.length) {
                            throw new RangeError("container");
                        }
                        if (typeof (options.virtualMachineId) !== "number") {
                            throw new RangeError("virtualMachineId");
                        }
                        if (typeof (options.instanceName) !== "string") {
                            throw new RangeError("instanceName");
                        }
                        if (typeof (options.nodeMapped) !== "boolean") {
                            throw new RangeError("nodeMapped");
                        }
                        if (typeof (options.unmanaged) !== "boolean") {
                            throw new RangeError("unmanaged");
                        }
                        if (typeof (options.state) !== "number") {
                            throw new RangeError("state");
                        }
                        if (typeof (options.allowManagement) !== "boolean") {
                            throw new RangeError("allowManagement");
                        }
                        if (typeof (options.allowUnmanage) !== "boolean") {
                            throw new RangeError("allowUnmanage");
                        }
                        if (!options.demo || typeof (options.demo.IsDemoMode) !== "function" || typeof (options.demo.DemoModeMessage) !== "function") {
                            throw new RangeError("demo");
                        }
                    }
                    function initializeManagementResource(options) {
                        assertOptions(options);
                        var config = {
                            hideWhenNoButtons: options.hideWhenNoButtons,
                            allowManagement: options.allowManagement,
                            allowUnmanage: options.allowUnmanage,
                            demo: options.demo,
                            virtualMachineId: options.virtualMachineId,
                            instanceName: options.instanceName,
                            state: options.state,
                            unmanaged: options.unmanaged,
                            nodeMapped: options.nodeMapped,
                            reloadTimeout: options.reloadTimeout,
                            provider: options.provider
                        };
                        var api = new Management.CloudInstanceManagementApi();
                        var view = new Management.ManagementResourceView(window.location);
                        view.initialize(options.container);
                        var controller = new Management.ManagementResourceController(config, api, view);
                        controller.initialize();
                        return controller;
                    }
                    Management.initializeManagementResource = initializeManagementResource;
                })(Management = CloudInstance.Management || (CloudInstance.Management = {}));
            })(CloudInstance = CloudMonitoring.CloudInstance || (CloudMonitoring.CloudInstance = {}));
        })(CloudMonitoring = VIM.CloudMonitoring || (VIM.CloudMonitoring = {}));
    })(VIM = SW.VIM || (SW.VIM = {}));
})(SW || (SW = {}));
//# sourceMappingURL=Management.gen.js.map