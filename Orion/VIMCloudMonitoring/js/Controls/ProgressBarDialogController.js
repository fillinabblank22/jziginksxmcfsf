﻿var vimControls = SW.Core.namespace("SW.VIM.Controls");

(function (controls) {

    controls.ProgressBarDialogController = function (dialogElementId, informationalElementId) {

        var progressBarDialogId = dialogElementId;
        var progressBarDialogInformationId = informationalElementId;

        this.setTitle = function(message) {
            $(progressBarDialogId).dialog("option", "title", message);
        };
        this.setProgressBarInformation = function(message) {
            $(progressBarDialogInformationId).html(message);
        };
        this.show = function() {
            $(progressBarDialogId).dialog("open");
        };
        this.hide = function() {
            $(progressBarDialogId).dialog("close");
        };
        this.init = function() {
            $(progressBarDialogId).dialog({
                autoOpen: false,
                closeOnEscape: false,
                resizable: false,
                modal: true,
                width: 400,
                open: function() {
                    $(".ui-dialog-titlebar-close").hide();
                },
                close: function() {
                }
            });
        };
    };
}(vimControls))