﻿SW.Core.namespace("SW.VIM.CloudAssetSummary").Resource = function () {
    var self = this;

    function addEvents() {
        $(self.containerId + "_Aws_Accordion").on("click", toogleAccordion);
        $(self.containerId + "_Azure_Accordion").on("click", toogleAccordion);
    }

    function toogleAccordion() {
        var target = $(this).attr("data-target");
        $(target).toggle();
        $(this).find(".arrow").toggleClass("arrowCollapsed");
        $(this).find(".arrow").toggleClass("arrowExpanded");
    }

    function hideAccordionsWhenOnlyOneAccountTypeExist(data) {

        if ((data.IsAnyAwsAccount === false && data.IsAnyAzureAccount === false)
            || (data.IsAnyAwsAccount === true && data.IsAnyAzureAccount === false)
            || (data.IsAnyAwsAccount === false && data.IsAnyAzureAccount === true)) {
            $(self.containerId + "_Aws_Accordion").hide();
            $(self.containerId + "_Azure_Accordion").hide();
        }


        if (data.IsAnyAwsAccount === false) {
            $(self.containerId + "_Aws").hide();
        }

        if (data.IsAnyAzureAccount === false) {
            $(self.containerId + "_Azure").hide();
        }
    }

    function loadDataToResourceForAws(data) {
        $(self.containerId + "_Aws_TotalInstances").html(data.AwsTotalInstances);
        $(self.containerId + "_Aws_TotalVolumes").html(data.AwsTotalVolumes);
        $(self.containerId + "_Aws_TotalStorageCapacity").html(SW.VIM.CloudMonitoring.CloudVolumePerformance.gigabytesFormatter(data.AwsTotalStorageCapacity));
        $(self.containerId + "_Aws_RunningInstances").html(data.AwsRunningInstances);
        $(self.containerId + "_Aws_StoppedInstances").html(data.AwsStoppedInstances);
        $(self.containerId + "_Aws_UnknownInstances").html(data.AwsUnknownInstances);
    }


    function loadDataToResourceForAzure(data) {
        $(self.containerId + "_Azure_TotalInstances").html(data.AzureTotalInstances);
        $(self.containerId + "_Azure_TotalStorageCapacity").html(SW.VIM.CloudMonitoring.CloudVolumePerformance.gigabytesFormatter(data.AzureTotalStorageCapacity));
        $(self.containerId + "_Azure_RunningInstances").html(data.AzureRunningInstances);
        $(self.containerId + "_Azure_StoppedInstances").html(data.AzureStoppedInstances);
        $(self.containerId + "_Azure_UnknownInstances").html(data.AzureUnknownInstances);
        $(self.containerId + "_Azure_TotalVolumes").html(data.AzureTotalVolumes);
    }


    function hideDetailStatisticsWhenValueIsZeroForAws(data) {
        if (data.AwsRunningInstances === 0) {
            $(self.containerId + "_Aws_RunningInstances").parent().hide();
        }

        if (data.AwsStoppedInstances === 0) {
            $(self.containerId + "_Aws_StoppedInstances").parent().hide();
        }

        if (data.AwsUnknownInstances === 0) {
            $(self.containerId + "_Aws_UnknownInstances").parent().hide();
        }
    }

    function hideDetailStatisticsWhenValueIsZeroForAzure(data) {
        if (data.AzureRunningInstances === 0) {
            $(self.containerId + "_Azure_RunningInstances").parent().hide();
        }

        if (data.AzureStoppedInstances === 0) {
            $(self.containerId + "_Azure_StoppedInstances").parent().hide();
        }

        if (data.AzureUnknownInstances === 0) {
            $(self.containerId + "_Azure_UnknownInstances").parent().hide();
        }
    }

    function createResource(data) {
        addEvents();
        loadDataToResourceForAws(data);
        loadDataToResourceForAzure(data);
        showResourceContainer();
        hideDetailStatisticsWhenValueIsZeroForAws(data);
        hideDetailStatisticsWhenValueIsZeroForAzure(data);
        hideAccordionsWhenOnlyOneAccountTypeExist(data);
    }

    function Resource(resourceId, containerId, limitationId) {
        self.resourceId = resourceId;
        self.containerId = containerId;
        self.limitationId = limitationId;
    }

    function showResourceContainer() {
        $(self.containerId).removeClass("hidden");
    }

    Resource.prototype = {
        refresh: function () {
            $.ajax({
                type: "GET",
                url: "/api/CloudAsset/Filter",
                data: { limitationId: self.limitationId },
                dataType: "json",
                cache: false
            }).success(function (data) {
                createResource(data);
            });
        }
    };
    return Resource;
}();