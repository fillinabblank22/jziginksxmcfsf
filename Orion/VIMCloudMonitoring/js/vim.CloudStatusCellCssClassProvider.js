﻿/* global SW */
(function (CloudStatusCellCssClassProvider) {
    CloudStatusCellCssClassProvider.getCellCssClass = function(statusName) {
        return "vim-clm-status" + (statusName ? (" vim-clm-status-" + statusName) : "");
    };
}(SW.Core.namespace("SW.VIM.CloudMonitoring.CloudStatusCellCssClassProvider")));
