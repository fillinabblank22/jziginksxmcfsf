﻿/* global SW, Ext42, $ */
(function (chooseInstances) {

    chooseInstances.mainPanel = null;
    chooseInstances.filtersPanel = null;
    chooseInstances.instancesGridPanel = null;
    chooseInstances.filterTree = null;
    chooseInstances.instancesGrid = null;

    var config = { isDemo: false };

    chooseInstances.updateToolbarButtons = function () {
        var selectedRows = chooseInstances.instancesGrid.getSelectionModel().selected.items;
        var nonmonitoredRows = selectedRows.filter(function (item) {
            return !item.data.MonitoredByAccount;
        });
        var isAnyNonmonitoredRowSelected = nonmonitoredRows.length > 0;
        var map = chooseInstances.instancesGrid.getDockedComponent("cloudInstancesGridTopBar").items.map;
        map.MonitoringOn.setDisabled(!isAnyNonmonitoredRowSelected || config.isDemo);
        map.MonitoringOff.setDisabled(!isAnyNonmonitoredRowSelected || config.isDemo);
    };

    chooseInstances.EnableMonitoring = function () {
        chooseInstances.changeMonitoredStatusOfSelectedItems(true);
    };

    chooseInstances.DisableMonitoring = function () {
        chooseInstances.changeMonitoredStatusOfSelectedItems(false);
    };

    chooseInstances.changeMonitoredStatusOfSelectedItems = function (monitored) {
        chooseInstances.setMonitoredStatusOfSelectedItems(monitored);
        chooseInstances.refreshFiltersPanel();
        chooseInstances.refreshGrid();
    };

    chooseInstances.setMonitoredStatusOfSelectedItems = function (monitored) {
        Ext42.each(
            chooseInstances.instancesGrid.getSelectionModel().selected.items,
            function (item) {
                if (item.data.MonitoredByAccount.length > 0) {
                    return;
                }
                chooseInstances.updateMonitoredStatus(item, monitored);
            }
        );
    };

    chooseInstances.updateMonitoredStatus = function (item, monitored) {

        item.data.Monitored = monitored;

        Ext42.each(
            chooseInstances.instancesGrid.store.proxy.data,
            function (virtualMachine) {
                if (virtualMachine.InstanceId == item.data.InstanceId) {
                    virtualMachine.Monitored = monitored;
                }
            }
        );
    };

    var createFiltersRootNode = function () {
        var getAvailableFilters = function () {
            var findPreviousNodeValue = function (propertyName, propertyValue) {
                var filterTreeNodeRetriever = new SW.VIM.Common.FilterTreeNodeRetriever();
                var filterTreeLeafStatusRetriever = new SW.VIM.Common.FilterTreeLeafStatusRetriever();

                if (chooseInstances.filterTree
                 && chooseInstances.filterTree.store
                 && chooseInstances.filterTree.store.tree
                 && chooseInstances.filterTree.store.tree.root
                 && chooseInstances.filterTree.store.tree.root.childNodes) {
                    var filterNodes = chooseInstances.filterTree.store.tree.root.childNodes;
                    var filterNode = filterTreeNodeRetriever.retrieve(filterNodes, propertyName);
                    var previousNodeValue = filterTreeLeafStatusRetriever.retrieve(filterNode.childNodes, propertyValue);
                    return previousNodeValue;
                }
                return null;
            };

            var calculateStatistics = function (cloudInstances, propertyName) {
                var statistics = {};
                cloudInstances.forEach(function (item) {
                    var propertyValue = item[propertyName];
                    if (!statistics[propertyValue]) {
                        var previousNodeValue = findPreviousNodeValue(propertyName, propertyValue);
                        statistics[propertyValue] = { count: 1, checked: previousNodeValue };
                    } else {
                        statistics[propertyValue].count += 1;
                    }
                });
                return statistics;
            };

            var filterTreeNodeValueCollectionFactory = new SW.VIM.Common.FilterTreeNodeValueCollectionFactory();

            var prepareRegionChildren = function () {
                var countCloudInstancesRegions = function (cloudInstances) {
                    return calculateStatistics(cloudInstances, "Region");
                };

                var availableOptions = countCloudInstancesRegions(chooseInstances.cloudInstances);
                return filterTreeNodeValueCollectionFactory.create(availableOptions);
            };

            var prepareAvailabilityZoneChildren = function () {
                var countCloudInstancesAvailabilityZones = function (cloudInstances) {
                    return calculateStatistics(cloudInstances, "AvailabilityZone");
                };

                var availableOptions = countCloudInstancesAvailabilityZones(chooseInstances.cloudInstances);
                return filterTreeNodeValueCollectionFactory.create(availableOptions);
            };

            var prepareTypeChildren = function () {
                var countCloudInstancesTypes = function (cloudInstances) {
                    return calculateStatistics(cloudInstances, "Type");
                };

                var availableOptions = countCloudInstancesTypes(chooseInstances.cloudInstances);
                return filterTreeNodeValueCollectionFactory.create(availableOptions);
            };

            var prepareStateChildren = function () {
                var countCloudInstancesStates = function (cloudInstances) {
                    return calculateStatistics(cloudInstances, "State");
                };

                var availableOptions = countCloudInstancesStates(chooseInstances.cloudInstances);
                return filterTreeNodeValueCollectionFactory.create(availableOptions);
            };

            var preparePlatformChildren = function () {
                var countCloudInstancesPlatforms = function (cloudInstances) {
                    return calculateStatistics(cloudInstances, "Platform");
                };

                var availableOptions = countCloudInstancesPlatforms(chooseInstances.cloudInstances);
                return filterTreeNodeValueCollectionFactory.create(availableOptions);
            };

            var prepareMonitoredChildren = function () {
                var countCloudInstancesMonitored = function (cloudInstances) {
                    return calculateStatistics(cloudInstances, "Monitored");
                };

                var availableOptions = countCloudInstancesMonitored(chooseInstances.cloudInstances);
                var mappingNames = { true: "@{R=VIM.Strings;K=VIMWEBDATA_CLM_25;E=js}", false: "@{R=VIM.Strings;K=VIMWEBDATA_CLM_26;E=js}" };
                return filterTreeNodeValueCollectionFactory.createWithMappedNames(availableOptions, mappingNames);
            };

            var instanceFilters = [
                {
                    data: { name: "@{R=VIM.Strings;K=VIMWEBDATA_CLM_4;E=js}" },
                    expanded: true,
                    id: "State",
                    iconCls: "noIconNode",
                    children: prepareStateChildren()
                },
                {
                    data: { name: "@{R=VIM.Strings;K=VIMWEBDATA_CLM_5;E=js}" },
                    expanded: true,
                    id: "Region",
                    iconCls: "noIconNode",
                    children: prepareRegionChildren()
                },
                {
                    data: { name: "@{R=VIM.Strings;K=VIMWEBDATA_CLM_6;E=js}" },
                    expanded: true,
                    id: "AvailabilityZone",
                    iconCls: "noIconNode",
                    children: prepareAvailabilityZoneChildren()
                },
                {
                    data: { name: "@{R=VIM.Strings;K=VIMWEBDATA_CLM_7;E=js}" },
                    expanded: true,
                    id: "Type",
                    iconCls: "noIconNode",
                    children: prepareTypeChildren()
                },
                {
                    data: { name: "@{R=VIM.Strings;K=VIMWEBDATA_CLM_8;E=js}" },
                    expanded: true,
                    id: "Platform",
                    iconCls: "noIconNode",
                    children: preparePlatformChildren()
                },
                {
                    data: { name: "@{R=VIM.Strings;K=VIMWEBDATA_CLM_24;E=js}" },
                    expanded: true,
                    id: "Monitored",
                    iconCls: "noIconNode",
                    children: prepareMonitoredChildren()
                }
            ];

            return instanceFilters;
        };

        var availableFilters = getAvailableFilters();
        var rootNode = {
            expanded: true,
            children: availableFilters
        };
        return rootNode;
    };

    chooseInstances.refreshFiltersPanel = function () {
        var newRootNode = createFiltersRootNode();
        chooseInstances.filterTree.store.setRootNode(newRootNode);
    };

    var initializeGridHeaderSummary = function () {
        var createStatistics = function (data) {
            var regionsStatistics = {};
            var availabilityZonesStatistics = {};
            var instancesStatistics = {};

            $.each(data, function (index, val) {
                chooseInstances.addToDictionary(regionsStatistics, val.Region);
                chooseInstances.addToDictionary(availabilityZonesStatistics, val.AvailabilityZone);
                chooseInstances.addToDictionary(instancesStatistics, val.InstanceId);

                if (val.Monitored) {
                    regionsStatistics[val.Region] = true;
                    availabilityZonesStatistics[val.AvailabilityZone] = true;
                    instancesStatistics[val.InstanceId] = true;
                }
            });

            return {
                regions: regionsStatistics,
                availabilityZones: availabilityZonesStatistics,
                instances: instancesStatistics
            };
        };
        var setGridHeaderValues = function (statistics) {
            var selectedRegions = chooseInstances.getNumberOfSelectedItems(statistics.regions);
            var allRegions = chooseInstances.getNumberOfItems(statistics.regions);
            var regionsCounterText = selectedRegions + "/" + allRegions;
            $("#RegionsCounter").text(regionsCounterText);

            var selectedAvailabilityZones = chooseInstances.getNumberOfSelectedItems(statistics.availabilityZones);
            var allAvailabilityZones = chooseInstances.getNumberOfItems(statistics.availabilityZones);
            var availabilityZonesCounterText = selectedAvailabilityZones + "/" + allAvailabilityZones;
            $("#AvailabilityZonesCounter").text(availabilityZonesCounterText);

            var selectedInstances = chooseInstances.getNumberOfSelectedItems(statistics.instances);
            var allInstances = chooseInstances.getNumberOfItems(statistics.instances);
            var instancesCounterText = selectedInstances + "/" + allInstances;
            $("#InstancesCounter").text(instancesCounterText);
        };

        var instancesGridStatistics = createStatistics(chooseInstances.instancesGrid.store.proxy.data);
        setGridHeaderValues(instancesGridStatistics);
    };

    chooseInstances.refreshGrid = function () {
        chooseInstances.instancesGrid.getView().refresh();
        initializeGridHeaderSummary();
    };

    var reapplyFilters = function (store) {
        var view = Ext42.getCmp("FilterInstances").getView();
        var checkedFilters = view.getChecked();
        var activeFilters = {};
        checkedFilters.forEach(function (item) {
            var itemRegex = "^" + item.data.text + "$";
            if (!activeFilters[item.data.parentId]) {
                activeFilters[item.data.parentId] = itemRegex;
            } else {
                activeFilters[item.data.parentId] += "|" + itemRegex;
            }
        });

        store.clearFilter();

        $.each(activeFilters, function (filterName, filterRegex) {
            store.filter(filterName, new RegExp(filterRegex));
        });
    };

    chooseInstances.searchForInstances = function () {
        var applySearchBoxFilter = function (store) {
            var searchText = Ext42.getCmp("SearchBox").getValue().toLowerCase();

            var nameAndInstanceIdFilters = [
                new Ext42.util.Filter({
                    filterFn: function (item) {
                        return item.get("Name").toLowerCase().indexOf(searchText) > -1
                            || item.get("InstanceId").toLowerCase().indexOf(searchText) > -1;
                    }
                })
            ];

            store.filter(nameAndInstanceIdFilters);
        };

        var instancesStore = chooseInstances.instancesGrid.getStore();
        reapplyFilters(instancesStore);
        applySearchBoxFilter(instancesStore);
    };

    chooseInstances.searchForInstancesOnEnter = function (pSender, pEvt) {
        if (pEvt.getKey() === pEvt.ENTER) {
            chooseInstances.searchForInstances();
        }
        return true;
    };

    chooseInstances.addToDictionary = function (dictionary, key) {
        if (!dictionary[key]) {
            dictionary[key] = false;
        }
    };

    chooseInstances.getNumberOfSelectedItems = function (collection) {
        var selected = 0;
        $.each(collection, function (index, val) {
            if (val) {
                selected++;
            }
        });

        return selected;
    };

    chooseInstances.getNumberOfItems = function (collection) {
        return $.map(collection, function (key, value) { return value; }).length;
    };

    var initializeFilters = function () {
        var defineTreeModelWithStatistics = function () {
            Ext42.define("SW.CloudMonitoring.AddCloudAccount.model.TreeModelWithStatistics", {
                extend: "Ext.data.TreeModel",
                fields: [
                    { name: "text", type: "string" },
                    { name: "qtip", type: "string" },
                    { name: "leaf", type: "bool" },
                    { name: "data", type: "auto" }
                ]
            });
        };

        var initializeFilterTree = function () {
            var createTreeStore = function () {
                return Ext42.create("Ext.data.TreeStore", {
                    root: createFiltersRootNode(),
                    model: "SW.CloudMonitoring.AddCloudAccount.model.TreeModelWithStatistics"
                });
            };

            var renderFilterTreeItem = function (value) {
                return [
                    "<span class=\"treeFilterItem-text\" >",
                    value.name,
                    "</span><span class=\"treeFilterItem-statistics\">",
                    value.statistics,
                    "<\span>"
                ].join("");
            };

            var createFilterTree = function (store, renderer, listener) {
                return Ext42.create("Ext.tree.Panel", {
                    id: "FilterInstances",
                    xtype: "check-tree",
                    width: 260,
                    store: store,
                    rootVisible: false,
                    useArrows: true,
                    hideHeaders: true,
                    columns: [
                        {
                            xtype: "treecolumn",
                            dataIndex: "data",
                            flex: 1,
                            renderer: renderer
                        }
                    ],
                    listeners: {
                        checkchange: listener
                    }
                });
            };

            var treeStore = createTreeStore();
            var columnRenderer = renderFilterTreeItem;
            var checkChangeListener = chooseInstances.searchForInstances;
            var filterTree = createFilterTree(treeStore, columnRenderer, checkChangeListener);
            chooseInstances.filterTree = filterTree;
        };

        defineTreeModelWithStatistics();
        initializeFilterTree();
    };

    var initializeFiltersPanel = function () {
        var createFiltersPanel = function () {
            return new Ext42.Panel({
                layout: "fit",
                region: "west",
                width: 260,
                heigth: 500,
                collapsible: true,
                title: "@{R=VIM.Strings;K=VIMWEBDATA_CLM_9; E=js}",
                items: [
                    chooseInstances.filterTree
                ]
            });
        };

        var filtersPanel = createFiltersPanel();
        chooseInstances.filtersPanel = filtersPanel;
    };

    var initializeCloudInstancesGrid = function () {
        var defineCloudInstancesGrid = function () {
            Ext42.define("CloudInstance", {
                extend: "Ext42.data.Model",
                fields: [
                    { name: "Name", type: "string" },
                    { name: "InstanceId", type: "string" },
                    { name: "State", type: "string" },
                    { name: "Region", type: "string" },
                    { name: "AvailabilityZone", type: "string" },
                    { name: "Type", type: "string" },
                    { name: "Monitored", type: "boolean" },
                    { name: "MonitoredByAccount", type: "string" },
                    { name: "Platform", type: "string" },
                    { name: "Status", type: "int" },
                    { name: "ReservationOwnerId", type: "string" },
                    { name: "KeyName", type: "string" },
                    { name: "VpcId", type: "string" },
                    { name: "IamRoleName", type: "string" },
                    { name: "EbsOptimized", type: "boolean" },
                    { name: "Tenancy", type: "string" },
                    { name: "RootDeviceType", type: "string" },
                    { name: "ReservationId", type: "string" },
                    { name: "LaunchTime", type: "datetime" },
                    { name: "Architecture", type: "string" },
                    { name: "SubnetId", type: "string" },
                    { name: "ImageId", type: "string" },
                    { name: "PlacementGroupName", type: "string" },
                    { name: "VirtualizationType", type: "string" },
                    { name: "AmiLaunchIndex", type: "int" },
                    { name: "SourceDestCheck", type: "boolean" },
                    { name: "MonitoringState", type: "string" },
                    { name: "ManagedStatus", type: "auto" }
                ]
            });
        };
        var createInstancesGrid = function () {
            var createTopBar = function () {
                return {
                    height: 30,
                    itemId: "cloudInstancesGridTopBar",
                    items: [
                        {
                            id: "MonitoringOn",
                            text: "@{R=VIM.Strings;K=VIMWEBDATA_CLM_10;E=js}",
                            iconCls: "clm-icon clm-icon-monitoring-on",
                            xtype: "button",
                            disabled: true,
                            handler: chooseInstances.EnableMonitoring
                        }, "-",
                        {
                            id: "MonitoringOff",
                            text: "@{R=VIM.Strings;K=VIMWEBDATA_CLM_11;E=js}",
                            iconCls: "clm-icon clm-icon-monitoring-off",
                            xtype: "button",
                            disabled: true,
                            handler: chooseInstances.DisableMonitoring
                        }, "-",
                        {
                            id: "RegionsHeader",
                            text: "@{R=VIM.Strings;K=VIMWEBDATA_CLM_19;E=js}",
                            cls: "headerSummary-header",
                            xtype: "label"
                        },
                        {
                            id: "RegionsCounter",
                            text: "0/0",
                            cls: "headerSummary-counter",
                            xtype: "label"
                        }, "-",
                        {
                            id: "AvailabilityZonesHeader",
                            text: "@{R=VIM.Strings;K=VIMWEBDATA_CLM_12;E=js}",
                            cls: "headerSummary-header",
                            xtype: "label"
                        },
                        {
                            id: "AvailabilityZonesCounter",
                            text: "0/0",
                            cls: "headerSummary-counter",
                            xtype: "label"
                        }, "-",
                        {
                            id: "InstancesHeader",
                            text: "@{R=VIM.Strings;K=VIMWEBDATA_CLM_13;E=js}",
                            cls: "headerSummary-header",
                            xtype: "label"
                        },
                        {
                            id: "InstancesCounter",
                            text: "0/0",
                            cls: "headerSummary-counter",
                            xtype: "label"
                        }, "-",
                        {
                            id: "SearchBox",
                            xtype: "textfield",
                            enableKeyEvents: true,
                            listeners: { specialkey: chooseInstances.searchForInstancesOnEnter }
                        }, " ",
                        {
                            xtype: "button",
                            height: 27,
                            width: 27,
                            cls: "searchButton",
                            icon: "/Orion/images/Button.SearchIcon.gif",
                            listeners: { click: chooseInstances.searchForInstances }
                        }
                    ]
                };
            };
            var createStore = function () {
                return Ext42.create("Ext42.data.Store", {
                    model: "CloudInstance",
                    autoLoad: true,
                    pageSize: 10,
                    remoteSort: true,
                    remoteFilter: true,
                    sorters: [{
                        property: 'Name',
                        direction: 'ASC'
                    }],
                    sortOnLoad: true,
                    proxy: {
                        type: "memory",
                        enablePaging: true,
                        data: chooseInstances.cloudInstances
                    }
                });
            };
            var createSelectionModel = function () {
                var selectionModel = Ext42.create("Ext42.selection.CheckboxModel");
                selectionModel.on("selectionchange", chooseInstances.updateToolbarButtons);
                selectionModel.renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                    var pfx = Ext42.baseCSSPrefix;
                    metaData.tdCls = pfx + "grid-cell-special " + pfx + "grid-cell-row-checker";
                    var monitoredBy = record.get("MonitoredByAccount");
                    return monitoredBy ? "" : "<div class=\"" + pfx + "grid-row-checker\"> </div>";
                };
                return selectionModel;
            };

            var createColumns = function () {
                return [
                    {
                        id: "Name",
                        header: "@{R=VIM.Strings;K=VIMWEBDATA_CLM_20; E=js}",
                        flex: 300,
                        sortable: true,
                        dataIndex: "Name",
                        renderer: SW.CloudMonitoring.AddCloudAccount.ChooseInstances.Renderers.renderNameWithManagedStatusIconFactory(SW.VIM.CloudMonitoring.CloudStatusIconProvider.getInstanceStatusIconClass)
                    },
                    {
                        id: "Monitored",
                        header: "@{R=VIM.Strings;K=VIMWEBDATA_CLM_21; E=js}",
                        width: 120,
                        sortable: true,
                        dataIndex: "Monitored",
                        renderer: SW.CloudMonitoring.AddCloudAccount.ChooseInstances.Renderers.renderMonitoredCellTemplate
                    },
                    {
                        id: "InstanceId",
                        header: "@{R=VIM.Strings;K=VIMWEBDATA_CLM_14; E=js}",
                        flex: 100,
                        sortable: true,
                        dataIndex: "InstanceId",
                        tdCls: "chooseCloudInstancesCell"
                    },
                    {
                        id: "State",
                        header: "@{R=VIM.Strings;K=VIMWEBDATA_CLM_4; E=js}",
                        flex: 75,
                        sortable: true,
                        dataIndex: "State",
                        tdCls: "chooseCloudInstancesCell"
                    },
                    {
                        id: "AvailabilityZone",
                        header: "@{R=VIM.Strings;K=VIMWEBDATA_CLM_6; E=js}",
                        flex: 100,
                        sortable: true,
                        dataIndex: "AvailabilityZone",
                        tdCls: "chooseCloudInstancesCell"
                    },
                    {
                        id: "Type",
                        header: "@{R=VIM.Strings;K=VIMWEBDATA_CLM_7; E=js}",
                        flex: 75,
                        sortable: true,
                        dataIndex: "Type",
                        tdCls: "chooseCloudInstancesCell"
                    }
                ];
            };
            var createBottomBar = function (gridStore) {
                return Ext42.create("Ext42.PagingToolbar", {
                    store: gridStore,
                    pageSize: 10,
                    items: [
                        new Ext42.form.Label({ text: "@{R=VIM.Strings;K=VIMWEBDATA_CLM_15; E=js}", style: "margin-left: 5px; margin-right: 5px" }),
                        new Ext42.form.ComboBox({
                            regex: /^\d*$/,
                            store: new Ext42.data.SimpleStore({
                                fields: ["pageSize"],
                                data: [[10], [20], [30], [40], [50], [100]]
                            }),
                            displayField: "pageSize",
                            mode: "local",
                            triggerAction: "all",
                            selectOnFocus: true,
                            width: 50,
                            editable: false,
                            value: 10,
                            listeners: {
                                'select': function (toolbar) {
                                    chooseInstances.instancesGrid.getStore().pageSize = toolbar.lastValue;
                                    chooseInstances.instancesGrid.getStore().load();
                                }
                            }
                        })
                    ],
                    displayInfo: true,
                    displayMsg: "@{R=VIM.Strings;K=VIMWEBDATA_CLM_16; E=js}",
                    emptyMsg: "@{R=VIM.Strings;K=VIMWEBDATA_CLM_17; E=js}"
                });
            };
            var createViewConfig = function () {
                var handleCellClickEvent = function (view, cell, cellIndex, record, row, rowIndex, e) {
                    var clickedDataIndex = view.panel.headerCt.getHeaderAtIndex(cellIndex).dataIndex;
                    if (clickedDataIndex === "Monitored" && e.target.classList.contains("clm-icon-in-cell")) {

                        if (config.isDemo) {
                            demoAction();
                            return;
                        }

                        chooseInstances.updateMonitoredStatus(record, !record.data.Monitored);
                        chooseInstances.refreshFiltersPanel();
                        chooseInstances.refreshGrid();
                    }
                };
                var stylizeRow = function (record) {
                    return record.get("Monitored") ? "monitored" : "not-monitored";
                };

                return {
                    emptyText: "@{R=VIM.Strings;K=VIMWEBDATA_CLM_18;E=js}",
                    deferEmptyText: false,
                    listeners: {
                        cellclick: handleCellClickEvent
                    },
                    getRowClass: stylizeRow,
                    preserveScrollOnRefresh: true
                };
            };

            var tbar = createTopBar();
            var store = createStore();
            var selModel = createSelectionModel();
            var columns = createColumns();
            var bbar = createBottomBar(store);
            var viewConfig = createViewConfig();

            var instancesGrid = new Ext42.grid.GridPanel({
                id: "cloudInstancesGrid",
                tbar: tbar,
                store: store,
                selModel: selModel,
                height: 500,
                flex: 1,
                columns: columns,
                bbar: bbar,

                stripeRows: true,
                stateId: "cloudInstancesGrid",
                viewConfig: viewConfig
            });
            return instancesGrid;
        };

        defineCloudInstancesGrid();
        var grid = createInstancesGrid();
        chooseInstances.instancesGrid = grid;
    };

    function createInstancesGridPanel() {
        return new Ext42.Panel({
            region: "center",
            flex: 1,
            items: [
                chooseInstances.instancesGrid
            ]
        });
    }

    function initializeInstancesGridPanel() {
        var instancesGridPanel = createInstancesGridPanel();
        chooseInstances.instancesGridPanel = instancesGridPanel;
    }

    function createMainPanel() {
        return new Ext42.Panel({
            height: 500,
            layout: "border",
            items: [
                chooseInstances.filtersPanel,
                chooseInstances.instancesGridPanel
            ]
        });
    }

    function initializeMainPanel() {
        var mainPanel = createMainPanel();
        chooseInstances.mainPanel = mainPanel;
        chooseInstances.mainPanel.render("ChooseInstancesPlaceholder");
    }

    chooseInstances.removeChooseCloudInstances = function () {
        chooseInstances.mainPanel.close();
    };

    var isRendered = false;

    chooseInstances.initializeConfig = function (settings) {
        $.extend(config, settings);
    }

    chooseInstances.initializeChooseCloudInstances = function (cloudInstances) {
        if (isRendered) {
            return;
        }

        chooseInstances.cloudInstances = cloudInstances;

        initializeFilters();
        initializeFiltersPanel();
        initializeCloudInstancesGrid();
        initializeInstancesGridPanel();
        initializeMainPanel();
        initializeGridHeaderSummary();

        isRendered = true;
    };

}(SW.Core.namespace("SW.CloudMonitoring.AddCloudAccount.ChooseInstances")));
