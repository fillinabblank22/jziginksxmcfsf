﻿/// <reference path="vim.CloudStatusCellCssClassProvider.js" />
/// <reference path="vim.CloudStatusNameProvider.js" />
/// <reference path="Controls/ManageNodeDialog.js" />
/* global SW */
(function (summary) {

    var CLM = SW.VIM.CloudMonitoring;

    summary.transmissionSpeedFormatterFromkBpsToBps = function (value) {
        return SW.Core.Charts.dataFormatters.bytepersecond(1024 * value, { customPrecision: 1 }, 1, null);
    };

    summary.getCellCssClass = function (managedStatus) {
        var statusName = CLM.CloudStatusNameProvider.getStatusName(managedStatus);
        return CLM.CloudStatusCellCssClassProvider.getCellCssClass(statusName);
    };

    summary.initializeCustomQueryTable = function (resourceId, resourceBodyId, rowsPerPage, searchBoxClientId, searchButtonClientId) {

        function refreshResource() {
            SW.Core.Resources.CustomQuery.refresh(resourceId);
        }

        function calculatePercentage(value) {
            return (Math.round(value * 100) / 100) + "%";
        }

        function dataUnavailableFormatter(value, formatter, suggestAgentInstall, instanceId, accountType) {
            if (value.length !== 0 && value >= 0) {
                return formatter(value);
            }

            var $span = $("<span>")
                .text("@{R=VIM.Strings.CloudMonitoring;K=CLM_CloudInstancesStatusSummary_NotAvailable;E=js}")
                .addClass("CloudNotAvailable");

            if (suggestAgentInstall) {
                $span.attr({
                    rel: "#InstallAgentHint",
                    title: "@{R=VIM.Strings.CloudMonitoring;K=CLM_CloudInstancesStatusSummary_MemoryNAHover_Title;E=js}",
                    "data-instanceid": instanceId,
                    "data-accounttype": accountType
                });
            }

            return $span.prop("outerHTML");
        }

        function updateTooltipDescription(eventObject) {
            var accountType = eventObject.context.attributes["data-accounttype"];
            if (!accountType) return;

            var memoryHover = $(".MemoryHoverPart1");

            if (accountType.value === "aws") {
                memoryHover.text("@{R=VIM.Strings.CloudMonitoring;K=CLM_AwsCloudInstancesStatusSummary_MemoryNAHover_ContentsPart1;E=js}");
            } else {
                if (accountType.value === "azure") {
                    memoryHover.text("@{R=VIM.Strings.CloudMonitoring;K=CLM_AzureCloudInstancesStatusSummary_MemoryNAHover_ContentsPart1;E=js}");
                }
            }
        }

        function manageInstanceAsNode() {
            try {
                var element = $("#cluetip").data("currentHoverElement");
                var instanceId = $(element).attr("data-instanceid");
                // TODO: enclose in namespace
                ManageInstanceAsNodeDialog(instanceId);
            } catch (ex) {
                window.console.log("Could not find InstanceId");
            }
        }

        function getAccountType(type) {
            switch (type) {
                case 1: {
                    return "aws";
                }
                case 2: {
                    return "azure";
                }
                default: {
                    return "";
                }
            }
        }

        function formatName(value, row) {
            var accountType = getAccountType(row[13]);
            var $innerIcon = $("<div>")
                .addClass("instanceName instanceName-icon clm-icon clm-icon-" + accountType + "-instance-" + row[2])
                .html("&nbsp;");
            var $innerText = $("<div>")
                .addClass("instanceName instanceName-text")
                .text(value);

            var $lnk = $("<a>")
                .addClass("instanceName-link")
                .append($innerIcon, $innerText);

            if (accountType.toLowerCase() === "aws") {
                $lnk.attr("href", "/Orion/VIMCloudMonitoring/AwsCloudInstanceDetails.aspx?NetObject=CVM:" + row[1]);
            } else if (accountType.toLowerCase() === "azure") {
                $lnk.attr("href", "/Orion/VIMCloudMonitoring/AzureCloudInstanceDetails.aspx?NetObject=CVZ:" + row[1]);
            } else {
                throw "Unknown AccountType: " + accountType;
            }

            return $lnk.prop("outerHTML");
        }

        var searchBoxSelector = "#" + searchBoxClientId;

        SW.Core.Resources.CustomQuery.initialize({
            uniqueId: resourceId,
            searchTextBoxId: searchBoxClientId,
            searchButtonId: searchButtonClientId,
            initialPage: 0,
            allowSort: true,
            allowPaging: true,
            rowsPerPage: rowsPerPage,
            columnSettings: {
                "Name": {
                    header: "@{R=VIM.Strings.CloudMonitoring;K=CLM_CloudInstancesStatusSummary_Name;E=js}",
                    formatter: formatName,
                    isHtml: true
                },
                "CpuLoad": {
                    header: "@{R=VIM.Strings.CloudMonitoring;K=CLM_CloudInstancesStatusSummary_CpuLoad;E=js}",
                    cellCssClassProvider: function (value, row) {
                        return summary.getCellCssClass(row[5]);
                    },
                    formatter: function (value) {
                        return dataUnavailableFormatter(value, calculatePercentage, false);
                    },
                    isHtml: true
                },
                "PercentMemoryUsed": {
                    header: "@{R=VIM.Strings.CloudMonitoring;K=CLM_CloudInstancesStatusSummary_Memory;E=js}",
                    cellCssClassProvider: function (value, row) {
                        return summary.getCellCssClass(row[7]);
                    },
                    formatter: function (value, row) {
                        return dataUnavailableFormatter(value, calculatePercentage, row[3] == null, row[12], getAccountType(row[13]));
                    },
                    isHtml: true
                },
                "NetworkReceiveRate": {
                    header: "@{R=VIM.Strings.CloudMonitoring;K=CLM_CloudInstancesStatusSummary_Receive;E=js}",
                    cellCssClassProvider: function (value, row) {
                        return summary.getCellCssClass(row[9]);
                    },
                    formatter: function (value) {
                        return dataUnavailableFormatter(value, summary.transmissionSpeedFormatterFromkBpsToBps, false);
                    },
                    isHtml: true
                },
                "NetworkTransmitRate": {
                    header: "@{R=VIM.Strings.CloudMonitoring;K=CLM_CloudInstancesStatusSummary_Transmit;E=js}",
                    cellCssClassProvider: function (value, row) {
                        return summary.getCellCssClass(row[11]);
                    },
                    formatter: function (value) {
                        return dataUnavailableFormatter(value, summary.transmissionSpeedFormatterFromkBpsToBps, false);
                    },
                    isHtml: true
                }
            },
            onLoad: function () {
                $(".CloudNotAvailable").setHint({
                        leftOffset: -8,
                        onActivate: function (eventObject) {
                            updateTooltipDescription(eventObject);
                            return true;
                        }
                });
            }
        });

        SW.Core.Resources.CustomQuery.FilterDropdown.initialize(resourceId, {
            setResultQueryCallback: SW.Core.Resources.CustomQuery.setQuery,
            setResultSearchQueryCallback: SW.Core.Resources.CustomQuery.setSearchQuery,
            filterFinishedCallback: SW.Core.Resources.CustomQuery.refresh
        });

        SW.Core.View.AddOnRefresh(refreshResource, resourceBodyId);

        $(".hidden[id*='Pager'")
            .removeClass("hidden");

        $(searchBoxSelector)
            .attr("placeholder", "@{R=VIM.Strings.CloudMonitoring;K=CLM_CloudInstance_SearchInstances;E=js}");

        $(".manageAsNode")
            .unbind("click")
            .click(function () {
                manageInstanceAsNode();
                return false;
            });
    };

}(SW.Core.namespace("SW.VIM.CloudMonitoring.CloudInstancesStatusSummary")));
