/// <reference path="../../typescripts/typings/jquery.d.ts"/>
/// <reference path="../../typescripts/typings/OrionCore.d.ts"/>
/// <reference path="../../typescripts/typings/OrionMinRegs.d.ts"/>
var SW;
(function (SW) {
    var VIM;
    (function (VIM) {
        var CloudMonitoring;
        (function (CloudMonitoring) {
            var CloudInstanceDetails;
            (function (CloudInstanceDetails) {
                var expanded = {};
                CloudInstanceDetails.selCollapsor = ".Collapsor";
                CloudInstanceDetails.clsCollapsed = "Collapsed";
                function resetState() {
                    Object.keys(expanded).forEach(function (k) { return delete expanded[k]; });
                }
                CloudInstanceDetails.resetState = resetState;
                function getState($item) {
                    $item = getCollapsor($item);
                    var key = getSelector($item);
                    return expanded[key] || false;
                }
                CloudInstanceDetails.getState = getState;
                function setState($item, expand) {
                    $item = getCollapsor($item);
                    var key = getSelector($item);
                    expanded[key] = expand;
                }
                CloudInstanceDetails.setState = setState;
                function getRows($body) {
                    return $body
                        .find("tr")
                        .filter(function () {
                        var $el = $(this);
                        return !($el.is(CloudInstanceDetails.selCollapsor) || $el.find(CloudInstanceDetails.selCollapsor).length > 0);
                    });
                }
                function updateRows($body, show) {
                    var $rows = getRows($body);
                    if (show) {
                        $rows.show();
                    }
                    else {
                        $rows.hide();
                    }
                }
                function getSubSelector($el) {
                    var el = $el.get(0);
                    var $par = $el.parent();
                    var tag = el.tagName;
                    var index = $par.find(tag).index(el);
                    return tag + ":eq(" + index + ")";
                }
                function getSelector($el) {
                    var selector = "";
                    while ($el && $el.length && $el[0].tagName !== "HTML") {
                        var id = $el.attr("id");
                        if (id && id.length) {
                            return (id + " " + selector).trim();
                        }
                        selector = (getSubSelector($el) + " " + selector).trim();
                        $el = $el.parent();
                    }
                    return selector;
                }
                function expandCollapsor($el, expand) {
                    if (expand) {
                        $el.removeClass(CloudInstanceDetails.clsCollapsed);
                    }
                    else {
                        $el.addClass(CloudInstanceDetails.clsCollapsed);
                    }
                    var $body = $el.closest("tbody");
                    updateRows($body, expand);
                    // store state of table
                    setState($el, expand);
                }
                function collapsorClickHandler(evt) {
                    var $el = $(evt.currentTarget);
                    expandCollapsor($el, $el.hasClass(CloudInstanceDetails.clsCollapsed));
                }
                function getTarget($target) {
                    return $target || $(".CloudInstanceDetails .Collapsible");
                }
                function attachHandlers($target) {
                    getTarget($target)
                        .off("click", CloudInstanceDetails.selCollapsor, collapsorClickHandler)
                        .on("click", CloudInstanceDetails.selCollapsor, collapsorClickHandler);
                }
                function getCollapsor($item) {
                    return $item.is(CloudInstanceDetails.selCollapsor)
                        ? $item
                        : $item.find(CloudInstanceDetails.selCollapsor);
                }
                function expandFromInternalState($target) {
                    $target = getTarget($target);
                    getCollapsor($target)
                        .each(function () {
                        var $el = $(this);
                        expandCollapsor($el, getState($el));
                    });
                }
                function init() {
                    attachHandlers();
                    expandFromInternalState();
                }
                CloudInstanceDetails.init = init;
            })(CloudInstanceDetails = CloudMonitoring.CloudInstanceDetails || (CloudMonitoring.CloudInstanceDetails = {}));
        })(CloudMonitoring = VIM.CloudMonitoring || (VIM.CloudMonitoring = {}));
    })(VIM = SW.VIM || (SW.VIM = {}));
})(SW || (SW = {}));
//# sourceMappingURL=vim.CloudInstanceDetails.js.map