﻿SW = SW || {};
SW.VIM = SW.VIM || {};

SW.VIM.ShowMessageDialog = function(titleVal, messageVal) {
    var element = $("<div />");

    element.html(messageVal);

    element.dialog({
        title: titleVal,
        modal: true,
        position: ["middle", "middle"],
        width: 600,
        heigh: "auto",
        minWidth: 500,
        buttons: [
            {
                text: "@{R=CloudMonitoring.Strings;K=Web_Close_Button;E=js}",
                click: function() {
                    $(this).dialog("close");
                }
            }
        ]
    });
}