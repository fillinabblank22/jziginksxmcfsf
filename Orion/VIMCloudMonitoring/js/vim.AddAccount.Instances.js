var clmAddAccountInstances = SW.Core.namespace("CLM.AddAccount.Instances");

(function() {
    var errorManagerProxyObj;

    function wizardStoreProxy(wizardStore) {
        var _wizardStore = wizardStore;

        if (!jQuery.isFunction(wizardStore.setAwsCredential)) {
            console.error("wizardStoreProxy->ctr(): wizardStore missing function setAwsCredential");
            return;
        }

        if (!jQuery.isFunction(wizardStore.getAwsCredential)) {
            console.error("wizardStoreProxy->ctr(): wizardStore missing function getAwsCredential");
            return;
        }


        if (!jQuery.isFunction(wizardStore.setCloudAccountId)) {
            console.error("wizardStoreProxy->ctr(): wizardStore missing function setCloudAccountId");
            return;
        }

        if (!jQuery.isFunction(wizardStore.getCloudAccountId)) {
            console.error("wizardStoreProxy->ctr(): wizardStore missing function getCloudAccountId");
            return;
        }

        this.getAwsCredential = function() {
            return _wizardStore.getAwsCredential();
        };
        this.setAwsCredential = function(accessKeyId, secretAccessKey) {
            return _wizardStore.setAwsCredential(accessKeyId, secretAccessKey);
        };
        this.setCloudAccountId = function(cloudAccountId) {
            _wizardStore.setCloudAccountId(cloudAccountId);
        };
        this.getCloudAccountId = function() {
            return _wizardStore.getCloudAccountId();
        };
    }

    function resourceFactory() {
        this.getVirtualMachines = function(awsCredenitals) {
                return $.ajax({
                    url: "/api/CloudInstanceAdmin/GetAwsInstances",
                    type: "post",
                    dataType: "json",
                    data: awsCredenitals
                });
            },
            this.addVirtualMachines = function(virtualMachines) {
                return $.ajax({
                    url: "/api/CloudInstanceAdmin/AddOrUpdateAwsInstances",
                    type: "post",
                    dataType: "json",
                    data: virtualMachines
                });
            };
    }

    function wizardProxyListener(proxyListener) {
        var _listener = proxyListener;

        if (!jQuery.isFunction(proxyListener.preparedToLeave)) {
            console.error("wizardProxyListener->ctr(): missing function preparedToLeave");
            return;
        }

        if (!jQuery.isFunction(proxyListener.finished)) {
            console.error("wizardProxyListener->ctr(): missing function finished");
            return;
        }

        this.preparedToLeave = function() {
            _listener.preparedToLeave();
        };
        this.finished = function() {
            _listener.finished();
        };
    }

    function progressBarDialogControllerProxy(dialogController) {

        if (dialogController === undefined) {
            console.error("dialogController is undefined");
            return;
        }

        var progressBarDialogControllerObj = dialogController;

        this.setTitle = function(message) {
            progressBarDialogControllerObj.setTitle(message);
        };
        this.setProgressBarInformation = function(message) {
            progressBarDialogControllerObj.setProgressBarInformation(message);
        };
        this.show = function() {
            progressBarDialogControllerObj.show();
        };
        this.hide = function() {
            progressBarDialogControllerObj.hide();
        };
    }

    function errorMangerProxy(errorManager) {

        if (errorManager === undefined) {
            console.error("errorManager is undefined");
            return;
        }

        var _errorManager = errorManager;

        this.show = function(message) {
            _errorManager.show(message);
        };
        this.hide = function() {
            _errorManager.hide();
        };
    }

    function instancesManager(commonProgressBarController, resourceFactory, wizardStore) {

        if (commonProgressBarController === undefined) {
            console.error("cloudManager->ctr(): commonProgressBarController is undefined");
            return;
        }

        if (resourceFactory === undefined) {
            console.error("cloudManager->ctr(): resourceFactory is undefined");
            return;
        }

        if (wizardStore === undefined) {
            console.error("cloudManager->ctr(): wizardStore is undefined");
            return;
        }

        var _commonProgressBarControllerObj = commonProgressBarController;
        var _resourceFactoryObj = resourceFactory;
        var _wizardStoreObj = wizardStore;
        var _wizardProxyListenerObj;

        function getVirtualMachinesInstances(awsCredentials) {


            if (awsCredentials === undefined || awsCredentials == null) {
                console.error("instancesManager->getVirtualMachinesInstances(): awsCredentials is undefined");
                return;
            }

            console.log("load vitual machines");
            _commonProgressBarControllerObj.setTitle("@{R=VIM.Strings;K=VIMWEBDATA_CLM_22;E=js}");
            _commonProgressBarControllerObj.setProgressBarInformation("@{R=VIM.Strings;K=VIMWEBDATA_CLM_22;E=js}");
            _commonProgressBarControllerObj.show();


            _resourceFactoryObj.getVirtualMachines(awsCredentials)
                .then(function(response) {
                    _commonProgressBarControllerObj.setProgressBarInformation("@{R=VIM.Strings;K=VIMWEBDATA_CLM_23;E=js}");
                    _commonProgressBarControllerObj.hide();

                    if (response.Success === false) {
                        errorManagerProxyObj.show(response.ErrorMessage);
                        _commonProgressBarControllerObj.hide();
                        return;
                    }

                    _commonProgressBarControllerObj.hide();

                    SW.CloudMonitoring.AddCloudAccount.ChooseInstances.initializeChooseCloudInstances(response.CloudInstances);

                })
                .error(function (response) {
                    errorManagerProxyObj.show(response.responseText);
                    _commonProgressBarControllerObj.hide();
                });
        };

        function addVirtualMachines(cloudAccountId) {

            function virtualMachines() {
                this.CloudAccountId = cloudAccountId;
                var store = SW.CloudMonitoring.AddCloudAccount.ChooseInstances.instancesGrid.store;
                this.VirtualMachines = $.map(store.proxy.data, function (item) { return Ext42.create(store.proxy.model, item).data; });
            }

            _resourceFactoryObj.addVirtualMachines(new virtualMachines())
                .then(function() {
                    _commonProgressBarControllerObj.setProgressBarInformation("@{R=VIM.Strings;K=VIMWEBDATA_CLM_23;E=js}");
                    setTimeout(_wizardProxyListenerObj.finished(), 1000);
                })
                .error(function(response) {

                    if (response.status === 200) {
                        window.location.reload();
                        return;
                    }

                    errorManagerProxyObj.show(response.responseText);
                    _commonProgressBarControllerObj.hide();
                });
        }

        this.onEntered = function() {
            console.log("instancesManager : onEntered");
            _commonProgressBarControllerObj.hide();
            getVirtualMachinesInstances(_wizardStoreObj.getAwsCredential());
        };
        this.onLeaving = function() {
            console.log("instancesManager : onLeaving");
            _wizardProxyListenerObj.preparedToLeave();
        };
        this.onFinishing = function() {
            console.log("instancesManager : OnFinished");
            var cloudAccountId = _wizardStoreObj.getCloudAccountId();
            addVirtualMachines(cloudAccountId);
        };
        this.setWizardProxyListener = function(listener) {

            if (listener === undefined) {
                console.error("cloudManager->setWizardProxyListener(): listener is undefined");
                return;
            }

            _wizardProxyListenerObj = listener;
        };
    }

    console.log("AddAccount.Instance->ready()");

    if (wizardEventHandlerObj === undefined) {
        console.error("wizardProxyObj is undefined");
        return;
    }

    if (errorManagerObj === undefined) {
        console.error("errorManagerObj is undefined");
        return;
    }

    if (stepEventHandlerObj === undefined) {
        console.error("wizardProxyListenerObj is undefined");
        return;
    }

    if (progressBarDialogControllerObj === undefined) {
        console.error("progressBarDialogControllerObj is undefined");
        return;
    }

    if (wizardStoreObj === undefined) {
        console.error("wizardStoreObj is undefined");
        return;
    }

    var wizardStoreProxyObj = new wizardStoreProxy(wizardStoreObj);

    commonProgressBarControllerObj = new progressBarDialogControllerProxy(progressBarDialogControllerObj);
    errorManagerProxyObj = new errorMangerProxy(errorManagerObj);
    var resourceFactoryObj = new resourceFactory();
    instancesManagerObj = new instancesManager(commonProgressBarControllerObj, resourceFactoryObj, wizardStoreProxyObj);
    instancesManagerObj.setWizardProxyListener(new wizardProxyListener(stepEventHandlerObj));
    wizardEventHandlerObj.addStep(instancesManagerObj);

})(clmAddAccountInstances);