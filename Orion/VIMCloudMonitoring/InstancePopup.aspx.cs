﻿using System;
using System.Web.UI;
using SolarWinds.CloudMonitoring.Shared.Enums;
using SolarWinds.Orion.Core.Common;
using SolarWinds.VIM.CloudMonitoring.Common;
using SolarWinds.VIM.CloudMonitoring.Web.CloudInstances;

public partial class Orion_VIMCloudMonitoring_InstancePopup : Page
{
    private ICloudInstanceDal _cloudInstanceDal;

    public Orion_VIMCloudMonitoring_InstancePopup() : this(new CloudInstanceDal()) { }

    public Orion_VIMCloudMonitoring_InstancePopup(ICloudInstanceDal cloudInstanceDal)
    {
        _cloudInstanceDal = cloudInstanceDal;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string netObject = Request.QueryString["NetObject"];

        if (string.IsNullOrEmpty(netObject))
        {
            throw new Exception($"Incorrect Net Object format for tool tip: {netObject}");
        }

        string[] netObjectIdParts = NetObjectHelper.ParseNetObject(netObject);

        if (netObjectIdParts.Length == 2 && netObjectIdParts[0].Equals(CloudEntityPrefixes.InstancePrefix, StringComparison.InvariantCultureIgnoreCase))
        {
            var virtualMachineId = Convert.ToInt32(netObjectIdParts[1]);

            var provider = _cloudInstanceDal.GetCloudInstanceProvider(virtualMachineId);

            if (!provider.HasValue)
            {
                throw new Exception($"Cloud Provider not found for NetObject: {CloudEntityPrefixes.InstancePrefix}:{virtualMachineId}");
            }

            switch (provider)
            {
                case CloudProvider.Aws:
                    Server.Transfer($"/Orion/VIMCloudMonitoring/AwsCloudInstancePopup.aspx?NetObject={CloudEntityPrefixes.AwsInstancePrefix}:{virtualMachineId}");
                    break;
                case CloudProvider.Azure:
                    Server.Transfer($"/Orion/VIMCloudMonitoring/AzureCloudInstancePopup.aspx?NetObject={CloudEntityPrefixes.AzureInstancePrefix}:{virtualMachineId}");
                    break;
            }
        }
    }
}