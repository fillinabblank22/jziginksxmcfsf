﻿<%@ WebService Language="C#" Class="VolumeChartsService" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.VIM.CloudMonitoring.Web.Charting;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class VolumeChartsService : ChartDataWebService
{
    private const string ReadBandwidthInKibibytesPerSecond = "ReadBandwidthInKibibytesPerSecond";
    private const string WriteBandwidthInKibibytesPerSecond = "WriteBandwidthInKibibytesPerSecond";

    private const string SqlQuery = @"SELECT [TimeStamp], ReadBandwidthInKibibytesPerSecond, WriteBandwidthInKibibytesPerSecond
        FROM VIM_CloudVolumeStatistics
        WHERE VolumeID = @NetObjectId AND [TimeStamp] >= @StartTime AND [TimeStamp] <= @EndTime
        ORDER BY [TimeStamp]";

    [WebMethod]
    public ChartDataResults GetVolumeReadWriteBandwidthDataSeries(ChartDataRequest request)
    {
        if (request == null) throw new ArgumentNullException("request");

        ApplyLimitOnNumberOfNetObjects(request);

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);

        request.CalculateSum = false; // Do not calculate Sum, as we do it later
        var chartData = SimpleLoadData(request, dateRange, SqlQuery, string.Empty, DateTimeKind.Utc, ReadBandwidthInKibibytesPerSecond, WriteBandwidthInKibibytesPerSecond, calculateSubSeries: false);
        var sumSeries = ChartDataGenerator.CreateSumSeries(chartData.DataSeries.ElementAt(0), chartData.DataSeries.ElementAt(1));
        chartData.DataSeries = new List<DataSeries>
            {
                chartData.DataSeries.ElementAt(0),
                chartData.DataSeries.ElementAt(1),
                sumSeries
            };

        return chartData;
    }
}
