﻿<%@ WebService Language="C#" Class="InstanceChartData" %>

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.VIM.CloudMonitoring.Web.Charting;
using SolarWinds.VIM.CloudMonitoring.Web.CloudInstances;
using SolarWinds.VIM.CloudMonitoring.Web.CloudInstances.StoragePerformance;
using SolarWinds.VIM.CloudMonitoring.Web.DAL;

// This construction is used because charts can't support WebApi Controller and we have to use ASMX.
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class InstanceChartData : ChartDataWebService, ICloudInstanceChartsDal
{
    private readonly StoragePerformanceWebService _storagePerformanceWebService;
    private readonly StoragePerformanceComparisonWebService _storagePerformanceComparisonWebService;
    private readonly CpuLoadWebService _cpuLoadWebService;
    private readonly NetworkUtilizationWebService _networkUtilizationWebService;

    public InstanceChartData(SeriesNamesProvider seriesNamesProvider, OptionsFactory optionsFactory)
    {
        if (seriesNamesProvider == null)
        {
            throw new ArgumentNullException(nameof(seriesNamesProvider));
        }
        if (optionsFactory == null)
        {
            throw new ArgumentNullException(nameof(optionsFactory));
        }

        var thresholdsDAL = new ThresholdDAL();
        var thresholdBandsProvider = new ThresholdBandsProvider(thresholdsDAL);
        var cloudInstanceDal = new CloudInstanceDal();
        var storagePerformanceDataSeriesUpdaterFactory = new StoragePerformanceDataSeriesUpdaterFactory(cloudInstanceDal);

        var storagePerformanceDataSeriesUpdater = storagePerformanceDataSeriesUpdaterFactory.Create();

        // Send this service as DAL class into backend.
        _storagePerformanceWebService = new StoragePerformanceWebService(this, seriesNamesProvider, thresholdBandsProvider, optionsFactory, storagePerformanceDataSeriesUpdater);
        _storagePerformanceComparisonWebService = new StoragePerformanceComparisonWebService(this, seriesNamesProvider);

        _cpuLoadWebService = new CpuLoadWebService(this, thresholdBandsProvider);
        _networkUtilizationWebService = new NetworkUtilizationWebService(this);
    }

    public InstanceChartData() : this(new SeriesNamesProvider(), new OptionsFactory())
    {
    }

    ChartDataResults ICloudInstanceChartsDal.SimpleLoadData(ChartDataRequest request, DateRange dateRange, string dataSql, string labelNamesSwql, DateTimeKind dateFormatInDatabase,
        string mainSeriesTag, string secondarySeriesTag)
    {
        return SimpleLoadData(request, dateRange, dataSql, labelNamesSwql, dateFormatInDatabase, mainSeriesTag, secondarySeriesTag);
    }

    ChartDataResults ICloudInstanceChartsDal.SimpleLoadData(ChartDataRequest request, DateRange dateRange, string dataSql, string labelNamesSwql, DateTimeKind dateFormatInDatabase,
        IEnumerable<string> seriesTags)
    {
        var allSeries = DoSimpleLoadData(request, dateRange, dataSql, labelNamesSwql, dateFormatInDatabase, seriesTags, SampleMethod.Average, true);
        var result = new ChartDataResults(allSeries);
        result.AppliedLimitation = request.AppliedLimitation;
        return new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);
    }

    ChartDataResults ICloudInstanceChartsDal.SimpleLoadMinMaxAvgData(ChartDataRequest request, DateRange dateRange, string dataSql, string labelNamesSwql,
        DateTimeKind dateFormatInDatabase, string averageSeriesTag, string minMaxSeriesTag, string secondarySeriesTag = null)
    {
        return SimpleLoadMinMaxAvgData(request, dateRange, dataSql, labelNamesSwql, dateFormatInDatabase, averageSeriesTag, minMaxSeriesTag, secondarySeriesTag);
    }

    public IList<DataSeries> GetData(string sqlQuery, DateRange dateRange, DateTimeKind dateFormatInDatabase, int netObjectId, params string[] seriesTags)
    {
        var parameters = new[]
        {
            new SqlParameter("NetObjectId", netObjectId),
            new SqlParameter("StartTime", dateRange.StartDate),
            new SqlParameter("EndTime", dateRange.EndDate)
        };
        return GetData(sqlQuery, parameters, dateFormatInDatabase, netObjectId.ToString(CultureInfo.InvariantCulture), seriesTags);
    }

    [WebMethod]
    public ChartDataResults GetStoragePerformanceComparisonDataSeries(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        return _storagePerformanceComparisonWebService.LoadStoragePerformanceComparisonChartData(request);
    }

    [WebMethod]
    public ChartDataResults GetStoragePerformanceDataSeries(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        return _storagePerformanceWebService.LoadStoragePerformanceChartData(request);
    }

    [WebMethod]
    public ChartDataResults GetMinMaxAverageCpuLoadDataSeries(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        return _cpuLoadWebService.LoadCpuLoadChartData(request);
    }

    [WebMethod]
    public ChartDataResults GetMinMaxAverageBpsInOutDataSeries(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        return _networkUtilizationWebService.LoadNetworkUtilizationChartData(request);
    }
}
