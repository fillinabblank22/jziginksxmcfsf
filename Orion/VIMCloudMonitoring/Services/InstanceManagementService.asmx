<%@ WebService Language="C#" Class="InstanceManagementService" %>
using System;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.VIM.CloudMonitoring.Common.Models;

/// <summary>
/// Summary description for InstanceManagementService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class InstanceManagementService : WebService
{

    [WebMethod]
    public ManagementActionResult Start(int virtualMachineId)
    {
        return ExecuteManagementAction("StartInstance", virtualMachineId, true);
    }

    [WebMethod]
    public ManagementActionResult Stop(int virtualMachineId, bool force)
    {
        return ExecuteManagementAction(force ? "ForceStopInstance" : "StopInstance", virtualMachineId, true);
    }

    [WebMethod]
    public ManagementActionResult Reboot(int virtualMachineId)
    {
        return ExecuteManagementAction("RebootInstance", virtualMachineId, true);
    }

    [WebMethod]
    public ManagementActionResult Delete(int virtualMachineId, bool removeNode)
    {
        return ExecuteManagementAction(removeNode ? "DeleteInstanceWithNode" : "DeleteInstance", virtualMachineId, true);
    }

    [WebMethod]
    public ManagementActionResult Unmanage(int virtualMachineId)
    {
        return ExecuteManagementAction("Unmanage", virtualMachineId, false);
    }

    [WebMethod]
    public ManagementActionResult Remanage(int virtualMachineId)
    {
        return ExecuteManagementAction("Remanage", virtualMachineId, false);
    }

    [WebMethod]
    public ManagementActionResult PollNow(int virtualMachineId)
    {
        return ExecuteManagementAction("PollNow", virtualMachineId, false);
    }

    private ManagementActionResult ExecuteManagementAction(string swisVerb, int virtualMachineId, bool executePollNow)
    {
        using (var swis = InformationServiceProxy.CreateV3())
        {
            var managementActionResult = swis.Invoke<ManagementActionResult>("Orion.Cloud.Instances", swisVerb, virtualMachineId);
            if (managementActionResult.Success && executePollNow)
            {
                try
                {
                    swis.Invoke<ManagementActionResult>("Orion.Cloud.Instances", "PollNow", virtualMachineId);
                }
                catch (Exception)
                {
                    // Note: We don't want to do anything when scheduling of PollNow fails after management action.
                }
            }
            return managementActionResult;
        }
    }
}
