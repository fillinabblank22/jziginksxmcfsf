﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.CloudMonitoring.Web.CloudVolumes;
using SolarWinds.VIM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_VIMCloudMonitoring_Resources_VolumeDetails_Details : VimBaseResource
{
    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    protected override string DefaultTitle
    {
        get { return VIMWebContent.WEBDATA_CLM_CloudVolumeDetail_ResourceName; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionCloudVolDetRes"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] {typeof (ICloudVolumeProvider)}; }
    }

    public override IEnumerable<Type> SupportedInterfaces
    {
        get { return new[] {typeof (INodeProvider), typeof (ICloudVolumeProvider)}; }
    }

    protected CloudVolume CloudVolume { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        var cloudVolumeProvider = GetInterfaceInstance<ICloudVolumeProvider>();
        if (cloudVolumeProvider != null)
        {
            CloudVolume = cloudVolumeProvider.CloudVolume.CloudVolume;
        }
    }
}