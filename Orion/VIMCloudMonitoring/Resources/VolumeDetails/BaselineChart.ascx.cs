﻿using System;
using System.Collections.Generic;
using System.Globalization;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.CloudMonitoring.Common;
using SolarWinds.VIM.CloudMonitoring.Web.CloudVolumes;
using SolarWinds.VIM.CloudMonitoring.Web.NetObjectConverters;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_VIMCloudMonitoring_Resources_VolumeDetails_BaselineChart : StandardChartResource, IResourceIsInternal
{
    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected override string DefaultTitle
    {
        get { return string.Empty; }
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override string NetObjectPrefix
    {
        get { return Resource.Properties["NetObjectPrefix"] ?? CloudEntityPrefixes.AwsVolumePrefix; }
    }

    public bool IsInternal
    {
        get { return true; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(ICloudVolumeProvider) }; }
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        var volume = new ConvertHelper<CloudVolumeNetObject>(NetObjectPrefix).GetDerivedVimNetObject(this);
        return new List<string>() { volume.Id.ToString(CultureInfo.InvariantCulture) };
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);
    }
}