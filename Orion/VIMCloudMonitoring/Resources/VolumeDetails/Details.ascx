﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Details.ascx.cs" Inherits="Orion_VIMCloudMonitoring_Resources_VolumeDetails_Details" %>
<%@ Import Namespace="Resources" %>

<orion:Include runat="server" File="CloudMonitoring/styles/icons.css" />
<orion:Include runat="server" Module="VIMCloudMonitoring" File="CloudMonitoringIcons.css" />
<orion:Include runat="server" Module="VIMCloudMonitoring" File="styles/Resources.css" />

<orion:resourceWrapper runat="server" ID="Wrapper" CssClass="CloudMonitoring">
    <Content>
        <table class="DetailsTable NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%" runat="server" id="cloudVolumeDetailsTable">
            <tr>
                <td class="PropertyHeader"><%= VIMWebContent.WEBDATA_CLM_CloudVolumeDetail_VolumeId %></td>
                <td>&nbsp;</td>
                <td class="Property"><%= CloudVolume.DiskIdentifier %></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= VIMWebContent.WEBDATA_CLM_CloudVolumeDetail_Type %></td>
                <td>&nbsp;</td>
                <td class="Property">

                    <% if (string.IsNullOrEmpty(CloudVolume.Type)) { %>
                        <span>N/A</span>
                    <% } else { %>
                    <%= CloudVolume.Type %>
                    <% } %>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= VIMWebContent.WEBDATA_CLM_CloudVolumeDetail_Size %></td>
                <td>&nbsp;</td>
                <td class="Property">
                    <% if (CloudVolume.Size == null || CloudVolume.Size == 0) { %>
                        <span>N/A</span>
                    <% } else { %>
                    <%= SolarWinds.VIM.CloudMonitoring.Web.Helpers.FormatHelper.FormatCloudVolumeBytes(CloudVolume.Size.Value) %>
                    <% } %>
                </td>
            </tr>
        </table>
    </Content>
</orion:resourceWrapper>
