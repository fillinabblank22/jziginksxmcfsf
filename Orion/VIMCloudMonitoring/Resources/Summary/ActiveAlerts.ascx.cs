﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.VIM.CloudMonitoring.Web.Resources;
using Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Alerts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsAlertValues.Alarms)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsAlertValues.Warnings)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsAlertValues.Warns)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIMCloudMonitoring_Resources_Summary_ActiveAlerts : CloudBaseResource
{
    private bool showAcknowledgedAlerts;

    protected void Page_Load(object sender, EventArgs e)
    {
        string showAck = Resource.Properties["ShowAcknowledgedAlerts"];
        showAcknowledgedAlerts = !String.IsNullOrEmpty(showAck) && showAck.Equals("true", StringComparison.OrdinalIgnoreCase);

        AlertsTable.ShowAcknowledgedAlerts = showAcknowledgedAlerts;
        AlertsTable.ResourceID = Resource.ID;

        AlertsTable.TriggeringObjectEntityNames = new List<string>
        {
            "Orion.Cloud.Accounts",
            "Orion.Cloud.Aws.Accounts",
            "Orion.Cloud.Azure.Accounts",
            "Orion.Cloud.Instances",
            "Orion.Cloud.Aws.Instances",
            "Orion.Cloud.Azure.Instances",
            "Orion.Cloud.Volumes",
            "Orion.Cloud.Aws.Volumes",
            "Orion.Cloud.Azure.Volumes"
        };

        int rowsPerPage;
        if (!int.TryParse(Resource.Properties["RowsPerPage"], out rowsPerPage))
            rowsPerPage = 5;

        AlertsTable.InitialRowsPerPage = rowsPerPage;
    }

    protected override string DefaultTitle
    {
        get { return VIMWebContent.CloudActiveAlerts_ResourceTitle; }
    }

    public override string SubTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(Resource.SubTitle.Trim()))
                return Resource.SubTitle;

            // if subtitle is not specified
            if (showAcknowledgedAlerts)
                return VIMWebContent.CloudActiveAlerts_AllActiveAlerts_SubTitle;
            else
                return VIMWebContent.CloudActiveAlerts_AllUnacknowledgedAlerts_SubTitle;
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionCoreCloud_ActiveCloudAlerts"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditAllAlerts.ascx"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Ajax;

}