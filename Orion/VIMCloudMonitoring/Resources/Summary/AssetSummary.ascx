﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AssetSummary.ascx.cs" Inherits="Orion_VIMCloudMonitoring_Resources_Summary_AssetSummary" %>
<%@ Import Namespace="Resources" %>

<orion:Include runat="server" Module="VIMCloudMonitoring" File="styles/Resources.css" />
<orion:Include runat="server" Module="VIMCloudMonitoring" File="styles/CloudAssetSummary.css" />
<orion:Include runat="server" Module="VIMCloudMonitoring" File="CloudMonitoringIcons.css" />
<orion:Include runat="server" Module="VIMCloudMonitoring" File="vim.CloudAssetSummary.js" Section="Top" />
<orion:Include runat="server" Module="VIM" File="Charts/Charts.VIM.Chart.Formatters.js" Section="Top" />
<orion:Include runat="server" Module="VIMCloudMonitoring" File="vim.CloudVolumePerformance.js" Section="Top" />


<orion:resourceWrapper runat="server" ID="Wrapper" CssClass="CloudMonitoring">
    <Content>
        <div id="cloudAssetSummaryTable_<%= Resource.ID %>" class="hidden">
            <div id="cloudAssetSummaryTable_<%= Resource.ID %>_Aws_Accordion" class="AccordionHeader" data-target="#cloudAssetSummaryTable_<%= Resource.ID %>_Aws">
                <div class="arrow arrowExpanded"></div>
                <div class="clm-icon clm-icon-aws"></div>
                <div class="instanceGroup-text"><%= VIMWebContent.CloudAssetSummary_Group_Aws %></div>
            </div>
            <table id="cloudAssetSummaryTable_<%= Resource.ID %>_Aws" class="DetailsTable NeedsZebraStripes DetailsTableWithoutSpacing">
                <tbody>
                    <tr>
                        <td class="PropertyHeader WideColumn"><%= VIMWebContent.CloudAssetSummary_TotalInstances %></td>
                        <td class="Property NarrowColumn">
                            <span id="cloudAssetSummaryTable_<%= Resource.ID %>_Aws_TotalInstances"></span>
                        </td>
                        <td class="DetailStatistics">
                            <span class="Category">
                                <span class="CategoryValue" id="cloudAssetSummaryTable_<%= Resource.ID %>_Aws_RunningInstances"></span>
                                <span class="CategoryName"><%= VIMWebContent.CloudAssetSummary_Running %></span>
                            </span>

                            <span class="Category">
                                <span class="CategoryValue" id="cloudAssetSummaryTable_<%= Resource.ID %>_Aws_StoppedInstances"></span>
                                <span class="CategoryName"><%= VIMWebContent.CloudAssetSummary_Stopped %></span>
                            </span>

                            <span class="Category">
                                <span class="CategoryValue" id="cloudAssetSummaryTable_<%= Resource.ID %>_Aws_UnknownInstances"></span>
                                <span class="CategoryName"><%= VIMWebContent.CloudAssetSummary_Unknown %></span>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader"><%= VIMWebContent.CloudAssetSummary_TotalVolumes %></td>
                        <td class="Property">
                            <span id="cloudAssetSummaryTable_<%= Resource.ID %>_Aws_TotalVolumes"></span>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader"><%= VIMWebContent.CloudAssetSummary_TotalStorageCapacity %></td>
                        <td class="Property">
                            <span id="cloudAssetSummaryTable_<%= Resource.ID %>_Aws_TotalStorageCapacity"></span>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </tbody>
            </table>
            <!--Azure Accordion-->
            <div id="cloudAssetSummaryTable_<%= Resource.ID %>_Azure_Accordion" class="AccordionHeader" data-target="#cloudAssetSummaryTable_<%= Resource.ID %>_Azure">
                <div class="arrow arrowExpanded"></div>
                <div class="clm-icon clm-icon-azure"></div>
                <div class="instanceGroup-text"><%= VIMWebContent.CloudAssetSummary_Group_Azure %></div>
            </div>
            <table id="cloudAssetSummaryTable_<%= Resource.ID %>_Azure" class="DetailsTable NeedsZebraStripes DetailsTableWithoutSpacing">
                <tbody>
                    <tr>
                        <td class="PropertyHeader WideColumn"><%= VIMWebContent.CloudAssetSummary_TotalVMs %></td>
                        <td class="Property NarrowColumn">
                            <span id="cloudAssetSummaryTable_<%= Resource.ID %>_Azure_TotalInstances"></span>
                        </td>
                        <td class="DetailStatistics">
                            <span class="Category">
                                <span class="CategoryValue" id="cloudAssetSummaryTable_<%= Resource.ID %>_Azure_RunningInstances"></span>
                                <span class="CategoryName"><%= VIMWebContent.CloudAssetSummary_Running %></span>
                            </span>

                            <span class="Category">
                                <span class="CategoryValue" id="cloudAssetSummaryTable_<%= Resource.ID %>_Azure_StoppedInstances"></span>
                                <span class="CategoryName"><%= VIMWebContent.CloudAssetSummary_Stopped %></span>
                            </span>

                            <span class="Category">
                                <span class="CategoryValue" id="cloudAssetSummaryTable_<%= Resource.ID %>_Azure_UnknownInstances"></span>
                                <span class="CategoryName"><%= VIMWebContent.CloudAssetSummary_Unknown %></span>
                            </span>
                        </td>
                    </tr>
                <tr>
                    <td class="PropertyHeader"><%= VIMWebContent.CloudAssetSummary_TotalVolumes %></td>
                    <td class="Property">
                        <span id="cloudAssetSummaryTable_<%= Resource.ID %>_Azure_TotalVolumes"></span>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VIMWebContent.CloudAssetSummary_TotalStorageCapacityManaged %></td>
                    <td class="Property">
                        <span id="cloudAssetSummaryTable_<%= Resource.ID %>_Azure_TotalStorageCapacity"></span>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </div>

        <script type="text/javascript">
            $(function () {
                var refresh = function () {
                    var resource = new SW.VIM.CloudAssetSummary.Resource("<%= Resource.JavaScriptFriendlyID %>", "#cloudAssetSummaryTable_<%= Resource.JavaScriptFriendlyID %>",<%= ViewLimitationId %>);
                    resource.refresh();
                };
                SW.Core.View.AddOnRefresh(refresh, "cloudAssetSummaryTable_<%= Resource.JavaScriptFriendlyID %>");
                refresh();
            });
        </script>
    </Content>
</orion:resourceWrapper>
