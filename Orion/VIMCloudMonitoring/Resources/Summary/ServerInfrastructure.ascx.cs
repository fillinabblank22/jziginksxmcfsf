﻿using System;
using System.Collections.Generic;
using System.Linq;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.CloudMonitoring.Web;
using SolarWinds.VIM.CloudMonitoring.Web.CloudInstances;
using SolarWinds.VIM.CloudMonitoring.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_VIMCloudMonitoring_Resources_Summary_ServerInfrastructure : CloudBaseResource
{
    private const string CookiePrefixSessionId = "JsTreeSessionCookiePrefixId";

    private static string CookieUniquePrefix
    {
        get { return SessionHelper.GetSessionString(CookiePrefixSessionId, Guid.NewGuid().ToString()); }
    }

    public string JsTreeStateCookieId
    {
        get { return $"{CookieUniquePrefix}_{Resource.JavaScriptFriendlyID}"; }
    }

    public int? ViewLimitationId => ((ViewInfo)Context.Items[typeof(ViewInfo).Name])?.LimitationID;

    protected override string DefaultTitle
    {
        get { return VIMWebContent.VIMWEBCODE_CLM_1; }
    }

    public override string SubTitle
    {
        get
        {
            if (!string.IsNullOrWhiteSpace(Resource.SubTitle))
            {
                return Resource.SubTitle.Trim();
            }

            var subtitle = VIMWebContent.VIMWEBCODE_CLM_2 + " " + GetGroupingLevelsDescription();
            return subtitle;
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionCloudSvcInfSummRes"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/VIMCloudMonitoring/Controls/EditServerInfrastructure.ascx"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override Dictionary<string, string> InitialProperties
    {
        get
        {
            return new Dictionary<string, string>
            {
                {
                    CloudInstanceGroupingConstants.GroupingLevel1PropertyName,
                    CloudInstanceGrouping.CloudAccountName.ToString()
                },
                {
                    CloudInstanceGroupingConstants.GroupingLevel2PropertyName,
                    CloudInstanceGrouping.InstanceRegion.ToString()
                },
                {
                    CloudInstanceGroupingConstants.GroupingLevel3PropertyName,
                    CloudInstanceGrouping.None.ToString()
                }
            };
        }
    }

    private string GetGroupingLevelsDescription()
    {
        var groupingLevelPropertyNames = new List<string>
        {
            CloudInstanceGroupingConstants.GroupingLevel1PropertyName,
            CloudInstanceGroupingConstants.GroupingLevel2PropertyName,
            CloudInstanceGroupingConstants.GroupingLevel3PropertyName
        };

        var groupingLevels = new GroupingLevels<CloudInstanceGrouping>(CloudInstanceGrouping.None);

        var cloudInstanceGroupingLevels =
            groupingLevels.GetGroupingLevelsFromResourceProperties(Resource.Properties, groupingLevelPropertyNames);

        var filteredCloudInstanceGroupings =
            cloudInstanceGroupingLevels.Where(x => x != CloudInstanceGrouping.None).ToList();

        var groupingLevelResources = filteredCloudInstanceGroupings.Select(GetResourceForGroupingLevel);

        return string.Join(", ", groupingLevelResources);
    }

    private string GetResourceForGroupingLevel(CloudInstanceGrouping cloudInstanceGrouping)
    {
        switch (cloudInstanceGrouping)
        {
            case CloudInstanceGrouping.None:
                return VIMWebContent.VIMWEBDATA_CLM_6;
            case CloudInstanceGrouping.CloudAccountName:
                return VIMWebContent.VIMWEBDATA_CLM_4;
            case CloudInstanceGrouping.InstanceRegion:
                return VIMWebContent.VIMWEBDATA_CLM_5;
            case CloudInstanceGrouping.InstanceAvailabilityZone:
                return VIMWebContent.VIMWEBDATA_CLM_8;
            case CloudInstanceGrouping.InstanceType:
                return VIMWebContent.VIMWEBDATA_CLM_7;
            case CloudInstanceGrouping.CloudProvider:
                return VIMWebContent.VIMWEBDATA_CLM_9;
            case CloudInstanceGrouping.ResourceGroup:
                return VIMWebContent.VIMWEBDATA_CLM_10;
            default:
                throw new ArgumentOutOfRangeException($"CloudInstanceGrouping {cloudInstanceGrouping} was not found");
        }
    }
}