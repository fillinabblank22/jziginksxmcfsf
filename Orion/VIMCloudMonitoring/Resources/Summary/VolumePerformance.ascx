﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VolumePerformance.ascx.cs" Inherits="Orion_VIMCloudMonitoring_Resources_Summary_VolumePerformance" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Register TagPrefix="orion" TagName="ResourceSearchControl" Src="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="FilterDropdown" Src="~/Orion/NetPerfMon/Controls/CustomQueryTableFilterDropdown.ascx" %>
<%@ Register TagPrefix="orion" TagName="resourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="vim" TagName="ManageNodeDialog" Src="~/Orion/VIM/Controls/ManageNodeDialog.ascx" %>
<%@ Register TagPrefix="core" TagName="HelpLink" Src="~/Orion/Controls/HelpLink.ascx" %>

<orion:Include runat="server" File="CloudMonitoring/Styles/icons.css"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="CloudMonitoringIcons.css"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="styles/Resources.css" />
<orion:Include runat="server" Module="VIMCloudMonitoring" File="Resources/ResourceEmptyStatePresenter.css" Section="Top" />
<orion:Include runat="server" Module="CloudMonitoring" File="cloud-shared.css"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="Resources/ResourceEmptyStatePresenter.js" Section="Top" />
<orion:Include runat="server" Module="VIMCloudMonitoring" File="Resources/ResourceStateChecker.js" Section="Top" />
<orion:Include runat="server" Module="VIM" File="Charts/Charts.VIM.Chart.Formatters.js" Section="Top" />
<orion:Include runat="server" Module="VIMCloudMonitoring" File="vim.CloudStatusNameProvider.js" Section="Top"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="vim.CloudStatusIconProvider.js" Section="Top"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="vim.CloudVolumePerformance.js" Section="Top" />
<orion:Include runat="server" Module="VIMCloudMonitoring" File="vim.Cloud.utils.js" Section="Top"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="CloudVolumePerformance.css" Section="Top" />
<orion:Include runat="server" Module="VIMCloudMonitoring" File="CloudMonitoringStatuses.css" Section="Top" />
<orion:Include runat="server" Module="CloudMonitoring" File="clm.ui.Tooltip.js" Section="Top"/>

<orion:resourceWrapper runat="server" ID="CloudVolumePerformanceWrapper">
    <Content>
        <div  class="filterControl">
        <orion:FilterDropdown runat="server" ID="FilterDropdown"/>
        </div>
        <div class="searchControl">
            <orion:ResourceSearchControl runat="server" ID="SearchControl" />
        </div>
        <div class="CloudVolumePerformance">
            <orion:CustomQueryTable runat="server" ID="CloudVolumeTable" />
        </div>
        <div id="InstallAgentHintForVolumes" class="hiddenElement">
            <p><%= Resources.VIMWebContent.CloudVolume_DataNA_ForAzureHover_Content %></p>
            <p>
                <asp:LinkButton class="manageAsNode" runat="server" ID="AddAsNodeLink" ><%= Resources.VIMWebContent.CloudVolume_DataNA_ForAzureHover_UseAddNodeWizard %></asp:LinkButton>
            </p>
        </div>
        <div id="ManageAsNodeDialogICMPWarning" style="display: none">
            <core:HelpLink
                CssClass="clm-dialog-link-icmp" runat="server" HelpUrlFragment="OrionCloudInstanceNodeLearnMore"
                LocalizedText="<%$ Resources: VIMWebContent, CloudInstance_ManageAsNode_ICMP_Warning_Link_Text %>"
                HelpDescription="<%$ Resources: VIMWebContent, CloudInstance_ManageAsNode_ICMP_Warning_Link_Text %>"/>
        </div>
        <vim:ManageNodeDialog runat="server"/>
        <script>
            $(document).ready(function() {
                var tryCounter = 0,
                    maxTries = 5;

                function initializeWhenModuleIsReady() {
                    // internal scripts are compiled after this inline code when resource is added via apollo widget drawer
                    // so we need to wait for external js are being invoked before we initialize resource
                    if (!SW.VIM.CloudMonitoring || !SW.VIM.CloudMonitoring.Utils) {
                        if (tryCounter >= maxTries) {
                            throw new Error("Resource could not be init.");
                        }

                        setTimeout(initializeWhenModuleIsReady, 100);
                        tryCounter++;
                        return;
                    }

                    SW.VIM.CloudMonitoring.Utils.initResource(function () {
                        SW.VIM.CloudMonitoring.CloudVolumePerformance.InitializeCustomQueryTable(
                            <%= Resource.JavaScriptFriendlyID %>,
                            "<%= CloudVolumeTable.ClientID %>",
                            <%= Resource.Properties["RowsPerPage"] ?? "10" %>,
                            "<%= SearchControl.SearchBoxClientID %>",
                            "<%= SearchControl.SearchButtonClientID %>"
                        );
                    });
                }

                initializeWhenModuleIsReady();
            });            
        </script>
    </Content>
</orion:resourceWrapper>
