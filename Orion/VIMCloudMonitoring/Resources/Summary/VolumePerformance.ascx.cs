﻿using System;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SolarWinds.VIM.CloudMonitoring.Common;
using SolarWinds.VIM.CloudMonitoring.Web.CloudInstances;
using SolarWinds.VIM.CloudMonitoring.Web.CloudInstances.AWS;
using SolarWinds.VIM.CloudMonitoring.Web.CloudVolumes;
using SolarWinds.VIM.CloudMonitoring.Web.NetObjectConverters;
using InformationServiceProxy = SolarWinds.Orion.Web.InformationService.InformationServiceProxy;
using SolarWinds.VIM.CloudMonitoring.Web;
using SolarWinds.VIM.CloudMonitoring.Web.Resources;
using SolarWinds.CloudMonitoring.Shared.Enums;
using SolarWinds.Orion.Common;
using SolarWinds.VIM.CloudMonitoring.Web.Helpers;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_VIMCloudMonitoring_Resources_Summary_VolumePerformance : CloudBaseResource
{

    private ICloudVolumeDal _volumeDal = new CloudVolumeDal();

    protected override string DefaultTitle => VIMWebContent.VIMWEBCODE_RP0_1;

    private readonly bool _isDemoMode = OrionConfiguration.IsDemoServer;

    public override string SubTitle
    {
        get
        {
            if (!string.IsNullOrWhiteSpace(Resource.SubTitle))
            {
                return Resource.SubTitle.Trim(); 
            }
            return string.Empty;
        }
    }

    public override string HelpLinkFragment => GetVirtualMachineId().HasValue ? "OrionCloudAttachedVolRes" : "OrionCloudVolPerfRes";

    public override string EditControlLocation => "/Orion/VIMCloudMonitoring/Controls/EditVolumePerformance.ascx";

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Ajax;

    public override IEnumerable<Type> SupportedInterfaces => new[] { typeof(IAwsCloudInstanceProvider) };

    protected void Page_Load(object sender, EventArgs e)
    {
        InitializeCloudVolumeTable();

        if (_isDemoMode || !Profile.AllowAdmin)
        {
            PermissionHelper.DisplayInsufficientPermissionsDialogForManageAsNodeWhenClicked(AddAsNodeLink);
        }

        CloudVolumePerformanceWrapper.ShowEditButton = CloudVolumePerformanceWrapper.ShowEditButton && !_isDemoMode;
    }

    private void InitializeCloudVolumeTable()
    {
        const string orderByColumn = "Disk";

        CloudVolumeTable.UniqueClientID = Resource.JavaScriptFriendlyID;
        CloudVolumeTable.OrderBy = orderByColumn;

        InitializeFilterDropdown();
    }

    private void InitializeFilterDropdown()
    {
        var virtualMachineId = GetVirtualMachineId();
        var displayContext = GetDisplayContext(virtualMachineId);

        const string diskSearchSwqlCondition = "Disk LIKE '%${SEARCH_STRING}%'";
        const string filterSwqlCondition = "FilteringType = '${FILTER_VALUE}'";

        FilterDropdown.ResourceID = CloudVolumeTable.UniqueClientID;
        FilterDropdown.Label = VIMWebContent.VIMWEBDATA_CLM_CloudVolumePerformance_FilterDropdownLabel;
        FilterDropdown.SWQL = GetVolumesQuery(virtualMachineId, displayContext);


        FilterDropdown.SearchSWQL = GetQueryFilter(FilterDropdown.SWQL, new List<string> { diskSearchSwqlCondition });
        FilterDropdown.FilterForSearchSWQL = GetQueryFilter(FilterDropdown.SWQL, new List<string> { diskSearchSwqlCondition, filterSwqlCondition });
        FilterDropdown.FilterForSelectSWQL = GetQueryFilter(FilterDropdown.SWQL, new List<string> { filterSwqlCondition });
        
        FilterDropdown.Values = BuildVolumeTypeDropdownSource(displayContext, virtualMachineId);
    }

    private static string GetVolumesQuery(int? cloudInstanceId, DisplayContext displayContext)
    {
        var awsColumnHider = string.Empty;
        var azureColumnHider = string.Empty;
        
        switch (displayContext)
        {
            case DisplayContext.Azure:
                awsColumnHider = "_";
                break;
            case DisplayContext.Aws:
                azureColumnHider = "_";
                break;
        }
        var volumesQuery =
            $"SELECT Disk, [_LinkFor_Disk], _Status, ReadLatencyInMilliseconds as {awsColumnHider}ReadLatencyInMilliseconds, WriteLatencyInMilliseconds as {awsColumnHider}WriteLatencyInMilliseconds, ReadOperationsPerSecond as {awsColumnHider}ReadOperationsPerSecond, WriteOperationsPerSecond as {awsColumnHider}WriteOperationsPerSecond, Size, Type, Managed as {azureColumnHider}Managed, ProviderId as _ProviderId, InstanceId AS _InstanceId, FilteringType as _FilteringType FROM ( " +
            "SELECT v.DisplayName AS Disk, v.DetailsUrl as [_LinkFor_Disk], v.Status as _Status, v.ReadLatencyInMilliseconds, v.WriteLatencyInMilliseconds, v.ReadOperationsPerSecond, v.WriteOperationsPerSecond, v.QueuedOperations, v.Size, v.Type, NULL as Managed, v.VirtualMachineId, a.ProviderId, ci.InstanceId, v.Type as FilteringType FROM Orion.Cloud.Aws.Volumes as v " +
                "INNER JOIN Orion.Cloud.Instances ci ON ci.VirtualMachineID = v.VirtualMachineId INNER JOIN Orion.Cloud.Accounts a ON ci.CloudAccountId = a.Id {0} " +
            "UNION (" +
            $"SELECT v.DisplayName AS Disk, v.DetailsUrl as [_LinkFor_Disk], v.Status as _Status, NULL as ReadLatencyInMilliseconds, NULL as WriteLatencyInMilliseconds, NULL as ReadOperationsPerSecond, NULL as WriteOperationsPerSecond, NULL as QueuedOperations, v.Size, v.Type, CASE IsManaged WHEN 1 THEN '{VIMWebContent.CloudVolumePerformance_Yes}' ELSE '{VIMWebContent.CloudVolumePerformance_No}' END as Managed, v.VirtualMachineId, a.ProviderId, ci.InstanceId, CASE WHEN v.Type IS NULL THEN 'N/A' WHEN v.Type = '' THEN 'N/A' ELSE v.Type END as FilteringType  FROM Orion.Cloud.Azure.Volumes as v " +
                "INNER JOIN Orion.Cloud.Instances ci ON ci.VirtualMachineID = v.VirtualMachineId INNER JOIN Orion.Cloud.Accounts a ON ci.CloudAccountId = a.Id {0} " +
            "))";

        const string cloudInstanceIdSwqlConditionTemplate = "WHERE v.VirtualMachineID = {0}";
        
        var instanceFilter = cloudInstanceId.HasValue
            ? string.Format(cloudInstanceIdSwqlConditionTemplate, cloudInstanceId.Value)
            : null;

       return cloudInstanceId.HasValue
            ? string.Format(volumesQuery, instanceFilter)
            : string.Format(volumesQuery, string.Empty);

    }

    private int? GetVirtualMachineId()
    {
        var instance = new ConvertHelper<CloudInstanceNetObjectBase>(CloudEntityPrefixes.InstancePrefix)
            .TryGetDerivedVimNetObject(this);

        return instance?.CloudInstance?.VirtualMachineId;
    }


    private DisplayContext GetDisplayContext(int? cloudInstanceId)
    {
        var displayContext = DisplayContext.AwsAndAzure;

        var swqlQuery = PrepareSwqlQuery(cloudInstanceId);
        var creator = InformationServiceProxy.CreateV3Creator();

        using (var swisProxy = creator.Create())
        using (var table = swisProxy.Query(swqlQuery))
        {
            if (table.Rows.Count > 0 && table.Rows[0].ItemArray.Length > 0)
            {
                displayContext = (DisplayContext)table.Rows[0].ItemArray[0];
            }
        }

        return displayContext;
    }
    
    private static string PrepareSwqlQuery(int? cloudInstanceId)
    {
        return cloudInstanceId.HasValue ? GetQueryForCloudInstanceDetailsPage(cloudInstanceId.Value) : GetQueryToFindContextOnCloudSummaryPage();
    }

    private static string GetQueryToFindContextOnCloudSummaryPage()
    {
        var findContextForCloudSummaryPage =
            $@"SELECT  
                        CASE WHEN _all_azure = 0 THEN 1 WHEN _all = _all_azure THEN 2 ELSE 0 END as result  
                        FROM (
                        SELECT COUNT (*) AS _all, COUNT (*) - (
                        (SELECT COUNT (*) AS RESULT
                        FROM Orion.Cloud.Volumes v 
                        INNER JOIN Orion.Cloud.Instances ci ON ci.VirtualMachineID = v.VirtualMachineId
                        INNER JOIN Orion.Cloud.Accounts a ON ci.CloudAccountId = a.Id 
                        WHERE a.ProviderId = {(int) CloudProvider.Aws}))
                        AS _all_azure
                        FROM Orion.Cloud.Volumes v 
                        INNER JOIN Orion.Cloud.Instances ci ON ci.VirtualMachineID = v.VirtualMachineId
                        INNER JOIN Orion.Cloud.Accounts a ON ci.CloudAccountId = a.Id )";
        return findContextForCloudSummaryPage;
    }

    private static string GetQueryForCloudInstanceDetailsPage(int cloudInstanceId)
    {
        var queryForAwsOrAzureCloudDetailsPage = $@"SELECT ProviderId AS result
            FROM Orion.Cloud.Volumes v
            INNER JOIN Orion.Cloud.Instances ci ON ci.VirtualMachineID = v.VirtualMachineId
            INNER JOIN Orion.Cloud.Accounts a ON ci.CloudAccountId = a.Id WHERE v.VirtualMachineId = {
                cloudInstanceId
            }";
        return queryForAwsOrAzureCloudDetailsPage;
    }


    private ListItem[] BuildVolumeTypeDropdownSource(DisplayContext displayContext, int? virtualMachineId) => new[] { CreateListItem(VimCloudMonitoringWebContent.Resources_Summary_VeolumePerformance_DisplayAllVolumeTypes, "") }.Concat(_volumeDal.GetVolumeTypes(displayContext, virtualMachineId).Select(t => CreateListItem(t, t))).ToArray();

    private static ListItem CreateListItem(string key, string value)
    {
        if (string.IsNullOrEmpty(key))
        {
            key = "N/A";
            value = "N/A";
        }
        return new ListItem(key, value);
    }

    private static string GetQueryFilter(string baseQuery, IEnumerable<string> filters)
    {
        if (baseQuery == null) { throw new ArgumentNullException(nameof(baseQuery)); }
        if (filters == null) { throw new ArgumentNullException(nameof(filters)); }

        var query = new StringBuilder();

        foreach (var filter in filters)
        {
            if (query.Length == 0)
            {
                query.Append(baseQuery).Append(" WHERE ");
            }
            else
            {
                query.Append(" AND ");
            }

            query.Append(filter);
        }

        return query.Length > 0 ? query.ToString() : baseQuery;
    }
}
