﻿using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.CloudMonitoring.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIMCloudMonitoring_Resources_Summary_AssetSummary : CloudBaseResource
{
    protected override string DefaultTitle => VIMWebContent.CloudAssetSummary_ResourceTitle;

    public override string HelpLinkFragment => "OrionCloudAssetSummRes";

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Ajax;

    public int? ViewLimitationId => ((ViewInfo)Context.Items[typeof(ViewInfo).Name])?.LimitationID;
}