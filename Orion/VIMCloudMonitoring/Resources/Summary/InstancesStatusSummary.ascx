﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InstancesStatusSummary.ascx.cs" Inherits="Orion_VIMCloudMonitoring_Resources_Summary_InstancesStatusSummary" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Register TagPrefix="orion" TagName="ResourceSearchControl" Src="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="FilterDropdown" Src="~/Orion/NetPerfMon/Controls/CustomQueryTableFilterDropdown.ascx" %>
<%@ Register TagPrefix="vim" TagName="ManageNodeDialog" Src="~/Orion/VIM/Controls/ManageNodeDialog.ascx" %>
<%@ Register TagPrefix="core" TagName="HelpLink" Src="~/Orion/Controls/HelpLink.ascx" %>

<orion:Include runat="server" Module="VIM" File="/js/Charts/Charts.VIM.Chart.Formatters.js" Section="Top"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="vim.CloudStatusNameProvider.js" Section="Top"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="vim.CloudStatusCellCssClassProvider.js" Section="Top"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="vim.CloudInstancesStatusSummary.js" Section="Top"/>
<orion:Include runat="server" Module="CloudMonitoring" File="clm.ui.Tooltip.js" Section="Top"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="vim.Cloud.utils.js" Section="Top"/>
<orion:Include runat="server" File="VIMCloudMonitoring/styles/Resources.css"/>
<orion:Include runat="server" File="CloudMonitoring/styles/icons.css"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="CloudMonitoringIcons.css"/>
<orion:Include runat="server" Module="CloudMonitoring" File="cloud-shared.css"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="CloudMonitoringStatuses.css"/>
<orion:Include runat="server" File="VIMCloudMonitoring/js/vim.MessageDialog.js"/>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div class="filterControl">
            <orion:FilterDropdown runat="server" ID="FilterDropdown"/>
        </div>
        <div class="searchControl">
            <orion:ResourceSearchControl runat="server" ID="SearchControl" />
        </div>
        <div class="CloudInstancesStatusSummary">
            <orion:CustomQueryTable runat="server" ID="CloudInstancesStatusTable"/>
        </div>
        <div id="InstallAgentHint" class="hiddenElement">
            <p class="MemoryHoverPart1"></p>
            <p><%= Resources.VIMWebContent.CloudInstancesStatusSummary_MemoryNAHover_ContentsPart2 %></p>
            <p>
                <asp:LinkButton class="manageAsNode" runat="server" ID="AddAsNodeLink" ><%= Resources.VIMWebContent.CloudInstancesStatusSummary_MemoryNAHover_UseAddNodeWizard %></asp:LinkButton>
            </p>
        </div>
        <div id="ManageAsNodeDialogICMPWarning" style="display: none">
            <core:HelpLink
                CssClass="clm-dialog-link-icmp" runat="server" HelpUrlFragment="OrionCloudInstanceNodeLearnMore"
                LocalizedText="<%$ Resources: VIMWebContent, CloudInstance_ManageAsNode_ICMP_Warning_Link_Text %>"
                HelpDescription="<%$ Resources: VIMWebContent, CloudInstance_ManageAsNode_ICMP_Warning_Link_Text %>"/>
        </div>
        <vim:ManageNodeDialog runat="server"/>
        <script>
            $(document).ready(function() {
                var tryCounter = 0,
                    maxTries = 5;

                function initializeWhenModuleIsReady() {
                    // internal scripts are compiled after this inline code when resource is added via apollo widget drawer
                    // so we need to wait for external js are being invoked before we initialize resource
                    if (!SW.VIM.CloudMonitoring || !SW.VIM.CloudMonitoring.Utils) {
                        if (tryCounter >= maxTries) {
                            throw new Error("Resource could not be init.");
                        }

                        setTimeout(initializeWhenModuleIsReady, 100);
                        tryCounter++;
                        return;
                    }

                    SW.VIM.CloudMonitoring.Utils.initResource(function () {
                        SW.VIM.CloudMonitoring.Utils.initResource(function () {
                            SW.VIM.CloudMonitoring.CloudInstancesStatusSummary.initializeCustomQueryTable(
                                <%= Resource.JavaScriptFriendlyID %>,
                                "<%= CloudInstancesStatusTable.ClientID %>",
                                <%= Resource.Properties["RowsPerPage"] ?? "10" %>,
                                "<%= SearchControl.SearchBoxClientID %>",
                                "<%= SearchControl.SearchButtonClientID %>"
                            );
                        });
                    });
                }

                initializeWhenModuleIsReady();
            });
        </script>
    </Content>
</orion:resourceWrapper>
