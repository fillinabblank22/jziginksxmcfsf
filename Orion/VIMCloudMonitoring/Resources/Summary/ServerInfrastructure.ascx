<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ServerInfrastructure.ascx.cs" Inherits="Orion_VIMCloudMonitoring_Resources_Summary_ServerInfrastructure" %>
<%@ Import Namespace="SolarWinds.VIM.CloudMonitoring.Web.CloudInstances" %>

<orion:Include runat="server" Module="VIM" File="jsTree/jquery.jstree.js" Section="Top"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="vim.CloudServerInfrastructure.js" Section="Top"/>
<orion:Include runat="server" Module="VIM" File="jsTree/jquery.cookie.js" Section="Top"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="CloudServerInfrastructure.css" Section="Top"/>
<orion:Include runat="server" File="CloudMonitoring/styles/icons.css"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="CloudMonitoringIcons.css"/>

<orion:resourceWrapper runat="server" ID="Wrapper" CssClass="CloudMonitoring">
    <Content>
        <div id="CloudServerInfrastructure<%= Resource.ID %>" class="clmServerInfrastructureTree"></div>
        <script type="text/javascript">
             $(function() {
                var refresh = function() {
                    var resource = new SW.VIM.CloudServerInfrastructure.Resource("<%= Resource.JavaScriptFriendlyID %>",
                        "#CloudServerInfrastructure<%= Resource.JavaScriptFriendlyID %>",
                        "<%= Resource.Properties[CloudInstanceGroupingConstants.GroupingLevel1PropertyName] %>",
                        "<%= Resource.Properties[CloudInstanceGroupingConstants.GroupingLevel2PropertyName] %>",
                        "<%= Resource.Properties[CloudInstanceGroupingConstants.GroupingLevel3PropertyName] %>",
                        "<%= JsTreeStateCookieId %>",
                        <%= ViewLimitationId %>);
                    resource.refresh();
                };
                SW.Core.View.AddOnRefresh(refresh, "CloudServerInfrastructure<%= Resource.JavaScriptFriendlyID %>");
                refresh();
            });
        </script>
    </Content>
</orion:resourceWrapper>
