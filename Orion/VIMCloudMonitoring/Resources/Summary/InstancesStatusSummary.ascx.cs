﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.CloudMonitoring.Web.Helpers;
using SolarWinds.VIM.CloudMonitoring.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_VIMCloudMonitoring_Resources_Summary_InstancesStatusSummary : CloudBaseResource
{

    private const string BaseSwqlQuery = @"
SELECT
    I.Name,
    I.VirtualMachineId AS _VirtualMachineId,
    {0},
    N.NodeID AS _NodeID,
    I.CpuLoad,
    I.MetricsStatus.CpuLoadStatus AS _CpuLoadStatus,
    N.PercentMemoryUsed,
    I.MetricsStatus.MemoryStatus AS _PercentMemoryUsedStatus,
    I.NetworkReceiveRate,
    I.MetricsStatus.NetworkReceiveRateStatus AS _NetworkReceiveRateStatus,
    I.NetworkTransmitRate,
    I.MetricsStatus.NetworkTransmitRateStatus AS _NetworkTransmitRateStatus,
    I.InstanceId AS _InstanceId,
    A.ProviderId AS _ProviderId
FROM Orion.Cloud.Instances AS I
LEFT JOIN Orion.Cloud.Accounts AS A
    ON A.Id = I.CloudAccountId
LEFT JOIN Orion.Nodes AS N
    ON I.NodeID = N.NodeID";

    private const string OrderByColumn = "I.Name";
    private const int OtherStatusValue = -1;

    private readonly VimManagedStatus[] _statusesInDropdownList =
    {
        VimManagedStatus.Down,
        VimManagedStatus.Critical,
        VimManagedStatus.Warning,
        VimManagedStatus.Unknown,
        VimManagedStatus.Up
    };


    private string BuildSwqlQuery()
    {
        List<Tuple<int, string>> statusMap = Enum.GetValues(typeof(VimManagedStatus))
            .Cast<VimManagedStatus>()
            .Select(value => new Tuple<int, string>((int) value, value.ToString().ToLowerInvariant()))
            .ToList();

        var statusFieldBuilder = new StringBuilder();
        statusFieldBuilder.Append("CASE");
        foreach (var status in statusMap)
        {
            statusFieldBuilder.Append($" WHEN I.Status = {InvariantString(status.Item1)} THEN '{status.Item2}'".ToString(CultureInfo.InvariantCulture));
        }
        statusFieldBuilder.Append(" ELSE '' END as _Status");

        return string.Format(BaseSwqlQuery, statusFieldBuilder);
    }

    protected override string DefaultTitle => VIMWebContent.CloudInstancesStatusSummary_ResourceTitle;

    public override string HelpLinkFragment => "OrionCloudInstanceStatusSummRes";

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Ajax;

    private readonly bool _isDemoMode = OrionConfiguration.IsDemoServer;

    protected void Page_Load(object sender, EventArgs e)
    {
        CloudInstancesStatusTable.UniqueClientID = Resource.JavaScriptFriendlyID;
        CloudInstancesStatusTable.OrderBy = OrderByColumn;

        string swqlQuery = CleanQuery(BuildSwqlQuery());
        string swqlSearch = CleanQuery($"{swqlQuery} WHERE I.Name LIKE '%${{SEARCH_STRING}}%'");

        FilterDropdown.ResourceID = CloudInstancesStatusTable.UniqueClientID;
        FilterDropdown.SWQL = swqlQuery;
        FilterDropdown.SearchSWQL = swqlSearch;
        FilterDropdown.FilterForSearchSWQL = CreateFilteringQuery(swqlSearch);
        FilterDropdown.FilterForSelectSWQL = CreateFilteringQuery(swqlQuery);
        FilterDropdown.Label = $"{VIMWebContent.CloudInstancesStatusSummary_FilterDropdown_Label}:";
        FilterDropdown.Values = BuildInstanceStatusDropdownList();

        if (_isDemoMode || !Profile.AllowAdmin)
        {
            PermissionHelper.DisplayInsufficientPermissionsDialogForManageAsNodeWhenClicked(AddAsNodeLink);
        }
    }

    private ListItem[] BuildInstanceStatusDropdownList()
    {
        var instanceStatusTypeItems = new List<ListItem> { new ListItem(VIMWebContent.CloudInstancesStatusSummary_FilterDropdown_AllItem, "") };
        instanceStatusTypeItems.AddRange(_statusesInDropdownList.Select(status => new ListItem(status.ToString(), InvariantString(status))));
        instanceStatusTypeItems.Add(new ListItem(VIMWebContent.CloudInstancesStatusSummary_FilterDropdown_OtherItem, InvariantString(OtherStatusValue)));

        return instanceStatusTypeItems.ToArray();
    }

    private string CreateFilteringQuery(string query)
    {
        var statusesSet = string.Join(",", _statusesInDropdownList.Select(InvariantString));

        var whereClause = $@"
WHERE CASE
    WHEN ${{FILTER_VALUE}} IN ({statusesSet}) AND I.Status = ${{FILTER_VALUE}} THEN 1
    WHEN ${{FILTER_VALUE}} = {InvariantString(OtherStatusValue)} AND I.Status NOT IN ({statusesSet}) THEN 1
    ELSE 0
END = 1".ToString(CultureInfo.InvariantCulture);

        return CleanQuery(query + whereClause);
    }

    private static string CleanQuery(string query)
    {
        var rxWhite = new Regex(@"\s+");
        return rxWhite.Replace(query ?? "", " ");
    }

    private static string InvariantString(VimManagedStatus value)
    {
        return InvariantString((int) value);
    }

    private static string InvariantString(int value)
    {
        return value.ToString(CultureInfo.InvariantCulture);
    }
}
