﻿using System;
using System.Collections.Generic;
using System.Globalization;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.CloudMonitoring.Common;
using SolarWinds.VIM.CloudMonitoring.Web.CloudInstances;
using SolarWinds.VIM.CloudMonitoring.Web.NetObjectConverters;
using SolarWinds.VIM.CloudMonitoring.Web.NetObjects;

public partial class Orion_VIMCloudMonitoring_Resources_InstanceDetails_BaseLineChart : StandardChartResource, IResourceIsInternal
{
    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }

    protected override string DefaultTitle
    {
        get { return string.Empty; }
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override string NetObjectPrefix
    {
        get { return Resource.Properties["NetObjectPrefix"]; }
    }

    public bool IsInternal
    {
        get { return true; }
    }
    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(ICloudInstanceDetailPage) }; }
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
       var instance = new ConvertHelper<VimNetObjectBase>(CloudEntityPrefixes.InstancePrefix).GetDerivedVimNetObject(this);
       return new List<string> {instance.Id.ToString(CultureInfo.InvariantCulture)};
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);
        OrionInclude.ModuleFile("VIM", "Charts/Charts.Vim.Chart.Formatters.js", OrionInclude.Section.Middle);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Wrapper.ShowEditButton = Wrapper.ShowEditButton && !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
    }
}