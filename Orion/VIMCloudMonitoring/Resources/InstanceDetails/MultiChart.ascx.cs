﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.VIM.CloudMonitoring.Common;
using SolarWinds.VIM.CloudMonitoring.Web;
using SolarWinds.VIM.CloudMonitoring.Web.Charting;
using SolarWinds.VIM.CloudMonitoring.Web.CloudInstances;
using SolarWinds.VIM.CloudMonitoring.Web.NetObjectConverters;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIMCloudMonitoring_Resources_InstanceDetails_MultiChart : CloudMultiChartBaseResource, IResourceIsInternal
{
    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] {typeof (ICloudInstanceDetailPage)}; }
    }

    public bool IsInternal
    {
        get { return true; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        leftPanel.Visible = !ForceDisplayOfNeedsEditPlaceholder;
        rightPanel.Visible = !ForceDisplayOfNeedsEditPlaceholder;

        if (!ForceDisplayOfNeedsEditPlaceholder)
        {
            middlePanel.Style.Add("width", Width + "px");
        }
        Wrapper.ShowEditButton = Wrapper.ShowEditButton && !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);

        if (Entities != null)
        {
            counterRepeater.DataSource = Entities.OfType<CloudMultipleChartEntity>();
            counterRepeater.DataBind();

            if (Entities.Count > 0)
            {
                ForceDisplayOfNeedsEditPlaceholder = false;
            }
        }

        OrionInclude.ModuleFile("VIM", "Charts/Charts.Vim.Chart.Formatters.js", OrionInclude.Section.Middle);
    }

    protected override IEnumerable<VimMultipleChartEntity> LoadEntities()
    {
        var instance = new ConvertHelper<CloudInstanceNetObjectBase>(CloudEntityPrefixes.InstancePrefix).GetDerivedVimNetObject(this);
        var id = instance?.CloudInstance.VirtualMachineId;

        ICloudChartEntityFactory cloudChartEntityFactory = new StoragePerformanceChartEntityFactory();
        return cloudChartEntityFactory.Create((int)id);
    }
}