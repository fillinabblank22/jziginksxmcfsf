﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Details.ascx.cs" Inherits="Orion_VIMCloudMonitoring_Resources_InstanceDetails_Details" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="core" TagName="HelpLink" Src="~/Orion/Controls/HelpLink.ascx" %>

<orion:Include runat="server" Module="VIMCloudMonitoring" File="js/vim.CloudInstanceDetails.gen.js" Section="Top" />
<orion:Include runat="server" File="CloudMonitoring/styles/icons.css" />
<orion:Include runat="server" Module="VIMCloudMonitoring" File="CloudMonitoringIcons.css"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="styles/Resources.css" />
<orion:Include runat="server" Module="VIMCloudMonitoring" File="styles/CloudInstanceDetails.css" />

<orion:resourceWrapper runat="server" ID="Wrapper" CssClass="CloudMonitoring">
    <Content>
        <script type="text/javascript">
            $(function () {
                SW.VIM.CloudMonitoring.CloudInstanceDetails.init();
            });
        </script>
        <table class="DetailsTable NeedsZebraStripes CloudInstanceDetails" border="0" cellpadding="2" cellspacing="0" width="100%">
            <tbody id="cloudInstanceDetailsTable_<%=Resource.ID %>">
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_InstanceName %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%: CloudInstance.Name %></td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_Status %></td>
                    <td class="StatusIcon">
                        <span class="<%= GetStatusIconCssClassElement(CloudInstance.Status) %>">&nbsp;</span>
                    </td>
                    <td class="Property">
                        <span><%= ((SolarWinds.Orion.Web.DisplayTypes.Status) (int)CloudInstance.Status).ToLocalizedString() %></span>
                        <% if (!string.IsNullOrWhiteSpace(CloudInstance.StatusDescription))
                           { %>
                               <br /><span><a target="_blank" href="<%=GetUrlForStatusDescription() %>"><%= CloudInstance.StatusDescription %></a></span>
                          <% } %>  
                    </td>
                </tr>
                  <%
                if (CloudInstance.DisableMonitorApiRequests)
                {
                %>
                <tr>
                    <td colspan=3>
                        <span class="sw-suggestion sw-suggestion-warn">
                        <span class='sw-suggestion-icon'></span>
                            <%= string.Format(VimCloudMonitoringWebContent.CloudInstance_CloudWatchDisabled, "<b>", "</b>", Server.HtmlEncode(CloudInstance.AccountName)) %>
                            <%
                            if (Profile.AllowAdmin)
                            {%>
                                 <a href="/Orion/CloudMonitoring/Admin/EditAccount/Default.aspx?Id=<%=CloudInstance.CloudAccountId%>"><%:string.Format(VimCloudMonitoringWebContent.CloudInstance_CloudWatchEditAccount, CloudInstance.AccountName)%>.</a>
                            <%}%>
                            <br/>
                            <core:HelpLink ID="CloudWatchDisabledHelpLink" runat="server" HelpUrlFragment="OrionCloudCloudWatchAPIDisabled" LocalizedText="CustomText" HelpDescription="<%$ Resources: VimCloudMonitoringWebContent, CloudInstance_CloudWatchDisabledLearnMore %>"/>
                        </span>
                    </td>
                </tr>
                <%}%>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_CloudAccountName %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%: CloudInstance.AccountName %></td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_Region %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= CloudInstance.Region %></td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_AutoScalingGroupName %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetAutoScalingGroupName()) %></td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_State %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= CloudInstance.State %></td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_LastSuccessfulPoll %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= CloudInstance.GetLastSuccessfulPollTimeAsString() %></td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_NextPoll %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= CloudInstance.CalculateNextPoll().ToShortTimeString() %>&nbsp;</td>
                </tr>
            </tbody>
            <tbody class="Collapsible" id="cloudInstanceConfigurationDetailsTable_<%=Resource.ID %>">
                <tr class="Collapsor">
                    <td class="ReportHeader" colspan="3">
                        <%= VimCloudMonitoringWebContent.CloudInstanceDetail_ConfigurationDetails %>
                    </td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_Platform %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetPlatform()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_Architecture %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetArchitecture()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_PublicDnsName %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetPublicDnsName()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_PrivateDnsName %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetPrivateDnsName()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_SubnetId %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetSubnetIds()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_SourceDestCheck %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetSourceDestinationCheck()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_InstanceId %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetInstanceId()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_InstanceType %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetInstanceType()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_LaunchTime %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetInstanceLaunchTimeAsString()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_ImageId %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetImageId()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_AmiLaunchIndex %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetAmiLaunchIndex()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_Monitoring %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetCloudWatchMonitored()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_PlacementGroup %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetPlacementGroup()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_AvailabilityZone %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetAvailabilityZone()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_Virtualization %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetVirtualization()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_ReservationOwnerId %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetReservationOwnerId()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_StateTransitionReason %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetStateTransitionReason()) %>&nbsp;</td>
                </tr>
            </tbody>
            <tbody class="Collapsible" id="cloudInstanceHardwareDetailsTable_<%=Resource.ID %>">
                <tr class="Collapsor">
                    <td class="ReportHeader" colspan="3">
                        <%= VimCloudMonitoringWebContent.CloudInstanceDetail_HardwareDetails %>
                    </td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_RamDiskId %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetRamdiskId()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_KernelId %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetKernelId()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_RootDeviceType %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetRootDeviceType()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_RootDeviceName %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetRootDeviceName()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_EbsOptimized %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= CloudInstance.EbsOptimized %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_Tenancy %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetTenancy()) %>&nbsp;</td>
                </tr>
            </tbody>
            <tbody class="Collapsible" id="cloudInstanceSecurityDetailsTable_<%=Resource.ID %>">
                <tr class="Collapsor">
                    <td class="ReportHeader" colspan="3">
                        <%= VimCloudMonitoringWebContent.CloudInstanceDetail_SecurityDetails %>
                    </td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_IamRole %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetIamRole()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_Owner %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetOwner()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_SecurityGroups %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetSecurityGroupsNames()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_VPCId %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetVpcId()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_KeypairName %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetKeyPairName()) %>&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </Content>
</orion:resourceWrapper>
