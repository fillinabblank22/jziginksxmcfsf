﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Management.ascx.cs" Inherits="Orion_VIMCloudMonitoring_Resources_InstanceDetails_Management" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.VIM.Common" %>

<orion:Include ID="i1" runat="server" File="VIMCloudMonitoring/Styles/Resources.css" />
<orion:Include ID="i2" File="VIMCloudMonitoring/js/InstanceManagement/Management.gen.js" Section="Bottom" runat="server" />
<orion:Include ID="i3" runat="server" File="VIMCloudMonitoring/Styles/CloudInstanceManagement.css" />

<script type="text/javascript">
$(function () {
    var data = {
        container: $('#ResourceID-<%= this.Resource.ID %>'),
        virtualMachineId: <%= this.ManagedInstance?.VirtualMachineId %>,
        instanceName: '<%= HttpUtility.HtmlEncode(ControlHelper.EncodeJsString(this.ManagedInstance?.Name)) %>',
        state: <%= (int) this.InstanceState %>,
        unmanaged: <%= (this.ManagedInstance?.Status == VimManagedStatus.Unmanaged).ToString().ToLower() %>,
        nodeMapped: <%= (this.ManagedInstance?.NodeId != null).ToString().ToLower() %>,
        allowManagement: <%= this.AllowNodeManagement.ToString().ToLower() %>,
        allowUnmanage: <%= this.AllowUnmanage.ToString().ToLower() %>,
        hideWhenNoButtons: true,
        demo: {
            IsDemoMode: function () {
                return <%= SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer.ToString().ToLower() %>;
            },
            DemoModeMessage: function() {
                window.alert("This functionality is not supported in demo mode");
            }
        },
        provider: <%= this.GetCloudProviderValue() %>
    };
    var mgmt = SW.VIM.CloudMonitoring.CloudInstance.Management.initializeManagementResource(data);
});
</script>

<orion:resourceWrapper ID="resourceContent" runat="server">
    <Content>
        <div class="mgmtSection">
            <div class="mgmtHeader"><%= VimCloudMonitoringWebContent.CloudInstanceManagement_CloudInstance %></div>
            <div class="mgmtButtons"></div>
        </div>
    </Content>
</orion:resourceWrapper>
