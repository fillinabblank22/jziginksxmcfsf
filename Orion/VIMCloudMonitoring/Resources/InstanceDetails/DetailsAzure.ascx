﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DetailsAzure.ascx.cs" Inherits="Orion_VIMCloudMonitoring_Resources_InstanceDetails_Details" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="core" TagName="HelpLink" Src="~/Orion/Controls/HelpLink.ascx" %>

<orion:Include runat="server" Module="VIMCloudMonitoring" File="js/vim.CloudInstanceDetails.gen.js" Section="Top" />
<orion:Include runat="server" File="CloudMonitoring/styles/icons.css" />
<orion:Include runat="server" Module="VIMCloudMonitoring" File="CloudMonitoringIcons.css" />
<orion:Include runat="server" Module="VIMCloudMonitoring" File="styles/Resources.css" />
<orion:Include runat="server" Module="VIMCloudMonitoring" File="styles/CloudInstanceDetails.css" />

<orion:resourceWrapper runat="server" ID="Wrapper" CssClass="CloudMonitoring">
    <Content>
        <script type="text/javascript">
            $(function () {
                SW.VIM.CloudMonitoring.CloudInstanceDetails.init();
            });
        </script>
        <table class="DetailsTable NeedsZebraStripes CloudInstanceDetails" border="0" cellpadding="2" cellspacing="0" width="100%">
            <tbody id="cloudInstanceDetailsTable_<%=Resource.ID %>">
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_ResourceGroup %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetResourceGroup()) %></td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_VmName %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%: CloudInstance.Name %></td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_Status %></td>
                    <td class="StatusIcon">
                        <span class="<%= GetStatusIconCssClassElement(CloudInstance.Status) %>">&nbsp;</span>
                    </td>
                    <td class="Property">
                        <span><%= ((SolarWinds.Orion.Web.DisplayTypes.Status) (int)CloudInstance.Status).ToLocalizedString() %></span>
                        <% if (!string.IsNullOrWhiteSpace(CloudInstance.StatusDescription))
                            { %>
                        <br />
                        <span><a target="_blank" href="<%=GetUrlForStatusDescription() %>"><%= CloudInstance.StatusDescription %></a></span>
                        <% } %>  
                    </td>
                </tr>
				<%
                    if (CloudInstance.DisableMonitorApiRequests)
                    {
                %>
                    <tr>
                        <td colspan=3>
                            <span class="sw-suggestion sw-suggestion-warn">
                                <span class='sw-suggestion-icon'></span>
                                <%= string.Format(VimCloudMonitoringWebContent.CloudInstance_AzureMonitorDisabled, HttpUtility.HtmlEncode(CloudInstance.AccountName)) %>
                                <%if (Profile.AllowAdmin)
                                {%>
                                    <a href="/ui/clm/editProperties?providerType=2&accountId=<%=CloudInstance.CloudAccountId%>"><%= string.Format(VimCloudMonitoringWebContent.CloudInstance_AzureMonitorEditAccount, HttpUtility.HtmlEncode(CloudInstance.AccountName))%>.</a>
                                <%}%>
                                <br/>
                                <core:HelpLink ID="CloudAzureMonitorDisabledHelpLink" runat="server" HelpUrlFragment="OrionCloudAzureMonitorAPIDisabled" LocalizedText="CustomText" HelpDescription="<%$ Resources: VimCloudMonitoringWebContent, CloudInstance_AzureMonitorDisabledLearnMore %>"/>
                            </span>
                        </td>
                    </tr>
                <%
                    }
                %>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_CloudAccountName %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= CloudInstance.AccountName %></td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_Location %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetLocation()) %></td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_AvailabilitySet %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetAvailabilitySet()) %></td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_AzureState %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= CloudInstance.State %></td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_LastSuccessfulPoll %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= CloudInstance.GetLastSuccessfulPollTimeAsString() %></td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_NextPoll %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= CloudInstance.CalculateNextPoll().ToShortTimeString() %>&nbsp;</td>
                </tr>
            </tbody>
            <tbody class="Collapsible" id="cloudInstanceConfigurationDetailsTable_<%=Resource.ID %>">
                <tr class="Collapsor">
                    <td class="ReportHeader" colspan="3">
                        <%= VimCloudMonitoringWebContent.CloudInstanceDetail_ConfigurationDetails %>
                    </td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_Platform %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetPlatform()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_PublicDnsName %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetPublicDnsName()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_SubnetId %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetSubnetIds()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_ProvisioningState %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%: CloudInstance.GetProvisioningState() %></td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_VmId %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetInstanceId()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_VmSize %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetInstanceType()) %>&nbsp;</td>
                </tr>

                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_ImageId %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetImageId()) %>&nbsp;</td>
                </tr>
            </tbody>
            <tbody class="Collapsible" id="cloudInstanceSecurityDetailsTable_<%=Resource.ID %>">
                <tr class="Collapsor">
                    <td class="ReportHeader" colspan="3">
                        <%= VimCloudMonitoringWebContent.CloudInstanceDetail_SecurityDetails %>
                    </td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_SubscriptionId %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%: CloudCredentials.SubscriptionId %></td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_SubscriptionName %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%: CloudAccount.SubscriptionName %></td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_SecurityGroups %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetSecurityGroupsNames()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_VirtualNetwork %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetVpcId()) %>&nbsp;</td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= VimCloudMonitoringWebContent.CloudInstanceDetail_KeypairName %></td>
                    <td>&nbsp;</td>
                    <td class="Property"><%= FormatIfNotAvailable(CloudInstance.GetKeyPairName()) %>&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </Content>
</orion:resourceWrapper>
