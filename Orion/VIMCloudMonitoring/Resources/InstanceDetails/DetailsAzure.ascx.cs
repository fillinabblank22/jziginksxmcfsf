﻿using System;
using System.Collections.Generic;
using SolarWinds.CloudMonitoring.Contract.Credentials;
using SolarWinds.CloudMonitoring.Shared.Enums;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.CloudMonitoring.Common;
using SolarWinds.VIM.CloudMonitoring.Web.CloudInstances;
using SolarWinds.VIM.Web.Resources;
using SolarWinds.VIM.CloudMonitoring.Web.CloudAccounts;
using SolarWinds.VIM.CloudMonitoring.Web.CloudAccounts.Azure;
using SolarWinds.VIM.CloudMonitoring.Web.CloudInstances.Azure;
using SolarWinds.VIM.CloudMonitoring.Web.NetObjectConverters;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIMCloudMonitoring_Resources_InstanceDetails_Details : VimBaseResource
{
    private readonly ISharedCredentialManager _credentialManager;
    private readonly CloudAccountsDal<AzureCloudAccountViewModel> _azureCloudAccountsDal;
    private readonly ICloudInstanceStatusIconHelper _cloudInstanceStatusIconHelper;

    public Orion_VIMCloudMonitoring_Resources_InstanceDetails_Details() : this(new CredentialManager(), new AzureCloudAccountsDal(), new CloudInstanceStatusIconHelper())
    {
        
    }

    public Orion_VIMCloudMonitoring_Resources_InstanceDetails_Details(ISharedCredentialManager credentialManager, CloudAccountsDal<AzureCloudAccountViewModel> azureCloudAccountsDal, ICloudInstanceStatusIconHelper cloudInstanceStatusIconHelper)
    {
        if (credentialManager == null) throw new ArgumentNullException(nameof(credentialManager));
        if (azureCloudAccountsDal == null) throw new ArgumentNullException(nameof(azureCloudAccountsDal));
        if (cloudInstanceStatusIconHelper == null)
            throw new ArgumentNullException(nameof(cloudInstanceStatusIconHelper));

        _credentialManager = credentialManager;
        _azureCloudAccountsDal = azureCloudAccountsDal;
        _cloudInstanceStatusIconHelper = cloudInstanceStatusIconHelper;
    }

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.RenderControl;
    private const string AzureThrottlingHelpPageUrl = "http://www.solarwinds.com/documentation/helpLoader.aspx?lang=en&topic=OrionCloudAlertThrottlingCloudInstancePollingAzure.htm";

    protected override string DefaultTitle
    {
        get { return Resources.VimCloudMonitoringWebContent.CloudInstanceDetail_Azure_Resource_Title; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionCloudInstanceDetRes"; }
    }

    public override IEnumerable<Type> RequiredInterfaces => new[] { typeof(IAzureCloudInstanceProvider) };

    public override IEnumerable<Type> SupportedInterfaces => new[] { typeof(IAzureCloudInstanceProvider) };

    protected AzureCloudInstanceViewModel CloudInstance { get; private set; }

    protected AzureCloudAccountViewModel CloudAccount { get; private set; }

    protected AzureCredentials CloudCredentials { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        CloudInstance = GetCloudInstance();
        CloudAccount = GetCloudAccount(CloudInstance);
        CloudCredentials = GetCloudCredentials(CloudAccount);
        Wrapper.ShowEditButton = Wrapper.ShowEditButton && !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
    }

    protected AzureCloudInstanceViewModel GetCloudInstance()
    {
        return new ConvertHelper<AzureCloudInstanceNetObject>(CloudEntityPrefixes.AzureInstancePrefix).GetDerivedVimNetObject(this).CloudInstance;
    }

    protected AzureCloudAccountViewModel GetCloudAccount(AzureCloudInstanceViewModel instance)
    {
        if (instance == null) throw new ArgumentNullException(nameof(instance));
        return _azureCloudAccountsDal.GetCloudAccount(CloudInstance.CloudAccountId);
    }

    protected AzureCredentials GetCloudCredentials(AzureCloudAccountViewModel cloudAccount)
    {
        return _credentialManager.GetCredential<AzureCredentials>(cloudAccount.CredentialId);
    }

    public string GetStatusIconCssClassElement(VimManagedStatus status)
    {
        return _cloudInstanceStatusIconHelper.GetIconCssClassElementForStatus(status, CloudProvider.Azure);
    }

    public string FormatIfNotAvailable(string value)
    {
        if (value == "N/A")
        {
            return $"<span class=\"CloudNotAvailable\">{value}</span>";
        }

        return value;
    }

    public string GetUrlForStatusDescription()
    {
        if (CloudInstance.StatusDescription.ToUpper().Contains("THROTTLING"))
        {
            return AzureThrottlingHelpPageUrl;
        }

        return "#";
    }
}
