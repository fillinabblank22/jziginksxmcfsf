<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ManageAzureVMAsNode.ascx.cs" Inherits="Orion_VIM_Resources_CloudInstanceDetails_ManageInstanceAsNode" %>
<%@ Register TagPrefix="vim" TagName="ManageNodeDialog" Src="~/Orion/VIM/Controls/ManageNodeDialog.ascx" %>
<%@ Register TagPrefix="core" TagName="HelpLink" Src="~/Orion/Controls/HelpLink.ascx" %>
<orion:Include ID="i3" runat="server" File="VIM/Styles/Resources/ManageInstanceAsNode.css"/>
<orion:Include runat="server" File="VIMCloudMonitoring/js/vim.MessageDialog.js"/>
<orion:resourceWrapper ID="Wrapper" runat="server" ShowEditButton="true" CssClass="CloudMonitoring">
    <Content> 
        <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" >
            <Services>
                <asp:ServiceReference path="/Orion/Services/Information.asmx" />
            </Services>
        </asp:ScriptManagerProxy>        

        <script type="text/javascript">
            function ManageInstanceAsNode() {
                ManageInstanceAsNodeDialog("<%= CloudInstance.InstanceId %>");
            }
        </script>

        <div id="manageNodeContents">
            <p><%= Resources.VIMWebContent.ManageAsNode_DescriptionLine1 %></p>
            <p><%= string.Format (Resources.VIMWebContent.ManageAsNode_DescriptionLine2, "<b>", "</b>") %></p>
            <p><core:HelpLink runat="server" HelpUrlFragment="OrionCloudManageInstanceAsNodeResource"
                    LocalizedText="<%$ Resources: VIMWebContent, ManageAsNode_AzureHelpText %>"
                    HelpDescription="<%$ Resources: VIMWebContent, ManageAsNode_AzureHelpText %>"/></p>
            <div id="manageNodeButton" class="sw-btn-bar">
                <orion:LocalizableButton ID="AddAsNodeButton" runat="server" DisplayType="Primary" OnClientClick="ManageInstanceAsNode(); return false;" LocalizedText="CustomText" Text="<%$ Resources: VIMWebContent, ManageAzureVMAsNode_AddButton %>" />
            </div>     
            <%-- #ManageAsNodeDialogICMPWarning div will be transferred to popup dialog --%>
            <div id="ManageAsNodeDialogICMPWarning" style="display: none">
            <core:HelpLink CssClass="clm-dialog-link-icmp" runat="server" HelpUrlFragment="OrionCloudInstanceNodeLearnMore"
                    LocalizedText="<%$ Resources: VIMWebContent, CloudInstance_ManageAsNode_ICMP_Warning_Link_Text %>"
                    HelpDescription="<%$ Resources: VIMWebContent, CloudInstance_ManageAsNode_ICMP_Warning_Link_Text %>"/>
            </div>
        </div>
        <vim:ManageNodeDialog runat="server"/>
    </Content>
</orion:resourceWrapper>