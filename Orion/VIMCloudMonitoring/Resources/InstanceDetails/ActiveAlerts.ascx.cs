﻿using System;
using Resources;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.VIM.CloudMonitoring.Common;
using SolarWinds.VIM.CloudMonitoring.Web.CloudInstances;
using SolarWinds.VIM.CloudMonitoring.Web.NetObjectConverters;
using SolarWinds.VIM.CloudMonitoring.Web.CloudVolumes;  

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Alerts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsAlertValues.Alarms)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsAlertValues.Warnings)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsAlertValues.Warns)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIMCloudMonitoring_Resources_InstanceDetails_ActiveAlerts : BaseResourceControl
{
    private bool showAcknowledgedAlerts;
    private ICloudVolumeDal cloudVolumeDal;
    private CloudInstance cloudInstance;

    private CloudInstance GetCloudInstance()
    {
        return new ConvertHelper<CloudInstanceNetObjectBase>(CloudEntityPrefixes.InstancePrefix).GetDerivedVimNetObject(this).CloudInstance;
    }

    protected ICloudVolumeDal CloudVolumeDal
    {
        get
        {
            if (cloudVolumeDal == null)
            {
                cloudVolumeDal = new CloudVolumeDal();
            }
            return cloudVolumeDal;
        }
        set
        {
            cloudVolumeDal = value;
        }
    }

    protected CloudInstance CloudInstance
    {
        get
        {
            if (cloudInstance == null)
            {
                cloudInstance = GetCloudInstance();
            }
            return cloudInstance;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string showAck = Resource.Properties["ShowAcknowledgedAlerts"];
        showAcknowledgedAlerts = !String.IsNullOrEmpty(showAck) && showAck.Equals("true", StringComparison.OrdinalIgnoreCase);

        AlertsTable.ShowAcknowledgedAlerts = showAcknowledgedAlerts;
        AlertsTable.ResourceID = Resource.ID;
        AlertsTable.TriggeringObjectEntityUris.Add(CloudInstance.SwisUri);

        foreach (string volumeSwisUris in CloudVolumeDal.GetCloudVolumesSwisUrisForInstance(CloudInstance.VirtualMachineId))
        {
            AlertsTable.TriggeringObjectEntityUris.Add(volumeSwisUris);
        }

        int rowsPerPage;
        if (!int.TryParse(Resource.Properties["RowsPerPage"], out rowsPerPage))
            rowsPerPage = 5;

        AlertsTable.InitialRowsPerPage = rowsPerPage;

        wrapper.ShowEditButton = wrapper.ShowEditButton && !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(ICloudInstanceDetailPage) }; }
    }

    protected override string DefaultTitle
    {
        get { return VIMWebContent.CloudInstanceActiveAlerts_ResourceTitle; }
    }

    public override string SubTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(Resource.SubTitle.Trim()))
                return Resource.SubTitle;

            // if subtitle is not specified
            if (showAcknowledgedAlerts)
                return VIMWebContent.CloudInstanceActiveAlerts_AllActiveAlerts_SubTitle;
            else
                return VIMWebContent.CloudInstanceActiveAlerts_AllUnacknowledgedAlerts_SubTitle;
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionCoreCloud_ActiveCloudAlerts"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditAllAlerts.ascx"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Ajax;
}