﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActiveAlerts.ascx.cs" Inherits="Orion_VIMCloudMonitoring_Resources_InstanceDetails_ActiveAlerts" %>
<%@ Register TagPrefix="orion" TagName="AlertsTable" Src="~/Orion/NetPerfMon/Controls/AlertsOnThisEntity.ascx" %>

<orion:resourceWrapper runat="server" ID="wrapper" ShowEditButton="True">
    <Content>
        <orion:AlertsTable runat="server" ID="AlertsTable" />
    </Content>
</orion:resourceWrapper>
