﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.CloudMonitoring.Common;
using SolarWinds.VIM.Web.Resources;
using SolarWinds.VIM.CloudMonitoring.Web.CloudInstances;
using SolarWinds.VIM.CloudMonitoring.Web.CloudInstances.AWS;
using SolarWinds.VIM.CloudMonitoring.Web.NetObjectConverters;
using SolarWinds.VIM.CloudMonitoring.Web.Helpers;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
public partial class Orion_VIM_Resources_CloudInstanceDetails_ManageInstanceAsNode: VimBaseResource
{
    protected CloudInstance CloudInstance { get; private set; }

    protected override string DefaultTitle => VIMWebContent.ManageAsNode_Title;

    public override IEnumerable<Type> RequiredInterfaces => new [] { typeof (IAwsCloudInstanceProvider) };

    public override string DrawerIcon => ResourceDrawerIconName.Summary.ToString();
    

    public override string HelpLinkFragment => "OrionCloudManageInstanceAsNodeResource";

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Ajax;

    private readonly bool _isDemoMode = OrionConfiguration.IsDemoServer;

    protected void Page_Load(object sender, EventArgs e)
    {
        var instance = new ConvertHelper<CloudInstanceNetObjectBase>(CloudEntityPrefixes.InstancePrefix).GetDerivedVimNetObject(this);
        var cloudInstance = instance?.CloudInstance;

        if (cloudInstance == null)
        {
            return;
        }
        CloudInstance = cloudInstance;
     
        if (_isDemoMode || !Profile.AllowAdmin)
        {
            PermissionHelper.DisplayInsufficientPermissionsDialogForManageAsNodeWhenClicked(AddAsNodeButton);
        }
        Wrapper.ShowEditButton = Wrapper.ShowEditButton && !_isDemoMode;
        
    }
}