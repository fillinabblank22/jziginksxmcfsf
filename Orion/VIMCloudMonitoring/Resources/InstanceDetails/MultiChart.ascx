﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultiChart.ascx.cs" Inherits="Orion_VIMCloudMonitoring_Resources_InstanceDetails_MultiChart" %>

<orion:Include runat="server" File="VIM/VIM.css"/>
<orion:Include runat="server" File="VIMCloudMonitoring/Styles/CloudInstancesMultiChart.css"/>
<orion:Include runat="server" File="VIMCloudMonitoring/Styles/CloudMonitoringStatuses.css"/>

<orion:ResourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div class="sw-vim-mc-container sw-clm-mc-container" style="height: <%= Height %>px;">
            <asp:Panel ID="leftPanel" runat="server" CssClass="sw-vim-mc-left-column sw-vim-mc-header sw-clm-mc-metric-name-header sw-clm-mc-header">
                <span class="title">
                    <%=Resources.VIMWebContent.CloudInstanceDetails_StoragePerformance_MetricName%>
                </span>
            </asp:Panel>
            <asp:Panel ID="middlePanel" runat="server" CssClass="sw-vim-mc-middle-column sw-vim-mc-header sw-clm-mc-chart">
                <asp:PlaceHolder runat="server" ID="WrapperContents" ></asp:PlaceHolder>
            </asp:Panel>
            <asp:Panel ID="rightPanel" runat="server" CssClass="sw-vim-mc-right-column sw-vim-mc-header sw-clm-mc-header">
                <span class="title">
                    <%=Resources.VIMWebContent.CloudInstanceDetails_StoragePerformance_ValueFromLastPoll%>
                </span>
            </asp:Panel>
            <asp:Repeater ID="counterRepeater" runat="server">
                <ItemTemplate>
                    <div class="sw-vim-mc-counter" style="height: <%= CounterDivHeight %>px;">
                        <table>
                             <tr>
                                 <td class="name" style="width: <%= FamilyNameColumnWidth %>px">
                                     <span class="sw-clm-mc-family-name"><%# AddLineBreaksToString(Eval("FamilyName").ToString()) %></span>
                                 </td>
                                 <td class="name" style="width: <%= SeriesNameColumnWidth %>px; text-align: right">
                                     <span class="sw-clm-mc-series-name"><%# AddLineBreaksToString(Eval("ShortName").ToString()) %></span>
                                 </td>
                                 <td class="chart"></td>
                                 <td class="lastValue" style="width: <%= LastValueColumnWidth %>px">
                                    <span class="lastValue"></span>
                                 </td>
                             </tr>
                        </table>
                        <hr />
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div> 
    </Content>
</orion:ResourceWrapper> 
