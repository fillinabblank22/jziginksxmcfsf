<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IpAddresses.ascx.cs" Inherits="Orion_VIMCloudMonitoring_Resources_InstanceDetails_IpAddresses" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>

<style type="text/css">
    table#Grid-<%= CustomTable.UniqueClientID %> {
        table-layout: fixed;
    }
</style>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="styles/Resources.css" />
<orion:resourceWrapper runat="server" ID="Wrapper" CssClass="CloudMonitoring CloudInstanceIpAddressTable">
    <Content>
        <orion:Include runat="server" File="VIMCloudMonitoring/js/Resources/CloudInstanceIpAddresses.gen.js" />
        <orion:CustomQueryTable runat="server" ID="CustomTable" />

        <script type="text/javascript">
            $(function () {
                SW.VIM.Resources.CloudInstanceIpAddresses.initialize({
                    resourceId: '<%= ScriptFriendlyResourceId %>',
                    tableId: '<%= CustomTable.ClientID %>',
                    rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "10" %>
                });
            });
        </script>
    </Content>
</orion:resourceWrapper>