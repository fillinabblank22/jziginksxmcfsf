﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.CloudMonitoring.Shared.Enums;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.CloudMonitoring.Common;
using SolarWinds.VIM.CloudMonitoring.Common.Models;
using SolarWinds.VIM.CloudMonitoring.Common.Models.Instances;
using SolarWinds.VIM.CloudMonitoring.Web.CloudAccounts;
using SolarWinds.VIM.CloudMonitoring.Web.CloudInstances;
using SolarWinds.VIM.CloudMonitoring.Web.NetObjectConverters;
using SolarWinds.VIM.Web.Resources;
using CloudInstance = SolarWinds.VIM.CloudMonitoring.Web.CloudInstances.CloudInstance;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
public partial class Orion_VIMCloudMonitoring_Resources_InstanceDetails_Management : VimBaseResource
{
    private readonly ICloudAccountsDal<CloudAccount> _cloudAccountsDal;

    public Orion_VIMCloudMonitoring_Resources_InstanceDetails_Management() : this(new CloudAccountsDal<CloudAccount>())
    {
    }

    public Orion_VIMCloudMonitoring_Resources_InstanceDetails_Management(ICloudAccountsDal<CloudAccount> cloudAccountsDal)
    {
        if (cloudAccountsDal == null)
        {
            throw new ArgumentNullException("cloudAccountsDal");
        }

        _cloudAccountsDal = cloudAccountsDal;
    }

    public override IEnumerable<Type> RequiredInterfaces => new[] { typeof(ICloudInstanceDetailPage) };

    public override IEnumerable<Type> SupportedInterfaces => new[] { typeof(INodeProvider), typeof(ICloudInstanceDetailPage) };

    public override string HelpLinkFragment => "OrionCloudInstanceManagementResource";

    protected override string DefaultTitle => VIMWebContent.CloudInstanceManagementTitle;

    public override string DrawerIcon => ResourceDrawerIconName.Summary.ToString();

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.RenderControl;

    public CloudInstance ManagedInstance => new ConvertHelper<CloudInstanceNetObjectBase>(CloudEntityPrefixes.InstancePrefix).GetDerivedVimNetObject(this).CloudInstance;

    public CloudInstanceState InstanceState => InstanceStateHelper.StateStringToCloudInstanceStateEnum(ManagedInstance?.State ?? string.Empty);

    public bool AllowNodeManagement => Profile.AllowNodeManagement;

    public bool AllowUnmanage => Profile.AllowUnmanage;
    

    protected void Page_Init(object sender, EventArgs e)
    {
        Visible = ManagedInstance != null;
    }
    
    public int GetCloudProviderValue()
    {
        return (int)_cloudAccountsDal.GetCloudAccount(ManagedInstance.CloudAccountId).Provider;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        resourceContent.ShowEditButton = resourceContent.ShowEditButton && !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
    }
    
}
