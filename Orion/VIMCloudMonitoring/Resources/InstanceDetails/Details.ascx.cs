﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.CloudMonitoring.Common;
using SolarWinds.VIM.CloudMonitoring.Web.CloudInstances;
using SolarWinds.VIM.Web.Resources;
using SolarWinds.VIM.CloudMonitoring.Web.CloudInstances.AWS;
using SolarWinds.VIM.CloudMonitoring.Web.NetObjectConverters;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIMCloudMonitoring_Resources_InstanceDetails_Details : VimBaseResource
{
    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.RenderControl;
    private const string AwsThrottlingHelpPageUrl = "http://www.solarwinds.com/documentation/helpLoader.aspx?lang=en&topic=OrionCloudAWSThrottling.htm";

    protected override string DefaultTitle
    {
        get { return Resources.VimCloudMonitoringWebContent.CloudInstanceDetail_AWS_Resource_Title; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionCloudInstanceDetRes"; }
    }

    public override IEnumerable<Type> RequiredInterfaces => new[] { typeof(IAwsCloudInstanceProvider) };

    public override IEnumerable<Type> SupportedInterfaces => new[] { typeof(IAwsCloudInstanceProvider) };

    protected AwsCloudInstanceViewModel CloudInstance { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        CloudInstance = new ConvertHelper<AwsCloudInstanceNetObject>(CloudEntityPrefixes.AwsInstancePrefix).GetDerivedVimNetObject(this).CloudInstance;
        Wrapper.ShowEditButton = Wrapper.ShowEditButton && !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
    }
    
    public string GetStatusIconCssClassElement(VimManagedStatus status)
    {
        var iconHelper = new CloudInstanceStatusIconHelper();
        return iconHelper.GetIconCssClassElementForStatus(status);
    }

    public string FormatIfNotAvailable(string value)
    {
        if (value == "N/A")
        {
            return $"<span class=\"CloudNotAvailable\">{value}</span>";
        }

        return value;
    }

    public string GetUrlForStatusDescription()
    {
        if (CloudInstance.StatusDescription.ToUpper().Contains("THROTTLING"))
        {
            return AwsThrottlingHelpPageUrl;
        }

        return "#";
    }
}
