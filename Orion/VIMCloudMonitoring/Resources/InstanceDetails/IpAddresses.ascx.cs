﻿using System;
using System.Collections.Generic;
using System.Globalization;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.CloudMonitoring.Common;
using SolarWinds.VIM.CloudMonitoring.Web.CloudInstances;
using SolarWinds.VIM.CloudMonitoring.Web.NetObjectConverters;
using SolarWinds.VIM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIMCloudMonitoring_Resources_InstanceDetails_IpAddresses : VimBaseResource
{
    private const string Query = @"
SELECT
    [Version],
    [IPAddress],
    [Type]
FROM
    Orion.VIM.VirtualMachineIPAddresses
WHERE
    VirtualMachineID = {0}
";

    private const string DefaultOrderBy = @"CASE [Type] WHEN 4 THEN 0 ELSE [Type] END";

    public override string DrawerIcon => ResourceDrawerIconName.List.ToString();

    public override IEnumerable<Type> RequiredInterfaces => new[] { typeof(ICloudInstanceDetailPage) };

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    private CloudInstance _cloudInstance;

    private CloudInstance GetCloudInstance()
    {
        var instance = new ConvertHelper<CloudInstanceNetObjectBase>(CloudEntityPrefixes.InstancePrefix).GetDerivedVimNetObject(this);
        return instance?.CloudInstance;
    }

    protected int VirtualMachineId
    {
        get
        {
            if (_cloudInstance == null)
            {
                _cloudInstance = GetCloudInstance();
            }
            return (_cloudInstance == null) ? 0 : _cloudInstance.VirtualMachineId;
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionCloudInstanceIPAddressesResource"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.CloudInstanceIpAddressesTitle; }
    }

    protected int ScriptFriendlyResourceId
    {
        get { return Math.Abs(Resource.ID); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CustomTable.UniqueClientID = ScriptFriendlyResourceId;
        CustomTable.SWQL = String.Format(CultureInfo.InvariantCulture, Query, VirtualMachineId);
        CustomTable.OrderBy = DefaultOrderBy;

        Wrapper.ShowEditButton = Wrapper.ShowEditButton && !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
    }
}
