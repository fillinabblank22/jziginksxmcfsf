﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ChooseInstances.ascx.cs" Inherits="Orion_VIMCloudMonitoring_Controls_ChooseCloudInstances" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="Resources" %>

<orion:Include runat="server" Module="VIMCloudMonitoring" File="vim.AddAccount.Instances.js" Section="Top"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="Common/FilterTreeNodeValueFactory.js" Section="Top"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="Common/FilterTreeNodeValueCollectionFactory.js" Section="Top"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="Common/FilterTreeNodeRetriever.js" Section="Top"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="Common/FilterTreeLeafStatusRetriever.js" Section="Top"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="vim.CloudStatusNameProvider.js" Section="Top"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="vim.CloudStatusIconProvider.js" Section="Top"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="vim.AddAccount.ChooseCloudInstances.Renderers.js" Section="Top"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="vim.AddAccount.ChooseCloudInstances.js" Section="Top"/>
<orion:Include runat="server" Module="CloudMonitoring" File="AddAccount.css"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="ChooseCloudInstances.css"/>
<orion:Include runat="server" File="CloudMonitoring/Styles/icons.css"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="CloudMonitoringIcons.css"/>

<div class="row mainRow" id="ChooseCloudInstances">
    <div class="row mainRow explanationHeader">
        <div class="clm-icon clm-icon-aws-logo"></div>
        <div>
            <%= string.Format(CultureInfo.InvariantCulture, VIMWebContent.VIMWEBCODE_CLM_3, "<strong>", "</strong>") %>
            <strong>
                <a href="../../../Admin/"><%= VIMWebContent.VIMWEBCODE_CLM_4 %></a>
                <span><%= VIMWebContent.VIMWEBCODE_CLM_6 %></span>
                <a href="/ui/clm/accounts"><%= VIMWebContent.VIMWEBCODE_CLM_5 %></a>
            </strong>
        </div>
    </div>
    <div class="explanationHeader chooseCloudInstancesGrid">
        <div id="ChooseInstancesPlaceholder"></div>
    </div>
    <div class="row mainRow explanationHeader">
        <div class="col-md-4">
            <div id="ChooseInstancesErrorMessage" class="alert alert-danger hiddenElement wrapWord"></div>
        </div>
    </div>
</div>