﻿using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;

public partial class Orion_VIMCloudMonitoring_Controls_EditVolumePerformance : BaseResourceEditControl
{
    public override Dictionary<string, object> Properties
    {
        get { return _resourceProperties; }
    }
    private readonly Dictionary<string, object> _resourceProperties = new Dictionary<string, object>();
}