﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.CloudMonitoring.Web;
using SolarWinds.VIM.CloudMonitoring.Web.CloudInstances;

public partial class Orion_VIMCloudMonitoring_Controls_EditServerInfrastructure : BaseResourceEditControl
{
    public override Dictionary<string, object> Properties
    {
        get
        {
            var selectedLevels = GetSelectedGroupingLevels();
            var groupingLevels = new GroupingLevels<CloudInstanceGrouping>(selectedLevels, CloudInstanceGrouping.None);

            var resourceProperties = new Dictionary<string, object>
            {
                {
                    CloudInstanceGroupingConstants.GroupingLevel1PropertyName,
                    groupingLevels.GetGroupingLevel(1)
                },
                {
                    CloudInstanceGroupingConstants.GroupingLevel2PropertyName,
                    groupingLevels.GetGroupingLevel(2)
                },
                {
                    CloudInstanceGroupingConstants.GroupingLevel3PropertyName,
                    groupingLevels.GetGroupingLevel(3)
                }
            };

            return resourceProperties;
        }
    }

    private List<CloudInstanceGrouping> GetSelectedGroupingLevels()
    {
        var groupingLevels = new List<string>
        {
            GroupingLevel1.SelectedValue,
            GroupingLevel2.SelectedValue,
            GroupingLevel3.SelectedValue
        };

        var cloudInstanceGroupingLevels = groupingLevels.Select(EnumUtils.Parse<CloudInstanceGrouping>).ToList();
        cloudInstanceGroupingLevels.RemoveAll(x => x == CloudInstanceGrouping.None);
        return cloudInstanceGroupingLevels;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        var resourceProperties = Resource.Properties;
        GroupingLevel1.SelectedValue = resourceProperties[CloudInstanceGroupingConstants.GroupingLevel1PropertyName];
        GroupingLevel2.SelectedValue = resourceProperties[CloudInstanceGroupingConstants.GroupingLevel2PropertyName];
        GroupingLevel3.SelectedValue = resourceProperties[CloudInstanceGroupingConstants.GroupingLevel3PropertyName];
    }
}