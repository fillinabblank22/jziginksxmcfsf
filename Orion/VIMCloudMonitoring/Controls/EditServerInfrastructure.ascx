﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditServerInfrastructure.ascx.cs" Inherits="Orion_VIMCloudMonitoring_Controls_EditServerInfrastructure" %>
<%@ Import Namespace="Resources" %>

<orion:Include runat="server" Module="VIMCloudMonitoring" File="vim.EditCloudServerInfrastructure.js" Section="Top"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="EditCloudServerInfrastructure.css" Section="Top"/>

<p>
    <b><%= VIMWebContent.VIMWEBDATA_CLM_1 %></b><br/>
    <%= VIMWebContent.VIMWEBDATA_CLM_2 %>
</p>

<p>
    <%= VIMWebContent.VIMWEBDATA_CLM_3 %> 1:<br/>
    <asp:ListBox runat="server" ID="GroupingLevel1" SelectionMode="single" Rows="1">
        <asp:ListItem Text="<%$ Resources:VIMWebContent, VIMWEBDATA_CLM_6 %>" Value="None"/>
        <asp:ListItem Text="<%$ Resources:VIMWebContent, VIMWEBDATA_CLM_4 %>" Value="CloudAccountName"/>
        <asp:ListItem Text="<%$ Resources:VIMWebContent, VIMWEBDATA_CLM_5 %>" Value="InstanceRegion"/>
        <asp:ListItem Text="<%$ Resources:VIMWebContent, VIMWEBDATA_CLM_8 %>" Value="InstanceAvailabilityZone"/>
        <asp:ListItem Text="<%$ Resources:VIMWebContent, VIMWEBDATA_CLM_7 %>" Value="InstanceType"/>
        <asp:ListItem Text="<%$ Resources:VIMWebContent, VIMWEBDATA_CLM_9 %>" Value="CloudProvider"/>
        <asp:ListItem Text="<%$ Resources:VIMWebContent, VIMWEBDATA_CLM_10 %>" Value="ResourceGroup"/>
    </asp:ListBox>
</p>

<p>
    <%= VIMWebContent.VIMWEBDATA_CLM_3 %> 2:<br/>
    <asp:ListBox runat="server" ID="GroupingLevel2" SelectionMode="single" Rows="1">
        <asp:ListItem Text="<%$ Resources:VIMWebContent, VIMWEBDATA_CLM_6 %>" Value="None"/>
        <asp:ListItem Text="<%$ Resources:VIMWebContent, VIMWEBDATA_CLM_4 %>" Value="CloudAccountName"/>
        <asp:ListItem Text="<%$ Resources:VIMWebContent, VIMWEBDATA_CLM_5 %>" Value="InstanceRegion"/>
        <asp:ListItem Text="<%$ Resources:VIMWebContent, VIMWEBDATA_CLM_8 %>" Value="InstanceAvailabilityZone"/>
        <asp:ListItem Text="<%$ Resources:VIMWebContent, VIMWEBDATA_CLM_7 %>" Value="InstanceType"/>
        <asp:ListItem Text="<%$ Resources:VIMWebContent, VIMWEBDATA_CLM_9 %>" Value="CloudProvider"/>
        <asp:ListItem Text="<%$ Resources:VIMWebContent, VIMWEBDATA_CLM_10 %>" Value="ResourceGroup"/>
    </asp:ListBox>
</p>

<p>
    <%= VIMWebContent.VIMWEBDATA_CLM_3 %> 3:<br/>
    <asp:ListBox runat="server" ID="GroupingLevel3" SelectionMode="single" Rows="1">
        <asp:ListItem Text="<%$ Resources:VIMWebContent, VIMWEBDATA_CLM_6 %>" Value="None"/>
        <asp:ListItem Text="<%$ Resources:VIMWebContent, VIMWEBDATA_CLM_4 %>" Value="CloudAccountName"/>
        <asp:ListItem Text="<%$ Resources:VIMWebContent, VIMWEBDATA_CLM_5 %>" Value="InstanceRegion"/>
        <asp:ListItem Text="<%$ Resources:VIMWebContent, VIMWEBDATA_CLM_8 %>" Value="InstanceAvailabilityZone"/>
        <asp:ListItem Text="<%$ Resources:VIMWebContent, VIMWEBDATA_CLM_7 %>" Value="InstanceType"/>
        <asp:ListItem Text="<%$ Resources:VIMWebContent, VIMWEBDATA_CLM_9 %>" Value="CloudProvider"/>
        <asp:ListItem Text="<%$ Resources:VIMWebContent, VIMWEBDATA_CLM_10 %>" Value="ResourceGroup"/>
    </asp:ListBox>
</p>

<div id="EditErrorAlert" class="alert-error"></div>