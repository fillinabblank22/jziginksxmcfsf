﻿using System;
using SolarWinds.VIM.CloudMonitoring.Web.CloudInstances.AWS;
using SolarWinds.VIM.CloudMonitoring.Web.Views;

public partial class Orion_VIMCloudMonitoring_AwsCloudInstancePopup : CloudInstancePopup<AwsCloudInstanceNetObject>
{
    protected override void OnInit(EventArgs e)
    {
        DivWithProgress = div_with_progress;
        base.OnInit(e);
    }

}