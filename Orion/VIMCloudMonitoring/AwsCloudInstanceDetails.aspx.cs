﻿using System;
using SolarWinds.CloudMonitoring.Shared.Enums;
using SolarWinds.VIM.CloudMonitoring.Web.CloudInstances;
using SolarWinds.VIM.CloudMonitoring.Web.CloudInstances.AWS;
using SolarWinds.VIM.CloudMonitoring.Web.Views;

public partial class Orion_VIMCloudMonitoring_AwsCloudInstanceDetails : CloudInstanceViewPage,
    IAwsCloudInstanceProvider, IAwsInstanceDetailPage
{
    public Orion_VIMCloudMonitoring_AwsCloudInstanceDetails() : this(new CloudInstanceStatusIconHelper())
    {
    }

    public Orion_VIMCloudMonitoring_AwsCloudInstanceDetails(
        ICloudInstanceStatusIconHelper cloudInstanceStatusIconHelper) : base(cloudInstanceStatusIconHelper)
    {
    }

    public override string ViewType => "CloudMonitoring CloudInstance Details";

    protected override CloudProvider CloudProvider => CloudProvider.Aws;

    protected string PageTitle { get; set; }

    public AwsCloudInstanceNetObject CloudInstanceNetObject => (AwsCloudInstanceNetObject) NetObject;

    protected override void OnInit(EventArgs e)
    {
        resContainer.DataSource = ViewInfo;
        resContainer.DataBind();
        base.OnInit(e);
    }
}