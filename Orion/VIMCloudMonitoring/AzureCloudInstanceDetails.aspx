﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/VIMCloudMonitoring/VimClmView.master" AutoEventWireup="true"
    CodeFile="AzureCloudInstanceDetails.aspx.cs" Inherits="Orion_VIMCloudMonitoring_AzureCloudInstanceDetails" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="VimPageTitle" runat="Server">
    <link rel="stylesheet" type="text/css" href="Styles/CloudMonitoringStatuses.css">
    <link rel="stylesheet" type="text/css" href="Styles/CloudMonitoringIcons.css">
    <h1>       
        <span class="CloudInstanceDetails-title"><%= ViewInfo.IsSubView ? ViewInfo.ViewGroupTitle : ViewInfo.ViewHtmlTitle%>
            -
            <span class="<%= GetStatusIconCssClassElement(this.CloudInstanceNetObject.CloudInstance.Status) %>">&nbsp;</span>
            <%= HttpUtility.HtmlEncode(this.CloudInstanceNetObject.Name) %></span>
        <%= ViewInfo.IsSubView ? ViewInfo.ViewHtmlTitle : string.Empty%>
    </h1>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="VimMainContentPlaceHolder" runat="server">
    <orion:ResourceHostControl ID="ResourceVMControl2" runat="server">
        <orion:ResourceContainer runat="server" ID="resContainer" />
    </orion:ResourceHostControl>
</asp:Content>
