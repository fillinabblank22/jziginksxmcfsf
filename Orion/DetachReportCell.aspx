<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DetachReportCell.aspx.cs" Inherits="Orion_DetachReportCell" MasterPageFile="~/Orion/OrionMinReqs.master"  %>
<%@ Register TagPrefix="orion" TagName="NetObjectTips" Src="~/Orion/Controls/NetObjectTips.ascx" %>

<asp:Content ContentPlaceHolderID="HeadContent" runat="server">
    <orion:Include File="Resources.css" runat="server" />
    <orion:Include runat="server" File="AsyncView.js" />
    <orion:Include runat="server" File="OrionCore.js" />
    <orion:Include runat="server" File="ReportingDisplay.css" />
    <orion:Include runat="server" File="visibilityObserver.js" />
</asp:Content>

<asp:Content ContentPlaceHolderID="BodyContent" runat="server">
	<orion:NetObjectTips ID="NetObjectTips1" runat="server" />
    <form id="form1" runat="server" class="sw-rpt-frame">

		<asp:ScriptManager id="ScriptManager1" runat="server">
		</asp:ScriptManager>

        <orion:PageLinkGutter id="TopRightPageLinks" runat="server">
            <orion:IconLinkExportToPDF ID="IconLinkExportToPDF1" runat="server" />
        </orion:PageLinkGutter>

		<h1 id="ResourceTitle" runat="server"><%= DefaultSanitizer.SanitizeHtml(CommonWebHelper.EncodeHTMLTags(Page.Title)) %></h1>
        
        <div runat="server" id="divContainer">
        
        </div>
    </form>
</asp:Content>