﻿<%@ WebHandler Language="C#" Class="OidIcons" %>

using System;
using System.Web;
using System.Collections.Generic;
using SolarWinds.Orion.Core.Common;
using System.IO;
using System.Threading;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

public class OidIcons : IHttpHandler
{

    private static Lazy<Dictionary<string, MemoryStream>> _cache =
        new Lazy<Dictionary<string, MemoryStream>>(() => getIcons(), LazyThreadSafetyMode.PublicationOnly);

    /// <summary>
    /// Loads the custom icons from the business layer.
    /// </summary>
    /// <returns></returns>
    private static Dictionary<string, MemoryStream> getIcons()
    {
        ICoreBusinessLayerProxyCreator blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
        using (var proxy = blProxyCreator.Create(handleError))
        {
            return proxy.GetIcons();
        }
    }

    /// <summary>
    /// A lazy-evaluated dictionary serving as a cache for the custom
    /// OID icons.
    /// </summary>
    private static Dictionary<string, System.IO.MemoryStream> Cache
    {
        get
        {
            return _cache.Value;
        }
    }

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "Image/png";
        string oid = string.Empty;
        oid = context.Request.QueryString["oid"];
        if (string.IsNullOrEmpty(oid) == false)
        {
            MemoryStream result = null;
            //Check if the oid has an unique icon in the cache.
            if (Cache.TryGetValue(oid, out result))
            {
                if (result != null)
                {
                    try
                    {
                        //The stream may or may not contain an image. Since it's not readable, we
                        //do a dirty test here.
                        System.Drawing.Image image = System.Drawing.Image.FromStream(result);
                        context.Response.BinaryWrite(result.ToArray());
                    }
                    catch
                    {
                        result = null;
                    }
                }
            }
            //No special icon for the OID, proceed with standard icons.
            if (result == null)
            {
                string stringType = string.Empty;
                stringType = context.Request.QueryString["stringtype"];
                //The default icon is a folder (icon_folder_closed.png). If we have no further
                //info, or no match we'll use it.
                string newUrl = "icon_folder_closed.png";
                if (string.IsNullOrEmpty(stringType) == false)
                {
                    switch (stringType.ToUpperInvariant())
                    {
                        case "SEQUENCE":
                            newUrl = "icon_table_v2.png";
                            break;
                        case "TRAP":
                            newUrl = "icon_traps.png";
                            break;
                        default:
                            if ((stringType.Length > 0) && (!stringType.Equals("unknown")))
                            {
                                newUrl = "icon_point_v2.png";
                            }
                            //Just added for clarity, if we don't find any match then we'll fall
                            //back to group.gif.
                            else
                            {
                                newUrl = "icon_folder_closed.png";
                            }
                            break;
                    }
                    if (stringType.ToUpperInvariant().Contains("ENTRY"))
                    {
                        newUrl = "icon_key_v2.png";
                    }
                    string isTabular = context.Request.QueryString["istabular"];
                    //Tabular oids are tables, entries and columns
                    if (string.IsNullOrEmpty(isTabular) == false)
                    {
                        //If tabular, but not a table or entry, then show the column icon.
                        if(bool.TrueString.Equals(isTabular) && 
                            stringType.ToUpperInvariant().Contains("SEQUENCE") == false &&
                            stringType.ToUpperInvariant().Contains("ENTRY") == false)
                        {
                            newUrl = "icon_column_v2.png";
                        }
                    }
                    
                }
                //Use server transfer to save a request.
                context.Server.Transfer(string.Format("/Orion/Images/OidPicker/{0}", newUrl));
            }
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    private static void handleError(Exception ex)
    {
        throw ex;
    }
}