<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" MasterPageFile="~/Orion/OrionMinReqs.master" %>
<%@ Register TagPrefix="orion" TagName="PageFooter" Src="~/Orion/Controls/PageFooter.ascx" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
	<!-- loginsig(09470EE361E54F3DB730000176BA16B2) used w/ ajax for session end detection -->
	<style type="text/css">
/* rules designed to exist only in login page */
body { background-color: #EEEEEE; }
body::after { content: ""; background: url("/orion/images/Login/pattern-login-screen.png"); opacity: 0.5; top: 0; left: 0; bottom: 0; right: 0; position: absolute; z-index: -1; }

* { box-sizing: border-box; }
#gutter { width: 400px; margin: auto; position: relative; padding-top: 50px; }
.sw-pg-login { padding: 0; }
.sw-pg-login #dialog { background: #fff; position: relative; padding: 60px 60px 22px 60px; margin-bottom: 0px; }
.sw-pg-login #dialog label { color: #1d1d1d;font-size: 14px; font-weight: bold; display: block; zoom: 1; margin: 7px 0; }
.sw-pg-login #dialog > label { margin-top: 0px !important; }
.sw-pg-login form.sw-pg-login #dialog #evalMsg { padding-left: 0; margin-top: 4px; }
.sw-pg-login .sw-pg-suggestion { font-size: 11px; color: #8f8f8f; padding: 0; margin: -5px 0 10px 0; }
.sw-pg-login .sw-login-btn > a { width: 100%; text-align: center; font-weight: bold; font-size: 12px; padding: 5px 10px; margin-top: 15px; }
.sw-pg-login #SiteLoginText { margin-top: 15px; font-weight: bold; text-align: justify; }
.sw-pg-hint-inner { border: 1px solid #abd5e3; background: #e6eff3;}
.sw-pg-hint-body { margin: 10px 10px 10px 10px; padding-left: 30px; background: url(/orion/images/Login/i-icon.png) top left no-repeat; color: #000000;}
.sw-pg-errortext {width: 100%; color: #d50000; font-weight: bold; }
input[type=text], input[type=password] { padding: 5px;font-size: 14px; width: 100%; margin-bottom: 15px; }
.sw-logo { text-align: center; padding-bottom: 30px; }
.sw-login-dialog-container {
    -moz-box-shadow: 0 1px 5px 0 #b9b9b9;
    -webkit-box-shadow: 0 1px 5px 0 #b9b9b9;
    box-shadow: 0 1px 5px 0 #b9b9b9;
}

.sw-login-dialog-container > div:last-child { padding-bottom: 60px !important; }

.sw-login-error-message { padding: 5px 60px 0px; background: #FFFFFF; }
.sw-login-error-message-inner { border: 1px solid #d50000; background: #ffe4e0;}
.sw-login-error-message-body { margin: 10px 10px 10px 10px; padding-left: 30px; color: red; font-weight: bold;
    background: url(/orion/images/error_28x28.png) top left no-repeat; background-size: 20px 20px; }

.sw-saml-login { text-align: center; background: #FFFFFF; height: auto; padding: 5px 60px 25px 60px; font-size: 14px; overflow-x: hidden; margin-top: -5px; }
.sw-saml-login a { width: 100%; padding: 5px 0; border: 2px solid #2D7A93; font-weight: bold; font-size: 12px; margin-top: 9px; word-wrap: break-word; margin-bottom: 0; }
	.sw-saml-login a:hover { color: #204a63 !important; }
.sw-saml-login .or-line { border-top: 1px solid #D5D5D5; }
.sw-saml-login .or-word { padding: 0 15px; background: #FFFFFF; display: inline-block; color: #8F8F8F; position: relative; bottom: 9px; }

.sw-brand-top-divider { margin: 0; padding: 0; height: 5px; width: 100%; display: flex; }
.sw-brand-top-divider div { height: 5px; margin: 0; padding: 0; flex-shrink: 0; border: none; }
.sw-brand-top-divider .line-1 { width: 60px; background-color: #666666; }
.sw-brand-top-divider .line-2 { width: 90px; background-color: #b8d757; }
.sw-brand-top-divider .line-3 { width: 20px; background-color: #fbe04b; }
.sw-brand-top-divider .line-4 { width: 100%; flex: 1 1 100%; background-color: #666666; }
.sw-brand-top-divider .line-5 { width: 50px; background-color: #04c9d7; }
.sw-brand-top-divider .line-6 { width: 60px; background-color: #666666; }

.sw-logout-message { padding: 5px 60px 0px; background: #FFFFFF; }
.sw-eval-iframe { width:100%; border:none; background: transparent; }

</style>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="BodyContent">
	<div id="container">
		<div id="content">
            <%if (IsEvalRedirectionNeeded) { %>
                <div id="gutter">
                    <div class="sw-brand-top-divider">
                        <div class="line-1"></div>
                        <div class="line-2"></div>
                        <div class="line-3"></div>
                        <div class="line-4"></div>
                        <div class="line-5"></div>
                        <div class="line-6"></div>
                    </div>
                    <iframe class="sw-eval-iframe" src="<%= DefaultSanitizer.SanitizeHtml(EvalRedirectionUrl) %>" scrolling="auto" height="800px"></iframe>
                </div>
            <% } else { %>
        <form runat="server" class="sw-app-region sw-pg-login">
            <div id="gutter">
                <div class="sw-login-dialog-container">
                    <div class="sw-brand-top-divider">
                        <div class="line-1"></div>
                        <div class="line-2"></div>
                        <div class="line-3"></div>
                        <div class="line-4"></div>
                        <div class="line-5"></div>
                        <div class="line-6"></div>
                    </div>
                    <div id="dialog">
	                    <div class="sw-logo"> 
		                    <img id="logo" src="<%= DefaultSanitizer.SanitizeHtml(SiteLogoUri) %>" width="180" alt="SolarWinds" />
	                    </div>
                        <label for="username"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_AK0_165) %></label>
                        <asp:TextBox ng-non-bindable ID="Username" tabindex="1" automation="username" runat="server" />
                        <div class="sw-pg-suggestion"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_42) %></div>

                        <asp:RequiredFieldValidator CssClass="sw-pg-errortext" SetFocusOnError="true" ControlToValidate="Username" Display="Dynamic" runat="server">
                            <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_43) %></span>
                        </asp:RequiredFieldValidator>
                        
                        <div id="PasswordBox" class="sw-password-box" runat="server">
                            <label for="password"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_40) %></label>
                            <asp:TextBox ng-non-bindable ID="Password" TextMode="password" tabindex="2" automation="password" autocomplete="new-password" runat="server" />
                        </div>
                        
                        <div id="ChangePasswordBox" class="sw-change-password" runat="server" visible="false">
                            <label for="newpassword"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.LoginPage_Password) %></label>
                            <asp:TextBox ID="NewPassword" TextMode="password" tabindex="2" automation="newpassword" autocomplete="new-password" runat="server" />
                            <asp:RequiredFieldValidator ID="RequiredNewPassword" runat="server" ControlToValidate="NewPassword" Display="Dynamic">
                                <div class="sw-pg-errortext">
                                    <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.LoginPage_PasswordRequired_Error) %></span>
                                </div>
                            </asp:RequiredFieldValidator>


                            <label for="newpasswordconfirm"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.LoginPage_ConfirmPassword) %></label>
                            <asp:TextBox ID="NewPasswordConfirm" TextMode="password" tabindex="3" automation="newpasswordconfirm" autocomplete="new-password" runat="server" />
                            <asp:RequiredFieldValidator ID="RequiredNewPasswordConfirm" runat="server" ControlToValidate="NewPasswordConfirm" Display="Dynamic">
                                <div class="sw-pg-errortext">
                                    <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.LoginPage_ConfirmPassRequired_Error) %></span>
                                </div>
                            </asp:RequiredFieldValidator>
                            
                            <asp:CompareValidator ID="CompareValidator" runat="server" ControlToValidate="NewPasswordConfirm" ControlToCompare="NewPassword"  Display="Dynamic">
                                <div class="sw-pg-errortext">
                                    <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.LoginPage_ComparePasswords_Error) %></span>
                                </div>
                            </asp:CompareValidator>
                        </div>

	                    <div ID="phMessage" class="sw-pg-errortext" EnableViewState="false" runat="server" visible="false"></div>
	                    <div id="SessionExpired" class="sw-pg-errortext" EnableViewState="false" visible="false" runat="server">
                            <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_44) %></span>
	                    </div>
    	                <div id="CookiesNotWorking" class="sw-pg-errortext" style="display:none;">
                            <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_45) %></span>
    	                </div>
    	                <noscript><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_46) %></noscript>

							<div class="sw-login-btn" automation="Login">
								<orion:LocalizableButton ID="LoginButton" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PCC_47 %>" DisplayType="Primary" tabindex="3" runat="server" />
							</div>
						</div>
						<asp:Panel ID="samlLogin" CssClass="sw-saml-login" Visible="False" runat="server">
							<div class="or-line">
								<div class="or-word">
									<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_KG0_2) %>
								</div>
								<asp:HyperLink ID="samlLoginLink" CssClass="sw-btn sw-btn-secondary" TabIndex="4" runat="server"></asp:HyperLink>
							</div>
						</asp:Panel>
						<asp:Panel ID="loginErrorMessagePanel" CssClass="sw-login-error-message" Visible="False" runat="server">
							<div>
								<div class="sw-login-error-message-inner">
									<div class="sw-login-error-message-body">
										<asp:Literal ID="loginErrorTitle" runat="server"></asp:Literal>
										<asp:Literal ID="loginErrorText" runat="server"></asp:Literal>
										<asp:HyperLink ID="loginErrorLink" Target="_blank" runat="server"></asp:HyperLink>
									</div>
								</div>
							</div>
						</asp:Panel>
						<div id="LogoutMessage" class="sw-logout-message" Visible="False" runat="server">
							<div id="tip">
								<div class="sw-pg-hint-inner">
									<div class="sw-pg-hint-body">
										<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Login_LogoutSuccessful) %>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="SiteLoginText"><%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.DAL.WebSettingsDAL.SiteLoginText) %></div>
				</div>
			</form>
            <% } %>
            <div id="container-after">&nbsp;</div>
		</div>
    <orion:PageFooter runat="server" />
</div>
<script type="text/javascript">
(function(){
  var ThisPage = SW.Core.ThisPage = SW.Core.ThisPage||{};

  ThisPage.Init = function(){
    var tip = $('#tip'), elems = $('#dialog input');

    elems.keypress(function(e){
        if(e.which == 13) {
            e.preventDefault();
            e.target.form.submit();
            return false;
        }
    });

    if( tip[0] ) $(window).load(function(){
        var username = elems[0];
        if( username.value == '' ) {
            username.value = 'admin';
            elems[1].focus();
        }
    });

    if( !SW.Core.Cookie.Get('<%=TestCookie_Name%>') )
        $('#CookiesNotWorking').show();
  };
})();

		$(SW.Core.ThisPage.Init);
	</script>
</asp:Content>
