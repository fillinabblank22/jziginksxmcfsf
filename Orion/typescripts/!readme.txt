This is place for yout typescrit files and files with typings.
Good to know:
1. you can compile all ts files by '/depot/dev/Main/Orion/Core/Build/TypeScript/compile_all_ts_files.cmd' or compile_all_ts_files_debug.cmd to have map files for debugging
2. avoid submitting generated js code to perforce, it will be recompiled anyway
3. structure your ts files to folders
4. use bundler to bundle multiple js files to one
