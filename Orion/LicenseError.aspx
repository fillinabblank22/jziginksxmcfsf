<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMinReqs.master" Title="Orion Website Error" ValidateRequest="false" %>
<%@ Register TagPrefix="orion" TagName="PageHeader" Src="~/Orion/Controls/PageHeader.ascx" %>
<%@ Register TagPrefix="orion" TagName="PageFooter" Src="~/Orion/Controls/PageFooter.ascx" %>

<script runat="server">
	protected string Message;
	
	protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        // We might have already started outputing the banner.  Let's start over.
        Response.Clear();

        Exception ex = Server.GetLastError();

        if (null != ex)
        {
            if (ex is HttpUnhandledException)
                ex = ex.InnerException;

			Message = ex.Message;

			Server.ClearError();
        }
    }
</script>

<asp:Content ID="Content3" ContentPlaceHolderID="BodyContent" Runat="Server">
<div id="container">
    <div id="content">
        <orion:PageHeader Minimalistic="true" runat="server" />

<form runat="server" class="sw-app-region">

    <h1><%= DefaultSanitizer.SanitizeHtml(Title) %></h1>
    <h2><%= DefaultSanitizer.SanitizeHtml(Message) %></h2>

</form>
        
    </div>
    <div id="container-after">&nbsp;</div>
</div>
    <orion:PageFooter ID="PageFooter1" runat="server" />
</asp:Content>
