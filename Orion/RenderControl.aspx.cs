﻿using System;
using System.Collections;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Collections.Generic;
using Resources;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Repository;
using SolarWinds.Orion.Web.Services;

public partial class Orion_RenderControl : System.Web.UI.Page
{
    public NetObject NetObject { get; private set; }
    public ResourceInfo Resource { get; private set; }
    System.Web.UI.Control ControlPlaceHolder { get; set; }
    Dictionary<string, object> JsonData { get; set; }
    private static readonly SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();

    const string ParamControl = "Control"; // path or namespace
    const string ParamResourceID = "ResourceID"; // int
    const string ParamResourceWrap = "ResourceWrap"; // bool
    const string ParamResourceWidth = "ResourceWidth"; // int
    const string ParamNetObject = "NetObject"; // string (net object format)
    const string MarkerStart = "<!-- 337f2ca00bdd4ee39d7df8c7dbcfe259:start -->";
    const string MarkerEnd = "<!-- 337f2ca00bdd4ee39d7df8c7dbcfe259:end -->";
    private static Regex reg = new Regex("(<div class=\"\\s*ResourceWrapper[^\"]*\")|(<div class='\\s*ResourceWrapper[^'])", RegexOptions.IgnoreCase);

    private static void OnRedirect(object sender, EventArgs ea)
    {
        var response = sender as HttpResponse;
        if (response == null) return;
        
        var context = typeof(HttpResponse).GetProperty("Context", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).GetValue(response) as HttpContext;
        if (context == null) return;
        
        var page = context.Handler as Page;
        if (page == null) return;
        
        var renderControl = page as Orion_RenderControl;
        if (renderControl == null) return;
        
        // ok, we're in rendercontrol! let's render code for redirection
        var redirectLocation = response.RedirectLocation;
        
        response.RedirectLocation = null;
        
        response.Clear();
        response.StatusCode = 200;
        
        response.Write(@"<script type=""text/sw-javascript"">");
        response.Write(@"window.location = """);
        response.Write(HttpUtility.JavaScriptStringEncode(redirectLocation));
        response.Write(@"""");
        response.Write(@"</script>");
        
        response.End();
    }

    static Orion_RenderControl()
    {
        // I'm not sure why this is not accessible from everywhere
        var evt = typeof(HttpResponse).GetEvent("Redirecting", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
        evt.GetAddMethod(true).Invoke(null, new object[] { new EventHandler(OnRedirect) });
    }

    string GetParam(string name)
    {
        string ret = Request.QueryString[name];

        if (ret != null)
            return HttpUtility.HtmlEncode(ret);

        
        object fetch;

        if (JsonData.TryGetValue(name, out fetch))
            return fetch == null ? null : fetch as string;

        if (JsonData.TryGetValue("IgnoreResourceID", out fetch) && Boolean.Parse(fetch.ToString()))
            return null;

        return HttpUtility.HtmlEncode(Request.Form[name]);
    }

    protected override void OnInit(EventArgs e)
    {
        if (Request.HttpMethod == "POST" && Request.InputStream.Length > 0)
        {
            using (var reader = new StreamReader(Page.Request.InputStream, Encoding.UTF8, true))
            {
                try
                {
                // probably noone sends those anyway
                    JsonData = PropertySetter.LoadJsonData(reader);
                }
                catch
                {
                    JsonData = null; 
                }
            }
        }
        
        if (JsonData == null)
            JsonData = new Dictionary<string, object>();

        var addedResourceId = AddResourceIfPossible();
        var paramResourceId = GetParam(ParamResourceID) ?? string.Empty;
        int resourceId;
        int.TryParse(paramResourceId, out resourceId);
        
        ControlPlaceHolder = new PlaceHolderWithID();
        ControlPlaceHolder.ID = "Resource" + resourceId;

        Page.Form.Controls.Add(ControlPlaceHolder);

        if (InitWebResource(addedResourceId) == false)
        {
            // not a web resource. must be another control.

            string ctrl = GetParam(ParamControl);

            if (string.IsNullOrEmpty(ctrl))
                throw new ArgumentException("Page requires " + ParamControl + " or " + ParamResourceID + " in query string");

            System.Web.UI.Control controlToRender = null;

            if (ctrl.StartsWith("SolarWinds.Orion.Web"))
            {
                // todo: modular plugin
                controlToRender = Activator.CreateInstance("OrionWeb", ctrl).Unwrap() as Control;
            }
            else
            {
                controlToRender = LoadControl(ctrl);
            }

            ApplyPropertiesAndAttributes(controlToRender);

            ControlPlaceHolder.Controls.Add(controlToRender);
        }

        base.OnInit(e);

        Response.Cache.SetCacheability(HttpCacheability.NoCache);
    }

    private int? AddResourceIfPossible()
    {
        if (OrionConfiguration.IsDemoServer)
        {
            return null;
        }

        if (!JsonData.ContainsKey("viewId") || !JsonData.ContainsKey("columnNumber") || !JsonData.ContainsKey("position") || !JsonData.ContainsKey("viewId"))
        {
            return null;
        }

        if (!Profile.AllowCustomize)
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_VB0_317) });

        var viewId = JsonData["viewId"] as int?;
        var columnNumber = JsonData["columnNumber"] as int?;
        var position = JsonData["position"] as int?;
        var resourcePaths = JsonData["resourcePaths"] as ArrayList;

        if (viewId == null || columnNumber == null || position == null || resourcePaths == null || resourcePaths.Count <=0)
        {
            _log.Warn("Not enough parameters spcified for dynamic resource add");
            return null;
        }

        var addedIds = new ResourceRepository().AddResource(resourcePaths.OfType<string>(),viewId.Value, columnNumber.Value,position.Value);

        return addedIds.First();
    }

    public override void RenderControl(HtmlTextWriter writer)
    {
        // hack hack hack - start

        // thou shalt not copy any of this hack

        // this code was made during code freeze in jericho, and internals aren't available to do this most cleanly
        // doing this cleanly would have meant that OrionInclude would render script tags compatible with this type of processing

        StringBuilder sb = new StringBuilder();
        using (StringWriter sw = new StringWriter(sb))
        using (HtmlTextWriter htw = new HtmlTextWriter(sw))
        {
          {
                // hack hack hack - ClientScriptManager is used by infragistics. 
                // I need to render all of the content infragistics registered.
                  // todo: I haven't solved duplicities - how to disable loading of ScriptResources already on page?
                
                var csmtype = this.Page.ClientScript.GetType();
                var renderClientScriptBlocksMethod = csmtype.GetMethod("RenderClientScriptBlocks", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
                renderClientScriptBlocksMethod.Invoke(this.Page.ClientScript, new object[] { htw });
            }
            
            // detect and trap 'top' javascript

            foreach (var included in new[] { 
                new IncludedFiles { Section = OrionInclude.Section.Top, Kind = SolarWinds.Orion.Web.UI.Localizer.PathResolver.MarkupItemType.JavaScript },
                new IncludedFiles { Section = OrionInclude.Section.Middle, Kind = SolarWinds.Orion.Web.UI.Localizer.PathResolver.MarkupItemType.JavaScript }
            })
            {
                included.RenderControl(htw);
            }

            string text = sb.ToString();
            text = Regex.Replace(text, "<script type=\"text/javascript\" src=\"([^\"]+)\"></script>", "<script type=\"text/sw-javascript\" swsrc=\"$1\"></script>");
            text = Regex.Replace(text, "<script src=\"([^\"]+)\" type=\"text/javascript\"></script>", "<script type=\"text/sw-javascript\" swsrc=\"$1\"></script>");
            text = Regex.Replace(text, "<script type=\"text/javascript\">", "<script type=\"text/sw-javascript\">");

            ControlPlaceHolder.Controls.AddAt(0, new LiteralControl(text));
            sb.Length = 0;

            // detect and trap css

            foreach (var included in new[] { 
                new IncludedFiles { Section = OrionInclude.Section.Top, Kind = SolarWinds.Orion.Web.UI.Localizer.PathResolver.MarkupItemType.Css },
                new IncludedFiles { Section = OrionInclude.Section.Middle, Kind = SolarWinds.Orion.Web.UI.Localizer.PathResolver.MarkupItemType.Css },
                new IncludedFiles { Section = OrionInclude.Section.Bottom, Kind = SolarWinds.Orion.Web.UI.Localizer.PathResolver.MarkupItemType.Css },
            })
            {
                included.RenderControl(htw);
            }

            text = sb.ToString();
            text = Regex.Replace(text, "<link rel=\"stylesheet\" type=\"text/css\" href=\"([^\"]+)\" />", "<script type=\"text/sw-stylesheet\" swsrc=\"$1\"></script>");

            ControlPlaceHolder.Controls.AddAt(0,new LiteralControl(text));
            sb.Length = 0;

            // detect javascript after control

            foreach (var included in new[] { 
                new IncludedFiles { Section = OrionInclude.Section.Bottom, Kind = SolarWinds.Orion.Web.UI.Localizer.PathResolver.MarkupItemType.JavaScript }
            })
            {
                included.RenderControl(htw);
            }

            text = sb.ToString();
            text = Regex.Replace(text, "<script type=\"text/javascript\" src=\"([^\"]+)\"></script>", "<script type=\"text/sw-javascript\" swsrc=\"$1\"></script>");
            text = Regex.Replace(text, "<script type=\"text/javascript\">", "<script type=\"text/sw-javascript\">");

            ControlPlaceHolder.Controls.Add(new LiteralControl(text));
            sb.Length = 0;

            // insert the markers so we can trap only what we want.

            ControlPlaceHolder.Controls.AddAt(0, new LiteralControl(MarkerStart));
            ControlPlaceHolder.Controls.Add(new LiteralControl(MarkerEnd));
            base.RenderControl(htw);

            text = sb.ToString();
            text = Regex.Replace(text, "<script type=\"text/javascript\" src=\"([^\"]+)\"></script>", "<script type=\"text/sw-javascript\" swsrc=\"$1\"></script>");
            text = Regex.Replace(text, "<script type=\"text/javascript\">", "<script type=\"text/sw-javascript\">");
            
            Match viewStateMatch = Regex.Match(text, " id=\"__VIEWSTATE\" value=\"([^\"]*)\"");
            if (viewStateMatch.Success)
            {
                string viewState = viewStateMatch.Groups[1].Value;
                writer.Write("<script type=\"text/sw-viewstate\">");
                writer.Write(viewState);
                writer.Write("</script>\n");
            }
            
            Match viewStateGeneratorMatch = Regex.Match(text, " id=\"__VIEWSTATEGENERATOR\" value=\"([^\"]*)\"");
            if (viewStateGeneratorMatch.Success)
            {
                string viewStateGenerator = viewStateGeneratorMatch.Groups[1].Value;
                writer.Write("<script type=\"text/sw-viewstate-generator\">");
                writer.Write(viewStateGenerator);
                writer.Write("</script>\n");
            }
            
            int indexStart = text.IndexOf(MarkerStart, StringComparison.OrdinalIgnoreCase) + MarkerStart.Length;
            int indexEnd = text.IndexOf(MarkerEnd, StringComparison.OrdinalIgnoreCase);

            sb.Length = 0;
            string result = text.Substring(indexStart, indexEnd - indexStart);
            string wrapper = string.Empty;

            // it is a web resource and is hidden (ResourceWrapper is not rendered), then we have to add it to page refreshing work properly
            if (Resource != null && !reg.IsMatch(result))
            {
                wrapper =
                    string.Format(@"<div class='ResourceWrapper HiddenResourceWrapper' resourceid='{0}' sw-widget-container='{0}' style='width: {1}px; display: none;' sw-widget-title='{2}'>
                                        <div class='HeaderBar'>
                                          <div class='mResourceHeader'><h1 class='sw-widget__title'>{2}</h1></div>
                                        </div>
                                        <div class='HiddenResourcePh'>{3}</div>
                                        <sw-widget-overlay></sw-widget-overlay>
                                    </div>", Resource.ID, Resource.Width + 2,
                        CommonWebHelper.EncodeHTMLTags(Resource.Title), CoreWebContent.WEBDATA_Hidden_Resource);
            }
            writer.Write(result);
            writer.Write(wrapper);
            
                
            sb.Length = 0;
            {
                // hack hack hack - ClientScriptManager is used by infragistics. 
                // I need to render all of the content infragistics registered. 
                
                var csmtype = this.Page.ClientScript.GetType();
                var renderClientStartupScripts = csmtype.GetMethod("RenderClientStartupScripts", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
                renderClientStartupScripts.Invoke(this.Page.ClientScript, new object[] { htw });
            }
            text = sb.ToString();
            // infragistics are rendering script='type', not script="type". 
            text = Regex.Replace(text, "<script type=(\"|')text/javascript(\"|') src=(\"|')([^\"]+)(\"|')></script>", "<script type=\"text/sw-javascript\" swsrc=\"$1\"></script>");
            text = Regex.Replace(text, "<script type=(\"|')text/javascript(\"|')>", "<script type=\"text/sw-javascript\">");
            writer.Write(text);
        }

        // hack hack hack - end
    }

    void ApplyPropertiesAndAttributes(System.Web.UI.Control controlToRender)
    {
        const string prefixConfig = "config.";
        const string prefixAttrib = "attrib.";

        object fetch;

        var attrs = GetAttributes(controlToRender);

        // configuration values

        foreach (string key in Request.QueryString.AllKeys.Where(x => x != null && x.StartsWith(prefixConfig, StringComparison.OrdinalIgnoreCase)))
            PropertySetter.SetProperty(controlToRender, key.Substring(prefixConfig.Length), HttpUtility.HtmlEncode(Request.QueryString[key]));

        if (JsonData != null && JsonData.TryGetValue(prefixConfig.Substring(0, prefixConfig.Length - 1), out fetch))
            PropertySetter.SetProperties( controlToRender, fetch as Dictionary<string, object> );

        if (attrs == null)
            return;

        foreach (string key in Request.QueryString.AllKeys.Where(x => x != null && x.StartsWith(prefixAttrib, StringComparison.OrdinalIgnoreCase)))
            attrs[key.Substring(prefixAttrib.Length)] = HttpUtility.HtmlEncode(Request.QueryString[key]);

        if (JsonData != null && JsonData.TryGetValue(prefixAttrib.Substring(0, prefixAttrib.Length - 1), out fetch) && fetch != null)
        {
            Dictionary<string, object> attrib = fetch as Dictionary<string, object>;

            if (attrib != null)
            {
                foreach (var pair in attrib.Where( x => x.Value is string ) )
                    attrs[pair.Key] = pair.Value as string;
            }
        }
    }

    bool InitWebResource(int? addedResourceId)
    {
        int resId;
        if (addedResourceId == null)
        {
            string resourceId = (GetParam(ParamResourceID) ?? string.Empty).Trim();

            if (string.IsNullOrEmpty(resourceId))
                return false;

            if (int.TryParse(resourceId, out resId) == false)
                throw new ArgumentException(ParamResourceID + " must be a valid id of a resource");
        }
        else
        {
            resId = addedResourceId.Value;
        }

        string netobjectValue = (GetParam(ParamNetObject) ?? String.Empty).Trim();

        if (!string.IsNullOrEmpty(netobjectValue))
        {
            try
            {
                NetObject = NetObjectFactory.Create(netobjectValue);
            }
            catch (AccountLimitationException)
            {
                this.Page.Server.Transfer(
                    string.Format("/Orion/AccountLimitationError.aspx?NetObject={0}", netobjectValue)
                    );
            }
        }

        Resource = SolarWinds.Orion.Web.ResourceManager.GetResourceByID(resId);
        this.Context.Items[typeof(ViewInfo).Name] = Resource.View;

        BaseResourceControl ctrl = (BaseResourceControl)LoadControl(Resource.File);
        ctrl.Resource = Resource;
        ResourceHostControl host = ResourceHostManager.GetSupportingControl(ctrl, Resource.View.ViewType);

        ControlPlaceHolder.Controls.Add(host);
        host.LoadFromRequest();
        
        var panel = new Panel();
        host.Controls.Add(panel);
        panel.Controls.Add(ctrl);

        ApplyPropertiesAndAttributes(ctrl);

        host.DataBind();

        bool includeWrapper = false;
        if (bool.TryParse(GetParam(ParamResourceWrap) ?? string.Empty, out includeWrapper) && includeWrapper)
        {
            // option to specify the width of the resource to render as, if we're not including the wrapper
            // set all three columns, 

            int columnWidth = 0;
            if (int.TryParse(GetParam(ParamResourceWidth) ?? string.Empty, out columnWidth) && columnWidth > 0)
            {
                switch (Resource.Column)
                {
                    case 1:
                        Resource.View.Column1Width = columnWidth;
                        break;
                    case 2:
                        Resource.View.Column2Width = columnWidth;
                        break;
                    case 3:
                        Resource.View.Column3Width = columnWidth;
                        break;
                    case 4:
                        Resource.View.Column4Width = columnWidth;
                        break;
                    case 5:
                        Resource.View.Column5Width = columnWidth;
                        break;
                    case 6:
                        Resource.View.Column6Width = columnWidth;
                        break;
                }
            }

            return true;
        }

        //var ph = ControlHelper.FindControlRecursive(ctrl, "ResourcePlaceHolder");

        //if (ph != null)
        //{
        //    host.Visible = false; // hide the wrapper, move the actual resource content into the page.
        //    foreach (Control c in ph.Controls.Cast<Control>().ToList())
        //        ControlPh.Controls.Add(c);
        //}

        return true;
    }

    System.Web.UI.AttributeCollection GetAttributes(Control ctrl)
    {
        var pi = ctrl.GetType().GetProperty("Attributes");
        if (pi != null)
        {
            if (pi.PropertyType == typeof(System.Web.UI.AttributeCollection))
                return pi.GetValue(ctrl, null) as System.Web.UI.AttributeCollection;
        }

        return null;
    }
    
    // We need placeholder that implements INamingContainer,
    // we will fill ResourceId there to avoid name clashes between resources
    class PlaceHolderWithID : System.Web.UI.WebControls.PlaceHolder, INamingContainer
    {
    }

    class PropertySetter
    {
        static JavaScriptSerializer _serializer;

        static public Dictionary<string, object> LoadJsonData( StreamReader sr )
        {
            if (_serializer == null)
                _serializer = new JavaScriptSerializer();

            return _serializer.Deserialize<Dictionary<string, object>>(sr.ReadToEnd());
        }

        static bool ToType(Type targetType, string value, out object converted)
        {
            if (targetType == typeof(string))
            {
                converted = value;
                return true;
            }

            // is it an enum?
            if (targetType.IsEnum)
            {
                try
                {
                    converted = Enum.Parse(targetType, value);
                    return true;
                }
                catch (Exception ex)
                {
                }
            }
            
            // is it a double?

            if (targetType == typeof (double))
            {
                double d;

                if (double.TryParse(value, NumberStyles.Float, CultureInfo.InvariantCulture, out d))
                {
                    converted = d;
                    return true;
                }
            }

            try
            {
                // is there a tryParse?
                Type[] argTypes = { typeof(string), targetType.MakeByRefType() };
                var tryParse = targetType.GetMethod("TryParse", argTypes);

                if (tryParse != null)
                {
                    object[] args = { value, targetType.IsValueType ? Activator.CreateInstance(targetType) : null };
                    bool successfulParse = (bool)tryParse.Invoke(null, args);

                    if (successfulParse == false)
                    {
                        converted = null;
                        return false;
                    }

                    converted = args[1];
                    return true;
                }
            }
            catch (Exception ex)
            {
            }


            // how about a type converter?

            try
            {
                var converter = TypeDescriptor.GetConverter(targetType, true);
                converted = converter.ConvertFrom(value);
                return true;
            }
            catch (Exception ex)
            {
            }

            // how about good old convert-change-type?

            try
            {
                converted = Convert.ChangeType(value, targetType);
                return true;
            }
            catch (Exception ex)
            {
            }

            converted = null;
            return false;
        }

        static public bool SetProperty(object obj, string name, object value)
        {
            if( obj == null || name == null )
                return false;

            var _t = obj.GetType();
            var prop = _t.GetProperty(name);

            if (prop == null || prop.CanWrite == false)
                return false;

            try
            {
                object converted;

                if (value == null)
                {
                    converted = prop.PropertyType.IsValueType == false ?
                        null :
                        Activator.CreateInstance(prop.PropertyType);
                }
                else if (value.GetType() == prop.PropertyType)
                {
                    converted = value;
                }
                else if (value is string)
                {
                    if (ToType(prop.PropertyType, value as string, out converted) == false)
                        return false;
                }
                else
                {
                    converted = Convert.ChangeType(value, prop.PropertyType);
                }

                prop.SetValue(obj, converted, null);
                return true;
            }
            catch (Exception ex)
            {
            }

            return false;
        }

        static public void SetProperties(object obj, Dictionary<string, object> bag)
        {
            if (bag == null || obj == null)
                return;

            foreach (var pair in bag)
                SetProperty(obj, pair.Key, pair.Value);
        }
    }
}