﻿using System;

public partial class Orion_PerfStack_Admin_EditViews_PerfStackAllowRealTimePolling : SolarWinds.Orion.Web.UI.ProfilePropEditUserControl
{
    public override string PropertyValue { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        { 
            if (!String.IsNullOrEmpty(PropertyValue))
            {
                ctrPerfStackAllowRealTimePolling.SelectedValue = PropertyValue;
            }
        }
        PropertyValue = ctrPerfStackAllowRealTimePolling.SelectedValue;
    }
}