﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PerfStackAllowRealTimePolling.ascx.cs" Inherits="Orion_PerfStack_Admin_EditViews_PerfStackAllowRealTimePolling" %>
<asp:RadioButtonList ID="ctrPerfStackAllowRealTimePolling" SelectionMode="Single" Rows="1" runat="server">
	<asp:ListItem Text="<%$ Resources: PerfStackWebContent, RealTimePolling_EditAccount_Allow %>" Selected="True" Value="True"/>
	<asp:ListItem Text="<%$ Resources: PerfStackWebContent, RealTimePolling_EditAccount_Disallow %>" Value="False"/>
</asp:RadioButtonList>