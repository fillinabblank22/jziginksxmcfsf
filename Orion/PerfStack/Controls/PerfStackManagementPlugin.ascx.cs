﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Plugins;
using SolarWinds.Orion.NPM.Web;

public partial class Orion_PerfStack_Controls_PerfStackManagementPlugin : ManagementTasksPluginBase
{

    public override IEnumerable<ManagementTaskItem> GetManagementTasks(NetObject netObject, string returnUrl)
    {
		var node = netObject as Node;
		if (node == null)
		{
			var netobject = netObject.NetObjectID;
			if (NetObjectFactory.ConvertNetObject(ref netobject, new[] { "N" }))
			{
				if (!String.IsNullOrEmpty(netobject))
				{
					node = NetObjectFactory.Create(netobject) as Node;
				}
			}
		}

		if (node == null)
			yield break;

        var linkUrl = string.Format(
            "/ui/perfstack/?context=0_Orion.Nodes_{0}&withRelationships=true&presetTime=last12Hours&charts=0_Orion.Nodes_{0}-Orion.CPULoad.AvgLoad,0_Orion.Nodes_{0}-Orion.CPULoad.AvgPercentMemoryUsed;0_Orion.Nodes_{0}-Orion.ResponseTime.AvgResponseTime;0_Orion.Nodes_{0}-Orion.LoadAverage.LoadAverage1Avg,0_Orion.Nodes_{0}-Orion.LoadAverage.LoadAverage5Avg,0_Orion.Nodes_{0}-Orion.LoadAverage.LoadAverage15Avg;0_Orion.Nodes_{0}-Orion.PerfStack.Alerts;0_Orion.Nodes_{0}-Orion.PerfStack.Events;0_Orion.Nodes_{0}-Orion.PerfStack.Status;0_Orion.Nodes_{0}-Orion.PerfStack.Changes.ScmChanges;",
            node.NodeID);

        var PCUId = NodeHelper.GetPCUId(node);
        if (PCUId.HasValue)
        {
            linkUrl += string.Format(
                "0_Cortex.Orion.PowerControlUnit_{0}-Cortex.Orion.PowerControlUnit.Metrics.AvgBatteryCapacity,0_Cortex.Orion.PowerControlUnit_{0}-Cortex.Orion.PowerControlUnit.Metrics.AvgBatteryTemperature,0_Cortex.Orion.PowerControlUnit_{0}-Cortex.Orion.PowerControlUnit.Metrics.AvgOutputPercentLoad;",
                PCUId.Value);
        }

        var taskSectionName = SolarWinds.Orion.PerfStack.Strings.Resources.PerfStack_TaskSectionName;
        yield return new ManagementTaskItem
		{
			Section = taskSectionName,
			ClientID = "perfstackNode",
			LinkInnerHtml = string.Format(
                "<img src='/Orion/PerfStack/images/icons/perfstack_launch.svg' height='16' width='16' alt='' />&nbsp;{0}", SolarWinds.Orion.PerfStack.Strings.Resources.PerfStack_TaskName),
			LinkUrl = linkUrl
		};
    }
}