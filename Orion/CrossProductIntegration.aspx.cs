﻿using System;
using System.Linq;
using System.Text;

public partial class Orion_CrossProductIntegration : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataBind();
    }

    protected void OrionSiteMapPath_OnInit(object sender, EventArgs e)
    {
        if (!CommonWebHelper.IsBreadcrumbsDisabled)
        {
            var renderer = new SolarWinds.Orion.NPM.Web.UI.AdminSiteMapRenderer();
            renderer.SetUpData();
            OrionSiteMapPath.SetUpRenderer(renderer);
        }
    }

    protected string Caption
    {
        get
        {
            return Resources.CoreWebContent.WEBDATA_LK0_8;
        }
    }

    protected string URL
    {
        get
        {
            var modules = SolarWinds.Orion.Web.OrionModuleManager.GetInstalledModules();
            var excludedModules = new[] 
            { 
                "Orion".ToLowerInvariant(),
                "VIM".ToLowerInvariant()
            };

            var sb = new StringBuilder();
            foreach (var item in modules.Where(m => !excludedModules.Contains(m.ProductTag.ToLowerInvariant())))
                sb.AppendFormat(",{0}", item.ProductTag);

            var installedProduct = String.Empty;
            if (sb.Length != 0)
                installedProduct = sb.ToString().Substring(1);

            var product = this.Request["product"];
            var lang = System.Threading.Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName;

            string swid;
            try
            {
                swid = SolarWinds.Orion.Web.Helpers.LicensingHelper.GetLicenseSWID();
            }
            catch (Exception)
            {
                swid = String.Empty;
            }

            const string defaultUrl = "/Orion/Admin/WebIntegrationProxy.aspx?linkId=integration_tips";
            var urlString = SolarWinds.Orion.Web.DAL.WebSettingsDAL.GetValue("TipsIntegrationURL", defaultUrl);
            Uri url;
            if (!Uri.TryCreate(urlString, UriKind.Absolute, out url))
                urlString = defaultUrl;

            if (this.Request.IsSecureConnection)
                urlString = urlString.ToLowerInvariant().Replace("http://", "https://");

            char separator = urlString.Contains("?") ? '&' : '?';

            return String.Format("{0}{5}product={1}&lang={2}&installed={3}&swid={4}", urlString, product, lang, installedProduct, swid, separator);
        }
    }
}