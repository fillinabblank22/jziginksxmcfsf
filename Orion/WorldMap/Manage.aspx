<%@ Page Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PCC_WM_MANAGETITLE %>" Language="C#"
    MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true" Inherits="SolarWinds.Orion.Web.WorldMap.ManagePage" %>
<%@ Import Namespace="SolarWinds.Orion.Web.WorldMap" %>
<%@ Import Namespace="SolarWinds.Orion.Web.UI.Localizer" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Swis" %>

<%@ Register TagPrefix="orion" TagName="WorldMapSelectionPane" Src="SelectionPane.ascx" %>
<%@ Register TagPrefix="orion" TagName="WorldMapSelectionSummary" Src="SelectionSummary.ascx" %>
<%@ Register TagPrefix="orion" TagName="NetObjectPicker" Src="~/orion/controls/NetObjectPicker.ascx" %>
<script runat="server" language="c#">
    protected override void OnLoad(EventArgs e)
    {
        OrionInclude.CoreFile("leaflet-0.7.7/leaflet.js");
        if (WorldMapHelper.UseExternalMapQuest(HttpContext.Current))
        {
            OrionInclude.ExternalFile("https://www.mapquestapi.com/sdk/leaflet/v2.2/mq-map.js?key=" + SolarWinds.Orion.Core.Common.DALs.WorldMapPointsDAL.GetMapQuestKey(), PathFinder.ResourceType.JavaScript);
        }
        else
        {
            OrionInclude.CoreFile("mq-map.js");
        }

        maincancel.NavigateUrl = CancelURL;
        mainsave.NavigateUrl = SaveURL;
        base.OnLoad(e);
    }
</script>
<asp:Content ID="Content2" ContentPlaceHolderID="HeadPlaceHolder" runat="Server">
    <orion:Include runat="server" File="WorldMap.css" />
    <orion:Include runat="server" File="ExpandMenu.css" />
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="RenderControl.js" Section="Bottom" />
    <orion:Include runat="server" File="WorldMap.js" Section="Bottom" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" name="Manage_AutoGeolocation" id="Manage_AutoGeolocation" value='<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting("AutomaticGeolocation-Enable") != null ? SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting("AutomaticGeolocation-Enable").SettingValue : 0) %>' />
    <div class="sw-hdr-links">
        <table>
            <tr>
                <td>
                    <a href=<%= SwisFederationInfo.IsFederationEnabled ? "/apps/orion-customproperties/management" : "/Orion/Admin/CPE/Default.aspx" %>  style="background: transparent url(/Orion/Nodes/images/icons/icon_edit.gif) scroll no-repeat left center;
                        padding: 2px 0px 2px 20px; font-size: 8pt;" target="_blank">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_4) %></a>
                </td>
                <td>
                            <img src="/Orion/images/ActiveAlerts/divider.gif" style="margin-left: 3pt; margin-right: 3pt;" />
                        </td>
                <td>
                    <a target="_blank" rel="noopener noreferrer" style="background: transparent url('/Orion/images/Icon.Help.gif') scroll no-repeat left center;
                        padding: 2px 0px 2px 20px;  font-size: 8pt;" href="<%: SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("OrionCoreAGManageWorldMapsOrionWebConsole") %>">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_WM_HELPLINK) %></a>
                </td>
                <td>
                            <img src="/Orion/images/ActiveAlerts/divider.gif" style="margin-left: 3pt; margin-right: 3pt;" />
                        </td>
                <td>
                    <table>
                        <tr class="sw-preference-row">
                            <td class="sw-preference">
                                <span class="sw-preference-more" style="margin-left: 27px; padding-top: 1px;">
                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_140) %></span>
                                <div class="sw-pref-options sw-pref-options-hidden">
                                    <table>
                                        <tr class="sw-pref-section">
                                            <td colspan="2" class="sw-pref-section-header">
                                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_174) %>
                                            </td>
                                        </tr>
                                        <tr class="sw-pref-item">
                                            <td class="sw-pref-action">
                                                <input type="checkbox" id="AutoGeolocation" name="AutoGeolocation" />
                                            </td>
                                            <td>
                                                <label for="PauseAutoRefresh">
                                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_175) %>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr class="sw-pref-item">
                                            <td colspan="2">
                                                <hr style="border-color: #E1E1E1; border-style: solid; border-bottom-style: none;
                                                    border-width: 1px;" />
                                            </td>
                                        </tr>
                                        <tr class="sw-pref-item">
                                            <td colspan="2" class="sw-pref-action">
                                                <a href="#" style="margin-left: 0px;" class="exportToPdf" id="ExportToPdf" onclick="ExportToPDF(); return false;">
                                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_146) %></a>
                                            </td>
                                        </tr>
                                        <tr class="sw-pref-item">
                                            <td colspan="2" class="sw-pref-action">
                                                <a href="/Orion/WorldMap/Manage.aspx?Printable=true" style="margin-left: 0px;" class="sw-hdr-links-print sw-hdr-links-print-image">
                                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_147) %></a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="mapframe" style="padding: 0 10px 0 10px;">
        <div>
            <h1 class="sw-hdr-title" style="margin: 15px 0 6px 0; padding: 0;">
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_WM_MANAGETITLE) %></h1>
            <div id="tool" style="margin-bottom: 4px;">
            </div>
            <div id="maptop" style="position: relative; min-width: 800px;">
                <div class="sw-wmap-pane-alt">
                    <div id='worldmap0' style="height: 350px; width: 100%;">
                    </div>
                </div>
                <div class="sw-wmap-pane-host" style="position: absolute; right: 0; top: 0; width: 244px;">
                    <orion:WorldMapSelectionPane runat="server" />
                </div>
            </div>
            <div id="mapbtnbar" class='sw-btn-bar' style="position: relative; padding: 10px 0 0 0;
                margin-left: 0;">
                <orion:LocalizableButtonLink ID="mainsave" runat="server" DisplayType="Primary" LocalizedText="Submit"
                    onclick="SW.Core.WorldMap.Editor.UI.Undo.Ignore(); return true;" />
                <orion:LocalizableButton runat="server" DisplayType="Secondary" LocalizedText="CustomText"
                    OnClientClick="SW.Core.WorldMap.Editor.UI.SaveAll(); return false;" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PCC_BTN_SAVEANDCONTINUE %>" />
                <orion:LocalizableButtonLink ID="maincancel" runat="server" DisplayType="Secondary"
                    LocalizedText="Cancel" onclick="SW.Core.WorldMap.Editor.UI.Undo.Ignore(); return true;" />
                <orion:WorldMapSelectionSummary Undo="True" runat="server" />
            </div>
        </div>
    </div>
    <div id="selectordlg" style="display: none;">
        <div style="margin-bottom: 10px;">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_66) %></div>
        <orion:NetObjectPicker runat="server" />
        <div class="location">
            <div>
                <b>
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_68) %></b></div>
            <input type="text" id="location" />
        </div>
        <div style="padding-top: 10px; float: left">
            <div class="sw-suggestion">
                <span class="sw-suggestion-icon"></span>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_82) %>
                <a class="sw-link" target="_blank" rel="noopener noreferrer" href="<%: SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("orioncoreph-resourceworldmapautomaticplacement") %>">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_51) %></a>
                <%-- » Learn more --%>
            </div>
        </div>
        <div class='sw-btn-bar-wizard' style="margin-bottom: 0; padding-bottom: 0; padding-top: 18px;
            float: right; width: 35%">
            <orion:LocalizableButton ID="selectordlgprimary" ClientIDMode="Static" runat="server"
                DisplayType="Primary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PCC_WM_BTN_PLACEONMAP %>"
                OnClientClick="SW.Core.WorldMap.Editor.UI.SelectorDialogFinish(); return false;" />
            <orion:LocalizableButton ID="selectordlgcancel" ClientIDMode="Static" runat="server"
                DisplayType="Secondary" LocalizedText="Cancel" OnClientClick="SW.Core.WorldMap.Editor.UI.Cancel(); return false;" />
        </div>
    </div>
    <div id="removeconfirm" style="display: none;">
        <div style="padding: 8px;">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_WM_DLG_REMOVE_QUESTION) %></div>
        <div class='sw-btn-bar-wizard' style="margin-bottom: 0; padding-bottom: 0;">
            <orion:LocalizableButton runat="server" DisplayType="Primary" LocalizedText="CustomText"
                Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PCC_WM_BTN_REMOVESELECTION %>" OnClientClick="SW.Core.WorldMap.Editor.UI.ClearSelection(); $('#removeconfirm').dialog('close'); return false;" />
            <orion:LocalizableButton runat="server" DisplayType="Secondary" LocalizedText="Cancel"
                OnClientClick="$('#removeconfirm').dialog('close'); return false;" />
        </div>
    </div>
    <div id="locationdlg" style="display: none;">
        <div style="margin: 8px;">
            <table style="table-layout: fixed; width: 100%;">
                <tr>
                    <td>
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_WM_LATITIDE) %>
                    </td>
                    <td style="width: 40px;">
                    </td>
                    <td>
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_WM_LONGITUDE) %>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input id="locationdlglat" type="text" style="width: 130px;" />
                    </td>
                    <td>
                        <!-- -->
                    </td>
                    <td>
                        <input id="locationdlglng" type="text" style="width: 130px;" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="locationdlgerr" style="margin: 8px; display: none;">
            <span class="sw-suggestion sw-suggestion-fail sw-suggestion-noicon">
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_WM_VALIDATE_LNGLAT) %>
            </span>
        </div>
        <div class='sw-btn-bar-wizard' style="margin-bottom: 0; padding-bottom: 0;">
            <orion:LocalizableButton runat="server" DisplayType="Primary" LocalizedText="Save"
                OnClientClick="SW.Core.WorldMap.Editor.UI.EditLocationFinish(); return false;" />
            <orion:LocalizableButton runat="server" DisplayType="Secondary" LocalizedText="Cancel"
                OnClientClick="$('#locationdlg').dialog('close'); return false;" />
        </div>
    </div>
    <orion:InlineCss runat="server">
    a.sw-wmap-edit { float: right; font-weight: normal; }
    #poipane { height: 100%; }
    #content { padding-bottom: 0; }
    .sw-wmap-selected { padding: 4px; }
    .sw-wmap-selected > span { font-weight: bold; }
    .sw-wmap-details { padding: 2px 0px 2px 4px; line-height: 14px; }
    .sw-wmap-details div:first-child { margin-top: 0; }
    .sw-wmap-details div { margin-top: 3px; }
    .sw-wmap-details a img { vertical-align: middle; }
    .sw-wmap-details { overflow: auto; background-color: #f0f0f0; }
    
    .sw-wmap-pane-host { display: none; }
    .sw-wmap-showpane .sw-wmap-pane-host { display: block; }
    .sw-wmap-showpane .sw-wmap-pane-alt { margin-right: 256px; }
    #mapbtnbar { margin: 0 0 0 10px; }
    .sw-wmap-showpane #mapbtnbar { margin: 0 256px 0 10px; }
    #selectordlg.ui-widget-content { background-color: #fff; }
    .location { padding-top : 10px; padding-right: 3px;}
    #location { width:50% }
    </orion:InlineCss>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="outsideFormPlaceHolder" runat="Server">
</asp:Content>
