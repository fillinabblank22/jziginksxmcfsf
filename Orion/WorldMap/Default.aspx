﻿<%@ Page Title="World Map" Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true" Inherits="SolarWinds.Orion.Web.WorldMap.DefaultPage" %>
<%@ Import Namespace="SolarWinds.Orion.Web.WorldMap" %>
<%@ Import Namespace="SolarWinds.Orion.Web.UI.Localizer" %>
<%@ Register tagPrefix="orion" tagName="WorldMapSelectionPane" src="SelectionPane.ascx" %>
<%@ Register tagPrefix="orion" tagName="WorldMapSelectionSummary" src="SelectionSummary.ascx" %>

<script runat="server" language="c#">
    protected override void OnLoad(EventArgs e)
    {
        ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
        Done.NavigateUrl = ReferrerRedirectorBase.GetDecodedReferrerUrlOrDefault("/orion/");

        int RefreshDelay = SolarWinds.Orion.Web.DAL.WebSettingsDAL.AutoRefreshSeconds * 1000;

        if (RefreshDelay < 5000)
            RefreshDelay = 5000;
        OrionInclude.CoreFile("leaflet-0.7.7/leaflet.js");
        if (WorldMapHelper.UseExternalMapQuest(HttpContext.Current))
        {
            OrionInclude.ExternalFile("https://www.mapquestapi.com/sdk/leaflet/v2.2/mq-map.js?key=" + SolarWinds.Orion.Core.Common.DALs.WorldMapPointsDAL.GetMapQuestKey(), PathFinder.ResourceType.JavaScript);
        }
        else
        {
            OrionInclude.CoreFile("mq-map.js");
        }
        OrionInclude.InlineJs(String.Format(System.Globalization.CultureInfo.InvariantCulture, "setInterval(SW.Core.WorldMap.Viewer.UI.Refresh, {0}); ", RefreshDelay) )
            .SetSection(OrionInclude.Section.Bottom);

        if (PgParams.ResourceId.HasValue)
        {
            var res = ResourceManager.GetResourceByID(PgParams.ResourceId.Value);

            if (res != null && res.View != null)
            {
                Page.Title = Macros.ParseMacros(res.Title, string.Empty);
                SelectionSummary.ResourceId = res.ID;
            }
        }

        base.OnLoad(e);
    }
</script>

<asp:Content ContentPlaceHolderID="HeadPlaceHolder" Runat="Server">
    <orion:Include runat="server" File="WorldMap.css" />
    <orion:Include runat="server" File="RenderControl.js" Section="Bottom" />
    <orion:Include runat="server" File="WorldMap.js" Section="Bottom" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" Runat="Server">
	<orion:IconLinkExportToPDF runat="server" ID="ExportToPDFLink"/>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <div id="mapframe" style="padding: 0 10px 0 10px;"><div>
    
        <h1 class="sw-hdr-title" style="margin: 15px 0 6px 0; padding: 0;">
            <%: Page.Title %>
        </h1>

        <div id="maptop" style="position: relative; min-width: 800px;">
            <div class="sw-wmap-pane-alt">
                <div id='worldmap0' style="height: 350px; width: 100%;"></div>
            </div>
            <div class="sw-wmap-pane-host" style="position: absolute; right: 0; top: 0; width: 244px;">
                <orion:WorldMapSelectionPane runat="server" />
            </div>
        </div>

        <div id="mapbtnbar" class='sw-btn-bar' style="position: relative; padding: 10px 0 0 0; margin-left: 0;">
            <orion:LocalizableButtonLink ID="Done" runat="server" DisplayType="Primary" LocalizedText="Done" />
            <orion:WorldMapSelectionSummary ID="SelectionSummary" runat="server" />
        </div>
    </div></div>

<orion:InlineCss runat="server">
    a.sw-wmap-edit { display: none; }
    #poipane { height: 100%; }
    #content { padding-bottom: 0; }
    .sw-wmap-selected { padding: 4px; }
    .sw-wmap-selected > span { font-weight: bold; }
    .sw-wmap-details { padding: 2px 0px 2px 4px; line-height: 14px; }
    .sw-wmap-details div:first-child { margin-top: 0; }
    .sw-wmap-details div { margin-top: 3px; }
    .sw-wmap-details a img { vertical-align: middle; }
    .sw-wmap-details { overflow: auto; background-color: #f0f0f0; }
    
    .sw-wmap-pane-host { display: none; }
    .sw-wmap-showpane .sw-wmap-pane-host { display: block; }
    .sw-wmap-showpane .sw-wmap-pane-alt { margin-right: 256px; }
    #mapbtnbar { margin: 0 0 0 10px; }
    .sw-wmap-showpane #mapbtnbar { margin: 0 256px 0 10px; }
    #selectordlg.ui-widget-content { background-color: #fff; }

</orion:InlineCss>

</asp:Content>

<asp:Content ContentPlaceHolderID="outsideFormPlaceHolder" Runat="Server">
</asp:Content>
