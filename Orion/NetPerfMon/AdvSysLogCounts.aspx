<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/OrionMasterPage.master" CodeFile="AdvSysLogCounts.aspx.cs" Inherits="Orion_NetPerfMon_AdvSysLogCounts" EnableEventValidation="false" EnableViewState="true" %>
<%@ Register src="Controls/AdvancedSyslogCounts.ascx" tagname="AdvancedSyslogCounts" tagprefix="uc1" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="placeHolder" runat="server">

<orion:Include runat="server" File="js/jquery/timePicker.css" />

    <asp:PlaceHolder ID="holder" runat="server" >
        <div style="margin-left: 0px; margin-right: 0px; width:100%">
        <uc1:AdvancedSyslogCounts ID="AdvancedSyslogCounts1" runat="server" />
        </div>
    </asp:PlaceHolder>
</asp:Content>
