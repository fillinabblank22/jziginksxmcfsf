using SolarWinds.Orion.Web;
using System;
using System.Web.UI;
using System.Collections.Generic;
using SolarWinds.Data;
using SolarWinds.Orion.Web.InformationService;

using NetObjectOpidConverter = SolarWinds.Orion.Web.Platform.Converters.NetObjectOpidConverter;
using Opid = SolarWinds.Orion.Platform.Models.Opid;

public partial class CustomChart : System.Web.UI.Page
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        string oldChartName = Request["ChartName"];
        string netObject = Request["NetObject"];
        Opid opid = null;

        if (!string.IsNullOrWhiteSpace(netObject))
        {
            NetObjectOpidConverter netObjConverter = new NetObjectOpidConverter();
            opid = netObjConverter.GetOpid(netObject);
        }

        var newChartName = GetNewChartName(oldChartName);
        if (!string.IsNullOrWhiteSpace(newChartName))
        {
            string presetTime = "presetTime=last12Hours";
            string withRelationships = "withRelationships=true";
            this.Page.Response.Redirect($"/ui/perfstack/?context={opid}&{withRelationships}&{presetTime}&charts={opid}-{newChartName};");
        }
    }

    private string GetNewChartName(string oldChartName)
    {
        try
        {
            using (var proxy = InformationServiceProxy.CreateV3())
            {
                return proxy.Invoke<string>("Orion.Resources", "GetModernResourceName", oldChartName);
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            return null;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        string controlPath = "~/Orion/NetPerfMon/NPMChart.ascx";
        if (!String.IsNullOrEmpty(Request["CustomPollerID"]))
        {
            controlPath = "~/Orion/NPM/CustomPollerChart.ascx";
        }

        Control control = LoadControl(controlPath);
        control.ID = "customChart";
        holder.Controls.Add(control);
    }
}
