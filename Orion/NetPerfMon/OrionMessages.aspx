<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/OrionMasterPage.master"
    CodeFile="OrionMessages.aspx.cs" Inherits="Orion_NetPerfMon_OrionMessages" EnableEventValidation="false"
    EnableViewState="true" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_VB0_270 %>" %>

<%@ Register Assembly="OrionWeb" Namespace="SolarWinds.Orion.Web.UI" TagPrefix="web" %>
<%@ Register TagPrefix="orion" TagName="NetworkObjects" Src="~/Orion/Controls/NetworkObjectControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="NetworkObjectTypes" Src="~/Orion/Controls/TypeOfDevicerControl.ascx" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="orion" TagName="OrionMessages" Src="~/Orion/Controls/OrionMessagesReportControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="Reposter" Src="~/Orion/Controls/Reposter.ascx" %>
<%@ Register TagPrefix="orion" TagName="PickerTimePeriodControl" Src="~/Orion/Controls/PickerTimePeriodControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="Vendors" Src="~/Orion/Controls/VendorControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="AlertNames" Src="~/Orion/Controls/AlertNameControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="AuditTypes" Src="~/Orion/Controls/AuditTypeControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <asp:UpdatePanel runat="server" ID="ExportToPDFPanel" UpdateMode="Conditional">
        <ContentTemplate>
                <orion:IconLinkExportToPDF runat="server" ID="ExportToPDFLink" />
                <asp:LinkButton ID="LinkButton1" runat="server" CssClass="printablePageLink" OnClick="Printable_Click"
                    Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_246 %>" />
                <orion:IconHelpButton HelpUrlFragment="OrionPHViewOrionMessages" ID="helpButton"
                    runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="placeHolder" runat="server">
    <orion:Reposter ID="Reposter1" runat="server" />
    <asp:PlaceHolder ID="holder" runat="server">
        <h1>
            <%= DefaultSanitizer.SanitizeHtml(Page.Title) %>
        </h1>
        <div style="margin: 0 15px;">
            <table style="width: 100%;">
                <tr>
                    <td>
                        <b>
                            <%= HttpUtility.HtmlEncode(Title2) %></b>
                    </td>
                    <td style="width: 40px;">
                        <orion:LocalizableButton runat="server" ID="btnHide" OnClick="Show_Click" DisplayType="Resource"
                            LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_223 %>"
                            Style="white-space: nowrap;" />
                        <orion:LocalizableButton runat="server" ID="btnShow" OnClick="Show_Click" DisplayType="Resource"
                            LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_219 %>"
                            Style="white-space: nowrap;" Visible="false" />
                    </td>
                </tr>
            </table>
            <asp:PlaceHolder runat="server" ID="details">
                <table style="width: 100%; margin-right: 0px;">
                    <tr runat="server" ID="SolarWindsServerFilterRow">
                        <td runat="server" style="width: 110px; font-size: 7pt;">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Events_FilterServers) %>&nbsp;
                        </td>
                        <td class="formTable" colspan="2">
                            <table class="formTable">
                                <tr>
                                    <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Events_FilterServersSolarwindsServerTitle) %></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DropDownList  runat="server" ID="SolarwindsServersDropDown"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 110px; font-size: 7pt;">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_210) %>&nbsp;
                        </td>
                        <td class="formTable" colspan="2">
                            <table class="formTable">
                                <tr>
                                    <td>
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_378) %>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_379) %>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_213) %>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_13) %>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_4) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <orion:NetworkObjects runat="server" ID="netObjects" PageType="All" />
                                    </td>
                                    <td>
                                        &nbsp;<b style="color: #5c5c5c;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_214) %></b>&nbsp;
                                    </td>
                                    <td>
                                        <orion:NetworkObjectTypes runat="server" ID="netObjectTypes" />
                                    </td>
                                    <td>
                                        &nbsp;<b style="color: #5c5c5c;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_214) %></b>&nbsp;
                                    </td>
                                    <td>
                                        <orion:Vendors runat="server" ID="vendors" PageType="All" />
                                    </td>
                                    <td>
                                        &nbsp;<b style="color: #5c5c5c;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_214) %></b>&nbsp;
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="ipAddress" Width="260"></asp:TextBox>
                                    </td>
                                    <td>
                                        &nbsp;<b style="color: #5c5c5c;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_214) %></b>&nbsp;
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="hostName"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 110px; font-size: 7pt;">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_215) %>&nbsp;
                        </td>
                        <td style="margin: 0px;" class="formTable" colspan="2">
                            <table class="formTable" style="margin: 0px; margin-right: 0px;">
                                <tr>
                                    <td style="white-space: nowrap;">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_380) %>&nbsp;<orion:PickerTimePeriodControl
                                            ID="pickerTimePeriodControl" InLine="true" runat="server" />
                                    </td>
                                    <td style="white-space: nowrap;">
                                        &nbsp;<img alt="" src="/Orion/images/form_prompt_divider.gif" />&nbsp;
                                    </td>
                                    <td style="white-space: nowrap;">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_541) %>&nbsp;
                                        <web:ValidatedTextBox CustomStringValue="" Text="250" ID="maxRecords" Columns="5"
                                            Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                            MinValue="0" MaxValue="2000" />
                                        <asp:HiddenField ID="previousMaxRecords" runat="server" />
                                        &nbsp;
                                    </td>
                                    <td style="white-space: nowrap;">
                                        &nbsp;<img alt="" src="/Orion/images/form_prompt_divider.gif" />&nbsp;
                                    </td>
                                    <td style="white-space: nowrap;">
                                        <asp:CheckBox runat="server" ID="cbShowCleared" /><asp:HiddenField ID="previousShowCleared"
                                            runat="server" />
                                        &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_382) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <asp:CheckBox ID="cbShowAlerts" runat="server" />
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_383) %><br />
                                        <table id="filterAlerts" runat="server">
                                            <tr>
                                                <td style="padding-left: 22px; font-size: 7pt; text-transform: uppercase; white-space: nowrap;">
                                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_384) %>
                                                </td>
                                                <td style="white-space: nowrap;">
                                                    &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_385) %>&nbsp;
                                                </td>
                                                <td>
                                                    <orion:AlertNames ID="alertNamesControl" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <asp:CheckBox ID="cbShowEvents" runat="server" />
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_386) %><br />
                                        <table id="filterEvents" runat="server">
                                            <tr>
                                                <td style="padding-left: 22px; font-size: 7pt; text-transform: uppercase; white-space: nowrap;">
                                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_387) %>
                                                </td>
                                                <td style="white-space: nowrap;">
                                                    &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_388) %>&nbsp;
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="eventTypesControl" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <% if (!HideSyslogControls)
                                    {%>
                                <tr>
                                    <td colspan="5">
                                        <asp:CheckBox ID="cbShowSyslogs" runat="server" />
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_389) %><br />
                                        <table id="filterSyslog" runat="server">
                                            <tr>
                                                <td style="padding-left: 22px; font-size: 7pt; text-transform: uppercase; white-space: nowrap;">
                                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_390) %>
                                                </td>
                                                <td style="white-space: nowrap;">
                                                    &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_391) %>&nbsp;
                                                </td>
                                                <td>
                                                    <asp:PlaceHolder ID="SeveritiesPlaceHolder" runat="server" />
                                                </td>
                                                <td>
                                                    &nbsp;<img alt="" src="/Orion/images/form_prompt_divider.gif" />&nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_392) %>&nbsp;
                                                </td>
                                                <td>
                                                    <asp:PlaceHolder ID="FacilitiesPlaceHolder" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <% }  %>
                                <% if (!HideTrapsControls)
                                    {%>
                                <tr>
                                    <td colspan="5">
                                        <asp:CheckBox ID="cbShowTraps" runat="server" />
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_393) %><br />
                                        <table id="filterTraps" runat="server">
                                            <tr>
                                                <td style="padding-left: 22px; font-size: 7pt; text-transform: uppercase; white-space: nowrap;">
                                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_394) %>
                                                </td>
                                                <td style="white-space: nowrap;">
                                                    &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_395) %>&nbsp;
                                                </td>
                                                <td>
                                                    <asp:PlaceHolder ID="TrapTypesPlaceHolder" runat="server" />
                                                </td>
                                                <td>
                                                    &nbsp;
                                                    <%if (Profile.AllowAdmin) { %>
                                                        <img alt="" src="/Orion/images/form_prompt_divider.gif" /> 
                                                    <% } %>
                                                    &nbsp; 
                                                </td>
                                                <td>
                                                    &nbsp;
                                                    <%if (Profile.AllowAdmin) { %>
                                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_396) %>
                                                    <% } %>
                                                    &nbsp;
                                                </td>

                                                <td>
                                                    <asp:PlaceHolder ID="CommunityStringsPlaceHolder" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <% }  %>
                                <%--filter audit messages--%>
                                <% if (ShowAuditControls) { %>
                                <tr>
                                    <td colspan="5">
                                        <asp:CheckBox ID="cbShowAudits" runat="server" />
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_DP0_8) %><br />
                                        <table id="filterAudits" runat="server">
                                            <tr>
                                                <td style="padding-left: 22px; font-size: 7pt; text-transform: uppercase; white-space: nowrap;">
                                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_DP0_9) %>
                                                </td>
                                                <td style="white-space: nowrap;">
                                                    &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_DP0_10) %>&nbsp;
                                                </td>
                                                <td>
                                                    <orion:AuditTypes runat="server" ID="auditTypesControl" />
                                                </td>
                                                <td>
                                                    &nbsp;<img alt="" src="/Orion/images/form_prompt_divider.gif" />&nbsp; 
                                                </td>
                                                <td>
                                                    &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_DP0_11) %>&nbsp;
                                                </td>
                                                <td><asp:TextBox ID="auditUser" runat="server"/></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <% } %>
                            </table>
                            <asp:HiddenField ID="previousAlertType" runat="server" />
                            <asp:HiddenField ID="previousEventType" runat="server" />
                            <asp:HiddenField ID="previousSyslogSeverity" runat="server" />
                            <asp:HiddenField ID="previousSyslogFacility" runat="server" />
                            <asp:HiddenField ID="previousTrapType" runat="server" />
                            <asp:HiddenField ID="previousCommunity" runat="server" />
                            <asp:HiddenField ID="previousShowMessageTypes" runat="server" />
                            <asp:HiddenField ID="previousAuditType" runat="server" />
                            <asp:HiddenField ID="previousAuditUser" runat="server" />
                            <asp:HiddenField ID="searchString" runat="server" />
                            <asp:HiddenField ID="previousSiteId" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <orion:LocalizableButton ID="btnRefresh" LocalizedText="Apply" DisplayType="Primary"
                                runat="server" OnClick="Refresh_Click" />
                        </td>
                        <td style="text-align: right;">
                            <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                <input type="text" runat="server" id="tbSearch" style="width: 170px; vertical-align: top;
                                    border-width: thin; line-height: 18px;" />
                                <orion:LocalizableButton runat="server" ID="btnSearch" OnClick="Search_Click" LocalizedText="CustomText"
                                    DisplayType="Secondary" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_VB0_131 %>" />
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </asp:PlaceHolder>
            <br />
            <orion:OrionMessages runat="server" ID="OrionMessagesControl" />
        </div>
    </asp:PlaceHolder>
    <script type="text/javascript">
        $(function () {
            $('#<%=cbShowAlerts.ClientID%>').click(function () { if (this.checked) $('#<%=filterAlerts.ClientID%>').show(); else $('#<%=filterAlerts.ClientID%>').hide(); });
            $('#<%=cbShowEvents.ClientID%>').click(function () { if (this.checked) $('#<%=filterEvents.ClientID%>').show(); else $('#<%=filterEvents.ClientID%>').hide(); });
            $('#<%=cbShowSyslogs.ClientID%>').click(function () { if (this.checked) $('#<%=filterSyslog.ClientID%>').show(); else $('#<%=filterSyslog.ClientID%>').hide(); });
            $('#<%=cbShowTraps.ClientID%>').click(function () { if (this.checked) $('#<%=filterTraps.ClientID%>').show(); else $('#<%=filterTraps.ClientID%>').hide(); });
            $('#<%=cbShowAudits.ClientID%>').click(function () { if (this.checked) $('#<%=filterAudits.ClientID%>').show(); else $('#<%=filterAudits.ClientID%>').hide(); });

            $('#<%=tbSearch.ClientID%>').each(function () {
                var searchVal = $('#<%=searchString.ClientID%>')[0].value;
                if (searchVal != null && searchVal != '') {
                    this.value = searchVal;
                    this.style.color = '#000';
                    this.style.fontStyle = 'normal';
                    this.style.fontSize = '9pt';
                }
                else {
                    this.value = '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_VB0_131) %>';
                    this.style.color = '#555';
                    this.style.fontStyle = 'italic';
                    this.style.fontSize = '9pt';
                }
            });
            $('#<%=tbSearch.ClientID%>').click(function () {
                if (this.value == '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_VB0_131) %>') {
                    this.value = '';
                    this.style.color = '#000';
                }
                this.style.fontStyle = 'normal';
                this.style.fontSize = '9pt';
            });
            $('#<%=tbSearch.ClientID%>').blur(function () { if (this.value == '') { this.value = '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_VB0_131) %>'; this.style.color = '#555'; this.style.fontStyle = 'italic'; this.style.fontSize = '9pt'; } });
        });
    </script>
</asp:Content>
