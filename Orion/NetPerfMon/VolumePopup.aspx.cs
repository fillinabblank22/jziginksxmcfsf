﻿using System;
using System.Web.UI;

using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;

public partial class Orion_NetPerfMon_VolumePopup : Page
{
    protected Volume Volume { get; set; }
    public int ErrorLevel { get; set; }
    public int WarningLevel { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Volume = NetObjectFactory.Create(Request["NetObject"], true) as Volume;
        ErrorLevel = Convert.ToInt32(Thresholds.DiskSpaceError.SettingValue);
        WarningLevel = Convert.ToInt32(Thresholds.DiskSpaceWarning.SettingValue);
        StatusBar.Status =PercentLabel.Status= Convert.ToInt32(Volume.PercentUsed);
        StatusBar.WarningLevel = PercentLabel.WarningLevel = WarningLevel;
        StatusBar.ErrorLevel = PercentLabel.ErrorLevel = ErrorLevel;
    }
}