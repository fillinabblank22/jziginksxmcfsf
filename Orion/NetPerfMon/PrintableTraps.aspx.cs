using System;
using System.Web.Security.AntiXss;
using System.Web.UI.HtmlControls;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using WebSettingsDAL = SolarWinds.Orion.Web.DAL.WebSettingsDAL;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.Charting;

public partial class PrintableTraps : System.Web.UI.Page
{
	private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

	protected void BusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}

    private string GetLocalizedPeriod
    {
        get
        {
            if (Request.QueryString["Period"].Contains("~"))
            {
                return WebSecurityHelper.SanitizeHtmlV2(AntiXssEncoder.HtmlEncode(Request.QueryString["Period"], true));
            }
            return Periods.GetLocalizedPeriod(WebSecurityHelper.SanitizeHtmlV2(AntiXssEncoder.HtmlEncode(Request.QueryString["Period"],true)));
        }
    }

    public string Title2
	{            
        get
		{
			if (string.IsNullOrEmpty(Request.QueryString["NetObject"]))
			{
				if (string.IsNullOrEmpty(Request.QueryString["DeviceType"]))
				{
					return String.Format(Resources.SyslogTrapsWebContent.WEBCODE_TM0_59, GetLocalizedPeriod);
				}
				else
				{
                    return String.Format(Resources.SyslogTrapsWebContent.WEBCODE_TM0_60, WebSecurityHelper.SanitizeHtmlV2(AntiXssEncoder.HtmlEncode(Request.QueryString["DeviceType"],true)), GetLocalizedPeriod);
				}
			}
			else
			{
                using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
				{
					Node node = proxy.GetNode(Convert.ToInt32(NetObjectHelper.GetObjectID(Request.QueryString["NetObject"])));
					return String.Format(Resources.SyslogTrapsWebContent.WEBCODE_TM0_61, node.Name, GetLocalizedPeriod);
				}
			}
		}
	}

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		if (!(new RemoteFeatureManager()).IsFeatureEnabled("Traps"))
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.SyslogTrapsWebContent.WEBCODE_VB0_327), Title = Resources.SyslogTrapsWebContent.WEBCODE_VB0_347 });

        SolarWinds.Orion.Web.OrionInclude.CoreThemeStylesheets();
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		int maxRecords = 0;
		int.TryParse(Request.QueryString["MaxRecords"], out maxRecords);

		trapReport.GenerateReport(Request.QueryString["NetObject"], WebSecurityHelper.SanitizeHtmlV2(Request.QueryString["Period"]), WebSecurityHelper.SanitizeHtmlV2(Request.QueryString["DeviceType"]), Request.QueryString["TrapType"],
			Request.QueryString["IPAddress"], Request.QueryString["Community"], maxRecords);
	}
}