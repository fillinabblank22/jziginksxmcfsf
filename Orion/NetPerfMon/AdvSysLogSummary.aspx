<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/OrionMasterPage.master" CodeFile="AdvSysLogSummary.aspx.cs" Inherits="Orion_NetPerfMon_AdvSysLogSummary" EnableEventValidation="false" EnableViewState="true" %>
<%@ Register src="Controls/AdvancedSyslogSummary.ascx" tagname="AdvancedSyslogSummary" tagprefix="uc1" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="placeHolder" runat="server">

<orion:Include runat="server" File="js/jquery/timePicker.css" />

    <asp:PlaceHolder ID="holder" runat="server" >
        <div style="margin-left: 0px; margin-right: 0px; width:100%">
        <uc1:AdvancedSyslogSummary ID="AdvancedSyslogSummary1" runat="server" />
        </div>
    </asp:PlaceHolder>
</asp:Content>
