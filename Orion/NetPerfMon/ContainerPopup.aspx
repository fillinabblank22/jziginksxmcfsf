<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ContainerPopup.aspx.cs" Inherits="Orion_NetPerfMon_ContainerPopup" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Containers" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>

<%--This is intended to be displayed as the body of a cluetip tooltip/popup.--%>

<h3 class="<%= DefaultSanitizer.SanitizeHtml(this.StatusCssClass) %>"><%= DefaultSanitizer.SanitizeHtml(UIHelper.Escape(this.Container.Name)) %></h3>

<div class="NetObjectTipBody">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_207) %></th>
			<td align="center">
			<orion:EntityStatusIcon ID="statusIcon" runat="server" />
			</td>
            <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_312) %> <%= DefaultSanitizer.SanitizeHtml(this.Container.Status.ShortDescription) %></td>
		</tr>
	</table>
	
	<table cellpadding="0" cellspacing="0">
	    <tr>
	        <th colspan="3"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_313) %></th>
	    </tr>
	    <asp:Repeater ID="itemsRep" runat="server">
	        <ItemTemplate>
	            <tr class="<%# Container.ItemIndex%2==0 ? "" : " ZebraStripe" %>">
	                <td style="width:20px;">&nbsp;</td>
	                <td>
	                <orion:EntityStatusIcon ID="EntityStatusIcon2" runat="server" Entity="<%# DefaultSanitizer.SanitizeHtml(((ContainerMember)Container.DataItem).Entity) %>" EntityId="<%# DefaultSanitizer.SanitizeHtml(((ContainerMember)Container.DataItem).MemberPrimaryID) %>" Status="<%# DefaultSanitizer.SanitizeHtml(((ContainerMember)Container.DataItem).Status.StatusId) %>" />
	                <%# DefaultSanitizer.SanitizeHtml(UIHelper.Escape(((ContainerMember)Container.DataItem).Name)) %>
	                </td>
	            </tr>
	        </ItemTemplate>
	    </asp:Repeater>
	    
        <% if (this.RemainingItemCount > 0) { %>
            <tr style="background-color:#E2E1D4;">
                <td colspan="3" align="center"><%= DefaultSanitizer.SanitizeHtml(this.MoreItemsMessage) %></td>
            </tr>
        <% } %>
        
        <% if (this.TopItems.Count == 0) { %>
            <tr style="background-color:#E2E1D4;">
                <td colspan="3" align="center"><%= DefaultSanitizer.SanitizeHtml(this.GroupIsEmpty ? Resources.CoreWebContent.WEBCODE_VB0_226 : Resources.CoreWebContent.WEBCODE_VB0_227) %></td>
            </tr>
        <% } %>
        
        
	</table>
</div>
