<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ActiveAlertPopup.aspx.cs" Inherits="Orion_NetPerfMon_ActiveAlertPopup" %>

<%@ Import Namespace="SolarWinds.Orion.Web.DisplayTypes" %>

<% if (IsGlobalAlert && Visible) { %>
    <%--This is intended to be displayed as the body of a cluetip tooltip/popup.--%>
    <h3 class="Status<%= DefaultSanitizer.SanitizeHtml(Status.FromStringInt(AlertStatus).ToString()) %>">
        <%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Core.Web.FormatHelper.MakeBreakableString(ActiveAlert.Name)) %>
    </h3>
    <div class="NetObjectTipBody">
        <table cellpadding="0" cellspacing="0">
        <asp:Repeater ID="rptObjects" runat="server">
            <ItemTemplate>
                <tr>
                    <td style="padding: 0; white-space: nowrap;">
                            <img src="/Orion/StatusIcon.ashx?entity=<%# DefaultSanitizer.SanitizeHtml(EntityName) %>&EntityUri='<%# DefaultSanitizer.SanitizeHtml(Eval("Uri")) %>'&status=<%# DefaultSanitizer.SanitizeHtml(Eval("Status")) %>&size=small"/> &nbsp;
                        <span style="vertical-align: top;">
                            <%# DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Core.Web.FormatHelper.MakeBreakableString(Eval("DisplayName").ToString())) %>
                        </span>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
        <% if (this.RemainingItemCount > 0) { %>
            <tr style="background-color:#E2E1D4;">
                <td align="center"><%= DefaultSanitizer.SanitizeHtml(this.MoreItemsMessage) %></td>
            </tr>
        <% } %>
        </table>
    </div>
<% } %>