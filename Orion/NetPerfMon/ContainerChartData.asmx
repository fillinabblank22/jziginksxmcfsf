﻿<%@ WebService Language="C#" Class="ContainerChartData" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Shared;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ContainerChartData : ChartDataWebService 
{
    private static SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();

    [WebMethod]
    public ChartDataResults ContainerAvailability(ChartDataRequest request)
    {
        try
        {
            var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);
            dateRange.StartDate = dateRange.StartDate.Date;
            //Old charts pull data from 12am.  
            var dateFormatInDatabase = DateTimeKind.Local;
            var dynamicLoader = new DynamicLoader(request, new ChartWidthSampleSizeCalculator());
           
            var originalDateRange = new DateRange() { StartDate = dateRange.StartDate, EndDate = dateRange.EndDate };
            dateRange = SetCustomDateRangeBasedOnRequest(request, dateRange, dateFormatInDatabase);
            var bucketDateRange = dynamicLoader.GetSamplingDateRange(dateRange, originalDateRange);

            const string sql =
                @"SELECT DateTime, GroupAvailability, GroupPercentAvailability
                             FROM Containers_ContainerAvailability 
                             WHERE GroupID = @NetObjectId
                             AND DateTime >= @StartTime AND DateTime <= @EndTime
                             ORDER BY [DateTime]";

            var limitedSql = Limitation.LimitSQL(sql);

            var parameters = new[]
                                 {
                                     new SqlParameter("StartTime", dateRange.StartDate),
                                     new SqlParameter("EndTime", dateRange.EndDate),
                                     new SqlParameter("NetObjectId", request.NetObjectIds.First())
                                 };

            var rawSeries = GetSeries(limitedSql, parameters, dateFormatInDatabase);
            var seriesToReturn = new List<DataSeries>();

            var sampleSize = dynamicLoader.CalculateDynamicSampleSize(dateRange);

            var sampled = rawSeries.CreateSampledSeries(bucketDateRange, sampleSize, SampleMethod.Average,
                                                        ColoredBucketizer);
            seriesToReturn.Add(sampled);

                      
            if (request.Calculate95thPercentile && rawSeries.HasData)
                seriesToReturn.Add(rawSeries.CreatePercentileSeries(sampled.Dates, rawSeries.TagName));

            if (request.CalculateTrendLine && rawSeries.HasData)
                seriesToReturn.Add(rawSeries.CreateTrendSeries(dateRange, request.SampleSizeInMinutes,
                                                               SampleMethod.Average, rawSeries.TagName));

         
            var result = dynamicLoader.SetDynamicChartOptions(new ChartDataResults(seriesToReturn));

            return result;
        }
        catch (Exception e)
        {
            _log.Error("Chart Data Loading Error: Container Availability Chart", e);
            throw;
        }
    }
    private DataSeries GetSeries(string sql, IEnumerable<SqlParameter> parameters, DateTimeKind dateFormatInDatabase)
    {
       
        DataTable dataTable = null;
        using (var cmd = SqlHelper.GetTextCommand(sql))
        {
            cmd.Parameters.AddRange(parameters.ToArray());
            dataTable = SqlHelper.ExecuteDataTable(cmd);
        }

        var dataTableEnumerable = dataTable.AsEnumerable();
        
        var series = new DataSeries();
        series.TagName = "ContainerAvailabilityUp";
        AddDataPoints(series, dataTableEnumerable, dateFormatInDatabase);



        return series;
    }

    private string GetColorBasedOnStatus(StatusInfo status)
    {
        string color;
        switch (status.StatusId)
        {
            case StatusInfo.UpStatusId:
                color = string.Empty;//This is the default color in the chart.  Setting this would only add client-side overhead.
                break;
            case StatusInfo.WarningStatusId:
                color = "#FCD928";
                break;
            case StatusInfo.CriticalStatusId:
                color = "#F99D1C";
                break;
            case StatusInfo.DownStatusId:
            case StatusInfo.UnreachableStatusId:    
                color = System.Drawing.ColorTranslator.ToHtml(status.ColorValue);
                break;
            default:
                color = "#8080FF";
                break;
        }
        

        return color;
    }

    private void AddDataPoints(DataSeries currentSeries, EnumerableRowCollection<DataRow> dataSeries, DateTimeKind dateFormatInDatabase)
    {
        
        foreach (var data in dataSeries)
        {
            var val = data.Field<int>("GroupPercentAvailability");
            
            if ((val != null) && (!Convert.IsDBNull(val)))
            {
                DateTime date = DateTime.SpecifyKind(data.Field<DateTime>("DateTime"), dateFormatInDatabase);
                var statusId = data.Field<int>("GroupAvailability");
                StatusInfo statusInfo = StatusInfo.GetStatus(statusId);
                string color = GetColorBasedOnStatus(statusInfo);
                DataPoint point;
                
                if( !string.IsNullOrEmpty(color))    
                    point = DataPointWithOptions.CreatePointWithOptions(   new Dictionary<string, object>()
                                                                                    {
                                                                                        {"y", Convert.ToDouble(val)},
                                                                                        {"x", DataPoint.ToUnixTime(date) },
                                                                                        {"color", color},
                                                                                        {"ranking", statusInfo.Ranking}
                                                                                    });
                else
                    point = DataPoint.CreatePoint(date, Convert.ToDouble(val));
                
                currentSeries.AddPoint(point);
            }
        }
        
    }


    private DataPoint ColoredBucketizer(double bucketValue, long bucketStart, IEnumerable<DataPoint> currentBucketPoints)
    {
        bool containsDataPointsWithOptions = currentBucketPoints.OfType<DataPointWithOptions>().Any();
        if (containsDataPointsWithOptions)
        {//If any of the points in our bucket have a color then we take the lowest ranking color.
            var bucketColor = currentBucketPoints.OfType<DataPointWithOptions>().OrderBy(p => p.Data["ranking"]).First().Data["color"];
            dynamic bucketHover = new JsonObject();
            bucketHover.hover.color = bucketColor;

            return DataPointWithOptions.CreatePointWithOptions(new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase)
                                                                                    {
                                                                                        {"y", bucketValue},
                                                                                        {"x", bucketStart },
                                                                                        {"color", bucketColor },
                                                                                        {"states", bucketHover}
                                                                                    });
        }

        return DataPoint.CreatePoint(bucketStart, bucketValue);
    }


  
    
    
    
}

