using System;
using System.Data;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Common;

//This namespace actually point to a Core assembly!
using SolarWinds.NPM.Common.Models;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.NpmAdapters;
using SolarWinds.Orion.Web.Helpers;


public partial class ChartData : System.Web.UI.Page
{
	static readonly Log _log = new Log("ChartData");

	NetObject mOrionNPMObject;
	String _netObjectID;
	String _chartName;
	String _sampleSize;
	DateTime _periodBegin;
	DateTime _periodEnd;
	CustomPoller _customPoller;
	String _rows;
	protected String _dataFormat;
	ChartInfo _chartInfo;
	bool _rawData;
	double _numberOfSamples;
	bool mNoHTML = false;
	String mSampleText;
	String _periodName;

    private bool _isMultiSource;

    protected override void OnPreInit(EventArgs e)
    {
        ResourceInfo resource = ResourceManager.GetResourceByID(Convert.ToInt32(Request.QueryString["ResourceID"]));

        if (null != resource)
            Context.Items[typeof(ViewInfo).Name] = resource.View;

        base.OnPreInit(e);
    }

	public void MyBusinessLayerExceptionHandler(Exception ex)
	{
		_log.Error(ex);
	}

	private void SelectChart()
	{
		_chartName = Request["ChartName"];
		_netObjectID = Request["NetObject"];
		_sampleSize = Request["SampleSize"];
		_periodBegin = Convert.ToDateTime(Request["PeriodBegin"]);
		_periodEnd = Convert.ToDateTime(Request["PeriodEnd"]);
		
		if (!String.IsNullOrEmpty(Request["CustomPollerID"]))
		{
            using(var adapter = NpmAdapterFactory.CreateCustomPollerAdapter())
            {
                _customPoller = adapter.GetCustomPoller(new Guid(Request["CustomPollerID"]));
            }
			_rows = Request["Rows"];
		}

		_dataFormat = (Request["DataFormat"] == null) ? "" : Request["DataFormat"].ToUpperInvariant();

		_rawData = Convert.ToBoolean(Request["RowData"]);
		mSampleText = Request["SampleText"];
		_periodName = WebSecurityHelper.SanitizeHtml(Request["Period"]);
	}

	// Generates a report of the Chart Data instead of the Chart
	public void DoReport()
	{
		bool OK = false;
		SelectChart();

		// TODO: Licensed ?
		//if (!ComPlusLicenseCheck()) {
		//    WriteErrorMessage("Evaluation Period Expired");
		//    return ;
		//}

		_chartInfo = ChartInfoFactory.Create(_chartName);

		_chartInfo.LoadResourceProperties(Request.QueryString);

		

        //FB40012 This should do the trick for broken headers. We just detect we are multisource,
        //that flag is then used when deciding what to use as a header
        _isMultiSource = !String.IsNullOrEmpty(Request["ShowSum"]);
		

		_chartInfo.SetupSubsets();
		if (String.IsNullOrEmpty(_chartName))
		{
			WriteErrorMessage(Resources.CoreWebContent.WEBCODE_TM0_16);
			return;
		}


        if (!string.IsNullOrEmpty(_chartInfo.Poller)
            && !_isMultiSource //FB40235. This is only needed for the old NPM charts.
			&& !_chartInfo.Poller.Equals("Summary", StringComparison.OrdinalIgnoreCase)
			&& !_chartInfo.Poller.Equals("CustomSummary", StringComparison.OrdinalIgnoreCase)
			&& !_chartInfo.Poller.Equals("none", StringComparison.OrdinalIgnoreCase)
			&& !SelectNetObject())
		{
			WriteErrorMessage(Resources.CoreWebContent.WEBCODE_TM0_17);
			return;
		}
		else if (!string.IsNullOrEmpty(_chartInfo.Poller)
			&& (_chartInfo.Poller.Equals("Custom", StringComparison.OrdinalIgnoreCase)
			|| _chartInfo.Poller.Equals("CustomSummary", StringComparison.OrdinalIgnoreCase))
			&& (_chartInfo.CustomPoller == null))
		{
			if (Request["CustomPollerID"].ToString() == new Guid().ToString())
			{
				WriteErrorMessage(Resources.CoreWebContent.WEBCODE_TM0_18);
			}
			else
			{
				WriteErrorMessage(Resources.CoreWebContent.WEBCODE_TM0_19);
			}
			return;
		}


		if ((_periodEnd.Subtract(_periodBegin)).TotalMinutes % _chartInfo.SampleSizeInMinutes == 0)
		{
			_numberOfSamples = (_periodEnd.Subtract(_periodBegin)).TotalMinutes / _chartInfo.SampleSizeInMinutes;
		}
		else
		{
			_numberOfSamples = (_periodEnd.Subtract(_periodBegin)).TotalMinutes / _chartInfo.SampleSizeInMinutes + 1;
		}

		// Too many samples ?
		if (_numberOfSamples > 3000)
		{
			WriteErrorMessage(Resources.CoreWebContent.WEBCODE_TM0_20);
			return;
		}

		// Not Enough Samples ?
		if (_numberOfSamples < 2)
		{
			WriteErrorMessage(Resources.CoreWebContent.WEBCODE_TM0_21);
			return;
		}

		DataSet dataset = _chartInfo.LoadData(true);

		switch (_dataFormat.ToUpper())
		{
			case "CSV":
			case "CDF":
			case "RAW":
				//OK = GenerateRAWReport(reader)
				break;
			case "TEXT":
			case "TXT":
				//OK = GenerateTEXTReport(reader)
				break;
			case "EXCEL":
			case "EXCELL":
			case "XLS":
				OK = GenerateHTMLReport(dataset);
				break;
			case "XML":
				//OK = GenerateXMLReport(reader);
				break;
			default:
				OK = GenerateHTMLReport(dataset);
				break;
		}

		if (!OK)
		{
			WriteErrorMessage(Resources.CoreWebContent.WEBCODE_TM0_22);
			return;
		}

		//If _chartInfo.Rescaleable Then ReScale
	}

	// Generates a report of the Chart data and sends it to the Client browser
	private bool GenerateHTMLReport(DataSet dataset)
	{
		int Columns;

		Columns = _chartInfo.Subsets.Count;
		foreach (Subset subset in _chartInfo.Subsets)
		{
			if (subset.SubsetType.Contains("MINMAX"))
			{
				Columns++;
			}
		}
		Columns += 2;

		//    With Response
		Response.Buffer = true;
		Response.Expires = -1000;
		bool isExcel = _dataFormat == "XLS" || _dataFormat == "EXCEL" || _dataFormat == "EXCELL";
		string text = "";

		if (isExcel)
		{
			_rawData = true;
			Response.ContentType = "application/vnd.ms-excel";
		}
		else
		{
			Response.ContentType = "TEXT/HTML";
		}
		Response.Write("<table border=\"0\" cellspacing=\"0\" cellpadding=\"4\">" + Environment.NewLine);
			
		// Title
		Response.Write("<tr>" + Environment.NewLine);
		text = WebSecurityHelper.SanitizeHtml(_chartInfo.ParseMacro(_chartInfo.Title));
		text = isExcel ? WebSecurityHelper.EscapeExcelFormulas(text) : text;
		Response.Write("<td class=\"ViewHeader\" align=\"center\" colspan=\"" + Columns + "\"><font size=\"3\"><b>" + text + "</b></font></td>" + Environment.NewLine);
		Response.Write("</tr>" + Environment.NewLine);
		// Sub-Title
		Response.Write("<tr>" + Environment.NewLine);
		text = WebSecurityHelper.SanitizeHtml(_chartInfo.ParseMacro(_chartInfo.SubTitle));
		text = isExcel ? WebSecurityHelper.EscapeExcelFormulas(text) : text;
		Response.Write("<td class=\"ViewHeader\" align=\"center\" colspan=\"" + Columns + "\"><b>" + text + "</b></td>" + Environment.NewLine);
		Response.Write("</tr>" + Environment.NewLine);
		// Sub-Title #2
		Response.Write("<tr>" + Environment.NewLine);
		text = WebSecurityHelper.SanitizeHtml(_chartInfo.ParseMacro(_chartInfo.SubTitle2)).Replace(Environment.NewLine, "<br>");
		text = isExcel ? WebSecurityHelper.EscapeExcelFormulas(text) : text;
		Response.Write("<td class=\"ViewHeader\" align=\"center\" colspan=\"" + Columns + "\"><b>" + text + "</b></td>" + Environment.NewLine);
		Response.Write("</tr>" + Environment.NewLine);

		// Setup Subsets and Output Header
		Response.Write("<tr>" + Environment.NewLine);
		Response.Write("<td class=\"ReportHeader\" colspan=\"2\">" + Resources.CoreWebContent.WEBCODE_TM0_24 + "</td>" + Environment.NewLine);

		CustomPollerAssignment customPollerAssignment = new CustomPollerAssignment();

		if (_chartInfo.CustomPoller != null)
		{
            using (INpmCustomPollerAdapter adapter = NpmAdapterFactory.CreateCustomPollerAdapter())
                customPollerAssignment = adapter.GetCustomPollerAssignmentForNetObject(_netObjectID, _chartInfo.CustomPoller.CustomPollerID);
		}
		foreach (DataTable datatable in dataset.Tables)
		{
			String label = String.Empty;

            if (_isMultiSource)
            {
                if (datatable.Rows[0]["Label"] != null)
                {
                    label = datatable.Rows[0]["Label"].ToString();
                }
            }
            else
            {
                if (_chartInfo.CustomPoller != null)
                {
                    string rowId = datatable.Rows[0]["Label"].ToString();

                    if (!string.IsNullOrEmpty(rowId))
                    {
                        using (INpmCustomPollerAdapter adapter = NpmAdapterFactory.CreateCustomPollerAdapter())
                        {
                            label = adapter.GetLabelAndRowIdString(customPollerAssignment, rowId);
                        }
                    }
                }
            }

			foreach (Subset subset in _chartInfo.Subsets)
			{
				if (mOrionNPMObject != null)
				{
					// if (mOrionNPMObject.ObjectType == "Interface") {
					if (NetObjectHelper.IsInterfaceNetObject(mOrionNPMObject.NetObjectID))
					{
						subset.InBandwidth = Convert.ToDouble(mOrionNPMObject["InBandwidth"]);
						subset.OutBandwidth = Convert.ToDouble(mOrionNPMObject["OutBandwidth"]);
					}
				}
				if (subset.SubsetType.ToUpper().Contains("MINMAX"))
				{
					Response.Write("<td colspan=\"2\" class=\"ReportHeader\">" + _chartInfo.ParseMacro(subset.Name) + " " + label +  "</td>" + Environment.NewLine);
				}
				else
				{
					Response.Write("<td class=\"ReportHeader\">" + _chartInfo.ParseMacro(subset.Name) + " " + label + "</td>" + Environment.NewLine);
				}
			}
		}
		Response.Write("</tr>" + Environment.NewLine);

		if (dataset == null)
		{
			WriteErrorMessage(Resources.CoreWebContent.WEBCODE_TM0_23);
			return false;
		}

        // this should hapend for instance if you are plotting transformation of pollers a,b where result = &{a} + &{b} and one of the
        // pollers is unassigned from the device
        if (dataset.Tables.Count == 0)
        {
            return false;
        }

		for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
		{
			Response.Write("<tr>" + Environment.NewLine);
			Response.Write("      <td width=\"70\" class=\"Property\">" + Utils.FormatMediumDate(Convert.ToDateTime(dataset.Tables[0].Rows[i]["DateTime"])) + "</td>" + Environment.NewLine);
			if (_chartInfo.SampleSizeInMinutes < 24 * 60)
			{
				Response.Write("      <td width=\"70\" class=\"Property\">" + Utils.FormatToLocalTime(Convert.ToDateTime(dataset.Tables[0].Rows[i]["DateTime"])) + "</td>" + Environment.NewLine);
			}
			else
			{
				Response.Write("      <td class=\"Property\">&nbsp;</td>" + Environment.NewLine);
			}

			foreach (DataTable table in dataset.Tables)
			{
				foreach (Subset subset in _chartInfo.Subsets)
				{
					if (subset.SubsetType.ToUpper().Contains("MINMAX"))
					{
						if (subset.SubsetType.Contains("%"))
						{
							Response.Write("      <td class=\"Property\">" + CookData("%", table.Rows[i][subset.Field2].ToString()) + "</td>" + Environment.NewLine);
							Response.Write("      <td class=\"Property\">" + CookData("%", table.Rows[i][subset.Field].ToString()) + "</td>" + Environment.NewLine);
						}
						else
						{
							Response.Write("      <td class=\"Property\">" + CookData(subset.Field2, table.Rows[i][subset.Field2].ToString()) + "</td>" + Environment.NewLine);
							Response.Write("      <td class=\"Property\">" + CookData(subset.Field, table.Rows[i][subset.Field].ToString()) + "</td>" + Environment.NewLine);
						}
					}
					else if (subset.SubsetType.Contains("%"))
					{
						Response.Write("      <td class=\"Property\">" + CookData("%", table.Rows[i][subset.Field].ToString()) + "</td>" + Environment.NewLine);
					}
					else 
					{
						Response.Write("      <td class=\"Property\">" + CookData(subset.Field, table.Rows[i][subset.Field].ToString()) + "</td>" + Environment.NewLine);
					}
				}
			}
			Response.Write("</tr>" + Environment.NewLine);

		}

		Response.Write("</tr>" + Environment.NewLine);

		Response.Write("</table>" + Environment.NewLine);
		return _chartInfo.GetValidSamples() > 0;
	}

    private void SetupSubsets()
	{
		//int S = 0;
		Single InBandwidth = 0;
		Single OutBandwidth = 0;

		if (mOrionNPMObject != null)
		{
			if (NetObjectHelper.IsInterfaceNetObject(mOrionNPMObject.NetObjectID))
			{
				InBandwidth = Convert.ToSingle(mOrionNPMObject["InBandwidth"]);
				OutBandwidth = Convert.ToSingle(mOrionNPMObject["OutBandwidth"]);
			}
		}

		foreach (Subset subset in _chartInfo.Subsets)
		{
			subset.InBandwidth = InBandwidth;
			subset.OutBandwidth = OutBandwidth;
			//.SubsetLabels(S) = ParseMacro(Subset.Name)
			if (_customPoller != null)
			{
				if (String.IsNullOrEmpty(subset.SubsetType))
				{
					switch (_customPoller.PollerType)
					{
						case CustomPollerType.Counter:
						case CustomPollerType.Rate:
						case CustomPollerType.RawValue:
                        case CustomPollerType.Calculation:
							subset.SubsetType = "AVG";
							break;
					}
				}
				else
				{
					if (String.IsNullOrEmpty(subset.Field))
					{
						switch (subset.SubsetType.ToUpper())
						{
							case "MIN":
								if (_customPoller.PollerType == CustomPollerType.RawValue || _customPoller.PollerType == CustomPollerType.Calculation)
								{
                                    subset.SetField("RawStatus", false);
								}
								else
								{
                                    subset.SetField("MinRate", false);
								}
								break;
							case "MAX":
                                if (_customPoller.PollerType == CustomPollerType.RawValue || _customPoller.PollerType == CustomPollerType.Calculation)
								{
                                    subset.SetField("RawStatus", false);
								}
								else
								{
									subset.SetField("MaxRate", false);
								}
								break;
							case "AVG":
                                if (_customPoller.PollerType == CustomPollerType.RawValue || _customPoller.PollerType == CustomPollerType.Calculation)
								{
                                    subset.SetField("RawStatus", false);
								}
								else
								{
                                    subset.SetField("AvgRate", false);
								}
								break;
						}
					}
				}
				if (String.IsNullOrEmpty(subset.Field) && subset.SubsetType.Length != 0)
				{
					switch (_customPoller.PollerType)
					{
						case CustomPollerType.Counter:
						case CustomPollerType.Rate:
                            subset.SetField("AvgRate", false);
							subset.ScalingFactor = _customPoller.TimeMultiplier();
							break;
						case CustomPollerType.RawValue:
                        case CustomPollerType.Calculation:
                            subset.SetField("RawStatus", false);
							subset.ScalingFactor = _customPoller.TimeMultiplier();
							break;
					}
				}
			}
			//S++;
		}
	}

	// Convert the raw chart data into a human readable "cooked" form
	private object CookData(String Property, String Value)
	{
		if (_rawData)
		{
			return String.Format("{0:0.00}", Value);//VB6.Format(Value, "0.00");
		}
		else
		{
			return Macros.DataMacro(Property, Value, true, !mNoHTML);
		}
	}

	public void WriteErrorMessage(String Text)
	{
		Response.Write("<font class=text color=Red><b>" + Text.Replace(Environment.NewLine, "<br>") + "</b></font>");
	}

	private bool SelectNetObject()
	{
		if (!string.IsNullOrEmpty(_netObjectID))
		{
			mOrionNPMObject = NetObjectFactory.Create(_netObjectID);
			if (mOrionNPMObject == null)
				return false;
			else
				return true;
		}
		else
		{
			return false;
		}
	}

	// Looks up an OrionNPMObject given the ID
	public NetObject GetNetObject(String id)
	{
		String ObjectType = id;

		// Licensed ?
		// TODO: 
		// If Not ComPlusLicenseCheck Then Exit Function

		// Strip off object type
		if (id.Contains(":"))
		{
			ObjectType = id.Substring(0, 2);
		}
		switch (ObjectType.ToUpper())
		{
			case "N:":
				return new SolarWinds.Orion.NPM.Web.Node(id);
			case "I:":
				return new SolarWinds.Orion.NPM.Web.Interface(id);
			#region VB6 Code
			//Case "V:"
			//    Set GetNetObject = GetVolume(id)
			//Case "A:"
			//    Set GetNetObject = GetApplication(id)
			//Case "B:"
			//    Set GetNetObject = GetModem(id)
			//Case "C:"
			//    Set GetNetObject = GetWirelessClient(id)
			//Case "W:"
			//    Set GetNetObject = GetWirelessSession(id)
			//Case "S:"
			//    Set GetNetObject = GetNetworkService(id)
			//'******************
			//' Netflow Stuff
			//Case "NC:"
			//    Set NetObject = CreateObject("NetflowCOMInterop.NetflowConversation")
			//    NetObject.NetObjectID = id
			//    Set GetNetObject = NetObject
			//Case "NP:"
			//    Set NetObject = CreateObject("NetflowCOMInterop.NetflowProtocol")
			//    NetObject.NetObjectID = id
			//    Set GetNetObject = NetObject
			//Case "NE:"
			//    Set NetObject = CreateObject("NetflowCOMInterop.NetflowEndpoint")
			//    NetObject.NetObjectID = id
			//    Set GetNetObject = NetObject
			//Case "NA:"
			//    Set NetObject = CreateObject("NetflowCOMInterop.NetflowApplication")
			//    NetObject.NetObjectID = id
			//    Set GetNetObject = NetObject
			//Case "ND:"
			//    Set NetObject = CreateObject("NetflowCOMInterop.NetflowDomain")
			//    NetObject.NetObjectID = id
			//    Set GetNetObject = NetObject
			//Case "NL1:"
			//    Set NetObject = CreateObject("NetflowCOMInterop.NetflowCountry")
			//    NetObject.NetObjectID = id
			//    Set GetNetObject = NetObject
			//Case "NG:"
			//    Set NetObject = CreateObject("NetflowCOMInterop.NetflowIPAddressGroup")
			//    NetObject.NetObjectID = id
			//    Set GetNetObject = NetObject
			//Case "NN:"
			//    Set NetObject = CreateObject("NetflowCOMInterop.NetflowNode")
			//    NetObject.NetObjectID = id
			//    Set GetNetObject = NetObject
			//Case "NI:"
			//    Set NetObject = CreateObject("NetflowCOMInterop.NetflowInterface")
			//    NetObject.NetObjectID = id
			//    Set GetNetObject = NetObject
			#endregion
			default:
				_log.Info("Can't get NetObject for prefix " + ObjectType.ToUpper());
				return null;
		}
	}

	public String ReformatDateTime(DateTime V)
	{
		//If gSQLServer Then
		// SQL Server is Funky. You can use a double to represent a Date/Time, but you have to
		// Subtract 2 from it first !!
		return Convert.ToString(V, System.Globalization.CultureInfo.InvariantCulture);// (Convert.ToDouble(V) - 2).ToString().Replace(",", ".");
		//Else
		//    ReformatDateTime = Replace(CDbl(V), ",", ".")
		//End If
	}

	// Parses a Macro using the current NetObject
	public String ParseMacro(String Macro)
	{
		_log.Info(String.Format("ParseMacro: '{0}'", Macro));
		String S1;
		String S2 = null;
		String Parm;
		int Endbrace;
		String ParseMacro;

		if (!Macro.Contains("${"))
		{
			return Macro;
		}

		Macro = ParseCommonMacros(Macro);
		Macro = Macro.Replace("${SampleSize}", mSampleText);
		Macro = Macro.Replace("${Period}", _periodName);
		if (_customPoller != null)
		{
			Macro = Macro.Replace("${CustomPollerUnit}", _customPoller.GetUnit());
			Macro = Macro.Replace("${CustomPollerUnitAbbv}", _customPoller.UnitAbbv());
			Macro = Macro.Replace("${CustomPollerUnitWithTime}", _customPoller.GetUnit(true));
			Macro = Macro.Replace("${CustomPollerUnitAbbvWithTime}", _customPoller.UnitAbbv(true));
			Macro = Macro.Replace("${CustomPollerName}", _customPoller.UniqueName);
		}
		ParseMacro = Macro;

		if (mOrionNPMObject == null)
		{
			return ParseMacro;
		}

		S1 = Macro;
		while (S1.Contains("${"))
		{
			//S2 = S2 + Mid$(S1, 1, InStr(S1, "${") - 1)
			S2 = S2 + S1.Substring(0, S1.IndexOf("${"));
			//S1 = Mid$(S1, InStr(S1, "${") + 2)
			S1 = S1.Substring(S1.IndexOf("${") + 2);

			Endbrace = S1.IndexOf("}");
			if (Endbrace > 0)
			{
				//Parm = Mid$(S1, 1, Endbrace - 1)
				Parm = S1.Substring(0, Endbrace);
				//S1 = Mid$(S1, Endbrace + 1)
				S1 = S1.Substring(Endbrace + 1);
				//S2 = S2 & mOrionNPMObject.GetProperty(Parm, True, False)
				S2 = S2 + mOrionNPMObject[Parm];
			}
			else
			{
				S2 = S2 + S1;
				break;
			}
		}
		return S2 + S1;
	}

	public String ParseCommonMacros(String Macro)
	{
		_log.Info(String.Format("ParseCommonMacros: '{0}'", Macro));
		String S1 = null;
		String S2 = null;
		String Parm;
		int Endbrace;

		S1 = Macro;
		while (S1.Contains("${"))
		{
			//    S2 = S2 & Mid$(S1, 1, InStr(S1, "${") - 1)
			S2 = S2 + S1.Substring(0, S1.IndexOf("${"));
			//    S1 = Mid$(S1, InStr(S1, "${") + 2)
			S1 = S1.Substring(S1.IndexOf("${") + 2);

			Endbrace = S1.IndexOf("}");
			if (Endbrace > 0)
			{
				Parm = S1.Substring(0, Endbrace);
				S1 = S1.Substring(Endbrace + 1);

				switch (Parm.ToUpper())
				{
					#region VB6 Code
					//            ' Time Periods
					//            Case "CURRENT", "LAST15MINUTES"
					//                S2 = S2 & FormatToLocalTime(DateAdd("n", -15, Now))
					//            Case "PASTHOUR", "PAST HOUR", "LASTHOUR", "LAST HOUR"
					//                S2 = S2 & FormatToLocalTime(Int(Now) + TimeSerial(Hour(Now) - 1, 0, 0))
					//            Case "PAST2HOURS", "LAST2HOURS", "PAST 2 HOURS", "LAST 2 HOURS"
					//                S2 = S2 & FormatToLocalTime(Int(Now) + TimeSerial(Hour(Now) - 2, 0, 0))
					//            Case "PAST24HOURS", "LAST24HOURS", "PAST 24 HOURS", "LAST 24 HOURS"
					//                S2 = S2 & FormatToLocalTime(Int(Now) + TimeSerial(Hour(Now) - 24, 0, 0))
					//            Case "TODAY"
					//                S2 = S2 & Format(Now, "Short Date")
					//            Case "YESTERDAY"
					//                S2 = S2 & DateAdd("d", -1, Int(Now))
					//            Case "LAST7DAYS", "PAST7DAYS", "PAST 7 DAYS", "LAST 7 DAYS"
					//                S2 = S2 & DateAdd("d", -7, Int(Now))

					//            ' Time Periods - in double format
					//            Case "PASTHOUR-D", "PAST HOUR-D", "LASTHOUR-D", "LAST HOUR-D"
					//                S2 = S2 & ReformatDateTime(Int(Now) + TimeSerial(Hour(Now) - 1, 0, 0))
					//            Case "PAST2HOURS-D", "LAST2HOURS-D", "PAST 2 HOURS-D", "LAST 2 HOURS-D"
					//                S2 = S2 & ReformatDateTime(Int(Now) + TimeSerial(Hour(Now) - 2, 0, 0))
					//            Case "PAST24HOURS-D", "LAST24HOURS-D", "PAST 24 HOURS-D", "LAST 24 HOURS-D"
					//                S2 = S2 & ReformatDateTime(Int(Now) + TimeSerial(Hour(Now) - 24, 0, 0))
					//            Case "TODAY-D"
					//                S2 = S2 & ReformatDateTime(Int(Now))
					//            Case "YESTERDAY-D"
					//                S2 = S2 & ReformatDateTime(DateAdd("d", -1, Int(Now)))
					//            Case "LAST7DAYS-D", "PAST7DAYS-D", "PAST 7 DAYS-D", "LAST 7 DAYS-D"
					//                S2 = S2 & ReformatDateTime(DateAdd("d", -7, Int(Now)))

					//            ' Dates and Times
					//            Case "DATETIME", "DATE TIME", "DATE/TIME"
					//                 S2 = S2 & Format(Now, "Short Date") & " " & FormatToLocalTime(Now)
					//            Case "DATE"
					//                S2 = S2 & Format(Now, "Short Date")
					//            Case "LONGDATE", "LONG DATE"
					//                S2 = S2 & Format(Now, "Long Date")
					//            Case "MEDIUMDATE", "MEDIUM DATE"
					//                S2 = S2 & Format(Now, "Medium Date")
					//            Case "SHORTDATE", "SHORT DATE"
					//                S2 = S2 & Format(Now, "Short Date")
					//            Case "TIME"
					//                S2 = S2 & Format(Now, "Short Time")
					//            Case "LONGTIME", "LONG TIME"
					//                S2 = S2 & Format(Now, "Long Time")
					//            Case "MEDIUMTIME", "MEDIUM TIME"
					//                S2 = S2 & Format(Now, "Medium Time")
					//            Case "SHORTTIME", "SHORTTIME"
					//                S2 = S2 & Format(Now, "Short Time")
					//            Case "DOW", "DAY OF WEEK", "DAYOFWEEK"
					//                S2 = S2 & Format(Now, "dddd")
					//            Case "D", "DAY"
					//                S2 = S2 & Format(Now, "d")
					//            Case "DD"
					//                S2 = S2 & Format(Now, "dd")
					//            Case "ABREVIATEDDOW", "ABREVIATED DAY OF WEEK", "ABREVIATEDDAYOFWEEK"
					//                S2 = S2 & Format(Now, "ddd")
					//            Case "LOCALDOW", "LOCAL DAY OF WEEK", "LOCALDAYOFWEEK"
					//                S2 = S2 & Format(Now, "aaaa")
					//            Case "MONTH", "M"
					//                S2 = S2 & Format(Now, "M")
					//            Case "MM"
					//                S2 = S2 & Format(Now, "mm")
					//            Case "MMM", "ABBREVIATEDMONTH", "ABBREVIATED MONTH"
					//                S2 = S2 & Format(Now, "mmm")
					//            Case "MMMM", "MONTHNAME", "MONTH NAME"
					//                S2 = S2 & Format(Now, "mmmm")
					//            Case "LOCALMONTHNAME", "LOCAL MONTH NAME"
					//                S2 = S2 & Format(Now, "oooo")
					//            Case "DAYOFYEAR", "DAY OF YEAR"
					//                S2 = S2 & Format(Now, "y")
					//            Case "YEAR2", "YEAR 2"
					//                S2 = S2 & Format(Now, "yy")
					//            Case "YEAR4", "YEAR 4", "YEAR"
					//                S2 = S2 & Format(Now, "yyyy")
					//            Case "H", "HOUR"
					//                S2 = S2 & Format(Now, "H")
					//            Case "HH"
					//                S2 = S2 & Format(Now, "Hh")
					//            Case "N"
					//                S2 = S2 & Format(Now, "N")
					//            Case "Nn", "MINUTE"
					//                S2 = S2 & Format(Now, "Nn")
					//            Case "S"
					//                S2 = S2 & Format(Now, "S")
					//            Case "Ss", "SECOND"
					//                S2 = S2 & Format(Now, "Ss")
					//            Case "AMPM", "AM/PM"
					//                S2 = S2 & Format(Now, "AM/PM")
					//            Case "VERSION"
					//                S2 = S2 & "Version " & App.Major & "." & App.Minor & "." & App.Revision
					//            Case "COPYRIGHT"
					//                S2 = S2 & App.LegalCopyright
					//            Case "RELEASE"
					//                S2 = S2 & App.LegalTrademarks
					//            Case "APPLICATION"
					//                S2 = S2 & App.FileDescription
					#endregion
					case "CR":
					case "CRLF":
						S2 = S2 + Environment.NewLine;
						break;
					default:
						S2 = S2 + "${" + Parm + "}";
						break;
				}
			}
			else
			{
				S2 = S2 + S1;
				break;
			}
		}
		return S2 + S1;
	}
}
