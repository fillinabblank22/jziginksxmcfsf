<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/OrionMasterPage.master" CodeFile="CustomChart.aspx.cs" Inherits="CustomChart" EnableEventValidation="false" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconLinkExportToPDF runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="placeHolder" runat="server">
    <asp:PlaceHolder ID="holder" runat="server" />
</asp:Content>

