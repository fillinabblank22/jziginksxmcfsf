<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EntityTooltipCustomizationControl.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EntityTooltipCustomizationControl" %>
<div>
<div ><%= DefaultSanitizer.SanitizeHtml(!string.IsNullOrEmpty(this.EntityDisplayName) ? this.EntityDisplayName : this.EntityName) %></div>
<asp:TextBox runat="server" ID="tooltipPatternText" TextMode="MultiLine" Width="200" Height="100"></asp:TextBox>
</div>