<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ForecastingProfilingInProgress.ascx.cs" Inherits="Orion_NetPerfMon_Controls_ForecastingProfilingInProgress" %>

<div style="width: 100%; background-color: #e7f3ff; padding-bottom: 10px;">
            <table>
                <tr>
                    <td style="border-bottom: none; width: 120px;">
                        <img src="/Orion/images/ForecastingIcons/monitor.png" style="padding-left:20px;" />
                    </td>
                    <td style="border-bottom: none;">
                        <p>
                            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LF0_Profiling) %></b>
                        </p>
                        <p>
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LF0_Profiling2) %>
                        </p>
                        <p>
                            <%= DefaultSanitizer.SanitizeHtml(ProfilingDays) %>
                        </p>
                        <p>
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LF0_Profiling4) %>
                        </p>
                    </td>
                </tr>
            </table>
            <div style="width: 100%;">
                <div style="width: 95%; background-color: white; height: 24px; border: 1px solid #7b797b;
                    margin: 0 auto;">
                    <div style="width: <%= DefaultSanitizer.SanitizeHtml(ProfilingPercentDone) %>%; height: 24px; background-color: #adaead; border-right: 1px solid #7b797b;">
                    </div>
                </div>
                <div style="width: 100%; text-align: right;">
                    <span style="padding-right: 3%;"><b>
                        <%= DefaultSanitizer.SanitizeHtml(RemainingText) %></b></span>
                </div>
            </div>
        </div>