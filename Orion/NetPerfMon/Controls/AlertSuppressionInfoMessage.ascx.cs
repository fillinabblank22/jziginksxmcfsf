﻿using SolarWinds.Orion.Core.Common.Models.Alerts;
using SolarWinds.Orion.Web.InformationService;
using System.Web.UI;

public partial class Orion_NetPerfMon_Controls_AlertSuppressionInfoMessage : UserControl
{
    public void SetSuppressAlertsMessageVisibility(string entityUri)
    {
        alertSuppressionMessage.Visible = false;
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            var state = proxy.Invoke<EntityAlertSuppressionState[]>("Orion.AlertSuppression", "GetAlertSuppressionState", new object[] {new[] {entityUri}});
            if (state != null && state.Length >= 1)
            {
                var mode = state[0].SuppressionMode;
                alertSuppressionMessage.Visible = mode == EntityAlertSuppressionMode.SuppressedByItself || mode == EntityAlertSuppressionMode.SuppressedByParent;
            }
        }
    }
}