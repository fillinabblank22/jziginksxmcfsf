﻿using System;
using System.Data;
using System.Linq;
using SolarWinds.MapEngine.Enums;
using SolarWinds.MapEngine.Model;

public partial class Orion_NetPerfMon_Controls_NetworkMapLegend : System.Web.UI.UserControl
{
    public MapLegend<MapLegendItem> MapLegend { get; set; }
    public bool HideExpectedResultsLink { get; set; }

    public void SetMapStyleAttributes(MapStyle mapStyle, string resourceId)
    {
        string attribute = string.Format("data-map-style-{0}", resourceId);
        mapLegendWrapper.Attributes.Add(attribute, mapStyle.ToString());
        mapLegendWrapper.ID = mapLegendWrapper.ID + "-" + resourceId + "-" + mapStyle.ToString();
    }

    public void ShowLegend()
    {
        mapLegendWrapper.Style.Clear();
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (MapLegend == null)
        {
            mapLegendContainer.Visible = false;
        }
        else
        {
            mapLegendContainer.Visible = true;

            MapLegendItem[] legendItems = MapLegend.Items.OrderBy(item => item.OrderInUILegend).ToArray<MapLegendItem>();
            var tblLegend = new DataTable();
            tblLegend.Columns.Add(new DataColumn("color"));
            tblLegend.Columns.Add(new DataColumn("borderstyle"));
            tblLegend.Columns.Add(new DataColumn("text"));

            for (int i = 0; i < legendItems.Length; i++)
            {
                DataRow legendRow = tblLegend.NewRow();
                legendRow["color"] = System.Drawing.ColorTranslator.ToHtml(legendItems[i].Color);
                if (legendItems[i].BorderType != SolarWinds.MapEngine.Enums.MapLegendItemBorderType.None)
                {
                    legendRow["borderstyle"] = string.Format("border: 1px {0} {1};", legendItems[i].BorderType, System.Drawing.ColorTranslator.ToHtml(legendItems[i].BorderColor));
                }
                else
                {
                    legendRow["borderstyle"] = string.Empty;
                }

                legendRow["text"] = legendItems[i].Text;
                tblLegend.Rows.Add(legendRow);
            }

            mapLegendItems.DataSource = tblLegend;
            mapLegendItems.DataBind();
            if (!string.IsNullOrEmpty(MapLegend.Text))
            {
                lblLegendLabel.Text = string.Format(Resources.CoreWebContent.WEBCODE_PS0_12, MapLegend.Text);
            }

            LblLegend.Visible = !HideExpectedResultsLink;
        }
    }
}