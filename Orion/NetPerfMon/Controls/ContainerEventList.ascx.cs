using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Federation;

public partial class Orion_Controls_ContainerEventList : System.Web.UI.UserControl
{
    private const int defaultMaxEventCount = 25;
    private const string defaultPeriod = "Last 12 Months";

    private string subTitle = "";

    private string periodName = "";
    public string PeriodName 
    { 
        get 
        { 
            return periodName; 
        } 
        set 
        { 
            periodName = value; 
        } 
    }
    public int MaxEventCount{ get; set; }
    public int ContainerId{ get; set; }
    public int ResourceID { get; set; }

    public void LoadData()
    {
        var startDate = new DateTime();
        var endDate = new DateTime();

        if (MaxEventCount <= 0)
        {
            MaxEventCount = defaultMaxEventCount;
        }

        if (String.IsNullOrEmpty(periodName))
        {
            periodName = defaultPeriod;
        }
        
        Periods.Parse(ref periodName, ref startDate, ref endDate);
        subTitle = Periods.GetLocalizedPeriod(periodName);

        DataTable eventsTable = EventDAL.GetContainerEvents(ContainerId, MaxEventCount, startDate, endDate, false);

        EventsGrid.DataSource = eventsTable;
        EventsGrid.DataBind();
    }

    public string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_AK0_71; }
    }

    public string HelpLinkFragment
    {
        get { return "OrionAPMPHAppDetailsContEvents"; }
    }

    public string SubTitle
    {
        get
        {
            return subTitle;
        }
    }

	public string EditControlLocation
	{
		get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditContainerEventList.ascx"; }
	} 
}
