﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.MapEngine;
using SolarWinds.MapEngine.Helpers;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web;
using SolarWinds.MapStudio.Web;

public partial class Orion_NetPerfMon_Controls_MapTooltipCustomizer : System.Web.UI.UserControl
{
    public const string TooltipsSettingsKey = "MapStudio-CustomTooltips";
    public IEngine MapEngine { get; set; }
    
    SolarWinds.MapStudio.Web.CustomTooltips CustomizedTooltips { get; set; }

    protected override void OnInit(EventArgs e)
    {
        string setting = string.Empty;
        try
        {
            setting = WebSettingsDAL.Get(TooltipsSettingsKey);
            CustomizedTooltips = CustomTooltips.DeserializeFromString(setting);
        }
        catch (ArgumentNullException)
        {
            CustomizedTooltips = new SolarWinds.MapStudio.Web.CustomTooltips();
        }
        catch (KeyNotFoundException)
        {
            CustomizedTooltips = new SolarWinds.MapStudio.Web.CustomTooltips();
        }

        string rootLocalPath = MapService.GetTempPath();
        string applicationLocalPath = MapService.GetMapStudioInstallPath();

        string swoisEndpoint = string.Format(System.Configuration.ConfigurationManager.AppSettings["SWOISv3.RemoteEndpoint"], "localhost");
        string serverAddress, swisPort, swisPostfix;
        MapService.ParseEndpointLocation(swoisEndpoint, out serverAddress, out swisPort, out swisPostfix);

        string userName = HttpContext.Current.Profile.UserName;
        var credentials = CredentialsHelper.GetOrionCertificateCredentials();
        MapEngine = MapEngineFactory.GetEngine(userName, credentials, serverAddress, swisPort, swisPostfix, 0, true);
        MapEngine.RootApplicationPath = applicationLocalPath;
        MapEngine.RootLocalPath = rootLocalPath;
	MapEngine.SetAlternativeMapWebPath();
        
        if (MapEngine != null)
        {
            foreach (var entity in MapEngine.EntitiesMetadata)
            {
                Orion_NetPerfMon_Controls_EntityTooltipCustomizationControl ctrl = (Orion_NetPerfMon_Controls_EntityTooltipCustomizationControl)LoadControl("~/Orion/NetPerfMon/Controls/EntityTooltipCustomizationControl.ascx");
                ctrl.ID = entity.Key;
                ctrl.EntityName = entity.Key;
                ctrl.EntityDisplayName = entity.Value.DisplayNamePlural;
                if (!IsPostBack)
                {
                    ctrl.TooltipPattern = CustomizedTooltips[entity.Key];
                }
                mainContainer.Controls.Add(ctrl);
            }
            
        }
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    public void SaveSettings()
    {
        
        foreach (Control ctrl in mainContainer.Controls)
        {
            var customizationControl = ctrl as Orion_NetPerfMon_Controls_EntityTooltipCustomizationControl;
            if (customizationControl != null)
            {
                CustomizedTooltips[customizationControl.EntityName] = customizationControl.TooltipPattern;
            }
        }
        WebSettingsDAL.Set(TooltipsSettingsKey, CustomizedTooltips.SerializeToString());
    }

    protected override void OnPreRender(EventArgs e)
    {
        if (MapEngine != null)
        {
            MapEngine.Close();
        }
        base.OnPreRender(e);
    }
}
