<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditCustomNodeChart.ascx.cs"
    Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditCustomNodeChart" %>
<%@ Register TagPrefix="orion" TagName="EditSampleSize" Src="~/Orion/Controls/SampleSizeControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/PeriodControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="ShowTrend" Src="~/Orion/Controls/ShowTrendControl.ascx" %>
<script type="text/javascript" language="javascript">
    $(function () {
        if (typeof (TitleTextBoxID) != 'undefined') {
            $('#<%=ListOfCharts.ClientID %>').change(function () {
                var displayText = $('#<%=ListOfCharts.ClientID %> option:selected').text();
                $('#' + TitleTextBoxID).val(displayText);
            });
        }
    });  
</script>
<div style="font-weight: bold;">
    <asp:Label runat="server" ID="ListOfChartsLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_293) %></asp:Label>
</div>
<div class="sw-res-editor-row">
    <asp:DropDownList runat="server" ID="ListOfCharts" />
</div>
<div>
    <orion:EditPeriod runat="server" ID="Period" InLine="true"/>
</div>
<div>
    <orion:EditSampleSize runat="server" InLine="true" ID="SampleSize" />
</div>
<div>
    <orion:ShowTrend ID="showTrend" Checked="true" runat="server" InLine="true"/>
</div>
