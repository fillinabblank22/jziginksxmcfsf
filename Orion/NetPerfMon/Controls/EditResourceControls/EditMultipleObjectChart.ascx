<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditMultipleObjectChart.ascx.cs"
    Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditMultipleObjectChart" %>
<%@ Register TagPrefix="orion" TagName="ListCharts" Src="~/Orion/NetPerfMon/Controls/ListCharts.ascx" %>
<%@ Register TagPrefix="orion" TagName="ListEntities" Src="~/Orion/NetPerfMon/Controls/ListEntities.ascx" %>
<%@ Register TagPrefix="orion" TagName="TopXX" Src="~/Orion/NetPerfMon/Controls/TopXXControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditSampleSize" Src="~/Orion/Controls/SampleSizeControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/PeriodControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditAutoHide" Src="~/Orion/Controls/AutoHideControl.ascx" %>

<style type="text/css">
    .leftcolumn
    {
        font-weight: bold;
        text-align: left;
        white-space: nowrap;
    }
</style>
<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <table cellpadding="2" cellspacing="2" width="100%">
            <tr>
                <td class="leftcolumn">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_333) %>
                </td>
                <td colspan="2">
                    <asp:DropDownList runat="server" ID="ddl_ObjectTypes" OnSelectedIndexChanged="ddl_ObjectTypes_SelectedIndexChanged"
                        AutoPostBack="true">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="leftcolumn">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_293) %>
                </td>
                <td colspan="2">
                    <orion:ListCharts runat="server" ID="lc_ListCharts" />
                </td>
            </tr>
            
            <tr>
                <td>
                </td>
                <td>
                    <table>
                        <tr>
                            <td valign="top">
                                <asp:RadioButton ID="AutoSelect" runat="server" Checked="false" GroupName="SelectObjects"
                                    AutoPostBack="true" OnCheckedChanged="SelectObjects_Change" />
                            </td>
                            <td>
                                <%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBDATA_AK0_341, Entities.ToLower())) %> 
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RadioButton ID="ManualSelect" runat="server" Checked="true" GroupName="SelectObjects"
                                    AutoPostBack="true" OnCheckedChanged="SelectObjects_Change" />
                            </td>
                            <td>
                                <%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBDATA_AK0_336, Entities.ToLower())) %>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <orion:LocalizableButton ID="bt_SelectObject" runat="server" DisplayType="Small" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_254 %>" OnClick="SelectObjects_Click"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td class="formRightInput" colspan="2">
                    <orion:ListEntities runat="server" ID="ListEntities" />
                </td>
            </tr>
            <tr>
                <td class="leftcolumn">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_334) %>
                </td>
                <td class="formRightInput" colspan="2">
                    <orion:TopXX runat="server" ID="TopXX" />
                </td>
            </tr>
            <tr>
                <td class="leftcolumn">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_335) %>
                </td>
                <td class="formRightInput">
                    <asp:DropDownList ID="ShowSum" runat="server">
                        <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_338 %>" Value="NoSum"></asp:ListItem>
                        <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_339 %>" Value="OnlySum"></asp:ListItem>
                        <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_340 %>" Value="AlsoSum"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <orion:EditPeriod runat="server" ID="Period" PeriodLabel="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_337 %>" />
            <tr>
            <td colspan="3">
                <orion:EditSampleSize runat="server" ID="SampleSize" HelpfulText="" />
            </td>
            </tr>
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 10%">
                </td>
                <td style="width: 50%">
                </td>
            </tr>
            <orion:EditAutoHide runat="server" ID="AutoHide" Description=" " />
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
