<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectCustomNodeProperty.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_SelectCustomNodeProperty" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers"%>

    <a href="<%= DefaultSanitizer.SanitizeHtml(HelpHelper.GetHelpUrl("OrionAGCustomProperties")) %>" style="color: #0000FF" target="_blank">
		<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_363) %>
	</a>
    
    <p>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_364) %>
        <br />
        <asp:ListBox runat="server" ID="lbxCustomProps" Rows="1" SelectionMode="single" />
    </p>