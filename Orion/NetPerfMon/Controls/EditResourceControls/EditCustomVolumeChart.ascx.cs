﻿using System;
using SolarWinds.Orion.Web.UI;
using System.Collections.Generic;
using SolarWinds.Orion.Web.Charting;
using System.Web.UI.WebControls;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditCustomVolumeChart : BaseResourceEditControl
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		Dictionary<string, string> list = ChartXMLConfigManager.GetChartSelection("V", Resource.Properties["ChartName"]);

		foreach (string key in list.Keys)
			ListOfCharts.Items.Add(new ListItem(list[key], key));

		if (!string.IsNullOrEmpty(Resource.Properties["ChartName"]))
			ListOfCharts.SelectedValue = Resource.Properties["ChartName"];

        if (System.Web.HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
        {
            ListOfCharts.Attributes.Add("onchange", "javascript:SaveData('ChartName', this.value);");
        }
	}

	public override Dictionary<string, object> Properties
	{
		get 
		{
			Dictionary<string, object> properties = new Dictionary<string, object>();

            if (ListOfCharts.Items.Count == 0)
                return properties;

            properties.Add("ChartName", ListOfCharts.SelectedValue);
			properties.Add("Period", Period.PeriodName);
			properties.Add("SampleSize", SampleSize.SampleSizeValue);

			return properties;
		}
	}

	public override string DefaultResourceTitle
	{
		get
		{
			return ListOfCharts.SelectedItem.Text;
		}
	}
}