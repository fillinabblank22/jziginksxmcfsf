<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditGroupRootCause.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditGroupRootCause" %>
<p>
	<b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_317) %></b>
	<br/>
	<asp:DropDownList ID="ctrMembersCount" runat="server">
		<asp:ListItem Text="20" Value="20"/>
		<asp:ListItem Text="40" Value="40"/>
		<asp:ListItem Text="60" Value="60"/>
		<asp:ListItem Text="80" Value="80"/>
		<asp:ListItem Text="100" Value="100"/>
		<asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_318 %>" Value="-1"/>
	</asp:DropDownList>
</p>