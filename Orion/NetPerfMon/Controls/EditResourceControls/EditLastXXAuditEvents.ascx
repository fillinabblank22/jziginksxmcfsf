<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditLastXXAuditEvents.ascx.cs"
    Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditLastXXAuditEvents" %>

<style type="text/css">
        .error {
            border: 2px solid red;
        }
        .spaced_checkbox label {
            padding-left: 5px;
        }
        .alternateRow {
            background-color: #f8f8f8;
        }
        #adminContentTable 
        {
        	font-family: Arial, Verdana, Helvetica, sans-serif;
        	background-color: #FFFFFF;
            border: 1px solid #ecedee;
            text-align: left;
            vertical-align: middle;
            white-space: normal;
            width: auto;
        }
        .vertical-space
        {
            padding-top: 10px;
        }
    </style>

<script type="text/javascript">
    function selectAll(invoker) {
        var rp = invoker.parentNode.parentNode.parentNode.parentNode;
        var inputElements = rp.getElementsByTagName('input');

        for (var i = 0; i < inputElements.length; i++) {
            var myElement = inputElements[i];

            // Filter through the input types looking for checkboxes
            if (myElement.type === "checkbox") {

                // Use the invoker (our calling element) as the reference 
                //  for our checkbox status
                myElement.checked = invoker.checked;
            }
        }
    }

    function setUpRootCb(container) {
        var selectedAll = true;
        container.find('[id*=chkbx]').each(function () {
            selectedAll &= this.checked;
        });

        container.find('[id*=rootCB]').each(function () {
            this.checked = selectedAll;
        });
    }
    $(function () {
        setUpRootCb($("#<%=AuditTypeContainer.ClientID%>"));
    });
</script>

<table border="0">
    <tr>
        <td>
            <asp:Label runat="server" ID="label" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_281 %>"
                Font-Bold="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox runat="server" ID="MaxEventsText" Width="50" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RangeValidator ID="EventsRangeValidator" runat="server" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_319 %>"
                Display="Dynamic" ControlToValidate="MaxEventsText" MinimumValue="1" MaximumValue="5000"
                Type="Integer" />
        </td>
    </tr>
    <tr>
        <td class="vertical-space">
            <asp:Label runat="server" ID="label1" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_263 %>" Font-Bold="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:DropDownList runat="server" ID="Period" />
        </td>
    </tr>
    <tr>
        <td class="vertical-space">
            <div runat="server" id="AuditTypeContainer">
                <asp:Repeater ID="auditTypes" runat="server" OnItemDataBound="AuditTypes_ItemDataBound">
                    <HeaderTemplate>
                        <table width="100%">
                            <tr>
                                <td style="vertical-align: top; text-align: left;">
                                    <table style="width: 100%!important" id="adminContentTable">
                                        <tr class="alternateRow">
                                            <td style="font-weight: bold;">
                                                <asp:CheckBox runat="server" Checked="true" CssClass="spaced_checkbox" ID="rootCB"
                                                    OnClick="javascript:selectAll(this);" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_SO0_50 %>" />
                                            </td>
                                        </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td style="text-align: left;">
                                <asp:CheckBox ID="chkbx" runat="server" Checked="true" CssClass="spaced_checkbox"
                                    OnClick="javascript:setUpRootCb($(this).parent().parent().parent().parent());" />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table></td>
                        <td>
                            <asp:CustomValidator ID="RequiredColumn" runat="server" OnServerValidate="ColumnsValidation"
                                ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_SO0_49 %>" EnableClientScript="false"
                                Display="Dynamic">
                            </asp:CustomValidator>
                        </td>
                        </tr></table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </td>
    </tr>
</table>
