﻿using System;
using System.Linq;
using System.Text;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web.UI;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common;

public partial class Orion_NetPerfMon_Controls_EditCustomPropertyList : BaseResourceEditControl
{
	private Dictionary<string, object> _properties = new Dictionary<string, object>();
	private const char separator = ',';

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		_properties.Add("PropertyList", string.Empty);

		string tableName;
	    bool isEntity = false;
		if (NetObjectHelper.IsNodeNetObject(NetObjectID))
            tableName = "NodesCustomProperties";
		else if (NetObjectHelper.IsInterfaceNetObject(NetObjectID))
			tableName = "Interfaces";
        else if (NetObjectHelper.IsContainerNetObject(NetObjectID))
        {
            tableName = "Orion.GroupCustomProperties";
            isEntity = true;
        }
        else
            tableName = "Volumes";

	    IEnumerable<CustomProperty> customProperties = null;
        if (isEntity)
            customProperties = from cp in CustomPropertyMgr.GetCustomPropertiesForEntity(tableName)
                               where cp.IsUsageAllowed(CustomPropertyUsage.IsForEntityDetailName)
                               orderby cp.PropertyName
                               select cp;
        else	customProperties = from cp in CustomPropertyMgr.GetCustomPropertiesForTable(tableName)
                               where cp.IsUsageAllowed(CustomPropertyUsage.IsForEntityDetailName)
							   orderby cp.PropertyName
							   select cp;

		if (customProperties.Count() > 0)
		{
			this.selectedProperties.DataSource = customProperties;
			this.selectedProperties.DataTextField = "PropertyName";
			this.selectedProperties.DataValueField = "PropertyName";
			this.selectedProperties.DataBind();
			return;
		}
		this.noPropertiesMessage.Visible = true;
		this.propertiesList.Visible = false;
	}

	protected void SelectedProperties_DataBound(object sender, EventArgs e)
	{
		if (!Resource.Properties.ContainsKey("PropertyList"))
		{
			// by default all properties should be selected
			foreach (ListItem item in this.selectedProperties.Items)
				item.Selected = true;
		}
		else
		{
			string properties = Resource.Properties["PropertyList"];
			if (!String.IsNullOrEmpty(properties))
				foreach (string property in (properties.Split(separator)))
				{
					ListItem item = this.selectedProperties.Items.FindByValue(property.Trim());
					if (item != null)
						item.Selected = true;
				}
		}

        if (System.Web.HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
        {
            StringBuilder value = new StringBuilder();
            var plus = "";
            for (var i =0; i< this.selectedProperties.Items.Count; i++)
            {
                value.AppendFormat("{1}(this.rows[{0}].children[0].children[0].checked ? this.rows[{0}].children[0].children[1].innerHTML + ',' : '')", i, plus);
                plus = "+";
            }
            selectedProperties.Attributes.Add("onclick", "javascript:SaveData('PropertyList', " + value + ");");
        }
	}

	public override Dictionary<string, object> Properties
	{
		get 
		{
			List<string> selected = new List<string>();
			foreach (ListItem item in this.selectedProperties.Items)
			{
				if (item.Selected)
				{
					selected.Add(item.Value);
				}
			}
			_properties["PropertyList"] = String.Join(separator.ToString(), selected.ToArray());

			return _properties;
		}
	}
}
