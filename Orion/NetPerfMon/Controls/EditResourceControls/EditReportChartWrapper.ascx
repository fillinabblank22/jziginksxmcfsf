﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditReportChartWrapper.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditReportChartWrapper" %>
<%@ Register Src="~/Orion/Reports/Controls/ReportChartConfiguration.ascx" TagPrefix="orion" TagName="ReportChartConfiguration" %>

<orion:ReportChartConfiguration runat="server" ID="reportChartConfigruration" />
<asp:Panel runat="server" ID="PreviewPanel"></asp:Panel>

