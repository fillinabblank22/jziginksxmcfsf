<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditNodeTree.ascx.cs"
    Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditNodeTree" %>

<%@ Register TagPrefix="orion" TagName="FilterNodesSql" Src="~/Orion/Controls/FilterNodesSql.ascx" %>

<script type="text/javascript">
       function ClientValidate(source, arguments) {
           var lbxGroup1 = $('#<%= this.lbxGroup1.ClientID %>');
           var lbxGroup2 = $('#<%= this.lbxGroup2.ClientID %>');

           if (arguments.Value != lbxGroup1[0].value
                && arguments.Value != lbxGroup2[0].value)
               arguments.IsValid = true;
           else
               arguments.IsValid = false; 
       }
</script>

    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_342) %>
<p>
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_343) %><br />
    <asp:ListBox runat="server" ID="lbxGroup1" SelectionMode="single" Rows="1" />
</p>
<p>
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_344) %><br />
    <asp:ListBox runat="server" ID="lbxGroup2" SelectionMode="single" Rows="1" />
    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="lbxGroup2"
        ControlToCompare="lbxGroup1" Type="String" Operator="NotEqual" Display="Dynamic"
        ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_351 %>"></asp:CompareValidator>
</p>
<p>
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_345) %><br />
    <asp:ListBox runat="server" ID="lbxGroup3" SelectionMode="single" Rows="1" />
    <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="lbxGroup3" 
        ClientValidationFunction="ClientValidate" Display="Dynamic" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_351 %>"></asp:CustomValidator>
</p>
<b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_346) %></b>
<asp:RadioButtonList runat="server" ID="GroupNulls">
    <asp:ListItem Value="true" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_347 %>"/>
    <asp:ListItem Value="false" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_348 %>"/>
</asp:RadioButtonList>
<p>
    <asp:CheckBox runat="server" ID="RememberCollapseState" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_280 %>" />
</p>
<orion:FilterNodesSql runat="server" ID="SQLFilter" />