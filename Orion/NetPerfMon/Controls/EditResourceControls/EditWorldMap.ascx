<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditWorldMap.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditWorldMap" %>

<orion:Include runat="server" File="WorldMap.js" Section="Bottom" />
<orion:Include runat="server" File="WorldMap.css" />

<orion:InlineCss ID="InlineCss1" runat="server">
    #mapdlg.ui-dialog-content { padding: 0; }
    .filterContainer {padding-top: 10px; padding-bottom:10px }
    .filterContainer .filterItem span {display: block }
    .filterContainer .filterItem input{width: 400px }
</orion:InlineCss>

<script type="text/javascript">
    (function () {

        var domids = { hintLng: 'hintLng', hintLat: 'hintLat', hintZoom: 'hintZoom', ctrlLng: '<%= longitudectrl.ClientID %>', ctrlLat: '<%= latitudectrl.ClientID %>', ctrlZoom: '<%= zoomctrl.ClientID %>', ctrlHeight: '<%= heightctrl.ClientID %>', btnUseLoc: '<%= uselocation.ClientID %>' };

        var mapwidth = <%= this.ResourceWidth %>;

        var localeParseFloat = function(s) {
            s = s || '';
            var dec = window.Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator;
            s = s.replace(dec, '.');
            return parseFloat(s);
        };

        var updatecoords = function (lat, lng, zoom) {

            var dec = window.Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator;

            if ( dec != '.') {
                lat = ('' + lat).replace('.', dec);
                lng = ('' + lng).replace('.', dec);
            }

            $('#' + domids.hintLng + ' span').text('' + lng);
            $('#' + domids.hintLat + ' span').text('' + lat);
            $('#' + domids.hintZoom + ' span').text('' + zoom);
            $('#' + domids.ctrlLng).val('' + lng);
            $('#' + domids.ctrlLat).val('' + lat);
            $('#' + domids.ctrlZoom).val('' + zoom);
            $('#hintLat,#hintLng,#hintZoom').show();
        };

        var map;
        var locationtemplate = '<%= HttpUtility.JavaScriptStringEncode(Resources.CoreWebContent.WEBDATA_PCC_WM_ER_COORDS) %>';
        var lastlocation;
        var callback;

        var updatelocation = function(e) {
            var center = map.leaflet().getCenter();
            var zoom = map.leaflet().getZoom();
            lastlocation = { 0: L.Util.formatNum( center.lat, 5), 1: L.Util.formatNum( SW.Core.WorldMap.Viewer.NormalizeLng(center.lng), 5), 2: zoom };

            $('#maplocation').text( L.Util.template( locationtemplate, lastlocation ) );
        };

        var picklocation = function(e,cb) {

            callback = cb;

            e.preventDefault();
            e.stopPropagation();
            
            var n = parseInt($('#' + domids.ctrlHeight).val(), 10);
            
            if (isNaN(n) || n < 80 ) return;

            $('#mapselector').width(mapwidth).height(n);

            var coords = { lng: localeParseFloat($('#' + domids.ctrlLng).val()), lat: localeParseFloat($('#' + domids.ctrlLat).val()), zoom: localeParseFloat($('#' + domids.ctrlZoom).val()) };

            if (isNaN(coords.lng) || isNaN(coords.lat)) {
                coords.lng = -97.7261353; 
                coords.lat = 30.28753;
                coords.wantlocate = true;
            }
            
            if (isNaN(coords.zoom)) {
                coords.zoom = 8;
            }

            $('#mapdlg').dialog({position: 'center',
                width: mapwidth + 20,
                height: 'auto',
                modal: true,
                title: '<%= HttpUtility.JavaScriptStringEncode( Resources.CoreWebContent.WEBDATA_PCC_WM_ER_SETLOC) %>',
                show: false,
                close: function () {
                }
            });

            if (!map) {
                map = SW.Core.WorldMap.Viewer.Create('mapselector', { lat: coords.lat, lng: coords.lng, zoom: coords.zoom, tileset: '<%= HttpUtility.JavaScriptStringEncode(Tileset) %>' });
                map.leaflet().on('moveend', updatelocation);
                map.leaflet().on('viewreset', updatelocation);
                

            } else {
                map.leaflet().setView(coords, coords.zoom );
                map.leaflet().invalidateSize(false);
            }

            updatelocation();
            $('#mapdlg').show(0);

            if (coords.wantlocate) {
                map.leaflet().locate({ setView: true, maxZoom: 11 });
            }
        };

        $(function () {

            $('#<%= SelectLocation.ClientID %>').click(function(e) { return picklocation(e, updatecoords); });

            $('#' + domids.btnUseLoc).click(function() {
                if (callback && lastlocation)
                    callback(lastlocation['0'], lastlocation['1'], lastlocation['2']);
                $('#locationrequired').hide();
                $('#mapdlg').dialog('close');
            });
            
           
            $("#<%= SupportedEntities.ClientID %>"+' input').bind('change', function() {
                var entity = $(this).prop('value');
                $(".filterContainer  [entity='" + entity + "']").parent('div.filterItem').css('display', $(this).is(':checked') ? 'block' : 'none');
            });
      

            var toggleclick = function(btnId, divId) {
                var bid = btnId, did = divId;
                return function() {
                    var btn = $('#' + bid);
                    if (btn.attr('data-expanded') == 1) {
                        // collapse
                        btn.attr('data-expanded', 0);
                        btn.attr('src', '/Orion/images/Button.Expand.gif');
                        $('#' + did).slideUp();
                    } else {
                        // expand
                        btn.attr('data-expanded', 1);
                        btn.attr('src', '/Orion/images/Button.Collapse.gif');
                        $('#' + did).slideDown();
                    }
                };
            };
            
            // custom position?
            if ($('#' + domids.hintLng + ' span').text() !== '0' || $('#' + domids.hintLat + ' span').text() !== '0') {
                $('#hintLat,#hintLng,#hintZoom').show();
            }

            $('#advancedBtn').click( toggleclick('advancedBtn', 'advcontrols') );
            $('#advancedBtn2').click( toggleclick('advancedBtn2', 'examples') );
        });
    })();
</script>

<div class="sw-res-editor-row">
    <div><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_WM_ER_MAPHEIGHT) %></b></div>
    <div><asp:TextBox runat="server" id="heightctrl" style="width: 50px;" /> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_WM_ER_PX) %>  <span class="helpfulText" style="vertical-align: inherit;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_WM_ER_DEFAULTHEIGHT) %></span></div>
    
    <asp:RequiredFieldValidator runat="server" ControlToValidate="heightctrl" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PCC_WM_ER_MAPHEIGHT_VALIDATE %>" Display="Dynamic" />
    <asp:RangeValidator runat="server" ControlToValidate="heightctrl" MinimumValue="80" MaximumValue="4000"  ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PCC_WM_ER_MAPHEIGHT_VALIDATE %>" Type="Integer" Display="Dynamic" />
</div>

<div class="sw-res-editor-row">
    <div><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_WM_ER_LOCATION) %></b></div>
    <div style="margin-top: 6px;">
        <orion:LocalizableButton runat="server" ID="SelectLocation" OnClientClick="return false;" DisplayType="Secondary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PCC_WM_ER_SETLOC %>" />

        <asp:PlaceHolder runat="server" ID="locationhint" Visible="False">
            <span id="hintLat" class="helpfulText" style="display: none; vertical-align: inherit; margin-left: 10px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_WM_ER_LATITIDE2) %> <span><%: latitudectrl.Text %></span></span>
            <span id="hintLng" class="helpfulText" style="display: none; vertical-align: inherit; margin-left: 10px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_WM_ER_LONGITUDE2) %> <span><%: longitudectrl.Text %></span></span>
            <span id="hintZoom" class="helpfulText" style="display: none; vertical-align: inherit; margin-left: 10px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_WM_ER_ZOOM2) %> <span><%= DefaultSanitizer.SanitizeHtml(zoomctrl.Text) %></span></span>
        </asp:PlaceHolder>
        
        <asp:PlaceHolder runat="server" ID="locationrequiredhint" Visible="False">
            <div id="locationrequired" class='sw-suggestion sw-suggestion-warn'><span class='sw-suggestion-icon'></span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_WM_ER_LOCREQUIRED) %></div>
        </asp:PlaceHolder>
    </div>
</div>

<div runat="server" ID="SectionAdvanced" class="sectionHeader">
    <img src="/Orion/images/Button.Expand.gif" id="advancedBtn" data-expanded="0" alt=""/> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_216) %>
</div>

<div id="advcontrols" style="display: none;">
    <div class="sw-res-editor-row">
        <div style="display: inline-block; margin-left: 16px;">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_WM_ER_LATITIDE2) %> <asp:TextBox runat="server" id="latitudectrl" style="width: 90px;" />
        </div>

        <div style="display: inline-block; margin-left: 16px;">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_WM_ER_LONGITUDE2) %> <asp:TextBox runat="server" id="longitudectrl" style="width: 90px;" />
        </div>

        <div style="display: inline-block; margin-left: 16px;">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_WM_ER_ZOOM2) %> <asp:TextBox runat="server" id="zoomctrl" style="width: 40px;" />
        </div>
    </div>
    
    <div>
        <asp:RequiredFieldValidator runat="server" ControlToValidate="latitudectrl" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PCC_WM_ER_LATITIDE_VALIDATE %>" Display="Dynamic" EnableClientScript="False" />
        <asp:RangeValidator runat="server" ControlToValidate="latitudectrl" MinimumValue="-90" MaximumValue="90"  ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PCC_WM_ER_LATITIDE_VALIDATE %>"  Type="Double" Display="Dynamic" EnableClientScript="False" />
        <asp:RequiredFieldValidator runat="server" ControlToValidate="longitudectrl" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PCC_WM_ER_LONGITUDE_VALIDATE %>" Display="Dynamic" EnableClientScript="False" />
        <asp:RangeValidator runat="server" ControlToValidate="longitudectrl" MinimumValue="-180" MaximumValue="180"  ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PCC_WM_ER_LONGITUDE_VALIDATE %>" Type="Double" Display="Dynamic" EnableClientScript="False" />
        <asp:RequiredFieldValidator runat="server" ControlToValidate="zoomctrl" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PCC_WM_ER_ZOOM_VALIDATE %>" Display="Dynamic" EnableClientScript="False" />
        <asp:RangeValidator runat="server" ControlToValidate="zoomctrl" MinimumValue="1" MaximumValue="18"  ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PCC_WM_ER_ZOOM_VALIDATE %>" Type="Integer" Display="Dynamic" EnableClientScript="False" />
    </div>
</div>

<div class="sw-res-editor-row" style="margin-top: 10px;">
    <asp:CheckBoxList runat="server" ID="SupportedEntities" ></asp:CheckBoxList>
    <div class="filterContainer">   
    <asp:Repeater ID="EntityFilters" runat="server">
      <ItemTemplate>
      <asp:Panel runat="server" CssClass="filterItem" Style='<%# DefaultSanitizer.SanitizeHtml(bool.Parse(Eval("Enabled").ToString())? "display:block" : "display:none") %>' ID="FilterItem" >       
        <b><asp:Label runat="server"><%# DefaultSanitizer.SanitizeHtml(Eval("text")) %></asp:Label></b>
        <asp:TextBox ID="Filter" Entity= '<%# DefaultSanitizer.SanitizeHtml(Eval("Attributes[Entity]")) %>' runat="server" text='<%# DefaultSanitizer.SanitizeHtml(Eval("value")) %>'></asp:TextBox>
        <asp:CustomValidator runat="server" ID="swqlvalidator" ControlToValidate="Filter" Entity= '<%# DefaultSanitizer.SanitizeHtml(Eval("Attributes[Entity]")) %>'  OnServerValidate="SWQLValidate" Display="Dynamic" />     
      </asp:Panel>
      </ItemTemplate>
    </asp:Repeater>
    </div>   

    <div>
        <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_285) %></p>
    </div>

    <div ID="FilterTips" class="sectionHeader">
        <img src="/Orion/images/Button.Expand.gif" id="advancedBtn2" data-expanded="0" alt=""/> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_WM_ER_EXAMPLES) %>
    </div>

    <div id="examples" style="display: none;">
        <ul>
            <li style="margin-bottom: 4px;"><%= HttpUtility.HtmlAttributeEncode(Resources.CoreWebContent.WEBDATA_PCC_WM_ER_EXAMPLES1) %><span class="sw-text-helpful" style="margin-left: 30px;"><%= HttpUtility.HtmlAttributeEncode(Resources.CoreWebContent.WEBDATA_PCC_WM_ER_EXAMPLES4) %></span></li>
            <li style="margin-bottom: 4px;"><%= HttpUtility.HtmlAttributeEncode(Resources.CoreWebContent.WEBDATA_PCC_WM_ER_EXAMPLES2) %></li>
            <li style="margin-bottom: 4px;"><%= HttpUtility.HtmlAttributeEncode(Resources.CoreWebContent.WEBDATA_PCC_WM_ER_EXAMPLES3) %></li>
        </ul>
    </div>

</div>

<div id="mapdlg" style="display: none; background-color: white">
    <div style="margin: 8px 10px 0px 10px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_196) %></div>
    <div class="sw-suggestion" style="margin: 8px 10px 4px 10px; padding-top: 2px; padding-bottom: 3px;"><span class="sw-suggestion-icon"></span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_197) %><img class="leaflet-control-zoom leaflet-bar" style="background-color: rgb(238,238,238); vertical-align: middle;" src="/orion/images/worldmap/icon-map-reset.png"  alt=""/></div>
    <div id="mapselector" style="margin: 8px 10px 4px 10px;"></div>
    <div id="maplocation" class="sw-text-helpful" style="margin-left: 10px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_WM_ER_COORDS) %></div>
    <div class="sw-btn-bar-wizard" style="padding: 0; margin: 0 10px 10px 0;">
        <orion:LocalizableButton ID="uselocation" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PCC_WM_ER_USEASDEFAULT %>" OnClientClick="return false;"/>
        <orion:LocalizableButton runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClientClick="$('#mapdlg').dialog('close'); return false;" />
    </div>
</div>
