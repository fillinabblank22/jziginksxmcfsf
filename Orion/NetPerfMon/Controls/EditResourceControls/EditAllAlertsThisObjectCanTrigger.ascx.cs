﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditAllAlertsThisObjectCanTrigger : BaseResourceEditControl
{
    private const string ENABLE_RELATED_ALERTS_RADIO_PROPERTY_KEY = "enable_related_alerts_radio";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.radioButtonEnableRelatedAlerts.Items.FindByValue(Resource.Properties.ContainsKey(ENABLE_RELATED_ALERTS_RADIO_PROPERTY_KEY)
            ? Resource.Properties[ENABLE_RELATED_ALERTS_RADIO_PROPERTY_KEY] : "True").Selected = true;
    }
    
    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            properties.Add(ENABLE_RELATED_ALERTS_RADIO_PROPERTY_KEY, this.radioButtonEnableRelatedAlerts.SelectedValue);

            return properties;
        }
    }
}