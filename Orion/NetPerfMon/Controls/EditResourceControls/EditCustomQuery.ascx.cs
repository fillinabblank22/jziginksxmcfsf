﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditCustomQuery : BaseResourceEditControl
{
    private const string _propertySwql = "SWQL";
    private const string _propertySearchSwql = "SearchSWQL";
    private const string _propertyRowsPerPage = "RowsPerPage";
    private const string _propertyDisableSWQLEdit = "DisableSWQLEdit";
    private const string _defaultRowsPerPage = "5";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (Resource.Properties.ContainsKey(_propertySwql))
        {
            SWQL.Text = Resource.Properties[_propertySwql];
        }

        if (Resource.Properties.ContainsKey(_propertySearchSwql))
        {
            SearchSWQL.Text = Resource.Properties[_propertySearchSwql];
            if (!string.IsNullOrEmpty(SearchSWQL.Text))
            {
                EnableSearchCheckbox.Checked = true;
                SearchSWQLBox.CssClass = string.Empty;
            }
        }

        if (Resource.Properties.ContainsKey(_propertyDisableSWQLEdit))
        {
            SWQLEditor.Visible = false;
        }

        RowsPerPage.Text = Resource.Properties[_propertyRowsPerPage] ?? _defaultRowsPerPage;
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            var properties = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase)
            {
                {_propertySwql, SWQL.Text},
                {_propertySearchSwql, SearchSWQL.Text},
                {_propertyRowsPerPage, RowsPerPage.Text}
            };
            return properties;
        }
    }
}