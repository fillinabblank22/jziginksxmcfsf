<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditVMVareChart.ascx.cs"
    Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditVMVareChart" %>
<%@ Register TagPrefix="orion" TagName="EditSampleSize" Src="~/Orion/Controls/SampleSizeControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/TimePeriodControl.ascx" %>

<table width="750px" cellpadding="8" border="0">
    <tr>
        <td colspan="3">
            <table class="text" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_355) %>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="chartSubTitle2" Width="200"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <orion:EditPeriod runat="server" ID="timePeriodControl" />
    <br />
    <orion:EditSampleSize runat="server" ID="sampleSizeControl" />
</table>
