﻿using System;
using System.Collections.Generic;
using ASP;

using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditTopXX : BaseResourceEditControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Resource.Properties["Type"] != EditNetObjectType.Interfaces.ToString())
        {
            this.NodesColumns.Visible = true;
            this.InterfacesColumns.Visible = false;
            this.VolumesColumns.Visible = true;
        }
        else
        {
            this.NodesColumns.Visible = true;
            this.InterfacesColumns.Visible = true;
            this.VolumesColumns.Visible = false;
        }

        this.NetObjectType = GetLocalizedProperty("Entity", String.IsNullOrEmpty(this.Resource.Properties["Type"]) ? "Nodes" : this.Resource.Properties["Type"]);

        if (!this.IsPostBack)
        {
            int max;
            if (!Int32.TryParse(this.Resource.Properties["MaxRecords"], out max))
            {
                this.Resource.Properties["MaxRecords"] = "10";
            }
            this.maxCount.Text = this.Resource.Properties["MaxRecords"];
            this.filter.Text = this.Resource.Properties["Filter"];
        }

        this.nodeColumns.DataSource = WebDAL.GetNodeColumnNames();
        this.nodeColumns.DataBind();

        if (InterfacesColumns.Visible)
        {
            this.interfaceColumns.DataSource = WebDAL.GetInterfaceColumnNames();
            this.interfaceColumns.DataBind();
        }

        if (VolumesColumns.Visible)
        {
            this.volumeColumns.DataSource = WebDAL.GetVolumeColumnNames();
            this.volumeColumns.DataBind();
        }
    }

	public override Dictionary<string, object> Properties
	{
		get
		{
			Dictionary<string, object> properties = new Dictionary<string, object>();

            //TODO: FB235926 we need to remove this when we change Top XX resouces to properly use Resource.Title. 
            var titleEditControl = ControlHelper.FindControlRecursive(this.Page, "TitleEditControl");
            if (titleEditControl != null)
            {
                properties["Title"] = ((EditResourceTitle)titleEditControl).ResourceTitle;
                properties["SubTitle"] = ((EditResourceTitle)titleEditControl).ResourceSubTitle;
            }
            properties["MaxRecords"] = this.maxCount.Text;
            properties["Filter"] = this.filter.Text;

		    return properties;
		}
	}

    private string netObjectType;

    protected string NetObjectType
    {
        get { return this.netObjectType; }
        set { this.netObjectType = value; }
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
}
