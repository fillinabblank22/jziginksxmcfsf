﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditForecastingThresholds : BaseResourceEditControl, IChartEditorSettings
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
    }

    public void Initialize(ChartSettings settings, ResourceInfo resourceInfo, string netObjectId)
    {

    }

    protected string GetHintText
    {
        get
        {
            return string.Format(Resources.CoreWebContent.WEBDATA_LF0_ForecastChartEditHint,
                NodeId == "" ? "" : String.Format("<a href=\"/Orion/Nodes/NodeProperties.aspx?Nodes={0}&ReturnTo={1}\">", NodeId, ReturnUrl),
                "<a href=\"/Orion/NetPerfMon/Admin/NetPerfMonSettings.aspx\">",
                NodeId == "" ? "" : "</a>",
                "<br />",
                "</a>"
                );
        }
    }

    private string nodeId = "";
    protected string NodeId
    {
        get
        {
            if (nodeId == "")
            {
                string[] netObj = NetObjectID.Split(new char[] { ':' }, 2);
                if ((netObj.Length == 2) && (netObj[0] == "N"))
                {
                    nodeId = netObj[1];
                }
            }
            return nodeId;
        }
    }

    public void SaveProperties(Dictionary<string, object> properties)
    {
    }

    protected string ReturnUrl
    {
        get { return ReferrerRedirectorBase.GetReturnUrl(); }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            return new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
        }
    }


}