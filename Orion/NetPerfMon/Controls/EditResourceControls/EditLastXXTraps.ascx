<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditLastXXTraps.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditLastXXTraps" %>
<%@ Register TagPrefix="orion" TagName="EditFilter" Src="~/Orion/Controls/FilterNodesSql.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/PeriodControl.ascx" %>

<table cellspacing="0" cellpadding="0">
    <tr><td colspan="3">
        <b><%= Resources.SyslogTrapsWebContent.WEBDATA_TM0_7 %></b><br />
        <input runat="server" id="maxMessages" size="5"/>
    </td></tr>
    <tr>
        <td>
            <asp:RangeValidator ID="MsgRangeValidator" runat="server" ErrorMessage="<%$ Resources: SyslogTrapsWebContent,WEBDATA_VB0_559 %>"
                Display="Dynamic" ControlToValidate="maxMessages" MinimumValue="1" MaximumValue="5000"
                Type="Integer" />
        </td>
    </tr>
    <orion:EditPeriod runat="server" ID="periodEditor" PeriodLabel="<%$ Resources: SyslogTrapsWebContent, WEBDATA_TM0_8 %>" InLine="true"/>
    <tr><td colspan="3" width="750px">
        <orion:EditFilter runat="server" ID="filterEditor" />
    </td></tr>
    </table>