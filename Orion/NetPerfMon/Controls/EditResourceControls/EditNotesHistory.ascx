<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditNotesHistory.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditNotesHistory" %>

<table border="0">
    <tr>
        <td>
            <asp:Label runat="server" ID="label" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_JV0_16 %>" Font-Bold="true" />
        </td>
    </tr>
     <tr>
        <td>
            <asp:TextBox runat="server" ID="NumberOfNotesText" Width="50" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RangeValidator ID="EventsRangeValidator" runat="server" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_JV0_17 %>"
                Display="Dynamic" ControlToValidate="NumberOfNotesText" MinimumValue="2" MaximumValue="100"
                Type="Integer" />
        </td>
    </tr>
</table>