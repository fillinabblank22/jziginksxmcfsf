<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditNodeIPAddresses.ascx.cs" 
    Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditNodeIPAddresses" %>

<p>
    <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_PSR_4) %></b><br/>
    <asp:TextBox runat="server" ID="RowsPerPage" ></asp:TextBox>
</p>
