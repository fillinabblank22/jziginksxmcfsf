<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditAllAlerts.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditAllAlerts" EnableViewState="true" %>

<table border="0">
    <tr>
        <td style="text-align: right">
            <asp:Label runat="server" ID="label1" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_24 %>" Font-Bold="true" />
        </td>
        <td style="padding-left: 10px">
            <asp:CheckBox ID="ShowAckAlertsCheckBox" runat="server" />
        </td>
    </tr>
</table>