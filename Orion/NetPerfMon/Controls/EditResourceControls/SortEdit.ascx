<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SortEdit.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_SortEdit" EnableViewState="true" %>
<table border="0">
    <tr>
        <td style="font-weight: bold">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_365) %>
        </td>
    </tr>
    <tr>
        <td>
            <asp:DropDownList runat="server" ID="SortItems" Width="180px" />
        </td>
    </tr>
</table>