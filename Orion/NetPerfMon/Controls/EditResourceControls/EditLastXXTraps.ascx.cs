﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditLastXXTraps : BaseResourceEditControl
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		if (this.Resource != null)
		{
			this.filterEditor.FilterTextBox.Text = this.Resource.Properties["Filter"];

			if (!String.IsNullOrEmpty(this.Resource.Properties["Period"]))
			{
				this.periodEditor.PeriodName = this.Resource.Properties["Period"];
			}

			if (!String.IsNullOrEmpty(this.Resource.Properties["MaxMessages"]))
			{
				this.maxMessages.Value = this.Resource.Properties["MaxMessages"];
			}
			else
			{
				this.maxMessages.Value = DefaultNumberOfMessages;
			}
		}
	}

	private const string DefaultNumberOfMessages = "25";
	public override Dictionary<string, object> Properties
	{
		get
		{
			Dictionary<string, object> properties = new Dictionary<string, object>();
			properties["Period"] = this.periodEditor.PeriodName;
			properties["Filter"] = this.filterEditor.FilterTextBox.Text;

			try
			{
				Int32.Parse(this.maxMessages.Value);
				properties.Add("MaxMessages", this.maxMessages.Value);
			}
			catch
			{
				properties.Add("MaxMessages", DefaultNumberOfMessages);
			}
			return properties;
		}
	}
}
