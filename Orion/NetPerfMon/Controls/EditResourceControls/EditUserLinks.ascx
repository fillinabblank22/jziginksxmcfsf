<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditUserLinks.ascx.cs"
    Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditUserLinks" %>
<table border="0" >
    <tr>
        <td colspan="">
            <hr class="Property" />
        </td>
    </tr>
    <tr>
        <td>
        <table border="0" cellspacing="2px">
            <asp:Repeater runat="server" ID="Links">
                <ItemTemplate>
                    <tr>
                        <td>
                            
                                <tr>
                                    <td style="white-space:nowrap;">
                                        <%# DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBDATA_AK0_349, Eval("ID"))) %>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TB_Name" runat="server" Width="320px" Text='<%# DefaultSanitizer.SanitizeHtml(Eval("Link")) %>'></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="white-space:nowrap;">
                                        <%# DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBDATA_AK0_350, Eval("ID"))) %>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TB_URL" runat="server" Width="320px" Text='<%# DefaultSanitizer.SanitizeHtml(Eval("URL")) %>'></asp:TextBox><br/>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:CheckBox runat="server" ID="NewWindow" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_354 %>" Checked='<%# (bool)Eval("NW") %>' />
                                    </td>
                                </tr>
                           
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <hr class="Property" />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            </table>
        </td>
    </tr>
</table>