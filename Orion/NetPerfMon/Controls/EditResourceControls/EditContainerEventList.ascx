<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditContainerEventList.ascx.cs"
    Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditContainerEventList" %>
<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/PeriodControl.ascx" %>

<table border="0">
    <tr>
        <table border="0">
                <tr>
                    <td>
                        <asp:Label runat="server" ID="label" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_281 %>" Font-Bold="true" />
                    </td>
                    <td style="padding-left: 10px">
                        <asp:TextBox runat="server" ID="MaxEventsText" Width="50" />
                    </td>
                    <td colspan="2">
                     <asp:RangeValidator ID="EventsRangeValidator" runat="server" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_282 %>"
                        Display="Dynamic" ControlToValidate="MaxEventsText" MinimumValue="1" MaximumValue="5000"
                         Type="Integer" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" ID="label1" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_263 %>" Font-Bold="true" />
                    </td>
                    <td style="padding-left: 10px">
                        <asp:DropDownList runat="server" ID="Period" />
                    </td>
                </tr>
         </table>
    </tr>
</table>
