﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Enums;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditAllContainers : BaseResourceEditControl
{
    private const string GroupingResourcePropertiesKey = "Grouping";
    private const string RememberCollapseStateResourcePropertiesKey = "RememberCollapseState";
	private const string OrderingResourcePropertiesKey = "Ordering";
    private const string ViewModeResourcePropertiesKey = "ViewMode";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.RememberCollapseState.Checked = Boolean.Parse(Resource.Properties[RememberCollapseStateResourcePropertiesKey] ?? "true");
        this.GroupingDropDown.SelectedValue = Resource.Properties[GroupingResourcePropertiesKey];
        this.OrderingDropDown.SelectedValue = Resource.Properties[OrderingResourcePropertiesKey];
        this.ViewMode.SelectedValue = Resource.Properties[ViewModeResourcePropertiesKey];
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            properties[RememberCollapseStateResourcePropertiesKey] = this.RememberCollapseState.Checked.ToString();
			properties[GroupingResourcePropertiesKey] = this.GroupingDropDown.SelectedValue;
			properties[OrderingResourcePropertiesKey] = this.OrderingDropDown.SelectedValue;
            properties[ViewModeResourcePropertiesKey] = this.ViewMode.SelectedValue;

            // We changed stuff so clear out the expanded tree node information
            if (!this.RememberCollapseState.Checked)
            {
                TreeStateManager manager = new TreeStateManager(Context.Session, Resource.ID);
                manager.Clear();
            }

            return properties;
        }
    }
}