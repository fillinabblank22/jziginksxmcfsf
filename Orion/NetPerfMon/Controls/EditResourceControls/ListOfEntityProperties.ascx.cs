﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class ListOfEntityProperties : System.Web.UI.UserControl
{

    private string entityName = "Orion.Nodes";
    public string EntityName { get; set; }
    public string DisplayEntityName
    {
        get
        {
            string[] splitted = EntityName.Split(new char[] { '.' });
            return splitted[splitted.Length - 1];
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string swql = string.Format(@"SELECT Name FROM Metadata.Property (nolock=true) WHERE EntityName = '{0}' AND IsNavigable != true ORDER BY Name", EntityName);
        DataTable results;
        using (var swis = SolarWinds.Orion.Web.InformationService.InformationServiceProxy.CreateV3())
        {
            results = swis.Query(swql);
        }
        List<string> names = new List<string>();
        foreach (DataRow row in results.Rows)
        {
            names.Add((string)row[0]);
        }

        this.thisEntityColumns.DataSource = names;
        this.thisEntityColumns.DataBind();
    }
}