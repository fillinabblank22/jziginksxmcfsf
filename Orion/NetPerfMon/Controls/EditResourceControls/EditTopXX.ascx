<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditTopXX.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditTopXX" %>
<%@ Register TagPrefix="orion" TagName="TitleEdit" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

    <div>
        <b><%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBDATA_TM0_56, HttpUtility.HtmlEncode(this.NetObjectType))) %></b><br />
        <asp:TextBox runat="server" ID="maxCount" Columns="5"/>
        <br /><br />
        <b><%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBDATA_TM0_57, HttpUtility.HtmlEncode(this.NetObjectType))) %></b><br />
        <asp:TextBox runat="server" ID="filter" Columns="60"/>
        <br /><br />
        <%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBDATA_TM0_61, HttpUtility.HtmlEncode(this.NetObjectType))) %> <br />
		<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YB0_2) %>
        <br /><br />
		<a href="#" class="ExpanderLink"><img src="/NetPerfMon/images/Icon_Plus.gif" style="padding-right: 5px;"/><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_15) %></b></a>
		<div class="text" style="padding-left: 10px; width: 750px;">
            <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_01) %></p>

		            <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_02) %><br />
		            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_03) %></b></p>

		            <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_04) %>
		
			            </p><table>

				            <tbody><tr>
					            <td>0&nbsp;=&nbsp;<b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_05) %></b></td><td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_06) %></td>
				            </tr>
				            <tr>
					            <td>1&nbsp;=&nbsp;<b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_07) %></b></td><td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_08) %></td>
				            </tr><tr>

					            <td>2&nbsp;=&nbsp;<b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_09) %></b></td><td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_10) %></td>
				            </tr><tr>
					            <td>3&nbsp;=&nbsp;<b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_11) %></b></td><td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_12) %></td>
				            </tr>
			            </tbody></table>
		

		            <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_13) %><br />
		            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_14) %></b></p>

		            <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_15) %><br />
		            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_16) %></b></p>
						
		            <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_17) %><br />
		            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_18) %></b></p>

		            <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_19) %><br />
		            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_20) %></b></p>

		            <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_21) %><br /> 
                    <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_22) %></b></p>
		            <p>

            <asp:Panel ID="NodesColumns" runat="server">
            <a href="#" class="ExpanderLink"><img src="/NetPerfMon/images/Icon_Plus.gif" style="padding-right: 5px;" /><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_37) %></b></a>
	        <div class="text" style="padding-left: 10px;">
				<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_58) %><br />
    	        <ul style="list-style-type:none;"> 
				    <asp:Repeater runat="server" ID="nodeColumns">
				        <ItemTemplate>
				            <li><%# DefaultSanitizer.SanitizeHtml(Container.DataItem) %></li>
				        </ItemTemplate>
				    </asp:Repeater>
    			</ul>
			</div>
			<br /><br />
			</asp:Panel>
	        
	        <asp:Panel ID="InterfacesColumns" runat="server">
	        <a href="#" class="ExpanderLink"><img src="/NetPerfMon/images/Icon_Plus.gif" style="padding-right: 5px;" /><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_38) %></b></a>
	        <div class="text" style="padding-left: 10px;">
				<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_59) %><br />
    	        <ul style="list-style-type:none;"> 
				    <asp:Repeater runat="server" ID="interfaceColumns">
				        <ItemTemplate>
				            <li><%# DefaultSanitizer.SanitizeHtml(Container.DataItem) %></li>
				        </ItemTemplate>
				    </asp:Repeater>
    			</ul>
			</div>
			</asp:Panel>
			
			<asp:Panel ID="VolumesColumns" runat="server">
	        <a href="#" class="ExpanderLink"><img src="/NetPerfMon/images/Icon_Plus.gif" style="padding-right: 5px;" /><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_39) %></b></a>
	        <div class="text" style="padding-left: 10px;">
				<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_60) %><br />
    	        <ul style="list-style-type:none;"> 
				    <asp:Repeater runat="server" ID="volumeColumns">
				        <ItemTemplate>
				            <li><%# DefaultSanitizer.SanitizeHtml(Container.DataItem) %></li>
				        </ItemTemplate>
				    </asp:Repeater>
    			</ul>
			</div>
			</asp:Panel>
		</div>
        
        <script type="text/javascript">
            //<![CDATA[
            $(function () {
                $(".ExpanderLink + div").hide();
                $(".ExpanderLink").toggle(
                    function () {
                        $("img:first", this).attr("src", "/NetPerfMon/images/Icon_Minus.gif");
                        $(this).next().slideDown("fast");
                    },
                    function () {
                        $("img:first", this).attr("src", "/NetPerfMon/images/Icon_Plus.gif");
                        $(this).next().slideUp("fast");
                    }
                )
            });
        //]]>
        </script>
    </div>
