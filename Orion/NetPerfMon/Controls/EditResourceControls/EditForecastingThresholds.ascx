<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditForecastingThresholds.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditForecastingThresholds" %>

<style>
.chartSpecificSettings {background-color:transparent;}
.sw-blueHref {background-color:transparent;}
.sw-blueHref a {background-color:transparent; color:#336699;}
.sw-blueHref a:hover {color: #F99D1C;}
</style>


<div class="sw-blueHref">
<div class="sw-suggestion"><span class="sw-suggestion-icon"></span><%= DefaultSanitizer.SanitizeHtml(GetHintText) %></div>
</div> 