<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditCPUsByPercentLoad.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditCPUsByPercentLoad" %>
<%@ Register TagPrefix="orion" TagName="EditAutoHide" Src="~/Orion/Controls/AutoHideControl.ascx" %>
<table border="0">
    <tr>
        <td>
            <asp:Label runat="server" ID="label" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBCODE_IB0_7 %>" Font-Bold="true" />
        </td>
        <td style="padding-left: 5px" colspan="2">
            <asp:TextBox runat="server" ID="TopXX" Width="50" />
            <asp:RangeValidator ID="EventsRangeValidator" runat="server" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBCODE_IB0_8 %>"
                Display="Dynamic" ControlToValidate="TopXX" MinimumValue="1" MaximumValue="999"
                Type="Integer" /> 
        </td>
    </tr>
    <orion:EditAutoHide runat="server" ID="AutoHide" Description=" " />
</table>