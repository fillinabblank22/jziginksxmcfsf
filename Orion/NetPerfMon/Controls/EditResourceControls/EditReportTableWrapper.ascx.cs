﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Reporting.DAL;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Reporting;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Reporting.Impl.Rendering;
using SolarWinds.Reporting.Models.Tables;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditReportTableWrapper : BaseResourceEditControl
{
    public override Dictionary<string, object> Properties
    {
        get
        {
            if (ReportDataProvider is ResourcePropertiesReportData)
            {
                string configXml = SerializationHelper.ToXmlString(reportTableConfigruration.ViewModel, Loader.GetKnownTypes());
                string dataSourceXml = SerializationHelper.ToXmlString(ReportDataProvider.GetDataSource(), Loader.GetKnownTypes());

                var properties = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase)
                                     {
                                         {WebConstants.ConfigurationDataMoniker, configXml},
                                         {WebConstants.DataSourceMoniker, dataSourceXml},
                                         // todo: deal with TF on view
                                         //{WebConstants.TimeFrameMoniker, null},
                                     };
                return properties;
            }
            return new Dictionary<string, object>();
        }
    }

    public void UpdateViewModel(TableConfiguration config)
    {
        reportTableConfigruration.DataSource = ReportDataProvider.GetDataSource();
        reportTableConfigruration.ViewModel = ReportDataProvider.GetConfigurationData<TableConfiguration>();
        ReportDataProvider.UpdateConfigurationData(config);
    }

    public override IEnumerable<Control> ButtonsForButtonBar
    {
        get
        {
            var previewBtn = new LocalizableButton {Text = Resources.CoreWebContent.WEBCODE_PF0_9};
            if (OrionConfiguration.IsDemoServer)
            {
                previewBtn.OnClientClick = "demoAction('Core_Reporting_PreviewResource', this); return false;";
            }
            else
            {
                previewBtn.Click += new EventHandler(previewBtn_Click);
            }
            yield return previewBtn;
        }
    }

    public ReportDataProviderFactory.ReportDataProviderBase ReportDataProvider { get; set; }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (ReportDataProvider == null)
        {
            ReportDataProvider = ReportDataProviderFactory.GetReportDataProvider(this, TableRenderer.Moniker);
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        reportTableConfigruration.DataSource = ReportDataProvider.GetDataSource();
        reportTableConfigruration.ReportDataProvider = ReportDataProvider;

        if (!Page.IsPostBack)
        {
            reportTableConfigruration.ViewModel = ReportDataProvider.GetConfigurationData<TableConfiguration>();
        }
        else
        {
            ReportDataProvider.UpdateConfigurationData(reportTableConfigruration.ViewModel);
        }

        string eventTarget = this.Request["__EVENTTARGET"];
        string eventArgument = this.Request["__EVENTARGUMENT"];
        if (eventTarget == "columnLayout" && eventArgument == "Preview")
        {
            previewBtn_Click(null, e);
        }

        PreviewPanel.Style["visibility"] = "hidden";
    }

    void previewBtn_Click(object sender, EventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            var report = ReportDataProvider.GetReport();
            var cell = ReportDataProvider.GetCell();
            
            PreviewPanel.Controls.Clear();
            PreviewPanel.Controls.Add(new ReportCellPreviewTableModal
            {
                Model = report,
                AllowColumnResizing = true,
                OnCreatedJS = "SW.Core.TableResource.TableConfigurationController.onPreviewColumnResize",
                OnSubmitJS = "SW.Core.TableResource.TableConfigurationController.onPreviewColumnResizeSubmit()",
                AllowToolTips = true,
                CellId = cell.RefId,
                ForcedMaxRows = WebsiteSettings.ReportingTablePreviewMaxRowCount
            });
        }
    }
}
