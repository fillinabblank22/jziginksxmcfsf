<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditForecastingTopXXCapacityProblems.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditForecastingThresholds_EditForecastingTopXXCapacityProblems" %>
<%@ Register TagPrefix="orion" TagName="ListOfEntityProperties" Src="~/Orion/NetPerfMon/Controls/EditResourceControls/ListOfEntityProperties.ascx" %>

<style>
.chartSpecificSettings {background-color:transparent;}
.sw-expander-link img {padding-right: 5px;}
</style>

<div class="sw-editForecastTopXX-blueHref">

<asp:Label runat="server" ID="label" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_281 %>"
                Font-Bold="true" /><br />
  <asp:TextBox runat="server" ID="MaxEventsText" Width="100" />

  <br /><br />
  <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LF0_133) %></b><br />

  <asp:TextBox runat="server" ID="FilterText" size="50" />
  <asp:Repeater runat="server" ID="metrics" OnItemDataBound="Metrics_ItemDataBound">
				        <HeaderTemplate>
                        <br />
                        <table style="width:600px;">
                        <tr>
                            
                        <b><td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LF0_134) %></td></b>
                        </tr>                        
                        </HeaderTemplate>
                        <ItemTemplate>
				            <tr>                            
                            
            <td><asp:CheckBox runat="server" ID="metric" /></td>
                               
                            </tr>
				        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
				    </asp:Repeater>

</div>

<br />

<a href="#" class="sw-expander-link"><img src="/NetPerfMon/images/Icon_Plus.gif" /><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_15) %></b></a>
		<div style="padding-left: 10px; width: 750px;">
            <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_01) %></p>

		            <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_02) %><br />
		            <b>Nodes.Status != 1</b></p>

		            <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_04) %>
		
			            </p><table>

				            <tbody><tr>
					            <td>0&nbsp;=&nbsp;<b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_05) %></b></td><td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_06) %></td>
				            </tr>
				            <tr>
					            <td>1&nbsp;=&nbsp;<b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_07) %></b></td><td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_08) %></td>
				            </tr><tr>

					            <td>2&nbsp;=&nbsp;<b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_09) %></b></td><td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_10) %></td>
				            </tr><tr>
					            <td>3&nbsp;=&nbsp;<b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_11) %></b></td><td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_12) %></td>
				            </tr>
			            </tbody></table>
		

		            <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_13) %><br />
		            <b>Nodes.Vendor = 'Cisco'</b></p>

		            <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_15) %><br />
		            <b>Nodes.City = 'Atlanta'</b></p>
						
		            <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_17) %><br />
		            <b>Nodes.Caption Like 'AX3-*'</b></p>

		            <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_19) %><br />
		            <b>Nodes.Vendor Like 'Nortel*' AND Status=2</b></p>

		            <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_21) %><br /> 
                    <b>Nodes.Vendor Like '*-TX'</b></p>
		            <p>


<orion:ListOfEntityProperties ID="ListOfEntityPropertiesForecasting" runat="server" EntityName="Orion.ForecastCapacity"  />
<orion:ListOfEntityProperties ID="ListOfEntityPropertiesForecastMetrics" runat="server" EntityName="Orion.ForecastMetrics"  />
<orion:ListOfEntityProperties ID="ListOfEntityPropertiesNodes" runat="server" EntityName="Orion.Nodes"  />

            <script type="text/javascript">
            //<![CDATA[
                $(function () {
                    $(".sw-expander-link + div").hide();
                    $(".sw-expander-link").toggle(
                    function () {
                        $("img:first", this).attr("src", "/NetPerfMon/images/Icon_Minus.gif");
                        $(this).next().slideDown("fast");
                    },
                    function () {
                        $("img:first", this).attr("src", "/NetPerfMon/images/Icon_Plus.gif");
                        $(this).next().slideUp("fast");
                    }
                )
                });
        //]]>
        </script>