<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditCustomQuery.ascx.cs" 
    Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditCustomQuery" %>

<script type="text/javascript">
    function updateSearchControls() {
        var enableCheckbox = $('#<%= DefaultSanitizer.SanitizeHtml(EnableSearchCheckbox.ClientID) %>');
        var searchPanel = $('#<%= SearchSWQLBox.ClientID %>');
        var searchSWQLTextBox = $('#<%= SearchSWQL.ClientID %>');
        
        var allowSearch = enableCheckbox.is(':checked');
        if (allowSearch) {
            searchPanel.show();
        } else {
            searchPanel.hide();
            searchSWQLTextBox.val('');
        }
    }
</script>
<p>
    <asp:Panel runat="server" ID="SWQLEditor">
        <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_PSR_3) %></b><br/>
        <asp:TextBox runat="server" ID="SWQL" Rows="10" Columns="80" TextMode="MultiLine" /><br/>
        <br/>
        <label for="<%= DefaultSanitizer.SanitizeHtml(EnableSearchCheckbox.ClientID) %>">
            <input type="checkbox" runat="server" id="EnableSearchCheckbox" onclick="updateSearchControls();"/>
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_JT0_1) %>
        </label>
        <br/>
        
        <asp:Panel runat="server" ID="SearchSWQLBox" CssClass="hidden">
            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_JT0_2) %></b><br/>
            <span class="helpfulText"><%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBCODE_JT0_3, "${SEARCH_STRING}")) %></span><br/>
            <asp:TextBox runat="server" ID="SearchSWQL" Rows="10" Columns="80" TextMode="MultiLine" /><br/>
        </asp:Panel>
        <br/>
    </asp:Panel>

    <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_PSR_4) %></b><br/>
    <asp:TextBox runat="server" ID="RowsPerPage" ></asp:TextBox>
</p>
