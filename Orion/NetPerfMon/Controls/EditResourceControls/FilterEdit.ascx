﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FilterEdit.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_FilterEdit" %>
<%@ Register TagPrefix="orion" TagName="FilterEdit" Src="~/Orion/Controls/FilterNodesSql.ascx" %>

<table>
    <tr>
        <td>
            <orion:FilterEdit runat="server" ID="FilterEditControl" />
        </td>
    </tr>
</table>