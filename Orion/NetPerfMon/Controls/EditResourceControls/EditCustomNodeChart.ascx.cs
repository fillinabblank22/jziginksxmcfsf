﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditCustomNodeChart : BaseResourceEditControl
{
    private Dictionary<string, object> _properties = new Dictionary<string, object>();

    private bool TryParseBool(string value, bool defaultValue = true)
    {
        bool temp;
        return bool.TryParse(value, out temp) ? temp : defaultValue;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        _properties.Add("ChartName", string.Empty);
        _properties.Add("Period", string.Empty);
        _properties.Add("SampleSize", string.Empty);
        _properties.Add("ShowTrend", string.Empty);

        string chartName = Resource.Properties["ChartName"];

        // this control also used by ContainerAvailabilityChart.ascx, which is always of "Percent Availability" type
        if (!String.IsNullOrEmpty(this.NetObjectID) && this.NetObjectID.StartsWith("C:", StringComparison.OrdinalIgnoreCase))
        {
            ListOfCharts.Visible = false;
            ListOfChartsLabel.Visible = false;
        }
        else
        {
            Dictionary<string, string> list = ChartXMLConfigManager.GetChartSelection("N", chartName);

            foreach (KeyValuePair<string, string> entry in list)
            {
                ListOfCharts.Items.Add(new ListItem(entry.Value, entry.Key));
            }

            if (!string.IsNullOrEmpty(chartName))
            {
                ListOfCharts.SelectedValue = chartName;
            }

            if (System.Web.HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
            {
                ListOfCharts.Attributes.Add("onchange", "javascript:SaveData('ChartName', this.value);");
            }
        }

        showTrend.Visible = !String.IsNullOrEmpty(chartName) && ChartInfoFactory.Create(chartName).IsTrendAllowed();
        showTrend.Checked = TryParseBool(Resource.Properties["ShowTrend"]);

        SampleSize.SampleSizeValue = Resource.Properties["SampleSize"];
        Period.PeriodName = Resource.Properties["Period"];
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            if (ListOfCharts.Items.Count == 0)
            {
                return new Dictionary<string, object>();
            }

            _properties["ChartName"] = ListOfCharts.SelectedValue;
            _properties["Period"] = Period.PeriodName;
            _properties["SampleSize"] = SampleSize.SampleSizeValue;
            _properties["ShowTrend"] = showTrend.Checked.ToString();
            _properties["HelpLinkFragment"] = ChartInfoFactory.Create(ListOfCharts.SelectedValue).HelpLinkFragment;

            return _properties;
        }
    }
}
