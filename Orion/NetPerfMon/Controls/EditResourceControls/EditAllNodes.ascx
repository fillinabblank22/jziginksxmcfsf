<%@ control language="C#" autoeventwireup="true" codefile="EditAllNodes.ascx.cs"
    inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditAllNodes" %>

<%@ Register TagPrefix="orion" TagName="editNodeTreeControl" Src="~/Orion/NetPerfMon/Controls/EditResourceControls/EditNodeTree.ascx" %>

<orion:editNodeTreeControl ID="originEdit" runat="server" />

<p>
    <strong><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_AllNodes_NodeTreeLoadingType) %></strong>:
    <br />
    <asp:listbox runat="server" id="lbxLoadType" selectionmode="single" rows="1" />
</p>
<div id="LoadTypeNumberBox" runat="server" style="margin: -7px 10px 3px 5px;">
    <label id="txtLoadNodesNumberText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_AllNodes_LimitNumberOfNodes) %>: </label>
    <asp:textbox type="number" id="txtLoadNodesNumber" runat="server"
        name="txtLoadNodesNumber" value="100" style="width: 60px;" />
    <asp:RangeValidator id="LoadNodesNumberValidator"
           ControlToValidate="txtLoadNodesNumber"
           MinimumValue="10"
           MaximumValue="1000"
           Type="Integer"
           EnableClientScript="true"
           Text="<%$ HtmlEncodedResources:CoreWebContent,Resource_AllNodes_LimitNumberOfNodesValidError %>"
           runat="server"/>
    <asp:CustomValidator ID="txtLoadNodesNumberValidator" runat="server" ValidateEmptyText="true" ControlToValidate="txtLoadNodesNumber" ClientValidationFunction="onValidate" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_DP0_1 %>"></asp:CustomValidator>
</div>

<script>
    function onValidate(source, args) {
        if (($("[id *= 'lbxLoadType']").find(":selected").val() == "ShowAll") || $("#<%=txtLoadNodesNumber.ClientID%>").val() != "") {
            args.IsValid = true;
        } else {
            args.IsValid = false;
        }
    }
    $(function() {
        var lbxLoadTypeChangeHandler = function() {
            if ($("[id *= 'lbxLoadType']").find(":selected").val() == "ShowAll") {
                $("[id *= 'LoadTypeNumberBox']").hide(200);
            } else {
                $("[id *= 'LoadTypeNumberBox']").show(200);
            }
        }
        lbxLoadTypeChangeHandler();
        $("[id *= 'lbxLoadType']").change(lbxLoadTypeChangeHandler).keypress(lbxLoadTypeChangeHandler);
    });
</script>
