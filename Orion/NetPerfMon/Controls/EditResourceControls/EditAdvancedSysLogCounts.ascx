<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditAdvancedSysLogCounts.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditAdvancedSysLogCounts" %>

<p style="margin-top: 0px;">
    <b><%= Resources.SyslogTrapsWebContent.WEBDATA_AK0_231 %></b><br/>
    <asp:DropDownList ID="ddlPeriod" runat="server">
        <asp:ListItem Text="<%$ Resources: SyslogTrapsWebContent,WEBDATA_AK0_233 %>" Value="minute"/>
        <asp:ListItem Text="<%$ Resources: SyslogTrapsWebContent,WEBDATA_AK0_234 %>" Value="hour" Selected="True"/>
        <asp:ListItem Text="<%$ Resources: SyslogTrapsWebContent,WEBDATA_AK0_235 %>" Value="day"/>
        <asp:ListItem Text="<%$ Resources: SyslogTrapsWebContent,WEBDATA_AK0_236 %>" Value="week"/>
        <asp:ListItem Text="<%$ Resources: SyslogTrapsWebContent,WEBDATA_AK0_237 %>" Value="month"/>
    </asp:DropDownList>
</p>