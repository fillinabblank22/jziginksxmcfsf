﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using SolarWinds.NPM.Web.Gauge.V1;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Core.Common.i18n.Registrar;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_LinearGaugeEdit : BaseResourceEditControl
{
    public string SelectedStyle { get; set; }
    private Dictionary<string, object> _properties = new Dictionary<string, object>();

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        _properties.Add("Scale", string.Empty);
        _properties.Add("Style", string.Empty);

        List<string> styles = GaugeHelper.GetAllGaugeStyles(GaugeType.Linear);

        //Set scale input
        string scale = Resource.Properties["Scale"];
        if (string.IsNullOrEmpty(scale))
            scale = "100";
        scaleInput.Text = scale;

        foreach (string styleName in styles)
        {

            stylesList.Items.Add(new ListItem(GetStyleText(styleName), styleName));
        }

        string style = Resource.Properties["Style"];
        if (string.IsNullOrEmpty(style))
            style = "Modern";

        this.stylesList.SelectedValue = style;

        this.SelectedStyle = this.stylesList.SelectedValue;

        int scaleNum;
        if (!Int32.TryParse(this.scaleInput.Text, out scaleNum)) { scaleNum = 100; }

        //Set sample gauges panel
        foreach (string gaugeStyle in styles)
        {
            Image webImage = new Image();
            string gaugeStyleText = GetStyleText(gaugeStyle);
            webImage.ImageUrl = String.Format("/Orion/NetPerfMon/Gauge.aspx?Style={0}&Property={2}&GaugeType=Linear&Units=&Min=0&Max=100&Scale={1}&WarningThreshold=60&ErrorThreshold=80&Value=35", gaugeStyle, scaleNum, HttpUtility.UrlEncode(gaugeStyleText));
            webImage.CssClass = "gauge";
            webImage.Attributes.Add("stylename", gaugeStyle);

            Panel panel = new Panel();

            panel.Width = 500;
            panel.Height = Convert.ToInt32(scaleNum * 1.2);
            panel.Controls.Add(webImage);

            webImage.Attributes.Add("onclick", "SelectMe(this); return false;");
            GaugeStylesPanel.Controls.Add(panel);
        }

        if (System.Web.HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
        {
            scaleInput.Attributes.Add("onchange", "javascript:SaveData('Scale', this.value);");
            //stylesList.Attributes.Add("onchange", "javascript:SaveData('Style', this.value);");
        }
    }

    /// <summary>
    /// Used to retrieve the style name appropriate to locale.
    /// </summary>
    /// <param name="style">style value</param>
    /// <returns>localized style name</returns>
    private string GetStyleText(string style)
    {
        return Resources.CoreWebContent.ResourceManager.GetString(
            ResourceManagerRegistrar.Instance.CleanResxKey("GaugeStyle", style)) ?? style;
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            _properties["Scale"] = scaleInput.Text;
            _properties["Style"] = stylesList.SelectedValue;

            return _properties;
        }
    }
}
