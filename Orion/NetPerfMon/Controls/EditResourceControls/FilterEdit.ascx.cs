﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_FilterEdit : BaseResourceEditControl
{
	protected override void OnInit(EventArgs e)
	{
		if (Resource != null)
			this.FilterEditControl.FilterTextBox.Text = Resource.Properties["Filter"];

		base.OnInit(e);
	}

	public override Dictionary<string, object> Properties
	{
		get 
		{
			Dictionary<string, object> properties = new Dictionary<string, object>();
			properties.Add("Filter", SqlFilterChecker.CleanFilter(this.FilterEditControl.FilterTextBox.Text));

			return properties;
		}
	}
}
