﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

using Resources;
using SolarWinds.Orion.Core.Common.EntityManager;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Enums;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditNodeTree : BaseResourceEditControl
{

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (this.Resource == null)
        {
            int resourceID = 0;
            Int32.TryParse(Request.QueryString["ResourceID"], out resourceID);
            Resource = ResourceManager.GetResourceByID(resourceID);

            NetObjectID = Request.QueryString["NetObject"];
        }

        SQLFilter.FilterTextBox.Text = Resource.Properties["Filter"];

        bool hideRememberExpanded;
        bool.TryParse(Request.QueryString["HideRememberExpanded"] ?? "false", out hideRememberExpanded);

        if (!this.IsPostBack)
        {
            InitializeGroupingControls();

            lbxGroup1.SelectedValue = Resource.Properties["Grouping1"];

            string grouping = Resource.Properties["Grouping2"];

            if (grouping != null && !grouping.Equals(lbxGroup1.SelectedValue, StringComparison.OrdinalIgnoreCase))
                lbxGroup2.SelectedValue = grouping;
            else
                lbxGroup2.SelectedValue = string.Empty;

            grouping = Resource.Properties["Grouping3"];

            if (grouping != null && !grouping.Equals(lbxGroup1.SelectedValue, StringComparison.OrdinalIgnoreCase)
                && !grouping.Equals(lbxGroup2.SelectedValue, StringComparison.OrdinalIgnoreCase))
                lbxGroup3.SelectedValue = grouping;
            else
                lbxGroup3.SelectedValue = string.Empty;

            GroupNulls.SelectedValue = Resource.Properties["GroupNodesWithNullPropertiesAsUnknown"] ?? "true";

            RememberCollapseState.Visible = !hideRememberExpanded;
            string rememberCollapseState = Resource.Properties["RememberCollapseState"] ?? "true";
            RememberCollapseState.Checked = Boolean.Parse(rememberCollapseState);
        }
    }

    private void InitializeGroupingControls()
    {
        SetGroupingNodeProperties();
        SetGroupingEnergyWiseProperties();
        SetGroupingNodeCustomProperties();
    }

    private void SetGroupingNodeProperties()
    {
        ListItem noneListItem = new ListItem(CoreWebContent.WEBCODE_SO0_9, "");
        ListItem statusListItem = new ListItem(CoreWebContent.WEBDATA_VB0_14, "Status");
        ListItem vendorListItem = new ListItem(CoreWebContent.WEBCODE_VB0_124, "Vendor");
        ListItem machineTypeListItem = new ListItem(CoreWebContent.WEBDATA_VB0_16, "MachineType");
        ListItem contactListItem = new ListItem(CoreWebContent.WEBDATA_VB0_129, "Contact");
        ListItem locationListItem = new ListItem(CoreWebContent.WEBDATA_VB0_128, "Location");
        ListItem iosImageListItem = new ListItem(CoreWebContent.WEBDATA_VB0_132, "IOSImage");
        ListItem iosVersionListItem = new ListItem(CoreWebContent.WEBCODE_VB0_125, "IOSVersion");
        ListItem ipAddressTypeListItem = new ListItem(CoreWebContent.WEBDATA_AK0_352, "IP_Address_Type");
        ListItem objectSubTypeListItem = new ListItem(CoreWebContent.WEBDATA_AK0_353, "ObjectSubType");
        ListItem sysObjectIDListItem = new ListItem(CoreWebContent.WEBDATA_AK0_182, "SysObjectID");
        ListItem isServerListItem = new ListItem(CoreWebContent.WEBDATA_SO_206, "IsServer");
        ListItem categoryListItem = new ListItem(CoreWebContent.WEBDATA_OF0_05, "EffectiveCategory");

        AddListItem(lbxGroup1, noneListItem);
        AddListItem(lbxGroup1, statusListItem);
        AddListItem(lbxGroup1, vendorListItem);
        AddListItem(lbxGroup1, machineTypeListItem);
        AddListItem(lbxGroup1, contactListItem);
        AddListItem(lbxGroup1, locationListItem);
        AddListItem(lbxGroup1, iosImageListItem);
        AddListItem(lbxGroup1, iosVersionListItem);
        AddListItem(lbxGroup1, ipAddressTypeListItem);
        AddListItem(lbxGroup1, objectSubTypeListItem);
        AddListItem(lbxGroup1, sysObjectIDListItem);
        AddListItem(lbxGroup1, isServerListItem);
        AddListItem(lbxGroup1, categoryListItem);

        foreach (ListItem listItem in lbxGroup1.Items)
        {
            AddListItem(lbxGroup2, listItem.Text, listItem.Value);
            AddListItem(lbxGroup3, listItem.Text, listItem.Value);
        }
    }

    private void SetGroupingEnergyWiseProperties()
    {
        //FB21478 - Only allow "EnergyWise capability" grouping when NPM is installed
        // This was originally in all the lbx lists: <asp:ListItem Text="EnergyWise Capability" Value="NPM_NV_EW_NODES_V.EnergyWise" />
        bool isEnergyWise = EntityManager.InstanceWithCache.IsThereEntity("Orion.NPM.EW.Entity");
        if (isEnergyWise)
        {
            string itemText = CoreWebContent.WEBCODE_AK0_99;
            string itemValue = "NPM_NV_EW_NODES_V.EnergyWise";

            AddListItem(lbxGroup1, itemText, itemValue);
            AddListItem(lbxGroup2, itemText, itemValue);
            AddListItem(lbxGroup3, itemText, itemValue);
        }
    }

    private void SetGroupingNodeCustomProperties()
    {
        bool allowUnsortableProperties;
        bool.TryParse(Request.QueryString["AllowUnsortableProperties"] ?? "false", out allowUnsortableProperties);

        foreach (string propName in Node.GetGroupingCustomPropertyNames(allowUnsortableProperties))
        {
            AddListItem(lbxGroup1, propName);
            AddListItem(lbxGroup2, propName);
            AddListItem(lbxGroup3, propName);
        }
    }

    private void AddListItem(ListBox listBox, ListItem listItem)
    {
        listBox.Items.Add(listItem);
    }

    private void AddListItem(ListBox listBox, string item)
    {
        listBox.Items.Add(item);
    }

    private void AddListItem(ListBox listBox, string itemText, string itemValue)
    {
        listBox.Items.Add(new ListItem(itemText, itemValue));
    }

    private int ValidGroups
    {
        get
        {
            if (string.IsNullOrEmpty(lbxGroup1.SelectedValue))
                return 0;
            if (string.IsNullOrEmpty(lbxGroup2.SelectedValue))
                return 1;
            if (string.IsNullOrEmpty(lbxGroup3.SelectedValue))
                return 2;

            return 3;
        }
    }

    private string BuildSubTitle()
    {
        switch (this.ValidGroups)
        {
            case 0:
                return CoreWebContent.WEBCODE_AK0_100;
            case 1:
                return string.Format(CoreWebContent.WEBCODE_AK0_101, lbxGroup1.SelectedItem.Text);
            case 2:
                return string.Format(CoreWebContent.WEBCODE_AK0_102, lbxGroup1.SelectedItem.Text, lbxGroup2.SelectedItem.Text);
            case 3:
                return string.Format(CoreWebContent.WEBCODE_AK0_103, lbxGroup1.SelectedItem.Text, lbxGroup2.SelectedItem.Text, lbxGroup3.SelectedItem.Text);
        }

        return string.Empty;
    }

    private void FixupBlankGroupings()
    {
        List<string> groups = new List<string>();
        groups.Add(lbxGroup1.SelectedValue);
        groups.Add(lbxGroup2.SelectedValue);
        groups.Add(lbxGroup3.SelectedValue);

        groups.RemoveAll(string.IsNullOrEmpty);

        for (int i = 0; i < 3; ++i) groups.Add(string.Empty);

        lbxGroup1.SelectedValue = groups[0];
        lbxGroup2.SelectedValue = groups[1];
        lbxGroup3.SelectedValue = groups[2];
    }

    public override string DefaultResourceSubTitle
    {
        get { return BuildSubTitle(); }
    }

    public override bool ShowSubTitleHintMessage
    {
        get
        {
            return true;
        }
    }

    public override string SubTitleHintMessage
    {
        get
        {
            return CoreWebContent.WEBCODE_AK0_104;
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            FixupBlankGroupings();

            properties.Add("Grouping1", lbxGroup1.SelectedValue);
            properties.Add("Grouping2", lbxGroup2.SelectedValue);
            properties.Add("Grouping3", lbxGroup3.SelectedValue);

            properties.Add("Filter", SqlFilterChecker.CleanFilter(SQLFilter.FilterTextBox.Text));

            properties.Add("GroupNodesWithNullPropertiesAsUnknown", GroupNulls.SelectedValue);
            properties.Add("RememberCollapseState", RememberCollapseState.Checked.ToString());

            // We changed stuff so clear out the expanded tree node information
            TreeStateManager manager = new TreeStateManager(Context.Session, Resource.ID);
            manager.Clear();

            return properties;
        }
    }
}
