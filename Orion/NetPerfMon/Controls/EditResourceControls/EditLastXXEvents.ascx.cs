﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditLastXXEvents : BaseResourceEditControl
{
	private const int DefaultNumberOfEvents = 25;

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		// fill in period list
		foreach (var period in Periods.GenerateSelectionList())
			Period.Items.Add(new ListItem(Periods.GetLocalizedPeriod(period), period));

		MaxEventsText.Text = Resource.Properties["MaxEvents"];
		Period.Text = Resource.Properties["Period"];
	}

	public override Dictionary<string, object> Properties
	{
		get
		{
			var properties = new Dictionary<string, object>();
			try
			{
				Int32.Parse(MaxEventsText.Text);
				properties.Add("MaxEvents", MaxEventsText.Text);
			}
			catch
			{
				properties.Add("MaxEvents", DefaultNumberOfEvents.ToString());
			}

			properties.Add("Period", Period.Text);

			return properties;
		}
	}

	public override string DefaultResourceTitle
	{
		get
		{
            string titleFormat = Resources.CoreWebContent.WEBCODE_VB0_134;

            return String.Format(titleFormat,
                String.IsNullOrEmpty(MaxEventsText.Text) ? DefaultNumberOfEvents.ToString() : MaxEventsText.Text);
		}
	}
}