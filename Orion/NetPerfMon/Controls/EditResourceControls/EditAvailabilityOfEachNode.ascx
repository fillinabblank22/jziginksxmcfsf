﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditAvailabilityOfEachNode.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditAvailabilityOfEachNode" %>
<%@ Register TagPrefix="orion" TagName="EditFilter" Src="~/Orion/Controls/FilterNodesSql.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/TimePeriodControl.ascx" %>

<table width="750px">
    <orion:EditPeriod runat="server" ID="timeEditor" />
    <tr><td colspan="3">
        <orion:EditFilter runat="server" ID="filterEditor" />
    </td></tr>
</table>
