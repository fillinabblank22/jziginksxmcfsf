﻿<%@ WebHandler Language="C#" Class="AddRemoveObjectsTreeProvider" %>

using System;
using System.Data;
using System.Text;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Linq;
using Resources;
using SolarWinds.InformationService.Contract2;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.InformationService;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Core.Common.Swis;

using SwisEntityHelper = SolarWinds.Orion.Web.SwisEntityHelper;

using System.Globalization;
using SolarWinds.InformationService.Linq.Plugins.Core.Orion.Web;

public class AddRemoveObjectsTreeProvider : IHttpHandler
{
    private readonly string unknownValue = CoreWebContent.WEBCODE_AK0_121;
    private readonly Log _log = new Log();
    private List<SolarWinds.InformationService.Contract2.ErrorMessage> _errorMessages;
    
    [Serializable]
    private class TreeNode
    {
        public string Uri { get; set; }
        public string FullName { get; set; }
        public string FullNameWithoutHighlight { get; set; }
        public string Entity { get; set; }
        public int Status { get; set; }
        public string StatusIconHint { get; set; }
        public string InstanceSiteID { get; set; }  
        public string EntityID
        {
            get { return UriHelper.GetStringId(Uri) + "_" + InstanceSiteID; }
        }

        protected string Escape(string input)
        {
            if (input == null)
                return string.Empty;

            return input.Replace(@"\", @"\\").Replace("'", @"\'");
        }
        
        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.Append("{");
            str.AppendFormat("id: '{0}',", EntityID);  // Uri.Split('=').Last()
            str.AppendFormat("text: '{0}',", string.IsNullOrEmpty(FullName) ? string.Empty : Escape(FullName));
            str.AppendFormat("entity: '{0}',", Entity);
            str.AppendFormat("status: '{0}',", Status);
            str.AppendFormat("statusIconHint: '{0}',", StatusIconHint);
            str.AppendFormat("fullName: '{0}',", string.IsNullOrEmpty(FullNameWithoutHighlight) ? string.Empty : Escape(FullNameWithoutHighlight));
            if (Entity == null)
            {
                str.Append("allowDrag: false,");
            }
            else
            {
                // no checkbox for "There are XX more items... message
                str.Append("checked: false,");
                str.Append("propagateCheck: true,");
            }

            if (!String.IsNullOrEmpty(Entity) && SwisEntityHelper.StatusColumnExists(Entity))
                str.AppendFormat("icon: '/Orion/StatusIcon.ashx?entity={0}&amp;status={1}&amp;size=small{2}',", Entity, Status, string.IsNullOrWhiteSpace(StatusIconHint) ? string.Empty : string.Format("&amp;hint={0}", StatusIconHint));
            else
                str.Append("icon: Ext.BLANK_IMAGE_URL,");
            
            str.Append("listeners: { 'checkchange': CheckChange },");
            str.Append("expandable: false,");
            str.Append("leaf: true,");
            str.AppendFormat("uri: '{0}',", Uri);
            str.AppendFormat("instanceSiteId: '{0}'", InstanceSiteID);
            str.Append("}");
            return str.ToString();
        }
    }

    private string GenerateFullName(IList<string> names)
    {
        if (names == null)
            throw new ArgumentNullException("names");

        return HttpUtility.HtmlEncode(string.Join(CoreWebContent.WEBCODE_VB1_9, names.ToArray()));
    }

    private string GetWhere(string property, string propertyType, string propertyValue)
    {
        if (String.IsNullOrEmpty(propertyValue))
        {
            var where = new StringBuilder(" ( ");
            if (propertyType.Equals("System.String"))
            {
                where.AppendFormat("e.{0} = '' OR", property);
            }

            where.AppendFormat(" e.{0} IS NULL ) ", property);
            return where.ToString();
        }

        switch (propertyType)
        {
            case "System.String":
            case "System.Boolean":
            case "System.Char":
                return String.Format(" e.{0} = '{1}' ", property, propertyValue.Replace("'", "''"));

            case "System.DateTime":
                DateTime date;
                if (DateTime.TryParse(propertyValue, CultureInfo.CurrentCulture, DateTimeStyles.None, out date) || DateTime.TryParse(propertyValue, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    return String.Format(" e.{0} = DateTime('{1:o}') ", property, date);
                }
                return String.Empty;

            case "System.Double":
            case "System.Decimal":
            case "System.Single":
                double d;
                if (Double.TryParse(propertyValue, NumberStyles.Float, CultureInfo.CurrentCulture, out d) || Double.TryParse(propertyValue, NumberStyles.Float, CultureInfo.InvariantCulture, out d))
                {
                    return String.Format(" e.{0} = '{1}' ", property, d.ToString(CultureInfo.InvariantCulture));
                }
                return String.Empty;

            default:
                return String.Format(" e.{0} = {1} ", property, propertyValue);
        }
    }


    private IEnumerable<TreeNode> GetEntities(string entityType, string displayColumnName, string whereCondition, int? viewLimitationId, int maxCount)
    {
        var results = new List<TreeNode>();
        using (var swis = InformationServiceProxy.CreateV3())
        {
            bool statusColumnExists = SwisEntityHelper.StatusColumnExists(entityType);
            var statusHintIconColumnExists = SwisEntityHelper.StatusIconHintColumnExists(entityType);
            var useAncessorDisplayName = displayColumnName.Equals("AncestorDisplayNames",
                                                                  StringComparison.OrdinalIgnoreCase);
            //Fix for FB359640
            var uri = "Uri";
            bool isContainerMembers = false;
            
            if (entityType.Equals("Orion.ContainerMembers"))
            {
                uri = "MemberUri";
                isContainerMembers = true;
            }   

            var query = new StringBuilder();
            query.AppendFormat(
                @"SELECT Sites.Name AS OrionName, DisplayName, Uri, Status, {7} StatusIconHint {8}, InstanceSiteID
                  FROM 
                  (
                    SELECT DISTINCT TOP {4} e.DisplayName, e.{5} as Uri, {0} AS Status, {1}
                                    {3} AS StatusIconHint
                                    {6}, e.InstanceSiteID
                                    FROM {2} (nolock=true) e
                                    {9} ORDER BY e.DisplayName 
                  ) AS Data
                  LEFT JOIN Orion.Sites ON Sites.SiteID=Data.InstanceSiteID",
                                                             statusColumnExists ? "e.Status" : "-1", useAncessorDisplayName ? "e." + displayColumnName +"," : string.Empty, entityType,
                                                             statusHintIconColumnExists ? "e.StatusIconHint" : "''", maxCount, uri, isContainerMembers ? ", e.MemberEntityType": string.Empty,
                                                             useAncessorDisplayName ? displayColumnName + "," : string.Empty,
                                                             isContainerMembers ? ", MemberEntityType" : string.Empty,
                                                             whereCondition);
            
            query.Append(viewLimitationId != null ? String.Format(" WITH LIMITATION {0}", viewLimitationId) : String.Empty);

            using (var swisErrorsContext = new SolarWinds.Orion.Core.Common.Swis.SwisErrorsContext())
            {
                DataTable table = swis.QueryWithAppendedErrors(query.ToString(), SwisFederationInfo.IsFederationEnabled);

                foreach (DataRow row in table.Rows)
                {
                    var treeNode = new TreeNode()
                    {
                        InstanceSiteID = row["InstanceSiteID"].ToString(),
                        Entity = isContainerMembers ? row["MemberEntityType"].ToString() : entityType,
                        Uri = row["Uri"].ToString(),
                        Status = ConvertStatusToInt(row["Status"]),
                        StatusIconHint = row["StatusIconHint"].ToString()
                    };
                    string fullName = useAncessorDisplayName ? GenerateFullName((string[])row["AncestorDisplayNames"]) : HttpUtility.HtmlEncode(row["DisplayName"].ToString());
                    string fullNameWithoutHighlight = useAncessorDisplayName ? GenerateFullName((string[])row["AncestorDisplayNames"]) : HttpUtility.HtmlEncode(row["DisplayName"].ToString());
                    if (SwisFederationInfo.IsFederationEnabled)
                    {
                        string orionName = (string) row["OrionName"];
                        if (!string.IsNullOrEmpty(fullName))
                            fullName = string.Format(CoreWebContent.NetObjectPicker_EntityNameWithServerName, fullName, orionName);
                        if(!string.IsNullOrEmpty(fullNameWithoutHighlight))
                            fullNameWithoutHighlight = string.Format(CoreWebContent.NetObjectPicker_EntityNameWithServerName, fullNameWithoutHighlight, orionName);
                    }
                    treeNode.FullName = fullName;
                    treeNode.FullNameWithoutHighlight = fullNameWithoutHighlight;

                    if (!string.IsNullOrEmpty(treeNode.Uri) && !string.IsNullOrEmpty(treeNode.FullName))
                    {
                        results.Add(treeNode);
                    }
                }

                _errorMessages = swisErrorsContext.GetErrorMessages();
            } 
        }

        return results;
    }
    private int ConvertStatusToInt(object status)
    {
        int var = -1;
        if (status != DBNull.Value)
        {
            var = Convert.ToInt32(status);
        }
        else
        {
            _log.Warn("Status column for the row is NULL, which cannot be casted to other types");
        }
        return var;
    }
   
    private string GetDisplayNameColumn(string entityType)
    {
        using (var swis = InformationServiceProxy.CreateV3())
        {
            var cntQuery =
                String.Format(
                    @"SELECT Name FROM Metadata.Property
                            WHERE EntityName = '{0}' AND Name = 'AncestorDisplayNames'",
                    entityType);

            try
            {
                var cntTable = swis.Query(cntQuery);

                return cntTable.Rows.Count > 0 ? "AncestorDisplayNames" : "DisplayName";
            }
            catch(Exception ex)
            {
                _log.Error(string.Format("Get entity displayname column failed for entity '{0}', will return 'DispleyName'",entityType), ex);
                return "DisplayName";
            }
        }
    }


    private int GetEntitiesCount(string entityType, string whereCondition, int? viewLimitationId)
    {
        try
        {
            var query = new StringBuilder();
            query.AppendFormat(
                @"SELECT COUNT(e.Uri) AS cnt FROM {0} (nolock=true) e", entityType);

            query.Append(whereCondition);
            query.Append(viewLimitationId != null ? String.Format(" WITH LIMITATION {0}", viewLimitationId) : String.Empty);
            using (var swis = InformationServiceProxy.CreateV3())
            {
                DataTable table = swis.Query(query.ToString());
                return int.Parse(table.Rows[0]["cnt"].ToString());
            }
        }
        catch (Exception ex)
        {
            _log.Error("Error while loading entities count.", ex);
            throw;
        }
    }

    private string BuildWhereCondition(string entityType, string property, string propertyType, string propertyValue, string searchValue, string excludeDefinitions, string filter, string displayColumnName)
    {
        var whereQuery = new List<String>();

        if (!String.IsNullOrEmpty(property) && !String.IsNullOrEmpty(propertyType))
        {
            property = property.Replace(entityType + ".", string.Empty);
            whereQuery.Add(GetWhere(property, propertyType, propertyValue));
        }
        
        if (!String.IsNullOrEmpty(filter))
        {
            whereQuery.Add(String.Format(" {0} ", filter));
        }

        if (!String.IsNullOrEmpty(searchValue))
        {
            if (displayColumnName.Equals("AncestorDisplayNames", StringComparison.OrdinalIgnoreCase))
            {
                whereQuery.Add(String.Format(" ( e.{0} LIKE '%{1}%' OR e.DisplayName LIKE '%{1}%') ", displayColumnName, searchValue.Replace("'", "''")));
            }
            else
            {
                whereQuery.Add(String.Format(" e.{0} LIKE '%{1}%' ", displayColumnName, searchValue.Replace("'", "''")));
            }
        }

        if (!String.IsNullOrEmpty(excludeDefinitions))
        {
            whereQuery.Add(String.Format(" e.{0} NOT IN ({1}) ", NetObjectTypeToSWISEntityMapper.GetIdColumn(entityType), excludeDefinitions));
        }

        if (whereQuery.Count > 0)
        {
            return string.Format(" WHERE {0}", string.Join(" AND ", whereQuery.ToArray()));
        }

        return string.Empty;
    }

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";

        String entityType = context.Request.Params["entityType"];
        String property = context.Request.Params["groupBy"];
        String propertyType = context.Request.Params["groupByType"];
        String propertyValue = context.Request.Params["value"];
        String searchValue = context.Request.Params["searchValue"];
        String excludeDefinitions = FilterConditionHelper.ResolveSelectedEntitesIds(context.Request.Params["excludeDefinitions"]);
        String filter = context.Request.Params["filter"];
        String viewId = context.Request.Params["viewId"];

        if (string.Compare(propertyValue, unknownValue, StringComparison.OrdinalIgnoreCase) == 0)
            propertyValue = String.Empty;

        int? viewLimitationId = GetLimitationIdFromViewIdString(viewId);
        int maxCount = ContainerHelper.GetManageGroupsMaxObjectTreeItems();

        var displayNameColumn = GetDisplayNameColumn(entityType);
        var whereCondition = BuildWhereCondition(entityType, property, propertyType, propertyValue, searchValue,
                                                 excludeDefinitions, filter, displayNameColumn);

        IEnumerable<TreeNode> results = GetEntities(entityType, displayNameColumn, whereCondition, viewLimitationId,
                                                    maxCount + 1); // maxCount+1 due to we need to know if there is more then maxCount items and call GetEntitiesCount then
        StringBuilder response = new StringBuilder("{");

        if (_errorMessages != null && _errorMessages.Any())
        {
            var control = new System.Web.UI.UserControl();
            var swisfErrorControl = control.LoadControl("~/Orion/SwisfErrorControl.ascx") as SolarWinds.Orion.Web.BaseSwisfErrorControl;
            string swisError = SolarWinds.Orion.Web.BaseSwisfErrorControl.GetSwisfErrorControlHtmlString(
                _errorMessages.Select(error => new SolarWinds.Orion.Web.Federation.SwisErrorMessage(error)).ToList(), swisfErrorControl);

            response.AppendFormat("errorControl: '{0}',", HttpUtility.JavaScriptStringEncode(swisError));            
        }

        response.Append("data: [");
        bool first = true;
        
        int count = 0;
        foreach (TreeNode entity in results)
        {
            TreeNode node;
            if (count++ == maxCount)
            {
                int remainingCount = GetEntitiesCount(entityType, whereCondition, viewLimitationId) - count + 1;
                node = new TreeNode
                           {
                    Uri = "0",
                    FullName = (remainingCount > 1) ? String.Format(CoreWebContent.WEBCODE_VB1_20, remainingCount) : String.Format(CoreWebContent.WEBCODE_VB1_21, remainingCount)
                };
            }
            else
            {
                node = entity;
                if (!String.IsNullOrEmpty(searchValue))
                {
                    // highlight search text
                    node.FullName = Regex.Replace(
                            entity.FullName,
                            String.Format("({0})", Regex.Escape(HttpUtility.HtmlEncode(searchValue))),
                            "<span class=\"searchHighlight\">$1</span>",
                            RegexOptions.IgnoreCase | RegexOptions.Compiled);
                }
            }
            if (!first)
                response.Append(",");
            response.Append(node);

            first = false;
            
            if (count > maxCount) break;
        }

        response.Append("]}");
        context.Response.Write(response.ToString());
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    #region Helpers creepers

    private int? GetLimitationIdFromViewIdString(string viewId)
    {
        int? viewLimitationId = null;
        if (!string.IsNullOrEmpty(viewId))
        {
            int viewIdInt;
            if (Int32.TryParse(viewId, out viewIdInt))
            {
                try
                {
                    viewLimitationId = ViewManager.GetViewById(viewIdInt).LimitationID;
                }
                catch
                {
                    return null;
                }
            }
        }

        return viewLimitationId;
    }

    #endregion
}
