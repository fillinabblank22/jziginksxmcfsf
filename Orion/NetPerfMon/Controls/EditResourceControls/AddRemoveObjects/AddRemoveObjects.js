Ext.namespace('SW');
Ext.namespace('SW.Orion');

SW.Orion.SelectObjects = function () {
    ORION.prefix = "Orion_SelectObjects_";

    var batchSize = 100;
    var batchDelay = 10;

    var selectorModel;
    var dataStore;
    var grid;
    var gridPanel;
    var initialized;
    var tree;
    var treePanel;
    var itemsToDelete;
    var itemsToAdd;
    var mask;
    var someAlreadyExist;

    var rightPanelTitle = '';
    var leftPanelTitle = '';
    var entityName = '';
    var sqlFilter = '';

    var gridItemsFieldClientID = '';
    var currentItemFieldClientID = '';
    var currentContainerID;
    var groupName = '';
    var initialData = new Array();


    var nodesToRemove; // remove these nodes at the end of the last batch

    function getQueryVariable(variable) {
        var query = window.location.search.substring(1);
        if (query != null) {
            var vars = query.split("&");
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                if (pair[0] == variable) {
                    return pair[1];
                }
            }
        }
        return "";
    }
    function renderName(value, meta, record) {
        // entity - Orion.Nodes, Orion.NPM.Interfaces
        // id - entityId (for computed status)
        // status - entity status
        // size - size of icon
        return String.format('<span class="entityIconBox"><img src="/Orion/StatusIcon.ashx?entity={0}&amp;status={1}&amp;size=small" /></span> {2} ', GetItemType(), record.data.MemberStatus, value);
    }
    
    function createDataTreeLoader(config) {
        var defaultConfig = {
            handleResponse: function (response) {
                this.transId = false;
                var a = response.argument;
                var responseObject = Ext.decode(response.responseText);

                if (responseObject.errorControl && responseObject.errorControl != '') {
                    showSwisError(responseObject.errorControl);
                }

                response.responseData = responseObject.data;

                this.processResponse(response, a.node, a.callback, a.scope);
                this.fireEvent("load", this, a.node, response);
            }
        };

        var treeLoader = new Ext.tree.TreeLoader($.extend({}, config, defaultConfig));

        return treeLoader;
    }

    var showSwisError = function (errorControlHtml) {
        $('.swis-error-wrapper').show();
        if ($('.swis-error-wrapper').length > 0) {
            $('.swis-error-wrapper').html(errorControlHtml);
        }
    };

    var hideSwisError = function () {
        $('.swis-error-wrapper').hide();
    };

    GetGridState = function () {
        // get all definitions that are used in grid
        var gridState = [];
        grid.store.each(function (record) { gridState.push(record.data.Id); });

        return gridState;
    }

    ShowAddMask = function () {
        mask = new Ext.LoadMask(grid.el, {
            msg: "@{R=Core.Strings;K=WEBJS_TM0_32; E=js}"
        });
        mask.show();

        grid.store.suspendEvents(false);
    }

    ShowRemoveMask = function () {
        mask = new Ext.LoadMask(grid.el, {
            msg: "@{R=Core.Strings;K=WEBJS_TM0_33; E=js}"
        });
        mask.show();

        grid.store.suspendEvents(false);
    }

    HideMask = function () {
        grid.store.sort('Name', 'ASC');
        grid.store.resumeEvents();
        grid.store.fireEvent('datachanged');
        mask.hide();

        RefreshTreeIfEmpty();
    }

    RefreshTreeIfEmpty = function () {
        // if last item in tre is "There are XX more items" (entity attribute is empty), relaod tree
        /*
        if ((tree.root.childNodes.length == 1) && (!tree.root.childNodes[0].attributes.entity)) {
        LoadGroups();
        }
        */
    }

    UpdateToolbarButtons = function () {
    };

    LoadEntities = function (itemType, entityIds) {
        if (entityIds) {
            ORION.callWebService("/Orion/Services/AddRemoveObjects.asmx", "LoadEntitiesWithLimitation", { entityType: itemType, entityIds: entityIds, viewId: getQueryVariable("ViewID") }, function (result) {
                var blankRecord = Ext.data.Record.create(grid.store.fields);
                for (var i = 0; i < result.length; i++) {
                    var item = result[i];

                    var record = new blankRecord({
                        Id: item.ID,
                        Name: item.FullName,
                        FullName: item.FullName,
                        MemberStatus: item.Status
                    });

                    if (grid.store.findExact("Id", record.data.Id) == -1) {
                        grid.store.add(record);
                    }
                }
            });
        }
    };

    GetGroupByProperties = function (itemType, onSuccess) {
        ORION.callWebService("/Orion/Services/AddRemoveObjects.asmx", "GetGroupByProperties", { entityType: itemType }, onSuccess);

    };


    LoadGroupByProperties = function (defaultGroupBy) {
        CancelSearch(false);
        var itemType = GetItemType();

        if (itemType) {
            GetGroupByProperties(itemType, function (result) {
                var groupBySelect = $("#groupBySelect");
                groupBySelect.empty();

                for (var i = 0; i < result.length; i++) {
                    var item = result[i];
                    var selected = (item.Column == defaultGroupBy) ? 'selected="selected"' : '';

                    groupBySelect.append('<option value="' + item.Column + '" type="' + item.Type + '" ' + selected + ' >' + item.DisplayName + '</option>');
                }

                groupBySelect.change(function () { CancelSearch(false); LoadGroups(); });
                groupBySelect.change();
            });
        }
    };


    GetGroups = function (itemType, groupBy, searchValue, onSuccess) {
        ORION.callWebService("/Orion/Services/AddRemoveObjects.asmx", "GetEntityGroupsWithLimitation", { entityType: itemType, groupByProperty: groupBy, searchValue: searchValue, excludeDefinitions: GetGridState(), filter: sqlFilter, viewId: getQueryVariable("ViewID") }, onSuccess);
    };

    // Changes icon on gived TreeNode in runtime
    SetNodeIcon = function (node, icon) {
        if ((node.ui) && (node.ui.iconNode)) {
            node.ui.iconNode.src = icon;
        }
    }

    CancelSearch = function (reloadTree) {
        $('#searchBox').val('');

        if (reloadTree)
            LoadGroups();
    }

    DoSearch = function () {
        LoadGroups();
    }

    GetLabelForNode = function (name, count) {
        return String.format("{0} ({1})", Ext.util.Format.htmlEncode(name), count);
    }

    UpdateGroupItemsCount = function (node, newCount) {
        node.attributes.groupCount = newCount;
        node.setText(GetLabelForNode(node.attributes.groupName, node.attributes.groupCount));
    }

    LoadGroups = function () {
        hideSwisError();
        var itemType = GetItemType();
        var groupBy = GetGroupBy();
        var groupByType = GetGroupByType();
        var searchValue = GetSearchValue();
        var loadText = "@{R=Core.Strings;K=WEBJS_VB0_1; E=js}";

        if (searchValue) {
            $('#searchButton img').attr('src', '/Orion/images/Button.SearchCancel.gif');
            $('#searchButton').unbind('click');
            $('#searchButton').click(function () { CancelSearch(true) });
            loadText = "@{R=Core.Strings;K=WEBJS_TM0_34; E=js}";
        } else {
            $('#searchButton img').attr('src', '/Orion/images/Button.SearchIcon.gif');
            $('#searchButton').unbind('click');
            $('#searchButton').click(DoSearch);
        }

        ClearTree();
        tree.root.appendChild({ text: loadText, icon: '/Orion/images/AJAX-Loader.gif', expandable: false, leaf: true });

        if ((itemType) && (groupBy)) {
            GetGroups(itemType, groupBy, searchValue, function (result) {
                ClearTree();
                for (var i = 0; i < result.length; i++) {
                    var item = result[i];

                    // if if groupBy is 'Status' or ends with '.Status' then value should be item.Status
                    var displayedName = item.Name == '[Unknown]' ? '@{R=Core.Strings;K=WEBJS_VB0_70; E=js}' : item.Name;
                    var itemValue = (groupBy == 'Status' || (groupBy.lastIndexOf('.Status') > -1 && groupBy.lastIndexOf('.Status') + '.Status'.length == groupBy.length)) ? item.Status : item.Value;
                    
                    if (item.Count > 0) {
                        var node = new Ext.tree.AsyncTreeNode({
                            id: 'tree-node-' + (i),
                            text: GetLabelForNode(displayedName, item.Count),
                            value: itemValue,
                            groupCount: item.Count,
                            groupName: item.Name,
                            icon: String.format('/Orion/StatusIcon.ashx?entity={0}&status={1}&size=small', itemType, item.Status),
                            allowDrag: true,
                            isGroup: true,
                            leaf: false,
                            checked: false,
                            propagateCheck: true,
                            listeners: {
                                'checkchange': function (node, checked) {
                                    if (node.attributes.propagateCheck) {
                                        node.eachChild(function (n) {
                                            n.attributes.propagateCheck = false;
                                            n.checked = checked;
                                            n.getUI().toggleCheck(checked);
                                            n.attributes.propagateCheck = true;
                                        });
                                    }

                                    if (checked) {
                                        node.getUI().addClass('x-tree-selected');
                                    } else {
                                        node.getUI().removeClass('x-tree-selected');
                                    }
                                }
                            },
                            loader: createDataTreeLoader({
                                dataUrl: '/Orion/NetPerfMon/Controls/EditResourceControls/AddRemoveObjects/AddRemoveObjectsTreeProvider.ashx',
                                listeners: {
                                    // init load event handler
                                    beforeload: {
                                        fn: function (treeLoader, node) {
                                            this.baseParams.entityType = itemType;
                                            this.baseParams.groupBy = groupBy;
                                            this.baseParams.groupByType = groupByType;
                                            this.baseParams.searchValue = searchValue;
                                            this.baseParams.value = node.attributes.value;
                                            this.baseParams.excludeDefinitions = GetGridState();
                                            this.baseParams.filter = sqlFilter;
                                            this.baseParams.viewId = getQueryVariable("ViewID");

                                            SetNodeIcon(node, '/Orion/images/AJAX-Loader.gif', false);
                                        }
                                    },
                                    // after load event handler
                                    load: {
                                        fn: function (treeLoader, node, response) {
                                            node.expanded = true;
                                            SetNodeIcon(node, node.attributes.icon);
                                        }
                                    }
                                }
                            })
                        });

                        node.attributes.loader.baseParams.icon = node.attributes.icon;
                        tree.root.appendChild(node);
                    }
                    else {
                        tree.root.appendChild(new Ext.tree.TreeNode({
                            text: item.Name,
                            allowDrag: false,
                            icon: '/Orion/StatusIcon.ashx?entity=&status=&size=small'
                        }));
                    }
                }
            });
        } else if (itemType) {
            // no grouping - load entites directly
            LoadEntitiesNonGrouped(itemType, searchValue);
        }
    };

    LoadEntitiesNonGrouped = function (itemType, searchValue) {
        hideSwisError();
        var loader = createDataTreeLoader({
            dataUrl: '/Orion/NetPerfMon/Controls/EditResourceControls/AddRemoveObjects/AddRemoveObjectsTreeProvider.ashx',
            listeners: {
                // init load event handler
                beforeload: {
                    fn: function (treeLoader, node) {
                        this.baseParams.entityType = itemType;
                        this.baseParams.searchValue = searchValue;
                        this.baseParams.excludeDefinitions = GetGridState();
                        this.baseParams.filter = sqlFilter;
                        this.baseParams.viewId = getQueryVariable("ViewID");
                    }
                },
                // after load event handler
                load: {
                    fn: function (treeLoader, node, response) {
                        ClearTree();
                        var result = response.responseData;
                        for (var i = 0; i < result.length; i++) {
                            var child = new Ext.tree.TreeNode(result[i]);
                            tree.root.appendChild(child);
                        }
                    }
                }
            }
        });
        loader.load(new Ext.tree.TreeNode(), null, null);
    };

    ClearTree = function () {
        while (tree.root.firstChild) {
            tree.root.removeChild(tree.root.firstChild);
        }
    }

    RefreshItemsData = function () {
        var data = '';
        var first = true;
        grid.store.each(function (record) {
            if (!first) {
                data += ",";
            }
            //data += Ext.util.JSON.encode(record.data.Id);
            data += record.data.Id;
            first = false;
        });
        //data += ']';
        $('#' + gridItemsFieldClientID).val(data);
    }

    AddMember = function (store, records, index) {
        RefreshItemsData();
    }

    RemoveMember = function (store, records, index) {
        RefreshItemsData();
    }

    RemoveBatch = function () {
        if (itemsToDelete.length > 0) {
            for (var i = itemsToDelete.length - 1, j = 0; i >= 0, j < batchSize; i--, j++) {
                if (i < 0)
                    break;
                grid.store.remove(itemsToDelete[i]);
                itemsToDelete.remove(itemsToDelete[i]);
            }
        }

        if (itemsToDelete.length > 0) {
            setTimeout(RemoveBatch, batchDelay);
        }
        else {
            LoadGroups();
            HideMask();
        }
    }

    RemoveSelectedItems = function (items) {
        ShowRemoveMask();
        itemsToDelete = items;
        RemoveBatch();
    }

    RemoveCheckedNodesFromGrid = function () {
        if (selectorModel.getCount() > 0) {
            Ext.Msg.minWidth = 310;
            Ext.Msg.confirm(
                String.format("@{R=Core.Strings;K=WEBJS_VB1_1; E=js}", rightPanelTitle),
                 "@{R=Core.Strings;K=WEBJS_VB1_2; E=js}",
				function (btn, text) {
				    if (btn == "yes") {
				        RemoveSelectedItems(selectorModel.getSelections());
				    }
				}
			);
        }
    }

    ReloadTree = function () {
        var itemType = GetItemType();
        var groupBy = GetGroupBy();

        if (groupBy) {
            $.each(tree.root.childNodes, function (index, node) {
                if (node.loaded) {
                    node.attributes.loader.baseParams.expandAfterLoad = node.expanded;
                    node.reload(function () { }, node);
                }
            });
        } else {
            LoadEntitiesNonGrouped(itemType);
        }
    }

    GetItemType = function () {
        return entityName;
    }

    GetGroupBy = function () {
        var item = $("#groupBySelect option:selected");
        $("#groupByType").val(item.attr("type")); // save the type of group by selection
        return item.val();
    }

    GetGroupByType = function () {
        return $("#groupByType").val();
    }

    GetSearchValue = function () {
        return $("#searchBox").val();
    }

    InitDragDrop = function () {
        nodesToRemove = [];

        var gridDropTargetEl = grid.getView().el.dom.childNodes[0].childNodes[1];
        var gridDropTarget = new Ext.dd.DropTarget(gridDropTargetEl, {
            ddGroup: 'treeDDGroup',
            copy: false,
            notifyDrop: function (ddSource, e, data) {
                if (data.node.attributes.isGroup) {
                    AddChildNodesToGrid(data.node);
                } else {
                    var parentNode = data.node.parentNode

                    AddNodeToGrid(data.node);
                    data.node.remove(true);

                    if (parentNode != null && parentNode.childNodes.length == 0) {
                        parentNode.remove(true);
                    } else if (parentNode != null) {
                        UpdateGroupItemsCount(parentNode, parentNode.attributes.groupCount - 1);
                    }

                    RefreshTreeIfEmpty();
                }

                return true;
            }
        });
    }

    ExpandAndAdd = function (node, callback) {
        if (typeof itemsToAdd === 'undefined') {
            itemsToAdd = [];
        }
        var loader = createDataTreeLoader({
            dataUrl: '/Orion/NetPerfMon/Controls/EditResourceControls/AddRemoveObjects/AddRemoveObjectsTreeProvider.ashx',
            listeners: {
                // init load event handler
                beforeload: {
                    fn: function (treeLoader, n) {
                        this.baseParams.entityType = GetItemType();
                        this.baseParams.groupBy = GetGroupBy();
                        this.baseParams.groupByType = GetGroupByType();
                        this.baseParams.searchValue = GetSearchValue();
                        this.baseParams.value = n.attributes.value;
                        this.baseParams.excludeDefinitions = GetGridState();
                        this.baseParams.filter = sqlFilter;
                        this.baseParams.viewId = getQueryVariable("ViewID");
                    }
                },
                // after load event handler
                load: {
                    fn: function (treeLoader, n, response) {
                        var result = response.responseData;
                        for (var i = 0; i < result.length; i++) {
                            var child = new Ext.tree.TreeNode(result[i]);
                            itemsToAdd.push(child);
                        }
                        if (itemsToAdd.length > 0) {
                            if (callback)
                                AddBatch(callback);
                            else
                                AddBatch();
                            node.remove(this);
                        } else {
                            node.remove(true);

                            if (callback)
                                callback();
                        }
                    }
                }
            }
        });
        loader.load(node, null, null);
    }

    AddBatch = function (callback) {
        ShowAddMask();
        if (itemsToAdd.length > 0) {
            var blankRecord = Ext.data.Record.create(grid.store.fields);

            if (itemsToAdd.length > 0) {
                for (var i = itemsToAdd.length - 1, j = 0; i >= 0, j < batchSize; i--, j++) {
                    if (i < 0)
                        break;
                    var child = itemsToAdd[i];

                    if (!child.attributes.entity) {
                        itemsToAdd.remove(child);
                        continue;
                    }

                    var record = new blankRecord({
                        Id: child.attributes.id,
                        Name: child.attributes.text,
                        FullName: child.attributes.fullName,
                        MemberStatus: child.attributes.status
                    });

                    var parentNode = child.parentNode;
                    if (grid.store.findExact("Id", record.data.Id) != -1) {
                        someAlreadyExist = true;
                    } else {
                        grid.store.add(record);
                        child.remove();

                        if (parentNode != null) {
                            UpdateGroupItemsCount(parentNode, parentNode.attributes.groupCount - 1);
                        }
                    }

                    itemsToAdd.remove(child);

                    if ((parentNode) && (!parentNode.hasChildNodes())) {
                        nodesToRemove.push(parentNode);
                    }
                }
            }
        }

        if (itemsToAdd.length > 0) {
            setTimeout(function () { AddBatch(callback) }, batchDelay);
        }
        else {
            if (callback) {
                callback();
            }

            if (nodesToRemove != null) {
                $.each(nodesToRemove, function (index, node) { node.remove(true); });
                nodesToRemove = [];
            }
        }
    }

    CheckChange = function (node, checked) {
        if (checked) {
            node.getUI().addClass('x-tree-selected');
        } else {
            node.getUI().removeClass('x-tree-selected');
            if ((node.parentNode) && (node.attributes.propagateCheck)) {
                node.parentNode.attributes.propagateCheck = false;
                node.parentNode.getUI().removeClass('x-tree-selected');
                node.parentNode.getUI().toggleCheck(false);
                node.parentNode.attributes.propagateCheck = true;
            }
        } 
    }

    AddCheckedNodesToGrid = function () {
        someAlreadyExist = false;
        itemsToAdd = [];
        nodesToRemove = [];
        var anythingSelected = false;

        ShowAddMask();

        var responses = 0;
        var toExpand = [];
        for (var i = 0; i < tree.root.childNodes.length; i++) {
            var n = tree.root.childNodes[i];
            if (n.attributes.isGroup) {
                if (n.attributes.checked) {
                    anythingSelected = true;
                    toExpand.push(n);
                } else {
                    n.eachChild(function (child) {
                        if (child.attributes.checked) {
                            anythingSelected = true;
                            itemsToAdd.push(child);
                        }
                    });
                }
            } else if (n.attributes.checked) {
                anythingSelected = true;
                itemsToAdd.push(n);
            }
        }

        for (var i = 0; i < toExpand.length; i++) {
            ExpandAndAdd(toExpand[i], function () {
                responses++;
                // we need to hide load mask when get last resporse from ExpandAndAdd
                if (toExpand.length == responses)
                    HideMask();
            });
        }

        if (!anythingSelected)
            HideMask();

        if (itemsToAdd.length > 0)
            AddBatch(HideMask);
    }

    AddChildNodesToGrid = function (node) {
        someAlreadyExist = false;

        ShowAddMask();

        grid.store.suspendEvents(false);

        ExpandAndAdd(node, HideMask);
    }

    AddNodeToGrid = function (node) {
        var blankRecord = Ext.data.Record.create(grid.store.fields);

        var record = new blankRecord({
            Id: node.attributes.id,
            Name: node.attributes.text,
            FullName: node.attributes.fullName,
            MemberStatus: node.attributes.status
        });
        if (grid.store.findExact("Id", record.data.Id) != -1) {
            Ext.Msg.alert("@{R=Core.Strings;K=WEBJS_TM0_40; E=js}", "@{R=Core.Strings;K=WEBJS_TM0_39; E=js}");
            return;
        }
        grid.store.addSorted(record);
    }

    ToggleAllCheckboxes = function (value) {
        tree.root.eachChild(function (n) {
            n.attributes.propagateCheck = false;
            n.checked = value;
            n.getUI().toggleCheck(value);
            n.attributes.propagateCheck = true;

            if (n.attributes.isGroup) {
                n.eachChild(function (child) {
                    child.attributes.propagateCheck = false;
                    child.checked = value;
                    child.getUI().toggleCheck(value);
                    child.attributes.propagateCheck = true;
                });
            }
        });
    }

    SelectAllInTree = function () {
        ToggleAllCheckboxes(true);
    }

    SelectNoneInTree = function () {
        ToggleAllCheckboxes(false);
    }

    InitTree = function () {
        selectPanel = new Ext.Container({
            applyTo: 'GroupItemsSelector',
            region: 'north',
            height: 100,
            layout: 'fit'
        });

        var toolbar = new Ext.Toolbar({
            height: 27,
            region: 'north',
            items: [{
                id: 'SelectAllButton',
                text: '@{R=Core.Strings;K=WEBJS_VB0_36; E=js}',
                iconCls: 'selectAllButton',
                handler: function () {
                    //if (IsDemoMode()) return DemoModeMessage();
                    SelectAllInTree();
                }
            }, '-', {
                id: 'SelectNoneButton',
                text: '@{R=Core.Strings;K=WEBJS_VB0_38; E=js}',
                iconCls: 'selectNoneButton',
                handler: function () {
                    //if (IsDemoMode()) return DemoModeMessage();
                    SelectNoneInTree();
                }
            }]
        });

        tree = new Ext.tree.TreePanel({
            id: 'TreeExtPanel',
            useArrows: true,
            autoScroll: true,
            animate: true,
            enableDrag: true,
            height: 337, // border layout somehow doesn't work so we have to set height manually
            region: 'center',
            root: {
                text: '',
                id: 'root'
            },
            rootVisible: false,
            ddGroup: "treeDDGroup"
        });

        treePanel = new Ext.Panel({
            title: (leftPanelTitle) ? Ext.util.Format.htmlEncode(leftPanelTitle) : '@{R=Core.Strings;K=WEBJS_TM0_31; E=js}',
            frame: true,
            region: 'west',
            width: 272,
            split: true,
            items: [
                selectPanel,
                toolbar,
                tree
            ]
        });
    }

    InitGrid = function () {
        // when changing this record, change also DataGridRecord in AddRemoveObjectsControl.ascx.cs
        record = Ext.data.Record.create([
            { name: 'Id' },
            { name: 'Name', sortType: Ext.data.SortTypes.asUCString },
            { name: 'FullName' },
            { name: 'MemberStatus' }
        ]);
        arrayReader = new Ext.data.ArrayReader({
            idIndex: 0
        },
        record);

        dataStore = new Ext.data.Store({
            reader: arrayReader,
            sortInfo: { field: 'Name', direction: 'ASC' }
        });

        dataStore.loadData(initialData);

        dataStore.on("add", AddMember);
        dataStore.on("remove", RemoveMember);
        dataStore.on('datachanged', RefreshItemsData);

        selectorModel = new Ext.grid.CheckboxSelectionModel();

        grid = new Ext.grid.GridPanel({
            store: dataStore,

            columns: [
				selectorModel,
				{ id: 'FullName', header: '@{R=Core.Strings;K=WEBJS_TM0_42; E=js}', width: 300, sortable: true, hideable: false, dataIndex: 'FullName', renderer: renderName }
            ],
            autoExpandColumn: 'FullName',

            sm: selectorModel,

            viewConfig: {
                forceFit: false
            },

            border: true,
            frame: true,
            hideHeaders: true,
            stripeRows: true,
            ddGroup: 'treeDDGroup',
            region: 'center',
            title: (rightPanelTitle) ? Ext.util.Format.htmlEncode(rightPanelTitle) : '@{R=Core.Strings;K=WEBJS_VB1_3; E=js}',
            loadMask: { msg: '@{R=Core.Strings;K=WEBJS_TM0_35; E=js}' },

            tbar: [{
                text: '@{R=Core.Strings;K=WEBJS_VB0_36; E=js}',
                iconCls: 'selectAllButton',
                handler: function () {
                    selectorModel.selectAll();
                }
            }, '-', {
                text: '@{R=Core.Strings;K=WEBJS_VB0_38; E=js}',
                iconCls: 'selectNoneButton',
                handler: function () {
                    selectorModel.clearSelections();
                }
            }]
        });

        var addButtonPanel = new Ext.Panel({
            border: false,
            region: 'west',
            width: 54,
            contentEl: 'AddButtonPanel'
        });

        gridPanel = new Ext.Panel({
            id: 'AddButtonPanelExtPanel',
            region: 'center',
            layout: 'border',
            border: false,
            items: [addButtonPanel, grid]
        });

        grid.getSelectionModel().on("selectionchange", UpdateToolbarButtons);
    }

    InitLayout = function () {
        var panel = new Ext.Container({
            id: 'MainExtPanel',
            renderTo: 'ContainerMembersTable',
            height: 500,
            layout: 'border',
            items: [treePanel, gridPanel]
        });
        $(window).bind('resize', function () {
            panel.doLayout();
        });
    }

    return {
        SetTitles: function (leftPanel, rightPanel) {
            leftPanelTitle = leftPanel;
            rightPanelTitle = rightPanel;
        },
        SetEntity: function (name) {
            entityName = name;
        },
        SetFilter: function (filter) {
            sqlFilter = filter;
        },
        SetGridItemsFieldClientID: function (id) {
            gridItemsFieldClientID = id;
        },
        init: function () {
            if (initialized)
                return;

            initialized = true;

            InitTree();
            InitGrid();
            InitLayout();
            LoadEntities(entityName, $('#' + gridItemsFieldClientID).val());
            InitDragDrop();

            UpdateToolbarButtons();

            var defaultGroupBy;
            switch (entityName) {
                case "Orion.Nodes":
                    defaultGroupBy = "Orion.Nodes.Vendor";
                    break;
                case "Orion.Volumes":
                    defaultGroupBy = "Orion.Volumes.Node.DisplayName";
                    break;
                case "Orion.NPM.Interfaces":
                    defaultGroupBy = "Orion.NPM.Interfaces.Node.DisplayName";
                    break;
                default:
                    defaultGroupBy = "";
                    break;
            }

            LoadGroupByProperties(defaultGroupBy);

            //$("#itemTypeSelect").val(ORION.Prefs.load("ItemType", "Nodes")).change(LoadGroupByProperties);
            //$("#itemTypeSelect").change();
            $('#AddButtonPanel').height($('#ContainerMembersTable').height());
            $('#AddToGroupButton').click(AddCheckedNodesToGrid);
            $('#RemoveFromGroupButton').click(RemoveCheckedNodesFromGrid);
            $('#searchButton').click(DoSearch);
            $('#searchBox').keydown(function (event) {
                if (event.keyCode == '13') {
                    event.preventDefault();
                    DoSearch();
                }
            });
        }
    };
} ();

Ext.onReady(SW.Orion.SelectObjects.init, SW.Orion.SelectObjects);
