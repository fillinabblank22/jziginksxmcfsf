<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddRemoveObjectsControl.ascx.cs" 
Inherits="Orion_NPM_Controls_AddRemoveObjectsControl" %>

<orion:Include runat="server" File="NetPerfMon/Controls/EditResourceControls/AddRemoveObjects/AddRemoveObjects.js" />

<div class="swis-error-wrapper" style="width:99%;"></div>

<div id="GroupItemsSelector">
	<div class="GroupSection">
		<label for="groupBySelect"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_5) %></label>
		<select id="groupBySelect">
  		</select>
	</div>
    <div class="GroupSection">
		<label for="searchBox"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_225) %></label>
        <table>
            <tr>
            <td><input type="text" class="searchBox" id="searchBox" name="searchBox" /></td>
            <td><a href="javascript:void(0);" id="searchButton"><img src="/Orion/images/Button.SearchIcon.gif" /></a></td>
            </tr>
        </table>
	</div>
</div>

<div id="ContainerMembersTable" style="width:99%;">
</div>

<div id="AddButtonPanel" style="padding:10px;">
    <a id="AddToGroupButton" href="javascript:void(0);" style="top: 44%; position: relative;">
        <img src="/Orion/images/AddRemoveObjects/arrows_add_32x32.gif" />
    </a>
    <a id="RemoveFromGroupButton" href="javascript:void(0);" style="top: 46%; position: relative;">
        <img src="/Orion/images/AddRemoveObjects/arrows_remove_32x32.gif" />
    </a>
</div>

<input type="hidden" id="groupByType" />
<asp:HiddenField ID="gridItems" runat="server" />

<script type="text/javascript">
    // <![CDATA[
    SW.Orion.SelectObjects.SetTitles('<%= LeftPanelTitle %>', '<%= RightPanelTitle %>');
    SW.Orion.SelectObjects.SetEntity('<%= EntityName %>');
    SW.Orion.SelectObjects.SetFilter('<%= Filter %>');
    SW.Orion.SelectObjects.SetGridItemsFieldClientID('<%= gridItems.ClientID %>');
    // ]]>
</script>
