<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/OrionResourceEdit.master"
 CodeFile="AddRemoveObjects.aspx.cs" Inherits="Orion_NPM_Controls_AddRemoveObjects" %>
 
<%@ Register Src="~/Orion/NetPerfMon/Controls/EditResourceControls/AddRemoveObjects/AddRemoveObjectsControl.ascx" TagName="AddRemoveObjectsControl" TagPrefix="orion" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register Src="~/Orion/Controls/HelpHint.ascx" TagPrefix="orion" TagName="HelpHint" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPlaceHolder" Runat="Server">
    <orion:Include runat="server" File="Admin/Containers/Containers.css" />
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js" />
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h1 style="font-size:large!important;"><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    
    <br />
    <p style="padding-bottom: 5px;">
	    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_25) %>
    </p>
    <orion:HelpHint ID="groupsHelpLink" runat="server">
        <HelpContent>
            <%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBDATA_VB1_26, string.Format("<a class=\"coloredLink\" href=\"/Orion/Admin/Containers/Add/Default.aspx{0}\"id=\"link\"><u>", ReturnUrl), "</u>.</a>")) %>
        </HelpContent>
    </orion:HelpHint>
    <br />

	<orion:AddRemoveObjectsControl ID="addRemoveObjectsControl" runat="server" />
	
	<div class="sw-btn-bar">
        <orion:LocalizableButton ID="submitButton" runat="server" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitClick"/>
        <orion:LocalizableButton ID="cancelButton" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="CancelClick"/>
	</div>
</asp:Content>
