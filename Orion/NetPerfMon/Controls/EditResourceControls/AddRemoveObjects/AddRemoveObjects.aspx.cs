﻿using System;
using Resources;
using SolarWinds.Orion.Web.Helpers;
using ContainerModel = SolarWinds.Orion.Core.Common.Models.Container;
using System.Collections.Generic;
using System.Collections.Specialized;

public partial class Orion_NPM_Controls_AddRemoveObjects : System.Web.UI.Page
{
	private string SessionGuid;
    private const string SessionID = "SessionID";
    private const string DefaultReturnUrl = "/Orion/NetPerfMon/Resources/EditResource.aspx{0}";
    private StringDictionary parameters = null;

    private bool LoadSessionData()
    {
        try
        {
            if (Request.QueryString[SessionID] != null)
            {
                SessionGuid = UrlHelper.FromSafeBase64UrlParameter(Request.QueryString[SessionID]);
            }
            if (Session[SessionGuid] != null)
            {
                parameters = (StringDictionary)Session[SessionGuid];
                return true;
            }
        }
        catch { }
        return false;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        { 
            if (!LoadSessionData() || parameters == null)
            {
                throw new SolarWinds.Internationalization.Exceptions.LocalizedExceptionBase(() => CoreWebContent.WEBCODE_VB0_353);
            }
            
            string entities = parameters.ContainsKey("Entities") ? parameters["Entities"] : Resources.CoreWebContent.WEBDATA_AK0_162;
            string entityName = parameters.ContainsKey("EntityName") ? parameters["EntityName"] : "Orion.Nodes";
            string filter = parameters.ContainsKey("Filter") ? parameters["Filter"] : String.Empty;
            string selectedEntities = parameters.ContainsKey("SelectedEntities") ? parameters["SelectedEntities"] : String.Empty;

            this.Title = String.Format(Resources.CoreWebContent.WEBCODE_VB1_7, entities);
            this.addRemoveObjectsControl.LeftPanelTitle = String.Format(Resources.CoreWebContent.WEBCODE_VB1_5, entities);
            this.addRemoveObjectsControl.RightPanelTitle = String.Format(Resources.CoreWebContent.WEBCODE_VB1_6, entities);
            this.addRemoveObjectsControl.EntityName = entityName;
            this.addRemoveObjectsControl.Filter = filter;
            this.addRemoveObjectsControl.SelectedItems = selectedEntities;

            groupsHelpLink.Visible = entityName.Equals("Orion.Groups", StringComparison.OrdinalIgnoreCase);

        }
    }

    private string CopySession()
    {
        if (!LoadSessionData() || parameters == null)
        {
            throw new SolarWinds.Internationalization.Exceptions.LocalizedExceptionBase(() => CoreWebContent.WEBCODE_VB0_353);
        }

        Session[SessionGuid] = parameters;
        parameters[SessionID] = SessionGuid;
        parameters["SelectedEntities"] = FilterConditionHelper.ResolveSelectedEntitesIds(this.addRemoveObjectsControl.SelectedItems);
        return SessionGuid;
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        string guid = CopySession();
        string url;        

        if (Request.QueryString["ReturnTo"] != null)
        {
            url = UrlHelper.FromSafeUrlParameter(Request.QueryString["ReturnTo"]);
        } else
        {
            url = String.Format(DefaultReturnUrl, GetUrlParameters(SessionID, guid));
        }

        url = UrlHelper.UrlAppendUpdateParameter(url, SessionID, UrlHelper.ToSafeBase64UrlParameter(SessionGuid));
        this.Response.Redirect(url);
    }

    protected void CancelClick(object sender, EventArgs e)
    {
        string guid = CopySession();
        string url;
        if (Request.QueryString["ReturnTo"] != null)
        {
            url = UrlHelper.FromSafeUrlParameter(Request.QueryString["ReturnTo"]);
        }
        else
        {
            url = String.Format(DefaultReturnUrl, GetUrlParameters(SessionID, guid));
        }
        url = UrlHelper.UrlAppendUpdateParameter(url, SessionID, UrlHelper.ToSafeBase64UrlParameter(SessionGuid));

        this.Response.Redirect(url);
    }

    private string GetUrlParameters(string name, string value)
    {
        var list = new List<string>();

        AddUrlParameter(list, "ResourceID");
        AddUrlParameter(list, "NetObject");
        AddUrlParameter(list, "ViewID");

        if (!String.IsNullOrEmpty(name) && !String.IsNullOrEmpty(value))
        {
            list.Add(String.Format("{0}={1}", name, value));
        }

		return (list.Count > 0) ? "?" + String.Join("&", list.ToArray()) : String.Empty;
    }

    private void AddUrlParameter(List<string> list, string name)
    {
        if (!String.IsNullOrEmpty(Request.QueryString[name]))
            list.Add(String.Format("{0}={1}", name, Request[name]));
    }

    public string ReturnUrl
	{
		get
		{
			return string.Format("?returnUrl={0}", UrlHelper.ToSafeUrlParameter(Request.Url.ToString()));
		}
	}
}
