﻿using System;
using System.Collections.Generic;

using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditAllAlerts : BaseResourceEditControl
{
    private Dictionary<string, object> _properties = new Dictionary<string, object>();

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        _properties.Add("ShowAcknowledgedAlerts", ShowAckAlertsCheckBox.Checked);
        string showAck = Resource.Properties["ShowAcknowledgedAlerts"];
        bool ch = false;

        if (!String.IsNullOrEmpty(showAck))
            Boolean.TryParse(showAck, out ch);

        ShowAckAlertsCheckBox.Checked = ch;
        if (System.Web.HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
        {
            ShowAckAlertsCheckBox.Attributes.Add("onclick", "javascript:SaveData('ShowAcknowledgedAlerts', this.checked ? 'True' : 'False');");
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            _properties["ShowAcknowledgedAlerts"] = ShowAckAlertsCheckBox.Checked;
            return _properties;
        }
    }
}
