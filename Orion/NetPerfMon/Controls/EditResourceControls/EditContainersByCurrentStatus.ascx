<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditContainersByCurrentStatus.ascx.cs"
    Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditContainersByCurrentStatus" %>
<table border="0">
    <tr>
        <td>
            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_283) %></b>
            <br />
            <asp:Repeater runat="server" ID="statesRepeater" OnInit="StatesRepeater_Init">
              <ItemTemplate>
                <asp:CheckBox Font-Size="10pt" runat="server" id="stateCheckbox" Text='<%# DefaultSanitizer.SanitizeHtml(Eval("Name")) %>'
                    Checked='<%# DefaultSanitizer.SanitizeHtml(Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "Value"))) %>'  />  
                    <br />    
              </ItemTemplate>
            </asp:Repeater>           
        </td>
    </tr>
    <tr>
        <td>
            <br/>
            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_284) %></b>
            <br/>
            <asp:TextBox runat="server" ID="tbFilter" MaxLength="200" Width="300px" />
        </td>
    </tr>
    <tr>
        <td>
            <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_285) %>.<br/></p>
        </td>
    </tr>
    <tr>
        <td style="font-size:8pt">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_286) %>
        </td>
    </tr>
</table> 
