<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NetworkTopologyEdit.ascx.cs"
    Inherits="Orion_NetPerfMon_Controls_EditResourceControls_NetworkTopologyEdit" %>
    <%@ Register TagPrefix="orion" TagName="AutoHide" Src="~/Orion/Controls/AutoHideControl.ascx" %>
<table>
    <tr>
        <td style="white-space:nowrap;">
            <b><%= Resources.TopologyWebContent.WEBDATA_AK0_357 %></b>
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox runat="server" ID="maxCount" Columns="5"></asp:TextBox>
            <asp:RangeValidator runat="server" Display="Dynamic" ControlToValidate="maxCount"
                Type="Integer" MaximumValue="250" MinimumValue="1" ErrorMessage="*"></asp:RangeValidator>
        </td>
    </tr>
    <tr>
        <td>
            <b><%= Resources.TopologyWebContent.WEBDATA_AK0_358 %></b><br />
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox runat="server" ID="NodeSrcFilter" Width="330"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <b><%= Resources.TopologyWebContent.WEBDATA_AK0_359 %></b><br />
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox runat="server" ID="NodeDestFilter" Width="330"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
        <%= Resources.TopologyWebContent.WEBDATA_AK0_360 %>

            <orion:CollapsePanel ID="CollapsePanel1" runat="server" Collapsed="true">
                <TitleTemplate>
                    <b><%= Resources.TopologyWebContent.WEBDATA_AK0_300 %></b></TitleTemplate>
                <BlockTemplate>
                    <p><%= Resources.TopologyWebContent.WEBDATA_PC0_01 %></p>

		                    <p><%= Resources.TopologyWebContent.WEBDATA_PC0_02 %><br />
		                    <b><%= Resources.TopologyWebContent.WEBDATA_PC0_03 %></b></p>

		                    <p><%= Resources.TopologyWebContent.WEBDATA_PC0_04 %>
		
			                    </p><table>

				                    <tbody><tr>
					                    <td>0&nbsp;=&nbsp;<b><%= Resources.TopologyWebContent.WEBDATA_PC0_05 %></b></td><td><%= Resources.TopologyWebContent.WEBDATA_PC0_06 %></td>
				                    </tr>
				                    <tr>
					                    <td>1&nbsp;=&nbsp;<b><%= Resources.TopologyWebContent.WEBDATA_PC0_07 %></b></td><td><%= Resources.TopologyWebContent.WEBDATA_PC0_08 %></td>
				                    </tr><tr>

					                    <td>2&nbsp;=&nbsp;<b><%= Resources.TopologyWebContent.WEBDATA_PC0_09 %></b></td><td><%= Resources.TopologyWebContent.WEBDATA_PC0_10 %></td>
				                    </tr><tr>
					                    <td>3&nbsp;=&nbsp;<b><%= Resources.TopologyWebContent.WEBDATA_PC0_11 %></b></td><td><%= Resources.TopologyWebContent.WEBDATA_PC0_12 %></td>
				                    </tr>
			                    </tbody></table>
		

		                    <p><%= Resources.TopologyWebContent.WEBDATA_PC0_13 %><br />
		                    <b><%= Resources.TopologyWebContent.WEBDATA_PC0_14 %></b></p>

		                    <p><%= Resources.TopologyWebContent.WEBDATA_PC0_15 %><br />
		                    <b><%= Resources.TopologyWebContent.WEBDATA_PC0_16 %></b></p>
						
		                    <p><%= Resources.TopologyWebContent.WEBDATA_PC0_17 %><br />
		                    <b><%= Resources.TopologyWebContent.WEBDATA_PC0_18 %></b></p>

		                    <p><%= Resources.TopologyWebContent.WEBDATA_PC0_19 %><br />
		                    <b><%= Resources.TopologyWebContent.WEBDATA_PC0_20 %></b></p>

		                    <p><%= Resources.TopologyWebContent.WEBDATA_PC0_21 %><br />
                            <b><%= Resources.TopologyWebContent.WEBDATA_PC0_22 %></b></p>
		                    <p>
                    <p>
                    </p>
                    <orion:CollapsePanel ID="CollapsePanel1" runat="server" Collapsed="true">
                        <TitleTemplate>
                            <b><%= Resources.TopologyWebContent.WEBDATA_AK0_361 %></b></TitleTemplate>
                        <BlockTemplate>
                            <ul>
                                <%foreach (string column in SolarWinds.Orion.NPM.Web.Node.GetAllColumnNames())
                                  {%>
                                <li>
                                    <%=column%></li>
                                <%}%>
                            </ul>
                        </BlockTemplate>
                    </orion:CollapsePanel>
                </BlockTemplate>
            </orion:CollapsePanel>
        </td>
    </tr>
     <tr>
        <orion:AutoHide runat="server" ID="autoHide" Description="<%$ Resources: TopologyWebContent,WEBDATA_AK0_309 %>" />
    </tr>
</table>