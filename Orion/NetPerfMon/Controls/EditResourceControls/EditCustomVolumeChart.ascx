<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditCustomVolumeChart.ascx.cs"
    Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditCustomVolumeChart" %>

<%@ Register TagPrefix="orion" TagName="EditSampleSize" Src="~/Orion/Controls/SampleSizeControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/PeriodControl.ascx" %>
    

<div style="font-weight: bold;">
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_293) %>
</div>
<div class="sw-res-editor-row">
    <asp:DropDownList runat="server" ID="ListOfCharts" />
</div>
<div>
    <orion:EditPeriod runat="server" ID="Period"  Inline="true"/>
</div>
<div>
    <orion:EditSampleSize runat="server" Inline="true" ID="SampleSize" />
</div>
