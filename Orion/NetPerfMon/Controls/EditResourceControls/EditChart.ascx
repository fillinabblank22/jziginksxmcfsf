<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditChart.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditChart" %>

<style type="text/css">
    .sw-res-editor .sectionHeader {
        font-weight: bold;
        padding-top: 20px;
        padding-bottom: 8px;
    }
    
    .chartSpecificSettings {
        margin-left: 20px;
        padding: 15px;
        border: 1px solid #ecedee;
        width: 550px;        
    }
    
    .advanced {
        margin-left: 20px;
        padding: 20px;
        background-color: #ecedee;
        width: 550px;
    }
</style>

<script type="text/javascript">

    (function () {

        $(function () {
            $('#advancedBtn').click(function () {
                var btn = $('#advancedBtn');
                var isExpanded = btn.attr('data-expanded') == 1;

                if (isExpanded) {
                    // collapse
                    btn.attr('data-expanded', 0);
                    btn.attr('src', '/Orion/images/Button.Expand.gif');
                    $('.advanced').slideUp();
                } else {
                    // expand
                    btn.attr('data-expanded', 1);
                    btn.attr('src', '/Orion/images/Button.Collapse.gif');
                    $('.advanced').slideDown();
                }
            });
        });
    })();



</script>


<div runat="server" ID="ErrorMessage" class="sw-suggestion sw-suggestion-fail" Visible="False">
    <span class="sw-suggestion-icon"></span>
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_87) %>
</div>

<div runat="server" ID="SectionData" class="sectionHeader">
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_88) %>
</div>
<div runat="server" ID="SectionListOfCharts" class="sw-res-editor-row">
    <asp:DropDownList runat="server" ID="ListOfCharts" AutoPostBack="True" OnSelectedIndexChanged="ListOfCharts_OnSelectedIndexChanged"/>
</div>

<div runat="server" ID="chartSpecificSettings" class="chartSpecificSettings" Visible="False"></div>




<div runat="server" ID="SectionDataSeries" class="sectionHeader">
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_89) %>
</div>



<div class="sw-res-editor-row" runat="server" ID="ShowTopPanel">
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_90) %>
    <asp:DropDownList runat="server" ID="ShowTop">
        <asp:ListItem Value="0" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_91%>"></asp:ListItem>
        <asp:ListItem Value="5" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_92, 5)%>"></asp:ListItem>
        <asp:ListItem Value="10" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_92, 10)%>"></asp:ListItem>
        <asp:ListItem Value="20" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_92, 20)%>"></asp:ListItem>
        <asp:ListItem Value="30" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_92, 30)%>"></asp:ListItem>
    </asp:DropDownList>
</div>


<div runat="server" ID="SectionCalulatedSeries" class="sectionHeader">
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_93) %>
</div>

<div>
    <asp:CheckBox runat="server" ID="CalculateTrendLine" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_94 %>"/>
</div>
<div>
    <asp:CheckBox runat="server" ID="CalculateSumLine" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_95 %>"/>
</div>
<div>
    <asp:CheckBox runat="server" ID="Calculate95thPercentileLine" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_96 %>"/>
</div>
<div>
    <asp:CheckBox runat="server" ID="ShowThreshold" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_ZT0_54 %>"/>
</div>


<div runat="server" ID="SectionTimePeriods" class="sectionHeader">
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_97) %>
</div>

<div class="sw-res-editor-row" runat="server" ID="ZoomPanel">
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_98) %> 
    <asp:DropDownList runat="server" ID="ChartZoom">
        <asp:ListItem Value="1h" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_99%>"></asp:ListItem>
        <asp:ListItem Value="2h" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_100, 2)%>"></asp:ListItem>
        <asp:ListItem Value="24h" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_100, 24)%>"></asp:ListItem>
        <asp:ListItem Value="today" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_VB0_133%>"></asp:ListItem>
        <asp:ListItem Value="yesterday" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_101%>"></asp:ListItem>
        <asp:ListItem Value="7d" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_187%>"></asp:ListItem>
        <asp:ListItem Value="thisMonth" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_102%>"></asp:ListItem>
        <asp:ListItem Value="lastMonth" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_103%>"></asp:ListItem>
        <asp:ListItem Value="30d" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_188%>"></asp:ListItem>
        <asp:ListItem Value="3m" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_104, 3)%>"></asp:ListItem>
        <asp:ListItem Value="6m" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_104, 6)%>"></asp:ListItem>
        <asp:ListItem Value="thisYear" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_105%>"></asp:ListItem>
        <asp:ListItem Value="12m" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_104, 12)%>"></asp:ListItem>        
    </asp:DropDownList>
</div>


<div class="sw-res-editor-row" runat="server" ID="TimeSpanPanel">
    <span runat="server" ID="TimeSpanLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_108) %></span> 
    <asp:DropDownList runat="server" ID="ChartTimeSpan">
        <asp:ListItem Value="1" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_107%>"></asp:ListItem>
        <asp:ListItem Value="7" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_187%>"></asp:ListItem>
        <asp:ListItem Value="30" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_103%>"></asp:ListItem>
        <asp:ListItem Value="90" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_104, 3)%>"></asp:ListItem>
        <asp:ListItem Value="365" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_106%>"></asp:ListItem>
    </asp:DropDownList>
</div>

<div class="sw-res-editor-row" runat="server" ID="SampleSizePanel">
    <span runat="server" ID="SampleSizeLabel"></span>
    <asp:DropDownList runat="server" ID="SampleSize">
        <asp:ListItem Value="1" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_110 %>"></asp:ListItem>
        <asp:ListItem Value="5" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_111, 5)%>"></asp:ListItem>
        <asp:ListItem Value="10" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_111, 10)%>"></asp:ListItem>
        <asp:ListItem Value="15" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_111, 15)%>"></asp:ListItem>
        <asp:ListItem Value="30" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_111, 30)%>"></asp:ListItem>
        <asp:ListItem Value="60" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_112 %>"></asp:ListItem>
        <asp:ListItem Value="120" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_113, 2)%>"></asp:ListItem>
        <asp:ListItem Value="360" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_113, 6)%>"></asp:ListItem>
        <asp:ListItem Value="720" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_113, 12)%>"></asp:ListItem>
        <asp:ListItem Value="1440" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_114 %>"></asp:ListItem>
        <asp:ListItem Value="10080" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_115 %>"></asp:ListItem>
    </asp:DropDownList>
    <div class="sw-text-helpful">
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_116) %>
    </div>
</div>

<asp:CustomValidator runat="server" OnServerValidate="ValidateNumberOfPoints" Display="None"></asp:CustomValidator>


<div runat="server" ID="SectionDisplayBehavior" class="sectionHeader">
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_117) %>
</div>

<div>
    <asp:CheckBox runat="server" ID="AutoHide" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_118 %>"/>
</div>

<div runat="server" ID="SectionAdvanced" class="sectionHeader">
    <img src="/Orion/images/Button.Expand.gif" id="advancedBtn" data-expanded="0" alt=""/> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_216) %>
</div>

<div class="advanced" style="display: none;">
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_119) %>
    <div class="sw-res-editor-row">
        <asp:TextBox runat="server" ID="ChartTitle"></asp:TextBox>

    </div>

    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_120) %>
    <div class="sw-res-editor-row">
        <asp:TextBox runat="server" ID="ChartSubtitle"></asp:TextBox>
    </div> 
    

</div>



