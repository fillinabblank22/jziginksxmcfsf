<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditEventSummary.ascx.cs"
    Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditEventSummary" %>
<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/PeriodControl.ascx" %>
<table border="0">
    <tr>
        <td>
            <asp:Label runat="server" ID="label1" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_311 %>" Font-Bold="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:DropDownList runat="server" ID="Period" />
        </td>
    </tr>
</table>
