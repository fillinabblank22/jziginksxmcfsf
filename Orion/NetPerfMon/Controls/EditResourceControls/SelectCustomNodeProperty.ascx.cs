﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_SelectCustomNodeProperty : BaseResourceEditControl
{
	protected override void OnInit(EventArgs e)
	{
		lbxCustomProps.DataSource = Node.GetCustomPropertyNames(true);
		lbxCustomProps.DataBind();

        var prop = Resource.Properties["CustomProperty"];
        if (!string.IsNullOrEmpty(prop))
        {
            this.lbxCustomProps.SelectedValue = Resource.Properties["CustomProperty"];
        }

        if (System.Web.HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
        {
            lbxCustomProps.Attributes.Add("onchange", "javascript:SaveData('CustomProperty', this.value);");
        }

		base.OnInit(e);
	}

	public override Dictionary<string, object> Properties
	{
		get 
		{
			Dictionary<string, object> properties = new Dictionary<string, object>();
			properties.Add("CustomProperty", this.lbxCustomProps.SelectedValue);

			return properties;
		}
	}
}
