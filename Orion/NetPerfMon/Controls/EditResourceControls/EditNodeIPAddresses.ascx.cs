﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditNodeIPAddresses : BaseResourceEditControl
{    
    private const string _propertyRowsPerPage = "RowsPerPage";
    private const string _defaultRowsPerPage = "5";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        RowsPerPage.Text = Resource.Properties[_propertyRowsPerPage] ?? _defaultRowsPerPage;
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            var properties = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase)
            {
                {_propertyRowsPerPage, RowsPerPage.Text}
            };
            return properties;
        }
    }
}