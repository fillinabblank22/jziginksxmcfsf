<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditCustomPropertyList.ascx.cs"
    Inherits="Orion_NetPerfMon_Controls_EditCustomPropertyList" %>
<div>
    <div runat="server" id="propertiesList">
        <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_294) %></b>
        <br />
        <asp:CheckBoxList runat="server" ID="selectedProperties" OnDataBound="SelectedProperties_DataBound" />
    </div>
    <div runat="server" id="noPropertiesMessage" style="color: Red; font-weight: bold;"
        visible="false">
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_295) %></div>
    <br />
</div>
