<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditLastxxMessages.ascx.cs"
    Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditLastxxMessages" %>

<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/PeriodControl.ascx" %>
<table border="0">
    <tr>
        <td>
            <asp:Label runat="server" ID="label" Text="<%$ Resources: SyslogTrapsWebContent,WEBDATA_AK0_330 %>" Font-Bold="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox runat="server" ID="MaxRecordsText" Width="50" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label runat="server" ID="label1" Text="<%$ Resources: SyslogTrapsWebContent,WEBDATA_AK0_263 %>" Font-Bold="true" />
        </td>
    </tr>
    <tr>

        <td>
            <asp:DropDownList runat="server" ID="Period" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label2" runat="server" Text="<%$ Resources: SyslogTrapsWebContent,WEBDATA_AK0_329 %>" Font-Bold="true"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox ID="FilterText" runat="server" Width="430px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="font-size: xx-small;">
            <%= Resources.SyslogTrapsWebContent.WEBDATA_AK0_320 %><br /><br />
            <orion:CollapsePanel ID="CollapsePanel1" runat="server" Collapsed="True">
                <TitleTemplate>
                    <b><%= Resources.SyslogTrapsWebContent.WEBDATA_AK0_321 %></b>
                </TitleTemplate>
                <BlockTemplate>
                    <table>
	                    <tr>
		                    <br /><%= Resources.SyslogTrapsWebContent.WEBDATA_PC0_26 %><br />
		                    <b><%= Resources.SyslogTrapsWebContent.WEBDATA_PC0_27 %></b><br /><br /><%= Resources.SyslogTrapsWebContent.WEBDATA_PC0_28 %><br />
		                    <b><%= Resources.SyslogTrapsWebContent.WEBDATA_PC0_29 %></b><br /><br />
		                    <table border="0" cellspacing="0">
			                    <tr>
				                    <td><%= Resources.SyslogTrapsWebContent.WEBDATA_PC0_30 %></td>
			                    </tr>
			                    <tr>
				                    <td>0&nbsp;&nbsp;<%= Resources.SyslogTrapsWebContent.WEBDATA_PC0_31 %></td>
			                    </tr>
			                    <tr>
				                    <td>1&nbsp;&nbsp;<%= Resources.SyslogTrapsWebContent.WEBDATA_PC0_32 %></td>
			                    </tr>
			                    <tr>
				                    <td>2&nbsp;&nbsp;<%= Resources.SyslogTrapsWebContent.WEBDATA_PC0_33 %></td>
			                    </tr>
			                    <tr>
				                    <td>3&nbsp;&nbsp;<%= Resources.SyslogTrapsWebContent.WEBDATA_PC0_34 %></td>
			                    </tr>
			                    <tr>
				                    <td>4&nbsp;&nbsp;<%= Resources.SyslogTrapsWebContent.WEBDATA_PC0_35 %></td>
			                    </tr>
			                    <tr>
				                    <td>5&nbsp;&nbsp;<%= Resources.SyslogTrapsWebContent.WEBDATA_PC0_36 %></td>
			                    </tr>
			                    <tr>
				                    <td>6&nbsp;&nbsp;<%= Resources.SyslogTrapsWebContent.WEBDATA_PC0_37 %></td>
			                    </tr>
			                    <tr>
				                    <td>7&nbsp;&nbsp;<%= Resources.SyslogTrapsWebContent.WEBDATA_PC0_38 %></td>
			                    </tr>
		                    </table>
                            <br/><%= Resources.SyslogTrapsWebContent.WEBDATA_IB0_228 %><br/>
                            <b><%= Resources.SyslogTrapsWebContent.WEBDATA_IB0_229 %></b><br />
		                    <br /><%= Resources.SyslogTrapsWebContent.WEBDATA_PC0_39 %><br />
		                    <b><%= Resources.SyslogTrapsWebContent.WEBDATA_PC0_40 %></b><br /><br />
		                    <b><%= Resources.SyslogTrapsWebContent.WEBDATA_PC0_41 %></b><br /><br />
		                    <b><%= Resources.SyslogTrapsWebContent.WEBDATA_PC0_42 %></b><br />
		                    </td>
	                    </tr>
                    </table>                    
                </BlockTemplate>
            </orion:CollapsePanel>
        </td>
    </tr>
    <tr>
        <td >
            <asp:RangeValidator ID="EventsRangeValidator" runat="server" ErrorMessage="<%$ Resources: SyslogTrapsWebContent,WEBDATA_AK0_319 %>"
                ControlToValidate="MaxRecordsText" Display="Dynamic" MinimumValue="1" MaximumValue="5000"
                Type="Integer" />
        </td>
    </tr>
</table>