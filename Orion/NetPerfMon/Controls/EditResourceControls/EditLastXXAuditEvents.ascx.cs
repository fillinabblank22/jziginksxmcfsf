﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;

using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Charting;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditLastXXAuditEvents : BaseResourceEditControl
{
	private const int DefaultNumberOfEvents = 10;
	private readonly Log _log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

	private DataTable GetTypes()
	{
		DataTable dt;
        using (var proxy = _blProxyCreator.Create(ex => _log.Error(ex)))
		{
			dt = proxy.GetAuditingTypesTable();
		}
		return dt;
	}

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

        // fill in period list
        foreach (var period in Periods.GenerateSelectionList())
            Period.Items.Add(new ListItem(Periods.GetLocalizedPeriod(period), period));

        MaxEventsText.Text = Resource.Properties["MaxRecords"];
        Period.Text = Resource.Properties["Period"];

		if (!string.IsNullOrEmpty(Resource.Properties["SelectedTypes"]))
		{
			SelectedTypes = new List<string>(Resource.Properties["SelectedTypes"].Split(','));
		}

		if (!this.IsPostBack)
		{
			this.DataSource = GetTypes();
		}
	}

	public override Dictionary<string, object> Properties
	{
		get
		{
			var properties = new Dictionary<string, object>();
			try
			{
				Int32.Parse(MaxEventsText.Text);
				properties.Add("MaxRecords", MaxEventsText.Text);
			}
			catch
			{
				properties.Add("MaxRecords", DefaultNumberOfEvents.ToString());
			}

			var selected = GetSelectedTypes();
			if (selected != null && selected.Count > 0)
				properties.Add("SelectedTypes", string.Join(",", selected.ToArray()));

            properties.Add("Period", Period.Text);

			return properties;
		}
	}

	public DataTable DataSource
	{
		set
		{
			auditTypes.DataSource = value;
			auditTypes.DataBind();
		}
	}

	public List<string> SelectedTypes
	{
		get;
		set;
	}

	protected void AuditTypes_ItemDataBound(Object sender, RepeaterItemEventArgs e)
	{
		if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
		{
			CheckBox cb = (CheckBox)e.Item.FindControl("chkbx");
			string auditType = ((DataRowView)e.Item.DataItem)["ActionTypeID"].ToString();
			cb.Attributes.Add("ActionTypeID", auditType);
			cb.Text = ((DataRowView)e.Item.DataItem)["ActionTypeDisplayName"].ToString();

			if (SelectedTypes != null && SelectedTypes.Count>0)
				cb.Checked = SelectedTypes.Contains(auditType);
		}
	}

	public List<string> GetSelectedTypes()
	{
		List<string> ilist = new List<string>();

		foreach (RepeaterItem rpItem in auditTypes.Items)
		{
			CheckBox chkbx = rpItem.FindControl("chkbx") as CheckBox;
			if (chkbx != null && chkbx.Checked)
			{
				ilist.Add(chkbx.Attributes["ActionTypeID"].ToString());
			}
		}

		return ilist;
	}

	protected void ColumnsValidation(object source, ServerValidateEventArgs args)
	{
		foreach (RepeaterItem rpItem in auditTypes.Items)
		{
			CheckBox chkbx = rpItem.FindControl("chkbx") as CheckBox;
			if (chkbx.Checked)
			{
				args.IsValid = true;
				return;
			}
		}

		args.IsValid = false;
	}
}
