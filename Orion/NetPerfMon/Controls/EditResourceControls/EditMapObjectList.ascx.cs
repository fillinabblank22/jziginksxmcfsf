﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;

using SolarWinds.MapEngine;
using SolarWinds.MapEngine.Helpers;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditMapObjectList : BaseResourceEditControl
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

	public override Dictionary<string, object> Properties
	{
		get
		{
			var properties = new Dictionary<string, object>();
            if (mapsListbox.SelectedItem != null)
            {
                properties["MapId"] = mapsListbox.SelectedValue;
                properties["MapNameFormat"] = mapNameFormatsDropDown.SelectedValue;
            }
		    return properties;
		}
	}
    

    protected override void OnInit(EventArgs e)
    {
        string rootLocalPath = MapService.GetTempPath();
        string applicationLocalPath = MapService.GetMapStudioInstallPath();

        string swoisEndpoint = string.Format(System.Configuration.ConfigurationManager.AppSettings["SWOISv3.RemoteEndpoint"], "localhost");
        string serverAddress, swisPort, swisPostfix;
        MapService.ParseEndpointLocation(swoisEndpoint, out serverAddress, out swisPort, out swisPostfix);

        string selectedMapId = Resource.Properties["MapId"];

        string errorMessage = string.Empty;
        var credentials = CredentialsHelper.GetOrionCertificateCredentials();
        Dictionary<string, string> availableMaps = MapService.GetAvailableMaps(HttpContext.Current.Profile.UserName,
            credentials,
            rootLocalPath,
            applicationLocalPath,
            serverAddress,
            swisPort,
            swisPostfix,
            0,
            false,
            ref errorMessage);

        if (!string.IsNullOrEmpty(errorMessage))
        {
            log.ErrorFormat("Error reading available maps. Exception message: {0}", errorMessage);
        }

        int index = 0;
        foreach (var map in availableMaps)
        {
            string id = map.Key.ToString().Replace("Maps: ", string.Empty);
            string displayName = GetLocalizedProperty("MapName", System.IO.Path.GetFileNameWithoutExtension(map.Value));
            mapsListbox.Items.Add(new ListItem(displayName, id + Path.GetExtension(map.Value)));
            if (id == System.IO.Path.GetFileNameWithoutExtension(selectedMapId))
            {
                mapsListbox.SelectedIndex = index;
            }
            index++;
        }
        if (mapsListbox.SelectedIndex == -1 && mapsListbox.Items.Count > 0)
        {
            mapsListbox.SelectedIndex = 0;
        }

        //fill formats ddl, only for groups (containers)
        if (!string.IsNullOrEmpty(Request["NetObject"]) &&
            Request["NetObject"].StartsWith("C:", true, CultureInfo.InvariantCulture))
        {
            foreach (var keyValuePair in MapHelper.GetMapNameFormats())
            {
                mapNameFormatsDropDown.Items.Add(new ListItem(keyValuePair.Key, keyValuePair.Value));
            }

            mapNameFormatsDropDown.SelectedValue = Resource.Properties["MapNameFormat"];
        }
        else
        {
            mapNameFormatsPanel.Visible = false;
        }

        base.OnInit(e);
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
}
