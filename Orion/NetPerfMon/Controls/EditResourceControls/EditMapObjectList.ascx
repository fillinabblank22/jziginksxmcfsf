<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditMapObjectList.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditMapObjectList" %>

    <script language="javascript" type="text/javascript">
        // will be executed on page load
        $(function () {
            if ($("select[id$=mapNameFormatsDropDown]")) {
                // adds onChange event to mapNameFormatsDropDown
                $("select[id$=mapNameFormatsDropDown]").change(
                    function () {
                        // selected value is empty
                        if ($(this).val()) {
                            // disables mapsListbox
                            $("select[id$=mapsListbox]").attr('disabled', 'disabled');
                        } else {
                            // enables mapsListbox
                            $("select[id$=mapsListbox]").removeAttr('disabled');
                        }
                    }
                );

                // if map format is not selected 
                if ($("select[id$=mapNameFormatsDropDown]").val()) {
                    // disables mapsListbox
                    $("select[id$=mapsListbox]").attr('disabled', 'disabled');
                } else {
                    // enables mapsListbox
                    $("select[id$=mapsListbox]").removeAttr('disabled');
                };
            }
        });
    </script>
    
<div>
    <asp:ListBox runat="server" ID="mapsListbox" Width="400" Height="400"></asp:ListBox>
    <asp:RequiredFieldValidator runat="server" InitialValue="" Display="Dynamic"
                                ControlToValidate="mapsListbox"
                                ErrorMessage="Atleast one member required"
                                Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_164 %>" />

    <asp:Panel id="mapNameFormatsPanel" runat="server">
        <div style="font-weight: bold; padding-top: 18px"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_2) %></div>
        <asp:DropDownList ID="mapNameFormatsDropDown" runat="server" Width="400"></asp:DropDownList>
    </asp:Panel>
    
</div>