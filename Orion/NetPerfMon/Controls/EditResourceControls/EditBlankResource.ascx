<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditBlankResource.ascx.cs"
    Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditBlankResource" %>

<b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_77) %></b><br/>
    <asp:TextBox runat="server" ID="txtHeight" Rows="1" Columns="1" Width="50"/>
    <asp:RangeValidator ID="heightRangeValidator" runat="server" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_TM1_4 %>"
                Display="Dynamic" ControlToValidate="txtHeight" MinimumValue="1" MaximumValue="9999"
                Type="Integer" />

