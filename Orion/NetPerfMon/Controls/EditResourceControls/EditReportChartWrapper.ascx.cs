﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Reporting.DAL;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Reporting;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Reporting.Models.Charts;
using SolarWinds.Orion.Web.Reporting.ReportCharts;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditReportChartWrapper : BaseResourceEditControl
{
    public override Dictionary<string, object> Properties
    {
        get
        {
            if (ReportDataProvider is ResourcePropertiesReportData)
            {
                string configXml = SerializationHelper.ToXmlString(reportChartConfigruration.ViewModel, Loader.GetKnownTypes());
                string dataSourceXml = SerializationHelper.ToXmlString(ReportDataProvider.GetDataSource(), Loader.GetKnownTypes());

                var properties = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase)
                                     {
                                         {WebConstants.ConfigurationDataMoniker, configXml},
                                         {WebConstants.DataSourceMoniker, dataSourceXml},
                                         // todo: deal with TF on view
                                         //{WebConstants.TimeFrameMoniker, null},
                                     };
                return properties;
            }
            return new Dictionary<string, object>();
        }
    }

    public override IEnumerable<Control> ButtonsForButtonBar
    {
        get
        {
            var previewBtn = new LocalizableButton { Text = Resources.CoreWebContent.WEBCODE_PF0_9 };
            if (OrionConfiguration.IsDemoServer)
            {
                previewBtn.OnClientClick = "demoAction('Core_Reporting_PreviewResource', this); return false;";
            }
            else
            {
                previewBtn.Click += new EventHandler(previewBtn_Click);
            }
            yield return previewBtn;
        }
    }

    public ReportDataProviderFactory.ReportDataProviderBase ReportDataProvider { get; set; }

    public void UpdateViewModel(ChartConfiguration config)
    {
        reportChartConfigruration.DataSource = ReportDataProvider.GetDataSource();
        reportChartConfigruration.ViewModel = ReportDataProvider.GetConfigurationData<ChartConfiguration>();
        ReportDataProvider.UpdateConfigurationData(config);
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (ReportDataProvider == null)
        {
            ReportDataProvider = ReportDataProviderFactory.GetReportDataProvider(this, ReportChartRenderer.Moniker);
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        reportChartConfigruration.DataSource = ReportDataProvider.GetDataSource();
        if (!Page.IsPostBack)
        {
            reportChartConfigruration.ViewModel = ReportDataProvider.GetConfigurationData<ChartConfiguration>();
        }
        else
        {
            ReportDataProvider.UpdateConfigurationData(reportChartConfigruration.ViewModel);
        }

    }

    void previewBtn_Click(object sender, EventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            var report = ReportDataProvider.GetReport();
            var cell = ReportDataProvider.GetCell();


            PreviewPanel.Controls.Clear();
            PreviewPanel.Controls.Add(new ReportCellPreviewModal
            {
                Model = report,
                AllowToolTips = true,
                CellId = cell.RefId
            });
        }
    }

}
