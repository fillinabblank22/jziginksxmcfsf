using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditSysLogSummary : BaseResourceEditControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.GroupItems.Items.Add(new ListItem(Resources.SyslogTrapsWebContent.WEBCODE_AK0_90, "Hostname"));
            this.GroupItems.Items.Add(new ListItem(Resources.SyslogTrapsWebContent.WEBDATA_VB0_13, "IPAddress"));
            this.GroupItems.Items.Add(new ListItem(Resources.SyslogTrapsWebContent.WEBCODE_AK0_91, "MessageType"));
            this.GroupItems.Items.Add(new ListItem(Resources.SyslogTrapsWebContent.WEBCODE_AK0_92, "FacilityName"));
            this.GroupItems.Items.Add(new ListItem(Resources.SyslogTrapsWebContent.WEBCODE_AK0_93, "SeverityName"));
            this.GroupItems.Items.Add(new ListItem(Resources.SyslogTrapsWebContent.WEBCODE_AK0_94, "SyslogTag"));

            this.GroupItems.SelectedValue = Resource.Properties["GroupBy"];

            // fill in period list
            foreach (string period in Periods.GenerateSelectionList())
                Period.Items.Add(new ListItem(Periods.GetLocalizedPeriod(period), period));
            this.Period.Text = Resource.Properties["Period"];
        }
    }

	public override Dictionary<string, object> Properties
	{
		get
		{
			var properties = new Dictionary<string, object>();
            properties["Period"] = this.Period.Text;
            properties["GroupBy"] = GroupItems.SelectedValue;
            properties["GroupByLocalized"] = GroupItems.SelectedItem.Text;
		    return properties;
		}
	}
}
