<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditLastXXEvents.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditLastXXEvents" %>

<table border="0">
    <tr>
        <td>
            <asp:Label runat="server" ID="label" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_281 %>" Font-Bold="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox runat="server" ID="MaxEventsText" Width="50" />
        </td>
    </tr>
    <tr>
        <td style="padding-top: 10px">
            <asp:Label runat="server" ID="label1" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_263 %>" Font-Bold="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:DropDownList runat="server" ID="Period" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RangeValidator ID="EventsRangeValidator" runat="server" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_319 %>"
                Display="Dynamic" ControlToValidate="MaxEventsText" MinimumValue="1" MaximumValue="5000"
                Type="Integer" />
        </td>
    </tr>
</table>
