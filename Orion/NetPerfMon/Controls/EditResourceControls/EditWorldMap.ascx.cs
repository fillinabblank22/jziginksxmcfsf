﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI.Localizer;
using SolarWinds.Orion.Web.WorldMap;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditWorldMap : BaseResourceEditControl
{
    private static readonly Log _log = new Log();

    public const string ConfigParamLongitude = "Longitude";
    public const string ConfigParamLatitude = "Latitude";
    public const string ConfigParamZoom = "Zoom";
    public const string ConfigParamMapHeight = "MapHeight";
    public const string ConfigParamFilter = "Filter,{0}";
    public const string ConfigParamEnable = "Enable,{0}";
    public const string ConfigParamTileset = "Tileset";
    public const int ConfigDefaultHeight = 400;
    public const int ConfigDefaultZoom = 8;

    public const string FilterAttributeKey = "Entity";

    private Dictionary<string, WorldMapEntityData> supportedEntities;

    protected int ResourceWidth
    {
        get
        {
            if (Resource == null || Resource.View == null || Resource.View.ColumnCount < 1)
                return 550; // ideally, reporting would give us the freakin width.

            return Resource.Width;
        }
    }

    protected string Tileset
    {
        get
        {
            if (this.Resource.Properties.ContainsKey(ConfigParamTileset) == false)
                return string.Empty;

            return this.Resource.Properties[ConfigParamTileset];
        }
    }

    protected bool LocationControlsOk
    {
        get
        {
            double d;
            int i;

            if ((double.TryParse(longitudectrl.Text, out d) && d >= -180 && d <= 180) &&
                (double.TryParse(latitudectrl.Text, out d) && d >= -90 && d <= 90) &&
                (int.TryParse(zoomctrl.Text, out i) && i >= 1 && i <= 18))
            {
                return true;
            }

            return false;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        OrionInclude.CoreFile("leaflet-0.7.7/leaflet.js");
        if (WorldMapHelper.UseExternalMapQuest(HttpContext.Current))
        {
            OrionInclude.ExternalFile("https://www.mapquestapi.com/sdk/leaflet/v2.2/mq-map.js?key=" + SolarWinds.Orion.Core.Common.DALs.WorldMapPointsDAL.GetMapQuestKey(), PathFinder.ResourceType.JavaScript);
        }
        else
        {
            OrionInclude.CoreFile("mq-map.js");
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        supportedEntities = WorldMapDAL.AvailableEntities;
        if (Page.IsPostBack == false)
        {
            heightctrl.Text = Resource.Properties[ConfigParamMapHeight] ?? ConfigDefaultHeight.ToString(CultureInfo.InvariantCulture);
            zoomctrl.Text = Resource.Properties[ConfigParamZoom] ?? "1";
            latitudectrl.Text = FromInvariantToCulture(Resource.Properties[ConfigParamLatitude]) ?? "0";
            longitudectrl.Text = FromInvariantToCulture(Resource.Properties[ConfigParamLongitude]) ?? "0";
          
            List<ListItem> dataBind=new List<ListItem>();
            foreach (var entityData in this.supportedEntities)
            {
                var entityInfo = entityData.Value;
                bool isEnableProperty = !Resource.Properties.ContainsKey(string.Format(ConfigParamEnable, entityInfo.EntityName)) ||
                                        Resource.Properties[string.Format(ConfigParamEnable, entityInfo.EntityName)].Equals(
                                            bool.TrueString,
                                            StringComparison.
                                                OrdinalIgnoreCase);

                var entityName = EntityHelper.GetEntityNamePlural(entityInfo.EntityName);
                this.SupportedEntities.Items.Add(new ListItem(entityName, entityInfo.EntityName)
                                                     {Selected = isEnableProperty});

                var item = new ListItem()
                                 {
                                     Text =string.Format(Resources.CoreWebContent.WEBDATA_VL0_64, entityName),
                                     Value = Resource.Properties.ContainsKey(string.Format(ConfigParamFilter, entityInfo.EntityName))
                                                 ? Resource.Properties[string.Format(ConfigParamFilter, entityInfo.EntityName)]
                                                 : string.Empty,
                                     Enabled = isEnableProperty
                                 };
                item.Attributes[FilterAttributeKey] = entityInfo.EntityName;
                dataBind.Add(item);
            }

            EntityFilters.DataSource = dataBind;
            EntityFilters.DataBind();
        }
        else
        {
            for (int i = 0; i < SupportedEntities.Items.Count; i++)
            {
                ((Panel) EntityFilters.Items[i].FindControl("FilterItem")).Style["display"] =
                    SupportedEntities.Items[i].Selected ? "block" : "none";
            }
         
           
        }

        base.OnLoad(e);
    }

    protected override void OnPreRender(EventArgs e)
    {
        if (LocationControlsOk)
        {
            locationhint.Visible = true;
        }
        else if (Page.IsPostBack)
        {
            locationrequiredhint.Visible = true;
        }

        base.OnPreRender(e);
    }

    private string FromCultureToInvariant(string val)
    {
        double d;

        if (val == null)
            return null;

        if (double.TryParse(val, out d) == false)
            return val;

        return d.ToString(CultureInfo.InvariantCulture);
    }

    private string FromInvariantToCulture(string val)
    {
        double d;

        if (val == null)
            return null;

        if (double.TryParse(val, NumberStyles.Float, CultureInfo.InvariantCulture, out d) == false)
            return val;

        return d.ToString(CultureInfo.CurrentCulture);
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            var properties = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase)
            {
                { ConfigParamLongitude, FromCultureToInvariant(longitudectrl.Text) },
                { ConfigParamLatitude, FromCultureToInvariant(latitudectrl.Text) },
                { ConfigParamMapHeight, heightctrl.Text },
                { ConfigParamZoom, zoomctrl.Text },
            };
            for (int i=0; i< SupportedEntities.Items.Count; i++)
            {
                var curentItem = SupportedEntities.Items[i];
                properties.Add(string.Format(ConfigParamEnable,curentItem.Value),curentItem.Selected.ToString());

                if (curentItem.Selected)
                {
                    var filter = (TextBox) EntityFilters.Items[i].FindControl("Filter");
                    properties.Add(string.Format(ConfigParamFilter, filter.Attributes[FilterAttributeKey]), filter.Text);
                }
            }

            return properties;
        }
    }

    protected void SWQLValidate(object source, ServerValidateEventArgs args)
    {
        if (string.IsNullOrWhiteSpace(args.Value))
        {
            args.IsValid = true;
            return;
        }
        var control = (BaseValidator) source;
        var entityName = control.Attributes[FilterAttributeKey];
        var entityData = supportedEntities[entityName];
        control.ErrorMessage = string.Format(Resources.CoreWebContent.WEBDATA_VL0_65, EntityHelper.GetEntityName(entityName), entityData.DefaultAlias);
        if (SupportedEntities.Items.FindByValue(entityName).Selected)
        {
            using (var proxy = SolarWinds.Orion.Web.InformationService.InformationServiceProxy.CreateV3())
            {
                try
                {
                    var query = string.Format("SELECT {0}.{1} from {2} {0} WHERE {3}", entityData.DefaultAlias,
                                              entityData.IdColumn, entityData.EntityName, args.Value);
                    proxy.Query(query);
                    args.IsValid = true;
                }
                catch (Exception)
                {
                    args.IsValid = false;
                }
            }
        }
    }

}
