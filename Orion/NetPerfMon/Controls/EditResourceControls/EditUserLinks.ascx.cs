﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditUserLinks : BaseResourceEditControl
{
	private static int maxLinks = 10;
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		if (Resource == null) return;

		// data source is a DataTable
		DataTable table = new DataTable();
		table.Columns.Add("ID");
		table.Columns.Add("Link");
		table.Columns.Add("URL");
		table.Columns.Add("NW", typeof(bool));  // open in new window

		for (int i = 1; i <= maxLinks; i++)
		{
			DataRow r = table.NewRow();

			r["ID"] = i.ToString();
			r["Link"] = Resource.Properties[String.Format("Link{0}", i)];
			r["URL"] = Resource.Properties[String.Format("URL{0}", i)];

			if (Resource.Properties["NewWindow" + i.ToString()] == "on") r["NW"] = true;
			else r["NW"] = false;

			table.Rows.Add(r);
		}

		Links.DataSource = table;
		Links.DataBind();
	}

	public override Dictionary<string, object> Properties
	{
		get 
		{
			Dictionary<string, object> properties = new Dictionary<string, object>();

			if (Resource == null) return properties;

			// store items status
			int i = 0;
			foreach (RepeaterItem item in Links.Items)
			{
				i++;
				if (((CheckBox)item.FindControl("NewWindow")).Checked) properties.Add("NewWindow" + i.ToString(), "on");
				else properties.Add("NewWindow" + i.ToString(), String.Empty);

				properties.Add(String.Format("Link{0}", i), ((TextBox)item.FindControl("TB_Name")).Text);
				properties.Add(String.Format("URL{0}", i), ((TextBox)item.FindControl("TB_URL")).Text);
			}

			return properties;
		}
	}
}
