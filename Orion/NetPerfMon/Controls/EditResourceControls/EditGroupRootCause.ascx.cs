﻿using System;
using System.Collections.Generic;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditGroupRootCause : SolarWinds.Orion.Web.UI.BaseResourceEditControl
{
	#region Override Members (SolarWinds.Orion.Web.UI.BaseResourceEditControl)

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		if (!this.IsPostBack)
		{
			ctrMembersCount.SelectedValue = this.Resource.Properties["MembersCount"] ?? "20";
		}
	}

	public override Dictionary<String, Object> Properties
	{
		get
		{
			return new Dictionary<String, Object> 
			{ 
				{ "MembersCount", ctrMembersCount.SelectedValue }
			};
		}
	}

	#endregion
}