<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditDependencies.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditDependencies" %>
<%@ Register TagPrefix="orion" TagName="AutoHide" Src="~/Orion/Controls/AutoHideControl.ascx" %>
<table cellspacing="0" cellspacing="0" style="width: 100%;">
    <tr>
        <td>
            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_296) %></b><br />
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox runat="server" ID="DepFilter" Width="330"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_297) %></b><br />
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox runat="server" ID="ParentFilter" Width="330"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_298) %></b><br />
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox runat="server" ID="ChildFilter" Width="330"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
        <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_299) %></p>

            <orion:CollapsePanel ID="CollapsePanel1" runat="server" Collapsed="true">
                <TitleTemplate>
                    <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_300) %></b></TitleTemplate>
                <BlockTemplate>
                    <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_01) %></p>
	                <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_23) %><br />
	                <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_03) %></b></p>
	                <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_04) %>
		                <table>
			                <tr>
				                <td>0&nbsp;=&nbsp;<b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_05) %></b></td>
			                </tr>
			                <tr>
				                <td>1&nbsp;=&nbsp;<b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_07) %></b></td>
                                
			                <tr>
				                <td>2&nbsp;=&nbsp;<b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_09) %></b></td>
			                <tr>
				                <td>3&nbsp;=&nbsp;<b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_11) %></b></td>
			                </tr>
		                </table>
	                </p>
	                <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_24) %><br />
	                <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_25) %></b></p>
	                <p></p>
                </BlockTemplate>
            </orion:CollapsePanel>
        </td>
    </tr>
    <tr>
        <td style="padding-top: 10px;">
            <orion:AutoHide runat="server" ID="autoHide" InLine="true" Description="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_309 %>" />
        </td>
    </tr>
</table>
