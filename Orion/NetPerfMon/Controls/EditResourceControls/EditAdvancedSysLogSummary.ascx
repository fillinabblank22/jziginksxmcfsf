<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditAdvancedSysLogSummary.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditAdvancedSysLogSummary" %>

<p style="margin-top: 0px;">
	<b><%= Resources.SyslogTrapsWebContent.WEBDATA_AK0_243 %></b><br/>
    <asp:DropDownList ID="ddlPeriod" runat="server">
        <asp:ListItem Text="<%$ Resources: SyslogTrapsWebContent,WEBDATA_AK0_233 %>" Value="Past Minute"/>
        <asp:ListItem Text="<%$ Resources: SyslogTrapsWebContent,WEBDATA_AK0_234 %>" Value="Past Hour" Selected="True"/>
        <asp:ListItem Text="<%$ Resources: SyslogTrapsWebContent,WEBDATA_AK0_235 %>" Value="Last 24 Hours"/>
        <asp:ListItem Text="<%$ Resources: SyslogTrapsWebContent,WEBDATA_AK0_236 %>" Value="Last 7 Days"/>
        <asp:ListItem Text="<%$ Resources: SyslogTrapsWebContent,WEBDATA_AK0_237 %>" Value="Last 30 Days"/>
    </asp:DropDownList>
</p>
<p>
	<b><%= Resources.SyslogTrapsWebContent.WEBDATA_AK0_244 %></b><br/>
	<asp:DropDownList ID="ddlOrderBy" runat="server">
	<asp:ListItem Text="<%$ Resources: SyslogTrapsWebContent,WEBDATA_AK0_246 %>" Value="syslogseverity asc" Selected="True"></asp:ListItem>
	<asp:ListItem Text="<%$ Resources: SyslogTrapsWebContent,WEBDATA_VB0_39 %>" Value="total desc"></asp:ListItem>
	</asp:DropDownList>
</p>