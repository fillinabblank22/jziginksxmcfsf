﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditBlankResource : BaseResourceEditControl
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
        if (Resource.Properties.ContainsKey("Height"))
            txtHeight.Text = Resource.Properties["Height"];
	}

	public override Dictionary<string, object> Properties
	{
		get 
		{
            Dictionary<string, object> properties = new Dictionary<string, object>();
		        properties.Add("Height", txtHeight.Text);
		        return properties;
		}
	}
}
