<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListOfEntityProperties.ascx.cs" Inherits="ListOfEntityProperties" %>
<orion:Include ID="Include2" runat="server" File="Forecasting.css" />

<asp:Panel ID="EntityColumns" runat="server">
            <a href="#" class="sw-expander-link"><img src="/NetPerfMon/images/Icon_Plus.gif"  /><b><%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBDATA_LF0_31, DisplayEntityName)) %></b></a>
	        <div class="sw-listOfEntity-div">
				<%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBDATA_LF0_30, DisplayEntityName)) %><br />
    	        <ul class="sw-listOfEntity-list"> 
				    <asp:Repeater runat="server" ID="thisEntityColumns">
				        <ItemTemplate>
				            <li><%# DefaultSanitizer.SanitizeHtml(Container.DataItem) %></li>
				        </ItemTemplate>
				    </asp:Repeater>
    			</ul>
			</div>
			<br /><br />
			</asp:Panel>