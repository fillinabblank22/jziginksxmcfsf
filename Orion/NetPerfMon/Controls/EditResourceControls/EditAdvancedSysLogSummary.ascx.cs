﻿using System;
using System.Collections.Generic;

using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditAdvancedSysLogSummary : BaseResourceEditControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ddlOrderBy.SelectedValue = Resource.Properties["order"];
            ddlPeriod.SelectedValue = Resource.Properties["Period"];
        }
    }

	public override Dictionary<string, object> Properties
	{
		get
		{
			var properties = new Dictionary<string, object>();
            properties["Period"] = ddlPeriod.SelectedValue;
            properties["order"] = ddlOrderBy.SelectedValue;
		    return properties;
		}
	}
}
