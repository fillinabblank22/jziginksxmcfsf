﻿using System;
using SolarWinds.Orion.Web.UI;
using System.Collections.Generic;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditCPUsByPercentLoad : BaseResourceEditControl
{
	private Dictionary<string, object> _properties = new Dictionary<string, object>();

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		var topXX = Resource.Properties["TopXX"];
		if (!string.IsNullOrEmpty(topXX))
		{
			TopXX.Text = topXX;
		}
		else
		{
			TopXX.Text = "20";
		}
	}

	public override Dictionary<string, object> Properties
	{
		get
		{
			_properties.Add("TopXX", TopXX.Text);
			_properties.Add("AutoHide", AutoHide.AutoHideValue);
			return _properties;
		}
	}
}