﻿using System;
using System.Collections.Generic;
using System.Globalization;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditNotesHistory : BaseResourceEditControl
{
    private const int DefaultNumberOfNotes = 5;

    public override Dictionary<string, object> Properties
    {
        get
        {
            var properties = new Dictionary<string, object>();
            try
            {
                Int32.Parse(NumberOfNotesText.Text);
                properties.Add("MaxRecords", NumberOfNotesText.Text);
            }
            catch
            {
                properties.Add("MaxRecords", DefaultNumberOfNotes.ToString(CultureInfo.InvariantCulture));
            }
            return properties;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        int numberOfNotes;
        if (!Int32.TryParse(Resource.Properties["MaxRecords"], out numberOfNotes))
        {
            Resource.Properties["MaxRecords"] = "5";
        }
        NumberOfNotesText.Text = Resource.Properties["MaxRecords"];
    }
}