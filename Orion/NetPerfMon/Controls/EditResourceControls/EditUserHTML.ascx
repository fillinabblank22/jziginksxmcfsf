<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditUserHTML.ascx.cs"
    Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditUserHTML" %>
<div runat="server" id="warnMessage" style="color: red;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_253) %></div>
<p>
    <asp:TextBox runat="server" ID="txtCustomHTML" Rows="6" Columns="60" TextMode="MultiLine" />
</p>
<p>
	<asp:CheckBox runat="server" ID="chbxCustomHTML" style="position:absolute"/>
	<h4 style="margin: 15px 0 6px 25px; padding: 0;position:relative"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB2_2) %></h4>
</p>
