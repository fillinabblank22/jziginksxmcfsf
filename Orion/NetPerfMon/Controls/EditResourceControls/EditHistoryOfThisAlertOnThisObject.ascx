<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditHistoryOfThisAlertOnThisObject.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditHistoryOfThisAlertOnThisObject" %>
<table border="0">
    <tr>
        <td>
            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_PSR_4) %></b>
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox runat="server" ID="RowsPerPage" ></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="padding-top: 10px">
            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_263) %></b><br/>
        </td>
    </tr>
    <tr>
        <td>
            <asp:DropDownList runat="server" ID="Period" />
        </td>
    </tr>
</table>





