<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditAllAlertsThisObjectCanTrigger.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditAllAlertsThisObjectCanTrigger" %>

<style>
    input[type="radio"]
    {
        margin-top: 2px;
        vertical-align: top;
    }
</style>

<p>
    <b>
        <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_254 %>" ID="labelEnableRelatedAlerts" />
    </b>

    <br />

    <asp:RadioButtonList ID="radioButtonEnableRelatedAlerts" runat="server">
        <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_255 %>" Value="True" Selected="True" />
        <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_256 %>" Value="False" />
    </asp:RadioButtonList>
</p>