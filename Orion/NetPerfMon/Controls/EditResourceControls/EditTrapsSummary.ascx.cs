﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditTrapsSummary : BaseResourceEditControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (this.Resource != null)
        {

            if (!String.IsNullOrEmpty(this.Resource.Properties["Period"]))
            {
                this.periodEditor.PeriodName = this.Resource.Properties["Period"];
            }

            if (!String.IsNullOrEmpty(this.Resource.Properties["GroupBy"]))
            {
                this.groupBy.SelectedValue = this.Resource.Properties["GroupBy"];
            }
        }
    }

	public override Dictionary<string, object> Properties
	{
		get
		{
			Dictionary<string, object> properties = new Dictionary<string, object>();
            properties["Period"] = this.periodEditor.PeriodName;
            properties["GroupBy"] = this.groupBy.SelectedValue;
            
			return properties;
		}
	}
}
