﻿using System;
using System.Collections.Generic;

using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditAdvancedSysLogCounts : BaseResourceEditControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ddlPeriod.SelectedValue = Resource.Properties["timee"];
        }
    }

	public override Dictionary<string, object> Properties
	{
		get
		{
			var properties = new Dictionary<string, object>();
            properties["timee"] = ddlPeriod.SelectedValue;
		    return properties;
		}
	}
}
