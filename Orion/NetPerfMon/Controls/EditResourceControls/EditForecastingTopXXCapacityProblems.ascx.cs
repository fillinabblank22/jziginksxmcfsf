﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.UI;
using System.Data;
public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditForecastingThresholds_EditForecastingTopXXCapacityProblems : BaseResourceEditControl
{
    private const int DefaultNumberOfEvents = 10;
    string filter = "";
    DataTable results;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        
        filter = Resource.Properties["Filter"];
        FilterText.Text = filter;

        int maxRecords = DefaultNumberOfEvents;
        
        if (!this.IsPostBack)
        {
            string swql = @"SELECT Id, Name, DisplayName FROM Orion.ForecastMetrics (nolock=true)";
            using (var swis = SolarWinds.Orion.Web.InformationService.InformationServiceProxy.CreateV3())
            {
                results = swis.Query(swql);
            }

            this.metrics.DataSource = results;
            this.metrics.DataBind();

            if (Int32.TryParse(Resource.Properties["MaxRecords"], out maxRecords))
                MaxEventsText.Text = maxRecords.ToString();
            else
                MaxEventsText.Text = DefaultNumberOfEvents.ToString();
        }
    }
        
    public List<string> GetSelectedTypes()
    {
        List<string> ilist = new List<string>();

        foreach (RepeaterItem rpItem in metrics.Items)
        {
            CheckBox chkbx = rpItem.FindControl("metric") as CheckBox;
            if (chkbx != null && !chkbx.Checked)
            {
                ilist.Add(chkbx.Attributes["MetricName"].ToString());
            }
        }

        return ilist;
    }
    
    public List<string> SelectedMetrics
    {
        get;
        set;
    }

    protected void Metrics_ItemDataBound(Object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (SelectedMetrics == null)
            {
                if (!string.IsNullOrEmpty(Resource.Properties["DisabledMetrics"]))
                {
                    SelectedMetrics = new List<string>(Resource.Properties["DisabledMetrics"].Split(','));
                }
            }

            CheckBox cb = (CheckBox)e.Item.FindControl("metric");
            string auditType = ((DataRowView)e.Item.DataItem)["Name"].ToString();
            cb.Attributes.Add("MetricName", auditType);
            cb.Text = ((DataRowView)e.Item.DataItem)["DisplayName"].ToString();

            if (SelectedMetrics != null)
                cb.Checked = !SelectedMetrics.Contains(auditType);
            else
                cb.Checked = true;
        }
    }

    protected void Metrics_ListOfEntityProperties_DataBound(Object sender, RepeaterItemEventArgs e)
    {
        ListOfEntityProperties lep = (ListOfEntityProperties)e.Item.FindControl("listOfEntity");
        lep.EntityName = ((DataRowView)e.Item.DataItem)[0].ToString();
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            var properties = new Dictionary<string, object>();
            try
            {
                Int32.Parse(MaxEventsText.Text);
                properties.Add("MaxRecords", MaxEventsText.Text);
            }
            catch
            {
                properties.Add("MaxRecords", DefaultNumberOfEvents.ToString());
            }

            properties.Add("Filter", FilterText.Text);

            var selected = GetSelectedTypes();
            if (selected != null && selected.Count > 0)
                properties.Add("DisabledMetrics", string.Join(",", selected.ToArray()));
            else                
                properties.Add("DisabledMetrics", "");
            
            return properties;
        }
    }

    protected string ReturnUrl
    {
        get { return ReferrerRedirectorBase.GetReturnUrl(); }
    }
}