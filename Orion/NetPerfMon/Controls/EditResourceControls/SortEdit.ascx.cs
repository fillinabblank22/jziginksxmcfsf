﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_SortEdit : BaseResourceEditControl
{
	private Dictionary<string, object> _properties = new Dictionary<string, object>();

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		_properties.Add("Sort", string.Empty);

		var sort = Resource.Properties["Sort"];
		var sortFields = Resource.Properties["SortFields"];

		if (String.IsNullOrEmpty(sortFields))
		{
			sortFields = string.Format(@"Interfaces.Index:{0},Interfaces.Status DESC:{1},Interfaces.Caption:{2}, 
                                        Interfaces.TypeDescription:{3},Interfaces.OutPercentUtil DESC:{4},
                                        Interfaces.InPercentUtil DESC:{5},(Interfaces.OutPercentUtil+Interfaces.InPercentUtil) DESC:{6}", 
                                        Resources.CoreWebContent.WEBCODE_AK0_105, Resources.CoreWebContent.WEBCODE_AK0_106, Resources.CoreWebContent.WEBCODE_AK0_107, 
                                        Resources.CoreWebContent.WEBCODE_AK0_108, Resources.CoreWebContent.WEBCODE_AK0_109, Resources.CoreWebContent.WEBCODE_AK0_110, 
                                        Resources.CoreWebContent.WEBCODE_AK0_111);
			sort = "Interfaces.Index";
		}

		string[] temp = sortFields.Split(',');
		foreach (string s in temp)
		{
			string[] result = s.Trim().Split(':');
			if (result.Length == 2)
				SortItems.Items.Add(new ListItem(result[1], result[0]));
		}

		SortItems.SelectedValue = sort ?? "Interfaces.Index";
        if (System.Web.HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
        {
            SortItems.Attributes.Add("onchange", "javascript:SaveData('Sort', this.value);");
        }
	}

	public override Dictionary<string, object> Properties
	{
		get 
		{
			_properties["Sort"] = SortItems.SelectedValue;
			return _properties;
		}
	}
}
