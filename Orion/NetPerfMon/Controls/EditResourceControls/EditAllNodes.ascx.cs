﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

using Resources;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditAllNodes : BaseResourceEditControl
{

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!this.IsPostBack)
        {
            SetLoadTypes();

            LoadTypeNumberBox.Visible = true;
            string loadType = Resource.Properties["LoadType"] ?? "ShowAll";
            string loadNodesNumber = "100";

            if (!loadType.Equals("ShowAll"))
                loadNodesNumber = Resource.Properties["LoadNodeNumbers"] ?? "100";

            if (!string.IsNullOrEmpty(loadType) && !loadType.Equals(lbxLoadType.SelectedValue, StringComparison.OrdinalIgnoreCase))
                lbxLoadType.SelectedValue = loadType;
            else
                lbxLoadType.SelectedValue = "ShowAll";


            if (!string.IsNullOrEmpty(loadType))
            {
                if (loadType == "ShowAll")
                {
                    lbxLoadType.SelectedValue = "ShowAll";
                }
                else
                {
                    lbxLoadType.SelectedValue = "PartialLoading";
                    LoadTypeNumberBox.Visible = true;

                    if (!string.IsNullOrEmpty(loadNodesNumber))
                        txtLoadNodesNumber.Text = loadNodesNumber;
                    else
                        txtLoadNodesNumber.Text = "100";
                }
            }
        }
    }

    private void SetLoadTypes()
    {
        ListItem showAllItem = new ListItem(CoreWebContent.Resource_AllNodes_LoadingType_ShowAll, "ShowAll");
        ListItem partialLoadingItem = new ListItem(CoreWebContent.Resource_AllNodes_LoadingType_PartialLoading, "PartialLoading");

        AddListItem(lbxLoadType, showAllItem);
        AddListItem(lbxLoadType, partialLoadingItem);
    }

    private void AddListItem(ListBox listBox, ListItem listItem)
    {
        listBox.Items.Add(listItem);
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            properties.Add("LoadType", lbxLoadType.SelectedValue);
            properties.Add("LoadNodeNumbers", txtLoadNodesNumber.Text);
            
            foreach (var prop in originEdit.Properties)
            {
                properties.Add(prop.Key, prop.Value);
            }

            // We changed stuff so clear out the expanded tree node information
            return properties;
        }
    }

    public override string DefaultResourceSubTitle
    {
        get { return originEdit.DefaultResourceSubTitle; }
    }

    public override bool ShowSubTitleHintMessage
    {
        get
        {
            return true;
        }
    }

    public override string SubTitleHintMessage
    {
        get
        {
            return CoreWebContent.WEBCODE_AK0_104;
        }
    }
}