﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditCustomChart : BaseResourceEditControl
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		bool boolValue;
		showTrend.Checked = (Resource != null) ? (bool.TryParse(Resource.Properties["ShowTrend"], out boolValue) ? boolValue : true) : true;
	}

	public override Dictionary<string, object> Properties
	{
		get
		{
			Dictionary<string, object> properties = new Dictionary<string, object>();

			properties.Add("Period", Period.PeriodName);
			properties.Add("SampleSize", SampleSize.SampleSizeValue);
			properties.Add("ShowTrend", showTrend.Checked.ToString());

			return properties;
		}
	}
}
