﻿using System;
using SolarWinds.Orion.Web.UI;
using System.Collections.Generic;
using SolarWinds.Orion.Web.Charting;
using System.Web.UI.WebControls;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditLastxxMessages : BaseResourceEditControl
{
	private static int defaultNumberOfMessages = 25;
	private Dictionary<string, object> _properties = new Dictionary<string, object>();

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		_properties.Add("MaxMessages", string.Empty);
		_properties.Add("Period", string.Empty);
		_properties.Add("Filter", string.Empty);

        foreach (string period in Periods.GenerateSelectionList())
            Period.Items.Add(new ListItem(Periods.GetLocalizedPeriod(period), period));

		this.MaxRecordsText.Text = Resource.Properties["MaxMessages"];
		this.Period.Text = Resource.Properties["Period"];
		this.FilterText.Text = Resource.Properties["Filter"];

        if (System.Web.HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
        {
            Period.Attributes.Add("onchange", "javascript:SaveData('Period', this.value);");
            FilterText.Attributes.Add("onchange", "javascript:SaveData('Filter', this.value);");
            MaxRecordsText.Attributes.Add("onchange", "javascript:SaveData('MaxMessages', this.value);");
        }
	}

	public override Dictionary<string, object> Properties
	{
		get
		{
			int recCount;
			
			if (Int32.TryParse(this.MaxRecordsText.Text, out recCount))
				_properties["MaxMessages"] = recCount.ToString();
			else
				_properties["MaxMessages"] = defaultNumberOfMessages.ToString();

			_properties["Period"] = this.Period.Text;
			_properties["Filter"] = this.FilterText.Text;

			return _properties;
		}
	}
}