<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditTrapsSummary.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditTrapsSummary" %>
<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/PeriodControl.ascx" %>

<table width="auto" cellspacing="0" cellpadding="0">    
    <orion:EditPeriod runat="server" ID="periodEditor" PeriodLabel="<%$ Resources: SyslogTrapsWebContent, WEBDATA_TM0_2 %>" InLine="true"/>
    <tr>
        <td colspan="3"><b><%= Resources.SyslogTrapsWebContent.WEBDATA_TM0_3 %></b><br/>
        <asp:DropDownList runat="server" id="groupBy">
            <asp:ListItem Text="<%$ Resources: SyslogTrapsWebContent, WEBDATA_VB0_13 %>" Value="IPAddress" />
            <asp:ListItem Text="<%$ Resources: SyslogTrapsWebContent, WEBDATA_TM0_4 %>" Value="Hostname" />
            <asp:ListItem Text="<%$ Resources: SyslogTrapsWebContent, WEBDATA_TM0_5 %>" Value="TrapType" Selected="True" />
            <asp:ListItem Text="<%$ Resources: SyslogTrapsWebContent, WEBDATA_VB0_16 %>" Value="MachineType" />
        </asp:DropDownList></td>
    </tr>
</table>