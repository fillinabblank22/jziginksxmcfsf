﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

using Resources;

using SolarWinds.InformationService.Linq.Plugins.Core.Orion;
using SolarWinds.MapEngine;
using SolarWinds.MapEngine.Helpers;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Caching;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Core.Common.PackageManager;
using SolarWinds.Orion.Core.Common.Swis;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditNetworkMap : BaseResourceEditControl
{
    private const string ShowWirelessClientsPropertyKeyFormat = "ShowWirelessClients{0}";

    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    private SolarWinds.MapEngine.IEngine mapEngine;
    private readonly bool _isWirelessHeatmapsAllowed = PackageManager.InstanceWithCache.IsPackageInstalled("Orion.Wireless.Heatmaps");
    private CustomResourceContentBase _wlhmClientsFilter;

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            
            if (mapsListbox.SelectedItem != null || SwisFederationInfo.IsFederationEnabled)
            {
                properties["selectedMapId"] = mapsListbox.SelectedValue;
                properties["ShowColorLegend"] = ShowColorLegendOnMapCheckbox.Checked ? string.Empty : "0";
                properties["ShowDownloadButton"] = ShowDownloadButtonCheckbox.Checked ? string.Empty : "0";
                properties["UseCaching"] = UseCachingCheckbox.Checked ? "1" : string.Empty;
                properties["MapNameFormat"] = mapNameFormatsDropDown.SelectedValue;

                //we are in HLWMDetails('All Wireless Heat Map' resource)
                //where each map has its own map scale
                if (Resource.View != null && Resource.View.ViewType == "WLHMDetails")
                {
                    AddOrUpdateWlhmDetailMapScaleValue();
                }
                else
                {
                    //Normal Map/WLHM resource
                    properties["mapScale"] = scaleTextBox.Text;
                }

                if (_isWirelessHeatmapsAllowed)
                {
                    AddOrUpdateShowWirelessClientsValue(properties);
                }

            }
            return properties;
        }
    }

    private void AddOrUpdateWlhmDetailMapScaleValue()
    {
        string mapId = Path.GetFileNameWithoutExtension(Resource.Properties["selectedMapId"]);
        string mapScaleId = string.Format("MapScale{0}", mapId);
        if (!string.IsNullOrEmpty(mapId))
            Resource.Properties[mapScaleId] = scaleTextBox.Text;
    }

    private void AddOrUpdateShowWirelessClientsValue(Dictionary<string, object> properties)
    {
        string mapId = Path.GetFileNameWithoutExtension(mapsListbox.SelectedValue).ToUpperInvariant();
        if (Resource.View != null && Resource.View.ViewType == "WLHMDetails")
            mapId = Path.GetFileNameWithoutExtension(Resource.Properties["selectedMapId"]).ToUpperInvariant();

        string showClients = string.Format(ShowWirelessClientsPropertyKeyFormat, mapId);
        Resource.Properties[showClients] = ShowClientsCheckbox.Checked ? "True" : "False";
        properties[showClients] = Resource.Properties[showClients];
    }

    protected override void OnInit(EventArgs e)
    {
        string rootLocalPath = MapService.GetTempPath();
        string applicationLocalPath = MapService.GetMapStudioInstallPath();

        string swoisEndpoint = string.Format(System.Configuration.ConfigurationManager.AppSettings["SWOISv3.RemoteEndpoint"], "localhost");
        string serverAddress, swisPort, swisPostfix;
        MapService.ParseEndpointLocation(swoisEndpoint, out serverAddress, out swisPort, out swisPostfix);

        string selectedMapId = Resource.Properties["selectedMapId"];
        string mapId = Path.GetFileNameWithoutExtension(selectedMapId);

        string mapScaleId = string.Format("MapScale{0}", mapId);
        string scale = "100";
        //get map scale for HWHLDetails
        //so this is only for All Wireless Heat Maps resource
        if (Resource.View != null && Resource.View.ViewType == "WLHMDetails")
        {
            if (!string.IsNullOrEmpty(mapId)
                && !string.IsNullOrEmpty(Resource.Properties[mapScaleId]))
            {
                scaleTextBox.Text = Resource.Properties[mapScaleId];
            }
            else
            {
                scaleTextBox.Text = scale;
            }
        }
        else if (!string.IsNullOrEmpty(Resource.Properties["mapScale"]))
        {
            scaleTextBox.Text = Resource.Properties["mapScale"].Replace("%", "").Trim();
        }
        else
        {
            scaleTextBox.Text = scale;
        }

        ShowColorLegendOnMapCheckbox.Checked = string.IsNullOrEmpty(Resource.Properties["ShowColorLegend"]);
        ShowDownloadButtonCheckbox.Checked = string.IsNullOrEmpty(Resource.Properties["ShowDownloadButton"]);
        UseCachingCheckbox.Checked = !string.IsNullOrEmpty(Resource.Properties["UseCaching"]);

        string errorMessage = string.Empty;
        string userName = HttpContext.Current.Profile.UserName;
        var usernameCredentials = CredentialsHelper.GetOrionCertificateCredentials();
        mapEngine = MapEngineFactory.GetEngine(userName, usernameCredentials, serverAddress, swisPort, swisPostfix, 0, true);
        mapEngine.RootApplicationPath = applicationLocalPath;
        mapEngine.RootLocalPath = rootLocalPath;
        mapEngine.SetAlternativeMapWebPath();
      
        Dictionary<string, string> availableMaps = null;

        try
        {
            availableMaps = mapEngine.GetMapsList();
        }
        catch (Exception ex)
        {
            log.ErrorFormat("Error reading available maps. Exception message: {0}", ex.Message);
        }

        //fill formats ddl, only for groups (containers)
        if (!string.IsNullOrEmpty(this.NetObjectID) &&
            this.NetObjectID.StartsWith("C:", StringComparison.OrdinalIgnoreCase))
        {
            foreach (var keyValuePair in MapHelper.GetMapNameFormats())
            {
                mapNameFormatsDropDown.Items.Add(new ListItem(keyValuePair.Key, keyValuePair.Value));
            }

            mapNameFormatsDropDown.SelectedValue = Resource.Properties["MapNameFormat"];
        }
        else
        {
            mapNameFormatsPanel.Visible = false;
        }

        if (availableMaps != null)
        {
            int index = 0;
            foreach (var map in (from map in availableMaps orderby map.Value ascending select map))
            {
                string id = map.Key.ToString().Replace("Maps: ", string.Empty);
                string displayName = GetLocalizedProperty("MapName", Path.GetFileNameWithoutExtension(map.Value));
                mapsListbox.Items.Add(new ListItem(displayName, id + Path.GetExtension(map.Value)));
                if (id == Path.GetFileNameWithoutExtension(selectedMapId))
                {
                    mapsListbox.SelectedIndex = index;
                }
                index++;
            }
            if (mapsListbox.SelectedIndex == -1 && mapsListbox.Items.Count > 0)
            {
                mapsListbox.SelectedIndex = 0;
            }
        }

        // WirelessHeatMaps (WLHM)
        if (_isWirelessHeatmapsAllowed)
            InitWirelessHeatMapControls();

        base.OnInit(e);
    }
    
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (mapEngine != null)
        {
            mapEngine.Close();
            mapEngine = null;
        }
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }

    /// <summary>
    /// Handles all the steps of WLHM control setup
    /// </summary>
    protected void InitWirelessHeatMapControls()
    {
        // Disable map selection on WLHM Details View
        if (IsWlhmView())
        {
            mapSelectionPanel.Visible = false;
        }

        // Update state of the "Show Clients" checkbox
        string mapId = Path.GetFileNameWithoutExtension(Resource.Properties["selectedMapId"]);
        string showClients = string.Format(ShowWirelessClientsPropertyKeyFormat, mapId);
        
        if (!string.IsNullOrEmpty(Resource.Properties[showClients]))
        {
            bool isChecked = Convert.ToBoolean(Resource.Properties[showClients]);
            ShowClientsCheckbox.Checked = isChecked;
        }

        // Load user control for clients filtering
        LoadWlhmClientsFilter();

        // Show the custom controls only when WLHM is selected in map list
        UpdateWlhmControlsVisibility();
    }

    // ClientsFilter user controls needs to be loaded only under some conditions.
    // This should handle the loading, when it's needed.
    protected void LoadWlhmClientsFilter()
    {
        try
        {
            _wlhmClientsFilter = LoadControl("~/Orion/WirelessHeatmaps/Controls/ClientsFilter.ascx") as CustomResourceContentBase;
        }
        catch (HttpException ex)
        {
            log.Error("Failed to load ~/Orion/WirelessHeatmaps/Controls/ClientsFilter.ascx. Ex: " + ex.Message);
        }
        if (_wlhmClientsFilter != null)
        {
            _wlhmClientsFilter.Resource = Resource;
            customResourceContents.Controls.Add(_wlhmClientsFilter);
        }
    }

    // Changing map does cause a postback so we have a chance to
    // determine if the newly selected map is a WLHM or not.
    protected void mapsListbox_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (_isWirelessHeatmapsAllowed)
            UpdateWlhmControlsVisibility();
    }

    // When WLHM is selected, thee related controls should be shown and their
    // properties set. Hide them otherwise.
    private void UpdateWlhmControlsVisibility()
    {
        WirelessHeatMapOptions.Visible = IsWlhmSelected();

        if (_wlhmClientsFilter == null)
            return;

        if (!IsWlhmSelected())
        {
            HideWlhmClientsFilter();
            return;
        }

        // I need to get SelectedMapId from URL, because when user
        // opens multiple WLHM Details views simultaneously, the
        // value in db is being overriden.
        if (IsWlhmView())
        {
            int wlhmId;
            string netObjectString = Request.QueryString["NetObject"];
            int.TryParse(netObjectString.Split(':')[1], out wlhmId);

            using (var swis = InformationServiceProxy.CreateV3())
            {
                const string query = @"SELECT MapStudioFileID FROM Orion.WirelessHeatMap.Map WHERE MapId = @mapId";
                DataTable data = swis.Query(query, new Dictionary<string, object> { { "mapId", wlhmId } });
                if (data != null && data.Rows.Count != 0)
                {
                    ShowWlhmClientsFilter(data.Rows[0][0] + ".OrionMap");
                }
                else
                {
                    log.DebugFormat("Failed to get GUID of WLHM from NetObject {0}.", netObjectString);
                    HideWlhmClientsFilter();
                }
            }
        }
        else
        {
            ShowWlhmClientsFilter(mapsListbox.SelectedValue);
        }
    }

    private void HideWlhmClientsFilter()
    {
        _wlhmClientsFilter.Properties["SelectedMapId"] = string.Empty;
        _wlhmClientsFilter.Hide();
    }

    private void ShowWlhmClientsFilter(string selectedMapId)
    {
        _wlhmClientsFilter.Properties["SelectedMapId"] = selectedMapId;
        _wlhmClientsFilter.Show();
    }

    // Helper
    private bool IsWlhmSelected()
    {
        // If we are on WLHM Details View, the map list is not
        // visible because the WLHM is selected automatically.
        if (IsWlhmView())
            return true;

        // MapId is supposed to be in GUID.orionMap format
        // e.g. 3f539776-278e-42ad-b47c-54607eef6572.OrionMap
        // If the dot isn't there, it's most likely a sample map
        var mapId = mapsListbox.SelectedValue;
        if (mapId.IndexOf('.') == -1)
        {
            return false;
        }

        // Other map instances might be of various types, so
        // we have to check if the selected one got matching
        // WLHM record in database to determine the type.
        var mapGuid = mapId.Substring(0, mapId.IndexOf('.'));
        using (var swis = InformationServiceProxy.CreateV3())
        {
            const string query = @"SELECT MapID FROM Orion.WirelessHeatMap.Map WHERE MapStudioFileID = @mapGuid";
            DataTable data = swis.Query(query, new Dictionary<string, object> { { "mapGuid", mapGuid } });
            if (data != null && data.Rows.Count != 0)
                return true;
        }

        return false;
    }

    // Helper
    private bool IsWlhmView()
    {
        return !string.IsNullOrEmpty(NetObjectID) && NetObjectID.StartsWith("WLHM:", StringComparison.OrdinalIgnoreCase);
    }

}
