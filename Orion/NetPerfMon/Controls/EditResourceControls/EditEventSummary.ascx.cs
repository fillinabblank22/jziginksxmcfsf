﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditEventSummary : BaseResourceEditControl
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
        foreach (string period in Periods.GenerateSelectionList())
            Period.Items.Add(new ListItem(Periods.GetLocalizedPeriod(period), period));

		this.Period.Text = Resource.Properties["Period"];
	}

	public override Dictionary<string, object> Properties
	{
		get
		{
			Dictionary<string, object> properties = new Dictionary<string, object>();
			properties.Add("Period", this.Period.Text);

			return properties;
		}
	}
}
