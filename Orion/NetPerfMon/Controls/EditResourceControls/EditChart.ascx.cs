﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.UI;
using System.Text;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditChart : BaseResourceEditControl
{
    private static readonly Log _log = new Log();
    private ChartResourceSettings _chartResourceSettings;
    private ChartSettingsDAL _settingsDal;

    private IChartEditorSettings _editorSpecificSettings;

    protected bool IsChartInDynamicLoadingMode
    {
        get { return _chartResourceSettings != null && _chartResourceSettings.ChartSettings != null && _chartResourceSettings.ChartSettings.LoadingMode == LoadingModeEnum.DynamicLoading; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        _settingsDal = new ChartSettingsDAL();
        _chartResourceSettings = ChartResourceSettings.FromResource(Resource, _settingsDal);

        if (!String.IsNullOrEmpty(ChartSpecificEditorControl))
        {
            CreateChartSpecificEditControl(ChartSpecificEditorControl);
        }

        if (!string.IsNullOrEmpty(_chartResourceSettings.ChartName))
        {
        }

        if (!Page.IsPostBack)
        {
            var listItems = _chartResourceSettings.AvailableAlternateCharts.Select(pair => new ListItem(pair.Value, pair.Key));
            ListOfCharts.Items.AddRange(listItems.ToArray());

            if (!string.IsNullOrEmpty(_chartResourceSettings.ChartName))
            {
                ListOfCharts.SelectedValue = _chartResourceSettings.ChartName;
            }

            SampleSize.SelectedValue = _chartResourceSettings.SampleSize.ToString(CultureInfo.InvariantCulture);
            ChartTimeSpan.SelectedValue = _chartResourceSettings.ChartDateSpan.TotalDays.ToString(CultureInfo.InvariantCulture);
            ChartZoom.SelectedValue = _chartResourceSettings.ChartInitialZoom;

            ShowTop.SelectedValue = _chartResourceSettings.NumberOfSeriesToShow.ToString(CultureInfo.InvariantCulture);

            ChartTitle.Text = _chartResourceSettings.Title;
            ChartSubtitle.Text = _chartResourceSettings.SubTitle;

            CalculateTrendLine.Checked = _chartResourceSettings.CalculateTrendLine;
            CalculateSumLine.Checked = _chartResourceSettings.CalculateSum;
            Calculate95thPercentileLine.Checked = _chartResourceSettings.Calculate95thPercentile;
            AutoHide.Checked = _chartResourceSettings.AutoHide;
            ShowThreshold.Checked = _chartResourceSettings.ShowThreshold;

            var chartSettings = _chartResourceSettings.ChartSettings;

            if (_chartResourceSettings.ChangingChartTypeEnabled)
            {
                // Use the saved chart settings if we have them.  Otherwise use the default chart
                chartSettings = _chartResourceSettings.ChartSettings ??
                                    _chartResourceSettings.GetChartSettings(ListOfCharts.SelectedValue);
            }

            if (chartSettings != null)
            {
                CreateChartSpecificEditControl(chartSettings.EditorUserControl);
                UpdateUiBasedOnChart(chartSettings);
            }
            
        }
       
        if (!IsValidNumberOfPoints())
        {
            ErrorMessage.Visible = true;
            TimeSpanLabel.Attributes["class"] = "sw-validation-error";
            SampleSizeLabel.Attributes["class"] = "sw-validation-error";
        }

        this.PreRender += new EventHandler(Orion_NetPerfMon_Controls_EditResourceControls_EditChart_PreRender);
    }

    protected void Orion_NetPerfMon_Controls_EditResourceControls_EditChart_PreRender(object sender, EventArgs e)
    {
        if (SampleSize != null)
        {
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "TimePeriodInitialScript", GetInitializeScript(), true);
            ChartTimeSpan.Attributes.Add("onChange", "CheckTimePeriods()");
            SolarWinds.Orion.Web.OrionInclude.CoreFile("TimePeriodScript.js");
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            bool haveTitles = ChartTitle.Text.Length > 0 || ChartSubtitle.Text.Length > 0;

            var properties = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            if (_chartResourceSettings.ChangingChartTypeEnabled)
            {
                properties["ChartName"] = ListOfCharts.SelectedValue;
            }

            properties["SampleSize"] = SampleSize.SelectedValue;
            properties["ChartDateSpan"] = ChartTimeSpan.SelectedValue;
            properties["ChartInitialZoom"] = ChartZoom.SelectedValue;
            properties["NumberOfSeriesToShow"] = ShowTop.SelectedValue;
            properties["ShowTitle"] = haveTitles ? "1" : "0";
            properties["ChartTitle"] = ChartTitle.Text;
            properties["ChartSubTitle"] = ChartSubtitle.Text;

            properties["CalculateTrendLine"] = CalculateTrendLine.Checked ? "1" : "0";
            properties["CalculateSum"] = CalculateSumLine.Checked ? "1" : "0";
            properties["Calculate95thPercentile"] = Calculate95thPercentileLine.Checked ? "1" : "0";
            properties["AutoHide"] = AutoHide.Checked ? "1" : "0";
            properties["HideThreshold"] = ShowThreshold.Checked ? "0" : "1";

            if (_editorSpecificSettings != null)
                _editorSpecificSettings.SaveProperties(properties);

            return properties;
        }
    }

    protected void ListOfCharts_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        var chartSettings = _settingsDal.GetChartSettings(ListOfCharts.SelectedValue);

        CreateChartSpecificEditControl(chartSettings.EditorUserControl);

        UpdateUiBasedOnChart(chartSettings);
    }

    private void UpdateUiBasedOnChart(ChartSettings chartSettings)
    {
        CalculateSumLine.Visible = chartSettings.AllowSumLine;
        CalculateTrendLine.Visible = chartSettings.AllowTrendLine;
        Calculate95thPercentileLine.Visible = chartSettings.AllowPercentileLine;
        AutoHide.Visible = chartSettings.AllowAutoHide;
        ShowThreshold.Visible = chartSettings.AllowThreshold;
        SectionData.Visible = SectionListOfCharts.Visible = _chartResourceSettings.ChangingChartTypeEnabled;

        ZoomPanel.Visible = chartSettings.IsTimeSeriesChart;
        TimeSpanPanel.Visible = chartSettings.IsTimeSeriesChart;
        ShowTopPanel.Visible = chartSettings.IsTimeSeriesChart && chartSettings.SupportTopXSeries;
        SampleSizePanel.Visible = chartSettings.IsTimeSeriesChart;

        if (_editorSpecificSettings != null)
            _editorSpecificSettings.Initialize(chartSettings, Resource, NetObjectID);

        HideHeaderIfEmpty(SectionCalulatedSeries, CalculateSumLine, CalculateTrendLine, Calculate95thPercentileLine);
        HideHeaderIfEmpty(SectionDisplayBehavior, AutoHide);
        HideHeaderIfEmpty(SectionDataSeries, ShowTopPanel);
        HideHeaderIfEmpty(SectionTimePeriods, TimeSpanPanel, ZoomPanel, SampleSizePanel);

        SetSampleIntervalLabelByLoadingMode();
    }

   
    private void CreateChartSpecificEditControl(string editControlPath)
    {
        if (String.IsNullOrEmpty(editControlPath))
        {
            // Remove the control
            chartSpecificSettings.Controls.Clear();
            chartSpecificSettings.Visible = false;
            ChartSpecificEditorControl = editControlPath;
            return;
        }

        try
        {
            var editControl = LoadControl(editControlPath);
            editControl.ID = "ChartSpecificEditor";
            _editorSpecificSettings = editControl as IChartEditorSettings;

            chartSpecificSettings.Controls.Clear();
            chartSpecificSettings.Controls.Add(editControl);
            chartSpecificSettings.Visible = true;

            ChartSpecificEditorControl = editControlPath;

            var baseResourceEditControl = editControl as BaseResourceEditControl;
            if (baseResourceEditControl != null)
            {
                baseResourceEditControl.NetObjectID = NetObjectID ?? Resource.Properties["netobjectid"];
                baseResourceEditControl.Resource = Resource;
            }
        }
        catch (Exception ex)
        {
            _log.Error("Error while loading chart specific editor settings from " + editControlPath, ex);
        }
        
    }

    private string ChartSpecificEditorControl
    {
        get { return (ViewState["ChartSpecificEditor"] ?? String.Empty).ToString(); }
        set { ViewState["ChartSpecificEditor"] = value; }
    }

    private void HideHeaderIfEmpty(Control header, params Control[] children)
    {
        header.Visible = children.Any(x => x.Visible);
    }

    protected void ValidateNumberOfPoints(object source, ServerValidateEventArgs args)
    {
        args.IsValid = IsValidNumberOfPoints();
    }

    private bool IsValidNumberOfPoints()
    {
        return CheckTimePeriodValue(ChartTimeSpan.SelectedValue, SampleSize.SelectedValue);
    }

    private bool CheckTimePeriodValue(string ChartTimeSpanValue, string SampleSizeValue)
    {  

        int sampleSizeInMinutes = Int32.Parse(SampleSizeValue, CultureInfo.InvariantCulture);
        int daysToLoad = Int32.Parse(ChartTimeSpanValue, CultureInfo.InvariantCulture);

        const int minutesPerDay = 60 * 24;
        const int maxNumberOfPointsAllowed = 3000;

        int pointsToChart = (daysToLoad * minutesPerDay) / sampleSizeInMinutes;

        // For chart dynamic (lazy loading) mode we show all sample intervals.
        return ((pointsToChart <= maxNumberOfPointsAllowed) || IsChartInDynamicLoadingMode) && (pointsToChart >= 2);
    }

    private string GetInitializeScript()
    {
        if (ViewState["TimePeriodInitialScript"] == null)
        {
            StringBuilder initializescript = new StringBuilder();
            string ids = string.Empty;
            string values = string.Empty;
            GetTimePeriods(ref ids, ref values);
            initializescript.Append(ids);
            initializescript.Append(values);
            initializescript.Append(GetTimeRelations());
            initializescript.Append("$(function(){");
            initializescript.AppendFormat("window.timePeriodControl = document.getElementById('{0}');", ChartTimeSpan.ClientID);
            initializescript.AppendFormat("window.sampleTimePeriodControl = document.getElementById('{0}');", SampleSize.ClientID);
            initializescript.Append("$(document).ready(function() { if(typeof(CheckTimePeriods) != 'undefined') $(CheckTimePeriods); }); });");
            ViewState["TimePeriodInitialScript"] = initializescript.ToString();
        }

        return ViewState["TimePeriodInitialScript"].ToString();
    }

    private void GetTimePeriods(ref string IDs, ref string Values)
    {
        StringBuilder _ids = new StringBuilder("var sampleTimeIDS = [");
        StringBuilder _values = new StringBuilder("var sampleTimeValue = [");
        for (int i = 0; i < SampleSize.Items.Count; i++)
        {
            _ids.Append(string.Format("{0}'{1}'", (i == 0) ? "" : ",", SampleSize.Items[i].Value));
            _values.Append(string.Format("{0}'{1}'", (i == 0) ? "" : ",", SampleSize.Items[i].Text));
        }

        _ids.Append("];");
        _values.Append("];");
        IDs = _ids.ToString();
        Values = _values.ToString();
    }

    private string GetTimeRelations()
    {
        StringBuilder result = new StringBuilder("var sampleTimeRelations = [");
        for (int i = 0; i < ChartTimeSpan.Items.Count; i++)
        {
            StringBuilder samplesizeassigned = new StringBuilder("[");
            string timePeriod = ChartTimeSpan.Items[i].Value;
            string separator = string.Empty;
            foreach (ListItem sli in SampleSize.Items)
            {
                if (CheckTimePeriodValue(timePeriod, sli.Value.ToUpper()))
                {
                    samplesizeassigned.Append(string.Format("{0}'{1}'", separator, sli.Value));
					separator = ",";
                }
            }

            samplesizeassigned.Append("]");
			result.Append(string.Format("{0}{1}", ((i == 0) ? "" : ","), samplesizeassigned.ToString()));
        }

        result.Append("];");
        return result.ToString();
    }

    private void SetSampleIntervalLabelByLoadingMode()
    {
        // For chart dynamic (lazy loading) mode, rename  field 'sample interval'  to 'minimum sample interval' 
        SampleSizeLabel.InnerText = IsChartInDynamicLoadingMode
                                        ? Resources.CoreWebContent.WEBDATA_ZT0_1
                                        : Resources.CoreWebContent.WEBDATA_IB0_109;
    }
}