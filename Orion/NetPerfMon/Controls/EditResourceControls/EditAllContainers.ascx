<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditAllContainers.ascx.cs"
    Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditAllContainers" %>

<p>    
<asp:Label ID="ModeLabel" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_275 %>" Font-Bold="true"></asp:Label><br/>
<asp:DropDownList ID="ViewMode" runat="server" Width="183">
    <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_276 %>" Value="Root" />
    <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_277 %>" Value="Flat" />
    <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_278 %>" Value="Hierarchy" />
</asp:DropDownList>
</p>
<p> 
    <asp:Label ID="GroupingLevelLabel" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_VB0_5 %>" Font-Bold="true"></asp:Label><br/>
    <asp:DropDownList ID="GroupingDropDown" runat="server" Width="183">
        <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBCODE_SO0_9 %>" Value="none" />
        <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_VB0_14 %>" Value="status" />
    </asp:DropDownList>
</p>
<p> 
    <asp:Label Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_279 %>" Font-Bold="true" runat="server" /><br/>
    <asp:DropDownList ID="OrderingDropDown" runat="server" Width="183">
        <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_VB0_12 %>" Value="Name" />
        <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_VB0_14 %>" Value="Status" />
    </asp:DropDownList>
</p>
<p> 
    <asp:CheckBox runat="server" ID="RememberCollapseState" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_280 %>" />
</p>
