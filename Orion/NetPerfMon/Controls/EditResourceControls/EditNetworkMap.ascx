<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditNetworkMap.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditNetworkMap" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Caching" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Swis" %>
<%@ Register Src="~/Orion/Controls/HelpHint.ascx" TagPrefix="orion" TagName="HelpHint" %>

<orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include ID="Include2" runat="server" File="OrionCore.js" />
<orion:Include ID="Include3" runat="server" File="Admin/js/SearchField.js" />

<script language="javascript" type="text/javascript">
    // will be executed on page load
    $(function () {
        if ($("select[id$=mapNameFormatsDropDown]")) {
            // adds onChange event to mapNameFormatsDropDown
            $("select[id$=mapNameFormatsDropDown]").change(
                    function () {
                        // selected value is empty
                        if ($(this).val()) {
                            // disables mapsListbox
                            $("select[id$=mapsListbox]").attr('disabled', 'disabled');
                        } else {
                            // enables mapsListbox
                            $("select[id$=mapsListbox]").removeAttr('disabled');
                        }
                    }
                );

            // if map format is not selected 
            if ($("select[id$=mapNameFormatsDropDown]").val()) {
                // disables mapsListbox
                $("select[id$=mapsListbox]").attr('disabled', 'disabled');
            } else {
                // enables mapsListbox
                $("select[id$=mapsListbox]").removeAttr('disabled');
            };
        }
    });
</script>

<div class="BodyContent" style="width: 780px; padding-left: 0px;">
    <%--Advanced features toolip--%>
    <div style="float: right; width: 240px; background-color: #ecedee; padding: 10px; margin-left: 10px; border: 1px solid #ecedee;">
        <div style="font-weight: bold; font-size: 11px">
            <i><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_153) %></i> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_154) %>
        </div>
        <div style="padding-top: 6px; font-size: 11px">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_155) %>
        </div>
        <div style="padding-top: 6px; font-size: 11px">
            &#0187; <a href="/Orion/NetPerfMon/Resources/EditMapTooltips.aspx"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_156) %></a>
        </div>
    </div>
    <div>
        <%--Maps List--%>
        <asp:Panel ID="mapSelectionPanel" runat="server">
            <div style="font-weight: bold"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_157) %></div><%--Select Network Map--%>
            <asp:ListBox
                runat="server"
                ID="mapsListbox"
                Width="400"
                Height="350"
                AutoPostBack="False"
                OnSelectedIndexChanged="mapsListbox_OnSelectedIndexChanged"
                ></asp:ListBox>
            <asp:RequiredFieldValidator ID="selectedMapValidator" runat="server" InitialValue="" Display="Dynamic" ControlToValidate="mapsListbox" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_158 %>" />
            <div id="errorMessage" visible="false" runat="server"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_158) %></div><%--No map selected.--%>
        </asp:Panel>

        <%--Map Format--%>
        <asp:Panel ID="mapNameFormatsPanel" runat="server">
            <div style="font-weight: bold; padding-top: 18px"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_2) %></div><%--Map name format:--%>
            <asp:DropDownList ID="mapNameFormatsDropDown" runat="server" Width="400"></asp:DropDownList>
        </asp:Panel>

        <%--Scale--%>
        <div style="font-weight: bold; padding-top: 18px"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_159) %></div><%--Scale - 10% to 250%--%>
        <div>
            <asp:TextBox runat="server" ID="scaleTextBox"></asp:TextBox>
        </div>
        <asp:RequiredFieldValidator runat="server" ControlToValidate="scaleTextBox" Display="Dynamic" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_160 %>" />
        <asp:RangeValidator runat="server" ControlToValidate="scaleTextBox" Type="Integer" MaximumValue="250" MinimumValue="10" Display="Dynamic" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_160 %>" />
        
        <%--Options [checkboxes]--%>
        <div style="font-weight: bold; padding-top: 18px;">
            <asp:CheckBox ID="ShowColorLegendOnMapCheckbox" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PS0_76 %>" />
        </div>
        <div style="font-weight: bold; padding-top: 3px">
            <asp:CheckBox ID="ShowDownloadButtonCheckbox" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_161 %>" />
        </div>
        <div style="font-weight: bold; padding-top: 3px">
            <asp:CheckBox ID="UseCachingCheckbox" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_162 %>" />
        </div>

        <%--WLHM Options [checkboxes]--%>
        <div style="font-weight: bold; padding-top: 18px" id="WirelessHeatMapOptions" runat="server" Visible="False">
            <div><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LH0_4) %></div>
            <asp:CheckBox ID="ShowClientsCheckbox" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_LH0_5 %>" />
        </div>

        <%--Placeholder for dynamic controls, currently used for WirelessHeatMaps--%>
        <asp:PlaceHolder ID="customResourceContents" runat="server"></asp:PlaceHolder>

    </div>
</div>
