﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditAvailabilityOfEachNode : BaseResourceEditControl
{
    private Dictionary<string, object> _properties = new Dictionary<string, object>();

    protected override void OnInit(EventArgs e)
    {
        if (this.Resource != null)
        {
            filterEditor.FilterTextBox.Text = this.Resource.Properties["Filter"];


            if (!String.IsNullOrEmpty(this.Resource.Properties["Period"]))
            {
                if (this.Resource.Properties["Period"].Contains("~"))
                {
                    timeEditor.TimePeriodText = "Custom";
                    string[] periods = this.Resource.Properties["Period"].Split('~');
                    timeEditor.CustomPeriodBegin = DateTime.FromBinary(long.Parse(periods[0])).ToString(CultureInfo.CurrentCulture);
                    timeEditor.CustomPeriodEnd = DateTime.FromBinary(long.Parse(periods[1])).ToString(CultureInfo.CurrentCulture);
                }
                else
                {
                    timeEditor.TimePeriodText = this.Resource.Properties["Period"];
                }
            }
        }
        base.OnInit(e);
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            _properties.Add("Filter", filterEditor.FilterTextBox.Text);
            if (timeEditor.TimePeriodText != "Custom")
            {
                _properties.Add("Period", timeEditor.TimePeriodText);
            }
            else
            {
                try
                {
                    _properties.Add("Period",
                        Convert.ToDateTime(timeEditor.CustomPeriodBegin, CultureInfo.CurrentCulture).Ticks.ToString() +
                        "~" +
                        Convert.ToDateTime(timeEditor.CustomPeriodEnd, CultureInfo.CurrentCulture).Ticks.ToString());
                }
                catch
                {
                    _properties.Add("Period", DateTime.Today.Ticks.ToString() + "~" + (DateTime.Today.AddDays(1)).AddSeconds(-1).Ticks.ToString());
                }
            }

            return _properties;
        }
    }
}