﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditCustomChart.ascx.cs"
    Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditCustomChart" %>
<%@ Register TagPrefix="orion" TagName="EditSampleSize" Src="~/Orion/Controls/SampleSizeControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/PeriodControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="ShowTrend" Src="~/Orion/Controls/ShowTrendControl.ascx" %>

<table width="750">
    <tr>
        <td colspan="3">
            <orion:EditPeriod runat="server" ID="Period" InLine="true" />
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <orion:EditSampleSize runat="server" ID="SampleSize" InLine="true" />
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <orion:ShowTrend ID="showTrend" Checked="true" runat="server" InLine="true" />
        </td>
    </tr>
</table>
