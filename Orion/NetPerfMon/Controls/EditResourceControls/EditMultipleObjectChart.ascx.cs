﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web.UI;
using System.Collections.Specialized;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Charting;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditMultipleObjectChart : BaseResourceEditControl
{
	private string SessionGuid;
	private const string SessionID = "SessionID";
	private StringDictionary parameters = null;
	private const string DefaultEntityName = "Orion.Nodes";

    public string Entities
    {
        get
        {
            return ddl_ObjectTypes.SelectedItem.Text;
        }
    }

    protected void SelectObjects_Change(object source, EventArgs e)
    {
        bt_SelectObject.Visible = ManualSelect.Checked;

        ListEntities.SelectedEntities = String.Empty;
        ListEntities.EntityName = ddl_ObjectTypes.SelectedValue;
        ListEntities.ReloadList();
    }

	private bool LoadSessionData()
	{
		try
		{
			if (!String.IsNullOrEmpty(Request.QueryString[SessionID]) && Session[SessionGuid] != null)
			{
				parameters = (StringDictionary)Session[SessionGuid];

				if (parameters.ContainsKey(SessionID) && 
                    parameters[SessionID].Equals(UrlHelper.FromSafeBase64UrlParameter(Request.QueryString[SessionID]), StringComparison.OrdinalIgnoreCase))
				{
					return true;
				}

				parameters = null;
			}
		}
		catch { }
		return false;
	}
    private string GetLocalizedEntityDescription(string entityValue)
    {
        var manager = ResourceManagerRegistrar.Instance;
        var key = manager.CleanResxKey("Entity", entityValue);
        var result =manager.SearchAll(key, manager.GetAllResourceManagerIds());
        return !String.IsNullOrEmpty(result)?result:entityValue;
    }


    protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		ContainersMetadataDAL cmdal = new ContainersMetadataDAL();
        string NetObject = Request.QueryString["NetObject"];

        if (Request.QueryString[SessionID] != null)
        {
            SessionGuid = UrlHelper.FromSafeBase64UrlParameter(Request.QueryString[SessionID]);
        }
        else
        if (ViewState[SessionID] != null)
        {
            SessionGuid = ViewState[SessionID].ToString();
        }
        else
        {
            SessionGuid = Guid.NewGuid().ToString();
        }

		Dictionary<string, string> entities = new Dictionary<string,string>();
		// do not include entities that haven't supports ChartInfoPlugin
		foreach (KeyValuePair<string, string> item in cmdal.GetAvailableContainerMemberEntities(true))
		{
            if (ChartInfoFactoryPluginManager.ChartInfoPluginSupported(item.Key))
            {
                if (NetObject != null)
                {
                    if (NetObject.ToUpper().StartsWith("C:") ||
                        (!NetObject.ToUpper().StartsWith("C:") && (item.Key != "Orion.Groups")))
                    {
                        entities.Add(item.Key, GetLocalizedEntityDescription(item.Value));
                    }
                }
                else
                {
                    entities.Add(item.Key, GetLocalizedEntityDescription(item.Value));
                }
            }
		}

		ddl_ObjectTypes.DataSource = entities;
		ddl_ObjectTypes.DataTextField = "Value";
		ddl_ObjectTypes.DataValueField = "Key";
		ddl_ObjectTypes.DataBind();
		
		if (!IsPostBack)
		{
			ddl_ObjectTypes.SelectedValue = DefaultEntityName;
			LoadProperties(Resource.Properties);

			if (LoadSessionData())
				LoadProperties(parameters);
			
			ListEntities.ReloadList();
		}
        
        AutoSelect.Enabled = isAllowedAutoSelect(NetObject);
        AutoSelect.Checked = !ManualSelect.Checked;
        if (!AutoSelect.Enabled)
            ManualSelect.Checked = true;

        this.Period.AttachSampleSizeControl(this.SampleSize.SampleSizeListControl);
        
	}

    /// <summary>
    /// return true if this NetObject support autoselect
    /// </summary>
    /// <param name="NetObject"></param>
    /// <returns></returns>
    private bool isAllowedAutoSelect(string NetObject)
    {
        if (NetObject == null)
            return false;
 
        string[] str = NetObject.Split(new char[] { ':' });
        if (str.Length != 2)
            return false;
        
        List<string> allowedPrefixes = new List<string>(new string[] { "N", "I", "V", "C" });
        return  allowedPrefixes.Contains(str[0]);        
    }

	protected void ddl_ObjectTypes_SelectedIndexChanged(object sender, EventArgs e)
	{
		var data = new StringDictionary();

		data["Entities"] = ddl_ObjectTypes.SelectedItem.Text;
		//data["EntityName"] = ChartEntity.SelectedValue;
		data["EntityName"] = ddl_ObjectTypes.SelectedValue;
		Session[SessionGuid] = data;

		lc_ListCharts.EntityName = ddl_ObjectTypes.SelectedValue;
		lc_ListCharts.ReloadList();
		
		ListEntities.EntityName = ddl_ObjectTypes.SelectedValue;
		ListEntities.SelectedEntities = string.Empty;
		ListEntities.ReloadList();

		if (LoadSessionData())
			LoadProperties(parameters);
	}

	private void LoadProperties(StringDictionary properties)
	{
		if (properties != null)
		{
			ListEntities.EntityName = DefaultEntityName;
			lc_ListCharts.EntityName = DefaultEntityName;

			if (properties.ContainsKey("EntityName"))
			{
				ddl_ObjectTypes.SelectedValue = properties["EntityName"];
				ListEntities.EntityName = properties["EntityName"];
				lc_ListCharts.EntityName = properties["EntityName"];
			}

			if (properties.ContainsKey("ShowSum"))
				ShowSum.SelectedValue = properties["ShowSum"];

			if (properties.ContainsKey("SelectedEntities"))
			    ListEntities.SelectedEntities = properties["SelectedEntities"];

			if (properties.ContainsKey("ChartName"))
				lc_ListCharts.SelectedChart = properties["ChartName"];

			if (properties.ContainsKey("Period"))
				Period.PeriodName = properties["Period"];

			if (properties.ContainsKey("SampleSize"))
				SampleSize.SampleSizeValue = properties["SampleSize"];

			if (properties.ContainsKey("FilterEntities"))
				ListEntities.FilterSelection = Boolean.Parse(properties["FilterEntities"]);
			
			if (properties.ContainsKey("TopXX") && !String.IsNullOrEmpty(properties["TopXX"]))
			{
				TopXX.Enabled = true;
				TopXX.Value = properties["TopXX"];
			}

            if (properties.ContainsKey("ManualSelect"))
            {
                ManualSelect.Checked = Boolean.Parse(properties["ManualSelect"]);
                AutoSelect.Checked = !ManualSelect.Checked;
                bt_SelectObject.Visible = ManualSelect.Checked;
            }

            if (properties.ContainsKey("AutoHide"))
                AutoHide.AutoHideValue = properties["AutoHide"];
		}
	}

	private Dictionary<string, object> GetProperties()
	{
		var properties = new Dictionary<string, object>();

		properties.Add("EntityName", ddl_ObjectTypes.SelectedValue);
		properties.Add("Period", Period.PeriodName);
		properties.Add("SampleSize", SampleSize.SampleSizeValue);
		properties.Add("ShowSum", ShowSum.SelectedValue);
		properties.Add("SelectedEntities", ListEntities.SelectedEntities);
		properties.Add("FilterEntities", ListEntities.FilterSelection.ToString());
		properties.Add("TopXX", TopXX.Enabled ? TopXX.Value.ToString() : String.Empty);
		properties.Add("ChartName", lc_ListCharts.SelectedChart);
        properties.Add("ManualSelect", ManualSelect.Checked.ToString());
        properties.Add("AutoHide", AutoHide.AutoHideValue);

		return properties;
	}

	public override Dictionary<string, object> Properties
	{
		get { return GetProperties(); }
	}

	protected void SelectObjects_Click(object source, EventArgs e)
	{
		var data = new StringDictionary();

		data["Entities"] = ddl_ObjectTypes.SelectedItem.Text;
		data["EntityName"] = ddl_ObjectTypes.SelectedValue;
		data["Period"] = Period.PeriodName;
		data["SampleSize"] = SampleSize.SampleSizeValue;
		data["ShowSum"] = ShowSum.SelectedValue;
		data["SelectedEntities"] = ListEntities.SelectedEntities; 
		data["ChartName"] = lc_ListCharts.SelectedChart;
        data["FilterEntities"] = ListEntities.FilterSelection.ToString();
        data["TopXX"] = TopXX.Enabled ? TopXX.Value.ToString() : String.Empty;
        data["ManualSelect"] = ManualSelect.Checked.ToString();
        data["AutoHide"] = AutoHide.AutoHideValue;

		Session[SessionGuid] = data;

		string url = String.Format("/Orion/NetPerfMon/Controls/EditResourceControls/AddRemoveObjects/AddRemoveObjects.aspx{0}", GetUrlParameters());
        url = UrlHelper.UrlAppendUpdateParameter(url, SessionID, UrlHelper.ToSafeBase64UrlParameter(SessionGuid));
        url = UrlHelper.UrlAppendUpdateParameter(url, "ReturnTo", UrlHelper.ToSafeUrlParameter(Request.Url.ToString()));
		this.Response.Redirect(url);
	}

	private string GetUrlParameters()
	{
		var list = new List<string>();

		AddUrlParameter(list, "ResourceID");
		AddUrlParameter(list, "NetObject");
		AddUrlParameter(list, "ViewID");

		return (list.Count > 0) ? "?" + String.Join("&", list.ToArray()) : String.Empty;
	}

	private void AddUrlParameter(List<string> list, string name)
	{
		if (!String.IsNullOrEmpty(Request.QueryString[name]))
			list.Add(String.Format("{0}={1}", name, Request[name]));
	}
}