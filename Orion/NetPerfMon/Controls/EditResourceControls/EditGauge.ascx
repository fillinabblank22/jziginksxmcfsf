<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditGauge.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditGauge" EnableViewState="true" %>

<asp:ValidationSummary ID="ValidationSummary1" runat="server" />
<b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_312) %></b>
<br />
<span id="GaugesList">
    <asp:DropDownList ID="stylesList" runat="server" onchange="SelectCurrentGauge()">
    </asp:DropDownList>
</span>
<br />
<br />
<b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_313) %></b>
<br />
<asp:TextBox ID="scaleInput" runat="server" MaxLength="3"></asp:TextBox>
<asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="scaleInput"
    ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_316 %>" MinimumValue="30" MaximumValue="250"
    Type="Integer">*</asp:RangeValidator><asp:RequiredFieldValidator ID="RequiredFieldValidator1"
        runat="server" ControlToValidate="scaleInput" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_315 %>">*</asp:RequiredFieldValidator>
<asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="scaleInput"
    ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_314 %>" Operator="DataTypeCheck"
    Type="Integer">*</asp:CompareValidator>
<br />
<br />

<div id="GaugeStylesPanel" runat="server" style="background-color: White; 
    width: 840px;">
</div>

<script language="javascript" type="text/javascript">
    SelectCurrentGauge();
</script>