﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditDependencies : BaseResourceEditControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ParentFilter.Text = Resource.Properties["ParentFilter"];
        ChildFilter.Text = Resource.Properties["ChildFilter"];
        DepFilter.Text = Resource.Properties["DependencyFilter"];

        if (System.Web.HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
        {
            ParentFilter.Attributes.Add("onchange", "javascript:SaveData('ParentFilter', this.value);");
            ChildFilter.Attributes.Add("onchange", "javascript:SaveData('ChildFilter', this.value);");
            DepFilter.Attributes.Add("onchange", "javascript:SaveData('DependencyFilter', this.value);");
        }
    }

	private static string CleanBreakableString(string val)
	{
		if (!string.IsNullOrEmpty(val))
			return val.Replace("<wbr>", "").Replace("\u200B", "");
		return val;
	}

    public override Dictionary<string, object> Properties
    {
        get
        {
			var properties = new Dictionary<string, object> { { "ParentFilter", SqlFilterChecker.CleanFilter(CleanBreakableString(ParentFilter.Text)) }, 
            { "ChildFilter", SqlFilterChecker.CleanFilter(CleanBreakableString(ChildFilter.Text)) }, 
			{ "DependencyFilter", SqlFilterChecker.CleanFilter(CleanBreakableString(DepFilter.Text)) }};
            properties.Add("AutoHide", this.autoHide.AutoHideValue);

            return properties;
        }
    }

}
