﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Shared;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditContainersByCurrentStatus : BaseResourceEditControl
{
    protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

        this.tbFilter.Text = Resource.Properties["Filter"];
	}

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            properties.Add("SelectedStatus", GetSelectedStatus());
            properties.Add("Filter", this.tbFilter.Text);

            return properties;
        }
    }

    protected string GetSelectedStatus()
    {
        string selectedStatus = string.Empty;

        foreach (RepeaterItem item in statesRepeater.Items)
        {
            CheckBox chItem = (item.FindControl("stateCheckbox") as CheckBox);
            if (null != chItem && chItem.Checked)
            {
                selectedStatus = string.Format("{0}{1}{2}",
                                        selectedStatus,
                                        StatusInfo.AllStatuses.Values.Where(s => s.ShortDescription == chItem.Text).First().StatusId,
                                        ';');
            }
        }

        return selectedStatus;
    }

    protected void StatesRepeater_Init(object sender, EventArgs e)
    {
        statesRepeater.DataSource = GetStateFilters();
        statesRepeater.DataBind();
    }

    private DataTable GetStateFilters()
    {
        StatusInfo.Init(new StatusInfoProvider(), new SolarWinds.Logging.Log());
        DataTable table = new DataTable();
        table.Columns.Add("Name");
        table.Columns.Add("Value");

        string[] selectedStatuses = GetProperty("SelectedStatus", string.Empty).Split(';');
        if (selectedStatuses != null && selectedStatuses.Length > 0)
        {
            foreach (KeyValuePair<int, StatusInfo> pair in StatusInfo.AllStatuses)
            {
                table.Rows.Add(pair.Value.ShortDescription, selectedStatuses.Contains( ((int)pair.Value.StatusId).ToString() ));
            }
        }

        return table;
    }

}
