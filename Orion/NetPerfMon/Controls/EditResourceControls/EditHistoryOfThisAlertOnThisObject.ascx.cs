﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Charting;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditHistoryOfThisAlertOnThisObject : BaseResourceEditControl
{
    private const string _propertyRowsPerPage = "RowsPerPage";
	private const string _propertyPeriod = "Period";
    private const string _defaultRowsPerPage = "5";
	private const string _defaultPeriod = "Last 7 Days";

    protected override void OnInit(EventArgs e)
    {
        RowsPerPage.Text = Resource.Properties[_propertyRowsPerPage] ?? _defaultRowsPerPage;
		foreach (var period in Periods.GenerateSelectionList())
			Period.Items.Add(new ListItem(Periods.GetLocalizedPeriod(period), period));

		Period.SelectedValue = Resource.Properties[_propertyPeriod] ?? Periods.GetPeriod(_defaultPeriod).Name;
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            var properties = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase)
            {
                { _propertyRowsPerPage, RowsPerPage.Text },
				{ _propertyPeriod, Period.SelectedValue}
            };

            return properties;
        }
    }
}