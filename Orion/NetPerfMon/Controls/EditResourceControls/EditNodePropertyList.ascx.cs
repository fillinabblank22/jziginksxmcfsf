﻿using System;
using SolarWinds.Orion.Web.UI;
using System.Collections.Generic;
using SolarWinds.Orion.NPM.Web;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Web;
using System.Text;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditNodePropertyList : BaseResourceEditControl
{
	private Dictionary<string, object> _properties = new Dictionary<string, object>();

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		_properties.Add("PropertyList", string.Empty);

		if (!string.IsNullOrEmpty(NetObjectID))
		{
			Node node = new Node(NetObjectID);
			foreach (string column in node.ObjectProperties.Keys)
				displayProperties.Items.Add(new ListItem(FormatHelper.GetPropertyName(column), column));

			if (!String.IsNullOrEmpty(Resource.Properties["PropertyList"]))
			{
				foreach (string selectedColumn in Resource.Properties["PropertyList"].Split(new string[] { ", " }, StringSplitOptions.None))
				{
					ListItem item = displayProperties.Items.FindByValue(selectedColumn);
					if (item != null)
						item.Selected = true;
				}
			}
		}
	}

	public override Dictionary<string, object> Properties
	{
		get
		{
			StringBuilder selectedColumns = new StringBuilder();
			foreach (ListItem item in displayProperties.Items)
				if (item.Selected)
				{
					selectedColumns.Append(item.Value);
					selectedColumns.Append(", ");
				}

			_properties["PropertyList"] = selectedColumns.ToString();
			return _properties;
		}
	}
}
