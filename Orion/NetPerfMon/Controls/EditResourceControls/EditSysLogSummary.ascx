<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditSysLogSummary.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_EditSysLogSummary" %>

<p style="margin-top: 0px;">
    <b><%= Resources.SyslogTrapsWebContent.WEBDATA_AK0_263 %></b><br/>
    <asp:DropDownList runat="server" ID="Period" />
</p>
<p>
    <b><%= Resources.SyslogTrapsWebContent.WEBDATA_AK0_264 %></b><br/>
    <asp:DropDownList runat="server" ID="GroupItems" />
</p>