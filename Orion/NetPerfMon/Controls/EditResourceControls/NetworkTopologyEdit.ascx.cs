﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_NetworkTopologyEdit : BaseResourceEditControl
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		if (!string.IsNullOrEmpty(Resource.Properties["ShowItemsCount"]))
			maxCount.Text = Resource.Properties["ShowItemsCount"];
		else
			maxCount.Text = "5";

		NodeSrcFilter.Text = Resource.Properties["NodeSrcFilter"];
		NodeDestFilter.Text = Resource.Properties["NodeDestFilter"];
	}

	public override Dictionary<string, object> Properties
	{
		get
		{
			Dictionary<string, object> properties = new Dictionary<string, object>();
			properties.Add("ShowItemsCount", maxCount.Text);
			properties.Add("NodeSrcFilter", SqlFilterChecker.CleanFilter(NodeSrcFilter.Text));
			properties.Add("NodeDestFilter", SqlFilterChecker.CleanFilter(NodeDestFilter.Text));
			properties.Add("AutoHide", this.autoHide.AutoHideValue);

			return properties;
		}
	}
}
