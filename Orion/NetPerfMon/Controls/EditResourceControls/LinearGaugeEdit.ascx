<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LinearGaugeEdit.ascx.cs" Inherits="Orion_NetPerfMon_Controls_EditResourceControls_LinearGaugeEdit" EnableViewState="true" %>
<orion:Include runat="server" File="ig_webGauge.js" />
<orion:Include runat="server" File="ig_shared.js" />

<asp:ValidationSummary ID="ValidationSummary1" runat="server" />
<b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_312) %></b>
<br />
<span id="GaugesList">
    <asp:DropDownList ID="stylesList" runat="server" onchange="SelectCurrent(this); return false;">
    </asp:DropDownList>
</span>
<br />
<br />
<b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_313) %></b>
<br />
<div style="margin-top:10px;">
    <asp:TextBox ID="scaleInput" runat="server" MaxLength="3"></asp:TextBox>
    <orion:LocalizableButton runat="server" DisplayType="Small" LocalizedText="Refresh" ID = "Refresh" style="margin-left: 10px;" />
</div>
<asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="scaleInput"
    ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_316 %>" MinimumValue="30" MaximumValue="250"
    Type="Integer">*</asp:RangeValidator><asp:RequiredFieldValidator ID="RequiredFieldValidator1"
        runat="server" ControlToValidate="scaleInput" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_315 %>">*</asp:RequiredFieldValidator>
<asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="scaleInput"
    ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_314 %>" Operator="DataTypeCheck"
    Type="Integer">*</asp:CompareValidator>
<br />
<div id="GaugeStylesPanel" runat="server" style="background-color: White; 
    width: 840px; text-align:center;">  
</div>     

<script language="javascript" type="text/javascript">
    SelectStyle('<%= this.SelectedStyle %>');
</script>