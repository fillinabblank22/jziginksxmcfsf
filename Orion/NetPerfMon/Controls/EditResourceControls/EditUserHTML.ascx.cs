﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditUserHTML : BaseResourceEditControl
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		if (Resource.Properties.ContainsKey("HTML"))
			txtCustomHTML.Text = Resource.Properties["HTML"];
        if (Resource.Properties.ContainsKey("IsDynamicLoading"))
        {
            bool result;
            if (bool.TryParse(Resource.Properties["IsDynamicLoading"], out result))
            {
                chbxCustomHTML.Checked = result;
            }
        }

        txtCustomHTML.Enabled = this.Profile.AllowAdmin;
        warnMessage.Visible = !this.Profile.AllowAdmin;
    }

	public override Dictionary<string, object> Properties
	{
		get 
		{
			Dictionary<string, object> properties = new Dictionary<string, object>();
			properties.Add("HTML", txtCustomHTML.Text);
            properties.Add("IsDynamicLoading",chbxCustomHTML.Checked);
			return properties;
		}
	}
}
