﻿using System;
using System.Collections.Generic;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.Enums;
using SolarWinds.Orion.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditContainerEventList : BaseResourceEditControl
{
	private static int defaultMaxEventCount = 25;
	private static string defaultPeriod = "Last 12 Months";

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		// fill in period list
        foreach (string period in Periods.GenerateSelectionList())
            Period.Items.Add(new ListItem(Periods.GetLocalizedPeriod(period), period));

		// load values from resources

		this.MaxEventsText.Text = Resource.Properties["MaxEvents"] ?? defaultMaxEventCount.ToString();
		this.Period.Text = Resource.Properties["Period"] ?? defaultPeriod;

        if (System.Web.HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
        {
            Period.Attributes.Add("onchange", "javascript:SaveData('Period', this.value);");
            MaxEventsText.Attributes.Add("onchange", "javascript:SaveData('MaxEvents', this.value);");
        }
	}

	public override Dictionary<string, object> Properties
	{
		get
		{
			Dictionary<string, object> properties = new Dictionary<string, object>();
			try
			{
				Int32.Parse(this.MaxEventsText.Text);
				properties.Add("MaxEvents", this.MaxEventsText.Text);
			}
			catch
			{
				properties.Add("MaxEvents", defaultMaxEventCount.ToString());
                properties["OrionServersFilter"] = string.Empty;
			}

			properties.Add("Period", this.Period.Text);
			return properties;
		}
	}

    public override bool ShowSubTitle
    {
        get { return false; }
    }
}
