﻿using System;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using System.Collections.Generic;
using SolarWinds.NPM.Web.Gauge.V1;
using SolarWinds.Orion.Core.Common.i18n.Registrar;

public partial class Orion_NetPerfMon_Controls_EditResourceControls_EditGauge : BaseResourceEditControl
{
	private Dictionary<string, object> _properties = new Dictionary<string, object>();

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		_properties.Add("Scale", string.Empty);
		_properties.Add("Style", string.Empty);

		//Set scale input
		string scale = Resource.Properties["Scale"];
		if (string.IsNullOrEmpty(scale))
			scale = "100";
		scaleInput.Text = scale;
		//Set styles combobox
		List<string> styles = GaugeHelper.GetAllGaugeStyles(GaugeType.Radial);
	    foreach (string styleName in styles)
	    {
	        stylesList.Items.Add(new ListItem(GetStyleText(styleName),styleName));
	    }

		string style = Resource.Properties["Style"];
		if (string.IsNullOrEmpty(style))
            style = "Minimalist Dark";
		stylesList.SelectedValue = style;
		//Set sample gauges panel

		foreach (string gaugeStyle in styles)
		{
			Gauge gauge = new Gauge();
			gauge.GaugeStyle = gaugeStyle;
		    gauge.BottomTitle = GetStyleText(gaugeStyle);
			gauge.IsSample = true;
			GaugeStylesPanel.Controls.Add(gauge);
		}

        if (System.Web.HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
        {
            scaleInput.Attributes.Add("onchange", "javascript:SaveData('Scale', this.value);");
            //stylesList.Attributes.Add("onchange", "javascript:SaveData('Style', this.value);");
        }
	}

    /// <summary>
    /// Used to retrieve the style name appropriate to locale.
    /// </summary>
    /// <param name="style">style value</param>
    /// <returns>localized style name</returns>
    private string GetStyleText(string style)
    {
        return Resources.CoreWebContent.ResourceManager.GetString(
            ResourceManagerRegistrar.Instance.CleanResxKey("GaugeStyle", style)) ?? style;
    }

	public override Dictionary<string, object> Properties
	{
		get
		{
			_properties["Scale"] = scaleInput.Text;
			_properties["Style"] = stylesList.SelectedValue;
			
			return _properties;
		}
	}
}
