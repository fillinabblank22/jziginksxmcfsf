<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdvancedSyslogParser.ascx.cs"
    Inherits="Orion_NetPerfMon_Controls_AdvancedSyslogParser" %>
<%@ Register src="AdvancedSyslogPeriodSelect.ascx" tagname="AdvancedSyslogPeriodSelect" tagprefix="uc1" %>    

<orion:Include runat="server" File="legacyevents.css" /><%-- severity css --%>

<% if (Resource == null)
   { %>
<table width="100%" border="0" cellpadding="8" cellspacing="0">
    <tr class="HeaderBar">
        <td height="35" align="left" nowrap>
            <b><%= Resources.SyslogTrapsWebContent.WEBCODE_VB0_159 %><br>
                <font size="2"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_191 %>
                    <asp:Label runat="server" ID="lblPeriod"></asp:Label></font></b>
        </td>
        <asp:PlaceHolder runat="server" ID="plhCustom" Visible="false">
            <td>
                <tr class="HeaderBar">
                    <td height="35" nowrap>
                            <table>
                                <tr>
                                   <td style="  vertical-align:top;">
                                        <font size="2">
                                            <uc1:AdvancedSyslogPeriodSelect ID="ps" runat="server" />
                                         </font>
                                    </td>
                                    <td style="float:left; vertical-align:top;">
                                         <font size="2">
                                            <asp:Button runat="server" ID="btnGo" Text="<%$ Resources: SyslogTrapsWebContent, WEBDATA_VB0_192 %>" Style="height: 18px;
                                                                            color: black; background-color: #fff; border: 1px solid Gray; font-size: 10px; vertical-align: bottom;"
                                                    OnClick="btnGo_Click" />
                                         </font>
                                    </td>
                                </tr>     
                            </table>
                    </td>
        </asp:PlaceHolder>
        <td>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="right" nowrap>
                        <orion:LocalizableButtonLink ID="LocalizableButtonLink1" runat="server" NavigateUrl='<%# this.HelpLink %>' LocalizedText="CustomText" Text="<%$ Resources: SyslogTrapsWebContent, ResourcesAll_Help %>" DisplayType = "Resource" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<% }%>
<table width="100%" border="0" cellpadding="8" cellspacing="0">
    <tr class="HeaderBar">
        <font size="2"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_203 %> <a href="/Orion/netperfmon/AdvSyslogParser.aspx?timee=minute&top=<%=this.Top%>"
            target="<%# GetLinkTarget()%>"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_194 %><%= Resources.SyslogTrapsWebContent.String_Separator_1_VB0_59 %></a> <a href="/Orion/netperfmon/AdvSyslogParser.aspx?timee=hour&top=<%=this.Top%>"
                target="<%# GetLinkTarget()%>"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_195 %><%= Resources.SyslogTrapsWebContent.String_Separator_1_VB0_59 %></a> <a href="/Orion/netperfmon/AdvSyslogParser.aspx?timee=day&top=<%=this.Top%>"
                    target="<%# GetLinkTarget()%>"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_196 %><%= Resources.SyslogTrapsWebContent.String_Separator_1_VB0_59 %></a> <a href="/Orion/netperfmon/AdvSyslogParser.aspx?timee=week&top=<%=this.Top%>"
                        target="<%# GetLinkTarget()%>"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_197 %><%= Resources.SyslogTrapsWebContent.String_Separator_1_VB0_59 %></a> <a href="/Orion/netperfmon/AdvSyslogParser.aspx?timee=month&top=<%=this.Top%>"
                            target="<%# GetLinkTarget()%>"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_198 %><%= Resources.SyslogTrapsWebContent.String_Separator_1_VB0_59 %></a> <a href="/Orion/netperfmon/AdvSyslogParser.aspx?timee=custom&top=<%=this.Top%>"
                                target="<%# GetLinkTarget()%>"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_199 %></a> </font>
    </tr>
</table>
<div style="overflow-y:auto;overflow-x:auto" runat="server" id="scrollDiv" >
    <table border="0" cellspacing="1" width="100%">
        <tr>
            <asp:Repeater runat="server" ID="mainRepeater">
                <ItemTemplate>
                    <td>
                        <table border="1" cellspacing="1" width="100%" style="white-space: nowrap !important">
                            <tr>
                                <td align="center" colspan="3">
                                    <b><font size="1">
                                        <%# GetLink("1", Resources.SyslogTrapsWebContent.WEBDATA_AK0_155, string.Format(@"<a href=""/Orion/NetPerfMon/AdvSysLogWho.aspx?timee={0}&severity={1}"" target=""_blank"">", this.Timee, Container.DataItem.ToString()))%>
                                    </font></b></font>
                                </td>
                                <td align="center" width="100%" style="white-space: nowrap !important">
                                    <b><font size="1">
                                        <%# GetLink("1", GetSeverityType(Container.DataItem), string.Format(@"<a href=""/Orion/NetPerfMon/AdvSysLogWho.aspx?timee={0}&severity={1}"" target=""_blank"">", this.Timee, Container.DataItem.ToString()))%>
                                    </font></b>
                                </td>
                            </tr>
                            <asp:Repeater runat="server" ID="MessageRepeater">
                                <ItemTemplate>
                                    <tr style="white-space: nowrap !important">
                                        <td class="Property" width="70%" style="white-space: nowrap !important">
                                            <%# GetNodeLink(Eval("NodeID").ToString(), Eval("Caption").ToString()) %>
                                        </td>
                                        <td class="Property" style="white-space: nowrap !important">
                                            <%# GetLink(Eval("NodeID").ToString(), Resources.SyslogTrapsWebContent.WEBCODE_VB0_170, @"<a href=""/Orion/Netperfmon/Syslog.aspx?Period=" + this.qPeriod + @"&IPAddress=" + Eval("IPAddress").ToString().Trim() + @""" target=""_blank"">")%>
                                        </td>
                                        <td class="Property" style="white-space: nowrap !important">
                                            <%# GetLink(Eval("NodeID").ToString(), Resources.SyslogTrapsWebContent.WEBCODE_VB0_171, @"<a href=""/Orion/Netperfmon/Syslog.aspx?Period=" + this.qPeriod + "&IPAddress=" + Eval("IPAddress").ToString().Trim() + @""" target=""_blank"">")%>
                                        </td>
                                        <td class="Severity<%# Eval("syslogseverity")%>" align="center">
                                            <%# GetLink(Eval("NodeID").ToString(), Eval("Total").ToString(), @"<a href=""/Orion/Netperfmon/Syslog.aspx?NetObject=N:" + Eval("NodeID").ToString() + "&Period=" + this.qPeriod + "&Severity=" + Eval("syslogseverity").ToString() + @""" target=""_blank"">")%>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table></td>
                                </FooterTemplate>
                            </asp:Repeater>
                            <%# Container.DataItem.ToString() == "3" ? @"</tr><tr><td COLSPAN=4><hr color=""#0000FF""></td></tr><tr>" : ""%>
                </ItemTemplate>
            </asp:Repeater>
        </tr>
    </table>
</div>
