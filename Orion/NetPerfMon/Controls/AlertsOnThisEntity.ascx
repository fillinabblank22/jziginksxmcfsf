﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlertsOnThisEntity.ascx.cs" Inherits="Orion_NetPerfMon_Controls_AlertsOnThisEntity" %>
<%@ Register TagPrefix="orion" TagName="SortablePageableTable" Src="~/Orion/NetPerfMon/Controls/SortablePageableTable.ascx" %>

<style type="text/css">
    /*For own grid rendering*/
    .activeAlerts-rowInfo {
        color: #333333;
    }
    .activeAlerts-rowWarning {
        color: #FCA01D;
    }
    .activeAlerts-rowError {
        color: #da3838;
    }
    .activeAlerts-rowSerious {
        color: #da3838;
    }
    .activeAlerts-rowNotice {
        color: #6b98ff;
    }
</style>
<orion:SortablePageableTable ID="SortablePageableTable" runat="server" />

<input type="hidden" id="OrderBy-<%= ResourceID %>" />

<script type="text/javascript">
    $(function () {
        var federationEnabled = '<%=FederationEnabled%>'.toUpperCase() === 'TRUE';
        var linkTarget = federationEnabled ? 'target="_blank"' : '';

        SW.Core.Resources.ActiveAlerts.SortablePageableTable.initialize(
        {
            uniqueId: '<%= ResourceID %>',
            pageableDataTableProvider: function(pageIndex, pageSize, onSuccess, onError) {
                var uniqueId = '<%= ResourceID %>';
                var currentOrderBy = $('#OrderBy-' + uniqueId).val();
                SW.Core.Services.callController("/api/ActiveAlertsOnThisEntity/GetActiveAlerts",
                {
                    CurrentPageIndex: pageIndex,
                    PageSize: pageSize,
                    OrderByClause: currentOrderBy,
                    LimitationIds: [<%= string.Join(",", Limitation.GetCurrentListOfLimitationIDs()) %>],
                    ShowAcknowledgedAlerts: '<%= ShowAcknowledgedAlerts %>',
                    RelatedNodeEntityUri: '<%= RelatedNodeEntityUri %>',
                    RelatedNodeId: <%= RelatedNodeId %>,
                    TriggeringObjectEntityNames: [<%= string.Join(", ", TriggeringObjectEntityNames.Select(n => "'" + n + "'")) %>],
                    TriggeringObjectEntityUris: [<%= string.Join(", ", TriggeringObjectEntityUris.Select(n => "'" + n + "'")) %>],
                    FederationEnabled: federationEnabled
                }, function(result) {
                    onSuccess(result);
                    var $resourceWrapperTitle = $("div.ResourceWrapper[resourceid=<%= ResourceID %>] h1:first");
                    if ($resourceWrapperTitle.find("span").length == 0) {
                        $resourceWrapperTitle.append(SW.Core.String.Format("<span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_ActiveAlerts_TitleCount) %></span>", result.TotalRows));
                    } else {
                        $resourceWrapperTitle.find("span").text(SW.Core.String.Format("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_ActiveAlerts_TitleCount) %>", result.TotalRows));
                    }
                }, function(errorResult) {
                    onError(errorResult);
                });
            },
            initialPage: 0,
            rowsPerPage: '<%= InitialRowsPerPage %>',
            allowSort: true,
            getRowClass: function(row) {
                if (row[8] === 0) {
                    return "activeAlerts-rowInfo";
                } else if (row[8] == 1) {
                    return "activeAlerts-rowWarning";
                } else if (row[8] == 2) {
                    return "activeAlerts-rowError";
                } else if (row[8] == 3) {
                    return "activeAlerts-rowSerious";
                } else if (row[8] == 4) {
                    return "activeAlerts-rowNotice";
                }
                return null;
            },
            columnSettings: {
                "AlertName": {
                    caption: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_ActiveAlerts_AlertName) %>",
                    isHtml: true,
                    formatter: function(cellValue, rowArray) {
                        var severityImage = "InformationalAlert";
                        if (rowArray[8] === 0) {
                            severityImage = "InformationalAlert";
                        } else if (rowArray[8] == 1) {
                            severityImage = "Warning";
                        } else if (rowArray[8] == 2) {
                            severityImage = "Critical";
                        } else if (rowArray[8] == 3) {
                            severityImage = "Serious";
                        } else if (rowArray[8] == 4) {
                            severityImage = "Notice";
                        }

                        var federationLink = federationEnabled ? SW.Core.String.Format('/Server/{0}', rowArray[12]) : "";
                        var alertName = cellValue;
                        if (rowArray[0] > 0)
                            alertName = SW.Core.String.Format('<a href="{3}/Orion/NetPerfMon/ActiveAlertDetails.aspx?NetObject=AAT:{0}" {2}>{1}</a>', rowArray[0], cellValue, linkTarget, federationLink);

                        return SW.Core.String.Format('<img src="/Orion/images/ActiveAlerts/{0}.png" />&nbsp;<span style="vertical-align: super;" >{1}</span>',
                            severityImage,
                            alertName);
                    }
                },
                "AlertMessage": {
                    caption: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_ActiveAlerts_Message) %>",
                    isHtml: true
                },
                "TriggeringObject": {
                    caption: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_ActiveAlerts_TriggeringObject) %>",
                    isHtml: true,
                    formatter: function(cellValue, rowArray) {
                        return SW.Core.String.Format('<a href="{0}" {2}>{1}</a>',
                            rowArray[4].length != 0 ? rowArray[4] : location.href,
                            cellValue, linkTarget);
                    }
                },
				"RelatedNode": {
                    caption: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_ActiveAlerts_RelatedNode) %>",
                    isHtml: true,
                    formatter: function(cellValue, rowArray) {
                        return SW.Core.String.Format('<a href="{0}" {2}>{1}</a>',
                            rowArray[11].length != 0 ? rowArray[11] : location.href,
                            cellValue, linkTarget);
                    }
                },
                "ActiveTime": {
                    caption: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_ActiveAlerts_ActiveTime) %>"
                }<% if (ShowAcknowledgedAlerts) { %>
                ,
                "Notes": {
                    caption: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_ActiveAlerts_Notes) %>",
                    isHtml: true,
                    formatter: function (cellValue) {
                        var note = cellValue;
                        if (note.length >= 10) {
                            note = SW.Core.String.Format('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_ActiveAlerts_Notes_Ellipses) %>', note.substring(0, 6));
                            return SW.Core.String.Format('<span title="{0}" >{1}</span>', cellValue, note);
                        }
                        else
                            return cellValue;
                    }
                },
                "AcknowledgedByFullName": {
                    caption: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_ActiveAlerts_AckdBy) %>",
                    isHtml: true,
                    formatter: function(cellValue) {
                        if ((cellValue !== null) && (cellValue !== ""))
                            return SW.Core.String.Format('<img src="/Orion/images/ActiveAlerts/Acknowliedged_icon16x16v1.png" /><span style="vertical-align: super">{0}</span>', cellValue);
                        else return "";
                    }
                }
                <% } %>
                <% if (FederationEnabled) { %>
                ,
                "SiteName": {
                    caption: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AlertsOnThisEntity_SolarWindsSiteColumn) %>",
                    isHtml: true,
                    formatter: function (cellValue, rowArray) {
                        return SW.Core.String.Format('<a {2} href="/Server/{0}">{1}</a>',
                            rowArray[12],
                            rowArray[13],
                            linkTarget);
                    }
                }
                <% } %>
            },
            pageSizeChanged: function(resourceId, newPageSize) {
                SW.Core.Services.callController("/api/Resources/AddOrUpdateResourceProperty",
                {
                    resourceId: resourceId,
                    propertyName: "RowsPerPage",
                    propertyValue: newPageSize
                });
            }
        });

        var refresh = function () { SW.Core.Resources.ActiveAlerts.SortablePageableTable.refresh(<%= ResourceID %>); };
        SW.Core.View.AddOnRefresh(refresh, '<%= SortablePageableTable.ClientID %>');
        refresh();
    });
</script>
