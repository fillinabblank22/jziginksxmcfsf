﻿using System;

public partial class ResourceSearchControl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public string SearchBoxClientID
    {
        get { return searchBox.ClientID; }
    }

    public string SearchButtonClientID
    {
        get { return searchButton.ClientID; }
    }
}