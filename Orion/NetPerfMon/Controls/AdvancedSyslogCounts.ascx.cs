using System;
using System.Data;
using System.Web.UI;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SyslogTraps.Web.DAL;

public partial class Orion_NetPerfMon_Controls_AdvancedSyslogCounts : System.Web.UI.UserControl
{
    public int all_msg = 0;

    public string HelpLink
    {
        get
        {
            return HelpHelper.GetHelpUrl("OrionPHResourceSyslogSummary");
        }
    }

    public ResourceInfo Resource
    {
        get
        {
            Control ctrl = this.Parent;
            while (ctrl != null)
            {
                if (ctrl is SolarWinds.Orion.Web.UI.BaseResourceControl)
                    return ((SolarWinds.Orion.Web.UI.BaseResourceControl)ctrl).Resource;
                ctrl = ctrl.Parent;
            }
            return null;
        }
    }
    public string Timee
    {
        get
        {
            String s = string.Empty;
            try
            {
                if (Resource != null)
                    s = Resource.Properties["timee"];
                else
                    s = Request.QueryString["timee"];
            }
            catch { }
            if (string.IsNullOrEmpty(s))
                s = "hour";
            return s;
        }
    }


    public string Period
    {
        get
        {
            String s = Timee;
            switch (s.ToLower())
            {
                case "minute": return "Past Minute";
                case "hour": return "Past Hour";
                case "day": return "Last 24 Hours";
                case "week": return "Last 7 Days";
                case "month": return "Last 30 Days";
                case "custom": return string.Format("From {0} to {1}", ps.dFrom.ToString(), ps.dTo.ToString());
                // tbFrom.Text, tbTo.Text); break;
                default: return "Past Hour";
            }
        }
    }

    public string qPeriod
    {
        get
        {
            if (Timee.Equals("custom", StringComparison.OrdinalIgnoreCase))
                return string.Format("{0}~{1}", ps.dFrom.ToString(), ps.dTo.ToString());
            return Period;
        }
    }

    public string GetNodeLink(string nodeId, string text)
    {
        string s = text;
        if (nodeId != "0")
        {
            s = string.Format(@"<a href=""/netperfmon/View.asp?View=NodeDetails&NetObject=N:{0} target=""_blank""> {1} </a>", nodeId, s);
        }
        return s;
    }

    public string GetLinkTarget()
    {
        if (Resource != null)
            return "_blank";
        else
            return string.Empty;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            LoadData();
    }

    private void LoadData()
    {
        //if (!IsPostBack)
        //{
        //    tbFrom.Text = tbTo.Text = DateTime.Now.ToString();
        //}

        var periodBegin = DateTime.Now;
        var periodEnd = DateTime.Now;
        string PeriodName = Period;
        if (Timee == "custom")
        {
            try
            {
                periodBegin = ps.dFrom;
                periodEnd = ps.dTo;
                //DateTime.TryParse(tbFrom.Text, out periodBegin);
                //DateTime.TryParse(tbTo.Text, out periodEnd);
            }
            catch { }
        }
        else
            Periods.Parse(ref PeriodName, ref periodBegin, ref periodEnd);

        using (WebDAL webdal = new WebDAL())
        {
            DataTable table = webdal.GetSysLogMessagesCountsForAdvancedParser(periodBegin, periodEnd);
            foreach (DataRow dr in table.Rows)
            {
                all_msg += (int)dr["Total"];
            }
            mainRepeater.DataSource = table;
            mainRepeater.DataBind();
        }

        if (Timee == "custom")
        {
            plhCustom.Visible = true;
            lblPeriod.Text = string.Format(Resources.SyslogTrapsWebContent.WEBCODE_VB0_174, ps.dFrom.ToString(), ps.dTo.ToString());
        }
        else
        {
            lblPeriod.Text = Periods.GetLocalizedPeriod(Period);
        }
        DataBind();
    }

    public string GetSeverityType(object severity)
    {
        string sseverity = severity == null ? "" : severity.ToString();
        switch (sseverity)
        {
            case "0": return Resources.SyslogTrapsWebContent.WEBCODE_VB0_169;
            case "1": return Resources.SyslogTrapsWebContent.WEBCODE_VB0_164;
            case "2": return Resources.SyslogTrapsWebContent.WEBCODE_VB0_165;
            case "3": return Resources.SyslogTrapsWebContent.WEBCODE_AK0_1;
            case "4": return Resources.SyslogTrapsWebContent.Status_Warning;
            case "5": return Resources.SyslogTrapsWebContent.WEBCODE_VB0_166;
            case "6": return Resources.SyslogTrapsWebContent.WEBCODE_VB0_167;
            case "7": return Resources.SyslogTrapsWebContent.WEBCODE_VB0_168;
        }
        return string.Empty;
    }

    protected void btnGo_Click(object sender, EventArgs e)
    {
        LoadData();
    }
}
