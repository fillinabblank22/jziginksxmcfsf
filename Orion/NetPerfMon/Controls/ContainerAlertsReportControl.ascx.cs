﻿using System;
using System.Web.UI;
using System.Data;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Common;

[Obsolete("This controls does not show v2 Alerts")]
public partial class Orion_NetPerfMon_Controls_ContainerAlertsReportControl : System.Web.UI.UserControl
{
    [PersistenceMode(PersistenceMode.Attribute)]
    public bool ShowAcknowledgedAlerts { get; set; }
    
    [PersistenceMode(PersistenceMode.Attribute)]
    public int ContainerID { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool FilterContainer { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool ShowGroupName { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        int? containerID = null;

        if (this.FilterContainer)
        {
            containerID = this.ContainerID;
        }

        DataTable data = SqlDAL.GetContainerAlerts(containerID, this.ShowAcknowledgedAlerts);

        data.Columns.Add("Url");

		foreach (DataRow row in data.Rows)
		{
			string prefix = ((string)row["ObjectType"]).Equals("Group") ? "C" : "GM";
			row["Url"] = String.Format("/Orion/View.aspx?NetObject={0}:{1}", prefix, row["ObjectID"]);
            row["AlertTime"] = ((DateTime)row["AlertTime"]).ToLocalTime();
		}

        alertsRepeater.DataSource = data;
        alertsRepeater.DataBind();
    }

    protected string FormatTime(object time)
    {
        DateTime t = (DateTime)time;
        return Utils.FormatCurrentCultureDate(t) + " " + Utils.FormatToLocalTime(t);
    }
}