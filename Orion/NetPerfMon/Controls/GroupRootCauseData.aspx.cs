﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Script.Serialization;

using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Shared;

public partial class Orion_NetPerfMon_Resources_Controls_GroupRootCauseData : System.Web.UI.Page
{
	#region Nested Types

	public class Entity
	{
		private List<Entity> _children = new List<Entity>();

		private String _statusName, _statusIcon;

		#region Properties

		public String Type { get; set; }

		public Int32 ID { get; set; }

		public String Name { get; set; }

        public bool HasChildren { get; set; }

		[ScriptIgnore]
		public Int32 StatusID { get; set; }

        /// <summary>
        /// Gets localized member status message being represented by current instance.
        /// </summary>
	    public String StatusMessage
	    {
	        get { return string.Format(Resources.CoreWebContent.WEBCODE_PEC_2, this.Name, Resources.CoreWebContent.WEBCODE_PEC_1,
                                                                        StatusInfo.GetStatus(this.StatusID).ShortDescription.ToLower()); }
	    }
        
		public String StatusName
		{
			get
			{   
				return _statusName ?? (_statusName = StatusInfo.GetStatus(this.StatusID).StatusName.ToLower());
			}
			set { _statusName = value; }
		}

		public String StatusIcon
		{
			get
			{
				if (this.Type == null)
				{
					_statusIcon = "/Orion/StatusIcon.ashx?size=small&entity=&status=1";
				}
				if (_statusIcon == null)
				{
					_statusIcon = String.Format("/Orion/StatusIcon.ashx?size=small&entity={0}&id={1}&status={2}", this.Type, this.ID, this.StatusID);
				}
				return _statusIcon;
			}
			set { _statusIcon = value; }
		}

		public String DetailsUrl { get; set; }

		[ScriptIgnore]
		public Entity Owner { get; set; }

		public List<Entity> Children
		{
			get { return _children; }
		}

		#endregion

		#region Helper Members

		public static Entity Create(Entity owner, DataRow row)
		{
			return new Entity
			{
				Owner = owner,

				ID = Convert.ToInt32(((string)row["EntityUri"]).Split('/').Last().Split('=').Last()),
				Type = Convert.ToString(row["EntityType"]),
				Name = Convert.ToString(row["EntityName"]),
				StatusID = Convert.ToInt32(row["EntityStatus"]),
				DetailsUrl = Convert.ToString(row["EntityDetailsUrl"])
			};
		}

		public Boolean Contains(Entity owner)
		{
			if (owner.Type == ContainersDAL.Entity)
			{
				if (this.ID == owner.ID && this.Type == ContainersDAL.Entity)
				{
					return true;
				}
				if (this.Owner != null)
				{
					return this.Owner.Contains(owner);
				}
			}
			return false;
		}

		#endregion
	}

    public class ResultsContainer
    {
        public Int32 TotalCount { get; set; }
        public Entity[] Entities { get; set; }
    }

	#endregion

	#region Fields

	private Int32 _membersCount, _membersCounter;

	private static DataTable _relations;

	private Dictionary<String, DataTable> _cache;

	#endregion

	#region Event Handlers

	protected void Page_Load(Object sender, EventArgs e)
	{
		Object response;
		
		if (Int32.TryParse(this.Request["MembersCount"], out _membersCount) == false || _membersCount == 0)
		{
			_membersCount = 20;
		}
		if (_membersCount == -1)
		{
			_membersCount = Int32.MaxValue-1; // we need to add 1 to this value later so that's why -1 is here
		}

		Int32 viewID;
		Int32.TryParse(this.Request["ViewID"], out viewID);
		if (viewID > 0)
		{
			var view = SolarWinds.Orion.Web.ViewManager.GetViewById(viewID);
			if (view != null)
			{
				System.Web.HttpContext.Current.Items[view.GetType().Name] = view;
			}
		}

		Int32 entityID;
		Int32.TryParse(this.Request["EntityID"], out entityID);
	    String entityType = this.Request["EntityType"];

	    bool isRoot;
        if (!bool.TryParse(this.Request["IsRoot"], out isRoot))
            isRoot = true;

		if (entityID > 0)
		{
			try
			{
				var entity = new Entity
				{
					ID = entityID,
					Type = entityType
				};
				this.Populate(entity);

				if ((entity.Children.Count == 0) && (isRoot))
				{
					response = string.Format("{{Error:'{0}'}}",Resources.CoreWebContent.WEBCODE_AK0_116);
				}
				else
				{
					if (_membersCounter >= _membersCount)
					{
						entity.Children.Add(
                            new Entity { Name = String.Format("<b style='color:#f00;'>{0}</b>", String.Format(Resources.CoreWebContent.WEBCODE_AK0_117, _membersCount)), DetailsUrl = "#showAll" }
						);
					}

				    ResultsContainer results = new ResultsContainer()
				                                   {
				                                       TotalCount = _membersCounter,
				                                       Entities = entity.Children.ToArray()
				                                   };
                    response = new JavaScriptSerializer().Serialize(results);
				}
			}
			catch (Exception ex)
			{
                response = string.Format("{{Error:'{0}'}}", Resources.CoreWebContent.WEBCODE_AK0_118);

				new SolarWinds.Logging.Log().Error(ex);
			}
		}
		else
		{
            response = String.Format("{{Error:'{0}'}}", string.Format(Resources.CoreWebContent.WEBCODE_AK0_119, this.Request["GroupID"] ?? "null"));
		}
		_cache = null;

		this.Response.ContentType = "text/html";
		this.Response.Write(response);
		this.Response.End();
	}

	#endregion

	#region Data Members

	private DataTable DS_Relations
	{
		get
		{
			if (_relations == null)
			{
				const String QUERY = @"
SELECT DISTINCT rs.SourceType, p.SourceProperty, rs.TargetType, p.TargetProperty
FROM Metadata.Relationship AS rs
JOIN (
	SELECT ps.EntityName AS SourceType, ps.Name AS SourceProperty, pt.EntityName as TargetType, pt.Name AS TargetProperty
	FROM Metadata.Property AS ps
	JOIN Metadata.Property AS pt ON pt.Name = ps.Name AND pt.Type = ps.Type
	WHERE ps.IsKey = 'true' AND ps.IsNavigable = 'false' AND  ps.IsInjected = 'false'
) AS p ON p.SourceType = rs.SourceType AND p.TargetType = rs.TargetType
WHERE (rs.Source.IsAbstract = 'false') AND (rs.BaseType = 'System.Hosting') AND ({0}) AND ({1})
";
				var managed = new ContainersMetadataDAL().GetAvailableContainerMemberEntities()
					.Keys
					.ToList();

				using (var swis = InformationServiceProxy.CreateV3())
				{
					_relations = swis.Query(String.Format(
						QUERY,
						String.Join(" OR ", managed.Select(item => String.Format("rs.SourceType = '{0}'", item)).ToArray()),
						String.Join(" OR ", managed.Select(item => String.Format("rs.TargetType = '{0}'", item)).ToArray())
					));
				}
			}
			return _relations;
		}
	}

	private DataTable GetMembers(Int32 groupID)
	{
		var KEY = String.Format("{0}|{1}", ContainersDAL.Entity, groupID);
		if ((_cache = _cache ?? new Dictionary<String, DataTable>()).ContainsKey(KEY))
		{
			return _cache[KEY];
		}
		_cache[KEY] = null;

		const String QUERY = @"
            SELECT 
	            cm.ContainerID, 
	            cm.MemberEntityType AS EntityType, 
	            cm.Status AS EntityStatus, 
	            cm.DisplayName AS EntityName,
	            '#' AS EntityDetailsUrl, 
	            cm.MemberAncestorDisplayNames AS AncestorNames, 
	            cm.MemberAncestorDetailsUrls AS AncestorUrls, 
	            cm.MemberUri AS EntityUri
            FROM 
	            Orion.ContainerMembers cm
            INNER JOIN
	            Orion.StatusInfo si ON
	            cm.Status = si.StatusId
            WHERE 
	            (cm.Status != 1) AND (cm.ContainerID = {0}) AND (si.RollupType != 4)
            ORDER BY 
	            cm.DisplayName
        ";
		using (var swis = InformationServiceProxy.CreateV3())
		{
			_cache[KEY] = swis.Query(String.Format(QUERY, groupID));
			foreach (var row in _cache[KEY].Select())
			{
				String[] names = row["AncestorNames"] as String[], urls = row["AncestorUrls"] as String[];
				if (urls != null && names.Length == urls.Length)
				{
					var index = names.ToList().IndexOf(row["EntityName"].ToString());
					if (index > -1)
					{
						row["EntityDetailsUrl"] = urls[index];
					}
				}
			}
			_cache[KEY].Columns.Remove("AncestorNames");
			_cache[KEY].Columns.Remove("AncestorUrls");
			_cache[KEY].AcceptChanges();
		}
		return _cache[KEY];
	}

	private DataTable GetData(DataRow info, Object value)
	{
		var KEY = String.Format("{0}|{1}|{2}", info["SourceType"], info["TargetType"], value);
		if ((_cache = _cache ?? new Dictionary<String, DataTable>()).ContainsKey(KEY))
		{
			return _cache[KEY];
		}
		_cache[KEY] = null;

		String QUERY = @"
SELECT TOP {3}
	'{0}' AS EntityType, DisplayName AS EntityName, Status AS EntityStatus, DetailsUrl AS EntityDetailsUrl, Uri AS EntityUri
FROM {0}
WHERE Status != 1 AND {1} = '{2}'
";
		using (var swis = InformationServiceProxy.CreateV3())
		{
			_cache[KEY] = swis.Query(String.Format(QUERY, info["TargetType"], info["TargetProperty"], value, _membersCount+1));
		}

		return _cache[KEY];
	}

	#endregion

	#region Helper Members

	private void Populate(Entity entity)
	{
		if (entity.Type == ContainersDAL.Entity)
		{
			var data = this.GetMembers(entity.ID);
			if (data != null)
			{
                _membersCounter = data.Rows.Count;

				for (Int32 i = 0, count = data.Rows.Count; i < count && i < _membersCount; i++)
				{
					var child = Entity.Create(entity, data.Rows[i]);
					if (!entity.Contains(child))
					{
					    child.HasChildren = HasChildren(child);
						entity.Children.Add(child);
					}
				}
			}
		}
		else
		{
			foreach (var row in this.DS_Relations.Select(String.Format("SourceType = '{0}'", entity.Type)))
			{
				var data = this.GetData(row, entity.ID);
				if (data != null)
				{
					for (Int32 i = 0, count = data.Rows.Count; i < count && i < _membersCount; i++)
					{
					    var child = Entity.Create(entity, data.Rows[i]);
                        child.HasChildren = HasChildren(child);
						entity.Children.Add(child);
					}
				}
			}
		}
	}

    private bool HasChildren(Entity entity)
    {
        if (entity.Type == ContainersDAL.Entity)
        {
            var data = this.GetMembers(entity.ID);
            return ((data != null) && (data.Rows.Count > 0));
        }
        else
        {
            foreach (var row in this.DS_Relations.Select(String.Format("SourceType = '{0}'", entity.Type)))
            {
                var data = this.GetData(row, entity.ID);
                if ((data != null) && (data.Rows.Count > 0))
                    return true;
            }

            return false;
        }
    }

	#endregion
}