<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContainerAlertsReportControl.ascx.cs"
    Inherits="Orion_NetPerfMon_Controls_ContainerAlertsReportControl" %>
<asp:Repeater ID="alertsRepeater" runat="server">
    <HeaderTemplate>
        <table class="DataGrid" CellPadding="3">
            <thead>
                <tr>
                    <td style="width:30px;" >
                        &nbsp
                    </td>
                    <td style="width:110px;" >
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_202) %>
                    </td>
                    <%if (this.ShowGroupName)
                      { %>
                    <td>
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_203) %>
                    </td>
                    <%} %>
                    <td>
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_150) %>
                    </td>
                </tr>
            </thead>
    </HeaderTemplate>
    <ItemTemplate>
        <tr>
            <td>
                <asp:Image ID="imgAlert" runat="server" ImageUrl="/NetPerfMon/images/Event-19.gif" />
            </td>
            <td>
                <%# DefaultSanitizer.SanitizeHtml(FormatTime(Eval("AlertTime"))) %>
            </td>
            <%if (this.ShowGroupName)
              { %>
            <td>
                <a href="<%# DefaultSanitizer.SanitizeHtml(Eval("Url")) %>">
                    <%# DefaultSanitizer.SanitizeHtml(Eval("ObjectName")) %>
            </td>
            <%} %>
            <td>
                <%# DefaultSanitizer.SanitizeHtml(Eval("EventMessage")) %>
            </td>
        </tr>
    </ItemTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
</asp:Repeater>
