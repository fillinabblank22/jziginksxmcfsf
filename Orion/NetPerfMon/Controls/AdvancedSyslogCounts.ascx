<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdvancedSyslogCounts.ascx.cs"
    Inherits="Orion_NetPerfMon_Controls_AdvancedSyslogCounts" %>
<%@ Register Src="AdvancedSyslogPeriodSelect.ascx" TagName="AdvancedSyslogPeriodSelect"
    TagPrefix="uc1" %>

<orion:Include runat="server" File="legacyevents.css" /><%-- severity css --%>

<% if (Resource == null)
   { %>
<table width="100%" border="0" cellpadding="8" cellspacing="0">
    <tr class="HeaderBar">
        <td height="35" align="left" nowrap>
            <b><%= Resources.SyslogTrapsWebContent.WEBCODE_VB0_158 %><br>
                <font size="2"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_191 %>
                    <asp:Label runat="server" ID="lblPeriod"></asp:Label></font></b>
        </td>
        <asp:PlaceHolder runat="server" ID="plhCustom" Visible="false">
                <<td>
                <tr class="HeaderBar">
                    <td height="35" nowrap>
                            <table>
                                <tr>
                                   <td>
                                        <font size="2">
                                            <uc1:AdvancedSyslogPeriodSelect ID="ps" runat="server" />
                                         </font>
                                    </td>
                                    <td style="float:left;">
                                         <font size="2">
                                            <asp:Button runat="server" ID="btnGo" Text="<%$ Resources: SyslogTrapsWebContent, WEBDATA_VB0_192 %>" Style="height: 18px;
                                                    color: black; background-color: #fff; border: 1px solid Gray; font-size: 10px;"
                                                    OnClick="btnGo_Click" />
                                         </font>
                                    </td>
                                </tr>     
                            </table>
                    </td>
        </asp:PlaceHolder>
        <td>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="right" nowrap>
                        <orion:LocalizableButtonLink runat="server" NavigateUrl='<%# this.HelpLink %>' LocalizedText="CustomText" Text="<%$ Resources: SyslogTrapsWebContent, ResourcesAll_Help %>" DisplayType = "Resource" />
                        
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<% }%>
<table width="100%" border="0" cellpadding="8" cellspacing="0">
    <tr class="HeaderBar">
        <font size="2"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_193 %><a href="/Orion/netperfmon/AdvSyslogCounts.aspx?timee=minute"
            target="<%# GetLinkTarget()%>"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_194 %><%= Resources.SyslogTrapsWebContent.String_Separator_1_VB0_59 %></a> <a href="/Orion/netperfmon/AdvSyslogCounts.aspx?timee=hour"
                target="<%# GetLinkTarget()%>"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_195 %><%= Resources.SyslogTrapsWebContent.String_Separator_1_VB0_59 %></a> <a href="/Orion/netperfmon/AdvSyslogCounts.aspx?timee=day"
                    target="<%# GetLinkTarget()%>"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_196 %><%= Resources.SyslogTrapsWebContent.String_Separator_1_VB0_59 %></a> <a href="/Orion/netperfmon/AdvSyslogCounts.aspx?timee=week"
                        target="<%# GetLinkTarget()%>"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_197 %><%= Resources.SyslogTrapsWebContent.String_Separator_1_VB0_59 %></a> <a href="/Orion/netperfmon/AdvSyslogCounts.aspx?timee=month"
                            target="<%# GetLinkTarget()%>"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_198 %><%= Resources.SyslogTrapsWebContent.String_Separator_1_VB0_59 %></a> <a href="/Orion/netperfmon/AdvSyslogCounts.aspx?timee=custom"
                                target="<%# GetLinkTarget()%>"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_199 %></a> </font>
    </tr>
</table>
<asp:Repeater runat="server" ID="mainRepeater">
    <HeaderTemplate>
        <table width="100%" cellspacing="0" cellpadding="0">
            <tr class="ViewHeader">
                <td class="Property" style="white-space:nowrap;">
                    &nbsp;
                </td>
                <td class="Property" align="left" style="white-space:nowrap;">
                    <b><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_200 %></b>
                </td>
                <td class="Property" style="white-space:nowrap;">
                    &nbsp;
                </td>
                <td class="Property" width="5%" style="white-space:nowrap;">
                    <%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_201 %>&nbsp;&nbsp;
                </td>
                <td class="Property" width="5%" align="center" style="white-space:nowrap;">
                    <b><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_39 %></b>
                </td>
            </tr>
    </HeaderTemplate>
    <ItemTemplate>
        <tr class="Severity<%# Eval("SyslogSeverity")%>">
            <td class="Property">
                &nbsp;
            </td>
            <td class="Property">
                <a href="/Orion/NetPerfMon/SysLog.aspx?Period=<%# this.qPeriod%>&severity=<%# Eval("SyslogSeverity")%>"
                    target="<%# GetLinkTarget()%>">
                    <%# this.GetSeverityType(Eval("SyslogSeverity")) %></a>
            </td>
            <td class="Property">
                &nbsp;
            </td>
            <td class="Property" nowrap>
                <a href="/Orion/NetPerfMon/AdvSyslogWho.aspx?timee=<%# this.Timee%>&MessageType=NULLL&severity=<%# Eval("SyslogSeverity")%>"
                    target="<%# GetLinkTarget()%>"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_204 %></a>
            </td>
            <td class="Property" nowrap align="center">
                <a href="/Orion/NetPerfMon/SysLog.aspx?Period=<%# this.qPeriod%>&severity=<%# Eval("SyslogSeverity")%>"
                    target="<%# GetLinkTarget()%>">
                    <%# Eval("Total") %></a>
            </td>
        </tr>
    </ItemTemplate>
    <FooterTemplate>
        <tr>
            <td class="Property">
                &nbsp;
            </td>
            <td class="Property">
                &nbsp;
            </td>
            <td class="Property">
                &nbsp;
            </td>
            <td class="Property">
                &nbsp;
            </td>
            <td class="Property" nowrap align="center">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="Property">
                &nbsp;
            </td>
            <td class="Property">
                <b><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_202 %></b>
            </td>
            <td class="Property">
                &nbsp;
            </td>
            <td class="Property">
                &nbsp;
            </td>
            <td class="Property" nowrap align="center">
                <b>
                    <%= this.all_msg.ToString() %></b>
            </td>
        </tr>
        </table>
    </FooterTemplate>
</asp:Repeater>
