<%@ Control Language="C#" CodeFile="StatusIconControl.ascx.cs" Inherits="Orion_NetPerfMon_Controls_StatusIconControl" EnableViewState="false" %>
<orion:Include ID="OrionCoreInclude" runat="server" File="OrionCore.js" />

<img id="NodeDetailsStatusIconLED" src="" alt="" runat="server" />

<script type="text/javascript">
//<![CDATA[
    $(document).ready(function () {
        var statusQuery = "<%= StatusLEDQuery.Replace("\"","\\\"") %>";
        if (!statusQuery)
            return;

        var refresh = function () {
            ORION.callWebService(
                    "/Orion/Services/Information.asmx",
                    "QueryWithPartialErrors",
                    { query: statusQuery },
                    function (result) {
                        var rows = result.d != null ? result.d.Data.Rows : result.Data.Rows;
                        var statusId = 0; // unknown as default

                        if (rows.length > 0) {
                            statusId = rows[0][0];
                        }

                        var iconPath = String.format('/Orion/StatusIcon.ashx?id={0}&entity={1}&status={2}&size=small{3}',
                                '<%= EntityId %>', '<%= EntityFullName %>', statusId, '<%= ViewLimitationId > 0 ? string.Format("&viewid={0}", ViewLimitationId) : string.Empty %>');
                        $("#<%= NodeDetailsStatusIconLED.ClientID %>").attr("src", iconPath);
                    }
                );
        };
        SW.Core.View.AddOnRefresh(refresh);

        var statusLEDImageSrc = "<%= StatusLEDImageSrc %>";
        if (!statusLEDImageSrc)
            refresh();
    });
//]]>    
</script>
