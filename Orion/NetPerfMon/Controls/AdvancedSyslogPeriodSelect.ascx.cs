using System;

using SolarWinds.Orion.Web;

public partial class Orion_NetPerfMon_Controls_AdvancedSyslogPeriodSelect : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Visible)
        {
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "initPickers",
                @"//<![CDATA[
				(function($) {
					var regionalSettings = " + DatePickerRegionalSettings.GetDatePickerRegionalSettings() + @"

					$(function() {
						$.datepicker.setDefaults(regionalSettings);
						$('.datePicker').datepicker({mandatory: true, closeAtTop: false});
						$('.timePicker').timePicker({separator:regionalSettings.timeSeparator, show24Hours:regionalSettings.show24Hours});
					});
				})(jQuery);
				//]]>", true);
        }


        if (string.IsNullOrEmpty(tbFromDate.Text + tbToDate.Text))
        {
            tbFromDate.Text = tbToDate.Text = DateTime.Now.ToShortDateString();
            tbFromTime.Text = tbToTime.Text = DateTime.Now.ToString("HH:mm");
        }
    }

    public DateTime dFrom
    {
        get
        {
            string s = tbFromDate.Text + " " + tbFromTime.Text;
            DateTime date;
            if (DateTime.TryParse(s, out date))
            {
                return date;
            }
            else
            {
                return DateTime.Now;
            }
        }
    }

    public DateTime dTo
    {
        get
        {
            string s = tbToDate.Text + " " + tbToTime.Text;
            DateTime date;
            if (DateTime.TryParse(s, out date))
            {
                return date;
            }
            else
            {
                return DateTime.Now;
            }
        }
    }
}