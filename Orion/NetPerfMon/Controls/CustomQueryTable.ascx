﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomQueryTable.ascx.cs" Inherits="Orion_NetPerfMon_Controls_CustomQueryTable" %>

<orion:Include ID="Include1" runat="server" File="NetPerfMon/resources/Misc/CustomQuery.js" />

<asp:ScriptManagerProxy runat="server" >
    <Services>
        <asp:ServiceReference path="/Orion/Services/Information.asmx" />
    </Services>
</asp:ScriptManagerProxy>
   
<div class="sw-custom-query-table-container"> 
    <input type="hidden" name="limitation" id="Limitation-<%= UniqueClientID %>" value='<%= DefaultSanitizer.SanitizeHtml(ViewLimitationString) %>'/>

    <div id="ErrorMsg-<%= UniqueClientID %>"></div>

    <div id="Loading-<%= UniqueClientID %>" class="sw-custom-query-table-loading" style="display: none;">
        <div class="sw-custom-query-table-loading-table">
            <div class="sw-custom-query-table-loading-cell">
                <img src="/Orion/images/loading_gen_small.gif" />
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_EK0_01) %>
            </div>
        </div>
    </div>

    <table id="Grid-<%= UniqueClientID %>" class="NeedsZebraStripes sw-custom-query-table" cellpadding="2" cellspacing="0" width="100%">
        <tr class="HeaderRow"></tr>
    </table>

    <div id="Pager-<%= UniqueClientID %>" class="ReportFooter ResourcePagerControl hidden"></div>

    <textarea id="SWQL-<%= UniqueClientID %>" style="display:none;">
        <%= HttpUtility.HtmlEncode(SWQL) %>
    </textarea>

    <textarea id="SearchSWQL-<%= UniqueClientID %>" style="display:none;">
        <%= HttpUtility.HtmlEncode(SearchSWQL) %>
    </textarea>
        
    <div id="PartialError-<%= UniqueClientID %>"></div>

    <input type="hidden" id="OrderBy-<%= UniqueClientID %>" value="<%= HttpUtility.HtmlEncode(OrderBy)%>" />
</div>

