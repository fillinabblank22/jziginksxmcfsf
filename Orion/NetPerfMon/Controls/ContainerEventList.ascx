<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContainerEventList.ascx.cs" Inherits="Orion_Controls_ContainerEventList" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Swis" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Federation" %>

<asp:Repeater ID="EventsGrid" runat="server">
	<HeaderTemplate>
		<table width="100%" cellspacing="0" cellpadding="3" class="NeedsZebraStripes Events">
	</HeaderTemplate>
	
	<ItemTemplate>
		<tr>
            <td style="width: 100px;" >
                <%# DefaultSanitizer.SanitizeHtml(Eval("EventTime", "{0:g}")) %> 
            </td>
            <td>
                <div class="event-icon" style="background-color:#<%# Convert.ToInt32(Eval("BackColor")).ToString("X") %>;">
                    <img src="<%# DefaultSanitizer.SanitizeHtml(FederationUrlHelper.GetImagePrefix(Convert.ToInt32(Eval("InstanceSiteId")))) %>/NetPerfMon/images/Event-<%# DefaultSanitizer.SanitizeHtml(Eval("EventType")) %>.gif" alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_209) %>" >
                </div>
            </td>
            <td>
                <asp:HyperLink ID="ItemHyperLink" runat="server" Target='<%# SwisFederationInfo.IsFederationEnabled ? "_blank" : "_self" %>' NavigateUrl='<%# DefaultSanitizer.SanitizeHtml(Eval("URL")) %>'><%# DefaultSanitizer.SanitizeHtml(Eval("Message")) %></asp:HyperLink>
            </td>
            <% if (SwisFederationInfo.IsFederationEnabled){ %> 
            <td>
                <asp:HyperLink ID="SiteLink" runat="server" Target="_blank" NavigateUrl='<%# DefaultSanitizer.SanitizeHtml(FederationUrlHelper.GetLinkPrefix((int)Eval("InstanceSiteId"))) %>'><%# DefaultSanitizer.SanitizeHtml(Eval("SiteName")) %></asp:HyperLink>
            </td>
		    <% }%>
		</tr>
	</ItemTemplate>
	
	<FooterTemplate>
		</table>
	</FooterTemplate>
</asp:Repeater>