﻿SW.Core.Resources = SW.Core.Resources || {};
SW.Core.Resources.CustomQuery = SW.Core.Resources.CustomQuery || {};
SW.Core.Resources.CustomQuery.FilterDropdown = SW.Core.Resources.CustomQuery.FilterDropdown || {};

(function (f) {
    var settings = {};

    var baseSettings = {
        setResultQueryCallback: function () { },
        setResultSearchQueryCallback: function () { },
        filterFinishedCallback: function () { }
    };

    var filter = function (resourceId, filterValue) {
        var swql = settings[resourceId].swql || '';
        var searchSwql = settings[resourceId].searchSwql || '';
        var filterForSelectSwql = settings[resourceId].filterForSelectSwql || '';
        var filterForSearchSwql = settings[resourceId].filterForSearchSwql || '';

        if (filterValue != '') {
            swql = filterForSelectSwql.replace(new RegExp('\\$\\{FILTER_VALUE\\}', 'g'), filterValue.trim());
            searchSwql = filterForSearchSwql.replace(new RegExp('\\$\\{FILTER_VALUE\\}', 'g'), filterValue.trim());
        }
        settings[resourceId].setResultQueryCallback(resourceId, swql);
        settings[resourceId].setResultSearchQueryCallback(resourceId, searchSwql);
        settings[resourceId].filterFinishedCallback(resourceId);
    }

    var getSettings = function (resourceId) {
        settings[resourceId] = settings[resourceId] || $.extend(true, {}, baseSettings);
        return settings[resourceId];
    }

    /*
     * Initial settins are expected in format:
     * {
     *  setResultQueryCallback: function(resourceId, query) {...},
     *  setResultSearchQueryCallback: function(resourceId, query) {...}
     *  filterFinishedCallback: function(resourceId) {...}
     * }
     */
    f.initialize = function (resourceId, initialSettings, initialValue) {
        $.extend(getSettings(resourceId), initialSettings);
        if (initialValue == null) {
            initialValue = '';
        }
        filter(resourceId, initialValue);
    };

    f.setQuery = function (resourceId, query) {
        getSettings(resourceId).swql = query;
    };

    f.setSearchQuery = function (resourceId, query) {
        getSettings(resourceId).searchSwql = query;
    };

    f.setFilterForSelectQuery = function (resourceId, query) {
        getSettings(resourceId).filterForSelectSwql = query;
    };

    f.setFilterForSearchQuery = function (resourceId, query) {
        getSettings(resourceId).filterForSearchSwql = query;
    };

    f.setFilter = function (resourceId, filterValue) {
        filter(resourceId, filterValue);
    }
})(SW.Core.Resources.CustomQuery.FilterDropdown);