using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;

public partial class Orion_NetPerfMon_Controls_StatusIconControl : System.Web.UI.UserControl
{
    /// <summary>
    /// Src path to initial status image. This image will be used before AJAX request when page is rendered.
    /// </summary>
    public string StatusLEDImageSrc { get; set; }

    private string _entityFullName = "Orion.Nodes";

    /// <summary>
    /// SWIS entity full name. Default value is Orion.Nodes.
    /// </summary>
    public string EntityFullName
    {
        get { return _entityFullName; }
        set { _entityFullName = value; }
    }

    private string _entityIdName = "NodeId";

    /// <summary>
    /// Name of ID property which is used for data filtering. Default value is NodeId.
    /// </summary>
    public string EntityIdName
    {
        get { return _entityIdName; }
        set { _entityIdName = value; }
    }

    /// <summary>
    /// ID of entity. It is used to get status.
    /// </summary>
    public string EntityId { get; set; }

    /// <summary>
    /// Uri of swis entity
    /// </summary>
    public string EntitySwisUri { get; set; }

    /// <summary>
    /// Id of view limitation. It is needed to apply view limitations on child status properly.
    /// </summary>
    public int ViewLimitationId { get; set; }

    protected string StatusLEDQuery
    {
        get 
        {
            if (!string.IsNullOrEmpty(EntitySwisUri) && !string.IsNullOrEmpty(EntityFullName))
            {
                return string.Format("SELECT Status FROM {0} WHERE Uri='{1}'", EntityFullName, EntitySwisUri);
            }

            return string.Format("SELECT Status FROM {0} WHERE {1} = '{2}'", EntityFullName, EntityIdName, EntityId);
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        NodeDetailsStatusIconLED.Src = StatusLEDImageSrc;
    }
}
