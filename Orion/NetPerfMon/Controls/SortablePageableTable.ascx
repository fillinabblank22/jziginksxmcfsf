﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SortablePageableTable.ascx.cs" Inherits="Orion_NetPerfMon_Controls_SortablePageableTable" %>

<orion:Include ID="Include1" runat="server" File="SortablePageableTable.js" />

<div id="ErrorMsg-<%= UniqueClientID %>"></div>

<table id="Grid-<%= UniqueClientID %>" class="NeedsZebraStripes sw-custom-query-table <%= RowSeparator ? "rowSeparator" : string.Empty %>" cellpadding="2" cellspacing="0" width="100%">
    <tr class="HeaderRow"></tr>
</table>

<div id="Pager-<%= UniqueClientID %>" class="ReportFooter ResourcePagerControl hidden"></div>