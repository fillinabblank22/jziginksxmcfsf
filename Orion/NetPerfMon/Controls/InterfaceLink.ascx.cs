﻿using System;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.NPM.Web;

[Obsolete("This class is obsolete and will be removed.  You should use /Orion/NPM/Controls/InterfaceLink.ascx")]
public partial class Orion_Obsolete_NetPerfMon_Controls_InterfaceLink : UserControl
{
    private Control _wrappedControl;

    public Orion_Obsolete_NetPerfMon_Controls_InterfaceLink()
    {
        _wrappedControl = LoadControl("~/Orion/NPM/Controls/InterfaceLink.ascx");

        if (_wrappedControl != null)
        {
            Controls.Add(_wrappedControl);
        }
        else
        {
            throw new InvalidOperationException("Could not load /Orion/NPM/Controls/InterfaceLink.ascx control.");
        }
    }

    [PersistenceMode(PersistenceMode.InnerProperty)]
    public PlaceHolder Content
    {
        get
        {
            return (PlaceHolder)GetWrappedControlPropertyValue("Content");
        }
    }

    public string InterfaceID
    {
        get
        {
            return (string)GetWrappedControlPropertyValue("InterfaceID");
        }
        set
        {
            SetWrappedControlPropertyValue("InterfaceID", value);
        }
    }

	public Interface Interface
	{
		get
		{
            return (Interface)GetWrappedControlPropertyValue("Interface");
		}
		set
		{
            SetWrappedControlPropertyValue("Interface", value);
		}
	}

    [PersistenceMode(PersistenceMode.Attribute)]
    public string TargetURLFormat
    {
        get
        {
            return (string)GetWrappedControlFieldValue("TargetURLFormat");
        }
        set
        {
            SetWrappedControlFieldValue("TargetURLFormat", value);
        }
    }

    private PropertyInfo GetWrappedControlProperty(string propertyName)
    {
        PropertyInfo propertyInfo = _wrappedControl.GetType().GetProperty(propertyName);

        if (propertyInfo == null)
        {
            throw new InvalidOperationException(string.Format("Property {0} not found on wrapped control.", propertyName));
        }

        return propertyInfo;
    }

    private FieldInfo GetWrappedControlField(string fieldName)
    {
        FieldInfo fieldInfo = _wrappedControl.GetType().GetField(fieldName);

        if (fieldInfo == null)
        {
            throw new InvalidOperationException(string.Format("Field {0} not found on wrapped control.", fieldName));
        }

        return fieldInfo;
    }

    private object GetWrappedControlPropertyValue(string propertyName)
    {
        return GetWrappedControlProperty(propertyName).GetValue(_wrappedControl, null);
    }

    private void SetWrappedControlPropertyValue(string propertyName, object value)
    {
        GetWrappedControlProperty(propertyName).SetValue(_wrappedControl, value, null);
    }

    private object GetWrappedControlFieldValue(string fieldName)
    {
        return GetWrappedControlField(fieldName).GetValue(_wrappedControl);
    }

    private void SetWrappedControlFieldValue(string fieldName, object value)
    {
        GetWrappedControlField(fieldName).SetValue(_wrappedControl, value);
    }
}
