﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Core.Common;
using System.Data;
using SolarWinds.Orion.Web.InformationService;

public partial class Orion_NetPerfMon_Controls_ForecastingProfilingInProgress : System.Web.UI.UserControl
{
    private TimeSpan timeRemaining;

    protected int DaysRemaining
    {
        get { return Math.Max(timeRemaining.Days, 0); }
    }

    protected int HoursRemaining
    {
        get { return Math.Max(timeRemaining.Hours, 0); }
    }

    protected int MinimalDays
    {
        get { return SettingsDAL.GetCurrentInt("ForecastMinDays", 7); }
    }

    protected string ProfilingDays
    {
        get { return String.Format(Resources.CoreWebContent.WEBDATA_LF0_Profiling3, MinimalDays); }
    }

    protected string RemainingText
    {
        get { return String.Format(Resources.CoreWebContent.WEBDATA_LF0_RemainingText, DaysRemaining, HoursRemaining); }
    }

    protected int ProfilingPercentDone
    {
        get
        {
            var percent = 100 - (100 * (24 * DaysRemaining + HoursRemaining)) / (24 * MinimalDays);
            percent = Math.Max(percent, 0);
            percent = Math.Min(percent, 100);
            return percent;
        }
    }

    public void Initialize(DateTime dataSinceUtc)
    {
        timeRemaining = DateTime.UtcNow - dataSinceUtc;
    }

    public void Initialize(DateTime? dataSinceUtc, String historicalTable, String identifierName, int id)
    {
        Initialize(dataSinceUtc, CreateQuery(historicalTable, identifierName), id);
    }

    public void Initialize(DateTime? dataSinceUtc, String query, int id)
    {
        if (dataSinceUtc.HasValue)
        {
			TimeSpan fullspan = TimeSpan.FromDays(MinimalDays);
            timeRemaining = fullspan - (DateTime.UtcNow - dataSinceUtc.Value);
        }
        else
        {
            timeRemaining = GetTimeRemaining(query, id);
        }
    }

    protected TimeSpan GetTimeRemaining(String query, int id)
    {
        DataTable results;
        using (var swis = InformationServiceProxy.CreateV3())
        {
            var queryParams = new Dictionary<string, object>();
            queryParams[@"identifier"] = id;
            queryParams[@"sinceDateTime"] = DateTime.UtcNow.AddDays(- MinimalDays - 1);
            queryParams[@"now"] = DateTime.UtcNow;

            results = swis.Query(query, queryParams);
        }

        TimeSpan fullspan = TimeSpan.FromDays(MinimalDays);
        if (results != null && results.Rows.Count > 0 && results.Rows[0][0] != DBNull.Value)
        {
            DateTime minDateTime = (DateTime)results.Rows[0][0];
            return fullspan - (DateTime.UtcNow - minDateTime);
        }

        return fullspan;
    }

    private static String CreateQuery(String historicalTable, String identifierName)
    {
        /*
         * SELECT TOP 1 DateTime ORDER BY was marked 10x faster than SELECT MIN(DateTime) by SQL Server
         * 
         * ToLocal is currently necessary in ORDER BY for performance reasons, causes SWIS not putting ToUtc there,
         * which causes having to calculate it for every single value and preventing usage of index
         */
        const String queryBase = @"
            SELECT TOP 1 DateTime AS MinDateTime
            FROM {0}
            WHERE
                {1} = @identifier
                AND
                DateTime >= @sinceDateTime
                AND
                DateTime <= @now
            ORDER BY ToLocal(DateTime)";

        return String.Format(queryBase, historicalTable, identifierName);
    }

    protected void Page_Load(object sender, EventArgs e) { }

}
