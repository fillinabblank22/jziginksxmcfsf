<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListEntities.ascx.cs" Inherits="CoreListEntities" %>

<table class="NeedsZebraStripes" style="border-collapse:collapse;">
    
    <asp:Repeater ID="EntitiesRepeater" runat="server">
        <ItemTemplate>
            <tr style="background-color:#f8f8f8">
                <td class="Icon"><img src="/Orion/StatusIcon.ashx?entity=<%= DefaultSanitizer.SanitizeHtml(EntityName) %>&amp;status=<%# DefaultSanitizer.SanitizeHtml(Eval("Status")) %>&amp;size=small"/></td>
                <td style="min-width:300px; padding-right:10px;"><a href='<%# DefaultSanitizer.SanitizeHtml(Eval("DetailsUrl")) %>'><%# DefaultSanitizer.SanitizeHtml(Eval("Name")) %></a></td>
                <td>
                    <asp:ImageButton ID="deleteButton" runat="server" ToolTip="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_31 %>" ImageUrl="~/Orion/Discovery/images/icon_delete.gif"
                    OnCommand="ActionItemCommand" CommandName="Delete" CommandArgument='<%# DefaultSanitizer.SanitizeHtml(Eval("ID")) %>' />
                </td>
            </tr>                    
        </ItemTemplate>
    </asp:Repeater>

    <tr style="background-color:#ecedee" id="FilterOption" runat="server">
        <td class="Icon"><asp:CheckBox ID="Filter" runat="server" /></td>
        <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_373) %></td>
        <td></td>
    </tr>
</table>
