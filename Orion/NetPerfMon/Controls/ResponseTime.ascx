<%@ Control Language="C#" ClassName="ResponseTime" EnableViewState="false" %>
<%@ Import Namespace="SolarWinds.Orion.NPM.Web" %>

<script runat="server">


    public int Value { get; set; }
    public string PredefinedClassName { get; set; }

    protected string ClassName
    {
        get
        {
            if (!string.IsNullOrEmpty(PredefinedClassName))
                return PredefinedClassName == "Normal" || PredefinedClassName == "Unknown" ? string.Empty : PredefinedClassName;

            if (Thresholds.ResponseTimeError.SettingValue <= this.Value)
                return "Error";
            else if (Thresholds.ResponseTimeWarning.SettingValue <= this.Value)
                return "Warning";

            return string.Empty;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        if (this.Value < 0)
            this.Controls.Clear();

        base.OnInit(e);
    }

    protected string FormatValue
    {
        get
        {
            if (Value < 0)
                return "&nbsp;";

            return string.Format(Resources.CoreWebContent.WEBCODE_AK0_48, Value);
        }
    }
</script>

<span class="<%= DefaultSanitizer.SanitizeHtml(this.ClassName) %>"><%= DefaultSanitizer.SanitizeHtml(this.FormatValue) %></span>