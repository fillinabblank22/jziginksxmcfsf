﻿using System;
using SolarWinds.Orion.Web;

public partial class Orion_NetPerfMon_Controls_CustomQueryTable : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public int UniqueClientID { get; set; }

    public string SWQL { get; set; }

    public string SearchSWQL { get; set; }

    public string OrderBy { get; set; }

    public override string ClientID
    {
        get { return "Grid-" + UniqueClientID; }
    }

    public string ViewLimitationString
    {
        get
        {
            ViewInfo info = (ViewInfo) Context.Items[typeof (ViewInfo).Name];
            if (info.LimitationID > 0)
                return string.Format(" WITH LIMITATION {0} ", info.LimitationID);
            else
                return string.Empty;
        }
    }
}