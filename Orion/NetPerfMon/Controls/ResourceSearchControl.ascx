﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResourceSearchControl.ascx.cs" Inherits="ResourceSearchControl" %>

<asp:Panel runat="server" CssClass="sw-resource-searchcontrol">
    <asp:TextBox runat="server" ID="searchBox" CssClass="sw-resource-searchbox" onkeydown="return (event.keyCode!=13);"></asp:TextBox>
    <asp:ImageButton runat="server" ID="searchButton" CssClass="sw-resource-searchbutton" ImageUrl="/Orion/images/Button.SearchIcon.gif" OnClientClick="return false;"/>
</asp:Panel>