<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdvancedSyslogSummary.ascx.cs"
    Inherits="Orion_NetPerfMon_Controls_AdvancedSyslogSummary" %>
<%@ Register Src="AdvancedSyslogPeriodSelect.ascx" TagName="AdvancedSyslogPeriodSelect"
    TagPrefix="uc1" %>

<orion:Include runat="server" File="legacyevents.css" /><%-- severity css --%>

<% if (Resource == null)
   { %>
<table width="100%" border="0" cellpadding="8" cellspacing="0">
    <tr class="HeaderBar">
        <td height="35" align="left" nowrap>
            <b><%= Resources.SyslogTrapsWebContent.WEBCODE_VB0_160 %><br>
                <font size="2"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_191 %>
                    <asp:Label runat="server" ID="lblPeriod"></asp:Label></font></b>
        </td>
        <asp:PlaceHolder runat="server" ID="plhCustom" Visible="false">
            <td>
                <tr class="HeaderBar">
                    <td height="35" align="left" nowrap>
                            <table>
                                <tr>
                                    <td>
                                        <font size="2">
                                            <uc1:AdvancedSyslogPeriodSelect ID="ps" runat="server" />
                                        </font>
                                    </td>
                                    <td>
                                        <font size="2">
                                            <asp:Button runat="server" ID="btnGo" Text="<%$ Resources: SyslogTrapsWebContent, WEBDATA_VB0_192 %>" Style=" height: 18px;
                                                color: black; background-color: #fff; border: 1px solid Gray; font-size: 10px;"
                                                OnClick="btnGo_Click" />
                                        </font>
                                    </td>
                                 </tr>
                             </table>
                    </td>
        </asp:PlaceHolder>
        <td>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="right" nowrap>
                        <orion:LocalizableButtonLink ID="LocalizableButtonLink1" runat="server" NavigateUrl='<%# this.HelpLink %>' LocalizedText="CustomText" Text="<%$ Resources: SyslogTrapsWebContent, ResourcesAll_Help %>" DisplayType = "Resource" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<% }%>
<table width="100%" border="0" cellpadding="8" cellspacing="0">
    <tr class="HeaderBar">
        <font size="2"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_203 %> <a href="/Orion/netperfmon/AdvSyslogSummary.aspx?Period=Past Minute&order=<%= this.Order%>"
            target="<%# GetLinkTarget()%>"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_194 %><%= Resources.SyslogTrapsWebContent.String_Separator_1_VB0_59 %></a> <a href="/Orion/netperfmon/AdvSyslogSummary.aspx?Period=Past Hour&oder=<%=this.Order%>"
                target="<%# GetLinkTarget()%>"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_195 %><%= Resources.SyslogTrapsWebContent.String_Separator_1_VB0_59 %></a> <a href="/Orion/netperfmon/AdvSyslogSummary.aspx?Period=Last 24 Hours&order=<%=this.Order%>"
                    target="<%# GetLinkTarget()%>"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_196 %><%= Resources.SyslogTrapsWebContent.String_Separator_1_VB0_59 %></a> <a href="/Orion/netperfmon/AdvSyslogSummary.aspx?Period=Last 7 Days&order=<%=this.Order%>"
                        target="<%# GetLinkTarget()%>"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_197 %><%= Resources.SyslogTrapsWebContent.String_Separator_1_VB0_59 %></a> <a href="/Orion/netperfmon/AdvSyslogSummary.aspx?Period=Last 30 Days&order=<%=this.Order%>"
                            target="<%# GetLinkTarget()%>"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_198 %><%= Resources.SyslogTrapsWebContent.String_Separator_1_VB0_59 %></a> <a href="/Orion/netperfmon/AdvSyslogSummary.aspx?Period=custom&order=<%= this.Order%>"
                                target="<%# GetLinkTarget()%>"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_199 %></a> </font>
    </tr>
</table>
<table width="100%" cellspacing="0" cellpadding="0">
    <tr class="ViewHeader">
        <td class="Property">
            &nbsp;
        </td>
        <td align="Left" style="white-space:nowrap;">
            <%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_207 %>
        </td>
        <td class="Property" align="center" style="white-space:nowrap;">
            <%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_200 %></a>
        </td>
        <td class="Property" style="white-space:nowrap;">
            &nbsp;
        </td>
        <td class="Property" style="white-space:nowrap;">
            <%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_201 %>
        </td>
        <td class="Property" align="center" style="white-space:nowrap;">
            <%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_39 %></a>
        </td>
        <td class="Property">
            &nbsp;  
        </td>
    </tr>
    <asp:Repeater runat="server" ID="MessageRepeater">
        <ItemTemplate>
            <tr>
                <td class="Property">
                    &nbsp;
                </td>
                <td class="Property" nowrap>
                    <a href="/Orion/NetPerfMon/SysLog.aspx?Period=<%# this.qPeriod%>&MessageType=<%# Eval("MessageType")%>&severity=<%# Eval("syslogseverity")%>"
                        target="_blank" rel="noopener noreferrer">
                        <%# Eval("MessageType")%></a>
                </td>
                <td class="Severity<%#Eval("syslogseverity")%>" nowrap align="center" width="5%">
                    <%# GetSeverityType(Eval("syslogseverity"))%>
                </td>
                <td class="Property">
                    &nbsp;
                </td>
                <td class="Property" nowrap align="left" width="5%">
                    <a href="/Orion/NetPerfMon/AdvSysLogWho.aspx?timee=<%# this.Timee%>&MessageType=<%#Eval("MessageType")%>&severity=<%# Eval("syslogseverity")%>"
                        target="_blank" rel="noopener noreferrer"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_204 %></a>
                </td>
                <td class="Property" nowrap align="center" width="5%">
                    <a href="/Orion/NetPerfMon/SysLog.aspx?severity=<%# Eval("syslogseverity")%>&Period=<%# this.qPeriod%>&MessageType=<%# Eval("MessageType")%>"
                        target="_blank" rel="noopener noreferrer"><b>
                            <%# Eval("Total") %></b></a>
                </td>
                <td class="Property">
                    &nbsp;
                </td>
            </tr>
        </ItemTemplate>
    </asp:Repeater>
</table>
<asp:Panel ID="InfoPanel" runat="server" Visible="false">
    <table cellpadding="10px">
        <tr>
            <td style="font-weight: bold; font-size: small; color: Green">
                <%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_146 %>
            </td>
        </tr>
    </table>
</asp:Panel>
