﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ManagementTasksControl.ascx.cs" Inherits="Orion_NetPerfMon_Controls_ManagementTasksControl" %>
<%@ Register TagPrefix="orion" TagName="ManagementTasksRenderer" Src="~/Orion/NetPerfMon/Controls/ManagementTasksControlRenderer.ascx" %>

<orion:ManagementTasksRenderer ID="ManagementTasksRenderer" runat="server" />

<script type="text/javascript">
    $(function() {
        var refresh = function() {
            SW.Core.Services.callWebService(
                "/Orion/Services/AsyncResources.asmx",
                "GetOrionManagementTasks",
                {
                    plugins: <%= ManagementTaskControlsSerialized %>,
                    netObjectId: "<%= NetObject.NetObjectID %>",
                    returnUrl: "<%= ReturnUrl %>"
                },
                function(result) {
                    SW.Core.ManagementTasks
                        .RenderManagementTaskItems(result, "<%= managementTasksPlaceHolder.ClientID %>", <%= ResourceID %>);
                });
        };
        SW.Core.View.AddOnRefresh(refresh, "<%= managementTasksPlaceHolder.ClientID %>");
        refresh();
    });
</script>

<div id="managementTasksPlaceHolder" runat="server">
</div>

<asp:PlaceHolder runat="server" ID="ResourcePlaceHolder" />