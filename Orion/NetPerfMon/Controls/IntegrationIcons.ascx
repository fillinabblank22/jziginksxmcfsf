<%@ Control Language="C#" ClassName="IntegrationIcons" %>
<%@ Import Namespace="SolarWinds.Orion.Common" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL"%>
<%@ Import Namespace="SolarWinds.Orion.NPM.Web" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Web" %>
<script runat="server">
    private int _nodeID = 0;

    public int NodeID
    {
        get { return _nodeID; }
        set { _nodeID = value; }
    }

    private Node _myNode;
    protected Node MyNode
    {
        get
        {
            if (null == this._myNode)
                this._myNode = (Node)NetObjectFactory.Create(string.Format("N:{0}", this.NodeID));
            return this._myNode;
        }
    }

	protected void AddNodeParams(HtmlControl control)
	{
		if (Profile.ToolsetIntegration)
		{
			control.Attributes.Add("IP", MyNode.IPAddress.ToString());
			control.Attributes.Add("NodeHostname", MyNode.Hostname);

			if (SolarWinds.Orion.Core.Common.RegistrySettings.AllowSecureDataOnWeb())
			{
				control.Attributes.Add("Community", MyNode.CommunityString);
			}
			else
			{
                string attrValue = FormatHelper.GetGuidParamString(WebCommunityStringsDAL.GetCommunityStringGuid(MyNode.CommunityString));
				control.Attributes.Add("Community", attrValue);
			}
		}
	}
	
    protected override void OnInit(EventArgs e)
    {
        var isMobileView = Request.QueryString["IsMobileView"];
       
        if (!this.Profile.ToolsetIntegration || 
            (isMobileView != null && isMobileView.ToString().Equals("true", StringComparison.OrdinalIgnoreCase)) ||
            OrionConfiguration.IsDemoServer)
            this.Visible = false;

		AddNodeParams(this.pingTool);
		AddNodeParams(this.traceRouteTool);
		
        base.OnInit(e);
    }
</script>

<script type="text/javascript">
    //<![CDATA[
$(function() {
    var srcReplace = function (img, find, replace) {
        var link = document.createElement('a');
        link.href = img.src;

        //we replace characters not in hostname, but in path to the image
        link.pathname = link.pathname.replace(find, replace);

		img.src = link.href;
	};

	$('img', '#<%=this.iconwrapper.ClientID %>').hover(function () {
		srcReplace(this, '.tb.', '.mo.');
	}, function () {
		srcReplace(this, '.mo.', '.tb.');
	});
});
//]]>
</script>

<span runat="server" id="iconwrapper">
<a href='<%= DefaultSanitizer.SanitizeHtml(string.Format("http://{0}", this.MyNode.HrefIPAddress)) %>'><img src="/Orion/images/ToolsetIntegration/Small.Goto.tb.gif" alt="Web Browse to <%= DefaultSanitizer.SanitizeHtml(this.MyNode.Name) %>" 
         title="Web Browse to <%= DefaultSanitizer.SanitizeHtml(this.MyNode.Name) %>" /></a><a href='<%= DefaultSanitizer.SanitizeHtml(string.Format("ssh://{0}", this.MyNode.HrefIPAddress)) %>'><img src="/Orion/images/ToolsetIntegration/Small.SSH.tb.gif" alt="Open a Secure Shell (SSH) session to <%= DefaultSanitizer.SanitizeHtml(this.MyNode.Name) %>" 
         title="Open a Secure Shell (SSH) session to <%= DefaultSanitizer.SanitizeHtml(this.MyNode.Name) %>" /></a><a href='<%= DefaultSanitizer.SanitizeHtml(string.Format("telnet://{0}", this.MyNode.HrefIPAddress)) %>'><img src="/Orion/images/ToolsetIntegration/Small.Telnet.tb.gif" alt="Open a Telnet session to <%= DefaultSanitizer.SanitizeHtml(this.MyNode.Name) %>" 
         title="Open a Telnet session to <%= DefaultSanitizer.SanitizeHtml(this.MyNode.Name) %>" /></a><a href="" onclick="return false;" ><img runat="server" id="pingTool"
         src="/Orion/images/ToolsetIntegration/Small.Ping.tb.gif" alt="Launch SolarWinds PING tool"
         title="Launch SolarWinds PING tool" onmouseout="status=''; return false;"
         onmouseover="status='Launch SolarWinds PING tool'; return true;"
         onclick="DoAction('SWTOOL:PING', '');"
         /></a><a href="" onclick="return false;" ><img runat="server" id="traceRouteTool"
         src="/Orion/images/ToolsetIntegration/Small.TraceRoute.tb.gif" alt="Launch SolarWinds TraceRoute tool"
         title="Launch SolarWinds TraceRoute tool" onmouseout="status=''; return false;"
         onmouseover="status='Launch SolarWinds TraceRoute tool'; return true;"
         onclick="DoAction('SWTOOL:TraceRoute', '');"
         /></a><a href='<%= DefaultSanitizer.SanitizeHtml(string.Format("/Orion/RemoteDesktop.aspx?Server={0}", this.MyNode.HrefIPAddress)) %>' target="_blank"><img src="/Orion/images/ToolsetIntegration/Small.RDP.tb.gif" alt="Open a Remote Desktop session to <%= DefaultSanitizer.SanitizeHtml(this.MyNode.Name) %>" 
         title="Open a Remote Desktop session to <%= DefaultSanitizer.SanitizeHtml(this.MyNode.Name) %>" /></a>
</span>
