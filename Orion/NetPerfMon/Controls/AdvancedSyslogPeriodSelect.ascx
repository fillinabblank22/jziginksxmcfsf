<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdvancedSyslogPeriodSelect.ascx.cs"
    Inherits="Orion_NetPerfMon_Controls_AdvancedSyslogPeriodSelect" %>

<orion:Include runat="server" File="jquery/jquery.timePicker.js" />

&nbsp;<%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_205 %><asp:TextBox runat="server" ID="tbFromDate" class="datePicker" Style="width: 110px; height: 14px;
    color: black; background-color: #fff; border: 1px solid Gray; font-size: 10px; vertical-align:top;"></asp:TextBox>
<asp:TextBox runat="server" ID="tbFromTime" class="timePicker"  Style="width: 110px; height: 14px;
    color: black; background-color: #fff; border: 1px solid Gray; font-size: 10px; vertical-align:top;"></asp:TextBox>    
&nbsp;<%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_206 %><asp:TextBox runat="server" ID="tbToDate" class="datePicker"  Style="width: 110px; height: 14px;
    color: black; background-color: #fff; border: 1px solid Gray; font-size: 10px; vertical-align:top;"></asp:TextBox>
<asp:TextBox runat="server" ID="tbToTime" class="timePicker"  Style="width: 110px; height: 14px;
    color: black; background-color: #fff; border: 1px solid Gray; font-size: 10px; vertical-align:top;"></asp:TextBox>
