<%@ Control Language="C#" AutoEventWireup="true" Inherits="NetworkMapControl" %>
<%@ Import Namespace="SolarWinds.MapEngine.Helpers" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="SolarWinds.MapEngine.Model" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Caching" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Enums" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Swis" %>
<%@ Import Namespace="SolarWinds.MapEngine" %>
<%@ Import Namespace="SolarWinds.MapStudio.Web" %>
<%@ Register Assembly="OrionWeb" Namespace="SolarWinds.Orion.Web.DAL" TagPrefix="web" %>
<%@ Register TagPrefix="x" TagName="NetworkMapLegend" Src="./NetworkMapLegend.ascx" %>

<style type="text/css">
    div.downloadButton
    {
        text-align:right;
        font-size:smaller;
    }
</style>
    
<asp:ScriptManagerProxy runat="server" ID="ScriptManagerProxy1"><Services><asp:ServiceReference Path="~/Orion/NetPerfMon/MapService.asmx" /></Services></asp:ScriptManagerProxy>

<script runat="server">

    protected override void OnInit(EventArgs e)
    {
        OrionInclude.CoreFile("NetPerfMon/js/networkmapcontrol.js");

        //this is for reports - no AJAX
        if(string.IsNullOrEmpty(Request.QueryString["InlineRendering"]) || Request.QueryString["InlineRendering"] != "TRUE")
        {
            // when inline rendering is not used for web then we call web service to get map data
            if (!UseInlineRenderingForWeb)
            {
                const string mapLoadScript = "MapLoadScript";
                if (!Page.ClientScript.IsClientScriptBlockRegistered(this.GetType(), mapLoadScript))
                {
                    var sbScript = new StringBuilder();
                    sbScript.AppendLine("$(function () {");
                    sbScript.AppendLine("   MapService.set_timeout(180000);");
                    sbScript.AppendLine("   $('.mapIdentifier').each(function (i) {");
                    //mapIdentifier contains string {mapId}|{resourceId}
                    //the resource id is sent to the OnComplete function, so it can update the networkMap-{resourceId} element
                    sbScript.AppendLine("   var identifiers = $(this).val().split('|');");
                    sbScript.AppendLine("   MapService.GetMap(identifiers[1], MapControlOnAjaxComplete, MapControlOnAjaxError, identifiers[2]);");
                    sbScript.AppendLine("});});");

                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), mapLoadScript, sbScript.ToString(), true);

                    utilizationLegend.MapLegend = MapService.GetMapLegend(SolarWinds.MapEngine.Enums.MapStyle.Utilization);
                    speedLegend.MapLegend = MapService.GetMapLegend(SolarWinds.MapEngine.Enums.MapStyle.Topology);
                }
            }
        }
        else
        {
            InlineRendering = true;
        }
        int shortCacheTimeout = 30;
        try
        {
            string cacheTimeoutString = SolarWinds.Orion.Web.DAL.WebSettingsDAL.Get("MapStudio-ShortCacheTimeout");
            if(!string.IsNullOrEmpty(cacheTimeoutString))
            {
                shortCacheTimeout = int.Parse(cacheTimeoutString);
            }
        }
        catch(KeyNotFoundException)
        {}
        ShortCacheTimeout = shortCacheTimeout;
        UseCaching = true;
        string swoisEndpoint = string.Format(ConfigurationManager.AppSettings["SWOISv3.RemoteEndpoint"], "localhost");
        string server, port, postfix;
        MapService.ParseEndpointLocation(swoisEndpoint, out server, out port, out postfix);

        RootLocalPath = MapService.GetTempPath();
        ApplicationLocalPath = MapService.GetMapStudioInstallPath();
        TargetDir = RootLocalPath;
        ServerAddress = server;
        SwisPort = port;
        SwisPostfix = postfix;
        UserName = HttpContext.Current.Profile.UserName;
        AccountId = HttpContext.Current.Profile.UserName;

        var viewInfo = ((ViewInfo)HttpContext.Current.Items[typeof(ViewInfo).Name]);
        ViewId = viewInfo == null ? 0 : viewInfo.ViewID;

        if (UsernameCredentials == null)
        {
            UsernameCredentials = CredentialsHelper.GetOrionCertificateCredentials();
        }

        try
        {
            string tooltipValue;
            if (WebSettingsDAL.TryGetValue("MapStudio-CustomTooltips", out tooltipValue))
            {
                var customTooltipsData = CustomTooltips.DeserializeFromString(tooltipValue)?.Data;
                var encodedTooltips = new CustomTooltips();
                if (customTooltipsData != null)
                {
                    foreach (var customTooltipData in customTooltipsData)
                    {
                        var tooltipData = customTooltipData.Split('|');
                        if (tooltipData.Length > 1)
                        {
                            var entityName = tooltipData[0];
                            var customTooltip = HttpUtility.HtmlEncode(tooltipData[1]);
                            encodedTooltips.Set(entityName, customTooltip);
                        }
                    }
                }
                CustomizedTooltips = encodedTooltips;
            }
            else
            {
                CustomizedTooltips = new CustomTooltips();
            }

        }
        catch (ArgumentNullException)
        {
            CustomizedTooltips = new CustomTooltips();
        }
        catch (KeyNotFoundException)
        {
            CustomizedTooltips = new CustomTooltips();
        }

        base.OnInit(e);
    }


    protected override void OnPreRender(EventArgs e)
    {

        //this is for reports - no AJAX
        if(InlineRendering)
        {
            base.OnPreRender(e);
            var mapService = new MapService();
            string mapHtml = mapService.GetMap(SessionId);
            if (UseInlineRenderingForWeb)
            {
                var mapDivBuilder = new StringBuilder();
                mapDivBuilder.AppendFormat("<div id='networkMap-{0}'>", ResourceId);
                mapDivBuilder.Append(mapHtml);
                mapDivBuilder.Append("</div>");

                var literalControl = new LiteralControl(mapDivBuilder.ToString());
                networkMapContainer.Controls.Add(literalControl);
                AddLegend();
                return;
            }

            string map = MapService.RemoveTooltips(mapHtml);
            networkMapContainer.Controls.Add(new LiteralControl(map));
            AddLegend();
            return;
        }

        if (string.IsNullOrEmpty(ResourceId))
        {
            ResourceId = "0";
        }
        networkMapContainer.Controls.Add(new LiteralControl("<div id=\"networkMap-" + ResourceId + "\"></div>"));

        var mapId = new HtmlInputHidden
        {
            ID = string.Format("mapId-{0}", ResourceId),
            Value = MapId,
            ClientIDMode = ClientIDMode.Static
        };
        Controls.Add(mapId);

        utilizationLegend.SetMapStyleAttributes(SolarWinds.MapEngine.Enums.MapStyle.Utilization, ResourceId);
        speedLegend.SetMapStyleAttributes(SolarWinds.MapEngine.Enums.MapStyle.Topology, ResourceId);

        base.OnPreRender(e);
    }

    /// <summary>
    /// Adds color legend, but just in the case, that wasn't banned on edit of this resource.
    /// </summary>
    private void AddLegend()
    {
        if (ShowColorLegend)
        {
            var mapLegend = MapService.GetMapLegend(MapId);
            if (mapLegend != null)
            {
                utilizationLegend.MapLegend = mapLegend;
                utilizationLegend.ShowLegend();
            }
        }
    }

    protected override void SetDownloadButtonVisibility(bool show)
    {
        downloadButton.Visible = show;
    }

    private ResourcePropertyCollection _properties;

    public ResourcePropertyCollection Properties
    {
        get
        {
            if (null == _properties)
                _properties = new ResourcePropertyCollection(int.Parse(ResourceId));

            return _properties;
        }
    }
</script>

<x:NetworkMapLegend ID="utilizationLegend" ClientIDMode="Static" runat="server" />
<x:NetworkMapLegend ID="speedLegend" ClientIDMode="Static" runat="server" />

<div runat="server" class="sw-text-reset sw-resource-wantedit" id="downloadButton" visible="false" style="background-color:#ecedee; padding: 10px 5px 10px 10px; margin-top: 10px;">	
	<ul>
		<li><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_1) %></li>
		<li><%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBCODE_VB0_103, "<strong>", "</strong>", @"<a href=""/NetworkAtlas/NetworkAtlas.exe"" class=""sw-link"">", "</a>")) %></li>
	</ul>
</div>
