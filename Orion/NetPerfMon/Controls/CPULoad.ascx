<%@ Control Language="C#" ClassName="CPULoad" %>
<%@ Import Namespace="SolarWinds.Orion.NPM.Web" %>

<script runat="server">

    public short Value { get; set; }
    public string PredefinedClassName { get; set; }

    protected string ClassName
    {
        get
        {
            if (!string.IsNullOrEmpty(PredefinedClassName))
                return PredefinedClassName == "Normal" || PredefinedClassName == "Unknown" ? string.Empty : PredefinedClassName;

            if (Thresholds.CPULoadError.SettingValue <= this.Value)
                return "Error";
            else if (Thresholds.CPULoadWarning.SettingValue <= this.Value)
                return "Warning";

            return string.Empty;
        }
    }


</script>

<span class="<%= DefaultSanitizer.SanitizeHtml(this.ClassName) %>"><%= DefaultSanitizer.SanitizeHtml(this.Value) %> %</span>