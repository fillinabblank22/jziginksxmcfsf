﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ManagementTasksControlRenderer.ascx.cs" Inherits="Orion_NetPerfMon_Controls_ManagementTasksControlRenderer" %>

<orion:Include runat="server" File="ManagementTasks.css" />

<script type="text/javascript">
    $(function() {
        var isAutomation = <%= SolarWinds.Orion.Common.OrionConfiguration.IsAutomationTestServer.ToString().ToLower() %>;
	
        SW.Core.namespace('SW.Core.ManagementTasks').RenderManagementTaskItems = function(managementTaskItems, targetElementId, resourceId) {
			var source = $("#managementTasksTemplate-<%= ClientID %>").html();
			var newHtml = _.template(source,
			{
			    TasksWrapper: managementTaskItems, 
			    TargetElementId: targetElementId, 
			    ResourceId: resourceId
			});
			var container = $("#" + targetElementId);
			container.html(newHtml);

			var dropdownLabel = $(".ManagementTaskDropdown > li", container);
			var dropdownContents = $(".ManagementTaskDropdownContent", container);
			var alreadyExistingDropdowns = $("body > .ManagementTaskDropdownContent." + targetElementId);
			
			alreadyExistingDropdowns.remove();
			
			var contentOpenEvent = isAutomation ? 'click' : 'mouseenter';
			var contentCloseEvent = isAutomation ? 'mouseup' : 'mouseleave';

			dropdownLabel
				.each(function() {
					var self = $(this);
					var dropdown = self.find(".ManagementTaskDropdownContent");
					self.data("dropdown", dropdown);
					dropdown.data("label", self);
				});
			dropdownLabel
				.on(contentOpenEvent, function() {
					var self = $(this);
					self.addClass("over");
					var dropdown = self.data("dropdown");
					dropdown.css("left", self.offset().left);
					dropdown.css("top", self.offset().top);
					dropdown.css("paddingTop", self.innerHeight());
					dropdown.css("width", self.innerWidth());
					dropdown.show();
				});
				
			dropdownContents
				.on(contentCloseEvent, function() {
					var self = $(this);
					self.data("label").removeClass("over");
					self.hide();
				});
			dropdownContents.appendTo("body");
        };
    });
</script>

<%-- UnderscoreJS template --%>
<script id="managementTasksTemplate-<%= ClientID %>" type="text/x-template">
    {# _.each(TasksWrapper, function(currentSection, index) { #}
        <div>
            {# if (currentSection.Title) { #}
            <div class="ReportHeader">
                {{currentSection.Title}}
            </div>
            {# } #}
            <div class="NodeManagementIcons ui-helper-clearfix" style="margin-bottom: 7px !important;">
            {# 
            var groupedTasks = _.groupBy(currentSection.Tasks, function(task) {
                return task.Group;
            });

            _.each(groupedTasks, function(currentGroup, index) {
                if(currentGroup[0].Group) {
                    <%-- dropdowns --%>
            #}
                    <ul class="ManagementTaskDropdown">
					    <li class="submenu">
						    <span>{{ currentGroup[0].Group }}</span>
						    <div class="ManagementTaskDropdownContent {{ TargetElementId }}" resourceid="{{ ResourceId }}">
							    <ul>
							        {# _.each(currentGroup, function(currentTask, index) { #}
								        <li>
									        <a id="{{ currentTask.ClientID }}" {{_.isEmpty(currentTask.MouseHoverTooltip) ? '' : 'title="' + currentTask.MouseHoverTooltip + '"'}}href="{{ _.isEmpty(currentTask.LinkUrl) ? 'javascript:;' : currentTask.LinkUrl }}"
                                               onclick="{{ currentTask.OnClick }}">{{ currentTask.LinkInnerHtml }}</a>
								        </li>
							        {# }); #}
							    </ul>
						    </div>
					    </li>
				    </ul>
                {# } else { #}
                    <%-- buttons --%>
                    {# _.each(currentGroup, function(currentTask, index) { #}
                        <a href="{{ _.isEmpty(currentTask.LinkUrl) ? 'javascript:;' : currentTask.LinkUrl }}"               
                            {{ _.isEmpty(currentTask.OnClick) ? '' : 'onclick="' + currentTask.OnClick + '"' }} 
                            {{ currentTask.NewWindow ? 'target="_blank"' : '' }} 
                            {{ _.isEmpty(currentTask.ClientID) ? '' : 'id="' + currentTask.ClientID + '"' }}{{_.isEmpty(currentTask.MouseHoverTooltip) ? '' : 'title="' + currentTask.MouseHoverTooltip + '"'}}>{{ currentTask.LinkInnerHtml }}</a> <span style="visibility:hidden" ></span>
                    {# }); #}
                {# }
            });
            #}
        </div>
        {# }); #}
</script>