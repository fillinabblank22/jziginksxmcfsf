﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_NetPerfMon_Controls_CustomQueryTableFilterDropdown : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Dropdown.Attributes["onChange"] = string.Format("SW.Core.Resources.CustomQuery.FilterDropdown.setFilter({0}, $(this).val());", ResourceID);
    }

    public int ResourceID { get; set; }

    public string SWQL { get; set; }
    
    public string SearchSWQL { get; set; }

    public string FilterForSearchSWQL { get; set; }

    public string FilterForSelectSWQL { get; set; }


    public string Label
    {
        get { return DropdownLabel.Text; }
        set { DropdownLabel.Text = value; }
    }

    public ListItem[] Values
    {
        get { return Dropdown.Items.Cast<ListItem>().ToArray(); }
        set
        {
            Dropdown.Items.Clear();
            Dropdown.Items.AddRange(value);
        }
    }
}