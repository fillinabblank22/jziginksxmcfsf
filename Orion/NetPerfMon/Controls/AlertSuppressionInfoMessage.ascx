<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlertSuppressionInfoMessage.ascx.cs" Inherits="Orion_NetPerfMon_Controls_AlertSuppressionInfoMessage" %>

<div class='sw-suggestion sw-suggestion-info' style="margin-bottom:10px" runat="server" id="alertSuppressionMessage">
    <span class='sw-suggestion-icon'></span>
    <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LH0_26) %></span>
	<a href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("orioncorealertstriggerifmuted")) %>"> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.MutingAlerts_LearnMore) %></a>
</div>
