<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NetworkMapLegend.ascx.cs" Inherits="Orion_NetPerfMon_Controls_NetworkMapLegend" %>
<%@ Import namespace="SolarWinds.Orion.Web.Helpers" %>

<style type="text/css">
    #mapLegendContainer {
        margin-top: 10px;
    }

    #mapLegendContainer span.legendLabel {
        font-family: Arial, Verdana, Helvetica, sans-serif;
        font-size: 8pt;
        padding-left: 3px;
    }

    #mapLegendContainer table {
        width: 108px;
        border: none;
    }

    #mapLegendContainer table td {
        border: none;
    }

    #mapLegendContainer .rect {
        width: 10px;
        height: 10px;
    }

</style>

<span id="mapLegendWrapper" runat="server" ClientIDMode="Static" style="display: none">
<div id="mapLegendContainer" runat="server" ClientIDMode="Static">
    <asp:label id="lblLegendLabel" CssClass="legendLabel" runat="server" /><br />
    <asp:repeater ID="mapLegendItems" runat="server">
        <ItemTemplate>
            <table style="float: left;">
                <tr>
                    <td style="white-space: nowrap; width: 10px;">
                        <div class="rect" style='background-color: <%# DefaultSanitizer.SanitizeHtml(Eval("color")) %>; <%# DefaultSanitizer.SanitizeHtml(Eval("borderstyle")) %>'></div>
                    </td><td style="white-space: nowrap;"><%# DefaultSanitizer.SanitizeHtml(Eval("text")) %></td>
                    <td style="width: 25px;"></td>
                </tr>
            </table>
        </ItemTemplate>
    </asp:repeater>
    
    <div style="clear: both;"></div>
	<span class="legendLabel" runat="server" Id="LblLegend"><a href="<%= DefaultSanitizer.SanitizeHtml(KnowledgebaseHelper.GetKBUrl(5433)) %>" target="_blank" rel="noopener noreferrer" class="sw-link"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_14) %></a></span>
	<div style="clear: both;"></div>
</div>
</span>
