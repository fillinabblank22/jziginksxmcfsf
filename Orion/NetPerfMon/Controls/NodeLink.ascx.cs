using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.NpmAdapters;
using RegistrySettings = SolarWinds.Orion.Core.Common.RegistrySettings;

public partial class Orion_NetPerfMon_Controls_NodeLink : System.Web.UI.UserControl
{
    [PersistenceMode(PersistenceMode.InnerProperty)]
    public PlaceHolder Content
    {
        get { return this.phContents; }
    }

    private string _nodeID;
    protected Node MyNode;

    public string NodeID
    {
        get { return _nodeID; }
        set
        {
            _nodeID = value;
            try
            {
                MyNode = (Node)NetObjectFactory.Create(this.NodeID);
            }
            catch
            {
                this.Visible = false;
            }
        }
    }

	public Node Node
	{
		get { return MyNode; }
		set
		{
			MyNode = value;
			_nodeID = MyNode.NetObjectID;
		}
	}

    [PersistenceMode(PersistenceMode.Attribute)]
    public string TargetURLFormat = "/Orion/NetPerfMon/NodeDetails.aspx?NetObject={0}";

	private bool used = false;
	protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (this.Visible)
        {
			setValues();
        }
    }
	public void setValues() 
	{
		if (used == false && MyNode != null)
		{
			used = true;
			this.lnkNode.HRef = string.Format(this.TargetURLFormat, this.NodeID);

			if (Profile.ToolsetIntegration)
			{
				this.lnkNode.Attributes.Add("IP", MyNode.IPAddress.ToString());
				this.lnkNode.Attributes.Add("NodeHostname", MyNode.Hostname);

				if (RegistrySettings.AllowSecureDataOnWeb())
				{
					this.lnkNode.Attributes.Add("Community", MyNode.CommunityString);
				}
				else
				{
				    string attrValue = FormatHelper.GetGuidParamString(WebCommunityStringsDAL.GetCommunityStringGuid(MyNode.CommunityString));
					this.lnkNode.Attributes.Add("Community", attrValue);
				}
			}

			if (this.phContents.Controls.Count == 0)
			{
                string nodeCaption = MyNode.Name;
			    nodeCaption = NpmAdapterFactory.CreateNodeNameAdapter().AdjustName(nodeCaption, Node.NodeID);

				this.phContents.Controls.Add(new LiteralControl(HttpUtility.HtmlEncode(nodeCaption)));
			}
		}
	}


}
