﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MapThumbnail.ascx.cs" Inherits="Orion_NetPerfMon_Controls_MapThumbnail" %>
<table height="100%" cellpadding="4" runat="server" width="<%= this.TableWidth %>">
    <tr><td class="ViewHeader"><b><%= DefaultSanitizer.SanitizeHtml(MapName) %> </b></td></tr>
    <tr>
        <td valign="middle" align="center">
        <a href="/Orion/NetPerfMon/MapView.aspx?Map=<%= DefaultSanitizer.SanitizeHtml(this.MapId) %>&Title=<%= DefaultSanitizer.SanitizeHtml(Uri.EscapeDataString(MapName)) %>"><img width="<%= this.TableWidth %>" border="0" src="/Orion/NetPerfMon/NetworkMap.aspx?Map=<%= DefaultSanitizer.SanitizeHtml(MapId) %>"/></a>
        </td>
    </tr>
</table>