﻿using System;


public partial class Orion_NetPerfMon_Controls_SortablePageableTable : System.Web.UI.UserControl
{
    /// <summary>
    /// Contains uniqueidentifier of resource (Resource.ID value).
    /// </summary>
    public int UniqueClientID { get; set; }

    public override string ClientID
    {
        get { return "Grid-" + UniqueClientID; }
    }

    /// <summary>
    /// If set to true, a line between rows is added
    /// </summary>
    public bool RowSeparator { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}