<%@ Control Language="C#" ClassName="ThresholdButton" %>

<script runat="server">
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		if (!Profile.AllowAdmin)
			this.Visible = false;
	}
</script>

<orion:LocalizableButtonLink runat="server" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, ResourcesAll_Thresholds %>" DisplayType="Resource" CssClass="EditResourceButton"
    NavigateUrl="/Orion/NetPerfMon/Admin/NetPerfMonSettings.aspx" />
