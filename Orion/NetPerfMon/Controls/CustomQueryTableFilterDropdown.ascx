﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomQueryTableFilterDropdown.ascx.cs" Inherits="Orion_NetPerfMon_Controls_CustomQueryTableFilterDropdown" %>

<orion:Include ID="Include1" runat="server" File="NetPerfMon/Controls/CustomQueryTable.FilterDropdown.js" />

<asp:Label runat="server" AssociatedControlID="Dropdown" ID="DropdownLabel"></asp:Label>
<asp:DropDownList runat="server" ID="Dropdown" />

<script type="text/javascript">
    $(function () {
        SW.Core.Resources.CustomQuery.FilterDropdown.setQuery(<%= ResourceID %>, '<%= SWQL.Replace("'", "\\'") %>');
        SW.Core.Resources.CustomQuery.FilterDropdown.setSearchQuery(<%= ResourceID %>, '<%= SearchSWQL.Replace("'", "\\'") %>');
        SW.Core.Resources.CustomQuery.FilterDropdown.setFilterForSearchQuery(<%= ResourceID %>, '<%= FilterForSearchSWQL.Replace("'", "\\'") %>');
        SW.Core.Resources.CustomQuery.FilterDropdown.setFilterForSelectQuery(<%= ResourceID %>, '<%= FilterForSelectSWQL.Replace("'", "\\'") %>');
    });
</script>
