﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Plugins;

public partial class Orion_NetPerfMon_Controls_ManagementTasksControl : System.Web.UI.UserControl
{
    private static readonly Log _log = new Log();
    private readonly List<string> _loadedPlugins = new List<string>();

    protected string ReturnUrl
    {
        get { return ReferrerRedirectorBase.GetReturnUrl(); }
    }

    protected string ManagementTaskControlsSerialized
    {
        get
        {
            var jsSerializer = new JavaScriptSerializer();
            var serializedList = jsSerializer.Serialize(_loadedPlugins);

            return serializedList;
        }
    }

    public NetObject NetObject { get; set; }

    public int ResourceID { get; set; }

    private void AddManagementTasksControls()
    {
        var pluginsList = OrionModuleManager.GetManagementTaskPlugins();
        if (pluginsList == null || pluginsList.Count == 0)
            return;

        bool isEmpty = true;

        // load plugins and add them to controls collection
        foreach (var pluginControl in pluginsList)
        {
            try
            {
                var ctrl = LoadControl(pluginControl);
                var plugin = ctrl as IManagementTasksPlugin;
                if (plugin == null)
                {
                    _log.ErrorFormat("Plugin '{0}' doesn't implement IManagementTasksPlugin interface.", pluginControl);
                    continue;
                }

                if (!plugin.GetManagementTasks(this.NetObject, this.ReturnUrl).Any())
                {
                    _log.DebugFormat("Plugin '{0}' doesn't contain any management tasks for '{1}'.", pluginControl, NetObject);
                    continue;
                }

                ResourcePlaceHolder.Controls.Add(ctrl);
                _loadedPlugins.Add(pluginControl);
                _log.DebugFormat("Plugin '{0}' was loaded.", pluginControl);
                isEmpty = false;
            }
            catch (Exception ex)
            {
                _log.ErrorFormat("Error when loading plugin '{0}'. Exception: {1}", pluginControl, ex);
            }
        }

        if (isEmpty)
        {
            this.Visible = false;
            _log.Debug("No tasks available, hiding resource.");
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

		//for RenderControl Management resource
		var currentUrl = Request.QueryString["currentUrl"];
		if (currentUrl != null)
        {
			//sideload encoded return URL for ReferrerRedirectorBase
			Context.Items["Redirector_Current"] = currentUrl;
        }
		
        if (ResourceID == 0)
        {
            throw new ApplicationException("ResourceID is not assigned");
        }

        if (NetObject == null)
        {
            throw new ApplicationException("NetObject is not assigned");
        }

        AddManagementTasksControls();
    }
}