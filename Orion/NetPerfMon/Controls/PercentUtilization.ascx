﻿<%@ Control Language="C#" ClassName="ObsoletePercentUtilization" EnableViewState="false" %>

<script runat="server">
     
    private Control _wrappedControl;

    [Obsolete("This control is obsolete and will be removed.  Use /Orion/NPM/Controls/PercentUtilization.ascx ")]
    public short Value
    {
        // Cast _wrappedControl to IPropertyProvider so we can set the value.
        get
        {
            throw new NotImplementedException("This needs to be fixed.");
            //return _wrappedControl.Value;
        }
        set
        {
            throw new NotImplementedException("This needs to be fixed.");
            //_wrappedControl.Value = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        _wrappedControl = LoadControl("~/Orion/NPM/Controls/PercentUtilization.ascx");
        
        if (_wrappedControl != null)
            Controls.Add(_wrappedControl);
    }
    
</script>
