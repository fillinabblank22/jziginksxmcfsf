﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_NetPerfMon_Controls_EntityTooltipCustomizationControl : System.Web.UI.UserControl
{
    public string EntityName { get; set; }
    public string EntityDisplayName { get; set; }

    public string TooltipPattern 
    {
        get
        {
            return tooltipPatternText.Text;
        }
        set
        {
            tooltipPatternText.Text = value;
        }
    }
}
