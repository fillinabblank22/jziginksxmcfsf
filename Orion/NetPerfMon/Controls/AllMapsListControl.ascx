﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="MapsListControl" %>
<%@ Import Namespace="SolarWinds.MapEngine" %>
<%@ Import Namespace="SolarWinds.MapEngine.Helpers" %>
<div id="objectsList" runat="server"></div>
<script runat="server">

    protected override void OnInit(EventArgs e)
    {
        string swoisEndpoint = string.Format(ConfigurationManager.AppSettings["SWOISv3.RemoteEndpoint"], "localhost");
        string server, port, postfix;
        MapService.ParseEndpointLocation(swoisEndpoint, out server, out port, out postfix);

        RootLocalPath = MapService.GetTempPath();
        ApplicationLocalPath = MapService.GetMapStudioInstallPath();
        ServerAddress = server;
        SwisPort = port;
        SwisPostfix = postfix;
        UserName = HttpContext.Current.Profile.UserName;
		ViewId = 0;
        
        if (UsernameCredentials == null)
        {
            UsernameCredentials = CredentialsHelper.GetOrionCertificateCredentials();
        }
       
        base.OnInit(e);
    }

    
    protected override void OnPreRender(EventArgs e)
    {
        objectsList.Controls.Add(GetObjectsTable());
        base.OnPreRender(e);
    }
</script>