﻿<%@ Control Language="C#" Inherits="MapObjectsListControl" %>
<%@ Import Namespace="SolarWinds.MapEngine" %>
<%@ Import Namespace="SolarWinds.MapEngine.Helpers" %>
<%@ Register Assembly="OrionWeb" Namespace="SolarWinds.Orion.Web.DAL" TagPrefix="web" %>
<div id="objectsList" runat="server"></div>
<script runat="server">
    public string ObjectListClientID
    {
        get { return objectsList.ClientID; }
    }    

    protected override void OnInit(EventArgs e)
    {
        string installPath = MapService.GetMapStudioInstallPath();
        string swoisEndpoint = string.Format(ConfigurationManager.AppSettings["SWOISv3.RemoteEndpoint"], "localhost");
        RootLocalPath = MapService.GetTempPath();
        ApplicationLocalPath = installPath;
        
        string server, port, postfix;
        MapService.ParseEndpointLocation(swoisEndpoint, out server, out port, out postfix);
        
        ServerAddress = server;
        SwisPort = port;
        SwisPostfix = postfix;
        UserName = HttpContext.Current.Profile.UserName;
        VendorImagesPath = "/Orion/NetPerfMon/NetworkObjectIcon.aspx?src=";
        StatusImagesPath = "/Orion/NetPerfMon/NetworkObjectIcon.aspx?src=/Orion/Images/StatusIcons/";
        DisableViewLimitations = true;

        var viewInfo = ((ViewInfo)HttpContext.Current.Items[typeof(ViewInfo).Name]);
        ViewId = viewInfo == null ? 0 : viewInfo.ViewID;
        
        if (UsernameCredentials == null)
        {
            UsernameCredentials = CredentialsHelper.GetOrionCertificateCredentials();
        }
       
        base.OnInit(e);
    }

    
    protected override void OnPreRender(EventArgs e)
    {
        if (string.IsNullOrEmpty(MapId))
        {
            objectsList.InnerHtml = GetMapNotAvailableLink();
        }
        else
        {
            objectsList.Controls.Add(GetObjectsTable());
        }
        base.OnPreRender(e);
    }
	
	protected override void OnUnload(EventArgs e)
	{
	    if(objectsList != null)
		{
			if(objectsList.Controls != null)
			{
				objectsList.Controls.Clear();
			}
			objectsList.Dispose();
			objectsList = null;
		}
		base.OnUnload(e);
		Page = null;
	}
</script>
