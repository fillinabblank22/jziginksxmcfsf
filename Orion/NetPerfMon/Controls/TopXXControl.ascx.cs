using System;

using SolarWinds.Orion.Web;

public partial class TopXXControl : System.Web.UI.UserControl
{
    public bool Enabled
    {
        get
        {
            return topXXEnabled.Checked;
        }

        set
        {
            topXXEnabled.Checked = value;
        }
    }

    public String Value
    {
        get
        {
            return topXXValue.SelectedValue;
        }

        set
        {
            topXXValue.SelectedValue = value;
        }
    }

    private void SetupControlFromRequest()
    {
        string resourceID = this.Request["ResourceID"];
        ResourceInfo resource = new ResourceInfo();
        resource.ID = Convert.ToInt32(resourceID);

        if (!String.IsNullOrEmpty(resource.Properties["TopXX"]))
        {
            Enabled = true;
            Value = resource.Properties["TopXX"];
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!IsPostBack)
        {
            SetupControlFromRequest();
        }
    }
}

