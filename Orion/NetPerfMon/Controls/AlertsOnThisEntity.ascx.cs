﻿using System;
using System.Collections.Generic;

public partial class Orion_NetPerfMon_Controls_AlertsOnThisEntity : System.Web.UI.UserControl
{
	public int ResourceID {get; set;}

	public int RelatedNodeId { get; set; }
	public string RelatedNodeEntityUri { get; set; }

    public bool FederationEnabled { get; set; }

    public List<string> TriggeringObjectEntityUris { get; set; }
	public List<string> TriggeringObjectEntityNames { get; set; }

	public bool ShowAcknowledgedAlerts { get; set; }
	public int InitialRowsPerPage { get; set; }

	public Orion_NetPerfMon_Controls_AlertsOnThisEntity()
	{
		TriggeringObjectEntityNames = new List<string>();
		TriggeringObjectEntityUris = new List<string>();
	}

    protected void Page_Load(object sender, EventArgs e)
    {
		SortablePageableTable.UniqueClientID = ResourceID;
    }
}