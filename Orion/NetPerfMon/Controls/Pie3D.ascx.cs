using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;

public partial class Pie3D : System.Web.UI.UserControl
{
	public string TitleTop
	{
		get
		{
			if (ViewState["TitleTop"] == null)
				ViewState["TitleTop"] = string.Empty;
			return (string)ViewState["TitleTop"];
		}
		set
		{
			ViewState["TitleTop"] = value;
		}
	}

	public string TitleBottom
	{
		get
		{
			if (ViewState["TitleBottom"] == null)
				ViewState["TitleBottom"] = string.Empty;
			return (string)ViewState["TitleBottom"];
		}
		set
		{
			ViewState["TitleBottom"] = value;
		}
	}

	public double Value
	{
		get
		{
			if (ViewState["Value"] == null)
				ViewState["Value"] = 0.0;
			return (double)ViewState["Value"];
		}
		set
		{
			ViewState["Value"] = value;
		}
	}

	public bool isWarning
	{
		get
		{
			if (ViewState["isWarning"] == null)
				ViewState["isWarning"] = false;
			return (bool)ViewState["isWarning"];
		}
		set
		{
			ViewState["isWarning"] = value;
		}
	}


    protected void Page_Load(object sender, EventArgs e)
    {
		
		Infragistics.WebUI.UltraWebChart.UltraChart uc = new Infragistics.WebUI.UltraWebChart.UltraChart();
		//uc.ID = "PieChart";
		uc.DeploymentScenario.ImageType = System.Drawing.Imaging.ImageFormat.Jpeg;
		//uc.DeploymentScenario.RenderingType = Infragistics.UltraChart.Shared.Styles.RenderingType.Image;
		uc.DeploymentScenario.Scenario = Infragistics.UltraChart.Shared.Styles.ImageDeploymentScenario.Session;		
		//uc.DeploymentScenario.ImageURL = "/Orion/NetPerfMon/Resources/Gauges/GaugeImage.aspx";

		uc.ImagePipePageName = "/Orion/NetPerfMon/Resources/Gauges/GaugeImage.aspx";
		uc.ColorModel.ColorBegin = Color.CornflowerBlue;// Color.RoyalBlue;
		uc.ColorModel.ColorEnd = Color.FromArgb(194, 194, 194);// "#C2C2C2";// Color.LightGray;
		uc.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.LinearRange;
		uc.Border.Thickness = 0;

		uc.PieChart3D.Labels.Visible = false;
		uc.PieChart3D.PieThickness = 10;
		uc.PieChart3D.RadiusFactor = 100;

		uc.ChartType = Infragistics.UltraChart.Shared.Styles.ChartType.PieChart3D;

		uc.Width = Unit.Parse("170px");
		uc.Height = Unit.Parse("120px");

		uc.TitleTop.Text = TitleTop;
		uc.TitleTop.HorizontalAlign = System.Drawing.StringAlignment.Center;
		uc.TitleTop.Font = new System.Drawing.Font("Microsoft Sans Serif", (float)7.8, System.Drawing.FontStyle.Bold);
		uc.TitleTop.FontColor = isWarning ? Color.Red : Color.Black;

		uc.TitleBottom.Text = TitleBottom;
		uc.TitleBottom.HorizontalAlign = System.Drawing.StringAlignment.Center;
		uc.TitleBottom.Font = new System.Drawing.Font("Microsoft Sans Serif", (float)7.8, System.Drawing.FontStyle.Bold);
		uc.TitleBottom.FontColor = isWarning?Color.Red:Color.Black;

		uc.Transform3D.XRotation = 30;
		uc.Transform3D.YRotation = 90;		
		uc.Transform3D.Scale = 100;
		uc.Transform3D.Perspective = 0;		

		DataTable datatable = new DataTable();
		datatable.Columns.Add("title");
		datatable.Columns.Add("value", typeof(double));
		DataRow dr = datatable.NewRow();
		dr[0] = "1";
		dr[1] = Value;
		datatable.Rows.Add(dr);
		dr = datatable.NewRow();
		dr[0] = "2";
		dr[1] = 100 - Value;
		datatable.Rows.Add(dr);
				
		uc.DataSource = datatable;
		this.Controls.Add(uc);
		uc.DataBind();
    }
}
