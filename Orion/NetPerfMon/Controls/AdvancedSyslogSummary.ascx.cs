using System;
using System.Data;
using System.Web.UI;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SyslogTraps.Web.DAL;

public partial class Orion_NetPerfMon_Controls_AdvancedSyslogSummary : System.Web.UI.UserControl
{
    public string HelpLink
    {
        get
        {
            return HelpHelper.GetHelpUrl("OrionPHResourceSyslogSummary");
        }
    }

    public ResourceInfo Resource
    {
        get
        {
            Control ctrl = this.Parent;
            while (ctrl != null)
            {
                if (ctrl is SolarWinds.Orion.Web.UI.BaseResourceControl)
                    return ((SolarWinds.Orion.Web.UI.BaseResourceControl)ctrl).Resource;

                ctrl = ctrl.Parent;
            }
            return null;
        }
    }

    public string Order
    {
        get
        {
            String s = string.Empty;
            try
            {
                if (Resource != null)
                    s = Resource.Properties["order"];
                else
                    s = Request.QueryString["order"];
            }
            catch { }

            if (string.IsNullOrEmpty(s))
                s = "total desc";

            return s;
        }
    }

    public string Timee
    {
        get
        {
            String s = string.Empty;
            try
            {
                if (Resource != null)
                    s = Resource.Properties["Period"];
                else
                    s = Request.QueryString["Period"];
            }
            catch { }

            if (string.IsNullOrEmpty(s))
                s = "Past Hour";
            return s;
        }
    }

    public string Period
    {
        get
        {
            String s = Timee;
            if (s.Equals("custom", StringComparison.InvariantCultureIgnoreCase))
                return string.Format("From {0} to {1}", ps.dFrom.ToString(), ps.dTo.ToString());

            return s;
        }
    }

    public string qPeriod
    {
        get
        {
            if (Timee.Equals("custom", StringComparison.InvariantCultureIgnoreCase))
                return string.Format("{0}~{1}", ps.dFrom.ToString(), ps.dTo.ToString());

            return Period;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            LoadData();
    }

    private void LoadData()
    {
        var periodBegin = DateTime.Now;
        var periodEnd = DateTime.Now;
        string periodName = Period;

        if (Timee.Equals("custom", StringComparison.InvariantCultureIgnoreCase))
        {
            periodBegin = ps.dFrom;
            periodEnd = ps.dTo;
            plhCustom.Visible = true;
            lblPeriod.Text = string.Format(Resources.SyslogTrapsWebContent.WEBCODE_VB0_174, ps.dFrom.ToString(), ps.dTo.ToString());
        }
        else
        {
            Periods.Parse(ref periodName, ref periodBegin, ref periodEnd);
            lblPeriod.Text = Periods.GetLocalizedPeriod(Period);
        }


        using (WebDAL webdal = new WebDAL())
        {
            DataTable table = webdal.GetSysLogMessagesSummaryForAdvancedParser(Order, periodBegin, periodEnd);

            InfoPanel.Visible = (table == null || table.Rows.Count == 0);

            MessageRepeater.DataSource = table;
            MessageRepeater.DataBind();
        }
        DataBind();
    }

    public string GetNodeLink(string nodeId, string text)
    {
        string s = text;
        if (nodeId != "0")
        {
            s = string.Format(@"<a href=""/netperfmon/View.asp?View=NodeDetails&NetObject=N:{0} target=""_blank""> {1} </a>", nodeId, s);
        }
        return s;
    }

    public string GetLinkTarget()
    {
        if (Resource != null)
            return "_blank";

        return string.Empty;
    }

    public string GetSeverityType(object severity)
    {
        string sseverity = severity == null ? "" : severity.ToString();
        switch (sseverity)
        {
            case "0":
                return Resources.SyslogTrapsWebContent.WEBCODE_VB0_169;
            case "1":
                return Resources.SyslogTrapsWebContent.WEBCODE_VB0_164;
            case "2":
                return Resources.SyslogTrapsWebContent.WEBCODE_VB0_165;
            case "3":
                return Resources.SyslogTrapsWebContent.WEBCODE_AK0_1;
            case "4":
                return Resources.SyslogTrapsWebContent.Status_Warning;
            case "5":
                return Resources.SyslogTrapsWebContent.WEBCODE_VB0_166;
            case "6":
                return Resources.SyslogTrapsWebContent.WEBCODE_VB0_167;
            case "7":
                return Resources.SyslogTrapsWebContent.WEBCODE_VB0_168;
        }
        return string.Empty;
    }

    protected void btnGo_Click(object sender, EventArgs e)
    {
        LoadData();
    }
}
