<%@ Control Language="C#" ClassName="SmallIntegrationIcons" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL"%>
<%@ Import Namespace="SolarWinds.Orion.Core.Web"%>
<%@ Import Namespace="SolarWinds.Orion.NPM.Web" %>

<script runat="server">
    private int _nodeID = 0;

    [PersistenceMode(PersistenceMode.Attribute)]
    public int NodeID
    {
        get { return _nodeID; }
        set { _nodeID = value; }
    }

    private Node _myNode;
    protected Node MyNode
    {
        get
        {
            if (null == this._myNode)
                this._myNode = (Node)NetObjectFactory.Create(string.Format("N:{0}", this.NodeID));
            return this._myNode;
        }
    }

	protected void AddNodeParams(HtmlControl control)
	{
		if (Profile.ToolsetIntegration)
		{
			control.Attributes.Add("IP", MyNode.IPAddress.ToString());
			control.Attributes.Add("NodeHostname", MyNode.Hostname);

			if (SolarWinds.Orion.Core.Common.RegistrySettings.AllowSecureDataOnWeb())
			{
				control.Attributes.Add("Community", MyNode.CommunityString);
			}
			else
			{
                string attrValue = FormatHelper.GetGuidParamString(WebCommunityStringsDAL.GetCommunityStringGuid(MyNode.CommunityString));
				control.Attributes.Add("Community", attrValue);
			}
		}
	}
	
    protected override void OnPreRender(EventArgs e)
    {
        if (!this.Profile.ToolsetIntegration)
            this.Visible = false;

		AddNodeParams(this.pingTool);

        base.OnPreRender(e);
    }
</script>

<a href='<%= DefaultSanitizer.SanitizeHtml(string.Format("http://{0}", this.MyNode.HrefIPAddress)) %>'><img src="/Orion/images/ToolsetIntegration/Small.Goto.gif" alt="Web Browse to <%= DefaultSanitizer.SanitizeHtml(this.MyNode.Name) %>" 
         title="Web Browse to <%= DefaultSanitizer.SanitizeHtml(this.MyNode.Name) %>" /></a><a href='<%= DefaultSanitizer.SanitizeHtml(string.Format("telnet://{0}", this.MyNode.HrefIPAddress)) %>'><img src="/Orion/images/ToolsetIntegration/Small.Telnet.gif" alt="Open a Telnet session to <%= DefaultSanitizer.SanitizeHtml(this.MyNode.Name) %>" 
         title="Open a Telnet session to <%= DefaultSanitizer.SanitizeHtml(this.MyNode.Name) %>" /></a><a href="" onclick="return false;" ><img runat="server" id="pingTool"
         src="/Orion/images/ToolsetIntegration/Small.Ping.gif" alt="Launch SolarWinds PING tool"
         title="Launch SolarWinds PING tool" onmouseout="status=''; return false;"
         onmouseover="status='Launch SolarWinds PING tool'; return true;"
         onclick="DoAction('SWTOOL:PING', '');"
         /></a>
