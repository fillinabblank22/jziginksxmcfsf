using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using System.Xml;
using System.Linq;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Helpers;
using System.Web.UI;
using SolarWinds.Orion.Web.Charting.v2;
using System.Reflection;
using SolarWinds.Orion.NPM.Web;

public partial class CoreListCharts : System.Web.UI.UserControl
{
    private string selectedChart = String.Empty;

    public event EventHandler OnChartChanged;

    public String EntityName
    {
        get
        {
            if (ViewState["EntityName"] == null)
            {
                EntityName = "Orion.Nodes";
            }

            return ViewState["EntityName"].ToString();
        }

        set
        {
            ViewState["EntityName"] = value;
        }
    }
    public String SelectedChart
    {
        get
        {
            return Charts.SelectedValue;
        }

        set
        {
            selectedChart = value;
        }
    }
    public bool EnableAutoPost
    {
        get { return Charts.AutoPostBack; }
        set { Charts.AutoPostBack = value; }
    }
    protected List<CoreEntityChartInfo> ChartList
    {
        get
        {
            if (ViewState["Charts"] == null)
            {
                ChartList = LoadChartsFromConfig();
                if (showNewChart)
                    ChartList.AddRange(LoadChartsV2FromTemplates());
            }

            return (List<CoreEntityChartInfo>)ViewState["Charts"];
        }

        set
        {
            ViewState["Charts"] = value;
        }
    }
    private bool showNewChart = false;
    public bool ShowNewCharts
    {
        get { return showNewChart; }
        set { showNewChart = value; }
    }

	private void SetupControlFromRequest()
	{
		string resourceID = this.Request["ResourceID"];
		ResourceInfo resource = new ResourceInfo();
		resource.ID = Convert.ToInt32(resourceID);

        if (!String.IsNullOrEmpty(resource.Properties["chartname"]) && String.IsNullOrEmpty(SelectedChart))
		{
            if (!String.IsNullOrEmpty(resource.Properties["EmbeddedObjectResource"]) &&
                !String.IsNullOrEmpty(resource.Properties["CustomObjectMode"]))
            {
                int mode;
                if (Int32.TryParse(resource.Properties["CustomObjectMode"], out mode) && mode == (int)OrionChartingHelper.CustomObjectMode.MultiObjectChartsV2)
                {
                    SelectedChart = resource.Properties["chartname"] + ';' + resource.Properties["EmbeddedObjectResource"];
                }
            }
            else
            {
                SelectedChart = resource.Properties["chartname"];
            }
		}
        
        if (!string.IsNullOrEmpty(resource.Properties["entityname"]))
        {
            EntityName = resource.Properties["entityname"];
        }
	}
    public bool ChartV2Selected
    {
        get
        {
            if (!String.IsNullOrEmpty(SelectedChart) && SelectedChart.Split(';').Length > 1)
                return true;

            return false;
        }
    }
	protected override void OnInit(EventArgs e)
	{
        SetupControlFromRequest();
        ReloadList();

        base.OnInit(e);
	}

    public void ReloadList()
    {
        Charts.Items.Clear();

        if (ChartList != null && ChartList.Count > 0)
        {
            foreach (CoreEntityChartInfo info in ChartList)
            {
                if (info.EntityType == EntityName)
                {
                    var item = new ListItem(info.DisplayName, info.ChartName);
                    item.Selected = (info.ChartName == selectedChart);
                    Charts.Items.Add(item);
                }
            }
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (!IsPostBack)
        {
            ReloadList();
        }
    }

    protected List<CoreEntityChartInfo> LoadChartsFromConfig()
    {

        var charts = new List<CoreEntityChartInfo>();

		Dictionary<string, CoreEntityChartInfo> chartInfoDictionary = ChartInfoFactoryPluginManager.GetAvailableChartsInfo();
		foreach (string key in chartInfoDictionary.Keys)
		{
			// we aren't support NPM UnDP charts 
			XmlNode xmlNode = ChartXMLConfigManager.GetXMLConfig(key);
			if (xmlNode != null)
			{
				var pollerTxt = xmlNode.SelectSingleNode("Poller");
				if (pollerTxt != null && pollerTxt.InnerText.Equals("custom", StringComparison.OrdinalIgnoreCase))
					continue;
			}
            charts.Add(chartInfoDictionary[key]);
		}

        return charts;
    }

    protected List<CoreEntityChartInfo> LoadChartsV2FromTemplates()
    {
        var charts = new List<CoreEntityChartInfo>();
        IEnumerable<ResourceTemplateCategory> categories = ResourceTemplateManager.GetAllCategories();

        foreach (ResourceTemplateCategory category in categories)
        {
            foreach (ResourceTemplate template in category.ResourceTemplates)
            {
                if (template.Title != null && template.Title != BaseResourceControl.notAvailable
                    && template.IsInternal == false && template.SupportedInterfaces.Any(i => typeof(IMultiObjectProvider).IsAssignableFrom(i)) &&
                    template.InitialProperties != null && template.InitialProperties.Any(c => c.Key.Equals("ChartName", StringComparison.OrdinalIgnoreCase)))
                {
                    var chartInfo = new CoreEntityChartInfo();
                    chartInfo.ChartName = template.InitialProperties
                        .Single(c => c.Key.Equals("ChartName", StringComparison.OrdinalIgnoreCase))
                        .Value + ";" + template.ControlPath;
                    chartInfo.DisplayName = template.Title;
                    chartInfo.ChartVersion = 2;
                    Control control = LoadControl(template.ControlPath);
                    if (control is StandardChartResource)
                    {
						var chartCnt = (StandardChartResource)control;
                        chartInfo.EntityType = NetObjectFactory.GetSWISTypeByNetObject(chartCnt.ProvidedNetObjectPrefix + ":");
                    };

                    charts.Add(chartInfo);
                }
            }
        }

        return charts;
    }

    protected void SelectedItemChanged(object sender, EventArgs e)
    {
        selectedChart = Charts.SelectedValue;
        
        if (OnChartChanged != null)
            OnChartChanged(sender, e);
    }
}
