using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.Helpers;
using System.Linq;
using System.Web;
using SolarWinds.Orion.Core.Common.InformationService;

public partial class CoreListEntities : System.Web.UI.UserControl
{
    public int MaxEntities { get; set; }
    public String EntityName
    {
        get
        {
            if (ViewState["EntityName"] == null)
            {
                EntityName = String.Empty;
            }
 
            return ViewState["EntityName"].ToString();
        }

        set
        {
            ViewState["EntityName"] = value;
        }
    }
    public String SelectedEntities
    {
        get
        {
            if (ViewState["SelectedEntities"] == null)
            {
                SelectedEntities = String.Empty;
            }

            return ViewState["SelectedEntities"].ToString();
        }

        set
        {
            ViewState["SelectedEntities"] = value;
        }
    }
    public bool ShowFilter
    {
        get
        {
            if (ViewState["ShowFilter"] == null)
            {
                ShowFilter = true;
            }

            return (bool)ViewState["ShowFilter"];
        }

        set
        {
            ViewState["ShowFilter"] = value;

            if (!value)
            {
                FilterSelection = false;
            }
        }
    }
    public bool FilterSelection
    {
        get
        {
            return Filter.Checked;
        }

        set
        {
            Filter.Checked = value;
        }
    }
    public int SelectedEntititesCount
    {
        get
        {
            if (String.IsNullOrEmpty(SelectedEntities))
                return 0;
            else
                return SelectedEntities.Split(',').Count();
        }
    }

    public delegate void SelectObjectChangedHandler();
    public event SelectObjectChangedHandler SelectObjectChanged;

    public class EntityInfo
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string DetailsUrl { get; set; }
    }

	private void SetupControlFromRequest()
	{
		string resourceID = this.Request["ResourceID"];
		ResourceInfo resource = new ResourceInfo();
		resource.ID = Convert.ToInt32(resourceID);
      
        if (!string.IsNullOrEmpty(resource.Properties["EntityName"]))
        {
            EntityName = resource.Properties["EntityName"];
        }

        if (!string.IsNullOrEmpty(resource.Properties["FilterEntities"]))
        {
            Filter.Checked = Boolean.Parse(resource.Properties["FilterEntities"]);
        }

        if (!string.IsNullOrEmpty(resource.Properties["SelectedEntities"]))
        {
            SelectedEntities = resource.Properties["SelectedEntities"];
        }
    }

    private void PersistEntities()
    {
        string resourceID = this.Request["ResourceID"];
        ResourceInfo resource = new ResourceInfo();
        resource.ID = Convert.ToInt32(resourceID);

        if (!string.IsNullOrEmpty(SelectedEntities))
        {
            resource.Properties["SelectedEntities"] = SelectedEntities;
        }
    }

	protected override void OnInit(EventArgs e)
	{
        base.OnInit(e);
       
		SetupControlFromRequest();
	}

    public void ReloadList()
    {
        DataTable entitiesTable = LoadEntities(EntityName, SelectedEntities);

        var entities = new List<EntityInfo>();
        var selectedEntities = new List<string>();

        if (entitiesTable != null && entitiesTable.Rows.Count > 0)
        {
            foreach (DataRow row in entitiesTable.Rows)
            {
                var entity = new EntityInfo()
                {
                    ID = row["ID"].ToString(),
                    Name = HttpUtility.HtmlEncode(GenerateFullName((string[])row["AncestorDisplayNames"])),
                    Status = row["Status"].ToString(),
                    DetailsUrl = row["DetailsUrl"].ToString()
                };

                entities.Add(entity);
                selectedEntities.Add(entity.ID);
            }
        }

        // save because of limitations
        SelectedEntities = String.Join(",", selectedEntities.ToArray());
        PersistEntities();
        EntitiesRepeater.DataSource = entities;
        EntitiesRepeater.DataBind();

        FilterOption.Visible = (ShowFilter && (entities.Count > 0));
    }

    private void DeleteItem(string itemID)
    {
        string[] ids = SelectedEntities.Split(',');
        var newList = new List<string>();

        if (ids != null)
        {
            foreach(string id in ids)
            {
                if(id != itemID)
                    newList.Add(id);
            }

            SelectedEntities = String.Join(",", newList.ToArray());
            if (SelectObjectChanged != null)
            {
                SelectObjectChanged();
            }
            ReloadList();
        }
    }

    protected void ActionItemCommand(object sender, CommandEventArgs e)
    {
        string id = String.Empty;
        if (e.CommandArgument != null)
            id = e.CommandArgument.ToString();

        switch (e.CommandName)
        {
            case "Delete":
                DeleteItem(id);
                break;
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (!IsPostBack)
        {
            ReloadList();
        }
    }

    private string GenerateFullName(string[] names)
    {
        if (names == null)
            throw new ArgumentNullException("names");

        return (string.Join(string.Format(" {0} ", Resources.CoreWebContent.WEBCODE_AK0_113), names));
    }

    protected DataTable LoadEntities(string entityName, string selectedEntities)
    {
        if (String.IsNullOrEmpty(entityName) || String.IsNullOrEmpty(selectedEntities))
        {
            return null;
        }

        string columnID = NetObjectTypeToSWISEntityMapper.GetIdColumn(entityName, false);
        if (String.IsNullOrEmpty(columnID))
            return null;

        string viewId = this.Request.QueryString["ViewID"];
        string limitationString = "";

        if (  !string.IsNullOrEmpty(viewId))
        {
            int viewIdInt;
            if (Int32.TryParse(viewId, out viewIdInt))
            {
                limitationString = "WITH LIMITATION "+ SolarWinds.Orion.Web.ViewManager.GetViewById(viewIdInt).LimitationID;
            }
        }

        
		string orderByField = "DisplayName";
		if (entityName.Equals("Orion.Volumes", StringComparison.OrdinalIgnoreCase) ||
			entityName.Equals("Orion.NPM.Interfaces", StringComparison.OrdinalIgnoreCase))
			orderByField = "AncestorDisplayNames";

        string query = String.Format(@"
SELECT e.{0} AS ID, e.AncestorDisplayNames, e.Status, e.DetailsUrl
FROM {1} e
WHERE e.{0} IN ({2}) ORDER BY e.{3} {4}", columnID, entityName, selectedEntities, orderByField, limitationString);

        DataTable results;
        using (var swis = InformationServiceProxy.CreateV3())
        {
            results = swis.QueryWithAppendedErrors(query);
        }

        return results;
    }
}
