﻿Ext.ns("SW.Orion.GroupRootCause");

SW.Orion.GroupRootCause.Tree = function (pInfo) {
	var mUrl = "/Orion/NetPerfMon/Controls/GroupRootCauseData.aspx";
	var mPanel = null, mCookie = null, mEl = null;

	//helper members
	function populateTree(pLoder, pNode, pData) {
		var rec = null, node = null, KEY_MEMBER = null, KEY_EXPANDED = null;

		var index = pNode.attributes.index;
		if (!index) index = 0;

		for (var i = 0; i < pData.Entities.length; i++) {
			rec = pData.Entities[i], KEY_EXPANDED = "GRC.NA";
			if (rec.Type) {
				KEY_MEMBER = SW.Core.String.Format("{0}{1}{2}{3}", pInfo.groupID, rec.Type, rec.ID, rec.Name);
				KEY_EXPANDED = SW.Core.String.Format("GRC.{0}{1}{2}", pInfo.renderTo, escape(KEY_MEMBER), index);
			}

			node = pLoder.createNode({
				nodeType: rec.HasChildren ? "async" : "node",
				text: (rec.Type ? rec.StatusMessage : rec.Name),
				href: rec.DetailsUrl,
				cls: ((index + i) % 2 == 0 ? "" : "ZebraStripe"),
				icon: rec.StatusIcon,
				iconCls: 'auto-icon-width',
				expanded: mCookie.get(KEY_EXPANDED, "0") == "1",
				leaf: !rec.HasChildren,
				entityId: rec.ID,
				entityType: rec.Type,
				index: index + i + 1
			});
			Ext.apply(node, { "DATA": rec, "EXPANDED": KEY_EXPANDED });
			pNode.appendChild(node);

			if (rec.DetailsUrl == "#showAll") {
				node.addListener('click', function () { showFullBranch(pNode) });
			}
		}
	}

	function showFullBranch(pNode) {
		pInfo.membersCount = -1;

		if (pNode == mPanel.root) {
			mPanel.hide(); mPanel.destroy(); mPanel = null;
			mEl.find("div.grcTree center").show();

			ctor();
		} else {
			pNode.removeAll();
			pNode.reload();
		}
	}

	//event handlers
	function onBeforeLoad(pLoder, pNode, pCallback) {
		this.baseParams =
        {
        	EntityID: ((pNode.attributes.entityId != null) ? pNode.attributes.entityId : pInfo.groupID),
        	EntityType: ((pNode.attributes.entityType != null) ? pNode.attributes.entityType : 'Orion.Groups'),
        	IsRoot: (!pNode.attributes.entityId),
        	ViewID: pInfo.viewID,
        	MembersCount: pInfo.membersCount
        };
	}
	function onLoaded(pLoder, pNode, pResponse) {
		while (pNode.firstChild) {
			pNode.removeChild(pNode.firstChild);
		}
		mEl.find("div.grcTree center").hide();

		eval(SW.Core.String.Format("var data = {0};", pResponse.responseText));
		if (data.Error) {
			onException(pLoder, pNode, { statusText: data.Error });
		} else {
			pNode.expand();
			populateTree(pLoder, pNode, data);

			if (!pNode.attributes.entityId) {
				// root nodes - update status message
				mEl.find("div.grcStatusInfo b").html(data.TotalCount.toString());
				mEl.find("div.grcStatusInfo").show();
			}
		}
	}
	function onException(pLoder, pNode, pResponse) {
		mEl.find("div.grcStatusInfo").remove();
		mEl.find("div.grcTree").html(String.format("<b>{0}</b>", pResponse.statusText));

		mPanel.hide(); mPanel.destroy(); mPanel = null;
	}

	function onNodeExpand(pNode) { mCookie.set(pNode.EXPANDED, "1"); }
	function onNodeCollapse(pNode) { mCookie.clear(pNode.EXPANDED); }

	/*create tree*/
	function ctor() {
		Ext.state.Manager.setProvider((mCookie = new Ext.state.CookieProvider({})));

		mEl = $(pInfo.renderTo);
		mEl.find("div.grcTree").empty();
		mPanel = new Ext.tree.TreePanel({
			renderTo: mEl.find("div.grcTree").get(0), lines: false, autoHeight: true, rootVisible: false, border: false,
			root: new Ext.tree.AsyncTreeNode(),
			animate: true,
			loader: new Ext.tree.TreeLoader({
				dataUrl: mUrl, listeners: { beforeload: onBeforeLoad, load: onLoaded, loadexception: onException }
			}),
			listeners: { collapsenode: onNodeCollapse, expandnode: onNodeExpand }
		});
	} ctor();
}

/*create tree*/
SW.Orion.GroupRootCause.Tree.create = function(pInfo) {
    return new SW.Orion.GroupRootCause.Tree(pInfo);
};
