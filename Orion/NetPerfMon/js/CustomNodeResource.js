﻿$(function() {
    var nodesID = $("#nodesSelectedValue").val();
    var titleID = $("#titleID").val();
    var subtitleID = $("#subtitleID").val();
    var resourcesID = $("#resourcesID").val();
    var selNetObjID = $("#selNetObj").val();
    var selNodeTextID = $("#selNode").val();

    $("#" + resourcesID).change(function() {
        var selNodeID = $("#" + nodesID).val();
        var selNodeName = $("#" + nodesID).find('option').filter(':selected').text();
        $("#" + titleID)[0].value = $.stringFormat("{0} - {1}", [selNodeName, $("#" + resourcesID).find('option').filter(':selected').text()]);
        $("#" + titleID).css({ width: $("#" + titleID).val().length * 6.5 });
    });

    $("#dialogCancel").click(function() {
        $("#selectNodeDialog").dialog('close');
    });

    $("#dialogSelectNode").click(function() {
        var selNodeID = $("#" + nodesID).val();
        $("#" + selNetObjID)[0].value = selNodeID;

        var selNodeName = $("#" + nodesID).find('option').filter(':selected').text();
        $("#" + titleID)[0].value = $.stringFormat("{0} - {1}", [selNodeName, $("#" + resourcesID).find('option').filter(':selected').text()]);
        $("#" + titleID).css({ width: $("#" + titleID).val().length * 6.5 });
        $("#" + selNodeTextID)[0].innerHTML = selNodeName;

        $("#selectNodeDialog").dialog('close');
    });


//    $("#btnSelected").click(function() {
//        $("#selectNodeDialog").show().dialog({
//            width: 850, height: 600, modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: "Select a Node"
//        });
//    });





    jQuery.stringFormat = function(format, arguments) {
        var str = format;
        for (i = 0; i < arguments.length; i++) {
            str = str.replace('{' + i + '}', arguments[i]);
        }
        return str;
    };
    //$.stringFormat("ORDER BY {0}.{1} {2}", [tableAlias, Sort.prop, Sort.ascending ? " ASC" : " DESC"]);
    
});

