var MapControlAddToolTips = function (resourceIdentifier) {

    // CONSTRUCTOR CODE
    if (!String.prototype.format) {
        String.prototype.format = function (params) {
            var pattern = /{([^}]+)}/g;
            return this.replace(pattern, function ($0, $1) { return params[$1]; });
        };
    }
        
    var networkMapDivIdentifier = "#networkMap-" + resourceIdentifier;
    var tooltipDivIndex = 0;
    $(networkMapDivIdentifier + " area").each(function () {

        var tooltipDivId = "mapStudioTooltip" + resourceIdentifier + "-" + tooltipDivIndex;
        var tooltiptext = this.title.slice(this.title.indexOf("|") + 1);

        if (tooltiptext.length > 0) {
           this.title = "";
            
            var tempTooltipText = tooltiptext;
            var tooltipParameters = null;
            var isJson = false;
            try {
                $tooltipText = $('<div/>').append($(tooltiptext));
                tooltiptext = $tooltipText.find("div.NetObjectTipBody").html();
                tooltipParameters = JSON.parse(tooltiptext);
                isJson = true;
            } catch (ex) { }

            if (!isJson) {
                $.swtooltip(this, $("<div id='" + tooltipDivId + "'>" + tempTooltipText + "</div>"));
            }
            else {
                $(this).on("mouseover", function (event) {
                    var elControl = this;
                    var loadingTooltip = $(elControl).data("loadingTooltip");
                    if (loadingTooltip) {
                        return;
                    }
                    
                    $(elControl).data("loadingTooltip", true);
                    var $tooltip = $("<div id='" + tooltipDivId + "'></div>");
                    var loadingTooltipTextTemplate = "<div style='height: 60px; padding-top: 25px;'><center><img src='/Orion/images/AJAX-Loader.gif' />&nbsp;@{R=Core.Strings; K=WEBJS_VB0_1;E=js}</center></div>";
                    $.swtooltip(elControl, $tooltip.append($(tempTooltipText).find("div.NetObjectTipBody").empty().append(loadingTooltipTextTemplate).parent()));
                    $(elControl).trigger("mouseover");
                    
                    var url = "";
                    if (tooltipParameters.TooltipFor != "MapPoint") {
                        url = "/Orion/NetPerfMon/MapEdgeTooltip.aspx";
                    } else {
                        url = "/Orion/NetPerfMon/MapPointTooltip.aspx";
                    }

                    $.ajax({
                        url: url,
                        type: "POST",
                        cache: false,
                        data: tooltipParameters,
                        dataType: "html"
                    }).done(function (returnedData, textStatus, xhr) {
                        $(elControl).data("loadingTooltip", false);
                        if (!returnedData.trim().startsWith("<!DOCTYPE html PUBLIC")) {
                            $(elControl).off(event); // will unbind current event handler
                            var $tempTooltipText = $("<div />").append($(tempTooltipText));
                            $returnedData = $("<div />").append($(returnedData));
                            $returnedDataStatusEl = $returnedData.find("statusinfo");
                            if ($returnedDataStatusEl.length > 0) {
                                $tempTooltipText.find("h3").removeClass();
                                $tempTooltipText.find("h3").addClass($returnedDataStatusEl.attr("status"));
                            }

                            tempTooltipText = $tempTooltipText.find("div.NetObjectTipBody").empty().append($(returnedData)).parent();
                            $tooltip.empty().append(tempTooltipText);
                        } else { // user was most likely log out and we get data for login page
                            var errorTextTemplate = $(tempTooltipText).find("div.NetObjectTipBody").empty().append(String.format('<table style="width:270px;"><tr><td style="height:40px;vertical-align:middle;"><i>{0}</i><td><tr><table>', '@{R=Core.Strings; K=WEBJS_VB1_4; E=js}')).parent();
                            $tooltip.empty().append(errorTextTemplate);
                        }
                       
                    }).fail(function (err) {
                        $(elControl).data("loadingTooltip", false);
                        var $tempTooltipText = $("<div />").append($(tempTooltipText));
                        var errorTextTemplate = $tempTooltipText.find("div.NetObjectTipBody").empty().append(String.format('<table style="width:270px;"><tr><td style="height:40px;vertical-align:middle;"><i>{0}</i><td><tr><table>', '@{R=Core.Strings; K=WEBJS_VB1_4; E=js}')).parent();
                        $tooltip.empty().append(errorTextTemplate);
                    });
                });
            }

            tooltipDivIndex++;
        }
    });
}

var MapControlOnAjaxComplete = function (htmlMapCode, resourceIdentifier) {
    var mapId = $("#mapId-" + resourceIdentifier)[0].value;
    MapService.GetMapLegendStyle(mapId,
        function(mapStyleResult) {
            $("#mapLegendWrapper-" + resourceIdentifier + "-" + mapStyleResult).show();
        });

    var networkMapDivIdentifier = "#networkMap-" + resourceIdentifier;
    $(networkMapDivIdentifier).html(htmlMapCode);
    MapControlAddToolTips(resourceIdentifier);

    $(".cachedMapContainer").each(function (i) {
        if ($(this).attr("id") == "cachedMapContainer-" + resourceIdentifier) {
            $(this).html("");
        }
    });
}

var MapControlOnAjaxError = function (args, args2) {
    $(".cachedMapContainer").each(function (i) {
        if ($(this).attr("id") == "cachedMapContainer-" + args2) {
            $(this).html("");
        }
    }
    );
    if (args.get_timedOut()) {
        $("#networkMap-" + args2).html("@{R=Core.Strings;K=WEBJS_VB0_132; E=js}");
    }
    else {
        $("#networkMap-" + args2).html("@{R=Core.Strings;K=WEBJS_VB0_133; E=js}");
    }
}
