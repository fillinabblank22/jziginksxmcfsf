ORIONDependencies = {};

ORIONDependencies.DependenciesTree = {
    LoadDependencies: function(treeDiv, resourceId, netObj) {
        treeDiv.html("<div>@{R=Core.Strings;K=WEBJS_VB0_1;E=js}</div>");
        DependenciesTree.BuildTree(resourceId, "NT_r" + resourceId, [], netObj, ORIONDependencies.DependenciesTree.Succeeded, ORIONDependencies.DependenciesTree.Failed, treeDiv.get(0));
    },

    Click: function(rootId, resourceId, keys, netObj) {
        return ORIONDependencies.DependenciesTree.HandleClick(rootId, function(contentDiv) {
            DependenciesTree.BuildTree(resourceId, rootId, keys, netObj, ORIONDependencies.DependenciesTree.Succeeded, ORIONDependencies.DependenciesTree.Failed, contentDiv);
        }, function() {
            DependenciesTree.BuildTree(resourceId, "", keys, netObj);
        });
    },

    HandleClick: function(rootId, expandCallback, collapseCallback) {
        var toggleImg = $('#' + rootId + '-toggle');
        var contentsDiv = $('#' + rootId + '-contents');

        if (!contentsDiv.is(':hidden')) {
            contentsDiv.slideUp('fast');
            toggleImg.attr("src", "/Orion/images/Button.Expand.gif");
            collapseCallback();
        } else {
            contentsDiv.html("@{R=Core.Strings;K=WEBJS_VB0_1;E=js}");
            contentsDiv.slideDown('fast');
            toggleImg.attr('src', "/Orion/images/Button.Collapse.gif");
            expandCallback(contentsDiv.get(0));
        }

        return false;
    },

    Succeeded: function(result, context) {
        var contentsDiv = $(context);
        if (typeof $(contentsDiv).get(0) != 'undefined') {
            // html() jquery method is too complex, so this attr operation is for activation of jquery events (NetObjectTips support)
            $('body').attr('_t','').removeAttr('_t'); 
            
            $(contentsDiv).get(0).innerHTML = result;
        }
        
        contentsDiv.slideDown('fast');
    },

    Failed: function(error, context) {
        var contentsDiv = context;
        contentsDiv.innerHTML = "@{R=Core.Strings;K=WEBJS_VB0_2;E=js}" + error.get_message();
    }

};