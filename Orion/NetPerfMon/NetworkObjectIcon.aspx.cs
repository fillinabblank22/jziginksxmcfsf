﻿using System;
using System.Web;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing;

public partial class Orion_NetPerfMon_NetworkObjectIcon : System.Web.UI.Page
{
    const string UnknownImagePath = "~/NetPerfMon/images/Vendors/Unknown.gif";

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Clear();
        Response.ContentType = "image/gif";
        string source = Server.MapPath(UnknownImagePath);
        string request = Request.QueryString["src"];
        string requestMappedPath = string.Empty;

        if (!string.IsNullOrEmpty(request))
        {
            try
            {
                requestMappedPath = Server.MapPath(request); //try virtual path first

                if (File.Exists(requestMappedPath))
                {
                    source = requestMappedPath;
                }
            }
            catch (HttpException)
            {
            }
        } //otherwize - if any error or path invalid source is unknown.

        try
        {
            var image = System.Drawing.Image.FromFile(source);
            image.Save(Response.OutputStream, ImageFormat.Gif);
        }
        catch
        {
            source = UnknownImagePath;
            try
            {
                var image = System.Drawing.Image.FromFile(Server.MapPath(source));
                image.Save(Response.OutputStream, ImageFormat.Gif);
            }
            catch
            {
                Bitmap bmp = new Bitmap(16,16);
                Graphics gr = Graphics.FromImage((System.Drawing.Image)bmp);
                gr.FillRectangle(Brushes.White, new Rectangle(0,0,16,16));
                ((System.Drawing.Image)bmp).Save(Response.OutputStream, ImageFormat.Gif);
                gr.Dispose();
                bmp.Dispose();
            }
        }
    }
}
