using System;
using System.Web.UI;

public partial class PrintableCustomChart : System.Web.UI.Page
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    protected override void OnInit(EventArgs e)
    {
         string controlPath = "~/Orion/NetPerfMon/NPMChart.ascx";
        if (!String.IsNullOrEmpty(Request["CustomPollerID"]))
            controlPath = "~/Orion/NPM/CustomPollerChart.ascx";

        Control control = LoadControl(controlPath);
        control.ID = "customChart";

        holder.Controls.Add(control);

        base.OnInit(e);
       
    }
}
