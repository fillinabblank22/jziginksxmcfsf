﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using System.IO;
using SolarWinds.Orion.Core.Common.Models.Alerts;
using SolarWinds.Orion.Core.Common.Models.Thresholds;
using SolarWinds.Orion.Web.Controllers;
using SolarWinds.Orion.Web.Model.Thresholds;

public partial class Orion_NetPerfMon_NodePopup : System.Web.UI.Page
{
    protected const string CpuLoadName           = "Nodes.Stats.CpuLoad";
    protected const string MemoryUsedName        = "Nodes.Stats.PercentMemoryUsed";
    protected const string ResponseTimeName      = "Nodes.Stats.ResponseTime";
    protected const string PercentLossName       = "Nodes.Stats.PercentLoss";
    
    private string _RevisedStatusDescription;
    private List<CalculatedThresholdValue> _calculatedThresholdValues;

    protected void Page_Load(object sender, EventArgs e)
    {
        // if limitation exists node won't be displayed
        // so, we don't need limitation here
        this.Node = NetObjectFactory.Create(Request["NetObject"], true) as Node;

        _calculatedThresholdValues = new ThresholdsController().GetActualThresholdValues("Orion.Nodes", Node.NodeID);
        
        this.ResponseTime.Value = this.Node.ResponseTime;
        this.ResponseTime.PredefinedClassName = GetControlState(this.Node.ResponseTime, ResponseTimeName);
        SetupBar(this.ResponseTimeBar, this.Node.ResponseTime, 2500, ResponseTimeName);

        this.AvgResponseTime.Value = this.Node.AvgResponseTime;
        this.AvgResponseTime.PredefinedClassName = GetControlState(this.Node.AvgResponseTime, ResponseTimeName);
        SetupBar(this.AvgResponseTimeBar, this.Node.AvgResponseTime, 2500, ResponseTimeName);

        this.MinResponseTime.Value = this.Node.MinResponseTime;
        this.MinResponseTime.PredefinedClassName = GetControlState(this.Node.MinResponseTime, ResponseTimeName);
        SetupBar(this.MinResponseTimeBar, this.Node.MinResponseTime, 2500, ResponseTimeName);

        this.MaxResponseTime.Value = this.Node.MaxResponseTime;
        this.MaxResponseTime.PredefinedClassName = GetControlState(this.Node.MaxResponseTime, ResponseTimeName);
        SetupBar(this.MaxResponseTimeBar, this.Node.MaxResponseTime, 2500, ResponseTimeName);

        this.PercentLoss.Value = (short)this.Node.PercentLoss;
        this.PercentLoss.PredefinedClassName = GetControlState(this.Node.PercentLoss, PercentLossName);       
        SetupBar(this.PercentLossBar, this.Node.PercentLoss, 100, PercentLossName);

        this.CPULoad.Value = this.Node.CPULoad;
        this.CPULoad.PredefinedClassName = GetControlState(this.Node.CPULoad, CpuLoadName);
        SetupBar(this.CPULoadBar, this.Node.CPULoad, 100, CpuLoadName);

        this.MemoryUsed.Value = (short) this.Node.PercentMemoryUsed;
        this.MemoryUsed.PredefinedClassName = GetControlState(this.Node.PercentMemoryUsed, MemoryUsedName);
        SetupBar(this.MemoryUsedBar, this.Node.PercentMemoryUsed, 100, MemoryUsedName);

        var mode = Node.AlertSuppressionState.SuppressionMode;
        AlertsMuted.Visible = mode == EntityAlertSuppressionMode.SuppressedByItself || mode == EntityAlertSuppressionMode.SuppressedByParent;

        foreach (string controlPath in NetObjectFactory.GetNodeTipExtensionControls())
        {
			StringWriter sw = new StringWriter(); 
			HtmlTextWriter writer = new HtmlTextWriter(sw);
			
			Control control = Page.LoadControl(controlPath);
			control.RenderControl(writer);

			if (writer.InnerWriter.ToString().Trim().StartsWith("<tr", StringComparison.OrdinalIgnoreCase))
				extensionPlaceholder.Controls.Add(control);
			else
				oldExtensionPlaceholder.Controls.Add(control);
        }

        SetRevisedStatusDescription(this.Node);

		this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
    }

    private CalculatedThresholdValue GetThresholdValueByName(string name)
    {
        return _calculatedThresholdValues == null
            ? null
            : _calculatedThresholdValues.FirstOrDefault(r => r.ThresholdName == name);
    }

    public bool IsThresholdValuesForBarExist(string name)
    {
        return GetThresholdValueByName(name) != null;
    }

    protected Node Node { get; set; }

    protected string GetVendorIcon()
	{
        string root = "/NetPerfMon/images/Vendors/";
        string icon = this.Node.VendorIcon.Trim();

        if (icon != String.Empty)
        {
            string fullPath = Server.MapPath(root + icon);

            if( System.IO.File.Exists(fullPath) )
            {
                return root + icon;
            }
        }
        return root +  "Unknown.gif";
	}

    public string GetRevisedStatusDescription()
    {
        return HttpUtility.HtmlEncode(_RevisedStatusDescription).Replace(Resources.CoreWebContent.String_Separator_1_VB0_59, (Resources.CoreWebContent.String_Separator_1_VB1_1 + "<br/>"));
    }

    private int? GetViewLimitationID()
    {
        string viewlim = Request.QueryString["viewlim"];
        int lim;

        if (Limitation.GetCurrentViewLimitationID() == 0 &&
            string.IsNullOrEmpty(viewlim) == false &&
            int.TryParse( viewlim, out lim ) )
                return lim;

        return null;
    }

    private void SetRevisedStatusDescription(Node node)
    {
        using (NodeChildStatusDAL dal = new NodeChildStatusDAL())
        {
            if (dal.GetChildStatusDescription(node.NodeID, GetViewLimitationID(), true, out _RevisedStatusDescription) == false )
                _RevisedStatusDescription = node.Description;

            _RevisedStatusDescription = _RevisedStatusDescription ?? "";
        }
    }

    private void SetupBar(InlineBar bar, float value, float max, string name)
    {       
        if (value > max)
            max = value;
        bar.Percentage = 100.0 * value / max;

        var thresholdValue = GetThresholdValueByName(name);

        if (thresholdValue != null)
        {
            if (CompareValues(value, thresholdValue.Critical, thresholdValue.ThresholdOperator))
                bar.ThresholdLevelValue = InlineBar.ThresholdLevel.Error;
            else if (CompareValues(value, thresholdValue.Warning, thresholdValue.ThresholdOperator))
                bar.ThresholdLevelValue = InlineBar.ThresholdLevel.Warning;
            else
                bar.ThresholdLevelValue = InlineBar.ThresholdLevel.Normal;
        }
        else
            bar.ThresholdLevelValue = InlineBar.ThresholdLevel.Unknown;
    }

    private string GetControlState(float value, string name)
    {
        var thresholdValue = GetThresholdValueByName(name);

        if (thresholdValue == null) return "Unknown";

        if (CompareValues(value, thresholdValue.Critical, thresholdValue.ThresholdOperator))
            return "Error";

        if (CompareValues(value, thresholdValue.Warning, thresholdValue.ThresholdOperator))
            return "Warning";

        return "Normal";
    }

    private bool CompareValues(float value, double? rangeValue, ThresholdOperatorEnum oper)
    {
        // check for disabled threshold level
        if (!rangeValue.HasValue || rangeValue.Value == -1)
            return false;

        switch (oper)
        {
            case ThresholdOperatorEnum.Equal:
                return value == rangeValue;
            case ThresholdOperatorEnum.Greater:
                return value > rangeValue;
            case ThresholdOperatorEnum.GreaterOrEqual:
                return value >= rangeValue;
            case ThresholdOperatorEnum.Less:
                return value < rangeValue;
            case ThresholdOperatorEnum.LessOrEqual:
                return value <= rangeValue;
            case ThresholdOperatorEnum.NotEqual:
                return value != rangeValue;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    protected bool HasOverviewStyle(params NodeOverviewStyle[] styles)
	{
		int styleValue;
		return Int32.TryParse(this.Request.QueryString["NodeOverviewStyle"], out styleValue) && styles.Any(s => (int)s == styleValue);
	}
}
