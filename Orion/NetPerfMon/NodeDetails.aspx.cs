using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_NodeDetails : OrionView
{
	private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    private static readonly String ViewNodeSettingValue = "ViewId";
	
	protected override void OnInit(EventArgs e)
	{
		this.Title = this.ViewInfo.ViewTitle;
        this.nodeResHost.Node = (Node)this.NetObject;
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();
        
        TitleDecorators.Controls.Clear();
	    foreach (var decoratorControl in OrionModuleManager.GetNodeDetailsTitleDecorators())
	    {
            Control c = LoadControl(decoratorControl);
            var provider = c as IPropertyProvider;

            if (provider != null)
                provider.Properties["NodeId"] = NetObject.NetObjectID;

            TitleDecorators.Controls.Add(c);
        }
        statusIconControl.StatusLEDImageSrc = nodeResHost.Node.Status.ToString("smallled", null);
        statusIconControl.EntityId = nodeResHost.Node.NodeID.ToString(CultureInfo.InvariantCulture);
        statusIconControl.ViewLimitationId = ViewInfo.LimitationID;
		base.OnInit(e);
	}

    private ViewInfo GetViewFromNodeSettings(int nodeID)
    {
        IDictionary<String, String> nodeSettings = NodeSettingsDAL.GetNodeSettings(nodeID);
        String ViewId = String.Empty;
        if (nodeSettings.TryGetValue(ViewNodeSettingValue, out ViewId))
        {
            return ViewManager.GetViewById(Convert.ToInt32(ViewId));
        }
        return null;
    }

	protected override ViewInfo GetViewForDeviceType()
	{
		if (null != this.NetObject)
		{
		    var viewfromNodeSettings = GetViewFromNodeSettings(((Node) this.NetObject).NodeID);
		    if (viewfromNodeSettings != null)
		        return viewfromNodeSettings;
		    
			int newViewID = ViewsByDeviceTypeDAL.GetViewIDForNodeType(((Node)this.NetObject).MachineType);
			if (newViewID > 0)
			{
				ViewInfo view = ViewManager.GetViewById(newViewID);
				if (view == null)
					log.WarnFormat("ViewID {0} for device type {1} does not exist. Reverting to default.", newViewID, ((Node)this.NetObject).MachineType);
				else
					return view;
			}
		}

        //workaround for FB 25953 --------------------------------------
        //Since the base.GetViewForDeviceType() returns simply the first view of type "NodeDetails", let's try to find "Node Details" view first
		ViewInfoCollection views = ViewManager.GetViewsByType(this.ViewType);
        ViewInfo defaultView = null;
        defaultView = (from v in views
                       where v.ViewKey.ToLower() == "node details"
                       select v).FirstOrDefault();
        //if the "Node Details" view wasn't found, get any view of type "NodeDetails"
        if (defaultView == null)
            defaultView = base.GetDefaultViewForViewType();
        return defaultView;
	}

	public override string ViewType
	{
		get { return "NodeDetails"; }
	}
}
