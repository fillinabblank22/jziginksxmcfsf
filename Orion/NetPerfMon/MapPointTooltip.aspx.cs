﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.Map;

public partial class Orion_NetPerfMon_MapPointTooltip : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MapPointTooltipParams tooltipParams = new MapPointTooltipParams(Request);
        MapPointTooltipControlDefinition tooltipControlDefinition = null;
        if (MapPointTooltipControlRepository.Instance.TryGetControl(tooltipParams.EntityName, out tooltipControlDefinition))
        {
            var tooltipControl = LoadControl(tooltipControlDefinition.ControlPath);
            tooltipContainer.Controls.Add(tooltipControl.TemplateControl);
        }
        else
        {
            var tooltipControl = LoadControl("~/Orion/NetPerfMon/Controls/MapEdgeNoTooltipControlDefined.ascx");
            tooltipContainer.Controls.Add(tooltipControl.TemplateControl);
        }

        if (!string.IsNullOrEmpty(tooltipParams.CustomPartWebTooltip))
        {
            Literal l = new Literal();
            l.Mode = LiteralMode.PassThrough;
            l.Text = tooltipParams.CustomPartWebTooltip;
            tooltipContainer.Controls.Add(l);
        }
    }
}