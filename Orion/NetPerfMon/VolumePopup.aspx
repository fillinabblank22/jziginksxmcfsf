<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VolumePopup.aspx.cs" Inherits="Orion_NetPerfMon_VolumePopup" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DisplayTypes" %>
<%@ Register TagPrefix="orion" TagName="PercentStatusBar" Src="~/Orion/Controls/PercentStatusBar.ascx" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" Assembly="App_Code" %>

<h3 class="Status<%= DefaultSanitizer.SanitizeHtml(((Status)Volume.Status).ToString()) %>">
    <%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Core.Web.FormatHelper.MakeBreakableString(Volume.Name)) %></h3>
<div class="NetObjectTipBody">
   <table cellpadding="0" cellspacing="0">
       <tr>
           <th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_568) %></th>
           <td colspan="2"><%= DefaultSanitizer.SanitizeHtml(Volume.Name) %></td>
       </tr>
       <tr>
           <th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_569) %></th>
           <td><img src="<%= DefaultSanitizer.SanitizeHtml(Volume.IconPath) %>" /></td>
           <td><%= DefaultSanitizer.SanitizeHtml(Volume.VolumeTypeName) %></td>
       </tr>
       <tr>
           <th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_570) %></th>
           <td colspan="2"><%= new Bytes(Volume.TotalSize).ToShort1024String() %></td>
       </tr>
       <tr>
           <th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_571) %></th>
           <td colspan="2"><%= new Bytes(Volume.SpaceUsed).ToShort1024String() %></td>
       </tr>
       <tr>
           <th>&nbsp;</th>
           <td>
               <orion:PercentLabel runat="server" ID="PercentLabel" />
           </td>
           <td>
               <orion:PercentStatusBar runat="server" ID="StatusBar" Show='True' />
           </td>
       </tr>
    </table>
</div>
