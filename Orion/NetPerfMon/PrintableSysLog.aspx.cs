using System;
using System.Web.Security.AntiXss;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_NetPerfMon_PrintableSysLog : System.Web.UI.Page
{
	private static readonly Log log = new Log();
    ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    protected void BusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}

    private string GetLocalizedPeriod
    {
        get
        {
            if(Request.QueryString["Period"].Contains("~"))
            {
                return WebSecurityHelper.SanitizeHtmlV2(AntiXssEncoder.HtmlEncode(Request.QueryString["Period"], true));
            }
            return Periods.GetLocalizedPeriod(WebSecurityHelper.SanitizeHtmlV2(AntiXssEncoder.HtmlEncode(Request.QueryString["Period"], true)));
        }
    }

	public string Title2
	{
		get
		{
			if (string.IsNullOrEmpty(Request.QueryString["NetObject"]))
			{
				if (string.IsNullOrEmpty(Request.QueryString["DeviceType"]))
				{
                    return Resources.SyslogTrapsWebContent.WEBCODE_VB0_178 + " - " + GetLocalizedPeriod;
				}
				else
				{
                    return String.Format(Resources.SyslogTrapsWebContent.WEBCODE_VB0_179, String.Format("<font color='blue'>{0}</font> ", WebSecurityHelper.SanitizeHtmlV2(AntiXssEncoder.HtmlEncode(Request.QueryString["DeviceType"],true)))) + " - " + GetLocalizedPeriod;
				}
			}
			else
			{
                using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
				{
					Node node = proxy.GetNode(Convert.ToInt32(NetObjectHelper.GetObjectID(Request.QueryString["NetObject"])));
                    return Resources.SyslogTrapsWebContent.WEBCODE_VB0_180 + node.Name + " - " + GetLocalizedPeriod;
				}
			}
		}
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!(new RemoteFeatureManager()).IsFeatureEnabled("Syslog"))
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.SyslogTrapsWebContent.WEBCODE_VB0_326), Title = Resources.SyslogTrapsWebContent.WEBCODE_VB0_346 });
	}

	protected override void OnLoadComplete(EventArgs e)
	{
		base.OnLoadComplete(e);
		int maxRecords = 0;
		int.TryParse(Request.QueryString["MaxRecords"], out maxRecords);

		bool showCleared = false;
		bool.TryParse(Request.QueryString["ShowCleared"], out showCleared);

		byte facilityCode;
		byte severityCode;

		//0 - isnt empty facility/severity codes
		if (!byte.TryParse(Request.QueryString["SeverityCode"], out severityCode))
			severityCode = 255;

		if (!byte.TryParse(Request.QueryString["FacilityCode"], out facilityCode))
			facilityCode = 255;

		sysLogsReport.GenerateReport(Request.QueryString["NetObject"],
            WebSecurityHelper.SanitizeHtmlV2(Request.QueryString["DeviceType"]),
			Request.QueryString["Vendor"],
			Request.QueryString["IP"],
			severityCode,
			facilityCode,
			Request.QueryString["MessageType"],
			Request.QueryString["MessagePattern"],
			Request.QueryString["SyslogTag"],
            WebSecurityHelper.SanitizeHtmlV2(Request.QueryString["Period"]),
			maxRecords,
			showCleared,
			Request.QueryString["RelatedIP"],
			Request.QueryString["RelatedMAC"]);
	}

    protected void Page_Init(object sender, EventArgs e)
    {
        SolarWinds.Orion.Web.OrionInclude.CoreThemeStylesheets();
    }
}