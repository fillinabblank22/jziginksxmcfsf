﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_NetPerfMon_Admin_UseAvgOrPeak : System.Web.UI.UserControl
{
    public string MetricName { get; set; }
    public string MyName
    {
        get
        {
            return MetricName.Replace(".","");
        }
    }
    
    public bool UsePeak
    {
        get
        {
            return peakRadio.Checked;
        }
        set
        {
            peakRadio.Checked = value;
            avgRadio.Checked = !value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {        
    }
}