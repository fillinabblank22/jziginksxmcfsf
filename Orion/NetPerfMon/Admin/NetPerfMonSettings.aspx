<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" CodeFile="NetPerfMonSettings.aspx.cs" 
    Inherits="Orion_NetPerfMon_Admin_NetPerfMonSettings" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_85 %>" %>
<%@ Register TagPrefix="NetPerfMon" TagName="ThresholdSetting" Src="~/Orion/NetPerfMon/Admin/ThresholdSetting.ascx" %>
<%@ Register TagPrefix="NetPerfMon" TagName="UseAvgOrPeak" Src="~/Orion/NetPerfMon/Admin/UseAvgOrPeak.ascx" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>


<asp:Content ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton HelpUrlFragment="OrionCoreAGNPMThresholds" ID="helpButton" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <table class="PageHeader HeaderTable" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <h1><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_85) %></h1>
            </td>
            <td align="right">
            </td>
        </tr>
    </table>
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_86) %>
    
    <table cellspacing="0">
        
        <tr>
            <td colspan="4"><h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_VB0_71) %></h2></td>
        </tr>
        <NetPerfMon:ThresholdSetting ID="stgCPULoadError" runat="server" DoRangeValidate="true" MinValue="1" MaxValue="100" AllowDisable="true" />
        <NetPerfMon:ThresholdSetting ID="stgCPULoadWarning" runat="server" DoRangeValidate="true" MinValue="1" MaxValue="100" AllowDisable="true" />    
        <tr>
            <td colspan="4" style="border-bottom: none;">
                <asp:CompareValidator ID="cmpCPULoad" ControlToValidate="stgCPULoadWarning" ControlToCompare="stgCPULoadError" Operator="LessThan" ErrorMessage='<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_PD0_19  %>' 
                    Display="Dynamic" Type="Integer" CssClass="sw-suggestion sw-suggestion-fail sw-capacity-item-validator" runat="server"></asp:CompareValidator>
            </td>
        </tr>    
        <NetPerfMon:UseAvgOrPeak ID="useAvgOrPeakCPU" runat="server" MetricName="Forecast.Metric.CpuLoad" />
        <tr>
            <td colspan="4"><h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_87) %></h2></td>
        </tr>
        <NetPerfMon:ThresholdSetting ID="stgDiskUsageError" runat="server" DoRangeValidate="true" MinValue="1" MaxValue="100" />
        <NetPerfMon:ThresholdSetting ID="stgDiskUsageWarning" runat="server" DoRangeValidate="true" MinValue="1" MaxValue="100" />
        <tr>
            <td colspan="4" style="border-bottom: none;">
                <asp:CompareValidator ID="cmpDiskUsage" ControlToValidate="stgDiskUsageWarning" ControlToCompare="stgDiskUsageError" Operator="LessThan" ErrorMessage='<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_PD0_19  %>' 
                    Display="Dynamic" Type="Integer" CssClass="sw-suggestion sw-suggestion-fail sw-capacity-item-validator" runat="server"></asp:CompareValidator>
            </td>
        </tr>   
        <NetPerfMon:UseAvgOrPeak ID="useAvgOrPeakVolumes" runat="server" MetricName="Forecast.Metric.PercentDiskUsed" />
        <tr>
            <td colspan="4"><h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_224) %></h2></td>
        </tr>
        <NetPerfMon:ThresholdSetting ID="stgPercentMemError" runat="server" DoRangeValidate="true" MinValue="1" MaxValue="100" AllowDisable="true" />
        <NetPerfMon:ThresholdSetting ID="stgPercentMemWarning" runat="server" DoRangeValidate="true" MinValue="1" MaxValue="100" AllowDisable="true" />
        <tr>
            <td colspan="4" style="border-bottom: none;">
                <asp:CompareValidator ID="cmpPercentMem" ControlToValidate="stgPercentMemWarning" ControlToCompare="stgPercentMemError" Operator="LessThan" ErrorMessage='<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_PD0_19  %>' 
                    Display="Dynamic" Type="Integer" CssClass="sw-suggestion sw-suggestion-fail sw-capacity-item-validator" runat="server"></asp:CompareValidator>
            </td>
        </tr>
        <NetPerfMon:UseAvgOrPeak ID="useAvgOrPeakMemory" runat="server" MetricName="Forecast.Metric.PercentMemoryUsed" />
        <tr>
            <td colspan="4"><h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_88) %></h2></td>
        </tr>
        <NetPerfMon:ThresholdSetting ID="stgPacketLossError" runat="server" DoRangeValidate="true" MinValue="1" MaxValue="100" AllowDisable="true" />
        <NetPerfMon:ThresholdSetting ID="stgPacketLossWarning" runat="server" DoRangeValidate="true" MinValue="1" MaxValue="100" AllowDisable="true" />
        <tr>
            <td colspan="4" style="border-bottom: none;">
                <asp:CompareValidator ID="cmpPacketLoss" ControlToValidate="stgPacketLossWarning" ControlToCompare="stgPacketLossError" Operator="LessThan" ErrorMessage='<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_PD0_19  %>' 
                    Display="Dynamic" Type="Integer" CssClass="sw-suggestion sw-suggestion-fail sw-capacity-item-validator" runat="server"></asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td colspan="4"><h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_VB0_78) %></h2></td>
        </tr>
        <NetPerfMon:ThresholdSetting ID="stgResponseTimeError" runat="server" DoRangeValidate="true" MinValue="1" MaxValue="5000" AllowDisable="true" />
        <NetPerfMon:ThresholdSetting ID="stgResponseTimeWarning" runat="server" DoRangeValidate="true" MinValue="1" MaxValue="5000" AllowDisable="true" />        
        <tr>
            <td colspan="4" style="border-bottom: none;">
                <asp:CompareValidator ID="cmpResponseTime" ControlToValidate="stgResponseTimeWarning" ControlToCompare="stgResponseTimeError" Operator="LessThan" ErrorMessage='<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_PD0_19  %>' 
                    Display="Dynamic" Type="Integer" CssClass="sw-suggestion sw-suggestion-fail sw-capacity-item-validator" runat="server"></asp:CompareValidator>
            </td>
        </tr>
    </table>
    <br />
    <div class="sw-btn-bar">
        <orion:LocalizableButton runat="server" ID="imgSubmit" OnClick="SubmitClick" LocalizedText="Submit" automation="Submit" DisplayType="Primary" />
    </div>    
</asp:Content>

<asp:Content ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
<style type="text/css">

.HeaderTable tr td
{
	border-bottom-style: none!important;
}

#adminContent table tr th, #adminContent table tr td
{
	border-bottom: thin solid #dddddd;
	padding-bottom: .1em;
	padding-top: .2em;
}

#adminContent table tr td h2
{
	margin-bottom: 0em;
	padding-bottom: 0em;
}

#adminContent table
{
    width: 100%;
}

#adminContent table tr th, #adminContent table tr td
{
    border-bottom: thin solid #dddddd;
}

#adminContent table tr td
{
    font-size: 8pt;
}

#adminContent table tr th
{
    text-align: left;
    font-size: 9pt;
}

#adminContent table tr th, #adminContent table tr td
{
    white-space: nowrap;
    padding-right: .5em;
    font-size: 8pt;
}

#adminContent table tr td.allowWrap
{
    white-space: normal;
}
</style>
</asp:Content>