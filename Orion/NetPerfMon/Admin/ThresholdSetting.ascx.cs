using Resources;
using SolarWinds.Orion.Core.Common.Models.Thresholds;
using SolarWinds.Orion.Core.Common.Thresholds;
using SolarWinds.Orion.Web.DAL;
using System;
using System.Web.UI;

[ValidationProperty("Value")]
public partial class Orion_NetPerfMon_Admin_ThresholdSetting : System.Web.UI.UserControl
{
    private OrionSetting _setting;

    public bool AllowDisable { get; set; }
    public string MinValue { get; set;}
    public string MaxValue { get; set; }
    /// <summary>
    /// checks the min and max only if this is true
    /// </summary>
    public bool DoRangeValidate { get; set; }

    public string ErrorRangeMessage
    {
        get
        {
            string title = string.Format(CoreWebContent.WEBDATA_LF0_2, MinValue, MaxValue);
            return string.Format("<img src='/Orion/images/Small-Down.gif' title='{0}'/>", title);
        }
    }

    public bool CheckOnSubmit()
    {
        if (this.IsEnabled())
        {
            return true;
        }

        ThesholdRangeValidator.Enabled = false;
        ThesholdRequiredfield.Enabled = false;
        ThesholdRangeValidator.IsValid = true;
        ThesholdRequiredfield.IsValid = true;
        return false;
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (DoRangeValidate)
        {
            ThesholdRangeValidator.ErrorMessage = this.ErrorRangeMessage;
            ThesholdRangeValidator.MinimumValue = this.MinValue;
            ThesholdRangeValidator.MaximumValue = this.MaxValue;
            ThesholdRequiredfield.ErrorMessage = this.ErrorRangeMessage;
        }
    }

    public OrionSetting Setting
    {
        get { return _setting; }
        set 
        {
            _setting = value; 

            string settingType = Setting.Name.Substring(Setting.Name.LastIndexOf('-') + 1).Trim();
            this.phType.Controls.Add(new LiteralControl(settingType));

            string minMax = string.Format(Resources.CoreWebContent.WEBCODE_TM0_25, Setting.Min, Setting.Units.Trim(), Setting.Max);
            this.phMinMax.Controls.Add(new LiteralControl(minMax));

            this.phDesc.Controls.Add(new LiteralControl(Setting.Description));

            this.chbEnable.Attributes.CssStyle.Add(HtmlTextWriterStyle.Display, this.AllowDisable ? "inline" : "none");
            this.chbEnable.Checked = this.AllowDisable == false ||
                ThresholdsHelper.CalculateEnabled(Setting.SettingValue, ThresholdOperatorEnum.GreaterOrEqual);

            this.txtValue.Text = this.IsEnabled() ? Setting.SettingValue.ToString() : string.Empty;
        }
    }

    public string Value
    {
        get
        {
            return this.IsEnabled() ? this.txtValue.Text :
                ThresholdsHelper.FromNumeric(ThresholdsHelper.GetDisabledValue(ThresholdOperatorEnum.GreaterOrEqual));
        }
    }

    private bool IsEnabled() => this.AllowDisable == false || this.chbEnable.Checked;
}
