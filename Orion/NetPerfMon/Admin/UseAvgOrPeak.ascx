<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UseAvgOrPeak.ascx.cs" Inherits="Orion_NetPerfMon_Admin_UseAvgOrPeak" %>

<tr>
<th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LF0_CapacityPlanning) %></th>
<td colspan="3">
<input type="radio" id="avgRadio" name="<%= DefaultSanitizer.SanitizeHtml(MyName) %>" runat="server" style="vertical-align:bottom;" /><span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LF0_CalculateAvg) %></span>
<input type="radio" id="peakRadio" name="<%= DefaultSanitizer.SanitizeHtml(MyName) %>" runat="server" style="vertical-align:bottom;" /><span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LF0_CalculatePeak) %></span>
</td>

</tr>