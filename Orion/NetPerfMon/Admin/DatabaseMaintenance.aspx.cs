﻿using System;


public partial class Orion_NetPerfMon_Admin_DatabaseMaintenance : System.Web.UI.Page
{
	SolarWinds.Data.DatabaseMaintenance.MaintenanceEngine engine;
	protected void Page_Load(object sender, EventArgs e)
	{

	}

	protected void Invoke(object sender, EventArgs e)
	{
		engine = new SolarWinds.Data.DatabaseMaintenance.MaintenanceEngine();
		engine.OnStatusChanged += new EventHandler<SolarWinds.Data.DatabaseMaintenance.StatusChangedEventArgs>(engine_OnStatusChanged);
	}

	void engine_OnStatusChanged(object sender, SolarWinds.Data.DatabaseMaintenance.StatusChangedEventArgs e)
	{
		Response.Write(e.PercentDone);
		//throw new NotImplementedException();
	}

	protected void Timer_Tick(object sender, EventArgs e)
	{
		//null ref exception?
		//Response.Write(p.StandardOutput.ReadLine());				
	}
}
