using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Admin_EditVolumeDetailsView : ProfilePropEditUserControl
{
    public override string PropertyValue
    {
        get
        {
            return this.lbxVolumeDetails.SelectedValue;
        }
        set
        {
            ListItem item = this.lbxVolumeDetails.Items.FindByValue(value);
            if (null != item)
            {
                item.Selected = true;
            }
            else
            {
                // default to "by device type"
                this.lbxVolumeDetails.Items.FindByValue("-1").Selected = true;
            }
        }
    }

    protected override void OnInit(EventArgs e)
    {
        foreach (ViewInfo info in ViewManager.GetViewsByType("VolumeDetails"))
        {
            ListItem item = new ListItem(info.ViewTitle, info.ViewID.ToString());
            this.lbxVolumeDetails.Items.Add(item);
        }

        base.OnInit(e);
    }

    
}
