<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true"
    CodeFile="DatabaseMaintenance.aspx.cs" Inherits="Orion_NetPerfMon_Admin_DatabaseMaintenance"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">

    <script src="../../js/jquery/jquery-progressbar.min.js" type="text/javascript"></script>
    <script src="../../js/jquery/jquery.timer.js" type="text/javascript"></script> 

    <script type="text/javascript">
        //<![CDATA[
   var i = 0;
        $(document).ready(function() {
            $("#progressbar").progressbar();
            $.timer(100, function(timer) {
                updateprogress(i);
                if (i == 100) timer.stop();
            });
        }
         );

        function updateprogress(a) {            
            $("#progressbar").progressbar('progress', i);
            i += 1;
        }
        //]]>
    </script>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <script type="text/javascript">
        //<![CDATA[
        $("#progressbar").progressbar();
        //]]>
    </script>
    
    <div id="progressbar">
    </div>
    <asp:Timer runat="server" ID="Timer1" OnTick="Timer_Tick" Enabled="false">
    </asp:Timer>
    
    <input type="button" value="start" id="start" onclick="<%= DefaultSanitizer.SanitizeHtml(Page.ClientScript.GetPostBackEventReference(lbtnHiddentInvoker, "")) %>" />    
    <asp:LinkButton runat="server" ID="lbtnHiddentInvoker" Text="" OnClick="Invoke"></asp:LinkButton>    
</asp:Content>
