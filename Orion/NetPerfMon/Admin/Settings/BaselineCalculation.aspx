﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NetPerfMon/Admin/Settings/NPMSettings.master"
    AutoEventWireup="true" CodeFile="BaselineCalculation.aspx.cs" Inherits="Orion_NetPerfMon_Admin_Settings_BaselineCalculation_"
    Title="Baseline calculation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <table width="100%" id="breadcrumb">
        <tr>
            <td>
                <a href="../../Admin">Admin</a> &gt; <a href="..">Baseline calculation</a> &gt;
                <h1>
                    <%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
            </td>
            <td align="right" style="vertical-align: top;">
            </td>
        </tr>
    </table>
    <div class="GroupBox">
        Orion will normally determine the current transmission rates
        <br />
        for all interfaces when the application is started. During the baseline calculation,
        <br />
        some Routers' CPU load can spike to 100% for a few seconds.
        <br />
        <br />
        <div class="contentBlock">
            You can disable the Baseline Calculation at startup if you do not want<br />
            your Routers' CPU Load to spike to 100% for a few seconds.<br />
            <asp:RadioButtonList runat="server" ID="rblEnableBaseLineCalculation">
                <asp:ListItem Text="Enable Baseline Calculation when Orion is started"
                    Value="enable"></asp:ListItem>
                <asp:ListItem Text="Disable Baseline Calculation at startup. Instead, schedule the Baseline Calculation during the first 10 to 15 minutes in order to minimize Router CPU Load."
                    Value="disable"></asp:ListItem>
            </asp:RadioButtonList>
        </div>
    </div>
    <asp:Button runat="server" ID="btnSave" Text="Save" OnClick="btnSave_Click" />
    <asp:Button runat="server" ID="btnCancel" Text="Cancel" />
</asp:Content>
