﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_NetPerfMon_Admin_NPM_Statistics : System.Web.UI.Page
{
	public OrionSetting DefaultNodeStatPollInterval;
	public OrionSetting DefaultInterfaceStatPollInterval;
	public OrionSetting DefaultVolumeStatPollInterval;	


	protected void Page_Load(object sender, EventArgs e)
	{
		Initialize();
	}

	protected void btnSave_Click(object sender, EventArgs e)
	{
		if (!string.IsNullOrEmpty(hfDefaultNodeStatPollInterval.Value))
			SettingsDAL.SetValue("NetPerfMon-DefaultNodeStatPollInterval", Convert.ToDouble(hfDefaultNodeStatPollInterval.Value));
		if (!string.IsNullOrEmpty(hfDefaultInterfaceStatPollInterval.Value))
			SettingsDAL.SetValue("NetPerfMon-DefaultInterfaceStatPollInterval", Convert.ToDouble(hfDefaultInterfaceStatPollInterval.Value));
		if (!string.IsNullOrEmpty(hfDefaultVolumeStatPollInterval.Value))
			SettingsDAL.SetValue("NetPerfMon-DefaultVolumeStatPollInterval", Convert.ToDouble(hfDefaultVolumeStatPollInterval.Value));
		
		Response.Redirect("../NPMSettings.aspx");
	}

	private void Initialize()
	{
		DefaultNodeStatPollInterval = SettingsDAL.GetSetting("NetPerfMon-DefaultNodeStatPollInterval");
		DefaultInterfaceStatPollInterval = SettingsDAL.GetSetting("NetPerfMon-DefaultInterfaceStatPollInterval");
		DefaultVolumeStatPollInterval = SettingsDAL.GetSetting("NetPerfMon-DefaultVolumeStatPollInterval");	
	}
}
