﻿<%@ Page Language="C#" MasterPageFile="~/Orion/NetPerfMon/Admin/Settings/NPMSettings.master"
    AutoEventWireup="true" CodeFile="Statistics.aspx.cs" Inherits="Orion_NetPerfMon_Admin_NPM_Statistics"
    Title="Statistics" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <table width="100%" id="breadcrumb">
        <tr>
            <td>
                <a href="../../Admin">Admin</a> &gt; <a href="..">Statistics</a> &gt;
                <h1>
                    <%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
            </td>
            <td align="right" style="vertical-align: top;">
            </td>
        </tr>
    </table>

    <script type="text/javascript">
    //<![CDATA[
    
        $(document).ready(function()
        {
        var prefix = "ctl00_ctl00_ContentPlaceHolder1_adminContentPlaceholder_";
            
            $("#dDefaultNodeStatPollInterval").slider({
                stepping: 1,
                min: <%=this.DefaultNodeStatPollInterval.Min %>,
                max: <%=this.DefaultNodeStatPollInterval.Max %>,
                startValue: <%= this.DefaultNodeStatPollInterval.SettingValue %>,
                change: function(e, ui)
                {                              
                    var obj = $("#" + prefix + "dDefaultNodeStatPollInterval");
                    var obj1 = $("#" + prefix + "hDefaultNodeStatPollInterval");
                    obj.text(ui.value);
                    obj1.val(ui.value);
                }
            });                              
            
             $("#dDefaultInterfaceStatPollInterval").slider({
                stepping: 1,
                min: <%=this.DefaultInterfaceStatPollInterval.Min %>,
                max: <%=this.DefaultInterfaceStatPollInterval.Max %>,
                startValue: <%=this.DefaultInterfaceStatPollInterval.SettingValue %>,
                change: function(e, ui)
                {                    
                    var obj = $("#" + prefix + "dDefaultInterfaceStatPollInterval");
                    var obj1 = $("#" + prefix + "hDefaultInterfaceStatPollInterval");
                    obj.text(ui.value);
                    obj1.val(ui.value);
                }
            });
            
             $("#dDefaultVolumeStatPollInterval").slider({
                stepping: 1,
                min: <%=this.DefaultVolumeStatPollInterval.Min %> ,
                max: <%=this.DefaultVolumeStatPollInterval.Max %> ,
                startValue: <%=this.DefaultVolumeStatPollInterval.SettingValue %>,
                change: function(e, ui)
                {                    
                   var obj = $("#" + prefix + "dDefaultVolumeStatPollInterval");
                   var obj1 = $("#" + prefix + "hfDefaultVolumeStatPollInterval");
                   obj.text(ui.value);
                    obj1.val(ui.value);
                }
            });           
        });       
    //]]>    
    </script>

    These settings apply to new nodes, interfaces, and volumes as they are added.
    <br />
    <table>
        <tr>
            <td>
                Default poll interval for Nodes
            </td>
            <td>
                Check response time and status of each node every <span id="dDefaultNodeStatPollInterval"
                    runat="server">
                    <%= DefaultSanitizer.SanitizeHtml(this.DefaultNodeStatPollInterval.SettingValue) %>
                </span>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.DefaultNodeStatPollInterval.Units) %>
                <div id='dDefaultNodeStatPollInterval' class='ui-slider-1' style="margin: 0px;">
                    <div class='ui-slider-handle'>
                    </div>
                </div>
                <asp:HiddenField ID="hfDefaultNodeStatPollInterval" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                Default poll interval for Interfaces
            </td>
            <td>
                Check the status of each Interface every <span id="dDefaultInterfaceStatPollInterval"
                    runat="server">
                    <%= DefaultSanitizer.SanitizeHtml(this.DefaultInterfaceStatPollInterval.SettingValue) %>
                </span>&nbsp;
                <%= DefaultSanitizer.SanitizeHtml(this.DefaultInterfaceStatPollInterval.Units) %>
                <div id='dDefaultInterfaceStatPollInterval' class='ui-slider-1' style="margin: 0px;">
                    <div class='ui-slider-handle'>
                    </div>
                </div>
                <asp:HiddenField ID="hfDefaultInterfaceStatPollInterval" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                Default poll interval for Volumes
            </td>
            <td>
                Check the status of each Volume every <span id="dDefaultVolumeStatPollInterval" runat="server">
                    <%= DefaultSanitizer.SanitizeHtml(this.DefaultVolumeStatPollInterval.SettingValue) %>
                </span>&nbsp;
                <%= DefaultSanitizer.SanitizeHtml(this.DefaultVolumeStatPollInterval.Units) %>
                <div id='dDefaultVolumeStatPollInterval' class='ui-slider-1' style="margin: 0px;">
                    <div class='ui-slider-handle'>
                    </div>
                </div>
                <asp:HiddenField ID="hfDefaultVolumeStatPollInterval" runat="server" />
            </td>
        </tr>        
        <br />
        <br />
    </table>
    <asp:Button Text="Apply these settings to All" ID="btnApplyToAll" runat="server" />
    <br />
    <br />
    <asp:Button runat="server" ID="btnSave" Text="Save" OnClick="btnSave_Click" />
</asp:Content>
