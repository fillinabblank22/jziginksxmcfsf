<%@ Page Language="C#" MasterPageFile="~/Orion/NetPerfMon/Admin/Settings/NPMSettings.master"
    AutoEventWireup="true" CodeFile="Polling.aspx.cs" Inherits="Orion_NetPerfMon_Admin_NPM_Polling"
    Title="Polling" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <table width="100%" id="breadcrumb">
        <tr>
            <td>
                <a href="../../Admin">Admin</a> &gt; <a href="..">Polling</a> &gt;
                <h1>
                    <%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
            </td>
            <td align="right" style="vertical-align: top;">
            </td>
        </tr>
    </table>

    <script type="text/javascript">
    //<![CDATA[
    
        $(document).ready(function()
        {
        var prefix = "ctl00_ctl00_ContentPlaceHolder1_adminContentPlaceholder_";
            
            $("#dDefaultNodePollInterval").slider({
                stepping: 1,
                min: <%=this.DefaultNodePollInterval.Min %>,
                max: <%=this.DefaultNodePollInterval.Max %>,
                startValue: <%= this.DefaultNodePollInterval.SettingValue %>,
                change: function(e, ui)
                {                              
                    var obj = $("#" + prefix + "dDefaultNodePollInterval");
                    var obj1 = $("#" + prefix + "hDefaultNodePollInterval");
                    obj.text(ui.value);
                    obj1.val(ui.value);
                }
            });                              
            
             $("#dDefaultInterfacePollInterval").slider({
                stepping: 1,
                min: <%=this.DefaultInterfacePollInterval.Min %>,
                max: <%=this.DefaultInterfacePollInterval.Max %>,
                startValue: <%=this.DefaultInterfacePollInterval.SettingValue %>,
                change: function(e, ui)
                {                    
                    var obj = $("#" + prefix + "dDefaultInterfacePollInterval");
                    var obj1 = $("#" + prefix + "hDefaultInterfacePollInterval");
                    obj.text(ui.value);
                    obj1.val(ui.value);
                }
            });
            
             $("#dDefaultVolumePollInterval").slider({
                stepping: 1,
                min: <%=this.DefaultVolumePollInterval.Min %> ,
                max: <%=this.DefaultVolumePollInterval.Max %> ,
                startValue: <%=this.DefaultVolumePollInterval.SettingValue %>,
                change: function(e, ui)
                {                    
                   var obj = $("#" + prefix + "dDefaultVolumePollInterval");
                   var obj1 = $("#" + prefix + "hfDefaultVolumePollInterval");
                   obj.text(ui.value);
                    obj1.val(ui.value);
                }
            });
            
            $("#dDefaultRediscoveryInterval").slider({
                stepping: 1,
                min: <%=this.DefaultRediscoveryInterval.Min %>,
                max: <%=this.DefaultRediscoveryInterval.Max %>,
                startValue: <%=this.DefaultRediscoveryInterval.SettingValue %>,
                change: function(e, ui)
                {                    
                    var obj = $("#" + prefix + "dDefaultRediscoveryInterval");
                    var obj1 = $("#" + prefix + "hfDefaultRediscoveryInterval");
                    obj.text(ui.value);
                     obj1.val(ui.value);
                }
            });
        });       
    //]]>    
    </script>

    These settings apply to new nodes, interfaces, and volumes as they are added.
    <br />
    <table>
        <tr>
            <td>
                Default poll interval for Nodes
            </td>
            <td>
                Check response time and status of each node every <span id="dDefaultNodePollInterval"
                    runat="server">
                    <%= DefaultSanitizer.SanitizeHtml(this.DefaultNodePollInterval.SettingValue) %>
                </span>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.DefaultNodePollInterval.Units) %>
                <div id='dDefaultNodePollInterval' class='ui-slider-1' style="margin: 0px;">
                    <div class='ui-slider-handle'>
                    </div>
                </div>
                <asp:HiddenField ID="hfDefaultNodePollInterval" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                Default poll interval for Interfaces
            </td>
            <td>
            Check the status of each Interface every 
                <span id="dDefaultInterfacePollInterval" runat="server">
                    <%= DefaultSanitizer.SanitizeHtml(this.DefaultInterfacePollInterval.SettingValue) %>
                </span>&nbsp;
                <%= DefaultSanitizer.SanitizeHtml(this.DefaultInterfacePollInterval.Units) %>
                <div id='dDefaultInterfacePollInterval' class='ui-slider-1' style="margin: 0px;">
                    <div class='ui-slider-handle'>
                    </div>
                </div>
                <asp:HiddenField ID="hfDefaultInterfacePollInterval" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                Default poll interval for Volumes
            </td>
            <td>
                Check the status of each Volume every 
                <span id="dDefaultVolumePollInterval" runat="server">
                    <%= DefaultSanitizer.SanitizeHtml(this.DefaultVolumePollInterval.SettingValue) %>
                </span>&nbsp;
                <%= DefaultSanitizer.SanitizeHtml(this.DefaultVolumePollInterval.Units) %>
                <div id='dDefaultVolumePollInterval' class='ui-slider-1' style="margin: 0px;">
                    <div class='ui-slider-handle'>
                    </div>
                </div>
                <asp:HiddenField ID="hfDefaultVolumePollInterval" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Rediscover Resources
            </td>
            <td>Redisciver each Node, Interface, and Volume every
                 <span id="dDefaultRediscoveryInterval" runat="server">
                    <%= DefaultSanitizer.SanitizeHtml(this.DefaultRediscoveryInterval.SettingValue) %>
                </span>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.DefaultRediscoveryInterval.Units) %>
                <div id='dDefaultRediscoveryInterval' class='ui-slider-1' style="margin: 0px;">
                    <div class='ui-slider-handle'>
                    </div>
                </div>
                <asp:HiddenField ID="hfDefaultRediscoveryInterval" runat="server" />
            </td>
        </tr>
        <br />
        <br />
    </table>
    <asp:Button Text="Apply these settings to All" ID="btnApplyToAll" runat="server" />
    <br />
    <br />
    <asp:Button runat="server" ID="btnSave" Text="Save" OnClick="btnSave_Click" />
    <asp:Button runat="server" ID="btnCancel" Text="Cancel" OnClick="btnCancel_Click" />
</asp:Content>
