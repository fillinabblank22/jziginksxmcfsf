﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_NetPerfMon_Admin_NPM_Polling : System.Web.UI.Page
{
	public OrionSetting DefaultNodePollInterval;
	public OrionSetting DefaultInterfacePollInterval;
	public OrionSetting DefaultVolumePollInterval;
	public OrionSetting DefaultRediscoveryInterval;


	protected void Page_Load(object sender, EventArgs e)
	{
		Initialize();
	}

	protected void btnSave_Click(object sender, EventArgs e)
	{
		if (!string.IsNullOrEmpty(hfDefaultNodePollInterval.Value))
			SettingsDAL.SetValue("NetPerfMon-DefaultNodePollInterval", Convert.ToDouble(hfDefaultNodePollInterval.Value));
		if (!string.IsNullOrEmpty(hfDefaultInterfacePollInterval.Value))
			SettingsDAL.SetValue("NetPerfMon-DefaultInterfacePollInterval", Convert.ToDouble(hfDefaultInterfacePollInterval.Value));
		if (!string.IsNullOrEmpty(hfDefaultVolumePollInterval.Value))
			SettingsDAL.SetValue("NetPerfMon-DefaultVolumePollInterval", Convert.ToDouble(hfDefaultVolumePollInterval.Value));
		if (!string.IsNullOrEmpty(hfDefaultRediscoveryInterval.Value))
			SettingsDAL.SetValue("NetPerfMon-DefaultRediscoveryInterval", Convert.ToDouble(hfDefaultRediscoveryInterval.Value));

		Response.Redirect("../NPMSettings.aspx");
	}

	protected void btnCancel_Click(object sender, EventArgs e)
	{
		Response.Redirect("../NPMSettings.aspx");
	}

	private void Initialize()
	{
		DefaultNodePollInterval = SettingsDAL.GetSetting("NetPerfMon-DefaultNodePollInterval");
		DefaultInterfacePollInterval = SettingsDAL.GetSetting("NetPerfMon-DefaultInterfacePollInterval");
		DefaultVolumePollInterval = SettingsDAL.GetSetting("NetPerfMon-DefaultVolumePollInterval");
		DefaultRediscoveryInterval = SettingsDAL.GetSetting("NetPerfMon-DefaultRediscoveryInterval");
	}
}

