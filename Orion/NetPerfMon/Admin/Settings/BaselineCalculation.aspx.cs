﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_NetPerfMon_Admin_Settings_BaselineCalculation_ : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		Initalize();
	}

	private void Initalize()
	{
		bool enabled = Convert.ToBoolean(SettingsDAL.GetSetting("NetPerfMon-BaselineCalculation").SettingValue);

		rblEnableBaseLineCalculation.SelectedIndex = (enabled ? 0 : 1);
	}

	protected void btnSave_Click(object sender, EventArgs e)
	{
		SettingsDAL.SetValue("NetPerfMon-BaselineCalculation", Convert.ToDouble(!Convert.ToBoolean(rblEnableBaseLineCalculation.SelectedIndex)));
	}
}
