<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ThresholdSetting.ascx.cs" Inherits="Orion_NetPerfMon_Admin_ThresholdSetting" %>

<script type="text/javascript">
$(function () {
    function syncFields() {
        var checked = $('#<%= chbEnable.ClientID %>')[0].checked, tbId = '<%= txtValue.ClientID %>', textbox = $('#' + tbId);
        //checked ? textbox.show() : textbox.hide();
        checked ? textbox.removeAttr("disabled") : textbox.attr("disabled", true);
        $.each(Page_Validators, function (i, v) { if (v["controltovalidate"] === tbId) { ValidatorEnable(v, checked); } });
    }
    $('#<%= chbEnable.ClientID %>').click(syncFields);
    $(document).ready(syncFields);
});
</script>

<tr>
    <th><asp:CheckBox runat="server" ID="chbEnable" automation="chbEnable" /><asp:PlaceHolder runat="server" ID="phType" /></th>
    <td class="Property"><asp:TextBox runat="server" ID="txtValue" Columns="6" automation="txtValue" /><%if (this.DoRangeValidate)
      {%>
    
    <asp:RangeValidator ID="ThesholdRangeValidator" runat="server" ControlToValidate="txtValue" MinimumValue="0" MaximumValue="2000000000" Type="Integer" Display="Dynamic"></asp:RangeValidator>
    <asp:RequiredFieldValidator runat="server" ID="ThesholdRequiredfield" ControlToValidate="txtValue"></asp:RequiredFieldValidator>
    <% } %>   </td>
    <td><asp:PlaceHolder runat="server" ID="phMinMax" /></td>
    <td class="allowWrap"><asp:PlaceHolder runat="server" ID="phDesc" /></td>

</tr>

