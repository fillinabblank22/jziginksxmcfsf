<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditNodeDetailsView.ascx.cs" Inherits="Orion_NetPerfMon_Admin_EditNodeDetailsView" %>

<asp:ListBox runat="server" ID="lbxNodeDetails" SelectionMode="single" Rows="1" >
    <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_SO0_9 %>" Value="0" />
    <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_487 %>" Value="-1" />
    
</asp:ListBox>
<%-- More ListItems will be populated at run-time --%>