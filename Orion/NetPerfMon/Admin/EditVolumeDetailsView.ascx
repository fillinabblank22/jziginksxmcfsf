<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditVolumeDetailsView.ascx.cs" Inherits="Orion_NetPerfMon_Admin_EditVolumeDetailsView" %>

<asp:ListBox runat="server" ID="lbxVolumeDetails" SelectionMode="single" Rows="1">
    <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_SO0_9 %>" Value="0" />
    <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_487 %>" Value="-1" />
</asp:ListBox>
<%--More list items added at runtime--%>