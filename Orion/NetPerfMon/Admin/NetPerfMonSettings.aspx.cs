using System;

using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using ThresholdSetting = Orion_NetPerfMon_Admin_ThresholdSetting;

public partial class Orion_NetPerfMon_Admin_NetPerfMonSettings : System.Web.UI.Page
{
    protected override void OnLoad(EventArgs e)
    {
        ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
        base.OnLoad(e);
    }

    protected override void OnInit(EventArgs e)
    {
        var allPeakSettings = ForecastingCapacityDAL.GetAllUsePeakValues();

        useAvgOrPeakCPU.UsePeak = allPeakSettings[useAvgOrPeakCPU.MetricName];
        useAvgOrPeakMemory.UsePeak = allPeakSettings[useAvgOrPeakMemory.MetricName];
        useAvgOrPeakVolumes.UsePeak = allPeakSettings[useAvgOrPeakVolumes.MetricName];

        stgCPULoadError.Setting = Thresholds.CPULoadError;
        stgCPULoadWarning.Setting = Thresholds.CPULoadWarning;
        stgDiskUsageError.Setting = Thresholds.DiskSpaceError;
        stgDiskUsageWarning.Setting = Thresholds.DiskSpaceWarning;
        stgPercentMemError.Setting = Thresholds.PercentMemoryError;
        stgPercentMemWarning.Setting = Thresholds.PercentMemoryWarning;
        stgPacketLossError.Setting = Thresholds.PacketLossError;
        stgPacketLossWarning.Setting = Thresholds.PacketLossWarning;
        stgResponseTimeError.Setting = Thresholds.ResponseTimeError;
        stgResponseTimeWarning.Setting = Thresholds.ResponseTimeWarning;
        
        base.OnInit(e);
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        bool validated = true;
        validated &= CheckOnSubmit(cmpCPULoad, stgCPULoadError, stgCPULoadWarning);
        validated &= CheckOnSubmit(cmpDiskUsage, stgDiskUsageError, stgDiskUsageWarning);
        validated &= CheckOnSubmit(cmpPercentMem, stgPercentMemError, stgPercentMemWarning);
        validated &= CheckOnSubmit(cmpPacketLoss, stgPacketLossError, stgPacketLossWarning);
        validated &= CheckOnSubmit(cmpResponseTime, stgResponseTimeError, stgResponseTimeWarning);
        if (!validated) Page.Validate();

        if (!Page.IsValid) return;

        Dictionary<string, bool> dict = new Dictionary<string, bool>();
        dict.Add(useAvgOrPeakCPU.MetricName, useAvgOrPeakCPU.UsePeak);
        dict.Add(useAvgOrPeakMemory.MetricName, useAvgOrPeakMemory.UsePeak);
        dict.Add(useAvgOrPeakVolumes.MetricName, useAvgOrPeakVolumes.UsePeak);
        ForecastingCapacityDAL.SetAllUsePeakValues(dict);

        stgCPULoadError.Setting.SaveSetting(Convert.ToDouble(stgCPULoadError.Value));
        stgCPULoadWarning.Setting.SaveSetting(Convert.ToDouble(stgCPULoadWarning.Value));
        stgDiskUsageError.Setting.SaveSetting(Convert.ToDouble(stgDiskUsageError.Value));
        stgDiskUsageWarning.Setting.SaveSetting(Convert.ToDouble(stgDiskUsageWarning.Value));
        stgPercentMemError.Setting.SaveSetting(Convert.ToDouble(stgPercentMemError.Value));
        stgPercentMemWarning.Setting.SaveSetting(Convert.ToDouble(stgPercentMemWarning.Value));
        stgPacketLossError.Setting.SaveSetting(Convert.ToDouble(stgPacketLossError.Value));
        stgPacketLossWarning.Setting.SaveSetting(Convert.ToDouble(stgPacketLossWarning.Value));
        stgResponseTimeError.Setting.SaveSetting(Convert.ToDouble(stgResponseTimeError.Value));
        stgResponseTimeWarning.Setting.SaveSetting(Convert.ToDouble(stgResponseTimeWarning.Value));

        ReferrerRedirectorBase.Return("/Orion/Admin/");
    }

    private bool CheckOnSubmit(CompareValidator cmp, ThresholdSetting error, ThresholdSetting warning)
    {
        cmp.Enabled = true;
        cmp.Enabled &= error.CheckOnSubmit();
        cmp.Enabled &= warning.CheckOnSubmit();
        return cmp.Enabled;
    }
}
