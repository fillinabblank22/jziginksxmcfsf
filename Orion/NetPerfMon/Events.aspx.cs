using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Caching;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Core.Common.Federation;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.Models.Events;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.Controls;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Federation;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.Web.UI;

using SettingsDAL = SolarWinds.Orion.Web.DAL.SettingsDAL;

public partial class Orion_NetPerfMon_Events : System.Web.UI.Page
{
	#region private members

	private const string MaxEventPerPageDefault = "250";
    private const string OrionDetailViewUrl = "/Orion/View.aspx?NetObject={0}:{1}";

    protected string validatorText = String.Format("<img src=\"/Orion/images/Small-Down.gif\" title=\"{0}\" />",Resources.CoreWebContent.WEBDATA_VB0_220);

	private static readonly Log _log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
    private List<SwisErrorMessage>  errors = new List<SwisErrorMessage>();

	protected void BusinessLayerExceptionHandler(Exception ex)
	{
		_log.Error(ex);
	}

	/// <summary>
	/// is current page called for sort
	/// </summary>
	private bool IsSort;

	/// <summary>
	/// null - first page load
	/// true - postback, sort
	/// false - postback, other user action
	/// </summary>
	private bool CameFromSort
	{
		get
		{
			object val = ViewState["CameFromSort"];
			return (val != null && val is bool) ? (bool)val : false;
		}
		set { this.ViewState["CameFromSort"] = value; }
	}
	#endregion

	#region Properties and ViewState Properties

	// this property contains string to idetify by what to sort
	public string SortString
	{
		get
		{
			object val = this.ViewState["sortString"];
			if (val != null && val is string)
			{
				return (string)val;
			}
			return "Time";
		}
		set
		{
			this.ViewState["sortString"] = value;
		}
	}

	// this property contains sorting direction
	public SortDirection SortDir
	{
		get
		{
			object val = this.ViewState["sortingDirection"];
			if (val != null && val is SortDirection)
			{
				return (SortDirection)val;
			}
			return SortDirection.Ascending;
		}
		set
		{
			this.ViewState["sortingDirection"] = value;
		}
	}

	// event data table
	public DataTable EventsTable
	{
		get
		{
			object val = this.ViewState["eventsTable"];
			if (val != null && val is DataTable)
			{
				return (DataTable)val;
			}
			return new DataTable();
		}
		set
		{
			this.ViewState["eventsTable"] = value;
		}
	}

	// event types
	public Dictionary<string, int> EventTypes
	{
		get
		{
			object val = this.ViewState["EventTypes"];
			if (val != null && val is Dictionary<string, int>)
			{
				return (Dictionary<string, int>)val;
			}
			return new Dictionary<string, int>();
		}
		set
		{
			this.ViewState["EventTypes"] = value;
		}
	}

	private string GetLocalizedPeriod
	{
		get
		{
			if (Period.Contains("~"))
			{
				return Period;
			}
			return Periods.GetLocalizedPeriod(Period);
		}
	}

	public string Period
	{
		get
		{
			if (pickerTimePeriodControl.TimePeriodText == "Custom")
			{
				if (string.IsNullOrEmpty(pickerTimePeriodControl.CustomPeriodBegin))
				{
					pickerTimePeriodControl.CustomPeriodBegin = "0:00:00";
				}
				if (string.IsNullOrEmpty(pickerTimePeriodControl.CustomPeriodEnd))
				{
					pickerTimePeriodControl.CustomPeriodEnd = "23:59:59";
				}

				return pickerTimePeriodControl.CustomPeriodBegin + "~" + pickerTimePeriodControl.CustomPeriodEnd;
			}
			else
			{
				return pickerTimePeriodControl.TimePeriodText;
			}
		}
	}

	public bool FederationEnabled
	{
		get { return SwisFederationInfo.IsFederationEnabled; }
	}

    #endregion

	#region Data

	// sql access
	private void ReloadData()
	{
		string id = netObjects.NetObjectID;
	    if (!String.IsNullOrEmpty(netObjects.NetObjectID)) id = id.Substring(2);
	    else id = "-1";

	    DateTime periodBegin = new DateTime();
	    DateTime periodEnd = new DateTime();
	    string period = Period;
	    Periods.Parse(ref period, ref periodBegin, ref periodEnd);
	    string netObjectType;
	    int netObjectId;

	    // FB11014, allows to filter events by interface or other net object type
	    // has the side effect, when we press 'refresh' it will find all events belongin to the node
	    if (!String.IsNullOrEmpty(Request.QueryString["NetObject"]) && CameFromSort)
	    {
	        var arr = Request.QueryString["NetObject"].Split(':');
	        netObjectType = arr[0];
	        netObjectId = Int32.Parse(arr[1]);
	    }
	    else
	    {
	        netObjectType = "N";
	        netObjectId = Int32.Parse(id);
	    }

	    var param = new GetEventsParameter()
	    {
	        NetObjectId = netObjectId,
	        NetObjectType = netObjectType,
	        DeviceType = this.netObjectTypes.DeviceType,
	        EventType = EventTypes[eventTypes.SelectedItem.Value],
	        FromDate = DateTime.SpecifyKind(periodBegin, DateTimeKind.Local),
	        ToDate = DateTime.SpecifyKind(periodEnd, DateTimeKind.Local),
	        IncludeAcknowledged = this.showClearedEvents.Checked,
	        MaxRecords = Int32.Parse(this.maxRecords.Text),
	        SiteId = this.SolarwindsServersDropDown.SelectedValue
	    };
	    
        this.EventsTable = EventDAL.GetEventsTable(param);
        this.errors.AddRange(SwisDataTableParser.GetDataTableErrors(this.EventsTable));
        this.errors.AddRange(this.netObjectTypes.SwisErrors);
        this.EventsTable.Columns.Add("URL");

	    String objectType;
	    Int64 objectID;
	    foreach (DataRow r in EventsTable.Rows)
	    {
	        objectType = (r["NetObjectType"] ?? String.Empty).ToString().Trim().ToUpperInvariant();
	        objectType = objectType.Equals("Y") ? "AM" : objectType.Equals("Z") ? "AA" : objectType;

	        Int64.TryParse((r["NetObjectID"] ?? String.Empty).ToString().Trim(), out objectID);

	        if (objectType == String.Empty || (objectID == 0 && r["NetObjectValue"] == DBNull.Value))
	        {
	            r["URL"] = String.Empty;
	        }
	        else
	        {
	            string netObjectValue = r["NetObjectValue"] != DBNull.Value &&
	                                    !String.IsNullOrEmpty((string) r["NetObjectValue"])
	                ? (string) r["NetObjectValue"]
	                : objectID.ToString();

	            r["URL"] = FederationUrlHelper.GetLinkPrefix((int) r["OrionSiteID"]) + String.Format(OrionDetailViewUrl, objectType, netObjectValue);
	        }


	        int color = (int) r["BackColor"];
	        r["BackColor"] = ((color & 0xFF0000) >> 16) + (color & 0x00FF00) + ((color & 0x0000FF) << 16);
	    }
	}

    // bind data to repeater
	private void PublishData()
	{
		EventsGrid.DataSource = EventsTable;
		EventsGrid.DataBind();
	}

	// update events list
	private void PopulateChildControls()
	{
	    string deviceTitle;
	    if (string.IsNullOrEmpty(netObjects.NetObjectID))
		{
			if (string.IsNullOrEmpty(netObjectTypes.DeviceType))
			{
				deviceTitle = Resources.CoreWebContent.WEBCODE_VB0_200;
			}
			else
			{
				deviceTitle = "<span style=\"color:blue;\">" + netObjectTypes.DeviceType + "</span>";
			}
		}
		else
		{
            using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
			{
				Node node = proxy.GetNode(Convert.ToInt32(NetObjectHelper.GetObjectID(netObjects.NetObjectID)));

				deviceTitle = string.Format("<span style=\"color:blue;\">{0}</span>", HttpUtility.HtmlEncode(node.Name));
			}
		}

        this.lbTitle.Text = string.Format(Resources.CoreWebContent.WEBDATA_VB0_241, deviceTitle); 

		this.TitlePeriod.Text = " - " + GetLocalizedPeriod;

		// validating of max event number per page
		int _maxRecords = 0;
		if (!Int32.TryParse(maxRecords.Text, out _maxRecords))
		{
			_maxRecords = 0;
		}
		else
		{
			_maxRecords = Math.Min(Math.Max(_maxRecords, 0), 2000);
		}

		maxRecords.Text = _maxRecords.ToString();

		// if value has changed, send it to database
		if (maxRecords.Text.Trim() != previousMaxRecords.Value)
		{
			WebUserSettingsDAL.Set("Web-EventMaxMsgPerPage", _maxRecords.ToString());
			previousMaxRecords.Value = _maxRecords.ToString();
		}

		bool printable;
		if ((Request["Printable"] != null) && bool.TryParse(Request["Printable"], out printable))
		{
			if (printable)
			{
				ShowPrintableVersion();
			}
		}

		ReloadData();
		SortOnPage(SortString, SortDir);
		PublishData();

		ExportToPDFLink.PageUrl = GetUrlWithParams(Request.Url.OriginalString.Split('?')[0]);
		this.DuplicateEventActions.Visible = this.Profile.AllowEventClear
			&& (this.EventsGrid.Items.Count >= CommonConstants.MIN_ROWS_NUMBER);
	}

	#endregion

	#region Sorting

	// on page sorting
	private void SortOnPage(string column, SortDirection direction)
	{
        string sortD = " ASC";
        if (direction == SortDirection.Descending) sortD = " DESC";

        if (column == "Message") EventsTable.DefaultView.Sort = EventsTable.Columns["Message"] + sortD;
        else if (column == "OrionSiteName") EventsTable.DefaultView.Sort = EventsTable.Columns["OrionSiteName"] + sortD;
        else EventsTable.DefaultView.Sort = EventsTable.Columns["EventTime"] + sortD;
	}

	// if sort buttons are clicked, sorting direction or order column are changed
    protected void SortChange(object sender, EventArgs e)
    {
        if (sender as LinkButton == null) return;

        //set flag to define it's a sort
        IsSort = true;

        string ID = (sender as LinkButton).ID;

        if (ID == "SortByTime")
        {
            if (SortString == "Time") SortDir = (SortDir == SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending);
            else
            {
                SortString = "Time";
                SortDir = SortDirection.Ascending;
                this.TimeDirImage.Visible = true;
                this.MessageDirImage.Visible = false;
                this.SolarwindsServerDirImage.Visible = false;
            }
        }
        else if (ID == "SortByMessages")
        {
            if (SortString == "Message") SortDir = (SortDir == SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending);
            else
            {
                SortString = "Message";
                SortDir = SortDirection.Ascending;
                this.TimeDirImage.Visible = false;
                this.MessageDirImage.Visible = true;
                this.SolarwindsServerDirImage.Visible = false;
            }
        }
        else if (ID == "SortBySolarwindsServer")
        {
            if (SortString == "OrionSiteName") SortDir = (SortDir == SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending);
            else
            {
                SortString = "OrionSiteName";
                SortDir = SortDirection.Ascending;
                this.TimeDirImage.Visible = false;
                this.MessageDirImage.Visible = false;
                this.SolarwindsServerDirImage.Visible = true;
            }
        }

        SetSortImages();

        SortOnPage(SortString, SortDir);
        PublishData();
    }

    private void SetSortImages()
	{
		// seting up images for sorting direction
		string img = (SortDir == SortDirection.Ascending ? "~/Orion/images/sortable_arrow_up.gif" : "~/Orion/images/sortable_arrow_down.gif");
        if (SortString == "Time") { MessageDirImage.Visible = false; TimeDirImage.Visible = true; SolarwindsServerDirImage.Visible = false;TimeDirImage.ImageUrl = img; }
        if (SortString == "Message") { MessageDirImage.Visible = true; TimeDirImage.Visible = false; SolarwindsServerDirImage.Visible = false; MessageDirImage.ImageUrl = img; }
        if (SortString == "OrionSiteName") { MessageDirImage.Visible = false; TimeDirImage.Visible = false; SolarwindsServerDirImage.Visible = true; SolarwindsServerDirImage.ImageUrl = img; }
	}
	#endregion

	#region Events

    protected override void OnInit(EventArgs e)
    {
        maxRecords.ValidatorText = validatorText;
        base.OnInit(e);
    }

	protected void Page_Load(object sender, EventArgs e)
	{
	    this.SolarWindsServerFilterRow.Visible = FederationEnabled;
		if (!IsPostBack)
		{
		    SortDir = SortDirection.Descending;

		    EventTypes = new Dictionary<string, int>();

		    string maxMSG = Request.QueryString["show"];
		    if (String.IsNullOrEmpty(maxMSG))
		        maxMSG = WebUserSettingsDAL.Get("Web-EventMaxMsgPerPage");

		    if (String.IsNullOrEmpty(maxMSG))
		    {
		        // if the setting does not exist, try to get default value
		        string maxPerPageDefaultDBSettings = SettingsDAL.GetSetting("Web-MaxEvents").SettingValue.ToString();

		        if (String.IsNullOrEmpty(maxPerPageDefaultDBSettings))
		        {
		            maxRecords.Text = MaxEventPerPageDefault;
		        }
		        else
		        {
		            maxRecords.Text = maxPerPageDefaultDBSettings;
		        }

		        WebUserSettingsDAL.Set("Web-EventMaxMsgPerPage", maxRecords.Text);
		    }
		    else
		    {
		        maxRecords.Text = maxMSG;
		    }

		    // synchronize
		    previousMaxRecords.Value = maxRecords.Text;

		    // generate the list of events
		    DataTable eventTypesTable = EventDAL.GetEventTypesTable(true);
            this.errors.AddRange(SwisDataTableParser.GetDataTableErrors(eventTypesTable));
		    eventTypes.Items.Add(new ListItem(Resources.CoreWebContent.WEBCODE_VB0_201, "All events"));
		    EventTypes.Add("All events", 0);
		    foreach (DataRow r in eventTypesTable.Rows)
		    {
		        eventTypes.Items.Add(r["Name"].ToString());
		        try
		        {
		            EventTypes.Add(r["Name"].ToString(), Int32.Parse(r["EventType"].ToString()));
		        }
		        catch
		        {
		        }
		    }
		    
            InitializeSolarWindsServers();
		}

	    this.EventActions.Visible = this.Profile.AllowEventClear;
	    this.helpButton.HelpUrlFragment = "OrionPHViewEvents";
	}

    private void InitializeSolarWindsServers()
    {
        if (!FederationEnabled)
            return;

        SolarwindsServersDropDown.Items.Clear();
        SolarwindsServersDropDown.Items.Add(new ListItem(Resources.CoreWebContent.Events_FilterServersSolarwindsServerDefaultValue, string.Empty));

        foreach (var site in new OrionSitesDAL().GetSites())
        {
            if (!site.Enabled)
                continue;

            SolarwindsServersDropDown.Items.Add(new ListItem(site.Name, site.SiteID.ToString()));
        }
    }

    // Regenerate list of events
    protected void Refresh_Click(object sender, EventArgs e)
	{

	}

	protected override void OnLoadComplete(EventArgs e)
	{
		base.OnLoadComplete(e);

		if (!IsPostBack)
		{
			if (!String.IsNullOrEmpty(Request.QueryString["NetObject"]))
			{
				// select net object from argument
				NetObject netObject = NetObjectFactory.Create(Request.QueryString["NetObject"]);

				if (netObject != null && netObject.ObjectProperties.ContainsKey("NodeID"))
				{
					// change netObject to Node type (Events page supports nodes only)
					netObjects.NetObjectID = String.Format("N:{0}", netObject.ObjectProperties["NodeID"]);
				}
			}

			if (!string.IsNullOrEmpty(Request.QueryString["DeviceType"]))
				netObjectTypes.DeviceType = Request.QueryString["DeviceType"];

			// select event type
			int eventTypeFromQS = 0;
			try
			{
				eventTypeFromQS = Int32.Parse(Request.QueryString["EventType"]);
			}
			catch { eventTypeFromQS = -1; }

			if (eventTypeFromQS > 0)
			{
				string selectedEventType = String.Empty;
				foreach (KeyValuePair<string, int> kvp in EventTypes)
					if (kvp.Value == eventTypeFromQS)
						selectedEventType = kvp.Key;

				if (!String.IsNullOrEmpty(selectedEventType))
					eventTypes.SelectedValue = selectedEventType;
			}

			string showAcknowledged = Request.QueryString["ShowClearedEvents"];

			bool showAck = false;
			if (!string.IsNullOrEmpty(showAcknowledged))
			{
				bool.TryParse(showAcknowledged, out showAck);
				showClearedEvents.Checked = showAck;
			}
			else
			{
				//to support old versions
				showAcknowledged = Request.QueryString["showCleared"];
				if (!string.IsNullOrEmpty(showAcknowledged) && bool.TryParse(showAcknowledged, out showAck))
					showClearedEvents.Checked = showAck;
			}

			if (!string.IsNullOrEmpty(Request.QueryString["SortDir"]))
				SortDir = Request.QueryString["SortDir"].Equals(Boolean.FalseString, StringComparison.OrdinalIgnoreCase) ?
					SortDirection.Descending : SortDirection.Ascending;

			if (!string.IsNullOrEmpty(Request.QueryString["SortString"]))
				SortString = Request.QueryString["SortString"];

			SetSortImages();

			if (!string.IsNullOrEmpty(Request.QueryString["HideDetails"]))
			{
				var hide = bool.Parse(Request.QueryString["HideDetails"]);
				ButtonShow.Visible = hide;
				ButtonHide.Visible = !hide;

				Details.Visible = !hide;
			}

		    if (!string.IsNullOrWhiteSpace(Request.QueryString["ServerId"]))
                this.SolarwindsServersDropDown.SelectedValue = Request.QueryString["ServerId"];
			// initial value has to be true
			CameFromSort = true;
		}
		else
		{
			CameFromSort = CameFromSort && IsSort;
		}

		PopulateChildControls();
        this.SwisErrorControl.SetError(this.errors);
        if (this.SwisErrorControl.ServersWithErrors == null || this.SwisErrorControl.ServersWithErrors.Length == 0)
	        this.SwisErrorControl.Visible = false;
	}

	// Shows or hides details (filter page)
	protected void ShowHide_Click(object sender, EventArgs e)
	{
		if (sender as LocalizableButton == null) return;

		if (((LocalizableButton)sender).ID == "ButtonShow")
		{
			ButtonShow.Visible = false;
			ButtonHide.Visible = true;

			Details.Visible = true;
		}
		else
		{
			ButtonShow.Visible = true;
			ButtonHide.Visible = false;

			Details.Visible = false;
		}
	}

	int? GetRequestedNumberOfEvents()
	{
		int number = -1;
		if (Request["MaxEvents"] != null && int.TryParse(Request["MaxEvents"], out number) && number > 0)
		{
			return number;
		}
		return null;
	}

	bool GetRequestedAcknowledgedFilter()
	{
		bool showAcknowledged = false;
		if (Request["showCleared"] != null && bool.TryParse(Request["showCleared"], out showAcknowledged))
		{
			return showAcknowledged;
		}
		return false;
	}

	void ShowPrintableVersion()
	{
		// duplicates a filter settings and redirect to printable version
		string id = netObjects.NetObjectID;
		if (!String.IsNullOrEmpty(netObjects.NetObjectID)) id = id.Substring(2);
		else id = "-1";

		DateTime periodBegin = new DateTime();
		DateTime periodEnd = new DateTime();
		string period = Period;
		Periods.Parse(ref period, ref periodBegin, ref periodEnd);

		int? maxEvents = null;
		if (!IsPostBack)
		{
			maxEvents = GetRequestedNumberOfEvents();
		}

		if (!maxEvents.HasValue || maxEvents.Value <= 0)
		{
			int x = 0;
			Int32.TryParse(this.maxRecords.Text, out x);
			maxEvents = x;
		}

		bool sortD = (SortDir == SortDirection.Ascending ? true : false);
		string netObjType = (String.IsNullOrEmpty(this.netObjectTypes.DeviceType) ? "ALL" : this.netObjectTypes.DeviceType);
	    
        string url = string.Format(@"PrintableEvents.aspx?NetObjectID={0}&NetObjectType={1}&EventType={2}&PeriodBegin={3}&PeriodEnd={4}&ShowClearedEvents={5}&MaxRecords={6}&SortDir={7}&SortString={8}&ServerId={9}",
            id, netObjType, EventTypes[eventTypes.SelectedItem.Value], periodBegin.Ticks, periodEnd.Ticks, this.showClearedEvents.Checked, maxEvents.Value, sortD.ToString(), HttpUtility.HtmlEncode(SortString), this.SolarwindsServersDropDown.SelectedValue);

		Response.Redirect(url);

	}

	protected void Printable_Click(object sender, EventArgs e)
	{
		ShowPrintableVersion();
	}

	protected void ClearEvents(object sender, EventArgs e)
	{
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            return;

		if (!Profile.AllowEventClear)
			throw new ApplicationException("You do not have permission to clear events.");

		// update flag acknowledged for selected events to true
		Dictionary<int, List<int>> eventsBySite = new Dictionary<int, List<int>>();
		foreach (RepeaterItem item in this.EventsGrid.Items)
		{
            ExtractCheckedEvent(eventsBySite, (CustomCheckBox)item.FindControl("EventsCheckBox"));
            ExtractCheckedEvent(eventsBySite, (CustomCheckBox)item.FindControl("AltEventsCheckBox"));
		}

	    foreach (var siteId in eventsBySite.Keys)
	    {
	        var eventIds = eventsBySite[siteId];
	        if (!eventIds.Any())
	            continue;

	        if (siteId == OrionSitesCache.Instance.GetLocalOrionSiteID())
	        {
	            using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
	            {
	                proxy.AcknowledgeEvents(eventIds);
	            }
	        }
	        else
	        {
                var proxyProvider = new OrionSiteSwisProxyProvider();
                using (var proxy = proxyProvider.CreateProxy(siteId))
                {
                    proxy.Invoke<bool>("Orion.Events", "Acknowledge", eventIds);
                }
            }
	    }
	}

    private void ExtractCheckedEvent(Dictionary<int, List<int>> storage, CustomCheckBox box)
    {
        if (box == null || !box.Checked)
            return;

        int siteId = (int) box.CustomValue;
        var eventId = box.EventID;

        List<int> events;
        if (!storage.TryGetValue(siteId, out events))
        {
            events = new List<int>();
            storage[siteId] = events;
        }

        events.Add(eventId);
    }

	private string GetUrlWithParams(string pageUrl)
	{
        return string.Format(@"{0}?NetObject={1}&DeviceType={2}&EventType={3}&MaxRecords={4}&ShowClearedEvents={5}&Period={6}&SortDir={7}&SortString={8}&HideDetails={9}&ServerId={10}",
			pageUrl,
			netObjects.NetObjectID,
			netObjectTypes.DeviceType,
			EventTypes[eventTypes.SelectedItem.Value],
			maxRecords.Text,
			showClearedEvents.Checked,
			Period,
			SortDir == SortDirection.Ascending,
            Server.UrlEncode(SortString),
			!Details.Visible,
            this.SolarwindsServersDropDown.SelectedValue);
	}

	#endregion
}
