<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NPMChart.ascx.cs" Inherits="NPMChart" %>

<%@ Register TagPrefix="orion" TagName="SampleSizeControl" Src="~/Orion/Controls/SampleSizeControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="TimePeriodControl" Src="~/Orion/Controls/TimePeriodControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="ShowTrend" Src="~/Orion/Controls/ShowTrendControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="IconHelpButton" Src="~/Orion/Controls/IconHelpButton.ascx" %>

<%if (Request["Printable"] == null)
  { %>
<table cellpadding="8" border="0">
    <tr valign="top">
        <td nowrap class="PageHeader">
            <%= DefaultSanitizer.SanitizeHtml(this.Page.Title) %>
        </td>
    </tr>
</table>
<%} %>
<img src="<%= DefaultSanitizer.SanitizeHtml(GetChartImageURL()) %>" />

<% if (Request["Printable"] != null) {
		Response.End();
   }
%>
<input type="hidden" name="Name" value="<%= DefaultSanitizer.SanitizeHtml(DisplayName) %>">
<%--input type="hidden" name="NetObject" id="NetObject" value="<--%= NetObjectID %-->"--%>
<div style="padding: 10px;">
	<table width="840" class="formTable" cellpadding="0" cellspacing="0">
	<tr class="PageHeader">
		<td style="width: 30%;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_73) %></td>
        <td style="width: 40%;">
            <div class="sw-btn-bar">
                <orion:LocalizableButton runat="server" ID="Img1" OnClick="RefreshClick" LocalizedText="Refresh" DisplayType="Primary" />
            </div>
        </td>
        <td style="width: 30%;"><!-- ie --></td>
	</tr>
	<tr>
		<td colspan="3"><hr class="formDivider"></td>
	</tr>
	<tr>
		<td colspan="3" class="formGroupHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_71) %></td>
	</tr>
	<tr>
		<td class="formLeftTitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_VB0_102) %></td>
		<td class="formRightInput"><asp:TextBox runat="server" ID="chartTitle" Width="200"></asp:TextBox></td>
		<td class="formHelpfulText">&nbsp;</td>
	</tr>
	<tr>
		<td class="formLeftTitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_163) %></td>
		<td class="formRightInput"><asp:TextBox runat="server" ID="chartSubTitle" Width="200"></asp:TextBox></td>
		<td class="formHelpfulText">&nbsp;</td>
	</tr>
	<tr>
		<td class="formLeftTitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_75) %></td>
		<td class="formRightInput"><asp:TextBox runat="server" ID="chartSubTitle2" Width="200"></asp:TextBox></td>
		<td class="formHelpfulText">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3"><hr class="formDivider"></td>
	</tr>
	<tr>
		<td colspan="3"><orion:TimePeriodControl ID="timePeriodControl" runat="server" /></td>
	</tr>
	<tr>
		<td colspan="3"><hr class="formDivider"></td>
	</tr>
	<tr>
		<td colspan="3"><orion:SampleSizeControl ID="sampleSizeControl" runat="server" /></td>
	</tr>
	<tr>
		<td colspan="3"><hr class="formDivider"></td>
	</tr>
	<tr>
		<td colspan="3" class="formGroupHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_70) %></td>
	</tr>
	<tr>
		<td class="formLeftTitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_69) %></td>
		<td class="formRightInput"><asp:TextBox runat="server" ID="chartWidth" Width="80"></asp:TextBox></td>
		<td rowspan="2"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_72) %></td>
	</tr>
	<tr>
		<td class="formLeftTitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_68) %></td>
		<td class="formRightInput"><asp:TextBox runat="server" ID="chartHeight" Width="80"></asp:TextBox></td>
	
	</tr>
	<tr>
		<td colspan="3"><hr class="formDivider"></td>
	</tr>
	<%--<tr>
		<td width="175"><b>Data Table Below Chart</b></td>
		<td width="120">
			<asp:DropDownList AutoPostBack="false" id="dataTableBelowChartDDL" runat="server" />			
		</td>
		<td>
			Displays a table of data points below the chart.<br>
			The data points may not be readable if you select a small Sample Interval.
			Selecting a larger Sample Interval will make the data points more readable.
		</td>
	</tr>
	<tr>
		<td colspan="3"><hr class="Property"></td>
	</tr>--%>
	<tr>
		<td class="formLeftTitle" style="font-weight: bold; text-align:left;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_67) %></td>
		<td class="formRightInput">
			<asp:DropDownList AutoPostBack="false" id="fontSizeDDL" runat="server" >
				<asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_76 %>" Value="2"></asp:ListItem>
				<asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_77 %>" Value="1"></asp:ListItem>
				<asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_78 %>" Value="0"></asp:ListItem>
			</asp:DropDownList>
		</td>
		<td class="formHelpfulText">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3"><hr class="formDivider"></td>
	</tr>
	<tr>
		<td colspan="3"><orion:ShowTrend ID="showTrend" Checked="true" runat="server" /></td>
	</tr>
	
	<!-- UnComment this section to enable Chart Styles
	<tr>
		<td width="175"><b>Chart Style</b></td>
		<td width="120">
			<+--%=Chart.GetStyleHTML%--+>
		</td>
		<td>
			Chart Styles are used to adjust the Fonts and Colors of a chart
		</td>
	</tr>
	<tr>
		<td colspan="3"><hr class="Property"></td>
	</tr>
	-->
	<tr>
		<td>&nbsp;</td>
		<td>
            <div class="sw-btn-bar">
                <orion:LocalizableButton runat="server" ID="btnRefresh" OnClick="RefreshClick" LocalizedText="Refresh" DisplayType="Primary" />
                <orion:LocalizableButton runat="server" ID="btnSave" OnClick="SubmitClick" LocalizedText="Submit" DisplayType="Primary" />
            </div>
		</td>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td colspan="3"><hr class="formDivider"></td>
	</tr>
	<tr>
		<td><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_74) %></b></td>
		<td colspan="2" align="right">
            <div class="sw-btn-bar-wizard">
                <orion:LocalizableButtonLink ID="locButtonExcel" NavigateUrl='<%# DefaultSanitizer.SanitizeHtml(GetDataURL() + "&DataFormat=Excel") %>' DisplayType="Small" 
                    LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_79 %>" runat="server" />
                <orion:LocalizableButtonLink ID="locButtonChart" NavigateUrl='<%# DefaultSanitizer.SanitizeHtml(GetDataURL()) %>' DisplayType="Small" 
                    LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_80 %>" runat="server" />
            </div>
		</td>
	</tr>
	</table>
</div>
