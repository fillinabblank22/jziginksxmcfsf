﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Shared;

public partial class Orion_NetPerfMon_ActiveAlertPopup : System.Web.UI.Page
{
    protected ActiveAlert ActiveAlert { get; set; }
    protected string AlertStatus { get; set; }
    protected string EntityName { get; set; }
    protected bool IsGlobalAlert { get; set; }
    protected bool Visible { get; set; }
    protected int RemainingItemCount { get; set; }
    protected string MoreItemsMessage { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        Visible = true;
        MoreItemsMessage = string.Empty;
        ActiveAlert = NetObjectFactory.Create(Request["NetObject"], true) as ActiveAlert;

        //show tip only for global alerts
        string triggeringObjectEntityUri = ActiveAlert.ObjectProperties["TriggeringObjectEntityUri"] != null ? Convert.ToString(ActiveAlert.ObjectProperties["TriggeringObjectEntityUri"]) : string.Empty;
        IsGlobalAlert = string.IsNullOrEmpty(triggeringObjectEntityUri);
        if (!IsGlobalAlert)
        {
            Response.Clear();
            Response.Write("NoTip");
            Visible = false;
            return;
        }

        AlertStatus = ActiveAlert.ObjectProperties["TriggeringObjectStatus"] != null ? Convert.ToString(ActiveAlert.ObjectProperties["TriggeringObjectStatus"]) : "0";
        string entityName = ActiveAlert.ObjectProperties["TriggeringObjectEntityName"] != null ? Convert.ToString(ActiveAlert.ObjectProperties["TriggeringObjectEntityName"]) : string.Empty;
        EntityName = string.Compare(entityName, "Orion.Container", StringComparison.InvariantCultureIgnoreCase) == 0 ? "Orion.Groups" : entityName;

        var lstHingObject = GetObjectsForGlobalAlert(EntityName, ActiveAlert.ActiveAlertId);
        int numberOfItems = lstHingObject.Count;
        lstHingObject = lstHingObject.OrderBy(item => item.DisplayName).Take(20).ToList();
        RemainingItemCount = numberOfItems - lstHingObject.Count;
        if (RemainingItemCount > 0)
        {
            MoreItemsMessage = string.Format(Resources.CoreWebContent.WEBCODE_PS0_27, RemainingItemCount);
        }

        if (lstHingObject.Count > 0)
        {
            // Collect different statuses
            IEnumerable<Status> differentStatuses =
                (from hintObjects in lstHingObject.AsEnumerable()
                 select new
                 {
                     hintObjects.Status
                 }).Distinct().Select(x =>
                    new Status() { Value = (OBJECT_STATUS)x.Status }).ToList();

            // Get the worst one
            AlertStatus = ((int)Status.Summarize(differentStatuses).Value).ToString(CultureInfo.InvariantCulture);

            rptObjects.DataSource = lstHingObject;
            rptObjects.DataBind();
        }
        else
        {
            Response.Clear();
            Response.Write("NoTip");
            Visible = false;
        }
    }

    private List<HintObject> GetObjectsForGlobalAlert(string entity, int alertObjectId)
    {
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            string swql = @"SELECT a.AlertActiveObjects.EntityUri
                                              FROM Orion.AlertActive (nolock=true) AS a
                                              WHERE a.AlertObjectID=@objectId";

            DataTable activeObjects = proxy.Query(swql, new Dictionary<string, object> { { "objectId", alertObjectId } });
            var uris = activeObjects.AsEnumerable().Where(item => item["EntityUri"] != DBNull.Value).Select(item => Convert.ToString(item["EntityUri"]));
            var result = new List<HintObject>();

            if (!uris.Any())
                return result;

            swql = "SELECT Name FROM Metadata.Property WHERE EntityName=@entityName AND Name='Status'";
            DataTable tbl = proxy.Query(swql, new Dictionary<string, object> { { "entityName", entity } });

            swql = String.Format("SELECT {0} AS Status, DisplayName, Uri FROM {{0}} (nolock=true) WHERE {{1}}", tbl.Rows.Count == 0 ? "NULL" : "Status");

            StringBuilder sbCondition = new StringBuilder();

            Action<Dictionary<string, object>> addUris = (Dictionary<string, object> swqlParameters) =>
            {
                string query = string.Format(swql, entity, sbCondition);

                DataTable tblUris = proxy.Query(query, swqlParameters);
                foreach (DataRow rUri in tblUris.Rows)
                {
                    int status = rUri["Status"] != DBNull.Value ? Convert.ToInt32(rUri["Status"]) : StatusInfo.UnknownStatusId;
                    string displayName = rUri["DisplayName"] != DBNull.Value ? Convert.ToString(rUri["DisplayName"]) : string.Empty;
                    string uri = rUri["Uri"] != DBNull.Value ? Convert.ToString(rUri["Uri"]).Replace("\"","") : string.Empty;

                    result.Add(new HintObject(status, displayName, uri));
                }
            };

            int count = 0;
            Dictionary<string, object> swisParameters = new Dictionary<string, object>();
            foreach (var uri in uris)
            {
                if (count > 0)
                {
                    sbCondition.Append(" OR ");
                }

                sbCondition.AppendFormat("Uri=@uri{0}", count);
                swisParameters.Add(string.Format("uri{0}", count), uri);
                count++;

                if ((count%1000) == 0)
                {
                    addUris(swisParameters);
                    swisParameters = new Dictionary<string, object>();
                    sbCondition.Clear();
                    count = 0;
                }
            }

            if (count > 0)
            {
                addUris(swisParameters);
            }

            return result;
        }
    }

    protected class HintObject
    {
        public int Status { get; private set; }
        public string DisplayName { get; private set; }
        public string Uri { get; private set; }

        public HintObject(int status, string displayName, string uri)
        {
            Status = status;
            DisplayName = displayName;
            Uri = uri;
        }
    }
}