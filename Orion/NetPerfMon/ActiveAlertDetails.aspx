<%@ Page Language="C#"  MasterPageFile="~/Orion/View.master" AutoEventWireup="true" 
    CodeFile="ActiveAlertDetails.aspx.cs" Inherits="Orion_NetPerfMon_ActiveAlertDetails" 
     Title="Node Details" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="orion" TagName="StatusIconControl" Src="~/Orion/NetPerfMon/Controls/StatusIconControl.ascx" %>
<%@ Register TagPrefix="netperfmon" Assembly="NetPerfMonWeb" Namespace="SolarWinds.Orion.NPM.Web.UI" %>
<%@ Register TagPrefix="npm" TagName="NodeLink" Src="~/Orion/NetPerfMon/Controls/NodeLink.ascx" %>
<%@ Register TagPrefix="npm" TagName="IntegrationIcons" Src="~/Orion/NetPerfMon/Controls/IntegrationIcons.ascx" %>


<asp:Content runat="server" ContentPlaceHolderID="ViewPageTitle" ID="Content1">
    <%if (!CommonWebHelper.IsBreadcrumbsDisabled)
               { %>
	            <div style="margin-left: 10px; margin-top: 10px" >
                    <orion:PluggableDropDownMapPath ID="NodeSiteMapPath" runat="server" SiteMapProvider="ActiveAlertSitemapProvider" SiteMapKey="NetObjectDetails" LoadSiblingsOnDemand="true"/>
				</div>
				<%} %>

	<h1>
	    <span style="float: right; padding-right: 30px;">
	       
	    </span>
	    <%= DefaultSanitizer.SanitizeHtml(ViewInfo.IsSubView ? ViewInfo.ViewGroupTitle : ViewInfo.ViewHtmlTitle) %> 
	    - 
        <img src="<%= SeverityImageUrl %>" />
        <asp:Literal ID="lblAlertName" runat="server" />
        - <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_86) %> 
        <orion:StatusIconControl ID="statusIconControl" runat="server" Visible="false" />
        <asp:Literal ID="lblNodeName" runat="server" />
        <asp:Literal ID="relatedNodeLable" runat="server" Visible="false" />
        <orion:StatusIconControl ID="relatedNodeStatusIconControl" runat="server" Visible="false" /> 
        <asp:Literal ID="lblRelaytedNodeName" runat="server" Visible="false" />
	    <asp:PlaceHolder ID="TitleDecorators" runat="server"></asp:PlaceHolder>	    
        <%= DefaultSanitizer.SanitizeHtml(ViewInfo.IsSubView ? ViewInfo.ViewHtmlTitle : string.Empty) %>
	</h1>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <input type="hidden" id="activeAlertDetailsIdentifier" />
    <netperfmon:ActiveAlertResourceHost runat="server" ID="activeAlertResHost">
	    <orion:ResourceContainer runat="server" ID="resContainer" />
	</netperfmon:ActiveAlertResourceHost>
</asp:Content>
