<%@ Control Language="C#" ClassName="ObsoleteInternalInterfaceList" EnableViewState="true" %>

<script runat="server">
     
    private Control _wrappedControl;

    [Obsolete("This control is obsolete and will be removed.  Use /Orion/NPM/InternalInterfaceList.ascx ")]
    public object DataSource
    {
        get { throw new NotImplementedException("This needs to be fixed."); }
        set { throw new NotImplementedException("This needs to be fixed."); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        _wrappedControl = LoadControl("~/Orion/NPM/InternalInterfaceList.ascx");
        
        if (_wrappedControl != null)
            Controls.Add(_wrappedControl);
    }
    
</script>
