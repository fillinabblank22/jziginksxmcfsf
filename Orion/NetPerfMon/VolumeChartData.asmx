﻿<%@ WebService Language="C#" Class="VolumeChartData" %>

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class VolumeChartData : ChartDataWebService
{
    private static SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();
    private const string _volumeNameLookupSwql = "SELECT VolumeID, FullName FROM Orion.Volumes WHERE VolumeID in @NodeID";

    [WebMethod]
    public ChartDataResults AvgDiskUsage(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        const string sql = @"SELECT DateTime, AvgDiskUsed
                             FROM VolumeUsage 
                             WHERE VolumeID = @NetObjectId
                               AND DateTime >= @StartTime AND DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, sql, _volumeNameLookupSwql, DateTimeKind.Local, "DiskSpaceUsed");
        return result;
    }

    [WebMethod]
    public ChartDataResults PercentDiskUsage(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        const string sql = @"SELECT DateTime, PercentDiskUsed
                             FROM VolumeUsage 
                             WHERE VolumeID = @NetObjectId
                               AND DateTime >= @StartTime AND DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, sql, _volumeNameLookupSwql, DateTimeKind.Local, "PercentDiskUsed");
        return result;
    }


    [WebMethod]
    public ChartDataResults DiskSize(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        const string sql = @"SELECT DateTime, DiskSize
                             FROM VolumeUsage 
                             WHERE VolumeID = @NetObjectId
                               AND DateTime >= @StartTime AND DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, sql, _volumeNameLookupSwql, DateTimeKind.Local, "VolumeSize");
        return result;
    }

    [WebMethod]
    public ChartDataResults DiskAllocationFailures(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        const string sql = @"SELECT DateTime, AllocationFailures
                             FROM VolumeUsage 
                             WHERE VolumeID = @NetObjectId
                               AND DateTime >= @StartTime AND DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, sql, _volumeNameLookupSwql, DateTimeKind.Local, "AllocationFailures");
        return result;
    }

    [WebMethod]
    public ChartDataResults DiskQueueLength(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        const string sql = @"SELECT DateTime, AvgDiskQueueLength
                             FROM VolumePerformance
                             WHERE VolumeID = @NetObjectId
                             AND DateTime >= @StartTime AND DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, sql, _volumeNameLookupSwql, DateTimeKind.Utc, "DiskQueueLength");
        return result;
    }

    [WebMethod]
    public ChartDataResults AvgDiskSecPerTransfer(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        const string sql =  @"SELECT DateTime, AvgDiskTransfer
                             FROM VolumePerformance
                             WHERE VolumeID = @NetObjectId
                             AND DateTime >= @StartTime AND DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, sql, _volumeNameLookupSwql, DateTimeKind.Utc, "AvgDiskSecPerTransfer");
        return result;
    }

    [WebMethod]
    public ChartDataResults TotalDiskIOPS(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        const string sql = @"SELECT DateTime, AvgDiskReads, AvgDiskWrites
                               FROM VolumePerformance
                               WHERE VolumeID = @NetObjectId
                               AND DateTime >= @StartTime AND DateTime <= @EndTime
                               ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, sql, _volumeNameLookupSwql, DateTimeKind.Utc, "DiskReads", "DiskWrites");

        if (request.CalculateSum && (result.DataSeries.Count() > 2))
        {
            var sumSerieIndex = result.DataSeries.Count() - 1;
            var sumSeries = result.DataSeries.ElementAt(sumSerieIndex);
            sumSeries.Data.Clear();
            var nSumSeries = DataSeries.CreateSumSeries(result.DataSeries);
            for (int i = 0; i < nSumSeries.Data.Count(); i++)
            {

                if (nSumSeries.Data[i].IsNullPoint)
                {
                    DataPoint.CreateNullPoint(nSumSeries.Data[i].UnixTime);
                }
                else
                {
                    sumSeries.Data.Add(DataPoint.CreatePoint(nSumSeries.Data[i].UnixTime, nSumSeries.Data[i].Value));
                }
            }
        }

        return result;
    }
}