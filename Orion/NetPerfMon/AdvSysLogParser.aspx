<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/OrionMasterPage.master" CodeFile="AdvSysLogParser.aspx.cs" Inherits="Orion_NetPerfMon_AdvSysLogParser" EnableEventValidation="false" EnableViewState="true" %>
<%@ Register src="Controls/AdvancedSyslogParser.ascx" tagname="AdvancedSyslogParser" tagprefix="uc1" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="placeHolder" runat="server">

<orion:Include runat="server" File="js/jquery/timePicker.css" />

    <asp:PlaceHolder ID="holder" runat="server" >
        <div style="margin-left: 0px; margin-right: 0px; width:100%">
        <uc1:AdvancedSyslogParser ID="AdvancedSyslogParser1" runat="server" />
        </div>
    </asp:PlaceHolder>
</asp:Content>
