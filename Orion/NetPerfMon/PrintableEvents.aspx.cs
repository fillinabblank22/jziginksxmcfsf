using System;
using System.Data;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Core.Models.Events;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Core.Common.Swis;

using Limitation = SolarWinds.Orion.Web.Limitation;

public partial class Orion_NetPerfMon_Printable_Events : System.Web.UI.Page
{
    #region private members

    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    private DataTable eventsTable = new DataTable();

    #endregion

	public bool FederationEnabled
	{
		get { return SwisFederationInfo.IsFederationEnabled; }
	}

    // on page sorting
    private void SortOnPage(string column, SortDirection direction)
    {
        string sortD = " ASC";
        if (direction == SortDirection.Descending) sortD = " DESC";

        if (column == "Message") eventsTable.DefaultView.Sort = eventsTable.Columns["Message"] + sortD;
        else eventsTable.DefaultView.Sort = eventsTable.Columns["EventTime"] + sortD;

        if (column == "OrionServerName") eventsTable.DefaultView.Sort = eventsTable.Columns["OrionSiteName"] + sortD;
        else eventsTable.DefaultView.Sort = eventsTable.Columns["OrionSiteName"] + sortD;
    }

    string GetArgument(string key)
    {
        string result = Request.QueryString[key];
        if (String.IsNullOrEmpty(result))
            throw new ArgumentNullException(" Agrument \"" + key + "\" not found!");
        return result;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DateTime periodBegin = new DateTime(Int64.Parse(GetArgument("PeriodBegin")), DateTimeKind.Local);
            DateTime periodEnd = new DateTime(Int64.Parse(GetArgument("PeriodEnd")), DateTimeKind.Local);
            bool showClearedEvents = false;
            SortDirection sortDir = SortDirection.Ascending;
            string serverId = Request.QueryString["ServerId"];

            int netObjectID = Int32.Parse(GetArgument("NetObjectID"));
            string netObjectType = GetArgument("NetObjectType");
            if (netObjectType == "ALL") netObjectType = "";
            int eventType = Int32.Parse(GetArgument("EventType"));
            showClearedEvents = bool.Parse(GetArgument("ShowClearedEvents"));
            int maxRecords = Int32.Parse(GetArgument("MaxRecords"));
            if (GetArgument("SortDir") == Boolean.FalseString) sortDir = SortDirection.Descending;
            var param = new GetEventsParameter()
            {
                NetObjectId = netObjectID,
                NetObjectType = "N",
                DeviceType = netObjectType,
                EventType = eventType,
                FromDate = periodBegin,
                ToDate = periodEnd,
                IncludeAcknowledged = showClearedEvents,
                MaxRecords = maxRecords,
                LimitationIDs = Limitation.GetCurrentListOfLimitationIDs(),
                SiteId = serverId
            };

            eventsTable = EventDAL.GetEventsTable(param);

            SortOnPage(GetArgument("SortString"), sortDir);

            foreach (DataRow row in eventsTable.Rows)
            {
                var color = (int) row["BackColor"];
                row["BackColor"] = ((color & 0xFF0000) >> 16) + (color & 0x00FF00) + ((color & 0x0000FF) << 16);
            }

            EventsGrid.DataSource = eventsTable;
            EventsGrid.DataBind();
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        SolarWinds.Orion.Web.OrionInclude.CoreThemeStylesheets();
    }
}
