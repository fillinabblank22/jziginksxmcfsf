using System;
using System.IO;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web;
using System.Collections.Generic;

public partial class Chart : System.Web.UI.Page
{
	private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

	protected void BusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}

    protected override void OnPreInit(EventArgs e)
    {
        ResourceInfo resource = ResourceManager.GetResourceByID(Convert.ToInt32(Request.QueryString["ResourceID"]));

        if (null != resource)
            Context.Items[typeof(ViewInfo).Name] = resource.View;

        base.OnPreInit(e);
    }

    protected void Page_Load(object sender, EventArgs e) 
	{
		ChartBase chart = ChartImageGenerator.GenerateChart(Request.QueryString);

		MemoryStream stream = new MemoryStream();

		chart.GetAsImage(stream, System.Drawing.Imaging.ImageFormat.Png);

		Response.ContentType = "image/png";
		stream.WriteTo(Response.OutputStream);
		Response.End();
	}
}
