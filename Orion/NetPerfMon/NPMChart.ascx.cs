using System;
using System.Globalization;
using System.Web;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using SolarWinds.Orion.Web.Helpers;
using System.Web.UI.WebControls;

public partial class NPMChart : GraphResource
{
	private static readonly Log log = new Log(); 

	public void MyBusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}

	NetObject netObject = null;

	public string DisplayName;
	public string _customPollerID;
	public string NetObjectID;
	public string ChartTitle = "";
	public string ChartSubTitle = "";
	public string ChartSubTitle2 = "";
	public string DisplayChartTitle = "";
	public string DisplayChartSubTitle = "";
	public string DisplayChartSubTitle2 = "";
	public string Period;
	public string SampleSize;
	public int SampleSizeInMinutes;
	public DateTime StartTime;
	public DateTime EndTime;

	public bool ShowDataTable;
	public String SubsetColor;
	public String RYSubsetColor;
	public String PlotStyle;
	public String TimeUnit;
	public String FontSize;

	public bool AllowTrend;

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

        var resourceId = SanitizedParam("ResourceID");

		if (Profile.AllowCustomize && resourceId != null)
		{
			btnSave.Visible = true;
		}
		else
		{
			btnSave.Visible = false;
		}
	}

    private void AddTopRightLinks()
    {
        if (this.Page.Master == null) return;

        try
        {

            var ctrl = ControlHelper.FindControlRecursive(this.Page.Master, "TopRightPageLinks");
            if (ctrl == null) return;

            ctrl.Controls.Add(new HyperLink
            {
                ID = "Img2",
                NavigateUrl = GetPrintableVersionURL(),
                CssClass = "printablePageLink",
                Text = Resources.CoreWebContent.WEBDATA_VB0_246,
                EnableViewState = false
            });

            ctrl.Controls.Add(new ASP.IconHelpButton
            {
                ID = "helpButton",
                HelpUrlFragment = "OrionPHViewCustomChart",
                EnableViewState = false
            });
        }
        catch (System.Web.HttpException)
        {
            // -- shouldn't occur --
            // the page may be using this control and incompatible - having control blocks in a content section.
            // if so, this exception will occur. we'd rather default to not having the links show up than see an error page.
            // -- shouldn't occur --
        }
    }

	protected override void OnLoad(EventArgs e)
    {
		base.OnLoad(e);
		ResourceInfo resource = null;

		ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);

        var resourceId = SanitizedParam("ResourceID");

        // Gets Parameters from the request (html safe)
        NetObject netObj = null;
		if (resourceId != null)
		{
			resource = ResourceManager.GetResourceByID(Convert.ToInt32(resourceId));
		}

		switch (SanitizedParam("NetObjectPrefix"))
		{
			case "N":
				netObj = NetObjectFactory.Create(SanitizedParam("NetObject"));
				NetObjectID = "N:" + netObj["NodeID"].ToString();
				break;
			case "S":
				NetObjectID = string.Empty;
				break;
			default:
				NetObjectID = SanitizedParam("NetObject");
				break;
		}

		if (!string.IsNullOrEmpty(NetObjectID))
		{
			netObject = NetObjectFactory.Create(NetObjectID);
		}

		if (IsPostBack)
		{
			FontSize = fontSizeDDL.SelectedValue;

            DisplayChartTitle = ChartTitle = WebSecurityHelper.SanitizeHtml(chartTitle.Text);
            DisplayChartSubTitle = ChartSubTitle = WebSecurityHelper.SanitizeHtml(chartSubTitle.Text);
            DisplayChartSubTitle2 = ChartSubTitle2 = WebSecurityHelper.SanitizeHtml(chartSubTitle2.Text);
		}
		else
		{
            FontSize = SanitizedParam("FontSize", () => string.Empty);


            DisplayChartTitle = ChartTitle = chartTitle.Text =
                SanitizedParam("Title", () => (resource != null ? resource.Properties["Title"] : string.Empty));
               
			DisplayChartSubTitle = ChartSubTitle = chartSubTitle.Text =
                SanitizedParam("SubTitle", () => (resource != null ? resource.Properties["SubTitle"] : string.Empty));

			DisplayChartSubTitle2 = ChartSubTitle2 = chartSubTitle2.Text =
                 SanitizedParam("SubTitle2", () => (resource != null ? resource.Properties["SubTitle2"] : string.Empty));
		}

		if (string.IsNullOrEmpty(DisplayChartTitle))
		{
			if (netObject != null)
			{
				if (netObject.ObjectProperties.ContainsKey("FullName"))
				{
					DisplayChartTitle = netObject["FullName"].ToString();
				}
				else if (netObject.ObjectProperties.ContainsKey("Caption"))
				{
					DisplayChartTitle = netObject["Caption"].ToString();
				}
			}
			else if (resource != null)
			{
				DisplayChartTitle = resource.Title;
			}
		}
        DisplayName = SanitizedParam("ChartName");
        AllowTrend = showTrend.Visible = (ChartInfoFactory.Create(Request["ChartName"]).IsTrendAllowed());

		chartWidth.Text = Width.ToString();
		chartHeight.Text = Height.ToString();

		if (!IsPostBack)
		{
			bool boolValue;
            string showTrendParam = SanitizedParam("ShowTrend");
            if (showTrendParam != null)
			{
				showTrend.Checked = (bool.TryParse(showTrendParam, out boolValue)) ? boolValue : false;
			}
			else
			{
				showTrend.Checked = (resource != null) ? (bool.TryParse(resource.Properties["ShowTrend"], out boolValue) ? boolValue : true) : true;
			}
		}

        var sampleSize = SanitizedParam("SampleSize");
		if (sampleSize != null && !IsPostBack)
			sampleSizeControl.SampleSizeValue = SampleSize = sampleSize;
		else
			SampleSize = sampleSizeControl.SampleSizeValue;

		if (timePeriodControl.TimePeriodText == "Custom")
        {
			if (string.IsNullOrEmpty(timePeriodControl.CustomPeriodBegin))
			{
				timePeriodControl.CustomPeriodBegin = "0:00:00";
			}
			if(string.IsNullOrEmpty(timePeriodControl.CustomPeriodEnd))
			{
				timePeriodControl.CustomPeriodEnd = "23:59:59";
			}

			Period = timePeriodControl.CustomPeriodBegin + "~" + timePeriodControl.CustomPeriodEnd;
        }
        else
        {
            Period = timePeriodControl.TimePeriodText;
        }

		timePeriodControl.AttachSampleSizeControl(sampleSizeControl.SampleSizeListControl);

        if (String.IsNullOrEmpty(FontSize))
        {
            OrionSetting setting = SettingsDAL.GetSetting("Web-ChartFontSize"); // ,1
            fontSizeDDL.SelectedValue = setting.SettingValue.ToString();
        }
        else
        {
            fontSizeDDL.SelectedValue = FontSize;
        }

		this.Page.Title = string.Format(Resources.CoreWebContent.WEBCODE_TM0_15, DisplayChartTitle);

        locButtonExcel.DataBind();
        locButtonChart.DataBind();
        Img1.DataBind();
        AddTopRightLinks();
	}

	// Returns a URL for Generation of a report with the Chart data
	public String GetDataURL() {
		NameValueCollection parameters = GetCurrentPageSettings();

		DateTime periodBegin = new DateTime();
		DateTime periodEnd = new DateTime();

		if (Period != "Custom")
		{
			Periods.Parse(ref Period, ref periodBegin, ref periodEnd);
		}
		else
		{
			periodBegin = Convert.ToDateTime(timePeriodControl.CustomPeriodBegin);
			periodEnd = Convert.ToDateTime(timePeriodControl.CustomPeriodEnd);
		}

		if (parameters["PeriodBegin"] == null)
		{
			parameters.Add("PeriodBegin", periodBegin.ToString());
		}

		if (parameters["PeriodEnd"] == null)
		{
			parameters.Add("PeriodEnd", periodEnd.ToString());
		}

		return GetFormattedURL("/Orion/NetPerfMon/ChartData.aspx", parameters, false);
	}

	protected void RefreshClick(object sender, EventArgs e)
	{
        //if there will be any code, uncomment this line
        if (!Page.IsValid) return;
		Response.Redirect(GetURL());
	}

	protected void SubmitClick(object sender, EventArgs e)
	{
        if (!Page.IsValid)
			return;

        var resourceId = SanitizedParam("ResourceID");

        ResourceInfo resource = ResourceManager.GetResourceByID(Convert.ToInt32(resourceId));

		resource.Properties.Remove("Period");
		if (timePeriodControl.TimePeriodText != "Custom")
		{
			resource.Properties.Add("Period", timePeriodControl.TimePeriodText);
		}
		else
		{
			resource.Properties.Add("Period", Convert.ToDateTime(timePeriodControl.CustomPeriodBegin, CultureInfo.CurrentCulture).Ticks.ToString() +
				"~" + Convert.ToDateTime(timePeriodControl.CustomPeriodEnd, CultureInfo.CurrentCulture).Ticks.ToString());
		}

		resource.Properties.Remove("SampleSize");
		resource.Properties.Add("SampleSize", sampleSizeControl.SampleSizeValue);

		resource.Properties.Remove("ShowTrend");
		resource.Properties.Add("ShowTrend", showTrend.Checked.ToString());

        //if (DisplayName == "VMCPUConsumptionArea" || DisplayName == "VMMemoryConsumptionArea" || DisplayName == "VMNetworkTrafficArea")
        // FB69373 - this should be saved for all charts.
		{
			resource.Properties.Remove("Title");
			resource.Properties.Add("Title", ChartTitle);

			resource.Properties.Remove("SubTitle");
			resource.Properties.Add("SubTitle", ChartSubTitle);

			resource.Properties.Remove("SubTitle2");
			resource.Properties.Add("SubTitle2", ChartSubTitle2);
		}
		string url = string.Format("/Orion/View.aspx?ViewID={0}", resource.View.ViewID);

        var netObject = SanitizedQuery("NetObject");
        if (netObject != null)
		{
			string netObjectViewType = NetObjectFactory.GetViewTypeForNetObject(netObject);

			if (netObjectViewType.Equals(resource.View.ViewType, StringComparison.InvariantCultureIgnoreCase))
				url = string.Format("{0}&NetObject={1}", url, netObject);
		}

		ReferrerRedirectorBase.Return(url);
		//Response.Redirect(url);
	}

	protected String GetPrintableVersionURL()
	{
        return GetFormattedURL("/Orion/Netperfmon/PrintableCustomChart.aspx", GetSanitizedQueryString(), true);
	}

	protected String GetChartImageURL()
	{
		return GetFormattedURL("/Orion/Netperfmon/Chart.aspx");
	}

	private string GetFormattedURL(string page)
	{
		return GetFormattedURL(page, GetSanitizedQueryString(), false);
	}

	private string GetFormattedURL(string page, NameValueCollection parameters, bool printable)
	{
		string queryString = page;

		string splitter = "?";
		foreach (string key in parameters.Keys)
		{
			queryString = string.Format("{0}{1}{2}={3}", queryString, splitter, key, 
				(key.IndexOf("title", StringComparison.OrdinalIgnoreCase) > -1) ? HttpUtility.UrlEncode(parameters[key]) : parameters[key]);
			splitter = "&";
		}

		if (printable){
			queryString = string.Format("{0}{1}Printable=true", queryString, splitter);
			splitter = "&";
		}

		// in case width parameter isn't in request string we need to set it
		if (queryString.IndexOf("Width=", StringComparison.OrdinalIgnoreCase)==-1){
			queryString = string.Format("{0}{1}Width={2}", queryString, splitter, Width);
			splitter = "&";
		}

		// fix for NPM Multiple Chart resources
		// Multiple Interface Chart resource in NPM 10.1 takes these
		// properties from Resource properties instead of request string
		if (!string.IsNullOrEmpty(parameters["ResourceID"]))
		{
			ResourceInfo resource = new ResourceInfo();
			resource = ResourceManager.GetResourceByID(int.Parse(parameters["ResourceID"]));

			string selectedEntities = resource.Properties["SelectedEntities"];
			string chartEntity = resource.Properties["EntityName"];
			string showSum = resource.Properties["ShowSum"];
			string dataColumnName = resource.Properties["DataColumnName"];
			string topXX = resource.Properties["TopXX"];
			bool filterEntities = String.IsNullOrEmpty(resource.Properties["FilterEntities"]) ? false : Boolean.Parse(resource.Properties["FilterEntities"]); 
            bool manualSelection;

            //FB40951 Fix for NPM 10.1 + hadrian
            //We need to compute the selected  entities for the ChartData page if automatic selection is on
            if (! Boolean.TryParse(resource.Properties["ManualSelect"], out manualSelection))
            {
                //Default to manual if no such property exists
                manualSelection = true;
            }

            if (!manualSelection)//This will only trigger for charts that actually have the property. No break of MultiObject charts.
            {
                //Automatic selection. We need to fill the selectedEntities
                selectedEntities = OrionChartingHelper.GetEntitiesFilteredByNetObject(chartEntity, parameters["NetObject"], null, topXX);
            }

			if (!string.IsNullOrEmpty(selectedEntities) && string.IsNullOrEmpty(parameters["SelectedEntities"]))
			{
				queryString = string.Format("{0}{1}SelectedEntities={2}", queryString, splitter,
					OrionChartingHelper.GetEntitiesFilteredByNetObject(chartEntity, filterEntities ? parameters["NetObject"] : string.Empty, selectedEntities, topXX));
				splitter = "&";
			}
			if (!string.IsNullOrEmpty(chartEntity) && string.IsNullOrEmpty(parameters["ChartEntity"]))
			{
				queryString = string.Format("{0}{1}ChartEntity={2}", queryString, splitter, chartEntity);
				splitter = "&";
			}
			if (!string.IsNullOrEmpty(showSum) && string.IsNullOrEmpty(parameters["ShowSum"]))
			{
				queryString = string.Format("{0}{1}ShowSum={2}", queryString, splitter, showSum);
				splitter = "&";
			}
			if (!string.IsNullOrEmpty(dataColumnName) && string.IsNullOrEmpty(parameters["DataColumnName"]))
			{
				queryString = string.Format("{0}{1}DataColumnName={2}", queryString, splitter, dataColumnName);
			}
		}
		// fix for NPM Multiple Chart resources

		// with couldn't be 0
		if (queryString.IndexOf("Width=0", StringComparison.OrdinalIgnoreCase) == -1)
		{
			return queryString;
		}
		return Regex.Replace(queryString, "Width=0", "Width=640", RegexOptions.IgnoreCase);
		
	}

	private NameValueCollection GetCurrentPageSettings()
	{
		NameValueCollection parameters = new NameValueCollection();

        var resourceId = SanitizedParam("ResourceID");

        if (resourceId != null)
		{
			ResourceInfo resource = new ResourceInfo();
			resource = ResourceManager.GetResourceByID(Convert.ToInt32(resourceId));
			parameters.Add("SubsetColor", resource.Properties["SubsetColor"]);
			parameters.Add("RYSubsetColor", resource.Properties["RYSubsetColor"]);
		}
		// update existing settings with customized on page
		parameters.Add("ChartName", DisplayName);
		parameters.Add("Title", ChartTitle);
		parameters.Add("SubTitle", ChartSubTitle);
		parameters.Add("SubTitle2", ChartSubTitle2);

		parameters.Add("Width", Width.ToString());
		parameters.Add("Height", Height.ToString());

		parameters.Add("SampleSize", SampleSize);
		parameters.Add("Period", Period);

		if (showTrend.Visible)
			parameters.Add("ShowTrend", showTrend.Checked.ToString());

		parameters.Add("FontSize", fontSizeDDL.SelectedValue);

		foreach (string key in Request.QueryString.Keys)
		{
			if (parameters[key] == null)
			{
                parameters.Add(key, SanitizedQuery(key));
            }
		}
		
		return parameters;
	}

	private string GetURL()
	{
		return GetFormattedURL("/Orion/NetPerfMon/CustomChart.aspx", GetCurrentPageSettings(), false);
	}

	protected int Width
	{
		get
		{
			string value = string.Empty;
			if (!IsPostBack)
			{
				value = SanitizedParam("Width");
			}
			else
			{
				value = chartWidth.Text;
			}
			
			int width = 0;
			if (Int32.TryParse(value, out width) && width > 0)
			{
				return width;
			}
			else
			{
				return 640;
			}
		}
	}

	protected int Height
	{
		get
		{
			string value = string.Empty;
			if (!IsPostBack)
			{
				value = SanitizedParam("Height");
			}
			else
			{
				value = chartHeight.Text;
			}

			int height = 0;
			if (Int32.TryParse(value, out height) && height > 0)
			{
				return height;
			}
			else
			{
				return 0;
			}
		}
	}

	protected override string DefaultTitle
	{
		get { return ""; }
	}

	public override string HelpLinkFragment
	{
		get { return ""; }
	}

    private string SanitizedParam(string key)
        => SanitizedParam(key, () => null);

    private string SanitizedParam(string key, Func<string> defaultValueProvider)
        => string.IsNullOrEmpty(Request.Params[key]) ? defaultValueProvider() : WebSecurityHelper.SanitizeHtml(Request.Params[key].ToString(), false);

    private string SanitizedQuery(string key)
        => SanitizedQuery(key, () => null);

    private string SanitizedQuery(string key, Func<string> defaultValueProvider)
        => string.IsNullOrEmpty(Request.QueryString[key]) ? defaultValueProvider() : WebSecurityHelper.SanitizeHtml(Request.QueryString[key].ToString(), false);

    private NameValueCollection GetSanitizedQueryString()
    {
        var queryString = new NameValueCollection();
        foreach (string key in Request.QueryString.Keys)
        {
            queryString.Add(key, SanitizedQuery(key));
        }
        return queryString;
    }
}