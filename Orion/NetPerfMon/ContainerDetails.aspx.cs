using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.NPM.Web.UI;
using System.Collections.Generic;

public partial class Orion_NetPerfMon_ContainerDetails : OrionView, IContainerProvider
{
    protected override void OnInit(EventArgs e)
    {
        ((Orion_View)this.Master).HelpFragment = "OrionCorePHGroups_GroupDetails";

        string netObject = this.Request.QueryString["NetObject"];

        if (!String.IsNullOrEmpty(netObject))
        {
            string[] parts = netObject.Split(':');

            if (parts.Length > 1)
            {
                // Redirect to the same page with ID for parent container
                if (parts[0].Equals("GM", StringComparison.InvariantCultureIgnoreCase))
                {
                    GroupMember gm = NetObjectFactory.Create(netObject) as GroupMember;

                    if (gm != null)
                    {
                        Response.Redirect(gm.DetailsUrl);
                    }
                }
            }
        }
        
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);

        this.Title = String.Format("{0}", Container.Name);
    }

    protected override void OnLoad(EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
        }

        base.OnLoad(e);
    }

    public override string ViewType
    {
        get { return "GroupDetails"; }
    }

    protected void GroupSiteMapPath_OnInit(object sender, EventArgs e)
    {
        if (!CommonWebHelper.IsBreadcrumbsDisabled)
        {
            var renderer = new NetObjectsSiteMapRenderer();
            renderer.SetUpData(new KeyValuePair<string, string>("NetObject", Request["NetObject"] ?? string.Empty));
            GroupSiteMapPath.SetUpRenderer(renderer);
        }
    }

    #region IContainerProvider Members

    public Container Container
    {
        get { return (Container)this.NetObject; }
    }

    #endregion
}
