<%@ Page Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PS0_129 %>" Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/Alerts/AlertsMasterPage.master" %>

<%@ Implements Interface="SolarWinds.Orion.Web.IBypassAccessLimitation" %>
<%@ Import Namespace="SolarWinds.Logging" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Swis" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>

<%@ Register TagPrefix="orion" TagName="AcknowledgeAlertDialog" Src="~/Orion/Controls/AcknowledgeAlertDialogControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="ClearTriggeredAlertsControl" Src="~/Orion/Controls/ClearTriggeredAlertsControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditAlertDefinition" Src="~/Orion/Controls/AlertEditDefinitionDraftDialogControl.ascx" %>
<%@ Register TagPrefix="swisf" TagName="swiserrorcontrol" Src="~/Orion/SwisfErrorControl.ascx" %>

<script language="c#" runat="server">

private static Log log = new Log();

protected override void OnPreInit(EventArgs e)
{
    ModulePatchHelper.SetIECompatibilityRendering(9);
    base.OnPreInit(e);
}

private bool IsIntegrationInitialized() 
{
    var settingKey = "IncidentIntegration-Enabled";
    try 
    {
        var setting = SettingsDAL.GetSetting(settingKey);
        return setting.SettingValue != 0d;

    } catch (Exception e)
    {
        log.WarnFormat("Error retrieving setting value for key {0}: {1}", settingKey, e);
    }
    
    return false;
}

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPlaceholder" runat="Server">
    <style type="text/css">
    .x-grid3-cell-inner { text-overflow: clip;}
    html.RenderPdf .sw-pref-options { display: none;} /*hide menu on pdf file*/
    </style>

    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include ID="Include2" runat="server" File="OrionCore.js" />

    <!-- Include filter extension -->
    <link rel="stylesheet" type="text/css" href="/Orion/js/extjs/3.4/ux/gridfilters/css/GridFilters.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/js/extjs/3.4/ux/gridfilters/css/RangeMenu.css" />
    <orion:Include ID="Include6" runat="server" File="extjs/3.4/ux/gridfilters/menu/RangeMenu.js" />
    <orion:Include ID="Include7" runat="server" File="extjs/3.4/ux/gridfilters/menu/ListMenu.js" />
    <orion:Include ID="Include8" runat="server" File="extjs/3.4/ux/gridfilters/GridFilters.js" />
    <orion:Include ID="Include9" runat="server" File="extjs/3.4/ux/gridfilters/filter/Filter.js" />
    <orion:Include ID="Include10" runat="server" File="extjs/3.4/ux/gridfilters/filter/StringFilter.js" />
    <orion:Include ID="Include13" runat="server" File="extjs/3.4/ux/gridfilters/filter/DateFilter.js" />
    <orion:Include ID="Include14" runat="server" File="extjs/3.4/ux/gridfilters/filter/ListFilter.js" />
    <orion:Include ID="Include15" runat="server" File="extjs/3.4/ux/gridfilters/filter/NumericFilter.js" />
    <orion:Include ID="Include16" runat="server" File="extjs/3.4/ux/gridfilters/filter/BooleanFilter.js" />
    <orion:Include ID="Include17" runat="server" File="extjs/3.4/ux/gridfilters/filter/DateTimeFilter.js" />

    <orion:Include ID="Include3" runat="server" File="Admin/js/SearchField.js" />
    <orion:Include ID="Include5" runat="server" File="Admin/js/ActiveAlertsGrid.js" />
    <orion:Include ID="Include18" runat="server" File="ActiveAlert.css" />
    <script type="text/javascript">
        window.IsOrionAdminPage = 0;
        Ext.namespace('SW');
        Ext.namespace('SW.Core');
        Ext.namespace('SW.Core.Alerts');
        SW.Core.Alerts.IsFederationEnabled = <%= SwisFederationInfo.IsFederationEnabled.ToString().ToLower() %>;
        SW.Core.Alerts.IsDemoServer = <%= SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer.ToString().ToLower() %>;
        SW.Core.Alerts.IsIncidentsIntegrationEnabled = <%= IsIntegrationInitialized().ToString().ToLower() %>;
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopRightPageLinks" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceholder" runat="Server">
    <div id="alertGrid">
        <orion:AcknowledgeAlertDialog ID="acknowledgeAlertDialog" runat="server" />
        <orion:ClearTriggeredAlertsControl ID="clearTriggeredAlertsControl" runat="server"/>
        <orion:EditAlertDefinition runat="server" ID="editAlertDefinition" />
        <div>
            <div class="sw-hdr-links">
                <table>
                    <tr>
                        <% if (Profile.AllowAlertManagement && !SwisFederationInfo.IsFederationEnabled)
                           {%>
                        <td>
                            <a href="/Orion/Alerts/Default.aspx" style="background: transparent url(/Orion/images/ActiveAlerts/ManageAlerts.gif) scroll no-repeat left center; padding: 2px 0px 2px 20px; font-size: 8pt;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_128) %></a>
                        </td>
                        <td>
                            <img src="/Orion/images/ActiveAlerts/divider.gif" style="margin-left: 3pt; margin-right: 3pt;" />
                        </td>
                        <% } %>
						<td>
							<a target="_blank" href='<%= DefaultSanitizer.SanitizeHtml(UrlHelper.UrlAppendUpdateParameter(HttpUtility.HtmlAttributeEncode(HttpContext.Current.Request.Url.AbsoluteUri), "isNOCView", "true")) %>' class="viewInNOCLink" style="font-size: 8pt;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_42) %></a>
						</td>
						<td>
                            <img src="/Orion/images/ActiveAlerts/divider.gif" style="margin-left: 3pt; margin-right: 3pt;" />
                        </td>
                        <td>
                            <table>
                                <tr class="sw-preference-row">
                                    <td class="sw-preference">
                                        <span class="sw-preference-more" style="margin-left: 27px; padding-top: 1px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_140) %></span>
                                        <div class="sw-pref-options sw-pref-options-hidden">
                                            <table>
                                                <tr class="sw-pref-section">
                                                    <td colspan="2" class="sw-pref-section-header"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_139) %></td>
                                                </tr>
                                                <tr class="sw-pref-item">
                                                    <td class="sw-pref-action">
                                                        <input type="checkbox" id="HideAcknowledgedAlerts" name="HideAcknowledgedAlerts" /></td>
                                                    <td>
                                                        <label for="HideAcknowledgedAlerts"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_141) %></label></td>
                                                </tr>
                                                <tr class="sw-pref-item">
                                                    <td class="sw-pref-action">
                                                        <input type="checkbox" id="PauseAutoRefresh" name="PauseAutoRefresh" /></td>
                                                    <td>
                                                        <label for="PauseAutoRefresh"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_142) %></label></td>
                                                </tr>
                                                <tr class="sw-pref-item sw-pauseActionsForAllItems">
                                                    <td class="sw-pref-action">
                                                        <input type="checkbox" id="PauseActionsOfAllAlerts" name="PauseActionsOfAllAlerts" /></td>
                                                    <td>
                                                        <label for="PauseActionsOfAllAlerts"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_143) %></label></td>
                                                </tr>
                                                <% if (Profile.AllowEventClear) { %>
                                                <tr class="sw-pref-item">
                                                    <td class="sw-pref-action">
                                                        <input type="checkbox" id="DontAskForNoteWhenAcknowledgeAlert" name="DontAskForNoteWhenAcknowledgeAlert" /></td>
                                                    <td>
                                                        <label for="DontAskForNoteWhenAcknowledgeAlert"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_144) %></label></td>
                                                </tr>
                                                <% } %>
                                                <tr class="sw-pref-item">
                                                    <td colspan="2">
                                                        <hr style="border-color: #E1E1E1; border-style: solid; border-bottom-style: none; border-width: 1px;" />
                                                    </td>
                                                </tr>
                                                <tr class="sw-pref-item">
                                                    <td colspan="2" class="sw-pref-action"><a href="#" style="margin-left: 0px;" class="exportToPdf" id="ExportToPdf" onclick="ExportToPDF(); return false;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_146) %></a></td>
                                                </tr>
                                                <tr class="sw-pref-item">
                                                    <td colspan="2" class="sw-pref-action"><a href="/Orion/NetPerfMon/Alerts.aspx?Printable=true" style="margin-left: 0px;" class="sw-hdr-links-print sw-hdr-links-print-image"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_147) %></a></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                    </tr>
                </table>
            </div>
            <h1 class="page-title" style="padding: 0px 0px 0px 0px!important; font-size: 14pt;"><%= DefaultSanitizer.SanitizeHtml(this.Title) %></h1>
        </div>
          <div id="SwisFErrorControlPlaceHolder" style="display: none; width: 98%"><swisf:swiserrorcontrol runat="server" id="SwisErrorControl"  ClientIDMode="Static" /></div>
        <div id="notifications" class="sw-notifications" style="margin-top: 5px;"></div>
        <div id="filterNotifications" class="sw-filter-notifications" style="margin-top: 5px;"></div>
        <div id="activeAlertsPlaceholder" class="sw-alertGrid"></div>
        <div>
            <input type="hidden" name="ActiveAlerts_SortOrder" id="ActiveAlerts_SortOrder" value='<%= HttpUtility.HtmlEncode(WebUserSettingsDAL.Get("ActiveAlerts_SortOrder"))%>' />
            <input type="hidden" name="ActiveAlerts_PageSize" id="ActiveAlerts_PageSize" value='<%= HttpUtility.HtmlEncode(WebUserSettingsDAL.Get("ActiveAlerts_PageSize"))%>' />
            <input type="hidden" name="ActiveAlerts_GroupingValue" id="ActiveAlerts_GroupingValue" value='<%= HttpUtility.HtmlEncode(WebUserSettingsDAL.Get("ActiveAlerts_GroupingValue"))%>' />
            <input type="hidden" name="ActiveAlerts_SelectedColumns" id="ActiveAlerts_SelectedColumns" value='<%= HttpUtility.HtmlEncode(WebUserSettingsDAL.Get("ActiveAlerts_SelectedColumns"))%>' />
            <input type="hidden" name="ActiveAlerts_AllowEventClear" id="ActiveAlerts_AllowEventClear" value='<%= this.Profile.AllowEventClear %>' />
            <input type="hidden" name="ActiveAlerts_AllowDisableAllActions" id="ActiveAlerts_AllowDisableAllActions" value='<%= this.Profile.AllowDisableAllActions %>' />
            <input type="hidden" name="ActiveAlerts_AllowAlertManagement" id="ActiveAlerts_AllowAlertManagement" value='<%= this.Profile.AllowAlertManagement %>' /> 
            <input type="hidden" name="ActiveAlerts_WithoutAcknowledgedAlerts" id="ActiveAlerts_WithoutAcknowledgedAlerts" value='<%= DefaultSanitizer.SanitizeHtml(WebUserSettingsDAL.Get("ActiveAlerts_WithoutAcknowledgedAlerts")) %>' />
            <input type="hidden" name="ActiveAlerts_PauseActionsOfAllAlerts" id="ActiveAlerts_PauseActionsOfAllAlerts" value='<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting("AlertEngine-PauseActionsOfAllAlerts") != null ? SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting("AlertEngine-PauseActionsOfAllAlerts").SettingValue : 0) %>' />
            <input type="hidden" name="ActiveAlerts_AutoRefreshInterval" id="ActiveAlerts_AutoRefreshInterval" value='<%= DefaultSanitizer.SanitizeHtml(WebSettingsDAL.Get("Auto Refresh Active Alerts", "1")) %>' />
            <input type="hidden" name="ActiveAlerts_AutoRefreshEnabled" id="ActiveAlerts_AutoRefreshEnabled" value='<%= DefaultSanitizer.SanitizeHtml(WebUserSettingsDAL.Get("ActiveAlerts_AutoRefreshEnabled")) %>' />
            <input type="hidden" name="ActiveAlerts_LastUsedFilters" id="ActiveAlerts_LastUsedFilters" value='<%= HttpUtility.HtmlEncode(WebUserSettingsDAL.Get("ActiveAlerts_LastUsedFilters")) %>' />
            <%
                var customProperties = SwisFederationInfo.IsFederationEnabled
                    ? SolarWinds.Orion.Core.Common.Alerting.AlertsHelper.GetCustomPropertiesForActiveAlertsSwis("Orion.AlertConfigurationsCustomProperties")
                    : SolarWinds.Orion.Core.Common.CustomPropertyMgr.GetCustomPropertiesForEntity("Orion.AlertConfigurationsCustomProperties");
                string strCustomPropertiesConfig = string.Empty;
                if (customProperties != null && customProperties.Any())
                {
                    Dictionary<string, string>[] customPropertiesConfig = new Dictionary<string, string>[customProperties.Count()];
                    int i = 0;
                    foreach (var cp in customProperties)
                    {
                        customPropertiesConfig[i] = new Dictionary<string, string>();
                        customPropertiesConfig[i].Add("PropertyName", cp.PropertyName);
                        customPropertiesConfig[i].Add("PropertyType", cp.PropertyType.ToString());
                        i++; 
                    }

                    strCustomPropertiesConfig = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(customPropertiesConfig);
                }
            %>
            <input type="hidden" name="ActiveAlerts_CustomPropertiesConfig" id="ActiveAlerts_CustomPropertiesConfig" value='<%= DefaultSanitizer.SanitizeHtml(strCustomPropertiesConfig) %>' />
         </div>
    </div>
</asp:Content>
