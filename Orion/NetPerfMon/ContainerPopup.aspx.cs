﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using System.Data;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Shared;

public partial class Orion_NetPerfMon_ContainerPopup : System.Web.UI.Page
{
    private Container container = null;
    protected const int TopItemsCount = 5;
    protected List<ContainerMember> TopItems { get; set; }
    protected int RemainingItemCount { get; set; }
    protected bool GroupIsEmpty { get; set; }

    protected Container Container 
    {
        get
        {
            if (container == null)
            {
                container = NetObjectFactory.Create(Request["NetObject"]) as Container;
            }

            return container;
        }
    }

    protected string StatusCssClass
    {
        get
        {
            switch (Container.Status.StatusId)
            {
                case StatusInfo.UnknownStatusId:
                    return "StatusUnknown";
                case StatusInfo.UpStatusId:
                    return "StatusUp";
                case StatusInfo.DownStatusId:
                    return "StatusDown";
                case StatusInfo.WarningStatusId:
                    return "StatusWarning";
                case StatusInfo.CriticalStatusId:
                    return "StatusCritical";
                case StatusInfo.ExternalStatusId:
                    return "StatusExternal";
                case StatusInfo.UnmanagedStatusId:
                    return "StatusUnmanaged";
                case StatusInfo.UnreachableStatusId:
                    return "StatusUnreachable";
                case StatusInfo.NotRunningStatusId: // Introduced in SAM 6.2, used by Components
                    return "StatusNotRunning";
                default:
                    return "StatusUnknown";
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        GroupIsEmpty = (Container.Members.Count == 0);

        var items = Container.ItemsWithProblems;
        TopItems = items.Take(TopItemsCount).ToList();
        RemainingItemCount = items.Count - TopItemsCount;

        itemsRep.DataSource = TopItems;
        itemsRep.DataBind();

        statusIcon.Entity = Container.Entity;
        statusIcon.Status = Container.Status.StatusId;
        
        this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
    }

    protected string MoreItemsMessage
    {
        get
        {
            if(RemainingItemCount==1)
            {
                return String.Format(Resources.CoreWebContent.WEBCODE_VB0_228, RemainingItemCount);
            }

            return String.Format(Resources.CoreWebContent.WEBCODE_VB0_229, RemainingItemCount);
        }
    }
}
