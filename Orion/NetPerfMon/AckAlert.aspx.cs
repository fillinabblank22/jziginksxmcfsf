using System;
using System.Collections.Generic;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

public partial class AckAlert : System.Web.UI.Page
{
	private static readonly Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

	private static void BusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}

	protected override void OnInit(EventArgs e)
	{

		if (!Profile.AllowEventClear)
		{
			log.ErrorFormat("Account {0} without permissions has tried to acknowledge alert. Operation canceled", Profile.UserName);
			throw new Exception("You are not allowed to acknowledge alerts!");
		}

		string alertKey = this.Request.QueryString["AlertDefID"];
	    var alertKeys = new string[0];
	    if (!string.IsNullOrEmpty(alertKey))
	        alertKeys = alertKey.Split(':');

		if (string.IsNullOrEmpty(alertKey))
		{
            log.ErrorFormat("Incorrectly formatted Alert key [{0}]. Should be [AlertDefID:ObjID:ObjType] or [ObjID]", alertKey);
            throw new ArgumentException(string.Format("Incorrectly formatted Alert key value [{0}]. Should be [AlertDefID:ObjID:ObjType] or [ObjID]", alertKey));
		}

		bool viaEmail = string.IsNullOrEmpty(this.Request.QueryString["viaEmail"]) ? false : bool.Parse(this.Request.QueryString["viaEmail"]);

        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
		{
		    if (alertKeys.Length > 1)
		    {
		        proxy.AcknowledgeAlerts(new List<string> {alertKey}, Profile.UserName, viaEmail);
		    }
		    else
		    {
		        int objectId;
		        if (int.TryParse(alertKey, out objectId))
		        {
		            proxy.AcknowledgeAlertsV2(new List<int> {objectId}, Profile.UserName, null, DateTime.UtcNow);
		        }
		        else
		        {
                    log.ErrorFormat("Incorrectly type of ObjID value {0}. Should be integer", alertKey);
                    throw new ArgumentException(string.Format("Incorrectly type of ObjID value {0}. Should be integer", alertKey));
		        }
		    }
		}
		Response.Redirect("Alerts.aspx");
	}
}

