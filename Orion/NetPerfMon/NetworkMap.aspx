﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="NetworkMap" %>
<%@ Import Namespace="SolarWinds.MapEngine" %>
<%@ Import Namespace="SolarWinds.MapEngine.Helpers" %>
<%@ Register Assembly="OrionWeb" Namespace="SolarWinds.Orion.Web.DAL" TagPrefix="web" %>

<script runat="server">
    protected override void OnInit(EventArgs e)
    {
        RootLocalPath = MapService.GetTempPath();
        ApplicationLocalPath = MapService.GetMapStudioInstallPath();
        TargetDir = RootLocalPath;

        string swoisEndpoint = string.Format(System.Configuration.ConfigurationManager.AppSettings["SWOISv3.RemoteEndpoint"], "localhost");
        string serverAddress, swisPort, swisPostfix;
        MapService.ParseEndpointLocation(swoisEndpoint, out serverAddress, out swisPort, out swisPostfix);

        ServerAddress = serverAddress;
        SwisPort = swisPort;
        SwisPostfix = swisPostfix;
        UserName = HttpContext.Current.Profile.UserName;
        AccountId = HttpContext.Current.Profile.UserName;
        TranslateMacros = Macros.TranslateLabelMacros;
        if (UsernameCredentials == null)
        {
            UsernameCredentials = CredentialsHelper.GetOrionCertificateCredentials();
        }
        
        var viewInfo = ((ViewInfo)HttpContext.Current.Items[typeof(ViewInfo).Name]);
        ViewId = viewInfo == null ? 0 : viewInfo.ViewID;
        
        base.OnInit(e);
    }
</script>
