﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Logging;

public partial class Orion_NetPerfMon_GroupMemberPopUp : System.Web.UI.Page
{
    private readonly Log log = new Log();

    protected void Page_Load(object sender, EventArgs e)
    {
        GroupMember member = NetObjectFactory.Create(Request["NetObject"]) as GroupMember;
        
        if (member.NestedObject != null)
        {
            Dictionary<string, string> tipMap = NetObjectFactory.GetNetObjectTipMap();

            string[] prefixFragments = member.NestedObject.NetObjectID.Split(':');

			if (prefixFragments.Length > 1)
            {
				var url = tipMap[prefixFragments.First()];
				if (url == null)
				{
					Response.Write("NoTip");
				}
				else
				{
					Response.Redirect(String.Format("{0}?NetObject={1}", url, member.NestedObject.NetObjectID));
				}
            }
            else
            {
                log.WarnFormat("Unable to get tool tip redirection for netobject {0}", Request["NetObject"]);
            }
        }
        else
        {
            log.WarnFormat("Unable create netobject {0}", Request["NetObject"]);
        }
    }
}
