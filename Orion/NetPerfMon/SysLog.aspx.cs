using System;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.Core.Web.Reporting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using Limitation = SolarWinds.Orion.Web.Limitation;
using SettingsDAL = SolarWinds.Orion.Web.DAL.SettingsDAL;
using SyslogTrapsReporting = SolarWinds.SyslogTraps.Web.Reporting;

public partial class Orion_NetPerfMon_SysLog : System.Web.UI.Page
{
    private static readonly Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
    private string tag;
    private string relatedIP;
    private string relatedMAC;

    protected string validatorText = String.Format("<img src=\"/Orion/images/Small-Down.gif\" title=\"{0}\" />",
                                             Resources.SyslogTrapsWebContent.WEBDATA_VB0_220);

    private string GetLocalizedPeriod
    {
        get
        {
            if (Period.Contains("~"))
            {
                return Period;
            }
            return Periods.GetLocalizedPeriod(Period);
        }
    }

    public string Period
    {
        get
        {

            if (pickerTimePeriodControl.TimePeriodText == "Custom")
            {
                if (string.IsNullOrEmpty(pickerTimePeriodControl.CustomPeriodBegin))
                {
                    pickerTimePeriodControl.CustomPeriodBegin = "0:00:00";
                }
                if (string.IsNullOrEmpty(pickerTimePeriodControl.CustomPeriodEnd))
                {
                    pickerTimePeriodControl.CustomPeriodEnd = "23:59:59";
                }

                return pickerTimePeriodControl.CustomPeriodBegin + "~" + pickerTimePeriodControl.CustomPeriodEnd;
            }
            else
            {
                return pickerTimePeriodControl.TimePeriodText;
            }
        }
    }

    protected void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

    private int _maxRecords;

    public string Title2
    {
        get
        {
            if (string.IsNullOrEmpty(netObjects.NetObjectID))
            {
                if (string.IsNullOrEmpty(netObjectTypes.DeviceType))
                {
                    return Resources.SyslogTrapsWebContent.WEBCODE_VB0_178 + " - " + GetLocalizedPeriod;
                }
                else
                {
                    return string.Format(Resources.SyslogTrapsWebContent.WEBCODE_VB0_179, netObjectTypes.DeviceType) + " - " + GetLocalizedPeriod;
                }
            }
            else
            {
                using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
                {
                    Node node = proxy.GetNode(Convert.ToInt32(NetObjectHelper.GetObjectID(netObjects.NetObjectID)));
                    return Resources.SyslogTrapsWebContent.WEBCODE_VB0_180 + node.Name + " - " + GetLocalizedPeriod;
                }
            }
        }
    }

    private void PopulateControlsFromRequest()
    {
        if (Request.QueryString["MessageType"] != null)
            messageType.Text = Request.QueryString["MessageType"];

        if (Request.QueryString["IP"] != null || Request.QueryString["IPAddress"] != null)
            ipAddress.Text = Request.QueryString["IP"] ?? Request.QueryString["IPAddress"];

        if (Request.QueryString["IP"] != null)
            ipAddress.Text = Request.QueryString["IP"];

        if (Request.QueryString["Hostname"] != null)
            netObjects.HostName = Request.QueryString["Hostname"];

        if (Request.QueryString["NetObject"] != null)
            netObjects.NetObjectID = Request.QueryString["NetObject"];

        if (Request.QueryString["FacilityName"] != null)
            facilities.FacilityName = Request.QueryString["FacilityName"];

        if (Request.QueryString["SeverityName"] != null)
            severities.SeverityName = Request.QueryString["SeverityName"];

        if (Request.QueryString["Severity"] != null || Request.QueryString["SeverityCode"] != null)
            severities.SeverityCode = Request.QueryString["Severity"] ?? Request.QueryString["SeverityCode"];

        if (Request.QueryString["FacilityCode"] != null)
            facilities.FacilityCode = Request.QueryString["FacilityCode"];

        if (Request.QueryString["SyslogTag"] != null)
            tag = Request.QueryString["SyslogTag"];

        if (Request.QueryString["Vendor"] != null)
            vendors.VendorName = Request.QueryString["Vendor"];

        if (Request.QueryString["MachineType"] != null)
        {
            netObjectTypes.DeviceType = Request.QueryString["MachineType"];
        }
        else
        {
            if (Request.QueryString["DeviceType"] != null)
                netObjectTypes.DeviceType = Request.QueryString["DeviceType"];
        }

        if (Request.QueryString["Message"] != null || Request.QueryString["MessagePattern"] != null)
            messagePattern.Text = Request.QueryString["Message"] ?? Request.QueryString["MessagePattern"];

        if (Request.QueryString["RelatedIP"] != null)
            relatedIP = Request.QueryString["RelatedIP"];

        if (Request.QueryString["RelatedMAC"] != null)
            relatedMAC = Request.QueryString["RelatedMAC"];

        if (Request.QueryString["HideDetails"] != null)
        {
            var hideDetails = bool.Parse(Request.QueryString["HideDetails"]);
            btnShow.Visible = hideDetails;
            btnHide.Visible = !hideDetails;
            details.Visible = !hideDetails;
        }
    }

    private void PopulateChildControls()
    {
        if (!Int32.TryParse(maxRecords.Text, out _maxRecords))
        {
            _maxRecords = 0;
        }
        else
        {
            _maxRecords = Math.Min(Math.Max(_maxRecords, 0), 2000);
        }

        maxRecords.Text = _maxRecords.ToString();

        if (maxRecords.Text.Trim() != previousMaxRecords.Value.ToString())
        {
            WebUserSettingsDAL.Set("Web-SyslogMaxMsgPerPage", _maxRecords.ToString());
            previousMaxRecords.Value = _maxRecords.ToString();
        }

        if (showCleared.Checked.ToString() != previousShowCleared.Value)
        {
            previousShowCleared.Value = showCleared.Checked.ToString();
            WebUserSettingsDAL.Set("Web-ShowClearedSyslogMessages", previousShowCleared.Value);
        }

        bool printable;
        byte facilityCode = 0;
        byte severityCode = 0;
        // 255 if none selected
        if (!byte.TryParse(severities.SeverityCode, out severityCode))
            severityCode = 255;
        // 255 if none selected
        if (!byte.TryParse(facilities.FacilityCode, out facilityCode))
            facilityCode = 255;

        if ((Request.QueryString["Printable"] != null) && bool.TryParse(Request.QueryString["Printable"], out printable))
        {
            if (printable)
            {
                Printable_Click(this, null);
            }
        }

        sysLogsReport.GenerateReport(netObjects.NetObjectID, netObjectTypes.DeviceType, vendors.VendorName, ipAddress.Text, severityCode, facilityCode, messageType.Text, messagePattern.Text, tag, Period, _maxRecords, showCleared.Checked, relatedIP, relatedMAC);

        ExportToPDFLink.PageUrl = GetUrlWithParams(Request.Url.OriginalString.Split('?')[0]);
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        maxRecords.ValidatorText = validatorText;
        if (!(new RemoteFeatureManager()).IsFeatureEnabled("Syslog"))
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.SyslogTrapsWebContent.WEBCODE_VB0_328), Title = Resources.SyslogTrapsWebContent.WEBCODE_VB0_346 });
    }

    void DisplayReportInCustomFormat(ReportFormat reportFormat)
    {

        ReportSettings settings = ReportHelper.GetReportSettings(Period, netObjects.NetObjectID);
        SyslogTrapsReporting.SysLogReport report = new SyslogTrapsReporting.SysLogReport(reportFormat, settings);

        int maxRecordsToDisplay = GetMaxRecords();

        byte facilityCode = 0;
        byte severityCode = 0;

        // 255 if none selected
        if (!byte.TryParse(severities.SeverityCode, out severityCode))
            severityCode = 255;
        // 255 if none selected
        if (!byte.TryParse(facilities.FacilityCode, out facilityCode))
            facilityCode = 255;

        report.GenerateReport(netObjectTypes.DeviceType, vendors.VendorName, ipAddress.Text,
            severityCode, facilityCode, messageType.Text,
            messagePattern.Text, tag, maxRecordsToDisplay, showCleared.Checked, relatedIP, relatedMAC,
            Limitation.GetCurrentListOfLimitationIDs(), BusinessLayerExceptionHandler);
        ReportHelper.WriteReportToResponse(Response, report);
    }

    private int GetMaxRecords()
    {
        int maxRecordsToDisplay = 0;
        if (!Int32.TryParse(maxRecords.Text, out maxRecordsToDisplay))
        {
            maxRecordsToDisplay = 0;
        }
        else if (_maxRecords < 0)
        {
            maxRecordsToDisplay = 0;
        }
        return maxRecordsToDisplay;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ////debug
        //InitializeMaxRecords();
        //DisplayReportInCustomFormat(ReportFormat.XML);
        //return;

        if (!IsPostBack)
        {
            InitializeMaxRecords();

            String showClearedMessagesValue = (Request.QueryString["ShowCleared"] != null) ? Request.QueryString["ShowCleared"].ToString() : WebUserSettingsDAL.Get("Web-ShowClearedSyslogMessages");
            if (String.IsNullOrEmpty(showClearedMessagesValue))
            {
                showCleared.Checked = false;
                WebUserSettingsDAL.Set("Web-ShowClearedSyslogMessages", false.ToString());
            }
            else
            {
                showCleared.Checked = bool.Parse(showClearedMessagesValue);
            }
            previousShowCleared.Value = showCleared.Checked.ToString();

            ReportFormat? reportFormat = ReportHelper.GetRequestedFormat(Request);
            if (reportFormat != null && reportFormat.Value != ReportFormat.HTML)
            {
                DisplayReportInCustomFormat(reportFormat.Value);
            }
        }
    }

    private void InitializeMaxRecords()
    {
        String userSettingValue = (Request.QueryString["MaxMessages"] != null) ? Request.QueryString["MaxMessages"].ToString() : WebUserSettingsDAL.Get("Web-SyslogMaxMsgPerPage");
        if (String.IsNullOrEmpty(userSettingValue))
        {
            String defaultSettingValue = SettingsDAL.GetSetting("Web-SyslogMaxMsgPerPage").SettingValue.ToString();
            maxRecords.Text = defaultSettingValue;
            WebUserSettingsDAL.Set("Web-SyslogMaxMsgPerPage", defaultSettingValue);
        }
        else
        {
            maxRecords.Text = userSettingValue;
        }
        previousMaxRecords.Value = maxRecords.Text;
    }

    protected override void OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);

        if (!IsPostBack)
        {
            PopulateControlsFromRequest();
        }
        PopulateChildControls();
    }

    protected void Refresh_Click(object sender, EventArgs e)
    {
    }

    protected void Show_Click(object sender, EventArgs e)
    {
        if (!details.Visible)
        {
            details.Visible = true;
            btnHide.Visible = true;
            btnShow.Visible = false;
        }
        else
        {
            details.Visible = false;
            btnHide.Visible = false;
            btnShow.Visible = true;
        }
    }

    private string GetUrlWithParams(string pageUrl)
    {
        return string.Format(@"{0}?NetObject={1}&DeviceType={2}&SeverityCode={3}&FacilityCode={4}&MessageType={5}&MessagePattern={6}&Period={7}&MaxRecords={8}&ShowCleared={9}&IP={10}&Vendor={11}&SyslogTag={12}&RelatedIP={13}&RelatedMAC={14}&HideDetails={15}",
            pageUrl,
            netObjects.NetObjectID,
            netObjectTypes.DeviceType,
            severities.SeverityCode,
            facilities.FacilityCode,
            Server.UrlEncode(messageType.Text),
            Server.UrlEncode(messagePattern.Text),
            Period,
            maxRecords.Text,
            showCleared.Checked,
            Server.UrlEncode(ipAddress.Text),
            vendors.VendorName,
            Server.UrlEncode(tag),
            Server.UrlEncode(relatedIP),
            Server.UrlEncode(relatedMAC),
            !details.Visible);
    }

    protected void Printable_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetUrlWithParams("PrintableSysLog.aspx"));
    }
}
