<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NodePopup.aspx.cs" Inherits="Orion_NetPerfMon_NodePopup" %>

<%@ Import Namespace="SolarWinds.Orion.Web.Enums" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>
<%@ Register TagPrefix="npm" TagName="ResponseTime" Src="~/Orion/NetPerfMon/Controls/ResponseTime.ascx" %>
<%@ Register TagPrefix="npm" TagName="PercentLoss" Src="~/Orion/NetPerfMon/Controls/PercentLoss.ascx" %>
<%@ Register TagPrefix="npm" TagName="CPULoad" Src="~/Orion/NetPerfMon/Controls/CPULoad.ascx" %>
<%@ Register TagPrefix="npm" TagName="MemoryUsed" Src="~/Orion/NetPerfMon/Controls/MemoryUsed.ascx" %>
<%--This is intended to be displayed as the body of a cluetip tooltip/popup.--%>
<h3 class="Status<%= DefaultSanitizer.SanitizeHtml(Node.Status.ToString("parentstatus", null)) %>">
    <%=HttpUtility.HtmlEncode(SolarWinds.Orion.Core.Web.FormatHelper.MakeBreakableString(Node.Name))%></h3>
<div class="NetObjectTipBody">
    <p class="StatusDescription">
        <%= DefaultSanitizer.SanitizeHtml(GetRevisedStatusDescription()) %></p>
    <table cellpadding="0" cellspacing="0">
        <tr runat="server" ID="AlertsMuted">
	        <th><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ToolTip_AlertStatus) %></th>
            <td colspan="2"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ToolTip_AlertMuted) %></td>
	    </tr>
        <tr>
            <th>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_34) %>
            </th>
            <td colspan="2">
                <%= DefaultSanitizer.SanitizeHtml(Node.IPAddressString) %>
            </td>
        </tr>
        <tr>
            <th>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_192) %>
            </th>
            <td colspan="2">
                <img src="<%= DefaultSanitizer.SanitizeHtml(GetVendorIcon()) %>" />
                <%= DefaultSanitizer.SanitizeHtml(Node.MachineType) %>
            </td>
        </tr>
        <%if (!Node.External)
           {%>
        <tr>
            <th>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_193) %>
            </th>
            <td>
                <npm:ResponseTime runat="server" ID="AvgResponseTime" />
            </td>
            <orion:InlineBar runat="server" ID="AvgResponseTimeBar" />
        </tr>
        <%if (!IsThresholdValuesForBarExist(ResponseTimeName))
        {%>
        <tr>
            <td class="ThresholdValuesNotExistMessage" colspan = "3">
                <%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBDATA_IK0_1, Resources.CoreWebContent.WEBDATA_AK0_193.TrimEnd(':'))) %>
            </td>
        </tr>
        <%}%> 
        <%if (HasOverviewStyle(NodeOverviewStyle.AvgResponseTime, NodeOverviewStyle.ResponseTime, NodeOverviewStyle.MaxResponseTime))
          {%>
        <tr>
            <th>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_194) %>
            </th>
            <td>
                <npm:ResponseTime runat="server" ID="MinResponseTime" />
            </td>
            <orion:InlineBar runat="server" ID="MinResponseTimeBar" />
        </tr>
        <%if (!IsThresholdValuesForBarExist(ResponseTimeName))
        {%>
        <tr>
            <td class="ThresholdValuesNotExistMessage" colspan = "3">
                <%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBDATA_IK0_1, Resources.CoreWebContent.WEBDATA_AK0_194.TrimEnd(':'))) %>
            </td>
        </tr>
        <%}%> 
        <tr>
            <th>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_195) %>
            </th>
            <td>
                <npm:ResponseTime runat="server" ID="MaxResponseTime" />
            </td>
            <orion:InlineBar runat="server" ID="MaxResponseTimeBar" />
        </tr>
        <%if (!IsThresholdValuesForBarExist(ResponseTimeName))
        {%>
        <tr>
            <td class="ThresholdValuesNotExistMessage" colspan = "3">
                <%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBDATA_IK0_1, Resources.CoreWebContent.WEBDATA_AK0_195.TrimEnd(':'))) %>
            </td>
        </tr>
        <%}%> 
        <tr>
            <th>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_196) %>
            </th>
            <td>
                <npm:ResponseTime runat="server" ID="ResponseTime" />
            </td>
            <orion:InlineBar runat="server" ID="ResponseTimeBar" />
        </tr>
        <%if (!IsThresholdValuesForBarExist(ResponseTimeName))
        {%>
        <tr>
            <td class="ThresholdValuesNotExistMessage" colspan = "3">
                <%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBDATA_IK0_1, Resources.CoreWebContent.WEBDATA_AK0_196.TrimEnd(':'))) %>
            </td>
        </tr>
        <%}%> 
        <%}%>
          <%if (Node.PercentLoss != -2)
          {%>
        <tr>
            <th>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_200) %>
            </th>
            <td>
                <npm:PercentLoss runat="server" ID="PercentLoss" />
            </td>
            <orion:InlineBar runat="server" ID="PercentLossBar" />
        </tr>
        <%if (!IsThresholdValuesForBarExist(PercentLossName))
        {%>
        <tr>
            <td class="ThresholdValuesNotExistMessage" colspan = "3">
                <%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBDATA_IK0_1, Resources.CoreWebContent.WEBDATA_AK0_200.TrimEnd(':'))) %>
            </td>
        </tr>
        <%}%> 

         <%}%>
        <%}%>
        <%if (Node.CPULoad != -2)
          {%>
        <tr>
            <th>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_198) %>
            </th>
            <td>
                <npm:CPULoad runat="server" ID="CPULoad" />
            </td>
            <orion:InlineBar runat="server" ID="CPULoadBar" />
        </tr>
        <%if (!IsThresholdValuesForBarExist(CpuLoadName))
        {%>
        <tr>
            <td class="ThresholdValuesNotExistMessage" colspan = "3">
                <%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBDATA_IK0_1, Resources.CoreWebContent.WEBDATA_AK0_198.TrimEnd(':'))) %>
            </td>
        </tr>
        <%}%> 
        <%}%>
        <%if (Node.MemoryUsed != -2)
          {%>
        <tr>
            <th>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_199) %>
            </th>
            <td>
                <npm:MemoryUsed runat="server" ID="MemoryUsed" />
            </td>
            <orion:InlineBar runat="server" ID="MemoryUsedBar" />
        </tr>
        <%if (!IsThresholdValuesForBarExist(MemoryUsedName))
        {%>
        <tr>
            <td class="ThresholdValuesNotExistMessage" colspan = "3">
                <%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBDATA_IK0_1, Resources.CoreWebContent.WEBDATA_AK0_199.TrimEnd(':'))) %>
            </td>
        </tr>
        <%}%> 
        <%}%>
        <asp:PlaceHolder runat="server" id="extensionPlaceholder" />
    </table>
    <asp:PlaceHolder runat="server" id="oldExtensionPlaceholder" />
</div>
