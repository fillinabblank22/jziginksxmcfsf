<%@ Page Language="C#" Title="<%$ Resources: SyslogTrapsWebContent, WEBDATA_VB0_233 %>" AutoEventWireup="true" CodeFile="PrintableSysLog.aspx.cs" Inherits="Orion_NetPerfMon_PrintableSysLog" MasterPageFile="~/Orion/OrionMinReqs.master" %>
<%@ Register TagPrefix="orion" TagName="SysLogsReport" Src="~/Orion/Controls/SysLogsReportControl.ascx" %>

<asp:Content ContentPlaceHolderID="BodyContent" runat="server">
    <orion:Include runat="server" File="MainLayout.css" />
    <orion:Include runat="server" File="Resources.css" />

    <form id="form1" runat="server" class="sw-app-region">
    <h1>
        <%=Title2 %>
    </h1>
    <div style="margin:10px 10px 10px 10px">
        <orion:SysLogsReport runat="server" ID="sysLogsReport" Printable="true" />
    </div>
    </form>

</asp:Content>
