using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.DAL;
using SettingsDAL = SolarWinds.Orion.Web.DAL.SettingsDAL;
using System.Web.UI.WebControls;
using System.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.Web.SyslogTrapInterfaces;
using SolarWinds.Orion.Web.Plugins;
using System.Web.UI;

public partial class Orion_NetPerfMon_OrionMessages : System.Web.UI.Page
{
    private static readonly Log _log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
    private static readonly RemoteFeatureManager _featureManager = new RemoteFeatureManager();
    private bool _syslogsEnabled;
    private bool _trapsEnabled;
    private bool _auditEnabled;

    private ISeverityControl syslogSeveritiesControl;
    private IFacilityControl syslogFacilitiesControl;
    private ITrapTypeControl trapTypesControl;
    private ICommunityStringControl trapCommunityControl;

    protected string validatorText = String.Format("<img src=\"/Orion/images/Small-Down.gif\" title=\"{0}\" />",
                                             Resources.CoreWebContent.WEBDATA_VB0_220);

    protected void BusinessLayerExceptionHandler(Exception ex)
    {
        _log.Error(ex);
    }

    public Dictionary<string, string> EventTypes
    {
        get
        {
            object val = this.ViewState["EventTypes"];
            if (val != null && val is Dictionary<string, string>)
            {
                return (Dictionary<string, string>)val;
            }

            Dictionary<string, string> eventTypes = GetEventTypes();
            this.ViewState["EventTypes"] = eventTypes;

            return eventTypes;
        }
    }

    public string SearchString
    {
        get { return searchString.Value; }
        set { searchString.Value = value; }
    }

    private Dictionary<string, string> GetEventTypes()
    {
        Dictionary<string, string> eventTypes = new Dictionary<string, string>();

        DataTable eventTypesTable = EventDAL.GetEventTypesTable(SwisFederationInfo.IsFederationEnabled);

        eventTypes.Add(string.Empty, Resources.CoreWebContent.WEBCODE_VB0_201);
        if (eventTypesTable != null)
            foreach (DataRow r in eventTypesTable.Rows)
                eventTypes.Add(r["EventType"].ToString(), r["Name"].ToString());

        return eventTypes;
    }

    protected bool HideSyslogControls { get { return !_syslogsEnabled; } }

    protected bool HideTrapsControls { get { return !_trapsEnabled; } }

    protected bool ShowAuditControls { get { return _auditEnabled; } }

    protected bool HideDatails
    {
        get
        {
            if (ViewState["CombinedHideDetails"] != null)
                return (bool)ViewState["CombinedHideDetails"];
            return false;

        }
        set
        {
            ViewState["CombinedHideDetails"] = value;
        }
    }

    private readonly IList<string> _messageTypes = new List<string> { Resources.CoreWebContent.WEBDATA_VB0_240, Resources.CoreWebContent.WEBCODE_VB0_241 };

    public string Title2
    {
        get
        {
            string titlepart = CommonWebHelper.GetTitlePart(_messageTypes);
            if (string.IsNullOrEmpty(netObjects.NetObjectID))
            {
                if (string.IsNullOrEmpty(netObjectTypes.DeviceType))
                {
                    return string.Format(Resources.CoreWebContent.WEBCODE_VB0_268, Periods.GetLocalizedPeriod(Period), titlepart);
                }
                else
                {
                    return string.Format(Resources.CoreWebContent.WEBCODE_VB0_269, netObjectTypes.DeviceType, Periods.GetLocalizedPeriod(Period), titlepart);
                }
            }
            else
            {
                using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
                {
                    Node node = proxy.GetNode(Convert.ToInt32(NetObjectHelper.GetObjectID(netObjects.NetObjectID)));
                    return string.Format(Resources.CoreWebContent.WEBCODE_JP0_1, node.Name, Periods.GetLocalizedPeriod(Period), titlepart);
                }
            }
        }
    }

    public string Period
    {
        get
        {
            if (pickerTimePeriodControl.TimePeriodText == "Custom")
            {
                if (string.IsNullOrEmpty(pickerTimePeriodControl.CustomPeriodBegin))
                    pickerTimePeriodControl.CustomPeriodBegin = "0:00:00";
                if (string.IsNullOrEmpty(pickerTimePeriodControl.CustomPeriodEnd))
                    pickerTimePeriodControl.CustomPeriodEnd = "23:59:59";

                return pickerTimePeriodControl.CustomPeriodBegin + "~" + pickerTimePeriodControl.CustomPeriodEnd;
            }
            else
            {
                return pickerTimePeriodControl.TimePeriodText;
            }
        }
    }

    private void SetSearchString()
    {
        if (!string.IsNullOrEmpty(tbSearch.Value) && !tbSearch.Value.Equals(Resources.CoreWebContent.WEBCODE_VB0_131, StringComparison.InvariantCultureIgnoreCase))
            SearchString = tbSearch.Value.Replace("<wbr>", "").Replace("\u200B", "");
        else
            SearchString = string.Empty;

    }

    private void ShowMessageTypes(string value)
    {
        cbShowAlerts.Checked = value.Contains("alert;");
        cbShowEvents.Checked = value.Contains("event;");

        if (!HideSyslogControls)
            cbShowSyslogs.Checked = value.Contains("syslog;");
        if (!HideTrapsControls)
            cbShowTraps.Checked = value.Contains("trap;");
        if(ShowAuditControls)
            cbShowAudits.Checked = value.Contains("audit;");
    }

    private string GetMessageTypesString()
    {
        string types = string.Empty;

        if (cbShowAlerts.Checked) types += "alert;";
        if (cbShowEvents.Checked) types += "event;";
        
        if (!HideSyslogControls && cbShowSyslogs.Checked) types += "syslog;";
        if (!HideTrapsControls && cbShowTraps.Checked) types += "trap;";
        if (ShowAuditControls && cbShowAudits.Checked) types += "audit;";
        return types;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadUserControls();

        this.SolarWindsServerFilterRow.Visible = SwisFederationInfo.IsFederationEnabled;
        OrionMessagesControl.SortChanged += OrionMessagesControl_SortChanged;

        _syslogsEnabled = TrapsSysLogsAvailabilityManager.Instance.IsLegacySysLogEnabled;
        _trapsEnabled = TrapsSysLogsAvailabilityManager.Instance.IsLegacyTrapsEnabled;
        _auditEnabled = Profile.AllowAdmin || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;

        if (_syslogsEnabled && !_messageTypes.Contains(Resources.CoreWebContent.WEBDATA_VB0_209))
            _messageTypes.Add(Resources.CoreWebContent.WEBDATA_VB0_209);

        if (_trapsEnabled && !_messageTypes.Contains(Resources.CoreWebContent.WEBCODE_VB0_274))
            _messageTypes.Add(Resources.CoreWebContent.WEBCODE_VB0_274);
        
        if (_auditEnabled && !_messageTypes.Contains(Resources.CoreWebContent.WEBDATA_DP0_14))
            _messageTypes.Add(Resources.CoreWebContent.WEBDATA_DP0_14);

        if (!IsPostBack)
        {
            eventTypesControl.DataSource = EventTypes;
            eventTypesControl.DataTextField = "Value";
            eventTypesControl.DataValueField = "Key";
            eventTypesControl.DataBind();

            InitializeSolarWindsServers();

            LoadFromUserSettings();
        }
    }

    private void InitializeSolarWindsServers()
    {
        if (!SwisFederationInfo.IsFederationEnabled)
            return;

        SolarwindsServersDropDown.Items.Clear();
        SolarwindsServersDropDown.Items.Add(new ListItem(Resources.CoreWebContent.Events_FilterServersSolarwindsServerDefaultValue, string.Empty));

        foreach (var site in new OrionSitesDAL().GetSites())
        {
            if (!site.Enabled)
                continue;

            SolarwindsServersDropDown.Items.Add(new ListItem(site.Name, site.SiteID.ToString()));
        }
    }

    protected override void OnLoadComplete(EventArgs e)
	{
		base.OnLoadComplete(e);
		OrionMessagesControl.ClearOrionMessage += OrionMessagesControl_ClearOrionMessage;
		if (!IsPostBack)
		{
			PopulateControlsFromRequest();
		}
		PopulateChildControls();
	}

	private void PopulateControlsFromRequest()
	{
		foreach (var key in Request.QueryString)
		{
            switch (key.ToString().ToLowerInvariant())
			{
                case "netobject": netObjects.NetObjectID = WebSecurityHelper.SanitizeAngular(Request.QueryString[key.ToString()]); break;
                case "devicetype": netObjectTypes.DeviceType = WebSecurityHelper.SanitizeAngular(Request.QueryString[key.ToString()]); break;
                case "vendor": vendors.VendorName = WebSecurityHelper.SanitizeAngular(Request.QueryString[key.ToString()]); break;
                case "ipaddress": ipAddress.Text = WebSecurityHelper.SanitizeAngular(Request.QueryString[key.ToString()]); break;
                case "hostname": hostName.Text = WebSecurityHelper.SanitizeAngular(Request.QueryString[key.ToString()]); break;
                case "maxrecords": maxRecords.Text = WebSecurityHelper.SanitizeAngular(Request.QueryString[key.ToString()]); break;
				case "showacknowledged": cbShowCleared.Checked = bool.Parse(Request.QueryString[key.ToString()]); break;
				case "showevents": cbShowEvents.Checked = bool.Parse(Request.QueryString[key.ToString()]); break;
				case "showalerts": cbShowAlerts.Checked = bool.Parse(Request.QueryString[key.ToString()]); break;
				case "showsyslogs":
					if (!HideSyslogControls)
						cbShowSyslogs.Checked = bool.Parse(Request.QueryString[key.ToString()]);
					break;
				case "showtraps":
					if (!HideTrapsControls)
						cbShowTraps.Checked = bool.Parse(Request.QueryString[key.ToString()]);
					break;
                case "showaudits":
                    if (ShowAuditControls)
                        cbShowAudits.Checked = bool.Parse(Request.QueryString[key.ToString()]);
			        break;
                case "alerttype": alertNamesControl.AlertID = WebSecurityHelper.SanitizeAngular(Request.QueryString[key.ToString()]); break;
                case "audittype": auditTypesControl.AuditTypeID = WebSecurityHelper.SanitizeAngular(Request.QueryString[key.ToString()]); break;
                case "eventtype": eventTypesControl.SelectedValue = WebSecurityHelper.SanitizeAngular(Request.QueryString[key.ToString()]); break;
                case "severity": syslogSeveritiesControl.SeverityCode = WebSecurityHelper.SanitizeAngular(Request.QueryString[key.ToString()]); break;
                case "facility": syslogFacilitiesControl.FacilityCode = WebSecurityHelper.SanitizeAngular(Request.QueryString[key.ToString()]); break;
                case "traptype": trapTypesControl.TrapType = WebSecurityHelper.SanitizeAngular(Request.QueryString[key.ToString()]); break;
                case "community": trapCommunityControl.CommunityString = WebSecurityHelper.SanitizeAngular(Request.QueryString[key.ToString()]); break;
                case "audituser": auditUser.Text = WebSecurityHelper.SanitizeAngular(Request.QueryString[key.ToString()]); break;
                case "searchstring": SearchString = WebSecurityHelper.SanitizeAngular(Request.QueryString[key.ToString()]); break;
				case "hidedetails":
					var hideDetails = bool.Parse(Request.QueryString[key.ToString()]);
					btnShow.Visible = hideDetails;
					btnHide.Visible = !hideDetails;
					details.Visible = !hideDetails;
					break;
				case "sortdir":
					OrionMessagesControl.SortDir = 
						Request.QueryString[key.ToString()].Equals(Boolean.FalseString, StringComparison.OrdinalIgnoreCase) 
						? SortDirection.Descending : SortDirection.Ascending;
					break;
				case "sortstring":
                    OrionMessagesControl.SortString = WebSecurityHelper.SanitizeAngular(Request.QueryString[key.ToString()]);
					break;
                case "siteid":
                    SolarwindsServersDropDown.SelectedValue = WebSecurityHelper.SanitizeAngular(Request.QueryString[key.ToString()]);
                    break;
			}
		}
	}

    void OrionMessagesControl_ClearOrionMessage()
    {
        PopulateChildControls();
    }

	void OrionMessagesControl_SortChanged()
	{
        ExportToPDFLink.PageUrl = GetUrlWithParams(Request.Url.OriginalString.Split('?')[0]);
		ExportToPDFPanel.Update();
	}

    private void SaveUserSettings()
    {
        if (maxRecords.Text.Trim() != previousMaxRecords.Value.ToString())
        {
            WebUserSettingsDAL.Set("Web-OrionMessagesMaxMsgPerPage", maxRecords.Text.ToString());
            previousMaxRecords.Value = maxRecords.Text.ToString();
        }
        if (cbShowCleared.Checked.ToString() != previousShowCleared.Value)
        {
            previousShowCleared.Value = cbShowCleared.Checked.ToString();
            WebUserSettingsDAL.Set("Web-ShowAcknowledgedOrionMessages", previousShowCleared.Value);
        }
        if (GetMessageTypesString() != previousShowMessageTypes.Value.ToString())
        {
            previousShowMessageTypes.Value = GetMessageTypesString();
            WebUserSettingsDAL.Set("Web-ShowOrionMessageTypes", previousShowMessageTypes.Value);
        }

        #region filter alerts
        if (alertNamesControl.AlertID != previousAlertType.Value)
        {
            previousAlertType.Value = alertNamesControl.AlertID;
            WebUserSettingsDAL.Set("Web-OrionMessage-AlertType", alertNamesControl.AlertID);
        }
        #endregion

        #region filter events
        if (eventTypesControl.SelectedValue != previousEventType.Value)
        {
            previousEventType.Value = eventTypesControl.SelectedValue;
            WebUserSettingsDAL.Set("Web-OrionMessage-EventType", eventTypesControl.SelectedValue);
        }
        #endregion

        #region filter syslog
        if (!HideSyslogControls)
        {
            if (syslogSeveritiesControl.SeverityCode != previousSyslogSeverity.Value)
            {
                previousSyslogSeverity.Value = syslogSeveritiesControl.SeverityCode;
                WebUserSettingsDAL.Set("Web-OrionMessage-SyslogSeverity", syslogSeveritiesControl.SeverityCode);
            }
            if (syslogFacilitiesControl.FacilityCode != previousSyslogFacility.Value)
            {
                previousSyslogFacility.Value = syslogFacilitiesControl.FacilityCode;
                WebUserSettingsDAL.Set("Web-OrionMessage-SyslogFacility", syslogFacilitiesControl.FacilityCode);
            }
        }
        #endregion

        #region filter traps
        if (!HideTrapsControls)
        {
            if (trapTypesControl.TrapType != previousTrapType.Value)
            {
                previousTrapType.Value = trapTypesControl.TrapType;
                WebUserSettingsDAL.Set("Web-OrionMessage-TrapType", trapTypesControl.TrapType);
            }
            if (trapCommunityControl.CommunityString != previousCommunity.Value)
            {
                previousCommunity.Value = trapCommunityControl.CommunityString;
                WebUserSettingsDAL.Set("Web-OrionMessage-Community", trapCommunityControl.CommunityString);
            }
        }
        #endregion

        #region audit messages
        if(ShowAuditControls)
        {
            if (auditTypesControl.AuditTypeID != previousAuditType.Value)
            {
                previousAuditType.Value = auditTypesControl.AuditTypeID;
                WebUserSettingsDAL.Set("Web-OrionMessage-AuditType", auditTypesControl.AuditTypeID);
            }
            if (auditUser.Text != previousAuditUser.Value)
            {
                previousAuditUser.Value = auditUser.Text;
                WebUserSettingsDAL.Set("Web-OrionMessage-AuditUser", auditUser.Text);
            }
        }
        #endregion

        #region solarwinds servers
        if (SwisFederationInfo.IsFederationEnabled)
        {
            if (SolarwindsServersDropDown.SelectedValue != previousSiteId.Value)
            {
                previousSiteId.Value = SolarwindsServersDropDown.SelectedValue;
                WebUserSettingsDAL.Set("Web-OrionMessage-SiteId", SolarwindsServersDropDown.SelectedValue);
            }
        }
        #endregion
    }

    private void LoadFromUserSettings()
    {
        string userSettingValue = WebUserSettingsDAL.Get("Web-OrionMessagesMaxMsgPerPage");
        if (String.IsNullOrEmpty(userSettingValue))
        {
            string defaultSettingValue = SettingsDAL.GetSetting("Web-OrionMessagesMaxMsgPerPage").SettingValue.ToString();
            maxRecords.Text = defaultSettingValue;
            WebUserSettingsDAL.Set("Web-OrionMessagesMaxMsgPerPage", defaultSettingValue);
        }
        else
        {
            maxRecords.Text = userSettingValue;
        }
        previousMaxRecords.Value = maxRecords.Text;

        string showAcknowledgedValue = WebUserSettingsDAL.Get("Web-ShowAcknowledgedOrionMessages");
        if (String.IsNullOrEmpty(showAcknowledgedValue))
        {
            cbShowCleared.Checked = false;
            WebUserSettingsDAL.Set("Web-ShowAcknowledgedOrionMessages", false.ToString());
        }
        else
        {
            cbShowCleared.Checked = bool.Parse(showAcknowledgedValue);
        }
        previousShowCleared.Value = cbShowCleared.Checked.ToString();

		if (!string.IsNullOrEmpty(Request["ShowOrionMessageTypes"]))
		{
			ShowMessageTypes(Request["ShowOrionMessageTypes"]);
		}
		else
		{
			string showMessageTypes = WebUserSettingsDAL.Get("Web-ShowOrionMessageTypes");

            if (String.IsNullOrEmpty(showMessageTypes))
			{
				showMessageTypes = "alert;event;";

				if (!HideSyslogControls)
					showMessageTypes += "syslog;";

				if (!HideTrapsControls)
					showMessageTypes += "trap;";

                if (ShowAuditControls)
                    showMessageTypes += "audit;";

				ShowMessageTypes(showMessageTypes);
				WebUserSettingsDAL.Set("Web-ShowOrionMessageTypes", showMessageTypes);
			}
			else
			{
				ShowMessageTypes(showMessageTypes);
			}
		}
        previousShowMessageTypes.Value = GetMessageTypesString();

        #region filter alerts
        string alertType = WebUserSettingsDAL.Get("Web-OrionMessage-AlertType");
        if (!string.IsNullOrEmpty(alertType))
            alertNamesControl.AlertID = previousAlertType.Value = alertType;
        #endregion

        #region filter events
        string eventType = WebUserSettingsDAL.Get("Web-OrionMessage-EventType");
        if (!string.IsNullOrEmpty(eventType))
            eventTypesControl.SelectedValue = previousEventType.Value = eventType;
        #endregion

        #region filter syslog
        if (!HideSyslogControls)
        {
            string severity = WebUserSettingsDAL.Get("Web-OrionMessage-SyslogSeverity");
            if (!string.IsNullOrEmpty(severity))
                syslogSeveritiesControl.SeverityCode = previousSyslogSeverity.Value = severity;

            string facility = WebUserSettingsDAL.Get("Web-OrionMessage-SyslogFacility");
            if (!string.IsNullOrEmpty(facility))
                syslogFacilitiesControl.FacilityCode = previousSyslogFacility.Value = facility;
        }
        #endregion

        #region filter traps
        if (!HideTrapsControls)
        {
            string trapType = WebUserSettingsDAL.Get("Web-OrionMessage-TrapType");
            if (!string.IsNullOrEmpty(trapType))
                trapTypesControl.TrapType = previousTrapType.Value = trapType;

            string community = WebUserSettingsDAL.Get("Web-OrionMessage-Community");
            if (!string.IsNullOrEmpty(community))
                trapCommunityControl.CommunityString = previousCommunity.Value = community;
        }
        #endregion

        #region filter audit messages
        if (ShowAuditControls)
        {
            string auditType = WebUserSettingsDAL.Get("Web-OrionMessage-AuditType");
            if (!string.IsNullOrEmpty(auditType))
                auditTypesControl.AuditTypeID = previousAuditType.Value = auditType;

			string auditUserVal = (string.IsNullOrEmpty(Request["AuditUser"])) ?
				WebUserSettingsDAL.Get("Web-OrionMessage-AuditUser") :
				Request["AuditUser"];

            if (!string.IsNullOrEmpty(auditType))
                auditUser.Text = previousAuditUser.Value = auditUserVal;
        }
        #endregion

        #region solarwinds servers

        if (SwisFederationInfo.IsFederationEnabled)
        {
            string siteId = WebUserSettingsDAL.Get("Web-OrionMessage-SiteId");
            if (!string.IsNullOrEmpty(siteId))
                SolarwindsServersDropDown.SelectedValue = siteId;
        }
        #endregion
    }

    private void PopulateChildControls()
    {
        SaveUserSettings();

        int? nodeId = null;
        if (!string.IsNullOrEmpty(netObjects.NetObjectID))
            nodeId = int.Parse(NetObjectHelper.GetObjectID(netObjects.NetObjectID));

        filterAlerts.Style["display"] = cbShowAlerts.Checked ? "block" : "none";
        filterEvents.Style["display"] = cbShowEvents.Checked ? "block" : "none";

        byte facilityCode = 0;
        byte severityCode = 0;
        if (!HideSyslogControls)
        {
            if (cbShowSyslogs.Checked)
            {
                filterSyslog.Style["display"] = "block";
                // 255 if none selected
                if (!byte.TryParse(syslogSeveritiesControl.SeverityCode, out severityCode))
                    severityCode = 255;
                // 255 if none selected
                if (!byte.TryParse(syslogFacilitiesControl.FacilityCode, out facilityCode))
                    facilityCode = 255;
            }
            else
                filterSyslog.Style["display"] = "none";
        }

        if (!HideTrapsControls)
        {
            if (cbShowTraps.Checked)
                filterTraps.Style["display"] = "block";
            else
                filterTraps.Style["display"] = "none";
        }

        if (ShowAuditControls)
        {
            filterAudits.Style["display"] = cbShowAudits.Checked ? "block" : "none";
        }

        int? siteId = null;
        if (SwisFederationInfo.IsFederationEnabled)
        {
            int id;
            if (int.TryParse(SolarwindsServersDropDown.SelectedValue, out id))
                siteId = id;
        }

        var period = Period;
        DateTime fromDate = new DateTime();
        DateTime toDate = new DateTime();

        Periods.Parse(ref period, ref fromDate, ref toDate);

        OrionMessagesFilter filter = new OrionMessagesFilter
                                         {
                                             NodeId = nodeId,
                                             DeviceType = netObjectTypes.DeviceType,
                                             Vendor = vendors.VendorName,
                                             IpAddress = ipAddress.Text,
                                             Hostname = hostName.Text,
                                             FromDate = fromDate.ToUniversalTime(),
                                             ToDate = toDate.ToUniversalTime(),
                                             Count = Convert.ToInt32(maxRecords.Text),
                                             ShowAcknowledged = cbShowCleared.Checked,
                                             IncludeAlerts = cbShowAlerts.Checked,
                                             IncludeAudits = ShowAuditControls && cbShowAudits.Checked,
                                             IncludeEvents = cbShowEvents.Checked,
                                             IncludeSyslogs = !HideSyslogControls && cbShowSyslogs.Checked,
                                             IncludeTraps = !HideTrapsControls && cbShowTraps.Checked,
                                             AlertType = alertNamesControl.AlertID,
                                             AuditType = auditTypesControl.AuditTypeID,
                                             EventType = eventTypesControl.SelectedValue,
                                             SyslogSeverity = severityCode,
                                             SyslogFacility = facilityCode,
                                             TrapType = trapTypesControl.TrapType,
                                             TrapCommunity = trapCommunityControl.CommunityString,
                                             Audituser = auditUser.Text,
                                             SearchString = SearchString,
                                             SiteID = siteId
                                         };

        OrionMessagesControl.GenerateReport(filter);

        ExportToPDFLink.PageUrl = GetUrlWithParams(Request.Url.OriginalString.Split('?')[0]);

        trapCommunityControl.Visible = Profile.AllowAdmin;  // only admins can see community strings

        btnRefresh.Focus();
    }

    protected void Show_Click(object sender, EventArgs e)
    {
        if (!details.Visible)
        {
            details.Visible = true;
            btnHide.Visible = true;
            btnShow.Visible = false;
        }
        else
        {
            details.Visible = false;
            btnHide.Visible = false;
            btnShow.Visible = true;
        }
    }

    protected void Search_Click(object sender, EventArgs e)
    {
        SetSearchString();
        PopulateChildControls();
    }

	private string GetUrlWithParams(string pageUrl)
	{
		var reqParams = new Dictionary<string, string>() { 
			{"NetObject", Server.UrlEncode(netObjects.NetObjectID)},
			{"DeviceType", Server.UrlEncode(netObjectTypes.DeviceType)},
			{"Vendor", Server.UrlEncode(vendors.VendorName)},
			{"IPAddress", Server.UrlEncode(ipAddress.Text)},
			{"Hostname", Server.UrlEncode(hostName.Text)},
			{"MaxRecords", Server.UrlEncode(maxRecords.Text)},
			{"ShowAcknowledged", cbShowCleared.Checked.ToString()},
			{"ShowEvents", cbShowEvents.Checked.ToString()},
			{"ShowAlerts", cbShowAlerts.Checked.ToString()},
            {"ShowAudits", (ShowAuditControls && cbShowAudits.Checked).ToString()},
			{"ShowSyslogs", (!HideSyslogControls && cbShowSyslogs.Checked).ToString()},
			{"ShowTraps", (!HideTrapsControls && cbShowTraps.Checked).ToString()},
			{"Period", Server.UrlEncode(Period)},
			{"AlertType", Server.UrlEncode(alertNamesControl.AlertID)},
            {"AuditType", Server.UrlEncode(auditTypesControl.AuditTypeID)},
            {"AuditUser", Server.UrlEncode(auditUser.Text)},
			{"EventType", Server.UrlEncode(eventTypesControl.SelectedValue)}, 
			{"Severity", Server.UrlEncode(syslogSeveritiesControl.SeverityCode)},
			{"Facility", Server.UrlEncode(syslogFacilitiesControl.FacilityCode)},
			{"TrapType", Server.UrlEncode(trapTypesControl.TrapType)},
			{"Community", Server.UrlEncode(trapCommunityControl.CommunityString)},
			{"SearchString", Server.UrlEncode(SearchString)},
			{"SortDir", (OrionMessagesControl.SortDir== SortDirection.Ascending).ToString()},
			{"SortString", Server.UrlEncode(OrionMessagesControl.SortString)},
			{"HideDetails", (!details.Visible).ToString()},
            {"SiteID", Server.UrlEncode(SolarwindsServersDropDown.SelectedValue)}
		};

		var pageUrlWithParams = new StringBuilder(string.Format("{0}", pageUrl));
		var separator = "?";

		foreach (var item in reqParams)
		{
			pageUrlWithParams.AppendFormat("{0}{1}={2}", separator, item.Key, item.Value);
			separator = "&";
		}

		return pageUrlWithParams.ToString();
	}

    protected void Printable_Click(object sender, EventArgs e)
    {
		Response.Redirect(GetUrlWithParams("PrintableOrionMessages.aspx"));
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        maxRecords.ValidatorText = validatorText;
    }

    protected void Refresh_Click(object sender, EventArgs e)
    {
        SetSearchString();
    }

    private void LoadUserControls()
    {
        syslogSeveritiesControl = (ISeverityControl)Page.LoadControl("~/Orion/Controls/SeverityControl.ascx");
        syslogSeveritiesControl.ID = "syslogSeveritiesControl";
        SeveritiesPlaceHolder.Controls.Add((Control)syslogSeveritiesControl);

        syslogFacilitiesControl = (IFacilityControl)Page.LoadControl("~/Orion/Controls/FacilityControl.ascx");
        syslogFacilitiesControl.ID = "syslogFacilitiesControl";
        FacilitiesPlaceHolder.Controls.Add((Control)syslogFacilitiesControl);

        trapTypesControl = (ITrapTypeControl)Page.LoadControl("~/Orion/Controls/TrapTypeControl.ascx");
        trapTypesControl.ID = "trapTypesControl";
        TrapTypesPlaceHolder.Controls.Add((Control)trapTypesControl);

        trapCommunityControl = (ICommunityStringControl)Page.LoadControl("~/Orion/Controls/CommunityStringControl.ascx");
        trapCommunityControl.ID = "trapCommunityControl";
        CommunityStringsPlaceHolder.Controls.Add((Control)trapCommunityControl);
    }
}
