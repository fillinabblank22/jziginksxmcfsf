using System;
using System.Drawing;
using System.Drawing.Imaging;

public partial class Orion_NetPerfMon_TitleImage : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		string fontSize = Context.Request["FontSize"].ToString().Trim();
		string text = Context.Request["TitleText"].ToString().Trim();
		int height = Convert.ToInt32(Context.Request["TitleHeight"].ToString());
		string orientation = Context.Request["Orientation"].ToString().Trim();
		Color textColor = ColorTranslator.FromWin32(Convert.ToInt32(Context.Request["Color"]));

		System.Drawing.Image bmp = new Bitmap(20, height);
		Graphics graph = Graphics.FromImage(bmp);

		Font font = new Font("Arial, Helvetica, sans-serif", float.Parse(fontSize));

		int textWidth = (int)graph.MeasureString(text, font).Width;

		graph.FillRectangle(new SolidBrush(Color.White), 0, 0, 20, height);
		if (orientation.Equals("LEFT", StringComparison.OrdinalIgnoreCase))
		{
			graph.TranslateTransform(0f, (height - textWidth) / 2 + textWidth);
			graph.RotateTransform(-90.0f);
		}
		else if (orientation.Equals("RIGHT", StringComparison.OrdinalIgnoreCase))
		{
			graph.TranslateTransform(20f, (height - textWidth) / 2);
			graph.RotateTransform(90.0f);
		}
		
		graph.DrawString(text, font, new SolidBrush(textColor), 0, 0);

		Context.Response.Clear();
		Context.Response.ContentType = @"Image/jpeg";
		bmp.Save(Context.Response.OutputStream, ImageFormat.Jpeg);
		Context.Response.End();
	}
}
