<%@ Page Language="C#" MasterPageFile="~/Orion/View.master" AutoEventWireup="true" CodeFile="ContainerDetails.aspx.cs" Inherits="Orion_NetPerfMon_ContainerDetails" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="orion" Assembly="OrionWeb" Namespace="SolarWinds.Orion.Web.Containers" %>


<asp:Content ID="SummaryTitle" ContentPlaceHolderID="ViewPageTitle" Runat="Server">
    <%if (!CommonWebHelper.IsBreadcrumbsDisabled) { %>
	            <div style="margin-left: 10px; margin-top: 10px;">
                    <orion:DropDownMapPath ID="GroupSiteMapPath" runat="server"  OnInit="GroupSiteMapPath_OnInit">
                    <RootNodeTemplate>
                        <a href="<%# DefaultSanitizer.SanitizeHtml(Eval("url")) %>" ><u> <%# DefaultSanitizer.SanitizeHtml(Eval("title")) %></u> </a>
                    </RootNodeTemplate>
                    <CurrentNodeTemplate>
				        <a href="<%# DefaultSanitizer.SanitizeHtml(Eval("url")) %>" ><%# DefaultSanitizer.SanitizeHtml(Eval("title")) %> </a>
				    </CurrentNodeTemplate>
                    </orion:DropDownMapPath>
				</div>
				<%} %>
	<h1>
	 <%= DefaultSanitizer.SanitizeHtml(ViewInfo.IsSubView ? ViewInfo.ViewGroupTitle : ViewInfo.ViewHtmlTitle) %> -    
	 <a href='<%= DefaultSanitizer.SanitizeHtml(BaseResourceControl.GetViewLink(Container)) %>'><%= DefaultSanitizer.SanitizeHtml(UIHelper.Escape(Title)) %></a>
     <%= DefaultSanitizer.SanitizeHtml(ViewInfo.IsSubView ? ViewInfo.ViewHtmlTitle : string.Empty) %>	
	</h1>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
	<orion:ContainerResourceHost ID="ResourceHostControl2" runat="server">
		<orion:ResourceContainer runat="server" ID="resContainer" />
	</orion:ContainerResourceHost>
</asp:Content>

