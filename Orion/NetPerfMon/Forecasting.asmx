<%@ WebService Language="C#" Class="NodeChartData" %>

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.DAL;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class NodeChartData : ChartDataWebService
{
    private static SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();
    private const long DayMiliseconds = 86400000;

    [WebMethod]
    public ChartDataResults Forecasting(ChartDataRequest request)
    {
        var tags = new[] { "Peak", "Avg", "Common" };

        string netObjectId = request.NetObjectIds[0];

        var resourceProperties = request.AllSettings["ResourceProperties"] as Dictionary<string, object>;

        string metricName = resourceProperties["metricname"].ToString();

        var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);
        var originalDateRange = new DateRange() { StartDate = dateRange.StartDate, EndDate = dateRange.EndDate };

        dateRange = SetCustomDateRangeBasedOnRequest(request, dateRange, DateTimeKind.Local);
        var dynamicLoader = new DynamicLoader(request, new ChartWidthSampleSizeCalculator());

        DateRange bucketDateRange = dynamicLoader.GetSamplingDateRange(dateRange, originalDateRange);
        int dynamicSampleSize = dynamicLoader.CalculateDynamicSampleSize(dateRange);

        List<DataSeries> chartDataSeriesList = new List<DataSeries>();
        IForecastingCapacityDAL forecastingDAL = new ForecastingCapacityDAL();

        if (forecastingDAL.AreDataAvailable(metricName, Int32.Parse(netObjectId)))
        {
            IEnumerable<IDictionary<DateTime, double>> data = forecastingDAL.GetDataPointListByForecastMetric(dateRange.StartDate, dateRange.EndDate, metricName, Int32.Parse(netObjectId));
            string label;

            if (resourceProperties.ContainsKey("labelnameswql"))
            {
                string labelNameSwql = resourceProperties["labelnameswql"].ToString();
                label = forecastingDAL.GetLabelName(labelNameSwql, netObjectId);
            }
            else
                label = request.AllSettings["title"].ToString();

            DataSeries peakSeries = GetTrendDataSeries(data.ElementAt(0), tags[0], netObjectId, label, bucketDateRange, dynamicSampleSize);
            DataSeries avgSeries = GetTrendDataSeries(data.ElementAt(1), tags[1], netObjectId, label, bucketDateRange, dynamicSampleSize);
            DataSeries commonDataSeries = GetRawForecastingChartSeries(data.ElementAt(2), tags[2], netObjectId, label)
                .CreateSampledSeries(bucketDateRange, dynamicSampleSize, SampleMethod.Average);

            if (!commonDataSeries.Data.Any())
                commonDataSeries = new DataSeries();

            chartDataSeriesList.Add(commonDataSeries);
            chartDataSeriesList.Add(peakSeries);
            chartDataSeriesList.Add(avgSeries);

            if (peakSeries.Data.Count() == 0 && avgSeries.Data.Count() == 0)
            {
                chartDataSeriesList = new List<DataSeries>();
            }
            else
            {
                foreach (var series in chartDataSeriesList)
                {
                    series.ShowPercentileLine = request.Calculate95thPercentile;
                    series.ShowTrendLine = request.CalculateTrendLine;
                }
                var threshHoldData = forecastingDAL.GetThreshHoldData(metricName, netObjectId);
                LoadThresholdData(threshHoldData, avgSeries, peakSeries);
            }
        }

        var allSeries = ApplyLimitOnNumberOfSeries(request, chartDataSeriesList);
        var result = new ChartDataResults(allSeries);
        result.AppliedLimitation = request.AppliedLimitation;

        var res = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);
        return res;
    }

    public DataSeries GetTrendDataSeries(IDictionary<DateTime, double> data, string tag, string netObjectId, string labelName, DateRange bucketDateRange, int dynamicSampleSize)
    {
        if (!data.Any())
            return new DataSeries();

        var rawSeries = GetRawForecastingChartSeries(data, tag, netObjectId, labelName);

        var timeStamp = (long)rawSeries.Data[2][0];

        float xA, xB;
        ComputeVectors(rawSeries.Data, out xA, out xB);

        var sampledSeries = rawSeries.CreateSampledSeries(bucketDateRange, dynamicSampleSize, SampleMethod.Average);

        if (sampledSeries.HasData)
        {
            for (int i = 0; i < sampledSeries.Data.Count; i++)
            {
                sampledSeries.Data[i] = RecomputeDataForDay(sampledSeries.Data[i], xA, xB, timeStamp);
            }
        }

        return sampledSeries;
    }

    public DataSeries GetRawForecastingChartSeries(IDictionary<DateTime, double> data, string tag, string netObjectId, string labelName)
    {
        //Getting data, assign tags, convert to data points
        var dataSerie = new DataSeries();
        dataSerie.TagName = tag;
        dataSerie.NetObjectId = netObjectId;

        foreach (var keyValue in data)
        {
            dataSerie.AddPoint(DataPoint.CreatePoint(keyValue.Key, keyValue.Value));
        }

        dataSerie.Label = labelName;

        return dataSerie;
    }


    private void ComputeVectors(List<DataPoint> data, out float a, out float b)
    {
        long startd = (long)data[1][0];        // first day of real data
        long endd = (long)data[0][0];          // last day of real data
        int totalDays = (int)((startd - endd) / DayMiliseconds);

        float deltaY = (float)data[1][1] - (float)data[0][1];
        b = deltaY / totalDays;
        a = (float)data[2][1];
    }

    private DataPoint RecomputeDataForDay(DataPoint p, float a, float b, long timeStamp)
    {
        double x = p.UnixTime;
        x -= timeStamp;
        x /= DayMiliseconds;
        double y = b * x + a;
        return DataPoint.CreatePoint(p.UnixTime, y);
    }

    private void LoadThresholdData(DataRow row, DataSeries avg, DataSeries peak)
    {
        avg.CustomProperties = new
        {
            Name = avg.TagName,
            Warn = row[3],
            Critical = row[4],
            Capacity = row[5],
            DayPercent = (double)(Math.Truncate(100 * (double)row[1]) / 100),
            WarningThreshold = row["WarningThreshold"],
            CriticalThreshold = row["CriticalThreshold"],
            CapacityThreshold = row["CapacityThreshold"]

        };
        peak.CustomProperties = new
        {
            Name = peak.TagName,
            Warn = row[6],
            Critical = row[7],
            Capacity = row[8],
            DayPercent = (double)(Math.Truncate(100 * (double)row[2]) / 100),
            WarningThreshold = row["WarningThreshold"],
            CriticalThreshold = row["CriticalThreshold"],
            CapacityThreshold = row["CapacityThreshold"]
        };
    }
}

