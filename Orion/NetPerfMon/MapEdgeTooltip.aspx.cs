﻿using SolarWinds.Orion.Web.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_NetPerfMon_MapEdgeTooltip : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MapEdgeTooltipParams tooltipParams = new MapEdgeTooltipParams(Request);
        MapEdgeTooltipControlDefinition tooltipControlDefinition = null;
        if (MapEdgeTooltipControlRepository.Instance.TryGetControl(tooltipParams.StartPointEntityName, tooltipParams.EndPointEntityName, out tooltipControlDefinition))
        {
            var tooltipControl = LoadControl(tooltipControlDefinition.ControlPath);
            tooltipContainer.Controls.Add(tooltipControl.TemplateControl);
        }
        else
        {
            var tooltipControl = LoadControl("~/Orion/NetPerfMon/Controls/MapEdgeNoTooltipControlDefined.ascx");
            tooltipContainer.Controls.Add(tooltipControl.TemplateControl);
        }
    }
}