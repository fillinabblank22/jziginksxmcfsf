<%@ WebService Language="C#" Class="NodeChartData" %>

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.DAL;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class NodeChartData : ChartDataWebService 
{
    private static SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();

    private const string _nodeNameLookupSwql = "SELECT NodeID, Caption FROM Orion.Nodes WHERE NodeID in @NodeID";
    
    /// <summary>
    /// Method use Chart.js for calculating TrendLine only from visible data
    /// </summary>
    /// <param name="request">Coordinates datapoints from which trend line will be computed first point contains StartDate value and last contains EndDate.</param>
    /// <param name="sampleSizeInMinutes">Sample size in minutes.</param>
    /// <returns>Coordinates data points which create trend line in form [x][y].</returns>
    [WebMethod]
    public double[][] CalculateTrendLine(double[][] request, int sampleSizeInMinutes)
    {
        double[][] result = null;
       // ChartDataResults result = null;
        var currentSeries = new DataSeries();
        foreach (var point in request.Skip(1).Take(request.Length - 2)) // skip the first and last.
        {
            currentSeries.AddPoint(DataPoint.CreatePoint(point[0], point[1]));
        }
        
        DateRange dateRange = new DateRange();
        DataPoint tmpPoint = DataPoint.CreatePoint(request[0][0], (double)0.0);
        dateRange.StartDate = tmpPoint.AsDatePoint().Date;
        tmpPoint = DataPoint.CreatePoint(request[request.Length-1][0], (double)0.0);
        dateRange.EndDate =  tmpPoint.AsDatePoint().Date;
        result = new double[2][];
        
        if (dateRange.StartDate == dateRange.EndDate)
        {
            if (currentSeries.Data.Count == 1)
            {
                result[0] = new double[2];
                result[1] = new double[2];
                var firstPoint = currentSeries.Data.FirstOrDefault(x => !x.IsNullPoint);
                if (firstPoint != null)
                {
                    result[0][0] = firstPoint.UnixTime;
                    result[0][1] = firstPoint.Value;
                }

                var lastPoint = currentSeries.Data.LastOrDefault(x => !x.IsNullPoint);
                if (lastPoint != null)
                {
                    result[1][0] = lastPoint.UnixTime;
                    result[1][1] = lastPoint.Value;
                }

                return result;
            }
        }
        
        var trendSeries = currentSeries.CreateTrendSeries(dateRange, sampleSizeInMinutes);

        /* take only first and last .. the rest we're able to interpolate. */
        /* why? there were some problems with alignment.. the Trend for some data is moved second to left/right, and it doesn't look good */
        {
            result[0] = new double[2];
            result[1] = new double[2];
           // var dataPoint = trendSeries.Data[i].AsDatePoint();
            var firstPoint = trendSeries.Data.FirstOrDefault(x => !x.IsNullPoint);
            if (firstPoint != null)
            {
                result[0][0] = firstPoint.UnixTime;
                result[0][1] = firstPoint.Value;
            }

            var lastPoint = trendSeries.Data.LastOrDefault(x => !x.IsNullPoint);
            if (lastPoint != null)
            {
                result[1][0] = lastPoint.UnixTime;
                result[1][1] = lastPoint.Value;
            }
        }

        return result;
    }

    [WebMethod]
    public ChartDataResults MinMaxAvgRT(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request, 2);
        const string sql = @"SELECT DateTime, AvgResponseTime, MinResponseTime, MaxResponseTime
                             FROM ResponseTime 
                             WHERE NodeId = @NetObjectId
                             AND DateTime >= @StartTime AND DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);
        var result = SimpleLoadMinMaxAvgData(request, dateRange, sql, _nodeNameLookupSwql, DateTimeKind.Local, "ResponseTime", "MinMaxResponseTime");
      
       
        
        return result;
    }
    
    [WebMethod]
    public ChartDataResults MMAvgRTLoss(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request, 2);
        const string sql = @"SELECT DateTime, AvgResponseTime, MinResponseTime, MaxResponseTime, PercentLoss
                             FROM ResponseTime 
                             WHERE NodeId = @NetObjectId
                             AND DateTime >= @StartTime AND DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);
        var result = SimpleLoadMinMaxAvgData(request, dateRange, sql, _nodeNameLookupSwql, DateTimeKind.Local, "ResponseTime", "MinMaxResponseTime", "PercentLoss");
      
       
        
        return result;
    }
    
    
    
    [WebMethod]
    public ChartDataResults AvgRT(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        const string sql = @"SELECT DateTime, AvgResponseTime
                             FROM ResponseTime 
                             WHERE NodeId = @NetObjectId
                             AND DateTime >= @StartTime AND DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, sql, _nodeNameLookupSwql, DateTimeKind.Local, "ResponseTime");
        return result;
    }
    
    [WebMethod]
    public ChartDataResults AvailabilityRT(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request, 2);
        const string sql = @"SELECT DateTime, AvgResponseTime, Availability
                             FROM ResponseTime 
                             WHERE NodeId = @NetObjectId
                             AND DateTime >= @StartTime AND DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, sql, _nodeNameLookupSwql, DateTimeKind.Local, "ResponseTime", "Availability");
        return result;
    }
    
    
    [WebMethod]
    public ChartDataResults AvgRTLoss(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request, 2);
        const string sql = @"SELECT DateTime, AvgResponseTime, PercentLoss
                             FROM ResponseTime 
                             WHERE NodeId = @NetObjectId
                             AND DateTime >= @StartTime AND DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, sql, _nodeNameLookupSwql, DateTimeKind.Local, "ResponseTime", "PacketLoss");
        return result;
    }

    [WebMethod]
    public ChartDataResults PacketLoss(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        const string sql = @"SELECT DateTime, PercentLoss
                             FROM ResponseTime 
                             WHERE NodeId = @NetObjectId
                             AND DateTime >= @StartTime AND DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, sql, _nodeNameLookupSwql, DateTimeKind.Local, "PacketLoss");
        return result;
    }

    [WebMethod]
    public ChartDataResults Availability(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        const string sql = @"SELECT DateTime, Availability
                             FROM ResponseTime 
                             WHERE NodeId = @NetObjectId
                             AND DateTime >= @StartTime AND DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, sql, _nodeNameLookupSwql, DateTimeKind.Local, "Availability");
        return result;
    }

    [WebMethod]
    public ChartDataResults HostAvgCPULoad(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        const string sql = @"SELECT DateTime, AvgLoad
                             FROM CPULoad 
                             WHERE NodeId = @NetObjectId
                             AND DateTime >= @StartTime AND DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, sql, _nodeNameLookupSwql, DateTimeKind.Local, "AvgCPULoad");
        return result;
    }

    [WebMethod]
    public ChartDataResults HostAvgMemoryUsage(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        const string sql = @"SELECT DateTime, AvgMemoryUsed
                             FROM CPULoad 
                             WHERE NodeId = @NetObjectId
                             AND DateTime >= @StartTime AND DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, sql, _nodeNameLookupSwql, DateTimeKind.Local, "AvgMemoryUsed");
        return result;
    }
    
    [WebMethod]
    public ChartDataResults HostAvgPercentMemoryUsed(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        const string sql = @"SELECT DateTime, AvgPercentMemoryUsed
                             FROM CPULoad 
                             WHERE NodeId = @NetObjectId
                             AND DateTime >= @StartTime AND DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, sql, _nodeNameLookupSwql, DateTimeKind.Local, "AvgPercentMemoryUsed");
        return result;
    }
    
    [WebMethod]
    public ChartDataResults HostMMAvgMemoryUsage(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request, 2);
        const string sql = @"SELECT DateTime, AvgMemoryUsed, MinMemoryUsed, MaxMemoryUsed
                             FROM CPULoad 
                             WHERE NodeId = @NetObjectId
                             AND DateTime >= @StartTime AND DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);
        var result = SimpleLoadMinMaxAvgData(request, dateRange, sql, _nodeNameLookupSwql, DateTimeKind.Local, "MemoryUsed", "MinMaxMemoryUsed");

        if (!DynamicLoader.IsDynamicLoadingEnabled(request))
        {
            double minRangeVal = CalculateMinRangeValueForDataSeries(result.DataSeries);

            dynamic options = new JsonObject();
            options.yAxis = JsonObject.CreateJsonArray(1);
            options.yAxis[0].minRange = minRangeVal;
            result.ChartOptionsOverride = options;
        }
        
        return result;
    }
    
    [WebMethod]
    public ChartDataResults ProgressiveLoadTest(ChartDataRequest request)
    {
        const int numberOfPartialLoadRequests = 5;
        
        var fullDateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);
        var totalMinutes = Math.Ceiling((fullDateRange.EndDate - fullDateRange.StartDate).TotalMinutes);
        int numberOfBuckets = (int)Math.Ceiling(totalMinutes / request.SampleSizeInMinutes);
        int bucketsPerRequest = numberOfBuckets / numberOfPartialLoadRequests;

        DateRange partialRange;
        bool moreDataAvailable = true;
        bool isInitialLoad = false;

        if (String.IsNullOrEmpty(request.PartialDataCookie))
            request.PartialDataCookie = "X";
        
        if ((request.PartialDataCookie.FirstOrDefault() == 'X') && (request.PartialDataCookie.Length < 4) )
        {
            _log.DebugFormat("Partial Data Load simulating long initial query. Returning empty data set.");
            return new ChartDataResults(new DataSeries[0])
            {
                MoreDataAvailable = true,
                DelayForMoreDataInMs = 3000,
                PartialDataCookie = request.PartialDataCookie + "X"
            };
        }
        
        if (request.PartialDataCookie.FirstOrDefault() == 'X')
        {
            // Load the initial data.
            _log.DebugFormat("Starting Progrssive Load.  Full Range Start: {0}   End: {1}", fullDateRange.StartDate, fullDateRange.EndDate);
            isInitialLoad = true;
            partialRange = new DateRange
            {
                EndDate = fullDateRange.EndDate,
                StartDate = fullDateRange.EndDate.AddMinutes(-bucketsPerRequest*request.SampleSizeInMinutes)
            };
        }
        else
        {
            // Load the next partial results;
            var endDate = new DateTime(long.Parse(request.PartialDataCookie), DateTimeKind.Local);
            partialRange = new DateRange
            {
                EndDate = endDate,
                StartDate = endDate.AddMinutes(-bucketsPerRequest * request.SampleSizeInMinutes)
            };
        }

        if (partialRange.StartDate <= fullDateRange.StartDate)
        {
            partialRange.StartDate = fullDateRange.StartDate;
            moreDataAvailable = false;
        }

        _log.DebugFormat("    Loading Partial Range Start: {0}    End: {1}", partialRange.StartDate, partialRange.EndDate);

        const string sql = @"SELECT DateTime, AvgResponseTime, PercentLoss
                             FROM ResponseTime 
                             WHERE NodeId = @NetObjectId
                             AND DateTime >= @StartTime AND DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var result = SimpleLoadData(request, partialRange, sql, _nodeNameLookupSwql, DateTimeKind.Local, "ResponseTime", "PacketLoss");
        
        if (isInitialLoad) 
        {
            // For the initial load, add a Null Point at the start date so that the full range is shown on the chart.
            foreach (var series in result.DataSeries)
            {
                series.Data.Insert(0, DataPoint.CreateNullPoint(fullDateRange.StartDate));
            }
            
            // As an example, let's override some of the chart options set in the database table
            var resourceProperties = request.AllSettings["ResourceProperties"] as Dictionary<string, object>;
            string lineColor = null;
            
            if (resourceProperties != null)
            {
                object value;
                if (resourceProperties.TryGetValue("linecolor", out value))
                    lineColor = value.ToString();
            }

            dynamic options = new JsonObject();
            options.yAxis = JsonObject.CreateJsonArray(1);
            if (!String.IsNullOrEmpty(lineColor))
                options.seriesTemplates.ResponseTime.color = lineColor;

            result.ChartOptionsOverride = options;
            
            // As an example, the first re-request should wait 10 seconds.
            result.DelayForMoreDataInMs = (int) TimeSpan.FromSeconds(10).TotalMilliseconds;
        }

        result.MoreDataAvailable = moreDataAvailable;
        result.PartialDataCookie = partialRange.StartDate.Ticks.ToString(CultureInfo.InvariantCulture);
        
        // Simulate a slow query
        Thread.Sleep(TimeSpan.FromSeconds(5));
        return result;
    }

    

    [WebMethod]
    public ChartDataResults CiscoMMAvgCPULoad(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request, 2);
        // make sure we ignore -1 or -2 values since these have special meaning.
        const string sql = @"SELECT DateTime,  
                                    NULLIF(NULLIF(AvgLoad, -1), -2) AvgLoad, 
                                    NULLIF(NULLIF(MinLoad, -1), -2) MinLoad, 
                                    NULLIF(NULLIF(MaxLoad, -1), -2) MaxLoad
                             FROM CPULoad 
                             WHERE Nodeid = @NetObjectId
                             AND DateTime >= @StartTime AND DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgo(request.DaysOfDataToLoad);
        var result = SimpleLoadMinMaxAvgData(request, dateRange, sql, _nodeNameLookupSwql, DateTimeKind.Local, "AvgCpu", "MinMaxCpu");
        return result;
    }    
    

    
    [WebMethod]
    public ChartDataResults AvgCPUMultiLoad(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        var dynamicLoader = new DynamicLoader(request, new ChartWidthSampleSizeCalculator());
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        const DateTimeKind dateFormatInDatabase = DateTimeKind.Utc;
        var originalDateRange = new DateRange() { StartDate = dateRange.StartDate, EndDate = dateRange.EndDate };
        dateRange = SetCustomDateRangeBasedOnRequest(request, dateRange, dateFormatInDatabase);
        var bucketDateRange = dynamicLoader.GetSamplingDateRange(dateRange, originalDateRange);

        const string sql = @"SELECT NodeId, CPUIndex, TimeStampUTC, AvgLoad            
                             FROM CPUMultiLoad 
                             WHERE NodeId IN ({0})
                               AND TimeStampUTC >= @StartTime AND TimeStampUTC <= @EndTime
                             ORDER BY NodeID, CPUIndex, TimeStampUTC";

        string[] paramNames = request.NetObjectIds.Select((s, i) => "@p" + i.ToString()).ToArray();
        string inClause = string.Join(",", paramNames);
        var sqlWithNodes = String.Format(sql, inClause);
        var limitedSql = Limitation.LimitSQL(sqlWithNodes);
       
        var parameters = new List<SqlParameter>
        {
            new SqlParameter("StartTime", dateRange.StartDate),
            new SqlParameter("EndTime", dateRange.EndDate)
        };

        for (int i = 0; i < paramNames.Length; i++)
        {
            parameters.Add(new SqlParameter(paramNames[i], request.NetObjectIds[i]));
        }

        var rawSeries = GetSeries(limitedSql, parameters, dateFormatInDatabase);
        
        // Update the labels
        var displayNames = LoadDisplayNames(request.NetObjectIds);

        var seriesToReturn = new List<DataSeries>();

        var sampleSize = dynamicLoader.CalculateDynamicSampleSize(dateRange);
        
        foreach (var raw in rawSeries)
        {
            var sampled = raw.CreateSampledSeries(bucketDateRange, sampleSize, SampleMethod.Average);
            
            var netObjectName = displayNames[raw.NetObjectId];
            sampled.Label = String.Format("{0} - CPU # {1}", netObjectName, raw.TagName);
            sampled.TagName = "CpuLoad";
            
            seriesToReturn.Add(sampled);
        }

        if (request.CalculateSum)
            seriesToReturn.Add(DataSeries.CreateSumSeries(seriesToReturn));
        var result =
            dynamicLoader.SetDynamicChartOptions(
                new ChartDataResults(ApplyLimitOnNumberOfSeries(request, seriesToReturn)));
        result.AppliedLimitation = request.AppliedLimitation;
        return result;
    }

    private List<DataSeries> GetSeries(string sql, IEnumerable<SqlParameter> parameters, DateTimeKind dateFormatInDatabase)
    {
        var results  = new List<DataSeries>();
        
        using (var cmd = SqlHelper.GetTextCommand(sql))
        {
            cmd.Parameters.AddRange(parameters.ToArray());

            using (var reader = SqlHelper.ExecuteReader(cmd))
            {
                int lastNodeId = Int32.MaxValue;
                int lastIndex = Int32.MaxValue;
                DataSeries currentSeries = null;

                while (reader.Read())
                {
                    int currentNodeId = DatabaseFunctions.GetInt32(reader, 0);
                    int currentIndex = DatabaseFunctions.GetInt16(reader, 1);
                    
                    if ((currentNodeId != lastNodeId) || (currentIndex != lastIndex) || (currentSeries == null))
                    {
                        lastNodeId = currentNodeId;
                        lastIndex = currentIndex;
                        currentSeries = new DataSeries();
                        currentSeries.TagName = (currentIndex + 1).ToString(CultureInfo.InvariantCulture);
                        currentSeries.NetObjectId = currentNodeId.ToString(CultureInfo.InvariantCulture);
                        results.Add(currentSeries);
                    }

                    var date = DatabaseFunctions.GetDateTime(reader, 2, dateFormatInDatabase);

                    var val = reader[3];
                    if ((val != null) && (!Convert.IsDBNull(val)))
                    {
                        var point = DataPoint.CreatePoint(date, Convert.ToDouble(val));
                        currentSeries.AddPoint(point);
                    }
                }
            }
        }

        return results;
    }

    private Dictionary<string, string> LoadDisplayNames(string[] netObjectIds)
    {
        var result = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        
        const string sqlFormat = "SELECT NodeID, Caption FROM Nodes WHERE NodeID IN ({0})";
        var nodeIdsCommaDelimited = String.Join(",", netObjectIds);
        
        using (var cmd = SqlHelper.GetTextCommand(String.Format(sqlFormat, nodeIdsCommaDelimited)))
        using (var reader = SqlHelper.ExecuteReader(cmd))
        {
            while (reader.Read())
            {
                int nodeId = DatabaseFunctions.GetInt32(reader, 0);
                string caption = DatabaseFunctions.GetString(reader, 1);

                result[nodeId.ToString(CultureInfo.InvariantCulture)] = caption;
            }
        }

        return result;
    }


    
    

    
    
    
    
}

