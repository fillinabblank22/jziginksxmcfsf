<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChartData.aspx.cs" Inherits="ChartData" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta http-equiv="Content-Type"; content="text/html; charset=UTF-8" />
</head>
<body>
    <form id="form1" runat="server">
    <div>

<%	
	DoReport();

    switch (_dataFormat.ToUpper()) {
        case "CSV":
        case "CDF":
            Response.AddHeader("Content-disposition", "attachment; filename=ChartData.csv");
            break;
        case "TEXT":
        case "RAW":
            Response.AddHeader("Content-disposition", "attachment; filename=ChartData.txt");
            break;
        case "EXCEL":
        case "EXCELL":
        case "XML":
        case "XLS":
            Response.AddHeader("Content-disposition", "attachment; filename=ChartData.xls");
            break;
            }
%>
    </div>
    </form>
</body>
</html>
