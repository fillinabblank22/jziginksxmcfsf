﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;
using System.Data;
using SolarWinds.Orion.Web.InformationService;

public partial class Orion_NetPerfMon_ActiveAlertDetails : OrionView
{
    private string GetCaptionForGlobalAlert(string entityType, int alertObjectId)
    {
        string captionDisplayFormat = string.Empty;
        string swqlQuery = @"SELECT DisplayNamePlural FROM Metadata.Entity WHERE FullName = @entityName";
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            DataTable resultTable = proxy.Query(swqlQuery, new Dictionary<string, object> { { "entityName", entityType } });
            if (resultTable.Rows.Count > 0)
            {
                string displayNamePlural = resultTable.Rows[0][0] as string;
                // caption for global alert is e.g. 5 Nodes
                swqlQuery = @"SELECT COUNT(a.AlertActiveObjects.EntityUri) AS Number
                            FROM Orion.AlertActive (nolock=true) AS a
                            WHERE a.AlertObjectID=@alertObjectId";
                resultTable = proxy.Query(swqlQuery, new Dictionary<string, object> { { "alertObjectId", alertObjectId } });
                if (resultTable.Rows.Count > 0) {
                    int entityCount = resultTable.Rows[0]["Number"] != DBNull.Value ? Convert.ToInt32(resultTable.Rows[0]["Number"]) : 0;
                    captionDisplayFormat = string.Format("{0} {1}", entityCount, displayNamePlural); 
                }
            }
        }

        return captionDisplayFormat;
    }

    protected override void OnInit(EventArgs e)
    {
        this.Title = this.ViewInfo.ViewTitle;
        var activeAlert = (ActiveAlert)this.NetObject;
        this.activeAlertResHost.ActiveAlert = activeAlert;

        string triggeringObjectEntityUri = activeAlert.ObjectProperties["TriggeringObjectEntityUri"] != null ? Convert.ToString(activeAlert.ObjectProperties["TriggeringObjectEntityUri"]) : string.Empty;
        if (!string.IsNullOrEmpty(triggeringObjectEntityUri))
        {
            this.statusIconControl.Visible = true;
            this.statusIconControl.EntityId = activeAlert.ObjectProperties["ActiveNetObject"] != null ? Convert.ToString(activeAlert.ObjectProperties["ActiveNetObject"]) : string.Empty;
            this.statusIconControl.EntitySwisUri = triggeringObjectEntityUri;
        }

        string entityName = activeAlert.ObjectProperties["TriggeringObjectEntityName"] != null ? Convert.ToString(activeAlert.ObjectProperties["TriggeringObjectEntityName"]) : string.Empty;

        entityName = SetEntityNameForSpecialCases(entityName);

        this.statusIconControl.EntityFullName = string.Compare(entityName, "Orion.Container", StringComparison.InvariantCultureIgnoreCase) == 0 ? "Orion.Groups" : entityName;
        if (string.Compare(this.statusIconControl.EntityFullName, "Orion.Nodes", StringComparison.InvariantCultureIgnoreCase) == 0) 
        {
            this.statusIconControl.EntityIdName = "NodeID";
        }
        else if (string.Compare(this.statusIconControl.EntityFullName, "Orion.NPM.Interfaces", StringComparison.InvariantCultureIgnoreCase) == 0)
        {
            this.statusIconControl.EntityIdName = "InterfaceID";
        }
        else if (string.Compare(this.statusIconControl.EntityFullName, "Orion.Groups", StringComparison.InvariantCultureIgnoreCase) == 0)
        {
            this.statusIconControl.EntityIdName = "ContainerID";
        }
        else
        {
            string swqlQuery = "SELECT Name, Type FROM Metadata.Property WHERE EntityName=@entityName AND Name='Status' AND Type='System.Int32'";
            using (var proxy = InformationServiceProxy.CreateV3())
            {
                DataTable tblMetadata = proxy.Query(swqlQuery, new Dictionary<string, object> { { "entityName", entityName } });
                if (tblMetadata.Rows.Count == 0)
                {
                    this.statusIconControl.Visible = false;
                }
            }
        }

        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();
        this.lblAlertName.Text = HttpUtility.HtmlEncode(activeAlert.Name);
                
        string triggeringObjectCaption = activeAlert.ObjectProperties["TriggeringObjectCaption"] != null ? Convert.ToString(activeAlert.ObjectProperties["TriggeringObjectCaption"]) : string.Empty;
        
        string triggeringObjectDetailUrl;
        string elementClass = "NetObjectLink";
        if (string.IsNullOrEmpty(triggeringObjectEntityUri))
        {
            triggeringObjectDetailUrl = HttpContext.Current.Request.RawUrl;
            elementClass += " ForceTip";
            string newTriggeringObjectCaption = GetCaptionForGlobalAlert(this.statusIconControl.EntityFullName, activeAlert.ActiveAlertId);
            if (!string.IsNullOrEmpty(newTriggeringObjectCaption))
            {
                triggeringObjectCaption = newTriggeringObjectCaption;
            }
        }
        else
            triggeringObjectDetailUrl = activeAlert.ObjectProperties["TriggeringObjectDetailsUrl"] != null ? Convert.ToString(activeAlert.ObjectProperties["TriggeringObjectDetailsUrl"]) : string.Empty;

        string trigObject = string.Format("<a href=\"{0}\" class=\"{2}\">{1}</a>", triggeringObjectDetailUrl,HttpUtility.HtmlEncode(triggeringObjectCaption), elementClass);
        this.lblNodeName.Text = trigObject;


        if (!entityName.Equals("Orion.Nodes") && activeAlert.ObjectProperties.ContainsKey("RelatedNodeEntityUri") && activeAlert.ObjectProperties["RelatedNodeEntityUri"] != null)
        {
            var relatedNodeCaption = activeAlert.ObjectProperties["RelatedNodeCaption"] != null ? Convert.ToString(activeAlert.ObjectProperties["RelatedNodeCaption"]) : string.Empty;
            if (relatedNodeCaption.Equals(triggeringObjectCaption, StringComparison.OrdinalIgnoreCase))
            {
                //do not show the same name twice
                return;
            }

            var relatedNodeDetailUrl = activeAlert.ObjectProperties["RelatedNodeDetailsUrl"] != null ? Convert.ToString(activeAlert.ObjectProperties["RelatedNodeDetailsUrl"]) : string.Empty;
            var relatedNodeUri = Convert.ToString(activeAlert.ObjectProperties["RelatedNodeEntityUri"]);

            if (!string.IsNullOrEmpty(relatedNodeUri))
            {
                this.relatedNodeStatusIconControl.Visible = true;
                this.relatedNodeStatusIconControl.EntityId = activeAlert.ObjectProperties["RelatedNodeId"] != null ? Convert.ToString(activeAlert.ObjectProperties["RelatedNodeId"]) : string.Empty;
                this.relatedNodeStatusIconControl.EntitySwisUri = relatedNodeUri;
                this.relatedNodeStatusIconControl.EntityIdName = "NodeID";
            }

            if (!string.IsNullOrEmpty(relatedNodeCaption))
            {
                string relatedNode = string.Format("<a href=\"{0}\" class=\"{2}\">{1}</a>", relatedNodeDetailUrl, HttpUtility.HtmlEncode(relatedNodeCaption), elementClass);
                this.lblRelaytedNodeName.Visible = true;
                this.lblRelaytedNodeName.Text = relatedNode;
                relatedNodeLable.Visible = true;
                relatedNodeLable.Text = string.Format("- {0}", Resources.CoreWebContent.WEBDATA_PS0_86);
            }
        }
    }

    private string SetEntityNameForSpecialCases(string entityName)
    {
        // In the case of group member we want to display status for entity on which group member is pointing
        if (string.Compare(entityName, "Orion.ContainerMembers", StringComparison.OrdinalIgnoreCase) == 0 && !string.IsNullOrEmpty(statusIconControl.EntitySwisUri))
        {
            const string swqlQuery = @"SELECT TOP 1 CM.MemberEntityType, CM.MemberUri FROM Orion.ContainerMembers (nolock=true) AS CM WHERE CM.Uri = @entityUri";

            using (var proxy = InformationServiceProxy.CreateV3())
            {
                DataTable tblMemberInfo = proxy.Query(swqlQuery,
                    new Dictionary<string, object> {{"entityUri", this.statusIconControl.EntitySwisUri}});
                if (tblMemberInfo.Rows.Count > 0)
                {
                    entityName = tblMemberInfo.Rows[0]["MemberEntityType"] != DBNull.Value
                        ? Convert.ToString(tblMemberInfo.Rows[0]["MemberEntityType"])
                        : entityName;
                    statusIconControl.EntitySwisUri = tblMemberInfo.Rows[0]["MemberUri"] != DBNull.Value
                        ? Convert.ToString(tblMemberInfo.Rows[0]["MemberUri"])
                        : this.statusIconControl.EntitySwisUri;
                }
            }
        }

        //In the case of SysLog or Traps we want to display related node status
        if (string.Equals(entityName, "Orion.SysLog", StringComparison.InvariantCultureIgnoreCase) ||
            string.Equals(entityName, "Orion.Traps", StringComparison.InvariantCultureIgnoreCase))
        {
            entityName = "Orion.Nodes";
        }
        return entityName;
    }

    protected string SeverityImageUrl
    {
        get
        {
            var activeAlert = (ActiveAlert)this.NetObject;
            int severity = activeAlert.ObjectProperties["Severity"] != null ? Convert.ToInt32(activeAlert.ObjectProperties["Severity"]) : 1;
            string res = string.Empty;
            switch (severity)
            {
                case 0:
                    res = "/Orion/images/ActiveAlerts/InformationalAlert.png";
                    break;
                case 1:
                    res = "/Orion/images/ActiveAlerts/Warning.png";
                    break;
                case 2:
                    res = "/Orion/images/ActiveAlerts/Critical.png";
                    break;
                case 3:
                    res = "/Orion/images/ActiveAlerts/serious.png";
                    break;
                case 4:
                    res = "/Orion/images/ActiveAlerts/Notice.png";
                    break;
                default:
                    break;
            }

            return res;
        }
    }

    public override string ViewType
    {
        get { return "ActiveAlertDetails" ; }
    }
}