using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using Infragistics.UltraGauge.Resources;
using Infragistics.WebUI.UltraWebGauge;
using SolarWinds.NPM.Web.Gauge.V1;

using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.NpmAdapters;
using SolarWinds.Orion.Web.DAL;

public partial class GaugePage : System.Web.UI.Page
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    protected void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string style = Request.QueryString["Style"];
        string gaugeName = Request.QueryString["GaugeName"];
        string gaugeType = Request.QueryString["GaugeType"];
        string units = Request.QueryString["Units"];
        string startDate = Request.QueryString["StartDate"];
        string endDate = Request.QueryString["EndDate"];
        string customPollerID = Request.QueryString["CustomPollerID"];
        string customLegends = Request.QueryString["CustomLegends"];

        int scale;
        int max;
        int min;
        int tickCount;
        float warningThreshold;
        float errorThreshold;
        bool reverseThreshold;
        double value;
        string valueFormatString = String.Empty;

        if (Int32.TryParse(Request.QueryString["TickCount"], out tickCount)) { tickCount = 0; }
        if (!Single.TryParse(Request.QueryString["WarningThreshold"], out warningThreshold)) { warningThreshold = 0; }
        if (!Single.TryParse(Request.QueryString["ErrorThreshold"], out errorThreshold)) { errorThreshold = 0; }
        if (!Boolean.TryParse(Request.QueryString["ReverseThreshold"], out reverseThreshold)) { reverseThreshold = false; }
        if (!Int32.TryParse(Request.QueryString["Min"], out min)) { min = 0; }
        if (!Int32.TryParse(Request.QueryString["Max"], out max)) { max = 100; }
        if (!Int32.TryParse(Request.QueryString["Scale"], out scale)) { scale = 100; }
        if (!Double.TryParse(Request.QueryString["Value"], out value)) { value = 0; }
        if (!Single.TryParse(Request.QueryString["WarningThreshold"], out warningThreshold)) { warningThreshold = 0; }
        if (!Single.TryParse(Request.QueryString["ErrorThreshold"], out errorThreshold)) { errorThreshold = 0; }

        if (string.IsNullOrEmpty(style))
        {
            style = "Minimalist Dark";
        }

        string netObject = Request.QueryString["NetObject"];
        int netObjectID = 0;
        string netObjectPrefix = string.Empty;
        if (!string.IsNullOrEmpty(netObject))
        {
            netObjectID = Convert.ToInt32(netObject.Substring(netObject.IndexOf(":") + 1, netObject.Length - netObject.IndexOf(":") - 1));
            netObjectPrefix = netObject.Substring(0, netObject.IndexOf(":"));
            GetDataByName(gaugeName, netObjectPrefix, netObjectID, ref  value, ref warningThreshold, ref errorThreshold, startDate, endDate, customPollerID, ref valueFormatString, ref reverseThreshold);
        }

        MemoryStream stream = new MemoryStream();
        System.Drawing.Image image = null;
        string decodedBottomTitle = HttpUtility.UrlDecode(Request.QueryString["Property"]);
        BaseGaugeGenerator generator;
        if (gaugeType.Equals("Radial"))
        {
            generator = new GaugeGenerator();
            UltraGauge gauge = (string.IsNullOrEmpty(customPollerID)) ?
                                generator.GenerateGauge(scale, style, value, min, max, value.ToString() + units,
                                decodedBottomTitle, warningThreshold, errorThreshold, reverseThreshold) :
                                generator.GenerateCustomPollerGauge(scale, style, value, min, max, valueFormatString,
                                decodedBottomTitle, warningThreshold, errorThreshold,
                                customLegends, tickCount, reverseThreshold);

            image = gauge.GetImage(Infragistics.UltraGauge.Resources.GaugeImageType.Png, new Size((int)(gauge.Width.Value), (int)(gauge.Height.Value + 15)));
        }
        else
        {
            generator = new LinearGaugeGenerator();
            image = (string.IsNullOrEmpty(customPollerID)) ?
                                ((LinearGaugeGenerator)generator).GetGaugeImage(scale, style, value, min, max, value.ToString() + units,
                                decodedBottomTitle, warningThreshold, errorThreshold, reverseThreshold) :
                                ((LinearGaugeGenerator)generator).GetCustomPollerGaugeImage(scale, style, value, min, max, valueFormatString,
                                decodedBottomTitle, warningThreshold, errorThreshold,
                                customLegends, tickCount, reverseThreshold);
        }


        image.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
        Response.ContentType = "image/png";
        stream.WriteTo(Response.OutputStream);
        Response.End();
    }

    private void GetDataByName(string gaugeName, string netObjectPrefix, int netObjectID, ref double value, ref  float warningThreshold, ref float errorThreshold,
        string startDate, string endDate, string customPollerID, ref string valueFormatString, ref bool reverseThreshold)
    {
        var adapter = NpmAdapterFactory.CreateGaugeAndChartDataAdapter();
        bool handled = adapter.GetGaugeDataByName(gaugeName, netObjectPrefix, netObjectID, ref value, ref warningThreshold,
                                                  ref errorThreshold, startDate, endDate, customPollerID,
                                                  ref valueFormatString, ref reverseThreshold);

        if (handled)
        {
            // The NPM code handled this so we are done.
            return;
        }

        switch (netObjectPrefix.ToUpperInvariant())
        {
            case "N":
                GetDataForNodeGauge(gaugeName, netObjectID, ref value, ref warningThreshold, ref errorThreshold, startDate, endDate, customPollerID, ref valueFormatString, ref reverseThreshold);
                break;
            case "V":
                GetDataForVolumeGauge(gaugeName, netObjectID, ref value, ref warningThreshold, ref errorThreshold, startDate, endDate, customPollerID, ref valueFormatString, ref reverseThreshold);
                break;
        }
    }

    private void GetDataForNodeGauge(string gaugeName, int netObjectID, ref  double value, ref float warningThreshold, ref float errorThreshold,
        string startDate, string endDate, string customPollerID, ref string valueFormatString, ref bool reverseThreshold)
    {
        using (var coreProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
        {
            var node = coreProxy.GetNodeWithOptions(netObjectID, false, false);

            switch (gaugeName.ToUpperInvariant())
            {
                case "CPULOAD":
                    value = node.CPULoad;
                    using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
                    {
                        var nodeThresholds = service.Query(string.Format("SELECT [Level1Value], [Level2Value] FROM Orion.NodesThresholds WHERE [EntityType] = '{0}' AND [InstanceId] = {1} AND [Name] = '{2}'", "Orion.Nodes", netObjectID, "Nodes.Stats.CpuLoad"));
                        warningThreshold = (float)(double)nodeThresholds.Rows[0]["Level1Value"];
                        errorThreshold = (float)(double)nodeThresholds.Rows[0]["Level2Value"];
                    }
                    break;
                case "MEMORYUSED":
                    value = node.PercentMemoryUsed;
                    using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
                    {
                        var nodeThresholds = service.Query(string.Format("SELECT [Level1Value], [Level2Value] FROM Orion.NodesThresholds WHERE [EntityType] = '{0}' AND [InstanceId] = {1} AND [Name] = '{2}'", "Orion.Nodes", netObjectID, "Nodes.Stats.PercentMemoryUsed"));
                        warningThreshold = (float)(double)nodeThresholds.Rows[0]["Level1Value"];
                        errorThreshold = (float)(double)nodeThresholds.Rows[0]["Level2Value"];
                    }
                    break;
                case "AVGRESPTIME":
                    value = node.AvgResponseTime;
                    using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
                    {
                        var nodeThresholds = service.Query(string.Format("SELECT [Level1Value], [Level2Value] FROM Orion.NodesThresholds WHERE [EntityType] = '{0}' AND [InstanceId] = {1} AND [Name] = '{2}'", "Orion.Nodes", netObjectID, "Nodes.Stats.ResponseTime"));
                        warningThreshold = (float)(double)nodeThresholds.Rows[0]["Level1Value"];
                        errorThreshold = (float)(double)nodeThresholds.Rows[0]["Level2Value"];
                    }
                    break;
                case "PACKETLOSS":
                    value = node.PercentLoss;
                    using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
                    {
                        var nodeThresholds = service.Query(string.Format("SELECT [Level1Value], [Level2Value] FROM Orion.NodesThresholds WHERE [EntityType] = '{0}' AND [InstanceId] = {1} AND [Name] = '{2}'", "Orion.Nodes", netObjectID, "Nodes.Stats.PercentLoss"));
                        warningThreshold = (float)(double)nodeThresholds.Rows[0]["Level1Value"];
                        errorThreshold = (float)(double)nodeThresholds.Rows[0]["Level2Value"];
                    }
                    break;
                case "AVAILABILITY":
                    value = Convert.ToInt32(coreProxy.GetAvailability(netObjectID, Convert.ToDateTime(startDate), Convert.ToDateTime(endDate)));
                    warningThreshold = (float)90; // default gauge value
                    errorThreshold = (float)95; // default gauge value
                    break;
                case "RESPONSETIME":
                    value = node.ResponseTime;
                    using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
                    {
                        var nodeThresholds = service.Query(string.Format("SELECT [Level1Value], [Level2Value] FROM Orion.NodesThresholds WHERE [EntityType] = '{0}' AND [InstanceId] = {1} AND [Name] = '{2}'", "Orion.Nodes", netObjectID, "Nodes.Stats.ResponseTime"));
                        warningThreshold = (float)(double)nodeThresholds.Rows[0]["Level1Value"];
                        errorThreshold = (float)(double)nodeThresholds.Rows[0]["Level2Value"];
                    }
                    break;
                case "MINRESPTIME":
                    value = node.MinResponseTime;
                    using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
                    {
                        var nodeThresholds = service.Query(string.Format("SELECT [Level1Value], [Level2Value] FROM Orion.NodesThresholds WHERE [EntityType] = '{0}' AND [InstanceId] = {1} AND [Name] = '{2}'", "Orion.Nodes", netObjectID, "Nodes.Stats.ResponseTime"));
                        warningThreshold = (float)(double)nodeThresholds.Rows[0]["Level1Value"];
                        errorThreshold = (float)(double)nodeThresholds.Rows[0]["Level2Value"];
                    }
                    break;
                case "MAXRESPTIME":
                    value = node.MaxResponseTime;
                    using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
                    {
                        var nodeThresholds = service.Query(string.Format("SELECT [Level1Value], [Level2Value] FROM Orion.NodesThresholds WHERE [EntityType] = '{0}' AND [InstanceId] = {1} AND [Name] = '{2}'", "Orion.Nodes", netObjectID, "Nodes.Stats.ResponseTime"));
                        warningThreshold = (float)(double)nodeThresholds.Rows[0]["Level1Value"];
                        errorThreshold = (float)(double)nodeThresholds.Rows[0]["Level2Value"];
                    }
                    break;
                case "TOTALVMCPULOAD":
                    value = Convert.ToDouble(Convert.ToInt32((new VMWareSwisDAL()).GetTotalCPULoadForESXServer(node.ID) ?? 0));
                    valueFormatString = value.ToString("f");
                    using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
                    {
                        var nodeThresholds = service.Query(string.Format("SELECT [Level1Value], [Level2Value] FROM Orion.NodesThresholds WHERE [EntityType] = '{0}' AND [InstanceId] = {1} AND [Name] = '{2}'", "Orion.Nodes", netObjectID, "Nodes.Stats.CpuLoad"));
                        warningThreshold = (float)(double)nodeThresholds.Rows[0]["Level1Value"];
                        errorThreshold = (float)(double)nodeThresholds.Rows[0]["Level2Value"];
                    }

                    break;
                case "TOTALVMMEMORYUSAGE":
                    value = Convert.ToDouble(Convert.ToInt32((new VMWareSwisDAL()).GetTotalMemoryLoadForESXServer(node.ID) ?? 0));
                    valueFormatString = value.ToString("f");
                    using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
                    {
                        var nodeThresholds = service.Query(string.Format("SELECT [Level1Value], [Level2Value] FROM Orion.NodesThresholds WHERE [EntityType] = '{0}' AND [InstanceId] = {1} AND [Name] = '{2}'", "Orion.Nodes", netObjectID, "Nodes.Stats.PercentMemoryUsed"));
                        warningThreshold = (float)(double)nodeThresholds.Rows[0]["Level1Value"];
                        errorThreshold = (float)(double)nodeThresholds.Rows[0]["Level2Value"];
                    }
                    break;
            }
        }
    }



    private void GetDataForVolumeGauge(string gaugeName, int netObjectID, ref  double value, ref float warningThreshold, ref float errorThreshold,
        string startDate, string endDate, string customPollerID, ref string valueFormatString, ref bool reverseThreshold)
    {
        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
        {
            SolarWinds.Orion.Core.Common.Models.Volume volume = proxy.GetVolume(netObjectID);

            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                var volumeThresholds = service.Query("SELECT [Level1Value], [Level2Value] FROM Orion.VolumesThresholds " +
                                                     $"WHERE [EntityType] = 'Orion.Volumes' AND [InstanceId] = {netObjectID} AND [Name] = 'Volumes.Stats.PercentDiskUsed'");

                switch (gaugeName.ToUpperInvariant())
                {
                    case "AVAILABLESPACE":
                        value = 100 - Convert.ToInt32(volume.VolumePercentUsed);
                        warningThreshold = 100 - (float)(double)volumeThresholds.Rows[0]["Level1Value"];
                        errorThreshold = 100 - (float)(double)volumeThresholds.Rows[0]["Level2Value"];
                        reverseThreshold = true;
                        break;
                    case "SPACEUSED":
                        value = Convert.ToInt32(volume.VolumePercentUsed);
                        warningThreshold = (float)(double)volumeThresholds.Rows[0]["Level1Value"];
                        errorThreshold = (float)(double)volumeThresholds.Rows[0]["Level2Value"];
                        reverseThreshold = false;
                        break;
                }
            }
        }
    }


}
