using System;
using System.Collections.Generic;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_NetPerfMon_PrintableOrionMessages : System.Web.UI.Page
{
	private static readonly Log _log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
	private static readonly RemoteFeatureManager _featureManager = new RemoteFeatureManager();
	private bool _syslogsEnabled;
	private bool _trapsEnabled;

	protected void BusinessLayerExceptionHandler(Exception ex)
	{
		_log.Error(ex);
	}

	private readonly IList<string> _messageTypes = new List<string> { Resources.CoreWebContent.WEBDATA_VB0_240, Resources.CoreWebContent.WEBCODE_VB0_241 };

	public string Title2
	{
		get
		{
			string titlepart = CommonWebHelper.GetTitlePart(_messageTypes);
			if (string.IsNullOrEmpty(Request.QueryString["NetObject"]))
			{
				if (string.IsNullOrEmpty(Request.QueryString["DeviceType"]))
				{
                    return string.Format(Resources.CoreWebContent.WEBCODE_VB0_268, WebSecurityHelper.SanitizeHtmlV2(Periods.GetLocalizedPeriod(Request.QueryString["Period"])), titlepart);
				}
				else
				{
                    return string.Format(Resources.CoreWebContent.WEBCODE_VB0_269, "<font color='blue'>" + WebSecurityHelper.SanitizeHtmlV2(Request.QueryString["DeviceType"]) + "</font>", WebSecurityHelper.SanitizeHtmlV2(Periods.GetLocalizedPeriod(Request.QueryString["Period"])), titlepart);
				}
			}
			else
			{
                using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
				{
					Node node = proxy.GetNode(Convert.ToInt32(NetObjectHelper.GetObjectID(Request.QueryString["NetObject"])));
                    return string.Format(Resources.CoreWebContent.WEBCODE_JP0_1, node.Name, WebSecurityHelper.SanitizeHtmlV2(Periods.GetLocalizedPeriod(Request.QueryString["Period"])), titlepart);
				}
			}
		}
	}
	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{ 
			_syslogsEnabled = _featureManager.IsFeatureEnabled("Syslog");
			_trapsEnabled = _featureManager.IsFeatureEnabled("Traps");

			if (_syslogsEnabled && !_messageTypes.Contains(Resources.CoreWebContent.WEBDATA_VB0_209))
				_messageTypes.Add(Resources.CoreWebContent.WEBDATA_VB0_209);

			if (_trapsEnabled && !_messageTypes.Contains(Resources.CoreWebContent.WEBCODE_VB0_274))
				_messageTypes.Add(Resources.CoreWebContent.WEBCODE_VB0_274);
		}

		bool showAcknowledged = false;
		bool.TryParse(Request.QueryString["ShowAcknowledged"], out showAcknowledged);

		bool includeEvents = false;
		bool.TryParse(Request.QueryString["ShowEvents"], out includeEvents);

		bool includeAlerts = false;
		bool.TryParse(Request.QueryString["ShowAlerts"], out includeAlerts);
		
        bool includeAudits = false;
		bool.TryParse(Request.QueryString["ShowAudits"], out includeAudits);

		bool includeSyslog = false;
		if (bool.TryParse(Request.QueryString["ShowSyslogs"], out includeSyslog))
			includeSyslog &= _syslogsEnabled;

		bool includeTraps = false;
		if (bool.TryParse(Request.QueryString["ShowTraps"], out includeTraps))
			includeTraps &= _trapsEnabled;
		
		string period = WebSecurityHelper.SanitizeHtmlV2(Request.QueryString["Period"].ToString());

		int? nodeId = null;
		int netObject;
		if (int.TryParse(NetObjectHelper.GetObjectID(Request.QueryString["NetObject"].ToString()), out netObject))
			nodeId = netObject;

		string deviceType = WebSecurityHelper.SanitizeHtmlV2(Request.QueryString["DeviceType"].ToString());

		int maxRecords;
		int.TryParse(Request.QueryString["MaxRecords"], out maxRecords);

		string vendor = Request.QueryString["Vendor"].ToString();
		string ip_address = Request.QueryString["IPAddress"].ToString();
		string hostname = Request.QueryString["Hostname"].ToString();

		string alertType = Request.QueryString["AlertType"].ToString();
		string eventType = Request.QueryString["EventType"].ToString();
        string auditType = Request.QueryString["AuditType"].ToString();
        string auditUser = Request.QueryString["AuditUser"].ToString();

		byte severityCode = 0;
		byte facilityCode = 0;
		// 255 if none selected
		if (!byte.TryParse(Request.QueryString["Severity"], out severityCode))
			severityCode = 255;
		// 255 if none selected
		if (!byte.TryParse(Request.QueryString["Facility"], out facilityCode))
			facilityCode = 255;

		string trapType = Request.QueryString["TrapType"].ToString();
		string community = Request.QueryString["Community"].ToString();

		string search = Request.QueryString["SearchString"].ToString();

	    int? siteId = null;
	    int id;
	    if (int.TryParse(Request.QueryString["SiteID"], out id))
	        siteId = id;

        DateTime fromDate = new DateTime();
        DateTime toDate = new DateTime();
        Periods.Parse(ref period, ref fromDate, ref toDate);

	    OrionMessagesFilter filter = new OrionMessagesFilter
	                                     {
	                                         NodeId = nodeId,
	                                         DeviceType = deviceType,
	                                         Vendor = vendor,
	                                         IpAddress = ip_address,
	                                         Hostname = hostname,
	                                         FromDate = fromDate,
	                                         ToDate = toDate,
	                                         Count = maxRecords,
	                                         ShowAcknowledged = showAcknowledged,
	                                         IncludeAlerts = includeAlerts,
	                                         IncludeEvents = includeEvents,
	                                         IncludeSyslogs = includeSyslog,
	                                         IncludeTraps = includeTraps,
	                                         IncludeAudits = includeAudits,
	                                         AlertType = alertType,
                                             AuditType = auditType,
                                             Audituser = auditUser,
	                                         EventType = eventType,
	                                         SyslogSeverity = severityCode,
	                                         SyslogFacility = facilityCode,
	                                         TrapType = trapType,
	                                         TrapCommunity = community,
	                                         SearchString = search,
                                             SiteID = siteId
	                                     };

	    orionMessagesReport.GenerateReport(filter);
	}

    protected void Page_Init(object sender, EventArgs e)
    {
        SolarWinds.Orion.Web.OrionInclude.CoreThemeStylesheets();
    }
}
