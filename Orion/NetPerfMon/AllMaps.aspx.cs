﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using SolarWinds.MapEngine;
using SolarWinds.MapEngine.Data;
using System.Web.UI.HtmlControls;
using SolarWinds.MapEngine.Helpers;
using SolarWinds.Orion.Core.Common.i18n.Registrar;

public partial class Orion_NetPerfMon_AllMaps : System.Web.UI.Page
{
    protected string ServerAddress { get; set; }
    protected string SwisPort { get; set; }
    protected string SwisPostfix { get; set; }
    protected string UserName { get; set; }
    protected SolarWinds.InformationService.Contract2.ServiceCredentials UsernameCredentials { get; set; }

    protected override void OnInit(EventArgs e)
    {
        string swoisEndpoint = string.Format(System.Configuration.ConfigurationManager.AppSettings["SWOISv3.RemoteEndpoint"], "localhost");
        string serverAddress, swisPort, swisPostfix;
        MapService.ParseEndpointLocation(swoisEndpoint, out serverAddress, out swisPort, out swisPostfix);

        ServerAddress = serverAddress;
        SwisPort = swisPort;
        SwisPostfix = swisPostfix;
        UserName = HttpContext.Current.Profile.UserName;

        if (UsernameCredentials == null)
        {
            UsernameCredentials = CredentialsHelper.GetOrionCertificateCredentials();
        }
		
        mapsTable.Controls.Add(GetMapThumbnails());

        base.OnInit(e);
    }

    private HtmlTable GetMapThumbnails()
    {
        IEngine engine = MapEngineFactory.GetEngine(UserName, UsernameCredentials, ServerAddress, SwisPort, SwisPostfix, 0, true);
        IMappingControlEngine mappingEngine = engine as IMappingControlEngine;
        engine.RootLocalPath = MapService.GetTempPath();
		engine.RootApplicationPath = MapService.GetMapStudioInstallPath();
        engine.SetAlternativeMapWebPath();
        List<MapNetObject> maps = mappingEngine.GetAvailableMaps(true, false);
        int columns = (int)Math.Ceiling(Math.Sqrt((double)maps.Count));
        int rows = (int)Math.Ceiling((double)maps.Count / (double)columns);
        int selectedMapObjectIndex = 0;
        HtmlTable table = new HtmlTable();
        for (int i = 0; i < rows; i++)
        {
            HtmlTableRow row = new HtmlTableRow();
            for (int j = 0; j < columns && selectedMapObjectIndex < maps.Count; j++)
            {
                HtmlTableCell cell = new HtmlTableCell();
                Orion_NetPerfMon_Controls_MapThumbnail thumbnail = (Orion_NetPerfMon_Controls_MapThumbnail)LoadControl("~/Orion/NetPerfMon/Controls/MapThumbnail.ascx");
                thumbnail.MapId = maps[selectedMapObjectIndex].SolarWindsId.ToString().Replace("Maps: ", string.Empty);
                thumbnail.MapName = GetLocalizedProperty("MapName", maps[selectedMapObjectIndex].DisplayName);
                thumbnail.TableWidth = 200;
                cell.Controls.Add(thumbnail);
                cell.Style.Add(HtmlTextWriterStyle.VerticalAlign, "top");
                row.Cells.Add(cell);
                selectedMapObjectIndex++;
            }
            table.Rows.Add(row);
        }
        table.Style.Add(HtmlTextWriterStyle.MarginLeft, "auto");
        table.Style.Add(HtmlTextWriterStyle.MarginRight, "auto");
        engine.Close();
        return table;
    }


    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
}
