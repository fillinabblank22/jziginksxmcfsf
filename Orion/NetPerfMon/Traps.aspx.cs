using System;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.Core.Web.Reporting;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.UI;
using Limitation = SolarWinds.Orion.Web.Limitation;
using SettingsDAL = SolarWinds.Orion.Web.DAL.SettingsDAL;
using System.Web;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SyslogTrapsReporting = SolarWinds.SyslogTraps.Web.Reporting;

public partial class Orion_NetPerfMon_Traps : System.Web.UI.Page
{
    private static readonly Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    protected void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }
    private bool _hideDetails;
    private int _maxRecords;

    private string GetLocalizedPeriod
    {
        get
        {
            if (Period.Contains("~"))
            {
                return Period;
            }
            return Periods.GetLocalizedPeriod(Period);
        }
    }


    public string Period
    {
        get
        {
            if (pickerTimePeriodControl.TimePeriodText == "Custom")
            {
                if (string.IsNullOrEmpty(pickerTimePeriodControl.CustomPeriodBegin))
                {
                    pickerTimePeriodControl.CustomPeriodBegin = "0:00:00";
                }
                if (string.IsNullOrEmpty(pickerTimePeriodControl.CustomPeriodEnd))
                {
                    pickerTimePeriodControl.CustomPeriodEnd = "23:59:59";
                }

                return pickerTimePeriodControl.CustomPeriodBegin + "~" + pickerTimePeriodControl.CustomPeriodEnd;
            }
            else
            {
                return pickerTimePeriodControl.TimePeriodText;
            }
        }
    }

    public string Title2
    {
        get
        {
            if (string.IsNullOrEmpty(netObjects.NetObjectID))
            {
                if (string.IsNullOrEmpty(netObjectTypes.DeviceType))
                {
                    return String.Format(Resources.SyslogTrapsWebContent.WEBCODE_TM0_59, GetLocalizedPeriod);
                }
                else
                {
                    return String.Format(Resources.SyslogTrapsWebContent.WEBCODE_TM0_60, netObjectTypes.DeviceType, GetLocalizedPeriod);
                }
            }
            else
            {
                using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
                {
                    Node node = proxy.GetNode(Convert.ToInt32(NetObjectHelper.GetObjectID(netObjects.NetObjectID)));
                    return String.Format(Resources.SyslogTrapsWebContent.WEBCODE_TM0_61, node.Name, GetLocalizedPeriod);
                }
            }
        }
    }

    public bool HideDatails
    {
        get { return _hideDetails; }
        set { _hideDetails = value; }
    }

    private void PopulateChildControls()
    {
        if (!Int32.TryParse(maxRecords.Text, out _maxRecords))
        {
            _maxRecords = 0;
        }
        else
        {
            _maxRecords = Math.Min(Math.Max(_maxRecords, 0), 2000);
        }

        maxRecords.Text = _maxRecords.ToString();

        if (maxRecords.Text.Trim() != previousMaxRecords.Value.ToString())
        {
            WebUserSettingsDAL.Set("Web-TrapMaxMsgPerPage", _maxRecords.ToString());
            previousMaxRecords.Value = _maxRecords.ToString();
        }

        bool printable;
        if ((Request.QueryString["Printable"] != null) && bool.TryParse(Request.QueryString["Printable"], out printable))
        {
            if (printable)
            {
                Printable_Click(this, null);
            }
        }

        communityStrings.Visible = Profile.AllowAdmin;
        if (Request.QueryString["Community"] != null)
        {
            communityStrings.CommunityString = Request.QueryString["Community"].ToString();
        }

        trapsReport.GenerateReport(netObjects.NetObjectID, Period, netObjectTypes.DeviceType, trapTypes.TrapType, sourceIpAddress.Text, communityStrings.CommunityString, _maxRecords);

        ExportToPDFLink.PageUrl = GetUrlWithParams(Request.Url.OriginalString.Split('?')[0]);
    }

    private void PopulateControlsFromRequest()
    {
        if (Request.QueryString["IPAddress"] != null)
            sourceIpAddress.Text = Request.QueryString["IPAddress"].ToString();

        if (Request.QueryString["Caption"] != null)
            netObjects.HostName = Request.QueryString["Caption"].ToString();

        if (Request.QueryString["NetObject"] != null)
            netObjects.NetObjectID = Request.QueryString["NetObject"].ToString();

        if (Request.QueryString["TrapType"] != null)
            trapTypes.TrapType = Request.QueryString["TrapType"].ToString();

        if (Request.QueryString["HostName"] != null)
            netObjects.HostName = Request.QueryString["HostName"].ToString();

        if (Request.QueryString["MachineType"] != null)
            netObjectTypes.DeviceType = Request.QueryString["MachineType"].ToString();
        else
        {
            if (Request.QueryString["DeviceType"] != null)
            {
                netObjectTypes.DeviceType = Request.QueryString["DeviceType"].ToString();
            }
        }

        if (Request.QueryString["HideDetails"] != null)
        {
            var hideDetails = bool.Parse(Request.QueryString["HideDetails"]);
            btnShow.Visible = hideDetails;
            btnHide.Visible = !hideDetails;
            details.Visible = !hideDetails;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!(new RemoteFeatureManager()).IsFeatureEnabled("Traps"))
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.SyslogTrapsWebContent.WEBCODE_VB0_329), Title = Resources.SyslogTrapsWebContent.WEBCODE_VB0_347 });

        maxRecords.ValidatorText = String.Format("<img src='/Orion/images/Small-Down.gif' title='{0}'/>", Resources.SyslogTrapsWebContent.WEBDATA_VB0_220);
    }

    void DisplayReportInCustomFormat(ReportFormat reportFormat)
    {

        ReportSettings settings = ReportHelper.GetReportSettings(Period, netObjects.NetObjectID);
        SyslogTrapsReporting.TrapsReport report = new SyslogTrapsReporting.TrapsReport(reportFormat, settings);

        int maxRecordsToDisplay = GetMaxRecords();

        report.GenerateReport(netObjectTypes.DeviceType, trapTypes.TrapType, sourceIpAddress.Text,
            communityStrings.CommunityString, maxRecordsToDisplay, Limitation.GetCurrentListOfLimitationIDs(),
            BusinessLayerExceptionHandler);

        ReportHelper.WriteReportToResponse(Response, report);

    }

    private int GetMaxRecords()
    {
        int maxRecordsToDisplay = 0;
        if (!Int32.TryParse(maxRecords.Text, out maxRecordsToDisplay))
        {
            maxRecordsToDisplay = 0;
        }
        else if (_maxRecords < 0)
        {
            maxRecordsToDisplay = 0;
        }
        return maxRecordsToDisplay;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializeMaxRecords();

            ReportFormat? reportFormat = ReportHelper.GetRequestedFormat(Request);

            if (reportFormat != null && reportFormat.Value != ReportFormat.HTML)
            {
                DisplayReportInCustomFormat(reportFormat.Value);
            }
        }
    }

    private void InitializeMaxRecords()
    {
        String userSettingValue = (Request.QueryString["MaxMessages"] != null) ? Request.QueryString["MaxMessages"].ToString() : WebUserSettingsDAL.Get("Web-TrapMaxMsgPerPage");
        if (String.IsNullOrEmpty(userSettingValue))
        {
            String defaultSettingValue = SettingsDAL.GetSetting("Web-TrapMaxMsgPerPage").SettingValue.ToString();
            maxRecords.Text = defaultSettingValue;
            WebUserSettingsDAL.Set("Web-TrapMaxMsgPerPage", defaultSettingValue);
        }
        else
        {
            maxRecords.Text = userSettingValue;
        }
        previousMaxRecords.Value = maxRecords.Text;
    }

    protected void Refresh_Click(object sender, EventArgs e)
    {
    }

    protected void Show_Click(object sender, EventArgs e)
    {
        if (sender as LocalizableButton == null) return;

        if (((LocalizableButton)sender).ID == "btnShow")
        {
            btnShow.Visible = false;
            btnHide.Visible = true;

            details.Visible = true;
        }
        else
        {
            btnShow.Visible = true;
            btnHide.Visible = false;

            details.Visible = false;
        }
    }

    protected override void OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);

        if (!IsPostBack)
        {
            PopulateControlsFromRequest();
        }

        PopulateChildControls();
    }

    private string GetUrlWithParams(string pageUrl)
    {
        return string.Format(@"{0}?NetObject={1}&MaxRecords={2}&Period={3}&DeviceType={4}&TrapType={5}&IPAddress={6}&Community={7}&HideDetails={8}",
            pageUrl,
            netObjects.NetObjectID,
            maxRecords.Text,
            Period,
            netObjectTypes.DeviceType,
            trapTypes.TrapType,
            Server.UrlEncode(sourceIpAddress.Text),
            communityStrings.CommunityString,
            !details.Visible);
    }

    protected void Printable_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetUrlWithParams("PrintableTraps.aspx"));
    }
}
