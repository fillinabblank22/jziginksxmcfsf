<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/OrionMasterPage.master" CodeFile="Traps.aspx.cs" Inherits="Orion_NetPerfMon_Traps" EnableEventValidation="false" EnableViewState="true" Title="<%$ Resources: SyslogTrapsWebContent, WEBDATA_TM0_176 %>" %>

<%@ Register Assembly="OrionWeb" Namespace="SolarWinds.Orion.Web.UI" TagPrefix="web" %>
<%@ Register TagPrefix="orion" TagName="NetworkObjects" Src="~/Orion/Controls/NetworkObjectControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="NetworkObjectTypes" Src="~/Orion/Controls/TypeOfDevicerControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="TrapTypes" Src="~/Orion/Controls/TrapTypeControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="CommunityStrings" Src="~/Orion/Controls/CommunityStringControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="HelpButton" Src="~/Orion/Controls/HelpButton.ascx" %>
<%@ Register TagPrefix="orion" TagName="TrapsReport" Src="~/Orion/Controls/TrapsReportControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="Reposter" Src="~/Orion/Controls/Reposter.ascx" %>
<%@ Register TagPrefix="orion" TagName="PickerTimePeriodControl" Src="~/Orion/Controls/PickerTimePeriodControl.ascx" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconLinkExportToPDF runat="server" ID="ExportToPDFLink" />
    <asp:LinkButton ID="btnPrintable" runat="server" CssClass="printablePageLink" OnClick="Printable_Click" Text="<%$ Resources: SyslogTrapsWebContent, WEBDATA_VB0_246 %>" />
    <orion:IconHelpButton HelpUrlFragment="OrionPHTrapsView" ID="helpButton" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="placeHolder" runat="server">
    <orion:Reposter ID="Reposter1" runat="server" />
    <asp:PlaceHolder ID="holder" runat="server" >
        <div class="dateArea">  
        </div>
        <h1><%=Page.Title %></h1>
        <div  style="margin-left: 5px; margin-right: 5px;">
            <table style="width: 100%;">
                <tr>
                    <td><b><%=Title2 %></b></td>
                    <td style="width:40px" >
                    </td>
                    <td style="text-align:right; width:110px" >
                    </td>
                    <td style="text-align:right; width:40px " >
                        <orion:LocalizableButton runat="server" ID="btnHide" LocalizedText="CustomText" 
                            Text="<%$ Resources: SyslogTrapsWebContent, WEBDATA_VB0_223 %>" DisplayType="Resource" 
                            OnClick="Show_Click" />
                        <orion:LocalizableButton runat="server" ID="btnShow" LocalizedText="CustomText" 
                            Text="<%$ Resources: SyslogTrapsWebContent, WEBDATA_VB0_219 %>" DisplayType="Resource" 
                            OnClick="Show_Click" Visible="false" />
                    </td>
                </tr>
            </table>
            <asp:PlaceHolder runat="server" ID="details">
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 85px;font-size: 7pt;"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_210 %>&nbsp;</td>
                        <td class="formTable">
                            <table class="formTable">
                                <tr>
                                    <td><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_211 %></td>
                                    <td></td>
                                    <td><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_212 %></td>
                                </tr>
                                <tr>
                                    <td><orion:NetworkObjects runat="server" ID="netObjects" PageType="Traps" /></td>
                                    <td>&nbsp;<b style="color: #5c5c5c;"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_214 %></b>&nbsp;</td>
                                    <td><orion:NetworkObjectTypes runat="server" ID="netObjectTypes" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="width: 85px;font-size: 7pt;"><%= Resources.SyslogTrapsWebContent.WEBDATA_TM0_174 %>&nbsp;</td>
                        <td class="formTable">
                            <table class="formTable">
                                <tr>
                                    <td><%= Resources.SyslogTrapsWebContent.WEBDATA_TM0_5 %></td>
                                    <td><%= Resources.SyslogTrapsWebContent.WEBDATA_TM0_175 %></td>
                                    <td><%if (Profile.AllowAdmin)
                                            { %><%= Resources.SyslogTrapsWebContent.WEBDATA_TM0_177 %><%}
                                            else
                                            { %>&nbsp;<%} %></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><orion:TrapTypes runat="server" ID="trapTypes" /></td>
                                    <td><asp:TextBox runat="server" ID="sourceIpAddress" Width="260"></asp:TextBox></td>
                                    <td><orion:CommunityStrings runat="server" ID="communityStrings" /> </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td></td>
                                </tr>
                            </table>
                            <table class="formTable">
                                <tr>
                                    <td><%= Resources.SyslogTrapsWebContent.WEBDATA_AK0_337 %></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><orion:PickerTimePeriodControl ID="pickerTimePeriodControl" InLine="true" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_542 %>&nbsp;<web:ValidatedTextBox CustomStringValue=""
                                        Text="250" ID="maxRecords" Columns="5"
                                        Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                        MinValue="0" MaxValue="2000" />
                                        <asp:HiddenField ID="previousMaxRecords" runat="server" /></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <div class="sw-btn-bar">
                                            <orion:LocalizableButton runat="server" ID="ImageButton1" OnClick="Refresh_Click" LocalizedText="Refresh" DisplayType="Primary" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:PlaceHolder>
            
            <orion:TrapsReport runat="server" ID="trapsReport" />
        </div>
    </asp:PlaceHolder>
</asp:Content>