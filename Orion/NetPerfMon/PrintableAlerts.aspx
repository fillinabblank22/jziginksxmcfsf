<%@ Page Language="C#" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_VB0_241 %>" AutoEventWireup="true" CodeFile="PrintableAlerts.aspx.cs" Inherits="Orion_NetPerfMon_PrintableAlerts" MasterPageFile="~/Orion/OrionMinReqs.master" %>
<%@ Register TagPrefix="orion" TagName="AlertsReport" Src="~/Orion/Controls/AlertsReportControl.ascx"  %>
<%@ Register TagPrefix="orion" TagName="NetObjectTips" Src="~/Orion/Controls/NetObjectTips.ascx" %>

<asp:Content ContentPlaceHolderID="BodyContent" runat="server">

    <orion:Include runat="server" File="MainLayout.css" />
    <orion:Include runat="server" File="Resources.css" />
    <orion:Include runat="server" File="jquery-1.7.1/jquery.livequery.js" />

    <form id="form1" runat="server" class="sw-app-region">
        <asp:ScriptManager runat="server" ID="MasterScriptManager" EnablePageMethods="true" AllowCustomErrorsRedirect="false" AsyncPostBackErrorMessage="ScriptAsyncPostBackError" ></asp:ScriptManager>
        <h1>
            <%= DefaultSanitizer.SanitizeHtml(Title2) %>
       </h1>
	<orion:NetObjectTips ID="NetObjectTips1" runat="server" />
        <div style="margin:10px 10px 10px 10px">
            <orion:AlertsReport runat="server" ID="alertsReport" Printable="true" />
        </div>
    </form>

</asp:Content>