using System;
using System.Web.UI.HtmlControls;

using SolarWinds.NPM.Common;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web.DAL;
using Node = SolarWinds.Orion.Core.Common.Models.Node;
using WebSettingsDAL = SolarWinds.Orion.Web.DAL.WebSettingsDAL;

public partial class Orion_NetPerfMon_PrintableAlerts : System.Web.UI.Page
{
	private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

	protected void BusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}

	public string Title2
	{
		get
		{
			if (string.IsNullOrEmpty(Request.QueryString["NetObject"]))
			{
				if (string.IsNullOrEmpty(Request.QueryString["DeviceType"]))
				{
					return Resources.CoreWebContent.WEBCODE_TM0_53;
				}
				else
				{
                    return String.Format(Resources.CoreWebContent.WEBCODE_TM0_54, Request.QueryString["DeviceType"]);
				}
			}
			else
			{
                using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
				{
					Node node = proxy.GetNode(Convert.ToInt32(NetObjectHelper.GetObjectID(Request.QueryString["NetObject"])));
					return String.Format(Resources.CoreWebContent.WEBCODE_TM0_55, node.Name);
				}
			}
		}
	}

	protected override void  OnInit(EventArgs e)
	{
		base.OnInit(e);

        SolarWinds.Orion.Web.OrionInclude.CoreThemeStylesheets();
	}

	protected void Page_Load(object sender, EventArgs e)
	{

	}

	protected override void OnLoadComplete(EventArgs e)
	{
		base.OnLoadComplete(e);
		int maxRecords = 0;
		int.TryParse(Request.QueryString["MaxRecords"], out maxRecords);
		bool showAcknowledged = false;
		bool.TryParse(Request.QueryString["ShowAcknowledged"], out showAcknowledged);

		alertsReport.GenerateReport(Request.QueryString["NetObject"], Request.QueryString["DeviceType"], Request.QueryString["AlertID"], maxRecords, showAcknowledged);
	}
}
