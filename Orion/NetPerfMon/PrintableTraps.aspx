<%@ Page Language="C#" Title="<%$ Resources: SyslogTrapsWebContent, WEBCODE_VB0_274 %>" AutoEventWireup="true" CodeFile="PrintableTraps.aspx.cs" Inherits="PrintableTraps" MasterPageFile="~/Orion/OrionMinReqs.master" %>
<%@ Register TagPrefix="orion"  TagName="TrapsReport" Src="~/Orion/Controls/TrapsReportControl.ascx"%>

<asp:Content ContentPlaceHolderID="BodyContent" runat="server">

    <orion:Include runat="server" File="MainLayout.css" />
    <orion:Include runat="server" File="Resources.css" />

    <form id="form1" runat="server" class="sw-app-region">
       <h1>
            <%=Title2 %>
       </h1>
        <div style="margin:10px 10px 10px 10px">
            <orion:TrapsReport runat="server" ID="trapReport" />
        </div>
    </form>

</asp:Content>
