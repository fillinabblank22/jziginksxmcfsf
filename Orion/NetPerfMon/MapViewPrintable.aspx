<%@ Page Language="C#" AutoEventWireup="true" Title="Network Map" MasterPageFile="~/Orion/OrionMinReqs.master" %>
<%@ Register TagPrefix="x" TagName="NetworkMapControl" Src="./Controls/NetworkMapControl.ascx" %>
<%@ Register TagPrefix="x" TagName="ObjectsList" Src="./Controls/MapObjectsListControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="NetObjectTips" Src="~/Orion/Controls/NetObjectTips.ascx" %>

<script runat="server">
    protected override void OnInit(EventArgs e)
    {
        objectsList.MapId = Request["Map"];
        map.MapId = Request["Map"];
        map.TranslateMacros = Macros.TranslateLabelMacros;
        map.ImageAttributes = "border=1 style=border-color:#dfded7;margin-left:10px;margin-bottom:10px";

        OrionInclude.CoreThemeStylesheets();
        base.OnInit(e);
    }
    
    protected override void OnPreRender(EventArgs e)
    {
        string displayName = Request["Title"];
        if(string.IsNullOrEmpty(displayName))
        {
            displayName = Request["Map"];
        }
        displayName = HttpUtility.HtmlEncode(displayName);
        if(string.IsNullOrEmpty(displayName))
        {
            Page.Title = displayName;
        }
        Page.Title = displayName;
        networkMapName.InnerHtml = "<h1>" + displayName + "</h1>";
    }
</script>

<asp:Content ContentPlaceHolderID="BodyContent" runat="server">

    <orion:Include runat="server" File="jquery/jquery.cluetip.js" />
    <orion:Include runat="server" File="Mainlayout.css" Section="Top" />
    <orion:Include runat="server" File="NetObjectTipsAll.css" />
    <orion:Include runat="server" File="NetworkMaps.css" />
    <orion:Include runat="server" File="js/jquery/jquery.cluetip.css" />

    <form id="form1" runat="server" class="sw-app-region">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <div id="networkMapName" runat="server"></div>
        <x:NetworkMapControl ID="map" runat="server" />
    </div>
    <div>
        <x:ObjectsList ID="objectsList" runat="server" />
    </div>
    <div>
        <orion:NetObjectTips ID="netobjecttips" runat="server"/>
    </div>
    </form>

</asp:Content>
