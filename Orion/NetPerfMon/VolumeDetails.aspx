<%@ Page Language="C#" MasterPageFile="~/Orion/View.master" AutoEventWireup="true" CodeFile="VolumeDetails.aspx.cs" Inherits="Orion_NetPerfMon_VolumeDetails" Title="Untitled Page" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="orion" TagName="StatusIconControl" Src="~/Orion/NetPerfMon/Controls/StatusIconControl.ascx" %>
<%@ Register TagPrefix="npm" Assembly="NetPerfMonWeb" Namespace="SolarWinds.Orion.NPM.Web.UI" %>
<%@ Register TagPrefix="npm" TagName="NodeLink" Src="~/Orion/NetPerfMon/Controls/NodeLink.ascx" %>
<%@ Register TagPrefix="orion" TagName="Refresher" Src="~/Orion/Controls/Refresher.ascx" %>
<%@ Register TagPrefix="npm" TagName="IntegrationIcons" Src="~/Orion/NetPerfMon/Controls/IntegrationIcons.ascx" %>
<%@ Import Namespace="SolarWinds.Orion.NPM.Web" %>

<script runat="server">
    protected void NodeSiteMapPath_OnInit(object sender, EventArgs e)
    {
        if (!CommonWebHelper.IsBreadcrumbsDisabled)
        {
            var renderer = new NetObjectsSiteMapRenderer();
            renderer.SetUpData(new KeyValuePair<string, string>("NetObject", Request["NetObject"] ?? string.Empty));
            VolumeSiteMapPath.SetUpRenderer(renderer);
        }
    }
</script>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ViewPageTitle">
    <%if (!CommonWebHelper.IsBreadcrumbsDisabled)
               { %>
	            <div style="margin-left: 10px; margin-top: 10px;">
                    <orion:DropDownMapPath ID="VolumeSiteMapPath" runat="server"  OnInit="NodeSiteMapPath_OnInit">
                    <RootNodeTemplate>
                        <a href="<%# DefaultSanitizer.SanitizeHtml(Eval("url")) %>" ><u> <%# DefaultSanitizer.SanitizeHtml(Eval("title")) %></u> </a>
                    </RootNodeTemplate>
                    <CurrentNodeTemplate>
				        <a href="<%# DefaultSanitizer.SanitizeHtml(Eval("url")) %>" ><%# DefaultSanitizer.SanitizeHtml(Eval("title")) %> </a>
				    </CurrentNodeTemplate>
                    </orion:DropDownMapPath>
				</div>
				<%} %>

	<h1>
	    <span style="float: right; padding-right: 15px;">
	        <npm:IntegrationIcons ID="IntegrationIcons1" runat="server" NodeID='<%$ HtmlEncodedCode: this.NetObject["NodeID"] %>' />
	    </span>	
        <%= DefaultSanitizer.SanitizeHtml(ViewInfo.IsSubView ? ViewInfo.ViewGroupTitle : ViewInfo.ViewHtmlTitle) %> 
        -  <orion:StatusIconControl ID="statusIconControl" runat="server" />
           <npm:NodeLink runat="server" NodeID='<%$ HtmlEncodedCode: string.Format("N:{0}", this.NetObject["NodeID"]) %>' />
	    - <img src="<%= DefaultSanitizer.SanitizeHtml(((SolarWinds.Orion.NPM.Web.Volume) this.NetObject).IconPath) %>" alt="Status" />
          <asp:PlaceHolder ID="PlaceHolder1" runat="server"><%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.UIHelper.NormalizeSpaces(HttpUtility.HtmlEncode(((Volume)this.NetObject).Name))) %></asp:PlaceHolder>
        <%= DefaultSanitizer.SanitizeHtml(ViewInfo.IsSubView ? ViewInfo.ViewHtmlTitle : string.Empty) %>	     	      
    </h1>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MainContentPlaceHolder">
	<npm:VolumeResourceHost ID="volHost" runat="server">
		<orion:ResourceContainer runat="server" ID="resContainer" />
	</npm:VolumeResourceHost>
</asp:Content>
