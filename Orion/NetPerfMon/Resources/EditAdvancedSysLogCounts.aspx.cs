﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.NPM.Common;
using SolarWinds.NPM.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_NetPerfMon_Resources_EditAdvancedSyslogCounts : System.Web.UI.Page
{
	public ResourceInfo Resource
	{
		get
		{
			try
			{
				int resourceID = 0;
				Int32.TryParse(Request.QueryString["ResourceID"], out resourceID);

				ResourceInfo info = ResourceManager.GetResourceByID(resourceID);
				return info;
			}
			catch { }
			return null;
		}
	}
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
			tbTitle.Text = Resource.Title;
			ddlPeriod.SelectedValue = Resource.Properties["timee"];
        }
    }


	protected void imbtnSubmit_Click(object sender, EventArgs e)
	{
		if (!Page.IsValid) return;
		ResourceInfo resource = Resource;
        resource.Properties.Clear();
        if (!String.IsNullOrEmpty(tbTitle.Text))
        {
            resource.Title = tbTitle.Text;
            resource.Properties.Add("title", tbTitle.Text);
        }
        else
        {
            var resourceControl = (BaseResourceControl)LoadControl(Resource.File);
            resource.Title = resourceControl.Title;
            resource.Properties.Add("title", resourceControl.Title);
        }
		resource.Properties.Add("timee", ddlPeriod.SelectedValue);

		ResourcesDAL.Update(resource);
		// redirect backward
        Response.Redirect(BaseResourceControl.GetEditPageReturnUrl(Request.QueryString));
	}
}
