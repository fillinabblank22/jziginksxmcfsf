﻿using System;
using System.Data;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using System.Collections.Generic;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_NetPerfMon_Resources_NodeDetails_LinuxLoadAverage_Xui : BaseResourceControl, IXuiResource, IResourceIsInternal
{
    private static string swqlCheckPollerQuery = @"SELECT NetObjectID FROM Orion.Pollers WHERE PollerType LIKE 'N.LoadAverage%' AND NetObject = 'N:{0}'";
    protected override string DefaultTitle => "Linux Load Average";

    protected void Page_Load(object sender, EventArgs e)
    {
        ResourceWrapper.Visible = OrionMinReqsMaster.IsApolloEnabled && IsLinuxCpuPollerAssigned();
        Selector = GetStringValue("selector", null);
    }

    protected string Selector { get; set; }
    public bool IsInternal => true;

    public override string HelpLinkFragment
    {
        get { return GetStringValue("HelpLinkFragment", ""); }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(INodeProvider) }; }
    }

    private bool IsLinuxCpuPollerAssigned()
    {
        DataTable table;
        var nodeId = GetNodeId();
        if (!nodeId.HasValue)
        {
            return false;
        }

        using (var swis = InformationServiceProxy.CreateV3())
        {
            table = swis.Query(string.Format(swqlCheckPollerQuery, nodeId.Value));
        }

        if (table == null || table.Rows.Count == 0)
        {
            return false;
        }

        return true;
    }

    private int? GetNodeId()
    {
        INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();
        return nodeProvider?.Node?.NodeID;
    }
}
