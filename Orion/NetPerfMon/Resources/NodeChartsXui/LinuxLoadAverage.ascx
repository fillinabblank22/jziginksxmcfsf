<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LinuxLoadAverage.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeDetails_LinuxLoadAverage_Xui" %>
<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true" CssClass="XuiResourceWrapper">
    <Content>
        <div class="xui">
            <<%= DefaultSanitizer.SanitizeHtml(Selector) %>></<%= DefaultSanitizer.SanitizeHtml(Selector) %>>
        </div>
    </Content>
</orion:ResourceWrapper>