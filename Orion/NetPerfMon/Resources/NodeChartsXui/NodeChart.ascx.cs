using System;
using System.Collections.Generic;

using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Resources_NodeChartsXui_NodeChart : BaseResourceControl, IXuiResource, IResourceIsInternal
{
    protected override string DefaultTitle =>
        GetStringValue(ResourceManager.DefaultTitlePropertyKey, Resources.CoreWebContent.NodeChartsV2_Title_4);

    protected string Selector { get; set; }

    public bool IsInternal => true;

    protected void Page_Load(object sender, EventArgs e)
    {
        Selector = GetStringValue("selector", null);
    }

    public override string GetNetObjectId()
    {
        return GetInterfaceInstance<INodeProvider>()?.Node?.NetObjectID;
    }
}