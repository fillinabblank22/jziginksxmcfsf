<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true" CodeFile="EditAdvancedSysLogSummary.aspx.cs" Inherits="Orion_NetPerfMon_Resources_EditAdvancedSyslogSummary" Title="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_245 %>"%>
<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/PeriodControl.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBCODE_AK0_88, CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Resource.Title, Request["netobject"])))) %></h1>

    <table border="0" style="width:100%;" cellpadding="0" cellspacing="0">
        <tr><td>
        <p>
		    <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_230) %></b><br/>
		    <asp:TextBox runat="server" ID="tbTitle" Text="<%# DefaultSanitizer.SanitizeHtml(Resource.Title) %>" Rows="1" Columns="30" />
        </p>
        <p>
			<b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_243) %></b><br/>
            <asp:DropDownList ID="ddlPeriod" runat="server">
                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_233 %>" Value="Past Minute"/>
                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_234 %>" Value="Past Hour" Selected="True"/>
                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_235 %>" Value="Last 24 Hours"/>
                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_236 %>" Value="Last 7 Days"/>
                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_237 %>" Value="Last 30 Days"/>
            </asp:DropDownList>
        </p>
        <p>
			<b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_244) %></b><br/>
			<asp:DropDownList ID="ddlOrderBy" runat="server">
			<asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_246 %>" Value="syslogseverity asc" Selected="True"></asp:ListItem>
			<asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_VB0_39 %>" Value="total desc"></asp:ListItem>
			</asp:DropDownList>
        </p>
		</td></tr>
        <tr><td>
            <div class="sw-btn-bar"><orion:LocalizableButton ID="ImageButton1" runat="server" DisplayType="Primary" LocalizedText="Submit" OnClick="imbtnSubmit_Click"/></div>
        </td></tr>
    </table>
</asp:Content>