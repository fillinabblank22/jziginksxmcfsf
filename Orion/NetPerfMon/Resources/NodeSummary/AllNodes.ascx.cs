﻿using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Logging;
using System;
using System.Data;
using System.Data.SqlClient;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_NodeSummary_AllNodes : BaseResourceControl
{
    private static readonly Log log = new Log();
    protected void Page_Load(object sender, EventArgs e)
	{
		DataTable table;
	    string filter = this.Resource.Properties["Filter"];

        try
		{
			table = SqlDAL.GetAllNodes(filter);
		}
		catch (SqlException ex) when (ex.Class >= 11 && ex.Class <= 16)
		{
		    //Severity levels from 11 through 16 are generated by the user, and can be corrected by the user.
		    log.Error($"Possible invalid filter: {filter}", ex);

		    this.SQLErrorPanel.Visible = true;
		    return;
		}
		catch (Exception ex)
		{
		    log.Error($"Error occured during load resource", ex);
		    throw;
		}

        this.allNodesTable.DataSource = table;
		this.allNodesTable.DataBind();
	}

	protected override string DefaultTitle
	{
		get { return Resources.CoreWebContent.WEBCODE_AK0_41; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionCorePHResourceAllNodes"; }
	}

	public override string EditControlLocation
	{
		get { return "/Orion/NetPerfMon/Controls/EditResourceControls/FilterEdit.ascx"; }
	}

	// overriden Edit URL
	public override String EditURL
	{
		get
		{
            return base.EditURL + "&HideInterfaceFilter=True";
		}
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
