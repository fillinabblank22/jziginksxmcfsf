<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllNodes.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeSummary_AllNodes" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>

<orion:resourceWrapper runat="server" ID="wrapper">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= DefaultSanitizer.SanitizeHtml(this.CustomSqlErrorMessage) %>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="allNodesTable">
            <HeaderTemplate>
                <table border="0" cellpadding="2" cellspacing="0" width="100%">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
		            <td class="Property" valign="middle" width="20">
		                <img alt="<%# DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_14) %>" src="<%# DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.UI.StatusIcons.NodeIconFactory.SmallIconURL( Eval("NodeID"), Eval("Status") )) %>" />&nbsp;
		            </td>
		            <td class="Property">
		                <orion:ToolsetLink runat="server" IPAddress='<%# DefaultSanitizer.SanitizeHtml(Eval("IP_Address")) %>' DNS='<%# DefaultSanitizer.SanitizeHtml(Eval("DNS")) %>' SysName='<%# DefaultSanitizer.SanitizeHtml(Eval("SysName")) %>' CommunityGUID='<%# DefaultSanitizer.SanitizeHtml(Eval("GUID")) %>' NodeID='<%# DefaultSanitizer.SanitizeHtml(Eval("NodeID")) %>' ToolTip='<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_VB0_14 %>'>
		                    <%# DefaultSanitizer.SanitizeHtml(Eval("Caption")) %>
		                </orion:ToolsetLink>
		            </td>
		        </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>