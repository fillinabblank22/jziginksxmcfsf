<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeTree.ascx.cs" Inherits="Orion_NetPerfMon_NodeTree" %>

<asp:ScriptManagerProxy id="NodeTreeScriptManager" runat="server">
	<Services>
		<asp:ServiceReference path="../NodeTree.asmx" />
	</Services>
</asp:ScriptManagerProxy>

<orion:Include runat="server" File="NetPerfMon/Resources/NodeSummary/AjaxNodeTree.js" />

<style type="text/css">
.CORE_GettingStartedBox
{
    padding: 1em;
    border: solid 15px #204a63;
}

.CORE_GettingStartedBox div
{
    clear: both;
}

.CORE_GettingStartedBox div.CORE_First
{
    padding-top: 0em;
}

.CORE_GettingStartedBox .CORE_Content {
    margin: 15px 0;
    padding: 15px;
    background: #F9F9F9;
}

.CORE_GettingStartedBox div h3
{
    font-weight: normal;
    font-size: 11pt;
    margin: 0 0 0 40px;
}

.CORE_GettingStartedBox div p
{
    margin: 0px 0px 0px 25px;
    line-height: 1.8em;
}

.CORE_GettingStartedBox div img
{
    float: left;
    margin-top: 2px;
}

.CORE_Buttons
{
    text-align: right;
}
</style>

<orion:resourceWrapper runat="server" ID="Wrapper" >
	<Content>
	    <script type="text/javascript">
	        //<![CDATA[
	        $(function () {
	            var refresh = function () {
	                ORION.NodeTree.LoadRoot($('#<%=this.nodetree.ClientID%>'), '<%=this.Resource.ID %>');
	            };
	            SW.Core.View.AddOnRefresh(refresh, '<%= nodetree.ClientID %>');
	            refresh();
	        });
	        //]]>
	    </script>
	
		<div class="Tree" ID="nodetree" runat="server">
			<asp:Literal runat="server" ID="TreeLiteral" />
		</div>
        
        <input type="hidden" id="filterValue-<%= this.Resource.ID %>" value="<%= DefaultSanitizer.SanitizeHtml(Filter) %>" />

         <!-- Following show me when no nodes exists in DB -->
        <div id="ORIONCoreTreeNode-<%= this.Resource.ID %>-NoDataContent" style="display: none">
           <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_5) %>

           <div class="CORE_GettingStartedBox">
              <div class="CORE_First ui-helper-clearfix">
                 <img src="/Orion/images/getting_started_36x36.gif" alt="Getting started" />
                 <h3><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_1) %></b> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_2) %></h3>
              </div>
               
               <div class="CORE_Content">
                  <div>
                     <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_3) %>
                  </div>
                   <br/>
                  <div>
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_4) %>
                  </div>
               </div>

              <div class="CORE_Buttons">
                  <orion:LocalizableButton runat="server" ID="discoverNetworkButton" PostBackUrl="~/Orion/Discovery/Default.aspx" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PS0_0 %>" />
              </div>
           </div>

        </div>
	</Content>
</orion:resourceWrapper>
