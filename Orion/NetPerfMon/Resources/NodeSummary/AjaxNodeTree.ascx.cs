using System;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_NetPerfMon_AjaxNodeTree : BaseResourceControl
{
	public override string EditControlLocation
	{
		get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditNodeTree.ascx"; }
	}

	public override string EditURL
	{
		get
		{
            return base.EditURL + "&HideInterfaceFilter=True";
		}
	}

	protected override void OnInit(EventArgs e)
	{
        if (new FeatureManager().GetMaxElementCount(WellKnownElementTypes.Nodes) > 0)
        {
            Wrapper.ManageButtonText = Resources.CoreWebContent.ResourcesAll_ManageNodes;
            Wrapper.ManageButtonTarget = "~/Orion/Nodes/Default.aspx";
            Wrapper.ShowManageButton = SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer ||
                                       Profile.AllowNodeManagement;

            string body = new NodeTree().GetTreeSection(Resource.ID, string.Format("NodeTree_r{0}", Resource.ID),
                                                        new object[] {}, 0);
            TreeLiteral.Text = body;
        }
        else
        {
            Wrapper.Visible = false;
        }

	    base.OnInit(e);
	}

	protected override string DefaultTitle
	{
		get { return Resources.CoreWebContent.WEBCODE_AK0_40; }
	}

	public override string HelpLinkFragment
	{
		get
		{
			return "OrionCorePHResourceAllNodes";
		}
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Synchronous; }
    }
}
