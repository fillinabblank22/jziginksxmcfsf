<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DownNodes.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeSummary_DownNodes" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>

<orion:resourceWrapper runat="server" ID="wrapper">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= DefaultSanitizer.SanitizeHtml(this.CustomSqlErrorMessage) %>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="downNodesTable">
            <HeaderTemplate>
                <table border="0" cellpadding="2" cellspacing="0" width="100%">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
		            <td class="Property" valign="middle" width="20">
		                <img alt="<%# DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_14) %>" src="<%# DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.UI.StatusIcons.NodeIconFactory.SmallIconURL( Eval("NodeID"), null, Eval("Status"), Eval("ChildStatus") )) %>" />&nbsp;
		            </td>
		            <td class="Property">
		                <orion:ToolsetLink ID="ToolsetLink1" runat="server" IPAddress='<%# DefaultSanitizer.SanitizeHtml(Eval("IP_Address")) %>' DNS='<%# DefaultSanitizer.SanitizeHtml(Eval("DNS")) %>' SysName='<%# DefaultSanitizer.SanitizeHtml(Eval("SysName")) %>' CommunityGUID='<%# DefaultSanitizer.SanitizeHtml(Eval("GUID")) %>' NodeID='<%# DefaultSanitizer.SanitizeHtml(Eval("NodeID")) %>'>
		                    <%# DefaultSanitizer.SanitizeHtml(Eval("Caption")) %>
		                </orion:ToolsetLink>
		            </td>
		        </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>