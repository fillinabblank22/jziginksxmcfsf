﻿if (typeof (ORION) == "undefined") {
    ORION = {};
}

var NodeTreeResourceDictionary = { };
ORION.NodeTree = {
    ResourceId: 0,

    Succeeded: function (result, context) {
        var contentsDiv = $(context);
        $(contentsDiv).get(0).innerHTML = result;
        contentsDiv.slideDown('fast');

        if (GetAutoLoadType(contentsDiv) == "ShowAll") {
            ORION.NodeTree.AutoClickLinks(contentsDiv);
        } else {
            // expand any nodes that are marked to be expanded	    
            $("[expand]", contentsDiv).removeAttr("expand").click();
        }

        var resId = NodeTreeResourceDictionary[contentsDiv.attr('id')];
        if (result == "" && resId != undefined) {
            var hv = $('#filterValue-' + resId);
            if (hv.length == 0 || hv.val() == "") {
                $("#ORIONCoreTreeNode-" + resId + "-NoDataContent").show();
            }
        }
    },

    Failed: function (error, context) {
        var contentsDiv = context;
        contentsDiv.innerHTML = String.format("@{R=Core.Strings;K=WEBJS_AK0_1;E=js}", error.get_message());
    },

    AutoClickLinks: function (contentsDiv) {
        // auto-click the "get 100 more Nodes" links
        $("[id*='-startFrom-'] a", contentsDiv).click();

        // expand any nodes that are marked to be expanded	    
        $("[expand]", contentsDiv).removeAttr("expand").click();
    },

    Click: function (rootId, resourceId, keys) {
        return ORION.NodeTree.HandleClick(rootId, function (contentDiv) {
            NodeTree.GetTreeSection(resourceId, rootId, keys, 0, ORION.NodeTree.Succeeded, ORION.NodeTree.Failed, contentDiv);
        }, function () {
            NodeTree.CollapseTreeSection(resourceId, keys);
        });
    },

    LoadRoot: function (treeDiv, resourceId) {
        ResourceId = resourceId;
        $("#ORIONCoreTreeNode-" + resourceId + "-NoDataContent").hide();
        treeDiv.html(String.format("<div>{0}</div>", "@{R=Core.Strings;K=WEBJS_AK0_2;E=js}"));
        var id = treeDiv.attr('id');
        if (!NodeTreeResourceDictionary[id])
            NodeTreeResourceDictionary[id] = resourceId;
        NodeTree.GetTreeSection(resourceId, "NT_r" + resourceId, [], 0, ORION.NodeTree.Succeeded, ORION.NodeTree.Failed, treeDiv.get(0));
    },

    HandleClick: function (rootId, expandCallback, collapseCallback) {
        var toggleImg = $('#' + rootId + '-toggle');
        var contentsDiv = $('#' + rootId + '-contents');

        if (!contentsDiv.is(':hidden')) {
            contentsDiv.slideUp('fast');
            toggleImg.attr("src", "/Orion/images/Button.Expand.gif");
            collapseCallback();
        } else {
            contentsDiv.html("@{R=Core.Strings;K=WEBJS_AK0_2;E=js}");
            contentsDiv.slideDown('fast');
            toggleImg.attr('src', "/Orion/images/Button.Collapse.gif");
            expandCallback(contentsDiv.get(0));
        }

        return false;
    },

    GetMore: function (linkId, resourceId, keys, startFrom) {
        var contentsDiv = $get(linkId);
        contentsDiv.innerHTML = "@{R=Core.Strings;K=WEBJS_AK0_2;E=js}";
        NodeTree.GetTreeSection(resourceId, linkId, keys, startFrom, ORION.NodeTree.Succeeded, ORION.NodeTree.Failed, contentsDiv);
        return false;
    }
};

function NodeTreeSectionReceived(result, context)
{
    var contentsDiv = $(context);
    contentsDiv.html(result);
    contentsDiv.slideDown('fast');
    AutoClickGetMoreLinks(contentsDiv);
}

function AutoClickGetMoreLinks(contentsDiv)
{
	// auto-click the "get 100 more nodes" links
	$("[id*='-startFrom-'] a", contentsDiv).click();
}

function NodeTreeSectionFailed(error, context)
{
	var contentsDiv = context;
	contentsDiv.innerHTML = String.format("@{R=Core.Strings;K=WEBJS_AK0_1;E=js}", error.get_message());
}

function FastNodeTree_Click(rootId, resourceId, keys)
{
	var toggleImg = $get(rootId + '-toggle');
	var contentsDiv = $get(rootId + '-contents');
	
	if (contentsDiv.style.display == "") {
		contentsDiv.style.display = "none";
		toggleImg.src = "/Orion/images/Button.Expand.gif";
	} else {
		contentsDiv.innerHTML = "@{R=Core.Strings;K=WEBJS_AK0_2;E=js}";
		contentsDiv.style.display = "";
		toggleImg.src = "/Orion/images/Button.Collapse.gif";
		NodeTree.GetTreeSection(resourceId, rootId, keys, 0, NodeTreeSectionReceived, NodeTreeSectionFailed, contentsDiv);
	}
	
	return false;
}

function FastNodeTree_GetMore_Click(linkId, resourceId, keys, startFrom)
{
	var contentsDiv = $get(linkId);
	contentsDiv.innerHTML = "@{R=Core.Strings;K=WEBJS_AK0_2;E=js}";
	NodeTree.GetTreeSection(resourceId, linkId, keys, startFrom, NodeTreeSectionReceived, NodeTreeSectionFailed, contentsDiv);
	
	return false;
}

function GetAutoLoadType(contentsDiv) {

    var properties = $.parseJSON(contentsDiv.closest("div.ResourceWrapper").attr("sw-widget-properties"));
    var loadType = properties["loadtype"];

    if (loadType)
        return loadType;
    else
        return "ShowAll";
}