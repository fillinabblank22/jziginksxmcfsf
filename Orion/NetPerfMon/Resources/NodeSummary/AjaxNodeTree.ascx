<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AjaxNodeTree.ascx.cs" Inherits="Orion_NetPerfMon_AjaxNodeTree" %>

<asp:ScriptManagerProxy id="NodeTreeScriptManager" runat="server">
	<Services>
		<asp:ServiceReference path="../NodeTree.asmx" />
	</Services>
</asp:ScriptManagerProxy>

<orion:Include runat="server" File="NetPerfMon/Resources/NodeSummary/AjaxNodeTree.js" />

<orion:resourceWrapper runat="server" ID="Wrapper" >
	<Content>
		<div class="Tree">
			<asp:Literal runat="server" ID="TreeLiteral" />
		</div>
	</Content>
</orion:resourceWrapper>
