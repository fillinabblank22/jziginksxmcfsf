<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeList.ascx.cs" Inherits="Orion_NetPerfMon_NodeList" %>
<%@ Register TagPrefix="orion" TagName="SmallNodeStatus" Src="~/Orion/Controls/SmallNodeStatus.ascx" %>
<%@ Register TagPrefix="npm" TagName="PercentLoss" Src="~/Orion/NetPerfMon/Controls/PercentLoss.ascx" %>
<%@ Register TagPrefix="npm" TagName="ResponseTime" Src="~/Orion/NetPerfMon/Controls/ResponseTime.ascx" %>
<%@ Register TagPrefix="npm" TagName="NodeLink" Src="~/Orion/NetPerfMon/Controls/NodeLink.ascx" %>
<%@ Import Namespace="SolarWinds.Orion.NPM.Web" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= DefaultSanitizer.SanitizeHtml(this.CustomSqlErrorMessage) %>
                    </td>
                </tr>
            </table>
        </asp:Panel>
	    <asp:Repeater ID="NodeTable" runat="server" OnInit="NodeTable_Init">
		    <HeaderTemplate>
			    <table border="0" cellspacing="0" cellpadding="3px">
				    <thead>
				        <tr>
					        <td></td>
					        <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_171) %></td>
					        <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_172) %></td>
					        <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_173) %></td>
					        <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_174) %></td>
				        </tr>
				    </thead>
		    </HeaderTemplate>

		    <ItemTemplate>
			    <tr>
				    <td>
				        <npm:NodeLink runat="server" Node='<%# DefaultSanitizer.SanitizeHtml((Node)Container.DataItem) %>'>
				            <Content>
				                <orion:SmallNodeStatus runat="server" StatusValue='<%# DefaultSanitizer.SanitizeHtml(Eval("Status")) %>' /> 
				            </Content>
				        </npm:NodeLink>
				    </td>
				    <td>
				        <npm:NodeLink runat="server" Node='<%# DefaultSanitizer.SanitizeHtml((Node)Container.DataItem) %>'>
				            <Content>
				                <%# DefaultSanitizer.SanitizeHtml(Eval("Name")) %>
				            </Content>
				        </npm:NodeLink>
				    </td>
				    <td>
				        <npm:NodeLink runat="server" Node='<%# DefaultSanitizer.SanitizeHtml((Node)Container.DataItem) %>'>
				            <Content>
				                <%# DefaultSanitizer.SanitizeHtml(GetRevisedStatusDescription( (int) Eval("NodeID") )) %>
				            </Content>
				        </npm:NodeLink>
				    </td>
				    <td><npm:ResponseTime runat="server" Value='<%# Convert.ToInt32(Eval("ResponseTime")) %>' /></td>
				    <td><npm:PercentLoss runat="server" Value='<%# Convert.ToInt16(Eval("PercentLoss")) %>' /></td>
			    </tr>
		    </ItemTemplate>

		    <FooterTemplate>
			    </table>
		    </FooterTemplate>
	    </asp:Repeater>
    </Content>
</orion:resourceWrapper>
