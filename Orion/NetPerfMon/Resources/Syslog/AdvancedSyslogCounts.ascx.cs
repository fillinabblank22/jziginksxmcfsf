using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SyslogTraps.Web.UI;
using System;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Syslog)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_AdvancedSyslogCounts : SyslogResourceControl
{
    public Orion_NetPerfMon_Resources_AdvancedSyslogCounts() : base("Syslog")
    {
    }
    #region properties

    // overriden main title of resources
    public override string DisplayTitle
    {
        get
        {
            string result = Resource.Title;

            if (String.IsNullOrEmpty(result))
                result = DefaultTitle;

            return result;
        }
    }

    // overriden subtitle of resource
    public override string SubTitle
    {
        get
        {
            return Periods.GetLocalizedPeriod(Period);
        }
    }

    // overriden dafault title
    protected override string DefaultTitle
    {
        get { return Resources.SyslogTrapsWebContent.WEBCODE_VB0_158; }
    }

    // overriden help fragment
    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceSyslogSummary";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditAdvancedSysLogCounts.ascx"; }
    }
    #endregion

    public string Timee
    {
        get
        {
            String s = Resource.Properties["timee"];
            if (string.IsNullOrEmpty(s))
                s = "hour";
            return s;
        }
    }

    public string Period
    {
        get
        {
            String s = Timee;
            switch (s.ToLower())
            {
                case "minute": return "Past Minute";
                case "hour": return "Past Hour";
                case "day": return "Last 24 Hours";
                case "week": return "Last 7 Days";
                case "month": return "Last 30 Days";
                default: return "Past Hour";
            }
        }
    }
}
