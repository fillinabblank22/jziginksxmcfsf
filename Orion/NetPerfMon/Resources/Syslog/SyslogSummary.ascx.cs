using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SyslogTraps.Web.DAL;
using SolarWinds.SyslogTraps.Web.UI;
using System;
using System.Data;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Syslog)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_Syslog_SyslogSummary : SyslogResourceControl
{
    private const string HostNameString = "HostName";

    public Orion_NetPerfMon_Resources_Syslog_SyslogSummary() : base("Syslog")
    {
    }

    #region properties

    // overriden main title of resources
    public override string DisplayTitle
    {
        get
        {
            string result = Resource.Title;

            if (String.IsNullOrEmpty(result))
                result = DefaultTitle;

            return result;
        }
    }

    // overriden subtitle of resource
    public override string SubTitle
    {
        get
        {
            // subtitle is a period
            string period = Periods.GetLocalizedPeriod(Resource.Properties["Period"]);

            if (String.IsNullOrEmpty(period))
                return Periods.GetLocalizedPeriod("Today");
            else
                return period;
        }
    }

    // this resource uses Period property for date time filter
    //public override bool SupportDateTimeFilter
    //{
    //    get { return true; }
    //}

    // overriden dafault title
    protected override string DefaultTitle
    {
        get { return Resources.SyslogTrapsWebContent.WEBCODE_VB0_163; }
    }

    // overriden help fragment
    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceSyslogSummary";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditSysLogSummary.ascx"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    public string GroupBy
    {
        get
        {
            var temp = Resource.Properties["GroupByLocalized"];

            if (String.IsNullOrEmpty(temp))
                return String.Empty;

            int index = 0;

            for (int i = temp.Length - 1; i > 0; i--)
                if (temp[i] == temp[i].ToString().ToUpper()[0])
                {
                    index = i;
                    break;
                }

            return temp.Insert(index, " ");
        }
    }

    #endregion

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var groupBy = Resource.Properties["GroupBy"];

            if (String.IsNullOrEmpty(groupBy))
                groupBy = HostNameString;

            var periodBegin = new DateTime();
            var periodEnd = new DateTime();
            var period = this.Resource.Properties["Period"];

            int nodeID = -1;
            // if is netObject a Node set filter for a node
            INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();
            if (nodeProvider != null)
                nodeID = nodeProvider.Node.NodeID;
            else
                nodeID = CommonWebHelper.TryToGetNodeIDFromRequest(Request.QueryString["NetObject"]);

            if (String.IsNullOrEmpty(period))
                period = "Today";

            var temp = period;

            Periods.Parse(ref temp, ref periodBegin, ref periodEnd);

            DataTable table = null;

            using (WebDAL webdal = new WebDAL())
            {
                table = webdal.GetSysLogMessagesSummary(nodeID, groupBy, periodBegin, periodEnd);
            }

            if (table != null && table.Rows.Count != 0)
            {
                table.Columns.Add("URL", typeof(string));

                foreach (DataRow r in table.Rows)
                {
                    if (groupBy.Equals(HostNameString, StringComparison.InvariantCultureIgnoreCase))
                    {
                        int objectId = 0;
                        var objectIdParam = (int.TryParse(r["NodeID"].ToString(), out objectId) && objectId > 0) ?
                            string.Format("NetObject=N:{0}", objectId) : string.Format("IPAddress={0}", r["IPAddress"].ToString());

                        r["URL"] = String.Format("/Orion/NetPerfMon/Syslog.aspx?{0}&Period={1}", objectIdParam, period);
                    }
                    else
                    {
                        r["URL"] = String.Format("/Orion/NetPerfMon/Syslog.aspx?{0}={1}&Period={2}", groupBy.Trim(), r["Data"].ToString().Trim(), period);
                    }
                }

                this.MessageRepeater.DataSource = table;
                this.MessageRepeater.DataBind();
            }
            else
                InfoPanel.Visible = true;
        }
    }
}
