<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SyslogSummary.ascx.cs"
    Inherits="Orion_NetPerfMon_Resources_Syslog_SyslogSummary" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
<orion:resourceWrapper ID="ResourceWrapper1" runat="server" ShowEditButton="true">
    <Content>
        <asp:Repeater runat="server" ID="MessageRepeater">
            <HeaderTemplate>
                <table style="width: 100%" cellspacing="0" cellpadding="0">
                    <tr class="ReportHeader">
                        <td style="padding: 5px;">
                            <%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_190 %>
                            <%=GroupBy%>
                        </td>
                        <td>
                            <%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_39 %>
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td style="padding: 5px;">
                        <a href='<%#Eval("URL")%>'>
                            <%#Eval("Data")%>
                        </a>
                    </td>
                    <td style="padding: 5px; text-align: left;">
                        <a href='<%#Eval("URL")%>'>
                            <%#Eval("Total")%>
                        </a>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <asp:Panel ID="InfoPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Green">
                        <%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_146 %>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </Content>
</orion:resourceWrapper>
