using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SyslogTraps.Web.UI;
using System;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Syslog)]
public partial class Orion_NetPerfMon_Resources_AdvancedSyslogParser : SyslogResourceControl
{
    public Orion_NetPerfMon_Resources_AdvancedSyslogParser() : base("Syslog")
    {
    }

    #region properties

    // overriden main title of resources

    public override string DisplayTitle
    {
        get
        {
            string result = Resource.Title;

            if (String.IsNullOrEmpty(result))
                result = DefaultTitle;

            return result;
        }
    }

    // overriden subtitle of resource
    public override string SubTitle
    {
        get
        {
            return Periods.GetLocalizedPeriod(Period);
        }
    }

    // overriden dafault title
    protected override string DefaultTitle
    {
        get { return Resources.SyslogTrapsWebContent.WEBCODE_VB0_159; }
    }

    // overriden help fragment
    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceSyslogSummary";
        }
    }

    // overriden EditURL
    public override string EditURL
    {
        get
        {
            string url = String.Format("/Orion/NetPerfMon/Resources/EditAdvancedSysLogParser.aspx?ResourceID={0}&ViewID={1}", this.Resource.ID, this.Resource.View.ViewID);
            if (!String.IsNullOrEmpty(Request.QueryString["NetObject"]))
                url = String.Format("{0}&NetObject={1}", url, Request.QueryString["NetObject"]);
            return url;
        }
    }

    #endregion

    public string Timee
    {
        get
        {
            String s = Resource.Properties["timee"];
            if (string.IsNullOrEmpty(s))
                s = "hour";
            return s;
        }
    }


    public string Period
    {
        get
        {
            String s = Timee;
            switch (s.ToLower())
            {
                case "minute": return "Past Minute";
                case "hour": return "Past Hour";
                case "day": return "Last 24 Hours";
                case "week": return "Last 7 Days";
                case "month": return "Last 30 Days";
                default: return "Past Hour";
            }
        }
    }
}
