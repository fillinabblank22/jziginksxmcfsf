using SolarWinds.Orion.Common;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SyslogTraps.Web.DAL;
using SolarWinds.SyslogTraps.Web.UI;
using System;
using System.Data;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Syslog)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_Syslog_LastxxMessages : SyslogResourceControl
{
    // default number is used if there is no record in resource properties 
    const int DefaultNumberOfRecords = 25;

    public Orion_NetPerfMon_Resources_Syslog_LastxxMessages() : base("Syslog")
    {
    }

    #region properties

    // overriden main title of resources
    public override string DisplayTitle
    {
        get
        {
            if (Resource.Title.Equals(DefaultTitle, StringComparison.OrdinalIgnoreCase))
            {
                //return Resource.Title; // this isn't working
                string result = Resource.Properties["MaxMessages"];

                if (String.IsNullOrEmpty(result))
                    result = DefaultNumberOfRecords.ToString();


                return String.Format(Resources.SyslogTrapsWebContent.WEBCODE_VB0_161, result);
            }
            else
                return Resource.Title;
        }
    }

    // overriden subtitle of resource
    public override string SubTitle
    {
        get
        {
            // subtitle is a period
            string period = Resource.Properties["Period"];

            if (String.IsNullOrEmpty(period))
                return Periods.GetLocalizedPeriod("Today");
            else
                return Periods.GetLocalizedPeriod(period);
        }
    }

    // this resource uses Period property for date time filter
    //public override bool SupportDateTimeFilter
    //{
    //    get { return true; }
    //}

    // overriden dafault title
    protected override string DefaultTitle
    {
        get { return Resources.SyslogTrapsWebContent.WEBCODE_VB0_162; }
    }

    // overriden help fragment
    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceLastXXSyslogMessages";
        }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/NetPerfMon/Controls/EditResourceControls/EditLastxxMessages.ascx";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int maxRecords;
            try
            {
                maxRecords = Int32.Parse(Resource.Properties["MaxMessages"]);
            }
            catch
            {
                maxRecords = DefaultNumberOfRecords;
            }

            var periodBegin = new DateTime();
            var periodEnd = new DateTime();
            var period = this.Resource.Properties["Period"];
            var filterText = this.Resource.Properties["Filter"];
            int nodeID = -1;

            // if is netObject a Node set filter for a node
            INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();
            if (nodeProvider != null)
                nodeID = nodeProvider.Node.NodeID;
            else
                nodeID = CommonWebHelper.TryToGetNodeIDFromRequest(Request.QueryString["NetObject"]);

            if (String.IsNullOrEmpty(period))
                period = "Today";

            Periods.Parse(ref period, ref periodBegin, ref periodEnd);

            DataTable table = null;

            using (WebDAL webdal = new WebDAL())
            {
                try
                {
                    table = webdal.GetSysLogMessages(nodeID, maxRecords, filterText, periodBegin, periodEnd);
                }
                catch (System.ServiceModel.FaultException ex)
                {
                    if ((ex is System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>) &&
                        ((System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>)(ex)).Detail.Message.Contains(filterText))
                    {
                        this.SQLErrorPanel.Visible = true;
                        return;
                    }
                    throw;
                }
            }

            if (table != null && table.Rows.Count > 0)
            {
                table.Columns.Add("NodeURL", typeof(string));

                foreach (DataRow r in table.Rows)
                {
                    r["NodeURL"] = r["NodeID"] is DBNull ? String.Empty : String.Format("/Orion/View.aspx?NetObject=N:{0}", r["NodeID"]);
                }

                this.MessageRepeater.DataSource = table;
                this.MessageRepeater.DataBind();
            }
            else
                InfoPanel.Visible = true;
        }
    }

    protected void MessageRepeaterOnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HtmlTableCell dateCell = e.Item.FindControl("dateCell") as HtmlTableCell;
            HtmlTableCell messageCell = e.Item.FindControl("messageCell") as HtmlTableCell;

            Literal date = new Literal();
            date.Text = String.Format("{0}&nbsp;{1}", Utils.FormatCurrentCultureDate(((DateTime)((DataRowView)e.Item.DataItem)["DateTime"]).ToLocalTime()),
                                                      Utils.FormatToLocalTime(((DateTime)((DataRowView)e.Item.DataItem)["DateTime"]).ToLocalTime()));

            Literal message = new Literal();
            message.Text = HttpUtility.HtmlEncode((string)((DataRowView)e.Item.DataItem)["Message"]);

            string url = (string)((DataRowView)e.Item.DataItem)["NodeURL"];

            if (String.IsNullOrEmpty(url))
            {
                dateCell.Controls.Add(date);
                messageCell.Controls.Add(message);
            }
            else
            {
                HtmlAnchor anchor = new HtmlAnchor();
                anchor.HRef = url;
                anchor.Controls.Add(date);
                dateCell.Controls.Add(anchor);

                anchor = new HtmlAnchor();
                anchor.HRef = url;
                anchor.Controls.Add(message);
                messageCell.Controls.Add(anchor);
            }
        }
    }
}
