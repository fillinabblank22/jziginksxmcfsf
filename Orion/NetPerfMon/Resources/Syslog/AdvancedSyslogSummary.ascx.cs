using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SyslogTraps.Web.UI;
using System;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Syslog)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_AdvancedSyslogSummary : SyslogResourceControl
{
    public Orion_NetPerfMon_Resources_AdvancedSyslogSummary() : base("Syslog")
    {
    }
    
    #region properties

	// overriden main title of resources

	public override string DisplayTitle
	{
		get
		{
			string title = Resource.Title;

			if (String.IsNullOrEmpty(title))
			{
				title = DefaultTitle;
				Resource.Properties["Title"] = title;
			}

			return title;
		}
	}

	// overriden subtitle of resource
	public override string SubTitle
	{
		get 
		{
			// subtitle is a period
			string subTitle = Resource.Properties["Period"];

			if (String.IsNullOrEmpty(subTitle))
			{
				subTitle = "Past Hour";
				Resource.Properties["Period"] = subTitle;
			}

			return Periods.GetLocalizedPeriod(subTitle);
		}
	}

    // this resource uses Period property for date time filter
	//public override bool SupportDateTimeFilter
	//{
	//    get { return true; }
	//}

	// overriden dafault title
	protected override string DefaultTitle
	{
		get { return Resources.SyslogTrapsWebContent.WEBCODE_VB0_160; }
	}

	// overriden help fragment
	public override string HelpLinkFragment
	{
		get
		{
			return "OrionPHResourceSyslogSummary";
		}
	}

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/NetPerfMon/Controls/EditResourceControls/EditAdvancedSysLogSummary.ascx";
        }
    }
	#endregion
}
