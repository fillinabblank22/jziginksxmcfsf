﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdvancedSyslogCounts.ascx.cs"
    Inherits="Orion_NetPerfMon_Resources_AdvancedSyslogCounts" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
<%@ Register src="../../Controls/AdvancedSyslogCounts.ascx" tagname="AdvancedSyslogCounts" tagprefix="uc1" %>
<orion:resourceWrapper ID="ResourceWrapper1" runat="server" ShowEditButton="true">
    <Content>
        <uc1:AdvancedSyslogCounts ID="AdvancedSyslogCounts1" runat="server" />
    </Content>
</orion:resourceWrapper>

