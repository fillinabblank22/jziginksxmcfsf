﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdvancedSyslogSummary.ascx.cs"
    Inherits="Orion_NetPerfMon_Resources_AdvancedSyslogSummary" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
<%@ Register src="../../Controls/AdvancedSyslogSummary.ascx" tagname="AdvancedSyslogSummary" tagprefix="uc1" %>
<orion:resourceWrapper ID="ResourceWrapper1" runat="server" ShowEditButton="true">
    <Content>
        <uc1:AdvancedSyslogSummary ID="AdvancedSyslogSummary1" runat="server" />
    </Content>
</orion:resourceWrapper>


