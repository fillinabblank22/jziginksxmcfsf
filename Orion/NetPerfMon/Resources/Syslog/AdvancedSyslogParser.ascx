﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdvancedSyslogParser.ascx.cs"
    Inherits="Orion_NetPerfMon_Resources_AdvancedSyslogParser" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
<%@ Register src="../../Controls/AdvancedSyslogParser.ascx" tagname="AdvancedSyslogParser" tagprefix="uc1" %>
<orion:resourceWrapper ID="ResourceWrapper1" runat="server" ShowEditButton="true">
    <Content>
        <uc1:AdvancedSyslogParser ID="AdvancedSyslogParser1" runat="server" />
    </Content>
</orion:resourceWrapper>

