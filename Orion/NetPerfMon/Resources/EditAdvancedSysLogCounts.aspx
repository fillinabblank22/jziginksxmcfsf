<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true" CodeFile="EditAdvancedSysLogCounts.aspx.cs" Inherits="Orion_NetPerfMon_Resources_EditAdvancedSyslogCounts" Title="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_232 %>"%>
<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/PeriodControl.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBCODE_AK0_88, CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Resource.Title, Request["netobject"])))) %></h1>

    <table border="0" style="width:100%;" cellpadding="0" cellspacing="0">
        <tr><td>
        <p>
            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_230) %></b><br/>
		    <asp:TextBox runat="server" ID="tbTitle" Text="<%# DefaultSanitizer.SanitizeHtml(Resource.Title) %>" Rows="1" Columns="30" />
        </p>
		<p>
            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_231) %></b><br/>
            <asp:DropDownList ID="ddlPeriod" runat="server">
                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_233 %>" Value="minute"/>
                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_234 %>" Value="hour" Selected="True"/>
                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_235 %>" Value="day"/>
                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_236 %>" Value="week"/>
                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_237 %>" Value="month"/>
            </asp:DropDownList>
        </p>
		</td></tr>
        <tr><td>
            <div class="sw-btn-bar"><orion:LocalizableButton ID="imbtnSubmit" runat="server" DisplayType="Primary" LocalizedText="Submit" OnClick="imbtnSubmit_Click"/></div>
        </td></tr>
    </table>
</asp:Content>