﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Extensions;
using AccountLimitationException = SolarWinds.Orion.Web.AccountLimitationException;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

public partial class Orion_NetPerfMon_Resources_EditCustomObjectResource : System.Web.UI.Page
{
    private Control _editControl;
    private static readonly Log _log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
    private static Dictionary<string, object> staticProps = new Dictionary<string, object>();
    private readonly string WirelessAutonomousAccessPoints = Resources.CoreWebContent.WEBCODE_VB1_10;
    private readonly string WirelessControllers = Resources.CoreWebContent.WEBCODE_VB1_11;
    private readonly string EnergyWiseControls = Resources.CoreWebContent.WEBCODE_VB1_12;
    private readonly string EnergyWiseCharts = Resources.CoreWebContent.WEBCODE_VB1_13;
    private readonly string VMWareESX = Resources.CoreWebContent.WEBCODE_VB1_14;

    private static string SelectObjectsURI = "/Orion/NetPerfMon/Controls/EditResourceControls/AddRemoveObjects/AddRemoveObjects.aspx{0}";
    private string SessionGuid;
    private const string SessionID = "SessionID";
    private const string DefaultEntityName = "Orion.Nodes";
    private const string Is_custom_title = "Is_custom_title";
    private StringDictionary _netObjectProperties = null;
    private OrionChartingHelper.CustomObjectMode _netObjectMode = OrionChartingHelper.CustomObjectMode.SingleObjectMode;
    private bool _forceTitleChange = false;

    private string netObjectID;
    private ResourceInfo resource;

    //todo: this is temporary solution (to minimize risks) for incompatible edit controls.
    private List<string> editControlsToSkip = new List<string>
    {
        "/orion/netperfmon/controls/editresourcecontrols/editappstack.ascx",
        "/orion/dpa/controls/editresourcecontrols/databaseresponsetime.ascx"
    };

    private string SafeTitle => HttpUtility.HtmlEncode(WebSecurityHelper.SanitizeAngular(txtTitle.Text));
    private string SafeSubTitle => HttpUtility.HtmlEncode(WebSecurityHelper.SanitizeAngular(txtSubTitle.Text));

    protected ResourceInfo Resource
    {
        get { return resource; }
    }

    public static Dictionary<string, object> EmbeddedResourceData
    {
        get
        {
            if (HttpContext.Current.Session["EmbeddedResourceData"] == null)
            {
                return null;
            }
            return (Dictionary<string, object>)HttpContext.Current.Session["EmbeddedResourceData"];
        }
        set
        {
            var data = value;
            HttpContext.Current.Session["EmbeddedResourceData"] = data;
        }
    }

    public string SelectedResource
    {
        get
        {
            string selectedRes = String.Empty;
            if (_netObjectMode == OrionChartingHelper.CustomObjectMode.SingleObjectMode)
            {
                selectedRes = resources.SelectedValue;
            }
            else
            {
                selectedRes = lc_ListCharts.ChartV2Selected ? lc_ListCharts.SelectedChart : null;
            }

            if (!string.IsNullOrEmpty(selectedRes))
                return selectedRes;
            if (ViewState["selected_resource"] != null)
                return (string)ViewState["selected_resource"];

            return string.Empty;
        }
        set { ViewState["selected_resource"] = value; }
    }

	protected bool ClearPrevProperties
	{
		get
		{
            if (_netObjectMode == OrionChartingHelper.CustomObjectMode.SingleObjectMode)
                return !(!String.IsNullOrEmpty(Resource.Properties["EmbeddedObjectResource"]) &&
                    Resource.Properties["EmbeddedObjectResource"].Equals(resources.SelectedValue, StringComparison.OrdinalIgnoreCase));
            else
                return false;
		}
	}

    public Dictionary<string, object> ResourceProperties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            if (!String.IsNullOrEmpty(ListEntities.SelectedEntities) && ListEntities.SelectedEntititesCount <= 1)
            {
                _netObjectMode = OrionChartingHelper.CustomObjectMode.SingleObjectMode;
            }
            else
            {
                _netObjectMode = lc_ListCharts.ChartV2Selected ?
                    OrionChartingHelper.CustomObjectMode.MultiObjectChartsV2 : OrionChartingHelper.CustomObjectMode.MultiObjectMode;
            }

            if (ViewState["resource_properties"] != null)
                properties = (Dictionary<string, object>)ViewState["resource_properties"];
            else if (_editControl != null)
                foreach (KeyValuePair<string, object> property in (_editControl as BaseResourceEditControl).Properties)
                    properties.Add(property.Key, property.Value.ToString());

            InsertOrUpdateProperty(properties, "Title",  SafeTitle);
            InsertOrUpdateProperty(properties, "SubTitle",  SafeSubTitle);

            if (!String.IsNullOrEmpty(ListEntities.SelectedEntities) && !String.IsNullOrEmpty(ListEntities.EntityName))
            {
                InsertOrUpdateProperty(properties, "NetObjectID", NetObjectTypeToSWISEntityMapper.GetNetObjectIDs(ListEntities.SelectedEntities, ListEntities.EntityName).FirstOrDefault());

                if (ListEntities.SelectedEntititesCount == 1)
                {
                    ContainersMetadataDAL containerMetadataDal = new ContainersMetadataDAL();
                    properties["ObjectUri"] = containerMetadataDal.GetObjectUriForEntityId(ListEntities.EntityName, ListEntities.SelectedEntities);

                    if (!String.IsNullOrEmpty(resources.SelectedValue))
                        InsertOrUpdateProperty(properties, "EmbeddedObjectResource", resources.SelectedValue);
                }
            }

            var embeddedData = resources.SelectedValue.Split(';');
            if (embeddedData.Length > 1 && ViewState["resource_properties"] == null)
            {
                for (var i = 1; i < embeddedData.Length; i++)
                {
                    var prop = embeddedData[i].Split('=');
                    if (prop.Length == 2)
                    {
                        InsertOrUpdateProperty(properties, prop[0], prop[1]);
                    }
                }
            }

            if (EmbeddedResourceData != null && EmbeddedResourceData.Count > 0)
            {
                foreach (var pair in EmbeddedResourceData)
                {
                    properties[pair.Key] = pair.Value;
                }
            }

            InsertOrUpdateProperty(properties, "EntityName", ddl_ObjectTypes.SelectedValue);
            InsertOrUpdateProperty(properties, "SelectedEntities", ListEntities.SelectedEntities);
            InsertOrUpdateProperty(properties, "FilterEntities", ListEntities.FilterSelection.ToString());
            InsertOrUpdateProperty(properties, "CustomObjectMode", (int)_netObjectMode);


            if (_netObjectMode != OrionChartingHelper.CustomObjectMode.SingleObjectMode)
            { // storing multiobject chart specific data
                InsertOrUpdateProperty(properties, "Period", Period.PeriodName);
                InsertOrUpdateProperty(properties, "ShowSum", ShowSum.SelectedValue);
                InsertOrUpdateProperty(properties, "TopXX", TopXX.Enabled ? TopXX.Value.ToString() : String.Empty);
                InsertOrUpdateProperty(properties, "ChartName", SelectedChartName());
                InsertOrUpdateProperty(properties, "EmbeddedObjectResource", EmbeddedObjectResource());
                InsertOrUpdateProperty(properties, "ManualSelect", (!AutoSelect.Checked).ToString());
                InsertOrUpdateProperty(properties, "AutoHide", AutoHide.AutoHideValue);
                InsertOrUpdateProperty(properties, "SampleSize", SampleSize.SampleSizeValue);
            }

            return properties;
        }
        set { ViewState["resource_properties"] = value; }
    }

    private void InsertOrUpdateProperty(IDictionary<string, object> dict, string key, object value)
    {
        if (dict == null)
            throw new ArgumentNullException("dict");
        if (String.IsNullOrEmpty(key))
            throw new ArgumentNullException("key");

        if (dict.ContainsKey(key))
            dict[key] = value;
        else
            dict.Add(key, value);
    }

    private string SelectedChartName()
    {
        if (lc_ListCharts.SelectedChart != null)
            return lc_ListCharts.SelectedChart.Split(';')[0];
        else
            return String.Empty;
    }

    private string EmbeddedObjectResource()
    {
        if (lc_ListCharts.SelectedChart != null)
        {
            var items = lc_ListCharts.SelectedChart.Split(';');
            if (items.Length == 2)
                return items[1];
        }

        return String.Empty;        
    }

    protected override object SaveViewState()
    {
        SelectedResource = resources.SelectedValue;
        if (_editControl != null)
        {
            ResourceProperties = (_editControl as BaseResourceEditControl).Properties;
        }

        return base.SaveViewState();
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["NetObject"]))
            netObjectID = Request.QueryString["NetObject"];

        if (Request.QueryString[SessionID] != null)
        {
            SessionGuid = UrlHelper.FromSafeBase64UrlParameter(Request.QueryString[SessionID]);
            _forceTitleChange = true;
        }
        else
        {
            SessionGuid = Guid.NewGuid().ToString();
        }

        InitializeResource();
        InitializeObjectTypes();
        if (LoadSessionData())
        {
            LoadProperties(_netObjectProperties);
            ListEntities.ReloadList();
            SetResourceProperties(false);
        }

        InitializeCustomResourceSettings();
        InitializeVisibility();

		if (!IsPostBack)
		{
			RebuildResourcesList();

			if (String.IsNullOrEmpty(ListEntities.EntityName))
				ddl_ObjectTypes.SelectedValue = DefaultEntityName;
			else
				ddl_ObjectTypes.SelectedValue = ListEntities.EntityName;
		}
    }

    private void InitializeVisibility()
    {
        if (ListEntities.SelectedEntititesCount > 1)
        {
            _netObjectMode = OrionChartingHelper.CustomObjectMode.MultiObjectMode;
            Period.Visible = SampleSize.Visible = !lc_ListCharts.ChartV2Selected;
            MultiObjectChartPanel.Visible = true;
            phResourceEditControl.Visible = resourcesLabel.Visible = resources.Visible = false;
            ListEntities.ShowFilter = true;
            AutoSelect.Enabled = isAllowedAutoSelect(netObjectID);
        }
        else
        {
            _netObjectMode = OrionChartingHelper.CustomObjectMode.SingleObjectMode;
            MultiObjectChartPanel.Visible = false;
            phResourceEditControl.Visible = resourcesLabel.Visible = resources.Visible = ListEntities.SelectedEntititesCount == 1 ? true : false;
            ListEntities.ShowFilter = false;
        }

        if (!resources.Visible)
            messagePlaceholder.Visible = false;

        EditUpdatePanel.Visible = true;
    }

    /// <summary>
    /// return true if this NetObject support autoselect
    /// </summary>
    /// <param name="NetObject"></param>
    /// <returns></returns>
    private bool isAllowedAutoSelect(string NetObject)
    {
        if (NetObject == null)
            return false;
        
        string[] str;
        try
        {
            str = NetObjectHelper.ParseNetObject(NetObject);
        }
        catch (ArgumentException ae)
        {
            _log.Warn("NetObject wasn't recognized", ae);
            return false;
        }        

        List<string> allowedPrefixes = new List<string>(new string[] { "N", "I", "V", "C" });
        return allowedPrefixes.Contains(str[0]);
    }

    /// <summary>
    /// Try to load data from session and store them in _netObjectProperties, data in session are then deleted
    /// </summary>
    /// <returns></returns>
    private bool LoadSessionData()
    {
        try
        {
            if (_netObjectProperties == null && !String.IsNullOrEmpty(Request.QueryString[SessionID]) && Session[SessionGuid] != null)
            {
                var newNetObjectProperties = (StringDictionary)Session[SessionGuid];
                Session.Remove(SessionGuid);

                if (newNetObjectProperties.ContainsKey(SessionID) &&
                    newNetObjectProperties[SessionID].Equals(UrlHelper.FromSafeBase64UrlParameter(Request.QueryString[SessionID]), StringComparison.OrdinalIgnoreCase))
                {
                    _netObjectProperties = newNetObjectProperties;
                    return true;
                }

                _netObjectProperties = null;
            }
        }
        catch (Exception e) {
            _log.Debug("Error during load data from session", e);
        }
        return false;
    }

    public string ObjectSWISType
    {
        get
        {
            return SolarWinds.Orion.Web.Dependencies.DependencyUIItem.GetSwisEntityTypeByEntityNameOrObjectFilter(Resource.Properties["EntityName"], Resource.Properties["ObjectFilter"]);
        }
    }

    private void LoadProperties(StringDictionary properties)
    {
        if (properties != null)
        {
            ListEntities.EntityName = DefaultEntityName;
            lc_ListCharts.EntityName = DefaultEntityName;

            if (properties.ContainsKey("EntityName"))
            {
                ddl_ObjectTypes.SelectedValue = properties["EntityName"];
                ListEntities.EntityName = properties["EntityName"];
                lc_ListCharts.EntityName = properties["EntityName"];
            }

            if (properties.ContainsKey("Title"))
                txtTitle.Text = HttpUtility.HtmlDecode(properties["Title"]);

            if (properties.ContainsKey("SubTitle"))
                txtSubTitle.Text = HttpUtility.HtmlDecode(properties["SubTitle"]);

            if (properties.ContainsKey("ShowSum"))
                ShowSum.SelectedValue = properties["ShowSum"];

            if (properties.ContainsKey("SelectedEntities"))
            {
                ListEntities.SelectedEntities = properties["SelectedEntities"];
                _netObjectMode = (properties["SelectedEntities"].Split(',').Count() <= 1) ? OrionChartingHelper.CustomObjectMode.SingleObjectMode :
                    OrionChartingHelper.CustomObjectMode.MultiObjectMode;
                MultiObjectChartPanel.Visible = _netObjectMode != OrionChartingHelper.CustomObjectMode.SingleObjectMode;
            }

            if (properties.ContainsKey("ChartName"))
                lc_ListCharts.SelectedChart = properties["ChartName"];

            if (properties.ContainsKey("Period"))
                Period.PeriodName = properties["Period"];

            if (properties.ContainsKey("SampleSize"))
                SampleSize.SampleSizeValue = properties["SampleSize"];

            if (properties.ContainsKey("FilterEntities"))
                ListEntities.FilterSelection = Boolean.Parse(properties["FilterEntities"]);

            if (properties.ContainsKey("TopXX") && !String.IsNullOrEmpty(properties["TopXX"]))
            {
                TopXX.Enabled = true;
                TopXX.Value = properties["TopXX"];
            }

            if (properties.ContainsKey("ManualSelect"))
            {
                AutoSelect.Checked = !Boolean.Parse(properties["ManualSelect"]);
            }

            if (properties.ContainsKey("AutoHide"))
                AutoHide.AutoHideValue = properties["AutoHide"];

            if (properties.ContainsKey(Is_custom_title))
                ViewState[Is_custom_title] = properties[Is_custom_title];
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Resource != null)
            PageHeaderTitle.Text = Page.Title = string.Format(Resources.CoreWebContent.WEBCODE_AK0_88, this.Resource.Title);            

        if (!Page.IsPostBack)
        {
            HttpContext.Current.Session.Remove("EmbeddedResourceData");

            if ((Resource.Properties.ContainsKey("ObjectUri")) && Resource.Properties.ContainsKey("ObjectFilter") && !Resource.Properties.ContainsKey("ResourceUpgraded"))
            {
                Resource.Properties["ResourceUpgraded"] = true.ToString();
                Resource.Properties["EntityName"] = ListEntities.EntityName = ddl_ObjectTypes.SelectedValue = ObjectSWISType;
                ListEntities.SelectedEntities = SolarWinds.Orion.Web.Helpers.UriHelper.GetId(Resource.Properties["ObjectUri"]).ToString();
                ListEntities.ReloadList();
                RebuildResourcesList();
                InitializeVisibility();
            }

            var embeddedPath = Resource.Properties["EmbeddedObjectResource"];
            if (!string.IsNullOrEmpty(embeddedPath))
            {
                var embeddedData = embeddedPath.Split(';');
                string lookfor = embeddedPath;
                if (embeddedData.Length > 1)
                {
                    var coma = ";";
                    var selected = string.Empty;
                    for (var i = 0; i < embeddedData.Length; i++)
                        if (!string.IsNullOrEmpty(embeddedData[i]))
                            selected += embeddedData[i] + coma;

                    lookfor = selected;
                }

                ListItem item = resources.Items.FindByValue(lookfor);
                if (item == null && resources.Visible)
                {
                    ListItem emptyitem = new ListItem("", lookfor);
                    resources.Items.Add(emptyitem);
                    messagePlaceholder.Visible = true;
                }
                else
                    messagePlaceholder.Visible = false;

                resources.SelectedValue = lookfor;
            }
			if (Resource.Properties.ContainsKey("ManualSelect"))
			{
				AutoSelect.Checked = !Boolean.Parse(Resource.Properties["ManualSelect"]);
			}

            FormatResourceControl(_forceTitleChange);
        }
        Page.ClientScript.RegisterClientScriptResource(typeof(CollapsePanel), "SolarWinds.Orion.Web.UI.CollapsePanel.js");
        var script = @"<script type='text/javascript'>
    function SaveData(key, value) {
        PageMethods.SaveData(key, value);
    } 
     </script>";
        if (!ClientScript.IsStartupScriptRegistered("JSScript"))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "JSScript", script);
        }

        if ((_netObjectMode == OrionChartingHelper.CustomObjectMode.MultiObjectMode) && (lc_ListCharts.ChartV2Selected))
        {
            _netObjectMode = OrionChartingHelper.CustomObjectMode.MultiObjectChartsV2;
            SampleSize.Visible = Period.Visible = AutoSelectObjects.Visible = OldChartSumRow.Visible = AutoHide.Visible = OldChartLimitSeries.Visible = Period.Visible = SampleSize.Visible = false;
        }
        else
        {
            SampleSize.Visible = Period.Visible = AutoSelectObjects.Visible = OldChartSumRow.Visible = AutoHide.Visible = OldChartLimitSeries.Visible = true;
        }

        SetResourceCustomizeLink();
    }

    private void InitializeResource()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["ResourceID"]))
        {
            int resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
            this.resource = ResourceManager.GetResourceByID(resourceID);

            if (Resource.Properties.ContainsKey("ShowSum"))
                ShowSum.SelectedValue = Resource.Properties["ShowSum"];
        }
    }

    private void InitializeCustomResourceSettings()
    {
        if (Resource.Properties.ContainsKey("Title"))
            txtTitle.Text = HttpUtility.HtmlDecode(Resource.Properties["Title"]);

        if (Resource.Properties.ContainsKey("SubTitle"))
            txtSubTitle.Text = HttpUtility.HtmlDecode(Resource.Properties["SubTitle"]);
    }

    private void ClearCache()
    {
        RemoveInitializationTag();
        ResourceProperties = null;
        EmbeddedResourceData = null;
    }

    private void InitializePropertiesForResource()
    {
        if (String.IsNullOrEmpty(SelectedResource)) return;

        var embeddedPath = EmbeddedPathParse();
        using (var control = LoadControl(embeddedPath) as BaseResourceControl)
        {
            if (control != null)
            {
                control.Resource = Resource;

                if (control is IResourceSupportsInitialization)
                {
                    (control as IResourceSupportsInitialization).InitializePropertiesForEdit(Resource);
                }
            }
        }
    }

    private void SetResourceCustomizeLink()
    {
        var embeddedPath = EmbeddedPathParse();

        if (string.IsNullOrEmpty(embeddedPath))
        {
            embeddedEdit.Visible = false;
            return;
        }

        Control control = LoadControl(embeddedPath);
        if (control is BaseResourceControl)
        {
            if (IsPostBack)
            { // we need to load properties in case of PostBack to have initialized data for edit component
                InitializeSelectedProperties(false, false);
            }

            (control as BaseResourceControl).Resource = Resource;
            var editCtrlUrl = (control as BaseResourceControl).EditControlLocation;
            var editUrl = (control as BaseResourceControl).EditURL;

            if (control is IResourceSupportsInitialization)
            {
                (control as IResourceSupportsInitialization).InitializePropertiesForEdit(Resource); 
            }

            // This is temporary solution to avoid risks - CORE-5330
            // Some resources doesn't work well with custom object resource. We force to use edit page for them except of using edit control.
            var skipEditCtrLoad = editControlsToSkip.IndexOf(editCtrlUrl.ToLower()) > -1;

            control.Dispose();
            phResourceEditControl.Controls.Clear();
            if (!string.IsNullOrEmpty(editCtrlUrl) && (_netObjectMode == OrionChartingHelper.CustomObjectMode.SingleObjectMode)&& !skipEditCtrLoad)
            {
                _editControl = LoadControl(editCtrlUrl);
                var cnt = _editControl as BaseResourceEditControl;

                if (cnt != null)
                {
                    cnt.Resource = Resource;
                    if (!String.IsNullOrEmpty(ListEntities.SelectedEntities) && !String.IsNullOrEmpty(ListEntities.EntityName))
                        cnt.NetObjectID = NetObjectTypeToSWISEntityMapper.GetNetObjectIDs(ListEntities.SelectedEntities, ListEntities.EntityName).FirstOrDefault();
                    phResourceEditControl.Controls.Add(_editControl.TemplateControl);
                    phResourceEditControl.Visible = true;
                    embeddedEdit.Visible = false;
                }
            }
            else if (editUrl.IndexOf("TitleEdit.aspx", StringComparison.OrdinalIgnoreCase) > 0 ||
                (editUrl.IndexOf("EditResource.aspx", StringComparison.OrdinalIgnoreCase) > 0 && !skipEditCtrLoad))
            {
                phResourceEditControl.Visible = false;
                embeddedEdit.Visible = false;
                if (_editControl != null)
                    _editControl.Dispose();
            }
            else
            {
                phResourceEditControl.Visible = false;
                embeddedEdit.Visible = true;
                //Pass query strings from current page to resource edit
                var newParams = Request.QueryString.ToEnumerable().Merge(UrlHelper.ParseQueryString(editUrl));
                this.hfEmbeddedPath.Value = UrlHelper.ReplaceQueryString(editUrl, newParams);
                if (_editControl != null)
                    _editControl.Dispose();
            }
        }
        EditUpdatePanel.Update();
    }

    private string EmbeddedPathParse()
    {
        if (String.IsNullOrEmpty(SelectedResource))
            return SelectedResource;

        if ((_netObjectMode != OrionChartingHelper.CustomObjectMode.SingleObjectMode) && (lc_ListCharts.ChartV2Selected))
        {
            string[] items = SelectedResource.Split(';');
            if (items.Length == 2)
                return items[1];
            else
                return items[0];
        }
        else
        {
            return SelectedResource.Split(';')[0];
        }
    }

    private void InitializeSelectedProperties(bool clear, bool perists)
    {
        if (clear)
            Resource.Properties.Clear();

        foreach (KeyValuePair<string, object> property in ResourceProperties)
        {
            if (Resource.Properties.ContainsKey(property.Key) && !perists)  // we don't want to ovewrite allready defined values
                continue;

            if (Resource.Properties.ContainsKey(property.Key))
            {
                Resource.Properties[property.Key] = property.Value.ToString();
            }
            else
            {
                if (perists)
                    Resource.Properties.Add(property.Key, property.Value.ToString());
                else
                    Resource.Properties.AddWithoutSave(property.Key, property.Value.ToString());
            }
        }
    }

    public void resources_SelectedIndexChanged(object sender, EventArgs e)
    {
        messagePlaceholder.Visible = string.IsNullOrEmpty(((DropDownList)sender).SelectedItem.Text);
        FormatResourceControl(true);
        ClearCache();
    }

    /// <summary>
    /// Event handler called when list of selected netobjects have changed
    /// </summary>
    public void SelectObjectChanged()
    {
        if (!String.IsNullOrEmpty(ListEntities.SelectedEntities))
        {
            _netObjectMode = (ListEntities.SelectedEntities.Split(',').Count() <= 1) ? OrionChartingHelper.CustomObjectMode.SingleObjectMode :
                OrionChartingHelper.CustomObjectMode.MultiObjectMode;
        } else
        { // default selection mode is Single
            _netObjectMode = OrionChartingHelper.CustomObjectMode.SingleObjectMode;
        }

        if (_netObjectMode == OrionChartingHelper.CustomObjectMode.SingleObjectMode)
        {
            RebuildResourcesList();
            lc_ListCharts.EntityName = ddl_ObjectTypes.SelectedValue;
            lc_ListCharts.ReloadList();
        }

        ListEntities.EntityName = ddl_ObjectTypes.SelectedValue;
        ListEntities.ReloadList();
        SetResourceCustomizeLink();
        InitializeVisibility();
        
        FormatResourceControl(true);
    }

    protected void embeddedEdit_Click(object sender, EventArgs e)
    {
        if (!IsTagInitialized)
            SetResourceProperties(ClearPrevProperties);

        Response.Redirect(hfEmbeddedPath.Value);
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        SetResourceProperties(ClearPrevProperties);
        HttpContext.Current.Session.Remove("EmbeddedResourceData");

        Response.Redirect(
            BaseResourceControl.GetEditPageReturnUrl(
                Request.QueryString,
                Resource.View.ViewID.ToString()));
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        HttpContext.Current.Session.Remove("EmbeddedResourceData");

        Response.Redirect(
            BaseResourceControl.GetEditPageReturnUrl(
                Request.QueryString,
                Resource.View.ViewID.ToString()));
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod]
    public static void SaveData(string key, object value)
    {
        var data = new Dictionary<string, object>();
        data[key] = value;

        var oldData = EmbeddedResourceData;
        if (oldData != null && oldData.Count > 0)
        {
            foreach (var pair in data)
            {
                if (oldData.ContainsKey(pair.Key))
                {
                    oldData.Remove(pair.Key);
                }
            }
            EmbeddedResourceData = data.Concat(oldData).ToDictionary(e => e.Key, e => e.Value);
        }
        else
        {
            EmbeddedResourceData = data;
        }

    }

    protected void FormatResourceControl(bool force)
    {
        if ((String.IsNullOrEmpty(txtTitle.Text) || force) && (ViewState[Is_custom_title] == null || !bool.Parse(ViewState[Is_custom_title].ToString())))
        {
            switch (_netObjectMode)
            {
                case OrionChartingHelper.CustomObjectMode.MultiObjectChartsV2:
                case OrionChartingHelper.CustomObjectMode.MultiObjectMode:
                    txtTitle.Text = Resources.CoreWebContent.WEBCODE_AK0_87;
                    break;
                case OrionChartingHelper.CustomObjectMode.SingleObjectMode:
                default:
                    if (!String.IsNullOrEmpty(ListEntities.SelectedEntities) &&
                        !String.IsNullOrEmpty(ListEntities.EntityName))
                    {
                        try
                        {
                            var netObject =
                                NetObjectFactory.Create(
                                    NetObjectTypeToSWISEntityMapper.GetNetObjectIDs(ListEntities.SelectedEntities,
                                        ListEntities.EntityName).
                                        FirstOrDefault());
                            if (netObject != null && !String.IsNullOrEmpty(netObject.Name) &&
                                resources.SelectedItem != null && !String.IsNullOrEmpty(resources.SelectedItem.Text))
                                txtTitle.Text = String.Format("{0} - {1}", netObject.Name, resources.SelectedItem.Text);
                        }
                        catch (Exception e)
                        {
                            _log.Warn("Custom Object resource error with non existing netobject", e);
                        }
                    }
                    else
                    {
                        txtTitle.Text = Resources.CoreWebContent.WEBCODE_VB0_112;
                    }

                    break;
            }
        }
        else
        {
            ViewState[Is_custom_title] = bool.TrueString;
        }
        txtSubTitle.Text = String.IsNullOrEmpty(txtSubTitle.Text) ? Resources.CoreWebContent.WEBCODE_VB0_111 : txtSubTitle.Text;
        TitleUpdatePanel.Update();
    }

    private void SetResourceProperties(bool clearPrevious)
    {
		if (clearPrevious)
			Resource.Properties.Clear();
        if (!String.IsNullOrEmpty(txtTitle.Text))
        {
            Resource.Title = SafeTitle;
        } else
        {
            var resourceControl = (BaseResourceControl) LoadControl(Resource.File);
            Resource.Title = resourceControl.Title;
        }

        Resource.Properties["SubTitle"] = Resource.SubTitle = string.IsNullOrEmpty(txtSubTitle.Text) ? Resources.CoreWebContent.WEBCODE_VB0_111 : SafeSubTitle;

        SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
        InitializePropertiesForResource();
        InitializeSelectedProperties(clearPrevious, true);
        SetInitializeTag();
    }

    private bool IsTagInitialized
    {
        get
        {
            return (Resource.Properties.ContainsKey("Initialized") &&
                    Boolean.TrueString.Equals(Resource.Properties["Initialized"], StringComparison.OrdinalIgnoreCase));
        }
    }

    private void RemoveInitializationTag()
    {
        if (Resource.Properties.ContainsKey("Initialized"))
            Resource.Properties.Remove("Initialized");
    }

    private void SetInitializeTag()
    {
        Resource.Properties["Initialized"] = true.ToString();
    }

    private IEnumerable<ResourceTemplateCategory> GetResourceTemplateCategories()
    {
        if (ListEntities == null || string.IsNullOrEmpty(ListEntities.EntityName))
            return null;

        var detailsViewName = NetObjectFactory.GetDetailsViewNameBySWISType(ListEntities.EntityName);
        if (string.IsNullOrEmpty(detailsViewName))
            return null;

        return ResourceTemplateManager.GetCategoriesForViewType(detailsViewName, false, IsSupported);
    }

    private static bool IsSupported(ResourceTemplate resourceTemplate, ICollection<Type> requiredList, ICollection<string> supportedKeys)
    {
        foreach (var si in resourceTemplate.SupportedInterfaces)
        {
            if (requiredList.Contains(si))
                return true;
        }
        
        if (ResourceTemplateManager.AreDynamicInfoKeysSupported(resourceTemplate, supportedKeys))
        {
            return true;
        }

        foreach (var ri in resourceTemplate.RequiredInterfaces)
        {
            if (!requiredList.Contains(ri))
                return false;
        }

        return resourceTemplate.RequiredInterfaces.Any();
    }

    private void RebuildResourcesList()
    {
        string oldSelVal = SelectedResource;

        resources.Items.Clear();
        Dictionary<string, string> items = new Dictionary<string, string>();

        IEnumerable<ResourceTemplateCategory> categories = GetResourceTemplateCategories();

        if (categories == null)
            return;

        PopulateObjectResources(categories, items);

        resources.Items.AddRange(items.OrderBy(p => p.Key).Select(p => new ListItem(p.Key, p.Value)).ToArray());

        if (resources.Items.FindByValue(oldSelVal) != null)
            resources.SelectedValue = oldSelVal;
        
        SelectedResource = resources.SelectedValue;
    }

    private void PopulateObjectResources(IEnumerable<ResourceTemplateCategory> categories, IDictionary<string, string> items)
    {
        bool isNodeWrls = false;
        bool isNodeVm = false;
        bool isNodeEW = false;

		NetObject netObject = null;
		try
		{
            netObject = NetObjectFactory.Create(NetObjectTypeToSWISEntityMapper.GetNetObjectIDs(ListEntities.SelectedEntities,
                ListEntities.EntityName).FirstOrDefault());
		}
		catch (AccountLimitationException)
		{
			// object is limited
			return;
		}
		catch (IndexOutOfRangeException)
		{
			// object isn't exist
			return;
		}
		catch (Exception)
		{
			// instance of selected object couldn't be created
			return;
		}

        using (var proxy = _blProxyCreator.Create((error) => _log.Error(error)))
        {
            int nodeID = netObject is Node ? ((Node)netObject).NodeID : netObject.Parent is Node ? (netObject.Parent as Node).NodeID : 0;
            if (nodeID > 0)
            {
                if (VimHelper.VMwarePollingAvailable)
                {
                    isNodeVm = (new VMWareSwisDAL()).IsNodeESX(nodeID);
                }
                else
                {
                    isNodeVm = false;
                }
                
                isNodeWrls = proxy.IsNodeWireless(nodeID);
                isNodeEW = proxy.IsNodeEnergyWise(nodeID);                
            }
        }

        foreach (ResourceTemplateCategory category in categories)
        {

            if ((category.Title == WirelessAutonomousAccessPoints) || (category.Title == WirelessControllers))
            {
                if (isNodeWrls)
                    PopulateResources(category, items, netObject.NetObjectID);
            }
            else if ((category.Title == EnergyWiseControls) || (category.Title == EnergyWiseCharts))
            {
                if (isNodeEW)
                    PopulateResources(category, items, netObject.NetObjectID);
            }
            else if (category.Title == VMWareESX)
            {
                if (isNodeVm)
                    PopulateResources(category, items, netObject.NetObjectID);
            }
            else
            {
                PopulateResources(category, items, netObject.NetObjectID);
            }
        }
    }

    private static void PopulateResources(ResourceTemplateCategory category, IDictionary<string, string> items, string netObjectId)
    {
        if (category == null)
            throw new ArgumentNullException("category");
        if (items == null)
            throw new ArgumentNullException("items");

        var coma = ";";
        var xuiCharts = new Dictionary<string, string>();
		
        foreach (ResourceTemplate res in category.ResourceTemplates)
        {
            var processedTitle = Macros.ParseMacros(res.Title, netObjectId);
            if ((res.SupportedInterfaces.Any() || res.RequiredInterfaces.Any() || res.RequiredDynamicInfoKeys.Any()) && res.Title != null && !res.Title.Equals("not available for selection") && !res.IsInternal)
            {
                var valueTemplate = string.Empty;
                if (res.InitialProperties.Count > 0)
                {
                    foreach (var prop in res.InitialProperties)
                    {
                        valueTemplate = $"{valueTemplate}{prop.Key}={prop.Value}{coma}";
                    }
                }

                var processedTemplate = string.IsNullOrEmpty(valueTemplate) ? res.ControlPath : res.ControlPath + ";" + valueTemplate;

                if (res.ImplementedInterfaces.Contains(typeof(IXuiResource)) && res.InitialProperties.ContainsKey("ChartConfiguration"))
                {
                    xuiCharts[processedTitle] = processedTemplate;
                }
                else if (!items.ContainsKey(processedTitle))
                {
                    items.Add(processedTitle, processedTemplate);
                }
            }
        }

        //add xui charts to collection
        foreach (var xuiChart in xuiCharts)
        {
            if (items.ContainsKey(xuiChart.Key))
            {
                //update title if there are any duplications
                items[$"{xuiChart.Key} ({Resources.CoreWebContent.ChartStyleModern})"] = xuiChart.Value;
            }
            else
            {
                items[xuiChart.Key] = xuiChart.Value;
            }
        }
    }

///////////////////////////////////////////////////////////////////
    // update for selecting multi object charts
    protected void SelectObjects_Click(object sender, EventArgs e)
    {
        var data = new StringDictionary();
        data["Title"] = string.IsNullOrEmpty(txtTitle.Text) ? Resources.CoreWebContent.WEBCODE_VB0_111 : SafeTitle;
        data["SubTitle"] = string.IsNullOrEmpty(txtSubTitle.Text) ? Resources.CoreWebContent.WEBCODE_VB0_111 : SafeSubTitle;
        data["Period"] = Period.PeriodName;
        data["SampleSize"] = SampleSize.SampleSizeValue;
        data["ShowSum"] = ShowSum.SelectedValue;
        data["Entities"] = ddl_ObjectTypes.SelectedItem.Text;
        data["EntityName"] = ddl_ObjectTypes.SelectedItem.Value;
        if (ListEntities != null && !String.IsNullOrEmpty(ListEntities.SelectedEntities))
        {
            data["SelectedEntities"] = ListEntities.SelectedEntities;
        }

        data["ChartName"] = lc_ListCharts.SelectedChart;
        data["FilterEntities"] = ListEntities.FilterSelection.ToString();
        data["TopXX"] = TopXX.Enabled ? TopXX.Value.ToString() : String.Empty;
        data["ManualSelect"] = (!AutoSelect.Checked).ToString();
        data["AutoHide"] = AutoHide.AutoHideValue;
        data[Is_custom_title] = ViewState[Is_custom_title] == null ? bool.FalseString : ViewState[Is_custom_title].ToString();

        Session[SessionGuid] = data;

        var url = String.Format(SelectObjectsURI, GetUrlParameters());
        url = UrlHelper.UrlAppendUpdateParameter(url, "SessionID", UrlHelper.ToSafeBase64UrlParameter(SessionGuid));
        url = UrlHelper.UrlAppendUpdateParameter(url, "ReturnTo", UrlHelper.ToSafeUrlParameter(Request.Url.ToString()));
        this.Response.Redirect(url);
    }

    private string GetUrlParameters()
    {
        var list = new List<string>();

        AddUrlParameter(list, "ResourceID");
        AddUrlParameter(list, "NetObject");
        AddUrlParameter(list, "ViewID");

        return (list.Count > 0) ? "?" + String.Join("&", list.ToArray()) : String.Empty;
    }

    private void AddUrlParameter(List<string> list, string name)
    {
        if (!String.IsNullOrEmpty(Request.QueryString[name]))
            list.Add(String.Format("{0}={1}", name, Request[name]));
    }

    protected void ddl_ObjectTypes_SelectedIndexChanged(object sender, EventArgs e)
    {
        var data = new StringDictionary();

        data["Entities"] = ddl_ObjectTypes.SelectedItem.Text;
        data["EntityName"] = ddl_ObjectTypes.SelectedValue;
        Session[SessionGuid] = data;

        Resource.Properties.Remove("EmbeddedObjectResource");

        lc_ListCharts.EntityName = ddl_ObjectTypes.SelectedValue;
        lc_ListCharts.ReloadList();

        ListEntities.EntityName = ddl_ObjectTypes.SelectedValue;
        ListEntities.SelectedEntities = string.Empty;
        ListEntities.ReloadList();

        EditUpdatePanel.Visible = false;
        MultiObjectChartPanel.Visible = false;
        _netObjectMode = OrionChartingHelper.CustomObjectMode.SingleObjectMode;

        if (LoadSessionData())
            LoadProperties(_netObjectProperties);

        FormatResourceControl(true);
    }

    /// <summary>
    /// Initialize combobox with object types
    /// </summary>
    private void InitializeObjectTypes()
    {
        ContainersMetadataDAL cmdal = new ContainersMetadataDAL();
        string NetObject = Request.QueryString["NetObject"];

        Dictionary<string, string> entities = new Dictionary<string, string>();
        // do not include entities that haven't supports ChartInfoPlugin
        foreach (KeyValuePair<string, string> item in cmdal.GetAvailableContainerMemberEntities(true).OrderBy(x => x.Value))
        {
            if (ChartInfoFactoryPluginManager.ChartInfoPluginSupported(item.Key))
            {
                if (!string.IsNullOrEmpty(NetObject))
                {
                    if (NetObject.ToUpper().StartsWith("C:") ||
                        (!NetObject.ToUpper().StartsWith("C:") && (item.Key != "Orion.Groups")))
                    {
                        entities.Add(item.Key, GetLocalizedEntityDescription(item.Value));
                    }
                }
                else
                {
                    entities.Add(item.Key, GetLocalizedEntityDescription(item.Value));
                }
            }
            else
            {
                // block net objects if its module "resourceHost" attribute isn't defined within the .config files
                string detailsView = NetObjectFactory.GetDetailsViewNameBySWISType(item.Key);
                if (string.IsNullOrEmpty(detailsView))
                    continue;
                if (string.IsNullOrEmpty(ViewManager.GetResourceHostByViewType(detailsView)))
                    continue;

                entities.Add(item.Key, GetLocalizedEntityDescription(item.Value));
            }
        }

        ddl_ObjectTypes.DataSource = entities;
        ddl_ObjectTypes.DataTextField = "Value";
        ddl_ObjectTypes.DataValueField = "Key";
        ddl_ObjectTypes.DataBind();
    }

    private static string GetLocalizedEntityDescription(string entityValue)
    {
        var manager = ResourceManagerRegistrar.Instance;
        var key = manager.CleanResxKey("Entity", entityValue);
        var result = manager.SearchAll(key, manager.GetAllResourceManagerIds());
        return !String.IsNullOrEmpty(result) ? result : entityValue;
    }

    protected string Entities
    {
        get
        {
            return ddl_ObjectTypes.SelectedItem.Text;
        }
    }

    protected void txtTitle_TextChanged(object sender, System.EventArgs e)
    {
        ViewState[Is_custom_title] = bool.TrueString;
    }
}