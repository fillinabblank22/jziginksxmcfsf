using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System.Data;
using SolarWinds.Orion.Core.Common.Federation;


[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.IsFederationAware, "true")]
public partial class Orion_NetPerfMon_Resources_TopXX_AvgDiskSecPerTransferTopXX : TopXXResourceControl
{
    private string currentNodeId = String.Empty;
    
    protected string FormatDecimalValue(object value)
    {
        decimal tmpValue = Convert.ToDecimal(value);
        return Convert.ToString(Math.Round(tmpValue, 3));
    }

    protected override string TitleTemplate
    {
        get { return Resources.CoreWebContent.WEBCODE_PS0_1; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Wrapper.ShowEditButton = this.EnableEdit;
        var filter = this.Resource.Properties["Filter"];
        DataTable table;
        
        try
        {
            table = TopXXSWQLDAL.GetVolumeAvgDiskSecPerTransfer(this.MaxRecords, filter, this.Resource.View.LimitationID);
        }
        catch (Exception ex)
        {
            if (HasSWQLFilterError(ex, filter))
            {
                this.SQLErrorPanel.Visible = true;
                return;
            }
            throw;
        }

        this.resourceTable.DataSource = table;
        this.resourceTable.DataBind();
    }

    public override Dictionary<string, string> InitialProperties
    {
        get
        {
            return new Dictionary<string, string> { { "Type", EditNetObjectType.Volumes.ToString() } };
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionCorePHResourceAvgDiskSecTransfer";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    protected void ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HtmlTableRow groupHeaderRow = e.Item.FindControl("groupHeaderRow") as HtmlTableRow;

            DataRowView row = e.Item.DataItem as DataRowView;
            bool newNode = false;
            if (row["NodeID"].ToString() != this.currentNodeId)
            {
                this.currentNodeId = row["NodeID"].ToString();
                newNode = true;
            }

            groupHeaderRow.Visible = newNode;
        }
    }

    public string GetPerfstackLink(string volumeID, string site)
    {
        return string.Format(@"{2}/ui/perfstack/?presetTime=Today&charts={1}_Orion.Volumes_{0}-Orion.VolumePerformanceHistory.AvgDiskTransfer;", volumeID, 0, FederationUrlHelper.GetLinkPrefix(site));
    }
}
