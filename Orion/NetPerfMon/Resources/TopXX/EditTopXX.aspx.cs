using System;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Core.Common.PackageManager;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_NetPerfMon_Resources_TopXX_Edit : System.Web.UI.Page
{
	#region Resource property
	private ResourceInfo resource;

	protected ResourceInfo Resource
	{
		get { return this.resource; }
		set { this.resource = value; }
	} 
	#endregion

	#region NetObjectType property
	private string netObjectType;

	protected string NetObjectType
	{
		get { return this.netObjectType; }
		set { this.netObjectType = value; }
	} 
	#endregion

    protected bool isInterfacesAllowed;

	protected void Page_Load(object sender, EventArgs e)
	{
        this.isInterfacesAllowed = PackageManager.InstanceWithCache.IsPackageInstalled("Orion.Interfaces");
        this.NodesColumns.Visible = !((this.Request.QueryString["HideNodesFilter"] ?? string.Empty).ToUpperInvariant() == "TRUE");
        this.InterfacesColumns.Visible = isInterfacesAllowed && !((this.Request.QueryString["HideInterfaceFilter"] ?? string.Empty).ToUpperInvariant() == "TRUE");
		this.VolumesColumns.Visible = (this.Request.QueryString["ShowVolumeFilter"] ?? string.Empty).ToUpperInvariant() == "TRUE";
        this.NetObjectType = GetLocalizedProperty("Entity", (this.Request.QueryString["Type"]));

		if (!String.IsNullOrEmpty(this.Request.QueryString["ResourceID"]))
		{
			int resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
			this.Resource = ResourceManager.GetResourceByID(resourceID);
            this.Title = String.Format(Resources.CoreWebContent.WEBCODE_AK0_88, Resource.Title);
		}

		if (!this.IsPostBack)
		{
			int max;
			if (!Int32.TryParse(this.Resource.Properties["MaxRecords"], out max))
			{
				this.Resource.Properties["MaxRecords"] = "10";
			}
			this.maxCount.Text = this.Resource.Properties["MaxRecords"];
			this.filter.Text = this.Resource.Properties["Filter"];

			if (!string.IsNullOrEmpty(Resource.Properties["Title"]))
			{
				this.resourceTitleEditor.ResourceTitle = Resource.Properties["Title"];
			}
			else
			{
				this.resourceTitleEditor.ResourceTitle = string.Empty;
			}

			if (!string.IsNullOrEmpty(Resource.Properties["SubTitle"]))
			{
				this.resourceTitleEditor.ResourceSubTitle = Resource.Properties["SubTitle"];
			}
			else
			{
				this.resourceTitleEditor.ResourceSubTitle = string.Empty;
			}
		}

		this.nodeColumns.DataSource = TopXXDAL.GetNodeColumnNames();
		this.nodeColumns.DataBind();

		if (InterfacesColumns.Visible)
		{
			this.interfaceColumns.DataSource = TopXXDAL.GetInterfaceColumnNames();
			this.interfaceColumns.DataBind();
		}

		if (VolumesColumns.Visible)
		{
			this.volumeColumns.DataSource = TopXXDAL.GetVolumeColumnNames();
			this.volumeColumns.DataBind();
		}
	}

	protected void SubmitClick(object sender, EventArgs e)
	{
		this.Resource.Properties["Title"] = WebSecurityHelper.SanitizeHtml(this.resourceTitleEditor.ResourceTitle);
		this.Resource.Properties["SubTitle"] = WebSecurityHelper.SanitizeHtml(this.resourceTitleEditor.ResourceSubTitle);

		this.Resource.Properties["MaxRecords"] = this.maxCount.Text;
		this.Resource.Properties["Filter"] = this.filter.Text;

        string strRedirectUrl = String.Format("/Orion/View.aspx?ViewID={0}", this.Resource.View.ViewID);
        if (!string.IsNullOrEmpty(Request["NetObject"]))
        {
            strRedirectUrl = string.Format("{0}&NetObject={1}", strRedirectUrl, Request["NetObject"]);
        }

		Response.Redirect(strRedirectUrl);
	}

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
}
