<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResponseTimeTopXX.ascx.cs" Inherits="Orion_NetPerfMon_Resources_TopXX_ResponseTime" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Federation" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Swis" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= DefaultSanitizer.SanitizeHtml(this.CustomSqlErrorMessage) %>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="resourceTable">
            <HeaderTemplate>
    <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%">
        <tr>
            <td class="ReportHeader" colspan="2"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_171) %></td>
            <td class="ReportHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_64) %></td>
            <td class="ReportHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_51) %></td>
            <% if (SwisFederationInfo.IsFederationEnabled)
               { %>
                <td class="ReportHeader" colspan="2"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YS0_1) %></td>
            <% } %>
        </tr>
            </HeaderTemplate>
            <ItemTemplate>
	    <tr>
	        <td class="Property" valign="middle" width="20"><img src="<%# DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.UI.StatusIcons.NodeIconFactory.SmallIconURL( Eval("NodeID"), Eval("Status") )) %>" alt="Status" style="vertical-align:bottom;" />&nbsp;</td>
	        <td class="Property"><a <%# DefaultSanitizer.SanitizeHtml(this.FormatParamString(Container.DataItem, this.Profile.AllowNodeManagement)) %> href="<%# DefaultSanitizer.SanitizeHtml($"{FederationUrlHelper.GetLinkPrefix(Eval("InstanceSiteId").ToString())}/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{Eval("NodeID")}") %>"><%# DefaultSanitizer.SanitizeHtml(Eval("Caption")) %></a>&nbsp;</td>
	        <td class="Property"><a href="<%# DefaultSanitizer.SanitizeHtml(GetPerfstackLink(Eval("NodeID").ToString(), Eval("InstanceSiteId").ToString())) %>" target="_blank"><%# DefaultSanitizer.SanitizeHtml(this.FormatResponseTime(Eval("ResponseTime"))) %></a>&nbsp;</td>
	        <td class="Property"><a href="<%# DefaultSanitizer.SanitizeHtml(GetLossPerfstackLink(Eval("NodeID").ToString(), Eval("InstanceSiteId").ToString())) %>" target="_blank"><%# DefaultSanitizer.SanitizeHtml(this.FormatPercentLoss(Eval("PercentLoss"))) %></a>&nbsp;</td>
	        <% if (SwisFederationInfo.IsFederationEnabled)
	           { %>
	            <td class="Property">
	                <asp:HyperLink ID="SiteLink" runat="server" Target="_blank" NavigateUrl='<%# DefaultSanitizer.SanitizeHtml(FederationUrlHelper.GetLinkPrefix((int)Eval("InstanceSiteId"))) %>'><%# DefaultSanitizer.SanitizeHtml(Eval("SiteName")) %></asp:HyperLink>
	            </td>
	        <% } %>
        </tr>
            </ItemTemplate>
            <FooterTemplate>
    </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>