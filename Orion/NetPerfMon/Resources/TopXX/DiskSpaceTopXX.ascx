<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DiskSpaceTopXX.ascx.cs" Inherits="Orion_NetPerfMon_Resources_TopXX_DiskSpace" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Federation" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Swis" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= DefaultSanitizer.SanitizeHtml(this.CustomSqlErrorMessage) %>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="resourceTable" OnItemDataBound="ItemDataBound">
            <HeaderTemplate>
    <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%">
            </HeaderTemplate>
            <ItemTemplate>
	    <tr runat="server" id="groupHeaderRow">
	        <td class="Property" valign="middle" width="20"><img src="<%# DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.UI.StatusIcons.NodeIconFactory.SmallIconURL( Eval("NodeID"), Eval("Status") )) %>" alt="Status" style="vertical-align:bottom;" /></td>
	        <td class="PropertyHeader" colspan="4"><a <%# DefaultSanitizer.SanitizeHtml(this.FormatParamString(Container.DataItem, this.Profile.AllowNodeManagement)) %> href="<%# DefaultSanitizer.SanitizeHtml($"{FederationUrlHelper.GetLinkPrefix(Eval("InstanceSiteId").ToString())}/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{Eval("NodeID")}") %>"><%# DefaultSanitizer.SanitizeHtml(Eval("NodeName")) %></a></td>
	        <td class="PropertyHeader" visible="<%# SwisFederationInfo.IsFederationEnabled %>">
	            <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank" NavigateUrl='<%# DefaultSanitizer.SanitizeHtml(FederationUrlHelper.GetLinkPrefix((int)Eval("InstanceSiteId"))) %>'><%# DefaultSanitizer.SanitizeHtml(Eval("SiteName")) %></asp:HyperLink>
	        </td>
	    </tr>
	    <tr>
	        <td class="Property"></td>
	        <td class="Property" valign="middle" width="20">
	            <asp:Image ID="VolumeImage" runat="server" ImageUrl='<%# DefaultSanitizer.SanitizeHtml("/NetPerfMon/images/Volumes/" + Eval("Icon")) %>' ImageAlign="Middle" />
	        </td>
	        <td class="Property"><a <%# DefaultSanitizer.SanitizeHtml(this.FormatParamString(Container.DataItem, this.Profile.AllowNodeManagement)) %> href="<%# DefaultSanitizer.SanitizeHtml($"{FederationUrlHelper.GetLinkPrefix(Eval("InstanceSiteId").ToString())}/Orion/NetPerfMon/VolumeDetails.aspx?NetObject=V:{Eval("VolumeID")}") %>"><%# DefaultSanitizer.SanitizeHtml(Eval("Caption")) %></a>&nbsp;</td>
	        <td class="Property"><a href="<%# DefaultSanitizer.SanitizeHtml(GetPerfstackLink(Eval("VolumeID").ToString(), Eval("InstanceSiteId").ToString())) %>" target="_blank"><%# DefaultSanitizer.SanitizeHtml(this.FormatPercentVolumeUsed(Eval("VolumePercentUsed"), (double)Eval("Level1Value"), (double)Eval("Level2Value"))) %></a></td>
	        <td class="Property"><a href="<%# DefaultSanitizer.SanitizeHtml(GetPerfstackLink(Eval("VolumeID").ToString(), Eval("InstanceSiteId").ToString())) %>" target="_blank"><%# DefaultSanitizer.SanitizeHtml(this.FormatPercentVolumeUsedBar(Eval("VolumePercentUsed"), (double)Eval("Level1Value"), (double)Eval("Level2Value"))) %></a></td>
	        <td class="PropertyHeader" runat="server" visible="<%# SwisFederationInfo.IsFederationEnabled %>"></td>
        </tr>
            </ItemTemplate>
            <FooterTemplate>
    </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>