using System;
using System.Data;
using SolarWinds.Orion.Core.Common.Federation;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.DAL;


[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.IsFederationAware, "true")]
public partial class Orion_NetPerfMon_Resources_TopXX_ResponseTime : TopXXResourceControl
{
    protected override string TitleTemplate
    {
        get { return Resources.CoreWebContent.WEBCODE_TM0_11; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Wrapper.ShowEditButton = this.EnableEdit;
        var filter = this.Resource.Properties["Filter"];
        DataTable table;

		try
		{
			table = TopXXSWQLDAL.GetCurrentResponseTime(this.MaxRecords, filter, this.Resource.View.LimitationID);
		}
		catch (Exception ex)
		{
		    if (HasSWQLFilterError(ex, filter))
		    {
		        this.SQLErrorPanel.Visible = true;
		        return;
		    }
		    throw;
		}

        this.resourceTable.DataSource = table;
        this.resourceTable.DataBind();
    }
       
    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceTopXXNodesCurrentResponseTime"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    public string GetPerfstackLink(string nodeID, string site)
    {
        return string.Format(@"{2}/ui/perfstack/?context={1}_Orion.Nodes_{0}&withRelationships=true&presetTime=Today&charts={1}_Orion.Nodes_{0}-Orion.ResponseTime.AvgResponseTime;", nodeID, 0, FederationUrlHelper.GetLinkPrefix(site));
    }

    public string GetLossPerfstackLink(string nodeID, string site)
    {
        return string.Format(@"{2}/ui/perfstack/?context={1}_Orion.Nodes_{0}&withRelationships=true&presetTime=Today&charts={1}_Orion.Nodes_{0}-Orion.ResponseTime.PercentLoss;", nodeID, 0, FederationUrlHelper.GetLinkPrefix(site));
    }
}
