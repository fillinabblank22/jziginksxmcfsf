﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System.Data;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Federation;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.IsFederationAware, "true")]
public partial class Orion_NetPerfMon_Resources_TopXX_TotalIOPSTopXX : TopXXResourceControl
{
    private string currentNodeId = String.Empty;
    private static readonly Log log = new Log();

    protected string FormatDecimalValue(object value)
    {
        decimal tmpValue = Convert.ToDecimal(value);
        return Convert.ToString(Math.Round(tmpValue, 2));
    }

    protected override string TitleTemplate
    {
        get { return Resources.CoreWebContent.WEBCODE_PS0_3; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Wrapper.ShowEditButton = this.EnableEdit;
        var filter = this.Resource.Properties["Filter"];
        DataTable table;

        try
        {
            table = TopXXSWQLDAL.GetVolumeTotalIOPS(this.MaxRecords, filter, this.Resource.View.LimitationID);
        }
		catch (Exception ex)
		{
		    if (HasSWQLFilterError(ex, filter))
		    {
		        this.SQLErrorPanel.Visible = true;
		        return;
		    }
		    throw;
		}

        this.resourceTable.DataSource = table;
        this.resourceTable.DataBind();
    }

    public override Dictionary<string, string> InitialProperties
    {
        get
        {
            return new Dictionary<string, string> { { "Type", EditNetObjectType.Volumes.ToString() } };
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionCorePHResourceTotalIOPS";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    protected void ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HtmlTableRow groupHeaderRow = e.Item.FindControl("groupHeaderRow") as HtmlTableRow;

            DataRowView row = e.Item.DataItem as DataRowView;
            bool newNode = false;
            if (row["NodeID"].ToString() != this.currentNodeId)
            {
                this.currentNodeId = row["NodeID"].ToString();
                newNode = true;
            }

            groupHeaderRow.Visible = newNode;
        }
    }

    public string GetReadPerfstackLink(string volumeID, string site)
    {
        return string.Format(@"{2}/ui/perfstack/?presetTime=Today&charts={1}_Orion.Volumes_{0}-Orion.VolumePerformanceHistory.AvgDiskReads;", volumeID, 0, FederationUrlHelper.GetLinkPrefix(site));
    }

    public string GetWritePerfstackLink(string volumeID, string site)
    {
        return string.Format(@"{2}/ui/perfstack/?presetTime=Today&charts={1}_Orion.Volumes_{0}-Orion.VolumePerformanceHistory.AvgDiskWrites;", volumeID, 0, FederationUrlHelper.GetLinkPrefix(site));
    }

    public string GetReadWritePerfstackLink(string volumeID, string site)
    {
        return string.Format(@"{2}/ui/perfstack/?presetTime=Today&charts={1}_Orion.Volumes_{0}-Orion.VolumePerformanceHistory.AvgDiskReads,{1}_Orion.Volumes_{0}-Orion.VolumePerformanceHistory.AvgDiskWrites;", volumeID, 0, FederationUrlHelper.GetLinkPrefix(site));
    }
}
