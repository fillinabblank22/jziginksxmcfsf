﻿using System;
using System.Web;

using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Charting;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_TopXX_LastXXAuditEvents : TopXXResourceControl
{
	private readonly Log _log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
	private string _selectedTypes;
	private string _netObject = string.Empty;

	protected string SelectedTypes
	{
		get { return _selectedTypes; }
		set { _selectedTypes = value; }
	}

	protected string NetObject
	{
		get { return _netObject; }
		set { _netObject = value; }
	}

    // this function resolves netObjectType & netObjectID
    private void GetCurrentNetObjectID(out int nodeID, out string netObjectType, out int netObjectID)
    {
        netObjectType = String.Empty;
        netObjectID = -1;
        nodeID = -1;

        // if is netObject an Interface set filter for a interface
        IInterfaceProvider interfaceProvider = GetInterfaceInstance<IInterfaceProvider>();
        if (interfaceProvider != null)
        {
            netObjectType = "I";
            netObjectID = interfaceProvider.Interface.InterfaceID;
            nodeID = interfaceProvider.Interface.NodeID;
			return;
        }

        // if is netObject a Volume set filter for a volume
        IVolumeProvider volumeProvider = GetInterfaceInstance<IVolumeProvider>();
        if (volumeProvider != null)
        {
            netObjectType = "V";
            netObjectID = volumeProvider.Volume.VolumeID;
            nodeID = volumeProvider.Volume.NodeID;
			return;
        }

        // if is netObject a Node set filter for a node
        INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();
        if (nodeProvider != null)
        {
            netObjectType = "N";
            netObjectID = nodeProvider.Node.NodeID;
            nodeID = nodeProvider.Node.NodeID;
            return;
        }

        //if no providers and netObject has not been defined
        //try to get netobject info from request
		string queryStringID = Request.QueryString["NetObject"];
		if (netObjectID == -1 && !string.IsNullOrEmpty(queryStringID))
        {
            try
            {
                queryStringID = queryStringID.Trim();

				NetObject netObject = NetObjectFactory.Create(queryStringID);

				if (netObject.Parent != null && netObject.Parent.ObjectProperties != null && netObject.Parent.ObjectProperties.ContainsKey("NodeID"))
					nodeID = (int) netObject.Parent.ObjectProperties["NodeID"];

				string[] args = queryStringID.Split(':');
				netObjectType = args[0];
				netObjectID = Convert.ToInt32(args[1]);
            }
            catch(Exception ex)
            {
                //incorrect Netobject ID
				_log.Error("Error occured during parsing objectID " + queryStringID, ex);
            }
        }
    }


	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		if (!String.IsNullOrEmpty(this.Resource.Properties["SelectedTypes"]))
		{
			SelectedTypes = this.Resource.Properties["SelectedTypes"];
		}

		if (!String.IsNullOrEmpty(Request["NetObject"]))
		{
			NetObject = Request["NetObject"];
		}
	}

    protected void Page_Load(object sender, EventArgs e)
    {
		// this resource is hidden for non admin users (except for demo)
        if (!(Profile.AllowAdmin || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer))
		{
			Visible = false;
			return;
		}
		Wrapper.ShowEditButton = EnableEdit;
        if (String.IsNullOrEmpty(Resource.Properties["Period"]))
            Resource.Properties["Period"] = "Last 30 Days";

        DateTime periodBegin = DateTime.UtcNow;
        DateTime periodEnd = DateTime.UtcNow;
        string periodName = Resource.Properties["Period"];
        Periods.Parse(ref periodName, ref periodBegin, ref periodEnd);

		Wrapper.ManageButtonText = Resources.CoreWebContent.WEBDATA_IB0_155;
		Wrapper.ManageButtonTarget = string.Format(
			"/Orion/NetPerfMon/OrionMessages.aspx?ShowOrionMessageTypes={0}", HttpUtility.UrlEncode("audit;"));
		Wrapper.ShowManageButton = true;

        // set filter args for netobject if necessary
        string netObjectType;
        int netObjectID;
        int nodeID;
        GetCurrentNetObjectID(out nodeID, out netObjectType, out netObjectID);

        using (var proxy = _blProxyCreator.Create(ex => _log.Error(ex)))
		{
			resourceTable.DataSource = proxy.GetAuditingTable(
                MaxRecords, 
                netObjectID,
                netObjectType,
                nodeID,
                SelectedTypes,
                periodBegin.ToUniversalTime(),
                periodEnd.ToUniversalTime());
		}

		resourceTable.DataBind();
    }

	protected override string TitleTemplate {
		get {
			return Resources.CoreWebContent.WEBDATA_IB0_154;
		}
	}

    public override string SubTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(Resource.SubTitle.Trim()))
                return Resource.SubTitle;

            // if subtitle is not specified subtitle is a period
            string period = Resource.Properties["Period"];

            if (String.IsNullOrEmpty(period))
                return String.Empty;

            return Periods.GetLocalizedPeriod(period);
        }
    }

	public override string EditURL
	{
		get
		{
            return base.EditURL + "&PropertiesMode=True";
		}
	}

	public override string EditControlLocation
	{
		get
		{
			return "/Orion/NetPerfMon/Controls/EditResourceControls/EditLastXXAuditEvents.ascx"; 
		}
	}

	public override string HelpLinkFragment
	{
		get
		{
			return "OrionCorePHResourceLastXXAuditEvents";
		}
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
