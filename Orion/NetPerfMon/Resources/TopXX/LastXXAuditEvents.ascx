<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LastXXAuditEvents.ascx.cs"
    Inherits="Orion_NetPerfMon_Resources_TopXX_LastXXAuditEvents" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>


<orion:Include ID="Include1" runat="server" File="auditevent.css" />
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:Repeater runat="server" ID="resourceTable">
            <HeaderTemplate>
                <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%">
                    <tr>
                        <td class="ReportHeader" style="padding-left: 10px;">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_399) %>&nbsp;
                        </td>
                        <td class="ReportHeader" colspan="2">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_152) %>&nbsp;
                        </td>
                        <td class="ReportHeader">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_153) %>&nbsp;
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td class="Property AuditEventMessage" style="padding-left: 10px;">
                        <%# DefaultSanitizer.SanitizeHtml(((DateTime)Eval("DateTime")).ToLocalTime()) %>&nbsp;
                    </td>
                    <td class="Property AuditEventMessage" valign="middle" width="20">
                        <img src="/NetPerfMon/images/audit_event.gif" alt="">&nbsp;
                    </td>
                    <td class="Property AuditEventMessage">
                        <a href="<%# DefaultSanitizer.SanitizeHtml(string.Format("/Orion/NetPerfMon/OrionMessages.aspx?ShowOrionMessageTypes={0}&AuditUser={1}",
                            HttpUtility.UrlEncode("audit;"), HttpUtility.UrlEncode(Eval("AccountID").ToString()))) %>">
                        <%# DefaultSanitizer.SanitizeHtml(Eval("AccountID")) %></a>&nbsp;
                    </td>
                    <td class="Property AuditEventMessage">
                        <%# WebSecurityHelper.SanitizeHtmlV2(Eval("Message").ToString())%>&nbsp;
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>
