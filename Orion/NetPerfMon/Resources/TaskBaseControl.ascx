﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TaskBaseControl.ascx.cs" Inherits="Orion_Core_Controls_TaskBaseControl" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>

<orion:Include runat="server" File="WSAsyncExecuteTasks.js" />

<div style="float:right;" runat="server" id="TaskDiv">
    <table cellpadding="0" cellspacing="0" style="width: auto;">
        <tr>
            <td style="padding: 0px; border: 0px;"><span runat="server" id="loadingSpan" style="display: none;"><img src="/Orion/images/AJAX-Loader.gif" alt="Loading..." /><asp:Literal ID="lblLoading" runat="server" /></span></td>
            <td style="padding: 0px; border: 0px;"><span runat="server" id="refreshSpan"><orion:LocalizableButtonLink ID="btnRefresh" runat="server" DisplayType="Resource" CssClass="EditResourceButton" /></span></td>
        </tr>
    </table>
</div>

<script type="text/javascript" language="javascript">
    function <%= this.ClientID %>_RefreshClick()
    {
        SW.Core.WSAsyncExecuteTasks.LoadResource({ "Loader": "<%= this.loadingSpan.ClientID %>", "MessageContainer": "<%= this._MessageContainer %>", "ContentContainer": "<%= this._ContentContainer %>" }
                                    , <%= this._RefreshExecutionList %>);
    }
        
    $(function() {
        var refresh = function () {
            SW.Core.WSAsyncExecuteTasks.LoadResource({ "Loader": "<%= this.loadingSpan.ClientID %>", "MessageContainer": "<%= this._MessageContainer %>", "ContentContainer": "<%= this._ContentContainer %>" }, <%= this._ExecutionList %>);
        };
        SW.Core.View.AddOnRefresh(refresh, '<%= this._ContentContainer %>');
        refresh();
    });
</script>