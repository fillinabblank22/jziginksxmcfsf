<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" Title="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_271 %>" 
    CodeFile="NodeSearchResults.aspx.cs" Inherits="Orion_NetPerfMon_Resources_NodeSearchResults" %>
<%@ Register TagPrefix="orion" TagName="SmallNodeStatus" Src="~/Orion/Controls/SmallNodeStatus.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:PlaceHolder ID="phMessage" runat="server" Visible="false">
        <div class="StatusMessage DefaultShading">
            <%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBCODE_AK0_95, GetNodePropertyDisplayName(Request.QueryString["Property"]), SolarWinds.Orion.Web.Helpers.WebSecurityHelper.SanitizeAngular(HttpUtility.HtmlEncode(Request.QueryString["SearchText"])))) %>
        </div>
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="phNoNodesMessage" runat="server" Visible="false">
        <div class="StatusMessage DefaultShading">
            <%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBCODE_AK0_96, GetNodePropertyDisplayName(Request.QueryString["Property"]), SolarWinds.Orion.Web.Helpers.WebSecurityHelper.SanitizeAngular(HttpUtility.HtmlEncode(Request.QueryString["SearchText"])))) %>
        </div>
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="phInvalidURLMessage" runat="server" Visible="false">
        <div class="StatusMessage DefaultShading">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_270) %>
        </div>
    </asp:PlaceHolder>
    
    <asp:Repeater ID="rptNodes" runat="server">
		<HeaderTemplate>
			<table style="margin-left:24px">
		</HeaderTemplate>
        <ItemTemplate>
                <tr style="font-weight:<%# nodeProp == "IPAddress" && IsPrimaryIP(Eval("NodeID"), Eval("IPAddress")) ? "bold" : "normal" %>">
				<td>
                    <img class="StatusIcon" alt="" src="/Orion/StatusIcon.ashx?<%# DefaultSanitizer.SanitizeHtml(GetStatusIconQueryString()) %>">
                    <a href='/Orion/NetPerfMon/NodeDetails.aspx?NetObject=<%# DefaultSanitizer.SanitizeHtml(Eval("NetObjectID")) %>'><%# DefaultSanitizer.SanitizeHtml(Eval("Name")) %></a>
				</td>
				<td width="15"></td>
				<td>
                    <%# DefaultSanitizer.SanitizeHtml(GetNodeProperties()) %>
				</td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
			</table>
        </FooterTemplate>
    </asp:Repeater>
</asp:Content>

