﻿#region USING
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.NPM.Common;
using SolarWinds.NPM.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Helpers;

#endregion

public partial class Orion_NetPerfMon_Resources_TitleEdit : System.Web.UI.Page
{

    // ========== PUBLIC ======== //

    #region public void SubmitClick(object sender, EventArgs e)
    public void SubmitClick(object sender, EventArgs e)
    {
        // get resource info
        int resourceID = 0;
        Int32.TryParse(Request.QueryString["ResourceID"], out resourceID);
        Resource = ResourceManager.GetResourceByID(resourceID);

        // store resources
        if (this.Resource == null) return;
        if (!String.IsNullOrEmpty(TitleEditControl.ResourceTitle))
        {
            Resource.Title = TitleEditControl.ResourceTitle;
        }
        else
        {
            var resourceControl = (BaseResourceControl)LoadControl(Resource.File);
            Resource.Title = resourceControl.Title;
        }
        Resource.SubTitle = this.TitleEditControl.ResourceSubTitle;

        SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(this.Resource);

        // redirect backward
        string netObjectID = Request.QueryString["NetObject"];
        string url = string.Format("/Orion/View.aspx?ViewID={0}", Request.QueryString["ViewID"]);
        
        if (!string.IsNullOrEmpty(netObjectID))
        {
            url = string.Format("{0}&NetObject={1}", url, this.Request.QueryString["NetObject"]);
        }

        this.Response.Redirect(url);
    }
    #endregion


    #region PROPERTIES

    #region public ResourceInfo Resource
    public ResourceInfo Resource
    {
        get;
        set;
    }
    #endregion

    #endregion


    // ========== PROTECTED ======== //

    #region protected void Page_Load(object sender, EventArgs e)
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            // get resource info
            int resourceID = 0;
            Int32.TryParse(Request.QueryString["ResourceID"], out resourceID);
            this.Resource = ResourceManager.GetResourceByID(resourceID);

            this.TitleEditControl.ResourceTitle = Resource.Title;
            this.TitleEditControl.ResourceSubTitle = Resource.SubTitle;
            this.ResourceName.Text = WebSecurityHelper.HtmlEncode(Resource.Name);
            this.Title = String.Format(Resources.CoreWebContent.WEBCODE_AK0_88, this.Resource.Title);
        }
    }
    #endregion

}
