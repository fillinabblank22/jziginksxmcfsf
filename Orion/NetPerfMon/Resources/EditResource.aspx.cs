﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Helpers;
using System.Text;

public partial class Orion_NetPerfMon_Resources_EditResource : System.Web.UI.Page
{
    private Control _editControl;

    public ResourceInfo Resource
    {
        get;
        set;
    }

    public string NetObjectID
    {
        get;
        set;
    }

    public int ViewID
    {
        get;
        set;
    }

    public bool PropertiesMode
    {
        get;
        set;
    }

    public void SubmitClick(object sender, EventArgs e)
    {
        if (Resource == null)
            return;

        Page.Validate();
        if (!Page.IsValid)
            return;

        string resTitle = TitleEditControl.ResourceTitle;
        string resSubTitle = TitleEditControl.ResourceSubTitle;

        if (_editControl != null)
        {
            foreach (KeyValuePair<string, object> property in (_editControl as BaseResourceEditControl).Properties)
            {
                if (Resource.Properties.ContainsKey(property.Key))
                    Resource.Properties[property.Key] = property.Value.ToString();
                else
                    Resource.Properties.Add(property.Key, property.Value.ToString());
            }

            if (string.IsNullOrEmpty(resTitle) && !string.IsNullOrEmpty((_editControl as BaseResourceEditControl).DefaultResourceTitle))
                resTitle = (_editControl as BaseResourceEditControl).DefaultResourceTitle;
            if (string.IsNullOrEmpty(resSubTitle) && !string.IsNullOrEmpty((_editControl as BaseResourceEditControl).DefaultResourceSubTitle))
                resSubTitle = (_editControl as BaseResourceEditControl).DefaultResourceSubTitle;
        }

        // if this is properties mode than we need to save titles into resource properties
        if (PropertiesMode)
        {
            if (resTitle.Equals(((BaseResourceControl) LoadControl(Resource.File)).Title, StringComparison.CurrentCulture))
            {
                Resource.Properties["Title"] = string.Empty; // If default title is set, erase the customized one
            }
            else
            {
                Resource.Properties["Title"] = resTitle;
            }

            Resource.Properties["SubTitle"] = resSubTitle;
        }
        else
        {
            if (String.IsNullOrEmpty(resTitle))
            {
                var resourceControl = (BaseResourceControl)LoadControl(Resource.File); //The original localized title was destroyed. Let's fetch the control's default.
                Resource.Title = resourceControl.Title;
            }
            else
                Resource.Title = resTitle;

            Resource.SubTitle = resSubTitle;
            ResourcesDAL.Update(this.Resource);
        }

        this.Response.Redirect(
            BaseResourceControl.GetEditPageReturnUrl(
                Request.QueryString,    
                ((ViewID > 0) ? ViewID : Resource.View.ViewID).ToString()));
    }

    private string GetCustomObjectId()
    {
        if (!string.IsNullOrEmpty(Resource.Properties["ObjectFilter"])
                && !string.IsNullOrEmpty(Resource.Properties["ObjectUri"]))
        {
            string filter = Resource.Properties["ObjectFilter"];
            if (!string.IsNullOrEmpty(filter) && filter.Split('$').Length == 3)
                return UriHelper.GetNetObjectId(Resource.Properties["ObjectUri"], filter.Split('$')[0]); ;
        }

        return string.Empty;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        int resourceID = 0;
        Int32.TryParse(Request.QueryString["ResourceID"], out resourceID);
        Resource = ResourceManager.GetResourceByID(resourceID);

        int viewId = 0;
        Int32.TryParse(Request.QueryString["ViewID"], out viewId);
        ViewID = viewId;

        NetObjectID = Request.QueryString["NetObject"];

        // try to get object id from resource properties in case of custom object resource
        if (string.IsNullOrEmpty(NetObjectID))
            NetObjectID = GetCustomObjectId();



        bool isPropertiesMode;
        if (bool.TryParse(Request.QueryString["PropertiesMode"], out isPropertiesMode))
        {
            PropertiesMode = isPropertiesMode;
        }

        LoadResourceEditControl();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Resource != null)
        {
            if (!IsPostBack)
            {
                TitleEditControl.ResourceTitle = Resource.Title;
                TitleEditControl.ResourceSubTitle = Resource.SubTitle;   
                ResourceName.Text = WebSecurityHelper.HtmlEncode(Resource.Title);
            }
            Title = String.Format(Resources.CoreWebContent.WEBCODE_AK0_88, Resource.Title);
        }
    }

    private void LoadResourceEditControl()
    {
        if (Resource == null)
            return;
        Control control = LoadControl(Resource.File);
        if (control is BaseResourceControl)
        {
            string editControlLocation = (control as BaseResourceControl).EditControlLocation;
            control.Dispose();

            if (string.IsNullOrEmpty(editControlLocation))
            {
                var embeddedPath = Resource.Properties["EmbeddedObjectResource"];
                if (!string.IsNullOrEmpty(embeddedPath))
                {
                    var embeddedData = embeddedPath.Split(';');
                    Control embeddedControl = null;

                    try
                    {
                        embeddedControl = LoadControl(embeddedData[0]);
                        embeddedControl.ID = "ObjectResourceControl";
                        if (embeddedControl is BaseResourceControl)
                        {
                            editControlLocation = (embeddedControl as BaseResourceControl).EditControlLocation;
                        }
                    }
                    finally
                    {
                        if (embeddedControl != null)
                        {
                            embeddedControl.Dispose();
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(editControlLocation))
            {
                _editControl = LoadControl(editControlLocation);

                if (_editControl is BaseResourceEditControl)
                {
                    TitleEditControl.ShowSubTitle = (_editControl as BaseResourceEditControl).ShowSubTitle;
                    TitleEditControl.ShowSubTitleHintMessage = (_editControl as BaseResourceEditControl).ShowSubTitleHintMessage;
                    TitleEditControl.SubTitleHintMessage = (_editControl as BaseResourceEditControl).SubTitleHintMessage;
                    (_editControl as BaseResourceEditControl).Resource = Resource;
                    (_editControl as BaseResourceEditControl).NetObjectID = NetObjectID;
                    phResourceEditControl.Controls.Add(_editControl.TemplateControl);
                    foreach (Control button in (_editControl as BaseResourceEditControl).ButtonsForButtonBar)
                    {
                        phAdditionalButtons.Controls.Add(button);
                    }
                }
            }
        }
    }
}

