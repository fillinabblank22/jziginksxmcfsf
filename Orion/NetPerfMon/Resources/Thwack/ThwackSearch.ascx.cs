using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Help)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsHelpValues.Assist)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsHelpValues.Assistance)]
public partial class ThwackSearch : BaseResourceControl
{
	protected void Page_Load(object sender, EventArgs e)
	{
		btnSearch.Attributes["onclick"] = "window.open(\"https://thwack.com/search.jspa?q=\" + escape(document.getElementById(\"SearchSentence\").value) + \"&o=Relevance\"); return false;";
		SearchOptions.Attributes["onclick"] = "window.open(\"https://thwack.com/search/\"); return false;";
	}

	protected override string DefaultTitle
	{
		get { return Resources.CoreWebContent.WEBCODE_AK0_122; }
	}

	public override string HelpLinkFragment
	{
		get
		{
			return "OrionPHResourceSearchThwackCom";
		}
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Static; }
    }
}
