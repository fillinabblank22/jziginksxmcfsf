<%@ Control Language="C#" CodeFile="ThwackSearch.ascx.cs" Inherits="ThwackSearch" %>
<orion:resourceWrapper runat="server" ID="resourceWrapper" ShowEditButton="false">
    <Content>
        <asp:Panel DefaultButton="btnSearch" runat="server">
            <table class="HeaderBar" style="width: 100%;">
                <tr >
                    <td style="width: 60%; border-bottom-width: 0px; padding:1px;">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_178) %>
                    </td>
                    <td style="border-bottom-width: 0px; padding:1px;">
                        &nbsp;
                    </td>
                    <td style="border-bottom-width: 0px; padding:1px;">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom-width: 0px; padding:1px;">
                        <input id="SearchSentence" style="width:95%" />
                    </td>
                    <td style="border-bottom-width: 0px; padding:1px;">
                        <orion:LocalizableButton id="btnSearch" runat="server" DisplayType="Secondary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_VB0_131 %>"/>
                    </td>
                    <td style="border-bottom-width: 0px; padding:1px;">
                        &nbsp;
                    </td>
                </tr>
                <tr >
                    <td style="border-bottom-width: 0px; padding:1px;">
                        &nbsp;
                    </td>
                    <td style="border-bottom-width: 0px; padding:1px;" colspan="2">
                        <span>
                            <span style="font-size: 10px; vertical-align: text-top;">&raquo;</span> <span><asp:LinkButton runat="server" ID="SearchOptions"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_377) %></asp:LinkButton></span>
                        </span>                            
                    </td>
                </tr>
            </table>
        </asp:Panel>                
    </Content>
</orion:resourceWrapper>
