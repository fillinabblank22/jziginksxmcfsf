﻿using System;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_NetPerfMon_Resources_EditSysLogSummary : System.Web.UI.Page
{
    public ResourceInfo Resource { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            // get resource info
            int resourceID = 0;
            Int32.TryParse(Request.QueryString["ResourceID"], out resourceID);
            this.Resource = ResourceManager.GetResourceByID(resourceID);

            this.GroupItems.Items.Add(new ListItem(Resources.CoreWebContent.WEBCODE_AK0_90, "Hostname"));
            this.GroupItems.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_VB0_13, "IPAddress"));
            this.GroupItems.Items.Add(new ListItem(Resources.CoreWebContent.WEBCODE_AK0_91, "MessageType"));
            this.GroupItems.Items.Add(new ListItem(Resources.CoreWebContent.WEBCODE_AK0_92, "FacilityName"));
            this.GroupItems.Items.Add(new ListItem(Resources.CoreWebContent.WEBCODE_AK0_93, "SeverityName"));
            this.GroupItems.Items.Add(new ListItem(Resources.CoreWebContent.WEBCODE_AK0_94, "SyslogTag"));

            this.GroupItems.SelectedValue = Resource.Properties["GroupBy"];
            
            // fill in period list
            foreach (string period in Periods.GenerateSelectionList())
                Period.Items.Add(new ListItem(Periods.GetLocalizedPeriod(period), period));
            this.Period.Text = Resource.Properties["Period"];

            this.TitleEditControl.ResourceTitle = Resource.Title;
            this.TitleEditControl.ResourceSubTitle = Resource.SubTitle;
        }
    }

    public void SubmitClick(object sender, EventArgs e)
    {
        // get resource info
        int resourceID = 0;
        Int32.TryParse(Request.QueryString["ResourceID"], out resourceID);
        Resource = ResourceManager.GetResourceByID(resourceID);

        // store resources
        if (this.Resource == null)
			return;
        if (!String.IsNullOrEmpty(TitleEditControl.ResourceTitle))
        {
            Resource.Title = TitleEditControl.ResourceTitle;
        }
        else
        {
            var resourceControl = (BaseResourceControl)LoadControl(Resource.File);
            Resource.Title = resourceControl.Title;
        }
        Resource.SubTitle = this.TitleEditControl.ResourceSubTitle;
        Resource.Properties["Period"] = this.Period.Text;
        Resource.Properties["GroupBy"] = GroupItems.SelectedValue;
        Resource.Properties["GroupByLocalized"] = GroupItems.SelectedItem.Text;
        SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(this.Resource);

        // redirect backward
        Response.Redirect(BaseResourceControl.GetEditPageReturnUrl(Request.QueryString));
    }
}
