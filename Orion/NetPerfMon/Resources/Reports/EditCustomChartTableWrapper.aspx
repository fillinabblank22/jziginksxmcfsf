<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true" CodeFile="EditCustomChartTableWrapper.aspx.cs" Inherits="Orion_NetPerfMon_Resources_Reports_EditTableWrapper" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<%@ Register TagPrefix="orion" TagName="DataSourcePicker" Src="~/Orion/Controls/DataSourcePicker.ascx" %>
<%@ Register TagPrefix="orion" TagName="TimePeriods" Src="~/Orion/Reports/Controls/TimePeriodsControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditReportTableWrapper" Src="~/Orion/NetPerfMon/Controls/EditResourceControls/EditReportTableWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditReportChartWrapper" Src="~/Orion/NetPerfMon/Controls/EditResourceControls/EditReportChartWrapper.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadPlaceHolder" Runat="Server">
    <orion:Include ID="Include2" File="AddContentLoader.css" runat="server" />
    <orion:Include ID="Include3" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include ID="Include4" runat="server" File="OrionCore.js" />
    <orion:Include runat="server" File="Reports/js/ReportPicker.js" />
    <orion:Include runat="server" File="Admin/js/SearchField.js" />
    <orion:Include File="ReportSchedule.css" runat="server" />
    <style type="text/css">
        div.sw-res-editor p {
            margin: 13px 0px 0px 0px;
        }

        .cbDynamicDatasource input {
            padding: 3px 3px 3px 3px;
        }

        .sw-btn-disabled,
        .sw-btn-disabled:hover {
            background-color: #ABD5E3 !important;
            border-color: #ABD5E3 !important;
            color: #FFFFFF !important;
        }

        .sw-disabled img {
            pointer-events: none;
        }

        .x-grid3-cell-inner a, .x-grid3-cell-inner a:active, .x-grid3-cell-inner a:hover {
            color: #0079aa;
        }
        
		#selectDataSource tr {
            padding-left: 5px;
        }
		
        #selectDataSource div.sw-suggestion {
            margin: 5px 0; 
        }
		
		#selectDataSource div.sw-suggestion-fail {
            padding-left: 5px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1 style="font-size: large; padding-top: 15px; padding-bottom: 10px;"><%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBCODE_AK0_88, CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Resource.Title, Request["netobject"])))) %></h1>
    <orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="true"/>

    <div id="selectReportDialog" style="display: none;">
        <div id="headerTitle" tabindex="1" style="margin:10px; font-size:16px;"><%= DefaultSanitizer.SanitizeHtml(IsTableResource() ?  Resources.CoreWebContent.WEBDATA_IB0_242 : Resources.CoreWebContent.WEBDATA_IB0_243) %></div>
        <div id="searchPanel" style="float: left; width: 100%;"></div>
        <div style="clear: both;"></div>
        <div id="sheduleReportsPicker"></div>
        <label class="selectedSchedulesLabel"></label>
        <div id='selectedObjects' class='selectedObjectsCSS'></div>
        <label><b><%= DefaultSanitizer.SanitizeHtml(IsTableResource() ?  Resources.CoreWebContent.WEBDATA_IB0_244 : Resources.CoreWebContent.WEBDATA_IB0_245) %></b></label>
        <div id="configLoagingMask" style="display:none;">
            <img src="/Orion/images/AJAX-Loader.gif" alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_213) %>"/>
            <span style="vertical-align:top;""><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_213) %></span>
        </div>
        <div id="ddConfigData"></div>
    </div>

    <div id = "resourceDataSourcePickerBox" style="display: none;">
        <div id="reportContentDialog" class="resourcePickerLoader offPage">
            <orion:DataSourcePicker runat="server" ID="DataSourcePicker" />        
        </div>
        <div class="bottom">
            <div class="sw-btn-bar-wizard" id="updateDataSource"></div>
        </div>
    </div>
    <asp:HiddenField ID="tbDataSource" runat="server" />
    <asp:HiddenField ID="tbReportConfig" runat="server" />
    
    <orion:TimePeriods runat="server" ID="timePeriods" Visible="false"/>

    <table id="selectDataSource">
        <tr>
            <td><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_180) %></b></td>
        </tr>
        <tr runat="server" ID="dynamicDatasourcePh">
            <td>
                <asp:CheckBox runat="server" ID="useDynamicDatasource" AutoPostBack="True" OnCheckedChanged="useDynamicDatasource_CheckedChanged" CssClass="cbDynamicDatasource" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_186 %>" />
            </td>
        </tr>
        <tr runat="server" ID="datasourcePh">
            <td>
                <span runat="server" id="dataSourceName"></span>&nbsp;
                <orion:LocalizableButton ID="SpecifyDatasource" runat="server" DisplayType="Secondary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_126 %>" OnClick="SpecifyDatasource_Click" OnClientClick="showDialog(); return false;"/>
                <span runat="server" id="orSpan"><i>&nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_367) %>&nbsp;</i></span>
                <orion:LocalizableButton ID="SelectConfigData" runat="server" DisplayType="Secondary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_246 %>" OnClick="SelectConfigData_Click" OnClientClick="SW.Orion.ReportPicker.selectReportDialog(); return false;" />
            </td>
        </tr>
        <tr runat="server" ID="datasourceErrorMsg" Visible="false">
            <td>
                <div class="sw-suggestion sw-suggestion-fail"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_DatasourceErrorMsg) %></div>
            </td>
        </tr>
        <tr runat="server" ID="sqlWarning">
            <td>
                <div class="sw-suggestion sw-suggestion-warn">
			        <span class="sw-suggestion-icon"></span>
			        <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_249) %></span>
		        </div>
            </td>
        </tr>
        <tr runat="server" ID="timePeriod">
            <td><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_311) %></b>&nbsp;</td>
        </tr>
        <tr>
            <td><div id="timeFrame_PickerControl"></div></td>
        </tr>
    </table>

        <asp:PlaceHolder ID="DynamicControlsHolder" runat="server"></asp:PlaceHolder>
        <div class="sw-btn-bar">    
            <orion:LocalizableButton LocalizedText="Save" DisplayType="Primary" runat="server" ID="btnSubmit" onclick="btnSubmit_Click" />
            <orion:LocalizableButton LocalizedText="Cancel" DisplayType="Secondary" runat="server" onclick="btnCancel_Click" CausesValidation="False" />
            <asp:PlaceHolder ID="phAdditionalButtons" runat="server"></asp:PlaceHolder>
        </div>
    <script type="text/javascript">
        var tdDatasourceID = function () {
            return "<%=tbDataSource.ClientID%>";
        };
        var SpecifyDatasourceID = function () {
            return "<%=SpecifyDatasource.UniqueID%>";
        };
        var getResourceType = function () {
            return "<%= IsTableResource() ? "tableconfiguration" : "chartconfiguration" %>";
        };


        $(function () {
            $('#timeFrame_PickerControl').timePeriods.settings.controlId = 'timeFrame_PickerControl';
            $('#timeFrame_PickerControl').timePeriods.syncTimeFrameIdDelegate = function () { };
            $('#timeFrame_PickerControl').timePeriods('InitTimeFrames', '<%=timePeriods.TimeFrame.RefId%>');
        });

        var configDataStore = new ORION.WebServiceStore(
            "/Orion/Services/ReportManager.asmx/GetReportCells",
            [
                { name: 'Value', mapping: 1 },
                { name: 'Name', mapping: 0 }
            ]);

        var configComboArray = new Ext.form.ComboBox(
            {
                id: 'configCombo', mode: 'local', width: 300, fieldLabel: 'Name', displayField: 'Name', valueField: 'Value', store: configDataStore,
                triggerAction: 'all', typeAhead: true, forceSelection: true, disabled: true, multiSelect: false, editable: false,
                emptyText: "<%= IsTableResource() ?  Resources.CoreWebContent.WEBDATA_IB0_250 : Resources.CoreWebContent.WEBDATA_IB0_251 %>"
            });

        var onCloseReportDialog = function () {
            configComboArray.getStore().removeAll();
            configComboArray.setValue("");
        };

        SW.Orion.ReportPicker.onSelectionChanged = function (items) {
            var repId = (items.length > 0) ? items[0][0] : 0;
            configComboArray.getStore().removeAll();
            configComboArray.setValue("");
            $("#configLoagingMask").show();
            $("#ddConfigData").hide();
            $("#selectedObjects").addClass('sw-disabled');
            $("#useSelectedReportsBtn").addClass('sw-btn-disabled');
            configComboArray.store.proxy.conn.jsonData = { reportId: repId, resourceType: getResourceType() };
            configComboArray.store.load({
                callback: function () {
                    if (configComboArray.getStore().getAt(0)) {
                        configComboArray.setValue(configComboArray.getStore().getAt(0).data.Value);
                        $("#useSelectedReportsBtn").removeClass('sw-btn-disabled');
                        $("#selectedObjects").removeClass('sw-disabled');
                        configComboArray.setDisabled(false);
                    } else {
                        $("#useSelectedReportsBtn").addClass('sw-btn-disabled');
                        $("#selectedObjects").removeClass('sw-disabled');
                        configComboArray.setDisabled(true);
                    }
                    $("#configLoagingMask").hide();
                    $("#ddConfigData").show();
                }
            });
        };

        SW.Orion.ReportPicker.selectReportDialog = function () {
            $("#selectReportDialog").dialog({
                modal: true, draggable: true, resizable: false, position: ['center', 'top'], width: 1000, close: onCloseReportDialog,
                title: "<%= IsTableResource() ?  Resources.CoreWebContent.WEBDATA_IB0_240 : Resources.CoreWebContent.WEBDATA_IB0_241 %>", dialogClass: 'ui-dialog-osx'
            }).show();


            if ($("#sheduleReportsPicker").children().length == 0) {
                SW.Orion.ReportPicker.init({
                    SwisBasedReportsOnly: <%= SolarWinds.Orion.Core.Common.Swis.SwisFederationInfo.IsFederationEnabled.ToString().ToLower() %>,
                    NoReportsSelectedMessage: String.format("<span style='color:gray;'>{0}</span>", "<%= IsTableResource() ?  Resources.CoreWebContent.WEBDATA_IB0_238 : Resources.CoreWebContent.WEBDATA_IB0_239 %>"),
                    IsSingleSelectionMode: true,
                    resourceFilter: '<%= IsTableResource() ? "tableconfiguration" : "chartconfiguration" %>'
                });
                $(SW.Core.Widgets.Button('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_28) %>', { type: 'secondary', id: 'cancelBtn' })).appendTo("#selectReportDialog").css('float', 'right').css('margin', '10px').click(function () {
                    $("#selectReportDialog").dialog("close");
                });

                configComboArray.render('ddConfigData');

                $(SW.Core.Widgets.Button('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_237) %>', { type: 'primary', id: 'useSelectedReportsBtn' })).appendTo("#selectReportDialog").css('float', 'right').css('margin', '10px').click(function () {
                    var pickedReports = SW.Orion.ReportPicker.getSelectedReports();
                    var configId = configComboArray.getValue();
                    $("#selectReportDialog").dialog("close");
                    $("#<%=tbReportConfig.ClientID%>").val(pickedReports[0].ID + "$" + configId);
                    __doPostBack("<%=SelectConfigData.UniqueID%>", '');
                });
            }

            $("#useSelectedReportsBtn").addClass('sw-btn-disabled');
            SW.Orion.ReportPicker.clearSelection();

        };

        var showDialog = function () {
            ORION.callWebService('/Orion/Services/ReportManager.asmx', 'LoadResourceConfigOptions', { location: '<%=Resource.File%>' },
                function (result) {
                    if (result != null) {
                        var dataSource = $("#" + tdDatasourceID()).val();
                        if (Ext.isEmpty(dataSource)) {
                            dataSource = { RefId: emptyGuid, Name: '', Type: 4, CommandText: '', MasterEntity: 'Orion.Nodes', Filter: null, EntityUri: [] };
                        } else {
                            dataSource = JSON.parse(dataSource);
                            $.fn.dataSources.editDataSourceConfig = dataSource;
                        }
                        $.fn.dataSources.setUpForm('<%=Guid.NewGuid().ToString()%>', dataSource, result.SupportedInterfaces);

                        // HACK: When dialog is shown it executes script inside it again. It creates new Field picker instance (data stores).  
                        // Moving the scripts out of the markup of FieldPicker.ascx will fix the need for this hack.
                        $("#resourceDataSourcePickerBox").find("script").remove();

                        $("#resourceDataSourcePickerBox").dialog({
                            modal: true,
                            draggable: true,
                            resizable: false,
                            position: ['center', 'center'],
                            width: 900,
                            title: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_182) %>",
                            dialogClass: 'ui-dialog-osx'
                        });
                        $("#updateDataSource").empty();
                        $(SW.Core.Widgets.Button('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_183) %>', { type: 'primary', id: '#updateDataSourceBtn' })).appendTo("#updateDataSource").click(function () {
                            if ($.fn.dataSources.validate()) {
                                $("#" + tdDatasourceID()).val(JSON.stringify($.fn.dataSources.dataSourceData));
                                $(this).dialog("close");
                                __doPostBack(SpecifyDatasourceID(), '');
                            }
                        });
                        $("#resourceDataSourcePickerBox").css("height", "auto").css("background-color", "white");
                    }
                });
        };
    </script>
</asp:Content>

