﻿using System;
using System.Collections.Generic;
using System.Linq;

using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Reporting.DAL;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Reporting;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Reporting.Extensions;
using SolarWinds.Reporting.Impl.Rendering;
using SolarWinds.Reporting.Models.Data;
using SolarWinds.Reporting.Models.Layout;
using SolarWinds.Reporting.Models.Selection;
using SolarWinds.Reporting.Models.Tables;
using SolarWinds.Reporting.Models.Timing;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Reports)]
[ResourceMetadata("", CoreMetadataReportingValues.RecommendedForReporting)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.IsBlockedForViews, "false")]
[ResourceMetadata(StandardMetadataPropertyName.IsFederationAware, "true")]
public abstract partial class Orion_NetPerfMon_Resources_Reports_ReportTableResourceWrapper : BaseResourceControl, ISupportsFederation
{
    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[0]; }
    }

    public override IEnumerable<Type> SupportedInterfaces
    {
        get { return new[] { typeof(IAnyDataProvider) }; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.Misc_ReportTableWrapper_Title_1; }
    }
    
    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceCustomTable"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditReportTableWrapper.ascx"; }
    }

    public override string EditURL
    {
        get
        {
            string url = string.Format("/Orion/NetPerfMon/Resources/Reports/EditCustomChartTableWrapper.aspx?ResourceID={0}", this.Resource.ID);

            if (!string.IsNullOrEmpty(Request["NetObject"]))
                url = string.Format("{0}&NetObject={1}", url, Request["NetObject"]);
            return url;
        }
    }
    
    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    public override string ReportRenderProvider
    {
        get { return TableRenderer.Moniker; }
    }

    public override bool SupportDateTimeFilter
    {
        get { return true; }
    }

    public override string LayoutIconPath
    {
        get { return "Table"; }
    }

    public override string EditButtonText
    {
        get { return Resources.CoreWebContent.WEBCODE_TM0_118; }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        string configXml = GetStringValue(WebConstants.ConfigurationDataMoniker, string.Empty);

        if (string.IsNullOrEmpty(configXml))
        {
            InitializeDefaultText();
        }
        else
        {
            bool isDynamic = GetBooleanValue(WebConstants.DynamicDataSourceMoniker, false);
            string dataSourceXml = GetStringValue(WebConstants.DataSourceMoniker, string.Empty);
            string timeFrameXml = GetStringValue(WebConstants.TimeFrameMoniker, string.Empty);
            var config = SerializationHelper.FromXmlString<TableConfiguration>(configXml, Loader.GetKnownTypes());

			int maxRows = WebsiteSettings.ReportingTableViewMaxRowCount;

			if (config.Filter == null)
			{
				config.Filter = new Filter();
			}

			if (config.Filter.Limit == null)
			{
				config.Filter.Limit = new Limit { Mode = LimitMode.TopEntries, Count = maxRows };
			}

			if ((config.Filter != null) && (config.Filter.Limit != null) && (config.Filter.Limit.Mode != LimitMode.TopPercent) && (maxRows < config.Filter.Limit.Count || config.Filter.Limit.Mode == LimitMode.ShowAll))
			{
				config.Filter.Limit.Count = maxRows;
				config.Filter.Limit.Mode = LimitMode.TopEntries;
			}

            var timeFrame = SerializationHelper.FromXmlString<TimeFrame>(timeFrameXml, Loader.GetKnownTypes());
            var dataSource = ResourcePropertiesReportData.LoadDataSource(dataSourceXml, isDynamic);
            RenderTable(config, dataSource, timeFrame);
        }
    }

    private void RenderTable(TableConfiguration configuration, DataSource dataSource, TimeFrame timeFrame)
    {
        // todo: compute data source from net object or summary view.

        // int webResourcePadding = 20; // web resources like to steal pixels for wrapping whitespace.

        var rpt = configuration.ReportFromConfiguration(TableRenderer.Moniker, Resource.Width /* - webResourcePadding */ );

        SectionCell cell = rpt.Sections.First().Columns.First().Cells.First();

        if (dataSource != null)
        {
            cell.DataSelectionRefId = dataSource.RefId;
            cell.ConfigId = configuration.RefId;
            cell.TimeframeRefId = (configuration.TimeField != null) ? timeFrame.RefId : WebConstants.NotSupportedGuid;

            rpt.TimeFrames = new[] {timeFrame};
            rpt.DataSources = new[] { !dataSource.SupportsAdvancedFiltering() ? dataSource.GetDataSourceSigned() : dataSource };
            rpt.Configs = new ConfigurationData[] { configuration };
        }

        ReportTableContainer.Controls.Clear();
        ReportTableContainer.Controls.Add(new SolarWinds.Orion.Web.Reporting.ReportCellPreviewControl
        {
            Model = rpt,
            CellId = cell == null ? Guid.Empty : cell.RefId,
            AllowToolTips = true,
            ForcedMaxRows = WebsiteSettings.ReportingTableViewMaxRowCount,
            RenderAsResource = true
        });
    }

    private void InitializeDefaultText()
    {
        ReportTableContainer.Visible = true;
        ReportTableContainer.Controls.Clear();
        ReportTableContainer.Controls.Add(new WebResourceWantsEdit
        {
            HelpfulHtml = Resources.CoreWebContent.WEBCODE_PF0_6,
            EditUrl = Profile.AllowCustomize ? EditURL : null,
            WatermarkImage = "/Orion/images/ResourceWatermarks/Watermark.ReportFromOrionReportWriter.gif",
            WatermarkHeight = 113,
            WatermarkWidth = 196,
            WatermarkBackgroundColor = "#3E92C4"  //when resource gets resized, use this color for background of the rest of the image
        });
    }
}
