﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Reporting.DAL;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Reporting.ReportCharts;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Reporting.Models.Charts;
using SolarWinds.Reporting.Extensions;
using SolarWinds.Orion.Web.Reporting;
using SolarWinds.Reporting.Models.Selection;
using SolarWinds.Reporting.Models.Timing;
using SolarWinds.Reporting.Models.Data;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Reports)]
[ResourceMetadata("", CoreMetadataReportingValues.RecommendedForReporting)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.IsBlockedForViews, "false")]
[ResourceMetadata(StandardMetadataPropertyName.IsFederationAware, "true")]
public partial class Orion_NetPerfMon_Resources_Reports_ReportChartWrapper :  BaseResourceControl, ISupportsFederation
{

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[0]; }
    }

    public override IEnumerable<Type> SupportedInterfaces
    {
        get { return new[] { typeof(IAnyDataProvider) }; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.Misc_ReportChartWrapper_Title_1; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceCustomChart"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditReportChartWrapper.ascx"; }
    }

    public override string LayoutIconPath
    {
        get { return "Chart"; }
    }

    public override string EditButtonText
    {
        get { return Resources.CoreWebContent.WEBCODE_TM0_117; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override string ReportRenderProvider
    {
        get { return ReportChartRenderer.Moniker; }
    }

    public override bool SupportDateTimeFilter
    {
        get { return true; }
    }

    public override bool SupportRenderingAsImage
    {
        get { return true; }
    }

    public override string EditURL
    {
        get
        {
            string url = string.Format("/Orion/NetPerfMon/Resources/Reports/EditCustomChartTableWrapper.aspx?ResourceID={0}", this.Resource.ID);

            if (!string.IsNullOrEmpty(Request["NetObject"]))
                url = string.Format("{0}&NetObject={1}", url, Request["NetObject"]);
            return url;
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        string configXml = GetStringValue(WebConstants.ConfigurationDataMoniker, string.Empty);

        if (string.IsNullOrEmpty(configXml))
        {
            InitializeDefaultText();
        }
        else
        {
            bool isDynamic = GetBooleanValue(WebConstants.DynamicDataSourceMoniker, false);
            string dataSourceXml = GetStringValue(WebConstants.DataSourceMoniker, string.Empty);
            string timeFrameXml = GetStringValue(WebConstants.TimeFrameMoniker, string.Empty);
            var config = SerializationHelper.FromXmlString<ChartConfiguration>(configXml, Loader.GetKnownTypes());
            var timeFrame = SerializationHelper.FromXmlString<TimeFrame>(timeFrameXml, Loader.GetKnownTypes());
            var dataSource = ResourcePropertiesReportData.LoadDataSource(dataSourceXml, isDynamic);
           
            config.TooltipPositioningEnabled = this.Resource.ID > -1; // condition if it is placed on view not on standard report
            RenderChart(config, dataSource, timeFrame);
        }
    }

    private void RenderChart(ChartConfiguration configuration, DataSource dataSource, TimeFrame timeFrame)
    {
       // todo: compute data source from net object or summary view.
        var CHART_PADDING = 20;
        var reportWidth = Resource.Width - CHART_PADDING;
        
        var rpt = configuration.ReportFromConfiguration(ReportChartRenderer.Moniker, reportWidth);
        var cell = rpt.Sections.First().Columns.First().Cells.First();

        if (dataSource != null)
        {
            cell.DataSelectionRefId = dataSource.RefId;
            cell.ConfigId = configuration.RefId;
            cell.TimeframeRefId = timeFrame.RefId;

            rpt.TimeFrames = new[] { timeFrame };
            rpt.DataSources = new[] { !dataSource.SupportsAdvancedFiltering() ? dataSource.GetDataSourceSigned(): dataSource };
            rpt.Configs = new ConfigurationData[] { configuration };
        }
        
        ReportChartContainer.Controls.Clear();
        ReportChartContainer.Controls.Add(new ReportCellPreviewControl
        {
            Model = rpt,
            CellId = cell == null ? Guid.Empty : cell.RefId,
            AllowToolTips = true,
            ChartRenderingMode = ReportChartRenderingMode.Interactive
        });
    }

    private void InitializeDefaultText()
    {
        ReportChartContainer.Controls.Clear();
        ReportChartContainer.Controls.Add(new WebResourceWantsEdit
        {
            // "Graph one or many data series on a single chart."
            // "Customize the layout with line colors, format, and assigning data to left/right y-axis, if needed."
            HelpfulHtml = string.Format(@"<ul><li>{0}</li><li>{1}</li></ul>", Resources.CoreWebContent.WEBCODE_PF0_7, Resources.CoreWebContent.WEBCODE_PF0_8),
            EditUrl = Profile.AllowCustomize ? EditURL : null,
            WatermarkImage = "/Orion/images/ResourceWatermarks/Watermark.CustomNodeChart.gif",
            WatermarkBackgroundColor = "#f2f2f2"  //when resource gets resized, use this color for background of the rest of the image
        });
    }
}
