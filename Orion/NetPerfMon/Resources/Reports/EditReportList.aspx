<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true" CodeFile="EditReportList.aspx.cs" Inherits="Orion_NetPerfMon_Resources_Reports_EditReportList" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_201 %>" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<%@ Reference Page="~/Orion/Report.aspx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBCODE_AK0_88, CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Resource.Title, Request["netobject"])))) %></h1>
    
    <orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="true"/>
    
    <h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_184) %></h2>
    
    <asp:Repeater runat="server" ID="groupsRepeater" OnItemDataBound="groupsRepeater_ItemDataBound">
        <ItemTemplate>
            <h3><%# DefaultSanitizer.SanitizeHtml(Eval("Key")) %></h3>
            <asp:Repeater runat="server" ID="reportsRepeater">
                <ItemTemplate>
                    <input type="checkbox" name="Property-ReportListIDs" value="<%# DefaultSanitizer.SanitizeHtml(Eval("ReportId")) %>"
                           <%# IsSelected(Eval("ReportId")) ? "checked" : "" %> id="<%# DefaultSanitizer.SanitizeHtml(MakeControlID(Eval("ReportId"))) %>" />
                    <label for="<%# DefaultSanitizer.SanitizeHtml(MakeControlID(Eval("ReportId"))) %>"><%# DefaultSanitizer.SanitizeHtml(Eval("Title")) %></label>
                    <br />
                </ItemTemplate>
            </asp:Repeater>
        </ItemTemplate>
    </asp:Repeater>

    <div class="sw-btn-bar"><orion:LocalizableButton runat="server" ID="btnSubmit" OnClick="SubmitClick" DisplayType="Primary" LocalizedText="Submit" /></div>
</asp:Content>
