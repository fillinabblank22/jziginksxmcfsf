﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Reporting;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Reports)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsCustomResourcesValues.Configurable)]
public partial class Orion_NetPerfMon_Resources_Reports_ReportList : BaseResourceControl
{
    private IEnumerable<OrionReport> reports;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        //make sure NotConfiguredText has its content every time it is shown
        var emptyResource = string.Format(Resources.CoreWebContent.WEBCODE_VB0_332,
           String.Format("<ul><li><a href=\"{0}\"><font color=\"blue\"><b>", EditURL), "</b></font></a>", "</li><li>", "</li></ul>", "<span style=\"font-weight:bold;\">", "</span>");
        NotConfiguredText.Controls.Add(new WebResourceWantsEdit
        {
            HelpfulHtml = emptyResource,
            EditUrl = Profile.AllowCustomize ? EditURL : null,
            WatermarkImage =
                "/Orion/Images/ResourceWatermarks/Watermark.CustomListOfReports.gif",
            BackgroundPosition = "right center",
            WatermarkBackgroundColor = "#f2f2f2"
            // image has default watermark dimention
        });

        reports = OrionReport.LoadAllReports()
            .Where(rpt => rpt.AllowOnWeb);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string reportList = Resource.Properties["ReportList"] ?? "";

        string newReportList = Resource.Properties["ReportListIDs"] ?? "";

        if (string.IsNullOrEmpty(reportList) && string.IsNullOrEmpty(newReportList))
        {
            NotConfiguredText.Visible = true;
            return;
        }
        else
        {
            //Edited by FB24386
            //This beast does the following:
            //- parses the string, looking for Filename - Subtitle pairs
            //- creates an Ienumerable af an anynymous type{Filename, Subtitle} with these
            //- filters the enumerable to only contain elements for report files that actually exist on disk
            var newReportListIDs = newReportList.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(s => Convert.ToInt32(s));

            var oldReportsList = reportList.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(s => s.Split(new char[] { ':' }, 2))
                .Select(p => new
                {
                    ReportURL = "/Orion/Report.aspx?Report=" + p[0].Trim(),
                    Filename = p[0].Trim(),
                    Title = p[1].Trim(),
                    ReportId = 0
                }).Where(elem => reports.Select(s => s.Filename).Contains(elem.Filename));

            var newReports = reports.Where(w => newReportListIDs.Contains(w.ReportId))
                .Select(p => new
                {
                    ReportURL = "/Orion/Report.aspx?ReportID=" + p.ReportId,
                    Filename = p.Filename,
                    Title = p.Title,
                    ReportId = p.ReportId
                });

            var unionReports = newReports.Union(oldReportsList);

            if (unionReports.Count() == 0)
            {
                NotConfiguredText.Visible = true;
            }

            ReportRepeater.DataSource = unionReports;
            ReportRepeater.DataBind();
        }
    }

    public override string EditURL
    {
        get
        {
            string netObPart = string.Empty;
            string netObjectId = this.Request.QueryString["NetObject"];
            if (!string.IsNullOrEmpty(netObjectId))
                netObPart = "&NetObject=" + netObjectId;

            return String.Format("/Orion/NetPerfMon/Resources/Reports/EditReportList.aspx?ResourceID={0}&ViewID={1}{2}",
                this.Resource.ID, this.Resource.View.ViewID, netObPart);
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_VB0_154; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceCustomListReports"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Static; }
    }
}