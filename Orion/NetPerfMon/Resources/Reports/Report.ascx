<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Report.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Reports_Report" %>
<%@ Reference Page="~/Orion/Report.aspx" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
	    <% if (InlineRendering) { %>
        <%= DefaultSanitizer.SanitizeHtml(GetReportHtml()) %>
		<% } else { %>
		<div runat="server" id="ReportPlaceholder">
			<div style="text-align: center; margin: 2em;">
				<img alt="" src="/Orion/images/AJAX-Loader.gif" />
				<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_182) %>
			</div>
		</div>

		<% if (!NotConfiguredText.Visible) { %>
		<script type="text/javascript">
		//<![CDATA[
		    $(function() {
		        var refresh = function() {
		            $.ajax({
		                url: <%= DefaultSanitizer.SanitizeHtml(ToJSON(reportUrl)) %>,
		                type: 'GET',
		                dataType: 'html',
		                cache: false,
		                success: function(txt) {
		                    $('#<%=ReportPlaceholder.ClientID %>').html(txt);
		                }
		            });
		        };
		        SW.Core.View.AddOnRefresh(refresh, '<%=ReportPlaceholder.ClientID %>');
		        refresh();
		    });
        //]]>
		</script>
		<% } %>

		<asp:Label runat="server" ID="ErrorText" Visible="false"></asp:Label>
		<div runat="server" id="NotConfiguredText" visible="false"></div>
		<% } %>
	</Content>
</orion:resourceWrapper>
