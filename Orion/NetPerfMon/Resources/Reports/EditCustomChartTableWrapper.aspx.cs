﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Xml.Linq;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Reporting.DAL;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Reporting;
using SolarWinds.Orion.Web.Reporting.ReportCharts;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Reporting.Impl.Rendering;
using SolarWinds.Reporting.Models;
using SolarWinds.Reporting.Models.Charts;
using SolarWinds.Reporting.Models.Data;
using SolarWinds.Reporting.Models.Selection;
using SolarWinds.Reporting.Models.Tables;
using SolarWinds.Reporting.Models.Timing;
using System.Web.UI;
using System.Linq;
using SolarWinds.Orion.Core.Reporting;
using SolarWinds.Reporting;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Reporting.Models.Layout;

public partial class Orion_NetPerfMon_Resources_Reports_EditTableWrapper : System.Web.UI.Page
{
    private BaseResourceEditControl resourceConfigruration;
    private readonly IDataPresenterRepository _presentersRepo = ReportingRepository.GetInstance<IDataPresenterRepository>();
    private string _netObjectID;
    private bool arePresentersValid = true;

    #region Properties
    protected ResourceInfo Resource { get; set; }

    public Guid StorageID
    {
        get
        {
            if (Session["Edit_Chart_Table_StorageID" + Resource.ID] == null)
            {
                Session["Edit_Chart_Table_StorageID" + Resource.ID] = ReportStorage.Instance.SetReport(new Report());
            }
            return (Guid)Session["Edit_Chart_Table_StorageID" + Resource.ID];
        }
        set
        {
            Session["Edit_Chart_Table_StorageID" + Resource.ID] = value;
        }
    }

    public DataSource DynamicDataSource
    {
        get
        {
            if (!IsPostBack || Session["Edit_Chart_Table_DynamicDataSource" + Resource.ID] == null)
            {
                Session["Edit_Chart_Table_DynamicDataSource" + Resource.ID] = ResourcePropertiesReportData.CreateDataSourceForNetObject(Request["netobject"]);
            }
            if (Session["Edit_Chart_Table_DynamicDataSource" + Resource.ID] != null)
                return (DataSource)Session["Edit_Chart_Table_DynamicDataSource" + Resource.ID];
            return null;
        }
    }

    public bool AbleToUseDynamicDataSource
    {
        get { return DynamicDataSource != null; }
    }

    public Report Report { get; set; }

    public DataSource DataSource
    {
        get
        {
            if (!IsPostBack || Report.DataSources == null || Report.DataSources.Length == 0)
            {
                bool isDynamic;
                if (bool.TryParse(Resource.Properties[WebConstants.DynamicDataSourceMoniker], out isDynamic) && isDynamic)
                {
                    Report.DataSources = new[] { DynamicDataSource };
                }
                else
                {
                    var dataSourceString = Resource.Properties[WebConstants.DataSourceMoniker] ?? DefaultDataSourceConfig();
                    var datasource = !string.IsNullOrEmpty(dataSourceString) ? SerializationHelper.FromXmlString<DataSource>(dataSourceString, Loader.GetKnownTypes()) : null;
                    Report.DataSources = new[] { datasource };
                }
                ReportStorage.Instance.Update(StorageID, Report);
            }

            return Report.DataSources[0];
        }
        set
        {
            Report.DataSources = new[] { value };
            dataSourceName.InnerText = value.Name;
        }
    }

    public ConfigurationData Configuration
    {
        get
        {
            if (!IsPostBack || Report.Configs == null || Report.Configs.Length == 0)
            {
                var dataConfigString = Resource.Properties[WebConstants.ConfigurationDataMoniker] ?? string.Empty;
                ConfigurationData config;
                if (IsTableResource())
                    config = !string.IsNullOrEmpty(dataConfigString)
                                            ? SerializationHelper.FromXmlString<TableConfiguration>(dataConfigString, Loader.GetKnownTypes())
                                            : ((useDynamicDatasource.Checked) ? new TableConfiguration { RefId = Guid.NewGuid() } : null);
                else
                    config = !string.IsNullOrEmpty(dataConfigString)
                                                ? SerializationHelper.FromXmlString<ChartConfiguration>(dataConfigString, Loader.GetKnownTypes())
                                                : ((useDynamicDatasource.Checked) ? new ChartConfiguration { RefId = Guid.NewGuid() } : null);
                Report.Configs = new[] { config };
                ReportStorage.Instance.Update(StorageID, Report);
            }
            return Report.Configs[0];
        }
        set { Report.Configs = new[] { value }; }
    }

    public TimeFrame TimeFrame
    {
        get
        {
            if (!IsPostBack || Report.TimeFrames == null || Report.TimeFrames.Length == 0)
            {
                var dataTimeFrameString = Resource.Properties[WebConstants.TimeFrameMoniker] ?? DefaultTimeFrameConfig();
                var timeFrame = !string.IsNullOrEmpty(dataTimeFrameString)
                                            ? SerializationHelper.FromXmlString<TimeFrame>(dataTimeFrameString, Loader.GetKnownTypes())
                                            : null;
                Report.TimeFrames = new[] { timeFrame };
                ReportStorage.Instance.Update(StorageID, Report);
            }
            return Report.TimeFrames[0];
        }
        set { Report.TimeFrames = new[] { value }; }
    }

    #endregion

    protected bool IsTableResource()
    {
        return WebConstants.ReportTableWrapperPath.Equals(Resource.File, StringComparison.OrdinalIgnoreCase);
    }

    private string DefaultDataSourceConfig()
    {
        return DynamicDataSource != null ? SerializeObject(DynamicDataSource) : string.Empty;
    }

    private string DefaultTimeFrameConfig()
    {
        return SerializeObject(new TimeFrame
        {
            RefId = Guid.NewGuid(),
            IsStatic = false,
            DisplayName = Resources.CoreWebContent.WEBCODE_VB0_133,
            Relative =
                                           new TimeFrameRelative
                                           { NamedTimeFrame = TimeFrameNames.Today, Unit = 0, UnitCount = 0 },
            Static = null
        });
    }

    protected override void OnInit(EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["ResourceID"]))
        {
            int resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
            Resource = ResourceManager.GetResourceByID(resourceID);
            if (!IsPostBack) StorageID = ReportStorage.Instance.SetReport(new Report());
            Page.Title = string.Format(Resources.CoreWebContent.WEBCODE_AK0_88, this.Resource.Title);
            Report = ReportStorage.Instance.GetReport(StorageID, false);
        }

        SelectConfigData.Text = IsTableResource() ? Resources.CoreWebContent.WEBDATA_IB0_246 : Resources.CoreWebContent.WEBDATA_IB0_247;
        orSpan.Visible = SelectConfigData.Visible = !string.IsNullOrEmpty(Profile.ReportFolder);
        dynamicDatasourcePh.Visible = AbleToUseDynamicDataSource;

        if (!IsPostBack && AbleToUseDynamicDataSource)
        {
            bool isDynamic;
            if (bool.TryParse(Resource.Properties[WebConstants.DynamicDataSourceMoniker], out isDynamic))
            {
                useDynamicDatasource.Checked = isDynamic;
                datasourcePh.Visible = !isDynamic;
            }
            else
            {
                useDynamicDatasource.Checked = true;
                datasourcePh.Visible = false;
            }
        }

        timePeriods.WorkflowGuid = StorageID.ToString();
        // setup default time frame
        if (!IsPostBack || timePeriods.TimeFrame == null && timePeriods.TimeFrames == null)
        {
            timePeriods.TimeFrame = TimeFrame;
            timePeriods.TimeFrames = new[] { TimeFrame };
        }

        if (!string.IsNullOrEmpty(Request.QueryString["NetObject"]))
            _netObjectID = Request.QueryString["NetObject"];

        resourceTitleEditor.ResourceTitle = Resource.Title;
        resourceTitleEditor.ResourceSubTitle = Resource.SubTitle;

        if (DataSource != null)
        {
            var serializer = new DataContractJsonSerializer(typeof(DataSource));
            using (var ms = new MemoryStream())
            {
                serializer.WriteObject(ms, DataSource);
                tbDataSource.Value = Encoding.UTF8.GetString(ms.ToArray());
            }
        }
        else
        {
            dataSourceName.InnerHtml = string.Format("<i>{0}</i>", Resources.CoreWebContent.WEBDATA_IB0_181);
        }

        TryToLoadEditControl(false);
    }

    protected void useDynamicDatasource_CheckedChanged(object sender, EventArgs e)
    {
        datasourcePh.Visible = !useDynamicDatasource.Checked;

        if (useDynamicDatasource.Checked)
            DataSource = DynamicDataSource;
    }

    private void TryToLoadEditControl(bool updateViewModel)
    {
        var resourceProperties = new ResourcePropertyCollection(Resource.ID, false);
        // we can't store resource properties into DB before press Submit button
        resourceProperties.AddWithoutSave(WebConstants.DataSourceMoniker, SerializeObject(DataSource));
        resourceProperties.AddWithoutSave(WebConstants.ConfigurationDataMoniker, SerializeObject(Configuration));
        resourceProperties.AddWithoutSave(WebConstants.TimeFrameMoniker, SerializeObject(timePeriods.TimeFrame));

        var editcontrol = DynamicControlsHolder.FindControl("Configuration");
        if (editcontrol is Orion_NetPerfMon_Controls_EditResourceControls_EditReportTableWrapper)
        {
            var editCtrl = editcontrol as Orion_NetPerfMon_Controls_EditResourceControls_EditReportTableWrapper;
            editCtrl.ReportDataProvider = new ResourcePropertiesReportData(editCtrl, TableRenderer.Moniker, resourceProperties);
            editCtrl.UpdateViewModel(Configuration as TableConfiguration);
            resourceConfigruration = editCtrl;
        }
        else if (editcontrol is Orion_NetPerfMon_Controls_EditResourceControls_EditReportChartWrapper)
        {
            var editCtrl = editcontrol as Orion_NetPerfMon_Controls_EditResourceControls_EditReportChartWrapper;
            editCtrl.ReportDataProvider = new ResourcePropertiesReportData(editCtrl, ReportChartRenderer.Moniker, resourceProperties);
            editCtrl.UpdateViewModel(Configuration as ChartConfiguration);
            resourceConfigruration = editCtrl;
        }

        if (DataSource != null && resourceConfigruration == null)
        {
            if (IsTableResource())
            {
                var editCtrl = (Orion_NetPerfMon_Controls_EditResourceControls_EditReportTableWrapper)
                                         LoadControl(
                                             @"/Orion/NetPerfMon/Controls/EditResourceControls/EditReportTableWrapper.ascx");

                if (editCtrl != null)
                {
                    editCtrl.ID = "Configuration";
                    // define ReportDataProvider with current resource properties collection which might not be stored yet within the DB
                    editCtrl.ReportDataProvider = new ResourcePropertiesReportData(editCtrl, TableRenderer.Moniker,
                                                                                      resourceProperties);
                    editCtrl.Resource = Resource;
                    DynamicControlsHolder.Controls.Add(editCtrl);
                    if (updateViewModel)
                        editCtrl.UpdateViewModel(Configuration as TableConfiguration);
                    resourceConfigruration = editCtrl;
                }
            }
            else
            {
                var editCtrl = (Orion_NetPerfMon_Controls_EditResourceControls_EditReportChartWrapper)
                                         LoadControl(
                                             @"/Orion/NetPerfMon/Controls/EditResourceControls/EditReportChartWrapper.ascx");

                if (editCtrl != null)
                {
                    editCtrl.ID = "Configuration";
                    // define ReportDataProvider with current resource properties collection which might not be stored yet within the DB
                    editCtrl.ReportDataProvider = new ResourcePropertiesReportData(editCtrl, ReportChartRenderer.Moniker,
                                                                                      resourceProperties);
                    editCtrl.Resource = Resource;
                    DynamicControlsHolder.Controls.Add(editCtrl);
                    if (updateViewModel)
                        editCtrl.UpdateViewModel(Configuration as ChartConfiguration);
                    resourceConfigruration = editCtrl;
                }
            }
            if (resourceConfigruration != null)
            {
                // put additional buttons into placeholder
                phAdditionalButtons.Controls.Clear();
                foreach (Control button in resourceConfigruration.ButtonsForButtonBar)
                {
                    phAdditionalButtons.Controls.Add(button);
                }
            }
            // updating datasource name
            dataSourceName.InnerText = DataSource.Name;
            SpecifyDatasource.Text = Resources.CoreWebContent.WEBDATA_IB0_248;
        }
        else if (DataSource == null)
        {
            DynamicControlsHolder.Controls.Clear();
        }

        datasourceErrorMsg.Visible = !arePresentersValid;
        sqlWarning.Visible = (DataSource != null && DataSource.Type == DataSourceType.CustomSQL);
    }

    protected void SelectConfigData_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(tbReportConfig.Value))
        {
            var param = tbReportConfig.Value.Split('$');
            if (param.Length == 2)
            {
                var reportId = int.Parse(param[0]);
                var configId = new Guid(param[1]);

                var report = ReportDAL.GetReportById(reportId);
                var config = report.Configs.Where(x => x.RefId == configId).FirstOrDefault();

                Configuration = config;
                SectionCell sectionCell = null;

                // try to find section cell with the selected configuration
                foreach (var section in report.Sections)
                {
                    if (sectionCell != null) break;
                    foreach (var column in section.Columns)
                    {
                        if (sectionCell != null) break;
                        foreach (var cell in column.Cells)
                        {
                            if (cell.ConfigId == configId)
                            {
                                sectionCell = cell;
                                break;
                            }
                        }
                    }
                }

                if (sectionCell != null)
                {
                    report.Sections.Select(x => x.Columns.Select(y => y.Cells.Where(z => z.ConfigId == configId).FirstOrDefault()).FirstOrDefault()).FirstOrDefault();

                    DataSource = report.DataSources.Where(x => x.RefId == sectionCell.DataSelectionRefId).FirstOrDefault();

                    if (DataSource != null)
                    {
                        var serializer = new DataContractJsonSerializer(typeof(DataSource));
                        using (var ms = new MemoryStream())
                        {
                            serializer.WriteObject(ms, DataSource);
                            tbDataSource.Value = Encoding.UTF8.GetString(ms.ToArray());
                        }
                    }

                    var timeFrame = report.TimeFrames.Where(x => x.RefId == sectionCell.TimeframeRefId).FirstOrDefault();
                    if (timeFrame != null)
                    {
                        TimeFrame = timeFrame;
                        timePeriods.TimeFrame = TimeFrame;
                        timePeriods.TimeFrames = new[] { TimeFrame };
                    }

                    resourceTitleEditor.ResourceTitle = config.DisplayTitle;
                    resourceTitleEditor.ResourceSubTitle = config.DisplaySubTitle;
                }
            }
            TryToLoadEditControl(true);
        }
    }

    protected void SpecifyDatasource_Click(object sender, EventArgs e)
    {

        if (!string.IsNullOrEmpty(tbDataSource.Value))
        {
            var serializer = new DataContractJsonSerializer(typeof(DataSource));
            DataSource = (DataSource)serializer.ReadObject(new MemoryStream(Encoding.UTF8.GetBytes(tbDataSource.Value)));
            // in case we specify datasource first time we should define default Configuration
            if (Configuration == null)
            {
                if (IsTableResource())
                    Configuration = new TableConfiguration { RefId = Guid.NewGuid() };
                else
                    Configuration = new ChartConfiguration { RefId = Guid.NewGuid() };
            }
            else if (resourceConfigruration != null)
            {
                // we should keep actual configuration when change datasource if possible
                var configXml = resourceConfigruration.Properties[WebConstants.ConfigurationDataMoniker];
                if (configXml != null)
                    if (IsTableResource())
                        Configuration = SerializationHelper.FromXmlString<TableConfiguration>(configXml.ToString(), Loader.GetKnownTypes());
                    else
                        Configuration = SerializationHelper.FromXmlString<ChartConfiguration>(configXml.ToString(), Loader.GetKnownTypes());
            }
        }
        else
        {
            DataSource = null;
        }
        TryToLoadEditControl(false);
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Page.Validate();
        arePresentersValid = presentersAreValid();
        datasourceErrorMsg.Visible = !arePresentersValid;
        var pageIsValid = Page.IsValid && arePresentersValid;

        if (pageIsValid)
        {
            Resource.Properties["Title"] = resourceTitleEditor.ResourceTitle;
            Resource.Properties["SubTitle"] = resourceTitleEditor.ResourceSubTitle;
            Resource.Properties[WebConstants.DynamicDataSourceMoniker] = useDynamicDatasource.Checked.ToString();
            if (DataSource != null)
            {
                Resource.Properties[WebConstants.DataSourceMoniker] = SerializeObject(DataSource);
                Resource.Properties[WebConstants.TimeFrameMoniker] = SerializeObject(timePeriods.TimeFrame);
                Resource.Properties[WebConstants.ConfigurationDataMoniker] =
                    resourceConfigruration.Properties[WebConstants.ConfigurationDataMoniker].ToString();
            }
            btnCancel_Click(sender, e);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        string url = string.Format("/Orion/View.aspx?ViewID={0}", Resource.View.ViewID);
        if (!string.IsNullOrEmpty(_netObjectID))
            url = string.Format("{0}&NetObject={1}", url, _netObjectID);
        Response.Redirect(url);
    }

    private bool presentersAreValid()
    {
        try
        {
            if (resourceConfigruration == null)
            {
                // resourceConfigruration is not set because data source was not selected
                // therefore user stays on the editing page of custom chart/table and a reminder of selecting data source appears
                return false;
            }

            var configuration =
                SerializationHelper.FromXmlString<TableConfiguration>(resourceConfigruration.Properties[WebConstants.ConfigurationDataMoniker].ToString(), Loader.GetKnownTypes());

            if (configuration.Columns != null)
            {
                foreach (var column in configuration.Columns)
                {
                    if (column.Presenters != null && column.Presenters.Any())
                    {
                        foreach (var presenter in column.Presenters)
                        {
                            if (DataSource != null)
                            {
                                var presenterRef = _presentersRepo.GetRef(presenter.PresenterId);
                                var initializedPresenter = presenterRef.Factory.NewInstance(DataSource, column.Field, presenter.Values);

                                // Ideally, presenter should be initialized by this point because we have already invoked corresponding factory
                                // and passed presenter values as configOverride of Factory.NewInstance method, right? Well no...
                                // Thus, calling presenter's Load Method to be double sure we initialized presenter.
                                initializedPresenter.Load(presenter.Values);

                                if (initializedPresenter.Validate() != null && initializedPresenter.Validate().Any())
                                {
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (SerializationException ex)
        {
            // exception might occur when malformed document is sent or chart configuration is used instead of table configuration.
            // therefore if at this point we encounter serialization error - we skip processing and assume presenters are valid.
            return true;
        }

        return true;
    }

    private string SerializeObject(Object data)
    {
        if (data != null)
        {
            var dcs = new DataContractSerializer(data.GetType(), Loader.GetKnownTypes());

            var d = new XDocument();
            using (var writer = d.CreateWriter())
            {
                dcs.WriteObject(writer, data);
                writer.Flush();
                writer.Close();
            }
            if (d.Root != null)
                return d.Root.ToString();
        }
        return string.Empty;
    }
}