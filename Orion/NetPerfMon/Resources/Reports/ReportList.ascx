<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReportList.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Reports_ReportList" %>
<orion:resourceWrapper runat="server">
    <Content>
        <asp:Repeater runat="server" ID="ReportRepeater">
            <HeaderTemplate>
                <table border="0" cellpadding="2" cellspacing="2" width="100%">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td class="Property">
                        <a href="<%# DefaultSanitizer.SanitizeHtml(Eval("ReportURL")) %>">
                            <%# DefaultSanitizer.SanitizeHtml(Eval("Title")) %>
                        </a>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
            </table>
            </FooterTemplate>
        </asp:Repeater>
        <div runat="server" id="NotConfiguredText" visible="false"></div>
    </Content>
</orion:resourceWrapper>
