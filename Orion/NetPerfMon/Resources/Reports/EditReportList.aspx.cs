﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Reporting;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Helpers;
using Newtonsoft.Json;
using System.Text;
using System.IO;

public partial class Orion_NetPerfMon_Resources_Reports_EditReportList : System.Web.UI.Page
{
    private ResourceInfo _resource;
    protected ResourceInfo Resource
    {
        get { return _resource; }
    }

    private string _netObjectID;

    private IEnumerable<int> selectedReports;
    private IEnumerable<OrionReport> reports;

    protected override void OnInit(EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["ResourceID"]))
        {
            int resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
            this._resource = ResourceManager.GetResourceByID(resourceID);
            Page.Title = string.Format(Resources.CoreWebContent.WEBCODE_AK0_88, this.Resource.Title);
        }

        if (!string.IsNullOrEmpty(Request.QueryString["NetObject"]))
            _netObjectID = Request.QueryString["NetObject"];

        this.resourceTitleEditor.ResourceTitle = this.Resource.Title;
        this.resourceTitleEditor.ResourceSubTitle = this.Resource.SubTitle;

        string reportList = Resource.Properties["ReportList"] ?? "";
        string newReportList = Resource.Properties["ReportListIDs"] ?? "";

        //Pull Filename from legacy selected reports
        var legacyReports = reportList.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
            .Select(s => s.Split(new char[] { ':' }, 2))
            .Select(p => p[0].Trim());

        //Get IDs from ReportListIDs property
        var newReports = newReportList.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
            .Select(s => Convert.ToInt32(s));

        //Load all reports from Reports table
        reports = OrionReport.LoadAllReports()
            .Where(rpt => rpt.AllowOnWeb);

        //Get legacy report ID, by finding it in List of reports, using Filename
        selectedReports = reports.Where(w => legacyReports.Contains(w.Filename)).
            Select(p => p.ReportId);
        selectedReports = selectedReports.Union(newReports);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        groupsRepeater.DataSource = reports.GroupBy(rpt => rpt.Group)
            .Select(g => new KeyValuePair<string, IEnumerable<OrionReport>>(g.Key, g.OrderBy(r => r.Title)))
            .OrderBy(p => p.Key);
        groupsRepeater.DataBind();
    }

    protected void groupsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater inner = (Repeater)item.FindControl("reportsRepeater");
            KeyValuePair<string, IEnumerable<OrionReport>> group = (KeyValuePair<string, IEnumerable<OrionReport>>)item.DataItem;
            inner.DataSource = group.Value;
            inner.DataBind();
        }
    }

    static Regex nonIDChars = new Regex(@"[^-A-Za-z0-9_:.]", RegexOptions.Compiled);

    protected string MakeControlID(object filename)
    {
        return "rpt_" + nonIDChars.Replace(filename.ToString(), string.Empty);
    }

    protected bool IsSelected(object id)
    {
        return selectedReports.Contains((int)id);
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(resourceTitleEditor.ResourceTitle))
        {
            Resource.Title = resourceTitleEditor.ResourceTitle;
        }
        else
        {
            var resourceControl = (BaseResourceControl)LoadControl(Resource.File);
            Resource.Title = resourceControl.Title;
        }
        Resource.SubTitle = resourceTitleEditor.ResourceSubTitle;
        ResourcesDAL.Update(Resource);

        Resource.Properties["ReportList"] = string.Empty;
        Resource.Properties["ReportListIDs"] = WebSecurityHelper.SanitizeHtmlV2(Request.Form["Property-ReportListIDs"]) ?? string.Empty;
        //this.Resource.Properties["Grouping1"] = this.lbxGroup1.SelectedValue;
        //this.Resource.Properties["Grouping2"] = this.lbxGroup2.SelectedValue;
        //this.Resource.Properties["Grouping3"] = this.lbxGroup3.SelectedValue;

        //this.Resource.Properties["Filter"] = SqlFilterChecker.CleanFilter(this.SQLFilter.FilterTextBox.Text);

        //this.Resource.Properties["GroupNodesWithNullPropertiesAsUnknown"] = this.GroupNulls.SelectedValue;

        string url = string.Format("/Orion/View.aspx?ViewID={0}", Resource.View.ViewID);
        if (!string.IsNullOrEmpty(_netObjectID))
            url = string.Format("{0}&NetObject={1}", url, _netObjectID);
        Response.Redirect(url);
    }
}