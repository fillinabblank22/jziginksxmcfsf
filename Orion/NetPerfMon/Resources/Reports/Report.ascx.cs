﻿using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Reporting;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Syslog)]
[ResourceMetadata(StandardMetadataPropertyName.IsBlockedForViews, "true")]

public partial class Orion_NetPerfMon_Resources_Reports_Report : BaseResourceControl
{
    private static readonly Log log = new Log();

    private OrionReport report;
    private string reportName;
    protected string reportUrl;
    protected bool InlineRendering = false;

	protected override void OnInit(EventArgs e)
	{
		bool.TryParse(Request["InlineRendering"], out InlineRendering);

		reportName = Resource.Properties["ReportName"];

        if (string.IsNullOrEmpty(reportName))
		{
			ReportPlaceholder.Visible = false;
			NotConfiguredText.Visible = true;
            var emptyResource = string.Format(
                   Resources.CoreWebContent.WEBCODE_VB0_333,
                   String.Format("<ul><li><a href=\"{0}\"><font color=\"blue\"><b>", EditURL), "</b></font></a>"
                   , "</li><li>", "</li></ul>", "<span style=\"font-weight:bold;\">", "</span>");
            NotConfiguredText.Controls.Add(new WebResourceWantsEdit
            {
                HelpfulHtml =emptyResource,
                EditUrl = Profile.AllowCustomize ? EditURL : null,
                WatermarkImage = "/Orion/Images/ResourceWatermarks/Watermark.ReportFromOrionReportWriter.gif",
                BackgroundPosition = "right center",
                WatermarkBackgroundColor = "#f2f2f2"
                // image has default watermark dimentions
            });
		}
		else if (InlineRendering)
		{
            report = OrionReport.Load(reportName);
		}
		else if (Resource.Title.Equals(Resources.CoreWebContent.WEBCODE_VB0_149, StringComparison.OrdinalIgnoreCase))
		{

			try
			{
                report = OrionReport.Load(reportName);
			}
			catch (Exception ex)
			{
                log.ErrorFormat("Error loading report {0}. {1}", reportName, ex);
				ReportPlaceholder.Visible = false;
				ErrorText.Visible = true;
                ErrorText.Text = string.Format(Resources.CoreWebContent.WEBCODE_VB0_150, reportName, 
                                                (ex is LocalizedExceptionBase) ? (ex as LocalizedExceptionBase).LocalizedMessage : ex.Message);
			}
		}

		base.OnInit(e);
	}

    protected void Page_Load(object sender, EventArgs e)
    {
        reportUrl = "/Orion/NetPerfMon/Resources/Reports/EmbeddedReport.aspx?ResourceID=" + Resource.ID;
        NetObject obj = TryToGetNetObject();
        if (obj != null)
            reportUrl = string.Format("{0}&NetObject={1}", reportUrl, obj.NetObjectID);
    }

    private NetObject TryToGetNetObject()
    {
        return GetInterfaceInstance<INetObjectProvider>().NetObject;
    }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_VB0_149; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceReportOrionReportWriter"; }
    }

    public override string EditURL
    {
        get
        {
            return String.Format("/Orion/NetPerfMon/Resources/Reports/EditReport.aspx?ResourceID={0}&ViewID={1}&NetObject={2}&HideInterfaceFilter=True",
                this.Resource.ID, this.Resource.View.ViewID, this.Request.QueryString["NetObject"]);
        }
    }

    public override string DisplayTitle
    {
        get
        {
            if (report != null && Resource.Title.Equals(Resources.CoreWebContent.WEBCODE_VB0_149, StringComparison.OrdinalIgnoreCase))
                return report.Title;
            else
                return Resource.Title;
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected static string ToJSON(object value, params Type[] types)
    {
        DataContractJsonSerializer serializer = new DataContractJsonSerializer(value.GetType(), types);
        using (MemoryStream ms = new MemoryStream())
        {
            serializer.WriteObject(ms, value);
            return Encoding.UTF8.GetString(ms.ToArray());
        }
    }

    protected string GetReportHtml()
    {
        try
        {
            return ReportRunner.Execute(
                report,
                new OrionReportHtmlFormatter(Request),
                string.Empty,
                string.Empty,
                Resource.Properties["Filter"],
                Request["NetObject"]);
        }
        catch (Exception ex)
        {
            log.ErrorFormat("Error generating report {0} for request {1}.\n{2}", reportName, Request.Url, ex);
            return Resources.CoreWebContent.WEBCODE_VB0_151 + ex.Message; 
        }
    }
}
