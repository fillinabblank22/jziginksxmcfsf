﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReportTableWrapper.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Reports_ReportTableResourceWrapper" %>
<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <asp:PlaceHolder ID="ReportTableContainer" runat="server"></asp:PlaceHolder>
    </Content>
</orion:ResourceWrapper>