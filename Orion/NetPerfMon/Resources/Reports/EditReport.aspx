<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true" CodeFile="EditReport.aspx.cs" Inherits="Orion_NetPerfMon_Resources_Reports_EditReport" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<%@ Register TagPrefix="orion" TagName="FilterNodesSql" Src="~/Orion/Controls/FilterNodesSql.ascx" %>
<%@ Reference Page="~/Orion/Report.aspx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBCODE_AK0_88, CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Resource.Title, Request["netobject"])))) %></h1>
    
    <orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="true"/>

	<h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_VB0_152) %></h2>

	<asp:DropDownList runat="server" ID="ReportList" DataTextField="Title" DataValueField="Filename"></asp:DropDownList>

    <orion:FilterNodesSql runat="server" ID="SQLFilter" />
    
    <div class="sw-btn-bar"><orion:LocalizableButton LocalizedText="Submit" DisplayType="Primary" runat="server" ID="btnSubmit" onclick="btnSubmit_Click" /></div>
</asp:Content>
