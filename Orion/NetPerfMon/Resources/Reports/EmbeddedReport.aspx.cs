using System;
using System.IO;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Reporting;

public partial class Orion_NetPerfMon_Resources_Reports_EmbeddedReport : System.Web.UI.Page
{
    private ResourceInfo resource;
    private OrionReport report;
    private string reportName;
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    private string reportErrorMessage = String.Empty;


    protected override void OnLoad(EventArgs e)
    {
        Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
        resource = ResourceManager.GetResourceByID(int.Parse(Request["ResourceID"]));
        this.Context.Items[typeof(ViewInfo).Name] = resource.View; // this will cause View Limitations to be applied

        reportName = resource.Properties["ReportName"];

        try
        {
            report = OrionReport.Load(reportName);
        }
        catch (Exception ex)
        {
            log.Error("Error during loading report: "+ex.ToString());
            reportErrorMessage = (ex is LocalizedExceptionBase) ? (ex as LocalizedExceptionBase).LocalizedMessage : ex.Message;
        }

        base.OnLoad(e);
    }

    protected string GetReportHtml()
    {
        try
        {
            if (report == null && !String.IsNullOrEmpty(reportErrorMessage))
                throw new Exception(reportErrorMessage);

            OrionReportFormatter formatter = new OrionReportHtmlFormatter(Request);

            return ReportRunner.Execute(
                report,
                new OrionReportHtmlFormatter(Request),
                string.Empty,
                string.Empty,
                resource.Properties["Filter"],
                Request["NetObject"]);
        }
        catch (Exception ex)
        {
            log.ErrorFormat("Error generating report {0} for request {1}.\n{2}", reportName, Request.Url, ex);
            return Resources.CoreWebContent.WEBCODE_VB0_151 + ex.Message; 
        }
    }
}
