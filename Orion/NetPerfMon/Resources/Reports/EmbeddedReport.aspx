<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EmbeddedReport.aspx.cs" Inherits="Orion_NetPerfMon_Resources_Reports_EmbeddedReport" %>
<%@ Reference Page="~/Orion/Report.aspx" %>

<%= DefaultSanitizer.SanitizeHtml(GetReportHtml()) %>
