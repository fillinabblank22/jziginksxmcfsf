﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReportChartWrapper.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Reports_ReportChartWrapper" %>
<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <asp:PlaceHolder ID="ReportChartContainer" runat="server"></asp:PlaceHolder>
    </Content>
</orion:ResourceWrapper>
