using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Reporting;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Resources_Reports_EditReport : System.Web.UI.Page
{
	private ResourceInfo _resource;
	protected ResourceInfo Resource
	{
		get { return _resource; }
	}

	private string _netObjectID;

	protected override void OnInit(EventArgs e)
	{
		if (!string.IsNullOrEmpty(Request.QueryString["ResourceID"]))
		{
			int resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
			_resource = ResourceManager.GetResourceByID(resourceID);
			Page.Title = string.Format(Resources.CoreWebContent.WEBCODE_AK0_88, this.Resource.Title);
		}

		if (!string.IsNullOrEmpty(Request.QueryString["NetObject"]))
			_netObjectID = Request.QueryString["NetObject"];

		this.resourceTitleEditor.ResourceTitle = this.Resource.Title;
		this.resourceTitleEditor.ResourceSubTitle = this.Resource.SubTitle;
	}

    private static readonly Log _log = new Log();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            List<OrionReport> list = new List<OrionReport>();
            string[] listOfNames = ReportDAL.GetAllReportNames(true);
            foreach (string repName in listOfNames)
            {
                try
                {
                    OrionReport rep = OrionReport.Load(repName);
                    if (rep.AllowOnWeb)
                        list.Add(rep);
                }
                catch (Exception ex)
                {
                    _log.Error(string.Format("Failed loading report [{0}].", repName), ex);
                }
            }

            ReportList.DataSource = list.OrderBy(rpt => rpt.Group)
                .ThenBy(rpt => rpt.Title);
            ReportList.DataBind();

            ReportList.SelectedValue = Resource.Properties["ReportName"];
            SQLFilter.FilterTextBox.Text = Resource.Properties["Filter"];
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
	{
		if (!String.IsNullOrEmpty(resourceTitleEditor.ResourceTitle))
		{
			Resource.Title = resourceTitleEditor.ResourceTitle;
		}
		else
		{
			var resourceControl = (BaseResourceControl)LoadControl(Resource.File);
			Resource.Title = resourceControl.Title;
		}
		Resource.SubTitle = resourceTitleEditor.ResourceSubTitle;
		ResourcesDAL.Update(Resource);

		Resource.Properties["ReportName"] = ReportList.SelectedValue;
		Resource.Properties["Filter"] = SQLFilter.FilterTextBox.Text;

		string url = string.Format("/Orion/View.aspx?ViewID={0}", Resource.View.ViewID);
		if (!string.IsNullOrEmpty(_netObjectID))
			url = string.Format("{0}&NetObject={1}", url, _netObjectID);
		Response.Redirect(url);
	}
}
