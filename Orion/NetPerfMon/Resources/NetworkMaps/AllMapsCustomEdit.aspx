<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AllMapsCustomEdit.aspx.cs" MasterPageFile="~/Orion/OrionMasterPage.master" Inherits="Orion_NetPerfMon_Resources_NetworkMaps_AllMapsCustomEdit" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

<asp:Content ID="Content1" Runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">
    <h1><%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBCODE_AK0_88, CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Resource.Title, Request["netobject"])))) %></h1>
    <div style="padding-left: 16px;">
    <orion:EditResourceTitle runat="server" ID="resourceTitleEditor" />
    
    <asp:CheckBoxList runat="server" ID="mapsList">
        
    </asp:CheckBoxList>
    <div class="sw-btn-bar"><orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" DisplayType="Primary" OnClick="SubmitClick" /></div>     
    </div>
</asp:Content>