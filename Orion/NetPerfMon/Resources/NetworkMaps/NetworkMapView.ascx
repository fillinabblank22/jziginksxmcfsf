﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NetworkMapView.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NetworkMaps_NetworkMapView" %>

<orion:resourceWrapper runat="server" ID="Wrapper" ShowEditButton="true"> 
    <Content>
        <script type="text/javascript">
            $(function () {
                var resourceId = '<%= Resource.ID %>';
                MapControlAddToolTips(resourceId);

				<%-- this link appears when map is not found, we need to change URL to the same URL like in EditURL of the resource --%>
                var editLink = $('#networkMap-' + resourceId + ' a[href *= "NetworkMapEdit.aspx?ResourceID="]');
                if (editLink.length > 0) {
                    editLink.attr('href', '<%= EditURL%>');
				}
            });
        </script>
        <asp:PlaceHolder ID="MapControlPlaceholder" runat="server" ></asp:PlaceHolder>
    </Content>
</orion:resourceWrapper>