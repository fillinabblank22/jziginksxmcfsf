﻿using System;
using System.Globalization;
using System.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI.Localizer;
using SolarWinds.Orion.Web.WorldMap;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.NetworkMaps)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsNetworkMaps.Maps)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.IsFederationAware, "true")]
public partial class Orion_NetPerfMon_Resources_NetworkMaps_WorldMapView : SolarWinds.Orion.Web.UI.BaseResourceControl
{
    public const int ConfigDefaultHeight = 400;

    protected WorldMapHelper.WorldMapParams PgParams { get; set; }

    private OrionInclude jsFile { get; set; }

    public override SolarWinds.Orion.Web.UI.ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return SolarWinds.Orion.Web.UI.ResourceLoadingMode.Ajax;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        if (Profile.AllowNodeManagement)
        {
            Wrapper.ShowManageButton = true;
            Wrapper.ManageButtonText = Resources.CoreWebContent.WEBDATA_PCC_WM_VR_MANAGEMAP;
            Wrapper.ManageButtonTarget = "/Orion/WorldMap/Manage.aspx?ResourceId=" + Resource.ID;
        }

        OrionInclude.CoreFile("WorldMap.css");

        OrionInclude.CoreFile("leaflet-0.7.7/leaflet.js");
        if (WorldMapHelper.UseExternalMapQuest(HttpContext.Current))
        {
            OrionInclude.ExternalFile("https://www.mapquestapi.com/sdk/leaflet/v2.2/mq-map.js?key=" + SolarWinds.Orion.Core.Common.DALs.WorldMapPointsDAL.GetMapQuestKey(), PathFinder.ResourceType.JavaScript);
        }
        else
        {
            OrionInclude.CoreFile("mq-map.js");
        }

        jsFile = OrionInclude.CoreFile("WorldMap.js", OrionInclude.Section.Bottom);

        base.OnInit(e);
    }

    protected override void OnLoad(EventArgs e)
    {
        PgParams = WorldMapHelper.GetParams(x => Resource.Properties[x], false);
        PgParams.Resource = Resource;
        PgParams.ResourceId = Resource.ID;

        if (!string.IsNullOrEmpty(Resource.Properties["Title"]))
        {
            Resource.Title = Resource.Properties["Title"];
        }
        if (!string.IsNullOrEmpty(Resource.Properties["SubTitle"]))
        {
            Resource.SubTitle = Resource.Properties["SubTitle"];
        }
        if (string.IsNullOrEmpty(Resource.Title))
        {
            Resource.Title = DefaultTitle;
        }

        if (!PgParams.Latitude.HasValue && !PgParams.Longitude.HasValue)
        {
            PgParams.Latitude = 0; // [pc]
            PgParams.Longitude = 0; // 
            if (!PgParams.Zoom.HasValue) PgParams.Zoom = 1;
        }

        if (!PgParams.MapHeight.HasValue)
        {
            PgParams.MapHeight = 400;
        }

        string js = string.Format(CultureInfo.InvariantCulture, @"$(function(){{
    var vol = SW.Core.namespace('SW.Core.Volatile.WorldMaps');
    var opts = {{ lat: {0}, lng: {1}, zoom: {2}, resourceid: '{3}', optimize: true, tileset: '{4}' }};
    var renderMap = function(){{
        var map = vol[opts.resourceid];
        if(!map)
             map= vol[opts.resourceid]= SW.Core.WorldMap.Viewer.Create('worldmap'+opts.resourceid, opts );
        map.Refresh();
    }};
    var Refresh=function(){{
        var resourceContainer=$('#worldmap'+opts.resourceid).closest('table');
        if(resourceContainer && resourceContainer.css('display') && resourceContainer.css('display')!='none'){{
        renderMap();
    }}
    else if(resourceContainer.length == 0) {{
        renderMap();
    }}}};
    SW.Core.View.AddOnRefresh(Refresh)
    Refresh();
   }});", PgParams.Latitude, PgParams.Longitude, PgParams.Zoom, Resource.ID, System.Web.HttpUtility.JavaScriptStringEncode(PgParams.Tileset));

        jsFile.AddJsInit(js);

        base.OnLoad(e);
    }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBDATA_VL0_67; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditWorldMap.ascx"; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionCorePHResourceWorldMap"; }
    }
}
