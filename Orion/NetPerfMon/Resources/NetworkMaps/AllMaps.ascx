﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllMaps.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NetworkMaps_AllMaps" %>
<%@ Register TagPrefix="x" TagName="Maps" Src="~/Orion/NetPerfMon/Controls/AllMapsListControl.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper" ShowEditButton="false"> 
    <Content>
        <x:Maps ID="MapsList" runat="server" />
    </Content>
</orion:resourceWrapper>