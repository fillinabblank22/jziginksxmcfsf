<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NetworkMapEdit.aspx.cs" MasterPageFile="~/Orion/MasterPage.master" Inherits="Orion_NetPerfMon_Resources_NetworkMaps_NetworkMapEdit" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

<asp:Content ID="Content1" Runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">
    <script language="javascript" type="text/javascript">
        // will be executed on page load
        $(function () {
            if ($("select[id$=mapNameFormatsDropDown]")) {
                // adds onChange event to mapNameFormatsDropDown
                $("select[id$=mapNameFormatsDropDown]").change(
                    function () {
                        // selected value is empty
                        if ($(this).val()) {
                            // disables mapsListbox
                            $("select[id$=mapsListbox]").attr('disabled', 'disabled');
                        } else {
                            // enables mapsListbox
                            $("select[id$=mapsListbox]").removeAttr('disabled');
                        }
                    }
                );

                // if map format is not selected 
                if ($("select[id$=mapNameFormatsDropDown]").val()) {
                    // disables mapsListbox
                    $("select[id$=mapsListbox]").attr('disabled', 'disabled');
                } else {
                    // enables mapsListbox
                    $("select[id$=mapsListbox]").removeAttr('disabled');
                };
            }
        });
    </script>
    <h1><%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBCODE_AK0_88, CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Resource.Title, Request["netobject"])))) %></h1>
    <div class="BodyContent" style="width:780px">
	<div style="float:right; width:240px; background-color: #ecedee; padding:10px; border: 1px solid #ecedee;" >
		    <div style="font-weight:bold; font-size:11px"><i><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_153) %></i> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_154) %></div>
			<div style="padding-top:6px; font-size:11px"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_155) %></div>
			<div style="padding-top:6px; font-size:11px">&#0187; <a style="text-decoration:underline;color:#336699;" href="../EditMapTooltips.aspx"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_156) %></a></div>
		</div>
	<div>
	    
        <div><orion:EditResourceTitle runat="server" ID="resourceTitleEditor" /></div>
        <div style="font-weight:bold"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_157) %></div>
        <asp:ListBox runat="server" ID="mapsListbox" Width="400" Height="350"></asp:ListBox>
        <div id="errorMessage" visible="false" runat="server"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_158) %></div>
        <asp:Panel id="mapNameFormatsPanel" runat="server">
            <div style="font-weight: bold; padding-top: 18px"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_2) %></div>
            <asp:DropDownList ID="mapNameFormatsDropDown" runat="server" Width="400"></asp:DropDownList>
        </asp:Panel>
        <div style="font-weight:bold; padding-top:18px"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_159) %></div>
        <div><asp:TextBox runat="server" ID="scaleTextBox"></asp:TextBox></div>
        <div id="errorMessage2" visible="false" runat="server"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_160) %></div>
        <div style="font-weight:bold; padding-top:18px"><asp:CheckBox ID="ShowDownloadButtonCheckbox" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_161 %>"/></div>
        <div style="font-weight:bold; padding-top:3px"><asp:CheckBox ID="UseCachingCheckbox" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_162 %>" /></div>
        <div class="sw-btn-bar"><orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" DisplayType="Primary" OnClick="SubmitClick" /></div>                
		</div>
    </div>
</asp:Content>