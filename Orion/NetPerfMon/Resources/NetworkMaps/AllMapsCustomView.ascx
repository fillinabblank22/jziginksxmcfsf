﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllMapsCustomView.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NetworkMaps_AllMapsCustomView" %>
<%@ Register TagPrefix="x" TagName="Maps" Src="~/Orion/NetPerfMon/Controls/AllMapsListControl.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper" ShowEditButton="true"> 
    <Content>
        <x:Maps ID="MapsList" runat="server" />
    </Content>
</orion:resourceWrapper>