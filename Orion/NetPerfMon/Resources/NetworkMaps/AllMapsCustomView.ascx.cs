﻿using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.NetworkMaps)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsCustomResourcesValues.Configurable)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsNetworkMaps.Maps)]
[ResourceMetadata(StandardMetadataPropertyName.IsFederationAware, "true")]
public partial class Orion_NetPerfMon_Resources_NetworkMaps_AllMapsCustomView : SolarWinds.Orion.Web.UI.BaseResourceControl
{
    protected override void OnInit(EventArgs e)
    {
        if (!string.IsNullOrEmpty(Resource.Properties["Title"]))
        {
            Resource.Title = Resource.Properties["Title"];
        }
        if (!string.IsNullOrEmpty(Resource.Properties["SubTitle"]))
        {
            Resource.SubTitle = Resource.Properties["SubTitle"];
        }
        if (string.IsNullOrEmpty(Resource.Title))
        {
            Resource.Title = DefaultTitle;
        }

        if (!string.IsNullOrEmpty(Resource.Properties["FilteredMaps"]))
        {
            MapsList.MapsFilter = Resource.Properties["FilteredMaps"].Split(';');
        }

        base.OnInit(e);
    }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_VB0_106; }   
    }

    public override string EditURL
    {
        get
        {
            var url = string.Format("/Orion/NetPerfMon/Resources/NetworkMaps/AllMapsCustomEdit.aspx?ResourceID={0}", this.Resource.ID);
            return ExtendCustomEditUrl(url);
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceCustomListMaps"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
