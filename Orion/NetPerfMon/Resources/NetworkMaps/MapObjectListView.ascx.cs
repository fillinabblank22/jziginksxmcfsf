﻿using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.NetworkMaps)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsNetworkMaps.Maps)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_NetPerfMon_Resources_NetworkMaps_MapObjectListView : SolarWinds.Orion.Web.UI.BaseResourceControl
{
    protected override void OnInit(EventArgs e)
    {
        //upgrade from 9.1 -> 9.5 - old map resource uses key NetworkMap
        string oldMap = Resource.Properties["NetworkMap"];
        if (!string.IsNullOrEmpty(oldMap))
        {
            Resource.Properties["MapId"] = oldMap;
            Resource.Properties["NetworkMap"] = string.Empty;
        }

        if (!string.IsNullOrEmpty(Resource.Properties["Title"]))
        {
            Resource.Title = Resource.Properties["Title"];
        }
        if (!string.IsNullOrEmpty(Resource.Properties["SubTitle"]))
        {
            Resource.SubTitle = Resource.Properties["SubTitle"];
        }
        if (string.IsNullOrEmpty(Resource.Title))
        {
            Resource.Title = DefaultTitle;
        }
        
        MapObjectsList.ResourceId = this.Resource.ID.ToString();

        // get container properties
        Container container = null;
        var containerProvider = this.GetInterfaceInstance<IContainerProvider>();

        if (containerProvider != null)
        {
            container = containerProvider.Container;
        }

        if (!string.IsNullOrEmpty(Resource.Properties["MapNameFormat"]) && container != null)
        {
            MapObjectsList.MapId = MapHelper.SubstituteContainerProperties(
                container.ObjectProperties,
                Resource.Properties["MapNameFormat"]
                );
        }else
        {
            MapObjectsList.MapId = Resource.Properties["MapId"];
        }
        
        base.OnInit(e);
    }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_VB0_100; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditMapObjectList.ascx"; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceListObjectsNetworkMap"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
