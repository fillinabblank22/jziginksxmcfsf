﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MapObjectListView.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NetworkMaps_MapObjectListView" %>
<%@ Register TagPrefix="x" TagName="MapObjects" Src="~/Orion/NetPerfMon/Controls/MapObjectsListControl.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper" ShowEditButton="true"> 
    <Content>
        <script type="text/javascript">
            $(function () {
				<%-- this link appears when map is not found, we need to change URL to the same URL like in EditURL of the resource --%>
                var editLink = $('#<%= MapObjectsList.ObjectListClientID %> a[href *= "MapObjectListEdit.aspx?ResourceID="]');
                if (editLink.length > 0) {
                    editLink.attr('href', '<%= EditURL%>');
                }
            });
        </script>

        <x:MapObjects ID="MapObjectsList" runat="server" />
    </Content>
</orion:resourceWrapper>