﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using SolarWinds.MapEngine.Helpers;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web;
using System.Text;
using SolarWinds.MapEngine;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Resources_NetworkMaps_AllMapsCustomEdit : System.Web.UI.Page
{
    protected ResourceInfo Resource { get; set; }
    private int resourceID;

    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    protected override void OnInit(EventArgs e)
    {
        string rootLocalPath = MapService.GetTempPath();
        string applicationLocalPath = MapService.GetMapStudioInstallPath();

        string swoisEndpoint = string.Format(System.Configuration.ConfigurationManager.AppSettings["SWOISv3.RemoteEndpoint"], "localhost");
        string serverAddress, swisPort, swisPostfix;
        MapService.ParseEndpointLocation(swoisEndpoint, out serverAddress, out swisPort, out swisPostfix);

        resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
        Resource = ResourceManager.GetResourceByID(resourceID);
        Page.Title = string.Format(Resources.CoreWebContent.WEBCODE_AK0_88, Resource.Title);
        if (!string.IsNullOrEmpty(Resource.Properties["Title"]))
        {
            resourceTitleEditor.ResourceTitle = Resource.Properties["Title"];
        }
        else
        {
            resourceTitleEditor.ResourceTitle = Resource.Title;
        }

        if (!string.IsNullOrEmpty(Resource.Properties["SubTitle"]))
        {
            resourceTitleEditor.ResourceSubTitle = Resource.Properties["SubTitle"];
        }
        else
        {
            resourceTitleEditor.ResourceSubTitle = Resource.SubTitle;
        }

        List<string> filteredMapsIds = new List<string>();
        if (!string.IsNullOrEmpty(Resource.Properties["FilteredMaps"]))
        {
            filteredMapsIds.AddRange(Resource.Properties["FilteredMaps"].Split(';'));
        }
        
        string errorMessage = string.Empty;
        var credentials = CredentialsHelper.GetOrionCertificateCredentials();
        var availableMaps = MapService.GetAvailableMaps(HttpContext.Current.Profile.UserName,
            credentials,
            rootLocalPath,
            applicationLocalPath,
            serverAddress,
            swisPort,
            swisPostfix,
			0,
			false,
            ref errorMessage);

        if (!string.IsNullOrEmpty(errorMessage))
        {
            log.ErrorFormat("Error reading available maps. Exception message: {0}", errorMessage);
        }

        foreach (var map in availableMaps)
        {
            string id = map.Key.ToString().Replace("Maps: ", string.Empty);
            string displayName = GetLocalizedProperty("MapName", System.IO.Path.GetFileNameWithoutExtension(map.Value));
            ListItem item = new ListItem(displayName, id);
            item.Selected = !filteredMapsIds.Contains(id);
            mapsList.Items.Add(item);
        }

        base.OnInit(e);
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        Resource.Properties["Title"] = resourceTitleEditor.ResourceTitle;
        Resource.Properties["SubTitle"] = resourceTitleEditor.ResourceSubTitle;
        if (!String.IsNullOrEmpty(resourceTitleEditor.ResourceTitle))
        {
            Resource.Title = resourceTitleEditor.ResourceTitle;
        }else
        {
            var resourceControl = (BaseResourceControl) LoadControl(Resource.File);
            Resource.Title = resourceControl.Title;
        }
        Resource.SubTitle = resourceTitleEditor.ResourceSubTitle;
        StringBuilder list = new StringBuilder();
        
        foreach (ListItem item in mapsList.Items)
            if (!item.Selected)
            {
                list.AppendFormat("{0};", item.Value);
            }
        Resource.Properties["FilteredMaps"] = list.ToString();

        SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
        this.Response.Redirect(BaseResourceControl.GetEditPageReturnUrl(Request.QueryString, Resource.View.ViewID.ToString())); 
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }

}
