﻿using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.NetworkMaps)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsNetworkMaps.Maps)]
public partial class Orion_NetPerfMon_Resources_NetworkMaps_AllMaps : SolarWinds.Orion.Web.UI.BaseResourceControl
{
    protected override void OnInit(EventArgs e)
    {
        if (!string.IsNullOrEmpty(Resource.Properties["Title"]))
        {
            Resource.Title = Resource.Properties["Title"];
        }
        else
        {
            Resource.Title = DefaultTitle;
        }

        if (!string.IsNullOrEmpty(Resource.Properties["SubTitle"]))
        {
            Resource.SubTitle = Resource.Properties["SubTitle"];
        }

        base.OnInit(e);
    }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_VB0_104; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceAllNetworkMaps"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
