﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using System;
using SolarWinds.Orion.Core.Common.Swis;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.NetworkMaps)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsNetworkMaps.Maps)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.IsFederationAware, "true")]
public partial class Orion_NetPerfMon_Resources_NetworkMaps_NetworkMapView : SolarWinds.Orion.Web.UI.BaseResourceControl
{
    private static readonly Log Log = new Log();
    private NetworkMapControl _mapControl;
    private string _mapId = "Sample Map"; // Default map
    private string _mapType = "NonInteractive"; // Default map type => control used for rendering

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e); // has to happen first to initialize the SW QueryString

        // We want to automatically hide the blue download
        // box when user has already selected a map. 
        // For that, we need to know what guids are used
        // for sample maps, because we can then identify them.
        if (string.IsNullOrEmpty(Resource.Properties["sampleMapIds"]))
            LoadSampleMapIds();

        #region WLHM View

        // When this resource is placed on a WirelessHeatMap view, we want to select
        // a matching map for current NetObject and disable the map selection in edit mode.
        if (ShouldAutoselectMap() && IsWlhmView())
        {
            var netObjectString = QueryString["NetObject"];

            // NetObjectFactory handles the NetObject format validation, so I can expect
            // getting an integer value and skip additional checks.
            var id = int.Parse(netObjectString.Remove(0, 5));

            using (var swis = InformationServiceProxy.CreateV3())
            {
                // MapResource expects to get MapID in "GUID.OrionMap" format
                const string query = @"
                    SELECT CONCAT(wlhm.MapStudioFileID,'.OrionMap') AS FileNameGuid
                    FROM Orion.WirelessHeatMap.Map AS wlhm
                    WHERE wlhm.MapID = @mapId
                ";

                var queryParams = new Dictionary<string, object>
                {
                    {"mapId", id}
                };

                var queryData = swis.Query(query, queryParams);

                if (queryData == null || queryData.Rows.Count == 0)
                {
                    Log.WarnFormat("WLHM: No data found for NetObject {0}. Showing Sample Map instead.", netObjectString);
                    _mapId = "Sample Map";
                }
                else
                {
                    string fileNameGuid = queryData.Rows[0]["FileNameGuid"].ToString();
                    _mapId = fileNameGuid;
                    Resource.Properties["selectedMapId"] = fileNameGuid;
                }
            }
        }
        #endregion

        // Finding out type of a selected map, so we 
        // can load the correct control to render it.
        if (!string.IsNullOrEmpty(Resource.Properties["selectedMapId"]) && MapExists(Resource.Properties["selectedMapId"]))
        {
            _mapId = Path.GetFileNameWithoutExtension(Resource.Properties["selectedMapId"]);
            _mapType = GetMapType(_mapId);
        }

        // Loading supported map types and their controls
        // These are defined via plugin files in modules.
        var availableMapControls = OrionModuleManager.GetNetworkMapControls();

        // Try to load the matching control with selected map.
        try
        {
            _mapControl = (NetworkMapControl)LoadControl(availableMapControls[_mapType]);
        }
        catch (KeyNotFoundException ex)
        {
            Log.ErrorFormat("The control necessary to render a map of selected type ({0}) hasn't been found. Using the default Core control. Exception message: {1}", _mapType, ex.Message);
            _mapControl = (NetworkMapControl)LoadControl("~/Orion/NetPerfMon/Controls/NetworkMapControl.ascx");
        }

        _mapControl.MapId = _mapId;
        _mapControl.UseInlineRenderingForWeb = true;

        MapControlPlaceholder.Controls.Add(_mapControl);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        int scale = 100;
        //we are applying custom Scale to each Map in WLHMDetails page
        //so this is only for All Wireless Heat Maps resource
        if (Resource.View.ViewType == "WLHMDetails")
        {
            string mapId = Path.GetFileNameWithoutExtension(Resource.Properties["selectedMapId"]);
            string mapScaleId = string.Format("MapScale{0}", mapId);
            if (!string.IsNullOrEmpty(mapId)
                && !string.IsNullOrEmpty(Resource.Properties[mapScaleId]))
            {
                if (int.TryParse(Resource.Properties[mapScaleId], out scale))
                {
                    _mapControl.Scale = scale;
                }
            }
            else
            {
                _mapControl.Scale = scale;
            }
        }
        else if (!string.IsNullOrEmpty(Resource.Properties["mapScale"]))
        {
            if (int.TryParse(Resource.Properties["mapScale"].Replace("%", "").Trim(), out scale))
            {
                _mapControl.Scale = scale;
            }
        }
        #region SyncMap
        if (SwisFederationInfo.IsFederationEnabled)
        {
            var syncMapIfNotAvailable = Resource.Properties["SyncMapIfNotAvailable"];
            if (!String.IsNullOrEmpty(syncMapIfNotAvailable))
            {
                _mapControl.SyncMapIfNotAvailable = Convert.ToBoolean(syncMapIfNotAvailable);
                Resource.Properties.Remove("SyncMapIfNotAvailable");
            }
            var syncMapFromServerId = Resource.Properties["SyncMapFromServerId"];
            if (!String.IsNullOrEmpty(syncMapFromServerId))
            {
                _mapControl.SyncMapFromServerId = Convert.ToInt32(syncMapFromServerId);
                Resource.Properties.Remove("syncMapFromServerId");
            }
        }
        #endregion

        #region Resource properties
        //upgrade from 9.1 -> 9.5 - old map resource uses key NetworkMap
        string oldMap = Resource.Properties["NetworkMap"];
        if (!string.IsNullOrEmpty(oldMap))
        {
            Resource.Properties["selectedMapId"] = oldMap;
            Resource.Properties["NetworkMap"] = string.Empty;
        }

        // fetch container properties
        Container container = null;
        var containerProvider = this.GetInterfaceInstance<IContainerProvider>();

        if (containerProvider != null)
        {
            container = containerProvider.Container;
        }

        if (!string.IsNullOrEmpty(Resource.Properties["MapNameFormat"]) && container != null)
        {
            _mapControl.MapId = MapHelper.SubstituteContainerProperties(
                container.ObjectProperties,
                Resource.Properties["MapNameFormat"]
            );
        }
        else if (!string.IsNullOrEmpty(Resource.Properties["selectedMapId"]))
        {
            _mapControl.MapId = Resource.Properties["selectedMapId"];
        }
        else
        {
            _mapControl.MapId = "Sample Map";
        }

        // Resource title
        if (string.IsNullOrEmpty(Resource.Title))
        {
            Resource.Title = DefaultTitle;
        }

        // Caching
        if (string.IsNullOrEmpty(Resource.Properties["UseCaching"]))
        {
            _mapControl.HideCachedImage();
        }
        #endregion

        if (!Page.IsPostBack)
        {
            _mapControl.ShowColorLegend = (string.IsNullOrEmpty(Resource.Properties["ShowColorLegend"]));
            _mapControl.ShowDownloadButton(string.IsNullOrEmpty(Resource.Properties["ShowDownloadButton"]));
        }

        _mapControl.ResourceId = this.Resource.ID.ToString();
        _mapControl.TranslateMacros = SolarWinds.Orion.Web.Macros.TranslateLabelMacros;
    }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_VB0_98; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditNetworkMap.ascx"; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceNetworkMapResource"; }
    }

    public override SolarWinds.Orion.Web.UI.ResourceLoadingMode ResourceLoadingMode
    {
        get { return SolarWinds.Orion.Web.UI.ResourceLoadingMode.RenderControl; }
    }

    // TODO: Move to external class and add tests.
    private string GetMapType(string mapId)
    {
        string retVal = "NonInteractive";
        try
        {
            var argList = new List<object> { mapId };
            using (var service = InformationServiceProxy.CreateV3())
            {
                var resultXml = service.Invoke("Orion.MapStudioFiles", "GetMapStyle", CustomPropertyHelper.GetArgElements(argList));
                retVal = resultXml.InnerText;
            }
        }
        catch { } // TODO: Ask Libor about this and finish

        return retVal;
    }

    // There might be various conditions leading to this
    // behavior, hence I do not provide specific parameters.
    private bool ShouldAutoselectMap()
    {
        // WLHM View wants to select the map automatically
        return IsWlhmView();
    }

    // Wireless Heat Map Details View requires special
    // care so this check's gonna be useful.
    private bool IsWlhmView()
    {
        string netObjectString = QueryString["NetObject"];
        if (!string.IsNullOrEmpty(netObjectString)
            && netObjectString.StartsWith("WLHM:", StringComparison.OrdinalIgnoreCase))
        {
            return true;
        }
        return false;
    }


    private bool MapExists(string mapId)
    {
        mapId = Path.GetFileNameWithoutExtension(mapId);

        Guid g;
        if (!Guid.TryParse(mapId, out g))
            return true;

        using (var swis = InformationServiceProxy.CreateV3())
        {
            const string query = "SELECT FileId FROM Orion.MapStudioFiles WHERE FileId = @fileId and IsDeleted = 0";
            var param = new Dictionary<string, object>
            {
                {"fileId", mapId}
            };
            try
            {
                var queryData = swis.Query(query, param);

                return queryData != null && queryData.Rows.Count != 0;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }

    private void LoadSampleMapIds()
    {
        using (var swis = InformationServiceProxy.CreateV3())
        {
            string query = @"
                    SELECT FileId
                    FROM Orion.MapStudioFiles
                    WHERE FileName = 'Groups - Sample Map.OrionMap' 
                    OR FileName = 'Sample Map.OrionMap'";


            if (SwisFederationInfo.IsFederationEnabled)
            {
                 query += " OR FileName = 'EOC Sample Map.EOCMap'";
            }

            var queryData = swis.Query(query);

            if (queryData != null && queryData.Rows.Count != 0)
            {
                var sb = new StringBuilder();
                for (int i = 0; i < queryData.Rows.Count; i++)
                {
                    sb.Append(queryData.Rows[i]["FileId"]);
                    sb.Append(",");
                }
                //we need to set ID's of two sample maps to prevent cases when use changes their names
                //and add name 'Sample Map' because at the first initiation we dont know its ID
                Resource.Properties["SampleMapIds"] = string.Format("{0},{1}", sb.ToString(), _mapId);
            }
        }
    }

}
