<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WorldMapView.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NetworkMaps_WorldMapView" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Swis" %>

<orion:resourceWrapper runat="server" ID="Wrapper" ShowEditButton="true">
    <Content>
         <% if (SwisFederationInfo.IsFederationEnabled)
            {%>
                <div id="worldmappartialerrors"></div>
           <%}%>        
        <div id="worldmap<%= Resource.ID %>" style="height: <%= this.PgParams.MapHeight %>px;"></div>
    </Content>
</orion:resourceWrapper>
