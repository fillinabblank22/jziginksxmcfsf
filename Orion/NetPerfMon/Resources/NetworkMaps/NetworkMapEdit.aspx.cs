﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using SolarWinds.MapEngine.Helpers;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web;
using SolarWinds.MapEngine;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Resources_NetworkMaps_NetworkMapEdit : System.Web.UI.Page
{
    protected ResourceInfo Resource { get; set; }
    private int resourceID;
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    private SolarWinds.MapEngine.IEngine mapEngine;

    protected override void OnInit(EventArgs e)
    {
        string rootLocalPath = MapService.GetTempPath();
        string applicationLocalPath = MapService.GetMapStudioInstallPath();

        string swoisEndpoint = string.Format(System.Configuration.ConfigurationManager.AppSettings["SWOISv3.RemoteEndpoint"], "localhost");
        string serverAddress, swisPort, swisPostfix;
        MapService.ParseEndpointLocation(swoisEndpoint, out serverAddress, out swisPort, out swisPostfix);

        resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
        Resource = ResourceManager.GetResourceByID(resourceID);
        Page.Title = string.Format(Resources.CoreWebContent.WEBCODE_AK0_88, Resource.Title);

        if (!string.IsNullOrEmpty(Resource.Properties["Title"]))
        {
            resourceTitleEditor.ResourceTitle = Resource.Properties["Title"];
        }
        else
        {
            resourceTitleEditor.ResourceTitle = Resource.Title;
        }

        if (!string.IsNullOrEmpty(Resource.Properties["SubTitle"]))
        {
            resourceTitleEditor.ResourceSubTitle = Resource.Properties["SubTitle"];
        }
        else 
        {
            resourceTitleEditor.ResourceSubTitle = Resource.SubTitle;
        }

        string selectedMapId = Resource.Properties["selectedMapId"];
        if (!string.IsNullOrEmpty(Resource.Properties["mapScale"]))
        {
            scaleTextBox.Text = string.Format("{0}%", Resource.Properties["mapScale"].Replace("%","").Trim());
        }
        else
        {
            scaleTextBox.Text = "100%";
        }

        ShowDownloadButtonCheckbox.Checked = string.IsNullOrEmpty(Resource.Properties["ShowDownloadButton"]);
        UseCachingCheckbox.Checked = !string.IsNullOrEmpty(Resource.Properties["UseCaching"]);

        string errorMessage = string.Empty;
        string userName = HttpContext.Current.Profile.UserName;
        var usernameCredentials = CredentialsHelper.GetOrionCertificateCredentials();
        mapEngine = MapEngineFactory.GetEngine(userName, usernameCredentials, serverAddress, swisPort, swisPostfix, 0, true);
        mapEngine.RootApplicationPath = applicationLocalPath;
        mapEngine.RootLocalPath = rootLocalPath;
	    mapEngine.SetAlternativeMapWebPath();

        Dictionary<string, string> availableMaps = null;
        try
        {
            availableMaps = mapEngine.GetMapsList();
        }
        catch (Exception ex)
        {
            log.ErrorFormat("Error reading available maps. Exception message: {0}", ex.Message);
        }
    
        //fill formats ddl, only for groups (containers)
        if (!string.IsNullOrEmpty(Request["NetObject"]) && 
            Request["NetObject"].StartsWith("C:", true, CultureInfo.InvariantCulture))
        {
            foreach (var keyValuePair in MapHelper.GetMapNameFormats())
            {
                mapNameFormatsDropDown.Items.Add(new ListItem(keyValuePair.Key, keyValuePair.Value));
            }

            mapNameFormatsDropDown.SelectedValue = Resource.Properties["MapNameFormat"];
        }
        else
        {
            mapNameFormatsPanel.Visible = false;
        }

        if (availableMaps != null)
        {
            int index = 0;
            foreach (var map in (from map in availableMaps orderby map.Value ascending select map))
            {
                string id = map.Key.ToString().Replace("Maps: ", string.Empty);
                string displayName = GetLocalizedProperty("MapName", System.IO.Path.GetFileNameWithoutExtension(map.Value));
                mapsListbox.Items.Add(new ListItem(displayName, id + System.IO.Path.GetExtension(map.Value)));
                if (id == System.IO.Path.GetFileNameWithoutExtension(selectedMapId))
                {
                    mapsListbox.SelectedIndex = index;
                }
                index++;
            }
            if (mapsListbox.SelectedIndex == -1 && mapsListbox.Items.Count > 0)
            {
                mapsListbox.SelectedIndex = 0;
            }
        }

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (mapEngine != null)
        {
            mapEngine.Close();
            mapEngine = null;
        }
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (mapsListbox.SelectedItem == null)
        {
            errorMessage.Visible = true;
            return;
        }
        int scale;
        bool result = Int32.TryParse(scaleTextBox.Text.Replace("%", string.Empty), out scale); 
        if (!result || scale < 10 || scale > 250)
        {
            errorMessage2.Visible = true;
            return;
        }

        if (mapsListbox.SelectedItem != null)
        {
            Resource.Properties["selectedMapId"] = mapsListbox.SelectedValue;
            Resource.Properties["Title"] = resourceTitleEditor.ResourceTitle;
            Resource.Properties["SubTitle"] = resourceTitleEditor.ResourceSubTitle;
            if (!String.IsNullOrEmpty(resourceTitleEditor.ResourceTitle))
            {
                Resource.Title = resourceTitleEditor.ResourceTitle;
            }
            else
            {
                var resourceControl = (BaseResourceControl)LoadControl(Resource.File);
                Resource.Title = resourceControl.Title;
            }
            Resource.SubTitle = resourceTitleEditor.ResourceSubTitle;
            Resource.Properties["mapScale"] = scale.ToString();

            Resource.Properties["ShowDownloadButton"] = ShowDownloadButtonCheckbox.Checked ? string.Empty : "0";
            Resource.Properties["UseCaching"] = UseCachingCheckbox.Checked ? "1" : string.Empty;
            Resource.Properties["MapNameFormat"] = mapNameFormatsDropDown.SelectedValue;
            
            SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);

            string url = string.Format("/Orion/View.aspx?ViewID={0}", Resource.View.ViewID);

            if (!string.IsNullOrEmpty(Request["NetObject"]))
            {
                url = string.Format("{0}&NetObject={1}", url, Request["NetObject"]);
            }

            Response.Redirect(url);
        }
        
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
}
