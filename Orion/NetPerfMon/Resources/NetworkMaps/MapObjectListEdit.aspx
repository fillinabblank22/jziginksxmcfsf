<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MapObjectListEdit.aspx.cs" MasterPageFile="~/Orion/MasterPage.master" Inherits="Orion_NetPerfMon_Resources_NetworkMaps_MapObjectListEdit" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

<asp:Content ID="Content1" Runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">
    <script language="javascript" type="text/javascript">
        // will be executed on page load
        $(function () {
            if ($("select[id$=mapNameFormatsDropDown]")) {
                // adds onChange event to mapNameFormatsDropDown
                $("select[id$=mapNameFormatsDropDown]").change(
                    function () {
                        // selected value is empty
                        if ($(this).val()) {
                            // disables mapsListbox
                            $("select[id$=mapsListbox]").attr('disabled', 'disabled');
                        } else {
                            // enables mapsListbox
                            $("select[id$=mapsListbox]").removeAttr('disabled');
                        }
                    }
                );

                // if map format is not selected 
                if ($("select[id$=mapNameFormatsDropDown]").val()) {
                    // disables mapsListbox
                    $("select[id$=mapsListbox]").attr('disabled', 'disabled');
                } else {
                    // enables mapsListbox
                    $("select[id$=mapsListbox]").removeAttr('disabled');
                };
            }
        });
    </script>
    <h1><%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBCODE_AK0_88, CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Resource.Title, Request["netobject"])))) %></h1>
    <div style="padding-left: 16px;">
    <orion:EditResourceTitle runat="server" ID="resourceTitleEditor" />
    <br />
    <div id="errorMessage" visible="false" runat="server"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_164) %></div>
    <asp:ListBox runat="server" ID="mapsListbox" Width="400" Height="400"></asp:ListBox>
    <asp:Panel id="mapNameFormatsPanel" runat="server">
        <div style="font-weight: bold; padding-top: 18px"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_2) %></div>
        <asp:DropDownList ID="mapNameFormatsDropDown" runat="server" Width="400"></asp:DropDownList>
    </asp:Panel>
    <div class="sw-btn-bar"><orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" DisplayType="Primary" OnClick="SubmitClick" /></div>        

    </div>
</asp:Content>
