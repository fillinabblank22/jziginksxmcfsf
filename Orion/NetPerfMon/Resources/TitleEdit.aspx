<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true"
    CodeFile="TitleEdit.aspx.cs" Inherits="Orion_NetPerfMon_Resources_TitleEdit"
    Title="-" %>

<%@ Register TagPrefix="orion" TagName="TitleEdit" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_261) %> <asp:Label ID="ResourceName" runat="server" Text="-" ForeColor="Black" /></h1>
    <table border="0">
        <tr>
            <orion:TitleEdit runat="server" ID="TitleEditControl" />
        </tr>
        <tr>
            <td>
                <div class="sw-btn-bar"><orion:LocalizableButton ID="btnSubmit" runat="server" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitClick"/></div>
            </td>
        </tr>
    </table>
</asp:Content>