<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true" CodeFile="EditAdvancedSysLogParser.aspx.cs" Inherits="Orion_NetPerfMon_Resources_EditAdvancedSyslogParser" Title="<%$ Resources: SyslogTrapsWebContent,WEBDATA_AK0_238 %>"%>
<%@ Import Namespace="System.Web.Security.AntiXss" %>
<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/PeriodControl.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= string.Format(Resources.SyslogTrapsWebContent.WEBCODE_AK0_88, CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Resource.Title, AntiXssEncoder.HtmlEncode(Request.QueryString["netobject"],true))))%></h1>

    <table border="0" style="width:100%;" cellpadding="0" cellspacing="0">
        <tr><td>
        <p>
			<b><%= Resources.SyslogTrapsWebContent.WEBDATA_AK0_230 %></b><br/>
			<asp:TextBox runat="server" ID="tbTitle" Text="<%# Resource.Title%>" Rows="1" Columns="30" />
        </p>
        <p>
            <b><%= Resources.SyslogTrapsWebContent.WEBDATA_AK0_231 %></b><br/>
            <asp:DropDownList ID="ddlPeriod" runat="server">
                <asp:ListItem Text="<%$ Resources: SyslogTrapsWebContent,WEBDATA_AK0_233 %>" Value="minute"/>
                <asp:ListItem Text="<%$ Resources: SyslogTrapsWebContent,WEBDATA_AK0_234 %>" Value="hour" Selected="True"/>
                <asp:ListItem Text="<%$ Resources: SyslogTrapsWebContent,WEBDATA_AK0_235 %>" Value="day"/>
                <asp:ListItem Text="<%$ Resources: SyslogTrapsWebContent,WEBDATA_AK0_236 %>" Value="week"/>
                <asp:ListItem Text="<%$ Resources: SyslogTrapsWebContent,WEBDATA_AK0_237 %>" Value="month"/>
            </asp:DropDownList>
        </p>
        <p>
			<b><%= Resources.SyslogTrapsWebContent.WEBDATA_AK0_241 %></b><br/>
			<asp:TextBox runat="server" ID="tbTop"/>
			<asp:RangeValidator ID="rvTop" runat="server" ControlToValidate="tbTop" Type="Integer"
				MinimumValue="1" MaximumValue="10000"
                Display="Dynamic" ErrorMessage="<%$ Resources: SyslogTrapsWebContent,WEBDATA_AK0_239 %>" 
                SetFocusOnError="True"/>
        </p>
		</td></tr>
        <tr><td>
            <div class="sw-btn-bar"><orion:LocalizableButton ID="imbtnSubmit" runat="server" DisplayType="Primary" LocalizedText="Submit" OnClick="imbtnSubmit_Click"/></div>
        </td></tr>
    </table>
</asp:Content>