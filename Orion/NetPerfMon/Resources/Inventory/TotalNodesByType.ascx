<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TotalNodesByType.ascx.cs"
    Inherits="Orion_NetPerfMon_Resources_Inventory_TotalNodesByType" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= DefaultSanitizer.SanitizeHtml(this.CustomSqlErrorMessage) %>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="RowRepeater">
            <HeaderTemplate>
                <table class="NeedsZebraStripes" style="width: 100%">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td class="Property" style="width: 20px; text-align: center;">
                        <%# DefaultSanitizer.SanitizeHtml(Eval("Total")) %>
                    </td>
                    <td class="Property" style="text-align: left">
                        <%# DefaultSanitizer.SanitizeHtml(Eval("MachineType")) %>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:ResourceWrapper>
