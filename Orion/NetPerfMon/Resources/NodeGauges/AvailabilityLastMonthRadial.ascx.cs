using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.NPM.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Web;
using System.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Gauges)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsGaugesValues.Dials)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsGaugesValues.Meters)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class AvailabilityLastMonth : AvailabilityGauge
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }
    }

	protected void Page_Load(object sender, EventArgs e)
	{
        LoadData(GetIntProperty("Scale", 100), GetStringValue("Style", "Minimalist Dark"), HttpUtility.UrlEncode(Resources.CoreWebContent.WEBCODE_VB0_61), 
            gaugePlaceHolder1, PerfstackChartResourceUrl,
				GetCurrentNetObject.Node.NetObjectID, "Availability", "Radial", " %", 0, 100);
	}

	public override Control GetSourceControl() { return Source; }

	public override ITextControl GetErrorControl() { return (ITextControl)ErrorLiteral; }

	protected override string DefaultTitle { get { return Resources.CoreWebContent.WEBCODE_VB0_60; } }

	public override string BottomTitle() { return Resources.CoreWebContent.WEBCODE_VB0_61;	}

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }

	public override DateTime StartDate()
	{
		return DateTime.Now.AddDays(-DateTime.Now.Day + 1).AddMonths(-1).Date.AddMilliseconds(-1);
	}
	public override DateTime EndDate()
	{
		return DateTime.Now.AddDays(-DateTime.Now.Day + 1).Date;
	}


    public string PerfstackChartResourceUrl
    {
        get { return string.Format(@"/ui/perfstack/?context={1}_Orion.Nodes_{0}&withRelationships=true&presetTime=last90Days&charts={1}_Orion.Nodes_{0}-Orion.ResponseTime.Availability;", GetCurrentNetObject.Node.NodeID, 0); }
    }

}
