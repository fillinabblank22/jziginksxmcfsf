<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MinMaxResponseTimeLinear.ascx.cs" Inherits="MinMaxResponseTimeLinear" %>
<%@ Register TagPrefix="npm" Namespace="SolarWinds.NPM.Web.Gauge.V1" Assembly="SolarWinds.NPM.Web.Gauge.V1" %>
<orion:Include runat="server" File="GaugeStyle.css" />
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:PlaceHolder ID="Source" runat="server">
        <br />
        <table class="GaugeTable">
	        <tr>
		        <td>
			        <a href="<%= DefaultSanitizer.SanitizeHtml(GetMinResponseTimeString) %>" target="_blank">
			            <asp:PlaceHolder runat="server" ID="gaugePlaceHolder1" />
			        </a>
	            </td>
	        </tr>
	        <tr>
	            <td>
			        <a href="<%= DefaultSanitizer.SanitizeHtml(GetMaxResponseTimeString) %>" target="_blank">
			            <asp:PlaceHolder runat="server" ID="gaugePlaceHolder2" />
			        </a>
	            </td>
	        </tr>
       </table>
       </asp:PlaceHolder>
       <asp:Literal ID="ErrorLiteral" runat="server" />
    </Content>
    <HeaderButtons>
		<npm:ThresholdButton runat="server" />
    </HeaderButtons>
</orion:resourceWrapper>