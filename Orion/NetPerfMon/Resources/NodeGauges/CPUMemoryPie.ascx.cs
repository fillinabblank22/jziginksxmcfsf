using System;
using System.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.NPM.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Gauges)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsGaugesValues.Dials)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsGaugesValues.Meters)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class CPUMemoryPie : GaugeResource<INodeProvider>
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }
    }

	protected void Page_Load(object sender, EventArgs e)
	{
		
		if (isVisible())
        {
			ScriptManager.RegisterStartupScript(this, typeof(string), "clickpie", GetOpenChartScript(), true);
			gaugeCPU.Value = GetCurrentNetObject.Node.CPULoad;
            gaugeCPU.TitleBottom = GetCurrentNetObject.Node.CPULoad.ToString() + " %";
            gaugeCPU.TitleTop = Resources.CoreWebContent.WEBCODE_VB0_71;
            gaugeCPU.isWarning = (GetCurrentNetObject.Node.CPULoad > Thresholds.CPULoadWarning.SettingValue);

            gaugeMemory.Value = GetCurrentNetObject.Node.PercentMemoryUsed;
            gaugeMemory.TitleBottom = GetCurrentNetObject.Node.PercentMemoryUsed.ToString() + " %";
            gaugeMemory.TitleTop = Resources.CoreWebContent.WEBCODE_VB0_72;
            gaugeMemory.isWarning = (GetCurrentNetObject.Node.PercentMemoryUsed > Thresholds.PercentMemoryWarning.SettingValue);
        }
        else
        {
            Parent.Visible = false;
        }
	}

	public override Control GetSourceControl() { return Source;	}

	public override ITextControl GetErrorControl() { return (ITextControl)ErrorLiteral; }

    public override string GetErrorText()
    {
        return String.Format("{0} <a href=\"/Orion/Nodes/Default.aspx\"> {1} </a>", Resources.CoreWebContent.WEBCODE_VB0_73,
                             Resources.CoreWebContent.WEBCODE_VB0_74);
    }

	//public override string DisplayTitle { get {	return "CPU Load & Memory Utilization - Pie Gauges"; } }
	
	protected override string DefaultTitle { get { return Resources.CoreWebContent.WEBCODE_VB0_75; } }

    public override string HelpLinkFragment { get { return "OrionPHResourceCPULoadMemoryUtilization"; } }

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }

    public override bool isError()
	{
		if (base.isError()) return true;
		
        return false;
	}

    private bool isVisible()
    {
        return ((GetCurrentNetObject.Node.CPULoad >= 0) && (GetCurrentNetObject.Node.PercentMemoryUsed >= 0));
    }

    public override string EditControlLocation
    {
        get { return string.Empty; }
    }

    public string GetCPUString
    {
        get { return string.Format(@"/ui/perfstack/?context={1}_Orion.Nodes_{0}&withRelationships=true&presetTime=last12Hours&charts={1}_Orion.Nodes_{0}-Orion.CPULoad.AvgLoad;", GetCurrentNetObject.Node.NodeID, 0); }
    }

    public string GetMemoryString
    {
        get { return string.Format(@"/ui/perfstack/?context={1}_Orion.Nodes_{0}&withRelationships=true&presetTime=last12Hours&charts={1}_Orion.Nodes_{0}-Orion.CPULoad.AvgPercentMemoryUsed;", GetCurrentNetObject.Node.NodeID, 0); }
    }
}
