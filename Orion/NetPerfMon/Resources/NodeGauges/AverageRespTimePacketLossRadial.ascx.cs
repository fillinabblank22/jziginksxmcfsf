using System;
using System.Web;
using System.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.NPM.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Gauges)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsGaugesValues.Dials)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsGaugesValues.Meters)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class AverageRespTimePacketLossRadial : GaugeResource<INodeProvider>
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }
    }

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!isError())
		{
            CreateGauge(GetIntProperty("Scale", 100), GetStringValue("Style", "Minimalist Dark"), HttpUtility.UrlEncode(Resources.CoreWebContent.WEBCODE_SO0_190),
                gaugePlaceHolder1, GetResponseTimeString,
                GetCurrentNetObject.Node.NetObjectID, "AvgRespTime", "Radial", HttpUtility.UrlEncode(String.Format(" {0}", Resources.CoreWebContent.WEBDATA_VB0_151)), 0, 2500);
            CreateGauge(GetIntProperty("Scale", 100), GetStringValue("Style", "Minimalist Dark"), HttpUtility.UrlEncode(Resources.CoreWebContent.WEBCODE_VB0_81), 
                gaugePlaceHolder2, GetPacketLossString,
				GetCurrentNetObject.Node.NetObjectID, "PacketLoss", "Radial", " %", 0, 100);
		}
	}

	public override Control GetSourceControl() { return Source;	}

	public override ITextControl GetErrorControl() { return (ITextControl)ErrorLiteral; }
	
	public override string GetErrorText() {	return Resources.CoreWebContent.WEBDATA_VB0_63; }
	
	protected override string DefaultTitle { get { return Resources.CoreWebContent.WEBCODE_SO0_191; } }

    public override string HelpLinkFragment { get { return "OrionPHResourceResponseTimeGauge"; } }

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }

    public string GetResponseTimeString {
        get { return string.Format(@"/ui/perfstack/?context={1}_Orion.Nodes_{0}&withRelationships=true&presetTime=last12Hours&charts={1}_Orion.Nodes_{0}-Orion.ResponseTime.AvgResponseTime;", GetCurrentNetObject.Node.NodeID, 0); }
    }

    public string GetPacketLossString
    {
        get { return string.Format(@"/ui/perfstack/?context={1}_Orion.Nodes_{0}&withRelationships=true&presetTime=last12Hours&charts={1}_Orion.Nodes_{0}-Orion.ResponseTime.PercentLoss;", GetCurrentNetObject.Node.NodeID, 0); }
    }
}
