<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CPUMemoryPie.ascx.cs" Inherits="CPUMemoryPie" %>
<%@ Register Src="~/Orion/NetPerfMon/Controls/Pie3D.ascx" TagName="Pie3D" TagPrefix="uc1" %>
<%@ Register TagPrefix="npm" Namespace="SolarWinds.NPM.Web.Gauge.V1" Assembly="SolarWinds.NPM.Web.Gauge.V1" %>
<orion:Include runat="server" File="GaugeStyle.css" />
<orion:resourceWrapper runat="server" ShowEditButton="false" ID="Wrapper">
    <Content>
        <asp:PlaceHolder ID="Source" runat="server">
        <br />
        <table class="GaugeTable">
	        <tr>
		        <td>
			        <a href="<%= DefaultSanitizer.SanitizeHtml(GetCPUString) %>" target="_blank">
			            <uc1:Pie3D ID="gaugeCPU" runat="server" />
			        </a>
	            </td>
	            <td>
			        <a href="<%= DefaultSanitizer.SanitizeHtml(GetMemoryString) %>" target="_blank">
			            <uc1:Pie3D ID="gaugeMemory" runat="server" />
			        </a>
	            </td>
	        </tr>
       </table>
       </asp:PlaceHolder>
       <asp:Literal ID="ErrorLiteral" runat="server" />
    </Content>
    <HeaderButtons>
		<npm:ThresholdButton runat="server" />
    </HeaderButtons>
</orion:resourceWrapper>
