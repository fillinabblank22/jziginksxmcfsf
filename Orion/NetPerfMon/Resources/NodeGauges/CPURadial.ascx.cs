using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.NPM.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Web;
using System.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Gauges)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsGaugesValues.Dials)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsGaugesValues.Meters)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class CPURadial : GaugeResource<INodeProvider>
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }
    }

	protected void Page_Load(object sender, EventArgs e)
	{
        if (isVisible())
        {
            CreateGauge(GetIntProperty("Scale", 100), GetStringValue("Style", "Minimalist Dark"), HttpUtility.UrlEncode(Resources.CoreWebContent.WEBCODE_VB0_71), 
                gaugePlaceHolder1, GetCPUString,
				GetCurrentNetObject.Node.NetObjectID, "CPULoad", "Radial", " %", 0, 100);
        }
        else
        {
            Parent.Visible = false;
        }
	}

	public override Control GetSourceControl() { return Source; }

	public override ITextControl  GetErrorControl()	{ return (ITextControl)ErrorLiteral; }

    public override string GetErrorText() { return String.Format("{0} <a href=\"/Orion/Nodes/Default.aspx\"> {1} </a>", Resources.CoreWebContent.WEBCODE_VB0_73, Resources.CoreWebContent.WEBCODE_VB0_74); }
	
	protected override string DefaultTitle { get { return Resources.CoreWebContent.WEBCODE_VB0_77; } }

    public override string HelpLinkFragment { get { return "OrionPHResourceCPULoadGauge"; } }

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }

    public override bool isError()
	{
		if (base.isError()) return true;
		
        return false;
	}

    private bool isVisible()
    {
        return (GetCurrentNetObject.Node.CPULoad >= 0);
    }

    public string GetCPUString
    {
        get { return string.Format(@"/ui/perfstack/?context={1}_Orion.Nodes_{0}&withRelationships=true&presetTime=last12Hours&charts={1}_Orion.Nodes_{0}-Orion.CPULoad.AvgLoad;", GetCurrentNetObject.Node.NodeID, 0); }
    }

}
