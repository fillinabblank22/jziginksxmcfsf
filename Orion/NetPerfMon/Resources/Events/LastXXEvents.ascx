<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LastXXEvents.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Events_LastXEvents" %>
<%@ Register TagPrefix="orion" TagName="EventsList" Src="~/Orion/Controls/EventsReportControl.ascx" %>

<orion:resourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="false">
    <Content>
        <orion:EventsList runat="server" ID="EList" />
    </Content>
</orion:resourceWrapper>