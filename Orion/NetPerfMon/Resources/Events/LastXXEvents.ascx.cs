using System;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Events)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_Events_LastXEvents : BaseResourceControl
{
	// default number is used if there is no record in resource properties (this should never happened)
    private const int defaultNumberOfEvents = 25;

    #region properties

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

	// overriden main title of resources
	public override string DisplayTitle
	{
        get
        {
            if (Resource.Title.Equals(DefaultTitle, StringComparison.OrdinalIgnoreCase) && string.IsNullOrEmpty(Resource.Properties["MaxEvents"]))
            {
                string titleFormat = Resources.CoreWebContent.WEBCODE_VB0_134;

                return string.Format(titleFormat, defaultNumberOfEvents);
            }

            return Resource.Title;
        }
	}
	
	// overriden subtitle of resource
	public override string SubTitle
	{
		get
		{
			if (!string.IsNullOrEmpty(Resource.SubTitle.Trim()))
				return Resource.SubTitle;
			
			// if subtitle is not specified subtitle is a period
			string period = Resource.Properties["Period"];

			if (String.IsNullOrEmpty(period))
				return String.Empty;

			return Periods.GetLocalizedPeriod(period);
		}
	}

	// overriden dafault title
	protected override string DefaultTitle
	{
        get
        {
            return Resources.CoreWebContent.WEBCODE_VB0_135;
        }
	}

	// overriden help fragment
	public override string HelpLinkFragment
	{
        get
        {
            return "OrionPHResourceLastXXEvents";
        }
	}

	public override string EditControlLocation
	{
		get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditLastXXEvents.ascx"; }
	}

    #endregion

	// this function resolves netObjectType & netObjectID
	protected void GetCurrentNetObjectID(out int nodeID, out string netObjectType, out int netObjectID)
	{
		netObjectType = String.Empty;
		netObjectID = -1;
		nodeID = -1;

		// if is netObject a Node set filter for a node
		INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();
		if (nodeProvider != null)
		{
			netObjectType = "N";
			netObjectID = nodeProvider.Node.NodeID;
			nodeID = nodeProvider.Node.NodeID;
		}

		// if is netObject an Interface set filter for a interface
		IInterfaceProvider interfaceProvider = GetInterfaceInstance<IInterfaceProvider>();
		if (interfaceProvider != null)
		{
			netObjectType = "I";
			netObjectID = interfaceProvider.Interface.InterfaceID;
			nodeID = interfaceProvider.Interface.NodeID;
		}

		// if is netObject a Volume set filter for a volume
		IVolumeProvider volumeProvider = GetInterfaceInstance<IVolumeProvider>();
		if (volumeProvider != null)
		{
			netObjectType = "V";
			netObjectID = volumeProvider.Volume.VolumeID;
			nodeID = volumeProvider.Volume.NodeID;
		}

		//if no providers and netObject has not been defined
		//try to get netobject info from request
		if (netObjectID == -1 && !string.IsNullOrEmpty(Request.QueryString["NetObject"]))
		{
			try
			{
				NetObject netObject = NetObjectFactory.Create(Request.QueryString["NetObject"].Trim());

				if (netObject is Volume)
				{
					netObjectType = "V";
					netObjectID = ((Volume)netObject).VolumeID;
					nodeID = ((Volume)netObject).NodeID;
				}

				if (netObject is Interface)
				{
					netObjectType = "I";
					netObjectID = ((Interface)netObject).InterfaceID;
					nodeID = ((Interface)netObject).NodeID;
				}

				if (netObject is Node)
				{
					netObjectType = "N";
					netObjectID = ((Node)netObject).NodeID;
					nodeID = ((Node)netObject).NodeID;
				}
			}
			catch
			{ 
				//incorrect Netobject ID
			}
		}
	}

	protected void Page_Load(object sender, EventArgs e)
	{
	    this.ErrorPlaceHolder = this.ResourceWrapper.Content;
		if (!IsPostBack)
		{
            // custom property means that editing is enabled
			if (String.IsNullOrEmpty(Resource.Properties["Title"]))
			{
				this.ResourceWrapper.ShowEditButton = true;
				if (String.IsNullOrEmpty(Resource.Properties["Period"]))
                    Resource.Properties["Period"] = "Last 12 Months";
			}

			// try to load max event number or use default value
			int maxEvents = defaultNumberOfEvents;
			try
			{
				maxEvents = Int32.Parse(Resource.Properties["MaxEvents"]);
			}
			catch
			{
				maxEvents = defaultNumberOfEvents;
			}

			// set filter args for netobject if necessary
			string netObjectType = String.Empty;
			int netObjectID = -1;
			int nodeID = -1;
			GetCurrentNetObjectID(out nodeID, out netObjectType, out netObjectID);

			DateTime periodBegin = new DateTime();
			DateTime periodEnd = new DateTime();

			// get period from resources
			string periodName = Resource.Properties["Period"];

			if (String.IsNullOrEmpty(periodName))
                periodName = "Last 12 Months";

			// parse period to begin and end
			Periods.Parse(ref periodName, ref periodBegin, ref periodEnd);

            this.EList.LoadData(nodeID, netObjectID, netObjectType, String.Empty, -1, periodBegin, periodEnd, false, maxEvents);
            this.Errors.AddRange(this.EList.Errors);
		}
	}
}