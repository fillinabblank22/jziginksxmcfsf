<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EventSummary.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Events_EventSummary" %>
<%@ Import Namespace="SolarWinds.Orion.NPM.Web" %>
<%@ Register TagPrefix="orion" TagName="EventsSummaryList" Src="~/Orion/Controls/EventSummaryReportControl.ascx" %>

<orion:resourceWrapper ID="ResourceWrapper1" runat="server" ShowEditButton="true">
    <Content>
        <orion:EventsSummaryList runat="server" ID="EList" />
    </Content>
</orion:resourceWrapper>