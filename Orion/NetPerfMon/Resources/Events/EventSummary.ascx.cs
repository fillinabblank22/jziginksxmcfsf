using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Text;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Events)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_Events_EventSummary : BaseResourceControl
{
    #region properties

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    // overriden main title of resources
    public override string DisplayTitle
    {
        get
        {
            string title = Resource.Title;

            if (String.IsNullOrEmpty(title))
            {
                title = Resources.CoreWebContent.WEBCODE_VB0_132; 
                Resource.Properties["Title"] = title;
            }

            return title;
        }
    }

    // overriden subtitle of resource
    public override string SubTitle
    {
        get
        {
			if (!string.IsNullOrEmpty(Resource.SubTitle))
				return Resource.SubTitle;

			// if subtitle is not specified subtitle is a period
            string subTitle = Periods.GetLocalizedPeriod(Resource.Properties["Period"]);

            if (String.IsNullOrEmpty(subTitle))
            {
                subTitle = Resources.CoreWebContent.WEBCODE_VB0_133;
                Resource.Properties["Period"] = "Today";
            }

            return subTitle;
        }
    }
	
    // overriden dafault title
    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_VB0_132; }
    }

    // overriden help fragment
    public override string HelpLinkFragment
    {
        get
        {
            string helpLink = Resource.Properties["HelpLink"];
            if (String.IsNullOrEmpty(helpLink))
            {
                helpLink = "OrionPHResourceEventSummary";
                Resource.Properties["HelpLink"] = helpLink;
            }
            return helpLink;
        }
    }

    public override string EditControlLocation
	{
		get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditEventSummary.ascx"; }
	}

    #endregion

    // this function resolves netObjectType & netObjectID
    protected void GetCurrentNetObjectID(out string netObjectType, out int netObjectID)
    {
        netObjectType = String.Empty;
        netObjectID = -1;

        // if is netObject a Node set filter for a node
        INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();
        if (nodeProvider != null) 
        { 
            netObjectType = "N"; 
            netObjectID = nodeProvider.Node.NodeID;
        }

        // if is netObject an Interface set filter for a interface
        IInterfaceProvider interfaceProvider = GetInterfaceInstance<IInterfaceProvider>();
        if (interfaceProvider != null) 
        { 
            netObjectType = "I"; 
            netObjectID = interfaceProvider.Interface.InterfaceID;
        }

        // if is netObject a Volume set filter for a volume
        IVolumeProvider volumeProvider = GetInterfaceInstance<IVolumeProvider>();
        if (volumeProvider != null) 
        { 
            netObjectType = "V"; 
            netObjectID = volumeProvider.Volume.VolumeID;
        }

		if (netObjectID > -1 && !string.IsNullOrEmpty(netObjectType))
			return;

        // no provider -> take the query string
        string netObjectString = Request.QueryString["NetObject"];

        if (!String.IsNullOrEmpty(netObjectString))
        {
            string[] temp = netObjectString.Split(':');
            if (temp.Length == 2)
            {
                if (Int32.TryParse(temp[1].Trim(), out netObjectID))
                {
                    netObjectType = temp[0].Trim();
                    return;
                }
            }
        }
    }

    // resolves url arguments (for backward redirection)
    private string GetUrlArg()
    {
        string netObjectType = String.Empty;
        int netObjectID = -1;

        GetCurrentNetObjectID(out netObjectType, out netObjectID);

        StringBuilder urlArg = new StringBuilder("?");

        // resource
        urlArg.AppendFormat("ResourceID={0}", this.Resource.ID);

        // viewID
        urlArg.AppendFormat("&ViewID={0}", this.Resource.View.ViewID);

        // net object
        if (!String.IsNullOrEmpty(netObjectType) && netObjectID > 0)
            urlArg.AppendFormat("&NetObject={0}:{1}", netObjectType, netObjectID);

        return urlArg.ToString();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.ErrorPlaceHolder = this.ResourceWrapper1.Content;
        if (!IsPostBack)
        {
            // if property Custom has any value then edit mode should be active
            if (!String.IsNullOrEmpty(Resource.Properties["Custom"])) ResourceWrapper1.ShowEditButton = true;

            string netObjectType;
            int netObjectID = -1;

            GetCurrentNetObjectID(out netObjectType, out netObjectID);

            // get period from resources
            string periodName = Resource.Properties["Period"];
            
            // if there is no valid period set "Today" as default
            if (String.IsNullOrEmpty(periodName))
				periodName = "Today";

            // get list from SQL
            this.EList.LoadData(netObjectID, netObjectType, periodName);
            this.Errors.AddRange(this.EList.Errors);
        }
    }
}
