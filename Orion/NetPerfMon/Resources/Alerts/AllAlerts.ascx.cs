﻿using System.Data.SqlClient;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Alerts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsAlertValues.Alarms)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsAlertValues.Warnings)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsAlertValues.Warns)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_Alerts_AllAlerts : BaseResourceControl
{
	private bool showAcknowledgedAlerts;

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

	protected void Page_Load(object sender, EventArgs e)
	{
		string showAck = Resource.Properties["ShowAcknowledgedAlerts"];
		AlertsTable.ShowAcknowledgedAlerts = showAcknowledgedAlerts = !String.IsNullOrEmpty(showAck) && showAck.Equals("true", StringComparison.OrdinalIgnoreCase);
		AlertsTable.ResourceID = Resource.ID;

		int rowsPerPage;
		if (!int.TryParse(Resource.Properties["RowsPerPage"], out rowsPerPage))
			rowsPerPage = 5;

		AlertsTable.InitialRowsPerPage = rowsPerPage;

		AddShowAllCurrentlyTriggeredAlertsControl(this.wrapper);
	}

	protected override string DefaultTitle
	{
		get { return Resources.CoreWebContent.WEBCODE_IB0_4; }
	}

	public override string SubTitle
	{
		get
		{
			if (!string.IsNullOrEmpty(Resource.SubTitle.Trim()))
				return Resource.SubTitle;

			// if subtitle is not specified
			if (showAcknowledgedAlerts)
                return Resources.CoreWebContent.WEBCODE_IB0_5;
            else
                return Resources.CoreWebContent.WEBCODE_IB0_6;
		}
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceAllTriggeredAlerts"; }
	}

	public override string EditControlLocation
	{
		get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditAllAlerts.ascx"; }
	}

    private bool HasCurrentlyLogInUserAlertsTabAllowed()
    {
        string strQuery = @"SELECT MenuItems.Link FROM MenuBars
                            INNER JOIN MenuItems ON MenuBars.MenuItemID = MenuItems.MenuItemID
                            INNER JOIN UserTabs ON UserTabs.MenuBarID = MenuBars.MenuName
                            WHERE UserTabs.AccountID = @userName AND MenuItems.Link = @link";

        return SqlHelper.ExecuteExistsParams(strQuery, new SqlParameter("@userName", Profile.UserName), new SqlParameter("@link", "/Orion/NetPerfMon/Alerts.aspx"));
    }

	private void AddShowAllCurrentlyTriggeredAlertsControl(Control resourceWrapperContents)
	{
		if (resourceWrapperContents.TemplateControl is IResourceWrapper && HasCurrentlyLogInUserAlertsTabAllowed())
		{
			IResourceWrapper wrapper = resourceWrapperContents.TemplateControl as IResourceWrapper;
			wrapper.ManageButtonText = Resources.CoreWebContent.WEBDATA_BV0_0043;
			wrapper.ManageButtonTarget = "/Orion/NetPerfMon/Alerts.aspx";
			wrapper.ShowManageButton = true;
		}
	}
}
