<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Management.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Alerts_Management" %>
<%@ Register TagPrefix="orion" TagName="UnmanageDialog" Src="~/Orion/Controls/UnmanageDialog.ascx" %>
<%@ Register TagPrefix="orion" TagName="AcknowledgeAlertDialog" Src="~/Orion/Controls/AcknowledgeAlertDialogControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditAlertDefinition" Src="~/Orion/Controls/AlertEditDefinitionDraftDialogControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="ChangeAlertDefinitionEnabledState" Src="~/Orion/Controls/ChangeAlertDefinitionEnabledStateControl.ascx" %>

<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <orion:AcknowledgeAlertDialog ID="acknowledgeAlertDialog" runat="server" />
        <orion:ChangeAlertDefinitionEnabledState ID="ChangeAlertDefinitionEnabledState" runat="server" />
        <script id="managementTasksTemplate-<%= Resource.ID %>" type="text/x-template">
            <div class="NodeManagementIcons ui-helper-clearfix" style="margin-bottom: 7px !important;">
                    {# _.each(ManagementTasks, function(currentTask, index) { #}
                        <a style="margin-right: 15px;" href="{{ _.isEmpty(currentTask.LinkUrl) ? '#' : currentTask.LinkUrl }}"               
                           {{ _.isEmpty(currentTask.OnClick) ? '' : 'onclick="' + currentTask.OnClick + '"' }} 
                           {{ currentTask.NewWindow ? 'target="_blank"' : '' }} 
                           {{ _.isEmpty(currentTask.ClientID) ? '' : 'id="' + currentTask.ClientID + '"' }} 
                           {{ _.isEmpty(currentTask.Automation) ? '' : 'automation="' + currentTask.Automation + '"' }}>{{ currentTask.LinkInnerHtml }}</a> <span style="visibility:hidden" ></span> 
                    {# }); #}
            </div>
        </script>
        <div id="managementTasksPlaceHolder" runat="server">
        </div>
        <script type="text/javascript">
            $(function () {
                var refresh = function() {
                    SW.Core.namespace("SW.Core.Alerting.Management");
                    SW.Core.Alerting.Management.EnableDisableAlertDefinition = function(alertDefId, value) {

                        if (!value) { // turn off
                            SW.Core.MessageBox.Dialog({
                                title: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_151) %>',
                                modal: true,
                                html: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_148) %>' + '<br/><br/>',
                                buttons: [
                                    {
                                        id: 'yesTurnOffDefinition',
                                        html: '<span style="margin-right: 130px;">' +
                                            $(SW.Core.Widgets.Button('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_149) %>', { type: 'primary', id: 'yesTurnOffDefinition' })).css('margin-right', '130px;')[0].outerHTML
                                            + '</span>',
                                        handler: function(msgbox) {
                                            var onSucc = function(result) {
                                                msgbox.dialog('close');
                                                window.location.reload();
                                            };
                                            var onFail = function(result) {};
                                            SW.Orion.Alerts.ChangeAlertDefinitionEnabledStateControl.SetAlertEnabledState(alertDefId, value, onSucc, onFail);
                                        }
                                    },
                                    {
                                        id: 'noTurnOffDefinition',
                                        html: SW.Core.Widgets.Button('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_150) %>', { type: 'primary', id: 'noTurnOffDefinition' }),
                                        handler: function(msgbox) {
                                            msgbox.dialog('close');
                                        }
                                    }
                                ]
                            });
                        } else {
                            SW.Core.MessageBox.Dialog({
                                title: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_152) %>',
                                modal: true,
                                html: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_153) %>' + '<br/><br/>',
                                buttons: [
                                    {
                                        id: 'yesTurnOnDefinition',
                                        html: '<span style="margin-right: 130px;">' +
                                            $(SW.Core.Widgets.Button('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_149) %>', { type: 'primary', id: 'yesTurnOnDefinition' })).css('margin-right', '130px;')[0].outerHTML
                                            + '</span>',
                                        handler: function(msgbox) {
                                            var onSucc = function (result) {
                                                msgbox.dialog('close');
                                                window.location.reload();
                                            };
                                            var onFail = function (result) { };
                                            SW.Orion.Alerts.ChangeAlertDefinitionEnabledStateControl.SetAlertEnabledState(alertDefId, value, onSucc, onFail);
                                        }
                                    },
                                    {
                                        id: 'noTurnOnDefinition',
                                        html: SW.Core.Widgets.Button('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_150) %>', { type: 'primary', id: 'noTurnOnDefinition' }),
                                        handler: function(msgbox) {
                                            msgbox.dialog('close');
                                        }
                                    }
                                ]
                            });
                        }

                        return false;
                    };

                    var managementTasks = [];

                    <% if (this.Profile.AllowEventClear ) {  %>

                    SW.Core.Alerting.Management.AcknowledgeAlertDialog = function(alertObjectId) {
                        SW.Orion.Alerts.AcknowledgeAlertDialog.AcknowledgeAlerts([{ AlertObjectID: alertObjectId }],
                            function(success) {
                                SW.Core.Alerting.Management.UpdateAckButtonState(true, alertObjectId);
                                SW.Core.View.Refresh(true);
                            }, function(error) {});
                    };

                    SW.Core.Alerting.Management.UnAcknowledgeAlert = function(alertObjectId) {
                        SW.Orion.Alerts.AcknowledgeAlertDialog.UnacknowledgeAlert(alertObjectId,
                            function(success) {
                                SW.Core.Alerting.Management.UpdateAckButtonState(false, alertObjectId);
                                SW.Core.View.Refresh(true);
                            }, function(error) {
                            });
                    };

                    SW.Core.Alerting.Management.UpdateAckButtonState = function(isAcked, alertObjectId) {
                        $('#ackButton').find('span').text(SW.Core.Alerting.Management.GetAckIconCaptionByAckState(isAcked));
                        $('#ackButton').find('span').attr('title', SW.Core.Alerting.Management.GetAckButtonTitle(isAcked));
                        $('#ackButton').find('img').attr('src', SW.Core.Alerting.Management.GetAckIconByAckState(isAcked));
                        $('#ackButton').attr('onclick', SW.Core.Alerting.Management.GetAckHandlerByAckState(isAcked, alertObjectId));
                    };

                    SW.Core.Alerting.Management.GetAckButtonTitle = function(isAcked) {
                        if (isAcked) {
                            return '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JK0_02) %>';
                        } else {
                            return '';
                        }
                    };

                    SW.Core.Alerting.Management.GetAckHandlerByAckState = function(isAcked, alertObjectIds) {
                        var params = '(' + alertObjectIds + ')';
                        if (isAcked) {
                            return 'return SW.Core.Alerting.Management.UnAcknowledgeAlert' + params;
                        } else {
                            return 'return SW.Core.Alerting.Management.AcknowledgeAlertDialog' + params;
                        }
                    };

                    SW.Core.Alerting.Management.GetAckIconByAckState = function(isAcked) {
                        if (isAcked) {
                            return '/Orion/images/ActiveAlerts/Un-Acknowledge_icons16x16v3b.png';
                        } else {
                            return '/Orion/images/ActiveAlerts/Acknowledge_icon16x16.png';
                        }
                    };

                    SW.Core.Alerting.Management.GetAckIconCaptionByAckState = function(isAcked) {
                        if (isAcked) {
                            return ' ' + '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JK0_01) %>';
                        } else {
                            return ' ' + '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_109) %>';
                        }
                    };

                    var getAckIcoByState = function(isAcked) {
                        var titleValue = SW.Core.Alerting.Management.GetAckButtonTitle(isAcked);
                        return '<img src=' + SW.Core.Alerting.Management.GetAckIconByAckState(isAcked) + '><span title="' + titleValue + '">' + SW.Core.Alerting.Management.GetAckIconCaptionByAckState(isAcked) + '</span>';
                    };

                    var renderAckButton = function() {
                        var id = '<%= this.ActiveAlertID %>';
                        SW.Core.Services.callControllerSync("/api/ActiveAlertsGrid/GetIsAcknowledged", { AlertObjectID: id },
                            function(result) {

                                var iconPath = getAckIcoByState(result);
                                var ackHandler = SW.Core.Alerting.Management.GetAckHandlerByAckState(result, '<%= ActiveAlertID %>');

                                managementTasks.push({
                                    id: 'ackButton',
                                    ClientID: 'ackButton',
                                    LinkUrl: '#',
                                    LinkInnerHtml: iconPath,
                                    cls: 'x-btn-text-icon',
                                    NewWindow: 0,
                                    OnClick: ackHandler
                                });
                            },
                            function(result) {});
                    };

                    renderAckButton();
                    <% } %>

                    <% if (this.Profile.AllowAlertManagement) { %>
                    managementTasks.push({
                        LinkUrl: '#',
                        OnClick: 'return function() { SW.Core.Alerting.Management.EditAlertDefinition.Edit(<%= AlertDefId %>); return false; }();',
                        NewWindow: 0,
                        LinkInnerHtml: '<img src="/Orion/Nodes/Images/icons/icon_edit.gif">&nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_131) %></img>'
                    });
                    <% } %>
                   <% if (this.Profile.AllowDisableAlert) { %>
                        <% if (IsAlertDefinitionEnabled) { %>
                        var enableDIsableText = '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_132) %>';
                        var enableDisableOnClick = 'return SW.Core.Alerting.Management.EnableDisableAlertDefinition(<%= AlertDefId %>, false)';
                        <% } else { %>
                        var enableDIsableText = '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_AlertManagement_EnableAlert) %>';
                        var enableDisableOnClick = 'return SW.Core.Alerting.Management.EnableDisableAlertDefinition(<%= AlertDefId %>, true)';
                        <% } %>

                        managementTasks.push({
                            Automation: 'turnAlertDefinitionOnOff',
                            LinkUrl: '#',
                            OnClick: enableDisableOnClick,
                            NewWindow: 0,
                            LinkInnerHtml: '<img src="/Orion/Images/ActiveAlerts/TurnOffThisAlertDefinition.png">&nbsp;' + enableDIsableText + '</img>'
                        });
                        <% } else { %>
                        <% } %>

                    <% if (IsAlertForNode && HasUserRightsForTriggeredNode && Profile.AllowNodeManagement) { %>
                    <% if (!IsUnmanaged) { %>
                    managementTasks.push({
                        LinkUrl: '#',
                        OnClick: 'return showUnmanageDialog([<%= NodeID %>], \'<%= TriggeredObjectCaption %>\')',
                        NewWindow: 0,
                        LinkInnerHtml: '<img src="/Orion/Nodes/images/icons/icon_manage.gif">&nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_119) %></img>'
                    });
                    <% } else { %>

                    managementTasks.push({
                        LinkUrl: '#',
                        OnClick: 'return remanageNodes([<%= NodeID %>])',
                        NewWindow: 0,
                        LinkInnerHtml: '<img src="/Orion/Nodes/images/icons/icon_remanage.gif">&nbsp;<%= SolarWinds.Orion.Core.Common.i18n.TokenSubstitution.Parse("@{R=Core.Strings.2;K=MaintenanceMode_ManageAgain;E=xml}") %></img>'
                    });
                    <% }
                } %>

                    var source = $('#managementTasksTemplate-<%= Resource.ID %>').html();
                    var newHtml = _.template(source, { ManagementTasks: managementTasks });
                    $('#<%= managementTasksPlaceHolder.ClientID %>').html(newHtml);
                }

                SW.Core.View.AddOnRefresh(refresh, 'managementTasksTemplate-<%= Resource.ID %>');
                refresh();
            });
        </script>
        
        <orion:UnmanageDialog runat="server" ID="unmanageDialog" />
        <orion:EditAlertDefinition runat="server" ID="editAlertDefinition" />
    </Content>
</orion:ResourceWrapper>