﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Alerts)]
public partial class Orion_NetPerfMon_Resources_Alerts_TopXNodesByTriggerCountOfThisAlert : BaseResourceControl
{
    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_PS0_14; }
    }

    /// <summary>
    /// Contains uniqueidentifier of resource (Resource.ID value).
    /// </summary>
    public int UniqueClientID { get; set; }

    /// <summary>
    /// Uniqueidentifier of Active alert definition
    /// </summary>
    public int AlertDefID { get; set; }

    public bool IsGlobalAlert { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        UniqueClientID = Resource.ID;
        SortablePageableTable.UniqueClientID = Resource.ID;
        string netObject = Request["NetObject"];
        if (!string.IsNullOrEmpty(netObject))
        {
            var activeAlertProvider = GetInterfaceInstance<IActiveAlertProvider>();
            if (activeAlertProvider != null)
            {
                AlertDefID = Convert.ToInt32(activeAlertProvider.ActiveAlert.ObjectProperties["AlertDefId"]);
                string uri = Convert.ToString(activeAlertProvider.ActiveAlert.ObjectProperties["TriggeringObjectEntityUri"]);
                IsGlobalAlert = string.IsNullOrEmpty(uri);
            }            
        }

        if (IsGlobalAlert)
            ResourceWrapper.Visible = false;
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/NetPerfMon/Controls/EditResourceControls/EditTopXNodesByTriggerCountOfThisAlert.ascx";
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionCorePH-AlertDetailsTop10ObjectsbyTriggerCount";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IActiveAlertProvider) }; }
    }
}