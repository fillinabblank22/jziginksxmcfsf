﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Alerts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsAlertValues.Alarms)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsAlertValues.Warnings)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsAlertValues.Warns)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_Alerts_AllAlertThisObjectCanTrigger : BaseResourceControl
{
    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_JK0_01; }
    }
    
    public override string SubTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_JK0_02; }
    }
    
    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }
    
    public override string HelpLinkFragment
    {
        get { return "OrionCorePH-NodesAlertsNodeCanTrigger"; }
    }

    public bool EnableRelatedAlerts
    {
        get
        {
            return !Resource.Properties.ContainsKey("enable_related_alerts_radio") || bool.Parse(Resource.Properties["enable_related_alerts_radio"]);
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditAllAlertsThisObjectCanTrigger.ascx"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(INodeProvider) }; }
    }

    private string _entityName = null;
    protected string EntityName
    {
        get
        {
            if (_entityName == null)
            {
                LoadProperties();
            }

            return _entityName;
        }
    }

    private string _entityUri = null;
    protected string EntityUri
    {
        get
        {
            if (_entityUri == null)
            {
                LoadProperties();
            }

            return _entityUri;
        }
    }

    protected string CustomPropertiesColumnSettings { get; set; }

    /// <summary>
    /// Contains uniqueidentifier of resource (Resource.ID value).
    /// </summary>
    public int UniqueClientId { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        UniqueClientId = Resource.ID;
        SortablePageableTable.UniqueClientID = Resource.ID;

        if (Profile.AllowAdmin)
        {
            AddManageAlertsControl(this.ResourceWrapper);
        }

        AlertSuppressionInfoMessage.SetSuppressAlertsMessageVisibility(EntityUri);
    }

    private void LoadProperties()
    {
        NetObject netObject = GetInterfaceInstance<INetObjectProvider>().NetObject;
        if (netObject != null && !string.IsNullOrEmpty(netObject.NetObjectID))
        {
            var masterEntity = NetObjectFactory.GetSWISTypeByNetObject(netObject.NetObjectID);
            var idColumn = NetObjectTypeToSWISEntityMapper.GetIdColumn(masterEntity);
            var cpList = CustomPropertyMgr.GetCustomPropertiesForEntity("Orion.AlertConfigurationsCustomProperties").ToList();

            CustomPropertiesColumnSettings = CreateDynamicJSCodeForCustomProperties(cpList);

            using (var proxy = InformationServiceProxy.CreateV3())
            {
                string query = string.Format("SELECT Uri FROM {0} WHERE {1}={2}"
                                            , masterEntity
                                            , idColumn
                                            , netObject.NetObjectID.Split(':')[1]);
                var dt = proxy.Query(query);

                if (dt != null && dt.Rows.Count > 0)
                {
                    string uri = dt.Rows[0]["Uri"] != DBNull.Value ? Convert.ToString(dt.Rows[0]["Uri"]) : string.Empty;
                    _entityUri = uri;
                    _entityName = masterEntity;
                }
            }
        }
    }

    private string CreateDynamicJSCodeForCustomProperties(IEnumerable<CustomProperty> customPropertiesList)
    {
        var result = new StringBuilder(",");

        foreach (var cp in customPropertiesList)
        {
            string code = "\"" + cp.PropertyName + "\": "
                          + "{isHidden: false,"
                          + "caption: \"" + cp.PropertyName + "\"},";
            result.Append(code);
        }

        // remove last comma
        result = result.Remove(result.Length - 1, 1);

        return result.ToString();
    }

    private void AddManageAlertsControl(Control resourceWrapperContents)
    {
        if (resourceWrapperContents.TemplateControl is IResourceWrapper)
        {
            IResourceWrapper wrapper = resourceWrapperContents.TemplateControl as IResourceWrapper;
            wrapper.ManageButtonText = Resources.CoreWebContent.WEBCODE_PS0_16;
            wrapper.ManageButtonTarget = "/Orion/Alerts/Default.aspx";
            wrapper.ShowManageButton = Profile.AllowAlertManagement;
        }
    }
}