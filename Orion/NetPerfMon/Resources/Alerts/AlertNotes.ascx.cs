﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;


[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Alerts)]
public partial class Orion_NetPerfMon_Resources_Alerts_AlertNotes : BaseResourceControl
{
    protected override string DefaultTitle
    {
        get { return CoreWebContent.WEBCODE_JV0_5; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionCorePHResourceAlertNotes"; }
    }

    protected bool IsDemoServer
    {
        get { return SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected int ActiveAlertID { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        string netObject = Request["NetObject"];

        if (!string.IsNullOrEmpty(netObject))
        {
            var activeAlertProvider = GetInterfaceInstance<IActiveAlertProvider>();
            if (activeAlertProvider != null)
            {
                ActiveAlertID = activeAlertProvider.ActiveAlert.ActiveAlertId;
            }
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IActiveAlertProvider) }; }
    }
}