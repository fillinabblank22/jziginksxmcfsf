﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Alerts)]
public partial class Orion_NetPerfMon_Resources_Alerts_AlertDefinitionDetails : BaseResourceControl
{
    protected int AlertDefID
    {
        get;
        set;
    }

	private void AddEditAlertDefinitionControl(Control resourceWrapperContents)
	{
		if (resourceWrapperContents.TemplateControl is IResourceWrapper)
		{
			IResourceWrapper wrapper = resourceWrapperContents.TemplateControl as IResourceWrapper;
			wrapper.ManageButtonText = Resources.CoreWebContent.WEBDATA_BV0_0041;
			wrapper.ManageButtonTarget = "#" + Page.Request.Url;
            wrapper.ShowManageButton = Profile.AllowAlertManagement;
		}
	}

    protected bool IsCanned { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        string netObject = Request["NetObject"];
        if (!string.IsNullOrEmpty(netObject))
        {
            var activeAlertProvider = GetInterfaceInstance<IActiveAlertProvider>();
            if (activeAlertProvider != null)
            {
                AlertDefID = Convert.ToInt32(activeAlertProvider.ActiveAlert.ObjectProperties["AlertDefId"]);
                IsCanned = Convert.ToBoolean(activeAlertProvider.ActiveAlert.ObjectProperties["Canned"]);
            }
        }

        if (Profile.AllowAlertManagement && !IsCanned)
		{
			AddEditAlertDefinitionControl(this.ResourceWrapper);
		}
    }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_PS0_17; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionCorePH-AlertDetailsAlertDefinitionDetails";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IActiveAlertProvider) }; }
    }
}