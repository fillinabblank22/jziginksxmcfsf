<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlertDefinitionDetails.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Alerts_AlertDefinitionDetails" %>
<%@ Register tagPrefix="orion" tagName="ActionPluginMetadataProvider" src="~/Orion/Actions/Controls/ActionPluginMetadataProvider.ascx" %>

<style type="text/css">
    div.section {
        border-bottom: 1px solid #cccccc;
		padding-top: 10px;
		padding-bottom: 10px;
    }

    div.section span.label {
        font-weight: bold;
        font-size: 10pt;
        font-family: Arial;
    }

	.value-not-specified {
		color: #888888;
	}

	pre {
		font-family: Arial, Verdana, Helvetica, sans-serif;
		font-size: inherit;
		margin-top: 0px;
        white-space:pre-wrap;
	}
</style>

<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <orion:ActionPluginMetadataProvider ID="pluginInfoProvider" runat="server"/>
        <script id="alertDefinitionDetailsTemplate" type="text/x-template">
            <div class="section">
              <span class="label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0030) %></span><br/>
              <table style="margin:0; border-collapse: collapse; border: none;">
                <tr>
                  <td style="border: none;">
                    <div>
                      <span class="value">{{NameOfAlertDefinition}}</span>
                    </div>
                  </td>
                 </tr>
              </table>
            </div>
            <div class="section">
                <span class="label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0029) %></span><br/>
				{# if ((Description == "") || (Description == null)) { #}
	                <span class="value-not-specified"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0018) %></span>
				{# } else { #}
	                <span class="value">{{Description}}</span>
				{# } #}
            </div>
            <div class="section">
                <span class="label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YP0_17) %></span><br/>
                <span class="value">{{ObjectType}}</span>
            </div>
            <div class="section">
                <span class="label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0021) %></span><br/>
                <span class="value">{{Severity}}</span>
            </div>
            <div class="section">
                <span class="label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_52) %></span><br/>
                <span class="value">{{Frequency}}</span>
            </div>
            <div class="section">
				<span class="label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0006) %>: ({{CustomProperties.length}})</span><br />
                {# if (CustomProperties.length > 0) { #}
					{# _.each(CustomProperties, function(customProperty, index) { #}
						<span class="value">{{customProperty.Key}}: </span>
						{# if (customProperty.Value) { #}
							<span class="value">{{customProperty.Value}}</span><br />
						{# } else { #}
							<span class="value-not-specified"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0037) %></span><br />
						{# } #}
					{# }); #}
                {# } else { #}
					<span class="value-not-specified"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0039) %></span><br />
                {# } #}
            </div>
            <div class="section">
                <span class="label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0022) %></span><br/>
				{# if (ScopeOfThisAlert) { #}
	                <span class="value"><pre>{{ScopeOfThisAlert}}</pre></span>
				{# } else { #}
					<span class="value-not-specified"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_60) %></span>
				{# } #} 
            </div>
            <div class="section">
                <span class="label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0023) %></span><br />
                <span class="value"><pre>{{TriggerCondition}}</pre></span>
            </div>
            <div class="section">
                <span class="label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0024) %></span><br/>
                <span class="value"><pre>{{ResetCondition}}</pre></span>
            </div>
            <div class="section">
                <span class="label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0025) %></span><br/>
            {# if (TimeOfDayScheduleEnabled.length > 0 || TimeOfDayScheduleDisabled.length > 0) { #}
                    {# if (TimeOfDayScheduleEnabled.length > 0) { #}
                    <span class="value-not-specified"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_OM0_3) %></span></br>
						{# _.each(TimeOfDayScheduleEnabled, function(timeOfDayScheduleEnabled, index) { #}
							<span class="value">{{timeOfDayScheduleEnabled}}</span><br/>
						{# }); #}
					{# }#}
                    {# if (TimeOfDayScheduleDisabled.length > 0) { #}
                    <span class="value-not-specified"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_OM0_4) %></span></br>
						{# _.each(TimeOfDayScheduleDisabled, function(timeOfDayScheduleDisabled, index) { #}
							<span class="value">{{timeOfDayScheduleDisabled}}</span><br/>
						{# }); #}
					{# } #}
            {# } else { #}
						<span class="value"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0038) %></span><br/>
			{# } #}
            </div>
            <div class="section">
                <span class="label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0026) %></span><br />
				{# if (TriggerActions.length == 0) { #}
						<span class="value-not-specified"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0020) %></span>
				{# } else { #}
					{# _.each(TriggerActions, function(escalationLevel, index) { #}
						<span class="value"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0027) %> {{index}}
						<ol style="margin: 0px">
							{# _.each(escalationLevel, function(triggerAction, index) { #}
								<li>
                                    <img title="{{triggerAction.ActionDescription}}" src="{{triggerAction.IconPath}}"/> <span class="value">{{triggerAction.Name}}</span>
                                </li>
							{# }); #}
						</ol>
					{# }); #}
				{# } #}
            </div>
            <div class="section" style="border-bottom: none;">
                <span class="label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0028) %></span><br />
				{# if (ResetActions.length == 0) { #}
						<span class="value-not-specified"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0019) %></span>
				{# } else { #}
					<ol style="margin: 0px">
						{# _.each(ResetActions, function(resetAction, index) { #}
							<li>
                                <img title="{{resetAction.ActionDescription}}" src="{{resetAction.IconPath}}"/> <span class="value">{{resetAction.Name}}</span>
                            </li>
						{# }); #}
	                </ol>
				{# } #}
            </div>
        </script>
        <div id="content-<%= Resource.ID %>"></div>
        
        <script type="text/javascript">
            $(function () {
                var escapeBraces = function(originalString) {
                    return originalString.replace(new RegExp("<", 'g'), "&lt;").replace(new RegExp(">", 'g'), "&gt;");
                };

                var refresh = function () {

                    var alertDefDetailsViewModel = {
                        NameOfAlertDefinition: '',
                        Description: '',
                        ObjectType: '',
                        Severity: '',
                        Frequency: '',
                        ScopeOfThisAlert: '',
                        TriggerCondition: '',
                        ResetCondition: '',
                        TimeOfDayScheduleEnabled: [],
                        TimeOfDayScheduleDisabled: [],
                        TriggerActions: '',
                        ResetActions: '',
                        CustomProperties: []
                    };

                    var alertDefinitionDetailsTemplate = $("#alertDefinitionDetailsTemplate").html();
                    
                    SW.Core.Services.callController("/api/AlertDefinitionDetails/GetAlertDefinitionDetails",
                        '<%= AlertDefID %>',
                        function (succeed) {
                            alertDefDetailsViewModel.NameOfAlertDefinition = escapeBraces(succeed.Name);
                            alertDefDetailsViewModel.Description = escapeBraces(succeed.Description);
                            alertDefDetailsViewModel.ObjectType = succeed.ObjectTypei18n;
                            alertDefDetailsViewModel.Severity = succeed.Severityi18nMessage;
                            alertDefDetailsViewModel.Frequency = succeed.Frequency;
                            alertDefDetailsViewModel.TriggerCondition = succeed.TriggerCondition;
                            alertDefDetailsViewModel.ResetCondition = succeed.ResetCondition;
                            alertDefDetailsViewModel.TimeOfDayScheduleEnabled = succeed.TimeOfDayScheduleEnabled;
                            alertDefDetailsViewModel.TimeOfDayScheduleDisabled = succeed.TimeOfDayScheduleDisabled;
                            alertDefDetailsViewModel.ScopeOfThisAlert = succeed.ScopeOfAlertCondition;
                            alertDefDetailsViewModel.CustomProperties = [];
                            for (var prop in succeed.CustomProperties) {
                                var custPropValue = typeof succeed.CustomProperties[prop] == "undefined" || succeed.CustomProperties[prop] == null ? "" : succeed.CustomProperties[prop];
                                alertDefDetailsViewModel.CustomProperties.push({ Key: prop, Value: custPropValue });
                            }

                            // Trigger actions
                            var triggerActions = [];
                            for (var i = 0; i < succeed.TriggerActions.length; i++) {
                                var currentAction = succeed.TriggerActions[i];
                                if (typeof triggerActions[currentAction.EscalationLevel] == "undefined") {
                                    triggerActions[currentAction.EscalationLevel] = [];
                                }

                                triggerActions[currentAction.EscalationLevel].push({ Name: currentAction.Name, IconPath: currentAction.IconPath, ActionDescription: currentAction.ActionDescription });
                            }

                            alertDefDetailsViewModel.TriggerActions = triggerActions;
                            alertDefDetailsViewModel.ResetActions = succeed.ResetActions;
                            var resAlertDefinition = _.template(alertDefinitionDetailsTemplate, alertDefDetailsViewModel);
                            $("#content-<%= Resource.ID %>").html(resAlertDefinition);

                            var allowEditDefinition = '<%= this.Profile.AllowAlertManagement %>';
                            if (allowEditDefinition.toLowerCase() == 'true') {
                                $(".ResourceWrapper[resourceid=<%= Resource.ID %>] a.ManageButton").on("click", function(event) {
                                    SW.Core.Alerting.Management.EditAlertDefinition.Edit(succeed.AlertID);
                                });
                            }

                        }, function (failure) {
          
                        });
                };

                SW.Core.View.AddOnRefresh(refresh, 'content-<%= Resource.ID %>');
                refresh();
            });
        </script>
    </Content>
</orion:ResourceWrapper>