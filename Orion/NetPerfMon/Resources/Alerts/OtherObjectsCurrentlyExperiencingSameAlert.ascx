<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OtherObjectsCurrentlyExperiencingSameAlert.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Alerts_OtherNodesCurrentlyExperiencingSameAlert" %>

<%@ Register TagPrefix="orion" TagName="SortablePageableTable" Src="~/Orion/NetPerfMon/Controls/SortablePageableTable.ascx" %>

<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <orion:SortablePageableTable ID="SortablePageableTable" runat="server" />

        <input type="hidden" id="AlertDefID-<%= UniqueClientID %>" value="<%= AlertDefID %>" />
        
        <input type="hidden" id="AlertObjectID-<%= UniqueClientID %>" value="<%= AlertObjectID %>"/>

        <input type="hidden" id="IsGlobalAlert-<%= UniqueClientID %>" value="<%= IsGlobalAlert.ToString().ToLowerInvariant() %>"/>
                
        <input type="hidden" id="TriggeringObjectEntityUri-<%= UniqueClientID %>" value="<%= DefaultSanitizer.SanitizeHtml(TriggeringObjectEntityUri) %>" />
         
        <input type="hidden" id="OrderBy-<%= UniqueClientID %>" />

        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.ActiveAlerts.SortablePageableTable.initialize(
                {
                    uniqueId: '<%= Resource.ID %>',
                    pageableDataTableProvider: function(pageIndex, pageSize, onSuccess, onError) {
                        var uniqueId = '<%= Resource.ID %>';
                        var currentOrderBy = $('#OrderBy-' + uniqueId).val();
                        var alertDefId = $('#AlertDefID-' + uniqueId).val();
                        var alertObjectId = $('#AlertObjectID-' + uniqueId).val();
                        var triggeringObjectEntityUri = $('#TriggeringObjectEntityUri-' + uniqueId).val();
                        SW.Core.Services.callController("/api/OtherObjectsCurrentlyExperiencingSameAlert/GetOtherObjects", {
                            AlertDefId: alertDefId,
                            AlertObjectId: alertObjectId,
                            TriggeringObjectEntityUri: triggeringObjectEntityUri,
                            CurrentPageIndex: pageIndex, PageSize: pageSize,
                            OrderByClause: currentOrderBy,
                            LimitationIds: [<%= string.Join(",", Limitation.GetCurrentListOfLimitationIDs().ToArray()) %>]
                        }, function (result) {
                            onSuccess(result);
                            $resourceWrapperTitle = $("div.ResourceWrapper[resourceid=<%= Resource.ID %>] h1:first");
                            if ($resourceWrapperTitle.find("span").length == 0) {
                                $resourceWrapperTitle.append(SW.Core.String.Format("<span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_104) %></span>", result.TotalRows));
                            } else {
                                $resourceWrapperTitle.find("span").text(SW.Core.String.Format("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_104) %>", result.TotalRows));
                            }

                        }, function (errorResult) {
                            onError(errorResult); 
                        });

                    },
                    initialPage: 0,
                    rowsPerPage: '<%= (Resource.Properties["RowsPerPage"] != null) ? Convert.ToString(Resource.Properties["RowsPerPage"]) : "5" %>',
                    allowSort: true,
                    columnSettings: {
                        "TriggeringObjectCaption" : {
                            caption: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_78) %>',
                            isHtml: true,
                            formatter: function (cellValue, rowArray, cellInfo) {
                                var displayValue = cellValue;
                                var detailsUrl = rowArray[1];
                                if (detailsUrl)
                                    displayValue = SW.Core.String.Format('<a href="{0}">{1}</a>', detailsUrl, cellValue);
                                var entityName = rowArray[2] == "Orion.Container" ? "Orion.Groups" : rowArray[3];
                                var res = SW.Core.String.Format('<img src="{0}"/>&nbsp<span style="vertical-align: top;" >{1}</span>',
                                SW.Core.String.Format("/Orion/StatusIcon.ashx?entity={0}&amp;EntityUri='{1}'&amp;status={2}&amp;size=small", entityName, encodeURI(rowArray[2]), rowArray[4]), displayValue);
                                return res;
                            }
                        },
                        "Alert" : {
                            caption: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_79) %>',
                            isHtml: true,
                            formatter: function (cellValue, rowArray, cellInfo) {
                                var uniqueId = '<%= Resource.ID %>';
                                var isGlobalAlert = $('#IsGlobalAlert-' + uniqueId).val();
                                if (isGlobalAlert == "true")
                                    return "";

                                var res = SW.Core.String.Format('<a href="/Orion/View.aspx?NetObject=AAT:{0}" style="color: #336699; text-decoration: underline;">{1}</a>', rowArray[7], '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_77) %>');
                                return res;
                            }
                        },
                        "ActiveTime" : {
                            caption: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_80) %>'
                        },
                        "TriggerTime" : {
                            caption: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_81) %>'
                        },
                        "AcknowledgedBy" : {
                            caption: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_82) %>'
                        }
                    }
                });

                var refresh = function() { SW.Core.Resources.ActiveAlerts.SortablePageableTable.refresh(<%= Resource.ID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= SortablePageableTable.ClientID %>');
                refresh();
            });
        </script>
    </Content>
</orion:ResourceWrapper>
