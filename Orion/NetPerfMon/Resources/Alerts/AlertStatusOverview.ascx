<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlertStatusOverview.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Alerts_AlertStatusOverview" %>
<%@ Register TagPrefix="orion" TagName="AcknowledgeAlertDialog" Src="~/Orion/Controls/AcknowledgeAlertDialogControl.ascx" %>
<orion:include runat="server" File="js/MaintenanceMode/MaintenanceModeUtils.js" />
<orion:resourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <style type="text/css">
            table.statusOverview {
                background-image: url("/Orion/images/ActiveAlerts/Stripes.png");
                background-repeat: repeat;
                border-collapse: collapse;
				margin-top: 10px;
				margin-bottom: 10px;
                display: none;
            }

            table.statusOverview tbody tr td {
                border-bottom: none;
                text-align: center;
            }

            table.statusOverview td.headerCell {
                text-transform: uppercase;
                color: #888888;
            }

            table.statusOverview td.row {
                font-size: 18pt;
                font-weight: bold;
            }

        	table.statusMoreDetails {
				width: auto;
        	}

        	table.statusMoreDetails tbody tr td:first-child {
				padding-right: 10px;
        	}

            div.statusOverview {
                line-height: 160%;
                display: none;
				margin-top: 10px;
            }

            div.statusOverview span.value {
                font-weight: bold;
            }

            div.statusOverview span.value-not-specified {
                font-weight: normal;
				color: #888888;
            }

            div.statusOverview span.acknowledgedBy {
            }

            div.statusOverview table tbody tr td {
                font-size: 10pt;
                border-bottom: none;
            }

        	.seperator {
				border-bottom: solid 1px #e4e4e4;
				margin-bottom: 20px;
				margin-top: 20px;
        	}

        	td.acknowledge {
				padding-bottom: 20px;
        	}

            div.suppressionMsg {
                margin-bottom: 10px;
                display: none;
            }
        </style>

        <orion:AcknowledgeAlertDialog ID="acknowledgeAlertDialog" runat="server" />

        <table class="statusOverview" id="tableStatusOverview-<%= Resource.ID %>">
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td class="headerCell" style="border-right: 2px solid #cccccc;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_97) %></td>
                <td class="headerCell activeTime" style="border-right: 2px solid #cccccc;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_98) %></td>
                <td class="headerCell"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_90) %></td>
            </tr>
            <tr style="height: 40pt;">
                <td class="row currentStatusValue" style="border-right: 2px solid #cccccc"></td>
                <td class="row activeTime activeTimeValue" style="border-right: 2px solid #cccccc"></td>
                <td class="row severity"></td>
            </tr>
            <tr>
                <td style="border-right: 2px solid #cccccc">&nbsp;</td>
                <td class="activeTime" style="border-right: 2px solid #cccccc">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
        </table>

        <div class="statusOverview" id="divStatusOverview-<%= Resource.ID %>">
            <span style="color: #888888; text-transform: uppercase; font-size: 9pt;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_102) %></span><br />
            <span class="alertMessage"></span>

            <div class="seperator"></div>

            <span style="color: #888888; text-transform: uppercase; font-size: 9pt;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_103) %></span><br />
			<table class="statusMoreDetails">
				<tbody>
					<tr><td><span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_87) %></span></td><td><span class="value triggerTime"></span></td></tr>
					<tr><td><span><%= HttpUtility.HtmlEncode(Resources.CoreWebContent.WEBDATA_PS0_88) %></span></td><td><span class="value triggeredBy"></span></td></tr>
					<tr><td><span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_89) %></span></td><td><span class="value alertDefinitionName"></span></td></tr>
					<tr><td><span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_90) %></span></td><td><span class="value escalationLevel"></span>&nbsp;<span class="willEscalateIn"></span><img class="alertDetail" src="/Orion/images/Show_Last_Note_icon16x16.png" /></td></tr>
					<tr><td><span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_91) %></span></td><td><span class="acknowledgedBy"></span></td></tr>
					<tr><td></td><td class="acknowledge"></td></tr>
				</tbody>
			</table>
			<div class="sw-suggestion sw-suggestion-info suppressionMsg" id="alertSuppressionMessage-<%= Resource.ID %>">
				<span class='sw-suggestion-icon'></span>
				<span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AlertStatusMutedAlertsLabel) %></span>
			    <a href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("orioncorealertstriggerifmuted")) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.MutingAlerts_LearnMore) %></a>
		    </div>

        </div>
        <script type="text/javascript">
            $(function () {

                var refresh = function () {
                    SW.Core.Services.callController("/api/AlertStatusOverview/GetAlertStatus", { AlertObjectID: '<%= AlertObjectID %>' }, function (result) {
                        var TriggeredStatusEnum = {
                            NotTriggered: 0,
                            Triggered: 1
                        };

                        var SeverityEnum = {
                            Information: 0,
                            Warning: 1,
                            Critical: 2,
                            Serious: 3,
                            Notice: 4
                        };

                        var uniqueId = '<%= Resource.ID %>';
                        var allowToAck = '<%= this.Profile.AllowEventClear %>';
                        var showSuppressionMessage = result.Status == TriggeredStatusEnum.Triggered;
                        $tableStatusOverview = $("#tableStatusOverview-" + uniqueId);
                        $divStatusOverview = $("#divStatusOverview-" + uniqueId);
                        if (result.Status == TriggeredStatusEnum.NotTriggered) { // Not triggered yet alert
                            $tableStatusOverview.find(".activeTime").hide();
                            $tableStatusOverview.find(".currentStatusValue").text("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_92) %>");
                            $divStatusOverview.hide();
                            $divStatusOverview.find(".triggerTime").hide();
                        } else {
                            $tableStatusOverview.find(".activeTimeValue").text(result.ActiveTimeDisplay);
                            $tableStatusOverview.find(".currentStatusValue").text("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_93) %>");
                            $tableStatusOverview.find(".activeTime").show();
                            $divStatusOverview.show();
                            $divStatusOverview.find(".triggerTime").show();
                        }

                        $tableStatusOverviewSeverity = $tableStatusOverview.find(".severity");
                        switch (result.Severity) {
                            case SeverityEnum.Information:
                                $tableStatusOverviewSeverity.text("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_99) %>");
                                if (result.Status == TriggeredStatusEnum.Triggered) {
                                    $tableStatusOverviewSeverity.css({ "color": "#0099FF", "font-weight": "bold" });
                                } else {
                                    $tableStatusOverviewSeverity.css({ "color": "#888888", "font-weight": "normal" });
                                }
                                break;
                            case SeverityEnum.Warning:
                                $tableStatusOverviewSeverity.text("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_100) %>");
                                if (result.Status == TriggeredStatusEnum.Triggered) {
                                    $tableStatusOverviewSeverity.css({ "color": "#F99D1A", "font-weight": "bold" });
                                } else {
                                    $tableStatusOverviewSeverity.css({ "color": "#888888", "font-weight": "normal" });
                                }

                                break;
                            case SeverityEnum.Critical:
                                $tableStatusOverviewSeverity.text("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_101) %>");
                                if (result.Status == TriggeredStatusEnum.Triggered) {
                                    $tableStatusOverviewSeverity.css({ "color": "red", "font-weight": "bold" });
                                } else {
                                    $tableStatusOverviewSeverity.css({ "color": "#888888", "font-weight": "normal" });
                                }
                                break;
                            case SeverityEnum.Notice:
                                $tableStatusOverviewSeverity.text("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JV0_19) %>");
                                if (result.Status == TriggeredStatusEnum.Triggered) {
                                    $tableStatusOverviewSeverity.css({ "color": "royalblue", "font-weight": "bold" });
                                } else {
                                    $tableStatusOverviewSeverity.css({ "color": "#888888", "font-weight": "normal" });
                                }
                                break;

                            case SeverityEnum.Serious:
                                $tableStatusOverviewSeverity.text("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JV0_20) %>");
                                if (result.Status == TriggeredStatusEnum.Triggered) {
                                    $tableStatusOverviewSeverity.css({ "color": "salmon", "font-weight": "bold" });
                                } else {
                                    $tableStatusOverviewSeverity.css({ "color": "#888888", "font-weight": "normal" });
                                }
                                break;
                            default:
                                break;
                        }

                        $divStatusOverview.find(".alertMessage").html(result.Message.split("\n").join("<br />"));
                        $divStatusOverview.find(".triggerTime").html(result.TriggerTimeInDisplayFormat);
                        var elementClass = '', href = '';
                        //global alert
                        if (result.TriggeringObjectEntityUri == "" || result.TriggeringObjectEntityUri == null) {
                            elementClass = ' Class="NetObjectLink ForceTip"';
                            href = window.location.href;
                        } else {
                            href = result.TriggeringObjectDetailsUrl;
                        }
                        var triggeredBy = SW.Core.String.Format('<a href="{0}"{2}>{1}</a>', href, result.TriggeredBy, elementClass);
                        var entityName = (result.TriggeringObjectEntityName == "Orion.Container") ? "Orion.Groups" : result.TriggeringObjectEntityName;
                        triggeredBy = SW.Core.String.Format('<img src="{0}"/>&nbsp<span style="vertical-align: top;" >{1}</span>',
                                        SW.Core.String.Format("/Orion/StatusIcon.ashx?entity={0}&amp;EntityUri='{1}'&amp;status={2}&amp;size=small", entityName, encodeURI(result.TriggeringObjectEntityUri), result.TriggeringObjectStatus), triggeredBy);

                        $divStatusOverview.find(".triggeredBy").html(triggeredBy);
                        $divStatusOverview.find(".alertDefinitionName").html(result.AlertDefinition);
                        var escalationLevelMessage = SW.Core.String.Format("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_94) %>", result.EscalationLevel);
                        $divStatusOverview.find(".escalationLevel").html(escalationLevelMessage);

                        if (!IsZeroTimeStamp(result.NextLevelWillBeIn)) {
                            var nextLevelMessage = SW.Core.String.Format("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_95) %>", result.EscalationLevel + 1, "");
                            $divStatusOverview.find(".willEscalateIn").html(nextLevelMessage);
                        } else {
                            $divStatusOverview.find(".willEscalateIn").html("");
                        }

                        if ((result.AcknowledgedBy == "") || (result.AcknowledgedBy == null)) {
                            $divStatusOverview.find(".acknowledgedBy").html("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_96) %>").css("color", "red");
                    	}
                    	else {
                    	    $divStatusOverview.find(".acknowledgedBy").html(result.AcknowledgedBy);
                    	}

                        $imgDetailTooltip = $divStatusOverview.find("img.alertDetail");
                        $tooltipContent = null;
                        SW.Core.Services.callController("/api/AlertStatusOverview/GetEscalationTooltipData", {
                            AlertDefID: '<%= AlertDefID %>',
                            AlertObjectID: '<%= AlertObjectID %>',
                            ActiveNetObject: '<%= TriggeringObjectEntityUri %>',
                        },
                            function (result) {
                                var createDateText = function (minutesCount) {
                                    var daysRemaining = Math.floor(minutesCount / MINUTES_PER_DAY);
                                    var hoursRemaining = Math.floor((minutesCount - daysRemaining * MINUTES_PER_DAY) / MINUTES_PER_HOUR);
                                    var minutesRemaining = minutesCount % MINUTES_PER_HOUR;

                                    var remainingText = '';
                                    if (daysRemaining > 0) {
                                        remainingText += ' ';
                                        if (daysRemaining == 1) remainingText += '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_AlertStatus_EscTooltip_OneDayRemaining) %>';
                                        else remainingText += SW.Core.String.Format('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_AlertStatus_EscTooltip_DaysRemaining) %>', daysRemaining);
                                    }

                                    if (hoursRemaining > 0) {
                                        remainingText += ' ';
                                        if (hoursRemaining == 1) remainingText += '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_AlertStatus_EscTooltip_OneHourRemaining) %>';
                                        else remainingText += SW.Core.String.Format('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_AlertStatus_EscTooltip_HoursRemaining) %>', hoursRemaining);
                                    }

                                    if (minutesRemaining > 0) {
                                        remainingText += ' ';
                                        if (minutesRemaining == 1) remainingText += '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_AlertStatus_EscTooltip_OneMinRemaining) %>';
                                        else remainingText += SW.Core.String.Format('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_AlertStatus_EscTooltip_MinsRemaining) %>', minutesRemaining);
                                    }
                                    return remainingText;
                                }

                                var value = '<div style="display: none; position: absolute; padding: 5px; background: white; border: 1px solid #cccccc">';

                                var minutesSinceTrigger = (new Date() - new Date(result.LastTriggerTime)) / (1000 * 60);
                                var isCompleted = true;
                                var lastEscalationLevelWaitTime = 0;
                                var progressPainted = false;

                                var MINUTES_PER_HOUR = 60;
                                var MINUTES_PER_DAY = MINUTES_PER_HOUR * 24;

                                for (var i = 0; i < result.EscalationLevels.length; i++) {
                                    var escLevel = result.EscalationLevels[i];

                                    isCompleted = escLevel.WaitMinutes < minutesSinceTrigger;

                                    if (escLevel.WaitMinutes > 0) {// paint wait time label
                                        var inProgress = false;
                                        if (!isCompleted && !progressPainted) {
                                            value += "<img src='/Orion/images/AJAX-Loader.gif' alt='.' />&nbsp;";
                                            inProgress = true;
                                            progressPainted = true;
                                        } else {
                                            value += '<span style="margin-left:22px"></span>';
                                        }

                                        if (inProgress) {
                                            value += '<span style="color: green;">';
                                        }
                                        else if (isCompleted) {
                                            value += '<span style="color: gray;">';
                                        }

                                        var actualLevelWaitMinutes = escLevel.WaitMinutes - lastEscalationLevelWaitTime;
                                        var actualLevelWaitText = createDateText(actualLevelWaitMinutes);
                                        value += '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_AlertStatus_EscTooltip_Wait) %>' + ' ' + actualLevelWaitText;

                                        if (inProgress) {
                                            var remainingMinutes = Math.ceil(escLevel.WaitMinutes - minutesSinceTrigger);
                                            var remainingText = createDateText(remainingMinutes);

                                            value += " -" + remainingText + ' ' + '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_AlertStatus_EscTooltip_Remaining) %>';
                                            value += '</span>';
                                        }
                                        else if (isCompleted) {
                                            value += '</span>';
                                        }

                                        value += '<br/><br/>';
                                    }

                                    // paint escalation level header
                                    value += '<b>' + SW.Core.String.Format('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_AlertStatus_EscTooltip_EscLevel) %>', (i + 1)) + '</b><br/>';

                                    if (escLevel.Items.length > 0) {
                                        for (var j = 0; j < escLevel.Items.length; j++) {
                                            var action = escLevel.Items[j];

                                            // paint item
                                            if (action.ActionStatus === 1) { //Success
                                                value += "<img src='/Orion/Images/ActiveAlerts/Check.png' alt='.' />&nbsp;";
                                            } else if (action.ActionStatus === 2) { //Failure
                                                value += "<img src='/Orion/Images/ActiveAlerts/HistoryResourceEvents/Event_5.png' alt='.' />&nbsp;";
                                            } else {
                                                value += '<span style="margin-left:22px"></span>';
                                            }

                                            value += '<img title="' + action.IconTitle + '" src="' + action.IconPath + '">';

                                            if (isCompleted)
                                                value += '<span style="color: gray;">';
                                            value += SW.Core.String.Format('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_AlertStatus_EscTooltip_Action) %>', (j + 1), action.Title);
                                            if (!isCompleted)
                                                value += '<span style="color: gray;">';
                                            if (action.RepeatMinutes > 0) {
                                                value += ' - ';
                                                value += '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_AlertStatus_EscTooltip_RepeatEvery) %>' + createDateText(action.RepeatMinutes);
                                            }
                                            value += '<br/></span>';
                                        }
                                    } else {// no action specified for this esc level
                                        value += '<span style="color: gray; margin-left:22px;">'+ '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JK0_03) %>' +'<br/></span>';
                                    }
                                    lastEscalationLevelWaitTime = escLevel.WaitMinutes;
                                }

                                value += '</div>';
                                $tooltipContent = $(value);
                                if (result.EscalationLevels.length === 0) {
                                    $imgDetailTooltip.hide();
                                    $divStatusOverview.find(".escalationLevel").html('<span class="value-not-specified"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_AlertStatus_NoActionsDefined) %></span>');
                                    } else {
                                        $tooltipContent.appendTo('body');
                                    }
                            });

                        $imgDetailTooltip.hover(function () {
                            if ($tooltipContent !== null)
                                $tooltipContent.fadeIn('slow');
                        }, function () {
                            if ($tooltipContent !== null)
                                $tooltipContent.fadeOut('slow');
                        }).mousemove(function (event) {
                            if ($tooltipContent)
                                $tooltipContent.css({ top: event.pageY - 15, left: event.pageX + 15 });
                        });

                        if (((result.AcknowledgedBy == "") || (result.AcknowledgedBy == null)) && allowToAck.toLowerCase() == 'true') {
                            $divStatusOverview.find(".acknowledge").empty();
                            $(SW.Core.Widgets.Button('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_110) %>', { type: 'primary' })).appendTo($divStatusOverview.find(".acknowledge")).click(function () {
                                var onSucc = function (result) {
                                    SW.Core.View.Refresh(true);
                                }

                                var onFail = function (errorResult) {
                                    alert(errorResult);
                                }

                                SW.Orion.Alerts.AcknowledgeAlertDialog.AcknowledgeAlerts([{ AlertObjectID: '<%= AlertObjectID %>' }], onSucc, onFail);
                            });
                        }
                        else {
                            $divStatusOverview.find(".acknowledge").html("");
                        }

                        $tableStatusOverview.show();
                        $divStatusOverview.show();

                        SW.Core.Services.callControllerAction("/api/AlertSuppression", "GetAlertSuppressionState", { "uris": ['<%= TriggeringObjectEntityUri %>'] }, function (result) {
                            var muted = result[0].SuppressionMode === SW.Core.MaintenanceMode.EntityAlertSuppressionMode.SuppressedByItself;
                            $("#alertSuppressionMessage-" + uniqueId).toggle(showSuppressionMessage && muted);
                        }, function (errorResult) { });
                    }, function (errorResult) {

                    });
                }

                var IsZeroTimeStamp = function (timeStamp) {
                    var parts = timeStamp.split(":");
                    if (parts.length != 3)
                        throw Error("Passed parameter is not timestamp value.");

                    var hours = parseInt(parts[0]);
                    var minutes = parseInt(parts[1]);
                    var seconds = parseInt(parts[2]);
                    return (hours == 0) && (minutes == 0) && (seconds == 0);
                }

                SW.Core.View.AddOnRefresh(refresh, 'tableStatusOverview-<%= Resource.ID %>');
                refresh();
            });
        </script>
    </Content>
</orion:resourceWrapper>

