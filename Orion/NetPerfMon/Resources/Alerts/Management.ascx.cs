﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Alerts)]
public partial class Orion_NetPerfMon_Resources_Alerts_Management : BaseResourceControl
{
    protected int ActiveAlertID { get; set; }

    protected bool IsAlertForNode { get; set; }

    protected bool HasUserRightsForTriggeredNode { get; set; }

    protected bool IsUnmanaged { get; set; }

    protected string TriggeredObjectCaption { get; set; }

    protected int NodeID { get; set; }

    protected string AlertDefId { get; set; }

    /// <summary>
    /// (get, set) Return true if alert definition is enabled; otherwise return false.
    /// </summary>
    protected bool IsAlertDefinitionEnabled { get; set; }

    private int GetNodeStatus(string entityName, string entityUri)
    {
        int status = 0;
        if (!string.IsNullOrEmpty(entityUri) && !string.IsNullOrEmpty(entityName))
        {
            string swqlQuery = string.Format("SELECT Status FROM {0} (nolock=true) WHERE Uri=@uri", entityName);
            using (var proxy = InformationServiceProxy.CreateV3())
            {
                DataTable tblStatus = proxy.Query(swqlQuery, new Dictionary<string, object> { { "uri", entityUri } });
                if (tblStatus.Rows.Count > 0)
                {
                    status = tblStatus.Rows[0]["Status"] != DBNull.Value ? Convert.ToInt32(tblStatus.Rows[0]["Status"]) : status;
                }
            }
        }

        return status;
    }

    private int GetNodeIdForNodeEntity(string entityUri)
    {
        int nodeId = 0;
        string swqlQuery = "SELECT NodeID FROM Orion.Nodes (nolock=true) WHERE Uri=@uri";
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            DataTable tblNode = proxy.Query(swqlQuery, new Dictionary<string, object> { { "uri", entityUri } });
            if (tblNode.Rows.Count > 0)
            {
                nodeId = tblNode.Rows[0]["NodeID"] != DBNull.Value ? Convert.ToInt32(tblNode.Rows[0]["NodeID"]) : 0;
            }
        }

        return nodeId;
    }

    protected bool isCanned { set; get; }

    string swqlQuery = "SELECT Enabled, Canned FROM Orion.AlertConfigurations (nolock=true) WHERE AlertID=@alertId";
    private void InitiateAlertProperties(string alertDefId)
    {
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            DataTable tblConf = proxy.Query(swqlQuery, new Dictionary<string, object> { { "alertId", alertDefId } });
            if (tblConf.Rows.Count > 0)
            {
                IsAlertDefinitionEnabled = tblConf.Rows[0]["Enabled"] != DBNull.Value ? Convert.ToBoolean(tblConf.Rows[0]["Enabled"]) : false;
                isCanned = tblConf.Rows[0]["Canned"] != DBNull.Value ? Convert.ToBoolean(tblConf.Rows[0]["Canned"]) : false;
            }
        }
    }

    private bool HasCurrentUserRightsForNodeOnThisAlertWasTriggered(string nodeSwisUri)
    {
        bool hasRights = false;
        string swqlQuery = "SELECT Uri FROM Orion.Nodes (nolock=true) WHERE Uri=@uri";
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            DataTable tblNode = proxy.Query(swqlQuery, new Dictionary<string, object> { { "uri", nodeSwisUri } });
            hasRights = tblNode.Rows.Count > 0;
        }

        return hasRights;
    }

    private void AddManageAlertsControl(Control resourceWrapperContents)
    {
        if (resourceWrapperContents.TemplateControl is IResourceWrapper)
        {
            IResourceWrapper wrapper = resourceWrapperContents.TemplateControl as IResourceWrapper;
            wrapper.ManageButtonText = Resources.CoreWebContent.WEBCODE_PS0_16;
            wrapper.ManageButtonTarget = "/Orion/Alerts/Default.aspx";
            wrapper.ShowManageButton = Profile.AllowAlertManagement;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string netObject = Request["NetObject"];
        IsAlertForNode = false;

        if (!string.IsNullOrEmpty(netObject))
        {
            var activeAlertProvider = GetInterfaceInstance<IActiveAlertProvider>();
            if (activeAlertProvider != null)
            {
                ActiveAlertID = activeAlertProvider.ActiveAlert.ActiveAlertId;
                AlertDefId = (activeAlertProvider.ActiveAlert.ObjectProperties["AlertDefId"] != DBNull.Value) ? Convert.ToString(activeAlertProvider.ActiveAlert.ObjectProperties["AlertDefId"]) : string.Empty;
                string triggeringObjectEntityName = activeAlertProvider.ActiveAlert.ObjectProperties["TriggeringObjectEntityName"] != null ? Convert.ToString(activeAlertProvider.ActiveAlert.ObjectProperties["TriggeringObjectEntityName"]) : string.Empty;
                if (triggeringObjectEntityName.Equals("Orion.Nodes"))
                {
                    IsAlertForNode = true;
                    string entityUri = activeAlertProvider.ActiveAlert.ObjectProperties["TriggeringObjectEntityUri"] != null ? Convert.ToString(activeAlertProvider.ActiveAlert.ObjectProperties["TriggeringObjectEntityUri"]) : string.Empty;
                    HasUserRightsForTriggeredNode = !string.IsNullOrEmpty(entityUri) && HasCurrentUserRightsForNodeOnThisAlertWasTriggered(entityUri);
                    int nodeStatus = GetNodeStatus("Orion.Nodes", entityUri);
                    IsUnmanaged = nodeStatus == 9;
                    TriggeredObjectCaption = activeAlertProvider.ActiveAlert.ObjectProperties["TriggeringObjectCaption"] != null ? HttpUtility.HtmlEncode(Convert.ToString(activeAlertProvider.ActiveAlert.ObjectProperties["TriggeringObjectCaption"])) : string.Empty;
                    NodeID = !string.IsNullOrEmpty(entityUri) ? GetNodeIdForNodeEntity(entityUri) : 0;
                }
            }
        }

        if (!Page.IsPostBack)
            InitiateAlertProperties(AlertDefId);

        if (Profile.AllowAdmin)
        {
            AddManageAlertsControl(this.ResourceWrapper);
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBDATA_VB0_114; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionCorePH-AlertDetailsAlertDefinitionManagement";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IActiveAlertProvider) }; }
    }
}