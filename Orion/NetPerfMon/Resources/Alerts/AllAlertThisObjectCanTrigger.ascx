<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllAlertThisObjectCanTrigger.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Alerts_AllAlertThisObjectCanTrigger" %>
<%@ Register TagPrefix="orion" TagName="AlertSuppressionInfoMessage" Src="~/Orion/NetPerfMon/Controls/AlertSuppressionInfoMessage.ascx" %>

<%@ Register TagPrefix="orion" TagName="SortablePageableTable" Src="~/Orion/NetPerfMon/Controls/SortablePageableTable.ascx" %>

<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <orion:AlertSuppressionInfoMessage ID="AlertSuppressionInfoMessage" runat="server" />
        <orion:SortablePageableTable ID="SortablePageableTable" runat="server" RowSeparator="true"/>
        
        <script type="text/javascript">
            $(function() {
                SW.Core.Resources.ActiveAlerts.SortablePageableTable.initialize(
                {
                    uniqueId: '<%= Resource.ID %>',
                    pageableDataTableProvider: function(pageIndex, pageSize, onSuccess, onError) {
                        var uniqueId = '<%= Resource.ID %>';
                        var currentOrderBy = $('#OrderBy-' + uniqueId).val();
                        var eName = '<%= this.EntityName%>';
                        var eUri = '<%=this.EntityUri%>';
                        var enableRelatedAlerts = '<%=this.EnableRelatedAlerts%>';

                        SW.Core.Services.callController("/api/AllAlertThisObjectCanTrigger/GetAlerts",
                        {
                            AlertDefId: 1,
                            EntityName: eName,
                            EnableRelatedAlerts: enableRelatedAlerts,
                            TriggeringObjectEntityUri: eUri, 
                            CurrentPageIndex: pageIndex,
                            PageSize: pageSize,
                            OrderByClause: currentOrderBy
                        }, function(result) {
                            onSuccess(result);
                            $resourceWrapperTitle = $("div.ResourceWrapper[resourceid=<%= Resource.ID %>] h1:first");
                            if ($resourceWrapperTitle.find("span").length == 0)
                            {
                                $resourceWrapperTitle.append(SW.Core.String.Format("<span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_104) %></span>", result.TotalRows));
                            }
                            else
                            {
                                $resourceWrapperTitle.find("span").text(SW.Core.String.Format("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_104) %>", result.TotalRows));
                            }
                        }, function(errorResult) {
                            onError(errorResult);
                        });
                    },

                    initialPage: 0,
                    rowsPerPage: '<%= (Resource.Properties["RowsPerPage"] != null) ? Convert.ToString(Resource.Properties["RowsPerPage"]) : "5" %>',
                    allowSort: false,
                    columnSettings: {
                        "AlertName": {
                            isHidden: false,
                            caption: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_154) %>".toUpperCase()
                        },
                        "Description": {
                            isHidden: false,
                            caption: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_155) %>".toUpperCase()
                        },
                        "Severity": {
                            isHidden: false,
                            caption: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_156) %>".toUpperCase()
                        }
                        <%= CustomPropertiesColumnSettings %> 
                    }
                });

                var refresh = function() { SW.Core.Resources.ActiveAlerts.SortablePageableTable.refresh(<%= Resource.ID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= SortablePageableTable.ClientID %>');
                refresh();
            });
        </script>
    </Content>
</orion:ResourceWrapper>