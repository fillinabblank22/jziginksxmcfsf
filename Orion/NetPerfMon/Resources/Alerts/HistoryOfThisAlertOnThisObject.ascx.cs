﻿using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Alerts)]
public partial class Orion_NetPerfMon_Resources_Alerts_HistoryOfThisAlertOnThisObject : BaseResourceControl
{
    /// <summary>
    /// Contains uniqueidentifier of resource (Resource.ID value).
    /// </summary>
    public int UniqueClientID { get; set; }

    /// <summary>
    /// Uniqueidentifier of Active alert definition
    /// </summary>
	public int AlertObjectID { get; set; }

    public bool IsGlobalAlert { get; set; }

    protected string TimePeriod { get; set; }
		    
    protected void Page_Load(object sender, EventArgs e)
    {
        UniqueClientID = Resource.ID;
        SortablePageableTable.UniqueClientID = Resource.ID;
        string netObject = Request["NetObject"];
        if (!string.IsNullOrEmpty(netObject))
        {
            var activeAlertProvider = GetInterfaceInstance<IActiveAlertProvider>();
            if (activeAlertProvider != null)
            {
				AlertObjectID = Convert.ToInt32(activeAlertProvider.ActiveAlert.ObjectProperties["AlertObjectID"]);
                string uri = Convert.ToString(activeAlertProvider.ActiveAlert.ObjectProperties["TriggeringObjectEntityUri"]);
                IsGlobalAlert = string.IsNullOrEmpty(uri);
            }
        }

		TimePeriod = Resource.Properties["Period"];

		if (String.IsNullOrEmpty(TimePeriod))
			TimePeriod = Periods.GetPeriod("Last 7 Days").Name;
        		        
        if (IsGlobalAlert)
            Resource.Title = Resources.CoreWebContent.WEBCODE_ED0_3;
    }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_AY0_93; }
    }

	// overriden subtitle of resource
	public override string SubTitle
	{
		get
		{
			if (!string.IsNullOrEmpty(Resource.SubTitle.Trim()))
				return Resource.SubTitle;

			if (String.IsNullOrEmpty(TimePeriod))
				return String.Empty;

			return Periods.GetLocalizedPeriod(TimePeriod);
		}
	}

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/NetPerfMon/Controls/EditResourceControls/EditHistoryOfThisAlertOnThisObject.ascx";
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionCorePH-AlertDetailsAlertHistoryperObject";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IActiveAlertProvider) }; }
    }
}