<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXObjectsByTriggerCountOfThisAlert.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Alerts_TopXNodesByTriggerCountOfThisAlert" %>

<%@ Register TagPrefix="orion" TagName="SortablePageableTable" Src="~/Orion/NetPerfMon/Controls/SortablePageableTable.ascx" %>

<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <orion:SortablePageableTable ID="SortablePageableTable" runat="server" />
        
        <input type="hidden" id="AlertDefID-<%=UniqueClientID %>" value="<%= AlertDefID %>" />
        
        <input type="hidden" id="OrderBy-<%= UniqueClientID %>" />

        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.ActiveAlerts.SortablePageableTable.initialize(
                {
                    uniqueId: '<%= Resource.ID %>',
                    pageableDataTableProvider: function(pageIndex, pageSize, onSuccess, onError) {
                        var uniqueId = '<%= Resource.ID %>';
                        var currentOrderBy = $('#OrderBy-' + uniqueId).val();
                        var alertDefId = $('#AlertDefID-' + uniqueId).val();
                        SW.Core.Services.callController("/api/TopXObjectsByTriggerCountOfThisAlert/GetTopXObjects", {
                            AlertDefId: alertDefId,
                            CurrentPageIndex: pageIndex,
                            PageSize: pageSize,
                            OrderByClause: currentOrderBy,
                            LimitationIds: [<%= string.Join(",", Limitation.GetCurrentListOfLimitationIDs().ToArray()) %>]
                        }, function (result) {
                                onSuccess(result);
                            }, function (errorResult) {
                            onError(errorResult); 
                        });

                    },
                    initialPage: 0,
                    rowsPerPage: '<%= Resource.Properties["RowsPerPage"] != null ? Convert.ToString(Resource.Properties["RowsPerPage"]) : "5" %>',
                    allowSort: true,
                    columnSettings: {
                        "TriggeringObjectCaption" : {
                            caption: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_78) %>',
                            isHtml: true,
                            formatter: function (cellValue, rowArray, cellInfo) {
                                var displayValue = cellValue;
                                var detailsUrl = rowArray[2];
                                if (detailsUrl)
                                    displayValue = SW.Core.String.Format('<a href="{0}">{1}</a>', detailsUrl, cellValue);
                                var entityName = rowArray[3] == "Orion.Container" ? "Orion.Groups" : rowArray[3];
                                var res = SW.Core.String.Format('<img src="{0}"/>&nbsp<span style="vertical-align: top;" >{1}</span>',
                                SW.Core.String.Format("/Orion/StatusIcon.ashx?entity={0}&amp;EntityUri='{1}'&amp;status={2}&amp;size=small", entityName, encodeURI(rowArray[0]), rowArray[4]), displayValue);
                                return res;
                            }
                        },
                        "WasTriggered" : {
                            caption: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_83) %>",
                            formatter: function(cellValue, rowArray, cellInfo) {
                                return SW.Core.String.Format("{0}x", cellValue);
                            }
                        },
                        "ActiveTime" : {
                            caption: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_84) %>", 
                            isHtml: true,
                            formatter: function (cellValue, rowArray, cellInfo) {
                                if (rowArray[4] == 0) {
                                    return SW.Core.String.Format("<span style=\"color: #888888\">{0}</span>", cellValue);
                                }

                                return cellValue;
                            }
                        },
                        "LastTrigger": {
                            caption: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_85) %>"
                        }
                    }
                });
                
                var refresh = function() { SW.Core.Resources.ActiveAlerts.SortablePageableTable.refresh(<%= Resource.ID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= SortablePageableTable.ClientID %>');
                refresh();
            });
        </script>
    </Content>
</orion:ResourceWrapper>
