<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HistoryOfThisAlertOnThisObject.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Alerts_HistoryOfThisAlertOnThisObject" %>

<%@ Register TagPrefix="orion" TagName="SortablePageableTable" Src="~/Orion/NetPerfMon/Controls/SortablePageableTable.ascx" %>

<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <div id="TriggerCount-<%= UniqueClientID %>" ></div>
        <br />
        <orion:SortablePageableTable ID="SortablePageableTable" runat="server" />

        <input type="hidden" id="AlertObjectID-<%= UniqueClientID %>" value="<%= AlertObjectID %>" />
        
        <input type="hidden" id="IsGlobalAlert-<%= UniqueClientID %>" value="<%= IsGlobalAlert.ToString().ToLowerInvariant() %>"/>
        
        <input type="hidden" id="OrderBy-<%= UniqueClientID %>" value="TimeStamp DESC" />

        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.ActiveAlerts.SortablePageableTable.initialize(
                {
                    uniqueId: '<%= Resource.ID %>',
                    pageableDataTableProvider: function (pageIndex, pageSize, onSuccess, onError) {
                        var uniqueId = '<%= Resource.ID %>';
                        var currentOrderBy = $('#OrderBy-' + uniqueId).val();
                        var alertObjectID = $('#AlertObjectID-' + uniqueId).val();
                        var isGlobalAlert = $('#IsGlobalAlert-' + uniqueId).val();
                        SW.Core.Services.callController("/api/HistoryOfThisAlertOnThisObject/GetHistory",
                            {
                                AlertDefId: alertObjectID,
                                CurrentPageIndex: pageIndex,
                                PageSize: pageSize,
                                OrderByClause: currentOrderBy,
                                TimePeriod: '<%= TimePeriod %>'
                            }, function (result)
                            {
                                onSuccess(result);
                                var text = isGlobalAlert == "true"
                                    ? '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_HistoryOfGlobalAlertOnObject_TriggerCount) %>'
                                    : '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_HistoryOfAlertOnObject_TriggerCount) %>';

                                $('#TriggerCount-' + uniqueId).html(text.replace("{0}", result.Metadata));
                            }, function (errorResult) {
                                onError(errorResult);
                        });
                    },
                    initialPage: 0,
                    rowsPerPage: '<%= (Resource.Properties["RowsPerPage"] != null) ? Convert.ToString(Resource.Properties["RowsPerPage"]) : "5" %>',
                    allowSort: false,
                    columnSettings: {
                        "EventType": {
                            isHidden: true
                        },
                        "Message": {
                            caption: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_HistoryOfAlertOnObject_Event) %>",
                            isHtml: true,
                            formatter: function (cellValue, rowArray, cellInfo) {
                                var eventText = "";
                                var actionName = rowArray[4];

                                switch (rowArray[0]) { //SolarWinds.Orion.Core.Common.Models.Alerts.EventType
                                    case 0: eventText = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_HistoryOfAlertOnObject_EventTriggered) %>";
                                        break;
                                    case 1: eventText = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_HistoryOfAlertOnObject_EventReset) %>";
                                        break;
                                    case 2: eventText = SW.Core.String.Format("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_HistoryOfAlertOnObject_EventAcknowledged) %>", rowArray[3]);
                                        break;
                                    case 3: eventText = rowArray[1] != ''
                                        ? SW.Core.String.Format("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_HistoryOfAlertOnObject_EventNoteModified) %>", rowArray[3])
                                        : SW.Core.String.Format("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_HistoryOfAlertOnObject_EventNoteDeleted) %>", rowArray[3]);
                                        break;
                                    case 4: eventText =
                                        SW.Core.String.Format("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_HistoryOfAlertOnObject_EventProcessingFailed) %>", rowArray[3]);
                                        break;
                                    case 5: eventText =
                                        (actionName === null) ? "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_HistoryOfAlertOnObject_DeletedEventFailed) %>"
                                        : SW.Core.String.Format("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_HistoryOfAlertOnObject_EventFailed) %>", actionName);
                                        break;
                                    case 6: eventText =
                                        (actionName === null) ? "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_HistoryOfAlertOnObject_DeletedEventSucceeded) %>"
                                        :SW.Core.String.Format("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_HistoryOfAlertOnObject_EventSucceeded) %>", actionName);
                                        break;
                                    case 7: eventText = SW.Core.String.Format("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_HistoryOfAlertOnObject_EventUnAcknowledged) %>", rowArray[3]);
                                        break;
                                    case 8: eventText = SW.Core.String.Format("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_HistoryOfAlertOnObject_EventCleared) %>", rowArray[3]);
                                }
                                var res = SW.Core.String.Format('<img src="/Orion/images/ActiveAlerts/HistoryResourceEvents/Event_{0}.png"/>&nbsp<span style="vertical-align: super;" >{1}</span>&nbsp;<span style="vertical-align: super; color: gray;" >{2}</span>',
                                    rowArray[0],
                                    eventText,
                                    cellValue);
                                return res;
                            }
                        },
                        "TimeStamp": {
                            caption: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Resource_HistoryOfAlertOnObject_Timestamp) %>"
                        },
                        "Account": {
                            isHidden: true
                        },
                    }
                });

                var refresh = function () { SW.Core.Resources.ActiveAlerts.SortablePageableTable.refresh(<%= Resource.ID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= SortablePageableTable.ClientID %>');
                refresh();
            });
        </script>

    </Content>
</orion:ResourceWrapper>