﻿using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Alerts)]
public partial class Orion_NetPerfMon_Resources_Alerts_AlertStatusOverview : BaseResourceControl
{
	protected int AlertDefID { get; set; }
	protected int AlertObjectID { get; set; }
	protected string TriggeringObjectEntityUri { get; set; }
    
    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_AY0_90; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string netObject = Request["NetObject"];
        if (!string.IsNullOrEmpty(netObject))
        {
            var activeAlertProvider = GetInterfaceInstance<IActiveAlertProvider>();
            if (activeAlertProvider != null)
            {
				AlertDefID = Convert.ToInt32(activeAlertProvider.ActiveAlert.ObjectProperties["AlertDefId"]);
                AlertObjectID = activeAlertProvider.ActiveAlert.ActiveAlertId;
				TriggeringObjectEntityUri = Convert.ToString(activeAlertProvider.ActiveAlert.ObjectProperties["TriggeringObjectEntityUri"]);
            }
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionCorePH-AlertDetailsAlertStatusOverview";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IActiveAlertProvider) }; }
    }
}