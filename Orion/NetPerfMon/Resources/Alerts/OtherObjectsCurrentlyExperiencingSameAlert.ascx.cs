﻿using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Alerts)]
public partial class Orion_NetPerfMon_Resources_Alerts_OtherNodesCurrentlyExperiencingSameAlert : BaseResourceControl
{
    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_PS0_15; }
    }

    /// <summary>
    /// Contains uniqueidentifier of resource (Resource.ID value).
    /// </summary>
    public int UniqueClientID { get; set; }

    /// <summary>
    /// Uniqueidentifier of Active alert definition
    /// </summary>
    public int AlertDefID { get; set; }

    public int AlertObjectID { get; set; }
        
    /// <summary>
    /// Uri of swis entity which unique identifier swis entity on which alert was triggered.
    /// </summary>
    public string TriggeringObjectEntityUri { get; set; }

    public bool IsGlobalAlert { get { return string.IsNullOrEmpty(TriggeringObjectEntityUri); } }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        UniqueClientID = Resource.ID;
        SortablePageableTable.UniqueClientID = Resource.ID;
        string netObject = Request["NetObject"];
        if (!string.IsNullOrEmpty(netObject))
        {
            var activeAlertProvider = GetInterfaceInstance<IActiveAlertProvider>();
            if (activeAlertProvider != null)
            {
                AlertDefID = Convert.ToInt32(activeAlertProvider.ActiveAlert.ObjectProperties["AlertDefId"]);
                TriggeringObjectEntityUri = Convert.ToString(activeAlertProvider.ActiveAlert.ObjectProperties["TriggeringObjectEntityUri"]);
                AlertObjectID = Convert.ToInt32(activeAlertProvider.ActiveAlert.ObjectProperties["AlertObjectID"]);
            }
        }

        if (IsGlobalAlert)
            Resource.Title = Resources.CoreWebContent.WEBCODE_ED0_4;
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/NetPerfMon/Controls/EditResourceControls/EditTopXNodesByTriggerCountOfThisAlert.ascx";
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionCorePH-AlertDetailsOtherObjectswithAlert";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IActiveAlertProvider) }; }
    }
}