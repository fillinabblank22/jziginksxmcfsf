<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlertNotes.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Alerts_AlertNotes" %>
<%@ Import Namespace="Resources" %>


<orion:resourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <style type='text/css'>
            .successmsg, .errormsg {
                background-position: 10px center;
                background-repeat: no-repeat;
                border: none;
                font-weight: bold;
                width: 300px;
                height: 25px;
                display: none;
            }
            .successmsg { color: #4F8A10; }
            .errormsg { color: #D8000C; }
            .disable {
                opacity: .65;
                background-color: lightgray;
                border: 1px solid gray;
            }
        </style>


        <script type="text/javascript">
            $(function() {
                var refresh = function() {
                    var uniqueId = '<%= Resource.ID %>';
                    var allowToAck = '<%= Profile.AllowEventClear %>';
                    $('.successmsg').hide();
                    $('.errormsg').hide();
                    var alertObjectId = '<%= ActiveAlertID %>';

                    SW.Core.Services.callController("/api/AlertNotes/GetAlertNote",
                        alertObjectId,
                        function(result) {
                            document.getElementById("currentNoteHd").value = result;
                            $('#AlertNoteContent').val(result);
                        },
                        function() {});

                    $divAlertNote = $("#divAlertNote-" + uniqueId);
                    $divAlertNote.find(".update").empty();

                    if (allowToAck.toLowerCase() == 'true') {
                        $('#AlertNoteContent').prop('disabled', false);
                        $('#AlertNoteContent').removeClass('disable');

                        $(SW.Core.Widgets.Button('<%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_JV0_8) %>', { type: 'primary' })).appendTo($divAlertNote.find(".update")).click(function() {
                            $("#AlertNoteContent").removeClass("disabled");
                            var alertObjectId = <%= ActiveAlertID %>;
                            var newNoteContent = $('#AlertNoteContent').val();

                            var showMsg = function(messaggeType) {
                                $(messaggeType).fadeIn(2000);
                                $(messaggeType).fadeOut(2000);
                            };

                            if (newNoteContent != $('#currentNoteHd').val()) {
                                <% if (!this.IsDemoServer){ %>
                                SW.Core.Services.callController("/api/AlertNotes/SetAlertNote",
                                    {
                                        AlertObjectID: alertObjectId,
                                        Note: newNoteContent
                                    },
                                    function(result) {
                                        if (result) {
                                            SW.Core.View.Refresh(false);
                                            $('#currentNoteHd').val(newNoteContent);
                                            showMsg('.successmsg');
                                        } else {
                                            SW.Core.View.Refresh(false);
                                            showMsg('.errormsg');
                                        }
                                    },
                                    function() {
                                        showMsg('.errormsg');
                                    });
                                <% } else { %>
                                demoAction('Core_Alerting_Unacknowledge', this);
                                <% } %>
                            };
                        });
                    };
                };
                SW.Core.View.AddOnRefresh(refresh, 'divAlertNote-<%= Resource.ID %>');
                refresh();
            });
        </script>

        <input type="hidden" ID="currentNoteHd" />
        <div id="divAlertNote-<%= Resource.ID %>">
            <table style="border: none; width: 100%;">
                <tr>
                    <td colspan = 2 style="border: none; padding: 5px; white-space: nowrap;">
                        <asp:TextBox ID="AlertNoteContent" ClientIDMode="Static" runat="server" Enabled="false" TextMode="MultiLine" ReadOnly="false" CssClass="disable" style="resize: none; height: 170px; width: 100%;"></asp:TextBox>
                    </td>
                </tr>    
                <tr>
                    <td style="border: none; width: 75%">
                        <div class="successmsg"> <%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_JV0_9) %></div>
                        <div class="errormsg"> <%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_JV0_10) %></div>
                    </td>
                    <td class="update" style="height: 25px; border: none; float: right"></td>
                </tr>    
            </table>
        </div>
    </Content>
</orion:resourceWrapper>