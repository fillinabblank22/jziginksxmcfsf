<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true" CodeFile="EditMapTooltips.aspx.cs" Inherits="Orion_NetPerfMon_Resources_EditMapTooltips" Title="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_260 %>" %>
<%@ Register TagPrefix="orion" TagName="TooltipCustomizer" Src="~/Orion/NetPerfMon/Controls/MapTooltipCustomizer.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<h1><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_255) %></h1>
<div><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_256) %></div>
<orion:TooltipCustomizer runat="server" ID="tooltipCustomizer" />

<br /><div><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_257) %></div>
<br /><div><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_258) %></div>
<br /><div><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_259) %></div>
<div class="sw-btn-bar"><orion:LocalizableButton ID="btnSubmit" runat="server" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitClick"/></div>
<div runat="server" id="div1"></div>
</asp:Content>