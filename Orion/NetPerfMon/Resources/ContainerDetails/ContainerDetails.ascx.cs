﻿using System;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_ContainerDetails_ContainerDetails : ContainerBaseResource
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // enable AngularJS for this resource so that xui status icon works
        CanContainAngularJsResource = true;
    }

    protected string ReturnUrlParameter
    {
        get
        {
            return UrlHelper.ToSafeUrlParameter(this.Request.UrlReferrer != null ? this.Request.UrlReferrer.PathAndQuery : this.Request.RawUrl);
        }
    }
    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.ApplicationServiceGroupDetails_DefaultName; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionAPMContainerDetails"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
