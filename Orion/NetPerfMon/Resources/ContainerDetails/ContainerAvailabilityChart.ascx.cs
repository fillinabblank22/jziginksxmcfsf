using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using System.Collections.Generic;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.UI;

public partial class ContainerAvailabilityChart : GraphResource
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		      CreateChart(null, GetInterfaceInstance<IContainerProvider>().Container.NetObjectID, "ContainerAvailability", chartPlaceHolder);
		Wrapper.SetDrDownMenuParameters("ContainerAvailability", Resource, "C");

	}

	protected override string DefaultTitle
	{
		get
		{
            return Resources.CoreWebContent.WEBCODE_AK0_61;
		}
	}

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditCustomNodeChart.ascx"; }
    }

	public override string HelpLinkFragment
	{
		get { return "OrionCorePHResourceGroupsGroupAvailabilityChart"; }
	}

	public override IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(IContainerProvider) }; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
