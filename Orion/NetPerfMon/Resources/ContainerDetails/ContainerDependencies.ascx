﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContainerDependencies.ascx.cs" Inherits="Orion_NetPerfMon_Resources_ContainerDetails_ContainerDependencies" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>

<asp:ScriptManagerProxy id="DepTreeScriptManager" runat="server">
	<Services>
		<asp:ServiceReference path="/Orion/Services/DependenciesTree.asmx" />
	</Services>
</asp:ScriptManagerProxy>

<orion:Include ID="Include1" runat="server" File="NetPerfMon/js/Dependencies.js" />

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div runat="server" id="errorMessage" style="color:Red;" visible="false"></div>
          <%if (!errorMessage.Visible)
            {%>
        <script type="text/javascript">
	        //<![CDATA[
            $(function () {
                var refresh = function () {
                    ORIONDependencies.DependenciesTree.LoadDependencies($('#<%=this.DepTree.ClientID%>'), '<%=this.Resource.ID %>', '<%=this.NetObectId %>');
                };
                SW.Core.View.AddOnRefresh(refresh, '<%= this.DepTree.ClientID %>');
                refresh();
            });
	        //]]>
	    </script>
    <%} %>
        <div class="Tree" ID="DepTree" runat="server">
            <asp:Literal runat="server" ID="TreeLiteral" />
        </div>
    </Content>
</orion:resourceWrapper>