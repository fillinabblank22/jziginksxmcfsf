﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_ContainerDetails_ContainerObjects : ContainerBaseResource
{
    private string warningMessage = Resources.CoreWebContent.WEBCODE_AK0_72;
	private String _key, _orderBy;

	protected override string DefaultTitle
	{
		get { return Resources.CoreWebContent.WEBCODE_AK0_73; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionAPMContainerObjects"; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

	public String Key
	{
		get { return _key ?? (_key = String.Format("ContainerObjects_OrderBy_{0}", Container.NetObjectID)); }
	}

	private String OrderBy
	{
		get
		{
			if (_orderBy == null)
			{
				_orderBy = Request["orderBy"];
				if (String.IsNullOrEmpty(_orderBy))
				{
					if (Session[Key] == null)
					{
						Session[Key] = _orderBy = "FullName";
					}
                    _orderBy = Session[Key].ToString();
					return _orderBy;
				}

				String sessOrderBy = (Session[Key] ?? String.Empty).ToString();
				if (sessOrderBy.Contains(_orderBy))
				{
					if (sessOrderBy == _orderBy)
					{
						_orderBy = String.Format("{0} DESC", _orderBy);
					}
				}
				Session[Key] = _orderBy;
			}
			return _orderBy;
		}
	}

    protected void Page_Load(object sender, EventArgs e)
    {
    }

	protected void itemRepeater_Load(object sender, EventArgs e)
	{
		itemRepeater.DataSource = GetContainerMembers();
		itemRepeater.DataBind();
		lbWarning.Text = (itemRepeater.DataSource as List<ContainerMember>).Count < 1 ? warningMessage : string.Empty;
	}
	
	protected void OnRepeater_ItemCreated(Object sender, RepeaterItemEventArgs e) 
	{
		if (e.Item.ItemType == ListItemType.Header)
		{
			if (OrderBy.StartsWith("FullName")) 
			{
				OnResource_SortClick(e.Item.FindControl("ctrAFN"), null);
			}
			else if (OrderBy.StartsWith("EntityName"))
			{
				OnResource_SortClick(e.Item.FindControl("ctrAEN"), null);
			}
			else if (OrderBy.StartsWith("Status"))
			{
				OnResource_SortClick(e.Item.FindControl("ctrAS"), null);
			}
		}
	}

	protected void OnResource_SortClick(Object sender, EventArgs e)
	{
		if (e == null) 
		{
			var ctr = sender as HtmlAnchor;
            ctr.DataBind();
			ctr.InnerHtml = String.Format(
				"{0}&nbsp;&nbsp;&nbsp;<img alt='' src=\"/Orion/images/nodemgmt_art/icons/arrow_sort{1}.png\"/>",
				ctr.InnerText.Trim(), OrderBy.EndsWith("DESC") ? String.Empty : "_up"
			);
		}
	}

	private List<ContainerMember> GetContainerMembers()
	{
		IEnumerable<ContainerMember> ds = null;

		if (OrderBy.StartsWith("FullName"))
		{
			if (OrderBy.EndsWith("DESC"))
			{
				ds = from item in Container.Members
					 orderby item.Name descending
					 select item;
			}
			else
			{
				ds = from item in Container.Members
					 orderby item.Name ascending
					 select item;
			}
		}
		else if (OrderBy.StartsWith("EntityName"))
		{
			if (OrderBy.EndsWith("DESC"))
			{
				ds = from item in Container.Members
					 orderby item.EntityName descending
					 select item;
			}
			else
			{
				ds = from item in Container.Members
					 orderby item.EntityName ascending
					 select item;
			}
		}
		else if (OrderBy.StartsWith("Status"))
		{
			if (OrderBy.EndsWith("DESC"))
			{
				ds = from item in Container.Members
					 orderby item.Status.ToString() descending
					 select item;
			}
			else
			{
				ds = from item in Container.Members
					 orderby item.Status.ToString() ascending
					 select item;
			}
		}
		else
		{
			ds = from item in Container.Members
				 orderby item.Name ascending
				 select item;
		}
		return ds.ToList();
	}
}