<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContainerObjects.ascx.cs" Inherits="Orion_NetPerfMon_Resources_ContainerDetails_ContainerObjects" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Containers" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
		<script type="text/javascript">
		    function setOrder(column) { $("#orderBy").val(column); return true; }

		    $(function () {
		        var refresh = function () { __doPostBack('<%= ctrResUP.ClientID %>', ''); };
		        SW.Core.View.AddOnRefresh(refresh, '<%= ctrResUP.ClientID %>');
		    });
		</script>
		
		<asp:UpdatePanel ID="ctrResUP" UpdateMode="Conditional" runat="server">
			<ContentTemplate>
				<input id="orderBy" name="orderBy" type="hidden" />
				<asp:Repeater ID="itemRepeater" runat="server" OnLoad="itemRepeater_Load" OnItemCreated="OnRepeater_ItemCreated">
					<HeaderTemplate>
						<table class="DataGrid NeedsZebraStripes">
							<thead>
								<tr>
									<td class="ReportHeader" style="text-transform: uppercase;" colspan="2">
										<a id="ctrAFN" onclick="return setOrder('FullName');" onserverclick="OnResource_SortClick" runat="server">
											<%# DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_203) %></a>
									</td>
									<td class="ReportHeader" style="text-transform: uppercase;">
										<a id="ctrAEN" onclick="return setOrder('EntityName');" onserverclick="OnResource_SortClick" runat="server">
											<%# DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_210) %></a>
									</td>
									<td class="ReportHeader" style="text-transform: uppercase; width:70px;">
										<a id="ctrAS" onclick="return setOrder('Status');" onserverclick="OnResource_SortClick" runat="server">
											<%# DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_14) %></a>
									</td>
								</tr>
							</thead>
					</HeaderTemplate>
					<ItemTemplate>
						<tr <%# (Container.ItemIndex%2==0) ? "" : "class=\"ZebraStripe\"" %>>
							<td style="border-bottom-width: 0px!important;" valign="center" width="27">
                                <orion:EntityStatusIcon ID="EntityStatusIcon1" runat="server" Entity="<%# DefaultSanitizer.SanitizeHtml(((ContainerMember)Container.DataItem).Entity) %>"
                                    EntityId="<%# ((ContainerMember)Container.DataItem).MemberPrimaryID %>" 
                                    Status="<%# ((ContainerMember)Container.DataItem).Status.StatusId %>" />
                            </td>
							<td style="border-bottom-width: 0px!important;" valign="center">
								<%# DefaultSanitizer.SanitizeHtml(((ContainerMember)Container.DataItem).FullName) %>&nbsp;
							</td>
							<td style="border-bottom-width: 0px!important;" valign="center">
								<%# DefaultSanitizer.SanitizeHtml(((ContainerMember)Container.DataItem).EntityName) %>&nbsp;
							</td>
							<td style="border-bottom-width: 0px!important;" valign="center">
								<%# DefaultSanitizer.SanitizeHtml(((ContainerMember)Container.DataItem).Status.ShortDescription) %>&nbsp;
							</td>
						</tr>
					</ItemTemplate>
					<FooterTemplate>
						</table>
					</FooterTemplate>
				</asp:Repeater>
				<asp:Label ID="lbWarning" ForeColor="red" Font-Size="8" runat="server" />
			</ContentTemplate>
		</asp:UpdatePanel>
	</Content>
</orion:resourceWrapper>