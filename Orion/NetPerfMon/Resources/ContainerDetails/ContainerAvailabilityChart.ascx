<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContainerAvailabilityChart.ascx.cs" Inherits="ContainerAvailabilityChart" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:PlaceHolder runat="server" ID="chartPlaceHolder" />
    </Content>
</orion:resourceWrapper>