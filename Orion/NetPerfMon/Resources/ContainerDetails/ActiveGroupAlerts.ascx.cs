﻿using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Alerts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsAlertValues.Alarms)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsAlertValues.Warnings)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsAlertValues.Warns)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_ContainerDetails_ActiveGroupAlerts : BaseResourceControl
{
    protected override void OnInit(EventArgs e)
    {
        string showAckString = this.Resource.Properties["ShowAcknowledgedAlerts"];
        bool showAck = false;

        if (!String.IsNullOrEmpty(showAckString) && showAckString.Equals("true", StringComparison.InvariantCultureIgnoreCase))
        {
            showAck = true;
        }

		AlertsTable.ShowAcknowledgedAlerts = showAck;
		AlertsTable.ResourceID = Resource.ID;
		
		int rowsPerPage;
        if (!int.TryParse(Resource.Properties["RowsPerPage"], out rowsPerPage))
            rowsPerPage = 5;
        AlertsTable.InitialRowsPerPage = rowsPerPage;
		
		var containerProvider = this.GetInterfaceInstance<IContainerProvider>();
        if (containerProvider != null)
        {
            AlertsTable.TriggeringObjectEntityUris = new List<string>() { containerProvider.Container.SwisUri };
            AlertsTable.TriggeringObjectEntityNames = new List<string>() { "Orion.Groups" };
        }

		AddShowAllCurrentlyTriggeredAlertsControl(this.Wrapper);

        base.OnInit(e);
    }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_AK0_62; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHActiveGroupAlerts"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditAllAlerts.ascx"; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IContainerProvider) }; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

	private void AddShowAllCurrentlyTriggeredAlertsControl(Control resourceWrapperContents)
	{
		if (resourceWrapperContents.TemplateControl is IResourceWrapper)
		{
			IResourceWrapper wrapper = resourceWrapperContents.TemplateControl as IResourceWrapper;
			wrapper.ManageButtonText = Resources.CoreWebContent.WEBDATA_BV0_0043;
			wrapper.ManageButtonTarget = "/Orion/NetPerfMon/Alerts.aspx";
			wrapper.ShowManageButton = true;
		}
	}
}