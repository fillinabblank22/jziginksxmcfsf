﻿using System;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_ContainerDetails_GroupRootCause : SolarWinds.Orion.Web.Containers.ContainerBaseResource
{
	#region Override Members (SolarWinds.Orion.Web.Containers.ContainerBaseResource)
    
    protected override String DefaultTitle
	{
		get { return Resources.CoreWebContent.WEBCODE_AK0_74; }
	}

	public override String HelpLinkFragment
	{
		get { return "OrionCoreGroupRootCauseResource"; }
	}

	public override String EditControlLocation
	{
		get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditGroupRootCause.ascx"; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

	#endregion
}
