﻿using System;
using System.Data;

using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.PackageManager;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Dependencies;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Containers;
using System.ServiceModel;
using System.Web.UI;


[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_NetPerfMon_Resources_ContainerDetails_ContainerDependenciesTable : BaseResourceControl
{
	private const string object_type = "Orion.Groups";
    
    private string _parentFilter;
	private string _childFilter;
	private string _dependencyFilter;

	public string NetObectId
	{
		get
		{
			IContainerProvider containerProvider = GetInterfaceInstance<IContainerProvider>();
			if (containerProvider != null)
			{
				Container container = containerProvider.Container;
				return container != null ? container.NetObjectID : Request["netobject"] ?? "";
			}
			return string.Empty;
		}
	}

	protected bool AutoHide
	{
		get
		{
			if (string.IsNullOrEmpty(Resource.Properties["AutoHide"]))
				return false;
			return Resource.Properties["AutoHide"].Equals("1");
		}
	}

	protected override string DefaultTitle
	{
		get { return Resources.CoreWebContent.WEBCODE_AK0_59; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceNodeDependencies"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(IContainerProvider) }; }
	}

	public override string EditControlLocation
	{
		get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditDependencies.ascx"; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

	protected override void OnInit(EventArgs e)
	{
		Wrapper.ManageButtonText = Resources.CoreWebContent.ResourcesAll_ManageDependencies;
		Wrapper.ManageButtonTarget = "/Orion/Admin/DependenciesView.aspx";
		Wrapper.ShowManageButton = Profile.AllowAdmin || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;

		_parentFilter = string.IsNullOrEmpty(Resource.Properties["ParentFilter"]) ? string.Empty : Resource.Properties["ParentFilter"];
		_childFilter = string.IsNullOrEmpty(Resource.Properties["ChildFilter"]) ? string.Empty : Resource.Properties["ChildFilter"];
		_dependencyFilter = string.IsNullOrEmpty(Resource.Properties["DependencyFilter"]) ? string.Empty : Resource.Properties["DependencyFilter"];
	    var filter = $"{_parentFilter} {_childFilter} {_dependencyFilter}";

        try
		{
			DependencyCache.UpdateDependencyCache(NetObectId, _parentFilter, _childFilter, _dependencyFilter);
		}
		catch (Exception ex)
		{
		    var errMessage = HasSWQLFilterError(ex, filter) ? string.Format(Resources.CoreWebContent.WEBCODE_VB0_32, ex.Message, filter) : ex.Message;
		    errorMessage.Controls.Clear();
		    errorMessage.Controls.Add(new LiteralControl(errMessage));
		    errorMessage.Visible = true;
		}

        base.OnInit(e);
	}

	protected override void OnPreRender(EventArgs e)
	{
		int objectId = 0;

		if (int.TryParse(NetObjectHelper.GetObjectID(NetObectId), out objectId))
		{
			// hide resource if there aren't any data
			if (AutoHide)
			{
				using (var dal = new DependenciesDAL())
                    if (dal.GetAllDependenciesFilteredCount(objectId, object_type, null, null, null) == 0)
						Wrapper.Visible = false;
			}

			DataTable sourceDataTable = null;
			DataTable groupDataTable = null;
		    var filter = $"{_parentFilter} {_childFilter} {_dependencyFilter}";

            try
			{
				using (DependenciesDAL dal = new DependenciesDAL())
				{
                    sourceDataTable = dal.GetAllDependenciesFiltered(objectId, object_type, _parentFilter, _childFilter, _dependencyFilter);
                    groupDataTable = dal.GetAllDependenciesFiltered("Orion.Groups", _parentFilter, _childFilter, _dependencyFilter);
				}

				if (sourceDataTable != null && groupDataTable != null)
				{
					foreach (DataRow row in groupDataTable.Rows)
					{
						int depId = int.Parse(row["DependencyId"].ToString());
						if (DependencyCache.GroupDependenciesContains(NetObectId, depId, row["ParentUri"].ToString(), row["ChildUri"].ToString()))
							sourceDataTable.ImportRow(row);
					}
					sourceDataTable.DefaultView.Sort = "Name Asc";

					DependencyTable.DataSource = sourceDataTable;
					DependencyTable.DataBind();
				}
			}
            catch (Exception ex)
            {
                var errMessage = HasSWQLFilterError(ex, filter) ? string.Format(Resources.CoreWebContent.WEBCODE_VB0_32, ex.Message, filter) : ex.Message;
                errorMessage.Controls.Clear();
                errorMessage.Controls.Add(new LiteralControl(errMessage));
                errorMessage.Visible = true;
            }
        }

		base.OnPreRender(e);
	}
}