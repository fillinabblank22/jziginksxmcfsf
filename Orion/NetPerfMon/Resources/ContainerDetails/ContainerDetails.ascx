<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContainerDetails.ascx.cs" Inherits="Orion_NetPerfMon_Resources_ContainerDetails_ContainerDetails" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <table cellspacing="0">
			<% if (Profile.AllowNodeManagement && !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
			{ %>
			<tr <%= DefaultSanitizer.SanitizeHtml(AngularJsHelper.RenderNgNonBindable()) %>>
				<td class="Property" width="10">&nbsp;</td>
				<td class="PropertyHeader" valign="center" style="padding-right: 5px; width: 25%;">
					<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.ApplicationServiceGroupDetails_Management) %>
				</td>
				<td colspan="2" class="Property" style="padding-bottom: 5px;">
					<a href="/Orion/Admin/Containers/EditContainer.aspx?id=<%= this.Container.Id %>&returnUrl=<%= DefaultSanitizer.SanitizeHtml(this.ReturnUrlParameter) %>">
						<img alt="" src="/Orion/Nodes/images/icons/icon_edit.gif" style="padding-right: 4px"/><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_205) %>
					</a>
				</td>
			</tr>
			<% } %>
			<tr <%= DefaultSanitizer.SanitizeHtml(AngularJsHelper.RenderNgNonBindable()) %>>
				<td class="Property" width="10">&nbsp;</td>				
				<td class="PropertyHeader" valign="center" style="padding-right: 5px;">
					<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.ApplicationServiceGroupDetails_Name) %>
				</td>
				<td style="width: 20px;">&nbsp;</td>
				<td class="Property" style="padding-top: 5px; padding-bottom: 5px; word-break: break-all; word-wrap: break-word">
						<%= DefaultSanitizer.SanitizeHtml(UIHelper.Escape(Container.Name)) %>
				</td>
			</tr>
            <% if (!string.IsNullOrWhiteSpace(Container.Description))
               { %>
            <tr <%= DefaultSanitizer.SanitizeHtml(AngularJsHelper.RenderNgNonBindable()) %>>
                <td class="Property" width="10">&nbsp;</td>				
                <td class="PropertyHeader" valign="center" style="padding-right: 5px;">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.ApplicationServiceGroupDetails_Description) %>
                </td>
                <td style="width: 20px;">&nbsp;</td>
                <td class="Property" style="padding-top: 5px; padding-bottom: 5px; word-break: break-all; word-wrap: break-word">
                    <%= DefaultSanitizer.SanitizeHtml(UIHelper.Escape(Container.Description)) %>
                </td>
            </tr>
            <% } %>
			<tr>
				<td class="Property" width="10">&nbsp;</td>
				<td class="PropertyHeader" valign="center" style="padding-right: 5px;">
					<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.ApplicationServiceGroupDetails_Status) %>
				</td>
				<td align="center" width="20"  style="padding-top: 5px;">
					<xui-icon icon="<%= DefaultSanitizer.SanitizeHtml(string.Format("{0}{1}", "status_", Container.Status.IconPostfix.ToLower())) %>" icon-size="small"></xui-icon>
				</td>
				<td class="Property">
					<%= DefaultSanitizer.SanitizeHtml(Container.Status.ShortDescription) %>
				</td>
			</tr>
		</table>
    </Content>
</orion:resourceWrapper>