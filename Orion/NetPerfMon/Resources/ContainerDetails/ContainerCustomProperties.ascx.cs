using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Containers;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_ContainerDetails_ContainerCustomProperties : BaseResourceControl
{
    private string EntityName
    {
        get { return "Orion.Container"; }
    }

    private string EntityIDFieldName
    {
        get { return "ContainerID"; }
    }

    private NetObject Netobject { set; get; }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.customPropertyList.Resource = this.Resource;

        Container container = GetInterfaceInstance<IContainerProvider>().Container;
        if (container != null)
        {
            this.customPropertyList.NetObjectId = container.Id;
        }

        this.Netobject = container;

        customPropertyList.CustomPropertyLoader = (netObjectId, displayProperties) =>
        {
            return CustomPropertyHelper.GetCustomPropertiesForObject(netObjectId, displayProperties, EntityName,
                EntityIDFieldName, Netobject);
        };

        customPropertyList.EditCustomPropertiesLinkGenerator = (netObjectId, redirectUrl) =>
        {
            if (Request.UrlReferrer != null)
            {
                redirectUrl = UrlHelper.ToSafeUrlParameter(Request.UrlReferrer.PathAndQuery);
            }
            return String.Format("/Orion/Admin/Containers/EditContainer.aspx?id={0}&GuidID={2}&returnUrl={1}",
                netObjectId, redirectUrl, Guid.NewGuid());
        };
    }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBDATA_SO0_173; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditCustomPropertyList.ascx"; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceCustomPropertiesGroups"; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] {typeof (IContainerProvider)}; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
