<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContainerDependenciesTable.ascx.cs"
    Inherits="Orion_NetPerfMon_Resources_ContainerDetails_ContainerDependenciesTable" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div runat="server" id="errorMessage" style="color:Red;" visible="false"></div>
        <asp:Repeater ID="DependencyTable" runat="server">
            <HeaderTemplate>
                <table width="100%" cellspacing="0" cellpadding="2">
                    <tr class="ReportHeader">
                        <td class="ReportHeader" colspan="2">
                            &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_142) %>
                        </td>
                        <td class="ReportHeader" colspan="3">
                            &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_143) %>
                        </td>
                        <td class="ReportHeader" colspan="2">
                            &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_144) %>
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td class="Property" width="10px" style="padding: 0px 5px 0px 5px;">
                        <img src="/Orion/images/dependency_16x16.gif" alt="" />
                    </td>
                    <td class="Property" width="35%">
                        <%# DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Core.Web.FormatHelper.MakeBreakableString(Eval("Name").ToString())) %>
                    </td>
                    <td class="Property" style="padding: 0px 5px 0px 5px;" width="10px">
                        <img src="<%# DefaultSanitizer.SanitizeHtml(CommonWebHelper.GetStatusIconPath(Eval("ParentUri"), Eval("ParentStatus"))) %>"
                            alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_14) %>" />
                    </td>
                    <td class="Property" width="25%">
                        <%# DefaultSanitizer.SanitizeHtml(CommonWebHelper.RenderObjectName(Eval("ParentUri"), Eval("ParentDisplayName"))) %>
                    </td>
                    <td class="Property" style="padding: 0px 5px 0px 5px;" width="16px">
                        <img src="/Orion/images/arrow_forward.gif" alt="" />
                    </td>
                    <td class="Property" style="padding: 0px 5px 0px 5px;" width="10px">
                        <img src="<%# DefaultSanitizer.SanitizeHtml(CommonWebHelper.GetStatusIconPath(Eval("ChildUri"), Eval("ChildStatus"))) %>"
                            alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_14) %>" />
                    </td>
                    <td class="Property" width="25%">
                        <%# DefaultSanitizer.SanitizeHtml(CommonWebHelper.RenderObjectName(Eval("ChildUri"), Eval("ChildDisplayName"))) %>
                    </td>
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr>
                    <td class="Property" style="background-color: #f8f8f8; padding: 0px 5px 0px 5px;" width="10px">
                        <img src="/Orion/images/dependency_16x16.gif" alt="" />
                    </td>
                    <td class="Property" style="background-color: #f8f8f8;" width="35%">
                        <%# DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Core.Web.FormatHelper.MakeBreakableString(Eval("Name").ToString())) %>
                    </td>
                    <td class="Property" style="background-color: #f8f8f8; padding: 0px 5px 0px 5px;" width="10px">
                        <img src="<%# DefaultSanitizer.SanitizeHtml(CommonWebHelper.GetStatusIconPath(Eval("ParentUri"), Eval("ParentStatus"))) %>"
                            alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_14) %>" />
                    </td>
                    <td class="Property" style="background-color: #f8f8f8;" width="25%">
                        <%# DefaultSanitizer.SanitizeHtml(CommonWebHelper.RenderObjectName(Eval("ParentUri"), Eval("ParentDisplayName"))) %>
                    </td>
                    <td class="Property" style="background-color: #f8f8f8; padding: 0px 5px 0px 5px;" width="16px">
                        <img src="/Orion/images/arrow_forward.gif" alt="" />
                    </td>
                    <td class="Property" style="background-color: #f8f8f8; padding: 0px 5px 0px 5px;" width="10px">
                        <img src="<%# DefaultSanitizer.SanitizeHtml(CommonWebHelper.GetStatusIconPath(Eval("ChildUri"), Eval("ChildStatus"))) %>"
                            alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_14) %>" />
                    </td>
                    <td class="Property" style="background-color: #f8f8f8;" width="25%">
                        <%# DefaultSanitizer.SanitizeHtml(CommonWebHelper.RenderObjectName(Eval("ChildUri"), Eval("ChildDisplayName"))) %>
                    </td>
                </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>
