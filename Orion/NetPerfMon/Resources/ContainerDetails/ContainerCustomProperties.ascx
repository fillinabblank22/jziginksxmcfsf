<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContainerCustomProperties.ascx.cs" Inherits="Orion_NetPerfMon_Resources_ContainerDetails_ContainerCustomProperties" %>
<%@ Register TagPrefix="orion" TagName="CustomPropertyList" Src="~/Orion/NetPerfMon/Controls/CustomPropertyList.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:CustomPropertyList runat="server" ID="customPropertyList" EntityName="Orion.GroupCustomProperties" />
    </Content>
</orion:resourceWrapper>