<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GroupRootCause.ascx.cs" Inherits="Orion_NetPerfMon_Resources_ContainerDetails_GroupRootCause" %>
<%@ Register TagPrefix="orion" TagName="IncludeExtJs" Src="~/Orion/Controls/IncludeExtJs.ascx" %>

<orion:IncludeExtJs ID="ctrExtJs" Version="3.4" Debug="false" runat="server"/>
<orion:Include runat="server" File="NetPerfMon/js/GroupRootCause.js" />
<orion:Include runat="server" File="MainLayout.css" />

<orion:resourceWrapper ID="Wrapper" runat="server">
	<Content>
		<style type="text/css">
		    
			div[id^="grcCnt"] div.grcStatusInfo { margin:5px 0px 5px 0px; font-weight:bold; display:none; }
			div[id^="grcCnt"] div.grcInfo { margin:5px 0px 5px 0px; font-size:8pt; color:#666; }
			
			div[id^="grcCnt"] div.grcTree {}
			
			div[id^="grcCnt"] div.grcTree img.x-tree-elbow-plus, 
			div[id^="grcCnt"] div.grcTree img.x-tree-elbow-end-plus { 
				padding-right:2px;
				background-image: url("/Orion/images/Button.Expand.gif");  
			}
			div[id^="grcCnt"] div.grcTree img.x-tree-elbow-minus, 
			div[id^="grcCnt"] div.grcTree img.x-tree-elbow-end-minus { 
				padding-right:2px;
				background-image: url("/Orion/images/Button.Collapse.gif");  
			}
			.auto-icon-width {
				width:auto !important;
				height:auto !important;
			}
		</style>
		<script type="text/javascript">
		    $(function() {
		        var refresh = function () {
		            SW.Orion.GroupRootCause.Tree.create({
		                renderTo: "#grcCnt<%= this.Resource.ID %>",
		                groupID: "<%=this.Container.Id%>",
		                groupName: "<%=this.Container.Name.Replace("\"","\\\"")%>",
		                groupStatus: "<%=this.Container.Status.StatusName%>",
		                membersCount: "<%=this.Resource.Properties["MembersCount"]%>",
		                viewID: "<%=this.Resource.View.ViewID%>"
		            });
		        };
		        SW.Core.View.AddOnRefresh(refresh, "grcCnt<%= this.Resource.ID %>");
		        refresh();
		    });
		</script>

		<div id="grcCnt<%= this.Resource.ID %>">
			<div class="grcStatusInfo">
				<%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBDATA_AK0_211, this.Container.Name, this.Container.Status.ShortDescription)) %>  
			</div>
			<div class="grcInfo">
				<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_212) %>
			</div>
			<div class="grcTree">
				<center>
					<img alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_213) %>" src="/Orion/images/AJAX-Loader.gif" /> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_213) %>
				</center>
			</div>
		</div>
	</Content>
</orion:resourceWrapper>