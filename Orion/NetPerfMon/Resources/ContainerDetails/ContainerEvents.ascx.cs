using System;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Containers;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_ContainerDetails_ContainerEvents : ContainerBaseResource
{
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

		Wrapper.ShowEditButton = Profile.AllowCustomize;

        containerEventList.ContainerId = Container.Id;
        containerEventList.MaxEventCount = GetIntProperty("MaxEvents", -1);
        containerEventList.PeriodName = Resource.Properties["Period"];
        containerEventList.ResourceID = Resource.ID;
        containerEventList.LoadData();
    }
	
    // this resource uses Period property for date time filter
	//public override bool SupportDateTimeFilter
	//{
	//    get { return true; }
	//}

    public override string DisplayTitle
    {
        get
        {
            return Title.Replace("XX", containerEventList.MaxEventCount.ToString());
        }
    }

    protected override string DefaultTitle
    {
        get { return containerEventList.DefaultTitle; }
    }

    public override string HelpLinkFragment
    {
        get { return containerEventList.HelpLinkFragment; }
    }
    
    public override string SubTitle
    {
        get { return containerEventList.SubTitle; }
    }

	public override string EditControlLocation
	{
		get { return containerEventList.EditControlLocation; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}





