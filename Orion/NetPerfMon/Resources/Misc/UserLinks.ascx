<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserLinks.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Misc_UserLinks" %>
<%@ Assembly Name="Infragistics2.WebUI.Shared.v8.2, Version=8.2.20082.1000, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Assembly Name="Infragistics2.WebUI.UltraWebNavigator.v8.2, Version=8.2.20082.1000, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>

<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<orion:ResourceWrapper ID="ResourceWrapper" runat="server">
    <Content>
        <asp:Repeater runat="server" ID="HyperLinks">
            <HeaderTemplate>
                <table id="Links" style="width: 100%;">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:HyperLink runat="server" NavigateUrl='<%# DefaultSanitizer.SanitizeHtml(Eval("URL")) %>' Target='<%# DefaultSanitizer.SanitizeHtml(Eval("TARGET")) %>'><%# DefaultSanitizer.SanitizeHtml(Eval("Link")) %></asp:HyperLink>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:ResourceWrapper>
