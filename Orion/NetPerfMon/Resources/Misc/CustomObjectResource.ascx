﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomObjectResource.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Misc_CustomObjectResource" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>

    <style type="text/css">
        .ResourceWrapper .ResourceWrapper 
        {
            border:0px solid #DFDED7;
            padding:0px;
            overflow: hidden;
        }
    </style>

    <orion:ResourceWrapper ID="Wrapper" runat="server" ShowEditButton="true">
            <Content>
                <asp:PlaceHolder ID="ResourceContainer" runat="server"></asp:PlaceHolder>
            </Content>
    </orion:ResourceWrapper>
    
