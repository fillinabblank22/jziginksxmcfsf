﻿using System;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_Misc_XuiWrapper : BaseResourceControl, IXuiResource, IResourceIsInternal
{
    protected override string DefaultTitle =>
        GetStringValue(ResourceManager.DefaultTitlePropertyKey, "Xui Wrapper");

    protected void Page_Load(object sender, EventArgs e)
    {
        ResourceWrapper.Visible = OrionMinReqsMaster.IsApolloEnabled;
        Selector = GetStringValue("selector", null);
    }

    protected string Selector { get; set; }
    public bool IsInternal => true;

    public override string HelpLinkFragment
    {
        get { return GetStringValue("HelpLinkFragment", ""); }
    }
}
