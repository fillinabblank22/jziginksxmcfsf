﻿using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_Misc_PollingEngineStatus : SolarWinds.Orion.Web.UI.BaseResourceControl
{
    #region properties

    // overriden DefaultTitle
    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_VB0_113; }
    }


    // overriden HelpLink
    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourcePollingEnginesStatus";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    #endregion

    protected string FormatKeepAlive(DateTime? keepAlive)
    {
        if (!keepAlive.HasValue || keepAlive.Value == default(DateTime) || keepAlive.Value <= new DateTime(2000, 1, 1))
            return Resources.CoreWebContent.WEBCODE_VB0_242;

        return FormatTimeSpan(DateTime.UtcNow - keepAlive.Value);
    }

    protected string FormatTimeSpan(TimeSpan time)
    {
        if (time.TotalSeconds < 1)
            return Resources.CoreWebContent.WEBCODE_VB0_114;
        
        var output  = String.Format(Resources.CoreWebContent.WEBCODE_VB0_120, time.Seconds);

        if(time.TotalMinutes > 1)
        output = String.Format(Resources.CoreWebContent.WEBCODE_VB0_121, (long)time.TotalMinutes, output);

        return output;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LastDBUpdateLabel.Text = FormatTimeSpan(EnginesDAL.GetNodeSyncDelta());

        using (WebDAL proxy = new WebDAL())
        {
            var table = proxy.GetPollingEngineStatus();
            EngineRepeater.DataSource = table;
            EngineRepeater.DataBind();
        }
    }
}
