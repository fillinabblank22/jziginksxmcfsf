<%@ Control Language="C#" ClassName="TallBlankSpace" Inherits="SolarWinds.Orion.Web.UI.BaseResourceControl"%>
<%@ Implements Interface="SolarWinds.Orion.Web.ISupportsFederation" %>

<style type="text/css">
        .editplaceholder .EditResourceButton {
    display: none;
}
.editplaceholder:hover .EditResourceButton {
    display:block;
}
    </style>

<orion:resourceWrapper runat="server" ShowHeaderBar="False" CssClass="ResourceWrapperNoBorder" ID="Wrapper" ShowEditButton="true">
	<Content>
<script runat="server">
    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_VB0_129; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Static; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditBlankResource.ascx"; }
    }

    private bool _isNOCView = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Resource.Properties.ContainsKey("Height"))
            Resource.Properties["Height"] = "300";
        
            this.contentTab.Height = Resource.Properties["Height"];

            bool.TryParse(Request.QueryString.Get("isNOCView"), out _isNOCView);
            lbtnEdit.Visible = !_isNOCView;
    }
</script>

<table runat="server" id="contentTab" style="width:100%; border-width:0px;" cellpadding="0" cellspacing="0">
<tr>
	<td style="vertical-align: top;" class="editplaceholder">
    <div>
            <orion:LocalizableButtonLink ID="lbtnEdit" runat="server" href="<%# DefaultSanitizer.SanitizeHtml(this.EditURL) %>" DisplayType="Resource" CssClass="EditResourceButton" LocalizedText="Edit" />
    </div>
    </td>
</tr>
</table>
</Content>	
</orion:resourceWrapper>