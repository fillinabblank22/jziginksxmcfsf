<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_166 %>"
    ValidateRequest="false" %> 
<%-- We're entering naughty HTML into this page, can't validate the request --%>

<%@ Import Namespace="SolarWinds.Orion.Web" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

<script runat="server">
    private ResourceInfo _resource;
    protected ResourceInfo Resource
    {
        get { return _resource; }
    }

    protected override void OnInit(EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["ResourceID"]))
        {
            int resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
            this._resource = ResourceManager.GetResourceByID(resourceID);

            this.resourceTitleEditor.ResourceTitle = this.Resource.Title;
            this.resourceTitleEditor.ResourceSubTitle = this.Resource.SubTitle;

            if (this.Resource.Properties.ContainsKey("HTML"))
            {
                this.txtCustomHTML.Text = this.Resource.Properties["HTML"];    
            }
        }
        
 	    base.OnInit(e);
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (!(resourceTitleEditor.ResourceTitle.Equals(Resource.Title) && resourceTitleEditor.ResourceSubTitle.Equals(Resource.SubTitle)))
        {
            if (!String.IsNullOrEmpty(resourceTitleEditor.ResourceTitle))
            {
                Resource.Title = resourceTitleEditor.ResourceTitle;
            }else
            {
                var resourceControl = (BaseResourceControl) LoadControl(Resource.File);
                Resource.Title = resourceControl.Title;
            }
            Resource.SubTitle = resourceTitleEditor.ResourceSubTitle;
            SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
        }

        this.Resource.Properties["HTML"] = txtCustomHTML.Text;
        Response.Redirect(string.Format("/Orion/View.aspx?ViewID={0}&NetObject={1}", Resource.View.ViewID, this.Request.QueryString["NetObject"]));
    }
</script>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= DefaultSanitizer.SanitizeHtml(CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Page.Title, Request["netobject"]))) %></h1>
    
    <orion:EditResourceTitle runat="server" ID="resourceTitleEditor" />
    
    <p>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_167) %><br />
        <asp:TextBox runat="server" id="txtCustomHTML" Rows="6" Columns="60" TextMode="MultiLine" />
    </p>
    
    <orion:LocalizableButton runat="server" ID="btnSubmit" OnClick="SubmitClick" DisplayType="Primary" LocalizedText="Submit" />
</asp:Content>

