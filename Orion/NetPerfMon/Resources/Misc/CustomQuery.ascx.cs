﻿using System;
using System.Collections.Generic;
using System.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_Misc_CustomQuery : BaseResourceControl
{
    private void BindCustomTable()
    {
        CustomTable.UniqueClientID = Resource.ID;
        CustomTable.SWQL = GetSwql();
        CustomTable.SearchSWQL = GetSearchSwql();
    }
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        
        if (!string.IsNullOrEmpty(GetSearchSwql()))
        {
            ResourceWrapper.HeaderButtons.Controls.Add(SearchControl);
        }

        ResourceWrapper.CssClass = Resource.Properties["CssClass"];
        BindCustomTable();
    }

    protected ResourceSearchControl SearchControl { get; private set; }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.Misc_CustomQuery_Title_1; }
    }

    public override string IconPath
    {
        get
        {
            return Resource.Properties["IconPath"];
        }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/NetPerfMon/Controls/EditResourceControls/EditCustomQuery.ascx";
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionCorePHCustomQuery";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public string GetSwql()
    {
        var rawSwql = Resource.Properties["SWQL"] ?? string.Empty;

        if (!rawSwql.Contains("${"))
        {
            // No Macros found.  Just return the raw query
            return rawSwql;
        }

        var context = new Dictionary<string, object>();
        var provider = GetInterfaceInstance<INetObjectProvider>();
        if (provider != null)
        {
            var netobject = provider.NetObject;
            if (netobject != null)
                context = netobject.ObjectProperties;
        }

        context["UserID"] = HttpContext.Current.User.Identity.Name;

        var updatedSwql = Macros.ParseDataMacros(rawSwql, context, false, false);
        return updatedSwql;
    }

    public string GetSearchSwql()
    {
        var rawSwql = Resource.Properties["SearchSWQL"] ?? string.Empty;

        if (!rawSwql.Contains("${"))
        {
            // No Macros found.  Just return the raw query
            return rawSwql;
        }

        var context = new Dictionary<string, object>();
        var provider = GetInterfaceInstance<INetObjectProvider>();
        if (provider != null)
        {
            var netobject = provider.NetObject;
            if (netobject != null)
                context = netobject.ObjectProperties;
        }

        context["UserID"] = HttpContext.Current.User.Identity.Name;

        var updatedSwql = Macros.ParseDataMacros(rawSwql, context, false, false);
        return updatedSwql;
    }
}