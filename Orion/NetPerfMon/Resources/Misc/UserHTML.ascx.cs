﻿using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using WebSettingsDAL = SolarWinds.Orion.Web.DAL.WebSettingsDAL;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsCustomResourcesValues.Configurable)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.IsFederationAware, "true")]
public partial class Orion_NetPerfMon_Resources_Misc_UserHTML : SolarWinds.Orion.Web.UI.BaseResourceControl
{
    #region properties
    // overriden DefaultTitle
    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_VB0_115; }
    }

    
    // overriden HelpLink
    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceCustomHTMLText";
        }
    }

	public override string EditControlLocation
	{
		get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditUserHTML.ascx"; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            bool result;
            if (bool.TryParse(Resource.Properties["IsDynamicLoading"], out result) && result)
            {
               return ResourceLoadingMode.Synchronous;
            }
            return ResourceLoadingMode.Static;
            
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        Resource.Title = Parse(Resource.Title);
        Resource.SubTitle = Parse(Resource.SubTitle);

        bool sanitizeHTML = WebSettingsDAL.GetValue("HTMLResourceSanitizationEnabled", true);
        string temp = Parse(Resource.Properties["HTML"]);
        if (sanitizeHTML)
        {
            this.HTML_DIV.InnerHtml = DefaultSanitizer.SanitizeHtml(temp)?.ToString();
        }
        else
            this.HTML_DIV.InnerHtml = temp;
    }

    private string Parse(string text)
    {
        if( string.IsNullOrEmpty(text))
        {
            return text;
        }

        NetObject netObj = null;

        // if is netObject a Node set filter for a node
        INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();
        if (nodeProvider != null)
        {
            netObj = NetObjectFactory.Create(nodeProvider.Node.NetObjectID);
        }

        // if is netObject an Interface set filter for a interface
        IInterfaceProvider interfaceProvider = GetInterfaceInstance<IInterfaceProvider>();
        if (interfaceProvider != null)
        {
            netObj = NetObjectFactory.Create(interfaceProvider.Interface.NetObjectID);
        }

        // if is netObject a Volume set filter for a volume
        IVolumeProvider volumeProvider = GetInterfaceInstance<IVolumeProvider>();
        if (volumeProvider != null)
        {
            netObj = NetObjectFactory.Create(volumeProvider.Volume.NetObjectID);
        }

        var netObjectProvider = GetInterfaceInstance<INetObjectProvider>();
        if (netObjectProvider != null)
        {
            netObj = netObjectProvider.NetObject;
        }

        if (netObj != null)
        {
            return Macros.ParseDataMacros(text, netObj.ObjectProperties, true, true);
        }
        else
        {
            return text;
        }
    }

}
