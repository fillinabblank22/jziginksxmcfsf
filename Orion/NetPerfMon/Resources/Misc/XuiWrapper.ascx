<%@ Control Language="C#" AutoEventWireup="true" CodeFile="XuiWrapper.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Misc_XuiWrapper" %>
<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true" CssClass="XuiResourceWrapper">
    <Content>
        <div class="xui" style="overflow:auto">
            <<%= DefaultSanitizer.SanitizeHtml(Selector) %>></<%= DefaultSanitizer.SanitizeHtml(Selector) %>>
        </div>
    </Content>
</orion:ResourceWrapper>