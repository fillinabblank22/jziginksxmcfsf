<%@ Control Language="C#" ClassName="SearchNodes" Inherits="SolarWinds.Orion.Web.UI.BaseResourceControl" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>

<%@ Import Namespace="SolarWinds.Orion.NPM.Web" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Swis" %>

<script runat="server">
    private bool? _isLegacySearch;

    private bool IsLegacySearch
    {
        get
        {
            if (_isLegacySearch == null)
            {
                _isLegacySearch = GetStringValue("SearchMode", string.Empty).Equals("legacy", StringComparison.OrdinalIgnoreCase);
            }
            return _isLegacySearch.Value;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        InitNodeProps();

        txtSearchString.Attributes.Add("onkeydown", "if(event.keyCode==13){var e=jQuery.Event(event); e.stopPropagation(); e.preventDefault(); __doPostBack('" + btnSearch.UniqueID + "',''); return false; }");

        if (new FeatureManager().GetMaxElementCount(WellKnownElementTypes.Nodes) > 0)
        {
            var script = string.Format("if($('#{0}').val()=='') return false;", txtSearchString.ClientID);
            btnSearch.Attributes.Add("onclick", script);
        }
        else
        {
            Wrapper.Visible = false;
        }

        base.OnInit(e);
    }
    
    private void InitNodeProps()
    {						
        lbxNodeProperty.Items.Clear();
        lbxNodeProperty.Items.Add(new ListItem(Resources.CoreWebContent.WEBCODE_VB0_123, "Caption"));
        lbxNodeProperty.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_VB0_13, "IPAddress"));
        lbxNodeProperty.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_AK0_352, "IP_Address_Type"));
        lbxNodeProperty.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_VB0_126, "DNS"));
        lbxNodeProperty.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_VB0_16, "MachineType"));
        lbxNodeProperty.Items.Add(new ListItem(Resources.CoreWebContent.WEBCODE_VB0_124, "Vendor"));
        lbxNodeProperty.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_VB0_15, "Description"));
        lbxNodeProperty.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_VB0_128, "Location"));
        lbxNodeProperty.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_VB0_129, "Contact"));
        lbxNodeProperty.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_VB0_14, "Status"));
        lbxNodeProperty.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_VB0_132, "IOSImage"));
        lbxNodeProperty.Items.Add(new ListItem(Resources.CoreWebContent.WEBCODE_VB0_125, "IOSVersion"));
        
        List<string> allFields = IsLegacySearch ? Node.GetAllFields() : new List<string>(0);
        IList<string> customProperties = Node.GetCustomPropertyNames(true);
        
        foreach (string field in allFields)
        {
            if ((!Node._predefinedFields.Contains(field)) && (!customProperties.Contains(field)))
            {
                lbxNodeProperty.Items.Add(field);
            }
        }

        foreach (string prop in customProperties)
        {
            lbxNodeProperty.Items.Add(prop);
        }
    }

    protected void SearchClick(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtSearchString.Text))
        {
            string searchMode = IsLegacySearch ? "&SearchMode=legacy" : string.Empty;
            string escapedSearchString = Uri.EscapeDataString(txtSearchString.Text);

            Response.Redirect(string.Format("/Orion/NetPerfMon/Resources/NodeSearchResults.aspx?Property={0}&SearchText={1}{2}&ResourceID={3}",
                                            lbxNodeProperty.SelectedValue, escapedSearchString, searchMode, Resource.ID)
                );
        }
    }
    
    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_VB0_127; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceSearchNodes";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Static; }
    }
</script>

<orion:ResourceWrapper runat="server" ID="Wrapper">
    <Content>
        <table class="DefaultShading LayoutTable">
            <tr>
                <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_178) %></td>
                <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_179) %></td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox runat="server" ID="txtSearchString" Width="95%" Rows="1" />
                </td>
                <td>
                    <asp:ListBox runat="server" ID="lbxNodeProperty" Rows="1" SelectionMode="Single" Width="100%" />
                </td>
                <td>
                    <orion:LocalizableButton DisplayType="Secondary" ID="btnSearch" OnClick="SearchClick" CausesValidation="False" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_VB0_131 %>" runat="server" />
                </td>
            </tr>
            <tr><td colspan="3"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_180) %></td></tr>
        </table>
    </Content>
</orion:ResourceWrapper>
