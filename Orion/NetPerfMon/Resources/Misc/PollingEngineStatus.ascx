<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PollingEngineStatus.ascx.cs"
    Inherits="Orion_NetPerfMon_Resources_Misc_PollingEngineStatus" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <br />
        <span style="font-weight: bold; padding-left: 10px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_141) %> &nbsp;&nbsp;</span><asp:Label
            ID="LastDBUpdateLabel" runat="server" Text="-" />
        <asp:Repeater ID="EngineRepeater" runat="server">
            <ItemTemplate>
                <table width="100%" cellpadding="10px">
                    <tr>
                        <td style="font-size: small">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_169) %> &nbsp; 
                                <%# DefaultSanitizer.SanitizeHtml(Eval("ServerName")) %>
                           
                        </td>
                    </tr>
                </table>
                <table width="100%" cellpadding="5px">
                    <tr>
                        <td style="width: 120px; font-weight: bold">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_13) %>
                        </td>
                        <td>
                            <%# DefaultSanitizer.SanitizeHtml(Eval("IP")) %>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_170) %>
                        </td>
                        <td>
                            <%# DefaultSanitizer.SanitizeHtml(FormatKeepAlive(Eval("KeepAlive") as DateTime?)) %>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_171) %>
                        </td>
                        <td>
                            <%# DefaultSanitizer.SanitizeHtml(Eval("Nodes")) %>
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_162) %><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.String_Separator_1_VB0_59) %>
                            <%# DefaultSanitizer.SanitizeHtml(Eval("Interfaces")) %>
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_36) %><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.String_Separator_1_VB0_59) %>
                            <%# DefaultSanitizer.SanitizeHtml(Eval("Volumes")) %>
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_37) %><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.String_Separator_1_VB0_59) %> 
                            <%# DefaultSanitizer.SanitizeHtml(Eval("Elements")) %>
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_174) %>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_175) %>
                        </td>
                        <td>
                            <%# DefaultSanitizer.SanitizeHtml(String.Format("{0:00.00}%",Eval("PollingCompletion"))) %>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_131) %>
                        </td>
                        <td>
                            <%# DefaultSanitizer.SanitizeHtml(Eval("WindowsVersion")) %>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_176) %>
                        </td>
                        <td>
                            <%# DefaultSanitizer.SanitizeHtml(Eval("ServicePack")) %>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </asp:Repeater>
    </Content>
</orion:ResourceWrapper>
