<%@ Control Language="C#" ClassName="EditCustomNodeProperty" Inherits="SolarWinds.Orion.Web.UI.BaseResourceControl" %>
<%@ Import Namespace="SolarWinds.InformationService.Contract2" %>
<%@ Import Namespace="SolarWinds.Logging" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common"%>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register Src="~/Orion/Controls/EditCpValueControl.ascx" TagPrefix="orion" TagName="EditCpValueControl" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Indications" %>
<%@ Import Namespace="SolarWinds.Orion.NPM.Web" %>
<%@ Import Namespace="SolarWinds.Orion.Web.CPE" %>

<script runat="server">
    private Log _log = new Log();
    private const string NodeCustomPropertiesTableName = "NodesCustomProperties";
    
    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_VB0_108; }
    }
    
    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new Type[] { typeof(INodeProvider) };
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceEditCustomNodeProperty";
        }
    }
    
    public string CustomProperty
    {
        get
        {
            if (this.Resource.Properties.ContainsKey("CustomProperty"))
                return this.Resource.Properties["CustomProperty"];

            return string.Empty;
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/SelectCustomNodeProperty.ascx"; }
    }
    
    protected override void OnInit(EventArgs e)
    {
        if (string.IsNullOrEmpty(this.CustomProperty))
        {
            this.phNoPropMessage.Visible = true;
            this.phPropEdit.Visible = false;
        }
        else
        {
            var customProperty = CustomPropertyMgr.GetCustomProperty(NodeCustomPropertiesTableName, this.CustomProperty);
            PropertyValue.Configure(true, customProperty);
            PropertyValue.Text = ThisNode.CustomProperties[this.CustomProperty];
        }
        
        base.OnInit(e);
    }

    protected void UpdateClick(object sender, EventArgs e)
    {
        if (!Page.IsValid) return;
        
        try
        {
            var customProperty = CustomPropertyMgr.GetCustomProperty(NodeCustomPropertiesTableName, this.CustomProperty);

            var value = PropertyValue.Text.Trim();

            if (customProperty.Values.Length > 0 && !string.IsNullOrEmpty(value))
            {
                var list = new List<string>(customProperty.Values);

                if (!list.Contains(value))
                {
                    list.Add(value);
                    CustomPropertyHelper.UpdateRestrictedValuesIfNeeded(customProperty, list.ToArray());
                }
            }


            CustomPropertyMgr.SetCustomProp(NodeCustomPropertiesTableName, this.CustomProperty, this.ThisNode.NodeID, value);

            // report indication
            var changedProperties = new PropertyBag()
                                        {
                                            {this.CustomProperty, value},
                                            {"InstanceType", "Orion.NodesCustomProperties"},
                                            {"NodeId", ThisNode.NodeID},
                                            {"ObjectName", ThisNode.Name},
                                        };


            IndicationPublisher.CreateV3().ReportIndication(
                IndicationHelper.GetIndicationType(IndicationType.System_InstanceModified),
                IndicationHelper.GetIndicationProperties(),
                changedProperties);


        }
        catch (Exception ex)
        {
            _log.Error(String.Format("Edit Custopm Node Property: Exception while updating custom property '{0}'", CustomProperty), ex);
            this.PropertyValue.Text = this.ThisNode.CustomProperties[this.CustomProperty];
        }
        
        Response.Redirect(this.Request.RawUrl); //full page refresh
    }

    protected Node ThisNode
    {
        get { return ((INodeProvider) this.GetInterfaceInstance(typeof (INodeProvider))).Node; }
    }

</script>
<orion:ResourceWrapper runat="server">
    <Content>
        <div class="DefaultShading sw-no-row-lines" style="padding-left: 9px;">
            <asp:PlaceHolder runat="server" Visible="false" ID="phNoPropMessage">
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_165) %>
                <%= DefaultSanitizer.SanitizeHtml(String.Format("<a href=\"{0}\">{1}", this.EditURL, String.Format(Resources.CoreWebContent.WEBCODE_VB0_109, "</a>"))) %>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="phPropEdit">
                <%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBCODE_VB0_110, this.Resource.Properties["CustomProperty"])) %><br />
                <asp:UpdatePanel runat="server" >
                    <ContentTemplate>
                        <orion:EditCpValueControl ID="PropertyValue" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <br/>
                <orion:LocalizableButton runat="server" LocalizedText="CustomText" ValidationGroup="EditCpValue" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_181 %>" DisplayType="Secondary" OnClick="UpdateClick" />
                <br />&nbsp;
            </asp:PlaceHolder>
        </div>
    </Content>
</orion:ResourceWrapper>
