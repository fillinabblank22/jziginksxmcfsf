﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserHTML.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Misc_UserHTML" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <div runat="server" ID="HTML_DIV" /> 
    </Content>
</orion:ResourceWrapper>
