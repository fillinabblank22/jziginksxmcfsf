﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.UI;

using SolarWinds.Logging;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Extensions;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsCustomResourcesValues.Configurable)]
public partial class Orion_NetPerfMon_Resources_Misc_CustomObjectResource : GraphResource
{
    private string netObjectID;
    private BaseResourceControl resourceControl;
    private OrionChartingHelper.CustomObjectMode _netObjectMode = OrionChartingHelper.CustomObjectMode.SingleObjectMode;
    private static Log log = new Log();

    #region multiObjectchart
    private string chartName;
    private string selectedEntities;

    private string entityName;
    private bool autoHide = false;
    private string topXX = String.Empty;
    private bool filterEntities;
    private bool manualSelection;
    #endregion

    public override string EditURL
    {
        get
        {
            string url = string.Format("/Orion/NetPerfMon/Resources/EditCustomObjectResource.aspx?ResourceID={0}", this.Resource.ID);
            return ExtendCustomEditUrl(url);
        }
    }

    protected override string DefaultTitle
    {
        get
        {
            switch (_netObjectMode)
            {
                case OrionChartingHelper.CustomObjectMode.MultiObjectMode:
                case OrionChartingHelper.CustomObjectMode.MultiObjectChartsV2:
                    return Resources.CoreWebContent.WEBCODE_AK0_87;
                case OrionChartingHelper.CustomObjectMode.SingleObjectMode:
                default:
                    if (Resource != null)
                    {
                        if (String.IsNullOrEmpty(Resource.Title))
                        {
                            var title = Resources.CoreWebContent.WEBCODE_VB0_111;
                            Resource.Properties["Title"] = title;
                            return title;
                        }

                        return Resource.Title;
                    }
                    return Resources.CoreWebContent.WEBCODE_VB0_112;
            }
        }
    }

    public override string SubTitle
    {
        get
        {
            if (Resource != null)
            {
                if (String.IsNullOrEmpty(Resource.SubTitle))
                {
                    var subTitle = Resources.CoreWebContent.WEBCODE_VB0_111;
                    Resource.Properties["SubTitle"] = subTitle;
                    return subTitle;
                }

                return Resource.SubTitle;
            }
            return Resources.CoreWebContent.WEBCODE_VB0_111;
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceCustomObject"; }
    }

    public string ObjectSWISType
    {
        get
        {
            return SolarWinds.Orion.Web.Dependencies.DependencyUIItem.GetSwisEntityTypeByEntityNameOrObjectFilter(Resource.Properties["EntityName"], Resource.Properties["ObjectFilter"]);
        }
    }

    private NetObject GetNetObject()
    {
        if (string.IsNullOrEmpty(Resource.Properties["NetObjectID"]) && string.IsNullOrEmpty(Resource.Properties["ObjectUri"]))
            return null;

        if (!string.IsNullOrEmpty(Resource.Properties["ObjectUri"]))
            return GetNetObject(UriHelper.GetNetObjectId(Resource.Properties["ObjectUri"], ObjectSWISType));
        else
            return GetNetObject(Resource.Properties["NetObjectID"]);
    }

    private NetObject GetNetObject(string netObjectID)
    {        
        try
        {
            return NetObjectFactory.Create(netObjectID);
        }
        catch (AccountLimitationException)
        {            
            return null;
        }
        catch (Exception)
        {
            //Could not find an object
            return null;
        }
    }
        
    protected void Page_Load(object sender, EventArgs e)
    {
        int objectMode = 0;
        CanContainAngularJsResource = true;

        if (Resource.Properties["CustomObjectMode"] != null && Int32.TryParse(Resource.Properties["CustomObjectMode"], out objectMode))
        {
            _netObjectMode = (OrionChartingHelper.CustomObjectMode)objectMode;
        }

        switch (_netObjectMode)
        {
            case OrionChartingHelper.CustomObjectMode.MultiObjectMode:
            case OrionChartingHelper.CustomObjectMode.MultiObjectChartsV2:
                MultiObjectModeInit();
                break;
            case OrionChartingHelper.CustomObjectMode.SingleObjectMode:
            default:
                SingleObjectModeInit();
                break;
        }        
    }

    private void SingleObjectModeInit()
    {
        if (string.IsNullOrEmpty(Resource.Properties["NetObjectID"]) && string.IsNullOrEmpty(Resource.Properties["ObjectUri"]))
        {
            InitializeDefaultText();
            return;
        }

        NetObject netObject = GetNetObject();
        if (netObject == null)
            return;

        ResourceHostControl resHostControl = ResourceHostManager.GetResourceHostControl(ObjectSWISType, netObject);

        if (ResourceContainer != null) 
            ResourceContainer.Controls.Clear();

        if (resHostControl != null)
            ResourceContainer.Controls.Add(resHostControl);

        BaseResourceControl control = GetResourceControl();
        if (control != null)
        {
            OverrideInnerResourceQueryString(control);
            if (control is GraphResource)
            {
                //FB24760 - let chart to set up it's own Title and SubTitle 
                Resource.Properties["Title"] = string.Empty;
                Resource.Properties["SubTitle"] = string.Empty;
            }
            control.Resource = Resource;
            if (ResourceContainer != null)
                resHostControl.Controls.Add(control.TemplateControl);
                                    
            var wrapper = FindControls<ASP.orion_resourcewrapper_ascx>(control);
            if (wrapper != null)
            {
                wrapper.ShowHeaderBar = false; 
            }


            StandardChartResource standardChart = control.TemplateControl as StandardChartResource;
            if (standardChart != null)
            {
                wrapper.Attributes["style"] = "overflow: visible; box-shadow: 0px 0px 0px white";
                IResourceWrapper corWrapper = this.Wrapper as IResourceWrapper;

                if (corWrapper != null)
                {
                    IResourceWrapper chartWrapper = wrapper as IResourceWrapper;
                    corWrapper.ManageButtonText = chartWrapper.ManageButtonText;
                    corWrapper.ManageButtonTarget = chartWrapper.ManageButtonTarget;
                    corWrapper.ShowManageButton = chartWrapper.ShowManageButton;
                    if (chartWrapper.ShowManageButton)
                    {
                        var btnManageButton = this.Wrapper.FindControl("ManageButton");
                        LocalizableButtonLink localizableManageButton = btnManageButton as LocalizableButtonLink;
                        if (localizableManageButton != null)
                        {
                            localizableManageButton.Target = "_blank";
                            localizableManageButton.CssClass = "NoTip " + localizableManageButton.CssClass;
                        }

                        standardChart.AfterShownEditPlaceholder += (s, e) =>
                        {
                            corWrapper.ShowManageButton = false;
                            chartWrapper.ShowManageButton = false;
                            btnManageButton.Visible = false;
                        };
                    }
                }
            }
        }
    }

    private void InitializeDefaultText()
    {
        if (ResourceContainer != null)
        {
            ResourceContainer.Controls.Clear();
            ResourceContainer.Controls.Add(new WebResourceWantsEdit
            {
                HelpfulHtml = @"<ul>
<li>" + Resources.CoreWebContent.WEBCODE_PCC_13 + @"</li>
<li>" + Resources.CoreWebContent.WEBCODE_PCC_14 + @"</li>
</ul>",
                EditUrl = Profile.AllowCustomize ? EditURL : null,
                WatermarkImage = "/Orion/images/ResourceWatermarks/Watermark.CustomObject.png",
                WatermarkHeight = 103, // image watermark ends at 103 nicely
                WatermarkWidth = 190, // default is 196, cheating and stealing 6px.
                WatermarkBackgroundColor = "#3E92C4"  //when resource gets resized, use this color for background of the rest of the image
            });
        }
        return;
    }

    private BaseResourceControl GetResourceControl()
    {
        if (resourceControl != null)
            return resourceControl;

        var embeddedPath = Resource.Properties["EmbeddedObjectResource"];

        if (string.IsNullOrEmpty(embeddedPath))
            return null;

        var embeddedData = embeddedPath.Split(';');

        Control control = LoadControl(embeddedData[0]);
        control.ID = "ObjectResourceControl";

        return resourceControl = control as BaseResourceControl;
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            var control = GetResourceControl();

            int objectMode;
            return control?.ResourceLoadingMode ??
                   // if no resource is set and it is multi object charts v1 then use RenderControl mode
                   (Int32.TryParse(Resource.Properties["CustomObjectMode"], out objectMode) &&
                        objectMode == (int) OrionChartingHelper.CustomObjectMode.MultiObjectMode
                       ? ResourceLoadingMode.RenderControl : ResourceLoadingMode.Static);
        }
    }

    private T FindControls<T>(Control parent) where T : Control
    {
        T control;

        FindControls(parent, out control);

        return control;
    }

    private void FindControls<T>(Control parent, out T control) where T : Control
    {
        control = default(T);
        foreach (Control c in parent.Controls)
        {
            if (control != null)
            { //we found what we were looking for exit from recursion                 
                break;
            }

            if (c is T)
            {
                control = c as T;
                break;
            }
            else if (c.Controls.Count > 0)
            {
                FindControls(c, out control);
            }
        }
    }

    #region MultiObject resource

    private void MultiObjectModeInit()
    {
        if (CheckProperties(Resource.Properties))
        {

            if (!manualSelection || filterEntities)
            {
                selectedEntities = OrionChartingHelper.GetEntitiesFilteredByNetObject(entityName, Request.QueryString["NetObject"], (!manualSelection) ? null : selectedEntities, topXX);
            }
            selectedEntities = string.Join(",", MultiObjectLimitationHelper.GetAccountLimitedEntities(selectedEntities, entityName));

            Dictionary<string, string> parameters = new Dictionary<string, string> { 
					{"SelectedEntities", selectedEntities},
					{"ChartEntity", Resource.Properties["EntityName"]},
					{"ShowSum", Resource.Properties["ShowSum"]},
					{"DataColumnName", Resource.Properties["DataColumnName"]},
					{"FilterEntities", Resource.Properties["FilterEntities"]},
					{"TopXX", Resource.Properties["TopXX"]},
                    {"ResourceID", Resource.ID.ToString()}
				};

            if (autoHide && String.IsNullOrEmpty(selectedEntities))
            {
                Visible = false;
            }
            else
            {
                if (IsChartV2())
                {
                    var nodeID = NetObjectFactory.GetNetObjectPrefixBySWISType(Resource.Properties["EntityName"]) + ":" + selectedEntities.Split(',').FirstOrDefault();
                    var netObject = GetNetObject(nodeID);

                    var cnt = LoadControl(Resource.Properties["EmbeddedObjectResource"]) as StandardChartResource;
                    if (netObject != null && cnt != null && cnt.SupportedInterfaces.Any(t => typeof(IMultiObjectProvider).IsAssignableFrom(t)))
                    {
                        OverrideInnerResourceQueryString(cnt);

                        var resourceHost = ResourceHostManager.GetResourceHostControl(Resource.Properties["EntityName"], netObject);
                        var resource = new ResourceInfo();
                        resource.Properties.Add("ChartName", Resource.Properties["ChartName"]);
                        resource.Properties.Add("SelectedEntities", selectedEntities);
                        resource.Properties.Add("ChartEntity", Resource.Properties["EntityName"]);
                        resource.Properties.Add("ShowSum", Resource.Properties["ShowSum"]);
                        if (!String.IsNullOrEmpty(Resource.Properties["ChartTitle"]))
                            resource.Properties.Add("ChartTitle", Resource.Properties["ChartTitle"]);
                        if (!String.IsNullOrEmpty(Resource.Properties["ChartSubTitle"]))
                            resource.Properties.Add("ChartSubTitle", Resource.Properties["ChartSubTitle"]);
                        if (!String.IsNullOrEmpty(Resource.Properties["ShowTitle"]))
                            resource.Properties.Add("ShowTitle", Resource.Properties["ShowTitle"]);
                        if (!String.IsNullOrEmpty(Resource.Properties["DateFrom"]))
                            resource.Properties.Add("DateFrom", Resource.Properties["DateFrom"]);
                        if (!String.IsNullOrEmpty(Resource.Properties["DateTo"]))
                            resource.Properties.Add("DateTo", Resource.Properties["DateTo"]);                        

                        resource.View = Resource.View;
                        resource.Column = Resource.Column;
                        resource.ID = this.Resource.ID;
                        resource.TimeFrame = this.Resource.TimeFrame;
                        resource.Title = this.Resource.Title;
                        resource.SubTitle = this.Resource.SubTitle;
                        cnt.Resource = resource;
                        this.Resource = resource;

                        int calculate;
                        if (!String.IsNullOrEmpty(Resource.Properties["CalculateTrendLine"]) && Int32.TryParse(Resource.Properties["CalculateTrendLine"], out calculate))
                            cnt.CalculateTrendLine = calculate != 0;
                        else
                            cnt.CalculateTrendLine = false;

                        if (!String.IsNullOrEmpty(Resource.Properties["CalculateSumLine"]) && Int32.TryParse(Resource.Properties["CalculateSumLine"], out calculate))
                            cnt.CalculateSumLine = calculate != 0;
                        else
                            cnt.CalculateSumLine = false;

                        if (!String.IsNullOrEmpty(Resource.Properties["Calculate95thPercentile"]) && Int32.TryParse(Resource.Properties["Calculate95thPercentile"], out calculate))
                            cnt.Calculate95thPercentileLine = calculate != 0;
                        else
                            cnt.Calculate95thPercentileLine = false;

                        if (!String.IsNullOrEmpty(Resource.Properties["ChartInitialZoom"]))
                            cnt.InitialZoom = Resource.Properties["ChartInitialZoom"];

                        if ((!String.IsNullOrEmpty(Resource.Properties["SampleSize"])) && Int32.TryParse(Resource.Properties["SampleSize"], out calculate))
                            cnt.SampleSize = calculate;

                        double timespan;
                        if ((!String.IsNullOrEmpty(Resource.Properties["ChartDateSpan"])) && Double.TryParse(Resource.Properties["ChartDateSpan"], out timespan))
                            cnt.TimeSpanInDays = timespan;
                        
                        var wrapper = cnt.TemplateControl.FindControl("Wrapper") as IResourceWrapper;
                        if (wrapper != null)
                            wrapper.ShowHeaderBar = false;

                        resourceHost.Controls.Add(cnt.TemplateControl);
                        ResourceContainer.Controls.Clear();
                        ResourceContainer.Controls.Add(resourceHost);                        
                    }
                }
                else
                {
                    CreateChart(null, Request.QueryString["NetObject"], chartName, parameters, ResourceContainer);
                    Wrapper.SetUpDrDownMenu(Resource.Properties["ChartName"], Resource, string.Empty, parameters);
                }
            }
        }
        else
        {
            InitializeDefaultText();
        }
    }

    private bool LoadProperty<T>(StringDictionary dict, string propertyName, out T property)
       where T : IConvertible
    {
        property = default(T);

        if (dict != null)
        {
            var smallPropertyName = propertyName.ToLowerInvariant();
            if (String.IsNullOrEmpty(dict[smallPropertyName]))
                return false;

            try
            {
                property = (T)Convert.ChangeType(dict[smallPropertyName], typeof(T));
            }
            catch
            {
                return false;
            }

            return true;
        }

        return false;
    }


    private bool CheckProperties(StringDictionary props)
    {
        if (!LoadProperty<string>(props, "EntityName", out entityName))
            return false;

        if (!LoadProperty<string>(props, "ChartName", out chartName))
            return false;

        if (!LoadProperty<bool>(props, "ManualSelect", out manualSelection))
        {
            // FB78334 - in 10.1.2 this resource doesn't support automatic netobject selection, only manual.
            // So, after upgrade to 10.2 "ManualSelect" resource property isn't exists.
            if (!String.IsNullOrEmpty(props["ManualSelect"]))
                return false;
            manualSelection = true;
        }

        if (manualSelection)
        {
            if (!LoadProperty<string>(props, "SelectedEntities", out selectedEntities))
                return false;

            if (!LoadProperty<bool>(props, "FilterEntities", out filterEntities))
                return false;
        }
        else
        {
            if (String.IsNullOrEmpty(Request.QueryString["NetObject"]))
                return false;
        }

        LoadProperty<string>(props, "TopXX", out topXX);

        if (String.IsNullOrEmpty(props["ShowSum"]))
            return false;

        string hide;
        if (LoadProperty<string>(props, "AutoHide", out hide))
            autoHide = (hide != "0");

        return true;
    }

    private bool IsChartV2()
    {
        return (_netObjectMode == OrionChartingHelper.CustomObjectMode.MultiObjectChartsV2);
    }

    #endregion


    private void OverrideInnerResourceQueryString(BaseResourceControl innerResource)
    {
        //Ensures internal resource is sandboxed with its own QueryString and is not affected by the cantaining view.
        innerResource.QueryString = Enumerable.Empty<KeyValuePair<string, string>>().ToNameValueCollection();
    }
}
