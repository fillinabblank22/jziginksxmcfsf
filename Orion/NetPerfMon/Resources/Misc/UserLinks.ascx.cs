using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Data;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsCustomResourcesValues.Configurable)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.IsFederationAware, "true")]
public partial class Orion_NetPerfMon_Resources_Misc_UserLinks : SolarWinds.Orion.Web.UI.BaseResourceControl
{
    // number of links in the database, default is 10 - should be also property (in future implementation)
    private const int numberOfLinksInTheDatabase = 10;

    #region properties

    // overriden DefaultTitle
    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_VB0_128; }
    }

    // overriden HelpLink
    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceUserDefinedLinks";
        }
    }

	public override string EditControlLocation
	{
		get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditUserLinks.ascx"; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
 
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        Resource.Title = Parse(Resource.Title);
        Resource.SubTitle = Parse(Resource.SubTitle);

        DataTable table = new DataTable();

        // there are two columns for URL and Link text
        table.Columns.Add("Link", typeof(string));
        table.Columns.Add("URL", typeof(string));
        table.Columns.Add("TARGET", typeof(string));

        // fill table with data from database
        for (int i = 1; i <= numberOfLinksInTheDatabase; i++)
        {
            string link = Parse(this.Resource.Properties[String.Format("Link{0}", i)]);
            string url = Parse(this.Resource.Properties[String.Format("URL{0}", i)]);

            if (!String.IsNullOrEmpty(link) && !String.IsNullOrEmpty(url))
            {
                DataRow r = table.NewRow();

                r["Link"] = link;
                r["URL"] = url;
                r["TARGET"] = (this.Resource.Properties[String.Format("NewWindow{0}", i)] == "on") ? "_blank" : "_parent";

                table.Rows.Add(r);
            }
        }

        // bind data to repeater
        HyperLinks.DataSource = table;
        HyperLinks.DataBind();
    }

    private string Parse(string text)
    {
        if( string.IsNullOrEmpty(text))
        {
            return text;
        }
        
        NetObject netObj = null;

        // if is netObject a Node set filter for a node
        INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();
        if (nodeProvider != null)
        {
            netObj = NetObjectFactory.Create(nodeProvider.Node.NetObjectID);
        }

        // if is netObject an Interface set filter for a interface
        IInterfaceProvider interfaceProvider = GetInterfaceInstance<IInterfaceProvider>();
        if (interfaceProvider != null)
        {
            netObj = NetObjectFactory.Create(interfaceProvider.Interface.NetObjectID);
        }

        // if is netObject a Volume set filter for a volume
        IVolumeProvider volumeProvider = GetInterfaceInstance<IVolumeProvider>();
        if (volumeProvider != null)
        {
            netObj = NetObjectFactory.Create(volumeProvider.Volume.NetObjectID);
        }

        if (netObj != null)
        {
            return Macros.ParseDataMacros(text, netObj.ObjectProperties, true, true);
        }
        else
        {
            return text;
        }
    }

}
