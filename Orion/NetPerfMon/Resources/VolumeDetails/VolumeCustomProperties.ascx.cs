using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_VolumeDetails_VolumeCustomProperties : BaseResourceControl
{
    private string EntityName { get { return "Orion.Volumes"; } }
    private string EntityIDFieldName { get { return "VolumeID"; } }
    private NetObject Netobject { set; get; }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.customPropertyList.Resource = this.Resource;
        Volume volume = GetInterfaceInstance<IVolumeProvider>().Volume;
        if (volume != null)
        {
            this.customPropertyList.NetObjectId = volume.VolumeID;
        }

        this.Netobject = volume;

        customPropertyList.CustomPropertyLoader = (netObjectId, displayProperties) =>
        {
            return CustomPropertyHelper.GetCustomPropertiesForObject(netObjectId, displayProperties, EntityName, EntityIDFieldName, Netobject);
        };

        customPropertyList.EditCustomPropertiesLinkGenerator = (netObjectId, redirectUrl) =>
        {
            if (Request.UrlReferrer != null)
            {
                redirectUrl = UrlHelper.ToSafeUrlParameter(Request.UrlReferrer.PathAndQuery);
            }
            return String.Format("/Orion/Nodes/VolumeProperties.aspx?Volumes={0}&GuidID={2}&ReturnTo={1}", netObjectId, redirectUrl, Guid.NewGuid());
        };
    }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_AK0_123; }
    }

	public override string EditControlLocation
	{
		get
		{
			return "/Orion/NetPerfMon/Controls/EditResourceControls/EditCustomPropertyList.ascx";
		}
	}

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceCustomPropertiesVolumes"; }
    }

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(IVolumeProvider) }; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
