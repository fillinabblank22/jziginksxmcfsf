<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VolumeCustomProperties.ascx.cs" Inherits="Orion_NetPerfMon_Resources_VolumeDetails_VolumeCustomProperties" %>
<%@ Register TagPrefix="orion" TagName="CustomPropertyList" Src="~/Orion/NetPerfMon/Controls/CustomPropertyList.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:CustomPropertyList runat="server" ID="customPropertyList" CustomPropertyTableName="Volumes" />
    </Content>
</orion:resourceWrapper>