﻿using System;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Common;

public partial class Orion_NetPerfMon_Resources_VolumeDetails_VolumePollingDetails : BaseResourceControl
{
	protected Volume Volume { get; set; }

	protected void Page_Load(object sender, EventArgs e)
	{
		this.Volume = GetInterfaceInstance<IVolumeProvider>().Volume;
		if (this.Volume != null)
		{
			this.pollInterval.Text = String.Format(Resources.CoreWebContent.WEBCODE_VB0_39, this.Volume.PollInterval);
			
			this.nextPoll.Text = (this.Volume.NextPoll.Date == DateTime.Today) ?
				String.Empty : (Utils.FormatMediumDate(this.Volume.NextPoll) + " ");
			this.nextPoll.Text += Utils.FormatToLocalTime(this.Volume.NextPoll);

			this.statCollection.Text = String.Format(Resources.CoreWebContent.WEBCODE_VB0_40, this.Volume.StatCollection);

			this.rediscoveryInterval.Text = String.Format(Resources.CoreWebContent.WEBCODE_VB0_40, this.Volume.RediscoveryInterval);

			this.nextRediscovery.Text = (this.Volume.NextRediscovery.Date == DateTime.Today) ?
				String.Empty : (Utils.FormatMediumDate(this.Volume.NextRediscovery) + " ");
			this.nextRediscovery.Text += Utils.FormatToLocalTime(this.Volume.NextRediscovery);

			this.lastSync.Text = Volume.LastSync.Equals(DateTime.MinValue) ? string.Empty :
				Utils.FormatDateTimeForDisplayingOnWebsite(Volume.LastSync.ToLocalTime(), true);
		}
		else
		{
			this.volumeError.Visible = true;
			this.pollingDetailsTable.Visible = false;
		}
	}

	protected override string DefaultTitle
	{
		get { return Resources.CoreWebContent.WEBCODE_AK0_125; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceVolumePollingDetails"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(IVolumeProvider) }; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
