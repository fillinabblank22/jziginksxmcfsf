﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ManagementTasks.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeDetails_ManagementTasks" %>
<%@ Register TagPrefix="orion" TagName="ManagementTasksControl" Src="~/Orion/NetPerfMon/Controls/ManagementTasksControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="UnmanageDialog" Src="~/Orion/Controls/UnmanageDialog.ascx" %>
<%@ Register TagPrefix="orion" TagName="PollRediscoverDialog" Src="~/Orion/Controls/PollRediscoverDialog.ascx" %>

<orion:include runat="server" File="js/MaintenanceMode/UnmanageHandlers.js" />
<orion:include runat="server" File="js/MaintenanceMode/MaintenanceModeUtils.js" />

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:PollRediscoverDialog runat="server" />
        <orion:UnmanageDialog runat="server" />
        <orion:ManagementTasksControl ID="ManagementTasksControl" runat="server" />
    </Content>
</orion:resourceWrapper>
