<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VolumePollingDetails.ascx.cs" Inherits="Orion_NetPerfMon_Resources_VolumeDetails_VolumePollingDetails" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div runat="server" id="volumeError" visible="false" style="color:Red;font-weight:bold;" class="Text">
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_378) %>
        </div>
        <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%" runat="server" id="pollingDetailsTable">
            <tr>
              <td class="Property" width="10">&nbsp;</td>
              <td class="PropertyHeader" valign="middle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_135) %></td>
              <td class="Property"><asp:Label runat="server" ID="pollInterval" />&nbsp;</td>
            </tr>
            <tr>
              <td class="Property" width="10">&nbsp;</td>
              <td class="PropertyHeader" valign="middle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_136) %></td>
              <td class="Property"><asp:Label runat="server" ID="nextPoll" />&nbsp;</td>
            </tr>
            <tr>
              <td colspan="3" class="Property" width="10">&nbsp;</td>
            </tr>
            <tr>
              <td class="Property" width="10">&nbsp;</td>
              <td class="PropertyHeader" valign="middle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_137) %></td>
              <td class="Property"><asp:Label runat="server" ID="statCollection" />&nbsp;</td>
            </tr>
            <tr>
              <td colspan="3" class="Property" width="10">&nbsp;</td>
            </tr>
            <tr>
              <td class="Property" width="10">&nbsp;</td>
              <td class="PropertyHeader" valign="middle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_139) %></td>
              <td class="Property"><asp:Label runat="server" ID="rediscoveryInterval" />&nbsp;</td>
            </tr>
            <tr>
              <td class="Property" width="10">&nbsp;</td>
              <td class="PropertyHeader" valign="middle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_140) %></td>
              <td class="Property"><asp:Label runat="server" ID="nextRediscovery" />&nbsp;</td>
            </tr>
            <tr>
              <td colspan="3" class="Property" width="10">&nbsp;</td>
            </tr>
            <tr>
              <td class="Property" width="10">&nbsp;</td>
              <td class="PropertyHeader" valign="middle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_141) %></td>
              <td class="Property"><asp:Label runat="server" ID="lastSync" />&nbsp;</td>
            </tr>
        </table>
    </Content>
</orion:resourceWrapper>