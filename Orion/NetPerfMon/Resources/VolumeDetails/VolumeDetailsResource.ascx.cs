﻿using System;
using System.Web;
using System.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.Web.Enums;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.DisplayTypes;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_VolumeDetails_VolumeDetailsResource : BaseResourceControl
{
    protected Volume Volume { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Volume = GetInterfaceInstance<IVolumeProvider>().Volume;
        if (this.Volume != null)
        {
            this.volumeDescription.Text = HttpUtility.HtmlEncode(UIHelper.NormalizeSpaces(this.Volume.Description));
            this.volumeTypeIcon.ImageUrl = this.Volume.IconPath;
            this.volumeType.Text = this.Volume.VolumeTypeName;
            this.volumeSize.Text = (this.Volume.TotalSize < 0) ?
                String.Empty : new Bytes(this.Volume.TotalSize).ToShort1024String();
            this.volumeSpaceUsed.Text = (this.Volume.SpaceUsed < 0) ?
                String.Empty : new Bytes(this.Volume.SpaceUsed).ToShort1024String();
            this.volumeSpaceAvailable.Text = (this.Volume.SpaceAvailable < 0) ?
                String.Empty : new Bytes(this.Volume.SpaceAvailable).ToShort1024String();

            double warningThreshold = 0;
            double errorThreshold = 0;

            GetPercentDiskUsedThresholds(Volume.VolumeID, ref warningThreshold, ref errorThreshold);

            if (this.Volume.PercentUsed >= 0)
            {
                this.volumePercentUsed.Controls.Add(new LiteralControl(FormatHelper.GetPercentFormat(this.Volume.PercentUsed, errorThreshold, warningThreshold)));
                this.volumePercentUsedBar.Controls.Add(new LiteralControl(FormatHelper.GetPercentBarFormat(this.Volume.PercentUsed, errorThreshold, warningThreshold)));
                this.volumePercentAvailable.Controls.Add(new LiteralControl(FormatHelper.GetPercentFormat(100 - this.Volume.PercentUsed, 100 - errorThreshold, 100 - warningThreshold, true)));
                this.volumePercentAvailableBar.Controls.Add(new LiteralControl(FormatHelper.GetPercentBarFormat(100 - this.Volume.PercentUsed, 100 - errorThreshold, 100 - warningThreshold, true)));
            }
        }
        else
        {
            this.volumeError.Visible = true;
            this.detailsTable.Visible = false;
        }
    }

    protected void GetPercentDiskUsedThresholds(int volumeId, ref double warningThreshold, ref double criticalThreshold)
    {
        using (WebDAL dal = new WebDAL())
        {
            var table = dal.GetVolumeThreshold(volumeId, "Volumes.Stats.PercentDiskUsed");

            if (table == null || table.Rows.Count == 0)
            {
                return;
            }

            warningThreshold = Convert.ToDouble(table.Rows[0]["Level1Value"]);
            criticalThreshold = Convert.ToDouble(table.Rows[0]["Level2Value"]);
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_AK0_124; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceVolumeDetails"; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IVolumeProvider) }; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
