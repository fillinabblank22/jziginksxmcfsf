<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VolumeDetailsResource.ascx.cs" Inherits="Orion_NetPerfMon_Resources_VolumeDetails_VolumeDetailsResource" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div runat="server" id="volumeError" visible="false" style="color:Red;font-weight:bold;" class="Text">
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_378) %>
        </div>
        <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%" runat="server" id="detailsTable">
            <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" valign="middle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.NetworkObjectType_Volume) %></td>
	          <td class="Property" width="10">&nbsp;</td>
	          <td colspan="2" class="Property"><asp:Label runat="server" ID="volumeDescription" /></td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" valign="middle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_379) %></td>
	          <td class="Property" width="10"><asp:Image runat="server" ID="volumeTypeIcon" /></td>
	          <td class="Property"><asp:Label runat="server" ID="volumeType" /></td>
			    <td class="Property">&nbsp;</td>
	        </tr>
	        <tr>
			    <td class="Property">&nbsp;</td>
			    <td class="Property">&nbsp;</td>
			    <td class="Property">&nbsp;</td>
			    <td class="Property">&nbsp;</td>
			    <td class="Property">&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" valign="middle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_380) %></td>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="Property"><asp:Label runat="server" ID="volumeSize" /></td>
			    <td class="Property">&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" valign="middle"><a href="/Orion/NetPerfMon/CustomChart.aspx?ChartName=AvgDiskUsage&NetObject=<%= DefaultSanitizer.SanitizeHtml(this.Volume.NetObjectID) %>" target="_blank"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_TM0_27) %></a></td>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="Property"><a href="/Orion/NetPerfMon/CustomChart.aspx?ChartName=AvgDiskUsage&NetObject=<%= DefaultSanitizer.SanitizeHtml(this.Volume.NetObjectID) %>" target="_blank"><asp:Label runat="server" ID="volumeSpaceUsed" /></a></td>
			    <td class="Property">&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" valign="middle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_381) %></td>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="Property"><asp:Label runat="server" ID="volumeSpaceAvailable" />&nbsp;</td>
			    <td class="Property">&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" valign="middle"><a href="/Orion/NetPerfMon/CustomChart.aspx?ChartName=PercentDiskUsage&NetObject=<%= DefaultSanitizer.SanitizeHtml(this.Volume.NetObjectID) %>" target="_blank"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_382) %></a></td>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="Property"><a href="/Orion/NetPerfMon/CustomChart.aspx?ChartName=PercentDiskUsage&NetObject=<%= DefaultSanitizer.SanitizeHtml(this.Volume.NetObjectID) %>" target="_blank"><asp:PlaceHolder runat="server" ID="volumePercentUsed" /></a></td>
	          <td class="Property"><a href="/Orion/NetPerfMon/CustomChart.aspx?ChartName=PercentDiskUsage&NetObject=<%= DefaultSanitizer.SanitizeHtml(this.Volume.NetObjectID) %>" target="_blank"><asp:PlaceHolder runat="server" ID="volumePercentUsedBar" /></a></td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" valign="middle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_383) %></td>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="Property"><asp:PlaceHolder runat="server" ID="volumePercentAvailable" /></td>
	          <td class="Property"><asp:PlaceHolder runat="server" ID="volumePercentAvailableBar" /></td>
	        </tr>
		</table>
    </Content>
</orion:resourceWrapper>