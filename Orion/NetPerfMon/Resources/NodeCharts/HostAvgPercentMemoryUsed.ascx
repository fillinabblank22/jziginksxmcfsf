<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HostAvgPercentMemoryUsed.ascx.cs" Inherits="HostAvgPercentMemoryUsed" %>

<orion:resourceWrapper runat="server" ID="Wrapper" ShowEditButton="false">
    <Content>
        <asp:PlaceHolder runat="server" ID="chartPlaceHolder" />
		<span class="sw-pg-hint-yellow" style="display:block;">
		    <span class="sw-pg-hint-body-warning" style="font-size: small;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_246) %> <a target="_blank" rel="noopener noreferrer" href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("orioncoreaddmoderncharts")) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_OJ0_8) %></a></span>
        </span>
    </Content>
</orion:resourceWrapper>