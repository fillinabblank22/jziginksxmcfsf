using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class HostAvgPercentMemoryUsed : GraphResource
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }

		CreateChart(null, GetInterfaceInstance<INodeProvider>().Node.NetObjectID, "HostAvgPercentMemoryUsed", chartPlaceHolder);
		Wrapper.SetDrDownMenuParameters("HostAvgPercentMemoryUsed", Resource, "N");
	}

	protected override string DefaultTitle
	{
		get
		{
            return notAvailable;//"Percent Memory Used";
		}
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourcePercentMemoryUsedChart"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}