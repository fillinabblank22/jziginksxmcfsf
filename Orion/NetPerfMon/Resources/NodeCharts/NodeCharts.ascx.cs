﻿using System;

using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;

public partial class Orion_NetPerfMon_Resources_NodeCharts_NodeCharts : BaseResourceControl
{
	protected Node Node { get; private set; }

	protected void Page_Load(object sender, EventArgs e)
	{
        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }

		this.Node = this.GetInterfaceInstance<INodeProvider>().Node;

		if (this.Node == null)
		{
			this.nodeError.Visible = true;
			return;
		}

		string[] periods = { "Today", "Last 7 Days", "Last 30 Days" };

		this.chartsTable.DataSource = periods;
		this.chartsTable.DataBind();
	}

	protected override string DefaultTitle
	{
		get { return Resources.CoreWebContent.WEBCODE_AK0_86; }
	}

	public override string DisplayTitle
	{
		get { return this.DefaultTitle; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceListNodeCharts"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Static; }
    }
}
