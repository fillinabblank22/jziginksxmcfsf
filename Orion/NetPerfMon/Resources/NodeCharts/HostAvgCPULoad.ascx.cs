using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class HostAvgCPULoad : GraphResource
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }

		CreateChart(null, GetInterfaceInstance<INodeProvider>().Node.NetObjectID, "HostAvgCPULoad", chartPlaceHolder);
		Wrapper.SetDrDownMenuParameters("HostAvgCPULoad", Resource, "N");
	}

	protected override string DefaultTitle
	{
		get
		{
            return notAvailable;//"Average CPU Load";
		}
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceAverageCPULoadchart"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
