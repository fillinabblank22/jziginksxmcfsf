using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Text;
using System.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsCustomResourcesValues.Configurable)]
public partial class CustomNodeChart : GraphResource
{
	private string _chartName;

	protected override void OnInit(EventArgs e)
	{
        base.OnInit(e);

        if (!IsVisible())
        {
            this.Parent.Visible = false;
        }

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }

		if (!string.IsNullOrEmpty(Resource.Properties["ChartName"]))
		{
			_chartName = Resource.Properties["ChartName"];
			CreateChart(null, GetInterfaceInstance<INodeProvider>().Node.NetObjectID, _chartName, chartPlaceHolder);
			Wrapper.SetDrDownMenuParameters(_chartName, Resource, "N");
		}
		else
		{
			
			_chartName = string.Empty;
            var emptyResource =
                string.Format(
                    Resources.CoreWebContent.WEBCODE_VB0_331,
                    "<ul><li>",
                    String.Format("<a href=\"{0}\"><font color=\"blue\"><b>", EditURL), "</b></font></a>"
                    , "</li><li>", "</li></ul>",
                    "<span style=\"font-weight:bold;\">", "</span>");
                        
			chartPlaceHolder.Controls.Add(new SolarWinds.Orion.Web.UI.WebResourceWantsEdit
            {
                HelpfulHtml = emptyResource,
                EditUrl = Profile.AllowCustomize ? EditURL : null,
                WatermarkImage = "/Orion/images/ResourceWatermarks/Watermark.CustomNodeChart.gif",
                BackgroundPosition = "right bottom",
                WatermarkBackgroundColor = "#f2f2f2"
                // image has default watermark dimentions
            });
        }
    }

	protected override string DefaultTitle
	{
		get { return Resources.CoreWebContent.WEBCODE_AK0_85; }
	}

	public override string HelpLinkFragment
	{
		get
        {
            if (string.IsNullOrEmpty(Resource.Properties["ChartName"]))
            {
                return "OrionPHResourceCustomNodeChart";
            }

            // Try to get initialized HelpLinkFragment otherwise old help topic composition fallback
            return (Resource.Properties.ContainsKey("HelpLinkFragment") && !string.IsNullOrEmpty(Resource.Properties["HelpLinkFragment"]))
                       ? Resource.Properties["HelpLinkFragment"]
                       : "Chart-" + Resource.Properties["ChartName"];
        }
	}

	public override string EditControlLocation
	{
		get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditCustomNodeChart.ascx"; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

    // Is the resource hidden because of beeing CPU and not beeing polled?
    private bool IsVisible()
    {
        INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();
        
        if (nodeProvider == null || nodeProvider.Node == null)
        {
            return true;
        }

        if (Resource.Properties["ChartName"] == null)
        {
            return true;
        }
        else if (String.Compare(Resource.Properties["ChartName"], "CiscoMMAvgCPULoad", true) == 0 || 
                 String.Compare(Resource.Properties["ChartName"], "HostMMAvgCPULoad", true) == 0)
        {
            return ((nodeProvider.Node.CPULoad >= 0) && (nodeProvider.Node.PercentMemoryUsed >= 0));
        }
        else
        {
            return true;
        }
    }
}
