<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeCharts.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeCharts_NodeCharts" %>

<orion:resourceWrapper runat="server" ShowEditButton="false">
    <Content>
        <div runat="server" id="nodeError" visible="false" style="color:Red; font-weight:bold;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_63) %></div>
        <asp:Repeater runat="server" ID="chartsTable">
            <HeaderTemplate>
                <table class="Text" width="100%" cellpadding="3" cellspacing="0">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
		            <td class="PropertyHeader" colspan="2"><%# DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Charting.Periods.GetLocalizedPeriod(Container.DataItem.ToString())) %></td>
	            </tr>
	            <tr>
		            <td class="Property">&nbsp;&nbsp;</td>
		            <td class="Property"><a href="/Orion/NetPerfMon/CustomChart.aspx?ChartName=AvgRTLoss&NetObject=<%= DefaultSanitizer.SanitizeHtml(this.Node.NetObjectID) %>&Period=<%# DefaultSanitizer.SanitizeHtml(Container.DataItem) %>" target="_new"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_222) %></a></td>
	            </tr>		
	            <tr>
		            <td class="Property">&nbsp;&nbsp;</td>
		            <td class="Property"><a href="/Orion/NetPerfMon/CustomChart.aspx?ChartName=MMAvgRT&NetObject=<%= DefaultSanitizer.SanitizeHtml(this.Node.NetObjectID) %>&Period=<%# DefaultSanitizer.SanitizeHtml(Container.DataItem) %>" target="_new"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_223) %></a></td>
	            </tr>		
	            <tr>
		            <td class="Property">&nbsp;&nbsp;</td>
		            <td class="Property"><a href="/Orion/NetPerfMon/CustomChart.aspx?ChartName=HostMMAvgCPULoad&NetObject=<%= DefaultSanitizer.SanitizeHtml(this.Node.NetObjectID) %>&Period=<%# DefaultSanitizer.SanitizeHtml(Container.DataItem) %>" target="_new"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_VB0_71) %></a></td>
	            </tr>		
	            <tr>
		            <td class="Property">&nbsp;&nbsp;</td>
		            <td class="Property"><a href="/Orion/NetPerfMon/CustomChart.aspx?ChartName=HostAvgPercentMemoryUsed&NetObject=<%= DefaultSanitizer.SanitizeHtml(this.Node.NetObjectID) %>&Period=<%# DefaultSanitizer.SanitizeHtml(Container.DataItem) %>" target="_new"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_224) %></a></td>
	            </tr>		
	            <tr runat="server" visible="<%# DefaultSanitizer.SanitizeHtml(this.Node.Cisco) %>">
		            <td class="Property">&nbsp;&nbsp;</td>
		            <td class="Property"><a href="/Orion/NetPerfMon/CustomChart.aspx?ChartName=BufferFailures&NetObject=<%= DefaultSanitizer.SanitizeHtml(this.Node.NetObjectID) %>&Period=<%# DefaultSanitizer.SanitizeHtml(Container.DataItem) %>" target="_new"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_225) %></a></td>
	            </tr>		
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>        
        </asp:Repeater>
		<span class="sw-pg-hint-yellow" style="display:block;">
		    <span class="sw-pg-hint-body-warning" style="font-size: small;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_246) %> <a target="_blank" rel="noopener noreferrer" href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("orioncoreaddmoderncharts")) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_OJ0_8) %></a></span>
        </span>
    </Content>
</orion:resourceWrapper>