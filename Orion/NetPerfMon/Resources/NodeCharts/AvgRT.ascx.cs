using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class AvgRT : GraphResource
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }

		CreateChart(null, GetInterfaceInstance<INodeProvider>().Node.NetObjectID, "AvgRT", chartPlaceHolder);
		Wrapper.SetDrDownMenuParameters("AvgRT", Resource, "N");
	}

	protected override string DefaultTitle
	{
		get 
		{
            return notAvailable;//"Network Latency";
		}
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceAverageResponseTimeChart"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
