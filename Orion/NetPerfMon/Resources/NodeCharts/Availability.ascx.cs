using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class Availability : GraphResource
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }

		CreateChart(null, GetInterfaceInstance<INodeProvider>().Node.NetObjectID, "Availability", chartPlaceHolder);
		Wrapper.SetDrDownMenuParameters("Availability", Resource, "N");

	}

	protected override string DefaultTitle
	{
		get
		{
            return notAvailable;//"Percent Availability";
		}
	}

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditCustomNodeChart.ascx"; }
    }

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceAvailabilityChart"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
