using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class HostMMAvgCPULoad : GraphResource
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }

		CreateChart(null, GetInterfaceInstance<INodeProvider>().Node.NetObjectID, "HostMMAvgCPULoad", chartPlaceHolder);
		Wrapper.SetDrDownMenuParameters("HostMMAvgCPULoad", Resource, "N");
	}

	protected override string DefaultTitle
	{
		get
		{
            return notAvailable;//"Min/Max/Average CPU Load";
		}
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceMinMaxAvgCPULoadChart"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}