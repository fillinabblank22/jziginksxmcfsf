using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class HostMMAvgMemoryUsage : GraphResource
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }

		CreateChart(null, GetInterfaceInstance<INodeProvider>().Node.NetObjectID, "HostMMAvgMemoryUsage", chartPlaceHolder);
		Wrapper.SetDrDownMenuParameters("HostMMAvgMemoryUsage", Resource, "N");
	}

	protected override string DefaultTitle
	{
		get
		{
            return notAvailable;//"Min/Max/Average Memory Usage";
		}
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceMinMaxAvgMemoryUsageChart"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}