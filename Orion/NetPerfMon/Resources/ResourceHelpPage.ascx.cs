using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class ResourceHelpPage : System.Web.UI.UserControl
{
	private String _editURL;

	public String EditURL
	{
		get { return _editURL; }
		set { _editURL = value; }
	}

    protected override void OnLoad(EventArgs e)
    {
        var emptyResource = string.Format(
                   Resources.CoreWebContent.WEBCODE_VB0_330,
                   "<ul><li>", String.Format("<a href=\"{0}\"><font color=\"blue\"><b>", EditURL), "</b></font></a>"
                   , "</li><li>", "</li></ul>", "<span style=\"font-weight:bold;\">", "</span>");
        this.Controls.Add(new SolarWinds.Orion.Web.UI.WebResourceWantsEdit
                              {
                                  HelpfulHtml = emptyResource,
                                  EditUrl = Profile.AllowCustomize ? EditURL : null,
                                  WatermarkImage =
                                      "/Orion/images/ResourceWatermarks/Watermark.UniversalDevicePoller.gif",
                                  WatermarkBackgroundColor = "#f1f1f1",
                                  BackgroundPosition = "right center"
                                  // image has default watermark dimentions
                              });
        base.OnLoad(e);
    }
}
