<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true"
    CodeFile="EditSysLogSummary.aspx.cs" Inherits="Orion_NetPerfMon_Resources_EditSysLogSummary"
    Title="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_265 %>" %>

<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/PeriodControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="TitleEdit" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_262) %></h1>
    <table border="0">
        <tr>
            <orion:TitleEdit runat="server" ID="TitleEditControl" ShowSubTitle="false" />
        </tr>
        <tr>
            <td style="font-weight:bold">
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_263) %>
            </td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList runat="server" ID="Period" />
            </td>
        </tr>
        <tr>
            <td style="font-weight:bold">
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_264) %>
            </td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList runat="server" ID="GroupItems" />
            </td>
        </tr>
        <tr>
            <td>
                <div class="sw-btn-bar"><orion:LocalizableButton ID="btnSubmit" runat="server" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitClick"/></div>
            </td>
        </tr>
    </table>
</asp:Content>