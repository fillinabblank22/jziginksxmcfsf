﻿using System;
using System.Web.UI;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_NetPerfMon_Resources_EditAdvancedSyslogSummary : System.Web.UI.Page
{
	public ResourceInfo Resource
	{
		get
		{
			try
			{
				int resourceID = 0;
				Int32.TryParse(Request.QueryString["ResourceID"], out resourceID);

				ResourceInfo info = ResourceManager.GetResourceByID(resourceID);
				return info;
			}
			catch { }
			return null;
		}
	}
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
			tbTitle.Text = Resource.Title;
			ddlOrderBy.SelectedValue = Resource.Properties["order"];
			ddlPeriod.SelectedValue = Resource.Properties["Period"];
        }
    }

	protected void imbtnSubmit_Click(object sender, EventArgs e)
	{
		if (!Page.IsValid) return;
		ResourceInfo resource = Resource;
        resource.Properties.Clear();
        if (!String.IsNullOrEmpty(tbTitle.Text))
        {
            resource.Title = tbTitle.Text;
            resource.Properties.Add("title", tbTitle.Text);
        }
        else
        {
            var resourceControl = (BaseResourceControl)LoadControl(Resource.File);
            resource.Title = resourceControl.Title;
            resource.Properties.Add("title", resourceControl.Title);
        }
		resource.Properties.Add("Period", ddlPeriod.SelectedValue);
		resource.Properties.Add("order", ddlOrderBy.SelectedValue);

		ResourcesDAL.Update(resource);
        Response.Redirect(BaseResourceControl.GetEditPageReturnUrl(Request.QueryString));
	}
}
