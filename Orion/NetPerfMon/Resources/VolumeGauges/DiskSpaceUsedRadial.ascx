<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DiskSpaceUsedRadial.ascx.cs" Inherits="DiskSpaceUsedRadial" %>
<%@ Register TagPrefix="npm" Namespace="SolarWinds.NPM.Web.Gauge.V1" Assembly="SolarWinds.NPM.Web.Gauge.V1" %>
<orion:Include runat="server" File="GaugeStyle.css" />
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:PlaceHolder ID="Source" runat="server">
        <br />
        <table class="GaugeTable">
	        <tr>
		        <td>
			        <a href="/Orion/NetPerfMon/CustomChart.aspx?chartName=PercentDiskUsage&NetObject=<%= DefaultSanitizer.SanitizeHtml(GetCurrentNetObject.Volume.NetObjectID) %>&Period=Today" target="_blank">
			            <asp:PlaceHolder runat="server" ID="gaugePlaceHolder1" />
			        </a>
	            </td>
	        </tr>
       </table>
       </asp:PlaceHolder>
       <asp:Literal ID="ErrorLiteral" runat="server" />
    </Content>
    <HeaderButtons>
		<npm:ThresholdButton runat="server" />
    </HeaderButtons>
</orion:resourceWrapper>