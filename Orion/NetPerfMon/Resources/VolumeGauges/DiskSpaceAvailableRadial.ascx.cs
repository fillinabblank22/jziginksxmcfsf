using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.NPM.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Gauges)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsGaugesValues.Dials)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsGaugesValues.Meters)]
public partial class DiskSpaceAvailableRadial : GaugeResource<IVolumeProvider>
{
	protected void Page_Load(object sender, EventArgs e)
	{
		if (!isError())
		{
            CreateGauge(GetIntProperty("Scale", 100), GetStringValue("Style", "Minimalist Dark"), 
				Resources.CoreWebContent.WEBCODE_TM0_26, gaugePlaceHolder1, "/Orion/NetPerfMon/CustomChart.aspx?chartName=PercentDiskUsage&NetObject=" + GetCurrentNetObject.Volume.NetObjectID + "&Period=Today",
				GetCurrentNetObject.Volume.NetObjectID, "AvailableSpace", "Radial", " %", 0, 100);
		}
	}

	public override Control GetSourceControl() { return Source; }

	public override ITextControl  GetErrorControl()	{ return (ITextControl)ErrorLiteral; }

	public override string GetErrorText() { return Resources.CoreWebContent.WEBCODE_TM0_32; }

	protected override string DefaultTitle { get { return Resources.CoreWebContent.WEBCODE_TM0_29; } }

    public override string HelpLinkFragment { get { return "OrionPHResourceDiskSpaceGauge"; } }

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }

	public override bool isError()
	{
		if (base.isError()) return true;
		return (GetCurrentNetObject.Volume.PercentUsed < 0);
	}
}
