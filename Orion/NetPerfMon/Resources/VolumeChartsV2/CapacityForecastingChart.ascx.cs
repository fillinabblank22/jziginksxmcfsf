﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_VolumeChartsV2_CapacityForecastingChart : StandardChartResource, IResourceIsInternal
{
    private readonly IForecastingCapacityDAL forecastingCapacityDal = new ForecastingCapacityDAL();
    private IEnumerable<string> elementIdsCache;

    protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);

        if (Visible)
        {
            Visible = GetElementIdsForChart().Any();
        }
    }
    
    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override string NetObjectPrefix
    {
        get { return "V"; }
    }
        
    protected override IEnumerable<string> GetElementIdsForChart()
    {
        if (elementIdsCache != null)
        {
            return elementIdsCache;
        }

        List<string> ids = new List<string>();

        var multipleVolumeProvider = GetInterfaceInstance<IMultiVolumeProvider>();
        if (multipleVolumeProvider != null && multipleVolumeProvider.SelectedNetObjects.Length > 0)
        {
            ids.AddRange(multipleVolumeProvider.SelectedNetObjects);
        }
        else
        {
            var volumeProvider = GetInterfaceInstance<IVolumeProvider>();
            if (volumeProvider != null)
            {
                ids.Add(volumeProvider.Volume.VolumeID.ToString(CultureInfo.InvariantCulture));
            }
        }

        string metricName = Resource.Properties["MetricName"];
        elementIdsCache = forecastingCapacityDal
            .FilterEntitiesVisibleForForecasting(metricName, ids.Select(id => Convert.ToInt32(id)))
            .Select(id => id.ToString());
        return elementIdsCache;
    }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.NodeChartsV2_Title_4; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IVolumeProvider) }; }
    }

    public override IEnumerable<Type> SupportedInterfaces
    {
        get { return new[] { typeof(IVolumeProvider)}; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public bool IsInternal
    {
        get { return true; }
    }

}

