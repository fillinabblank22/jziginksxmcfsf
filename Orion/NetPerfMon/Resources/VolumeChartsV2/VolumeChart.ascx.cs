﻿using System;
using System.Collections.Generic;
using System.Globalization;
using SolarWinds.Logging;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_VolumeChartsV2_VolumeChart : StandardChartResource, IResourceIsInternal
{
    private static readonly Log _log = new Log();

    protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override string NetObjectPrefix
    {
        get { return "V"; }
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        var multipleVolumeProvider = GetInterfaceInstance<IMultiVolumeProvider>();
        if (multipleVolumeProvider != null)
        {
            var netObjects = multipleVolumeProvider.SelectedNetObjects;
            if (netObjects.Length != 0)
                return netObjects;
        }

        var volumeProvider = GetInterfaceInstance<IVolumeProvider>();
        if (volumeProvider != null)
        {
            return new[] {volumeProvider.Volume.VolumeID.ToString(CultureInfo.InvariantCulture)};
        }

        var nodeProvider = GetInterfaceInstance<INodeProvider>();
        if (nodeProvider != null)
        {
            const string swql = "SELECT VolumeID FROM Orion.Volumes WHERE NodeID = @NodeId";
            return GetElementsFromSwql(swql, "NodeId", nodeProvider.Node.NodeID);
        }

        return new string[0];
    }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.VolumeChartsV2_Title_5; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(INodeProvider) }; }
    }

    public override IEnumerable<Type> SupportedInterfaces
    {
        get { return new[] { typeof(IVolumeProvider), typeof(IMultiVolumeProvider) }; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public bool IsInternal
    {
        get { return true; }
    }
}
