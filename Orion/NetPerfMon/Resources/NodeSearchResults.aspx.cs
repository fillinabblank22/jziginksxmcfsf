﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;

using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.InformationService.Contract2;
using SolarWinds.Orion.Web.Federation;
using SolarWinds.Orion.Web.Helpers;

/// <summary>
/// Summary description for NodeSearchResults
/// </summary>
public partial class Orion_NetPerfMon_Resources_NodeSearchResults : Page
{

    protected string nodeProp;
    protected Type nodePropType;
    private Dictionary<int, string> primaryIP = new Dictionary<int, string>();
    private bool searchForIPAddress = false;

    protected bool IsLegacySearch
    {
        get
        {
            string searchMode = Request.QueryString["SearchMode"];
            return "legacy".Equals(searchMode, StringComparison.OrdinalIgnoreCase);
        }
    }

    private int ResourceID
    {
        get
        {
            return Int32.Parse(Request.QueryString["ResourceID"]);
        }
    }

    protected override void OnInit(EventArgs e)
    {
        nodeProp = Request.QueryString["Property"];
        nodePropType = FindPropertyType(nodeProp);
        string searchText = Request.QueryString["SearchText"];

        if (string.IsNullOrEmpty(nodeProp) || string.IsNullOrEmpty(searchText))
        {
            phInvalidURLMessage.Visible = true;
        }
        else
        {
            if (nodeProp.Equals("IPAddress", StringComparison.OrdinalIgnoreCase))
            {
                searchText = IPAddressHelper.ToStringIp(searchText);
                searchForIPAddress = true;
            }

            if (IsLegacySearch)
            {
                SearchNodesOld(searchText);
            }
            else
            {
                SearchNodesNew(searchText);
            }
        }


        base.OnInit(e);
    }

    private void SearchNodesNew(string searchText)
    {
        var originSearchText = searchText;
        searchText = searchText.Replace("*", "%");
        searchText = searchText.Replace("?", "_");

        if (!searchText.Contains("%"))
        {
            searchText = string.Format("%{0}%", searchText);
        }

        if (nodePropType == typeof (Boolean))
        {
            var matchTrue = Boolean.TrueString.IndexOf(originSearchText, StringComparison.OrdinalIgnoreCase) > -1;
            var matchFalse = Boolean.FalseString.IndexOf(originSearchText, StringComparison.OrdinalIgnoreCase) > -1;

            if (matchTrue && matchFalse)
                searchText = "%";
            else if (matchTrue)
                searchText = Boolean.TrueString;
            else if (matchFalse)
                searchText = Boolean.FalseString;
        }


        var propertyTranslation = new Dictionary<string, string>
                                      {
                                          {"IP_Address_Type", "IPAddressType"},
                                          {"Status", "StatusShortDescription"},
                                          {"Description", "NodeDescription"}
                                      };

        string translatedProperty;
        if (propertyTranslation.TryGetValue(nodeProp, out translatedProperty))
        {
            nodeProp = translatedProperty;
        }
        // Status -> Parse text to int
        string selectProperties = string.Format(", {0}", nodeProp);

        if (nodeProp.Equals("StatusShortDescription", StringComparison.OrdinalIgnoreCase) || nodeProp.Equals("SolarWindsServerName", StringComparison.OrdinalIgnoreCase) || nodeProp.Equals("Caption", StringComparison.OrdinalIgnoreCase)) 
        {
            selectProperties = string.Empty;
        }

        using (var swis = InformationServiceProxy.CreateV3())
        {
            string query = GetSwisSearchQuery(searchText, selectProperties);
            DataTable nodes = swis.Query(query, new Dictionary<string, object> {{nodeProp, searchText}});
            if (nodes.Rows.Count > 0)
            {
                nodes.Columns.Add("CompoundStatus", typeof (CompoundStatus));
                foreach (DataRow nodeRow in nodes.Rows)
                {
                    var status = (Status) (int) nodeRow["Status"];
                    var nodeId = (int) nodeRow["NodeID"];
                    nodeRow["CompoundStatus"] = Node.GetCompoundStatus(nodeId, status);

                    // if we search for IPs then add it to IP address dictionary
                    if (searchForIPAddress)
                    {
                        primaryIP[nodeId] = nodeRow["PrimaryIPAddress"].ToString();
                    }
                }

                phMessage.Visible = true;
                rptNodes.DataSource = nodes;
                rptNodes.DataBind();
            }
            else
            {
                phNoNodesMessage.Visible = true;
            }
        }
    }

    private Type FindPropertyType(string propertyName)
    {
        var isCustomProperty = CustomPropertyMgr.IsCustom("Nodes", propertyName);
        var entityName = (isCustomProperty) ? "Orion.NodesCustomProperties" : "Orion.Nodes";

        var propertyType = typeof (string);
        using (var swis = InformationServiceProxy.CreateV3())
        {
            var dt =
                swis.Query("SELECT Type FROM Metadata.Property Where EntityName = @entityName AND Name = @propertyName",
                           new Dictionary<string, object> {{"entityName", entityName}, {"propertyName", propertyName}});
            if (dt != null && dt.Rows.Count > 0)
                propertyType = Type.GetType(dt.Rows[0]["Type"].ToString());
        }
        return propertyType;
    }

    private string GetSwisSearchQuery(string searchText, string selectProperties)
    {
        string comparisonOperator = searchText.Contains("%") || searchText.Contains("_") ? "like" : "=";

        string query;

        if (searchForIPAddress)
        {
            // query for IP address search
			query = string.Format(@"SELECT n.NodeID, n.Caption AS Name, n.Status, si.ShortDescription AS StatusShortDescription,
                                           n.IPAddress AS PrimaryIpAddress, ISNULL(nia.IPAddress, n.IPAddress) AS IPAddress, OrionIdPrefix + ToString(n.NodeID) AS NetObjectID,
                                            OrionSites.Name AS SolarWindsServerName, OrionSites.SiteID AS SolarWindsServerID
                                    FROM Orion.Nodes (nolock=true) n
                                    JOIN Orion.StatusInfo (nolock=true) si ON si.StatusId = n.Status
                                    LEFT JOIN Orion.NodeIPAddresses (nolock=true) nia ON nia.NodeID = n.NodeID AND nia.InstanceSiteID = n.InstanceSiteID
                                    JOIN Orion.Sites (nolock=true) OrionSites ON OrionSites.SiteID = n.InstanceSiteID
                                    WHERE (n.IPAddress {0} @{1} OR nia.IPAddress {0} @{1})
                                    ORDER BY Caption, CASE WHEN n.IPAddress = nia.IPAddress THEN 0 ELSE 1 END, ISNULL(nia.IPAddress, n.IPAddress)", comparisonOperator, nodeProp);
        }
        else
        {
			query = string.Format(@"SELECT n.NodeID, n.Caption AS Name, n.Status, si.ShortDescription AS StatusShortDescription,
                                           n.IPAddress, n.OrionIdPrefix + ToString(n.NodeID) AS NetObjectID {0}, OrionSites.Name AS SolarWindsServerName, OrionSites.SiteID AS SolarWindsServerID
                                    FROM Orion.Nodes (nolock=true) n
                                    JOIN Orion.StatusInfo (nolock=true) si ON si.StatusId = n.Status
                                    LEFT JOIN Orion.NodesCustomProperties (nolock=true) ncp ON ncp.NodeID = n.NodeID AND ncp.InstanceSiteID = n.InstanceSiteID
                                    JOIN Orion.Sites (nolock=true) OrionSites ON OrionSites.SiteID = n.InstanceSiteID
                                    WHERE ({1} {2} @{1})
                                    ORDER BY Caption", selectProperties, nodeProp, comparisonOperator);
        }
        return query;
    }

    private void SearchNodesOld(string searchText)
    {
        searchText = searchText.Replace("*", ".*");
        searchText = searchText.Replace("?", ".?");

        if (nodeProp.Equals("Caption"))
        {
            nodeProp = "Name";
        }

        List<NetObject> objs = new List<NetObject>(NetObjectFactory.GetAllObjects(typeof (Node)));
        var nodeIPs = new List<SolarWinds.Orion.Core.Common.Models.NodeIPAddresses>();
        if (searchForIPAddress)
        {
            nodeIPs = NodeIPAddressesDAL.GetAllIpAddresses();
        }
        var nodeWithIPs = AssignIPsToNode(objs, nodeIPs);

        var nodes = new List<Node>();
        foreach (var ni in nodeWithIPs)
        {
            if (ni.Key is Node)
            {
                Node n = ni.Key as Node;
				var propertyName = nodeProp.Equals("IPAddress", StringComparison.OrdinalIgnoreCase) ? "IP_Address" : nodeProp;
                var propValue = nodeProp.Equals("Status", StringComparison.OrdinalIgnoreCase)
                                    ? ((SolarWinds.Orion.Web.DisplayTypes.Status) n[propertyName]).ToLocalizedString()
                                    : n[propertyName].ToString();
                if ((Regex.IsMatch(propValue, searchText, RegexOptions.IgnoreCase)) ||
                    (searchForIPAddress && SearchInAllIPs(ni, searchText)))
                {
                    nodes.Add(n);
                    primaryIP.Add(n.NodeID, n.IPAddressString);

                    if (searchForIPAddress)
                    {
                        n.SetCustomProperty("IPAddress", n.IPAddressString);
                        foreach (var v in ni.Value)
                        {
                            if (!string.Equals(n.IPAddressString, v.IpAddress, StringComparison.OrdinalIgnoreCase))
                            {
                                Node nNew = new Node(n);
                                nNew.SetCustomProperty("IP_Address", v.IpAddress);
                                nNew.SetCustomProperty("IPAddress", v.IpAddress);
                                nodes.Add(nNew);
                            }
                        }
                    }
                }
            }
        }

        if (0 == nodes.Count)
        {
            phNoNodesMessage.Visible = true;
        }
        else
        {
            phMessage.Visible = true;
            rptNodes.DataSource = nodes;
            rptNodes.DataBind();
        }
    }

    private Dictionary<NetObject, List<SolarWinds.Orion.Core.Common.Models.NodeIPAddresses>> AssignIPsToNode(
        List<NetObject> nodes,
        List<SolarWinds.Orion.Core.Common.Models.NodeIPAddresses> ips)
    {
        int nodesCount = nodes.Count;
        var result = new Dictionary<NetObject, List<SolarWinds.Orion.Core.Common.Models.NodeIPAddresses>>(nodesCount);
		var nodesIPDictionary = new Dictionary<int, List<SolarWinds.Orion.Core.Common.Models.NodeIPAddresses>>(nodesCount);

        foreach (var nodeIpAddress in ips)
        {
            if (!nodesIPDictionary.ContainsKey(nodeIpAddress.NodeID))
            {
				nodesIPDictionary[nodeIpAddress.NodeID] = new List<SolarWinds.Orion.Core.Common.Models.NodeIPAddresses>();
            }
            nodesIPDictionary[nodeIpAddress.NodeID].Add(nodeIpAddress);
        }

        foreach (var n in nodes)
        {
            var node = (Node) n;
            List<SolarWinds.Orion.Core.Common.Models.NodeIPAddresses> ipAddresses;
            if (nodesIPDictionary.TryGetValue(node.NodeID, out ipAddresses))
            {
                result.Add(n, ipAddresses);
            }
            else
            {
                result.Add(n, new List<SolarWinds.Orion.Core.Common.Models.NodeIPAddresses>());
            }
        }

        return result;
    }

    protected bool IsPrimaryIP(object nodeID, object nodeIP)
    {
        if (nodeID == null)
            throw new ArgumentNullException("nodeID");

        if (nodeIP == null)
            throw new ArgumentNullException("nodeIP");

        int nodeIDInt;
        bool result = false;

        if (Int32.TryParse(nodeID.ToString(), out nodeIDInt))
        {
            string ip;
            if (primaryIP.TryGetValue(nodeIDInt, out ip))
            {
                if (String.Equals(nodeIP.ToString(), ip))
                {
                    result = true;
                }
            }
        }

        return result;
    }

    private bool SearchInAllIPs(
        KeyValuePair<NetObject, List<SolarWinds.Orion.Core.Common.Models.NodeIPAddresses>> nodeIPs,
        string searchText)
    {
        if ((nodeIPs.Key == null) || (nodeIPs.Value == null))
            throw new ArgumentNullException("nodeIPs");
        if (searchText == null)
            throw new ArgumentNullException("searchText");

        var result = false;
        foreach (var ni in nodeIPs.Value)
        {
            if ((ni.IpAddress != null) && (Regex.IsMatch(ni.IpAddress, searchText, RegexOptions.IgnoreCase)))
            {
                result = true;
                break;
            }
        }

        return result;
    }

    protected string GetNodePropertyDisplayName(string key)
    {
        switch (key)
        {
            case "Caption":
                return Resources.CoreWebContent.WEBCODE_VB0_123;
            case "IPAddress":
                return Resources.CoreWebContent.WEBDATA_VB0_13;
            case "IP_Address_Type":
                return Resources.CoreWebContent.WEBDATA_AK0_352;
            case "DNS":
                return Resources.CoreWebContent.WEBDATA_VB0_126;
            case "Vendor":
                return Resources.CoreWebContent.WEBCODE_VB0_124;
            case "Description":
                return Resources.CoreWebContent.WEBDATA_VB0_15;
            case "Location":
                return Resources.CoreWebContent.WEBDATA_VB0_128;
            case "Contact":
                return Resources.CoreWebContent.WEBDATA_VB0_129;
            case "Status":
                return Resources.CoreWebContent.WEBDATA_VB0_14;
            case "IOSImage":
                return Resources.CoreWebContent.WEBDATA_VB0_132;
            case "IOSVersion":
                return Resources.CoreWebContent.WEBCODE_VB0_125;
            case "CustomPollerLastStatisticsPoll":
                return Resources.CoreWebContent.WEBCODE_VB0_126;
            default:
                return key;
        }
    }

    protected string GetStatusIconQueryString()
    {
		var queryString = string.Format("id={0}&entity=Orion.Nodes&status={1}", Eval("NodeID"), IsLegacySearch ? (int)(Status)Eval("StatusOriginal") : Eval("Status"));

        return queryString;
    }

    protected string GetNodeProperties()
    {
		if (string.Equals(nodeProp, "Name", StringComparison.OrdinalIgnoreCase) || string.Equals(nodeProp, "Caption", StringComparison.OrdinalIgnoreCase))
        {
            return string.Empty;
        }

        string result;

        // search for status
		if (nodeProp.Equals("Status", StringComparison.OrdinalIgnoreCase) || nodeProp.Equals("StatusShortDescription", StringComparison.OrdinalIgnoreCase))
        {
			result = (IsLegacySearch ? (Status)Eval("ObjectProperties[\"Status\"]") : (Status)(int)Eval("Status")).ToLocalizedString();
        }
        else
        {
            // other properties
			result = (IsLegacySearch ? Eval("ObjectProperties[\"" + nodeProp + "\"]") ?? string.Empty : Eval(nodeProp) ?? string.Empty).ToString();

            // if we search for IP and it is primary
			if (nodeProp.Equals("IPAddress", StringComparison.OrdinalIgnoreCase) && IsPrimaryIP(Eval("NodeID"), Eval("IPAddress")))
            {
                result = string.Format("{0} {1}", result, Resources.CoreWebContent.WEBDATA_TP0_2);
            }
        }

		return WebSecurityHelper.HtmlEncode(result);
    }
}