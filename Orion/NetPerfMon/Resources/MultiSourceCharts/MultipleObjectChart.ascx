﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultipleObjectChart.ascx.cs"
    Inherits="Orion_NetPerfMon_Resources_MultiSourceCharts_MultipleObjectChart" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
<orion:resourceWrapper runat="server" ID="wrapper" ShowEditButton="True">
    <Content>
        <asp:PlaceHolder runat="server" ID="chartPlaceHolder" />
    </Content>
</orion:resourceWrapper>
