﻿using System;
using System.Text;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using System.Collections.Specialized;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Resources_MultiSourceCharts_MultipleObjectChart : GraphResource
{
	private string chartName;
    private string selectedEntities;
    
	private string entityName;
	private bool autoHide = false;
	private string topXX = String.Empty;
	private bool filterEntities;
    private bool manualSelection;

	protected void Page_Load(object sender, EventArgs e)
	{
        if (CheckProperties(Resource.Properties))
        {

            if (!manualSelection)
            {
                selectedEntities = OrionChartingHelper.GetEntitiesFilteredByNetObject(entityName, Request.QueryString["NetObject"], null, topXX);
            }

			Dictionary<string, string> parameters = new Dictionary<string, string> { 
				{"SelectedEntities", selectedEntities},
				{"ChartEntity", Resource.Properties["EntityName"]},
				{"ShowSum", Resource.Properties["ShowSum"]},
				{"DataColumnName", Resource.Properties["DataColumnName"]},
				{"FilterEntities", Resource.Properties["FilterEntities"]},
				{"TopXX", Resource.Properties["TopXX"]},
                {"ResourceID", Resource.ID.ToString()}
			};

            if (autoHide && String.IsNullOrEmpty(selectedEntities))
            {
                Visible = false;                
            }
            else
            {
                CreateChart(null, Request.QueryString["NetObject"], chartName, parameters, chartPlaceHolder);
                wrapper.SetUpDrDownMenu(Resource.Properties["ChartName"], Resource, string.Empty, parameters);
            }
        }
        else
        {
            var emptyResource = new StringBuilder();

            emptyResource.AppendFormat(
                Resources.CoreWebContent.WEBCODE_VB0_334,
                "<ul><li>",
                "</li><li>",
                String.Format("<a href=\"{0}\"><font color=\"blue\"><b>", EditURL),
                "</b></font></a>",
                "</li></ul>",
                "<span style=\"font-weight:bold;\">",
                "</span>",
                "</li></ul><br/>"
            );

            chartPlaceHolder.Controls.Add(
                new SolarWinds.Orion.Web.UI.WebResourceWantsEdit
                {
                    HelpfulHtml = emptyResource.ToString(),
                    EditUrl = Profile.AllowCustomize ? EditURL : null,
                    WatermarkImage = "/Orion/images/ResourceWatermarks/Watermark.MultipleLineChart.gif",
                    WatermarkBackgroundColor = "#ffffff",
                    BackgroundPosition = "right top"
                    // image has default watermark dimentions
                }
            );
        }
	}

	private bool LoadProperty<T>(StringDictionary dict, string propertyName, out T property)
	   where T : IConvertible
	{
		property = default(T);

		if (dict != null)
		{
			if (String.IsNullOrEmpty(dict[propertyName]))
				return false;

			try
			{
				property = (T)Convert.ChangeType(dict[propertyName], typeof(T));
			}
			catch
			{
				return false;
			}

			return true;
		}

		return false;
	}


	private bool CheckProperties(StringDictionary props)
	{
		if (!LoadProperty<string>(props, "EntityName", out entityName))
			return false;

		if (!LoadProperty<string>(props, "ChartName", out chartName))
			return false;

		if (!LoadProperty<bool>(props, "ManualSelect", out manualSelection)) {
			// FB78334 - in 10.1.2 this resource doesn't support automatic netobject selection, only manual.
			// So, after upgrade to 10.2 "ManualSelect" resource property isn't exists.
			if (!String.IsNullOrEmpty(props["ManualSelect"]))
				return false;
			manualSelection = true;
		}

		if (manualSelection)
        {
            if (!LoadProperty<string>(props, "SelectedEntities", out selectedEntities))
                return false;

            if (!LoadProperty<bool>(props, "FilterEntities", out filterEntities))
                return false;
        }
        else
        {
            if (String.IsNullOrEmpty(Request.QueryString["NetObject"]))
                return false;
        }

		LoadProperty<string>(props, "TopXX", out topXX);

		if (String.IsNullOrEmpty(props["ShowSum"]))
			return false;

		string hide;
        if (LoadProperty<string>(props, "AutoHide", out hide))
        {
            autoHide = (hide != "0");
        }

		return true;
	}

	protected override string DefaultTitle
	{
		get { return Resources.CoreWebContent.WEBCODE_AK0_87; }
	}

	public override string EditControlLocation
	{
		get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditMultipleObjectChart.ascx"; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceMultipleObjectChart"; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
