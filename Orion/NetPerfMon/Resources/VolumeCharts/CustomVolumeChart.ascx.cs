using System;
using System.Text;
using System.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Charting;

public partial class CustomVolumeChart : GraphResource
{
	private string _chartName;

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

        if (!string.IsNullOrEmpty(Resource.Properties["ChartName"]))
		{
			_chartName = Resource.Properties["ChartName"];
			CreateChart(null, GetInterfaceInstance<IVolumeProvider>().Volume.NetObjectID, _chartName, chartPlaceHolder);
			Wrapper.SetDrDownMenuParameters(_chartName, Resource);
		}
		else
		{
			_chartName = string.Empty;
		    var emptyResource =
		        string.Format(
		            Resources.CoreWebContent.WEBCODE_VB0_331,
                    "<ul><li>",
		            String.Format("<a href=\"{0}\"><font color=\"blue\"><b>", EditURL), "</b></font></a>"
                    , "</li><li>", "</li></ul>", 
                    "<span style=\"font-weight:bold;\">", "</span>");

            chartPlaceHolder.Controls.Add(new SolarWinds.Orion.Web.UI.WebResourceWantsEdit
            {
                HelpfulHtml = emptyResource,
                EditUrl = Profile.AllowCustomize ? EditURL : null,
                WatermarkImage = "/Orion/images/ResourceWatermarks/Watermark.BarChartWithTrend.png",
                BackgroundPosition = "right bottom",
                WatermarkBackgroundColor = "#ffffff"
                // image has default watermark dimentions
            });
        }
    }

	protected override string DefaultTitle
	{
		get { return Resources.CoreWebContent.WEBCODE_TM0_13; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceCustomVolumeDiskChart"; }
	}

	public override string EditControlLocation
	{
		get
		{
			return "/Orion/NetPerfMon/Controls/EditResourceControls/EditCustomVolumeChart.ascx";
		}
	}

	private string RemoveQueryStringParameter(string paramName)
	{
		string returnURL = Page.Request.Url.PathAndQuery;
		if (!returnURL.Contains(paramName))
			return returnURL;

		int paramStart = returnURL.IndexOf(paramName) - 1;
		int paramEnd = returnURL.IndexOf("&", paramStart);

		// if this parameter is last in query string
		if (paramEnd < 0)
		{
			return returnURL.Remove(paramStart);
		}
		else
		{
			return returnURL.Remove(paramStart, paramEnd - paramStart);
		}
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(IVolumeProvider) }; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
