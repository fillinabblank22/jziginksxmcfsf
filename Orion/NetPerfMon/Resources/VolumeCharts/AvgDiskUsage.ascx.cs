using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class AvgDiskUsage : GraphResource
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		CreateChart(null, GetInterfaceInstance<IVolumeProvider>().Volume.NetObjectID, "AvgDiskUsage", chartPlaceHolder);
		Wrapper.SetDrDownMenuParameters("AvgDiskUsage", Resource);
	}

	protected override string DefaultTitle
	{
		get
		{
            return notAvailable;// "Min/Max/Average Response Time";
		}
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceAverageDiskSpaceUsedChart"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(IVolumeProvider) }; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
