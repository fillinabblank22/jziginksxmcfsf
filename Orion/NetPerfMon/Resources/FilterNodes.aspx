<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" Title="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_201 %>" %>
<%@ Import Namespace="SolarWinds.Orion.Web" %>
<%@ Import Namespace="SolarWinds.Orion.NPM.Web" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<%@ Register TagPrefix="orion" TagName="FilterNodesSql" Src="~/Orion/Controls/FilterNodesSql.ascx" %>

<script runat="server">
    private ResourceInfo _resource;
    protected ResourceInfo Resource
    {
        get { return _resource; }
    }

    protected override void OnInit(EventArgs e)
    {
        if(!string.IsNullOrEmpty(Request.QueryString["ResourceID"]))
        {
            int resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
            this._resource = ResourceManager.GetResourceByID(resourceID);
            Page.Title = string.Format(Resources.CoreWebContent.WEBCODE_AK0_88, this.Resource.Title);
        }

        if (this.Resource.Properties.ContainsKey("FilterText"))
            this.txtFilterValue.Text = this.Resource.Properties["FilterText"];

		this.SQLFilter.FilterTextBox.Text = this.Resource.Properties["Filter"];
		
        this.resourceTitleEditor.ResourceTitle = this.Resource.Title;
        this.resourceTitleEditor.ResourceSubTitle = this.Resource.SubTitle;
        
        InitNodeProperties();
        
        base.OnInit(e);
    }

    private void InitNodeProperties()
    {
        ListItemCollection properties = new ListItemCollection();
        properties.Add(new ListItem(Resources.CoreWebContent.WEBCODE_VB0_123, "Caption"));
        properties.Add(new ListItem(Server.HtmlDecode(Resources.CoreWebContent.WEBDATA_TP0_1), "IP_Address"));
        properties.Add(new ListItem(Resources.CoreWebContent.WEBDATA_VB0_128, "Location"));
        properties.Add(new ListItem(Resources.CoreWebContent.WEBDATA_VB0_14, "Status"));
        properties.Add(new ListItem(Resources.CoreWebContent.WEBDATA_VB0_40, "MachineType"));

        foreach (string prop in Node.GetCustomPropertyNames(true))
        {
            properties.Add(prop);
        }
        
        lstNodeFields.DataSource = properties;
        lstNodeFields.DataBind();
        if (this.Resource.Properties.ContainsKey("NodeProperty"))
            lstNodeFields.SelectedValue = this.Resource.Properties["NodeProperty"];
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (!(resourceTitleEditor.ResourceTitle.Equals(Resource.Title) && resourceTitleEditor.ResourceSubTitle.Equals(Resource.SubTitle)))
        {
            if (!String.IsNullOrEmpty(resourceTitleEditor.ResourceTitle))
            {
                Resource.Title = resourceTitleEditor.ResourceTitle;
            }
            else
            {
                var resourceControl = (BaseResourceControl)LoadControl(Resource.File);
                Resource.Title = resourceControl.Title;
            }
            Resource.SubTitle = resourceTitleEditor.ResourceSubTitle;
            SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
        }
        
        this.Resource.Properties["NodeProperty"] = this.lstNodeFields.SelectedValue;
        this.Resource.Properties["FilterText"] = this.txtFilterValue.Text;
        this.Resource.Properties["Filter"] = SqlFilterChecker.CleanFilter(this.SQLFilter.FilterTextBox.Text);
        this.Response.Redirect(BaseResourceControl.GetEditPageReturnUrl(Request.QueryString, Resource.View.ViewID.ToString()));
    }
</script>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= DefaultSanitizer.SanitizeHtml(CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Page.Title, Request["netobject"]))) %></h1>
    
    <orion:EditResourceTitle runat="server" ID="resourceTitleEditor" />
    
    <p>
        <%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBDATA_AK0_269, Resource.Title)) %>
    </p>
    
    <p>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_266) %><br />
        <asp:TextBox runat="server" id="txtFilterValue" Rows="1" Columns="30" /><br />
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_267) %>
    </p>
    
    <p>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_268) %> <br />
        <asp:ListBox runat="server" ID="lstNodeFields" SelectionMode="single" Rows="1" DataTextField="Text" DataValueField="Value" />
    </p>

   <orion:FilterNodesSql runat="server" ID="SQLFilter" />
   <div class="sw-btn-bar"><orion:LocalizableButton ID="btnSubmit" runat="server" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitClick"/></div>
</asp:Content>

