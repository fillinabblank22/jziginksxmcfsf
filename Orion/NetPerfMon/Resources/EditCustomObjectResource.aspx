<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master"
    AutoEventWireup="true" CodeFile="EditCustomObjectResource.aspx.cs" Inherits="Orion_NetPerfMon_Resources_EditCustomObjectResource" EnableViewState="true" %>

<%@ Register TagPrefix="orion" TagName="ListCharts" Src="~/Orion/NetPerfMon/Controls/ListCharts.ascx" %>
<%@ Register TagPrefix="orion" TagName="ListEntities" Src="~/Orion/NetPerfMon/Controls/ListEntities.ascx" %>
<%@ Register TagPrefix="orion" TagName="TopXX" Src="~/Orion/NetPerfMon/Controls/TopXXControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditSampleSize" Src="~/Orion/Controls/SampleSizeControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/PeriodControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditAutoHide" Src="~/Orion/Controls/AutoHideControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="DateTimePicker" Src="~/Orion/Controls/DateTimePicker.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPlaceHolder" runat="Server">
    <orion:Include runat="server" File="CustomNodeResource.css" />
    <orion:Include runat="server" File="EditGauge.css" />
    <orion:Include runat="server" File="NetPerfMon/js/CustomNodeResource.js" />
    <orion:Include runat="server" File="ig_webGauge.js" />
    <orion:Include runat="server" File="ig_shared.js" />
    <orion:Include runat="server" File="GaugeScript.js" />
    <orion:Include runat="server" File="NodeMNG.css"  />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h1><asp:Literal runat="server" ID="PageHeaderTitle" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_247 %>" /></h1>
       <%-- FB24636--%>
       <%--Add redundant validator to fix validating in dynamically added control (gauges)--%>
        <asp:TextBox style="display:none;" ID="FB24636" runat="server"></asp:TextBox>
        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="FB24636" ErrorMessage="" MinimumValue="0" MaximumValue="1"
        Type="String">*</asp:RangeValidator>
        <%--FB24636--%>

    <asp:UpdatePanel ID="TitleUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <p runat="server" id="title2">
                <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_VB0_102) %></b><br />
                <asp:TextBox runat="server" ID="txtTitle" Rows="1" Columns="30" OnTextChanged="txtTitle_TextChanged"/>
            </p>
            <p runat="server" id="subtitle">
                <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_163) %></b><br />
                <asp:TextBox runat="server" ID="txtSubTitle" Rows="1" Columns="30" />
                <br />
            </p>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:Panel ID="EntitiesObjectPanel" runat="server">
        <table cellpadding="2" width="100%">
            <thead>
                <tr>
                    <th style="width: 10%"></th>
                    <th style="width: 10%"></th>
                    <th style="width: 50%"></th>
                </tr>
            </thead>
            <tr>
                <td class="leftcolumn"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_333) %></td>
                <td class="formRightInput" colspan="2"><asp:DropDownList runat="server" ID="ddl_ObjectTypes" OnSelectedIndexChanged="ddl_ObjectTypes_SelectedIndexChanged" AutoPostBack="true"> </asp:DropDownList> </td>
            </tr>
            <tr>
                <td class="leftcolumn"><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_248) %></b></td>
                <td class="formRightInput" colspan="2"><orion:LocalizableButton ID="bt_SelectObjects" runat="server" DisplayType="Small" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_254 %>" OnClick="SelectObjects_Click" /></td>
            </tr>
            <tr>
                <td class="leftcolumn" colspan="2"><orion:ListEntities runat="server" ID="ListEntities" OnSelectObjectChanged="SelectObjectChanged"/></td>
                <td></td>
            </tr>            
        </table>
        <br />
    </asp:Panel> 

    <asp:UpdateProgress runat="server" AssociatedUpdatePanelID="EditUpdatePanel">
        <ProgressTemplate><table><tr><td style="width: 15px;"><img src="/Orion/images/AJAX-Loader.gif"/></td><td>&nbsp; <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_249) %></td></tr></table></ProgressTemplate>
    </asp:UpdateProgress>

    <asp:Panel ID="MultiObjectChartPanel" runat="server" Visible="false">
        <table cellpadding="2" width="100%">
            <thead>
                <tr>
                    <th style="width: 10%"></th>
                    <th style="width: 10%"></th>
                    <th style="width: 50%"></th>
                </tr>
            </thead>
            <tr>
                <td class="leftcolumn">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_293) %>
                </td>
                <td colspan="2">
                    <orion:ListCharts runat="server" ID="lc_ListCharts" ShowNewCharts="true" EnableAutoPost="true" OnOnChartChanged="resources_SelectedIndexChanged" />
                </td>
            </tr>
            
            <tr runat="server" id="AutoSelectObjects">
                <td>
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_248) %> 
                </td>
                <td valign="top">
                    <asp:CheckBox ID="AutoSelect" runat="server" Checked="false" GroupName="SelectObjects" />
                    <%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBDATA_AK0_341, Entities.ToLower())) %> 
                </td>
                <td></td>
            </tr>
            <tr />
            <tr runat="server" id="OldChartLimitSeries">
                <td class="leftcolumn">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_334) %>
                </td>
                <td class="formRightInput">
                    <orion:TopXX runat="server" ID="TopXX" />
                </td>
                <td></td>
            </tr>
            <tr runat="server" id="OldChartSumRow" >
                <td class="leftcolumn">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_335) %>
                </td>
                <td class="formRightInput">
                    <asp:DropDownList ID="ShowSum" runat="server">
                        <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_338 %>" Value="NoSum"></asp:ListItem>
                        <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_339 %>" Value="OnlySum"></asp:ListItem>
                        <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_340 %>" Value="AlsoSum"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <orion:EditPeriod runat="server" ID="Period" PeriodLabel="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_337 %>" />
            <tr>
                <td colspan="3">
                    <orion:EditSampleSize runat="server" ID="SampleSize" HelpfulText="" />
                </td>
            </tr>
            <orion:EditAutoHide runat="server" ID="AutoHide" Description=" " />
        </table>

    </asp:Panel>
        <asp:UpdatePanel ID="EditUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel runat="server" id="SelResourcePanel" >
                <table cellpadding="2" width="100%">
                    <thead>
                        <tr>
                            <th style="width: 10%"></th>
                            <th style="width: 10%"></th>
                            <th style="width: 50%"></th>
                        </tr>
                    </thead>
                    <tr> 
                        <td class="leftcolumn"> <asp:Label runat="server" ID="resourcesLabel" Font-Bold="true" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_250 %>"></asp:Label> </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td class="formRightInput" style="width:auto;"> <asp:DropDownList runat="server" ID="resources" AutoPostBack="true" OnSelectedIndexChanged="resources_SelectedIndexChanged" /> </td>
                        <td colspan="2">
                            <span runat="server" id="messagePlaceholder" visible="false" class="sw-pg-hint-yellow">
		                         <span class="sw-pg-hint-body-warning" style="font-size: small;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_247) %><a target="_blank" rel="noopener noreferrer" href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("orioncoreaddmoderncharts")) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_OJ0_8) %></a></span>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="leftcolumn"> <orion:LocalizableButton ID="embeddedEdit" runat="server" DisplayType="Secondary" LocalizedText="CustomText" 
                            Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_251 %>" OnClick="embeddedEdit_Click" Visible="false" /> </td>
                        <td class="formRightInput"> <asp:HiddenField runat="server" ID="hfEmbeddedPath" /> </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="leftcolumn"><asp:PlaceHolder runat="server" ID="phResourceEditControl" EnableViewState="false" ></asp:PlaceHolder></td>
                        <td></td>
                        <td></td>
                    </tr>
                 </table>
            </asp:Panel>            
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="sw-btn-bar">
        <orion:LocalizableButton ID="btnSubmit" runat="server" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitClick"/>
        <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" DisplayType="Secondary" OnClick="btnCancel_Click" CausesValidation="False" />
    </div>
    <input type="hidden" runat="server" id="selNetObjID" />
</asp:Content>