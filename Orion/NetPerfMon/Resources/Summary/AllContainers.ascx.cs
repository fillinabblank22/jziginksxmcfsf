using System;

using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsFederationAware, "true")]
public partial class Orion_NetPerfMon_Resources_Summary_AllContainers : BaseResourceControl
{
    private const string GroupingResourcePropertiesKey = "Grouping";

    #region Resource Properties

    protected string RememberExpandedGroups
    {
        get { return this.Resource.Properties["RememberCollapseState"] ?? "true"; }
    }

    protected int BatchSize
    {
        get { return ContainerHelper.GetAllGroupsBatchSize(); }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected override string DefaultTitle
    {
        get
        {
            return Resources.CoreWebContent.WEBDATA_AK0_278;
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceAllContainers";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditAllContainers.ascx"; }
    }

    public override string SubTitle
    {
        get
        {
            if (!String.IsNullOrEmpty(base.SubTitle))
            {
                return base.SubTitle;
            }

            if (String.IsNullOrEmpty(Resource.Properties[GroupingResourcePropertiesKey]))
            {
                return Resources.CoreWebContent.WEBCODE_AK0_100;
            }
            else
            {
                return String.Format(Resources.CoreWebContent.WEBCODE_AK0_101, GetLocalizedProperty("GroupBy", Resource.Properties[GroupingResourcePropertiesKey]));
            }
        }
    }

    public bool EmptyContentPlaceholderEnabled
    {
        get { return !SwisFederationInfo.IsFederationEnabled; }
    }
    #endregion

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        Wrapper.ManageButtonText = Resources.CoreWebContent.ResourcesAll_ManageGroups;
        Wrapper.ManageButtonTarget = "/Orion/Admin/Containers/Default.aspx";
        Wrapper.ShowManageButton = !SwisFederationInfo.IsFederationEnabled && (Profile.AllowNodeManagement || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer);
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
}