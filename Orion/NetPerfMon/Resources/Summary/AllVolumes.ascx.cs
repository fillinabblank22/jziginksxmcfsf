﻿using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Logging;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_Summary_AllVolumes : SolarWinds.Orion.Web.UI.BaseResourceControl
{
    private static readonly Log log = new Log();

    private string currentNodeId = String.Empty;

    #region properties

    // overriden DefaultTitle
    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_SO0_11; }
    }

    // overriden HelpLink
    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceAllVolumes";
        }
    }

	public override string EditControlLocation
	{
		get { return "/Orion/NetPerfMon/Controls/EditResourceControls/FilterEdit.ascx"; }
	}

    // overriden Edit URL
    public override string EditURL
    {
        get
        {
            return base.EditURL + "&HideInterfaceFilter=True&ShowVolumeFilter=True";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    public int WarningLevel { get; set; }
    public int ErrorLevel { get; set; }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        var filter = this.Resource.Properties["Filter"];

        // if is netObject a Node set filter for a node
        INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();

        if (nodeProvider != null)
            if (String.IsNullOrEmpty(filter))
                filter = String.Format("Nodes.NodeId={0}", nodeProvider.Node.NodeID);
            else
                filter = String.Format("{0} AND Nodes.NodeId={1}", filter, nodeProvider.Node.NodeID);

        DataTable table = null;

        try
        {
            table = WebDAL.GetProblemVolumesSWQL(filter, this.Resource.View.LimitationID);
        }
        catch (Exception ex)
        {
            if (HasSWQLFilterError(ex, filter))
            {
                this.SQLErrorPanel.Visible = true;
                return;
            }
            throw;
        }

        if (table == null || table.Rows.Count == 0)
        {
            return;
        }
        
        table.Columns.Add("NodeURL", typeof(string));
        table.Columns.Add("VolumeURL", typeof(string));
        table.Columns.Add("VolumeLoadURL", typeof(string));
        
        // preprocessing
        foreach(DataRow r in table.Rows)
        {
            // node details url
            r["NodeURL"] = String.Format("/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}", r["NodeID"]);
            
            // volume details
            r["VolumeURL"] = String.Format("/Orion/NetPerfMon/VolumeDetails.aspx?NetObject=V:{0}", r["VolumeID"]);

            // volume URL
            r["VolumeLoadURL"] = String.Format("/Orion/NetPerfMon/CustomChart.aspx?ChartName=PercentDiskUsage&NetObject=V:{0}&Period=Today", r["VolumeID"]);
        }

        // bind data to repeater
        this.NodesRepeater.DataSource = table;
        this.NodesRepeater.DataBind();
    }

    // item bound event
    protected void ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HtmlTableRow groupHeaderRow = e.Item.FindControl("groupHeaderRow") as HtmlTableRow;
            HtmlTableRow itemRow = e.Item.FindControl("itemRow") as HtmlTableRow;

            DataRowView row = e.Item.DataItem as DataRowView;
            bool newNode = false;
            if (row["NodeID"].ToString() != this.currentNodeId)
            {
                this.currentNodeId = row["NodeID"].ToString();
                newNode = true;
            }
			
            groupHeaderRow.Visible = newNode;
        }
    }

}

