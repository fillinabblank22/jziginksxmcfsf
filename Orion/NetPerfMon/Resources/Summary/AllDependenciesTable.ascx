<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllDependenciesTable.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Summary_AllDependenciesTable" %>
<orion:Include runat="server" File="NetPerfMon/resources/Summary/AllDependenciesTable.js" />

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        
        <asp:ScriptManagerProxy runat="server" >
            <Services>
                <asp:ServiceReference path="/Orion/Services/AsyncResources.asmx" />
            </Services>
        </asp:ScriptManagerProxy>

        <div id="ErrorMsg-<%= Resource.ID %>"></div>
        <div id="DependencyLoadWait-<%= Resource.ID %>">
			     <center type="loading_container">
				      <img alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_213) %>" src="/Orion/images/AJAX-Loader.gif" /> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_213) %>
			     </center>
		    </div>
		    
        <table id="Grid-<%= Resource.ID %>" cellpadding="2" cellspacing="0" width="100%">
            <tr class="ReportHeader"></tr>
        </table>

        <div id="pager-<%= Resource.ID %>" class="ReportFooter ResourcePagerControl hidden" style="display: block;">
            <img id="pager-btnFirst-<%= Resource.ID %>" style="vertical-align:middle" src="/Orion/images/Arrows/button_white_paging_first_disabled.gif" >
            |
            <img id="pager-btnPrevious-<%= Resource.ID %>" style="vertical-align:middle" src="/Orion/images/Arrows/button_white_paging_previous_disabled.gif">
            |
            <span id="pager-pagesStatus-<%= Resource.ID %>"></span>
            |
            <img id="pager-btnNext-<%= Resource.ID %>" style="vertical-align:middle" src="/Orion/images/Arrows/button_white_paging_next_disabled.gif">
            |
            <img id="pager-btnLast-<%= Resource.ID %>" style="vertical-align:middle" src="/Orion/images/Arrows/button_white_paging_last_disabled.gif">
            |
            <span id="pager-itemsPerPageText-<%= Resource.ID %>"></span>
            <input id="pager-tbItemsPerPage-<%= Resource.ID %>" type="text" class="pageSize SmallInput" value="15"/>
            <div id="pager-itemsStatusText-<%= Resource.ID %>" class="ResourcePagerInfo"></div>
        </div>

        <textarea id="parentFilter-<%= Resource.ID %>" style="display:none;"><%= HttpUtility.HtmlEncode(Resource.Properties["ParentFilter"]) %></textarea>
        <textarea id="childFilter-<%= Resource.ID %>" style="display:none;"><%= HttpUtility.HtmlEncode(Resource.Properties["ChildFilter"]) %></textarea>
        <textarea id="dependencyFilter-<%= Resource.ID %>" style="display:none;"><%= HttpUtility.HtmlEncode(Resource.Properties["DependencyFilter"]) %></textarea>
        <input id="autoHide-<%= Resource.ID %>" type="checkbox" <%= AutoHide ? "checked" : string.Empty %> style="display:none"/>

        <textarea id="hfDependenciesData-<%= Resource.ID %>" style="display:none;"></textarea>
        <textarea id="hfCurrentPage-<%= Resource.ID %>" style="display:none;">1</textarea>

        <script type="text/javascript">
            $(function () {
                var refresh = function() {
                    SW.Core.Resources.AllDependenciesTable.createTable(<%= Resource.ID %>);
                };
                SW.Core.View.AddOnRefresh(refresh);
                refresh();
            });	    
        </script>
    </Content>
</orion:resourceWrapper>
