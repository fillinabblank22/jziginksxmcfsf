<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AvailabilityOfEachNode.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Summary_AvailabilityOfEachNode" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= DefaultSanitizer.SanitizeHtml(this.CustomSqlErrorMessage) %>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="availabilityTable">
            <HeaderTemplate>
		        <table border="0" cellpadding="2" cellspacing="0" width="100%">
		            <tr>
		                <td class="ReportHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_171) %></td>
		                <td class="ReportHeader" align="center"><%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBDATA_TM0_89, this.PeriodName.ToUpper())) %></td>
		            </tr>
            </HeaderTemplate>
            <ItemTemplate>
				    <tr>
				        <td class="Property">
				            <a href="/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:<%# DefaultSanitizer.SanitizeHtml(Eval("NodeID")) %>"><%# DefaultSanitizer.SanitizeHtml(Eval("NodeCaption")) %></a>&nbsp;
				        </td>
				        <td class="Property" align="center">
				            <a href='#' onclick="window.open(String.format('/ui/perfstack/?context=0_Orion.Nodes_{0}&withRelationships=true&charts=0_Orion.Nodes_{0}-Orion.ResponseTime.Availability&{1}', <%# DefaultSanitizer.SanitizeHtml(Eval("NodeID").ToString()) %>, SW.Core.DateHelper.getPerfStackTimeFrame('<%# DefaultSanitizer.SanitizeHtml(this.PeriodName) %>')));return false;" target="_blank"><%# DefaultSanitizer.SanitizeHtml(String.Format("{0:#0.000} %", Eval("Availability"))) %></a>&nbsp;
				        </td>
				    </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>