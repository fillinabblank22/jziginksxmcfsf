<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllContainers.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Summary_AllContainers" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Swis" %>

<asp:ScriptManagerProxy id="ContainersTreeScriptManager" runat="server">
	<Services>
		<asp:ServiceReference path="/Orion/Services/ContainersTree.asmx" />
	</Services>
</asp:ScriptManagerProxy>

<orion:Include runat="server" File="NetPerfMon/Resources/Summary/AllContainers.js" />

<style type="text/css">
.CORE_GettingStartedBox
{
    padding: 1em;
    border: solid 15px #204a63;
}

.CORE_GettingStartedBox div
{
    clear: both;
}

.CORE_GettingStartedBox div.CORE_First
{
    padding-top: 0em;
}

.CORE_GettingStartedBox .CORE_Content {
    margin: 15px 0;
    padding: 15px;
    background: #F9F9F9;
}

.CORE_GettingStartedBox div h3
{
    font-weight: normal;
    font-size: 11pt;
    margin: 0 0 0 40px;
    text-transform: uppercase;
}

.CORE_GettingStartedBox div p
{
    margin: 0px 0px 0px 25px;
    line-height: 1.8em;
}

.CORE_GettingStartedBox div img
{
    float: left;
    margin-top: 2px;
}

.CORE_Buttons
{
    text-align: right;
}
</style>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
	    <script type="text/javascript">
	        $(function() {
	            var refresh = function() {
                    var tree = new ORIONContainers.ContainersTree(<%=SwisFederationInfo.IsFederationEnabled.ToString().ToLower()%>, <%=this.EmptyContentPlaceholderEnabled.ToString().ToLower()%>);
	                tree.Load('<%=this.tree.ClientID%>', '<%= this.Resource.ID %>', 'containersTreeNode', <%=this.RememberExpandedGroups.ToLower() %>, <%=this.BatchSize %>);
	            };
	            SW.Core.View.AddOnRefresh(refresh);
	            refresh();
	        });	    
	    </script>
	    <input type="hidden" ID="treeState" value='<%= DefaultSanitizer.SanitizeHtml(WebUserSettingsDAL.Get("GroupTreeState." + Resource.ID)) %>'/>
		<div class="containersTree" ID="tree" runat="server">
			<center type="loading_container">
				<img alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_213) %>" src="/Orion/images/AJAX-Loader.gif" /> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_213) %>
			</center>
		</div>
		
		<div id="<%= this.Resource.ID %>-containersTreeNode" style="display: none;">
		    <a href="javascript:void(0);" class="containersTreeNode-expandLink">
		        <img src="/Orion/images/Button.Expand.gif" class="containersTreeNode-expandButton" />
		        <img class="containersTreeNode-statusIcon" />
		    </a>
		    <span class="containersTreeNode-itemName"></span>
		    <span class="containersTreeNode-itemDescription"></span>
		    <div class="containersTreeNode-children" style="display: none; padding-left: 30px;"></div>
		</div>

        <!-- Following show me when no nodes exists in DB -->
        <div id="ORIONCoreTreeGroup-<%= this.Resource.ID %>-NoDataContent" style="display: none">
		   <div class="CORE_GettingStartedBox">
              <div class="CORE_First ui-helper-clearfix">
                 <img src="/Orion/images/getting_started_36x36.gif" alt="Getting started" />
                 <h3><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_1) %></b> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_6) %></h3>
              </div>
               
              <div class="CORE_Content">
                  <div>
                      <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_7) %>
                  </div>
                  <br />
                  <div>
                  <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_8) %>
                  </div>
              </div>

              <div class="CORE_Buttons">
                  <orion:LocalizableButton runat="server" ID="discoverNetworkButton" PostBackUrl="~/Orion/Admin/Containers/Add/Default.aspx" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PS0_9 %>" />
              </div>
           </div>
        </div>
	</Content>	
</orion:resourceWrapper>
