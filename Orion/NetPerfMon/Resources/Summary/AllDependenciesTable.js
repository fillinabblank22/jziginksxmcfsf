﻿SW.Core.Resources = SW.Core.Resources || {};
SW.Core.Resources.AllDependenciesTable = SW.Core.Resources.AllDependenciesTable || {};

(function(adt) {
    
    var generateColumnInfo = function(columns) {
        var columnInfo = [];
        var indexLookup = {};

        // Map column text to its index.
        $.each(columns, function(index, name) {
            indexLookup[name] = index;
            columnInfo[index] = { };
        });
        
        // Lookup each column's formatting information.
        $.each(columns, function(index, name) {
            columnInfo[index].header = ((name != null) && (name.substring(0,1) !== '_')) ? name : null;

            var otherColumnIndex = getReferencedColumnIndex(name, '_IconFor_',
                                                            indexLookup);
            if (otherColumnIndex != null) {
                columnInfo[otherColumnIndex].iconColumn = index;
            }
            
            otherColumnIndex = getReferencedColumnIndex(name, '_LinkFor_', indexLookup);
            if (otherColumnIndex != null) {
                columnInfo[otherColumnIndex].linkColumn = index;
            }
        });
        
        return columnInfo;
    };

    var getReferencedColumnIndex = function(name, prefix, columnIndexLookup) {
        var re = new RegExp('^' + prefix + '(.+)$', 'i');
        var match = name.match(re);
        return match ? columnIndexLookup[match[1]] : null;
    };

    var renderCell = function(cellValue, rowArray, columnInfo) {
        var cell = $('<td/>');
        if (columnInfo.iconColumn) {
            $('<img/>').attr({src: rowArray[columnInfo.iconColumn], style: 'float: left; margin-right: 7px; margin-left: 2px'}).appendTo(cell);
        }
        
        if (columnInfo.linkColumn) {
            $('<a/>').attr('href', rowArray[columnInfo.linkColumn])
                     .text(cellValue)
                     .appendTo(cell);
        } else {
            $('<span/>').html(cellValue).appendTo(cell);
        }

        return cell;        
    };

    var updatePager = function (pageIndex, pageSize, totalRecords, totalPages) {

        var firstImgRoot = '/Orion/images/Arrows/button_white_paging_first';
        var previousImgRoot = '/Orion/images/Arrows/button_white_paging_previous';
        var nextImgRoot = '/Orion/images/Arrows/button_white_paging_next';
        var lastImgRoot = '/Orion/images/Arrows/button_white_paging_last';
        var displayingObjectsText = '@{R=Core.Strings;K=WEBJS_AK0_54;E=js}'; //Displaying objects {0} - {1} of {2}
        var itemsOnPageText = '@{R=Core.Strings;K=WEBJS_JT0_3;E=js}'; // Items on page

        pagerBtnFirst.unbind();
        pagerBtnPrevious.unbind();
        pagerBtnNext.unbind();
        pagerBtnLast.unbind();

        if (pageIndex == 1)
        {
            pagerBtnFirst.attr("src", firstImgRoot + '_disabled.gif');
            pagerBtnPrevious.attr("src", previousImgRoot + '_disabled.gif');
        } else {
            pagerBtnFirst.attr("src", firstImgRoot + '.gif');
            pagerBtnPrevious.attr("src", previousImgRoot + '.gif');

            pagerBtnFirst.click(function () { hfCurrentPage.val(1); createTablePage(); });
            pagerBtnPrevious.click(function () { hfCurrentPage.val(pageIndex - 1); createTablePage(); });
        }

        if (pageIndex == totalPages)
        {
            pagerBtnNext.attr("src", nextImgRoot + '_disabled.gif');
            pagerBtnLast.attr("src", lastImgRoot + '_disabled.gif');
        } else {
            pagerBtnNext.attr("src", nextImgRoot + '.gif');
            pagerBtnLast.attr("src", lastImgRoot + '.gif');

            pagerBtnNext.click(function () { hfCurrentPage.val(pageIndex + 1); createTablePage(); });
            pagerBtnLast.click(function () { hfCurrentPage.val(totalPages); createTablePage(); });
        }

        pagerCurrentPage.val(pageIndex);
        pagerTotalPagesText.html(totalPages);
        pagerItemsStatusText.html(String.format(displayingObjectsText, Math.max((pageIndex - 1) * pageSize + 1, totalRecords), Math.min(pageIndex * pageSize, totalRecords), totalRecords));
        pagerItemsPerPageText.html(itemsOnPageText);
    }

    
    var createTablePage = function () {
        
        var result = JSON.parse(hfData.val());
        var pageIndex = parseInt(hfCurrentPage.val());
        var pageSize = parseInt(pagerItemsPerPage.val());
        var totalRecords = result.Rows.length;
        var totalPages = Math.floor(totalRecords / pageSize);;

        if (totalPages * pageSize < totalRecords) {
            totalPages = totalPages + 1
        };

        if (pageIndex > totalPages) {
            pageIndex = totalPages
        };

        var headers = $('tr.ReportHeader', grid);
        headers.empty();

        var columnInfo = generateColumnInfo(result.Columns);
        $.each(columnInfo, function (colIndex, column) {
            if (column.header !== null) {
                $('<td/>').addClass("ReportHeader").html("&nbsp;" + localizedColumnHeader(column.header)).appendTo(headers);
            }
        });

        $('tr', grid).not('.ReportHeader').remove();

        $.each(result.Rows, function (rowIndex, row) {

            if (rowIndex >= (pageIndex - 1) * pageSize && rowIndex < pageIndex * pageSize)
            {
                var tr = $('<tr/>');

                $.each(row, function (cellIndex, cell) {
                    var info = columnInfo[cellIndex];
                    if (info.header !== null) {
                        renderCell(cell, row, info).addClass("Property").appendTo(tr);
                    }
                });
                grid.append(tr);
            }
        });

        grid.find("tr:gt(0)").each(function (rowIndex, row) {
            $(row).find("td:eq(0)").each(function () { this.style.width = "40%"; });
            $(row).find("td").each(function (cellIndex, cell) {
                if (cellIndex > 0) {
                    if (rowIndex % 2) {
                        cell.style.backgroundColor = "#f8f8f8";
                    }
                    else {
                        cell.style.backgroundColor = "#ffffff";
                    }
                    if (cellIndex != 2) {
                        cell.style.width = "30%";
                    }
                    else {
                        cell.style.width = "16px";
                    }
                }
                else {
                    if (rowIndex % 2) {
                        cell.style.backgroundColor = "#f8f8f8";
                    }
                }
            });
        });

        updatePager(pageIndex, pageSize, totalRecords, totalPages);
    }
    
    var parentFilter;
    var childFilter;
    var dependencyFilter;
    var autoHide;
    var errorMsg;
    var grid;
    var hfData;
    var hfCurrentPage;
    var pager;
    var pagerBtnFirst;
    var pagerBtnPrevious;
    var pagerBtnNext;
    var pagerBtnLast;
    var pagerCurrentPage;
    var pagerTotalPagesText;
    var pagerPagesStatus;
    var pagerItemsPerPageText;
    var pagerItemsPerPage;
    var pagerItemsStatusText;

    adt.createTable = function(resourceId) {
        parentFilter = $('#parentFilter-' + resourceId).val();
        childFilter = $('#childFilter-' + resourceId).val();
        dependencyFilter = $('#dependencyFilter-' + resourceId).val();
        autoHide = $("#autoHide-" + resourceId).prop("checked");
        errorMsg = $('#ErrorMsg-' + resourceId);
        grid = $('#Grid-' + resourceId);
        hfData = $('#hfDependenciesData-' + resourceId);
        hfCurrentPage = $('#hfCurrentPage-' + resourceId);
        pager = $('#pager-' + resourceId);
        pagerBtnFirst = $('#pager-btnFirst-' + resourceId);
        pagerBtnPrevious = $('#pager-btnPrevious-' + resourceId);
        pagerBtnNext = $('#pager-btnNext-' + resourceId);
        pagerBtnLast = $('#pager-btnLast-' + resourceId);
        pagerPagesStatus = $('#pager-pagesStatus-' + resourceId);
        pagerItemsPerPageText = $('#pager-itemsPerPageText-' + resourceId);
        pagerItemsPerPage = $('#pager-tbItemsPerPage-' + resourceId);
        pagerItemsStatusText = $('#pager-itemsStatusText-' + resourceId);


        var pageXofYText = '@{R=Core.Strings;K=WEBJS_JT0_2;E=js}'; //Page {0} of {1}
        pagerPagesStatus.html(String.format(pageXofYText, '<input type="text" id="pager-pagesStatus-currentPage-' + resourceId + '" class="pageNumber SmallInput" />', '<span id="pager-pagesStatus-totalPages-' + resourceId + '"></span>'));

        pagerCurrentPage = $('#pager-pagesStatus-currentPage-' + resourceId);
        pagerTotalPagesText = $('#pager-pagesStatus-totalPages-' + resourceId);

        pagerItemsPerPage.change(function () { if (isNaN(parseInt(pagerItemsPerPage.val())) || parseInt(pagerItemsPerPage.val()) < 1) { pagerItemsPerPage.val(15); } else { createTablePage(); } });
        pagerCurrentPage.change(function () { if (isNaN(parseInt(pagerCurrentPage.val())) || parseInt(pagerCurrentPage.val()) < 1) { pagerCurrentPage.val(hfCurrentPage.val()); } else { hfCurrentPage.val(pagerCurrentPage.val()); createTablePage(); } });
            
        errorMsg.hide();
        
        AsyncResources.GetAllDependencies(resourceId, parentFilter, childFilter, dependencyFilter, function (result) {

                hfData.val(JSON.stringify(result.ResultTable));

                $("#DependencyLoadWait-" + resourceId).hide();
                if ((autoHide) && (result.ResultTable.Rows.length == 0)) {
                    grid.parent().parent().addClass("HiddenResourceWrapper");
                    grid.parent().parent().css("display", "none");
                }

                var errorHtml = result.ErrorHtml;
                if (errorHtml !== null && errorHtml.length > 0) {
                    //some endpoints are not available
                    errorMsg.html(result.ErrorHtml).show();
                }
                
                createTablePage();

        },
        function(error) {
           $("#DependencyLoadWait-" + resourceId).hide();
           errorMsg.text(error.get_message()).show();
        }
        );
    };

    var localizedColumnHeader = function(columnHeader) {
        switch (columnHeader) {
        case 'DEPENDENCY NAME':
            return '@{R=Core.Strings;K=WEBJS_VB0_88;E=js}';
        case 'PARENT':
            return '@{R=Core.Strings;K=WEBJS_VB0_89;E=js}';
        case 'CHILD':
            return '@{R=Core.Strings;K=WEBJS_VB0_90;E=js}';
        default:
            return columnHeader;
        }
    };

})(SW.Core.Resources.AllDependenciesTable);
