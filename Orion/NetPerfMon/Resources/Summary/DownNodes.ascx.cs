﻿using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Data;


[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_Summary_DownNodes : SolarWinds.Orion.Web.UI.BaseResourceControl
{
	#region properties

    // overriden DefaultTitle
    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_VB0_212; }
    }

    // overriden HelpLink
    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceDownNodes";
        }
    }

	public override string EditControlLocation
	{
		get { return "/Orion/NetPerfMon/Controls/EditResourceControls/FilterEdit.ascx"; }
	}

	// overriden Edit URL
	public override String EditURL
	{
		get
		{
            return base.EditURL + "&HideInterfaceFilter=True";
		}
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        var filter = this.Resource.Properties["Filter"];

        DataTable table;

        try
        {
			var temp = "Nodes.Status=2 AND Nodes.Severity>0";
			if(!String.IsNullOrEmpty(filter)) temp += " AND " + filter;

            table = WebDAL.GetProblemNodesWithoutSeveritySWQL(temp, "NodeName ASC", this.Resource.View.LimitationID);
        }
        catch (Exception ex)
        {
            if (HasSWQLFilterError(ex, filter))
            {
                this.SQLErrorPanel.Visible = true;
                return;
            }
            throw;
        }

        if (table == null || table.Rows.Count == 0)
        {
            return;
        }
        
        table.Columns.Add("NodeURL", typeof(string));
        
        // preprocessing
        foreach(DataRow r in table.Rows)
        {
            // node details url
            r["NodeURL"] = String.Format("/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}", r["NodeID"]);
        }

        // bind data to repeater
        this.NodesRepeater.DataSource = table;
        this.NodesRepeater.DataBind();
    }
}
