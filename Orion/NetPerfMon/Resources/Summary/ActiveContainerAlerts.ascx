<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActiveContainerAlerts.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Summary_ActiveContainerAlerts" %>
<%@ Register TagPrefix="orion" TagName="AlertsTable" Src="~/Orion/NetPerfMon/Controls/AlertsOnThisEntity.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:AlertsTable runat="server" ID="AlertsTable" />
    </Content>
</orion:resourceWrapper>
