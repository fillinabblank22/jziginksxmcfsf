<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContainersLastXXEvents.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Summary_ContainersLastXXEvents" %>
<%@ Register TagPrefix="orion" TagName="ContainerEventList" Src="~/Orion/NetPerfMon/Controls/ContainerEventList.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
	    <orion:ContainerEventList runat="server" ID="containerEventList">
	    </orion:ContainerEventList>
	</Content>
</orion:resourceWrapper>
