﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CoreNetworkTopology.ascx.cs"
    Inherits="Orion_NetPerfMon_Resources_Summary_CoreNetworkTopology" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
    
<script type="text/javascript">
    $(function () {
        var refresh = function () { <%= Page.ClientScript.GetPostBackEventReference(LinkButtonRefreshResource, string.Empty) %>; };
        SW.Core.View.AddOnRefresh(refresh, '<%= updatePanel.ClientID %>');
    });
</script>

<orion:resourceWrapper ID="ResourceWrapper1" runat="server" ShowEditButton="true">
    <Content>
	<asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%=this.CustomSqlErrorMessage %> <%= LastSWQLError %>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:UpdatePanel runat="server" ID="updatePanel" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Repeater ID="TopologyData" runat="server">
                    <HeaderTemplate>
                        <table width="100%" cellspacing="0" cellpadding="2">
                            <tr class="ReportHeader">
                                <td class="ReportHeader" colspan="2">
                                    &nbsp;<%= Resources.CoreWebContent.WEBDATA_AK0_171 %>
                                </td>
                                <td class="ReportHeader" align="center">
                                    &nbsp;<%= Resources.CoreWebContent.WEBDATA_VB0_258 %>
                                </td>
                                <td class="ReportHeader" colspan="2">
                                    &nbsp;<%= Resources.CoreWebContent.WEBDATA_AK0_171 %>
                                </td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="Property TopologyRow" width="10px">
                                    <img src="/Orion/images/StatusIcons/Small-<%#Eval("AGroupStatus") %>" alt="<%= Resources.CoreWebContent.WEBDATA_VB0_14 %>"/>
                            </td>
                            <td class="Property TopologyRow" width="40%">
                                &nbsp;
                                <orion:ToolsetLink ID="ToolsetLink1" runat="server" IPAddress='<%# Eval("AIPAddress") %>'
                                    DNS='<%# Eval("ADNS") %>' SysName='<%# Eval("ASysName") %>' CommunityGUID='<%# Eval("AGuid") %>'
                                    NodeID='<%#Eval("ANodeID") %>' ToolTip=''>
		                    <%# HighlightSearchValues(Eval("ACaption"))%>
                                </orion:ToolsetLink>
                            </td>
                            <td class="Property TopologyRow" style="vertical-align: middle; text-align: center;">
                                &nbsp;<img alt="" src="/Orion/images/TopologyItems/arrow_center.gif" />
                            </td>
                            <td class="Property TopologyRow" width="10px">
                                    <img src="/Orion/images/StatusIcons/Small-<%#Eval("BGroupStatus") %>" alt="<%= Resources.CoreWebContent.WEBDATA_VB0_14 %>" />
                            </td>
                            <td class="Property TopologyRow" width="40%">
                                &nbsp;
                                <orion:ToolsetLink ID="ToolsetLink2" runat="server" IPAddress='<%# Eval("BIPAddress") %>'
                                    DNS='<%# Eval("BDNS") %>' SysName='<%# Eval("BSysName") %>' CommunityGUID='<%# Eval("BGuid") %>'
                                    NodeID='<%#Eval("BNodeID") %>' ToolTip=''>
		                    <%# HighlightSearchValues(Eval("BCaption"))%>
                                </orion:ToolsetLink>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <asp:UpdateProgress runat="server" AssociatedUpdatePanelID="updatePanel">
                    <ProgressTemplate>
                        <img alt="" src="/Orion/images/AJAX-Loader.gif" alt=""/></ProgressTemplate>
                </asp:UpdateProgress>
                <table width="100%" runat="server" id="gridFooter" cellpadding="0" cellspacing="0">
                    <tr>
                        <td colspan="3" class="TopologyFooter">
                        <div class="topologyDiv" style="white-space:nowrap;padding-right:5px;">
                            <asp:LinkButton CommandName="Page" OnCommand="TopologyData_IndexChanging" CausesValidation="false"
                                CommandArgument="Prev" ID="lbPrev" runat="server">
                                <img src="/Orion/images/TopologyItems/Arrows/arrow1Left.gif" alt=""/>&nbsp;<%= Resources.CoreWebContent.WEBDATA_VB0_259 %>&nbsp;<%=PageCount %></asp:LinkButton>
                                </div>
                                <div class="topologyDiv" style="white-space:nowrap; padding-right:5px; padding-left:5px;">
                            <asp:LinkButton CommandName="Page" OnCommand="TopologyData_IndexChanging" CausesValidation="false"
                                CommandArgument="Next" ID="lbNext" runat="server">
                                <%= Resources.CoreWebContent.WEBDATA_VB0_260 %>&nbsp;<%=PageCount %>&nbsp;<img src="/Orion/images/TopologyItems/Arrows/arrow1Right.gif" alt="" /></asp:LinkButton>
                                </div>
                                <div class="topologyDiv" style="white-space:nowrap; padding-left:5px;padding-right:5px;">
                            <asp:LinkButton CommandName="Page" OnCommand="TopologyData_IndexChanging" CommandArgument="All"
                                ID="LinkButton1" runat="server">
                                <img src="/Orion/images/TopologyItems/Arrows/Arrow2Rigth.gif" alt=""/>&nbsp;<span style="white-space:nowrap;"><%= Resources.CoreWebContent.WEBCODE_VB0_210%></span></asp:LinkButton>
                                </div>
                            <div class="topologyDiv" style="width: 5px; white-space:nowrap;padding-left:5px;">
                                <img src="/Orion/images/form_prompt_divider.gif" />
                            </div>
                            <asp:Panel ID="Panel1" CssClass="topologyDiv" Width="210" runat="server" DefaultButton="btnSearch">
                                <input type="text" runat="server" id="tbSearch" style="width: 170px; border-width: thin;" />&nbsp;
                                <asp:ImageButton runat="server" ID="btnSearch" OnClick="Search_Click" ImageUrl="~/Orion/images/TopologyItems/Arrows/refresh.gif" />
                            </asp:Panel>
                            <div style="height: 0px; line-height: 0px; width: 0px; clear: both;"></div>
                        </td>
                    </tr>
                </table>
                <%-- this button is here because of async views refreshing, we need to generate postback event to refresh update panel --%>
                <asp:LinkButton CommandName="Page" OnCommand="TopologyData_IndexChanging" CausesValidation="false" CommandArgument="Refresh" ID="LinkButtonRefreshResource" runat="server" />
                <asp:HiddenField runat="server" ID="hfPageIndex" />
                <asp:HiddenField runat="server" ID="hfSearchStr" />
                <asp:HiddenField runat="server" ID="hfShowAll" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="tbSearch" />
                <asp:AsyncPostBackTrigger ControlID="btnSearch" />
                <asp:AsyncPostBackTrigger ControlID="lbPrev" />
                <asp:AsyncPostBackTrigger ControlID="lbNext" />
                <asp:AsyncPostBackTrigger ControlID="LinkButton1" />
            </Triggers>
        </asp:UpdatePanel>
<script type="text/javascript">
    $(function() {
        $('#<%=tbSearch.ClientID%>').each(function() {
            this.value = '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_18) %>';
            this.style.color = '#555';
            this.style.fontStyle = 'italic';
            this.style.fontSize = '9pt';
        });

        $('#<%=tbSearch.ClientID%>').click(function() {
            if (this.value == '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_18) %>') {
                this.value = '';
                this.style.color = '#000';
            }
            this.style.fontStyle = 'normal';
            this.style.fontSize = '9pt';
        });

        $('#<%=tbSearch.ClientID%>').blur(function() {
            if (this.value == '') {
                this.value = '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_18) %>';
                this.style.color = '#555';
                this.style.fontStyle = 'italic';
                this.style.fontSize = '9pt';
            }
        });
    });
</script>
    </Content>
</orion:resourceWrapper>
