﻿using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.PackageManager;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Data;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_NetPerfMon_Resources_Summary_AllDependencies : BaseResourceControl
{
    protected bool AutoHide
	{
		get
		{
			if (string.IsNullOrEmpty(Resource.Properties["AutoHide"]))
				return false;
			return Resource.Properties["AutoHide"].Equals("1");
		}
	}

	protected string Filter
	{
		get
		{
			if (string.IsNullOrEmpty(Resource.Properties["Filter"]))
				return string.Empty;
			return Resource.Properties["Filter"];
		}
	}

	protected override void OnInit(EventArgs e)
	{
        Wrapper.ManageButtonText = Resources.CoreWebContent.ResourcesAll_ManageDependencies;
		Wrapper.ManageButtonTarget = "~/Orion/Admin/DependenciesView.aspx";
		Wrapper.ShowManageButton = Profile.AllowAdmin || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;

		// hide resource if there aren't any data
		if (string.IsNullOrEmpty(Filter) && AutoHide){
            bool allowInterfaces = PackageManager.InstanceWithCache.IsPackageInstalled("Orion.Interfaces");
		    using (var dal = new DependenciesDAL())
		        if (dal.GetAllDependenciesCount(allowInterfaces, Filter) == 0)
		            Wrapper.Visible = false;
        }

        base.OnInit(e);
	}

	protected override string DefaultTitle
	{
		get { return Resources.CoreWebContent.WEBCODE_VB0_205; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceAllDependencies"; }
	}

	public override string EditControlLocation
	{
		get
		{
			return "/Orion/NetPerfMon/Controls/EditResourceControls/EditDependencies.ascx";
		}
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }
}
