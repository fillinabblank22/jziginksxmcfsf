﻿using System.Linq;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Caching;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Data;
using System.Text;
using SolarWinds.Orion.Core.Common.Federation;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.IsFederationAware, "true")]
public partial class Orion_NetPerfMon_Resources_Summary_ProblemNodes : BaseResourceControl
{
    #region properties

    // overriden DefaultTitle
    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_VB0_211; }
    }

    // overriden HelpLink
    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceNodesProblems";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/FilterEdit.ascx"; }
    }

    // overriden Edit URL
    public override String EditURL
    {
        get
        {
            return base.EditURL + "&HideInterfaceFilter=True&IsSQLMode=False";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    public int ResponseWarningLevel { get; set; }
    public int ResponseErrorLevel { get; set; }
    public int PacketWarningLevel { get; set; }
    public int PacketErrorLevel { get; set; }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        var filter = this.Resource.Properties["Filter"];
        ResponseWarningLevel = Convert.ToInt32(Thresholds.ResponseTimeWarning.SettingValue);
        ResponseErrorLevel = Convert.ToInt32(Thresholds.ResponseTimeError.SettingValue);
        PacketWarningLevel = Convert.ToInt32(Thresholds.PacketLossWarning.SettingValue);
        PacketErrorLevel = Convert.ToInt32(Thresholds.PacketLossError.SettingValue);

        DataTable table = null;

        try
        {
            string temp = (string.IsNullOrEmpty(filter)) ? string.Empty : " AND ";
            string queryFilter = string.Format("{2} {0}{1}", temp, filter, TopXXSWQLDAL.GetNodesDataStatusFilter(string.Empty));
            table = WebDAL.GetProblemNodesSWQL(queryFilter, this.Resource.View.LimitationID);
            NodeChildStatusDAL.UpdateWithChildStatusDetails(table, true, true);
        }
        catch (Exception ex)
        {
            if (HasSWQLFilterError(ex, filter))
            {
                this.SQLErrorPanel.Visible = true;
                return;
            }
            throw;
        }


        if (table == null || table.Rows.Count == 0)
        {
            return;
        }

        table.Columns.Add("NodeURL", typeof(string));
        table.Columns.Add("ResponseURL", typeof(string));
        table.Columns.Add("PacketLossURL", typeof(string));
        table.Columns.Add("FormatedResponse", typeof(string));
        table.Columns.Add("FormatedPacketLoss", typeof(string));

        // preprocessing
        foreach (DataRow r in table.Rows)
        {
            var federationPrefix = FederationUrlHelper.GetLinkPrefix(r["InstanceSiteId"].ToString());
            // node details url
            r["NodeURL"] = $"{federationPrefix}/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{r["NodeID"]}";

            // chart with response time for node
            r["ResponseURL"] =
                $"{federationPrefix}/Orion/NetPerfMon/CustomChart.aspx?ChartName=AvgRTLoss&NetObject=N:{r["NodeID"]}&Period=Today";

            // chart with packet loss for node
            r["PacketLossURL"] =
                $"{federationPrefix}/Orion/NetPerfMon/CustomChart.aspx?ChartName=PercentLoss&NetObject=N:{r["NodeID"]}&Period=Today";

            // if description contains ',' separator is content divided into red and normal part
            if (!String.IsNullOrEmpty((string)r["StatusDescription"]))
            {
                StringBuilder desc = new StringBuilder();
                string[] statuses = ((string)r["StatusDescription"]).Split(Resources.CoreWebContent.String_Separator_1_VB0_59[0]);
                for (int i = 0; i < statuses.Length; i++)
                {
                    string status = statuses[i].Trim(Resources.CoreWebContent.String_Separator_1_VB0_59[0], Resources.CoreWebContent.String_Separator_1_VB1_1[0], ' ');
                    if (i == 0)
                    {
                        desc.AppendFormat("<span style='color: {0}; font-weight: bold'>{1}</span>",
                            (status.Contains("Down") ? "Red" : "Orange"),
                            System.Web.HttpUtility.HtmlEncode(status));
                    }
                    else
                    {
                        desc.Append(System.Web.HttpUtility.HtmlEncode(status));
                    }

                    if (i == statuses.Length - 1)
                    {
                        desc.Append(Resources.CoreWebContent.String_Separator_1_VB1_1);
                    }
                    else
                    {
                        desc.Append("<br/>");
                    }
                }

                r["StatusDescription"] = desc.ToString();
            }
        }

        // bind data to repeater

        if (table.Columns.Contains("Severity"))
        {
            // severity may have been affected by limitations, resorting locally.
            DataView dv = new DataView(table);
            dv.Sort = "Severity DESC";
            this.NodesRepeater.DataSource = dv;
        }
        else
        {
            this.NodesRepeater.DataSource = table;
        }

        this.NodesRepeater.DataBind();
    }
}