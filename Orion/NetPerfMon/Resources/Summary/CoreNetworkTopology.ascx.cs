﻿using SolarWinds.Orion.Core.Web;
using SolarWinds.Topology.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_Summary_CoreNetworkTopology : SolarWinds.Orion.Web.UI.BaseResourceControl
{
	private int _pagecount;
	private int _itemsCount;
	private string _lastSWQLError;

	public bool IsResourceShownAtReportsPage
	{
		get
		{
			return Request.Url.AbsolutePath.ToLowerInvariant().Contains("/orion/reports/") || Request.Url.AbsolutePath.ToLowerInvariant().Contains("/orion/report.aspx");
		}
	}

	public string LastSWQLError
	{
		get
		{
			return _lastSWQLError;
		}
	}

	public int PageIndex
	{
		get
		{
			int pageIntex = 0;
			int.TryParse(hfPageIndex.Value, out pageIntex);
			return pageIntex;
		}
		set
		{
			hfPageIndex.Value = value.ToString();
		}
	}

	public int PageCount
	{
		get { return _pagecount; }
		set { _pagecount = value; }
	}

	public bool IsShowedAll
	{
		get
		{
			bool val;
			bool.TryParse(hfShowAll.Value, out val);
			return val;
		}
		set
		{
			hfShowAll.Value = value.ToString();
		}
	}

	public int ItemsCount
	{
		get { return _itemsCount; }
		set { _itemsCount = value; }
	}

	public string SearchStr
	{
		get { return hfSearchStr.Value; }
		set { hfSearchStr.Value = value; }
	}

	protected override string DefaultTitle
	{
		get
		{
			if (Resource != null)
			{
				string title = String.IsNullOrEmpty(Resource.Title) ? Resources.TopologyWebContent.WEBCODE_VB0_214 : Resource.Title;
				Resource.Properties["Title"] = title;
				return title;
			}
			return Resources.TopologyWebContent.WEBCODE_VB0_214;
		}
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceCoreTopology"; }
	}

    public override string CustomSqlErrorMessage
    {
        get
        {
            return Resources.TopologyWebContent.WEBCODE_VB0_215;
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
		int pagecount;
		if (int.TryParse(Resource.Properties["ShowItemsCount"], out pagecount))
			PageCount = pagecount;
		else
			PageCount = 5;

        LinkButtonRefreshResource.Style.Add("display", "none");

		if (!IsPostBack)
		{
			string netObjectType;
			int netObjectID = -1;

			GetCurrentNetObjectID(out netObjectType, out netObjectID);

			var nodeSrcFilter = Resource.Properties["NodeSrcFilter"];
			var nodeDestFilter = Resource.Properties["NodeDestFilter"];

			DataTable table = null;
			_lastSWQLError = string.Empty;

			try
			{
				using (TopologyDAL dal = new TopologyDAL())
				{
					ItemsCount = dal.GetCoreNetworkTopologyItemsCount(netObjectType, netObjectID, nodeSrcFilter, nodeDestFilter, SearchStr);
					table = dal.GetCoreNetworkTopologyDataTable(netObjectType, netObjectID, nodeSrcFilter, nodeDestFilter, string.Empty, 1, PageCount);
				}
			}
			catch (System.ServiceModel.FaultException ex)
			{
				base.HandleResourceException(ex);
			}
            catch (SolarWinds.Orion.Web.InformationService.SwisQueryException ex)
            {
                _lastSWQLError = ex.LocalizedMessage;
                if (ex.Cause == SolarWinds.Orion.Web.InformationService.SwisQueryException.Reason.RunQueryFailed && 
                    (!string.IsNullOrEmpty(nodeSrcFilter) || !string.IsNullOrEmpty(nodeDestFilter)))
                {
                    this.SQLErrorPanel.Visible = true;
                    this.updatePanel.Visible = false;
                    return;
                }
                base.HandleResourceException(ex);
            }
			catch (Exception ex)
			{
				base.HandleResourceException(ex);
			}

			PageIndex = 0;
			SetPagerStates();

			if (IsResourceShownAtReportsPage)
			{
				IsShowedAll = true;
				gridFooter.Visible = false;
			}

			if (table == null || table.Rows.Count == 0)
			{
				if (string.IsNullOrEmpty(nodeSrcFilter + nodeDestFilter) && Resource.Properties["AutoHide"] == "1")
					this.Visible = false;
				else
					this.TopologyData.Visible = false;
				return;
			}

			this.TopologyData.DataSource = table;
			this.TopologyData.DataBind();

			SearchStr = string.Empty;
		}

		if (string.IsNullOrEmpty(tbSearch.Value) || tbSearch.Value.Equals(Resources.TopologyWebContent.WEBCODE_VB0_216, StringComparison.OrdinalIgnoreCase))
		{
			tbSearch.Style["color"] = "#555";
			tbSearch.Style["font-style"] = "italic";
			tbSearch.Value = Resources.TopologyWebContent.WEB_JS_CODE_VB0_18;
		}
		else
		{
			tbSearch.Style["color"] = "#000";
			tbSearch.Style["font-style"] = "normal";
		}

        tbSearch.Attributes.Add("onclick", String.Format("if (this.value == '{0}') {1} this.style.fontStyle = 'normal'; this.style.fontSize = '9pt'", Resources.TopologyWebContent.WEB_JS_CODE_VB0_18, "{ this.value = ''; this.style.color = '#000'; }"));
        tbSearch.Attributes.Add("onblur", String.Format("{0}'{2}'{1}", "if (this.value == '') { this.value = ", "; this.style.color = '#555'; this.style.fontStyle = 'italic'; this.style.fontSize = '9pt'; }", Resources.CoreWebContent.WEB_JS_CODE_VB0_18));
	}

	protected void TopologyData_IndexChanging(object sender, CommandEventArgs e)
	{
		switch (e.CommandArgument.ToString().ToLowerInvariant())
		{
			case "next":
				SetItemsCount();
				PageIndex = PageIndex + 1;
				break;
			case "prev":
				SetItemsCount();
				PageIndex = PageIndex - 1;
				break;
			case "all":
				SetSearchString();
				SetItemsCount();
				PageIndex = 0;
				PageCount = ItemsCount;
				IsShowedAll = true;
				break;
            case "refresh":
                SetItemsCount();
                break;
		}

		DataBind();
	}

	private void SetItemsCount()
	{
		string netObjectType;
		int netObjectID = -1;

		GetCurrentNetObjectID(out netObjectType, out netObjectID);

		var nodeSrcFilter = Resource.Properties["NodeSrcFilter"];
		var nodeDestFilter = Resource.Properties["NodeDestFilter"];

		try
		{
			using (TopologyDAL dal = new TopologyDAL())
				ItemsCount = dal.GetCoreNetworkTopologyItemsCount(netObjectType, netObjectID, nodeSrcFilter, nodeDestFilter, SearchStr);
		}
		catch (Exception ex)
		{
			base.HandleResourceException(ex);
		}
	}

	public override void DataBind()
	{
		string netObjectType;
		int netObjectID = -1;

		if (IsShowedAll)
		{
			PageIndex = 0;
			PageCount = ItemsCount;
		}

		GetCurrentNetObjectID(out netObjectType, out netObjectID);

		var nodeSrcFilter = Resource.Properties["NodeSrcFilter"];
		var nodeDestFilter = Resource.Properties["NodeDestFilter"];

		DataTable table = null;
		int fromRow = (PageIndex * PageCount) + 1;
		int toRow = fromRow + PageCount - 1;

		try
		{
			using (TopologyDAL webdal = new TopologyDAL())
				table = webdal.GetCoreNetworkTopologyDataTable(netObjectType, netObjectID, nodeSrcFilter, nodeDestFilter, SearchStr, fromRow, toRow);
		}
		catch (Exception ex)
		{
			base.HandleResourceException(ex);
		}

		if (table == null || table.Rows.Count == 0)
		{
			TopologyData.Visible = false;
			return;
		}
		else
			TopologyData.Visible = true;

		TopologyData.DataSource = table;
		TopologyData.DataBind();

		SetPagerStates();
	}

	private void SetSearchString()
	{
		if (!string.IsNullOrEmpty(tbSearch.Value) && !tbSearch.Value.Equals(Resources.TopologyWebContent.WEBCODE_VB0_216, StringComparison.OrdinalIgnoreCase))
			SearchStr = tbSearch.Value.Replace("<wbr>", "").Replace("\u200B", "");
		else
			SearchStr = string.Empty;
	}

	protected void Search_Click(object sender, EventArgs e)
	{
		PageIndex = 0;

		SetSearchString();
		SetItemsCount();
		DataBind();
	}

	private void SetPagerStates()
	{
		lbPrev.Enabled = (PageIndex != 0);
		lbNext.Enabled = (PageIndex < ((double)ItemsCount / PageCount - 1));
	}

	protected void GetCurrentNetObjectID(out string netObjectType, out int netObjectID)
	{
		netObjectType = String.Empty;
		netObjectID = -1;

		string netObjectString = Request.QueryString["NetObject"];

		if (!String.IsNullOrEmpty(netObjectString))
		{
			string[] temp = netObjectString.Split(':');
			if (temp.Length == 2)
			{
				if (Int32.TryParse(temp[1].Trim(), out netObjectID))
				{
					netObjectType = temp[0].Trim();
					return;
				}
			}
		}
	}

	protected string MakeBreakableString(object val)
	{
		if (Request.Browser.Browser == "IE" && Request.Browser.MajorVersion == 6)
			return FormatHelper.MakeBreakableString(val.ToString(), true);
		else
			return FormatHelper.MakeBreakableString(val.ToString(), false);
	}

	protected string HighlightSearchValues(object val)
	{
		string str = SearchStr.Replace("*", "");
		string result = val.ToString();
		if (!string.IsNullOrEmpty(str))
		{
			string[] arr = Regex.Split(val.ToString(), Regex.Escape(str), RegexOptions.IgnoreCase);
			foreach (string item in arr)
				if (!string.IsNullOrEmpty(item))
					result = Regex.Replace(result, Regex.Escape(item), new MatchEvaluator(MakeBreakable), RegexOptions.IgnoreCase);

			return Regex.Replace(
				result,
				Regex.Escape(str),
				new MatchEvaluator(ReplaceHighlight),
				RegexOptions.IgnoreCase);
		}
		return MakeBreakableString(val);
	}

	private string MakeBreakable(Match m)
	{
		return MakeBreakableString(m.ToString());
	}

	private string ReplaceHighlight(Match m)
	{
		return string.Format("<span class=\"selectedSearchStr\" >{0}</span>", MakeBreakableString(m));
	}

	public override string EditControlLocation
	{
		get
		{
			return "/Orion/NetPerfMon/Controls/EditResourceControls/NetworkTopologyEdit.ascx";
		}
	}
}
