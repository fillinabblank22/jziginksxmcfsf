using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Linq;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_Summary_DownContainerItems : BaseResourceControl
{
	protected void grid_Init(object sender, EventArgs e)
	{
		//string filter = this.GetStringValue("Filter", "");
		//string selectedStatus = ((int) Status.NotAvailable).ToString();

		grid.DataSource =
			from c in new ContainersDAL().GetMembers(String.Format("m.Status = {0}", (Int32)OBJECT_STATUS.Down), false)
			select new ContainerMember(c);
		grid.DataBind();
	}

	protected override string DefaultTitle
	{
		get { return Resources.CoreWebContent.WEBCODE_VB0_213; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
