﻿ORIONContainers = {};

ORIONContainers.ContainersTree = function (federationEnabled, missingContentPlaceholderEnabled) {
    var nodeTemplateId;
    var treeRoot;
    var resourceId;
    var rememberExpandedGroups;
    var maxBatchSize;
    var federationEnabled = federationEnabled;
    var emptyContentPlaceholderEnabled = missingContentPlaceholderEnabled;

    var CreateTreeLevelBatch = function (nodeId, fromIndex, expandAll) {
        var moreNode = $("#" + GetUniqueNodeID(nodeId + '_' + 'showMore'));

        if (moreNode) {
            moreNode.remove();
        }

        var node = new Node(nodeId);

        var status = '';

        if (node.id().indexOf('-status-') != -1) {
            status = node.id();
        }

        ContainersTree.GetContainerNodes(resourceId, parseInt(node.myContainerID()), status, fromIndex, maxBatchSize, $('#treeState').val(), parseInt(node.instanceSiteId()), function (items, e) {
            var loadingNode = $("#" + GetUniqueNodeID('loadingNode'));
            if (loadingNode) {
                loadingNode.remove();
            }

            if ((items.length > 0) && (node.expanded())) {
                CreateTreeLevel(nodeId, items, true);

                if (items[items.length - 1].Id == '') {
                    if (expandAll) {
                        node.addChild(CreateLoadingNode());
                        setTimeout(function () { CreateTreeLevelBatch(nodeId, fromIndex + items.length - 1, true); }, 100);
                    }
                }
            }
        });
    };

    var CreateRootLevelBatch = function (fromIndex) {
        ContainersTree.GetContainerNodes(resourceId, 0, '', fromIndex, maxBatchSize, $('#treeState').val(), 0, function (items, e) {
            var loadingNode = $("#" + GetUniqueNodeID('loadingNode'));
            if (loadingNode) {
                loadingNode.remove();
            }
            treeRoot.find("center[type='loading_container']").remove();

            if (items.length > 0) {
                CreateRootLevel(items);
                if (items[items.length - 1].Id == '') {
                    treeRoot.append(CreateLoadingNode());
                    setTimeout(function () { CreateRootLevelBatch(fromIndex + items.length - 1); }, 100);
                }
            } else if (emptyContentPlaceholderEnabled) {
                $("#ORIONCoreTreeGroup-" + resourceId + "-NoDataContent").show();
            }
        });
    };

    var CreateRootLevel = function (items) {
        for (var i = 0; i < items.length; i++) {
            var node;
            if (items[i].Id != '') {
                node = CreateTreeNode(
                    items[i].Id,
                    0,
                    items[i].Name,
                    items[i].Entity,
                    items[i].Status,
                    items[i].IsExpandable,
                    items[i].Description,
                    items[i].MyContainerID,
                    (i % 2 != 0),
                    items[i].StatusIconHint,
                    items[i].InstanceSiteId);
            } else {
                continue;
            }

            treeRoot.append(node);

            if (rememberExpandedGroups) {
                if (items[i].ExpandedState != '') {
                    var expandItemFunction = function(n) {
                        var status = '';

                        if (n.id().indexOf('-status-') != -1) {
                            status = n.id();
                        }

                        n.addChild(CreateLoadingNode());
                        n.expand();

                        ContainersTree.GetContainerNodes(resourceId, parseInt(n.myContainerID()), status, 0, maxBatchSize, $('#treeState').val(), parseInt(n.instanceSiteId()), function (innerItems, e) {
                            var loadingNode = $("#" + GetUniqueNodeID('loadingNode'));
                            if (loadingNode) {
                                loadingNode.remove();
                            }
                            n.childrenDiv.children().remove();
                            CreateTreeLevel(n.id(), innerItems, true);
                        });
                    }
                    expandItemFunction(new Node(items[i].ExpandedState));
                }
            }
        }
    }

    var CreateTreeLevel = function (parentNodeId, items, checkForExpansion) {
        var node = new Node(parentNodeId);
        for (var i = 0; i < items.length; i++) {
            var newNodeID;
            var newNode;

            if (items[i].Id != '') {
                newNodeID = parentNodeId + '_' + items[i].Id;
                newNode = CreateTreeNode(
                    newNodeID,
                    parseInt(node.level()),
                    items[i].Name,
                    items[i].Entity,
                    items[i].Status,
                    items[i].IsExpandable,
                    items[i].Description,
                    items[i].MyContainerID,
                    false,
                    items[i].StatusIconHint,
                    items[i].InstanceSiteId);

                node.addChild(newNode);

            } else {
                node.addChild(CreateShowMoreNode(parentNodeId, parseInt(items[items.length - 1].Name),
                    function () {
                        CreateTreeLevelBatch(parentNodeId, items.length - 1, true);
                    }));
                continue;
            }

            if (checkForExpansion && rememberExpandedGroups && items[i].IsExpandable) {
                if (items[i].ExpandedState != '') {
                    var expandItemFunction = function (n) {
                        var status = '';

                        if (n.id().length > 5 && n.id().substr(0, 6) == 'status') {
                            status = n.id();
                        }

                        n.addChild(CreateLoadingNode());
                        n.expand();

                        ContainersTree.GetContainerNodes(resourceId, parseInt(n.myContainerID()), status, 0, maxBatchSize, $('#treeState').val(), parseInt(n.instanceSiteId()), function (innerItems, e) {
                            n.childrenDiv.children().remove();
                            CreateTreeLevel(n.id(), innerItems, checkForExpansion);
                        });

                        n.expand();
                    };
                    expandItemFunction(new Node(items[i].ExpandedState));
                }
            }
        }
    };

    var CreateTreeNode = function (nodeId, parentLevel, name, entity, status, isExpandable, description, myContainerID, setStripe, statusIconHint, instanceSiteId) {
        var node = new Node(nodeTemplateId);
        node.id(nodeId); // this clones node
        node.level(parentLevel + 1);
        node.myContainerID(myContainerID);
        node.name(name);
        node.description(description);
        node.isExpandable(isExpandable);
        var imagePrefix = federationEnabled ? String.format('/Server/{0}/ImageHandler', instanceSiteId) : '';
		
        // get Entity Id which is next to Entity name
        var idParts = nodeId.split('-');
		var entityPosition = -1;
		for (var i = 0; i < idParts.length; ++i) {
			if (idParts[i].endsWith(entity)) {
				entityPosition = i;
				break;
			}
		}
		var id = (entityPosition == -1) ? idParts.pop() : idParts[entityPosition + 1];
        
		node.statusIcon(String.format('{0}/Orion/StatusIcon.ashx?size=small&entity={1}&id={2}&status={3}{4}', imagePrefix, entity, id, status, (statusIconHint != "") ? ("&hint=" + statusIconHint) : ""));
        node.instanceSiteId(instanceSiteId);
        node.onExpand(function () { Toggle(nodeId); });
        if (setStripe)
            node.setZebraStripe();
        node.show();

        return node.getNode();
    };

    var CreateLoadingNode = function () {
        var node = new Node(nodeTemplateId);
        node.id('loadingNode'); // this clones node
        node.level(0);
        node.myContainerID(0);
        node.name('@{R=Core.Strings;K=WEBJS_VB0_1; E=js}');
        node.description('');
        node.isExpandable(false);
        node.statusIcon('/Orion/images/AJAX-Loader.gif');
        node.show();

        return node.getNode();
    };

    var CreateShowMoreNode = function (parentId, numMore, callback) {
        var node = new Node(nodeTemplateId);
        node.id(parentId + '_' + 'showMore'); // this clones node
        node.level(0);
        node.myContainerID(0);
        node.name((numMore == 1) ? String.format('@{R=Core.Strings;K=WEBJS_VB0_9; E=js}', numMore) : String.format('@{R=Core.Strings;K=WEBJS_VB0_8; E=js}', numMore));
        node.description('');
        node.isExpandable(false);
        node.statusIcon(null);
        node.onClick(callback);
        node.show();

        return node.getNode();
    };

    var GetExpandedNodes = function () {
        var expandedNodes = "";
        treeRoot.find(".expanded").each(function (index, elem) {
            expandedNodes += (expandedNodes == "") ? elem.id : ";" + elem.id;
        });
        return expandedNodes;
    };

    var Toggle = function (nodeId) {
        var node = new Node(nodeId);
        if (node.expanded()) {
            node.collapse();

            if (rememberExpandedGroups) {
                ContainersTree.ChangeTree(resourceId, GetExpandedNodes());
            }
        } else {
            node.addChild(CreateLoadingNode());
            node.expand();
            CreateTreeLevelBatch(nodeId, 0);
            if (rememberExpandedGroups) {
                ContainersTree.ChangeTree(resourceId, GetExpandedNodes());
            }
        }
    };

    var GetUniqueNodeID = function (nodeId) {
        var id = GetSafeID(nodeId);
        if (id.indexOf(resourceId.toString() + '-') == 0) {
            return id;
        }
        return resourceId + '-' + id;
    };

    var GetSafeID = function (id) {
        return id.replace(/\./g, ''); // element id can't contain dots
    };

    function Node(nodeId) {
        this.elementId = GetUniqueNodeID(nodeId);

        this.node = $('#' + this.elementId);
        this.expandButton = this.node.find('a > img.containersTreeNode-expandButton');
        this.childrenDiv = $('#' + this.elementId + '-children');
        this.expandable = true;

        this.id = function (id) {
            if (typeof id == 'undefined') {
                return this.node.attr('id');
            } else {
                this.elementId = GetUniqueNodeID(id);

                this.node = this.node.clone();
                this.expandButton = this.node.find('a > img.containersTreeNode-expandButton');
                this.node.attr('id', this.elementId);
                this.childrenDiv = this.node.children('div.containersTreeNode-children');
                this.childrenDiv.attr('id', this.elementId + '-children');
            }
        },
        this.expanded = function () {
            return this.node.hasClass('expanded');
        },
        this.isExpandable = function (isExpandable) {
            if (typeof isExpandable == 'undefined') {
                return this.expandable;
            } else {
                this.expandable = isExpandable;
                if (!isExpandable) {
                    this.expandButton.css('display', 'none'); // jquery hide isn't working in this exact moment.
                } else {
                    this.expandButton.show();
                }
            }
        },
        this.myContainerID = function (myContainerID) {
            if (typeof myContainerID == 'undefined') {
                return this.node.attr("mycontainerid");
            } else {
                this.node.attr('mycontainerid', myContainerID);
            }
        },
        this.instanceSiteId = function (instanceSiteId) {
            if (typeof instanceSiteId == 'undefined') {
                return this.node.attr("instancesiteid");
            } else {
                this.node.attr('instancesiteid', instanceSiteId);
            }
        },
        this.level = function (level) {
            if (typeof level == 'undefined') {
                return this.node.attr("level");
            } else {
                this.node.attr('level', level);
            }
        },
        this.expand = function () {
            this.node.addClass('expanded');
            this.expandButton.attr('src', '/Orion/images/Button.Collapse.gif');
            this.childrenDiv.slideDown('fast');
        },
        this.collapse = function () {
            this.childrenDiv.slideUp('fast');
            this.node.removeClass('expanded');
            this.expandButton.attr('src', '/Orion/images/Button.Expand.gif');
            this.childrenDiv.children().remove();
        },
        this.name = function (name) {
            if (typeof name == 'undefined') {
                return this.node.find('span.containersTreeNode-itemName').html();
            } else {
                this.node.find('span.containersTreeNode-itemName').html(name);
            }
        },
        this.description = function (description) {
            if (typeof description == 'undefined') {
                return this.node.find('span.containersTreeNode-itemDescription').html();
            } else {
                this.node.find('span.containersTreeNode-itemDescription').html(description);
            }
        },
        this.statusIcon = function (src) {
            if (typeof src == 'undefined') {
                return this.node.find('img.containersTreeNode-statusIcon').attr('src');
            } else if (src === null) {
                this.node.find('img.containersTreeNode-statusIcon').hide();
            } else {
                this.node.find('img.containersTreeNode-statusIcon').attr('src', src);
            }
        },
        this.show = function () {
            this.node.show();
        },
        this.getNode = function () {
            return this.node;
        },
        this.onClick = function (handler) {
            this.node.find('span.containersTreeNode-itemName').wrap('<a href="javascript:void(0);" class="containersTreeNode-clickWrapper"></a>');

            this.node.find('a.containersTreeNode-clickWrapper').click(
            function () {
                handler();
            });
        },
        this.onExpand = function (handler) {
            var self = this;
            if (self.isExpandable()) {
                this.node.find('a.containersTreeNode-expandLink').click(
                function () {
                    if (self.isExpandable())
                        handler();
                });
            }
        },
        this.addChild = function (child) {
            this.childrenDiv.append(child);
        },
        this.setZebraStripe = function () {
            this.node.addClass('ZebraStripe');
        }
    }

    this.Load = function (elementId, resId, treeNodeTemplateId, rememberExpanded, batchSize) {
        nodeTemplateId = treeNodeTemplateId;
        resourceId = resId;
        rememberExpandedGroups = rememberExpanded;
        maxBatchSize = batchSize;
        treeRoot = $('#' + elementId);
        treeRoot.empty();
        $("#ORIONCoreTreeGroup-" + resourceId + "-NoDataContent").hide();
        CreateRootLevelBatch(0);
    };
};
