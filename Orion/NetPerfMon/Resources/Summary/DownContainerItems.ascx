<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DownContainerItems.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Summary_DownContainerItems" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Containers" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:Repeater ID="grid" runat="server" OnInit="grid_Init">
            <HeaderTemplate>
                <table class="DataGrid NeedsZebraStripes">
                    <thead>
                        <tr>
                            <td class="ReportHeader" style="text-transform: uppercase;" colspan="2"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_256) %></td>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr <%# Container.ItemIndex%2==0 ? "" : "class=\"ZebraStripe\"" %>>
                    <td style="border-bottom-width: 0px!important;" valign="center" width="27">
                        <orion:EntityStatusIcon ID="EntityStatusIcon1" runat="server" Entity="<%# DefaultSanitizer.SanitizeHtml(((ContainerMember)Container.DataItem).Entity) %>" EntityId="<%# DefaultSanitizer.SanitizeHtml(((ContainerMember)Container.DataItem).MemberPrimaryID) %>" Status="<%# DefaultSanitizer.SanitizeHtml(((ContainerMember)Container.DataItem).Status.StatusId) %>" />                    
                    </td>
                    <td style="border-bottom-width: 0px!important;" valign="center">
                            <%# DefaultSanitizer.SanitizeHtml(((ContainerMember)Container.DataItem).FullName) %>
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_257) %>
                        <a href="<%# DefaultSanitizer.SanitizeHtml(((ContainerMember)Container.DataItem).Container.DetailsUrl) %>">
                            <%# DefaultSanitizer.SanitizeHtml(((ContainerMember)Container.DataItem).Container.Name) %></a>&nbsp;
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>