<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HighResponseTime.ascx.cs"
    Inherits="Orion_NetPerfMon_Resources_Summary_HighResponseTime" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Swis" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Federation" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= DefaultSanitizer.SanitizeHtml(this.CustomSqlErrorMessage) %>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="NodesRepeater">
            <HeaderTemplate>
                <table border="0" cellpadding="2" cellspacing="0" width="100%">
                    <tr>
                        <td class="ReportHeader" valign="middle" width="20">
                            &nbsp;
                        </td>
                        <td class="ReportHeader" valign="middle" >
                            &nbsp;
                        </td>
                        <td class="ReportHeader" valign="middle" align="center">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_261) %> &nbsp;
                        </td>
                        <td class="ReportHeader" valign="middle" align="center">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_262) %> &nbsp;
                        </td>
                        <td class="ReportHeader" valign="middle" align="center">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_263) %>
                        </td>
                        <td class="ReportHeader" valign="middle" align="center">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_264) %>
                        </td>
                        <% if (SwisFederationInfo.IsFederationEnabled)
                           { %>
                            <td class="ReportHeader" valign="middle" align="center">
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YS0_1) %>
                            </td>
                        <% } %>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td class="Property" valign="middle" width="20">
                        <img alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_14) %>" src="<%# DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.UI.StatusIcons.NodeIconFactory.SmallIconURL( Eval("NodeID"), Eval("Status") )) %>" />&nbsp;
                    </td>
                    <td class="Property" valign="middle">
                        <orion:ToolsetLink  runat="server" ID="link" 
                                            IPAddress='<%# DefaultSanitizer.SanitizeHtml(Eval("IP_Address")) %>'
                                            DNS='<%# DefaultSanitizer.SanitizeHtml(Eval("DNS")) %>'
                                            SysName='<%# DefaultSanitizer.SanitizeHtml(Eval("SysName")) %>' 
                                            CommunityGUID='<%# DefaultSanitizer.SanitizeHtml(Eval("GUID")) %>'
                                            NavigateUrl='<%# DefaultSanitizer.SanitizeHtml(Eval("NodeURL")) %>'>
                        <%# DefaultSanitizer.SanitizeHtml(Eval("NodeName")) %>
                        </orion:ToolsetLink>
                    </td>
                    <td class="Property" valign="middle" align="center" >
                        <asp:HyperLink ID="CurrentLink" runat="server" NavigateUrl='<%# DefaultSanitizer.SanitizeHtml(Eval("ResponseURL")) %>'
                            Target="_blank" >
                            <orion:ResponseLabel runat="server" ID="PercentLabel" Status='<%# DefaultSanitizer.SanitizeHtml(Eval("MinResponseTime")) %>'
                                WarningLevel='<%# ResponseWarningLevel %>' ErrorLevel='<%# ResponseErrorLevel %>' />
                        </asp:HyperLink>
                    </td>
                    <td class="Property" valign="middle" align="center" >
                        <asp:HyperLink ID="AverageLink" runat="server" NavigateUrl='<%# DefaultSanitizer.SanitizeHtml(Eval("ResponseURL")) %>'
                            Target="_blank" >
                        <orion:ResponseLabel runat="server" ID="PercentLabel1" Status='<%# DefaultSanitizer.SanitizeHtml(Eval("AvgResponseTime")) %>'
                                WarningLevel='<%# ResponseWarningLevel %>' ErrorLevel='<%# ResponseErrorLevel %>' /></asp:HyperLink>
                    </td>
                    <td class="Property" valign="middle" align="center" >
                        <asp:HyperLink ID="MaximumLink" runat="server" NavigateUrl='<%# DefaultSanitizer.SanitizeHtml(Eval("ResponseURL")) %>'
                            Target="_blank" >
                        <orion:ResponseLabel runat="server" ID="PercentLabel2" Status='<%# DefaultSanitizer.SanitizeHtml(Eval("MaxResponseTime")) %>'
                                WarningLevel='<%# ResponseWarningLevel %>' ErrorLevel='<%# ResponseErrorLevel %>' /></asp:HyperLink>
                    </td>
                    <td class="Property" valign="middle" align="center" >
                        <asp:HyperLink ID="PacketLossLink" runat="server" NavigateUrl='<%# DefaultSanitizer.SanitizeHtml(Eval("PacketLossURL")) %>'
                            Target="_blank" >
                        <orion:PercentLabel runat="server" ID="PercentLabel3" Status='<%# Convert.ToInt32(Eval("PercentLoss")) %>'
                                WarningLevel='<%# PacketWarningLevel %>' ErrorLevel='<%# PacketErrorLevel %>' /></asp:HyperLink>&nbsp;
                    </td>
                    <% if (SwisFederationInfo.IsFederationEnabled)
                       { %>
                        <td class="Property" valign="middle" align="center">
                            <asp:HyperLink ID="SiteLink" runat="server" Target="_blank" NavigateUrl='<%# DefaultSanitizer.SanitizeHtml(FederationUrlHelper.GetLinkPrefix((int)Eval("InstanceSiteId"))) %>'><%# DefaultSanitizer.SanitizeHtml(Eval("SiteName")) %></asp:HyperLink>
                        </td>
                    <% } %>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:ResourceWrapper>
