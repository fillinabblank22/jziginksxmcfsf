<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContainersByCurrentStatus.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Summary_ContainersByCurrentStatus" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Federation" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Swis" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Containers" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:Repeater ID="grid" runat="server" OnInit="grid_Init">
            <HeaderTemplate>
                <table class="DataGrid NeedsZebraStripes">
                    <thead>
                        <tr>
                            <td class="ReportHeader" style="text-transform: uppercase;" colspan="2"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_265) %></td>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr <%# DefaultSanitizer.SanitizeHtml(((Container.ItemIndex%2==0) ? "" : "class=\"ZebraStripe\"")) %>>
                    <td style="border-bottom-width: 0px!important;" valign="center" width="27">
                        <orion:EntityStatusIcon ID="EntityStatusIcon1" runat="server" Entity="<%# DefaultSanitizer.SanitizeHtml(((Container)Container.DataItem).Entity) %>" Status="<%# DefaultSanitizer.SanitizeHtml(((Container)Container.DataItem).Status.StatusId) %>" ServerId="<%# DefaultSanitizer.SanitizeHtml(((Container)Container.DataItem).SolarWindsServerId) %>" />
                    </td>
                    <td style="border-bottom-width: 0px!important;" valign="center">
                        <a href="<%# DefaultSanitizer.SanitizeHtml(FederationUrlHelper.GetLinkPrefix(((Container)Container.DataItem).SolarWindsServerId)) %><%# DefaultSanitizer.SanitizeHtml(((Container)Container.DataItem).DetailsUrl) %>" Target='<%# DefaultSanitizer.SanitizeHtml(SwisFederationInfo.IsFederationEnabled ? "_blank" : "_self") %>'>
                            <%# DefaultSanitizer.SanitizeHtml(UIHelper.Escape(((Container)Container.DataItem).Name)) %></a>&nbsp;
                            <% if (SwisFederationInfo.IsFederationEnabled){ %>
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.ContainersByCurrentStatus_GroupSiteSeparator) %>&nbsp;<a href="<%# DefaultSanitizer.SanitizeHtml(FederationUrlHelper.GetLinkPrefix(((Container)Container.DataItem).SolarWindsServerId)) %>" target="_blank">
                                <%# DefaultSanitizer.SanitizeHtml(UIHelper.Escape(((Container)Container.DataItem).SolarWindsServerName)) %></a>
                            <% }%>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>