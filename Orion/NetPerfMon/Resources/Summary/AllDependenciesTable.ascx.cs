using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Dependencies;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Data;
using System.ServiceModel;
using System.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_NetPerfMon_Resources_Summary_AllDependenciesTable : BaseResourceControl
{
	protected bool AutoHide
	{
		get
		{
			if (string.IsNullOrEmpty(Resource.Properties["AutoHide"]))
				return false;
			return Resource.Properties["AutoHide"].Equals("1");
		}
	}

	protected string Filter
	{
		get
		{
			if (string.IsNullOrEmpty(Resource.Properties["Filter"]))
				return string.Empty;
			return Resource.Properties["Filter"];
		}
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

	protected override string DefaultTitle
	{
		get { return Resources.CoreWebContent.WEBCODE_VB0_206; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceNodeDependencies"; }
	}

	public override string EditControlLocation
	{
		get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditDependencies.ascx"; }
	}

	protected override void OnInit(EventArgs e)
	{
        Wrapper.ManageButtonText = Resources.CoreWebContent.ResourcesAll_ManageDependencies;
		    Wrapper.ManageButtonTarget = "~/Orion/Admin/DependenciesView.aspx";
		    Wrapper.ShowManageButton = Profile.AllowAdmin || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
		base.OnInit(e);
	}
}