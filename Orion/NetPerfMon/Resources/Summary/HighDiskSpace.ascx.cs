﻿using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.Federation;


[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.IsFederationAware, "true")]
public partial class Orion_NetPerfMon_Resources_Summary_HighDiskSpace : SolarWinds.Orion.Web.UI.BaseResourceControl
{
    private string currentNodeId = String.Empty;
    
    #region properties

    // overriden DefaultTitle
    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_VB0_220; }
    }

    public override string SubTitle
    {
        get
        {
            return base.SubTitle.Replace("XX", Math.Round(Thresholds.DiskSpaceWarning.SettingValue).ToString());
        }
    }

    // overriden HelpLink
    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceVolumesHighPercentUsage";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/FilterEdit.ascx"; }
    }

    // overriden Edit URL
    public override String EditURL
    {
        get
        {
            return base.EditURL + "&HideInterfaceFilter=True&ShowVolumeFilter=True&IsSQLMode=False";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    public int WarningLevel { get; set; }
    public int ErrorLevel { get; set; }

    #endregion

    // returns double value or default value if conversion is not aviable or is invalid
    protected double GetNotNullValue(object what, double defaultValue)
    {
        double value = 0.0;

        try
        {
            value = Convert.ToDouble(what);
        }
        catch { return defaultValue; }

        return value;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var filter = this.Resource.Properties["Filter"];

        ErrorLevel = Convert.ToInt32(Thresholds.DiskSpaceError.SettingValue);
        WarningLevel = Convert.ToInt32(Thresholds.DiskSpaceWarning.SettingValue);

        DataTable table = null;

        try
        {
            var temp = (String.IsNullOrEmpty(filter)) ? String.Empty : " AND ";
            table = WebDAL.GetProblemVolumesSWQL(String.Format("Volumes.VolumePercentUsed > {0} {1}{2}", WarningLevel, temp, filter), this.Resource.View.LimitationID);
        }
        catch (Exception ex)
        {
            if (HasSWQLFilterError(ex, filter))
            {
                this.SQLErrorPanel.Visible = true;
                return;
            }
            throw;
        }



        if (table == null || table.Rows.Count == 0)
        {
            return;
        }

        table.Columns.Add("NodeURL", typeof(string));
        table.Columns.Add("VolumeURL", typeof(string));
        table.Columns.Add("VolumeLoadURL", typeof(string));

        // preprocessing
        foreach (DataRow r in table.Rows)
        {
            var federationPrefix = FederationUrlHelper.GetLinkPrefix(r["InstanceSiteId"].ToString());
            // node details url
            r["NodeURL"] = $"{federationPrefix}/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{r["NodeID"]}";

            // volume details
            r["VolumeURL"] = $"{federationPrefix}/Orion/NetPerfMon/VolumeDetails.aspx?NetObject=V:{r["VolumeID"]}";

            // volume URL
            r["VolumeLoadURL"] =
                $"{federationPrefix}/Orion/NetPerfMon/CustomChart.aspx?ChartName=PercentDiskUsage&NetObject=V:{r["VolumeID"]}&Period=Today";
        }

        // bind data to repeater
        this.NodesRepeater.DataSource = table;
        this.NodesRepeater.DataBind();
    }

    // item bound event
    protected void ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HtmlTableRow groupHeaderRow = e.Item.FindControl("groupHeaderRow") as HtmlTableRow;
            HtmlTableRow itemRow = e.Item.FindControl("itemRow") as HtmlTableRow;

            DataRowView row = e.Item.DataItem as DataRowView;
            bool newNode = false;
            if (row["NodeID"].ToString() != this.currentNodeId)
            {
                this.currentNodeId = row["NodeID"].ToString();
                newNode = true;
            }

            groupHeaderRow.Visible = newNode;
        }
    }

}
