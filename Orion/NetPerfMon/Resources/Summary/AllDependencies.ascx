﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllDependencies.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Summary_AllDependencies" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>

<asp:ScriptManagerProxy id="DepTreeScriptManager" runat="server">
	<Services>
		<asp:ServiceReference path="/Orion/Services/DependenciesTree.asmx" />
	</Services>
</asp:ScriptManagerProxy>

<orion:Include runat="server" File="NetPerfMon/js/Dependencies.js" />

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <script type="text/javascript">
	        //<![CDATA[
            $(function () {
                var refresh = function () { ORIONDependencies.DependenciesTree.LoadDependencies($('#<%=this.DepTree.ClientID%>'), '<%=this.Resource.ID %>', ''); };
                SW.Core.View.AddOnRefresh(refresh, '<%=this.DepTree.ClientID%>');
                refresh();
            });
	        //]]>
	    </script>
    
        <div class="Tree" ID="DepTree" runat="server">
            <asp:Literal runat="server" ID="TreeLiteral" />
        </div>
    </Content>
</orion:resourceWrapper>
