﻿using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Data;
using SolarWinds.Orion.Core.Common.Federation;


[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.IsFederationAware, "true")]
public partial class Orion_NetPerfMon_Resources_Summary_HighCPULoad : SolarWinds.Orion.Web.UI.BaseResourceControl
{
    
    #region properties

    // overriden DefaultTitle
    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_VB0_218; }
    }

    public override string SubTitle
    {
        get
        {
            return base.SubTitle.Replace("XX", Math.Round(Thresholds.CPULoadWarning.SettingValue).ToString());
        }
    }

    // overriden HelpLink
    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceNodesHighCPULoad";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/FilterEdit.ascx"; }
    }

    // overriden Edit URL
    public override String EditURL
    {
        get
        {
            return base.EditURL + "&HideInterfaceFilter=True&IsSQLMode=False";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    public int WarningLevel { get; set; }
    public int ErrorLevel { get; set; }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        var filter = this.Resource.Properties["Filter"];
        WarningLevel = Convert.ToInt32(Thresholds.CPULoadWarning.SettingValue);

        DataTable table = null;

        try
        {
            var temp = (String.IsNullOrEmpty(filter)) ? String.Empty : " AND ";
            table = WebDAL.GetProblemNodesWithoutSeveritySWQL(
                String.Format("CPULoad > {0}{1}{2}", WarningLevel, temp, filter), "CPULoad DESC, NodeName ASC",
                this.Resource.View.LimitationID);
        }
        catch (Exception ex)
        {
            if (HasSWQLFilterError(ex, filter))
            {
                this.SQLErrorPanel.Visible = true;
                return;
            }
            throw;
        }


        var cpuLoadError = Thresholds.CPULoadError.SettingValue;
        ErrorLevel = Convert.ToInt32(cpuLoadError);

        if (table == null || table.Rows.Count == 0)
        {
            return;
        }

        table.Columns.Add("NodeURL", typeof(string));
        table.Columns.Add("CPULoadURL", typeof(string));

        // preprocessing
        foreach (DataRow r in table.Rows)
        {
            var federationPrefix = FederationUrlHelper.GetLinkPrefix(r["InstanceSiteId"].ToString());
            // node details url
            r["NodeURL"] = $"{federationPrefix}/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{r["NodeID"]}";

            // chart with current CPU Load
            r["CPULoadURL"] = $"{federationPrefix}/Orion/NetPerfMon/CustomChart.aspx?ChartName=HostAvgCPULoad&NetObject=N:{r["NodeID"]}&Period=Today";
        }

        // bind data to repeater
        this.NodesRepeater.DataSource = table;
        this.NodesRepeater.DataBind();
    }

}
