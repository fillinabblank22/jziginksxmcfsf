using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Federation;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.IsFederationAware, "true")]
public partial class Orion_NetPerfMon_Resources_Summary_ContainersByCurrentStatus : BaseResourceControl
{
    protected void grid_Init(object sender, EventArgs e)
    {
        string filter = this.GetStringValue("Filter", "");
        string selectedStatus = this.GetStringValue("SelectedStatus", "");
        IEnumerable<int> statuses;
        try
        {
            statuses = (from status in selectedStatus.Split(new char[] {';'}, StringSplitOptions.RemoveEmptyEntries)
                       select Convert.ToInt32(status)).ToArray();
        }
        catch
        {
            statuses = Enumerable.Empty<int>();
        }

        var containers = from c in new ContainersDAL().GetContainersByStatus(statuses, filter)
                         select new Container(c);
        grid.DataSource = containers;
        grid.DataBind();
    }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_VB0_223; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceGroupsWithProblems"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditContainersByCurrentStatus.ascx"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
