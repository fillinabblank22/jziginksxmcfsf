<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HighDiskSpace.ascx.cs"
    Inherits="Orion_NetPerfMon_Resources_Summary_HighDiskSpace" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Swis" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Federation" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="PercentStatusBar" Src="~/Orion/Controls/PercentStatusBar.ascx" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= DefaultSanitizer.SanitizeHtml(this.CustomSqlErrorMessage) %>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="NodesRepeater" OnItemDataBound="ItemDataBound">
            <HeaderTemplate>
                <table border="0" cellpadding="2" cellspacing="0" width="100%">
            </HeaderTemplate>
            <ItemTemplate>
                <tr runat="server" id="groupHeaderRow">
                    <td class="Property" valign="middle" width="20">
                        <img alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_14) %>" src="<%# DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.UI.StatusIcons.NodeIconFactory.SmallIconURL( Eval("NodeID"), Eval("Status") )) %>" />&nbsp;
                    </td>
                    <td class="Property" valign="middle">
                        <orion:ToolsetLink  runat="server" ID="link" 
                                            IPAddress='<%# DefaultSanitizer.SanitizeHtml(Eval("IP_Address")) %>'
                                            DNS='<%# DefaultSanitizer.SanitizeHtml(Eval("DNS")) %>'
                                            SysName='<%# DefaultSanitizer.SanitizeHtml(Eval("SysName")) %>' 
                                            CommunityGUID='<%# DefaultSanitizer.SanitizeHtml(Eval("GUID")) %>'
                                            NavigateUrl='<%# DefaultSanitizer.SanitizeHtml(Eval("NodeURL")) %>'>
                        <%# DefaultSanitizer.SanitizeHtml(Eval("NodeName")) %>
                        </orion:ToolsetLink>
                    </td>
                    <td>
                    &nbsp;
                    </td>
                    <td class="PropertyHeader" visible="<%# DefaultSanitizer.SanitizeHtml(SwisFederationInfo.IsFederationEnabled) %>">
                        <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank" NavigateUrl='<%# DefaultSanitizer.SanitizeHtml(FederationUrlHelper.GetLinkPrefix((int)Eval("InstanceSiteId"))) %>'><%# DefaultSanitizer.SanitizeHtml(Eval("SiteName")) %></asp:HyperLink>
                    </td>
                </tr>
                <tr runat="server" id="itemRow">
                    <td class="Property" style="padding-left:20px">
                        <asp:Image ID="VolumeImage" runat="server" ImageUrl='<%# DefaultSanitizer.SanitizeHtml("/NetPerfMon/images/Volumes/" + Eval("VolumeTypeIcon")) %>' />
                    </td>
                    <td class="Property" style="padding-left:10px">
                        <asp:HyperLink ID="CurrentLink" runat="server" NavigateUrl='<%# DefaultSanitizer.SanitizeHtml(Eval("VolumeURL")) %>'
                            Target="_blank">
                        <%# DefaultSanitizer.SanitizeHtml(Eval("Caption")) %>
                        </asp:HyperLink>
                    </td>
                    <td class="Property" valign="middle" align="center">
                        <asp:HyperLink ID="VolumePercerUseLink" runat="server" NavigateUrl='<%# DefaultSanitizer.SanitizeHtml(Eval("VolumeLoadURL")) %>'
                            Target="_blank">
                            <orion:PercentLabel runat="server" ID="PercentLabel" Status='<%# Convert.ToInt32(Eval("VolumePercentUsed")) %>'
                                WarningLevel='<%# Convert.ToInt32(Eval("Level1Value")) %>' ErrorLevel='<%# Convert.ToInt32(Eval("Level2Value")) %>' />
                        </asp:HyperLink>
                    </td>
                    <td class="Property" valign="middle" align="left">
                        <asp:HyperLink ID="VolumeBar" runat="server" NavigateUrl='<%# DefaultSanitizer.SanitizeHtml(Eval("VolumeLoadURL")) %>'
                            Target="_blank">
                            <orion:PercentStatusBar runat="server" ID="StatusBar" Status='<%# Convert.ToInt32(Eval("VolumePercentUsed")) %>'
                                WarningLevel='<%# Convert.ToInt32(Eval("Level1Value")) %>' ErrorLevel='<%# Convert.ToInt32(Eval("Level2Value")) %>' />
                        </asp:HyperLink>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:ResourceWrapper>
