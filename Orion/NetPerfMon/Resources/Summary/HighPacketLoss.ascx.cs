﻿using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Data;
using SolarWinds.Orion.Core.Common.Federation;


[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.IsFederationAware, "true")]
public partial class Orion_NetPerfMon_Resources_Summary_HighPacketLoss : SolarWinds.Orion.Web.UI.BaseResourceControl
{
    
    #region properties

    // overriden DefaultTitle
    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_VB0_222; }
    }

    public override string SubTitle
    {
        get
        {
            return base.SubTitle.Replace("XX", Math.Round(Thresholds.PacketLossWarning.SettingValue).ToString());
        }
    }

    // overriden HelpLink
    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceNodesHighPacketLoss";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/FilterEdit.ascx"; }
    }

    // overriden Edit URL
    public override String EditURL
    {
        get
        {
            return base.EditURL + "&HideInterfaceFilter=True&IsSQLMode=False";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    public int ResponseWarningLevel { get; set; }
    public int ResponseErrorLevel { get; set; }
    public int PacketWarningLevel { get; set; }
    public int PacketErrorLevel { get; set; }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        var filter = this.Resource.Properties["Filter"];
        ResponseWarningLevel = Convert.ToInt32(Thresholds.ResponseTimeWarning.SettingValue);
        ResponseErrorLevel = Convert.ToInt32(Thresholds.ResponseTimeError.SettingValue);
        PacketWarningLevel = Convert.ToInt32(Thresholds.PacketLossWarning.SettingValue);
        PacketErrorLevel = Convert.ToInt32(Thresholds.PacketLossError.SettingValue);

        DataTable table = null;

        try
        {
            var temp = (String.IsNullOrEmpty(filter)) ? String.Empty : " AND ";
            table = WebDAL.GetProblemNodesWithoutSeveritySWQL(String.Format("{3} AND PercentLoss > {0}{1}{2}", PacketWarningLevel, temp, filter, TopXXSWQLDAL.GetNodesDataStatusFilter(string.Empty)),
                "PercentLoss DESC, AvgResponseTime DESC, NodeName ASC", this.Resource.View.LimitationID);
        }
        catch (Exception ex)
        {
            if (HasSWQLFilterError(ex, filter))
            {
                this.SQLErrorPanel.Visible = true;
                return;
            }
            throw;
        }


        if (table == null || table.Rows.Count == 0)
        {
            return;
        }

        table.Columns.Add("NodeURL", typeof(string));
        table.Columns.Add("ResponseURL", typeof(string));
        table.Columns.Add("PacketLossURL", typeof(string));

        // preprocessing
        foreach (DataRow r in table.Rows)
        {
            var federationPrefix = FederationUrlHelper.GetLinkPrefix(r["InstanceSiteId"].ToString());
            // node details url
            r["NodeURL"] = $"{federationPrefix}/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{r["NodeID"]}";

            // chart with current min, max and average response time
            r["ResponseURL"] =
                $"{federationPrefix}/Orion/NetPerfMon/CustomChart.aspx?ChartName=MMAvgRT&NetObject=N:{r["NodeID"]}&Period=Today";

            // chart with packet loss for node
            r["PacketLossURL"] =
                $"{federationPrefix}/Orion/NetPerfMon/CustomChart.aspx?ChartName=PercentLoss&NetObject=N:{r["NodeID"]}&Period=Today";
        }

        // bind data to repeater
        this.NodesRepeater.DataSource = table;
        this.NodesRepeater.DataBind();
    }

}