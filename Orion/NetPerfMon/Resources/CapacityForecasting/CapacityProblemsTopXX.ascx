<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CapacityProblemsTopXX.ascx.cs"
    Inherits="Orion_NetPerfMon_Resources_CapacityForecasting_CapacityProblemsTopXX" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Register TagPrefix="orion" TagName="ProfilingProgress" Src="~/Orion/NetPerfMon/Controls/ForecastingProfilingInProgress.ascx" %>

<orion:Include ID="Include1" runat="server" File="Formatters.js" />
<orion:Include ID="Include2" runat="server" File="Forecasting.css" />
<orion:resourceWrapper runat="server" ID="Wrapper" CssClass="sw-forecastingCapacityResource">
    <Content>
        
        <orion:CustomQueryTable runat="server" ID="CustomTable" />
        
        <script type="text/javascript">

        $(function () {
          SW.Core.Resources.CustomQuery.initialize({          
          uniqueId: <%= ScriptFriendlyResourceID %>,
          initialPage: 0,
          rowsPerPage: <%= this.maxRecords %>,
          allowSort: false,
          allowPaging: false,
          onLoad: function(rows) {
              var tableId = <%= ScriptFriendlyResourceID %>;
              var tableRows = $("#Grid-" + tableId + " tr");

              for (var i = 0; i < rows.length; i++) {
                  var warningValue = rows[i][2];
                  var criticalValue = rows[i][3];
                  var atCapacity = rows[i][4];
                  SW.Core.Formatters.capacityForecastCellFormat(warningValue,$(tableRows[i+1].cells[2]),'sw-forecast-warning');
                  SW.Core.Formatters.capacityForecastCellFormat(criticalValue,$(tableRows[i+1].cells[3]),'sw-forecast-critical');
                  SW.Core.Formatters.capacityForecastCellFormat(atCapacity,$(tableRows[i+1].cells[4]),'sw-forecast-capacity');
              }
          },
          columnSettings: {
            "NODE": {
              header: 'NODE',               
              formatter: function (value, row, cellInfo){
                return String.format('<img src="{0}" style="vertical-align: middle;"/>&nbsp;',row[5]) + '<span><a href="/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:' + row[8] + '" >' + SW.Core.Formatters.capacityForecastMakeBreakable(value) + '</a></span>';
              },
              isHtml: true
            },
            "DETAIL": {
              header: 'DETAIL',
              formatter: function (value, row, cellInfo){ 		  
                return String.format('<img src="{0}" style="vertical-align: middle;" />&nbsp;',row[6]) + '<span><a href="' + row[7] + '">' + SW.Core.Formatters.capacityForecastMakeBreakable(value) + '</a></span>';
              },
              isHtml: true
            },
            "WARNING": {
              header: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LF0_11) %>', 
              formatter: function (value, row, cellInfo){                
                  return SW.Core.Formatters.capacityForecastDateFormatWOThreshold(value);
              },
              isHtml: true
            },            
			"CRITICAL": {
              header: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LF0_12) %>', 
              formatter: function (value, row, cellInfo){
                  return SW.Core.Formatters.capacityForecastDateFormatWOThreshold(value);       
              },
              isHtml: true
            },
			"CAPACITY": {
              header: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LF0_13) %>', 
              isHtml: true,
              formatter: function (value, row, cellInfo){              
                  return SW.Core.Formatters.capacityForecastDateFormatWOThreshold(value);      
              }              
            }			
          }
        });
        var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
        SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
        refresh();

      });
        </script>        
    </Content>
</orion:resourceWrapper>
