<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeCapacityForecast.ascx.cs"
    Inherits="Orion_NetPerfMon_Resources_CapacityForecasting_NodeCapacityForecast" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Register TagPrefix="orion" TagName="ProfilingProgress" Src="~/Orion/NetPerfMon/Controls/ForecastingProfilingInProgress.ascx" %>

<orion:Include ID="Include1" runat="server" File="Formatters.js" />
<orion:Include ID="Include2" runat="server" File="Forecasting.css" />
<orion:resourceWrapper runat="server" ID="Wrapper" CssClass="sw-forecastingCapacityResource">
    <Content>
        <% if (AreDataAvailable) { %>
        <orion:CustomQueryTable runat="server" ID="CustomTable" />
        
        <script type="text/javascript">

        $(function () {
          SW.Core.Resources.CustomQuery.initialize({          
          uniqueId: <%= ScriptFriendlyResourceID %>,
          initialPage: 0,
          rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "5" %>,                              
          allowSort: true,
          columnSettings: {
            "LAST7DAYS": {
              header: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LF0_14) %>',               
              formatter: function (value, row, cellInfo){                
                var down = '';
                if (row[6] < 0)
                down = 'down';
                return   String.format('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LF0_161) %>',value) + '<br><img src="/Orion/images/ForecastingIcons/trend'+down+'.png" />&nbsp;' + row[6] + '%';
              },
              isHtml: true
            },
            "RESOURCE": {
              header: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LF0_10) %>',               
              isHtml: true
            },
            "WARNING": {
              header: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LF0_11) %>', 
              formatter: function (value, row, cellInfo){                
                return SW.Core.Formatters.capacityForecastDateFormat(value, row[7], 'sw-forecast-warning');
              },
              isHtml: true
            },            
			"CRITICAL": {
              header: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LF0_12) %>', 
              formatter: function (value, row, cellInfo){
                return SW.Core.Formatters.capacityForecastDateFormat(value, row[8], 'sw-forecast-critical');                
              },
              isHtml: true
            },
			"CAPACITY": {
              header: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LF0_13) %>', 
              formatter: function (value, row, cellInfo){              
                return SW.Core.Formatters.capacityForecastDateFormat(value, row[9], 'sw-forecast-capacity');                
              },
              isHtml: true
            }			
          },
        });
        var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
        SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
        refresh();

      });
        </script>
        <% } else { %>        
        <orion:ProfilingProgress id="profiling" runat="server" />
        <% } %>
    </Content>
</orion:resourceWrapper>
