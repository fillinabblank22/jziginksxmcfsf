﻿using SolarWinds.Orion.Web.UI;
using System;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SettingsDAL = SolarWinds.Orion.Core.Common.SettingsDAL;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_CapacityForecasting_CapacityProblemsTopXX : BaseResourceControl
{
    protected int maxRecords = 10;
    private string selectedMetrics = "";
    private const string defaultOrderClause = "CAPACITY, CRITICAL, WARNING";
    private readonly IForecastingCapacityDAL forecastingCapacityDal = new ForecastingCapacityDAL();

    // This is here to make resource working in reports. Resources in reports get negative ID assigned.
    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.Visible = AreVolumesAvailable();
        if (!Int32.TryParse(Resource.Properties["MaxRecords"], out maxRecords))
            maxRecords = 10;
        DisabledMetrics = Resource.Properties["DisabledMetrics"];
        filter = Resource.Properties["Filter"];
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // Set unique ID CustomQueryTable instance that you put to .ascx file
        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        // Set SWQL query that loads data to display
        CustomTable.SWQL = SWQL;
        // Set default custom table order
        CustomTable.OrderBy = defaultOrderClause;
    }

    private string filter = "";
    private string Filter
    {
        get { return string.IsNullOrWhiteSpace(filter) ? string.Empty : string.Format(" and {0}", filter); }
        set { filter = value; }
    }

    private string _disabledMetrics = "";
    private string DisabledMetrics
    {
        get
        {
            if (string.IsNullOrWhiteSpace(_disabledMetrics))
                return string.Empty;

            return string.Format(@" AND ForecastMetrics.Name NOT IN ('{0}')", _disabledMetrics.Replace(",", "','"));
        }
        set
        {
            _disabledMetrics = value;
        }
    }

    public string SWQL
    {
        get
        {
            // Top XX limitation is done by setting CustomQuery rowsPerPage & allowPaging = false
            // Order by is done by setting CustomTableQuery.OrderBy here in Page_Load
            string swql = String.Format(@"
            SELECT TOP {3} NODE, DETAIL, WARNING, CRITICAL, CAPACITY, _Icon_NODE, _Icon_DETAIL, _URL, _NODEID FROM
            (
            SELECT
                Nodes.Caption as [NODE],
                ForecastCapacity.InstanceCaption + ' - ' + ForecastMetrics.DisplayName as [DETAIL], 
                CASE WHEN (isnull(ForecastCapacitySettings.UsePeakValues, ForecastMetrics.UsePeakValues) = 1)
                    THEN ForecastCapacity.DaysToWarningPeak ELSE ForecastCapacity.DaysToWarningAvg END AS WARNING, 
                CASE WHEN (isnull(ForecastCapacitySettings.UsePeakValues, ForecastMetrics.UsePeakValues) = 1)
                    THEN ForecastCapacity.DaysToCriticalPeak ELSE ForecastCapacity.DaysToCriticalAvg END AS CRITICAL, 
                CASE WHEN (isnull(ForecastCapacitySettings.UsePeakValues, ForecastMetrics.UsePeakValues) = 1)
                    THEN ForecastCapacity.DaysToCapacityPeak ELSE ForecastCapacity.DaysToCapacityAvg END AS CAPACITY,
                '/Orion/images/StatusIcons/small-' + Nodes.StatusLED AS [_Icon_NODE],
                '/Orion/images/ForecastingIcons/' + ForecastMetrics.Icon as [_Icon_DETAIL], 
                ForecastCapacity.InstanceUri as [_URL],
                Nodes.NodeId as [_NODEID],
                CASE WHEN (isnull(ForecastCapacitySettings.UsePeakValues, ForecastMetrics.UsePeakValues) = 1)
                    THEN ForecastCapacity.DaysToCapacityPeak ELSE ForecastCapacity.DaysToCapacityAvg END as ConditionValue,
                MinDateTime, MaxDateTime
            FROM Orion.ForecastCapacity (nolock=true) ForecastCapacity
            JOIN Orion.ForecastMetrics (nolock=true) ForecastMetrics ON ForecastMetrics.Name = ForecastCapacity.MetricName AND ForecastMetrics.EntityType = ForecastCapacity.EntityType
            LEFT JOIN Orion.ForecastCapacitySettings (nolock=true) ForecastCapacitySettings ON ForecastCapacitySettings.InstanceId = ForecastCapacity.InstanceId AND ForecastCapacitySettings.MetricId = ForecastMetrics.Id
            JOIN Orion.Nodes (nolock=true) Nodes on ForecastCapacity.NodeID = Nodes.NodeId
            {1} {2} AND (
                ForecastCapacity.EntityType != 'Orion.Volumes'
                OR
                ForecastCapacity.InstanceId IN (
                    SELECT VolumeID FROM Orion.Volumes WHERE VolumeTypeID NOT IN ({4})
                )
            ))
            WHERE
                DayDiff(MinDateTime, MaxDateTime) >= {0} AND
                ConditionValue IS NOT NULL",
                SettingsDAL.GetCurrentInt("ForecastMinDays", 7),
                DisabledMetrics,
                CommonHelper.FormatFilter(Filter),
                maxRecords,
                string.Join(",", forecastingCapacityDal.ExcludedVolumeTypes)
            );
            return swql;
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBDATA_LM0_1; }
    }

    public override string DisplayTitle
    {
        get { return base.DisplayTitle.Replace("XX", maxRecords.ToString()); }
    }

    private bool? areDataAvailable = null;
    private TimeSpan remainingTime = new TimeSpan();
    protected TimeSpan RemainingTime
    {
        get { return remainingTime; }
    }

    protected bool AreDataAvailable()
    {
        return true;
    }

    protected bool AreVolumesAvailable()
    {
        return true;
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditForecastingTopXXCapacityProblems.ascx"; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceCapacityProblemsTopXX"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }
}
