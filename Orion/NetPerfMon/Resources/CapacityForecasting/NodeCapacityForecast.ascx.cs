﻿using System.Globalization;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SettingsDAL = SolarWinds.Orion.Core.Common.SettingsDAL;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_CapacityForecasting_NodeCapacityForecast : BaseResourceControl
{
    private const String DataSourceEntityName = "Orion.CPULoad";
    private const String MetricName = "Forecast.Metric.CpuLoad";

    private readonly IForecastingCapacityDAL forecastingCapacityDal = new ForecastingCapacityDAL();

    private int nodeId;

    // This is here to make resource working in reports. Resources in reports get negative ID assigned.
    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    protected int MinimalDays
    {
        get { return SettingsDAL.GetCurrentInt("ForecastMinDays", 7); }
    }

    protected override void OnInit(EventArgs e)
    {        
        base.OnInit(e);

        nodeId = GetNodeId();
        if (!IsPollerAssigned())
        {
            Visible = false;
        }
    }

    private bool IsPollerAssigned()
    {
        return PollerDAL.IsPollerAssigned(new[] {nodeId.ToString(CultureInfo.InvariantCulture)}, "Node.CpuAndMemory");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // Set unique ID CustomQueryTable instance that you put to .ascx file
        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        // Set SWQL query that loads data to display
        CustomTable.SWQL = SWQL;

        if (!AreDataAvailable)
        {
            profiling.Initialize(MinDateTime, DataSourceEntityName, "NodeId", nodeId);
        }
    }

    public string SWQL
    {
        get
        {
            string netObjectId = null;
            INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();
            if (nodeProvider == null) return null;

            if (nodeProvider != null)
            {
                var node = nodeProvider.Node;
                if (node != null)
                    netObjectId = node.NetObjectID;
            }

            string swql = @"SELECT m.DisplayName AS RESOURCE, 
'/Orion/images/ForecastingIcons/' + Icon AS [_IconFor_RESOURCE],
CASE WHEN (isnull(s.UsePeakValues, m.UsePeakValues) = 0) THEN
	CASE	WHEN (m.Name='Forecast.Metric.CpuLoad') THEN cpu.AvgCPULoad
		WHEN (m.Name='Forecast.Metric.PercentMemoryUsed') THEN cpu.AvgPercentMemoryUsed
        END 
ELSE
	CASE	WHEN (m.Name='Forecast.Metric.CpuLoad') THEN cpu.MaxCPULoad
		WHEN (m.Name='Forecast.Metric.PercentMemoryUsed') THEN cpu.MaxPercentMemoryUsed
        END 
END as LAST7DAYS,
CASE WHEN (isnull(s.UsePeakValues, m.UsePeakValues) = 1)  THEN c.DaysToWarningPeak ELSE c.DaysToWarningAvg END AS WARNING, 
CASE WHEN (isnull(s.UsePeakValues, m.UsePeakValues) = 1)  THEN c.DaysToCriticalPeak ELSE c.DaysToCriticalAvg END AS CRITICAL, 
CASE WHEN (isnull(s.UsePeakValues, m.UsePeakValues) = 1)  THEN c.DaysToCapacityPeak ELSE c.DaysToCapacityAvg END AS CAPACITY,
CASE WHEN (isnull(s.UsePeakValues, m.UsePeakValues) = 1)  THEN 
	CASE	WHEN (m.Name='Forecast.Metric.CpuLoad') THEN FLOOR(cpu.BCPULoadPeak)
		WHEN (m.Name='Forecast.Metric.PercentMemoryUsed') THEN FLOOR(BMemoryLoadPeak)
        END
ELSE
	CASE	WHEN (m.Name='Forecast.Metric.CpuLoad') THEN FLOOR(cpu.BCPULoadAvg)
		WHEN (m.Name='Forecast.Metric.PercentMemoryUsed') THEN FLOOR(cpu.BMemoryLoadAvg)
        END
END AS [_TRENDSLOPE],
c.WarningThreshold as [_WARNINGTHRESHOLD],
c.CriticalThreshold as [_CRITICALHRESHOLD],
c.CapacityThreshold as [_CAPACITYHRESHOLD]

FROM Orion.ForecastCapacity (nolock=true) c
LEFT JOIN Orion.ForecastMetrics (nolock=true) m ON m.Name = c.MetricName AND m.EntityType = c.EntityType
LEFT JOIN Orion.ForecastCapacitySettings (nolock=true) s ON s.InstanceId = c.InstanceId AND s.MetricId = m.Id
LEFT JOIN
(
   SELECT
	NodeID
	,((Sum_AvgCPULoadxDays - (SumAvgCPULoad * SumDays/nRows))/(Sum_SquareDays - ((SumDays/nRows)*SumDays))) AS BCPULoadAvg
	,(Sum_MaxCPULoadxDays - (SumMaxCPULoad * SumDays/nRows))/(Sum_SquareDays - ((SumDays/nRows)*SumDays)) AS BCPULoadPeak
	,((Sum_AvgMemoryLoadxDays - (SumAvgMemoryLoad * SumDays/nRows))/(Sum_SquareDays - ((SumDays/nRows)*SumDays))) AS BMemoryLoadAvg
	,(Sum_MaxMemoryLoadxDays - (SumMaxMemoryLoad * SumDays/nRows))/(Sum_SquareDays - ((SumDays/nRows)*SumDays)) AS BMemoryLoadPeak
	,AvgCPULoad
        ,MaxCPULoad
	,AvgPercentMemoryUsed
        ,MaxPercentMemoryUsed
	FROM(
	  SELECT  
	   NodeID,
	   Count(*) AS nRows,
	   SUM(ndays) * 1.00 AS SumDays,
	   SUM(ndays * ndays) * 1.00 AS Sum_SquareDays,
	   SUM(AvgLoad) * 1.00 AS SumAvgCPULoad,
           SUM(ndays*AvgLoad) * 1.00 AS Sum_AvgCPULoadxDays,
	   SUM(MaxLoad) * 1.00 AS SumMaxCPULoad,
           SUM(ndays*MaxLoad) * 1.00 AS Sum_MaxCPULoadxDays,
           SUM(AvgMemUsed) * 1.00 AS SumAvgMemoryLoad,
           SUM(ndays*AvgMemUsed) * 1.00 AS Sum_AvgMemoryLoadxDays,
	   SUM(MaxMemUsed) * 1.00 AS SumMaxMemoryLoad,
           SUM(ndays*MaxMemUsed) * 1.00 AS Sum_MaxMemoryLoadxDays,
	   FLOOR(AVG(AvgLoad)) AS AvgCPULoad,
           FLOOR(MAX(MaxLoad)) AS MaxCPULoad,
	   FLOOR(AVG(AvgMemUsed)) AS AvgPercentMemoryUsed,
           FLOOR(MAX(MaxMemUsed)) AS MaxPercentMemoryUsed
	   FROM (
		SELECT
	        NodeID
		,DayDiff(GETDATE(), DateTime) as ndays
		,AvgLoad
		,MaxLoad
		,AvgPercentMemoryUsed AS AvgMemUsed
		,100 * MaxMemoryUsed/(ISNULL(TotalMemory, NULL)) AS MaxMemUsed
		FROM Orion.CPULoad (NOLOCK = true)
	        WHERE DayDiff(DateTime, GETDATE()) <= 7			
	   ) daily
	   GROUP BY NodeID
	) data WHERE (SumDays >0 OR SumDays <-1) AND nRows > 1
) cpu ON c.InstanceId=cpu.NodeID
";
            if (netObjectId != null)
            {

                string[] netObject = netObjectId.Split(new char[] { ':' });
                if (netObject.Length == 2)
                {
                    string swisType = NetObjectFactory.GetSWISTypeByNetObject(netObject[0]);
                    swql += string.Format(" WHERE c.EntityType = '{0}' AND c.InstanceId = {1}  ", swisType, netObject[1]);
                }
            }
            return swql;
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBDATA_LF0_15; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }

    private bool minDateLoaded;
    private DateTime? minDateTime;
    private DateTime? MinDateTime
    {
        get
        {
            if (!minDateLoaded)
            {
                minDateTime = GetMinDateTime();
                minDateLoaded = true; // null is a valid value too
            }

            return minDateTime;
        }
    }
        
    private int GetNodeId()
    {
        int retVal = 0;
        INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();

        if (nodeProvider != null)
        {
            var netobject = nodeProvider.Node;

            if (netobject != null)
            {
                string netObjectId = netobject.NetObjectID;
                string[] netObject = netObjectId.Split(new char[] { ':' });

                if (netObject.Length == 2)
                {
                    retVal = Int32.Parse(netObject[1]);
                }
            }
        }

        return retVal;
    }

    private bool? areDataAvailable;
    protected bool AreDataAvailable
    {
        get
        {
            if (!areDataAvailable.HasValue)
            {
                areDataAvailable = MinDateTime.HasValue && MinDateTime <= DateTime.UtcNow.AddDays(-MinimalDays);
            }

            return areDataAvailable.Value;
        }
    }

    private DateTime? GetMinDateTime()
    {
        if (nodeId != 0)
        {
            var dateRange = forecastingCapacityDal.GetDataAvailableRangeForNode(MetricName, nodeId);
            return dateRange == null ? (DateTime?)null : dateRange.Item1;
        }

        return null;
    }


    public override string EditControlLocation
    {
        get
        {
            return "/Orion/NetPerfMon/Controls/EditResourceControls/EditForecastingThresholds.ascx";
        }
    }
        
    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceNodeCapacityForecast"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.Ajax;
        }
    }

}