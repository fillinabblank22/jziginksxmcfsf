﻿using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Data;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SettingsDAL = SolarWinds.Orion.Core.Common.SettingsDAL;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_CapacityForecasting_VolumeCapacityForecast : BaseResourceControl
{
    private class VolumeContext
    {
        public string EntityName { get; set; }
        public int EntityID { get; set; }
        public bool IsVolume { get { return EntityName.Equals("Volume"); } }
    }

    private VolumeContext displayedEntity;
    private VolumeContext DisplayedEntity
    {
        get
        {
            if (displayedEntity != null)
                return displayedEntity;

            string entityName = "Node";
            string netObjectId = string.Empty;
            IVolumeProvider volumeProvider = GetInterfaceInstance<IVolumeProvider>();
            if (volumeProvider != null)
            {
                entityName = "Volume";
                netObjectId = volumeProvider.Volume != null ? volumeProvider.Volume.NetObjectID : string.Empty;
            }
            else
            {
                INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();
                if (nodeProvider != null)
                    netObjectId = nodeProvider.Node != null ? nodeProvider.Node.NetObjectID : string.Empty;
            }

            string[] netObject = netObjectId.Split(new char[] { ':' });
            return displayedEntity = new VolumeContext
            {
                EntityName = entityName,
                EntityID = netObject.Length >= 2 ? Convert.ToInt32(netObject[1]) : 0
            };
        }
    }

    private const String DataSourceEntityName = "Orion.VolumeUsageHistory";
    private const String MetricName = "Forecast.Metric.PercentDiskUsed";

    private readonly IForecastingCapacityDAL forecastingCapacityDal = new ForecastingCapacityDAL();

    // This is here to make resource working in reports. Resources in reports get negative ID assigned.
    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    protected int MinimalDays
    {
        get { return SettingsDAL.GetCurrentInt("ForecastMinDays", 7); }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        Visible = AreVolumesAvailable();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // Set unique ID CustomQueryTable instance that you put to .ascx file
        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        // Set SWQL query that loads data to display
        CustomTable.SWQL = SWQL;

        // Initialize profiling
        if (!AreDataAvailable)
        {
            profiling.Initialize(MinDateTime, DataSourceEntityName, DisplayedEntity.EntityName + "ID", DisplayedEntity.EntityID);
        }
    }

    public string SWQL
    {
        get
        {
            string swql = @"SELECT v.Caption AS RESOURCE, 
'/NetPerfMon/images/Volumes/' + V.Icon AS [_IconFor_RESOURCE],
CASE WHEN (isnull(s.UsePeakValues, m.UsePeakValues) = 0) THEN
	Last7Days.AvgDiskUsage
ELSE
	Last7Days.MaxDiskUsage    
END as LAST7DAYS,
CASE WHEN (isnull(s.UsePeakValues, m.UsePeakValues) = 1)  THEN c.DaysToWarningPeak ELSE c.DaysToWarningAvg END AS WARNING, 
CASE WHEN (isnull(s.UsePeakValues, m.UsePeakValues) = 1)  THEN c.DaysToCriticalPeak ELSE c.DaysToCriticalAvg END AS CRITICAL, 
CASE WHEN (isnull(s.UsePeakValues, m.UsePeakValues) = 1)  THEN c.DaysToCapacityPeak ELSE c.DaysToCapacityAvg END AS CAPACITY,
CASE WHEN (isnull(s.UsePeakValues, m.UsePeakValues) = 1)  THEN FLOOR(Last7Days.Bpeak) ELSE FLOOR(Last7Days.Bavg) END AS [_TRENDSLOPE],
v.VolumeId as _VolumeId,
c.WarningThreshold as [_WARNINGTHRESHOLD],
c.CriticalThreshold as [_CRITICALHRESHOLD],
c.CapacityThreshold as [_CAPACITYHRESHOLD]
FROM Orion.ForecastCapacity (nolock=true) c
LEFT JOIN Orion.ForecastMetrics (nolock=true) m ON m.Name = c.MetricName AND m.EntityType = c.EntityType
LEFT JOIN Orion.ForecastCapacitySettings (nolock=true) s ON s.InstanceId = c.InstanceId AND s.MetricId = m.Id
LEFT JOIN Orion.Volumes V on V.VolumeId = c.InstanceId and VolumeTypeId NOT IN ({2})
LEFT JOIN (
	SELECT
	VolumeID
	,((Sum_AvgLoadxDays - (SumAvgLoad * SumDays/nRows))/(Sum_SquareDays - ((SumDays/nRows)*SumDays))) AS Bavg
	,(Sum_MaxLoadxDays - (SumMaxLoad * SumDays/nRows))/(Sum_SquareDays - ((SumDays/nRows)*SumDays)) AS Bpeak
	,AvgDiskUsage
	,MaxDiskUsage
	FROM(
	SELECT  
	   VolumeID,
	   Count(*) AS nRows,
	   SUM(ndays) * 1.00 AS SumDays,
	   SUM(ndays * ndays) * 1.00 AS Sum_SquareDays,
	   SUM(AvgLoad) * 1.00 AS SumAvgLoad,
           SUM(ndays*AvgLoad) * 1.00 AS Sum_AvgLoadxDays,
	   SUM(MaxLoad) * 1.00 AS SumMaxLoad,
           SUM(ndays*MaxLoad) * 1.00 AS Sum_MaxLoadxDays,
	   FLOOR(AVG(AvgLoad)) AS AvgDiskUsage,
           FLOOR(MAX(MaxLoad)) AS MaxDiskUsage
	   FROM (
			SELECT VolumeID, ndays, AVG(AvgLoad) AS AvgLoad, MAX(MaxLoad) as MaxLoad
			FROM
			(SELECT
						VolumeID,
					DayDiff(GETDATE(), DateTime) as ndays,
						PercentDiskUsed as AvgLoad,
						(100 * MaxDiskUsed / CASE WHEN DiskSize=0 THEN 1 ELSE DiskSize END) as MaxLoad
					FROM Orion.VolumeUsageHistory (NOLOCK = true)
						WHERE DayDiff(DateTime, GETDATE()) <= 7) source
			GROUP BY VolumeID, ndays
	   ) daily
	   GROUP BY VolumeID
	) data WHERE (SumDays >0 OR SumDays <-1) AND nRows > 1
) Last7Days ON v.VolumeID=Last7Days.VolumeID
WHERE c.EntityType = 'Orion.Volumes' AND V.{0}ID = {1}";

            return string.Format(swql, DisplayedEntity.EntityName, DisplayedEntity.EntityID, string.Join(",", forecastingCapacityDal.ExcludedVolumeTypes));
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBDATA_LF0_16; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(INodeProvider) }; }
    }

    private bool minDateLoaded;
    private DateTime? minDateTime;
    private DateTime? MinDateTime
    {
        get
        {
            if (!minDateLoaded)
            {
                minDateTime = GetMinDateTime();
                minDateLoaded = true; // null is a valid value too
            }

            return minDateTime;
        }
    }

    protected bool AreDataAvailable
    {
        get { return MinDateTime.HasValue && MinDateTime <= DateTime.UtcNow.AddDays(-MinimalDays); }
    }

    private DateTime? GetMinDateTime()
    {
        Tuple<DateTime, DateTime> dateRange = null;
        if (DisplayedEntity.IsVolume)
        {
            dateRange = forecastingCapacityDal.GetDataAvailableRange(MetricName, DisplayedEntity.EntityID);
        }
        else
        {
            dateRange = forecastingCapacityDal.GetDataAvailableRangeForNode(MetricName, DisplayedEntity.EntityID);
        }

        return dateRange == null ? (DateTime?)null : dateRange.Item1;
    }

    protected bool AreVolumesAvailable()
    {
        string query = string.Format(
            @"SELECT TOP 1 NodeID AS cnt FROM Orion.Volumes WHERE {0}ID = @entityId AND VolumeTypeId NOT IN ({1})",
            DisplayedEntity.EntityName,
            string.Join(",", forecastingCapacityDal.ExcludedVolumeTypes)
        );

        DataTable results;
        using (var swis = InformationServiceProxy.CreateV3())
        {
            var queryParams = new Dictionary<string, object>();
            queryParams[@"entityId"] = DisplayedEntity.EntityID;

            results = swis.Query(query, queryParams);
        }
        return results != null && results.Rows.Count > 0;
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditForecastingVolumesThresholds.ascx"; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceVolumeCapacityForecast"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }
}
