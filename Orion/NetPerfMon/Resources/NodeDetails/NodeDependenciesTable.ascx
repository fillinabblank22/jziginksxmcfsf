﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeDependenciesTable.ascx.cs"
    Inherits="Orion_NetPerfMon_Resources_NodeDetails_NodeDependenciesTable" %>
<%@ Register TagPrefix="orion" TagName="TaskBaseControl" Src="../TaskBaseControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="RepeaterControl" Src="~/Orion/Nodes/Controls/DependecyRepeater.ascx" %>    

<script type="text/javascript">
    function AutoHide(result, dataDiv) {
        if (result) {
            var targetDiv = $("#" + dataDiv);
            targetDiv.empty();
            targetDiv.append(result);
            targetDiv.show('slow');
        } else {
            $("#" + dataDiv).parents('.ResourceWrapper:first').css('display', 'none').addClass("HiddenResourceWrapper");

        }
    } 
</script>
<orion:resourceWrapper runat="server" ID="Wrapper">   
    <HeaderButtons>
         <orion:TaskBaseControl runat="server" ID="wSAsyncExecuteTask" />
    </HeaderButtons>
  
    <Content>
        <div id="dataDiv" runat="server" ></div>              
    </Content>
</orion:resourceWrapper>
