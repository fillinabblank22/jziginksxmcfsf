﻿using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Web;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_NetPerfMon_Resources_NodeDetails_NodeActions : BaseResourceControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }
    }
    
    protected void Page_Load(object sender, EventArgs e)
	{
		if (this.GetInterfaceInstance<INodeProvider>().Node == null)
		{
			this.nodeError.Visible = true;
			this.detailsTable.Visible = false;
		}

        object showToolset = HttpContext.Current.Profile.GetPropertyValue("ToolsetIntegration");
        if (showToolset == null || !(bool)showToolset )
        {
            this.accessDenied.Visible = true;
            this.detailsTable.Visible = false;
        }
	}

    protected override string DefaultTitle
	{
		get { return Resources.CoreWebContent.WEBCODE_VB0_30; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceNodeActions"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Static; }
    }

}
