<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SystemOID.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeDetails_SystemOID" %>

<orion:resourceWrapper ID="ResourceWrapper1" runat="server">
    <Content>
        <table width="100%" border="0" cellpadding="2" cellspacing="0">
            <tr>
                <td class="Property" width="10">&nbsp;</td>
	            <td runat="server" id="nodeError" class="PropertyHeader" style="color:Red;"><%# DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_181) %></td>
    			<td runat="server" id="nodeOID"  class="PropertyHeader"><%# DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_182) %> </td>
            </tr>
        </table>
     </Content>
</orion:resourceWrapper>