﻿using System;
using System.Data;
using System.IO;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.PackageManager;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Dependencies;
using SolarWinds.Orion.Web.Model;
using SolarWinds.Orion.Web.UI;
using System.ServiceModel;
using System.Web.UI;
using SolarWinds.Orion.Web;

public partial class Orion_NetPerfMon_Resources_NodeDetails_NodeDependenciesTable : BaseResourceControl
{
    private const string object_type = "Orion.Nodes";
    public TaskResult DataResultState { get; set; }

    public string NetObjectId
    {
        get
        {
            INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();
            if (nodeProvider != null)
            {
                Node node = nodeProvider.Node;
                return node != null ? node.NetObjectID : Request["netobject"] ?? "";
            }

            return string.Empty;
        }
    }

    protected bool AutoHide
    {
        get
        {
            if (string.IsNullOrEmpty(Resource.Properties["AutoHide"]))
                return false;
            return Resource.Properties["AutoHide"].Equals("1");
        }
    }

    protected override string DefaultTitle
    {
        get { return String.Format(Resources.CoreWebContent.WEBCODE_IB0_14, "${Caption}"); }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceNodeDependencies"; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditDependencies.ascx"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected override void OnInit(EventArgs e)
    {
        Wrapper.ManageButtonText = Resources.CoreWebContent.ResourcesAll_ManageDependencies;
        Wrapper.ManageButtonTarget = "~/Orion/Admin/DependenciesView.aspx";
        Wrapper.ShowManageButton = Profile.AllowAdmin || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
       
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var parentFilter = string.IsNullOrEmpty(Resource.Properties["ParentFilter"]) ? string.Empty : Resource.Properties["ParentFilter"];
        var childFilter = string.IsNullOrEmpty(Resource.Properties["ChildFilter"]) ? string.Empty : Resource.Properties["ChildFilter"];
        var dependencyFilter = string.IsNullOrEmpty(Resource.Properties["DependencyFilter"]) ? string.Empty : Resource.Properties["DependencyFilter"];
	
	    Context.Items[typeof(SolarWinds.Orion.Web.ViewInfo).Name] = Resource.View; // FB128666
        // Firing asynchronous rendering content via WS WSAsyncExecuteTasks
        wSAsyncExecuteTask.ExecutionList.Add(new TaskExecutionEntity()
        {
            HtmlServerMethod = this.GetNodeDependencies,
            Parameters = new object[] { NetObjectId, AutoHide, parentFilter, childFilter, dependencyFilter, Resource.ID },
            ClientObject = dataDiv,
            ClientMethod = "AutoHide"
        });

    }

    // This method will be called from WS WSAsyncExecuteTasks method GetTaskResult
    [WSAsyncExecuteMethod(true)]
    public string GetNodeDependencies(object[] parameters)
    {
        // Parameters indexes, just to avoid magic numbers
        const int netObjectIdIndex = 0;
        const int autoHideIndex = 1;
        const int parentFilterIndex = 2;
        const int childFilterIndex = 3;
        const int dependencyFilterIndex = 4;
        const int resourceIDIndex = 5;

        int objectId = 0;
        string output = String.Empty;
	    int resourceId = Convert.ToInt32(parameters[resourceIDIndex]);
	    SolarWinds.Orion.Web.ResourceInfo resource = SolarWinds.Orion.Web.ResourceManager.GetResourceByID(resourceId);
        if (resource != null)
        {
            Context.Items[typeof(SolarWinds.Orion.Web.ViewInfo).Name] = resource.View;
        }

        if (int.TryParse(NetObjectHelper.GetObjectID(parameters[netObjectIdIndex].ToString()), out objectId))
        {
            bool autoHide;
            bool.TryParse(parameters[autoHideIndex].ToString(), out autoHide);

            string _parentFilter = parameters[parentFilterIndex].ToString();
            string _childFilter = parameters[childFilterIndex].ToString();
            string _dependencyFilter = parameters[dependencyFilterIndex].ToString();
            var filter = $"{_parentFilter} {_childFilter} {_dependencyFilter}";

            try
            {
                DependencyCache.UpdateDependencyCache(parameters[netObjectIdIndex].ToString(), _parentFilter, _childFilter, _dependencyFilter);

                // hide resource if there aren't any data)
                if (autoHide)
                {
                    using (var dal = new DependenciesDAL())
                        if (dal.GetAllDependenciesFilteredCount(objectId, object_type, null, null, null) == 0)
                            return String.Empty;
                }

                DataTable sourceDataTable = null;
                DataTable groupDataTable = null;

                using (DependenciesDAL dal = new DependenciesDAL())
                {
                    sourceDataTable = dal.GetAllDependenciesFiltered(objectId, object_type, _parentFilter, _childFilter, _dependencyFilter);
                    groupDataTable = dal.GetAllDependenciesFiltered("Orion.Groups", _parentFilter, _childFilter, _dependencyFilter);
                }

                if (sourceDataTable != null && groupDataTable != null)
                {
                    foreach (DataRow row in groupDataTable.Rows)
                    {
                        int depId = int.Parse(row["DependencyId"].ToString());
                        if (DependencyCache.GroupDependenciesContains(parameters[netObjectIdIndex].ToString(), depId, row["ParentUri"].ToString(), row["ChildUri"].ToString()))
                            sourceDataTable.ImportRow(row);
                    }

                    sourceDataTable.DefaultView.Sort = "Name Asc";

                    // Control DependencyRepeater is responsible for displaying DependencyTable
                    Control control = LoadControl(@"~\Orion\Nodes\Controls\DependecyRepeater.ascx");
                    var dependencyTable = control as ASP.orion_nodes_controls_dependecyrepeater_ascx;

                    if (dependencyTable != null)
                    {
                        dependencyTable.BindData(sourceDataTable);

                        using (StringWriter stringWriter = new StringWriter())
                        {
                            using (HtmlTextWriter htmlTextWriter = new HtmlTextWriter(stringWriter))
                            {
                                dependencyTable.RenderControl(htmlTextWriter);
                                htmlTextWriter.Flush();
                                output = htmlTextWriter.InnerWriter.ToString();
                            }
                        }
                    }
                }

                DataResultState = new TaskResult() { Data = null, ErrorMessage = String.Empty, Outcome = TaskOutcome.Success };
            }
            catch (Exception ex)
            {
                var errMessage = HasSWQLFilterError(ex, filter) ? string.Format(Resources.CoreWebContent.WEBCODE_VB0_32, ex.Message, filter) : ex.Message;
                DataResultState = new TaskResult() { Data = null, ErrorMessage = errMessage, Outcome = TaskOutcome.Error };
            }
        }

        return output;
    }
}