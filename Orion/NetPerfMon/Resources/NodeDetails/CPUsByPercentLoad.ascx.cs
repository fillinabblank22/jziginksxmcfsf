﻿using System;
using SolarWinds.Orion.NPM.Web;
using System.Data;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using SettingsDAL = SolarWinds.Orion.Web.DAL.SettingsDAL;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

public partial class Orion_NetPerfMon_Resources_NodeDetails_CPUsByPercentLoad : SolarWinds.Orion.Web.UI.BaseResourceControl
{
	private static readonly Log _log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
	private Node _node;

    private bool isVisible()
    {
        return Node!=null && Node.CPULoad >= 0;
    }

	public bool IsResourceShownAtReportsPage
	{
		get
		{
			return Request.Url.AbsolutePath.ToLowerInvariant().Contains("/orion/reports/") || Request.Url.AbsolutePath.ToLowerInvariant().Contains("/orion/report.aspx");
		}
	}

	#region Thresholds
	private static OrionSetting CPULoadError { get { return SettingsDAL.GetSetting("NetPerfMon-CPULoad-Error"); } }
	private static OrionSetting CPULoadWarning { get { return SettingsDAL.GetSetting("NetPerfMon-CPULoad-Warning"); } }
	#endregion

	public int TopXX
	{
		get;
		set;
	}

	public int PageSize
	{
		get;
		set;
	}

	public int PageNumber
	{
		get
		{
			int pageIntex = 0;
			int.TryParse(hfPageIndex.Value, out pageIntex);
			return pageIntex;
		}
		set
		{
			hfPageIndex.Value = value.ToString();
		}
	}

	public int ItemsCount
	{
		get;
		set;
	}

	public bool IsShowedAll
	{
		get
		{
			bool val;
			bool.TryParse(hfShowAll.Value, out val);
			return val;
		}
		set
		{
			hfShowAll.Value = value.ToString();
		}
	}

	public Node Node
	{
		get
		{
			if (_node == null)
			{
				var nodeProvider = GetInterfaceInstance<INodeProvider>();
				if (nodeProvider != null) _node = nodeProvider.Node;
			}
			return _node;
		}
	}

	protected void BusinessLayerExceptionHandler(Exception ex)
	{
		_log.Error(ex);
	}

	public override void DataBind()
	{
		if (Node != null)
		{
			if (IsShowedAll)
			{
				PageNumber = 0;
				PageSize = ItemsCount;
			}

			DataTable dt;
            using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
				dt = proxy.GetNodeCPUsByPercentLoad(Node.NodeID, PageNumber + 1, PageSize);

			resourceTable.DataSource = dt;
			resourceTable.DataBind();
		}
	}

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (isVisible())
        {
            var topXX = Resource.Properties["TopXX"];
            var autoHide = Resource.Properties["AutoHide"];
            TopXX = (!string.IsNullOrEmpty(topXX)) ? int.Parse(topXX) : 20;

            PageSize = TopXX;

            SetItemsCount();

            if (ItemsCount <= 1
                && !string.IsNullOrEmpty(autoHide)
                && autoHide.Equals("1", StringComparison.OrdinalIgnoreCase))
                this.Visible = false;

	        if (!IsResourceShownAtReportsPage)
	        {
		        if (ItemsCount <= PageSize)
			        gridFooter.Visible = false;

		        gridShowLess.Visible = IsShowedAll;
		        gridFooter.Visible = !IsShowedAll;
	        }
	        else
	        {
		        IsShowedAll = true;
		        gridFooter.Visible = false;
		        gridShowLess.Visible = false;
	        }

	        DataBind();
        }
        else
        {
            base.Visible = false;
        }
    }

	protected override string DefaultTitle
	{
		get { return Resources.CoreWebContent.WEBCODE_IB0_1; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceCPUsByPercentLoad"; }
	}
    
	public override string EditControlLocation
	{
		get
		{
			return "/Orion/NetPerfMon/Controls/EditResourceControls/EditCPUsByPercentLoad.ascx";
		}
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

	public string FormatCPULoad(object value)
	{
		try
		{
			return FormatHelper.GetPercentFormat(Convert.ToDouble(value), CPULoadError.SettingValue, CPULoadWarning.SettingValue);
		}
		catch
		{
			return String.Empty;
		}
	}

	public string FormatCPULoadBar(object value)
	{
		try
		{
			return FormatHelper.GetPercentBarFormat(Convert.ToDouble(value), CPULoadError.SettingValue, CPULoadWarning.SettingValue);
		}
		catch
		{
			return String.Empty;
		}
	}

	private void SetItemsCount()
	{
		using (var dal = new WebDAL())
			ItemsCount = (Node != null) ? dal.GetNodeCPUIndexCount(Node.NodeID) : 0;
	}

	protected void Grid_IndexChanging(object sender, CommandEventArgs e)
	{
		switch (e.CommandArgument.ToString().ToLowerInvariant())
		{
			case "next":
				SetItemsCount();
				if (PageNumber < (double)ItemsCount / PageSize - 1)
					PageNumber += 1;
				break;
			case "prev":
				SetItemsCount();
				if (PageNumber > 0)
					PageNumber -= 1;
				break;
			case "all":
				SetItemsCount();
				PageNumber = 0;
				PageSize = ItemsCount;
				IsShowedAll = true;
				gridFooter.Visible = false;
				gridShowLess.Visible = true;
				break;
			case "less":
				SetItemsCount();
				PageNumber = 0;
				IsShowedAll = false;
				PageSize = TopXX;
				gridShowLess.Visible = false;
				gridFooter.Visible = true;
				break;
		}

		DataBind();
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}
}
