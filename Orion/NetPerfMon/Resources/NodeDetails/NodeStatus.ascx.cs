﻿using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_NodeDetails_NodeStatus : BaseResourceControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }
    }

	protected void Page_Load(object sender, EventArgs e)
	{
		Node node = this.GetInterfaceInstance<INodeProvider>().Node;
		bool nodeOk = node != null;
		this.nodeError.Visible = !nodeOk;
		this.nodeStatus.Visible = nodeOk;
		this.nodeDescription.Visible = nodeOk;

		if (nodeOk)
		{
			this.description.Text = node.StatusDescription;
			this.GroupStatus.ImageUrl = SolarWinds.Orion.Web.UI.StatusIcons.NodeIconFactory.LargeIconURL( node.NodeID, (int) node.Status.ParentStatus.Value );
		}
	}

	public override string HelpLinkFragment
	{
		get
		{
			return "OrionPHResourceNodeStatus";
		}
	}

	protected override string DefaultTitle
	{
		get { return Resources.CoreWebContent.WEBDATA_VB0_122; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
