<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeDetails.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeDetails_NodeDetails" %>
<%@ Register TagPrefix="orion" TagName="ProgressDialog" Src="~/Orion/Controls/ProgressDialog.ascx" %>
<%@ Register TagPrefix="orion" TagName="AlertSuppressionStatusDescription" Src="~/Orion/Controls/AlertSuppressionStatusDescription.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:ProgressDialog ID="ProgressDialog" runat="server" />
        <orion:AlertSuppressionStatusDescription runat="server" />
        <script type="text/javascript">
            $(function () {
                SW.Core.namespace("SW.Core.NodeManagement");
                SW.Core.NodeManagement.UsePolledStatusAgain = function(nodeId) {
                    var serverMethod = function(args, onSuccess, onFailure) {
                        SW.Core.Services.callWebService("/Orion/Services/NodeManagement.asmx",
                            "UsePolledStatus",
                            {
                                netObjectIds: SW.Core.String.Format("N:{0}", nodeId)
                            }, function(success) {
                                onSuccess(success);
                            },
                            function(failure) {
                                onFailure(failure);
                                setTimeout(function() {
                                    $(".StatusDialog").dialog('destroy').remove();
                                }, 1000);
                            }
                        );
                    };

                    SW.Core.MessageBox.ProgressDialog({
                        title: "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBJS_PS0_1) %>", // WEBJS_AV_01
                        items: [SW.Core.String.Format("N:{0}", nodeId)],
                        serverMethod: serverMethod,
                        sucessMessage: "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBJS_PS0_2) %>", // will be given to resx
                    getArg: function(id) { return [id]; },
                    failMessage: "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBJS_PS0_3) %>",
                    afterSucceeded: function(id) {
                        setTimeout(function() {
                            window.location.reload(true);
                        }, 100);
                    }
                    });
                };

                var processResults = function(result, planDescription) {
                    var source = $('#nodeDetailsTemplate').html();
                    result.MaintenancePlanDescription = planDescription;
                    var newHtml = _.template(source, result);
                    var $appendedHtml = $('#<%= resourceContent.ClientID %>').html(newHtml);                 

                var isNodeManager = '<%= Profile.AllowNodeManagement %>'.toLowerCase() == 'true';
                    if (!result.CustomStatus || !isNodeManager)
                        $appendedHtml.find("a.UsePolledStatusAgainLink").hide();
                };

                var refresh = function () {
            
                    var detailsPromise = $.Deferred();
                    var planPromise = SW.Core.MaintenanceMode.GetStatusDescription('<%=SwisUri %>', <%=IsDemo.ToString().ToLower() %>, <%=Profile.AllowUnmanage.ToString().ToLower() %>);

                    $.when(detailsPromise, planPromise).done(processResults);
            
                    SW.Core.Services.callWebService(
                        "/Orion/Services/AsyncResources.asmx",
                        "GetNodeDetails",
                        { 
                            nodeId: <%= NodeID %>,
                        viewLimitationId: <%= ViewLimitationId %>
                        },
                    detailsPromise.resolve
                );
            };
                SW.Core.View.AddOnRefresh(refresh, '<%= resourceContent.ClientID %>');
                refresh();
            });

            CancelPlannedOutage = function() {
            <%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {%>
            demoAction("Core_Cancel_Of_Planned_Maintenance", this);
            return false;
            <%}%>	
            if (confirm('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VS1_9) %>')) {

                SW.Core.Services.callWebService(
                       "/Orion/Services/NodeManagement.asmx",
                       "RemanageNodes",
                       {nodeIds: [<%= this.NodeID %>]},
                    function () { window.location.reload(); },
			        function (error) {
			            alert(error.get_message() + '\n' + error.get_stackTrace());
			        }
                );
                   }
        };
        </script>

        <script id="nodeDetailsTemplate" type="text/x-template">
        <div style="height: 5px;">&nbsp;</div>
        <table class="NeedsZebraStripes" border="0" cellPadding="2" cellSpacing="0" width="100%">          
	        <tr>
              <td class="Property" width="10">&nbsp;</td>
              <td class="PropertyHeader" vAlign="center"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_122) %></td>
              <td align="center" class="Property"><img class="StatusIcon" alt="{{ NodeStatusAltText }}" src="{{ NodeStatusImagePath }}" /></td>
              <td class="Property">
                <div>
                     <a href="/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{{ NodeID }}" class="NetObjectLink">{{ NodeStatusDescription }}</a>
                     <span style ="{{ StatusReasonStyle }}" class="property-smaller" >( <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AJ0_01) %> )</span>
                     &nbsp;<a style="color: #336699;cursor: pointer;" class="UsePolledStatusAgainLink" onclick="SW.Core.NodeManagement.UsePolledStatusAgain({{ NodeID }})"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AV0_O1) %></a>
                     &nbsp;
                </div>
                <div style="{{ UnmanageReasonStyle }}" class="property-smaller">{{ UnmanageReason }}</div>
                <div style="{{ UnmanageInfoStyle }}" class="property-smaller blueLink">
                    {{ UnmanageInfo }}&nbsp;
                    <span style="{{ UnmanageCancelStyle }}" class="property-smaller blueLink">
                    <a href="#" onclick="CancelPlannedOutage(); return false;">{{ UnmanageCancel }}</a>
                    </span>
                </div>
                <div id="maintenancePlan-<%= Resource.ID %>" class="property-smaller blueLink">
                    {{ MaintenancePlanDescription }}
                </div>
              </td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TP0_1) %></td>
	          <td class="Property">&nbsp;</td>
              <td class="Property"><a href="/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{{ NodeID }}" class="NetObjectLink">{{ IPAddressString }}</a>&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_123) %></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property">{{ DynamicIPString }}&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" nowrap><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_16) %></td>
	          <td align="center" class="Property"><img src="/NetPerfMon/Images/Vendors/{{ VendorIcon }}" onerror="this.src='/NetPerfMon/images/Vendors/Unknown.gif'; return true;" /></td>
	          <td class="Property">{{ MachineType }}&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VT0_9) %></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property">{{ EffectiveCategory }}&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_126) %></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property">{{ DNSName }}&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" nowrap><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_127) %></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property">{{ SysName }}&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_15) %></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property">{{ Description }}&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_128) %></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property">{{ Location }}&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_129) %></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property">{{ Contact }}&nbsp;</td>
	        </tr>
	        <tr>
              <td class="Property" width="10">&nbsp;</td>
              <td class="PropertyHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_10) %></td>
              <td class="Property">&nbsp;</td>
              <td class="Property">{{ SysObjectID }}&nbsp;</td>
            </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_130) %></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property">{{ LastBoot }}&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_131) %></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property">{{ IOSVersion }}&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_132) %></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property">{{ IOSImage }}&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_133) %></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property">{{ HardwareType }}&nbsp;</td>
	        </tr>
 	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_82) %></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property">{{ CpuNo }}&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_75) %></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property"><a href="telnet://{{ HrefIPAddress }}">telnet://{{ HrefIPAddress }}</a></td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_72) %></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property"><a href="<%= DefaultSanitizer.SanitizeHtml(WebBrowseTemplate) %>"><%= DefaultSanitizer.SanitizeHtml(WebBrowseTemplate) %></a></td>
	        </tr>
        </table>

        </script>

        <span id="resourceContent" runat="server"></span>
    </Content>
</orion:resourceWrapper>
