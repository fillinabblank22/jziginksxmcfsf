﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI;
using Resources;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System.Web;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_NetPerfMon_Resources_NodeDetails_NotesHistory : TopXXResourceControl
{
    private const int DefaultNumberOfNodeNotes = 5;
    internal int NodeID { get; private set; }
    internal string NodeName { get; private set; }
    internal int NodeStatus { get; private set; }
    internal string NodeSwisUri { get; private set; }
    internal string NetObjectId { get; private set; }

    /// <summary>
    /// Contains uniqueidentifier of resource (Resource.ID value)
    /// </summary>
    public int UniqueClientID { get; set; }

    protected override string TitleTemplate
    {
        get { return CoreWebContent.WEBDATA_JV0_15; }
    }

    public override string EditURL
    {
        get { return base.EditURL + "&PropertiesMode=True"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditNotesHistory.ascx"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] {typeof (INodeProvider)}; }
    }

    protected override void OnInit(EventArgs e)
    {
        if (String.IsNullOrEmpty(Resource.Properties["MaxRecords"]))
        {
            Resource.Properties["MaxRecords"] = DefaultNumberOfNodeNotes.ToString(CultureInfo.InvariantCulture);
        }
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        UniqueClientID = Resource.ID;
        Node node = GetInterfaceInstance<INodeProvider>().Node;
        NodeID = node.NodeID;
        NodeName = " " + HttpUtility.HtmlEncode(node.Name);
        NodeStatus = (int)node.Status.ParentStatus.Value;
        NodeSwisUri = node.SwisUri;
        NetObjectId = node.NetObjectID;

        AddNoteButton(ResourceWrapper);
    }

    private void AddNoteButton(Control resourceWrapperContents)
    {
        if (resourceWrapperContents.TemplateControl is IResourceWrapper)
        {
            IResourceWrapper wrapper = resourceWrapperContents.TemplateControl as IResourceWrapper;
            wrapper.ManageButtonId = "ADD NOTE";
            wrapper.ManageButtonText = CoreWebContent.WEBDATA_JV0_18;
            wrapper.ManageButtonTarget = Page.Request.Url + "#";
        }
    }
}