using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Web;
using SolarWinds.Orion.Common;
using Node = SolarWinds.Orion.NPM.Web.Node;
using SolarWinds.Orion.Web.Pendo;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_NodeDetails_NodeDetails : ResourceControl
{
    internal int NodeID { get; private set; }
    private string webBrowseTemplate;

    /// <summary>
    /// Gets view limitation ID from HttpContext
    /// </summary>
    internal int ViewLimitationId
    {
        get
        {
            if (HttpContext.Current.Items.Contains(typeof(ViewInfo).Name))
            {
                var viewInfo = HttpContext.Current.Items[typeof (ViewInfo).Name] as ViewInfo;
                return viewInfo != null ? viewInfo.LimitationID : 0;
            }
            return 0;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Node node = GetInterfaceInstance<INodeProvider>().Node;
        NodeID = node.NodeID;
        SwisUri = node.SwisUri;
        webBrowseTemplate = NodeSettingsDAL.GetLastNodeSettings(node.NodeID, "Core.WebBrowseTemplate");

        if (NodeHelper.IsIntelArchitectureLinuxServer(node))
        {
            this.DecorateWrapper(PendoTriggerConstants.LinuxServerNode, this.Wrapper);
        }
    }

    protected string ReturnUrl
    {
        get { return ReferrerRedirectorBase.GetReturnUrl(); }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceAdvancedNodeDetails"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_VB0_35; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public string WebBrowseTemplate
    {
        get { return string.IsNullOrEmpty(webBrowseTemplate) ? CoreConstants.WebBrowseTemplate : webBrowseTemplate; }
    }

    internal bool IsDemo
    {
        get { return OrionConfiguration.IsDemoServer; }
    }

    internal string SwisUri
    {
        get; private set;
    }
}
