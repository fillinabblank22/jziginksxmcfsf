﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Core.Common.i18n;
using SolarWinds.Orion.NPM.Web;
using System.Net;

public partial class Orion_NetPerfMon_Resources_NodeDetails_NodeIPAddresses : BaseResourceControl
{

    protected override string DefaultTitle
    {
        get { return notAvailable; }
    }

    private int rowsPerPage;
    protected int RowsPerPage
    {
        get { return rowsPerPage; }
        set { rowsPerPage = value; }
    }

    private IPAddress primaryIP;
    protected IPAddress PrimaryIP
    {
        get { return primaryIP; }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
         if (!Int32.TryParse(Resource.Properties["RowsPerPage"], out rowsPerPage))
         {
             rowsPerPage = 5;
         }
        Node node = GetInterfaceInstance<INodeProvider>().Node;
        primaryIP = node.IPAddress;
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/NetPerfMon/Controls/EditResourceControls/EditNodeIPAddresses.ascx";
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceNodeIPAddresses"; }
    }

    public string GetSwql()
    {
        var rawSwql = Resource.Properties["SWQL"] ?? string.Empty;
        if (rawSwql == String.Empty)
        {
            return rawSwql;
        }

        var substituted = TokenSubstitution.Parse(rawSwql);

        if (!substituted.Contains("${"))
        {
            // No Macros found.  Just return the raw query
            return substituted;
        }

        var context = new Dictionary<string, object>();
        var provider = GetInterfaceInstance<INetObjectProvider>();
        if (provider != null)
        {
            var netobject = provider.NetObject;
            if (netobject != null)
                context = netobject.ObjectProperties;
        }

        var updatedSwql = Macros.ParseDataMacros(substituted, context, false, false);
        return updatedSwql;
    }


    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.Ajax;
        }
    }
    
}