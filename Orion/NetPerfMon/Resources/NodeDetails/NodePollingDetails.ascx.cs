using SolarWinds.Orion.Common;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Data;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_NetPerfMon_Resources_NodeDetails_NodePollingDetails : BaseResourceControl
{
	protected Node Node { get; set; }

    private string GetNodeObjectSubTypeText(string objectSubType)
    {
        return Resources.CoreWebContent.ResourceManager.GetString($"NodeSubType_{objectSubType}") ?? objectSubType;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
	{
		this.Node = this.GetInterfaceInstance<INodeProvider>().Node;
		if (this.Node != null)
		{
			DataTable table;
			using (var proxy = new WebDAL())
			{
				table = proxy.GetNodePollingDetails(this.Node.NodeID);
			}
			if (table != null && table.Rows.Count > 0)
			{
				this.pollingEngine.Text = String.Format("{0} ({1})", table.Rows[0]["ServerName"], table.Rows[0]["IP"]);
				this.pollingType.Text = GetNodeObjectSubTypeText(Node.ObjectSubType);

				this.pollingInterval.Text = String.Format(Resources.CoreWebContent.WEBCODE_VB0_39, table.Rows[0]["PollInterval"]);

                this.pollingIP.Text = Node.IPAddressString;

				try
				{
					DateTime poll = Convert.ToDateTime(table.Rows[0]["NextPoll"]).ToLocalTime();
					this.nextPoll.Text = Utils.FormatDateTime(poll);
				}
				catch
				{
					this.nextPoll.Text = String.Empty;
				}

				this.statCollection.Text = String.Format(Resources.CoreWebContent.WEBCODE_VB0_40, table.Rows[0]["StatCollection"]);

				this.allow64bitCounters.Text = Convert.ToBoolean(table.Rows[0]["Allow64bitCounters"]) ? Resources.CoreWebContent.WEBDATA_VB0_124 : Resources.CoreWebContent.WEBDATA_VB0_125;

				this.rediscoveryInterval.Text = String.Format(Resources.CoreWebContent.WEBCODE_VB0_40, table.Rows[0]["RediscoveryInterval"]);

				try
				{
					DateTime rediscovery = Convert.ToDateTime(table.Rows[0]["NextRediscovery"]).ToLocalTime();
                    this.nextRediscovery.Text = Utils.FormatDateTimeForDisplayingOnWebsite(rediscovery);
				}
				catch
				{
					this.nextRediscovery.Text = String.Empty;
				}

				try
				{
					DateTime lastSync = Convert.ToDateTime(table.Rows[0]["LastSync"]).ToLocalTime();
                    this.lastSync.Text = Utils.FormatDateTimeForDisplayingOnWebsite(lastSync, true); 
				}
				catch
				{
					this.lastSync.Text = String.Empty;
				}

				return;
			}
		}
		// in case of any error, hide the table with data
		this.nodeError.Visible = true;
		this.pollingDetailsTable.Visible = false;
	}

	protected override string DefaultTitle
	{
		get { return Resources.CoreWebContent.WEBCODE_VB0_41; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceNodePollingDetails"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
