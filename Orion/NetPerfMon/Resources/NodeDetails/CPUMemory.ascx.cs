﻿using System;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_NodeDetails_CPUMemory : SolarWinds.Orion.Web.UI.BaseResourceControl
{
	#region properties

	// overriden DefaultTitle
	protected override string DefaultTitle
	{
		get { return Resources.CoreWebContent.WEBCODE_VB0_29; }
	}

	// overriden HelpLink
	public override string HelpLinkFragment
	{
		get
		{
			return "OrionPHResourceCPULoadMemoryStatistics";
		}
	}

	public override ResourceLoadingMode ResourceLoadingMode
	{
		get { return ResourceLoadingMode.RenderControl; }
	}

	#endregion

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		if (NodeHelper.IsResourceUnderExternalNode(this))
		{
			this.Visible = false;
		}
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		// if is netObject a Node set filter for a node
		INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();
		if (nodeProvider == null) return;

		var node = nodeProvider.Node;

		// CPU Load
		this.CPULoadLink.NavigateUrl = String.Format("/Orion/NetPerfMon/CustomChart.aspx?ChartName=HostAvgCPULoad&NetObject={0}&Period=Today", node.NetObjectID);
		this.CPULoadPercentLabelLink.NavigateUrl = this.CPULoadLink.NavigateUrl;
		this.CPULoadPercentStatusBarLink.NavigateUrl = this.CPULoadLink.NavigateUrl;

		int warningLevel = Convert.ToInt32(Thresholds.CPULoadWarning.SettingValue);
		int errorLevel = Convert.ToInt32(Thresholds.CPULoadError.SettingValue);

		this.CPULoadPercentLabel.ErrorLevel = errorLevel;
		this.CPULoadPercentLabel.WarningLevel = warningLevel;
		this.CPULoadPercentLabel.Status = Convert.ToInt32(node.CPULoad);

		this.CPULoadPercentStatusBar.WarningLevel = warningLevel;
		this.CPULoadPercentStatusBar.ErrorLevel = errorLevel;
		this.CPULoadPercentStatusBar.Status = this.CPULoadPercentLabel.Status;

		// memory used
		this.MemotyUsedLink.NavigateUrl = String.Format("/Orion/NetPerfMon/CustomChart.aspx?ChartName=HostAvgMemoryUsage&NetObject={0}&Period=Today", node.NetObjectID);
		this.MemoryUsedPercentLabelLink.NavigateUrl = String.Format("/Orion/NetPerfMon/CustomChart.aspx?ChartName=HostAvgPercentMemoryUsed&NetObject={0}&Period=Today", node.NetObjectID);
		this.MemoryUsedPercentStatusBarLink.NavigateUrl = this.MemoryUsedPercentLabelLink.NavigateUrl;

		warningLevel = Convert.ToInt32(Thresholds.PercentMemoryWarning.SettingValue);
		errorLevel = Convert.ToInt32(Thresholds.PercentMemoryError.SettingValue);

		this.MemoryUsedLabel.Text = "0 B";
		this.MemoryAvialableLabel.Text = this.MemoryUsedLabel.Text;
		this.TotalMemoryLabel.Text = this.MemoryUsedLabel.Text;

		if (node.MemoryUsed >= 0)
		{
			this.MemoryUsedLabel.Text = Utils.ConvertToMB(node.MemoryUsed, "B", false);

			this.MemoryUsedPercentLabel.ErrorLevel = errorLevel;
			this.MemoryUsedPercentLabel.WarningLevel = warningLevel;
			this.MemoryUsedPercentLabel.Status = Convert.ToInt32(node.PercentMemoryUsed);

			this.MemoryUsedPercentStatusBar.ErrorLevel = errorLevel;
			this.MemoryUsedPercentStatusBar.WarningLevel = warningLevel;
			this.MemoryUsedPercentStatusBar.Status = this.MemoryUsedPercentLabel.Status;

			// memory aviable
			this.MemoryAvialableLink.NavigateUrl = String.Format("/Orion/NetPerfMon/CustomChart.aspx?ChartName=HostAvgMemoryUsage&NetObject={0}&Period=Today", node.NetObjectID);
			this.MemoryAvialablePercentLabelLink.NavigateUrl = String.Format("/Orion/NetPerfMon/CustomChart.aspx?ChartName=HostAvgPercentMemoryUsed&NetObject={0}&Period=Today", node.NetObjectID);
			this.MemoryAvialablePercentStatusBarLink.NavigateUrl = this.MemoryAvialablePercentLabelLink.NavigateUrl;

			this.MemoryAvialableLabel.Text = Utils.ConvertToMB(node.MemoryAvailable, "B", false);

			this.MemoryAvialablePercentLabel.ErrorLevel = 100;
			this.MemoryAvialablePercentLabel.WarningLevel = 100;
			this.MemoryAvialablePercentLabel.Status = 100 - this.MemoryUsedPercentLabel.Status;

			this.MemoryAvialablePercentStatusBar.Reverse = true;
			this.MemoryAvialablePercentStatusBar.WarningLevel = 100 - warningLevel;
			this.MemoryAvialablePercentStatusBar.ErrorLevel = 100 - errorLevel;
			this.MemoryAvialablePercentStatusBar.Status = this.MemoryAvialablePercentLabel.Status;

			this.TotalMemoryLabel.Text = Utils.ConvertToMB(node.TotalMemory, "B", false);
		}

		// total memory
		this.TotalMemoryLink.NavigateUrl = String.Format("/Orion/NetPerfMon/CustomChart.aspx?ChartName=HostAvgMemoryUsage&NetObject={0}&Period=Today", node.NetObjectID);
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}
}
