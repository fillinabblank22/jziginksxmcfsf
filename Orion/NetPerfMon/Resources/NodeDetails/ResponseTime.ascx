<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResponseTime.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeDetails_ResponseTime" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="PercentStatusBar" Src="~/Orion/Controls/PercentStatusBar.ascx" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_175) %>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <table border="0" cellpadding="2" cellspacing="0" width="100%">
            <tr>
                <td style="padding-left:15px;">
                    <asp:HyperLink ID="CurrentResponseTimeLink" runat="server" Target="_blank"><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_176) %></b></asp:HyperLink>
                </td>
                <td>
                    <asp:HyperLink ID="CurrentResponseTimeResponseLabelLink" runat="server" Target="_blank">
                        <orion:ResponseLabel runat="server" ID="CurrentResponseTimeResponseLabel" />
                    </asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td style="padding-left:15px;">
                    <asp:HyperLink ID="MinimumResponseTimeLink" runat="server" Target="_blank"><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_177) %></b></asp:HyperLink>
                </td>
                <td>
                    <asp:HyperLink ID="MinimumResponseTimeResponseLabelLink" runat="server" Target="_blank">
                        <orion:ResponseLabel runat="server" ID="MinimumResponseTimeResponseLabel" />
                    </asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td style="padding-left:15px;">
                    <asp:HyperLink ID="AverageResponseTimeLink" runat="server" Target="_blank"><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_178) %></b></asp:HyperLink>
                </td>
                <td>
                    <asp:HyperLink ID="AverageResponseTimeResponseLabelLink" runat="server" Target="_blank">
                        <orion:ResponseLabel runat="server" ID="AverageResponseTimeResponseLabel" />
                    </asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td style="padding-left:15px;">
                    <asp:HyperLink ID="MaximumResponseTimeLink" runat="server" Target="_blank"><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_179) %></b></asp:HyperLink>
                </td>
                <td>
                    <asp:HyperLink ID="MaximumResponseTimeResponseLabelLink" runat="server" Target="_blank">
                        <orion:ResponseLabel runat="server" ID="MaximumResponseTimeResponseLabel" />
                    </asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="padding-left:15px; width:70%">
                    <asp:HyperLink ID="CurrentPacketLossLink" runat="server" Target="_blank"><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_180) %></b></asp:HyperLink>
                </td>
                <td>
                    <asp:HyperLink ID="CurrentPacketLossPercentLabelLink" runat="server" Target="_blank">
                        <orion:PercentLabel runat="server" ID="CurrentPacketLossPercentLabel" />
                    </asp:HyperLink>
                </td>
            </tr>
        </table>
    </Content>
</orion:ResourceWrapper>