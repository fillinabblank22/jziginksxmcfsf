﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ManagementTasks.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeDetails_ManagementTasks" %>
<%@ Register TagPrefix="orion" TagName="ManagementTasksControl" Src="~/Orion/NetPerfMon/Controls/ManagementTasksControl.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:ManagementTasksControl ID="ManagementTasksControl" runat="server" />
	</Content>
</orion:resourceWrapper>
