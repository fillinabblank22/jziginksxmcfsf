<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CPUsByPercentLoad.ascx.cs"
    Inherits="Orion_NetPerfMon_Resources_NodeDetails_CPUsByPercentLoad" %>
    
    <script type="text/javascript">
        $(function () {
            var refresh = function () { __doPostBack('<%= updatePanel.ClientID %>', ''); };
            SW.Core.View.AddOnRefresh(refresh, '<%= updatePanel.ClientID %>');
        });
    </script>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:UpdatePanel runat="server" ID="updatePanel" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Repeater runat="server" ID="resourceTable">
                    <HeaderTemplate>
                        <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%">
                            <tr>
                                <td class="ReportHeader" style="text-transform: uppercase;">
                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_IB0_3) %>
                                </td>
                                <td class="ReportHeader" colspan="2" style="text-transform: uppercase;">
                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_IB0_2) %>
                                </td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="Property" width="40%">
                                &nbsp;<%# DefaultSanitizer.SanitizeHtml(Eval("CPUName")) %>&nbsp;
                            </td>
                            <td class="Property" width="25px">
                                <%# DefaultSanitizer.SanitizeHtml(FormatCPULoad(Eval("AvgLoad"))) %>&nbsp;
                            </td>
                            <td class="Property" width="55%">
                                <%# DefaultSanitizer.SanitizeHtml(FormatCPULoadBar(Eval("AvgLoad"))) %>&nbsp;
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatePanel">
                    <ProgressTemplate>
                        <img alt="" src="/Orion/images/AJAX-Loader.gif" /></ProgressTemplate>
                </asp:UpdateProgress>
                <table width="100%" runat="server" id="gridFooter" cellpadding="0" cellspacing="0">
                    <tr>
                        <td colspan="3" class="TopologyFooter">
                            <div class="topologyDiv" style="white-space: nowrap; padding-right: 5px; height: 15px;">
                                <asp:LinkButton CommandName="Page" OnCommand="Grid_IndexChanging" CausesValidation="false"
                                    CommandArgument="Prev" ID="lbPrev" runat="server">
                                <img src="/Orion/images/TopologyItems/Arrows/arrow1Left.gif" alt=""/>&nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_259) %>&nbsp;<%= DefaultSanitizer.SanitizeHtml(PageSize) %></asp:LinkButton>
                            </div>
                            <div class="topologyDiv" style="white-space: nowrap; padding-right: 5px; padding-left: 5px; height: 15px;">
                                <asp:LinkButton CommandName="Page" OnCommand="Grid_IndexChanging" CausesValidation="false"
                                    CommandArgument="Next" ID="lbNext" runat="server">
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_260) %>&nbsp;<%= PageSize %>&nbsp;<img src="/Orion/images/TopologyItems/Arrows/arrow1Right.gif" alt="" /></asp:LinkButton>
                            </div>
                            <div class="topologyDiv" style="white-space: nowrap; padding-left: 5px; padding-right: 5px; height: 15px;">
                                <asp:LinkButton CommandName="Page" OnCommand="Grid_IndexChanging" CommandArgument="All"
                                    ID="LinkButton1" runat="server">
                                <img src="/Orion/images/TopologyItems/Arrows/Arrow2Rigth.gif" alt="" />&nbsp;<span style="white-space:nowrap;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_VB0_210) %></span></asp:LinkButton>
                            </div>
                            <div style="height: 0px; line-height: 0px; width: 0px; clear: both;">
                            </div>
                        </td>
                    </tr>
                </table>
                <table width="100%" runat="server" id="gridShowLess" cellpadding="0" cellspacing="0">
                    <tr>
                        <td colspan="3" class="TopologyFooter">
                            <div class="topologyDiv" style="white-space: nowrap; padding-right: 5px; height: 15px;">
                                <asp:LinkButton CommandName="Page" OnCommand="Grid_IndexChanging" CausesValidation="false"
                                    CommandArgument="Less" ID="LinkButton2" runat="server">
                                <img src="/Orion/images/TopologyItems/Arrows/Arrow2Left.gif" alt=""/>&nbsp;<%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBCODE_IB0_9, TopXX)) %></asp:LinkButton>
                            </div>
                            <div style="height: 0px; line-height: 0px; width: 0px; clear: both;">
                            </div>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField runat="server" ID="hfPageIndex" />
                <asp:HiddenField runat="server" ID="hfShowAll" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="lbPrev" />
                <asp:AsyncPostBackTrigger ControlID="lbNext" />
                <asp:AsyncPostBackTrigger ControlID="LinkButton1" />
            </Triggers>
        </asp:UpdatePanel>
    </Content>
</orion:resourceWrapper>
