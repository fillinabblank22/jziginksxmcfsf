using System;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_NodeDetails_NodeCustomProperties : BaseResourceControl
{
    private string EntityName { get { return "Orion.Nodes"; } }
    private string EntityIDFieldName { get { return "NodeID"; } }
    private NetObject Netobject { set; get; }

    protected override void OnInit(EventArgs e)
    {
        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }

        base.OnInit(e);
        this.customPropertyList.Resource = this.Resource;
        Node node = GetInterfaceInstance<INodeProvider>().Node;
        if (node != null)
        {
            this.customPropertyList.NetObjectId = node.NodeID;
        }

        this.Netobject = node;

        customPropertyList.CustomPropertyLoader = (netObjectId, displayProperties) =>
        {
            return CustomPropertyHelper.GetCustomPropertiesForObject(netObjectId, displayProperties, EntityName, EntityIDFieldName, Netobject);
        };

        customPropertyList.EditCustomPropertiesLinkGenerator = (netObjectId, redirectUrl) =>
        {
            if (Request.UrlReferrer != null)
            {
                redirectUrl = UrlHelper.ToSafeUrlParameter(Request.UrlReferrer.PathAndQuery);
            }
            return String.Format("/Orion/Nodes/NodeProperties.aspx?Nodes={0}&GuidID={2}&ReturnTo={1}", netObjectId, redirectUrl, Guid.NewGuid());
        };
    }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_VB0_31; }
    }

    public override string  EditControlLocation
    {
        get
        {
			return "/Orion/NetPerfMon/Controls/EditResourceControls/EditCustomPropertyList.ascx";
        }
    }

    public override string DetachURL
    {
        get
        {
            return string.Format("/Orion/DetachResource.aspx?ResourceID={0}&NetObject=N:{1}",
                this.Resource.ID, this.customPropertyList.NetObjectId);
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceCustomPropertiesNodes"; }
    }

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
