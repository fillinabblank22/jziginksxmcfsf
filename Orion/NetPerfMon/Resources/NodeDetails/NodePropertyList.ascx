<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodePropertyList.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeDetails_NodePropertyList" %>

<orion:resourceWrapper runat="server">
    <Content>
        <div runat="server" id="nodeError" style="color:Red; font-weight:bold;" visible="false">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_63) %>        
        </div>
        <asp:Repeater runat="server" ID="propertyTable">
            <HeaderTemplate>
                <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
		    		<td class="Property" width="10">&nbsp;</td>
	      			<td class="PropertyHeader" valign="middle"><%# DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Core.Web.FormatHelper.GetPropertyName((string)Container.DataItem)) %></td>
	      			<td class="Property"><%# DefaultSanitizer.SanitizeHtml(this.FormatProperty((string)Container.DataItem)) %></td>
	    	    </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>            
            </FooterTemplate>        
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>