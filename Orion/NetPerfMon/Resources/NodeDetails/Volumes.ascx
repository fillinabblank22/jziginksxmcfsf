<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Volumes.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeDetails_Volumes" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DisplayTypes" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="PercentStatusBar" Src="~/Orion/Controls/PercentStatusBar.ascx" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_175) %>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="VolumesRepeater">
            <HeaderTemplate>
                <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%">
                    <tr class="ReportHeader">
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_183) %>
                        </td>
                        <td>
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_184) %>
                        </td>
                        <td>
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_185) %>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td class="Property" style="padding-left: 20px">
                        <asp:Image ID="VolumeImage" runat="server" ImageUrl='<%# DefaultSanitizer.SanitizeHtml("/NetPerfMon/images/Volumes/" + Eval("Icon")) %>' />
                    </td>
                    <td class="Property" style="padding-left: 10px">
                        <asp:HyperLink ID="CurrentLink" runat="server" NavigateUrl='<%# DefaultSanitizer.SanitizeHtml(Eval("VolumeURL")) %>'>
                        <%# HttpUtility.HtmlEncode(Eval("Caption").ToString()) %>
                        </asp:HyperLink>
                    </td>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text='<%# DefaultSanitizer.SanitizeHtml(new Bytes(Eval("Size")).ToShort1024String()) %>'
                            Visible='<%# Eval("DoShow") %>' />&nbsp;
                    </td>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text='<%# DefaultSanitizer.SanitizeHtml(new Bytes(Eval("SpaceUsed")).ToShort1024String()) %>'
                            Visible='<%# Eval("DoShow") %>' />&nbsp;
                    </td>
                    <td class="Property" valign="middle" align="center">
                        <asp:HyperLink ID="VolumePercerUseLink" runat="server" NavigateUrl='<%# DefaultSanitizer.SanitizeHtml(Eval("VolumeLoadURL")) %>'
                            Target="_blank">
                            <orion:PercentLabel runat="server" ID="PercentLabel" Status='<%# Convert.ToInt32((System.Single)Eval("PercentUsed")) %>'
                                WarningLevel='<%# Convert.ToInt32(Eval("Level1Value")) %>' ErrorLevel='<%# Convert.ToInt32(Eval("Level2Value")) %>' Show='<%# Eval("DoShow") %>' />
                        </asp:HyperLink>
                    </td>
                    <td class="Property" valign="middle" align="left">
                        <asp:HyperLink ID="VolumeBar" runat="server" NavigateUrl='<%# DefaultSanitizer.SanitizeHtml(Eval("VolumeLoadURL")) %>'
                            Target="_blank">
                            <orion:PercentStatusBar runat="server" ID="StatusBar" Status='<%# Convert.ToInt32((System.Single)Eval("PercentUsed")) %>'
                                WarningLevel='<%# Convert.ToInt32(Eval("Level1Value")) %>' ErrorLevel='<%# Convert.ToInt32(Eval("Level2Value")) %>' Show='<%# Eval("DoShow") %>' />
                        </asp:HyperLink>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:ResourceWrapper>
