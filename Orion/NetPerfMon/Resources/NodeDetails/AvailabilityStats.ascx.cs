﻿using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Data;
using System.Web.Script.Serialization;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_NodeDetails_AvailabilityStats : BaseResourceControl
{
    protected bool inlineRendering;

	protected Node Node { get; private set; }
    protected string SerializedStatisticsData { get; private set; }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }

        bool.TryParse(Request.QueryString.Get("InlineRendering"), out inlineRendering);
    }

    protected void Page_Load(object sender, EventArgs e)
	{
		this.Node = this.GetInterfaceInstance<INodeProvider>().Node;
		if (this.Node == null)
		{
			return;
		}

        if (inlineRendering)
        {
            DataTable availabilityStats = new AsyncResources().GetAvailabilityStats(this.Node.NodeID);

            var javaScriptSerializer = new JavaScriptSerializer();
            javaScriptSerializer.RegisterConverters(new[] { new DataTableConverter() });
            SerializedStatisticsData = javaScriptSerializer.Serialize(availabilityStats);
        }
	}

	protected override string DefaultTitle
	{
		get { return Resources.CoreWebContent.WEBCODE_VB0_28; }
	}

	public override string HelpLinkFragment
	{
		get
		{
			return "OrionPHResourceAvailabilityStatistics";
		}
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }
}
