﻿using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Linq;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_NodeDetails_NodePropertyList : BaseResourceControl
{
    private const char separator = ',';

	protected Node Node { get; private set; }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }
    }

	protected void Page_Load(object sender, EventArgs e)
	{
		this.Node = this.GetInterfaceInstance<INodeProvider>().Node;
		if (this.Node == null)
		{
			this.nodeError.Visible = true;
			return;
		}

		if (!String.IsNullOrEmpty(this.Resource.Properties["PropertyList"]))
		{
            string[] propertyList = this.Resource.Properties["PropertyList"].Split(separator).Select(item => item.Trim()).ToArray();

			if (UpdateOldPropertyNames(ref propertyList))
			{
                this.Resource.Properties["PropertyList"] = String.Join(separator.ToString(), propertyList);
			}
			this.propertyTable.DataSource = propertyList;
			this.propertyTable.DataBind();
		}
	}

	protected string FormatProperty(string property)
	{
		if (String.IsNullOrEmpty(property) || !this.Node.ObjectProperties.ContainsKey(property))
		{
			return String.Empty;
		}
		return Macros.DataMacro(property, this.Node.ObjectProperties[property].ToString(), true, true);
	}

	protected override string DefaultTitle
	{
		get { return Resources.CoreWebContent.WEBCODE_VB0_42; }
	}

	public override string HelpLinkFragment
	{
		get
		{
			return "OrionPHResourceCustomPropertiesNodes";
		}
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

	public override string EditControlLocation
	{
		get
		{
			return "/Orion/NetPerfMon/Controls/EditResourceControls/EditNodePropertyList.ascx";
		}
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

	public static bool UpdateOldPropertyNames(ref string[] propertyList)
	{
		bool changed = false;
		for (int i = 0; i < propertyList.Length; i++)
		{
			if (propertyList[i].IndexOf(' ') > 0)
			{
				string oldName = propertyList[i];
				switch (propertyList[i])
				{
					case "Node ID": propertyList[i] = "NodeID"; break;
					case "Last Database Sync": propertyList[i] = "LastSync"; break;
					case "IP Address": propertyList[i] = "IP_Address"; break;
					case "SNMP Community String": propertyList[i] = "Community"; break;
					case "Polling Type": propertyList[i] = "ObjectSubType"; break;
					case "Machine Type": propertyList[i] = "MachineType"; break;
					case "Vendor Icon": propertyList[i] = "VendorIcon"; break;
					case "Last Boot Time": propertyList[i] = "LastBoot"; break;
					case "System OID": propertyList[i] = "SysObjectID"; break;
					case "IOS Image": propertyList[i] = "IOSImage"; break;
					case "IOS Version": propertyList[i] = "IOSVersion"; break;
					case "Node Status": propertyList[i] = "GroupStatus"; break;
					case "System Description": propertyList[i] = "Description"; break;
					case "Status Description": propertyList[i] = "StatusDescription"; break;
					case "Status LED": propertyList[i] = "StatusLED"; break;
					case "Status Severity": propertyList[i] = "Severity"; break;
					case "System Name": propertyList[i] = "SysName"; break;
					case "Polling Interval": propertyList[i] = "PollInterval"; break;
					case "Statistics Collection Interval": propertyList[i] = "StatCollection"; break;
				}
				if (!changed && oldName != propertyList[i])
				{
					changed = true;
				}
			}
		}
		return changed;
	}

}
