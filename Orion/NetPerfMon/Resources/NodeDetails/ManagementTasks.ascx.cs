﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Resources_NodeDetails_ManagementTasks : BaseResourceControl
{
    protected NetObject NetObject
    {
        get
        {
            return this.GetInterfaceInstance<INetObjectProvider>().NetObject;
        }
    }

    protected string NetObjectId
    {
        get
        {
            return this.NetObject.NetObjectID;
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceManagementTasks";
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.WEBCODE_PF0_5; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new [] { typeof(INodeProvider) }; }
    }

    public override IEnumerable<string> RequiredDynamicInfoKeys
    {
        // to allow showing this on detail pages of netobjects that aren't inherited from Node
        get { return new[] { "SupportsManagementResource" }; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        this.ManagementTasksControl.ResourceID = Resource.ID;
        this.ManagementTasksControl.NetObject = NetObject;
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        this.Visible = this.ManagementTasksControl.Visible;
    }
}
