﻿using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_NetPerfMon_Resources_NodeDetails_SystemOID : BaseResourceControl
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
            
        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }
    
		Node node = this.GetInterfaceInstance<INodeProvider>().Node;

		bool nodeOk = (node != null);
		this.nodeError.Visible = !nodeOk;
		this.nodeOID.Visible = nodeOk;
		if (nodeOk)
		{
			this.nodeOID.Controls.Add(new LiteralControl(node.SysObjectID));
		}
	}

	public override string HelpLinkFragment
	{
		get
		{
			return "OrionPHResourceSystemOID";
		}
	}

	protected override string DefaultTitle
	{
		get { return Resources.CoreWebContent.WEBCODE_AK0_52; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
