<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeCustomProperties.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeDetails_NodeCustomProperties" %>
<%@ Register TagPrefix="orion" TagName="CustomPropertyList" Src="~/Orion/NetPerfMon/Controls/CustomPropertyList.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:CustomPropertyList runat="server" ID="customPropertyList" CustomPropertyTableName="NodesCustomProperties"/>
    </Content>
</orion:resourceWrapper>