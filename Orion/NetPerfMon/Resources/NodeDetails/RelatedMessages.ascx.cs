using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SyslogTraps.Web.DAL;
using System;
using System.Data;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Syslog)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_NodeDetails_ReleatedMessages : SolarWinds.Orion.Web.UI.BaseResourceControl
{
    // default number is used if there is no record in resource properties 
    private const int defaultNumberOfRecords = 25;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }
    }

    #region properties

    // overriden dafault title
    protected override string DefaultTitle
    {
        get { return Resources.SyslogTrapsWebContent.WEBCODE_VB0_45; }
    }

    // this resource uses Period property for date time filter
    //public override bool SupportDateTimeFilter
    //{
    //    get { return true; }
    //}

    // overriden main title of resources
    public override string DisplayTitle
    {
        get
        {
            //return Resource.Title; // this isn't working
            string result = Resource.Properties["MaxMessages"];

            if (String.IsNullOrEmpty(result)) result = defaultNumberOfRecords.ToString();

            return String.Format(Resources.SyslogTrapsWebContent.WEBCODE_VB0_46, result);
        }
    }

    // overriden subtitle of resource
    public override string SubTitle
    {
        get
        {
            // subtitle is a period
            string period = Resource.Properties["Period"];
            if (String.IsNullOrEmpty(period)) return Periods.GetLocalizedPeriod("Today");
            else
            {
                Periods.GetLocalizedPeriod(period);
                return period;
            }
        }
    }

    // overriden help fragment
    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceLastXXSyslogMessages";
        }
    }

    // overriden EditURL
    public override string EditControlLocation
    {
        get
        {
            return "/Orion/NetPerfMon/Controls/EditResourceControls/EditLastxxMessages.ascx"; ;
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            // try to load max event number or use default value
            int maxRecords;
            try
            {
                maxRecords = Int32.Parse(Resource.Properties["MaxMessages"]);
            }
            catch
            {
                maxRecords = defaultNumberOfRecords;
            }

            var periodBegin = new DateTime();
            var periodEnd = new DateTime();
            var period = this.Resource.Properties["Period"];
            var filterText = this.Resource.Properties["Filter"];
            int nodeID = -1;

            // if is netObject a Node set filter for a node
            INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();
            if (nodeProvider != null)
                nodeID = nodeProvider.Node.NodeID;

            if (String.IsNullOrEmpty(period))
                period = "Today";

            Periods.Parse(ref period, ref periodBegin, ref periodEnd);

            DataTable table;

            using (WebDAL webdal = new WebDAL())
            {
                table = webdal.GetSysLogMessages(nodeID, maxRecords, filterText, periodBegin, periodEnd);
            }

            if (table == null || table.Rows.Count == 0)
                InfoPanel.Visible = true;
            else
            {
                table.Columns.Add("NodeURL", typeof(string));

                foreach (DataRow r in table.Rows)
                {
                    r["NodeURL"] = String.Format("/Orion/View.aspx?NetObject=N:{0}", r["NodeID"]);
                }

                this.MessageRepeater.DataSource = table;
                this.MessageRepeater.DataBind();
            }
        }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }
}
