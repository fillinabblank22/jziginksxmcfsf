<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeIPAddresses.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeDetails_NodeIPAddresses" %>

<orion:Include ID="Include2" runat="server" File="NetPerfMon/resources/NodeDetails/NodeIPAddressesQuery.js" />

<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" >
            <Services>
                <asp:ServiceReference path="/Orion/Services/Information.asmx" />
            </Services>
        </asp:ScriptManagerProxy>
        <div id="Search" style="float:right;">            
            <input type="text" id="SearchInput<%= Resource.ID %>" style="font-style:italic;color:Gray;width:125px;" value="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TP0_3) %>" /> 
        </div>            
        <div style="clear:both"> </div>
        <div id="ErrorMsg-<%= Resource.ID %>"></div>

        <table class="NeedsZebraStripes" id="Grid-<%= Resource.ID %>" cellpadding="2" cellspacing="0" width="100%">
            <tr class="HeaderRow"></tr>
        </table>

        <div id="Pager-<%= Resource.ID %>" class="ReportHeader hidden" style="line-height: 15px; text-align: center; float:left"></div>

        <textarea id="SWQL-<%= Resource.ID %>" style="display:none;">
            <%= HttpUtility.HtmlEncode(GetSwql()) %>
        </textarea>

        <script type="text/javascript">            
            var searchValue<%= Resource.ID %> = $('#SearchInput<%= Resource.ID %>').val();
            
            $('#SearchInput<%= Resource.ID %>').focus(function()
            {
                var searchInput = $('#SearchInput<%= Resource.ID %>');
                var value = searchInput.val();
                if ('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TP0_3) %>' === value)
                {
                    searchInput.val('');
                    searchInput.css({'font-style': 'normal',
                                        'color': 'Black'});                    
                }
            });
            $('#SearchInput<%= Resource.ID %>').blur(function()
            {
                var searchInput = $('#SearchInput<%= Resource.ID %>');
                var value = searchInput.val();
                if (!value)
                {
                    searchInput.val('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TP0_3) %>');
                    searchInput.css({'font-style': 'italic',
                                    'color': 'Gray'});                    
                }
            });

            $('#SearchInput<%= Resource.ID %>').keyup(
            function checkSearchChanged() {
                var currentValue = $('#SearchInput<%= Resource.ID %>').val();
                if ((currentValue) && currentValue != searchValue<%= Resource.ID %> && currentValue != '' && currentValue != '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TP0_3) %>') 
                {
                    searchValue<%= Resource.ID %> = $('#SearchInput<%= Resource.ID %>').val();
                    SW.Core.Resources.NodeIPAddressesQuery.FilterInResult(<%= Resource.ID %>, 1, '<%= PrimaryIP %>', searchValue<%= Resource.ID %>);                                                          
                } else
                if (currentValue === '')
                {
                    SW.Core.Resources.NodeIPAddressesQuery.createTableFromQuery(<%= Resource.ID %>, 0, <%= RowsPerPage %>, '<%= PrimaryIP %>', undefined, 1);
                }            
            });
            
            $(function () {
                var refresh = function () {
                       SW.Core.Resources.NodeIPAddressesQuery.createTableFromQuery(<%= Resource.ID %>, 0, <%= RowsPerPage %>, '<%= PrimaryIP %>', undefined, 1);
                   };

                SW.Core.View.AddOnRefresh(refresh, "Grid-<%= Resource.ID %>");
                refresh();
            });
               
        </script>
    </Content>
</orion:ResourceWrapper>