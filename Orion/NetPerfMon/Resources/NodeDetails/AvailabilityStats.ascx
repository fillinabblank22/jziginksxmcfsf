<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AvailabilityStats.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeDetails_AvailabilityStats" %>
<%@ Reference virtualPath="~/Orion/Services/AsyncResources.asmx" %>

<orion:resourceWrapper runat="server">
    <Content>
        <table class="NeedsZebraStripes" id="availabilityTable" runat="server" border="0" cellpadding="2" cellspacing="0" width="100%" style="display: none;">
		    <tr>
		        <td class="ReportHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_112) %></td>
		        <td class="ReportHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_113) %></td>
		    </tr>
        </table>       
        <img id="loaderImage" runat="server" alt="" src="/Orion/images/AJAX-Loader.gif" style="margin:0 auto; display: block;"/>

        <script type="text/javascript">
            $(document).ready(function () {

                var refresh = function() {
                    var renderingParams = {
                        netObjectId: "<%= this.Node.NetObjectID %>",
                        loaderId: "<%= loaderImage.ClientID %>",
                        availabilityTableId: "<%= availabilityTable.ClientID %>",
                        Id: "<%= this.Node.NodeID %>"
                    };
                    $("#<%= availabilityTable.ClientID %>").find("tr:gt(0)").remove(); // remove all table rows except the first one

                    <% if (this.inlineRendering) { %>
                    RenderAvailabilityStats(renderingParams, <%= this.SerializedStatisticsData %>);

                    <% } else { %>
                    $.ajax({
                        type: "POST",
                        url: "/Orion/Services/AsyncResources.asmx/GetAvailabilityStats",
                        data: "{ nodeId: <%= this.Node.NodeID.ToString() %> }",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function(result) {
                            RenderAvailabilityStats(renderingParams, result);
                        },
                        error: function(error) {
                            GetAvailabilityStatsFailed("<%= loaderImage.ClientID %>", error);
                        }
                    });

                    <% } %>
                };
                SW.Core.View.AddOnRefresh(refresh, '<%= availabilityTable.ClientID %>');
                refresh();
            });

            function RenderAvailabilityStats(params, result) {
                var html = "";
                var rows = result.d != null ? result.d.Rows : result.Rows;
                $.each(rows, function (i, value) {
                    var perfstacklink =
                        String.format("/ui/perfstack/?context=0_Orion.Nodes_{0}&withRelationships=true&charts=0_Orion.Nodes_{0}-Orion.ResponseTime.Availability&{1}", params.Id, SW.Core.DateHelper.getPerfStackTimeFrame(value[2]));
                    var aCodePart = String.format("<td class='Property'><a href='#' onclick='window.open(\"{0}\");return false;' target='_blank'>", perfstacklink);
                    html += "<tr>" + aCodePart + value[0] + "</a></td>";
                    html += aCodePart + value[1] + "</a></td></tr>";
                });

                if (html != "") {
                    $("#" + params.availabilityTableId + " tr:last").after(html);
                }
                $("#" + params.loaderId).hide();
                $("#" + params.availabilityTableId).show();
            }

            function GetAvailabilityStatsFailed(loaderId, error) {
                $("#" + loaderId).hide().after("<div>Server error: " + error.status + "-" + error.statusText + "</div>");
            }
 	    </script>
    </Content>
</orion:resourceWrapper>