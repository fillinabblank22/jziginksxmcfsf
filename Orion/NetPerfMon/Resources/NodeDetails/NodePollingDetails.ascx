<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodePollingDetails.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeDetails_NodePollingDetails" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div runat="server" id="nodeError" visible="false" style="color:Red;font-weight:bold;" class="Text">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_63) %>
        </div>
        <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%" runat="server" id="pollingDetailsTable">
        	<tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" valign="middle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TP0_1) %></td>
	          <td class="Property"><asp:Label runat="server" ID="pollingIP" />&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" valign="middle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_134) %></td>
	          <td class="Property"><asp:Label runat="server" ID="pollingEngine" />&nbsp;</td>
	        </tr>
            <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" valign="middle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_1) %></td>
	          <td class="Property"><asp:Label runat="server" ID="pollingType" />&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" valign="middle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_135) %></td>
	          <td class="Property"><asp:Label runat="server" ID="pollingInterval" />&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" valign="middle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_136) %></td>
	          <td class="Property"><asp:Label runat="server" ID="nextPoll" />&nbsp;</td>
	        </tr>
	        <tr>
	          <td colspan="3" class="Property" width="10">&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" valign="middle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_137) %></td>
	          <td class="Property"><asp:Label runat="server" ID="statCollection" />&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" valign="middle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_138) %></td>
	          <td class="Property"><asp:Label runat="server" ID="allow64bitCounters" />&nbsp;</td>
	        </tr>
	        <tr>
	          <td colspan="3" class="Property" width="10">&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" valign="middle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_139) %></td>
	          <td class="Property"><asp:Label runat="server" ID="rediscoveryInterval" />&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" valign="middle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_140) %></td>
	          <td class="Property"><asp:Label runat="server" ID="nextRediscovery" />&nbsp;</td>
	        </tr>
	        <tr>
	          <td colspan="3" class="Property" width="10">&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" valign="middle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_141) %></td>
	          <td class="Property"><asp:Label runat="server" ID="lastSync" />&nbsp;</td>
	        </tr>
	    </table>
    </Content>
</orion:resourceWrapper>    