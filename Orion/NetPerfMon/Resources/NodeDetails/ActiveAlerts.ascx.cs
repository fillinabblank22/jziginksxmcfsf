﻿using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Controls;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Alerts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsAlertValues.Alarms)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsAlertValues.Warnings)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsAlertValues.Warns)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_NodeDetails_ActiveAlerts : BaseResourceControl
{
	private bool showAcknowledgedAlerts;

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		if (NodeHelper.IsResourceUnderExternalNode(this))
		{
			this.Visible = false;
		}
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		var node = GetInterfaceInstance<INodeProvider>().Node;
		if (node == null)
		{
		 	return;
		}
		
		string showAck = Resource.Properties["ShowAcknowledgedAlerts"];
		AlertsTable.ShowAcknowledgedAlerts = showAcknowledgedAlerts = !String.IsNullOrEmpty(showAck) && showAck.Equals("true", StringComparison.OrdinalIgnoreCase);
		AlertsTable.ResourceID = Resource.ID;
		AlertsTable.RelatedNodeId = node.NodeID;
		AlertsTable.RelatedNodeEntityUri = node.SwisUri;
		
		int rowsPerPage;
		if (!int.TryParse(Resource.Properties["RowsPerPage"], out rowsPerPage))
			rowsPerPage = 5;

		AlertsTable.InitialRowsPerPage = rowsPerPage;

		AddShowAllCurrentlyTriggeredAlertsControl(this.wrapper);

        AlertSuppressionInfoMessage.SetSuppressAlertsMessageVisibility(node.SwisUri);
    }

    protected override string DefaultTitle
	{
		get { return Resources.CoreWebContent.WEBCODE_IB0_4; }
	}

	public override string SubTitle
	{
		get
		{
			if (!string.IsNullOrEmpty(Resource.SubTitle.Trim()))
				return Resource.SubTitle;

			// if subtitle is not specified
			if (showAcknowledgedAlerts)
				return Resources.CoreWebContent.WEBCODE_IB0_5;
			else
				return Resources.CoreWebContent.WEBCODE_IB0_6;
		}
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceActiveAlerts"; }
	}

	public override string EditControlLocation
	{
		get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditAllAlerts.ascx"; }
	}

	public override ResourceLoadingMode ResourceLoadingMode
	{
		get { return ResourceLoadingMode.Ajax; }
	}

	private void AddShowAllCurrentlyTriggeredAlertsControl(Control resourceWrapperContents)
	{
		if (resourceWrapperContents.TemplateControl is IResourceWrapper)
		{
			IResourceWrapper wrapper = resourceWrapperContents.TemplateControl as IResourceWrapper;
			wrapper.ManageButtonText = Resources.CoreWebContent.WEBDATA_BV0_0043;
			wrapper.ManageButtonTarget = "/Orion/NetPerfMon/Alerts.aspx";
			wrapper.ShowManageButton = true;
		}
	}
}
