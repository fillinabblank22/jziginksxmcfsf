<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeStatus.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeDetails_NodeStatus" %>

<orion:resourceWrapper runat="server" ID="wrapper">
    <Content>
        <table class="NeedsZebraStripes" width="100%" border="0" cellpadding="2" cellspacing="0">
            <tr>
                <td class="Property" width="10">&nbsp;</td>
	            <td align="left" class="PropertyHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_122) %></td>
	            <td runat="server" id="nodeError" class="PropertyHeader" style="color:Red;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_145) %></td>
    			<td runat="server" id="nodeStatus"><asp:Image runat="server" ID="GroupStatus" /></td>
			    <td runat="server" id="nodeDescription" class="Property" ><asp:Label runat="server" ID="description" /></td>
            </tr>
        </table>
    </Content>
</orion:resourceWrapper>