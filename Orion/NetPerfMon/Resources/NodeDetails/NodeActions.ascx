<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeActions.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeDetails_NodeActions" %>

<orion:resourceWrapper runat="server">
    <Content>
        <div runat="server" id="nodeError" visible="false" style="color:Red;font-weight:bold;" class="Text">
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_63) %>
        </div>
        <div runat="server" id="accessDenied" visible="false" style="color:Red;font-weight:bold;" class="Text">
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_64) %>
        </div>
        <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%" runat="server" id="detailsTable">
            <tr><td class="link"><a href="Javascript:DoAction('SWTOOL:LOOKUPIPADDRESS', '');"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_65) %></a></td></tr>
	        <tr><td class="link"><a href="Javascript:DoAction('SWTOOL:LOOKUPHOSTNAME', '');"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_66) %></a></td></tr>
	        <tr><td class="link"><a href="Javascript:DoAction('SWTOOL:IPNETWORKBROWSE', '');"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_67) %></a></td></tr>
	        <tr><td class="link"><a href="Javascript:DoAction('SWTOOL:Traceroute', '');"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_68) %></a></td></tr>
            <tr><td class="link"><a href="Javascript:DoAction('SWTOOL:Ping', '');"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_69) %></a></td></tr>
            <tr><td class="link"><a href="Javascript:DoAction('SWTOOL:EnhancedPing', '');"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_70) %></a></td></tr>
            <tr><td class="link"><a href="Javascript:DoAction('SWTOOL:PortScan', '');"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_71) %></a></td></tr>
            <tr><td class="link"><a href="Javascript:DoAction('SWTOOL:WebBrowse', '');"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_72) %></a></td></tr>
            <tr><td class="link"><a href="Javascript:DoAction('SWTOOL:DNSAnalysis', '');"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_73) %></a></td></tr>
            <tr><td class="link"><a href="Javascript:DoAction('SWTOOL:TELNET', '');"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_75) %></a></td></tr>
            <tr><td class="link"><a href="Javascript:DoAction('SWTOOL:WatchIT', '');"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_74) %></a></td></tr>
            <tr><td class="link"><a href="Javascript:DoAction('SWTOOL:SubnetList', '');"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_76) %></a></td></tr>
            <tr><td class="link"><a href="Javascript:DoAction('SWTOOL:CPUGAUGE', '');"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_77) %></a></td></tr>
	        <tr><td class="link"><a href="Javascript:DoAction('SWTOOL:RDP', '');"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_78) %></a></td></tr>
            <tr><td class="link"><a href="Javascript:DoAction('SWTOOL:MIBBROWSE', 'RFC1213-MIB:mib-2');"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_79) %></a></td></tr>

	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:MIBBROWSE', 'IF-MIB:ifTable');"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_102) %></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:MIBBROWSE', 'RFC1213-MIB:system');"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_103) %></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:MIBBROWSE', 'CISCO-SMI:cisco');"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_104) %></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:MIBBROWSE', 'MSFT-MIB:microsoft');"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_105) %></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:MIBBROWSE', 'DOCS-IF-MIB:docsIfMib');"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_106) %></a></td></tr>

	        <tr><td class="link"><a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Traffic');"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_85) %></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Traffic');">                                 <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_80) %></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Interface/Port Status');">                      <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_81) %></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Broadcast/Multicast');">                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_82) %></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Errors/Discards');">                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_83) %></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Interface/Port Configuration');">               <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_84) %></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Time data was last Recevied/Transmitted');">    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_86) %></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Ethernet Statistics');">                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_87) %></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Ethernet Collisions');">                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_88) %></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Token Ring Statistics');">                      <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_89) %></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Frame Relay Configuration');">                  <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_90) %></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Frame Relay Traffic');">                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_91) %></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Wireless Clients (Cisco APs)');">               <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_92) %></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Wireless Access Point SSID (Cisco APs)');">     <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_93) %></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Wireless Bridges and Repeaters (Cisco APs)');"> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_94) %></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'ISDN Line Configuration');">                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_95) %></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Traffic Rates (Cisco only)');">                 <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_96) %></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Queue Errors (Cisco only)');">                  <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_97) %></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Slow vs Fast Switching');">                     <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_98) %></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Protocols (Cisco only)');">                     <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_99) %></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'IGRP Settings');">                              <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_100) %></a></td></tr>
	        <tr><td class="link">&nbsp&nbsp;&nbsp;<a href="Javascript:DoAction('SWTOOL:RealTimeInterfaceMonitor', 'Cisco Discovery Protocol');">                   <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_101) %></a></td></tr>
	    </table>
    </Content>
</orion:resourceWrapper>


