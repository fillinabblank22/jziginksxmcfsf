<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RelatedMessages.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeDetails_ReleatedMessages" %>
<%@ Import Namespace="SolarWinds.Orion.Common" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>

<orion:Include File="legacyevents.css" /><%-- severity css --%>

<orion:resourceWrapper ID="ResourceWrapper1" runat="server" ShowEditButton="true">
    <Content>
        <asp:Panel ID="InfoPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Green">
                        <%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_146 %>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="MessageRepeater">
            <HeaderTemplate>
                <table style="width: 100%" cellspacing="0" cellpadding="0">
                    <tr class="ReportHeader" style="text-align: center">
                        <td  style="padding: 5px;">
                            <%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_147 %>
                        </td>
                        <td  style="padding: 5px;">
                            <%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_148 %>
                        </td>
                        <td  style="padding: 5px;">
                            <%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_149 %>
                        </td>
                        <td style="padding: 5px;">
                            &nbsp;
                        </td>
                        <td style="padding: 5px;">
                            <%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_150 %>
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr style="text-align: center" class='Severity<%#Eval("SysLogSeverity")%>'>
                    <td style="padding: 5px;">
                        <%#Utils.FormatCurrentCultureDate(((DateTime)Eval("DateTime")).ToLocalTime()) + "&nbsp;" + Utils.FormatToLocalTime(((DateTime)Eval("DateTime")).ToLocalTime())%>
                    </td>
                    <td style="padding: 5px;">
                        <orion:ToolsetLink runat="server" ID="link" IPAddress='<%#Eval("IPAddress")%>' DNS='<%#Eval("DNS")%>'
                            SysName='<%#Eval("SysName")%>' CommunityGUID='<%#Eval("GUID")%>' NavigateUrl='<%#Eval("NodeURL")%>'>

                        <%#Eval("HostName")%>
                        </orion:ToolsetLink>
                    </td>
                    <td style="padding: 5px;">
                        <orion:ToolsetLink runat="server" ID="ToolsetLink1" IPAddress='<%#Eval("IPAddress")%>'
                            DNS='<%#Eval("DNS")%>' SysName='<%#Eval("SysName")%>' CommunityGUID='<%#Eval("GUID")%>'
                            NavigateUrl='<%#Eval("NodeURL")%>'>
                        
                        <%#Eval("IPAddress")%>
                        </orion:ToolsetLink>
                    </td>
                    <td style="padding: 5px;">
                        <img alt="" src="/NetPerfMon/images/Severity<%#Eval("SysLogSeverity")%>.gif" />
                    </td>
                    <td style="padding: 5px; text-align: left;">
                        <a href="/Orion/NetPerfMon/Syslog.aspx">
                            <%#Eval("Message")%>
                        </a>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>
