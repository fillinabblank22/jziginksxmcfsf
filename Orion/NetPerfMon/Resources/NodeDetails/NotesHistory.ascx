<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NotesHistory.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeDetails_NotesHistory" %>
<%@ Import Namespace="Resources" %>

<orion:resourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true" ShowManageButton="true">
    <Content>
        <style type="text/css">
            #NotesTable { table-layout: fixed; }

            #NotesTable td {
                border: none;
                font-family: Verdana;
                font-size: 10px;
                padding-top: 10px;
                word-wrap: break-word;
            }

            #NotesTable .User {
                padding-left: 25px;
                padding-right: 5px;
                text-align: left;
                width: 55px;
            }

            #NotesTable .Date {
                color: #aaaaaa;
                font-family: Verdana;
                padding-right: 5px;
            }

            #NotesTable .Note {
                font-family: Arial;
                font-size: 12px;
                padding-bottom: 10px;
                padding-left: 25px;
                padding-top: 2px;
            }

            .addTitle { font-family: Verdana, Arial;}

            .EvenRow { background-color: #F0F0F0; }

        </style>
        
        <script type="text/javascript">
            $(function() {
                var refresh = function() {
                    var uniqueId = '<%= Resource.ID %>';
                    var currentOrderBy = $('#OrderBy-' + uniqueId).val();
                    var nodeId = <%= NodeID %>;
                    var pageIndex = 0;
                    var pageSize = <%= MaxRecords %>;
                    var divAddNodeNote = $("#divAddNodeNote-" + uniqueId);

                    $(divAddNodeNote).find(".add").empty();
                    $(divAddNodeNote).find(".cancel").empty();

                    // Cancel New Note button
                    $(SW.Core.Widgets.Button('<%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_PCC_28) %>', { type: 'secondary' })).appendTo($(divAddNodeNote).find(".cancel").css('margin', '5px').css('margin-right', '0')).click(function() {
                        $(divAddNodeNote).find('#NodeNoteContent').val('');
                        $(divAddNodeNote).hide();
                        refresh();
                    });
                    // Add New Note button
                    $(SW.Core.Widgets.Button('<%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_JV0_8) %>', { type: 'primary' })).appendTo($(divAddNodeNote).find(".add").css('margin', '5px')).click(function() {
                        <% if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer && !Profile.AllowAdmin) { %>
                        demoAction('Core_Node_AddNotes', this);
                        return;
                        <% } %>
                        var entityName = 'Orion.Nodes';
                        var entityUri = '<%= NodeSwisUri %>';
                        var keyPropertyValue = '<%= NetObjectId %>';
                        var timestamp = $.now();
                        var statusIconSrc = String.format("/Orion/StatusIcon.ashx?entity={0}&EntityUri='{1}'&KeyPropertyValue={2}&size=small&timestamp={3}", entityName, encodeURI(entityUri), keyPropertyValue, timestamp);
                        $('#statusIcon').attr('src', statusIconSrc);
                        var note = $(divAddNodeNote).find('#NodeNoteContent').val();
                        if (note != '') {
                            // Add New Note API call
                            SW.Core.Services.callController("/api/NodeNotes/AddNodeNote",
                                {
                                    NodeNote: note,
                                    NodeId: nodeId,
                                },
                                function(result) {
                                    if (result) {
                                        $(divAddNodeNote).find('#NodeNoteContent').val('');
                                        $(divAddNodeNote).hide();
                                        refresh();
                                    }
                                },
                                function() {});
                        };
                    });

                    // Get Notes API call
                    SW.Core.Services.callController("/api/NodeNotes/GetNodeNotes", {
                        NodeId: nodeId,
                        CurrentPageIndex: pageIndex,
                        PageSize: pageSize,
                        OrderByClause: currentOrderBy
                    }, function(result) {
                        $("#divNodeNotes-<%= Resource.ID %>").find("tr").remove();
                        $.each(result.DataTable.Rows, function(key) {
                            var user = result.DataTable.Rows[key][2];
                            var timeAgo = result.DataTable.Rows[key][1];
                            var note = result.DataTable.Rows[key][0];
                            var isEvenRow = key % 2 == 0;

                            var headerNewRow = "<tr" + (isEvenRow ? " class=EvenRow " : "") + "><td class=User>" + user + "</td><td class=Date>" + timeAgo + "</td></tr>";
                            var noteNewRow = "<tr" + (isEvenRow ? " class=EvenRow " : "") + "><td class=Note colspan=2>" + note + "</td></tr>";

                            $("#divNodeNotes-<%= Resource.ID %>").find("tbody").append(headerNewRow);
                            $("#divNodeNotes-<%= Resource.ID %>").find("tbody").append(noteNewRow);
                        });
                        $resourceWrapperTitle = $("div.ResourceWrapper[resourceid=<%= Resource.ID %>] h1:first");
                        if ($resourceWrapperTitle.find("span").length == 0) {
                            $resourceWrapperTitle.append(SW.Core.String.Format("<span><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_PS0_104) %></span>", result.TotalRows));
                        } else {
                            $resourceWrapperTitle.find("span").text(SW.Core.String.Format("<%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_PS0_104) %>", result.TotalRows));
                        }
                    }, function() {
                    });

                    // Resource menu ADD NOTE button
                    $(".ResourceWrapper[resourceid=<%= Resource.ID %>] a.ManageButton").on("click", function(e) {
                        e.preventDefault();
                        $(divAddNodeNote).show();
                        $(divAddNodeNote).find("#NodeNoteContent").focus();
                    });

                    // Enter key while writing adds new note
                    $(divAddNodeNote).find("#NodeNoteContent").keydown(function(event) {
                        if (event.keyCode == 13) {
                            event.keyCode = '';
                            $(divAddNodeNote).find(".add").find("a").click();
                        }
                    });
                };

                SW.Core.View.AddOnRefresh(refresh, 'divNodeNotes-<%= Resource.ID %>');
                refresh();
            });
        </script>
        
        <div id="divNodeNotes-<%= Resource.ID %>">
            <br />
            <table id="NotesTable" cellspacing="0" cellpadding="0">
                <tbody></tbody>
            </table>
        </div>
        
        <div id="divAddNodeNote-<%= Resource.ID %>" style="display: none">
            <br/>
            <table style="border: none; width: 100%;">
                <tr>
                    <td colspan = 3 style="border: none; padding: 5px; white-space: nowrap;">
                        <span class="addTitle">
                            <%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_JV0_14) %>  
                            <img id="statusIcon" src="/Orion/StatusIcon.ashx?entity=Orion.Nodes&amp;id=<%= NodeID %>&amp;status=<%= DefaultSanitizer.SanitizeHtml(NodeStatus) %>&amp;size=small" />
                            <%= DefaultSanitizer.SanitizeHtml(NodeName) %> 
                        </span>
                    </td>
                </tr>
                <tr>
                    <td colspan = 3 style="border: none; padding: 5px; white-space: pre-wrap;">
                        <asp:TextBox ID="NodeNoteContent" ClientIDMode="Static" runat="server" TextMode="MultiLine" ReadOnly="false" Rows="8"  style="resize: none; width: 100%; margin-top: -25px; margin-bottom: -10px; "></asp:TextBox>
                    </td>
                </tr>    
                <tr>
                    <td style="border: none; height: 20px;">
                        <span class="cancel" style="float: right"></span>
                        <span class="add" style="float: right"></span>
                    </td>
                </tr>    
            </table>
        </div>
    </Content>
</orion:resourceWrapper>