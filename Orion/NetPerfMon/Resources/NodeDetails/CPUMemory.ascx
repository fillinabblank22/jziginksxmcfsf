<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CPUMemory.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeDetails_CPUMemory" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="PercentStatusBar" Src="~/Orion/Controls/PercentStatusBar.ascx" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
		<style>
			table.NeedsZebraStripes td{font-size: 8pt;}
		</style>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_107) %>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%">
            <tr>
                <td style="padding-left:15px;">
                    <asp:HyperLink ID="CPULoadLink" runat="server" Target="_blank"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_108) %></asp:HyperLink>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:HyperLink ID="CPULoadPercentLabelLink" runat="server" Target="_blank">
                        <orion:PercentLabel runat="server" ID="CPULoadPercentLabel" />
                    </asp:HyperLink>
                </td>
                <td>
                    <asp:HyperLink ID="CPULoadPercentStatusBarLink" runat="server" Target="_blank">
                        <orion:PercentStatusBar runat="server" ID="CPULoadPercentStatusBar" />
                    </asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td style="padding-left:15px;">
                    <asp:HyperLink ID="MemotyUsedLink" runat="server" Target="_blank"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_109) %></asp:HyperLink>
                </td>
                <td>
                    <asp:Label ID="MemoryUsedLabel" runat="server" />
                </td>
                <td>
                    <asp:HyperLink ID="MemoryUsedPercentLabelLink" runat="server" Target="_blank">
                        <orion:PercentLabel runat="server" ID="MemoryUsedPercentLabel" />
                    </asp:HyperLink>
                </td>
                <td>
                    <asp:HyperLink ID="MemoryUsedPercentStatusBarLink" runat="server" Target="_blank">
                        <orion:PercentStatusBar runat="server" ID="MemoryUsedPercentStatusBar" />
                    </asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td style="padding-left:15px;">
                    <asp:HyperLink ID="MemoryAvialableLink" runat="server" Target="_blank"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_110) %></asp:HyperLink>
                </td>
                <td>
                    <asp:Label ID="MemoryAvialableLabel" runat="server" />
                </td>
                <td>
                    <asp:HyperLink ID="MemoryAvialablePercentLabelLink" runat="server" Target="_blank">
                        <orion:PercentLabel runat="server" ID="MemoryAvialablePercentLabel" />
                    </asp:HyperLink>
                </td>
                <td>
                    <asp:HyperLink ID="MemoryAvialablePercentStatusBarLink" runat="server" Target="_blank">
                        <orion:PercentStatusBar runat="server" ID="MemoryAvialablePercentStatusBar" />
                    </asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td style="padding-left:15px;">
                    <asp:HyperLink ID="TotalMemoryLink" runat="server" Target="_blank"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_111) %></asp:HyperLink>
                </td>
                <td>
                    <asp:Label ID="TotalMemoryLabel" runat="server" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </Content>
</orion:ResourceWrapper>
