﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActiveAlerts.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeDetails_ActiveAlerts" %>
<%@ Register TagPrefix="orion" TagName="AlertSuppressionInfoMessage" Src="~/Orion/NetPerfMon/Controls/AlertSuppressionInfoMessage.ascx" %>
<%@ Register TagPrefix="orion" TagName="AlertsTable" Src="~/Orion/NetPerfMon/Controls/AlertsOnThisEntity.ascx" %>

<orion:resourceWrapper runat="server" ID="wrapper" ShowEditButton="True">
    <Content>
        <orion:AlertSuppressionInfoMessage ID="AlertSuppressionInfoMessage" runat="server" />
        <orion:AlertsTable runat="server" ID="AlertsTable" />
    </Content>
</orion:resourceWrapper>
