﻿SW.Core.Resources = SW.Core.Resources || {};
SW.Core.Resources.NodeIPAddressesQuery = SW.Core.Resources.NodeIPAddressesQuery || {};

(function(cq) {

    var PageManager = function(pageIndex, pageSize) {
        this.currentPageIndex = pageIndex;
        this.rowsPerPage = pageSize;

        this.startItem = function() {
            return (this.rowsPerPage * this.currentPageIndex) + 1;
        };

        this.lastItem = function() {
            // Ask for rowsPerPage + 1 rows so we can detect if this is the last page.
            return (this.rowsPerPage * (this.currentPageIndex + 1)) + 1;
        };

        this.withRowsClause = function() {
            return " WITH ROWS " + this.startItem() + " TO " + this.lastItem();
        };

        this.isLastPage = function(rowCount) {
            // We asked for rowsPerPage + 1 rows.  If more than rowsPerPage rows 
            // got returned, we know this isn't the last page.
            return rowCount <= this.rowsPerPage;
        };
    };

    var generateColumnInfo = function(columns) {
        var columnInfo = [];
        var indexLookup = {};

        // Map column text to its index.
        $.each(columns, function(index, name) {
            indexLookup[name] = index;
            columnInfo[index] = { };
        });
        
        // Lookup each column's formatting information.
        $.each(columns, function(index, name) {

            // hack hack hack, SWIS is not handling renaming coumns FB 161182 
            if (name == 'IP_Address2')
            {
                columnInfo[index].header = '@{R=Core.Strings;K=XMLDATA_TP0_3;E=js}';
                return;
            } else
            if (name == 'IPAddressType')
            {
                columnInfo[index].header = '@{R=Core.Strings;K=XMLDATA_TP0_2;E=js}';
                return;
            }

            columnInfo[index].header = ((name != null) && (name.substring(0, 1) !== '_')) ? name : null;

            var otherColumnIndex = getReferencedColumnIndex(name, '_IconFor_', indexLookup);
            if (otherColumnIndex != null) {
                columnInfo[otherColumnIndex].iconColumn = index;
            }
            
            otherColumnIndex = getReferencedColumnIndex(name, '_LinkFor_', indexLookup);
            if (otherColumnIndex != null) {
                columnInfo[otherColumnIndex].linkColumn = index;
            }

            otherColumnIndex = getReferencedColumnIndex(name, '_RadioButtonFor_', indexLookup);
            if (otherColumnIndex != null) {
                columnInfo[index].radioButtonColumn = otherColumnIndex;
                columnInfo[index].header = '';
            }
        });
        
        return columnInfo;
    };

    var getReferencedColumnIndex = function(name, prefix, columnIndexLookup) {
        var re = new RegExp('^' + prefix + '(.+)$', 'i');
        var match = name.match(re);
        return match ? columnIndexLookup[match[1]] : null;
    };

    var renderCell = function(cellValue, rowArray, columnInfo, resourceId) {
        var cell = $('<td/>');
        if (cellValue == null)
        {
           cellValue = "";
        }
        else if (Date.isInstanceOfType(cellValue)) {
            cellValue = cellValue.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + " " + cellValue.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern); 
        }
         
        if (columnInfo.iconColumn) {
            $('<img/>').attr('src', rowArray[columnInfo.iconColumn]).appendTo(cell);
        } else        
        if (columnInfo.linkColumn) {
            $('<a/>').attr('href', rowArray[columnInfo.linkColumn])
                     .attr('class', 'NoTip')
                     .text(cellValue)
                     .appendTo(cell);
        } 
        if (columnInfo.radioButtonColumn) {
            var name = "rbGroup-" + resourceId;
            $('<input type="radio" name=' + name + '>').attr('value', rowArray[columnInfo.radioButtonColumn])
                         .appendTo(cell);
        }
        else {
            $('<span/>').text(cellValue).appendTo(cell);
        }

        return cell;
        
    };

    var updatePagerControls = function(resourceId, pageManager, rowCount, primaryIP, customSWQL, columnIndexForFiltering, onFinished) {
        var pager = $('#Pager-' + resourceId);
        var pageIndex = pageManager.currentPageIndex;
        var html = [];

        var showAllText = '@{R=Core.Strings;K=WEBJS_TM1_CUSTOMQUERY_SHOWALL;E=js}'; //Show all
        
        var style = 'style="vertical-align:middle;"';
        var previousImgRoot = '/Orion/images/Arrows/button_white_paging_previous';
        var nextImgRoot = '/Orion/images/Arrows/button_white_paging_next';

        var showAll = showAllText;
        var haveLinks = false;

        var startHtml;
        var endHtml;
        var contents;
        
        pager.css({'background-color': '#FFFFFF',
                    'border-top': '0px',
                    'font-size': '7pt!important'});

        if (pageIndex > 0) {
            startHtml = '<a href="#" class="previousPage NoTip">';
            contents = String.format('<img src="{0}.gif" {1}/>', previousImgRoot, style);
            endHtml = '</a>';
            haveLinks = true;
        } else {
            startHtml = '<span>';
            contents = String.format('<img src="{0}_disabled.gif" {1}/>', previousImgRoot, style);
            endHtml = '</span>';
        }

        html.push(startHtml + contents + endHtml);
        html.push(' | ');
        
        if (!pageManager.isLastPage(rowCount)) {
            startHtml = '<a href="#" class="nextPage NoTip">';
            contents = String.format('<img src="{0}.gif" {1}/>', nextImgRoot, style);
            endHtml = '</a>';
            haveLinks = true;
        } else {
            startHtml = '<span>';
            contents = String.format('<img src="{0}_disabled.gif" {1}/>', nextImgRoot, style);
            endHtml = '</span>';
        }

        html.push(startHtml + contents + endHtml);
        html.push('<a href="#" class="showAll NoTip">' + showAll + '</a>');

        pager.empty().append(html.join(' '));
        var method = haveLinks ? 'show' : 'hide';
        pager[method]();
       
        pager.find('.previousPage').click(function() {
            cq.createTableFromQuery(resourceId, pageIndex - 1, pageManager.rowsPerPage, primaryIP, undefined, columnIndexForFiltering, undefined, customSWQL, onFinished);
            return false;
        });

        pager.find('.nextPage').click(function() {
            cq.createTableFromQuery(resourceId, pageIndex + 1, pageManager.rowsPerPage, primaryIP, undefined, columnIndexForFiltering, undefined, customSWQL, onFinished);
            return false;
        });

        pager.find('.showAll').click(function() {
            // We don't have a good way to show all.  We'll show 1 million and 
            // accept that there's an issue if there are more than that :)
            cq.createTableFromQuery(resourceId, 0, 1000000, primaryIP, undefined, columnIndexForFiltering, undefined, customSWQL, onFinished);
            return false;
        });
    };
    
     RegExp.quote = function(str) {
         return str.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
     };

    var outputRows = undefined;
    var columnInfo = undefined;
    var fullOutputDone = false;


    cq.createTableFromQuery = function(resourceId, pageIndex, pageSize, primaryIP, customFilter, 
        columnIndexForFiltering, filterRegex, customSWQL, onFinished) 
    {
        var swql;
        if (customSWQL != undefined && customSWQL != null)
        {
            swql = customSWQL;
        } else{
            swql = $('#SWQL-' + resourceId).val();
        }
        var errorMsg = $('#ErrorMsg-' + resourceId);
        var grid = $('#Grid-' + resourceId);
        
        if (swql.trim().length === 0)
        {
            errorMsg.text('@{R=Core.Strings;K=WEBJS_TM1_CUSTOMQUERY_NOQUERY;E=js}').show();            
            return;
        }

        errorMsg.hide();

        var pageManager = new PageManager(pageIndex, pageSize);
        var swqlUpperCase = swql.toUpperCase();
        if (swqlUpperCase.indexOf("ORDER BY") === -1) {
            var commaIndex = swqlUpperCase.indexOf(",");
            var fromIndex = swqlUpperCase.indexOf("FROM");
            var selectIndex = swqlUpperCase.indexOf("SELECT");
            var endIndex = commaIndex;
            if (endIndex == -1)
            {
                endIndex = fromIndex;
            }

            var startIndex = selectIndex + 6;
            if (startIndex < endIndex) {
              var orderColumn = swql.substring(startIndex, endIndex).trim();
              var orderColumnBackspaceIndex =  orderColumn.indexOf(" ");
              if (orderColumnBackspaceIndex > -1)
              {
                 orderColumn = orderColumn.substring(0, orderColumnBackspaceIndex);
              }
              
              swql += "ORDER BY " + orderColumn;
            }
            else 
            {
                swql += " ORDER BY 1 ";
            }
        }
        
        swql += pageManager.withRowsClause();


        Information.Query(swql, function(result) {
            var headers = $('tr.HeaderRow', grid);
                   
            if (pageSize >= 1000000)
            {
                fullOutputDone = true;
            } else{
                fullOutputDone = false;
            }

            headers.empty();
            grid.find('tr:not(.HeaderRow)').remove();
            columnInfo = generateColumnInfo(result.Columns);      
            $.each(columnInfo, function(colIndex, column) {
                if (column.header !== null) {
                    $('<td/>')
                        .addClass('ReportHeader')
                        .css('text-transform', 'uppercase')
                        .text(column.header)
                        .appendTo(headers);
                }
            });
            
           outputRows = result.Rows.slice(0, pageManager.rowsPerPage);
           if ((customFilter !== undefined) && (customFilter !== null))
           {
                customFilter(resourceId, columnIndexForFiltering, primaryIP, filterRegex, swql);
           }else 
           {
                var primary = false; 

                $.each(outputRows, function(rowIndex, row) {
                    var tr = $('<tr/>');
                    if (row[columnIndexForFiltering] == primaryIP) 
                    {
                        tr.css('font-weight', 'bold');
                        row[columnIndexForFiltering] = row[columnIndexForFiltering] + ' @{R=Core.Strings;K=XMLDATA_TP0_1;E=xml}';
                        primary = true;
                    } else if (typeof(row[columnIndexForFiltering])=='string' && row[columnIndexForFiltering].indexOf('@{R=Core.Strings;K=XMLDATA_TP0_1;E=xml}') != -1)
                    {
                        tr.css('font-weight', 'bold');
                        primary = true;
                    } else{
                        primary = false;
                    }
                    $.each(row, function(cellIndex, cell) {
                        var info = columnInfo[cellIndex];
                        if (info.header !== null) 
                            renderCell(cell, row, info, resourceId).appendTo(tr);
                    });
                    if (primary)
                    {
                        $('#Grid-' + resourceId + ' tr:first').after(tr);
                    } else{
                        grid.append(tr);
                    }
                });
                grid.find('tr:not(.HeaderRow):odd').css('background-color','#ECF6FB');
                
                updatePagerControls(resourceId, pageManager, result.Rows.length, primaryIP, customSWQL, columnIndexForFiltering, onFinished);
            }

            if (onFinished != undefined)
            {
                onFinished();
            }
        }, function(error) {
            errorMsg.text(error.get_message()).show();
        });
    };

    // http://stackoverflow.com/questions/494035/how-do-you-pass-a-variable-to-a-regular-expression-javascript/494122#494122
    RegExp.quote = function(str) {
     return str.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
    };

    cq.FilterInResult = function(resourceId, columnIndexForFiltering, primaryIP, filterRegex, customSWISQuery, onFinished) {
          if (!fullOutputDone)
          {
                cq.createTableFromQuery(resourceId, 0, 1000000, primaryIP, cq.FilterInResult, 
                                    columnIndexForFiltering, filterRegex, customSWISQuery, onFinished);
          } else
          {
             
              if (filterRegex === undefined || filterRegex === null)
              {
                return;
              }

              var grid = $('#Grid-' + resourceId);
              grid.find('tr:not(.HeaderRow)').remove();            
              $('#Pager-'+ resourceId).empty();

              var primary = false;
              var foundResults = 0;
              var maximumAllowed = 100;
              var filteredOutputRows = new Array();

              $.each(outputRows, function(rowIndex, row) {
                    var re = new RegExp(RegExp.quote(filterRegex), "g");
                    var m = re.exec(row[columnIndexForFiltering]);
                    if (m !== null)
                    {
                        if (++foundResults >= maximumAllowed)
                        {
                            var tr = $('<tr/>');
                            grid.find('tr:not(.HeaderRow)').remove();
                            $('#Pager-'+ resourceId).empty();                            
                            tr.append($('<td/>').append($('<span style="color:Red"/>').text('@{R=Core.Strings;K=XMLDATA_TP0_5;E=xml}')));
                            tr.append($('<td/>'));
                            grid.append(tr);
                            filteredOutputRows = new Array();
                            return false;
                        } else {
                            filteredOutputRows.push(row);
                        }
                    }
                });


              $.each(filteredOutputRows, function(rowIndex, row) {
                    var tr = $('<tr/>');
                                          
                    if (row[columnIndexForFiltering] == primaryIP) 
                    {
                        tr.css('font-weight', 'bold');
                        row[columnIndexForFiltering] = row[columnIndexForFiltering] + ' @{R=Core.Strings;K=XMLDATA_TP0_1;E=xml}';
                        primary = true;
                    } else if (typeof(row[columnIndexForFiltering])=='string' && row[columnIndexForFiltering].indexOf('@{R=Core.Strings;K=XMLDATA_TP0_1;E=xml}') != -1)
                    {
                        tr.css('font-weight', 'bold');
                        primary = true;
                    } else{
                        primary = false;
                    }
                    $.each(row, function(cellIndex, cell) {
                        var info = columnInfo[cellIndex];
                    
                        if (info.header !== null)
                        {
                            renderCell(cell, row, info, resourceId).appendTo(tr);
                        }
                    });

                    if (primary)
                    {
                        $('#Grid-' + resourceId + ' tr:first').after(tr);
                    } else{
                        grid.append(tr);
                    }
                });
            
            grid.find('tr:not(.HeaderRow):odd').css('background-color','#ECF6FB');            
        }

    };

})(SW.Core.Resources.NodeIPAddressesQuery);
