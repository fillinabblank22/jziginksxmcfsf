﻿using SolarWinds.Orion.Core.Common.PackageManager;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Dependencies;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.ServiceModel;
using System.Web.UI;


[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_NetPerfMon_Resources_NodeDetails_NodeDependencies : BaseResourceControl
{
    public string NetObectId
	{
        get
        {
            Node node = GetInterfaceInstance<INodeProvider>().Node;
            return node != null ? node.NetObjectID : Request["netobject"] ?? "";
        }
	}

	protected bool AutoHide
	{
		get
		{
			if (string.IsNullOrEmpty(Resource.Properties["AutoHide"]))
				return false;
			return Resource.Properties["AutoHide"].Equals("1");
		}
	}

	protected override void OnInit(EventArgs e)
	{
        Wrapper.ManageButtonText = Resources.CoreWebContent.ResourcesAll_ManageDependencies;
		Wrapper.ManageButtonTarget = "~/Orion/Admin/DependenciesView.aspx";
		Wrapper.ShowManageButton = Profile.AllowAdmin || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;

		string parentFilter = string.IsNullOrEmpty(Resource.Properties["ParentFilter"]) ? string.Empty : Resource.Properties["ParentFilter"];
		string childFilter = string.IsNullOrEmpty(Resource.Properties["ChildFilter"]) ? string.Empty : Resource.Properties["ChildFilter"];
		string dependencyFilter = string.IsNullOrEmpty(Resource.Properties["DependencyFilter"]) ? string.Empty : Resource.Properties["DependencyFilter"];
	    var filter = $"{parentFilter} {childFilter} {dependencyFilter}";

        try
		{
			DependencyCache.UpdateDependencyCache(NetObectId, parentFilter, childFilter, dependencyFilter);
		}
        catch (Exception ex)
        {
            var errMessage = HasSWQLFilterError(ex, filter) ? string.Format(Resources.CoreWebContent.WEBCODE_VB0_32, ex.Message, filter) : ex.Message;
            errorMessage.Controls.Clear();
            errorMessage.Controls.Add(new LiteralControl(errMessage));
            errorMessage.Visible = true;
        }

        base.OnInit(e);
	}

	protected override void OnPreRender(EventArgs e)
	{
		// hide resource if there aren't any data
		if (AutoHide)
		{
			using (var dal = new DependenciesDAL())
                if (dal.GetAllDependenciesFilteredCount(GetInterfaceInstance<INodeProvider>().Node.NodeID, "Orion.Nodes", null, null, null) == 0)
					Wrapper.Visible = false;
		}

		base.OnPreRender(e);
	}

	protected override string DefaultTitle
	{
		get { return Resources.CoreWebContent.WEBCODE_VB0_33; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceNodeDependencies"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

	public override string EditControlLocation
	{
		get
		{
			return "/Orion/NetPerfMon/Controls/EditResourceControls/EditDependencies.ascx";
		}
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }
}
