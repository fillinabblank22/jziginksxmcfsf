﻿using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Resources_Traps_EditTrapsSummary : System.Web.UI.Page
{
    protected ResourceInfo Resource { get; set; }

	protected override void OnInit(EventArgs e)
	{
		int resourceID = 0;
		Int32.TryParse(Request.QueryString["ResourceID"], out resourceID);
		this.Resource = ResourceManager.GetResourceByID(resourceID);

		if (this.Resource != null)
		{
			//this.titleLabel.Text = CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(this.Resource.Title, Request["netobject"]));
			this.titleEditor.ResourceTitle = this.Resource.Title;

			if (!String.IsNullOrEmpty(this.Resource.Properties["Period"]))
			{
				this.periodEditor.PeriodName = this.Resource.Properties["Period"];
			}

			if (!String.IsNullOrEmpty(this.Resource.Properties["GroupBy"]))
			{
				this.groupBy.SelectedValue = this.Resource.Properties["GroupBy"];
			}
		}
		base.OnInit(e);
    }

	protected void SubmitClick(object sender, EventArgs e)
	{
		if (this.Resource == null) return;

		this.Resource.Properties["Period"] = this.periodEditor.PeriodName;
		this.Resource.Properties["GroupBy"] = this.groupBy.SelectedValue;
        if (!String.IsNullOrEmpty(titleEditor.ResourceTitle))
        {
            Resource.Title = titleEditor.ResourceTitle;
        }
        else
        {
            var resourceControl = (BaseResourceControl)LoadControl(Resource.File);
            Resource.Title = resourceControl.Title;
        }

		ResourcesDAL.Update(this.Resource);

		string netObjectID = Request.QueryString["NetObject"];
		string url = string.Format("/Orion/View.aspx?ViewID={0}", Request.QueryString["ViewID"]);
		if (!string.IsNullOrEmpty(netObjectID))
			url = string.Format("{0}&NetObject={1}", url, this.Request.QueryString["NetObject"]);
		this.Response.Redirect(url);
	}
}
