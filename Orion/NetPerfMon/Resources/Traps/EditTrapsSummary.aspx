<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true" CodeFile="EditTrapsSummary.aspx.cs" Inherits="Orion_NetPerfMon_Resources_Traps_EditTrapsSummary" %>
<%@ Import Namespace="System.Web.Security.AntiXss" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/PeriodControl.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= string.Format(Resources.SyslogTrapsWebContent.WEBCODE_AK0_88, CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Resource.Title, AntiXssEncoder.HtmlEncode(Request.QueryString["netobject"],true))))%></h1>
    <table width="auto">
        <tr><td colspan="3">
            <orion:EditResourceTitle runat="server" ID="titleEditor" ShowSubTitle="false" ShowSubTitleHintMessage="false" />    
           <%= Resources.SyslogTrapsWebContent.WEBDATA_TM0_1 %>
        </td></tr>            
        <orion:EditPeriod runat="server" ID="periodEditor" PeriodLabel="<%$ Resources: SyslogTrapsWebContent, WEBDATA_TM0_2 %>" InLine="true"/>
        <tr>
            <td colspan="3"><b><%= Resources.SyslogTrapsWebContent.WEBDATA_TM0_3 %></b><br/>
            <asp:DropDownList runat="server" id="groupBy">
                <asp:ListItem Text="<%$ Resources: SyslogTrapsWebContent, WEBDATA_VB0_13 %>" Value="IPAddress" />
                <asp:ListItem Text="<%$ Resources: SyslogTrapsWebContent, WEBDATA_TM0_4 %>" Value="Hostname" />
                <asp:ListItem Text="<%$ Resources: SyslogTrapsWebContent, WEBDATA_TM0_5 %>" Value="TrapType" Selected="True" />
                <asp:ListItem Text="<%$ Resources: SyslogTrapsWebContent, WEBDATA_VB0_16 %>" Value="MachineType" />
            </asp:DropDownList></td>
        </tr>
    </table>
    <div class="sw-btn-bar"><orion:LocalizableButton runat="server" ID="btnSubmit" OnClick="SubmitClick" LocalizedText="Submit" DisplayType="Primary" /></div>
</asp:Content>
