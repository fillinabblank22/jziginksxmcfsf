<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true" CodeFile="EditLastXXTraps.aspx.cs" Inherits="Orion_NetPerfMon_Resources_Traps_EditLastXXTraps" %>
<%@ Import Namespace="System.Web.Security.AntiXss" %>
<%@ Register TagPrefix="orion" TagName="EditFilter" Src="~/Orion/Controls/FilterNodesSql.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/PeriodControl.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= string.Format(Resources.SyslogTrapsWebContent.WEBCODE_AK0_88, CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Resource.Title, AntiXssEncoder.HtmlEncode(Request.QueryString["netobject"],true))))%></h1>
    <table>
        <tr><td colspan="3">
            <b><%= Resources.SyslogTrapsWebContent.WEBDATA_TM0_7 %></b><br />
            <input runat="server" id="maxMessages" size="5"/>
        </td></tr>
        <orion:EditPeriod runat="server" ID="periodEditor" PeriodLabel="<%$ Resources: SyslogTrapsWebContent, WEBDATA_TM0_8 %>" InLine="true"/>
        <tr><td colspan="3" width="750px">
            <orion:EditFilter runat="server" ID="filterEditor" />
        </td></tr>            
    </table>
    <div class="sw-btn-bar"><orion:LocalizableButton runat="server" ID="btnSubmit" OnClick="SubmitClick" LocalizedText="Submit" DisplayType="Primary" /></div>
</asp:Content>
