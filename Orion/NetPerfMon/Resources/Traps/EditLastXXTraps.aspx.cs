﻿using System;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web;

public partial class Orion_NetPerfMon_Resources_Traps_EditLastXXTraps : System.Web.UI.Page
{
    protected ResourceInfo Resource { get; set; }

	protected override void OnInit(EventArgs e)
	{
		int resourceID = 0;
		Int32.TryParse(Request.QueryString["ResourceID"], out resourceID);
		this.Resource = ResourceManager.GetResourceByID(resourceID);

		if (this.Resource != null)
		{
			this.filterEditor.FilterTextBox.Text = this.Resource.Properties["Filter"];

			if (!String.IsNullOrEmpty(this.Resource.Properties["Period"]))
			{
				this.periodEditor.PeriodName = this.Resource.Properties["Period"];
			}

			if (!String.IsNullOrEmpty(this.Resource.Properties["MaxMessages"]))
			{
				this.maxMessages.Value = this.Resource.Properties["MaxMessages"];
			}
			else
			{
				this.maxMessages.Value = DefaultNumberOfMessages;
			}
		}
		base.OnInit(e);
    }

    private const string DefaultNumberOfMessages = "25";
	protected void SubmitClick(object sender, EventArgs e)
	{
		if (this.Resource == null) return;

		this.Resource.Properties["Period"] = this.periodEditor.PeriodName;
		this.Resource.Properties["Filter"] = this.filterEditor.FilterTextBox.Text;

		try
		{
			Int32.Parse(this.maxMessages.Value);
			this.Resource.Properties["MaxMessages"] = this.maxMessages.Value;
		}
		catch
		{
			this.Resource.Properties["MaxMessages"] = DefaultNumberOfMessages;
		}

		ResourcesDAL.Update(this.Resource);

		string netObjectID = Request.QueryString["NetObject"];
		string url = string.Format("/Orion/View.aspx?ViewID={0}", Request.QueryString["ViewID"]);
		if (!string.IsNullOrEmpty(netObjectID))
			url = string.Format("{0}&NetObject={1}", url, this.Request.QueryString["NetObject"]);
		this.Response.Redirect(url);
	}
}
