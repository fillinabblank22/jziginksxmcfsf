<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LastXXTraps.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Traps_LastXXTraps" %>

<orion:resourceWrapper runat="server">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%=this.CustomSqlErrorMessage %>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="trapsTable">
            <HeaderTemplate>
                <table width="100%" cellpadding="2" cellspacing="0">
                    <tr>
                        <td class="PropertyHeader"><%= Resources.SyslogTrapsWebContent.WEBDATA_TM0_9 %></td>
                        <td class="PropertyHeader"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_13 %></td>
                        <td class="PropertyHeader"><%= Resources.SyslogTrapsWebContent.WEBDATA_TM0_10 %></td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr style="background-color:#<%#Eval("Color")%>">
                    <td class="Property"><%# this.FormatDateTime(Eval("DateTime")) %>&nbsp;</td>
                    <td class="Property"><%# Eval("Link") %>&nbsp;</td>
                    <td class="Property"><%# Eval("Details") %></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>        
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>