using System;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SyslogTraps.Web.DAL;
using TrapsUI = SolarWinds.SyslogTraps.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Traps)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_Traps_TrapsSummary : TrapsUI.TrapsResourceControl
{
    public Orion_NetPerfMon_Resources_Traps_TrapsSummary()
        : base("Traps")
    {
    }
    protected string Period
    {
        get
        {
            string value = this.Resource.Properties["Period"];
            if (String.IsNullOrEmpty(value))
            {
                value = "Today";
            }

            return value;
        }
    }

    // this resource uses Period property for date time filter
    //public override bool SupportDateTimeFilter
    //{
    //    get { return true; }
    //}

    private string groupBy;
    protected string GroupBy
    {
        get
        {
            if (String.IsNullOrEmpty(groupBy))
            {
                string value = this.Resource.Properties["GroupBy"];
                if (String.IsNullOrEmpty(value))
                {
                    value = "TrapType";
                }
                groupBy = value;
                return groupBy;
            }
            else
            {
                return groupBy;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        int nodeId = -1;
        var temp = this.Request.QueryString["NetObject"];

        if (temp != null)
            if (NetObjectHelper.IsNodeNetObject(temp))
                nodeId = Convert.ToInt32(NetObjectHelper.GetObjectID(temp));

        DataTable table = SqlDAL.GetTrapSummary(this.Period, nodeId, this.GroupBy);
        bool encodeTraps = false;
        bool.TryParse(ConfigurationManager.AppSettings["HTMLEncodeTraps"], out encodeTraps);
        if (table != null && table.Rows.Count > 0)
        {
            if (encodeTraps)
            {
                foreach (DataRow trapRow in table.Rows)
                {
                    for (int colI = 0; colI < table.Columns.Count; colI++)
                    {
                        if (trapRow[colI] != DBNull.Value)
                            trapRow[colI] = System.Web.HttpUtility.HtmlEncode(trapRow[colI]);
                    }
                }
            }

            this.trapsTable.DataSource = table;
            this.trapsTable.DataBind();
        }
    }


    protected override string DefaultTitle
    {
        get { return Resources.SyslogTrapsWebContent.WEBCODE_TM0_2; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceTraps"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditTrapsSummary.ascx"; }
    }

    public override string SubTitle
    {
        get
        {
            // Localizing Period subtitle
            return Periods.GetLocalizedPeriod(this.Period);
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    protected string GetNetObject(object nodeId)
    {
        if (!String.IsNullOrEmpty(this.Request.QueryString["NetObject"]))
        {
            return this.Request.QueryString["NetObject"];
        }
        return String.Format("N:{0}", nodeId);
    }

    protected string ReformatGroupBy(string groupBy)
    {
        if (!groupBy.Equals("Hostname", StringComparison.InvariantCultureIgnoreCase))
        {
            return "NodeID";
        }
        else
        {
            return groupBy;
        }
    }

    protected void trapsTable_OnItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView row = e.Item.DataItem as DataRowView;

            if (groupBy.Equals("Hostname", StringComparison.InvariantCultureIgnoreCase))
            {
                int id = Convert.ToInt32(row["GroupedNodeID"]);
                string href = String.Empty;

                if (id > 0)
                {
                    href = String.Format(@"/Orion/NetPerfMon/Traps.aspx?Period={0}&NetObject=N:{1}", this.Period, row["GroupedNodeID"]);
                }
                else
                {
                    href = @"/Orion/NetPerfMon/Traps.aspx";
                }

                ((HtmlAnchor)e.Item.FindControl("link1")).HRef = href;
                ((HtmlAnchor)e.Item.FindControl("link2")).HRef = href;
            }
            else
            {
                string href = String.Format(@"/Orion/NetPerfMon/Traps.aspx?Period={0}&{1}={2}", this.Period, this.GroupBy, row[this.groupBy]);
                ((HtmlAnchor)e.Item.FindControl("link1")).HRef = href;
                ((HtmlAnchor)e.Item.FindControl("link2")).HRef = href;
            }
        }
    }

    /// <summary>
    /// used for localization of Group By
    /// [Localization]
    /// </summary>
    /// <returns>either localized group by, or original one</returns>
    public string LocalizedGroupBy
    {
        get
        {
            switch (groupBy)
            {
                case "IPAddress": return Resources.SyslogTrapsWebContent.WEBDATA_AK0_154;
                case "Hostname": return Resources.SyslogTrapsWebContent.WEBDATA_TM0_4;
                case "TrapType": return Resources.SyslogTrapsWebContent.WEBDATA_TM0_65;
                case "MachineType": return Resources.SyslogTrapsWebContent.WEBDATA_TM0_66;
            }
            return Resources.SyslogTrapsWebContent.WEBDATA_TM0_65;
        }
    }

}
