using System;
using System.Configuration;
using System.Data;
using System.Web;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SyslogTraps.Web.DAL;
using TrapsUI = SolarWinds.SyslogTraps.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Traps)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_Traps_LastXXTraps : TrapsUI.TrapsResourceControl
{
    private const int DefaultMaxMessages = 25;

    public Orion_NetPerfMon_Resources_Traps_LastXXTraps() : base("Traps")
    {
    }

    protected int MaxMessages
    {
        get
        {
            int value;
            if (!Int32.TryParse(this.Resource.Properties["MaxMessages"], out value))
            {
                value = DefaultMaxMessages;
            }
            return value;
        }
    }

    protected string Period
    {
        get
        {
            string value = this.Resource.Properties["Period"];
            if (String.IsNullOrEmpty(value))
            {
                value = "Today";
            }
            return value;
        }
    }

    // this resource uses Period property for date time filter
    //public override bool SupportDateTimeFilter
    //{
    //    get { return true; }
    //}

    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable table = null;

        try
        {
            table = SqlDAL.GetTraps(true, this.Resource.Properties["Filter"], this.Period, this.Request.QueryString["NetObject"], null, this.MaxMessages);
        }
        catch
        {
            this.SQLErrorPanel.Visible = true;
            return;
        }

        DataTable newTable = new DataTable();
        newTable.Columns.Add("DateTime", typeof(DateTime));
        newTable.Columns.Add("Link", typeof(string));
        newTable.Columns.Add("Details", typeof(string));
        newTable.Columns.Add("Color", typeof(string));

        DataRow newRow = null;
        long trapID = 0;
        bool encodeTraps = false;
        bool.TryParse(ConfigurationManager.AppSettings["HTMLEncodeTraps"], out encodeTraps);
        foreach (DataRow row in table.Rows)
        {
            if ((long)row["TrapID"] != trapID)
            {
                newRow = newTable.NewRow();
                newTable.Rows.Add(newRow);
                trapID = (long)row["TrapID"];
                newRow["DateTime"] = row["DateTime"];
                object nodeId = row["LinkNodeID"];
                if (nodeId != null && nodeId != DBNull.Value && (int)nodeId != 0)
                {
                    newRow["Link"] = String.Format("<a href=\"/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}\">{1}</a>",
                        nodeId, encodeTraps ? HttpUtility.HtmlEncode(row["IPAddress"]) : row["IPAddress"]);
                }
                else
                {
                    if (encodeTraps)
                    {
                        newRow["Link"] = HttpUtility.HtmlEncode(row["IPAddress"]);
                    }
                    else
                    {
                        newRow["Link"] = row["IPAddress"];
                    }
                }

                if (encodeTraps)
                {
                    newRow["Details"] = FormatHelper.MakeBreakableString(String.Format("{0} = {1}", HttpUtility.HtmlEncode(row["OIDName"]), HttpUtility.HtmlEncode(row["OIDValue"])), true);
                }
                else
                {
                    newRow["Details"] = FormatHelper.MakeBreakableString(String.Format("{0} = {1}", row["OIDName"], row["OIDValue"]), true);
                }

                newRow["Color"] = GetHexColor(row["ColorCode"]);
            }
            else
            {
                if (encodeTraps)
                {
                    newRow["Details"] = (string)newRow["Details"] +
                                        String.Format(" <br /> {0}",
                                            FormatHelper.MakeBreakableString(
                                                String.Format("{0} = {1}", HttpUtility.HtmlEncode(row["OIDName"]), HttpUtility.HtmlEncode(row["OIDValue"])), true));
                }
                else
                {
                    newRow["Details"] = (string)newRow["Details"] +
                                        String.Format(" <br /> {0}",
                                            FormatHelper.MakeBreakableString(
                                                String.Format("{0} = {1}", row["OIDName"], row["OIDValue"]), true));
                }
            }
        }

        this.trapsTable.DataSource = newTable;
        this.trapsTable.DataBind();
    }


    protected override string DefaultTitle
    {
        get { return Resources.SyslogTrapsWebContent.WEBCODE_TM0_1; }
    }

    public override string DisplayTitle
    {
        get
        {
            if (Resource.Title.Equals(DefaultTitle, StringComparison.OrdinalIgnoreCase))
            {
                string result = Resource.Properties["MaxMessages"];

                if (String.IsNullOrEmpty(result))
                    result = DefaultMaxMessages.ToString();


                return String.Format(Resources.SyslogTrapsWebContent.WEBCODE_TM0_3, result);
            }
            else
                return Resource.Title;
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceLastXTraps"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditLastXXTraps.ascx"; }
    }

    public override string SubTitle
    {
        get
        {
            return Periods.GetLocalizedPeriod(this.Period);
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    protected string FormatDateTime(object timeOfTrap)
    {
        if (timeOfTrap == null || timeOfTrap == DBNull.Value)
        {
            return String.Empty;
        }
        DateTime time = Convert.ToDateTime(timeOfTrap);
        if (time.Date == DateTime.Today)
        {
            return Utils.FormatToLocalTime(time);
        }
        return Utils.FormatCurrentCultureDate(time) + " " + Utils.FormatToLocalTime(time);
    }

    private static string GetHexColor(object value)
    {
        try
        {
            int color = Convert.ToInt32(value);
            string hexColor = color.ToString("X").PadLeft(6, '0');
            // transform BGR to RGB
            return hexColor.Substring(4, 2) + hexColor.Substring(2, 2) + hexColor.Substring(0, 2);
        }
        catch
        {
            return "FFFFFF";
        }
    }

}
