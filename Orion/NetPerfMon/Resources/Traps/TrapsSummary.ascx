<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TrapsSummary.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Traps_TrapsSummary" %>

<orion:resourceWrapper runat="server">
    <Content>
        <asp:Repeater runat="server" ID="trapsTable" OnItemDataBound="trapsTable_OnItemDataBound">
            <HeaderTemplate>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr class="ViewHeader">
                        <td style="padding-left: 5px;"><%= this.LocalizedGroupBy%></td>
                        <td align="center"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_39 %></td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td class="Event" style="padding-left: 5px;"><a runat="server" id="link1"><%# Eval(this.GroupBy) %></a> &nbsp;</td>
                    <td class="Event" align="center"><a runat="server" id="link2"><b><%# Eval("Total")%></b></a>&nbsp;</td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>        
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>