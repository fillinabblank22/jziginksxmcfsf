﻿using System;
using System.Collections.Generic;
using System.Globalization;
using SolarWinds.Logging;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_ForecastingCharts_CapacityForecastingMemoryChart : StandardChartResource, IResourceIsInternal
{
    private static readonly Log _log = new Log();

    protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);
    }
    
    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override string NetObjectPrefix
    {
        get { return "N"; }
    }
        
    protected override IEnumerable<string> GetElementIdsForChart()
    {
        var multipleNodeProvider = GetInterfaceInstance<IMultiNodeProvider>();
        if (multipleNodeProvider != null)
        {
            var netObjects = multipleNodeProvider.SelectedNetObjects;
            if (netObjects.Length != 0)
                return netObjects; 
        }

        var nodeProvider = GetInterfaceInstance<INodeProvider>();
        if (nodeProvider != null)
        {
            return new[] { nodeProvider.Node.NodeID.ToString(CultureInfo.InvariantCulture) };
        }

        return new string[0];
    }

    protected override string DefaultTitle
    {
        get { return Resources.CoreWebContent.NodeChartsV2_Title_4; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(INodeProvider) }; }
    }

    public override IEnumerable<Type> SupportedInterfaces
    {
        get { return new[] { typeof(INodeProvider)}; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public bool IsInternal
    {
        get { return true; }
    }

}

