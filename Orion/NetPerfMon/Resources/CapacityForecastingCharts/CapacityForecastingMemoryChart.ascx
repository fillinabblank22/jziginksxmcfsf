﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CapacityForecastingMemoryChart.ascx.cs" Inherits="Orion_NetPerfMon_Resources_ForecastingCharts_CapacityForecastingMemoryChart" %>
<orion:Include ID="include1" runat="server" File="OrionMinReqs.js" />
<orion:Include ID="include2" runat="server" File="OrionCore.js" />

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:PlaceHolder runat="server" ID="WrapperContents"></asp:PlaceHolder>
    </Content>
</orion:resourceWrapper> 