using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security.AntiXss;
using SolarWinds.AppStack;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")] // reporting has some visual glitches: FB362957, FB362964, FB362959
public partial class Orion_NetPerfMon_Resources_AppStack_AppStack : ResourceControl, IResourceHasSupportedInterfaceMode
{
    private string _appstackId = null;
    private NetObject _netObject = null;

    protected virtual bool IsSummary
    {
        get
        {
            return false;
        }
    }

    public override string DisplayTitle
    {
        get
        {
            // reports look stupid due to "Environment for X for X".

            if (Resource != null && Resource.IsInReport && base.DisplayTitle == SolarWinds.AppStack.Strings.Archive.AppStack_DefaultPageTitle)
            {
                return SolarWinds.AppStack.Strings.Archive.AppStack_SummaryPageTitle;
            }
            else if (_netObject != null)
            {
                return AntiXssEncoder.HtmlEncode(Macros.ParseMacros(base.DisplayTitle, _netObject), false);
            }
            else
            {
                return base.DisplayTitle;
            }
        }
    }

    public Guid TraceId { get; set; }

    protected override void OnLoad(EventArgs e)
    {
        try
        {
            if (IsSummary == false)
            {
                _appstackId = GetAppstackIdFromEnvironment();

                if (_appstackId == null)
                {
                    Sections.Visible = false;
                    this.Visible = false;
                    return;
                }
            }
            else
            {
                OrionInclude.CoreFile("charts/js/allcharts.js", OrionInclude.Section.Bottom);
            }

            var factory = new AppStackFactory();

            var config = factory
                .Config()
                .InitForResource(TraceId, Resource.ID, _appstackId);

            btnViewAll.NavigateUrl = factory.UiUtility().GetEnvironmentUrl(config.Settings, config.SelectedIds);

            config.Settings.SpotlightEnabled = IsSummary == false;

            var c = factory.Environment(config);

			Sections.Environment = c.AllSectionsPartial(config.SelectedIds);

            var errorMessages = Sections.Environment.SwisErrors;
            if (errorMessages != null && errorMessages.Any())
            {
                Sections.ErrorMessages = errorMessages.Select(err => new SolarWinds.Orion.Web.Federation.SwisErrorMessage(err)).ToList();
            }				

            if (IsSummary == false)
            {
                var found = Sections.Environment.Model.Where(x => x.Icons != null).SelectMany(x => x.Icons).Any(x => x.IsSelected);
                var anyErrorMessage = errorMessages != null && errorMessages.Any();

                if (found == false && anyErrorMessage == false)
                {
                    Sections.Visible = false;
                    this.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            Sections.Visible = false;
            HandleResourceException(ex);
        }
    }

    private string GetAppstackIdFromEnvironment()
    {
        var provider = GetInterfaceInstance<INetObjectProvider>();

        if (provider == null)
            return null;

        _netObject = provider.NetObject;
        if (_netObject == null)
            return null;

        var integration = new SolarWinds.AppStack.Contract.Integration.AppStackIntegrateFactory()
            .Start("app");

        // -- see if ministack is requested to be turned off.

        var visibilityOverride = integration
            .MiniStackResolve<SolarWinds.AppStack.Contract.Web.MiniStackVisibility>(_netObject.NetObjectID, this);

        if (visibilityOverride != null && visibilityOverride.IsHidden)
            return null;

        // -- see if there is a requested override of the netobject.

        var netObjectOverride = integration.MiniStackResolve<NetObject>(_netObject.NetObjectID, this);

        if (netObjectOverride != null)
            _netObject = netObjectOverride;

        var entityName = NetObjectFactory.GetSWISTypeByNetObject(_netObject.NetObjectID);

        string supportedEntity = new AppStackFactory().Participation().MapToSupportedEntity(entityName);

        if (string.IsNullOrWhiteSpace(supportedEntity))
            return null;

        return supportedEntity + ":" + _netObject.NetObjectID.Substring(_netObject.NetObjectID.IndexOf(':') + 1);
    }

	protected string ReturnUrl
    {
        get { return ReferrerRedirectorBase.GetReturnUrl(); }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/AppStack/Controls/EditAppStack.ascx"; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceAppStack"; }
    }

    protected override string DefaultTitle
    {
        get
        {
            return IsSummary
                ? SolarWinds.AppStack.Strings.Archive.AppStack_SummaryPageTitle
                : SolarWinds.AppStack.Strings.Archive.AppStack_DefaultPageTitle;
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[0]; }
    }

    public override IEnumerable<Type> SupportedInterfaces
    {
        get
        {
            if (IsSummary)
                return Enumerable.Empty<Type>();

            // the inetobjectprovider is too generic, it'll target everything.

            var entities =  new AppStackFactory().Participation().SupportedEntities;

            var types = ViewManager.GetSupportedInterfacesForSwisEnities(entities).Where( x => x != typeof(INetObjectProvider));

            return types;
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.RenderControl;

    public ResourceTemplateSupportedInterfacesMode SupportedInterfaceMode
    {
        get
        {
            return IsSummary ? ResourceTemplateSupportedInterfacesMode.NeedNone : ResourceTemplateSupportedInterfacesMode.NeedOne;
        }
    }
}
