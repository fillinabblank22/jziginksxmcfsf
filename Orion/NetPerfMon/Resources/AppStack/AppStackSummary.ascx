<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AppStackSummary.ascx.cs" Inherits="Orion_NetPerfMon_Resources_AppStack_AppStackSummary" %>
<%@ Register TagPrefix="appstack" Namespace="SolarWinds.AppStack.Web" Assembly="SolarWinds.AppStack" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
	    <div class="sw-appstack-ministack">
	        
            <div id="appstackoverview" class="sw-appstack-sel-pane">

                <table style="border-collapse: collapse; width: 100%;">
                    <tr style="vertical-align: middle;">
                        <td style="padding-left: 4px; padding-top: 2px; white-space: nowrap;" class="sw-appstack-chart-parent"><span class="sw-appstack-overview-label" style="text-transform: uppercase; color: #888; font-size: 11px;"><%: SolarWinds.AppStack.Strings.Archive.AppStack_OverviewTitle %></span>  <div class="sw-appstack-chart"></div></td>
                        <td style="width: 1px; white-space: nowrap; font-size: 11px; background: url(/orion/appstack/images/pane.splitter.png) center right no-repeat; padding-right: 10px;"><div class="sw-appstack-pie-count" sw:textselection="<%: SolarWinds.AppStack.Strings.Archive.AppStack_LbObjectCountSelected %>" sw:textoverview="<%: SolarWinds.AppStack.Strings.Archive.AppStack_LbObjectCountOverview %>"></div></td>
                        <td style="padding-left: 16px; font-size: 11px;"><span style="text-transform: uppercase; color: #888;"><%: SolarWinds.AppStack.Strings.Archive.AppStack_lbIssues %></span>
                            <div class="sw-appstack-issues"/>
                        </td>
                    </tr>
                </table>
            </div>

            <appstack:IconSectionFrameControl runat="server" ID="Sections"/>
            <div style="text-align:right; width:100%">
                <asp:HyperLink ID="btnViewAll" runat="server" NavigateUrl="~/Orion/AppStack/" Target="New" CssClass="sw-suggestion-link sw-ministack-view-entire-environment"><%= SolarWinds.AppStack.Strings.Archive.AppStack_ViewEntireEnvironment %></asp:HyperLink>
            </div>
        </div>
	</Content>
</orion:resourceWrapper>
