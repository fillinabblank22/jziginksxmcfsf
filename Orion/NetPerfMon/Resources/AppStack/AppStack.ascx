<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AppStack.ascx.cs" Inherits="Orion_NetPerfMon_Resources_AppStack_AppStack" %>
<%@ Register TagPrefix="appstack" Namespace="SolarWinds.AppStack.Web" Assembly="SolarWinds.AppStack" %>

<orion:resourceWrapper runat="server" ID="Wrapper" >
	<Content>
	    <div class="sw-appstack-ministack">
            <appstack:IconSectionFrameControl runat="server" ID="Sections"/>
            <div style="text-align:right; width:100%">
                <asp:HyperLink ID="btnViewAll" runat="server" NavigateUrl="~/Orion/AppStack/" Target="New" CssClass="sw-suggestion-link sw-ministack-view-entire-environment"><%= SolarWinds.AppStack.Strings.Archive.AppStack_ViewEntireEnvironment %></asp:HyperLink>
            </div>
        </div>
	</Content>
</orion:resourceWrapper>
