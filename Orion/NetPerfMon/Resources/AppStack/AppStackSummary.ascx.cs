using System;
using System.Linq;
using SolarWinds.AppStack;
using SolarWinds.AppStack.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")] // reporting has some visual glitches: FB362957, FB362964, FB362959
public partial class Orion_NetPerfMon_Resources_AppStack_AppStackSummary : AppStackBaseResourceControl
{
    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.RenderControl;

    protected override bool IsSummary
    {
        get { return true; }
    }

    protected override string DefaultTitle
    {
        get
        {
            return SolarWinds.AppStack.Strings.Archive.AppStack_SummaryPageTitle;
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        try
        {
            OrionInclude.CoreFile("charts/js/allcharts.js", OrionInclude.Section.Bottom);

            var factory = new AppStackFactory();

            var config = factory
                .Config()
                .InitForResource(TraceId, Resource.ID, null);

            btnViewAll.NavigateUrl = factory.UiUtility().GetEnvironmentUrl(config.Settings, config.SelectedIds);

            config.Settings.SpotlightEnabled = false;

            var c = factory.Environment(config);

            Sections.Environment = c.AllSectionsPartial(config.SelectedIds);

            var errorMessages = Sections.Environment.SwisErrors;
            if (errorMessages != null && errorMessages.Any())
            {
                Sections.ErrorMessages = errorMessages.Select(err => new SolarWinds.Orion.Web.Federation.SwisErrorMessage(err)).ToList();
            }
        }
        catch (Exception ex)
        {
            Sections.Visible = false;
            HandleResourceException(ex);
        }
    }

    public override string DisplayTitle
    {
        get
        {
            // reports look stupid due to "Environment for X for X".

            if (Resource != null && Resource.IsInReport && base.DisplayTitle == SolarWinds.AppStack.Strings.Archive.AppStack_DefaultPageTitle)
            {
                return SolarWinds.AppStack.Strings.Archive.AppStack_SummaryPageTitle;
            }

            return base.DisplayTitle;
        }
    }
}
