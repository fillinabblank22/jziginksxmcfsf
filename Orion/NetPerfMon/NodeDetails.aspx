<%@ Page Language="C#" MasterPageFile="~/Orion/View.master" AutoEventWireup="true" 
         CodeFile="NodeDetails.aspx.cs" Inherits="Orion_NetPerfMon_NodeDetails" 
         Title="Node Details" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="orion" TagName="StatusIconControl" Src="~/Orion/NetPerfMon/Controls/StatusIconControl.ascx" %>
<%@ Register TagPrefix="netperfmon" Assembly="NetPerfMonWeb" Namespace="SolarWinds.Orion.NPM.Web.UI" %>
<%@ Register TagPrefix="npm" TagName="NodeLink" Src="~/Orion/NetPerfMon/Controls/NodeLink.ascx" %>
<%@ Register TagPrefix="npm" TagName="IntegrationIcons" Src="~/Orion/NetPerfMon/Controls/IntegrationIcons.ascx" %>


<asp:Content runat="server" ContentPlaceHolderID="ViewPageTitle" ID="Content1">
    <%if (!CommonWebHelper.IsBreadcrumbsDisabled)
               { %>
	            <div style="margin-left: 10px; margin-top: 10px;">
                    <orion:PluggableDropDownMapPath ID="NodeSiteMapPath" runat="server" SiteMapKey="NetObjectDetails" LoadSiblingsOnDemand="true"/>
				</div>
				<%} %>

	<h1>
	    <span style="float: right; padding-right: 15px;">
	        <npm:IntegrationIcons ID="IntegrationIcons1" runat="server" NodeID='<%$ HtmlEncodedCode: this.NetObject["NodeID"] %>' />
	    </span>
	    <%= DefaultSanitizer.SanitizeHtml(ViewInfo.IsSubView ? ViewInfo.ViewGroupTitle : ViewInfo.ViewHtmlTitle) %> 
	    - 
        <orion:StatusIconControl ID="statusIconControl" runat="server" />
        <npm:NodeLink ID="NodeLink1" runat="server" NodeID="<%$ HtmlEncodedCode: this.NetObject.NetObjectID %>" />
	    <asp:PlaceHolder ID="TitleDecorators" runat="server"></asp:PlaceHolder>	    
        <%= DefaultSanitizer.SanitizeHtml(ViewInfo.IsSubView ? ViewInfo.ViewHtmlTitle : string.Empty) %>
	</h1>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
	<netperfmon:NodeResourceHost runat="server" ID="nodeResHost">
	    <orion:ResourceContainer runat="server" ID="resContainer" />
	</netperfmon:NodeResourceHost>
</asp:Content>
