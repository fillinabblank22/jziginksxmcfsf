using System;
using System.Globalization;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Helpers;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_VolumeDetails : OrionView
{
	protected override void OnInit(EventArgs e)
	{
		this.Title = UIHelper.NormalizeSpaces(this.ViewInfo.ViewTitle);
        this.volHost.Volume = (SolarWinds.Orion.NPM.Web.Volume)this.NetObject;
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

	    var node = ((SolarWinds.Orion.NPM.Web.Node) this.NetObject.Parent);
        statusIconControl.StatusLEDImageSrc = node.Status.ToString("smallled", null);
	    statusIconControl.EntityId = node.NodeID.ToString(CultureInfo.InvariantCulture);
	    statusIconControl.ViewLimitationId = ViewInfo.LimitationID;

		base.OnInit(e);
	}

	public override string ViewType
	{
		get { return "VolumeDetails"; }
	}
}
