<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/OrionMasterPage.master" CodeFile="SysLog.aspx.cs" Inherits="Orion_NetPerfMon_SysLog" EnableEventValidation="false" EnableViewState="true" Title="<%$ Resources: SyslogTrapsWebContent, WEBDATA_VB0_209 %>" %>
<%@ Register Assembly="OrionWeb" Namespace="SolarWinds.Orion.Web.UI" TagPrefix="web" %>
<%@ Register TagPrefix="orion" TagName="NetworkObjects" Src="~/Orion/Controls/NetworkObjectControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="NetworkObjectTypes" Src="~/Orion/Controls/TypeOfDevicerControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="Severities" Src="~/Orion/Controls/SeverityControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="Facilities" Src="~/Orion/Controls/FacilityControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="HelpButton" Src="~/Orion/Controls/HelpButton.ascx" %>
<%@ Register TagPrefix="orion" TagName="SysLogsReport" Src="~/Orion/Controls/SysLogsReportControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="Vendors" Src="~/Orion/Controls/VendorControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="Reposter" Src="~/Orion/Controls/Reposter.ascx" %>
<%@ Register TagPrefix="orion" TagName="PickerTimePeriodControl" Src="~/Orion/Controls/PickerTimePeriodControl.ascx" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" runat="server">
            <orion:IconLinkExportToPDF runat="server" ID="ExportToPDFLink"/>
            <asp:LinkButton ID="btnPrintable" runat="server" CssClass="printablePageLink" OnClick="Printable_Click" Text="<%$ Resources: SyslogTrapsWebContent, WEBDATA_VB0_246 %>" />
            <orion:IconHelpButton HelpUrlFragment="OrionPHViewSyslog" ID="helpButton" runat="server" /> 
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="placeHolder" runat="server">
    <orion:Reposter ID="Reposter1" runat="server" />
    <asp:PlaceHolder ID="holder" runat="server" >
        <h1>
            <%=Page.Title %>
        </h1>
        <div style="margin-left: 5px; margin-right: 5px;">
            <table style="width: 100%;">
                <tr>
                    <td><b><%=Title2 %></b></td>
                    <td style="width:40px;" >
                            <orion:LocalizableButton runat="server" ID="btnHide" OnClick="Show_Click" DisplayType="Resource" LocalizedText="CustomText" Text="<%$ Resources: SyslogTrapsWebContent, WEBDATA_VB0_223 %>" style="white-space:nowrap;" />
                            <orion:LocalizableButton runat="server" ID="btnShow" OnClick="Show_Click" DisplayType="Resource" LocalizedText="CustomText" Text="<%$ Resources: SyslogTrapsWebContent, WEBDATA_VB0_219 %>" style="white-space:nowrap;" Visible="false"/>
                    </td>
                </tr>
            </table>
            <asp:PlaceHolder runat="server" ID="details">
                <table style="width:100%;margin-right: 0px;">
                    <tr >
                        <td style="width: 95px;font-size: 7pt;"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_210 %>&nbsp;</td>
                        <td class="formTable">
                            <table class="formTable">
                                <tr>
                                    <td><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_211 %></td>
                                    <td>&nbsp;</td>
                                    <td><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_212 %></td>
                                    <td>&nbsp;</td>
                                    <td><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_213 %></td>
                                    <td>&nbsp;</td>
                                    <td><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_13 %></td>
                                </tr>
                                <tr>
                                    <td><orion:NetworkObjects runat="server" ID="netObjects" PageType="SysLog" /></td>
                                    <td>&nbsp;<b style="color: #5c5c5c;"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_214 %></b>&nbsp;</td>
                                    <td><orion:NetworkObjectTypes runat="server" ID="netObjectTypes" /></td>
                                    <td>&nbsp;<b style="color: #5c5c5c;"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_214 %></b>&nbsp;</td>
                                    <td><orion:Vendors runat="server" ID="vendors" PageType="SysLog" /></td>
                                    <td>&nbsp;<b style="color: #5c5c5c;"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_214 %></b>&nbsp;</td>
                                    <td><asp:TextBox runat="server" ID="ipAddress" Width="260"></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="width: 95px;font-size: 7pt;"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_215 %>&nbsp;</td>
                        <td style="margin: 0px;" class="formTable">
                            <table class="formTable" style="margin: 0px;margin-right: 0px;">
                                <tr>
                                    <td><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_216 %></td>
                                    <td><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_217 %></td>
                                    <td><%= Resources.SyslogTrapsWebContent.WEBCODE_AK0_91 %></td>
                                    <td><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_218 %></td>
                                </tr>
                                <tr>
                                    <td><orion:Severities runat="server" ID="severities" /></td>
                                    <td><orion:Facilities runat="server" ID="facilities" /></td>
                                    <td><asp:TextBox runat="server" ID="messageType"></asp:TextBox></td>
                                    <td><asp:TextBox runat="server" ID="messagePattern"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td></td>
                                </tr>
                            </table>
                            <table class="formTable" style="margin: 0px;margin-right: 0px;">
                                <tr>
                                    <td><%= Resources.SyslogTrapsWebContent.WEBDATA_AK0_337 %></td>
                                </tr>
                                <tr>
                                    <td><orion:PickerTimePeriodControl ID="pickerTimePeriodControl" InLine="true" runat="server" /></td>
                                </tr>
                                <tr>    
                                    <td>
                                    <%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_541 %>&nbsp;
                                    <web:ValidatedTextBox CustomStringValue=""
                                        Text="250" ID="maxRecords" Columns="5"
                                        Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                        MinValue="0" MaxValue="2000" style="vertical-align:bottom" />
                                    <asp:HiddenField ID="previousMaxRecords" runat="server" />
                                    <asp:CheckBox runat="server" ID="showCleared" style="padding-left:20px;"/><asp:HiddenField ID="previousShowCleared" runat="server" /><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_222 %></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><orion:LocalizableButton  ID="ImageButton1" LocalizedText="Refresh" DisplayType="Primary" runat="server" OnClick="Refresh_Click" /></td>
                        <td></td>
                    </tr>
                </table>
            </asp:PlaceHolder>
            <br />
            <orion:SysLogsReport runat="server" ID="sysLogsReport" Printable="false" />
        </div>
    </asp:PlaceHolder>
</asp:Content>
