using System;
using System.Data;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.SyslogTraps.Web.DAL;

public partial class Orion_NetPerfMon_AdvSysLogWho : System.Web.UI.Page
{
    public string Timee
    {
        get
        {
            String s = string.Empty;
            try
            {
                s = Request.QueryString["timee"];
            }
            catch { }
            if (string.IsNullOrEmpty(s))
                s = "hour";
            return s;
        }
    }

    public string Period
    {
        get
        {
            String s = Timee;
            switch (s.ToLowerInvariant())
            {
                case "minute": return "Past Minute";
                case "hour": return "Past Hour";
                case "day": return "Last 24 Hours";
                case "week": return "Last 7 Days";
                case "month": return "Last 30 Days";
                case "custom": return string.Format("From {0} to {1}", ps.dFrom.ToString(), ps.dTo.ToString());
                default: return "Past Hour";
            }
        }
    }

    /// <summary>
    /// Needed because of date usage in period layout
    /// [i18n]
    /// </summary>
    public string LocalizedPeriod
    {
        get
        {
            if (Timee.Equals("custom", StringComparison.OrdinalIgnoreCase))
            {
                return string.Format(Resources.SyslogTrapsWebContent.WEBCODE_VB0_174, ps.dFrom.ToString(), ps.dTo.ToString());
            }
            return Periods.GetLocalizedPeriod(Period);
        }
    }

    public string qPeriod
    {
        get
        {
            if (Timee.Equals("custom", StringComparison.OrdinalIgnoreCase))
                return string.Format("{0}~{1}", ps.dFrom.ToString(), ps.dTo.ToString());
            return Period;
        }
    }

    public string MessageType
    {
        get
        {
            string s = this.Request.QueryString["messagetype"];
            if (string.IsNullOrEmpty(s))
                s = string.Empty;
            return s;
        }
    }

    public int Severity
    {
        get
        {
            string s = this.Request.QueryString["severity"];
            if (string.IsNullOrEmpty(s))
                s = "-1";
            int severity = -1;
            if (!int.TryParse(s, out severity)) severity = -1;
            return severity;
        }
    }

    public string GetSeverityType(object severity)
    {
        string sseverity = severity == null ? "" : severity.ToString();
        switch (sseverity)
        {
            case "0": return Resources.SyslogTrapsWebContent.WEBCODE_VB0_169;
            case "1": return Resources.SyslogTrapsWebContent.WEBCODE_VB0_164;
            case "2": return Resources.SyslogTrapsWebContent.WEBCODE_VB0_165;
            case "3": return Resources.SyslogTrapsWebContent.WEBCODE_AK0_1;
            case "4": return Resources.SyslogTrapsWebContent.Status_Warning;
            case "5": return Resources.SyslogTrapsWebContent.WEBCODE_VB0_166;
            case "6": return Resources.SyslogTrapsWebContent.WEBCODE_VB0_167;
            case "7": return Resources.SyslogTrapsWebContent.WEBCODE_VB0_168;
        }
        return string.Empty;
    }

    public string GetLinkTarget()
    {
        return string.Empty;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        LoadData();
        DataBind();
    }

    private void LoadData()
    {
        if (MessageType != "NULLL")
        {
            if (!string.IsNullOrEmpty(MessageType))
                lblbSummaryBy.Text = MessageType + Resources.SyslogTrapsWebContent.WEBCODE_VB0_175;
            else
                lblbSummaryBy.Text = Resources.SyslogTrapsWebContent.WEBCODE_VB0_176;
        }
        else
        {
            lblbSummaryBy.Text = GetSeverityType(Severity) + Resources.SyslogTrapsWebContent.WEBCODE_VB0_177;
        }

        var periodBegin = DateTime.Now;
        var periodEnd = DateTime.Now;
        string PeriodName = Period;
        if (Timee == "custom")
        {
            plhCustom.Visible = true;
            periodBegin = ps.dFrom;
            periodEnd = ps.dTo;
        }
        else
            Periods.Parse(ref PeriodName, ref periodBegin, ref periodEnd);


        using (WebDAL webdal = new WebDAL())
        {
            DataTable table = webdal.GetSysLogMessagesCountsForAdvancedWho(MessageType == "NULLL" ? string.Empty : MessageType, Severity, periodBegin, periodEnd);
            mainRepeater.DataSource = table;
            mainRepeater.DataBind();
        }
    }

    protected void btnGo_Click(object sender, EventArgs e)
    {
        LoadData();
    }
}
