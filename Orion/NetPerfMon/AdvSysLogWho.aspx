<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/OrionMasterPage.master"
    CodeFile="AdvSysLogWho.aspx.cs" Inherits="Orion_NetPerfMon_AdvSysLogWho" EnableEventValidation="false"
    EnableViewState="true" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>

<%@ Register Src="Controls/AdvancedSyslogPeriodSelect.ascx" TagName="AdvancedSyslogPeriodSelect"
    TagPrefix="uc1" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="placeHolder" runat="server">

<orion:Include runat="server" File="js/jquery/timePicker.css" />
<orion:Include runat="server" File="legacyevents.css" /><%-- severity css --%>

    <asp:PlaceHolder ID="holder" runat="server">
        <div style="margin-left: 0px; margin-right: 0px; width: 100%">
            <table width="100%" border="0" cellpadding="8" cellspacing="0">
                <tr class="HeaderBar">
                    <td height="35" align="left" nowrap>
                        <b><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_208 %>
                            <asp:Label runat="server" ID="lblbSummaryBy"></asp:Label>
                            <br>
                            <font size="2"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_191 %>
                                <%= this.LocalizedPeriod%>.</b></font>
                        <asp:PlaceHolder runat="server" ID="plhCustom" Visible="false">
                            <div class="HeaderBar" style="height: 35;">
                                <font size="2">
                                    <uc1:AdvancedSyslogPeriodSelect ID="ps" runat="server" />
                                    &nbsp;<asp:Button runat="server" ID="btnGo" Text="<%$ Resources: SyslogTrapsWebContent, WEBDATA_VB0_192 %>" Style=" height: 17px;
                                        color: black; background-color: #fff; border: 1px solid Gray; font-size: 10px;"
                                        OnClick="btnGo_Click" />
                                </font>
                            </div>
                        </asp:PlaceHolder>
                        <br>
                        <font size="2"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_203 %> <a href="/Orion/netperfmon/AdvSyslogWho.aspx?timee=minute&messagetype=<%= this.MessageType%>&severity=<%=this.Severity%>"
                            target="<%# GetLinkTarget()%>"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_194 %><%= Resources.SyslogTrapsWebContent.String_Separator_1_VB0_59 %></a> <a href="/Orion/netperfmon/AdvSyslogWho.aspx?timee=hour&messagetype=<%=this.MessageType%>&severity=<%=this.Severity%>"
                                target="<%# GetLinkTarget()%>"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_195 %><%= Resources.SyslogTrapsWebContent.String_Separator_1_VB0_59 %></a> <a href="/Orion/netperfmon/AdvSyslogWho.aspx?timee=day&messagetype=<%=this.MessageType%>&severity=<%=this.Severity%>"
                                    target="<%# GetLinkTarget()%>"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_196 %><%= Resources.SyslogTrapsWebContent.String_Separator_1_VB0_59 %></a> <a href="/Orion/netperfmon/AdvSyslogWho.aspx?timee=week&messagetype=<%=this.MessageType%>&severity=<%=this.Severity%>"
                                        target="<%# GetLinkTarget()%>"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_197 %><%= Resources.SyslogTrapsWebContent.String_Separator_1_VB0_59 %></a> <a href="/Orion/netperfmon/AdvSyslogWho.aspx?timee=month&messagetype=<%=this.MessageType%>&severity=<%=this.Severity%>"
                                            target="<%# GetLinkTarget()%>"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_198 %><%= Resources.SyslogTrapsWebContent.String_Separator_1_VB0_59 %></a> <a href="/Orion/netperfmon/AdvSyslogWho.aspx?timee=custom&messagetype=<%= this.MessageType%>&severity=<%=this.Severity%>"
                                                target="<%# GetLinkTarget()%>"><%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_199 %><%= Resources.SyslogTrapsWebContent.String_Separator_1_VB1_1 %></a> </font>
                    </td>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="right" nowrap>
                                    <orion:LocalizableButtonLink runat="server" DisplayType="Resource" LocalizedText="CustomText" Text="<%$ Resources: SyslogTrapsWebContent, ResourcesAll_Help %>"
                                     NavigateUrl='<%# HelpHelper.GetHelpUrl("OrionPHResourceSyslogSummary") %>'/>
                                </td>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <asp:Repeater runat="server" ID="mainRepeater">
                <HeaderTemplate>
                    <table width="100%" cellspacing="0" cellpadding="0">
                        <tr class="ViewHeader">
                            <td class="Property">
                                &nbsp;
                            </td>
                            <td class="Property" align="Left">
                                <%= Resources.SyslogTrapsWebContent.WEBDATA_AK0_155 %>
                            </td>
                            <td class="Property" align="center">
                                <%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_200 %>
                            </td>
                            <td class="Property">
                                &nbsp;
                            </td>
                            <td class="Property">
                                <%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_207 %>
                            </td>
                            <td class="Property" align="center">
                                <%= Resources.SyslogTrapsWebContent.WEBDATA_VB0_39 %>
                            </td>
                            <td class="Property">
                                &nbsp;
                            </td>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td class="Property">
                            &nbsp;
                        </td>
                        <td class="Property" nowrap>
                            <a href="/Orion/Netperfmon/NodeDetails.aspx?NetObject=N:<%# Eval("NodeID") %>" target="_blank" rel="noopener noreferrer">
                                <%# Eval("Caption") %>
                            </a>
                        </td>
                        <td class="Severity<%# Eval("syslogseverity")%>" nowrap align="center" width="5%">
                            <%# GetSeverityType(Eval("syslogseverity"))%>
                        </td>
                        <td class="Property">
                            &nbsp;
                        </td>
                        <td class="Property" nowrap align="left" width="5%">
                            <a href="/Orion/Netperfmon/SysLog.aspx?NetObject=N:<%# Eval("NodeID")%>&Period=<%# this.qPeriod%>&MessageType=<%# Eval("MessageType")%>&severity=<%# Eval("syslogseverity")%>"
                                target="_blank" rel="noopener noreferrer">
                                <%# Eval("messagetype")%>
                            </a>
                        </td>
                        <td class="Property" nowrap align="center" width="5%">
                            <a href="/Orion/Netperfmon/SysLog.aspx?NetObject=N:<%# Eval("NodeID")%>&Period=<%# this.qPeriod%>&MessageType=<%# Eval("MessageType")%>&severity=<%# Eval("syslogseverity")%>"
                                target="_blank" rel="noopener noreferrer"><b>
                                    <%# Eval("Total") %></b></a>
                        </td>
                        <td class="Property">
                            &nbsp;
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </asp:PlaceHolder>
</asp:Content>
