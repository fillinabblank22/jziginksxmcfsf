<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/OrionMasterPage.master"%>

<%@ Register TagPrefix="x" TagName="NetworkMapControl" Src="./Controls/NetworkMapControl.ascx" %>
<%@ Register TagPrefix="x" TagName="ObjectsList" Src="./Controls/MapObjectsListControl.ascx" %>

<script runat="server">
    protected override void OnInit(EventArgs e)
    {
        if (!string.IsNullOrEmpty(Context.Request["Printable"]) && Context.Request["Printable"] == "true")
        {
            Response.Redirect("./MapViewPrintable.aspx?Map=" + Context.Request["Map"]);
        }
        int refreshTime = 5;
        if(!int.TryParse(SolarWinds.Orion.Web.DAL.WebSettingsDAL.Get("Auto Refresh"), out refreshTime))
        {
            refreshTime = 5;
        }
		string requestUseCaching = Context.Request["UseCaching"];
		
		if(string.IsNullOrEmpty(requestUseCaching) || 
		  (!string.IsNullOrEmpty(requestUseCaching) && !requestUseCaching.Equals("true", StringComparison.InvariantCultureIgnoreCase)))
			{
				map.HideCachedImage();
			}
        autoRefresh.Attributes["content"] = (60*refreshTime).ToString() + ";url=";
        objectsList.MapId = Request["Map"];
        map.MapId = Request["Map"];
        map.TranslateMacros = Macros.TranslateLabelMacros;
        map.ImageAttributes = "border=1 style=border-color:#dfded7;margin-left:10px;margin-bottom:10px";
        base.OnInit(e);
    }
    
    protected override void OnPreRender(EventArgs e)
    {
        string displayName = Request["Title"];
        if(string.IsNullOrEmpty(displayName))
        {
            displayName = Request["Map"];
        }
        
        displayName = HttpUtility.HtmlEncode(displayName);
        if(string.IsNullOrEmpty(displayName))
        {
            Page.Title = displayName;
        }
        Page.Title = displayName;
        networkMapName.InnerHtml = "<h1>" + displayName + "</h1>";
    }
</script>
    
<asp:Content ContentPlaceHolderID="ResourcesStyleSheetPlaceHolder" runat="server">
    <orion:Include runat="server" File="NetworkMaps.css" />
    <meta http-equiv="refresh" id="autoRefresh" runat="server" />
</asp:Content>
    
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="placeHolder" runat="server">
    
    <div>
        <div id="networkMapName" runat="server"></div>
        <x:NetworkMapControl ID="map" runat="server" />
    </div>
    <div>
        <x:ObjectsList ID="objectsList" runat="server" />
    </div>
    
</asp:Content>
