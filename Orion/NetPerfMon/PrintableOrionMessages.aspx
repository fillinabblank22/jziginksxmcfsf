<%@ Page Language="C#" AutoEventWireup="true" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_VB0_270 %>" CodeFile="PrintableOrionMessages.aspx.cs" Inherits="Orion_NetPerfMon_PrintableOrionMessages" MasterPageFile="~/Orion/OrionMinReqs.master" %>
<%@ Register TagPrefix="orion" TagName="OrionMessages" Src="~/Orion/Controls/OrionMessagesReportControl.ascx" %>

<asp:Content runat="server" ContentPlaceHolderID="BodyContent">

    <orion:Include runat="server" File="jquery-1.7.1/jquery.livequery.js" />

    <form id="form1" runat="server" class="sw-app-region">
    <asp:ScriptManager runat="server" ID="MasterScriptManager" EnablePageMethods="true" AllowCustomErrorsRedirect="false" AsyncPostBackErrorMessage="ScriptAsyncPostBackError" ></asp:ScriptManager>
    <h1>
        <%= DefaultSanitizer.SanitizeHtml(Title2) %>
    </h1>
    <div style="margin:10px 10px 10px 10px">
        <orion:OrionMessages runat="server" ID="orionMessagesReport" Printable="true" />
    </div>
    </form>
</asp:Content>
