﻿<%@ Control Language="C#" ClassName="AlertPicker" %>

<script runat="server">
    private string onAlertsSelected = "function(){}";

    [PersistenceMode(PersistenceMode.Attribute)]
    public string Title { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string GetHeaderTitleFn { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string OnAlertsSelected
    {
        get { return onAlertsSelected; }
        set { onAlertsSelected = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool RenderSelectedItems { get; set; }

</script>
<orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="Alerts/js/AlertPicker.js" />
<orion:Include runat="server" File="Admin/js/SearchField.js" />
<%if (RenderSelectedItems)
 {%>
<div id="selectedAlertsList" style="margin: 10px 0;">
</div>
<% } %>
<div id="actionAlertBox" style="display: none;">
    <div id="headerTitle" style="margin:10px; font-size:16px;"></div>
    <div id="searchPanel" style="float: left; width: 100%;"></div>
    <div style="clear: both;"></div>
    <div id="actionAlertsPicker"></div>
    <label class="selectedActionsLabel" style="font-weight: bold;"></label>
    <div id='selectedObjects' class='selectedObjectsCSS'><span style='color:gray;'><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_101) %></span></div>
</div>


<script type="text/javascript">
    SW.Orion.AlertPicker.selectAlertDialog = function() {
        $("#actionAlertBox").show();
        $("#actionAlertBox").dialog({ modal: true, draggable: true, resizable: false, position: ['center', 'top'], width: 1000, title: "<%= Title %>", dialogClass: 'ui-dialog-osx' });
        var getHeaderTitleFn = <%= GetHeaderTitleFn%>;
        if (typeof getHeaderTitleFn == "function")
            $("#headerTitle").html(getHeaderTitleFn());

        if ($("#actionAlertsPicker").children().length == 0) {
            SW.Orion.AlertPicker.init();
            $(SW.Core.Widgets.Button('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_28) %>', { type: 'secondary', id: 'cancelBtn' })).appendTo("#actionAlertBox").css('float', 'right').css('margin', '10px').click(function() {
                $("#actionAlertBox").dialog("close");
            });
            $(SW.Core.Widgets.Button("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_100) %>", { type: 'primary', id: 'assignActionToAlertBtn' })).appendTo("#actionAlertBox").css('float', 'right').css('margin', '10px').click(function() {

                $("#actionAlertBox").dialog("close");
                var pickedAlerts = SW.Orion.AlertPicker.getSelectedAlerts();
                <%if (!string.IsNullOrEmpty(OnAlertsSelected))
                      {%>
                var onSelected = <%= OnAlertsSelected%>;
                if (typeof onSelected == "function") {
                    onSelected(pickedAlerts);
                }
                <% } %>

                SW.Orion.AlertPicker.setSelectedAlerts(pickedAlerts);
            });
        }
        SW.Orion.AlertPicker.clearSelection();
    }
</script>
