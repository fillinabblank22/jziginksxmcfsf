<%@ Page Language="C#" MasterPageFile="~/Orion/Alerts/AlertsMasterPage.master"
	AutoEventWireup="true" CodeFile="Deleted.aspx.cs" Inherits="Orion_Alerts_Deleted"
	Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_BV0_0015 %>" %>

<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="ContentPlaceholder">
	<div>
		<span class="sw-suggestion sw-suggestion-warn"><span class="sw-suggestion-icon"></span>
			<!-- Replace these strings with localized resource strings -->
			<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0016) %>  &nbsp; &raquo; <a class="sw-link" href="Default.aspx"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0017) %></a>
		</span>
	</div>
</asp:Content>
