﻿using System;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Alerts_MigrationLog : System.Web.UI.Page
{
    public string MigrationId { get; set; }

    protected override void OnInit(EventArgs e)
    {
        MigrationId = WebSecurityHelper.SanitizeHtml(Request["MigrationId"]);
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}