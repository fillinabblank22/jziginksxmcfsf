<%@ Page Language="C#" MasterPageFile="~/Orion/Alerts/AlertsMasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_Alerts_Default" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_YK0_26%>" %>
<%@ Register Src="~/Orion/Controls/NavigationTabBar.ascx" TagPrefix="orion" TagName="NavigationTabBar" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Alerts" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" Assembly="OrionWeb" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ContentPlaceHolderID="headplaceholder" runat="server">
     <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
     <orion:Include runat="server" File="OrionCore.js" />
     <orion:Include runat="server" File="Alerts/js/AlertManagerGrid.js" />
     <orion:Include runat="server" File="Actions/js/ActionsEditor.js" />
     <orion:Include ID="Include1" runat="server" File="Admin/js/SearchField.js" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" runat="server">
        <div class="contTitleBarDate">
             <span class="activeAlert"><a href="../NetPerfMon/Alerts.aspx"> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_AY0_94) %></a></span>
		    <orion:IconHelpButton ID="btnHelp" runat="server"  HelpUrlFragment="OrionCoreAG_AlertsManaging" />
		    <div style="padding-top: 5px; font-size: 8pt; text-align: right;"><%= DateTime.Now.ToString("F") %></div>
	    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceholder" Runat="Server">
<style type="text/css">
    #duplicateDialog { display: none; background-color: white;}
     #mainContent { margin: 0px 10px 15px 10px; }
    .disabled-row, .disabled-row a {color:gray;}
    .import-file-dialog {
        display: none;
        position: absolute;
        -moz-opacity: 0;
        width: 10px;
        cursor: pointer;
        font-size: 100px; /*to ensure we click on button but not on text field*/
        overflow: hidden;
        filter: alpha(opacity: 0);
        opacity: 0;
        z-index: 99999;
    }
</style>
<script type="text/javascript">
    // <![CDATA[
        function alertDraftExists() { return <%= (AlertWorkflowDraftCache.DraftExists()) ? "true": "false" %>; }
        function getBtnContinueID() { return '<%= btnContinue.ClientID %>'; }
        function getDialogContinueID() { return '<%= dialogContainer.ClientID %>'; }
        $(function() {
            SW.Orion.AlertManager.setFileUploadControl('<%= importFileDialog.ClientID%>');
            SW.Orion.AlertManager.setExportImportDialogControl('<%= CredentialDialog.ClientID %>');
            SW.Orion.AlertManager.setAlertConfigurationDataControl('<%= hdnImportAlertFileContent.ClientID %>');
            SW.Orion.AlertManager.setValidationControl('<%= Validation.ClientID %>');
        });
    // ]]>
</script> 
<div id="mainContent">
<div id="Hint" style="position: static; width: 100%;" runat="server">
    <div>
        <h1 class="page-title" style="padding:15px 0px 0px 0px!important"><%= DefaultSanitizer.SanitizeHtml(this.Title) %></h1>
    </div>
</div>
    <div>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_152) %>
    </div>
                              <div runat="server" id="Validation" class="sw-suggestion sw-suggestion-pass sw-suggestion-border" style="margin-bottom: 10px; display: none" visible="true" >
                                <span class="sw-suggestion-icon"></span>
                            </div>

      <% if (AlertWorkflowDraftCache.DraftExists() && !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
         { %>
            <div>
           <span class="sw-suggestion"><span class="sw-suggestion-icon"></span><%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBDATA_TM0_328, AlertWorkflowDraftCache.GetCurrentUserDraft().LastChange.ToString("t"))) %> &nbsp;
               &raquo; <a class="sw-link" href="<%= DefaultSanitizer.SanitizeHtml(EditDraftURL) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_290) %></a>&nbsp;
               &raquo; <asp:LinkButton CssClass="sw-link" runat="server" ID="lnkRemoveDraft" OnClick="RemoveDraft" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_291 %>" />
           </span>
            </div>
      <% } %>  
      <br/>
      <orion:NavigationTabBar ID="AlertListNavigationTabBar" runat="server" Visible="true">   
            <orion:NavigationTabItem Name="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_YK0_75 %>" Url="~/Orion/Alerts/Default.aspx" />
            <orion:NavigationTabItem Name="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_YK0_76 %>" Url="~/Orion/Actions/Default.aspx" />
       </orion:NavigationTabBar>
     <input type="hidden" name="AlertManager_UserHasRightToEnableOrDisableAlert" id="AlertManager_UserHasRightToEnableOrDisableAlert" value='<%= DefaultSanitizer.SanitizeHtml(UserHasRightToEnableOrDisableAlert) %>' />
     <input type="hidden" name="AlertManager_PageSize" id="AlertManager_PageSize" value='<%= HttpUtility.HtmlEncode(WebUserSettingsDAL.Get("AlertManager_PageSize"))%>' />     
     <input type="hidden" name="AlertManager_SelectedColumns" id="AlertManager_SelectedColumns" value='<%= HttpUtility.HtmlEncode(WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, "AlertManager_SelectedColumns",WebConstants.AlertDafultSelectedColumns))%>' />
     <input type="hidden" name="AlertManager_SortOrder" id="AlertManager_SortOrder" value='<%= HttpUtility.HtmlEncode(WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, "AlertManager_SortOrder",WebConstants.ReportSchedulerDafultSortOrder))%>' />
     <input type="hidden" name="AlertManager_GroupingValue" id="AlertManager_GroupingValue" value='<%= HttpUtility.HtmlEncode(WebUserSettingsDAL.Get("AlertManager_GroupingValue"))%>' />   
     <input type="hidden" name="SelectedAlertID" id="SelectedAlertID" value='<%= DefaultSanitizer.SanitizeHtml(Request["AlertID"]) %>' />   
     <input type="hidden" name="SelectedAlertID" id="SelectedGrouping" value='<%= DefaultSanitizer.SanitizeHtml(Request["Grouping"]) %>' />  
      <div ng-non-bindable id="AlertManagerGrid"></div>
</div>
    
<div id="draftDialog" style="display:none;" title="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_292) %>">
    <div><b><%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBDATA_TM0_329, AlertWorkflowDraftCache.DraftExists() ? AlertWorkflowDraftCache.GetCurrentUserDraft().LastChange.ToString("t") : String.Empty)) %></b></div>
    <div><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_330) %></div>
    <div class="sw-btn-bar-wizard" style="padding-top: 30px;">
        <orion:LocalizableButtonLink runat="server" ID="btnEditDraft" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_295 %>" DisplayType="Primary" />
        <orion:LocalizableButtonLink runat="server" ID="btnContinue" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_331 %>" DisplayType="Secondary" onclick="return false;" />
    </div>
</div>
    
<div runat="server" id="dialogContainer"></div>
    
    <asp:Panel ID="CredentialDialog" style="display: none"  runat="server">
        <h4><span id="exportImportDialogHeaderText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_159) %></span></h4>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_160) %><br/>
        <input type="radio" id="rbStripSensitiveData" name="rbExportImportGroup" checked="checked"/><label id="lblStripSensitiveData" for="rbStripSensitiveData"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_166) %></label><br/>
        <div id="lblStripSensitiveDataDescription"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_161) %></div>
        <input type="radio" id="rbProtectSensitiveDataByPassword" name="rbExportImportGroup"/><label Id="lblProtectSensitiveDataByPassword" for="rbProtectSensitiveDataByPassword"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_167) %></label><br/>
        <table id="protectionPassword" style="display: none">
            <tr>
                <td><label for="tbProtectionPassword"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_162) %></label></td>
                <td><input type="password" id="tbProtectionPassword" /></td>
                <td><span id="lblPasswordValidationMessage" style="color: red; display: none;" ><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_163) %></span></td>
            </tr>
            <tr id="trRetypePassword">
                <td><label for="tbRetypeProtectionPassword"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_164) %></label></td>
                <td><input type="password" id="tbRetypeProtectionPassword"/></td>
                <td><span id="lblRetypePasswordValidationMessage" style="color: red; display: none" ><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_165) %></span></td>
            </tr>
        </table>
    </asp:Panel>

<asp:FileUpload ID="importFileDialog" runat="server" onchange="SW.Orion.AlertManager.importFileSelected(this)" CssClass="import-file-dialog"/>  
<asp:HiddenField runat="server" ID="hdnImportAlertFileContent" />
</asp:Content>

