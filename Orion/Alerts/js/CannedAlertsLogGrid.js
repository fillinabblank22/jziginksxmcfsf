﻿SW.Core.namespace("SW.Core").CannedAlertsLogGrid = function (config) {
    var dataStore;
    var grid;

    function statusRenderer(value) {
        if (value) {
            return '@{R=Core.Strings;K=WEBJS_PS1_23;E=js}';
        } else {
            return "@{R=Core.Strings;K=WEBJS_PS1_24;E=js}";
        }
    }

    function columnWrap(val) {
        return '<div style="white-space:normal !important;">' + val + '</div>';
    }

    function newAlert(e) {
        e.preventDefault();
        var guid = SW.Core.Services.generateNewGuid();
        SW.Core.Services.postToTarget(String.format("/Orion/Alerts/Add/Default.aspx?AlertWizardGuid={0}", guid));
    }

    function editAlert(e) {
        e.preventDefault();
        var guid = SW.Core.Services.generateNewGuid();
        var editParams = {
            AlertID: $(this).data('alertid')
        };
        SW.Core.Services.postToTarget(String.format("/Orion/Alerts/Add/Default.aspx?AlertWizardGuid={0}", guid), editParams);
    }

    this.Init = function (importId) {
        var gridColumnsModel = [
            { header: '@{R=Core.Strings.2;K=WEBJS_MD0_01; E=js}', width: 35, sortable: true, dataIndex: 'Name' },
            { header: '@{R=Core.Strings.2;K=WEBJS_MD0_02; E=js}', width: 35, sortable: true, dataIndex: 'Description' },
            { header: '@{R=Core.Strings.2;K=WEBJS_MD0_03; E=js}', width: 15, sortable: true, dataIndex: 'Status', renderer: statusRenderer },
            { header: '@{R=Core.Strings.2;K=WEBJS_MD0_04; E=js}', width: 15, sortable: true, dataIndex: 'ObjectType' }
        ];
        var gridStoreColumns = [
            { name: 'Name', mapping: 0 },
            { name: 'Description', mapping: 1 },
            { name: 'Status', mapping: 2 },
            { name: 'ObjectType', mapping: 3 }
        ];

        dataStore = new ORION.WebApiStore("/api/AlertMigrationLog/GetNewlyCannedAlerts", gridStoreColumns, 'Name');
        grid = new Ext.grid.GridPanel({
            id: 'macroVariableGrid',
            region: 'center',
            store: dataStore,
            columns: gridColumnsModel,
            sm: new Ext.grid.RowSelectionModel(),
            enableHdMenu: false,
            autoScroll: true,
            loadMask: true,
            height: 420,
            layout: 'fit',
            viewConfig: {
                forceFit: true
            },
            cls: 'panel-no-border'
        });

        var mainGridPanel = new Ext.Panel({
            id: 'macroMainGrid',
            height: 420,
            region: 'center',
            split: true,
            layout: 'border',
            collapsible: false,
            items: [grid]
        });

        mainGridPanel.render('alertMigrationGridPlace');
        if (importId) {
            dataStore.proxy.conn.jsonData = { importId: importId };
            dataStore.load();
        }
    };
}