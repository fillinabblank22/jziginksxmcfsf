﻿SW.Core.namespace("SW.Core").AlertMigrationLogGrid = function (config) {
    var dataStore;
    var grid;

    function renderMigrationStatus(value) {
        switch(value) {
            case 0:
                return '<div><img class="statusIcon" src="/Orion/Images/failed_16x16.gif"><span>@{R=Core.Strings.2;K=WEBJS_IT0_15; E=js}</span></div>';
                break;
            case 1:
                return '<div><img class="statusIcon" src="/Orion/Images/warning_16x16.gif"><span>@{R=Core.Strings;K=WEBJS_PS0_7; E=js}</span></div>';
                break;
            case 2:
                return '<div><img class="statusIcon" src="/Orion/Images/skipped_16x16.png"><span>@{R=Core.Strings.2;K=WEBJS_IT0_11; E=js}</span></div>';
                break;
            case 3:
                return '<div><img class="statusIcon" src="/Orion/Images/ok_16x16.gif"><span>@{R=Core.Strings;K=PCDATA_VB1_14; E=js}</span></div>';
                break;
        }
    }

    function renderAction(value, meta, record) {
        switch (value) {
            case 0:
                return '<a href="#" class="newAlert" style="background: transparent url(/Orion/Nodes/images/icons/icon_add.gif) scroll no-repeat left center; padding: 2px 0px 2px 20px;">@{R=Core.Strings.2;K=WEBJS_IT0_12; E=js}</a>';
                break;
            case 1:
                return String.format('<a href="#" data-alertid="{0}" style="background: transparent url(/Orion/Nodes/images/icons/icon_edit.gif) scroll no-repeat left center; padding: 2px 0px 2px 20px;">@{R=Core.Strings.2;K=WEBJS_IT0_13; E=js}</a>', record.data.NewAlertId);
                break;
            default:
                return '';
                break;
        }
    }

    function renderStateBeforeMigration(value) {
        if (value) {
            return '@{R=Core.Strings;K=WEBJS_PS1_23; E=js}';
        } else {
            return "@{R=Core.Strings;K=WEBJS_PS1_24; E=js}";
        }
    }

    function columnWrap(val) {
        return '<div style="white-space:normal !important;">' + val + '</div>';
    }

    function newAlert(e) {
        e.preventDefault();
        var guid = SW.Core.Services.generateNewGuid();
        SW.Core.Services.postToTarget(String.format("/Orion/Alerts/Add/Default.aspx?AlertWizardGuid={0}", guid));
    }

    function editAlert(e) {
        e.preventDefault();
        var guid = SW.Core.Services.generateNewGuid();
        var editParams = {
            AlertID: $(this).data('alertid')
        };
        SW.Core.Services.postToTarget(String.format("/Orion/Alerts/Add/Default.aspx?AlertWizardGuid={0}", guid), editParams);
    }

    this.Init = function (migrationId) {
        var gridColumnsModel = [
            { header: '@{R=Core.Strings.2;K=WEBJS_IT0_7; E=js}', width: 20, sortable: true, dataIndex: 'OldAlertName' },
            { header: '@{R=Core.Strings.2;K=WEBJS_IT0_8; E=js}', width: 10, sortable: true, dataIndex: 'AlertMigrationCode', renderer: renderMigrationStatus },
            { header: '@{R=Core.Strings.2;K=WEBJS_IT0_9; E=js}', width: 35, sortable: false, dataIndex: 'Messages', renderer: columnWrap },
            { header: '@{R=Core.Strings;K=PCDATA_VB1_5; E=js}', width: 15, sortable: false, dataIndex: 'AlertMigrationCode', renderer: renderAction },
            { header: '@{R=Core.Strings.2;K=WEBJS_IT0_10; E=js}', width: 20, sortable: true, dataIndex: 'EnabledBeforeMigration', renderer: renderStateBeforeMigration },
            { header: 'NewAlertId', hidden: true, hideable: false, sortable: true, dataIndex: 'NewAlertId' }
        ];
        var gridStoreColumns = [
            { name: 'OldAlertName', mapping: 0 },
            { name: 'AlertMigrationCode', mapping: 1 },
            { name: 'Messages', mapping: 2 },
            { name: 'EnabledBeforeMigration', mapping: 3 },
            { name: 'NewAlertId', mapping: 4 }
        ];

        dataStore = new ORION.WebApiStore("/api/AlertMigrationLog/GetAlertMigrationResults", gridStoreColumns, 'AlertMigrationCode');
        grid = new Ext.grid.GridPanel({
            id: 'macroVariableGrid',
            region: 'center',
            store: dataStore,
            columns: gridColumnsModel,
            sm: new Ext.grid.RowSelectionModel(),
            enableHdMenu: false,
            autoScroll: true,
            loadMask: true,
            height: 420,
            layout: 'fit',
            viewConfig: {
                forceFit: true
            },
            cls: 'panel-no-border'
        });

        grid.store.on('load', function () {
            var store = grid.getStore();
            $("#failedCount").text(_.filter(store.data.items, function (elem) { return elem.data.AlertMigrationCode == 0; }).length);
            $("#warningCount").text(_.filter(store.data.items, function (elem) { return elem.data.AlertMigrationCode == 1; }).length);
            $("#skippedCount").text(_.filter(store.data.items, function (elem) { return elem.data.AlertMigrationCode == 2; }).length);
            $("#successCount").text(_.filter(store.data.items, function (elem) { return elem.data.AlertMigrationCode == 3; }).length);
            $('[data-alertid]').on('click', editAlert);
            $('.newAlert').on('click', newAlert);
        });

        var mainGridPanel = new Ext.Panel({
            id: 'macroMainGrid',
            height: 420,
            region: 'center',
            split: true,
            layout: 'border',
            collapsible: false,
            items: [grid]
        });

        mainGridPanel.render('alertMigrationGridPlace');
        if (migrationId) {
            dataStore.proxy.conn.jsonData = { migrationId: migrationId };
            dataStore.load();
        }
    }
}