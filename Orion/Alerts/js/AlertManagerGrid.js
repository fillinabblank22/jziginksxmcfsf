Ext.namespace('SW');
Ext.namespace('SW.Orion');
if (Ext.isIE)
	delete Ext.Tip.prototype.minWidth;
Ext.QuickTips.init();
SW.Orion.AlertManager = function () {

    var selectorModel;
    var pageSizeBox;
    var sortOrder;
    var dataStore;
    var grid;
    var userPageSize;
    var pagingToolbar;
    var selectedColumns;
    var comboArray;
    var comboboxMaxWidth = 500;
    var groupingValue;
    var groupGrid;
    var grpGridFirstLoad;
    var alertIdToSelect;
    var selectedAlertName;
    var customPropertyNames = [];
    var fileUploadControlID;
    var exportImportDialogControlID;
    var alertConfigurationDataControlID;
    var validationControlID;
    var exportImportDialogWindow;
    var fileUploadControlInitialized = false;
    var userHasRightToEnableOrDisableAlert;
    var assignActionSelectorController = null;

    var exportImportDialogWindowData = {
        StripSensitiveData: true,
        ProtectionPassword: ""
    };

    var ExportImportDialogMode =
    {
        Export: 1,
        Import: 2
    };

    var hash = {
        '.xml': 1,
        '.AlertDefinition': 2
    };

    var categoryTypeEnum = {
        'Trigger': "Trigger",
        'Reset': "Reset"
    };

    var updateToolbarButtons = function () {
        var selCount = grid.getSelectionModel().getCount();
        var map = grid.getTopToolbar().items.map;
        var needsToDisable = (selCount === 0);

        map.EnableDisable.setDisabled(!userHasRightToEnableOrDisableAlert || needsToDisable);

        var disableDel = needsToDisable;
        Ext.each(grid.getSelectionModel().getSelections(), function (item) {
            if (item.data.Type == true) {
                disableDel = true;
                return;
            }
        });

        map.Edit.setDisabled(selCount != 1);
        map.DuplicateEdit.setDisabled(selCount != 1);
        map.ExportImport.menu.items.map.ExportAlert.setDisabled(selCount != 1);
        map.Delete.setDisabled(disableDel);
        map.AssignAction.menu.items.map.AssignExistingTriggerAction.setDisabled(needsToDisable);
        map.AssignAction.menu.items.map.AssignExistingResetAction.setDisabled(needsToDisable);

        var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');

        if (grid.getStore().getCount() > 0 && selCount == grid.getStore().getCount()) {
            hd.addClass('x-grid3-hd-checker-on');
        } else {
            hd.removeClass('x-grid3-hd-checker-on');
        }
    };

    function refreshGridObjects(start, limit, elem, property, type, value, search, callback) {
        elem.store.removeAll();

        var tmpfilterText = search.replace(/\*/g, "%");

        if (tmpfilterText.length > 0) {
            if (!tmpfilterText.match("^%"))
                tmpfilterText = "%" + tmpfilterText;

            if (!tmpfilterText.match("%$"))
                tmpfilterText = tmpfilterText + "%";
        }

        elem.store.proxy.conn.jsonData = { property: property, type: type, value: value || "", search: tmpfilterText };
        
        if (limit)
            elem.store.load({ params: { start: start, limit: limit }, callback: callback });
        else
            elem.store.load({ callback: callback });
    }

    var getSelectedAlertIds = function (items) {
        var ids = [];

        Ext.each(items, function (item) {
            ids.push(item.data.AlertID);
        });

        return ids;
    };

    function setMainGridHeight(pageSize) {
        window.setTimeout(function () {
            var mainGrid = Ext.getCmp('mainGrid');
            var maxHeight = calculateMaxGridHeight(mainGrid);
            var calculated = calculateGridHeight(pageSize);
            var height = (calculated > 0) ? Math.min(calculated, maxHeight) : maxHeight;
            setHeightForGrids(height);

            //we need this to add height if horizontal scroll bar is present, with low window width and in chrome
            if (grid.el.child(".x-grid3-scroller").dom.scrollWidth != grid.el.child(".x-grid3-scroller").dom.clientWidth) {
                height = Math.min(height + 20, maxHeight);
                setHeightForGrids(height);
            }
        }, 0);
    }

    function setHeightForGrids(height) {
        var mainGrid = Ext.getCmp('mainGrid');
        var groupingGrid = Ext.getCmp('groupingGrid');

        mainGrid.setHeight(Math.max(height, 300));
        groupingGrid.setHeight(Math.max(height - 50, 250)); //height without groupByTopPanel
    }

    function calculateGridHeight(numberOfRows) {
        if (grid.store.getCount() == 0)
            return 0;
        var rowsHeight = Ext.fly(grid.getView().getRow(0)).getHeight() * (numberOfRows + 1);
        return grid.getHeight() - grid.getInnerHeight() + rowsHeight + 7;
    }

    function calculateMaxGridHeight(gridPanel) {
        var gridTop = gridPanel.getPosition()[1];
        return $(window).height() - gridTop - $('#footer').height() - 25;
    }

    var deleteSelectedItems = function (items, onSuccess) {
        var toDelete = getSelectedAlertIds(items);
        var waitMsg = Ext.Msg.wait("@{R=Core.Strings;K=WEBJS_VL0_9; E=js}");

        deleteAlerts(toDelete, function (result) {
            waitMsg.hide();
            //  ErrorHandler(result);
            onSuccess(result);
        });
    };

    function deleteAlerts(ids, onSuccess) {
        ORION.callWebService("/Orion/Services/NewAlertingService.asmx",
            "DeleteAlerts", { ids: ids },
            function (result) {
                onSuccess(result);
            });
    }

    var enableDisableSelectedItems = function (items, enable, onSuccess) {
    	var toUpdate = getSelectedAlertIds(items);

    	function enableDisableAlerts() {
    		ORION.callWebService("/Orion/Services/NewAlertingService.asmx",
				"EnableDisableAlerts", { ids: toUpdate, enable: enable },
				function (result) {
					onSuccess(result);
				});
    	}

    	// Confirm before disabling
    	if (!enable) {
    		Ext.Msg.show({
    			title: '@{R=Core.Strings.2;K=WEBJS_BV0_0046; E=js}',
    			msg: '@{R=Core.Strings.2;K=WEBJS_BV0_0047; E=js}',
    			buttons: {
    				yes: '@{R=Core.Strings.2;K=WEBJS_BV0_0045; E=js}',
    				cancel: '@{R=Core.Strings;K=WEBJS_AK0_27; E=js}'
    			},
    			fn: function (btn) {
    				if (btn === 'yes') {
    					enableDisableAlerts();
    				}
    			}
    		});
    	} else {					// Enabling, no confirmation needed
    		enableDisableAlerts();
    	}
    };

    var checkForDraft = function (btnContinueText, defaultAction) {
        if (alertDraftExists()) {
            $("#draftDialog").dialog({
                resizable: false,
                width: 500,
                modal: true
            });
            $('#' + getBtnContinueID() + " .sw-btn-t").text(btnContinueText);
            $('#' + getBtnContinueID()).click(function () { defaultAction(); });
        } else {
            defaultAction();
        }
    };

    var addAlert = function () {
        checkForDraft("@{R=Core.Strings;K=WEBJS_VL0_25;E=js}", function () {
            location.href = String.format('/Orion/Alerts/Add/Default.aspx?AlertWizardGuid={0}', SW.Core.Services.generateNewGuid());
        });
    };

    var editAlert = function () {
        checkForDraft("@{R=Core.Strings;K=WEBJS_VL0_24;E=js}", redirectToEdit); 
    };

    var goToActions = function() {
        var selected = grid.getSelectionModel().getSelected();

        var ids = selected.data.TriggerActionsIds;
        if (!ids || ids == "") return;
        var name = selected.data.Name;
        SW.Core.Services.postToTarget("/Orion/Actions/Default.aspx", { ActionIds: ids, Grouping: escape(name) });
    };

    function redirectToEdit() {
        var alertId = grid.getSelectionModel().getSelected().data.AlertID;
        sessionStorage.setItem("lastEditedAlertID", alertId);
        sessionStorage.setItem("searchfieldValue", $("#searchfield").val());
        var guid = SW.Core.Services.generateNewGuid();
        var editParams = {
            AlertID: alertId
        };
        SW.Core.Services.postToTarget(String.format("/Orion/Alerts/Add/Default.aspx?AlertWizardGuid={0}", guid), editParams);
    }

    function renderEditName(value, meta, record) {
        return String.format('<a href="#" onclick="SW.Orion.AlertManager.redirectToEdit({0}); return false;" class="schedule-edit-link">{1}</a>',
                            record.data.AlertID, renderString(value));
    }

    function renderObjectType(value, meta, record) {
        var objTypeName = record.data.ObjectTypeName;
        if (!objTypeName || objTypeName.length === 0)
            return renderString(value);
        return renderString(objTypeName);
    }

    function renderSchedule(value) {
        var actionPattern = '<span ext:qtitle="" ext:qtip="{0}">{1}</span>';
        if (value != null) {
            return String.format(actionPattern, value, renderString(value));
        }
        return String.format(actionPattern, "@{R=Core.Strings;K=WEBJS_VM0_3;E=js}", renderString("@{R=Core.Strings;K=WEBJS_VM0_3;E=js}"));
    }

    function renderType(value) {
        var typePattern = '<span ext:qtitle="" ext:qtip="{0}">{1}</span>';
        var type = "@{R=Core.Strings;K=WEBJS_SO0_101;E=js}";

        if (value == true)
            type = "@{R=Core.Strings;K=WEBJS_SO0_102;E=js}";

        return String.format(typePattern, encodeHTML(type), renderString(type));
    }
    
    function renderEnabled(value) {
        if (userHasRightToEnableOrDisableAlert ) {
            if (value == true) {
                return ('<a href="#" class="toggleOn">@{R=Core.Strings;K=WEBJS_TM0_144;E=js}</a>');
            } else {
                return ('<a href="#" class="toggleOff">@{R=Core.Strings;K=LogLevel_off;E=js}</a>');
            }
        } else {
            if (value == true) {
                return ('<label class="toggleOn">@{R=Core.Strings;K=WEBJS_TM0_144;E=js}</label>');
            } else {
                return ('<label class="toggleOff">@{R=Core.Strings;K=LogLevel_off;E=js}</label>');
            }
        }
    };

    function stringToBoolean(value) {
        switch (value.toString().toLowerCase()) {
            case "true": case "yes": case "1": return true;
            case "false": case "no": case "0": case null: return false;
            default: return Boolean(value);
        }
    }

    function renderAction(value, meta, record) {
        if ($.isNumeric(value)) {
            if (value == "0")
                return renderString('@{R=Core.Strings;K=WEBJS_VL0_22; E=js}');
            var actions = record.data.TriggerActionsData;
            for (var i = 0; i < actions.length; i++) {
                if (actions[i].Title.toLowerCase().indexOf(filterText.toLowerCase() != 0) && filterText.length > 0) {
                    return '<span>' +
                           renderActionTipText(record.data.TriggerActionsData, String.format('@{R=Core.Strings;K=WEBJS_VL0_23; E=js}', value.toString())) +
                           '</span>';
                }
            }
            return renderActionTipText(record.data.TriggerActionsData, String.format('@{R=Core.Strings;K=WEBJS_VL0_23; E=js}', value.toString()));
        }
        else {
            return renderActionTipText(record.data.TriggerActionsData, value.Title);
        }
    }

   function renderActionTipText(actions, displayText) {
        var actionPattern, resultString;
        actionPattern = '<a href="#" onclick="SW.Orion.AlertManager.redirectToActions(); return false;"><span ext:qtitle="" ext:qtip="{0}">{1}</span></a>';
        var tipText = String.format("<b>{0}</b><br/><span style='word-wrap:break-word'>{1}</span>", encodeHTML(encodeHTML(actions[0].Title)), actions[0].Description); // encodeHTML twice as extjs tooltip requires.
        for (var i = 1; i < actions.length; i++) {
            tipText = String.format('@{R=Core.Strings;K=WEBJS_TM0_146; E=js}', tipText, String.format('<br/><b>{0}</b><br/>{1}', encodeHTML(encodeHTML(actions[i].Title)), actions[i].Description));
        }
        resultString = String.format(actionPattern, tipText, renderString(displayText));
        return resultString;
    }

    function renderBool(value) {
        if (!value || value.length === 0 || value.toString() == '0' || value.toString() == 'false')
            return '@{R=Core.Strings;K=WEBJS_IB0_13;E=js}';
        return '@{R=Core.Strings;K=WEBJS_IB0_12;E=js}';
    }

    function renderFloat(value) {
        if (Ext.isEmpty(value) || value.length === 0)
            return value;
        return renderString(String(value).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator));
    }

    var reMsAjax = /^\/Date\((d|-|.*)\)\/$/;
    function renderDateTime(value) {
        if (Ext.isEmpty(value) || value.length === 0)
            return '';

        var a = reMsAjax.exec(value);
        if (a) {
            var b = a[1].split(/[-,.]/);
            var val = new Date(+b[0]);
            return val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + ' ' + val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern);
        }

        var dateVal = new Date(value);
        if (dateVal && dateVal != 'Invalid Date') {
            return dateVal.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + ' ' + dateVal.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern);
        }
    }

    function renderString(value) {
        if (!value || value.length === 0)
            return value;

        if (!filterText || filterText.length === 0)
            return encodeHTML(value);

        var patternText = filterText;

        // replace any %'s with a *
        patternText = patternText.replace(/\%/g, "*");

        // replace multiple *'s with a single instance
        patternText = patternText.replace(/\*{2,}/g, "*");

        // check if the search string is now just down to a single *, and if so return without any further regex
        if (patternText == '*') {
            return '<span style=\"background-color: #FFE89E\">' + encodeHTML(value) + '</span>';
        }

        // replace \ with \\
        patternText = patternText.replace(/\\/g, '\\\\');
        // replace . with \.
        patternText = patternText.replace(/\./g, '\\.');
        // replace * with .*
        patternText = patternText.replace(/\*/g, '.*');

        // set regex pattern
        var x = '((' + escapeRegExp(patternText) + ')+)\*';
        var content = value, pattern = new RegExp(x, "gi"), replaceWith = '{SPAN-START-MARKER}$1{SPAN-END-MARKER}';

        // do regex replace + content gets encoded
        var fieldValue = encodeHTML(content.replace(pattern, replaceWith));

        // now replace the literal SPAN markers with the HTML span tags
        fieldValue = fieldValue.replace(/{SPAN-START-MARKER}/g, '<span style=\"background-color: #FFE89E\">');
        fieldValue = fieldValue.replace(/{SPAN-END-MARKER}/g, '</span>');

        return fieldValue;
    }

    // Encoding value function
    function encodeHTML(value) {
        return Ext.util.Format.htmlEncode(value);
    };

    function escapeRegExp(str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }

    function duplicateEditAlert(name, callback) {
        if ($("#isOrionDemoMode").length != 0) {
            demoAction("Core_AlertManager_DublicateEditAlert", this);
            return;
        }
        var id = grid.getSelectionModel().getSelected().data.AlertID;

        ORION.callWebService('/Orion/Services/NewAlertingService.asmx',
            'DuplicateAlert', { alertId: id, alertName: name},
            function (result) {
                if (result) {
                    var guid = SW.Core.Services.generateNewGuid();
                    var editParams = {
                        AlertID: result,
                        IsDuplicated: true
                    };
                    SW.Core.Services.postToTarget(String.format("/Orion/Alerts/Add/Default.aspx?AlertWizardGuid={0}", guid), editParams);
                } else {
                    // TODO: show message that value wasn't set
                }
            });

            if (callback) callback();
    }

    function renderStatus(status) {
        if (status === '[@{R=Core.Strings;K=LIBCODE_TM0_15;E=js}]' || status === '[All]') return '@{R=Core.Strings;K=LIBCODE_TM0_15;E=js}';
        return stringToBoolean(status) ? '@{R=Core.Strings;K=WEBJS_TM0_144;E=js}' : '@{R=Core.Strings;K=LogLevel_off;E=js}';
    }

    function renderGroup(value, meta, record) {
        var disp;
        if (comboArray.getValue().indexOf('Enabled') > -1) {
            disp = String.format('@{R=Core.Strings;K=WEBJS_TM0_145; E=js}', renderStatus(record.data.Value), record.data.Cnt);
        } else {
            if (value == '{empty}')
                value = '';


            if (!value && value != false || value == '[Unknown]') {
                value = "@{R=Core.Strings;K=StatusDesc_Unknown;E=js}";
            }
            if (value == "[All]") {
                value = "@{R=Core.Strings;K=WEBJS_SO0_31; E=js}";
            }
            disp = value + " (" + record.data.Cnt + ")";
        }
        return encodeHTML(disp);
    }

    function getAlertRowNumber(alertData, prop, type, value, isId, succeeded) {
        if (!alertData) {
            succeeded({ ind: -1 });
            return;
        }

        var sort = "";
        var dir = "";

        if (sortOrder != null) {
            sort = sortOrder[0];
            dir = sortOrder[1];
        }

        SW.Core.Services.callWebServiceSync('/Orion/Services/NewAlertingService.asmx', 'GetAlertNumber', { alertData: alertData, property: prop, type: type, value: value, sort: sort, direction: dir, isId: isId },
            function (result) {
                if (succeeded != null) {
                    succeeded({ ind: result });
                    return;
                }
                // not found
                succeeded({ ind: -1 });
                return;
            });
        }

    function exportAlert() {
        var firstSelectedAlert = grid.getSelectionModel().getSelections()[0].data;
        var id = firstSelectedAlert.AlertID;
        var alertName = firstSelectedAlert.Name;
        SW.Core.Services.postToTarget("/Orion/Alerts/Default.aspx", { ExportAlertID: encodeURIComponent(id), ExportAlertName: encodeURIComponent(alertName), StripSensitiveData: exportImportDialogWindowData.StripSensitiveData, ProtectionPassword: exportImportDialogWindowData.ProtectionPassword });
    }

    function getFileNameFromFullPath(fileNameFullPath)
    {
        return fileNameFullPath.replace(/^.*[\\\/]/, '');
    }

    function importAlertDialogCallback(alertConfigurationDataControlID, fileName) {
        var res = {
            AlertId: null,
            IncorrectPasswordForDecryptSensitiveData: false,
            Name: null,
            MigrationMessage: null,
            NetworkError: false
        };

        SW.Core.Services.callWebServiceSync("/Orion/Services/NewAlertingService.asmx",
            "ImportAlert",
            {
                stripSensitiveData: exportImportDialogWindowData.StripSensitiveData,
                protectionPassword: exportImportDialogWindowData.ProtectionPassword,
                alertConfigurationData: $("#" + alertConfigurationDataControlID).val(),
                fileName: fileName
            },
            function (result) {
                res.AlertId = result.AlertId;
                res.IncorrectPasswordForDecryptSensitiveData = result.IncorrectPasswordForDecryptSensitiveData;
                res.Name = result.Name;
            },
            function (failure) {
                res.NetworkError = true;
            }
        );

        return res;
    }

    function showExportImportDialogWindow(mode, okHandler) {
        var windowTitle = "@{R=Core.Strings;K=WEBJS_PS0_69;E=js}";
        if (ExportImportDialogMode.Import === mode) {
            windowTitle = "@{R=Core.Strings;K=WEBJS_PS0_70;E=js}";
        }

        exportImportDialogWindow = new Ext.Window({
            title: windowTitle,
            modal: true,
            html: $("#" + exportImportDialogControlID).html(),
            closeAction: "hide",
            draggable: true, 
            id: "exportImportAlertDialog",
            height: 250,
            width: 550,
            closable: true, 
            constrain: true, 
            buttonAlign: "center", 
            buttons: [ 
                {
                    text: "@{R=Core.Strings;K=WEBJS_PS0_72;E=js}",
                    handler: function () {
                        
                        if ($(".x-window-body #rbProtectSensitiveDataByPassword").attr("checked") === "checked") {
                            if ($(".x-window-body #tbProtectionPassword").val() === "") {
                                $(".x-window-body #lblPasswordValidationMessage").show();
                                return;
                            } else {
                                $(".x-window-body #lblPasswordValidationMessage").hide();
                            }

                            if (ExportImportDialogMode.Export === mode) {
                                if ($(".x-window-body #tbProtectionPassword").val() !== $(".x-window-body #tbRetypeProtectionPassword").val()) {
                                    $(".x-window-body #lblRetypePasswordValidationMessage").show();
                                    return;
                                } else {
                                    $(".x-window-body #lblRetypePasswordValidationMessage").hide();
                                }
                            }
                        }

                        exportImportDialogWindowData.StripSensitiveData = $(".x-window-body #rbStripSensitiveData").attr("checked") === "checked";
                        exportImportDialogWindowData.ProtectionPassword = $(".x-window-body #tbProtectionPassword").val();

                        if (ExportImportDialogMode.Export === mode) {
                            exportImportDialogWindow.close();
                            okHandler();
                        } else {
                            var okHandlerResult = okHandler();
                            if (!okHandlerResult.IncorrectPasswordForDecryptSensitiveData) {
                                exportImportDialogWindow.close();
                                refreshGridObjects(0, userPageSize, grid, groupingValue[0], "", groupingValue[1], filterText, function() {
                                    grid.getSelectionModel().selectRow(grid.getStore().find("AlertID", okHandlerResult.AlertId), false);
                                });
                                comboArray.store.reload();
                                groupGrid.store.reload();
                                showHint(okHandlerResult.Name, okHandlerResult.AlertId > 0 && okHandlerResult.Name !== "");
                            } else {
                                $(".x-window-body #lblPasswordValidationMessage").text("@{R=Core.Strings;K=WEBJS_PS0_71;E=js}").show();
                            }
                        }
                    }
                },
                {
                    text: "@{R=Core.Strings;K=WEBJS_PS0_73;E=js}",
                    handler: function() {
                        exportImportDialogWindow.close();
                    }
                }
            ]
        });

        exportImportDialogWindow.show();

        if (ExportImportDialogMode.Export === mode) {
            $(".x-window-body #trRetypePassword").show();
        } else {
            $(".x-window-body #exportImportDialogHeaderText").html("@{R=Core.Strings;K=WEBJS_PS0_74;E=js}");
            $(".x-window-body #lblStripSensitiveData").html("@{R=Core.Strings;K=WEBJS_PS0_75;E=js}");
            $(".x-window-body #lblStripSensitiveDataDescription").hide();
            $(".x-window-body #lblProtectSensitiveDataByPassword").html("@{R=Core.Strings;K=WEBJS_PS0_76;E=js}");
            $(".x-window-body #trRetypePassword").hide();
        }

        $(".x-window-body #rbStripSensitiveData").on("click",
            function() {
                $(".x-window-body #protectionPassword").hide();
            });

        $(".x-window-body #rbProtectSensitiveDataByPassword").on("click",
            function() {
                $(".x-window-body #protectionPassword").show();
            });

        $(".x-window-body #tbProtectionPassword").on("focusout",
            function() {
                var passwordValue = $(".x-window-body #tbProtectionPassword").val();
                if (passwordValue === "") {
                    $(".x-window-body #lblPasswordValidationMessage").show();
                } else {
                    $(".x-window-body #lblPasswordValidationMessage").hide();
                }
            });

        $(".x-window-body #tbRetypeProtectionPassword").on("focusout", function() {
            if ($(".x-window-body #rbProtectSensitiveDataByPassword").attr("checked") === "checked") {
                if ($(".x-window-body #tbProtectionPassword").val() !== $(".x-window-body #tbRetypeProtectionPassword").val()) {
                    $(".x-window-body #lblRetypePasswordValidationMessage").show();
                }
            } else {
                $(".x-window-body #lblRetypePasswordValidationMessage").hide();
            }
        });
    }

    function assignToAlert(ctegoryType) {
        // Get all ActionsIds Assigned to selected Alerts > 1 
        var actionsIds = []; 
        var selectedAlerts = grid.getSelectionModel().getSelections();

        if (selectedAlerts.length === 1) {
            if (ctegoryType === categoryTypeEnum.Trigger) {
                $.each(selectedAlerts, function() {
                    if (this.data.TriggerActionsIds !== "")
                        actionsIds.push({ ID: this.data.TriggerActionsIds.split(',') });
                });
            } else if (ctegoryType === categoryTypeEnum.Reset) {
                $.each(selectedAlerts, function() {
                    if (this.data.ResetActionsIds !== "")
                        actionsIds.push({ ID: this.data.ResetActionsIds.split(',') });
                });
            }
        }

        assignActionSelectorController = new SW.Core.Actions.AssignActionSelector({
            containerID: getDialogContinueID(),
            enviromentType: "Alerting",
            categoryType: ctegoryType,
            actions: actionsIds,
            onAssigned: function (actionDefinitions) { assignAlertsToActions(actionDefinitions, ctegoryType) }
        });

        assignActionSelectorController.ShowInDialog();
    }

    function assignAlertsToActions(actionDefinitions, categoryType) {

        var selectedAlerts = grid.getSelectionModel().getSelections();
        var alertIds = [];
        $.each(selectedAlerts, function () {
            alertIds.push(this.data.AlertID);
        });

        var actionsData = [];
        $.each(actionDefinitions, function () {
            actionsData.push({ ActionID: this.ID, CategoryType: categoryType });
        });

        var oldAlertIds = [];

        ORION.callWebService('/Orion/Services/ActionManager.asmx',
                    'UpdateActionsAssignments', { actionsDataJson: JSON.stringify(actionsData), oldAlertIds: oldAlertIds, newAlertIds: alertIds },
                    function (result) {
                        if (result) {
                            filterText = "";
                            if (alertIds.length > 1) {
                                refreshGridObjects(0, userPageSize, grid, groupingValue[0], "", groupingValue[1], filterText, function() {});
                            } else {
                                var objectIndex = grid.store.indexOf(grid.getSelectionModel().getSelections()[0]);
                                refreshGridObjects(0, userPageSize, grid, groupingValue[0], "", groupingValue[1], filterText, function() {
                                    if (objectIndex > -1) {
                                        grid.getView().focusRow(objectIndex % userPageSize);
                                        var rowEl = grid.getView().getRow(objectIndex % userPageSize);
                                        rowEl.scrollIntoView(grid.getGridEl().id, false);
                                        grid.getSelectionModel().selectRow(objectIndex % userPageSize);
                                    }
                                });
                            }
                        }
                    });
    }

    function selectAlertConfigurationWithAlertId(alertIdToSelect) {
        getAlertRowNumber(alertIdToSelect, groupingValue[0], "", groupingValue[1], true, function (res) {
            var startFrom = 0;
            var objectIndex = res.ind[0];

            if (objectIndex > -1) {
                var pageNumFl = objectIndex / userPageSize;
                var index = parseInt(pageNumFl.toString());
                startFrom = index * userPageSize;
            }

            var settToSave = ',';
            if (groupingValue && groupingValue.length == 2)
                settToSave = groupingValue[0] + ',' + groupingValue[1];
            ORION.Prefs.save('GroupingValue', settToSave);

            refreshGridObjects(startFrom, userPageSize, grid, groupingValue[0], "", groupingValue[1], filterText, function () {
                if (alertIdToSelect && alertIdToSelect != "") {
                    grid.getView().focusRow(objectIndex % userPageSize);
                    var rowEl = grid.getView().getRow(objectIndex % userPageSize);
                    if (rowEl) {
                        rowEl.scrollIntoView(grid.getGridEl().id, false);
                    }

                    if (res.ind.length > 1) {
                        var rowArray = [];
                        for (var rowId = 0; rowId < res.ind.length; rowId = rowId + 1) {
                            rowArray.push(res.ind[rowId] % userPageSize);
                        }
                        grid.getSelectionModel().selectRows(rowArray);
                    } else
                        grid.getSelectionModel().selectRow(objectIndex % userPageSize);
                }
            });
        });
    }

    function showHint(name, success) {
        if (name === "")
            return;

        var validationControl = $('#' + validationControlID);
        validationControl.css("display", "block");
        validationControl.removeClass("sw-suggestion-pass");
        validationControl.removeClass("sw-suggestion-fail");
        if (!success) {
            validationControl.addClass("sw-suggestion-fail");
        } else {
            validationControl.addClass("sw-suggestion-pass");
        }

        validationControl.empty();
        validationControl.append("<span class='sw-suggestion-icon'></span>");
        var formattedMessage = "<span style='font-weight: normal;'>" +
            String.format(success
                ? "@{R=Core.Strings;K=WEBJS_PS0_77;E=js}"
                : "@{R=Core.Strings;K=WEBJS_PS0_78;E=js}",
                "<span id='alertName'>" + name + "</span>") +
            "</span>";
        validationControl.append(formattedMessage);
    }

    ORION.prefix = 'AlertManager_';
    return {
        redirectToEdit: function () {
            editAlert();
        },
        redirectToActions: function() {
            goToActions();
        },
        init: function () {
            alertIdToSelect = $("#SelectedAlertID").val();
            userPageSize = parseInt(ORION.Prefs.load('PageSize', '20'));
            sortOrder = ORION.Prefs.load('SortOrder', 'Name,ASC').split(',');
            groupingValue = ORION.Prefs.load('GroupingValue', ',').split(',');
            selectedColumns = ORION.Prefs.load('SelectedColumns', '').split(',');
            userHasRightToEnableOrDisableAlert = ORION.Prefs.load('UserHasRightToEnableOrDisableAlert', 'False') === 'True';
            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", updateToolbarButtons);
            userPageSize = parseInt(ORION.Prefs.load('PageSize', '20'));
            sortOrder = ORION.Prefs.load('SortOrder', 'Name, ASC').split(',');
            grpGridFirstLoad = true;

            var gridStoreColumns = [
                { name: 'AlertID', mapping: 0 },
                { name: 'Name', mapping: 1 },
                { name: 'Description', mapping: 2 },
                { name: 'ObjectType', mapping: 3 },
                { name: 'TriggerActions', mapping: 5 },
                { name: 'ResetActionsCount', mapping: 6},
                { name: 'Enabled', mapping: 4 },
                { name: 'TriggerActionsData', mapping: 7 },
                { name: 'TriggerActionsIds', mapping: 8 },
                { name: 'ObjectTypeName', mapping: 9 },
                { name: 'TimeOfDay', mapping: 10 },
                { name: 'CreatedBy', mapping: 11 },
                { name: 'Type', mapping: 12 },
                { name: 'Category', mapping: 13 },
                { name: 'ResetActionsIds', mapping: 14 }
            ];

            function getGridColumnHeader(title) {
            	return '<span title="' + title + '">' + title + '</span>';
            }

            var gridColumnsModel = [selectorModel,
                { header: 'ID', width: 80, hidden: true, hideable: false, sortable: false, dataIndex: 'AlertID' },
                { header: getGridColumnHeader('@{R=Core.Strings;K=WEBJS_VB0_22; E=js}'), hideable: false, width: 350, sortable: true, dataIndex: 'Name', renderer: renderEditName },
                { header: getGridColumnHeader('@{R=Core.Strings;K=WEBJS_VL0_16; E=js}'), width: 150, sortable: true, dataIndex: 'Enabled', renderer: renderEnabled },
                { header: getGridColumnHeader('@{R=Core.Strings;K=WEBJS_VL0_12; E=js}'), width: 350, sortable: true, dataIndex: 'Description', renderer: renderString },
                { header: getGridColumnHeader('@{R=Core.Strings;K=WEBJS_VL0_13; E=js}'), width: 150, sortable: true, dataIndex: 'ObjectType', renderer: renderObjectType },
                { header: getGridColumnHeader('@{R=Core.Strings;K=WEBJS_VL0_14; E=js}'), width: 300, sortable: false, dataIndex: 'TriggerActions', renderer: renderAction },
                { header: getGridColumnHeader('@{R=Core.Strings;K=WEBJS_VL0_15; E=js}'), width: 200, sortable: true, dataIndex: 'TimeOfDay', renderer: renderSchedule },
                { header: getGridColumnHeader('@{R=Core.Strings;K=FILEDATA_VS0_47; E=js}'), width: 150, sortable: true, dataIndex: 'CreatedBy', renderer: renderString },
                { header: getGridColumnHeader('@{R=Core.Strings;K=WEBJS_AK0_7; E=js}'), width: 120, sortable: true, dataIndex: 'Type', renderer: renderType },
                { header: getGridColumnHeader('@{R=Core.Strings;K=WEBJS_SO0_109; E=js}'), width: 120, sortable: true, dataIndex: 'Category', renderer: renderString }
            ];

            // get list of custom properties to push them into datastore mapping and grid columns
            SW.Core.Services.callWebServiceSync('/Orion/Services/NewAlertingService.asmx', 'GetAlertCustomProperties', {}, function (result) {
                if (result && result.Rows) {
                    var index = gridStoreColumns.length;
                    for (var j = 0; j < result.Rows.length; j++) {
                        var renderer;
                        // setup correct renderer according to CP type
                        switch (result.Rows[j][2].toString().toLowerCase()) {
                            case "system.datetime":
                                renderer = renderDateTime;
                                break;
                            case "system.int32":
                            case "system.single":
                                renderer = renderFloat;
                                break;
                            case "system.boolean":
                                renderer = renderBool;
                                break;
                            default:
                                renderer = renderString;
                                break;
                        }
                        var columnName = String.format('CustomProperties.{0}', result.Rows[j][0]);
                        customPropertyNames.push(columnName);
                        gridStoreColumns.push({ name: columnName, mapping: index });
                        gridColumnsModel.push({ header: result.Rows[j][1], width: 150, sortable: true, dataIndex: columnName, renderer: renderer });
                        index++;
                    }
                }
            });

            // check if we're using custom property and if this custom property still exists
            if (groupingValue[0].indexOf("CustomProperties.") > -1 && $.inArray(groupingValue[0], customPropertyNames) == -1) {
                groupingValue = ',,'.split(',');
            }
            if (sortOrder[0].indexOf("CustomProperties.") > -1 && $.inArray(sortOrder[0], customPropertyNames) == -1) {
                sortOrder = 'Name,ASC'.split(',');
            }
            var isDirtyColumns = false;

            for (var index = selectedColumns.length - 1; index >= 0; index--) {
                if (selectedColumns[index].indexOf("CustomProperties.") > -1 && $.inArray(selectedColumns[index], customPropertyNames) == -1) {
                    selectedColumns.splice(index, 1);
                    isDirtyColumns = true;
                }
            }

            if (isDirtyColumns)
                ORION.Prefs.save('SelectedColumns', selectedColumns.join(','));

            dataStore = new ORION.WebServiceStore("/Orion/Services/NewAlertingService.asmx/GetAlerts", gridStoreColumns);
            dataStore.sortInfo = { field: sortOrder[0], direction: sortOrder[1] }; // or 'DESC' (case sensitive for local sorting) };
            dataStore.on('beforeload', function () {
                if (dataStore.proxy.conn.jsonData == undefined) {
                    dataStore.proxy.conn.jsonData = { property: "", type: "", value: "", search: "" };
                }
            });

            // combo datastore
            var groupingDataStore = new ORION.WebServiceStore(
                "/Orion/Services/NewAlertingService.asmx/GetObjectGroupProperties",
                [
                    { name: 'Value', mapping: 0 },
                    { name: 'Name', mapping: 1 },
                    { name: 'Type', mapping: 2 }
                ]);

            //datastore for grouping grid > for selected combo value
            var groupingStore = new ORION.WebServiceStore(
                "/Orion/Services/NewAlertingService.asmx/GetAlertGroupValues",
                [
                    { name: 'Value', mapping: 0 },
                    { name: 'DisplayNamePlural', mapping: 1 },
                    { name: 'Cnt', mapping: 2 }
                ],
                "Value", "");

            comboArray = new Ext.form.ComboBox(
                {
                    id: 'mrGroupCombo',
                    mode: 'local',
                    boxMaxWidth: comboboxMaxWidth,
                    fieldLabel: 'Name',
                    displayField: 'Name',
                    valueField: 'Value',
                    store: groupingDataStore,
                    triggerAction: 'all',
                    value: '@{R=Core.Strings;K=WEBJS_VB0_76; E=js}',
                    typeAhead: true,
                    forceSelection: true,
                    selectOnFocus: false,
                    multiSelect: false,
                    editable: false,
                    listeners: {
                        'select': function () {
                            var val = this.store.data.items[this.selectedIndex].data.Value;
                            var type = this.store.data.items[this.selectedIndex].data.Type;

                            refreshGridObjects(null, null, groupGrid, val, type, "", filterText);

                            if (val == '') {
                                ORION.Prefs.save('GroupingValue', ',');
                                refreshGridObjects(0, userPageSize, grid, "", "", "", filterText, function () {
                                    $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function () {
                                        this.tooltip = 'processed';
                                        $.swtooltip(this);
                                    });
                                });
                            }
                            else {
                                var newGroupingValues = val + ',';
                                ORION.Prefs.save('GroupingValue', newGroupingValues);
                            }
                        },
                        'beforerender': function () {
                            refreshGridObjects(null, null, comboArray, "", "", "", "");
                        }
                    }
                });

            comboArray.getStore().on('load', function () {
                comboArray.setValue(groupingValue[0]);

                refreshGridObjects(null, null, groupGrid, groupingValue[0], groupingValue[0], "", "");
            });

            pageSizeBox = new Ext.form.NumberField({
                id: 'PageSizeField',
                enableKeyEvents: true,
                allowNegative: false,
                width: 40,
                allowBlank: false,
                minValue: 1,
                maxValue: 100,
                value: userPageSize,
                listeners: {
                    scope: this,
                    'keydown': function (f, e) {
                        var k = e.getKey();
                        if (k == e.RETURN) {
                            e.stopEvent();
                            var v = parseInt(f.getValue());
                            if (!isNaN(v) && v > 0 && v <= 100) {
                                userPageSize = v;
                                pagingToolbar.pageSize = userPageSize;
                                ORION.Prefs.save('PageSize', userPageSize);
                                pagingToolbar.doLoad(0);
                                setMainGridHeight(userPageSize);
                            }
                            else {
                                Ext.Msg.show({
                                    title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                    msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                                });
                                return;
                            }
                        }
                    },
                    'focus': function (field) {
                        field.el.dom.select();
                    },
                    'change': function (f, numbox, o) {
                        var pSize = parseInt(f.getValue());
                        if (isNaN(pSize) || pSize < 1 || pSize > 100) {
                            Ext.Msg.show({
                                title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                            });
                            return;
                        }
                        if (pagingToolbar.pageSize != pSize) { // update page size only if it is different
                            pagingToolbar.pageSize = pSize;
                            userPageSize = pSize;

                            ORION.Prefs.save('PageSize', userPageSize);
                            pagingToolbar.doLoad(0);
                            setMainGridHeight(userPageSize);
                        }
                    }
                }
            });

            pagingToolbar = new Ext.PagingToolbar(
                { id: 'rmGridPaging',
                    store: dataStore,
                    pageSize: userPageSize,
                    displayInfo: true,
                    displayMsg: "@{R=Core.Strings;K=WEBJS_TM0_5; E=js}",
                    emptyMsg: "@{R=Core.Strings;K=WEBJS_VL0_10; E=js}",
                    beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                    afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                    firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                    prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                    nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                    lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                    refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}",
                    items: [
                        '-',
                        new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_VB0_46; E=js}', style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;' }),
                        pageSizeBox
                    ]
                }
            );

            grid = new Ext.grid.GridPanel({
                region: 'center',
                viewConfig: {
                    forceFit: false, emptyText: "@{R=Core.Strings;K=WEBJS_VL0_10; E=js}",
                    getRowClass: function (rec, rowIdx, params, store) {
                        return rec.get('Enabled') ? '' : 'disabled-row';
                    }
                },
                store: dataStore,
                baseParams: { start: 0, limit: userPageSize },
                stripeRows: true,
                trackMouseOver: false,
                split: true,
                columns: gridColumnsModel,
                listeners: {
                    cellclick: function (grid, rowIndex, columnIndex, e) {
                        var fieldName = grid.getColumnModel().getDataIndex(columnIndex); // Get field name
                        if (userHasRightToEnableOrDisableAlert && fieldName == 'Enabled') {
                            if ($("#isOrionDemoMode").length != 0) {
                                demoAction("Core_AlertManager_EnableDisableAlert", this);
                                return;
                            }
                            var record = grid.getStore().getAt(rowIndex); // Get the Record
                            var data = record.get(fieldName);
                            var id = record.get('AlertID');

                            var toggleEnabled = function () {
                            	ORION.callWebService("/Orion/Services/NewAlertingService.asmx",
										"EnableDisableAlert", { alertId: id, value: !data },
										function (result) {
											if (result) {
												record.set(fieldName, !stringToBoolean(data));
												record.commit();
											} else {
												Ext.Msg.show({
													title: '@{R=Core.Strings;K=WEBJS_TM0_74; E=js}', //todo - clarify message with Andrew
													msg: '@{R=Core.Strings;K=WEBJS_VL0_17; E=js}',
													icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
												});
											}
										});
                            };

                            if (data) {					// Enabled, confirm before disabling
                            	Ext.Msg.show({
                            		title: '@{R=Core.Strings.2;K=WEBJS_BV0_0043; E=js}',
                            		msg: '@{R=Core.Strings.2;K=WEBJS_BV0_0044; E=js}',
                            		buttons: {
                            			yes: '@{R=Core.Strings.2;K=WEBJS_BV0_0045; E=js}',
                            			cancel: '@{R=Core.Strings;K=WEBJS_AK0_27; E=js}'
                            		},
                            		fn: function (btn) {
                            			if (btn === 'yes') {
                            				toggleEnabled();
                            			}
                            		}
                            	});
                            } else {					// Disabled, go ahead and enable
                            	toggleEnabled();
                            }
						}
						else if (fieldName == 'TriggerActions') {
							var ids = grid.getStore().getAt(rowIndex).get("TriggerActionsIds");
							if (!ids || ids == "") return;
							var name = grid.getStore().getAt(rowIndex).get("Name");
							SW.Core.Services.postToTarget("/Orion/Actions/Default.aspx", { ActionIds: ids, Grouping: escape(name) });
						}
                    }
                },
            	sm: selectorModel, autoScroll: 'true', loadMask: true, width: 750, height: 350,
                tbar: [
                        { id: 'Add', text: '@{R=Core.Strings;K=WEBJS_VL0_5; E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_VL0_5; E=js}', icon: '/Orion/Nodes/images/icons/icon_add.gif', cls: 'x-btn-text-icon', handler: addAlert }, '-',
                        { id: 'Edit', text: '@{R=Core.Strings;K=WEBJS_VL0_6; E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_VL0_6; E=js}', icon: '/Orion/Nodes/images/icons/icon_edit.gif', cls: 'x-btn-text-icon', handler: editAlert }, '-',
                        { id: 'DuplicateEdit', text: '@{R=Core.Strings;K=WEBJS_TM0_129;E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_TM0_129;E=js}', icon: '/Orion/images/Reports/duplicateEdit_report.png', cls: 'x-btn-text-icon', handler: function() { duplicateEditAlert(''); } }, '-',
                        { id: 'EnableDisable', text: '@{R=Core.Strings;K=WEBJS_VL0_18;E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_VL0_18;E=js}', icon: '/Orion/images/enable_disable.png', cls: 'x-btn-text-icon',
                            menu: {
                                xtype: 'menu',
                                items: [{
                                    id: 'EnableAlerts',
                                    text: '@{R=Core.Strings;K=WEBJS_YK0_14;E=js}',
                                    tooltip: '@{R=Core.Strings;K=WEBJS_YK0_14;E=js}',
                                    icon: '/Orion/images/Check.Green.gif',
                                    cls: 'x-btn-text-icon',
                                    handler: function () {
                                        if ($("#isOrionDemoMode").length != 0) {
                                            demoAction("Core_AlertManager_EnableDisableAlert", this);
                                            return;
                                        }
                                        enableDisableSelectedItems(grid.getSelectionModel().getSelections(), true, function () {
                                            refreshGridObjects(0, userPageSize, grid, groupingValue[0], "", groupingValue[1], filterText, function () { });
                                        });
                                        this.parentMenu.hide();
                                    }
                                },
                                    {
                                        id: 'DisableAlerts',
                                        text: '@{R=Core.Strings;K=WEBJS_YK0_15;E=js}',
                                        tooltip: '@{R=Core.Strings;K=WEBJS_YK0_15;E=js}',
                                        icon: '/Orion/images/failed_16x16.gif',
                                        cls: 'x-btn-text-icon',
                                        handler: function () {
                                            if ($("#isOrionDemoMode").length != 0) {
                                                demoAction("Core_AlertManager_EnableDisableAlert", this);
                                                return;
                                            }
                                            enableDisableSelectedItems(grid.getSelectionModel().getSelections(), false, function () {
                                                refreshGridObjects(0, userPageSize, grid, groupingValue[0], "", groupingValue[1], filterText, function () { });
                                            });
                                            this.parentMenu.hide();
                                        }
                                    }]
                            }
                        }, '-',
                        {
                            id: 'AssignAction',
                            text: '@{R=Core.Strings.2;K=WEBJS_AY0_19;E=js}',
                            tooltip: '@{R=Core.Strings.2;K=WEBJS_AY0_19;E=js}',
                            icon: '/Orion/images/Reports/icon_assign_to.gif',
                            cls: 'x-btn-text-icon',
                            menu: {
                                xtype: 'menu',
                                items: [
                                    {
                                        id: 'AssignExistingTriggerAction',
                                        text: '@{R=Core.Strings.2;K=WEBJS_AY0_20;E=js}',
                                        tooltip: '@{R=Core.Strings.2;K=WEBJS_AY0_20;E=js}',
                                        icon: '/Orion/images/Reports/icon_assign_to.gif',
                                        cls: 'x-btn-text-icon',
                                        handler: function () {
					if ($("#isOrionDemoMode").length != 0) {
                                                demoAction("Core_AlertManager_AssignTrigger", this);
                                                return;
                                            }
                                            this.parentMenu.hide();
                                            assignToAlert(categoryTypeEnum.Trigger);
                                        }
                                    },
                                    {
                                        id: 'AssignExistingResetAction',
                                        text: '@{R=Core.Strings.2;K=WEBJS_AY0_21;E=js}',
                                        tooltip: '@{R=Core.Strings.2;K=WEBJS_AY0_21;E=js}',
                                        icon: '/Orion/images/Reports/icon_assign_to.gif',
                                        cls: 'x-btn-text-icon',
                                        handler: function() {
					if ($("#isOrionDemoMode").length != 0) {
                                                demoAction("Core_AlertManager_AssignReset", this);
                                                return;
                                            }
                                            assignToAlert(categoryTypeEnum.Reset);
                                        }
                                    }]
                            }
                        },'-',
                        { id: 'ExportImport', text: '@{R=Core.Strings;K=WEBJS_YK0_16;E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_YK0_16;E=js}', icon: '/Orion/images/import_export_icon_16x16.png', cls: 'x-btn-text-icon',
                            menu: {
                                xtype: 'menu',
                                items: [{
                                    id: 'ExportAlert',
                                    text: '@{R=Core.Strings;K=WEBJS_YK0_17;E=js}',
                                    tooltip: '@{R=Core.Strings;K=WEBJS_YK0_17;E=js}',
                                    icon: '/Orion/images/CPE/export.png',
                                    cls: 'x-btn-text-icon',
                                    handler: function () {
                                        if ($("#isOrionDemoMode").length != 0) {
                                            demoAction("Core_AlertManager_ExportAlert", this);
                                            return;
                                        }

                                        var alertId = grid.getSelectionModel().getSelected().data.AlertID;
                                            ORION.callWebService("/Orion/Services/NewAlertingService.asmx",
                                                "AreSensitiveDataPresentInAlertConfiguration",
                                                { alertId: alertId },
                                                function(result) {
                                                    if (result) {
                                                        showExportImportDialogWindow(ExportImportDialogMode.Export, exportAlert);
                                                    } else {
                                                        exportAlert();
                                                    }
                                                });
                                    }
                                },
                                    {
                                        id: 'ImportAlert',
                                        text: '@{R=Core.Strings;K=WEBJS_YK0_18;E=js}',
                                        tooltip: '@{R=Core.Strings;K=WEBJS_YK0_18;E=js}',
                                        icon: '/Orion/images/CPE/import.png',
                                        cls: 'x-btn-text-icon',
                                        handler: function () {
                                            if ($("#isOrionDemoMode").length != 0) {
                                                demoAction("Core_AlertManager_ImportAlert", this);
                                                return;
                                            }
                                            this.parentMenu.hide();
                                        }
                                    }],
                                listeners: {
                                    hide: function () {
                                        // menu hide event fire before click event on file uploader, so set 1 sec timeout before hiding file uploader control
                                        setTimeout(function () { $("#" + fileUploadControlID).css({ 'display': 'none' }); }, 1000);
                                    },
                                    show: function () {
                                        if ($("#isOrionDemoMode").length != 0) {
                                            return;
                                        }
                                        if (!fileUploadControlInitialized && fileUploadControlID != undefined) {
                                            $('#ImportAlert').mousemove(function (e) {
                                                var position = $(this).offset();
                                                $("#" + fileUploadControlID).css({
                                                    'display': 'block',
                                                    'left': position.left,
                                                    'top': position.top,
                                                    'height': $(this).outerHeight(),
                                                    'width': $(this).outerWidth()
                                                });
                                            });
                                            fileUploadControlInitialized = true;
                                        }
                                    }
                                }
                            }

                        }, '-',
                        { id: 'Delete', text: '@{R=Core.Strings;K=CommonButtonType_Delete; E=js}', tooltip: '@{R=Core.Strings;K=CommonButtonType_Delete; E=js}', icon: '/Orion/Nodes/images/icons/icon_delete.gif', cls: 'x-btn-text-icon',
                            handler: function () {
                                if ($("#isOrionDemoMode").length != 0) {
                                    demoAction("Core_AlertManager_DeleteAlert", this);
                                    return;
                                }
                                Ext.Msg.confirm("@{R=Core.Strings;K=WEBJS_VL0_7; E=js}", "@{R=Core.Strings;K=WEBJS_VL0_8; E=js}",
                                    function (btn) {
                                        if (btn == 'yes') {
                                            deleteSelectedItems(grid.getSelectionModel().getSelections(), function () {
                                                if (comboArray.getValue() == "@{R=Core.Strings;K=WEBJS_VB0_76; E=js}" || comboArray.getValue() == "") {
                                                    refreshGridObjects(0, userPageSize, grid, "", "", "", filterText, function () { });
                                                }
                                                else {
                                                    refreshGridObjects(null, null, groupGrid, groupingValue[0], groupingValue[0], "", filterText, function () {
                                                        groupGrid.getSelectionModel().selectRow(groupGrid.getStore().find("Value", groupingValue[1]), false);
                                                    });
                                                    refreshGridObjects(0, userPageSize, grid, groupingValue[0], "", groupingValue[1], filterText, function () { });
                                                }
                                            });
                                        }
                                    });
                            }
                        },'-'
                        , ' ', '->',
                        new Ext.ux.form.SearchField({
                            store: dataStore,
                            width: 200
                        })
                    ],
                bbar: pagingToolbar
            });

            // making columns visible or hidden according to saved configuration
            for (var i = 1; i < grid.getColumnModel().getColumnCount(); i++) {
                if (selectedColumns.indexOf(grid.getColumnModel().getDataIndex(i)) > -1 && i > 1) {
                    grid.getColumnModel().setHidden(i, false);
                } else {
                    grid.getColumnModel().setHidden(i, true);
                }
            }

            grid.on('sortchange', function (store, option) {
                var sort = option.field;
                var dir = option.direction;

                if (sort && dir)
                    ORION.Prefs.save('SortOrder', sort + ',' + dir);
            });

            grid.store.on('load', function () {
                grid.getEl().unmask();
                setMainGridHeight(userPageSize);
            });

            grid.getColumnModel().on('hiddenchange', function () {
                var cols = '';
                for (var i = 1; i < grid.getColumnModel().getColumnCount(); i++) {
                    if (!grid.getColumnModel().isHidden(i)) {
                        cols += grid.getColumnModel().getDataIndex(i) + ',';
                    }
                }
                ORION.Prefs.save('SelectedColumns', cols.slice(0, -1));
            });

            groupGrid = new Ext.grid.GridPanel({
                id: 'groupingGrid',
                store: groupingStore,
                cls: 'hide-header',
                columns: [
                        { id: 'Value', width: 193, editable: false, sortable: false, dataIndex: 'DisplayNamePlural', renderer: renderGroup }
                    ],
                selModel: new Ext.grid.RowSelectionModel({ singleSelect: true }),
                autoScroll: true,
                heigth: 300,
                loadMask: true,
                listeners: {
                    cellclick: function (mygrid, row, cell, e) {
                        var val = mygrid.store.data.items[row].data[mygrid.store.data.items[row].fields.keys[cell]];
                        var type = "";
                        $.each(comboArray.store.data.items, function (index, item) {
                            if (item.data.Value == comboArray.getValue()) {
                                type = item.data.Type;
                                return;
                            }
                        });
                        if (val == '') {
                            val = '{empty}';
                        }

                        if (val == null) {
                            val = "null";
                        }
                        if (val == '[All]')
                            val = '';

                        var newGroupingValues = comboArray.getValue() + ',' + val;
                        groupingValue = newGroupingValues.split(',');
                        
                        //Do not save invalid date to the db
                        if (type == 'datetime' && isNaN(Date.parse(val))) {
                            ORION.Prefs.save('GroupingValue', comboArray.getValue() + ',');
                        } else {
                            ORION.Prefs.save('GroupingValue', newGroupingValues);
                        }
                        
                        refreshGridObjects(0, userPageSize, grid, comboArray.getValue(), type, val, filterText, function () {
                            $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function () {
                                this.tooltip = 'processed';
                                $.swtooltip(this);
                            });
                        });
                    }
                },
                anchor: '0 0', viewConfig: { forceFit: true }, split: true, autoExpandColumn: 'Value'
            });

            groupGrid.store.on('load', function () {
                if (grpGridFirstLoad) {
                    var selectedVal = groupGrid.getStore().find("Value", groupingValue[1]);
                    if (selectedVal == -1) {
                        groupingValue[1] = "";
                        selectedVal = 0;
                    }

                    setTimeout(function () { groupGrid.getSelectionModel().selectRow(selectedVal, false);}, 0);
                    grpGridFirstLoad = false;
                }
            });

            var groupByTopPanel = new Ext.Panel({
                id: 'groupByTopPanel',
                region: 'center',
                split: false,
                heigth: 50,
                width: 200,
                collapsible: false,
                viewConfig: { forceFit: true },
                items: [new Ext.form.Label({ text: "@{R=Core.Strings;K=WEBJS_AK0_76; E=js}" }), comboArray],
                cls: 'panel-no-border panel-bg-gradient'
            });

            var navPanel = new Ext.Panel({
                id: 'navPanel',
                region: 'west',
                width: 200,
                heigth: 350,
                split: true,
                anchor: '0 0',
                collapsible: false,
                viewConfig: { forceFit: true },
                items: [groupByTopPanel, groupGrid],
                cls: 'panel-no-border'
            });

            navPanel.on("bodyresize", function () {
                groupGrid.setWidth(navPanel.getSize().width);
            });

            var searchFilter = function (storage) {

                var searchfieldValue = sessionStorage.getItem("searchfieldValue");
                if (searchfieldValue != null && searchfieldValue != "@{R=Core.Strings;K=WEBJS_TM0_53;E=js}") {
                    $("#searchfield").val(searchfieldValue);
                    filterText = searchfieldValue;
                } else {
                    return storage;
                }
                var tmpfilterText = "";
                tmpfilterText = filterText.replace(/\*/g, "%");

                if (tmpfilterText == "@{R=Core.Strings;K=WEBJS_TM0_53;E=js}") {
                    tmpfilterText = "";
                }

                if (tmpfilterText.length > 0) {
                    if (!tmpfilterText.match("^%"))
                        tmpfilterText = "%" + tmpfilterText;

                    if (!tmpfilterText.match("%$"))
                        tmpfilterText = tmpfilterText + "%";
                }
                storage.proxy.conn.jsonData = { property: '', type: '', value: '', search: tmpfilterText };
                var o = { start: 0, limit: parseInt(ORION.Prefs.load('PageSize', '20')) };
                storage.load({ params: o });
                sessionStorage.removeItem("searchfieldValue");
                return storage;
            };

            var mainGridPanel = new Ext.Panel({ id: 'mainGrid', region: 'center', split: true, layout: 'border', collapsible: false, items: [navPanel, grid], cls: 'no-border' });
            mainGridPanel.render("AlertManagerGrid");
            selectedAlertName = $('#alertName').text();
            searchFilter(dataStore);

            var lastEditedAlertId = sessionStorage.getItem("lastEditedAlertID");
            if (lastEditedAlertId != null) {
                sessionStorage.removeItem("lastEditedAlertID");
            }

            if ((alertIdToSelect == null || alertIdToSelect === "") &&
                (lastEditedAlertId != null && lastEditedAlertId != ""))
                alertIdToSelect = lastEditedAlertId;

            if (alertIdToSelect && alertIdToSelect != "") {
                selectAlertConfigurationWithAlertId(alertIdToSelect);
            }
            else if (selectedAlertName && selectedAlertName != "") {
                getAlertRowNumber(selectedAlertName, groupingValue[0], "", groupingValue[1], false, function (res) {
                    var startFrom = 0;
                    var objectIndex = res.ind;

                    if (objectIndex > -1) {
                        var pageNumFl = objectIndex / userPageSize;
                        var index = parseInt(pageNumFl.toString());
                        startFrom = index * userPageSize;
                    }
                    refreshGridObjects(startFrom, userPageSize, grid, groupingValue[0], "", groupingValue[1], filterText, function () {
                        grid.getView().focusRow(objectIndex % userPageSize);
                        var rowEl = grid.getView().getRow(objectIndex % userPageSize);
                        if (rowEl) {
                            rowEl.scrollIntoView(grid.getGridEl().id);
                            grid.getSelectionModel().selectRow(objectIndex % userPageSize);
                        }
                    });
                });
            }
            else {
                refreshGridObjects(0, userPageSize, grid, groupingValue[0], "", groupingValue[1], filterText);
            }

            $(window).resize(function () {
                setMainGridHeight(userPageSize);
                mainGridPanel.doLayout();
            });
            updateToolbarButtons();
        },
        setFileUploadControl: function (id) {
            fileUploadControlID = id;
        },
        importFileSelected: function (file) {
            var re = /\.[^.]+$/;
            if (file.value === "")
                return false;

            var ext = file.value.match(re);
            if (hash[ext]) {
                if (typeof (FileReader) === "undefined") {
                    __doPostBack(fileUploadControlID, '');
                } else {
                    var fileUploadControl = $("#" + fileUploadControlID);
                    var fileToUpload = $("#" + fileUploadControlID)[0].files[0];
                    var fileReader = new FileReader();
                    var fileName = getFileNameFromFullPath(ext.input);
                    fileReader.onload = function(readerEvt) {
                        $("#" + alertConfigurationDataControlID).val(readerEvt.target.result);
                        Ext.MessageBox.hide();
                        exportImportDialogWindowData.StripSensitiveData = false;
                        exportImportDialogWindowData.ProtectionPassword = "";
                        var importResult = importAlertDialogCallback(alertConfigurationDataControlID, fileName);
                        if (importResult.IncorrectPasswordForDecryptSensitiveData) {
                            showExportImportDialogWindow(ExportImportDialogMode.Import,
                                function() {
                                    return importAlertDialogCallback(alertConfigurationDataControlID, fileName);
                                });
                        } else {
                            var name = importResult.Name === null && importResult.AlertId === null
                                ? fileName
                                : importResult.Name;
                            showHint(name, importResult.AlertId > 0 && importResult.Name !== "");
                            selectAlertConfigurationWithAlertId(importResult.AlertId);

                            comboArray.store.reload();
                            groupGrid.store.reload();
                        }
                    };
                    fileReader.onloadstart = function(readerEvt) {
                        Ext.MessageBox.show({
                            title: "@{R=Core.Strings;K=WEBJS_PS0_79;E=js}",
                            msg: "@{R=Core.Strings;K=WEBJS_PS0_80;E=js}",
                            progressText: "@{R=Core.Strings;K=WEBJS_PS0_81;E=js}",
                            width: 350,
                            progress: true,
                            closable: true
                        });
                    };
                    fileReader.onloadend = function(readerEvt) {
                        Ext.MessageBox.hide();
                    };
                    fileReader.onprogress = function (readerEvt) {
                        var loaded = readerEvt.loaded / readerEvt.total;
                        Ext.MessageBox.updateProgress(loaded, String.format("@{R=Core.Strings;K=WEBJS_PS0_82;E=js}", Math.round(loaded*100)));
                    };
                    
                    fileReader.readAsText(fileToUpload, 'UTF-8');
                    fileUploadControl.val(null);
                }
                return true;
            } else {
                Ext.Msg.show({
                    width: 400,
                    title: "@{R=Core.Strings;K=WEBJS_YK0_24; E=js}",
                    msg: "@{R=Core.Strings;K=WEBJS_YK0_25; E=js}",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
                return false;
            }
        },
        setExportImportDialogControl: function(id) {
            exportImportDialogControlID = id;
        },
        setAlertConfigurationDataControl: function(id) {
            alertConfigurationDataControlID = id;
        },
        showImportFileDialog: function(alertConfigurationDataControlID, fileName) {
            showExportImportDialogWindow(ExportImportDialogMode.Import, function () {
                return importAlertDialogCallback(alertConfigurationDataControlID, getFileNameFromFullPath(fileName));
            });
        },
        setValidationControl: function(id) {
            validationControlID = id;
        }
    };

} ();
Ext.onReady(SW.Orion.AlertManager.init, SW.Orion.AlertManager);
