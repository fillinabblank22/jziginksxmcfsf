﻿Ext.namespace('SW');
Ext.namespace('SW.Orion');
Ext.QuickTips.init();
SW.Orion.AlertPicker = function () {
    var selectedAlertsIds = new Array();
    var alertsComboArray;
    var pageSizeBox;
    var userPageSize;
    var alertsSelectorModel;
    var alertsGroupingValue;
    var dataStore;
    var alertsGrid;
    var alertsGroupGrid;
    var comboboxMaxWidth = 500;
    var noAlertsSelectedElem = String.format("<span style='color:gray;'>{0}</span>", "@{R=Core.Strings.2;K=WEBJS_AY0_16; E=js}");

    function encodeHTML(value) {
        return Ext.util.Format.htmlEncode(value);
    }

    function renderString(value) {
        if (!value || value.length === 0)
            return value;

        if (!filterText || filterText.length === 0)
            return encodeHTML(value);

        var patternText = filterText;

        // replace any %'s with a *
        patternText = patternText.replace(/\%/g, "*");

        // replace multiple *'s with a single instance
        patternText = patternText.replace(/\*{2,}/g, "*");

        // check if the search string is now just down to a single *, and if so return without any further regex
        if (patternText == '*') {
            return '<span style=\"background-color: #FFE89E\">' + encodeHTML(value) + '</span>';
        }

        // replace \ with \\
        patternText = patternText.replace(/\\/g, '\\\\');
        // replace . with \.
        patternText = patternText.replace(/\./g, '\\.');
        // replace * with .*
        patternText = patternText.replace(/\*/g, '.*');

        // set regex pattern
        var x = '((' + escapeRegExp(patternText) + ')+)\*';
        var content = value, pattern = new RegExp(x, "gi"), replaceWith = '{SPAN-START-MARKER}$1{SPAN-END-MARKER}';

        // do regex replace + content gets encoded
        var fieldValue = encodeHTML(content.replace(pattern, replaceWith));

        // now replace the literal SPAN markers with the HTML span tags
        fieldValue = fieldValue.replace(/{SPAN-START-MARKER}/g, '<span style=\"background-color: #FFE89E\">');
        fieldValue = fieldValue.replace(/{SPAN-END-MARKER}/g, '</span>');

        return fieldValue;
    }

    function renderGroup(value, meta, record) {
        var disp;
        if (value == '{empty}')
            value = '';

            if (!value && value != false || value == '') {
                value = "@{R=Core.Strings;K=StatusDesc_Unknown;E=js}";
            }
            if (value == "[All]") {
                value = "@{R=Core.Strings;K=WEBJS_SO0_31; E=js}";
            }
            disp = value + " (" + record.data.Cnt + ")";
        return '<span ext:qtitle="" ext:qtip="' + disp + '">' + encodeHTML(disp) + '</span>';
    }

    function escapeRegExp(str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }

    function getAlertRowIndexById(id) {
        var rowIndex;
        for (var i = 0; i < alertsGrid.store.data.items.length; i++) {
            if (alertsGrid.getStore().getAt(i).data.AlertID == id) {
                rowIndex = i;
                return rowIndex;
            }
        }
        return null;
    }

    function addAlertToSelection(id, name) {
        if (selectedAlertsIds.length == 0) {
            $("#selectedObjects").empty();
        }
        selectedAlertsIds.push([id, name]);
        $("#selectedObjects").append("<span class='selectedGridItem' dataRow=" + id + ">" + name + "<img class='removeObjectCross' src='/Orion/images/Reports/delete_netobject.png' style='cursor: pointer;margin-bottom: -3px; margin-left: 5px;'></span>");
        $("#selectedObjects span[dataRow=" + id + "] img").one('click', function () {
            var index = $(this).parent().attr("dataRow");
            removeAlertFromSelection(index);
            refreshSelection();
            checkAllOrNoneSelection();
        });
    }

    function removeAlertFromSelection(index) {
        var ind = $.grep(selectedAlertsIds, function (n) { return n[0] == index; });
        for (var i = 0; i < ind.length; i++) {
            selectedAlertsIds.splice(selectedAlertsIds.indexOf(ind[i]), 1);
        }
        $("#selectedObjects span[dataRow=" + index + "]").remove();

    }

    function refreshSelection() {
        var containerId = "#selectedObjects";
        if (selectedAlertsIds.length == 0) {
            $(containerId).children().remove();
            $(containerId).append(noAlertsSelectedElem);
        }
        if (containerId == "#selectedObjects") {
            var rowsIds = new Array();
            $.each(selectedAlertsIds, function () {
                rowsIds.push(getAlertRowIndexById(this[0]));
            });
            alertsGrid.getSelectionModel().selectRows(rowsIds);
        }
        $(".selectedActionsLabel").text(String.format('@{R=Core.Strings.2;K=WEBJS_AY0_15; E=js}', selectedAlertsIds.length));
    }

    function checkAllOrNoneSelection() {
        var count = 0;
        $.each(alertsGrid.getSelectionModel().selections.items, function () {
            var item = this;
            if ($.grep(alertsGrid.store.data.items, function (n) { return n.data.AlertID == item.data.AlertID; }).length == 1) {
                count++;
            }
        });
        if (alertsGrid.store.data.items.length == count && count != 0) {
            $("#checker").parent().addClass("x-grid3-hd-checker-on");
        } else {
            $("#checker").parent().removeClass("x-grid3-hd-checker-on");
        }
    }

    function refreshGridObjects(start, limit, elem, property, type, value, search, callback) {
        elem.store.removeAll();
        var tmpfilterText = search.replace(/\*/g, "%");
        if (tmpfilterText.length > 0) {
            if (!tmpfilterText.match("^%"))
                tmpfilterText = "%" + tmpfilterText;
            if (!tmpfilterText.match("%$"))
                tmpfilterText = tmpfilterText + "%";
        }

        elem.store.proxy.conn.jsonData = { property: property, type: type, value: value || "", search: tmpfilterText };

        if (limit)
            elem.store.load({ params: { start: start, limit: limit }, callback: callback });
        else
            elem.store.load({ callback: callback });
    }

    function resizeToFitContent(combo) {
        if (!combo.elMetrics) {
            combo.elMetrics = Ext.util.TextMetrics.createInstance(combo.getEl());
        }
        var m = combo.elMetrics, width = 0, el = combo.el, s = combo.getSize();
        combo.store.each(function (r) {
            var text = r.get(combo.displayField);
            width = Math.max(width, m.getWidth(text));
        }, combo);
        if (el) {
            width += el.getBorderWidth('lr');
            width += el.getPadding('lr');
        }
        if (combo.trigger) {
            width += combo.trigger.getWidth();
        }
        s.width = width + 10;
        var navPanel = Ext.getCmp('alertsNavPanel');
        if (combo.getWidth() < s.width) {
            combo.setSize(s);
            if (s.width <= comboboxMaxWidth) {
                navPanel.setWidth(s.width + 50);
            }
        }
        else
            if (combo.getWidth() > navPanel.getWidth()) {
                combo.setWidth(navPanel.getWidth() - 20);
            }
    }

    return{
        selectAlertDialog: function () { },
        getSelectedAlerts: function() {
            var jsonResult = [];
            $.each(selectedAlertsIds, function () {
                jsonResult.push({ "AlertID": this[0], "Name": this[1] });
            });
            return jsonResult;
        },
        setSelectedAlerts: function (alertsArray) {
            SW.Orion.AlertPicker.setSelectedAlertsIds(alertsArray, null, null);
        },
        setSelectedAlertsIds: function (alertsArray, controlUpdateFunction) {
            var containerId = "#selectedObjects";
            var result = [];
            $.each(alertsArray, function () {
                result.push([this.AlertID, this.Name]);
            });
            selectedAlertsIds = result;
            if (!controlUpdateFunction) {
                $(containerId).empty();
                $.each(selectedAlertsIds, function () {
                    $(containerId).append("<span class='selectedGridItem' dataRow=" + this[0] + ">" + this[1] + "<img class='removeObjectCross' src='/Orion/images/Reports/delete_netobject.png' style='cursor: pointer;margin-bottom: -3px; margin-left: 5px;'></span>");
                    $(containerId + " span[dataRow=" + this[0] + "] img").one('click', function () {
                        var index = $(this).parent().attr("dataRow");
                        removeAlertFromSelection(index, containerId);
                        refreshSelection();
                    });
                });
                refreshSelection();
            } else {
                controlUpdateFunction(alertsArray);
            }
        },
        clearSelection: function () {
        },
        init: function () {
            filterText = "";
            userPageSize = 16;
            alertsGroupingValue = ['ObjectType', 'null', '0'];

            alertsSelectorModel = new Ext.grid.CheckboxSelectionModel({ checkOnly: true, header: '<div id="checker" class="x-grid3-hd-checker">&nbsp;</div>' });

            var alertsGridStoreColumns = [{ name: 'AlertID', mapping: 0 },
                { name: 'Name', mapping: 1 },
                { name: 'Description', mapping: 2 }
            ];

            var alertsGridColumnsModel = [alertsSelectorModel,
                   { header: 'AlertID', width: 80, hidden: true, hideable: false, sortable: false, dataIndex: 'AlertID' },
                   { header: '@{R=Core.Strings;K=WEBJS_VB0_22; E=js}', width: 300, sortable: true, hideable: false, dataIndex: 'Name', renderer: renderString },
                   { header: '@{R=Core.Strings;K=XMLDATA_SO0_28;E=js}', id: 'Description', width: 250, sortable: true, dataIndex: 'Description', renderer: renderString }
            ];

            dataStore = new ORION.WebServiceStore("/Orion/Services/NewAlertingService.asmx/GetAlerts", alertsGridStoreColumns);
            dataStore.sortInfo = { field: "Name", direction: "Asc" };

            var alertsGroupingStore = new ORION.WebServiceStore(
              "/Orion/Services/NewAlertingService.asmx/GetAlertGroupValues",
              [
                  { name: 'Value', mapping: 0 },
                  { name: 'DisplayNamePlural', mapping: 1 },
                  { name: 'Cnt', mapping: 2 }
              ],
              "Value", "");

            pageSizeBox = new Ext.form.NumberField({
                id: 'alertsPageSizeField',
                enableKeyEvents: true,
                allowNegative: false,
                width: 40,
                allowBlank: false,
                minValue: 1,
                maxValue: 100,
                value: userPageSize,
                listeners: {
                    scope: this,
                    'keydown': function (f, e) {
                        var k = e.getKey();
                        if (k == e.RETURN) {
                            e.stopEvent();
                            var v = parseInt(f.getValue());
                            if (!isNaN(v) && v > 0 && v <= 100) {
                                userPageSize = v;
                                alertsPagingToolbar.pageSize = userPageSize;
                                alertsPagingToolbar.doLoad(0);
                            } else {
                                Ext.Msg.show({
                                    title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                    msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                    icon: Ext.Msg.ERROR,
                                    buttons: Ext.Msg.OK
                                });
                                return;
                            }
                        }
                    },
                    'focus': function (field) {
                        field.el.dom.select();
                    },
                    'change': function (f, numbox, o) {
                        var pSize = parseInt(f.getValue());
                        if (isNaN(pSize) || pSize < 1 || pSize > 100) {
                            Ext.Msg.show({
                                title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                icon: Ext.Msg.ERROR,
                                buttons: Ext.Msg.OK
                            });
                            return;
                        }

                        if (alertsPagingToolbar.pageSize != pSize) { // update page size only if it is different
                            alertsPagingToolbar.pageSize = pSize;
                            userPageSize = pSize;
                            alertsPagingToolbar.doLoad(0);
                        }
                    }
                }
            });

            var alertsPagingToolbar = new Ext.PagingToolbar(
                {
                    id: 'alertsGridPaging',
                    store: dataStore,
                    pageSize: userPageSize,
                    displayInfo: true,
                    displayMsg: "@{R=Core.Strings;K=WEBJS_TM0_5; E=js}",
                    emptyMsg: "@{R=Core.Strings;K=WEBJS_SO0_54; E=js}",
                    beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                    afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                    firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                    prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                    nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                    lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                    refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}",
                    items: [
                        '-',
                        new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_VB0_46; E=js}', style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;' }),
                        pageSizeBox
                    ]
                });

            var alertGroupingDataStore = new ORION.WebServiceStore(
                "/Orion/Services/NewAlertingService.asmx/GetObjectGroupProperties",
                [
                    { name: 'Value', mapping: 0 },
                    { name: 'Name', mapping: 1 },
                    { name: 'Type', mapping: 2 }
                ]);

             alertsComboArray = new Ext.form.ComboBox(
                {
                    id: 'alertsGroupCombo',
                    mode: 'local',
                    boxMaxWidth: comboboxMaxWidth,
                    fieldLabel: 'Name',
                    displayField: 'Name',
                    valueField: 'Value',
                    store: alertGroupingDataStore,
                    triggerAction: 'all',
                    value: '@{R=Core.Strings;K=WEBJS_VB0_76;E=js}',
                    typeAhead: true,
                    forceSelection: true,
                    selectOnFocus: false,
                    multiSelect: false,
                    editable: false,
                    listeners: {
                        'select': function () { 
                            var val = this.store.data.items[this.selectedIndex].data.Value;
                            var type = this.store.data.items[this.selectedIndex].data.Type;

                            refreshGridObjects(null, null, alertsGroupGrid, val, type, "", filterText, function () {
                                var alertsGroupGridValue = "";
									alertsGroupGridValue = alertsComboArray.getValue();
									
									var type = "";
									$.each(alertsComboArray.store.data.items, function (index, item) {
										if (item.data.Value == alertsGroupGridValue) {
											type = item.data.Type;
											return false;
										}
									});
							
                                refreshGridObjects(0, userPageSize, alertsGrid, alertsGroupGridValue, type, "", filterText, function () {
                                    $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function () {
                                        this.tooltip = 'processed';
                                        $.swtooltip(this);
                                    });
                                });
                            });
                        }
                    }
                });

            alertsComboArray.getStore().on('load', function () {
                alertsComboArray.setValue(alertsGroupingValue[0]);
                refreshGridObjects(null, null, alertsGroupGrid, alertsGroupingValue[0], alertsGroupingValue[0], "", "");
                resizeToFitContent(alertsComboArray);
            });

            alertsGrid = new Ext.grid.GridPanel({
                id: 'alertGrid',
                region: 'center',
                height: 500,
                viewConfig: { forceFit: false, emptyText: "@{R=Core.Strings;K=WEBJS_SO0_54;E=js}" },
                store: dataStore,
                baseParams: { start: 0, limit: userPageSize },
                stripeRows: true,
                trackMouseOver: false,
                columns: alertsGridColumnsModel,
                sm: alertsSelectorModel,
                autoScroll: true,
                autoExpandColumn: 'Description',
                loadMask: true,
                listeners: {
                    cellmousedown: function (grid, rowIndex, columnIndex, e) {
                        var fieldName = grid.getColumnModel().getDataIndex(columnIndex);
                        if (fieldName == "") {
                            var alertName = grid.getStore().getAt(rowIndex).data.Name;
                            var alertId = grid.getStore().getAt(rowIndex).data.AlertID;
                            if ($.grep(selectedAlertsIds, function (n) { return n[0] == alertId; }).length == 0) {
                                addAlertToSelection(alertId, alertName);
                            } else {
                                removeAlertFromSelection(alertId);
                            }
                            refreshSelection();
                            checkAllOrNoneSelection();
                        }
                        return false;
                    }
                },
                bbar: alertsPagingToolbar,
                cls: 'panel-with-border'
            });

            alertsGrid.getColumnModel().on('hiddenchange', function () {
                var cols = '';
                for (var i = 1; i < alertsGrid.getColumnModel().getColumnCount() ; i++) {
                    if (!alertsGrid.getColumnModel().isHidden(i)) {
                        cols += alertsGrid.getColumnModel().getDataIndex(i) + ',';
                    }
                }
            });

            alertsGrid.store.on('beforeload', function (store, options) {
                alertsGrid.getEl().mask('@{R=Core.Strings;K=WEBJS_VB0_1; E=js}', 'x-mask-loading');
                options.params.limit = userPageSize;
            });

            alertsGrid.store.on('load', function () {
                alertsGrid.getEl().unmask();
                refreshSelection();
                $("#checker").click(function () {
                    if (alertsGrid.getSelectionModel().selections.items.length == 0) {
                        $.each(alertsGrid.store.data.items, function () {
                            var item = this;
                            if ($.grep(selectedAlertsIds, function (n) { return n[0] == item.data.AlertID; }).length != 0) {
                                removeAlertFromSelection(item.data.AlertID);
                            }
                        });
                    } else {
                        $.each(alertsGrid.getSelectionModel().selections.items, function () {
                            var item = this;
                            if ($.grep(selectedAlertsIds, function (n) { return n[0] == item.data.AlertID; }).length == 0) {
                                addAlertToSelection(item.data.AlertID, item.data.Name);
                            }
                        });
                    }
                    refreshSelection();
                });
                setTimeout(function () {
                    refreshSelection();
                }, 0);
                checkAllOrNoneSelection();
            });

            alertsGrid.store.on("exception", function (dataProxy, type, action, options, response, arg) {
                var error = eval("(" + response.responseText + ")");
                Ext.Msg.show({
                    title: '@{R=Core.Strings;K=WEBJS_VB0_133; E=js}',
                    msg: error.Message,
                    icon: Ext.Msg.ERROR,
                    buttons: Ext.Msg.OK
                });
                alertsGrid.getEl().unmask();
            });


            alertsGroupGrid = new Ext.grid.GridPanel({
                id: 'alertsGroupGrid',
                height: 450,
                store: alertsGroupingStore,
                cls: 'hide-header',
                columns: [
                    { id: 'Value', width: 193, editable: false, sortable: false, dataIndex: 'DisplayNamePlural', renderer: renderGroup }
                ],
                selModel: new Ext.grid.RowSelectionModel(),
                autoScroll: true,
                loadMask: true,
                listeners: {
                    cellclick: function (mygrid, row, cell, e) {
                        var val = mygrid.store.data.items[row].data[mygrid.store.data.items[row].fields.keys[cell]];
                        var type = "";
                        $.each(alertsComboArray.store.data.items, function (index, item) {
                            if (item.data.Value == alertsComboArray.getValue()) {
                                type = item.data.Type;
                                return false;
                            }
                        });
                        if (val == '') {
                            val = '{empty}';
                        }

                        if (val == null) {
                            val = "null";
                        }

                        if (val == '[All]')
                            val = '';
						
                        refreshGridObjects(0, userPageSize, alertsGrid, alertsComboArray.getValue(), type, val, filterText, function () {
                            $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function () {
                                this.tooltip = 'processed';
                                $.swtooltip(this);
                            });
                        });
                    }
                },
                anchor: '0 0',
                viewConfig: { forceFit: true },
                split: true,
                autoExpandColumn: 'Value'
            });

            alertsGroupGrid.store.on('load', function () {
				var rowNum = alertsGroupingValue[2];
				 $.each(alertsGroupGrid.store.data.items, function (index, item) {
                            if (item.data.Value == '[All]') {
                                rowNum = index;
                                return false;
                            }
                        });
                alertsGroupGrid.getSelectionModel().selectRow(rowNum, false);
            });

            var alertsGroupByTopPanel = new Ext.Panel({
                id: 'alertsGroupByTopPanel',
                region: 'center',
                split: false,
                heigth: 50,
                collapsible: false,
                viewConfig: { forceFit: true },
                items: [new Ext.form.Label({ text: "@{R=Core.Strings;K=WEBJS_AK0_76; E=js}" }), alertsComboArray],
                cls: 'panel-no-border panel-bg-gradient'
            });

            var alertsNavPanel = new Ext.Panel({
                id: 'alertsNavPanel',
                width: 205,
                height: 400,
                region: 'west',
                split: false,
                anchor: '0 0',
                collapsible: false,
                viewConfig: { forceFit: true },
                items: [alertsGroupByTopPanel, alertsGroupGrid],
                cls: 'panel-with-border'
            });

            alertsNavPanel.on("bodyresize", function () {
                alertsGroupGrid.setWidth(alertsNavPanel.getSize().width);
            });

            var alertsMainGridPanel = new Ext.Panel({
                id: 'alertsMainGridPanel',
                region: 'center',
                height: 500,
                split: false,
                layout: 'border',
                collapsible: false,
                items: [alertsNavPanel, alertsGrid],
                cls: 'panel-no-border'
            });

            alertsMainGridPanel.render('actionAlertsPicker');

            var initialSearchText = "@{R=Core.Strings.2;K=WEBJS_AY0_14; E=js}";

            var searchField = new Ext.Panel({
                id: 'searchFieldPanel',
                region: 'center',
                split: false,
                heigth: 50,
                collapsible: false,
                layout: 'table',
                layoutConfig: {
                    columns: 3
                },
                items: [new Ext.form.Label({ text: "@{R=Core.Strings.2;K=WEBJS_AY0_13; E=js}", width: 694, cls: "availableReportsLabel" }),
                    new Ext.ux.form.SearchField({
                        id: 'alertsSearchField',
                        guid: 'assign-alerts',
                        emptyText: initialSearchText,
                        store: dataStore,
                        width: 280
                    })
                ],
                cls: 'panel-no-border'
            });
            searchField.render('searchPanel');
            refreshGridObjects(null, null, alertsComboArray, "", "", "", "");
            refreshGridObjects(0, userPageSize, alertsGrid, alertsGroupingValue[0], "", "", filterText);

        }
    }

}();

