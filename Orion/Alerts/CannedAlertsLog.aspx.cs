﻿using System;

public partial class Orion_Alerts_CannedAlertsLog : System.Web.UI.Page
{
    public string ImportId { get; set; }

    protected override void OnInit(EventArgs e)
    {
        ImportId = Request["ImportId"];
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}