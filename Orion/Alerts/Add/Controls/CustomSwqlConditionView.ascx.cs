﻿using System;
using System.Linq;
using Newtonsoft.Json;
using SolarWinds.Logging;
using System.Collections.Generic;
using SolarWinds.Orion.Core.Alerting.Plugins.Conditions.Models;
using SolarWinds.Orion.Core.Alerting.Plugins.Conditions.Swql;
using SolarWinds.Orion.Core.Models.Alerting;
using SolarWinds.Orion.Core.Alerting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Alerts.TriggerCondition;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;

namespace Orion.Alerts.Add.Controls
{
    public partial class CustomSwqlConditionView : TriggerConditionPluginViewBase
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            IEnumerable<Entity> customSwqlItems = LoadEntities();
            SetQueryFragement(customSwqlItems);

            swqlEntityTypesId.DataSource = customSwqlItems;
            swqlEntityTypesId.DataTextField = "DisplayName";
            swqlEntityTypesId.DataValueField = "ObjectType";
            swqlEntityTypesId.DataBind();

            var swqlConfig = ChainItemConfig as CustomSwqlChainItemConfig;
            if (swqlConfig != null)
            {
                userSwqlQueryText.Value = swqlConfig.Command;
            }
        }

        private IEnumerable<Entity> LoadEntities()
        {
            var conditionTypeProvider = ConditionTypeProvider.Create(AppDomainCatalog.Instance, AlertConditionHelper.GetConditionTypeServices());
            var customSwqlType = (ConditionTypeCustomSwql)conditionTypeProvider.Items.First(n => n.RefID == ConditionTypeCustomSwql.ID);

            IEnumerable<Entity> customSwqlItems = customSwqlType.EntityProvider.Items.OrderBy(entity => entity.DisplayName);
            return customSwqlItems;
        }

        private void SetQueryFragement(IEnumerable<Entity> items)
        {
            var customData = new Dictionary<string, string>();
            foreach (var entityQueryable in items.OfType<EntityQueryable>())
            {
                customData[entityQueryable.ObjectType] = entityQueryable.GetSelectQueryFragementForSwql();
            }

            swqlQueryFragement.Value = JsonConvert.SerializeObject(customData);
        }
    }
}
