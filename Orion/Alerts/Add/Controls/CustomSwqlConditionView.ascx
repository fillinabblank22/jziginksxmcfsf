﻿<%@ Control Language="C#" AutoEventWireup="false" CodeFile="CustomSwqlConditionView.ascx.cs" Inherits="Orion.Alerts.Add.Controls.CustomSwqlConditionView" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Alerts.TriggerCondition" %>

<orion:Include ID="Include2" File="Alerts/Add/typescripts/ConditionPluginControllerBase.js" runat="server" />
<orion:Include ID="Include1" File="Alerts/Add/typescripts/CustomSwqlConditionController.js" runat="server" />

<input type="hidden" id="swqlQueryFragement" runat="server" />

<div>
    <table>
        <tr>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_165) %><br/>
                <asp:DropDownList runat="server" ID="swqlEntityTypesId" style="margin-left: 0;"/>
            </td>
        </tr>
        <tr>
            <td>
                <textarea disabled="disabled" runat="server" id="customSwqlQueryText" rows="3" style="color: black; border:1px solid #ABADB3; width:700px;"></textarea><br/>
                <textarea runat="server" id="userSwqlQueryText" rows="12" style="border:1px solid #ABADB3; width:700px;"></textarea>
            </td>
        </tr>
        <tr>
            <td>
                <orion:LocalizableButton runat="server" ID="btnValidateSWQL" DisplayType="Small" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_139 %>"></orion:LocalizableButton><br/>
                <div class="sw-suggestion sw-suggestion-fail" id="msgFail" runat="server" style="display: none; margin-left: 0;"><span class="sw-suggestion-icon"></span><span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_202) %></span></div>
                <div class="sw-suggestion sw-suggestion-pass" id="msgPass" runat="server" style="display: none; margin-left: 0;"><span class="sw-suggestion-icon"></span><span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_203) %></span></div>
            </td>
        </tr>
    </table>
</div>
<script type="text/javascript">
    (function () {
        var config = new SW.Core.Alerts.Add.CustomSwqlConditionConfig();
        config.swqlEntityTypesId = '<%= swqlEntityTypesId.ClientID %>';
        config.swqlFragmentId = '<%= swqlQueryFragement.ClientID %>';
        config.swqlQueryTextId = '<%= customSwqlQueryText.ClientID %>';
        config.userSwqlQueryTextId = '<%= userSwqlQueryText.ClientID %>';
        config.chainItemConfigType = '<%= GetChainItemConfigType(typeof (CustomSwqlChainItemConfig)) %>';
        config.chainItemConfigInputId = '<%= JsonInputChainItemConfigId %>';
        config.conditionControllerId = '<%= ConditionControllerId %>';
        config.validateButton = {
            ButtonId: '<%=btnValidateSWQL.ClientID %>',
            MsgPassID: '<%=msgPass.ClientID %>',
            MsgFailID: '<%=msgFail.ClientID %>'
        };

        var controller = new SW.Core.Alerts.Add.CustomSwqlConditionController(config);
        controller.init();
    })();
</script>
