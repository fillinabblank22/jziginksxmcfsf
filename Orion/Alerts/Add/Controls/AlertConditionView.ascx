﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlertConditionView.ascx.cs" Inherits="Orion.Alerts.Add.Controls.AlertConditionView" %>

<orion:Include ID="Include2" runat="server" File="RenderControl.js" />
<orion:Include ID="Include4" runat="server" File="Alerts/Add/typescripts/ConditionPluginControllerBase.js" />
<orion:Include ID="Include1" runat="server" File="Alerts/Add/typescripts/AlertConditionController.js" />
<orion:Include ID="Include3" runat="server" File="Alerts/Styles/AlertConditionBuilder.css" />

<div id="conditionViewContainer" runat="server">
    <orion:JsonInput runat="server" ID="jsonInputChainItemConfig" />
    <orion:JsonInput runat="server" ID="jsonInputAlertTypes" />

    <b><%--I want to alert on:--%><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_24) %></b><br/>
    <div id="alertTypesComboBox" runat="server"></div><br />

    <div id="conditionContent" runat="server" style="padding-top: 1em;"></div>
    <div class="acv-additionalOptions">
        <div>
            <input type="checkbox" id="chkConditionPeriod" runat="server" data-form="chkConditionPeriod" />
            <span>
                <%-- Condition must exist for more than --%>
                <span data-form="conditionPeriodSpan"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_37) %></span>&nbsp;
                <input type='text' data-form='periodTimeValue' data-val-message="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_27) %>" MaxLength='4' style='width: 32px' disabled/>
                <select data-form='periodTimeKind' disabled>
                    <option value='1'><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_46) %></option>
                    <option value='60' selected><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_48) %></option>
                    <option value='3600'><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_70) %></option>
                </select>
            </span>
        </div>
        <div id="advancedOptionsContainer" runat="server" class="hidden" style="margin-left:0px;">
            <input type="checkbox" id="chkWaitUntil" runat="server" data-form="ChkWaitUntil" />
            <span>
                <%--Alert can be triggered if {0} (at the same time) objects have met the specified condition--%>
                <%=string.Format(DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_25).ToString(), 
                string.Format("<select data-form='netObjectsMinCountThresholdType'><option value='0'>{0}</option><option value='1'>{1}</option></select> <input type='text' data-form='NetObjectsMinCountThreshold' data-val-message='{2}' MaxLength='6'style='width: 32px' disabled/>", 
                 DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_68), DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_69), DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_28)) + " <span data-form='netObjectMinCountThresholdAdditionalText'></span>") %><%--Value must greater than 0--%>
            </span>
            <img src="/Orion/images/Info_Icon_MoreDecent_16x16.png" alt=""/>
        </div>
    </div>
    
    <script type="text/javascript">        
        (function () {
            var config = new SW.Core.Alerts.Add.AlertConditionConfig();
            config.conditionContentId = '<%= conditionContent.ClientID %>';
            config.conditionViewContainerId = '<%= conditionViewContainer.ClientID %>';
            config.advancedOptionsContainerId = '<%= advancedOptionsContainer.ClientID %>';
            config.chainItemConfigInputId = '<%= jsonInputChainItemConfig.ClientID %>';
            config.clientIDPrefix = '<%= ClientIDPrefix %>';
            config.alertTypesComboBoxId = '<%= alertTypesComboBox.ClientID %>';
            config.alertTypesComboBoxInitValue = '<%= AlertTypeOption %>';
            config.jsonInputAlertTypesId = '<%= jsonInputAlertTypes.ClientID %>';

            var triggerCondition = new SW.Core.Alerts.Add.AlertConditionController(config);
            triggerCondition.init();
        })();
    </script>
</div>
