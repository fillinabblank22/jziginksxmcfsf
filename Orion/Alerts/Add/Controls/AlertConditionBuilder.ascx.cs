﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Web.UI.HtmlControls;
using Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Alerting;
using SolarWinds.Orion.Core.Alerting.Models;
using SolarWinds.Internationalization.Extensions;
using SolarWinds.Orion.Core.Alerting.Plugins.Conditions.Dynamic;
using SolarWinds.Orion.Core.Models.Alerting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Alerts.TriggerCondition;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;

namespace Orion.Alerts.Add.Controls
{
    public partial class AlertConditionBuilder : System.Web.UI.UserControl
    {
        private static volatile Dictionary<string, Type> _alertConfigTypes = null;
        private static volatile object _alertConfigTypesLocker = new object();
        private static readonly Log _log = new Log();
        private ConditionChain _conditionChain;

        public static readonly string ImportEventArgument = "AlertConditionBuilder.ImportCondition";

        private static Dictionary<string, Type> AlertConfigTypes
        {
            get
            {
                if (_alertConfigTypes == null)
                {
                    lock (_alertConfigTypesLocker)
                    {
                        if (_alertConfigTypes == null)
                        {
                            _alertConfigTypes = new Dictionary<string, Type>(StringComparer.OrdinalIgnoreCase);
                            
                            try
                            {
                                // load ChainItemConfig successors via MEF and add them to dictionary
                                using (var container = new CompositionContainer(AppDomainCatalog.Instance))
                                {
                                    foreach (var item in container.GetExportedValues<ChainItemConfig>())
                                    {
                                        _log.InfoFormat("Loading ChainItemConfig type {0}", item.GetType());
                                        _alertConfigTypes[item.SupportedConditionTypeRefID] = item.GetType();
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                _log.Error("Error while loading ChainItemConfig successors.", ex);
                            }
                        }
                    }
                }
                return _alertConfigTypes;
            }
        }

        public IEnumerable<string> SaveButtonClientIDs { get; set; }

        public string SaveButtonClientIDsJSArray
        {
            get
            {
                string arrayContent = string.Empty;

                if (SaveButtonClientIDs != null && SaveButtonClientIDs.Any())
                {
                    arrayContent = string.Join("','", SaveButtonClientIDs);
                    arrayContent = string.Format("'{0}'", arrayContent);
                }

                return string.Format("[{0}]", arrayContent);
            }
        }

        public ConditionChain ConditionChain
        {
            get
            {
                var configArray = jsonInputConditions.GetObjectWithTypes<ChainItemConfig[]>();
                var conditionTypeProvider = ConditionTypeProvider.Create(AppDomainCatalog.Instance, AlertConditionHelper.GetConditionTypeServices());

                _conditionChain = ChainItemConfig.CreateConditionChain(configArray, conditionTypeProvider);

                return _conditionChain;
            }
            set
            {
                if (value == null) throw new ArgumentNullException("value");

                _conditionChain = value;

                var configList = new List<ChainItemConfig>(_conditionChain.Conditions.Count);
                foreach (ConditionChainItem conditionChainItem in _conditionChain.Conditions)
                {
                    Type configType;
                    if (AlertConfigTypes.TryGetValue(conditionChainItem.Type.RefID, out configType))
                    {
                        var itemConfig = (ChainItemConfig)Activator.CreateInstance(configType);
                        itemConfig.InitFromConditionChainItem(conditionChainItem);

                        configList.Add(itemConfig);
                    }
                    else
                    {
                        _log.ErrorFormat("Chain item config for '{0}' not found.", conditionChainItem.Type.RefID);
                    }
                }
                jsonInputConditions.SetObjectWithTypes(configList.ToArray());
                SetUserControls();
            }
        }

        public bool DisableInit { get; set; }

        protected bool ExpandAdvancedOptions { get; private set; }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            var localizedConditions = Enum.GetValues(typeof (ConditionConjunctionOperator))
                .Cast<ConditionConjunctionOperator>()
                .Where(c => c != ConditionConjunctionOperator.None)
                .Select(c => new {Value = (int)c, Text = c.LocalizedLabel()});

            conditionOperator.DataValueField = "Value";
            conditionOperator.DataTextField = "Text";
            conditionOperator.DataSource = localizedConditions;
            conditionOperator.DataBind();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (IsPostBack)
            {
                validationResultsContainer.Style["display"] = "none";  // to not show error message container

                // import button clicked
                if (string.Equals(Request.Params["__EVENTTARGET"], UniqueID, StringComparison.OrdinalIgnoreCase)
                    && string.Equals(Request.Params["__EVENTARGUMENT"], ImportEventArgument))
                {
                    ImportCondition(fileUpload);
                }
            }
        }

        public void ImportCondition(HtmlInputFile fileUp)
        {
            string errorMessage = string.Empty;

            try
            {
                using (Stream stream = fileUp.PostedFile.InputStream)
                using (var reader = new StreamReader(stream, Encoding.UTF8))
                {
                    string xmlContent = reader.ReadToEnd();
                    var shelves = AlertConditionShelve.FromXmlStringEnumerable(xmlContent);

                    var conditionTypeProvider = ConditionTypeProvider.Create(AppDomainCatalog.Instance, InformationServiceProxy.CreateV3Creator());
                    ConditionChain = WebSecurityHelper.EncodeExpressionTree(shelves.RestoreConditionChain(conditionTypeProvider));
                }
            }
            catch (SerializationException sex)
            {
                // "File \"{0}\" cannot be imported. Only *.xml files in correct format are allowed."
                errorMessage = string.Format(CoreWebContent.WEBCODE_PF0_15, fileUp.PostedFile.FileName);
                _log.Error("Error when deserializing file.", sex);
            }
            catch (Exception ex)
            {
                errorMessage = string.Format(CoreWebContent.WEBCODE_PF0_14, fileUp.PostedFile.FileName); // "Unexpected error while importing file \"{0}\"."
                _log.Error("Error while importing file.", ex);
            }

            if (!string.IsNullOrEmpty(errorMessage))
            {
                ConditionTypeProvider conditionTypeProvider = ConditionTypeProvider.Create(AppDomainCatalog.Instance, InformationServiceProxy.CreateV3Creator());
                IConditionType dynamicType = conditionTypeProvider.Get(ConditionTypeDynamic.ID);

                var defaultChain = new ConditionChain();
                defaultChain.Conditions.Add(new ConditionChainItem
                {
                    Condition = new AlertConditionDynamic(),
                    Type = dynamicType,
                    ObjectType = "Node"
                });

                this.ConditionChain = defaultChain;
                validationResultsContainer.Style.Remove("display");
                resultMessage.InnerText = errorMessage;
            }
        }

        private void SetUserControls()
        {
            bool ninjaChecked = _conditionChain.Conditions.Count == 1 &&
                                _conditionChain.Conditions[0].NetObjectsMinCountThreshold.HasValue ||
                                _conditionChain.Conditions.Count > 1;
            
            chkNinjaUser.Checked = ninjaChecked;
            ExpandAdvancedOptions = ninjaChecked;
        }
    }
}