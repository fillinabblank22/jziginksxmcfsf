﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DynamicConditionView.ascx.cs" Inherits="Orion_Controls_DynamicConditionView" %>
<%@ Register TagPrefix="orion" TagName="ExprBuilder" Src="~/Orion/Controls/ExpressionBuilder.ascx" %>
<orion:Include runat="server" File="Alerts/Add/styles/DynamicConditionView.css" />
<orion:Include ID="Include2" File="Alerts/Add/typescripts/ConditionPluginControllerBase.js" runat="server" />
<orion:Include ID="Include1" File="Alerts/Add/typescripts/DynamicConditionController.js" runat="server" />
<%@ Import Namespace="SolarWinds.Orion.Web.Alerts.TriggerCondition" %>

<div class="dyn-con-slide-no-top"></div>

<span class="scope-name" style="margin-top:5px"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_65) %></span> <%--scope of alert--%>
<img src="/Orion/images/Info_Icon_MoreDecent_16x16.png" alt="" style="vertical-align: text-top;margin-left:10px" title="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_66) %>">
<div class="dyn-con-scope" style="margin-top:5px">
    <input type="radio" id='<%= DefaultSanitizer.SanitizeHtml(this.ClientIDPrefix) %>rbAlertOnly' name='<%= DefaultSanitizer.SanitizeHtml(this.ClientIDPrefix) %>scope' value="0" <%= this.IsScopeLoaded() ? String.Empty : "checked='checked'" %> /> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_60) %>&nbsp;<a id='<%= DefaultSanitizer.SanitizeHtml(this.ClientIDPrefix) %>linkAlertOnlyShowList'  <%= !this.IsScopeLoaded() ? "style='cursor: pointer;'" : "style='display: none; cursor: pointer;'" %> class="sw-link"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCO_001) %></a><br /> <!-- On all objects in my environment -->
    <input type="radio" id='<%= DefaultSanitizer.SanitizeHtml(this.ClientIDPrefix) %>rbScope' name='<%= DefaultSanitizer.SanitizeHtml(this.ClientIDPrefix) %>scope' value="1" <%= !this.IsScopeLoaded() ? String.Empty : "checked='checked'" %> /> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_61) %>&nbsp;<a id='<%= DefaultSanitizer.SanitizeHtml(this.ClientIDPrefix) %>linkScopeShowList'  <%= this.IsScopeLoaded() ? "style='cursor: pointer;'" : "style='display: none; cursor: pointer;'" %> class="sw-link"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCO_001) %></a> <!-- Only following set of objects -->
</div>

<div id='<%= DefaultSanitizer.SanitizeHtml(this.ClientIDPrefix) %>scopeBuilder'>
<orion:ExprBuilder ID="exprBuilderForScope" ShowOnlyNonTransientFields="true" ClientInstanceName="PARENTexprBuilderForScope" runat="server" RootNodeTitle="<%$ HtmlEncodedResources: CoreWebContent, WEBJS_TK0_2 %>" ConditionTitle="" IsMaster="true" /> <!-- All objects where -->
</div>
<div class="dyn-con-slide"></div>

<orion:ExprBuilder ID="exprBuilderForAlert" ClientInstanceName="PARENTexprBuilderForAlert" runat="server" RootNodeTitle="<%$ HtmlEncodedResources: CoreWebContent, WEBJS_TK0_3 %>" ConditionTitle="<%$ HtmlEncodedResources: CoreWebContent, WEBJS_TK0_4 %>" IsMaster="true" /> <!-- Trigger alert when  |  The actual trigger condition -->

<%-- Validation results placeholder --%>
<div id="validationResultsContainer" data-container="validationResultsContainer" class="sw-suggestion sw-suggestion-fail" 
    style="margin-top: 1em; margin-bottom: 3em; margin-left: 0px; display: none;" runat="server">
    <span class="sw-suggestion-icon"></span>
    <span id="resultMessage" data-message="resultMessage" runat="server"></span>
</div>

<div class="dyn-con-slide-no-top"></div>

<%-- Sliding window --%>
<div data-form='slidingWindowDiv' style="display:none">
    <span>
        <%--Condition must happen in a sliding window of --%>
        <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_26) %></span>&nbsp;
        <input type='text' data-form='timeValue' data-val-message="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_27) %>"
            data-val-maxvalue="3600" MaxLength='4' style='width: 32px' disabled/>
        <select data-form='timeKind' disabled>
            <option value='1'><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_46) %></option>
            <option value='60' selected><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_48) %></option>
        </select>
    </span>
    <img src="/Orion/images/Info_Icon_MoreDecent_16x16.png" alt="" style="vertical-align: text-top;" title="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_38) %>"/>
</div>

<asp:Panel runat="server" ID="ObjectsListDialog" style="display:none; background-color: white;">
        <table style="width: 100%">
            <tr>
                <td>
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCO_002) %><asp:Label runat="server" ID="ObjectsCountText"></asp:Label>
                </td>
                <td>
                    <%--<asp:TextBox runat="server" ID="SearchField"></asp:TextBox>--%>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel runat="server" ID="ObjectsGrid"></asp:Panel>
                </td>
            </tr>
        </table>

		<div class="sw-btn-bar-wizard">
			<span class="closeButtonPlaceHolder"></span>
		</div>
</asp:Panel>

<asp:Panel runat="server" ID="ShowSWQLDialog" style="display:none; background-color: white;">
	<div>
        <textarea rows="20" cols="54" id="showSWQLArea" runat="server"></textarea>

		<div class="sw-btn-bar-wizard">
			<span class="closeButtonPlaceHolder"></span>
		</div>
	</div>
</asp:Panel>

<script type="text/javascript">
    (function () {
        $("#<%= DefaultSanitizer.SanitizeHtml(this.ClientIDPrefix) %>rbScope, #<%= DefaultSanitizer.SanitizeHtml(this.ClientIDPrefix) %>rbAlertOnly").change(function () {
            if ($("#<%= DefaultSanitizer.SanitizeHtml(this.ClientIDPrefix) %>rbScope").attr("checked")) {
                $('#<%= DefaultSanitizer.SanitizeHtml(this.ClientIDPrefix) %>scopeBuilder').show();
                $('#<%= DefaultSanitizer.SanitizeHtml(this.ClientIDPrefix) %>linkAlertOnlyShowList').hide();
                $('#<%= DefaultSanitizer.SanitizeHtml(this.ClientIDPrefix) %>linkScopeShowList').show();
            }
            else {
                $('#<%= DefaultSanitizer.SanitizeHtml(this.ClientIDPrefix) %>scopeBuilder').hide();
                $('#<%= DefaultSanitizer.SanitizeHtml(this.ClientIDPrefix) %>linkAlertOnlyShowList').show();
                $('#<%= DefaultSanitizer.SanitizeHtml(this.ClientIDPrefix) %>linkScopeShowList').hide();
            }
        });

        <%  if(!this.IsScopeLoaded())  { %>
        $('#<%= DefaultSanitizer.SanitizeHtml(this.ClientIDPrefix) %>scopeBuilder').hide();
        <% } %>

        $("#<%= DefaultSanitizer.SanitizeHtml(this.ClientIDPrefix) %>linkAlertOnlyShowList, #<%= DefaultSanitizer.SanitizeHtml(this.ClientIDPrefix) %>linkScopeShowList").click(function () {
            controller.showObjectsList();
        });

        var config = new SW.Core.Alerts.Add.DynamicConditionConfig();
        config.BuilderInstanceForAlert = '<%= exprBuilderForAlert.ClientInstanceName %>';
        config.BuilderInstanceForScope = '<%= exprBuilderForScope.ClientInstanceName %>';
        config.conditionControllerId = '<%= this.ConditionControllerId %>';
        config.chainItemConfigType = '<%=GetChainItemConfigType(typeof (DynamicChainItemConfig))%>';
        config.chainItemConfigInputId = '<%= JsonInputChainItemConfigId %>';
        config.ObjectType = '<%= this.ChainItemConfig.ObjectType %>';
        config.ScopeRadioButtonId = '<%= DefaultSanitizer.SanitizeHtml(this.ClientIDPrefix) %>rbScope';
        config.DialogControlId = '<%= ObjectsListDialog.ClientID %>';
        config.GridControlId = '<%= ObjectsGrid.ClientID %>';
        config.ObjectsCountTextId = '<%= ObjectsCountText.ClientID %>';
        config.ShowSWQLDialogControlId = '<%= ShowSWQLDialog.ClientID %>';
        config.ShowSWQLDialogTextAreaId = '<%= showSWQLArea.ClientID %>';
        config.ClientIdPrefix = '<%=this.ClientIDPrefix%>';
    	config.ShowObjectsListDialogControlId = '<%= this.ObjectsListDialog.ClientID %>';
        var controller = new SW.Core.Alerts.Add.DynamicConditionController(config);
        controller.init();
    })();
</script>

