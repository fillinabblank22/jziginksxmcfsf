﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Alerting;
using SolarWinds.Orion.Core.Alerting.Plugins.Conditions.Dynamic;
using SolarWinds.Orion.Core.Models.Alerting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.Alerts.TriggerCondition;

namespace Orion.Alerts.Add.Controls
{
    public partial class AlertConditionView : TriggerConditionPluginViewBase
    {
        class AlertTypeItem
        {
            public string DisplayName { get; set; }
            public string ObjectType { get; set; }
            public string ViewPath { get; set; }
            public string TypeRefId { get; set; }
            public string EntityFullName { get; set; }
            public bool Recommended { get; set; }
        }

        protected string AlertTypeOption
        {
            get
            {
                if (ChainItemConfig == null)
                {
                    return "Node"; // select Node if exists
                }

                if (string.Equals(ChainItemConfig.ConditionTypeRefID, ConditionTypeDynamic.ID, StringComparison.OrdinalIgnoreCase))
                {
                    return ChainItemConfig.ObjectType;
                }
                else
                {
                    return  ChainItemConfig.ConditionTypeRefID;
                }  
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            LoadAlertConditionTypes();

            jsonInputChainItemConfig.Value = ChainItemConfigJson;
        }

        private void LoadAlertConditionTypes()
        {
            var conditionTypeProvider = ConditionTypeProvider.Create(AppDomainCatalog.Instance, AlertConditionHelper.GetConditionTypeServices());
            
            IConditionType dynamicCondition = conditionTypeProvider.Get(ConditionTypeDynamic.ID);
            var dynamicEntities = dynamicCondition.EntityProvider.Items
                .OrderBy(entity => entity.DisplayName)
                .Select(item => new AlertTypeItem 
                                    { 
                                        DisplayName = item.DisplayName,
                                        ObjectType = item.ObjectType,
                                        EntityFullName = item.FullName, 
                                        ViewPath = dynamicCondition.ViewPath, 
                                        TypeRefId = dynamicCondition.RefID,
                                        Recommended = item.Recommended
                                    })
                .ToList();

            var otherEntities = new List<AlertTypeItem>();
                
            // get other not dynamic types
            foreach (IConditionType conditionType in conditionTypeProvider.Items
                .Where(ct => !string.Equals(ct.RefID, ConditionTypeDynamic.ID, StringComparison.OrdinalIgnoreCase))
                .OrderBy(ct => ct.DisplayName))
            {
                otherEntities.Add(new AlertTypeItem
                                      {
                                          DisplayName = conditionType.DisplayName,
                                          ViewPath = conditionType.ViewPath, 
                                          TypeRefId = conditionType.RefID,
                                          ObjectType = conditionType.RefID,
                                          Recommended = false
                                      });
            }

            dynamicEntities.AddRange(otherEntities);

            jsonInputAlertTypes.SetObject(dynamicEntities);
        }

    } 
}