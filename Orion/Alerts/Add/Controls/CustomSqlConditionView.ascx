﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomSqlConditionView.ascx.cs" Inherits="Orion.Alerts.Add.Controls.CustomSqlConditionView" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Alerts.TriggerCondition" %>

<orion:Include ID="Include2" File="Alerts/Add/typescripts/ConditionPluginControllerBase.js" runat="server" />
<orion:Include ID="Include1" File="Alerts/Add/typescripts/CustomSqlConditionController.js" runat="server" />

<input type="hidden" id="sqlQueryFragement" runat="server" />

<div>
    <table>
        <tr>
            <td>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_164) %><br/>
                <asp:DropDownList runat="server" ID="sqlEntityTypesId" style="margin-left: 0;" />
            </td>
        </tr>
        <tr>
            <td>
                <textarea disabled="disabled" runat="server" id="customSqlQueryText" rows="3" style="color: black; border:1px solid #ABADB3; width:700px;"></textarea><br/>
                <textarea runat="server" id="userSqlQueryText" rows="12" style="border:1px solid #ABADB3; width:700px;"></textarea>
            </td>
        </tr>
        <tr>
            <td>
                <orion:LocalizableButton runat="server" ID="btnValidateSQL" DisplayType="Small" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_139 %>"></orion:LocalizableButton><br/>
                <div class="sw-suggestion sw-suggestion-fail" id="msgFail" runat="server" style="display: none; margin-left: 0;"><span class="sw-suggestion-icon"></span><span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_202) %></span></div>
                <div class="sw-suggestion sw-suggestion-pass" id="msgPass" runat="server" style="display: none; margin-left: 0;"><span class="sw-suggestion-icon"></span><span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_203) %></span></div>
            </td>
        </tr>
    </table>    
</div>
<script type="text/javascript">    
    (function () {
        var config = new SW.Core.Alerts.Add.CustomSqlConditionConfig();
        config.sqlEntityTypesId = '<%= sqlEntityTypesId.ClientID %>';
        config.sqlFragmentId = '<%= sqlQueryFragement.ClientID %>';
        config.sqlQueryTextId = '<%= customSqlQueryText.ClientID %>';
        config.userSqlQueryTextId = '<%= userSqlQueryText.ClientID %>';
        config.chainItemConfigType = '<%= GetChainItemConfigType(typeof (CustomSqlChainItemConfig)) %>';
        config.chainItemConfigInputId = '<%= JsonInputChainItemConfigId %>';
        config.conditionControllerId = '<%= ConditionControllerId %>';
        config.validateButton = {
            ButtonId: '<%=btnValidateSQL.ClientID %>',
            MsgPassID: '<%=msgPass.ClientID %>',
            MsgFailID: '<%=msgFail.ClientID %>'
        };

        var controller = new SW.Core.Alerts.Add.CustomSqlConditionController(config);
        controller.init();
    })();
</script>
