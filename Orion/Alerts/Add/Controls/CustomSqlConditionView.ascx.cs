﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using System.Collections.Generic;
using SolarWinds.Orion.Core.Alerting.Plugins.Conditions.Models;
using SolarWinds.Orion.Core.Alerting.Plugins.Conditions.Sql;
using SolarWinds.Orion.Core.Models.Alerting;
using SolarWinds.Orion.Core.Alerting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.Alerts.TriggerCondition;

namespace Orion.Alerts.Add.Controls
{
    public partial class CustomSqlConditionView : TriggerConditionPluginViewBase
    {   
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            
            IEnumerable<Entity> custSqlItems = LoadEntities();

            SetQueryFragement(custSqlItems);
            sqlEntityTypesId.DataSource = custSqlItems;
            sqlEntityTypesId.DataTextField = "DisplayName";
            sqlEntityTypesId.DataValueField = "ObjectType";
            sqlEntityTypesId.DataBind();

            var sqlConfig = ChainItemConfig as CustomSqlChainItemConfig;
            if (sqlConfig != null)
            {
                userSqlQueryText.Value = sqlConfig.Command; 
            }
        }

        private IEnumerable<Entity> LoadEntities()
        {
            var conditionTypeProvider = ConditionTypeProvider.Create(AppDomainCatalog.Instance, AlertConditionHelper.GetConditionTypeServices());
            var customSqlType = (ConditionTypeCustomSql)conditionTypeProvider.Items.First(n => n.RefID == ConditionTypeCustomSql.ID);
                
            IOrderedEnumerable<Entity> customSqlItems = customSqlType.EntityProvider.Items
                .Where(entity => !string.IsNullOrEmpty(entity.FullName))    // include only items with in *.NetObjects schema file specified related SWIS entity
                .OrderBy(entity => entity.DisplayName);
            return customSqlItems;
        }

        private void SetQueryFragement(IEnumerable<Entity> items)
        {
            var customData = new Dictionary<string, string>();
            foreach (var entityQueryable in items.OfType<EntityQueryable>())
            {
                customData[entityQueryable.ObjectType] = entityQueryable.GetSelectQueryFragement();
            }

            sqlQueryFragement.Value = JsonConvert.SerializeObject(customData);
        }
    }
}
