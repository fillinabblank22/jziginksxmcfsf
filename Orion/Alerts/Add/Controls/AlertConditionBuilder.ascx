﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlertConditionBuilder.ascx.cs" Inherits="Orion.Alerts.Add.Controls.AlertConditionBuilder" %>
<orion:Include ID="Include1" runat="server" File="RenderControl.js" />
<orion:Include ID="Include2" runat="server" File="Alerts/Add/typescripts/AlertConditionBuilder.js" />
<orion:Include ID="Include4" runat="server" File="AlertTimeIntervals.js" />
<orion:Include ID="Include3" runat="server" File="Alerts/Styles/AlertConditionBuilder.css" />

<orion:JsonInput runat="server" ID="jsonInputConditions" />

<%-- Condition builder initialization --%>
<script type="text/javascript">    
    var config = new SW.Core.Alerts.Add.AlertConditionBuilderConfig();
    config.triggerConditionsContainerId = '<%= triggerConditionsContainer.ClientID %>';
    config.inputJsonConditionsId = '<%= jsonInputConditions.ClientID %>';
    config.conditionBuilderContainerId = '<%= conditionBuilderContainer.ClientID %>';    
    config.expandAdvancedOptions = <%= ExpandAdvancedOptions.ToString().ToLowerInvariant() %>;
    config.exportFileName = '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_32) %>'; // condtition.xml is a default file name
    config.saveButtonIds = <%= SaveButtonClientIDsJSArray %>;

    var conditionBuilder = new SW.Core.Alerts.Add.AlertConditionBuilder(config);
</script>

<style type="text/css">
    .import-file-dialog {
        display: none;
        position: absolute;
        -moz-opacity: 0;
        width: 10px;
        cursor: pointer;
        font-size: 100px; /*to ensure we click on button but not on text field*/
        overflow: hidden;
        filter: alpha(opacity: 0);
        opacity: 0;
        z-index: 99999;
    }

    ::-webkit-file-upload-button { cursor:pointer; }
</style>

<%-- Placeholder for conditions --%>
<div id="triggerConditionsContainer" runat="server" />

<%-- Container for condition builder. It gives a scope for data-* attributes --%>
<div id="conditionBuilderContainer" runat="server">

    <%-- Add button --%>
    <div data-container="addButtonContainer" class="hidden abc-addButtonContainer">
        <orion:LocalizableButtonLink runat="server" LocalizedText="CustomText" DisplayType="Secondary" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PF0_20 %>" data-form="addButton"/>
    </div>

    <%-- Condition operator which is loaded by js --%>
    <div data-container="conditionOperatorContainer" class="abc-conditionOperatorContainer hidden">
        <select id="conditionOperator" data-form="conditionOperator" style="width: 150px; margin-right: 1em;" runat="server" />
        <span data-container="timeIntervalContainer" class="hidden"><%-- Place holder for AndThen condition time interval --%></span>
    </div>

    <%-- Advanced options --%>
    <div style="margin-top: 30px;" class="acb-advancedSection">
        <asp:Image ID="advancedOptionsButton" runat="server" ImageUrl="~/Orion/images/Button.Expand.gif" style="cursor:pointer" data-form="AdvancedOptionsButton"/>
        <%-- Advanced options--%><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_21) %><br/>
        <div data-container="advancedOptionsContainer" class="hidden">
            <div style="background-color: #EFEFEF;margin: 5px; padding: 10px;">
                <input type="checkbox" id="chkNinjaUser" runat="server" data-form="ChkNinjaUser"/>
                <label for="<%= chkNinjaUser.ClientID %>">
                    <img src="/Orion/images/ToolTip/Ninja_icon_16.png" alt=""/>
                    <%--I am power user, enable all advanced features for me--%><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_22) %>
                    » <a href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("orioncoreag-alertsbuildingcomplexconditions")) %>" target="_blank" rel="noopener noreferrer" class="sw-link"><%--Learn even more about advanced features--%><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_23) %></a>
                </label>
            </div>
       </div>
    </div>

    <%-- Validation results placeholder --%>
    <div id="validationResultsContainer" data-container="validationResultsContainer" class="sw-suggestion sw-suggestion-fail" 
        style="margin-top: 16px; margin-bottom: 8px; margin-left: 0px; display: none;" runat="server">
        <span class="sw-suggestion-icon"></span>
        <span id="resultMessage" data-message="resultMessage" runat="server"></span>
    </div>

    <%--Import/Export condition buttons--%>
    <div style="margin-top: 2em;">
        <orion:LocalizableButtonLink runat="server" LocalizedText="Import" DisplayType="Small" data-form="btnImportCondition" ID="btnImportCondition"/>
	    <orion:LocalizableButtonLink runat="server" LocalizedText="Export" DisplayType="Small" data-form="btnExportCondition" ID="btnExportCondition"/>
        <%-- it needs to be hidden like this to work triggering click() work correctly due to browser's security --%>
        <input class="import-file-dialog" id="fileUpload" type="file" accept="text/xml" data-form="fileUpload" runat="server"/>
    </div>

    <%-- Disable power user dialog --%>
    <div data-container="disablePoweUserDialog" class="hidden" style="background-color: #FFF;" title="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_29) %>"><%--Disable Power User Mode?--%>
        <table>
            <tr><td style="width:40px; vertical-align: top;">
                <img src="/Orion/Images/stop_32x32.gif" alt="error" />
            </td><td>
                <%--The multiple sections are supported only in advanced (power user) mode. If you disable this mode all sections except the first one will be removed. Do you want to proceed?--%>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_30) %>
            </td></tr>
        </table>
        <div class="bottom">
            <div class="sw-btn-bar-wizard">
                <orion:LocalizableButtonLink runat="server" LocalizedText="CustomText" DisplayType="Primary" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PF0_31 %>" data-form="btnNinjaDisable"/><%--Disable--%>
		        <orion:LocalizableButtonLink runat="server" LocalizedText="Cancel" DisplayType="Secondary" data-form="btnNinjaCancel"/>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    
    jQuery.fn.center = function () {
        this.css("position", "absolute");
        this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +
        $(window).scrollTop()) + "px");
        this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
        $(window).scrollLeft()) + "px");
        return this;
    };
    <% if (DisableInit) { %>
    $(function() {
        SW.Core.Alerts.Add.AlertConditionBuilder.instance = conditionBuilder;
    });
    <% } else { %>
    $(function () {
        conditionBuilder.init();
        conditionBuilder.loadConditions();

        // to be able to save conditions when clicking on tabs, see ProgressIndicator.ascx
        if (isRedirectAllowedFunction == null) {
            isRedirectAllowedFunction = function() { return conditionBuilder.saveConditions(); };
        }

        $("a[data-form='btnImportCondition']").mousemove(function (e) {
            var position = $(this).offset();
            var fileUp = $("input[data-form='fileUpload']");
            fileUp.css({
                'display': 'block',
                'left': position.left,
                'top': position.top,
                'height': $(this).outerHeight(),
                'width': $(this).outerWidth(),
                'cursor': 'pointer'
            });
            $("input[data-form='fileUpload']").mouseout(function() { $(this).css({ 'display': 'none' }); });
            fileUp.change(function() { <%= Page.ClientScript.GetPostBackEventReference(this, ImportEventArgument) %>; });

            fileUp.mousemove(function(e) {
                $("a[data-form='btnImportCondition']").mouseenter();
            });
        });
    });
    <% } %>
</script>