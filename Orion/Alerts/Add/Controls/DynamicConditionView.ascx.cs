﻿using Newtonsoft.Json;
using SolarWinds.Orion.Core.Alerting.Models;
using SolarWinds.Orion.Web.Alerts.TriggerCondition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_Controls_DynamicConditionView : TriggerConditionPluginViewBase
{
    protected override void OnInit(EventArgs e)
    {
        exprBuilderForAlert.ClientIdPrefix = this.ClientIDPrefix;
        exprBuilderForScope.ClientIdPrefix = this.ClientIDPrefix;
        exprBuilderForScope.ClientInstanceName = exprBuilderForScope.ClientInstanceName.Replace("PARENT", this.ClientIDPrefix);
        exprBuilderForAlert.ClientInstanceName = exprBuilderForAlert.ClientInstanceName.Replace("PARENT", this.ClientIDPrefix);

        DynamicChainItemConfig condition = this.ChainItemConfig as DynamicChainItemConfig;

        exprBuilderForScope.ExprTree = (condition != null && condition.ScopeTree != null) ? JsonConvert.SerializeObject(condition.ScopeTree) : String.Empty;
        exprBuilderForAlert.ExprTree = (condition != null && condition.AlertTree != null) ? JsonConvert.SerializeObject(condition.AlertTree) : String.Empty;

        exprBuilderForScope.EntityFullName = this.ChainItemConfig.EntityFullName;
        exprBuilderForAlert.EntityFullName = this.ChainItemConfig.EntityFullName;
        exprBuilderForScope.ObjectType = this.ChainItemConfig.ObjectType;
        exprBuilderForAlert.ObjectType = this.ChainItemConfig.ObjectType;
    }

    public bool IsScopeLoaded()
    {
        DynamicChainItemConfig condition = this.ChainItemConfig as DynamicChainItemConfig;

        if (condition == null || condition.ScopeTree == null)
            return false;

        return true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}