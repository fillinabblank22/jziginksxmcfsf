﻿using System;
using System.Linq;
using SolarWinds.Orion.Core.Actions.Impl.Email;
using SolarWinds.Orion.Core.Alerting.Models;
using SolarWinds.Orion.Web.Alerts;
using SolarWinds.Orion.Core.Models.Actions;
using System.Collections.Generic;

public partial class Orion_Alerts_Add_ResetActions : AddAlertWizardBase
{
    protected override void OnInit(EventArgs e)
    {
        actionsEditorView.PreSelectedActionTypeID = EmailConstants.ActionTypeID;

        if (Workflow.Alert != null && Workflow.Alert.AlertID.GetValueOrDefault(0) != 0)
        {
            this.Title = Resources.CoreWebContent.WEBDATA_VL0_35;
        }

        if (Workflow.Alert != null)
        {
            Workflow.AlertingActionContext.ExecutionMode = ActionExecutionModeType.Reset;
            actionsEditorView.ViewContext = Workflow.AlertingActionContext;
        }

        if (!IsPostBack)
        {
            if (Workflow.Alert != null)
            {
                ActionSettings baseActionsData = Workflow.Alert.ResetActions;
                var res = InitActions(baseActionsData);
                actionsEditorView.Actions = res;
            }
        }
        Workflow.SetStepFromUrl(this.Page.Request.Url.GetLeftPart(UriPartial.Path));
        base.OnInit(e);
    }

    private List<ActionDefinition> InitActions(ActionSettings baseActionsData)
    {
        List<ActionDefinition> res = null;
        if (baseActionsData.Any())
        {
            res = baseActionsData.Select(actionInfo =>
            {
                actionInfo.IconPath = pluginInfoProvider.GetActionIconPath(actionInfo.ActionTypeID);
                return actionInfo;
            }).ToList();
        }
        return res;
    }

    protected void CopyActionsToReset(object sender, EventArgs e)
    {
        SaveCurrentState();
        ActionSettings baseActionsData = Workflow.Alert.TriggerActions;
        List<ActionDefinition> res = InitActions(baseActionsData);
        base.CopyActionsToReset(res);

        if (res != null && res.Count > 0)
        {
            actionsEditorView.Actions = Workflow.Alert.ResetActions;
            suggestionActionsCopied.Visible = true;
            noActionsCopied.Visible = false;
        }
        else
        {
            suggestionActionsCopied.Visible = false;
            noActionsCopied.Visible = true;
        }   
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        suggestionActionsCopied.Visible = false;
        noActionsCopied.Visible = false;

		Title += " - \"" + Workflow.Alert.Name + "\"";
    }

    protected override bool SaveCurrentState()
    {
        try
        {
            if (Workflow.Alert != null)
            {
                Workflow.Alert.ResetActions.Clear();
                Workflow.Alert.ResetActions.AddRange(actionsEditorView.Actions);
                return true;
            }
        }
        catch(Exception ex)
        {
            log.Error("Error while updating alert reset actions data", ex);
        }
        return false;
    }
}
