﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Alerts/Add/AddAlertWizard.master"
    AutoEventWireup="true" CodeFile="ResetCondition.aspx.cs" Inherits="Orion_Alerts_Add_ResetCondition"
    Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_36 %>" %>
<%@ Import Namespace="Orion.Alerts.Add.Controls" %>

<%@ Register TagPrefix="orion" TagName="AlertConditionBuilder" Src="~/Orion/Alerts/Add/Controls/AlertConditionBuilder.ascx" %>
<%@ Register TagPrefix="orion" TagName="AlertTimeIntervals" Src="~/Orion/Controls/AlertTimeIntervals.ascx" %>
<%@ Register Src="~/Orion/Controls/NetObjectPicker.ascx" TagPrefix="orion" TagName="NetObjectPicker" %>
<%@ Register TagPrefix="orion" TagName="MacroVariablePicker" Src="~/Orion/Controls/MacroVariablePicker.ascx" %>

<asp:Content ContentPlaceHolderID="headPlaceholder" runat="server">
    <orion:Include runat="server" Framework="Ext"  FrameworkVersion="3.4" />
</asp:Content>
<asp:Content ID="suggestionMessageContent" ContentPlaceHolderID="WizardStepHintContent" runat="Server">
    <% if (Workflow.Alert.Canned)
       { %> 
           <div><span class='sw-suggestion' style='margin : 0px 0px 10px 0px'><span class='sw-suggestion-icon'></span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_MG0_1) %></span></div>
    <% } %>
</asp:Content>
<asp:Content ContentPlaceHolderID="wizardContent" runat="Server">
    <style type="text/css">
        .rc-radioButton {
            margin: 0px 3px 3px 0px;
            display: block;
        }
        .rc-radioButton input[type='radio'] {
            margin: 3px 3px 3px 0px;
        }
       
        .import-file-dialog {
            display: none;
            position: absolute;
            -moz-opacity: 0;
            width: 10px;
            cursor: pointer;
            font-size: 100px; /*to ensure we click on button but not on text field*/
            overflow: hidden;
            filter: alpha(opacity: 0);
            opacity: 0;
            z-index: 99999;
        }

        ::-webkit-file-upload-button { cursor:pointer; }
    </style>
    <orion:MacroVariablePicker ID="MacroVariablePicker" runat="server"/> 
    <asp:Panel runat="server" DefaultButton="imgbNext">
        <div id="insertVariableNetObjectPickerDlg" style="display: none">
            <div class='sw-btn-bar-wizard' style="margin-bottom: 0; padding-bottom: 0;">
                <orion:LocalizableButton ID="btnNetObjectChange"  CausesValidation="false" ClientIDMode="Static" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBJS_TK0_5 %>" OnClientClick="return false;" />
                <orion:LocalizableButton ID="btnNetObjectCancel"  CausesValidation="false" ClientIDMode="Static" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClientClick="return false;" />
            </div>
        </div>

        <div id="insertNetObjectPickerDlg" style="display: none">
            <div class='sw-btn-bar-wizard' style="margin-bottom: 0; padding-bottom: 0;">
                <orion:LocalizableButton ID="btnNetObjectPickerChange"  CausesValidation="false" ClientIDMode="Static" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBJS_TK0_5 %>" OnClientClick="return false;" />
                <orion:LocalizableButton ID="btnNetObjectPickerCancel"  CausesValidation="false" ClientIDMode="Static" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClientClick="return false;" />
            </div>
        </div>

        <div style="display: none">
             <orion:NetObjectPicker runat="server" ID="NetObjectPicker" />
        </div>
        <div class="GroupBox"
            <% if (Workflow.Alert.Canned)
               { %> 
                   style="position: relative"
            <% } %>>
            <% if (Workflow.Alert.Canned)
               { %> 
              <div class="editCannedAlertDisabledContainer" style="display: block"></div>
            <% } %>
            <div class="wizardHeader">
                <h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_151) %></h2>
            </div>
            <div class="wizardContent">
               <span>
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_145) %>&nbsp;<span class="LinkArrow">»</span><a target="_blank" rel="noopener noreferrer" href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("orioncoreag-alertssettingresetconditions")) %>" style="text-decoration:underline;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_404) %></a>
                </span>
                <br /><br />
                <div style="padding-bottom:35px; width: 100%;">
                    <asp:RadioButton id="inverseCondition" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_146 %>" Checked="False" GroupName="reset" runat="server" CssClass="rc-radioButton" />
                    <div id="automaticConditionContainer" runat="server">
                        <%-- Reset this alert automatically after XX minutes --%>
                        <asp:RadioButton id="automaticCondition" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PF0_39 %>" Checked="False" GroupName="reset" runat="server" CssClass="rc-radioButton" style="display: inline" />
                        <orion:AlertTimeIntervals runat="server" ID="timeIntervals" />
                    </div>
                    <asp:RadioButton id="resetWhenTriggered" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IT0_23 %>" Checked="False" GroupName="reset" runat="server" CssClass="rc-radioButton"/>
                    <asp:RadioButton id="noResetCondition" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_204 %>" Checked="False" GroupName="reset" runat="server" CssClass="rc-radioButton"/>
                    <asp:RadioButton id="specialCondition" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_147 %>" Checked="False" GroupName="reset" runat="server" CssClass="rc-radioButton"/>
                    
                    <div id="specialCondPanel" style="margin-left: 1.1em; margin-top: 1em;" runat="server" ClientIDMode="Static">
                        <div id="conditionMenu" runat="server" style="width: 100%; background-color: #EBEBEB; padding: 0.3em; padding-top: 0.5em; text-align: center;">
                            <span style="color: #696969; float: left; width: 33%; text-align: left;">
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_34) %>
                            </span>
                            <%-- Add Condition button --%>
                            <orion:LocalizableButtonLink runat="server" LocalizedText="CustomText" DisplayType="Primary" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PF0_33 %>" ID="btnMenu" ArrowStyle="Down" />
                            <span style="float: right; width: 33%;">&nbsp;</span>
                            <input id="fileUpload" type="file" class="import-file-dialog" data-form="fileUploadReset" runat="server"/>
                        </div>
                        <div id="conditionBuilderContainer" runat="server" class="hidden">
                            <orion:AlertConditionBuilder runat="server" ID="conditionBuilder" DisableInit="True" />
                        </div>
                    </div>
                </div>
            </div>
         </div>
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButton runat="server" ID="imgbBack" OnClick="Back_Click" LocalizedText="Back" DisplayType="Secondary" CausesValidation="false" />
            <orion:LocalizableButton runat="server" ID="imgbNext" OnClick="Next_Click" LocalizedText="Next" DisplayType="Primary" />
            <orion:LocalizableButton runat="server" ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" />
        </div>

   <script type="text/javascript">
       
       $(function () {
           function createNewCondition() {
               <%= Page.ClientScript.GetPostBackEventReference(this, NewConditionEventArgument) %>;
           }

           function copyFromTrigger() {
               <%= Page.ClientScript.GetPostBackEventReference(this, CopyConditionFromTriggerEventArgument) %>;
           }

           var config = {
               menuButtonId: '<%= btnMenu.ClientID %>',
               menuItems: [
                   { onClickHandler: createNewCondition, title: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_35) %>' <%-- 'Create new condition' --%> },
                   { onClickHandler: copyFromTrigger, title: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_67) %>' <%-- 'Copy condition from trigger' --%> },
                   { onClickHandler: function () {}, title: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_36) %>', id: 'resetConditionImport' <%-- 'Import condition...' --%> }
               ]
           };
           
           var menu = new SW.Core.Widgets.SimpleMenu(config);
           menu.init();

           var automaticConditionInterval = <%= timeIntervals.JsInstanceName %>;
           automaticConditionInterval.enable($('#<%= automaticCondition.ClientID %>')[0].checked);
           
           
           // show/hide specialCondition
           $('#specialCondPanel').css('display', $("#<%= specialCondition.ClientID %>")[0].checked ? '' : 'none');

           $('#<%= btnMenu.ClientID %>').click(function () { menu.toggle(); });
           $('#<%= inverseCondition.ClientID %>').click(function () {
               $('#specialCondPanel').css('display', 'none');
               automaticConditionInterval.enable(false);
           });

           var setDefaultValueForAutomaticConditionInterval = function() {
               $('#specialCondPanel').css('display', 'none');
               
               if (isNaN(automaticConditionInterval.getTimeInSeconds())) {
                   automaticConditionInterval.setTimeInterval(3600);
               }
               automaticConditionInterval.enable($('#<%= automaticCondition.ClientID %>')[0].checked);
           };

           $('#<%= automaticCondition.ClientID %>').click(
               setDefaultValueForAutomaticConditionInterval
           );
           $('#<%= noResetCondition.ClientID %>').click(function () {
               $('#specialCondPanel').css('display', 'none');
               automaticConditionInterval.enable(false);
           });
           $('#<%= resetWhenTriggered.ClientID %>').click(function () {
               $('#specialCondPanel').css('display', 'none');
               automaticConditionInterval.enable(false);
           });
           $('#<%= specialCondition.ClientID %>').click(function () {
               $('#specialCondPanel').css('display', '');
               automaticConditionInterval.enable(false);
           });

           var hash = {
               '.xml': 1
          };

           function importFileSelected (file) {
               var re = /\..+$/;
               var ext = file.match(re);
               if (hash[ext]) {
                    <%= Page.ClientScript.GetPostBackEventReference(this, AlertConditionBuilder.ImportEventArgument) %>;
                    return true;
               } else {
                   Ext.Msg.show({
                       width: 400,
                       title: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_AY0_95) %>',
                       msg: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_AY0_96) %>',
                       buttons: Ext.Msg.OK,
                       icon: Ext.MessageBox.ERROR
                   });
                   return false;
               }
           };

           $("div[data-form='resetConditionImport']").mousemove(function (e) {
               var position = $(this).offset();
               var fileUp = $("input[data-form='fileUploadReset']");
               fileUp.css({
                   'display': 'block',
                   'left': position.left,
                   'top': position.top,
                   'height': $(this).outerHeight(),
                   'width': $(this).outerWidth(),
                   'cursor': 'pointer'
               });

               fileUp.change(function() {
                   var fileName = $("input[data-form='fileUploadReset']").val();
                   importFileSelected(fileName);
               });    

               fileUp.mouseenter(function(e) {
                   $("div[data-form='resetConditionImport']").toggleClass("hovered");
               });

               fileUp.mouseleave(function(e) {
                   $("div[data-form='resetConditionImport']").toggleClass("hovered");
               });
           });

       });
   </script>
    </asp:Panel>
</asp:Content>
