﻿using System;
using System.Linq;
using SolarWinds.Orion.Core.Alerting;
using SolarWinds.Orion.Core.Alerting.Models;
using SolarWinds.Orion.Core.Alerting.Plugins.Conditions.Dynamic;
using SolarWinds.Orion.Core.Models.Alerting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Alerts;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Core.Common.Alerting;
using SolarWinds.Internationalization.Extensions;
using System.Web;

namespace Orion.Alerts.Add
{
    public partial class TriggerCondition : AddAlertWizardBase
    {

        protected override void OnInit(EventArgs e)
        {
            Workflow.SetStepFromUrl(this.Page.Request.Url.GetLeftPart(UriPartial.Path));
            base.OnInit(e);

            conditionBuilder.SaveButtonClientIDs = new[] { imgbNext.ClientID, imgbBack.ClientID };

            if (Workflow.Alert != null && Workflow.Alert.AlertID.GetValueOrDefault(0) != 0)
            {
                this.Title = Resources.CoreWebContent.WEBDATA_VL0_35;
            }

            if (!IsPostBack)
            {
                if (Workflow.Alert != null)
                {
                    if (Workflow.Alert.Trigger != null && Workflow.Alert.Trigger.Conditions.Any())
                    {
                        conditionBuilder.ConditionChain = Workflow.Alert.Trigger;
                        conditionBuilder.ConditionChain = WebSecurityHelper.EncodeExpressionTree(conditionBuilder.ConditionChain);
                    }
                    else
                    {
                        ConditionTypeProvider conditionTypeProvider = ConditionTypeProvider.Create(AppDomainCatalog.Instance, AlertConditionHelper.GetConditionTypeServices());
                        IConditionType dynamicType = conditionTypeProvider.Get(ConditionTypeDynamic.ID);

                        var defaultChain = new ConditionChain();
                        defaultChain.Conditions.Add(new ConditionChainItem
                        {
                            Condition = new AlertConditionDynamic(),
                            Type = dynamicType,
                            ObjectType = "Node"
                        });

                        conditionBuilder.ConditionChain = defaultChain;
                    }
                }
            }
        }

		protected void Page_Load(object sender, EventArgs e)
		{
			Title += " - \"" + Workflow.Alert.Name + "\"";
		}

		protected override bool SaveCurrentState()
        {
            try
            {
                if (Workflow.Alert != null)
                {
                    if (Workflow.Alert.Canned)
                    {
                        return true;
                    }

                    ConditionChain conditionChain = conditionBuilder.ConditionChain;
                    Workflow.Alert.Trigger = WebSecurityHelper.DecodeExpressionTree(conditionChain);
                    Workflow.Alert.ObjectType = conditionChain.Conditions.First().ObjectType;

                    if (Workflow.Alert.AlertID == null)
                    {
                        // Keep in sync with SolarWinds.Orion.Web.Alerts.AddAlertWorkflowProvider, method GetNotificationSettings
                        var provider = new AlertNotificationSettingsProvider();
                        Workflow.Alert.NotificationSettings = provider.GetDefaultAlertNotificationSettings(Workflow.Alert.ObjectType, Workflow.Alert.Name);
                        Workflow.Alert.NotificationSettings.Severity = Workflow.Alert.Severity.LocalizedLabel();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error while updating alert trigger condition data", ex);
            }
            return false;
        }
    }
}