﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Alerts/Add/AddAlertWizard.master"
    AutoEventWireup="true" CodeFile="TriggerActions.aspx.cs" Inherits="Orion_Alerts_Add_TriggerActions"
    Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_36 %>" %>

<%@ Register TagPrefix="orion" TagName="ActionsEditorView" Src="~/Orion/Actions/Controls/ActionsEditorView.ascx" %>
<%@ Register tagPrefix="orion" tagName="ActionPluginMetadataProvider" src="~/Orion/Actions/Controls/ActionPluginMetadataProvider.ascx" %>

<asp:Content ContentPlaceHolderID="headPlaceholder" runat="server">
    <style type="text/css">
        .sw-suggestion { margin: 10px 0; }
        .actions-section { padding-top: 0; }
        .helpfulText         { color: #5F5F5F; font-size: 10px; vertical-align: middle; }
        .wizardHeader h2 { padding: 0 10px 10px 10px !important; }
        .wizardContent { margin: 0 10px 0 10px; width: auto; }
        .sw-btn-bar { margin: 30px 0 30px 0; }
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="wizardContent" runat="Server">
    <orion:ActionPluginMetadataProvider runat="server" ID="pluginInfoProvider"/>
    <asp:Panel runat="server" DefaultButton="imgbNext">
        <div class="GroupBox">
            <div class="wizardHeader">
                <h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_156) %></h2>
            </div>
            <div class="wizardContent">
                <div>
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_157) %>                    
                </div>
                <div style="margin: 5px 0 30px 0;">
                    <a style="color: #336699;" target="_blank" rel="noopener noreferrer" href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("orioncoreag-alertssettingtriggeractions")) %>"><span class="LinkArrow">»</span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_184) %></a>
                </div>
                <div style="margin-bottom: 30px;">
                    <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_182) %></b><br/>
                    <span class="helpfulText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_183) %></span>
                    <table cellpadding="0" cellspacing="0" width="100%" style="margin-top: 6px;">
                    <tr>
                    <td style="width: 100%;">
                        <textarea rows="6" cols="1" runat="server" id="alertMessage" data-form="AlertMessageText" style="width: 99%; resize: both;"></textarea>
                    </td>
                    <td style="vertical-align: top;">
                        <orion:LocalizableButton ID="txtBodyInsertVariable" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IT0_1 %>"
                      DisplayType="Small" OnClientClick="return false" data-macro="AlertMessageText" runat="server"/>
                    </td>
                    </tr>
                    </table>
                </div>
                <div>
                    <h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IT0_24) %></h2>
                </div>
                <div class="actions-section">
                    <orion:ActionsEditorView runat="server" EnableTestAction="True"  ID="actionsEditorView" ClientInstanceName="TriggerAlertingActions" EnvironmentType="Alerting" CategoryType="Trigger" EnableEscalationLevels="true" />
                </div>
                <div class="sw-btn-bar" >
                    <orion:LocalizableButton runat="server" ID="btnCopyToReset" OnClick="CopyActionsToReset" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_344 %>"
                        DisplayType="Small" CausesValidation="false" />
                </div>
                 <div>
                        <div id="suggestionActionsCopied" runat="server" class="sw-suggestion sw-suggestion-pass">
                                <span class="sw-suggestion-icon"></span>
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_339) %>
                            </div>
                        <div id="noActionsCopied" runat="server" class="sw-suggestion sw-suggestion-warn">
                            <span class="sw-suggestion-icon"></span>
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_341) %>
                        </div>
                  </div>
             </div>
        </div>
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButton runat="server" ID="imgbBack" OnClick="Back_Click" LocalizedText="Back" DisplayType="Secondary" CausesValidation="false" />
            <orion:LocalizableButton runat="server" ID="imgbNext" OnClick="Next_Click" LocalizedText="Next" DisplayType="Primary" />
            <orion:LocalizableButton runat="server" ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" />
        </div>
    </asp:Panel>
    <script type="text/javascript">
        (function () {
            SW.Core.MacroVariablePickerController.EnableInsertVariableButtons();
        })();
    </script>
</asp:Content>