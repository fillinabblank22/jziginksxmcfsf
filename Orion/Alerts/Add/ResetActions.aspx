﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Alerts/Add/AddAlertWizard.master"
    AutoEventWireup="true" CodeFile="ResetActions.aspx.cs" Inherits="Orion_Alerts_Add_ResetActions"
    Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_36 %>" %>
<%@ Register TagPrefix="orion" TagName="ActionsEditorView" Src="~/Orion/Actions/Controls/ActionsEditorView.ascx" %>
<%@ Register tagPrefix="orion" tagName="ActionPluginMetadataProvider" src="~/Orion/Actions/Controls/ActionPluginMetadataProvider.ascx" %>

<asp:Content ContentPlaceHolderID="headPlaceholder" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="wizardContent" runat="Server">
    <orion:ActionPluginMetadataProvider ID="pluginInfoProvider" runat="server"/>
    <asp:Panel runat="server" DefaultButton="imgbNext">
        <div class="GroupBox">
            <div class="wizardHeader">
                <h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_154) %></h2>
            </div>
            <div class="wizardContent">
                <span>
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_155) %>
                </span>
                <br/><br/>
                <orion:ActionsEditorView runat="server" ID="actionsEditorView" EnableTestAction="True"  ClientInstanceName="ResetAlertingActions" EnvironmentType="Alerting" CategoryType="Reset" EnableExecutionSettings="False"/>
                <br/>
            </div>
            <div class="sw-btn-bar" style="margin-left: 10px;">
                    <div style="margin-left: 10px;">
                    <orion:LocalizableButton runat="server" ID="btnCopyToReset" OnClick="CopyActionsToReset" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_340 %>"
                        DisplayType="Small" CausesValidation="false" />
                        </div>
                    <div>
                        <div id="suggestionActionsCopied" runat="server" class="sw-suggestion sw-suggestion-pass">
                                <span class="sw-suggestion-icon"></span>
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_339) %>
                            </div>
                        <div id="noActionsCopied" runat="server" class="sw-suggestion sw-suggestion-warn">
                            <span class="sw-suggestion-icon"></span>
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_341) %>
                        </div>
                    </div>
                </div>
        </div>
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButton runat="server" ID="imgbBack" OnClick="Back_Click" LocalizedText="Back" DisplayType="Secondary" CausesValidation="false" />
            <orion:LocalizableButton runat="server" ID="imgbNext" OnClick="Next_Click" LocalizedText="Next" DisplayType="Primary" />
            <orion:LocalizableButton runat="server" ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel"
                DisplayType="Secondary" CausesValidation="false" />
        </div>
    </asp:Panel>
</asp:Content>