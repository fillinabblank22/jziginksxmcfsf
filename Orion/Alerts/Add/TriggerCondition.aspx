﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Alerts/Add/AddAlertWizard.master"
    AutoEventWireup="true" CodeFile="TriggerCondition.aspx.cs" Inherits="Orion.Alerts.Add.TriggerCondition"
    Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_36 %>" %>

<%@ Register TagPrefix="orion" TagName="AlertConditionBuilder" Src="~/Orion/Alerts/Add/Controls/AlertConditionBuilder.ascx" %>
<%@ Register Src="~/Orion/Controls/NetObjectPicker.ascx" TagPrefix="orion" TagName="NetObjectPicker" %>
<%@ Register TagPrefix="orion" TagName="MacroVariablePicker" Src="~/Orion/Controls/MacroVariablePicker.ascx" %>

<asp:Content ContentPlaceHolderID="headPlaceholder" runat="server">
    <orion:Include runat="server" Framework="Ext"  FrameworkVersion="3.4" />
</asp:Content>
<asp:Content ID="suggestionMessageContent" ContentPlaceHolderID="WizardStepHintContent" runat="Server">
    <% if (Workflow.Alert.Canned)
       { %> 
           <div><span class='sw-suggestion' style='margin : 0px 0px 10px 0px'><span class='sw-suggestion-icon'></span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_MG0_1) %></span></div>
    <% } %>
</asp:Content>
<asp:Content ContentPlaceHolderID="wizardContent" runat="Server">
    <orion:MacroVariablePicker ID="MacroVariablePicker" runat="server"/> 
    <asp:Panel runat="server" DefaultButton="imgbNext">
        <div id="insertNetObjectPickerDlg" style="display: none">
            <div class='sw-btn-bar-wizard' style="margin-bottom: 0; padding-bottom: 0;">
                <orion:LocalizableButton ID="btnNetObjectPickerChange"  CausesValidation="false" ClientIDMode="Static" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBJS_TK0_5 %>" OnClientClick="return false;" />
                <orion:LocalizableButton ID="btnNetObjectPickerCancel"  CausesValidation="false" ClientIDMode="Static" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClientClick="return false;" />
            </div>
        </div>

        <div style="display: none">
             <orion:NetObjectPicker runat="server" ID="NetObjectPicker" />
        </div>
        <div class="GroupBox"
            <% if (Workflow.Alert.Canned)
               { %> 
                   style="position: relative"
            <% } %>>
            <% if (Workflow.Alert.Canned)
               { %> 
              <div class="editCannedAlertDisabledContainer" style="display: block"></div>
            <% } %>
            <div class="wizardHeader">
                <h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YP0_15) %></h2>
            </div>  
            <div class="wizardContent">
                <div id="disabledContainer"></div>
                <div style="padding-bottom: 10px;">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YP0_16) %>&nbsp;<span class="LinkArrow">»</span><a target="_blank" rel="noopener noreferrer" href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("orioncoreag-alertssettingtriggerconditions")) %>" style="text-decoration:underline;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_404) %></a>
                </div>
                <table style="padding-bottom:35px;">
                    <tr>
                        <td>
                            <orion:AlertConditionBuilder runat="server" ID="conditionBuilder" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButton runat="server" ID="imgbBack" OnClick="Back_Click" LocalizedText="Back" DisplayType="Secondary" CausesValidation="false" />
            <orion:LocalizableButton runat="server" ID="imgbNext" OnClick="Next_Click" LocalizedText="Next" DisplayType="Primary" />
            <orion:LocalizableButton runat="server" ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" />
        </div>
    </asp:Panel>
</asp:Content>
