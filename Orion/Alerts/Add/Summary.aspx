﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Alerts/Add/AddAlertWizard.master" AutoEventWireup="true" CodeFile="Summary.aspx.cs" Inherits="Orion_Alerts_Add_Summary"
    Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_36 %>" %>
    
<%@ Register TagPrefix="orion" TagName="MacroVariablePicker" Src="~/Orion/Controls/MacroVariablePicker.ascx" %>
<%@ Register Src="~/Orion/Controls/NetObjectPicker.ascx" TagPrefix="orion" TagName="NetObjectPicker" %>
<%@ Register tagPrefix="orion" tagName="ActionPluginMetadataProvider" src="~/Orion/Actions/Controls/ActionPluginMetadataProvider.ascx" %>

<asp:Content ContentPlaceHolderID="headPlaceholder" runat="server">
            <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
            <orion:Include ID="Include2" runat="server" File="OrionCore.js" />
     <script type="text/javascript">
         function ToggleAdvanced(button) {
             var div = $("#integrationAdvancedDiv");
             div.toggleClass("hidden");
             if (div.hasClass("hidden")) {
                 button.setAttribute("src", "/Orion/images/Button.Expand.gif");
             } else {
                 button.setAttribute("src", "/Orion/images/Button.Collapse.gif");
             }
         }

         function ToggleAdvancedRow() {
             var div = $("#<%=advancedTr.ClientID%>");
             div.toggleClass("hidden");
         }

         function GetNotifName() {
             var chb = $("#<%=alertNameTitle.ClientID%>");
             if (!chb.is(':checked'))
                 return;

             var tb = $("#<%=auditTitle.ClientID%>");
             var tbName = $("#<%=alertName.ClientID%>");
             tb.val(tbName.html());
         }

         jQuery(function () {
             $('#<%=auditTitle.ClientID%>').keyup(function () {
                 var chb = $("#<%=alertNameTitle.ClientID%>");
                 if (chb.is(':checked'))
                     chb.attr('checked', false);
                 return true;
             });

         	Ext.QuickTips.init();
         });
     </script>
     </asp:Content>
    <asp:Content ContentPlaceHolderID="wizardContent" runat="Server">
    <style type="text/css">
        .mGrid {   
    background-color: #fff;   
    margin: 5px 0 10px 0;   
    border: solid 1px #525252;   
    border-collapse:collapse;   
}  
.mGrid td {   
    padding: 2px;   
    border: solid 1px #c1c1c1;   
    color: #717171;   
}  
.mGrid th {   
    padding: 4px 2px;   
    color: #fff;   
    background: #424242;   
    border-left: solid 1px #525252;   
    font-size: 0.9em;   
}  
.mGrid .alt { background: #fcfcfc; } 
        
         .sw-suggestion a { color:#336699; }
        .affectedObjectsHolder
        {
            margin:0px 0px 20px 0px;
            text-align: left;
            width: 310px;
        }

	span.value-not-specified {
		color: #888888 !important;
	}

    .conditions span.informText {
        word-break: break-all;
        display: block;
    }

        .hidden-variable-textbox {
            -moz-opacity: 0;
            position: absolute;
            width: 0;
            height: 0;
            filter: alpha(opacity: 0);
            opacity: 0;
        }

    </style>
    <orion:ActionPluginMetadataProvider ID="pluginInfoProvider" runat="server"/>
    <orion:MacroVariablePicker ID="MacroVariablePicker" runat="server"/>
    <div style="display: none">
       <orion:NetObjectPicker runat="server" ID="NetObjectPicker" />
   </div>
    <asp:Panel runat="server" DefaultButton="imgbNext">
        <div class="GroupBox">
        <div style="max-width: 850px;">
            <div class="wizardHeader">
            <h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_50) %></h2>
            </div>
            <div class="wizardContent">
            <div class="paragraph">
            <div>
            <span class="informText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_58) %></span>
            </div>
            </div>
            <span class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0004) %>:</span>
            <div class="editLink"><a href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.UrlHelper.AppendParameter(Workflow.FirstStepUrl, "AlertWizardGuid", Workflow.Id.ToString())) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_28) %></a></div>
            <div class="paragraph">
            <asp:Label ID="alertName" runat="server" CssClass="alertName"></asp:Label>
            </div>
            <div class="paragraph">
            <span class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0005) %>:</span>
            <div style="word-wrap: break-word"><asp:Label ID="description" CssClass="informText" runat="server"></asp:Label></div> 
            </div>
            <div class="paragraph">
                <div class="boldLabel"> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YP0_17) %> </div>
                 <div style="word-wrap: break-word"><asp:Label ID="propType" CssClass="informText" runat="server"></asp:Label></div> 
            </div>
            <div class="paragraph">
            <span class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_51) %></span>
            <div><asp:Label ID="enabledAlert" CssClass="informText" runat="server"></asp:Label>
            </div>             
            </div>
            <div class="paragraph">
            <span class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_52) %></span>
            <div><asp:Label ID="frequency" CssClass="informText" runat="server"></asp:Label>
            </div>
            </div>
            <div class="paragraph">
            <span class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0007) %></span>
            <div><asp:Label ID="severity" CssClass="informText" runat="server"></asp:Label>
            </div>
            </div>
            <div class="paragraph">
            <div class="boldLabel"> <asp:Label runat="server" ID="alertCustomPropertiesSectionTitle"></asp:Label> </div>
            <asp:Panel CssClass="informText" runat="server" ID="alertCustomProperties"></asp:Panel>
            </div>
            <div class="paragraph" ID="alertOwnerInfoContainer" runat="server">
            <span class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AlertWizard_Summary_AlertOwner) %>:</span>
            <div><asp:Label ID="alertOwnerInfo" CssClass="informText" runat="server"></asp:Label></div>
            </div>
            <div class="paragraph">
            <span class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AlertWizard_Summary_Category) %></span>
            <div><asp:Label ID="category" CssClass="informText" runat="server"></asp:Label></div>
            </div>
             <div class="verticalLine"></div>
            <div class="paragraph">
            <span class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_53) %></span>
             <div class="editLink"><a href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.UrlHelper.AppendParameter(Workflow.Steps[1].Url, "AlertWizardGuid", Workflow.Id.ToString())) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_28) %></a></div>
            <asp:Panel CssClass="infoCollection conditions" ID="TriggerConditions" runat="server"></asp:Panel>
            </div> 

            <div class="verticalLine"></div>   

            <div class="paragraph">
            <span class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_54) %></span>
               <div class="editLink"><a href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.UrlHelper.AppendParameter(Workflow.Steps[2].Url, "AlertWizardGuid", Workflow.Id.ToString())) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_28) %></a></div>
            <asp:Panel  CssClass="infoCollection conditions" ID="ResetConditions" runat="server"></asp:Panel>
            </div>

               <div class="verticalLine"></div> 

            <div class="paragraph">
            <span class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_55) %></span>
             <div class="editLink"><a href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.UrlHelper.AppendParameter(Workflow.Steps[3].Url, "AlertWizardGuid", Workflow.Id.ToString())) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_28) %></a></div>
            <asp:Panel CssClass="infoCollection" ID="TimesOfDay" runat="server"></asp:Panel>
            </div>

             <div class="verticalLine"></div>

             <div class="paragraph">
            <span class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_56) %></span>
              <div class="editLink"><a href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.UrlHelper.AppendParameter(Workflow.Steps[4].Url, "AlertWizardGuid", Workflow.Id.ToString())) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_28) %></a></div>
            <asp:Panel  CssClass="infoCollection " ID="triggerActions" runat="server"></asp:Panel>
            </div>

             <div class="verticalLine"></div>

            <div class="paragraph">
            <span class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_57) %></span>
             <div class="editLink"><a href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.UrlHelper.AppendParameter(Workflow.Steps[5].Url, "AlertWizardGuid", Workflow.Id.ToString())) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_28) %></a></div>
            <asp:Panel  CssClass="infoCollection " ID="resetActions" runat="server"></asp:Panel>
            </div>
            <div class="verticalLine"></div>
        <div class="paragraph">
                <asp:ImageButton ID="AdvImageButton" runat="server" ImageUrl="~/Orion/images/Button.Expand.gif" OnClientClick="ToggleAdvanced(this); return false;" CausesValidation="false" /><span class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO_189) %></span>
                <div style="padding-left: 17px; padding-top: 10px;" id="integrationAdvancedDiv" class="hidden">
                    <table cellspacing="10" width="850px;">
                        <tr>
                            <td>
                                <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO_190) %></b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO_195) %><a class="editLink" target="_blank" rel="noopener noreferrer" href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("OrionCoreAG-AlertIntegratedProducts")) %>" target="_blank" style="color: #336699; margin-left: 5px; float:none;"><span class="LinkArrow">»</span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO_196) %></a><br/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox runat="server" ID="integrateCb" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO_191 %>" OnClick="ToggleAdvancedRow();"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="advancedTr" style="background-color: lightgrey; padding: 10px; margin-left: 10px;" runat="server">
                                    <div style="vertical-align: middle;">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO_192) %> <br/>
                                        <asp:TextBox runat="server" ID="auditTitle" Width="500px"/>&nbsp;<asp:CheckBox runat="server" ID="alertNameTitle" OnClick="GetNotifName();" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO_193 %>"/><br/>
                                    </div>
                                    <div style="padding-top: 10px;">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_200) %><br/>
                                        <asp:DropDownList runat="server" ID="auditSeverity" />
                                    </div>
                                    <div style="padding-top: 10px;">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO_194) %><br/>
                                    </div>
                                    <div style="padding-top: 10px;">
                                         <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                        <asp:GridView ID="VariablesGrid" runat="server" AutoGenerateColumns="False"
                                                    GridLines="None" CssClass="mGrid" onrowediting="VariablesGrid_RowEditing" onrowcancelingedit="VariablesGrid_RowCancelingEdit" onrowupdating="VariablesGrid_RowUpdating" OnRowDeleting="VariablesGrid_RowDeleting">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_44 %>">
                                                             <EditItemTemplate>
                                                                <asp:TextBox ID="nameTb" runat="server" Text='<%# Bind("Name") %>' Width="100%"></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="nameLable" runat="server" Text='<%# DefaultSanitizer.SanitizeHtml(Eval("Name")) %>' Width="180" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_191 %>">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="valueTb" runat="server" Text='<%# Bind("Value") %>' Width="300" CssClass="variable-value-tb"></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="valueLable" runat="server" Text='<%# DefaultSanitizer.SanitizeHtml(Eval("Value")) %>' Width="300" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_83 %>">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="EditBtn" runat="server" ToolTip="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_28 %>"
                                                                    ImageUrl="~/Orion/Discovery/images/icon_edit.gif" CommandName="Edit"/>
                                                                <asp:ImageButton ID="DelBtn" runat="server" ToolTip="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_31 %>"
                                                                    ImageUrl="~/Orion/Discovery/images/icon_delete.gif" CommandName="Delete" />
                                                            </ItemTemplate>
                                                             <EditItemTemplate>
                                                                 <asp:ImageButton ID="UpdateBtn" runat="server" ToolTip="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_VB0_181 %>"
                                                                    ImageUrl="~/Orion/Images/ok_16x16.gif"
                                                                    CommandName="Update"/>
                                                                    <asp:ImageButton ID="CancelBtn" runat="server" ToolTip="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_PCC_28 %>"
                                                                    ImageUrl="~/Orion/Discovery/images/icon_deselect_all.gif"
                                                                    CommandName="Cancel"/>
                                                             </EditItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <EmptyDataTemplate>
                                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_190) %>
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                                <input type="text" data-form="AuditNotificationCell" runat="server" id="AuditNotificationCell" class="hidden-variable-textbox"/>
                                              </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <orion:LocalizableButton ID="txtBodyInsertVariable" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IT0_1 %>"
                                              DisplayType="Small" OnClientClick="return false" data-macro="AuditNotificationCell" runat="server" Style="margin-left: 0;"/>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        </div>
        <asp:Panel ID="affectedObjectsCountHolder" runat="server">
            <div style="text-align: right">
                <div class="sw-suggestion <%= affectedObjectsDivStyle %> affectedObjectsHolder">
                    <span id="affectedObjectsIconSpan" runat="server" class="sw-suggestion-icon"></span>
                    <asp:Label ID="affectedObjectsCount" runat="server" CssClass="boldLabel"></asp:Label>&nbsp;
                    <a id="showAllAffectedObjects" runat="server" href="#"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IT0_11) %></a>
                    <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IT0_9) %>&nbsp;</span>
                    <a href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.UrlHelper.AppendParameter(Workflow.Steps[1].Url, "AlertWizardGuid", Workflow.Id.ToString())) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IT0_10) %></a>
                    <div style="display: none">
                        <asp:Panel ID="affectedObjects" runat="server"></asp:Panel>
                    </div>
                </div>
            </div>
        </asp:Panel>
        </div>
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButton runat="server" ID="imgbBack" OnClick="Back_Click" LocalizedText="Back"
                DisplayType="Secondary" CausesValidation="false" />
            <orion:LocalizableButton runat="server" ID="imgbNext" OnClick="Next_Click" LocalizedText="Submit"
                DisplayType="Primary" />
            <orion:LocalizableButton runat="server" ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel"
                DisplayType="Secondary" CausesValidation="false" />
        </div>
        <script type="text/javascript">
            $(function () {
                $('#<%=showAllAffectedObjects.ClientID %>').click(function (e) {
                    e.preventDefault();
                    Ext.Msg.show({
                        title: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_IT0_1) %>',
                        msg: $('#<%= affectedObjects.ClientID %>').html(),
                        modal: true,
                        closable: false,
                        buttons: { ok: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_253) %>' },
                        icon: Ext.MessageBox.INFO,
                        fn: function () { }
                    });
                });
                SW.Core.MacroVariablePickerController.SetOnInsertHandler(function (variables) {
                    if ($('table.mGrid input.variable-value-tb').length == 1) {
                        var variablesToInsert = '';
                        $.each(variables, function (index, item) {
                            variablesToInsert += item.Value;
                        });
                        var currentText = $('table.mGrid input.variable-value-tb').val();
                        $('table.mGrid input.variable-value-tb').val(currentText + variablesToInsert);
                    } else {
                        __doPostBack("<%=AuditNotificationCell.UniqueID %>", JSON.stringify(variables));
                    }
                });
                SW.Core.MacroVariablePickerController.EnableInsertVariableButtons();
            });
        </script>
    </asp:Panel>
</asp:Content>