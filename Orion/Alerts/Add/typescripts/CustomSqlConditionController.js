﻿/// <reference path="../../../typescripts/typings/jquery.d.ts" />
/// <reference path="../../../typescripts/typings/OrionMinRegs.d.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var SW;
(function (SW) {
    (function (Core) {
        (function (Alerts) {
            (function (Add) {
                var CustomSqlConditionController = (function (_super) {
                    __extends(CustomSqlConditionController, _super);
                    function CustomSqlConditionController(_config) {
                        _super.call(this);
                        this._config = _config;
                    }
                    CustomSqlConditionController.prototype.init = function () {
                        var _this = this;
                        this._sqlData = JSON.parse($('#' + this._config.sqlFragmentId).val());

                        var $sqlEntityTypes = $('#' + this._config.sqlEntityTypesId);

                        // register onchange event
                        $sqlEntityTypes.change(function () {
                            return _this._handleEntityTypeChange();
                        });

                        var jsonValue = $('#' + this._config.chainItemConfigInputId).val();
                        var condition = JSON.parse(jsonValue);

                        // select node by default when there are no data supplied
                        if ($.isEmptyObject(condition)) {
                            $sqlEntityTypes.val('Node');
                        } else {
                            $sqlEntityTypes.val(condition.ObjectType);
                        }

                        $sqlEntityTypes.change();

                        this._conditionController = SW.Core.Alerts.Add.AlertConditionBuilder.instance.getConditionController(this._config.conditionControllerId);
                        this._conditionController.pluginLoaded(this);

                        if (this._config.validateButton) {
                            var self = this;
                            $('#' + this._config.validateButton.ButtonId).click(function () {
                                if (self.validate())
                                    $('#' + self._config.validateButton.MsgPassID).show();
                                else
                                    $('#' + self._config.validateButton.MsgFailID).show();
                                return false;
                            });
                        }
                    };

                    CustomSqlConditionController.prototype.getConditionChainItemConfig = function () {
                        var userSql = $('#' + this._config.userSqlQueryTextId).val();
                        var objectType = $('#' + this._config.sqlEntityTypesId + ' option:selected').val();

                        var result = new CustomSqlChainItemConfig();
                        result.$type = this._config.chainItemConfigType;
                        result.Command = userSql;
                        result.ObjectType = objectType;
                        result.ConditionTypeRefID = 'Core.CustomSql';
                        return result;
                    };

                    CustomSqlConditionController.prototype.reportInvalidInput = function (validationResult) {
                        var $txtUserSql = $('#' + this._config.userSqlQueryTextId);
                        if (this._config.validateButton) {
                            $('#' + this._config.validateButton.MsgPassID).hide();
                            $('#' + this._config.validateButton.MsgFailID).hide();
                        }

                        if (validationResult.IsValid) {
                            $txtUserSql.removeClass('sw-validation-input-error');
                            $txtUserSql.removeAttr('title');
                            return true;
                        }

                        var message = validationResult.ErrorMessages.join('\n');
                        $txtUserSql.addClass('sw-validation-input-error');
                        $txtUserSql.attr('title', message);
                        return false;
                    };

                    CustomSqlConditionController.prototype._handleEntityTypeChange = function () {
                        var sqlQuery = this._sqlData[$('#' + this._config.sqlEntityTypesId).val()];
                        $('#' + this._config.sqlQueryTextId).val(sqlQuery);
                    };
                    return CustomSqlConditionController;
                })(Add.ConditionPluginControllerBase);
                Add.CustomSqlConditionController = CustomSqlConditionController;

                var CustomSqlConditionConfig = (function (_super) {
                    __extends(CustomSqlConditionConfig, _super);
                    function CustomSqlConditionConfig() {
                        _super.apply(this, arguments);
                    }
                    return CustomSqlConditionConfig;
                })(Add.ConditionPluginConfigBase);
                Add.CustomSqlConditionConfig = CustomSqlConditionConfig;

                var CustomSqlChainItemConfig = (function (_super) {
                    __extends(CustomSqlChainItemConfig, _super);
                    function CustomSqlChainItemConfig() {
                        _super.apply(this, arguments);
                    }
                    return CustomSqlChainItemConfig;
                })(Add.ChainItemConfig);
            })(Alerts.Add || (Alerts.Add = {}));
            var Add = Alerts.Add;
        })(Core.Alerts || (Core.Alerts = {}));
        var Alerts = Core.Alerts;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
