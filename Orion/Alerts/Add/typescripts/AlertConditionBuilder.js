﻿/// <reference path="../../../typescripts/typings/jquery.d.ts" />
/// <reference path="../../../typescripts/typings/OrionMinRegs.d.ts" />
/// <reference path="ConditionPluginControllerBase.ts" />
/// <reference path="AlertConditionController.ts" />
var SW;
(function (SW) {
    (function (Core) {
        (function (Alerts) {
            (function (Add) {
                var AlertConditionBuilder = (function () {
                    function AlertConditionBuilder(_config) {
                        this._config = _config;
                        this._shouldMuteError = false;
                        if ($.isEmptyObject(_config))
                            throw '_config is undefined';

                        this._loadedConditions = new Array();
                        this._triggerConditionsContainerIdSelector = '#' + this._config.triggerConditionsContainerId;
                    }
                    //------ PUBLIC METHODS -----------
                    //---------------------------------
                    AlertConditionBuilder.prototype.init = function () {
                        this._setLoadingScreen(true);

                        SW.Core.Alerts.Add.AlertConditionBuilder.instance = this;

                        this._registerEvents();
                    };

                    AlertConditionBuilder.prototype.loadConditions = function () {
                        this._loadedConditions = new Array();
                        this._existingSections = new Array();
                        this._timeIntervals = {};

                        var chainItemConfigs = JSON.parse($('#' + this._config.inputJsonConditionsId).val());

                        if (this._config.expandAdvancedOptions) {
                            this._toggleAdvancedOptions();
                            this._getContainer('ninjaTip').removeClass('hidden');
                            this._getContainer('addButtonContainer').removeClass('hidden');
                        }

                        this._countOfLoadedConditions = chainItemConfigs.length;

                        for (var i = 0; i < chainItemConfigs.length; i++) {
                            this._addCondition(chainItemConfigs[i]);
                        }
                    };

                    AlertConditionBuilder.prototype.reportConditionLoaded = function () {
                        this._countOfLoadedConditions--;

                        if (this._countOfLoadedConditions == 0) {
                            var call = function () {
                                this._setLoadingScreen(false);

                                var muteDisableFunc = function () {
                                    this._shouldMuteError = false;
                                };
                                setTimeout($.proxy(muteDisableFunc, this), 2500);
                            };

                            this._shouldMuteError = true;
                            setTimeout($.proxy(call, this), 2200);
                        }
                    };

                    AlertConditionBuilder.prototype.saveConditions = function (eventObject) {
                        if (typeof eventObject === "undefined") { eventObject = null; }
                        // do not save/validate conditions for invisible builder
                        if (!$(this._triggerConditionsContainerIdSelector).is(":visible")) {
                            return true;
                        }

                        var isValid = true;
                        try  {
                            var dataArray = new Array();

                            for (var i = 0; i < this._existingSections.length; i++) {
                                var controllerId = this._existingSections[i];
                                var controller = this.getConditionController(controllerId);
                                var validationResult = controller.validate();
                                var timeInterval = -1;

                                // validate time intervals
                                if (typeof this._timeIntervals[controllerId] != 'undefined') {
                                    validationResult = validationResult && this._timeIntervals[controllerId].validateTimeValue();
                                    timeInterval = this._timeIntervals[controllerId].getTimeInSeconds();
                                }

                                if (validationResult) {
                                    var chainItemConfig = controller.getData();
                                    var $selectedOption = $('#' + controllerId).prev('div[data-container="conditionOperatorContainer"]').find('select[data-form="conditionOperator"] option:selected');

                                    chainItemConfig.ConjunctionOperator = $selectedOption.length == 1 ? parseInt($selectedOption.attr('value')) : 2; // None is equal to 2

                                    if (timeInterval > 0) {
                                        chainItemConfig.AndThenTimeInSeconds = timeInterval;
                                    }

                                    dataArray.push(chainItemConfig);
                                } else {
                                    isValid = false;
                                }
                            }

                            if (isValid) {
                                // save data to JSON hidden field
                                $('#' + this._config.inputJsonConditionsId).val(JSON.stringify(dataArray));

                                // hide validation message
                                this._getContainer('validationResultsContainer').hide();
                            } else {
                                this._showValidationMessage('@{R=Core.Strings;K=WEBJS_PF0_5; E=js}'); // 'Condition is not valid. Please fix reported errors to continue.'
                            }
                        } catch (ex) {
                            isValid = false;
                            if (this._shouldMuteError) {
                                this._showValidationMessage('@{R=Core.Strings;K=WEBJS_PF0_6; E=js}' + '<br>' + ex); // 'Fatal error while saving data.'
                            }
                        }

                        if (!isValid && !$.isEmptyObject(eventObject)) {
                            eventObject.preventDefault();
                        }

                        return isValid;
                    };

                    AlertConditionBuilder.prototype.conditionLoaded = function (condition) {
                        this._loadedConditions.push(condition);
                        condition.showAdvancedOptions(this._getDataFormInput("ChkNinjaUser").prop('checked'));
                    };

                    AlertConditionBuilder.prototype.hideValidationMessage = function () {
                        var container = this._getContainer('validationResultsContainer');
                        container.hide();
                    };

                    AlertConditionBuilder.prototype.getConditionController = function (controllerId) {
                        for (var i = 0; i < this._loadedConditions.length; i++) {
                            if (this._loadedConditions[i].getId() == controllerId)
                                return this._loadedConditions[i];
                        }
                    };

                    //------ PRIVATE METHODS -----------
                    //---------------------------------
                    AlertConditionBuilder.prototype._addCondition = function (chainItemConfig) {
                        var _this = this;
                        var conditionTargetId = this._getNewSectionId();

                        this._existingSections.push(conditionTargetId);

                        // create a new div for each user control to have them in order
                        var $container = $('<div id="' + conditionTargetId + '" class="abc-conditionContainer"></div>');
                        if (this._config.expandAdvancedOptions || this._existingSections.length > 1) {
                            $container.addClass('withBorder');
                        }

                        if (this._existingSections.length <= 1) {
                            $container.addClass('primarySection');
                        } else {
                            $container.addClass('secondarySection');
                        }

                        $container.appendTo(this._triggerConditionsContainerIdSelector);

                        var titleText = this._getConditionSectionTitle(this._existingSections.length);
                        var $titleBar = $('<div class="abc-conditionTitleBar"><h2>' + titleText + '</h2></div>');
                        $container.append($titleBar);

                        if (this._existingSections.length > 1) {
                            // add operator between two conditions
                            var $conditionOperatorContainer = this._getContainer('conditionOperatorContainer', '.hidden').clone().removeClass('hidden');

                            // add time interval
                            var timeInterval = this._createTimeInterval();
                            this._timeIntervals[conditionTargetId] = timeInterval;
                            var $timeIntervalContainer = $conditionOperatorContainer.find('span[data-container="timeIntervalContainer"]');
                            $timeIntervalContainer.append(timeInterval.getJQueryElement());

                            var $conditionOperatorSelect = $conditionOperatorContainer.find('select[data-form="conditionOperator"]');
                            $conditionOperatorSelect.change(function (e) {
                                return _this._conditionOperatorSelectChange($conditionOperatorSelect, $timeIntervalContainer, timeInterval);
                            });
                            if (!$.isEmptyObject(chainItemConfig)) {
                                $conditionOperatorSelect.find('option[value="' + chainItemConfig.ConjunctionOperator + '"]').attr('selected', 'true'); // select operator
                                $conditionOperatorSelect.trigger('change');
                                if (chainItemConfig.AndThenTimeInSeconds > 0) {
                                    timeInterval.setTimeInterval(chainItemConfig.AndThenTimeInSeconds);
                                }
                            }
                            $('#' + conditionTargetId).before($conditionOperatorContainer);

                            // add remove section button
                            var $closeImage = $('<img src = "/Orion/images/delete_icon_blue_16x16.png" class="abc-removeButton">');
                            $closeImage.click((function (condTgtId) {
                                return function () {
                                    _this._removeCondition(condTgtId);
                                };
                            })(conditionTargetId));
                            $titleBar.append($closeImage);
                        }

                        var screenOnCallback = function () {
                            _this._setLoadingScreen(false);
                        };

                        SW.Core.Loader.Control('#' + conditionTargetId, { Control: '/Orion/Alerts/Add/Controls/AlertConditionView.ascx' }, {
                            'config': {
                                'ChainItemConfigJson': JSON.stringify(chainItemConfig),
                                'ClientIDPrefix': conditionTargetId
                            }
                        }, 'append', function () {
                            alert('Error while loading trigger condition.');
                        }, null);
                    };

                    AlertConditionBuilder.prototype._getConditionSectionTitle = function (index) {
                        return (index == 1) ? '@{R=Core.Strings;K=Alerts_Trigger_Condition_Primary_Section_Title; E=js} <img src="/Orion/images/Info_Icon_MoreDecent_16x16.png" alt="" style="vertical-align: text-top;" title="@{R=Core.Strings;K=Alerts_Trigger_Condition_Primary_Section_Tooltip; E=js}"/>' : '@{R=Core.Strings;K=Alerts_Trigger_Condition_Secondary_Section_Title; E=js} <img src="/Orion/images/Info_Icon_MoreDecent_16x16.png" alt="" style="vertical-align: text-top;" title="@{R=Core.Strings;K=Alerts_Trigger_Condition_Secondary_Section_Tooltip; E=js}"/>';
                    };

                    AlertConditionBuilder.prototype._removeCondition = function (conditionTargetId) {
                        for (var i = 0; i < this._existingSections.length; i++) {
                            if (this._existingSections[i] == conditionTargetId)
                                break;
                        }
                        this._existingSections.splice(i, 1);

                        // remove time interval
                        if (typeof this._timeIntervals[conditionTargetId] != 'undefined') {
                            this._timeIntervals[conditionTargetId] = undefined;
                        }
                        var $container = $('#' + conditionTargetId);

                        // remove operator and container
                        $container.prev('div[data-container="conditionOperatorContainer"]').remove();
                        $container.remove();
                    };

                    AlertConditionBuilder.prototype._getNewSectionId = function () {
                        var lastIndex = -1;

                        if (this._existingSections.length > 0) {
                            var lastId = this._existingSections[this._existingSections.length - 1];
                            lastIndex = parseInt(lastId.substr(lastId.lastIndexOf('-') + 1));
                        }

                        return this._config.triggerConditionsContainerId + '-' + (lastIndex + 1);
                    };

                    AlertConditionBuilder.prototype._setLoadingScreen = function (show) {
                        var _this = this;
                        if (show == true) {
                            this._overlay = $('<div class="ui-overlay"><div class="ui-widget-overlay"></div></div>').hide().appendTo('body');
                            this._overlay.fadeIn();

                            $(window).resize(function () {
                                _this._overlay.width($(document).width());
                                _this._overlay.height($(document).height());
                                $('#waitmodal').center();
                            });

                            $('#waitmodal').center();
                            $("#waitmodal").show();
                        } else {
                            this._overlay.remove();
                            $("#waitmodal").hide();
                        }
                    };

                    AlertConditionBuilder.prototype._toggleAdvancedOptions = function () {
                        var $container = this._getContainer('advancedOptionsContainer');
                        var $button = this._getAdvancedOptionsButton();

                        $container.toggleClass('hidden');
                        if ($container.hasClass('hidden')) {
                            $button.attr({ src: '/Orion/images/Button.Expand.gif' });
                        } else {
                            $button.attr({ src: '/Orion/images/Button.Collapse.gif' });
                        }
                    };

                    AlertConditionBuilder.prototype._registerEvents = function () {
                        var _this = this;
                        this._getAdvancedOptionsButton().click(function () {
                            return _this._toggleAdvancedOptions();
                        });
                        this._getDataFormInput("ChkNinjaUser").click(function () {
                            return _this._ninjaChanged();
                        });
                        this._getDataFormButton('addButton').click((function () {
                            return function () {
                                return _this._addCondition(null);
                            };
                        })());

                        // dialog buttons click events
                        this._getContainer('disablePoweUserDialog').find('[data-form="btnNinjaDisable"]').click(function () {
                            return _this._btnNinjaDisableClick();
                        });
                        this._getContainer('disablePoweUserDialog').find('[data-form="btnNinjaCancel"]').click(function () {
                            return _this._btnNinjaCancelClick();
                        });

                        // import/export
                        this._getDataFormButton('btnExportCondition').click(function (eventObject) {
                            return _this._exportCondition(eventObject);
                        });

                        for (var i = 0; i < this._config.saveButtonIds.length; i++) {
                            $('#' + this._config.saveButtonIds[i]).click(function (eventObject) {
                                return _this.saveConditions(eventObject);
                            });
                        }
                    };

                    AlertConditionBuilder.prototype._getAdvancedOptionsButton = function () {
                        var element = $('#' + this._config.conditionBuilderContainerId + ' img[data-form="AdvancedOptionsButton"]');
                        return element;
                    };

                    AlertConditionBuilder.prototype._getDataFormInput = function (dataFormName) {
                        var element = $('#' + this._config.conditionBuilderContainerId + ' input[data-form="' + dataFormName + '"]');
                        return element;
                    };

                    AlertConditionBuilder.prototype._getContainer = function (containerName, additionalSelector) {
                        if (typeof additionalSelector === "undefined") { additionalSelector = ''; }
                        var element = $('#' + this._config.conditionBuilderContainerId + ' div[data-container="' + containerName + '"]' + additionalSelector);
                        return element;
                    };

                    AlertConditionBuilder.prototype._getDataFormButton = function (dataFormName) {
                        var element = $('#' + this._config.conditionBuilderContainerId + ' a[data-form="' + dataFormName + '"]');
                        return element;
                    };

                    AlertConditionBuilder.prototype._ninjaChanged = function () {
                        var checked = this._getDataFormInput("ChkNinjaUser").prop('checked');

                        // show dialog if ninja is unchecked and there at least two conditions
                        if (!checked && this._existingSections.length > 1) {
                            var $ninjadialog = $('div[data-container="disablePoweUserDialog"]');
                            $ninjadialog.dialog({
                                width: 600,
                                modal: true,
                                open: function () {
                                    $ninjadialog.find('[data-form="btnNinjaDisable"]').focus();
                                }
                            });
                        } else {
                            this._toggleNinjaOptions(checked);
                        }
                    };

                    AlertConditionBuilder.prototype._toggleNinjaOptions = function (checked) {
                        this._getContainer('ninjaTip').toggleClass('hidden', !checked);
                        this._getContainer('addButtonContainer').toggleClass('hidden', !checked);

                        // change style of sections
                        this._addRemoveBorderToSections(checked);

                        // notify listeners
                        $(this).trigger('AlertConditionBuilder.ChkNinjaUserChanged', [checked]);
                    };

                    AlertConditionBuilder.prototype._btnNinjaDisableClick = function () {
                        for (var i = 1; i < this._existingSections.length;) {
                            this._removeCondition(this._existingSections[i]);
                        }

                        this._toggleNinjaOptions(false);
                        $('div[data-container="disablePoweUserDialog"]').dialog('close');
                    };

                    AlertConditionBuilder.prototype._btnNinjaCancelClick = function () {
                        this._getDataFormInput("ChkNinjaUser").prop('checked', true);
                        $('div[data-container="disablePoweUserDialog"]').dialog('close');
                    };

                    AlertConditionBuilder.prototype._showValidationMessage = function (message) {
                        var container = this._getContainer('validationResultsContainer');
                        container.find('span[data-message="resultMessage"]').html(message);
                        container.show();
                    };

                    AlertConditionBuilder.prototype._addRemoveBorderToSections = function (add) {
                        for (var i = 0; i < this._existingSections.length; i++) {
                            var $section = $('#' + this._existingSections[i]);
                            if (add) {
                                if (i == 0) {
                                    if (!$section.hasClass("primarySection")) {
                                        $section.addClass("primarySection");
                                    }
                                }
                                $section.addClass('withBorder');
                            } else {
                                $section.removeClass('withBorder');
                            }
                        }
                    };

                    AlertConditionBuilder.prototype._exportCondition = function (eventObject) {
                        this.saveConditions(eventObject);

                        if (!eventObject.isDefaultPrevented()) {
                            var json = $('#' + this._config.inputJsonConditionsId).val();
                            var filename = SW.Core.String.Format('{0}.xml', this._config.exportFileName);
                            SW.Core.Services.downloadFileAsync('/api/AlertDefinitionData/ExportAlertConditions', JSON.stringify({ ChainItemConfigsJson: json, FileName: filename }), filename);
                        }
                    };

                    AlertConditionBuilder.prototype._createTimeInterval = function () {
                        var config = {
                            onlyClientMode: true,
                            minTimeInSeconds: 1,
                            minTimeUnit: 'seconds',
                            maxTimeInSeconds: 108 * 60 * 60,
                            defaultTimeKindIndex: 1
                        };

                        var timeInterval = new SW.Core.Controls.AlertTimeIntervals(config);
                        timeInterval.enable(false);

                        return timeInterval;
                    };

                    AlertConditionBuilder.prototype._conditionOperatorSelectChange = function ($conditionOperatorSelect, $timeIntervalContainer, timeInterval) {
                        // AndThen operator
                        if ($conditionOperatorSelect.find('option:selected').val() == '1') {
                            $timeIntervalContainer.removeClass('hidden');
                            timeInterval.enable(true);
                        } else {
                            $timeIntervalContainer.addClass('hidden');
                            timeInterval.enable(false);
                        }
                    };
                    return AlertConditionBuilder;
                })();
                Add.AlertConditionBuilder = AlertConditionBuilder;

                var AlertConditionBuilderConfig = (function () {
                    function AlertConditionBuilderConfig() {
                    }
                    return AlertConditionBuilderConfig;
                })();
                Add.AlertConditionBuilderConfig = AlertConditionBuilderConfig;
            })(Alerts.Add || (Alerts.Add = {}));
            var Add = Alerts.Add;
        })(Core.Alerts || (Core.Alerts = {}));
        var Alerts = Core.Alerts;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
