﻿/// <reference path="../../../typescripts/typings/jquery.d.ts" />
/// <reference path="../../../typescripts/typings/OrionMinRegs.d.ts" />
var SW;
(function (SW) {
    (function (Core) {
        (function (Alerts) {
            (function (Add) {
                var ConditionPluginControllerBase = (function () {
                    function ConditionPluginControllerBase() {
                        SW.Core.Alerts.Add.AlertConditionBuilder.instance.reportConditionLoaded();
                    }
                    ConditionPluginControllerBase.prototype.init = function () {
                    };
                    ConditionPluginControllerBase.prototype.getConditionChainItemConfig = function () {
                        return null;
                    };
                    ConditionPluginControllerBase.prototype.reportInvalidInput = function (validationResult) {
                        return true;
                    };
                    ConditionPluginControllerBase.prototype.validateOnClient = function () {
                        return true;
                    };
                    ConditionPluginControllerBase.prototype.conditionMustExistWasChanged = function (checked, time, unit) {
                    };
                    ConditionPluginControllerBase.prototype.waitUntilXNetObjectWasChanged = function (checked, count, operator) {
                    };

                    ConditionPluginControllerBase.prototype.validate = function () {
                        var _this = this;
                        if (!this.validateOnClient()) {
                            return false;
                        }

                        var chainItemConfigJson = JSON.stringify(this.getConditionChainItemConfig());
                        var result = true;

                        SW.Core.Services.callControllerSync("/api/AlertDefinitionData/ValidateAlertCondition", chainItemConfigJson, // On success
                        function (validationResult) {
                            return result = _this.reportInvalidInput(validationResult);
                        }, // On error
                        function (response) {
                            return result = _this.reportInvalidInput(new ValidationResult(false, ['Fatal error while validating alert condition.', response]));
                        });
                        return result;
                    };

                    ConditionPluginControllerBase.prototype.showInvalidInput = function ($element, show, message) {
                        if (typeof message === "undefined") { message = ''; }
                        if (show) {
                            $element.addClass('sw-validation-input-error');
                            $element.attr('title', message);
                        } else {
                            $element.removeClass('sw-validation-input-error');
                            $element.removeAttr('title');
                        }
                    };
                    return ConditionPluginControllerBase;
                })();
                Add.ConditionPluginControllerBase = ConditionPluginControllerBase;

                var ConditionPluginConfigBase = (function () {
                    function ConditionPluginConfigBase() {
                    }
                    return ConditionPluginConfigBase;
                })();
                Add.ConditionPluginConfigBase = ConditionPluginConfigBase;

                // Corresponds to C# class SolarWinds.Orion.Web.Alerts.TriggerCondition.ChainItemConfig
                var ChainItemConfig = (function () {
                    function ChainItemConfig() {
                    }
                    return ChainItemConfig;
                })();
                Add.ChainItemConfig = ChainItemConfig;

                // Corresponds to C# class SolarWinds.Orion.Core.Models.ValidationResult
                var ValidationResult = (function () {
                    function ValidationResult(IsValid, ErrorMessages) {
                        this.IsValid = IsValid;
                        this.ErrorMessages = ErrorMessages;
                    }
                    return ValidationResult;
                })();
                Add.ValidationResult = ValidationResult;
            })(Alerts.Add || (Alerts.Add = {}));
            var Add = Alerts.Add;
        })(Core.Alerts || (Core.Alerts = {}));
        var Alerts = Core.Alerts;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
