﻿/// <reference path="../../../typescripts/typings/jquery.d.ts" />
/// <reference path="../../../typescripts/typings/OrionMinRegs.d.ts" />
/// <reference path="ConditionPluginControllerBase.ts" />
/// <reference path="AlertConditionBuilder.ts" />
var SW;
(function (SW) {
    (function (Core) {
        (function (Alerts) {
            (function (Add) {
                var AlertConditionController = (function () {
                    function AlertConditionController(_config) {
                        this._config = _config;
                    }
                    //------ PUBLIC METHODS -----------
                    //---------------------------------
                    AlertConditionController.prototype.init = function () {
                        var _this = this;
                        var chainItemConfig = JSON.parse($('#' + this._config.chainItemConfigInputId).val());

                        var loadCallback = function () {
                            _this._loadData(chainItemConfig);
                        };

                        // IE workaround to be able set inputs/selects properly
                        setTimeout(loadCallback, 0);

                        this._createAlertTypesCombobox();
                        this._loadConditionEditor();
                        this._registerEvents();

                        SW.Core.Alerts.Add.AlertConditionBuilder.instance.conditionLoaded(this);
                    };

                    AlertConditionController.prototype.validate = function () {
                        var result = this._plugin.validate();

                        if (this._getDataFormInput('ChkWaitUntil').is(':checked')) {
                            result = this._validateNetObjectsMinCount() && result;
                        }

                        if (this._getDataFormInput('chkConditionPeriod').is(':checked')) {
                            result = this._validateTimeValue() && result;
                        }

                        return result;
                    };

                    AlertConditionController.prototype.getData = function () {
                        var pluginData = this._plugin.getConditionChainItemConfig();

                        if (this._getDataFormInput('ChkWaitUntil').is(':checked')) {
                            pluginData.NetObjectsMinCountThreshold = this._getDataFormInput('NetObjectsMinCountThreshold').val();
                            pluginData.IsInvertedMinCountThreshold = parseInt(this._getDataFormSelect('netObjectsMinCountThresholdType', 'option:selected').val()) == 1;
                        }

                        if (this._getDataFormInput('chkConditionPeriod').is(':checked')) {
                            var timeValue = parseInt(this._getDataFormInput('periodTimeValue').val(), 10);
                            var timeKind = parseInt(this._getDataFormSelect('periodTimeKind', 'option:selected').attr('value'), 10);

                            pluginData.SustainTimeInSeconds = timeValue * timeKind;
                        }

                        return pluginData;
                    };

                    AlertConditionController.prototype.pluginLoaded = function (plugin) {
                        this._plugin = plugin;

                        var $txtTimeValue = this._getDataFormInput('periodTimeValue');
                        var $timeKind = this._getDataFormSelect('periodTimeKind');
                        var $chck = this._getDataFormInput('chkConditionPeriod');
                        var $waitUntilType = this._getDataFormSelect('netObjectsMinCountThresholdType');

                        this._plugin.conditionMustExistWasChanged($chck.is(':checked'), parseInt($txtTimeValue.val()), $timeKind.find('option:selected').text());
                        this._plugin.waitUntilXNetObjectWasChanged(this._getDataFormInput('ChkWaitUntil').is(':checked'), parseInt(this._getDataFormInput('NetObjectsMinCountThreshold').val()), $waitUntilType.find('option:selected').text());
                    };

                    AlertConditionController.prototype.getId = function () {
                        return this._config.clientIDPrefix;
                    };

                    AlertConditionController.prototype.showAdvancedOptions = function (show) {
                        this._ninjaUserChanged(show);
                    };

                    AlertConditionController.prototype.enableConditionPeriodChange = function (enable) {
                        var $conditionPeriodDiv = $("#" + this._config.conditionViewContainerId + " .acv-additionalOptions");
                        if (enable) {
                            $conditionPeriodDiv.show();
                        } else {
                            $conditionPeriodDiv.hide();
                        }

                        var $cpSpan = this._getDataFormSpan('conditionPeriodSpan');
                        var $chck = this._getDataFormInput('chkConditionPeriod');
                        if (enable && $cpSpan.hasClass('disabledSpan')) {
                            $cpSpan.removeClass('disabledSpan');
                            $chck.prop('disabled', false);
                        } else if (!enable && !$cpSpan.hasClass('disabledSpan')) {
                            $cpSpan.addClass('disabledSpan');
                            $chck.prop('checked', false);
                            $chck.prop('disabled', true);
                            this._enableChkConditionPeriodChange(false);
                        }
                    };

                    //------ PRIVATE METHODS -----------
                    //---------------------------------
                    AlertConditionController.prototype._sortAlertTypesComboboxData = function (alertTypesComboboxData) {
                        var sortedAlertTypesComboboxData = [];
                        var index;

                        for (index = 0; index < alertTypesComboboxData.length; ++index) {
                            if (alertTypesComboboxData[index].Recommended) {
                                sortedAlertTypesComboboxData.push(alertTypesComboboxData[index]);
                            }
                            ;
                        }

                        for (index = 0; index < alertTypesComboboxData.length; ++index) {
                            if (!alertTypesComboboxData[index].Recommended) {
                                sortedAlertTypesComboboxData.push(alertTypesComboboxData[index]);
                            }
                            ;
                        }

                        return sortedAlertTypesComboboxData;
                    };

                    AlertConditionController.prototype._createAlertTypesCombobox = function () {
                        var alertTypesComboboxData = JSON.parse($('#' + this._config.jsonInputAlertTypesId).val());
                        alertTypesComboboxData = this._sortAlertTypesComboboxData(alertTypesComboboxData);

                        var alertTypesComboboxStore = new Ext.data.ArrayStore({
                            fields: [
                                { name: 'DisplayName', mapping: 'DisplayName', type: 'string' },
                                { name: 'ObjectType', mapping: 'ObjectType', type: 'string' },
                                { name: 'EntityFullName', mapping: 'EntityFullName', type: 'string' },
                                { name: 'ViewPath', mapping: 'ViewPath', type: 'string' },
                                { name: 'TypeRefId', mapping: 'TypeRefId', type: 'string' },
                                { name: 'Recommended', mapping: 'Recommended', type: 'bool' }
                            ],
                            data: alertTypesComboboxData
                        });

                        var alertTypeComboBoxTemplate = new Ext.XTemplate('<tpl for=".">', '    <tpl if="Recommended == true">', '        <tpl exec="this.fundamentalCount = this.fundamentalCount + 1;" ></tpl>', '    </tpl>', '    <tpl if="Recommended != true">', '        <tpl exec="this.otherCount = this.otherCount + 1;" ></tpl>', '    </tpl>', '</tpl>', '<div class="x-combo-group-item x-combo-group-item-first">@{R=Core.Strings.2;K=WEBJS_PCO_006;E=js} ({[this.fundamentalCount]})</div>', '<tpl for=".">', '    <tpl if="Recommended == true"><div class="x-boundlist-item x-combo-list-item">{DisplayName}</div></tpl>', '</tpl>', '<div class="x-combo-group-item">@{R=Core.Strings.2;K=WEBJS_PCO_007;E=js} ({[this.otherCount]})</div>', '<tpl for=".">', '    <tpl if="Recommended != true">', '        <div class="x-boundlist-item x-combo-list-item">{DisplayName}</div>', '    </tpl>', '</tpl>', {
                            compiled: true,
                            disableFormats: true,
                            fundamentalCount: 0,
                            otherCount: 0
                        });

                        this._alertTypeComboBox = new Ext.form.ComboBox({
                            mode: 'local',
                            store: alertTypesComboboxStore,
                            renderTo: this._config.alertTypesComboBoxId,
                            displayField: 'DisplayName',
                            valueField: 'ObjectType',
                            forceSelection: true,
                            editable: false,
                            typeAhead: true,
                            triggerAction: 'all',
                            tpl: alertTypeComboBoxTemplate,
                            itemSelector: 'div.x-boundlist-item',
                            width: 300
                        });

                        this._alertTypeComboBox.setValue(this._config.alertTypesComboBoxInitValue);
                    };

                    AlertConditionController.prototype._alertTypeListChangedHandler = function () {
                        // Do nothing if the selection was unchanged.
                        if (this._alertTypeComboBox.getValue() == this._alertTypeComboBox.dataPreviousValue) {
                            return;
                        }

                        // If nothing else was changed, don't ask the user for confirmation.
                        var obj = this;
                        if (this._getConditionViewContainer().data("isConditionChanged") != true) {
                            // Update the previous value to the new selection and refresh the conditions.
                            SW.Core.Alerts.Add.AlertConditionBuilder.instance.hideValidationMessage();
                            obj._alertTypeComboBox.dataPreviousValue = obj._alertTypeComboBox.getValue();
                            obj._loadConditionEditor(true);
                            return;
                        }

                        // Confirm before clearing out the existing conditions.
                        Ext.Msg.show({
                            title: '@{R=Core.Strings;K=AlertConditionController_Scope_Change_Confirm_Title; E=js}',
                            msg: '@{R=Core.Strings;K=AlertConditionController_Scope_Change_Confirm_Message; E=js}',
                            buttons: { yes: '@{R=Core.Strings;K=AlertConditionController_Scope_Change_Confirm_Button; E=js}', cancel: '@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}' },
                            icon: Ext.Msg.QUESTION,
                            fn: function (btn) {
                                if (btn == 'yes') {
                                    // Update the previous value to the new selection and refresh the conditions.
                                    SW.Core.Alerts.Add.AlertConditionBuilder.instance.hideValidationMessage();
                                    obj._alertTypeComboBox.dataPreviousValue = obj._alertTypeComboBox.getValue();
                                    obj._getConditionViewContainer().data("isConditionChanged", false);
                                    obj._loadConditionEditor(true);
                                } else {
                                    // Re-select the previous value in the combobox.
                                    obj._alertTypeComboBox.setValue(obj._alertTypeComboBox.dataPreviousValue);
                                }
                            }
                        });
                    };

                    AlertConditionController.prototype._handleFormChange = function (eventObject) {
                        this._getConditionViewContainer().data("isConditionChanged", true);
                    };

                    AlertConditionController.prototype._loadConditionEditor = function (clearChainItemConfig) {
                        if (typeof clearChainItemConfig === "undefined") { clearChainItemConfig = false; }
                        var alertTypeStore = this._alertTypeComboBox.getStore();
                        var alertTypeRecord = alertTypeStore.getAt(alertTypeStore.findExact('ObjectType', this._alertTypeComboBox.getValue()));

                        var chainItemConfig = new Add.ChainItemConfig();
                        if (!clearChainItemConfig) {
                            chainItemConfig = JSON.parse($('#' + this._config.chainItemConfigInputId).val()) || new Add.ChainItemConfig();
                        }

                        // only dynamic types has ObjectType and EntityFullName in the main drop down
                        if (alertTypeRecord.get('TypeRefId') == 'Core.Dynamic') {
                            chainItemConfig.ObjectType = alertTypeRecord.get('ObjectType');
                            chainItemConfig.EntityFullName = alertTypeRecord.get('EntityFullName');
                        }
                        var chainItemConfigJson = JSON.stringify(chainItemConfig);
                        $('#' + this._config.chainItemConfigInputId).val(chainItemConfigJson);

                        var controlPath = alertTypeRecord.get('ViewPath');
                        $('#' + this._config.conditionContentId).empty();

                        SW.Core.Loader.Control('#' + this._config.conditionContentId, { Control: controlPath }, {
                            'config': {
                                'ChainItemConfigJson': chainItemConfigJson,
                                'ClientIDPrefix': this._config.clientIDPrefix,
                                'ConditionControllerId': this.getId(),
                                'JsonInputChainItemConfigId': this._config.chainItemConfigInputId
                            }
                        }, 'append', function () {
                            alert('Error while loading trigger condition plugin.');
                        });
                    };

                    AlertConditionController.prototype._registerEvents = function () {
                        var _this = this;
                        this._alertTypeComboBox.dataPreviousValue = this._alertTypeComboBox.getValue();
                        this._getConditionViewContainer().on('change', function (e) {
                            return _this._handleFormChange(e);
                        });
                        this._alertTypeComboBox.on('select', function (e) {
                            return _this._alertTypeListChangedHandler();
                        });
                        this._getDataFormInput('ChkWaitUntil').change(function (eventObject) {
                            return _this._chkWaitUntilChange(eventObject);
                        });
                        this._getDataFormInput('chkConditionPeriod').change(function (eventObject) {
                            return _this._chkConditionPeriodChange(eventObject);
                        });
                        $(SW.Core.Alerts.Add.AlertConditionBuilder.instance).on('AlertConditionBuilder.ChkNinjaUserChanged', function (e, data) {
                            return _this._ninjaUserChanged(data);
                        });

                        // events which triggers validations
                        this._getDataFormInput('NetObjectsMinCountThreshold').change(function (e) {
                            return _this._validateNetObjectsMinCount();
                        }).keyup(function (e) {
                            _this._validateNetObjectsMinCount();
                            return true;
                        });

                        this._getDataFormInput('periodTimeValue').change(function (e) {
                            return _this._validateTimeValue();
                        }).keyup(function (e) {
                            _this._validateTimeValue();
                            return true;
                        });
                        this._getDataFormSelect('periodTimeKind').change(function (e) {
                            return _this._validateTimeValue();
                        });

                        this._getDataFormSelect('netObjectsMinCountThresholdType').change(function (e) {
                            return _this._validateNetObjectsMinCount();
                        });
                    };

                    AlertConditionController.prototype._loadData = function (chainItemConfig) {
                        if ($.isEmptyObject(chainItemConfig))
                            return;

                        var chckWaitUntilComp = this._getDataFormInput('ChkWaitUntil');

                        var isPrimarySection = chckWaitUntilComp.parents('.primarySection').length != 0;

                        var $txtNetObjectsCount = this._getDataFormInput('NetObjectsMinCountThreshold');
                        var $netObjectCountType = this._getDataFormSelect('netObjectsMinCountThresholdType');

                        if (isPrimarySection) {
                            this._getDataFormSpan("netObjectMinCountThresholdAdditionalText").text('@{R=Core.Strings.2;K=WEBJS_TK0_19; E=js}');
                        }

                        if ($.isNumeric(chainItemConfig.NetObjectsMinCountThreshold) && chainItemConfig.NetObjectsMinCountThreshold > 0) {
                            if (isPrimarySection) {
                                $netObjectCountType.prop('disabled', true);
                            } else {
                                $netObjectCountType.prop('disabled', false);
                            }

                            if (chainItemConfig.IsInvertedMinCountThreshold && !isPrimarySection) {
                                $netObjectCountType.children('option[value="1"]').attr('selected', true);
                            } else {
                                $netObjectCountType.children('option[value="0"]').attr('selected', true);
                            }

                            $txtNetObjectsCount.val(chainItemConfig.NetObjectsMinCountThreshold.toString());
                            $txtNetObjectsCount.prop('disabled', false);
                            chckWaitUntilComp.prop('checked', true);
                        } else {
                            $txtNetObjectsCount.prop('disabled', true);
                            $netObjectCountType.prop('disabled', true);
                        }

                        // condition period data
                        var $txtTimeValue = this._getDataFormInput('periodTimeValue');
                        var $timeKind = this._getDataFormSelect('periodTimeKind');
                        var conditionPeriodEnabled = $.isNumeric(chainItemConfig.SustainTimeInSeconds) && chainItemConfig.SustainTimeInSeconds > 0;

                        if (conditionPeriodEnabled) {
                            if (chainItemConfig.SustainTimeInSeconds % 3600 == 0) {
                                $timeKind.children('option[value="3600"]').attr('selected', true);
                                $txtTimeValue.val((chainItemConfig.SustainTimeInSeconds / 3600).toString());
                            } else if (chainItemConfig.SustainTimeInSeconds % 60 == 0) {
                                $timeKind.children('option[value="60"]').attr('selected', true);
                                $txtTimeValue.val((chainItemConfig.SustainTimeInSeconds / 60).toString());
                            } else {
                                $timeKind.children('option[value="1"]').attr('selected', true);
                                $txtTimeValue.val(chainItemConfig.SustainTimeInSeconds.toString());
                            }
                            this._getDataFormInput('chkConditionPeriod').prop('checked', false);
                            this._getDataFormInput('chkConditionPeriod').click();
                        }
                        $txtTimeValue.prop('disabled', !conditionPeriodEnabled);
                        $timeKind.prop('disabled', !conditionPeriodEnabled);
                    };

                    AlertConditionController.prototype._chkWaitUntilChange = function (eventObject) {
                        var checked = $(eventObject.target).is(':checked');
                        this._enableChkWaitUntiChange(checked);

                        if (this._plugin) {
                            var $waitUntilType = this._getDataFormSelect('netObjectsMinCountThresholdType');
                            this._plugin.waitUntilXNetObjectWasChanged(checked, parseInt(this._getDataFormInput('NetObjectsMinCountThreshold').val(), 10), $waitUntilType.find('option:selected').text());
                        }
                    };

                    AlertConditionController.prototype._enableChkWaitUntiChange = function (enable) {
                        var $txtNetObjectsCount = this._getDataFormInput('NetObjectsMinCountThreshold');
                        var $waitUntilType = this._getDataFormSelect('netObjectsMinCountThresholdType');
                        var isPrimarySection = this._getDataFormInput('ChkWaitUntil').parents('.primarySection').length != 0;

                        if (enable) {
                            if (!isPrimarySection) {
                                $waitUntilType.prop('disabled', false);
                            }
                            $txtNetObjectsCount.prop('disabled', false);
                            $txtNetObjectsCount.focus();
                            if ($txtNetObjectsCount.val() == '') {
                                $txtNetObjectsCount.val('5');
                            }
                        } else {
                            $waitUntilType.prop('disabled', true);
                            $txtNetObjectsCount.prop('disabled', true);
                            $txtNetObjectsCount.val('');
                            this._showInvalidInput($txtNetObjectsCount, false);
                        }
                    };

                    AlertConditionController.prototype._chkConditionPeriodChange = function (eventObject) {
                        var checked = $(eventObject.target).is(':checked');
                        this._enableChkConditionPeriodChange(checked);
                    };

                    AlertConditionController.prototype._enableChkConditionPeriodChange = function (enable) {
                        var $txtTimeValue = this._getDataFormInput('periodTimeValue');
                        var $timeKind = this._getDataFormSelect('periodTimeKind');

                        if (enable) {
                            $txtTimeValue.prop('disabled', false);
                            $txtTimeValue.focus();
                            if ($txtTimeValue.val() == '') {
                                $txtTimeValue.val('10');
                            }
                            $timeKind.prop('disabled', false);
                            if (this._plugin) {
                                this._plugin.conditionMustExistWasChanged(true, parseInt($txtTimeValue.val()), $timeKind.find('option:selected').text());
                            }
                        } else {
                            $txtTimeValue.prop('disabled', true);
                            $txtTimeValue.val('');
                            $timeKind.prop('disabled', true);
                            this._showInvalidInput($txtTimeValue, false);

                            if (this._plugin) {
                                this._plugin.conditionMustExistWasChanged(false, null, null);
                            }
                        }
                    };

                    AlertConditionController.prototype._ninjaUserChanged = function (checked) {
                        var $advancedContainer = $('#' + this._config.advancedOptionsContainerId);

                        if (checked) {
                            $advancedContainer.removeClass('hidden');
                        } else {
                            $advancedContainer.addClass('hidden');
                            this._getDataFormInput('ChkWaitUntil').prop('checked', false);
                            this._enableChkWaitUntiChange(false);
                        }
                    };

                    AlertConditionController.prototype._getConditionViewContainer = function () {
                        var element = $('#' + this._config.conditionViewContainerId);
                        return element;
                    };

                    AlertConditionController.prototype._getDataFormInput = function (dataFormName) {
                        var element = $('#' + this._config.conditionViewContainerId + ' input[data-form="' + dataFormName + '"]');
                        return element;
                    };

                    AlertConditionController.prototype._getDataFormSpan = function (dataFormName) {
                        var element = $('#' + this._config.conditionViewContainerId + ' span[data-form="' + dataFormName + '"]');
                        return element;
                    };

                    AlertConditionController.prototype._getDataFormSelect = function (dataFormName, additionalSelector) {
                        if (typeof additionalSelector === "undefined") { additionalSelector = ''; }
                        var element = $('#' + this._config.conditionViewContainerId + ' select[data-form="' + dataFormName + '"] ' + additionalSelector);
                        return element;
                    };

                    //------ VALIDATIONS --------------
                    //---------------------------------
                    AlertConditionController.prototype._showInvalidInput = function ($element, show, message) {
                        if (typeof message === "undefined") { message = ''; }
                        if (show) {
                            $element.addClass('sw-validation-input-error');
                            $element.attr('title', message);
                        } else {
                            $element.removeClass('sw-validation-input-error');
                            $element.removeAttr('title');
                        }
                    };

                    AlertConditionController.prototype._validateNetObjectsMinCount = function () {
                        var result = true;
                        var $txtNetObjectsMinCount = this._getDataFormInput('NetObjectsMinCountThreshold');
                        var $waitUntilType = this._getDataFormSelect('netObjectsMinCountThresholdType');
                        var minCount = $txtNetObjectsMinCount.val();
                        var minCountInt = parseInt(minCount, 10);

                        // check for integer > 0
                        if (minCountInt.toString() !== minCount || minCountInt <= 0 || $.trim(minCount) === '') {
                            this._showInvalidInput($txtNetObjectsMinCount, true, $txtNetObjectsMinCount.data('val-message'));
                            result = false;
                        } else {
                            this._plugin.waitUntilXNetObjectWasChanged(true, minCountInt, $waitUntilType.find('option:selected').text());
                            this._showInvalidInput($txtNetObjectsMinCount, false);
                        }

                        return result;
                    };

                    AlertConditionController.prototype._validateTimeValue = function () {
                        var result = true;
                        var $txtTimeValue = this._getDataFormInput('periodTimeValue');
                        var timeValue = $txtTimeValue.val();
                        var timeValueInt = parseInt(timeValue);
                        var $timeKind = this._getDataFormSelect('periodTimeKind', 'option:selected');

                        // check for integer > 0
                        if (timeValueInt.toString() !== timeValue || timeValueInt <= 0 || $.trim(timeValue) === '') {
                            var errorMessage = $txtTimeValue.data('val-message');
                            this._showInvalidInput($txtTimeValue, true, errorMessage);
                            result = false;
                        } else {
                            this._plugin.conditionMustExistWasChanged(true, parseInt(timeValue), $timeKind.text());
                            this._showInvalidInput($txtTimeValue, false);
                        }

                        return result;
                    };
                    return AlertConditionController;
                })();
                Add.AlertConditionController = AlertConditionController;

                var AlertConditionConfig = (function () {
                    function AlertConditionConfig() {
                    }
                    return AlertConditionConfig;
                })();
                Add.AlertConditionConfig = AlertConditionConfig;
            })(Alerts.Add || (Alerts.Add = {}));
            var Add = Alerts.Add;
        })(Core.Alerts || (Core.Alerts = {}));
        var Alerts = Core.Alerts;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
