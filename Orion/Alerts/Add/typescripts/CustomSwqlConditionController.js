﻿/// <reference path="../../../typescripts/typings/jquery.d.ts" />
/// <reference path="../../../typescripts/typings/OrionMinRegs.d.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var SW;
(function (SW) {
    (function (Core) {
        (function (Alerts) {
            (function (Add) {
                var CustomSwqlConditionController = (function (_super) {
                    __extends(CustomSwqlConditionController, _super);
                    function CustomSwqlConditionController(_config) {
                        _super.call(this);
                        this._config = _config;
                    }
                    CustomSwqlConditionController.prototype.init = function () {
                        var _this = this;
                        this._swqlData = JSON.parse($('#' + this._config.swqlFragmentId).val());

                        var $sqlEntityTypes = $('#' + this._config.swqlEntityTypesId);

                        // register onchange event
                        $sqlEntityTypes.change(function () {
                            return _this._handleEntityTypeChange();
                        });

                        var jsonValue = $('#' + this._config.chainItemConfigInputId).val();
                        var condition = JSON.parse(jsonValue);

                        // select node by default when there are no data supplied
                        if ($.isEmptyObject(condition)) {
                            $sqlEntityTypes.val('Node');
                        } else {
                            $sqlEntityTypes.val(condition.ObjectType);
                        }

                        $sqlEntityTypes.change();

                        this._conditionController = SW.Core.Alerts.Add.AlertConditionBuilder.instance.getConditionController(this._config.conditionControllerId);
                        this._conditionController.pluginLoaded(this);

                        if (this._config.validateButton) {
                            var self = this;
                            $('#' + this._config.validateButton.ButtonId).click(function () {
                                if (self.validate())
                                    $('#' + self._config.validateButton.MsgPassID).show();
                                else
                                    $('#' + self._config.validateButton.MsgFailID).show();
                                return false;
                            });
                        }
                    };

                    CustomSwqlConditionController.prototype.getConditionChainItemConfig = function () {
                        var userSwql = $('#' + this._config.userSwqlQueryTextId).val();
                        var objectType = $('#' + this._config.swqlEntityTypesId + ' option:selected').val();

                        var result = new CustomSwqlChainItemConfig();
                        result.$type = this._config.chainItemConfigType;
                        result.Command = userSwql;
                        result.ObjectType = objectType;
                        result.ConditionTypeRefID = 'Core.CustomSwql';
                        return result;
                    };

                    CustomSwqlConditionController.prototype.reportInvalidInput = function (validationResult) {
                        var $txtUserSwql = $('#' + this._config.userSwqlQueryTextId);

                        if (this._config.validateButton) {
                            $('#' + this._config.validateButton.MsgPassID).hide();
                            $('#' + this._config.validateButton.MsgFailID).hide();
                        }

                        if (validationResult.IsValid) {
                            $txtUserSwql.removeClass('sw-validation-input-error');
                            $txtUserSwql.removeAttr('title');
                            return true;
                        }

                        var message = validationResult.ErrorMessages.join('\n');
                        $txtUserSwql.addClass('sw-validation-input-error');
                        $txtUserSwql.attr('title', message);
                        return false;
                    };

                    CustomSwqlConditionController.prototype._handleEntityTypeChange = function () {
                        var sqlQuery = this._swqlData[$('#' + this._config.swqlEntityTypesId).val()];
                        $('#' + this._config.swqlQueryTextId).val(sqlQuery);
                    };
                    return CustomSwqlConditionController;
                })(Add.ConditionPluginControllerBase);
                Add.CustomSwqlConditionController = CustomSwqlConditionController;

                var CustomSwqlConditionConfig = (function (_super) {
                    __extends(CustomSwqlConditionConfig, _super);
                    function CustomSwqlConditionConfig() {
                        _super.apply(this, arguments);
                    }
                    return CustomSwqlConditionConfig;
                })(Add.ConditionPluginConfigBase);
                Add.CustomSwqlConditionConfig = CustomSwqlConditionConfig;

                var CustomSwqlChainItemConfig = (function (_super) {
                    __extends(CustomSwqlChainItemConfig, _super);
                    function CustomSwqlChainItemConfig() {
                        _super.apply(this, arguments);
                    }
                    return CustomSwqlChainItemConfig;
                })(Add.ChainItemConfig);
            })(Alerts.Add || (Alerts.Add = {}));
            var Add = Alerts.Add;
        })(Core.Alerts || (Core.Alerts = {}));
        var Alerts = Core.Alerts;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
