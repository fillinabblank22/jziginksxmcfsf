﻿/// <reference path="../../../typescripts/typings/jquery.d.ts" />
/// <reference path="../../../typescripts/typings/OrionMinRegs.d.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var SW;
(function (SW) {
    (function (Core) {
        (function (Alerts) {
            (function (Add) {
                var DynamicConditionController = (function (_super) {
                    __extends(DynamicConditionController, _super);
                    function DynamicConditionController(_config) {
                        _super.call(this);
                        this._config = _config;

                        if (SW.Core.Alerts.Add.DynamicConditionController.Instance == null) {
                            SW.Core.Alerts.Add.DynamicConditionController.Instance = [];
                        }

                        SW.Core.Alerts.Add.DynamicConditionController.Instance[this._config.ClientIdPrefix] = this;
                    }
                    DynamicConditionController.prototype.init = function () {
                        var _this = this;
                        this._ebAlert = SW.Core.Controls.ExpressionBuilder.Instance[this._config.BuilderInstanceForAlert];
                        this._ebScope = SW.Core.Controls.ExpressionBuilder.Instance[this._config.BuilderInstanceForScope];

                        this._ebAlert.SetOnChangeHandler($.proxy(this._ebAlertHasChanged, this));
                        this._ebScope.SetOnChangeHandler($.proxy(this._ebScopeHasChanged, this));

                        var jsonValue = $('#' + this._config.chainItemConfigInputId).val();
                        var chainItemConfig = JSON.parse(jsonValue);

                        // sliding window data
                        var $txtTimeValue = this._getDataFormInput('timeValue');
                        var $timeKind = this._getDataFormSelect('timeKind');
                        var slidingWindowEnabled = $.isNumeric(chainItemConfig.TimeWindowInSeconds) && chainItemConfig.TimeWindowInSeconds > 0;

                        if (slidingWindowEnabled) {
                            if (chainItemConfig.TimeWindowInSeconds % 60 == 0) {
                                $timeKind.children('option[value="60"]').attr('selected', 'true');
                                $txtTimeValue.val((chainItemConfig.TimeWindowInSeconds / 60).toString());
                            } else {
                                $timeKind.children('option[value="1"]').attr('selected', 'true');
                                $txtTimeValue.val(chainItemConfig.TimeWindowInSeconds.toString());
                            }
                            this._getDataFormDiv('slidingWindowDiv').show();
                        }
                        $txtTimeValue.prop('disabled', !slidingWindowEnabled);
                        $timeKind.prop('disabled', !slidingWindowEnabled);

                        // validation events
                        this._getDataFormInput('timeValue').change(function (e) {
                            return _this._validateTimeValue();
                        }).keyup(function (e) {
                            _this._validateTimeValue();
                            return true;
                        });
                        this._getDataFormSelect('timeKind').change(function (e) {
                            return _this._validateTimeValue();
                        });

                        // notify controller
                        this._conditionController = SW.Core.Alerts.Add.AlertConditionBuilder.instance.getConditionController(this._config.conditionControllerId);
                        this._conditionController.pluginLoaded(this);
                    };

                    DynamicConditionController.prototype.conditionMustExistWasChanged = function (checked, time, unit) {
                        if (checked) {
                            this._addConditionMustExistNoteToExpressionBuilder(time, unit);
                        } else {
                            this._ebAlert.RemoveFromInfoPanel('mustExistFor');
                        }
                    };

                    DynamicConditionController.prototype.waitUntilXNetObjectWasChanged = function (checked, count, operator) {
                        if (checked) {
                            this._addWaitForObjectNoteToExpressionBuilder(count, operator);
                        } else {
                            this._ebAlert.RemoveFromInfoPanel('waitForObjects');
                        }
                    };

                    DynamicConditionController.prototype.getConditionChainItemConfig = function () {
                        var resultAlert;
                        var resultScope;

                        if (this._ebAlert != undefined) {
                            resultAlert = this._ebAlert.GetExpressionTree();
                        } else {
                            alert('ExpressionBuilder instance for Alert was not found');
                            return;
                        }

                        if (this._ebScope != undefined) {
                            resultScope = this._ebScope.GetExpressionTree();
                        } else {
                            alert('ExpressionBuilder instance for Scope was not found');
                            return;
                        }

                        var result = new DynamicChainItemConfig();
                        result.$type = this._config.chainItemConfigType;
                        result.ScopeTree = this._isScopeEnabled() ? resultScope : null;
                        result.AlertTree = resultAlert;
                        result.ObjectType = this._config.ObjectType;
                        result.ConditionTypeRefID = 'Core.Dynamic';

                        if (this._getDataFormDiv('slidingWindowDiv').is(":visible")) {
                            var timeValue = parseInt(this._getDataFormInput('timeValue').val(), 10);
                            var timeKind = parseInt(this._getDataFormSelect('timeKind', 'option:selected').attr('value'), 10);

                            result.TimeWindowInSeconds = timeValue * timeKind;
                        }

                        return result;
                    };

                    DynamicConditionController.prototype._isScopeEnabled = function () {
                        return $('#' + this._config.ScopeRadioButtonId).is(':checked');
                    };

                    DynamicConditionController.prototype.showObjectsList = function () {
                        // create and show scope objects dialog with grid
                        this._createShowObjectsListDialog();
                    };

                    DynamicConditionController.prototype.showSwqlQueryDialog = function () {
                        var _this = this;
                        // remember dialog parent
                        this._showSwqlQueryDialogParent = $("#" + this._config.ShowSWQLDialogControlId).parent();

                        // create dialog (modal window), and Open it (autoOpen=true)
                        this._showSwqlQueryDialog = $("#" + this._config.ShowSWQLDialogControlId).dialog({
                            width: 430,
                            modal: true,
                            title: "@{R=Core.Strings.2;K=WEBJS_TK0_15;E=js}",
                            autoOpen: true,
                            close: function () {
                                _this._cleanUpShowSwqlQueryDialog();
                            }
                        });

                        var dialog = this._showSwqlQueryDialog;
                        $("#" + this._config.ShowSWQLDialogControlId + " span.closeButtonPlaceHolder").empty();
                        $(SW.Core.Widgets.Button('@{R=Core.Strings.2;K=WEBJS_PCO_005;E=js}', { type: 'secondary' })).appendTo($("#" + this._config.ShowSWQLDialogControlId + " span.closeButtonPlaceHolder")).click(function () {
                            dialog.dialog("close");
                        });

                        var param = {
                            ExprScopeTree: this._isScopeEnabled() ? this._ebScope.GetExpressionTree() : null,
                            ExprAlertTree: this._ebAlert.GetExpressionTree(),
                            ObjectType: this._config.ObjectType
                        };

                        SW.Core.Services.callControllerAction("/api/AlertDefinitionData/", "GetDynamicConditionQueryString", param, function (res) {
                            $("#" + _this._config.ShowSWQLDialogTextAreaId).val(res);
                        }, function () {
                            $.error("Can't load condition query string");
                        });
                    };

                    DynamicConditionController.prototype._cleanUpShowSwqlQueryDialog = function () {
                        // destroy dialog
                        $("#" + this._config.ShowSWQLDialogControlId).dialog("destroy");

                        // move DIV/panel back to user control
                        $("#" + this._config.ShowSWQLDialogControlId).appendTo(this._objectsListDialogParent);
                    };

                    DynamicConditionController.prototype._createShowObjectsListDialog = function () {
                        var _this = this;
                        // create data store
                        this._objectsListDataStore = new Ext.data.Store({
                            proxy: new Ext.data.HttpProxy({
                                url: '/api/AlertDefinitionData/GetScopeObjects',
                                method: "POST",
                                timeout: 900000,
                                success: function (response, options) {
                                    $('#objectsListDialogSwisError').html('');
                                    if (typeof response.responseText !== 'undefined' && response.status == 200) {
                                        try  {
                                            var responseJson = JSON.parse(response.responseText);
                                            if (responseJson.Metadata && responseJson.Metadata.ErrorControl !== null && responseJson.Metadata.ErrorControl != '') {
                                                $('#objectsListDialogSwisError').html(responseJson.Metadata.ErrorControl);
                                            }
                                        } catch (err) {
                                            console.log(err);
                                        }
                                    }
                                },
                                failure: function (xhr, options) {
                                    SW.Core.Services.handleError(xhr);
                                }
                            }),
                            reader: new Ext.data.JsonReader({
                                totalProperty: "TotalRows",
                                root: "DataTable.Rows"
                            }, [
                                { name: "ObjectName", mapping: 0 },
                                { name: "ObjectURI", mapping: 1 }
                            ])
                        });

                        // create grid panel
                        this._objectsListGrid = new Ext.grid.GridPanel({
                            store: this._objectsListDataStore,
                            columns: [
                                { header: '@{R=Core.Strings.2;K=WEBJS_PCO_002;E=js}', sortable: false, dataIndex: 'ObjectName', width: 390 }
                            ],
                            height: 270,
                            width: 405,
                            stripeRows: true,
                            renderTo: this._config.GridControlId,
                            bbar: new Ext.PagingToolbar({
                                store: this._objectsListDataStore,
                                pageSize: 10,
                                displayInfo: true,
                                displayMsg: "@{R=Core.Strings.2;K=WEBJS_PCO_004;E=js}",
                                emptyMsg: "@{R=Core.Strings.2;K=WEBJS_PCO_003;E=js}"
                            })
                        });

                        // clear objects total count before data load
                        this._objectsListGrid.getStore().on("beforeload", function () {
                            $('#' + _this._config.ObjectsCountTextId).html('');
                        });

                        // fill objects total count after data load
                        this._objectsListGrid.getStore().on("load", function () {
                            $('#' + _this._config.ObjectsCountTextId).html(' (' + _this._objectsListGrid.getStore().getTotalCount() + ')');
                        });

                        // remember dialog parent
                        this._objectsListDialogParent = $("#" + this._config.DialogControlId).parent();

                        // create dialog (modal window), and Open it (autoOpen=true)
                        this._objectsListDialog = $("#" + this._config.DialogControlId).dialog({
                            width: 430,
                            modal: true,
                            title: "@{R=Core.Strings.2;K=WEBJS_PCO_001;E=js}",
                            autoOpen: true,
                            close: function () {
                                _this._cleanUpShowObjectsListDialog();
                            }
                        });

                        var dialog = this._objectsListDialog;
                        $("#" + this._config.ShowObjectsListDialogControlId + " span.closeButtonPlaceHolder").empty();
                        $(SW.Core.Widgets.Button('@{R=Core.Strings.2;K=WEBJS_PCO_005;E=js}', { type: 'secondary' })).appendTo($("#" + this._config.ShowObjectsListDialogControlId + " span.closeButtonPlaceHolder")).click(function () {
                            dialog.dialog("close");
                        });

                        this._refreshGridData(0, 10, "", "", "", "", function () {
                        });
                    };

                    DynamicConditionController.prototype._cleanUpShowObjectsListDialog = function () {
                        // clean "placeholder" for grid rendering
                        $('#' + this._config.GridControlId).empty();

                        // clean label with total objects count
                        $('#' + this._config.ObjectsCountTextId).empty();

                        // destroy dialog
                        $("#" + this._config.DialogControlId).dialog("destroy");

                        // move DIV/panel back to user control
                        $("#" + this._config.DialogControlId).appendTo(this._objectsListDialogParent);
                    };

                    DynamicConditionController.prototype._refreshGridData = function (start, limit, property, type, value, search, callback) {
                        var scopeExpTree = this._isScopeEnabled() ? this._ebScope.GetExpressionTree() : null;
                        var alertExpTree = this._ebAlert.GetExpressionTree();
                        this._objectsListDataStore.removeAll();
                        this._objectsListDataStore.proxy.conn.jsonData = { property: property, type: type, value: value != null ? value : "", search: search, objectType: this._config.ObjectType, exprScopeTree: scopeExpTree, exprAlertTree: alertExpTree };
                        this._objectsListDataStore.load({ params: { start: start, limit: limit }, callback: callback });
                    };

                    DynamicConditionController.prototype._ebAlertHasChanged = function (builder) {
                        var exprTree = builder.getExpression();

                        this._eventCounter = 0;

                        if (!builder.isCorrelationEngineEnabled()) {
                            var valError;
                            var hasMoreEvents = this._hasMoreEvents(exprTree);
                            var message = '@{R=Core.Strings.2;K=WEBJS_TK0_12;E=js}';

                            if (hasMoreEvents) {
                                valError = new Add.ValidationResult(false, [message]);
                                this.reportInvalidInput(valError);
                            } else if (this.isErrorMessageVisible(message)) {
                                valError = new Add.ValidationResult(true, []);
                                this.reportInvalidInput(valError);
                            }
                        }

                        this._expressionBuilderHasChanged("alert", builder);
                    };

                    DynamicConditionController.prototype._ebScopeHasChanged = function (builder) {
                        this._expressionBuilderHasChanged("scope", builder);
                    };

                    DynamicConditionController.prototype._expressionBuilderHasChanged = function (type, builder) {
                        if (type == 'alert') {
                            var exprTree = builder.getExpression();
                            this._eventCounter = 0;
                            var isSlidingWindowRequired = (this._hasMoreEvents(exprTree) && builder.isCorrelationEngineEnabled()) || this._isAggregationSupported(exprTree);
                            var isMustExistForMoreThanAllowed = this._isMustExistForMoreThanAllowed(exprTree);

                            this._conditionController.enableConditionPeriodChange(isMustExistForMoreThanAllowed);

                            if (isSlidingWindowRequired && !this._getDataFormDiv('slidingWindowDiv').is(":visible")) {
                                this._getDataFormDiv('slidingWindowDiv').show();
                                this._enableChkSlidingWindowChange(true, '10');
                            } else if (!isSlidingWindowRequired) {
                                this._getDataFormDiv('slidingWindowDiv').hide();
                                this._enableChkSlidingWindowChange(false);
                            }
                        }
                    };

                    DynamicConditionController.prototype._isMustExistForMoreThanAllowed = function (expr) {
                        if (!expr) {
                            return true;
                        }

                        if (expr.NodeType == 3) {
                            return false;
                        }

                        var result = true;

                        if (expr.Child && expr.Child.length > 0) {
                            for (var i = 0; i < expr.Child.length; i++) {
                                result = result && this._isMustExistForMoreThanAllowed(expr.Child[i]);
                            }
                        }

                        return result;
                    };

                    DynamicConditionController.prototype._isAggregationSupported = function (expr) {
                        if (!expr) {
                            return false;
                        }

                        if (expr.NodeType == 1 && expr.Child && expr.Child.length == 1) {
                            return true;
                        }

                        var result = false;

                        if (expr.Child && expr.Child.length > 0) {
                            for (var i = 0; i < expr.Child.length; i++) {
                                result = result || this._isAggregationSupported(expr.Child[i]);
                            }
                        }

                        return result;
                    };

                    DynamicConditionController.prototype._hasMoreEvents = function (expr, init) {
                        if (typeof init === "undefined") { init = true; }
                        if (init) {
                            this._eventCounter = 0;
                        }

                        if (!expr) {
                            return false;
                        }

                        if (expr.NodeType == 3) {
                            this._eventCounter++;
                            if (expr.Child[2].Value != '0') {
                                return true;
                            }
                        }

                        if (this._eventCounter > 1)
                            return true;

                        var result = false;

                        if (expr.Child && expr.Child.length > 0) {
                            for (var i = 0; i < expr.Child.length; i++) {
                                result = result || this._hasMoreEvents(expr.Child[i], false);
                            }
                        }

                        return result;
                    };

                    DynamicConditionController.prototype._getDataFormDiv = function (dataFormName) {
                        var element = $('#' + this._config.conditionControllerId + ' div[data-form="' + dataFormName + '"]');
                        return element;
                    };

                    DynamicConditionController.prototype._getDataFormSpan = function (dataFormName) {
                        var element = $('#' + this._config.conditionControllerId + ' span[data-form="' + dataFormName + '"]');
                        return element;
                    };

                    DynamicConditionController.prototype._getDataFormInput = function (dataFormName) {
                        var element = $('#' + this._config.conditionControllerId + ' input[data-form="' + dataFormName + '"]');
                        return element;
                    };

                    DynamicConditionController.prototype._getDataFormSelect = function (dataFormName, additionalSelector) {
                        if (typeof additionalSelector === "undefined") { additionalSelector = ''; }
                        var element = $('#' + this._config.conditionControllerId + ' select[data-form="' + dataFormName + '"] ' + additionalSelector);
                        return element;
                    };

                    DynamicConditionController.prototype._chkSlidingWindowChange = function (eventObject) {
                        var checked = $(eventObject.target).is(':checked');
                        this._enableChkSlidingWindowChange(checked);
                    };

                    DynamicConditionController.prototype._enableChkSlidingWindowChange = function (enable, defaultTime) {
                        var $txtTimeValue = this._getDataFormInput('timeValue');
                        var $timeKind = this._getDataFormSelect('timeKind');

                        if (enable) {
                            $txtTimeValue.prop('disabled', false);

                            $timeKind.prop('disabled', false);

                            if (defaultTime) {
                                $txtTimeValue.val(defaultTime);
                            } else {
                                $txtTimeValue.focus();
                            }
                        } else {
                            $txtTimeValue.prop('disabled', true);
                            $txtTimeValue.val('');
                            $timeKind.prop('disabled', true);
                            this.showInvalidInput($txtTimeValue, false);

                            this._ebAlert.RemoveFromInfoPanel('slidingWindow');
                        }
                    };

                    DynamicConditionController.prototype._addConditionMustExistNoteToExpressionBuilder = function (time, unit) {
                        var tooltipMessage = '@{R=Core.Strings.2;K=WEBJS_TK0_9;E=js} ' + time + ' ' + unit;

                        this._ebAlert.RemoveFromInfoPanel('mustExistFor');
                        this._ebAlert.AddToInfoPanel('mustExistFor', tooltipMessage, '/Orion/images/ExpressionBuilder/Must_exist_icons24x24_big.png');
                    };

                    DynamicConditionController.prototype._addWaitForObjectNoteToExpressionBuilder = function (count, operator) {
                        var tooltipMessage = '@{R=Core.Strings.2;K=WEBJS_TK0_20;E=js} ' + operator + ' ' + count + ' ' + '@{R=Core.Strings.2;K=WEBJS_TK0_21;E=js}';

                        this._ebAlert.RemoveFromInfoPanel('waitForObjects');
                        this._ebAlert.AddToInfoPanel('waitForObjects', tooltipMessage, '/Orion/images/ExpressionBuilder/wait_untilX_icons24x24_b.png');
                    };

                    //------ VALIDATIONS --------------
                    //---------------------------------
                    DynamicConditionController.prototype.validateOnClient = function () {
                        var result = _super.prototype.validateOnClient.call(this);

                        if (this._getDataFormDiv('slidingWindowDiv').is(":visible")) {
                            result = this._validateTimeValue() && result;
                        }

                        if (!this._ebAlert.IsCorrelationEngineEnabled()) {
                            var hasMoreEvents = this._hasMoreEvents(this._ebAlert.GetExpressionTree());

                            if (hasMoreEvents) {
                                this.reportInvalidInput(new Add.ValidationResult(false, ["@{R=Core.Strings.2;K=WEBJS_TK0_12;E=js}"]));
                                return false;
                            }
                        }

                        var errorInAlertItems = this._ebAlert.GetRootElement().find("input.sw-validation-input-error:visible");
                        var errorInScopeItems = this._ebScope.GetRootElement().find("input.sw-validation-input-error:visible");

                        var errorInAlert = errorInAlertItems.length > 0;
                        var errorInScope = errorInScopeItems.length > 0;

                        if (errorInAlert || errorInScope) {
                            result = !(errorInAlert || errorInScope);

                            var errors = [];

                            if (errorInAlert) {
                                errors.push($(errorInAlertItems[0]).attr('title'));
                            }

                            if (errorInScope && !errorInAlert) {
                                errors.push($(errorInScopeItems[0]).attr('title'));
                            }

                            this.reportInvalidInput(new Add.ValidationResult(false, errors));
                        }

                        return result;
                    };

                    DynamicConditionController.prototype.reportInvalidInput = function (validationResult) {
                        var container = this._getContainer('validationResultsContainer');
                        if (!validationResult.IsValid) {
                            var message = validationResult.ErrorMessages.join('<br/>');

                            container.find('span[data-message="resultMessage"]').html(message);
                            container.show();
                        } else {
                            container.hide();
                        }

                        return validationResult.IsValid;
                    };

                    DynamicConditionController.prototype.isErrorMessageVisible = function (message) {
                        var container = this._getContainer('validationResultsContainer');
                        if (!message) {
                            return container.is(":visible");
                        } else {
                            return message == container.find('span[data-message="resultMessage"]').html();
                        }
                    };

                    DynamicConditionController.prototype._getContainer = function (containerName, additionalSelector) {
                        if (typeof additionalSelector === "undefined") { additionalSelector = ''; }
                        var element = $('#' + this._config.conditionControllerId + ' div[data-container="' + containerName + '"]' + additionalSelector);
                        return element;
                    };

                    DynamicConditionController.prototype._validateTimeValue = function () {
                        var result = true;
                        var $txtTimeValue = this._getDataFormInput('timeValue');
                        var timeValue = $txtTimeValue.val();
                        var timeValueInt = parseInt(timeValue);
                        var timeKind = parseInt(this._getDataFormSelect('timeKind', 'option:selected').attr('value'));
                        var maxValue = parseInt($txtTimeValue.data('val-maxvalue'));

                        // check for integer > 0
                        if (timeValueInt.toString() !== timeValue || timeValueInt <= 0 || $.trim(timeValue) === '' || timeKind * timeValueInt > maxValue) {
                            var errorMessage = Core.String.Format($txtTimeValue.data('val-message'), maxValue / 60);
                            this.showInvalidInput($txtTimeValue, true, errorMessage);
                            result = false;
                        } else {
                            this.showInvalidInput($txtTimeValue, false);
                        }

                        return result;
                    };
                    return DynamicConditionController;
                })(Add.ConditionPluginControllerBase);
                Add.DynamicConditionController = DynamicConditionController;

                var DynamicConditionConfig = (function (_super) {
                    __extends(DynamicConditionConfig, _super);
                    function DynamicConditionConfig() {
                        _super.apply(this, arguments);
                    }
                    return DynamicConditionConfig;
                })(Add.ConditionPluginConfigBase);
                Add.DynamicConditionConfig = DynamicConditionConfig;

                var DynamicChainItemConfig = (function (_super) {
                    __extends(DynamicChainItemConfig, _super);
                    function DynamicChainItemConfig() {
                        _super.apply(this, arguments);
                    }
                    return DynamicChainItemConfig;
                })(Add.ChainItemConfig);
            })(Alerts.Add || (Alerts.Add = {}));
            var Add = Alerts.Add;
        })(Core.Alerts || (Core.Alerts = {}));
        var Alerts = Core.Alerts;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
