using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Internationalization.Extensions;
using SolarWinds.Orion.Core.Actions.Impl;
using SolarWinds.Orion.Core.Alerting;
using SolarWinds.Orion.Core.Alerting.DAL;
using SolarWinds.Orion.Core.Alerting.Models;
using SolarWinds.Orion.Core.Alerting.Plugins.Conditions.Dynamic;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Alerting;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Models.Actions;
using SolarWinds.Orion.Core.Models.Alerting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Alerts;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;
using AlertDefinition = SolarWinds.Orion.Core.Alerting.Models.AlertDefinition;

public partial class Orion_Alerts_Add_Summary : AddAlertWizardBase
{
    protected string affectedObjectsDivStyle;

    protected override void OnInit(EventArgs e)
    {
        if (Workflow.Alert != null && Workflow.Alert.AlertID.GetValueOrDefault(0) != 0)
        {
            this.Title = Resources.CoreWebContent.WEBDATA_VL0_35;
        }

        Workflow.SetStepFromUrl(this.Page.Request.Url.GetLeftPart(UriPartial.Path));
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
		{
            UpdateUiWithAlertConfiguraion();

			if (Workflow.DirectSubmit && !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
			{
				Validate();
				Next_Click(this.imgbNext, EventArgs.Empty);
			}

		}
        else if (Request["__EVENTTARGET"] == AuditNotificationCell.UniqueID)
        {
            var newVariables =
                OrionSerializationHelper.FromJSON(Request["__EVENTARGUMENT"], typeof (AlertNotificationProperty[])) as
                    AlertNotificationProperty[];
            if (newVariables != null)
            {
                if (Workflow.Alert.NotificationSettings == null || Workflow.Alert.NotificationSettings.NotificationProperties == null)
                {
                    Workflow.Alert.NotificationSettings = new AlertNotificationSettings();
                    Workflow.Alert.NotificationEnabled = true;
                }

                foreach (var variable in newVariables)
                {
                    if (string.IsNullOrEmpty(variable.Name))
                        variable.Name = GetUniqueName(Workflow.Alert.NotificationSettings.NotificationProperties);

                    Workflow.Alert.NotificationSettings.AddProperty(variable);
                }
                RefreshGridView();
            }
        }

		// block saving report in case of demo mode
		if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
		{
			imgbNext.OnClientClick = "demoAction('Core_Alerting_SaveAlert', this); return false;";
		}
    }

    private string GetUniqueName(IEnumerable<AlertNotificationProperty> notificationProperties)
    {
        int i = 1;
        
        if (notificationProperties == null)
            return string.Format(Resources.CoreWebContent.WEBDATA_SO_201, i);
            
            while (notificationProperties.Any(property => property.Name.Equals(string.Format(Resources.CoreWebContent.WEBDATA_SO_201, i))))
                i++;

        return string.Format(Resources.CoreWebContent.WEBDATA_SO_201, i);
    }

    private void UpdateUiWithAlertConfiguraion()
    {
        MacroVariablePicker.MacroContexts = Workflow.AlertingActionContext.MacroContext.Contexts;
        var alert = Workflow.Alert;
        if (alert != null)
        {
            this.alertName.Text = HttpUtility.HtmlEncode(alert.Name);

			if (!string.IsNullOrWhiteSpace(alert.Description))
			{
				this.description.Text = HttpUtility.HtmlEncode(alert.Description);
			}
			else
			{
				this.description.Text = Resources.CoreWebContent.WEBDATA_BV0_0018;
				this.description.CssClass = "value-not-specified";
			}

			enabledAlert.Text = alert.Enabled ? Resources.CoreWebContent.WEBDATA_YP0_20 : Resources.CoreWebContent.WEBDATA_SO0_153;
            propType.Text = EntityHelper.GetLocalizedEntityName(alert.ObjectType);
            frequency.Text = DateTimeHelper.FormatFrequency(alert.Frequency);
			severity.Text = alert.Severity.LocalizedLabel();
	        category.Text = string.IsNullOrEmpty(alert.Category) ? Resources.CoreWebContent.AlertWizard_Summary_Category_NoLimitation : alert.Category;
            
            string currentUserCategory = HttpContext.Current.Profile["AlertCategory"] as string;
            if ((string.IsNullOrWhiteSpace(currentUserCategory) || string.Equals(currentUserCategory, alert.Category, StringComparison.OrdinalIgnoreCase)) && !string.IsNullOrWhiteSpace(alert.CreatedBy))
            {
                alertOwnerInfoContainer.Visible = true;
                alertOwnerInfo.Text = alert.CreatedBy; 
            }
            else
            {
                alertOwnerInfoContainer.Visible = false;
            }

           
            Panel childPanel;

            // Update Trigger Action
			var escalationLevels = (from triggerAction in alert.TriggerActions
								   let hasEscalationLevel = triggerAction.Properties.ContainsKey(CommonActionConstants.EscalationLevelPropertyName)
								   let escalationLevel = hasEscalationLevel ? int.Parse(triggerAction.Properties[CommonActionConstants.EscalationLevelPropertyName].PropertyValue) : 0
								   orderby escalationLevel
								   group triggerAction by escalationLevel into escalationLevelTriggerActions
								   select escalationLevelTriggerActions).ToArray();



            if (escalationLevels.Length > 0)
			{
				int escalationLevelCount = 0;
				foreach (var escalationLevel in escalationLevels)
				{
					escalationLevelCount++;

					Panel escalationLevelPanel = new Panel();
					this.triggerActions.Controls.Add(escalationLevelPanel);

					escalationLevelPanel.Controls.Add(new Label()
					{
						Text = string.Format(Resources.CoreWebContent.WEBDATA_TM0_343, escalationLevelCount),
					});

					Panel actionsPanel = new Panel();
					escalationLevelPanel.Controls.Add(actionsPanel);

                    foreach (var triggerAction in escalationLevel.OrderBy(ta => ta.Order))
					{
                        actionsPanel.Controls.Add(GetActionPanel(triggerAction));
                    }
				}
			}
			else
			{
				this.triggerActions.Controls.Add(new Label { Text = Resources.CoreWebContent.WEBDATA_BV0_0020, CssClass = "value-not-specified" });
			}

            // Update Reset Action
			if (alert.ResetActions.Count > 0)
			{
				foreach (var resetAction in alert.ResetActions.OrderBy(ra => ra.Order))
				{
				    resetActions.Controls.Add(GetActionPanel(resetAction));
				}
			}
			else
			{
				this.resetActions.Controls.Add(new Label { Text = Resources.CoreWebContent.WEBDATA_BV0_0019, CssClass = "value-not-specified" });
			}

            if (alert.ExecutionTimePeriods!=null && alert.ExecutionTimePeriods.Count > 0)
            {
                var enableitems = alert.ExecutionTimePeriods.Where(x => x.EnabledDuringTimePeriod == true);
                var disableitems = alert.ExecutionTimePeriods.Where(x => x.EnabledDuringTimePeriod == false);

                childPanel = new Panel();
                childPanel.Controls.Add(new Label() { Text = Resources.CoreWebContent.WEBDATA_OM0_3, CssClass = "stateDuringTimePeriod" });
                this.TimesOfDay.Controls.Add(childPanel);

                foreach (var schedule in enableitems)
                {
                    childPanel = new Panel();
                    childPanel.Controls.Add(new Label() { Text = CronHelper.GetFrequencyName(schedule), CssClass = "informText" });
                    this.TimesOfDay.Controls.Add(childPanel);
                }

                childPanel = new Panel();
                childPanel.Controls.Add(new Label() { Text = Resources.CoreWebContent.WEBDATA_OM0_4, CssClass = "stateDuringTimePeriod" });
                this.TimesOfDay.Controls.Add(childPanel);

                foreach (var schedule in disableitems)
                {
                    childPanel = new Panel();
                    childPanel.Controls.Add(new Label(){Text = CronHelper.GetFrequencyName(schedule), CssClass = "informText" });
                    this.TimesOfDay.Controls.Add(childPanel);
                }
            }
            else
            {
                childPanel = new Panel();
                childPanel.Controls.Add(new Label(){Text = Resources.CoreWebContent.WEBDATA_SO0_158, CssClass = "informText" });
                this.TimesOfDay.Controls.Add(childPanel);
            }
            if (alert.Trigger != null)
            {
                string informationText = String.Empty;
                IConditionEntityScope iEntityScope = null;
                bool showScope = alert.Trigger.Conditions.Count == 1 && (iEntityScope = (alert.Trigger.Conditions[0].Type as IConditionEntityScope)) != null;

                if (showScope)
                {
                    string scopeDescription = iEntityScope.GetSummaryDescriptionForScope(alert.Trigger.Conditions[0].Condition,alert.Trigger.Conditions[0].ObjectType);

                    if(!String.IsNullOrEmpty(scopeDescription))
                    {
                        informationText = String.Format("<span class='subLabel'>{0}:</span><br/>",Resources.CoreWebContent.WEBDATA_TK0_62); // Scope
                        informationText += scopeDescription.Replace("\r\n", "<br/>");
                        informationText += "<br/>";
                    }
                    
                    informationText += String.Format("<span class='subLabel'>{0}:</span><br/>",Resources.CoreWebContent.WEBDATA_TK0_63); // Alert
                    informationText += iEntityScope.GetSummaryDescriptionForAlert(WebSecurityHelper.EncodeExpressionTree(alert.Trigger).Conditions[0].Condition, alert.Trigger.Conditions[0].ObjectType).Replace("\r\n", "<br/>");
                }
                else
                {
                    informationText = alert.Trigger.GetSummaryInformation().Replace("\r\n", "<br/>");
                }

                TriggerConditions.Controls.Add(new Label()
                {
                    Text = informationText,
                    CssClass = "informText"
                });
            }

            if (alert.Reset != null && alert.Reset.Conditions.Any())
                ResetConditions.Controls.Add(new Label() { Text = WebSecurityHelper.EncodeExpressionTree(alert.Reset).GetSummaryInformation().Replace("\r\n", "<br/>"), CssClass = "informText" });
			else {
				ResetConditions.Controls.Add(new Label() { Text = Resources.CoreWebContent.WEBDATA_SO0_159, CssClass = "informText" });
			}

			alertCustomPropertiesSectionTitle.Text = string.Format(Resources.CoreWebContent.WEBDATA_BV0_0006 + ": ({0})", alert.CustomProperties.Count);
			if (alert.CustomProperties != null && alert.CustomProperties.Count > 0)
			{
				foreach (var customProperties in alert.CustomProperties)
				{
                    alertCustomProperties.Controls.Add(new Label { Text = string.Format("{0}: {1}<br/>", customProperties.Key, WebSecurityHelper.SanitizeHtmlV2(customProperties.Value.ToString())) });
				}
			}
			else
			{
				alertCustomProperties.Controls.Add(new Label { Text = Resources.CoreWebContent.WEBDATA_BV0_0039, CssClass = "value-not-specified" });
			}
            
            if (ShouldShowAffectedObjectsCount(alert) && !Workflow.DirectSubmit)
            {
                try
                {
                    int triggeredObjectsCount;
                    List<string> affectedObjectsList = CalculateAffectedObjects(WebSecurityHelper.DecodeExpressionTree(alert), out triggeredObjectsCount);
                    affectedObjectsCount.Text = string.Format(Resources.CoreWebContent.WEBDATA_IT0_8, triggeredObjectsCount);
                    showAllAffectedObjects.Visible = affectedObjectsList.Count > 0;
                    this.affectedObjectsDivStyle = affectedObjectsList.Count > 0 ? "sw-suggestion-warn" : "sw-suggestion-info";
                    this.affectedObjectsIconSpan.Visible = affectedObjectsList.Count > 0;

					if (affectedObjectsList.Count > 25)		//  Don't truncate the list unless there are several more than 20.  (i.e. if there are only a few more, we may as well show them)
					{
						int count = affectedObjectsList.Count;

						affectedObjectsList = affectedObjectsList.Take(20).ToList();
						affectedObjectsList.Add("");
						affectedObjectsList.Add(string.Format(Resources.CoreWebContent.WEBDATA_BV0_0048, count - 20));
					}

                    affectedObjects.Controls.Add(new Label { Text = String.Join("<br/>", affectedObjectsList.Select(HttpUtility.HtmlEncode)) });
                }
                catch (Exception e)
                {
                    log.Error("Error occurred during alert affected object calculation", e);
                    affectedObjectsCountHolder.Visible = false;
                }
            }
            else
            {
                affectedObjectsCountHolder.Visible = false;
            }

            foreach (AlertNotificationSeverity value in AlertSeverityHelper.GetOrderedAlertNotificationSeverities())
            {
                auditSeverity.Items.Add(new ListItem(value.LocalizedLabel(), value.ToString()) { Selected = false });
            }

            integrateCb.Checked = alert.NotificationEnabled;
            if (alert.NotificationEnabled && alert.NotificationSettings != null)
            {
                auditSeverity.SelectedValue = alert.NotificationSettings.Severity;
                alertNameTitle.Checked = alert.NotificationSettings.Subject.Equals(alert.Name);
                auditTitle.Text = alert.NotificationSettings.Subject;
            }
            else
            {
                integrateCb.Checked = false;
                advancedTr.Attributes.Add("class", "hidden");
            }

            if (!Page.IsPostBack)
                RefreshGridView();
        }
    }

    private Panel GetActionPanel(ActionDefinition action)
    {        
        string orderText = action.Order + ".";
        string actionDescription = WebSecurityHelper.SanitizeHtmlV2(HttpUtility.HtmlDecode(action.Description));

        var actionPanel = new Panel();
        actionPanel.Style[HtmlTextWriterStyle.PaddingLeft] = "20px";
        actionPanel.Controls.Add(new Label {Text = orderText, CssClass = "informText"});
        Image actionIcon = new Image {ImageUrl = pluginInfoProvider.GetActionIconPath(action.ActionTypeID)};
        actionIcon.Attributes["ext:qtip"] = actionDescription;
        actionPanel.Controls.Add(actionIcon);
        actionPanel.Controls.Add(new Label
        {
            Text = " " + WebSecurityHelper.SanitizeHtmlV2(action.Title),
            CssClass = "informText"
        });
        Image icon = new Image {ImageUrl = "/Orion/images/Show_Last_Note_icon16x16.PNG"};
        icon.Attributes["ext:qtip"] = actionDescription;
        actionPanel.Controls.Add(icon);

        if (action.TimePeriods.Count > 0)
        {
            var triggerTypes = from timePeriod in action.TimePeriods
                let triggerType = CronHelper.GetScheduleType(timePeriod)
                orderby triggerType
                select triggerType;

            List<string> maintenanceSchedules = new List<string>();
            if (triggerTypes.Contains(CronHelper.TriggerType.Daily))
                maintenanceSchedules.Add(Resources.CoreWebContent.WEBDATA_BV0_0001);
            if (triggerTypes.Contains(CronHelper.TriggerType.Weekly))
                maintenanceSchedules.Add(Resources.CoreWebContent.WEBDATA_BV0_0002);
            if (triggerTypes.Contains(CronHelper.TriggerType.Monthly))
                maintenanceSchedules.Add(Resources.CoreWebContent.WEBDATA_BV0_0003);

            string tooltip = string.Join(Environment.NewLine, maintenanceSchedules);

            actionPanel.Controls.Add(new Panel() {CssClass = "verticalDivider"});
            Image scheduleIcon = new Image() {ImageUrl = "/Orion/images/Time_icon_Small16x16.png"};
            scheduleIcon.Attributes["ext:qtip"] = tooltip;
            actionPanel.Controls.Add(scheduleIcon);
        }	
		
        return actionPanel;
    }

    private bool ShouldShowAffectedObjectsCount(AlertDefinition alert)
    {
        if (alert.Trigger.Conditions.Count > 1)
        {
            return false;
        }

        ConditionChainItem chainItem = alert.Trigger.Conditions[0];

        if (chainItem.NetObjectsMinCountThreshold.HasValue)
        {
            return false;
        }

        var dynamicConditionType = chainItem.Type as ConditionTypeDynamic;
        if (dynamicConditionType != null)
        {
            // for event based conditions we don't know affected object count
            return !dynamicConditionType.IsEventBase(chainItem.Condition, chainItem.ObjectType);
        }
        
        return true;
    }

    private List<string> CalculateAffectedObjects(AlertDefinition alert, out int triggeredObjectsCount)
    {
        var affectedObjectsList = new List<AlertConditionResultItem>();
        
        ConditionChainItem chainItem = alert.Trigger.Conditions[0];
        IConditionEvaluator conditionEvaluator = chainItem.Type.GetConditionEvaluator(chainItem.Condition, chainItem.ObjectType);

        string currentUser = HttpContext.Current.Profile.UserName;
        string createdBy = alert.AlertID.HasValue ? alert.CreatedBy : currentUser;

        // run evaluation under alert owner
        using (var context = new AlertEvalContext(alert.AlertRefID, createdBy))
        {
            conditionEvaluator.Start(context);
            affectedObjectsList.AddRange(context.Results);
        }

        // if current user is different from alert owner then we need to limit objects to display according to his limitation
        if (!string.IsNullOrWhiteSpace(createdBy) && !string.Equals(createdBy, currentUser))
        {
            using (var context = new AlertEvalContext(alert.AlertRefID, currentUser))
            {
                conditionEvaluator.Start(context);

                var objectIdHashSet = new HashSet<string>(context.Results.Select(o => o.ObjectId), StringComparer.OrdinalIgnoreCase);
                affectedObjectsList.RemoveAll(o => !objectIdHashSet.Contains(o.ObjectId));
            }
        }

        List<string> result = affectedObjectsList.Select(o => o.Caption).ToList();
        triggeredObjectsCount = result.Count;

        return result;
    }

    private class AlertEvalContext : IConditionEvaluatorContext, IDisposable
    {
        readonly List<AlertConditionResultItem> results = new List<AlertConditionResultItem>();
        readonly ManualResetEventSlim WaitSignal = new ManualResetEventSlim(false);
        readonly Guid AlertRefId;
        readonly string alertOwner;

        public AlertEvalContext(Guid alertId, string alertOwner)
        {
            AlertRefId = alertId;
            this.alertOwner = alertOwner;
        }

        internal IEnumerable<AlertConditionResultItem> Results
        {
            get
            {
                WaitSignal.Wait(TimeSpan.FromSeconds(10));
                return results;
            }
        }

        public ICollection<string> GetActiveObjects()
        {
            return new string[0];
        }

        public ConditionEvaluationContext ConditionEvaluationContext { get { return new ConditionEvaluationContext(alertOwner); } }

        public void OnNext(AlertConditionEvaluateResult result)
        {
            results.AddRange(result.Results);
        }

        public void OnStop()
        {
            if (log.IsDebugEnabled)
            {
                log.DebugFormat("Evaluation of AlertCondition for Alert={0} was successfuly done.", AlertRefId);
            }
            WaitSignal.Set();
        }

        public void OnError(string message, Exception ex)
        {
            log.ErrorFormat("Evaluation of AlertCondition for Alert={0} CalculateAffectedObjects failed {1}. {2}",
                            AlertRefId, message, ex);
            WaitSignal.Set();
        }

        public void Dispose()
        {
            WaitSignal.Set();
            WaitSignal.Dispose();
        }
    }

    protected override bool Next()
    {	
        var alert = Workflow.Alert;

        if (alert != null)
        {
            bool isNew;
            Session["CreatedAlertName"] = alert.Name;

            alert.Trigger = WebSecurityHelper.DecodeExpressionTree(alert.Trigger);
            alert.Reset = WebSecurityHelper.DecodeExpressionTree(alert.Reset);

            alert.NotificationEnabled = integrateCb.Checked;


            if (alert.NotificationSettings == null)
            {
                AlertNotificationSettings settings = new AlertNotificationSettings();
                settings.Enabled = true;
                AlertNotificationProperty prop = new AlertNotificationProperty("DateTime",
                    "${N=Generic;M=DateTime;F=DateTime}");
                settings.AddProperty(prop);
                prop = new AlertNotificationProperty("DayOfYear", "${N=Generic;M=DayOfYear}");
                settings.AddProperty(prop);
                alert.NotificationSettings = settings;
            }

            alert.NotificationSettings.Subject = alertNameTitle.Checked ? alert.Name : auditTitle.Text;
            alert.NotificationSettings.NetObjectType = alert.ObjectType;
            alert.NotificationSettings.Severity = auditSeverity.SelectedValue;
            alert.NotificationSettings.Enabled = integrateCb.Checked;


            var dal =
                AlertDefinitionsDAL.Create(
                    ConditionTypeProvider.Create(AppDomainCatalog.Instance, InformationServiceProxy.CreateV3Creator()),
                    InformationServiceProxy.CreateV3Creator());
            if (alert.AlertID.HasValue && !dal.Exist(alert.AlertID.Value))
            {
                alert.AlertID = null;
            }
            if (alert.AlertID == null)
            {
                //ToDo:
                isNew = true;
                alert.AlertRefID = Guid.NewGuid();
                alert.CreatedBy = HttpContext.Current.Profile.UserName;
                int? i = dal.Create(alert);

                returnUrl = UrlHelper.AppendParameter(returnUrl, "AlertID", i.ToString());
            }
            else
            {
                isNew = false;
                dal.Update(alert);
                if (!Workflow.IsDuplicated)
                    Session["CreatedAlertName"] += ":hightlight=true";

                returnUrl = UrlHelper.AppendParameter(returnUrl, "AlertID", alert.AlertID.ToString());
            }
            foreach (var cp in alert.CustomProperties.Where(cp => cp.Value != null))
            {
                if (!isNew || (cp.Value != null && !string.IsNullOrEmpty(cp.Value.ToString())))
                    //no need to run update query for empty cps
                {
                    CustomPropertyMgr.SetCustomProp("AlertConfigurationsCustomProperties", cp.Key,
                        alert.AlertID.Value, cp.Value);

                    // update restricted values if needed
                    var customProperty = CustomPropertyMgr.GetCustomProperty("AlertConfigurationsCustomProperties",
                        cp.Key);
                    CustomPropertyHelper.UpdateRestrictedValuesIfNeeded(customProperty,
                        new[]
                        {
                            CustomPropertyHelper.GetInvariantCultureString(
                                (cp.Value != null) ? cp.Value.ToString() : string.Empty, customProperty.PropertyType)
                        });
                }
            }
        }
        return base.Next();
    }
        
    private void DeleteItem(string varName)
    {
        Workflow.Alert.NotificationSettings.Remove(varName);
    }

    private void RefreshGridView()
    {
        if (!Page.IsPostBack)
        {
            if (Workflow.Alert.NotificationSettings == null)
            {
                var provider = new AlertNotificationSettingsProvider();
                Workflow.Alert.NotificationSettings =
                    provider.GetDefaultAlertNotificationSettings(Workflow.Alert.ObjectType, Workflow.Alert.Name);
            }
        }

        if (Workflow.Alert.NotificationSettings != null && Workflow.Alert.NotificationSettings.NotificationProperties != null)
        {
            this.VariablesGrid.DataSource = Workflow.Alert.NotificationSettings.NotificationProperties;
            this.VariablesGrid.DataBind();
        }
    }

    protected string VarName
    {
        get
        {
            return ViewState["varName"].ToString();
        }
        set { ViewState["varName"] = value; }
    }

    protected void VariablesGrid_RowEditing(object sender, GridViewEditEventArgs e)
    {
        VariablesGrid.EditIndex = e.NewEditIndex;
        GridViewRow row = (GridViewRow)VariablesGrid.Rows[e.NewEditIndex];
        Label textname = (Label)row.FindControl("nameLable");
        VarName = textname.Text;
        RefreshGridView();
    }

    protected void VariablesGrid_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        VariablesGrid.EditIndex = -1;
        RefreshGridView();
    }

    protected void VariablesGrid_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridViewRow row = (GridViewRow)VariablesGrid.Rows[e.RowIndex];
        TextBox textname = (TextBox)row.FindControl("nameTb");
        TextBox textValue = (TextBox)row.FindControl("valueTb");

        
        VariablesGrid.EditIndex = -1;
        AlertNotificationProperty property = new AlertNotificationProperty(textname.Text, textValue.Text);

        IEnumerable<AlertNotificationProperty> clonedSettings = Workflow.Alert.NotificationSettings.NotificationProperties;
        List<AlertNotificationProperty> settings = new List<AlertNotificationProperty>();

        foreach (AlertNotificationProperty prop in clonedSettings)
        {
            if (prop.Name.Equals(VarName))
                settings.Add(property);
            else
                settings.Add(prop);
        }

        Workflow.Alert.NotificationSettings.Clear();
        Workflow.Alert.NotificationSettings.AddProperties(settings);
        RefreshGridView();
    }

    protected void VariablesGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        GridViewRow row = (GridViewRow)VariablesGrid.Rows[e.RowIndex];
        Label textname = (Label)row.FindControl("nameLable");
        DeleteItem(textname.Text);
        RefreshGridView();
    }
}