﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using SolarWinds.Orion.Core.Models.Interfaces;
using SolarWinds.Orion.Core.Models.Schedules;
using SolarWinds.Orion.Web.Alerts;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Alerts_Add_TimeOfDay : AddAlertWizardBase
{
    private List<TimePeriodSchedule> SchedulesList { get; set; }

    public string SchedulesListJson
    {
        get
        {
            var settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Objects };
            return JsonConvert.SerializeObject(SchedulesList, settings);
        }
    }

    protected override void OnInit(EventArgs e)
    {
        if (Workflow.Alert != null && Workflow.Alert.AlertID.GetValueOrDefault(0) != 0)
        {
            this.Title = Resources.CoreWebContent.WEBDATA_VL0_35;
        }

        if (!IsPostBack)
        {
            if (Workflow.Alert != null)
            {
                SchedulesList = Workflow.Alert.ExecutionTimePeriods;

                foreach (var timePeriodSchedule in SchedulesList)
                {
                    if (timePeriodSchedule.StartTime.Kind != DateTimeKind.Utc)
                    {
                        timePeriodSchedule.StartTime = TimeZoneInfo.ConvertTimeToUtc(timePeriodSchedule.StartTime, timePeriodSchedule.CronExpressionTimeZoneInfo);
                    }

                    if (timePeriodSchedule.EndTime.HasValue && timePeriodSchedule.EndTime.Value.Kind != DateTimeKind.Utc)
                    {
                        timePeriodSchedule.EndTime = TimeZoneInfo.ConvertTimeToUtc(timePeriodSchedule.EndTime.Value, timePeriodSchedule.CronExpressionTimeZoneInfo);
                    }

                    string sanitizedDisplayName = WebSecurityHelper.SanitizeHtmlV2(timePeriodSchedule.DisplayName);
                    timePeriodSchedule.DisplayName = sanitizedDisplayName;
                }

                bool isScheduled = SchedulesList != null && SchedulesList.Count > 0;
                NoSchedule.Checked = !isScheduled;
                EnableTriggerResetSchedule.Checked = isScheduled;
            }
        }
        Workflow.SetStepFromUrl(this.Page.Request.Url.GetLeftPart(UriPartial.Path));
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FrequencyControl.WizardGuid = Workflow.WizardGuid;

        Title += " - \"" + Workflow.Alert.Name + "\"";
    }

    private List<TimePeriodSchedule> CastListToTimePeriodSchedule(IEnumerable<ISchedule> listOfSchedules)
    {
        var reportSchedulesList = new List<TimePeriodSchedule>();
        foreach (var schedule in listOfSchedules)
        {
            reportSchedulesList.Add(schedule as TimePeriodSchedule);
        }
        return reportSchedulesList;
    }
    protected override bool SaveCurrentState()
    {
        try
        {
            if (Workflow.Alert != null)
            {
                Workflow.Alert.ExecutionTimePeriods = NoSchedule.Checked ? new List<TimePeriodSchedule>() : CastListToTimePeriodSchedule(FrequencyControl.Schedules);
            }
            return true;
        }
        catch (Exception ex)
        {
            log.Error("Error while updating alert trigger actions data", ex);
        }
        return false;
    }
}
