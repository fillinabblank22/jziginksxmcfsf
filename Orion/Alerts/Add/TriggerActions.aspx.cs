﻿using System;
using System.Collections.Generic;
using System.Linq;
using Orion.Actions.Controls;
using SolarWinds.Orion.Core.Models.Actions;
using SolarWinds.Orion.Web.Alerts;
using SolarWinds.Orion.Core.Actions.Impl.Email;
using SolarWinds.Orion.Core.Alerting.Models;

public partial class Orion_Alerts_Add_TriggerActions : AddAlertWizardBase
{
    protected override void OnInit(EventArgs e)
    {
        actionsEditorView.PreSelectedActionTypeID = EmailConstants.ActionTypeID;

        if (Workflow.Alert != null && Workflow.Alert.AlertID.GetValueOrDefault(0) != 0)
        {
            this.Title = Resources.CoreWebContent.WEBDATA_VL0_35;
        }

        if (Workflow.Alert != null)
        {
            Workflow.AlertingActionContext.ExecutionMode = ActionExecutionModeType.Trigger;
            actionsEditorView.ViewContext = Workflow.AlertingActionContext;
        }

        if (!IsPostBack)
        {
            if (Workflow.Alert != null)
            {
                var baseActionsData = Workflow.Alert.TriggerActions;
                var res = InitActions(baseActionsData);
                actionsEditorView.Actions = res;
                alertMessage.Value = Workflow.Alert.AlertMessage;
            }
        }
        Workflow.SetStepFromUrl(this.Page.Request.Url.GetLeftPart(UriPartial.Path));
        base.OnInit(e);
    }

    private List<ActionDefinition> InitActions(List<ActionDefinition> baseActionsData)
    {
        List<ActionDefinition> res = null;
        if (baseActionsData.Any())
        {
            res = baseActionsData.Select(actionInfo =>
            {
                actionInfo.IconPath = pluginInfoProvider.GetActionIconPath(actionInfo.ActionTypeID);
                return actionInfo;
            }).ToList();
        }
        return res;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        suggestionActionsCopied.Visible = false;
        noActionsCopied.Visible = false;
		Title += " - \"" + Workflow.Alert.Name + "\"";
    }

    protected override bool SaveCurrentState()
    {
        try
        {
            if (Workflow.Alert != null)
            {
                Workflow.Alert.TriggerActions.Clear();
                Workflow.Alert.TriggerActions.AddRange(actionsEditorView.Actions);
                Workflow.Alert.AlertMessage = alertMessage.Value;
                return true;
            }
        }
        catch (Exception ex)
        {
            log.Error("Error while updating alert trigger actions data", ex);
        }
        return false;
    }

    protected void CopyActionsToReset(object sender, EventArgs e)
    {
        var baseActionsData = actionsEditorView.Actions.ToList();
        List<ActionDefinition> res = InitActions(baseActionsData);
        base.CopyActionsToReset(res);

        if (res != null && res.Count > 0)
        {
            suggestionActionsCopied.Visible = true;
            noActionsCopied.Visible = false;
        }
        else
        {
            suggestionActionsCopied.Visible = false;
            noActionsCopied.Visible = true;
        }   
    }
}
