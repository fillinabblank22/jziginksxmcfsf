﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Alerts/Add/AddAlertWizard.master"
    AutoEventWireup="true" CodeFile="TimeOfDay.aspx.cs" Inherits="Orion_Alerts_Add_TimeOfDay"
    Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_36 %>" %>
<%@ Register TagPrefix="orion" TagName="FrequencyControl" Src="~/Orion/Controls/FrequencyControl.ascx" %>

<asp:Content ContentPlaceHolderID="headPlaceholder" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="wizardContent" runat="Server">
    <orion:Include File="ReportSchedule.css" runat="server" />
    <asp:Panel runat="server" DefaultButton="imgbNext">
        <div class="GroupBox">          
            <div class="wizardHeader"><h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_27) %></h2>            
            </div>

            <div class="wizardContent">
             <div class="paragraph">
               <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_23) %></span><a target="_blank" rel="noopener noreferrer" href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("orioncoreag-alertssettingtimeofday")) %>"><span class="LinkArrow">»</span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_26) %></a>
             </div>

            <div class="paragraph">          
             <asp:RadioButton ID="NoSchedule" runat="server" GroupName="timeOfDay" />
             <label for="<%= NoSchedule.ClientID %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_24) %></label><br />
             <asp:RadioButton ID="EnableTriggerResetSchedule" runat="server" GroupName="timeOfDay" />             
             <label for="<%= EnableTriggerResetSchedule.ClientID %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_25) %></label>
            </div>

             <div id="timePeriod" style="padding-bottom: 20px; padding-left: 17px;">
             <orion:FrequencyControl runat="server" UseTimePeriodMode="True" ID="FrequencyControl" SingleScheduleMode="False" Alerting="true" />
             </div>
            </div>            
        </div>
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButton runat="server" ID="imgbBack" OnClick="Back_Click" LocalizedText="Back"
                DisplayType="Secondary" CausesValidation="false" />
            <orion:LocalizableButton runat="server" ID="imgbNext" OnClientClick="return SW.Orion.FrequencyPickerController.Validate();" OnClick="Next_Click" LocalizedText="Next"
                DisplayType="Primary"  ValidationGroup="none"/>
            <orion:LocalizableButton runat="server" ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel"
                DisplayType="Secondary" CausesValidation="false" />
        </div>
    </asp:Panel>
    <script type="text/javascript">
        $(function() {
            function showHidefrequencyControl() {
                if ($("#" + "<%= NoSchedule.ClientID %>").is(":checked")) {
                    $("#timePeriod").hide();
                } else {
                    $("#timePeriod").show();
                }
            }
            showHidefrequencyControl();
            
            $(".paragraph input[type='radio']").click(showHidefrequencyControl);
        });
        var frequency = <%= SchedulesListJson %>;
        var containerTimeOfDay = $("#timePeriod");
        SW.Orion.FrequencyControllerDefinition = new SW.Orion.FrequencyController({
            container: containerTimeOfDay,
            frequencies: frequency,
            useTimePeriodMode: true,
            disableTimePeriodStr: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_OM0_1) %>',
            enableTimePeriodStr:  '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_OM0_2) %>', 
            singleScheduleMode: false
        });
        SW.Orion.FrequencyControllerDefinition.init();
    </script>
</asp:Content>
