﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Alerts/Add/AddAlertWizard.master"
    AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_Alerts_Add_Default"
    Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_36 %>" %>

<%@ Register Src="~/Orion/Nodes/Controls/CustomProperties.ascx" TagPrefix="orion"
    TagName="CustomProperties" %>
<%@ Register TagPrefix="orion" TagName="AlertTimeIntervals" Src="~/Orion/Controls/AlertTimeIntervals.ascx" %>

<asp:Content ContentPlaceHolderID="headPlaceholder" runat="server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <style type="text/css">
        .edit-cp-value-div, .blueBox { width: 100%; }
        .datePicker { width: 136px !important; }
        .helpfulText {
            color: #5F5F5F;
            font-size: 10px;
            vertical-align: middle;
        }
        .sw-validation-error {
            padding-left: 6px;
        }
        .contentBlockHeader { font-weight: bold; }
        .linktoCPE { text-align: left !important; }
        .enabledOnOffEdit {
            position:absolute; 
            z-index: 100;
            background-color: white;
            border: 1px solid;
            left: 5px;
            top: 0;
        }
    </style>
    <script type="text/javascript">
        Ext.QuickTips.init();

        // <![CDATA[
        function toggleOnOff(sender, args) {
            var div = $("#<%= alertEnabled.ClientID %>");
            if (div.hasClass("toggleOff")) {
                div.removeClass('toggleOff');
                div.addClass('toggleOn');
                div[0].innerHTML = '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YP0_20) %>';
                $("#<%= alertEnableVal.ClientID %>").val("True");
            } else {
                div.removeClass('toggleOn');
                div.addClass('toggleOff');
                div[0].innerHTML = '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_153) %>';
                $("#<%= alertEnableVal.ClientID %>").val("False");
            }
        }
    	// ]]>

    	$(function () {
    		var validateAlertName = function () {
    			var $alertName = $("#<%= this.alertName.ClientID %>");

    			if (!$alertName.val()) {
    				$alertName.addClass("sw-validation-input-error");
    			} else {
    				$alertName.removeClass("sw-validation-input-error");
    			}
    		};

    		$("#<%= this.alertName.ClientID %>").change(validateAlertName);
    		$("#<%= this.imgbNext.ClientID %>").click(validateAlertName);
    	});
    </script>
</asp:Content>
<asp:Content ID="suggestionMessageContent" ContentPlaceHolderID="WizardStepHintContent" runat="Server">
    <% if (Workflow.Alert.Canned)
       { %> 
           <div><span class='sw-suggestion' style='margin : 0px 0px 10px 0px'><span class='sw-suggestion-icon'></span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_MG0_1) %></span></div>
    <% } %>
</asp:Content>
<asp:Content ContentPlaceHolderID="wizardContent" runat="Server">
    <asp:HiddenField ID="alertEnableVal" ClientIDMode="Static" runat="server" />
    <asp:Panel runat="server" DefaultButton="imgbNext">
        <div class="GroupBox"
            <% if (Workflow.Alert.Canned)
               { %> 
                   style="position: relative"
            <% } %>>
            <% if (Workflow.Alert.Canned)
               { %> 
              <div class="editCannedAlertDisabledContainer" style="display: block"></div>
            <% } %>
            <div class="wizardHeader">
                <h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YP0_7) %></h2>
            </div>
            <div class="wizardContent">
                    <table style="padding-bottom: 35px;">
                        <tr>
                            <td style="vertical-align: middle; padding: 5px;">
                                <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YP0_8) %></b> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0031) %><br />
                                <asp:TextBox runat="server" ID="alertName" Width="600px" MaxLength="255" /><br />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="alertName" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_YP0_9 %>" ForeColor="Red" CssClass="sw-suggestion sw-suggestion-fail"></asp:RequiredFieldValidator><br />
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: middle; padding: 5px;">
                                <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YP0_10) %></b><br />
                                <span class="helpfulText" style="padding: 0px; vertical-align: top;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YP0_22) %></span><br />
                                <asp:TextBox runat="server" ID="alertDescr" TextMode="MultiLine" Columns="1" Rows="6"
                                    Width="600px" /><br />
                            </td>
                        </tr>
                        <tr>
                            <td id="alertEnabledTd" style="<%= string.Format("vertical-align: middle; padding: 5px; {0}", Workflow.Alert.Canned ? "position: relative; height: 55px" : string.Empty) %>">
                               <div id="alertEnabledContainer" 
                                    <% if (Workflow.Alert.Canned)
                                        { %> 
                                            class="enabledOnOffEdit"
                                     <% } %>>
                                  <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YP0_11) %></b><br />
                                  <a id="alertEnabled" runat="server" onclick="toggleOnOff()" href="#" class="toggleOn"></a>
                                   <% if (Workflow.Alert.Canned)
                                      { %>
                                        <span class="sw-suggestion" style="margin: 7px 2px 1px 5px;"><span class="sw-suggestion-icon"></span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_MG0_2) %></span>
                                   <% } %>
                               </div>
                               <br />
                           </td>      
                        </tr>
                        <tr>
                            <td style="vertical-align: middle; padding: 5px;">
                                <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_87) %></b><br />
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YP0_13) %>
                                <orion:AlertTimeIntervals ID="alertTimeIntervals" runat="server" />
                                <br />
                                <span class="helpfulText" style="padding: 0px; vertical-align: top;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_193) %></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: middle; padding: 5px;">
                                <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0008) %></b><br />
                                <asp:DropDownList runat="server" ID="alertSeverity" />
                                <img style="margin: 0 5px -3px 5px;" alt=""
                                    ext:qtitle=""
                                    ext:qtip="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0009) %>" src="/Orion/images/Info_Icon_MoreDecent_16x16.png" />
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: middle; padding: 5px;">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <orion:CustomProperties runat="server" ID="alertCps" NetObjectType="AlertConfigurationsCustomProperties"
                                            HelpFullHintText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_YK0_79 %>" CustomPropertiesTable="AlertConfigurationsCustomProperties"
                                            UseDefaultManageCustomPropertiesLink="false" />
                                        <br />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: middle; padding: 5px;">
                                <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AlertWizard_Default_Category) %></b><br />
                                <span class="helpfulText" style="padding: 0px; vertical-align: top;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AlertWizard_Default_CategoryDescription) %></span><br />
                                <div style="margin: 5px 0;">
                                    <asp:DropDownList runat="server" ID="ddlCategory" Width="200px" /><br />
                                </div>
                                <div id="newAlertCategory" style="display: none;">
                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AlertWizard_Default_Category_New) %><br />
                                    <asp:TextBox runat="server" ID="tbNewCategory" Width="200px" MaxLength="255" /><br />
                                    <asp:RequiredFieldValidator ID="newCategoryValidator" runat="server" ControlToValidate="tbNewCategory" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,AlertWizard_Default_Category_ValidatorWarning %>" ForeColor="Red" CssClass="sw-suggestion sw-suggestion-fail" />
                                </div>
                                <script type="text/javascript">
                                    var validator = document.getElementById("<%=newCategoryValidator.ClientID %>");
                                    $("#<%= ddlCategory.ClientID %>").change(function () {
                                        if (this.value === "<%= AddNewCategoryValue%>") {
                                        $("#newAlertCategory").show();
                                        validator.enabled = true;
                                    } else {
                                        $("#newAlertCategory").hide();
                                        validator.enabled = false;
                                    }
                                });
                                $("#<%= ddlCategory.ClientID %>").change();
                                </script>
                            </td>
                        </tr>
                    </table>
                </div>
        </div>
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButton runat="server" ID="imgbNext" OnClick="Next_Click" LocalizedText="Next"
                DisplayType="Primary" />
            <orion:LocalizableButton runat="server" ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel"
                DisplayType="Secondary" CausesValidation="false" />
        </div>
    </asp:Panel>
</asp:Content>

