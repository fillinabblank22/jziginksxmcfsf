﻿using System;
using System.Linq;
using Orion.Alerts.Add.Controls;
using SolarWinds.Orion.Core.Alerting;
using SolarWinds.Orion.Core.Alerting.Plugins.Conditions.Dynamic;
using SolarWinds.Orion.Core.Alerting.Plugins.Conditions.Models;
using SolarWinds.Orion.Core.Alerting.Plugins.Extensions;
using SolarWinds.Orion.Core.Models.Alerting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Alerts;
using SolarWinds.Orion.Core.Alerting.Models;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;

public partial class Orion_Alerts_Add_ResetCondition : AddAlertWizardBase
{
    public readonly string NewConditionEventArgument = "ResetCondition.NewCondition";
    public readonly string CopyConditionFromTriggerEventArgument = "ResetCondition.CopyFromTrigger";

	protected override void OnInit(EventArgs e)
	{
		Workflow.SetStepFromUrl(this.Page.Request.Url.GetLeftPart(UriPartial.Path));
		base.OnInit(e);

        conditionBuilder.SaveButtonClientIDs = new [] { imgbNext.ClientID, imgbBack.ClientID };

        if (Workflow.Alert != null && Workflow.Alert.AlertID.GetValueOrDefault(0) != 0)
		{
			this.Title = Resources.CoreWebContent.WEBDATA_VL0_35;
		}
	    timeIntervals.MinTimeFrequency = TimeSpan.FromSeconds(10);
	    timeIntervals.MaxTimeFrequency = TimeSpan.FromHours(108);
	    timeIntervals.DefaultTimeKindIndex = 1;

		if (!IsPostBack)
		{
            if (Workflow.Alert == null)
            {
                log.Error("Workflow.Alert is null.");
                return;
            }

            ResolveRadioButtonsAvailability();

		    if (Workflow.Alert.Reset != null) // existing definition load
            {
                ConditionChain reset = Workflow.Alert.Reset;

                if (reset.ChainType == ConditionChainType.ResetInverseToTrigger)
                {
                    if (inverseCondition.Visible)
                    {
                        inverseCondition.Checked = true;
                    }
                    else
                    {
                        inverseCondition.Checked = false;
                        automaticCondition.Checked = true;
                    }
                }
                else if (reset.ChainType == ConditionChainType.ResetAfterTimeout)
                {
                    automaticCondition.Checked = true;
                    if (reset.Conditions[0].SustainTime.HasValue)
                    {
                        timeIntervals.SetTimeFrequency(reset.Conditions[0].SustainTime.Value);
                    }
                }
                else if (reset.ChainType == ConditionChainType.ResetWhenTriggered)
                {
                    resetWhenTriggered.Checked = true;
                }
                else if (reset.ChainType == ConditionChainType.NoReset)
                {
                    noResetCondition.Checked = true;
                }
                else
                {
                    if (specialCondPanel.Visible)
                    {
                        HideInitialMenu();
                        conditionBuilder.ConditionChain = Workflow.Alert.Reset;
                        conditionBuilder.ConditionChain = WebSecurityHelper.EncodeExpressionTree(conditionBuilder.ConditionChain);
                    }
                    else
                    {
                        specialCondition.Checked = false;
                        automaticCondition.Checked = true;
                    }
                }
            }
		    else // new alert definition - first load
		    {
		        if (resetWhenTriggered.Visible)
		        {
		            resetWhenTriggered.Checked = ShouldResetWhenTriggeredAsDefault(Workflow.Alert.Trigger);
		        }

		        if (inverseCondition.Visible)
		        {
		            inverseCondition.Checked = true;
		        }
		        else
                {
                    automaticCondition.Checked = true;
		        }
		    }
		}
	}

    private bool ShouldResetWhenTriggeredAsDefault(ConditionChain trigger)
    {
        return trigger.Conditions.Any(condition =>
        {
            var dynamicCondition = condition.Condition as AlertConditionDynamic;
            return (dynamicCondition != null
                    && dynamicCondition.ExprTree.GetAllExprInTree().Any(c => c.NodeType == ExprType.Event));
        });
    }

    protected override void OnPreLoad(EventArgs e)
    {
        base.OnPreLoad(e);
        timeIntervals.ServerValidationEnabled = automaticCondition.Checked;
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (IsPostBack)
        {
            bool hideInitialMenu = false;
            // create a new condition was clicked
            if (string.Equals(Request.Params["__EVENTTARGET"], UniqueID, StringComparison.OrdinalIgnoreCase))
            {

                if (string.Equals(Request.Params["__EVENTARGUMENT"], NewConditionEventArgument))
                {
                    conditionBuilder.ConditionChain = CreateDefaultConditionChain();
                    hideInitialMenu = true;
                }
                else if (string.Equals(Request.Params["__EVENTARGUMENT"], CopyConditionFromTriggerEventArgument))
                {
                    conditionBuilder.ConditionChain = LoadExistingConditionChain();
                    hideInitialMenu = true;
                }
                else if (string.Equals(Request.Params["__EVENTARGUMENT"], AlertConditionBuilder.ImportEventArgument))
                {
                    hideInitialMenu = true;
                    conditionBuilder.ImportCondition(fileUpload);
                }

                if (hideInitialMenu)
                    HideInitialMenu();
            }
            else if (string.Equals(Request.Params["__EVENTTARGET"], conditionBuilder.UniqueID,
                StringComparison.OrdinalIgnoreCase))
            {
                if (string.Equals(Request.Params["__EVENTARGUMENT"], AlertConditionBuilder.ImportEventArgument))
                {
                    HideInitialMenu();
                }
            }

        }
    }

    protected void Page_Load(object sender, EventArgs e)
	{
		Title += " - \"" + Workflow.Alert.Name + "\"";
	}

	private ConditionChain LoadExistingConditionChain()
    {
        if (Workflow.Alert.Trigger != null && Workflow.Alert.Trigger.Conditions.Any())
        {
            return Workflow.Alert.Trigger;
        }

        return CreateDefaultConditionChain();
    }

    private void ResolveRadioButtonsAvailability()
    {
        bool supportsInverse = true;
        bool supportsCustom = true;
        bool supportsResetWhenTriggered = true;

        // check whether all trigger conditions supports inverse or custom reset types
        foreach (ConditionChainItem chainItem in Workflow.Alert.Trigger.Conditions)
        {
            supportsInverse &= chainItem.Type.SupportsConditionChainType(chainItem.Condition, chainItem.ObjectType,
                                                                         ConditionChainType.ResetInverseToTrigger);
            supportsCustom &= chainItem.Type.SupportsConditionChainType(chainItem.Condition, chainItem.ObjectType,
                                                                        ConditionChainType.ResetCustom);

            supportsResetWhenTriggered &= chainItem.Condition is AlertConditionDynamic;
        }

        if (!supportsInverse)
        {
            inverseCondition.Visible = false;
        }

        if (!supportsCustom)
        {
            specialCondition.Visible = false;
            specialCondPanel.Visible = false;
        }
    }

    private static ConditionChain CreateDefaultConditionChain()
    {
        ConditionTypeProvider conditionTypeProvider = ConditionTypeProvider.Create(AppDomainCatalog.Instance, AlertConditionHelper.GetConditionTypeServices());

        var chain = new ConditionChain();
        chain.Conditions.Add(new ConditionChainItem
                                 {
                                     Condition = new AlertConditionDynamic(),
                                     Type = conditionTypeProvider.Get(ConditionTypeDynamic.ID),
                                     ObjectType = "Node"
                                 });
        return chain;
    }

    private void HideInitialMenu()
    {
        conditionBuilder.DisableInit = false;
        specialCondition.Checked = true;
        conditionMenu.Attributes["class"] = "hidden";
        conditionBuilderContainer.Attributes["class"] = conditionBuilderContainer.Attributes["class"].Replace("hidden", string.Empty);
    }

    protected override bool SaveCurrentState()
	{
		try
		{
			if (Workflow.Alert != null)
			{
			    if (Workflow.Alert.Canned)
			    {
			        return true;
			    }

			    ConditionChain chain;
                if (inverseCondition.Checked)
                {
                    chain = AlertResetConditionProvider.CreateInverseCondition();
                }
                else if (automaticCondition.Checked)
                {
                    chain = AlertResetConditionProvider.CreateTimeoutCondition(timeIntervals.GetTimeFrequency());
                }
                else if (noResetCondition.Checked)
                {
                    chain = AlertResetConditionProvider.CreateNoResetCondition();
                }
                else if (resetWhenTriggered.Checked)
                {
                    chain = AlertResetConditionProvider.CreateResetWhenTriggeredCondition();
                }
                else
                {
                    chain = WebSecurityHelper.DecodeExpressionTree(conditionBuilder.ConditionChain);
                    foreach (var condition in chain.Conditions)
                    {
                        condition.ChainType = ConditionChainType.ResetCustom;
                    }
                }

			    Workflow.Alert.Reset = chain;
				return true;
			}
		}
		catch (Exception ex)
		{
			log.Error("Error while updating alert reset condition.", ex);
		}
		return false;
	}
}
