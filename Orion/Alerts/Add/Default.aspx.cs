﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.UI.WebControls;
using SolarWinds.Internationalization.Extensions;
using SolarWinds.Orion.Core.Alerting;
using SolarWinds.Orion.Core.Alerting.DAL;
using SolarWinds.Orion.Core.Alerting.Models;
using SolarWinds.Orion.Core.Alerting.Plugins.Conditions.Dynamic;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Models.Alerting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Alerts;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;

public partial class Orion_Alerts_Add_Default : AddAlertWizardBase
{
    //Random unique string
    protected string AddNewCategoryValue = "937C934B-1C39-43E4-B0CC-1310D7BBA3E8";

	private bool CanSetCategory()
	{
        return Profile.AllowAdmin && string.IsNullOrEmpty(Profile.AlertCategory);
	}

    protected override void OnInit(EventArgs e)
    {
		int id = 0;
        if (!IsPostBack)
        {
            alertEnabled.InnerHtml = Resources.CoreWebContent.WEBDATA_YP0_20;

            string alerttWizardGuid = Request["AlertWizardGuid"];
            Workflow.WizardGuid = (string.IsNullOrEmpty(alerttWizardGuid))
                                      ? Guid.NewGuid()
                                      : Guid.Parse(alerttWizardGuid);
            bool isDuplicated;
            Workflow.IsDuplicated = bool.TryParse(Request["IsDuplicated"], out isDuplicated) && isDuplicated;
            string alertId = Request["AlertID"];

			AlertDefinition alert = null;
            if (!string.IsNullOrEmpty(alertId) && int.TryParse(alertId, out id))
            {
                var dal = AlertDefinitionsDAL.Create(ConditionTypeProvider.Create(AppDomainCatalog.Instance, AlertConditionHelper.GetConditionTypeServices()), InformationServiceProxy.CreateV3Creator());

				try
				{
					alert = dal.Get(id);
				}
				catch (KeyNotFoundException)
				{
					Response.Redirect("../Deleted.aspx");
				}

                alertName.Text = alert.Name;
                alertDescr.Text = alert.Description;
                alertTimeIntervals.SetTimeFrequency(alert.Frequency);
                if (alert.Enabled)
                {
                    alertEnabled.Attributes.Add("class", "toggleOn");
					alertEnabled.InnerText = Resources.CoreWebContent.WEBDATA_YP0_20;
                    alertEnableVal.Value = bool.TrueString;
                }
                else
                {
                    alertEnabled.Attributes.Add("class", "toggleOff");
					alertEnabled.InnerHtml = Resources.CoreWebContent.WEBDATA_SO0_153;
                }

                Workflow.Alert = alert;
                alertCps.EntityIds = new List<int>() { id };
            }
            else
            {
                alert = Workflow.Alert;
                if (alert == null)
                {
                    AlertNotificationSettings settings = new AlertNotificationSettings();
                    settings.Enabled = true;

					alert = new AlertDefinition { Enabled = true, NotificationEnabled = true, NotificationSettings = settings, AlertMessage = Resources.CoreWebContent.AlertWizard_TriggerActions_DefaultMessage };
                    alertEnableVal.Value = bool.TrueString;
                    alert.LastEdit = DateTime.Now.ToLocalTime();
                    alert.Frequency = TimeSpan.FromMinutes(1);
                    alertTimeIntervals.SetTimeFrequency(alert.Frequency);

                    string alertObjectType = Request["ObjectType"];
                    if (!string.IsNullOrEmpty(alertObjectType))
                    {
                        alert.ObjectType = alertObjectType; //"Node";
                        string alertObject = Request["Object"]; // "<this>.Orion.Nodes|instance"
                        string alertObjectUri = HttpUtility.UrlDecode(Request["Uri"]); // "swis://vmswAlexander./Orion/Orion.Nodes/NodeID=4"
                        
                        AlertConditionDynamic condition = new AlertConditionDynamic()
                        {
                            ExprTree = 
                                new Expr(ExprType.Operator, "AND",
                                    new Expr(ExprType.Operator, "",
                                        new Expr(ExprType.Field, ""))),
                            Scope = 
                                new Expr(ExprType.Operator, "AND",
                                    new Expr(ExprType.Operator, "IS",
                                        new Expr(ExprType.Field, alertObject),
                                        new Expr(ExprType.Constant, "",
                                            new Expr(ExprType.Constant,
                                                alertObjectUri))
                                        ))
                        };

                        ConditionChainItem item = new ConditionChainItem
                        {
                            ChainType = ConditionChainType.Trigger,
                            ObjectType = alertObjectType,
                            Condition = condition
                        };

                        ConditionTypeProvider conditionTypeProvider = ConditionTypeProvider.Create(AppDomainCatalog.Instance, AlertConditionHelper.GetConditionTypeServices());
                        item.Type = conditionTypeProvider.Get(ConditionTypeDynamic.ID);
                        alert.Trigger = new ConditionChain()
                        {
                            Conditions = new List<ConditionChainItem>() {item},
                        };
                    }
                    
                    Workflow.Alert = alert;
                }
                else
                {
                    alertName.Text = alert.Name;
                    alertDescr.Text = alert.Description;
                    alertTimeIntervals.SetTimeFrequency(alert.Frequency);
                    alertEnableVal.Value = alert.Enabled.ToString();
                    if (alert.Enabled)
                    {
                        alertEnabled.Attributes.Add("class", "toggleOn");
						alertEnabled.InnerText = Resources.CoreWebContent.WEBDATA_YP0_20;
                    }
                    else
                    {
                        alertEnabled.Attributes.Add("class", "toggleOff");
						alertEnabled.InnerHtml = Resources.CoreWebContent.WEBDATA_SO0_153;
                    }
                }
            }
            foreach (AlertSeverity value in AlertSeverityHelper.GetOrderedAlertSeverities())
            {
                bool selected = alert.Severity == value;
                alertSeverity.Items.Add(new ListItem(value.LocalizedLabel(), value.ToString()) { Selected = selected });
            }

			this.ddlCategory.Items.Add(new ListItem(Resources.CoreWebContent.AlertWizard_Default_Category_NoLimitation, string.Empty));
			using (var swis3 = InformationServiceProxy.CreateV3())
			{
				var categories =
					swis3.Query(
@"SELECT DISTINCT AlertCategory FROM 
(SELECT AlertCategory FROM Orion.Accounts
 UNION (SELECT Category AS AlertCategory FROM Orion.AlertConfigurations))
WHERE (AlertCategory IS NOT NULL) AND (AlertCategory <> '') ORDER BY AlertCategory");
				foreach (var row in categories.Rows.Cast<DataRow>())
				{
					var category = Convert.ToString(row[0]);
					bool selected = alert.Category == category;
					this.ddlCategory.Items.Add(new ListItem(category, category) { Selected = selected });
				}
			}
			this.ddlCategory.Items.Add(new ListItem(Resources.CoreWebContent.AlertWizard_Default_Category_AddNew, AddNewCategoryValue));
			if (!CanSetCategory())
			{
				this.ddlCategory.SelectedValue = Profile.AlertCategory;
				this.ddlCategory.Enabled = false;
			}
        }
        else
        {
	        if (ddlCategory.SelectedValue != AddNewCategoryValue)
		        newCategoryValidator.Enabled = false;
        }

		if (Workflow.Alert != null && Workflow.Alert.AlertID.HasValue)
        {
			this.Title = Resources.CoreWebContent.WEBDATA_VL0_35;
        }

		Workflow.SetInitialRedirect(id > 0 ? Workflow.Steps.Count - 1 : 0);
        alertTimeIntervals.MinTimeFrequency = TimeSpan.FromSeconds(15);
        Workflow.FirstStep();
        base.OnInit(e);
    }

    protected override void OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);
        
        if (!IsPostBack)
        {
            var alert = Workflow.Alert;
            if (alert != null)
            {
                alertCps.CustomProperties = alert.CustomProperties;
                if (alert.Canned)
                {
                    foreach (var validator in Validators)
                    {
                        ((BaseValidator)validator).Enabled = false;
                    }
                }
            }

			this.alertCps.Caption = string.Format(Resources.CoreWebContent.WEBDATA_BV0_0006 + " ({0})", alertCps.CustomProperties.Count);
        }
	}

    protected override bool ValidateUserInput()
    {
        Page.Validate("EditCpValue");

        return Page.IsValid;
    }

    protected override bool SaveCurrentState()
    {
        try
        {
            if (Workflow.Alert != null && Workflow.Alert.Canned)
            {
                Workflow.Alert.Enabled = alertEnableVal.Value.Equals(bool.TrueString);
                return true;
            }
            string categoryName = Profile.AlertCategory;
            if (CanSetCategory())
            {
                categoryName = ddlCategory.SelectedValue == AddNewCategoryValue
                    ? (string)DefaultSanitizer.SanitizeHtml(tbNewCategory.Text)
                    : ddlCategory.SelectedValue;
            }
			
            var customProperties = alertCps.CustomProperties;
            foreach (var CpKey in customProperties.Keys.ToArray())
            {
                customProperties[CpKey] = DefaultSanitizer.SanitizeHtml(customProperties[CpKey]);
            }

            AlertDefinition alert = Workflow.Alert;
            alert.Name = (string)DefaultSanitizer.SanitizeHtml(alertName.Text);
            alert.Description = (string)DefaultSanitizer.SanitizeHtml(alertDescr.Text);
            alert.Enabled = alertEnableVal.Value.Equals(bool.TrueString);
			alert.Severity = (AlertSeverity)Enum.Parse(typeof(AlertSeverity), alertSeverity.SelectedValue);
            alert.CustomProperties = customProperties;
            alert.Frequency = alertTimeIntervals.GetTimeFrequency().Value;
	        alert.Category = categoryName;

            return true;
        }
        catch (Exception ex)
        {
            log.Error("Error while updating alert properties", ex);
            return false;
        }
    }
}