<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CannedAlertsLog.aspx.cs" Inherits="Orion_Alerts_CannedAlertsLog" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" 
    Title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_MD0_02 %>" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>
<asp:Content ID="cannedLogHeader" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">

    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js" />
     <orion:Include runat="server" File="Alerts/js/CannedAlertsLogGrid.js" />

    <style type="text/css">
        .statusDescription {
            font-size: smaller;
            color: grey;
        }
        .statusLabel {
            font-weight: bold;
        }
        .statusIcon {
            margin-right: 3px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" runat="server" Visible="false">
    <% if (Profile.AllowAlertManagement)
    { %>
        <a href="/Orion/Alerts/Default.aspx" style="background: transparent url(/Orion/images/ActiveAlerts/ManageAlerts.gif) scroll no-repeat left center; padding: 2px 0px 2px 20px; font-size: 8pt;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_128) %></a>
    <% } %> 
</asp:Content>
<asp:Content ID="cannedLogContent" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <h1 style="font-size:large"><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    <div style="padding: 1em 0 1em 0;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_MD0_01) %></div>
   <div style="margin-top:20px" id="alertMigrationGridPlace">
    </div>
        <script type="text/javascript">
            Ext.onReady(function () {
                var cannedAlertsLogGrid = new SW.Core.CannedAlertsLogGrid();
                cannedAlertsLogGrid.Init('<%= ImportId%>');
            });
    </script>
</asp:Content>