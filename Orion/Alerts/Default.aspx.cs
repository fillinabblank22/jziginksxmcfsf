using System;
using System.IO;
using System.Web;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Alerting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Alerts;
using System.Web.UI;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Alerts_Default : System.Web.UI.Page
{
	private static readonly Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    protected bool UserHasRightToEnableOrDisableAlert
    {
        get { return Profile.AllowDisableAlert; }
    }

    private void ShowHint(string name, bool success = true)
    {
        if (string.IsNullOrEmpty(name)) return;
        
        Validation.Attributes.Remove("sw-suggestion-pass");
        Validation.Attributes.Remove("sw-suggestion-fail");
        Validation.Attributes.Add("class", success ? "sw-suggestion sw-suggestion-pass sw-suggestion-border" : "sw-suggestion sw-suggestion-fail sw-suggestion-border");       

        Validation.Visible = true;
        Validation.Style["display"] = "block";
        Validation.Controls.Add(new LiteralControl("<span style='font-weight: normal;'> "+
            string.Format(success ? Resources.CoreWebContent.WEBDATA_YK0_78 : Resources.CoreWebContent.WEBDATA_ZS0_1,
            "<span id='alertName'>" + HttpUtility.HtmlEncode(name.Replace(":hightlight=true", "")) + "</span>") +
            "</span>"));

        if (name.IndexOf(":hightlight=true", StringComparison.OrdinalIgnoreCase) > 0)
            Validation.Style["display"] = "none";
    }

    protected override void OnInit(EventArgs e)
    {
        if (!Profile.AllowAlertManagement && !(this.Page is IBypassAccessLimitation))
        {
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new SolarWinds.Internationalization.Exceptions.LocalizedExceptionBase(() => Resources.CoreWebContent.WEBDATA_SO0_188), Title = Resources.CoreWebContent.WEBDATA_VB0_567 });
        }
    }

    private void registerScript(string errorsMessage)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "myalert", string.Format("Ext.Msg.show({0});", errorsMessage), true);
    }

    protected void Page_Load(object sender, EventArgs e)
	{
		btnEditDraft.NavigateUrl = EditDraftURL;

        if (IsPostBack && importFileDialog.HasFile && Page.Request.Params.Get("__EVENTTARGET").EndsWith("importFileDialog"))
        {
            try
            {
                AlertImportResult importResult;
                string importAlertFileContent = string.Empty;
                using (var blProxy = _blProxyCreator.Create(ex => log.Error("Import failed during business layer call ", ex), EnginesDAL.GetPrimaryEngineId()))
                {
                    var streamReader = new StreamReader(importFileDialog.FileContent);
                    importAlertFileContent = streamReader.ReadToEnd();
                    importResult = blProxy.ImportAlertConfiguration(importAlertFileContent, HttpContext.Current.Profile.UserName, true, true, true, false, "testPass");
                }
                
                if (importResult.AlertId > 0 && !string.IsNullOrEmpty(importResult.Name))
                {
                    ShowHint(importResult.Name);
                }
                else if (importResult.IncorrectPasswordForDecryptSensitiveData)
                {
                    hdnImportAlertFileContent.Value = importAlertFileContent;
                    ClientScript.RegisterStartupScript(this.GetType(), "displayalertimportdialog", $"$(function() {{ SW.Orion.AlertManager.showImportFileDialog(\"{hdnImportAlertFileContent.ClientID}\", \"{importFileDialog.FileName}\"); }})", true);
                }
                else
                {
                    ShowHint(importFileDialog.FileName, false);
                    log.ErrorFormat("Failed to import alert from {0}! Migration message: {1}", importFileDialog.FileName, string.IsNullOrEmpty(importResult.MigrationMessage) ? "" : importResult.MigrationMessage);
                }
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Failed to import alert from {0}! Error: {1}", importFileDialog.FileName, ex);
                this.registerScript(
                    string.Format(
                        "{{title: '{0}', msg: '{1}', minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR}}",
                        Resources.CoreWebContent.WEBDATA_SO0_177,
                        HttpUtility.HtmlEncode(string.Format(Resources.CoreWebContent.WEBDATA_SO0_178,
                            importFileDialog.FileName))));
            }
        }
        else
        {
            int id;
            bool stripSensitiveData = false;
            if (int.TryParse(Request["ExportAlertID"], out id) && bool.TryParse(Request["StripSensitiveData"], out stripSensitiveData))
            {
                using (var blProxy = _blProxyCreator.Create(ex => log.Error("Import failed during business layer call ", ex), EnginesDAL.GetPrimaryEngineId()))
                {
                    string alertXml = blProxy.ExportAlertConfiguration(id, stripSensitiveData, Request["ProtectionPassword"]);
                    byte[] data = System.Text.Encoding.UTF8.GetBytes(alertXml);
                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.ContentType = "text/xml";
                    HttpContext.Current.Response.AppendHeader("Content-Disposition", string.Format("attachment; filename= {0}.xml", HttpUtility.UrlEncode(HttpUtility.UrlDecode(Request["ExportAlertName"], System.Text.Encoding.UTF8), System.Text.Encoding.UTF8)));
                    HttpContext.Current.Response.AddHeader("Content-Length", data.Length.ToString());
                    HttpContext.Current.Response.ContentType = "application/octet-stream";
                    HttpContext.Current.Response.BinaryWrite(data);
                    HttpContext.Current.Response.End();
                }
            }
            else
            {
                var alertName = Session["CreatedAlertName"];
                if (alertName != null)
                {
                    ShowHint(alertName.ToString());
                    Session["CreatedAlertName"] = null;
                }
            }
        }
	}

	protected void RemoveDraft(object sender, EventArgs e)
	{
		AlertWorkflowDraftCache.CleanDraftForCurrentUser();
	}

	public string EditDraftURL
	{
		get
		{
			return AlertWorkflowDraftCache.DraftExists()
					   ? String.Format("/Orion/Alerts/Add/Default.aspx?AlertWizardGuid={0}", AlertWorkflowDraftCache.GetCurrentUserDraft().WorkflowId)
					   : String.Empty;
		}
	}
}