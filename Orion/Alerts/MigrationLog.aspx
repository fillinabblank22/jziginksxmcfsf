<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MigrationLog.aspx.cs" Inherits="Orion_Alerts_MigrationLog" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" Title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_IT0_14 %>" %>
<asp:Content ID="migrationLogHeader" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">

    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js" />
     <orion:Include runat="server" File="Alerts/js/AlertMigrationLogGrid.js" />

    <style type="text/css">
        .statusDescription {
            font-size: smaller;
            color: grey;
        }
        .statusLabel {
            font-weight: bold;
        }
        .statusIcon {
            margin-right: 3px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" runat="server" Visible="false">
    <% if (Profile.AllowAlertManagement)
    {%>
        <a href="/Orion/Alerts/Default.aspx" style="background: transparent url(/Orion/images/ActiveAlerts/ManageAlerts.gif) scroll no-repeat left center; padding: 2px 0px 2px 20px; font-size: 8pt;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_128) %></a>
    <% } %> 
</asp:Content>
<asp:Content ID="migrationLogContent" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <h1 style="font-size:large"><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    <div style="padding: 1em 0 1em 0;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IT0_15) %></div>
    <div style="margin-bottom: 10px">
        <div style="float: left; margin-right: 5px">
            <img src="/Orion/Images/ok_16x16.gif" alt/>
        </div>
        <div>
            <span class="statusLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IT0_16) %></span>
            <span id="successCount"></span>&nbsp;<span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_VB0_241) %></span><br/>
            <span class="statusDescription"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IT0_19) %></span>
        </div>
    </div>
    <div style="margin-bottom: 8px">
        <div style="float: left; margin-right: 5px">
            <img src="/Orion/Images/warning_16x16.gif" alt/>
        </div>
        <div>
            <span class="statusLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_41) %></span>
            <span id="warningCount"></span>&nbsp;<span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_VB0_241) %></span><br/>
            <span class="statusDescription"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IT0_20) %></span>
        </div>
    </div>
    <div style="margin-bottom: 8px">
        <div style="float: left; margin-right: 5px">
            <img src="/Orion/Images/failed_16x16.gif" alt/>
        </div>
        <div>
            <span class="statusLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_AY0_118) %></span>
            <span id="failedCount"></span>&nbsp;<span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_AY0_116) %></span><br/>
            <span class="statusDescription"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_AY0_117) %></span>
        </div>
    </div>
    <div style="margin-bottom: 8px">
        <div style="float: left; margin-right: 5px">
            <img src="/Orion/Images/skipped_16x16.png" alt/>
        </div>
        <div>
            <span class="statusLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IT0_18) %></span>
            <span id="skippedCount"></span>&nbsp;<span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_VB0_241) %></span><br/>
            <span class="statusDescription"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IT0_22) %></span>
        </div>
    </div>
    <div style="margin-top:20px" id="alertMigrationGridPlace">
    </div>
        <script type="text/javascript">
            Ext.onReady(function () {
                var  alertMigrationLogGrid = new SW.Core.AlertMigrationLogGrid();
                alertMigrationLogGrid.Init('<%= MigrationId%>');
            });
    </script>
</asp:Content>