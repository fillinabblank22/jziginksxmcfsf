﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Visualize.aspx.cs" Inherits="Orion_Reports_Visualize" MasterPageFile="~/Orion/OrionMinReqs.master" %>
<%@ Register Assembly="OrionWeb" Namespace="SolarWinds.Orion.Web.Reporting" TagPrefix="orionrpt" %>
<%@ Register TagPrefix="orion" TagName="NetObjectTips" Src="~/Orion/Controls/NetObjectTips.ascx" %>

<script runat="server">
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    
    protected void ScriptAsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
    {
        this.MasterScriptManager.AsyncPostBackErrorMessage = e.Exception.Message;
    }
</script>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="BodyContent">
<div id="container">
<div id="content" style="padding-top: 8px;">

<form runat="server" method="post" id="aspnetForm" class="sw-app-region">
<%-- <orion:NetObjectTips ID="NetObjectTips1" runat="server" /> --%>

<div style="text-align: right;">
    <span style="border: 1px solid black; padding: 2px 4px 2px 4px; margin-right: 8px;">
        Adhoc Edit Mode
    </span>
    <span style="border: 1px solid black; padding: 2px 4px 2px 4px; margin-right: 8px;">
        Print
    </span>
    <span style="border: 1px solid black; padding: 2px 4px 2px 4px; margin-right: 8px;">
        Export
    </span>
</div>

    <asp:ScriptManager runat="server" ID="MasterScriptManager" EnablePageMethods="true" AllowCustomErrorsRedirect="false" EnablePartialRendering="true" OnAsyncPostBackError="ScriptAsyncPostBackError" EnableScriptGlobalization="true" ></asp:ScriptManager>
    <orionrpt:ReportPreviewControl ID="Preview" runat="server" />
</form>

</div>
</div>

</asp:Content>