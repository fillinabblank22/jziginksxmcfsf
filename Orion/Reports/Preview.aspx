<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true"
    EnableSessionState="ReadOnly" CodeFile="Preview.aspx.cs" Inherits="Orion_Reports_Preview" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_141 %>" %>

<%@ Register Assembly="OrionWeb" Namespace="SolarWinds.Orion.Web.Reporting" TagPrefix="orionrpt" %>
<%@ Register TagPrefix="xui" TagName="Include" Src="~/Orion/Controls/XuiInclude.ascx" %>

<asp:Content ContentPlaceHolderID="headPlaceholder" runat="server">    
    <xui:Include runat="server" />
    <orion:include id="Include2" runat="server" file="Breadcrumb.css" />
    <style type="text/css">
        body.sw-pg-hidehubble #hubbleWindow { display: none; }
        
        /* *_pdfsig_4C5187C5165A4075878201F882534984__4C5187C5165A4075878201F882534984_pdfsig_/
            .sw-pg-rpt-wrap { padding-top: 0px !important; }
            #pageHeader, #footer, .sw-pg-rpthide, #hubbleWindow { display: none; }
        /* */

        a.sw-pg-backlink:hover
        {
            color: orange;
        }
        a.sw-pg-backlink, a.sw-pg-backlink:active, a.sw-pg-backlink:visited
        {
            color: #336699;
            text-decoration: none;
        }        
        .GroupBox
        {
	       border: solid 1px #dbdcd7;
	       background-color:#fff;
	       margin-right: 0;
	       margin-bottom: 0;
	       margin-top: 0;
	       padding: 10px 10px 0px 10px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="padding: 5px;">
         <% if (Profile.AllowAdmin || Profile.AllowReportManagement) { %>
        <div class="sw-pg-rpthide"><orion:DropDownMapPath ID="OrionSiteMapPath" runat="server"  Provider="AdminSitemapProvider" OnInit="OrionSiteMapPath_OnInit">
            <RootNodeTemplate>
                <a href="<%# DefaultSanitizer.SanitizeHtml(Eval("url")) %>" ><u> <%# DefaultSanitizer.SanitizeHtml(Eval("title")) %></u> </a>
            </RootNodeTemplate>
            <CurrentNodeTemplate>
             <a href="<%# DefaultSanitizer.SanitizeHtml(Eval("url")) %>" ><%# DefaultSanitizer.SanitizeHtml(Eval("title")) %> </a>
         </CurrentNodeTemplate>
        </orion:DropDownMapPath></div>
        <%} %>
    <div class="sw-pg-rpt-wrap" style="padding-top: 30px;">
        <table cellspacing="0" cellpadding="0" style="vertical-align: top;">
            <tr><td>
                <div class="sw-pg-rpthide" style="padding-bottom: 5px;">
                    <div class='sw-hdr-links sw-hdr-links-nested'>
                    <% if (Profile.AllowReportManagement) { %>
                        <a href="javascript:goToEditReportPage();" class='sw-hdr-links-edit'><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_66) %></a>&nbsp;
			        <%} %>
                        <orion:IconLinkExportToExcel ID="IconLinkExportToExcel" runat="server" />
                        <orion:IconLinkExportToPDF ID="IconLinkExportToPDF1" runat="server" />
                        <a class="sw-hdr-links-print" href="#" onclick="SW.pg.PrintableStart(); window.print(); SW.pg.PrintableStop(); return false;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_86) %></a>
                    </div>
					<orion:LocalizableButtonLink ID="backLink" runat="server" LocalizedText="CustomText"/>
                </div>
                <div class="GroupBox">
                    <orionrpt:reportpreviewcontrol id="Preview" runat="server"/>
                </div>
            </td></tr>
        </table>
    </div>
    </div>
    <script type="text/javascript">
    // <![CDATA[

        SW.Core.Export.Excel._filename = 'Report_<%= HttpUtility.JavaScriptStringEncode(Page.Title) %>';

        (function (){
            var pg = SW.Core.namespace('SW.pg'), ptop = null, isprintable = false;

            pg.PrintableStart = function () {
                if (isprintable) return;
                isprintable = true;
                ptop = $('.sw-pg-rpt-wrap').css('padding-top');
                $('.sw-pg-rpt-wrap').css('padding-top', '0px');
                $('#pageHeader').hide();
                $('#footer').hide();
                $('.sw-pg-rpthide').hide();
                $('body').addClass('sw-pg-hidehubble');
            };

            pg.PrintableStop = function () {
                if (!isprintable) return;
                isprintable = false;
                $('.sw-pg-rpt-wrap').css('padding-top', ptop);
                $('#pageHeader').show();
                $('#footer').show();
                $('.sw-pg-rpthide').show();
                $('body').removeClass('sw-pg-hidehubble');
            };
        })();

        <% if (OrionMinReqsMaster.IsPrintable) { %>
            $(SW.pg.PrintableStart);
        <% } %>

        // _pdfsig_4C5187C5165A4075878201F882534984_ pdf strip start
        /* // pdf strip end _4C5187C5165A4075878201F882534984_pdfsig_
            SW.pg.PrintableStart();
        // _pdfsig_4C5187C5165A4075878201F882534984_ pdf strip start
        */ // pdf strip end _4C5187C5165A4075878201F882534984_pdfsig_

        //FB219269 - remove this when detach resource is handled in report rendering
        $('a[href*="DetachResource"]').click(function () { return false; });
        
        function goToEditReportPage() {
            var stringForm = "<form id='reportDataForm' action='/Orion/Reports/Add/Default.aspx?ReportWizardGuid=<%= WizardGuid %>' method='POST' target='_blank'> \
                <input type='hidden' name='ReportID' value='<%= Preview.ReportId %>'/> \
            </form>";

            $('body').append(stringForm);
            $('#reportDataForm').submit();
        }
    // ]]>
    </script>
</asp:Content>
