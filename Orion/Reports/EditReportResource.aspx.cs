﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.UI;

using SolarWinds.Orion.Core.Reporting.DAL;
using SolarWinds.Orion.Core.Reporting.Models.WebResource;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Reporting.Models.Charts;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Reporting.Models.Data;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Reporting.Models.Layout;
using SolarWinds.Reporting.Models.Selection;
using SolarWinds.Reporting.Models.Tables;
using SolarWinds.Orion.Web.Reporting;
using SolarWinds.Reporting.Impl.Rendering;
using SolarWinds.Reporting.Models.Timing;

public partial class Orion_Reports_EditReportResource : Page, INotifyOnSave
{
    private Control _editControl;
    private string _returnUrl;

    private bool configurationDataUpdated = false;
    public event System.EventHandler BeforeSave;

    public ResourceInfo Resource
    {
        get;
        set;
    }

    public string NetObjectId
    {
        get;
        set;
    }

    public ConfigurationData[] GetCongifsFromSession()
    {
        var sessionId = Request["SessionId"];
        if (Session[sessionId] != null)
        {
            return (ConfigurationData[]) Session[sessionId];

        }
        return null;
    }

    public void UpdateSectionsConfig(ConfigurationData[] sections)
    {
        Session[Request["SessionId"]] = sections;
    }

    public ConfigurationData GetWebResourceConfigurationById(Guid id)
    {
        var configs = GetCongifsFromSession();
        
        if (configs == null) return null;

        foreach (var config in configs)
        {
            if (config.RefId == id)
                return config;
        }
        return null;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        var config = GetWebResourceConfigurationById(new Guid(Request["RefId"]));
        if (config != null)
        {
            
            if (config is TableConfiguration)
            {
                Resource = new ResourceInfo {File = WebConstants.ReportTableWrapperPath};
            }
            else if (config is ChartConfiguration)
            {
                Resource = new ResourceInfo { File = WebConstants.ReportChartWrapperPath };
            }
            else
            {
                Resource = new ResourceInfo { File = ((WebResourceConfiguration)config).ResourceFile };
                foreach (var setting in ((WebResourceConfiguration)config).Settings)
                {
                    Resource.Properties.Add(setting.Name, setting.Value);
                }
            }

            var dataSource = GetCurrentDataSource();
            if (dataSource != null && dataSource.Type == DataSourceType.Entities && dataSource.EntityUri.Length > 0)
                NetObjectId = UriHelper.GetNetObjectId(dataSource.EntityUri[0], dataSource.MasterEntity);

            // try to get object id from resource properties in case of custom object resource
            if (string.IsNullOrEmpty(NetObjectId))
                NetObjectId = GetCustomObjectId();

            LoadResourceEditControl();
        }
        

        ReferrerRedirectorBase.GetDecodedReferrerUrlOrDefault(_returnUrl);
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var config = GetWebResourceConfigurationById(new Guid(Request["RefId"]));
            if (config != null)
            {
                TitleEditControl.ResourceTitle = config.DisplayTitle;
                TitleEditControl.ResourceSubTitle = config.DisplaySubTitle;
                Title = String.Format(Resources.CoreWebContent.WEBCODE_AK0_88, config.DisplayTitle);
            }
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (IsPostBack && !configurationDataUpdated)
        {
            // following code is needed, because we need also set data in cache in the case of click on Preview button
            if (UpdateConfigurationByPostData()) return;
            configurationDataUpdated = true;
        }

        //Update the datasource/timeperiod description in the title
        string resourceNameText = string.Empty;
        var store = GetReportStorageData();
        var timeframe = store == null || !store.IsTimeFrameSupported() ? null : store.GetTimeFrame();
        using (var stringWriter = new StringWriter())
        {
            using (var writer = new HtmlTextWriter(stringWriter))
            {
                RenderCellHeader.Render(writer, GetCurrentDataSource(), timeframe, null, TitleEditControl.ResourceTitle, string.Empty);
                resourceNameText = stringWriter.GetStringBuilder().ToString();
            }
        }
        ResourceName.Text = resourceNameText;
    }

    private ContextValue[] AddUpdateSettings(ContextValue[] original, ContextValue[] newValues)
    {
        var combinedSettings = original.ToList();
        foreach (var val in newValues)
        {
            var originalMatch = combinedSettings.FirstOrDefault( v => v.Name == val.Name);
            if (originalMatch == null)
                combinedSettings.Add(val);
            else
                originalMatch.Value = val.Value;
        }
        return combinedSettings.ToArray();
    }

    public void OnBeforeConfigurationSave(EventArgs e)
    {
        if (BeforeSave != null)
            BeforeSave(this, e);
    }

    public void SubmitClick(object sender, EventArgs e)
    {
        OnBeforeConfigurationSave(EventArgs.Empty);

        if (UpdateConfigurationByPostData()) return;
        configurationDataUpdated = true;

        ReferrerRedirectorBase.Return(_returnUrl);
    }

    private bool UpdateConfigurationByPostData()
    {
        if (Resource == null)
            return true;

        Page.Validate();
        if (!Page.IsValid)
            return true;

        string resTitle = TitleEditControl.ResourceTitle;
        string resSubTitle = TitleEditControl.ResourceSubTitle;

        var settings = new List<ContextValue>();

        if (_editControl != null)
        {
            var ct = _editControl as BaseResourceEditControl;
            if (ct != null)
            {
                foreach (var property in ct.Properties)
                {
                    var contextValue = new ContextValue {Name = property.Key, Value = property.Value.ToString()};
                    if (!settings.Contains(contextValue))
                        settings.Add(contextValue);
                }

                if (string.IsNullOrEmpty(resTitle) && !string.IsNullOrEmpty(ct.DefaultResourceTitle))
                    resTitle = ct.DefaultResourceTitle;
                if (string.IsNullOrEmpty(resSubTitle) && !string.IsNullOrEmpty(ct.DefaultResourceSubTitle))
                    resSubTitle = ct.DefaultResourceSubTitle;
            }
        }


        var configs = GetCongifsFromSession();
        var configId = new Guid(Request["RefId"]);
        
        for (var i = 0; i < configs.Length; i++)
        {
            if (configs[i].RefId == configId)
            {
                if (configs[i] is WebResourceConfiguration)
                {
                    ((WebResourceConfiguration) configs[i]).Settings =
                        AddUpdateSettings(((WebResourceConfiguration) configs[i]).Settings, settings.ToArray());
                }
                else if (!string.IsNullOrEmpty(Request["ReportStorageGuid"]))
                {
                    var report = ReportStorage.Instance.GetReport(new Guid(Request["ReportStorageGuid"]));
                    if (report != null && report.Configs != null)
                    {
                        foreach (var configurationData in report.Configs)
                        {
                            if (configurationData.RefId == configs[i].RefId)
                            {
                                configs[i] = configurationData;
                                break;
                            }
                        }
                    }
                }
                configs[i].DisplayTitle = resTitle;
                configs[i].DisplaySubTitle = resSubTitle;
                break;
            }
        } 
        
        UpdateSectionsConfig(configs);
        return false;
    }

    private string GetCustomObjectId()
    {
        if (!string.IsNullOrEmpty(Resource.Properties["ObjectFilter"])
                && !string.IsNullOrEmpty(Resource.Properties["ObjectUri"]))
        {
            string filter = Resource.Properties["ObjectFilter"];
            if (!string.IsNullOrEmpty(filter) && filter.Split('$').Length == 3)
                return UriHelper.GetNetObjectId(Resource.Properties["ObjectUri"], filter.Split('$')[0]); ;
        }

        return string.Empty;
    }

    private void LoadResourceEditControl()
    {
        const string chartsV2EditControlLocation = "/Orion/NetPerfMon/Controls/EditResourceControls/EditChart.ascx";
        if (Resource == null)
            return;
        Control control = LoadControl(Resource.File);
        if (control is BaseResourceControl)
        {
            string editControlLocation = (control as BaseResourceControl).EditControlLocation;
            if (control is StandardChartResource)
                editControlLocation = chartsV2EditControlLocation;
            control.Dispose();

            if (!string.IsNullOrEmpty(editControlLocation))
            {
                _editControl = LoadControl(editControlLocation);

                if (_editControl is BaseResourceEditControl)
                {
                    TitleEditControl.ShowSubTitle = (_editControl as BaseResourceEditControl).ShowSubTitle;
                    TitleEditControl.ShowSubTitleHintMessage = (_editControl as BaseResourceEditControl).ShowSubTitleHintMessage;
                    TitleEditControl.SubTitleHintMessage = (_editControl as BaseResourceEditControl).SubTitleHintMessage;
                    (_editControl as BaseResourceEditControl).Resource = Resource;
                    (_editControl as BaseResourceEditControl).NetObjectID = NetObjectId;
                    phResourceEditControl.Controls.Add(_editControl.TemplateControl);
                    foreach (Control button in (_editControl as BaseResourceEditControl).ButtonsForButtonBar)
                    {
                        phAdditionalButtons.Controls.Add(button);
                    }
                }
            }
        }
    }

    private DataSource GetCurrentDataSource()
    {
        if (!String.IsNullOrEmpty(Request["ReportStorageGuid"]))
        {
            var report = ReportStorage.Instance.GetReport(Guid.Parse(Request["ReportStorageGuid"]));

            Guid dataSourceGuid;
            if (report != null && report.DataSources != null && Guid.TryParse(Request["DatasourceID"], out dataSourceGuid))
            {
                return report.DataSources.FirstOrDefault(dataSource => dataSource.RefId == dataSourceGuid);
            }
        }
        return null;
    }

    private ReportStorageData GetReportStorageData()
    {
         if (!String.IsNullOrEmpty(Request["ReportStorageGuid"]) && !String.IsNullOrEmpty(Request["SectionCellRefId"]))
        {
            try
            {
                var reportStorageGuid = Guid.Parse(Request["ReportStorageGuid"]);
                var sectionCellRefId = Guid.Parse(Request["SectionCellRefId"]);
                var store = new ReportStorageData(reportStorageGuid, sectionCellRefId);
                return store;
            }
            catch (Exception)
            {
                //report guid may not be viable if session expires.
            }
        }
        return null;
    }
    
}
