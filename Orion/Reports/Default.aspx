<%@ Page Language="C#" MasterPageFile="~/Orion/Reports/ReportsMasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_Reports" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_52 %>" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Reporting.DAL" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Swis" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/Reports/Controls/ReportTabBar.ascx" TagPrefix="orion" TagName="ReportTabBar" %>
<%@ Register TagPrefix="orion" TagName="SchedulePicker" Src="~/Orion/Reports/Controls/SchedulePicker.ascx" %>
<%@ Register TagPrefix="orion" TagName="ImportReportSelection" Src="~/Orion/Reports/Import/ImportReportSelection.ascx" %>

<asp:Content ContentPlaceHolderID="headplaceholder" runat="server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include ID="Include2" runat="server" File="OrionCore.js" />
    <orion:Include ID="Include3" runat="server" File="Admin/js/SearchField.js" />
<style type="text/css">
  #mainContent h1 { font-size: large; padding: 15px 0px 10px 10px; }
  .x-panel-bwrap {border-width: 1px;}
  .import-file-dialog {
        display: none;
        position: absolute;
        -moz-opacity: 0;
        width: 10px;
        font-size: 100px; /*to ensure we click on button but not on text field*/
        overflow: hidden;
        cursor: pointer;
        filter: alpha(opacity: 0);
        opacity: 0;
        z-index: 99999;
    }
</style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" runat="server">
        <div class="contTitleBarDate">
		    <orion:IconHelpButton ID="btnHelp" runat="server"  HelpUrlFragment="OrionCoreAGCreatingViewingReports" />
		    <div style="padding-top: 5px; font-size: 8pt; text-align: right;"><%= DateTime.Now.ToString("F") %></div>
	    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceholder" Runat="Server">
    <input type="hidden" id="grid_page_size" value="<%= HttpUtility.HtmlEncode(WebUserSettingsDAL.Get("ReportManager_PageSize")) %>" />
    <input type="hidden" id="grid_selected_columns" value="<%= HttpUtility.HtmlEncode(WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, "ReportManager_SelectedColumns", WebConstants.AllReportsDefaultSelectedColumns)) %>" />
    <input type="hidden" id="grid_sort_order" value="<%= HttpUtility.HtmlEncode(WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, "ReportManager_SortOrder", WebConstants.AllReportsDefaultSortOrder)) %>" />
    <input type="hidden" id="grid_grouping_value" value="<%= HttpUtility.HtmlEncode(WebUserSettingsDAL.Get("ReportManager_GroupingValue")) %>" />
<script type="text/javascript">
    function reportDraftExists() { return <%= (DraftCache.DraftExists()) ? "true": "false" %>; }
    function getBtnContinueID() { return '<%= btnContinue.ClientID %>'; }
    
    function displayReportsNotification(isError, text)
    {
        var notification = $("#reportsNotification");
        notification.removeClass(isError ? "sw-suggestion-pass" : "sw-suggestion-fail").addClass(isError ? "sw-suggestion-fail" : "sw-suggestion-pass");

        var textElement = notification.find(".sw-notification-text");
        textElement.text(text);

        notification.show();
    }

	var IsFederationEnabled = <%= SwisFederationInfo.IsFederationEnabled.ToString().ToLower() %>;

    var reportManager;
    var importReportController;

    $(function()
    {
        if (IsFederationEnabled)
        {
            importReportController = new SW.Core.Reports.ImportReportController(
            {
                ReportManagerConfiguration:
                {
                    Prefix:            'ReportManager_ImportReports_',
                    ContainerID:       'ImportReportSelection_ReportManagerGrid',
                    RemoteReportsMode: 'true',
                    PageSize:          $("#grid_page_size").val(),
                    SelectedColumns:   'Title,LastImported',
                    SortOrder:         $("#grid_sort_order").val(),
                    GroupingValue:     $("#grid_grouping_value").val(),
                    AllowReportMgmt:   'false',
                    FixedHeight:       310,
                    SwisBasedReportsOnly: "true"
                },

                RemoteReportsDialogAccordion: $('#importRemoteReportsDialogAccordion'),
                
                SelectedReportsContainerID: 'selectedReportsContainer'
            });
        }

        reportManager = new SW.Orion.ReportManager(
        {
            ContainerID:            'ReportManagerGrid',
            sessionInput:           '<%=Session["NewReportId"]%>',
            PageSize:               $("#grid_page_size").val(),
            SelectedColumns:        $("#grid_selected_columns").val(),
            SortOrder:              $("#grid_sort_order").val(),
            GroupingValue:          $("#grid_grouping_value").val(),
            AllowReportMgmt:        '<%=Profile.AllowReportManagement%>',
            ReturnToUrl:            '<%= ReturnUrl %>',
            IsManagerGrid:          'true',
            ImportReportController: importReportController,
            SwisBasedReportsOnly:   IsFederationEnabled
        });
        
        reportManager.setFileUploadControl('<%= importFileDialog.ClientID%>');
        reportManager.init();
    });
</script>
<div id="mainContent">
    <div style="display:none;">
        <img src="/Orion/images/Reports/Star-full.png" alt=""/>
        <img src="/Orion/images/Reports/Star-empty.png" alt=""/>
    </div>
    <h1 class="page-title"><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    <% if (DraftCache.DraftExists() && !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) { %>
            <span class="sw-suggestion"><span class="sw-suggestion-icon"></span><%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBDATA_TM0_289, DraftCache.GetCurrentUserDraft().LastChange.ToString("t"))) %> &nbsp;
                &raquo; <a class="sw-link" href="<%= DefaultSanitizer.SanitizeHtml(EditDraftURL) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_290) %></a>&nbsp;
                &raquo; <asp:LinkButton CssClass="sw-link" runat="server" ID="lnkRemoveDraft" OnClick="RemoveDraft" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_291 %>" />
            </span>
    <% } %>
    <h2 runat="server" id="subtitleText" style="font-size:small;" />
	<div id="trainingVideo">
	    <%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBCODE_AB0_5,
	            String.Format("<a class='sw-link' href='{0}embedded_in_products/productLink.aspx?id=sw_core_NewReportingVideo' target='_blank'>", Resources.CoreWebContent.SolarWindsComLocUrl), "</a>",
	            "<a class=\'sw-link\' href=\'" + KnowledgebaseHelper.GetMindTouchKBUrl(46407) + "' target=\'_blank\'>", "</a>")) %>
        <br />
        
        <div class="sw-suggestion sw-suggestion-warn" style="margin: 10px 0;" id="divReportWriterWarningMessage" runat="server" visible="false">
            <span class="sw-suggestion-icon"></span>
            <asp:Label ID="lblReportWriterWarningMessage" runat="server"></asp:Label>
            <a href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("todo")) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.ManageReports_ReportWriterWarning_LearnMore) %></a>
        </div>
	</div>
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
        <Services>
            <asp:ServiceReference Path="/Orion/Services/Information.asmx" />
        </Services>
    </asp:ScriptManagerProxy>
    <orion:Include runat="server" File="Reports/js/ImportReportController.js" />
    <orion:Include runat="server" File="Reports/js/ReportManagerGrid.js" />
    <orion:Include runat="server" File="Admin/js/SearchField.js" />
    <div id="reportsNotification" class="sw-suggestion" style="margin-bottom: 1em; display: none;">
        <span class="sw-suggestion-icon"></span> <span class="sw-notification-text"></span>
        <a href='#' class='sw-ffpcrumb-remove' onclick='$(this).parent().hide(); return false;'><img src='/orion/images/breadcrumb.12.x.png' style='padding-left: 10px;' class='sw-ffpcrumb-x'></a>
    </div>
    <orion:ReportTabBar  ID="reportTabBar" runat="server" Visible="true" />
    <table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <div id="ReportManagerGrid"></div>
            </td>
        </tr>
    </table>
  </div>
  <div id="draftDialog" style="display:none;" title="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_292) %>">
      <div><b><%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBDATA_TM0_293, DraftCache.DraftExists() ? DraftCache.GetCurrentUserDraft().LastChange.ToString("t") : String.Empty)) %></b></div>
      <div><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_301) %></b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_294) %></div>
      <div class="sw-btn-bar-wizard" style="padding-top: 30px;">
          <orion:LocalizableButtonLink runat="server" ID="btnEditDraft" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_295 %>" DisplayType="Primary" />
          <orion:LocalizableButtonLink runat="server" ID="btnContinue" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_296 %>" DisplayType="Secondary" onclick="return false;" />
      </div>
  </div>
    
<div id="remoteReportsSelectionDialog" style="display: none;">
    <orion:ImportReportSelection runat="server" />
</div>    

   <orion:SchedulePicker ID="SchedulePicker1" runat="server" 
        Title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_YP0_2 %>" 
        GetHeaderTitleFn="reportManager.getHeaderTitleFn" 
        OnSchedulesSelected="reportManager.scheduleSelected" BlockIfDemo="True" />
    <div id = "shceduleUnassignBox" class="unassignDialog">
        <div id="unassignTitle"  class="unassignDialogTitle"></div>
        <table id="assignedSchedules" class="unassignDialogTitle"></table>
    </div>
    <asp:FileUpload ID="importFileDialog" runat="server" onchange="reportManager.importFileSelected(this)" CssClass="import-file-dialog"/>
</asp:Content>

