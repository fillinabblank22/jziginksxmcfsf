﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Xml.Linq;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Reporting.DAL;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Reporting.Models;
using SolarWinds.Reporting.Models.Selection;

public partial class Orion_Reports : System.Web.UI.Page
{
    private static Log log = new Log();
    private static IEnumerable<Type> GetKnownTypes()
    {
        yield return typeof(DBNull);
        yield return typeof (SolarWinds.Reporting.Models.Tables.TableConfiguration);
        yield return typeof (SolarWinds.Reporting.Models.Charts.ChartConfiguration);
        yield return typeof(SolarWinds.Orion.Core.Reporting.Models.WebResource.WebResourceConfiguration);
    }
    protected static HashSet<String> _hiddenReportWriterHelpLink = new HashSet<string>(OrionModuleManager.GetCustomizations().Where(c => c.Action.Equals(Customization.ManageReportsHideSection)).Select(c => c.Key));

    private void ExportReport()
    {
        int reportId;
        if (int.TryParse(Request["ExportReportID"], out reportId))
        {
            const string selectQuery = @"SELECT TOP 1 r.ReportID, Name, Category, Title, Type, SubTitle, r.Description, LegacyPath, Definition, ModuleTitle, RecipientList, LimitationCategory, Owner, 
CASE WHEN f.AccountID=@accountID THEN 'true' 
                                                    ELSE 'false' 
                                                    END AS Favorite{0}
FROM Orion.Report r LEFT JOIN Orion.ReportFavorites f ON (f.ReportID=r.ReportID AND f.AccountID=@accountID)
Where r.ReportID = @reportId
";
            Report report = null;
            try
            {
                using (var service = InformationServiceProxy.CreateV3())
                {
                    var queryParams = new Dictionary<string, object>();
                    queryParams[@"reportId"] = Convert.ToInt32(reportId);
                    queryParams[@"accountID"] = Profile.UserName;

                    var cpColumns = new StringBuilder();
                    var properties = CustomPropertyMgr.GetCustomPropertiesForEntity("Orion.ReportsCustomProperties");
                    if (properties != null)
                    {
                        foreach (var customProperty in properties)
                        {
                            cpColumns.AppendFormat(", r.CustomProperties.[{0}]", customProperty.PropertyName);
                        }
                    }

                    var rDataTable = service.Query(string.Format(selectQuery, cpColumns), queryParams);
                    if (rDataTable == null || rDataTable.Rows == null || rDataTable.Rows.Count == 0)
                        throw new ArgumentOutOfRangeException("ReportID");

                    report = Loader.Deserialize(XElement.Parse(rDataTable.Rows[0]["Definition"].ToString()));
                    bool isFavorite;
                    report.IsFavorite = bool.TryParse(rDataTable.Rows[0]["Favorite"].ToString(), out isFavorite) && isFavorite;

                    report.CustomProperties = new Dictionary<string, object>();
                    if (properties != null)
                    {
                        foreach (var customProperty in properties)
                        {
                            report.CustomProperties.Add(customProperty.PropertyName, rDataTable.Rows[0][customProperty.PropertyName]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Couldn't export report by using {0} reportId. Error - {1}", reportId, ex);
            }

            if (report != null)
            {
                var dcs = new DataContractSerializer(typeof(Report), GetKnownTypes());
                byte[] data;
                using (var memStm = new System.IO.MemoryStream())
                {
                    dcs.WriteObject(memStm, report);
                    data = memStm.ToArray();
                }

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ContentType = "text/xml";
                HttpContext.Current.Response.AppendHeader("Content-Disposition", string.Format("attachment; filename= {0}.xml", HttpUtility.UrlEncode(report.Name, Encoding.UTF8)));
                HttpContext.Current.Response.AddHeader("Content-Length", data.Length.ToString());
                HttpContext.Current.Response.ContentType = "application/octet-stream";
                HttpContext.Current.Response.BinaryWrite(data);
                HttpContext.Current.Response.End();
            }
        }
    }

    private void RegisterScript(string errorsMessage)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "myalert", string.Format("Ext.Msg.show({0});", errorsMessage), true);
    }

    private bool ValidateDataSources(Report report)
    {
        const string selectQuery = @"SELECT FullName FROM Metadata.Entity where FullName = @fullName";
        var queryParams = new Dictionary<string, object>();

        if (report.DataSources != null)
        {
            foreach (var dataSource in report.DataSources)
            {
                //skip other types
                if (dataSource.Type == DataSourceType.Entities || dataSource.Type == DataSourceType.Dynamic)
                {
                    //check master entity
                    queryParams[@"fullName"] = dataSource.MasterEntity;
                    using (var service = InformationServiceProxy.CreateV3())
                    {
                        var rDataTable = service.Query(selectQuery, queryParams);
                        if (rDataTable == null || rDataTable.Rows == null || rDataTable.Rows.Count == 0)
                            return false;
                    }
                }
            }
        }
        return true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        btnEditDraft.NavigateUrl = EditDraftURL;
        if (IsPostBack && importFileDialog.HasFile && Page.Request.Params.Get("__EVENTTARGET").EndsWith("importFileDialog"))
        {
            var data = XElement.Load(importFileDialog.FileContent);
            Report report = null;
            var dcs = new DataContractSerializer(typeof(Report), GetKnownTypes());
            using (var reader = data.CreateReader())
            {
                try
                {
                    report = (Report)dcs.ReadObject(reader);
                }
                catch (SerializationException ex)
                {
                    log.ErrorFormat("Failed to import report from {0}! Error: {1}", importFileDialog.FileName, ex);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "importreport", String.Format("<script type='text/javascript'>alert('{0}');</script>",
                        string.Format("{0}\\n {1}", string.Format(Resources.CoreWebContent.WEBDATA_YK0_81, importFileDialog.FileName), ex.Message).Replace("'", "\\'")));
                    return;
                }
            }

            if (report != null)
            {
                if (!ValidateDataSources(report))
                {
                    RegisterScript(string.Format("{{title: '{0}', msg: '{1}', minWidth: 300, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR}}", Resources.CoreWebContent.WEBDATA_YK0_83,
                                                string.Format(Resources.CoreWebContent.WEBDATA_YK0_82, importFileDialog.FileName).Replace("'", "\\'")));
                    return;
                }

                string folderToCompare = String.IsNullOrEmpty(report.LimitationCategory) ? this.Profile.ReportFolder : report.LimitationCategory;
                if (report.Header != null)
                {
                    ValidateReportName(report, folderToCompare);
                }
                var definition = Loader.Serialize(report);
                var argList = new List<object> { report.Name, report.Description, report.LimitationCategory, report.Category, report.Header.Title, report.Header.SubTitle, definition, report.IsFavorite.ToString(),
                                      HttpContext.Current.Profile.UserName, HttpContext.Current.Profile.UserName };
                using (var service = InformationServiceProxy.CreateV3())
                {
                    var resultXml = service.Invoke("Orion.Report", "CreateReport", CustomPropertyHelper.GetArgElements(argList));
                    Session["NewReportId"] = resultXml.InnerText;
                }
            }
            else
            {
                log.ErrorFormat("Failed to import report from {0}!", importFileDialog.FileName);
            }
        }
        else
        {
            ExportReport();
        }

        if (!IsPostBack)
        {
        	// OADP-443 following message has been temporarily disabled until we provide report migration tool
            string message = null; // GetReportWriterWarningMessage();
            divReportWriterWarningMessage.Visible = !string.IsNullOrWhiteSpace(message);
            lblReportWriterWarningMessage.Text = message;
        }
    }

    private bool ValidateReportName(Report report, string folderToCompare)
    {
        if (!ReportHelper.IsReportNameUnique(report.Header.Title, folderToCompare))
        {
            report.Header.Title = string.Format(Resources.CoreWebContent.WEBDATA_TM0_299, report.Header.Title);
            report.Name = string.Format(Resources.CoreWebContent.WEBDATA_TM0_299, report.Name);
            if (report.Header.Title.Length > 255 || report.Name.Length > 255)
            {
                report.Header.Title = report.Header.Title.Substring(0, 255);
                report.Name = report.Name.Substring(0, 255);
            }
            return ValidateReportName(report, folderToCompare);
        }
        return true;
    }

    [WebMethod(EnableSession = true)]
	public static void ClearSessionValue(string sessionVarName)
	{
		HttpContext.Current.Session[sessionVarName] = null;
	}

	protected string ReturnUrl
	{
		get { return ReferrerRedirectorBase.GetReturnUrl(); }
	}

    protected void RemoveDraft(object sender, EventArgs e)
    {
        if (DraftCache.GetCurrentUserDraft() != null)
            ReportStorage.Instance.Discard(DraftCache.GetCurrentUserDraft().ReportId);
        DraftCache.CleanDraftForCurrentUser();
    }

    public string EditDraftURL
    {
        get
        {
            return DraftCache.DraftExists()
                       ? String.Format("/Orion/Reports/Add/Default.aspx?ReportWizardGuid={0}", DraftCache.GetCurrentUserDraft().ReportId)
                       : String.Empty;
        }
    }

	public int IsFederationEnabled
	{
		get
		{
			return SwisFederationInfo.IsFederationEnabled ? 1 : 0;
		}
	}

    private string GetReportWriterWarningMessage()
    {
        using (var proxy = InformationServiceProxy.CreateV3())
        using (var data = proxy.Query(@"SELECT COUNT(ReportID) AS cnt FROM Orion.Report WHERE LegacyPath IS NOT NULL"))
        {
            int legacyReportsCount;
            if (int.TryParse(data.Rows[0]["cnt"]?.ToString(), out legacyReportsCount) && legacyReportsCount > 0)
            {
                return Resources.CoreWebContent.ManageReports_ReportWriterWarning.Replace("{legacyreports.count}", legacyReportsCount.ToString());
            }
        }
        return null;
    }
}
