﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using Newtonsoft.Json;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_Reports_Scheduler : System.Web.UI.Page
{
    private static readonly Log log = new Log();

    protected void Page_Load(object sender, EventArgs e)
    {
        var name = Session["CreatedScheduledJobName"];
		if (name != null)
        {
			ValidationOk.Visible = true;
			ValidationOk.Controls.Add(new LiteralControl(string.Format("<span id='scheduleName'>{0}</span><span style='font-weight: normal;'> {1}</span>", HttpUtility.HtmlEncode(name.ToString().Replace(":hightlight=true", "")), Resources.CoreWebContent.WEBDATA_AB0_16)));

			if (name.ToString().IndexOf(":hightlight=true", StringComparison.OrdinalIgnoreCase) > 0)
				ValidationOk.Style["display"] = "none";
			
            Session["CreatedScheduledJobName"] = null;
        }
    }

    [WebMethod]
    public static string RunNow(List<string> jobIds)
    {
        var primaryEngineId = EnginesDAL.GetPrimaryEngineId();

        ICoreBusinessLayerProxyCreator blProxyCreator;
        int proxySendTimeoutSeconds;
        if(Int32.TryParse(ConfigurationSettings.AppSettings["ReportsSchedulerProxySendTimeoutSeconds"], out proxySendTimeoutSeconds))
        {
            blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator(TimeSpan.FromSeconds(proxySendTimeoutSeconds));
        }
        else
        {
            blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
        }
        
        Dictionary<int, bool> executionResults;
        using (var businessProxy = blProxyCreator.Create(BusinessLayerExceptionHandler, primaryEngineId))
        {
            executionResults = businessProxy.RunNow(jobIds.Select(int.Parse).ToList());
        }
        return JsonConvert.SerializeObject(executionResults);
    }

    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }
}