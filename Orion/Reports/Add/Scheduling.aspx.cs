﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using SolarWinds.Orion.Core.Actions.Impl.Email;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Interfaces;
using SolarWinds.Orion.Core.Models.Actions.Contexts;
using SolarWinds.Orion.Core.Models.MacroParsing;
using SolarWinds.Orion.Core.Reporting.DAL;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.Reporting;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Models.Interfaces;

public partial class Orion_Reports_Add_Scheduling : AddReportWizardBase
{
    private List<ISchedule> SchedulesList { get; set; }
    public string SchedulesListJson
    {
        get
        {
            var settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Objects };
            if (IsPostBack)
            {
                SchedulesList = frequency.Schedules.ToList();
            }
            return JsonConvert.SerializeObject(SchedulesList, settings);
        }
    }
    protected string GetHeaderTitleFn
    {
        get
        {
            var report = ReportStorage.Instance.GetReport(Workflow.WizardGuid);
            var reportName = (report != null) ? HttpUtility.JavaScriptStringEncode(HttpUtility.HtmlEncode(report.Name)) : string.Empty;
            return "function(){ return '" + string.Format(Resources.CoreWebContent.WEBDATA_IB0_176, reportName) + "';}";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        frequency.WizardGuid = Workflow.WizardGuid;
        var macroContext = new MacroContext();
        macroContext.Add(new GenericContext());
        macroContext.Add(new ReportingContext());
        actionsEditorView.ViewContext = new ReportingActionContext{MacroContext = macroContext};
       
        actionsEditorView.PreSelectedActionTypeID = EmailConstants.ActionTypeID;
		if (string.IsNullOrEmpty(hfScheduleMode.Value) && Session["HfScheduleMode"] != null)
			hfScheduleMode.Value = Session["HfScheduleMode"].ToString();
		if (string.IsNullOrEmpty(hfSelectedSchedules.Value) && Session["HfSelectedSchedules"] != null)
			hfSelectedSchedules.Value = Session["HfSelectedSchedules"].ToString();

		if (!IsPostBack)
		{
			InitControls();
		}

		var createdJob = ReportSchedulesStorage.Instance.GetJob(Workflow.WizardGuid);
		if (createdJob != null)
		{
		    SchedulesList = new List<ISchedule>(createdJob.Schedules);
			actionsEditorView.Actions = createdJob.Actions;
			Name.Text = createdJob.Name;
			Description.Value = createdJob.Description;
			if (!IsPostBack)
                RSAdvancedSettingsControl.setAccount(createdJob.AccountID);
                RSAdvancedSettingsControl.setWebsite(createdJob.WebsiteID);
			if (Session["HfScheduleMode"].ToString().Equals("new", StringComparison.OrdinalIgnoreCase))
				selectSchedule.SelectedIndex = 1;
			if (Session["HfScheduleMode"].ToString().Equals("existing", StringComparison.OrdinalIgnoreCase))
				selectSchedule.SelectedIndex = 0;
		}
		else
		{
			if (!IsPostBack)
                RSAdvancedSettingsControl.setAccount(this.Profile.UserName);
            SchedulesList = new List<ISchedule>();
		}

		if (Workflow.ReportId != 0)
		{
			Title = Resources.CoreWebContent.WEBDATA_SO0_66;
		}

		Workflow.SetStepFromUrl(this.Page.Request.Url.GetLeftPart(UriPartial.Path));
		this.UpdateProgressIndicator();
	}

	private bool ValidateJobConfiguration(ReportJobConfiguration jobConfig)
	{
	    if (string.IsNullOrEmpty(jobConfig.AccountID))
	    {
            return false;
	    }
	    else if (!ReportHelper.IsReportJobNameUnique(Name.Text, jobConfig.ReportJobID))
		{
			this.registerScript(
				string.Format(
					"{{title: '{0}', msg: '{1}', minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR}}",
					Resources.CoreWebContent.WEBDATA_AB0_15, Resources.CoreWebContent.WEBDATA_AB0_14));
			return false;
		}
		else if (jobConfig.Schedules.Count == 0)
		{
			this.registerScript(
				string.Format(
					"{{title: '{0}', msg: '{1}', minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR}}",
                    Resources.CoreWebContent.WEBDATA_SO0_141, Resources.CoreWebContent.WEBDATA_AB0_45));
			return false;
		}
		else if (!actionsEditorView.Actions.Any())
		{
			this.registerScript(
				string.Format(
					"{{title: '{0}', msg: '{1}', minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR}}",
					Resources.CoreWebContent.WEBDATA_VM0_1, Resources.CoreWebContent.WEBDATA_AB0_46));
			return false;
		}
		return true;
	}

	private bool ValidateExistingJobs(string jobsJson)
	{
        var dict = (Dictionary<int, ScheduleInfo>)OrionSerializationHelper.FromJSON(jobsJson,typeof(Dictionary<int, ScheduleInfo>));
		if (dict != null && dict.Count > 0)
		{
			return true;
		}
		this.registerScript(
					string.Format(
						"{{title: '{0}', msg: '{1}', minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR}}",
						Resources.CoreWebContent.WEBDATA_SO0_139, Resources.CoreWebContent.WEBDATA_AB0_66));
		return false;
	}
    private List<ReportSchedule> CastListToReportSchedule(IEnumerable<ISchedule> listOfSchedules)
    {
        var reportSchedulesList = new List<ReportSchedule>();
        foreach (var schedule in listOfSchedules)
        {
            reportSchedulesList.Add(schedule as ReportSchedule);
        }
        return reportSchedulesList;
    }
    protected override bool SaveCurrentState()
    {
        Page.Validate();
        if (!Page.IsValid) return false;

        if (!string.IsNullOrEmpty(hfScheduleMode.Value) && rbWithSchedule.Checked)
        {
            if (hfScheduleMode.Value.Equals("new", StringComparison.OrdinalIgnoreCase))
            {
                var jobConfiguration = new ReportJobConfiguration
                {
                    AccountID = RSAdvancedSettingsControl.getAccount(),
                    WebsiteID = RSAdvancedSettingsControl.getWebsite(),
                    Enabled = true,
                    Name = Name.Text,
                    Description = Description.Value
                };
                jobConfiguration.Schedules = CastListToReportSchedule(frequency.Schedules);
                jobConfiguration.Actions = actionsEditorView.Actions.ToList();
                if (!ValidateJobConfiguration(jobConfiguration)) return false;

                ReportSchedulesStorage.Instance.SetJob(jobConfiguration, Workflow.WizardGuid);
                Session["HfSelectedSchedules"] = hfSelectedSchedules.Value;
            }
            else if (hfScheduleMode.Value.Equals("existing", StringComparison.OrdinalIgnoreCase))
            {
                if (!ValidateExistingJobs(hfSelectedSchedules.Value)) return false;
                Session["HfSelectedSchedules"] = hfSelectedSchedules.Value;
            }

            Session["HfScheduleMode"] = hfScheduleMode.Value;
        }
        else if (rbNoSchedule.Checked)
        {
            Session["HfScheduleMode"] = string.Empty;
            hfScheduleMode.Value = string.Empty;
            Session["HfSelectedSchedules"] = "[]";
        }
        return true;
    }

    protected void ScheduleNameValidator(object source, ServerValidateEventArgs args)
    {
        args.IsValid = !string.IsNullOrEmpty(args.Value) || !hfScheduleMode.Value.Equals("new", StringComparison.OrdinalIgnoreCase);
    }
   
    private void registerScript(string errorsMessage)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "myalert", string.Format("Ext.Msg.show({0});", errorsMessage), true);
    }

    private void InitControls()
    {
        if (Workflow.ReportId != 0)
        {
            var schedules = ReportDAL.GetReportSchedulesInfo(Workflow.ReportId);
            if (schedules.Count > 0 && Session["HfSelectedSchedules"] == null)
            {
               hfSelectedSchedules.Value = OrionSerializationHelper.ToJSON(schedules, typeof(Dictionary<int, ScheduleInfo>));
            }
        }
    }
}
