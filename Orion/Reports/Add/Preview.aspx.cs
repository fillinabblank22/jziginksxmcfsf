﻿using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Reporting;

public partial class Orion_Reports_Add_Preview : AddReportWizardBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AngularJsHelper.EnableAngularJsForPageContent();

        Workflow.SetStepFromUrl(this.Page.Request.Url.GetLeftPart(UriPartial.Path));
        this.UpdateProgressIndicator();

        if (Workflow.ReportId != 0)
            Title = Resources.CoreWebContent.WEBDATA_SO0_66;

        if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            Preview.CacheId = Workflow.WizardGuid;
        }
        else
        {
            Preview.CacheId = Guid.Empty;
            Preview.ReportId = Workflow.ReportId;
        }
        Preview.FastPreviewCount = WebsiteSettings.ReportingTablePagePreviewMaxRowCount;
    }
}