﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;
using System.Linq;

using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Reporting.DAL;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.Reporting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Reporting.Models.WebResource;
using SolarWinds.Reporting.Models;
using SolarWinds.Reporting.Models.Charts;
using SolarWinds.Reporting.Models.Tables;

public partial class Orion_Reports_Add_Default : AddReportWizardBase
{
	string selectQuery =
@"SELECT TOP 1 r.ReportID, Name, Category, Title, Type, SubTitle, r.Description, LegacyPath, Definition, ModuleTitle, RecipientList, LimitationCategory, Owner, 
CASE WHEN f.AccountID=@accountID THEN 'true' 
                                                    ELSE 'false' 
                                                    END AS Favorite{0}
FROM Orion.Report r LEFT JOIN Orion.ReportFavorites f ON (f.ReportID=r.ReportID AND f.AccountID=@accountID)
Where r.ReportID = @reportId
";

    private const int maxColsize = 255;

	protected override void OnInit(EventArgs e)
	{
		if (!IsPostBack)
		{
			string reportWizardGuid = Request["ReportWizardGuid"];

            // no point processing further if the guid is empty
            // this can happen if js method fails to get a guid due to session timeout
            if (string.IsNullOrEmpty(reportWizardGuid))
                Response.Redirect(DefaultReturnUrl);

			layoutBuilder.WorkflowGuid = reportWizardGuid;
			HiddenReportWizardGuid.Value = reportWizardGuid;
			Workflow.WizardGuid = Guid.Parse(reportWizardGuid);

			string reportId = Request["ReportID"];
			string reportUri = Request["ReportUri"];

			Report report;
			if (!string.IsNullOrEmpty(reportId))
			{
				this.Title = Resources.CoreWebContent.WEBDATA_SO0_66;
				using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
				{
					var queryParams = new Dictionary<string, object>();
					queryParams[@"reportId"] = Convert.ToInt32(reportId);
					queryParams[@"accountID"] = Profile.UserName;

                    var cpColumns = new StringBuilder();
                    var properties = CustomPropertyMgr.GetCustomPropertiesForEntity("Orion.ReportsCustomProperties");
                    if (properties != null)
                    {
                        foreach (var customProperty in properties)
                        {
                            cpColumns.AppendFormat(", r.CustomProperties.[{0}]", customProperty.PropertyName);
                        }
                    }

					var rDataTable = service.Query(string.Format(selectQuery, cpColumns), queryParams);
					if (rDataTable == null || rDataTable.Rows == null || rDataTable.Rows.Count == 0)
						throw new ArgumentOutOfRangeException("ReportID");

					report = Loader.Deserialize(XElement.Parse(rDataTable.Rows[0]["Definition"].ToString()));
					bool isFavorite;
					report.IsFavorite = bool.TryParse(rDataTable.Rows[0]["Favorite"].ToString(), out isFavorite) &&
										isFavorite;

					Workflow.ReportId = Convert.ToInt32(reportId);
					if (!string.IsNullOrEmpty(reportUri))
						Workflow.ReportUri = reportUri;

                    report.CustomProperties = new Dictionary<string, object>();
                    if (properties != null)
                    {
                        foreach (var customProperty in properties)
                        {
                            report.CustomProperties.Add(customProperty.PropertyName, rDataTable.Rows[0][customProperty.PropertyName]);
                        }
                    }
				    ReportStorage.Instance.Update(Workflow.WizardGuid, report);

					layoutBuilder.Sections = report.Sections;
					layoutBuilder.Configs = report.Configs;
					layoutBuilder.TimeFrames = report.TimeFrames;
					layoutBuilder.DataSources = report.DataSources;
				}
				Session["HfSelectedSchedules"] = null;
				Session["HfScheduleMode"] = null;
			}
			else
			{
				report = ReportStorage.Instance.GetReport(Workflow.WizardGuid);
				if (report == null)
				{
					report = new Report();
					ReportStorage.Instance.Update(Workflow.WizardGuid, report);
					Session["HfSelectedSchedules"] = null;
					Session["HfScheduleMode"] = null;
				}
				else if (Workflow.ReportId > 0)
				{
					this.Title = Resources.CoreWebContent.WEBDATA_SO0_66;
				}
			}

			reportFooter.Footer = report.Footer;
			reportHeader.Header = report.Header;
			reportPageConfig.PageLayout = report.PageLayout;
		}

		Workflow.SetInitialRedirect(Workflow.ReportId > 0 ? Workflow.Steps.Count-1 : 0);
		Workflow.FirstStep();
		base.OnInit(e);
	}

	protected override void OnLoad(EventArgs e)
	{
		base.OnLoad(e);

		// using postback store report to storage and then to redirect to edit page of the resource
		if (IsPostBack && !string.IsNullOrEmpty(RedirectToUrlHiddenField.Value))
		{
			Report report = UpdateReportFromLayout();
			// if not redirecting to resource edit page then validate report
			if (!RedirectToUrlHiddenField.Value.StartsWith("/Orion/Reports/EditReportResource.aspx", StringComparison.OrdinalIgnoreCase) && !ValidateInputs(report))
				return;
			Response.Redirect(RedirectToUrlHiddenField.Value);
		}
	}

	protected override bool SaveCurrentState()
	{
		Report report = UpdateReportFromLayout();
		if (!ValidateInputs(report))
		{
			if (string.IsNullOrEmpty(report.Header.Logo))
			{
				report.Header.Logo = WebConstants.StandardReportLogoIdentifier;
			}
			return false;
		}
	    return base.SaveCurrentState();
	}

    private Report UpdateReportFromLayout()
    {
        Report report = ReportStorage.Instance.GetReport(Workflow.WizardGuid);
        report.Header = reportHeader.Header;
        report.Sections = layoutBuilder.Sections;
        report.TimeFrames = layoutBuilder.TimeFrames;
        report.DataSources = layoutBuilder.DataSources;
        report.Footer = reportFooter.Footer;
        report.PageLayout = reportPageConfig.PageLayout;
        report.Configs = layoutBuilder.Configs;

        return report;
    }

    private void registerScript(string errorsMessage)
	{
		ClientScript.RegisterStartupScript(this.GetType(), "myalert", string.Format("Ext.Msg.show({0});", errorsMessage), true);
	}

	// todo: reconsider the meaning of this method, ValidateReport() method should be enough or should be extended
	private bool ValidateInputs(Report report)
	{
		var configs = layoutBuilder.Configs;
		var sections = layoutBuilder.Sections;

		var resources = (from section in sections from column in section.Columns from cell in column.Cells select cell).ToList();
		bool result = true;
		StringBuilder errorStringBuilder = new StringBuilder();
		string errorText = Resources.CoreWebContent.WEBDATA_SO0_91;

		foreach (var resource in resources)
		{
			if (resource.DataSelectionRefId == Guid.Empty)
			{
				foreach (var config in configs)
				{
					if (resource.ConfigId == config.RefId)
					{
						//check if resource requares datasource to be selected
						if (config is WebResourceConfiguration)
						{
							var template = ResourceTemplate.CreateASCX(((WebResourceConfiguration)config).ResourceFile);
							if (template.SupportedInterfaces.Any(
								i => typeof(SolarWinds.Orion.NPM.Web.IMultiObjectProvider).IsAssignableFrom(i))
								 || template.RequiredInterfaces.Count() > 0 || template.SupportedInterfaces.Count() > 0)
							{
								errorStringBuilder.AppendFormat(errorText, template.Title);
								result = false;
							}
						}
						else
						{
							// check if resource is a custom chart or custom table then it requares datasorce 
							if ((config is ChartConfiguration) || (config is TableConfiguration))
							{
								errorStringBuilder.AppendFormat(errorText, config.DisplayTitle);
								result = false;
							}
						}
					}
				}
			}
		}

		if (!result)
			this.registerScript(string.Format("{{title: '{0}', msg: '{1}', minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR}}", Resources.CoreWebContent.WEBDATA_SO0_90, errorStringBuilder.ToString()));

		result = result && layoutBuilder.Validate();

		if (report.Header != null && string.IsNullOrEmpty(report.Header.Title))
		{
			this.registerScript(string.Format("{{title: '{0}', msg: '{1}', minWidth: 300, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR}}", Resources.CoreWebContent.WEBDATA_AK0_230, Resources.CoreWebContent.WEBDATA_YK0_62));
			result = false;
		}

        if (report.Header != null && report.Header.Title.Length > maxColsize)
        {
            this.registerScript(string.Format("{{title: '{0}', msg: '{1}', minWidth: 300, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR}}", Resources.CoreWebContent.WEBDATA_AK0_230, Resources.CoreWebContent.WEBDATA_AY0_67));
            result = false;
        }

        if (report.Header != null && report.Header.SubTitle.Length > maxColsize)
        {
            this.registerScript(string.Format("{{title: '{0}', msg: '{1}', minWidth: 300, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR}}", Resources.CoreWebContent.WEBDATA_AY0_66, Resources.CoreWebContent.WEBDATA_AY0_68));
            result = false;
        }

		string folderToCompare = String.IsNullOrEmpty(report.LimitationCategory) ? this.Profile.ReportFolder : report.LimitationCategory;

		if (report.Header != null && ((report.Name == null || (!report.Header.Title.Equals(report.Name)))
			|| (report.LimitationCategory == null || (!report.LimitationCategory.Equals(folderToCompare))))
            && !ReportHelper.IsReportNameUnique(report.Header.Title, folderToCompare))
		{
			this.registerScript(string.Format("{{title: '{0}', msg: '{1}', minWidth: 300, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR}}", Resources.CoreWebContent.WEBDATA_AK0_230, String.Format(Resources.CoreWebContent.WEBDATA_TM0_297, TranslateFolder(folderToCompare))));
			result = false;
		}
		return result;
	}

	private string TranslateFolder(string value)
	{
		if (value.Equals(CoreConstants.ReportDefaultFolder, StringComparison.OrdinalIgnoreCase))
		{
			return Resources.CoreWebContent.WEBCODE_TM0_116;
		}
		return value;
	}
}
