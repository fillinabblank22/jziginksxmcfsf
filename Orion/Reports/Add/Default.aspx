<%@ Page Language="C#" MasterPageFile="~/Orion/Reports/Add/AddReportWizard.master"
    AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_Reports_Add_Default"
    Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_161 %>" %>
<%@Register tagName="layoutBuilder" tagPrefix="orion" src="~/Orion/Controls/LayoutBuilderControl.ascx" %>
<%@ Register Src="~/Orion/Reports/Controls/ReportLayoutFooterControl.ascx" TagPrefix="orion" TagName="ReportLayoutFooterControl" %>
<%@ Register Src="~/Orion/Reports/Controls/ReportLayoutHeaderControl.ascx" TagPrefix="orion" TagName="ReportLayoutHeaderControl" %>
<%@ Register Src="~/Orion/Reports/Controls/ReportPageConfigControl.ascx" TagPrefix="orion" TagName="ReportPageConfigControl" %>

<asp:Content ContentPlaceHolderID="headPlaceholder" runat="server">
<style type="text/css" >
    .layoutBuilderBtn{display: none;}
</style>
</asp:Content>
<asp:Content ContentPlaceHolderID="wizardContent" runat="Server">
    <asp:HiddenField ID="HiddenReportWizardGuid" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="RedirectToUrlHiddenField" ClientIDMode="Static" runat="server" />
    <asp:Panel runat="server" DefaultButton="imgbNext">
        <div class="GroupBox">
        <table><tr><td>
            <orion:ReportPageConfigControl runat="server" id="reportPageConfig" />
            <orion:ReportLayoutHeaderControl runat="server" id="reportHeader" />
            <asp:Panel runat="server" DefaultButton="layoutBuilderBtn">
                <orion:layoutBuilder runat="server" ID="layoutBuilder" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_69 %>" EmptyMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_68 %>"></orion:layoutBuilder>
                <asp:Button runat="server" CssClass="layoutBuilderBtn" ID="layoutBuilderBtn" OnClientClick="return false;"/>
            </asp:Panel>
            <orion:ReportLayoutFooterControl runat="server" id="reportFooter" />
        </td></tr></table>
        </div>
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButton runat="server" ID="imgbNext" OnClick="Next_Click" LocalizedText="Next"
                DisplayType="Primary" />
            <orion:LocalizableButton runat="server" ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel"
                DisplayType="Secondary" CausesValidation="false" />
        </div>
    </asp:Panel>
</asp:Content>

