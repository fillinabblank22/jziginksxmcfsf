<%@ Page Language="C#" MasterPageFile="~/Orion/Reports/Add/AddReportWizard.master"
    AutoEventWireup="true" CodeFile="Properties.aspx.cs" Inherits="Orion_Reports_Add_Properties"
    Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_161 %>" %>

<%@ Reference Control="~/Orion/Controls/HelpButton.ascx" %>
<%@ Register Src="~/Orion/Nodes/Controls/CustomProperties.ascx" TagPrefix="orion"
    TagName="CustomProperties" %>
<asp:Content ContentPlaceHolderID="headPlaceholder" runat="server">
    <style type="text/css">
        .edit-cp-value-div, .blueBox
        {
            width: 100%;
        }
        .datePicker
        {
            width: 136px !important;
        }
        .helpfulText
        {
            color: #5F5F5F;
            font-size: 10px;
            vertical-align: middle;
            text-align: left;
            display: block;
        }
        .contentBlockHeader
        {
            font-weight: bold;
        }
        .linktoCPE
        {
            text-align: left !important;
        }
    </style>
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <script type="text/javascript">
    // <![CDATA[
        function MakeFavorite(thisEl) {
            if (thisEl.checked) {
                $('#favoriteIcon').attr('src', "../../images/Reports/Star-full.png");
            }
            else {
                $('#favoriteIcon').attr('src', "../../images/Reports/Star-empty.png");
            }
        }

        function ToggleAdvanced() {
            var div = $("#reportAdvancedDiv");
            div.toggleClass("hidden");
            var button = $("#<%= colapseButton.ClientID %>");
            if (div.hasClass("hidden")) {
                button.attr({ src: "/Orion/images/Button.Expand.gif" });
            } else {
                button.attr({ src: "/Orion/images/Button.Collapse.gif" });
            }
        }

        function CategoryClick(thisEl) {
            if (thisEl.value == "<%= AddNewSelValue %>") {
                $("#categoryPanel").show();
            }
            else {
                $("#categoryPanel").hide();
            }
        }

        function LimCategoryClick(thisEl) {
            if (thisEl.value == "<%= AddNewSelValue %>") {
                $("#limitCatPanel").show();
            }
            else {
                $("#limitCatPanel").hide();
            }
        }

        function ClientCategoryValidate(sender, args) {
            var val = $("#<%= reportCategory.ClientID %> option:selected").val();
            if (val != "<%= AddNewSelValue %>")
                args.IsValid = true;
            else {
                var newCat = $("#<%= additionalCategoryValue.ClientID %>").val();
                args.IsValid = newCat && $.trim(newCat).length != 0;
            }
        }

        $(document).ready(function () {
            var uniqueReportNameUsed = <%= uniqueReportNameValidator.IsValid ? "true" : "false" %>;
            var validCategory = <%= newLimCategoryValidator.IsValid ? "true" : "false" %>;
            if (!uniqueReportNameUsed && validCategory) {
                ToggleAdvanced();
            }
            var val = $("#<%= limitationCategory.ClientID %> option:selected").val();
            if (val != "<%= AddNewSelValue %>")
                return;
            if (!validCategory) {
                ToggleAdvanced();
                $("#limitCatPanel").show();
            }
        });
    // ]]>
    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="wizardContent" runat="Server">
    <asp:Panel runat="server" DefaultButton="imgbNext">
        <div class="GroupBox">
            <div id="cachedImages" style="display: none;">
                <img src="../../images/Reports/Star-full.png" alt="" />
                <img src="../../images/Reports/Star-empty.png" alt="" />
            </div>
            <table style="padding-bottom:35px;">
                <tr>
                    <td style="vertical-align: middle; padding: 5px;">
                        <asp:CheckBox runat="server" ID="favoriteReport" onclick="MakeFavorite(this)" />
                        <img id="favoriteIcon" src="<%= DefaultSanitizer.SanitizeHtml(FavoriteUrl) %>">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_22) %>&nbsp;<span class="helpfulText" style="padding: 0px;">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_93) %></span><br />                        
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: middle; padding: 5px;">
                        <b>
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_23) %></b><br />
                        <asp:TextBox runat="server" ID="reportDescr" TextMode="MultiLine" Columns="1" Rows="3"
                            Width="400px" /><br />
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: middle; padding: 5px;">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <b>
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_25) %></b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="helpfulText" style="padding: 0px; vertical-align: top;">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_94) %></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:DropDownList runat="server" ID="reportCategory" onclick="CategoryClick(this); return false;"
                                        Width="400px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="categoryPanel" class="<%= reportCategory.SelectedValue.Equals(AddNewSelValue)?"":"hidden" %>">
                                        <asp:Label ID="Label1" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_YK0_30 %>">
                                        </asp:Label><br />
                                        <asp:TextBox runat="server" ID="additionalCategoryValue" Width="252px">
                                        </asp:TextBox>
                                        <asp:CustomValidator ID="CategoryValidator" runat="server" ControlToValidate="additionalCategoryValue" ValidateEmptyText = "true" ClientValidationFunction="ClientCategoryValidate" Display="Dynamic" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_YK0_63 %>" ForeColor="Red"></asp:CustomValidator>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: middle; padding: 5px;">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <orion:customproperties runat="server" id="reportCps" netobjecttype="ReportDefinitions"
                                    helpfullhinttext="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_95 %>" custompropertiestable="ReportDefinitions" />
                                <br />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: middle; padding: 5px;">
                        <asp:ImageButton ID="colapseButton" runat="server" ImageUrl="~/Orion/images/Button.Expand.gif"
                            OnClientClick="ToggleAdvanced(); return false;" CausesValidation="false" />
                        <b>
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_26) %></b><br/>
                            <span class="helpfulText" style="padding: 0px; vertical-align: top;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_64) %></span>
                        <div id="reportAdvancedDiv" class="hidden" style="background-color: #EFEFEF; width: 400px;">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_27) %>
                                    </td>
                                    <td style="text-align: right;">
                                        <span class="LinkArrow">&raquo;</span> <a href="<%= DefaultSanitizer.SanitizeHtml(this.LimitationUsageHelpURL) %>" id="reportHelpLink"
                                            target="_blank" rel="noopener noreferrer" style="text-decoration: underline; color: #336699;"><u>
                                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_28) %></u></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="limitationCategory" onchange="LimCategoryClick(this); return false;"
                                                        Width="100%" />
                                                    <asp:CustomValidator ID="uniqueReportNameValidator" OnServerValidate="ReportNameValidate"
                                                                         runat="server" ForeColor="Red" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div id="limitCatPanel" class="<%= limitationCategory.SelectedValue.Equals(AddNewSelValue)?"": "hidden" %>" style="padding-right: 5px;">
                                                        <asp:Label ID="Label2" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_YK0_31 %>" />
                                                        <br />
                                                        <asp:TextBox runat="server" ID="additionalLimitationCategoryValue" Width="100%" />
                                                        <br />
                                                        <asp:CustomValidator ID="newLimCategoryValidator" OnServerValidate="LimitationValidate"
                                                            runat="server" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_SO0_92 %>"
                                                            ForeColor="Red">
                                                        </asp:CustomValidator>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="sw-btn-bar-wizard">
            <orion:localizablebutton runat="server" id="LocalizableButton1" onclick="Back_Click"
                localizedtext="Back" displaytype="Secondary" causesvalidation="false" />
            <orion:localizablebutton runat="server" id="imgbNext" onclick="Next_Click" localizedtext="Next"
                displaytype="Primary" />
            <orion:localizablebutton runat="server" id="imgbCancel" onclick="Cancel_Click" localizedtext="Cancel"
                displaytype="Secondary" causesvalidation="false" />
        </div>
    </asp:Panel>
</asp:Content>
