<%@ Page Language="C#" MasterPageFile="~/Orion/Reports/Add/AddReportWizard.master"
    AutoEventWireup="true" CodeFile="Summary.aspx.cs" Inherits="Orion_Reports_Add_Summary"
    Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_161 %>" %>

<asp:Content ContentPlaceHolderID="headPlaceholder" runat="server">
</asp:Content>

<asp:Content ContentPlaceHolderID="wizardContent" runat="Server">
 <orion:Include File="ReportingDisplay.css" runat="server" />
    <asp:Panel runat="server" DefaultButton="imgbNext">
       <div class="GroupBox">
       <div style="width: 800px; padding: 20px">

       <div class="summaryHeader">
           <p style="padding-left: 15px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_47) %></p>
       </div>
        <div class="summaryContent">
            <div class="paragraph">
                <span class="informText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_37) %></span>
            </div>

            <div class="paragraph" style="margin-bottom: 0px; margin-left: 0px;word-wrap: break-word">
               <div class="editLink"><a href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.UrlHelper.AppendParameter(Workflow.Steps[0].Url, "ReportWizardGuid", Workflow.Id.ToString())) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_28) %></a></div>
                <img id="favoriteIcon" src="../../images/Reports/Star-empty.png" runat="server" style="margin-left: -20px"/>
                <span class="reportHeader" id ="reportTitle" runat="server"></span>
            </div>

            <div class="paragraph" style="word-wrap: break-word">
                <span class="informText" id="reportSubTitle" runat="server"></span>
            </div>

            <div class="paragraph" style="word-wrap: break-word">
                <div class="header"> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_38) %> </div>
                <span class="informText" id="reportDescription" runat="server"></span>
            </div>

            <div class="paragraph">
                <div class="header"> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_39) %> </div>
                <span class="informText" id="reportCategory" runat="server"></span>
            </div>

            <div class="paragraph">
                <div class="header"> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_40) %> </div>
                 <asp:Panel CssClass="informText" runat="server" ID="reportCustomProperties"></asp:Panel>
            </div>

            <div class="paragraph">
                <div class="header"> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_41) %> </div>
                <span class="informText" id="reportLimitatiom" runat="server"></span>
            </div>

            <div class="verticalLine"></div>

            <div class="paragraph">
                <div class="editLink"><a href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.UrlHelper.AppendParameter(Workflow.Steps[0].Url, "ReportWizardGuid", Workflow.Id.ToString())) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_28) %></a></div>
                <span class="header" > <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_42) %> </span>
                <asp:Panel CssClass="informText" runat="server" ID="reportObjects"></asp:Panel>
            </div>

            <div class="paragraph">
                <div class="header"> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_43) %> </div>
                 <asp:Panel CssClass="informText" runat="server" ID="reportTimeFrames"></asp:Panel>
            </div>

            <div class="paragraph">
                <div class="header"> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_44) %> </div>
                 <asp:Panel CssClass="informText" runat="server" ID="reportResource"></asp:Panel>
            </div>

            <div class="verticalLine"></div>

            <div class="paragraph">
                <div class="editLink"><a href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.UrlHelper.AppendParameter(Workflow.Steps[3].Url, "ReportWizardGuid", Workflow.Id.ToString())) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_28) %></a></div>
                <span class="header"> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_45) %> </span>
                <asp:Panel runat="server" ID="reportSchedule"></asp:Panel>
            </div>
            
            <div class="paragraph">
                <div class="header"> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_46) %> </div>
                <span class="informText" id="orionAccount" runat="server"></span>
            </div>

            <div class="paragraph">
                <orion:LocalizableButton runat="server" ID="preview" OnClientClick="<%$ HtmlEncodedCode: GetPreviewURL %>"
                        LocalizedText="CustomText" DisplayType="Secondary" CausesValidation="false" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PS0_67 %>" />
            </div>
            
            <div class="paragraph">
                <input type="checkbox" runat="server" id="viewAfterSave" /><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_174) %>
            </div>
        </div>
        </div>
        </div>
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButton runat="server" ID="LocalizableButton1" OnClick="Back_Click"
                LocalizedText="Back" DisplayType="Secondary" CausesValidation="false" />
            <orion:LocalizableButton runat="server" ID="imgbNext" OnClick="Next_Click" LocalizedText="Submit"
                DisplayType="Primary" />
            <orion:LocalizableButton runat="server" ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel"
                DisplayType="Secondary" CausesValidation="false" />
        </div>
    </asp:Panel>
</asp:Content>
