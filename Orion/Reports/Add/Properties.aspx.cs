﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;

using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Core.Reporting.DAL;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.Reporting;

public partial class Orion_Reports_Add_Properties : AddReportWizardBase
{
	private static Log log = new Log();
	public const string AddNewSelValue = "-";
    public string DefaultReportCategory = Resources.CoreWebContent.WEBCODE_AK1_1;
	public string FavoriteUrl { get; set; }

	protected void Page_Load(object sender, EventArgs e)
	{
		if (Workflow.ReportId != 0)
		{
			Title = Resources.CoreWebContent.WEBDATA_SO0_66;
		}

		if (!Page.IsPostBack)
		{
			InitControls();
		}
        FavoriteUrl = favoriteReport.Checked ? "../../images/Reports/Star-full.png" : "../../images/Reports/Star-empty.png";

		Workflow.SetStepFromUrl(this.Page.Request.Url.GetLeftPart(UriPartial.Path));
		this.UpdateProgressIndicator();
	}

	private void InitControls()
	{
        var rpCategories = ReportDAL.GetReportCategories();
	    if (rpCategories.Select(string.Format("Category = '{0}'", DefaultReportCategory)).Length == 0)
	    {
	        DataRow newRow = rpCategories.NewRow();
	        newRow["Category"] = DefaultReportCategory;
	        rpCategories.Rows.Add(newRow);
	        DataView dv = new DataView(rpCategories);
	        dv.Sort = "Category ASC";
	        rpCategories = dv.ToTable();
	    }
	    reportCategory.DataSource = rpCategories;
		reportCategory.DataTextField = "Category";
		reportCategory.DataValueField = "Category";
		reportCategory.DataBind();
        if (Profile.AllowAdmin || Profile.AllowReportManagement)
		{
			reportCategory.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_YK0_29, AddNewSelValue));
		}
        var limCategory = ReportDAL.GetAllReportLimitationCategories();

		limitationCategory.Items.Add(new ListItem(Resources.CoreWebContent.WEBCODE_TM0_116, CoreConstants.ReportDefaultFolder));
		limitationCategory.Items.Add(new ListItem(Resources.CoreWebContent.WEBCODE_TM0_78, ""));

		foreach (var val in from DataRow row in limCategory.Rows where row != null select row["LimitationCategory"].ToString())
		{
			if (val.Equals(CoreConstants.ReportDefaultFolder, StringComparison.OrdinalIgnoreCase) || val.Equals("No Reports", StringComparison.OrdinalIgnoreCase) || string.IsNullOrEmpty(val))
				continue;

			limitationCategory.Items.Add(new ListItem(val, val));
		}

		if (Profile.AllowAdmin)
		{
			limitationCategory.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_YK0_29, AddNewSelValue));
		}

		foreach (ListItem item in limitationCategory.Items)
		{
			if (item.Value.Equals(Profile.ReportFolder, StringComparison.Ordinal))
				limitationCategory.SelectedValue = item.Value;
		}

		var report = ReportStorage.Instance.GetReport(Workflow.WizardGuid);

		// handle back behaviour when adding new category
	    if (!string.IsNullOrEmpty(report.Category))
	    {
	        if (reportCategory.Items.FindByValue(report.Category) == null)
	        {
	            reportCategory.SelectedValue = AddNewSelValue;
	            additionalCategoryValue.Text = report.Category;
	        }
	        else
	        {
	            reportCategory.SelectedValue = report.Category;
	        }
	    }
	    else
	    {
            reportCategory.SelectedValue = DefaultReportCategory;
	    }

	    // handle back behaviour when adding new limitation category
		if (!string.IsNullOrEmpty(report.LimitationCategory) && limitationCategory.Items.FindByValue(report.LimitationCategory) == null)
		{
			limitationCategory.SelectedValue = AddNewSelValue;
			additionalLimitationCategoryValue.Text = report.LimitationCategory;
		}
		else
		{
			limitationCategory.SelectedValue = report.LimitationCategory;
		}

		reportDescr.Text = (Workflow.ReportId == 0 && string.IsNullOrEmpty(report.Description)) ? report.Header.SubTitle : report.Description;
		favoriteReport.Checked = report.IsFavorite;

		if (Workflow.ReportId != 0)
		{
			reportCps.EntityIds = new List<int>() { Workflow.ReportId };
		}
	}

	protected override void OnLoadComplete(EventArgs e)
	{
		base.OnLoadComplete(e);
		if (!IsPostBack)
		{
			var report = ReportStorage.Instance.GetReport(Workflow.WizardGuid);
			reportCps.CustomProperties = report.CustomProperties;
		}
	}

	protected string LimitationUsageHelpURL
	{
		get
		{
			return HelpHelper.GetHelpUrl("OrionCorePHUsingReportLimitationCategories");
		}
	}

	protected void LimitationValidate(object source, ServerValidateEventArgs args)
	{
		args.IsValid = true;

		if (limitationCategory.Text.Equals(AddNewSelValue))
		{
			string newVal = additionalLimitationCategoryValue.Text.Trim();
			foreach (ListItem cat in limitationCategory.Items)
			{
				if (cat.Value.Equals(newVal, StringComparison.OrdinalIgnoreCase))
				{
					args.IsValid = false;
					return;
				}
			}
			args.IsValid = !OrionReportDAL.CheckLimitationCategoryExists(additionalLimitationCategoryValue.Text.Trim());
		}
	}

	protected void ReportNameValidate(object source, ServerValidateEventArgs args)
	{
		args.IsValid = true;

		var report = ReportStorage.Instance.GetReport(Workflow.WizardGuid);

		var limitationCategoryValue = (limitationCategory.Text.Equals(AddNewSelValue))
										   ? additionalLimitationCategoryValue.Text.Trim()
										   : limitationCategory.Text;
		if (((report.Name == null || !report.Header.Title.Equals(report.Name))
				|| (report.LimitationCategory == null || (!report.LimitationCategory.Equals(limitationCategoryValue))))
				&& !ReportHelper.IsReportNameUnique(report.Header.Title, limitationCategoryValue))
		{
			uniqueReportNameValidator.ErrorMessage = String.Format(Resources.CoreWebContent.WEBDATA_TM0_297, limitationCategoryValue);
			args.IsValid = false;
		}
	}

    protected override bool ValidateUserInput()
    {
        Page.Validate("EditCpValue");

        return Page.IsValid;
    }

    protected override bool SaveCurrentState()
    {
        try
        {
            var report = ReportStorage.Instance.GetReport(Workflow.WizardGuid);
            if (report != null)
            {
                report.Category = (reportCategory.Text.Equals(AddNewSelValue))
                                            ? additionalCategoryValue.Text
                                            : reportCategory.Text;

                report.Name = report.Header.Title;
                report.Description = reportDescr.Text;
                report.IsFavorite = favoriteReport.Checked;
                report.LimitationCategory = (limitationCategory.Text.Equals(AddNewSelValue))
                                               ? additionalLimitationCategoryValue.Text.Trim()
                                               : limitationCategory.Text;
                report.CustomProperties = reportCps.CustomProperties;
            }
            return true;

        }
        catch (Exception ex)
        {
            log.Error("Error while saving report data", ex);
            return false;
        }
    }
}
