using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.Reporting.DAL;
using SolarWinds.Orion.Core.Reporting.Models.WebResource;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.Reporting;

using SolarWinds.Reporting.Models.Layout;

public partial class Orion_Reports_Add_Summary : AddReportWizardBase
{
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    public string GetPreviewURL
	{
		get
		{
			return "window.open('" + UrlHelper.AppendParameter(Workflow.Steps[1].Url, "ReportWizardGuid", Workflow.Id.ToString()) + "', '_blank'); return false;";
		}
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		Workflow.SetStepFromUrl(this.Page.Request.Url.GetLeftPart(UriPartial.Path));
		this.UpdateProgressIndicator();

		if (Workflow.ReportId != 0)
			Title = Resources.CoreWebContent.WEBDATA_SO0_66;

		InitControls();

		// block saving report in case of demo mode
		if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
		{
			imgbNext.OnClientClick = "demoAction('Core_Reporting_SaveScheduledReport', this); return false;";
		}
	}

	public void InitControls()
	{
		var report = ReportStorage.Instance.GetReport(Workflow.WizardGuid);

		reportTitle.InnerText = report.Header.Title;
		reportSubTitle.InnerText = report.Header.SubTitle;
		reportDescription.InnerText = report.Description;
		reportCategory.InnerText = report.Category;

		string lim;
		if (string.IsNullOrEmpty(report.LimitationCategory) || report.LimitationCategory.Equals("No Reports", StringComparison.OrdinalIgnoreCase))
			lim = Resources.CoreWebContent.WEBCODE_TM0_78;
		else
			if (report.LimitationCategory.Equals(CoreConstants.ReportDefaultFolder))
				lim = Resources.CoreWebContent.WEBCODE_TM0_116;
			else lim = report.LimitationCategory;

		reportLimitatiom.InnerText = lim;
		orionAccount.InnerText = HttpContext.Current.Profile.UserName;

		if (report.DataSources != null && report.DataSources.Length > 0)
		{
			List<Guid> dataSourcesInUse = GetDataSourcesInUse(report.Sections);

			foreach (var dataSource in report.DataSources)
			{
				if (dataSourcesInUse.Contains(dataSource.RefId))
					reportObjects.Controls.Add(new Label { Text = HttpUtility.HtmlEncode(dataSource.Name) + "<br/>" });
			}
		}

		if (report.CustomProperties != null && report.CustomProperties.Count > 0)
			foreach (var customProperties in report.CustomProperties)
			{
				reportCustomProperties.Controls.Add(new Label { Text = string.Format("{0}: {1}<br/>", customProperties.Key, HttpUtility.HtmlEncode(customProperties.Value)) });
			}

		if (report.Configs != null && report.Configs.Length > 0)
		{
			foreach (var configs in report.Configs)
			{
				reportResource.Controls.Add(new Label { Text = HttpUtility.HtmlEncode(configs.DisplayTitle) + "<br/>" });

				//adding timeframes from standard resources:
				if (configs is WebResourceConfiguration)
				{
					foreach (var setting in (configs as WebResourceConfiguration).Settings)
					{
						if (setting.Name.Equals("Period", StringComparison.OrdinalIgnoreCase) && !string.IsNullOrEmpty(setting.Value))
						{
							var rSettings = ReportHelper.GetReportSettings(setting.Value, string.Empty);
							reportTimeFrames.Controls.Add(new Label { Text = String.Format("{0} ({1} - {2})", HttpUtility.HtmlEncode(setting.Value), rSettings.PeriodBegin, rSettings.PeriodEnd) + "<br/>" });
						}
					}
				}
			}
		}

		if (report.IsFavorite)
		{
			favoriteIcon.Src = "../../images/Reports/Star-full.png";
		}

		if (report.TimeFrames != null && report.TimeFrames.Length > 0)
		{
			List<Guid> timeInUse = GetTimePeriodsInUse(report.Sections);
			foreach (var timeFrame in ReportHelper.DeduplicateTimeFrames(report.TimeFrames))
			{
				if (timeFrame != null && timeInUse.Contains(timeFrame.RefId))
				{
					timeFrame.Reinitalize();
					reportTimeFrames.Controls.Add(new Label { Text = String.Format("{0} ({1} - {2})", HttpUtility.HtmlEncode(timeFrame.DisplayName), timeFrame.Start, timeFrame.End) + "<br/>" });
				}
			}
		}

		//if new schedule created
		var jobConfiguration = ReportSchedulesStorage.Instance.GetJob(Workflow.WizardGuid);
		if (jobConfiguration != null && Session["HfScheduleMode"].ToString().Equals("new", StringComparison.OrdinalIgnoreCase))
		{
			orionAccount.InnerText = jobConfiguration.AccountID;
			reportSchedule.Controls.Add(new Label { Text = HttpUtility.HtmlEncode(jobConfiguration.Name) });

            if (Session["HfSelectedSchedules"] != null)
            {
                var dict = (Dictionary<int, ScheduleInfo>)OrionSerializationHelper.FromJSON(Session["HfSelectedSchedules"].ToString(), typeof(Dictionary<int, ScheduleInfo>));
                PopulateScheduleList(dict);
            }
		}

			//if existing schedule assigned
		else if (Session["HfSelectedSchedules"] != null)
		{
			var dict = (Dictionary<int, ScheduleInfo>)OrionSerializationHelper.FromJSON(Session["HfSelectedSchedules"].ToString(), typeof(Dictionary<int, ScheduleInfo>));
			PopulateScheduleList(dict);
		}
		else
		{
			var dict = ReportDAL.GetReportSchedulesInfo(Workflow.ReportId);
			PopulateScheduleList(dict);
		}
	}

	private void PopulateScheduleList(Dictionary<int, ScheduleInfo> dict)
	{
		if (dict == null) return;
		foreach (var schedule in dict)
		{
			reportSchedule.Controls.Add(new HyperLink
			{
				Text = HttpUtility.HtmlEncode(schedule.Value.Name) + "<br/>",
				NavigateUrl = String.Format("/Orion/Reports/ScheduleAdd/Default.aspx?ScheduleWizardGuid={0}&ReportJobId={1}", Guid.NewGuid(), schedule.Key),
				Target = "_blank",
				CssClass = "imageItem"
			});
		}
	}

	protected override bool Next()
	{
		Page.Validate();
		if (!Page.IsValid) return false;

		var report = ReportStorage.Instance.GetReport(Workflow.WizardGuid);
		if (report.ReportGuid == Guid.Empty) report.ReportGuid = Guid.NewGuid();

		List<object> argList;
		string commandName = "CreateReport";
		var definition = Loader.Serialize(report);
		if (Workflow.ReportId != 0)
		{
			commandName = "UpdateReport";
			argList = new List<object> {Workflow.ReportId, report.Name, report.Description, report.LimitationCategory, report.Category, report.Header.Title, 
                report.Header.SubTitle, definition, report.IsFavorite.ToString(), HttpContext.Current.Profile.UserName, HttpContext.Current.Profile.UserName };

		}
		else
			argList = new List<object> {report.Name, report.Description, report.LimitationCategory, report.Category, report.Header.Title, 
                report.Header.SubTitle, definition, report.IsFavorite.ToString(), HttpContext.Current.Profile.UserName, HttpContext.Current.Profile.UserName };

		try
		{
			using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
			{
				var resultXml = service.Invoke("Orion.Report",
											   commandName,
							   CustomPropertyHelper.GetArgElements(argList)); //TODO: GetArgElements should be moved to some common place

				if (Workflow.ReportId == 0)
					Session["NewReportId"] = resultXml.InnerText;
				else
					Session["NewReportId"] = Workflow.ReportId;

				foreach (var cp in report.CustomProperties.Where(cp => cp.Value != null))
				{
					if (Workflow.ReportId != 0 || (cp.Value != null && !string.IsNullOrEmpty(cp.Value.ToString()))) //no need to run update query for empty cps
					{
						CustomPropertyMgr.SetCustomProp("ReportDefinitions", cp.Key,
														Convert.ToInt32(Session["NewReportId"].ToString()), cp.Value);

						// update restricted values if needed
						var customProperty = CustomPropertyMgr.GetCustomProperty("ReportDefinitions", cp.Key);
						CustomPropertyHelper.UpdateRestrictedValuesIfNeeded(customProperty,
							new[] { CustomPropertyHelper.GetInvariantCultureString((cp.Value != null) ? cp.Value.ToString() : string.Empty, customProperty.PropertyType) });
					}
				}

				ScheduleReport(Convert.ToInt32(Session["NewReportId"].ToString()), report.Name);

				DraftCache.CleanDraftForCurrentUser();
			}
			//redirect to view report page
			if (viewAfterSave.Checked)
				returnUrl = string.Format("/Orion/Report.aspx?ReportID={0}&ReturnTo={1}", Session["NewReportId"], UrlHelper.ToSafeUrlParameter("/Orion/Reports/Default.aspx"));

		}
		catch (Exception ex)
		{
			log.Error(ex);
			throw;
		}
		finally
		{
			ReportStorage.Instance.Discard(Workflow.WizardGuid);
		}
		return base.Next();
	}

	private void ScheduleReport(int reportId, string reportTitle)
	{
		if (Session["HfScheduleMode"] != null)
		{
			var scheduleMode = Session["HfScheduleMode"].ToString();

			var jobConfiguration = ReportSchedulesStorage.Instance.GetJob(Workflow.WizardGuid);
			if (jobConfiguration != null)
			{
				jobConfiguration.Reports.Add(new ReportTuple { ID = reportId, Title = reportTitle });
			}

			int primaryEngineId = EnginesDAL.GetPrimaryEngineId();

			switch (scheduleMode)
			{
				case "existing":
					// updating existing assignments
					if (!string.IsNullOrEmpty(Session["HfSelectedSchedules"].ToString()))
					{
						var dict = (Dictionary<int, ScheduleInfo>)OrionSerializationHelper.FromJSON(Session["HfSelectedSchedules"].ToString(), typeof(Dictionary<int, ScheduleInfo>));
                        using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, primaryEngineId))
						{
							businessProxy.AssignJobsToReport(reportId, new List<int>(dict.Keys));
						}
					}
					break;
				case "new":
                    using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, primaryEngineId))
					{

                        if (!string.IsNullOrEmpty(Session["HfSelectedSchedules"].ToString()))
                        {
                            var dict = (Dictionary<int, ScheduleInfo>)OrionSerializationHelper.FromJSON(Session["HfSelectedSchedules"].ToString(), typeof(Dictionary<int, ScheduleInfo>));
                            businessProxy.AssignJobsToReport(reportId, new List<int>(dict.Keys));
                        }

						businessProxy.AddReportJob(jobConfiguration);
					}
					break;
				default:
					// delete all schedule assignments for report
                    using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, primaryEngineId))
					{
						businessProxy.AssignJobsToReport(reportId, new List<int>());
					}
					break;
			}
		}
	}

	private static List<Guid> GetDataSourcesInUse(Section[] sections)
	{
		var list = new List<Guid>();

		if (sections == null || sections.Length == 0)
			return list;

		foreach (var section in sections)
		{
			if (section == null || section.Columns == null)
				continue;
			foreach (var sectionCol in section.Columns)
			{
				if (sectionCol == null || sectionCol.Cells == null)
					continue;
				foreach (var sectionCell in sectionCol.Cells)
				{
					if (sectionCell == null)
						continue;

					if (sectionCell.DataSelectionRefId != Guid.Empty && !list.Contains(sectionCell.DataSelectionRefId))
						list.Add(sectionCell.DataSelectionRefId);
				}
			}
		}

		return list;
	}

	private static List<Guid> GetTimePeriodsInUse(Section[] sections)
	{
		var list = new List<Guid>();

		if (sections == null || sections.Length == 0)
			return list;

		foreach (var section in sections)
		{
			if (section == null || section.Columns == null)
				continue;
			foreach (var sectionCol in section.Columns)
			{
				if (sectionCol == null || sectionCol.Cells == null)
					continue;
				foreach (var sectionCell in sectionCol.Cells)
				{
					if (sectionCell == null)
						continue;

					if (sectionCell.TimeframeRefId != Guid.Empty && !list.Contains(sectionCell.TimeframeRefId))
						list.Add(sectionCell.TimeframeRefId);
				}
			}
		}

		return list;
	}

	private static void BusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}
}