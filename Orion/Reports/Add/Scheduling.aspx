<%@ Page Language="C#" MasterPageFile="~/Orion/Reports/Add/AddReportWizard.master"
    AutoEventWireup="true" CodeFile="Scheduling.aspx.cs" Inherits="Orion_Reports_Add_Scheduling"
    Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_161 %>" %>

<%@ Register TagPrefix="orion" TagName="FrequencyControl" Src="~/Orion/Controls/FrequencyControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="SchedulePicker" Src="~/Orion/Reports/Controls/SchedulePicker.ascx" %>
<%@ Register TagPrefix="orion" TagName="ActionsEditorView" Src="~/Orion/Actions/Controls/ActionsEditorView.ascx" %>
<%@ Register TagPrefix="orion" TagName="RSAdvancedSettingsControl" Src="~/Orion/Controls/RSAdvancedSettingsControl.ascx" %>
<asp:Content ContentPlaceHolderID="headPlaceholder" runat="server">
</asp:Content>

<asp:Content ContentPlaceHolderID="wizardContent" runat="Server">
  <orion:Include File="ReportSchedule.css" runat="server" />
    <asp:Panel runat="server" DefaultButton="imgbNext">
        <div class="GroupBox" style="min-height: 400px;">
            <span style="font-weight: bold; font-size: 16px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YP0_3) %></span>
            <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_63) %></p>
            <br/>
            <input type="radio" id="rbNoSchedule" name="rbScheduled" runat="server" class="rbScheduled" value="0" /><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_168) %><br/>
            <input type="radio" id="rbWithSchedule" name="rbScheduled" runat="server" class="rbScheduled" value="1" /><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_169) %><br/>
            <div runat="server" id="scheduleOptions" style="padding: 10px 10px 10px 20px">
                <div>
                <select runat="server" id="selectSchedule">
                    <option value="0" text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_170%>"></option>
                    <option value="1" text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_171%>"></option>
                </select>
                <span style="float: right;">
                            <img src="/Orion/Nodes/images/icons/icon_edit.gif" alt="" />
                            <a href="/Orion/Reports/Scheduler.aspx" class="RenewalsLink" target="_blank">
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_306) %></a> </span>
                </div>
                <div runat="server" id="browseMode" style="padding: 10px 0px 10px 0px;">
                    <label class="boldLabel" style="float: left;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YP0_21) %></label><br/>
                    <orion:SchedulePicker runat="server" 
                        Title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_YP0_2 %>" 
                        GetHeaderTitleFn="<%$ HtmlEncodedCode: GetHeaderTitleFn %>" 
                        RenderSelectedItems="True" OnSchedulesSelected="scheduleSelected" OnUpdateSelectedSchedules="updateSelectedSchedules" />
                </div>
                <div runat="server" id="createMode" style="padding: 10px 0px 10px 0px; ">
                        <div style="padding: 5px 5px 5px 5px; width: 788px; margin-top: 15px;">
                    <table style="padding: 5px 5px 5px 5px; width: 100%;">
                        <tr><td><label class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_173) %></label></td></tr>
                        <tr>
                            <td>
                                <asp:TextBox runat="server" Width="450" ID="Name" MaxLength="255" /><span style="margin: 0 10px;">
                                <asp:CustomValidator runat="server" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AB0_9 %>" OnServerValidate="ScheduleNameValidator" ControlToValidate="Name" ValidationGroup="Scheduling" ValidateEmptyText="True"></asp:CustomValidator></span>
                            </td>
                        </tr>
                        <tr><td><label class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_45) %></label></td></tr>
                        <tr><td><textarea runat="server" style="width: 600px; max-width: 760px; border:1px solid #ABADB3; resize: both; border:1px solid #ABADB3;" rows="5" ID="Description"></textarea></td></tr>
                    </table>
                    </div>
                    <h2 style="padding-top: 10px"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_112) %></h2>
                    <div id="timePeriod">
                        <orion:FrequencyControl runat="server" ID="frequency" UseTimePeriodMode="False" SingleScheduleMode="False"/>
                    </div>
                    <h2 style="padding-top: 10px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_309) %></h2>
                    <orion:ActionsEditorView runat="server" ID="actionsEditorView" ClientInstanceName="ReportingActions" EnvironmentType="Reporting" />
                    <orion:RSAdvancedSettingsControl runat="server" ID="RSAdvancedSettingsControl" />
                </div>
            </div>
            </div>
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButton runat="server" ID="LocalizableButton1" OnClick="Back_Click"
                LocalizedText="Back" DisplayType="Secondary" CausesValidation="false" />
            <orion:LocalizableButton runat="server" ID="imgbNext" OnClientClick="return SW.Orion.FrequencyController.Validate();" OnClick="Next_Click" LocalizedText="Next"
                DisplayType="Primary" ValidationGroup="Scheduling"/>
            <orion:LocalizableButton runat="server" ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel"
                DisplayType="Secondary" CausesValidation="false" />
        </div>
    </asp:Panel>
    <input type="hidden" runat="server" id="hfSelectedSchedules"/>
    <input type="hidden" runat="server" id="hfScheduleMode"/>
    <script type="text/javascript">
        $(function () {
            $("input.rbScheduled").click(function () {
                if (this.value == "0") {
                    $("[id*=scheduleOptions]").css("display", "none");
                    $("[id*=hfScheduleMode]").val("none");
                }
                else {
                    $("[id*=scheduleOptions]").css("display", "block");
                    $("[id*=hfScheduleMode]").val($("[id*=selectSchedule]").val() == "0" ? "existing" : "new");
                }
            });
            $("[id*=selectSchedule]").click(function () {
                if (this.value == "0") {
                    $("[id*=createMode]").css("display", "none");
                    $("[id*=browseMode]").css("display", "block");
                    $("[id*=hfScheduleMode]").val("existing");
                    var assignedSchedules = JSON.parse($("[id*=hfSelectedSchedules]").val() == "" ? "[]" : $("[id*=hfSelectedSchedules]").val());
                    var schedulesDict = [];
                    $.each(assignedSchedules, function () {
                        schedulesDict.push({ ID: this.Key, Info: this.Value });
                    });
                    SW.Orion.SchedulePicker.setSelectedReportJobs(schedulesDict);
                }
                else {
                    $("[id*=createMode]").css("display", "block");
                    $("[id*=browseMode]").css("display", "none");
                    $("[id*=hfScheduleMode]").val("new");
                }
            });
            if (Ext.isEmpty($("[id*=hfScheduleMode]").val())) {
                $("[id*=selectSchedule]").click();
                var selectedSchedules = $("[id*=hfSelectedSchedules]").val();
                if (Ext.isEmpty(selectedSchedules) || selectedSchedules == "[]") {
                    $("input.rbScheduled[value=0]").click();
                } else {
                    $("input.rbScheduled[value=1]").click();
                    var schedules = JSON.parse(selectedSchedules);
                    var pickedSchedules = [];
                    $.each(schedules, function () {
                        pickedSchedules.push({ ID: this.Key, Info: this.Value });
                    });
                    SW.Orion.SchedulePicker.setSelectedReportJobs(pickedSchedules);

                }
            } else {
                if ($("[id*=hfScheduleMode]").val() != "") {
                    $("input.rbScheduled[value=1]").click();
            }
                $("[id*=selectSchedule]").click();
            }
        });
        var scheduleSelected = function (dictionary) {
            $("[id*=hfSelectedSchedules]").val(JSON.stringify(dictionary));
        };

        var updateSelectedSchedules = function (selectedJobs) {
            var dict = [];
            $.each(selectedJobs, function () { dict.push({ Key: this.ID, Value: this.Info }); });
            $("[id*=hfSelectedSchedules]").val(JSON.stringify(dict));
        };
        $(function () {
            var dict = [];
            var pickedJobs = $("[id*=hfSelectedSchedules]").val();
            $.each(JSON.parse(pickedJobs), function () { dict.push({ ID: this.Key, Info: this.Value }); });
            SW.Orion.SchedulePicker.setSelectedReportJobs(dict);
        });
        var frequency = <%= SchedulesListJson %>;
        var containerTimeOfDay = $("#timePeriod");
        SW.Orion.FrequencyControllerDefinition = new SW.Orion.FrequencyController({
            container: containerTimeOfDay,
            frequencies: frequency,
            useTimePeriodMode: false,
            singleScheduleMode: false,
        });
        SW.Orion.FrequencyControllerDefinition.init();
</script>
</asp:Content>
