<%@ Page Language="C#" MasterPageFile="~/Orion/Reports/Add/AddReportWizard.master"
    AutoEventWireup="true" CodeFile="Preview.aspx.cs" Inherits="Orion_Reports_Add_Preview"
    Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_161 %>" %>
<%@ Register Assembly="OrionWeb" Namespace="SolarWinds.Orion.Web.Reporting" TagPrefix="orionrpt" %>
<%@ Register TagPrefix="xui" TagName="Include" Src="~/Orion/Controls/XuiInclude.ascx" %>

<asp:Content ContentPlaceHolderID="headPlaceholder" runat="server">
    <xui:Include runat="server" />
    <orion:Include runat="server" File="visibilityObserver.js" />
</asp:Content>
<asp:Content ContentPlaceHolderID="wizardContent" runat="Server">
    <asp:Panel runat="server" DefaultButton="imgbNext">
        <div class="GroupBox">
            <orionrpt:ReportPreviewControl ID="Preview" runat="server" />            
        </div>
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButton runat="server" ID="LocalizableButton1" OnClick="Back_Click"
                LocalizedText="Back" DisplayType="Secondary" CausesValidation="false" />
            <orion:LocalizableButton runat="server" ID="imgbNext" OnClick="Next_Click" LocalizedText="Next"
                DisplayType="Primary" />
            <orion:LocalizableButton runat="server" ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel"
                DisplayType="Secondary" CausesValidation="false" />
        </div>
    </asp:Panel>
    <script type="text/javascript">
    // <![CDATA[
        var reportWidth = $('.sw-rpt-frame').width() + 30;
        if (reportWidth > $('#wizardWrapper').width())
            $('#wizardWrapper').width(reportWidth);

        //FB219269 - remove this when detach resource is handled in report rendering
        $('a[href*="DetachResource"]').click(function () { return false; });
    // ]]>
    </script>
</asp:Content>

