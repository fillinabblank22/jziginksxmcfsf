<%@ Page Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_110%>" Language="C#" MasterPageFile="~/Orion/Reports/ReportsMasterPage.master" AutoEventWireup="true" CodeFile="Scheduler.aspx.cs" Inherits="Orion_Reports_Scheduler" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Swis" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/Reports/Controls/ReportTabBar.ascx" TagPrefix="orion" TagName="ReportTabBar" %>
<%@ Register tagPrefix="orion" tagName="ReportPicker" src="~/Orion/Reports/Controls/ReportPicker.ascx" %>
<asp:Content ContentPlaceHolderID="HeadPlaceholder" Runat="Server">
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js" />
    <orion:Include runat="server" File="Reports/js/ReportSchedulerGrid.js" />
    <orion:Include runat="server" File="Admin/js/SearchField.js" />
    <orion:Include ID="Include1" File="ReportSchedule.css" runat="server" />
    <script type="text/javascript">
        Ext.onReady(function() {
            SW.Orion.ReportScheduler.init({ SwisBasedReportsOnly: <%= SwisFederationInfo.IsFederationEnabled.ToString().ToLower() %> })
        }, SW.Orion.ReportScheduler);
    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="TopRightPageLinks" Runat="Server">
    <div class="contTitleBarDate">
		<orion:IconHelpButton ID="btnHelp" runat="server"  HelpUrlFragment="OrionCoreAGCreatingWebBasedScheduledReportJob" />
		<div style="padding-top: 5px; font-size: 8pt; text-align: right;"><%= DateTime.Now.ToString("F") %></div>
	</div>
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceholder" Runat="Server">
    <div id="mainContent">
            <div id="Hint" style="position: static; width: 100%;" runat="server">
                <table>
                    <tr>
                        <td>
                            <h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
                        </td>
                    </tr>
                    <tr>
                        <td>
                              <div runat="server" id="ValidationOk" class="sw-suggestion sw-suggestion-pass" style="margin-bottom: 10px;" visible="false" >
                                <span class="sw-suggestion-icon"></span>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        
        <orion:ReportTabBar  ID="reportTabBar" runat="server" Visible="true" />

        <table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <input type="hidden" name="ReportScheduler_PageSize" id="ReportScheduler_PageSize" value='<%= DefaultSanitizer.SanitizeHtml(WebUserSettingsDAL.Get("ReportScheduler_PageSize")) %>' />
                    <!-- TODO: update default values for selected columns -->
                    <input type="hidden" name="ReportScheduler_SelectedColumns" id="ReportScheduler_SelectedColumns" value='<%= DefaultSanitizer.SanitizeHtml(WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, "ReportScheduler_SelectedColumns", WebConstants.ReportSchedulerDafultSelectedColumns)) %>' />
                    <input type="hidden" name="ReportScheduler_SortOrder" id="ReportScheduler_SortOrder" value='<%= DefaultSanitizer.SanitizeHtml(WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, "ReportScheduler_SortOrder", WebConstants.ReportSchedulerDafultSortOrder)) %>' />
                    <input type="hidden" name="ReportScheduler_GroupingValue" id="ReportScheduler_GroupingValue" value='<%= DefaultSanitizer.SanitizeHtml(WebUserSettingsDAL.Get("ReportScheduler_GroupingValue")) %>' />
                    <input type="hidden" name="ReportScheduler_AllowScheduleMgmt" id="ReportScheduler_AllowScheduleMgmt" value='<%= this.Profile.AllowReportManagement %>' />
				
                    <div id="ReportSchedulerGrid"></div>
                </td>
            </tr>
        </table>
    </div>
    <orion:ReportPicker ID="ReportPicker1" runat="server"
        Title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AB0_13 %>"
        GetHeaderTitleFn="SW.Orion.ReportScheduler.getAssignReportsHeaderTitle"
        OnReportsSelected="SW.Orion.ReportScheduler.reportsSelected" BlockIfDemo="True"/>
</asp:Content>

