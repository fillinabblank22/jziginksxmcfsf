<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true" CodeFile="ViewReports.aspx.cs" Inherits="Orion_Reports_ViewReports" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_55 %>" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Swis" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ContentPlaceHolderID="headplaceholder" runat="server">
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js" />
    <orion:Include runat="server" File="Admin.css" />

<style type="text/css">
    .contTitleBarDate { font-size: 8pt; text-align:right; }
    #mainContent h1 { font-size: large; padding: 15px 0px 10px 10px; }
    #mainContent { padding: 0px 5px ; }
    #ReportManagerGrid { margin-bottom:15px; }
</style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" runat="server">
        <div class="contTitleBarDate">
            <% if (Profile.AllowReportManagement) { %>
                <a href="/Orion/Reports/Default.aspx" style="background: transparent url(/Orion/images/CPE/manage_nodes.gif) scroll no-repeat left center;
            padding: 2px 0px 2px 20px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_52) %></a>
            <% } %>
		    <orion:IconHelpButton ID="btnHelp" runat="server"  HelpUrlFragment="OrionCoreAGViewingReportsOrionWebConsole" />
		    <div style="padding-top: 5px; font-size: 8pt; text-align: right;"><%= DateTime.Now.ToString("F") %></div>
	    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <input type="hidden" id="grid_page_size" value="<%= HttpUtility.HtmlEncode(WebUserSettingsDAL.Get("ReportManager_PageSize")) %>" />
    <input type="hidden" id="grid_selected_columns" value="<%= HttpUtility.HtmlEncode(WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, "ReportManager_SelectedColumns", WebConstants.AllReportsDefaultSelectedColumns)) %>" />
    <input type="hidden" id="grid_sort_order" value="<%= HttpUtility.HtmlEncode(WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, "ReportManager_SortOrder", WebConstants.AllReportsDefaultSortOrder)) %>" />
    <input type="hidden" id="grid_grouping_value" value="<%= HttpUtility.HtmlEncode(WebUserSettingsDAL.Get("ReportManager_GroupingValue")) %>" />
    <script type="text/javascript">
        
        var reportManager;
        var IsFederationEnabled = <%= SwisFederationInfo.IsFederationEnabled.ToString().ToLower() %>;
        
        $(function()
        {
            reportManager = new SW.Orion.ReportManager(
                {
                    ContainerID: 'ReportManagerGrid',
                    PageSize:               $("#grid_page_size").val(),
                    SelectedColumns:        $("#grid_selected_columns").val(),
                    SortOrder:              $("#grid_sort_order").val(),
                    GroupingValue:          $("#grid_grouping_value").val(),
                    ReturnToUrl: '<%= ReturnUrl %>',
                    IsManagerGrid: 'false',
                    SwisBasedReportsOnly: IsFederationEnabled
                });

            reportManager.init();
        });
    </script>

<div id="mainContent">
    <div style="display:none;">
        <img src="/Orion/images/Reports/Star-full.png" alt=""/>
        <img src="/Orion/images/Reports/Star-empty.png" alt=""/>
    </div>
    <h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    <h2 runat="server" id="subtitleText" style="font-size:small;" />
    
    <orion:Include runat="server" File="Admin/js/SearchField.js" />
    <orion:Include runat="server" File="extjs/Ext.sw.grid.RadioSelectionModel.js" />
    <orion:Include runat="server" File="Reports/js/ReportmanagerGrid.js" />
    <table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td style="padding-top:4px!important">
                <div id="ReportManagerGrid"></div>
            </td>
        </tr>
    </table>
    <%if (_isOrionDemoMode) {%> <div id="isOrionDemoMode" style="display: none;"></div> <%}%>
  </div>  
</asp:Content>

