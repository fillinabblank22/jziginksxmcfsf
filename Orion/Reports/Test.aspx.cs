﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web;
using System.Text;
using System.Collections.Specialized;
using SolarWinds.Orion.Web.Reporting.ReportCharts;
using SolarWinds.Reporting.Models;
using SolarWinds.Reporting.Models.Layout;
using SolarWinds.Reporting.Models.Data;
using System.Xml.Linq;
using SolarWinds.Orion.Common;
using System.Runtime.Serialization;
using SolarWinds.Reporting.Impl.Rendering;
using SolarWinds.Orion.Core.Reporting.Rendering;
using SolarWinds.Reporting;
using System.IO;
using SolarWinds.Orion.Core.Reporting.Models.WebResource;
using SolarWinds.Reporting.Models.Tables;
using SolarWinds.Reporting.Models.Styles;
using SolarWinds.Orion.Web.Reporting;
using SolarWinds.Internationalization.Extensions;

public partial class Orion_Reports_Test : System.Web.UI.Page
{
    NameValueCollection queryparts = null;

    protected void Page_Init(object sender, EventArgs e)
    {
        queryparts = HttpUtility.ParseQueryString(Request.Url.Query);

        // - edit

        var id = queryparts.Get("edit");

        if (string.IsNullOrEmpty(id) == false)
        {
            AccessPanel.Visible = false;
            EditPanel.Visible = true;

            if (IsPostBack == false)
            {
                var report = SolarWinds.Orion.Core.Reporting.DAL.SessionCache.Instance.Fetch(new Guid(id));

                // yes, I should be using SerializationHelper.ToXmlString, but that does a lousy job at formatting.
                // this test page can swing this.

                DataContractSerializer dcs = new DataContractSerializer(
                    typeof(SolarWinds.Reporting.Models.Report), 
                    SolarWinds.Orion.Core.Reporting.DAL.Loader.GetKnownTypes());

                var d = new XDocument();
            
                using (var writer = d.CreateWriter())
                {
                    dcs.WriteObject(writer, report);
                    writer.Flush();
                    writer.Close();
                }
    
                ConfigXml.InnerText = d.Root.ToString();
            }

            return;
        }

        // - test

        id = queryparts.Get("test");

        if (string.IsNullOrEmpty(id) == false)
        {
            Response.Cache.SetCacheability( HttpCacheability.NoCache );
            Response.Redirect("/Orion/Reports/Visualize.aspx?ctx=mem-" + id);
            return;
        }

        // - selection

        InitReportList();
        InitViewList();
    }
    
    public void EditBtn_Click(object sender, EventArgs e)
    {
        var id = queryparts.Get("edit");

        if( string.IsNullOrEmpty(id) == false )
        {
            var gid = new Guid(id);

            var entry = SolarWinds.Orion.Core.Reporting.DAL.SessionCache.Instance.Where(x => x.CacheId == gid).FirstOrDefault();

            if (entry != null)
            {
                entry.Config = SerializationHelper.FromXmlString<Report>(ConfigXml.InnerText, SolarWinds.Orion.Core.Reporting.DAL.Loader.GetKnownTypes());
                entry.Dirty = true;
            }

            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Redirect(Request.Url.AbsolutePath + "?ref=" + id);
        }
    }

    public void OnBtnReportTest(object sender, EventArgs e)
    {
        var reportid = Request.Form[InMemoryReports.UniqueID];
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Redirect(Request.Url.AbsolutePath + "?test=" + reportid);
    }

    public void OnBtnReportEdit(object sender, EventArgs e)
    {
        var reportid = Request.Form[InMemoryReports.UniqueID];
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Redirect(Request.Url.AbsolutePath + "?edit=" + reportid);
    }

    public void OnBtnReportDelete(object sender, EventArgs e)
    {
        var reportid = new Guid(Request.Form[InMemoryReports.UniqueID]);
        SolarWinds.Orion.Core.Reporting.DAL.SessionCache.Instance.Discard(reportid);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Redirect(Request.Url.AbsolutePath);
    }

    public void OnBtnViewImport(object sender, EventArgs e)
    {
        int ViewId = int.Parse(Request.Form[InDatabaseViews.UniqueID]);
        Load(ViewId);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Redirect(Request.Url.AbsolutePath);
    }

    void Load(int ViewId)
    {
        var view = ViewManager.GetViewById(ViewId);

        if (view != null)
        {
            var report = new Report();

            report.Header = new SolarWinds.Reporting.Models.Layout.Header
            {
                Visible = true,
                Title = view.ViewTitle
            };

            var columns = new SectionColumn[view.ColumnCount];
            var widths = new[] { view.Column1Width, view.Column2Width, view.Column3Width };
            var resources = ViewManager.GetResourcesForView(view);
            var configs = new List<ConfigurationData>();

            // add fake table cell.

            var defStyling = new CellStyling
            {
                BackgroundColor = "#fff",
                RefId = Guid.NewGuid(),
                Font = new FontStyling { Color = "#000", PxHeight = 14 },
                TextAlign = TextAlignment.InferFromValueType
            };

            var hdrStyling = new CellStyling
            {
                BackgroundColor = "#ddd",
                RefId = Guid.NewGuid(),
                Font = new FontStyling { PxHeight = 16, Boldface = true },
                TextAlign = TextAlignment.InferFromValueType
            };

            var cellStyling = new CellStyling
            {
                RefId = Guid.NewGuid(),
                Borders = new BorderStyling { Color = "#888", Bottom = 1 }
            };

            var tbl = new TableConfiguration { DefaultStyle = defStyling };
            tbl.Columns = new[] {
                new TableColumn {
                    CellStyle = cellStyling,
                    HeaderStyle = hdrStyling,
                    DisplayName = "Column 1"
                },
                new TableColumn {
                    CellStyle = cellStyling,
                    HeaderStyle = hdrStyling,
                    DisplayName = "Column 2"
                },
                new TableColumn {
                    CellStyle = cellStyling,
                    HeaderStyle = hdrStyling,
                    DisplayName = "Column 3"
                }
            };

            configs.Add( tbl );

            // add fake table cell.

            Func<ResourceInfo, Guid> addconfig = delegate(ResourceInfo ri)
            {
                var ret = new WebResourceConfiguration { 
                    RefId = Guid.NewGuid(),
                    DisplayTitle = ri.Title, // todo: I expect one of these sets to go.
                    DisplaySubTitle = ri.SubTitle,
                    Title = ri.Title,
                    SubTitle = ri.SubTitle,
                    ResourceFile = ri.File,
                    ParentViewType = ri.View.ViewType,
                    Settings = ri.Properties.Cast<System.Collections.DictionaryEntry>().Select(
                        x => new ContextValue { Name = (string)x.Key, Value = (string)x.Value }
                    ).ToArray()
                };

                configs.Add(ret);
                return ret.RefId;
            };


        
            for (int i = 0; i < view.ColumnCount; ++i)
            {
                var column = columns[i] = new SectionColumn();
                column.PixelWidth = widths[i];

                var cells = new List<SectionCell>();

                column.Cells = resources.Where(x => x.Column == i+1).Select(x =>

                    new SectionCell
                    {
                        RefId = Guid.NewGuid(),
                        ConfigId = addconfig( x ),
                        DisplayName = x.Title,
                        RenderProvider = WebResourceRenderer.Moniker
                    }

                ).ToArray();
            }

            // get this table added to the top.

            columns[0].Cells = new[] { 
                new SectionCell { 
                    RefId = Guid.NewGuid(),
                    ConfigId = tbl.RefId,
                    DisplayName = "Fake Table",
                    RenderProvider = TableRenderer.Moniker
                } }.Concat(columns[0].Cells).ToArray();

            

            report.Sections = new Section[] { new Section { Columns = columns } };
            report.Configs = configs.ToArray();

            var id = SolarWinds.Orion.Core.Reporting.DAL.SessionCache.Instance.Store(report);

            Response.Redirect(Request.Url.AbsolutePath + "?ref=" + id);
            return;
        }

        Response.Redirect(Request.Url.AbsolutePath);
    }

    void InitReportList()
    {
        var id = queryparts.Get("ref");

        foreach (var entry in SolarWinds.Orion.Core.Reporting.DAL.SessionCache.Instance.Where(x => x.Config != null) )
        {
            var entryid = entry.CacheId.ToString();

            InMemoryReports.Items.Add(new ListItem
            {
                Text = entry.Config.Header.Title,
                Value = entryid,
                Selected = entryid == id
            });
        }
    }

    void InitViewList()
    {
        var id = queryparts.Get(null);
    
        foreach( var view in ViewManager.GetAllViews() )
        {
            InDatabaseViews.Items.Add(new ListItem
            {
                Text = view.ViewTitle,
                Value = view.ViewID.ToString()
            });
        }
    }

    public bool AnyCachedReports()
    {
        return SolarWinds.Orion.Core.Reporting.DAL.SessionCache.Instance.Where(x => true).Count() > 0;
    }
}