﻿<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true" CodeFile="Test.aspx.cs" Inherits="Orion_Reports_Test" %>

<asp:Content ContentPlaceHolderID="TopRightPageLinks" runat="server" Visible="true">
    <a href="?" class="sw-pg-exitpreview" id="ExitRender" runat="server" visible="false">Exit Render Preview</a>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<orion:InlineCss ID="InlineCss1" runat="server">.sw-pg-exitpreview { background: url('/Orion/images/button.closecross.gif') center left no-repeat; padding: 2px 0px 2px 20px;</orion:InlineCss>

<asp:PlaceHolder ID="AccessPanel" runat="server">
    <div style="margin: 10px; <%= AnyCachedReports() ? "" : "display: none;" %>">
        <h3>In-Memory Reports</h3>
        <div class='sw-btn-bar'>
            <asp:DropDownList runat="server" ID="InMemoryReports" EnableViewState="false" /> <orion:LocalizableButton ID="BtnReportTest" runat="server" LocalizedText="Test" OnClick="OnBtnReportTest" /> <orion:LocalizableButton ID="BtnReportEdit" runat="server" LocalizedText="Edit" OnClick="OnBtnReportEdit" /> <orion:LocalizableButton ID="BtnReportDelete" runat="server" LocalizedText="Delete" OnClick="OnBtnReportDelete" />
        </div>
    </div>
    <div style="margin: 10px;">
        <h3>Views</h3>
        <div class='sw-btn-bar'>
            <asp:DropDownList runat="server" ID="InDatabaseViews" EnableViewState="false" /> <orion:LocalizableButton ID="BtnViewImport" runat="server" LocalizedText="Import" OnClick="OnBtnViewImport" />
        </div>
    </div>
</asp:PlaceHolder>

<asp:PlaceHolder ID="EditPanel" runat="server" Visible="false">
    
    <div style="margin:16px;"><div>
        <div class='sw-btn-bar'><orion:LocalizableButton LocalizedText="Save" ID="EditBtn" OnClick="EditBtn_Click" runat="server" DisplayType="Primary" /></div>
        <textarea id="ConfigXml" style="width: 100%; height: 800px;" runat="server"></textarea>
    </div></div>
</asp:PlaceHolder>

<asp:PlaceHolder ID="TestPanel" runat="server" Visible="false" />


</asp:Content>
