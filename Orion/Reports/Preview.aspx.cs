﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Xml.Linq;

using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Reporting.DAL;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Reporting.Models;
using SolarWinds.Orion.Core.Reporting;

public partial class Orion_Reports_Preview : System.Web.UI.Page
{
	public Guid WizardGuid { set; get; }

    private const string QueryParamAjaxId = "waitid";

    private Guid _ajaxId;
    private string _reportId;
    private DateTime _start;
    private DateTime _end;
    private bool _trackTiming;

    protected void Page_Init(object sender, EventArgs e)
    {
        _reportId = Request["ReportID"];
        _start = DateTime.Now;
        _trackTiming = true;

        if (string.IsNullOrWhiteSpace(_reportId))
            throw new ArgumentException("ReportID query parameter is required.");

        if (TryPurgeWaitIdOnReturn())
            return;

        var fn = GetAjaxCmd();

        if (fn != null)
            fn();
    }

    protected void Page_Load(object sender, EventArgs e)
	{
        AngularJsHelper.EnableAngularJsForPageContent();
        if (!IsPostBack)
		{
			Guid reportWizardGuid = Guid.NewGuid();
			string defaultBackUrl = "/Orion/Reports/ViewReports.aspx";

			string navigateBackUrl = ReferrerRedirectorBase.GetDecodedReferrerUrlOrDefault(defaultBackUrl);
			string navigateBackTextPart = navigateBackUrl.Contains(defaultBackUrl) ? Resources.CoreWebContent.WEBDATA_SO0_55 : Resources.CoreWebContent.WEBDATA_SO0_52;

            Preview.ReportId = Convert.ToInt32(_reportId);
			Report report = ReportDAL.GetReportById(Preview.ReportId);
            if (report == null)
                throw new Exception(string.Format("Could not find report with id '{0}'.", Preview.ReportId));
			WizardGuid = reportWizardGuid;
			ReportStorage.Instance.Update(WizardGuid, report);
			Preview.CacheId = WizardGuid;

		    if( report.Header != null )
                Page.Title = DefaultSanitizer.SanitizeHtml(report.Header.Title).ToString();

            this.backLink.NavigateUrl = navigateBackUrl;
			this.backLink.Text = string.Format(Resources.CoreWebContent.WEBDATA_SO0_96, navigateBackTextPart);
		}
	}

    protected void OrionSiteMapPath_OnInit(object sender, EventArgs e)
	{
		if (!CommonWebHelper.IsBreadcrumbsDisabled)
		{
            var queryItems = HttpUtility.ParseQueryString(Request.Url.Query);
            queryItems.Remove(QueryParamAjaxId);

			string viewID = Request["ViewID"];
			string returnTo = Request["ReturnTo"];
			string accountID = Request["AccountID"];
			var renderer = new SolarWinds.Orion.NPM.Web.UI.AdminSiteMapRenderer();
            renderer.CurrentURL = "/Orion/Report.aspx?" + queryItems;
            renderer.SetUpData(new KeyValuePair<string, string>("ViewID", viewID), new KeyValuePair<string, string>("ReturnTo", returnTo), new KeyValuePair<string, string>("AccountID", accountID));
			OrionSiteMapPath.SetUpRenderer(renderer);
		}
	}

    protected override void Render(System.Web.UI.HtmlTextWriter writer)
    {
        base.Render(writer);

        _end = DateTime.Now;

        if (_trackTiming)
            SolarWinds.Orion.Core.Common.DALs.OrionReportDAL.UpdateReportRenderDuration( int.Parse(_reportId), (int) Math.Ceiling(_end.Subtract(_start).TotalSeconds) );
    }

    // ---

    private bool TryPurgeWaitIdOnReturn()
    {
        // are we returning directly from reporting? if so, we are not worried about a url captured with a waitid in it.

        var refUrl = Request.UrlReferrer == null ? string.Empty : (Request.UrlReferrer.PathAndQuery ?? string.Empty);

        if ( refUrl.StartsWith("/orion/reports/", StringComparison.OrdinalIgnoreCase) ||
            refUrl.StartsWith("/orion/report.aspx", StringComparison.OrdinalIgnoreCase) ||
            refUrl.StartsWith("/orion/reportwait.aspx", StringComparison.OrdinalIgnoreCase))
            return false;

        // otherwise
        
        var items = HttpUtility.ParseQueryString(Request.RawUrl);

        if (items[QueryParamAjaxId] == null)
            return false; // no wait id to remove.

        string gid = (Request.QueryString[QueryParamAjaxId] ?? string.Empty).ToLower();

        if (string.IsNullOrWhiteSpace(gid) || Request.UrlReferrer == null)
            return false;

        // let's just remove waitid, we'll force the report to re-render.

        items.Remove(QueryParamAjaxId);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Redirect("/Orion/Report.aspx?" + items);
        return true;
    }

    private Action GetAjaxCmd()
    {
        string gid = (Request.QueryString[QueryParamAjaxId] ?? string.Empty).ToLower();

        if (Guid.TryParse(gid, out _ajaxId) == false || _ajaxId == Guid.Empty)
            return null;

        return AjaxStart;
    }

    private void AjaxStart()
    {
        // did we just request to restart rendering a report in the background?
        // was that report already rendering?

        var d = TempFiles.GetDetails(_ajaxId);

        if (d.Exists)
        {
            AjaxStatus();
            return;
        }

        // no. let's start a new report... and create it within this thread.

        string json = "{ \"ready\": true }";
        var f = Response.Filter; // [pc] read before set or .net 3.5 goes kaboom.
        Response.Filter = TempFiles.GetFilterStream(Response.OutputStream, _ajaxId, json);
        Response.ContentType = "application/json; charset=utf-8";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
    }

    private void AjaxStatus()
    {
        // did we just request to restart rendering a report in the background?
        // was that report already rendering?

        var d = TempFiles.GetDetails(_ajaxId);
        _trackTiming = false;

        string json;

        int reportId = int.Parse(_reportId);

        if (d.Exists && d.IsInProgress == false)
        {
            // the report is done.

            json = "{ \"ready\": true }";
        }
        else
        {
            // no, it is not ready.
            // respond back with status and how long we think it will take.

            string remaining = string.Empty;

            int sofar = (int)Math.Ceiling(DateTime.Now.Subtract(d.Started).TotalSeconds);

            int? lastTime = SolarWinds.Orion.Web.DAL.ReportDAL.GetLastRenderTime(reportId);

            if (lastTime.HasValue && sofar < lastTime)
                remaining = string.Format(Resources.CoreWebContent.WEBCODE_PCC_52, lastTime.Value - sofar);

            else if (lastTime.HasValue)
                remaining = string.Format(Resources.CoreWebContent.WEBCODE_PCC_53, sofar - lastTime.Value);
            else
                remaining = string.Format(Resources.CoreWebContent.WEBCODE_PCC_54, sofar);

            json = string.Format("{{ \"ready\": false, \"remaining\": \"{0}\" }}", HttpUtility.JavaScriptStringEncode(remaining));
        }

        var bytes = Encoding.UTF8.GetBytes(json);
        Response.ContentType = "application/json; charset=UTF-8";
        Response.OutputStream.Write(bytes, 0, bytes.Length);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.End();
    }
}
