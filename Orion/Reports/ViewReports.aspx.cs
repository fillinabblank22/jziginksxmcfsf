﻿using System;
using System.Configuration;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Common.Swis;

public partial class Orion_Reports_ViewReports : System.Web.UI.Page
{
    internal bool _isOrionDemoMode;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            _isOrionDemoMode = true;
        }
    }
    
	protected string ReturnUrl
	{
		get { return ReferrerRedirectorBase.GetReturnUrl(); }
	}

	

	public int IsFederationEnabled
	{
		get
		{
			return SwisFederationInfo.IsFederationEnabled ? 1 : 0;
		}
	}
}