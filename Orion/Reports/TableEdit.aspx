<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/Reports/ReportsMasterPage.master" AutoEventWireup="true" CodeFile="TableEdit.aspx.cs" Inherits="Orion_Reports_TableEdit" %>
<%@ Register Src="~/Orion/Reports/Controls/ReportTableConfiguration.ascx" TagPrefix="orion" TagName="TableConfiguration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPlaceholder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopRightPageLinks" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceholder" Runat="Server">
    <style type="text/css">
        .temporaryControlPlaceholder {
            border: 1px solid black;
            padding: 10px;
            margin: 10px;
            width: 400px;
        }
        
    </style>

    <h1>Report Table</h1>
    

    <div class="temporaryControlPlaceholder">Data Source Editor</div>
    <div class="temporaryControlPlaceholder">Time Source Editor</div>
    
    <orion:TableConfiguration runat="server" ID="reportTableConfiguration" />
    
    <div class="sw-btn-bar">
        <orion:LocalizableButton ID="btnSubmit" runat="server" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitClick"/>
        <orion:LocalizableButton ID="btnPreview" runat="server" DisplayType="Secondary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_280 %>" OnClick="PreviewClick"/>
    </div>
    <asp:Panel runat="server" ID="PreviewPanel"></asp:Panel>
</asp:Content>

