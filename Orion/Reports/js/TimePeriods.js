﻿(function ($) {
    $.fn.timePeriods = function (method) {
        var methods = {
            init: function (options) {
                $(SW.Core.Widgets.Button('@{R=Core.Strings;K=WEBJS_PS0_1; E=js}', { type: 'primary', id: 'customizeSubmit' })).appendTo("#buttons").click(function () {
                    var radioBtn = $("#RelativePeriod");
                    var name = $("#PeriodName")[0].value;
                    if (name == "") {
                        $("#nameValidator").show();
                        return;
                    }
                    else {
                        $("#nameValidator").hide();
                    }

                    var guid = helpers.guidGenerator();

                    if (radioBtn[0].checked) {
                        var lastNumber = $("#LastNumber")[0].value;
                        if (isNaN(lastNumber) || !lastNumber.match(/^\d+$/)) {
                            $("#lastNumberValidator").show();
                            return;
                        }
                        else {
                            $("#lastNumberValidator").hide();
                        }
                        var periodType = $("#PeriodType")[0].value;
                        $.fn.timePeriods.timeFrameData = { RefId: guid, IsStatic: false, DisplayName: name, Relative: { NamedTimeFrame: 0, Unit: periodType, UnitCount: lastNumber }, Static: null };
                        $.fn.timePeriods.timeFrameDataArray.push($.fn.timePeriods.timeFrameData);
                    }
                    else {

                        try {
                            $("#customTimeValidator").hide();
                            $.fn.timePeriods.settings.fromDateValue = $('#spanFrom').orionGetDate();
                            $.fn.timePeriods.settings.endDateValue = $('#spanTo').orionGetDate();
                        }
                        catch (err) {
                            $("#customTimeValidator").show();
                            return;
                        }

                        if ($.fn.timePeriods.settings.fromDateValue > $.fn.timePeriods.settings.endDateValue) {
                            $("#greaterValidator").show();
                            return;
                        }
                        else
                            $("#greaterValidator").hide();

                        $.fn.timePeriods.timeFrameData = { RefId: guid, IsStatic: true, DisplayName: name, Relative: null, Static: { UserStart: "/Date(" + Date.parse($.fn.timePeriods.settings.fromDateValue) + ")/", UserEnd: "/Date(" + Date.parse($.fn.timePeriods.settings.endDateValue) + ")/"} };
                        $.fn.timePeriods.timeFrameDataArray.push($.fn.timePeriods.timeFrameData);
                    }
                    $.fn.timePeriods.timeFrameDataDictionary[$.fn.timePeriods.frameId] = $.fn.timePeriods.timeFrameData;
                    if ($.fn.timePeriods.syncTimeFrameIdDelegate) $.fn.timePeriods.syncTimeFrameIdDelegate($.fn.timePeriods.frameId, guid);
                    var currentFrameList = helpers.getCurrentFrameList($.fn.timePeriods.frameId);

                    var exists = false;
                    var records = $.fn.timePeriods.frameList.getStore().getRange();
                    for (var s = 0; s < records.length; s++) {
                        if (records[s].get('displayText') == name) {
                            exists = true;
                            if (records[s].get('value') != guid) {
                                records[s].set('value', guid);
                            }
                            break;
                        }
                    }

                    if (!exists) {
                        $.fn.timePeriods.frameList.getStore().add(new Ext.data.Record({ value: guid, displayText: name }));
                    }
                    if (currentFrameList && currentFrameList.setValue)
                        currentFrameList.setValue(guid);
                    helpers.syncConfigsData();
                    $("#customDialog").dialog('close');
                });

                $(SW.Core.Widgets.Button('@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}', { type: 'secondary', id: 'customizeCancel' })).appendTo("#buttons").click(function () { $("#customDialog").dialog('close'); });
                $("#customDialog").dialog({
                    width: 370, height: 340, modal: true, autoOpen: false,
                    open: function () {
                        //Workaround JQuery dialog issue - according to the jQuery UI ticket this will be fixed in version 1.9 by using the focusSelector option
                        $(this).find('input').attr("tabindex", "1");
                    },
                    overlay: { "background-color": "black", opacity: 0.4 }, title: '@{R=Core.Strings;K=WEBJS_SO0_81; E=js}', resizable: false
                });
                $.fn.timePeriods.timeFrameSessionIdentifier = $('[id*=timeFrameSessionIdentifier]')[0].value;
                $.fn.timePeriods.selectedTimeFrameSessionIdentifier = $('[id*=selectedTimeFrameSessionIdentifier]')[0].value;

                helpers.initTimeFrameDataStore();
                this.timePeriods.settings = $.extend({}, this.timePeriods.defaults, options);
                return this.each(function () {
                    var $element = $(this),
                        element = this;
                });
            },

            InitTimeFrames: function (timeRefId) {
                $.fn.timePeriods.frameList = new Ext.form.ComboBox({
                    id: $.fn.timePeriods.settings.controlId + '_List', triggerAction: 'all', lazyRender: false, width: 150, mode: 'local', editable: false, value: 1, allowBlank: false,
                    store: $.fn.timePeriods.timeFrames,
                    valueField: 'value', displayField: 'displayText', renderTo: $.fn.timePeriods.settings.controlId,
                    tpl: '<tpl for="."><div class="x-combo-list-item">{displayText:htmlEncode}</div></tpl>',
                    listeners: {
                        'select': function (record, index) {
                            $.fn.timePeriods.frameId = this.id.replace("_List", "").replace("timeFrame_", "");
                            if (index.data.value == "Custom") {
                                if ($("#RelativePeriod")[0].checked) {
                                    $("#txtFromDatePicker").attr("disabled", "disabled");
                                    $("#txtFromTimePicker").attr("disabled", "disabled");
                                    $("#txtToDatePicker").attr("disabled", "disabled");
                                    $("#txtToTimePicker").attr("disabled", "disabled");

                                    $("#LastNumber").removeAttr("disabled", "disabled");
                                    $("#PeriodType").removeAttr("disabled", "disabled");
                                }
                                else {
                                    $("#txtFromDatePicker").removeAttr("disabled");
                                    $("#txtFromTimePicker").removeAttr("disabled");
                                    $("#txtToDatePicker").removeAttr("disabled");
                                    $("#txtToTimePicker").removeAttr("disabled");

                                    $("#LastNumber").attr("disabled", "disabled");
                                    $("#PeriodType").attr("disabled", "disabled");
                                }
                                $("#customDialog").dialog("open");
                            }
                            else {
                                if (isNaN(index.data.value)) {
                                    for (var s = 0; s < $.fn.timePeriods.timeFrameDataArray.length; s++) {
                                        if ($.fn.timePeriods.timeFrameDataArray[s].RefId == index.data.value) {
                                            $.fn.timePeriods.timeFrameData = $.fn.timePeriods.timeFrameDataArray[s];
                                            break;
                                        }
                                    }
                                }
                                else {
                                    var guid = helpers.guidGenerator();
                                    $.fn.timePeriods.timeFrameData = { RefId: guid, IsStatic: false, DisplayName: index.data.displayText, Relative: { NamedTimeFrame: index.data.value, Unit: 0, UnitCount: 0 }, Static: null };
                                    $.fn.timePeriods.timeFrameDataArray.push($.fn.timePeriods.timeFrameData);
                                    helpers.syncConfigsData();
                                }
                                $.fn.timePeriods.timeFrameDataDictionary[$.fn.timePeriods.frameId] = $.fn.timePeriods.timeFrameData;
                                if ($.fn.timePeriods.syncTimeFrameIdDelegate) $.fn.timePeriods.syncTimeFrameIdDelegate($.fn.timePeriods.frameId, $.fn.timePeriods.timeFrameData.RefId);
                            }
                        }
                    }
                });

                $.fn.timePeriods.frames.push({ ID: $.fn.timePeriods.settings.controlId, FrameList: $.fn.timePeriods.frameList });
                helpers.initDateValues($.fn.timePeriods.frameList.value, $.fn.timePeriods.frameList.lastSelectionText, $.fn.timePeriods.settings.controlId.replace("timeFrame_", ""), timeRefId);
            },

            SetTimeFrame: function (data) {
                if (!data.RefId) return;
                $.fn.timePeriods.timeFrameData = data;

                var exists = false;
                var records = $.fn.timePeriods.frameList.getStore().getRange();
                for (var s = 0; s < records.length; s++) {
                    if (records[s].get('displayText') == data.DisplayName) {
                        exists = true;
                        if (data.Relative && data.Relative.NamedTimeFrame > 0) {
                            if (records[s].get('value') != data.Relative.NamedTimeFrame) {
                                records[s].set('value', data.Relative.NamedTimeFrame);
                            }
                        }
                        else {
                            if (records[s].get('value') != data.RefId) {
                                records[s].set('value', data.RefId);
                            }
                        }
                        break;
                    }
                }

                if (data.IsStatic) {
                    if (!exists) {
                        $.fn.timePeriods.frameList.getStore().add(new Ext.data.Record({ value: data.RefId, displayText: data.DisplayName }));
                    }
                    $.fn.timePeriods.timeFrameDataArray.push(data);
                    $.fn.timePeriods.frameList.setValue(data.RefId);
                }
                else {
                    if (data.Relative.NamedTimeFrame > 0) {
                        $.fn.timePeriods.frameList.setValue(data.Relative.NamedTimeFrame);
                    }
                    else if (data.Relative.Unit > 0) {
                        if (!exists) {
                            $.fn.timePeriods.frameList.getStore().add(new Ext.data.Record({ value: data.RefId, displayText: data.DisplayName }));
                        }
                        $.fn.timePeriods.frameList.setValue(data.RefId);
                    }
                }
            },

            GetTimeFrame: function (id) {
                return $.fn.timePeriods.timeFrameDataDictionary[id];
            }
        };

        var helpers = {
            initDateValues: function (value, text, id, timeRefId) {
                if (timeRefId != '00000000-0000-0000-0000-000000000000') {
                    for (var i = 0; i < $.fn.timePeriods.timeFrameDataArray.length; i++) {
                        if ($.fn.timePeriods.timeFrameDataArray[i].RefId == timeRefId) {
                            $.fn.timePeriods.timeFrameDataDictionary[id] = $.fn.timePeriods.timeFrameDataArray[i];
                            $.fn.timePeriods('SetTimeFrame', $.fn.timePeriods.timeFrameDataArray[i]);
                            break;
                        }
                    }
                }
                else {
                    if ($.fn.timePeriods.initialTimeFrameData == null) {
                        var guid = helpers.guidGenerator();
                        $.fn.timePeriods.initialTimeFrameData = { RefId: guid, IsStatic: false, DisplayName: text, Relative: { NamedTimeFrame: value, Unit: 0, UnitCount: 0 }, Static: null };
                        $.fn.timePeriods.timeFrameDataArray.push($.fn.timePeriods.initialTimeFrameData);
                        helpers.syncConfigsData();
                    }
                    $.fn.timePeriods.timeFrameData = $.fn.timePeriods.initialTimeFrameData;
                    $.fn.timePeriods.timeFrameDataDictionary[id] = $.fn.timePeriods.timeFrameData;
                    if ($.fn.timePeriods.syncTimeFrameIdDelegate) $.fn.timePeriods.syncTimeFrameIdDelegate(id, $.fn.timePeriods.initialTimeFrameData.RefId);
                }
            },
            initTimeFrameDataStore: function () {
                SW.Core.Services.callWebServiceSync('/Orion/Services/ReportManager.asmx', 'GetSessionData', { name: $.fn.timePeriods.timeFrameSessionIdentifier },
                    function (res) {
                        if (res != null) {
                            var data = JSON.parse(res);
                            if (data != null && data != undefined) {
                                for (var i = 0; i < data.length; i++) {
                                    $.fn.timePeriods.timeFrameDataArray.push(data[i]);
                                }
                            }
                        }
                    });
            },
            toUtcFormat: function (d) {
                var date = new Date(d);
                return [date.getUTCFullYear(),
                    helpers.pad(date.getUTCMonth() + 1),
                    helpers.pad(date.getUTCDate())].join("-") + "T" +
                        [helpers.pad(date.getUTCHours()),
                            helpers.pad(date.getUTCMinutes()),
                            helpers.pad(date.getUTCSeconds())].join(":");
            },
            pad: function (num) {
                return ("0" + num).slice(-2);
            },
            guidGenerator: function () {
                return SW.Core.Services.generateNewGuid();
            },

            getCurrentFrameList: function (id) {
                var listId = 'timeFrame_' + id;
                for (var s = 0; s < $.fn.timePeriods.frames.length; s++) {
                    if ($.fn.timePeriods.frames[s].ID == listId) {
                        return $.fn.timePeriods.frames[s].FrameList;
                    }
                }
                return null;
            },
            syncConfigsData: function () {
                SW.Core.Services.callWebServiceSync('/Orion/Services/ReportManager.asmx', 'SetSessionData', { name: $.fn.timePeriods.selectedTimeFrameSessionIdentifier, value: Ext.util.JSON.encode($.fn.timePeriods.timeFrameData) }, function () { });
                SW.Core.Services.callWebServiceSync('/Orion/Services/ReportManager.asmx', 'SetSessionData', { name: $.fn.timePeriods.timeFrameSessionIdentifier, value: Ext.util.JSON.encode($.fn.timePeriods.timeFrameDataArray) }, function () { });
            },
            dateTimeStringToDate: function (dateString, timeString, settings) {
                var datepart = new Date(Date.parse(dateString, settings.dateFormat));
                var hours = 0;
                var minutes = 0;

                if (timeString) {
                    var array = timeString.split(settings.timeSeparator);
                    hours = parseFloat(array[0]);
                    minutes = parseFloat(array[1]);
                    if (!settings.show24Hours) {
                        if (hours == 12)
                            hours = 0;
                        if (timeString.indexOf('PM') > -1)
                            hours += 12;
                    }
                }

                datepart.setHours(hours);
                datepart.setMinutes(minutes);
                return datepart;
            }
        };

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method "' + method + '" does not exist in timePeriods plugin!');
            return null;
        }
    };

    $.fn.timePeriods.settings = {
        controlId: 'body',
        fromDateValue: '',
        endDateValue: ''
    };

    $.fn.timePeriods.timeFrameSessionIdentifier = null;
    $.fn.timePeriods.selectedTimeFrameSessionIdentifier = null;
    $.fn.timePeriods.regionalSettings = null;
    $.fn.timePeriods.syncTimeFrameIdDelegate = null;
    $.fn.timePeriods.timeFrameDataDictionary = {};
    $.fn.timePeriods.timeFrameDataArray = new Array();
    $.fn.timePeriods.initialTimeFrameData = null;
    $.fn.timePeriods.frames = new Array(); ;
    $.fn.timePeriods.timeFrameData = null;
    $.fn.timePeriods.frameList = null;
    $.fn.timePeriods.frameId = null;
    $.fn.timePeriods.timeFrames = new Ext.data.ArrayStore({ fields: ['value', 'displayText'], data: [
        ['1', '@{R=Core.Strings;K=Reports_YK0_4;E=js}'],
        ['2', '@{R=Core.Strings;K=Reports_YK0_5;E=js}'],
        ['3', '@{R=Core.Strings;K=Reports_YK0_6;E=js}'],
        ['4', '@{R=Core.Strings;K=Reports_YK0_7;E=js}'],
        ['5', '@{R=Core.Strings;K=Reports_YK0_8;E=js}'],
        ['6', '@{R=Core.Strings;K=Reports_YK0_9;E=js}'],
        ['7', '@{R=Core.Strings;K=Reports_YK0_10;E=js}'],
        ['8', '@{R=Core.Strings;K=Reports_IB0_1;E=js}'],
        ['9', '@{R=Core.Strings;K=Reports_YK0_11;E=js}'],
        ['10', '@{R=Core.Strings;K=Reports_YK0_12;E=js}'],
        ['11', '@{R=Core.Strings;K=Reports_YK0_13;E=js}'],
        ['12', '@{R=Core.Strings;K=Reports_YK0_14;E=js}'],
        ['13', '@{R=Core.Strings;K=Reports_YK0_15;E=js}'],
        ['14', '@{R=Core.Strings;K=Reports_YK0_16;E=js}'],
        ['Custom', '@{R=Core.Strings;K=Reports_YK0_3;E=js}']]
    });
})(jQuery);