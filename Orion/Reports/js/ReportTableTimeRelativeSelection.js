﻿SW.Core.namespace('SW.Core.TableResource');
/* Object is responsible for managing UI of control TimeFieldColumnSelection which serve for ReportTable
 * @param {Object} config
 * config.selectionElementId {String} id of the ASP.NET control by which is this control represented
 * config.hiddenElementId {String} id of element maintains state of dropdown so if it is disabled we still get the value posted to the server.
*/

SW.Core.TableResource.TimeRelativeSelection = function (config) {
    var self = this;
    var ddlTimeRelative = $('#' + config.selectionElementId);
    var hiddenTimeRelative = $('#' + config.hiddenElementId); 
    var timeRelativeConfig = $('.timeRelativeConfig');

    this.ShowDetailsBasedOnSelection = function(e) {
        var isRelative = ddlTimeRelative.val() === 'true';
        hiddenTimeRelative.val(isRelative);
        if (isRelative) {
            timeRelativeConfig.fadeIn();
        } else {
            timeRelativeConfig.fadeOut();
        }
        SW.Core.TableResource.EventPublisher.TriggerTimeRelativeSelectionChanged({ isTimeRelative: isRelative });
    };
    
    this.Init = function () {
        ddlTimeRelative.change(self.ShowDetailsBasedOnSelection);
        self.ShowDetailsBasedOnSelection();
    };

    
};