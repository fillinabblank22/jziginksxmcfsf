Ext.namespace('SW');
Ext.namespace('SW.Orion');
if (Ext.isIE)
    delete Ext.Tip.prototype.minWidth;
Ext.QuickTips.init();

SW.Core.namespace("SW.Orion").ReportManager = function (configuration) {
    var comboArray;
    var pageSizeBox;
    var userPageSize;
    var selectedColumns;
    var selectorModel;
    var sortOrder;
    var groupingValue;
    var dataStore;
    var grid;
    var groupGrid = null;
    var grpGridFirstLoad;
    var comboboxMaxWidth = 500;
    var gridToolBar = [];
    var isManagerGrid;
    var isSelectAllMode = false;
    var tempSelectedItemsArray;
    var customPropertyNames = [];
    var fileUploadControlID;
    var fileUploadControlInitialized = false;
    var importReportController;

    var remoteReportsMode;
    var remoteOrionID;
    var remoteOrionName;

    var swisBasedReportsOnly;

    var guid = SW.Core.Services.generateNewGuid();

    var mainGridID = "mainGrid-" + guid;
    var navPanelID = "navPanel-" + guid;
    var groupByTopPanelID = "groupByTopPanel-" + guid;
    var groupComboID = "mrGroupCombo-" + guid;
    var groupingGridID = "groupingGrid-" + guid;
    var gridID = "rmGrid-" + guid;

    var hash = {
        '.xml': 1,
        '.report': 2
    };

    var updateToolbarButtons = function () {
        var map = grid.getTopToolbar().items.map;
        var selCount;
        if (isManagerGrid) {
            selCount = getSelectedItemsArray().length;
            var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');

            if (grid.getStore().getCount() > 0 && grid.getSelectionModel().getCount() == grid.getStore().getCount()) {
                hd.addClass('x-grid3-hd-checker-on');
            } else {
                hd.removeClass('x-grid3-hd-checker-on');
            }

            if (!allowReport) {
                map.Delete.setDisabled(true);
                map.Add.setDisabled(true);
                map.DublicateEdit.setDisabled(true);
            } else {
                map.Delete.setDisabled(selCount == 0);
                map.DublicateEdit.setDisabled(selCount != 1);
            }

            map.Edit.setDisabled(selCount != 1 || !allowReport);
            map.ExportImport.menu.items.map.ExportReport.setDisabled(selCount != 1);
            map.ScheduleReport.menu.items.map.AssignExistingSchedule.setDisabled(selCount == 0 || !allowReport);
            map.ScheduleReport.menu.items.map.UnassignSchedule.setDisabled(selCount != 1 || !allowReport || JSON.parse(getSelectedItemsArray()[0].data.JobsDisplayName) == null || JSON.parse(getSelectedItemsArray()[0].data.JobsDisplayName).length == 0);
            map.View.setDisabled(selCount != 1);

        } else if (!remoteReportsMode) {
            selCount = getSelectedItemsArray().length;
            map.View.setDisabled(selCount != 1);
        }
    };

    function selectAllTooltip(grid, pageSizeBox, webStore) {
        if (grid.store.data.items.length == grid.getSelectionModel().selections.items.length && grid.store.data.items.length != 0 && grid.store.data.items.length != pageSizeBox.ownerCt.dsLoaded[0].totalLength) {
            var text = String.format("@{R=Core.Strings;K=WEBJS_AB0_46;E=js}", grid.getSelectionModel().selections.items.length);

            var selectionTipLocation = "#" + configuration.ContainerID + " .x-panel-tbar.x-panel-tbar-noheader";
            $('<div id="selectionTip" style="padding-left: 5px; background-color:#FFFFCC;">' + text + ' </div>').insertAfter(selectionTipLocation);

            $("#selectionTip").append($("<span id='selectAllLink' style='text-decoration: underline;color:red;cursor:pointer;'>" + String.format("@{R=Core.Strings;K=WEBJS_AB0_45;E=js}", pageSizeBox.ownerCt.dsLoaded[0].totalLength) + "</span>").click(function () {
                var allGridElementsStore = webStore();
                allGridElementsStore.proxy.conn.jsonData = grid.store.proxy.conn.jsonData;
                allGridElementsStore.load({
                    callback: function () {
                        isSelectAllMode = true;
                        tempSelectedItemsArray = allGridElementsStore.data.items;
                        $("#selectionTip").text("@{R=Core.Strings;K=WEBJS_AB0_47;E=js}").css("background-color", "white");
                        $("#selectAllLink").remove();
                        updateToolbarButtons();

                        for (var index = 0; index < allGridElementsStore.data.items.length; index++) {
                            invokeOnSelectionChanged(allGridElementsStore.data.items[index].data, true);
                        }
                    }
                });
            }));
            var gridHeight = $("#selectAllLink").closest('.x-panel-body.x-panel-body-noheader').height();
            $("#selectAllLink").closest('.x-panel-body.x-panel-body-noheader').height(gridHeight + $("#selectionTip").outerHeight());
            $(".x-layout-split.x-layout-split-west.x-splitbar-h").css("z-index", "0").height(function (index, height) {
                return (height + $("#selectionTip").outerHeight());
            });
        } else {
            clearSelectAllTooltip();
        }
    }
    function clearSelectAllTooltip() {
        if ($("#selectionTip").length != 0) {
            var rowsContainerHeight = $("#selectAllLink").closest('.x-panel-body.x-panel-body-noheader').height();
            $("#selectAllLink").closest('.x-panel-body.x-panel-body-noheader').height(rowsContainerHeight - $("#selectionTip").outerHeight());
            $("#selectionTip").remove();
            $(".x-layout-split.x-layout-split-west.x-splitbar-h").css("z-index", "1").height(function (index, height) {
                return (height - $("#selectionTip").outerHeight());
            });
            if (isSelectAllMode) {
                isSelectAllMode = false;
            }
        }
    }

    function getSelectedItemsArray() {
        return isSelectAllMode ? tempSelectedItemsArray : grid.getSelectionModel().getSelections();
    }

    var isLegacyReportsSelected = function (items) {
        for (var y = 0; y < items.length; y++) {
            if (items[y].data.LegacyPath)
                return true;
        }
        return false;
    };

    var showWarningPopup = function (title, message) {
        Ext.Msg.show({
            title: title,
            cls: 'warn-popup-message',
            msg: message,
            width: 500,
            buttons: { yes: '@{R=Core.Strings;K=CommonButtonType_Ok; E=js}' },
            icon: Ext.Msg.WARNING
        });
    };

    var getSelectedReportsIds = function (items, encode) {
        var ids = [];

        Ext.each(items, function (item) {
            if (encode) {
                ids.push(encodeURIComponent(item.data.ReportID));
            }
            else {
                ids.push(item.data.ReportID);
            }
        });
        return ids;
    };

    function checkForDraft(btnContinueText, defaultAction) {
        if (reportDraftExists()) {
            $("#draftDialog").dialog({
                resizable: false,
                width: 500,
                modal: true
            });
            $('#' + getBtnContinueID() + " .sw-btn-t").text(btnContinueText);
            $('#' + getBtnContinueID()).click(function () { defaultAction(); });
        } else {
            defaultAction();
        }
    }

    function addReport() {
        if ($("#isOrionDemoMode").length != 0) {
            demoAction("Core_Reporting_AddReport", this);
            return;
        }
        checkForDraft("@{R=Core.Strings;K=WEBJS_VL0_26;E=js}", function () {
            var guid = SW.Core.Services.generateNewGuid();
            window.location = String.format("/Orion/Reports/Add/Default.aspx?ReportWizardGuid={0}", guid);
        });
    }

    function getReportLink(record) {
        var reportLink;
        if (!record.data.LegacyPath) {
            reportLink = "/Orion/Report.aspx?ReportID=" + record.data.ReportID;
        } else {
            reportLink = "/Orion/Report.aspx?Report=" + encodeURIComponent(record.data.Name);
        }
        return reportLink + "&ReturnTo=" + configuration.ReturnToUrl;
    }

    function viewReport() {
        var record = grid.getSelectionModel().getSelected();
        window.location = getReportLink(record);
    }

    function editReport() {
        if (grid.getSelectionModel().getSelected().data.LegacyPath) {
            showWarningPopup('@{R=Core.Strings;K=WEBJS_TM0_128;E=js}', String.format('@{R=Core.Strings;K=WEBJS_IB0_36;E=js}', String.format('<a href="{0}" target="_blank" rel="noopener noreferrer">', SW.Core.KnowledgebaseHelper.getKBUrl(4603, '@{R=Core.Strings;K=CurrentHelpLanguage;E=js}')), '</a>'));
            return;
        }

        if ($("#isOrionDemoMode").length != 0) {
            redirectToEdit(); // drafts shouldn't be in demo mode
            return;
        } else {
            checkForDraft("@{R=Core.Strings;K=WEBJS_VL0_27;E=js}", redirectToEdit);
        }
    }

    function redirectToEdit() {
        var guid = SW.Core.Services.generateNewGuid();
        var editParams = {
            ReportID: encodeURIComponent(grid.getSelectionModel().getSelected().data.ReportID),
            ReportUri: grid.getSelectionModel().getSelected().data.Uri
        };
        SW.Core.Services.postToTarget(String.format("/Orion/Reports/Add/Default.aspx?ReportWizardGuid={0}", guid), editParams);
    }

    function dublicateEditReport() {
        if ($("#isOrionDemoMode").length != 0) {
            demoAction("Core_Reporting_DuplicateReport", this);
            return;
        }
        if (grid.getSelectionModel().getSelected().data.LegacyPath) {
            showWarningPopup('@{R=Core.Strings;K=WEBJS_TM0_129;E=js}', String.format('@{R=Core.Strings;K=WEBJS_IB0_36;E=js}', String.format('<a href="{0}" target="_blank" rel="noopener noreferrer">', SW.Core.KnowledgebaseHelper.getKBUrl(4603, '@{R=Core.Strings;K=CurrentHelpLanguage;E=js}')), '</a>'));
            return;
        }
        //add two popups
        var guid = SW.Core.Services.generateNewGuid();
        ORION.callWebService("/Orion/Services/ReportManager.asmx",
            "DuplicateReport", { reportId: grid.getSelectionModel().getSelected().data.ReportID },
            function (result) {
                var editParams = { ReportID: encodeURIComponent(result) };
                SW.Core.Services.postToTarget(String.format("/Orion/Reports/Add/Default.aspx?ReportWizardGuid={0}", guid), editParams);
            });
    }

    var deleteSelectedItems = function (items, onSuccess) {
        var toDelete = getSelectedReportsIds(items, false);

        var waitMsg = Ext.Msg.wait("@{R=Core.Strings;K=WEBJS_TM0_126;E=js}");

        deleteReports(toDelete, function (result) {
            waitMsg.hide();
            ErrorHandler(result);
            onSuccess(result);
        });
    };

    var deleteReports = function (ids, onSuccess) {
        ORION.callWebService("/Orion/Services/ReportManager.asmx",
            "RemoveReports", { ids: ids },
            function (result) {
                onSuccess(result);
            });
    };

    // Encoding value function
    function encodeHTML(value) {
        return Ext.util.Format.htmlEncode(value);
    };

    function stringToBoolean(value) {
        switch (value.toString().toLowerCase()) {
            case "true": case "yes": case "1": return true;
            case "false": case "no": case "0": case null: return false;
            default: return Boolean(value);
        }
    }

    function renderBool(value) {
        if (!value || value.length === 0 || value.toString() == '0' || value.toString() == 'false')
            return '@{R=Core.Strings;K=WEBJS_IB0_13;E=js}';
        return '@{R=Core.Strings;K=WEBJS_IB0_12;E=js}';
    }

    function renderFloat(value) {
        if (Ext.isEmpty(value) || value.length === 0)
            return value;
        return renderString(String(value).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator));
    }

    var reMsAjax = /^\/Date\((d|-|.*)\)\/$/;
    function renderDateTime(value) {
        return doRenderDateTime(value, true);
    }
    function renderDate(value) {
        return doRenderDateTime(value, false);
    }
    function doRenderDateTime(value, renderTime) {
        if (Ext.isEmpty(value) || value.length === 0)
            return '';

        var a = reMsAjax.exec(value);
        if (a) {
            var b = a[1].split(/[-,.]/);
            var val = new Date(+b[0]);

            var output1 = val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern);
            if (renderTime) {
                output1 += ' ' + val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern);
            }

            return output1;
        }

        var dateVal = new Date(value);
        if (dateVal && dateVal != 'Invalid Date') {
            var output2 = dateVal.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern);
            if (renderTime) {
                output2 += ' ' + dateVal.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern);
            }
            return output2;
        }

        return '';
    }

    function DeprecatedReportInfo() {
        return String.format('<a href="{0}" target="_blank" rel="noopener noreferrer">&nbsp(@{R=Core.Strings;K=WEBJS_RV0_1;E=js})</a>', SW.Core.HelpDocumentationHelper.getHelpLink('orioncorereportmigrationtool'));
    }

    function renderString(value) {
        if (!value || value.length === 0)
            return value;

        if (value == 'ReportWriter')
            return value + DeprecatedReportInfo();

        if (!filterText || filterText.length === 0) {
            return encodeHTML(value);
        }

        var patternText = filterText;

        // replace any %'s with a *
        patternText = patternText.replace(/\%/g, "*");

        // replace multiple *'s with a single instance
        patternText = patternText.replace(/\*{2,}/g, "*");

        // check if the search string is now just down to a single *, and if so return without any further regex
        if (patternText == '*') { return '<span style=\"background-color: #FFE89E\">' + encodeHTML(value) + '</span>'; }

        // replace \ with \\
        patternText = patternText.replace(/\\/g, '\\\\');
        // replace . with \.
        patternText = patternText.replace(/\./g, '\\.');
        // replace * with .*
        patternText = patternText.replace(/\*/g, '.*');

        // set regex pattern
        var x = '((' + escapeRegExp(patternText) + ')+)\*';
        var content = value.toString(), pattern = new RegExp(x, "gi"), replaceWith = '{SPAN-START-MARKER}$1{SPAN-END-MARKER}';

        // do regex replace + content gets encoded
        var fieldValue = encodeHTML(content.replace(pattern, replaceWith));

        // now replace the literal SPAN markers with the HTML span tags
        fieldValue = fieldValue.replace(/{SPAN-START-MARKER}/g, '<span style=\"background-color: #FFE89E\">');
        fieldValue = fieldValue.replace(/{SPAN-END-MARKER}/g, '</span>');

        return fieldValue;
    };

    function escapeRegExp(str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }

    function renderFavorite(value) {
        if (stringToBoolean(value)) {
            return ('<a href="#"><img src="/Orion/images/Reports/Star-full.png"/></a>');
        } else {
            return ('<a href="#"><img src="/Orion/images/Reports/Star-empty.png"/></a>');
        }
    }

    function renderReportCategory(value) {
        if ((!value && value != false) || value == '') {
            value = "@{R=Core.Strings;K=Period_custom;E=js}";
        }
        if (value == "[All]") {
            value = "@{R=Core.Strings;K=WEBJS_SO0_31; E=js}";
        }
        return value;
    }

    function renderCategory(value) {
        if (value == "[All]") {
            return "@{R=Core.Strings;K=WEBJS_SO0_31; E=js}";
        }
        if (value == "Default Folder") {
            return "@{R=Core.Strings;K=WEBJS_SO0_55; E=js}";
        }
        return value ? value : "@{R=Core.Strings;K=WEBJS_RB0_3; E=js}";
    }

    function renderLimitationCategory(value) {
        if (value == "Default Folder") {
            return "@{R=Core.Strings;K=WEBJS_SO0_55; E=js}";
        }
        if (!value) {
            return "@{R=Core.Strings;K=WEBJS_SO0_56; E=js}";
        }

        return renderString(value);
    }

    function renderYesNo(value) {
        if (value == "[All]") {
            return "@{R=Core.Strings;K=WEBJS_SO0_31; E=js}";
        }
        return value ? '@{R=Core.Strings;K=WEBJS_AK0_28;E=js}' : '@{R=Core.Strings;K=WEBJS_AK0_27;E=js}';
    }

    function renderFavoriteText(value) {
        if (value == "[All]") {
            return "@{R=Core.Strings;K=WEBJS_SO0_31; E=js}";
        }
        return value ? '@{R=Core.Strings;K=WEBJS_SO0_72;E=js}' : '@{R=Core.Strings;K=WEBJS_SO0_73;E=js}';
    }

    function renderLegacyText(value) {
        if (value == "[All]") {
            return "@{R=Core.Strings;K=WEBJS_SO0_31; E=js}";
        }
        return value ? '@{R=Core.Strings;K=WEBJS_SO0_76;E=js}' : '@{R=Core.Strings;K=WEBJS_SO0_77;E=js}';
    }

    function renderProduct(value) {
        if ((!value && value != false) || value == '') {
            value = "@{R=Core.Strings;K=Period_custom;E=js}";
        }

        if (value == "[All]") {
            value = "@{R=Core.Strings;K=WEBJS_SO0_31; E=js}";
        }
        return value;
    }

    function renderGroup(value, meta, record) {
        var disp;
        if (value == '{empty}')
            value = '';

        if (comboArray.getValue().indexOf('legacy') > -1) {
            disp = renderLegacyText(value) + " (" + record.data.Cnt + ")";
        } else if (comboArray.getValue().indexOf('LimitationCategory') > -1) {
            disp = renderCategory(value) + " (" + record.data.Cnt + ")";
        } else if (comboArray.getValue().indexOf('favorites') > -1) {
            disp = renderFavoriteText(value) + " (" + record.data.Cnt + ")";
        } else if (comboArray.getValue().indexOf('ModuleTitle') > -1) {
            disp = renderProduct(value) + " (" + record.data.Cnt + ")";
        } else if (comboArray.getValue().indexOf('Category') > -1) {
            disp = renderReportCategory(value) + " (" + record.data.Cnt + ")";
        } else {
            if (!value && value != false || value == '') {
                value = "@{R=Core.Strings;K=StatusDesc_Unknown;E=js}";
            }
            if (value == "[All]") {
                value = "@{R=Core.Strings;K=WEBJS_SO0_31; E=js}";
            }
            disp = value + " (" + record.data.Cnt + ")";
        }
        return encodeHTML(disp);
    }

    function renderSchedulesAssigned(value) {
        if (value) {
            var jobData = JSON.parse(value);
            var resultedString = "";
            var coma = "";
            $.each(jobData, function () {
                var description = "";
                if (this.JobDescription.toString() != "")
                    description = encodeHTML(encodeHTML(this.JobDescription.toString())) + " </br> ";
                var tooltipText = description + String.format("@{R=Core.Strings;K=FILEDATA_AB0_9;E=js}", encodeHTML(encodeHTML(this.ActionsDisplayName.toString())), encodeHTML(encodeHTML(this.SchedulesDisplayName.toString())));

                resultedString += String.format('{0}<a href="#" onclick="reportManager.redirectToEditSchedule({1}); return false;"><span ext:qtitle="" ext:qtip="{2}">{3}</span></a>',
                    coma, this.JobId, tooltipText, renderString(this.JobDisplayName));

                coma = "@{R=Core.Strings;K=AvailabilityService_Member_names_delimiter; E=js}";
            });
            return String.format('<span class="scheduleColumn" >{0}</span>', resultedString);
        } else {
            return "";
        }
    }

    function renderSchedulesCountTooltip() {
        $.each($(".scheduleColumn"), function () {
            var reportsCount = $(this).find("a").length;
            var charsLength = reportsCount.toString().split('').length * 10 + 16;
            if ($(this).parent().parent().find(".reportsCountTooltip").length > 0)
                $(this).parent().parent().find(".reportsCountTooltip").remove();
            $(this).parent().css("width", ($(this).parent().parent().width() - charsLength) + "px").css("float", "left");
            
            if ($(this).width() > ($(this).parent().parent().width() - charsLength)) {
                $(this).parent().parent().append(String.format('<div class="reportsCountTooltip" style="padding:2px 0;float:right">({0})</div>', reportsCount));
            }
        });
    }
    function renderEditName(value, metadata, record) {
        if (remoteReportsMode) {
            return String.format('<div>{0}</div>', renderString(value));
        } else {
            return String.format('<a href="{1}">{0}</a>', renderString(value), getReportLink(record));
        }
    }

    function refreshGridObjects(start, limit, elem, property, type, value, search, callback) {
        if (elem == null) return;

        elem.store.removeAll();
        var tmpfilterText = search.replace(/\*/g, "%");
        if (tmpfilterText.length > 0) {
            if (!tmpfilterText.match("^%"))
                tmpfilterText = "%" + tmpfilterText;
            if (!tmpfilterText.match("%$"))
                tmpfilterText = tmpfilterText + "%";
        }

        elem.store.proxy.conn.jsonData =
        {
            serverID: remoteOrionID,
            property: property,
            type: type,
            value: value || "",
            search: tmpfilterText,
            swisBasedReportsOnly: swisBasedReportsOnly,
            resourceFilter: ""
        };

        if (limit)
            elem.store.load({ params: { start: start, limit: limit }, callback: callback });
        else
            elem.store.load({ callback: callback });
    };

    //Error handler
    function ErrorHandler(result) {
        if (result != null && result.Error) {
            Ext.Msg.show({ title: result.Source, msg: result.Msg, minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
        }
    }

    function setMainGridHeight(pageSize) {
        window.setTimeout(function () {
            if (typeof configuration.FixedHeight == "undefined") {
                var mainGrid = Ext.getCmp(mainGridID);
                var maxHeight = calculateMaxGridHeight(mainGrid);
                var calculated = calculateGridHeight(pageSize);
                var height = (calculated > 0) ? Math.min(calculated, maxHeight) : maxHeight;
                setHeightForGrids(height);

                //we need this to add height if horizontal scroll bar is present, with low window width and in chrome
                if (grid.el.child(".x-grid3-scroller").dom.scrollWidth != grid.el.child(".x-grid3-scroller").dom.clientWidth) {
                    height = Math.min(height + 20, maxHeight);
                    setHeightForGrids(height);
                }
            }
            else {
                setHeightForGrids(configuration.FixedHeight);
            }
        }, 0);
    }

    function setHeightForGrids(height) {
        var mainGrid = Ext.getCmp(mainGridID);
        mainGrid.setHeight(Math.max(height, 300));

        var groupingGrid = Ext.getCmp(groupingGridID);
        if (typeof groupingGrid != "undefined" && groupingGrid != null) {
            groupingGrid.setHeight(Math.max(height - 50, 250)); //height without groupByTopPanel
        }

        mainGrid.doLayout();
    }

    function calculateGridHeight(numberOfRows) {
        var rowHeight = grid.store.getCount() == 0 ? 21 :
            Ext.fly(grid.getView().getRow(0)).getHeight();

        var rowsHeight = rowHeight * (numberOfRows + 1);
        return grid.getHeight() - grid.getInnerHeight() + rowsHeight + 7;
    }

    function calculateMaxGridHeight(gridPanel) {
        var gridTop = gridPanel.getPosition()[1];
        return $(window).height() - gridTop - $('#footer').height() - 25;
    }

    function getReportRowNumber(rId, prop, type, value, succeeded) {
        if (!rId) {
            succeeded({ ind: -1 });
            return;
        }

        var sort = "";
        var dir = "";

        if (sortOrder != null && sortOrder[0] != 'Favorite Desc') {
            sort = sortOrder[0];
            dir = sortOrder[1];
        }

        SW.Core.Services.callWebServiceSync('/Orion/Services/ReportManager.asmx', 'GetReportNumber', { reportId: rId, property: prop, type: type, value: value, sort: sort, direction: dir, resourceFilter: "", swisBasedReportsOnly: swisBasedReportsOnly },
            function (result) {
                if (succeeded != null) {
                    succeeded({ ind: result });
                    return;
                }
                // not found
                succeeded({ ind: -1 });
                return;
            });
    }

    function selectCreatedReport(res) {
        var startFrom = 0;
        var objectIndex = res.ind;

        if (objectIndex > -1) {
            var pageNumFl = objectIndex / userPageSize;
            var index = parseInt(pageNumFl.toString());
            startFrom = index * userPageSize;
        }
        if (isManagerGrid) {
            refreshGridObjects(startFrom, userPageSize, grid, groupingValue[0], groupingValue[2], groupingValue[1], filterText, function () {
                if (objectIndex > -1) {
                    grid.getView().focusRow(objectIndex % userPageSize);
                    var rowEl = grid.getView().getRow(objectIndex % userPageSize);
                    rowEl.scrollIntoView(grid.getGridEl().id, false);
                    grid.getSelectionModel().selectRow(objectIndex % userPageSize);
                }
            });
        } else {
            refreshGridObjects(0, userPageSize, grid, groupingValue[0], groupingValue[2], groupingValue[1], filterText, function () { });
        }
    }

    function resizeToFitContent(combo) {
        if (!combo.elMetrics) {
            combo.elMetrics = Ext.util.TextMetrics.createInstance(combo.getEl());
        }
        var m = combo.elMetrics, width = 0, el = combo.el, s = combo.getSize();
        combo.store.each(function (r) {
            var text = r.get(combo.displayField);
            width = Math.max(width, m.getWidth(text));
        }, combo);
        if (el) {
            width += el.getBorderWidth('lr');
            width += el.getPadding('lr');
        }
        if (combo.trigger) {
            width += combo.trigger.getWidth();
        }
        s.width = width + 10;
        if (combo.getWidth() < s.width) {
            combo.setSize(s);
            var navPanel = Ext.getCmp(navPanelID);
            if (navPanel && s.width <= comboboxMaxWidth) {
                navPanel.setWidth(s.width + 50);
            }
        }
    }

    function invokeOnSelectionChanged(recordData, added) {
        if (typeof configuration.OnSelectionChanged == 'undefined') return;
        configuration.OnSelectionChanged(recordData, added, remoteOrionID, remoteOrionName);
    }
    function invokeOnReportsLoading() {
        if (typeof configuration.OnReportsLoading == 'undefined') return;
        configuration.OnReportsLoading();
    }
    function invokeOnReportsLoaded() {
        if (typeof configuration.OnReportsLoaded == 'undefined') return;
        configuration.OnReportsLoaded();
    }

    //Ext grid
    ORION.prefix = typeof configuration.Prefix == "undefined" ?
        "ReportManager_" : configuration.Prefix;

    var allowReport = false;
    var selectReport;
    var scheduleUnassignDialog;

    return {
        redirectToEditSchedule: function (jobId) {
            var guid = SW.Core.Services.generateNewGuid();
            window.location = String.format("/Orion/Reports/ScheduleAdd/Default.aspx?ScheduleWizardGuid={0}&ReportJobId={1}", guid, jobId);
        },
        scheduleSelected: function (dictionary) {
            var schedulesIds = [];
            $.each(dictionary, function () {
                schedulesIds.push(this.Key);
            });
            var rIds = getSelectedReportsIds(getSelectedItemsArray());
            ORION.callWebService('/Orion/Services/ReportScheduler.asmx',
                'AssignSchedules', { reportIds: rIds, schedulesIds: schedulesIds },
                function (result) {
                    filterText = "";
                    if (result) {
                        if (rIds.length > 1) {
                            refreshGridObjects(0, userPageSize, grid, groupingValue[0], groupingValue[2], groupingValue[1], filterText, function () { });
                        } else {
                            getReportRowNumber(rIds[0], groupingValue[0], "", groupingValue[1], selectCreatedReport);
                        }
                    } else {
                        // TODO: show message that value wasn't set
                    }
                });
        },
        getHeaderTitleFn: function () {
            var reports = getSelectedItemsArray();
            var temp = new Array();

            $.each(reports, function () {
                temp.push(this.data.Title);
            });

            var coma = "@{R=Core.Strings;K=AvailabilityService_Member_names_delimiter; E=js}" + " ";
            var reportsNames = temp.join(coma);

            if (reports.length < 2) {
                return String.format("@{R=Core.Strings;K=WEBJS_AB0_14;E=js}".replace(/&lt;/gi, "<").replace(/&gt;/gi, ">"), reportsNames);
            } else {
                return String.format("@{R=Core.Strings;K=WEBJS_AB0_21;E=js}".replace(/&lt;/gi, "<").replace(/&gt;/gi, ">"), reportsNames);
            }
        },
        viewNewReport: function () {
            viewReport();
        },
        reload: function () {
            if (comboArray.getValue() == "@{R=Core.Strings;K=WEBJS_VB0_76; E=js}" || comboArray.getValue() == "") {
                refreshGridObjects(0, userPageSize, grid, "", "", "", filterText, function () { });
            }
            else {
                refreshGridObjects(null, null, groupGrid, groupingValue[0], groupingValue[0], "", filterText);
                refreshGridObjects(0, userPageSize, grid, groupingValue[0], groupingValue[2], groupingValue[1], filterText, function () { });
            }
        },

        init: function () {
            isManagerGrid = (configuration.IsManagerGrid || "false") == "true";

            remoteReportsMode = typeof configuration.RemoteReportsMode == "undefined" ?
                false : configuration.RemoteReportsMode == 'true';

            remoteOrionID = typeof configuration.RemoteOrion == "undefined" ?
                0 : configuration.RemoteOrion.ID;

            remoteOrionName = typeof configuration.RemoteOrion == "undefined" ?
                "" : configuration.RemoteOrion.Name;

            swisBasedReportsOnly = typeof configuration.SwisBasedReportsOnly == "undefined" ?
                false : configuration.SwisBasedReportsOnly;

            userPageSize = parseInt(configuration.PageSize || '20');
            allowReport = (configuration.AllowReportMgmt || 'false').toLowerCase() == 'true';

            sortOrder = (configuration.SortOrder || '').split(',');
            groupingValue = (configuration.GroupingValue || 'legacy,null,').split(',');

            importReportController = configuration.ImportReportController;

            $("#importRemoteReportsDialog").bind('dialogclose', function () {
                if (comboArray.getValue() == "@{R=Core.Strings;K=WEBJS_VB0_76; E=js}" || comboArray.getValue() == "") {
                    refreshGridObjects(0, userPageSize, grid, "", "", "", filterText, function () { });
                }
                else {
                    refreshGridObjects(null, null, groupGrid, groupingValue[0], groupingValue[0], "", filterText);
                    refreshGridObjects(0, userPageSize, grid, groupingValue[0], groupingValue[2], groupingValue[1], filterText, function () { });
                }
            });

            var rIds = configuration.sessionInput;
            if (rIds) {
                selectReport = rIds;

                $.ajax({
                    type: "POST",
                    url: 'Default.aspx/ClearSessionValue',
                    data: "{ sessionVarName : 'NewReportId' }",
                    contentType: 'application/json; charset=utf-8',
                    failure: function (response) {
                        alert(response);
                    }
                });
            }

            selectedColumns = (configuration.SelectedColumns || '').split(',');

            if (isManagerGrid || remoteReportsMode) {
                selectorModel = new Ext.grid.CheckboxSelectionModel();

                var originalClearSelection = selectorModel.clearSelections;
                selectorModel.clearSelections = function () {
                    var selectedItems = getSelectedItemsArray();

                    originalClearSelection.call(selectorModel);

                    $(selectedItems).each(function (selectedItemIndex) {
                        invokeOnSelectionChanged(selectedItems[selectedItemIndex].data, false);
                    });
                };
            } else {
                selectorModel = new Ext.sw.grid.RadioSelectionModel();
                selectorModel.width = 25;
                selectorModel.hideable = false;
            }

            selectorModel.on("rowselect", function (sender, recordIndex, record) {
                selectAllTooltip(grid, pageSizeBox, function () {
                    var methodPath = remoteReportsMode ?
                        "/Orion/Services/ReportManager.asmx/GetAllReportsFromRemoteOrion" :
                        "/Orion/Services/ReportManager.asmx/GetAllSelectedReports";

                    return new ORION.WebServiceStore(methodPath,
                        [
                            { name: 'ReportID', mapping: 0 },
                            { name: 'Title', mapping: 1 },
                            { name: 'LegacyPath', mapping: 2 }
                        ]);
                });
                invokeOnSelectionChanged(record.data, true);
            });
            selectorModel.on("rowdeselect", function (sender, recordIndex, record) {
                clearSelectAllTooltip();
                invokeOnSelectionChanged(record.data, false);
            });
            selectorModel.on("selectionchange", updateToolbarButtons);
            grpGridFirstLoad = true;

            var gridStoreColumns = remoteReportsMode ?
                [{ name: 'ReportID', mapping: 0 },
                { name: 'Name', mapping: 1 },
                { name: 'Favorite', mapping: 2 },
                { name: 'Title', mapping: 3 },
                { name: 'Description', mapping: 4 }]
                :
                [{ name: 'ReportID', mapping: 0 },
                { name: 'Name', mapping: 1 },
                { name: 'Favorite', mapping: 2 },
                { name: 'Title', mapping: 3 },
                { name: 'Description', mapping: 4 },
                { name: 'LegacyPath', mapping: 5 },
                { name: 'ModuleTitle', mapping: 6 },
                { name: 'LimitationCategory', mapping: 7 },
                { name: 'Uri', mapping: 8 },
                { name: 'Category', mapping: 9 },
                { name: 'JobsDisplayName', mapping: 10 },
                { name: 'Type', mapping: 11 }];


            var gridColumnsModel = remoteReportsMode ?
                [selectorModel,
                    { header: 'ReportID', width: 80, hidden: true, hideable: false, sortable: true, dataIndex: 'ReportID' },
                    { id: 'rmFavorite', header: '<img src="/Orion/images/Reports/Star-empty.png"/>', width: 28, sortable: true, dataIndex: 'Favorite', renderer: renderFavorite },
                    { header: '@{R=Core.Strings;K=WEBJS_IB0_40;E=js}', width: 400, sortable: true, hideable: false, dataIndex: 'Title', renderer: renderEditName },
                    { id: 'Description', header: '@{R=Core.Strings;K=XMLDATA_SO0_28;E=js}', width: 300, sortable: true, dataIndex: 'Description', renderer: renderString }]
                :
                [selectorModel,
                    { header: 'ReportID', width: 80, hidden: true, hideable: false, sortable: true, dataIndex: 'ReportID' },
                    { id: 'rmFavorite', header: '<img src="/Orion/images/Reports/Star-empty.png"/>', width: 28, sortable: true, dataIndex: 'Favorite', renderer: renderFavorite },
                    { header: '@{R=Core.Strings;K=WEBJS_IB0_40;E=js}', width: 300, sortable: true, hideable: false, dataIndex: 'Title', renderer: renderEditName },
                    { id: 'Description', header: '@{R=Core.Strings;K=XMLDATA_SO0_28;E=js}', width: 300, sortable: true, dataIndex: 'Description', renderer: renderString },
                    { header: '@{R=Core.Strings;K=WEBJS_AB0_20;E=js}', width: 350, sortable: true, dataIndex: 'JobsDisplayName', renderer: renderSchedulesAssigned },
                    { header: '@{R=Core.Strings;K=WEBJS_IB0_38;E=js}', width: 150, hidden: true, sortable: true, dataIndex: 'Type', renderer: renderString },
                    { id: 'Category', header: '@{R=Core.Strings;K=XMLDATA_SO0_25;E=js}', width: 300, sortable: true, dataIndex: 'Category', renderer: renderString },
                    { header: '@{R=Core.Strings;K=XMLDATA_SO0_29;E=js}', width: 150, sortable: true, dataIndex: 'ModuleTitle', renderer: renderString },
                    { header: '@{R=Core.Strings;K=XMLDATA_SO0_34;E=js}', width: 110, sortable: true, dataIndex: 'LegacyPath', renderer: renderString },
                    { id: 'LimitationCategory', header: '@{R=Core.Strings;K=XMLDATA_SO0_31;E=js}', hidden: true, width: 150, sortable: true, dataIndex: 'LimitationCategory', renderer: renderLimitationCategory }];

            // get list of custom properties to push them into datastore mapping and grid columns
            SW.Core.Services.callWebServiceSync('/Orion/Services/ReportManager.asmx', 'GetReportCustomProperties', {}, function (result) {
                if (result && result.Rows) {
                    var index = gridStoreColumns.length;
                    for (var j = 0; j < result.Rows.length; j++) {
                        var renderer;
                        // setup correct renderer according to CP type
                        switch (result.Rows[j][2].toString().toLowerCase()) {
                            case "system.datetime":
                                renderer = renderDateTime;
                                break;
                            case "system.int32":
                            case "system.single":
                                renderer = renderFloat;
                                break;
                            case "system.boolean":
                                renderer = renderBool;
                                break;
                            default:
                                renderer = renderString;
                                break;
                        }
                        var columnName = String.format('CustomProperties.{0}', result.Rows[j][0]);
                        gridStoreColumns.push({ name: columnName, mapping: index });
                        customPropertyNames.push(columnName);
                        gridColumnsModel.push({ header: result.Rows[j][1], width: 150, sortable: true, dataIndex: columnName, renderer: renderer });
                        index++;
                    }
                }
            });

            // check if we're using custom property and if this custom property still exists
            if (groupingValue[0].indexOf("CustomProperties.") > -1 && $.inArray(groupingValue[0], customPropertyNames) == -1) {
                groupingValue = ',,,'.split(',');
            }
            if (sortOrder[0].indexOf("CustomProperties.") > -1 && $.inArray(sortOrder[0], customPropertyNames) == -1) {
                sortOrder = 'Name,ASC'.split(',');
            }
            var isDirtyColumns = false;
            for (var index = selectedColumns.length - 1; index >= 0; index--) {
                if (selectedColumns[index].indexOf("CustomProperties.") > -1 && $.inArray(selectedColumns[index], customPropertyNames) == -1) {
                    selectedColumns.splice(index, 1);
                    isDirtyColumns = true;
                }
            }

            if (isDirtyColumns)
                ORION.Prefs.save('SelectedColumns', selectedColumns.join(','));

            dataStore = remoteReportsMode ?
                new ORION.WebServiceStore("/Orion/Services/ReportManager.asmx/GetReportsFromRemoteOrion", gridStoreColumns) :
                new ORION.WebServiceStore("/Orion/Services/ReportManager.asmx/GetReports", gridStoreColumns);

            if (sortOrder[0] != 'Favorite Desc') // manually changed
                dataStore.sortInfo = { field: sortOrder[0], direction: sortOrder[1] }; // or 'DESC' (case sensitive for local sorting) };

            function addSchedule() {
                if ($("#isOrionDemoMode").length != 0) {
                    demoAction("Core_Scheduler_CreateNewSchedule", this);
                    return;
                }

                var selectedReports = [];
                $.each(getSelectedItemsArray(), function () {
                    selectedReports.push({ ID: this.data.ReportID, Title: this.data.Title });
                });

                var guid = SW.Core.Services.generateNewGuid();
                var params = { ReportData: JSON.stringify(selectedReports) };
                SW.Core.Services.postToTarget(String.format("/Orion/Reports/ScheduleAdd/Default.aspx?ScheduleWizardGuid={0}", guid), params);
            }

            function unAssignReport() {
                var reportIds = getSelectedReportsIds(getSelectedItemsArray());
                var temp = new Array();

                $.each(getSelectedItemsArray(), function () {
                    temp.push(this.data.Title);
                });
                var coma = "@{R=Core.Strings;K=AvailabilityService_Member_names_delimiter; E=js}" + " ";
                var reportsNames = temp.join(coma);

                var unassignTitle = "@{R=Core.Strings;K=WEBJS_YK0_10;E=js}";

                if (!scheduleUnassignDialog) {

                    scheduleUnassignDialog = $('#shceduleUnassignBox').dialog({
                        modal: true,
                        draggable: true,
                        resizable: false,
                        position: ['center'],
                        show: 'blind',
                        hide: 'blind',
                        width: 600,
                        title: '@{R=Core.Strings;K=WEBJS_YK0_11;E=js}',
                        dialogClass: 'ui-dialog-osx'
                    });

                    $(SW.Core.Widgets.Button('@{R=Core.Strings;K=CommonButtonType_Cancel;E=js}', { id: 'cancelBtn', type: 'secondary' })).appendTo(scheduleUnassignDialog).css('float', 'right').css('margin', '10px').click(function () {
                        scheduleUnassignDialog.dialog('close');
                    });

                    $(SW.Core.Widgets.Button('@{R=Core.Strings;K=WEBJS_YK0_9;E=js}', { id: 'unassignSelectedJobs', type: 'primary' })).appendTo(scheduleUnassignDialog).css('float', 'right').css('margin', '10px').click(function () {
                        if ($("#isOrionDemoMode").length != 0) {
                            demoAction("Core_Scheduler_UnassignSchedule", this);
                            return;
                        }
                        scheduleUnassignDialog.dialog('close');

                        var selectedJobs = [];
                        $.each($("#assignedSchedules").find('input:not(:checked)'), function () {
                            selectedJobs.push($(this).val());
                        });
                        var reportId = getSelectedReportsIds(getSelectedItemsArray())[0];
                        ORION.callWebService('/Orion/Services/ReportScheduler.asmx',
                            'UnassignReport', { jobIds: selectedJobs, reportId: reportId },
                            function (result) {
                                if (result) {
                                    getReportRowNumber(reportId, groupingValue[0], "", groupingValue[1], selectCreatedReport);
                                } else {
                                    //ToDO:
                                }
                            });
                    });
                }

                $("#assignedSchedules").empty();
                ORION.callWebService('/Orion/Services/ReportScheduler.asmx',
                    'GetReportAssignedSchedules', { reportId: reportIds[0] },
                    function (result) {
                        if (result) {
                            //unassignTitle = (result.length == 0) ? "@{R=Core.Strings;K=WEBJS_YK0_13;E=js}" : (result.length == 1) ? "@{R=Core.Strings;K=WEBJS_YK0_12;E=js}" : "@{R=Core.Strings;K=WEBJS_YK0_10;E=js}";
                            if (result.length > 0) {
                                $.each(result, function () {
                                    $("#assignedSchedules").append(String.format('<tr><td><input type="checkbox" value="{0}"></td><td>{1}</td></tr>', this.ReportJobID, Ext.util.Format.htmlEncode(this.Name)));
                                });
                            } else {
                                $("#assignedSchedules").append(String.format('<tr><td style="color:gray;">{0}</td></tr>', '@{R=Core.Strings;K=WEBJS_SO0_78;E=js}'));
                            }
                        }
                    });
                $("#unassignTitle").html(String.format(unassignTitle.replace(/&lt;/gi, "<").replace(/&gt;/gi, ">"), "<b>" + Ext.util.Format.htmlEncode(reportsNames) + "</b>"));

                scheduleUnassignDialog.dialog('open');
            }

            function assignToReport() {
                var reportIds = getSelectedReportsIds(getSelectedItemsArray());
                var jsonResult = [];
                SW.Orion.SchedulePicker.selectScheduleDialog();
                SW.Orion.SchedulePicker.clearSelection();
                if (reportIds.length == 1) {
                    ORION.callWebService('/Orion/Services/ReportScheduler.asmx',
                        'GetReportAssignedSchedules', { reportId: reportIds[0] },
                        function (result) {
                            if (result) {
                                $.each(result, function () {
                                    jsonResult.push({ "ID": this.ReportJobID, "Title": this.Name });
                                    if (jsonResult.length > 0) {
                                        SW.Orion.SchedulePicker.setSelectedReportJobsIds(jsonResult);
                                    }

                                });
                            } else {
                                // TODO: show message that value wasn't set
                            }
                        });
                }

                $("#shceduleReportBox").parent().css('margin-top', '7%');
            }

            function getExportImportMenuItems() {
                var items = [
                    {
                        id: 'ExportReport',
                        text: '@{R=Core.Strings;K=WEBJS_YK0_26;E=js}',
                        tooltip: '@{R=Core.Strings;K=WEBJS_YK0_26;E=js}',
                        icon: '/Orion/images/CPE/export.png',
                        cls: 'x-btn-text-icon',
                        handler: function () {
                            if (grid.getSelectionModel().getSelected().data.LegacyPath) {
                                Ext.Msg.show({ title: "@{R=Core.Strings;K=WEBJS_YK0_26;E=js}", cls: 'warn-popup-message', msg: "@{R=Core.Strings;K=WEBJS_YK0_27;E=js}", width: 250, buttons: { yes: '@{R=Core.Strings;K=CommonButtonType_Ok; E=js}' }, icon: Ext.Msg.WARNING });
                                return;
                            }
                            var id = grid.getSelectionModel().getSelections()[0].data.ReportID;
                            SW.Core.Services.postToTarget("/Orion/Reports/Default.aspx", { ExportReportID: encodeURIComponent(id) });
                            this.parentMenu.hide();
                        }
                    },
                    {
                        id: 'ImportReport',
                        text: '@{R=Core.Strings;K=WEBJS_YK0_28;E=js}',
                        tooltip: '@{R=Core.Strings;K=WEBJS_YK0_28;E=js}',
                        icon: '/Orion/images/CPE/import.png',
                        cls: 'x-btn-text-icon',
                        handler: function () {
                            if ($("#isOrionDemoMode").length != 0) {
                                demoAction("Core_Reporting_ImportReport", this);
                                return;
                            }

                            this.parentMenu.hide();
                        }
                    }];

                if (typeof importReportController != "undefined") {
                    items.push({
                        id: 'ImportRemoteOrionReport',
                        text: '@{R=Core.Strings;K=RemoteReportsImport_MenuItem;E=js}',
                        tooltip: '@{R=Core.Strings;K=RemoteReportsImport_MenuItem;E=js}',
                        icon: '/Orion/images/CPE/import.png',
                        cls: 'x-btn-text-icon',
                        handler: function () {
                            importReportController.selectReportsDilog();
                        }
                    });
                }

                return items;
            }

            if (isManagerGrid) {

                gridToolBar = [
                    { id: 'Add', text: '@{R=Core.Strings;K=WEBJS_TM0_127;E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_TM0_127;E=js}', icon: '/Orion/images/Reports/add_16x16.png', cls: 'x-btn-text-icon', handler: addReport }, '-',
                    { id: 'Edit', text: '@{R=Core.Strings;K=WEBJS_TM0_128;E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_TM0_128;E=js}', icon: '/Orion/images/Reports/edit_16x16.png', cls: 'x-btn-text-icon', handler: editReport }, '-',
                    { id: 'DublicateEdit', text: '@{R=Core.Strings;K=WEBJS_TM0_129;E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_TM0_129;E=js}', icon: '/Orion/images/Reports/duplicateEdit_report.png', cls: 'x-btn-text-icon', handler: dublicateEditReport }, '-',
                    { id: 'View', text: '@{R=Core.Strings;K=WEBJS_TM0_125;E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_TM0_125;E=js}', icon: '/Orion/images/Reports/view_report.png', cls: 'x-btn-text-icon', handler: viewReport }, '-',
                    {
                        id: 'ScheduleReport',
                        text: '@{R=Core.Strings;K=WEBJS_YK0_6;E=js}',
                        tooltip: '@{R=Core.Strings;K=WEBJS_YK0_6;E=js}',
                        icon: '/Orion/images/Reports/icon_scheduler.png',
                        cls: 'x-btn-text-icon',
                        menu: {
                            xtype: 'menu',
                            items: [{
                                id: 'CreateNewSchedule',
                                text: '@{R=Core.Strings;K=WEBJS_TM0_151;E=js}',
                                tooltip: '@{R=Core.Strings;K=WEBJS_TM0_151;E=js}',
                                icon: '/Orion/Nodes/images/icons/icon_add.gif',
                                cls: 'x-btn-text-icon',
                                handler: addSchedule
                            },
                            {
                                id: 'AssignExistingSchedule',
                                text: '@{R=Core.Strings;K=WEBJS_YK0_7;E=js}',
                                tooltip: '@{R=Core.Strings;K=WEBJS_YK0_7;E=js}',
                                icon: '/Orion/images/Reports/icon_assign_to.gif',
                                cls: 'x-btn-text-icon',
                                handler: function () {
                                    this.parentMenu.hide();
                                    assignToReport();
                                }
                            },
                            {
                                id: 'UnassignSchedule',
                                text: '@{R=Core.Strings;K=WEBJS_YK0_8;E=js}',
                                tooltip: '@{R=Core.Strings;K=WEBJS_YK0_8;E=js}',
                                icon: '/Orion/images/Reports/UnAssignReport.png',
                                cls: 'x-btn-text-icon',
                                handler: unAssignReport
                            }]
                        }
                    },
                    '-',
                    {
                        id: 'ExportImport', text: '@{R=Core.Strings;K=WEBJS_YK0_16;E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_YK0_16;E=js}', icon: '/Orion/images/import_export_icon_16x16.png', cls: 'x-btn-text-icon',
                        menu: {
                            xtype: 'menu',
                            items: getExportImportMenuItems(),
                            listeners: {
                                hide: function () {
                                    // menu hide event fire before click event on file uploader, so set 1 sec timeout before hiding file uploader control
                                    setTimeout(function () { $("#" + fileUploadControlID).css({ 'display': 'none' }); }, 1000);
                                },
                                show: function () {
                                    if (!fileUploadControlInitialized && fileUploadControlID != undefined && $("#isOrionDemoMode").length == 0) {
                                        $('#ImportReport').mousemove(function (e) {
                                            var position = $(this).offset();
                                            $("#" + fileUploadControlID).css({
                                                'display': 'block',
                                                'left': position.left,
                                                'top': position.top,
                                                'height': $(this).outerHeight(),
                                                'width': $(this).outerWidth()
                                            });
                                        });
                                        fileUploadControlInitialized = true;
                                    }
                                }
                            }
                        }

                    }, '-',
                    {
                        id: 'Delete',
                        text: '@{R=Core.Strings;K=CommonButtonType_Delete;E=js}',
                        tooltip: '@{R=Core.Strings;K=CommonButtonType_Delete;E=js}',
                        icon: '/Orion/Nodes/images/icons/icon_delete.gif',
                        cls: 'x-btn-text-icon',
                        handler: function () {
                            if ($("#isOrionDemoMode").length != 0) {
                                demoAction("Core_Reporting_DeleteReport", this);
                                return;
                            }
                            if (isLegacyReportsSelected(getSelectedItemsArray())) {
                                showWarningPopup('@{R=Core.Strings;K=CommonButtonType_Delete;E=js}', String.format('@{R=Core.Strings;K=WEBJS_IB0_37;E=js}', String.format('<a href="{0}" target="_blank" rel="noopener noreferrer">', SW.Core.KnowledgebaseHelper.getKBUrl(4603, '@{R=Core.Strings;K=CurrentHelpLanguage;E=js}')), '</a>'));
                                return;
                            }
                            Ext.Msg.show({
                                title: '@{R=Core.Strings;K=WEBJS_TM0_130;E=js}',
                                msg: '<b>@{R=Core.Strings;K=WEBJS_TM0_131;E=js}</b>',
                                buttons: { yes: '@{R=Core.Strings;K=CommonButtonType_Delete; E=js}', cancel: '@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}' },
                                icon: Ext.Msg.ERROR,
                                fn: function (btn) {
                                    if (btn == 'yes') {
                                        deleteSelectedItems(getSelectedItemsArray(), function (result) {
                                            if (result && result != '')
                                                Ext.Msg.show({ title: "@{R=Core.Strings;K=WEBJS_TM0_132;E=js}", msg: result, minWidth: 500, buttons: Ext.Msg.CANCEL, icon: Ext.MessageBox.ERROR });
                                            if (comboArray.getValue() == "@{R=Core.Strings;K=WEBJS_VB0_76; E=js}" || comboArray.getValue() == "") {
                                                refreshGridObjects(0, userPageSize, grid, "", "", "", filterText, function () {
                                                });
                                            } else {
                                                refreshGridObjects(null, null, groupGrid, groupingValue[0], groupingValue[0], "", filterText);
                                                refreshGridObjects(0, userPageSize, grid, groupingValue[0], groupingValue[2], groupingValue[1], filterText, function () {
                                                });
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }, ' ', '->',
                    new Ext.ux.form.SearchField({
                        id: 'searchFieldLocal',
                        store: dataStore,
                        width: 200,
                        listeners: {
                            searchStarting: function (data) {
                                data.swisBasedReportsOnly = swisBasedReportsOnly;
                                data.resourceFilter = "";
                            }
                        }
                    })
                ];
            } else if (remoteReportsMode) {
                gridToolBar = [
                    '->',
                    new Ext.ux.form.SearchField({
                        id: 'searchFieldRemote',
                        store: dataStore,
                        width: 200,
                        guid: guid,
                        listeners: {
                            searchStarting: function (data) {
                                data.serverID = remoteOrionID;
                                data.swisBasedReportsOnly = swisBasedReportsOnly;
                                data.resourceFilter = "";
                            }
                        }
                    })
                ];
            } else {
                gridToolBar = [
                    { id: 'View', text: '@{R=Core.Strings;K=WEBJS_TM0_125;E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_TM0_125;E=js}', icon: '/Orion/images/Reports/view_report.png', cls: 'x-btn-text-icon', handler: viewReport },
                    ' ', '->',
                    new Ext.ux.form.SearchField({
                        id: 'searchFieldLocal',
                        store: dataStore,
                        width: 200,
                        listeners: {
                            searchStarting: function (data) {
                                data.swisBasedReportsOnly = swisBasedReportsOnly;
                                data.resourceFilter = "";
                            }
                        }
                    })
                ];
            }

            var groupingStore = new ORION.WebServiceStore(
                "/Orion/Services/ReportManager.asmx/GetReportGroupValues",
                [
                    { name: 'Value', mapping: 0 },
                    { name: 'DisplayNamePlural', mapping: 1 },
                    { name: 'Cnt', mapping: 2 }
                ],
                "Value", "");

            pageSizeBox = new Ext.form.NumberField({
                id: 'PageSizeField' + (remoteReportsMode ? 'Remote' : 'Local'),
                enableKeyEvents: true,
                allowNegative: false,
                width: 40,
                allowBlank: false,
                minValue: 1,
                maxValue: 100,
                value: userPageSize,
                listeners: {
                    scope: this,
                    'keydown': function (f, e) {
                        var k = e.getKey();
                        if (k == e.RETURN) {
                            e.stopEvent();
                            var v = parseInt(f.getValue());
                            if (!isNaN(v) && v > 0 && v <= 100) {
                                userPageSize = v;
                                pagingToolbar.pageSize = userPageSize;
                                ORION.Prefs.save('PageSize', userPageSize);
                                pagingToolbar.doLoad(0);
                                setMainGridHeight(userPageSize);
                            }
                            else {
                                Ext.Msg.show({
                                    title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                    msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                                });
                                return;
                            }
                        }
                    },
                    'focus': function (field) {
                        field.el.dom.select();
                    },
                    'change': function (f, numbox, o) {
                        var pSize = parseInt(f.getValue());
                        if (isNaN(pSize) || pSize < 1 || pSize > 100) {
                            Ext.Msg.show({
                                title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                            });
                            return;
                        }

                        if (pagingToolbar.pageSize != pSize) { // update page size only if it is different
                            pagingToolbar.pageSize = pSize;
                            userPageSize = pSize;

                            ORION.Prefs.save('PageSize', userPageSize);
                            pagingToolbar.doLoad(0);
                            setMainGridHeight(userPageSize);
                        }
                    }
                }
            });

            var pagingToolbar = new Ext.PagingToolbar(
                {
                    store: dataStore,
                    pageSize: userPageSize,
                    displayInfo: true,
                    displayMsg: "@{R=Core.Strings;K=WEBJS_TM0_5; E=js}",
                    emptyMsg: "@{R=Core.Strings;K=WEBJS_SO0_54; E=js}",
                    beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                    afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                    firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                    prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                    nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                    lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                    refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}",
                    items: [
                        '-',
                        new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_VB0_46; E=js}', style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;' }),
                        pageSizeBox
                    ]
                }
            );

            var groupingDataStore = new ORION.WebServiceStore(
                "/Orion/Services/ReportManager.asmx/GetObjectGroupProperties",
                [
                    { name: 'Value', mapping: 0 },
                    { name: 'Name', mapping: 1 },
                    { name: 'Type', mapping: 2 }
                ]);

            comboArray = new Ext.form.ComboBox(
                {
                    id: groupComboID,
                    mode: 'local',
                    boxMaxWidth: comboboxMaxWidth,
                    fieldLabel: 'Name',
                    displayField: 'Name',
                    valueField: 'Value',
                    store: groupingDataStore,
                    triggerAction: 'all',
                    value: '@{R=Core.Strings;K=WEBJS_VB0_76; E=js}',
                    typeAhead: true,
                    forceSelection: true,
                    selectOnFocus: false,
                    multiSelect: false,
                    editable: false,
                    listeners: {
                        'select': function () {
                            var val = this.store.data.items[this.selectedIndex].data.Value;
                            var type = this.store.data.items[this.selectedIndex].data.Type;

                            refreshGridObjects(null, null, groupGrid, val, type, "", filterText);

                            if (val == '') {
                                ORION.Prefs.save('GroupingValue', ',,');
                                refreshGridObjects(0, userPageSize, grid, "", "", "", filterText, function () {
                                    $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function () {
                                        this.tooltip = 'processed';
                                        $.swtooltip(this);
                                    });
                                });
                            }
                        },
                        'beforerender': function () {
                            refreshGridObjects(null, null, comboArray, "", "", "", "");
                        }
                    }
                });

            comboArray.getStore().on('load', function () {
                comboArray.setValue(groupingValue[0]);

                refreshGridObjects(null, null, groupGrid, groupingValue[0], groupingValue[0], "", "");
                resizeToFitContent(comboArray);
            });
            var updateColumnWidth = function () {
                $.each($("#" + gridID), function () {
                    var gridColumnsWidth = $(this).find(".x-grid3-header-offset").width();
                    var parentWidth = $(this).width();
                    $.each($(this).find(".x-grid3-body"), function () {
                        if (gridColumnsWidth < parentWidth) {
                            $(this).children().width(parentWidth - 5); // - 5px for borders
                        }
                    });
                });
            };
            grid = new Ext.grid.GridPanel({
                id: gridID,
                region: 'center',
                viewConfig: { forceFit: true, emptyText: "@{R=Core.Strings;K=WEBJS_SO0_54; E=js}" },
                store: dataStore,
                baseParams: { start: 0, limit: userPageSize },
                stripeRows: true,
                trackMouseOver: false,
                columns: gridColumnsModel,
                sm: selectorModel,
                autoScroll: true,
                autoExpandColumn: 'Description',
                loadMask: true,
                listeners: {
                    cellmousedown: function (grid, rowIndex, columnIndex, e) {
                        var fieldName = grid.getColumnModel().getDataIndex(columnIndex); // Get field name
                        if (fieldName == 'Favorite') {
                            var record = grid.getStore().getAt(rowIndex); // Get the Record
                            var data = record.get(fieldName);
                            var id = record.get('ReportID');
                            if ($("#isOrionDemoMode").length == 0) {
                                ORION.callWebService("/Orion/Services/ReportManager.asmx",
                                    "ChangeFavoriteState", { reportId: id, value: !stringToBoolean(data) }, function (result) {
                                        if (result) {
                                            record.set(fieldName, !stringToBoolean(data));
                                            record.commit();
                                        } else {
                                            // TODO: show message that value wasn't set
                                        }
                                    });
                            }
                            else {
                                // if it's demo mode than just change image
                                record.set(fieldName, !stringToBoolean(data));
                                record.commit();
                            }
                            return false;
                        }
                    },
                    columnmove: function () {
                        renderSchedulesCountTooltip();
                    },
                    resize: function () {
                        renderSchedulesCountTooltip();
                        updateColumnWidth();
                    },
                    columnresize: function () {
                        renderSchedulesCountTooltip();
                    }
                },
                tbar: gridToolBar,
                bbar: pagingToolbar
            });

            // making columns visible or hidden according to saved configuration
            for (var i = 1; i < grid.getColumnModel().getColumnCount(); i++) {
                if (selectedColumns.indexOf(grid.getColumnModel().getDataIndex(i)) > -1) {
                    grid.getColumnModel().setHidden(i, false);
                } else {
                    grid.getColumnModel().setHidden(i, true);
                }
            }

            grid.getColumnModel().on('hiddenchange', function () {
                var cols = '';
                for (var i = 1; i < grid.getColumnModel().getColumnCount(); i++) {
                    if (!grid.getColumnModel().isHidden(i)) {
                        cols += grid.getColumnModel().getDataIndex(i) + ',';
                    }
                }
                ORION.Prefs.save('SelectedColumns', cols.slice(0, -1));
            });

            grid.store.on('beforeload', function () {
                invokeOnReportsLoading();
                grid.getEl().mask('@{R=Core.Strings;K=WEBJS_VB0_1; E=js}', 'x-mask-loading');
            });

            grid.on('sortchange', function (store, option) {
                if ((!grpGridFirstLoad) || (remoteReportsMode)) {
                    var sort = option.field;
                    var dir = option.direction;

                    if (sort && dir) {
                        sortOrder[0] = sort;
                        sortOrder[1] = dir;
                        ORION.Prefs.save('SortOrder', sort + ',' + dir);
                    }
                }
            });

            grid.store.on('load', function () {
                invokeOnReportsLoaded();
                grid.getEl().unmask();
                setMainGridHeight(userPageSize);
                updateColumnWidth();
                renderSchedulesCountTooltip();
                $("#selectionTip").remove();
                if (isSelectAllMode) {
                    isSelectAllMode = false;
                    updateToolbarButtons();
                }
            });
            
            grid.store.on("exception", function (dataProxy, type, action, options, response, arg) {
                var error = eval("(" + response.responseText + ")");
                Ext.Msg.show({
                    title: '@{R=Core.Strings;K=ReportManager_RemoteServerUnreachable; E=js}',
                    msg: error.Message,
                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                });
                grid.getEl().unmask();
            });

            var mainGridPanelItems = [];

            if (!remoteReportsMode) {
                groupGrid = new Ext.grid.GridPanel({
                    id: groupingGridID,
                    store: groupingStore,
                    cls: 'hide-header',
                    columns: [
                        { id: 'Value', width: 193, editable: false, sortable: false, dataIndex: 'DisplayNamePlural', renderer: renderGroup }
                    ],
                    selModel: new Ext.grid.RowSelectionModel(),
                    autoScroll: true,
                    loadMask: true,
                    listeners: {
                        cellclick: function (mygrid, row, cell, e) {
                            var val = mygrid.store.data.items[row].data[mygrid.store.data.items[row].fields.keys[cell]];
                            var type = "";
                            $.each(comboArray.store.data.items, function (index, item) {
                                if (item.data.Value == comboArray.getValue()) {
                                    type = item.data.Type;
                                    return false;
                                }
                            });
                            if (val == '') {
                                val = '{empty}';
                            }

                            if (val == null) {
                                val = "null";
                            }

                            if (val == '[All]')
                                val = '';
                            var newGroupingValues = comboArray.getValue() + ',' + val + ',' + type;
                            ORION.Prefs.save('GroupingValue', newGroupingValues);
                            groupingValue = newGroupingValues.split(',');

                            refreshGridObjects(0, userPageSize, grid, comboArray.getValue(), type, val, filterText, function () {
                                $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function () {
                                    this.tooltip = 'processed';
                                    $.swtooltip(this);
                                });
                            });
                        }
                    },
                    anchor: '0 0', viewConfig: { forceFit: true }, split: true, autoExpandColumn: 'Value'
                });

                groupGrid.store.on('load', function () {
                    if (grpGridFirstLoad) {
                        var selectedVal = groupGrid.getStore().find("Value", groupingValue[1]);
                        if (selectedVal == -1) {
                            groupingValue[1] = "";
                            selectedVal = 0;
                        }
                        groupGrid.getSelectionModel().selectRow(selectedVal, false);
                        grpGridFirstLoad = false;
                    }
                });

                var groupByTopPanelControls = [];

                groupByTopPanelControls.push(new Ext.form.Label({ text: "@{R=Core.Strings;K=WEBJS_AK0_76; E=js}" }));
                groupByTopPanelControls.push(comboArray);

                var groupByTopPanel = new Ext.Panel({
                    id: groupByTopPanelID,
                    region: 'center',
                    split: false,
                    heigth: 50,
                    collapsible: false,
                    viewConfig: { forceFit: true },
                    items: groupByTopPanelControls,
                    cls: 'panel-no-border panel-bg-gradient'
                });

                var navigationPanelControls = [];
                navigationPanelControls.push(groupByTopPanel);

                if (groupGrid != null) navigationPanelControls.push(groupGrid);

                var navPanel = new Ext.Panel({
                    id: navPanelID,
                    region: 'west',
                    split: true,
                    anchor: '0 0',
                    collapsible: false,
                    viewConfig: { forceFit: true },
                    items: navigationPanelControls,
                    cls: 'panel-no-border'
                });
                mainGridPanelItems.push(navPanel);

                navPanel.on("bodyresize", function () {
                    if (groupGrid != null) groupGrid.setWidth(navPanel.getSize().width);
                });
            }

            mainGridPanelItems.push(grid);

            var mainGridPanel = new Ext.Panel({
                id: mainGridID,
                region: 'center',
                split: true,
                layout: 'border',
                collapsible: false,
                items: mainGridPanelItems,
                cls: 'no-border'
            });

            mainGridPanel.render(configuration.ContainerID);

            $(window).resize(function () {
                setMainGridHeight(userPageSize);
                mainGridPanel.doLayout();
            });

            if (!remoteReportsMode) {
                    getReportRowNumber(selectReport, groupingValue[0], "", groupingValue[1], selectCreatedReport);
            }

            updateToolbarButtons();
        },
        setFileUploadControl: function (id) {
            fileUploadControlID = id;
        },
        importFileSelected: function (file) {
            var re = /\.[^.]+$/;
            var ext = file.value.match(re);
            if (hash[ext]) {
                __doPostBack(fileUploadControlID, '');
                return true;
            } else {
                Ext.Msg.show({
                    width: 400,
                    title: "@{R=Core.Strings;K=WEBJS_YK0_24; E=js}",
                    msg: "@{R=Core.Strings;K=WEBJS_YK0_25; E=js}",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
                return false;
            }
        },

        clearSelections: function () {
            grid.getSelectionModel().clearSelections();
        },

        selectRow: function (reportID) {
            for (var index = 0; index < grid.getStore().getCount(); index++) {
                var current = grid.getStore().getAt(index);
                if (current.data.ReportID == reportID) {
                    grid.getSelectionModel().selectRow(index, true);
                    break;
                }
            }
        },
        deselectRow: function (reportID) {
            for (var index = 0; index < grid.getStore().getCount(); index++) {
                var current = grid.getStore().getAt(index);
                if (current.data.ReportID == reportID) {
                    grid.getSelectionModel().deselectRow(index);
                    break;
                }
            }
        },

        load: function () {
            refreshGridObjects(0, userPageSize, grid, "", "", "", filterText);
        },
        layout: function () {
            setMainGridHeight(userPageSize);

            var mainGrid = Ext.getCmp(mainGridID);
            mainGrid.doLayout();
        }
    };
};
