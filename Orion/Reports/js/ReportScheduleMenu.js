﻿Ext.namespace('SW');
Ext.namespace('SW.Orion');
Ext.QuickTips.init();

Ext.onReady(function () {
        if (!Ext.isEmpty(Ext.get('sheduleMenu'))) {
            var tb = new Ext.Toolbar({
                renderTo: "sheduleMenu",
                border: false,
                cls: 'sheduleMenu'
            });
        } else return;

        function addSchedule() {
            if ($("#isOrionDemoMode").length != 0) {
                demoAction("Core_Scheduler_CreateNewSchedule", this);
                return;
            }

            var selectedReports = [];
            selectedReports.push({ ID: getReportID(), Title: getReportName() });

            var guid = SW.Core.Services.generateNewGuid();
            var params = { ReportData: JSON.stringify(selectedReports) };
            SW.Core.Services.postToTarget(String.format("/Orion/Reports/ScheduleAdd/Default.aspx?ScheduleWizardGuid={0}", guid), params);
        }

        var menu = new Ext.menu.Menu({
            id: 'mainMenu',
            overCls: 'overSheduleMenu',
            items: [
                {
                    id: 'CreateNewSchedule',
                    text: '@{R=Core.Strings;K=WEBJS_TM0_151;E=js}',
                    tooltip: '@{R=Core.Strings;K=WEBJS_TM0_151;E=js}',
                    icon: '/Orion/Nodes/images/icons/icon_add.gif',
                    cls: 'x-btn-text-icon',
                    handler: addSchedule
                },
                {
                    id: 'AssignExistingSchedule',
                    text: '@{R=Core.Strings;K=WEBJS_YK0_7;E=js}',
                    tooltip: '@{R=Core.Strings;K=WEBJS_YK0_7;E=js}',
                    icon: '/Orion/images/Reports/icon_assign_to.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        this.parentMenu.hide();
                        assignToReport();
                    }
                }
            ]
    });
    menu.removeClass("x-btn-over");
    tb.add({
        text: '@{R=Core.Strings;K=WEBJS_YK0_6;E=js}',
        tooltip: '@{R=Core.Strings;K=WEBJS_YK0_6;E=js}',
        icon: '/Orion/images/Reports/icon_scheduler.png',
        menu: menu
    });
    tb.doLayout();

    function assignToReport() {
        var jsonResult = [];
        var id = getReportID();

        SW.Orion.SchedulePicker.selectScheduleDialog();
        SW.Orion.SchedulePicker.clearSelection();
        ORION.callWebService('/Orion/Services/ReportScheduler.asmx',
            'GetReportAssignedSchedules', { reportId: id },
            function (result) {
                if (result) {
                    $.each(result, function () {
                        jsonResult.push({ "ID": this.ReportJobID, "Title": this.Name });
                        if (jsonResult.length > 0) {
                            SW.Orion.SchedulePicker.setSelectedReportJobsIds(jsonResult);
                        }

                    });
                } else {
                    // TODO: show message that value wasn't set
                }
            });
        // }                     
        $("#shceduleReportBox").parent().css('margin-top', '7%');
    }
});

function SchedulesSelected(dictionary) {
    var schedulesIds = [];
    var id = getReportID();
    $.each(dictionary, function () {
        schedulesIds.push(this.Key);
    });

    var rIds = [id];
    ORION.callWebService('/Orion/Services/ReportScheduler.asmx',
                    'AssignSchedules', { reportIds: rIds, schedulesIds: schedulesIds },
                    function (result) {
                        if (result) {
                        } else {
                            // TODO: show message that value wasn't set
                        }
                    });
};
