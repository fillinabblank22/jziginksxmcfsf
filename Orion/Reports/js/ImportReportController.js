﻿SW.Core.namespace("SW.Core.Reports").ImportReportController = function (configuration)
{
    var solarwindsServersControl;
    var reportsToImport;
    var reportManager;
    var selectedReportsIndex = 0;
    var importRemoteReportsSessionID;
    var immutable = false;

    var solarwindsServersTabID = 1;
    var reportsSelectionTabID = 2;
    var currentTab;
    var callerFilterText;
    

    var ImportingDuplicatedReportStrategy =
    {
        None: 0,
        KeepOriginal: 1,
        Overwrite: 2,
        Both: 3
    };

    var createReportsToImportStorage = function (selectedServer)
    {
        var orionID = selectedServer.ID;
        var orionName = selectedServer.Name;

        var storage = [];

        var indexOf = function (reportID)
        {
            for (var index = 0; index < storage.length; index++)
            {
                if (storage[index].ReportID == reportID) return index;
            }

            return -1;
        };

        return {

            add: function (reportID, blockID)
            {
                storage.push(
                {
                    ReportID: reportID,
                    BlockID: blockID,
                    ImportingDuplicatedReportStrategy: ImportingDuplicatedReportStrategy.None
                });
            },
            remove: function (reportID)
            {
                var index = indexOf(reportID);
                if (index >= 0) storage.splice(index, 1);
            },

            setImportingDuplicatedReportStrategy: function (reportID, strategy)
            {
                var index = indexOf(reportID);
                if (index < 0) return;

                var item = storage[index];
                item.ImportingDuplicatedReportStrategy = strategy;
            },

            findItem: function (reportID)
            {
                var index = indexOf(reportID);
                return index >= 0 ? storage[index] : null;
            },

            selectedOrionID: function ()
            {
                return orionID;
            },
            selectedOrionName: function ()
            {
                return orionName;
            },

            selectedReportsIDs: function ()
            {
                var reportsIDs = [];

                for (var index = 0; index < storage.length; index++)
                {
                    var reportID = storage[index].ReportID;
                    reportsIDs.push(reportID);
                }

                return reportsIDs;
            },

            items: function ()
            {
                return storage;
            }
        };
    };


    function activateOrionSelectionTab()
    {
        if (currentTab == solarwindsServersTabID) return;
        currentTab = solarwindsServersTabID;
        
        configuration.RemoteReportsDialogAccordion.accordion('activate', 0);

        $("#orionServerSelectionHeaderText1").show();
        $("#orionServerSelectionHeaderText2").hide();
        $("#orionServerSelectionHeaderText3").hide();

        $("#selectOrionAndContinue").show();
        $("#importReportsButton").hide();

        $("#orionServerSelectionHeader").removeClass("selectionHeaderInactive");
        $("#remoteReportsSelectionHeader").addClass("selectionHeaderInactive");
    }
    function activateReportsSelectionTab()
    {
        if (currentTab == reportsSelectionTabID) return;
       
        var selectedServer = solarwindsServersControl.getSelectedSite();
        if (selectedServer == null) return;

        // Show error if server status is not OK
        if (selectedServer.Status != "1") {
            Ext.Msg.show({
                title: '@{R=Core.Strings;K=ReportManager_RemoteServerUnreachable; E=js}',
                msg: '@{R=Core.Strings;K=RemoteReportsImport_UnableToContactServer; E=js}',
                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
            })
            return;
        }

        currentTab = reportsSelectionTabID;
        configuration.RemoteReportsDialogAccordion.accordion('activate', 1);

        $("#orionServerSelectionHeaderText1").hide();
        $("#orionServerSelectionHeaderText2").show();
        $("#orionServerSelectionHeaderText3").show();

        $("#orionServerSelectionHeaderText3").text(selectedServer.Name);

        destroyReportManagerGrid();
        createReportManagerGrid(selectedServer);

        $("#selectOrionAndContinue").hide();
        $("#importReportsButton").show();

        $("#orionServerSelectionHeader").addClass("selectionHeaderInactive");
        $("#remoteReportsSelectionHeader").removeClass("selectionHeaderInactive");

        displayNumberOfSelectedReports();
    }


    $("#selectOrionAndContinue").click(function ()
    {
        activateReportsSelectionTab();
    });
    $("#importReportsButton").click(function ()
    {
        if (importReportButtonsDisabled) return;

        var orionID = reportsToImport.selectedOrionID();
        var reportIDs = reportsToImport.selectedReportsIDs();

        if (reportIDs.length == 0)
        {
            Ext.Msg.show({
                title: "@{R=Core.Strings;K=RemoteReportsImport_ErrorSelectingReports_Title; E=js}",
                msg: "@{R=Core.Strings;K=RemoteReportsImport_ErrorSelectingReports_Description; E=js}",
                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
            });
            return;
        }

        disableImportReportButtons();
        var importingReportsConflictsRequestArguments =
        {
            serverID: orionID,
            reportIDs: reportIDs
        };

        SW.Core.Services.callWebService('/Orion/Services/ReportManager.asmx', 'GetImportingReportsConflicts', importingReportsConflictsRequestArguments, function (result)
        {
            var reportsDuplicatesResolvingData = new Array();

            if (result.DataTable.Rows.length == 0)
            {
                importReports();
            }
            else
            {
                for (var recordIndex = 0; recordIndex < result.DataTable.Rows.length; recordIndex++)
                {
                    var row = result.DataTable.Rows[recordIndex];

                    var reportID = row[0];
                    var reportTitle = row[1];

                    reportsDuplicatesResolvingData.push([reportID, reportTitle, true, false, false]);

                    reportsToImport.setImportingDuplicatedReportStrategy(reportID,
                        ImportingDuplicatedReportStrategy.KeepOriginal);
                }

                displayResolveImportingReportsConflictsDialog(reportsDuplicatesResolvingData, function ()
                {
                    importReports();
                });
            }
        });
    });
    $("#cancelImportButton").click(function ()
    {
        if (importReportButtonsDisabled) return;
        
        closeImportReportsDialog();
    });


    var importReportButtonsDisabled = false;
    var disableImportReportButtons = function()
    {
        importReportButtonsDisabled = true;

        $("#importReportsButton").removeClass("sw-btn-primary");
        $("#importReportsButton").addClass("sw-btn-disabled");

        $("#cancelImportButton").addClass("sw-btn-disabled");
    };
    var enableImportReportButtons = function()
    {
        importReportButtonsDisabled = false;

        $("#importReportsButton").removeClass("sw-btn-disabled");
        $("#importReportsButton").addClass("sw-btn-primary");

        $("#cancelImportButton").removeClass("sw-btn-disabled");
    };


    var initialize = function ()
    {
        initializeAccordion();

        createSolarwindsServersControl();
        activateOrionSelectionTab();

        $("#orionServerSelectionHeader").click(function ()
        {
            if (!$(this).hasClass("ui-state-disabled")) activateOrionSelectionTab();
        });

        $("#remoteReportsSelectionHeader").click(function ()
        {
            if (!$(this).hasClass("ui-state-disabled")) activateReportsSelectionTab();
        });
    };
    var initializeAccordion = function ()
    {
        configuration.RemoteReportsDialogAccordion.accordion(
        {
            autoHeight: false,
            active: 0
        });

        var accordion = configuration.RemoteReportsDialogAccordion.data("accordion");
        accordion._std_clickHandler = accordion._clickHandler;
        accordion._clickHandler = function (event, target)
        {
            var clicked = $(event.currentTarget || target);
            if (!clicked.hasClass("ui-state-disabled"))
            {
                this._std_clickHandler(event, target);
            }
        };
    };
    var destroyAccordion = function ()
    {
        configuration.RemoteReportsDialogAccordion.accordion('destroy');
    };


    var createSolarwindsServersControl = function ()
    {
        var searchFieldID = SW.Core.Services.generateNewGuid();

        var solarwindsServersControlConfiguration =
        {
            ContainerID: "solarwindsServersControlContainer",
            Height: 350,
            OnSelectedSiteChanged: solarwindsServersSelectionChangeHandler,
            SearchFieldID: searchFieldID
        };

        solarwindsServersControl = SW.Core.Reports.ImportReportSourceSiteGrid(solarwindsServersControlConfiguration);
        solarwindsServersControl.init();
    };
    var destroySolarwindsServersControl = function ()
    {
        $("#solarwindsServersControlContainer").empty();
    };
    var solarwindsServersSelectionChangeHandler = function ()
    {
        var selectedServer = solarwindsServersControl.getSelectedSite();
        if (selectedServer == null)
        {
            $("#selectOrionAndContinue").addClass("sw-btn-disabled");
            $("#remoteReportsSelectionHeader").addClass("ui-state-disabled");
            $("#remoteReportsSelectionHeader").addClass("remoteReportsSelectionHeaderDisabled");

        }
        else
        {
            $("#selectOrionAndContinue").removeClass("sw-btn-disabled");
            $("#remoteReportsSelectionHeader").removeClass("ui-state-disabled");
            $("#remoteReportsSelectionHeader").removeClass("remoteReportsSelectionHeaderDisabled");
        }
    };

    var createReportManagerGrid = function (selectedServer)
    {
        reportsToImport = createReportsToImportStorage(selectedServer);

        configuration.ReportManagerConfiguration.RemoteOrion =
        {
            ID: selectedServer.ID,
            Name: selectedServer.Name
        };

        configuration.ReportManagerConfiguration.OnReportsLoading = function ()
        {
            immutable = true;
        };
        configuration.ReportManagerConfiguration.OnReportsLoaded = function ()
        {
            immutable = false;

            var items = reportsToImport.items();
            for (var index = 0; index < items.length; index++)
            {
                reportManager.selectRow(items[index].ReportID);
            }
        };

        configuration.ReportManagerConfiguration.OnSelectionChanged = function (recordData, added)
        {
            if (added)
            {
                createReportBlock(recordData);
            }
            else
            {
                removeReportBlock(recordData.ReportID);
            }

            displayNumberOfSelectedReports();
        };

        $("#removeAllSelectedReports").click(function ()
        {
            while (reportsToImport.items().length > 0)
            {
                deselectReport(reportsToImport.items()[0].ReportID);
            }
            reportManager.clearSelections();
            
            displayNumberOfSelectedReports();
        });

        $("#importRemoteReportsDialog").show();
        $(".importReportsWarning").hide();

        // SearchField control global variable
        filterText = "";

        reportManager = new SW.Orion.ReportManager(configuration.ReportManagerConfiguration);
        reportManager.init();
        reportManager.load();

        reportManager.layout();
    };
    var displayNumberOfSelectedReports = function ()
    {
        var selectedReportsCountTemplate = "@{R=Core.Strings;K=RemoteReportsImport_SelectedReports;E=js}";
        var selectedReportsCount = String.format(selectedReportsCountTemplate, reportsToImport.items().length);

        $("#selectedReportsCount").text(selectedReportsCount);
    };
    var prepareImportReportsResultNotification = function (importedCount)
    {
        var importedReportsCountTemplate = "@{R=Core.Strings;K=RemoteReportsImport_ImportingReports_Progress;E=js}";
        var importedReportsCountMessage = String.format(importedReportsCountTemplate, importedCount, reportsToImport.items().length);

        displayReportsNotification(importedCount != reportsToImport.items().length, importedReportsCountMessage);
    };
    var destroyReportManagerGrid = function () {
        ORION.prefix = "ReportManager_";
        $("#" + configuration.ReportManagerConfiguration.ContainerID).empty();
        $("#selectedReportsContainer").empty();
    };

    var deselectReport = function(reportID)
    {
        if (immutable) return;

        removeReportBlock(reportID);
        reportManager.deselectRow(reportID);
    };

    var createReportBlock = function (recordData)
    {
        if (immutable) return;

        var reportItem = reportsToImport.findItem(recordData.ReportID);
        if (reportItem != null) return;

        reportsToImport.add(recordData.ReportID, selectedReportsIndex);

        var selectedReportBlockID = 'selectedReportBlock-' + selectedReportsIndex;

        $('#' + configuration.SelectedReportsContainerID).append(
            '<table id="' + selectedReportBlockID + '" class="selectedReportBlock">\
                <tr>\
                    <td class = "column1" style="font-size: 12px;">• <b>' + recordData.Title + '</b></td>\
                    <td class = "column2"><div class="deselectReportButton"/></td>\
                </tr>\
            </table>');

        $('#' + selectedReportBlockID).find('.deselectReportButton').each(function ()
        {
            $(this).click(function ()
            {
                deselectReport(recordData.ReportID);
            });
        });

        selectedReportsIndex++;
    };
    var removeReportBlock = function (reportID)
    {
        if (immutable) return;

        var reportItem = reportsToImport.findItem(reportID);
        if (reportItem == null) return;

        $('#selectedReportBlock-' + reportItem.BlockID).remove();
        reportsToImport.remove(reportID);
    };


    var displayResolveImportingReportsConflictsDialog = function (reportsDuplicatesResolvingData, callback)
    {
        var reportsDuplicatesResolvingGridPageSize = 5;

        $("#importRemoteReportsDuplicatesResolvingDialog").dialog(
        {
            resizable: false,
            width: 650,
            height: 'auto',
            modal: true
        });

        function destroyResolveImportingReportsConflictsDialog()
        {
            $("#reportsDuplicatesResolvingAccept").unbind("click");
            $("#reportsDuplicatesResolvingCancel").unbind("click");
            
            $("#reportsDuplicatesResolvingGridContainer").empty();
        }

        function searchReportsDuplicatesResolvingItem(reportID)
        {
            for (var index = 0; index < reportsDuplicatesResolvingData.length; index++)
            {
                var item = reportsDuplicatesResolvingData[index];
                if (item[0] == reportID) return item;
            }

            return null;
        }

        function renderReportTitle(value)
        {
            return String.format("<div class='importReportTitle'>{0}</div>", value);
        }

        function renderImportReportOption(value, meta, currentRecord, recordIndex, columnIndex)
        {
            var checked = value ? "checked='checked'" : "";

            return String.format("<input type='radio' name='reportID-{0}' class='importReportOption' reportID='{0}' columnIndex = '{1}' {2}' />",
                currentRecord.data.ReportID, columnIndex, checked);
        }

        var record = Ext.data.Record.create(
        [
            { name: 'ReportID' },
            { name: 'ReportTitle' },
            { name: 'Original' },
            { name: 'New' },
            { name: 'Both' }
        ]);

        var dataStoreReader = new Ext.data.ArrayReader({ idIndex: 0 }, record);

        var reportsDuplicatesResolvingDataSource = new Ext.data.Store(
        {
            reader: dataStoreReader,
            arraySource: reportsDuplicatesResolvingData,
            arrayParams: {},

            load: function (options)
            {
                if (!options) options = { params: { start: 0, limit: reportsDuplicatesResolvingGridPageSize} };

                options = Ext.apply({}, options);
                this.storeOptions(options);

                try
                {
                    this.arrayParams = options.params;
                    var records = this.reader.readRecords(this.arraySource.slice(options.params.start, options.params.start + options.params.limit));
                    this.loadRecords(records, {}, true);

                    return true;
                }
                catch (e)
                {
                    this.handleException(e);
                    return false;
                }
            },
            reload: function (options)
            {
                if (this.lastOptions.params.start >= this.getTotalCount())
                {
                    this.lastOptions.params.start = Math.max(0, this.getTotalCount() - this.lastOptions.params.limit);
                }

                this.load(Ext.applyIf(options || {}, this.lastOptions));
            },
            getTotalCount: function ()
            {
                return this.arraySource.length || 0;
            }
        });
        reportsDuplicatesResolvingDataSource.load();

        var attachImportReportOptionChangeHandler = function ()
        {
            $(".importReportOption").change(function ()
            {
                var reportID = $(this).attr("reportID");

                var item = searchReportsDuplicatesResolvingItem(reportID);
                if (item == null) return;

                item[2] = false;
                item[3] = false;
                item[4] = false;

                var columnIndex = parseInt($(this).attr("columnIndex"));

                if ((columnIndex) == 2) { //Showing the warning when user selected "New"
                    $(".importReportsWarning").show();
                } else { //Hide the warning when none of the report has new as selected
                    if (!($(".importReportOption:checked").is(".importReportOption:checked[columnIndex='2']")))
                        $(".importReportsWarning").hide();
                }
                item[columnIndex + 1] = true;

                reportsToImport.setImportingDuplicatedReportStrategy(reportID, columnIndex);
            });
        };

        var pagingToolbar = new Ext.PagingToolbar(
        {
            store: reportsDuplicatesResolvingDataSource,
            pageSize: reportsDuplicatesResolvingGridPageSize,

            listeners: {
                'change': function ()
                {
                    attachImportReportOptionChangeHandler();
                }
            },

            onLoad: function (store, r, o)
            {
                if (!this.rendered)
                {
                    this.dsLoaded = [store, r, o];
                    return;
                }

                var p = this.getParams();
                this.cursor = (this.store.arrayParams && this.store.arrayParams[p.start]) ? this.store.arrayParams[p.start] : 0;
                var d = this.getPageData(), ap = d.activePage, ps = d.pages;
                this.afterTextItem.setText(String.format(this.afterPageText, d.pages));
                this.inputItem.setValue(ap);

                this.first.setDisabled(ap == 1);
                this.prev.setDisabled(ap == 1);
                this.next.setDisabled(ap == ps);
                this.last.setDisabled(ap == ps);

                this.refresh.enable();
                this.updateInfo();

                this.fireEvent('change', this, d);
            }
        });

        var reportsDuplicatesResolvingGrid = new Ext.grid.GridPanel(
        {
            store: reportsDuplicatesResolvingDataSource,

            columns:
            [
                { header: '@{R=Core.Strings;K=RemoteReportsImport_ResolveDuplicates_ReportTitle;E=js}',
                    width: 200, sortable: true, dataIndex: 'ReportTitle', renderer: renderReportTitle
                },

                { header: '@{R=Core.Strings;K=RemoteReportsImport_ResolveDuplicates_Original;E=js}',
                    width: 50, align: 'center', sortable: true, dataIndex: 'Original', renderer: renderImportReportOption
                },

                { header: '@{R=Core.Strings;K=RemoteReportsImport_ResolveDuplicates_New;E=js}',
                    width: 50, align: 'center', sortable: true, dataIndex: 'New', renderer: renderImportReportOption
                },

                { header: '@{R=Core.Strings;K=RemoteReportsImport_ResolveDuplicates_Both;E=js}',
                    width: 50, align: 'center', sortable: true, dataIndex: 'Both', renderer: renderImportReportOption
                }
            ],

            viewConfig:
            {
                forceFit: true
            },

            height: 200,
            stripeRows: true,

            bbar: pagingToolbar,

            listeners: {
                'viewready': function ()
                {
                    attachImportReportOptionChangeHandler();
                }
            }
        });

        destroyResolveImportingReportsConflictsDialog();
        reportsDuplicatesResolvingGrid.render("reportsDuplicatesResolvingGridContainer");


        $("#reportsDuplicatesResolvingAccept").click(function ()
        {
            closeResolveImportingReportsConflictsDialog();

            callback();
        });

        $("#reportsDuplicatesResolvingCancel").click(function ()
        {
            closeResolveImportingReportsConflictsDialog();
        });

        $("#importRemoteReportsDuplicatesResolvingDialog").bind('dialogclose', function ()
        {
            destroyResolveImportingReportsConflictsDialog();
        });
    };
    var closeResolveImportingReportsConflictsDialog = function ()
    {
        $("#importRemoteReportsDuplicatesResolvingDialog").dialog('close');

        enableImportReportButtons();
    };


    var importReports = function ()
    {
        var serverID = reportsToImport.selectedOrionID();
        var importingReports = reportsToImport.items();

        var importReportsArguments =
        {
            serverID: serverID,
            importingReports: importingReports
        };

        SW.Core.Services.callWebService("/Orion/Services/ReportManager.asmx", "ImportReports", importReportsArguments, function (sessionID) {
            importRemoteReportsSessionID = sessionID;

            closeResolveImportingReportsConflictsDialog();
            displayImportRemoteReportsProgressDialog();
        });
    };

    var displayImportRemoteReportsProgressDialog = function ()
    {
        var updateImportRemoteReportsProgress = function (value)
        {
            if (value > 100) value = 100;

            var step = $("#importRemoteReportsProgressIndicator").width() / 100;
            $("#importRemoteReportsProgressIndicator img").width(value * step);
        };

        $("#importRemoteReportsProgressDialog").dialog(
        {
            resizable: false,
            width: 400,
            height: 'auto',
            modal: true
        });

        var progressImage = $("<img />").attr("src", "/Orion/Reports/Import/Images/progBar_on.gif").height(20);
        $("#importRemoteReportsProgressIndicator").append(progressImage);

        updateImportRemoteReportsProgressDetails(0);

        $("#importRemoteReportsProgressCancel").click(function ()
        {
            var importRemoteReportsProgressCancelArguments = { sessionID: importRemoteReportsSessionID };
            SW.Core.Services.callWebService("/Orion/Services/ReportManager.asmx", "CancelReportsImport", importRemoteReportsProgressCancelArguments, 
				function(result)
				{
					$("#importRemoteReportsProgressDialog").dialog('close');
				}			
			);
        });

        var intervalID = setInterval(function () {
            SW.Core.Services.callWebService("/Orion/Services/ReportManager.asmx", "GetImportReportsProgress", { sessionID: importRemoteReportsSessionID }, function (progress) {
                    progressChangedHandler(progress.SessionID, progress.Progress, progress.ImportedReportsCount);
                })
            },
            1000
        );

        var progressChangedHandler = function (sessionID, progress, importedReportsCount)
        {
            if (importRemoteReportsSessionID != sessionID) return;

            updateImportRemoteReportsProgress(progress);
            updateImportRemoteReportsProgressDetails(importedReportsCount);

            if (progress < 0 || progress >= 100)
            {
                clearInterval(intervalID);

                prepareImportReportsResultNotification(importedReportsCount);

                /// wait 1 second before closing progress dialog for user to see final results
                setTimeout(function () {
                    closeImportRemoteReportsProgressDialog();
                    closeImportReportsDialog();
                }, 1000);
            }
        };
    };

    var updateImportRemoteReportsProgressDetails = function (importedCount)
    {
        var importRemoteReportsProgressDetailsTemplate = "@{R=Core.Strings;K=RemoteReportsImport_ImportingReports_Progress;E=js}";
        var importRemoteReportsProgressDetails = String.format(importRemoteReportsProgressDetailsTemplate, importedCount, reportsToImport.items().length);

        $("#importRemoteReportsProgressDetails").text(importRemoteReportsProgressDetails);
    };
    var closeImportRemoteReportsProgressDialog = function ()
    {
        $("#importRemoteReportsProgressIndicator").empty();

        $("#importRemoteReportsProgressDialog").dialog('close');
    };

    var displayImportReportsDialog = function ()
    {
        // backup global variable value to not break search phrase highlighting in caller
        callerFilterText = filterText;
        var remoteReportsSelectionDialog = $("#importRemoteReportsDialog");

        remoteReportsSelectionDialog.dialog(
        {
            resizable: false,
            width: 'auto',
            height: 'auto',
            modal: true
        });

        remoteReportsSelectionDialog.bind('dialogclose', closeImportReportsDialogHandler);

        initialize();
    };
    var closeImportReportsDialog = function ()
    {
        // restore global variable value to not break search phrase highlighting in caller
        filterText = callerFilterText;
        enableImportReportButtons();
        
        $("#importRemoteReportsDialog").dialog('close');
    };
    var closeImportReportsDialogHandler = function ()
    {
        destroySolarwindsServersControl();
        destroyReportManagerGrid();
        destroyAccordion();
    };

    return {
        selectReportsDilog: function ()
        {
            displayImportReportsDialog();
        }
    };
}
