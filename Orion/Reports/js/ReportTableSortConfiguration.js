SW.Core = SW.Core || {};
SW.Core.TableResource = SW.Core.TableResource || {};
/* Object is resposible for managing dragable sort columns.
 * @param {Object} config
 * config.parentElId {String} id of the element into which will be rendered representation of dragable columns
 * config.columnOrderConfig {String} uniqueidentifier of hidden field into which will be serialized information about which columns was added to dragable representation
 * config.isGroupByConfiguration {bool}
 * config.chartMode {bool} true in the case, that it serve for chart config setting, false in the case for table.
 * and in which order and whis SortType was selected on individual added columns
*/
SW.Core.TableResource.ColumnOrder = function (config) {
    var self = this;
    var columnTemplate = ""; // template of how will look like individual added dragable column

    var availableColumns = []; // array of objects of type { FieldID: '', RefID: '', Caption: '' }

    var fieldPicker = null;

    /// <summary>
    /// Methods adds arrow between displayed sort dragable columns.
    /// </summary>
    var addArrowBetweenDragableElements = function () {
        $("#" + config.parentElId + " .arrow").remove();
        $("<div class=\"arrow\">@{R=Core.Strings; K=WEBJS_LK0_1; E=js}</div>").insertAfter("#" + config.parentElId + " .portletItem:not(.ui-sortable-placeholder)");
    };

    /// <summary>
    /// Method adds one display representation of dragable column by which user can specified order of column and sort by (Asc, Desc)
    /// </summary>
    /// <param name="fieldId">Uniqueidentifier of column which we will add.</param>
    /// <param name="caption">Title of the column which we will add.</param>
    /// <param name="refId">Uniqueidentifier of column detail</param>
    /// <param name="sortType">Type of sorting (Ascending, Descending)</param>
    var addColumn = function (fieldId, caption, refId, sortType, isHidden, isHTMLTagsAllowed) {
        if (typeof fieldId === "undefined" && refId === "selectItem") {
            return;
        }

        var data = {
            ColumnTitle: '<span style="white-space: nowrap">' + caption + '</span>'
        };

        var resTemplate = _.template(columnTemplate, data);
        var $newColumn = $(resTemplate).insertBefore("#" + config.parentElId + " div.addColumn");
        $newColumn.data("caption", caption);
        $newColumn.data("fieldid", fieldId);
        $newColumn.data("refid", refId);
        $newColumn.data("ishidden", isHidden);
        $newColumn.data("ishtmltagsallowed", isHTMLTagsAllowed);
        if (isIE7Browser()) // in IE7 is issue, that when css table width is 100%, that table overflow div and 100% is widht of all window
        {
            $newColumn.find("table").css({ "width": "70px" });
        }

        if (sortType == "Descending") {
            var $imgSortButton = $newColumn.find("img[data-name='sortButton']");
            $imgSortButton.attr("src", "/Orion/images/Reports/Arrow_Descending.png");
            $imgSortButton.toggleClass("sortAscending");
            $newColumn.find("span.helpfulText").text("@{R=Core.Strings; K=FILEDATA_AB0_7; E=js}");
        }

        $newColumn.on("click", function (event) { // changing sort type (Asc or Desc)
            event.preventDefault();
            event.stopPropagation();
            var $imgSortButton = $(this).find("img[data-name='sortButton']");
            if ($imgSortButton.hasClass("sortAscending")) {
                $imgSortButton.attr("src", "/Orion/images/Reports/Arrow_Descending.png");
                $newColumn.find("span.helpfulText").text("@{R=Core.Strings; K=FILEDATA_AB0_7; E=js}");
            } else {
                $imgSortButton.attr("src", "/Orion/images/Reports/Arrow_Ascending.png");
                $newColumn.find("span.helpfulText").text("@{R=Core.Strings; K=FILEDATA_AB0_8; E=js}");
            }

            $imgSortButton.toggleClass("sortAscending");
            storeColumnOrderConfigInfo();
        });

        $newColumn.find("img[data-name='deleteButton']").on("click", function (event) { // remove sort column
            event.preventDefault();
            event.stopPropagation();
            var $itemToRemove = $(this).closest(".portletItem");
            availableColumns.push({ FieldID: $itemToRemove.data("fieldid"), RefID: $itemToRemove.data("refid"), Caption: $itemToRemove.data("caption"), IsHidden: $itemToRemove.data("ishidden"), IsHTMLTagsAllowed: $itemToRemove.data("ishtmltagsallowed") });
            $itemToRemove.remove();
            fillComboBoxByListOfAvailableColumns();
            storeColumnOrderConfigInfo();
            addArrowBetweenDragableElements();
        });

        addArrowBetweenDragableElements();
        removeItemFromAvailableColumn(refId);
        fillComboBoxByListOfAvailableColumns();
        storeColumnOrderConfigInfo();
    };

    /// <summary>
    /// Method fills combobox by list of available column which is stored in inner property availableColumns.
    /// </summary>
    var fillComboBoxByListOfAvailableColumns = function () {
        var $columnList = $("#" + config.parentElId + " select.columnList");

        // into value we will store fieldId identifier
        $columnList.empty();
        $columnList.append($("<option></option>").val("selectItem").html("@{R=Core.Strings; K=WEBJS_PS0_28; E=js}"));
        for (var i = 0; i < availableColumns.length; i++) {
            if (config.isGroupByConfiguration && availableColumns[i].IsHidden != null && availableColumns[i].IsHidden.toString().toLowerCase() == "true")
                continue;
            var $optionToAdd = $("<option></option>").val(availableColumns[i].RefID).html(availableColumns[i].Caption);
            $optionToAdd.data("fieldid", availableColumns[i].FieldID);
            $columnList.append($optionToAdd);
        }
    };

    /// <summary>
    /// Method finds out whether browser is IE7
    /// </summary>
    /// <returns type="int">True in the case of IE7 browser; otherwise false.</returns>
    var isIE7Browser = function () {
        return $("html").hasClass("sw-is-msie-7");
    };

    /// <summary>
    /// Method removes column specified by uniqueidentifier fieldId from combo box and also from displayed dragable sort columns.
    /// </summary>
    /// <param name="refId" type="String">Uniqueidentifier of column which we will remove.</param>
    var removeItemFromAvailableColumn = function (refId) {
        var indexToRemove = -1;
        for (var i = 0; i < availableColumns.length; i++) {
            if (availableColumns[i].RefID == refId) {
                indexToRemove = i;
                break;
            }
        }

        if (indexToRemove > -1) {
            availableColumns.splice(indexToRemove, 1);
        }
    };

    /// <summary>
    /// Method stores into hidden field whose uniqueidentifier was passed in config object in property columnOrderConfig serialized JSON array of object
    /// with following properties: FieldID, Order, SortType.
    /// </summary>
    var storeColumnOrderConfigInfo = function () {
        var $columns = $("#" + config.parentElId + " div.portletItem");
        var columnsConfig = [];
        for (var i = 0; i < $columns.length; i++) {
            var sortType = $($columns[i]).find("img[data-name='sortButton']").hasClass("sortAscending") ? "Ascending" : "Descending";
            columnsConfig.push({ FieldID: $($columns[i]).data("fieldid"), RefID: $($columns[i]).data("refid"), Order: i, SortType: sortType, IsHidden: $($columns[i]).data("ishidden"), IsHTMLTagsAllowed: $($columns[i]).data("ishtmltagsallowed") });
        }

        $("#" + config.columnOrderConfig).val(JSON.stringify(columnsConfig));
    };
    
    /// <summary>
    /// This methods adds column into combobox and ensure, that column will be added just once by checking refId
    /// </summary>
    this.AddColumnIntoListOfAvailable = function (fieldId, caption, refId, ishidden, ishtmltagsallowed) {
        var exists = false;
        for (var i = 0; i < availableColumns.length; i++) {
            if (availableColumns[i].refID == refId) {
                exists = true;
            }
        }

        if (!exists) {
            var columnOrderConfigVal = $("#" + config.columnOrderConfig).val();
            if (columnOrderConfigVal && (columnOrderConfigVal != "")) {
                var columnsOrderConfig = JSON.parse(columnOrderConfigVal);
                for (var i = 0; i < columnsOrderConfig.length; i++) {
                    if (columnsOrderConfig[i].RefID == refId) {
                        exists = true;
                        break;
                    }
                }
            }
        }

        if (!exists) {
            availableColumns.push({ FieldID: fieldId, RefID: refId, Caption: caption, IsHidden: ishidden, IsHTMLTagsAllowed:ishtmltagsallowed });
            fillComboBoxByListOfAvailableColumns();
        }
    };
    
    
    /// <summary>
    /// Method adds one display representation of dragable column by which user can specified order of column and sort by (Asc, Desc)
    /// </summary>
    /// <param name="fieldId">Uniqueidentifier of column which we will add.</param>
    /// <param name="caption">Title of the column which we will add.</param>
    /// <param name="refId">Uniqueidentifier of column detail</param>
    /// <param name="sortType">Type of sorting (Ascending, Descending)</param>
    this.AddColumn = function (fieldId, caption, refId, sortType, isHidden, isHtmlTagsAllowed) {
        addColumn(fieldId, caption, refId, sortType, isHidden, isHtmlTagsAllowed);
    };

    /// <summary>
    /// Method initialize placeholder for dragable columns, that will allow us to drag and drop columns and also initialize combobox for choosing column to add.
    /// </summary>
    this.CreateDraggableColumns = function () {
        var $parentEl = $("#" + config.parentElId);
        $parentEl.sortable({
            axis: "x",
            items: ".portletItem",
            over: function (event, ui) {
                $("#" + config.parentElId + " .arrow").remove();
            },
            stop: function (event, ui) {
                addArrowBetweenDragableElements();
            },
            update: function (event, ui) {
                addArrowBetweenDragableElements();
                storeColumnOrderConfigInfo();
            }
        });

        addArrowBetweenDragableElements();

        var $columnList = $("#" + config.parentElId + " select.columnList");
        $columnList.on("change", function () {
            addingColumnAllowed = false;
            var selectedValue = $(this).val();
            var selectedCaption = $(this).find("option:selected").text();
            addColumn($(this).find("option:selected").data("fieldid"), selectedCaption, selectedValue, false);
        });
    };

    /// <summary>
    /// Method serve for removing column specified by fieldId from list of available column for sorting/grouping
    /// </summary>
    /// <param name="refId" type="String">Uniqueidentifier of the column which should be removed from available columns</param>
    this.DeleteColumn = function (refId) {
        var $elementsToDelete = $("#" + config.parentElId + " div.portletItem"); 
        for (var i = 0; i < $elementsToDelete.length; i++) {
            if ($($elementsToDelete[i]).data("refid") == refId) {
                $($elementsToDelete[i]).remove();
                break;
            }
        }

        removeItemFromAvailableColumn(refId);
        addArrowBetweenDragableElements();
        fillComboBoxByListOfAvailableColumns();
        storeColumnOrderConfigInfo();
    };

    /// <summary>
    /// Method serve for removing column from alreade used one as portlet
    /// </summary>
    this.DeletePortletItem = function (refId, caption, fieldId, isHidden, isHtmlTagsAllowed) {
        var $elementsToDelete = $("#" + config.parentElId + " div.portletItem");
        for (var i = 0; i < $elementsToDelete.length; i++) {
            if ($($elementsToDelete[i]).data("refid") == refId) {
                $($elementsToDelete[i]).remove();
                availableColumns.push({ FieldID: fieldId, RefID: refId, Caption: caption, IsHidden: isHidden, IsHTMLTagsAllowed: isHtmlTagsAllowed });
                break;
            }
        }

        addArrowBetweenDragableElements();
        fillComboBoxByListOfAvailableColumns();
        storeColumnOrderConfigInfo();
    };

    /// <summary>
    /// This method serve for changing title of the column in list of sort configuration columns.
    /// </summary>
    /// <param name="refId">Uniqueidentifier of the column (from column layout) whose caption we want to change.</param>
    /// <param name="caption">New title of the column.</param>
    /// <param name="fieldid">Uniqueidentifier of the column from field picker.</param>
    this.ChangeColumnCaption = function (refId, caption, fieldId, isHidden, isHtmlTagsAllowed) {
        for (var i = 0; i < availableColumns.length; i++) {
            if (availableColumns[i].RefID == refId) {
                availableColumns[i].Caption = caption;
                availableColumns[i].IsHidden = isHidden;
                availableColumns[i].IsHTMLTagsAllowed = isHtmlTagsAllowed;
                fillComboBoxByListOfAvailableColumns(); // we need to change caption in combobox
            }
        }

        var $columnsDisplayed = $("#" + config.parentElId + " div.portletItem");
        for (var i = 0; i < $columnsDisplayed.length; i++) {
            if ($($columnsDisplayed[i]).data("refid") == refId) {
                $($columnsDisplayed[i]).data("caption", caption);
                $($columnsDisplayed[i]).data("fieldid", fieldId);
                $($columnsDisplayed[i]).data("ishidden", isHidden);
                $($columnsDisplayed[i]).data("ishtmltagsallowed", isHtmlTagsAllowed);
                $($columnsDisplayed[i]).find("table td:eq(1)").text(caption);
                break;
            }
        }
    };

    /// <summary>
    /// Method fills combobox with list of available column by data passed in parameter dataToFill and store in inner property list of available columns
    /// </summary>
    /// <param name="dataToFill" type="Object">Array of objects with properties FieldId and Caption</param>
    /// <param name="includeSwissEntityNameIntoCaption" type="bool">True in the case, that we will add into property name "-" + entityName otherwise false</param>
    this.FillColumnList = function (dataToFill, includeSwissEntityNameIntoCaption) {
        availableColumns = [];
        var $columnList = $("#" + config.parentElId + " select.columnList");

        // into value we will store fieldId identifier
        $columnList.empty();
        $columnList.append($("<option></option>").val("selectItem").html("@{R=Core.Strings; K=WEBJS_PS0_28; E=js}"));
        if (jQuery.isArray(dataToFill)) {
            for (var i = 0; i < dataToFill.length; i++) {
                if (dataToFill[i].RefID != "00000000-0000-0000-0000-000000000000") {
                    var caption = dataToFill[i].Caption;
                    if (includeSwissEntityNameIntoCaption) {
                        if (dataToFill[i].CaptionWithSwisEntityIncluded) {
                            caption = dataToFill[i].CaptionWithSwisEntityIncluded;
                        }
                    }
                    
                    var $optionToAdd = $("<option></option>").val(dataToFill[i].RefID).html(caption).data("fieldid", dataToFill[i].FieldId);
                    $columnList.append($optionToAdd);
                    availableColumns.push({ FieldID: dataToFill[i].FieldId, RefID: dataToFill[i].RefID, Caption: caption, IsHidden: dataToFill[i].IsHidden, IsHTMLTagsAllowed: dataToFill[i].IsHTMLTagsAllowed });
                }
            }
        }

        fillComboBoxByListOfAvailableColumns();
    };

    /// <summary>
    /// This method initialized draggable columns by stored configuration in hidden field which identifier was passed in config object.
    /// </summary>
    this.InitializeSortColumnsByOrderColumnsConfig = function () {
        var $columns = $("#" + config.parentElId + " div.portletItem");
        if ($columns.length != 0) {
            return;
        }
        
        var strConfig = $("#" + config.columnOrderConfig).val();
        if (strConfig != "") {
            var initialConfig = JSON.parse(strConfig);
            // create associative array from availableColumns
            var ascAvColumns = [];
            for (var i = 0; i < availableColumns.length; i++) {
                ascAvColumns[availableColumns[i].RefID] = availableColumns[i];
            }

            for (var i = 0; i < initialConfig.length; i++) {
                if (!config.chartMode || ((typeof(config.chartMode)=="string") && eval(config.chartMode.toLowerCase())==false)) {
                    var addObj = ascAvColumns[initialConfig[i].RefID];
                    if (typeof(addObj) != "undefined") {
                        addColumn(addObj.FieldID, addObj.Caption, initialConfig[i].RefID, initialConfig[i].SortType, addObj.IsHidden, addObj.IsHTMLTagsAllowed);
                    }
                } else {
                    addColumn(initialConfig[i].FieldID, initialConfig[i].Caption, initialConfig[i].RefID, initialConfig[i].SortType, false);
                }
            }
        }
    };

    /// <summary>
    /// Method serve for specify how individual columns should look like.
    /// </summary>
    this.SetColumnTemplate = function (template) {
        columnTemplate = template;
    };

    this.SetFieldPicker = function(picker) {
        fieldPicker = picker;
        $("#" + config.parentElId + " .selectField").on("click", function (event) {
            event.preventDefault();
            event.stopPropagation();
            fieldPicker.ShowInDialog();
        });
    };
}; 
