﻿Ext.namespace('SW');
Ext.namespace('SW.Core');
Ext.namespace('SW.Core.Reports');

Ext.QuickTips.init();

SW.Core.namespace("SW.Core.Reports").ImportReportSourceSiteGrid = function (configuration)
{
    SW.Core.namespace("SW.Core.Reports").PageableEntitiesListWebApiStore = function (url, readerFields, sortOrderField) {
        var proxy = {};

        SW.Core.Reports.PageableEntitiesListWebApiStore.superclass.constructor.call(this, Ext.apply(proxy, {
            proxy: new Ext.data.HttpProxy({
                url: url,
                method: "POST",
                timeout: 900000,
                failure: function (xhr, options) {
                    SW.Core.Services.handleError(xhr);
                }
            }),
            reader: new Ext.data.JsonReader({
                totalProperty: "TotalRecordsCount",
                root: "Items"
            },
            readerFields
        ),

            remoteSort: true,
            sortInfo: {
                field: sortOrderField,
                direction: "ASC"
            }
        }));
    };

    Ext.extend(SW.Core.Reports.PageableEntitiesListWebApiStore, Ext.data.Store);

    var enabledSitesOnly = true;
    
    var gridHeight = typeof configuration.Height == "undefined" ?
        350 : configuration.Height;
    
    function selectedSiteChangeHandler()
    {
        if (typeof configuration.OnSelectedSiteChanged != "undefined")
        {
            configuration.OnSelectedSiteChanged();
        }
    }

    function renderGridColumn(value)
    {
        return $('<div/>').text(value).html();
    }

    function renderSiteName(value, meta, record)
    {
        var format = '<div><img id=statusImage_{3} src="/Orion/StatusIcon.ashx?size=small&amp;entity=Orion.Sites&amp;id={0}&amp;status={1}" /> {2}</div>';
        return String.format(format, record.data.ID, record.data.Status, value, record.data.ID);
    }

    var selectorModel;
    var dataStore;
    var grid;
    var pageSize;

    ORION.prefix = 'ImportReportSourceSiteGrid_';

    return {
        init: function()
        {
            pageSize = parseInt(ORION.Prefs.load('PageSize', '20'));

            selectorModel = new Ext.sw.grid.RadioSelectionModel();
            selectorModel.width = 25;
            selectorModel.hideable = false;
            
            selectorModel.on("selectionchange", selectedSiteChangeHandler);

            dataStore = new SW.Core.Reports.PageableEntitiesListWebApiStore(
                "/api/OrionServer/SearchOrionSites",
                [
                  { name: 'ID', mapping: 'SiteID' },
                  { name: 'SiteID', mapping: 'SiteID' },
                  { name: 'Name', mapping: 'Name' },
                  { name: 'Host', mapping: 'Host' },
                  { name: 'Website', mapping: 'Website' },
                  { name: 'Status', mapping: 'Status' }
                  
                ],
                "Name");

            var toolbarButtons = [];

            toolbarButtons.push('->');
            toolbarButtons.push(new Ext.ux.form.SearchField(
            {
                store: dataStore,
                width: 200,
                guid: configuration.SearchFieldID,
                listeners:
                {
                    searchStarting: function (data)
                    {
                        data.enabledOnly = enabledSitesOnly;
                    }
                }
            }));

            grid = new Ext.grid.GridPanel({
                store: dataStore,

                columns: [
                    selectorModel,
                    { header: '@{R=Core.Strings;K=ImportReportSourceSiteGrid_SiteName_ColumnHeader;E=js}', width: 200, sortable: true, dataIndex: 'Name', renderer: renderSiteName },
                    { header: '@{R=Core.Strings;K=ImportReportSourceSiteGrid_Hostname_ColumnHeader;E=js}', width: 200, sortable: true, dataIndex: 'Host', renderer: renderGridColumn },
                    { header: '@{R=Core.Strings;K=ImportReportSourceSiteGrid_Website_ColumnHeader;E=js}', width: 200, sortable: true, dataIndex: 'Website', renderer: renderGridColumn }
                ],

                sm: selectorModel,

                viewConfig: {
                    forceFit: true,
                    markDirty: false,
                    emptyText: '@{R=Core.Strings;K=ImportReportSourceSiteGrid_NoItemsDisplayedMessage;E=js}',
					loadMask :true
                },
				loadMask :true,
				height: gridHeight,
                width: 'auto',
                stripeRows: true,
                tbar: toolbarButtons,
				baseParams: { start: 0, limit: pageSize },

                // bottom toolbar
                bbar: new Ext.PagingToolbar(
                    {
                        store: dataStore,
                        pageSize: pageSize,
                        displayInfo: true,
                        displayMsg: "Displaying items {0} - {1} of {2}",//'<R=EOC.Strings;K=EOC_SolarWindsServers_DisplayingItemsToolbarStatus;E=js}',
                        emptyMsg: '@{R=Core.Strings;K=ImportReportSourceSiteGrid_NoItemsDisplayedMessage;E=js}'
                    })
            });

            grid.render(configuration.ContainerID);

			grid.store.on('beforeload', function (store, options) {
                options.params.limit = pageSize;
				if(!options.params.start)
				{
					options.params.start = 0;
				}
            });
		   
            grid.store.proxy.conn.jsonData = { search: "", enabledOnly: enabledSitesOnly };
            grid.store.load();

            selectedSiteChangeHandler();
        },
        
        getSelectedSite: function()
        {
            var selections = grid.getSelectionModel().getSelections();
            if (selections.length == 0) return null;

            return {
                ID: selections[0].data["SiteID"],
                Name: selections[0].data["Name"],
                Status: selections[0].data["Status"]
            };
        }
    };
};