﻿SW.Core = SW.Core || {};
SW.Core.TableResource = SW.Core.TableResource || {};
SW.Core.TableResource.SizeColumnTypeEnum = SW.Core.TableResource.SizeColumnTypeEnum || {
    Automatic: 0,
    Custom: 1
};

/* Object is responsible for adding/removing and reordering columns
 * @param {Object} config
 * config.parentTabId {String} uniqueidentifier of this resource
 * config.emptyColumnLayoutId {String} uniqueidentifier of template which should be displayed instead of tabbed column if no column was added.
 * config.columnLayoutConfig {String} uniqueidentifier of hidden field into which will be stored information about which column are added and which order
*/
SW.Core.TableResource.ReportTableColumnLayout = function (config) {
	var self = this;
	var tabs = undefined;
	var sortable;
	var tabContentTemplate = "";
	var existingTabs = []; // will hold info about order currently existing tabs

	var expandedOrCollapsedStatuses = []; // value true means expanded, false collapsed

	var lastSelectedTabIndex = 0;
	var lastSelectedTabUI = null;

	var totalTabCount = 0; // totoal count of tabs

	var tabNumber = 0;

	var fieldPicker = null;

	var modifiedTabOrder = false;

	SW.Core.Observer.makeObserver(self);

    SW.Core.TableResource.EventPublisher.SubscribeSumarizingModeChanged(sumarizingModeChanged);

	var adjustAddColumnWidth = function () {
		setTimeout(function () {
			var tabsWidth = $("#" + config.parentTabId).outerWidth();
			var $items = $("#" + config.parentTabId + " li");
			var itemsWidth = 0;
			for (var i = 0; i < $items.length; i++) {
				if (!$($items[i]).hasClass("addColumn")) {
					itemsWidth += $($items[i]).outerWidth() + 5.3;
				}
			}

			$("#" + config.parentTabId + " ul").css({ "background-position": itemsWidth + 40 + "px 4px", "background-repeat": "no-repeat", "background-size": "100% 57px" });
			if (isIEBrowser()) {
				$("#" + config.parentTabId + " ul").css({ "background-image": "url('/Orion/images/Reports/ColumnLayoutBackgroundIE.png')" });
				if (isIE9Browser()) {
					$("#" + config.parentTabId + " ul").css({ "background-position": itemsWidth + 40 + "px 3px" });
				}
			}

			$("#" + config.parentTabId + " ul").parent().css({ "width": $("html").width() - 50 + "px", "overflow-x": "auto", "overflow-y": "hidden" });
			if ($items.length > 1) {
				$("#" + config.parentTabId + " ul").parent().css({ "display": "inline-block" }); // needed otherwise overflow div in IE9 will expand automatically on hover, because has setted overflow property
			}

			$("#" + config.parentTabId + " ul").css({ "width": Math.max(itemsWidth + 105, $("html").width() - 50) + "px" });

			// start detection whether horizontal scrollbar is visible and if so for IE7 will be resolved issue with overlapping content by bottom scrollbar
			if (isIE7Browser()) {
				var $elementToDetect = $("#" + config.parentTabId);
				if ($elementToDetect[0].scrollWidth > $elementToDetect[0].clientWidth) { // horizontal scrollbar is present
					$elementToDetect.css({ "padding-bottom": "19px" }); // this will prevent overlaping content by bottom scrollbar
				} else {
					$elementToDetect.css({ "padding-bottom": "0px" }); // this will prevent overlaping content by bottom scrollbar
				}
			}

			// resizing displayed column detail by tabHeader (this will resolve situation especially when displayed detail was loaded for first time by code behind)
			var $detailsImgs = $("#" + config.parentTabId + " ul div.footerLessDetails img:eq(0)");
			if ($detailsImgs.length > 0) {
				var $img = $($detailsImgs[0]);
				var $detail = $("#" + $img.data("contentid"));
				resizeColumnDetailToWidthOfTabUi($detail);
			}
		}, 1);
	};

	var resizeColumnDetailToWidthOfTabUi = function ($detail) {
		var ulWidth = Math.max($("#" + config.parentTabId + " ul").width() - 39, $("#" + config.parentTabId + " ul").parent().width() - 40);

		if (!isIE7Browser()) {
			$detail.css({ "width": ulWidth });
		} else {
			$detail.css({ "width": ulWidth - 2 });
		}
	};

	var columnToggleDetail = function ($img) {
		var img = $img[0];
		var contentId = $img.data("contentid");
		var collapsed = $img.hasClass("collapsed");
		if (collapsed) {
			img.src = "/Orion/images/Button.Collapse.gif";
			var $detail = $("#" + contentId);
			$detail.show();
			$detail.find(".template").parent("div").show();
			resizeColumnDetailToWidthOfTabUi($detail);
			expandedOrCollapsedStatuses["#" + contentId] = true;
			$($img).parent().find("span").text("@{R=Core.Strings; K=WEBJS_PS0_29; E=js}");
			var $parentClosestLi = $($img).closest("li");
			$parentClosestLi.addClass("ui-state-active");
			$parentClosestLi.addClass("ui-state-selected");
			$parentClosestLi.find("div.header img:eq(0)").attr("src", "/Orion/images/Reports/GrabbyThing_White_16x16.png");
			$parentClosestLi.find("div.header .ui-icon-close").attr("src", "/Orion/images/Reports/delete_icon_White16x16.png");
		} else {
			img.src = "/Orion/images/Button.Expand.gif";
			var $detail = $("#" + contentId);
			$detail.hide();
			$detail.find(".template").parent("div").hide();
			expandedOrCollapsedStatuses["#" + contentId] = false;
			$($img).parent().find("span").text("@{R=Core.Strings; K=WEBJS_PS0_27; E=js}");
			var $parentClosestLi = $($img).closest("li");
			$parentClosestLi.removeClass("ui-state-active");
			$parentClosestLi.removeClass("ui-state-selected");
			$parentClosestLi.find("div.header img:eq(0)").attr("src", "/Orion/images/Reports/GrabbyThing.png");
			$parentClosestLi.find("div.header .ui-icon-close").attr("src", "/Orion/images/Reports/delete_icon16x16.png");
		}

		$img.parent().toggleClass("footerLessDetails");
		$img.closest("li").toggleClass("moreDetails");
		$img.toggleClass("collapsed");
		adjustAddColumnWidth();
		storeExistingTabs();
	};

	var columnToggleEventHandlerDiv = function (event) {
		var div = this;
		var $img = $(this).find("img");
		columnToggleDetail($img);
	};

	var closeTab = function (event) {
		event.preventDefault();
		event.stopPropagation(); // preventDefault and stopPropagation prevents changing url when user will close tab

		var selectedTab = self.GetSelectedTabIndex();
		var $tabToRemove = $(this).closest("li");
		var $tabToRemoveAEl = $tabToRemove.find("a");
		var refId = $tabToRemoveAEl.data("refid");
		var fieldId = $tabToRemoveAEl.data("fieldid");
		var fieldCaption = $tabToRemoveAEl.data("tabcaption");
		var columnDataType = $tabToRemoveAEl.data("columndatatype");
		var removingActiveTab = $tabToRemove.hasClass("ui-tabs-selected");
		var href = $tabToRemoveAEl.attr("href");
		var isColumnToRemoveStatistical = $tabToRemoveAEl.data("isstatistical");
		
      
	    $tabToRemove.remove();
		$(href).remove();

		var $lis = tabs.find("li");
		for (var i = 0; i < $lis.length; i++) {
			$($lis[i]).data("tabindex", i);
		}

		totalTabCount--;

		adjustAddColumnWidth();
		storeExistingTabs();
		tabs.tabs("refresh");
		solveVisibilityOfEmptyColumnLayout();
		self.trigger("columnRemoved", self, { FieldID: fieldId, RefID: refId, Caption: fieldCaption });

		setTimeout(function () {
			__doPostBack("closetab", isColumnToRemoveStatistical);
		}, 0);
	};

	/// <summary>
	/// Method retrieves number of tabs by internal state of jQuery tab plugin
	/// </summary>
	/// <returns type="int">Number of tabs which jQuery tabs plugin return </returns>
	var getTabCount = function () {
		return tabs.tabs("length");
	};

	/// <summary>
	/// Method retrieves number of tabs by computing from li elements. Unfortunately getTabCount method is also needed, because sometimes
	/// I will need to know how much tabs jQuery tab panel thinks that exists and sometimes real number which states number of li elements.
	/// E.g when I will call close method which removes tabs from list, jQuery tab plugin still thinks, that number is same and update will do after
	/// calling method AddNewTab, but in this method I need to know for adding new tab number of tabs which jQuery plugin thinks that exist.
	/// But when I need to disable moving tab after add button tab I conversely need to know real number of tabs which gives me this method.
	/// </summary>
	/// <returns type="int">Number of existing tabs </returns>
	var getTabCountComputedFromLiElements = function () {
		var tabCount = $("#" + config.parentTabId).find("li").length;
		return tabCount;
	};

	var getDisplayNameForField = function (field) {
		var displayName = "";
		if (typeof (field) != "undefined") {
			if (field.OwnerDisplayName != null) {
				displayName += field.OwnerDisplayName + "/" + field.DisplayName;
			} else {
				displayName = field.DisplayName;
			}
		}

		return displayName;
	};

	var getDataTypeForField = function (field) {
		if (field && field.DataTypeInfo && field.DataTypeInfo.DataType && field.DataTypeInfo.DataType.Data) {
			return field.DataTypeInfo.DataType.Data;
		}

		return "";
	};

	/// <summary>
	/// Method finds out whether browser is IE7
	/// </summary>
	/// <returns type="int">True in the case of IE7 browser; otherwise false.</returns>
	var isIE7Browser = function () {
		return $("html").hasClass("sw-is-msie-7");
	};

	var isIE9Browser = function () {
		return $("html").hasClass("sw-is-msie-9");
	};

	/// <summary>
	/// Method finds out whether browser is IE
	/// </summary>
	/// <returns type="int">True in the case of IE browser; otherwise false.</returns>
	var isIEBrowser = function () {
		return $("html").hasClass("sw-is-msie");
	};

	var recreateDragableColumns = function (selectedTab) {
		if (tabs) {
			tabs.tabs("destroy");
			tabs = null;
		}

		lastSelectedTabIndex = selectedTab;
		var countTab = getTabCountComputedFromLiElements();

		if (countTab > 1) {
			tabs = $("#" + config.parentTabId).tabs({ selected: selectedTab });
		} else {
			tabs = $("#" + config.parentTabId).tabs({ selected: null, unselect: true }); // this prevents selecting add Tab button in the case that no column haven't added yet
		}

		tabs.tabs("disable", getTabCount() - 1); // add column tab

		lastSelectedTabUI = tabs.find("img.ui-icon-close:eq(" + selectedTab + ")").closest("div.header");
		$(lastSelectedTabUI).find("img:eq(0)").attr("src", "/Orion/images/Reports/GrabbyThing_White_16x16.png");
		$(lastSelectedTabUI).find("img.ui-icon-close").attr("src", "/Orion/images/Reports/delete_icon_White16x16.png");

		tabs.bind("tabsselect", function (event, ui) {
			if ((lastSelectedTabUI != null) && ((ui.index != lastSelectedTabIndex) || modifiedTabOrder)) {
				$(lastSelectedTabUI).find("img:eq(0)").attr("src", "/Orion/images/Reports/GrabbyThing.png");
				$(lastSelectedTabUI).find("img.ui-icon-close").attr("src", "/Orion/images/Reports/delete_icon16x16.png");
				var $footerImg = $(lastSelectedTabUI).parent().find("div.footer img");
				var contentId = $footerImg.attr("src", "/Orion/images/Button.Expand.gif").addClass("collapsed").data("contentid");
				$footerImg.parent().find("span").text("@{R=Core.Strings; K=WEBJS_PS0_27; E=js}");
				$footerImg.parent().removeClass("footerLessDetails");
				$footerImg.closest("li").removeClass("moreDetails");
				var $detail = $("#" + contentId);
				$detail.hide();
				$detail.find(".template").parent("div").hide();
				expandedOrCollapsedStatuses["#" + contentId] = false;
			}

			if (!$(ui.tab).parent().hasClass("addColumn")) {
				var tabIndex = $(ui.tab).parent().data("tabindex");
				storeExistingTabs(tabIndex);
			} else {
				$(ui.tab).parent().addClass("ui-state-disabled");
				event.preventDefault();
				event.stopPropagation();
				return;
			}

			$(ui.tab).find("img:eq(0)").attr("src", "/Orion/images/Reports/GrabbyThing_White_16x16.png");
			$(ui.tab).find("img.ui-icon-close").attr("src", "/Orion/images/Reports/delete_icon_White16x16.png");
			lastSelectedTabUI = ui.tab;
		});

		solveVisibilityOfEmptyColumnLayout();
	};

	var setActiveTab = function (tabIndex) {
		var $liEl = tabs.find("li:eq(" + tabIndex + ")");
		if ($liEl.length == 0) {
			$liEl = tabs.find("li:eq(0)");
		}

		$liEl.find("a").trigger('click');
	};

	var storeExistingTabs = function (selectedIndexTab) {
		existingTabs = [];
		if (typeof (selectedIndexTab) == "undefined") {
			selectedIndexTab = self.GetSelectedTabIndex();
		}

		var $aArray = $(sortable.toArray()[0]).find("li a");
		for (var i = 0; i < $aArray.length; i++) {
			var isExpanded = false;
			if (expandedOrCollapsedStatuses[$aArray[i].hash]) {
				isExpanded = true;
			}

			var $tabElement = $($aArray[i]);
			existingTabs.push({
				refHref: $aArray[i].hash,
				caption: $tabElement.data("tabcaption"),
				captionWithSwisEntityIncluded: $tabElement.data("captionwithswisentityincluded"),
				refid: $tabElement.data("refid"),
				fieldid: $tabElement.data("fieldid"),
				datacolumnname: $tabElement.data("datacolumnname"),
				columndatatype: $tabElement.data("columndatatype"),
				transformid: $tabElement.data("transformid"),
				isexpanded: isExpanded,
				isselected: selectedIndexTab == i,
				ishidden: $tabElement.data("ishidden"),
				order: i,
				exist: true
			});
		}

		$("#" + config.columnLayoutConfig).val(JSON.stringify(existingTabs));
		lastSelectedTabIndex = selectedIndexTab;
	};

	var solveVisibilityOfEmptyColumnLayout = function () {
		if (getTabCountComputedFromLiElements() == 1) {
			tabs.css("display", "none");
			$("#" + config.emptyColumnLayoutId).css("display", "block");
		} else {
			tabs.css("display", "block");
			$("#" + config.emptyColumnLayoutId).css("display", "none");
		}
	};

	var convertDecimalSeparatorToServerForm = function (decimalNumberString) {
		if (typeof (decimalNumberString) == "undefined" || (decimalNumberString == ""))
			return decimalNumberString;

		var n = 1.1;
		var clientSeparator = n.toLocaleString().substring(1, 2);
		var serverSeparator = Sys.CultureInfo.CurrentCulture.numberFormat.CurrencyDecimalSeparator;
		return decimalNumberString.toLocaleString().replace(clientSeparator, serverSeparator);
	};

	var convertDecimalSeparatorToClientForm = function (decimalNumberString) {
		if (typeof (decimalNumberString) == "undefined" || (decimalNumberString == ""))
			return decimalNumberString;

		var n = 1.1;
		var clientSeparator = n.toString().substring(1, 2);
		var serverSeparator = Sys.CultureInfo.CurrentCulture.numberFormat.CurrencyDecimalSeparator;
		return parseFloat(decimalNumberString.toLocaleString().replace(serverSeparator, clientSeparator));
	};

	function sumarizingModeChanged(sender, args) {
	    
        // When sumarizing mode changed we set visibility (isHidden) for timestamp column. 
	    if (args.fieldId) {
	        var $aArray = $(sortable.toArray()[0]).find("li a");

	        var index = -1;

	        for (var i = 0; i < $aArray.length; i++) {
	            var $tabElement = $($aArray[i]);
	            if ($tabElement.data("fieldid") == args.fieldId) {
	                index = ++i;
	                break;
	            }
	        }

	        var columSearchString = config.parentTabId + "-column-" + index;

	        var defaultDisplayCheckbox = $("#" + columSearchString).find("[data-name='DefaultDisplay']");
	        defaultDisplayCheckbox.prop("checked", args.isEnabled ? "": "checked");

	        setHiddenStateForColumn( $("#" + config.parentTabId).find("a[href='#" + columSearchString + "']"), ! args.isEnabled);
	    }
	};

	function setHiddenStateForColumn($columnParentElement, isHidden) {
	    var hiddenColumnIconHolder = $columnParentElement.find(".hiddenColumnIconHolder");
	    hiddenColumnIconHolder.toggleClass("hidden-column", isHidden);

	    var headerDiv = $columnParentElement.find(".header");
	    headerDiv.toggleClass("ui-state-disabled", isHidden);

	}

	/// <summary>
	/// Method adds new tab to the end of tabs with collapsed content. Before calling this method is needed to call method SetTabContentTemplate.
	/// </summary>
	/// <param name="caption" type="String" >Specifies caption of the tab. </param>
	/// <param name="field" type="SolarWinds.Reporting.Models.Selection.Field">Serialized object of given code behind class</param>
    /// <param name="options" type="JSON">(Optional) Additional creating options.</param>
    /// <param name="options.isHidden" type="Bool">If is a new colum hidden by default.</param>
	this.AddNewTab = function (fieldId, caption, field, options) {
	    var defaults = {
	        isHidden: false,
	        isHTMLTagsAllowed : true
	    };

	    var dataOptions = $.extend(true, {}, defaults, options);
	    var tabCount = tabNumber++;

		var resTabContentTemplate = "";
		if (tabContentTemplate != "") {

            var data = {
				UniqueID: config.parentTabId,
				tabNumber: tabCount,
				detailid: config.parentTabId + "-column-" + tabCount,
				DatabaseColumnName: getDisplayNameForField(field),
				ColumnDataType: getDataTypeForField(field),
				DisplayNameValue: caption,
				DefaultDisplayValue: dataOptions.isHidden ? "checked" : '',
                AllowHTMLTagsValue : dataOptions.isHTMLTagsAllowed ? "checked" : '',
				ColumnWidthDynamicWidthChecked: 'checked',
				ColumnWidthCustomWidthChecked: '',
				FunctionNumericDataSelected: '',
				transformId: '',
				IsHidden: dataOptions.isHidden,
				ColumnWidthInPixelsValue: '',
				ColumnWidthInPercentsValue: '',
				AggregationStyle: ''
			};

			resTabContentTemplate = _.template(tabContentTemplate, data);
		}

        $("<div id=\"" + config.parentTabId + "-column-" + tabCount + "\" class=\"ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide\"><div style=\"display: none\">" + resTabContentTemplate + "</div></div>").appendTo(tabs).hide();

		var tabHeaderContent = "<div class=\"header\">";
		tabHeaderContent += "<table style=\"width: 100%; _width: 8em;\" cellspacing=\"0\" cellpadding=\2\" border=\"0\"><tbody>";
		tabHeaderContent += "<tr>";
		tabHeaderContent += "<td width=\"17px\"><img src=\"/Orion/images/Reports/GrabbyThing.png\" class=\"grabby\"></td>";
		tabHeaderContent += "<td class=\"ReportHeader\"><span style=\"white-space: nowrap\">" + caption + "</span></td>";
		tabHeaderContent += "<td width=\"17px\"><img src=\"/Orion/images/Reports/delete_icon16x16.png\" class=\"ui-icon-close\" /></td>";
		tabHeaderContent += "</tr>";
		tabHeaderContent += "<tbody></table>";
		tabHeaderContent += "</div>";
		tabHeaderContent += "<div class=\"footer\">";
		tabHeaderContent += "<img src=\"/Orion/images/Button.Expand.gif\" data-contentid=\"" + config.parentTabId + "-column-" + tabCount + "\" class=\"collapsed\" /><span>@{R=Core.Strings; K=WEBJS_PS0_27; E=js}</span></div>";
		var res = tabs.tabs("add", "#" + config.parentTabId + "-column-" + tabCount, tabHeaderContent, getTabCount() - 1);
		var $aEl = res.find("a[href=\"#" + config.parentTabId + "-column-" + tabCount + "\"]");
		$aEl.data("tabcaption", caption);
		var captionWithSwisEntityIncluded = caption;
		if (field.OwnerDisplayName && field.OwnerDisplayName != "") {
			captionWithSwisEntityIncluded = caption + " - " + field.OwnerDisplayName;
		}

		$aEl.data("captionwithswisentityincluded", captionWithSwisEntityIncluded);
		var refid = self.GenereteNewGuid();
		$aEl.data("refid", refid);
		$aEl.data("fieldid", fieldId);
		$aEl.data("datacolumnname", data.DatabaseColumnName);
		$aEl.data("columndatatype", data.ColumnDataType);
		$aEl.data("ishidden", data.IsHidden);
		$aEl.data("ishtmltagsallowed", data.isHTMLTagsAllowed);
		$aEl.parent().data("tabindex", totalTabCount);

		$aEl.find("img.ui-icon-close").on("click", closeTab);

		// by UX team we want to prevent select tab by clicking on the header
		$aEl.parent().find("div.header").on("click", function (event) {
			event.preventDefault();
			event.stopPropagation();
		});


		$aEl.find("div.footer").on("click", columnToggleEventHandlerDiv);
        		
		totalTabCount++;

		// updating data attribute tabindex (needed otherwise it will not work correctly between postback last added tab, because add tab adds span element)
		var $lis = $(res).find("li");
		for (var i = 0; i < $lis.length; i++) {
			$($lis[i]).data("tabindex", i);
		}

		tabs.find("li.addColumn").addClass("ui-state-disabled"); // we need to ensure, that tab plugin will not enable last add column for selected

		solveVisibilityOfEmptyColumnLayout();
		storeExistingTabs();
		adjustAddColumnWidth();

		setTimeout(function () {
			adjustAddColumnWidth();
		}, 2);

		self.trigger('columnAdded', self, { FieldID: fieldId, RefID: refid, Caption: caption, ColumnDataType: data.ColumnDataType, CaptionWithSwisEntityIncluded: captionWithSwisEntityIncluded, IsHidden: false, IsHTMLTagsAllowed: true });
	};

	/// <summary>
	/// This method generates new Guid identifier similarry as Guid.NewGuid() in .NET do.
	/// </summary>
	this.GenereteNewGuid = function () {
		var g1 = function () {
			return Math.floor((1 + Math.random()) * 0x10000)
             .toString(16)
             .substring(1);
		};

		return g1() + g1() + '-' + g1() + '-' + g1() + '-' + g1() + '-' + g1() + g1() + g1();
	};

	/// <summary>
	/// Method returns in array info about all column which are currently added into column layout editor control.
	/// </summary>
	/// <returns type="Array">Arrray of following objects: { refHref: {String}, caption: {String}, refid: {String}, fieldid: {String}, datacolumnname: {String}, columndatatype: {String}, isexpanded: {Bool}, isselected: {Bool}, order: {int}, exist: {Bool} } </returns>
	this.GetAllAddedTabs = function () {
		return _.clone(existingTabs);
	};

	/// <summary>
	/// Method modifies name of tab and data in tab detail specified uniqueidentifier in parameter tabIdentifier and data in parameter data
	/// </summary>
	/// <param name="tabIdentifier" type="String" />
	/// Uniqueidentifier of tab which we will modify.
	/// </param>
	/// <param name="data" type="Object" />
	/// Object which encapsulates data which will be used for modify tab. Object has properties DisplayNameValue {String} and FieldID {String}. 
	/// </param>
	this.ModifyTabDetailAndHeaderByData = function (tabIdentifier, data) {
		var $header = $("#" + config.parentTabId).find("a[href='#" + tabIdentifier + "'] div.header");
		var $parentOfHeader = $header.parent();
		if (($parentOfHeader[0].outerHTML != null) && ($parentOfHeader[0].outerHTML.indexOf("<span") == 0)) {
			$parentOfHeader = $parentOfHeader.parent();
		} else if (!$parentOfHeader[0].outerHTML && $parentOfHeader[0].tagName && ($parentOfHeader[0].tagName.toUpperCase() == "SPAN")) {
			$parentOfHeader = $parentOfHeader.parent();
		}

		var refid = $parentOfHeader.data("refid");
		var fieldid = $parentOfHeader.data("fieldid");
		$parentOfHeader.data("tabcaption", data.DisplayNameValue);
		$parentOfHeader.data("ishidden", data.IsHidden);
		var captionWithSwisEntityIncluded = data.DisplayNameValue;
		if (data.Field.OwnerDisplayName && data.Field.OwnerDisplayName != "") {
			captionWithSwisEntityIncluded = data.DisplayNameValue + " - " + data.Field.OwnerDisplayName;
		}

		$parentOfHeader.data("captionwithswisentityincluded", captionWithSwisEntityIncluded);
		$parentOfHeader.data("fieldid", data.FieldID);
		var displayNameForField = getDisplayNameForField(data.Field);
		$parentOfHeader.data("datacolumnname", displayNameForField);
		$header.find(".ReportHeader span").text(data.DisplayNameValue);
		var $tabDetail = $("#" + tabIdentifier);
		$tabDetail.find("[data-name='DisplayName']").val(data.DisplayNameValue);
		$tabDetail.find("span.databaseColumnName").text(displayNameForField);

		storeExistingTabs();
		adjustAddColumnWidth(); // needed because by changing title is changed width of column
		self.trigger('columnChanged', self, { PreviousFieldID: fieldid, NewFieldID: data.FieldID, RefID: refid, Caption: data.DisplayNameValue, CaptionWithSwisEntityIncluded: captionWithSwisEntityIncluded });
	};

	/// <summary>
	/// Method is reponsible for setting column width of currently selected column by column width specified in passed array
	/// </summary>
	this.SetColumnWidthForSelectedTab = function (columnsSizeInPixels, columnsSizeInPercentage) {
		var selectedColumnIndex = self.GetSelectedTabIndex();
		if (columnsSizeInPixels.length > selectedColumnIndex) {
			var width = columnsSizeInPixels[selectedColumnIndex];

			if (existingTabs.length > selectedColumnIndex) {
				var $detailContent = $(existingTabs[selectedColumnIndex].refHref);
				var $columnWidthInput = $detailContent.find("[data-name='ColumnWidthInPixels']");
				$columnWidthInput.val(width);
			}
		}
	};

	/// <summary>
	/// Method is responsible for setting column width for each column which width was modified on Report Preview dialog
	/// </summary>
    /// <param name="columnSizesInfo" type="Object">Following object: { ColumnsSizeInPixels: {Array}, ColumnsSizeInPercentage: {Array}, ColumnsSizeType: {Array of SizeColumnTypeEnum}, ColumnsSizeRefId: {Array of uniqueidentifier of individual columns} }</param>
	this.UpdateColumnWidth = function (columnSizesInfo) {
	    // First we set all columns to dynamic (reseting state of columns)
	    for (var tI = 0; tI < existingTabs.length; tI++) {
	        var $detailC = $(existingTabs[tI].refHref);
	        $detailC.find(":radio[value=FixedWidth]").prop("checked", "");
	        $detailC.find(":radio[value=DynamicWidth]").prop("checked", "checked");
	        $detailC.find("[data-name='ColumnWidthInPercents']").val("");
	    }

        // Here we will set appropriate width for columns
	    for (var i = 0; i < columnSizesInfo.ColumnsSizeType.length; i++) {
	        var $detailContent = null; 

	        // find proper detail content by refid
		    var refIdToFound = columnSizesInfo.ColumnsSizeRefId[i];
		    if (typeof(refIdToFound) == "undefined") {
		        continue;
		    }
	       		   
		    var foundColIndex = SW.Core.TableResource.ReportTableHelperUtils.FindColumn(existingTabs, refIdToFound);
	        if (foundColIndex > -1) {
	            $detailContent = $(existingTabs[foundColIndex].refHref);
	        }

	        if ($detailContent == null) {
	            continue;
	        }

	        var $fixedWidthInput = $detailContent.find(":radio[value=FixedWidth]");
			var $dynamicWidthInput = $detailContent.find(":radio[value=DynamicWidth]");
			if (columnSizesInfo.ColumnsSizeType[i] == SW.Core.TableResource.SizeColumnTypeEnum.Custom) {
				$dynamicWidthInput.prop("checked", "");
				$fixedWidthInput.prop("checked", "checked");
				var $columnWidthInputPercent = $detailContent.find("[data-name='ColumnWidthInPercents']");
				$columnWidthInputPercent.val(convertDecimalSeparatorToServerForm(columnSizesInfo.ColumnsSizeInPercentage[i]));
			} else {
				$dynamicWidthInput.prop("checked", "checked");
				$fixedWidthInput.prop("checked", "");
			}
		}
	};

	this.GetColumnsWidthInfo = function () {
		/// <summary>Method is responsible for getting info about setted column width for all columns</summary>
		/// <returns type="Array">Array of following objects: {refid: {guid}, colIndex: {numeric}, colWidthType: {numeric 0 or 1} colWidth: {numeric} } </returns>
		var colWidthInfo = [];
		for (var i = 0; i < existingTabs.length; i++) {
			if (existingTabs[i].refid == '00000000-0000-0000-0000-000000000000') continue;
			var $detailContent = $(existingTabs[i].refHref);
			var $dynamicWidthInput = $detailContent.find(":radio[value=DynamicWidth]");
			if ($dynamicWidthInput.prop("checked")) {
			    colWidthInfo.push({ refid: existingTabs[i].refid, colIndex: i, colWidthType: 0 });
			} else {
				var $columnPercentWidth = $detailContent.find("[data-name='ColumnWidthInPercents']");
				colWidthInfo.push({ refid: existingTabs[i].refid, colIndex: i, colWidthType: 1, colWidth: convertDecimalSeparatorToClientForm($columnPercentWidth.val()) });
			}
		}

		return colWidthInfo;
	};

	/// <summary>
	/// Method will create dragable columns with collapsible details.
	/// </summary>
	this.CreateDragableColumns = function () {
		var selectedTab = 0;
		var configObj = null;
		try {
			expandedOrCollapsedStatuses = [];
			var layoutConfigEl = document.getElementById(config.columnLayoutConfig);
			configObj = JSON.parse($(layoutConfigEl).val());
			if (configObj) {
				for (var i = 0; i < configObj.length; i++) {
					expandedOrCollapsedStatuses[configObj[i].refHref] = (configObj[i].isexpanded == true) || (configObj[i].isexpanded == "True");
					if ((configObj[i].isselected == true) || (configObj[i].isselected == "True")) {
						selectedTab = i;
					}
				}
			}
		} catch (e) {
		}

		recreateDragableColumns(selectedTab);

		sortable = tabs.find(".ui-tabs-nav").sortable({
			axis: "x",
			cancel: ".addColumn", // I don't want to be last tab sortable
			change: function (event, ui) {
				if ((ui.placeholder.index() + 1) == getTabCountComputedFromLiElements()) {
					ui.placeholder.removeClass("ui-state-hover");
					ui.placeholder.removeClass("ui-tabs-selected");
					ui.placeholder.removeClass("ui-state-active");
					$(ui.placeholder).css('background-image', 'url(/Orion/images/Reports/ColumnLayoutBackground.png)');
					$(ui.placeholder).css('margin-left', '-5px');
				} else if (!ui.placeholder.hasClass("ui-state-hover")) {
					ui.placeholder.addClass("ui-state-hover");
					ui.placeholder.addClass("ui-tabs-selected");
					ui.placeholder.addClass("ui-state-active");
					$(ui.placeholder).css('background-image', '');
					$(ui.placeholder).css('margin-left', '0px');
				}


			},
			start: function (event, ui) {
				$(this).attr('data-previndex', ui.item.index());

				ui.item.parent().parent()[0].onscroll = function (event) {
					ui.item.parent().parent()[0].scrollTop = 0;
				}; // this will prevent verticall scroll by catching event and sett back 0 value

			},
			stop: function (event, ui) {
				if ((ui.item.index() + 1) == getTabCountComputedFromLiElements()) {
					$(this).sortable('cancel');
				}

				ui.item.parent().parent()[0].scrollTop = 0; // this will prevent verticall scroll
			},
			update: function (event, ui) {
				var newIndex = ui.item.index();
				var prevIndex = $(this).attr('data-previndex');
				$(this).removeAttr('data-previndex');

				lastSelectedTabIndex = newIndex;

				// updating data attribute tabindex
				var $lis = $(this).find("li");
				for (var i = 0; i < $lis.length; i++) {
					$($lis[i]).data("tabindex", i);
				}

				modifiedTabOrder = true;
				storeExistingTabs();
			}
		});

		storeExistingTabs();

		var $content = $("#" + config.parentTabId);
		$content.find("img.ui-icon-close").on("click", closeTab);

		// by UX team we want to prevent select tab by clicking on the header
		$content.find("li div.header").on("click", function (event) {
			event.preventDefault();
			event.stopPropagation();
		});

		// $content.find("img.collapseTabContent-icon").on("click", columnToggleEventHandlerImg).removeClass("collapseTabContent-icon");
		$content.find("div.footer").on("click", columnToggleEventHandlerDiv).find("img.collapseTabContent-icon").removeClass("collapseTabContent-icon");

		$content.find("input:text[data-name='DisplayName']").on('blur', function () {
			var detailId = $(this).data("detailid");
			var detailFieldId = $("a[href=\"#" + detailId + "\"]").data('fieldid');
			if (detailId) {
				detailId = "#" + detailId;
			}

			// find appropriate record in existingTabs array
			var foundRecord = null;
			for (var tabIndex = 0; tabIndex < existingTabs.length; tabIndex++) {
				if (existingTabs[tabIndex].refHref == detailId) {
					foundRecord = existingTabs[tabIndex];
					break;
				}
			}

			if (foundRecord) {
				var arr = foundRecord.datacolumnname.split("/");
                var captionWithSwisEntityIncluded = Ext.util.Format.htmlEncode($(this).val());
				if (arr.length > 1) {
					captionWithSwisEntityIncluded = captionWithSwisEntityIncluded + " - " + arr[0];
				}

                self.trigger('columnChanged', self, { PreviousFieldID: foundRecord.fieldid, NewFieldID: foundRecord.fieldid, RefID: foundRecord.refid, Caption: $(this).val(), CaptionWithSwisEntityIncluded: captionWithSwisEntityIncluded }); //TODO:ishidden here
			}
		});

		$content.find("input:checkbox[data-name='DefaultDisplay']").on('change', function () {
            
            var detailId = $(this).data("detailid");
			var detailFieldId = $("a[href=\"#" + detailId + "\"]").data('fieldid');
			if (detailId) {
				detailId = "#" + detailId;
			}

			// find appropriate record in existingTabs array
			var foundRecord = null;
			for (var tabIndex = 0; tabIndex < existingTabs.length; tabIndex++) {
				if (existingTabs[tabIndex].refHref == detailId) {
					foundRecord = existingTabs[tabIndex];
					break;
				}
			}

			if (foundRecord) {
				var isHidden = $(this).is(':checked');
			    setHiddenStateForColumn($("a[href=\"" + detailId + "\"]"), isHidden);
				self.trigger('columnChanged', self, { PreviousFieldID: foundRecord.fieldid, NewFieldID: foundRecord.fieldid, RefID: foundRecord.refid, Caption: foundRecord.caption, CaptionWithSwisEntityIncluded: foundRecord.captionWithSwisEntityIncluded, IsHidden: isHidden });
			}
		});

		$content.find("input:checkbox[data-name='AllowHTMLTags']").on('change', function () {

		    var detailId = $(this).data("detailid");
		    var detailFieldId = $("a[href=\"#" + detailId + "\"]").data('fieldid');
		    if (detailId) {
		        detailId = "#" + detailId;
		    }

		    // find appropriate record in existingTabs array
		    var foundRecord = null;
		    for (var tabIndex = 0; tabIndex < existingTabs.length; tabIndex++) {
		        if (existingTabs[tabIndex].refHref == detailId) {
		            foundRecord = existingTabs[tabIndex];
		            break;
		        }
		    }

		    if (foundRecord) {
		        var isHTMLTagsAllowed = $(this).is(':checked');
		        setHiddenStateForColumn($("a[href=\"" + detailId + "\"]"), isHidden);
		        self.trigger('columnChanged', self, { PreviousFieldID: foundRecord.fieldid, NewFieldID: foundRecord.fieldid, RefID: foundRecord.refid, Caption: foundRecord.caption, CaptionWithSwisEntityIncluded: foundRecord.captionWithSwisEntityIncluded, IsHTMLTagsAllowed: isHTMLTagsAllowed });
		    }
		});

		tabNumber = totalTabCount = getTabCount();

		// per UX team discussion if no tabs was expanded no tabs should be selected
		var expandedTabsCount = $("#" + config.parentTabId + " ul li div.footerLessDetails").length;
		if (expandedTabsCount == 0) {
			var $selectedTab = $("#" + config.parentTabId + " ul li.ui-tabs-selected");
			$selectedTab.removeClass("ui-tabs-selected").removeClass("ui-state-active");
			$selectedTab.find("div.header img:eq(0)").attr("src", "/Orion/images/Reports/GrabbyThing.png");
			$selectedTab.find("img.ui-icon-close").attr("src", "/Orion/images/Reports/delete_icon16x16.png");
		}

		adjustAddColumnWidth();
		$(window).resize(function () {
			adjustAddColumnWidth();
		});

		var columns = [];
		for (var i = 0; i < configObj.length; i++) {
			columns.push({ FieldId: configObj[i].fieldid, RefID: configObj[i].refid, Caption: configObj[i].caption, ColumnDataType: configObj[i].columndatatype, CaptionWithSwisEntityIncluded: configObj[i].captionWithSwisEntityIncluded, IsHidden: configObj[i].ishidden });
		}

		self.trigger("columnsCreated", self, { Columns: columns });

		$(function () {
			setTimeout(function () {
				adjustAddColumnWidth();
				if (isIEBrowser()) {
					$("#" + config.parentTabId + " ul li.addColumn div.header").css({ "height": "1.3em", "padding-bottom": "3px" });
				}

				$("#" + config.parentTabId + " img.addNewTab-icon").attr("src", "/Orion/images/SubViewImages/Add.png");

				var configObj = null;
				try {
					expandedOrCollapsedStatuses = [];
					var layoutConfigEl = document.getElementById(config.columnLayoutConfig);
					configObj = JSON.parse($(layoutConfigEl).val());

					$.each(configObj, function (index, value) {
						var $columnDetail = $(this.refHref);
						var $placeholder = $columnDetail.find('.jsPresentersPlaceHolder');
						var $presenterContent = $(String.format('.jsTempTable[data-columnrefid="{0}"]', this.refid));
						var content = $presenterContent.children().detach();

						$placeholder.before(content);

						var toggleCustomFormatState = function (presenterId, funcForShow, funcForHide, ddlValue) {
							if (typeof (presenterId) == "undefined" || typeof (ddlValue) == "undefined")
								return;
							presenterId = presenterId + '_CustomFormatSection';
							if (ddlValue.toLowerCase() == 'customformat') {
								$('#' + presenterId)[funcForShow]();
							} else {
								$('#' + presenterId)[funcForHide]();
							}
						};

						var dateFormatListId = '#' + $(content)[0].id + '_DateFormatList';
						var dateFormatListVal = $(dateFormatListId).val();
						toggleCustomFormatState($(content)[0].id, 'show', 'hide', dateFormatListVal);
						$(dateFormatListId).change(function () {
							toggleCustomFormatState(this.id.split('_DateFormatList')[0], 'fadeIn', 'fadeOut', $(this).val());
						});
					});

				} catch (ex) { }
			}, 2);
		});
	};

	/// <summary>
	/// Method returns index (number) of currently selected (choosen) tab.
	/// </summary>
	/// <returns type="int" />
	this.GetSelectedTabIndex = function () {
		var selectedIndex = lastSelectedTabIndex;
		var $elements = $("#" + config.parentTabId + " ul li");
		for (var i = 0; i < $elements.length; i++) {
			if ($($elements[i]).hasClass("ui-tabs-selected")) {
				selectedIndex = i;
				break;
			}
		}

		return selectedIndex;
	};

	/// <summary>
	/// Method will return in serialized JSON format data representation of values entered into each column's detail.
	/// We will e.g be able to use this representation for transfering via web service to server.
	/// </summary>
	/// <returns type="String" />
	this.GetSerializedJSONDetailsValues = function () {
		var detailsValues = [];

		// JSON will have following format: { name: '', value: ''}
		var detailTables = $("#" + config.parentTabId + " table.template");
		for (var i = 0; i < detailTables.length; i++) {
			var $displayName = $(detailTables[i]).find("[data-name='DisplayName']");
			detailsValues.push({ name: $displayName[0].name, value: $displayName.val() });
			var $defaultDisplay = $(detailTables[i]).find("[data-name='DefaultDisplay']");
			detailsValues.push({ name: $defaultDisplay[0].name, value: $defaultDisplay.is(':checked') });
			var $alignment = $(detailTables[i]).find("[data-name='Alignment']");
			detailsValues.push({ name: $alignment[0].name, value: $alignment.val() });
			var $columnWidth = $(detailTables[i]).find("[data-name='ColumnWidth']");
			detailsValues.push({ name: $columnWidth[0].name, value: $columnWidth.val() });
			var $urlLink = $(detailTables[i]).find("[data-name='UrlLink']");
			detailsValues.push({ name: $urlLink[0].name, value: $urlLink.val() });
			var $dataFormat = $(detailTables[i]).find("[data-name='DataFormat']");
			detailsValues.push({ name: $dataFormat[0].name, value: $dataFormat.val() });
			var $formatString = $(detailTables[i]).find("[data-name='FormatString']");
			detailsValues.push({ name: $formatString[0].name, value: $formatString.val() });
			var $unitLabel = $(detailTables[i]).find("[data-name='UnitLabel']");
			detailsValues.push({ name: $unitLabel[0].name, value: $unitLabel.val() });
			var $function = $(detailTables[i]).find("[data-name='Function']");
			detailsValues.push({ name: $function[0].name, value: $function.val() });
			var $validRange = $(detailTables[i]).find("[data-name='ValidRange']");
			detailsValues.push({ name: $validRange[0].name, value: $validRange.val() });
			var $transformId = $(detailTables[i]).find("[data-name='TransformId']");
			detailsValues.push({ name: $transformId[0].name, value: $transformId.val() });
		}

		return JSON.stringify(detailsValues);
	};

	/// <summary>
	/// Method serve for specify which object will be used for display dialog box for choosing field which we will want's to add into list.
	/// </summary>
	this.SetFieldPicker = function (picker) {
		fieldPicker = picker;
	};

	/// <summary>
	/// By this method we specify template by which will be constructed each column detail.
	/// </summary>
	/// <param name="template" type="String">Template which specify visual representation of column detail.
	/// </param>
	this.SetTabContentTemplate = function (template) {
		tabContentTemplate = template;
	};
};
