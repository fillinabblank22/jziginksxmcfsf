﻿
$(function () {
    var footerCheckId = $("#hiddenFooterData").find('[id*="footerCheckData"]')[0].id;
    var creationDateId = $("#hiddenFooterData").find('[id*="creationDateData"]')[0].id;
    var customTextCheckId = $("#hiddenFooterData").find('[id*="customTextCheckData"]')[0].id;

    var footerTextAreaId = getFooterTextID();

    var footerCheck = $("#" + footerCheckId).val();
    var creationDate = $("#" + creationDateId).val();
    var customTextCheck = $("#" + customTextCheckId).val();

    var footerTextArea = $("#" + footerTextAreaId).val();

    $("#footerVisible").prop('checked', footerCheck === 'true');
    $("#creationDate").prop('checked', creationDate === 'true');
    $("#customText").prop('checked', customTextCheck === 'true');

    if (footerTextArea == "") {
        $("#" + footerTextAreaId).val(getDefaultFooterText());
    }
    else {
        $("#" + footerTextAreaId).val(footerTextArea);
    }

    $("#footerVisible").click(function () {
        if ($(this).is(':checked')) {
            $("#" + footerCheckId).val('true');
        }
        else {
            $("#" + footerCheckId).val('false');
        }
    });

    $("#creationDate").click(function () {
        if ($(this).is(':checked')) {
            $("#" + creationDateId).val('true');
            $("#footerVisible").prop('checked', 'true');
        }
        else {
            $("#" + creationDateId).val('false');
        }
    });

    $("#customText").click(function () {
        if ($(this).is(':checked')) {
            $("#" + customTextCheckId).val('true');
            $("#footerVisible").prop('checked', 'true');
            $("#" + footerTextAreaId).removeAttr('disabled');
        }
        else {
            $("#" + customTextCheckId).val('false');
            $("#" + footerTextAreaId).attr("disabled", "disabled");
        }
    });

    $("#" + footerTextAreaId).blur(function () {
        if ($(this).val() == "") {
            $("#" + footerTextAreaId).val(getDefaultFooterText());
        }
    });

    $("#" + footerTextAreaId).click(function () {
        if ($(this).val() == getDefaultFooterText()) {
            $(this).val("");
        }
    });
});

