﻿
$(function () {
    var defaultTitle = getDefaultTitle();
    var defaultSubtitle = getDefaultSubTitle();

    var titleTextId = getTitleID();
    var subtitleTextId = getSubTitleID();

    var titleText = $('#' + titleTextId).val();
    var subtitleText = $('#' + subtitleTextId).val();

    if (titleText == '') {
        $('#' + titleTextId).val(defaultTitle);
        $('#' + titleTextId).css('color', 'gray');
    }
    else {
        $('#' + titleTextId).val(titleText);
    }

    if (subtitleText == '') {
        $('#' + subtitleTextId).val(defaultSubtitle);
        $('#' + subtitleTextId).css('color', 'gray');
    }
    else {
        $('#' + subtitleTextId).val(subtitleText);
    }

    if ($("#isOrionDemoMode").length != 0) {
        $('#' + getFileUploadImage() + ' > div > input[type="file"]').css('display', 'none');
        $('#' + getBtnBrowse()).click(function () {
            demoAction("Core_Reporting_BrowseLogo", this);
            return false;
        });
    }

    $('#headerVisible').click(function () {
        if ($(this).is(':checked')) {
            $('#' + headerCheckId).val('true');
        }
        else {
            $('#' + headerCheckId).val('false');
        }
    });

    $('#' + titleTextId).blur(function () {
        if ($(this).val() == '') {
            $('#' + titleTextId).val(defaultTitle);
            $(this).css('color', 'gray');
        }
    });

    $('#' + titleTextId).focus(function () {
        if ($(this).val() == defaultTitle) {
            $(this).val('');
            $(this).css('color', 'black');
        }
    });

    $('#' + subtitleTextId).blur(function () {
        if ($(this).val() == '') {
            $('#' + subtitleTextId).val(defaultSubtitle);
            $(this).css('color', 'gray');
        }
    });

    $('#' + subtitleTextId).focus(function () {
        if ($(this).val() == defaultSubtitle) {
            $(this).val('');
            $(this).css('color', 'black');
        }
    });

    var browseWidth = $('#' + getBtnBrowse()).outerWidth();
    $('#browseWrap').width(browseWidth);

    var uploadInput = $('#' + getFileUploadImage() + ' > div > input[type="file"]');
    uploadInput.width(browseWidth);

    $('#browseWrap').mousemove(function (e) {
        $('#' + getFileUploadImage()).css({
            'right': $('#' + getBtnBrowse()).outerWidth() - (e.pageX - $('#' + getBtnBrowse()).offset().left) - 20
        });
    });
});

function uploadComplete() {
    $('#' + getCtnProcessDataID()).trigger('click');
    $('#' + getCheckLogoID()).prop('checked', true);
    $('#' + getThrobberUploadID()).css('display', 'none');
}

function checkExtension(sender, args) {
    var extentions = ['png', 'jpeg', 'jpg', 'gif', 'bmp'];

    var filename = args.get_fileName();
    var ext = filename.substring(filename.lastIndexOf(".") + 1);

    if ($.inArray(ext.toLowerCase(), extentions) <= -1) {
        $('#fileFormatError').css('display', 'block');

        throw new Error('@{R=Core.Strings;K=WEBJS_TM0_133;E=js}');

        return false;
    }
    $('#fileFormatError').css('display', 'none');
    $('#' + getThrobberUploadID()).css('display', 'block');
     $('#' + getFileUploadImage() + ' > div > input[type="file"]').css('display', 'none');
    $('#browseWrap').find('.sw-btn-t').css('color', 'silver');
    return true;
}
