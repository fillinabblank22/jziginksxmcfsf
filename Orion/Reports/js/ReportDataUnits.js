﻿(function(){

var pg = SW.Core.namespace('SW._pg');

pg.UnitsPopulate = function (domid, map, vMetric) {
    var sel = $('#' + domid), idx = -1;
    vMetric = vMetric || '';

    sel.find('option').remove();

    $.each(map, function (i, v) {
        sel.append($('<option>').attr({ value: v.value }).text(v.name));
        if( (v.value || '').toLowerCase() == vMetric.toLowerCase() )
            idx = i;
    });

    if (idx < 0) idx = 0;
    sel[0].selectedIndex = idx;
    return idx;
};
    
pg.UnitsRehydrate = function(domPrefix,vSuffixIn,vSuffixOut) {
  vSuffixIn = vSuffixIn || '';
  vSuffixOut = vSuffixOut || '';
  var map = pg.DataUnitMap,
      sel = $('#' + domPrefix + '_UnitPrefixId'),
      idx = sel[0].selectedIndex;

  var children = map[idx].children;
  pg.UnitsPopulate(domPrefix + '_UnitSuffixIn', children, vSuffixIn);
  children = children.slice();
  children.unshift({ name: '@{R=Core.Strings.3;K=WEBDATA_IB0_1;E=js}', value: '' });
  pg.UnitsPopulate(domPrefix + '_UnitSuffixOut', children, vSuffixOut);
};

pg.InitUnits = function (domPrefix, vMetric, vSuffixIn, vSuffixOut) {
  var map = pg.DataUnitMap;
  var idx = pg.UnitsPopulate(domPrefix + '_UnitPrefixId', map, vMetric);
  pg.UnitsRehydrate( domPrefix, vSuffixIn, vSuffixOut);
  
  var prefix = domPrefix;
  $('#' + domPrefix + '_UnitPrefixId').change(function () {
      pg.UnitsRehydrate(prefix);
  });
};

})();