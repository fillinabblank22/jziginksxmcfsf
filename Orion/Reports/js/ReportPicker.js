﻿Ext.namespace('SW');
Ext.namespace('SW.Orion');
Ext.QuickTips.init();
SW.Orion.ReportPicker = function () {
    var selectedReportsIds = new Array();
    var schComboArray;
    var pageSizeBox;
    var userPageSize;
    var schSelectorModel;
    var schGroupingValue;
    var dataStore;
    var schGrid;
    var schGroupGrid;
    var comboboxMaxWidth = 500;
    var noReportsSelectedElem = String.format("<span style='color:gray;'>{0}</span>", "@{R=Core.Strings;K=WEBJS_AB0_7; E=js}");
    var swisBasedReportsOnly;
    var isSingleSelectionMode = false;
    var resourceFilter;

    function getReportLink(record) {
        var reportLink;
        if (!record.data.LegacyPath) {
            reportLink = "/Orion/Report.aspx?ReportID=" + record.data.ReportID;
        } else {
            reportLink = "/Orion/Report.aspx?Report=" + encodeURIComponent(record.data.Name);
        }
        return reportLink;
    }

    function encodeHTML(value) {
        return Ext.util.Format.htmlEncode(value);
    }

    function stringToBoolean(value) {
        switch (value.toString().toLowerCase()) {
            case "true":
            case "yes":
            case "1":
                return true;
            case "false":
            case "no":
            case "0":
            case null:
                return false;
            default:
                return Boolean(value);
        }
    }

    function renderString(value) {
        if (!value || value.length === 0)
            return value;

        if (!filterText || filterText.length === 0)
            return encodeHTML(value);

        var patternText = filterText;

        // replace any %'s with a *
        patternText = patternText.replace(/\%/g, "*");

        // replace multiple *'s with a single instance
        patternText = patternText.replace(/\*{2,}/g, "*");

        // check if the search string is now just down to a single *, and if so return without any further regex
        if (patternText == '*') {
            return '<span style=\"background-color: #FFE89E\">' + encodeHTML(value) + '</span>';
        }

        // replace \ with \\
        patternText = patternText.replace(/\\/g, '\\\\');
        // replace . with \.
        patternText = patternText.replace(/\./g, '\\.');
        // replace * with .*
        patternText = patternText.replace(/\*/g, '.*');

        // set regex pattern
        var x = '((' + escapeRegExp(patternText) + ')+)\*';
        var content = value, pattern = new RegExp(x, "gi"), replaceWith = '{SPAN-START-MARKER}$1{SPAN-END-MARKER}';

        // do regex replace + content gets encoded
        var fieldValue = encodeHTML(content.replace(pattern, replaceWith));

        // now replace the literal SPAN markers with the HTML span tags
        fieldValue = fieldValue.replace(/{SPAN-START-MARKER}/g, '<span style=\"background-color: #FFE89E\">');
        fieldValue = fieldValue.replace(/{SPAN-END-MARKER}/g, '</span>');

        return fieldValue;
    }

    function escapeRegExp(str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }

    function renderFavorite(value) {
        if (stringToBoolean(value)) {
            return ('<a href="#"><img src="/Orion/images/Reports/Star-full.png"/></a>');
        } else {
            return ('<a href="#"><img src="/Orion/images/Reports/Star-empty.png"/></a>');
        }
    }

    function renderReportCategory(value) {
        if ((!value && value != false) || value == '') {
            value = "@{R=Core.Strings;K=Period_custom;E=js}";
        }
        if (value == "[All]") {
            value = "@{R=Core.Strings;K=WEBJS_SO0_31; E=js}";
        }
        return value;
    }

    function renderCategory(value) {
        if (value == "[All]") {
            return "@{R=Core.Strings;K=WEBJS_SO0_31; E=js}";
        }
        if (value == "Default Folder") {
            return "@{R=Core.Strings;K=WEBJS_SO0_55; E=js}";
        }
        return value ? value : "@{R=Core.Strings;K=WEBJS_RB0_3; E=js}";
    }

    function renderFavoriteText(value) {
        if (value == "[All]") {
            return "@{R=Core.Strings;K=WEBJS_SO0_31; E=js}";
        }
        return value ? '@{R=Core.Strings;K=WEBJS_SO0_72;E=js}' : '@{R=Core.Strings;K=WEBJS_SO0_73;E=js}';
    }

    function renderLegacyText(value) {
        if (value == "[All]") {
            return "@{R=Core.Strings;K=WEBJS_SO0_31; E=js}";
        }
        return value ? '@{R=Core.Strings;K=WEBJS_SO0_76;E=js}' : '@{R=Core.Strings;K=WEBJS_SO0_77;E=js}';
    }

    function renderProduct(value) {
        if ((!value && value != false) || value == '') {
            value = "@{R=Core.Strings;K=Period_custom;E=js}";
        }

        if (value == "[All]") {
            value = "@{R=Core.Strings;K=WEBJS_SO0_31; E=js}";
        }
        return value;
    }

    function renderGroup(value, meta, record) {
        var disp;
        if (value == '{empty}')
            value = '';

        if (schComboArray.getValue().indexOf('legacy') > -1) {
            disp = renderLegacyText(value) + " (" + record.data.Cnt + ")";
        } else if (schComboArray.getValue().indexOf('LimitationCategory') > -1) {
            disp = renderCategory(value) + " (" + record.data.Cnt + ")";
        } else if (schComboArray.getValue().indexOf('favorites') > -1) {
            disp = renderFavoriteText(value) + " (" + record.data.Cnt + ")";
        } else if (schComboArray.getValue().indexOf('ModuleTitle') > -1) {
            disp = renderProduct(value) + " (" + record.data.Cnt + ")";
        } else if (schComboArray.getValue().indexOf('Category') > -1) {
            disp = renderReportCategory(value) + " (" + record.data.Cnt + ")";
        } else {
            if (!value && value != false || value == '') {
                value = "@{R=Core.Strings;K=StatusDesc_Unknown;E=js}";
            }
            if (value == "[All]") {
                value = "@{R=Core.Strings;K=WEBJS_SO0_31; E=js}";
            }
            disp = value + " (" + record.data.Cnt + ")";
        }
        return '<span ext:qtitle="" ext:qtip="' + disp + '">' + encodeHTML(disp) + '</span>';
    }

    function renderEditName(value, metadata, record) {
        return String.format('<a target="blank" href="{1}">{0}</a>', renderString(value), getReportLink(record));
    }

    function getReportRowIndexById(id) {
        var rowIndex;
        for (var i = 0; i < schGrid.store.data.items.length; i++) {
            if (schGrid.getStore().getAt(i).data.ReportID == id) {
                rowIndex = i;
                return rowIndex;
            }
        }
        return null;
    }

    function addReportToSelection(id, name) {
        if (selectedReportsIds.length == 0) {
            $("#selectedObjects").empty();
        }

        if (isSingleSelectionMode) {
            selectedReportsIds = new Array();
            $("#selectedObjects").children().remove();
        }
        selectedReportsIds.push([id, name]);
        var reportName = Ext.util.Format.htmlEncode(name);
        $("#selectedObjects").append("<span class='selectedGridItem' dataRow=" + id + ">" + reportName + "<img class='removeObjectCross' src='/Orion/images/Reports/delete_netobject.png' style='cursor: pointer;margin-bottom: -3px; margin-left: 5px;'></span>");
        $("#selectedObjects span[dataRow=" + id + "] img").one('click', function () {
            var index = $(this).parent().attr("dataRow");
            removeReportFromSelection(index);
            refreshSelection();
            checkAllOrNoneSelection();

            if (isSingleSelectionMode) {
                $(".x-grid3-row-radio input:radio").removeAttr("checked");
            }
        });

        if (typeof SW.Orion.ReportPicker.onSelectionChanged === 'function') {
            SW.Orion.ReportPicker.onSelectionChanged(selectedReportsIds);
        }

    }

    function removeReportFromSelection(index) {
        var ind = $.grep(selectedReportsIds, function (n) { return n[0] == index; });
        for (var i = 0; i < ind.length; i++) {
            selectedReportsIds.splice(selectedReportsIds.indexOf(ind[i]), 1);
        }
        $("#selectedObjects span[dataRow=" + index + "]").remove();

        if (typeof SW.Orion.ReportPicker.onSelectionChanged === 'function') {
            SW.Orion.ReportPicker.onSelectionChanged(selectedReportsIds);
        }
    }

    function refreshSelection() {
        var containerId = "#selectedObjects";
        if (selectedReportsIds.length == 0) {
            $(containerId).children().remove();
            $(containerId).append(noReportsSelectedElem);
        }
        if (containerId == "#selectedObjects") {
            var rowsIds = new Array();
            $.each(selectedReportsIds, function () {
                rowsIds.push(getReportRowIndexById(this[0]));
            });
            schGrid.getSelectionModel().selectRows(rowsIds);
        }

        $(".selectedSchedulesLabel").text(String.format('@{R=Core.Strings;K=WEBJS_AB0_8; E=js}', selectedReportsIds.length));
    }

    function checkAllOrNoneSelection() {
        var count = 0;
        $.each(schGrid.getSelectionModel().selections.items, function () {
            var item = this;
            if ($.grep(schGrid.store.data.items, function (n) { return n.data.ReportID == item.data.ReportID; }).length == 1) {
                count++;
            }
        });
        if (schGrid.store.data.items.length == count && count != 0) {
            $("#checker").parent().addClass("x-grid3-hd-checker-on");
        } else {
            $("#checker").parent().removeClass("x-grid3-hd-checker-on");
        }
    }

    function refreshGridObjects(start, limit, elem, property, type, value, search, callback) {
        elem.store.removeAll();
        var tmpfilterText = search.replace(/\*/g, "%");
        if (tmpfilterText.length > 0) {
            if (!tmpfilterText.match("^%"))
                tmpfilterText = "%" + tmpfilterText;
            if (!tmpfilterText.match("%$"))
                tmpfilterText = tmpfilterText + "%";
        }

        elem.store.proxy.conn.jsonData = { property: property, type: type, value: value || "", search: tmpfilterText, swisBasedReportsOnly: swisBasedReportsOnly, resourceFilter: resourceFilter };

        if (limit)
            elem.store.load({ params: { start: start, limit: limit }, callback: callback });
        else
            elem.store.load({ callback: callback });
    }

    function resizeToFitContent(combo) {
        if (!combo.elMetrics) {
            combo.elMetrics = Ext.util.TextMetrics.createInstance(combo.getEl());
        }
        var m = combo.elMetrics, width = 0, el = combo.el, s = combo.getSize();
        combo.store.each(function (r) {
            var text = r.get(combo.displayField);
            width = Math.max(width, m.getWidth(text));
        }, combo);
        if (el) {
            width += el.getBorderWidth('lr');
            width += el.getPadding('lr');
        }
        if (combo.trigger) {
            width += combo.trigger.getWidth();
        }
        s.width = width + 10;
        var navPanel = Ext.getCmp('schNavPanel');
        if (combo.getWidth() < s.width) {
            combo.setSize(s);
            if (s.width <= comboboxMaxWidth) {
                navPanel.setWidth(s.width + 50);
            }
        }
        else
            if (combo.getWidth() > navPanel.getWidth()) {
                combo.setWidth(navPanel.getWidth() - 5);
            }
    }

    return {
        selectReportDialog: function () { },
        getSelectedReports: function () {
            var jsonResult = [];
            $.each(selectedReportsIds, function () {
                jsonResult.push({ "ID": this[0], "Title": this[1] });
            });
            return jsonResult;
        },
        setSelectedReports: function (reportsArray) {
            SW.Orion.ReportPicker.setSelectedReportsIds(reportsArray, null, null);
        },
        setSelectedReportsIds: function (reportsArray, controlUpdateFunction) {
            var containerId = "#selectedObjects";
            var result = [];
            $.each(reportsArray, function () {
                result.push([this.ID, this.Title]);
            });
            selectedReportsIds = result;
            if (!controlUpdateFunction) {
                $(containerId).empty();
                $.each(selectedReportsIds, function () {
                    var reportName = Ext.util.Format.htmlEncode(this[1]);
                    $(containerId).append("<span class='selectedGridItem' dataRow=" + this[0] + ">" + reportName + "<img class='removeObjectCross' src='/Orion/images/Reports/delete_netobject.png' style='cursor: pointer;margin-bottom: -3px; margin-left: 5px;'></span>");
                    $(containerId + " span[dataRow=" + this[0] + "] img").one('click', function () {
                        var index = $(this).parent().attr("dataRow");
                        removeReportFromSelection(index, containerId);
                        refreshSelection();
                    });
                });
                refreshSelection();
            } else {
                controlUpdateFunction(reportsArray);
            }
        },
        clearSelection: function () {
            selectedReportsIds = new Array();
            refreshSelection();
            if (isSingleSelectionMode) {
                $(".x-grid3-row-radio input:radio").removeAttr("checked");
            } else {
                $("#checker").parent().removeClass("x-grid3-hd-checker-on");
            }
        },
        init: function (configuration) {
            userPageSize = 16;
            schGroupingValue = ['legacy', 'null', '0'];


            if (configuration.NoReportsSelectedMessage) {
                noReportsSelectedElem = configuration.NoReportsSelectedMessage;
            }

            if (configuration.IsSingleSelectionMode) {
                schSelectorModel = new Ext.sw.grid.RadioSelectionModel();
                schSelectorModel.width = 25;
                schSelectorModel.hideable = false;
                isSingleSelectionMode = configuration.IsSingleSelectionMode;
            } else {
                schSelectorModel = new Ext.grid.CheckboxSelectionModel({ checkOnly: true, header: '<div id="checker" class="x-grid3-hd-checker">&nbsp;</div>' });
            }

            swisBasedReportsOnly = typeof configuration.SwisBasedReportsOnly == "undefined" ?
                false : configuration.SwisBasedReportsOnly;

            resourceFilter = typeof configuration.resourceFilter == "undefined" ?
                "" : configuration.resourceFilter;

            var schGridStoreColumns = [{ name: 'ReportID', mapping: 0 },
            { name: 'Name', mapping: 1 },
            { name: 'Favorite', mapping: 2 },
            { name: 'Title', mapping: 3 },
            { name: 'Description', mapping: 4 },
            { name: 'LegacyPath', mapping: 5 }
            ];

            var schGridColumnsModel = [schSelectorModel,
                { header: 'ReportID', width: 80, hidden: true, hideable: false, sortable: false, dataIndex: 'ReportID' },
                { id: 'Favorite', header: '<img src="/Orion/images/Reports/Star-empty.png"/>', width: 28, sortable: true, dataIndex: 'Favorite', renderer: renderFavorite },
                { header: '@{R=Core.Strings;K=WEBJS_IB0_40;E=js}', width: 300, sortable: true, hideable: false, dataIndex: 'Title', renderer: renderEditName },
                { id: 'Description', header: '@{R=Core.Strings;K=XMLDATA_SO0_28;E=js}', width: 250, sortable: true, dataIndex: 'Description', renderer: renderString }
            ];

            dataStore = new ORION.WebServiceStore("/Orion/Services/ReportManager.asmx/GetReports", schGridStoreColumns);
            dataStore.sortInfo = { field: "Title", direction: "Asc" };

            var schGroupingStore = new ORION.WebServiceStore(
                "/Orion/Services/ReportManager.asmx/GetReportGroupValues",
                [
                    { name: 'Value', mapping: 0 },
                    { name: 'DisplayNamePlural', mapping: 1 },
                    { name: 'Cnt', mapping: 2 }
                ],
                "Value", "");

            pageSizeBox = new Ext.form.NumberField({
                id: 'schPageSizeField',
                enableKeyEvents: true,
                allowNegative: false,
                width: 40,
                allowBlank: false,
                minValue: 1,
                maxValue: 100,
                value: userPageSize,
                listeners: {
                    scope: this,
                    'keydown': function (f, e) {
                        var k = e.getKey();
                        if (k == e.RETURN) {
                            e.stopEvent();
                            var v = parseInt(f.getValue());
                            if (!isNaN(v) && v > 0 && v <= 100) {
                                userPageSize = v;
                                schPagingToolbar.pageSize = userPageSize;
                                schPagingToolbar.doLoad(0);
                            } else {
                                Ext.Msg.show({
                                    title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                    msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                    icon: Ext.Msg.ERROR,
                                    buttons: Ext.Msg.OK
                                });
                                return;
                            }
                        }
                    },
                    'focus': function (field) {
                        field.el.dom.select();
                    },
                    'change': function (f, numbox, o) {
                        var pSize = parseInt(f.getValue());
                        if (isNaN(pSize) || pSize < 1 || pSize > 100) {
                            Ext.Msg.show({
                                title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                icon: Ext.Msg.ERROR,
                                buttons: Ext.Msg.OK
                            });
                            return;
                        }

                        if (schPagingToolbar.pageSize != pSize) { // update page size only if it is different
                            schPagingToolbar.pageSize = pSize;
                            userPageSize = pSize;
                            schPagingToolbar.doLoad(0);
                        }
                    }
                }
            });

            var schPagingToolbar = new Ext.PagingToolbar(
                {
                    id: 'schGridPaging',
                    store: dataStore,
                    pageSize: userPageSize,
                    displayInfo: true,
                    displayMsg: "@{R=Core.Strings;K=WEBJS_TM0_5; E=js}",
                    emptyMsg: "@{R=Core.Strings;K=WEBJS_SO0_54; E=js}",
                    beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                    afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                    firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                    prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                    nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                    lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                    refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}",
                    items: [
                        '-',
                        new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_VB0_46; E=js}', style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;' }),
                        pageSizeBox
                    ]
                }
            );

            var schGroupingDataStore = new ORION.WebServiceStore(
                "/Orion/Services/ReportManager.asmx/GetObjectGroupProperties",
                [
                    { name: 'Value', mapping: 0 },
                    { name: 'Name', mapping: 1 },
                    { name: 'Type', mapping: 2 }
                ]);

            schComboArray = new Ext.form.ComboBox(
                {
                    id: 'schGroupCombo',
                    mode: 'local',
                    boxMaxWidth: comboboxMaxWidth,
                    fieldLabel: 'Name',
                    displayField: 'Name',
                    valueField: 'Value',
                    store: schGroupingDataStore,
                    triggerAction: 'all',
                    value: '@{R=Core.Strings;K=WEBJS_VB0_76;E=js}',
                    typeAhead: true,
                    forceSelection: true,
                    selectOnFocus: false,
                    multiSelect: false,
                    editable: false,
                    listeners: {
                        'select': function () {
                            var val = this.store.data.items[this.selectedIndex].data.Value;
                            var type = this.store.data.items[this.selectedIndex].data.Type;

                            refreshGridObjects(null, null, schGroupGrid, val, type, "", filterText, function () {
                                var schGroupGridValue = "";
                                if (schGroupGrid.store.data.items.length > 0)
                                    schGroupGridValue = schGroupGrid.store.data.items[0].data[schGroupGrid.store.data.items[0].fields.keys[0]];
                                refreshGridObjects(0, userPageSize, schGrid, schGroupGridValue, "", "", filterText, function () {
                                    $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function () {
                                        this.tooltip = 'processed';
                                        $.swtooltip(this);
                                    });
                                });
                            });
                        }
                    }
                });

            schComboArray.getStore().on('load', function () {
                schComboArray.setValue(schGroupingValue[0]);
                refreshGridObjects(null, null, schGroupGrid, schGroupingValue[0], schGroupingValue[0], "", "");
                resizeToFitContent(schComboArray);
            });

            schGrid = new Ext.grid.GridPanel({
                id: 'schGrid',
                region: 'center',
                height: 500,
                viewConfig: { forceFit: false, emptyText: "@{R=Core.Strings;K=WEBJS_SO0_54;E=js}" },
                store: dataStore,
                baseParams: { start: 0, limit: userPageSize },
                stripeRows: true,
                trackMouseOver: false,
                columns: schGridColumnsModel,
                sm: schSelectorModel,
                autoScroll: true,
                autoExpandColumn: 'Description',
                loadMask: true,
                listeners: {
                    cellmousedown: function (grid, rowIndex, columnIndex, e) {
                        var fieldName = grid.getColumnModel().getDataIndex(columnIndex);
                        if (fieldName == "" || isSingleSelectionMode === true) {
                            var reportName = grid.getStore().getAt(rowIndex).data.Title;
                            var reportId = grid.getStore().getAt(rowIndex).data.ReportID;
                            if ($.grep(selectedReportsIds, function (n) { return n[0] == reportId; }).length == 0) {
                                addReportToSelection(reportId, reportName);
                            } else if (isSingleSelectionMode !== true) {
                                removeReportFromSelection(reportId);
                            }
                            refreshSelection();
                            checkAllOrNoneSelection();
                        }
                        return false;
                    }
                },
                bbar: schPagingToolbar,
                cls: 'panel-with-border'
            });

            schGrid.getColumnModel().on('hiddenchange', function () {
                var cols = '';
                for (var i = 1; i < schGrid.getColumnModel().getColumnCount(); i++) {
                    if (!schGrid.getColumnModel().isHidden(i)) {
                        cols += schGrid.getColumnModel().getDataIndex(i) + ',';
                    }
                }
            });

            schGrid.store.on('beforeload', function (store, options) {
                schGrid.getEl().mask('@{R=Core.Strings;K=WEBJS_VB0_1; E=js}', 'x-mask-loading');
                options.params.limit = userPageSize;
            });

            schGrid.store.on('load', function () {
                schGrid.getEl().unmask();
                refreshSelection();
                $("#checker").click(function () {
                    if (schGrid.getSelectionModel().selections.items.length == 0) {
                        $.each(schGrid.store.data.items, function () {
                            var item = this;
                            if ($.grep(selectedReportsIds, function (n) { return n[0] == item.data.ReportID; }).length != 0) {
                                removeReportFromSelection(item.data.ReportID);
                            }
                        });
                    } else {
                        $.each(schGrid.getSelectionModel().selections.items, function () {
                            var item = this;
                            if ($.grep(selectedReportsIds, function (n) { return n[0] == item.data.ReportID; }).length == 0) {
                                addReportToSelection(item.data.ReportID, item.data.Title);
                            }
                        });
                    }
                    refreshSelection();
                });
                setTimeout(function () {
                    refreshSelection();
                }, 0);
                checkAllOrNoneSelection();
            });

            schGrid.store.on("exception", function (dataProxy, type, action, options, response, arg) {
                var error = eval("(" + response.responseText + ")");
                Ext.Msg.show({
                    title: '@{R=Core.Strings;K=WEBJS_VB0_133; E=js}',
                    msg: error.Message,
                    icon: Ext.Msg.ERROR,
                    buttons: Ext.Msg.OK
                });
                schGrid.getEl().unmask();
            });

            schGroupGrid = new Ext.grid.GridPanel({
                id: 'schGroupingGrid',
                height: 450,
                store: schGroupingStore,
                cls: 'hide-header',
                columns: [
                    { id: 'Value', width: 193, editable: false, sortable: false, dataIndex: 'DisplayNamePlural', renderer: renderGroup }
                ],
                selModel: new Ext.grid.RowSelectionModel(),
                autoScroll: true,
                loadMask: true,
                listeners: {
                    cellclick: function (mygrid, row, cell, e) {
                        var val = mygrid.store.data.items[row].data[mygrid.store.data.items[row].fields.keys[cell]];
                        var type = "";
                        $.each(schComboArray.store.data.items, function (index, item) {
                            if (item.data.Value == schComboArray.getValue()) {
                                type = item.data.Type;
                                return false;
                            }
                        });
                        if (val == '') {
                            val = '{empty}';
                        }

                        if (val == null) {
                            val = "null";
                        }

                        if (val == '[All]')
                            val = '';
                        refreshGridObjects(0, userPageSize, schGrid, schComboArray.getValue(), type, val, filterText, function () {
                            $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function () {
                                this.tooltip = 'processed';
                                $.swtooltip(this);
                            });
                        });
                    }
                },
                anchor: '0 0',
                viewConfig: { forceFit: true },
                split: true,
                autoExpandColumn: 'Value'
            });

            schGroupGrid.store.on('load', function () {
                schGroupGrid.getSelectionModel().selectRow(schGroupingValue[2], false);
            });

            var schGroupByTopPanel = new Ext.Panel({
                id: 'schGroupByTopPanel',
                region: 'center',
                split: false,
                heigth: 50,
                collapsible: false,
                viewConfig: { forceFit: true },
                items: [new Ext.form.Label({ text: "@{R=Core.Strings;K=WEBJS_AK0_76; E=js}" }), schComboArray],
                cls: 'panel-no-border panel-bg-gradient'
            });

            var schNavPanel = new Ext.Panel({
                id: 'schNavPanel',
                width: 205,
                height: 400,
                region: 'west',
                split: false,
                anchor: '0 0',
                collapsible: false,
                viewConfig: { forceFit: true },
                items: [schGroupByTopPanel, schGroupGrid],
                cls: 'panel-with-border'
            });

            schNavPanel.on("bodyresize", function () {
                schGroupGrid.setWidth(schNavPanel.getSize().width);
            });

            var schMainGridPanel = new Ext.Panel({
                id: 'schMainGrid',
                region: 'center',
                height: 500,
                split: false,
                layout: 'border',
                collapsible: false,
                items: [schNavPanel, schGrid],
                cls: 'panel-no-border'
            });


            schMainGridPanel.render('sheduleReportsPicker');

            var initialSearchText = "@{R=Core.Strings;K=WEBJS_YP0_2; E=js}";
            var searchField = new Ext.Panel({
                id: 'searchFieldPanel',
                region: 'center',
                split: false,
                heigth: 50,
                collapsible: false,
                layout: 'table',
                layoutConfig: {
                    columns: 3
                },
                items: [new Ext.form.Label({ text: "@{R=Core.Strings;K=WEBJS_AB0_6; E=js}", width: 694, cls: "availableReportsLabel" }),
                new Ext.ux.form.SearchField({
                    id: 'schSearchField',
                    emptyText: initialSearchText,
                    store: dataStore,
                    width: 280,
                    listeners: {
                        searchStarting: function (data) {
                            data.swisBasedReportsOnly = swisBasedReportsOnly;
                            data.resourceFilter = resourceFilter;
                        }
                    }
                })
                ],
                cls: 'panel-no-border'
            });
            searchField.render('searchPanel');
            refreshGridObjects(null, null, schComboArray, "", "", "", "");
            refreshGridObjects(0, userPageSize, schGrid, schGroupingValue[0], "", "", filterText);
        }
    };
}();

