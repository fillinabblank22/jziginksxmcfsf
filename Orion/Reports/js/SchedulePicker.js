﻿Ext.namespace('SW');
Ext.namespace('SW.Orion');
Ext.QuickTips.init();
SW.Orion.SchedulePicker = function () {
    var selectedReportJobsIds = new Array();
    var selectedReportJobsInfo = new Array();
    var schComboArray;
    var pageSizeBox;
    var userPageSize;
    var selectorModel;
    var groupingValue;
    var dataStore;
    var schGrid;
    var schGroupGrid;
    var comboboxMaxWidth = 500;
    var noReportJobsSelectedElem = String.format("<span style='color:gray;'>{0}</span>", "@{R=Core.Strings;K=WEBJS_AB0_19; E=js}");

    function encodeHTML(value) {
        return Ext.util.Format.htmlEncode(value);
    }

    function escapeRegExp(str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }

    function renderString(value, meta, record) {
        if (!value || value.length === 0)
            return value;

        if (!filterText || filterText.length === 0)
            return encodeHTML(value);

        var patternText = filterText;

        // replace any %'s with a *
        patternText = patternText.replace(/\%/g, "*");

        // replace multiple *'s with a single instance
        patternText = patternText.replace(/\*{2,}/g, "*");

        // check if the search string is now just down to a single *, and if so return without any further regex
        if (patternText == '*') { return '<span style=\"background-color: #FFE89E\">' + encodeHTML(value) + '</span>'; }

        // replace \ with \\
        patternText = patternText.replace(/\\/g, '\\\\');
        // replace . with \.
        patternText = patternText.replace(/\./g, '\\.');
        // replace * with .*
        patternText = patternText.replace(/\*/g, '.*');

        // set regex pattern
        var x = '((' + escapeRegExp(patternText) + ')+)\*';
        var content = value, pattern = new RegExp(x, "gi"), replaceWith = '{SPAN-START-MARKER}$1{SPAN-END-MARKER}';

        // do regex replace + content gets encoded
        var fieldValue = encodeHTML(content.replace(pattern, replaceWith));

        // now replace the literal SPAN markers with the HTML span tags
        fieldValue = fieldValue.replace(/{SPAN-START-MARKER}/g, '<span style=\"background-color: #FFE89E\">');
        fieldValue = fieldValue.replace(/{SPAN-END-MARKER}/g, '</span>');

        return fieldValue;
    }

    function renderFrequencies(value) {
        if (value == null)
            return '';

        // adding tooltip to display full cell value
        return String.format('<span ext:qtitle="" ext:qtip="{0}">{1}</span>', encodeHTML(value), renderString(value));
    }

    function renderActions(value, meta, record) {
        value = record.data.ActionsData;
        if (value == null)
            return;
        var parsedActions = JSON.parse(value);
        var actionsPattern = '<span ext:qwidth="312" ext:qtitle="" ext:qtip="{0}">{1}</span>';
        var tipText = '';
        var resultString = '';
        if (parsedActions.length > 0) {
            tipText = String.format("<b>{0}</b><br/>{1}", encodeHTML(encodeHTML(parsedActions[0].Title)), parsedActions[0].Description);
            for (var i = 1; i < parsedActions.length; i++) {
                tipText = String.format('@{R=Core.Strings;K=WEBJS_TM0_146; E=js}', tipText, (String.format('<br/><b>{0}</b><br/>{1}', encodeHTML(encodeHTML(parsedActions[i].Title)), parsedActions[i].Description)));
            }
            resultString = String.format(actionsPattern, tipText, renderString(parsedActions[0].Title));
            for (i = 1; i < parsedActions.length; i++) {
                resultString = String.format('@{R=Core.Strings;K=WEBJS_TM0_146; E=js}', resultString, String.format(actionsPattern, tipText, renderString(parsedActions[i].Title)));
            }
        }
        return String.format('<span class="actionsColumn">{0}</span>', resultString);
    }

    function renderGroup(value, meta, record) {
        var disp;

        if (schComboArray.getValue().indexOf('Enabled') > -1) {
            disp = String.format('@{R=Core.Strings;K=WEBJS_TM0_145; E=js}', renderStatus(value), record.data.Cnt);
        } else {
            if (value == null) {
                value = "null";
            }
            if (value == "[All]") {
                value = "@{R=Core.Strings;K=WEBJS_SO0_31; E=js}";
            }
            disp = String.format('@{R=Core.Strings;K=WEBJS_TM0_145; E=js}', encodeHTML(value), record.data.Cnt);
        }
        return '<span ext:qtitle="" ext:qtip="' + disp + '">' + encodeHTML(disp) + '</span>';
    }

    function stringToBoolean(value) {
        switch (value.toString().toLowerCase()) {
            case "true": case "yes": case "1": return true;
            case "false": case "no": case "0": case null: return false;
            default: return Boolean(value);
        }
    }

    function renderStatus(status) {
        if (status == '[All]') return '@{R=Core.Strings;K=LIBCODE_TM0_15;E=js}';
        return stringToBoolean(status) ? '@{R=Core.Strings;K=WEBJS_TM0_144;E=js}' : '@{R=Core.Strings;K=LogLevel_off;E=js}';
    }

    function getReportJobRowIndexById(id) {
        var rowIndex;
        for (var i = 0; i < schGrid.store.data.items.length; i++) {
            if (schGrid.getStore().getAt(i).data.ReportJobID == id) {
                rowIndex = i;
                return rowIndex;
            }
        }
        return null;
    }

    function addReportJobToSelection(id, info) {
        if (selectedReportJobsIds.length == 0) {
            $("#selectedObjects").empty();
        }
        selectedReportJobsIds.push([id, info.Name]);
        if ($.grep(selectedReportJobsInfo, function (n) { return n[0] == id; }).length == 0) {
            selectedReportJobsInfo.push([id, info]);
        }
    }

    function removeReportJobFromSelection(index, containerId) {
        var ind = $.grep(selectedReportJobsIds, function (n) { return n[0] == index; });
        for (var i = 0; i < ind.length; i++) {
            selectedReportJobsIds.splice(selectedReportJobsIds.indexOf(ind[i]), 1);
        }
        var infoInd = $.grep(selectedReportJobsInfo, function (n) { return n[0] == index; });
        for (var i = 0; i < infoInd.length; i++) {
            selectedReportJobsInfo.splice(selectedReportJobsInfo.indexOf(infoInd[i]), 1);
        }
        if (containerId) {
            $(containerId + " span[dataRow=" + index + "]").remove();
        } else {
            $("#selectedObjects span[dataRow=" + index + "]").remove();
        }
    }

    function checkAllOrNoneSelection() {
        var count = 0;
        $.each(schGrid.getSelectionModel().selections.items, function () {
            var item = this;
            if ($.grep(schGrid.store.data.items, function (n) { return n.data.ReportJobID == item.data.ReportJobID; }).length == 1) {
                count++;
            }
        });
        if (schGrid.store.data.items.length == count && count != 0) {
            $("#checker").parent().addClass("x-grid3-hd-checker-on");
        } else {
            $("#checker").parent().removeClass("x-grid3-hd-checker-on");
        }
    }

    function refreshGridObjects(start, limit, elem, property, type, value, search, callback) {
        elem.store.removeAll();
        var tmpfilterText = search.replace(/\*/g, "%");
        if (tmpfilterText.length > 0) {
            if (!tmpfilterText.match("^%"))
                tmpfilterText = "%" + tmpfilterText;
            if (!tmpfilterText.match("%$"))
                tmpfilterText = tmpfilterText + "%";
        }
        elem.store.proxy.conn.jsonData = { property: property, type: type, value: value || "", search: tmpfilterText };

        if (limit)
            elem.store.load({ params: { start: start, limit: limit }, callback: callback });
        else
            elem.store.load({ callback: callback });
    }

    function resizeToFitContent(combo) {
        if (!combo.elMetrics) {
            combo.elMetrics = Ext.util.TextMetrics.createInstance(combo.getEl());
        }
        var m = combo.elMetrics, width = 0, el = combo.el, s = combo.getSize();
        combo.store.each(function (r) {
            var text = r.get(combo.displayField);
            width = Math.max(width, m.getWidth(text));
        }, combo);
        if (el) {
            width += el.getBorderWidth('lr');
            width += el.getPadding('lr');
        }
        if (combo.trigger) {
            width += combo.trigger.getWidth();
        }
        s.width = width + 10;
        var navPanel = Ext.getCmp('schNavPanel');
        if (combo.getWidth() < s.width) {
            combo.setSize(s);
            if (s.width <= comboboxMaxWidth) {
                navPanel.setWidth(s.width + 50);
            }
        }
        else
            if (combo.getWidth() > navPanel.getWidth()) {
                combo.setWidth(navPanel.getWidth() - 5);
            }
    }

    return {
        selectScheduleDialog: function () { },
        getSelectedReportJobs: function () {
            var jsonResult = [];
            for (var i = 0; i < selectedReportJobsIds.length; i++) {
                var item = selectedReportJobsIds[i];
                var array = $.grep(selectedReportJobsInfo, function (n) { return n[0] == item[0]; });
                $.each(array, function () {
                    jsonResult.push({ "ID": this[0], "Info": this[1] });
                });
            }
            selectedReportJobsInfo = new Array();
            $.each(jsonResult, function () {
                selectedReportJobsInfo.push(this.ID, this.Info);
            });
            return jsonResult;
        },
        setSelectedReportJobs: function (reportJobsArray) {
            SW.Orion.SchedulePicker.setSelectedReportJobsIds(reportJobsArray, null, null);
        },
        removeFromGridSelection: function (index) {
            removeReportJobFromSelection(index);
        },
        setSelectedReportJobsIds: function (reportJobsInfoArray, controlUpdateFunction) {
            var containerId = "#selectedObjects";
            var result = [];
            if (!controlUpdateFunction) {
                $.each(reportJobsInfoArray, function () {
                    result.push([this.ID, this.Title]);
                });
                selectedReportJobsIds = result;
                selectedReportJobsInfo = [];
                $.each(selectedReportJobsIds, function () {
                    selectedReportJobsInfo.push([this[0], {}]);
                });
                SW.Orion.SchedulePicker.refreshSelection(containerId);
            } else {
                selectedReportJobsIds = [];
                $.each(reportJobsInfoArray, function () {
                    selectedReportJobsIds.push([this.ID, this.Info.Name]);
                    result.push([this.ID, this.Info]);
                });
                selectedReportJobsInfo = result;
                controlUpdateFunction(reportJobsInfoArray);
            }

        },
        clearSelection: function () {
            selectedReportJobsIds = new Array();
            SW.Orion.SchedulePicker.refreshSelection();
            $("#checker").parent().removeClass("x-grid3-hd-checker-on");
        },
        refreshSelection: function (containerId) {
            if (!containerId) {
                containerId = "#selectedObjects";
            }
            if (selectedReportJobsIds.length == 0) {
                $(containerId).children().remove();
                $(containerId).append(noReportJobsSelectedElem);
            }
            if (containerId == "#selectedObjects") {
                var rowsIds = new Array();
                $.each(selectedReportJobsIds, function () {
                    rowsIds.push(getReportJobRowIndexById(this[0]));
                });

                schGrid.getSelectionModel().selectRows(rowsIds);

                $(containerId).empty();
                $.each(selectedReportJobsIds, function () {
                    var scheduleName = Ext.util.Format.htmlEncode(this[1]);
                    $(containerId).append("<span class='selectedGridItem' dataRow=" + this[0] + ">" + scheduleName + "<img class='removeObjectCross' src='/Orion/images/Reports/delete_netobject.png' style='cursor: pointer;margin-bottom: -3px; margin-left: 5px;'></span>");
                    $(containerId + " span[dataRow=" + this[0] + "] img").one('click', function () {
                        var index = $(this).parent().attr("dataRow");
                        removeReportJobFromSelection(index, containerId);
                        SW.Orion.SchedulePicker.refreshSelection();
                    });
                });
            }
            checkAllOrNoneSelection();
            $(".selectedSchedulesLabel").text(String.format('@{R=Core.Strings;K=WEBJS_AB0_16; E=js}', selectedReportJobsIds.length));
        },
        init: function () {
            userPageSize = 16;
            groupingValue = ['', '', ''];
            selectorModel = new Ext.grid.CheckboxSelectionModel({ checkOnly: true, header: '<div id="checker" class="x-grid3-hd-checker">&nbsp;</div>' });

            var schGridColumnsModel = [selectorModel,
                { header: 'ReportJobID', width: 80, hidden: true, hideable: false, sortable: false, dataIndex: 'ReportJobID' },
                { id: 'Name', header: '@{R=Core.Strings;K=WEBJS_AK0_9;E=js}', width: 175, sortable: true, hideable: false, dataIndex: 'Name', renderer: renderString },
                { id: 'Description', header: '@{R=Core.Strings;K=XMLDATA_SO0_28;E=js}', width: 200, sortable: true, dataIndex: 'Description', renderer: renderString },
                { header: '@{R=Core.Strings;K=WEBJS_TM0_150;E=js}', width: 175, sortable: true, dataIndex: 'FrequencyTitle', renderer: renderFrequencies },
                { header: '@{R=Core.Strings;K=PCDATA_VB1_5;E=js}', width: 175, sortable: true, dataIndex: 'ActionTitles', renderer: renderActions }
            ];

            dataStore = new ORION.WebServiceStore("/Orion/Services/ReportScheduler.asmx/GetReportJobs",
                [
                    { name: 'ReportJobID', mapping: 0 },
                    { name: 'Name', mapping: 1 },
                    { name: 'Description', mapping: 2 },
                    { name: 'FrequencyTitle', mapping: 5 },
                    { name: 'ActionsData', mapping: 6 },
                    { name: 'ActionTitles', mapping: 7 }
                ]);

            dataStore.sortInfo = { field: "Name", direction: "Asc" };

            var schGroupingStore = new ORION.WebServiceStore("/Orion/Services/ReportScheduler.asmx/GetSchedulesGroupValues",
                [
                    { name: 'Value', mapping: 0 },
                    { name: 'DisplayNamePlural', mapping: 1 },
                    { name: 'Cnt', mapping: 2 }
                ], "Value", "");

            pageSizeBox = new Ext.form.NumberField({
                id: 'schPageSizeField',
                enableKeyEvents: true,
                allowNegative: false,
                width: 40,
                allowBlank: false,
                minValue: 1,
                maxValue: 100,
                value: userPageSize,
                listeners: {
                    scope: this,
                    'keydown': function (f, e) {
                        var k = e.getKey();
                        if (k == e.RETURN) {
                            e.stopEvent();
                            var v = parseInt(f.getValue());
                            if (!isNaN(v) && v > 0 && v <= 100) {
                                userPageSize = v;
                                schPagingToolbar.pageSize = userPageSize;
                                schPagingToolbar.doLoad(0);
                            } else {
                                Ext.Msg.show({
                                    title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                    msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                    icon: Ext.Msg.ERROR,
                                    buttons: Ext.Msg.OK
                                });
                                return;
                            }
                        }
                    },
                    'focus': function (field) {
                        field.el.dom.select();
                    },
                    'change': function (f, numbox, o) {
                        var pSize = parseInt(f.getValue());
                        if (isNaN(pSize) || pSize < 1 || pSize > 100) {
                            Ext.Msg.show({
                                title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                icon: Ext.Msg.ERROR,
                                buttons: Ext.Msg.OK
                            });
                            return;
                        }

                        if (schPagingToolbar.pageSize != pSize) { // update page size only if it is different
                            schPagingToolbar.pageSize = pSize;
                            userPageSize = pSize;
                            schPagingToolbar.doLoad(0);
                        }
                    }
                }
            });

            var schPagingToolbar = new Ext.PagingToolbar(
                {
                    id: "schPagingToolbar",
                    store: dataStore,
                    pageSize: userPageSize,
                    displayInfo: true,
                    displayMsg: "@{R=Core.Strings;K=WEBJS_TM0_5; E=js}",
                    emptyMsg: "@{R=Core.Strings;K=WEBJS_SO0_78; E=js}",
                    beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                    afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                    firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                    prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                    nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                    lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                    refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}",
                    items: [
                        '-',
                        new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_VB0_46; E=js}', style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;' }),
                        pageSizeBox
                    ]
                }
            );

            var schGroupingDataStore = new Ext.data.SimpleStore({
                fields: ['Name', 'Value'],
                data: [['@{R=Core.Strings;K=WEBJS_VB0_76; E=js}', ''],
                ['@{R=Core.Strings;K=WEBJS_TD0_14;E=js}', 'Action'],
                ['@{R=Core.Strings;K=WEBJS_TM0_147;E=js}', 'ReportsData'],
                ['@{R=Core.Strings;K=WEBJS_AK0_6;E=js}', 'Enabled']]
            });

            schComboArray = new Ext.form.ComboBox(
                {
                    id: 'schGroupCombo',
                    mode: 'local',
                    boxMaxWidth: comboboxMaxWidth,
                    fieldLabel: 'Name',
                    width: 197,
                    displayField: 'Name',
                    valueField: 'Value',
                    store: schGroupingDataStore,
                    triggerAction: 'all',
                    value: '@{R=Core.Strings;K=WEBJS_VB0_76;E=js}',
                    typeAhead: true,
                    forceSelection: true,
                    selectOnFocus: false,
                    multiSelect: false,
                    editable: false,
                    listeners: {
                        'select': function () {
                            var val = this.store.data.items[this.selectedIndex].data.Value;

                            refreshGridObjects(null, null, schGroupGrid, val, "", "", filterText);

                            if (val == '') {
                                refreshGridObjects(0, userPageSize, schGrid, "", "", "", filterText, function () {
                                    $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function () {
                                        this.tooltip = 'processed';
                                        $.swtooltip(this);
                                    });
                                });
                            }
                        }
                    }
                });

            schComboArray.getStore().on('load', function () {
                schComboArray.setValue(groupingValue[0]);

                refreshGridObjects(null, null, schGroupGrid, groupingValue[0], groupingValue[0], "", "");
                resizeToFitContent(schComboArray);
            });

            schGrid = new Ext.grid.GridPanel({
                id: 'schGrid',
                region: 'center',
                height: 400,
                viewConfig: { forceFit: false, emptyText: "@{R=Core.Strings;K=WEBJS_SO0_78;E=js}" },
                store: dataStore,
                baseParams: { start: 0, limit: userPageSize },
                stripeRows: true,
                trackMouseOver: false,
                columns: schGridColumnsModel,
                sm: selectorModel,
                autoScroll: true,
                autoExpandColumn: 'Description',
                loadMask: true,
                listeners: {
                    cellmousedown: function (grid, rowIndex, columnIndex, e) {
                        var fieldName = grid.getColumnModel().getDataIndex(columnIndex);
                        if (fieldName == "") {
                            var selectedJob = grid.getStore().getAt(rowIndex);
                            var reportJobInfo = {
                                Name: selectedJob.data.Name,
                                Description: selectedJob.data.Description == null ? "" : selectedJob.data.Description,
                                ActionTitles: selectedJob.data.ActionTitles,
                                FrequencyTitle: selectedJob.data.FrequencyTitle,
                            };
                            var reportJobId = selectedJob.data.ReportJobID;
                            if ($.grep(selectedReportJobsIds, function (n) { return n[0] == reportJobId; }).length == 0) {
                                addReportJobToSelection(reportJobId, reportJobInfo);
                            } else {
                                removeReportJobFromSelection(reportJobId);
                            }
                            SW.Orion.SchedulePicker.refreshSelection();
                        }
                        return false;
                    }
                },
                bbar: schPagingToolbar,
                cls: 'panel-with-border'
            });

            schGrid.getColumnModel().on('hiddenchange', function () {
                var cols = '';
                for (var i = 1; i < schGrid.getColumnModel().getColumnCount(); i++) {
                    if (!schGrid.getColumnModel().isHidden(i)) {
                        cols += schGrid.getColumnModel().getDataIndex(i) + ',';
                    }
                }
            });

            schGrid.store.on('beforeload', function (store, options) {
                schGrid.getEl().mask('@{R=Core.Strings;K=WEBJS_VB0_1; E=js}', 'x-mask-loading');
                options.params.limit = userPageSize;
            });

            schGrid.store.on('load', function () {
                schGrid.getEl().unmask();
                SW.Orion.SchedulePicker.refreshSelection();
                $("#checker").click(function () {
                    if (schGrid.getSelectionModel().selections.items.length == 0) {
                        $.each(schGrid.store.data.items, function () {
                            var item = this;
                            if ($.grep(selectedReportJobsIds, function (n) { return n[0] == item.data.ReportJobID; }).length != 0) {
                                removeReportJobFromSelection(item.data.ReportJobID);
                            }
                        });
                    } else {
                        $.each(schGrid.getSelectionModel().selections.items, function () {
                            var item = this;
                            if ($.grep(selectedReportJobsIds, function (n) { return n[0] == item.data.ReportJobID; }).length == 0) {
                                var reportJobInfo = {
                                    Name: item.data.Name,
                                    Description: item.data.Description == null ? "" : item.data.Description,
                                    ActionTitles: item.data.ActionTitles,
                                    FrequencyTitle: item.data.FrequencyTitle,
                                };
                                addReportJobToSelection(item.data.ReportJobID, reportJobInfo);
                            }
                        });
                    }
                    SW.Orion.SchedulePicker.refreshSelection();
                });
                setTimeout(function () {
                    SW.Orion.SchedulePicker.refreshSelection();
                }, 0);
            });

            schGrid.store.on("exception", function (dataProxy, type, action, options, response, arg) {
                var error = eval("(" + response.responseText + ")");
                Ext.Msg.show({
                    title: '@{R=Core.Strings;K=WEBJS_VB0_133; E=js}',
                    msg: error.Message,
                    icon: Ext.Msg.ERROR,
                    buttons: Ext.Msg.OK
                });
                schGrid.getEl().unmask();
            });

            schGroupGrid = new Ext.grid.GridPanel({
                id: 'schGroupingGrid',
                height: 350,
                store: schGroupingStore,
                cls: 'hide-header',
                columns: [
                    { id: 'Value', width: 193, editable: false, sortable: false, dataIndex: 'DisplayNamePlural', renderer: renderGroup }
                ],
                selModel: new Ext.grid.RowSelectionModel(),
                autoScroll: true,
                loadMask: true,
                listeners: {
                    cellclick: function (mygrid, row, cell, e) {
                        var val = mygrid.store.data.items[row].data[mygrid.store.data.items[row].fields.keys[cell]];
                        var type = "";
                        $.each(schComboArray.store.data.items, function (index, item) {
                            if (item.data.Value == schComboArray.getValue()) {
                                type = item.data.Type;
                                return false;
                            }
                        });
                        if (val == '') {
                            val = '{empty}';
                        }

                        if (val == null) {
                            val = "null";
                        }

                        if (val == '[All]')
                            val = '';

                        refreshGridObjects(0, userPageSize, schGrid, schComboArray.store.data.items[schComboArray.selectedIndex].data.Value, "", val, filterText, function () {
                            $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function () {
                                this.tooltip = 'processed';
                                $.swtooltip(this);
                            });
                        });
                    }
                },
                anchor: '0 0',
                viewConfig: { forceFit: true },
                split: true,
                autoExpandColumn: 'Value'
            });

            schGroupGrid.store.on('load', function () {
                schGroupGrid.getSelectionModel().selectRow(groupingValue[2], false);
            });

            var schGroupByTopPanel = new Ext.Panel({
                id: 'schGroupByTopPanel',
                region: 'center',
                split: false,
                heigth: 50,
                collapsible: false,
                viewConfig: { forceFit: true },
                items: [new Ext.form.Label({ text: "@{R=Core.Strings;K=WEBJS_AK0_76; E=js}" }), schComboArray],
                cls: 'panel-no-border panel-bg-gradient'
            });

            var schNavPanel = new Ext.Panel({
                id: 'schNavPanel',
                width: 205,
                region: 'west',
                split: false,
                anchor: '0 0',
                collapsible: false,
                viewConfig: { forceFit: true },
                items: [schGroupByTopPanel, schGroupGrid],
                cls: 'panel-with-border'
            });

            schNavPanel.on("bodyresize", function () {
                schGroupGrid.setWidth(schNavPanel.getSize().width);
            });

            var schMainGridPanel = new Ext.Panel({
                id: 'schMainGrid',
                height: 500,
                region: 'center',
                split: false,
                layout: 'border',
                collapsible: false,
                items: [schNavPanel, schGrid],
                cls: 'panel-no-border'
            });

            schMainGridPanel.render('sheduleReportPicker');
            var initialSearchText = "@{R=Core.Strings;K=WEBJS_YP0_1; E=js}";
            var searchField = new Ext.Panel({
                id: 'searchBoxStyle',
                region: 'center',
                split: false,
                heigth: 50,
                collapsible: false,
                layout: 'table',
                layoutConfig: {
                    columns: 3
                },
                items: [new Ext.form.Label({ text: "@{R=Core.Strings;K=WEBJS_AB0_15; E=js}", width: 694, cls: "availableReportJobsLabel" }),
                new Ext.ux.form.SearchField({
                    id: 'schSearchField',
                    emptyText: initialSearchText,
                    store: dataStore,
                    width: 280
                })
                ],
                cls: 'panel-no-border '
            });
            searchField.render('searchPanel');
            refreshGridObjects(0, userPageSize, schGrid, groupingValue[0], "", groupingValue[1], filterText);
        }
    };
}();

