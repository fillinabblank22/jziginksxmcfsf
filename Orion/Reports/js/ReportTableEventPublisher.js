﻿SW.Core.namespace('SW.Core.TableResource');
//Javascript Singelton Object for sharing pub/sub events.
SW.Core.TableResource.EventPublisher = (function () {

    var timeFieldSelectionChanged = 'TableResource.TimeField.SelectionChanged';
    var timeRelativeSelectionChanged = 'TableResource.TimeRelativeSelection.SelectionChanged';
    var validationMessageRegistered = 'TableResource.ValidationMessageRegistered';
    var sumarizingModeChanged = 'TableResource.SumarizingModeChanged';

    return {
        SubscribeTimeFieldSelectionChanged: function (handler) {
            SW.Core.TableResource.EventPublisher.on(timeFieldSelectionChanged, handler);
        },
        TriggerTimeFieldSelectionChanged: function (data) {
            SW.Core.TableResource.EventPublisher.trigger(timeFieldSelectionChanged, data);
        },
        SubscribeTimeRelativeSelectionChanged: function (handler) {
            SW.Core.TableResource.EventPublisher.on(timeRelativeSelectionChanged, handler);
        },
        TriggerTimeRelativeSelectionChanged: function (data) {
            SW.Core.TableResource.EventPublisher.trigger(timeRelativeSelectionChanged, data);
        },
        SubscribeValidationMessageRegistered: function (handler) {
            SW.Core.TableResource.EventPublisher.on(validationMessageRegistered, handler);
        },
        TriggerValidationMessageRegistered: function (data) {
            SW.Core.TableResource.EventPublisher.trigger(validationMessageRegistered, data);
        },
        TriggerSumarizingModeChanged: function(sender, args) {
             SW.Core.TableResource.EventPublisher.trigger(sumarizingModeChanged, sender, args);
        },
        SubscribeSumarizingModeChanged: function(handler) {
             SW.Core.TableResource.EventPublisher.on(sumarizingModeChanged, handler);
        }
        
    };
})();
SW.Core.Observer.makeObserver(SW.Core.TableResource.EventPublisher);