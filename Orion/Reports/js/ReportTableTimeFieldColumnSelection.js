﻿SW.Core.namespace('SW.Core.TableResource');
/* Object is responsible for managing UI of control TimeFieldColumnSelection which serve for ReportTable
 * @param {Object} config
 * config.selectionHiddenField {JQuery Element} the element which maintains state between postbacks.
 * config.parentElId {String} id of the ASP.NET control by which is this control represented
 * config.timefieldRefId {Guid} uniqueidentifier of column which serve as timeField. Can be empty if no timefield was selected
*/

SW.Core.TableResource.TimeFieldColumnSelection = function (config) {
    var self = this;

    SW.Core.TableResource.EventPublisher.SubscribeTimeRelativeSelectionChanged(function (e) {
        //Update timefield icons when TimeRelativeSelection changes
        if (e.isTimeRelative)
            timeFieldSelectionChanged();
        else
            clearIconForSelectedTimeFieldInColumnLayout();
    });

    var availableDateTimeColumns = []; // array of objects of type { FieldID: '', RefID: '', Caption: '', ColumnDataType: '' }

    var lastSelectedValue = "selectField";

    var getjQueryTimeFieldColumnList = function () {
        return $("#" + config.parentElId + " select.timeFieldList");
    };

    var clearIconForSelectedTimeFieldInColumnLayout = function () {
        $('.selectedTimeField').removeClass('selectedTimeField');
    };
    var showIconForSelectedTimeFieldInColumnLayout = function (column) {
        clearIconForSelectedTimeFieldInColumnLayout();
        if ($('.TimeRelativeSelection').val().toLowerCase() === 'true')
            $('a[data-fieldid="' + column + '"]').find('.timestampColumnIconHolder').addClass('selectedTimeField');
    };

    var timeFieldSelectionChanged = function () {
        var curSelection = getjQueryTimeFieldColumnList().val();
        config.selectionHiddenField.val(curSelection);

        showIconForSelectedTimeFieldInColumnLayout(curSelection);
        SW.Core.TableResource.EventPublisher.TriggerTimeFieldSelectionChanged({ selectedValue: curSelection });
    };

    var solveEnabledStateOfFieldPickerControl = function () {
        var $columnList = getjQueryTimeFieldColumnList();
        if (availableDateTimeColumns.length > 0) {
            $columnList.attr('disabled', false);
        }
        else {
            $columnList.attr('disabled', 'disabled');
        }
    };

    this.AddFieldIntoList = function (fieldId, refID, caption, columnDataType) {
        /// <summary>
        /// Method adds new timeField column into list of already added columns. If columnDataType will not be DateTime no operation will be performed.
        /// </summary>
        /// <param name="fieldId" type="Guid">Uniqueidentifier of field which was choosen from fieldpicker</param>
        /// <param name="refID" type="Guid">Uniqueidentifier of column which is contained in draggable columns</param>
        /// <param name="caption" type="String">Tittle of column which we wants to add</param>
        /// <param name="columnDataType" type="String">In string e.g System.DateTime, System.String etc. is passed datatype of field which I wants to add.</param>

        if (columnDataType == "System.DateTime") {
            var $columnList = getjQueryTimeFieldColumnList();
            var $optionToAdd = $("<option></option>").val(fieldId).html(caption).data("fieldid", fieldId);
            $columnList.append($optionToAdd);
            availableDateTimeColumns.push({ FieldID: fieldId, RefID: refID, Caption: caption, ColumnDataType: columnDataType });
        }

        solveEnabledStateOfFieldPickerControl();
    };

    this.FillColumnList = function (dataToFill) {
        /// <summary>
        /// Method fills combobox with list of available datetime columns by data passed in parameter dataToFill and store in inner property list of available datetime columns.
        /// from list dataToFill will be choosen just columns of DateTime columndatetype
        /// </summary>
        /// <param name="dataToFill" type="Array">Array of object with properties FieldId, RefID, Caption which will be used for filling combobox</param>

        availableDateTimeColumns = [];
        var $columnList = getjQueryTimeFieldColumnList();
        $columnList.empty();
        if (jQuery.isArray(dataToFill)) {
            for (var i = 0; i < dataToFill.length; i++) {
                if ((dataToFill[i].RefID != "00000000-0000-0000-0000-000000000000") && (dataToFill[i].ColumnDataType == "System.DateTime")) {
                    var $optionToAdd = $("<option></option>").val(dataToFill[i].FieldId).html(dataToFill[i].Caption).data("fieldid", dataToFill[i].FieldId);
                    $columnList.append($optionToAdd);
                    availableDateTimeColumns.push({ FieldID: dataToFill[i].FieldId, RefID: dataToFill[i].RefID, Caption: dataToFill[i].Caption });
                }
            }

            if (typeof (config.timefieldRefId) != "undefined") {
                $columnList.val(config.timefieldRefId);
                if (config.timefieldRefId == "") {
                    lastSelectedValue = "selectField";
                }
                else {
                    lastSelectedValue = config.timefieldRefId;
                }
            }
        }

        getjQueryTimeFieldColumnList().on("change", timeFieldSelectionChanged);
        solveEnabledStateOfFieldPickerControl();
    };

    this.RemoveColumnFromList = function (fieldID) {
        /// <summary>
        /// Method removes column specified in parameter fieldID from the list of available columns
        /// </summary>
        /// <parameter name="refID" type="Guid">Uniqueidentifier of the column which we want's to remove from the list</param>

        var $columnList = getjQueryTimeFieldColumnList();
        var selectedVal = $columnList.val();
        $columnList.find("option[value='" + fieldID + "']").remove();
        var indexToRemove = -1;
        for (var i = 0; i < availableDateTimeColumns.length; i++) {
            if (availableDateTimeColumns[i].FieldID == fieldID) {
                indexToRemove = i;
                break;
            }
        }

        availableDateTimeColumns.splice(indexToRemove, 1); // delete item from specified position indexToRemove
        $columnList.trigger("change");
        solveEnabledStateOfFieldPickerControl();
    };
};

