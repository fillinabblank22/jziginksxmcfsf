﻿
SW.Core = SW.Core || {};
SW.Core.TableResource = SW.Core.TableResource || {};
SW.Core.TableResource.SizeColumnTypeEnum = SW.Core.TableResource.SizeColumnTypeEnum || {
    Automatic: 0,
    Custom: 1
};

SW.Core.TableResource.ReportPreviewColumnResize = function (config) {
    ///<summary>Class which takes care about columns resizing in report preview</summary>
    /// <param name="config">config.onCreatedJS - function/method which should be called when ReportPreviewColumnResize will be created</param>

    var self = this;
    var colSizesPx = [];
    var colSizesPct = []; // here will be stored in % column size e.g. 3 columns 25%, 64% and 11%

    var minimumColumnWidth = 30; // here you can change minimum colWidth for all columns. User will not be able to change column width which will be under this value
    var $previewTable = null;

    var $columnWidthControlsTable = null;

    var $reportPreviewTip = null;

    var $reportPreviewEmptyTip = null;

    var wasColumnSizesUpdated = false;

    var columnSizesAfterInit = null;

    var columnSizeInit = false;

    SW.Core.Observer.makeObserver(self);

    var getGroupColumnsCount = function () {
        /// <summary>Method returns number of column which are used for grouping. This columns shouldn't be possible to resize.</summary>
        return $previewTable.find("th.qqp0_hi").length;
    };

    var createAndAppendColumnWidthControls = function () {
        $columnWidthControlsTable = $("<table style=\"border-collapse: collapse;\"><tbody><tr></tr></tbody>").insertBefore($previewTable);
        var $tableRow = $columnWidthControlsTable.find("tr");
        $tableRow.append("<td style=\"font-weight: bold\">@{R=Core.Strings; K=WEBJS_PS0_34; E=js}</td><td style=\"padding-left: 10px;\"><input type=\"radio\" name=\"SizeColumnType\" id=\"optAutomatic\" value=\"0\" /><label style=\"margin-left: 2px;\" for=\"optAutomatic\">@{R=Core.Strings; K=WEBJS_PS0_35; E=js}</label></td><td style=\"padding-left: 10px\"><input type=\"radio\" name=\"SizeColumnType\" id=\"optCustom\" value=\"1\" /><label style=\"margin-left: 2px;\" for=\"optCustom\">@{R=Core.Strings; K=WEBJS_PS0_36; E=js}</label></td>");
        $tableRow.find("#optAutomatic").on('change', function (event) {
            enableOrDisableColumnResizableForAllColumns(false);
        });

        $tableRow.find("#optCustom").on('change', function (event) {
            enableOrDisableColumnResizableForAllColumns(true);
        });

        $reportPreviewTip = $("<div style=\"margin-top: 7px; margin-bottom: 7px\"><span style=\"font-size: 8pt; color: #646464;\">@{R=Core.Strings; K=WEBJS_PS0_33; E=js}</span></div>").insertBefore($previewTable);
        $reportPreviewEmptyTip = $("<div style=\"margin-top: 7px; margin-bottom: 7px; height: 9pt; display: none;\">&nbsp;</div>").insertBefore($previewTable);
    };

    var enableOrDisableColumnResizableForAllColumns = function (enabled, updateColWidthSettingControl) {
        var columnSizesObj = self.GetColumnSizes();
        
        if (columnSizesObj.ColumnsSizeInPercentage.length > 1) {
            for (var i = getGroupColumnsCount(); i < columnSizesObj.ColumnsSizeInPercentage.length; i++) {
                enableOrDisableColumnResizableForIndividualColumn(i, enabled);
            }
        }

        var $previewPanel = $("#ReportCell-PreviewPanel");
        if (enabled) {
            $reportPreviewTip.show();
            $reportPreviewEmptyTip.hide();
            $previewPanel.find(".sw-btn-cancel").show();
            $previewPanel.find(".sw-btn:not(.sw-btn-cancel)").addClass("sw-btn-primary").find("span.sw-btn-t").text("@{R=Core.Strings; K=WEBCODE_PS0_2; E=js}");

        } else {
            $reportPreviewTip.hide();
            $reportPreviewEmptyTip.show();
            $previewPanel.find(".sw-btn-cancel").hide();
            var $primaryButton = $previewPanel.find(".sw-btn-primary");
            $primaryButton.removeClass("sw-btn-primary");
            $primaryButton.find("span.sw-btn-t").text("@{R=Core.Strings; K=WEBJS_AK0_53; E=js}");
        }

        if (updateColWidthSettingControl) {
            if (enabled) {
                $columnWidthControlsTable.find("#optCustom").prop("checked", true);
            } else {
                $columnWidthControlsTable.find("#optAutomatic").prop("checked", true);
            }
        }
    };

    var enableOrDisableColumnResizableForIndividualColumn = function (colIndex, enable) {
        if (colIndex >= (colSizesPct.length - 1)) {
            colIndex = colIndex - 1;
        }

        if (enable) {
            $(".ReportPreviewColResizeGrip:eq(" + colIndex + ")").show();
            $(".JColResizer:eq(" + colIndex + ")").show();
        } else {
            $(".ReportPreviewColResizeGrip:eq(" + colIndex + ")").hide();
            $(".JColResizer:eq(" + colIndex + ")").hide();
        }
    };

    var updateColumnSizes = function () {
        var columns = $previewTable.find("th");
        var tableWidth = $previewTable.width();

        if (!wasColumnSizesUpdated) {
            wasColumnSizesUpdated = true;
        }

        columns.each(function (i) {
            // adjust for borders
            var cellWidth = $(this).width() - 2;

            if (i === 0) {
                // adjust for first column
                cellWidth -= 1;
            }

            if (cellWidth < minimumColumnWidth) {
                cellWidth = minimumColumnWidth;
                $(this).width(cellWidth);
            }

            colSizesPx[i] = cellWidth;
            colSizesPct[i] = Math.round((cellWidth * 100) / tableWidth);
        });

        // check for rounding error
        var cTotal = 100;
        var cMax = 0;
        var jj = 0;
        var j = colSizesPct.length;
        while (j--) {
            cTotal -= colSizesPct[j];
            if (Math.max(cMax, colSizesPct[j]) === colSizesPct[j]) {
                cMax = colSizesPct[j];
                jj = j;
            }
        }

        colSizesPct[jj] += cTotal;

        // We will be able also to hook into event in the case, that other object will need to be informed immediately after any column size will change.
        self.trigger("columnResized", self, { ColumnsSizeInPixels: _.clone(colSizesPx), ColumnsSizeInPercentage: _.clone(colSizesPct) });
    };

    var onDragColumn = function () {
        $previewTable.parent().find(".ReportPreviewColResizeGrip").css("margin-top", ($previewTable.height() + 4) + "px");
    };

    this.GetAllColumnWithSizesWhichSizeWasChangedAfterInitialization = function () {
        /// <summary>Method find out which column size was changed after inicialization and return this object with index of column and new size (width) in px and percentage</summary>
        /// <returns type="Object">Returns following object { colIndex: {int}, widthInPx: {int}, colSizesPct: {int} }</returns>

        var currentColumnSizes = self.GetColumnSizes();
        var res = [];

        if (!wasColumnSizesUpdated) {
            return res;
        }

        for (var i = 0; i < currentColumnSizes.ColumnsSizeInPixels.length; i++) {
            if (columnSizesAfterInit.ColumnsSizeInPixels.length > i) {
                var value = currentColumnSizes.ColumnsSizeInPixels[i];
                if (columnSizesAfterInit.ColumnsSizeInPixels[i] != value) {
                    res.push({ colIndex: i, widthInPx: value, widthInPct: currentColumnSizes.ColumnsSizeInPercentage[i] });
                }
            }
        }

        return res;
    };

    this.GetColumnSizes = function (newColumnsCount, indexToDelete) {
        /// <summary>
        /// Method returns current column size for each column in pixels as well in percentage.
        /// parameters newColumnsCount, indexToDelete are optional and used to calculate columns widths if we're adding / deleting columns
        /// </summary> 
        /// <returns type="Object">Following object: { ColumnsSizeInPixels: {Array}, ColumnsSizeInPercentage: {Array}, ColumnsSizeType: {Array of SizeColumnTypeEnum}, ColumnsSizeRefId: {Array of uniqueidentifier of individual columns} }</returns>

        var columns = $previewTable.find("th");
        var tableWidth = $previewTable.width();
        var columnSizeTypes = [];
        var columnsSizeRefId = [];
        var scale = 1;
        if (newColumnsCount != null) {
            if (newColumnsCount > 0 && indexToDelete > -1) {
                scale = 1 / (1 - ($(columns[indexToDelete]).width() - 2) / tableWidth);
            } else {
                var newColumnSize = tableWidth / newColumnsCount;
                var knownColsWidth = tableWidth - (newColumnSize * (newColumnsCount - columns.length));
                scale = knownColsWidth / tableWidth;
            }
        }

        columns.each(function (i) {
            // adjust for borders
            var cellWidth = $(this).width() - 2;
            colSizesPx[i] = Math.round(scale * cellWidth);
            colSizesPct[i] = scale * ((cellWidth * 100.0) / tableWidth);

            if ($(this).hasClass(".qqp0_hi")) { // in the case, that columns is used for column group
                columnSizeTypes.push(SW.Core.TableResource.SizeColumnTypeEnum.Automatic);
            } else {
                var isCustom = $columnWidthControlsTable.find("#optCustom").prop("checked");
                if (isCustom) {
                    columnSizeTypes.push(SW.Core.TableResource.SizeColumnTypeEnum.Custom);
                } else {
                    columnSizeTypes.push(SW.Core.TableResource.SizeColumnTypeEnum.Automatic);
                }
            }
            
            columnsSizeRefId.push($(this).data("refid"));
        });

        return { ColumnsSizeInPixels: _.clone(colSizesPx), ColumnsSizeInPercentage: _.clone(colSizesPct), ColumnsSizeType: _.clone(columnSizeTypes), ColumnsSizeRefId: _.clone(columnsSizeRefId) };
    };

    this.Init = function () {
        /// <summary>
        /// Method responsible for initialization of the resizable preview table.
        /// </summary>

        $previewTable = $("table.sw-rpt-tbl:first");

        $previewTable.css({
            "border-spacing": "0",
            "border-collapse": "collapse",
            "border": "1px solid #dadada"
        });

        $previewTable.find("th").css({
            "background": "#ccc"
        });

        $previewTable.find("th,td").css({
            "border-bottom": "1px solid #dadada"
        });

            $previewTable.find("th:last,tr td:nth-last-child(-n+1)").css({
                "border-right": "0px"
            });

        createAndAppendColumnWidthControls();

        $previewTable.colResizable({
            liveDrag: true,
            minWidth: minimumColumnWidth,
            gripInnerHtml: "<div class='ReportPreviewColResizeGrip' style='margin-top:" + ($previewTable.height() + $columnWidthControlsTable.height() + 4) + "px'></div>",
            draggingClass: "dragging",
            onDrag: onDragColumn,
            onResize: updateColumnSizes
        });

        // works around a strange bug
        var fixMe = $previewTable.width();
        $previewTable.width(fixMe);

        $previewTable.parent().find(".ReportPreviewColResizeGrip").css("margin-top", ($previewTable.height() + 4) + "px");

        // the first column is fixed for group by reports
        if ($previewTable.find("th:first").hasClass("qqp0_hi")) {
            $(".ReportPreviewColResizeGrip:first").hide();
            $(".JColResizer:first").hide();
        }

        // setTimeout(function () { columnSizesAfterInit = self.GetColumnSizes(); }, 6);

        $(function () {
            $previewTable.parent().find(".ReportPreviewColResizeGrip").css("margin-top", ($previewTable.height() + 4) + "px");
        });

        if (config.onCreatedJS && jQuery.isFunction(config.onCreatedJS)) {
            config.onCreatedJS(self);
        }
    };

    this.SetColumnWidth = function (colIndex, colPercWidth) {
        if (!columnSizeInit) { // code withing this condition is needed when user try to manually editing with of column
            var minWidthPerc = ((minimumColumnWidth + 2) * 100) / $previewTable.width();

            var deltaWidth = colPercWidth - colSizesPct[colIndex];
            var groupColumnCount = getGroupColumnsCount();
            if ((colIndex > groupColumnCount) && (colIndex < (colSizesPct.length - 1))) {
                if (deltaWidth > 0) { // user tries to increase column width
                    if ((colSizesPct[colIndex + 1] - minWidthPerc) < deltaWidth) {
                        colPercWidth -= (deltaWidth - colSizesPct[colIndex + 1] + minWidthPerc);
                        colPercWidth = Math.floor(colPercWidth);
                    }
                }
            }
            else if (colIndex == groupColumnCount) { // first resizable column
                if (colSizesPct.length > groupColumnCount) {
                    if (deltaWidth > 0) {
                        if ((colSizesPct[colIndex + 1] - minWidthPerc) < deltaWidth) {
                            colPercWidth -= (deltaWidth - colSizesPct[colIndex + 1] + minWidthPerc);
                            colPercWidth = Math.floor(colPercWidth);
                        }
                    }
                } else {
                    // we have just one resizable column
                    if (colPercWidth > 100) {
                        colPercWidth = 100;
                    }
                }
            } else { // last column
                if (colIndex != 0) {
                    if (deltaWidth > 0) {
                        if ((colSizesPct[colIndex - 1] - minWidthPerc) < deltaWidth) {
                            colPercWidth -= (deltaWidth - colSizesPct[colIndex - 1] + minWidthPerc);
                            colPercWidth = Math.floor(colPercWidth);
                        }
                    }
                }
            }
        }

        $previewTable.setColumnPercentWidth(colIndex, colPercWidth);

        $previewTable.parent().find(".ReportPreviewColResizeGrip").css("margin-top", ($previewTable.height() + 4) + "px");
    };

    this.InitColumnStates = function (columnStates) {
        /// <summary>
        /// Method responsible for setting correct column width and whether column has Automatic or Custom width
        /// </summary>
        /// <param name="columnStates" type="Array">Array of following object: refid: {guid}, colIndex: {numeric}, colWidthType: {object}, colWidth }</param>

        columnSizeInit = true;
        var foundCustomColumn = false;
        var $headerColumns = $previewTable.find("th");
        var headerColumns = [];
        $.each($headerColumns, function() {
            headerColumns.push({ refid: $(this).data("refid") });
        });

        for (var i = 0; i < columnStates.length; i++) {
            if (columnStates[i].colWidthType == SW.Core.TableResource.SizeColumnTypeEnum.Custom) {
                var $column = $columnWidthControlsTable.find("tr:first").find("td:eq(" + (columnStates[i].colIndex) + ")");
                $column.find("select").val(SW.Core.TableResource.SizeColumnTypeEnum.Custom);
                $column.find("input").parent().show();
                
                // find proper column index by uniquieidentifier of column
                var colIndex = SW.Core.TableResource.ReportTableHelperUtils.FindColumn(headerColumns, columnStates[i].refid);

                if (colIndex > -1) {
                    self.SetColumnWidth(colIndex, columnStates[i].colWidth);
                }
                
                foundCustomColumn = true;
            }
        }

        if (foundCustomColumn) {
            enableOrDisableColumnResizableForAllColumns(true, true);
        } else {
            enableOrDisableColumnResizableForAllColumns(false, true);
        }

        // the first column is fixed for group by reports
        if ($previewTable.find("th:first").hasClass("qqp0_hi")) {
            $(".ReportPreviewColResizeGrip:first").hide();
            $(".JColResizer:first").hide();
        }
        columnSizeInit = false;
    };
};
