﻿SW.Core.namespace('SW.Core.TableResource');
/* Object is responsible for managing UI of control ReportTableSummarizing which serve for ReportTable
 * @param {Object} config
 * config.selectionElementId {String} id of the ASP.NET control by which is this control represented
*/

SW.Core.TableResource.ReportTableSummarizing = function (config) {
    //page elements defined by config
    var self = this;
    var ddlSummarizeBy = config.ddlSummarizeBy;
    var sumarizingFieldId = '';

    //private methods
    

    // enable or disable dropdown list according to summarizing method
    var summarizingMethodChange = function (event) {
            var target = event.target || event.srcElement;
            var enabled = target.value !== 'NoDataSummarization';

            SW.Core.TableResource.EventPublisher.TriggerSumarizingModeChanged(self, 
            {
                 isEnabled: enabled, 
                 fieldId: sumarizingFieldId
            });
    };


    this.init = function () {
        // attach on click event
        ddlSummarizeBy.on('change', summarizingMethodChange);

        // call onclick event handler on selected radio button
        summarizingMethodChange({ srcElement: ddlSummarizeBy[0] });

        SW.Core.TableResource.EventPublisher.SubscribeTimeFieldSelectionChanged( function (timeFieldSelection) {
                sumarizingFieldId = timeFieldSelection.selectedValue;    
        });
    };

};