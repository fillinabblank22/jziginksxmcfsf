﻿Ext.namespace('SW');
Ext.namespace('SW.Orion');
Ext.QuickTips.init();

SW.Orion.ReportScheduler = function () {
    var reMsAjax = /^\/Date\((d|-|.*)\)\/$/;

    var comboArray;
    var pageSizeBox;
    var selectorModel;
    var userPageSize;
    var allowSchedule;
    var sortOrder;
    var groupingValue;
    var selectedColumns;
    var isSelectAllMode = false;
    var tempSelectedItemsArray;
    var swisBasedReportsOnly;

    var dataStore;
    var grid;
    var groupGrid;

    var updateToolbarButtons = function () {
        var map = grid.getTopToolbar().items.map;
        var selCount = getSelectedItemsArray().length;

        if (!allowSchedule) {
            map.Delete.setDisabled(true);
            map.Add.setDisabled(true);
            map.DuplicateEdit.setDisabled(true);
            map.AssignToReport.setDisabled(true);
            map.Edit.setDisabled(true);
            map.RunNow.setDisabled(true);
        } else {
            map.Delete.setDisabled(selCount == 0);
            map.DuplicateEdit.setDisabled(selCount != 1);
            map.AssignToReport.setDisabled(selCount == 0);
            map.Edit.setDisabled(selCount != 1);
            map.RunNow.setDisabled(selCount == 0);
        }

        var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');

        if (grid.getStore().getCount() > 0 && grid.getSelectionModel().getCount() == grid.getStore().getCount()) {
            hd.addClass('x-grid3-hd-checker-on');
        }
        else {
            hd.removeClass('x-grid3-hd-checker-on');
        }
    };

    function selectAllTooltip(grid, pageSizeBox, webStore) {
        if (grid.store.data.items.length == grid.getSelectionModel().selections.items.length && grid.store.data.items.length != 0 && grid.store.data.items.length != pageSizeBox.ownerCt.dsLoaded[0].totalLength) {
            var text = String.format("@{R=Core.Strings;K=WEBJS_AB0_43;E=js}", grid.getSelectionModel().selections.items.length);
            $('<div id="selectionTip" style="padding-left: 5px; background-color:#FFFFCC;">' + text + ' </div>').insertAfter(".x-panel-tbar.x-panel-tbar-noheader");
            $("#selectionTip").append($("<span id='selectAllLink' style='text-decoration: underline;color:red;cursor:pointer;'>" + String.format("@{R=Core.Strings;K=WEBJS_AB0_42;E=js}", pageSizeBox.ownerCt.dsLoaded[0].totalLength) + "</span>").click(function () {
                var allGridElementsStore = webStore();
                allGridElementsStore.proxy.conn.jsonData = grid.store.proxy.conn.jsonData;
                allGridElementsStore.load({
                    callback: function (result) {
                        isSelectAllMode = true;
                        tempSelectedItemsArray = allGridElementsStore.data.items;
                        $("#selectionTip").text("@{R=Core.Strings;K=WEBJS_AB0_44;E=js}").css("background-color", "white");
                        $("#selectAllLink").remove();
                        updateToolbarButtons();
                    }
                });
            }));
            var gridHeight = $("#selectAllLink").closest('.x-panel-body.x-panel-body-noheader').height();
            $("#selectAllLink").closest('.x-panel-body.x-panel-body-noheader').height(gridHeight + $("#selectionTip").height());
        } else {
            clearSelectAllTooltip();
        }
    }

    function getSelectedItemsArray() {
        return isSelectAllMode ? tempSelectedItemsArray : grid.getSelectionModel().getSelections();
    }

    function clearSelectAllTooltip() {
        if ($("#selectionTip").length != 0) {
            var rowsContainerHeight = $("#selectAllLink").closest('.x-panel-body.x-panel-body-noheader').height();
            $("#selectAllLink").closest('.x-panel-body.x-panel-body-noheader').height(rowsContainerHeight - $("#selectionTip").height());
            $("#selectionTip").remove();
            if (isSelectAllMode) {
                isSelectAllMode = false;
            }
        }
    }

    function refreshGridObjects(start, limit, elem, property, type, value, search, callback) {
        elem.store.removeAll();

        var tmpfilterText = search.replace(/\*/g, "%");

        if (tmpfilterText.length > 0) {
            if (!tmpfilterText.match("^%"))
                tmpfilterText = "%" + tmpfilterText;

            if (!tmpfilterText.match("%$"))
                tmpfilterText = tmpfilterText + "%";
        }

        elem.store.proxy.conn.jsonData = { property: property, type: type, value: value || "", search: tmpfilterText, resourceFilter: "", swisBasedReportsOnly: swisBasedReportsOnly };

        if (limit)
            elem.store.load({ params: { start: start, limit: limit }, callback: callback });
        else
            elem.store.load({ callback: callback });
    }

    function addSchedule() {
        if ($("#isOrionDemoMode").length != 0) {
            demoAction("Core_Scheduler_CreateNewSchedule", this);
            return;
        }
        var guid = SW.Core.Services.generateNewGuid();
        window.location = String.format("/Orion/Reports/ScheduleAdd/Default.aspx?ScheduleWizardGuid={0}", guid);
    }

    function redirectToEdit(jobId) {
        var guid = SW.Core.Services.generateNewGuid();
        window.location = String.format("/Orion/Reports/ScheduleAdd/Default.aspx?ScheduleWizardGuid={0}&ReportJobId={1}", guid, jobId);
    }

    function editSchedule() {
        var jobId = getSelectedItemsArray()[0].data.ReportJobID;
        redirectToEdit(jobId);
    }

    function dublicateEditSchedule() {
        if ($("#isOrionDemoMode").length != 0) {
            demoAction("Core_Scheduler_DublicateSchedule", this);
            return;
        }
        var id = getSelectedItemsArray()[0].data.ReportJobID;
        var name = getSelectedItemsArray()[0].data.Name;
        ORION.callWebService('/Orion/Services/ReportScheduler.asmx',
                    'DublicateReportJob', { jobsId: id, jobName: name },
                    function (result) {
                        if (result) {
                            ORION.callWebService('/Orion/Services/ReportManager.asmx',
                                'SetSessionData', { name: 'CreatedScheduledJobName', value: result.Title },
                                function () {
                                    window.location = "/Orion/Reports/ScheduleAdd/Default.aspx?ScheduleWizardGuid=" + SW.Core.Services.generateNewGuid() + "&IsDuplicated=true&ReportJobId=" + result.ID;
                                });
                        } else {
                            // TODO: show message that value wasn't set
                        }
                    });
    }

    function assignToReport() {
        var selected = getSelectedItemsArray();
        var assignedReports = [];
        if (selected.length == 1)
            assignedReports = JSON.parse(selected[0].data.ReportsData);

        SW.Orion.ReportPicker.selectReportDialog();
        SW.Orion.ReportPicker.clearSelection();
        SW.Orion.ReportPicker.setSelectedReportsIds(assignedReports);
        $("#shceduleReportBox").parent().css('margin-top', '7%');
    }

    function deleteSelectedItems(items, onSuccess) {
        if ($("#isOrionDemoMode").length != 0) {
            demoAction("Core_Scheduler_DeleteSchedule", this);
            return;
        }
        var idsToDelete = [];
        for (var i = 0; i < items.length; i++) {
            idsToDelete.push(items[i].data.ReportJobID);
        }
        var waitMsg = Ext.Msg.wait("@{R=Core.Strings;K=WEBJS_TM0_143;E=js}");
        ORION.callWebService('/Orion/Services/ReportScheduler.asmx',
                    'RemoveReportJobs', { jobsIds: idsToDelete },
                    function (result) {
                        window.location = "/Orion/Reports/Scheduler.aspx";
                        waitMsg.hide();
                    });
    }

    function startRunNowSchedules() {
        if ($("#isOrionDemoMode").length != 0) {
            demoAction("Core_Scheduler_RunNow", this);
            return;
        }
        var selected = getSelectedItemsArray();
        var idsToRunNow = [];
        for (var i = 0; i < selected.length; i++) {
            idsToRunNow.push(selected[i].data.ReportJobID);
        }
        var waitMsg = Ext.Msg.wait("@{R=Core.Strings;K=WEBJS_AB0_50;E=js}");
        ORION.callWebService('/Orion/Reports/Scheduler.aspx',
                    'RunNow', { jobIds: idsToRunNow },
                    function (result) {
                        var executionResults = "";
                        var resultDictionary = JSON.parse(result);
                        if (result) {
                            for (var i = 0; i < selected.length; i++) {
                                if (resultDictionary[selected[i].data.ReportJobID]) {
                                    executionResults += String.format("@{R=Core.Strings;K=WEBJS_AB0_56;E=js}", encodeHTML(selected[i].data.Name));
                                    executionResults += "<br/>";
                                }
                                else if (resultDictionary[selected[i].data.ReportJobID] == false) {
                                    executionResults += String.format("@{R=Core.Strings;K=WEBJS_AB0_57;E=js}", encodeHTML(selected[i].data.Name));
                                    executionResults += "<br/>";
                                }
                            }
                            waitMsg.hide();
                            if (executionResults != "")
                                Ext.Msg.show({ title: "@{R=Core.Strings;K=WEBJS_AB0_58;E=js}", msg: executionResults, minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.INFO });
                            else {
                                Ext.Msg.show({ title: "@{R=Core.Strings;K=WEBJS_AB0_58;E=js}", msg: "@{R=Core.Strings;K=WEBJS_AY0_18;E=js}", minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.INFO });
                            }
                        }
                    },
                    function (message) {
                        waitMsg.hide();
                        Ext.Msg.show({ title: "@{R=Core.Strings;K=WEBJS_AB0_58;E=js}", msg: "@{R=Core.Strings;K=WEBJS_VB0_133;E=js}", minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.WARNING });
                    });
    }

    function encodeHTML(value) {
        return Ext.util.Format.htmlEncode(value);
    }

    function escapeRegExp(str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }

    function renderString(value, meta, record) {
        if (!value || value.length === 0)
            return value;

        if (!filterText || filterText.length === 0)
            return encodeHTML(value);

        var patternText = filterText;

        // replace any %'s with a *
        patternText = patternText.replace(/\%/g, "*");

        // replace multiple *'s with a single instance
        patternText = patternText.replace(/\*{2,}/g, "*");

        // check if the search string is now just down to a single *, and if so return without any further regex
        if (patternText == '*')
        { return '<span style=\"background-color: #FFE89E\">' + encodeHTML(value) + '</span>'; }

        // replace \ with \\
        patternText = patternText.replace(/\\/g, '\\\\');
        // replace . with \.
        patternText = patternText.replace(/\./g, '\\.');
        // replace * with .*
        patternText = patternText.replace(/\*/g, '.*');

        // set regex pattern
        var x = '((' + escapeRegExp(patternText) + ')+)\*';
        var content = value, pattern = new RegExp(x, "gi"), replaceWith = '{SPAN-START-MARKER}$1{SPAN-END-MARKER}';

        // do regex replace + content gets encoded
        var fieldValue = encodeHTML(content.replace(pattern, replaceWith));

        // now replace the literal SPAN markers with the HTML span tags
        fieldValue = fieldValue.replace(/{SPAN-START-MARKER}/g, '<span style=\"background-color: #FFE89E\">');
        fieldValue = fieldValue.replace(/{SPAN-END-MARKER}/g, '</span>');

        return fieldValue;
    }

    function renderEditName(value, meta, record) {
        return String.format('<a href="#" onclick="SW.Orion.ReportScheduler.redirectToEdit({0}); return false;" class="schedule-edit-link">{1}</a>',
                             record.data.ReportJobID, renderString(value));
    }

    function renderToggle(value) {
        if (value == true) {
            return ('<a href="#" class="toggleOn">@{R=Core.Strings;K=WEBJS_TM0_144;E=js}</a>');
        } else {
            return ('<a href="#" class="toggleOff">@{R=Core.Strings;K=LogLevel_off;E=js}</a>');
        }
    }

    function renderLastRun(value) {
        if (Ext.isEmpty(value) || value.length === 0)
            return '';
        var a = reMsAjax.exec(value);
        if (a) {
            var b = a[1].split(/[-,.]/);
            var val = new Date(+b[0]);
            return val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + ' ' + val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern);
        }

        var dateVal = new Date(value);
        if (dateVal && dateVal != 'Invalid Date') {
            return dateVal.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + ' ' + dateVal.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern);
        }

        return '';
    }

    function renderReports(value, meta, record) {
        if (value == null)
            return;
        var parsedReports = JSON.parse(value);
        var reportPattern = '<a href="/Orion/Report.aspx?ReportID={0}"><span ext:qwidth="312" ext:qtitle="" ext:qtip="{1}">{2}</span></a>';

        var tipText = '';
        var resultString = '';
        if (parsedReports.length > 0) {
            tipText = encodeHTML(encodeHTML(parsedReports[0].Title));
            for (var i = 1; i < parsedReports.length; i++) {
                tipText = String.format('@{R=Core.Strings;K=WEBJS_TM0_146; E=js}', tipText, encodeHTML(encodeHTML(parsedReports[i].Title)));
            };

            resultString = String.format(reportPattern, parsedReports[0].ID, tipText, renderString(parsedReports[0].Title));
            for (i = 1; i < parsedReports.length; i++) {
                resultString = String.format('@{R=Core.Strings;K=WEBJS_TM0_146; E=js}', resultString, String.format(reportPattern, parsedReports[i].ID, tipText, renderString(parsedReports[i].Title)));
            }
        }
        return String.format('<span class="reportsColumn">{0}</span>', resultString);
    }

    function renderUrls(value, meta, record) {
        if (value == null)
            return;
        var parsedUrls = JSON.parse(value);
        var reportPattern = '<a href="{0}"><span ext:qwidth="312" ext:qtitle="" ext:qtip="{1}">{2}</span></a>';

        var tipText = '';
        var resultString = '';
        if (parsedUrls.length > 0) {
            tipText = encodeHTML(parsedUrls[0]);
            for (var i = 1; i < parsedUrls.length; i++) {
                tipText = String.format('@{R=Core.Strings;K=WEBJS_TM0_146; E=js}', tipText, encodeHTML(parsedUrls[i]));
            };

            resultString = String.format(reportPattern, parsedUrls[0], tipText, renderString(parsedUrls[0]));
            for (i = 1; i < parsedUrls.length; i++) {
                resultString = String.format('@{R=Core.Strings;K=WEBJS_TM0_146; E=js}', resultString, String.format(reportPattern, parsedUrls[i], tipText, renderString(parsedUrls[i])));
            }
        }
        return String.format('<span class="urlsColumn">{0}</span>', resultString);
    }

    function renderFrequencies(value) {
        if (value == null)
            return '';

        // adding tooltip to display full cell value
        return String.format('<span ext:qtitle="" ext:qtip="{0}">{1}</span>', encodeHTML(encodeHTML(value)), renderString(value));
    }

    function renderActions(value, meta, record) {
        value = record.data.ActionsData;
        if (value == null)
            return;
        var parsedActions = JSON.parse(value);
        var actionsPattern = '<span ext:qwidth="312" ext:qtitle="" ext:qtip="{0}">{1}</span>';
        var tipText = '';
        var resultString = '';
        if (parsedActions.length > 0) {
            tipText = String.format("<b>{0}</b><br/><span style='word-wrap:break-word'>{1}</span>", encodeHTML(encodeHTML(parsedActions[0].Title)), parsedActions[0].Description); // encodeHTML twice as extjs tooltip requires.
            for (var i = 1; i < parsedActions.length; i++) {
                tipText = String.format('@{R=Core.Strings;K=WEBJS_TM0_146; E=js}', tipText, (String.format('<br/><b>{0}</b><br/>{1}', encodeHTML(encodeHTML(parsedActions[i].Title)), parsedActions[i].Description)));
            }
            resultString = String.format(actionsPattern, tipText, renderString(parsedActions[0].Title));
            for (i = 1; i < parsedActions.length; i++) {
                resultString = String.format('@{R=Core.Strings;K=WEBJS_TM0_146; E=js}', resultString, String.format(actionsPattern, tipText, renderString(parsedActions[i].Title)));
            }
        }
        return String.format('<span class="actionsColumn">{0}</span>', resultString);
    }

    function renderGroup(value, meta, record) {
        var disp;

        if (comboArray.getValue().indexOf('Enabled') > -1) {
            disp = String.format('@{R=Core.Strings;K=WEBJS_TM0_145; E=js}', renderStatus(value), record.data.Cnt);
        } else {
            if (value == null) {
                value = "null";
            }
            if (value == "[All]") {
                value = "@{R=Core.Strings;K=WEBJS_SO0_31; E=js}";
            }
            disp = String.format('@{R=Core.Strings;K=WEBJS_TM0_145; E=js}', value, record.data.Cnt);
        }
        return '<span ext:qtitle="" ext:qtip="' + encodeHTML(encodeHTML(disp)) + '">' + encodeHTML(disp) + '</span>';
    }

    function renderStatus(status) {
        if (status == '[All]') return '@{R=Core.Strings;K=LIBCODE_TM0_15;E=js}';
        return stringToBoolean(status) ? '@{R=Core.Strings;K=WEBJS_TM0_144;E=js}' : '@{R=Core.Strings;K=LogLevel_off;E=js}';
    }

    function ErrorHandler(result) {
        if (result != null && result.Error) {
            Ext.Msg.show({ title: result.Source, msg: result.Msg, minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
        }
    }

    function stringToBoolean(value) {
        switch (value.toString().toLowerCase()) {
            case "true": case "yes": case "1": return true;
            case "false": case "no": case "0": case null: return false;
            default: return Boolean(value);
        }
    }

    function setMainGridHeight(pageSize) {
        var mainGrid = Ext.getCmp('mainGrid');
        var maxHeight = calculateMaxGridHeight(mainGrid);
        var calculated = calculateGridHeight(pageSize);
        var height = (calculated > 0) ? Math.min(calculated, maxHeight) : maxHeight;
        setHeightForGrids(height);

        //we need this to add height if horizontal scroll bar is present, with low window width and in chrome
        if (grid.el.child(".x-grid3-scroller").dom.scrollWidth != grid.el.child(".x-grid3-scroller").dom.clientWidth) {
            height = Math.min(height + 20, maxHeight);
            setHeightForGrids(height);
        }
        mainGrid.doLayout();
    }

    function selectCreatedSchedule(res) {
        var startFrom = 0;
        var objectIndex = res.ind;

        if (objectIndex > -1) {
            var pageNumFl = objectIndex / userPageSize;
            var index = parseInt(pageNumFl.toString());
            startFrom = index * userPageSize;
        }

        refreshGridObjects(startFrom, userPageSize, grid, groupingValue[0], "", groupingValue[1], filterText, function () {
            if (objectIndex > -1) {
                var rowEl = grid.getView().getRow(objectIndex % userPageSize);
                rowEl.scrollIntoView(grid.getGridEl().id, false);
                grid.getView().focusRow(objectIndex % userPageSize);
                grid.getSelectionModel().selectRow(objectIndex % userPageSize);
            }
        });
    }

    function getReportJobRowNumber(jobName, prop, type, value, succeeded) {
        if (!jobName || jobName == "") {
            succeeded({ ind: -1 });
            return;
        }

        var sort = "";
        var dir = "";

        if (sortOrder != null && sortOrder[0] != 'Favorite Desc') {
            sort = sortOrder[0];
            dir = sortOrder[1];
        }

        SW.Core.Services.callWebServiceSync('/Orion/Services/ReportScheduler.asmx', 'GetReportJobNumber', { reportJobName: jobName, property: prop, type: type, value: value, sort: sort, direction: dir },
            function (result) {
                if (succeeded != null) {
                    succeeded({ ind: result });
                    return;
                }
                // not found
                succeeded({ ind: -1 });
                return;
            });
    }

    function setHeightForGrids(height) {
        var mainGrid = Ext.getCmp('mainGrid');
        var groupingGrid = Ext.getCmp('groupingGrid');

        mainGrid.setHeight(Math.max(height, 300));
        groupingGrid.setHeight(Math.max(height - 50, 250)); //height without groupByTopPanel

        mainGrid.doLayout();
    }

    function calculateGridHeight(numberOfRows) {
        if (grid.store.getCount() == 0)
            return 0;
        var rowsHeight = Ext.fly(grid.getView().getRow(0)).getHeight() * (numberOfRows + 1);
        return grid.getHeight() - grid.getInnerHeight() + rowsHeight + 7;
    }

    function calculateMaxGridHeight(gridPanel) {
        var gridTop = gridPanel.getPosition()[1];
        return $(window).height() - gridTop - $('#footer').height() - 25;
    }

    function renderReportsCountTooltip() {
        $.each($(".reportsColumn"), function () {
            var reportsCount = $(this).find("a").length;
            var charsLength = reportsCount.toString().split('').length * 10 + 16;
            if ($(this).parent().parent().find(".reportsCountTooltip").length > 0)
                $(this).parent().parent().find(".reportsCountTooltip").remove();
            $(this).parent().css("width", ($(this).parent().parent().width() - charsLength) + "px").css("float", "left");
            
            if ($(this).width() > ($(this).parent().parent().width() - charsLength)) {
                $(this).parent().parent().append(String.format('<div class="reportsCountTooltip" style="padding:2px 0;float:right">({0})</div>', reportsCount));
            }
        });
    }

    var selectedJobName;
    ORION.prefix = 'ReportScheduler_';

    return {
        redirectToEdit: function (id) {
            redirectToEdit(id);
        },
        getAssignReportsHeaderTitle: function () {
            var selected = getSelectedItemsArray();
            var scheduleNamesArray = [];
            $.each(selected, function () {
                scheduleNamesArray.push(this.data.Name);
            });
            $("#headerTitle").html(String.format("@{R=Core.Strings;K=WEBJS_AB0_9;E=js}".replace(/&lt;/gi, "<").replace(/&gt;/gi, ">"), scheduleNamesArray.join(", ")));
        },
        reportsSelected: function (selectedReports) {
            var reportIds = [];
            $.each(selectedReports, function () {
                reportIds.push(this.ID);
            });
            var selectedItems = getSelectedItemsArray();
            var scheduleIds = [];
            $.each(selectedItems, function () {
                scheduleIds.push(this.data.ReportJobID);
            });
            var oldReportIds = [];
            if (selectedItems.length == 1) {
                var reportData = JSON.parse(selectedItems[0].data.ReportsData);
                $.each(reportData, function () {
                    oldReportIds.push(this.ID);
                });
            }

            ORION.callWebService('/Orion/Services/ReportScheduler.asmx',
                    'UpdateScheduleAssignments', { schedulesIds: scheduleIds, oldReportIds: oldReportIds, newReportIds: reportIds },
                    function (result) {
                        if (result) {
                            filterText = "";
                            if (scheduleIds.length > 1) {
                                refreshGridObjects(0, userPageSize, grid, groupingValue[0], "", groupingValue[1], filterText, function () { });
                            } else {
                                getReportJobRowNumber(selectedItems[0].data.Name, groupingValue[0], "", groupingValue[1], selectCreatedSchedule);
                            }
                        } else {
                            // TODO: show message that value wasn't set
                        }
                    });
        },
        reload: function () {
            if (comboArray.getValue() == '@{R=Core.Strings;K=WEBJS_VB0_76; E=js}') {
                refreshGridObjects(0, userPageSize, grid, '', '', '', filterText, function () { });
            }
            else {
                var sell = "";
                if (groupGrid.getSelectionModel().getCount() == 1) {
                    sell = groupGrid.getSelectionModel().getSelected().get('Value');
                }
                refreshGridObjects(null, null, groupGrid, comboArray.getValue(), '', '', filterText);
                refreshGridObjects(0, userPageSize, grid, comboArray.getValue(), '', sell, filterText, function () { });
            }
        },

        init: function (configuration) {
            userPageSize = parseInt(ORION.Prefs.load('PageSize', '20'));
            allowSchedule = (ORION.Prefs.load('AllowScheduleMgmt', 'false')).toLowerCase() == 'true';
            sortOrder = ORION.Prefs.load('SortOrder', 'Name, ASC').split(',');
            groupingValue = ORION.Prefs.load('GroupingValue', ',').split(',');
            selectedColumns = ORION.Prefs.load('SelectedColumns', '').split(',');

            swisBasedReportsOnly = typeof configuration.SwisBasedReportsOnly == "undefined" ?
                false : configuration.SwisBasedReportsOnly;

            selectorModel = new Ext.grid.CheckboxSelectionModel();

            selectorModel.on("rowselect", function () {
                selectAllTooltip(grid, pageSizeBox, function () {
                    return new ORION.WebServiceStore("/Orion/Services/ReportScheduler.asmx/GetAllSelectedJobs",
                                            [
                                                { name: 'ReportJobID', mapping: 0 },
                                                { name: 'Name', mapping: 1 }
                                            ]);
                });
            });

            selectorModel.on("rowdeselect", function () {
                clearSelectAllTooltip();
            });
            selectorModel.on("selectionchange", function () {
                updateToolbarButtons();
            });

            dataStore = new ORION.WebServiceStore("/Orion/Services/ReportScheduler.asmx/GetReportJobs",
                                            [
                                                { name: 'ReportJobID', mapping: 0 },
                                                { name: 'Name', mapping: 1 },
                                                { name: 'Description', mapping: 2 },
                                                { name: 'Enabled', mapping: 3 },
                                                { name: 'LastRun', mapping: 4 },
                                                { name: 'FrequencyTitle', mapping: 5 },
                                                { name: 'ActionsData', mapping: 6 },
                                                { name: 'ActionTitles', mapping: 7 },
                                                { name: 'ReportsData', mapping: 8 },
                                                { name: 'UrlData', mapping: 9 },
                                                { name: 'AssignedReports', mapping: 10 }
                                            ]);

            dataStore.sortInfo = { field: sortOrder[0], direction: sortOrder[1] };

            var groupingStore = new ORION.WebServiceStore("/Orion/Services/ReportScheduler.asmx/GetSchedulesGroupValues",
                                            [
                                                { name: 'Value', mapping: 0 },
                                                { name: 'DisplayNamePlural', mapping: 1 },
                                                { name: 'Cnt', mapping: 2 }
                                            ], "Value", "");

            pageSizeBox = new Ext.form.NumberField({
                id: 'PageSizeField',
                enableKeyEvents: true,
                allowNegative: false,
                width: 40,
                allowBlank: false,
                minValue: 1,
                maxValue: 100,
                value: userPageSize,
                listeners: {
                    scope: this,
                    'keydown': function (f, e) {
                        var k = e.getKey();
                        if (k == e.RETURN) {
                            e.stopEvent();
                            var v = parseInt(f.getValue());
                            if (!isNaN(v) && v > 0 && v <= 100) {
                                userPageSize = v;
                                pagingToolbar.pageSize = userPageSize;
                                ORION.Prefs.save('PageSize', userPageSize);
                                pagingToolbar.doLoad(0);
                                setMainGridHeight(userPageSize);
                            }
                            else {
                                Ext.Msg.show({
                                    title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                    msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                                });
                                return;
                            }
                        }
                    },
                    'focus': function (field) {
                        field.el.dom.select();
                    },
                    'change': function (f, numbox, o) {
                        var pSize = parseInt(f.getValue());
                        if (isNaN(pSize) || pSize < 1 || pSize > 100) {
                            Ext.Msg.show({
                                title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                            });
                            return;
                        }

                        if (pagingToolbar.pageSize != pSize) { // update page size only if it is different
                            pagingToolbar.pageSize = pSize;
                            userPageSize = pSize;

                            ORION.Prefs.save('PageSize', userPageSize);
                            pagingToolbar.doLoad(0);
                            setMainGridHeight(userPageSize);
                        }
                    }
                }
            });

            var pagingToolbar = new Ext.PagingToolbar(
                {
                    id: 'schedulePagingToolbar',
                    store: dataStore,
                    pageSize: userPageSize,
                    displayInfo: true,
                    displayMsg: "@{R=Core.Strings;K=WEBJS_TM0_5; E=js}",
                    emptyMsg: "@{R=Core.Strings;K=WEBJS_SO0_78; E=js}",
                    beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                    afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                    firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                    prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                    nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                    lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                    refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}",
                    items: ['-',
                        new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_VB0_46; E=js}', style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;' }),
                        pageSizeBox
                    ]
                }
            );

            var groupingDataStore = new Ext.data.SimpleStore({
                fields: ['Name', 'Value'],
                data: [['@{R=Core.Strings;K=WEBJS_VB0_76; E=js}', ''],
                                                        ['@{R=Core.Strings;K=WEBJS_TD0_14;E=js}', 'Action'],
                                                        ['@{R=Core.Strings;K=WEBJS_TM0_147;E=js}', 'ReportsData'],
                                                        ['@{R=Core.Strings;K=WEBJS_AK0_6;E=js}', 'Enabled']]
            });

            comboArray = new Ext.form.ComboBox(
                            {
                                id: 'comboGroupBy',
                                mode: 'local',
                                boxMaxWidth: 500,
                                fieldLabel: 'Name',
                                displayField: 'Name',
                                valueField: 'Value',
                                store: groupingDataStore,
                                triggerAction: 'all',
                                value: '@{R=Core.Strings;K=WEBJS_VB0_76; E=js}',
                                typeAhead: true,
                                forceSelection: true,
                                selectOnFocus: false,
                                multiSelect: false,
                                editable: false,
                                listeners: {
                                    'select': function () {
                                        var val = this.store.data.items[this.selectedIndex].data.Value;

                                        refreshGridObjects(null, null, groupGrid, val, "", "", filterText);
                                        if (val == '') {
                                            ORION.Prefs.save('GroupingValue', ',');
                                            refreshGridObjects(0, userPageSize, grid, "", "", "", filterText, function () {
                                                $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function () {
                                                    this.tooltip = 'processed';
                                                    $.swtooltip(this);
                                                });
                                            });
                                        }
                                    }
                                }
                            });

            var updateColumnWidth = function() {
                $.each($("#rsGrid"), function () {
                    var gridColumnsWidth = $(this).find(".x-grid3-header-offset").width();
                    var parentWidth = $(this).width();
                    $.each($(this).find(".x-grid3-body"), function() {
                        if (gridColumnsWidth < parentWidth) {
                            $(this).children().width(parentWidth - 5); // - 5px for borders
                        }
                    });
                });
            };
            grid = new Ext.grid.GridPanel({
                id: 'rsGrid',
                region: 'center',
                viewConfig: { forceFit: false, emptyText: "@{R=Core.Strings;K=WEBJS_SO0_78; E=js}" },
                store: dataStore,
                baseParams: { start: 0, limit: userPageSize },
                stripeRows: true,
                trackMouseOver: false,
                columns: [selectorModel,
                    { header: 'ReportJobId', width: 80, hidden: true, hideable: false, sortable: true, dataIndex: 'ReportJobID' },
                    { header: '@{R=Core.Strings;K=WEBJS_TM0_148;E=js}', hideable: false, width: 150, sortable: true, dataIndex: 'Name', renderer: renderEditName },
                    { header: '@{R=Core.Strings;K=WEBJS_TM0_149;E=js}', width: 200, sortable: true, dataIndex: 'Description', renderer: renderString },
                    { header: '@{R=Core.Strings;K=WEBJS_TM0_150;E=js}', width: 200, sortable: true, dataIndex: 'FrequencyTitle', renderer: renderFrequencies },
                    { header: '@{R=Core.Strings;K=PCDATA_VB1_5;E=js}', width: 200, sortable: true, dataIndex: 'ActionTitles', renderer: renderActions },
                    { header: '@{R=Core.Strings;K=WEBJS_AK0_6;E=js}', width: 70, sortable: true, dataIndex: 'Enabled', renderer: renderToggle },
                    { id: 'reportsAssigned', header: '@{R=Core.Strings;K=WEBJS_TM0_147;E=js}', sortable: true, dataIndex: 'ReportsData', renderer: renderReports },
                    { header: '@{R=Core.Strings;K=WEBJS_TK0_35;E=js}', width: 150, sortable: true, dataIndex: 'UrlData', renderer: renderUrls },
                    { header: '@{R=Core.Strings;K=WEBJS_AY0_5;E=js}', width: 300, sortable: true, dataIndex: 'LastRun', renderer: renderLastRun }
                ],
                listeners: {
                    cellclick: function (grid, rowIndex, columnIndex, e) {
                        var fieldName = grid.getColumnModel().getDataIndex(columnIndex); // Get field name
                        if (fieldName == 'Enabled') {
                            if ($("#isOrionDemoMode").length != 0) {
                                demoAction("Core_Scheduler_Enable_DisableSchedule", this);
                                return;
                            }
                            var record = grid.getStore().getAt(rowIndex); // Get the Record
                            var data = record.get(fieldName);
                            var urls = JSON.parse(record.get('UrlData'));
                            if (data === false && record.get('AssignedReports') == 0 && (urls != null && urls.length == 0)) {
                                Ext.Msg.show({
                                    title: '@{R=Core.Strings;K=WEBJS_SO0_79;E=js}', //todo - clarify message with Andrew
                                    msg: '@{R=Core.Strings;K=WEBJS_SO0_80;E=js}',
                                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                                });
                                return;
                            }
                            var id = record.get('ReportJobID');

                            ORION.callWebService("/Orion/Services/ReportScheduler.asmx",
                                                 "EnableDisableJob", { jobId: id, value: !data },
                                                 function (result) {
                                                     if (result) {
                                                         record.set(fieldName, !stringToBoolean(data));
                                                         record.commit();
                                                     } else {
                                                         Ext.Msg.show({
                                                             title: '@{R=Core.Strings;K=WEBJS_AB0_29;E=js}', //todo - clarify message with Andrew
                                                             msg: '@{R=Core.Strings;K=WEBJS_AB0_30;E=js}',
                                                             icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                                                         });
                                                     }
                                                 });
                        }
                    },
                    columnmove: function (container, coulmn, from, to) {
                        renderReportsCountTooltip();
                    },
                    resize: function () {
                        renderReportsCountTooltip();
                        updateColumnWidth();
                    },
                    columnresize: function () {
                        renderReportsCountTooltip();
                    }
                },
                sm: selectorModel,
                autoScroll: true,
                autoExpandColumn: 'reportsAssigned',
                loadMask: true,
                height: 500,
                tbar: [
                    { id: 'Add', text: '@{R=Core.Strings;K=WEBJS_TM0_151;E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_TM0_151;E=js}', icon: '/Orion/Nodes/images/icons/icon_add.gif', cls: 'x-btn-text-icon', handler: addSchedule }, '-',
                    { id: 'Edit', text: '@{R=Core.Strings;K=WEBJS_TM0_152;E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_TM0_152;E=js}', icon: '/Orion/Nodes/images/icons/icon_edit.gif', cls: 'x-btn-text-icon', handler: editSchedule }, '-',
                    { id: 'DuplicateEdit', text: '@{R=Core.Strings;K=WEBJS_TM0_129;E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_TM0_129;E=js}', icon: '/Orion/images/Reports/duplicateEdit_report.png', cls: 'x-btn-text-icon', handler: dublicateEditSchedule }, '-',
                    { id: 'AssignToReport', text: '@{R=Core.Strings;K=WEBJS_TM0_153;E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_TM0_153;E=js}', icon: '/Orion/images/Reports/icon_assign_to.gif', cls: 'x-btn-text-icon', handler: assignToReport }, '-',
                    { id: 'RunNow', text: '@{R=Core.Strings;K=WEBJS_AB0_51;E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_AB0_51;E=js}', icon: '/Orion/Nodes/images/icons/icon_remanage.gif', cls: 'x-btn-text-icon', handler: startRunNowSchedules }, '-',
                    { id: 'Delete', text: '@{R=Core.Strings;K=CommonButtonType_Delete;E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_TM0_155;E=js}', icon: '/Orion/Nodes/images/icons/icon_delete.gif', cls: 'x-btn-text-icon',
                        handler: function () {
                            Ext.Msg.show({
                                title: '@{R=Core.Strings;K=WEBJS_TM0_155;E=js}',
                                msg: '@{R=Core.Strings;K=WEBJS_TM0_156;E=js}',
                                buttons: { yes: '@{R=Core.Strings;K=CommonButtonType_Delete; E=js}', cancel: '@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}' },
                                icon: Ext.Msg.ERROR,
                                fn: function (btn) {
                                    if (btn == 'yes') {
                                        deleteSelectedItems(getSelectedItemsArray(), function (result) {
                                            if (result && result != '')
                                                Ext.Msg.show({ title: "@{R=Core.Strings;K=WEBJS_TM0_155;E=js}", msg: result, minWidth: 500, buttons: Ext.Msg.CANCEL, icon: Ext.MessageBox.ERROR });

                                            if (comboArray.getValue() == "@{R=Core.Strings;K=WEBJS_VB0_76; E=js}") {
                                                refreshGridObjects(0, userPageSize, grid, "", "", "", filterText, function () { });
                                            } else {
                                                var sell = "";
                                                if (groupGrid.getSelectionModel().getCount() == 1) {
                                                    sell = groupGrid.getSelectionModel().getSelected().get("Value");
                                                }
                                                refreshGridObjects(null, null, groupGrid, comboArray.getValue(), "", "", filterText);
                                                refreshGridObjects(0, userPageSize, grid, comboArray.getValue(), "", sell, filterText, function () { });
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }, ' ', '->',
                    new Ext.ux.form.SearchField({
                        store: dataStore,
                        width: 200
                    })
                    ],
                bbar: pagingToolbar
            });

            // making columns visible or hidden according to saved configuration
            for (var i = 1; i < grid.getColumnModel().getColumnCount(); i++) {
                if (selectedColumns.indexOf(grid.getColumnModel().getDataIndex(i)) > -1) {
                    grid.getColumnModel().setHidden(i, false);
                } else {
                    grid.getColumnModel().setHidden(i, true);
                }
            }

            grid.getColumnModel().on('hiddenchange', function () {
                var cols = '';
                for (var i = 1; i < grid.getColumnModel().getColumnCount(); i++) {
                    if (!grid.getColumnModel().isHidden(i)) {
                        cols += grid.getColumnModel().getDataIndex(i) + ',';
                    }
                }
                ORION.Prefs.save('SelectedColumns', cols.slice(0, -1));
            });

            grid.store.on('beforeload', function () {
                grid.getEl().mask('@{R=Core.Strings;K=WEBJS_VB0_1; E=js}', 'x-mask-loading');
            });

            grid.on('sortchange', function (store, option) {
                var sort = option.field;
                var dir = option.direction;

                if (sort && dir) {
                    sortOrder[0] = sort;
                    sortOrder[1] = dir;
                    ORION.Prefs.save('SortOrder', sort + ',' + dir);
                }
            });

            grid.store.on('load', function () {
                grid.getEl().unmask();
                setMainGridHeight(userPageSize);
                updateColumnWidth();
                renderReportsCountTooltip();
                $("#selectionTip").remove();
                if (isSelectAllMode) {
                    isSelectAllMode = false;
                    updateToolbarButtons();
                }
            });

            grid.store.on("exception", function (dataProxy, type, action, options, response, arg) {
                //var error = eval("(" + response.responseText + ")");
                Ext.Msg.show({
                    title: '@{R=Core.Strings;K=WEBJS_VB0_133; E=js}',
                    //msg: '', TODO: error message here
                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                });
                grid.getEl().unmask();
            });

            groupGrid = new Ext.grid.GridPanel({
                id: 'groupingGrid',
                store: groupingStore,
                cls: 'hide-header',
                columns: [
                                { id: 'Value', width: 193, editable: false, sortable: false, dataIndex: 'DisplayNamePlural', renderer: renderGroup }
                            ],
                selModel: new Ext.grid.RowSelectionModel(),
                autoScroll: true,
                loadMask: true,
                listeners: {
                    cellclick: function (mygrid, row, cell, e) {
                        var val = mygrid.store.data.items[row].data[mygrid.store.data.items[row].fields.keys[cell]];
                        if (val == '[All]')
                            val = '';

                        var newGroupingValues = comboArray.getValue() + ',' + val;
                        ORION.Prefs.save('GroupingValue', newGroupingValues);
                        groupingValue = newGroupingValues.split(',');
                        refreshGridObjects(0, userPageSize, grid, comboArray.getValue(), "", val, filterText, function () {
                            $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function () {
                                this.tooltip = 'processed';
                                $.swtooltip(this);
                            });
                        });
                    }
                },
                anchor: '0 0', viewConfig: { forceFit: true }, split: true, autoExpandColumn: 'Value'
            });

            var groupByTopPanel = new Ext.Panel({
                id: 'groupByTopPanel',
                region: 'center',
                split: false,
                heigth: 50,
                collapsible: false,
                viewConfig: { forceFit: true },
                items: [new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_AK0_76; E=js}' }), comboArray],
                cls: 'panel-no-border panel-bg-gradient'
            });

            var navPanel = new Ext.Panel({
                id: 'navPanel',
                region: 'west',
                split: true,
                anchor: '0 0',
                width: 200,
                heigth: 500,
                collapsible: false,
                viewConfig: { forceFit: true },
                items: [groupByTopPanel, groupGrid],
                cls: 'panel-no-border'
            });

            navPanel.on("bodyresize", function () {
                groupGrid.setWidth(navPanel.getSize().width);
            });

            var mainGridPanel = new Ext.Panel({
                id: 'mainGrid',
                region: 'center',
                split: true,
                height: 500,
                layout: 'border',
                collapsible: false,
                items: [navPanel, grid],
                cls: 'no-border'
            });

            mainGridPanel.render('ReportSchedulerGrid');
            $(window).resize(function () {
                setMainGridHeight(userPageSize);
            });
            selectedJobName = $('#scheduleName').text();
            comboArray.setValue(groupingValue[0]);
            refreshGridObjects(null, null, groupGrid, groupingValue[0], groupingValue[0], "", "", function () {
                var selectedVal = groupGrid.getStore().find("Value", groupingValue[1]);
                if (selectedVal == -1) {
                    groupingValue[1] = "";
                    selectedVal = 0;
                }
                groupGrid.getSelectionModel().selectRow(selectedVal, false);
                getReportJobRowNumber(selectedJobName, groupingValue[0], "", groupingValue[1], selectCreatedSchedule);
            });
            updateToolbarButtons();
        }
    };
} ();

