﻿var AxisProperty = SW.Core.namespace('SW.Core.ReportChart.ChartEdit.AxisProperty');


    AxisProperty.ScaleMaxEnum = {
        AutoScale: '0',
        Fixed: '1'
    };

    AxisProperty.VisibleModeEnum = {
        Left: '0',
        Right: '1'
    };

    AxisProperty.AxisController = function(config) {
        /// <summary>
        /// Controller of axis properties
        /// </summary>
        /// <param name="config.lblTimeSeriesID">Uniqueidentifier of lblTimeSeries</param>
        /// <param name="config.clientID">Uniqueidentifier of client</param>
        /// <param name="config.dataSeriesSelectionMode">Uniqueidentifier of dataSeriesSelectionMode</param>
        /// <param name="config.ddScaleMaxID">Uniqueidentifier of ddScaleMax</param>
        /// <param name="config.ddChartFormatsID">Uniqueidentifier of ddChartFormats</param>
        /// <param name="config.tbMaxScaleID">Uniqueidentifier of tbMaxScale</param>
        /// <param name="config.dataSeriesTableID">Uniqueidentifier of dataSeriesTable</param>
        /// <param name="config.axisNonePlaceHolderID">Uniqueidentifier of axisNonePlaceHolder</param>
        /// <param name="config.axisPlaceHolderID">Uniqueidentifier of axisPlaceHolder</param>
        /// <param name="config.btnLessText">Uniqueidentifier of btnLessText - localized</param>
        /// <param name="config.btnMoreText">Uniqueidentifier of btnMoreText - localized</param>
        /// <param name="config.hfDataSeriesID">Uniqueidentifier of hfDataSeries</param>
        /// <param name="config.hfDataSeriesDetailsID">Uniqueidentifier of hfDataSeriesDetails</param>
        /// <param name="config.dataSeriesDetailsID">Uniqueidentifier of dataSeriesDetails</param>
        /// <param name="config.lblDataSeriesID">Uniqueidentifier of lblDataSeries</param>
        /// <param name="config.btnAddDataSeriesID">Uniqueidentifier of btnAddDataSeries</param>
        /// <param name="config.onSelectDetails">Uniqueidentifier of onSelectDetails</param>

        var self = this;
        self.fieldpicker = null;
        self.dataSeriesArray = [];

        SW.Core.Observer.makeObserver(self);

        initControl();

        function toggleMaximumValueVisibility() {

            var value = $('#' + config.ddScaleMaxID + ' option:selected').val();

            switch (value) {
                case AxisProperty.ScaleMaxEnum.AutoScale:
                    $('#' + config.tbMaxScaleID).css('display', 'none');
                    break;
                case AxisProperty.ScaleMaxEnum.Fixed:
                    $('#' + config.tbMaxScaleID).css('display', 'inline');
                    break;
                default:
                    $('#' + config.tbMaxScaleID).css('display', 'none');
                    break;
            }
        };

        function showDetailsBox() {
            self.dataSeriesDetails.css({ 'display': 'block' });
        }

        function hideDetailsBox() {
            self.dataSeriesDetails.css({ 'display': 'none' });
        }

        function toggleButtonDescription(index, mode) {

            $('.moreBtn').text(config.btnMoreText);
            $('.dataSerieRow .collapse').attr('src', '/Orion/images/Button.Expand.gif');

            var row = $('.dataSerieRow[index="' + index + '"][mode="' + mode + '"]');
            var btn = row.find('.moreBtn');
            if (typeof btn !== 'undefined' && btn != null && btn.length != 0) {
                btn.text(config.btnLessText);
            }

            var collapse = row.find('.collapse');
            if (typeof collapse !== 'undefined' && collapse != null && collapse.length != 0) {
                collapse.attr('src', '/Orion/images/Button.Collapse.gif');
            }
        }

        function untoggleButtonDescription(index, mode) {

            var row = $('.dataSerieRow[index="' + index + '"][mode="' + mode + '"]');
            var btn = row.find('.moreBtn');
            if (typeof btn !== 'undefined' && btn != null && btn.length != 0) {
                btn.text(config.btnMoreText);
            }

            var collapse = row.find('.collapse');
            if (typeof collapse !== 'undefined' && collapse != null && collapse.length != 0) {
                collapse.attr('src', '/Orion/images/Button.Expand.gif');
            }
        }

        function getTemplateDataSeries() {
            var templateDataSeries = {
                "Id": null,
                "Field": null,
                "TimeField": null,
                "DisplayName": null,
                "ColorMode": 0,
                "CustomColorHexValue": null,
                "AggregateMode": 0,
                "Aggregate": 0,
                "CustomAggregate": null,
                "ShowTrendLine": false,
                "Show95thPercentile": false
            };
            return templateDataSeries;
        }

        function persistsDataSeriesArray() {
            var json = JSON.stringify(self.dataSeriesArray);
            $('#' + config.hfDataSeriesID).val(json);
        }

        function loadPersistedDataSeries() {
            var hfDs = $('#' + config.hfDataSeriesID);
            if (hfDs == null || hfDs.val() == null || hfDs.val() == 'null')
                return;

            var jsonData = JSON.parse(hfDs.val());
            self.dataSeriesArray = jsonData;
        }

        function markTheDataSeriesRow() {
            var row = $(this);
            var displayName = row.find('.displayName');
            var removeSerie = row.find('.removeSerie');
            var collapse = row.find('.collapse');
            var backgroundColor = '#ffac36';
            var expand;
            if (config.dataSeriesSelectionMode == AxisProperty.VisibleModeEnum.Left) {
                expand = row.find('.leftExpand');
            } else {
                expand = row.find('.rightExpand');
            }
            
            markTheDataSeriesRowInternal(backgroundColor, displayName, removeSerie, collapse, expand);
        }

        function markTheDataSeriesRowInternal(backgroundColor, displayName, removeSerie, collapse, expand) {
            displayName.css({
                'background-color': backgroundColor,
                'color': 'white'
            });

            removeSerie.css({
                'background-color': backgroundColor,
                'color': 'white'
            });

            expand.css({
                'background-color': 'white'
            });

            var removeBtn = removeSerie.find('.resource-manage-btn');
            removeBtn.attr('src', '/Orion/images/Reports/delete_icon_White16x16.png');
        }
        
        function markExpand(expand, expandColumn, backgroundColor) {
            expand.css({
                'border-top': '1px solid ' + backgroundColor,
                'border-bottom': '1px solid ' + backgroundColor,
                'width': '100%'
            });

            if (config.dataSeriesSelectionMode == AxisProperty.VisibleModeEnum.Left) {
                expandColumn.css({
                    'padding-right': '0px'
                });
            } else {
                expandColumn.css({
                    'padding-left': '0px'
                });
            }
        }

        function clearDataSerieMarks() {
            $('.dataSerieRow .displayName').css({
                'background-color': '#e0e0e0',
                'color': 'black'
            });

            $('.rightExpand').css({
                'background-color': '#ecf6fb',
                'border': '0px'
            });

            $('.leftExpand').css({
                'background-color': '#ecf6fb',
                'border': '0px'
            });

            $('.expandLeftColumn').css({
                'padding-right' : '4px' 
            });

            $('.expandRightColumn').css({
                'padding-left': '4px'
            });

            var remove = $('.dataSerieRow .removeSerie');
            var removeBtn = remove.find('.resource-manage-btn');

            remove.css({
                'background-color': '#e0e0e0',
                'color': 'black'
            });

            removeBtn.attr('src', '/Orion/images/Reports/delete_icon16x16.png');
            
            var allRows = $('.dataSerieRow');
            allRows.off('hover');
            allRows.attr({ 'select': 'false' });
            allRows.hover(markTheDataSeriesRow, unmarkTheDataSeriesRow);
        }

        function clickOnDataSerie() {

            var index = $(this).parent().attr('index');
            var mode = $(this).parent().attr('mode');

            self.showDetails(index, mode);
        }

        function unmarkTheDataSeriesRow() {
            var row = $(this);
            var displayName = row.find('.displayName');
            var removeSerie = row.find('.removeSerie');
            var collapse = row.find('.collapse');
            var expand;
            if (config.dataSeriesSelectionMode == AxisProperty.VisibleModeEnum.Left) {
                expand = row.find('.leftExpand');
            } else {
                expand = row.find('.rightExpand');
            }

            unmarkTheDataSeriesRowInternal('#e0e0e0', displayName, removeSerie, collapse, expand);
        }

        function unmarkTheDataSeriesRowInternal(backgroundColor, displayName, removeSerie, collapse, expand)
        {
            displayName.css({
                'background-color': backgroundColor,
                'color': 'black'
            });

            removeSerie.css({
                'background-color': backgroundColor,
                'color': 'black'
            });

            expand.css({
                'background-color': '#ecf6fb',
                'border' : '1px'
            });

            var removeBtn = removeSerie.find('.resource-manage-btn');
            removeBtn.attr('src', '/Orion/images/Reports/delete_icon16x16.png');

        }

        function unmarkExpand(expand, expandColumn) {
            expand.css({
                'border-top': '1px solid white',
                'border-bottom': '1px solid white',
                'width': '100%'
            });

            if (config.dataSeriesSelectionMode == AxisProperty.VisibleModeEnum.Left) {
                expandColumn.css({
                    'padding-right': '4px'
                });
            } else {
                expandColumn.css({
                    'padding-left': '4px'
                });
            }
        }

        // The DisplayName of data series can be set to empty string by user
        // In this case we should display something better than empty string in other controls (e.g. dataSeries list or sortBy list)
        function getDataSeriesDisplayName(dataSeries) {
            if (dataSeries.DisplayName !== '')
                return dataSeries.DisplayName;

            if (dataSeries.Field)
                return dataSeries.Field.DisplayName;

            return dataSeries.Id;
        }

        function drawAllDataSeries() {
            var dataseriesTable = $('#' + config.dataSeriesTableID);
            if (dataseriesTable == null)
                return;

            var selectedDataRow = $('.dataSerieRow[select="true"]');
            var selectedDataRowIndex = selectedDataRow.attr('index');
            var selectedDataRowMode = selectedDataRow.attr('mode');

            dataseriesTable.empty();

            if (self.dataSeriesArray == null || self.dataSeriesArray.length == 0) {
                $('#' + config.axisNonePlaceHolderID).show();
                $('#' + config.axisPlaceHolderID).css({ 'display': 'none' });
                $('#' + config.lblDataSeriesID).css({ 'display': 'none' });
                dataseriesTable.css({ 'display': 'none' });
                return;
            }

            for (var i = 0; i < self.dataSeriesArray.length; i++) {
                var row = prepareDataseriesTableRow(self.getDataSeriesDisplayName(self.dataSeriesArray[i]), i);
                dataseriesTable.append(row);
            }

            if (self.dataSeriesArray.length > 0) {
                $('#' + config.axisNonePlaceHolderID).css({ 'display': 'none' });
                $('#' + config.axisPlaceHolderID).css({ 'display': 'block' });
                $('#' + config.lblDataSeriesID).css({ 'display': 'inline' });
                dataseriesTable.css({ 'display': 'block' });

            }

            var dataSerieRow = $('.dataSerieRow[mode="' + config.dataSeriesSelectionMode + '"]');
            var displayName = dataSerieRow.find('.displayName');

            var expand;
            if (config.dataSeriesSelectionMode == AxisProperty.VisibleModeEnum.Left) {
                expand = dataSerieRow.find('.expandLeftColumn');
            } else {
                expand = dataSerieRow.find('.expandRightColumn');
            }

            bindClickFunction(dataSerieRow, displayName, expand);

            markSelectedRow(selectedDataRowIndex, selectedDataRowMode);
        }

        function markSelectedRow(selectedDataRowIndex, selectedDataRowMode) {

            var selectedRow = $('.dataSerieRow[index="' + selectedDataRowIndex + '"][mode="' + selectedDataRowMode + '"]');
            if (typeof selectedRow !== 'undefined' && selectedRow != null && selectedRow.length != 0) {

                var selectedDisplayName = selectedRow.find('.displayName');
                var selectedRemoveSerie = selectedRow.find('.removeSerie');
                var selectedCollapse = selectedRow.find('.collapse');
                var selectedBackgroundColor = '#ffac36';
                var selectedExpand;
                var selectedExpandColumn;
                if (selectedDataRowMode == AxisProperty.VisibleModeEnum.Left) {
                    selectedExpand = selectedRow.find('.leftExpand');
                    selectedExpandColumn = selectedRow.find('.expandLeftColumn');
                } else {
                    selectedExpand = selectedRow.find('.rightExpand');
                    selectedExpandColumn = selectedRow.find('.expandRightColumn');
                }

                selectedRow.off('hover');
                selectedRow.attr({ 'select': 'true' });

                markTheDataSeriesRowInternal(selectedBackgroundColor, selectedDisplayName, selectedRemoveSerie, selectedCollapse, selectedExpand);
                markExpand(selectedExpand, selectedExpandColumn, selectedBackgroundColor);
                toggleButtonDescription(selectedDataRowIndex, selectedDataRowMode);
            }
        }

        function bindClickFunction(dataSerieRow, displayName, expand) {
            dataSerieRow.off('hover');
            dataSerieRow.hover(markTheDataSeriesRow, unmarkTheDataSeriesRow);

            displayName.off('click').on('click', clickOnDataSerie);
            expand.off('click').on('click', clickOnDataSerie);
        }

        function prepareDataseriesTableRow(displayName, index) {
            var row = '<tr class="dataSerieRow" index="' + index + '" mode="' + config.dataSeriesSelectionMode + '">';
            var moreBtn = '<img class="collapse" id="collapse' + config.clientID + '_' + index + '" src="/Orion/images/Button.Expand.gif"></img><a href="#" onClick="return false;" class="moreBtn" id="btn' + config.clientID + '_' + index + '" mode="' + config.dataSeriesSelectionMode + '">' + config.btnMoreText + '</a>';

            if (config.dataSeriesSelectionMode === AxisProperty.VisibleModeEnum.Right) {
                row += '<td class="expandRightColumn"><span class="rightExpand">' + moreBtn + '</span></td>';
            }

            row +=
                '<td class="displayName">' + displayName + '</td>' +
                '<td class="removeSerie"><a href="#" onClick="controler' + config.clientID + '.removeDataSeries(' + index + '); return false;"> <img class="resource-manage-btn" title="Delete" alt="Delete" src="/Orion/images/Reports/delete_icon16x16.png"><a/></td>';

            if (config.dataSeriesSelectionMode === AxisProperty.VisibleModeEnum.Left) {
                row += '<td class="expandLeftColumn"><span class="leftExpand">' + moreBtn + '</span></td>';
            }
            row += '</tr>';

            return row;
        };

        function tryLoadAssociatedTimestampField(picker, series) {
            picker.getTimestampForField(series.Field, function (timestampField, sourceField) {
                if (timestampField) {
                    series.TimeField = timestampField.Field;
                    persistsDataSeriesArray();
                }
            });
        }

        function dataBind() {
            loadPersistedDataSeries();
            drawAllDataSeries();
        };

        function initControl() {

            $(function () {

                self.dataSeriesDetails = $('#' + config.dataSeriesDetailsID);

                $('#' + config.ddScaleMaxID).change(function () {

                    toggleMaximumValueVisibility();
                });

                $('#' + config.btnAddDataSeriesID).click(function (event) {
                    event.preventDefault();
                    event.stopPropagation();

                    self.fieldpicker.SetIsStatisticFilter(true);
                    // Only types which are convertible to double are shown for historical chart.
                    self.fieldpicker.SetDataTypesFilters(SW.Core.Pickers.DataDeclarationType.Float, SW.Core.Pickers.DataDeclarationType.Integer);
                    self.fieldpicker.ShowInDialog();
                });

                var dataSeriesTable = $('#' + config.dataSeriesTableID);
                if (config.dataSeriesSelectionMode == AxisProperty.VisibleModeEnum.Left) {
                    dataSeriesTable.css({ 'left': '9px' });
                } else {
                    dataSeriesTable.css({ 'right': '9px' });
                }

                toggleMaximumValueVisibility(config.ddScaleMaxID, config.tbMaxScaleID);

                // Hide/Show custom unit visibility.
                $('#' + config.ddUnitsDisplayedID).change(function () {
                
                    var $tbCustomUnit = $('#' + config.tbCustomUnitID);
                    if ($(this).val() == 'custom') {
                        $tbCustomUnit.show();
                    } else {
                        $tbCustomUnit.hide();
                        $tbCustomUnit.val('');
                    }
                });

                dataBind();
                var editAxisPropertiesControlCreatedCallback = config.editAxisPropertiesControlCreatedCallback;
                if (editAxisPropertiesControlCreatedCallback && jQuery.isFunction(editAxisPropertiesControlCreatedCallback)) {
                    editAxisPropertiesControlCreatedCallback(self);
                }
            });
        }

        this.onFieldPickerCreated = function (picker) {
            self.fieldpicker = picker;
        };

        this.onFieldPickerSelected = function (picker, customData) {

            var data = picker.GetSelectedFields();
            if (typeof data !== 'undefined' && typeof data[0] != 'undefined') {
                data = data[0];

                var item = getTemplateDataSeries();
                item.DisplayName = data.DisplayName;
                item.Field = data.Field;

                //TODO make it bulletproof use GUID
                item.Id = data.RefID + '_' + self.dataSeriesArray.length + '_' + config.dataSeriesSelectionMode;

                self.dataSeriesArray.push(item);
                tryLoadAssociatedTimestampField(picker, item);

                drawAllDataSeries();
                persistsDataSeriesArray();

                self.trigger('dataSerieAdded', self, { Item: item });

            } else {
                //console.log('nothing returned!!');
            }
        };

        this.removeDataSeries = function (index) {
            if (index == undefined || self.dataSeriesArray == null || self.dataSeriesArray.length < index) {
                return false;
            }

            var itemToRemove = self.dataSeriesArray[index];
            self.dataSeriesArray.splice(index, 1);
            drawAllDataSeries();
            persistsDataSeriesArray();

            var details = $('#' + config.hfDataSeriesDetailsID);
            var detailsIndex = details.attr('index');
            var detailsForMode = details.attr('mode');
            if (detailsIndex != undefined && detailsForMode != undefined && detailsIndex == index && detailsForMode == config.dataSeriesSelectionMode) {
                hideDetailsBox();
            }

            self.trigger('dataSerieRemoved', self, { Item: itemToRemove });
            return false;
        };

        this.showDetails = function (index, mode) {
            if (typeof index === 'undefined' || self.dataSeriesArray == null || self.dataSeriesArray.length < index) {
                return false;
            }

            var details = $('#' + config.hfDataSeriesDetailsID);
            if (typeof details !== 'undefined' && details != null) {
                details.val(JSON.stringify(self.dataSeriesArray[index]));
                details.attr({
                    'index': index,
                    'mode': mode,
                    'updateCallback': 'controler' + config.clientID + '.getUpdatedDetails'
                });
            }

            var clickOnSameRow = $('.dataSerieRow[select="true"][index="' + index + '"][mode="' + mode + '"]').length == 1;
            var row = $('.dataSerieRow[index="' + index + '"][mode="' + mode + '"]');

            var displayName = row.find('.displayName');
            var removeSerie = row.find('.removeSerie');
            var collapse = row.find('.collapse');
            var backgroundColor = '#f99d1c';
            var backgroundColorDefault = '#e0e0e0';
            var expandColumn;
            var expand;
            if (mode == AxisProperty.VisibleModeEnum.Left) {
                expand = row.find('.leftExpand');
                expandColumn = row.find('.expandLeftColumn');
            } else {
                expand = row.find('.rightExpand');
                expandColumn = row.find('.expandRightColumn');
            }
            
            clearDataSerieMarks();

            if (clickOnSameRow) {
                hideDetailsBox();
                unmarkTheDataSeriesRowInternal(backgroundColorDefault, displayName, removeSerie, collapse, expand);
                unmarkExpand(expand, expandColumn);
                untoggleButtonDescription(index, mode);

            } else {

                row.off('hover');
                row.attr({ 'select': 'true' });

                showDetailsBox();
                markTheDataSeriesRowInternal(backgroundColor, displayName, removeSerie, collapse, expand);
                markExpand(expand, expandColumn, backgroundColor);
                toggleButtonDescription(index, mode);
            }

            if (jQuery.isFunction(eval(config.onSelectDetails))) {
                eval(config.onSelectDetails)();
            }

            return false;
        };

        this.getUpdatedDetails = function (data, index) {

            if (typeof data === 'undefined' || typeof index === 'undefined') {
                return;
            }

            self.dataSeriesArray[index] = data;
            drawAllDataSeries();
            persistsDataSeriesArray();
        };

        this.getAllDataSeries = function () {
            return _.clone(self.dataSeriesArray);
        };

        this.getDataSeriesDisplayName = function (dataSeries) {
            return getDataSeriesDisplayName(dataSeries);
        }
    };