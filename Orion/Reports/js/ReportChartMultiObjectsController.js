﻿var ChartEdit = SW.Core.namespace('SW.Core.ReportChart.ChartEdit');

ChartEdit.FIELD_TYPE_ENUM = {
    KEY: 0,
    CAPTION: 1
};

ChartEdit.MultiObjectsController = function (config) {
    var self = this;
    var fieldPicker = null;
    var editLeftAxisPropertiesControl = null;
    var editRightAxisPropertiesControl = null;
    var secondarySortControl = null;
    var secondarySortFieldPickerControl = null;

    this.OnFieldPickerCreated = function (picker) {
        fieldPicker = picker;
    };

    this.OnEditLeftAxisPropertiesControlCreated = function (editAxisPropertiesControl) {
        self.editLeftAxisPropertiesControl = editAxisPropertiesControl;
        self.editLeftAxisPropertiesControl.on("dataSerieAdded", function (sender, eventArgs) {
            refreshSortBy(eventArgs.Item);
        });

        self.editLeftAxisPropertiesControl.on("dataSerieRemoved", function (sender, eventArgs) {
            removeDataSerieFromSortByList(eventArgs.Item);
        });

        var dataSerieItems = self.editLeftAxisPropertiesControl.getAllDataSeries();
        $.each(dataSerieItems, function (index, value) {
            addDataSerieIntoSortByList(value, self.editLeftAxisPropertiesControl.getDataSeriesDisplayName(value));
        });

        $('#' + config.ddlSortByID).val($('#' + config.hdnSortByID).val());
    };

    this.OnEditRightAxisPropertiesControlCreated = function (editAxisPropertiesControl) {
        self.editRightAxisPropertiesControl = editAxisPropertiesControl;
        self.editRightAxisPropertiesControl.on("dataSerieAdded", function (sender, eventArgs) {
            refreshSortBy();
        });

        self.editRightAxisPropertiesControl.on("dataSerieRemoved", function (sender, eventArgs) {
            removeDataSerieFromSortByList(eventArgs.Item);
        });

        var dataSerieItems = self.editRightAxisPropertiesControl.getAllDataSeries();
        $.each(dataSerieItems, function (index, value) {
            addDataSerieIntoSortByList(value, self.editRightAxisPropertiesControl.getDataSeriesDisplayName(value));
        });

        $('#' + config.ddlSortByID).val($('#' + config.hdnSortByID).val());
    };

    this.OnFieldSelected = function (picker, customData) {
        var selectedField = _.first(picker.GetSelectedFields());

        var empty = !selectedField;

        if (customData.fieldType == ChartEdit.FIELD_TYPE_ENUM.KEY) {

            if (empty) {
                cleanKeyField();
            } else {
                $('#' + config.lblKeyFieldDisplayNameID).text(selectedField.DisplayName);
                $('#' + config.hfKeyFieldID).val(JSON.stringify(selectedField.Field));
            }

        }

        else if (customData.fieldType == ChartEdit.FIELD_TYPE_ENUM.CAPTION) {

            if (empty) {
                cleanCaptionField();
            }
            else {
                $('#' + config.lblCaptionFieldDisplayNameID).text(selectedField.DisplayName);
                $('#' + config.hfCaptionFieldID).val(JSON.stringify(selectedField.Field));
            }

        }
    };

    this.OnSecondarySortControlCreated = function (secondarySortControl) {
        self.secondarySortControl = secondarySortControl;
        self.secondarySortControl.InitializeSortColumnsByOrderColumnsConfig();
        if (self.secondarySortFieldPickerControl) {
            self.secondarySortControl.SetFieldPicker(self.secondarySortFieldPickerControl);
        }
    };

    this.OnSecondarySortFieldPickerControlCreated = function (secondarySortFieldPickerControl) {
        self.secondarySortFieldPickerControl = secondarySortFieldPickerControl;
        if (self.secondarySortControl) {
            self.secondarySortControl.SetFieldPicker(self.secondarySortFieldPickerControl);
        }
    };

    this.OnSecondarySortFieldPickerControlSelected = function (picker, customData) {
        if (self.secondarySortControl) {
            var selectedField = _.first(picker.GetSelectedFields());
            self.secondarySortControl.AddColumn(selectedField.RefID, selectedField.Field.DisplayName, generateNewGuid(), "Ascending", false);
        }
    };

    this.removeKeyField = function () {

        cleanKeyField();
        return false;
    };

    this.removeCaptionField = function () {

        cleanCaptionField();
        return false;
    };

    function refreshSortBy(addingItem) {
        var $ddlSortBy = $("#" + config.ddlSortByID);
        var $hdnSortByID = $('#' + config.hdnSortByID);
        if (($ddlSortBy.children().length == 0) && (typeof (addingItem) != 'undefined')) {
            $hdnSortByID.val(JSON.stringify(addingItem.Field));
        }

        $ddlSortBy.empty();
        var dataSerieItems = null;
        if (self.editLeftAxisPropertiesControl && self.editLeftAxisPropertiesControl != null) {
            dataSerieItems = self.editLeftAxisPropertiesControl.getAllDataSeries();
            $.each(dataSerieItems, function (index, value) {
                addDataSerieIntoSortByList(value, self.editLeftAxisPropertiesControl.getDataSeriesDisplayName(value));
            });
        }

        if (self.editRightAxisPropertiesControl && self.editRightAxisPropertiesControl != null) {
            dataSerieItems = self.editRightAxisPropertiesControl.getAllDataSeries();
            $.each(dataSerieItems, function (index, value) {
                addDataSerieIntoSortByList(value, self.editRightAxisPropertiesControl.getDataSeriesDisplayName(value));
            });
        }

        $ddlSortBy.val($hdnSortByID.val());
        refreshDefaultAggregation();
    };

    /// <summary>
    /// This method generates new Guid identifier similarry as Guid.NewGuid() in .NET do.
    /// </summary>
    function generateNewGuid() {
        var g1 = function () {
            return Math.floor((1 + Math.random()) * 0x10000)
             .toString(16)
             .substring(1);
        };

        return g1() + g1() + '-' + g1() + '-' + g1() + '-' + g1() + '-' + g1() + g1() + g1();
    };

    function addDataSerieIntoSortByList(item, displayName) {
        $("#" + config.ddlSortByID).append("<option value='" + JSON.stringify(item.Field) + "'>" + displayName + "</option>");
    };

    function removeDataSerieFromSortByList(item) {
        var $ddlSortByID = $("#" + config.ddlSortByID);
        $ddlSortByID.find("option[value='" + JSON.stringify(item.Field) + "']").remove();
        $('#' + config.hdnSortByID).val($ddlSortByID.val());
        refreshDefaultAggregation();
    };

    function cleanCaptionField() {
        $('#' + config.lblCaptionFieldDisplayNameID).text('');
        $('#' + config.hfCaptionFieldID).val('');
    };

    function cleanKeyField() {
        $('#' + config.lblKeyFieldDisplayNameID).text('');
        $('#' + config.hfKeyFieldID).val('');
    };


    /// <summary>
    /// If currently select SortBy column has default aggregation, it will be used as current aggregation
    /// </summary>
    function refreshDefaultAggregation() {
        var notSpecifiedAggId = "0";
        var defaultAgg = notSpecifiedAggId;
        try{
			defaultAgg = JSON.parse($('#' + config.hdnSortByID).val()).DataTypeInfo.DefaultAggregation;
		}catch(e){}
        if (defaultAgg != notSpecifiedAggId)
            $('#' + config.ddlDataAggregationID).val(defaultAgg);
    }

    function solveEnableDisableFilterControlsBySelOption() {
        var $sortingFrame1 = $('#' + config.sortingFrame1ID);
        var $sortingFrame2 = $('#' + config.sortingFrame2ID);
        var $sortRecordsControl = null;

        if ($('#' + config.filter1RbFilterID).is(':checked')) {
            $('#' + config.tbTopXXFilterID).removeAttr('disabled');
            
            $sortingFrame1.show();
            $sortingFrame2.hide();

            $sortRecordsControl = $sortingFrame2.find("div:eq(0)");
            $sortRecordsControl.appendTo($sortingFrame1);
        }
        else {
            $('#' + config.tbTopXXFilterID).attr('disabled', 'disabled');
        }

        if ($('#' + config.filter2RbFilterID).is(':checked')) {
            $('#' + config.tbTopPercentFilterID).removeAttr('disabled');
            $sortingFrame2.show();
            $sortingFrame1.hide();

            $sortRecordsControl = $sortingFrame1.find("div:eq(0)");
            $sortRecordsControl.appendTo($sortingFrame2);
        }
        else {
            $('#' + config.tbTopPercentFilterID).attr('disabled', 'disabled');
        }

        if ($('#' + config.showAllRbFilterID).is(':checked')) {
            $sortingFrame1.hide();
            $sortingFrame2.hide();
        }
    };

    function returnJustNumerics(value) {
        if (typeof (value) != 'undefined') {
            return value.replace(/[^\d]+/g, '');
        }

        return value;
    };

    function filterJustNumericKeys(event) {
        // While browsers use differing properties to store this information, 
        // jQuery normalizes the .which property so you can reliably use it to retrieve the character code.
        if (event.which != 46 && event.which != 8 && event.which != 48) {
            if (!parseInt(String.fromCharCode(event.which))) {
                event.preventDefault();
                return;
            }
        }
    };

    // Toggle visibility of custom data grouping section and filtering section based on data grouping checkbox
    function toggleCustomDataGrouping() {
        var $checkbox = $("#" + config.chbCustomDataGroupingID);

        if ($checkbox.length) {
            // Proceed only when checkbox exists on page

            var customDataGroupingEnabled = $checkbox.is(':checked');

            $('#' + config.trCustomDataGroupingPropertiesID).toggle(customDataGroupingEnabled);
        }
    }

    function init() {

        $(function () {

            $('#' + config.btnSelectKeyFieldID).click(function (event) {

                event.preventDefault();
                event.stopPropagation();

                fieldPicker.ShowInDialog({ fieldType: ChartEdit.FIELD_TYPE_ENUM.KEY });

                return false;
            });


            $('#' + config.btnSelectCaptionFieldID).click(function (event) {

                event.preventDefault();
                event.stopPropagation();

                fieldPicker.ShowInDialog({ fieldType: ChartEdit.FIELD_TYPE_ENUM.CAPTION });

                return false;
            });

            $('#' + config.btnDeleteKeyFieldID).click(self.removeKeyField);
            $('#' + config.btnDeleteCaptionFieldID).click(self.removeCaptionField);

            $('#' + config.showAllRbFilterID).on("change", function () {
                solveEnableDisableFilterControlsBySelOption();
            });

            $('#' + config.filter1RbFilterID).on("change", function () {
                solveEnableDisableFilterControlsBySelOption();
            });

            $('#' + config.filter2RbFilterID).on("change", function () {
                solveEnableDisableFilterControlsBySelOption();
            });

            $('#' + config.ddlSortByID).on("change", function () {
                $('#' + config.hdnSortByID).val($(this).val());
                refreshDefaultAggregation();
            });

            var $tbTopXXFilter = $('#' + config.tbTopXXFilterID);
            $tbTopXXFilter.on("change", function () {
                $tbTopXXFilter.val(returnJustNumerics($tbTopXXFilter.val()));
            });

            $tbTopXXFilter.on("keypress", function (event) {
                filterJustNumericKeys(event);
            });

            var $tbTopPercentFilter = $('#' + config.tbTopPercentFilterID);
            $tbTopPercentFilter.on("change", function () {
                $tbTopPercentFilter.val(returnJustNumerics($tbTopPercentFilter.val()));
            });

            $tbTopPercentFilter.on("keypress", function (event) {
                filterJustNumericKeys(event);
            });

            $("#" + config.sortingFrame1ID).find("#moreDetailtext").on("click", function (event) {
                var $parentEl = $(this).parent();
                $parentEl.parent().find("#advancedSorting").hide();
                $parentEl.find("#lessDetailtext").show();
                $(this).parent().find("img.collapsed").attr("src", "/Orion/images/Button.Expand.gif");
                $(this).hide();
            });

            $("#" + config.sortingFrame1ID).find("#lessDetailtext").on("click", function (event) {
                var $parentEl = $(this).parent();
                $parentEl.parent().find("#advancedSorting").show();
                $parentEl.find("#moreDetailtext").show();
                $parentEl.find("img.collapsed").attr("src", "/Orion/images/Button.Collapse.gif");
                $(this).hide();
            });

            $("#" + config.sortingFrame1ID + " #moreLessDetails img.collapsed").on("click", function (event) {
                var srcImg = $(this).attr("src");
                var $parentEl = $(this).parent();
                if (srcImg == "/Orion/images/Button.Expand.gif") {
                    // we should invoke expand
                    $parentEl.parent().find("#advancedSorting").show();
                    $parentEl.find("#moreDetailtext").show();
                    $parentEl.find("img.collapsed").attr("src", "/Orion/images/Button.Collapse.gif");
                    $parentEl.find("#lessDetailtext").hide();
                } else {
                    // we should invoke collapse
                    $parentEl.parent().find("#advancedSorting").hide();
                    $parentEl.find("#moreDetailtext").hide();
                    $parentEl.find("img.collapsed").attr("src", "/Orion/images/Button.Expand.gif");
                    $parentEl.find("#lessDetailtext").show();
                }
            });

            $("#" + config.chbCustomDataGroupingID).on("change", function () {
                toggleCustomDataGrouping();
            });

            solveEnableDisableFilterControlsBySelOption();

            toggleCustomDataGrouping();
        });

    }

    init();
};
