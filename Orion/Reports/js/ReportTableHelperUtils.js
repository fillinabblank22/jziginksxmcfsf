﻿SW.Core.namespace('SW.Core.TableResource');
SW.Core.TableResource.ReportTableHelperUtils = (function () {
    return {
        FindColumn: function (arrayColumns, refId) {
            /// <summary>Method try to find right column by passed parameter refId in array passed in parameter arrayColumns </summary>
            /// <param name="arrayColumns" type="array of object">List of objects which has property refid which will be searched </param>
            /// <param name="refId" type="uniqueidentifier">Indentifier of column which we want to find </param>
            /// <returns type="int">-1 in the case, that no column is found, otherwise index of column </returns>

            var res = -1;
            if (jQuery.isArray(arrayColumns)) {
                for (var i = 0; i < arrayColumns.length; i++) {
                    if (arrayColumns[i].refid == refId) {
                        return i;
                    }
                }
            }

            return res;
        }
    };
})();