﻿var ChartEdit = SW.Core.namespace('SW.Core.ReportChart.ChartEdit');

ChartEdit.ColorModeEnum = {
    Standard: '0',
    Custom: '1'
};

ChartEdit.DataSeriesDetailController = function (config) {
    /// <summary>
    /// Constroller of details overview
    /// </summary>
    /// <param name="config.lblTimeSeriesID">Uniqueidentifier of lblTimeSeries</param>    
    /// <param name="config.lblSeriesNameID">Uniqueidentifier of lblSeriesNameID</param>
    /// <param name="config.ddSelectColorModeID">Uniqueidentifier of ddSelectColorModeID</param>
    /// <param name="config.colorSelectorID">Uniqueidentifier of colorSelectorID</param>
    /// <param name="config.btnTimeSeriesID">Uniqueidentifier of btnTimeSeriesID</param>
    /// <param name="config.hfSeriesDetailsID">Uniqueidentifier of hfSeriesDetailsID</param>
    /// <param name="config.chooseTextI18N">Uniqueidentifier of chooseTextI18N</param>
    /// <param name="config.cancelTextI18N">Uniqueidentifier of cancelTextI18N</param>
    /// <param name="config.tbDisplayNameID">Uniqueidentifier of tbDisplayNameID</param>
    /// <param name="config.hostingDivID">Uniqueidentifier of hostingDivID</param>

    var self = this;    
    self.fieldpickerDataSeries = null;
    self.fieldpickerTimeSeries = null;
    self.detailsData = null;
    self.displayNameText = null;

    function toggleColorPickerVisibility() {
        var value = $('#' + config.ddSelectColorModeID + ' option:selected').val();

        switch (value) {
            case ChartEdit.ColorModeEnum.Standard:
                $('#' + config.colorSelectorID).parent().css('display', 'none');
                break;
            case ChartEdit.ColorModeEnum.Custom:
                $('#' + config.colorSelectorID).parent().css('display', 'inline');
                break;
            default:
                $('#' + config.colorSelectorID).parent().css('display', 'none');
                break;
        }
    };

    function displayNameChanged() {
        var display = $('#' + config.tbDisplayNameID);
        if (display.val() != self.displayNameText) {
            self.displayNameText = $("<div>").text(display.val()).html();
            self.detailsData.DisplayName = self.displayNameText;
            onDataUpdated();
        }
    }

    function cbShowTrendLineChanged() {
        var cbShowTrendLine = $('#' + config.cbShowTrendLineID);
        self.detailsData.ShowTrendLine = cbShowTrendLine.prop('checked');
        onDataUpdated();
    }

    function cbCalculate95thPercentileLineChanged() {
        var cbCalculate95thPercentileLine = $('#' + config.cbCalculate95thPercentileLineID);
        self.detailsData.Show95thPercentile = cbCalculate95thPercentileLine.prop('checked');
        onDataUpdated();
    }

    function ddSelectColorModeChanged() {
        var ddSelectColorMode = $('#' + config.ddSelectColorModeID);
        self.detailsData.ColorMode = ddSelectColorMode.val();

        if (self.detailsData.ColorMode == 0) {
            self.detailsData.CustomColorHexValue = null;
        }
        else {
            colorSelectorChanged();
        }

        onDataUpdated();
    }

    function colorSelectorChanged() {
        var colorSelector = $('#' + config.colorSelectorID);
        self.detailsData.CustomColorHexValue = colorSelector.val();
        onDataUpdated();
    }

    function onDataUpdated() {
        var details = $('#' + config.hfSeriesDetailsID);
        var index = details.attr('index');
        var callBack = details.attr('updatecallback');

        if (jQuery.isFunction(eval(callBack))) {
            (eval(callBack))(self.detailsData, index);
        }
    };

    function getDisplayNameForField(field) {
        var displayName = "";
        if (typeof (field) != "undefined") {
            if (field.OwnerDisplayName != null) {
                displayName += field.OwnerDisplayName + "/" + field.DisplayName;
            } else {
                displayName = field.DisplayName;
            }
        }

        return displayName;
    };

    function tryLoadAssociatedTimestampField(picker, field) {
        picker.getTimestampForField(field, function (timestampField, sourceField) {
            if (timestampField) {
                
                $('#' + config.lblTimeSeriesID).text(getDisplayNameForField(timestampField));
                self.detailsData.TimeField = timestampField.Field;

                onDataUpdated();
            }
        });
    }

    function setContentHeight() {
        var hostingDiv = $('#' + config.hostingDivID);
        var parentColumn = hostingDiv.parent();

        hostingDiv.css({
            'height': parentColumn.height() -2
        });
    }

    function initControl() {

        $(function () {
            $('#' + config.btnTimeSeriesID).click(function (event) {
                event.preventDefault();
                event.stopPropagation();

                self.fieldpickerTimeSeries.SetDataTypeFilter(SW.Core.Pickers.DataDeclarationType.DateTime);
                self.fieldpickerTimeSeries.ShowInDialog();
            });
            
            $('#' + config.colorSelectorID).spectrum({
                color: '#000',
                preferredFormat: 'hex6',
                showInput: true,
                chooseText: config.chooseTextI18N,
                cancelText: config.cancelTextI18N
            });

            $('#' + config.ddSelectColorModeID).change(toggleColorPickerVisibility);

            toggleColorPickerVisibility();
            setContentHeight();
        });
    }

    initControl();

    this.onFieldPickerDataSerieCreated = function(picker) {
        self.fieldpickerDataSeries = picker;
    };

    this.onFieldPickerTimeSeriesCreated = function(picker) {
        self.fieldpickerTimeSeries = picker;
    };

    this.onDataSeriesFieldSelected = function(fieldPicker, customData) {

        var data = self.fieldpickerDataSeries.GetSelectedFields();
        if (data != undefined && data[0] != undefined) {
            data = data[0];

            $('#' + config.lblSeriesNameID).text(getDisplayNameForField(data.Field));
            $('#' + config.tbDisplayNameID).val(data.Field.DisplayName);

            self.detailsData.Field = data.Field;
            self.detailsData.DisplayName = data.Field.DisplayName;

            tryLoadAssociatedTimestampField(self.fieldpickerDataSeries, data.Field);
                    
            onDataUpdated();
        }
    };
    
    this.onTimeSeriesFieldSelected = function (fieldPicker, customData) {

        var data = self.fieldpickerTimeSeries.GetSelectedFields();
        if (data != undefined) {
            data = data[0];

            $('#' + config.lblTimeSeriesID).text(getDisplayNameForField(data.Field));
            
            self.detailsData.TimeField = data.Field;
            onDataUpdated();
        }
    };

    this.bindData = function() {
        var $seriesDetailHolder =  $('#' + config.hfSeriesDetailsID);
        var detailsDataJson = $seriesDetailHolder.val();
        
        if (detailsDataJson == null) {
            return;
        }

        self.detailsData = JSON.parse(detailsDataJson);
        if (self.detailsData == null || self.detailsData == undefined) {
            return;
        }

        setContentHeight();

        var tbDisplayName = $('#' + config.tbDisplayNameID);
        var cbShowTrendLine = $('#' + config.cbShowTrendLineID);
        var cbCalculate95thPercentileLine = $('#' + config.cbCalculate95thPercentileLineID);        
        var ddSelectColorMode = $('#' + config.ddSelectColorModeID);
        var colorSelector = $('#' + config.colorSelectorID);
        
        tbDisplayName.off('keyup');
        cbShowTrendLine.off('change');
        cbCalculate95thPercentileLine.off('change');        
        ddSelectColorMode.off();
        colorSelector.off('change');
        
        tbDisplayName.val(self.detailsData.DisplayName);
        self.displayNameText = self.detailsData.DisplayName;        
        ddSelectColorMode.val(self.detailsData.ColorMode);       

        colorSelector.spectrum({
            color: self.detailsData.CustomColorHexValue,
            preferredFormat: 'hex6',
            showInput: true,
            chooseText: config.chooseTextI18N,
            cancelText: config.cancelTextI18N
        });
        
        cbShowTrendLine.prop('checked', self.detailsData.ShowTrendLine);
        cbCalculate95thPercentileLine.prop('checked', self.detailsData.Show95thPercentile);

        if (typeof (self.detailsData) != 'undefined' && self.detailsData != null && typeof (self.detailsData.Field.DisplayName) != 'undefined') {
            $('#' + config.lblSeriesNameID).text(getDisplayNameForField(self.detailsData.Field));
        }
        
        if (typeof (self.detailsData.TimeField) != 'undefined' && self.detailsData.TimeField != null && typeof(self.detailsData.TimeField.DisplayName) != 'undefined') {
            $('#' + config.lblTimeSeriesID).text(getDisplayNameForField(self.detailsData.TimeField));
        } else {
            $('#' + config.lblTimeSeriesID).text('');
        }

        
        tbDisplayName.on('keyup', displayNameChanged);        
        cbShowTrendLine.on('change', cbShowTrendLineChanged);               
        cbCalculate95thPercentileLine.on('change', cbCalculate95thPercentileLineChanged);
        ddSelectColorMode.on('change', ddSelectColorModeChanged);
        ddSelectColorMode.on('change', toggleColorPickerVisibility);
        colorSelector.on('change', colorSelectorChanged);

       toggleColorPickerVisibility();
    };
};
