﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_Reports_Visualize : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var queryparts = HttpUtility.ParseQueryString(Request.Url.Query);

        var id = queryparts["ctx"];

        if (id != null)
        {
            Preview.CacheId = new Guid(id.StartsWith("mem-") ? id.Substring(4) : id);
        }
    }
}