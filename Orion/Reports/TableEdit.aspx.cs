﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Reporting.Models.Layout;
using SolarWinds.Reporting.Models.Selection;
using SolarWinds.Reporting.Models.Tables;

public partial class Orion_Reports_TableEdit : ModelBasedPage<SectionCell>
{
    protected Guid CacheEntryId
    {
        get { 
            if( String.IsNullOrEmpty( Page.Request.QueryString["CacheEntryId"]) )
                return new Guid(Page.Request.QueryString["CacheEntryId"]);
            else
            {
                return Guid.Empty;
            }
        }
    }
    protected Guid SectionCellId
    {
        get
        {
            if (String.IsNullOrEmpty(Page.Request.QueryString["SectionCellId"]))
                return new Guid(Page.Request.QueryString["SectionCellId"]);
            else
            {
                return Guid.Empty;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public override void LoadViewModel()
    {
        //TODO use query string to pull table from cache
        //Need peters test page to populate some Tables in session
        //var report = SolarWinds.Orion.Core.Reporting.DAL.SessionCache.Instance.Fetch(CacheEntryId);
        //SectionCell sectionCell = report.Sections
        //                                .SelectMany(section => section.Columns)
        //                                .SelectMany(column => column.Cells)
        //                                .First(config => config.RefId == SectionCellId);
        //this.ViewModel = sectionCell.Config as TableConfiguration;
        this.ViewModel = new SectionCell(){ Config = new TableConfiguration()};
    }

    protected override void BindViewModel()
    {
        reportTableConfiguration.ViewModel = ViewModel.Config as TableConfiguration;
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            //Todo: Update session with ViewModel
            throw new NotImplementedException();
        }
    }

    private void RenderPreview()
    {
       
        //For testing % widths
        //    <%--<th style='width: 50%;'>Column 1</th>
        //<th style='width: 20%;'>Column 2</th>
        //<th style='width: 30%;'>Column 3</th>--%>

        //TODO: get html from table render component
        string tableHtml = @"<table rel='cell:xxxxxx-xxxxxx-xxxx-xxxx' id='the-table' style='table-layout: fixed;' cellspacing='0' >
                            <thead>
                            <th style='width: 200px;'>Column 1</th>
                            <th style='width: 100px;'>Column 2</th>
                            <th style='width: 200px;'>Column 3</th>
    
                            </thead>
                            <tbody>
                                <tr><td>Cats</td><td>In</td><td>hats</td></tr>
                                <tr><td>Mice</td><td>on</td><td>rice</td></tr>
                                <tr><td>Dogs</td><td>for</td><td>logs</td></tr>
                            </tbody>
                            </table>";
        string modalScript = string.Format(@"<script type='text/javascript'>
                                    $(function () {{
                                        $('#{0}').dialog();
                                    }});
                               </script>", PreviewPanel.ClientID);


        PreviewPanel.Controls.Add(new Literal { Text = tableHtml, Mode = LiteralMode.PassThrough });
        PreviewPanel.Controls.Add(new Literal { Text = modalScript, Mode = LiteralMode.PassThrough });
    }

    private void PreviewSave()
    {
        //TODO: Update model based on widths chosen in preview 
    }

    protected void PreviewClick(object sender, EventArgs e)
    {
        RenderPreview();
    }
}