<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true" CodeFile="Import.aspx.cs" Inherits="Orion_Reports_Import" %>

<asp:Content ContentPlaceHolderID="HeadPlaceHolder" runat="server">
<style type="text/css">
.sw-pg-fauxpopup { display: inline-block; margin: 16px; border: 1px solid #BBBBBB; background-color: white; padding: 10px 10px 0px 10px; overflow-x: hidden; moz-box-shadow: 2px 2px 3px #DDD; webkit-box-shadow: 2px 2px 3px #DDD; box-shadow: 2px 2px 3px #DDD; }    
.sw-pg-fauxpopup h1 { padding-left: 0; padding-top: 0; }
</style>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="sw-pg-fauxpopup">
        <h1>
            Import Report File
        </h1>
        
        <div>Select report file to upload</div>
        
        <div style="border: 1px solid #ddd; padding: 4px; margin-top: 4px;">
            <asp:FileUpload runat="server" ID="ImportFile" />
        </div>
        
        <div runat="server" style="margin-top: 10px;" EnableViewState="false" Visible="False" id="errorContainer">
            <orion:ExceptionWarning runat="server" EnableViewState="false" id="errorControl" />
        </div>
        
        <div runat="server" style="margin-top: 10px;" EnableViewState="false" Visible="False" id="successContainer">
            <span class='sw-suggestion sw-suggestion-pass'><span class="sw-suggestion-icon"></span>
                <%= HttpUtility.HtmlEncode( string.Format("Report '{0}' has been created.", ReportName)) %> &raquo; <a runat="server" class="sw-suggestion-link" href="#" onclick="$('#gotoEdit')[0].submit(); return false;">Edit report</a>
            </span>
        </div>

        <div class='sw-btn-bar' style="margin-top: 16px;">
            <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Import" DisplayType="Primary" />
            <orion:LocalizableButtonLink runat="server" LocalizedText="Cancel" DisplayType="Secondary" NavigateUrl="/Orion/Reports/Default.aspx" />
        </div>
    </div>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="outsideFormPlaceHolder">
    
<form id="gotoEdit" method="POST" action="/Orion/Reports/Add/Default.aspx?ReportWizardGuid=<%= Guid.NewGuid() %>">
    <input type="hidden" autocomplete="off" name="ReportID" value="<%= DefaultSanitizer.SanitizeHtml(ReportId) %>" >
</form>
</asp:Content>
