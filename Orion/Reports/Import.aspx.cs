﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using SolarWinds.Orion.Core.Reporting.DAL;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Reporting.Models;
using SolarWinds.Reporting.Models.Layout;
using SolarWinds.Reporting.Models.Selection;

public partial class Orion_Reports_Import : System.Web.UI.Page
{
    public string ReportId { get; set; }
    public string ReportName { get; set; }

    protected override void OnInit(EventArgs e)
    {
        if (!Profile.AllowReportManagement)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Redirect("/Orion/Reports/ViewReports.aspx");
        }

        Form.Enctype = "multipart/form-data";
        base.OnInit(e);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (ImportFile.HasFile)
        {
            var file = ImportFile.PostedFile;
            XDocument xdoc = null;
            Report report = null;

            try
            {
                xdoc = XDocument.Load(file.InputStream);
            }
            catch (Exception ex)
            {
                SetError("Unable to parse uploaded xml file.", ex);
                return;
            }

            try
            {
                report = Loader.Deserialize(xdoc.Root);

                if( report.Header == null)
                    report.Header = new Header();
            }
            catch (Exception ex)
            {
                SetError("Unable to deserialize new report file.", ex);
                return;
            }

            string reportName = report.Name ?? "Unamed Report";
            int? counter = null;

            for (;;)
            {
                string newname = counter.HasValue ? reportName + " (" + counter + ")" : reportName;

                if (ReportDAL.ReportExists(newname))
                {
                    counter = counter.HasValue ? counter.Value + 1 : 1;
                    continue;
                }

                report.Name = newname;
                break;
            }

            try
            {
                var g = SaveReport(report);
                ReportId = g.ToString(CultureInfo.InvariantCulture);
                ReportName = report.Name;
                successContainer.Visible = true;
            }
            catch (Exception ex)
            {
                SetError("Unable to save new report.", ex);
                return;
            }
        }
    }

    int SaveReport(Report report)
    {
        if (Profile.AllowAdmin == false)
        {
            if (report.DataSources != null && report.DataSources.Any(x => x.Type == DataSourceType.CustomSQL))
                throw new InvalidOperationException("Only administrators can add reports with custom sql.");
        }

        report.ReportGuid = Guid.NewGuid();

        if (report.Header == null)
            report.Header = new Header();

        var definition = Loader.Serialize(report);

        List<object> argList = new List<object> {
                report.Name,
                report.Description ?? string.Empty, 
                report.LimitationCategory, 
                report.Category ?? string.Empty, 
                report.Header.Title ?? report.Name, 
                report.Header.SubTitle ?? string.Empty, 
                definition,
                report.IsFavorite.ToString(),
                HttpContext.Current.Profile.UserName, 
                HttpContext.Current.Profile.UserName
        };

        using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
        {
            var resultXml = service.Invoke("Orion.Report", "CreateReport", GetArgElements(argList));
            return int.Parse(resultXml.InnerText);
        }
    }

    void SetError(string message, Exception ex)
    {
        var details = new ErrorInspectorDetails { Title = message, Error = ex };

        OrionErrorPageBase.CacheError(HttpContext.Current, details);

        errorControl.Text = message;
        errorControl.DataSource = details;
        errorControl.Visible = true;
        errorContainer.Visible = true;
    }

    public static XmlElement[] GetArgElements(IEnumerable<object> arguments)
    {
        var argDoc = new XmlDocument();
        return arguments.Select(delegate(object arg)
        {
            var argElt = argDoc.CreateElement("arg");
            if (arg is IEnumerable<object>)
            {
                var serializer = new DataContractSerializer(typeof(string[]));
                using (var backing = new StringWriter())
                using (var writer = new XmlTextWriter(backing))
                {
                    var argEltChild = argDoc.CreateElement("arg");

                    serializer.WriteObject(writer, arg);
                    argEltChild.InnerXml = backing.ToString();
                    argElt.InnerXml = argEltChild.LastChild.InnerXml;
                }

            }
            else
            {
                argElt.InnerText = arg.ToString();
            }
            return argElt;
        }).ToArray();
    }
}