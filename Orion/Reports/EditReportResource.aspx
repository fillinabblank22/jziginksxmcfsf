<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/Reports/ReportResourceEdit.master"
    AutoEventWireup="true" CodeFile="EditReportResource.aspx.cs" Inherits="Orion_Reports_EditReportResource" EnableViewState="true" %>
<%@ Register TagPrefix="orion" TagName="TitleEdit" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPlaceHolder" runat="Server">
    <orion:Include ID="Include1" runat="server" File="EditGauge.css" />
    <orion:Include ID="Include2" runat="server" File="ig_webGauge.js" />
    <orion:Include ID="Include3" runat="server" File="ig_shared.js" />
    <orion:Include ID="Include4" runat="server" File="GaugeScript.js" />
    <orion:Include ID="Include5" runat="server" File="ReportingDisplay.css" />

    <script type="text/javascript" language="javascript">
        var TitleTextBoxID = '<%=TitleEditControl.ResourceTitleID%>';
    </script>
    <script type="text/javascript" language="javascript">
        var maxRowsCountForPreview = '<%= WebsiteSettings.ReportingTablePreviewMaxRowCount %>';
    </script>
    <style type="text/css">
        div.sw-res-editor table p {
            margin: 13px 0px 0px;
        }
        h1 span .sw-rpt-edit-only {
            display:inline-block;
            margin-left: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h1 style="font-size: large; padding-top: 15px; padding-bottom: 10px;">
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_261) %> <asp:Label ID="ResourceName" runat="server" Text="-" ForeColor="Black" /></h1>
    <table border="0" style="width:100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <orion:TitleEdit runat="server" ID="TitleEditControl" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:PlaceHolder runat="server" ID="phResourceEditControl"></asp:PlaceHolder>
            </td>
        </tr>
        <tr>
            <td>
                <div class="sw-btn-bar">
                    <orion:LocalizableButton ID="LocalizableButton1" runat="server" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitClick"/>
                    <asp:PlaceHolder ID="phAdditionalButtons" runat="server"></asp:PlaceHolder>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>