<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TestPresenterEdit.aspx.cs" Inherits="Orion_Reports_TestPresenterEdit" MasterPageFile="~/Orion/OrionMasterPage.master" %>
<%@ Register Src="~/Orion/Reports/Controls/ReportPresenterConfigControl.ascx" TagPrefix="orion" TagName="PresenterConfig" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div style="margin: 10px;">

<div style="padding: 4px; display: inline-block; border: 1px solid #666;"><b>FieldRef</b>: <%= DefaultSanitizer.SanitizeHtml(cfg.Field.RefID) %></div><br />

<div style="padding: 4px; display: inline-block; border: 1px solid #666; margin-top: 10px;">
<table>
    <orion:PresenterConfig id="cfg" runat="server" />
</table>
</div>

<div class='sw-btn-bar'>
    <orion:LocalizableButton ID="SaveBtn" runat="server" LocalizedText="Save" DisplayType="Primary"  OnClick="SaveBtn_OnClick" />
    <orion:LocalizableButton ID="TestBtn" runat="server" LocalizedText="Test" DisplayType="Secondary" OnClick="TestBtn_OnClick" />
</div>

</div>


</asp:Content>
