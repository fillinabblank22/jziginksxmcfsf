<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ImportReportSelection.ascx.cs" Inherits="Orion_Reports_Import_ImportReportSelection" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>

<orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="extjs/Ext.sw.grid.RadioSelectionModel.js" />
<orion:Include runat="server" File="Reports/js/ImportReportSourceSiteGrid.js" />
<orion:Include runat="server" File="Admin/js/SearchField.js" />
<orion:Include runat="server" File="Reports/js/ReportManagerGrid.js" />
<orion:Include runat="server" File="Reports/js/ImportReportController.js" />

<!-- SignalR -->
<orion:Include runat="server" File="/signalR/jquery.signalR-1.2.2.js" />
<script src='<%: ResolveClientUrl("~/signalr/hubs") %>'></script>

<orion:Include runat="server" File="ReportsImport.css" />

<div id="importRemoteReportsDialog" title="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.RemoteReportsImport_Title) %>">
    <input type="hidden" name="SolarWindsServers_PageSize" id="SolarWindsServers_PageSize" value='<%= DefaultSanitizer.SanitizeHtml(WebUserSettingsDAL.GetPageSize("SolarWindsServers_PageSize")) %>' />        
    <div class="importRemoteReportsDialogInner">
    
        <div id="importRemoteReportsDialogAccordion" style="width: 100%;">
        
            <h3 id="orionServerSelectionHeader">
                <span id="orionServerSelectionHeaderText1"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.RemoteReportsImport_SelectOrionServer) %></span>
                <span id="orionServerSelectionHeaderText2"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.RemoteReportsImport_SelectedOrionServer) %></span>
                <span id="orionServerSelectionHeaderText3"></span>
            </h3>
            <div id="orionServerSelectionBody">
                <table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <div id="solarwindsServersControlContainer"></div>
                        </td>
                    </tr>
                </table>
            </div>
    
            <h3 id="remoteReportsSelectionHeader">
                <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.RemoteReportsImport_PleaseSelectReports) %></span>
            </h3>
            <div id="remoteReportsSelectionBody" style="display: none;">
                <div id="importReportsSelectionContent">
                    <table id="importReportsSelection" border="0" cellpadding="0" cellspacing="0">
                        <tr style="height: 24px;">
                            <td style="width: 70%">
                                <div class="title"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.RemoteReportsImport_AvailableReports) %></div>
                            </td>
                            <td style="width: 30%">
                                <div id="selectedReportsCount" class="title"></div>
                                <a id="removeAllSelectedReports" class="sw-link">Remove All</a>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 70%" valign="top">
                                <div id="ImportReportSelection_ReportManagerGrid">
                                </div>
                            </td>
                            <td style="width: 30%;" valign="top">
                                <div id="selectedReportsContainer" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="importReportsInProgress" style="display: none;">
                    <div class="content">
                        <img src="/Orion/images/loading_gen_small.gif" />
                        <div class="mainText">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.RemoteReportsImport_InProgress) %></div>
                    </div>
                </div>
            </div>

        </div>
    
        <div id="buttonsContainer" class="sw-btn-bar-wizard">
            <a id="selectOrionAndContinue" class=" sw-btn-primary sw-btn">
                <span class="sw-btn-c">
                    <span class="sw-btn-t"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.RemoteReportsImport_SelectAndContinue) %></span>
                </span>
            </a>
            <a id="importReportsButton" class=" sw-btn-primary sw-btn">
                <span class="sw-btn-c">
                    <span class="sw-btn-t"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.RemoteReportsImport_Import) %></span>
                </span>
            </a>
            <a id="cancelImportButton" class="sw-btn">
                <span class="sw-btn-c">
                    <span class="sw-btn-t"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.RemoteReportsImport_Cancel) %></span> 
                </span>
            </a>
        </div>
    
        <div id="importRemoteReportsDuplicatesResolvingDialog" style="display: none;" title="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.RemoteReportsImport_ResolveDuplicatesTitle) %>">
    
            <div class="title"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.RemoteReportsImport_ResolveDuplicatesDescription1) %></div>
            <div style="font-size:12px;padding-bottom:5px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.RemoteReportsImport_ResolveDuplicatesDescription2) %></div>
            <div class="importReportsWarning">
                <img src="/Orion/images/warning_16x16.gif"/> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.RemoteReportsImport_ResolveDuplicatesDescription3) %>
            </div>
            <div id="reportsDuplicatesResolvingGridContainer" style="width: 100%;padding-top:8px;"></div>
    
            <div class="sw-btn-bar-wizard">
                <a id="reportsDuplicatesResolvingAccept" class=" sw-btn-primary sw-btn">
                    <span class="sw-btn-c">
                        <span class="sw-btn-t"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.RemoteReportsImport_Continue) %></span>
                    </span>
                </a>
                <a id="reportsDuplicatesResolvingCancel" class="sw-btn">
                    <span class="sw-btn-c">
                        <span class="sw-btn-t"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.RemoteReportsImport_Cancel) %></span> 
                    </span>
                </a>
            </div>


        </div>

        <div id="importRemoteReportsProgressDialog" style="display: none;" title="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.RemoteReportsImport_ImportingReports_Title) %>">
    
        <div id="importRemoteReportsProgressDetails"></div>
        <div id="importRemoteReportsProgressIndicator" class="progressBar"></div>
    
        <div class="sw-btn-bar-wizard">
            <a id="importRemoteReportsProgressCancel" class="sw-btn">
                <span class="sw-btn-c">
                    <span class="sw-btn-t"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.RemoteReportsImport_Cancel) %></span> 
                </span>
            </a>
        </div>

    </div>
    </div>

</div>