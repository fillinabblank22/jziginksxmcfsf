﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Reporting;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.Reporting;
using SolarWinds.Reporting.Models.Selection;
using SolarWinds.Reporting.Models.Presentation;
using SolarWinds.Orion.Core.Reporting.DAL;
using CacheEntry = SolarWinds.Orion.Core.Reporting.DAL.SessionCache.CacheEntry;
using SessionCache = SolarWinds.Orion.Core.Reporting.DAL.SessionCache;
using SolarWinds.Orion.Core.Common;
using System.Xml.Linq;
using SolarWinds.Reporting.Models;
using SolarWinds.Reporting.Models.Tables;

public partial class Orion_Reports_TestPresenterEdit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string guidref = Request.QueryString["ref"];

        Report rpt;

        if (guidref == null)
        {
            string reportid = Request.QueryString["id"];

            if (reportid == null)
                throw new InvalidOperationException("id is a required query string parameter, which maps to a report id");

            var loaded = Loader.Deserialize(XElement.Parse((OrionReportHelper.GetReportbyID(int.Parse(reportid)).Definition)));

            var g = SessionCache.Instance.Store(loaded);

            Response.Redirect("/Orion/Reports/TestPresenterEdit.aspx?ref=" + g + "&c=0&t=0");
            return;
        }
        else
        {
            var g = Guid.Parse(guidref);

            rpt = SessionCache.Instance.Fetch(g);

            if (rpt == null)
                throw new InvalidOperationException("ref " + g + " is not in the session any more");
        }

        //---

        string t = Request.QueryString["t"];

        if (t == null)
            throw new InvalidOperationException("t is a required query string parameter, which maps to the cell ordinal");

        int cellOrd = int.Parse(t);

        //---

        string c = Request.QueryString["c"];

        if (c == null)
            throw new InvalidOperationException("c is a required query string parameter, which maps to the table column ordinal");

        int columnOrd = int.Parse(c);

        //---

        var cell = rpt.Sections.SelectMany(x => x.Columns).SelectMany(x => x.Cells).Skip(cellOrd).FirstOrDefault();

        if (cell == null || cell.RenderProvider != SolarWinds.Reporting.Impl.Rendering.TableRenderer.Moniker)
            throw new InvalidOperationException("t is not an ordinal that points to a table cell");

        //---

        var config = rpt.Configs.FirstOrDefault(x => x.RefId == cell.ConfigId) as TableConfiguration;

        var column = config.Columns.Skip(columnOrd).FirstOrDefault();

        if (column == null)
            throw new InvalidOperationException("c is not an ordinal to a table column");

        //---

        cfg.Source = rpt.DataSources.FirstOrDefault(x => x.RefId == cell.DataSelectionRefId);
        cfg.ColumnPrefix = "xxxxx_col0"; // we only have one column on this page. if we were going to do more, we'd want to inc 0

        if (Page.IsPostBack == false)
        {
            var goodDefaults = new TableColumnDefaultFactory(cfg.Source);
            goodDefaults.ReviseWithDefaults(column, false);
        }

        cfg.Field = column.Field;

        if (Page.IsPostBack == false)
        {
            cfg.ViewModel = column.Presenters;
        }
        else
        {
            cfg.ViewModel = new PresenterRef[] { };
        }

        cfg.ForceMergeModel(cfg.ViewModel); // hack to combat recent breaking changes to the control.
    }

    protected void TestBtn_OnClick(object sender, EventArgs e)
    {
        var g = Guid.Parse(Request.QueryString["ref"]);
        Response.Redirect("/Orion/Reports/Visualize.aspx?ctx=mem-" + g);
    }

    protected void SaveBtn_OnClick(object sender, EventArgs e)
    {
        var g = Guid.Parse(Request.QueryString["ref"]);
        var rpt = SessionCache.Instance.Fetch(g);
        int cellOrd = int.Parse(Request.QueryString["t"]);
        int columnOrd = int.Parse(Request.QueryString["c"]);
        var cell = rpt.Sections.SelectMany(x => x.Columns).SelectMany(x => x.Cells).Skip(cellOrd).FirstOrDefault();
        var config = rpt.Configs.FirstOrDefault(x => x.RefId == cell.ConfigId) as TableConfiguration;
        var column = config.Columns.Skip(columnOrd).FirstOrDefault();
        column.Presenters = cfg.ViewModel;
    }
}