<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReportLayoutFooterControl.ascx.cs"
    Inherits="Orion_Reports_Controls_ReportLayoutFooterControl" %>
<orion:Include ID="Include2" runat="server" File="Reports/js/ReportLayoutFooter.js" />
<orion:Include ID="Include1" runat="server" File="LayoutBuilder.css" />

<script type="text/javascript">
    // <![CDATA[
    function getDefaultFooterText() { return '<%= DefaultFooterText %>'; }
    function getFooterTextID() { return '<%= footerTextArea.ClientID %>'; }

    $(document).ready(function () {
        if (!$("#customText").is(':checked')) {
            $("#<%= footerTextArea.ClientID %>").hide();
        }

        if (!$("#footerVisible").is(':checked')) {
            $("#footerContent").hide();
        }

        $("#footerVisible").on("click", function () { ($("#footerVisible").is(':checked')) ? $("#footerContent").show() : $("#footerContent").hide(); });
    });

    function ShowHideCustomText(cb) {
        if (cb.checked) {
            $("#<%= footerTextArea.ClientID %>").show();
        }
        else {
            $("#<%= footerTextArea.ClientID %>").hide();
        }
    }
        // ]]>
</script>
<div id="reportLayoutFooter">
    <div id="hiddenFooterData">
        <asp:HiddenField ID="footerCheckData" runat="server" />
        <asp:HiddenField ID="creationDateData" runat="server" />
        <asp:HiddenField ID="customTextCheckData" runat="server" />
    </div>
    <div id="layoutFooterTop">
        <div style="display: inline; vertical-align: middle;">
            <input type="checkbox" id="footerVisible" />
        </div>
        <label for="footerVisible" style="display: inline; font-weight: bold;">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_60) %>
        </label>
    </div>
    <table id="footerContent" style="width: 100%; padding: 10px 5px 5px 10px; white-space: nowrap;
        vertical-align: middle;" cellpadding="0" cellspacing="0">
        <tr>
            <td style="vertical-align: middle; padding-right: 2px; width: 20px;">
                <input type="checkbox" id="creationDate" />
            </td>
            <td style="vertical-align: middle;  padding-right: 20px; text-align: left; width: 100px;">
                <label for="creationDate">
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_56) %>
                </label>
            </td>
            <td style="vertical-align: middle;  padding-right: 2px; width: 20px;">
                <input type="checkbox" id="customText" onclick="ShowHideCustomText(this);" />
            </td>
            <td style="vertical-align: middle;  padding-right: 5px; text-align: left; width: 100px;">
                <label for="customText">
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_57) %>
                </label>
            </td>
            <td style="vertical-align: middle;  padding-right: 15px; padding-top: 2px; width: 100%; height: 30px;">
                <asp:TextBox runat="server" TextMode="MultiLine" style="width: 100%;" Rows="1" ID="footerTextArea" name="footerTextArea" CssClass="footerTextArea"></asp:TextBox>
            </td>
            <td style="padding-right: 5px; width: 15%;">
            </td>
        </tr>
    </table>
</div>
