<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReportTableColumnLayout.ascx.cs" Inherits="Orion_Controls_ReportTable_ReportTableColumnLayout" %>
<%@ Import Namespace="SolarWinds.Orion.Web.UI.Localizer" %>
<%@ Register Src="~/Orion/Controls/FieldPicker/FieldPicker.ascx" TagPrefix="orion" TagName="FieldPicker" %>
<orion:Include ID="Include1" runat="server" File="Reports/js/ReportTableColumnLayout.js" />
<orion:Include ID="Include2" runat="server" File="ReportTableColumnLayout.css" />

<div id="emptyColumns" class="emptyColumns" runat="server">
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_38) %>  <span style="margin-left: 1em;"></span>
</div>
<script id="columnDetailTemplate" type="text/x-template">
<asp:Literal runat="server" ID="tabTemplate">
<table class="columnDetail template">
    <tr>
        <td>@{R=Core.Strings; K=WEBDATA_PS0_1; E=xml}</td>
        <td><span class="databaseColumnName">{{DatabaseColumnName}}</span>
   </tr>
   <tr>
      <td>@{R=Core.Strings; K=WEBDATA_PS0_3; E=xml}</td>
      <td><input type="text" style="width: 74%;" data-name="DisplayName" name="{{UniqueID}}-DisplayName-{{tabNumber}}" data-detailid="{{detailid}}" value="{{DisplayNameValue}}" /></td>
   </tr>
   <tr>
      <td><input type="hidden" data-name="TransformId" name="{{UniqueID}}-TransformId-{{tabNumber}}" value="{{transformId}}" /></td>
      <td>
        <input type="checkbox" data-name="DefaultDisplay" {{DefaultDisplayValue}} name="{{UniqueID}}-DefaultDisplay-{{tabNumber}}" data-detailid="{{detailid}}" /><label for="{{UniqueID}}-DefaultDisplay-{{tabNumber}}"> @{R=Core.Strings; K=WEBDATA_PS0_4; E=xml}</label>
        <span class='hide-column-info' ext:qtip='@{R=Core.Strings; K=WEBDATA_ZT0_7; E=xml}' ></span>      
    </td>
   </tr>
    <tr>
      <td></td>
      <td>
        <input type="checkbox" data-name="AllowHTMLTags" {{AllowHTMLTagsValue}} name="{{UniqueID}}-AllowHTMLTags-{{tabNumber}}" data-detailid="{{detailid}}" /><label for="{{UniqueID}}-AllowHTMLTags-{{tabNumber}}"> @{R=Core.Strings; K=WEBRESOURCES_TK0_1; E=xml}</label>
        <span class='hide-column-info' ext:qtip='@{R=Core.Strings; K=WEBRESOURCES_TK0_2; E=xml}' ></span>      
    </td>
   </tr>
   <tr style="display:none;">
    <td></td>
    <td class="helpfulText" style="padding-left: 1.8em;">@{R=Core.Strings; K=WEBDATA_PS0_21; E=xml}</td>
   </tr>
   <tr style="{{AggregationStyle}}" class="jsPresentersPlaceHolder">
      <td>@{R=Core.Strings; K=WEBDATA_PS0_12; E=xml}</td>
      <td><select style="width: 74%;" data-name="Function" name="{{UniqueID}}-Function-{{tabNumber}}"><!-- TODO: list of options should be plugable -->
             {{FunctionOptions}}
          </select>
      </td>
   </tr>
   <tr style="display: none;">
      <td>@{R=Core.Strings; K=WEBDATA_PS0_13; E=xml}</td>
      <td><select style="width: 74%;" data-name="ValidRange" name="{{UniqueID}}-ValidRange-{{tabNumber}}">
             {{ValidRangeOptions}}
          </select>
      </td>
   </tr>
   <tr>
      <td>@{R=Core.Strings; K=WEBDATA_PS0_5; E=xml}</td>
      <td><select style="width: 74%;" style="width: 65%;" data-name="Alignment" name="{{UniqueID}}-Alignment-{{tabNumber}}">
             {{AlingmentOptions}}
          </select>
      </td> 
   </tr>
    <tr style="display: none">
    <td>@{R=Core.Strings; K=WEBDATA_PS0_6; E=xml}</td>
    <td><input type="radio" name="{{UniqueID}}-ColumnWidth-{{tabNumber}}" {{ColumnWidthDynamicWidthChecked}} value="DynamicWidth" /> @{R=Core.Strings; K=WEBDATA_PS0_23; E=xml}<input type="radio" style="margin-left: 1.5em;" name="{{UniqueID}}-ColumnWidth-{{tabNumber}}" {{ColumnWidthCustomWidthChecked}} value="FixedWidth" /> @{R=Core.Strings; K=WEBDATA_PS0_24; E=xml} <a href="#" onclick="__doPostBack('columnLayout','Preview')">@{R=Core.Strings; K=WEBDATA_PS0_25; E=xml}</a> @{R=Core.Strings; K=WEBDATA_PS0_26; E=xml}
        <input type="hidden" name="{{UniqueID}}-ColumnWidthInPixels-{{tabNumber}}" data-name="ColumnWidthInPixels" value="{{ColumnWidthInPixelsValue}}" />
        <input type="hidden" name="{{UniqueID}}-ColumnWidthInPercents-{{tabNumber}}" data-name="ColumnWidthInPercents" value="{{ColumnWidthInPercentsValue}}" />
    </td>
   </tr>
</table>
</asp:Literal>
</script>

<asp:HiddenField ID="columnLayoutConfig" runat="server" />
<asp:HiddenField ID="previousExistedColumns" runat="server"/>
<asp:HiddenField ID="allFieldsRefIDsContainer" runat="server" />
<script type="text/javascript">
    SW.Core = SW.Core || {};
    SW.Core.ReportTable = SW.Core.ReportTable || {};
    SW.Core.ReportTable.PageController = (function () {
        var self = this;
        var fieldpicker = null;
        var editFieldPicker = null;
        var allFields = JSON.parse( $("#<%= allFieldsRefIDsContainer.ClientID %>").val());
        var isTableStatistical = <%= IsTableStatistical.ToString().ToLowerInvariant()%>;

        var numColumnsToAdd = 0;

        function onPickerCreated(picker) {
            fieldpicker = picker;
        }

        /// <summary>
        /// Method finds out, whether in array addedColumns exist field specified by uniqueidentifier fieldid
        /// </summary>
        /// <param name="addedColumns" type="Array">Array in which is contained information about all added fields into table layout.</param>
        /// <param name="fieldid" type="Object">Uniqueidentifier of column for which we try to find out, whether is added into column layout.</param>
        /// <returns type="bool">True in the case that field specified by fieldid is contained in array addedColumns.</returns>
        function existFieldInAddedColumns(addedColumns, fieldid) {
            for (var i = 0; i < addedColumns.length; i++) {
                if (addedColumns[i].fieldid && fieldid) {
                    if (addedColumns[i].fieldid === fieldid) {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Method finds out, whether in array of selectedFields exist field specified by uniqueidentifier fieldid
        /// </summary>
        /// <param name="selectedFields" type="Array">Array of all selected fields from fieldpicker.</param>
        /// <param name="fieldid" type="Object">Uniqueidentifier of column for which we try to find out, whether this field is contained in the array of selected fields.</param>
        /// <returns type="bool">True in the case that field specified by fieldid is contained in array selectedFields.</returns>
        function existFieldInSelectedFields(selectedFields, fieldid) {
            for (var i = 0; i < selectedFields.length; i++) {
                if (selectedFields[i].Field && selectedFields[i].Field.RefID && fieldid) {
                    if (selectedFields[i].Field.RefID.Data === fieldid) {
                        return true;
                    }
                }
            }

            return false;
        }

        function updateAllFieldsAccordingToSelection(newField, oldFieldRefID) {
            if (newField) {
                // Removes old field id from collection and add a new one.
                allFields = _.without(allFields, oldFieldRefID);
                allFields.push(newField.RefID);
            }
        }

        function onFieldSelected(fieldPicker, customData) {
            if (customData && customData.Mode == "Edit") {
                
                var newField = editFieldPicker.GetSelectedFields()[0];
                updateAllFieldsAccordingToSelection(newField, customData.EditedFieldRefID);
                
                editColumnLayout.ModifyTabDetailAndHeaderByData(customData.DetailId, {
                     DisplayNameValue: newField.DisplayName, 
                     FieldID: newField.RefID, 
                     Field: newField.Field
                });
            } 
            // New columns/fields added.
            else 
            {
                var selectedFields = fieldpicker.GetSelectedFields();
                var alreadyAddedColumns = editColumnLayout.GetAllAddedTabs();

                self.numColumnsToAdd = selectedFields.length;
                for (var i = 0; i < selectedFields.length; i++) {
                    
                    // Non-statistical fields
                    if (!selectedFields[i].Field.DataTypeInfo || !selectedFields[i].Field.DataTypeInfo.IsStatistic) {
                        editColumnLayout.AddNewTab(selectedFields[i].RefID, selectedFields[i].DisplayName, selectedFields[i].Field);
                        self.numColumnsToAdd--;
                    } 
                    // Statisticals fields, we try find related timestamp field.
                    else 
                    {
                        self.numColumnsToAdd++;
                       
                        // Self-executed function with own local scope. 
                        (function(selectedField) {
                            fieldPicker.getTimestampForField(selectedField.Field, function(timestampField, sourceField) {
                                
                                // If we have found related timestamp field we add it and then we add origin field.
                                if (timestampField) {
                                    if (!existFieldInAddedColumns(alreadyAddedColumns, timestampField.RefID) && !existFieldInSelectedFields(selectedFields, timestampField.RefID)) {
                                        editColumnLayout.AddNewTab(timestampField.RefID, timestampField.DisplayName, timestampField.Field, { isHidden: true, isHTMLTagsAllowed:false });
                                        alreadyAddedColumns = editColumnLayout.GetAllAddedTabs(); // needed, because I need to check, whether in future also against automaticly added column
                                    }
                                    
                                    self.numColumnsToAdd--;
                                }

                                editColumnLayout.AddNewTab(selectedField.RefID, selectedField.DisplayName, selectedField.Field);
                                self.numColumnsToAdd--;
                            }, function(errorMsg, sourceField) {
                                
                                // If any error have occured, we add just origin field.
                                editColumnLayout.AddNewTab(selectedField.RefID, selectedField.DisplayName, selectedField.Field);
                                self.numColumnsToAdd -= 2;
                            });

                            
                        }(selectedFields[i]));
                    }
                }
                var postBack = (function() {
                     if ((selectedFields.length > 0) && (self.numColumnsToAdd<=0)) {
                                __doPostBack("ColumnsAdded", "");
                     } else {
                         setTimeout(function() { postBack(); }, 1);
                     }
               });
               
               setTimeout(postBack, 1);
            }
        }

        function onEditPickerCreated(editPicker) {
            editFieldPicker = editPicker;
            editColumnLayout.SetFieldPicker(editFieldPicker);
        }
        
        function stopEventAndShowFieldPicker(event) {
            event.preventDefault();
            event.stopPropagation();
            
            fieldpicker.ComputeAndSetNavigationPathFilterAsync(allFields, null, function() {
                fieldpicker.ShowInDialog();
            });

            return false;
        }

        function initPage() {

            // Init will be called after dom is ready
            $(function() {
                $("#" + "<%= GetSaveUniqueID(this.UniqueID) %>" + ' li.addColumn a div img.addNewTab-icon').click(function(event) {
                    return stopEventAndShowFieldPicker(event);
                });

                // This button is displayed just in case, that no column has been added yet or all column was removed
                $(SW.Core.Widgets.Button('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_39) %>', { type: 'secondary', cls: 'addColumn', id: 'btnAddColumn' })).appendTo("#" + "<%= emptyColumns.ClientID %>" + " span").click(function(event) {
                    return stopEventAndShowFieldPicker(event);
                });

                $('.fieldPickerTrigger').click(function(event) {
                    return stopEventAndShowFieldPicker(event);
                });

                if (isTableStatistical === true)
                    $('.TimeRelativeSelection').prop('disabled', true);
            });
        }

        var $emptyColumn = $("#" + "<%= emptyColumns.ClientID %>");
        $emptyColumn.appendTo("div.tableLayoutEditor");

        var editColumnLayout = new SW.Core.TableResource.ReportTableColumnLayout({
            parentTabId: "<%= GetSaveUniqueID(this.UniqueID) %>",
            emptyColumnLayoutId: "<%= emptyColumns.ClientID %>",
            columnLayoutConfig: "<%= columnLayoutConfig.ClientID %>",
            allFields: allFields,
            isTableStatistical: isTableStatistical
        });
        
        var dragableColumnCreatedCallback = <%= this.OnCreatedJS %>;
        if (dragableColumnCreatedCallback && jQuery.isFunction(dragableColumnCreatedCallback)) {
            dragableColumnCreatedCallback(editColumnLayout);
        }
        
        editColumnLayout.CreateDragableColumns();
        var template = $('#columnDetailTemplate').html();
        editColumnLayout.SetTabContentTemplate(template);
        
        initPage();

        return {
            onPickerCreated: onPickerCreated,
            onFieldSelected: onFieldSelected,
            onEditPickerCreated: onEditPickerCreated
        };

    })();
</script>

<orion:FieldPicker ID="fieldPicker" runat="server" OnCreatedJs="SW.Core.ReportTable.PageController.onPickerCreated" OnSelectedJs="SW.Core.ReportTable.PageController.onFieldSelected" /> 
<orion:FieldPicker ID="editFieldPicker" SingleMode="True" runat="server" OnCreatedJs="SW.Core.ReportTable.PageController.onEditPickerCreated" OnSelectedJs="SW.Core.ReportTable.PageController.onFieldSelected" /> 

