﻿using System;
using System.Linq;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Reporting.Models.Selection;

public partial class Orion_Reports_Controls_ReportTableFilter : ModelBasedControl<Filter>
{
    public override void LoadViewModel()
    {
        base.LoadViewModel();

        var filterLimit = new Limit
                              {
                                  Mode = LimitMode.ShowAll
                              };

        if (hdnSelectedValue.Value == rbtnShowOnlyTop.ClientID)
        {
            filterLimit.Mode = LimitMode.TopEntries;
            filterLimit.Count = Convert.ToInt32(tbTopRecords.Text);
        }
        else if (hdnSelectedValue.Value == rbtnShowOnlyTopPercent.ClientID)
        {
            filterLimit.Mode = LimitMode.TopPercent;
            filterLimit.Percentage = Convert.ToInt32(tbTopRecordPercent.Text);
        }
        
        ViewModel = new Filter()
        {
            Limit = filterLimit
        };
    }

    protected override void BindViewModel()
    {
        base.BindViewModel();        

        switch (ViewModel.Limit.Mode)
        {
            case LimitMode.ShowAll:
                rbtnShowAll.Checked = true;
                hdnSelectedValue.Value = rbtnShowAll.ClientID;
                break;
            case LimitMode.TopEntries:
                rbtnShowOnlyTop.Checked = true;
                tbTopRecords.Text = ViewModel.Limit.Count != null ? ViewModel.Limit.Count.Value.ToString() : string.Empty;
                hdnSelectedValue.Value = rbtnShowOnlyTop.ClientID;
                break;
            case LimitMode.TopPercent:
                rbtnShowOnlyTopPercent.Checked = true;
                tbTopRecordPercent.Text = ViewModel.Limit.Percentage != null ? ViewModel.Limit.Percentage.Value.ToString() : string.Empty;
                hdnSelectedValue.Value = rbtnShowOnlyTopPercent.ClientID;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        tbTopRecordPercent.ValidatorText = "&nbsp;" + Resources.CoreWebContent.WEBDATA_TM1_3;
        tbTopRecords.ValidatorText = "&nbsp;" + Resources.CoreWebContent.WEBDATA_TM1_4;
    }
}