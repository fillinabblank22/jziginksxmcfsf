<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TimePeriodsControl.ascx.cs" Inherits="Orion_Reports_Controls_TimePeriodsControl" %>

<orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include ID="Include2" runat="server" File="OrionCore.js" />
<orion:Include ID="Include4" runat="server" File="jquery/jquery.timePicker.js" />
<orion:Include ID="Include5" runat="server" File="js/jquery/timePicker.css" />
<orion:Include ID="Include3" runat="server" File="Reports/js/TimePeriods.js" />

<style type="text/css">
.dialogBody { padding: 10px; }
#ui-datepicker-div { z-index: 9999 !important; }
#customDialog { background-color: #FFFFFF; height:auto !important; }
.timeContent { padding-left: 8px; }    
</style>

<script type="text/javascript">
//<![CDATA[
    $(function () {
        var regionalSettings = <%=DatePickerRegionalSettings.GetDatePickerRegionalSettings()%>;
        $('#timeControlContent').timePeriods();
        $('#timeControlContent').timePeriods.regionalSettings = regionalSettings;

        <% if (this.Visible)
           { %>
        $('#timeControlContent').timePeriods.settings.controlId = "timeControlContent";
        $('#timeControlContent').timePeriods('InitTimeFrames');
        <% }
           %>

        $.fn.orionGetDate = function () {
            var datepicker = $('.datePicker', this);
            var timepicker = $('.timePicker', this);
            var datepart = $.datepicker.parseDate(regionalSettings.dateFormat, datepicker.val());
            var timepart = timepicker[0].timePicker.getTime();
            return new Date(datepart.getFullYear(), datepart.getMonth(), datepart.getDate(), timepart.getHours(), timepart.getMinutes(), timepart.getSeconds());
        };

        $.fn.orionSetDate = function (date) {
            $('.datePicker', this).val($.datepicker.formatDate(regionalSettings.dateFormat, date));
            var time = date.getTime(); // timePicker.setTime screws up the object. preserve it for our poor caller
            $('.timePicker', this)[0].timePicker.setTime(date);
            date.setTime(time);
        };

        $(function () {
            $.datepicker.setDefaults(regionalSettings);
            $('.datePicker').datepicker({ mandatory: true, closeAtTop: false });
            $('.timePicker').timePicker({ separator: regionalSettings.timeSeparator, show24Hours: regionalSettings.show24Hours });
            
            $("#RelativePeriod").change(function () {
                if (this.checked) {
                    $("#txtFromDatePicker").attr("disabled", "disabled"); 
                    $("#txtFromTimePicker").attr("disabled", "disabled"); 
                    $("#txtToDatePicker").attr("disabled", "disabled"); 
                    $("#txtToTimePicker").attr("disabled", "disabled"); 
                    
                    $("#LastNumber").removeAttr("disabled", "disabled"); 
                    $("#PeriodType").removeAttr("disabled", "disabled"); 
                }
                    
            });
            
            $("#CustomPeriod").change(function () {
                if (this.checked) {
                    $("#txtFromDatePicker").removeAttr("disabled"); 
                    $("#txtFromTimePicker").removeAttr("disabled"); 
                    $("#txtToDatePicker").removeAttr("disabled"); 
                    $("#txtToTimePicker").removeAttr("disabled"); 
                    
                    $("#LastNumber").attr("disabled", "disabled");
                    $("#PeriodType").attr("disabled", "disabled");
                }
            });
        });
    });
//]]>
</script>

<div id="timeControlContent" <%= Visible ? "" : "style='display: none;'" %>>
    <div id="customDialog" style="display:none;" class="disposable">
	    <div id="customDialogBody" class="dialogBody">
	        <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_32) %></b> <br/><br/>
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_33) %><br/>
            <input type="text" id="PeriodName" value="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_39) %>" /><br/>
            <span id="nameValidator" style="color : red;" class="hidden" >
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_34) %>
            </span>
            <br/>
            <input type="radio" name="CustomTime" id="RelativePeriod" checked="checked" />
            &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_35) %><br/>
            <div class="timeContent">
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_60) %> &nbsp; 
                <input type="text" id="LastNumber" style="width: 40px;" value="1" />
                <select id="PeriodType">
                    <option value="2"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_40) %></option>
                    <option value="3"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_41) %></option>
                    <option value="4"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_42) %></option>
                    <option value="5"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_43) %></option>
                    <option value="6"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_44) %></option>
                    <option value="7"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_45) %></option>
                </select>
            </div>
            <span id="lastNumberValidator" style="color : red;" class="hidden" >
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_36) %>
            </span>
            <br/>
            <div>
                <input type="radio" name="CustomTime" id="CustomPeriod" />&nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_61) %><br/>
                <table class="timeContent">
                    <tr>
                        <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_37) %>&nbsp;</td>
                        <td>
                            <span id="spanFrom">
                                <input type="text" id="txtFromDatePicker" style="width: 78px;" class="datePicker disposable" />
                                <input type="text" id="txtFromTimePicker" style="width: 78px;" class="timePicker disposable" />
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_38) %>&nbsp;</td>
                        <td style="padding-top: 5px;">
                            <span id="spanTo">
                                <input type="text" id="txtToDatePicker" style="width: 78px;" class="datePicker disposable" />
                                <input type="text" id="txtToTimePicker" style="width: 78px;" class="timePicker disposable" />
                            </span>
                        </td>  
                    </tr>
                </table> 
                <span id="customTimeValidator" style="color : red;" class="hidden" >
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_SO0_5) %>
                </span>      
            </div>
            <span id="greaterValidator" style="color : red;" class="hidden" >
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_87) %>
            </span>
	    </div>
        <div class="bottom">
            <div class="sw-btn-bar-wizard" id="buttons">
            </div>
	    </div>
	</div>
    <input type="hidden" id="timeFrameSessionIdentifier" runat="server" />
    <input type="hidden" id="selectedTimeFrameSessionIdentifier" runat="server" />
</div>
