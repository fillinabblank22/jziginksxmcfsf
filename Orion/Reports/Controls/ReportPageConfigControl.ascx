<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReportPageConfigControl.ascx.cs"
    Inherits="Orion_Reports_Controls_ReportPageConfigControl" %>

<orion:Include runat="server" File="LayoutBuilder.css" />
<orion:Include runat="server" File="Reports/js/ReportPageConfig.js" Section="Bottom" />

<script type="text/javascript">
    // <![CDATA[
    function getTbWebWidthID() { return '<%= tbWebWidth.ClientID %>'; }
// ]]>
</script>
<div id="ReportPageConfigControl">
    <table id="configContent" style=" white-space: nowrap; vertical-align: middle; padding-bottom: 10px">
        <tr>
            <td>
                <table width="100%" id="secondRowWebWidth">
                    <tr>
                        <td style="width: 50%; text-align: left; padding-right: 3px">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_276) %>
                        </td>
                        <td style="width: 50%">
                            <asp:TextBox ID="tbWebWidth" runat="server" Width="108px" Text="960" /><span style="padding-left: 3px"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_277) %></span>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <a class="layout-edit-link" href="#" onclick="fitInputWithWindowWitdth(); return false;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_278) %></a>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="tbWebWidth" Type="Integer" Display="Dynamic" 
                                    MinimumValue="300" MaximumValue="4096" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_279 %>" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="tbWebWidth" 
                                            Display="Dynamic" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_280 %>" />
            </td>
        </tr>

    </table>
</div>
