<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WhatIsNewInWebBasedReportDialog.ascx.cs" Inherits="Orion_Reports_Controls_WhatIsNewInWebBasedReportDialog" %>
<orion:Include runat="server" File="ReportWhatIsNewInWebBasedReportDialog.css" />

  <!-- Here will be placed code for dialog What's new with web-based reports -->
    <asp:Panel ID="WhatIsNewDialog" CssClass="whatIsNew" runat="server">
        <div class="whatIsNew-Header">
            <span><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_RV0_3) %></b></span><br/>
            <div style="margin-top: 15px;">
                <%= DefaultSanitizer.SanitizeHtml(getingStartedWithWebReports) %><br/>
            </div>
        </div>

        <div class="whatIsNew-Body">
        <div class="whatIsNew-WorkflowSection">
            <table>
                <tr>
                    <td class="content">
                        <span class="title"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_60) %></span>
                        <ul>
                            <li><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_57) %></li>                            
                            <li><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_59) %></li>
                        </ul>
                    </td>
                    <td class="empty"></td>
                    <td class="content">
                        <span class="title"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_61) %></span>
                        <ul>
                            <li><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_62) %></li>
                        </ul>
                    </td>
                    <td class="empty" style="width: 17px;"></td>
                    <td class="content">
                        <span class="title"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_63) %></span>
                        <ul>
                            <li><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_64) %></li>                                
                        </ul>
                    </td>
                    <td class="empty" style="width: 19px;"></td>
                    <td class="content">
                        <span class="title"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_67) %></span>
                        <ul>
                            <li><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_68) %></li>                           
                        </ul>
                    </td>
                    <td class="empty"></td>
                    <td class="content">
                        <span class="title"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_70) %></span>
                        <ul>
                            <li><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_71) %></li>
                        </ul>
                    </td>
                    <td class="empty" style="width: 19px;"></td>
                     <td class="content">
                        <span class="title"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YP0_3) %></span>
                        <ul>
                            <li><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YP_4) %></li>                            
                        </ul>
                    </td>
                       
                </tr>
            </table>
        </div>
        <div class="whatIsNew-WorkflowSection-Footer" style="background-image: url('<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath("Orion","/Reports/GettingStarted_Picture_For_Localization_6thStep.png")) %>');">
            <div class="title" style="margin-left: 125px; padding: 230px 0 0 0; width: 420px;">
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_72) %>
            </div>
        </div>
        </div>
        <table class = "buttonsBar" style="width: 1310px;padding-top:5px;">
            <tr>
                <td style="width: 1215px;"><input type="checkbox" id="chbDontShowThisAgain" /><label for="chbDontShowThisAgain">&nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_73) %></label></td>
                <td><span ID="btnGetStarted" ></span></td>
            </tr>
        </table>
    </asp:Panel>
    <style type="text/css">
        .whatIsNew 
        {            
            display: none;
            overflow: hidden !important;
            background-color: #FFFFFF !important;
            padding-bottom: 20px !important;
            }
    </style>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            $(window).resize(function () {
                $("#" + "<%= WhatIsNewDialog.ClientID %>").dialog("option", "position", "center");
            });
            var dialog = $("#" + "<%= WhatIsNewDialog.ClientID %>").dialog({
                modal: true,
                draggable: true,
                resizable: false,
                autoOpen: false,
                width: "auto",
                height: "auto",
                show: 'blind',
                hide: 'blind',
                title: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_RV0_4) %>",
                close: function () {
                    SW.Orion.WhatIsNewInWebBasedReportDialog.SaveShowStatus(function () {
                    }, function () {
                    });
                },
                open: function () {
                    if ($("body").width() < $("#" + "<%= WhatIsNewDialog.ClientID %>").width()) {
                        $(".whatIsNew-Header").width($("body").width() - 100);
                        $(".whatIsNew-Body").width($("body").width() - 100).css("overflow-x", "scroll");
                        $(".buttonsBar").width($("body").width() - 100);
                        setTimeout(function () { $(window).trigger('resize'); }, 1000);
                    }
                }
            });

            SW.Core.namespace("SW.Orion").WhatIsNewInWebBasedReportDialog = function(dialog) {
                // CONSTRUCTOR
                var wasDialogHidden = eval("<%= this.WasDialogHidden %>".toLowerCase());
                
                $("#chbDontShowThisAgain").prop("checked", wasDialogHidden);
              
                $("#btnGetStarted").empty();
                $(SW.Core.Widgets.Button('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_53) %>', { type: 'primary', id: 'btnGetStarted1' })).appendTo("#btnGetStarted").click(function() {
                    SW.Orion.WhatIsNewInWebBasedReportDialog.HideDialog();
                });

                // PRIVATE METHODS

                var showDialog = function (closeFunction) {
                    if (typeof (closeFunction) != "undefined") {
                        dialog.on("dialogclose", closeFunction);
                    } else {
                        dialog.off("dialogclose");
                    }

                    dialog.dialog("open");
                };

                // PUBLIC METHODS
                return {
                    WasDialogHidden : function() {
                        return wasDialogHidden;
                    },

                    ShowDialog: function(closeFunction) {
                        showDialog(closeFunction);
                    },
                    
                    HideDialog: function() {
                        dialog.dialog("close");
                    },
                    SaveShowStatus: function(onSuccess, onFailure) {
                        SW.Core.Services.callWebService('/Orion/Services/ReportManager.asmx', "SetShowWhatIsNewInWebBasedReportDialog", { show: !$("#chbDontShowThisAgain").prop("checked") }, onSuccess, onFailure);
                    }
                };
            }(dialog);
        });
    </script>
