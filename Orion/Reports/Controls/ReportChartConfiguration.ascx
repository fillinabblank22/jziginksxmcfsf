<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReportChartConfiguration.ascx.cs" Inherits="Orion_Reports_Controls_ReportChartConfiguration" %>
<%@ Import Namespace="SolarWinds.Reporting.Models.Selection" %>

<%@ Register Src="~/Orion/Reports/Controls/EditDetailDataSeriesControl.ascx" TagPrefix="orion" TagName="EditDetailDataSeriesControl" %>
<%@ Register Src="~/Orion/Reports/Controls/EditAxisPropertiesControl.ascx" TagPrefix="orion" TagName="EditAxisPropertiesControl" %>
<%@ Register TagPrefix="orion" TagName="FieldPicker" Src="~/Orion/Controls/FieldPicker/FieldPicker.ascx" %>
<%@ Register Src="~/Orion/Reports/Controls/ReportTableSortConfiguration.ascx" TagPrefix="orion" TagName="ReportChartColumnOrderSortBy" %>
<%@ Register TagPrefix="orion" TagName="ServerFilter" Src="~/Orion/Controls/OrionServerFilterControl.ascx" %>

<orion:Include ID="Include1" runat="server" File="Reports/js/ReportChartMultiObjectsController.js" />
<orion:Include ID="Include2" runat="server" File="ReportChartConfiguration.css" />

<script type="text/javascript">

    var <%= MultiObjectsControllerInstanceName %> = new SW.Core.ReportChart.ChartEdit.MultiObjectsController({
        lblKeyFieldDisplayNameID : '<%= lblKeyFieldDisplayName.ClientID %>',
        btnSelectKeyFieldID : '<%= btnSelectKeyField.ClientID %>',
        hfKeyFieldID : '<%= jsonKeyField.ClientID %>',
        lblCaptionFieldDisplayNameID : '<%= lblCaptionFieldDisplayName.ClientID %>',
        btnSelectCaptionFieldID : '<%= btnSelectCaptionField.ClientID %>',
        btnDeleteKeyFieldID : '<%= btnDeleteKeyField.ClientID %>',
        btnDeleteCaptionFieldID : '<%= btnDeleteCaptionField.ClientID %>',
        fieldPickerFilter : '<%= fieldPickerFilter.ClientID %>',
        hfCaptionFieldID : '<%= jsonCaptionField.ClientID %>',
        // filtering controls
        showAllRbFilterID: '<%= rbShowAll.ClientID %>',
        filter1RbFilterID: '<%= rbFilter1.ClientID %>',
        filter2RbFilterID: '<%= rbFilter2.ClientID %>',
        tbTopXXFilterID: '<%= tbTopXX.ClientID %>',
        tbTopPercentFilterID: '<%= tbTopPercent.ClientID %>',
        ddlSortByID: '<%= ddlSortBy.ClientID %>',
        hdnSortByID: '<%= hdnSortBy.ClientID %>',
        ddlSortOrderID: '<%= ddlSortOrder.ClientID %>',
        ddlDataAggregationID: '<%= ddlDataAggregation.ClientID %>',
        sortingFrame1ID: '<%= sortingFrame1.ClientID %>',
        sortingFrame2ID: '<%= sortingFrame2.ClientID %>',
        chbCustomDataGroupingID: '<%= chbCustomDataGroupingEnabled.ClientID %>',
        trCustomDataGroupingPropertiesID: '<%= trCustomDataGroupingProperties.ClientID %>',
        trFilterResultsID: '<%= trFilterResults.ClientID %>'
    });

</script>

<div id="columnsWithError" Visible="False" runat="server">
    <div style="color: red; font-weight: bold;">
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_PF0_12) %><%--Data series with problems:--%>
    </div>
    <asp:Repeater ID="columnListRepeater" runat="server">
        <HeaderTemplate>
            <ul style="color: red;">
        </HeaderTemplate>
        <ItemTemplate>
             <li>
                <%# DefaultSanitizer.SanitizeHtml(((Tuple<string,string>)Container.DataItem).Item1) %> - 
                <%# DefaultSanitizer.SanitizeHtml(((Tuple<string,string>)Container.DataItem).Item2) %>
             </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
        </FooterTemplate>
    </asp:Repeater>
</div>
<asp:CustomValidator ID="ValidateColumns" ValidationGroup="columnValidation" Display="None" runat="server" OnServerValidate="ValidateChartConfiguration_ServerValidate"></asp:CustomValidator>


<table id="reportChartWrapper">
    <thead></thead>
    <tbody>
        <tr>
            <td id="leftcolumn">
                <orion:EditAxisPropertiesControl ID="leftAxisEditor" runat="server" Mode="Left" />        
            </td>
            <td id="contentcolumn">
                <orion:EditDetailDataSeriesControl runat="server" ID="dataSeriesDetail" ShowDetailsByDefault="False"/>                
            </td>
            <td id="rightcolumn">
                <orion:EditAxisPropertiesControl ID="rightAxisEditor" runat="server" Mode="Right"/>
            </td>
        </tr>        
    </tbody>
</table>

<!--  good candidate for user control -->
<table>
    <thead>
    </thead>
    <tbody>
       <tr id="trCustomDataGroupingSwitch" runat="server">
           <td class="customDataGroupingTitle">
               <asp:CheckBox ID="chbCustomDataGroupingEnabled" runat="server"/>
               <asp:Label ID="lblCustomDataGroupingEnabled" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_ReportChartConfiguration_CustomDataGrouping_CheckBox %>" runat="server"></asp:Label>
           </td>
       </tr> 
       <tr id="trCustomDataGroupingTitle" runat="server">
           <th class="customDataGroupingTitle">
               <asp:Label runat="server" CssClass="sectionTitle" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_ReportChartConfiguration_CustomDataGrouping_SectionTitle %>" />
           </th>
       </tr>
       <tr id="trCustomDataGroupingProperties" runat="server" class="multipleObjects">
           <td>

               <table>
                   <tr>
                       <td>
                           <asp:Label ID="lblKey" runat="server" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_ReportChartConfiguration_CustomDataGrouping_KeyField %>" /></td>
                       <td>
                            <asp:Label runat="server" ID="lblKeyFieldDisplayName"></asp:Label> </td>
                       <td>
                           <orion:LocalizableButton ID="btnSelectKeyField" runat="server" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_YK0_11 %>" DisplayType="Small" />
                           <a href="#" runat="server" id="btnDeleteKeyField"><img class="resource-manage-btn" title="Delete" alt="Delete" src="/Orion/images/Reports/delete_icon16x16.png"></a>
                           <orion:JsonInput runat="server" ID="jsonKeyField" />
                       </td>
                   </tr>
                   <tr>
                       <td>
                           <asp:Label ID="lblCaption" runat="server" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_ReportChartConfiguration_CustomDataGrouping_CaptionField %>" /></td>
                       <td>
                           <asp:Label runat="server" ID="lblCaptionFieldDisplayName"></asp:Label></td>
                       <td>
                           <orion:LocalizableButton ID="btnSelectCaptionField" runat="server" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_YK0_11 %>" DisplayType="Small" />
                           <a href="#" runat="server" id="btnDeleteCaptionField">
                               <img class="resource-manage-btn" title="Delete" alt="Delete" src="/Orion/images/Reports/delete_icon16x16.png"></a>
                           <orion:JsonInput runat="server" ID="jsonCaptionField" />
                       </td>
                   </tr>
               </table>
                
               <orion:FieldPicker ID="fieldPickerMultipleObjects" runat="server" SingleMode="True"  />
               <br/>
           </td>
        </tr>

        <tr>
           <th>
              <asp:Label ID="lblTimePeriods" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_97%>" CssClass="sectionTitle"></asp:Label>
           </th>
        </tr>
<!-- We have data source and time period defined else where -->
<%--        <tr><td><asp:Label runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_97%>" CssClass="sectionTitle"></asp:Label></td></tr>
        <tr>
            <td>                                
                <div class="sw-res-editor-row" runat="server" ID="ZoomPanel">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_98) %> 
                    <asp:DropDownList runat="server" ID="ChartZoom">
                        <asp:ListItem Value="1h" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_99%>"></asp:ListItem>
                        <asp:ListItem Value="2h" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_100, 2)%>"></asp:ListItem>
                        <asp:ListItem Value="24h" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_100, 24)%>"></asp:ListItem>
                        <asp:ListItem Value="today" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_VB0_133%>"></asp:ListItem>
                        <asp:ListItem Value="yesterday" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_101%>"></asp:ListItem>
                        <asp:ListItem Value="7d" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_187%>"></asp:ListItem>
                        <asp:ListItem Value="thisMonth" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_102%>"></asp:ListItem>
                        <asp:ListItem Value="lastMonth" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_103%>"></asp:ListItem>
                        <asp:ListItem Value="30d" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_188%>"></asp:ListItem>
                        <asp:ListItem Value="3m" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_104, 3)%>"></asp:ListItem>
                        <asp:ListItem Value="thisYear" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_105%>"></asp:ListItem>
                        <asp:ListItem Value="12m" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_104, 12)%>"></asp:ListItem>        
                    </asp:DropDownList>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="sw-res-editor-row" runat="server" ID="TimeSpanPanel">
                    <span runat="server" ID="TimeSpanLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_108) %></span> 
                    <asp:DropDownList runat="server" ID="ChartTimeSpan">
                        <asp:ListItem Value="1" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_107%>"></asp:ListItem>
                        <asp:ListItem Value="7" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_187%>"></asp:ListItem>
                        <asp:ListItem Value="30" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_103%>"></asp:ListItem>
                        <asp:ListItem Value="90" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_104, 3)%>"></asp:ListItem>
                        <asp:ListItem Value="365" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_106%>"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </td>
        </tr>
--%>
  
        <tr>
            <td>
                <div class="sw-res-editor-row" runat="server" ID="SampleSizePanel">
                    <span runat="server" ID="SampleSizeLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_290) %></span>
                    <asp:DropDownList runat="server" ID="SampleSize">
                        <asp:ListItem Value="1" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_110 %>"></asp:ListItem>
                        <asp:ListItem Value="5" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_111, 5)%>"></asp:ListItem>
                        <asp:ListItem Value="10" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_111, 10)%>"></asp:ListItem>
                        <asp:ListItem Value="15" Selected="True" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_111, 15)%>"></asp:ListItem>
                        <asp:ListItem Value="30" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_111, 30)%>"></asp:ListItem>
                        <asp:ListItem Value="60" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_112 %>"></asp:ListItem>
                        <asp:ListItem Value="120" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_113, 2)%>"></asp:ListItem>
                        <asp:ListItem Value="360" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_113, 6)%>"></asp:ListItem>
                        <asp:ListItem Value="720" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_113, 12)%>"></asp:ListItem>
                        <asp:ListItem Value="1440" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_114 %>"></asp:ListItem>
                        <asp:ListItem Value="10080" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_115 %>"></asp:ListItem>
                    </asp:DropDownList>
                    <div class="sw-text-helpful">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_116) %>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <% if (IsFederationEnabled) {%>
                <div style="padding-top: 8px;">
                    <orion:ServerFilter ID="ServerFilter" runat="server" Reporting="True" />
                </div>
                <% }%>
            </td>
        </tr>
        <tr id="trFilterResults" runat="server">
            <td>

                <asp:Label ID="lblFilterResults" runat="server" CssClass="sectionTitle" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TP0_41%>" /> <br/>
                <asp:Label ID="lblFilterDescription" runat="server" CssClass="sectionSubTitle" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TP0_42%>" />
                <br/>
                <br />
                <table>
                    <tr>
                        <td colspan="2"><asp:RadioButton runat="server" ID="rbShowAll" GroupName="Filter" Checked="True"/>
                        <asp:Label ID="lblShowAll" AssociatedControlId="rbShowAll" runat="server" CssClass="sectionSubTitle" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TP0_43%>" /></td>
                    </tr>
                    <tr>
                        <td colspan="2"><asp:RadioButton runat="server" ID="rbFilter1" GroupName="Filter" />
                        <asp:Label ID="lblShowOnlyTop" AssociatedControlId="rbFilter1" runat="server" CssClass="sectionSubTitle" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TP0_44%>" />
                            <asp:TextBox runat="server" ID="tbTopXX" TextMode="SingleLine" MaxLength="4" Text="5" Width="20"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div id="sortingFrame1" runat="server" class="sortFrame" style="display: none;">
                                <div style="margin: 0; padding: 0;">
                                    <table>
                                        <tr>
                                            <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_44) %></td>
                                            <td><asp:DropDownList ID="ddlSortBy" Width="100%" runat="server" />
                                                <orion:JsonInput runat="server" ID="hdnSortBy" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_45) %></td>
                                            <td><asp:DropDownList ID="ddlSortOrder" Width="100%" runat="server">
                                               
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_46) %></td>
                                            <td><asp:DropDownList ID="ddlDataAggregation" Width="100%" runat="server" /></td>
                                        </tr>
                                    </table>
                                    <span id="moreLessDetails" style="cursor: pointer;">
                                       <img src="/Orion/images/Button.Expand.gif" style="margin: 0px 5px -2px 5px;" class="collapsed">
                                       <span id="moreDetailtext" style="display: none;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_50) %></span>
                                       <span id="lessDetailtext" style="display: inline;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_50) %></span>
                                    </span>
                                    <div id="advancedSorting" style="padding-left: 2.3em; margin-bottom: 1em; display: none;">
                                        <table><tr><td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_51) %> </td><td><orion:ReportChartColumnOrderSortBy ID="secondarySortBy" ChartMode="True" runat="server" /></td></tr></table>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><asp:RadioButton runat="server" ID="rbFilter2" GroupName="Filter" />
                        <asp:Label ID="lblTopPercent" AssociatedControlId="rbFilter2" CssClass="sectionSubTitle" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PS0_47 %>" runat="server" /><asp:TextBox ID="tbTopPercent" MaxLength="3" Text="5" runat="server" Width="20" /></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div id="sortingFrame2" runat="server" class="sortFrame">
                            </div>
                        </td>
                    </tr>
                </table>
 
                <orion:FieldPicker ID="fieldPickerFilter" runat="server" SingleMode="true"/>
                <orion:FieldPicker ID="secondarySortFieldPicker" runat="server" SingleMode="True" />
                
                <asp:CustomValidator ID="valTopXXIsNumericAndGreaterOrEqualZero" OnServerValidate="TopXXIsNumericAndGreaterOrEqualZero_ServerValidate" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_YK0_36 %>" runat="server"></asp:CustomValidator>
                <asp:CustomValidator ID="valTopPercentIsNumericAndGreaterOrEqualZero" OnServerValidate="TopPercentIsNumericAndGreaterOrEqualZero_ServerValidate" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_YK0_36 %>" runat="server"></asp:CustomValidator>
            </td>

        </tr>
    </tbody>  
</table>

<asp:CustomValidator ID="CustomValidator1" runat="server" OnServerValidate="ValidateNumberOfPoints" Display="None"></asp:CustomValidator>








