<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditAxisPropertiesControl.ascx.cs" Inherits="Orion_Reports_Controls_EditAxisPropertiesControl" %>
<%@ Register TagPrefix="orion" TagName="FieldPicker" Src="~/Orion/Controls/FieldPicker/FieldPicker.ascx" %>

<orion:Include ID="Include1" runat="server" File="Reports/js/EditAxisPropertiesControl.js"/>


<orion:Include ID="Include2" runat="server" File="ReportChartEditAxis.css" />

<script>
    
    var controler<%= this.ClientID %> = new SW.Core.ReportChart.ChartEdit.AxisProperty.AxisController({
        clientID: '<%= this.ClientID %>',
        dataSeriesSelectionMode: '<%= (int)(this.Mode) %>',
        ddScaleMaxID: '<%= ddScaleMax.ClientID %>',
        ddChartFormatsID: '<%= ddChartFormats.ClientID %>',
        tbMaxScaleID: '<%= tbMaxScale.ClientID %>',
        dataSeriesTableID: '<%= dataSeriesTable.ClientID %>',
        axisNonePlaceHolderID: '<%= axisNonePlaceHolder.ClientID %>',
        axisPlaceHolderID: '<%= axisPlaceHolder.ClientID %>',
        btnLessText: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TP0_40) %>',
        btnMoreText: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TP0_39) %>',
        hfDataSeriesID: '<%= hfDataSeries.ClientID %>',
        hfDataSeriesDetailsID: '<%= DetailsHiddenField %>',
        dataSeriesDetailsID: '<%= DataDetails %>',
        lblDataSeriesID: '<%= lblDataSeries.ClientID %>',
        btnAddDataSeriesID: '<%= btnAddDataSeries.ClientID %>',
        onSelectDetails: '<%= BindDataJsFunction %>',
        ddUnitsDisplayedID: '<%= ddUnitsDisplayed.ClientID %>',
        tbCustomUnitID: '<%= tbCustomUnit.ClientID %>',
        editAxisPropertiesControlCreatedCallback: <%= this.OnCreatedJS %>
    });
</script>

<orion:FieldPicker ID="fieldPicker" runat="server" SingleMode="True" 
    AddFieldsButtonTextI18N="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_ZT0_11 %>"
    DialogTitleTextI18N="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_ZT0_11 %>"
    GroupByHeaderTextI18N="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_ZT0_13 %>"
    SelectedFieldsTextI18N="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_ZT0_14 %>"
    SearchWatermarkTextI18N="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_ZT0_15 %>"
    FieldsGridHeaderTextI18N="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_ZT0_16 %>"
      />
<asp:HiddenField runat="server" ID="hfDataSeries"/>

<div runat="server" ID="axis" class="wrapper">
    <table class="axisOverview">
        <thead>
            <tr>
                <th class="h3noWrap">
                    <%= DefaultSanitizer.SanitizeHtml(AxisName) %><span class="h4noWrap">&nbsp;<%= DefaultSanitizer.SanitizeHtml(AxisSubName) %></span>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr runat="server" ID="axisNonePlaceHolder">
                <td class="noDataBackgroud"><div><asp:Label runat="server" ID="lblNoData" CssClass="noData" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TP0_35 %>" ></asp:Label></div></td>
            </tr>
            <tr>
                <td>
                    <table runat="server" ID="axisPlaceHolder">
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblSectionTitle" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TP0_9 %>" CssClass="sectionTitle"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <asp:TextBox CssClass="customLabel"  runat="server" ID="tbCustomLabel"></asp:TextBox>
                            </td>
                        </tr>
                           <tr>
                            <td>
                                <asp:Label ID="lblUnitsDisplayed" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TP0_10 %>" CssClass="sectionTitle"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList runat="server" ID="ddUnitsDisplayed" CssClass="ddlWidth" />                        
                                <asp:TextBox runat="server" ID="tbCustomUnit" />
                            </td>
                        </tr>

                        <%-- (ZT) Temporary disabled, because not working. --%>
                        <tr  style="display: none">
                            <td>
                                <asp:Label ID="lblScalemax" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TP0_11 %>" CssClass="sectionTitle"></asp:Label>                        
                            </td>
                        </tr>
                        <tr  style="display: none">
                            <td>
                                <asp:DropDownList ID="ddScaleMax" runat="server" />
                            </td>
                        </tr>
                        <tr  style="display: none">
                            <td>
                                <asp:TextBox ID="tbMaxScale" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="lblChartType" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TP0_14 %>" CssClass="sectionTitle"></asp:Label></td>
                        </tr>
                        <tr>                
                            <td>
                                <asp:DropDownList runat="server" ID="ddChartFormats" CssClass="ddlWidth">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="lblShowSumOfCalculatedSeries" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TP0_16 %>" CssClass="sectionTitle"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox runat="server" ID="cbShowSumOfCalculatedSeries"/>
                                <asp:Label ID="lblShowSumOfCalculatedSeriesDescription" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_IB0_95 %>"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDataSeries" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_IB0_89 %>" CssClass="sectionTitle"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="potletItem">
                        <table class="dataSeriesTable" runat="server" ID="dataSeriesTable" >
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                     <orion:LocalizableButton  ID="btnAddDataSeries" runat="server" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_TP0_23 %>" DisplayType="Secondary"  />
                </td>
            </tr>
        </tbody>
    </table>

</div>
