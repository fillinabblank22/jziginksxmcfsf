﻿using System.Globalization;
using SolarWinds.Internationalization.Extensions;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Core.Reporting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.Reporting;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Reporting;
using SolarWinds.Reporting.Models.Data;
using SolarWinds.Reporting.Models.Selection;
using SolarWinds.Reporting.Models.Styles;
using SolarWinds.Reporting.Models.Tables;
using System;
using System.Web;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.Script.Serialization;
using SolarWinds.Orion.Core.Common.InformationService;
using SolarWinds.Orion.Core.Common.i18n;
using SolarWinds.Orion.Core.Reporting.Extensions;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Controls_ReportTable_ReportTableColumnLayout : ModelBasedControl<TableColumn[]>  
{
   private class SelectedAndExpandedColumnState
    {
        public bool IsSelected { get; set; }

        public bool IsExpanded { get; set; }
    }

    private Dictionary<string, SelectedAndExpandedColumnState> selectedAndExpandedColumnStates = null; 

    // In this field will be copied data from columns field in order which corresponds how user by drag and drop functionality reordered columns
    private List<TableColumn> orderedColumns;

    private readonly string DummyJsAction = "function(){}";

    private readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    public string OnCreatedJS { get; set; }

    public DataSource DataSource { get; set; }

    private TableColumnDefaultFactory columnDefaultFactory = null;
    private TableColumnDefaultFactory ColumnDefaultFactory
    {
        get
        {
            if (columnDefaultFactory == null)
            {
                columnDefaultFactory = new TableColumnDefaultFactory(DataSource);
            }

            return columnDefaultFactory;
        }
    }

    private IDataSourceFieldProvider _dataSourceFieldProvider = null;
    private IDataSourceFieldProvider DataSourceFieldProvider
    {
        get
        {
            if (_dataSourceFieldProvider == null)
            {
                _dataSourceFieldProvider = DataSourceFactory.GetFieldProvider(DataSource, new InformationServiceProxyCreator( () => SolarWinds.Orion.Web.InformationService.InformationServiceProxy.CreateV3()));
            }
            return _dataSourceFieldProvider;
        }
    }

    /// <summary>
    /// True when there are any non-DateTime Statistical colums
    /// </summary>
    protected bool IsTableStatistical
    {
        get { return ViewModel.HasStatisticalColumn(); }
    }

    protected string EventTarget
    {
        get { return Request.Form["__EVENTTARGET"]; }
    }

    protected string EventArgument
    {
        get { return Request.Form["__EVENTARGUMENT"]; }
    }

    private bool IsTabClosed
    {
        get { return EventTarget == "closetab"; }
    }

    /// <summary>
    /// Returns TRUE if was closed a tab with the last statistical column which is not datetime.
    /// </summary>
    private bool WasRemovedLastStatisticalColumn
    {
        get
        {
            if (IsTabClosed)
            {
                bool statisticalColumnRemoved;

                if (!bool.TryParse(EventArgument, out statisticalColumnRemoved))
                {
                    statisticalColumnRemoved = false;
                }

                return statisticalColumnRemoved && ViewModel.IsStatistical() && !ViewModel.HasStatisticalColumn();
            }

            return false;
        }
    }

    public bool HideAggregationUI { get; set; }
    
    public Orion_Controls_ReportTable_ReportTableColumnLayout()
    {
        OnCreatedJS = DummyJsAction;
    }

    /// <summary>
    /// Loads the view model. Method is called when postback happened
    /// </summary>
    public override void LoadViewModel()
    {
        base.LoadViewModel();
        selectedAndExpandedColumnStates = new Dictionary<string, SelectedAndExpandedColumnState>();
        
        this.orderedColumns = new List<TableColumn>();
        
        ParseDataFromRequest();

        ViewModel = orderedColumns.ToArray();

        // If a user removed a last statistical column we will automatically remove timestamp column.
        if (WasRemovedLastStatisticalColumn)
        {
            ViewModel = RemoveStatisticalTimestampColumnIfExists(ViewModel);
        }
    }

   

    private TableColumn[] RemoveStatisticalTimestampColumnIfExists(TableColumn[] tableColumns)
    {
        var survivingColumns = new List<TableColumn>(tableColumns.Length);
        
        foreach (TableColumn tableColumn in tableColumns)
        {
            if (tableColumn.IsDateTime() && tableColumn.IsStatistical())
            {
                continue;
            }

            survivingColumns.Add(tableColumn);
        }

        return survivingColumns.ToArray();
    }


    /// <summary>
    /// Gets the save unique ID. This is present because jquery is confused by $ letter in selector.
    /// </summary>
    /// <returns>UniqueID property where letters '$' are replaced by '-' letters.</returns>
    public string GetSaveUniqueID(string uniqueId)
    {
        string strRes = string.Empty;
        if (!string.IsNullOrEmpty(uniqueId))
        {
            strRes = uniqueId.Replace("$", "-");
        }

        return strRes;
    }

    /// <summary>
    /// Binds the view model.
    /// </summary>
    protected override void BindViewModel()
    {
        base.BindViewModel();

        fieldPicker.DataSource = DataSource;
        editFieldPicker.DataSource = DataSource;

        // ============ Start serializing data for columnLayoutConfig ==============
        if (ViewModel != null)
        {
            Dictionary<string, string>[] configOptions = new Dictionary<string, string>[ViewModel.Length];
            string saveUniqueId = GetSaveUniqueID(UniqueID);

            for (int i = 0; i < ViewModel.Length; i++)
            {
                ViewModel[i].DisplayName = WebSecurityHelper.SanitizeHtml(ViewModel[i].DisplayName);

                configOptions[i] = new Dictionary<string, string>();
                configOptions[i].Add("refHref", "#" + saveUniqueId + "-column-" + (i + 1));
                configOptions[i].Add("caption", ViewModel[i].DisplayName);
                configOptions[i].Add("captionWithSwisEntityIncluded", ViewModel[i].DisplayName + " - " + ViewModel[i].Field.OwnerDisplayName);
                configOptions[i].Add("refid", Convert.ToString(ViewModel[i].RefId));
                configOptions[i].Add("fieldid", ViewModel[i].Field.RefID);
                configOptions[i].Add("columndatatype", ViewModel[i].Field.DataTypeInfo.DataType.Data);
    

                bool isExpanded = false;
                bool isSelected = false;
                
                if ((selectedAndExpandedColumnStates != null) &&
                   (selectedAndExpandedColumnStates.ContainsKey(Convert.ToString(ViewModel[i].RefId))))
                {
                    var states = selectedAndExpandedColumnStates[Convert.ToString(ViewModel[i].RefId)];
                    isExpanded = states.IsExpanded;
                    isSelected = states.IsSelected;
                }

                configOptions[i].Add("transformid", ViewModel[i].TransformId);
                configOptions[i].Add("datacolumnname", ViewModel[i].PropertyName);
                configOptions[i].Add("isexpanded", Convert.ToString(isExpanded));
                configOptions[i].Add("isselected", Convert.ToString(isSelected));
                configOptions[i].Add("ishidden", Convert.ToString(ViewModel[i].IsHidden));
                configOptions[i].Add("ishtmltagsallowed", Convert.ToString(ViewModel[i].IsHTMLTagsAllowed));
                configOptions[i].Add("order", Convert.ToString(i));
                configOptions[i].Add("exist", "true");
                configOptions[i].Add("isstatistical", Convert.ToString(ViewModel[i].IsStatistical()));
            }

            try
            {
                columnLayoutConfig.Value = new JavaScriptSerializer().Serialize(configOptions);
            }
            catch (Exception e)
            {
                log.Warn("Can't serialize table config to JSON", e);
            }
        }

        if (ViewModel != null)
        {
            Dictionary<string, string>[] existingColumns = new Dictionary<string, string>[ViewModel.Length];
            for (int i = 0; i < ViewModel.Length; i++)
            {
                existingColumns[i] = new Dictionary<string, string>();
                existingColumns[i].Add("RefId", Convert.ToString(ViewModel[i].RefId));
            }

            try
            {
                previousExistedColumns.Value = new JavaScriptSerializer().Serialize(existingColumns);
            }
            catch (Exception e)
            {
                log.Warn("Can't serialize existing columns to JSON", e);
            }
        }

        // Finds all fields which are used on page from data source filter and table columns and pass it to JS code.
        // Fields will be used on fieldpicker to limit the selection according to the longest navigation path.
        allFieldsRefIDsContainer.Value = new JavaScriptSerializer().Serialize(GetAllFields());
    }

    /// <summary>
    /// Sends server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter" /> object, which writes the content to be rendered on the client.
    /// </summary>
    /// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter" /> object that receives the server control content.</param>
    protected override void Render(HtmlTextWriter writer)
    {
        string saveUniqueId = GetSaveUniqueID(UniqueID);
        tabTemplate.Text = TokenSubstitution.Parse(tabTemplate.Text);

        // Function (Data aggregation)
        var aggregations =
            Enum.GetValues(typeof(Aggregate))
                .Cast<Aggregate>()
                .Select(item => new { Value = item, Text = item.LocalizedLabel() });

        writer.AddAttribute("class", "tableLayoutEditor");
        writer.RenderBeginTag("div"); // this div was added just because to be able easily specify css classes for ui tabs and sizables
        writer.AddAttribute("id", saveUniqueId);
        writer.RenderBeginTag("div");
        writer.RenderBeginTag("ul");
        int i = 1;
   
        if (ViewModel != null)
        {
            int colNumber = 0;
            foreach (TableColumn column in ViewModel)
            {
                writer.AddAttribute("data-tabindex", Convert.ToString(colNumber++));
                bool isExpanded = false;
                bool isSelected = false;
                
                if ((selectedAndExpandedColumnStates != null) &&
                    (selectedAndExpandedColumnStates.ContainsKey(Convert.ToString(column.RefId))))
                {
                    var states = selectedAndExpandedColumnStates[Convert.ToString(column.RefId)];
                    isExpanded = states.IsExpanded;
                    isSelected = states.IsSelected;
                }
                
                if (isExpanded && isSelected)
                {
                    writer.AddAttribute("class", "moreDetails");
                }
   
                writer.RenderBeginTag("li");
                writer.AddAttribute("data-refid", Convert.ToString(column.RefId));
                writer.AddAttribute("data-fieldid", column.Field.RefID);
                writer.AddAttribute("data-datacolumnname", column.PropertyName);
                writer.AddAttribute("data-transformid", column.TransformId);
                writer.AddAttribute("data-ishidden", column.IsHidden.ToString());
                writer.AddAttribute("data-ishtmltagsallowed", column.IsHTMLTagsAllowed.ToString());
                writer.AddAttribute("data-isstatistical", column.IsStatistical().ToString());
                if ((column.Field.DataTypeInfo != null) && (column.Field.DataTypeInfo.DataType != null) && (column.Field.DataTypeInfo.DataType.Data != null))
                {
                    writer.AddAttribute("data-columndatatype", column.Field.DataTypeInfo.DataType.Data);
                }
                
                writer.AddAttribute("data-tabcaption", column.DisplayName);
                string captionWithSwisEntityIncluded = column.DisplayName;
                if (!string.IsNullOrEmpty(captionWithSwisEntityIncluded))
                {
                    captionWithSwisEntityIncluded = column.DisplayName + " - " + column.Field.OwnerDisplayName;
                }

                writer.AddAttribute("data-captionwithswisentityincluded", captionWithSwisEntityIncluded);

                writer.AddAttribute("href", "#" + saveUniqueId + "-column-" + i);
                writer.RenderBeginTag("a");

                // render div header (tab header)
                string headerCssClass = "header";
                if (column.IsHidden)
                {
                    headerCssClass += " ui-state-disabled";
                }

                writer.AddAttribute("class", headerCssClass);
                writer.RenderBeginTag("div");

                writer.AddAttribute("style", "width: 8em;");
                writer.AddAttribute("cellspacing", "0");
                writer.AddAttribute("cellpadding", "2");
                writer.AddAttribute("border", "0");
                writer.RenderBeginTag("table");
                writer.RenderBeginTag("tbody");
                writer.RenderBeginTag("tr");
                writer.AddAttribute("width", "17px");
                writer.RenderBeginTag("td");

                // maybe here will be produced img tag with icon
                writer.AddAttribute("class", "grabby");
                writer.AddAttribute("src", "/Orion/images/Reports/GrabbyThing.png");
                writer.RenderBeginTag("img");
                writer.RenderEndTag();
                writer.RenderEndTag(); // end td

                writer.AddAttribute("class", "column-no-wrap");
                writer.RenderBeginTag("td");

                // Placeholder for Hidden icon with tooltip.
                string iconCssClass = "hiddenColumnIconHolder";
                if (column.IsHidden)
                {
                   iconCssClass +=  " hidden-column";
                }
                writer.AddAttribute("class", iconCssClass);

                writer.AddAttribute("ext:qtip", Resources.CoreWebContent.WEBDATA_ZT0_39);
                writer.RenderBeginTag("span");
                writer.RenderEndTag();

                // Placeholder for Timestamp icon with tooltip. It is managed by JS code.
                iconCssClass = "timestampColumnIconHolder";
    
                writer.AddAttribute("class", iconCssClass);
                writer.AddAttribute("ext:qtip",  Resources.CoreWebContent.WEBDATA_ZT0_38);
                writer.RenderBeginTag("span");
                writer.RenderEndTag();


                // Placeholder for Historical icon with tooltip.
                iconCssClass = "historicalColumnIconHolder";
                
                if (column.IsStatistical() && !column.IsDateTime())
                {
                    iconCssClass += " statistic-column";
                    writer.AddAttribute("ext:qtip", Resources.CoreWebContent.WEBDATA_ZT0_37);
                }

                writer.AddAttribute("class", iconCssClass);
              
                writer.RenderBeginTag("span");
                writer.RenderEndTag();
                writer.RenderEndTag(); // end td
                
                writer.AddAttribute("class", "ReportHeader");
                writer.RenderBeginTag("td");
                writer.AddAttribute("style", "white-space: nowrap"); // needed because IE7
                writer.RenderBeginTag("span");
                writer.Write(column.DisplayName);
                writer.RenderEndTag();
                writer.RenderEndTag(); // end td

                writer.AddAttribute("width", "17px");
                writer.RenderBeginTag("td");


                writer.AddAttribute("src", "/Orion/images/Reports/delete_icon16x16.png");
                writer.AddAttribute("class", "ui-icon-close");               
                
                if (!CanBeColumnDeleted(column))
                {
                    writer.AddAttribute("style", "display:none;");
                }
                writer.RenderBeginTag("img");
                writer.RenderEndTag();
                
                writer.RenderEndTag(); // end td

                writer.RenderEndTag(); // end tr
                writer.RenderEndTag(); // end tbody
                writer.RenderEndTag(); // end table
                writer.RenderEndTag(); // end div header

                // render div footer (tab footer)
                if (isExpanded && isSelected)
                {
                    writer.AddAttribute("class", "footer footerLessDetails");
                }
                else
                {
                    writer.AddAttribute("class", "footer");
                }

                writer.RenderBeginTag("div");

                // render expand content tag
                if (!isExpanded)
                {
                    writer.AddAttribute("src", "/Orion/images/Button.Expand.gif");
                    writer.AddAttribute("class", "collapsed collapseTabContent-icon");
                }
                else
                {
                    writer.AddAttribute("src", "/Orion/images/Button.Collapse.gif");
                    writer.AddAttribute("class", "collapseTabContent-icon");
                }

                writer.AddAttribute("data-contentid", saveUniqueId + "-column-" + i);
                writer.RenderBeginTag("img");

                writer.RenderEndTag();
                writer.RenderBeginTag("span");
                if (!isExpanded)
                {
                    writer.Write(Resources.CoreWebContent.WEBCODE_PS0_7);
                }
                else
                {
                    writer.Write(Resources.CoreWebContent.WEBCODE_PS0_8);
                }

                writer.RenderEndTag();
                writer.RenderEndTag();
                writer.RenderEndTag();
                writer.RenderEndTag();

                i++;
            }
        }

        // Render add column tab
        writer.AddAttribute("class", "addColumn");
        writer.AddAttribute("style", "width: 100px");
        writer.RenderBeginTag("li");
        writer.AddAttribute("data-tabcaption", string.Empty);
        writer.AddAttribute("data-refid", Convert.ToString(Guid.Empty));
        writer.AddAttribute("data-fieldid", Convert.ToString(Guid.Empty));
        writer.AddAttribute("data-ishidden", false.ToString());
        writer.AddAttribute("data-ishtmltagsallowed", true.ToString());
        writer.AddAttribute("href", "#" + saveUniqueId + "-column-add");
        writer.AddAttribute("style", "width: 100%");
        writer.RenderBeginTag("a");
        writer.AddAttribute("class", "header");
        writer.RenderBeginTag("div");
        writer.AddAttribute("src", "/Orion/images/SubViewImages/Add.png");
        writer.AddAttribute("class", "addNewTab-icon");
        writer.RenderBeginTag("img");
        writer.RenderEndTag();
        writer.RenderEndTag();
        writer.AddAttribute("class", "footer addColumnFooter");
        writer.RenderBeginTag("div");
        writer.Write("&nbsp;");
        writer.RenderEndTag(); // end div
        writer.RenderEndTag();
        writer.RenderEndTag();

        writer.RenderEndTag();

        var alignmentsToExclude = new Enum[] {TextAlignment.NotSpecified, TextAlignment.InferFromValueType,};

        // render tab details for columns
        i = 1;
        if (ViewModel != null)
        {
            foreach (TableColumn column in ViewModel)
            {
                writer.AddAttribute("id", saveUniqueId + "-column-" + i);
                bool isExpanded = false;
                bool isSelected = false;
               
                if ((selectedAndExpandedColumnStates != null) &&
                   (selectedAndExpandedColumnStates.ContainsKey(Convert.ToString(column.RefId))))
                {
                    var states = selectedAndExpandedColumnStates[Convert.ToString(column.RefId)];
                    isExpanded = states.IsExpanded;
                    isSelected = states.IsSelected;
                }

                
                if (!isExpanded || !isSelected)
                {
                    writer.AddAttribute("style", "display: none");
                }

                writer.RenderBeginTag("div");
                
                if (!isExpanded)
                {
                    writer.AddAttribute("style", "display: none");
                }

                writer.RenderBeginTag("div");

                string templateContent = tabTemplate.Text.Replace("{{UniqueID}}", saveUniqueId);
                templateContent = templateContent.Replace("{{tabNumber}}", Convert.ToString(i));
                templateContent = templateContent.Replace("{{DatabaseColumnName}}", column.PropertyName);
                templateContent = templateContent.Replace("{{DisplayNameValue}}", HttpUtility.HtmlEncode(column.DisplayName));
                templateContent = templateContent.Replace("{{transformId}}", column.TransformId);
                templateContent = templateContent.Replace("{{detailid}}", saveUniqueId + "-column-" + i);
                

                // in future will be taken from model
                templateContent = templateContent.Replace("{{DefaultDisplayValue}}", column.IsHidden ? "checked" : string.Empty);
                templateContent = templateContent.Replace("{{AllowHTMLTagsValue}}", column.IsHTMLTagsAllowed ? "checked" : string.Empty);
                templateContent = templateContent.Replace("{{AggregationStyle}}", HideAggregationUI ? "display:none;" : string.Empty);

                StringBuilder functionOptions = new StringBuilder();
                string selected = string.Empty;
                foreach (var aggregation in aggregations)
                {
                    if ((column.Summary != null) && (column.Summary.Calculation == aggregation.Value))
                    {
                        selected = "selected";
                    }

                    if (IsAggregationAllowedForDataType(aggregation.Value, column.Field.DataTypeInfo.DataType, column.Field.DataTypeInfo.DeclType))
                    {
                        functionOptions.AppendFormat("<option value=\"{0}\" {1}>{2}</option>",
                                                     Convert.ToString(aggregation.Value),
                                                     selected, aggregation.Text);
                    }
                    selected = string.Empty;
                }

                templateContent = templateContent.Replace("{{FunctionOptions}}", functionOptions.ToString());

                // Alignment
                string alingmentOptions = CreateOptionsFromEnum(column.CellStyle != null ? column.CellStyle.TextAlign : TextAlignment.Left, alignmentsToExclude);
                templateContent = templateContent.Replace("{{AlingmentOptions}}", alingmentOptions);

                // Column width
                templateContent = templateContent.Replace("{{ColumnWidthDynamicWidthChecked}}", (!column.PercentWidth.HasValue) ? "checked" : string.Empty);
                templateContent = templateContent.Replace("{{ColumnWidthCustomWidthChecked}}", (column.PercentWidth.HasValue) ? "checked" : string.Empty);
                templateContent = templateContent.Replace("{{ColumnWidthInPixelsValue}}", (column.FixedWidth.HasValue) ? Convert.ToString(column.FixedWidth.Value) : string.Empty);
                templateContent = templateContent.Replace("{{ColumnWidthInPercentsValue}}", (column.PercentWidth.HasValue) ? Convert.ToString(column.PercentWidth.Value) : string.Empty);
                                              
                // Valid range
                var validRangeOptions = CreateOptionsFromEnum(column.ValidRange);
                templateContent = templateContent.Replace("{{ValidRangeOptions}}", validRangeOptions);
                

                writer.Write(templateContent);

                writer.RenderEndTag();
                writer.RenderEndTag();
                i++;
            }
        }

        // render content for column-add tab
        writer.AddAttribute("id", saveUniqueId + "-column-add");
        writer.RenderBeginTag("div");
        writer.AddAttribute("style", "display: none");
        writer.RenderBeginTag("div");

        writer.RenderEndTag();
        writer.RenderEndTag();
        writer.RenderEndTag();
        writer.RenderEndTag();

        // function
        StringBuilder functionOptionsG = new StringBuilder();
        foreach (var aggregation in aggregations)
        {
            functionOptionsG.AppendFormat("<option value=\"{0}\">{1}</option>", Convert.ToString(aggregation.Value),
                                          GetLocalizedProperty("Aggregate", aggregation.Text));
        }

        tabTemplate.Text = tabTemplate.Text.Replace("{{FunctionOptions}}", functionOptionsG.ToString());

        string textAlingmentOptions = CreateOptionsFromEnum(TextAlignment.Left, alignmentsToExclude);
        tabTemplate.Text = tabTemplate.Text.Replace("{{AlingmentOptions}}", textAlingmentOptions);

        string rangeOptions = CreateOptionsFromEnum(ValidRange.NotSpecified);
        tabTemplate.Text = tabTemplate.Text.Replace("{{ValidRangeOptions}}", rangeOptions);

        base.Render(writer);
    }

    /// <summary>
    /// Return True if column can be deleted by user. 
    /// Usually, user can NOT delete timestamp column in historical table.
    /// </summary>
    /// <param name="column"></param>
    /// <returns></returns>
    private bool CanBeColumnDeleted(TableColumn column)
    {
        // We can not delete timestamp column in historical table.
        if (ViewModel.HasStatisticalColumn() && column.IsDateTime() && column.IsStatistical())
        {
            return false;
        }
        return true;
    }

    private string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }

    private string CreateOptionsFromEnum(Enum selectedValue, params Enum[] excludedItems)
    {
        excludedItems = excludedItems ?? new Enum[0];
        var ranges = Enum.GetValues(selectedValue.GetType())
            .Cast<Enum>()
            .SkipWhile(item => excludedItems.Any(e => e.Equals(item)))
            .Select(item => new { Value = item, Text = item.LocalizedLabel() });
        var options = new StringBuilder();
        string selected = string.Empty;
        foreach (var range in ranges)
        {
            if (range.Value.Equals(selectedValue))
            {
                selected = "selected";
            }
            options.AppendFormat("<option value=\"{0}\" {1}>{2}</option>", range.Value, selected, range.Text);
            selected = string.Empty;
        }
        return options.ToString();
    }

    private Field ConvertFromFieldId(string fieldId)
    {
        Field field;
        if (!DataSourceFieldProvider.TryGetField(fieldId, out field))
        {
            // if field is not a part of data source then create a field based on information provided
            field = new Field
            {
                RefID = fieldId,
                DataTypeInfo = new DataDeclaration { DataType = new DataType() }
            };
            log.DebugFormat("Requested field '{0}' was not found by field provider '{1}'.", fieldId, DataSourceFieldProvider);
        }
        return field;
    }

    /// <summary>
    /// Determines whether [is aggregation allowed for data type] [the specified aggregate].
    /// </summary>
    /// <param name="aggregate">The aggregate type</param>
    /// <param name="dataType">Type of the data.</param>
    /// <returns>
    ///   <c>true</c> if [is aggregation allowed for data type] [the specified aggregate]; otherwise, <c>false</c>.
    /// </returns>
    private bool IsAggregationAllowedForDataType(Aggregate aggregate, DataType dataType, DataDeclarationType declType)
    {
        if (dataType == DataType.DateTime)
        {
            if ((aggregate == Aggregate.Sum) || (aggregate == Aggregate.Average))
            {
                return false;
            }
        }
        else if ((dataType == DataType.Char) || (dataType == DataType.String))
        {
            if ((aggregate == Aggregate.Sum) || (aggregate == Aggregate.Average))
            {
                return false;
            }
        }
        else if (dataType == DataType.Boolean)
        {
            if ((aggregate == Aggregate.Sum) || (aggregate == Aggregate.Average) || (aggregate == Aggregate.Min) ||
                (aggregate == Aggregate.Max))
            {
                return false;
            }
        }

        if (declType == DataDeclarationType.Enumerated)
        {
            if (aggregate == Aggregate.Sum || aggregate == Aggregate.Average)
                return false;
        }

        return true;
    }

    private void SetColumnValue(string columnName, int columnIndex, Dictionary<int, TableColumn> columns, object value)
    {
        if (!columns.ContainsKey(columnIndex))
        {
            var newColumn = new TableColumn();
            newColumn.HeaderStyle = new CellStyling();
            newColumn.CellStyle = new CellStyling();
            newColumn.Summary = new TableColumnSummary();
           
            columns.Add(columnIndex, newColumn);
        }

        if (value == null)
        {
            return;
        }

        string dummyValue = Convert.ToString(value);
        if (columnName == "DisplayName")
        {
            columns[columnIndex].DisplayName = HttpUtility.HtmlDecode(Convert.ToString(value));
        }
        else if (columnName == "DefaultDisplay")
        {
            columns[columnIndex].IsHidden = true;

        }
        else if (columnName == "AllowHTMLTags")
        {
            columns[columnIndex].IsHTMLTagsAllowed = true;

        }
        else if (columnName == "Alignment")
        {
            TextAlignment dummyAlignment;
            if (Enum.TryParse(dummyValue, out dummyAlignment))
            {
                columns[columnIndex].CellStyle.TextAlign = dummyAlignment;
                columns[columnIndex].HeaderStyle.TextAlign = dummyAlignment; // we want to have same align also for column label in column header
            }
        }
        else if (columnName == "ColumnWidth")
        {
            if (dummyValue == "FixedWidth")
            {
                //columns[columnIndex].FixedWidth = 100;
                columns[columnIndex].PercentWidth = 10;
            }
        }
        else if (columnName == "ColumnWidthInPixels")
        {
            // Check whether column wasn't select as Dynamic, because for dynamick shouldn't be set fixed value
            if (!string.IsNullOrEmpty(dummyValue) && columns[columnIndex].FixedWidth.HasValue)
            {
                int colWidth = 0;
                Int32.TryParse(dummyValue, out colWidth);
                columns[columnIndex].FixedWidth = colWidth;
            }
        }
        else if (columnName == "ColumnWidthInPercents")
        {
            if (!string.IsNullOrEmpty(dummyValue) && columns[columnIndex].PercentWidth.HasValue)
            {
                double colWidth = 0;
				double.TryParse(dummyValue, NumberStyles.Float, CultureInfo.CurrentCulture, out colWidth);
                columns[columnIndex].PercentWidth = colWidth;
            }
        }
        else if (columnName == "Function")
        {
            Aggregate dummyAggregate;
            if (Enum.TryParse(dummyValue, out dummyAggregate))
            {
                columns[columnIndex].Summary.Calculation = dummyAggregate;
            }
        }
        else if (columnName == "ValidRange")
        {
            ValidRange dummyValidRange;
            if (Enum.TryParse(dummyValue, out dummyValidRange))
            {
                columns[columnIndex].ValidRange = dummyValidRange;
            }
        }
        else if (columnName == "TransformId")
        {
            columns[columnIndex].TransformId = string.IsNullOrWhiteSpace(dummyValue) ? null : dummyValue;
        }
    }

    private void ParseDataFromRequest()
    {
        Dictionary<int, TableColumn> columns = new Dictionary<int, TableColumn>();
        
        // In the case, that was invoked full postback we will parse it by this way
        string strSaveUniqueId = GetSaveUniqueID(UniqueID);
        string displayNameKey = strSaveUniqueId + "-DisplayName-";
        string defaultDisplayKey = strSaveUniqueId + "-DefaultDisplay-";
        string htmlTagsAllowedKey = strSaveUniqueId + "-AllowHTMLTags-";
        string alignmentKey = strSaveUniqueId + "-Alignment-";
        string columnWidthKey = strSaveUniqueId + "-ColumnWidth-";
        string ColumnWidthInPixelsKey = strSaveUniqueId + "-ColumnWidthInPixels-";
        string TransformId = strSaveUniqueId + "-TransformId-";
        string ColumnWidthInPercentsKey = strSaveUniqueId + "-ColumnWidthInPercents-";
       
        string functionKey = strSaveUniqueId + "-Function-";
        string validRangeKey = strSaveUniqueId + "-ValidRange-";
        foreach (var key in Request.Form.AllKeys)
        {
            int index = 0;
            string strIndex = string.Empty;
            if (key.StartsWith(displayNameKey))
            {
                strIndex = key.Substring(displayNameKey.Length);
                if (int.TryParse(strIndex, out index))
                {
                    SetColumnValue("DisplayName", index, columns, Request[key]);
                }
            }
            else if (key.StartsWith(defaultDisplayKey))
            {
                strIndex = key.Substring(defaultDisplayKey.Length);
                if (int.TryParse(strIndex, out index))
                {
                    SetColumnValue("DefaultDisplay", index, columns, Request[key]);
                }
            }
            else if (key.StartsWith(htmlTagsAllowedKey))
            {
                strIndex = key.Substring(htmlTagsAllowedKey.Length);
                if (int.TryParse(strIndex, out index))
                {
                    SetColumnValue("AllowHTMLTags", index, columns, Request[key]);
                }
            }
            else if (key.StartsWith(alignmentKey))
            {
                strIndex = key.Substring(alignmentKey.Length);
                if (int.TryParse(strIndex, out index))
                {
                    SetColumnValue("Alignment", index, columns, Request[key]);
                }
            }
            else if (key.StartsWith(columnWidthKey))
            {
                strIndex = key.Substring(columnWidthKey.Length);
                if (int.TryParse(strIndex, out index))
                {
                    SetColumnValue("ColumnWidth", index, columns, Request[key]);
                }
            }
            else if (key.StartsWith(ColumnWidthInPixelsKey))
            {
                strIndex = key.Substring(ColumnWidthInPixelsKey.Length);
                if (int.TryParse(strIndex, out index))
                {
                    SetColumnValue("ColumnWidthInPixels", index, columns, Request[key]);
                }
            }
            else if (key.StartsWith(ColumnWidthInPercentsKey))
            {
                strIndex = key.Substring(ColumnWidthInPercentsKey.Length);
                if (int.TryParse(strIndex, out index))
                {
                    SetColumnValue("ColumnWidthInPercents", index, columns, Request[key]);
                }
            }
            else if (key.StartsWith(functionKey))
            {
                strIndex = key.Substring(functionKey.Length);
                if (int.TryParse(strIndex, out index))
                {
                    SetColumnValue("Function", index, columns, Request[key]);
                }
            }
            else if (key.StartsWith(validRangeKey))
            {
                strIndex = key.Substring(validRangeKey.Length);
                if (int.TryParse(strIndex, out index))
                {
                    SetColumnValue("ValidRange", index, columns, Request[key]);
                }
            }
            else if (key.StartsWith(TransformId))
            {
                strIndex = key.Substring(TransformId.Length);
                if (int.TryParse(strIndex, out index))
                {
                    SetColumnValue("TransformId", index, columns, Request[key]);
                }
            }
        }

        StoreParsedColumnsInUserDefinedOrder(strSaveUniqueId, columns);

        if (Page.IsPostBack)
        {
            this.orderedColumns = EnsureThatNewAddedColumnsWillHaveDefaultValues(this.orderedColumns);
        }
    }

    private void StoreParsedColumnsInUserDefinedOrder(string strSaveUniqueId, Dictionary<int, TableColumn> columns)
    {
        dynamic dColumnsStates = null;
        try
        {
            dColumnsStates =
                new System.Web.Script.Serialization.JavaScriptSerializer().DeserializeObject(columnLayoutConfig.Value);
        }
        catch (Exception)
        {
        }

        if (dColumnsStates != null)
        {
            DataTable columnStatusesTable = new DataTable();
            columnStatusesTable.Columns.Add(new DataColumn("refHref", typeof (string)));
            columnStatusesTable.Columns.Add(new DataColumn("caption", typeof (string)));
            columnStatusesTable.Columns.Add(new DataColumn("refid", typeof (Guid)));
            columnStatusesTable.Columns.Add(new DataColumn("fieldid", typeof (string)));
            columnStatusesTable.Columns.Add(new DataColumn("datacolumnname", typeof (string)));
            columnStatusesTable.Columns.Add(new DataColumn("columndatatype", typeof(string)));
            columnStatusesTable.Columns.Add(new DataColumn("isexpanded", typeof (bool)));
            columnStatusesTable.Columns.Add(new DataColumn("isselected", typeof (bool)));
            columnStatusesTable.Columns.Add(new DataColumn("order", typeof (int)));
            columnStatusesTable.Columns.Add(new DataColumn("transformid", typeof (string)));

            for (int i = 0; i < dColumnsStates.Length; i++)
            {
                DataRow newRow = columnStatusesTable.NewRow();
                newRow["refHref"] = dColumnsStates[i]["refHref"];
                newRow["caption"] = dColumnsStates[i]["caption"];
                if (dColumnsStates[i].ContainsKey("refid"))
                {
                    newRow["refid"] = dColumnsStates[i]["refid"];
                }

                if (dColumnsStates[i].ContainsKey("fieldid"))
                {
                    newRow["fieldid"] = dColumnsStates[i]["fieldid"];
                }

                if (dColumnsStates[i].ContainsKey("datacolumnname"))
                {
                    newRow["datacolumnname"] = dColumnsStates[i]["datacolumnname"];
                }

                if (dColumnsStates[i].ContainsKey("columndatatype"))
                {
                    newRow["columndatatype"] = dColumnsStates[i]["columndatatype"];
                }

                if (dColumnsStates[i].ContainsKey("transformid"))
                {
                    newRow["transformid"] = dColumnsStates[i]["transformid"];    
                }

                newRow["isexpanded"] = dColumnsStates[i]["isexpanded"];
                newRow["isselected"] = dColumnsStates[i]["isselected"];
                newRow["order"] = dColumnsStates[i]["order"];


                columnStatusesTable.Rows.Add(newRow);
            }

            DataRow[] rows = columnStatusesTable.Select(string.Empty, "order ASC");
            string refHrefKey = "#" + strSaveUniqueId + "-column-";
            foreach (var row in rows)
            {
                string refHref = (row["refHref"] != null) ? Convert.ToString(row["refHref"]) : string.Empty;
                string strIndex = refHref.Substring(refHrefKey.Length);
                string strIsExpanded = (row["isexpanded"] != null) ? Convert.ToString(row["isexpanded"]) : string.Empty;
                bool isExpanded = false;
                bool.TryParse(strIsExpanded, out isExpanded);
                string strIsSelected = (row["isselected"] != null) ? Convert.ToString(row["isselected"]) : string.Empty;
                bool isSelected = false;
                bool.TryParse(strIsSelected, out isSelected);
                int index = 0;
                if (int.TryParse(strIndex, out index))
                {
                    if (columns.ContainsKey(index))
                    {
                        columns[index].RefId = (row["refid"] != DBNull.Value)
                                                   ? new Guid(Convert.ToString(row["refid"]))
                                                   : Guid.Empty;

                        columns[index].TransformId = (row["transformid"] != DBNull.Value)
                                                   ? Convert.ToString(row["transformid"])
                                                    : string.Empty;

                        columns[index].Field = ConvertFromFieldId((row["fieldid"] != DBNull.Value)
                                                   ? Convert.ToString(row["fieldid"])
                                                   : string.Empty);

                        columns[index].Field.DataTypeInfo.DataType.Data = (row["columndatatype"] != DBNull.Value) 
                                                    ? Convert.ToString(row["columndatatype"]) 
                                                    : string.Empty;
                        

                        string dataColumnName = (row["datacolumnname"] != DBNull.Value)
                                                ? Convert.ToString(row["datacolumnname"]) : string.Empty;
                        
                        if (!selectedAndExpandedColumnStates.ContainsKey(Convert.ToString(columns[index].RefId)))
                        {
                            selectedAndExpandedColumnStates.Add(Convert.ToString(columns[index].RefId), new SelectedAndExpandedColumnState { IsExpanded = isExpanded, IsSelected = isSelected });
                        }
                        
                        columns[index].PropertyName = dataColumnName;

                        this.orderedColumns.Add(columns[index]);
                    }
                }
            }
        }
        else
        {
            this.orderedColumns.AddRange(columns.Values);
        }
    }

    private List<TableColumn> EnsureThatNewAddedColumnsWillHaveDefaultValues(List<TableColumn> orderedColumns)
    {
        List<TableColumn> res = new List<TableColumn>();
        List<Guid> existedColumns = new List<Guid>();
        if (!string.IsNullOrEmpty(previousExistedColumns.Value))
        {
            dynamic dExistedColumns = null;
            try
            {
                dExistedColumns =
                    new System.Web.Script.Serialization.JavaScriptSerializer().DeserializeObject(
                        previousExistedColumns.Value);
            }
            catch (Exception)
            {
            }

            if (dExistedColumns != null)
            {
                for (int i = 0; i < dExistedColumns.Length; i++)
                {
                    var refId = Convert.ToString(dExistedColumns[i]["RefId"]);
                    existedColumns.Add(new Guid(refId));
                }
            }
        }

        foreach (var tableColumn in orderedColumns)
        {
            if (existedColumns.Contains(tableColumn.RefId))
            {
                res.Add(tableColumn);
            }
            else
            {
                var newColumn = ColumnDefaultFactory.ManufactureColumn(tableColumn.Field.RefID, false);
                newColumn.RefId = tableColumn.RefId;
                newColumn.IsHidden = tableColumn.IsHidden;
                res.Add(newColumn);
            }
        }

        return res;
    }

    private string[] GetAllFields()
    {
        var allFields = new List<Field>();
        var fieldManager = new FieldPickerManager();
        
        // Adds fields from dataSource filter/where clause.    
        var filterFields = fieldManager.GetFilterFields(DataSource);
        allFields.AddRange(filterFields);

        if (ViewModel != null && ViewModel.Length != 0)
        {
            // Adds fields from selected columns.
            var columnFields = ViewModel.Where(colum => colum.Field != null).Select(column => column.Field);
            allFields.AddRange(columnFields);
        }

        return allFields.Select(f => f.RefID.Data).ToArray();
    }
}
