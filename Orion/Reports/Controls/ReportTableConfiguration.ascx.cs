﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Reporting;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Reporting;
using SolarWinds.Reporting.Models.Selection;
using SolarWinds.Reporting.Models.Tables;
using SolarWinds.Reporting.Models.Layout;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Reporting;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Json;
using SolarWinds.Reporting.Impl.Presentation;
using SolarWinds.Orion.Core.Common.i18n;
using SolarWinds.Orion.Core.Common.InformationService;

public partial class Orion_Reports_Controls_ReportTableConfiguration : ModelBasedControl<TableConfiguration>
{
    public DataSource DataSource { get; set; }
    public ReportDataProviderFactory.ReportDataProviderBase ReportDataProvider { get; set; }

    public bool CreateCaptionWithSwisEntityIncluded
    {
        get
        {
            return ((DataSource.Type != DataSourceType.CustomSQL) && (DataSource.Type != DataSourceType.CustomSWQL));
        }
    }

    public override void LoadViewModel()
    {
        base.LoadViewModel();

        reportTableColumnLayout.DataSource = DataSource;

        foreach (var tableColumn in reportTableColumnLayout.ViewModel)
        {
            var dummyColumn = presenters.ViewModel.FirstOrDefault(item => item.RefId == tableColumn.RefId);
            if ((dummyColumn != null) && dummyColumn.Presenters.Any())
            {
                tableColumn.Presenters = dummyColumn.Presenters;
            }
        }

        ViewModel = new TableConfiguration()
        {
            Columns = reportTableColumnLayout.ViewModel, 
            TimeField = reportTableTimeRelativeConfiguration.ViewModel.TimeField == null ||
                        !reportTableColumnLayout.ViewModel.Any( column => column.Field.RefID == reportTableTimeRelativeConfiguration.ViewModel.TimeField.RefID) ? //Make sure that the selected timefield exists in the list of columns
                                                          null : reportTableTimeRelativeConfiguration.ViewModel.TimeField,
            Sorts = reportTableSort.ViewModel,    
            Indents = reportTableGroup.ViewModel,
            Filter = reportTableFilter.ViewModel,
            SummarizeMode = reportTableTimeRelativeConfiguration.ViewModel.Summarization.Item1
        };

        bool suitableForSummarizing = (ViewModel.Columns != null) && ViewModel.Columns.Any(item => IsSuitableForSumarizing(item));
        if (!suitableForSummarizing)
        {
            ViewModel.SummarizeMode = DataSummarizeMode.NoDataSummarization;
            reportTableTimeRelativeConfiguration.ViewModel.Summarization = Tuple.Create(DataSummarizeMode.NoDataSummarization);
        }

        SetTimeField(ViewModel);

		if (IsFederationEnabled)
		{
			ViewModel.Filter.Limit.OrionServerIDsToIgnore = ServerFilter.GetCommaSeparatedServerIDsToSkip();
		}
    }

    private static bool IsSuitableForSumarizing(TableColumn tableColumn)
    {
        // Only datetime field from table is suitable for sumarizing.
        return tableColumn != null
               && tableColumn.Field != null
               && tableColumn.Field.DataTypeInfo != null
               && tableColumn.Field.DataTypeInfo.DataType != null
               && tableColumn.Field.DataTypeInfo.DataType == SolarWinds.Reporting.Models.Data.DataType.DateTime;
    }

    protected override void BindViewModel()
    {
        base.BindViewModel();

		if (IsFederationEnabled)
		{
			String orionServerIDsToIgnore = ViewModel.Filter != null && ViewModel.Filter.Limit != null ? 
				ViewModel.Filter.Limit.OrionServerIDsToIgnore: String.Empty;

			ServerFilter.Initialize(orionServerIDsToIgnore);
		}

        reportTableColumnLayout.ViewModel = ViewModel.Columns;
        reportTableColumnLayout.DataSource = DataSource;
        reportTableSort.ViewModel = ViewModel.Sorts;
        reportTableGroup.ViewModel = ViewModel.Indents;
        reportTableFilter.ViewModel = ViewModel.Filter;
        reportTableTimeRelativeConfiguration.ViewModel = new TimeRelativeConfigurationView
        {
            TimeField = ViewModel.TimeField,
            Summarization = Tuple.Create(ViewModel.SummarizeMode),
            TableIsSuitableForSumarizing = (ViewModel.Columns != null) && ViewModel.Columns.Any(item => IsSuitableForSumarizing(item))
        };

        if (DataSource != null)
        {
            reportTableSort.Visible = reportTableFilter.Visible = reportTableTimeRelativeConfiguration.Visible = DataSource.SupportsAdvancedFiltering();
            reportTableColumnLayout.HideAggregationUI = !DataSource.SupportsAdvancedFiltering();
        }

        ValidateTableConfiguration(ViewModel, DataSource);

        presenters.ViewModel = ViewModel.Columns;
    }

    protected override void OnInit(EventArgs e)
    {
        var page = this.Page as INotifyOnSave;
        if (page != null)
            page.BeforeSave += btnSaveConfiguration_Click;
    }

    /// <summary>
    /// Raises the <see cref="E:System.Web.UI.Control.Load" /> event.
    /// </summary>
    /// <param name="e">The <see cref="T:System.EventArgs" /> object that contains the event data.</param>
    protected override void OnLoad(EventArgs e)
    {
        reportTableTimeRelativeConfiguration.DataSource = DataSource;
        presenters.DataSource = DataSource;
 
        Localize();
        UpdateSectionCellBasedTimeFrameSupport();
    }
     
    protected void ValidateTableConfiguration_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!ValidateTableConfiguration(ViewModel, DataSource))
        {
            args.IsValid = false;
        }
    }

    private bool ValidateStatisticalEntityHasTimeStamp()
    {
        bool isValid = true;
        if (DataSource.Type == DataSourceType.Dynamic || DataSource.Type == DataSourceType.Entities)
        {
            bool entityIsStatistic = DataSourceFactory.GetProvider(DataSource).GetEntity(DataSource.MasterEntity).BaseTypes.Contains(SwisEntityBaseType.StatisticsEntity);
            if (entityIsStatistic && ViewModel.TimeField == null)
            {
                isValid = false;
            }
        }
        return isValid;
    }

    protected void ValidateStatisticalEntityHasTimeStamp_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = ValidateStatisticalEntityHasTimeStamp();
    }

    private bool ValidateTableConfiguration(TableConfiguration config, DataSource dataSource)
    {
        dataSource.ApplyQueryContextIfPresent();

        columnsWithError.Visible = false;

        if (config.Columns == null)
        {
            return true;
        }
        
        var columnsWithErrorList = new List<string>();        
        IDataSourceFieldProvider fieldProvider = DataSourceFactory.GetFieldProvider(dataSource, new InformationServiceProxyCreator(() => SolarWinds.Orion.Web.InformationService.InformationServiceProxy.CreateV3()));

        var repo = ReportingRepository.GetInstance<IDataPresenterRepository>();
        foreach (TableColumn column in config.Columns)
        {
            Field field;
            if (!fieldProvider.TryGetField(column.Field.RefID, out field))
            {
                columnsWithErrorList.Add(column.DisplayName.ToUpper());
            }
            else if (dataSource.Type == DataSourceType.CustomSQL || dataSource.Type == DataSourceType.CustomSWQL)
            {
                if (column.Presenters == null) continue;
                foreach (var presenter in column.Presenters)
                {
                    var p = repo.GetRef(presenter.PresenterId);
                    var instance = p.Factory.NewInstance(DataSource, column.Field, null);
                    if (instance == null || instance.RequiredProperties == null)
						continue;

                    var missedProperties = new List<string>();
                    foreach (var requiredProperty in instance.RequiredProperties)
                    {
                        if (!fieldProvider.TryGetField(requiredProperty, out field))
                        {
                            missedProperties.Add(requiredProperty);
                        }
                    }

                    if (missedProperties.Count > 0)
                    {
                        columnsWithErrorList.Add(String.Format(Resources.CoreWebContent.WEBCODE_IB0_10,
                                                                column.DisplayName.ToUpper(),
                                                                TokenSubstitution.Parse(p.DisplayNameToken),
                                                                string.Join(",", missedProperties.ToArray())));
                    }
                }
            }
        }
        
        if (columnsWithErrorList.Count > 0)
        {
            columnsWithError.Visible = true;
            columnListRepeater.DataSource = columnsWithErrorList;
            columnListRepeater.DataBind();
            return false;
        }
        return true;
    }

    protected void ValidateSumarizeMode_ServerValidate(object source, ServerValidateEventArgs args)
    {
        bool valid = true;

        if (ViewModel.SummarizeMode != DataSummarizeMode.NoDataSummarization && ViewModel.Columns != null && ViewModel.Columns.Length > 0 )
        {
            bool hasAggregationSet = false;

            foreach (var col in ViewModel.Columns)
            {
                if (col.Summary == null || col.Summary.Calculation != Aggregate.NotSpecified )
                {
                    hasAggregationSet = true;
                    break;
                }
            }

            valid = hasAggregationSet;
        }

        args.IsValid = valid;
    }

    private void SetTimeField(TableConfiguration tableConfiguration)
    {
        //Auto load if columns just added.
        string eventTarget = this.Request["__EVENTTARGET"];
        if (eventTarget == "ColumnsAdded")
        {
            //We already have a timeField
            if (tableConfiguration.TimeField != null && tableConfiguration.TimeField.RefID != null)
                return;

            var timeFields = new List<FieldRef>();            
            IDataSourceFieldProvider fieldProvider = DataSourceFactory.GetFieldProvider(DataSource, new InformationServiceProxyCreator(() => SolarWinds.Orion.Web.InformationService.InformationServiceProxy.CreateV3()));
            // get time fileds for all columns
            foreach (var column in tableConfiguration.Columns)
            {
                Field timeField;
                if (fieldProvider.TryGetTimeField(column.Field.RefID, out timeField))
                {
                    timeFields.Add(timeField.RefID);
                }
            }            

            // find time field in selected columns
            foreach (FieldRef timeFieldRef in timeFields)
            {
                if (tableConfiguration.Columns.Count(c => c.Field.RefID.Equals(timeFieldRef)) > 0)
                {
                    tableConfiguration.TimeField = new Field();
                    tableConfiguration.TimeField.RefID = timeFieldRef;
                    break;
                }
            }
        }
    }

    private void Localize()
    {
        ValidateSumarizeMode.ErrorMessage = string.Format("<b>{0}</b><br/>{1} <b>{2}</b> {3}",
                                                          Resources.CoreWebContent.WEDDATA_ZT0_32,
                                                          Resources.CoreWebContent.WEDDATA_ZT0_33,
                                                          Resources.CoreWebContent.WEDDATA_ZT0_29,
                                                          Resources.CoreWebContent.WEDDATA_ZT0_34);

        ValidatorStatisticalEntityHasTimeStamp.ErrorMessage = string.Format(Resources.CoreWebContent.WEBDATA_OJ0_10,
                                                                            DataSource.MasterEntity);            
    }

    private void UpdateSectionCellBasedTimeFrameSupport()
    {
        var hideTimeFrameSelection = HideTimeFrameSelection();
        var cell = ReportDataProvider.GetCell();

        if (cell != null && hideTimeFrameSelection)
            cell.TimeframeRefId = WebConstants.NotSupportedGuid;
        else if (cell != null && !hideTimeFrameSelection && cell.TimeframeRefId == WebConstants.NotSupportedGuid)
        {
            var report = ReportDataProvider.GetReport();
            var firstTimeFrame = report.TimeFrames.FirstOrDefault();
                cell.TimeframeRefId = firstTimeFrame == null ? Guid.Empty : firstTimeFrame.RefId;
        }
    }

    private Section[] GetSectionsFromSession()
    {
        var sessionId = Request["SectionsSessionId"];
        if (Session[sessionId] != null)
        {
            var serializer = new DataContractJsonSerializer(typeof(Section[]));
            return (Section[])serializer.ReadObject(new MemoryStream(Encoding.UTF8.GetBytes((string)Session[sessionId])));
        }
        return null;
    }
    private void UpdateSections(Section[] sections)
    {
        var serializer = new DataContractJsonSerializer(typeof(Section[]));
        using (var ms = new MemoryStream())
        {
            serializer.WriteObject(ms, sections);
            Session[Request["SectionsSessionId"]] = Encoding.UTF8.GetString(ms.ToArray());
        }
    }
    private void PersistSectionCellBasedTimeFrameSupport()
    {
        var sections = GetSectionsFromSession();
        var sectionCellId = new Guid(Request["SectionCellRefId"]);

        var hideTimeFrameSelection = HideTimeFrameSelection();
        var cell = (sections ?? Enumerable.Empty<Section>())
                            .SelectMany(x => x.Columns ?? Enumerable.Empty<SectionColumn>())
                            .SelectMany(x => x.Cells ?? Enumerable.Empty<SectionCell>())
                            .FirstOrDefault(x => x.RefId == sectionCellId);

        if (cell != null && hideTimeFrameSelection)
            cell.TimeframeRefId = WebConstants.NotSupportedGuid;
        else if (cell != null && !hideTimeFrameSelection && cell.TimeframeRefId == WebConstants.NotSupportedGuid)
            cell.TimeframeRefId = Guid.Empty;
        UpdateSections(sections);
    }
    void btnSaveConfiguration_Click(object sender, EventArgs e)
    {
        PersistSectionCellBasedTimeFrameSupport();
    }

    private bool HideTimeFrameSelection()
    {
        if (DataSource == null)
            return false;

        return (DataSource.SupportsAdvancedFiltering() && ViewModel.TimeField == null) ||
               (!DataSource.SupportsAdvancedFiltering() && !(new DataTableMacroProcessor()).ContainsAnyDateTimeMacro(DataSource.CommandText));
    }

	public Int32 ColumnsCount
	{
		get
		{
			return ViewModel.Columns == null ? 0
				: ViewModel.Columns.Count();
		}
	}
   
}
