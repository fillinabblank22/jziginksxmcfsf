﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReportTableSortConfiguration.ascx.cs" Inherits="Orion_Reports_Controls_ReportTableSortConfiguration" %>
<orion:Include ID="Include1" runat="server" File="Reports/js/ReportTableSortConfiguration.js" />
<orion:Include ID="Include2" runat="server" File="ReportTableSortConfiguration.css" />

<% if (!ChartMode)
   { %>
<div class="ReportTableConfiguration-SectionHeader">
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_OJ0_4) %><%--Sort results by:--%>
</div>
<% } %>

<div id="<%= DefaultSanitizer.SanitizeHtml(GetSaveUniqueID(UniqueID)) %>" class="columnItem">
    <div class="addColumn" style="float: left; display: <%= this.ChartMode ? "none" : "inline" %>; padding: 2px 5px 3px 3px;">
        <select class="columnList"></select>
   </div>
    <div class="addSerie" style="float: left; display: <%= !this.ChartMode ? "none" : "inline" %>; padding: 4px 5px 3px 3px;">
        <a style="color: #3d6e9e; text-decoration: underline;" class="selectField" href="#"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_49) %></a>
    </div>
</div>
<asp:HiddenField runat="server" ID="columnSortByOrderConfig"/>
<div style="clear: both; float: none; width: 1px;"></div>

<script id="columnSortByOrderTemplate" type="text/x-template">
    <div class="portletItem">
        <a href="#">
            <div class="portlet-header">
                <table>
                    <tbody>
                        <tr>
                            <td style="width: 15px;">
                                <img src="/Orion/images/Reports/ddpick.png" style="margin-top: 4px" />
                            </td>
                            <td>{{ColumnTitle}}</td>
                            <td style="width: 15px; padding-left:10px;">
                                <!-- TODO: here will be sort order control -->
                                <img class="sortAscending" data-name="sortButton" src="/Orion/images/Reports/Arrow_Ascending.png" />
                            </td>
                            <td style="width: 15px; padding-right:10px;">
                            <span class="helpfulText" style="font-weight: normal; color: #646464; cursor:pointer;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TP0_49) %>
                            </span>
                            </td>
                            <td style="width: 15px">
                                <img class="resource-manage-btn" data-name="deleteButton" src="/Orion/images/Reports/delete_icon16x16.png"/>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </a>
    </div>

</script>

<script type="text/javascript">
    $(function () {
        var columnOrder = new SW.Core.TableResource.ColumnOrder({
            parentElId: "<%= GetSaveUniqueID(this.UniqueID) %>",
            columnOrderConfig: "<%= this.columnSortByOrderConfig.ClientID %>",
            isGroupByConfiguration: false,
            chartMode: "<%= this.ChartMode %>"
        });

        columnOrder.SetColumnTemplate($("#columnSortByOrderTemplate").html());
        
        columnOrder.CreateDraggableColumns();
        columnOrder.FillColumnList();
        var sortColumnCreatedCallback = <%= this.OnCreatedJS %>;
        if (sortColumnCreatedCallback && jQuery.isFunction(sortColumnCreatedCallback)) {
            sortColumnCreatedCallback(columnOrder);
        }
    });
</script>