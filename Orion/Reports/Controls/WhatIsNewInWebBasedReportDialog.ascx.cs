﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Reports_Controls_WhatIsNewInWebBasedReportDialog : System.Web.UI.UserControl
{
    public string getingStartedWithWebReports
    {
        get
        {
            return String.Format(Resources.CoreWebContent.WEBCODE_RV0_1,
                string.Format("<a style='color: #336699;' href='{0}embedded_in_products/productLink.aspx?id=sw_core_NewReportingVideo' target='_blank'>{1}</a>", Resources.CoreWebContent.SolarWindsComLocUrl, Resources.CoreWebContent.WEBCODE_PS0_10),
                string.Format("<a style='color: #336699;' href='{0}' target='_blank'>{1}</a>", KnowledgebaseHelper.GetMindTouchKBUrl(46407), Resources.CoreWebContent.WEBCODE_PS0_11));
        }
    }

    /// <summary>
    /// Returns TRUE if was dialog explicitly hidden by user.
    /// </summary>
    public bool WasDialogHidden
    {
        get
        {
            return bool.Parse(WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, "DontShowWhatIsNewInReportDialog", "false"));
        }
    }

    protected static HashSet<String> _hiddenReportWriterHelpLink = new HashSet<string>(OrionModuleManager.GetCustomizations().Where(c => c.Action.Equals(Customization.ManageReportsHideSection)).Select(c => c.Key));

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}