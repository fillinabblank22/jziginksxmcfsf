﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Reporting.Models.Selection;

public partial class Orion_Reports_Controls_ReportTableTimeFieldColumnSelection : ModelBasedControl<SolarWinds.Reporting.Models.Selection.Field> 
{
    private readonly string DummyJsAction = "function(){}";

    public DataSource DataSource { get; set; }

    /// <summary>
    /// (get) True in the case of SQLDataSource; otherwise false;
    /// </summary>
    public bool IsCustomSQLDataSource
    {
        get
        {
            if (DataSource != null)
            {
                return DataSource.Type == DataSourceType.CustomSQL;
            }

            return false;
        }
    }

    public bool ValidationEnabled { get; set; }

    public string OnCreatedJS { get; set; }

    public Orion_Reports_Controls_ReportTableTimeFieldColumnSelection()
    {
        ValidationEnabled = false;
        OnCreatedJS = DummyJsAction;
    }

    /// <summary>
    /// Loads the view model.
    /// </summary>
    public override void LoadViewModel()
    {
        base.LoadViewModel();
        this.ViewModel = string.IsNullOrEmpty( timeFieldSelected.Value) ||timeFieldSelected.Value == "selectField"  ? 
                                                                    null : 
                                                                    new SolarWinds.Reporting.Models.Selection.Field() { RefID = timeFieldSelected.Value };
    }
       
    /// <summary>
    /// Gets the save unique ID. This is present because jquery is confused by $ letter in selector.
    /// </summary>
    /// <returns>UniqueID property where letters '$' are replaced by '-' letters.</returns>
    protected string GetSaveUniqueID(string uniqueId)
    {
        string strRes = string.Empty;
        if (!string.IsNullOrEmpty(uniqueId))
        {
            strRes = uniqueId.Replace("$", "-");
        }

        return strRes;
    }

    /// <summary>
    /// Binds the view model.
    /// </summary>
    protected override void BindViewModel()
    {
        base.BindViewModel();

        timeFieldSelected.Value = ViewModel == null ? null:
                                                      ViewModel.RefID;
    }

    protected void TimeFieldValidator_Validate(object sender, ServerValidateEventArgs e)
    {
        if (!ValidationEnabled)
        {
            e.IsValid = true;
            // todo : temporary disabled server validation to resolve case when we don't have any datetime filter
            return;
        }

        if (IsCustomSQLDataSource)
        {
            if ((ViewModel.RefID == null) || (ViewModel.RefID.Data == Convert.ToString(Guid.Empty)))
            {
                e.IsValid = false;
            }
        }
    }
}