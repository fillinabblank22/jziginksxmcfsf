﻿using System;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.UI;
using SolarWinds.Reporting.Models.Timing;

public partial class Orion_Reports_Controls_TimePeriodsControl : System.Web.UI.UserControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!IsPostBack)
        {
            timeFrameSessionIdentifier.Value = String.Format("TimeFrameState-{0}", WorkflowGuid);
            selectedTimeFrameSessionIdentifier.Value = String.Format("SelectedTimeFrame-{0}", WorkflowGuid);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string WorkflowGuid
    {
        get
        {
            if (ViewState["TimeFrameGuid"] == null)
                ViewState["TimeFrameGuid"] = Guid.NewGuid().ToString();
            return (string)ViewState["TimeFrameGuid"];
        }
        set
        {
            ViewState["TimeFrameGuid"] = value;
            timeFrameSessionIdentifier.Value = String.Format("TimeFrameState-{0}", value);
            selectedTimeFrameSessionIdentifier.Value = String.Format("SelectedTimeFrame-{0}", value);
        }
    }

    public bool Visible { get; set; }

    public TimeFrame TimeFrame
    {
        get
        {
            if (Session[selectedTimeFrameSessionIdentifier.Value] != null)
            {
                var serializer = new DataContractJsonSerializer(typeof(TimeFrame));
                return (TimeFrame)serializer.ReadObject(new MemoryStream(Encoding.UTF8.GetBytes((string)Session[selectedTimeFrameSessionIdentifier.Value])));
            }
            return null;
        }
        set
        {
            var serializer = new DataContractJsonSerializer(typeof(TimeFrame));
            using (var ms = new MemoryStream())
            {
                serializer.WriteObject(ms, value);
                Session[selectedTimeFrameSessionIdentifier.Value] = Encoding.UTF8.GetString(ms.ToArray());
            }

        }
    }

    public TimeFrame[] TimeFrames
    {
        get
        {
            if (Session[timeFrameSessionIdentifier.Value] != null)
            {
                var serializer = new DataContractJsonSerializer(typeof(TimeFrame[]));
                return (TimeFrame[])serializer.ReadObject(new MemoryStream(Encoding.UTF8.GetBytes((string)Session[timeFrameSessionIdentifier.Value])));
            }
            return null;
        }
        set
        {
            var serializer = new DataContractJsonSerializer(typeof(TimeFrame[]));
            using (var ms = new MemoryStream())
            {
                serializer.WriteObject(ms, value);
                Session[timeFrameSessionIdentifier.Value] = Encoding.UTF8.GetString(ms.ToArray());
            }

        }
    }
}