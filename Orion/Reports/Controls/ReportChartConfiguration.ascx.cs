﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Reporting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Extensions;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.Reporting;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Reporting;
using SolarWinds.Reporting.Models.Charts;
using SolarWinds.Reporting.Models.Layout;
using SolarWinds.Reporting.Models.Selection;
using SolarWinds.Internationalization.Extensions;
using SolarWinds.Orion.Core.Common.InformationService;

public partial class Orion_Reports_Controls_ReportChartConfiguration : ModelBasedControl<ChartConfiguration>
{
    private DataSource _dataSource;
    public DataSource DataSource {
        get { return _dataSource; }
        set
        {
            _dataSource = value;
            SetVisibility();
        }
    }

    protected string MultiObjectsControllerInstanceName
    {
        get { return string.Format("multipleObjectsController_{0}", ClientID); }
    }

    protected override void OnInit(EventArgs e)
    {
        var page = this.Page as INotifyOnSave;
        if (page != null)
            page.BeforeSave += btnSaveConfiguration_Click;
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        leftAxisEditor.AxisName = Resources.CoreWebContent.WEBDATA_TP0_18;
        leftAxisEditor.DetailsHiddenField = dataSeriesDetail.DetailsHiddenField;
        leftAxisEditor.BindDataJsFunction = dataSeriesDetail.OnBindDataJsFunction;
        leftAxisEditor.OnCreatedJS = string.Format("{0}.OnEditLeftAxisPropertiesControlCreated", MultiObjectsControllerInstanceName);
        leftAxisEditor.DataDetails = dataSeriesDetail.ClientID;

        rightAxisEditor.AxisName = Resources.CoreWebContent.WEBDATA_TP0_19;
        rightAxisEditor.AxisSubName = Resources.CoreWebContent.WEBDATA_TP0_34;
        rightAxisEditor.DetailsHiddenField = dataSeriesDetail.DetailsHiddenField;
        rightAxisEditor.BindDataJsFunction = dataSeriesDetail.OnBindDataJsFunction;
        rightAxisEditor.OnCreatedJS = string.Format("{0}.OnEditRightAxisPropertiesControlCreated", MultiObjectsControllerInstanceName);
        rightAxisEditor.DataDetails = dataSeriesDetail.ClientID;
        
        dataSeriesDetail.ShowDetailsByDefault = false;
        secondarySortBy.OnCreatedJS = string.Format("{0}.OnSecondarySortControlCreated",
                                                    MultiObjectsControllerInstanceName);
        secondarySortFieldPicker.OnCreatedJs = string.Format("{0}.OnSecondarySortFieldPickerControlCreated", MultiObjectsControllerInstanceName);
        secondarySortFieldPicker.OnSelectedJs = string.Format("{0}.OnSecondarySortFieldPickerControlSelected",
                                                              MultiObjectsControllerInstanceName);
        secondarySortFieldPicker.DataSource = DataSource;
        
        SetVisibility();
    }

    private void SetVisibility()
    {
        InitMultipleSelection();

        SetVisibilityChartFilterControls(DataSource.Type == DataSourceType.Dynamic || DataSource.Type == DataSourceType.Entities);
    }

    public override void LoadViewModel()
    {
        base.LoadViewModel();

        int sampleSize = 0;
        Int32.TryParse(SampleSize.SelectedValue, out sampleSize);

        Limit limit = new Limit();
        Ordering ordering;
        Enum.TryParse(ddlSortOrder.SelectedValue, out ordering);
        Aggregate aggregation;
        Enum.TryParse(ddlDataAggregation.SelectedValue, out aggregation);
        int limitSize = 0;
        if (rbFilter1.Checked)
        {
            Int32.TryParse(tbTopXX.Text, out limitSize);
            limit.Count = limitSize;
            limit.Mode = LimitMode.TopEntries;
        }
        else if (rbFilter2.Checked)
        {
            Int32.TryParse(tbTopPercent.Text, out limitSize);
            limit.Percentage = limitSize;
            limit.Mode = LimitMode.TopPercent;
        }
        else
        {
            limit.Mode = LimitMode.ShowAll;
        }

        secondarySortBy.DataSource = DataSource;
        if (secondarySortBy.ViewModel == null)
        {
            secondarySortBy.LoadViewModel();
        }

        ViewModel = new ChartConfiguration()
        {
            LeftAxis = leftAxisEditor.ViewModel,
            RightAxis = rightAxisEditor.ViewModel,
            SampleIntervalInMinutes = sampleSize,
            KeyField = jsonKeyField.GetObject<Field>(),
            CaptionField = jsonCaptionField.GetObject<Field>(),
            Limit = limit,
            Order = ordering,
            SortRecordsBy = hdnSortBy.GetObject<Field>(),
            DataAggregation = aggregation,
            SecondarySortRecordsBy = secondarySortBy.ChartSortBy.ToArray(),
            CustomDataGroupingEnabled = chbCustomDataGroupingEnabled.Checked
        };

        if (IsFederationEnabled)
        {
            ViewModel.Limit.OrionServerIDsToIgnore =
                ViewModel.OrionServerIDsToIgnore = ServerFilter.GetCommaSeparatedServerIDsToSkip();
        }
    }

    protected override void BindViewModel()
    {
        base.BindViewModel();

        if (IsFederationEnabled)
        {
            ServerFilter.Initialize(ViewModel.OrionServerIDsToIgnore);
        }
        
        FillSortOrderDropDownList();
        FillAggregationDropDownList();

        ddlDataAggregation.SelectedIndex = (int)ViewModel.DataAggregation;
        leftAxisEditor.DataSource = DataSource;
        leftAxisEditor.ViewModel = ViewModel.LeftAxis;
        rightAxisEditor.DataSource = DataSource;
        rightAxisEditor.ViewModel = ViewModel.RightAxis;
        dataSeriesDetail.DataSource = DataSource;
        SampleSize.SelectedValue = ViewModel.SampleIntervalInMinutes.ToString();
        if (ViewModel.SecondarySortRecordsBy != null)
        {
            secondarySortBy.ChartSortBy.Clear();
            secondarySortBy.ChartSortBy.AddRange(ViewModel.SecondarySortRecordsBy);
        }

        if (ViewModel.Limit != null)
        {
            switch (ViewModel.Limit.Mode)
            {
                case LimitMode.TopEntries:
                    rbFilter1.Checked = true;
                    tbTopXX.Text = ViewModel.Limit.Count.ToString();
                    break;
                case LimitMode.TopPercent:
                    rbFilter2.Checked = true;
                    tbTopPercent.Text = ViewModel.Limit.Percentage.ToString();
                    break;
                case LimitMode.ShowAll:
                default:
                    tbTopXX.Text = "5";
                    tbTopPercent.Text = "5";
                    rbShowAll.Checked = true;
                    break;
            }
        }

       if (ViewModel.Order == Ordering.Descending)
       {
           ddlSortOrder.SelectedIndex = 0;
       }
       else if (ViewModel.Order == Ordering.Ascending)
       {
           ddlSortOrder.SelectedIndex = 1;
       }

       chbCustomDataGroupingEnabled.Checked = ViewModel.CustomDataGroupingEnabled;
        
        lblKeyFieldDisplayName.Text = ViewModel.KeyField != null ? ViewModel.KeyField.DisplayName : string.Empty;
        jsonKeyField.SetObject(ViewModel.KeyField);

        jsonCaptionField.SetObject(ViewModel.CaptionField);
        lblCaptionFieldDisplayName.Text = ViewModel.CaptionField != null ? ViewModel.CaptionField.DisplayName : string.Empty;

        hdnSortBy.SetObject(ViewModel.SortRecordsBy);
        ValidateChartConfiguration(ViewModel, DataSource);
    }


    protected void ValidateNumberOfPoints(object source, ServerValidateEventArgs args)
    {
        args.IsValid = IsValidNumberOfPoints();
    }

    protected void ValidateChartConfiguration_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!ValidateChartConfiguration(ViewModel, DataSource))
            args.IsValid = false;
    }

    protected void TopXXIsNumericAndGreaterOrEqualZero_ServerValidate(object source, ServerValidateEventArgs args)
    {
        int topXX;
        if (int.TryParse(tbTopXX.Text, out topXX))
        {
            if (topXX < 0)
            {
                args.IsValid = false;
                valTopXXIsNumericAndGreaterOrEqualZero.ErrorMessage = Resources.CoreWebContent.WEBDATA_PS0_48;
            }
        }
        else
        {
            args.IsValid = false;
            valTopXXIsNumericAndGreaterOrEqualZero.ErrorMessage = Resources.CoreWebContent.WEBDATA_YK0_36;
        }
    }

    protected void TopPercentIsNumericAndGreaterOrEqualZero_ServerValidate(object source, ServerValidateEventArgs args)
    {
        int topPercent;
        if (int.TryParse(tbTopPercent.Text, out topPercent))
        {
            if (topPercent < 0)
            {
                args.IsValid = false;
                valTopPercentIsNumericAndGreaterOrEqualZero.ErrorMessage = Resources.CoreWebContent.WEBDATA_YK0_36;
            }
        }
        else
        {
            args.IsValid = false;
            valTopPercentIsNumericAndGreaterOrEqualZero.ErrorMessage = Resources.CoreWebContent.WEBDATA_PS0_48;
        }
    }

    private void SetVisibilityChartFilterControls(bool visible)
    {
        lblFilterResults.Visible = visible;
        lblFilterDescription.Visible = visible;
        rbShowAll.Visible = visible;
        lblShowAll.Visible = visible;
        rbFilter1.Visible = visible;
        lblShowOnlyTop.Visible = visible;
        tbTopXX.Visible = visible;
        sortingFrame1.Visible = visible;
        sortingFrame2.Visible = visible;
        rbFilter2.Visible = visible;
        lblTopPercent.Visible = visible;
        tbTopPercent.Visible = visible;
    }

    private void FillAggregationDropDownList()
    {
        var aggregations =
                Enum.GetValues(typeof(Aggregate))
                    .Cast<Aggregate>()
                    .Select(item => new { Value = (int)item, Text = item.LocalizedLabel() });
        ddlDataAggregation.DataSource = aggregations;
        ddlDataAggregation.DataTextField = "Text";
        ddlDataAggregation.DataValueField = "Value";
        ddlDataAggregation.DataBind();
    }

    private void FillSortOrderDropDownList()
    {
        var sortOrder = Enum.GetValues(typeof (Ordering))
                            .Cast<Ordering>()
                            .Where(item => item != Ordering.NotSpecified)
                            .Select(item => new {Value = item, Text = item.LocalizedLabel()}).OrderByDescending(item => item.Value );
        ddlSortOrder.DataSource = sortOrder;
        ddlSortOrder.DataTextField = "Text";
        ddlSortOrder.DataValueField = "Value";
        ddlSortOrder.DataBind();
    }

    private bool ValidateChartConfiguration(ChartConfiguration config, DataSource dataSource)
    {
        dataSource.ApplyQueryContextIfPresent();

        var columnsWithErrorList = new List<Tuple<string, string>>();
        var fieldsFromConfig = config.GetFields().ToList();
        if (DataSource.Type != DataSourceType.CustomSQL && 
            DataSource.Type != DataSourceType.CustomSWQL &&
            !config.CustomDataGroupingEnabled)
        {
            config.CaptionField = null;
            config.KeyField = null;
        }
        else
        {
            if (config.CaptionField == null)
                columnsWithErrorList.Add(
                    Tuple.Create(Resources.CoreWebContent.WEBDATA_ReportChartConfiguration_CustomDataGrouping_SectionTitle,
                                 String.Format(Resources.CoreWebContent.WEBDATA_ReportChartConfiguration_CustomDataGrouping_MissingField,
                                 Resources.CoreWebContent.WEBDATA_ReportChartConfiguration_CustomDataGrouping_KeyField)));

            if (config.KeyField == null)
                columnsWithErrorList.Add(
                    Tuple.Create(Resources.CoreWebContent.WEBDATA_ReportChartConfiguration_CustomDataGrouping_SectionTitle,
                                 String.Format(Resources.CoreWebContent.WEBDATA_ReportChartConfiguration_CustomDataGrouping_MissingField,
                                 Resources.CoreWebContent.WEBDATA_ReportChartConfiguration_CustomDataGrouping_CaptionField)));
        }

        if (config.CaptionField != null)
        {
            fieldsFromConfig.Add(config.CaptionField);
        }
        if (config.KeyField != null)
        {
            fieldsFromConfig.Add(config.KeyField);
        }

        IDataSourceFieldProvider fieldProvider = DataSourceFactory.GetFieldProvider(dataSource, new InformationServiceProxyCreator( () => SolarWinds.Orion.Web.InformationService.InformationServiceProxy.CreateV3()));

            foreach (Field field in fieldsFromConfig)
            {
                Field fieldParam;
                if (!fieldProvider.TryGetField(field.RefID, out fieldParam))
                {
                    columnsWithErrorList.Add(Tuple.Create(field.DisplayName.ToUpper(), string.Empty));
                }
            }
        
        foreach (var series in config.GetDataSeriesConfigurations())
        {
            //Validate TimeField is selected foreach dataseries.
            if (series.Field != null && series.TimeField == null)
                columnsWithErrorList.Add(Tuple.Create(series.Field.DisplayName.ToUpper(), Resources.CoreWebContent.WEBDATA_OJ0_7));
        }
        if (columnsWithErrorList.Count > 0)
        {
            columnsWithError.Visible = true;
            columnListRepeater.DataSource = columnsWithErrorList;
            columnListRepeater.DataBind();
            return false;
        }
        else
        {
            columnsWithError.Visible = false;
            return true;
        }
    }

    private bool IsValidNumberOfPoints()
    {
        //return CheckTimePeriodValue(ChartTimeSpan.SelectedValue, SampleSize.SelectedValue);
        return CheckTimePeriodValue("0", SampleSize.SelectedValue);
    }

    private bool CheckTimePeriodValue(string chartTimeSpanValue, string sampleSizeValue)
    {

        int sampleSizeInMinutes = Int32.Parse(sampleSizeValue, CultureInfo.InvariantCulture);
        int daysToLoad = Int32.Parse(chartTimeSpanValue, CultureInfo.InvariantCulture);

        const int minutesPerDay = 60 * 24;
        const int maxNumberOfPointsAllowed = 3000;

        int pointsToChart = (daysToLoad * minutesPerDay) / sampleSizeInMinutes;

        
        return (pointsToChart <= maxNumberOfPointsAllowed);
    }

    private void InitMultipleSelection()
    {
        trCustomDataGroupingSwitch.Visible = DataSource.Type == DataSourceType.Dynamic ||
                                             DataSource.Type == DataSourceType.Entities;

        trCustomDataGroupingTitle.Visible = DataSource.Type == DataSourceType.CustomSQL ||
                                            DataSource.Type == DataSourceType.CustomSWQL;

        fieldPickerMultipleObjects.DataSource = DataSource;
        fieldPickerMultipleObjects.OnCreatedJs = string.Format("{0}.OnFieldPickerCreated", MultiObjectsControllerInstanceName);
        fieldPickerMultipleObjects.OnSelectedJs = string.Format("{0}.OnFieldSelected", MultiObjectsControllerInstanceName);
    }

    private void btnSaveConfiguration_Click(object sender, EventArgs e)
    {
        PersistSectionCellBasedTimeFrameSupport();
    }

    private void PersistSectionCellBasedTimeFrameSupport()
    {
        var sections = GetSectionsFromSession();
        var sectionCellId = new Guid(Request["SectionCellRefId"]);

        var hideTimeFrameSelection = HideTimeFrameSelection();
        var cell = (sections ?? Enumerable.Empty<Section>())
                            .SelectMany(x => x.Columns ?? Enumerable.Empty<SectionColumn>())
                            .SelectMany(x => x.Cells ?? Enumerable.Empty<SectionCell>())
                            .FirstOrDefault(x => x.RefId == sectionCellId);

        if (cell != null && hideTimeFrameSelection)
            cell.TimeframeRefId = WebConstants.NotSupportedGuid;
        else if (cell != null && !hideTimeFrameSelection && cell.TimeframeRefId == WebConstants.NotSupportedGuid)
            cell.TimeframeRefId = Guid.Empty;
        UpdateSections(sections);
    }

    private Section[] GetSectionsFromSession()
    {
        var sessionId = Request["SectionsSessionId"];
        if (Session[sessionId] != null)
        {
            var serializer = new DataContractJsonSerializer(typeof(Section[]));
            return (Section[])serializer.ReadObject(new MemoryStream(Encoding.UTF8.GetBytes((string)Session[sessionId])));
        }
        return null;
    }

    private void UpdateSections(Section[] sections)
    {
        var serializer = new DataContractJsonSerializer(typeof(Section[]));
        using (var ms = new MemoryStream())
        {
            serializer.WriteObject(ms, sections);
            Session[Request["SectionsSessionId"]] = Encoding.UTF8.GetString(ms.ToArray());
        }
    }

    private bool HideTimeFrameSelection()
    {
        if (DataSource == null)
            return false;

        return (!DataSource.SupportsAdvancedFiltering() && !(new DataTableMacroProcessor()).ContainsAnyDateTimeMacro(DataSource.CommandText));
    }
}
