﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Reporting.Models.Charts;
using SolarWinds.Reporting.Models.Selection;

public partial class Orion_Reports_Controls_EditDetailDataSeriesControl : ModelBasedControl<DataSeriesConfiguration>
{
  

    public bool ShowDetailsByDefault { get; set; }
    public string DetailsHiddenField { get; private set; }
    public string OnBindDataJsFunction { get; private set; }

    public DataSource DataSource { get; set; }

    protected override void OnInit(EventArgs e)
    {

        ddSelectColorMode.Items.Add(
            new ListItem(Resources.CoreWebContent.WEBDATA_TP0_36, ((int)(ColorMode.Standard)).ToString())
            );

        ddSelectColorMode.Items.Add(
            new ListItem(Resources.CoreWebContent.WEBDATA_YK0_46, ((int)(ColorMode.Custom)).ToString())
            );

        fieldPickerDataSeries.OnCreatedJs = "controler" + this.ClientID + ".onFieldPickerDataSerieCreated";
        fieldPickerDataSeries.OnSelectedJs = "controler" + this.ClientID + ".onDataSeriesFieldSelected";
        

        fieldPickerTimeSeries.OnCreatedJs = "controler" + this.ClientID + ".onFieldPickerTimeSeriesCreated";
        fieldPickerTimeSeries.OnSelectedJs = "controler" + this.ClientID + ".onTimeSeriesFieldSelected";        

        OnBindDataJsFunction = "controler" + this.ClientID + ".bindData";

        DetailsHiddenField = hfSeriesDetails.ClientID;

    }

    public override void LoadViewModel()
    { // data loaded by push request from JS
    }

    protected override void BindViewModel()
    { // binded via JSON
        fieldPickerDataSeries.DataSource = DataSource;
        fieldPickerTimeSeries.DataSource = DataSource;

        SetLongestNavigationPathForFieldPickers();

        if (DataSource != null && !(DataSource.Type == DataSourceType.CustomSQL || DataSource.Type == DataSourceType.CustomSWQL))
        {
            btnTimeSeries.Visible = false;
        }
        else
        {
            btnTimeSeries.Visible = true;
        }
    }

    private void SetLongestNavigationPathForFieldPickers()
    {
        var fieldManager = new FieldPickerManager();

        var allFields = fieldManager.GetFilterFields(DataSource).Select(f => f.RefID.Data).ToArray();

        fieldPickerDataSeries.NavigationPathFilterFields = allFields;
        fieldPickerTimeSeries.NavigationPathFilterFields = allFields;
    }

    protected string GetShowDetailDisplayCssValue()
    {
        if (ShowDetailsByDefault)
        {
            return "block";
        }
        else
        {
            return "none";
        }
    }
}