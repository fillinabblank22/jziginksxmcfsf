﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Reporting.Models.Tables;

public partial class Orion_Reports_Controls_ReportTableGroupByConfiguration : ModelBasedControl<TableIndentEntry[]>
{
    private readonly string DummyJsAction = "function(){}";

    /// <summary>
    /// (get, set) the on created JS. Definition of JavaScript callback function whill will be called when control will be created
    /// </summary>
    /// <value>
    /// The on created JS.
    /// </value>
    public string OnCreatedJS { get; set; }

    /// <summary>
    /// Initializes a new instance of the <see cref="Orion_Reports_Controls_ReportTableGroupByConfiguration"/> class.
    /// </summary>
    public Orion_Reports_Controls_ReportTableGroupByConfiguration()
    {
        OnCreatedJS = DummyJsAction;
    }

    /// <summary>
    /// Loads the view model.
    /// </summary>
    public override void LoadViewModel()
    {
        base.LoadViewModel();
        string configJSONString = columnGroupByOrderConfig.Value;
        if (!string.IsNullOrEmpty(configJSONString))
        {
            dynamic dGroupColumnsStates = null;
            try
            {
                dGroupColumnsStates =
                    new System.Web.Script.Serialization.JavaScriptSerializer().DeserializeObject(configJSONString);
            }
            catch (Exception)
            {
            }

            if (dGroupColumnsStates != null)
            {
                ViewModel = new TableIndentEntry[dGroupColumnsStates.Length];
                DataTable tblIndentEntry = new DataTable();
                tblIndentEntry.Columns.Add(new DataColumn("RefID", typeof(Guid)));
                tblIndentEntry.Columns.Add(new DataColumn("Order", typeof (int)));

                for (int i = 0; i < dGroupColumnsStates.Length; i++)
                {
                    DataRow newRow = tblIndentEntry.NewRow();
                    newRow["RefID"] = dGroupColumnsStates[i]["RefID"];
                    newRow["Order"] = dGroupColumnsStates[i]["Order"];
                    tblIndentEntry.Rows.Add(newRow);
                }

                DataRow[] rows = tblIndentEntry.Select(string.Empty, "Order Asc");
                for (int i = 0; i < rows.Length; i++)
                {
                    ViewModel[i] = new TableIndentEntry();
                    ViewModel[i].ColumnId = new Guid((rows[i]["RefID"] != DBNull.Value) ? Convert.ToString(rows[i]["RefID"]) : Convert.ToString(Guid.Empty));
                    // TODO: Here will be in future settings lot of other properties
                }
            }
        }
    }

    /// <summary>
    /// Gets the save unique ID. This is present because jquery is confused by $ letter in selector.
    /// </summary>
    /// <returns>UniqueID property where letters '$' are replaced by '-' letters.</returns>
    public string GetSaveUniqueID(string uniqueId)
    {
        string strRes = string.Empty;
        if (!string.IsNullOrEmpty(uniqueId))
        {
            strRes = uniqueId.Replace("$", "-");
        }

        return strRes;
    }

    /// <summary>
    /// Binds the view model.
    /// </summary>
    protected override void BindViewModel()
    {
        base.BindViewModel();

        if (ViewModel == null)
        {
            return;
        }

        Dictionary<string, string>[] configOptions = new Dictionary<string, string>[ViewModel.Length];
        for (int i = 0; i < ViewModel.Length; i++)
        {
            configOptions[i] = new Dictionary<string, string>();
            configOptions[i].Add("RefID", Convert.ToString(ViewModel[i].ColumnId));
            configOptions[i].Add("Order", Convert.ToString(i));
        }

        if (configOptions.Length > 0)
        {
            columnGroupByOrderConfig.Value = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(configOptions);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}