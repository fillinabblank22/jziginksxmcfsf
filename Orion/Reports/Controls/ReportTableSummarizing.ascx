<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReportTableSummarizing.ascx.cs" Inherits="Orion_Reports_Controls_ReportTableSummarizing" %>
<orion:Include runat="server" File="Reports/js/ReportTableSummarizing.js" />
<asp:Panel runat="server" ID="pnlSummarization">
<div class="ReportTableConfiguration-SectionHeader">
     <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_11) %><%--Sample Interval:--%>
</div>
<div style="margin-top: 5px;">        
    <asp:DropDownList ID="ddlSummarizeBy" runat="server"></asp:DropDownList><asp:literal runat="server" id="litInfoMessage" />
    <div runat="server" id="spanSumarizeHint" class="helpfulText" >
          <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_OJ0_11) %>
    </div>
</div>

<script>
    $(function () {
        var reportTableSummarizing = new SW.Core.TableResource.ReportTableSummarizing({
            ddlSummarizeBy: $('#<%= ddlSummarizeBy.ClientID %>')
        });
        reportTableSummarizing.init();
    });
</script>
</asp:Panel>