﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Reporting.Models.Data;

public partial class Orion_Reports_Controls_ReportConfigurationData : ModelBasedControl<ConfigurationData>
{
    protected string DefaultTitleText {
        get { return Resources.CoreWebContent.WEBDATA_OJ0_2; }
    }
    protected string DefaultSubTitleText
    {
        get { return Resources.CoreWebContent.WEBDATA_OJ0_3; }
    }

    public override void LoadViewModel()
    {
        base.LoadViewModel();
        ViewModel = new ConfigurationData()
                        {
                            DisplayTitle = tbTitle.Text.Trim(),
                            DisplaySubTitle = tbSubTitle.Text.Trim()
                        };
    }
    protected override void BindViewModel()
    {
        base.BindViewModel();
        tbTitle.Text = ViewModel.DisplayTitle;
        tbSubTitle.Text = ViewModel.DisplaySubTitle;
    }
}