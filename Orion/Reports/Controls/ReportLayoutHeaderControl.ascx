<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReportLayoutHeaderControl.ascx.cs"
    Inherits="Orion_Reports_Controls_ReportLayoutHeaderControl" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<orion:include runat="server" file="Reports/js/ReportLayoutHeader.js" />
<orion:include runat="server" file="LayoutBuilder.css" />
<orion:include runat="server" file="ajaxupload.css" />

<script type="text/javascript">
    function getCtnProcessDataID() { return '<%= btnProcessData.ClientID %>'; }
    function getReportLogoID() { return '<%= reportLogo.ClientID %>'; }
    function getCheckLogoID() { return '<%= checkLogo.ClientID %>'; }
    function getTitleID() { return '<%= headerTitleArea.ClientID %>'; }
    function getSubTitleID() { return '<%= headerSubTitleArea.ClientID %>'; }
    function getThrobberUploadID() { return '<%=throbberUpload.ClientID %>'; }
    function getFileUploadImage() { return '<%= fileUploadImage.ClientID %>'; }
    function getBtnBrowse() { return '<%= btnBrowse.ClientID %>'; }
    function getDefaultTitle() { return '<%= DefaultTitle %>'; }
    function getDefaultSubTitle() { return '<%= DefaultSubTitle %>'; }
</script>
<div id="reportLayoutHeader">
    <div id="layoutHeaderTop">
        <b>
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_62) %></b>
    </div>
    <table id="headerContent" cellpadding="0" cellspacing="0" style="width: 100%; padding: 10px 10px 5px 10px">
        <tr>
            <td>
                <table style="width: 100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 75%;">
                            <asp:TextBox runat="server" TextMode="MultiLine" ID="headerTitleArea" Class="headerTitleArea"></asp:TextBox>
                            <asp:TextBox runat="server" TextMode="MultiLine" Rows="1" ID="headerSubTitleArea" CssClass="headerSubTitleArea" style="margin-top: 5px;"></asp:TextBox>
                        </td>
                        <td>
                        <asp:UpdatePanel runat="server" ID="updPanel" UpdateMode="Conditional">
                           <ContentTemplate>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <div style="display: inline; vertical-align: middle; z-index: 999">
                                                        <asp:CheckBox runat="server" ID="checkLogo" Style="z-index: 999" />
                                                    </div>
                                                    <div style="display: inline; padding-left: 1px; z-index: 999">
                                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_63) %>
                                                    </div>
                                                </td>
                                                <td style="padding-top: 7px;">
                                                    <div id="browseWrap" class="x-form-field-wrap x-form-file-wrap" style="float: right;
                                                        overflow: hidden;">
                                                        <ajaxtoolkit:asyncfileupload id="fileUploadImage" runat="server" uploaderstyle="Traditional"
                                                            throbberid="throbberUpload" onclientuploadstarted="checkExtension" onclientuploadcomplete="uploadComplete"
                                                            onuploadedcomplete="AsyncFileUpload_UploadeComplete" cssclass="x-form-file" />
                                                        <orion:localizablebutton cssclass="x-form-file-btn" runat="server" id="btnBrowse"
                                                            text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_70 %>" onclientclick="return false;"
                                                            displaytype="Small" />
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="report-logo">
                                                <asp:Label runat="server" ID="lblBadImage" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_TM0_268 %>"
                                                    Visible="false" CssClass="upload-error" />
                                                <span id="fileFormatError" class="upload-error" style="display: none;">
                                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_269) %>
                                                </span>
                                                <asp:Label runat="server" ID="throbberUpload" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_267 %>"
                                                    Style="display: none;" />
                                                <asp:Button ID="btnProcessData" runat="server" Text="ProcessDataHiddenButton" OnClick="btnProcessData_Click"
                                                    Style="display: none;" />
                                                <asp:Image runat="server" ID="reportLogo" Style="max-height: 103px; max-width: 238px;"
                                                    ImageAlign="Middle" />
                                    </td>
                                </tr>
                            </table>
                           </ContentTemplate>
                        </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
