﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Reporting;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Reporting;
using SolarWinds.Reporting.Models.Charts;
using SolarWinds.Reporting.Models.Data;
using SolarWinds.Reporting.Models.Selection;
using Microsoft.CSharp.RuntimeBinder;
using SolarWinds.Orion.Core.Common.InformationService;

public partial class Orion_Reports_Controls_ReportTableSortConfiguration : ModelBasedControl<SortEntry[]> // System.Web.UI.UserControl
{
    private readonly string DummyJsAction = "function(){}";

    public string OnCreatedJS { get; set; }


    /// <summary>
    /// If true control will be displayed in mode for edit chart, otherwise for edit table report.
    /// </summary>
    public bool ChartMode { get; set; }

    public DataSource DataSource { get; set; }

    private IDataSourceFieldProvider _dataSourceFieldProvider = null;
    private IDataSourceFieldProvider DataSourceFieldProvider
    {
        get
        {
            if (_dataSourceFieldProvider == null)
            {
                _dataSourceFieldProvider = DataSourceFactory.GetFieldProvider(DataSource, new InformationServiceProxyCreator( () => SolarWinds.Orion.Web.InformationService.InformationServiceProxy.CreateV3()));
            }

            return _dataSourceFieldProvider;
        }
    }

    public List<ChartSortBy> ChartSortBy { get; set; }

    private Field ConvertFromFieldId(string fieldId)
    {
        Field field;
        if (!DataSourceFieldProvider.TryGetField(fieldId, out field))
        {
            // if field is not a part of data source then create a field based on information provided
            field = new Field
            {
                RefID = fieldId,
                DataTypeInfo = new DataDeclaration { DataType = new DataType() }
            };

            // log.DebugFormat("Requested field '{0}' was not found by field provider '{1}'.", fieldId, DataSourceFieldProvider);
        }
        return field;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Orion_Reports_Controls_ReportTableSortConfiguration"/> class.
    /// </summary>
    public Orion_Reports_Controls_ReportTableSortConfiguration()
    {
        OnCreatedJS = DummyJsAction;
        ChartMode = false;
        ChartSortBy = new List<ChartSortBy>();
    }

    /// <summary>
    /// Loads the view model.
    /// </summary>
    public override void LoadViewModel()
    {
        base.LoadViewModel();
        string configJSONString = columnSortByOrderConfig.Value;
        if (!string.IsNullOrEmpty(configJSONString))
        {
            dynamic dSortColumnsStates = null;
            try
            {
                dSortColumnsStates =
                    new System.Web.Script.Serialization.JavaScriptSerializer().DeserializeObject(configJSONString);
            }
            catch (Exception)
            {
            }

            if (dSortColumnsStates != null)
            {
                ViewModel = new SortEntry[dSortColumnsStates.Length];
                ChartSortBy.Clear();
                DataTable tblSortEntry = new DataTable();
                tblSortEntry.Columns.Add(new DataColumn("FieldID", typeof (string)));
                tblSortEntry.Columns.Add(new DataColumn("RefID", typeof (Guid)));
                tblSortEntry.Columns.Add(new DataColumn("Order", typeof (int)));
                tblSortEntry.Columns.Add(new DataColumn("SortType", typeof (string)));

                for (int i = 0; i < dSortColumnsStates.Length; i++)
                {
                    //ViewModel[i].QFieldRefID = dSortColumnsStates[i]["RefID"];
                    try
                    {
                        DataRow newRow = tblSortEntry.NewRow();
                        if (dSortColumnsStates[i] is Dictionary<string, object> && dSortColumnsStates[i].ContainsKey("FieldID"))
                            newRow["FieldID"] = dSortColumnsStates[i]["FieldID"];
                        else
                            newRow["FieldID"] = string.Empty;
                        newRow["RefID"] = dSortColumnsStates[i]["RefID"];
                        newRow["Order"] = dSortColumnsStates[i]["Order"];
                        newRow["SortType"] = dSortColumnsStates[i]["SortType"];
                        tblSortEntry.Rows.Add(newRow);
                    }
                    catch ( RuntimeBinderException)
                    {
                        //one of the above properties does not exist.
                        //likely a column was added as a timefield then deleted. No easy way to do null checking with dynamic obj.
                    }
                }

                DataRow[] rows = tblSortEntry.Select("", "Order Asc");
                for (int i = 0; i < rows.Length; i++)
                {
                    ViewModel[i] = new SortEntry();
                    ViewModel[i].QFieldRefID = new Guid((rows[i]["RefID"] != DBNull.Value) ? Convert.ToString(rows[i]["RefID"]) : Convert.ToString(Guid.Empty));
                    Ordering ordering = Ordering.Ascending;
                    Enum.TryParse(Convert.ToString(rows[i]["SortType"]), out ordering);
                    ViewModel[i].Direction = ordering;

                    if (ChartMode)
                    {
                        ChartSortBy chartSortBy = new ChartSortBy();
                        string fieldId = (rows[i]["FieldID"] != DBNull.Value)
                                             ? Convert.ToString(rows[i]["FieldID"])
                                             : string.Empty;
                        if (fieldId != string.Empty)
                        {
                            chartSortBy.Field = ConvertFromFieldId(fieldId);
                            chartSortBy.Order = ordering;
                            ChartSortBy.Add(chartSortBy);
                        }
                    }
                }
            }
        }
    }
    
    /// <summary>
    /// Gets the save unique ID. This is present because jquery is confused by $ letter in selector.
    /// </summary>
    /// <returns>UniqueID property where letters '$' are replaced by '-' letters.</returns>
    public string GetSaveUniqueID(string uniqueId)
    {
        string strRes = string.Empty;
        if (!string.IsNullOrEmpty(uniqueId))
        {
            strRes = uniqueId.Replace("$", "-");
        }

        return strRes;
    }

    /// <summary>
    /// Binds the view model.
    /// </summary>
    protected override void BindViewModel()
    {
        base.BindViewModel();
        
        Dictionary<string, string>[] configOptions = null;

        if (!ChartMode)
        {
            if (ViewModel == null)
            {
                return;
            }
            configOptions = new Dictionary<string, string>[ViewModel.Length];
            for (int i = 0; i < ViewModel.Length; i++)
            {
                configOptions[i] = new Dictionary<string, string>();
                configOptions[i].Add("RefID", Convert.ToString(ViewModel[i].QFieldRefID));
                configOptions[i].Add("Order", Convert.ToString(i));
                configOptions[i].Add("SortType", Convert.ToString(ViewModel[i].Direction));
            }
        }
        else
        {
            configOptions = new Dictionary<string, string>[ChartSortBy.Count];
            for (int i = 0; i < ChartSortBy.Count; i++)
            {
                configOptions[i] = new Dictionary<string, string>();
                configOptions[i].Add("RefID", Convert.ToString(Guid.NewGuid()));
                configOptions[i].Add("FieldID", Convert.ToString(ChartSortBy[i].Field.RefID));
                configOptions[i].Add("Caption", ChartSortBy[i].Field.DisplayName);
                configOptions[i].Add("Order", Convert.ToString(i));
                configOptions[i].Add("SortType", Convert.ToString(ChartSortBy[i].Order));
            }
        }

        if (configOptions.Length > 0)
        {
            columnSortByOrderConfig.Value = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(configOptions);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}