﻿using System;
using System.Collections.Generic;
using SolarWinds.Reporting.Models.Layout;

public partial class Orion_Reports_Controls_ReportPageConfigControl : System.Web.UI.UserControl
{
	public int Width
	{
		set
		{

            this.tbWebWidth.Text = value.ToString();

		}
		get
		{
            return string.IsNullOrEmpty(tbWebWidth.Text) ? 0 : Convert.ToInt32(tbWebWidth.Text);
		}
	}

	public PageLayout PageLayout
	{
		set
		{
			if (value != null)
			{
				Width = value.Width;
			}
		}
		get
		{
			return new PageLayout { PublishingType = "web", Width = Width };
		}
	}
}