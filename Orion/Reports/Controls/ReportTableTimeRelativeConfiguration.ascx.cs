﻿using SolarWinds.Orion.Core.Reporting;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Reporting;
using SolarWinds.Reporting.Models.Selection;
using SolarWinds.Reporting.Models.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public class TimeRelativeConfigurationView
{
    //Most other ModelBasedControl implementations work directly off of the reporting model.  Here we want to combine the TimeField/Summarization into a single control.
    public Field TimeField { get; set; }
    public Tuple<DataSummarizeMode> Summarization { get; set; }
    public bool TableIsSuitableForSumarizing { get; set; }
}

public partial class Orion_Reports_Controls_ReportTableTimeRelativeConfiguration : ModelBasedControl<TimeRelativeConfigurationView>
{
    public DataSource DataSource { get; set; }
    private ListItem[] items = new ListItem[] { new ListItem(Resources.CoreWebContent.WEBCODE_OJ0_11, "false"), //Does not depend
                                                new ListItem(Resources.CoreWebContent.WEBCODE_OJ0_12, "true") };//Does depend

    private bool IsTimeRelative
    {
        get
        {
            bool result;
            return bool.TryParse(hdnTimeRelative.Value, out result) && result;
        }
    }

    private void BindDdlTimeRelative()
    {
            ddlTimeRelative.DataSource = items;
            ddlTimeRelative.DataTextField = "Text";
            ddlTimeRelative.DataValueField = "Value";
            ddlTimeRelative.DataBind();
            bool isTimeFieldSelected = ViewModel.TimeField != null && ViewModel.TimeField.RefID != null;
            hdnTimeRelative.Value = isTimeFieldSelected.ToString().ToLower();
            ddlTimeRelative.SelectedValue = hdnTimeRelative.Value;

    }
    private void ShowHideUI()
    {
        //// disable summarizing for custom SQL and SWQL
        if (DataSource != null && (DataSource.Type == DataSourceType.CustomSQL || DataSource.Type == DataSourceType.CustomSWQL))
        {
            reportTableSummarizing.SummarizationHidden = true;
        }

        phTimeFieldSelection.Visible = ViewModel.TableIsSuitableForSumarizing;
        phNoTimeFieldsAvailable.Visible = !ViewModel.TableIsSuitableForSumarizing;
    }

    
    protected override void BindViewModel()
    {
        base.BindViewModel();

        BindDdlTimeRelative();
        reportTableTimeFieldSelection.ViewModel = ViewModel.TimeField;
        reportTableSummarizing.ViewModel = ViewModel.Summarization;

        ShowHideUI();
    }

    public override void LoadViewModel()
    {
        base.LoadViewModel();

        ViewModel = new TimeRelativeConfigurationView {
            TimeField = IsTimeRelative ? reportTableTimeFieldSelection.ViewModel:  null,
            Summarization = IsTimeRelative ? reportTableSummarizing.ViewModel : new Tuple<DataSummarizeMode>(DataSummarizeMode.NoDataSummarization)
        };
    }

    protected override void OnLoad(EventArgs e)
    {
        reportTableTimeFieldSelection.DataSource = DataSource;
    }
   
}