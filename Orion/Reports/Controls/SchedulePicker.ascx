<%@ Control Language="C#" ClassName="SchedulePicker" %>
<script runat="server">
    private string getHeaderTitleFn, onUpdateSelectedSchedules, onSchedulesSelected = "function(){}";
    
    [PersistenceMode(PersistenceMode.Attribute)]
    public string Title { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string GetHeaderTitleFn
    {
        get { return getHeaderTitleFn; }
        set { getHeaderTitleFn = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string OnUpdateSelectedSchedules
    {
        get { return onUpdateSelectedSchedules; }
        set { onUpdateSelectedSchedules = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string OnSchedulesSelected
    {
        get { return onSchedulesSelected; }
        set { onSchedulesSelected = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool RenderSelectedItems { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool BlockIfDemo { get; set; }
</script>
<orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="Reports/js/SchedulePicker.js" />
<orion:Include runat="server" File="Admin/js/SearchField.js" />
<orion:Include runat="server" File="ReportSchedule.css" />
<%if (RenderSelectedItems)
  {%>
<div id="selectedSchedulesList" width="610px"></div>
<% } %>
<%if (BlockIfDemo && SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer){%>
<div id="isShceduleReportDemoMode" style="display: none;"></div>
<%}%>
<div id="shcedulePickerBox" style="display: none;">
    <div id="headerTitle" style="margin: 10px; font-size: 16px;"></div>
    <div id="searchPanel" style="float: left; width: 100%;"></div>
    <div style="clear: both;"></div>
    <div id="sheduleReportPicker"></div>
    <label class="selectedSchedulesLabel"></label>
    <div id='selectedObjects' class='selectedObjectsCSS'><span style='color: gray;'><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_48) %></span></div>
</div>
<script id="schedulesTemplate" type="text/x-template">
    <table id ='selectedSchedules'>
        <tr class="tableHeader">
           <th class = 'titleColumn'><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_173) %></th>
           <th class = 'buttonColumn'><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_141) %></th>
           <th class = 'buttonColumn'><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_31) %></th> 
        </tr>       
        <tr class='empty'><td colspan='3'>&nbsp;</td></tr>
    {# _.each(schedules, function(current, index) { #}
        <tr class='selectedSchedule' data-index='{{index}}'>
           <td class='title'><span dataRow = '{{current.ID}}'>{{current.Name}}</span></td>
           <td class = 'buttonColumn'><img ext:qtitle="{{current.Name}}" ext:qtip="{{current.Information}}" alt="" src="/Orion/images/Show_Last_Note_icon16x16.png" style='cursor: pointer; margin:0 5px -3px 5px;'/></td>
           <td class='delete btn buttonColumn'><img alt="" src='/Orion/images/delete_icon_gray_16x16.png' /></td> 
        </tr>       
        <tr class='empty'><td colspan='3'>&nbsp;</td></tr>
     {# }); #}
    
        <tr class='addNextSchedule'> 
        <td colspan = '3'>
        <div style='width:100%;'>
        <span class='noScheduleInfo'>&nbsp;</span>
        <orion:LocalizableButton runat="server" ID="addSchedule" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_179 %>"
    DisplayType="Secondary" CausesValidation="false" OnClientClick="SW.Orion.SchedulePicker.selectScheduleDialog(); return false;" />
    <span class='rightScheduleInfo'></span>
    </div>
    </td>
        </tr>

    </table>
</script>
<script id="noSchedulesTemplate" type="text/x-template">
     <table id ='selectedSchedules'>
      <tr class='addNextSchedule' style='background-color: #ececec; border: none;'> 
      <td colspan = '3'><div style='width:100%;'>
      <span class='noScheduleInfo'><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_56) %></span>
      <orion:LocalizableButton runat="server" ID="addAnotherSchedule" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_179 %>"
    DisplayType="Primary" CausesValidation="false" OnClientClick="SW.Orion.SchedulePicker.selectScheduleDialog(); return false;" />
    <span class='rightScheduleInfo'>&nbsp;</span>
        </div>
        </td>         
            </tr>
       </table>
</script>
<script type="text/javascript">
    $(function () {
        var renderSchedules = function (schedulesInfo) {
            var template;
            if (schedulesInfo && schedulesInfo.length > 0) {
                template = $('#schedulesTemplate').html();
            } else {
                template = $('#noSchedulesTemplate').html();
            }
            var html = $(_.template(template, { schedules: collectScheduleInfoString(schedulesInfo) }));

            html.delegate(".delete", "click", function () {
                var elem = this;
                var scheduleId = $(elem).closest(".selectedSchedule").find(".title span").attr("dataRow");
                $.each(schedulesInfo, function (index) {
                    if (this.ID == scheduleId) {
                        //Remove from array
                        schedulesInfo.splice(index, 1);
                    }
                });
                SW.Orion.SchedulePicker.removeFromGridSelection(scheduleId);
            <%=OnUpdateSelectedSchedules%>(schedulesInfo);
            renderSchedules(schedulesInfo);
        });

         $("#selectedSchedulesList").html(html);
          <%if (!string.IsNullOrEmpty(OnUpdateSelectedSchedules)){ %>
                <%=OnUpdateSelectedSchedules%>(schedulesInfo);
            <% }%>
     };

     var collectScheduleInfoString = function (schedulesInfo) {
         var idNameInfoArray = new Array;

         $.each(schedulesInfo, function () {
             var item = this;
             var idNameInfo = {
                 ID: item.ID,
                 Name: Ext.util.Format.htmlEncode(item.Info.Name),
                 Information: function () {
                     var str = item.Info.Description == '' ? '' : (item.Info.Description + '<br/>');
                     str += String.format('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_70) %>', Ext.util.Format.htmlEncode(item.Info.FrequencyTitle), Ext.util.Format.htmlEncode(item.Info.ActionTitles));
                     return Ext.util.Format.htmlEncode(str);
                 }()
             };
             idNameInfoArray.push(idNameInfo);
         });
         return idNameInfoArray;
     };


     SW.Orion.SchedulePicker.setSelectedReportJobs = function (reportJobsArray) {
         SW.Orion.SchedulePicker.setSelectedReportJobsIds(reportJobsArray, renderSchedules);
     };
     SW.Orion.SchedulePicker.selectScheduleDialog = function () {
         $("#shceduleReportBox").show();
         $("#shcedulePickerBox").dialog({ modal: true, draggable: true, resizable: false, position: ['center', 'top'], width: 1000, title: "<%= Title %>", dialogClass: 'ui-dialog-osx' });
         var getHeaderTitleFn = Ext.util.Format.htmlEncode("<%= GetHeaderTitleFn%>");
         if (typeof getHeaderTitleFn == "function")
             $("#headerTitle").html(getHeaderTitleFn());
         if ($("#sheduleReportPicker").children().length == 0) {
             SW.Orion.SchedulePicker.init();
             $(SW.Core.Widgets.Button('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_28) %>', { type: 'secondary', id: 'cancelBtn' })).appendTo("#shcedulePickerBox").css('float', 'right').css('margin', '10px').click(function () {
                 $("#shcedulePickerBox").dialog("close");
             });
             $(SW.Core.Widgets.Button("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_175) %>", { type: 'primary', id: 'scheduleSelectedReportsBtn' })).appendTo("#shcedulePickerBox").css('float', 'right').css('margin', '10px').click(function () {
                 if ($("#isShceduleReportDemoMode").length != 0) {
                     demoAction("Core_Scheduler_AssignSchedule", this);
                     return;
                 }
                 $("#shcedulePickerBox").dialog("close");
                 var pickedSchedules = SW.Orion.SchedulePicker.getSelectedReportJobs();
                 // push dictionary into hidden field
                 var dictionary = [];
                 $.each(pickedSchedules, function () { dictionary.push({ Key: this.ID, Value: this.Info }); });

                 <%if (!string.IsNullOrEmpty(OnSchedulesSelected))
                      {%>
                 var onSelected = <%= OnSchedulesSelected%> ;
                 if (typeof onSelected == "function") {
                     onSelected(dictionary);
                 }
                 <% } %>
                 SW.Orion.SchedulePicker.setSelectedReportJobs(pickedSchedules);
             });
         }
         SW.Orion.SchedulePicker.refreshSelection();

         $("#shcedulePickerBox").parent().css('margin-top', '7%');
     };
 });
</script>
