<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReportConfigurationData.ascx.cs" Inherits="Orion_Reports_Controls_ReportConfigurationData" %>


<table>
    <tr>
        <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_OJ0_4) %><%--Title--%></td>
        <td><orion:ValidatedTextBox ID="tbTitle" Type="Text" runat="server" UseRequiredValidator="true"  />    </td>
    </tr>
    <tr>
        <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_OJ0_5) %><%--SubTitle--%></td>
        <td><orion:ValidatedTextBox ID="tbSubTitle" Type="Text" runat="server" UseRequiredValidator="true" />  </td>
    </tr>
</table>

<script type="text/javascript">
// Watermark wont work with required validators
//    var tbTitleTextId = '#' + $('#<%= tbTitle.ClientID %>').find("input[type='text']:first").attr('id');
//    var tbSubTitleTextId = '#' + $('#<%= tbSubTitle.ClientID %>').find("input[type='text']:first").attr('id');

//    var tbTitle = new SW.Core.Widgets.WatermarkTextbox(tbTitleTextId, '<%=DefaultTitleText %>');
//    var tbSubTitle = new SW.Core.Widgets.WatermarkTextbox(tbSubTitleTextId, '<%=DefaultSubTitleText %>');
    
    
</script>
