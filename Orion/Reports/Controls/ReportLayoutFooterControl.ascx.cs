﻿using System;
using SolarWinds.Reporting.Models.Layout;

public partial class Orion_Reports_Controls_ReportLayoutFooterControl : System.Web.UI.UserControl
{
	public string DefaultFooterText = "© " + Resources.CoreWebContent.WEBDATA_RV0_4;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(footerTextArea.Text))
            footerTextArea.Text = DefaultFooterText;
    }

    public bool ShowFooter
    {
        set
        {
            footerCheckData.Value = value.ToString().ToLower();
        }
        get
        {
            bool showFooter;
            return bool.TryParse(footerCheckData.Value, out showFooter) && showFooter;
        }
    }

    public bool ShowCustomText
    {
        set
        {
			customTextCheckData.Value = value.ToString().ToLower();
        }
        get
        {
            bool showCustomText;
            return bool.TryParse(customTextCheckData.Value, out showCustomText) && showCustomText;
        }
    }

    public bool ShowCreationDate
    {
        set
        {
			creationDateData.Value = value.ToString().ToLower();
        }
        get
        {
            bool creationDate;
            return bool.TryParse(creationDateData.Value, out creationDate) && creationDate;
        }
    }

    public string CustomText
    {
        set
        {
            footerTextArea.Text = value;
        }
        get
        {
            return footerTextArea.Text;
        }
    }

    public Footer Footer
    {
        set
        {
            if (value != null)
            {
				ShowFooter = value.Visible;
				ShowCustomText = value.ShowCustomText;
				CustomText = value.CustomText;
                ShowCreationDate = value.ShowTimestamp;
            } else 
			{
				ShowFooter = true;
				ShowCustomText = true;
				ShowCreationDate = true;
				CustomText = DefaultFooterText;
            }
        }
        get
        {
			return new Footer { ShowCustomText = ShowCustomText, CustomText = CustomText, ShowTimestamp = ShowCreationDate, Visible = ShowFooter, ShowPageNumber = true};
        }
    }
}