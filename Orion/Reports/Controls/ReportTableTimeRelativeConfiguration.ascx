<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReportTableTimeRelativeConfiguration.ascx.cs" Inherits="Orion_Reports_Controls_ReportTableTimeRelativeConfiguration" %>
<%@ Register Src="~/Orion/Reports/Controls/ReportTableSummarizing.ascx" TagPrefix="orion" TagName="ReportTableSummarizing" %>
<%@ Register src="~/Orion/Reports/Controls/ReportTableTimeFieldColumnSelection.ascx" TagPrefix="orion" TagName="ReportTableTimeFieldColumnSelection" %>

<style type="text/css">
    .link {
        color: #3D6E9E;
        cursor: pointer;
    }
    .tipLink {
        margin-left: 5px;
    }
    .timeRelativeConfig {
        border: 1px solid #CCC;
        padding: 20px;
        width: 850px;
        margin: 10px 0px;
    }
    .timeRelativeConfig .container {
        margin-left: 25px;
        }

    .timeRelativeIntro {
        float:left;
        margin-top: 4px;
    }
    #helpTextEptyTimeFields {
        text-align:left;
        margin-left: 30px;
    }
    .infoIcon {
        background-image:url('/Orion/images/Reports/Icon_Info.png');
	    background-repeat: no-repeat;
	    padding: 2px 0px 0px 22px;
    }
</style>
<div>
    <h3><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_OJ0_6) %><%--Time-based settings:--%></h3>
    <div class="timeRelativeIntro">
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_OJ0_7) %><%--Data in table above--%> 
    </div>
    <div class="timeSettings">
        <asp:HiddenField runat="server" ID="hdnTimeRelative"/>
        <asp:dropdownlist runat="server" ID="ddlTimeRelative" CssClass="TimeRelativeSelection"></asp:dropdownlist><span class="link tipLink infoIcon" ext:qtip="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_OJ0_13) %>"></span>
    </div>
   
    
</div>
<div class="ui-corner-all ui-helper-hidden timeRelativeConfig">
            <div class="container">
                <asp:Placeholder runat="server" ID="phNoTimeFieldsAvailable">
                        <img src="/Orion/images/Reports/clock_time_period.png" style="margin:0px 4px;" />
                        <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_OJ0_9) %><%--There is no main Date/Time column added into the table yet..--%><a class="link fieldPickerTrigger"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_OJ0_10) %><%--Add Column--%></a></span>
                        <div id="helpTextEptyTimeFields" class="helpfulText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_42) %></div>
                </asp:Placeholder>
                <asp:Placeholder runat="server" ID="phTimeFieldSelection">
                    <div class="ReportTableTimeFieldColumnSelection-Wrapper" id="reportTableTimeFieldSelectionWrapper" runat="server">
                        <orion:ReportTableTimeFieldColumnSelection OnCreatedJS="SW.Core.TableResource.TableConfigurationController.onTimeFieldColumnSelectionCreated" ID="reportTableTimeFieldSelection" runat="server" />
                    </div>
                    <div class="ReportTableConfiguration-Wrapper" id="reportTableSummarizingWrapper" runat="server">
                        <orion:ReportTableSummarizing runat="server" ID="reportTableSummarizing" />
                    </div>
                </asp:Placeholder> 
            </div>
</div>

<script type="text/javascript">
    $(function () {
        var timeRelativeSelection = new SW.Core.TableResource.TimeRelativeSelection({
            selectionElementId: '<%= ddlTimeRelative.ClientID%>',
            hiddenElementId: '<%= hdnTimeRelative.ClientID %>'
        });
        timeRelativeSelection.Init();
    });
</script>