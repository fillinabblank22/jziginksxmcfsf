<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReportTableConfiguration.ascx.cs" Inherits="Orion_Reports_Controls_ReportTableConfiguration" %>
<%@ Register Src="~/Orion/Reports/Controls/ReportTableColumnLayout.ascx" TagPrefix="orion" TagName="ReportTableColumnLayout" %>
<%@ Register Src="~/Orion/Reports/Controls/ReportTableSortConfiguration.ascx" TagPrefix="orion" TagName="ReportTableColumnOrderSortBy" %>
<%@ Register src="~/Orion/Reports/Controls/ReportTableGroupByConfiguration.ascx" TagPrefix="orion" TagName="ReportTableColumnOrderGroupBy" %>
<%@ Register Src="~/Orion/Reports/Controls/ReportTableFilter.ascx" TagPrefix="orion" TagName="ReportTableFilter" %>
<%@ Register Src="~/Orion/Reports/Controls/ReportTablePresenterEdit.ascx" TagPrefix="orion" TagName="ReportTablePresenters" %>
<%@ Register Src="~/Orion/Reports/Controls/ReportTableTimeRelativeConfiguration.ascx" TagPrefix="orion" TagName="ReportTableTimeRelativeConfiguration" %>
<%@ Register TagPrefix="orion" TagName="ServerFilter" Src="~/Orion/Controls/OrionServerFilterControl.ascx" %>

<orion:Include runat="server" File="Reports/js/ReportTableHelperUtils.js" />
<orion:Include runat="server" File="Reports/js/ReportTableEventPublisher.js" />
<orion:Include runat="server" File="Reports/js/ReportTableTimeRelativeSelection.js" />
<orion:Include runat="server" File="ReportTableConfiguration.css" />

<script type="text/javascript">
    SW.Core = SW.Core || {};
    SW.Core.TableResource = SW.Core.TableResource || {};
    SW.Core.TableResource.TableConfigurationController = (function () {

        var columnLayoutEditor = null;
        var timeFieldSelectionControl = null;
        var columnSortEditor = null;
        var columnGroupEditor = null;
        var columnResizeControl = null;

        var createCaptionWithSwisEntityIncluded = eval("<%= CreateCaptionWithSwisEntityIncluded %>".toLowerCase()); 

        var createdColumns = []; // will be array of object with properties FieldId, RefID and Caption
        function ColumnLayoutEditorCreated(columnLayoutEd) {
            columnLayoutEditor = columnLayoutEd;
            columnLayoutEditor.on("columnsCreated", function (sender, eventArgs) {
                // TODO: here will be code which will react to that dragable columnsWas created. In eventArgs is passed initial configuration of columns
                // eventArgs is array of following. Into object should be added also Field and RefId
                // { refHref: $aArray[i].hash, caption: $($aArray[i]).data("tabcaption"), isexpanded: isExpanded, isselected: selectedIndexTab == i, order: i, exist: true }
                createdColumns = [];
                for (var i = 0; i < eventArgs.Columns.length; i++) {
                    createdColumns.push({ FieldId: eventArgs.Columns[i].FieldId, RefID: eventArgs.Columns[i].RefID, Caption: eventArgs.Columns[i].Caption, ColumnDataType: eventArgs.Columns[i].ColumnDataType, CaptionWithSwisEntityIncluded: eventArgs.Columns[i].CaptionWithSwisEntityIncluded, IsHidden: eventArgs.Columns[i].IsHidden, IsHTMLTagsAllowed: eventArgs.Columns[i].IsHTMLTagsAllowed });
                }

                if (columnSortEditor) {
                    columnSortEditor.FillColumnList(createdColumns, createCaptionWithSwisEntityIncluded);
                    columnSortEditor.InitializeSortColumnsByOrderColumnsConfig();
                }

                if (columnGroupEditor) {
                    columnGroupEditor.FillColumnList(createdColumns, createCaptionWithSwisEntityIncluded);
                    columnGroupEditor.InitializeSortColumnsByOrderColumnsConfig();
                }

                if (timeFieldSelectionControl) {
                    timeFieldSelectionControl.FillColumnList(createdColumns);
                }

            });

            columnLayoutEditor.on("columnAdded", function (sender, eventArgs) {
                // In eventArgs is passed following object: { FieldID: '', Caption: '', ColumnDataType: '' }
                createdColumns.push({ FieldId: eventArgs.FieldID, Caption: eventArgs.Caption, ColumnDataType: eventArgs.ColumnDataType, IsHidden: eventArgs.IsHidden, IsHTMLTagsAllowed: eventArgs.IsHTMLTagsAllowed });
                var caption = eventArgs.Caption;

                if (createCaptionWithSwisEntityIncluded) {
                    if (eventArgs.CaptionWithSwisEntityIncluded) {
                        caption = eventArgs.CaptionWithSwisEntityIncluded;
                    }
                }

                if (timeFieldSelectionControl != null) {
                    timeFieldSelectionControl.AddFieldIntoList(eventArgs.FieldID, eventArgs.RefID, caption, eventArgs.ColumnDataType);
                }

                if (columnSortEditor) {
                    columnSortEditor.AddColumnIntoListOfAvailable(eventArgs.FieldID, caption, eventArgs.RefID, eventArgs.IsHidden, eventArgs.IsHTMLTagsAllowed);
                }

                if (columnGroupEditor) {
                    columnGroupEditor.AddColumnIntoListOfAvailable(eventArgs.FieldID, caption, eventArgs.RefID, eventArgs.IsHidden, eventArgs.IsHTMLTagsAllowed);
                }

                if (columnLayoutEditor && columnResizeControl) {
                    columnLayoutEditor.UpdateColumnWidth(columnResizeControl.GetColumnSizes(createdColumns.length));
                }
            });

            columnLayoutEditor.on("columnRemoved", function (sender, eventArgs) {
                // In eventArgs is passed following object: { FieldID: '', Caption: '', RefID: '' }
                var indexToDelete = -1;
                for (var i = 0; i < createdColumns.length; i++) {
                    if (createdColumns[i].RefID == eventArgs.RefID) {
                        indexToDelete = i;
                        break;
                    }
                }

                if (indexToDelete > -1) {
                    createdColumns.splice(indexToDelete, 1);
                }

                var exists = false;
                for (var i = 0; i < createdColumns.length; i++) {
                    if (createdColumns[i].RefID == eventArgs.RefID) {
                        exists = true;
                        break;
                    }
                }
                
                if (!exists) {
                    if (timeFieldSelectionControl != null) {
                        timeFieldSelectionControl.RemoveColumnFromList(eventArgs.FieldID);
                    }

                    if (columnSortEditor) {
                        columnSortEditor.DeleteColumn(eventArgs.RefID);
                    }

                    if (columnGroupEditor) {
                        columnGroupEditor.DeleteColumn(eventArgs.RefID);
                    }
                }

                if (columnLayoutEditor && columnResizeControl) {
                    columnLayoutEditor.UpdateColumnWidth(columnResizeControl.GetColumnSizes(createdColumns.length, indexToDelete));
                }
            });

            columnLayoutEditor.on("columnChanged", function (sender, eventArgs) {
                // In eventArgs is passed following object: { PreviousFieldID: fieldid, NewFieldID: '', RefID: refid, Caption: data.DisplayNameValue }
                
                var caption = eventArgs.Caption;
                if (createCaptionWithSwisEntityIncluded) {
                    if (eventArgs.CaptionWithSwisEntityIncluded) {
                        caption = eventArgs.CaptionWithSwisEntityIncluded;
                    }
                }
                
                if (columnSortEditor) {
                    columnSortEditor.ChangeColumnCaption(eventArgs.RefID, caption, eventArgs.NewFieldID, eventArgs.IsHidden, eventArgs.IsHTMLTagsAllowed);
                }

                if (columnGroupEditor) {
                    if (eventArgs.IsHidden)
                        columnGroupEditor.DeletePortletItem(eventArgs.RefID, caption, eventArgs.NewFieldID, eventArgs.IsHidden, eventArgs.IsHTMLTagsAllowed);
                    columnGroupEditor.ChangeColumnCaption(eventArgs.RefID, caption, eventArgs.NewFieldID, eventArgs.IsHidden, eventArgs.IsHTMLTagsAllowed);
                }
            });
        }

        function TimeFieldColumnSelectionCreated(timeFieldControl) {
            timeFieldSelectionControl = timeFieldControl;
            timeFieldSelectionControl.FillColumnList(createdColumns);
        }

        function ColumnSortEditorCreated(sortEd) {
            columnSortEditor = sortEd;
            columnSortEditor.FillColumnList(createdColumns, createCaptionWithSwisEntityIncluded);
            columnSortEditor.InitializeSortColumnsByOrderColumnsConfig();
        }

        function ColumnGroupEditorCreated(groupEd) {
            columnGroupEditor = groupEd;
            columnGroupEditor.FillColumnList(createdColumns, createCaptionWithSwisEntityIncluded);
            columnGroupEditor.InitializeSortColumnsByOrderColumnsConfig(); // TODO: here will be probably different calling, because it will be needed to initialize different properties, than are for sorting
        }
        
        function onPreviewColumnResize(columnPreviewResize) {
            columnResizeControl = columnPreviewResize;
            if (columnLayoutEditor) {
                columnResizeControl.InitColumnStates(columnLayoutEditor.GetColumnsWidthInfo());
            }
        }
        
        function onPreviewColumnResizeSubmit() {
            var columnSizesInfo = columnResizeControl.GetColumnSizes();
            columnLayoutEditor.UpdateColumnWidth(columnSizesInfo);
        }
        
        //Function displays a formatted message object
        //param message data is an object {message:"hello {0} how are you {1}", data:['bob','today']}
        function showValidationMessage(messageData) {
            var combinedMsgData = [messageData.message].concat(messageData.data);
            var newMsgElement = $('<div>' + String.format.apply(undefined, combinedMsgData) + '</div>');
            $('#jsValidationSummary').hide().empty().append(newMsgElement).fadeIn();
        }

        SW.Core.TableResource.EventPublisher.SubscribeValidationMessageRegistered(function (e) {
            showValidationMessage(e);
        });


        return {
            selectedColumns: createdColumns,
            onColumnLayoutEditorCreated: ColumnLayoutEditorCreated,
            onTimeFieldColumnSelectionCreated: TimeFieldColumnSelectionCreated,
            onColumnSortEditorCreated: ColumnSortEditorCreated,
            onColumnGroupEditorCreated: ColumnGroupEditorCreated,
            onPreviewColumnResize: onPreviewColumnResize,
            onPreviewColumnResizeSubmit: onPreviewColumnResizeSubmit
        };
    })();

    $(function()
    {
        if (<%= ColumnsCount %> == 0)
        {
            $(".columnDependentSettings").hide();
        }
    });
</script>

<asp:ValidationSummary ID="ReportTableValidationSummary" ValidationGroup="columnValidation" runat="server" />
<div id="jsValidationSummary" class="Error"></div>
<div id="columnsWithError" Visible="False" class="ReportTableConfiguration-Wrapper" runat="server">
    <div class="ReportTableConfiguration-SectionHeader" class="Error">
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_PF0_11) %><%--Columns with problems:--%>
    </div>
    <asp:Repeater ID="columnListRepeater" runat="server">
        <HeaderTemplate>
            <ul class="Error">
        </HeaderTemplate>
        <ItemTemplate>
             <li>
                <%# DefaultSanitizer.SanitizeHtml(Container.DataItem) %>
             </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
        </FooterTemplate>
    </asp:Repeater>
</div>

<asp:CustomValidator ID="ValidateColumns" ValidationGroup="columnValidation" Display="None" runat="server" OnServerValidate="ValidateTableConfiguration_ServerValidate"/>
<asp:CustomValidator ID="ValidatorStatisticalEntityHasTimeStamp" runat="server" OnServerValidate="ValidateStatisticalEntityHasTimeStamp_ServerValidate"/>

<div class="ReportTableConfiguration-Wrapper">
    <div class="ReportTableConfiguration-SectionHeader"><%--TODO:This should be moved inside tablecolumnLayout but right now that causes the label to render below the control.--%>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_OJ0_5) %> <a href="#" style="font-weight: normal;" onclick="__doPostBack('columnLayout','Preview')"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_43) %></a><%--Table layout:--%>
    </div>
    <orion:ReportTableColumnLayout OnCreatedJS="SW.Core.TableResource.TableConfigurationController.onColumnLayoutEditorCreated" runat="server" ID="reportTableColumnLayout" />
</div>

<div class="columnDependentSettings">
    <asp:Panel runat="server" ID="pnlColumnDependentSettings">
        
        <% if (IsFederationEnabled) {%>
        <div style="padding-top: 8px;">
            <orion:ServerFilter ID="ServerFilter" runat="server" Reporting="True" />
        </div>
        <% }%>

        <div class="ReportTableConfiguration-Wrapper">
            <orion:ReportTableFilter runat="server" ID="reportTableFilter" />
        </div>

        <asp:CustomValidator ID="ValidateSumarizeMode" ValidationGroup="columnValidation" Display="None" runat="server" OnServerValidate="ValidateSumarizeMode_ServerValidate"/>
        <orion:ReportTableTimeRelativeConfiguration runat="server" ID="reportTableTimeRelativeConfiguration" />

        <div class="ReportTableConfiguration-Wrapper">
            <orion:ReportTableColumnOrderSortBy OnCreatedJS="SW.Core.TableResource.TableConfigurationController.onColumnSortEditorCreated" runat="server" ID="reportTableSort" />
        </div>
        <div class="ReportTableConfiguration-Wrapper">
            <orion:ReportTableColumnOrderGroupBy OnCreatedJS="SW.Core.TableResource.TableConfigurationController.onColumnGroupEditorCreated" ID="reportTableGroup" runat="server"/>
        </div>
    </asp:Panel>
</div>

<orion:ReportTablePresenters runat="server" ID="presenters" />
