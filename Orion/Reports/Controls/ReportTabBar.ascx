<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReportTabBar.ascx.cs" Inherits="Orion_Reports_Controls_ReportTabBar" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Controls"  %>
<%@ Register Src="~/Orion/Controls/NavigationTabBar.ascx" TagPrefix="orion" TagName="NavigationTabBar" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" Assembly="OrionWeb" %>
<orion:NavigationTabBar ID="IgnoreListNavigationTabBar" runat="server" Visible="true">   
    <orion:NavigationTabItem Name="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_305 %>" Url="~/Orion/Reports/Default.aspx" />
    <orion:NavigationTabItem Name="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_306 %>" Url="~/Orion/Reports/Scheduler.aspx" />
</orion:NavigationTabBar>