<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditDetailDataSeriesControl.ascx.cs" Inherits="Orion_Reports_Controls_EditDetailDataSeriesControl" %>
<%@ Import Namespace="SolarWinds.Orion.Web.UI" %>
<%@ Register TagPrefix="orion" TagName="FieldPicker" Src="~/Orion/Controls/FieldPicker/FieldPicker.ascx" %>

<orion:Include ID="Include0" runat="server" File="jQuery" />
<orion:Include ID="Include1" runat="server" File="spectrum.css" />
<orion:Include ID="Include2" runat="server" File="ColorPicker.js" />
<orion:Include ID="Include3" runat="server" File="Reports/js/EditDetailDataSeriesControl.js" />

<style type="text/css">
    #<%= this.ClientID %> {
        border: 1px solid #f99d1c;
        padding: 0px;
        padding-left: 10px;
        padding-right: 10px;
    }
    .detailsTable {
        border-spacing: 10px;
    }
</style>

<script type="text/javascript">
    var controler<%= this.ClientID %> = new SW.Core.ReportChart.ChartEdit.DataSeriesDetailController({
        lblTimeSeriesID: '<%= lblTimeSeries.ClientID %>',
        lblSeriesNameID: '<%= lblSeriesName.ClientID %>',
        ddSelectColorModeID: '<%= ddSelectColorMode.ClientID %>',
        colorSelectorID: '<%= colorSelector.ClientID %>',
        btnTimeSeriesID: '<%= btnTimeSeries.ClientID %>',
        hfSeriesDetailsID: '<%= hfSeriesDetails.ClientID %>',
        tbDisplayNameID: '<%= tbDisplayName.ClientID %>',
        cbShowTrendLineID: '<%= cbShowTrendLine.ClientID %>',
        cbCalculate95thPercentileLineID: '<%= cbCalculate95thPercentileLine.ClientID %>',
        chooseTextI18N: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_311) %>',
        cancelTextI18N: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_28) %>',
        hostingDivID: '<%= this.ClientID %>'
    });

</script>


<orion:FieldPicker ID="fieldPickerDataSeries" runat="server" SingleMode="true" 
    AddFieldsButtonTextI18N="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_ZT0_11 %>"
    DialogTitleTextI18N="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_ZT0_11 %>"
    GroupByHeaderTextI18N="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_ZT0_13 %>"
    SelectedFieldsTextI18N="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_ZT0_14 %>"
    SearchWatermarkTextI18N="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_ZT0_15 %>"
    FieldsGridHeaderTextI18N="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_ZT0_16 %>"/>
<orion:FieldPicker ID="fieldPickerTimeSeries" runat="server" SingleMode="true"/>

<asp:HiddenField runat="server" ID="hfSeriesDetails"/>

<div id="<%= this.ClientID %>" style="display: <%= GetShowDetailDisplayCssValue() %>">
    <table class="detailsTable">
        <tr>
            <td><asp:Label runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TP0_12 %>"></asp:Label></td>
            <td>
                <asp:Label runat="server" ID="lblSeriesName" Text="<%$ HtmlEncodedResources:CoreWebContent,WEB_JS_CODE_TM0_3 %>"></asp:Label>
            </td>
            <td></td>
        </tr>
        <tr>
            <td><asp:Label runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TP0_13 %>"></asp:Label></td>
            <td><asp:TextBox runat="server" ID="tbDisplayName" Width="200"></asp:TextBox></td>
            <td></td>
        </tr>
        <tr>
            <td><asp:Label runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TP0_15 %>"></asp:Label></td>
            <td>
                <asp:DropDownList runat="server" ID="ddSelectColorMode" Width="200"></asp:DropDownList>
            </td>
            <td><input type="hidden" runat="server" ID="colorSelector" /></td>
        </tr>
        <tr>
            <td><asp:Label runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TP0_16 %>"></asp:Label></td>
            <td>
                <table>
                    <tbody>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" ID="cbCalculate95thPercentileLine" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_96 %>"/>
                        </td>
                    </tr>
                    <tr>
                        <td>    
                            <asp:CheckBox runat="server" ID="cbShowTrendLine" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_291 %>"/>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td><asp:Label runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TP0_17 %>"></asp:Label></td>
            <td>
                <asp:Label runat="server" ID="lblTimeSeries" Text="<%$ HtmlEncodedResources:CoreWebContent,WEB_JS_CODE_TM0_3 %>"></asp:Label>
               <orion:LocalizableButton  ID="btnTimeSeries" runat="server" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_YK0_11 %>" DisplayType="Small"  />
              
            </td>
            <td></td>
        </tr>
    </table>
</div>

