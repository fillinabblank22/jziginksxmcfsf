﻿using System;
using System.Linq;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Internationalization.Extensions;
using SolarWinds.Reporting.Models.Selection;
using System.Collections;
using SolarWinds.Orion.Web.Reporting;
using System.IO;
using System.Web.UI;
using SolarWinds.Reporting.Impl.Rendering;

public partial class Orion_Reports_Controls_ReportTableSummarizing :  ModelBasedControl<Tuple<DataSummarizeMode>>
{
    public bool SummarizationHidden { get; set; }

    public override void LoadViewModel()
    {
        base.LoadViewModel();

        if (SummarizationHidden || string.IsNullOrEmpty(ddlSummarizeBy.SelectedValue))
        {
            ViewModel = new Tuple<DataSummarizeMode>(DataSummarizeMode.NoDataSummarization); 
        }
        else
        {
            var selectedMode = (DataSummarizeMode) Enum.Parse(typeof(DataSummarizeMode), ddlSummarizeBy.SelectedValue);
            ViewModel = new Tuple<DataSummarizeMode>(selectedMode);
        }
    }

    private void BindInfoMessage()
    {
        string infoMessage;
        var store = GetReportStorageData();
        var timeframe = store == null || !store.IsTimeFrameSupported() ? null : store.GetTimeFrame();
        using (var stringWriter = new StringWriter())
        {
            using (var writer = new HtmlTextWriter(stringWriter))
            {
                RenderCellHeader.Render(writer, null, timeframe, null, null, string.Empty);
                infoMessage = stringWriter.GetStringBuilder().ToString();
            }
        }
        litInfoMessage.Text = infoMessage;
    }
    private void BindDDlSummarize()
    {
        ddlSummarizeBy.DataValueField = "Value";
        ddlSummarizeBy.DataTextField = "Text";
        var summarizationOptions = Enum.GetValues(typeof(DataSummarizeMode))
            .Cast<DataSummarizeMode>()
            .Where(m => !m.Equals(DataSummarizeMode.NoDataSummarization))
            .Select(m => new { Value = m, Text = m.LocalizedLabel() });

        var options = summarizationOptions.ToList();
        options.Insert(0, new { Value = DataSummarizeMode.NoDataSummarization, Text = Resources.CoreWebContent.WEBDATA_OJ0_12 });

        ddlSummarizeBy.DataSource = options;
        ddlSummarizeBy.DataBind();
        ddlSummarizeBy.SelectedValue = ViewModel.Item1.ToString();
    }

    protected override void BindViewModel()
    {
        base.BindViewModel();

        BindDDlSummarize();
        BindInfoMessage();

        SetUiVisibility();
    }

    private void SetUiVisibility()
    {
        pnlSummarization.Visible = !SummarizationHidden;
    }

    private ReportStorageData GetReportStorageData()
    {
        if (!String.IsNullOrEmpty(Request["ReportStorageGuid"]) && !String.IsNullOrEmpty(Request["SectionCellRefId"]))
        {
            try
            {
                var reportStorageGuid = Guid.Parse(Request["ReportStorageGuid"]);
                var sectionCellRefId = Guid.Parse(Request["SectionCellRefId"]);
                var store = new ReportStorageData(reportStorageGuid, sectionCellRefId);
                return store;
            }
            catch (Exception)
            {
                //report guid may not be viable if session expires.
            }
        }
        return null;
    }
}