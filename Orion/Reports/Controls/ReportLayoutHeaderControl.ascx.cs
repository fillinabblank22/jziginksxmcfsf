﻿using System;
using System.Drawing;
using System.IO;
using Resources;
using SolarWinds.Orion.Core.Reporting.DAL;
using SolarWinds.Orion.Web;
//using SolarWinds.Logging;
using SolarWinds.Reporting.Models;
using SolarWinds.Reporting.Models.Layout;

public partial class Orion_Reports_Controls_ReportLayoutHeaderControl : System.Web.UI.UserControl
{
    private string logoImageByte64;

    private string getLogoUrl()
    {
        return String.Format("/Orion/Reports/ImageProvider.ashx?f=logo&id={0}&time={1}",
                Request["ReportWizardGuid"],
                DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds); // timestamp for logo URL needed for IE7, to avert image caching.
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Guid id;
        if (Guid.TryParse(Request["ReportWizardGuid"] ?? Guid.Empty.ToString(), out id))
        {
            Report report = ReportStorage.Instance.GetReport(id);
            if (report.Header != null && !string.IsNullOrEmpty(report.Header.Logo))
            {
                reportLogo.ImageUrl = getLogoUrl(); 
            }
        }
    }

    protected void AsyncFileUpload_UploadeComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        if (fileUploadImage.HasFile)
        {

            byte[] imageBytes = fileUploadImage.FileBytes;
            Guid id;
            if (isApplicableImage(fileUploadImage.FileContent))
            {
                if (Guid.TryParse(Request["ReportWizardGuid"] ?? Guid.Empty.ToString(), out id))
                {
                    Report report = ReportStorage.Instance.GetReport(id);
                    if (report.Header == null)
                    {
                        report.Header = new Header();
                    }
                    report.Header.Logo = Convert.ToBase64String(imageBytes);
                    Header = report.Header;
                    ReportStorage.Instance.Update(id, report);
                }
                Session["logoImage"] = Convert.ToBase64String(imageBytes);
            }
            fileUploadImage.FileContent.Close();
            updPanel.Update();
        }
    }

    protected void btnProcessData_Click(object sender, EventArgs e)
    {
        if (Session["logoImage"] != null)
        {
            reportLogo.ImageUrl = getLogoUrl();
            logoImageByte64 = Session["logoImage"].ToString();
            Session.Remove("logoImage");
            lblBadImage.Visible = false;
        }
        else
        {
            lblBadImage.Visible = true;
        }

        updPanel.Update();
    }

    private bool isApplicableImage(Stream imageStream)
    {
        try
        {
            using (Image image = Image.FromStream(imageStream))
                return (image.Width <= 600) && (image.Height <= 240);
        }
        catch
        {
            return false;
        }
    }

    public string TitleText
    {
        set 
        {
            headerTitleArea.Text = value; 
        }
        get
        {
            return (DefaultTitle.Equals(headerTitleArea.Text)) ? string.Empty : headerTitleArea.Text;
        }
    }

    public string SubtitleText
    {
        set
        {
            headerSubTitleArea.Text = value;
        }
        get
        {
            return (DefaultSubTitle.Equals(headerSubTitleArea.Text)) ? string.Empty : headerSubTitleArea.Text;
        }
    }
    
    public string DefaultTitle
    {
        get { return CoreWebContent.WEBDATA_SO0_64; }
    }
    
    public string DefaultSubTitle
    {
        get { return CoreWebContent.WEBDATA_SO0_65; }
    }

    public Header Header
    {
        set
        {
            if (value != null)
            {
                if (!string.IsNullOrEmpty(value.Logo))
                {
                    logoImageByte64 = value.Logo;
                    checkLogo.Checked = true;
                }
                else
                {
                    reportLogo.ImageUrl = "/Orion/images/Reports/default_report_logo.png";
                    logoImageByte64 = WebConstants.StandardReportLogoIdentifier;   
                }

                TitleText = value.Title;
                SubtitleText = value.SubTitle;
            }
            else
            {
                checkLogo.Checked = true;
                reportLogo.ImageUrl = "/Orion/images/Reports/default_report_logo.png";
                logoImageByte64 = WebConstants.StandardReportLogoIdentifier;
                
            }
        }
        get
        {
            var header = new Header { Title = TitleText, SubTitle = SubtitleText, Visible = true, Logo = String.Empty };
            if (checkLogo.Checked)
            {
                Guid id;
                if (Guid.TryParse(Request["ReportWizardGuid"] ?? Guid.Empty.ToString(), out id))
                {
                    Report report = ReportStorage.Instance.GetReport(id);
                    if (report.Header != null && !string.IsNullOrEmpty(report.Header.Logo))
                    {
                        header.Logo = report.Header.Logo;
                    }
                    else
                    {
                        header.Logo = WebConstants.StandardReportLogoIdentifier;
                    }
                }
            }
            return header;
        }
    }
}
