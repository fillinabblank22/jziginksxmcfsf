<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReportTableGroupByConfiguration.ascx.cs" Inherits="Orion_Reports_Controls_ReportTableGroupByConfiguration" %>
<!-- TODO: in future will be needed to replace ReportTableSortConfiguration.js by ReportTableGroupConfiguration.js and also
    ReportTableSortConfiguration.css by ReportTableGroupConfiguration.css
 -->
<orion:Include ID="Include1" runat="server" File="Reports/js/ReportTableSortConfiguration.js" />
<orion:Include ID="Include2" runat="server" File="ReportTableSortConfiguration.css" />

<div class="ReportTableConfiguration-SectionHeader">
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_OJ0_3) %><%--Group results by:--%>
</div>
<div id="<%= DefaultSanitizer.SanitizeHtml(GetSaveUniqueID(UniqueID)) %>" class="columnItem">
    <div class="addColumn" style="float: left; display: inline; padding: 2px 5px 3px 3px;">
        <select class="columnList"></select>
   </div>
</div>
<asp:HiddenField runat="server" ID="columnGroupByOrderConfig"/>
<div style="clear: both; float: none; width: 1px;"></div>

<script id="columnGroupByOrderTemplate" type="text/x-template">
    <div class="portletItem">
        <a href="#">
            <div class="portlet-header">
                <table>
                    <tbody>
                        <tr>
                            <td style="width: 15px;">
                                <img src="/Orion/images/Reports/ddpick.png" style="margin-top: 4px" />
                            </td>
                            <td>{{ColumnTitle}}</td>
                            <td style="width: 15px">
                                <img class="resource-manage-btn" data-name="deleteButton" src="/Orion/images/Reports/delete_icon16x16.png"/>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </a>
    </div>
</script>

<script type="text/javascript">
    $(function () {
        var columnOrder = new SW.Core.TableResource.ColumnOrder({
            parentElId: "<%= GetSaveUniqueID(this.UniqueID) %>",
            columnOrderConfig: "<%= this.columnGroupByOrderConfig.ClientID %>",
            isGroupByConfiguration: true
        });

        columnOrder.SetColumnTemplate($("#columnGroupByOrderTemplate").html());
        
        columnOrder.CreateDraggableColumns();
        columnOrder.FillColumnList();
        var groupByColumnCreatedCallback = <%= this.OnCreatedJS %>;
        if (groupByColumnCreatedCallback && jQuery.isFunction(groupByColumnCreatedCallback)) {
            groupByColumnCreatedCallback(columnOrder);
        }
    });
</script>
