﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReportTableFilter.ascx.cs" Inherits="Orion_Reports_Controls_ReportTableFilter" %>
<%@ Register Assembly="OrionWeb" Namespace="SolarWinds.Orion.Web.UI" TagPrefix="web" %>

<style type="text/css">
    .checkbox input[type="checkbox"] 
    { 
        margin-right: 5px; 
    }
</style>

<div class="ReportTableConfiguration-SectionHeader">
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_3) %><%--Filter results:--%>
</div>
<div>
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_4) %><%--This table can include all the records retrieved, or records can be filtered.--%>
</div>
<br/>
<asp:HiddenField ID="hdnSelectedValue" runat="server"/>
<asp:RadioButton ID="rbtnShowAll" runat="server" GroupName="FilterTypeGroup" Height="25" Checked="True" />
<label for="<%= rbtnShowAll.ClientID %>">
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_5) %><%--Show all records--%>
</label>
<br/>

<asp:RadioButton ID="rbtnShowOnlyTop" runat="server" GroupName="FilterTypeGroup" Height="25"/>
<label for="<%= rbtnShowOnlyTop.ClientID %>">
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_6) %><%--Show only the top--%>
</label>
<web:ValidatedTextBox ID="tbTopRecords" Text="10" Columns="2" Type="Integer" runat="server"  ValidatorText="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_OJ0_8 %>" UseRequiredValidator="true" UseRangeValidator="true" MinValue="1" MaxValue="9999" MaxLength="4" />
<label for="<%= rbtnShowOnlyTop.ClientID %>">    
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_7) %><%--records--%>
</label>
<br/>

<asp:RadioButton ID="rbtnShowOnlyTopPercent" runat="server" GroupName="FilterTypeGroup" Height="25"/>
<label for="<%= rbtnShowOnlyTopPercent.ClientID %>">
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_8) %><%--Show only the top--%>
</label>
<web:ValidatedTextBox ID="tbTopRecordPercent" Text="10" Columns="2" Type="Integer" runat="server" ValidatorText="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_OJ0_9 %>" UseRequiredValidator="true" UseRangeValidator="true" MinValue="1" MaxValue="100" MaxLength="3" />
<label for="<%= rbtnShowOnlyTopPercent.ClientID %>">
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PF0_9) %><%--% of records--%>
</label>
<br/>

<script type="text/javascript">
    $(function () {        
        <%
            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
         %>
        // enable or disable text boxes according to filter type
        var filterTypeGroupClick = function (event) {
            var tbTopRecords = $('#<%= tbTopRecords.ClientID %> input');
            var tbTopRecordPercent = $('#<%= tbTopRecordPercent.ClientID %> input');
            var tbTopRecordValidators = <%=serializer.Serialize(tbTopRecords.ValidatorsIds) %>;            
            var tbTopRecordPercentageValidators = <%=serializer.Serialize(tbTopRecordPercent.ValidatorsIds) %>;
            var hdnSelectedValue = $('#<%=hdnSelectedValue.ClientID %>');
            var target = event.target || event.srcElement;
            switch (target.value) {
                case '<%= rbtnShowOnlyTop.ID %>':
                    hdnSelectedValue.val('<%= rbtnShowOnlyTop.ClientID %>');
                    tbTopRecords.attr('disabled', false);
                    tbTopRecordPercent.attr('disabled', 'disabled');                    
                    for(var key in tbTopRecordValidators) {
                        $('#'+tbTopRecordValidators[key]).each(function () {
                        ValidatorEnable(this, true);
                        });                        
                    }                    
                    
                    for(var key in tbTopRecordPercentageValidators) {
                        $('#'+tbTopRecordPercentageValidators[key]).each(function () {
                        ValidatorEnable(this, false);
                        });                        
                    }
                    break;
                case '<%= rbtnShowOnlyTopPercent.ID %>': 
                    hdnSelectedValue.val('<%= rbtnShowOnlyTopPercent.ClientID %>');
                    tbTopRecords.attr('disabled', 'disabled');
                    tbTopRecordPercent.attr('disabled', false);
                    for(var key in tbTopRecordValidators) {
                        $('#'+tbTopRecordValidators[key]).each(function () {
                        ValidatorEnable(this, false);
                        });                        
                    }                    
                    
                    for(var key in tbTopRecordPercentageValidators) {
                        $('#'+tbTopRecordPercentageValidators[key]).each(function () {
                        ValidatorEnable(this, true);
                        });                        
                    }
                    break;
                default:
                    hdnSelectedValue.val('<%= rbtnShowAll.ID %>');
                    tbTopRecords.attr('disabled', 'disabled');
                    tbTopRecordPercent.attr('disabled', 'disabled');
                    for(var key in tbTopRecordValidators) {
                        $('#'+tbTopRecordValidators[key]).each(function () {
                        ValidatorEnable(this, false);
                        });                        
                    }                    
                    
                    for(var key in tbTopRecordPercentageValidators) {
                        $('#'+tbTopRecordPercentageValidators[key]).each(function () {
                        ValidatorEnable(this, false);
                        });                        
                    }
                    break;
            }
        };

        // radio boxes group name
        var groupName = $('#<%= rbtnShowAll.ClientID %>').attr('name');

        // attach on click event
        $(String.format('input[name="{0}"]', groupName)).on('click', filterTypeGroupClick);

        // call onclick event handler on selected radio button
        filterTypeGroupClick({ srcElement: $(String.format('input[name="{0}"]:checked', groupName))[0] });
    });
</script>