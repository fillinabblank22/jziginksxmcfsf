<%@ Control Language="C#" ClassName="ReportPicker" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Swis" %>

<script runat="server">
    private string onReportsSelected = "function(){}";

    [PersistenceMode(PersistenceMode.Attribute)]
    public string Title { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string GetHeaderTitleFn { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string OnUpdateSelectedReports { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string OnUpdateTypedUrls { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string OnReportsSelected
    {
        get { return onReportsSelected; }
        set { onReportsSelected = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string GetTypedUrlsFn { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool RenderSelectedItems { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool BlockIfDemo { get; set; }
</script>
<orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="Reports/js/ReportPicker.js" />
<orion:Include runat="server" File="Admin/js/SearchField.js" />
<orion:Include File="ReportSchedule.css" runat="server" />
<%if (RenderSelectedItems)
  {%>
<div id="selectedReportsList" style="margin: 10px 0;">
</div>
<% } %>
<%if (BlockIfDemo && SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer){%> <div id="isReportDemoMode" style="display: none;"></div> <%}%>
<div id="shceduleReportBox" style="display: none;">
    <div id="headerTitle" style="margin:10px; font-size:16px;"></div>
    <div id="searchPanel" style="float: left; width: 100%;"></div>
    <div style="clear: both;"></div>
    <div id="sheduleReportsPicker"></div>
    <label class="selectedSchedulesLabel"></label>
    <div id='selectedObjects' class='selectedObjectsCSS'><span style='color:gray;'><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_49) %></span></div>
</div>

<script id="reportsTemplate" type="text/x-template">
    <table id ='selectedReports'>
    <tr class="tableHeader">
           <th class = 'titleColumn'><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_89) %></th>            
           <th class = 'buttonColumn'>
           {# if(urls.length>0) { #}
           <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_141) %>
            {# } #}
           </th>          
           <th class = 'buttonColumn'><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_31) %></th> 
        </tr>
        <tr class='empty'><td colspan='3'>&nbsp;</td></tr>   
    {# _.each(reports, function(current, index) { #}
        
    <tr class='selectedReport' data-index='{{index}}'>
       <td class='title'><span dataRow = '{{current.ID}}'><a target='blank' href="/Orion/Report.aspx?ReportID={{current.ID}}">{{Ext.util.Format.htmlEncode(current.Title)}}</a></span></td>
       <td class = 'buttonColumn'></td>
       <td class='deleteReport btn buttonColumn'><img alt="" src='/Orion/images/delete_icon_gray_16x16.png' /></td> 
    </tr>       
    <tr class='empty'><td colspan='3'>&nbsp;</td></tr>

     {# }); #}
    
    {# _.each(urls, function(current, index) { #}
        
    <tr class='selectedUrl'>
       <td class="urlAddress"><input type="text" class="typedURL" id="url" value="{{current}}"/></td>
       <td class = 'buttonColumn'><a target='blank' href="{{current}}"><img alt="" src='/Orion/images/Show_Last_Note_icon16x16.png' style='cursor: pointer; margin:0 5px -3px 5px;'></a></td>
       <td class='deleteUrl btn buttonColumn'><img alt="" src='/Orion/images/delete_icon_gray_16x16.png' /></td> 
    </tr>       
    <tr class='empty'><td colspan='3'>&nbsp;</td></tr>

     {# }); #}
    
    <tr class='addNextReport'> 
        <td colspan = '3'>
        <span id='urlErrors'></span>
        <div style='width:100%;'>
            <span class='noReportInfo'>&nbsp;</span>
            <span>
                <orion:LocalizableButton runat='server' ID='addReport' LocalizedText='CustomText' Text='<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VL0_33 %>' DisplayType="Secondary" CausesValidation='false' OnClientClick='SW.Orion.ReportPicker.selectReportDialog(); return false;' />
            	<orion:LocalizableButton style="margin-left: 10px;" runat='server' ID='assignWebpage' LocalizedText='CustomText' Text='<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_61 %>' DisplayType="Secondary" CausesValidation='false' OnClientClick='SW.Orion.ReportPicker.assignWebPage();return false; ' />
           </span>
            <span class="rightScheduleInfo ">&nbsp;</span>
        </div>            
        </td>
    </tr>

    </table>
</script>
<script id="noReportsTemplate" type="text/x-template">
    <table id ='selectedReports'>
    <tr class='addNextReport' style='background-color: #ececec; border: none;'> 
        <td colspan = '3'>
            <div style='width:100%; float: left;'>
                <span class='noReportInfo'><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_57) %></span>
                <span>
                    <orion:LocalizableButton runat='server' ID='addAnotherReport' LocalizedText='CustomText' Text='<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VL0_34 %>' DisplayType="Primary" CausesValidation='false' OnClientClick='SW.Orion.ReportPicker.selectReportDialog(); return false;' />
                   <orion:LocalizableButton style="margin-left: 10px;" runat='server' ID='assignAnotherWebpage' LocalizedText='CustomText' Text='<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_61 %>' DisplayType="Secondary" CausesValidation='false' OnClientClick='SW.Orion.ReportPicker.assignWebPage();return false; ' />
            </span>
            <span class="rightScheduleInfo ">&nbsp;</span>
            </div>            
        </td>
    </tr>
    </table>
</script>
<script type="text/javascript">
    $(function() {
        function checkUrl(value) {
            var urlregex = new RegExp("^(http|https)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*($|[a-zA-Z0-9\/\.{}\:\,\?\'\\\+&amp;%\$#@\=~_\-]+)$");
            var escapedUrl = value.replace("(", "%28").replace(")", "%29");
            if (urlregex.test(escapedUrl)) {
                return true;
            }
            return false;
        }
        
        function checkAllUrls() {
            var errorFounded = false;
            
            $.each($("#selectedReports .selectedUrl td:first-child input"), function() {
                var url = $(this).val();
                if (url != null && url != '' && !checkUrl(url)) {
                    errorFounded = true;
                    $(this).css('background-color', '#FCBDBD');
                }
                else
                    $(this).css('background-color', '#FFFFFF');
            });
            
            if (errorFounded == true) {
                $("#urlErrors").text('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TK0_59) %>');
                $("#urlErrors").addClass("urlErrors");
            } else {
                $("#urlErrors").removeClass("urlErrors");
                $("#urlErrors").text('');
            }
        }
        
        var saveUrls = function() {
            checkAllUrls();
             var selectedUrls = [];
             $.each($("#selectedReports .selectedUrl td:first-child input"), function() {
                 $(this).closest(".selectedUrl").find(".buttonColumn a").attr("href",$(this).val());
                 selectedUrls.push($(this).val());
             });

         var func = <%= DefaultSanitizer.SanitizeHtml(string.IsNullOrEmpty(OnUpdateTypedUrls) ? "undefined" : OnUpdateTypedUrls) %>;
          if( func && typeof func === "function" )
            func(selectedUrls);

         showHidePreviewColumn($("#selectedReports .selectedUrl"));
         return selectedUrls;
     };

        function showHidePreviewColumn(urls) {
            if (urls.length == 0) {
                $($("#selectedReports .tableHeader .buttonColumn")[0]).text("");
            } else {
                $($("#selectedReports .tableHeader .buttonColumn")[0]).text("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_141) %>");
            }
        }

        var renderReports = function(reports) {
         var template;
         var urls = [];
         var func = <%= DefaultSanitizer.SanitizeHtml(string.IsNullOrEmpty(GetTypedUrlsFn) ? "undefined" : GetTypedUrlsFn) %>;
         if( func && typeof func === "function" )
            urls = $.parseJSON(func());

         if (urls == null || urls.length == 0) {
             $.each($("#selectedReports .selectedUrl td:first-child input"), function() {
                 urls.push($(this).val());
             });
         }

        if ((reports && reports.length > 0) || (urls && urls.length > 0)) {
            template = $('#reportsTemplate').html();
        } else {
           template = $('#noReportsTemplate').html();
        }
         
        var html = $(_.template(template, {reports: reports, urls: urls } ));

         var tblReconstruction = function() {
             var selectedReports = [];
             var selectedUrls = saveUrls();
             
             $.each($("#selectedReports .selectedReport td:first-child span"), function() {
                 if ($(this).attr("dataRow")) {
                     selectedReports.push({ "ID": $(this).attr("dataRow"), "Title": $(this).text() });
                 }
             });
             
             if (selectedReports.length == 0) {
                 renderReports(selectedReports);
             } else {
             var func = <%= string.IsNullOrEmpty(OnUpdateSelectedReports) ? "undefined" : OnUpdateSelectedReports%>;
             if (func && typeof func === "function")
                 func(selectedReports,selectedUrls);
             }
             showHidePreviewColumn($("#selectedReports .selectedUrl"));
         };

        html.delegate(".deleteReport", "click", function() {
            $(this).closest(".selectedReport").next().remove();
            $(this).closest(".selectedReport").remove();

            tblReconstruction();
        });
         
        html.delegate(".deleteUrl", "click", function() {
            $(this).closest(".selectedUrl").next().remove();
            $(this).closest(".selectedUrl").remove();

            tblReconstruction();
        });

            SW.Orion.ReportPicker.assignWebPage = function() {

                var reportCount = $("#selectedReports .selectedReport").length;
                var urlCount = $("#selectedReports .selectedUrl").length;

                var newUrl;
                newUrl = "<tr class='selectedUrl'>";
                newUrl += "<td class='urlAddress'><input class='typedURL' id='url' type=\"text\" value=\"\"/></td>";
                newUrl += "<td class='buttonColumn'><a target='blank' href=\"\"><img alt='' src='/Orion/images/Show_Last_Note_icon16x16.png' style='cursor: pointer; margin:0 5px -3px 5px;'></a></td>";
                newUrl += "<td class='deleteUrl btn buttonColumn'><img alt=\"\" src='/Orion/images/delete_icon_gray_16x16.png' /></td>";
                newUrl += "</tr>";
                newUrl += "<tr class='empty'><td colspan='3'>&nbsp;</td></tr>";

                $("#selectedReports tr:last-child").before(newUrl);

                if (reportCount == 0 && urlCount == 0) {
                    renderReports([]);
                }
                showHidePreviewColumn($("#selectedReports .selectedUrl"));
            };

         html.delegate(".typedURL", "change", saveUrls);
        
         $("#selectedReportsList").html(html);

         checkAllUrls();

          <%if (!string.IsNullOrEmpty(OnUpdateSelectedReports)){ %>
                <%=OnUpdateSelectedReports%>(reports,urls);
         <% }%>
    };

    SW.Orion.ReportPicker.setSelectedReports = function(pickedReports) {
        $("#selectedReports .selectedReport").remove();
        SW.Orion.ReportPicker.setSelectedReportsIds(pickedReports, renderReports);
    };
    SW.Orion.ReportPicker.selectReportDialog = function() {
        $("#shceduleReportBox").show();
        $("#shceduleReportBox").dialog({ modal: true, draggable: true, resizable: false, position: ['center', 'top'], width: 1000, title: "<%= Title %>", dialogClass: 'ui-dialog-osx' });
        var getHeaderTitleFn = Ext.util.Format.htmlEncode("<%= GetHeaderTitleFn%>");
        if (typeof getHeaderTitleFn == "function")
            $("#headerTitle").html(getHeaderTitleFn());

        if ($("#sheduleReportsPicker").children().length == 0) {
            SW.Orion.ReportPicker.init({ SwisBasedReportsOnly: <%= SwisFederationInfo.IsFederationEnabled.ToString().ToLower() %> });
            $(SW.Core.Widgets.Button('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_28) %>', { type: 'secondary', id: 'cancelBtn' })).appendTo("#shceduleReportBox").css('float', 'right').css('margin', '10px').click(function() {
                $("#shceduleReportBox").dialog("close");
            });
            $(SW.Core.Widgets.Button("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_12) %>", { type: 'primary', id: 'scheduleSelectedReportsBtn' })).appendTo("#shceduleReportBox").css('float', 'right').css('margin', '10px').click(function() {
                if ($("#isReportDemoMode").length != 0) {
                    demoAction("Core_Scheduler_AssignToReport", this);
                    return;
                } 
                $("#shceduleReportBox").dialog("close");
                var pickedReports = SW.Orion.ReportPicker.getSelectedReports();
                <%if (!string.IsNullOrEmpty(OnReportsSelected))
                      {%>
                var onSelected = <%= OnReportsSelected%>;
                if (typeof onSelected == "function") {
                    onSelected(pickedReports);
                }
                <% } %>

                SW.Orion.ReportPicker.setSelectedReports(pickedReports);
            });
        }
        SW.Orion.ReportPicker.clearSelection();
        
        if ($(".selectedReport td:first-child span").length > 0) {
            var jsonResult = [];
            $.each($(".selectedReport td:first-child span"), function() {
                if ($(this).attr("dataRow")) {
                    jsonResult.push({ "ID": $(this).attr("dataRow"), "Title": $(this).text() });
                }
            });
            SW.Orion.ReportPicker.setSelectedReportsIds(jsonResult);
        }
        $("#shceduleReportBox").parent().css('margin-top', '7%');
    };
    });
</script>
