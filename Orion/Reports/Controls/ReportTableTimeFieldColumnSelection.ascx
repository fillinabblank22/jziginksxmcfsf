﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReportTableTimeFieldColumnSelection.ascx.cs" Inherits="Orion_Reports_Controls_ReportTableTimeFieldColumnSelection" %>
<orion:Include ID="Include1" runat="server" File="Reports/js/ReportTableTimeFieldColumnSelection.js" />
<style type="text/css">
    .helpfulText {
        color: #5f5f5f;
        font-size: 10px;
        text-align: left;
    }
</style>

<asp:HiddenField id="timeFieldSelected" runat="server" />
<table id="<%= GetSaveUniqueID(UniqueID) %>" style="width:100%;">
    <tbody>
        <tr>
            <td style="width: 14em;"><h3><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_40) %></h3></td>
        </tr>
        <tr>
            <td colspan="2">
                <img src="/Orion/images/Reports/clock_time_period.png" style="margin:0px 4px;" /> 
                    <select name="<%= GetSaveUniqueID(UniqueID) + "-timeFieldList" %>" class="timeFieldList"></select>
                    &nbsp;<asp:CustomValidator ID="TimeFieldValidator" Display="Static" ClientValidationFunction="TimeFieldValidator_Validate" OnServerValidate="TimeFieldValidator_Validate" runat="server" ><img src="../images/warning_16x16.gif" id="TimeFieldValidatorImg" /> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_41) %></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="helpfulText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_42) %></td>
        </tr>
    </tbody>
</table>

<script type="text/javascript">
    function TimeFieldValidator_Validate(sender, args) {
        if(<%= (!ValidationEnabled).ToString().ToLowerInvariant() %>)
        {
            args.IsValid = true;
            return;
        }
        
        var isCustomSQLDataSource = eval("<%= IsCustomSQLDataSource %>".toLowerCase());
        // Todo: Here I will be also able to display warning icon
        $selectCombo = $("#" + "<%= GetSaveUniqueID(UniqueID) %>" + " select.timeFieldList");
        // todo: temp solution for FB226304
        var timeFieldAvailable = $('#<%= GetSaveUniqueID(UniqueID) %> select.timeFieldList option').length > 1;
        args.IsValid = $selectCombo.val() != "selectField" || !timeFieldAvailable;
    }

    $(function() {
        var timeFieldColumnSelection = new SW.Core.TableResource.TimeFieldColumnSelection({
                parentElId: "<%= GetSaveUniqueID(UniqueID) %>",
                selectionHiddenField: $("#" + "<%= timeFieldSelected.ClientID %>"),
                timefieldRefId: $("#" + "<%= timeFieldSelected.ClientID %>").val(),
                isCustomSQLDatasource: eval("<%= IsCustomSQLDataSource %>".toLowerCase())
            });

        var timeFieldColumnCreatedCallback = <%= this.OnCreatedJS %>;
        if (timeFieldColumnCreatedCallback && jQuery.isFunction(timeFieldColumnCreatedCallback)) {
            timeFieldColumnCreatedCallback(timeFieldColumnSelection);
        }
    });
</script>