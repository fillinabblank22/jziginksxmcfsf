﻿using SolarWinds.Orion.Web.UI;
using SolarWinds.Reporting.Models.Selection;
using SolarWinds.Reporting.Models.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_Reports_Controls_ReportTablePresenterEdit : ModelBasedControl<TableColumn[]>  
{
    public DataSource DataSource { get; set; }

    public override void LoadViewModel()
    {
        var columns = new List<TableColumn>();
        foreach (RepeaterItem item in rptPresenters.Items)
        {
            var columnRefId = (HiddenField)item.FindControl("columnRefId");
            var columnId = new Guid(columnRefId.Value);
           
            var Presenters = (Orion_Reports_Controls_ReportPresenterConfigControl)item.FindControl("Presenters");

            Presenters.ColumnPrefix = columnId.ToString();


            TableColumn column = new TableColumn() { Presenters = Presenters.ViewModel, RefId = columnId };
                columns.Add(column);
            
        }

        ViewModel = columns.ToArray();
        
    }

    protected override void BindViewModel()
    {
        base.BindViewModel();

        rptPresenters.ItemCreated += rptPresenters_ItemCreated;
        rptPresenters.DataSource = ViewModel;
        rptPresenters.DataBind();
    }

    protected void rptPresenters_ItemCreated(object sender, RepeaterItemEventArgs e)
    {
        var dataItem = (TableColumn)e.Item.DataItem;
        var columnRefId = (HiddenField)e.Item.FindControl("columnRefId");
        columnRefId.Value = dataItem.RefId.ToString();
       
        var Presenters = (Orion_Reports_Controls_ReportPresenterConfigControl)e.Item.FindControl("Presenters");
        Presenters.ColumnPrefix = dataItem.RefId.ToString();
        Presenters.Field = dataItem.Field;
        Presenters.Source = DataSource;
        Presenters.ViewModel = dataItem.Presenters;       
    }
}