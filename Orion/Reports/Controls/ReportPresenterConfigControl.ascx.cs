﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Text.RegularExpressions;

using SolarWinds.Reporting.Models.Presentation;
using SolarWinds.Reporting.Models.Selection;
using SolarWinds.Reporting.Impl.Presentation;
using SolarWinds.Reporting;
using SolarWinds.Reporting.Models.Data;
using SolarWinds.Orion.Core.Reporting;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Reporting.Rendering;
using SolarWinds.Orion.Core.Common.i18n;

public partial class Orion_Reports_Controls_ReportPresenterConfigControl : ModelBasedControl<PresenterRef[]>
{
	private RenderContext _renderContext = new RenderContext();
	private Dictionary<string, Tuple<DataPresenter, IDataPresenter>> _instances;
	private IEnumerable<PresenterRef> _orderdInstances;

	public string ColumnPrefix { get; set; }
	public DataSource Source { get; set; }
	public Field Field { get; set; }

	public override void LoadViewModel()
	{
		base.LoadViewModel();

		if (CurrentViewModel == null)
		{
			ViewModel = TryUpdateModel();
		}
	}

	protected override void BindViewModel()
	{
		base.BindViewModel();
	}

	public void ForceMergeModel(PresenterRef[] model)
	{
		ViewModel = model;
		ViewModel = TryUpdateModel();
	}

	string GeneratePrefix(params string[] blah)
	{
		string ret = string.Empty;

		foreach (string s in blah)
		{
			if (ret.Length != 0)
				ret += "_";

			ret += s;
		}

		return ret;
	}

	protected override void OnPreRender(EventArgs e)
	{
		base.OnPreRender(e);

		if (this.ViewModel == null ||
			ColumnPrefix == null ||
			Source == null ||
			Field == null)
			return;

		_instances = PresenterInstances();
		_orderdInstances = Ordered(_instances, ViewModel);

		foreach (var pref in _orderdInstances)
		{
			Tuple<DataPresenter, IDataPresenter> found;

			if (_instances.TryGetValue(pref.PresenterId, out found) == false || found == null || found.Item2 == null)
				continue;

			found.Item2.PreRenderInitialize(_renderContext, true);
		}

		OrionInclude.CoreFile("ReportTableColumnLayout.css");
	}

	protected override void Render(HtmlTextWriter writer)
	{
		if (this.ViewModel == null ||
			ColumnPrefix == null ||
			Source == null ||
			Field == null)
			return;

		int pos = 0;
		foreach (var pref in _orderdInstances)
		{
			Tuple<DataPresenter, IDataPresenter> found;

			if (_instances.TryGetValue(pref.PresenterId, out found) == false || found == null)
				continue; // let's just skip it so it's retained.

			string id = GeneratePrefix(ColumnPrefix, "p" + pos);

			if (found.Item2 == null)
			{
				RenderIncompatiblePresenter(writer, found.Item1, pref, id);
			}
			else
			{
				RenderKnownPresenter(writer, found.Item1, found.Item2, pref, id);
			}

			++pos;
		}

		var compatibles = ComputeCompatibles(_instances);

		var options = compatibles.
			Where(x => x.Item2 != null)
			.Select(x => new
			{
				Text = DisplayLabel(x.Item1),
				Value = x.Item1.PresenterId
			}).OrderBy(x => x.Text);

		if (options.Any())
		{
			writer.WriteLine("<tr class='sw-presenter-addNew'><td>{0}</td><td>", Resources.CoreWebContent.WEBDATA_ZT0_35);

			string onChangeJS = @"var $that = $(this); if(!$that || !$that.val()) return; $that.attr('name', $that.attr('name').replace( /_want$/, '_add' )); $(form).submit(); return false;";

			writer.WriteLine("<select name='{0}_want' onchange=\"{1}\" >", HttpUtility.HtmlAttributeEncode(ColumnPrefix), onChangeJS);
			writer.WriteLine("<option value='{0}'>{1}</option>", "", HttpUtility.HtmlAttributeEncode(Resources.CoreWebContent.WEBCODE_PCC_51));

			foreach (var option in options)
			{
				writer.WriteLine("<option value='{0}'>{1}</option>",
					HttpUtility.HtmlAttributeEncode(option.Value),
					HttpUtility.HtmlAttributeEncode(option.Text));
			}

			writer.WriteLine("</select>");

			writer.WriteLine("</td><td>&nbsp;</td></tr>");
		}
	}

	IEnumerable<PresenterRef> Ordered(Dictionary<string, Tuple<DataPresenter, IDataPresenter>> all, IEnumerable<PresenterRef> tosort)
	{
		Func<PresenterRef, string> fnGetText = delegate(PresenterRef pref)
		{
			return all.ContainsKey(pref.PresenterId) ?
				TokenSubstitution.Parse(all[pref.PresenterId].Item1.DisplayNameToken) :
				string.Empty;
		};

		return tosort
			.Where(x => x != null && x.PresenterId != null)
			.Select(x => new { text = fnGetText(x), pref = x })
			.OrderBy(x => x.text)
			.Select(x => x.pref)
			.ToArray();
	}

	string DisplayLabel(DataPresenter dp)
	{
		if (dp.CategoryToken == null)
			return TokenSubstitution.Parse(dp.DisplayNameToken ?? string.Empty);

		return TokenSubstitution.Parse(dp.CategoryToken) + " - " + TokenSubstitution.Parse(dp.DisplayNameToken ?? string.Empty);
	}

	string GetNameOverride(PresenterRef pref)
	{
		var name =
			(pref.Values ?? Enumerable.Empty<ContextValue>()).FirstOrDefault(
				x => StringComparer.OrdinalIgnoreCase.Equals(x.Name, "NameOverride"));

		if (name == null)
			return null;

		if (string.IsNullOrWhiteSpace(name.Value))
			return null;

		return name.Value;
	}

	void RenderKnownPresenter(HtmlTextWriter writer, DataPresenter factoryInfo, IDataPresenter presenter, PresenterRef pref, string idPrefix)
	{
		if (pref.Values != null)
			presenter.Load(pref.Values);

		var presenterWithHelpURL = presenter as IDataPresenterHelpURLProvider;
		if (presenterWithHelpURL != null)
		{
			presenterWithHelpURL.HelpURL = HelpHelper.GetHelpUrl(presenterWithHelpURL.HelpURLFragment);
		}

		string nameOverride = GetNameOverride(pref);

		RenderPresenterStart(writer, nameOverride ?? factoryInfo.DisplayNameToken, idPrefix);
		RenderHiddenValue(writer, idPrefix + "_pid", pref.PresenterId);
		if (nameOverride != null)
			RenderHiddenValue(writer, idPrefix + "_NameOverride", nameOverride);

		bool bRet = presenter.RenderEditUI(_renderContext, writer, idPrefix);

		if (!bRet)
		{
			// no edit ui? we'll render the variables into the page as hidden
			RenderPresenterRefDetails(writer, pref, idPrefix, false);
		}

		RenderPresenterEnd(writer, idPrefix, factoryInfo.PresenterId);
	}

	void RenderPresenterRefDetails(HtmlTextWriter writer, PresenterRef pref, string idPrefix, bool doPid)
	{
		if (doPid)
			RenderHiddenValue(writer, idPrefix + "_pid", pref.PresenterId);

		foreach (var val in pref.Values ?? Enumerable.Empty<ContextValue>())
		{
			RenderHiddenValue(writer, idPrefix + "_" + val.Name, val.Value);
		}
	}

	void RenderIncompatiblePresenter(HtmlTextWriter writer, DataPresenter factoryInfo, PresenterRef pref, string idPrefix)
	{
		RenderPresenterStart(writer, GetNameOverride(pref) ?? factoryInfo.DisplayNameToken, idPrefix);
		RenderPresenterRefDetails(writer, pref, idPrefix, true);

		writer.Write("<div class='sw-suggestion sw-suggestion-warn'><span class='sw-suggestion-icon'></span>");
		writer.WriteEncodedText(Resources.CoreWebContent.WEBDATA_SO0_127);
		writer.Write("</div>");

		RenderPresenterEnd(writer, idPrefix);
	}

	void RenderPresenterStart(HtmlTextWriter writer, string displayToken, string idPrefix)
	{
		writer.Write("<tr class='sw-presenter-row' id='" + idPrefix + "'><td>");

		writer.Write(Resources.CoreWebContent.WEDDATA_ZT0_36);

		writer.WriteLine("</td><td class='sw-presenter-label'>");

		writer.Write(TokenSubstitution.Parse(displayToken));

		writer.WriteLine("</td><td><a id='" +
		 HttpUtility.HtmlAttributeEncode(idPrefix + "_x") +
		 "' class='sw-presenter-row-del' href='#' onclick='return false;'><img src='/Orion/images/Reports/delete_icon16x16.png'></a>");

		writer.Write("</td></tr><tr class='sw-presenter-row'><td></td><td class='sw-presenter-edit'>");
	}

	void RenderPresenterEnd(HtmlTextWriter writer, string idPrefix, string presenterId = "")
	{
		writer.WriteLine("</td><td></td></tr><tr class='sw-presenter-addNew-separator' />");

		_renderContext.IncludeJavascriptContent(
string.Format(@"SW.Core.namespace('SW._pg').HookPresenterDelete = function(id){{

    $('#'+id+'_x').click(function(){{
       var inp = $('#'+id).next().find('[data-form=""action""]')[0];
       if( !inp ) return;

       if( confirm('{0}') ){{
            inp.name = inp.name.replace( /_pid$/, '_delete' );
            inp.form.submit('');
        }}
    }});
}};", Resources.CoreWebContent.WEBDATA_SO0_128));

		_renderContext.IncludeJavascriptContent("$(function(){ SW._pg.HookPresenterDelete('" + HttpUtility.JavaScriptStringEncode(idPrefix) + "'); });");
	}

	void RenderHiddenValue(HtmlTextWriter writer, string id, string value)
	{
		writer.Write("<input data-form='action' type='hidden' name='");
		writer.WriteEncodedText(id);
		writer.Write("' value='");
		writer.WriteEncodedText(value ?? string.Empty);
		writer.WriteLine("'  />");
	}

	private ApplicationType GetAppType(Field field)
	{
		if (field == null || field.DataTypeInfo == null)
			return null;

		string appid = field.DataTypeInfo.ApplicationType;

		ApplicationType ret;

		if (ApplicationTypeRepository.TryGetDefinition(appid, out ret) == false)
			return null;

		return ret;
	}


    IApplicationTypeRepository ApplicationTypeRepository
    {
        get { return ReportingRepository.GetInstance<IApplicationTypeRepository>(); }
    }

    IDataPresenterRepository PresenterReportistory
    {
        get { return ReportingRepository.GetInstance<IDataPresenterRepository>(); }
    }

	Dictionary<string, Tuple<DataPresenter, IDataPresenter>> PresenterInstances()
	{
        var app = GetAppType(Field);
		if ((Source.Type == DataSourceType.CustomSQL) || (app == null))
		{
			return PresenterReportistory.Presenters
										  .Select(
											  x =>
											  new
												  {
													  p = x,
													  id = x.PresenterId,
													  obj = x.Factory.NewInstance(Source, Field, null)
												  })
										  .ToDictionary(x => x.id,
														x => new Tuple<DataPresenter, IDataPresenter>(x.p, x.obj),
														StringComparer.OrdinalIgnoreCase);
		}


		return PresenterReportistory.Presenters.Where(x => x != null && x.PresenterId != null &&
					(((app.AllowedPresentersId == null || !app.AllowedPresentersId.Any() ||
					app.AllowedPresentersId.Any(item => item == x.PresenterId)) &&
					(app.DisallowedPresentersId == null || app.DisallowedPresentersId.All(item => item != x.PresenterId)))))
					.Select(
											  x =>
											  new
												  {
													  p = x,
													  id = x.PresenterId,
													  obj = x.Factory.NewInstance(Source, Field, null)
												  })
										  .ToDictionary(x => x.id,
														x => new Tuple<DataPresenter, IDataPresenter>(x.p, x.obj),
														StringComparer.OrdinalIgnoreCase);
	}

	IEnumerable<Tuple<DataPresenter, IDataPresenter>> ComputeCompatibles(Dictionary<string, Tuple<DataPresenter, IDataPresenter>> all)
	{
		var m = (ViewModel ?? Enumerable.Empty<PresenterRef>()).Where(x => x != null);

		var haveids = m.Select(x => x.PresenterId);

		// look at exclusivity and remove any presenter already added that's declared itself exclusive

		HashSet<int> blacklist = new HashSet<int>();

		int exclusive = (int)PresentationOrder.Exclusive;
		int strip = ~exclusive;

		foreach (var kvp in all.Where(
			x => haveids.Contains(x.Key) &&
			x.Value.Item2 != null &&
			(x.Value.Item2.RenderOrder & exclusive) != 0))
		{
			blacklist.Add(kvp.Value.Item2.RenderOrder);
			blacklist.Add(kvp.Value.Item2.RenderOrder & strip);
		}

		return all.Where(x => x.Value.Item2 != null &&
			blacklist.Contains(x.Value.Item2.RenderOrder) == false)
			.Select(x => x.Value)
			.ToArray();
	}

	bool _loaded = false;

	private PresenterRef[] TryUpdateModel()
	{
		var m = (CurrentViewModel ?? Enumerable.Empty<PresenterRef>())
			.Where(x => x != null && x.PresenterId != null);

		var keys = Request.Form.AllKeys.Where(x => x != null && x.StartsWith(ColumnPrefix));

		var addreq = Request.Form[ColumnPrefix + "_add"];

		if (string.IsNullOrWhiteSpace(addreq) == false)
		{
			var dp = PresenterReportistory.GetRef(addreq);

			if (dp != null)
			{
				var newp = new PresenterRef { PresenterId = addreq, Values = dp.Config };
				m = m.Concat(new[] { newp });
			}
		}

		// locate all the presenter ids.

		List<PresenterRef> found = new List<PresenterRef>();
		List<string> deletes = new List<string>();

		Regex rx = new Regex("^" + Regex.Escape(ColumnPrefix) + "_p[0-9]+_(?:pid|delete)$", RegexOptions.IgnoreCase);

		foreach (var key in keys.Where(x => rx.IsMatch(x)))
		{
			string pid = Request.Form[key];

			if (string.IsNullOrWhiteSpace(pid))
				continue;

			if (key.EndsWith("delete", StringComparison.OrdinalIgnoreCase))
			{
				deletes.Add(Request.Form[key]);
				continue;
			}

			string pre = key.Substring(0, key.Length - 3);

			PresenterRef r = new PresenterRef
			{
				PresenterId = pid,
				Values = keys.Where(x => x.StartsWith(pre) && x.EndsWith("pid") == false)
					.Select(x => new ContextValue
				{
					Name = x.Substring(pre.Length),
					Value = Request.Form[x]
				}).ToArray()
			};

			found.Add(r);
		}

		// now update the supplied model if present.

		var uploaded = found.Select(x => x.PresenterId);

		var ret = m.Where(x =>
			deletes.Contains(x.PresenterId) == false &&
			uploaded.Contains(x.PresenterId) == false)
			.Concat(found)
			.ToArray();

		return ret;
	}
}
