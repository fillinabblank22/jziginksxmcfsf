﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Reporting.Models.Charts;
using SolarWinds.Reporting.Models.Selection;


public partial class Orion_Reports_Controls_EditAxisPropertiesControl : ModelBasedControl<YAxis>
{
    private const string CUSTOM_UNIT_KEY = "custom";

    private readonly string DummyJsAction = "function(){}";

    public string OnCreatedJS { get; set; }

    public enum ScaleMaxEnum
    {
        AutoScale = 0,
        Fixed,
    }

    public enum VisibleModeEnum
    {
        Left = 0,
        Right,
    }

    public string AxisName { get; set; }
    public string AxisSubName { get; set; }
    public VisibleModeEnum Mode { get; set; }
    public string DetailsHiddenField { get; set; }
    public string DataDetails { get; set; }
    public string BindDataJsFunction { get; set; }

    private static Log log = new Log();

    public DataSource DataSource { get; set; }

    public Orion_Reports_Controls_EditAxisPropertiesControl()
    {
        OnCreatedJS = DummyJsAction;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        
        ddScaleMax.Items.AddRange(new[]
            {
                new ListItem(Resources.CoreWebContent.WEBDATA_TP0_37, ((int)(ScaleMaxEnum.AutoScale)).ToString()),
                new ListItem(Resources.CoreWebContent.WEBDATA_TP0_38, ((int)(ScaleMaxEnum.Fixed)).ToString()),
            });

        ddChartFormats.Items.AddRange(new[] {
            new ListItem(Resources.CoreWebContent.WEBDATA_TP0_24, ((int)(SeriesType.Line)).ToString()),
            new ListItem(Resources.CoreWebContent.WEBDATA_TP0_27, ((int)(SeriesType.Column)).ToString()),
            new ListItem(Resources.CoreWebContent.WEBDATA_TP0_25, ((int)(SeriesType.Area)).ToString()),
            new ListItem(Resources.CoreWebContent.WEBDATA_TP0_31, ((int)(SeriesType.StackedLine)).ToString()),
            new ListItem(Resources.CoreWebContent.WEBDATA_TP0_33, ((int)(SeriesType.StackedColumn)).ToString()),
            new ListItem(Resources.CoreWebContent.WEBDATA_TP0_30, ((int)(SeriesType.StackedArea)).ToString()),
            new ListItem(Resources.CoreWebContent.WEBDATA_TP0_28, ((int)(SeriesType.MinMax)).ToString()),
        
        });

        FillInUnitsList();

        fieldPicker.OnCreatedJs = "controler" + this.ClientID + ".onFieldPickerCreated";
        fieldPicker.OnSelectedJs = "controler" + this.ClientID + ".onFieldPickerSelected";
    }

 

    public override void LoadViewModel()
    {
        base.LoadViewModel();

        DataSeriesConfiguration[] dataSeries = null;

        try
        {
            dataSeries = JsonConvert.DeserializeObject<DataSeriesConfiguration[]>(hfDataSeries.Value);
            SanitizeDataSeriesConfiguration(dataSeries);
        }
        catch (Exception e)
        {
            log.Error("Not able to deserialize hidden field with Data series", e);
        }

        int maxValue = 0;
        if (!Int32.TryParse(tbMaxScale.Text, out maxValue))
        {
            log.Info("Provided value for Scale max is not an integer");
        }

        ViewModel = new YAxis()
        {
            DisplayName = WebSecurityHelper.SanitizeHtmlV2(tbCustomLabel.Text),
            ScaleMax = (ScaleMaxEnum)(Enum.Parse(typeof(ScaleMaxEnum), ddScaleMax.SelectedValue)) == ScaleMaxEnum.AutoScale ? "0" : maxValue.ToString(),
            //TODO store firstly selected data series
            Unit = ddUnitsDisplayed.SelectedValue == CUSTOM_UNIT_KEY 
                ? WebSecurityHelper.SanitizeHtmlV2(tbCustomUnit.Text)
                : ddUnitsDisplayed.SelectedValue,
            DataSeries = dataSeries,
            ShowSumOfDataSeries = cbShowSumOfCalculatedSeries.Checked,
            SeriesType = (SeriesType)(Enum.Parse(typeof(SeriesType), ddChartFormats.SelectedValue)),
        };
    }

    private void SanitizeDataSeriesConfiguration(DataSeriesConfiguration[] configuration)
    {
        if (configuration == null)
            return;

        foreach (var series in configuration)
        {
            if (series?.DisplayName != null)
                series.DisplayName = WebSecurityHelper.SanitizeHtmlV2(series.DisplayName);

            if (series?.Field?.DisplayName != null)
                series.Field.DisplayName = WebSecurityHelper.SanitizeHtmlV2(series.Field.DisplayName);

            if (series?.TimeField?.DisplayName != null)
                series.TimeField.DisplayName = WebSecurityHelper.SanitizeHtmlV2(series.TimeField.DisplayName);
        }
    }

    protected override void BindViewModel()
    {
        fieldPicker.DataSource = DataSource;

        tbCustomLabel.Text = ViewModel.DisplayName;
        int scaleMax = 0;
        Int32.TryParse(ViewModel.ScaleMax, out scaleMax);

        ddScaleMax.SelectedValue = scaleMax == 0
                                       ? ((int) (ScaleMaxEnum.AutoScale)).ToString()
                                       : ((int) (ScaleMaxEnum.Fixed)).ToString();
        tbMaxScale.Text = ViewModel.ScaleMax;
        
        // TODO bind to first selected data series
      
        if (ddUnitsDisplayed.Items.FindByValue(ViewModel.Unit) != null)
        {
            ddUnitsDisplayed.SelectedValue = ViewModel.Unit;
            tbCustomUnit.Text = string.Empty;
            tbCustomUnit.Attributes["style"] = "display:none";
        }
        else
        {
            ddUnitsDisplayed.SelectedValue = CUSTOM_UNIT_KEY;
            tbCustomUnit.Text = ViewModel.Unit;
            tbCustomUnit.Attributes["style"] = "display:inline";
        }


        ddChartFormats.SelectedValue = ((int) ViewModel.SeriesType).ToString();
        cbShowSumOfCalculatedSeries.Checked = ViewModel.ShowSumOfDataSeries;

        SetLongestNavigationPathForFieldPickers();

        try
        {
            SanitizeDataSeriesConfiguration(ViewModel.DataSeries);
            hfDataSeries.Value = WebSecurityHelper.SanitizeAngular(JsonConvert.SerializeObject(ViewModel.DataSeries,
               new JsonSerializerSettings { StringEscapeHandling = StringEscapeHandling.EscapeHtml }));
        }
        catch (Exception e)
        {
            log.Error("Not able to serialize data series to JSON", e);
        }
    }

    private void SetLongestNavigationPathForFieldPickers()
    {
        var fieldManager = new FieldPickerManager();

        var allFields = fieldManager.GetFilterFields(DataSource).Select(f => f.RefID.Data).ToArray();

        fieldPicker.NavigationPathFilterFields = allFields;
    }


    private void FillInUnitsList()
    {
        ddUnitsDisplayed.Items.Clear();
        // Empty
        ddUnitsDisplayed.Items.Add(new ListItem { Text = Resources.CoreWebContent.WEBDTA_ZT0_25, Value = "", Selected = true });
        // Bit/s (1024)
        ddUnitsDisplayed.Items.Add(new ListItem { Text = Resources.CoreWebContent.WEBDTA_ZT0_27, Value = "bps" });
        // Bit/s (1000)
        ddUnitsDisplayed.Items.Add(new ListItem { Text = Resources.CoreWebContent.WEBDTA_ZT0_29, Value = "bps1000" });
        // Bytes
        ddUnitsDisplayed.Items.Add(new ListItem { Text = Resources.CoreWebContent.WEBDTA_ZT0_21, Value = "bytes" });
        // Bytes/s (1024)
        ddUnitsDisplayed.Items.Add(new ListItem { Text = Resources.CoreWebContent.WEBDTA_ZT0_22, Value = "Bps" });
        // Bytes/s (1000)
        ddUnitsDisplayed.Items.Add(new ListItem { Text = Resources.CoreWebContent.WEBDTA_ZT0_30, Value = "Bps1000" });
        // Packets
        ddUnitsDisplayed.Items.Add(new ListItem { Text = Resources.CoreWebContent.WEBDTA_ZT0_23, Value = "packets" });
        // Packets/s
        ddUnitsDisplayed.Items.Add(new ListItem { Text = Resources.CoreWebContent.WEBDTA_ZT0_24, Value = "pps" });
        // Miliseconds
        ddUnitsDisplayed.Items.Add(new ListItem { Text = Resources.CoreWebContent.WEBDTA_ZT0_26, Value = "ms" });
        // Percent
        ddUnitsDisplayed.Items.Add(new ListItem { Text = Resources.CoreWebContent.WEBDTA_ZT0_28, Value = "%" });
        // Custom
        ddUnitsDisplayed.Items.Add(new ListItem { Text = Resources.CoreWebContent.WEBDTA_ZT0_20, Value = CUSTOM_UNIT_KEY });

    }
    
}
