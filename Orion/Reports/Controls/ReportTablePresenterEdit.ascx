<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReportTablePresenterEdit.ascx.cs" Inherits="Orion_Reports_Controls_ReportTablePresenterEdit" %>
<%@ Register Src="~/Orion/Reports/Controls/ReportPresenterConfigControl.ascx" TagPrefix="orion" TagName="Presenters" %>


<div class="presenterWrapper">
    <asp:Repeater ID="rptPresenters" runat="server">
        <ItemTemplate>
            <asp:HiddenField ID="columnRefId" runat="server" />
            <table style="display:none;" >
                <tbody class="jsTempTable" data-columnRefId="<%# DefaultSanitizer.SanitizeHtml(((SolarWinds.Reporting.Models.Tables.TableColumn)Container.DataItem).RefId) %>"> 
                    <orion:Presenters ID="Presenters" runat="server" /> 
                </tbody> 
            </table>
        </ItemTemplate>
    </asp:Repeater>
</div>
