<%@ Page Language="C#" MasterPageFile="~/Orion/Reports/ScheduleAdd/AddScheduleWizard.master"
    AutoEventWireup="true" CodeFile="GeneralSettings.aspx.cs" Inherits="Orion_Reports_Schedule_Add_GeneralSettings"
    Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AB0_6 %>" %>
<%@ Register TagPrefix="orion" TagName="FrequencyControl" Src="~/Orion/Controls/FrequencyControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headPlaceholder" runat="server">
    <orion:Include File="ReportSchedule.css" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="wizardContent" runat="Server">
    <asp:Panel ID="FreqPicker" runat="server" DefaultButton="imgbNext">
        <div class="GroupBox">
        <div style="padding: 30px;">
            <div class="wizardHeader" style="font-size: 16px">
            <h2 style="padding-top: 10px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_49) %></h2>
            </div>

            <div class="paragraph" style="padding-top: 10px">
            <span class="informText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_307) %></span>
            </div>
            <asp:HiddenField ID="jobGuid" runat="server" />
            <h2 style="padding-top: 20px; padding-bottom: 5px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_59) %></h2>
            <div id="timePeriod">
                <orion:FrequencyControl runat="server" ID="FrequencyControl"/>
            </div>
        </div>
        </div>
        <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton runat="server" ID="imgbBack" OnClick="Back_Click" 
                LocalizedText="Back" DisplayType="Secondary" CausesValidation="false" />
            <orion:LocalizableButton runat="server" ID="imgbNext" OnClientClick="return SW.Orion.FrequencyController.Validate();" OnClick="Next_Click" LocalizedText="Next" DisplayType="Primary" ValidationGroup="none"/>
            <orion:LocalizableButton runat="server" ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel"
                DisplayType="Secondary" CausesValidation="false" />
        </div>
    </asp:Panel>
        <script type="text/javascript">
            var frequency = <%= SchedulesListJson %>;
            if (frequency) {
                // decode DisplayName
                for (var i = 0; i < frequency.length; i++) {
                    frequency[i].DisplayName = $('<div>').html(frequency[i].DisplayName).text();
                }
            }
            SW.Orion.FrequencyControllerDefinition = new SW.Orion.FrequencyController({
                container: "#timePeriod",
                frequencies: frequency,
                useTimePeriodMode: false,
                singleScheduleMode: false,
            });
            SW.Orion.FrequencyControllerDefinition.init();
    </script>
</asp:Content>
