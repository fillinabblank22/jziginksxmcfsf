<%@ Page Language="C#" MasterPageFile="~/Orion/Reports/ScheduleAdd/AddScheduleWizard.master"
    AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_Reports_Schedule_Add_Default" 
    Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AB0_6 %>" %>
<%@ Register tagPrefix="orion" tagName="ReportPicker" src="~/Orion/Reports/Controls/ReportPicker.ascx" %>
<%@ Register TagPrefix="orion" TagName="RSAdvancedSettingsControl" Src="~/Orion/Controls/RSAdvancedSettingsControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headPlaceholder" runat="server">
    <orion:Include ID="Include1" File="ReportSchedule.css" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="wizardContent" runat="Server">
        <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js" />
     <asp:Panel ID="GeneralSettingsPanel" runat="server" DefaultButton="imgbNext">
        <div class="GroupBox" >
            <div style="padding: 20px;">
            <div class="scheduleGeneralSettings">
            <span style="font-weight: bold; font-size: 16px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_51) %></span>
            <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_52) %></p>
                <br/>
            <table>
                <tr><td><label class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_173) %></label></td></tr>
                <tr><td><asp:TextBox runat="server" Width="400" ID="Name" MaxLength="255"/><span style="margin: 0 10px;"><asp:RequiredFieldValidator runat="server" id="reqName" controltovalidate="Name" errormessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AB0_9 %>" /></span></td></tr>
                <tr><td><label class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_50) %></label></td></tr>
                <tr><td><textarea runat="server" style="width: 600px; max-width: 760px; border:1px solid #ABADB3; resize: both; border:1px solid #ABADB3;" rows="5" ID="Description"></textarea></td></tr>
            </table>
            </div>
            <div class="scheduleGeneralSettings">
            <label class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_10) %></label>
            <orion:ReportPicker runat="server"
                Title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AB0_13 %>"
                GetHeaderTitleFn="getHeaderTitle"
                GetTypedUrlsFn="getTypedUrls"
                RenderSelectedItems="True"
                OnReportsSelected="reportsSelected"
                OnUpdateSelectedReports="updateSelectedReports"
                OnUpdateTypedUrls="updateTypedUrls"/>
            <asp:HiddenField ID="reportsIds" runat="server" />
            <asp:HiddenField ID="urlFields" runat="server" />
            <asp:HiddenField ID="siteAddresses" runat="server" />
            <asp:HiddenField ID="selectWebsiteId" runat="server" />
            <asp:HiddenField ID="jobId" runat="server" />
            </div>
             <orion:RSAdvancedSettingsControl runat="server" ID="RSAdvancedSettingsControl" />
             </div>
         </div>
        <div class="sw-btn-bar-wizard">            
                <orion:LocalizableButton runat="server" ID="imgbNext" OnClick="Next_Click" OnClientClick="return validateUI(this);" LocalizedText="Next"
                DisplayType="Primary" />         
            <orion:LocalizableButton runat="server" ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel"
                DisplayType="Secondary" CausesValidation="false" />
        </div>
    </asp:Panel>

  <script type="text/javascript">
      var reportsAssignedCount = 0;
      var getHeaderTitle = function () {
          return String.format("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_5) %>", htmlEncode($("#" + "<%= Name.ClientID %>").val()));
      };
      var reportsSelected = function (pickedReports) {
          $("#" + "<%=reportsIds.ClientID %>").val(JSON.stringify(pickedReports));
      };
      var updateTypedUrls = function (typedUrls) {
          $("#" + "<%=urlFields.ClientID %>").val(JSON.stringify(typedUrls));
      };
      var getTypedUrls = function() {
          return $("#" + "<%=urlFields.ClientID %>").val();
      };
      var updateSelectedReports = function (selectedReports, selectedUrls) {
          reportsAssignedCount = selectedReports.length;
          $("#" + "<%=reportsIds.ClientID %>").val(JSON.stringify(selectedReports));
          $("#" + "<%=urlFields.ClientID %>").val(JSON.stringify(selectedUrls));
      };
      var validateUI = function (button) {
          if ($(button).attr("confirmed")) {
              $(button).attr("confirmed", false);
              return true;
          }

          if ($("#" + "<%=reportsIds.ClientID %>").val() == "" && $("#" + "<%= jobId.ClientID %>").val() == "" && $("#" + "<%=urlFields.ClientID %>").val() == "") {
              Ext.Msg.show({ title: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_7) %>", msg: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_8) %>", minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
              return false;
          } else if ($("#" + "<%=urlFields.ClientID %>").val() != "") {
              var isHttpsUsed = window.location.protocol == 'https:';
              var isAssignedUrlIncorrect = false;
              var urls = JSON.parse($("#" + "<%=urlFields.ClientID %>").val());
              var parser = document.createElement('a');
              var windowLocationOrigin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
              var parserOrigin;

              $.each(urls, function (index, url) {
                  parser.href = url;
                  parserOrigin = parser.protocol + "//" + parser.hostname + ((parser.port && !(parser.protocol == 'https:' && parser.port == 443) && !(parser.protocol == 'http:' && parser.port == 80)) ? ':' + parser.port : '');
                  if (parserOrigin != (windowLocationOrigin) && (isHttpsUsed || parser.protocol == 'https:')) {
                      isAssignedUrlIncorrect = true;
                      return false;
                  }
              });

              var isAssignedWebsiteIncorrect = false;
              var siteAddresses = JSON.parse($("#" + "<%=siteAddresses.ClientID %>").val());
              var selectedWebsiteId = $("#" + $("#" + "<%=selectWebsiteId.ClientID %>").val()).val();
              var selectedSiteAddress = '';

              if (siteAddresses) {
                  var websiteCount = Object.keys(siteAddresses).length;
                  selectedSiteAddress = (websiteCount == 1) ? siteAddresses[Object.keys(siteAddresses)[0]] : siteAddresses[selectedWebsiteId];
                  isAssignedWebsiteIncorrect = selectedSiteAddress.toLowerCase() != windowLocationOrigin.toLowerCase() && (isHttpsUsed || selectedSiteAddress.toLowerCase().indexOf("https") >= 0);
              }

              if (!isAssignedUrlIncorrect && !isAssignedWebsiteIncorrect) return true;


              var messageAssignedUrl = String.format("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_88) %>", windowLocationOrigin, parserOrigin);
              var messageAssignedWebsite = String.format("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_89) %>", windowLocationOrigin, selectedSiteAddress);
              var message = String.format("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_90) %>", SW.Core.KnowledgebaseHelper.getKBUrlWithLang("2081"));
              var width = 810;

              if (isAssignedUrlIncorrect && isAssignedWebsiteIncorrect && reportsAssignedCount > 0) {
                  message = messageAssignedUrl + "<br/>" + messageAssignedWebsite + "<br/>" + message;
              }
              else if (isAssignedUrlIncorrect) {
                  message = messageAssignedUrl + "<br/>" + message;
                  width = 700;
              }
              else if (reportsAssignedCount > 0) {
                  message = messageAssignedWebsite + "<br/>" + message;
              }
              else
                  return true;

              Ext.Msg.show({ title: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_91) %>", msg: message, minWidth: width, buttons: Ext.Msg.OKCANCEL, icon: Ext.MessageBox.WARNING,
                  fn: function (buttonId) {
                      if (buttonId == "ok") {
                          $(button).attr("confirmed", true);
                          $(button).click();
                          document.location = $(button).attr('href');
                      }
                  }
              });

              return false;
          }
          else {
              return true;
          }
      };
      $(function () {
          var pickedReports = $("#" + "<%=reportsIds.ClientID %>").val();
          SW.Orion.ReportPicker.setSelectedReports(JSON.parse(pickedReports));
      });
      
      function htmlEncode(s) {
        return s.replace(/&/g, '&amp;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')
            .replace(/'/g, '&#39;')
            .replace(/"/g, '&#34;');
    }
  </script>
</asp:Content>

