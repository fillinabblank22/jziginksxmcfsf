<%@ Page Language="C#" MasterPageFile="~/Orion/Reports/ScheduleAdd/AddScheduleWizard.master" 
 AutoEventWireup="true" CodeFile="Summary.aspx.cs" Inherits="Orion_Reports_Schedule_Add_Summary"
 Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AB0_6 %>" %>

 <asp:Content ID="Content2" ContentPlaceHolderID="headPlaceholder" runat="server">
    <orion:Include File="ReportSchedule.css" runat="server" />
    <orion:Include File="ReportingDisplay.css" runat="server" />
</asp:Content>
<asp:Content ID="wizardContent" ContentPlaceHolderID="wizardContent" runat="Server">
 <div class="GroupBox">
 <div style="width: 800px;  padding: 20px;">
 <div class="wizardHeader"><h2>4. <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_13) %></h2></div>
 <div class="wizardContent">
 <div class="paragraph"> 
 <div><span class="informText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_14) %></span></div>
 </div>

 <div class="paragraph" style="word-wrap:break-word">  
 <asp:Label ID="ScheduleName" runat="server" CssClass="scheduleName"></asp:Label>
 <div class="editLink"><a href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.UrlHelper.AppendParameter(Workflow.FirstStepUrl, "ScheduleWizardGuid", Workflow.Id.ToString())) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_28) %></a></div>
 </div>

  <div class="paragraph" style="word-wrap:break-word">
  <span class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_15) %>:</span>
  <div><asp:Label ID="Description" CssClass="informText" runat="server"></asp:Label></div>
  <%--<span class="informText">More detailed report schedule description would be here. Cius dit vellab idunt acestot aturecu lluptaepe eum qui con poraesciusae dolenis moditatiusam.</span>--%>
  </div>

  <div class="paragraph">
  <span class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_16) %>: 
  <asp:Label ID="CountReports" runat="server" ></asp:Label>
  </span>  
   <asp:Panel ID="Reports" runat="server"> </asp:Panel>
  </div>

  <div class="paragraph">
  <span class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_17) %>: <asp:Label ID="CountWebpages" runat="server"
          ></asp:Label></span>
   <asp:Panel ID="Webpages" runat="server"></asp:Panel>  
  </div>

  <div class="paragraph">
  <span class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_15) %>: </span>
 <div><asp:Label ID="Account" runat="server" CssClass="informText"></asp:Label></div>
  </div>
  
  <div class="paragraph">
  <span class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_86) %>: </span>
 <div><asp:Label ID="webServer" runat="server" CssClass="informText"></asp:Label></div>
  </div>

  <div class="verticalLine"></div>

  <div class="paragraph">
  <span class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_46) %>: </span>
  <div class="editLink"><a href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.UrlHelper.AppendParameter(Workflow.Steps[1].Url, "ScheduleWizardGuid", Workflow.Id.ToString())) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_28) %></a></div>
  <asp:Panel ID="Frequency" runat="server"></asp:Panel>  
  </div>
  <div class="verticalLine"></div>

  <div class="paragraph">
  <div class="editLink"><a href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.UrlHelper.AppendParameter(Workflow.Steps[2].Url, "ScheduleWizardGuid", Workflow.Id.ToString())) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_28) %></a></div>
  <span class="boldLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_18) %>: <asp:Label ID="CountActions" runat="server"></asp:Label></span>
          <asp:Panel ID="Actions" runat="server"></asp:Panel>  
  </div>

  <div class="paragraph">
   <orion:LocalizableButton runat="server" Visible="false" ID="testAction" OnClick="TestAction_Click" LocalizedText="CustomText"
                DisplayType="Small" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VL0_19 %>"/>
  </div>
 </div>
 </div>
 </div>

<div class="sw-btn-bar-wizard">
            <orion:LocalizableButton runat="server" ID="imgbBack" OnClick="Back_Click" 
                LocalizedText="Back" DisplayType="Secondary" CausesValidation="false" />
            <orion:LocalizableButton runat="server" ID="imgbNext" OnClick="Next_Click" LocalizedText="CustomText"
                DisplayType="Primary" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_111 %>"/>
            <orion:LocalizableButton runat="server" ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel"
                DisplayType="Secondary" CausesValidation="false" />
        </div>
</asp:Content>
