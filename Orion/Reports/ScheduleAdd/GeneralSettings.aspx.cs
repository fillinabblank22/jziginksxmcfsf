﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Models.Interfaces;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Reporting.DAL;
using SolarWinds.Orion.Web.Reporting;

public partial class Orion_Reports_Schedule_Add_GeneralSettings : AddScheduleWizardBase
{
	protected static Log log = new Log();

	private ReportJobConfiguration reportJobConfiguration { get; set; }
	private List<ReportSchedule> SchedulesList { get; set; }
    public string SchedulesListJson
    {
        get
        {
            var newList = new List<ReportSchedule>(SchedulesList.Count);
            foreach (var reportSchedule in SchedulesList)
            {
                var newRep = new ReportSchedule
                {
                    CronExpression = reportSchedule.CronExpression,
                    DisplayName = HttpUtility.HtmlEncode(reportSchedule.DisplayName),
                    CronExpressionTimeZoneInfoId = reportSchedule.CronExpressionTimeZoneInfoId,
                    FrequencyId = reportSchedule.FrequencyId,
                    EndTime = reportSchedule.EndTime,
                    StartTime = reportSchedule.StartTime
                };
                newList.Add(newRep);
            }
            var settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Objects };
            return JsonConvert.SerializeObject(newList, settings);
        }
    }
    
    protected override void OnInit(EventArgs e)
	{
        var job = this.GetJob();
        if (job != null && job.ReportJobID > 0)
        {
            this.Title = Resources.CoreWebContent.WEBDATA_SO0_109;           
        }
        Workflow.SetStepFromUrl(this.Page.Request.Url.GetLeftPart(UriPartial.Path));
		base.OnInit(e);
	}
    private ReportJobConfiguration GetJob()
    {
        return ReportSchedulesStorage.Instance.GetJob(Workflow.WizardGuid);
    }
	protected override void OnLoad(EventArgs e)
	{
        GetSchedules();
	    FrequencyControl.WizardGuid = Workflow.WizardGuid;
	    base.OnLoad(e);
    }

    private void GetSchedules()
    {
        if (ReportSchedulesStorage.Instance.GetJob(Workflow.WizardGuid).Schedules == null)
        {
            ReportSchedulesStorage.Instance.GetJob(Workflow.WizardGuid).Schedules = new List<ReportSchedule>();
        }
        SchedulesList = ReportSchedulesStorage.Instance.GetJob(Workflow.WizardGuid).Schedules;
        foreach (var reportSchedule in SchedulesList)
        {
            if (reportSchedule.StartTime.Kind != DateTimeKind.Utc)
            {
                reportSchedule.StartTime = reportSchedule.StartTime.ToUniversalTime();
            }

            if (reportSchedule.EndTime.HasValue && reportSchedule.EndTime.Value.Kind != DateTimeKind.Utc)
            {
                reportSchedule.EndTime = reportSchedule.EndTime.Value.ToUniversalTime();
            }
        }
    }
    
    private void registerScript(string errorsMessage)
	{
		ClientScript.RegisterStartupScript(this.GetType(), "myalert", string.Format("Ext.Msg.show({0});", errorsMessage), true);
	}

    private List<ReportSchedule> CastListToReportSchedule(IEnumerable<ISchedule> listOfSchedules)
    {
        var reportSchedulesList = new List<ReportSchedule>();
        foreach (var schedule in listOfSchedules)
        {
            reportSchedulesList.Add(schedule as ReportSchedule);
        }
        return reportSchedulesList;
    }
    protected override bool ValidateUserInput()
	{
        var schedulesList = FrequencyControl.Schedules;
        if (schedulesList == null || schedulesList.ToList().Count == 0)
        {
            this.registerScript(string.Format("{{title: '{0}', msg: '{1}', minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR}}",
                    Resources.CoreWebContent.WEBDATA_SO0_141, Resources.CoreWebContent.WEBDATA_AB0_45));
            return false;
        }
        return true;
	}

    protected override bool SaveCurrentState()
    {
        try
        {
            reportJobConfiguration = ReportSchedulesStorage.Instance.GetJob(Workflow.WizardGuid);
            reportJobConfiguration.Schedules = CastListToReportSchedule(FrequencyControl.Schedules);
            ReportSchedulesStorage.Instance.SetJob(reportJobConfiguration, Guid.Parse(Request["ScheduleWizardGuid"]));
            return true;
        }
        catch (Exception ex)
        {
            log.Error("Error while storing job changes", ex);
            return false;
        }
    }
    
	protected static void BusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}
}