﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Actions.Impl.Email;
using SolarWinds.Orion.Core.Actions.Impl.PrintURL;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Models.Actions;
using SolarWinds.Orion.Core.Models.Actions.Contexts;
using SolarWinds.Orion.Core.Reporting.DAL;
using SolarWinds.Orion.Web.Reporting;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Core.Models.MacroParsing;

public partial class Orion_Reports_ScheduleAdd_Actions : AddScheduleWizardBase
{
    protected static Log log = new Log();
    private ReportJobConfiguration reportJobConfiguration { get; set; }

    protected override void OnInit(EventArgs e)
    {
        var macroContext = new MacroContext();
        var editedJob = ReportSchedulesStorage.Instance.GetJob(Workflow.WizardGuid);

        if (editedJob != null && editedJob.ReportJobID > 0)
        {
            this.Title = Resources.CoreWebContent.WEBDATA_SO0_109;
            actionsEditorView.Actions = editedJob.Actions;
        }
            
        macroContext.Add(new GenericContext());
            
        if (editedJob != null)
        {
            macroContext.Add( new ReportingContext
            {
                ScheduleDescription = editedJob.Description,
                ScheduleName = editedJob.Name,
                AccountID = editedJob.AccountID,
                WebsiteID = editedJob.WebsiteID,
                LastRun = editedJob.LastRun
            });
        }

        actionsEditorView.ViewContext = new ReportingActionContext { MacroContext = macroContext };
        actionsEditorView.PreSelectedActionTypeID = EmailUrlConstants.ActionTypeID;
        Workflow.SetStepFromUrl(this.Page.Request.Url.GetLeftPart(UriPartial.Path));
        base.OnInit(e);
    }

    protected override void OnLoad(EventArgs e)
    {
        if (!IsPostBack)
        {
            var job = ReportSchedulesStorage.Instance.GetJob(Workflow.WizardGuid);
            if (job != null && job.Actions != null)
            {
                actionsEditorView.Actions = job.Actions;
            }
        }
        base.OnLoad(e);
    }

    protected override bool SaveCurrentState()
    {
        try
        {
            reportJobConfiguration = ReportSchedulesStorage.Instance.GetJob(Workflow.WizardGuid);
            reportJobConfiguration.Actions = actionsEditorView.Actions.ToList();
            ReportSchedulesStorage.Instance.SetJob(reportJobConfiguration, Guid.Parse(Request["ScheduleWizardGuid"]));
            return true;
        }
        catch (Exception ex)
        {
            log.Error("Error while storing job changes", ex);
            return false;
        }
    }

    private void registerScript(string errorsMessage)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "myalert", string.Format("Ext.Msg.show({0});", errorsMessage), true);
    }

    protected override bool ValidateUserInput()
    {
        if (!actionsEditorView.Actions.Any())
        {
            this.registerScript(string.Format("{{title: '{0}', msg: '{1}', minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR}}", Resources.CoreWebContent.WEBDATA_VM0_1, Resources.CoreWebContent.WEBDATA_AB0_46));
            return false;
        }
        //Needed for migrated print actions
        var printActions = actionsEditorView.Actions.Where(a => a.ActionTypeID == PrintURLConstants.ActionTypeID).Cast<object>().ToList();
        foreach (var action in printActions)
        {
            int credentialsID;
            UsernamePasswordCredential credentials = null;
            if (int.TryParse(((ActionDefinition)action).Properties.GetPropertyValue(PrintURLConstants.CredentialsIDPropertyKey),
                out credentialsID))
            {
                credentials = new CredentialManager().GetCredential<UsernamePasswordCredential>(credentialsID);
            }
            
            if (credentials == null)
            {
                this.registerScript(string.Format("{{title: '{0}', msg: '{1}', minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR}}", Resources.CoreWebContent.WEBDATA_AB0_74, Resources.CoreWebContent.WEBDATA_AB0_75));
                return false;
            }
        }
        
        return true;
    }
}
