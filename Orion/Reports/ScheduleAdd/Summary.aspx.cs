﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Reporting;
using SolarWinds.Orion.Core.Reporting.DAL;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using ReportJobDAL = SolarWinds.Orion.Web.DAL.ReportJobDAL;

public partial class Orion_Reports_Schedule_Add_Summary : AddScheduleWizardBase
{
	protected static Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

	protected override void OnInit(EventArgs e)
	{
		var job = GetJob();
        if (job != null && job.ReportJobID > 0)
		{
			imgbNext.Text = Resources.CoreWebContent.WEBDATA_IB0_159;
			this.Title = Resources.CoreWebContent.WEBDATA_SO0_109;
		}
		base.OnInit(e);
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		UpdateUiWithReportJobConfiguraion(GetJob());
		Workflow.SetStepFromUrl(this.Page.Request.Url.GetLeftPart(UriPartial.Path));
		this.UpdateProgressIndicator();

		// block saving report in case of demo mode
		if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
		{
			imgbNext.OnClientClick = "demoAction('Core_Scheduler_SaveSchedule', this); return false;";
		}
	}

	private ReportJobConfiguration GetJob()
	{
		return ReportSchedulesStorage.Instance.GetJob(Workflow.WizardGuid);
	}

    private void UpdateUiWithReportJobConfiguraion(ReportJobConfiguration job)
    {
        CountReports.Text = job.Reports.Count.ToString();
        CountActions.Text = job.Actions.Count.ToString();
		CountWebpages.Text = job.Urls.Count.ToString();
        Description.Text = HttpUtility.HtmlEncode(job.Description);
        Account.Text = HttpUtility.HtmlEncode(job.AccountID);
        ScheduleName.Text = HttpUtility.HtmlEncode(job.Name);
		
		Dictionary<int, string> sites = WebsitesDAL.GetAllSites();
		if (sites.ContainsKey(job.WebsiteID))
			webServer.Text = WebsitesDAL.GetAllSites()[job.WebsiteID];
		else {
			//almost impossible but...
			foreach (var site in sites) {
				if (site.Value.IndexOf("(primary)", StringComparison.OrdinalIgnoreCase) > 0)
				{
					webServer.Text = site.Value;
					break;
				}
			}
		}

		Panel childPanel;
		//Add Actions
		if (job.Actions != null)
		{
			foreach (var action in job.Actions)
			{
				childPanel = new Panel();
				childPanel.Controls.Add(new Label() { Text = HttpUtility.HtmlEncode(action.Title), CssClass = "action" });
				childPanel.Controls.Add(new Image() { ToolTip = DefaultSanitizer.SanitizeHtml(action.Description.Replace("<br/>", Environment.NewLine).Replace(";","; "))?.ToString(), ImageUrl = "/Orion/images/Show_Last_Note_icon16x16.PNG" });
				this.Actions.Controls.Add(childPanel);
			}
		}

		//Add Frequency
		if (job.Schedules != null)
		{
			foreach (var schedule in job.Schedules)
			{
				childPanel = new Panel();
				childPanel.Controls.Add(new Label() { Text = CronHelper.GetFrequencyName(schedule, true, true), CssClass = "informText" });
				this.Frequency.Controls.Add(childPanel);
			}
		}

		// Add reports
		if (job.Reports != null)
		{
			foreach (var report in job.Reports)
			{
				childPanel = new Panel();
				childPanel.Controls.Add(new HyperLink()
				{
				    NavigateUrl = SolarWinds.Orion.Web.Helpers.UrlHelper.AppendParameter("/Orion/Report.aspx", "ReportID", report.ID.ToString()), 
                    Target = "_blank", Text = HttpUtility.HtmlEncode(report.Title), CssClass = "summaryLink"
				});
				this.Reports.Controls.Add(childPanel);
			}
		}

 		// Add webpages
		if(job.Urls != null)
		{
        foreach (string url in job.Urls)
        {
            childPanel = new Panel();
            childPanel.Controls.Add(new HyperLink() { NavigateUrl = url, Target = "_blank", Text = url, CssClass = "summaryLink" });
            this.Webpages.Controls.Add(childPanel);
        }
		}
	}

    protected override bool Next()
    {
        var job = GetJob();
        if (job != null)
        {
            int primaryEngineId = EnginesDAL.GetPrimaryEngineId();
            using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, primaryEngineId))
            {
                Session["CreatedScheduledJobName"] = job.Name;
                if (job.ReportJobID == 0)
                {
                    businessProxy.AddReportJob(job);
                }
                else
                {
                    businessProxy.UpdateReportJob(job, ReportJobDAL.GetLimitedReportIds());
                    if (!Workflow.IsDuplicated)
                        Session["CreatedScheduledJobName"] += ":hightlight=true";
                }
            }
            return base.Next();
        }
        else
        {
            return false;
        }
    }

    protected void TestAction_Click(object sender, EventArgs e)
	{
	}

	protected static void BusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}
}
