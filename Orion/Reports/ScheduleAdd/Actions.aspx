<%@ Page Language="C#" MasterPageFile="~/Orion/Reports/ScheduleAdd/AddScheduleWizard.master" 
 AutoEventWireup="true" CodeFile="Actions.aspx.cs" Inherits="Orion_Reports_ScheduleAdd_Actions"
 Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AB0_6 %>" %>
<%@ Register TagPrefix="orion" TagName="ActionsEditorView" Src="~/Orion/Actions/Controls/ActionsEditorView.ascx" %>

<asp:Content ContentPlaceHolderID="headPlaceholder" runat="server">
    <orion:Include File="ReportSchedule.css" runat="server" />
</asp:Content>

<asp:Content ID="wizardContent" ContentPlaceHolderID="wizardContent" runat="Server">
<div class="GroupBox">
<div style="padding: 30px;">
<div class="wizardHeader" style="font-size: 16px">
    <h2 style="padding-top: 10px"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_137) %></h2>
</div>

<div class="paragraph" style="padding-top: 10px">
<span class="informText"> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_48) %></span>
</div>

<h2 style="padding-top: 20px; padding-bottom: 5px"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_138) %></h2>
<orion:ActionsEditorView runat="server" ID="actionsEditorView" ClientInstanceName="ReportingActions" EnvironmentType="Reporting" />
</div>
</div>

<div class="sw-btn-bar-wizard">
            <orion:LocalizableButton runat="server" ID="imgbBack" OnClick="Back_Click" 
                LocalizedText="Back" DisplayType="Secondary" CausesValidation="false" />
            <orion:LocalizableButton runat="server" ID="imgbNext" OnClick="Next_Click" LocalizedText="Next"
                DisplayType="Primary"/>
            <orion:LocalizableButton runat="server" ID="imgbCancel" OnClick="Cancel_Click" LocalizedText="Cancel"
                DisplayType="Secondary" CausesValidation="false" />
        </div>
</asp:Content>