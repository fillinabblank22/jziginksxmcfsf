﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Reporting.DAL;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.Reporting;
using SolarWinds.Orion.Web.DAL;
using ReportJobDAL = SolarWinds.Orion.Web.DAL.ReportJobDAL;

public partial class Orion_Reports_Schedule_Add_Default : AddScheduleWizardBase
{
	protected static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    internal bool _isOrionDemoMode;
	protected override void OnInit(EventArgs e)
	{
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            _isOrionDemoMode = true;
        }
		int jobId = 0;
        if (!IsPostBack)
        {
            string scheduleWizardGuid = Request["ScheduleWizardGuid"];

            // no point processing further if the guid is empty
            // this can happen if js method fails to get a guid due to session timeout
            if (string.IsNullOrEmpty(scheduleWizardGuid))
                Response.Redirect(DefaultReturnUrl);

            Workflow.WizardGuid = Guid.Parse(scheduleWizardGuid);
            bool isDuplicated;
            Workflow.IsDuplicated = bool.TryParse(Request["IsDuplicated"], out isDuplicated) && isDuplicated;
            string id = Request["ReportJobId"];
			
            ReportJobConfiguration editedJob = ReportSchedulesStorage.Instance.GetJob(Workflow.WizardGuid);
            if (editedJob == null && !string.IsNullOrEmpty(id))
            {
                // get job with applied limitaions from SolarWinds.Orion.Web.DALReportJobDAL.GetReportJob method
                editedJob = ReportJobDAL.GetReportJob(Convert.ToInt32(id));
                ReportSchedulesStorage.Instance.SetJob(editedJob, Workflow.WizardGuid);
            }
            if (editedJob != null)
            {
                jobId = editedJob.ReportJobID;
                if (jobId != 0)
                    this.Title = Resources.CoreWebContent.WEBDATA_SO0_109;
            }
        }
		Workflow.SetInitialRedirect(jobId >0 ? Workflow.Steps.Count-1 : 0);
        Workflow.FirstStep();
        base.OnInit(e);
	}

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            SaveCurrentState();
        }
        else
        {
            var job = GetJob();
            if (string.IsNullOrEmpty(job.AccountID))
                RSAdvancedSettingsControl.setAccount(this.Profile.UserName);
            else
                RSAdvancedSettingsControl.setAccount(job.AccountID);
            RSAdvancedSettingsControl.setWebsite(job.WebsiteID);

        }
        UpdateUiWithReportJobConfiguraion(GetJob());
        Workflow.SetStepFromUrl(this.Page.Request.Url.GetLeftPart(UriPartial.Path));
        this.UpdateProgressIndicator();

        this.siteAddresses.Value = JsonConvert.SerializeObject(WebsitesDAL.GetAllSiteAddresses());
        this.selectWebsiteId.Value = RSAdvancedSettingsControl.GetSelectWebsiteId();
    }

    private void registerScript(string errorsMessage)
	{
		ClientScript.RegisterStartupScript(this.GetType(), "myalert", string.Format("Ext.Msg.show({0});", errorsMessage), true);
	}

	private ReportJobConfiguration GetJob()
	{
        if (ReportSchedulesStorage.Instance.GetJob(Workflow.WizardGuid) == null)
        {
            ReportJobConfiguration newJob = new ReportJobConfiguration();

            if (!string.IsNullOrEmpty(Request["ReportData"]))
            {
              newJob.Reports = (List<ReportTuple>)OrionSerializationHelper.FromJSON(Request["ReportData"],typeof(List<ReportTuple>));
            }
            ReportSchedulesStorage.Instance.SetJob(newJob, Workflow.WizardGuid);
            return newJob;
        }
		return ReportSchedulesStorage.Instance.GetJob(Workflow.WizardGuid);
	}

	private bool ValidateJobConfiguration(ReportJobConfiguration jobConfig)
	{
	    if (string.IsNullOrEmpty(jobConfig.AccountID))
	    {
            return false;
	    }
        else if (!AccountManagementDAL.AccountExists(jobConfig.AccountID, true))
        {
            this.registerScript(string.Format("{{title: '{0}', msg: '{1}', minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR}}", Resources.CoreWebContent.WEBDATA_TD0_8, Resources.CoreWebContent.WEBDATA_TD0_9));
            return false;
        }
        else if (jobConfig.Reports.Count == 0 && jobConfig.Urls.Count == 0)
        {
            this.registerScript(string.Format("{{title: '{0}', msg: '{1}', minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR}}", Resources.CoreWebContent.WEBDATA_AB0_7, Resources.CoreWebContent.WEBDATA_AB0_8));
            return false;
        }
        else if (!ReportHelper.IsReportJobNameUnique(Name.Text, jobConfig.ReportJobID))
        {
            this.registerScript(string.Format("{{title: '{0}', msg: '{1}', minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR}}", Resources.CoreWebContent.WEBDATA_AB0_15, Resources.CoreWebContent.WEBDATA_AB0_14));
            return false;
        }
        else if (jobConfig.Urls.Count > 0 && !ReportHelper.IsJobValidUrls(jobConfig.Urls))
        {
            this.registerScript(string.Format("{{title: '{0}', msg: '{1}', minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR}}", Resources.CoreWebContent.WEBDATA_TK0_59, Resources.CoreWebContent.WEBDATA_TK0_59));
            return false;
        }

		return true;
	}

	private ReportJobConfiguration CollectReportJobConfiguraion()
	{
		var job = GetJob();
        job.AccountID = RSAdvancedSettingsControl.getAccount();
	    job.WebsiteID = RSAdvancedSettingsControl.getWebsite();
        job.Enabled = job.ReportJobID == 0 || job.Enabled;
		job.Name = Name.Text;
		job.Description = Description.Value;
        job.Reports = (List<ReportTuple>)OrionSerializationHelper.FromJSON(reportsIds.Value, typeof(List<ReportTuple>));
        job.Urls = (List<string>)OrionSerializationHelper.FromJSON(urlFields.Value, typeof(List<string>));
		return job;
	}

	private void UpdateUiWithReportJobConfiguraion(ReportJobConfiguration job)
	{
		Name.Text = job.Name;
		Description.Value = job.Description;
        reportsIds.Value = OrionSerializationHelper.ToJSON(job.Reports, typeof(List<ReportTuple>));
        urlFields.Value = OrionSerializationHelper.ToJSON(job.Urls, typeof(List<string>));
	}

    protected override bool ValidateUserInput()
    {
        return ValidateJobConfiguration(CollectReportJobConfiguraion());
    }

    protected override bool SaveCurrentState()
    {
        try
        {
            ReportSchedulesStorage.Instance.SetJob(CollectReportJobConfiguraion(), Workflow.WizardGuid);
            return true;
        }
        catch (Exception ex)
        {
            log.Error("Error while storing job changes", ex);
            return false;
        }
    }

    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }
}
