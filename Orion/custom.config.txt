<!-- 
	This file is here to help show what the custom.config file should contain.
	The XML attribute values below are samples.
-->
<configuration>
	<identity impersonate="false" />
	<proxy host="localhost" port="80" httpsPort="443" />
</configuration>