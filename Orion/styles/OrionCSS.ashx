<%@ WebHandler Language="C#" Class="OrionCSS" %>

using System;
using System.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI.Localizer;

public class OrionCSS : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        var path = context.Request.QueryString["path"];
        if (! WebSecurityHelper.IsLocalOrSameServerUrl(context.Request.Url, path))
        {
            throw new InvalidOperationException($"Stylesheet path ({path}) is not local.");
        }

        HttpContext.Current = context;
        LocalizerHttpHandler.RedirectToMe(context, path);
    }

    public bool IsReusable {
        get 
        {
            return true;
        }
    }
}