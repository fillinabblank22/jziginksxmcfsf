<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SetupWebConsole.aspx.cs"
    Inherits="Orion_PM_Admin_SetupWebConsole" MasterPageFile="~/Orion/PM/Admin/PMAdmin.master" %>

<asp:Content runat="server" ContentPlaceHolderID="adminHeadPlaceholder">
    <orion:Include runat="server" Module="PM" File="SetupWebConsole.css" />
    <script type="text/javascript">
        ShowLoadingGif = function () {
            if (typeof (Page_ClientValidate) == 'function') {
                Page_ClientValidate();
            }

            if (Page_IsValid) {
                $("#<%= this.TestingFailedPanel.ClientID %>").hide();
                $("#<%= this.TestingSuccessPanel.ClientID %>").hide();
                $("#<%= this.TestingPanel.ClientID %>").show();
            }
        }

        ClearOnFocus = function () {
            var pwd = $("#<%= this.PasswordTextBox.ClientID %>");
            var con = $("#<%= this.ConfirmPasswordTextBox.ClientID %>");

            if (pwd.val() == '<%= UNCHANGED_PASSWORD %>' || con.val() == '<%= UNCHANGED_PASSWORD %>') {
                pwd.val('');
                con.val('');
            }
        }

        TestIfNotTested = function () {
            ShowLoadingGif();
            var controlID = "<%= this.TestButton.ClientID %>".replace(/_/g, '$');
            WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(controlID, "<%= SUBMIT_KEY %>", true, "", "", false, true));
            
            return false;
        }
    </script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="adminContentPlaceholder">
    <h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>

    <asp:Panel ID="DescriptionPanel" runat="server" CssClass="descriptionPanel">
        <asp:Label ID="PageDescriptionLabel" runat="server" />
    </asp:Panel>

    <table id="adminContentTable" class="controlFrame">
        <tr>
            <td>
                <table id="inside_table" 
                       border="0" 
                       cellpadding="0" 
                       cellspacing="0" 
                       width="100%" 
                       class="alternateRow" 
                       style="padding:12px;">
                    <tr>
                        <td style="width:180px;"><asp:Label ID="ServerLabel" runat="server" /></td>
                        <td style="width:180px;">
                            <asp:TextBox ID="ServerTB" runat="server" CssClass="inputBox" />
                            <div class="smallText">
                                <span style="white-space: nowrap;"><asp:Label ID="ServerHintLabel" runat="server" /></span>
                            </div>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="ServerTbRequiredFieldValidator" runat="server" 
                                                        ControlToValidate="ServerTB" 
                                                        ErrorMessage="*"
                                                        ForeColor="Red"
                                                        Font-Bold="true"
                                                        Display="Dynamic" />
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:CustomValidator ID="IPv4Validator" runat="server" 
                                                         ErrorMessage="IPv6 is not supported"
                                                         ForeColor="Red"
                                                         Font-Bold="true"
                                                         ControlToValidate="ServerTB"
                                                         OnServerValidate="ValidateIP"
                                                         Display="Dynamic" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        
                    </tr>
                    <tr>
                        <td><asp:Label ID="PortLabel" runat="server" /></td>
                        <td>
                            <asp:TextBox ID="PortTB" runat="server" CssClass="inputBox" />
                            <div class="smallText">
                                <span style="white-space: nowrap;"><asp:Label ID="PortHintLabel" runat="server" /></span>
                            </div>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="PortTbRequiredFieldValidator" runat="server" 
                                                        ControlToValidate="PortTB" 
                                                        ErrorMessage="*"
                                                        ForeColor="Red"
                                                        Font-Bold="true" />
                            <asp:RangeValidator ID="PortTbRangeValidator" 
                                                runat="server" 
                                                ControlToValidate="PortTB" 
                                                ErrorMessage="Invalid Port"
                                                ForeColor="Red"
                                                Font-Bold="true"
                                                Type="Integer"
                                                MinimumValue="0"
                                                MaximumValue="65535" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table id="inside_table" 
                       border="0" 
                       cellpadding="0" 
                       cellspacing="0" 
                       width="100%" 
                       class="alternateRow"
                       style="padding:12px;">
                    <tr>
                        <td colspan="2" style="padding-bottom:0px;">
                            <asp:Label ID="WindowsCredentialsLabel" runat="server" Font-Size="Larger" Font-Bold="true" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"  style="padding-top:0px;">
                            <asp:Label ID="CredentialDescriptionLabel" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td><asp:Label ID="UserNameLabel" runat="server" /></td>
                        <td>
                            <asp:TextBox ID="UserNameTextBox" runat="server" CssClass="inputBox" />
                            <asp:RequiredFieldValidator runat="server" 
                                                        ControlToValidate="UserNameTextBox" 
                                                        ErrorMessage="*"
                                                        ForeColor="Red"
                                                        Font-Bold="true" />
                            <asp:RegularExpressionValidator runat="server"
                                                            ID="UserNameRegExValidator"
                                                            ControlToValidate="UserNameTextBox"
                                                            ValidationExpression="(^([^@]+)@([^@]+)$)|(^([^\\]+)\\([^\\]+)$)"
                                                            ForeColor="Red"
                                                            Font-Bold="true" />

                             <div style="display:inline-block;">
                                <div class="smallText" style="white-space: nowrap;">
                                    <asp:Label ID="HintLabel" runat="server" />
                                </div>
                            </div>

                       </td>
                    </tr>
                    <tr>
                        <td><asp:Label ID="PasswordLabel" runat="server" /></td>
                        <td>
                            <asp:TextBox ID="PasswordTextBox" runat="server" 
                                                              CssClass="inputBox" 
                                                              TextMode="Password" 
                                                              onfocus="ClearOnFocus();" />
                            <asp:RequiredFieldValidator runat="server" 
                                                        ControlToValidate="PasswordTextBox" 
                                                        ErrorMessage="*"
                                                        ForeColor="Red"
                                                        Font-Bold="true" />
                        </td>
                    </tr>
                    <tr>
                        <td><asp:Label ID="ConfirmPasswordLabel" runat="server" /></td>
                        <td>
                            <asp:TextBox ID="ConfirmPasswordTextBox" runat="server" 
                                                                     CssClass="inputBox" 
                                                                     TextMode="Password" 
                                                                     onfocus="ClearOnFocus();" />
                            <asp:RequiredFieldValidator runat="server" 
                                                        ControlToValidate="ConfirmPasswordTextBox" 
                                                        ErrorMessage="*"
                                                        ForeColor="Red"
                                                        Font-Bold="true" />
                            <asp:CompareValidator ID="PasswordMatchValidator" runat="server"
                                                    ControlToValidate="PasswordTextBox"
                                                    ControlToCompare="ConfirmPasswordTextBox"
                                                    ForeColor="Red"
                                                    Font-Bold="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="2" >
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <orion:LocalizableButton ID="TestButton" 
                                                                            runat="server" 
                                                                            DisplayType="Small" 
                                                                            LocalizedText="Test" 
                                                                            OnClick="TestClick"
                                                                            OnClientClick="ShowLoadingGif();" />
                                            </td>
                                            <td>
                                                <asp:Panel ID="TestingPanel" runat="server" CssClass="hidden" >
                                                    <img src="/Orion/images/AJAX-Loader.gif" alt="testing..." />
                                                </asp:Panel>
                                                <asp:Panel ID="TestingSuccessPanel" runat="server" BackColor="LightGreen" CssClass="resultPanel">
                                                    <img src="/Orion/images/ok_16x16.gif" alt="[ok]" />
                                                    <asp:Label ID="TestSuccededLabel" runat="server" />
                                                </asp:Panel>
                                                <asp:Panel ID="TestingFailedPanel" runat="server" BackColor="LightCoral" CssClass="resultPanel">
                                                    <img src="/Orion/images/failed_16x16.gif" alt="[ok]" />
                                                    <asp:Label ID="TestFailedLabel" runat="server" />
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table cellspacing="10" class="buttonBar">
        <tr>
            <td>
                <orion:LocalizableButton ID="SubmitButton" 
                                            runat="server" 
                                            DisplayType="Primary" 
                                            LocalizedText="Submit" 
                                            OnClick="SubmitClick"
                                            OnClientClick="return TestIfNotTested();" />
            </td>
            <td>
                <orion:LocalizableButton ID="CancelButton" 
                                                runat="server" 
                                                DisplayType="Secondary" 
                                                LocalizedText="Cancel" 
                                                OnClick="CancelClick"
                                                CausesValidation="false" />
            </td>
        </tr>
    </table>
</asp:Content>
