<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Settings.aspx.cs"
    Inherits="Orion_SCM_Admin_Settings" MasterPageFile="~/Orion/PM/Admin/PMAdmin.master" %>

<%@ Register Src="~/Orion/Controls/HelpButton.ascx" TagPrefix="orion" TagName="HelpButton" %>
<asp:Content runat="server" ContentPlaceHolderID="adminHeadPlaceholder">
    <style type="text/css">
        .propertyTable
        {
            text-align:left;
            border-style:solid;
            border-width:1px;
            width:70%;
            margin:18px;
            padding:12px;
            background-color:White;
            border-color:Gray;
        }
        .cell1 { width:20%;  font-weight:bold; }
        .cell2 { width:8%; text-align:right; }
        .cell3 { width:10%; }
        .cell4 { width:40%; }
        .tb { width:60px; }
        .alterItem { background-color:#E4F1F8; }
    </style>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="adminContentPlaceholder">
    <h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>

    <asp:Repeater runat="server" ID="settingsRepeater" OnItemDataBound="SettingRepeater_ItemDataBound" >
        <HeaderTemplate>
            <table class="propertyTable" cellspacing="0">
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td class="cell1"><asp:Label runat="server" ID="settingNameLabel" /></td>
                <td class="cell2"><asp:TextBox runat="server" ID="userInputTextBox" CssClass="tb" /></td>
                <td class="cell3"><asp:Label runat="server" ID="rangeLabel" /></td>
                <td class="cell4"><asp:Label runat="server" ID="descriptionLabel" /></td>
                <td>
                    <asp:RangeValidator runat="server" 
                                        ID="rangeValidator" 
                                        ControlToValidate="userInputTextBox"
                                        Type="Integer"
                                        ForeColor="Red"
                                        Font-Bold="true"
                                        Display="Dynamic" />
                    <asp:RequiredFieldValidator runat="server" 
                                                ID="requiredFieldValidator" 
                                                ControlToValidate="userInputTextBox"
                                                ForeColor="Red"
                                                Font-Bold="true"
                                                Display="Dynamic" />
                </td>
            </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr class="alterItem">
                <td class="cell1"><asp:Label runat="server" ID="settingNameLabel" /></td>
                <td class="cell2"><asp:TextBox runat="server" ID="userInputTextBox" CssClass="tb" /></td>
                <td class="cell3"><asp:Label runat="server" ID="rangeLabel" /></td>
                <td class="cell4"><asp:Label runat="server" ID="descriptionLabel" /></td>
                <td>
                    <asp:RangeValidator runat="server" 
                                        ID="rangeValidator" 
                                        ControlToValidate="userInputTextBox"
                                        Type="Integer"
                                        ForeColor="Red"
                                        Font-Bold="true"
                                        Display="Dynamic" />
                    <asp:RequiredFieldValidator runat="server" 
                                                ID="requiredFieldValidator" 
                                                ControlToValidate="userInputTextBox"
                                                ForeColor="Red"
                                                Font-Bold="true"
                                                Display="Dynamic" />
                </td>
            </tr>
        </AlternatingItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>

    <div class="sw-btn-bar" style="padding:18px;">
        <orion:LocalizableButton ID="btnSubmit" 
                                 runat="server" 
                                 DisplayType="Primary" 
                                 LocalizedText="Submit" 
                                 OnClick="SubmitClick" />
        <orion:LocalizableButton ID="btnDefault"
                                 ValidationGroup="none" 
                                 runat="server" 
                                 DisplayType="Secondary" 
                                 LocalizedText="CustomText" 
                                 Text="Defaults" 
                                 OnClick="DefaultsClick" />
        <orion:LocalizableButton ID="btnCancel" 
                                 runat="server" 
                                 ValidationGroup="none" 
                                 DisplayType="Secondary" 
                                 LocalizedText="Cancel" 
                                 OnClick="CancelClick" />
    </div>
</asp:Content>
