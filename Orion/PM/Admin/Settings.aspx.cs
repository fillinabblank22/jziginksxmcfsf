﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.PM.Common;
using SolarWinds.PM.Common.DAL;
using SolarWinds.PM.Common.Model;
using SolarWinds.Logging;

public partial class Orion_SCM_Admin_Settings : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = this.pageTitleText;

        if (!this.IsPostBack)
        {
            this.settingsRepeater.DataSource = this.settingsDal.GetAllPMSettings();
            this.settingsRepeater.DataBind();
        }
    }

    public void SubmitClick(object sender, EventArgs e)
    {
        if (this.IsValid)
        {
            var settings = new PMSettingsDAL().GetAllPMSettings();

            var changedSettings = new List<PMSetting>();

            foreach (var setting in settings)
            {
                int? value = this.GetSetting(setting.SettingID);

                if (value.HasValue && value.Value != setting.Value)
                {
                    setting.Value = value.Value;
                    changedSettings.Add(setting);
                }
            }

            if (changedSettings.Count > 0)
                this.settingsDal.UpdateSettingsCurrentValue(changedSettings);
        }

        this.RedirectBack();
    }

    public void DefaultsClick(object sender, EventArgs e)
    {
        var settings = this.settingsDal.GetAllPMSettings();
        settings.ForEach((s) => s.Value = s.DefaultValue);
        this.settingsRepeater.DataSource = settings;
        this.settingsRepeater.DataBind();
    }

    public void CancelClick(object sender, EventArgs e)
    {
        this.RedirectBack();   
    }

    protected void SettingRepeater_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            PMSetting dataItem = ((PMSetting)e.Item.DataItem);

            TextBox editControl = (TextBox)e.Item.FindControl("userInputTextBox");
            editControl.Attributes["SettingID"] = dataItem.SettingID;
            editControl.Text = dataItem.Value.ToString();

            ((Label)e.Item.FindControl("settingNameLabel")).Text = dataItem.SettingName;
            ((Label)e.Item.FindControl("rangeLabel")).Text = dataItem.Units;
            ((Label)e.Item.FindControl("descriptionLabel")).Text = dataItem.SettingDescription;

            RangeValidator rv = ((RangeValidator)e.Item.FindControl("rangeValidator"));
            rv.MinimumValue = dataItem.MinValue.ToString();
            rv.MaximumValue = dataItem.MaxValue.ToString();
            rv.ErrorMessage = String.Format(this.rangeMessageFormat, dataItem.MinValue, dataItem.MaxValue, dataItem.Units);

            RequiredFieldValidator rfv = ((RequiredFieldValidator)e.Item.FindControl("requiredFieldValidator"));
            rfv.ErrorMessage = this.requiredMessage;
        }
    }

    private void RedirectBack()
    {
        //TODO: redirect to refferer when return url is specified in query string
        Response.Redirect("/Orion/PM/Admin");
    }

    private int? GetSetting(string id)
    {
        foreach (Control control in this.settingsRepeater.Controls)
        {
            if (control.Controls.Count > 3)
            {
                var tb = control.Controls[3] as TextBox;

                if (tb != null && tb.Attributes["SettingID"] == id)
                    return Convert.ToInt32(tb.Text);
            }
        }

        log.ErrorFormat("Unable to find setting control \"{0}\"", id);
        return null;
    }

    private static readonly Log log = new Log();

    private readonly string rangeMessageFormat = "* Value has to be from {0} to {1} {2}.";
    private readonly string requiredMessage = "* Value is required.";
    private readonly string pageTitleText = "Settings";

    private readonly PMSettingsDAL settingsDal = new PMSettingsDAL();
}