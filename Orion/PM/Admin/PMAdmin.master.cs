﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;

public partial class Orion_SCM_Admin_SCMAdmin : System.Web.UI.MasterPage
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!Profile.AllowAdmin)
        {
            Server.Transfer("~/Orion/Error.aspx?Message=You must be an Admin to access this page.");
        }
    }
}
