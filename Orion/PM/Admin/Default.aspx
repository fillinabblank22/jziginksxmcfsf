<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" Title="Patch Manager Settings"
    Inherits="Orion_PM_Admin_Default" AutoEventWireup="true" CodeFile="~/Orion/PM/Admin/Default.aspx.cs" %>

<%@ Register Src="~/Orion/Controls/HelpButton.ascx" TagPrefix="orion" TagName="HelpButton" %>


<asp:Content ContentPlaceHolderID="adminHeadPlaceholder" runat="server" >
    <style type="text/css">
        #adminNav { display: none; }
	    #adminContent p { border: none; width: auto; padding: 0; font-size: 12px;}
	    #adminContent a { color: #336699; }
	    #adminContent a:hover {color:orange;}
	    .column1of2 { padding-left:0px; padding-right:0px; vertical-align:top; text-align: left; width: 50%; }
	    .column2of2 { padding-left:0px; padding-right:0px; vertical-align:top; text-align: left; width: 50%; }
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>    
    
    <table id="adminContent" style="padding-top: 0; padding-left: 0;" width="100%" border="0"
        cellpadding="0" cellspacing="0">
        <tr>
            <td class="column1of2">
                <div class="ContentBucket">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="36" height="100%">
                                <img class="BucketIcon" src="/Orion/images/getting_started_36x36.gif" alt="" />
                            </td>
                            <td>
                                <div class="BucketHeader">Getting Started with Patch Manager</div>
                                <p>Connect the web console to the Patch Manager Desktop Console.</p>
                            </td>
                        </tr>
                    </table>
                    <table class="BucketLinkContainer">
                        <tr>
                            <td class="LinkColumn" width="33%">
                                <p>
                                    <span class="LinkArrow">&#0187;&nbsp;</span>
                                    <a href="/Orion/PM/Admin/SetupWebConsole.aspx">Web Console Setup</a>
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <!-- Begin 2nd Column -->
            <td class="column2of2">
                <div class="ContentBucket">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="36" height="100%">
                                <img class="BucketIcon" src="/Orion/PM/images/License32x32.png" />
                            </td>
                            <td>
                                <div class="BucketHeader">License Summary</div>
                                <p>View your license usage.</p>
                            </td>
                        </tr>
                    </table>
                    <table class="BucketLinkContainer">
                        <tr>
                            <td class="LinkColumn" width="33%">
                                <p>
                                    <span class="LinkArrow">&#0187;&nbsp;</span> 
                                    <a href="/Orion/Admin/Details/ModulesDetailsHost.aspx">License Summary</a>
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="ContentBucket">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="36" height="100%">
                                <img class="BucketIcon" src="/Orion/images/nodemgmt_art/icons/icon_36x36_settings.gif" />
                            </td>
                            <td>
                                <div class="BucketHeader">Global Settings</div>
                                <p>View and manage global settings.</p>
                            </td>
                        </tr>
                    </table>
                    <table class="BucketLinkContainer">
                        <tr>
                            <td class="LinkColumn" width="33%">
                                <p>
                                    <span class="LinkArrow">&#0187;&nbsp;</span> 
                                    <a href="/Orion/PM/Admin/Settings.aspx">Settings</a>
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <!-- End 2nd Column -->
        </tr>
    </table>
    <br />
</asp:Content>
