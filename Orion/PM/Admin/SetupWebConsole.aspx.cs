﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Logging;
using SolarWinds.PM.Common.Model;
using SolarWinds.PM.Common;
using System.Text;
using System.Net;
using System.Net.Sockets;
using SolarWinds.Orion.Security;

public partial class Orion_PM_Admin_SetupWebConsole : Page
{
    private static readonly Log Log = new Log();
    private const string PAGE_TITLE = "Web Console Setup";
    private const string PAGE_DESCRIPTION = "Enter the machine and credential information below to link the web console to " +
                                            "your Patch Manager server. All admin tasks must be done on the windows console" +
                                            " on the server. Reports and dashboard views may be accessed from the web conso" +
                                            "le once linked.";
    private const string SERVER_LABEL_TEXT = "Server name or IP address:";
    private const string SERVER_HINT_LABEL_TEXT = "Where Patch Manager desktop software is installed";
    private const string PORT_LABEL_TEXT = "Port:";
    private const string PORT_RANGE_VALIDATOR_MESSAGE = "Invalid Port";
    private const string PORT_HINT_LABEL_TEXT = "Default port value is 4092";
    private const string WINDOWS_CREDENTIALS_LABEL_TEXT = "Windows Credentials";
    private const string CREDENTIAL_DESCRIPTION = "for server entered above";
    private const string USERNAME_LABEL_TEXT = "User name:";
    private const string PASSWORD_LABEL_TEXT = "Password:";
    private const string CONFIRM_PASSWORD_LABEL_TEXT = "Confirm password:";
    private const string HINT_LABEL_TEXT = "Enter Domain\\Username or Username@Domain";
    private const string PASSWORD_MATCH_VALIDATOR_TEXT = "Passwords do not match.";
    private const string USERNAME_REG_EX_VALIDATOR_TEXT = "Invalid format";
    private const string TEST_SUCCEDED_LABEL_TEXT = "Test succeeded";
    private const string TEST_FAILED_LABEL_TEXT = "Test Failed";

    protected const string UNCHANGED_PASSWORD = "**********";

    protected const string SUBMIT_KEY = "Submit";

    private readonly ICryptoHelper _cryptoHelper;

    public Orion_PM_Admin_SetupWebConsole()
    {
        _cryptoHelper = new CryptoHelper();
    }

    protected string SessionPassword
    {
        get
        {
            object sessionObject = Session["DataGridPassword"];

            if (sessionObject == null)
                return null;
            else
                return (string)sessionObject;
        }
        set
        {
            Session["DataGridPassword"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = PAGE_TITLE;
        this.PageDescriptionLabel.Text = PAGE_DESCRIPTION;
        
        this.ServerLabel.Text = SERVER_LABEL_TEXT;
        this.ServerHintLabel.Text = SERVER_HINT_LABEL_TEXT;
        this.PortLabel.Text = PORT_LABEL_TEXT;
        this.PortHintLabel.Text = PORT_HINT_LABEL_TEXT;
        this.PortTbRangeValidator.ErrorMessage = PORT_RANGE_VALIDATOR_MESSAGE;

        this.WindowsCredentialsLabel.Text = WINDOWS_CREDENTIALS_LABEL_TEXT;
        this.CredentialDescriptionLabel.Text = CREDENTIAL_DESCRIPTION;
        this.UserNameLabel.Text = USERNAME_LABEL_TEXT;
        this.PasswordLabel.Text = PASSWORD_LABEL_TEXT;
        this.ConfirmPasswordLabel.Text = CONFIRM_PASSWORD_LABEL_TEXT;
        this.HintLabel.Text = HINT_LABEL_TEXT;
        this.PasswordMatchValidator.ErrorMessage = PASSWORD_MATCH_VALIDATOR_TEXT;
        this.UserNameRegExValidator.ErrorMessage = USERNAME_REG_EX_VALIDATOR_TEXT;
        this.TestSuccededLabel.Text = TEST_SUCCEDED_LABEL_TEXT;
        this.TestFailedLabel.Text = TEST_FAILED_LABEL_TEXT;

        this.TestingPanel.Style[HtmlTextWriterStyle.Display] = "none";
        this.TestingFailedPanel.Style[HtmlTextWriterStyle.Display] = "none";
        this.TestingSuccessPanel.Style[HtmlTextWriterStyle.Display] = "none";

        if (!this.IsPostBack)
        {
            var credential = this.GetCurrentCredential();

            if (credential == null)
            {
                this.UserNameTextBox.Text = "<enter user name>";
            }
            else
            {
                this.UserNameTextBox.Text = String.Format("{0}\\{1}", credential.Domain, credential.UserName);
                this.SessionPassword = credential.Password;

                StringBuilder script = new StringBuilder();
                script.Append("<script type=\"text/javascript\">");
                script.AppendFormat("$(\"#{0}\").val('{1}');", this.PasswordTextBox.ClientID, UNCHANGED_PASSWORD);
                script.AppendFormat("$(\"#{0}\").val('{1}');", this.ConfirmPasswordTextBox.ClientID, UNCHANGED_PASSWORD);
                script.Append("</script>");

                ScriptManager.RegisterStartupScript(this, this.GetType(), "PaswordPreSet", script.ToString(), false);
            }

            this.PortTB.Text = new Settings().DataGridServerPort.ToString();
            
            var server = new Settings().DataGridServerHost;

            if (String.IsNullOrEmpty(server))
                this.ServerTB.Text = "<enter server name or IP>";
            else
                this.ServerTB.Text = server;
        }
    }

    public void TestClick(object sender, EventArgs e)
    {
        if (this.IsValid)
        {
            var password = this.PasswordTextBox.Text == UNCHANGED_PASSWORD ? this.SessionPassword : this.PasswordTextBox.Text;


            if (this.TestCredentialTest(this.ServerTB.Text, 
                                        Convert.ToInt32(this.PortTB.Text), 
                                        this.UserNameTextBox.Text, password))
            {
                var argument = this.Request.Form["__EVENTARGUMENT"];

                if (!String.IsNullOrEmpty(argument) && argument == SUBMIT_KEY)
                {
                    StoreCredentialsAndRedirectBack();
                }
                
                this.TestingSuccessPanel.Style.Remove(HtmlTextWriterStyle.Display);
            }
            else
            {
                this.TestingFailedPanel.Style.Remove(HtmlTextWriterStyle.Display);
            }
        }
    }

    public void SubmitClick(object sender, EventArgs e)
    {
        StoreCredentialsAndRedirectBack();
    }

    private void StoreCredentialsAndRedirectBack()
    {
        if (this.IsValid)
        {
            var password = this.PasswordTextBox.Text == UNCHANGED_PASSWORD ? this.SessionPassword : this.PasswordTextBox.Text;

            SetCurrentCredential(this.ServerTB.Text, 
                                 Convert.ToInt32(this.PortTB.Text),
                                 this.UserNameTextBox.Text, password);
            this.RedirectBack();
        }
    }

    public void CancelClick(object sender, EventArgs e)
    {
        this.RedirectBack();
    }

    // invalidate the page ONLY if user entered IPV6
    public void ValidateIP(object source, ServerValidateEventArgs args)
    {
        IPAddress ip;

        if (!IPAddress.TryParse(args.Value, out ip))
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = ip.AddressFamily != AddressFamily.InterNetworkV6;
        }
    }

    private void RedirectBack()
    {
        // clean the session
        this.SessionPassword = null;

        //TODO: redirect to refferer when return url is specified in query string
        Response.Redirect("/Orion/PM/Admin");
    }

    private SharedNetworkCredential GetCurrentCredential()
    {
        try
        {
            var credential = SWISVerbsHelper.InvokeV3<SharedNetworkCredential>("Orion.PM.Management", "GetCurrentDataGridCredentials");
            
            if (credential != null)
                credential.Password = _cryptoHelper.Decrypt(credential.Password);
            
            return credential;
        }
        catch (Exception ex)
        {
            Log.Error(ex);
        }

        return null;
    }

    private void SetCurrentCredential(string server, int port, string username, string password)
    {
        try
        {
            SWISVerbsHelper.InvokeV3<SharedNetworkCredential>("Orion.PM.Management",
                                                            "SetCurrentDataGridCredentials",
                                                            server,
                                                            port,
                                                            username,
                                                            _cryptoHelper.Encrypt(password));
        }
        catch (Exception ex)
        {
            Log.Error(ex);
        }
    }

    private bool TestCredentialTest(string server, int port, string username, string password)
    {
        try
        {
            return SWISVerbsHelper.InvokeV3<bool>("Orion.PM.Management",
                                                "TestDataGridCredentials",
                                                server,
                                                port,
                                                username,
                                                _cryptoHelper.Encrypt(password));
        }
        catch (Exception ex)
        {
            Log.Error(ex);
        }

        return false;
    }
}