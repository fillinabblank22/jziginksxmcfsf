﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;

public partial class Orion_SCM_Admin_EditViews_EditWsusServerDetailsView : ProfilePropEditUserControl
{
    public override string PropertyValue
    {
        get { return ViewSelector.PropertyValue; }
        set { ViewSelector.PropertyValue = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}