﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditWsusNodeDetailsView.ascx.cs" Inherits="Orion_SCM_Admin_EditViews_EditWsusNodeDetailsView" %>
<%@ Register Src="~/Orion/Controls/SelectViewForViewType.ascx" TagPrefix="orion" TagName="SelectView" %>

<orion:SelectView runat="server" ID="ViewSelector" AllowViewsByDeviceType="false" ViewType="WsusNodeDetails" />
