<%@ Page Language="C#" MasterPageFile="~/Orion/PM/PMViewNoBreadCrumbs.master" AutoEventWireup="true" CodeFile="Summary.aspx.cs" Inherits="Orion_PM_Summary" Title="Patch Manager Summary" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>

<asp:Content runat="server" ContentPlaceHolderID="ViewPageTitle">
	<h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="MainContentPlaceHolder">
	<orion:ResourceHostControl ID="ResourceHostControl1" runat="server">
		<orion:ResourceContainer runat="server" ID="resContainer" />
	</orion:ResourceHostControl>
</asp:Content>
