﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using System.IO;

public partial class Orion_PM_NodePopup : System.Web.UI.UserControl
{
    private string _RevisedStatusDescription;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Node == null)
            return;
        
        this.ResponseTime.Value = this.Node.ResponseTime;
        SetupBar(this.ResponseTimeBar, this.Node.ResponseTime, 2500,
            Thresholds.ResponseTimeWarning.SettingValue, Thresholds.ResponseTimeError.SettingValue);

        this.AvgResponseTime.Value = this.Node.AvgResponseTime;
        SetupBar(this.AvgResponseTimeBar, this.Node.AvgResponseTime, 2500,
            Thresholds.ResponseTimeWarning.SettingValue, Thresholds.ResponseTimeError.SettingValue);

        this.MinResponseTime.Value = this.Node.MinResponseTime;
        SetupBar(this.MinResponseTimeBar, this.Node.MinResponseTime, 2500,
            Thresholds.ResponseTimeWarning.SettingValue, Thresholds.ResponseTimeError.SettingValue);

        this.MaxResponseTime.Value = this.Node.MaxResponseTime;
        SetupBar(this.MaxResponseTimeBar, this.Node.MaxResponseTime, 2500,
            Thresholds.ResponseTimeWarning.SettingValue, Thresholds.ResponseTimeError.SettingValue);

        this.PercentLoss.Value = (short)this.Node.PercentLoss;
        SetupBar(PercentLossBar, Node.PercentLoss, 100,
            Thresholds.PacketLossWarning.SettingValue, Thresholds.PacketLossError.SettingValue);

        this.CPULoad.Value = this.Node.CPULoad;
        SetupBar(CPULoadBar, Node.CPULoad, 100,
            Thresholds.CPULoadWarning.SettingValue, Thresholds.CPULoadError.SettingValue);

        this.MemoryUsed.Value = (short)this.Node.PercentMemoryUsed;
        SetupBar(MemoryUsedBar, Node.PercentMemoryUsed, 100,
            Thresholds.PercentMemoryWarning.SettingValue, Thresholds.PercentMemoryError.SettingValue);

        foreach (string controlPath in NetObjectFactory.GetNodeTipExtensionControls())
        {
            StringWriter sw = new StringWriter();
            HtmlTextWriter writer = new HtmlTextWriter(sw);

            Control control = Page.LoadControl(controlPath);
            control.RenderControl(writer);

            if (writer.InnerWriter.ToString().Trim().StartsWith("<tr", StringComparison.OrdinalIgnoreCase))
                extensionPlaceholder.Controls.Add(control);
            else
                oldExtensionPlaceholder.Controls.Add(control);
        }

        SetRevisedStatusDescription(this.Node);

        this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
    }

    public Node Node { get; set; }

    protected string GetVendorIcon()
    {
        string root = "/NetPerfMon/images/Vendors/";
        string icon = this.Node.VendorIcon.Trim();

        if (icon != String.Empty)
        {
            string fullPath = Server.MapPath(root + icon);

            if (System.IO.File.Exists(fullPath))
            {
                return root + icon;
            }
        }
        return root + "Unknown.gif";
    }

    public string GetRevisedStatusDescription()
    {
        return HttpUtility.HtmlEncode(_RevisedStatusDescription).Replace(",", ".<br/>");
    }

    private int? GetViewLimitationID()
    {
        string viewlim = Request.QueryString["viewlim"];
        int lim;

        if (SolarWinds.Orion.Web.Limitation.GetCurrentViewLimitationID() == 0 &&
            string.IsNullOrEmpty(viewlim) == false &&
            int.TryParse(viewlim, out lim))
            return lim;

        return null;
    }

    private void SetRevisedStatusDescription(Node node)
    {
        using (NodeChildStatusDAL dal = new NodeChildStatusDAL())
        {
            if (dal.GetChildStatusDescription(node.NodeID, GetViewLimitationID(), true, out _RevisedStatusDescription) == false)
                _RevisedStatusDescription = node.Description;

            _RevisedStatusDescription = _RevisedStatusDescription ?? "";
        }
    }

    private void SetupBar(InlineBar bar, float value, float max, double warning, double error)
    {
        if (value > max)
            max = value;
        bar.Percentage = 100.0 * value / max;
        if (value > error)
            bar.ThresholdLevelValue = InlineBar.ThresholdLevel.Error;
        else if (value > warning)
            bar.ThresholdLevelValue = InlineBar.ThresholdLevel.Warning;
        else
            bar.ThresholdLevelValue = InlineBar.ThresholdLevel.Normal;
    }

    protected bool HasOverviewStyle(params NodeOverviewStyle[] styles)
    {
        int styleValue;
        return Int32.TryParse(this.Request.QueryString["NodeOverviewStyle"], out styleValue) && styles.Any(s => (int)s == styleValue);
    }
}