﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.PM.Web.DAL;
using SolarWinds.Orion.Core.Web;
using SolarWinds.PM.Web.NetObjects;
using SolarWinds.Orion.Web;
using SolarWinds.PM.Web.Charting;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.PM.Web.Model;
using SolarWinds.PM.Web.Resources.Summary;

public partial class Orion_SCM_WsusServerPopup : System.Web.UI.Page
{
    private readonly string IpAddressCaption = "IP Address";
    private readonly string MachineTypeCaption = "Machine Type";
    private readonly string DescriptionCaption = "Description";
    private readonly string GraphCaption = "Servers Health";
    private readonly string OSDescriptionCaption = "OS Description";
    private readonly string PortCaption = "Port";
    private readonly string UseSSLCaption = "Use SSL";
    private readonly string StatusCaption = "Node Status";
    private readonly string UnmanagedNodeCaption = "Node is not managed by Orion";

    private readonly string ComputerUpToDateCaption = "Computers Up To Date";
    private readonly string ComputerNeedingUpdatesCaption = "Computers Needing Updates";
    private readonly string ComputerWithUnkownStatusCaption = "Computers with unknown status";
    private readonly string ComputerWithUpdatesErrorsCaption = "Computers With Updates Errors";

    private WsusServerEntity node = null;

    public Boolean IsNotOrion { get { return node == null ? true : node.OrionNodeId == default(Int32?); } }

	protected void Page_Load(object sender, EventArgs e)
    {
        new SolarWinds.PM.Web.TaskServices.Server.WsusServerDetails().Init(new Object[] { Guid.Empty, Request.Url, false }, null);

        var netObjReq = Request["NetObject"];
        bool justApprovedUpdates;
        string netObj = SolarWinds.PM.Web.Helper.ParseNetObjectIDHelper.ParseRequestNetObjectId(netObjReq, out justApprovedUpdates);


        var nodeProvider = NetObjectFactory.Create(netObj, true) as WsusServer;
        if (nodeProvider == null || nodeProvider.WsusServerDetails == null || nodeProvider.WsusServerDetails.Servername == null)
        {
            lblTitle.Text = "No content available";
            contentDiv.Style[HtmlTextWriterStyle.Display] = "none";
            return;
        }

        node = nodeProvider.WsusServerDetails;

        lblTitle.Text = node.Servername;


        if (node.OrionNodeId == default(Int32?))
        {
            lblStatusCaption.Text = StatusCaption;
            imgStatus.Src = "/Orion/images/StatusIcons/Small-Unknown.gif";
            lblStatus.Text = UnmanagedNodeCaption;
        }
        else
        {
            nodePopup.Node = NetObjectFactory.Create("N:" + node.OrionNodeId.Value, true) as Node;
        }

        rowOfIpAddress.Visible = node.IPAddress != null;

        lblIpAddressCaption.Text = IpAddressCaption;
        lblIpAddress.Text = MakeBreakableString(node.IPAddress);

        rowOfMachineType.Visible = !String.IsNullOrEmpty(node.OSProductType);
        lblMachineTypeCaption.Text = MachineTypeCaption;
        lblMachineType.Text = MakeBreakableString(node.OSProductType);
        imgMachineType.Src = node.OSProductType.ToLower().Contains("server") ? "/Orion/PM/images/device_16x16.gif" : "/NetPerfMon/images/Endpoints/endpoint.gif";

        rowOfPort.Visible = node.Port != 0;
        lblPortCaption.Text = PortCaption;
        lblPort.Text = MakeBreakableString(node.Port);

        lblUseSSLCaption.Text = UseSSLCaption;
        lblUseSSL.Text = MakeBreakableString(node.UseSSL);

        rowOfOSDescription.Visible = !String.IsNullOrEmpty(node.OSDescription);
        lblOSDescriptionCaption.Text = OSDescriptionCaption;
        lblOSDescription.Text = MakeBreakableString(node.OSDescription);
        imgVendor.Src = node.OsVendor.ToLower().Contains("microsoft") || node.OsVendor.ToLower().Contains("windows") ? "/NetPerfMon/images/Vendors/311.gif" : "/NetPerfMon/images/Vendors/Unknown.gif";

        rowOfDescription.Visible = !String.IsNullOrEmpty(String.Concat(node.OsVendor, node.OsType, node.OsVersion));
        lblDescriptionCaption.Text = DescriptionCaption;
        lblDescription.Text = MakeBreakableString(String.Format("{0} {1}, {2} ", node.OsVendor, node.OsType, node.OsVersion));

        

        var detailsData = new NodesHealthOverview.DetailsData()
        {
            ComputerUpToDate = ComputerUpToDateCaption,
            ComputerNeedingUpdates = ComputerNeedingUpdatesCaption,
            ComputerWithUnkownStatus = ComputerWithUnkownStatusCaption,
            ComputerWithUpdatesErrors = ComputerWithUpdatesErrorsCaption,
            Filter = "Server",
            JustApprovedUpdates = justApprovedUpdates,
        };

        var table = new NodesHealthOverview().GetHealthOverview(node.DeviceID, detailsData, null);
        var computerUpToDateCount = Convert.ToInt32(table.Rows[0]["Value"]);
        var computerNeedingUpdatesCount = Convert.ToInt32(table.Rows[1]["Value"]);
        var computerWitchUknownStatusCount = Convert.ToInt32(table.Rows[2]["Value"]);
        var computerWithUpdatesErrorsCount = Convert.ToInt32(table.Rows[3]["Value"]);


        rowOfGraph.Visible = computerUpToDateCount + computerNeedingUpdatesCount + computerWithUpdatesErrorsCount + computerWitchUknownStatusCount > 0;
        lblGraph.Text = GraphCaption;
        var chartInfo = SolarWinds.Orion.Web.Charting.ChartInfoFactory.Create("NodesHealthOverview") as NodesHealthOverviewChartInfo;
        chartInfo.Height = chartInfo.Width = 80;

        chartInfo.ComputerUpToDateCount = computerUpToDateCount;
        chartInfo.ComputerNeedingUpdatesCount = computerNeedingUpdatesCount;
        chartInfo.ComputerWithUpdatesErrorsCount = computerWithUpdatesErrorsCount;
        chartInfo.ComputerWithUnkownStatusCount = computerWitchUknownStatusCount;
        chartInfo.WsusServer = node.DeviceID;

        imgGraphHealth.ChartInfo = chartInfo;

        lblComputerUpToDateCaption.Text = ComputerUpToDateCaption;
        lblComputerNeedingUpdatesCaption.Text = ComputerNeedingUpdatesCaption;
        lblComputerWithUknownStatusCaption.Text = ComputerWithUnkownStatusCaption;
        lblComputerWithUpdatesErrorsCaption.Text = ComputerWithUpdatesErrorsCaption;

        lblComputerUpToDateCount.Text = chartInfo.ComputerUpToDateCount.ToString();
        lblComputerNeedingUpdatesCount.Text = chartInfo.ComputerNeedingUpdatesCount.ToString();
        lblComputerWithUknownStatusCount.Text = chartInfo.ComputerWithUnkownStatusCount.ToString();
        lblComputerWithUpdatesErrorsCount.Text = chartInfo.ComputerWithUpdatesErrorsCount.ToString();

        this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
    }

    protected string MakeBreakableString(object val)
    {
        if (val == null)
            return String.Empty;

        if (Request.Browser.Browser == "IE" && Request.Browser.MajorVersion == 6)
            return FormatHelper.MakeBreakableString(val.ToString(), true);
        else
            return FormatHelper.MakeBreakableString(val.ToString(), false);
    }
}
