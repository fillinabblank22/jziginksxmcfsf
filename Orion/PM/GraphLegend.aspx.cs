﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using SolarWinds.PM.Web.DAL;
using SolarWinds.PM.Web.Model.ChartLegendModel;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_PM_GraphLegend : System.Web.UI.Page
{
    private const Int32 MAX_COUNT_OF_LEGEND_RECORDS = 100;

    private String index;
    private Int32 totalRows;

    public String CustomErrorMessage { get; set; }

    protected List<String> Header { get; set; }
    protected Legend Legend { get; set; }
    protected LegendRow LegendRow { get; set; }
    protected LegendColumn LegendColumn { get; set; }

    protected String GetGroup { get { return String.Format("Sel_{0}", this.Legend == null ? String.Empty : this.Legend.Name.GetHashCode().ToString()); } }
    protected String GetStyleDetail { get { return this.Legend != null && this.Legend.Name != this.index ? "display: none;" : String.Empty; } }
    protected String GetImage { get { return String.Format("/Orion/images/Button.{0}.gif", this.Legend != null && this.Legend.Name != this.index ? "Expand" : "Collapse"); } }
    protected String GetColumnWrapping { get { return this.LegendColumn != null && this.LegendColumn.CanBeWrapped ? "wrap;" : "nowrap"; } }
    protected String GetDisplayOfMessage { get { return this.totalRows > MAX_COUNT_OF_LEGEND_RECORDS ? String.Empty : "display: none;"; } }
    protected String MoreRecordsIsHiddenCaption { get { return String.Format("Other {0} items are not displayed", Math.Max(this.totalRows - MAX_COUNT_OF_LEGEND_RECORDS, 0)); } }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.index = Request.QueryString["Index"];
            var sessionKey = Request.QueryString["Legend"];
            if (Session[sessionKey + "Delegate"] == null)
                throw new Exception("Legend ID does not exist.");

            var methodSubscription = Session[sessionKey + "Delegate"].ToString().Split(';');
            var typeString = methodSubscription[0];
            var methodString = methodSubscription[1];

            var type = Type.GetType(typeString);
            var instance = Activator.CreateInstance(type);
            var method = type.GetMethod(methodString);

            var legend = method.Invoke(instance, new object[] { sessionKey, this.index }) as ClickableLegendEntity;
            this.Header = legend.Header;

            this.helpLink.HRef = HelpHelper.GetHelpUrl(legend.HelpFragment);

            this.resourceTable.DataSource = legend.Data;
            this.resourceTable.DataBind();

            lblTitle.Text = String.Format("{0} - Legend Details", legend.Title);
            Page.Title = lblTitle.Text;
        }
        catch (Exception ex)
        {
            this.CustomErrorMessage = ex.Message;
            this.SQLErrorPanel.Visible = true;
            return;
        }
    }

    protected void resourceTable_ItemCreated(object sender, RepeaterItemEventArgs e)
    {
        this.Legend = e.Item.DataItem as Legend;

        this.totalRows = this.Legend != null ? this.Legend.Rows.Count : 0;
        if (this.totalRows > MAX_COUNT_OF_LEGEND_RECORDS)
            this.Legend.Rows = this.Legend.Rows.Take(MAX_COUNT_OF_LEGEND_RECORDS).ToList();
    }

    protected void resourceTableRow_ItemCreated(object sender, RepeaterItemEventArgs e)
    {
        this.LegendRow = e.Item.DataItem as LegendRow;
    }

    protected void resourceTableColumn_ItemCreated(object sender, RepeaterItemEventArgs e)
    {
        this.LegendColumn = e.Item.DataItem as LegendColumn;
    }
}