﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UpdateNodesDetail.ascx.cs"
    Inherits="Orion_PM_Resources_Update_UpdateNodesDetail" EnableViewState="false" %>

<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="task" TagName="TaskBaseControl" Src="~/Orion/PM/Controls/TaskBaseControl.ascx" %>


<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <HeaderButtons>
        <task:TaskBaseControl runat="server" ID="wsusTask" />
    </HeaderButtons>
    <Content>
        <div id="dataDiv" runat="server" ></div>
    </Content>
</orion:ResourceWrapper>