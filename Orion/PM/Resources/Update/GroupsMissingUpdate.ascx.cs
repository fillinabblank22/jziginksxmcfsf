﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.PM.Web.Charting;
using SolarWinds.PM.Web.DAL;
using System.Data;
using SolarWinds.PM.Web.Resources;
using SolarWinds.PM.Web.NetObjects;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
public partial class Orion_PM_Resources_Update_GroupsMissingUpdate : TopXXScmBaseResource
{
    #region properties

    protected override string TitleTemplate
    {
        get
        {
            if (Resource.Title.Equals(DefaultTitle, StringComparison.OrdinalIgnoreCase)
                && string.IsNullOrEmpty(Resource.Properties["MaxEvents"]))
            {
                return String.Format("Top {0} Groups Missing Update", this.MaxRecords);
            }
            return Resource.Title;
        }
    }
    
    public override string EditControlLocation
    {
        get { return "/Orion/PM/Controls/EditResourceControls/EditGroupsMissingUpdate.ascx"; }
    }

    // overriden DefaultTitle
    protected override string DefaultTitle
    {
        get { return "Top XX Groups Missing Update"; }
    }

    // overriden HelpLink
    public override string HelpLinkFragment
    {
        get
        {
            return "SPMPH_OrionResource_Top_XX_Groups_Missing_Update";
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IUpdateProvider) }; }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        IUpdateProvider updateProvider = GetInterfaceInstance<IUpdateProvider>();
        if (updateProvider == null)
        {
            this.Visible = false;
            return;
        }

        Int32 criticalThreshold;
        if (!Int32.TryParse(this.Resource.Properties["CriticalThreshold"] ?? "15", out criticalThreshold))
            criticalThreshold = 15;

        Int32 warningThreshold;
        if (!Int32.TryParse(this.Resource.Properties["WarningThreshold"] ?? "8", out warningThreshold))
            warningThreshold = 8;

        string justApprovedUpdatesString = this.Resource.Properties["JustApprovedUpdates"] ?? "true";
        Boolean justApprovedUpdates = Boolean.Parse(justApprovedUpdatesString);

        string updateInstallationState = this.Resource.Properties["UpdateInstallationState"] ?? "";
 

        var cleanTimeStampsExecution = new TaskExecutionEntity()
        {
            HtmlServerMethod = new SolarWinds.PM.Web.TaskServices.Update.UpdateGroup().Clear,
            Parameters = new object[] { updateProvider.Update.NetObjectID },
        };
        var showResourceExecution = new TaskExecutionEntity()
        {
            ServerResourceControl = new ServerResourceModel("/Orion/PM/Controls/Update/GroupsMissingUpdateCtrl.ascx"
                                                          , new Dictionary<string, object>()
                                                            {
                                                                { "MaxRecords", this.MaxRecords},
                                                                { "JustApprovedUpdates", justApprovedUpdates },
                                                                { "UpdateInstallationState", updateInstallationState },
                                                                { "WarningThreshold", warningThreshold },
                                                                { "CriticalThreshold", criticalThreshold },
                                                                { "NetObjectId", updateProvider.Update.NetObjectID },
                                                            }
                                                          , new SolarWinds.PM.Web.TaskServices.Update.UpdateGroup().Load
                                                          , new object[] { updateProvider.Update.NetObjectID }),
            EnableImmediateRendering = true,
            ClientObject = dataDiv,
        };

        wsusTask.ExecutionList.Add(showResourceExecution);

        wsusTask.RefreshExecutionList.Add(cleanTimeStampsExecution);
        wsusTask.RefreshExecutionList.Add(showResourceExecution);

        if (this.MasterRefresher != null)
        {
            ((ASP.orion_pm_controls_taskbasecontrol_ascx)this.MasterRefresher).RefreshExecutionList.Add(cleanTimeStampsExecution);
        }
    }
}
