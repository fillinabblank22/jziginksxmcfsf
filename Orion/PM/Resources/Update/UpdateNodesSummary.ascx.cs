﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.PM.Web.Charting;
using SolarWinds.PM.Web.DAL;
using System.Data;
using SolarWinds.PM.Web.Resources;
using SolarWinds.PM.Web.NetObjects;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.PieCharts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsPieChartsValues.Charts)]
public partial class Orion_PM_Resources_Update_UpdateNodesSummary : ScmBaseResource
{
    #region properties

    public override string EditControlLocation
    {
        get { return "/Orion/PM/Controls/EditResourceControls/EditWSUSNodeUpdatesOverview.ascx"; }
    }

    // overriden DefaultTitle
    protected override string DefaultTitle
    {
        get { return "Nodes with Update - Summary"; }
    }

    // overriden HelpLink
    public override string HelpLinkFragment
    {
        get
        {
            return "SPMPH_OrionResource_Nodes_with_Update_Summary";
        }
    }

    public string LegendHelpLinkFragment
    {
        get
        {
            return "SPMPH_OrionResource_Nodes_with_Update_Summary_Legend";
        }
    }    
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        IUpdateProvider updateProvider = GetInterfaceInstance<IUpdateProvider>();
        if (updateProvider == null)
        {
            this.Visible = false;
            return;
        }

        string justApprovedUpdatesString = this.Resource.Properties["JustApprovedUpdates"] ?? "true";
        Boolean justApprovedUpdates = Boolean.Parse(justApprovedUpdatesString);

        
        var cleanTimeStampsExecution = new TaskExecutionEntity()
        {
            HtmlServerMethod = new SolarWinds.PM.Web.TaskServices.Update.WsusNodesBase().Clear,
            Parameters = new object[] { updateProvider.Update.NetObjectID },
            IsRedirectable = true,
        };
        var showResourceExecution = new TaskExecutionEntity()
        {
            ServerResourceControl = new ServerResourceModel("/Orion/PM/Controls/Update/UpdateNodesSummaryCtrl.ascx"
                                                          , new Dictionary<string, object>()
                                                            {
                                                                { "JustApprovedUpdates", justApprovedUpdates },
                                                                { "NetObjectId", updateProvider.Update.NetObjectID },
                                                                { "DefaultTitle", String.Concat(updateProvider.Update.Name, " - ", this.DefaultTitle) },
                                                                { "LegendHelpFragment", this.LegendHelpLinkFragment},
                                                            }
                                                          , new SolarWinds.PM.Web.TaskServices.Update.WsusNodesBase().Load
                                                          , new object[] { updateProvider.Update.NetObjectID }),
            EnableImmediateRendering = true,
            ClientObject = dataDiv,
        };

        wsusTask.ExecutionList.Add(base.InitUpdateData);
        wsusTask.ExecutionList.Add(showResourceExecution);

        wsusTask.RefreshExecutionList.Add(cleanTimeStampsExecution);
        wsusTask.RefreshExecutionList.Add(showResourceExecution);

        if (this.MasterRefresher != null)
        {
            ((ASP.orion_pm_controls_taskbasecontrol_ascx)this.MasterRefresher).RefreshExecutionList.Insert(0, cleanTimeStampsExecution);
        }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IUpdateProvider) }; }
    }
}
