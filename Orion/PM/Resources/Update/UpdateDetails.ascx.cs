﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.PM.Web.Charting;
using SolarWinds.PM.Web.DAL;
using System.Data;
using SolarWinds.PM.Web.Resources;
using SolarWinds.PM.Web.NetObjects;
using System.Web.UI.HtmlControls;
using SolarWinds.Logging;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_PM_Resources_Update_UpdateDetails : ScmBaseResource
{
    #region properties

    // overriden DefaultTitle
    protected override string DefaultTitle
    {
        get { return "Update Details"; }
    }

    // overriden HelpLink
    public override string HelpLinkFragment
    {
        get
        {
            return "SPMPH_OrionResource_Update_Details";
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        IUpdateProvider updateProvider = GetInterfaceInstance<IUpdateProvider>();

        if (updateProvider == null)
        {
            log.Error("Unable to get update net object for current view");
            this.Visible = false;
            return;
        }


        var cleanTimeStampsExecution = new TaskExecutionEntity()
        {
            HtmlServerMethod = new SolarWinds.PM.Web.TaskServices.Update.UpdateDetails().Clear,
            Parameters = new object[] { updateProvider.Update.UpdateDetails.WsusServer.DeviceID },
            IsRedirectable = true,
        };
        var showResourceExecution = new TaskExecutionEntity()
        {
            ServerResourceControl = new ServerResourceModel("/Orion/PM/Controls/Update/UpdateDetailsCtrl.ascx"
                                                          , new Dictionary<string, object>()
                                                            {
                                                                { "NetObjectId", updateProvider.Update.NetObjectID },
                                                            }
                                                          , new SolarWinds.PM.Web.TaskServices.Update.UpdateDetails().Load
                                                          , new object[] { updateProvider.Update.UpdateDetails.WsusServer.DeviceID }),
            EnableImmediateRendering = true,
            ClientObject = dataDiv,
        };

        wsusTask.ExecutionList.Add(base.InitUpdateData);
        wsusTask.ExecutionList.Add(showResourceExecution);

        wsusTask.RefreshExecutionList.Add(cleanTimeStampsExecution);
        wsusTask.RefreshExecutionList.Add(showResourceExecution);


        if (this.MasterRefresher != null)
        {
            ((ASP.orion_pm_controls_taskbasecontrol_ascx)this.MasterRefresher).RefreshExecutionList.Add(cleanTimeStampsExecution);
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IUpdateProvider) }; }
    }

    private static readonly Log log = new Log();
}
