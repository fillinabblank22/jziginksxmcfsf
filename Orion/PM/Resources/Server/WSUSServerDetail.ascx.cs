﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.PM.Web.Resources;
using SolarWinds.PM.Web.NetObjects;
using SolarWinds.PM.Web.Charting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Common;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_PM_Resources_Server_WSUSServerDetail : ScmBaseResource
{
    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IWsusServerProvider) }; }
    }

    #region properties

    // overriden DefaultTitle
    protected override string DefaultTitle
    {
        get { return "WSUS Server Details"; }
    }

    // overriden HelpLink
    public override string HelpLinkFragment
    {
        get
        {
            return "SPMPH_OrionResource_WSUS_Server_Details";
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        IWsusServerProvider serverProvider = GetInterfaceInstance<IWsusServerProvider>();

        if (serverProvider == null)
        {
            log.Error("Unable to get wsus server net object for current view");
            this.Visible = false;
        }

        var cleanTimeStampsExecution = new TaskExecutionEntity()
        {
            HtmlServerMethod = new SolarWinds.PM.Web.TaskServices.Server.WsusServerDetails().Clear,
            Parameters = new object[] { serverProvider.Node.WsusServerDetails.DeviceID },
            IsRedirectable = true,
        };
        var showResourceExecution = new TaskExecutionEntity()
        {
            ServerResourceControl = new ServerResourceModel("/Orion/PM/Controls/Server/WSUSServerDetailCtrl.ascx"
                                                          , new Dictionary<string, object>()
                                                            {
                                                                { "NetObjectId", serverProvider.Node.NetObjectID },
                                                            }
                                                          , new SolarWinds.PM.Web.TaskServices.Server.WsusServerDetails().Load
                                                          , new object[] { serverProvider.Node.WsusServerDetails.DeviceID }),
            EnableImmediateRendering = true,
            ClientObject = dataDiv,
        };

        wsusTask.ExecutionList.Add(base.InitServerData);
        wsusTask.ExecutionList.Add(showResourceExecution);

        wsusTask.RefreshExecutionList.Add(cleanTimeStampsExecution);
        wsusTask.RefreshExecutionList.Add(showResourceExecution);

        if (this.MasterRefresher != null)
        {
            ((ASP.orion_pm_controls_taskbasecontrol_ascx)this.MasterRefresher).RefreshExecutionList.Add(cleanTimeStampsExecution);
        }
    }

    private static readonly Log log = new Log();
}