﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using System.Data;
using System.Data.SqlClient;
using SolarWinds.PM.Web.DAL;
using SolarWinds.PM.Web.Resources;
using SolarWinds.PM.Web.Helper;
using SolarWinds.Orion.Web;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
public partial class Orion_PM_Resources_Summary_LastXXTasks : TopXXScmBaseResource
{
    protected override string TitleTemplate
    {
        get
        {
            if (Resource.Title.Equals(DefaultTitle, StringComparison.OrdinalIgnoreCase)
                && string.IsNullOrEmpty(Resource.Properties["MaxEvents"]))
            {
                return String.Format("Last {0} Tasks", this.MaxRecords);
            }
            return Resource.Title;
        }
    }

    public override string EditURL
    {
        get
        {
            string str = string.Format("/Orion/PM/Controls/EditResourceControls/EditTopXX.aspx?ResourceID={0}", this.Resource.ID);
            if (!string.IsNullOrEmpty(base.Request["NetObject"]))
            {
                str = string.Format("{0}&NetObject={1}", str, base.Request["NetObject"]);
            }
            if (this.Page is OrionView)
            {
                str = string.Format("{0}&ViewID={1}", str, ((OrionView)this.Page).ViewInfo.ViewID);
            }
            if (!string.IsNullOrEmpty(base.Request.QueryString["ThinAP"]))
            {
                str = string.Format("{0}&ThinAP={1}", str, base.Request.QueryString["ThinAP"]);
            }
            return str;
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/PM/Controls/EditResourceControls/EditLastXXTasks.ascx"; }
    }

    // overriden dafault title
    protected override string DefaultTitle
    {
        get { return "Last XX Tasks"; }
    }

    public override string HelpLinkFragment
    {
        get { return "SPMPH_OrionResource_Last_Tasks"; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Wrapper.ShowEditButton = this.EnableEdit;

        var showResourceExecution = new TaskExecutionEntity()
        {
            ServerResourceControl = new ServerResourceModel("/Orion/PM/Controls/Summary/LastXXTasksCtrl.ascx"
                                                          , new Dictionary<String, Object>() 
                                                            {
                                                              { "MaxRecords", this.MaxRecords },
                                                              { "Filter", this.Resource.Properties["Filter"]},
                                                          }),
            ClientObject = dataDiv,
        };

        wsusTask.ExecutionList.Add(showResourceExecution);

    }
}
