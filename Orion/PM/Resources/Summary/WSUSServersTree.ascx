﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WSUSServersTree.ascx.cs" Inherits="Orion_PM_Resources_Summary_WSUSServersTree" EnableViewState="false" %>

<%@ Register TagPrefix="AjaxTree" Assembly="SolarWinds.PM.Web" Namespace="SolarWinds.PM.Web.AjaxTree.AjaxTreeControl" %>
<%@ Register TagPrefix="task" TagName="TaskBaseControl" Src="~/Orion/PM/Controls/TaskBaseControl.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <HeaderButtons>
        <script type="text/javascript">
            function Orion_PM_Resources_Summary_WSUSServersTreeRefreshTree(treeClientName) {
                var expanderMethodHolder = $('#' + treeClientName + '-expander');
                if (expanderMethodHolder != null) {
                    eval(expanderMethodHolder.attr("script"));

                    setTimeout(expanderMethodHolder.attr("script"), 300);
                }
            }
        </script>
        <task:TaskBaseControl runat="server" ID="wsusTask" />
    </HeaderButtons>
    <Content>
        <div runat="server" ID="lblNoDataToDisplay"></div> 

        <AjaxTree:TreeControl ID="tree" runat="server">
            <WebService Path="~/Orion/PM/Services/AssetTreeService.asmx" />
        </AjaxTree:TreeControl>

        <script type="text/javascript">
            function Orion_PM_Resources_Summary_WSUSServersTreeCheckErrorBox(treeClientName, lblNoDataToDisplay) {
                $('#' + lblNoDataToDisplay).hide();

                var expanderMethodHolder = $('#' + treeClientName + '-expander');
                if (expanderMethodHolder != null) {
                    eval(expanderMethodHolder.attr('script'));
                }
            }
        </script>
    </Content>
</orion:resourceWrapper>
