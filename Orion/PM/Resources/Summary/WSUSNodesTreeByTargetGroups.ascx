﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WSUSNodesTreeByTargetGroups.ascx.cs" Inherits="Orion_PM_Resources_Summary_WSUSNodesTreeByTargetGroups" EnableViewState="false" %>

<%@ Register TagPrefix="AjaxTree" Assembly="SolarWinds.PM.Web" Namespace="SolarWinds.PM.Web.AjaxTree.AjaxTreeControl" %>
<%@ Register TagPrefix="task" TagName="TaskBaseControl" Src="~/Orion/PM/Controls/TaskBaseControl.ascx" %>

<orion:Include runat="server" File="js/OrionMinReqs.js/json2.js" />
<orion:Include runat="server" File="JSHelper.js" Module="PM" />
<orion:Include runat="server" Module="PM" File="MoreItemBox.css" />

<script language="javascript" type="text/javascript">
    function Orion_PM_Resources_Summary_WSUSNodesTreeByTargetGroups_expanderClick(id) {
        var image = $("#exp_" + id);
        var src = $(image).attr("src");

        if (src.indexOf("/Orion/images/Button.Expand.gif") == -1) {
            $(image).attr("src", "/Orion/images/Button.Expand.gif");
            $(image).attr("alt", "[+]");
            $("#row_" + id).hide();
        }
        else {
            $(image).attr("src", "/Orion/images/Button.Collapse.gif");
            $(image).attr("alt", "[-]");
            $("#row_" + id).show();
        }
    }

    function Orion_PM_Resources_Summary_WSUSNodesTreeByTargetGroups_showAllNodes(id) {
        SendMessage("/Orion/PM/Services/WsusNodesByTargetGroupsPageService.asmx", "TreeFilterWithSearch", { viewDetailId: $("#viewDetailId_" + id).val(), nodeId: $("#params_" + id).val(), search: "" }, function (result) {
            window.location = "/Orion/PM/WsusNodesByTargetGroupsPage/WsusNodesByTargetGroupsPage.aspx?ViewDetailId=" + $("#viewDetailId_" + id).val();
        });
    }

    function Orion_PM_Resources_Summary_WSUSNodesTreeByTargetGroups_searchClick(id) {
        var search = $("#txt_" + id).val();
        if (CheckValidityInput(search) == false)
            return;

        SendMessage("/Orion/PM/Services/WsusNodesByTargetGroupsPageService.asmx", "TreeFilterWithSearch", { viewDetailId: $("#viewDetailId_" + id).val(), nodeId: $("#params_" + id).val(), search: search }, function (result) {
            window.location = "/Orion/PM/WsusNodesByTargetGroupsPage/WsusNodesByTargetGroupsPage.aspx?ViewDetailId=" + $("#viewDetailId_" + id).val();
        });
    }
</script>
<orion:resourceWrapper runat="server" ID="Wrapper">
    <HeaderButtons>
        <script type="text/javascript">
            function Orion_PM_Resources_Summary_WSUSNodesTreeByTargetGroupsRefreshTree(treeClientName) {
                var expanderMethodHolder = $('#' + treeClientName + '-expander');
                if (expanderMethodHolder != null) {
                    eval(expanderMethodHolder.attr("script"));

                    setTimeout(expanderMethodHolder.attr("script"), 300);
                }
            }
        </script>
        <task:TaskBaseControl runat="server" ID="wsusTask" />
    </HeaderButtons>
    <Content>
        <div runat="server" ID="lblNoDataToDisplay"></div> 

        <AjaxTree:TreeControl ID="tree" runat="server">
            <WebService Path="~/Orion/PM/Services/AssetTreeService.asmx" />
        </AjaxTree:TreeControl>

        <script type="text/javascript">
            function Orion_PM_Resources_Summary_WSUSNodesTreeByTargetGroupsCheckErrorBox(treeClientName, lblNoDataToDisplay) {
                $('#' + lblNoDataToDisplay).hide();

                var expanderMethodHolder = $('#' + treeClientName + '-expander');
                if (expanderMethodHolder != null) {
                    eval(expanderMethodHolder.attr('script'));
                }
            }
        </script>
    </Content>
</orion:resourceWrapper>
