﻿using System;
using System.Text;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.PM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, "Patch Manager")]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_PM_Resources_Summary_ComputerUpdateStatusReportResource : ScmBaseResource
{
    private string getFilterValues(string filterName, string columnToFilterBy)
    {
        string[] filterList = (Resource.Properties.ContainsKey(filterName) ? Resource.Properties[filterName] : "").Split(',');
        StringBuilder filterQueryPart = new StringBuilder();

        filterQueryPart.AppendFormat(" {0} LIKE ", columnToFilterBy);

        for (int i = 0; i < filterList.Length; i++)
        {
            filterQueryPart.AppendFormat("'%{0}%'", filterList[i].TrimStart('\r', '\n', ' ').TrimEnd('r', '\n', ' '));

            if (i < filterList.Length - 1)
            {
                filterQueryPart.AppendFormat(" OR {0} LIKE ", columnToFilterBy);
            }
        }

        return filterQueryPart.ToString();
    }

    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public string PatchFilter => getFilterValues("patch_filter", "updates.title");

    public string MachineFilter => getFilterValues("machine_filter", "computers.fulldomainname");

    public string WsusGroupFilter => getFilterValues("wsus_group_filter", "computers.ComputerMembershipsDisplayables_GroupMemberships");

    public string FilterString => $"WHERE{PatchFilter} {PatchMachineRadioFilter} {MachineFilter} {MachineWsusGroupRadioFilter} {WsusGroupFilter}{ApprovedFilter}";

    public string PatchMachineRadioFilter
    {
        get { return Resource.Properties.ContainsKey("patch_machine_radio") ? Resource.Properties["patch_machine_radio"] : "AND"; }
    }

    public string MachineWsusGroupRadioFilter
    {
        get { return Resource.Properties.ContainsKey("machine_wsus_group_radio") ? Resource.Properties["machine_wsus_group_radio"] : "AND"; }
    }

    public string ApprovedFilter
    {
        get
        {
            string approved_filter = "";

            switch (Resource.Properties.ContainsKey("approved_filter")
                ? Resource.Properties["approved_filter"] : "APPROVED")
            {
                case "APPROVED":
                    approved_filter = " AND updates.IsApproved = TRUE";
                    break;
                case "NOTAPPROVED":
                    approved_filter = " AND updates.IsApproved = FALSE";
                    break;
            }

            return approved_filter;
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/PM/Controls/EditResourceControls/EditComputerUpdateStatusReportResource.ascx"; }
    }

    protected override string DefaultTitle
    {
        get { return "Computer Update Status"; }
    }

    public override string EditURL
    {
        get
        {
            return string.Format("/Orion/PM/Controls/EditResourceControls/EditTopXX.aspx?ResourceID={0}", ScriptFriendlyResourceID);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        CustomTable.SWQL = SWQL;
        CustomTable.OrderBy = OrderBy;
    }

    public string SWQL
    {
        get
        {
            var query =
                $@"SELECT	updates.title as [Title],
		computers.fulldomainname as [Hostname],
		computers.ComputerMembershipsDisplayables_GroupMemberships as [WSUS Group],
		(CASE 
			WHEN tasks.operationresult = 1 
			THEN MAX(tasks.operationcompletiontime)
			ELSE NULL
			END) as [Install Date],
		(CASE WHEN updates.IsApproved = 1 THEN 'True' ELSE 'False' END) as [Approved],
		updates.MsrcSeverity as [Severity],
		(CASE WHEN updateInstallInfos.UpdateInstallationState IS NULL THEN NULL
		ELSE
		CASE updateInstallInfos.UpdateInstallationState
			WHEN 4 THEN 'Installed'
			WHEN 3 THEN 'Not Installed (Downloaded)'
			WHEN 2 THEN 'Not Installed'
			WHEN 0 THEN 'Not Applicable'
			ELSE 'Other'
		END
		End) AS [Installation State]
FROM Orion.PM.dt_wsus_updates_installinfo_details as updateInstallInfos
INNER JOIN Orion.PM.dt_wsus_computers as computers ON computers.RID_Id = updateInstallInfos.ComputerRID AND computers.pkdeviceid = updateInstallInfos.pkdeviceid
INNER JOIN Orion.PM.dt_wsus_updates as updates ON updateInstallInfos.UpdateId = updates.Id_UpdateId AND updateInstallInfos.pkdeviceid = updates.pkdeviceid
LEFT JOIN Orion.PM.ewtasksresults as tasks ON tasks.operationname = 'Install' AND tasks.operationresult = 1 AND updates.title = tasks.objectname AND tasks.deviceconnectkey = computers.fulldomainname
{FilterString}
GROUP BY updates.title, computers.fulldomainname, tasks.operationresult, updateInstallInfos.UpdateInstallationState, computers.ComputerMembershipsDisplayables_GroupMemberships, updates.MsrcSeverity, updates.IsApproved";

            return query;
        }
    }

    public string OrderBy
    {
        get
        {
            return "computers.fulldomainname DESC";
        }
    }
}