﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.PM.Web.Resources;
using SolarWinds.PM.Web.Helper;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
public partial class Orion_PM_Resources_Summary_ThwackRecentPosts : TopXXResourceControl
{
    protected readonly string SubTitleCaption = "Recent {0} Patch Manager Posts:";
    protected readonly string DefaultSubTitleCaption = "Recent Patch Manager Posts:";

    protected readonly string ViewAllCaption = "View All";
    protected readonly string JoinThwackCaption = "Join thwack community";
    protected readonly string ErrorMessage = "Unable to contact thwack server.";
    protected readonly string ThwackLogoCaption = "Thwack logo";

    #region properties

    public override string EditURL
    {
        get
        {
            var thwackData = new object[] { false, 10 };
            string str = string.Format("/Orion/PM/Controls/EditResourceControls/EditTopXX.aspx?ThwackData={0}&ResourceID={1}", Serializer.Serialize(thwackData), this.Resource.ID);
            if (!string.IsNullOrEmpty(base.Request["NetObject"]))
            {
                str = string.Format("{0}&NetObject={1}", str, base.Request["NetObject"]);
            }
            if (this.Page is OrionView)
            {
                str = string.Format("{0}&ViewID={1}", str, ((OrionView)this.Page).ViewInfo.ViewID);
            }
            if (!string.IsNullOrEmpty(base.Request.QueryString["ThinAP"]))
            {
                str = string.Format("{0}&ThinAP={1}", str, base.Request.QueryString["ThinAP"]);
            }
            return str;
        }
    }

    protected override string TitleTemplate { get { return this.DefaultTitle; } }

    // overriden dafault title
    protected override string DefaultTitle
    {
        get { return String.Format("THWACK {0}", DefaultSubTitleCaption); }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "SPMPH_OrionResource_thwack";
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        this.HelpButton.HelpUrlFragment = this.HelpLinkFragment;
    }

}