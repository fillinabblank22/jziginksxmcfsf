﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI.Localizer;
using SolarWinds.PM.Web.Resources;
using SolarWinds.PM.Web.AjaxTree;
using SolarWinds.PM.Web.Controls;
using SolarWinds.PM.Web.Controls.AssetTree;
using SolarWinds.PM.Web.Helper;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.PM.Web.NetObjects;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
public partial class Orion_PM_Resources_Summary_LatestPatches : TopXXScmBaseResource
{
    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[0]; }
    }

    protected override string TitleTemplate
    {
        get
        {
            if (Resource.Title.Equals(DefaultTitle, StringComparison.OrdinalIgnoreCase))
            {
                return String.Format("Latest {0} Patches on WSUS Servers", this.MaxRecords);
            }
            else if (Resource.Title.Equals(DefaultTitleSingular, StringComparison.OrdinalIgnoreCase))
            {
                return String.Format("Latest {0} Patches on WSUS Server", this.MaxRecords);
            }
            return Resource.Title;
        }
    }

    protected override string DefaultTitle
    {
        get { return "Latest XX Patches on WSUS Servers"; }
    }

    protected string DefaultTitleSingular
    {
        get { return "Latest XX Patches on WSUS Server"; }
    }

    public override string HelpLinkFragment
    {
        get { return "SPMPH_OrionResource_Latest_Patches_WSUS_Servers"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/PM/Controls/EditResourceControls/EditLastXXPatches.ascx"; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Guid wsusServer = Guid.Empty;
        IWsusServerProvider wsusNodeProvider = GetInterfaceInstance<IWsusServerProvider>();
        if (wsusNodeProvider != null && wsusNodeProvider.Node.WsusServerDetails != null)
            wsusServer = wsusNodeProvider.Node.WsusServerDetails.DeviceID;

        // null means user did not entered "None" manually; that would be String.Empty
        if (this.Resource.Properties[ResourceHelper.GROUPING_RP_KEY_LEVEL1] == null)
        {
            // set default grouping
            this.Resource.Properties[ResourceHelper.GROUPING_RP_KEY_LEVEL1] = DEFAULT_GROUPING_LEVEL1;
            this.Resource.SubTitle = ResourceHelper.GetGroupingSubtitle(DEFAULT_GROUPING_LEVEL1_CAPTION);
        }


        Int32 criticalThreshold;
        if (!Int32.TryParse(this.Resource.Properties["CriticalThreshold"] ?? "15", out criticalThreshold))
            criticalThreshold = 15;

        Int32 warningThreshold;
        if (!Int32.TryParse(this.Resource.Properties["WarningThreshold"] ?? "8", out warningThreshold))
            warningThreshold = 8;

        string excludeUpdatesString = Resource.Properties["ExcludeNotApplicableUpdates"] ?? "false";
        var excludeUpdates = Boolean.Parse(excludeUpdatesString);


        var cleanTimeStampsExecution = new TaskExecutionEntity()
        {
            HtmlServerMethod = new SolarWinds.PM.Web.TaskServices.Summary.UpdateServer().Clear,
            Parameters = new object[] { wsusServer },
            IsRedirectable = true,
        };
        var showResourceExecution = new TaskExecutionEntity()
        {
            ServerResourceControl = new ServerResourceModel("/Orion/PM/Controls/Summary/LatestPatchesCtrl.ascx"
                                                          , new Dictionary<String, Object>() 
                                                            {
                                                              { "MaxRecords", this.MaxRecords },
                                                              { "GroupingLevel", this.Resource.Properties[ResourceHelper.GROUPING_RP_KEY_LEVEL1] },
                                                              { "WarningThreshold", warningThreshold },
                                                              { "CriticalThreshold", criticalThreshold },
                                                              { "ExcludeNotApplicableUpdates", excludeUpdates },
                                                              { "NetObjectId", this.Request.QueryString["NetObject"]},
                                                          }
                                                          , new SolarWinds.PM.Web.TaskServices.Summary.UpdateServer().Load
                                                          , new object[] { wsusServer }),
            EnableImmediateRendering = true,
            ClientObject = dataDiv,
        };

        wsusTask.ExecutionList.Add(showResourceExecution);

        wsusTask.RefreshExecutionList.Add(cleanTimeStampsExecution);
        wsusTask.RefreshExecutionList.Add(showResourceExecution);

        if (this.MasterRefresher != null)
        {
            ((ASP.orion_pm_controls_taskbasecontrol_ascx)this.MasterRefresher).RefreshExecutionList.Add(cleanTimeStampsExecution);
        }
    }

    private const string DEFAULT_GROUPING_LEVEL1 = "ProductTitles";
    private const string DEFAULT_GROUPING_LEVEL1_CAPTION = "Product Titles";
}