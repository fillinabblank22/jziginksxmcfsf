﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PatchingTasks.ascx.cs" Inherits="Orion_PM_Controls_Summary_PatchingTasksControl" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>

<orion:resourceWrapper ID="Wrapper" runat="server" ShowEditButton="true">
    <Content>
        <div id="dialog" style="display: none;" title="Task Details"></div>
        <orion:CustomQueryTable runat="server" ID="CustomTable" />

        <script type="text/javascript">
            $(function () {
                var rowsOnCurrentPage = [];

                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= CustomTable.UniqueClientID %>,
                        initialPage: 0,
                        rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "5" %>,
                        allowSort: true,
                        onLoad: function (rows, columnsInfo) {
                            console.log("In OnLoad");
                            rowsOnCurrentPage = rows;
                            var uiRows = $('#Grid-' + <%= ScriptFriendlyResourceID %> + ' > tbody > tr');
                            var mainRows = $('#Grid-' + <%= ScriptFriendlyResourceID %>).find('tbody').children();
                            mainRows.each(function (index) {
                                if (index > 0)
                                    $(this).children().last().css("cursor", "pointer").css("color", "blue")
                                    .css("text-decoration", "underline");

                                var prevColor = $(this).css("background-color");
                                $(this).mouseenter(function () {
                                    $(this).css("background", "#aaa");
                                }).mouseleave(function () {
                                    $(this).css("background", prevColor);
                                });
                            });

                            $.each(rows, function (index, row) {
                                $(uiRows[index + 1]).on("click", (function () {
                                    $(document.body).css({ 'cursor': 'wait' });
                                    var taskId = rowsOnCurrentPage[index][0];
                                    var taskTitle = rowsOnCurrentPage[index][1];

                                    ORION.callWebService("/Orion/PM/Services/PatchingTasksWebService.asmx",
                                        "GetTasksResultsHtmlTableById", { "taskId": taskId }, function (response) {
                                            $('#dialog').empty();
                                            $('#dialog').append('<input id="filterTasksResults" type="text" style="margin-left: 5px;" placeholder="Filter..">');
                                            $('#filterTasksResults').on("keyup", function () {
                                                var value = $(this).val().toLowerCase();
                                                $('#tableTasksResults tr').filter(function () {
                                                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
                                                });
                                            });

                                            $('#dialog').append(response);
                                            $("#dialog").dialog();
                                            $("#ui-dialog-title-dialog").text(taskTitle);
                                            $("#dialog").parent().css("width", "70%");
                                            $("#dialog").parent().css("max-height", "80%");
                                            $("#dialog").parent().css("overflow-y", "auto");
                                            $("#dialog").parent().css("left", "10%");
                                            
                                            $(document.body).css({ 'cursor': 'default' });
                                        });
                                }));
                            });
                        }
                    });

                var refresh = function () { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
                // This registers resource in view refresh routine
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                // And finally loads initial data
                refresh();
            });
        </script>
    </Content>
</orion:resourceWrapper>

