﻿using System;
using SolarWinds.PM.Web.Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, "Patch Manager")]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_PM_Resources_Summary_LatestPatchesReportResource : ScmBaseResource
{
    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    public int LastCountDays
    {
        get
        {
            return Convert.ToInt32(Resource.Properties["lastx"] ?? "7");
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }


    public override string EditControlLocation
    {
        get { return "/Orion/PM/Controls/EditResourceControls/EditLatestPatchesReportResource.ascx"; }
    }

    protected override string DefaultTitle
    {
        get { return "Latest Patches"; }
    }

    public sealed override string SubTitle
    {
        get { return String.Format("Information within last {0} days", LastCountDays); }
    }

    public override string EditURL
    {
        get
        {
            string str = string.Format("/Orion/PM/Controls/EditResourceControls/EditTopXX.aspx?ResourceID={0}", ScriptFriendlyResourceID);
            return str;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        CustomTable.SWQL = SWQL;
        CustomTable.OrderBy = OrderBy;
    }

    public string SWQL
    {
        get
        {
            string dateend = SolarWinds.Orion.Web.Macros.ReformatDateTime(DateTime.Now.ToOADate());
            string datestart = SolarWinds.Orion.Web.Macros.ReformatDateTime(DateTime.Now.AddDays(-LastCountDays).ToOADate());

            string query = @"SELECT wsus_updates.CompanyTitles as [Company Title], wsus_updates.title as [Title], wsus_updates.Description as [Description], wsus_updates.KnowledgebaseArticles as [KB],
wsus_updates.SecurityBulletins as [Security Bulletins], wsus_updates.ArrivalDate as [Arrival Date]
FROM Orion.PM.dt_wsus_updates as wsus_updates
WHERE ToUtc(wsus_updates.ArrivalDate) > ToUtc(" + datestart + ") AND ToUtc(wsus_updates.ArrivalDate) < ToUtc(" + dateend + ")";

            return query;
        }
    }

    public string OrderBy
    {
        get
        {
            return "wsus_updates.CompanyTitles DESC";
        }
    }
}