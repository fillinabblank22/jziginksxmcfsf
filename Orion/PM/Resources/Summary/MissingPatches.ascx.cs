﻿using System;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.PM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, "Patch Manager")]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_PM_Resources_Summary_MissingPatches : ScmBaseResource
{
    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        CustomTable.SWQL = SWQL;
        CustomTable.OrderBy = OrderBy;
    }

    public string ApprovedFilter
    {
        get
        {
            string approved_filter = "";

            switch (Resource.Properties.ContainsKey("approved_filter")
                ? Resource.Properties["approved_filter"] : "APPROVED")
            {
                case "APPROVED":
                    approved_filter = " AND u.IsApproved = TRUE";
                    break;
                case "NOTAPPROVED":
                    approved_filter = " AND u.IsApproved = FALSE";
                    break;
            }

            return approved_filter;
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/PM/Controls/EditResourceControls/EditMissingPatches.ascx"; }
    }
    
    public override string EditURL
    {
        get
        {
            return string.Format("/Orion/PM/Controls/EditResourceControls/EditMissingPatches.ascx?ResourceID={0}", ScriptFriendlyResourceID);
        }
    }

    protected override string DefaultTitle
    {
        get { return "Missing Patches"; }
    }

    public string SWQL
    {
        get
        {
            return String.Format(@"SELECT v.title as Title, u.KnowledgebaseArticles AS [Knowledgebase Articles], u.SecurityBulletins as [Bulletin], u.Description as [Patch Description], u.IsApproved as [Approved], wsus_computers.fulldomainname as [Machine Name], wsus_computers.ComputerMembershipsDisplayables_GroupMemberships as [WSUS Group],
(CASE WHEN v.UpdateInstallationState IS NULL THEN NULL
ELSE
CASE v.UpdateInstallationState
    WHEN 4 THEN 'Installed'
    WHEN 3 THEN 'Not Installed (Downloaded)'
    WHEN 2 THEN 'Not Installed'
    WHEN 0 THEN 'Not Applicable'
    ELSE 'Other'
END
End) AS [Installation State]
FROM Orion.PM.WSUS_ComputerUpdateStatusToUpdatesView as v
INNER JOIN Orion.PM.dt_wsus_updates as u ON v.UpdateId = u.Id_UpdateId
INNER JOIN Orion.PM.dt_wsus_computers as wsus_computers ON wsus_computers.id = v.id
WHERE (v.UpdateInstallationState = 3 OR v.UpdateInstallationState = 2){0}", ApprovedFilter);
        }
    }

    public string OrderBy
    {
        get
        {
            return "v.title DESC";
        }
    }
}