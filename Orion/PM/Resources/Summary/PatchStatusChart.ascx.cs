﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using SolarWinds.PM.Web.NetObjects;
using SolarWinds.PM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataTypeValues.Reports)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsPieChartsValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_PM_Resources_Charts_PatchStatusChart : TopXXScmBaseResource
{
    #region properties

    protected override string TitleTemplate
    {
        get
        {
            if (Resource.Title.Equals(DefaultTitle, StringComparison.OrdinalIgnoreCase))
            {
                return String.Format("Top {0} Patches Missing", this.MaxRecords);
            }
            return Resource.Title;
        }
    }

    // overriden dafault title
    protected override string DefaultTitle
    {
        get { return "Patch Status Pie Chart Resources"; }
    }

    // overriden HelpLink
    public override string HelpLinkFragment
    {
        get
        {
            return "SPMPH_OrionResource_Top_Patches_Missing";
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        Guid deviceId = Guid.Empty;
        IWsusServerProvider wsusNodeProvider = GetInterfaceInstance<IWsusServerProvider>();
        if (wsusNodeProvider != null && wsusNodeProvider.Node.WsusServerDetails != null)
            deviceId = wsusNodeProvider.Node.WsusServerDetails.DeviceID;

        string showGraphString = Resource.Properties["ShowPieChart"] ?? "true";
        var showGraph = Boolean.Parse(showGraphString);

        bool justApprovedUpdates;
        if (!bool.TryParse(this.Resource.Properties["JustApprovedUpdates"] ?? "True", out justApprovedUpdates))
            justApprovedUpdates = true;

        var cleanTimeStampsExecution = new TaskExecutionEntity()
        {
            HtmlServerMethod = new SolarWinds.PM.Web.TaskServices.Summary.UpdateServer().Clear,
            Parameters = new object[] { deviceId },
        };
        var cleanTimeStampsExecutionDetails = new TaskExecutionEntity()
        {
            HtmlServerMethod = new SolarWinds.PM.Web.TaskServices.Summary.TopXXPatchesMissing().Clear,
            Parameters = new object[] { deviceId, this.MaxRecords, justApprovedUpdates },
        };
        var cleanTimeStampsExecutionNode = new TaskExecutionEntity()
        {
            HtmlServerMethod = new SolarWinds.PM.Web.TaskServices.Node.WsusNodeDetails().Clear,
            Parameters = new object[] { deviceId },
        };
        var showResourceExecution = new TaskExecutionEntity()
        {
            ServerResourceControl = new ServerResourceModel("/Orion/PM/Controls/Summary/TopXXPatchesMissingCtrl.ascx"
                                                          , new Dictionary<string, object>()
                                                            {
                                                                { "ShowGraph", showGraph},
                                                                { "MaxRecords", this.MaxRecords},
                                                                { "NetObjectId", this.Request.QueryString["NetObject"]},
                                                                { "JustApprovedUpdates", justApprovedUpdates},
                                                            }
                                                          , new SolarWinds.PM.Web.TaskServices.Summary.UpdateServer().Load
                                                          , new object[] { deviceId }),
            EnableImmediateRendering = true,
            ClientObject = dataDiv,
        };

        wsusTask.ExecutionList.Add(showResourceExecution);

        wsusTask.RefreshExecutionList.Add(cleanTimeStampsExecutionDetails);
        wsusTask.RefreshExecutionList.Add(cleanTimeStampsExecutionNode);
        wsusTask.RefreshExecutionList.Add(cleanTimeStampsExecution);
        wsusTask.RefreshExecutionList.Add(showResourceExecution);

        if (this.MasterRefresher != null)
        {
            ((ASP.orion_pm_controls_taskbasecontrol_ascx)this.MasterRefresher).RefreshExecutionList.Insert(0, cleanTimeStampsExecutionDetails);
            ((ASP.orion_pm_controls_taskbasecontrol_ascx)this.MasterRefresher).RefreshExecutionList.Add(cleanTimeStampsExecutionNode);
            ((ASP.orion_pm_controls_taskbasecontrol_ascx)this.MasterRefresher).RefreshExecutionList.Add(cleanTimeStampsExecution);
        }
    }
}