﻿using System;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.PM.Web.Resources;


[ResourceMetadata(StandardMetadataPropertyName.Type, "Patch Manager")]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_PM_Controls_Summary_PatchingTasksControl : ScmBaseResource
{
    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        CustomTable.SWQL = SWQL;
        CustomTable.SearchSWQL = SearchSWQL;
        CustomTable.OrderBy = OrderBy;
    }

    protected override string DefaultTitle
    {
        get { return "Patching Tasks"; }
    }

    public string SWQL
    {
        get
        {
            return @"SELECT tasks.taskid as [_taskid], tasks.DisplayName as [Display Name], tasks.completiontime as [Completition Time],
(CASE tasks_results.operationresult
    WHEN 1 THEN 'Success'
    WHEN 2 THEN 'Failed'    
    ELSE 'Other'
END) AS [Task Result]
FROM Orion.PM.ewtasks as tasks
INNER JOIN (
    SELECT task_res.taskid as [taskid], MAX(task_res.operationresult) as [operationresult] 
    FROM Orion.PM.ewtasksresults as task_res 
    WHERE task_res.operationname = 'Install' OR task_res.operationname = 'Download'
    GROUP BY task_res.taskid) as tasks_results
ON tasks.taskid = tasks_results.taskid";
        }
    }

    public string SearchSWQL
    {
        get
        {
            return @"SELECT tasks.DisplayName FROM Orion.PM.ewtasks as tasks WHERE tasks.DisplayName LIKE '%${SEARCH_STRING}%'";
        }
    }

    public string OrderBy
    {
        get
        {
            return "tasks.DisplayName DESC";
        }
    }
}