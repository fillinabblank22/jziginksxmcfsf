﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.PM.Web.Resources;
using SolarWinds.Orion.Web.UI.Localizer;
using SolarWinds.PM.Web.AjaxTree;
using SolarWinds.PM.Web.Controls.AssetTree;
using SolarWinds.PM.Web.Controls;
using SolarWinds.PM.Web.Helper;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.PM.Web.NetObjects;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using SolarWinds.PM.Web.TaskServices.Summary;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_PM_Resources_Summary_WSUSNodesTreeByTargetGroups : ScmBaseResource
{
    private readonly string NoDataToDisplayCaption = "No data to display";

    protected override string DefaultTitle
    {
        get { return "Nodes Managed by WSUS Servers by Target Groups"; }
    }

    public override string HelpLinkFragment
    {
        get { return "SPMPH_OrionResource_Nodes_Managed_WSUS_Servers_Target_Groups"; }
    }
    
    public override string EditControlLocation
    {
        get { return "/Orion/PM/Controls/EditResourceControls/EditWSUSNodesTreeByTargetGroup.ascx"; }
    }

    protected override void OnInit(EventArgs e)
    {
        //Wrapper.ManageButtonImage = "~/Orion/PM/Images/Button.VMwareSettings.gif";
        //Wrapper.ManageButtonTarget = "~/Orion/PM/Admin/VMwareServers.aspx";
        Wrapper.ShowManageButton = (Profile.AllowAdmin && Profile.AllowNodeManagement) || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;

        base.OnInit(e);
    }

    protected override void OnPreRender(EventArgs e)
    {
        var manager = ScriptManager.GetCurrent(this.Page);

        if (manager != null)
        {
            manager.Scripts.Add(new ScriptReference(PathResolver.GetVirtualPath("PM", "TreeControl.js")));
        }

        base.OnPreRender(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        lblNoDataToDisplay.InnerHtml = ResourceHelper.GetInfoMessageHtml(NoDataToDisplayCaption);

        Guid wsusServer = Guid.Empty;
        IWsusServerProvider wsusNodeProvider = GetInterfaceInstance<IWsusServerProvider>();
        if (wsusNodeProvider != null && wsusNodeProvider.Node.WsusServerDetails != null)
            wsusServer = wsusNodeProvider.Node.WsusServerDetails.DeviceID;



        var cleanTimeStampsExecution = new TaskExecutionEntity()
        {
            HtmlServerMethod = new SolarWinds.PM.Web.TaskServices.Summary.NodeServerGroup().Clear,
            Parameters = new object[] { wsusServer },
            IsRedirectable = true,
        };
        var loadData = new TaskExecutionEntity()
        {
            HtmlServerMethod = new SolarWinds.PM.Web.TaskServices.Summary.NodeServerGroup().Load,
            Parameters = new object[] { wsusServer },
        };
        var javaScriptExecution = new TaskExecutionEntity()
        {
            ClientMethod = String.Format("Orion_PM_Resources_Summary_WSUSNodesTreeByTargetGroupsRefreshTree(\"{0}\");", tree.ClientID),
        };
        var javaScriptPostLoadExecution = new TaskExecutionEntity()
        {
            ClientMethod = String.Format("Orion_PM_Resources_Summary_WSUSNodesTreeByTargetGroupsCheckErrorBox(\"{0}\", \"{1}\");", tree.ClientID, lblNoDataToDisplay.ClientID),
        };

        if (this.MasterRefresher != null)
        {
            ((ASP.orion_pm_controls_taskbasecontrol_ascx)this.MasterRefresher).RefreshExecutionList.Add(cleanTimeStampsExecution);
        }



        var dataProviderIdentifier = "WSUSNodesTreeByTargetGroups";// data provider identifier ! resource id is used to uniquely identify provider in application scope
        var expandedStateProviderIdentifier = String.Format("PM_WSUS_NODES_TREE_BY_TARGET_GROUPS_STATE_-{0}-{1}-{2}",
            this.Resource.ID,
            this.Resource.View.ViewID,
            Request.QueryString["NetObject"] ?? String.Empty); //identifies this instance in session scope

        this.Session[String.Format("{0}-remember", expandedStateProviderIdentifier)] = this.Resource.Properties["RememberExpandedState"] ?? "true";

        if (!AppScopeTreeDataManager.ContainsProvider(dataProviderIdentifier))
        {
            AppScopeTreeDataManager.RegisterProvider(dataProviderIdentifier, new WSUSNodesTreeByTargetGroupsNodeProvider());
        }
        if (!SessionScopeTreeExpandedStateManager.ContainsProvider(expandedStateProviderIdentifier))
        {
            SessionScopeTreeExpandedStateManager.RegisterProvider(expandedStateProviderIdentifier);
        }

        var parameters = GetAsyncNode(dataProviderIdentifier, expandedStateProviderIdentifier, wsusServer);
        var task = new NodeServerGroup().HasDataForTree(wsusServer);
        if (task.Outcome == SolarWinds.PM.Common.Enums.TaskOutcome.Success)
        {
            lblNoDataToDisplay.Visible = false;
            parameters[11] = true;
        }
        else if (task.Outcome == SolarWinds.PM.Common.Enums.TaskOutcome.PartialSuccess)
        {
            lblNoDataToDisplay.InnerHtml = ResourceHelper.GetWarningMessageHtml(new ErrorInspectorDetails() { Title = "Resource displays just partial information", Error = new Exception(String.Concat("Some of WSUS servers are down.\n\n", task.ErrorMessage)) });
            parameters[11] = true;
        }
        else
        {
            wsusTask.ExecutionList.Add(loadData);
            wsusTask.ExecutionList.Add(javaScriptPostLoadExecution);
        }

        wsusTask.RefreshExecutionList.Add(cleanTimeStampsExecution);
        wsusTask.RefreshExecutionList.Add(loadData);
        wsusTask.RefreshExecutionList.Add(javaScriptExecution);
        wsusTask.RefreshExecutionList.Add(javaScriptPostLoadExecution);

        tree.LoadTree(new AssetTreeService(), parameters);
    }

    private object[] GetAsyncNode(string dataProviderIdentifier, string expandedStateProviderIdentifier, Guid wsusServer)
    {
        List<object> definition = new List<object>() 
        { 
            dataProviderIdentifier, 
            expandedStateProviderIdentifier,
            -1, // tree level
            wsusServer, // nodeId
            "root", // parent path
            String.Empty, // caption
            this.Resource.ID, // resource ID
            null, // group level 1
            null, //group level 2
            null, // group level 3
            "root",
            false
       };

        return definition.ToArray();
    }
}
