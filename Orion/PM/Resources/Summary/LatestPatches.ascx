﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LatestPatches.ascx.cs" Inherits="Orion_PM_Resources_Summary_LatestPatches" EnableViewState="false" %>
<%@ Register TagPrefix="task" TagName="TaskBaseControl" Src="~/Orion/PM/Controls/TaskBaseControl.ascx" %>

<script type="text/javascript" language="javascript">
    function Orion_PM_Controls_Summary_LatestPatchesToggle(className) {
        $("." + className).each(function (index) {
            var row = $(this);

            if (row.css("display") == "none") {
                row.fadeIn();
            } else {
                row.css("display", "none");
            }
        });

    }

    function Orion_PM_Controls_Summary_LatestPatchesChangeExpander(imageId) {
        var image = $("#" + imageId);
        var src = $(image).attr("src");

        if (src.indexOf("/Orion/images/Button.Expand.gif") == -1) {
            $(image).attr("src", "/Orion/images/Button.Expand.gif");
            $(image).attr("alt", "[+]");
        }
        else {
            $(image).attr("src", "/Orion/images/Button.Collapse.gif");
            $(image).attr("alt", "[-]");
        }
    }
</script>

<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <HeaderButtons>
        <task:TaskBaseControl runat="server" ID="wsusTask" />
    </HeaderButtons>
    <Content>
        <div id="dataDiv" runat="server" ></div>
    </Content>
</orion:ResourceWrapper>