<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ThwackRecentPosts.ascx.cs" Inherits="Orion_PM_Resources_Summary_ThwackRecentPosts" EnableViewState="false" %>
<%@ Register Src="~/Orion/Controls/HelpButton.ascx" TagPrefix="orion" TagName="HelpButton" %>

<asp:ScriptManagerProxy runat="server">
    <Services>
			<asp:ServiceReference path="../../Services/ThwackPM.asmx" />
    </Services>
</asp:ScriptManagerProxy>

<style type="text/css">
    #manageButtons .HelpButton {float: none !Important;}
    #manageButtons .EditResourceButton {float: none !Important;}
</style>

<orion:Include File="Thwack.css" runat="server" />
<orion:resourceWrapper runat="server" ID="resourceWrapper" ShowHeaderBar="false">
    <Content>
		<div style="border: 1px solid #d5d5d5;">
			<table cellpadding="0px" cellspacing="0px" border="0" class="ThwackHeader" style="border: 0;border-bottom: 0; background-image: url(/Orion/PM/images/Thwack/New_ThwackHeader_Background.png); background-repeat:repeat-x;">
				<tr>
					<td style="border: none; vertical-align: middle;">
                        <table cellpadding="0px" cellspacing="0px" style="border-width: 0px;">
                            <tr>
                                <td style="border: none; width: 1%;"><img class="ThwackHeaderImage" src="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath("PM", "Thwack/New_ThwackLogo.png")) %>" alt="<%= DefaultSanitizer.SanitizeHtml(this.ThwackLogoCaption) %>" /></td>
                                <td style="border: none;"><span style="padding-left:3px; color: White; font-size: 12px;"><%= DefaultSanitizer.SanitizeHtml(String.Format(this.SubTitleCaption, this.MaxRecords)) %></span></td>
                            </tr>
                        </table>
                    </td>
					<td id="manageButtons" style="border: none; padding: 10px 10px;" align="right" valign="top">
                        <span style="white-space: nowrap;">
                        <% if (Profile.AllowCustomize) { %>
						<asp:PlaceHolder ID="phEditButton" runat="server">
                            <orion:LocalizableButtonLink ID="btnEdit" runat="server" href="<%# DefaultSanitizer.SanitizeHtml(this.EditURL) %>" DisplayType="Resource" CssClass="EditResourceButton" LocalizedText="Edit" />
						</asp:PlaceHolder>&nbsp;
                        <% } %>
						<asp:PlaceHolder ID="phHelpButton" runat="server">
                            <orion:HelpButton runat="server" ID="HelpButton"/>
						</asp:PlaceHolder>
                        </span>
					</td>
				</tr>
			</table>
            
            <div runat="server" id="ThwackRecentPostsList" class="ThwackContent" style="padding-top: 10px;"/>

            <div style="padding-top: 10px;">
		        <table cellpadding="0" cellspacing="0" style="background-color: #eaeaea; height: 22px;">
                    <tr>              
                        <td style="border: none; text-align: right; padding-right: 10px;">
                            <a href="https://thwack.solarwinds.com/t5/Patch-Manager/ct-p/patch-manager" class="CTA_bottom_resource">&#0187;&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.ViewAllCaption) %></a>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="https://thwack.solarwinds.com/t5/user/userloginpage" class="CTA_bottom_resource">&#0187;&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.JoinThwackCaption) %></a>
                        </td>
                    </tr>
                </table>
            </div>
		</div>
        <script type="text/javascript">
//<![CDATA[
            Sys.Application.add_init(function(){
                ThwackPM.GetRecentPosts(<%= this.MaxRecords %>, 
                    function(result) {
                        var thwackRecentPostsList = $get('<%=this.ThwackRecentPostsList.ClientID %>');
                        thwackRecentPostsList.innerHTML = result;
                    },
                    function() {
                        var thwackRecentPostsList = $get('<%=this.ThwackRecentPostsList.ClientID %>');
                        thwackRecentPostsList.innerHTML = '<%= ControlHelper.EncodeJsString(this.ErrorMessage)%>';
                    }); });
//]]>
        </script>
     </Content>
</orion:resourceWrapper>