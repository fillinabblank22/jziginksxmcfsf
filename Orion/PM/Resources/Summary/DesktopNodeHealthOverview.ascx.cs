﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.PM.Web.Charting;
using SolarWinds.PM.Web.Resources;
using SolarWinds.PM.Web.NetObjects;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.PieCharts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsPieChartsValues.Charts)]
public partial class Orion_PM_Resources_Summary_DesktopNodeHealthOverview : ScmBaseResource
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Guid deviceId = Guid.Empty;
        IWsusServerProvider wsusNodeProvider = GetInterfaceInstance<IWsusServerProvider>();
        if (wsusNodeProvider != null && wsusNodeProvider.Node.WsusServerDetails != null)
            deviceId = wsusNodeProvider.Node.WsusServerDetails.DeviceID;

        bool justApprovedUpdates;
        if (!bool.TryParse(this.Resource.Properties["JustApprovedUpdates"] ?? "True", out justApprovedUpdates))
            justApprovedUpdates = true;
        
        var cleanTimeStampsExecution = new TaskExecutionEntity()
        {
            HtmlServerMethod = new SolarWinds.PM.Web.TaskServices.Summary.NodeServer().Clear,
            Parameters = new object[] { deviceId },
            IsRedirectable = true,
        };
        var showResourceExecution = new TaskExecutionEntity()
        {
            ServerResourceControl = new ServerResourceModel("/Orion/PM/Controls/Summary/NodesHealthOverviewCtrl.ascx"
                                                          , new Dictionary<string, object>()
                                                            {
                                                                { "DefaultTitle", deviceId == Guid.Empty ? this.DefaultTitle : String.Concat(wsusNodeProvider.Node.WsusServerDetails.Servername, " - ", this.DefaultTitle) },
                                                                { "Filter", "Desktop" },
                                                                { "NetObjectId", this.Request.QueryString["NetObject"]},
                                                                { "LegendHelpFragment", this.LegendHelpLinkFragment},
                                                                { "JustApprovedUpdates", justApprovedUpdates},
                                                            }
                                                          , new SolarWinds.PM.Web.TaskServices.Summary.NodeServer().Load
                                                          , new object[] { deviceId }),
            EnableImmediateRendering = true,
            ClientObject = dataDiv,
        };

        wsusTask.ExecutionList.Add(showResourceExecution);

        wsusTask.RefreshExecutionList.Add(cleanTimeStampsExecution);
        wsusTask.RefreshExecutionList.Add(showResourceExecution);

        if (this.MasterRefresher != null)
        {
            ((ASP.orion_pm_controls_taskbasecontrol_ascx)this.MasterRefresher).RefreshExecutionList.Add(cleanTimeStampsExecution);
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/PM/Controls/EditResourceControls/EditNodeHealthOverviewDesktop.ascx"; }
    }
    
    protected override string DefaultTitle
    {
        get { return "Desktop Node Health Overview"; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "SPMPH_OrionResource_Desktop_Node_Health_Overview";
        }
    }

    public string LegendHelpLinkFragment
    {
        get
        {
            return "SPMPH_OrionResource_Desktop_Node_Health_Overview_Legend";
        }
    }
}