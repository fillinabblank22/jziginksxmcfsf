﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.PM.Web.Resources;
using SolarWinds.Orion.Web.UI.Localizer;
using SolarWinds.PM.Web.AjaxTree;
using SolarWinds.PM.Web.Controls.AssetTree;
using SolarWinds.PM.Web.Controls;
using SolarWinds.PM.Web.Helper;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using SolarWinds.PM.Web.TaskServices.Server;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_PM_Resources_Summary_WSUSServersTree : ScmBaseResource
{
    private readonly string NoDataToDisplayCaption = "No data to display";

    protected override string DefaultTitle
    {
        get { return "WSUS Servers"; }
    }

    public override string HelpLinkFragment
    {
        get { return "SPMPH_OrionResource_WSUS_Servers"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/PM/Controls/EditResourceControls/EditWSUSServersTree.ascx"; }
    }

    protected override void OnPreRender(EventArgs e)
    {
        var manager = ScriptManager.GetCurrent(this.Page);

        if (manager != null)
        {
            manager.Scripts.Add(new ScriptReference(PathResolver.GetVirtualPath("PM", "TreeControl.js")));
        }

        base.OnPreRender(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        lblNoDataToDisplay.InnerHtml = ResourceHelper.GetInfoMessageHtml(NoDataToDisplayCaption);


        var cleanTimeStampsExecution = new TaskExecutionEntity()
        {
            HtmlServerMethod = new SolarWinds.PM.Web.TaskServices.Server.WsusServerDetails().Clear,
            Parameters = new object[] { Guid.Empty },
            IsRedirectable = true,
        };
        var loadData = new TaskExecutionEntity()
        {
            HtmlServerMethod = new SolarWinds.PM.Web.TaskServices.Server.WsusServerDetails().Load,
            Parameters = new Object[] { Guid.Empty },
        };
        var javaScriptExecution = new TaskExecutionEntity()
        {
            ClientMethod = String.Format("Orion_PM_Resources_Summary_WSUSServersTreeRefreshTree(\"{0}\");", tree.ClientID),
        };
        var javaScriptPostLoadExecution = new TaskExecutionEntity()
        {
            ClientMethod = String.Format("Orion_PM_Resources_Summary_WSUSServersTreeCheckErrorBox(\"{0}\", \"{1}\");", tree.ClientID, lblNoDataToDisplay.ClientID),
        };

        if (this.MasterRefresher != null)
        {
            ((ASP.orion_pm_controls_taskbasecontrol_ascx)this.MasterRefresher).RefreshExecutionList.Add(cleanTimeStampsExecution);
        }


        
        var dataProviderIdentifier = "WSUSServersTree";// data provider identifier ! resource id is used to uniquely identify provider in application scope
        var expandedStateProviderIdentifier = String.Format("SCM_WSUS_SERVERS_TREE_STATE_-{0}-{1}-{2}",
            this.Resource.ID,
            this.Resource.View.ViewID,
            Request.QueryString["NetObject"] ?? String.Empty); //identifies this instance in session scope

        this.Session[String.Format("{0}-remember", expandedStateProviderIdentifier)] = this.Resource.Properties["RememberExpandedState"] ?? "true";

        if (!AppScopeTreeDataManager.ContainsProvider(dataProviderIdentifier))
        {
            AppScopeTreeDataManager.RegisterProvider(dataProviderIdentifier, new WSUSServersTreeNodeProvider());
        }
        if (!SessionScopeTreeExpandedStateManager.ContainsProvider(expandedStateProviderIdentifier))
        {
            SessionScopeTreeExpandedStateManager.RegisterProvider(expandedStateProviderIdentifier);
        }

        var parameters = GetAsyncNode(dataProviderIdentifier, expandedStateProviderIdentifier);
        var task = new WsusServerDetails().HasDataForTree();
        if (task.Outcome == SolarWinds.PM.Common.Enums.TaskOutcome.Success)
        {
            lblNoDataToDisplay.Visible = false;
            parameters[11] = true;
        }
        else if (task.Outcome == SolarWinds.PM.Common.Enums.TaskOutcome.PartialSuccess)
        {
            lblNoDataToDisplay.InnerHtml = ResourceHelper.GetWarningMessageHtml(new ErrorInspectorDetails() { Title = "Resource displays just partial information", Error = new Exception(String.Concat("Some of WSUS servers are down.\n\n", task.ErrorMessage)) });
            parameters[11] = true;
        }
        else
        {
            wsusTask.ExecutionList.Add(loadData);
            wsusTask.ExecutionList.Add(javaScriptPostLoadExecution);
        }

        wsusTask.RefreshExecutionList.Add(cleanTimeStampsExecution);
        wsusTask.RefreshExecutionList.Add(loadData);
        wsusTask.RefreshExecutionList.Add(javaScriptExecution);
        wsusTask.RefreshExecutionList.Add(javaScriptPostLoadExecution);

        tree.LoadTree(new AssetTreeService(), parameters);
    }

    private object[] GetAsyncNode(string dataProviderIdentifier, string expandedStateProviderIdentifier)
    {
        // null means user did not entered "None" manually; that would be String.Empty
        if (this.Resource.Properties[ResourceHelper.GROUPING_RP_KEY_LEVEL1] == null)
        {
            // set default grouping
            this.Resource.Properties[ResourceHelper.GROUPING_RP_KEY_LEVEL1] = DEFAULT_GROUPING_LEVEL1;
            this.Resource.Properties[ResourceHelper.GROUPING_RP_KEY_LEVEL2] = DEFAULT_GROUPING_LEVEL2;
            this.Resource.SubTitle = ResourceHelper.GetGroupingSubtitle(DEFAULT_GROUPING_LEVEL1_CAPTION,
                                                                        DEFAULT_GROUPING_LEVEL2_CAPTION);
            // subtitle is not stored into DB when property is changed
            ResourcesDAL.Update(this.Resource);
        }
        
        List<object> definition = new List<object>() 
        { 
            dataProviderIdentifier, 
            expandedStateProviderIdentifier,
            -1, // tree level
            null, // nodeId
            "root", // parent path
            String.Empty, // caption
            this.Resource.ID, // resource ID
            this.Resource.Properties[ResourceHelper.GROUPING_RP_KEY_LEVEL1], // group level 1
            this.Resource.Properties[ResourceHelper.GROUPING_RP_KEY_LEVEL2], //group level 2
            String.Empty, // group level 3
            "root",
            false
        };

        return definition.ToArray();
    }

    private const string DEFAULT_GROUPING_LEVEL1 = "OSDescription";
    private const string DEFAULT_GROUPING_LEVEL2 = "";

    private const string DEFAULT_GROUPING_LEVEL1_CAPTION = "OS Description";
    private const string DEFAULT_GROUPING_LEVEL2_CAPTION = "";
}
