﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.PM.Web.Charting;
using SolarWinds.PM.Web.DAL;
using System.Data;
using SolarWinds.PM.Web.Resources;
using SolarWinds.PM.Web.NetObjects;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
public partial class Orion_PM_Resources_Summary_TopXXVulnerableMachines : TopXXScmBaseResource
{
    #region properties

    protected override string TitleTemplate
    {
        get
        {
            if (Resource.Title.Equals(DefaultTitle, StringComparison.OrdinalIgnoreCase)
                && string.IsNullOrEmpty(Resource.Properties["MaxEvents"]))
            {
                return String.Format("Top {0} Most Vulnerable Machines", this.MaxRecords);
            }
            return Resource.Title;
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/PM/Controls/EditResourceControls/EditTopXXVulnerableMachines.ascx"; }
    }

    // overriden dafault title
    protected override string DefaultTitle
    {
        get { return "Top XX Most Vulnerable Machines"; }
    }

    // overriden HelpLink
    public override string HelpLinkFragment
    {
        get
        {
            return "SPMPH_OrionResource_Top_Most_Vulnerable_Machines";
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        Guid deviceId = Guid.Empty;
        IWsusServerProvider wsusNodeProvider = GetInterfaceInstance<IWsusServerProvider>();
        if (wsusNodeProvider != null && wsusNodeProvider.Node.WsusServerDetails != null)
            deviceId = wsusNodeProvider.Node.WsusServerDetails.DeviceID;

        Int32 criticalThreshold;
        if (!Int32.TryParse(this.Resource.Properties["CriticalThreshold"] ?? "15", out criticalThreshold))
            criticalThreshold = 15;

        Int32 warningThreshold;
        if (!Int32.TryParse(this.Resource.Properties["WarningThreshold"] ?? "8", out warningThreshold))
            warningThreshold = 8;

        bool justApprovedUpdates;
        if (!bool.TryParse(this.Resource.Properties["JustApprovedUpdates"] ?? "True", out justApprovedUpdates))
            justApprovedUpdates = true;

        var cleanTimeStampsExecution = new TaskExecutionEntity()
        {
            HtmlServerMethod = new SolarWinds.PM.Web.TaskServices.Summary.NodeServer().Clear,
            Parameters = new object[] { deviceId },
        };
        var cleanTimeStampsExecutionDetails = new TaskExecutionEntity()
        {
            HtmlServerMethod = new SolarWinds.PM.Web.TaskServices.Summary.TopXXVulnerableMachines().Clear,
            Parameters = new object[] { deviceId, this.MaxRecords, justApprovedUpdates},
        };
        var showResourceExecution = new TaskExecutionEntity()
        {
            ServerResourceControl = new ServerResourceModel("/Orion/PM/Controls/Summary/TopXXVulnerableMachinesCtrl.ascx"
                                                          , new Dictionary<string, object>()
                                                            {
                                                                { "MaxRecords", this.MaxRecords},
                                                                { "WarningThreshold", warningThreshold },
                                                                { "CriticalThreshold", criticalThreshold },
                                                                { "NetObjectId", this.Request.QueryString["NetObject"]},
                                                                { "JustApprovedUpdates", justApprovedUpdates},
                                                            }
                                                          , new SolarWinds.PM.Web.TaskServices.Summary.NodeServer().Load
                                                          , new object[] { deviceId }),
            EnableImmediateRendering = true,
            ClientObject = dataDiv,
        };

        wsusTask.ExecutionList.Add(showResourceExecution);

        wsusTask.RefreshExecutionList.Add(cleanTimeStampsExecutionDetails);
        wsusTask.RefreshExecutionList.Add(cleanTimeStampsExecution);
        wsusTask.RefreshExecutionList.Add(showResourceExecution);

        if (this.MasterRefresher != null)
        {
            ((ASP.orion_pm_controls_taskbasecontrol_ascx)this.MasterRefresher).RefreshExecutionList.Insert(0, cleanTimeStampsExecutionDetails);
            ((ASP.orion_pm_controls_taskbasecontrol_ascx)this.MasterRefresher).RefreshExecutionList.Add(cleanTimeStampsExecution);
        }
    }
}
