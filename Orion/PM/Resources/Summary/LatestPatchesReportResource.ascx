﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LatestPatchesReportResource.ascx.cs" Inherits="Orion_PM_Resources_Summary_LatestPatchesReportResource" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:CustomQueryTable runat="server" ID="CustomTable"/>
 
        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= CustomTable.UniqueClientID %>,
                        initialPage: 0,
                        rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "5" %>,
                        allowSort: true
                    });
  
              
                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });    
        </script>
    </Content>
</orion:resourceWrapper>
