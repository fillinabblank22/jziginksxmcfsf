﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LastXXTasks.ascx.cs" Inherits="Orion_PM_Resources_Summary_LastXXTasks" EnableViewState="false" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="task" TagName="TaskBaseControl" Src="~/Orion/PM/Controls/TaskBaseControl.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <HeaderButtons>
        <task:TaskBaseControl runat="server" ID="wsusTask" />
    </HeaderButtons>
    <Content>
        <div id="dataDiv" runat="server" ></div>
    </Content>
</orion:resourceWrapper>