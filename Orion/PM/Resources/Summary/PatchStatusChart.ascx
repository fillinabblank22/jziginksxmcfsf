﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PatchStatusChart.ascx.cs" Inherits="Orion_PM_Resources_Charts_PatchStatusChart" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="task" TagName="TaskBaseControl" Src="~/Orion/PM/Controls/TaskBaseControl.ascx" %>

<orion:ResourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:Include runat="server" File="WSAsyncExecutor.js" Module="PM" />

        <script type="text/javascript" language="javascript">
            function Orion_PM_Controls_Summary_TopXXPatchesMissingCtrlToggle(identifier, data) {
                var row = $("#Sel_" + identifier);

                if (row.css("display") == "none") {
                    row.fadeIn();

                    data[0][1] = "content_" + identifier;
                    SW.Core.WSAsyncExecutor.LoadResource(identifier, { "Loader": "loader_" + identifier, "MessageContainer": null, "ContentContainer": "content_" + identifier }, data);
                } else {
                    row.css("display", "none");
                    $("#content_" + identifier).empty();
                }
            }

            function Orion_PM_Controls_Summary_TopXXPatchesMissingCtrlChangeExpander(imageId) {
                var image = $("#" + imageId);
                var src = $(image).attr("src");

                if (src.indexOf("/Orion/images/Button.Expand.gif") == -1) {
                    $(image).attr("src", "/Orion/images/Button.Expand.gif");
                    $(image).attr("alt", "[+]");
                }
                else {
                    $(image).attr("src", "/Orion/images/Button.Collapse.gif");
                    $(image).attr("alt", "[-]");
                }
            }
        </script>

        <orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
            <HeaderButtons>
                <task:TaskBaseControl runat="server" ID="wsusTask" />
            </HeaderButtons>
            <Content>
                <div id="dataDiv" runat="server"></div>
            </Content>
        </orion:ResourceWrapper>

    </Content>
</orion:ResourceWrapper>
