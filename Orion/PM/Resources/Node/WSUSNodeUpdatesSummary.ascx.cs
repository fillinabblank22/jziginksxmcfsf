﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.PM.Web.Charting;
using SolarWinds.PM.Web.DAL;
using System.Data;
using SolarWinds.PM.Web.Resources;
using SolarWinds.PM.Web.NetObjects;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.PieCharts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsPieChartsValues.Charts)]
public partial class Orion_PM_Resources_Node_WSUSNodeUpdatesSummary : ScmBaseResourceWsusNode
{
    private ASP.orion_resourcewrapper_ascx resourceWrapper;
    private ASP.orion_pm_controls_taskbasecontrol_ascx wsusTask;
    private System.Web.UI.HtmlControls.HtmlGenericControl dataDiv;

    #region properties

    // overriden DefaultTitle
    protected override string DefaultTitle
    {
        get { return "Node Updates - Summary"; }
    }

    // overriden HelpLink
    public override string HelpLinkFragment
    {
        get
        {
            return "SPMPH_OrionResource_Node_Updates_Summary";
        }
    }
    
    public string LegendHelpLinkFragment
    {
        get
        {
            return "SPMPH_OrionResource_Node_Updates_Summary_Legend";
        }
    }
    #endregion

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        resourceWrapper = new ASP.orion_resourcewrapper_ascx();
        this.Controls.Add(resourceWrapper);

        dataDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
        resourceWrapper.Content.Controls.Add(dataDiv);

        wsusTask = new ASP.orion_pm_controls_taskbasecontrol_ascx();
        resourceWrapper.HeaderButtons.Controls.Add(wsusTask);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string justApprovedUpdatesString = this.Resource.Properties["JustApprovedUpdates"] ?? "true";
        Boolean justApprovedUpdates = Boolean.Parse(justApprovedUpdatesString);



        var cleanTimeStampsExecution = new TaskExecutionEntity()
        {
            HtmlServerMethod = new SolarWinds.PM.Web.TaskServices.Node.WsusUpdatesBase().Clear,
            Parameters = new object[] { this.Node.NetObjectID, this.Node.NodeDetails.FullDomainName },
            IsRedirectable = true,
        };
        var showResourceExecution = new TaskExecutionEntity()
        {
            ServerResourceControl = new ServerResourceModel("/Orion/PM/Controls/Node/WSUSNodeUpdatesSummaryCtrl.ascx"
                                                          , new Dictionary<string, object>()
                                                            {
                                                                { "JustApprovedUpdates", justApprovedUpdates },
                                                                { "NetObjectId", this.Node.NetObjectID },
                                                                { "DefaultTitle", String.Concat(this.Node.Name, " - ", this.DefaultTitle) },
                                                                { "LegendHelpFragment", this.LegendHelpLinkFragment},
                                                            }
                                                          , new SolarWinds.PM.Web.TaskServices.Node.WsusUpdatesBase().Load
                                                          , new object[] { this.Node.NetObjectID }),
            EnableImmediateRendering = true,
            ClientObject = dataDiv,
        };

        wsusTask.ExecutionList.Add(base.InitNodeData);
        wsusTask.ExecutionList.Add(showResourceExecution);

        wsusTask.RefreshExecutionList.Add(cleanTimeStampsExecution);
        wsusTask.RefreshExecutionList.Add(showResourceExecution);

        if (this.MasterRefresher != null)
        {
            ((ASP.orion_pm_controls_taskbasecontrol_ascx)this.MasterRefresher).RefreshExecutionList.Insert(0, cleanTimeStampsExecution);
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/PM/Controls/EditResourceControls/EditWSUSNodeUpdatesOverview.ascx"; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IWsusNodeProvider) }; }
    }
}
