﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using System.Data;
using SolarWinds.PM.Web.DAL;
using System.Data.SqlClient;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.PM.Web.Resources;
using SolarWinds.PM.Web.NetObjects;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
public partial class Orion_PM_Resources_Node_WSUSNodeUpdatesMissingList : TopXXScmBaseResourceWsusNode
{
    private ASP.orion_resourcewrapper_ascx resourceWrapper;
    private ASP.orion_pm_controls_taskbasecontrol_ascx wsusTask;
    private System.Web.UI.HtmlControls.HtmlGenericControl dataDiv;

    protected override string TitleTemplate
    {
        get
        {
            if (Resource.Title.Equals(DefaultTitle, StringComparison.OrdinalIgnoreCase))
            {
                return String.Format("Top {0} Missing Updates", this.MaxRecords);
            }
            return Resource.Title;
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/PM/Controls/EditResourceControls/EditWSUSNodeUpdatesOverview.ascx"; }
    }

    protected override string DefaultTitle
    {
        get { return "Top XX Missing Updates"; }
    }

    public override string HelpLinkFragment
    {
        get { return "SPMPH_OrionResource_Top_XX_Missing_Updates"; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IWsusNodeProvider) }; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        resourceWrapper = new ASP.orion_resourcewrapper_ascx();
        this.Controls.Add(resourceWrapper);

        dataDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
        resourceWrapper.Content.Controls.Add(dataDiv);

        wsusTask = new ASP.orion_pm_controls_taskbasecontrol_ascx();
        resourceWrapper.HeaderButtons.Controls.Add(wsusTask);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        resourceWrapper.ShowEditButton = this.EnableEdit;

        string justApprovedUpdatesString = this.Resource.Properties["JustApprovedUpdates"] ?? "true";
        Boolean justApprovedUpdates = Boolean.Parse(justApprovedUpdatesString);


        var cleanTimeStampsExecution = new TaskExecutionEntity()
        {
            HtmlServerMethod = new SolarWinds.PM.Web.TaskServices.Node.WsusUpdatesBase().Clear,
            Parameters = new object[] { this.Node.NetObjectID, this.Node.NodeDetails.FullDomainName },
            IsRedirectable = true,
        };
        var showResourceExecution = new TaskExecutionEntity()
        {
            ServerResourceControl = new ServerResourceModel("/Orion/PM/Controls/Node/WSUSNodeUpdatesMissingListCtrl.ascx"
                                                          , new Dictionary<string, object>()
                                                            {
                                                                { "JustApprovedUpdates", justApprovedUpdates },
                                                                { "NetObjectId", this.Node.NetObjectID },
                                                                { "MaxRecords", this.MaxRecords },
                                                            }
                                                          , new SolarWinds.PM.Web.TaskServices.Node.WsusUpdatesBase().Load
                                                          , new object[] { this.Node.NetObjectID }),
            EnableImmediateRendering = true,
            ClientObject = dataDiv,
        };

        wsusTask.ExecutionList.Add(base.InitNodeData);
        wsusTask.ExecutionList.Add(showResourceExecution);

        wsusTask.RefreshExecutionList.Add(cleanTimeStampsExecution);
        wsusTask.RefreshExecutionList.Add(showResourceExecution);

        if (this.MasterRefresher != null)
        {
            ((ASP.orion_pm_controls_taskbasecontrol_ascx)this.MasterRefresher).RefreshExecutionList.Insert(0, cleanTimeStampsExecution);
        }
    }
}
