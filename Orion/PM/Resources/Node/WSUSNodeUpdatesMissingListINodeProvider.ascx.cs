﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.NPM.Web;

public partial class Orion_PM_Resources_Node_WSUSNodeUpdatesMissingListINodeProvider : Orion_PM_Resources_Node_WSUSNodeUpdatesMissingList
{
    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }
}
