﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.PM.Web.NetObjects;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;

public partial class Orion_SCM_UpdateDetails : OrionView
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    protected override void OnInit(EventArgs e)
    {
        this.Title = this.ViewInfo.ViewTitle;
        this.updateResHost.Update = (Update)this.NetObject;
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        lnkUpdate.InnerText = this.NetObject.Name;
        lnkUpdate.HRef = ((Update)this.NetObject).Link;

        TitleDecorators.Controls.Clear();
        foreach (var decoratorControl in OrionModuleManager.GetNodeDetailsTitleDecorators())
        {
            Control c = LoadControl(decoratorControl);
            var provider = c as IPropertyProvider;

            if (provider != null)
                provider.Properties["UpdateId"] = NetObject.NetObjectID;

            TitleDecorators.Controls.Add(c);
        }
        base.OnInit(e);
    }

    public override string ViewType
    {
        get { return "UpdateDetails"; }
    }
}
