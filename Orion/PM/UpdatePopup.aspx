﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UpdatePopup.aspx.cs" Inherits="Orion_SCM_UpdatePopup" %>

<%--This is intended to be displayed as the body of a cluetip tooltip/popup.--%>
<h3 class="StatusUndefined"><asp:literal runat="server" id="lblTitle" /></h3>
<div class="NetObjectTipBody">
    <p class="StatusDescription"><asp:literal runat="server" id="lblStatus" /></p>
</div>
