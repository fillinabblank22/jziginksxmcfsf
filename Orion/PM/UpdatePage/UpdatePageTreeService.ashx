﻿<%@ WebHandler Language="C#" Class="UpdatePageTreeService" %>

using System;
using System.Web;

public class UpdatePageTreeService : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    [Serializable]
    private class TreeNode
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public Boolean IsLeaf { get; set; }
        public Boolean Expanded { get; set; }
        public Boolean Selected { get; set; }
        public string Icon { get; set; }

        public override string ToString()
        {
            System.Text.StringBuilder str = new System.Text.StringBuilder();
            str.Append("{");
            str.AppendFormat("\"id\": \"{0}\",", this.Id.Replace("\"", "\\\""));
            str.AppendFormat("\"text\": \"{0}\",", this.Text.Replace("\"", "\\\""));
            str.AppendFormat("\"leaf\": {0},", this.IsLeaf.ToString().ToLower());
            str.AppendFormat("\"icon\": \"{0}\",", this.Icon.Replace("\"", "\\\""));
            str.AppendFormat("\"expanded\": {0},", this.Expanded.ToString().ToLower());
            str.AppendFormat("\"selected\": {0}", this.Selected.ToString().ToLower());
            str.Append("}");
            return str.ToString();
        }

        public static string JsonWritter(params TreeNode[] nodes)
        {
            var sb = new System.Text.StringBuilder("{ \"d\": [");
            for (int i = 0; i < nodes.Length; i++)
            {
                if (i != 0)
                    sb.Append(",");
                sb.Append(nodes[i].ToString());
            }
            sb.Append("]}");

            return sb.ToString();
        }
    }

    public void ProcessRequest(HttpContext context)
    {
        try
        {
            context.Response.ContentType = "text/plain";
            
            String id = context.Request.Params["id"];
            String viewDetailId = context.Request.Params["viewDetailId"];

            if (String.IsNullOrEmpty(context.Session[String.Concat(SolarWinds.PM.Web.Helper.ResourceHelper.UPDATE_PAGE_PREFIX, viewDetailId, "NodeParameters")] as String))
            {
                context.Response.Write(SolarWinds.PM.Web.Helper.ResourceHelper.GetErrorMessageHtml("Session is expired or unavailable! Leave the page and came back by clicking to the particular link."));
                return;
            }

            var clickedNodeId = SolarWinds.PM.Web.Helper.Serializer.Deserialize<Object[]>(context.Session[String.Concat(SolarWinds.PM.Web.Helper.ResourceHelper.UPDATE_PAGE_PREFIX, viewDetailId, "NodeParameters")] as String);
            var clickedNode = new SolarWinds.PM.Web.Controls.AssetTree.AssetTreeNode(clickedNodeId);

            object[] definition;
            if (id == "root")
            {
                var rootNode = new SolarWinds.PM.Web.Controls.AssetTree.AssetTreeNode(clickedNode.DataProviderIdentifier
                                                                                    , clickedNode.ExpandedStateProviderIdentifier
                                                                                    , -1
                                                                                    , clickedNode.GetListOfParentsIDs()[0].ToString()
                                                                                    , "root"
                                                                                    , "root"
                                                                                    , clickedNode.ResourceID
                                                                                    , clickedNode.GroupLevel);


                var isNoGrouping = String.IsNullOrEmpty(rootNode.GroupLevel[0]);
                if (isNoGrouping)
                {
                    // need to create a root item to the tree because the grouping is not set and the next level shows just nodes that I do not want to have in tree
                    var rootNodeData = new SolarWinds.PM.Web.Controls.AllPatchesTreeNodeProvider().GetChildsTreeNodeList(rootNode, rootNode.TreeLevel);
                    var item = rootNodeData[0] as SolarWinds.PM.Web.Controls.AssetTree.AssetTreeNode;
                    context.Response.Write(TreeNode.JsonWritter(new TreeNode()
                    {
                        Id = SolarWinds.PM.Web.Helper.Serializer.Serialize(item.Parameters),
                        Text = item.Caption,
                        IsLeaf = true,
                        Icon = item.IconUrl,
                        Expanded = false,
                        Selected = true,
                    }));

                    return;
                }

                definition = new SolarWinds.PM.Web.Controls.AllPatchesTreeNodeProvider().GetChildsTreeNodeList(rootNode, rootNode.TreeLevel)[0].Parameters;
            }
            else
            {
                definition = SolarWinds.PM.Web.Helper.Serializer.Deserialize<Object[]>(id);
            }

            var treeNodes = new System.Collections.Generic.List<TreeNode>();
            var node = new SolarWinds.PM.Web.Controls.AssetTree.AssetTreeNode(definition);
            var nodes = new SolarWinds.PM.Web.Controls.AllPatchesTreeNodeProvider().GetChildsTreeNodeList(node, node.TreeLevel);
            foreach (SolarWinds.PM.Web.Controls.AssetTree.AssetTreeNode item in nodes)
            {
                var expanded = clickedNode.ParentPath.Contains(item.ParentPath) && clickedNode.Path.Contains(item.Caption);
                var isLeaf = item.TreeLevel >= item.GroupLevel.Length || String.IsNullOrEmpty(item.GroupLevel[item.TreeLevel]);
                
                treeNodes.Add(new TreeNode() 
                    {
                        Id = SolarWinds.PM.Web.Helper.Serializer.Serialize(item.Parameters), 
                        Text = item.Caption,
                        IsLeaf = isLeaf,
                        Icon = item.IconUrl,
                        Expanded = expanded,
                        Selected = isLeaf && expanded,
                    });
            }

            context.Response.Write(TreeNode.JsonWritter(treeNodes.ToArray()));
        }
        catch (Exception ex)
        {
            context.Response.Write(SolarWinds.PM.Web.Helper.ResourceHelper.GetErrorMessageHtml(ex));
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}