using System;
using System.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Common;
using System.IO;
using SolarWinds.Logging;

public partial class Orion_PM_UpdatePage_UpdatePage : System.Web.UI.Page
{
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        Title = "Search Results";
    }
}
