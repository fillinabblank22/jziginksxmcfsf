﻿Ext.namespace('SW');
Ext.namespace('SW.Core');

SW.Core.UpdatePage = function () {
    var dataStore;
    var grid;
    var pageSizeNum;

    function encodeHTML(htmlText) {
        return $('<div/>').text(htmlText).html();
    };

    function formatSearchValue(value) {
        var myValue = encodeHTML(value);

        var search = GetSearchValue();
        if (search != null) {
            var index = myValue.toLowerCase().indexOf(search.toLowerCase());
            if (index != -1)
                myValue = myValue.substr(0, index) + "<span style='background-color: yellow;'>" + myValue.substr(index, search.length) + "</span>" + myValue.substr(index + search.length);
        }

        return myValue;
    }

    // Column rendering functions
    function renderUpdate(value, meta, record) {
        var item = String.format("<span style='font-size: 12px;'><img alt='icon' src='{0}'/>&nbsp;<a href='{1}'>{2}</a></span>"
                                , encodeHTML(record.data.Image)
                                , encodeHTML(record.data.Link)
                                , formatSearchValue(value));

        return item;
    };

    // Column rendering functions
    function renderServer(value, meta, record) {
        var item = String.format("<span style='font-size: 12px;'><img alt='icon' src='{0}'/>&nbsp;<a href='{1}'>{2}</a></span>"
                                , encodeHTML(record.data.ImageServer)
                                , encodeHTML(record.data.LinkServer)
                                , formatSearchValue(value));

        return item;
    };

    // Column rendering functions
    function renderSearchable(value, meta, record) {
        return String.format("<span style='font-size: 12px;'>{0}</span>", formatSearchValue(value));
    };

    function GetSearchValue() {
        var map = grid.getTopToolbar().items.map;

        var search = map.txtSearch.getValue();
        if (search.length == 0 || search == map.txtSearch.initialConfig.value)
            return null;

        return search;
    }

    function Search() {
        var search = GetSearchValue();
        if (CheckValidityInput(search) == false)
            return;

        ORION.callWebService("/Orion/PM/Services/UpdatePageService.asmx", "Search", { search: search, viewDetailId: $("#txtViewDetailId").val() }, function (result) {
            grid.getStore().load({ params: { start: 0, limit: grid.bottomToolbar.pageSize} });
        });
    }

    function UpdateClick(node) {
        ORION.callWebService("/Orion/PM/Services/UpdatePageService.asmx", "SetClickedNode", { nodeId: node.id, viewDetailId: $("#txtViewDetailId").val() }, function (result) {
            grid.getStore().load({ params: { start: 0, limit: grid.bottomToolbar.pageSize} }); 
        });
    }

    function GetNode(data) {
        return new Ext.tree.AsyncTreeNode({
            text: data.text,
            draggable: false,
            id: data.id,
            leaf: data.leaf,
            icon: data.icon,
            cls: 'nodeIcon',
            expanded: data.expanded && !data.leaf,
            listeners: {
                click: UpdateClick
            },
            loader: new Ext.tree.TreeLoader({
                dataUrl: 'UpdatePageTreeService.ashx',
                listeners: {
                    // init load event handler
                    beforeload: {
                        fn: function (treeLoader, node) {
                            this.baseParams.id = node.id;
                            this.baseParams.viewDetailId = $("#txtViewDetailId").val();
                            SetNodeIcon(node, '/Orion/images/AJAX-Loader.gif', false);
                        }
                    },
                    // after load event handler
                    load: {
                        fn: function (treeLoader, node, response) {
                            LoadNode(node, response);
                            node.expanded = true;
                            SetNodeIcon(node, node.attributes.icon);
                        }
                    }
                }
            })
        });
    }

    function LoadNode(root, response) {
        try {
            var data = JSON.parse(response.responseText);

            $.each(data.d, function (key, value) {
                var newNode = GetNode(value);
                root.appendChild(newNode);

                if (value.selected)
                    newNode.select();
            });
        } catch (e) {
            $("#errorMessage").empty();
            $("#errorMessage").append("<br/>" + response.responseText);
            $("#content").hide();
        }
    }

    // Changes icon on gived TreeNode in runtime
    SetNodeIcon = function (node, icon) {
        if ((node.ui) && (node.ui.iconNode)) {
            node.ui.iconNode.src = icon;
        }
    }

    return {
        reload: function () { grid.getStore().reload(); },
        init: function () {
            var tree = new Ext.tree.TreePanel({
                renderTo: 'tree-div',
                title: '&nbsp;',
                height: 650,
                width: 250,
                useArrows: true,
                autoScroll: true,
                animate: true,
                enableDD: true,
                containerScroll: true,
                rootVisible: false,
                root: GetNode({ "text": "", "id": "root", "leaf": false })
            });

            // load default page size from Orion settings
            pageSizeNum = parseInt(ORION.Prefs.load('PageSize', '20'));

            Ext.override(Ext.PagingToolbar, {
                addPageSizer: function () {
                    // add a combobox to the toolbar
                    var store = new Ext.data.SimpleStore({
                        fields: ['pageSize'],
                        data: [[10], [20], [30], [40], [50]]
                    });
                    var combo = new Ext.form.ComboBox({
                        regex: /^\d*$/,
                        store: store,
                        displayField: 'pageSize',
                        mode: 'local',
                        triggerAction: 'all',
                        selectOnFocus: true,
                        width: 50,
                        editable: false,
                        value: pageSizeNum
                    });
                    this.addField(new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_VB0_46; E=js}', style: 'margin-left: 5px; margin-right: 5px' }));
                    this.addField(combo);

                    combo.on("select", function (c, record) {
                        this.pageSize = record.get("pageSize");
                        this.cursor = 0;
                        ORION.Prefs.save('PageSize', this.pageSize);
                        this.doRefresh();
                    }, this);
                }
            });

            // define data store for credential grid - use web service (method) for load of data
            dataStore = new ORION.WebServiceStore(
                "/Orion/PM/Services/UpdatePageService.asmx/GetUpdates",
                [
                { name: 'Link', mapping: 0 },
                { name: 'Image', mapping: 1 },
                { name: 'Update', mapping: 2 },
                { name: 'RevisionNumber', mapping: 3 },
                { name: 'KnowledgebaseArticles', mapping: 4 },
                { name: 'Servername', mapping: 5 },
                { name: 'LinkServer', mapping: 6 },
                { name: 'ImageServer', mapping: 7 }
                ],
                "Update");

            // define grid panel
            grid = new Ext.grid.GridPanel({

                store: dataStore,

                columns: [
                    { header: 'Update', width: 480, sortable: true, dataIndex: 'Update', renderer: renderUpdate },
                    { header: 'Rev. Number', width: 100, sortable: true, dataIndex: 'RevisionNumber', renderer: renderSearchable },
                    { header: 'KB Article', width: 90, sortable: true, dataIndex: 'KnowledgebaseArticles', renderer: renderSearchable },
                    { header: 'Wsus Server', width: 200, sortable: true, dataIndex: 'Servername', renderer: renderServer }
                ],

                sm: new Ext.grid.RowSelectionModel({ singleSelect: true }),

                viewConfig: {
                    forceFit: false
                },

                //width has to be smaller here than div width on page
                //grid will be resized when it's rendered
                width: 950,
                height: 650,
                stripeRows: true,

                tbar: [
                {
                    xtype: 'tbfill'
                },
                {
                    id: 'txtSearch',
                    xtype: 'textfield',
                    value: '',
                    listeners: {
                        specialkey: function (frm, evt) {
                            if (evt.getKey() == evt.ENTER) {
                                Search();
                            }
                        }
                    }
                },
                {
                    id: 'btnSearch',
                    xtype: 'button',
                    text: 'Search',
                    listeners: {
                        click: function () { Search(); }
                    }
                }],

                bbar: new Ext.PagingToolbar({
                    store: dataStore,
                    pageSize: pageSizeNum,
                    displayInfo: true,
                    displayMsg: 'Displaying items {0} - {1} of {2}',
                    emptyMsg: "No item found",
                    beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                    afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                    firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                    prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                    nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                    lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                    refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}"
                })

            });

            grid.getView().on('refresh', function () {
                var obj = $('.x-grid3-hd-checker');
                if (obj && obj.hasClass('x-grid3-hd-checker-on'))
                    obj.removeClass('x-grid3-hd-checker-on');
            });

            grid.render('Grid');
            grid.bottomToolbar.addPageSizer();
            grid.setWidth($('#gridPanel').width());
            grid.store.proxy.conn.jsonData = { viewDetailId: $("#txtViewDetailId").val() };
            grid.getStore().load({ params: { start: 0, limit: grid.bottomToolbar.pageSize} });

            var map = grid.getTopToolbar().items.map;
            map.txtSearch.setValue($("#txtSearchSessionValue").val());
        }
    };

} ();


Ext.onReady(SW.Core.UpdatePage.init, SW.Core.UpdatePage);