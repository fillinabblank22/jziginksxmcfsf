<%@ Page Language="C#" MasterPageFile="~/Orion/PM/PM.master" AutoEventWireup="true" CodeFile="UpdatePage.aspx.cs" 
    Inherits="Orion_PM_UpdatePage_UpdatePage" %>

<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>

<asp:Content ContentPlaceHolderID="TopRightPageLinks" runat="server" ID="TopRightPageLinksID" Visible="false">
    <orion:Include ID="Include3" Module="PM" File="SCM.css" runat="server" />

    <% if (string.IsNullOrEmpty(Request.QueryString["IsMobileView"]) || !Request.QueryString["IsMobileView"].Equals("true", StringComparison.OrdinalIgnoreCase))
    {%>
        <div>
            <orion:IconLinkExportToPDF runat="server" ID="ExportToPDFLink" />
            <%if (Profile.AllowAdmin && !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
                <a class="settingsLink" href="/Orion/PM/Admin/Default.aspx">Patch Manager Settings</a>
            <%}%>
        </div>
    <%} %>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPlaceHolder" runat="server">
    <orion:Include ID="IncludeExtJs1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include ID="Include1" runat="server" File="OrionCore.js" />
    <orion:Include ID="Include4" runat="server" File="js/OrionMinReqs.js/json2.js" />
    <orion:Include ID="Include5" runat="server" Module="PM" File="JSHelper.js" />
    <orion:Include ID="Include2" runat="server" Module="PM" File="/UpdatePage/UpdatePage.js" />
    <style type="text/css">
        span.Label { white-space:nowrap; margin-left:5px;}
        .smallText {color:#979797;font-size:9px;}
        #Grid td { padding-right: 0px; padding-bottom: 0px; vertical-align: middle; }
        #Grid { padding-top: 12px; }
        #CredManagerDialogFrame {height:auto !important;}
        #CredManagerDialogBody {height:auto !important;}
        .x-btn td {text-align: center !important;}
        .nodeIcon img {width:auto!important;}
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="titleTable">
        <h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    </div>

    <input type="hidden" id="txtViewDetailId" value="<%= DefaultSanitizer.SanitizeHtml(Request.QueryString["ViewDetailId"]) %>" />
    <input type="hidden" id="txtSearchSessionValue" value="<%= DefaultSanitizer.SanitizeHtml(Session[String.Concat(SolarWinds.PM.Web.Helper.ResourceHelper.UPDATE_PAGE_PREFIX, Request.QueryString["ViewDetailId"], "SearchKey")]) %>" />

    <div id="errorMessage"></div>
    <div style="padding-left:12px;">
        <table id="content">
            <tr>
                <td style="vertical-align: top;"><div style="padding-top: 12px;" id="tree-div" /></td>
                <td style="vertical-align: top;">
                    <div id="gridPanel" style="max-width:870px;">	
                        <div id="Grid" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>


