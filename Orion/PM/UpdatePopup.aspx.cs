﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.PM.Web.DAL;
using SolarWinds.PM.Web.Model;
using SolarWinds.Orion.Web;
using SolarWinds.PM.Web.NetObjects;

public partial class Orion_SCM_UpdatePopup : System.Web.UI.Page
{
    private readonly string TitleCaption = "Update";
    private readonly string NoDescriptionAvailableCaption = "No description for update available";

    protected void Page_Load(object sender, EventArgs e)
    {
        String description = String.Empty;

        var update = NetObjectFactory.Create(Request["NetObject"], true) as Update;

        if (update != null)
        {
            description = update.UpdateDetails.Description;
        }

        if (String.IsNullOrEmpty(description))
        {
            description = NoDescriptionAvailableCaption;
        }

        lblStatus.Text = description;
        lblTitle.Text = TitleCaption;

        this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
    }
}
