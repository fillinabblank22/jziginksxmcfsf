using System;

using SolarWinds.Orion.Web.UI;

public partial class Orion_PM_Summary : OrionView
{
    protected override void OnInit(EventArgs e)
    {
        Title = ViewInfo.ViewTitle;
        resContainer.DataSource = ViewInfo;
        resContainer.DataBind();

        base.OnInit(e);
    }

    public override string ViewType
    {
        get { return "PatchManagerSummary"; }
    }
}
