<%@ Page Language="C#" MasterPageFile="~/Orion/PM/PMView.master" AutoEventWireup="true" CodeFile="WsusServerDetails.aspx.cs" Inherits="Orion_SCM_WsusServerDetails" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="pm" Assembly="SolarWinds.PM.Web" Namespace="SolarWinds.PM.Web.NetObjects" %>


<asp:Content runat="server" ContentPlaceHolderID="ViewPageTitle" ID="Content1">
	<h1>
	    <%= DefaultSanitizer.SanitizeHtml(ViewInfo.ViewTitle) %> -&nbsp;
        <img alt="icon" src="/Orion/PM/images/WsusServer_16x16.png" style="vertical-align: bottom; padding-bottom:3px;" />&nbsp;<a runat="server" id="lnkNode" class="NetObjectLink"></a>
	    <asp:PlaceHolder ID="TitleDecorators" runat="server"></asp:PlaceHolder>	    
	</h1>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
	<pm:WsusServerResourceHost runat="server" ID="wsusServerResHost">
	    <orion:ResourceContainer runat="server" ID="resContainer" />
	</pm:WsusServerResourceHost>
</asp:Content>
