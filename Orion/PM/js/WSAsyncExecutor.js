﻿$.getScript('/Orion/js/OrionMinReqs.js/json2.js', function () { });

(function (Core) {
var WSAsyncExecutor = Core.WSAsyncExecutor = Core.WSAsyncExecutor || {};
    var resources = new Array();

    var CHECKING_INTERVAL = 5000;
    var forceStart = false;
    var runningQuery = false;

    var clientMethodIndex = 0;
    var clientObjectIndex = 1;
    var serverMethodIndex = 2;
    var parametersIndex = 3;
    var serverControlDefinitionIndex = 4;
    var taskIdIndex = 5;

    var checkingTaskDelegate = null;
    var syncRoot = false;

    var timeOutTemplate = null;
    var errorTemplate = null;

    WSAsyncExecutor.Init = function (timeOut, error) {
        timeOutTemplate = timeOut;
        errorTemplate = error;
    }

    WSAsyncExecutor.LoadResource = function (resourceId, taskObjects, ControlLifeCycle) {
        if (syncRoot)
            setTimeout(function () { SW.Core.WSAsyncExecutor.LoadResource(resourceId, taskObjects, ControlLifeCycle); }, 10);

        syncRoot = true;

        var tasks = new Array();

        $.each(ControlLifeCycle, function (key, value) {
            var hash = value[taskIdIndex];
            tasks[hash] = { "Task": value, "Hash": hash, "Running": false };
        });

        if (SW.Core.WSAsyncExecutor.ArrayLength(tasks) != 0)
            resources[resourceId] = { "TaskObjects": taskObjects, "Timer": null, "ResourceId": resourceId, "Results": new Array(), "Tasks": tasks, "HasStarted": false };

        forceStart = true;

        if (runningQuery == false) {
            if (checkingTaskDelegate != null)
                clearTimeout(checkingTaskDelegate);

            checkingTaskDelegate = setTimeout(function () { SW.Core.WSAsyncExecutor.CheckTasks(); }, 1);
        }

        syncRoot = false;
    }

    WSAsyncExecutor.CheckTasks = function () {
        if (syncRoot)
            setTimeout(function () { SW.Core.WSAsyncExecutor.CheckTasks(); }, 10);

        if (SW.Core.WSAsyncExecutor.ArrayLength(resources) == 0) {
            checkingTaskDelegate = null;

            return;
        }

        syncRoot = true;

        var tasks = new Array();

        for (var resourceId in resources) {
            if(!resources.hasOwnProperty(resourceId))
                continue;

            var resource = resources[resourceId];

            var i = 0;
            var waitBeforeExecuteJS = false;
            var tasksCountForResource = 0;
            for (var hash in resource.Tasks) {
                if(!resource.Tasks.hasOwnProperty(hash))
                    continue;
                
                if (resource.Tasks[hash].Running == false) {
                    //we can have mixed list with server methods and JS methods and again server method.
                    //to reach the correct order, we need to execute all server method and then JS method.
                    //so when the server methods are pending, and JS method is the next one to execute, we have to wait to finish all server pending methods
                    //then we can execute the JS methods and continue with the other server ones
                    if (resource.Tasks[hash].Task[serverMethodIndex] == null && resource.Tasks[hash].Task[serverControlDefinitionIndex] == null) {
                        //JS Method
                        if (i != 0 || waitBeforeExecuteJS)
                            break;
                    }
                    else
                        i++;

                    tasks.push(
                    {
                        "ResourceId": resource.ResourceId,
                        "Hash": hash,
                        "ServerMethod": resource.Tasks[hash].Task[serverMethodIndex],
                        "ServerControlDefinition": resource.Tasks[hash].Task[serverControlDefinitionIndex],
                        "Parameters": resource.Tasks[hash].Task[parametersIndex]
                    });
                    tasksCountForResource++;

                    resource.Tasks[hash].Running = true;
                }

                if (resource.Tasks[hash].Task[serverMethodIndex] != null || resource.Tasks[hash].Task[serverControlDefinitionIndex] != null)
                    waitBeforeExecuteJS = true;
            }

            if (tasksCountForResource > 0 && resource.HasStarted == false) {
                resource.HasStarted = true;

                resource.Results["NoMessage"] = resource.Results["Message"] = resource.Results["JustMessage"] = 0;

                resource.Timer = setTimeout('$("#' + resource.TaskObjects.Loader + '").show();', 500);

                if (resource.TaskObjects.ContentContainer != null)
                    $("#" + resource.TaskObjects.ContentContainer).show();
                if (resource.TaskObjects.MessageContainer != null)
                    $("#" + resource.TaskObjects.MessageContainer).empty();
            }
        }

        syncRoot = false;

        if (tasks.length != 0)
            SW.Core.WSAsyncExecutor.Execute(tasks);
    }

    WSAsyncExecutor.Execute = function (tasks) {
        if (syncRoot)
            setTimeout(function () { SW.Core.WSAsyncExecutor.Execute(tasks); }, 10);

        syncRoot = true;

        var serverMethods = new Array();

        $.each(tasks, function (key, value) {
            if (value.ServerMethod == null && value.ServerControlDefinition == null)
                setTimeout(function () { SW.Core.WSAsyncExecutor.ProcessData({ "Result": null, "DataResultState": null, "Message": null }, value.ResourceId, value.Hash); }, 1);
            else
                serverMethods.push(value);
        });

        if (serverMethods.length != 0)
            SW.Core.WSAsyncExecutor.callWebService(serverMethods);
        else {
            //no server callback was performed, so the JS just has performed
            //need to check next tasks
            checkingTaskDelegate = setTimeout(function () { SW.Core.WSAsyncExecutor.CheckTasks(); }, forceStart == true ? 1 : CHECKING_INTERVAL);
            forceStart = false;
        }

        syncRoot = false;
    }

    WSAsyncExecutor.Success = function (data) {
        if (syncRoot)
            setTimeout(function () { SW.Core.WSAsyncExecutor.Success(data); }, 10);

        syncRoot = true;

        $.each(data, function (key, value) {
            if (value.IsDone == true)
                setTimeout(function () { SW.Core.WSAsyncExecutor.ProcessData(value.Data, value.ResourceId, value.Hash); }, 1);
            else
                resources[value.ResourceId].Tasks[value.Hash].Running = false;
        });

        checkingTaskDelegate = setTimeout(function () { SW.Core.WSAsyncExecutor.CheckTasks(); }, forceStart == true ? 1 : CHECKING_INTERVAL);
        forceStart = false;

        syncRoot = false;
    }

    WSAsyncExecutor.ProcessData = function (data, resourceId, hash) {
        var resource = resources[resourceId];
        if (resource == undefined)
            return;

        var task = resource.Tasks[hash].Task;

        resource.Results[data.DataResultState == null ? "NoMessage" : data.DataResultState]++;

        if (task[clientMethodIndex] == null) {
            if (task[clientObjectIndex] != null) {
                var dataDiv = $("#" + task[clientObjectIndex]);

                dataDiv.empty();
                dataDiv.append(data.Result);
                dataDiv.show('slow');
            }
        } else {
            if (task[clientMethodIndex] == "SW.Core.WSAsyncExecutor.DoRefreshPage") {
                if (data.Result != null) {
                    window.location = data.Result;
                    return;
                } else {
                    if (task[clientObjectIndex] != null)
                        $("#" + task[clientObjectIndex]).show();
                }
            } else {
                if (task[serverMethodIndex] == null && data.Result == null) {
                    //no server callback... just user defined JS method with user defined parameters
                    eval(task[clientMethodIndex]);
                } else {
                    //server callback with result into the user defined client JS method
                    eval(task[clientMethodIndex] + "(data.Result, task[clientObjectIndex]);");
                }
            }
        }

        if ((resource.Results["Message"] + resource.Results["JustMessage"] != 0) && data.Message != null) {
            //show error message only if servers that we get data from are available
            //if no server is available the error message is not so important... 
            //in that case unreachable flag is more important
            if (resource.TaskObjects.MessageContainer != null) {
                var errorDiv = $("#" + resource.TaskObjects.MessageContainer);

                errorDiv.empty();
                errorDiv.append(data.Message);
                errorDiv.show('slow');
            }

            if (resource.Results["JustMessage"] != 0 && resource.TaskObjects.ContentContainer != null) {
                $("#" + resource.TaskObjects.ContentContainer).hide();
                for (var hash in resource.Tasks) {
                    if(!resource.Tasks.hasOwnProperty(hash))
                        continue;

                    SW.Core.WSAsyncExecutor.DeleteTask(resource, hash);
                }

                return;
            }
        }

        SW.Core.WSAsyncExecutor.DeleteTask(resource, hash);
    }

    WSAsyncExecutor.ArrayLength = function (obj) {
        var length = 0;
        for (var key in obj) {
            if(!obj.hasOwnProperty(key)) continue;
            length++;
        }
        return length;
    }

    WSAsyncExecutor.DeleteTask = function (resource, hash) {
        if (syncRoot)
            setTimeout(function () { SW.Core.WSAsyncExecutor.DeleteTask(resource, hash); }, 10);

        syncRoot = true;

        delete resource.Tasks[hash];
        if (SW.Core.WSAsyncExecutor.ArrayLength(resource.Tasks) == 0) {
            if(resource.Timer != null) {
                clearTimeout(resource.Timer);
                resource.Timer = null;
            }

            if (resource.TaskObjects.Loader != null)
                $("#" + resource.TaskObjects.Loader).hide();

            delete resources[resource.ResourceId];
        }

        syncRoot = false;
    }

    WSAsyncExecutor.handleError = function (xhr, customErrorHandler) {
        if (xhr.status == 401 || xhr.status == 403) {
            // login cookie expired. reload the page so we get bounced to the login page.
            alert('@{R=Core.Strings;K=WEBJS_AK0_66;E=js}');
            window.location.reload();
        }

        var msg;
        if (xhr.status == 12002) {
            // request timeout expired
            msg = timeOutTemplate;
        } else {
            try {
                if (xhr.status != 0 && xhr.statusText != null && xhr.statusText != undefined && xhr.statusText.length != 0)
                    msg = errorTemplate.replace("errortemplate", xhr.status + ": " + xhr.statusText);
            }
            catch (err) {
                msg = errorTemplate.replace("errortemplate", err.description);
            }
        }
        
        var rscs = new Array();
        for (var resourceId in resources) {
            if(!resources.hasOwnProperty(resourceId))
                continue;

            var resource = resources[resourceId];

            rscs.push(resourceId);

            if (resource.TaskObjects.MessageContainer != null) {
                var errorDiv = $("#" + resource.TaskObjects.MessageContainer);

                errorDiv.empty();
                errorDiv.append(msg);
                errorDiv.show('slow');
            }
        }

        $.each(rscs, function(key, value){
            for (var hash in resources[value].Tasks) {
                if(!resources[value].Tasks.hasOwnProperty(hash))
                    continue;

                SW.Core.WSAsyncExecutor.DeleteTask(resources[value], hash);
            }
        });
                       
        if (typeof (customErrorHandler) == 'function')
            customErrorHandler(msg);
    }

    WSAsyncExecutor.callWebService = function (param, onError) {
        var paramString = JSON.stringify(param);

        if (paramString.length === 0) {
            paramString = "{}";
        }

        runningQuery = true;

        $.ajax({
            type: "POST",
            url: "/Orion/PM/Controls/WSAsyncExecuteTasks.aspx",
            data: paramString,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                runningQuery = false;
                SW.Core.WSAsyncExecutor.Success(data);
            },
            error: function (xhr, status, errorThrown) {
                runningQuery = false;
                SW.Core.WSAsyncExecutor.handleError(xhr, onError);
            }
        });
    }
})(SW.Core);