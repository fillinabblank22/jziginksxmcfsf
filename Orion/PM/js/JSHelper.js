﻿function SendMessage(service, method, param, success) {
    var paramString = JSON.stringify(param);

    if (paramString.length === 0)
        paramString = "{}";

    $.ajax({
        type: "POST",
        url: service + "/" + method,
        data: paramString,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) { success(data); }
    });
}

function CheckValidityInput(input) {
    if (input == null)
        return true;

    if (input.indexOf("%") != -1
     || input.indexOf("$") != -1
     || input.indexOf("&") != -1
     || input.indexOf("#") != -1
     || input.indexOf("'") != -1
     || input.indexOf("_") != -1
     || input.indexOf("^") != -1) {
        alert('Search string contains not supported characters');
        return false;
    }

    return true;
}