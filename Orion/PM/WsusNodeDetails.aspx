<%@ Page Language="C#" MasterPageFile="~/Orion/PM/PMView.master" AutoEventWireup="true" CodeFile="WsusNodeDetails.aspx.cs" Inherits="Orion_SCM_WsusNodeDetails" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="pm" Assembly="SolarWinds.PM.Web" Namespace="SolarWinds.PM.Web.NetObjects" %>


<asp:Content runat="server" ContentPlaceHolderID="ViewPageTitle" ID="Content1">


	<h1>
	    <%= DefaultSanitizer.SanitizeHtml(ViewInfo.ViewTitle) %> -
        <a runat="server" id="lnkNode" class="NetObjectLink"></a>
	    <asp:PlaceHolder ID="TitleDecorators" runat="server"></asp:PlaceHolder>	    
	</h1>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
	<pm:WsusNodeResourceHost runat="server" ID="wsusNodeResHost">
	    <orion:ResourceContainer runat="server" ID="resContainer" />
	</pm:WsusNodeResourceHost>
</asp:Content>
