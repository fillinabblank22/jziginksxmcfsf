<%@ WebService Language="C#" Class="SCMInformation" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Services;
using System.ServiceModel;
using System.Web.Script.Services;
using SolarWinds.PM.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.PM.Common.Model;
using SolarWinds.PM.Common.Enums;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class SCMInformation : WebService
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    [WebMethod(EnableSession=true)]
    public DataTable Query(string query)
    {
        return DoQueryWithRetries(query, null);
    }

    [WebMethod(EnableSession = true)]
    public DataTable QueryWithParameters(string query, IDictionary<string, object> parameters)
    {
        return DoQueryWithRetries(query, parameters);
    }

    [WebMethod(EnableSession = true)]
    public string Create(IDictionary<string, object> properties)
    {
        var instanceType = (string)properties["InstanceType"];
        properties.Remove("InstanceType");
        properties.Remove("Uri");
        foreach (var key in properties.Where(y => y.Value == null).Select(y => y.Key).ToList())
            properties.Remove(key);
        using (InformationServiceProxy swis = InformationServiceProxy.CreateV3())
        {
            return swis.Create(instanceType, properties);
        }
    }

    [WebMethod(EnableSession = true)]
    public IDictionary<string, object> Read(string uri)
    {
        using (InformationServiceProxy swis = InformationServiceProxy.CreateV3())
        {
            return swis.Read(uri).ToDictionary(x => x.Key, x => x.Value is DBNull ? null : x.Value);
        }
    }

    [WebMethod(EnableSession = true)]
    public void Update(string uri, IDictionary<string, object> propertiesToUpdate)
    {
        foreach (var key in propertiesToUpdate.Where(y => y.Value == null).Select(y => y.Key).ToList())
            propertiesToUpdate[key] = DBNull.Value;
        using (InformationServiceProxy swis = InformationServiceProxy.CreateV3())
        {
            swis.Update(uri, propertiesToUpdate);
        }
    }

    [WebMethod(EnableSession = true)]
    public void Delete(string uri)
    {
        using (InformationServiceProxy swis = InformationServiceProxy.CreateV3())
        {
            swis.Delete(uri);
        }
    }

    private DataTable DoQueryWithRetries(string query, IDictionary<string, object> parameters)
    {
        int retries = 1;
        while (true)
        {
            --retries;
            try
            {
                return DoQuery(query, parameters);
            }
            catch (FaultException)
            {
                // FaultException is a subclass of CommunicationException. We want to retry on 
                // CommunicationException but not FaultException, so we need to catch and rethrow here.
                throw;
            }
            catch (CommunicationException ex)
            {
                if (retries >= 0)
                    log.Warn("CommunicationException caught. Retrying...", ex);
                else
                    throw;
            }
        }
    }

    public DataTable DoQuery(string query, IDictionary<string, object> parameters)
    {
        using (InformationServiceProxy swis = InformationServiceProxy.CreateV3())
        {
            return swis.Query(query, parameters);
        }
    }
}
