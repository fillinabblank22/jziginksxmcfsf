﻿<%@ WebService Language="C#" Class="ThwackPM" %>

// NOTE:  This is a copy of \Orion\Services\Thwack.asmx.  That version doesn't support
//        other Forums so I had to make my own copy here.  Once that version has
//        that support, this code should be removed and we should use the new code.

using System;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.NPM.Common;
using SolarWinds.NPM.Common.Models;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ThwackPM : System.Web.Services.WebService
{
    private static SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();

    private void BLExceptionHandler(Exception ex)
    {
        _log.Error(ex);
        throw ex;
    }

    [WebMethod]
    public string GetRecentPosts(int postsCount)
    {
        PostItemList posts = GetPostItems("http://thwackfeeds.solarwinds.com/products/community/PatchManager.aspx");
        if (posts != null && posts.ItemList != null)
        {
            return FormatRecentPostsTable(posts.ItemList, postsCount);
        }
        return GetErrorMessage();
    }


    private static PostItemList LoadPostItems(string url)
    {
        System.Xml.XmlDocument dev = new System.Xml.XmlDocument();
        List<PostItem> postItems = new List<PostItem>();

        var request = System.Net.WebRequest.Create(url);
        using (var response = request.GetResponse())
        {
            var dataStream = response.GetResponseStream();

            //string ret;
            //using (var reader = new StreamReader(response.GetResponseStream()))
            //    ret = reader.ReadToEnd();

            try
            {
                dev.Load(dataStream);
            }
            catch (Exception ex)
            {
                _log.Error("Unable to parse thwack response as XML document.", ex);
            }

            foreach (System.Xml.XmlElement item in dev.GetElementsByTagName("item"))
            {
                try
                {
                    var title = item.GetElementsByTagName("title")[0].InnerText;
                    var link = item.GetElementsByTagName("link")[0].InnerText;

                    postItems.Add(new PostItem()
                    {
                        Title = title,
                        Link = link
                    });
                }
                catch (Exception ex)
                {
                    _log.Error("Unable to parse thwack post.", ex);
                }
            }
        }
                
        return new PostItemList() { ItemList = postItems };
    }

    #region Query methods
    private static PostItemList GetPostItems(string url)
    {
        ConfigurePostsManager();
        try
        {
            return LoadPostItems(url);
        }
        catch
        {
            return null;
        }
    }

    private static void ConfigurePostsManager()
    {
        try
        {
            if (ConfigurationManager.AppSettings["proxyAvailable"].ToLowerInvariant().Equals("true", StringComparison.InvariantCulture))
            {
                PostsManagerGeneric<PostItemList>.ProxyAvailable = true;
                PostsManagerGeneric<PostItemList>.SetProxyInfo(ConfigurationManager.AppSettings["proxyAddress"], Convert.ToInt32(ConfigurationManager.AppSettings["proxyPort"]),
                    ConfigurationManager.AppSettings["userName"], ConfigurationManager.AppSettings["password"]);
                return;
            }
        }
        catch { }
        // no need to handle this one, just set proxy to false
        PostsManagerGeneric<PostItemList>.ProxyAvailable = false;
    }

    #endregion

    #region Formatting methods
    private static string GetErrorMessage()
    {
        return string.Format("<div class=\"ThwackConnectError\">{0}</div>", Resources.CoreWebContent.WEBCODE_AK0_171);
    }

    private static void WriteSeparator(HtmlTextWriter writer)
    {
        writer.RenderBeginTag(HtmlTextWriterTag.Tr);
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Colspan, "3");
            writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");
            writer.AddStyleAttribute(HtmlTextWriterStyle.Padding, "5px 10px 5px 10px");
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            {
                writer.AddStyleAttribute("*display", "inline-block");
                //writer.AddStyleAttribute(HtmlTextWriterStyle.VerticalAlign, "bottom");
                writer.AddStyleAttribute("height", "1px");
                writer.AddStyleAttribute("border-bottom", "1px dashed #333333");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.RenderEndTag(); // Div
            }
            writer.RenderEndTag(); // Td
        }
        writer.RenderEndTag(); // Tr
    }

    private static string FormatRecentPostsTable(List<PostItem> posts, int count)
    {
        StringWriter content = new StringWriter();
        HtmlTextWriter writer = new HtmlTextWriter(content);

        writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding, @"0");
        writer.AddAttribute(HtmlTextWriterAttribute.Cellspacing, @"0");
        writer.RenderBeginTag(HtmlTextWriterTag.Table);

        int realCount = Math.Min(count, posts.Count);
        for (int i = 0; i < realCount; i++)
        {
            if (i % 2 != 0)
            {
                writer.AddStyleAttribute(HtmlTextWriterStyle.BackgroundColor, @"#e4f1f8");
            }
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, @"0px");
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.AddStyleAttribute(HtmlTextWriterStyle.MarginLeft, @"10px");

            writer.RenderBeginTag(HtmlTextWriterTag.Ul);


            writer.AddStyleAttribute(HtmlTextWriterStyle.ListStyleType, @"disc !important");
            writer.RenderBeginTag(HtmlTextWriterTag.Li);


            writer.AddAttribute(HtmlTextWriterAttribute.Href, posts[i].Link);
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.WriteEncodedText(posts[i].Title);

            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();
        }
        writer.RenderEndTag();
        return content.ToString();
    }
    #endregion
}