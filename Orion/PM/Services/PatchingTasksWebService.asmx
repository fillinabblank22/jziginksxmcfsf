﻿<%@ WebService Language="C#" Class="PatchingTasksWebService" %>

using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.PM.Web.DAL;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class PatchingTasksWebService : WebService
{
    private string convertDataTableToHTML(DataTable dt)
    {
        int coulumnWidth = 100 / dt.Columns.Count;

        string html = "<table style=\"width:100%;\">";
        
        //add header row
        html += "<thead style=\"display:block;\">";
        html += "<tr class=\"HeaderRow\" style=\"width: 100%; display:inline-table;\">";
        for (int i = 0; i < dt.Columns.Count; i++)
            html += "<th style=\"width:" + coulumnWidth + "%\" class=\"ReportHeader Sortable\">" + dt.Columns[i].ColumnName.ToUpper() + "</th>";
        html += "</tr>";
        html += "</thead>";

        html += "<tbody id=\"tableTasksResults\" style=\"display:block;overflow:auto;height:200px;width:100%;\">";
            
        //add rows
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string backgroundColor = i % 2 == 0 ? "#f6f6f6" : "#ffffff";

            html += String.Format(
                "<tr style=\"padding-top: 5px;padding-bottom: 5px;width: 100%; display:inline-table;background-color: {0};\" class=\"column{1}\">",
                backgroundColor, i + 1);

            for (int j = 0; j < dt.Columns.Count; j++)
                html += String.Format("<td style=\"width: {0}%;\">{1}</td>",coulumnWidth, dt.Rows[i][j]);
            html += "</tr>";
        }
       
        html += "</tbody>";
        html += "</table>";
        return html;
    }

    [WebMethod]
    public string GetTasksResultsHtmlTableById(string taskId)
    {
        Guid guidId;
        if (Guid.TryParse(taskId, out guidId))
        {
            return convertDataTableToHTML(new LastXXTasksDAL().GetTasksResultsByTaskId(guidId));
        }
        else
        {
            return String.Empty;
        }
    }
}

