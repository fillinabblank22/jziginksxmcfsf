﻿<%@ WebService Language="C#" Class="WsusNodesByTargetGroupsPageService" %>

using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.SharedCredentials.Exceptions;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class WsusNodesByTargetGroupsPageService : System.Web.Services.WebService
{
    private static readonly Log log = new Log();

    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

    [WebMethod(true)]
    public void Search(string search, string viewDetailId)
    {
        Session[String.Concat(SolarWinds.PM.Web.Helper.ResourceHelper.WSUS_NODES_BY_TARGET_GROUPS_PAGE_PREFIX, viewDetailId, "SearchKey")] = search;
    }

    [WebMethod(true)]
    public void TreeFilterWithSearch(string viewDetailId, string nodeId, string search)
    {
        Search(search, viewDetailId);
        SetClickedNode(nodeId, viewDetailId);
    }

    [WebMethod(true)]
    public void SetClickedNode(string nodeId, string viewDetailId)
    {
        var id = SolarWinds.PM.Web.Helper.Serializer.Deserialize<Object[]>(nodeId);
        var node = new SolarWinds.PM.Web.Controls.AssetTree.AssetTreeNode(id);

        var idSplited = node.NodeID.ToString().Split(',');
        Session[String.Concat(SolarWinds.PM.Web.Helper.ResourceHelper.WSUS_NODES_BY_TARGET_GROUPS_PAGE_PREFIX, viewDetailId, "DeviceId")] = idSplited[1];
        Session[String.Concat(SolarWinds.PM.Web.Helper.ResourceHelper.WSUS_NODES_BY_TARGET_GROUPS_PAGE_PREFIX, viewDetailId, "GroupId")] = idSplited[0];
        Session[String.Concat(SolarWinds.PM.Web.Helper.ResourceHelper.WSUS_NODES_BY_TARGET_GROUPS_PAGE_PREFIX, viewDetailId, "NodeParameters")] = nodeId;
    }

    [WebMethod(true)]
    public PageableDataTable GetNodes(string viewDetailId)
    {
        try
        {
            string search = String.IsNullOrEmpty(Session[String.Concat(SolarWinds.PM.Web.Helper.ResourceHelper.WSUS_NODES_BY_TARGET_GROUPS_PAGE_PREFIX, viewDetailId, "SearchKey")] as string) 
                            ? string.Empty
                            : Session[String.Concat(SolarWinds.PM.Web.Helper.ResourceHelper.WSUS_NODES_BY_TARGET_GROUPS_PAGE_PREFIX, viewDetailId, "SearchKey")].ToString();
            Guid groupId = String.IsNullOrEmpty(Session[String.Concat(SolarWinds.PM.Web.Helper.ResourceHelper.WSUS_NODES_BY_TARGET_GROUPS_PAGE_PREFIX, viewDetailId, "GroupId")] as string) 
                            ? Guid.Empty
                            : new Guid(Session[String.Concat(SolarWinds.PM.Web.Helper.ResourceHelper.WSUS_NODES_BY_TARGET_GROUPS_PAGE_PREFIX, viewDetailId, "GroupId")] as string);
            Guid deviceId = String.IsNullOrEmpty(Session[String.Concat(SolarWinds.PM.Web.Helper.ResourceHelper.WSUS_NODES_BY_TARGET_GROUPS_PAGE_PREFIX, viewDetailId, "DeviceId")] as string) 
                            ? Guid.Empty
                            : new Guid(Session[String.Concat(SolarWinds.PM.Web.Helper.ResourceHelper.WSUS_NODES_BY_TARGET_GROUPS_PAGE_PREFIX, viewDetailId, "DeviceId")] as string);
           
            string sortColumn = this.Context.Request.QueryString["sort"];
            string sortDirection = this.Context.Request.QueryString["dir"];

            int pageSize;
            int startRowNumber;
            Int32.TryParse(this.Context.Request.QueryString["limit"], out pageSize);
            Int32.TryParse(this.Context.Request.QueryString["start"], out startRowNumber);

            if (pageSize == 0)
                pageSize = 10;

            DataTable table = new DataTable();
            table.Columns.Add("Link", typeof(string));
            table.Columns.Add("Image", typeof(string));
            table.Columns.Add("Node", typeof(string));
            table.Columns.Add("IPAddress", typeof(string));
            table.Columns.Add("Servername", typeof(string));
            table.Columns.Add("LinkServer", typeof(string));
            table.Columns.Add("ImageServer", typeof(string));

            var wsusServers = new Dictionary<string, SolarWinds.PM.Web.Model.WsusServerEntity>();
            var nodeList = new SolarWinds.PM.Web.DAL.WsusNodesByTargetGroupsPageDAL().GetListOfNodes(deviceId, groupId, search, sortColumn, sortDirection, startRowNumber + 1, startRowNumber + pageSize);
            foreach (var item in nodeList)
            {
                if (!wsusServers.ContainsKey(item.WsusServer.NetObjectId))
                {
                    wsusServers.Add(item.WsusServer.NetObjectId, new SolarWinds.PM.Web.Model.WsusServerEntity(item.WsusServer.NetObjectId));
                    wsusServers[item.WsusServer.NetObjectId].RefreshData();
                }
                                    
                string img;

                if (item.OrionNodeId == null)
                    img = "/Orion/PM/images/Workstation.png";
                else
                    img = String.Format("/Orion/images/StatusIcons/Small-{0}", item.OrionNodeStatusIcon);

                table.Rows.Add("/Orion/PM/WsusNodeDetails.aspx?NetObject=" + item.NetObjectId
                             , img
                             , item.FullDomainName
                             , item.IPAddress.ToString()
                             , wsusServers[item.WsusServer.NetObjectId].Servername
                             , "/Orion/PM/WsusServerDetails.aspx?NetObject=" + item.WsusServer.NetObjectId
                             , "/Orion/PM/images/WsusServer_16x16.png");
            }

            return new PageableDataTable(table, new SolarWinds.PM.Web.DAL.WsusNodesByTargetGroupsPageDAL().GetCount(deviceId, groupId, search));
        }
        catch (Exception ex)
        {
            log.Error("Exception occured when getting 'WsusNode grouped by Target Groups' table to the grid", ex);
            throw;
        }
    }
}


