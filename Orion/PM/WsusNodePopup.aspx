﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WsusNodePopup.aspx.cs" Inherits="Orion_SCM_WsusNodePopup" %>
<%@ Register Assembly="SolarWinds.PM.Web" Namespace="SolarWinds.PM.Web.Charting" TagPrefix="charts" %>
<%@ Register TagPrefix="NodePopup" TagName="NodePopupControl" Src="~/Orion/PM/NodePopup.ascx" %>

<%--This is intended to be displayed as the body of a cluetip tooltip/popup.--%>


<%if (this.IsNotOrion) { %>
    <h3 class="StatusUndefined"><asp:literal runat="server" id="lblTitle" /></h3>
<%} else { %>
    <NodePopup:NodePopupControl runat="server" ID="nodePopup" />
<%} %>

<div class="NetObjectTipBody">
    <p class="StatusDescription">
	    <table border="0" cellpadding="2" cellspacing="0" width="100%" style="white-space: nowrap;">
<%if (this.IsNotOrion) { %>
  	        <tr>
              <td><span style="white-space: nowrap; font-weight: bold;"><asp:Literal ID="lblStatusCaption" runat="server" />:&nbsp;&nbsp;</span></td>
              <td><img runat="server" id="imgStatus" />&nbsp;<asp:Literal ID="lblStatus" runat="server" /></td>
	        </tr>
	        <tr>
              <td><span style="white-space: nowrap; font-weight: bold;"><asp:Literal ID="lblIpAddressCaption" runat="server" />:&nbsp;&nbsp;</span></td>
              <td><asp:Literal ID="lblIpAddress" runat="server" /></td>
	        </tr>
	        <tr>
              <td><span style="white-space: nowrap; font-weight: bold;"><asp:Literal ID="lblMachineTypeCaption" runat="server" />:&nbsp;&nbsp;</span></td>
              <td><img runat="server" id="imgMachineType" />&nbsp;<asp:Literal ID="lblMachineType" runat="server" /></td>
	        </tr>
	        <tr>
              <td><span style="white-space: nowrap; font-weight: bold;"><asp:Literal ID="lblOSDescriptionCaption" runat="server" />:&nbsp;&nbsp;</span></td>
              <td><img runat="server" id="imgVendor" />&nbsp;<asp:Literal ID="lblOSDescription" runat="server" /></td>
	        </tr>
<%} %>
	        <tr>
              <td><span style="white-space: nowrap; font-weight: bold;"><asp:Literal ID="lblWsusServerIPAddressCaption" runat="server" />:&nbsp;&nbsp;</span></td>
              <td><asp:Literal ID="lblWsusServerIPAddress" runat="server" /></td>
	        </tr>
            <tr>
              <td><span style="white-space: nowrap; font-weight: bold;"><asp:Literal ID="lblWsusServerNameCaption" runat="server" />:&nbsp;&nbsp;</span></td>
              <td><asp:Literal ID="lblWsusServerName" runat="server" /></td>
	        </tr>
	        <tr>
              <td><span style="white-space: nowrap; font-weight: bold;"><asp:Literal ID="lblDescriptionCaption" runat="server" />:&nbsp;&nbsp;</span></td>
              <td><asp:Literal ID="lblDescription" runat="server" /></td>
	        </tr>
            <tr>
              <td><span style="white-space: nowrap; font-weight: bold;"><asp:Literal ID="lblGraph" runat="server" />:&nbsp;&nbsp;</span></td>
              <td>
                <table>
                    <tr>
                        <td><charts:ScmChartImage runat="server" id="imgGraphDetail" /></td>
                        <td>
                            <table cellspacing="2">
                                <tr>
                                    <td style="padding: 0px; background-color: #77BD2D; width: 1%; color: White; text-align:center; font-weight: bold;">&nbsp;<asp:Literal ID="lblInstalledCount" runat="server" />&nbsp;</td>
                                    <td style="padding: 0px;"><span style="white-space: nowrap;"><asp:Literal ID="lblInstalled" runat="server" /></span></td>
                                </tr>
                                <tr>
                                    <td style="padding: 0px; background-color: #F88017; width: 1%; color: White; text-align:center; font-weight: bold;">&nbsp;<asp:Literal ID="lblNeededCount" runat="server" />&nbsp;</td>
                                    <td style="padding: 0px;"><span style="white-space: nowrap;"><asp:Literal ID="lblNeeded" runat="server" /></span></td>
                                </tr>
                            </table>
                       </td>
                    </tr>
                </table>
              </td>
            </tr>
	    </table>
    </p>
</div>
