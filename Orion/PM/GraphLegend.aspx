<%@ Page Language="C#" MasterPageFile="~/Orion/PM/PM.master" AutoEventWireup="true" CodeFile="GraphLegend.aspx.cs" Inherits="Orion_PM_GraphLegend" %>
<%@ Register TagPrefix="orion" TagName="NetObjectTips" Src="~/Orion/Controls/NetObjectTips.ascx" %>
<%@ Register TagPrefix="orion" TagName="IncludeExtJs" Src="~/Orion/Controls/IncludeExtJs.ascx" %>

<asp:Content ContentPlaceHolderID="TopRightPageLinks" runat="server" ID="TopRightPageLinksID" Visible="false">
    <orion:Include Module="PM" File="SCM.css" runat="server" />

    <% if (string.IsNullOrEmpty(Request.QueryString["IsMobileView"]) || !Request.QueryString["IsMobileView"].Equals("true", StringComparison.OrdinalIgnoreCase))
    {%>
        <div>
            <a runat="server" id="helpLink" target="_blank" class="helpLink">What I can do with Legend Detail</a>
            <orion:IconLinkExportToPDF runat="server" ID="ExportToPDFLink" />
            <%if (Profile.AllowAdmin && !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
                <a class="settingsLink" href="/Orion/PM/Admin/Default.aspx">Patch Manager Settings</a>
            <%}%>
        </div>
    <%} %>
</asp:Content>

<asp:Content ContentPlaceHolderID="HeadPlaceHolder" runat="server">
    <orion:Include File="jquery/ui.core.js" runat="server" />
    <orion:Include File="Resources.css" runat="server" />

    <script type="text/javascript" language="javascript">
        function Toggle(className) {
            $("." + className).each(function (index) {
                var row = $(this);

                if (row.css("display") == "none") {
                    row.fadeIn();
                } else {
                    row.css("display", "none");
                }
            });

        }

        function ChangeExpander(image) {
            var src = $(image).attr("src");

            if (src.indexOf("/Orion/images/Button.Expand.gif") == -1) {
                $(image).attr("src", "/Orion/images/Button.Expand.gif");
                $(image).attr("alt", "[+]");
            }
            else {
                $(image).attr("src", "/Orion/images/Button.Collapse.gif");
                $(image).attr("alt", "[-]");
            }
        }
    </script></asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1><asp:literal runat="server" ID="lblTitle" /></h1>

    <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
        <table cellpadding="10px">
            <tr>
                <td style="font-weight: bold; font-size: small; color: Red">
                    <%= DefaultSanitizer.SanitizeHtml(this.CustomErrorMessage) %>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <div style="padding-left: 10px; padding-top: 25px;" >
        <table cellpadding = "0" cellspacing = "0">
            <tr>
                <td>
                    <div runat="server" id="dataDiv" style="padding: 20px; padding-top: 10px; background-color: White; border-color: Silver; border-style: solid; border-width: thin;">

                        <asp:Repeater runat="server" ID="resourceTable" OnItemCreated="resourceTable_ItemCreated">
                            <HeaderTemplate>
                                <table cellpadding = "0" cellspacing = "6">
                            </HeaderTemplate>
                            <ItemTemplate>
	                            <tr>
	                                <td style="font-size: 12px; padding-right:5px; vertical-align: middle; text-align:left;"><img style="cursor: pointer; margin-top: 3px;" alt="Icon" src="<%# DefaultSanitizer.SanitizeHtml(this.GetImage) %>" onclick="Toggle('<%# DefaultSanitizer.SanitizeHtml(this.GetGroup) %>'); ChangeExpander(this);" /></td>
	                                <td style="color: white; font-weight: bold; text-align: center; font-size: 12px; background-color: <%# DefaultSanitizer.SanitizeHtml(this.Legend.Color) %>;">&nbsp;<%# DefaultSanitizer.SanitizeHtml(this.Legend.Value) %>&nbsp;</td>
	                                <td class="Property" style="font-weight: bold; font-size: 12px; border-color: #C0C0C0;"><%# DefaultSanitizer.SanitizeHtml(this.Legend.Name) %></td>
                                </tr>

                                <asp:Repeater runat="server" ID="resourceTableRow" DataSource="<%# DefaultSanitizer.SanitizeHtml(this.Legend.Rows) %>" OnItemCreated="resourceTableRow_ItemCreated">
                                    <HeaderTemplate>
                                        <tr class="<%# DefaultSanitizer.SanitizeHtml(this.GetGroup) %>" style="<%# DefaultSanitizer.SanitizeHtml(this.GetStyleDetail) %>">
	                                        <td colspan="2">&nbsp;</td>
	                                        <td style="padding-bottom: 10px;">
                                                <table cellpadding = "0" cellspacing = "0" style="width: 100%">
                                                    <tr>
                                                        <asp:Repeater runat="server" ID="resourceTableInnerHeader" DataSource="<%# DefaultSanitizer.SanitizeHtml(this.Header) %>">
                                                            <ItemTemplate>
                                                                <td class="ReportHeader"><%# DefaultSanitizer.SanitizeHtml(Container.DataItem.ToString().ToUpperInvariant()) %></td>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </tr>       
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                                    <tr>
                                                <asp:Repeater runat="server" ID="resourceTableColumn" DataSource="<%# DefaultSanitizer.SanitizeHtml(this.LegendRow.Columns) %>" OnItemCreated="resourceTableColumn_ItemCreated">
                                                    <ItemTemplate>
	                                                        <td class="<%# DefaultSanitizer.SanitizeHtml(this.LegendRow.Class) %>" style="padding-left: 3px; padding-right: 3px; border-width: 0px; font-size: 12px; white-space: <%# DefaultSanitizer.SanitizeHtml(this.GetColumnWrapping) %>">
                                                                <img alt="<%# DefaultSanitizer.SanitizeHtml(this.LegendColumn.Alt) %>" src="<%# DefaultSanitizer.SanitizeHtml(this.LegendColumn.Icon) %>" />&nbsp;
                                                                <a href="<%# DefaultSanitizer.SanitizeHtml(this.LegendColumn.Link) %>"><%# DefaultSanitizer.SanitizeHtml(this.LegendColumn.Name) %></a>
                                                            </td>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                    </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                                    <tr><td colspan="<%# DefaultSanitizer.SanitizeHtml(this.Header.Count) %>" style="font-size: 12px; font-weight: bold; font-style: italic; padding-top: 6px; <%# DefaultSanitizer.SanitizeHtml(this.GetDisplayOfMessage) %>"><%# DefaultSanitizer.SanitizeHtml(this.MoreRecordsIsHiddenCaption) %></td></tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>


                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
