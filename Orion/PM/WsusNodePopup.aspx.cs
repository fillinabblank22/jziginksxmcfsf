﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.PM.Web.DAL;
using SolarWinds.PM.Web.NetObjects;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Common;
using SolarWinds.PM.Web.Charting;
using SolarWinds.Orion.Core.Web;
using System.Data;
using SolarWinds.Orion.Web.DAL;
using System.IO;
using SolarWinds.PM.Web.Model;
using System.Net;
using SolarWinds.Orion.NPM.Web;
using System.Text.RegularExpressions;

public partial class Orion_SCM_WsusNodePopup : System.Web.UI.Page
{
    private readonly string IpAddressCaption = "IP Address";
    private readonly string MachineTypeCaption = "Machine Type";
    private readonly string DescriptionCaption = "Description";
    private readonly string WsusServerIpAddressCaption = "Wsus Server";
    private readonly string WsusServerNameCaption = "Wsus Server Name";
    private readonly string GraphCaption = "Updates Details";
    private readonly string OSDescriptionCaption = "Operating System";
    private readonly string StatusCaption = "Node Status";
    private readonly string UnmanagedNodeCaption = "Node is not managed by Orion";

    private readonly string InstalledCaption = "Installed updates";
    private readonly string NeededCaption = "Needed updates";

    private WsusNode node = null;

    public Boolean IsNotOrion { get { return node == null ? true : node.NodeDetails == null ? true : node.NodeDetails.OrionNodeId == default(Int32?); } }

    protected void Page_Load(object sender, EventArgs e)
    {
        new SolarWinds.PM.Web.TaskServices.Node.WsusNodeDetails().Init(new Object[] { Guid.Empty, Request.Url, false }, null);

        var netObjReq = Request["NetObject"];
        bool justApprovedUpdates;
        string netObj = SolarWinds.PM.Web.Helper.ParseNetObjectIDHelper.ParseRequestNetObjectId(netObjReq, out justApprovedUpdates);

        node = NetObjectFactory.Create(netObj, true) as WsusNode;
        
        if (node == null || node.NodeDetails == null)
        {
            this.Visible = false;
            return;
        }

        lblTitle.Text = node.Name;

        lblIpAddressCaption.Text = IpAddressCaption;
        lblMachineTypeCaption.Text = MachineTypeCaption;
        lblDescriptionCaption.Text = DescriptionCaption;
        lblWsusServerIPAddressCaption.Text = WsusServerIpAddressCaption;
        lblWsusServerNameCaption.Text = WsusServerNameCaption;
        lblGraph.Text = GraphCaption;
        lblOSDescriptionCaption.Text = OSDescriptionCaption;
        lblStatusCaption.Text = StatusCaption;


        if (node.NodeDetails.OrionNodeId == default(Int32?))
        {
            imgStatus.Src = "/Orion/images/StatusIcons/Small-Unknown.gif";
            lblStatus.Text = UnmanagedNodeCaption;
        }
        else
        {
            nodePopup.Node = NetObjectFactory.Create("N:" + node.NodeDetails.OrionNodeId.Value, true) as Node;
        }
        
        
        lblIpAddress.Text = MakeBreakableString(node.NodeDetails.IPAddress);
        lblMachineType.Text = MakeBreakableString(node.NodeDetails.ComputerRole);
        lblOSDescription.Text = MakeBreakableString(node.NodeDetails.OSDescription);
        lblDescription.Text = MakeBreakableString(String.Format("{0}, {1}, {2} ", node.NodeDetails.OSArchitecture, node.NodeDetails.Make, node.NodeDetails.Model));
        if (node.NodeDetails.WsusServer.IPAddress != null)
        {
            lblWsusServerIPAddress.Text = MakeBreakableString(node.NodeDetails.WsusServer.IPAddress.ToStringIp());
            lblWsusServerName.Text = MakeBreakableString(node.NodeDetails.WsusServer.Servername);
        }

        imgMachineType.Src = node.NodeDetails.ComputerRole.ToLower().Contains("server") ? "/Orion/PM/images/device_16x16.gif" : "/NetPerfMon/images/Endpoints/endpoint.gif";
        imgVendor.Src = node.NodeDetails.OSDescription.ToLower().Contains("microsoft") || node.NodeDetails.OSDescription.ToLower().Contains("windows") ? "/NetPerfMon/images/Vendors/311.gif" : "/NetPerfMon/images/Vendors/Unknown.gif";


        var chartInfo = SolarWinds.Orion.Web.Charting.ChartInfoFactory.Create("WSUSNodeUpdatesSummary") as WSUSNodeUpdatesSummaryChartInfo;
        chartInfo.Height = chartInfo.Width = 80;

        chartInfo.InstalledCount = node.NodeDetails.InstalledCount;
        chartInfo.NeededCount = (justApprovedUpdates ? node.NodeDetails.NotInstalledApprovedCount : node.NodeDetails.NotInstalledCount) + node.NodeDetails.FailedCount + node.NodeDetails.DownloadedCount + node.NodeDetails.InstalledPendingRebootCount;

        imgGraphDetail.ChartInfo = chartInfo;

        lblInstalledCount.Text = chartInfo.InstalledCount.ToString();
        lblNeededCount.Text = chartInfo.NeededCount.ToString();

        lblInstalled.Text = InstalledCaption;
        lblNeeded.Text = NeededCaption;


        this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
    }

    protected string MakeBreakableString(object val)
    {
        if (val == null)
            return string.Empty;

        if (Request.Browser.Browser == "IE" && Request.Browser.MajorVersion == 6)
            return FormatHelper.MakeBreakableString(val.ToString(), true);
        else
            return FormatHelper.MakeBreakableString(val.ToString(), false);
    }

    public string GetNodeState()
    {
        if (node == null)
            return "Undefined";

        return "Up";
    }
}
