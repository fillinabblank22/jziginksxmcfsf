﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using SolarWinds.PM.Web.DAL;
using SolarWinds.Orion.Web;
using SolarWinds.Logging;

public partial class Orion_PMViewNoBreadCrumbs : System.Web.UI.MasterPage
{
    private static readonly Log log = new Log();

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        MasterRefresher.RefreshExecutionList.Add(new TaskExecutionEntity() { ClientMethod = String.Format("window.location = '{0}';", Request.Url) });
    }

    protected override void OnInit(EventArgs e)
    {
        var dt = DateTime.Now;
        var state = new CheckEWDataGridAvailabilityDAL().CheckAvailability(true);
        log.DebugFormat("Check Service Availability took {0}ms", (DateTime.Now - dt).TotalMilliseconds);
        
        //if (!state.IsAvailable)
        //{
        //    var ex = new Exception(state.Message);
        //    var detail = new ErrorInspectorDetails() { Title = state.State.ToString(), Error = ex };
        //    HttpContext.Current.Cache["sw.error." + detail.Id] = detail;

        //    Response.Redirect(detail.ErrorPageResolved);
        //}

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(HelpFragment))
        {
            btnHelp.HelpUrlFragment = this.HelpFragment;
            btnHelp.Visible = true;
        }
    }

    public string HelpFragment { get; set; }
}