﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.PM.Web.Charting;
using SolarWinds.PM.Web.DAL;
using System.Data;
using SolarWinds.PM.Web.Resources;
using SolarWinds.PM.Web.NetObjects;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using SolarWinds.PM.Web.Model.ChartLegendModel;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using SolarWinds.PM.Web.Resources.Update;
using SolarWinds.PM.Web.Helper;

public partial class Orion_PM_Controls_Update_UpdateNodesDetailCtrl : ScmResourceBaseAsync
{
    public String NetObjectId { get; set; }
    public String DefaultTitle { get; set; }
    public Boolean JustApprovedUpdates { get; set; }
    public String LegendHelpFragment { get; set; }

    private readonly string NoUpdatesToDisplayCaption = "No nodes to display";

    private readonly string InstalledCaption = "Installed";
    private readonly string NotInstalledCaption = "Not installed";
    private readonly string FailedCaption = "Failed";
    private readonly string DownloadedCaption = "Downloaded";
    private readonly string InstalledPendingRebootCaption = "Installed pending reboot";
    private readonly string UnknownCaption = "Unknown";
    private readonly string NotApplicableCaption = "Not applicable";
    private readonly string LegendTitleCaption = "Node Count";

    private readonly string NodeNameLegendFieldCaption = "Node name";
    private readonly string WsusServerLegendFieldCaption = "Wsus server";


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            lblNoDataToDisplay.InnerHtml = ResourceHelper.GetInfoMessageHtml(NoUpdatesToDisplayCaption);

            var detailsData = new WsusNodesDetail.DetailsData()
            {
                Installed = InstalledCaption,
                NotInstalled = NotInstalledCaption,
                Failed = FailedCaption,
                Downloaded = DownloadedCaption,
                InstalledPendingReboot = InstalledPendingRebootCaption,
                NotApplicable = NotApplicableCaption,
                Unknown = UnknownCaption,
                JustApprovedUpdates = this.JustApprovedUpdates,
            };
            var legendData = new WsusNodesBase.LegendData()
            {
                Title = this.DefaultTitle,
                HelpFragment = this.LegendHelpFragment,
                NodeName = NodeNameLegendFieldCaption,
                WsusServer = WsusServerLegendFieldCaption,
            };
            var table = new WsusNodesDetail().GetData(this.NetObjectId, detailsData, legendData);

            this.resourceTable.DataSource = table;
            this.resourceTable.DataBind();


            var chartInfo = SolarWinds.Orion.Web.Charting.ChartInfoFactory.Create("UpdateNodesDetail") as UpdateNodesDetailChartInfo;
            chartInfo.Height = chartInfo.Width = 200;

            chartInfo.InstalledCount = Convert.ToInt32(table.Rows[0]["Value"]); ;
            chartInfo.NotInstalledCount = Convert.ToInt32(table.Rows[1]["Value"]); ;
            chartInfo.FailedCount = Convert.ToInt32(table.Rows[2]["Value"]); ;
            chartInfo.DownloadedCount = Convert.ToInt32(table.Rows[3]["Value"]); ;
            chartInfo.InstalledPendingRebootCount = Convert.ToInt32(table.Rows[4]["Value"]); ;
            chartInfo.UnknownCount = Convert.ToInt32(table.Rows[5]["Value"]); ;
            chartInfo.NotApplicableCount = Convert.ToInt32(table.Rows[6]["Value"]); ;

            imgGraph.ChartInfo = chartInfo;

            var count = chartInfo.InstalledCount
                      + chartInfo.NotInstalledCount
                      + chartInfo.FailedCount
                      + chartInfo.DownloadedCount
                      + chartInfo.InstalledPendingRebootCount
                      + chartInfo.UnknownCount
                      + chartInfo.NotApplicableCount;

            this.lblNodeCountCaption.Text = LegendTitleCaption;
            this.lblNodeCount.Text = count.ToString();

            dataDiv.Visible = count != 0;
            lblNoDataToDisplay.Visible = count == 0;
        }
        catch (Exception)
        {
            return;
        }
    }
}
