﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.PM.Web.Charting;
using SolarWinds.PM.Web.DAL;
using System.Data;
using SolarWinds.PM.Web.Resources;
using SolarWinds.PM.Web.NetObjects;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using SolarWinds.Orion.Web;
using SolarWinds.PM.Web.Helper;
using System.Text;
using System.Web.Script.Serialization;
using SolarWinds.PM.Web.Resources.Update;

public partial class Orion_PM_Controls_Update_GroupsMissingUpdateCtrl : ScmResourceBaseAsync
{
    public Int32 WarningThreshold { get; set; }
    public Int32 CriticalThreshold { get; set; }
    public Boolean JustApprovedUpdates { get; set; }
    public String UpdateInstallationState { get; set; }
    public String NetObjectId { get; set; }
    public Int32 MaxRecords { get; set; }

    private const string NoDataToDisplayCaption = "No data to display";

    protected readonly string LoadingCaption = "Loading...";

    protected readonly string GroupNameCaption = "Group Name";
    protected readonly string UpdatesMissingCaption = "Missing Update";

    protected String GetColorThreshold(Int32 value)
    {
        return value >= WarningThreshold ? "color: red;" : String.Empty;
    }

    protected String GetWeightThreshold(Int32 value)
    {
        return value >= CriticalThreshold ? "font-weight: bold;" : String.Empty;
    }

    protected String GetId(Object id)
    {
        return id.ToString().Replace(SolarWinds.PM.Web.NetObjects.WsusNode.ID_DELIMITER, '_').Replace(":", "_").Replace("-", "_");
    }

    protected String PlusDisplayStyle(Object value)
    {
        return Convert.ToInt32(value) == 0 ? "display: none;" : string.Empty;
    }

    protected String GetDataToLoad(String id)
    {
        var taskList = new List<TaskExecutionEntity>()
        {
            new TaskExecutionEntity()
            {
                ServerResourceControl = new ServerResourceModel("/Orion/PM/Controls/Update/GroupsMissingUpdateNodeListCtrl.ascx"
                                                             , new Dictionary<string, object>()
                                                             {
                                                                 { "GroupId", id},
                                                                 { "NetObjectId", this.NetObjectId},
                                                                 { "JustApprovedUpdates", this.JustApprovedUpdates },
                                                                 { "UpdateInstallationState", this.UpdateInstallationState },
                                                             }
                                                             , new SolarWinds.PM.Web.TaskServices.Update.UpdateGroup().Load
                                                             , new object[] { this.NetObjectId }),
            }
        };

        return FormatArray(taskList.Select(s => s.ToJson()).ToArray());
    }

    protected string FormatArray(object[] obj)
    {
        StringBuilder sb = new StringBuilder();
        JavaScriptSerializer js = new JavaScriptSerializer();
        return js.Serialize(obj);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        lblNoDataToDisplay.InnerHtml = ResourceHelper.GetInfoMessageHtml(NoDataToDisplayCaption);

        try
        {
            var table = new GroupsMissingUpdate().GetGroups(this.MaxRecords, this.NetObjectId, this.JustApprovedUpdates, this.UpdateInstallationState);

            this.resourceTable.DataSource = table;
            this.resourceTable.DataBind();

            dataDiv.Visible = table.Rows.Count != 0;
            lblNoDataToDisplay.Visible = !dataDiv.Visible;
        }
        catch (Exception)
        {
            dataDiv.Visible = false;
            lblNoDataToDisplay.Visible = !dataDiv.Visible;
        }
    }
}
