﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.PM.Web.Charting;
using SolarWinds.PM.Web.DAL;
using System.Data;
using SolarWinds.PM.Web.Resources;
using SolarWinds.PM.Web.NetObjects;
using System.Web.UI.HtmlControls;
using SolarWinds.Logging;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using SolarWinds.PM.Web.Model;
using SolarWinds.PM.Web.Helper;

public partial class Orion_PM_Controls_Update_UpdateDetailsCtrl : ScmResourceBaseAsync
{
    public String NetObjectId { get; set; }

    private readonly string NoDataToDisplayCaption = "No data to display";

    protected readonly string DescriptionCaption = "Description";
    protected readonly string ProductCaption = "Product";
    protected readonly string CompanyCaption = "Company";
    protected readonly string ProductFamilyCaption = "Product Family";
    protected readonly string CreationDateCaption = "Creation Date";
    protected readonly string ArivalDateCaption = "Arrival Date";
    protected readonly string SeverityCaption = "Severity";
    protected readonly string IsApprovedCaption = "Is Approved";
    protected readonly string HasStaleUpdateApprovalsCaption = "Has Stale Update Approvals";
    protected readonly string HasSupersededUpdatesCaption = "Has Superseded Updates";
    protected readonly string IsDeclinedCaption = "Is Declined";
    protected readonly string IsWsusInfrastructureUpdateCaption = "Is WsusInfrastructure Update";
    protected readonly string KnowledgebaseArticlesCaption = "Knowledgebase Articles";
    protected readonly string PublicationStateCaption = "Publication State";
    protected readonly string RevisionNumberCaption = "Revision Number";
    protected readonly string StateCaption = "State";

    protected UpdateEntity Update { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Update = new UpdateEntity(this.NetObjectId);
        this.Update.RefreshData();

        lblNoDataToDisplay.InnerHtml = ResourceHelper.GetInfoMessageHtml(NoDataToDisplayCaption);

        divDetail.Visible = this.Update != null && !String.IsNullOrEmpty(this.Update.Title);
        lblNoDataToDisplay.Visible = !divDetail.Visible;
    }
}
