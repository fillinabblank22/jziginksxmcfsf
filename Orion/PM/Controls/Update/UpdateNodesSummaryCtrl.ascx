﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UpdateNodesSummaryCtrl.ascx.cs"
    Inherits="Orion_PM_Controls_Update_UpdateNodesSummaryCtrl" %>

<%@ Register Assembly="SolarWinds.PM.Web" Namespace="SolarWinds.PM.Web.Charting" TagPrefix="charts" %>

<div runat="server" ID="lblNoDataToDisplay"></div> 
<div runat="server" id="dataDiv">
    <table>
        <tr>
            <td style="width: 1%; border-width: 0px; vertical-align: top;"><charts:ScmChartImage runat="server" id="imgGraph" /></td>
            <td style="width: 1%; border-width: 0px; vertical-align: top; padding-top: 40px;">
                    <div style="font-weight:bold; font-size: 13px; padding-bottom: 10px;"><span style="white-space: nowrap;"><asp:Label runat="server" ID="lblNodeCountCaption" />:&nbsp<asp:Label runat="server" ID="lblNodeCount" /></span></div>
                    <asp:Repeater runat="server" ID="resourceTable">
                        <HeaderTemplate>
                                <table cellpadding = "0" cellspacing = "0">
                        </HeaderTemplate>
                        <ItemTemplate>
	                                <tr>
	                                    <td class="Property" style="width: 1%; color: white; font-weight: bold; text-align: center; font-size: 12px; background-color: <%# HttpUtility.HtmlEncode(Eval("Color").ToString())%>;">&nbsp;<%# HttpUtility.HtmlEncode(Eval("Value").ToString())%>&nbsp;</td>
	                                    <td class="Property" style="padding-left: 10px;"><span style="white-space: nowrap;"><a href="<%# HttpUtility.HtmlEncode(Eval("Link").ToString())%>"><%# HttpUtility.HtmlEncode(Eval("Name").ToString())%></a></span></td>
                                    </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                                </table>
                        </FooterTemplate>
                    </asp:Repeater>
            </td>
            <td style="border-width: 0px;">&nbsp;</td>
        </tr>
    </table>
</div>