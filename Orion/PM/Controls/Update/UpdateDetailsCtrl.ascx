<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UpdateDetailsCtrl.ascx.cs"
    Inherits="Orion_PM_Controls_Update_UpdateDetailsCtrl" %>

<div runat="server" ID="lblNoDataToDisplay"></div> 
<div runat="server" id="divDetail">
    <div style="font-weight: bold; font-size: 11px;"><%= DefaultSanitizer.SanitizeHtml(this.DescriptionCaption) %></div>
    <div style="padding-left: 2px; font-size: 11px;"><%= DefaultSanitizer.SanitizeHtml(this.Update.Description) %></div>
    <table cellpadding="0" cellspacing="0" style="margin-top: 15px;">
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.ProductCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.Update.ProductTitles) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.CompanyCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.Update.CompanyTitles) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.ProductFamilyCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.Update.ProductFamilyTitles) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.CreationDateCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.Update.CreationDate) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.ArivalDateCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.Update.ArrivalDate) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.SeverityCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.Update.MsrcSeverity) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.IsApprovedCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.Update.IsApproved) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.HasStaleUpdateApprovalsCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.Update.HasStaleUpdateApprovals) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.HasSupersededUpdatesCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.Update.HasSupersededUpdates) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.IsDeclinedCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.Update.IsDeclined) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.IsWsusInfrastructureUpdateCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.Update.IsWsusInfrastructureUpdate) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.KnowledgebaseArticlesCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.Update.KnowledgebaseArticles) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.PublicationStateCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.Update.PublicationState) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.RevisionNumberCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.Update.RevisionNumber) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.StateCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.Update.State) %></td>
        </tr>
    </table>
</div>