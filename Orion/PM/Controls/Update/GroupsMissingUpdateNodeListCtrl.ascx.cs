﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI.Localizer;
using SolarWinds.PM.Web.Resources;
using SolarWinds.PM.Web.AjaxTree;
using SolarWinds.PM.Web.Controls;
using SolarWinds.PM.Web.Controls.AssetTree;
using SolarWinds.PM.Web.Helper;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.PM.Web.NetObjects;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using SolarWinds.Orion.Web.UI;
using System.Data;
using SolarWinds.PM.Web.Model;
using SolarWinds.PM.Common.Enums;
using SolarWinds.PM.Web.Resources.Summary;
using SolarWinds.PM.Web.Resources.Update;

public partial class Orion_PM_Controls_Update_GroupsMissingUpdateNodeListCtrl : ScmResourceBaseAsync
{
    private const Int32 MAX_COUNT_OF_RECORDS = 100;

    public String NetObjectId { get; set; }
    public String GroupId { get; set; }
    public Boolean JustApprovedUpdates { get; set; }
    public String UpdateInstallationState { get; set; }

    private const string NoDataToDisplayCaption = "No data to display";

    protected readonly string NodeNameCaption = "Node name";
    protected readonly string WsusServerCaption = "Wsus server";

    protected String MoreRecordsIsHiddenCaption { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        lblNoDataToDisplay.InnerHtml = ResourceHelper.GetInfoMessageHtml(NoDataToDisplayCaption);

        try
        {
            var data = new GroupsMissingUpdate().GetNodes(this.NetObjectId, this.GroupId, this.JustApprovedUpdates, this.UpdateInstallationState);

            var count = data.Rows.Count;
            if (count > MAX_COUNT_OF_RECORDS)
            {
                var table = new DataTable();
                foreach (DataColumn item in data.Columns)
                    table.Columns.Add(new DataColumn(item.ColumnName, item.DataType));

                for (Int32 i = 0; i < MAX_COUNT_OF_RECORDS; i++)
                    table.Rows.Add(data.Rows[i].ItemArray);

                data = table;
                this.MoreRecordsIsHiddenCaption = String.Format("Other {0} items are not displayed", count - MAX_COUNT_OF_RECORDS);
            }

            this.resourceTable.DataSource = data;
            this.resourceTable.DataBind();

            dataDiv.Visible = count != 0;
            lblNoDataToDisplay.Visible = !dataDiv.Visible;
        }
        catch (Exception)
        {
            dataDiv.Visible = false;
            lblNoDataToDisplay.Visible = !dataDiv.Visible;
        }
    }
}