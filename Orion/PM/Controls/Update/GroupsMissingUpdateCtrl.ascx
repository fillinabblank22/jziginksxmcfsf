<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GroupsMissingUpdateCtrl.ascx.cs"
    Inherits="Orion_PM_Controls_Update_GroupsMissingUpdateCtrl" %>
<%@ Register Assembly="SolarWinds.PM.Web" Namespace="SolarWinds.PM.Web.Charting" TagPrefix="charts" %>

<div runat="server" ID="lblNoDataToDisplay"></div> 
<div runat="server" id="dataDiv">
    <asp:Repeater runat="server" ID="resourceTable">
        <HeaderTemplate>
            <table cellpadding = "0" cellspacing = "0">
            <tr>
                <td class="ReportHeader" colspan="2"><span style="white-space: nowrap;"><%= DefaultSanitizer.SanitizeHtml(this.GroupNameCaption.ToUpperInvariant()) %></span></td>
                <td class="ReportHeader"><span style="white-space: nowrap;"><%= DefaultSanitizer.SanitizeHtml(this.UpdatesMissingCaption.ToUpperInvariant()) %></span></td>
            </tr>
        </HeaderTemplate>
        <ItemTemplate>
	        <tr>
                <script type="text/javascript" language="javascript">
                    function PlusClick_<%# DefaultSanitizer.SanitizeHtml(this.GetId(Eval("ID"))) %>() {
                        Orion_PM_Resources_Update_GroupsMissingUpdateToggle('<%# DefaultSanitizer.SanitizeHtml(this.GetId(Eval("ID"))) %>', <%# DefaultSanitizer.SanitizeHtml(this.GetDataToLoad(Eval("ID").ToString())) %>); 
                        Orion_PM_Resources_Update_GroupsMissingUpdateChangeExpander('img_<%# DefaultSanitizer.SanitizeHtml(this.GetId(Eval("ID"))) %>');
                    }
                </script>
	            <td class="<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "Class")) %>" style="width: 1%; padding-left: 3px; padding-right: 3px; border-width: 0px;"><a style="cursor: pointer; margin-right:5px; margin-top: 3px; <%# DefaultSanitizer.SanitizeHtml(this.PlusDisplayStyle(Eval("Value"))) %>" href="javascript: PlusClick_<%# DefaultSanitizer.SanitizeHtml(this.GetId(Eval("ID"))) %>();"><img id="img_<%# DefaultSanitizer.SanitizeHtml(this.GetId(Eval("ID"))) %>" alt="[+]" src="/Orion/images/Button.Expand.gif" /></a></td>
	            <td class="<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "Class")) %>" style="padding-left: 3px; padding-right: 3px; border-width: 0px;"><img alt="icon" src="/Orion/PM/images/Group_03.png" />&nbsp;<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "Title")) %></td>
	            <td class="<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "Class")) %>" style="padding-left: 3px; padding-right: 3px; border-width: 0px; <%# DefaultSanitizer.SanitizeHtml(this.GetColorThreshold(Convert.ToInt32(Eval("Value")))) %> <%# DefaultSanitizer.SanitizeHtml(this.GetWeightThreshold(Convert.ToInt32(Eval("Value")))) %>"><%# HttpUtility.HtmlEncode(Eval("Value").ToString()) %></td>
            </tr>
            <tr id="Sel_<%# DefaultSanitizer.SanitizeHtml(this.GetId(Eval("ID"))) %>" style="display: none; border-width: 0px;">
                <td style="border-width: 0px;">&nbsp;</td>
                <td style="border-width: 0px;" colspan="3">
                    <span id="loader_<%# DefaultSanitizer.SanitizeHtml(this.GetId(Eval("ID"))) %>" style="margin-bottom: 10px; margin-top:10px;"><img src="/Orion/images/AJAX-Loader.gif" alt="Loading..." /><%= DefaultSanitizer.SanitizeHtml(this.LoadingCaption) %></span>
                    <div id="content_<%# DefaultSanitizer.SanitizeHtml(this.GetId(Eval("ID"))) %>" style="margin-bottom: 10px; margin-top:10px;" />
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</div>