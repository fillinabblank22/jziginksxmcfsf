﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.PM.Web.Charting;
using SolarWinds.PM.Web.DAL;
using System.Data;
using SolarWinds.PM.Web.Resources;
using SolarWinds.PM.Web.NetObjects;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using SolarWinds.PM.Web.Resources.Update;
using SolarWinds.PM.Web.Helper;

public partial class Orion_PM_Controls_Update_UpdateNodesSummaryCtrl : ScmResourceBaseAsync
{
    public String NetObjectId { get; set; }
    public String DefaultTitle { get; set; }
    public Boolean JustApprovedUpdates { get; set; }
    public String LegendHelpFragment { get; set; }

    private readonly string NoUpdatesToDisplayCaption = "No nodes to display";

    private readonly string InstalledCaption = "Installed";
    private readonly string OtherCaption = "Other";
    private readonly string LegendTitleCaption = "Node Count";

    private readonly string NodeNameLegendFieldCaption = "Node name";
    private readonly string WsusServerLegendFieldCaption = "Wsus server";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            lblNoDataToDisplay.InnerHtml = ResourceHelper.GetInfoMessageHtml(NoUpdatesToDisplayCaption);

            var detailsData = new WsusNodesSummary.DetailsData()
            {
                Installed = InstalledCaption,
                Unknown = OtherCaption,
                JustApprovedUpdates = this.JustApprovedUpdates,
            };
            var legendData = new WsusNodesBase.LegendData()
            {
                Title = this.DefaultTitle,
                HelpFragment = this.LegendHelpFragment,
                NodeName = NodeNameLegendFieldCaption,
                WsusServer = WsusServerLegendFieldCaption,
            };

            var table = new WsusNodesSummary().GetData(this.NetObjectId, detailsData, legendData);

            this.resourceTable.DataSource = table;
            this.resourceTable.DataBind();


            var chartInfo = SolarWinds.Orion.Web.Charting.ChartInfoFactory.Create("UpdateNodesSummary") as UpdateNodesSummaryChartInfo;
            chartInfo.Height = chartInfo.Width = 200;

            chartInfo.InstalledCount = Convert.ToInt32(table.Rows[0]["Value"]); ;
            chartInfo.OtherCount = Convert.ToInt32(table.Rows[1]["Value"]); ;

            imgGraph.ChartInfo = chartInfo;

            var count = chartInfo.InstalledCount + chartInfo.OtherCount;

            this.lblNodeCountCaption.Text = LegendTitleCaption;
            this.lblNodeCount.Text = count.ToString();

            dataDiv.Visible = count != 0;
            lblNoDataToDisplay.Visible = count == 0;
        }
        catch (Exception)
        {
            return;
        }
    }
}
