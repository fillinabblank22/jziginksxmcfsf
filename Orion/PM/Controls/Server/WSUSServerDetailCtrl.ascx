<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WSUSServerDetailCtrl.ascx.cs" Inherits="Orion_PM_Controls_Server_WSUSServerDetailCtrl" %>

<div runat="server" ID="lblNoDataToDisplay"></div> 
<div runat="server" id="divDetail">
    <table cellpadding="0" cellspacing="0" style="margin-top: 15px;">
<%if (this.Server.OrionNodeId == default(Int32?)) { %>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.StatusCaption) %></td>
            <td style="text-align: center;">&nbsp;<img alt="Icon" src="/Orion/images/StatusIcons/Unknown.gif" /></td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.UnmanagedNodeCaption) %></td>
        </tr>
<%} else { %>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.StatusCaption) %></td>
            <td style="text-align: center;">&nbsp;<img alt="Icon" src="<%= DefaultSanitizer.SanitizeHtml(String.Format("/Orion/images/StatusIcons/{0}", this.Server.OrionNodeStatusIcon)) %>" /></td>
            <td>&nbsp;<a href="<%= DefaultSanitizer.SanitizeHtml(String.Format("/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}", this.Server.OrionNodeId)) %>"><%= DefaultSanitizer.SanitizeHtml(this.GetStatusCaption) %></a></td>
        </tr>
<%} %>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.PortCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.DisplayValue(this.Server.Port)) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.UseSSLCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.DisplayValue(this.Server.UseSSL)) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.OSProductTypeCaption) %></td>
            <td style="text-align: center;">&nbsp;<img alt="Icon" src="<%= DefaultSanitizer.SanitizeHtml(this.Server.OSProductType.ToLower().Contains("server") ? "/Orion/PM/images/device_16x16.gif" : "/NetPerfMon/images/Endpoints/endpoint.gif") %>" /></td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.DisplayValue(this.Server.OSProductType)) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.DomainCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.DisplayValue(this.Server.DomainOrWorkgroup)) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.NetBiosNameCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.DisplayValue(this.Server.NetBIOSName)) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.InstalledStateCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.DisplayValue(this.Server.InstalledState)) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.LastInventoryAttemptedTimeCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(SolarWinds.PM.Web.Helper.ResourceHelper.CompareToMinDBValue(this.Server.LastInventoryAttemptedTime) ? "never" : this.DisplayValue(this.Server.LastInventoryAttemptedTime)) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.LastContactTimeCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(SolarWinds.PM.Web.Helper.ResourceHelper.CompareToMinDBValue(this.Server.LastContactTime) ? "never" : this.DisplayValue(this.Server.LastContactTime)) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.OSDescriptionCaption) %></td>
            <td style="text-align: center;">&nbsp;<img alt="Icon" src="<%= DefaultSanitizer.SanitizeHtml(this.Server.OsType.ToLower().Contains("microsoft") || this.Server.OsType.ToLower().Contains("windows") ? "/NetPerfMon/images/Vendors/311.gif" : "/NetPerfMon/images/Vendors/Unknown.gif") %>" /></td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(String.IsNullOrEmpty(this.Server.OSDescription) ? this.DisplayValue(this.Server.OsType) : this.DisplayValue(this.Server.OSDescription)) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.OSVendorCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.DisplayValue(this.Server.OsVendor)) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.OSTypeCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.DisplayValue(this.Server.OsType)) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.VersionCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.DisplayValue(this.Server.OsVersion)) %></td>
        </tr>
    </table>
</div>