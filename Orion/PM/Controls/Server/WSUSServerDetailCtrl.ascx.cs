﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.PM.Web.Resources;
using SolarWinds.PM.Web.NetObjects;
using SolarWinds.PM.Web.Charting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Common;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using SolarWinds.Logging;
using SolarWinds.PM.Web.Model;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.PM.Web.Helper;

public partial class Orion_PM_Controls_Server_WSUSServerDetailCtrl : ScmResourceBaseAsync
{
    public String NetObjectId { get; set; }

    private readonly string NoDataToDisplayCaption = "No data to display";

    protected readonly string DomainCaption = "Domain";
    protected readonly string InstalledStateCaption = "WMI Providers Installed State";
    protected readonly string IpAddressCaption = "IP Address";
    protected readonly string LastContactTimeCaption = "Last Contact Time";
    protected readonly string LastInventoryAttemptedTimeCaption = "Last Inventory Attempted Time";
    protected readonly string NetBiosNameCaption = "Net BIOS Name";
    protected readonly string OSDescriptionCaption = "OS Description";
    protected readonly string OSProductTypeCaption = "OS Product Type";
    protected readonly string OSTypeCaption = "OS Type";
    protected readonly string OSVendorCaption = "OS Vendor";
    protected readonly string PortCaption = "Port";
    protected readonly string UseSSLCaption = "Use SSL";
    protected readonly string VersionCaption = "Version";
    protected readonly string StatusCaption = "Node Status";
    protected readonly string UnmanagedNodeCaption = "Node is not managed by Orion";
    protected readonly string UnknownCaption = "Unknown";

    protected WsusServerEntity Server { get; set; }

    protected String GetStatusCaption
    {
        get
        {
            string statusDescription = string.Empty;
            using (NodeChildStatusDAL dal = new NodeChildStatusDAL())
            {
                if (dal.GetChildStatusDescription(this.Server.OrionNodeId.Value, null, true, out statusDescription) == false)
                    statusDescription = this.Server.OrionStatusDescription;
            }

            return statusDescription;
        }
    }

    protected String DisplayValue(object value)
    {
        if (value == null)
            return UnknownCaption;

        return String.IsNullOrEmpty(value.ToString()) ? UnknownCaption : value.ToString();
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Server = new WsusServerEntity(this.NetObjectId);
        this.Server.RefreshData();

        lblNoDataToDisplay.InnerHtml = ResourceHelper.GetInfoMessageHtml(NoDataToDisplayCaption);

        divDetail.Visible = this.Server != null && this.Server.IPAddress != null;
        lblNoDataToDisplay.Visible = !divDetail.Visible;
    }
}