﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;

public partial class Orion_PM_Controls_EditResourceControls_EditLatestPatchesReportResource : BaseResourceEditControl
{
    private const string FILTER_CAPTION = "Get Latest Patches Within X Last Days";
    private const string FILTER_RESOURCE_PROPERTY_KEY = "lastx";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        labelGetWithinLastXDays.Text = FILTER_CAPTION;

        if (Resource.Properties.ContainsKey(FILTER_RESOURCE_PROPERTY_KEY))
        {
            textBoxWithiLastXDays.Text = Resource.Properties[FILTER_RESOURCE_PROPERTY_KEY];
        }
        else
        {
            textBoxWithiLastXDays.Text = "7";
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            properties.Add(FILTER_RESOURCE_PROPERTY_KEY, this.textBoxWithiLastXDays.Text);
            return properties;
        }
    }
}