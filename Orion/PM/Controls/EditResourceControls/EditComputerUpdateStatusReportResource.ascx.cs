﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;
using System.Web.UI.WebControls;

public partial class
    Orion_PM_Controls_EditResourceControls_EditComputerUpdateStatusReportResource : BaseResourceEditControl
{
    private const string FILTER_PATCH_NAME = "Filter Patches";
    private const string FILTER_MACHINE_NAME = "Filter Machines";
    private const string FILTER_WSUS_GROUPS = "Filter WSUS Groups";
    private const string FILTER_APPROVED = "Include Next Updates";
    private const string FILTER_RESOURCE_PATCH_NAME_PROPERTY_KEY = "patch_filter";
    private const string FILTER_RESOURCE_MACHINE_NAME_PROPERTY_KEY = "machine_filter";
    private const string FILTER_RESOURCE_WSUS_GROUPS_PROPERTY_KEY = "wsus_group_filter";

    private const string FILTER_RESOURCE_PATCH_MACHINE_RADIO_PROPERTY_KEY = "patch_machine_radio";
    private const string FILTER_RESOURCE_MACHINE_WSUS_GROUP_RADIO_PROPERTY_KEY = "machine_wsus_group_radio";

    private const string FILTER_RESOURCE_APPROVED_PROPERTY_KEY = "approved_filter";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        labelPatchNameFilter.Text = FILTER_PATCH_NAME;
        labelMachineNameFilter.Text = FILTER_MACHINE_NAME;
        labelWsusGroupFilter.Text = FILTER_WSUS_GROUPS;

        valuePatchNameFilter.Text = Resource.Properties.ContainsKey(FILTER_RESOURCE_PATCH_NAME_PROPERTY_KEY) ? Resource.Properties[FILTER_RESOURCE_PATCH_NAME_PROPERTY_KEY] : String.Empty;
        valueMachineNameFilter.Text = Resource.Properties.ContainsKey(FILTER_RESOURCE_MACHINE_NAME_PROPERTY_KEY) ? Resource.Properties[FILTER_RESOURCE_MACHINE_NAME_PROPERTY_KEY] : String.Empty;
        valueWsusGroupFilter.Text = Resource.Properties.ContainsKey(FILTER_RESOURCE_WSUS_GROUPS_PROPERTY_KEY) ? Resource.Properties[FILTER_RESOURCE_WSUS_GROUPS_PROPERTY_KEY] : String.Empty;

        radioButtonListPatch_Machine.Items.FindByValue(Resource.Properties.ContainsKey(FILTER_RESOURCE_PATCH_MACHINE_RADIO_PROPERTY_KEY) ? Resource.Properties[FILTER_RESOURCE_PATCH_MACHINE_RADIO_PROPERTY_KEY] : "AND").Selected = true;
        radioButtonListMachine_Wsus.Items.FindByValue(Resource.Properties.ContainsKey(FILTER_RESOURCE_MACHINE_WSUS_GROUP_RADIO_PROPERTY_KEY) ? Resource.Properties[FILTER_RESOURCE_MACHINE_WSUS_GROUP_RADIO_PROPERTY_KEY] : "AND").Selected = true;

        listBoxDropDownApproved.Items.AddRange(new ListItem[] { new ListItem("All", "ALL"),
            new ListItem("Approved", "APPROVED"),
            new ListItem("Not Approved", "NOTAPPROVED")
        });
        
        dropDownApproved.Text = FILTER_APPROVED;

        listBoxDropDownApproved.SelectedIndex = 1;
        foreach (ListItem item in listBoxDropDownApproved.Items)
        {
            if (item.Value.Equals(Resource.Properties[FILTER_RESOURCE_APPROVED_PROPERTY_KEY], StringComparison.OrdinalIgnoreCase))
            {
                listBoxDropDownApproved.SelectedValue = Resource.Properties[FILTER_RESOURCE_APPROVED_PROPERTY_KEY];
                break;
            }
        }
    }


    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            properties.Add(FILTER_RESOURCE_PATCH_NAME_PROPERTY_KEY, this.valuePatchNameFilter.Text);
            properties.Add(FILTER_RESOURCE_MACHINE_NAME_PROPERTY_KEY, this.valueMachineNameFilter.Text);
            properties.Add(FILTER_RESOURCE_WSUS_GROUPS_PROPERTY_KEY, this.valueWsusGroupFilter.Text);

            properties.Add(FILTER_RESOURCE_PATCH_MACHINE_RADIO_PROPERTY_KEY, this.radioButtonListPatch_Machine.SelectedValue);
            properties.Add(FILTER_RESOURCE_MACHINE_WSUS_GROUP_RADIO_PROPERTY_KEY, this.radioButtonListMachine_Wsus.SelectedValue);

            properties.Add(FILTER_RESOURCE_APPROVED_PROPERTY_KEY, this.listBoxDropDownApproved.SelectedValue);

            return properties;
        }
    }
}