using System;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.PM.Web.Helpers;
using SolarWinds.PM.Web.Helper;

public partial class Orion_PM_Controls_EditResourceControls_TopXX_Edit : System.Web.UI.Page
{
    private const int MIN_VALUE_OF_MAX_COUNT = 1;
    private const int MAX_VALUE_OF_MAX_COUNT = 10000;
    private const string MAX_COUNT_ERROR_RANGE_MESSAGE = "Value has to be a number in range of: ";

    private const string MAX_COUNT_TITLE_CAPTION = "Maximum Number of Items to Display";
    private const int DEFAULT_MAX_COUNT = 10;
    private const string MAX_RECORDS_RESOURCE_PROPERTY_KEY = "MaxRecords";
    private const string FILTER_RESOURCE_PROPERTY_KEY = "Filter";


    private string _netObjectID;
    private Control _editControl;

    #region Resource property
    private ResourceInfo resource;

    protected ResourceInfo Resource
    {
        get { return this.resource; }
        set { this.resource = value; }
    }
    #endregion

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!String.IsNullOrEmpty(this.Request.QueryString["ResourceID"]))
        {
            int resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
            this.Resource = ResourceManager.GetResourceByID(resourceID);
            this.Title = String.Format(Resources.CoreWebContent.WEBCODE_AK0_88, Resource.Title);
        }

        _netObjectID = string.IsNullOrEmpty(Request.QueryString["NetObject"]) ? string.Empty : Request.QueryString["NetObject"];

        LoadResourceEditControl();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            var isTitle = true;
            var maxValidate = MAX_VALUE_OF_MAX_COUNT;

            if (Request.QueryString["ThwackData"] != null)
            {
                var thwackData = Serializer.Deserialize<Object[]>(Request.QueryString["ThwackData"]);
                isTitle = Convert.ToBoolean(thwackData[0]);
                maxValidate = Convert.ToInt32(thwackData[1]);
            }


            maxCountValidator.ErrorMessage = String.Format("{0}<{1}, {2}>", MAX_COUNT_ERROR_RANGE_MESSAGE, MIN_VALUE_OF_MAX_COUNT, maxValidate);
            maxCountValidator.MinimumValue = MIN_VALUE_OF_MAX_COUNT.ToString();
            maxCountValidator.MaximumValue = maxValidate.ToString();
            maxCountValidator.Type = ValidationDataType.Integer;

            lblTitleHint.Text = Resources.CoreWebContent.WEBDATA_TM0_53;
            lblMaxRecords.Text = MAX_COUNT_TITLE_CAPTION;

            int max;
            if (!Int32.TryParse(this.Resource.Properties["MaxRecords"], out max))
                this.Resource.Properties["MaxRecords"] = "10";
            this.maxCount.Text = this.Resource.Properties["MaxRecords"];

            this.resourceTitleEditor.Visible = isTitle;
            lblTitleHint.Visible = isTitle;
            lblSubTitleHint.Visible = isTitle;

            this.resourceTitleEditor.ResourceTitle = string.IsNullOrEmpty(Resource.Properties["Title"]) ? string.Empty : Resource.Properties["Title"];
            this.resourceTitleEditor.ResourceSubTitle = string.IsNullOrEmpty(Resource.SubTitle) ? string.Empty : Resource.SubTitle;
        }
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        var resTitle = this.resourceTitleEditor.ResourceTitle;
        var resSubTitle = this.resourceTitleEditor.ResourceSubTitle;

        if (_editControl != null)
        {
            foreach (KeyValuePair<string, object> property in (_editControl as BaseResourceEditControl).Properties)
            {
                if (Resource.Properties.ContainsKey(property.Key))
                    Resource.Properties[property.Key] = property.Value.ToString();
                else
                    Resource.Properties.Add(property.Key, property.Value.ToString());
            }

            if (string.IsNullOrEmpty(resTitle) && !string.IsNullOrEmpty((_editControl as BaseResourceEditControl).DefaultResourceTitle))
                resTitle = (_editControl as BaseResourceEditControl).DefaultResourceTitle;
            if (string.IsNullOrEmpty(resSubTitle) && !string.IsNullOrEmpty((_editControl as BaseResourceEditControl).DefaultResourceSubTitle))
                resSubTitle = (_editControl as BaseResourceEditControl).DefaultResourceSubTitle;
        }

        this.Resource.Properties["Title"] = resTitle;
        this.Resource.SubTitle = resSubTitle;
        this.Resource.Properties["MaxRecords"] = this.maxCount.Text;

        // subtitle is not stored into DB when property is changed
        ResourcesDAL.Update(this.Resource);

        string url = string.Format("/Orion/View.aspx?ViewID={0}", Resource.View.ViewID);
        if (!string.IsNullOrEmpty(_netObjectID))
            url = string.Format("{0}&NetObject={1}", url, _netObjectID);

        Response.Redirect(url);
    }

    private void LoadResourceEditControl()
    {
        if (Resource == null)
            return;

        Control control = LoadControl(Resource.File);
        if (control is BaseResourceControl)
        {
            string editControlLocation = (control as BaseResourceControl).EditControlLocation;
            control.Dispose();

            if (!string.IsNullOrEmpty(editControlLocation))
            {
                _editControl = LoadControl(editControlLocation);

                if (_editControl is BaseResourceEditControl)
                {
                    lblSubTitleHint.Visible = (_editControl as BaseResourceEditControl).ShowSubTitleHintMessage;
                    lblSubTitleHint.Text = (_editControl as BaseResourceEditControl).SubTitleHintMessage;
                    resourceTitleEditor.ShowSubTitle = (_editControl as BaseResourceEditControl).ShowSubTitle;
                    (_editControl as BaseResourceEditControl).Resource = Resource;
                    (_editControl as BaseResourceEditControl).NetObjectID = _netObjectID;
                    phResourceEditControl.Controls.Add(_editControl.TemplateControl);
                }
            }
        }
    }
}
