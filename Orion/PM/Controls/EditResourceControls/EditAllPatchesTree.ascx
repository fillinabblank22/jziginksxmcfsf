<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditAllPatchesTree.ascx.cs"
    Inherits="Orion_SCM_Controls_EditResourceControls_EditAllPatchesTree" %>

<script type="text/javascript">
       function ClientValidate(source, arguments) {
           var lbxGroup1 = $('#<%= this.lbxGroup1.ClientID %>');
           var lbxGroup2 = $('#<%= this.lbxGroup2.ClientID %>');

           if (arguments.Value != lbxGroup1[0].value
                && arguments.Value != lbxGroup2[0].value)
               arguments.IsValid = true;
           else
               arguments.IsValid = false; 
       }
</script>
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_342) %>
<p>
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_343) %><br />
    <asp:ListBox runat="server" ID="lbxGroup1" SelectionMode="single" Rows="1">
        <asp:ListItem Text="Severity" Value="MsrcSeverity" />
        <asp:ListItem Text="Update Classification Title" Value="UpdateClassificationTitle" />
        <asp:ListItem Text="Company Titles" Value="CompanyTitles" />
        <asp:ListItem Text="Product Titles" Value="ProductTitles" />
        <asp:ListItem Text="Product Family Titles" Value="ProductFamilyTitles" />
        <asp:ListItem Text="Update Type" Value="UpdateType" />
    </asp:ListBox>
</p>
<p>
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_344) %><br />
    <asp:ListBox runat="server" ID="lbxGroup2" SelectionMode="single" Rows="1">
        <asp:ListItem Text="None" Value="" />
        <asp:ListItem Text="Severity" Value="MsrcSeverity" />
        <asp:ListItem Text="Update Classification Title" Value="UpdateClassificationTitle" />
        <asp:ListItem Text="Company Titles" Value="CompanyTitles" />
        <asp:ListItem Text="Product Titles" Value="ProductTitles" />
        <asp:ListItem Text="Product Family Titles" Value="ProductFamilyTitles" />
        <asp:ListItem Text="Update Type" Value="UpdateType" />
    </asp:ListBox>
    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="lbxGroup2"
        ControlToCompare="lbxGroup1" Type="String" Operator="NotEqual" Display="Dynamic"
        ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_351 %>"></asp:CompareValidator>
</p>
<p>
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_345) %><br />
    <asp:ListBox runat="server" ID="lbxGroup3" SelectionMode="single" Rows="1">
        <asp:ListItem Text="None" Value="" />
        <asp:ListItem Text="Severity" Value="MsrcSeverity" />
        <asp:ListItem Text="Update Classification Title" Value="UpdateClassificationTitle" />
        <asp:ListItem Text="Company Titles" Value="CompanyTitles" />
        <asp:ListItem Text="Product Titles" Value="ProductTitles" />
        <asp:ListItem Text="Product Family Titles" Value="ProductFamilyTitles" />
        <asp:ListItem Text="Update Type" Value="UpdateType" />
    </asp:ListBox>
    <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="lbxGroup3" 
        ClientValidationFunction="ClientValidate" Display="Dynamic" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_351 %>"></asp:CustomValidator>
</p>

<p>
    <asp:CheckBox runat="server" ID="RememberExpandedState" Text="Remember Expanded State" />
</p>
