﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditGroupsMissingUpdate.ascx.cs" Inherits="Orion_SCM_Controls_EditResourceControls_EditGroupsMissingUpdate" %>

    <b><asp:Literal runat="server" ID="lblFilterUpdates" /></b><br />
    <asp:CheckBox runat="server" ID="chkJustApprovedUpdates" />

    <br /><br />

    <b><asp:Literal runat="server" ID="lblThresholds" /></b><br />
    <asp:Literal runat="server" ID="lblWarningThreshold" /><br />
    <asp:TextBox runat="server" ID="txtWarning" />
    <asp:RangeValidator ID="vldWarning" runat="server" ControlToValidate="txtWarning" EnableClientScript="true"></asp:RangeValidator><br />
   
    <asp:Literal runat="server" ID="lblCriticalThreshold" /><br />
    <asp:TextBox runat="server" ID="txtCritical" />
    <asp:RangeValidator ID="vldCritical" runat="server" ControlToValidate="txtCritical" EnableClientScript="true"></asp:RangeValidator><br />

    <br /><br />

<b><asp:Literal runat="server" ID="lblFilter" /></b><br />
    <asp:ListBox runat="server" ID="lbFilter" SelectionMode="single" Rows="1"></asp:ListBox>


