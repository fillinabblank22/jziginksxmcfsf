﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.PM.Web.Helper;

public partial class Orion_SCM_Controls_EditResourceControls_EditLastXXPatches : BaseResourceEditControl
{
    private const string GROUPING_TITLE_CAPTION = "Grouping Updates";
    private const string UPDATE_FILTER_CAPTION = "Update Filter";
    private const string EXCLUDE_NOTAPPLICABLE_UPDATES_CAPTION = "Exclude Not Applicable Updates";

    private const int MIN_VALUE_OF_MAX_COUNT = 1;
    private const int MAX_VALUE_OF_MAX_COUNT = 10000;
    private const string MAX_COUNT_ERROR_RANGE_MESSAGE = "Value has to be a number in range of: ";

    private const string WARNING_THRESHOLDS_CAPTION = "Warning:";
    private const string CRITICAL_THRESHOLDS_CAPTION = "Critical:";
    private const string THRESHOLDS_CAPTION = "Threshold Settings";

    private const string CRITICAL_THRESHOLDS_RESOURCE_PROPERTY_KEY = "CriticalThreshold";
    private const Int32 CRITICAL_THRESHOLDS_DEFAULT = 15;

    private const string WARNING_THRESHOLDS_RESOURCE_PROPERTY_KEY = "WarningThreshold";
    private const Int32 WARNING_THRESHOLDS_DEFAULT = 8;

    private const string EXCLUDE_UPDATES_PROPERTY_KEY = "ExcludeNotApplicableUpdates";
    private const string EXCLUDE_UPDATES_DEFAULT = "false";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!this.IsPostBack)
        {
            vldCritical.ErrorMessage = String.Format("{0}<{1}, {2}>", MAX_COUNT_ERROR_RANGE_MESSAGE, MIN_VALUE_OF_MAX_COUNT, MAX_VALUE_OF_MAX_COUNT);
            vldCritical.MinimumValue = MIN_VALUE_OF_MAX_COUNT.ToString();
            vldCritical.MaximumValue = MAX_VALUE_OF_MAX_COUNT.ToString();
            vldCritical.Type = ValidationDataType.Integer;

            vldWarning.ErrorMessage = String.Format("{0}<{1}, {2}>", MAX_COUNT_ERROR_RANGE_MESSAGE, MIN_VALUE_OF_MAX_COUNT, MAX_VALUE_OF_MAX_COUNT);
            vldWarning.MinimumValue = MIN_VALUE_OF_MAX_COUNT.ToString();
            vldWarning.MaximumValue = MAX_VALUE_OF_MAX_COUNT.ToString();
            vldWarning.Type = ValidationDataType.Integer;


            lblThresholds.Text = THRESHOLDS_CAPTION;
            lblCriticalThreshold.Text = CRITICAL_THRESHOLDS_CAPTION;
            lblWarningThreshold.Text = WARNING_THRESHOLDS_CAPTION;
            lblFilterUpdates.Text = UPDATE_FILTER_CAPTION;

            chkExcludeUpdates.Text = EXCLUDE_NOTAPPLICABLE_UPDATES_CAPTION;

            string excludeUpdates = Resource.Properties[EXCLUDE_UPDATES_PROPERTY_KEY] ?? EXCLUDE_UPDATES_DEFAULT;
            chkExcludeUpdates.Checked = Boolean.Parse(excludeUpdates);

            txtCritical.Text = Resource.Properties[CRITICAL_THRESHOLDS_RESOURCE_PROPERTY_KEY] ?? CRITICAL_THRESHOLDS_DEFAULT.ToString();
            txtWarning.Text = Resource.Properties[WARNING_THRESHOLDS_RESOURCE_PROPERTY_KEY] ?? WARNING_THRESHOLDS_DEFAULT.ToString();

            this.lblGrouping.Text = GROUPING_TITLE_CAPTION;

            lbxGroup1.SelectedValue = Resource.Properties[ResourceHelper.GROUPING_RP_KEY_LEVEL1];
        }
    }

    private int ValidGroups
    {
        get
        {
            if (string.IsNullOrEmpty(lbxGroup1.SelectedValue))
                return 0;

            return 1;
        }
    }

    private string BuildSubTitle()
    {
        switch (this.ValidGroups)
        {
            case 0:
                return Resources.CoreWebContent.WEBCODE_AK0_100;
            case 1:
                return string.Format(Resources.CoreWebContent.WEBCODE_AK0_101, lbxGroup1.SelectedItem.Text);
        }

        return string.Empty;
    }
    
    public override string DefaultResourceSubTitle
    {
        get { return BuildSubTitle(); }
    }

    public override bool ShowSubTitleHintMessage
    {
        get
        {
            return true;
        }
    }

    public override string SubTitleHintMessage
    {
        get
        {
            return Resources.CoreWebContent.WEBCODE_AK0_104;
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            Int32 critical;
            if (!Int32.TryParse(txtCritical.Text, out critical))
                critical = CRITICAL_THRESHOLDS_DEFAULT;

            Int32 warning;
            if (!Int32.TryParse(txtWarning.Text, out warning))
                warning = WARNING_THRESHOLDS_DEFAULT;

            properties.Add(EXCLUDE_UPDATES_PROPERTY_KEY, chkExcludeUpdates.Checked.ToString());
            properties.Add(CRITICAL_THRESHOLDS_RESOURCE_PROPERTY_KEY, critical);
            properties.Add(WARNING_THRESHOLDS_RESOURCE_PROPERTY_KEY, warning);
            properties.Add(ResourceHelper.GROUPING_RP_KEY_LEVEL1, lbxGroup1.SelectedValue);

            return properties;
        }
    }
}