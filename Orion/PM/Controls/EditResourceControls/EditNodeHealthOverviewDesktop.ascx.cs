﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web;
using System.Text;

public partial class Orion_SCM_Controls_EditResourceControls_EditNodeHealthOverviewDesktop : BaseResourceEditControl
{
    private const string JUST_APPROVED_UPDATES_CAPTION = "Show just approved updates";
    private const string JUST_APPROVED_UPDATES_PROPERTY_KEY = "JustApprovedUpdates";
    private const string FILTER_UPDATES_TITLE_CAPTION = "Filter Update Approval Action";
    private const string SUB_TITLE_HINT_CAPTION = "Leave the subtitle field blank to generated it automatically.";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!this.IsPostBack)
        {
            chkJustApprovedUpdates.Text = JUST_APPROVED_UPDATES_CAPTION;

            string justApprovedUpdates = Resource.Properties[JUST_APPROVED_UPDATES_PROPERTY_KEY] ?? "true";
            chkJustApprovedUpdates.Checked = Boolean.Parse(justApprovedUpdates);
        }
    }
    private string BuildSubTitle()
    {
        var result = new StringBuilder();
        var subTitleItems = new List<String>();

        if (chkJustApprovedUpdates.Checked)
            result.Append(JUST_APPROVED_UPDATES_CAPTION);

        return result.ToString();
    }

    public override string DefaultResourceSubTitle
    {
        get { return BuildSubTitle(); }
    }

    public override bool ShowSubTitleHintMessage
    {
        get
        {
            return true;
        }
    }

    public override string SubTitleHintMessage
    {
        get
        {
            return SUB_TITLE_HINT_CAPTION;
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            properties.Add(JUST_APPROVED_UPDATES_PROPERTY_KEY, chkJustApprovedUpdates.Checked.ToString());
            return properties;
        }
    }
}
