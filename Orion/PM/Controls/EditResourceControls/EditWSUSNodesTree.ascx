<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditWSUSNodesTree.ascx.cs" Inherits="Orion_SCM_Controls_EditResourceControls_EditWSUSNodesTree" %>

<script type="text/javascript">
    function ClientValidate(source, arguments) {
        var lbxGroup1 = $('#<%= this.lbxGroup1.ClientID %>');
        var lbxGroup2 = $('#<%= this.lbxGroup2.ClientID %>');

        if (arguments.Value != lbxGroup1[0].value
                && arguments.Value != lbxGroup2[0].value)
            arguments.IsValid = true;
        else
            arguments.IsValid = false;
    }
</script>
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_342) %>
<p>
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_343) %><br />
    <asp:ListBox runat="server" ID="lbxGroup1" SelectionMode="single" Rows="1">
        <asp:ListItem Text="None" Value="" />
        <asp:ListItem Text="Wsus Server" Value="s.Servername" />
        <asp:ListItem Text="Role" Value="c.computerrole" />
        <asp:ListItem Text="Manufacturer" Value="c.make" />
        <asp:ListItem Text="Model" Value="c.model" />
        <asp:ListItem Text="OS Architecture" Value="c.osarchitecture" />
        <asp:ListItem Text="OS Description" Value="c.OSDescription" />
    </asp:ListBox>
</p>
<p>
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_344) %><br />
    <asp:ListBox runat="server" ID="lbxGroup2" SelectionMode="single" Rows="1">
        <asp:ListItem Text="None" Value="" />
        <asp:ListItem Text="Wsus Server" Value="s.Servername" />
        <asp:ListItem Text="Role" Value="c.computerrole" />
        <asp:ListItem Text="Manufacturer" Value="c.make" />
        <asp:ListItem Text="Model" Value="c.model" />
        <asp:ListItem Text="OS Architecture" Value="c.osarchitecture" />
        <asp:ListItem Text="OS Description" Value="c.OSDescription" />
    </asp:ListBox>
    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="lbxGroup2"
        ControlToCompare="lbxGroup1" Type="String" Operator="NotEqual" Display="Dynamic"
        ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_351 %>"></asp:CompareValidator>
</p>
<p>
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_345) %><br />
    <asp:ListBox runat="server" ID="lbxGroup3" SelectionMode="single" Rows="1">
        <asp:ListItem Text="None" Value="" />
        <asp:ListItem Text="Wsus Server" Value="s.Servername" />
        <asp:ListItem Text="Role" Value="c.computerrole" />
        <asp:ListItem Text="Manufacturer" Value="c.make" />
        <asp:ListItem Text="Model" Value="c.model" />
        <asp:ListItem Text="OS Architecture" Value="c.osarchitecture" />
        <asp:ListItem Text="OS Description" Value="c.OSDescription" />
    </asp:ListBox>
    <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="lbxGroup3" 
        ClientValidationFunction="ClientValidate" Display="Dynamic" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_351 %>"></asp:CustomValidator>
</p>

<p>
    <asp:CheckBox runat="server" ID="RememberExpandedState" Text="Remember Expanded State" />
</p>
