﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.PM.Web.AjaxTree;

public partial class Orion_SCM_Controls_EditResourceControls_EditWSUSNodesTreeByTargetGroup : BaseResourceEditControl
{
    private const string EXPANDED_STATE_TITLE_CAPTION = "Expanded State";


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        bool allowUnsortableProperties;
        bool.TryParse(Request.QueryString["AllowUnsortableProperties"] ?? "false", out allowUnsortableProperties);

        bool hideRememberExpanded;
        bool.TryParse(Request.QueryString["HideRememberExpanded"] ?? "false", out hideRememberExpanded);

        if (!this.IsPostBack)
        {
            lblFilter.Text = EXPANDED_STATE_TITLE_CAPTION;

            RememberExpandedState.Visible = !hideRememberExpanded;
            string rememberCollapseState = Resource.Properties["RememberExpandedState"] ?? "true";
            RememberExpandedState.Checked = Boolean.Parse(rememberCollapseState);
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            properties.Add("RememberExpandedState", RememberExpandedState.Checked.ToString());

            if (RememberExpandedState.Checked == false)
            {
                try
                {
                    string rememberCollapseState = Resource.Properties["RememberExpandedState"] ?? "true";
                    var oldValue = bool.Parse(rememberCollapseState);
                    if (oldValue != RememberExpandedState.Checked)
                    {
                        var expandedStateProviderIdentifier = String.Format("PM_WSUS_NODES_TREE_BY_TARGET_GROUPS_STATE_-{0}-{1}-{2}",
                            this.Resource.ID,
                            this.Resource.View.ViewID,
                            Request.QueryString["NetObject"] ?? String.Empty); //identifies this instance in session scope

                        if (SessionScopeTreeExpandedStateManager.ContainsProvider(expandedStateProviderIdentifier))
                            SessionScopeTreeExpandedStateManager.ClearExpandedStateManager(expandedStateProviderIdentifier);
                    }
                }
                catch (Exception)
                { }
            }
            // We changed stuff so clear out the expanded tree node information

            return properties;
        }
    }
}
