﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditComputerUpdateStatusReportResource.ascx.cs" Inherits="Orion_PM_Controls_EditResourceControls_EditComputerUpdateStatusReportResource" %>

<style>
    .filter-text-box { width: 208px }
    .filter-radio-list { margin-top: 10px;}
</style>

<p>
    <b><asp:Literal runat="server" ID="labelPatchNameFilter" /></b><br />
    <asp:TextBox runat="server" TextMode="multiline" CssClass="filter-text-box" ID="valuePatchNameFilter"></asp:TextBox>
</p>

<asp:RadioButtonList ID="radioButtonListPatch_Machine" CssClass="filter-radio-list" runat="server">
    <asp:ListItem Text="AND" Value="AND" Selected="True"/>
    <asp:ListItem Text="OR" Value="OR" />
</asp:RadioButtonList>

<p>
    <b><asp:Literal runat="server" ID="labelMachineNameFilter" /></b><br />
    <asp:TextBox runat="server" TextMode="multiline" CssClass="filter-text-box" ID="valueMachineNameFilter"></asp:TextBox>
</p>

<asp:RadioButtonList ID="radioButtonListMachine_Wsus" CssClass="filter-radio-list" runat="server">
    <asp:ListItem Text="AND" Value="AND" Selected="True"/>
    <asp:ListItem Text="OR" Value="OR" />
</asp:RadioButtonList>

<p>
    <b><asp:Literal runat="server" ID="labelWsusGroupFilter" /></b><br />
    <asp:TextBox runat="server" TextMode="multiline" CssClass="filter-text-box" ID="valueWsusGroupFilter"></asp:TextBox>
</p>

<p>
    <b><asp:Literal runat="server" ID="dropDownApproved" /></b><br />
    <asp:ListBox runat="server" ID="listBoxDropDownApproved" SelectionMode="single" Rows="1"></asp:ListBox>
</p>