﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.PM.Web.DAL;
using SolarWinds.PM.Web.Helper;

public partial class Orion_PM_Controls_EditResourceControls_EditInstallationStatusReportResource : BaseResourceEditControl
{
    private const string FILTER_CAPTION = "Filter of Latest Patches";
    private const string FILTER_RESOURCE_PROPERTY_KEY = "Filter";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!this.IsPostBack)
        {
            lbFilter.Items.Add(new ListItem("None", ""));
            var list = new LastXXTasksDAL().GetCurrentStates();
            foreach (var item in list)
            {
                lbFilter.Items.Add(new ListItem(ResourceHelper.GetSpacedTextFromUnspaces(item), item));
            }

            lblFilter.Text = FILTER_CAPTION;

            lbFilter.SelectedIndex = 0;
            foreach (ListItem item in lbFilter.Items)
            {
                if (item.Value.Equals(Resource.Properties[FILTER_RESOURCE_PROPERTY_KEY], StringComparison.OrdinalIgnoreCase))
                {
                    lbFilter.SelectedValue = Resource.Properties[FILTER_RESOURCE_PROPERTY_KEY];
                    break;
                }
            }
        }
    }


    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            properties.Add("FILTER_RESOURCE_PROPERTY_KEY", this.lbFilter.SelectedValue);

            return properties;
        }
    }
}