<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true" CodeFile="EditTopXX.aspx.cs" Inherits="Orion_PM_Controls_EditResourceControls_TopXX_Edit" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBCODE_AK0_88, CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Resource.Title, Request["netobject"])))) %></h1>
       
        <orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="true" ShowSubTitleHintMessage="false" />
        <asp:Literal runat="server" ID="lblTitleHint" /><br />
        <asp:Literal runat="server" ID="lblSubTitleHint" />
        <br /><br />
        <b><asp:Literal runat="server" ID="lblMaxRecords" /></b><br />
        <asp:TextBox runat="server" ID="maxCount" Columns="5"/>
        <asp:RangeValidator ID="maxCountValidator" runat="server" ControlToValidate="maxCount" EnableClientScript="true"></asp:RangeValidator>
        <br /><br />

        <asp:PlaceHolder runat="server" ID="phResourceEditControl"></asp:PlaceHolder>

        <br />
        
        <div class="sw-btn-bar">
            <orion:LocalizableButton runat="server" ID="btnSubmit" OnClick="SubmitClick" LocalizedText="Submit" DisplayType="Primary" />            
        </div>
</asp:Content>
