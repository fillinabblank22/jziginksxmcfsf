﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.PM.Web.AjaxTree;

public partial class Orion_SCM_Controls_EditResourceControls_EditWSUSServersTree : BaseResourceEditControl
{
    private readonly string GROUP_CAPTION = "Select up to two levels of grouping.";
    private readonly string GROUPING_NODES_CAPTION = "Grouping Nodes";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        bool allowUnsortableProperties;
        bool.TryParse(Request.QueryString["AllowUnsortableProperties"] ?? "false", out allowUnsortableProperties);

        bool hideRememberExpanded;
        bool.TryParse(Request.QueryString["HideRememberExpanded"] ?? "false", out hideRememberExpanded);

        if (!this.IsPostBack)
        {
            lblGroupCaption.Text = GROUP_CAPTION;
            lblGroupNodesCaption.Text = GROUPING_NODES_CAPTION;

            lbxGroup1.SelectedValue = Resource.Properties["Grouping1"];

            string grouping = Resource.Properties["Grouping2"];

            if (grouping != null && !grouping.Equals(lbxGroup1.SelectedValue, StringComparison.OrdinalIgnoreCase))
                lbxGroup2.SelectedValue = grouping;
            else
                lbxGroup2.SelectedValue = string.Empty;

            RememberExpandedState.Visible = !hideRememberExpanded;
            string rememberCollapseState = Resource.Properties["RememberExpandedState"] ?? "true";
            RememberExpandedState.Checked = Boolean.Parse(rememberCollapseState);
        }
    }

    private int ValidGroups
    {
        get
        {
            if (string.IsNullOrEmpty(lbxGroup1.SelectedValue))
                return 0;
            if (string.IsNullOrEmpty(lbxGroup2.SelectedValue))
                return 1;

            return 2;
        }
    }

    private string BuildSubTitle()
    {
        switch (this.ValidGroups)
        {
            case 0:
                return Resources.CoreWebContent.WEBCODE_AK0_100;
            case 1:
                return string.Format(Resources.CoreWebContent.WEBCODE_AK0_101, lbxGroup1.SelectedItem.Text);
            case 2:
                return string.Format(Resources.CoreWebContent.WEBCODE_AK0_102, lbxGroup1.SelectedItem.Text, lbxGroup2.SelectedItem.Text);
        }

        return string.Empty;
    }

    private void FixupBlankGroupings()
    {
        List<string> groups = new List<string>();
        groups.Add(lbxGroup1.SelectedValue);
        groups.Add(lbxGroup2.SelectedValue);

        groups.RemoveAll(string.IsNullOrEmpty);

        for (int i = 0; i < 3; ++i) groups.Add(string.Empty);

        lbxGroup1.SelectedValue = groups[0];
        lbxGroup2.SelectedValue = groups[1];
    }

    public override string DefaultResourceSubTitle
    {
        get { return BuildSubTitle(); }
    }

    public override bool ShowSubTitleHintMessage
    {
        get
        {
            return true;
        }
    }

    public override string SubTitleHintMessage
    {
        get
        {
            return Resources.CoreWebContent.WEBCODE_AK0_104;
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            FixupBlankGroupings();

            properties.Add("Grouping1", lbxGroup1.SelectedValue);
            properties.Add("Grouping2", lbxGroup2.SelectedValue);

            //properties.Add("Filter", SqlFilterChecker.CleanFilter(SQLFilter.FilterTextBox.Text));

            //properties.Add("GroupNodesWithNullPropertiesAsUnknown", GroupNulls.SelectedValue);
            properties.Add("RememberExpandedState", RememberExpandedState.Checked.ToString());

            // We changed stuff so clear out the expanded tree node information
            try
            {
                string rememberCollapseState = Resource.Properties["RememberExpandedState"] ?? "true";
                var oldValue = bool.Parse(rememberCollapseState);
                if (oldValue != RememberExpandedState.Checked)
                {
                    var expandedStateProviderIdentifier = String.Format("SCM_WSUS_SERVERS_TREE_STATE_-{0}-{1}-{2}",
                                                                        this.Resource.ID,
                                                                        this.Resource.View.ViewID,
                                                                        Request.QueryString["NetObject"] ?? String.Empty); //identifies this instance in session scope

                    if (SessionScopeTreeExpandedStateManager.ContainsProvider(expandedStateProviderIdentifier))
                        SessionScopeTreeExpandedStateManager.ClearExpandedStateManager(expandedStateProviderIdentifier);
                }
            }

            catch (Exception)
            { }

            return properties;
        }
    }
}
