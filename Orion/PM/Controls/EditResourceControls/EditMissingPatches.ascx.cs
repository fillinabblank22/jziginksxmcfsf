﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_PM_Controls_EditResourceControls_EditMissingPatches : BaseResourceEditControl
{
    private const string FILTER_APPROVED = "Include Next Updates";
    private const string FILTER_RESOURCE_APPROVED_PROPERTY_KEY = "approved_filter";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        listBoxDropDownApproved.Items.AddRange(new ListItem[] { new ListItem("All", "ALL"),
            new ListItem("Approved", "APPROVED"),
            new ListItem("Not Approved", "NOTAPPROVED")
        });

        dropDownApproved.Text = FILTER_APPROVED;

        listBoxDropDownApproved.SelectedIndex = 1;
        foreach (ListItem item in listBoxDropDownApproved.Items)
        {
            if (item.Value.Equals(Resource.Properties[FILTER_RESOURCE_APPROVED_PROPERTY_KEY], StringComparison.OrdinalIgnoreCase))
            {
                listBoxDropDownApproved.SelectedValue = Resource.Properties[FILTER_RESOURCE_APPROVED_PROPERTY_KEY];
                break;
            }
        }
    }


    public override Dictionary<string, object> Properties
    {
        get
        {
            var properties = new Dictionary<string, object>();

            properties.Add(FILTER_RESOURCE_APPROVED_PROPERTY_KEY, this.listBoxDropDownApproved.SelectedValue);

            return properties;
        }
    }
}