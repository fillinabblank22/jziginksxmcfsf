﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.PM.Web.DAL;
using SolarWinds.PM.Web.Helper;
using System.Text;

public partial class Orion_SCM_Controls_EditResourceControls_EditGroupsMissingUpdate : BaseResourceEditControl
{
    private const int MIN_VALUE_OF_MAX_COUNT = 1;
    private const int MAX_VALUE_OF_MAX_COUNT = 10000;
    private const string MAX_COUNT_ERROR_RANGE_MESSAGE = "Value has to be a number in range of: ";

    private const string WARNING_THRESHOLDS_CAPTION = "Warning:";
    private const string CRITICAL_THRESHOLDS_CAPTION = "Critical:";
    private const string THRESHOLDS_CAPTION = "Threshold Settings";

    private const string CRITICAL_THRESHOLDS_RESOURCE_PROPERTY_KEY = "CriticalThreshold";
    private const Int32 CRITICAL_THRESHOLDS_DEFAULT = 15;

    private const string WARNING_THRESHOLDS_RESOURCE_PROPERTY_KEY = "WarningThreshold";
    private const Int32 WARNING_THRESHOLDS_DEFAULT = 8;

    private const string JUST_APPROVED_UPDATES_CAPTION = "Show just approved updates";
    private const string UPDATE_STATUS_CAPTION = "Filter of Update Installation";
    private const string SUB_TITLE_HINT_CAPTION = "Leave the subtitle field blank to generated it automatically.";
    private const string FILTER_UPDATES_TITLE_CAPTION = "Filter Update Approval Action";

    private const string JUST_APPROVED_UPDATES_PROPERTY_KEY = "JustApprovedUpdates";
    private const string UPDATE_STATUS_RESOURCE_PROPERTY_KEY = "UpdateInstallationState";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!this.IsPostBack)
        {
            vldCritical.ErrorMessage = String.Format("{0}<{1}, {2}>", MAX_COUNT_ERROR_RANGE_MESSAGE, MIN_VALUE_OF_MAX_COUNT, MAX_VALUE_OF_MAX_COUNT);
            vldCritical.MinimumValue = MIN_VALUE_OF_MAX_COUNT.ToString();
            vldCritical.MaximumValue = MAX_VALUE_OF_MAX_COUNT.ToString();
            vldCritical.Type = ValidationDataType.Integer;

            vldWarning.ErrorMessage = String.Format("{0}<{1}, {2}>", MAX_COUNT_ERROR_RANGE_MESSAGE, MIN_VALUE_OF_MAX_COUNT, MAX_VALUE_OF_MAX_COUNT);
            vldWarning.MinimumValue = MIN_VALUE_OF_MAX_COUNT.ToString();
            vldWarning.MaximumValue = MAX_VALUE_OF_MAX_COUNT.ToString();
            vldWarning.Type = ValidationDataType.Integer;


            lblThresholds.Text = THRESHOLDS_CAPTION;
            lblCriticalThreshold.Text = CRITICAL_THRESHOLDS_CAPTION;
            lblWarningThreshold.Text = WARNING_THRESHOLDS_CAPTION;

            txtCritical.Text = Resource.Properties[CRITICAL_THRESHOLDS_RESOURCE_PROPERTY_KEY] ?? CRITICAL_THRESHOLDS_DEFAULT.ToString();
            txtWarning.Text = Resource.Properties[WARNING_THRESHOLDS_RESOURCE_PROPERTY_KEY] ?? WARNING_THRESHOLDS_DEFAULT.ToString();




            chkJustApprovedUpdates.Text = JUST_APPROVED_UPDATES_CAPTION;
            lblFilterUpdates.Text = FILTER_UPDATES_TITLE_CAPTION;

            string justApprovedUpdates = Resource.Properties[JUST_APPROVED_UPDATES_PROPERTY_KEY] ?? "true";
            chkJustApprovedUpdates.Checked = Boolean.Parse(justApprovedUpdates);


            lbFilter.Items.Add(new ListItem("None", ""));
            var list = new WsusNodesUpdatesDAL().GetCurrentStatesExceptOfInstalled();
            foreach (var item in list)
            {
                lbFilter.Items.Add(new ListItem(ResourceHelper.GetSpacedTextFromUnspaces(item), item));
            }

            lblFilter.Text = UPDATE_STATUS_CAPTION;

            lbFilter.SelectedIndex = 0;
            foreach (ListItem item in lbFilter.Items)
            {
                if (item.Value.Equals(Resource.Properties[UPDATE_STATUS_RESOURCE_PROPERTY_KEY], StringComparison.OrdinalIgnoreCase))
                {
                    lbFilter.SelectedValue = Resource.Properties[UPDATE_STATUS_RESOURCE_PROPERTY_KEY];
                    break;
                }
            }
        }
    }

    private string BuildSubTitle()
    {
        var result = new StringBuilder();
        var subTitleItems = new List<String>();

        if (chkJustApprovedUpdates.Checked)
            result.Append(JUST_APPROVED_UPDATES_CAPTION);

        if (this.lbFilter.SelectedIndex > 0)
        {
            if (result.Length != 0)
            {
                result.Append(", ").Append(lbFilter.SelectedItem.Text);
            }
            else
            {
                result.Append(string.Format(Resources.CoreWebContent.WEBCODE_AK0_101, this.lbFilter.SelectedValue));
            }
        }

        return result.ToString();
    }

    public override string DefaultResourceSubTitle
    {
        get { return BuildSubTitle(); }
    }

    public override bool ShowSubTitleHintMessage
    {
        get
        {
            return true;
        }
    }

    public override string SubTitleHintMessage
    {
        get
        {
            return SUB_TITLE_HINT_CAPTION;
        }
    }


    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            Int32 critical;
            if (!Int32.TryParse(txtCritical.Text, out critical))
                critical = CRITICAL_THRESHOLDS_DEFAULT;

            Int32 warning;
            if (!Int32.TryParse(txtWarning.Text, out warning))
                warning = WARNING_THRESHOLDS_DEFAULT;

            properties.Add(CRITICAL_THRESHOLDS_RESOURCE_PROPERTY_KEY, critical);
            properties.Add(WARNING_THRESHOLDS_RESOURCE_PROPERTY_KEY, warning);
            properties.Add(JUST_APPROVED_UPDATES_PROPERTY_KEY, chkJustApprovedUpdates.Checked.ToString());
            properties.Add(UPDATE_STATUS_RESOURCE_PROPERTY_KEY, this.lbFilter.SelectedValue);

            return properties;
        }
    }
}
