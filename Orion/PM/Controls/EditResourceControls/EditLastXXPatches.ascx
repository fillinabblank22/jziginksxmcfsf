﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditLastXXPatches.ascx.cs" Inherits="Orion_SCM_Controls_EditResourceControls_EditLastXXPatches" %>

<b><asp:Literal runat="server" ID="lblThresholds" /></b><br />
    <asp:Literal runat="server" ID="lblWarningThreshold" /><br />
    <asp:TextBox runat="server" ID="txtWarning" />
    <asp:RangeValidator ID="vldWarning" runat="server" ControlToValidate="txtWarning" EnableClientScript="true"></asp:RangeValidator><br />
   
    <asp:Literal runat="server" ID="lblCriticalThreshold" /><br />
    <asp:TextBox runat="server" ID="txtCritical" />
    <asp:RangeValidator ID="vldCritical" runat="server" ControlToValidate="txtCritical" EnableClientScript="true"></asp:RangeValidator><br />
<br />

<b><asp:Literal runat="server" ID="lblFilterUpdates" /></b><br />
    <asp:CheckBox runat="server" ID="chkExcludeUpdates" /><br />
    <br />

<b><asp:Literal runat="server" ID="lblGrouping" /></b><br />
    <asp:ListBox runat="server" ID="lbxGroup1" SelectionMode="single" Rows="1">
        <asp:ListItem Text="None" Value="" />
        <asp:ListItem Text="Severity" Value="MsrcSeverity" />
        <asp:ListItem Text="Update Classification Title" Value="UpdateClassificationTitle" />
        <asp:ListItem Text="Company Titles" Value="CompanyTitles" />
        <asp:ListItem Text="Product Titles" Value="ProductTitles" />
        <asp:ListItem Text="Product Family Titles" Value="ProductFamilyTitles" />
        <asp:ListItem Text="Update Type" Value="UpdateType" />
    </asp:ListBox>