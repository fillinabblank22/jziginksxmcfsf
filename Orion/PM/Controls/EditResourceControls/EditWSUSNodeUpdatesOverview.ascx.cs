﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web;

public partial class Orion_SCM_Controls_EditResourceControls_EditWSUSNodeUpdatesOverview : BaseResourceEditControl
{
    private readonly string JustApprovedUpdatesCaption = "Show just approved updates";
    private readonly string SubTitleHindCaption ="Leave the subtitle field blank to generated it automatically.";
    private readonly string FilterUpdatesTitleCaption = "Filter Updates";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!this.IsPostBack)
        {
            chkJustApprovedUpdates.Text = JustApprovedUpdatesCaption;
            lblFilterUpdates.Text = FilterUpdatesTitleCaption;

            string justApprovedUpdates = Resource.Properties["JustApprovedUpdates"] ?? "true";
            chkJustApprovedUpdates.Checked = Boolean.Parse(justApprovedUpdates);
        }
    }

    private string BuildSubTitle()
    {
        if (chkJustApprovedUpdates.Checked)
        {
            return JustApprovedUpdatesCaption;
        }

        return string.Empty;
    }

    public override string DefaultResourceSubTitle
    {
        get { return BuildSubTitle(); }
    }

    public override bool ShowSubTitleHintMessage
    {
        get
        {
            return true;
        }
    }

    public override string SubTitleHintMessage
    {
        get
        {
            return SubTitleHindCaption;
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            properties.Add("JustApprovedUpdates", chkJustApprovedUpdates.Checked.ToString());

            return properties;
        }
    }
}
