<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditWSUSServersTree.ascx.cs" Inherits="Orion_SCM_Controls_EditResourceControls_EditWSUSServersTree" %>

<script type="text/javascript">
    function ClientValidate(source, arguments) {
        var lbxGroup1 = $('#<%= this.lbxGroup1.ClientID %>');
        var lbxGroup2 = $('#<%= this.lbxGroup2.ClientID %>');

        if (arguments.Value != lbxGroup1[0].value
                && arguments.Value != lbxGroup2[0].value)
            arguments.IsValid = true;
        else
            arguments.IsValid = false;
    }
</script>
<b><asp:Literal runat="server" ID="lblGroupNodesCaption" /></b><br />
<asp:Literal runat="server" ID="lblGroupCaption" />
<p>
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_343) %><br />
    <asp:ListBox runat="server" ID="lbxGroup1" SelectionMode="single" Rows="1">
        <asp:ListItem Text="None" Value="" />
        <asp:ListItem Text="OS Vendor" Value="OsVendor" />
        <asp:ListItem Text="OS Product Type" Value="OSProductType" />
        <asp:ListItem Text="OS Type" Value="OsType" />
        <asp:ListItem Text="OS Description" Value="OSDescription" />
        <asp:ListItem Text="Port" Value="Port" />
    </asp:ListBox>
</p>
<p>
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_344) %><br />
    <asp:ListBox runat="server" ID="lbxGroup2" SelectionMode="single" Rows="1">
        <asp:ListItem Text="None" Value="" />
        <asp:ListItem Text="OS Vendor" Value="OsVendor" />
        <asp:ListItem Text="OS Product Type" Value="OSProductType" />
        <asp:ListItem Text="OS Type" Value="OsType" />
        <asp:ListItem Text="OS Description" Value="OSDescription" />
        <asp:ListItem Text="Port" Value="Port" />
    </asp:ListBox>
    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="lbxGroup2"
        ControlToCompare="lbxGroup1" Type="String" Operator="NotEqual" Display="Dynamic"
        ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_351 %>"></asp:CompareValidator>
</p>

<p>
    <asp:CheckBox runat="server" ID="RememberExpandedState" Text="Remember Expanded State" />
</p>
