<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXPatchesMissingCtrl.ascx.cs"
    Inherits="Orion_PM_Controls_Summary_TopXXPatchesMissingCtrl" %>
<%@ Register Assembly="SolarWinds.PM.Web" Namespace="SolarWinds.PM.Web.Charting" TagPrefix="charts" %>

<div runat="server" ID="lblNoDataToDisplay"></div> 
<div runat="server" id="dataDiv">
    <div style="width: 100%; text-align: center">
        <charts:ScmChartImage runat="server" id="imgGraph" />
    </div>
    <asp:Repeater runat="server" ID="resourceTable">
        <HeaderTemplate>
            <table cellpadding = "0" cellspacing = "0">
            <tr>
                <td class="ReportHeader" colspan="3"><%= DefaultSanitizer.SanitizeHtml(this.PatchNameCaption.ToUpperInvariant()) %></td>
                <td class="ReportHeader"><span style="white-space: nowrap;"><%= DefaultSanitizer.SanitizeHtml(this.NodeMissingUpdateCaption.ToUpperInvariant()) %></span></td>
            </tr>
        </HeaderTemplate>
        <ItemTemplate>
	        <tr>
                <script type="text/javascript" language="javascript">
                    function PlusClick_<%# DefaultSanitizer.SanitizeHtml(this.GetId(Eval("NetObjectID"))) %>() {
                        Orion_PM_Controls_Summary_TopXXPatchesMissingCtrlToggle('<%# DefaultSanitizer.SanitizeHtml(this.GetId(Eval("NetObjectID"))) %>', <%# DefaultSanitizer.SanitizeHtml(this.GetDataToLoad(Eval("NetObjectID").ToString())) %>); 
                        Orion_PM_Controls_Summary_TopXXPatchesMissingCtrlChangeExpander('img_<%# DefaultSanitizer.SanitizeHtml(this.GetId(Eval("NetObjectID"))) %>');
                    }
                </script>
	            <td class="Property" style="width: 1%; padding-left: 3px; padding-right: 3px;"><a style="cursor: pointer; margin-right:5px; margin-top: 3px;" href="javascript: PlusClick_<%# DefaultSanitizer.SanitizeHtml(this.GetId(Eval("NetObjectID"))) %>();"><img id="img_<%# DefaultSanitizer.SanitizeHtml(this.GetId(Eval("NetObjectID"))) %>" alt="Icon" src="/Orion/images/Button.Expand.gif"/></a></td>
	            <td class="Property" style="width: 1%; padding-left: 3px; padding-right: 3px;"><div style="width: 15px; height: 15px; margin-right:10px; background-color:<%# HttpUtility.HtmlEncode(Eval("Color").ToString()) %>; border-color: Black; border-width: 1px; border-style: solid; <%# DefaultSanitizer.SanitizeHtml(this.GetColorVisibility()) %>">&nbsp;</div></td>
	            <td class="Property" style="padding-left: 3px; padding-right: 3px;"><img alt="Icon" src="<%# DefaultSanitizer.SanitizeHtml(Eval("Icon").ToString()) %>" /><a href="<%# HttpUtility.HtmlEncode(Eval("Link").ToString())%>"><%# HttpUtility.HtmlEncode(Eval("Name").ToString())%></a></td>
	            <td class="Property" style="padding-left: 3px; padding-right: 3px;"><%# HttpUtility.HtmlEncode(Eval("Value").ToString()) %></td>
            </tr>
            <tr id="Sel_<%# DefaultSanitizer.SanitizeHtml(this.GetId(Eval("NetObjectID"))) %>" style="display: none;">
                <td style="border-width: 0px;" colspan="2">&nbsp;</td>
                <td style="border-width: 0px;" colspan="2">
                    <span id="loader_<%# DefaultSanitizer.SanitizeHtml(this.GetId(Eval("NetObjectID"))) %>" style="margin-bottom: 10px; margin-top:10px;"><img src="/Orion/images/AJAX-Loader.gif" alt="Loading..." /><%= DefaultSanitizer.SanitizeHtml(this.LoadingCaption) %></span>
                    <div id="content_<%# DefaultSanitizer.SanitizeHtml(this.GetId(Eval("NetObjectID"))) %>" style="margin-bottom: 10px; margin-top:10px;" />
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</div>