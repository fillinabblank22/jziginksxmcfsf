<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LastXXTasksCtrl.ascx.cs" Inherits="Orion_PM_Resources_Summary_LastXXTasksCtrl" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>

    <div ID="errorPanel" runat="server" Visible="false"></div>
    <asp:Repeater runat="server" ID="resourceTable">
        <HeaderTemplate>
<table cellpadding = "0" cellspacing = "0">
    <tr>
        <td class="ReportHeader" colspan="2"><span style="white-space: nowrap;"><%= DefaultSanitizer.SanitizeHtml(this.DisplaynameCaption) %></span></td>
        <td class="ReportHeader"><span style="white-space: nowrap;"><%= DefaultSanitizer.SanitizeHtml(this.CurrentStateCaption) %></span></td>
        <td class="ReportHeader"><span style="white-space: nowrap;"><%= DefaultSanitizer.SanitizeHtml(this.SubmissionTimeCaption) %></span></td>
        <td class="ReportHeader"><span style="white-space: nowrap;"><%= DefaultSanitizer.SanitizeHtml(this.CompletedTimeCaption) %></span></td>
    </tr>
        </HeaderTemplate>
        <ItemTemplate>
	<tr style="<%# DefaultSanitizer.SanitizeHtml(Eval("Style")) %>Task">
	    <td class="Property" style="border-color: white; padding-left: 3px; padding-right: 3px;"><img alt="Icon" src="<%# DefaultSanitizer.SanitizeHtml(Eval("Icon").ToString()) %>" /></td>
	    <td class="Property" style="border-color: white; padding-left: 3px; padding-right: 3px;">&nbsp;<%# HttpUtility.HtmlEncode(Eval("Displayname").ToString())%></td>
	    <td class="Property" style="border-color: white; padding-left: 3px; padding-right: 3px;"><span style="white-space: nowrap;"><%# SolarWinds.PM.Web.Helper.ResourceHelper.GetSpacedTextFromUnspaces(HttpUtility.HtmlEncode(Eval("CurrentState").ToString()))%></span></td>
	    <td class="Property" style="border-color: white; padding-left: 3px; padding-right: 3px;"><span style="white-space: nowrap;"><%# HttpUtility.HtmlEncode(Eval("SubmissionTimeString").ToString())%></span></td>
	    <td class="Property" style="border-color: white; padding-left: 3px; padding-right: 3px;"><span style="white-space: nowrap;"><%# HttpUtility.HtmlEncode(Eval("CompletionTimeString").ToString())%></span></td>
    </tr>
        </ItemTemplate>
        <FooterTemplate>
</table>
        </FooterTemplate>
    </asp:Repeater>
