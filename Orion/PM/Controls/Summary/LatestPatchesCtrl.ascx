<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LatestPatchesCtrl.ascx.cs" Inherits="Orion_PM_Controls_Summary_LatestPatchesCtrl" %>

<div runat="server" ID="lblNoDataToDisplay"></div> 
<div runat="server" id="dataDiv">
    <asp:Repeater runat="server" ID="resourceTable" OnItemCreated="resourceTable_ItemCreated">
        <HeaderTemplate>
                <table cellpadding = "0" cellspacing = "0">
	                <tr>
	                    <td class="ReportHeader" colspan="4"><%= DefaultSanitizer.SanitizeHtml(TH_NAME) %></td>
	                    <td class="ReportHeader" style="width: 80px;"><%= DefaultSanitizer.SanitizeHtml(TH_SEVERITY) %></td>
	                    <td class="ReportHeader" style="width: 80px;"><%= DefaultSanitizer.SanitizeHtml(TH_INSTALLATIONS) %></td>
	                    <td class="ReportHeader" style="width: 80px;"><%= DefaultSanitizer.SanitizeHtml(TH_CREATION_DATE) %></td>
                    </tr>
        </HeaderTemplate>
        <ItemTemplate>
	                <tr style="<%# DefaultSanitizer.SanitizeHtml(this.GetStyleGroup) %>">
	                    <td class="Property" style="padding-left: 3px; padding-right:3px; width: 20px;"><a style="cursor: pointer;" href="javascript: Orion_PM_Controls_Summary_LatestPatchesToggle('<%# DefaultSanitizer.SanitizeHtml(this.GetGroup) %>'); Orion_PM_Controls_Summary_LatestPatchesChangeExpander('img_<%# DefaultSanitizer.SanitizeHtml(this.GetGroup) %>');"><img id="img_<%# DefaultSanitizer.SanitizeHtml(this.GetGroup) %>" alt="[+]" src="/Orion/images/Button.Expand.gif" /></a></td>
	                    <td class="Property" style="padding-left: 3px; padding-right:3px; width: 1%;"><img alt="icon" src="<%# DefaultSanitizer.SanitizeHtml(this.ItemGroup.Icon) %>" /></td>
	                    <td class="Property" style="padding-left: 3px; padding-right:3px;" colspan="2"><%# DefaultSanitizer.SanitizeHtml(this.ItemGroup.Grouping) %></td>
    <%--	                <td class="Property" style="padding-left: 3px; padding-right:3px;"><%# DefaultSanitizer.SanitizeHtml(this.ItemGroup.Severity) %></td>
	                    <td class="Property" style="padding-left: 3px; padding-right:3px; <%# DefaultSanitizer.SanitizeHtml(this.GetGroupColorThreshold) %> <%# DefaultSanitizer.SanitizeHtml(this.GetGroupWeightThreshold) %>"><%# DefaultSanitizer.SanitizeHtml(this.ItemGroup.Installations.ToString()) %></td>
    --%>	                <td class="Property" style="padding-left: 3px; padding-right:3px;">&nbsp;</td>
	                    <td class="Property" style="padding-left: 3px; padding-right:3px;">&nbsp;</td>
	                    <td class="Property" style="padding-left: 3px; padding-right:3px;">&nbsp;</td>
                    </tr>
                    <asp:Repeater runat="server" ID="resourceTableInner" DataSource="<%# DefaultSanitizer.SanitizeHtml(this.ItemGroup.Childrens) %>" OnItemCreated="resourceTableInner_ItemCreated">
                        <ItemTemplate>
                                <tr class="<%# DefaultSanitizer.SanitizeHtml(this.GetGroup) %>" style="<%# DefaultSanitizer.SanitizeHtml(this.GetStyleDetail) %>">
	                                <td class="Property" style="padding-left: 3px; padding-right:3px;" colspan="2">&nbsp;</td>
	                                <td class="Property" style="padding-left: 3px; padding-right:3px;"><img alt="icon" src="<%# DefaultSanitizer.SanitizeHtml(SolarWinds.PM.Common.Enums.UpdateSeverityHelper.GetIcon(this.ItemDetail.Severity.ToString())) %>" /></td>
	                                <td class="Property" style="padding-left: 3px; padding-right:3px;"><a href="<%# DefaultSanitizer.SanitizeHtml(this.GetHref) %>"><%# DefaultSanitizer.SanitizeHtml(this.ItemDetail.Title) %></a></td>
	                                <td class="Property" style="padding-left: 3px; padding-right:3px;"><%# DefaultSanitizer.SanitizeHtml(this.ItemDetail.Severity.ToString().ToUpperInvariant()) %></td>
	                                <td class="Property" style="padding-left: 3px; padding-right:3px; <%# DefaultSanitizer.SanitizeHtml(this.GetColorThreshold) %> <%# DefaultSanitizer.SanitizeHtml(this.GetWeightThreshold) %>"><%# DefaultSanitizer.SanitizeHtml(this.ItemDetail.Installations.ToString()) %></td>
	                                <td class="Property" style="padding-left: 3px; padding-right:3px;"><%# DefaultSanitizer.SanitizeHtml(this.ItemDetail.ArrivalDate.ToShortDateString()) %></td>
                                </tr>
                        </ItemTemplate>
                    </asp:Repeater>
        </ItemTemplate>
        <FooterTemplate>
                </table>
        </FooterTemplate>
    </asp:Repeater>
</div>