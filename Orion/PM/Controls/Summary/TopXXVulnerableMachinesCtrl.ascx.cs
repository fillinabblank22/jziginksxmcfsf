﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.PM.Web.Charting;
using SolarWinds.PM.Web.DAL;
using System.Data;
using SolarWinds.PM.Web.Resources;
using SolarWinds.PM.Web.NetObjects;
using SolarWinds.PM.Web.Resources.Summary;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using SolarWinds.Orion.Web;
using SolarWinds.PM.Web.Helper;
using System.Text;
using System.Web.Script.Serialization;

public partial class Orion_PM_Controls_Summary_TopXXVulnerableMachinesCtrl : ScmResourceBaseAsync
{
    public Int32 MaxRecords { get; set; }
    public Int32 WarningThreshold { get; set; }
    public Int32 CriticalThreshold { get; set; }
    public String NetObjectId { get; set; }
    public Boolean JustApprovedUpdates { get; set; }

    private const string NoDataToDisplayCaption = "No data to display";

    protected readonly string LoadingCaption = "Loading...";

    protected readonly string MachineNameCaption = "Machine";
    protected readonly string OperatingSystemCaption = "Operating System";
    protected readonly string ServerNameCaption = "Wsus Server";
    protected readonly string UpdatesMissingCaption = "Updates Missing";

    protected String GetColorThreshold(Int32 value)
    {
        return value >= WarningThreshold ? "color: red;" : String.Empty;
    }

    protected String GetWeightThreshold(Int32 value)
    {
        return value >= CriticalThreshold ? "font-weight: bold;" : String.Empty;
    }

    protected String GetId(Object netObjectId)
    {
        return netObjectId.ToString().Replace(SolarWinds.PM.Web.NetObjects.WsusNode.ID_DELIMITER, '_').Replace(":", "_").Replace("-", "_");
    }

    protected String GetDataToLoad(String netObjectId)
    {
        var taskList = new List<TaskExecutionEntity>()
        {
            new TaskExecutionEntity()
            {
                ServerResourceControl = new ServerResourceModel("/Orion/PM/Controls/Summary/TopXXVulnerableMachinesUpdateListCtrl.ascx"
                                                             , new Dictionary<string, object>()
                                                             {
                                                                 { "NetObjectId", netObjectId},
                                                                 { "JustApprovedUpdates", this.JustApprovedUpdates },
                                                             }
                                                             , new SolarWinds.PM.Web.TaskServices.Node.WsusUpdatesBase().Load
                                                             , new object[] { netObjectId }),
            }
        };

        return FormatArray(taskList.Select(s => s.ToJson()).ToArray());
    }

    protected string FormatArray(object[] obj)
    {
        StringBuilder sb = new StringBuilder();
        JavaScriptSerializer js = new JavaScriptSerializer();
        return js.Serialize(obj);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        lblNoDataToDisplay.InnerHtml = ResourceHelper.GetInfoMessageHtml(NoDataToDisplayCaption);

        Guid deviceId = Guid.Empty;
        if (!String.IsNullOrEmpty(this.NetObjectId))
        {
            var wsusServer = NetObjectFactory.Create(this.NetObjectId, true) as WsusServer;
            if (wsusServer != null)
                deviceId = wsusServer.WsusServerDetails.DeviceID;
        }

        try
        {
            var table = new TopXXVulnerableMachines().GetNodes(this.MaxRecords, deviceId, JustApprovedUpdates);

            this.resourceTable.DataSource = table;
            this.resourceTable.DataBind();

            dataDiv.Visible = table.Rows.Count != 0;
            lblNoDataToDisplay.Visible = !dataDiv.Visible;
        }
        catch (Exception)
        {
            dataDiv.Visible = false;
            lblNoDataToDisplay.Visible = !dataDiv.Visible;
        }
    }
}
