﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI.Localizer;
using SolarWinds.PM.Web.Resources;
using SolarWinds.PM.Web.AjaxTree;
using SolarWinds.PM.Web.Controls;
using SolarWinds.PM.Web.Controls.AssetTree;
using SolarWinds.PM.Web.Helper;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.PM.Web.NetObjects;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using SolarWinds.Orion.Web.UI;
using System.Data;
using SolarWinds.PM.Web.Model;
using SolarWinds.PM.Common.Enums;
using SolarWinds.PM.Web.Resources.Summary;
using SolarWinds.Orion.Web;

public partial class Orion_PM_Controls_Summary_LatestPatchesCtrl : ScmResourceBaseAsync
{
    private Guid controlId = Guid.NewGuid();

    public Int32 MaxRecords { get; set; }
    public String GroupingLevel { get; set; }
    public Int32 WarningThreshold { get; set; }
    public Int32 CriticalThreshold { get; set; }
    public Boolean ExcludeNotApplicableUpdates { get; set; }
    public String NetObjectId { get; set; }


    private const string NoDataToDisplayCaption = "No data to display";

    protected const string TH_NAME = "NAME";
    protected const string TH_SEVERITY = "SEVERITY";
    protected const string TH_INSTALLATIONS = "INSTALLATIONS";
    protected const string TH_CREATION_DATE = "ARRIVAL DATE";

    protected UpdateGroup ItemGroup { get; set; }
    protected ShortUpdateInfo ItemDetail { get; set; }
    protected Boolean IsGrouping { get { return this.ItemGroup != null && !String.IsNullOrEmpty(this.ItemGroup.Grouping); } }
    protected String GetStyleGroup { get { return this.ItemGroup != null && !String.IsNullOrEmpty(this.ItemGroup.Grouping) ? String.Empty : "display: none;"; } }
    protected String GetStyleDetail { get { return this.ItemGroup != null && !String.IsNullOrEmpty(this.ItemGroup.Grouping) ? "display: none;" : String.Empty; } }
    protected String GetGroup { get { return String.Format("Sel_{0}_{1}", controlId, this.ItemGroup == null ? String.Empty : System.Text.RegularExpressions.Regex.Replace(this.ItemGroup.Grouping,"[^a-zA-Z0-9]", "_")); } }
    protected String GetHref { get { return this.ItemDetail == null ? String.Empty : "/Orion/PM/UpdateDetails.aspx?NetObject=" + new UpdateEntity(this.ItemDetail.UpdateId, this.ItemDetail.WsusServerDeviceID).NetObjectId; } }
    protected String GetColorThreshold { get { return this.ItemDetail == null ? String.Empty : this.ItemDetail.Installations >= WarningThreshold ? "color: red;" : String.Empty; } }
    protected String GetWeightThreshold { get { return this.ItemDetail == null ? String.Empty : this.ItemDetail.Installations >= CriticalThreshold ? "font-weight: bold;" : String.Empty; } }
    //protected String GetGroupColorThreshold { get { return this.ItemGroup == null ? String.Empty : this.ItemGroup.Childrens.Max(m => m.Installations) >= WarningThreshold ? "color: red;" : String.Empty; } }
    //protected String GetGroupWeightThreshold { get { return this.ItemGroup == null ? String.Empty : this.ItemGroup.Childrens.Max(m => m.Installations) >= CriticalThreshold ? "font-weight: bold;" : String.Empty; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        lblNoDataToDisplay.InnerHtml = ResourceHelper.GetInfoMessageHtml(NoDataToDisplayCaption);

        try
        {
            Guid deviceId = Guid.Empty;
            if (!String.IsNullOrEmpty(this.NetObjectId))
            {
                var wsusServer = NetObjectFactory.Create(this.NetObjectId, true) as WsusServer;
                if (wsusServer != null)
                    deviceId = wsusServer.WsusServerDetails.DeviceID;
            }

            var data = new LatestPatches().GetLatestPatches(this.MaxRecords, deviceId, this.GroupingLevel, this.ExcludeNotApplicableUpdates);

            this.resourceTable.DataSource = data;
            this.resourceTable.DataBind();

            dataDiv.Visible = data.Count() != 0;
            lblNoDataToDisplay.Visible = !dataDiv.Visible;
        }
        catch (Exception)
        {
            dataDiv.Visible = false;
            lblNoDataToDisplay.Visible = !dataDiv.Visible;
        }
    }

    protected void resourceTable_ItemCreated(object sender, RepeaterItemEventArgs e)
    {
        this.ItemGroup = e.Item.DataItem as UpdateGroup;
    }

    protected void resourceTableInner_ItemCreated(object sender, RepeaterItemEventArgs e)
    {
        this.ItemDetail = e.Item.DataItem as ShortUpdateInfo;
    }
}