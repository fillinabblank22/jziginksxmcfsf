﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.PM.Web.Charting;
using SolarWinds.PM.Web.Resources;
using SolarWinds.PM.Web.NetObjects;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using System.Data;
using System.Data.SqlClient;
using SolarWinds.PM.Web.Resources.Summary;
using SolarWinds.Orion.Web;
using SolarWinds.PM.Web.Helper;

public partial class Orion_PM_Controls_Summary_NodesHealthOverviewCtrl : ScmResourceBaseAsync
{
    public String DefaultTitle { get; set; }
    public String Filter { get; set; }
    public String NetObjectId { get; set; }
    public String LegendHelpFragment { get; set; }
    public bool JustApprovedUpdates { get; set; }

    private readonly string NoDataToDisplayCaption = "No data to display";

    private readonly string ComputerUpToDateCaption = "Computers up to date";
    private readonly string ComputerNeedingUpdatesCaption = "Computers needing updates";
    private readonly string ComputerWithUpdatesErrorsCaption = "Computers with update errors";
    private readonly string ComputerWithUnkownStatusCaption = "Computers with unknown status";
    private readonly string LegendTitleCaption = "Node Count";

    private readonly string NodeNameLegendFieldCaption = "Node name";
    private readonly string WsusServerLegendFieldCaption = "Wsus server";

    protected void Page_Load(object sender, EventArgs e)
    {
        lblNoDataToDisplay.InnerHtml = ResourceHelper.GetInfoMessageHtml(NoDataToDisplayCaption);

        Guid deviceId = Guid.Empty;
        if (!String.IsNullOrEmpty(this.NetObjectId))
        {
            var wsusServer = NetObjectFactory.Create(this.NetObjectId, true) as WsusServer;
            if (wsusServer != null)
                deviceId = wsusServer.WsusServerDetails.DeviceID;
        }

        try
        {
            var detailsData = new NodesHealthOverview.DetailsData()
            {
                ComputerUpToDate = ComputerUpToDateCaption,
                ComputerNeedingUpdates = ComputerNeedingUpdatesCaption,
                ComputerWithUnkownStatus = ComputerWithUnkownStatusCaption,
                ComputerWithUpdatesErrors = ComputerWithUpdatesErrorsCaption,
                JustApprovedUpdates = JustApprovedUpdates,
                Filter = this.Filter,
            };
            var legendData = new NodesHealthOverview.LegendData()
            {
                Title = this.DefaultTitle,
                HelpFragment = this.LegendHelpFragment,
                NodeName = NodeNameLegendFieldCaption,
                WsusServer = WsusServerLegendFieldCaption,
            };

            var table = new NodesHealthOverview().GetHealthOverview(deviceId, detailsData, legendData);

            this.resourceTable.DataSource = table;
            this.resourceTable.DataBind();

            var chartInfo = SolarWinds.Orion.Web.Charting.ChartInfoFactory.Create("NodesHealthOverview") as NodesHealthOverviewChartInfo;
            chartInfo.Height = chartInfo.Width = 200;

            chartInfo.ComputerUpToDateCount = Convert.ToInt32(table.Rows[0]["Value"]);
            chartInfo.ComputerNeedingUpdatesCount = Convert.ToInt32(table.Rows[1]["Value"]);
            chartInfo.ComputerWithUpdatesErrorsCount = Convert.ToInt32(table.Rows[3]["Value"]);
            chartInfo.ComputerWithUnkownStatusCount = Convert.ToInt32(table.Rows[2]["Value"]);

            chartInfo.WsusServer = deviceId;

            var count = chartInfo.ComputerUpToDateCount + chartInfo.ComputerNeedingUpdatesCount + chartInfo.ComputerWithUnkownStatusCount + chartInfo.ComputerWithUpdatesErrorsCount;

            this.lblNodeCountCaption.Text = LegendTitleCaption;
            this.lblNodeCount.Text = count.ToString();


            imgGraph.ChartInfo = chartInfo;

            dataDiv.Visible = count != 0;
            lblNoDataToDisplay.Visible = !dataDiv.Visible;
        }
        catch (Exception)
        {
            dataDiv.Visible = false;
            lblNoDataToDisplay.Visible = !dataDiv.Visible;
        }
    }
}