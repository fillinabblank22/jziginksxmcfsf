﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.PM.Web.Charting;
using SolarWinds.PM.Web.DAL;
using System.Data;
using SolarWinds.PM.Web.Resources;
using SolarWinds.PM.Web.NetObjects;
using SolarWinds.PM.Web.Resources.Summary;
using SolarWinds.Orion.Web;
using SolarWinds.PM.Web.Helper;

public partial class Orion_PM_Controls_Summary_WSUSNodesOSOverviewCtrl : ScmResourceBaseAsync
{
    public String DefaultTitle { get; set; }
    public String NetObjectId { get; set; }
    public String LegendHelpFragment { get; set; }

    private const string NoDataToDisplayCaption = "No data to display";

    private readonly string LegendTitleCaption = "Node Count";

    private readonly string NodeNameLegendFieldCaption = "Node name";
    private readonly string WsusServerLegendFieldCaption = "Wsus server";

    protected void Page_Load(object sender, EventArgs e)
    {
        lblNoDataToDisplay.InnerHtml = ResourceHelper.GetInfoMessageHtml(NoDataToDisplayCaption);

        Guid deviceId = Guid.Empty;
        if (!String.IsNullOrEmpty(this.NetObjectId))
        {
            var wsusServer = NetObjectFactory.Create(this.NetObjectId, true) as WsusServer;
            if (wsusServer != null)
                deviceId = wsusServer.WsusServerDetails.DeviceID;
        }

        try
        {
            var legendData = new WsusNodesOSOverview.LegendData()
            {
                Title = this.DefaultTitle,
                HelpFragment = this.LegendHelpFragment,
                NodeName = NodeNameLegendFieldCaption,
                WsusServer = WsusServerLegendFieldCaption,
            };

            var table = new WsusNodesOSOverview().GetOSOverview(deviceId, legendData);

            this.resourceTable.DataSource = table;
            this.resourceTable.DataBind();

            Int32 countOfItems = 0;
            foreach (DataRow item in table.Rows)
                countOfItems += Convert.ToInt32(item["Value"]);

            this.lblNodeCountCaption.Text = LegendTitleCaption;
            this.lblNodeCount.Text = countOfItems.ToString();


            var chartInfo = SolarWinds.Orion.Web.Charting.ChartInfoFactory.Create("WSUSNodesOSOverview") as WSUSNodesOSOverviewChartInfo;
            chartInfo.Height = chartInfo.Width = 200;

            chartInfo.WsusServer = deviceId;

            imgGraph.ChartInfo = chartInfo;

            dataDiv.Visible = countOfItems != 0;
            lblNoDataToDisplay.Visible = !dataDiv.Visible;
        }
        catch (Exception)
        {
            dataDiv.Visible = false;
            lblNoDataToDisplay.Visible = !dataDiv.Visible;
        }
    }
}
