<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXPatchesMissingNodeListCtrl.ascx.cs" Inherits="Orion_PM_Controls_Summary_TopXXPatchesMissingNodeListCtrl" %>

<div runat="server" ID="lblNoDataToDisplay"></div> 
<div runat="server" id="dataDiv">
    <asp:Repeater runat="server" ID="resourceTable">
        <HeaderTemplate>
    <table cellpadding = "0" cellspacing = "0">
    <tr>
        <td class="ReportHeader" style="white-space: nowrap;"><%= DefaultSanitizer.SanitizeHtml(this.NodeNameCaption.ToUpperInvariant()) %></td>
        <td class="ReportHeader" style="white-space: nowrap;"><%= DefaultSanitizer.SanitizeHtml(this.WsusServerCaption.ToUpperInvariant()) %></td>
    </tr>
        </HeaderTemplate>
        <ItemTemplate>
    <tr>
	    <td class="<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "Class")) %>" style="padding-left: 3px; padding-right: 3px; border-width: 0px;"><img alt="Icon" src="<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "OrionNodeStatusIcon")) %>" />&nbsp;<a href="<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "NetObject")) %>"><%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "Title")) %></a></td>
	    <td class="<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "Class")) %>" style="padding-left: 3px; padding-right: 3px; border-width: 0px;"><img alt="Icon" src="/Orion/PM/images/WsusServer_16x16.png" />&nbsp;<a href="<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "NetObjectServer")) %>"><%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "ServerName")) %></a></td>
    </tr>
        </ItemTemplate>
        <FooterTemplate>
    <tr><td colspan="2" style="font-size: 12px; font-weight: bold; font-style: italic; padding-top: 6px; border-width: 0px;"><%# DefaultSanitizer.SanitizeHtml(this.MoreRecordsIsHiddenCaption) %></td></tr>
    </table>
        </FooterTemplate>
    </asp:Repeater>
</div>