﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using System.Data;
using System.Data.SqlClient;
using SolarWinds.PM.Web.DAL;
using SolarWinds.PM.Web.Resources;
using SolarWinds.PM.Web.Helper;
using SolarWinds.Orion.Web;

public partial class Orion_PM_Resources_Summary_LastXXTasksCtrl : ScmResourceBaseAsync
{
    protected readonly string SubmissionTimeCaption = "SUBMISSION TIME";
    protected readonly string CompletedTimeCaption = "COMPLETED TIME";
    protected readonly string CurrentStateCaption = "STATE";
    protected readonly string DisplaynameCaption = "TASK";

    public Int32 MaxRecords { get; set; }
    public String Filter { get; set; }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable table;
        try
        {
            table = new LastXXTasksDAL().GetLastXXTasks(this.MaxRecords, this.Filter);

            table.Columns.Add("SubmissionTimeString", typeof(string));
            table.Columns.Add("CompletionTimeString", typeof(string));
            table.Columns.Add("Icon", typeof(string));
            table.Columns.Add("Style", typeof(string));
            foreach (DataRow item in table.Rows)
            {
                var submissionTime = Convert.ToDateTime(item["SubmissionTime"]);
                var completionTime = Convert.ToDateTime(item["CompletionTime"]);

                item["SubmissionTimeString"] = String.Concat(submissionTime.ToShortDateString(), " ", submissionTime.ToShortTimeString());
                item["CompletionTimeString"] = String.Concat(completionTime.ToShortDateString(), " ", completionTime.ToShortTimeString());
                item["Style"] = String.Empty;

                if (submissionTime == completionTime)
                    item["CompletionTimeString"] = String.Empty;

                switch (item["CurrentState"].ToString().ToLower())
                {
                    case "completed":
                        item["Icon"] = "/Orion/PM/images/TaskStates/Status_Completed.png";
                        break;
                    case "completedwitherrors":
                        item["Icon"] = "/Orion/PM/images/TaskStates/Status_CompletedWithErrors.png";
                        break;
                    case "failed":
                        item["Icon"] = "/Orion/PM/images/TaskStates/Status_Failed.png";
                        item["Style"] = "background-color: #facecf; color: #ce0000;";
                        break;
                    case "stopped":
                        item["Icon"] = "/Orion/PM/images/TaskStates/Status_Stopped.png";
                        break;
                    case "running":
                        item["Icon"] = "/Orion/PM/images/TaskStates/Status_Running.png";
                        break;
                    default:
                        item["Icon"] = "/Orion/PM/images/TaskStates/Status_Unknown.png";
                        break;
                }
            }
        }
        catch (Exception ex)
        {
            this.errorPanel.Visible = true;
            this.errorPanel.InnerHtml = ResourceHelper.GetErrorMessageHtml(new ErrorInspectorDetails() { Title = ex.GetType().Name, Error = ex });
            return;
        }
        this.resourceTable.DataSource = table;
        this.resourceTable.DataBind();
    }
}
