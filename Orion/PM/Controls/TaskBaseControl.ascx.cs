﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.PM.Web.DAL;
using System.Data;
using System.Text;
using SolarWinds.PM.Web.AjaxTree.Json;
using System.IO;
using SolarWinds.Logging;
using SolarWinds.PM.Web.Charting;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using System.Web.Script.Serialization;
using SolarWinds.PM.Web.Helper;
using SolarWinds.PM.Web.Resources;

public partial class Orion_SCM_Controls_TaskBaseControl : UserControl
{
    private readonly string LoadingCaption = "";//" Loading content...";

    private System.Web.UI.HtmlControls.HtmlGenericControl messageDiv;
    private System.Web.UI.HtmlControls.HtmlGenericControl contentDiv;

    private Dictionary<TaskExecutionEntity, Control> controls = new Dictionary<TaskExecutionEntity, Control>();

    public List<TaskExecutionEntity> ExecutionList { get; set; }
    public List<TaskExecutionEntity> RefreshExecutionList { get; set; }
    public String RefreshClientScript { get { return String.Format("javascript:{0}_RefreshClick();", this.ClientID); } }
    public Boolean VisibleControls { get { return TaskDiv.Visible; } set { TaskDiv.Visible = value; } }

    protected String _ExecutionList { get { return FormatArray(this.ExecutionList.Distinct().Select(s => s.ToJson()).ToArray()); } }
    protected String _RefreshExecutionList { get { return FormatArray(this.RefreshExecutionList.Distinct().Select(s => s.ToJson()).ToArray()); } }
    protected String _MessageContainer { get { return messageDiv == null ? string.Empty : messageDiv.ClientID; } }
    protected String _ContentContainer { get { return contentDiv == null ? string.Empty : contentDiv.ClientID; } }

    protected String TimeOutMessage { get { return ResourceHelper.GetTimeOutErrorMessageHtml().Replace("\r", "").Replace("\n", ""); } }
    protected String ErrorMessage { get { return ResourceHelper.GetErrorMessageHtml("errortemplate").Replace("\r", "").Replace("\n", ""); } }


    public Orion_SCM_Controls_TaskBaseControl()
    {
        this.ExecutionList = new List<TaskExecutionEntity>();
        this.RefreshExecutionList = new List<TaskExecutionEntity>();
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        foreach (var item in controls)
        {
            if (item.Value is ScmResourceBaseAsync && (item.Value as ScmResourceBaseAsync).IsDone)
                this.ExecutionList.Remove(item.Key);
        }

        btnRefresh.Visible = !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
        refreshSpan.Visible = this.RefreshExecutionList.Count != 0;
        this.Visible = this.RefreshExecutionList.Count != 0 || this.ExecutionList.Count != 0;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Parent.NamingContainer is ASP.orion_resourcewrapper_ascx)
        {
            var res = this.Parent.NamingContainer as ASP.orion_resourcewrapper_ascx;

            messageDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("div") { ID = "MessageContainer" };
            contentDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("div") { ID = "ContentContainer" };

            Int32 count = res.Content.Controls.Count;
            for (int i = 0; i < count; i++)
                contentDiv.Controls.Add(res.Content.Controls[0]);

            res.Content.Controls.Add(messageDiv);
            res.Content.Controls.Add(contentDiv);
        }

        lblLoading.Text = LoadingCaption;
        //btnRefresh.LocalizedText = CommonButtonType.Refresh;
        btnRefresh.NavigateUrl = this.RefreshClientScript;
        btnRefresh.ImageUrl = "/Orion/PM/images/RefreshBlueIco.png";

        foreach (var item in this.ExecutionList.Union(this.RefreshExecutionList))
        {
            if (item.ServerMethod != null && item.HtmlServerMethod != null)
            {
                string message = "You can specify only one server method";
                log.Error(message);
                throw new InvalidOperationException(message);
            }

            if (item.ClientObject != null && String.IsNullOrEmpty(item.ClientObject.ID))
                item.ClientObject.ID = "dataDiv";
        }

        foreach (var item in this.ExecutionList.Where(w => w.HtmlServerMethod != null))
        {
            if (item.EnableImmediateRendering && item.ClientObject != null)
                item.ClientObject.InnerHtml = item.HtmlServerMethod(item.Parameters, null);
        }
        foreach (var item in this.ExecutionList.Where(w => w.ServerResourceControl != null))
        {
            if (item.EnableImmediateRendering && item.ClientObject != null)
            {
                var obj = LoadControl(item.ServerResourceControl.ServerResourceURL);

                foreach (var propertyItem in item.ServerResourceControl.Properties)
                    PropertySetter.SetProperty(obj, propertyItem.Key, propertyItem.Value);

                PropertySetter.SetProperty(obj, "AllowAdmin", Profile.AllowAdmin);
                PropertySetter.SetProperty(obj, "PreLoadMethodSerial", String.Concat(item.ServerResourceControl.PreLoadMethod.Target.GetType().AssemblyQualifiedName, ";", item.ServerResourceControl.PreLoadMethod.Method.Name));
                PropertySetter.SetProperty(obj, "ParametersSerial", Serializer.Serialize(item.ServerResourceControl.Parameters));
                controls.Add(item, obj);

                item.ClientObject.Controls.Add(obj);
            }
        }
    }

    protected string FormatArray(object[] obj)
    {
        StringBuilder sb = new StringBuilder();
        JavaScriptSerializer js = new JavaScriptSerializer();
        return js.Serialize(obj);
    }

    private static readonly Log log = new Log();
}