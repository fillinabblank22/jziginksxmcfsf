﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InsufficientSetupInfo.ascx.cs"
    Inherits="Orion_SCM_Controls_InsufficientSetupInfo" %>

<orion:resourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="false">
    <Content>
        <div runat="server" id="dataDiv"></div>
    </Content>
</orion:resourceWrapper>
