﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.PM.Web.Helper;

public partial class Orion_SCM_Controls_InsufficientSetupInfoDecryptingPasswordProblem : BaseResourceControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.dataDiv.InnerHtml = ResourceHelper.GetInsufficientSetupInfoDecryptingPasswordProblem(Profile.AllowAdmin);
    }

    protected override string DefaultTitle
    {
        get { return "InsufficientSetupInfo"; }
    }
}