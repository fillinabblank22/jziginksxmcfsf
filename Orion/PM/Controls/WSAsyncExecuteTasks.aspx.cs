﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.IO;
using System.Web.Script.Serialization;
using SolarWinds.PM.Web.Helper;
using SolarWinds.Orion.Web;
using SolarWinds.PM.Common.Model;
using SolarWinds.PM.Common.Enums;
using System.Data;
using SolarWinds.Logging;
using System.Text;
using Amib.Threading;
using SolarWinds.PM.Web.Resources;
using SolarWinds.PM.Web.DAL;
using System.Web.Services;

public partial class Orion_PM_Controls_WSAsyncExecuteTasks : System.Web.UI.Page
{
    private class JSTaskItem
    {
        public string ResourceId { get; set; }
        public string Hash { get; set; }
        public string ServerMethod { get; set; }
        public String ServerControlDefinition { get; set; }
        public object[] Parameters { get; set; }
    }

    private class JSReturnItem
    {
        public Dictionary<String, object> Data { get; set; }
        public String ResourceId { get; set; }
        public String Hash { get; set; }
        public Boolean IsDone { get; set; }

        public JSReturnItem(JSTaskItem item, Boolean isDone, Dictionary<String, object> data)
        {
            this.ResourceId = item.ResourceId;
            this.Hash = item.Hash;
            this.Data = data;
            this.IsDone = isDone;
        }

        public JSReturnItem(ControlsReturnItem item, Boolean isDone, Dictionary<String, object> data)
        {
            this.ResourceId = item.ResourceId;
            this.Hash = item.Hash;
            this.Data = data;
            this.IsDone = isDone;
        }
    }

    private class ControlsReturnItem
    {
        public ScmResourceBaseAsync DataControl { get; set; }
        public String ResourceId { get; set; }
        public String Hash { get; set; }
        public String ControlName { get; set; }

        public ControlsReturnItem(JSTaskItem item, ScmResourceBaseAsync dataControl, string controlName)
        {
            this.ResourceId = item.ResourceId;
            this.Hash = item.Hash;
            this.DataControl = dataControl;
            this.ControlName = controlName;
        }
    }

    private JSTaskItem[] JSONData { get; set; }
    private List<JSReturnItem> Return { get; set; }
    private List<ControlsReturnItem> ControlsReturn { get; set; }

    private DateTime beginTime = DateTime.Now;

    private EWDataGridEntity ServerState { get; set; }

    protected override void OnInit(EventArgs e)
    {
        try
        {
            log.Debug("Start to async load page resources");

            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            var serializer = new JavaScriptSerializer();
            var reader = new StreamReader(Page.Request.InputStream, Encoding.UTF8, true);
            var data = reader.ReadToEnd();

            this.JSONData = serializer.Deserialize<JSTaskItem[]>(data);
            this.Return = new List<JSReturnItem>();
            this.ControlsReturn = new List<ControlsReturnItem>();

            var dt = DateTime.Now;
            this.ServerState = new CheckEWDataGridAvailabilityDAL().CheckAvailability(false);
            log.DebugFormat("Check Service Availability took {0}ms", (DateTime.Now - dt).TotalMilliseconds);
        }
        catch (Exception ex)
        {
            foreach (var item in this.JSONData)
            {
                this.Return.Add(new JSReturnItem(item, true, new Dictionary<String, object>()
                                {
                                    { "Result", null },
                                    { "DataResultState", "JustMessage" },
                                    { "Message", ResourceHelper.GetErrorMessageHtml(new ErrorInspectorDetails() { Title = ex.GetType().Name, Error = ex }) },
                                }));
            }
            return;
        }

        foreach (var item in this.JSONData)
            ExecuteItem(item);

        base.OnInit(e);
    }

    private void ExecuteItem(JSTaskItem item)
    {
        if (item.ServerControlDefinition != null)
        {
            try
            {
                var contorlDefinition = System.Web.HttpUtility.UrlDecode(item.ServerControlDefinition);
                var contorlDefinitionSplitted = contorlDefinition.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

                var parameters = new Dictionary<String, String>();
                foreach (var query in contorlDefinitionSplitted)
                {
                    var name = query.Substring(0, query.IndexOf("="));
                    var value = query.Substring(name.Length + 1);

                    parameters.Add(name, value);
                }

                var obj = LoadControl(parameters["Control"]);
                if (obj is ScmResourceBaseAsync)
                {
                    dataDiv.Controls.Add(obj);

                    foreach (var propertyItem in parameters.Where(s => s.Key.Contains("config.")))
                        PropertySetter.SetProperty(obj, propertyItem.Key.Replace("config.", ""), propertyItem.Value);

                    PropertySetter.SetProperty(obj, "AllowAdmin", Profile.AllowAdmin);
                    PropertySetter.SetProperty(obj, "ServerState", this.ServerState);

                    this.ControlsReturn.Add(new ControlsReturnItem(item, obj as ScmResourceBaseAsync, parameters["Control"]));
                }
                else
                {
                    this.Return.Add(new JSReturnItem(item, true, new Dictionary<String, object>()
                                {
                                    { "Result", null },
                                    { "DataResultState", "JustMessage" },
                                    { "Message", ResourceHelper.GetErrorMessageHtml(new ErrorInspectorDetails() { Title = "Security Exception", Error = new Exception("Unable to load async control, because it is not secure.") }) },
                                }));
                }
            }
            catch (Exception ex)
            {
                this.Return.Add(new JSReturnItem(item, true, new Dictionary<String, object>()
                                {
                                    { "Result", null },
                                    { "DataResultState", "JustMessage" },
                                    { "Message", ResourceHelper.GetErrorMessageHtml(new ErrorInspectorDetails() { Title = "Unable to load resource", Error = ex }) },
                                }));
            }
        }
        else
        {
            var dt = DateTime.Now;

            try
            {
                var methodSubscription = item.ServerMethod.Split(';');
                string typeString = methodSubscription[0];
                string methodString = methodSubscription[1];

                var type = Type.GetType(typeString);
                var method = type.GetMethod(methodString);

                bool secureMethod = false;
                foreach (var attr in method.GetCustomAttributes(true))
                {
                    if (attr is WSAsyncExecuteMethodAttribute)
                    {
                        WSAsyncExecuteMethodAttribute attribute = attr as WSAsyncExecuteMethodAttribute;
                        if (attribute.IsSecureMethod)
                        {
                            secureMethod = true;
                            break;
                        }
                    }
                }

                if (!secureMethod)
                {
                    this.Return.Add(new JSReturnItem(item, true, new Dictionary<String, object>()
                                {
                                    { "Result", null },
                                    { "DataResultState", "JustMessage" },
                                    { "Message", ResourceHelper.GetErrorMessageHtml(new ErrorInspectorDetails() { Title = "Security Exception", Error = new Exception("Unable to execute method, because it is not secure.") }) },
                                }));
                }


                var instance = Activator.CreateInstance(type);

                var propDataResultState = type.GetProperty("DataResultState");

                var task = new TaskResult() { Outcome = TaskOutcome.Success };

                object result = null;
                var errorMessage = String.Empty;
                try
                {
                    result = method.Invoke(instance, new object[] { item.Parameters, this.ServerState });

                    if (result is DataTable)
                        EscapingChars(result as DataTable);
                }
                catch (Exception ex)
                {
                    errorMessage = ex.InnerException != null ? ex.InnerException.Message : String.Empty;

                    if (propDataResultState != null)
                        task = (TaskResult)propDataResultState.GetValue(instance, null);
                }

                if (propDataResultState != null)
                    task = (TaskResult)propDataResultState.GetValue(instance, null);

                switch (task.Outcome)
                {
                    case TaskOutcome.Unavailable:
                    case TaskOutcome.Error:
                        this.Return.Add(new JSReturnItem(item, true, new Dictionary<String, object>()
                            {
                                { "Result", null },
                                { "DataResultState", "JustMessage" },
                                { "Message", ResourceHelper.GetErrorMessageHtml(new ErrorInspectorDetails() { Title = String.Concat(errorMessage, " ", result is DataTable ? null : result).Trim(), Error = new Exception(task.ErrorMessage) }) },
                            }));
                        break;
                    case TaskOutcome.Pending:
                        this.Return.Add(new JSReturnItem(item, false, null));
                        break;
                    case TaskOutcome.Unreachable:
                        this.Return.Add(new JSReturnItem(item, true, new Dictionary<String, object>()
                            {
                                { "Result", null },
                                { "DataResultState", "JustMessage" },
                                { "Message", ResourceHelper.GetErrorMessageHtml(new ErrorInspectorDetails() { Title = "WSUS server is down. No data to display.", Error = new Exception(task.ErrorMessage) }) },
                            }));
                        break;
                    case TaskOutcome.UnreachableAll:
                        this.Return.Add(new JSReturnItem(item, true, new Dictionary<String, object>()
                            {
                                { "Result", null },
                                { "DataResultState", "JustMessage" },
                                { "Message", ResourceHelper.GetErrorMessageHtml(new ErrorInspectorDetails() { Title = "All WSUS servers are down. No data to display.", Error = new Exception(task.ErrorMessage) }) },
                            }));
                        break;
                    case TaskOutcome.Unauthorized:
                        this.Return.Add(new JSReturnItem(item, true, new Dictionary<String, object>()
                            {
                                { "Result", null },
                                { "DataResultState", "JustMessage" },
                                { "Message", ResourceHelper.GetInsuficientControl(Profile.AllowAdmin)},
                            }));
                        break;
                    case TaskOutcome.PartialSuccess:
                        this.Return.Add(new JSReturnItem(item, true, new Dictionary<String, object>()
                            {
                                { "Result", result },
                                { "DataResultState", "Message" },
                                { "Message", ResourceHelper.GetWarningMessageHtml(new ErrorInspectorDetails() { Title = "Resource displays just partial information", Error = new Exception(String.Concat("Some of WSUS servers are down.\n\n", task.ErrorMessage)) }) },
                            }));
                        break;
                    case TaskOutcome.Unknown:
                        this.Return.Add(new JSReturnItem(item, true, new Dictionary<String, object>()
                            {
                                { "Result", null },
                                { "DataResultState", "JustMessage" },
                                { "Message", ResourceHelper.GetErrorMessageHtml(new ErrorInspectorDetails() { Title = errorMessage, Error = new Exception("Unknown error") }) },
                            }));
                        break;
                    case TaskOutcome.Success:
                    default:
                        this.Return.Add(new JSReturnItem(item, true, new Dictionary<String, object>()
                            {
                                { "Result", result },
                                { "DataResultState", null },
                                { "Message", null },
                            }));
                        break;
                }
            }
            catch (Exception ex)
            {
                string message = ex.InnerException != null ? ex.InnerException.Message : String.Empty;
                log.Error(String.Format("Exception occured when tried to reflexing method [{0}] \r\nMessage: {1}", item.ServerMethod, message), ex);

                this.Return.Add(new JSReturnItem(item, true, new Dictionary<String, object>()
                    {
                        { "Result", null },
                        { "DataResultState", "JustMessage" },
                        { "Message", ResourceHelper.GetErrorMessageHtml(new ErrorInspectorDetails() { Title = "Exception occured when tried to reflexing method", Error = ex.InnerException }) },
                    }));
            }

            log.DebugFormat("Loading {0} took {1}ms", item.ServerMethod, (DateTime.Now - dt).TotalMilliseconds);
        }
    }

    public override void RenderControl(HtmlTextWriter writer)
    {
        foreach (var item in this.ControlsReturn)
        {
            if (item.DataControl.IsDone)
            {
                try
                {
                    StringBuilder sb = new StringBuilder();
                    using (StringWriter sw = new StringWriter(sb))
                    using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                    {
                        item.DataControl.RenderControl(htw);
                        this.Return.Add(new JSReturnItem(item, true, new Dictionary<String, object>()
                                {
                                    { "Result", sb.ToString() },
                                    { "DataResultState", null },
                                    { "Message", null },
                                }));
                    }

                    log.DebugFormat("Loading {0} took {1}ms", item.ControlName, item.DataControl.LoadingTime.TotalMilliseconds);
                }
                catch (Exception ex)
                {
                    this.Return.Add(new JSReturnItem(item, true, new Dictionary<String, object>()
                                {
                                    { "Result", null },
                                    { "DataResultState", "JustMessage" },
                                    { "Message", ResourceHelper.GetErrorMessageHtml(new ErrorInspectorDetails() { Title = "Unable to render resource", Error = ex }) },
                                }));
                }
            }
            else
            {
                this.Return.Add(new JSReturnItem(item, false, null));
            }
        }

        var serializer = new JavaScriptSerializer();
        writer.Write(serializer.Serialize(this.Return));

        log.DebugFormat("Entire page refresh took {0}ms", (DateTime.Now - this.beginTime).TotalMilliseconds);
    }

    private void EscapingChars(DataSet dataSet)
    {
        foreach (DataTable table in dataSet.Tables)
        {
            EscapingChars(table);
        }
    }

    private void EscapingChars(DataTable table)
    {
        for (Int32 i = 0; i < table.Rows.Count; i++)
        {
            for (Int32 j = 0; j < table.Columns.Count; j++)
            {
                if (table.Rows[i][j] is DataSet)
                {
                    EscapingChars(table.Rows[i][j] as DataSet);
                }
                else if (table.Rows[i][j] is DataTable)
                {
                    EscapingChars(table.Rows[i][j] as DataTable);
                }
                else
                {
                    try
                    {
                        table.Rows[i][j] = System.Web.HttpUtility.HtmlEncode(table.Rows[i][j].ToString());
                    }
                    catch (Exception ex)
                    {
                        throw new ArgumentException(String.Format("Data Type '{0}' is not implemented in /Orion/PM/Services/WsusTasks.asmx/EscapingChars. Error: {1}", table.Rows[i][j].GetType().Name, ex));
                    }
                }
            }
        }
    }

    private static readonly Log log = new Log();

}