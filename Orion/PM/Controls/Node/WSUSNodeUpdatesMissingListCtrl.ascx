<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WSUSNodeUpdatesMissingListCtrl.ascx.cs" Inherits="Orion_PM_Controls_Node_WSUSNodeUpdatesMissingListCtrl" %>

<div runat="server" ID="lblNoDataToDisplay"></div> 
<div runat="server" id="dataDiv">
    <asp:Repeater runat="server" ID="resourceTable">
        <HeaderTemplate>
    <table cellpadding = "0" cellspacing = "0">
    <tr>
        <td class="ReportHeader" colspan="2"><span style="white-space: nowrap;"><%= DefaultSanitizer.SanitizeHtml(this.UpdateNameCaption.ToUpperInvariant()) %></span></td>
        <td class="ReportHeader"><span style="white-space: nowrap;"><%= DefaultSanitizer.SanitizeHtml(this.UpdateCreationDateCaption.ToUpperInvariant()) %></span></td>
        <td class="ReportHeader"><span style="white-space: nowrap;"><%= DefaultSanitizer.SanitizeHtml(this.UpdateSeverityCaption.ToUpperInvariant()) %></span></td>
        <td class="ReportHeader"><span style="white-space: nowrap;"><%= DefaultSanitizer.SanitizeHtml(this.UpdateApprovalCaption.ToUpperInvariant()) %></span></td>
    </tr>
        </HeaderTemplate>
        <ItemTemplate>
    <tr>
	    <td class="<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "Class")) %>" style="padding-left: 3px; padding-right: 3px; border-width: 0px;"><img alt="Icon" src="<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "Picture")) %>" /></td>
	    <td class="<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "Class")) %>" style="padding-left: 3px; padding-right: 3px; border-width: 0px;"><a href="<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "NetObject")) %>"><%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "Title")) %></a></td>
	    <td class="<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "Class")) %>" style="padding-left: 3px; padding-right: 3px; border-width: 0px;"><span style="white-space: nowrap;"><%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "Date")) %></span></td>
	    <td class="<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "Class")) %>" style="padding-left: 3px; padding-right: 3px; border-width: 0px; color: <%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "SeverityColor")) %>"><%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "Severity")) %></td>
	    <td class="<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "Class")) %>" style="padding-left: 3px; padding-right: 3px; border-width: 0px;"><span style="white-space: nowrap;"><%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "Approval")) %></span></td>
    </tr>
        </ItemTemplate>
        <FooterTemplate>
    </table>
        </FooterTemplate>
    </asp:Repeater>
</div>