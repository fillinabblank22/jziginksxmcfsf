﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.PM.Web.Charting;
using SolarWinds.PM.Web.DAL;
using System.Data;
using SolarWinds.PM.Web.Resources;
using SolarWinds.PM.Web.NetObjects;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using SolarWinds.PM.Web.Resources.Node;
using SolarWinds.PM.Web.Helper;

public partial class Orion_PM_Controls_Node_WSUSNodeUpdatesSummaryCtrl : ScmResourceBaseAsync
{
    public Boolean JustApprovedUpdates { get; set; }
    public String NetObjectId { get; set; }
    public String DefaultTitle { get; set; }
    public String LegendHelpFragment { get; set; }

    private readonly string NoUpdatesToDisplayCaption = "No updates to display";

    private readonly string InstalledCaption = "Installed updates";
    private readonly string NeededCaption = "Needed updates";
    private readonly string UnknownCaption = "Unknown updates";
    private readonly string LegendTitleCaption = "Update Count";

    private readonly string UpdateNameLegendFieldCaption = "Update name";
    private readonly string WsusServerLegendFieldCaption = "Wsus server";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            lblNoDataToDisplay.InnerHtml = ResourceHelper.GetInfoMessageHtml(NoUpdatesToDisplayCaption);

            var detailsData = new WsusUpdatesSummary.DetailsData()
            {
                Installed = InstalledCaption,
                Needed = NeededCaption,
                Unknown = UnknownCaption,
                JustApprovedUpdates = this.JustApprovedUpdates,
            };
            var legendData = new WsusUpdatesBase.LegendData()
            {
                Title = this.DefaultTitle,
                HelpFragment = this.LegendHelpFragment,
                UpdateName = UpdateNameLegendFieldCaption,
                WsusServer = WsusServerLegendFieldCaption,
            };

            var table = new WsusUpdatesSummary().GetData(this.NetObjectId, detailsData, legendData);

            this.resourceTable.DataSource = table;
            this.resourceTable.DataBind();


            var chartInfo = SolarWinds.Orion.Web.Charting.ChartInfoFactory.Create("WSUSNodeUpdatesSummary") as WSUSNodeUpdatesSummaryChartInfo;
            chartInfo.Height = chartInfo.Width = 200;

            chartInfo.InstalledCount = Convert.ToInt32(table.Rows[0]["Value"]);
            chartInfo.UnknownCount = Convert.ToInt32(table.Rows[1]["Value"]);
            chartInfo.NeededCount = Convert.ToInt32(table.Rows[2]["Value"]);

            imgGraph.ChartInfo = chartInfo;

            var count = chartInfo.InstalledCount + chartInfo.NeededCount + chartInfo.UnknownCount;

            this.lblNodeCountCaption.Text = LegendTitleCaption;
            this.lblNodeCount.Text = count.ToString();

            dataDiv.Visible = count != 0;
            lblNoDataToDisplay.Visible = count == 0;
        }
        catch (Exception)
        {
            return;
        }
    }
}
