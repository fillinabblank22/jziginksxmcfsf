<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WSUSNodeUpdatesInstalledListTopXXCtrl.ascx.cs" Inherits="Orion_PM_Controls_Node_WSUSNodeUpdatesInstalledListTopXXCtrl" %>

<div runat="server" ID="lblNoDataToDisplay"></div> 
<div runat="server" id="dataDiv">
    <asp:Repeater runat="server" ID="resourceTable">
        <HeaderTemplate>
    <table cellpadding = "0" cellspacing = "0">
    <tr>
        <td class="ReportHeader" style="white-space: nowrap;" colspan="2"><%= DefaultSanitizer.SanitizeHtml(this.UpdateNameCaption.ToUpperInvariant()) %></td>
        <td class="ReportHeader" style="white-space: nowrap;"><%= DefaultSanitizer.SanitizeHtml(this.UpdateReleaseDateCaption.ToUpperInvariant()) %></td>
        <td class="ReportHeader" style="white-space: nowrap;"><%= DefaultSanitizer.SanitizeHtml(this.UpdateSourceCaption.ToUpperInvariant()) %></td>
    </tr>
        </HeaderTemplate>
        <ItemTemplate>
    <tr>
	    <td class="<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "Class")) %>" style="padding-left: 3px; padding-right: 3px; border-width: 0px;"><img alt="Icon" src="<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "Picture")) %>" /></td>
	    <td class="<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "Class")) %>" style="padding-left: 3px; padding-right: 3px; border-width: 0px;"><a href="<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "NetObject")) %>"><%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "Title")) %></a></td>
	    <td class="<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "Class")) %>" style="padding-left: 3px; padding-right: 3px; border-width: 0px;"><%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "ReleaseDate")) %></td>
	    <td class="<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "Class")) %>" style="padding-left: 3px; padding-right: 3px; border-width: 0px;"><%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "UpdateSource")) %></td>
    </tr>
        </ItemTemplate>
        <FooterTemplate>
    </table>
        </FooterTemplate>
    </asp:Repeater>
</div>