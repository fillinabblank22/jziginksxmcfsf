<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WSUSNodeDetailCtrl.ascx.cs" Inherits="Orion_PM_Controls_Node_WSUSNodeDetailCtrl" %>

<div runat="server" ID="lblNoDataToDisplay"></div> 
<div runat="server" id="divDetail">
    <table cellpadding="0" cellspacing="0" style="margin-top: 15px;">
<%if (this.Node.OrionNodeId == default(Int32?)) { %>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.StatusCaption) %></td>
            <td style="text-align: center;">&nbsp;<img alt="Icon" src="/Orion/images/StatusIcons/Unknown.gif" /></td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.UnmanagedNodeCaption) %></td>
        </tr>
<%} else { %>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.StatusCaption) %></td>
            <td style="text-align: center;">&nbsp;<img alt="Icon" src="<%= DefaultSanitizer.SanitizeHtml(String.Format("/Orion/images/StatusIcons/{0}", this.Node.OrionNodeStatusIcon)) %>" /></td>
            <td>&nbsp;<a href="<%= DefaultSanitizer.SanitizeHtml(String.Format("/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}", this.Node.OrionNodeId)) %>"><%= DefaultSanitizer.SanitizeHtml(this.GetStatusCaption) %></a></td>
        </tr>
<%} %>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.IpAddressCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.Node.IPAddress) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.MachineTypeCaption) %></td>
            <td style="text-align: center;">&nbsp;<img alt="Icon" src="<%= DefaultSanitizer.SanitizeHtml(this.Node.ComputerRole.ToLower().Contains("server") ? "/Orion/PM/images/device_16x16.gif" : "/NetPerfMon/images/Endpoints/endpoint.gif") %>" /></td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.Node.ComputerRole) %></td>
        </tr>
<%if (this.Node.WsusServer.IPAddress != null) { %>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.WsusServerIpAddressCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.Node.WsusServer.IPAddress) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.WsusServerNameCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.Node.WsusServer.Servername) %></td>
        </tr>
<%} %>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.LastReportedInventoryTimeCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(SolarWinds.PM.Web.Helper.ResourceHelper.CompareToMinDBValue(this.Node.LastReportedInventoryTime) ? "never" : this.Node.LastReportedInventoryTime.ToString()) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.LastSyncTimeCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(SolarWinds.PM.Web.Helper.ResourceHelper.CompareToMinDBValue(this.Node.LastSyncTime) ? "never" : this.Node.LastSyncTime.ToString()) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.LastSyncResultCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.Node.LastSyncResult) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.OSDescriptionCaption) %></td>
            <td style="text-align: center;">&nbsp;<img alt="Icon" src="<%= DefaultSanitizer.SanitizeHtml(this.Node.OSDescription.ToLower().Contains("microsoft") || this.Node.OSDescription.ToLower().Contains("windows") ? "/NetPerfMon/images/Vendors/311.gif" : "/NetPerfMon/images/Vendors/Unknown.gif") %>" /></td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.Node.OSDescription) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.OSArchitectureCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.Node.OSArchitecture) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.MakeCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.Node.Make) %></td>
        </tr>
        <tr>
            <td style="font-weight: bold;"><%= DefaultSanitizer.SanitizeHtml(this.ModelCaption) %></td>
            <td style="text-align: center;">&nbsp;</td>
            <td>&nbsp;<%= DefaultSanitizer.SanitizeHtml(this.Node.Model) %></td>
        </tr>
    </table>
</div>