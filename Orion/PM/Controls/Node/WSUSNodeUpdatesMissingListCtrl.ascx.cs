﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using System.Data;
using SolarWinds.PM.Web.DAL;
using System.Data.SqlClient;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.PM.Web.Resources;
using SolarWinds.PM.Web.NetObjects;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using SolarWinds.PM.Web.Resources.Node;
using SolarWinds.PM.Web.Helper;
using System.IO;

public partial class Orion_PM_Controls_Node_WSUSNodeUpdatesMissingListCtrl : ScmResourceBaseAsync
{
    public Boolean JustApprovedUpdates { get; set; }
    public Int32 MaxRecords { get; set; }
    public String NetObjectId { get; set; }

    private readonly string NoUpdatesToDisplayCaption = "No updates to display";

    protected readonly string UpdateNameCaption = "Update name";
    protected readonly string UpdateCreationDateCaption = "Date";
    protected readonly string UpdateSeverityCaption = "Severity";
    protected readonly string UpdateApprovalCaption = "Approval";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            lblNoDataToDisplay.InnerHtml = ResourceHelper.GetInfoMessageHtml(NoUpdatesToDisplayCaption);

            var dataDetails = new WsusUpdatesMissingList.DetailsData()
            {
                MaxRecord = this.MaxRecords,
                JustApprovedUpdates = this.JustApprovedUpdates,
            };

            var data = new WsusUpdatesMissingList().GetData(this.NetObjectId, dataDetails);

            this.resourceTable.DataSource = data;
            this.resourceTable.DataBind();

            dataDiv.Visible = data.Rows.Count != 0;
            lblNoDataToDisplay.Visible = data.Rows.Count == 0;
        }
        catch (Exception)
        {
            return;
        }
    }
}
