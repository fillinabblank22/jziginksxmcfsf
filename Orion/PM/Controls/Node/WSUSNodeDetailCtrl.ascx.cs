﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.PM.Web.Resources;
using SolarWinds.PM.Web.NetObjects;
using SolarWinds.PM.Web.Charting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Common;
using SolarWinds.PM.Web.Model.AsyncResourceModel;
using SolarWinds.Logging;
using SolarWinds.PM.Web.Model;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.PM.Web.Helper;

public partial class Orion_PM_Controls_Node_WSUSNodeDetailCtrl : ScmResourceBaseAsync
{
    public String NetObjectId { get; set; }

    private readonly string NoDataToDisplayCaption = "No data to display";

    protected readonly string IpAddressCaption = "IP Address";
    protected readonly string LastReportedInventoryTimeCaption = "Last Reported Inventory Time";
    protected readonly string LastSyncResultCaption = "Last Sync Result";
    protected readonly string LastSyncTimeCaption = "Last Sync Time";
    protected readonly string MachineTypeCaption = "Machine Type";
    protected readonly string MakeCaption = "Make";
    protected readonly string ModelCaption = "Model";
    protected readonly string OSArchitectureCaption = "OS Architecture";
    protected readonly string OSDescriptionCaption = "OS Description";
    protected readonly string WsusServerIpAddressCaption = "Wsus Server IP Address";
    protected readonly string WsusServerNameCaption = "Wsus Server Name";
    protected readonly string StatusCaption = "Node Status";
    protected readonly string UnmanagedNodeCaption = "Node is not managed by Orion";

    protected WsusNodeEntity Node { get; set; }

    protected String GetStatusCaption
    {
        get
        {
            string statusDescription = string.Empty;
            using (NodeChildStatusDAL dal = new NodeChildStatusDAL())
            {
                if (dal.GetChildStatusDescription(this.Node.OrionNodeId.Value, null, true, out statusDescription) == false)
                    statusDescription = this.Node.OrionStatusDescription;
            }

            return statusDescription;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Node = new WsusNodeEntity(this.NetObjectId);
        this.Node.RefreshData();

        lblNoDataToDisplay.InnerHtml = ResourceHelper.GetInfoMessageHtml(NoDataToDisplayCaption);

        divDetail.Visible = this.Node != null && this.Node.IPAddress != null;
        lblNoDataToDisplay.Visible = !divDetail.Visible;
    }
}