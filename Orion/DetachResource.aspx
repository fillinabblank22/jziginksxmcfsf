<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DetachResource.aspx.cs" Inherits="Orion_DetachResource" MasterPageFile="~/Orion/OrionMinReqs.master"  %>
<%@ Register TagPrefix="orion" TagName="NetObjectTips" Src="~/Orion/Controls/NetObjectTips.ascx" %>
<%@ Register TagPrefix="orion" TagName="Refresher" Src="~/Orion/Controls/Refresher.ascx" %>
<%@ Register TagPrefix="xui" TagName="Include" Src="~/Orion/Controls/XuiInclude.ascx" %>

<asp:Content ContentPlaceHolderID="HeadContent" runat="server">
    <orion:Include File="Resources.css" runat="server" />
    <orion:Include runat="server" File="NodeMNG.css"  />
    <orion:Include ID="Include1" runat="server" File="AsyncView.js" />
    <orion:Include runat="server" File="OrionCore.js" />
	<orion:Include runat="server" File="visibilityObserver.js" />
    <xui:Include runat="server"/>

    <style type="text/css">
        body {
            background: #dfe0e1;
            min-width: 0;
        }

		form.sw-app-region.sw-detached-resource {
            padding-top: 0;
       }
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="BodyContent" runat="server">
    <orion:Refresher ID="Refresher1" runat="server" />
	<orion:NetObjectTips ID="NetObjectTips1" runat="server" />
    <form id="form1" runat="server" class="sw-app-region sw-detached-resource">

		<asp:ScriptManager id="ScriptManager1" runat="server">
		</asp:ScriptManager>

        <orion:PageLinkGutter id="TopRightPageLinks" runat="server">
            <orion:IconLinkExportToPDF runat="server" />
        </orion:PageLinkGutter>

		<h1 id="ResourceTitle" runat="server"><%= DefaultSanitizer.SanitizeHtml(CommonWebHelper.EncodeHTMLTags(Page.Title)) %></h1>
        
        <div runat="server" id="divContainer">
        
        </div>
    </form>
</asp:Content>
