﻿using System;
using System.Globalization;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Wireless_Admin_EditThinAccessPointDetailsView : ProfilePropEditUserControl
{
    #region ProfilePropEditUserControl members
    public override string PropertyValue
    {
        get
        {
            return lbxThinAccessPointDetails.SelectedValue;
        }
        set
        {
            ListItem item = lbxThinAccessPointDetails.Items.FindByValue(value);
            if (null != item)
            {
                item.Selected = true;
            }
            else
            {
                //TODO default to "by device type"
                lbxThinAccessPointDetails.Items.FindByValue("-1").Selected = true;
            }
        }
    }
    #endregion

    protected override void OnInit(EventArgs e)
    {
        foreach (var info in ViewManager.GetViewsByType("WirelessThinAccessPointDetails"))
        {
            var item = new ListItem(info.ViewTitle, info.ViewID.ToString(CultureInfo.InvariantCulture));
            lbxThinAccessPointDetails.Items.Add(item);
        }

        base.OnInit(e);
    }
}