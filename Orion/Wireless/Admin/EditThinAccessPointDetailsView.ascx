﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditThinAccessPointDetailsView.ascx.cs" Inherits="Orion_Wireless_Admin_EditThinAccessPointDetailsView" %>

<asp:ListBox runat="server" ID="lbxThinAccessPointDetails" SelectionMode="single" Rows="1" >
    <asp:ListItem Text="<%$Resources:WirelessWebContent,NPMWEBDATA_AK1_5%>" Value="0" />
    <asp:ListItem Text="<%$Resources:WirelessWebContent,NPMWEBDATA_AK1_6%>" Value="-1" />
    
</asp:ListBox>
<%-- More ListItems will be populated at run-time --%>