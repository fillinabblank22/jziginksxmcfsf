﻿using System;
using System.Collections.Generic;
using System.Globalization;
using SolarWinds.Orion.NPM.Web.UI;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Wireless.Web;
using SolarWinds.Wireless.Web.NetObjects;

public partial class Orion_Wireless_ThinAccessPointDetailsView : OrionView
{
    protected override void OnInit(EventArgs e)
    {
        var thinAp = NetObject as ThinAccessPoint;
        if (thinAp == null)
            return;

        resHost.ThinAccessPoint = thinAp;
        resContainer.DataSource = ViewInfo;
        resContainer.DataBind();

        controller.Node = resHost.Node;
        controllerIcon.StatusLEDImageSrc = resHost.Node.Status.ToString("smallled", null);
        controllerIcon.EntityId = resHost.Node.NodeID.ToString(CultureInfo.InvariantCulture);
        controllerIcon.ViewLimitationId = ViewInfo.LimitationID;

        ap.Text = string.Format("{0}{1}", resHost.ThinAccessPoint.Name, Resources.WirelessWebContent.NPMWEBCODE_VT0_1);
        ap.NavigateUrl = WirelessResourceBase.CreateThinAPViewLink(thinAp.ID);

        base.OnInit(e);

        // Wireless Thin AP - NodeName Controller - APName AP
        Title = string.Format("{0} - {1} {2} - {3}", Resources.WirelessWebContent.WIRELESSWEBDATA_ThinAp, resHost.Node.Name, Resources.WirelessWebContent.WIRELESSWEBDATA_Controller, ap.Text);
    }

    public override string ViewType
    {
        get { return "WirelessThinAccessPointDetails"; }
    }

    protected void WirelessSiteMapPath_OnInit(object sender, EventArgs e)
    {
        if (!CommonWebHelper.IsBreadcrumbsDisabled)
        {
            var renderer = new NetObjectsSiteMapRenderer();
            renderer.SetUpData(new KeyValuePair<string, string>("NetObject", NetObject.NetObjectID));
            WirelessSiteMapPath.SetUpRenderer(renderer);
        }
    }
}