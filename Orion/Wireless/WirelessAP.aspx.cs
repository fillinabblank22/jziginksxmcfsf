﻿using System;
using System.Collections.Generic;
using System.Data;
using SolarWinds.Wireless.Web;
using SolarWinds.Orion.Web.InformationService;

public partial class Orion_Wireless_WirelessAP : System.Web.UI.Page
{
	#region URL properties
	private int NodeID
	{
		get
		{
			try
			{
				if (Request.QueryString["NetObject"] != null)
				{
					string[] parts = Request.QueryString["NetObject"].Split(':');

					if (parts.Length == 2)
						if (parts[0].Equals("N", StringComparison.InvariantCultureIgnoreCase))
							return Convert.ToInt32(parts[1]);

					return 0;
				}

				return 0;
			}
			catch
			{
				return 0;
			}
		}
	}

	private Int64 AccessPointID
	{
		get
		{
			try
			{
				return Convert.ToInt64(Request.QueryString["AccessPointID"]);
			}
			catch
			{
				return 0;
			}
		}
	}

	private Int64 RecordID
	{
		get
		{
			try
			{
				return Convert.ToInt64(Request.QueryString["RecordID"]);
			}
			catch
			{
				return 0;
			}
		}
	}

	private Int64 ThinAP
	{
		get
		{
			try
			{
				return Convert.ToInt64(Request.QueryString["ThinAP"]);
			}
			catch
			{
				return 0;
			}
		}
	}
	#endregion

	protected void Page_Load(object sender, EventArgs e)
	{
		Response.Redirect(BuildPageLink());
	}


	private string BuildPageLink()
	{
		var wheres = new List<string>();
		var swqlParams = new Dictionary<string, object>();


		if (NodeID > 0)
		{
			wheres.Add("A.NodeID = @nodeid");
			swqlParams["nodeid"] = NodeID;
		}

		if (AccessPointID > 0)
		{
			wheres.Add("A.ID = @apid");
			swqlParams["apid"] = AccessPointID;
		}


		if (RecordID > 0)
		{
			wheres.Add("A.Index = @recid");
			swqlParams["recid"] = RecordID;
		}

		if (ThinAP > 0)
		{
			wheres.Add("A.Index = @thinap");
			swqlParams["thinap"] = ThinAP;
		}

		using(var swql = InformationServiceProxy.CreateV3())
		{
			DataTable dt;
			if (wheres.Count == 0)
				dt = null;
			else
                dt = swql.Query("SELECT ID, NodeID, WirelessType FROM Orion.Packages.Wireless.AccessPoints A WHERE " + string.Join(" AND ", wheres), swqlParams);
			

			if (dt == null || dt.Rows.Count != 1)
				return "/Orion/";

            if (dt.Rows[0][2].Equals(0)) // WLAccessPoint.WirelessApType.Autonomous -- avoiding referencing Pollers
				return string.Format("/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}", dt.Rows[0]["NodeID"]);
		    
            return WirelessResourceBase.CreateThinAPViewLink(Convert.ToInt64(dt.Rows[0]["ID"]));
		}
	}
}
