using System;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using System.Collections.Generic;

public partial class Orion_Wireless_Summary : System.Web.UI.Page, IBypassAccessLimitation
{

    protected void Page_Load(object sender, EventArgs e)
    {
        //this.CustomPropertiesRepeater.DataSource = GetCustomProperties("Nodes");
        //this.CustomPropertiesRepeater.DataBind();
    }
    
	protected string[] GetCustomProperties(string table)
	{
        return new List<string>(CustomPropertyMgr.GetPropNamesForTable(table, false)).ToArray();
	}

    protected string[] GetCustomPropertiesTypes(string table)
    {
        List<string> types = new List<string>();
        foreach(string property in GetCustomProperties(table))
            types.Add(CustomPropertyMgr.GetTypeForProp(table, property).ToString());
        return types.ToArray();
    }
     
}
