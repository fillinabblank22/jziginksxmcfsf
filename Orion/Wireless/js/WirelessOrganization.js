﻿(function (wirelessOrganization) {

    function hideDropDown(dropDownOrganizationsId, valDropDownOrganizationsId) {
        $('#' + dropDownOrganizationsId).hide();
        $('#' + valDropDownOrganizationsId).hide();
        ValidatorEnable(document.getElementById(valDropDownOrganizationsId), false);
    }

    wirelessOrganization.onGetOrganizationListClick = function (apiKeyClientId, validationProgressId, dropDownOrganizationsId, apiKeyInputErrorContainerId, valDropDownOrganizationsId) {
        hideDropDown(dropDownOrganizationsId, valDropDownOrganizationsId);

        $('#' + apiKeyInputErrorContainerId).hide();
        var apiKey = $('#' + apiKeyClientId).val();
        if (!apiKey) {
            return true;
        }

        $('#' + validationProgressId).show();
        return true;
    };

    wirelessOrganization.onApiKeyChange = function(apiKeyInputErrorContainerId) {
        $('#' + apiKeyInputErrorContainerId).hide();
    };

}(SW.Core.namespace('SW.Wireless.Organization')));