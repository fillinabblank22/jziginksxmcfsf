﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Wireless.Common;
using SolarWinds.Wireless.Common.Models.Meraki;
using SolarWinds.Wireless.Web.DataProviders;
using SolarWinds.Wireless.Web.Meraki;

public partial class Orion_Wireless_Controls_MerakiPollingMethodSettings : UserControl, IMerakiAddNodeSettings
{
    IMerakiWirelessDataProvider dataProvider = new MerakiWirelessDataProvider();

    public string APIKey
    {
        get { return txtApiKey.Text; }
        set { txtApiKey.Text = value; }
    }

    public long OrganizationId
    {
        get
        {
            long organizationId;
            long.TryParse(dropDownOrganizations.SelectedValue, out organizationId);
            return organizationId;
        }
        set { dropDownOrganizations.SelectedValue = value.ToString(); }
    }

    public IEnumerable<Organization> Organizations
    {
        get
        {
            return ViewState[MerakiConstantStrings.OrganizationsViewStateKey] as IEnumerable<Organization>;
        }
        set
        {
            dropDownOrganizations.SelectedValue = null;

            IEnumerable<Organization> items = value;
            if (items != null && items.Count() > 1)
            {
                items = new[]
                {
                    new Organization() { Id = 0, Name = WirelessWebContent.MerakiChooseOrganization }
                }.Union(items);
            }
            
            ViewState[MerakiConstantStrings.OrganizationsViewStateKey] = value;
            dropDownOrganizations.DataSource = items;
            dropDownOrganizations.DataBind();
            dropDownOrganizations.Visible = value != null && value.Any();
            valDropDownOrganizations.Enabled = dropDownOrganizations.Visible;
        }
    }

    public void ShowValidationResult(string message)
    {
        bool showError = !string.IsNullOrEmpty(message);
        apiKeyInputErrorContainer.Visible = showError;

        if (showError)
            apiKeyErrorMessage.Text = message;
    }

    public void SetControlsState(bool enabled)
    {
        this.txtApiKey.Enabled = enabled;
        this.txtApiKey.CausesValidation = enabled;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        btnGetOrganizations.OnClientClick =
            $"return SW.Wireless.Organization.onGetOrganizationListClick('{txtApiKey.ClientID}'," +
            $" '{validationProgress.ClientID}', '{dropDownOrganizations.ClientID}'," +
            $" '{apiKeyInputErrorContainer.ClientID}', '{valDropDownOrganizations.ClientID}');";

        txtApiKey.Attributes["oninput"] =
            $"SW.Wireless.Organization.onApiKeyChange('{apiKeyInputErrorContainer.ClientID}');";
    }

    protected void btnGetOrganizations_OnClick(object sender, EventArgs e)
    {
        int? engineId = NodeWorkflowHelper.Node != null ? NodeWorkflowHelper.Node.EngineID : (int?) null;
        var organizationsResult = dataProvider.GetOrganizations(APIKey, engineId);

        if (!organizationsResult.Success)
        {
            Organizations = null;
            ShowValidationResult(WirelessWebContent.MerakiAPIKeyError);
            return;
        }

        Organizations = organizationsResult.Organizations;
    }
}