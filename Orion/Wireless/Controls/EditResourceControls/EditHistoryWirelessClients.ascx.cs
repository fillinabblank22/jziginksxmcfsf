using System;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web.UI;
using System.Collections.Generic;
using SolarWinds.Orion.Web.Charting;

public partial class Orion_NPM_Controls_EditResourceControls_EditHistoryWirelessClients : BaseResourceEditControl
{
	private Dictionary<string, object> _properties = new Dictionary<string, object>();

    private string GetLocalizedTimePeriod(string period)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey("Period", period);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds());
    }

    protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		_properties.Add("Period", string.Empty);

		List<string> plist = Periods.GenerateSelectionList();
        foreach (string period in plist)
        {
            Period.Items.Add(new ListItem(GetLocalizedTimePeriod(period),period));
        }

        if (Period.Items.FindByValue(Resource.Properties["Period"]) != null) 
            // check if there isn't any nonsense in db, as this could throw exception
        {
            Period.SelectedValue = Resource.Properties["Period"];
        }
	}

	public override System.Collections.Generic.Dictionary<string, object> Properties
	{
		get 
		{
			_properties["Period"] = this.Period.SelectedValue;
			return _properties;
		}
	}
}
