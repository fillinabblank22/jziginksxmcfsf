<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditHistoryWirelessClients.ascx.cs"
    Inherits="Orion_NPM_Controls_EditResourceControls_EditHistoryWirelessClients" %>
<table border="0">
    <tr>
        <td>
            <asp:Label runat="server" ID="label1" Text="<%$ Resources: WirelessWebContent, NPMWEBDATA_VB0_298%>" Font-Bold="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:DropDownList runat="server" ID="Period" />
        </td>
    </tr>
</table>
