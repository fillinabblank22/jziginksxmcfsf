﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MerakiPollingMethodSettings.ascx.cs" Inherits="Orion_Wireless_Controls_MerakiPollingMethodSettings" %>
 <orion:Include File="../Orion/Wireless/styles/wirelessMerakiSettings.css" runat="server"/>
 <orion:Include File="../Orion/Wireless/js/WirelessOrganization.js" runat="server"/>

<div id="MerakiPlugin" class="contentBlock">
    <table class="blueBox" style="padding-bottom: 5px">
        <tr>
            <td class="meraki-column-leftLabel">
                <asp:Label runat="server" Text="<%$ Resources: WirelessWebContent, MerakiAPIKey %>"/>
            </td>
            <td class="meraki-column-content">
                <asp:TextBox CssClass="meraki-text-apiKeyInput" ID="txtApiKey" runat="server" Width="280" Height="18"/> 
            </td>
            <td>  
                <asp:RequiredFieldValidator ID="valApiKey" CssClass="meraki-error" ControlToValidate="txtApiKey" EnableClientScript="True" ErrorMessage="<%$ Resources: WirelessWebContent, MerakiAPIKeyErrorBlank %>" runat="server"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2" class="meraki-column-content">
                <orion:LocalizableButton ID="btnGetOrganizations" runat="server" LocalizedText="CustomText" Text="<%$ Resources: WirelessWebContent, MerakiGetOrganizations%>"
                        OnClick="btnGetOrganizations_OnClick" DisplayType="Small" />
                <asp:DropDownList runat="server" ID="dropDownOrganizations" DataValueField="Id" DataTextField="Name" Visible="False">
                </asp:DropDownList>
                <span runat="server" id="validationProgress" style="display:none;" class="meraki-loading-progress">
                    <span class="sw-pg-hint-text"><%= Resources.WirelessWebContent.MerakiLoadingOrganizations %></span>
                </span>

                <div runat="server" ID="apiKeyInputErrorContainer" class="meraki-error" Visible="False" EnableViewState="false">
                    <asp:Label runat="server" ID="apiKeyErrorMessage"/>
                </div>
                <asp:RequiredFieldValidator ID="valDropDownOrganizations" CssClass="meraki-error" Enabled="False"
                    ControlToValidate="dropDownOrganizations" EnableClientScript="True"
                    ErrorMessage="<%$ Resources: WirelessWebContent, MerakiOrganizationRequiredField%>" InitialValue="0" 
                    runat="server"></asp:RequiredFieldValidator>                
           
            </td>
        </tr>
    </table>
    <table class="blueBox" style="padding-top: 0">
        <tr>
            <td class ="meraki-column-leftLabel"></td>
            <td class="rightInputColumn" style="display: inline-block;">
                <asp:Label runat="server" Text="<%$ Resources : WirelessWebContent, MerakiUseHTTPProxyLabel %>"/>
                <a href="/Orion/Admin/HttpProxySettings/Default.aspx" target="_blank" style="text-decoration: none">
                    <span class="meraki-text-httpProxyConfig"><%= Resources.WirelessWebContent.MerakiUseHTTPProxyConfig %></span>
                </a>
            </td>
        </tr>
    </table>


</div>