﻿<%@ Page Language="C#" MasterPageFile="~/Orion/View.master" AutoEventWireup="true" CodeFile="ThinAccessPointDetailsView.aspx.cs" Inherits="Orion_Wireless_ThinAccessPointDetailsView" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="wl" Assembly="SolarWinds.Wireless.Web" Namespace="SolarWinds.Wireless.Web.NetObjects" %>
<%@ Register TagPrefix="npm" TagName="NodeLink" Src="~/Orion/NetPerfMon/Controls/NodeLink.ascx" %>
<%@ Register TagPrefix="npm" TagName="IntegrationIcons" Src="~/Orion/NetPerfMon/Controls/IntegrationIcons.ascx" %>
<%@ Register TagPrefix="orion" TagName="StatusIconControl" Src="~/Orion/NetPerfMon/Controls/StatusIconControl.ascx" %>


<asp:Content runat="server" ContentPlaceHolderID="ViewPageTitle" ID="Content1">
    <%if (!CommonWebHelper.IsBreadcrumbsDisabled)
      { %>
    <br />
    <div style="margin-left: 10px">
        <orion:FileBasedDropDownMapPath runat="server" 
            ID="WirelessSiteMapPath" 
            Provider="WirelessSiteMapProvider" 
            SiteMapFilePath="\Orion\Wireless\Wireless.sitemap" 
            OnInit="WirelessSiteMapPath_OnInit" />
    </div>
    <%} %>

    <h1>
        <span style="float: right; padding-right: 30px;">
            <npm:IntegrationIcons ID="IntegrationIcons1" runat="server" NodeID='<%$ Code: this.NetObject["NodeID"] %>' />
        </span>
        <%= ViewInfo.IsSubView ? ViewInfo.ViewGroupTitle : ViewInfo.ViewHtmlTitle%> 
	    - 
        <orion:StatusIconControl ID="controllerIcon" runat="server" />
        <npm:NodeLink ID="controller" runat="server" />
        -
	    <asp:HyperLink ID="ap" runat="server" />
    </h1>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="MainContentPlaceHolder">
    <wl:ThinAccessPointResourceHost runat="server" ID="resHost">
        <orion:ResourceContainer runat="server" ID="resContainer" />
    </wl:ThinAccessPointResourceHost>
</asp:Content>
