<%@ Control Language="C#" ClassName="SearchNodes" Inherits="SolarWinds.Orion.Web.UI.BaseResourceControl" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>

<script runat="server">
    protected override void OnInit(EventArgs e)
    {
        InitNodeProps();

        txtSearchString.Attributes.Add("onkeydown", "if(event.keyCode==13){var e=jQuery.Event(event); e.stopPropagation(); e.preventDefault(); __doPostBack('" + btnSearch.UniqueID + "',''); return false; }");

        var script = string.Format("if($('#{0}').val()=='') return false;", txtSearchString.ClientID);
        btnSearch.Attributes.Add("onclick", script);
        
        base.OnInit(e);
    }

    private void InitNodeProps()
    {
        ListItemCollection properties = new ListItemCollection();
        lbxByWhatString.Items.Clear();
        lbxByWhatString.Items.Add(new ListItem(Resources.WirelessWebContent.NPMWEBDATA_VB0_120, "MAC Address"));
        lbxByWhatString.Items.Add(new ListItem(Resources.WirelessWebContent.NPMWEBCODE_VB0_84, "IP Address"));
        lbxByWhatString.Items.Add(new ListItem(Resources.WirelessWebContent.NPMWEBDATA_VB0_46, "SSID"));
        lbxByWhatString.Items.Add(new ListItem(Resources.WirelessWebContent.NPMWEBCODE_VB0_85, "Client Name"));
    }

    protected void SearchClick(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtSearchString.Text))
        {
            Response.Redirect(string.Format("/Orion/Wireless/Resources/WirelessSearchResults.aspx?Property={0}&SearchText={1}",
                                Server.UrlEncode(lbxByWhatString.SelectedValue), Server.UrlEncode(txtSearchString.Text))
                            );
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.WirelessWebContent.NPMWEBCODE_VB0_86; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceSearchWirelessClients";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Static; } }
</script>

<orion:ResourceWrapper runat="server">
    <Content>
        <table class="DefaultShading LayoutTable">
            <tr>
                <td>
                    <%=Resources.WirelessWebContent.NPMWEBDATA_VB0_205%>
                </td>
                <td>
                    <%=Resources.WirelessWebContent.NPMWEBDATA_VB0_206%>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox runat="server" ID="txtSearchString" Width="95%" Rows="1" />
                </td>
                <td>
                    <asp:ListBox runat="server" ID="lbxByWhatString" Rows="1" SelectionMode="Single"
                        Width="100%" />
                </td>
                <td>
                    <orion:LocalizableButton runat="server" ID="btnSearch" CssClass="resList" LocalizedText="CustomText" Text="<%$Resources: WirelessWebContent, NPMWEBDATA_VB0_218%>" DisplayType="Primary" OnClick="SearchClick" />
                </td>
            </tr>
            <tr>
                <td colspan="3" class="helpfulTextWithoutPadding">
                    <%=Resources.WirelessWebContent.NPMWEBDATA_VB0_207%>
                </td>
            </tr>
        </table>
    </Content>
</orion:ResourceWrapper>
