﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.HtmlControls;
using SolarWinds.Logging;
using SolarWinds.Wireless.Web.DisplayTypes;
using SolarWinds.Wireless.Web;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.Web.InformationService;
using i18nExternalized = Resources.WirelessWebContent;


public partial class Orion_Wireless_Resources_APs_ActiveClients : WirelessResourceBase
{
	private static readonly Log log = new Log();

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceActiveWirelessClients"; }
	}

	protected override string DefaultTitle
	{
		get { return i18nExternalized.NPMWEBCODE_VB0_139; }
	}

	protected override void LoadData()
	{

		var swqlParams = new Dictionary<string, object> { { "node", NodeID } };
		DataTable clientTable;
		
		// list of all devices with specific NodeID, in this case there should be just one device in the list
		using (var swis = InformationServiceProxy.CreateV3())
		{
			var apCount = Convert.ToInt32(swis.Query("SELECT COUNT(NodeId) as cnt FROM Orion.Packages.Wireless.AccessPoints.Autonomous WHERE NodeId=@node", swqlParams).Rows[0][0]);
			
			if (apCount != 1)
			{
				this.OutputTable.Visible = false;
				log.ErrorFormat("NPM Resource Error (Selected APs = {0})", apCount);
				return;
			}

			clientTable =
				swis.Query(@"
SELECT 
C.IPAddress, C.SSID, ISNULL(C.SignalStrength,0) AS C_SignalStrength, ToLocal(C.FirstUpdate) as FirstUpdate, C.OutDataRate, ISNULL(C.InTotalBytes,0) AS C_InTotalBytes, ISNULL(C.OutTotalBytes,0) AS C_OutTotalBytes
FROM Orion.Packages.Wireless.Clients C
WHERE C.NodeID=@node
ORDER BY InterfaceID", swqlParams);
		}
		var icon = GetIconForDeviceType(PollerDeviceType.WirelessAutonomous);
		if (clientTable.Rows.Count > 0)
		{
			foreach (DataRow c in clientTable.Rows)
			{
				var row = new HtmlTableRow();

				row.Cells.Add(NewTableCell(icon));
				row.Cells.Add(NewTableCell(c["IPAddress"]));
				row.Cells.Add(NewTableCell(ValueFormatter.EmptyToNotAvailable(c["SSID"])));
                row.Cells.Add(NewTableCell(new SignalStrength(c["C_SignalStrength"]), "center"));
				row.Cells.Add(NewTableCell(c["FirstUpdate"]));
				row.Cells.Add(NewTableCell(GetTxRateString(c, "OutDataRate", Resources.WirelessWebContent.NPMWEBCODE_VB0_12), "right"));
                row.Cells.Add(NewTableCell(new Bytes(c["C_OutTotalBytes"]).ToShortString(), "right"));
                row.Cells.Add(NewTableCell(new Bytes(c["C_InTotalBytes"]).ToShortString(), "right"));

				OutputTable.Rows.Add(row);
			}
		}
		else
		{
			this.OutputTable.Visible = false;
			this.ErrorString.Text = i18nExternalized.NPMWEBCODE_VB0_141;
			this.ErrorString.Visible = true;
			return;
		}
	}

}
