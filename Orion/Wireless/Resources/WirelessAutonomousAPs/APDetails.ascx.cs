using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.HtmlControls;
using SolarWinds.Logging;
using SolarWinds.Wireless.Web;
using SolarWinds.Orion.Web.InformationService;
using i18nExternalized = Resources.WirelessWebContent;

public partial class Orion_Wireless_Resources_APs_APDetails : WirelessResourceBase
{
	private static readonly Log log = new Log();

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceAutonomousAccessPointDetails"; }
	}

	protected override string DefaultTitle
	{
		get { return i18nExternalized.NPMWEBCODE_VB0_152; }
	}

	private void AddOutputRow(string label, string val)
	{
		var row = new HtmlTableRow();
		row.Cells.Add(CreateTableCell("PropertyHeader NPM_PropertyHeader", label));
		row.Cells.Add(CreateTableCell("Property", val));

		OutputTable.Rows.Add(row);
	}

	private void AddOutputRow(string label, DataRow drow, string key)
	{
		if (drow.IsNull(key))
			return;

		var val = drow[key].ToString().Trim();
		if (string.IsNullOrEmpty(val))
			return;

		AddOutputRow(label, val);
	}

	protected override void LoadData()
	{
		var swqlParams = new Dictionary<string, object> { { "node", NodeID } };
		DataRow ap;
		DataRow firstInterface;
		DataTable ssids;

		using (var swql = InformationServiceProxy.CreateV3())
		{
			var list = swql.Query(@"
SELECT N.NodeDescription, A.IPAddress FROM Orion.Packages.Wireless.AccessPoints.Autonomous A 
INNER JOIN Orion.Nodes N ON N.NodeID=A.NodeID
WHERE A.NodeID=@node", swqlParams);

			if (list.Rows.Count != 1)
			{
				this.OutputTable.Visible = false;
				log.Error("No AP in database yet");
				return;
			}

			ap = list.Rows[0];

			var interfaces =
				swql.Query(
					@"
SELECT TOP 1 RadioType, Channel as CurrentChannel, AutoChannel, WEPEnabled, BeaconPeriod
FROM Orion.Packages.Wireless.Interfaces
WHERE NodeID=@node",
					swqlParams);

			ssids = swql.Query("SELECT DISTINCT SSID FROM Orion.Packages.Wireless.Interfaces WHERE NodeID=@node ORDER BY SSID", swqlParams);

			if (interfaces.Rows.Count == 0)
			{
				this.OutputTable.Visible = false;
				return;
			}

			firstInterface = interfaces.Rows[0];
		}


		AddOutputRow(i18nExternalized.NPMWEBCODE_VB0_153, ap, "NodeDescription");
		AddOutputRow(i18nExternalized.NPMWEBCODE_VB0_84, ap, "IPAddress");

		AddOutputRow(i18nExternalized.NPMWEBDATA_VB0_293, firstInterface, "RadioType");

		AddOutputRow(i18nExternalized.NPMWEBCODE_VB0_155,
			string.Join("<br />", ssids.Rows.Cast<DataRow>().Select(x => x[0].ToString()))
		);

		AddOutputRow(i18nExternalized.NPMWEBDATA_VB0_292, firstInterface, "CurrentChannel");
		AddOutputRow(i18nExternalized.NPMWEBCODE_VB0_163, firstInterface, "AutoChannel");
		AddOutputRow(i18nExternalized.NPMWEBCODE_VB0_170, firstInterface, "BeaconPeriod");
		AddOutputRow(i18nExternalized.NPMWEBCODE_VB0_171, firstInterface, "WEPEnabled");
	}
}
