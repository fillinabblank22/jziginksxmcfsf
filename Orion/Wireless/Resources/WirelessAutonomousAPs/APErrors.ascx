<%@ Control Language="C#" AutoEventWireup="true" CodeFile="APErrors.ascx.cs" Inherits="Orion_Wireless_Resources_APs_APErrors" %>
<%@ Register TagPrefix="npm" TagName="NodeLink" Src="~/Orion/NetPerfMon/Controls/NodeLink.ascx" %>

<style type="text/css"> 
    .NPM_PropertyHeader
    {   
    	padding-left: 17px;
    	vertical-align: top;
    	width: 55%;
    }
</style>

<orion:resourceWrapper runat="server" ID="Wrapper" >
	<Content>
        <table id="OutputTable" border="0" cellpadding="2" cellspacing="0" width="100%" runat="server" class="NeedsZebraStripes">
        </table>
        
        <div style="padding: 5px 5px;">
	    <asp:Label ID="ErrorString" Visible="false" runat="server"></asp:Label>
	    </div>

    </Content>
</orion:resourceWrapper>
