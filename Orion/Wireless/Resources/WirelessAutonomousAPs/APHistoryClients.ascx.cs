using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.HtmlControls;
using SolarWinds.Wireless.Web.DisplayTypes;
using SolarWinds.Wireless.Web;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.Web.InformationService;


public partial class Orion_Wireless_Resources_WirelessControllers_APHistoryClients : WirelessResourceBase
{

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceHistoryWirelessClients"; }
	}

	protected override string DefaultTitle
	{
		get { return Resources.WirelessWebContent.NPMWEBCODE_VB0_135; }
	}

	private static string GetLocalizedTimePeriod(string period)
	{
		ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
		string key = manager.CleanResxKey("Period", period);
		return manager.SearchAll(key, manager.GetAllResourceManagerIds());
	}

	// overriden subtitle of resource
	public override string SubTitle
	{
		get
		{
			if (!string.IsNullOrEmpty(Resource.SubTitle.Trim()))
			{
				//Subtitle is user-defined
				return Resource.SubTitle;
			}
			else
			{
				// subtitle is a period
				string subTitle = GetLocalizedTimePeriod(Resource.Properties["Period"]);

				if (String.IsNullOrEmpty(subTitle))
				{
					Resource.Properties["Period"] = "Today";
					subTitle = GetLocalizedTimePeriod("Today");
				}

				return subTitle;
			}
		}
	}

	public override string DetachURL
	{
		get
		{
			try
			{
				return String.Format("{0}", base.DetachURL);
			}
			catch
			{
				return String.Empty;
			}
		}
	}

	public override string EditControlLocation
	{
		get
		{
            return "/Orion/Wireless/Controls/EditResourceControls/EditHistoryWirelessClients.ascx";
		}
	}

	protected override void LoadData()
	{
		// get period from resources
		string periodName = Resource.Properties["Period"];

		// if there is no valid period set "Today" as default
		if (String.IsNullOrEmpty(periodName)) periodName = "Today";

		// get period interval
		var periodBegin = new DateTime();
		var periodEnd = new DateTime();

		string periodTemp = periodName;
		Periods.Parse(ref periodTemp, ref periodBegin, ref periodEnd);

		DataTable clients;
		


		using (var swis = InformationServiceProxy.CreateV3())
		{
			var swqlParams = new Dictionary<string, object>
			                 	{
			                 		{"node", NodeID},
			                 		{"startPeriod", periodBegin},
			                 		{"endPeriod", periodEnd},
			                 	};
			clients =
				swis.Query(
					@"
SELECT
	cl.MACAddress,
	ToLocal(MIN(cl.FirstUpdate)) AS FirstUpdate,
	ToLocal(MAX(cl.LastUpdate)) AS LastUpdate,
	MAX(cl.IPAddress) AS IPAddress,
	MAX(cl.SSID) AS SSID,
	SUM(cl.CL_TotalBytesTxDiff) AS TotalBytesTxDiff,
	SUM(cl.CL_TotalBytesRxDiff) AS TotalBytesRxDiff,
	AVG(cl.CL_SignalStrength) AS SignalStrength
	
FROM 
(
	SELECT 
		cl.MAC as MACAddress,
		cl.FirstUpdate,
		cl.LastUpdate,
		cl.IPAddress,
		cl.SSID,
		cl.SignalStrength AS CL_SignalStrength,
		ISNULL(cl.InBytes,0) AS CL_TotalBytesRxDiff,
		ISNULL(cl.OutBytes,0) AS CL_TotalBytesTxDiff
	FROM Orion.Packages.Wireless.HistoricalClients cl
	WHERE (cl.NodeID = @node AND (ISNULL(cl.MAC,'') <> ''))
) AS cl
WHERE  (
(cl.LastUpdate >= @startPeriod AND cl.LastUpdate <= @endPeriod) OR 
(cl.FirstUpdate >= @startPeriod AND cl.FirstUpdate <= @endPeriod) OR 
(cl.FirstUpdate < @startPeriod AND cl.LastUpdate > @endPeriod)
)
GROUP BY cl.MACAddress
ORDER BY LastUpdate DESC
",
					swqlParams);
		}

		if (clients.Rows.Count > 0)
		{
			var icon = GetIconForDeviceType(PollerDeviceType.WirelessAutonomous);

			foreach (DataRow c in clients.Rows)
			{
				var row = new HtmlTableRow();


				row.Cells.Add(NewTableCell(icon));
				row.Cells.Add(NewTableCell(c["IPAddress"]));
				row.Cells.Add(NewTableCell(ValueFormatter.EmptyToNotAvailable(c["SSID"])));
				row.Cells.Add(NewTableCell(c["MACAddress"]));
				row.Cells.Add(NewTableCell(new SignalStrength(c["SignalStrength"]), "center"));
				row.Cells.Add(NewTableCell(c["FirstUpdate"]));
				row.Cells.Add(NewTableCell(c["LastUpdate"]));
				row.Cells.Add(NewTableCell(new Bytes(c["TotalBytesTxDiff"]).ToShortString(), "right"));
				row.Cells.Add(NewTableCell(new Bytes(c["TotalBytesRxDiff"]).ToShortString(), "right"));

				OutputTable.Rows.Add(row);
			}
		}
		else
		{
			this.OutputTable.Visible = false;
			this.ErrorString.Text = Resources.WirelessWebContent.NPMWEBCODE_VB0_138;
			this.ErrorString.Visible = true;
			return;
		}
	}
}
