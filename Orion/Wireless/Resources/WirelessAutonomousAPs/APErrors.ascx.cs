using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.HtmlControls;
using SolarWinds.Logging;
using SolarWinds.Wireless.Web;
using SolarWinds.Orion.Web.InformationService;
using i18nExternalized = Resources.WirelessWebContent;


public partial class Orion_Wireless_Resources_APs_APErrors : WirelessResourceBase
{
	private static readonly Log log = new Log();

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceWirelessErrors"; }
	}

	protected override string DefaultTitle
	{
		get { return i18nExternalized.NPMWEBCODE_VB0_142; }
	}

	private void AddOutputRow(string label, DataRow drow, string key)
	{
		decimal d = Convert.ToDecimal(drow[key]);
		var row = new HtmlTableRow();
		row.Cells.Add(CreateTableCell("PropertyHeader NPM_PropertyHeader", label));

		var cell = CreateTableCell("Property", string.Format(i18nExternalized.NPMWEBCODE_VB0_151, d));

		row.Cells.Add(cell);

		OutputTable.Rows.Add(row);

	}

	protected override void LoadData()
	{

		var swqlParams = new Dictionary<string, object> { { "node", NodeID } };
		DataRow row;
		using (var swis = InformationServiceProxy.CreateV3())
		{
			var apCount = Convert.ToInt32(swis.Query("SELECT COUNT(NodeId) as cnt FROM Orion.Packages.Wireless.AccessPoints.Autonomous WHERE NodeId=@node", swqlParams).Rows[0][0]);
			var ifaceCount = Convert.ToInt32(swis.Query("SELECT COUNT(NodeId) as cnt FROM Orion.Packages.Wireless.Interfaces WHERE NodeId=@node", swqlParams).Rows[0][0]);
			if (apCount != 1)
			{
				this.OutputTable.Visible = false;
				return;
			}

			if (ifaceCount == 0)
			{
				this.OutputTable.Visible = false;
				this.ErrorString.Text = i18nExternalized.NPMWEBCODE_VB0_140;
				this.ErrorString.Visible = true;
				return;
			}

			row = swis.Query(
				@"
SELECT 
SUM(ISNULL(InAckFailure, 0))*60 AS RxAckFailurePerSec,
SUM(ISNULL(OutFailures, 0))*60 AS TxFailedPerSec,
SUM(ISNULL(InFCSError, 0))*60 AS RxFCSErrorPerSec
FROM Orion.Packages.Wireless.Interfaces
WHERE NodeId=@node",
				swqlParams).Rows[0];
		}

		AddOutputRow(i18nExternalized.NPMWEBCODE_VB0_147, row, "RxAckFailurePerSec");
		AddOutputRow(i18nExternalized.NPMWEBCODE_VB0_144, row, "TxFailedPerSec");
		AddOutputRow(i18nExternalized.NPMWEBCODE_VB0_150, row, "RxFCSErrorPerSec");


	}
}
