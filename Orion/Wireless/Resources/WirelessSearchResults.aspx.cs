﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.InformationService;

public partial class Orion_NetPerfMon_Resources_WirelessSearchResults : System.Web.UI.Page
{
	enum SearchMethod
	{
		Unknown,
		MACAddress,
		IPAddress,
		SSID,
		ClientName
	}


	private readonly string SearchMethodError = Resources.WirelessWebContent.NPMWEBCODE_AK0_60;
	private SearchMethod _method;
	private string _searchExpression;

	private string lastAPName = String.Empty;

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		if (!String.IsNullOrEmpty(Request.QueryString["SearchText"]))
		{
			this._searchExpression = Server.UrlDecode(Request.QueryString["SearchText"]);
		}

		string method = Request.QueryString["Property"];

		if (String.IsNullOrEmpty(method))
		{
			this._method = SearchMethod.Unknown;
			this.ErrorLabel.Text = SearchMethodError;
			this.ErrorMessage.Visible = true;
			this.Results.Visible = false;
			return;
		}

		method = Server.UrlDecode(method);

		var escapedSearchExpression = HttpUtility.HtmlEncode(this._searchExpression);
		this.TitleLabel.Text = String.Format(Resources.WirelessWebContent.NPMWEBCODE_AK0_61, escapedSearchExpression, GetMethodUIName(method));
		this._searchExpression = this._searchExpression.Replace("?", "_").Replace("*", "%");

		switch (method)
		{
			case "MAC Address": this._method = SearchMethod.MACAddress; return;
			case "IP Address": this._method = SearchMethod.IPAddress; return;
			case "SSID": this._method = SearchMethod.SSID; return;
			case "Client Name": this._method = SearchMethod.ClientName; return;
			default: this._method = SearchMethod.Unknown; break;
		}

		this.ErrorLabel.Text = SearchMethodError;
		this.ErrorMessage.Visible = true;
		this.Results.Visible = false;
	}

	private string GetMethodUIName(string name)
	{
		switch (name)
		{
			case "MAC Address": return Resources.WirelessWebContent.NPMWEBDATA_VB0_120;
			case "IP Address": return Resources.WirelessWebContent.NPMWEBCODE_VB0_84;
			case "SSID": return Resources.WirelessWebContent.NPMWEBDATA_VB0_46;
			case "Client Name": return Resources.WirelessWebContent.NPMWEBCODE_VB0_85;
			default: return Resources.WirelessWebContent.NPMWEBCODE_AK0_2;
		}
	}

	protected override void OnLoad(EventArgs e)
	{
		base.OnLoad(e);

		DataTable results = GetResultsTable(_method, _searchExpression);

		if (results != null && results.Rows.Count != 0)
		{
			results.Columns.Add("Url");

			string url = "/Orion/Wireless/WirelessAP.aspx?AccessPointID={0}";

			foreach (DataRow row in results.Rows)
			{
				row["Url"] = String.Format(url, row["APID"]);
			}

			this.ResultsRepeater.DataSource = results;
			this.ResultsRepeater.DataBind();
		}
	}

	private static DataTable GetResultsTable(SearchMethod method, string expression)
	{
		using (var swql = InformationServiceProxy.CreateV3())
		{
			var swqlParams = new Dictionary<string, object>();
			var sql = new StringBuilder(@"
SELECT 
    AP.Status AS ControllerStatus, AP.DisplayName AS APName, AP.IPAddress AS APIP,
    AP.ID AS APID, 

    CL.DisplayName AS ClientName, CL.IPAddress AS ClientIP, CL.MAC AS ClientMAC, CL.SSID AS ClientSSID, 
    ISNULL(AP.ControllerID, -1) AS ControllerID

FROM    Orion.Packages.Wireless.Clients CL
    INNER JOIN  Orion.Packages.Wireless.Interfaces I ON CL.InterfaceID = I.ID
    INNER JOIN  Orion.Packages.Wireless.AccessPoints AP ON I.AccessPointID = AP.ID
    INNER JOIN  Orion.Nodes N ON AP.NodeID = N.NodeID"
                                        );


			if (method != SearchMethod.Unknown && !String.IsNullOrEmpty(expression.Replace("%", "").Trim()))
			{
				string whereClause = " WHERE {0} LIKE @expr";

				switch (method)
				{
					case SearchMethod.ClientName: sql.AppendFormat(whereClause, "CL.DisplayName"); break;
					case SearchMethod.IPAddress: sql.AppendFormat(whereClause, "CL.IPAddress"); break;
					case SearchMethod.MACAddress: sql.AppendFormat(whereClause, "CL.MAC"); break;
					case SearchMethod.SSID: sql.AppendFormat(whereClause, "CL.SSID"); break;
				}

				swqlParams["expr"] = expression;
			}

			sql.Append(" ORDER BY APName ASC, ClientName ASC");

            DataTable result = swql.Query(sql.ToString(), swqlParams);

            result.Columns.Add("Classes", typeof(string));

            IEnumerable<DataRow> rows = result.Rows.OfType<DataRow>();
            var APWithClients = rows.GroupBy(k => k["APName"].ToString(),v => v["ClientName"].ToString());
            Dictionary<string, string> lastClients = new Dictionary<string, string>();
            foreach (var groupClients in APWithClients) 
            {
                string APName = groupClients.Key;
                string LastClient = groupClients.Last();
                lastClients.Add(APName, LastClient);
            }
            foreach (DataRow row in result.Rows)
            {
                string classes = string.Empty;
                string currentAP = row["APName"].ToString();
                string currentClient = row["ClientName"].ToString();
                if (lastClients[currentAP] == currentClient)
                {
                    classes = "LastClient";
                }

                row["Classes"] = classes;
                row["ClientMAC"] = FormatMacAddress(row["ClientMAC"].ToString());
            }
            
			return result;
		}
	}

	protected void ResultsRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
	{
		if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
		{
			DataRowView row = e.Item.DataItem as DataRowView;
			string apName = Convert.ToString(row["APName"]);

			if (apName != this.lastAPName)
			{
				(e.Item.FindControl("apRow") as PlaceHolder).Visible = true;
				this.lastAPName = apName;
			}

			var cType = Convert.ToInt32(row["ControllerID"]);
			int status = Convert.ToInt32(row["ControllerStatus"]);
			string file = "wireless_auto.gif";

			if (cType > 0)
			{
				switch (status)
				{
					case 0: file = "thin_unknown_16x16.gif"; break;
					case 1: file = "wireless_thin.gif"; break;
					case 2: file = "thin_down_16x16.gif"; break;
					case 9: file = "thin_unmanaged_16x16.gif"; break;
				}
			}
			else
			{


				switch (status)
				{
					case 0: file = "auto_unknown_16x16.gif"; break;
					case 1: file = "auto_up_16x16.gif"; break;
					case 2: file = "auto_down_16x16.gif"; break;
					case 9: file = "auto_unmanaged_16x16.gif"; break;
				}
			}

            ((HtmlImage)e.Item.FindControl("controllerIcon")).Src = String.Format("/Orion/Wireless/images/{0}", file);
		}
	}

    public static string FormatMacAddress(string source)
    {
        const int chunkSize = 4;
        const string delimiter = "-";
        int position = chunkSize;
        string result = source;
        while (position + chunkSize <= result.Length)
        {
            result = result.Substring(0,position) + delimiter + result.Substring(position);
            position += chunkSize + 1;
        }

        return result;
    }
}
