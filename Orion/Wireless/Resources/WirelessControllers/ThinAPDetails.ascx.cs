using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.HtmlControls;
using Resources;
using SolarWinds.Wireless.Web;
using SolarWinds.Orion.Web.InformationService;

public partial class Orion_Wireless_Resources_WirelessControllers_ThinAPDetails : WirelessThinAccessPointResourceBase
{
    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceThinAccessPointDetails"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.WirelessWebContent.NPMWEBCODE_VB0_148; }
    }

    private string GetSSID(DataRow i, InformationServiceProxy swis)
    {
        string ssid = i["SSID"].ToString();
        if (!string.IsNullOrWhiteSpace(ssid))
            // if exists AP's SSID, use that            
                return ssid;

        int clientsCnt = -1;
        if (Int32.TryParse(i["Clients"].ToString(), out clientsCnt) && (clientsCnt > 0))
        {
            var swqlParams = new Dictionary<string, object> { { "clientParentId", i["ID"] } };
            DataTable clients = swis.Query(@"
                                    select c.SSID, c.IPAddress
                                    from Orion.Packages.Wireless.Clients c
                                    where c.InterfaceID = @clientParentId
                                    ", swqlParams);


            // if exists client's SSID, use that
            foreach (DataRow c in clients.Rows)
            {
                if (c["IPAddress"].ToString().Equals("0.0.0.0"))
                    continue;
                string clientSSID = c["SSID"].ToString();
                if (!string.IsNullOrWhiteSpace(clientSSID))
                    return clientSSID;
            }
        }
	    return WirelessWebContent.WirelessNotAvailable;
    }

    protected override void LoadData()
    {
        var swqlParams = new Dictionary<string, object> { { "nodeid", NodeID }, { "thinap", ThinApIndex } };
                
        DataTable interfacesTable;
        using (var swis = InformationServiceProxy.CreateV3())
        {
            interfacesTable =
            swis.Query(
                @"
SELECT aps.IPAddress, i.MAC, i.SSID, i.Channel, i.RadioType, i.Clients, i.ID
FROM Orion.Packages.Wireless.Interfaces AS i
left join Orion.Packages.Wireless.AccessPoints.Thin aps ON aps.ID = i.AccessPointID
WHERE ((aps.NodeID = @nodeid) and (aps.Index = @thinap))
", swqlParams);
                        
            if (interfacesTable.Rows.Count == 0)
            {
                this.OutputTable.Visible = false;
                return;
            }
            
            foreach (DataRow i in interfacesTable.Rows)
            {
                var row = new HtmlTableRow();
                row.Cells.Add(NewTableCell(i["IPAddress"].ToString()));
                row.Cells.Add(NewTableCell(i["MAC"].ToString()));
                row.Cells.Add(NewTableCell(GetSSID(i, swis)));
                row.Cells.Add(NewTableCell(i["Channel"].ToString(), "right"));
                row.Cells.Add(NewTableCell(i["RadioType"].ToString(), "center"));
                row.Cells.Add(NewTableCell(i["Clients"].ToString(), "right"));

                OutputTable.Rows.Add(row);
            }
        }
    }
}
