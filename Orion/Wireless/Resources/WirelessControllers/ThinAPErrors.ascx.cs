using System;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;


public partial class Orion_Wireless_Resources_WirelessControllers_ThinAPErrors : ResourceControl
{
    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceWirelessErrors"; }  
    }

    protected override string DefaultTitle
    {
        get { return Resources.WirelessWebContent.NPMWEBCODE_VB0_142; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }

    public int NodeID
    {
        get
        {
            return ((INodeProvider)GetInterfaceInstance(typeof(INodeProvider))).Node.NodeID;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Visible = false;
    }


	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Static; } }
  
}
