<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ThinAPDetails.ascx.cs" Inherits="Orion_Wireless_Resources_WirelessControllers_ThinAPDetails" %>
<%@ Register TagPrefix="npm" TagName="NodeLink" Src="~/Orion/NetPerfMon/Controls/NodeLink.ascx" %>

<style type="text/css"> 
    .NPM_PropertyHeader
    {   
    	padding-left: 17px;
    	vertical-align: top;
    	width: 55%;
    }
</style>

<orion:resourceWrapper runat="server" ID="Wrapper" >
	<Content>
	
       <table id="OutputTable" border="0" cellpadding="2" cellspacing="0" width="100%" runat="server" class="NeedsZebraStripes">
            <tr>
	            <td class="ReportHeader"><%=Resources.WirelessWebContent.NPMWEBCODE_VB0_84%></td>
	            <td class="ReportHeader"><%=Resources.WirelessWebContent.NPMWEBDATA_VB0_291%></td>
	            <td class="ReportHeader"><%=Resources.WirelessWebContent.NPMWEBDATA_VB0_46%></td>
	            <td class="ReportHeader" align="right"><%=Resources.WirelessWebContent.NPMWEBDATA_VB0_292%></td>
	            <td class="ReportHeader" align="center"><%=Resources.WirelessWebContent.NPMWEBDATA_VB0_293%></td>
	            <td class="ReportHeader" align="right"><%=Resources.WirelessWebContent.NPMWEBDATA_VB0_294%></td>
	        </tr>
       </table>
        
       <div style="padding: 5px 5px;">
	   <asp:Label ID="ErrorString" Visible="false" runat="server"></asp:Label>
	   </div>

    </Content>
</orion:resourceWrapper>
