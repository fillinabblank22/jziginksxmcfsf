<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ThinAPHistoryClients.ascx.cs" Inherits="Orion_Wireless_Resources_WirelessControllers_ThinAPHistoryClients" %>

<orion:resourceWrapper runat="server" ID="Wrapper" >
	<Content>
        <table id="OutputTable" class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%" runat="server" >
            <tr>
	            <td class="ReportHeader">&nbsp;</td>
	            <td class="ReportHeader"><%=Resources.WirelessWebContent.NPMWEBCODE_VB0_84%></td>
	            <td class="ReportHeader"><%=Resources.WirelessWebContent.NPMWEBDATA_VB0_46%></td>
                <td class="ReportHeader"><%=Resources.WirelessWebContent.NPMWEBDATA_VB0_120%></td>
	            <td class="ReportHeader"><%=Resources.WirelessWebContent.NPMWEBDATA_VB0_286%></td>
	            <td class="ReportHeader"><%=Resources.WirelessWebContent.NPMWEBDATA_VB0_287%></td>
	            <td class="ReportHeader"><%=Resources.WirelessWebContent.NPMWEBDATA_VB0_295%></td>
	            <td class="ReportHeader" align="right"><%=Resources.WirelessWebContent.NPMWEBDATA_VB0_289%></td>
	            <td class="ReportHeader" align="right"><%=Resources.WirelessWebContent.NPMWEBDATA_VB0_290%></td>
	        </tr>
        </table>
        
       <div style="padding: 5px 5px;">
	   <asp:Label ID="MigrationString" Visible="false" runat="server"></asp:Label>       
	   <asp:Label ID="ErrorString" Visible="false" runat="server"></asp:Label>
	   </div>

    </Content>
</orion:resourceWrapper>
