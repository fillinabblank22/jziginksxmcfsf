using System;
using System.Linq;
using System.Web.UI.HtmlControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web.InformationService;
using System.Collections.Generic;
using System.Data;
using SolarWinds.Wireless.Web;
using SolarWinds.Wireless.Web.DisplayTypes;


public partial class Orion_Wireless_Resources_WirelessControllers_ThinAPHistoryClients : WirelessThinAccessPointResourceBase
{
	private static readonly Log log = new Log();

	private string GetLocalizedTimePeriod(string period)
	{
		ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
		string key = manager.CleanResxKey("Period", period);
		return manager.SearchAll(key, manager.GetAllResourceManagerIds());
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceHistoryWirelessClients"; }
	}

	protected override string DefaultTitle
	{
		get { return Resources.WirelessWebContent.NPMWEBCODE_VB0_135; }
	}

	// overriden subtitle of resource
	public override string SubTitle
	{
		get
		{
			if (!string.IsNullOrEmpty(Resource.SubTitle.Trim()))
			{
				//Subtitle is user-defined
				return Resource.SubTitle;
			}
			else
			{
				// subtitle is a period
				string subTitle = GetLocalizedTimePeriod(Resource.Properties["Period"]);

				if (String.IsNullOrEmpty(subTitle))
				{
					Resource.Properties["Period"] = "Today";
					subTitle = GetLocalizedTimePeriod("Today");
				}

				return subTitle;
			}
		}
	}

	public override string EditControlLocation
	{
		get
		{
			return "/Orion/Wireless/Controls/EditResourceControls/EditHistoryWirelessClients.ascx";
		}
	}

	// this function resolves netObjectType & netObjectID
	protected void GetCurrentNetObjectID(out string netObjectType, out int netObjectID)
	{
		netObjectType = String.Empty;
		netObjectID = -1;

		// if is netObject a Node set filter for a node
		INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();
		if (nodeProvider != null) { netObjectType = "N"; netObjectID = nodeProvider.Node.NodeID; }

		// if is netObject an Interface set filter for a interface
		IInterfaceProvider interfaceProvider = GetInterfaceInstance<IInterfaceProvider>();
		if (interfaceProvider != null) { netObjectType = "I"; netObjectID = interfaceProvider.Interface.InterfaceID; }

		// if is netObject a Volume set filter for a volume
		IVolumeProvider volumeProvider = GetInterfaceInstance<IVolumeProvider>();
		if (volumeProvider != null) { netObjectType = "V"; netObjectID = volumeProvider.Volume.VolumeID; }
	}

	protected override void LoadData()
	{
		// get period from resources
		string periodName = Resource.Properties["Period"];

		// if there is no valid period set "Today" as default
		if (String.IsNullOrEmpty(periodName)) periodName = "Today";

		// get period interval
		DateTime periodBegin = new DateTime();
		DateTime periodEnd = new DateTime();

		string periodTemp = periodName;
		Periods.Parse(ref periodTemp, ref periodBegin, ref periodEnd);

	    DataTable clients = LoadClients(periodBegin, periodEnd);

		var icon = GetIconForDeviceType(PollerDeviceType.WirelessController);

		if (clients != null && clients.Rows.Count > 0)
		{

			foreach (DataRow c in clients.Rows)
			{
				var row = new HtmlTableRow();

				row.Cells.Add(NewTableCell(icon));
				row.Cells.Add(NewTableCell(c["IPAddress"]));
				row.Cells.Add(NewTableCell(ValueFormatter.EmptyToNotAvailable(c["SSID"])));
				row.Cells.Add(NewTableCell(c["MACAddress"]));
				row.Cells.Add(NewTableCell(new SignalStrength(c["SignalStrength"]), "center"));
				row.Cells.Add(NewTableCell(c["FirstUpdate"]));
				row.Cells.Add(NewTableCell(c["LastUpdate"]));
				row.Cells.Add(NewTableCell(new Bytes(c["TotalBytesTxDiff"]).ToShortString(), "right"));
				row.Cells.Add(NewTableCell(new Bytes(c["TotalBytesRxDiff"]).ToShortString(), "right"));

				OutputTable.Rows.Add(row);
			}
		}
		else
		{
			this.OutputTable.Visible = false;
			this.ErrorString.Text = Resources.WirelessWebContent.NPMWEBCODE_VB0_138;
			this.ErrorString.Visible = true;
			return;
		}
	}

    private DataTable LoadClients(DateTime periodBegin, DateTime periodEnd)
    {
        DataTable clients = null;
        using (var swis = InformationServiceProxy.CreateV3())
        {
            var swqlParams = new Dictionary<string, object>
                             {
                                 {"nodeid", NodeID},
                                 {"thinap", ThinApIndex},
                                 {"startPeriod", periodBegin},
                                 {"endPeriod", periodEnd},
                             };

            var interfaceIndices = LoadInterfaces(swis, swqlParams);

            if (interfaceIndices.Any())
            {
                const string swql = @"
SELECT
	cl.MAC as MACAddress,
	ToLocal(MIN(cl.FirstUpdate)) AS FirstUpdate,
	ToLocal(MAX(cl.LastUpdate)) AS LastUpdate,
	MAX(cl.IPAddress) AS IPAddress,
	MAX(cl.SSID) AS SSID,
	SUM(ISNULL(cl.OutBytes,0)) AS TotalBytesTxDiff,
	SUM(ISNULL(cl.InBytes,0)) AS TotalBytesRxDiff,
	AVG(cl.SignalStrength) AS SignalStrength	
	FROM Orion.Packages.Wireless.HistoricalClients cl
WHERE (cl.NodeID = @nodeid AND cl.InterfaceIndex IN ({0}) AND (ISNULL(cl.MAC,'') <> ''))
AND (
(cl.LastUpdate >= @startPeriod AND cl.LastUpdate <= @endPeriod) OR 
(cl.FirstUpdate >= @startPeriod AND cl.FirstUpdate <= @endPeriod) OR 
(cl.FirstUpdate < @startPeriod AND cl.LastUpdate > @endPeriod)
)
GROUP BY cl.MAC
ORDER BY LastUpdate DESC
";
                clients =
                    swis.Query(string.Format(swql, string.Join(",", interfaceIndices))
                        , swqlParams); 
            }
        }
        return clients;
    }

    private static List<long> LoadInterfaces(InformationServiceProxy swis, Dictionary<string, object> swqlParams)
    {
        var interfaceIndices = new List<long>();
        var interfacesTable = swis.Query(
            @"SELECT Index FROM Orion.Packages.Wireless.Interfaces i
WHERE i.AccessPoint.Index = @thinap", swqlParams);

        foreach (var row in interfacesTable.AsEnumerable())
        {
            interfaceIndices.Add(long.Parse(row[0].ToString()));
        }
        return interfaceIndices;
    }
}

