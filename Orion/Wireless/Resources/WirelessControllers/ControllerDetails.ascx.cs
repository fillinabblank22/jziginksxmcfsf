using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.HtmlControls;
using SolarWinds.Wireless.Web;
using SolarWinds.Orion.Web.InformationService;


public partial class Orion_Wireless_Resources_WirelessControllers_ControllerDetails : WirelessResourceBase
{
    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceWirelessControllerDetails"; }  
    }

    protected override string DefaultTitle
    {
        get { return Resources.WirelessWebContent.NPMWEBCODE_VB0_130; }
    }
        
    private void AddTableRow(HtmlTable table, HtmlTableRow row)
    {
        if (row != null)
            table.Rows.Add(row);
    }


    private HtmlTableRow NewTableRow(string PropertyName, string PropertyValue)
    {
        // empty properties are not showed in table
        if(String.IsNullOrEmpty(PropertyValue.Trim())) 
            return null;

        var row = new HtmlTableRow();

        HtmlTableCell cell = new HtmlTableCell();
        cell.Attributes["class"] = "PropertyHeader NPM_PropertyHeader";
        cell.InnerHtml = PropertyName;
        row.Cells.Add(cell);

        cell = new HtmlTableCell();
        cell.Attributes["class"] = "Property";
        cell.InnerHtml = PropertyValue;
        row.Cells.Add(cell);

        return row;
    }

    protected override void LoadData()
    {
        var swqlParams = new Dictionary<string, object> { { "nodeid", NodeID } };

        DataTable controllerTable, rogueCnt, thinCnt;
        using (var swis = InformationServiceProxy.CreateV3())
        {
            controllerTable = swis.Query(@"
                                    SELECT co.ID, n.NodeDescription, co.IPAddress, ToLocal(co.LastUpdate) as LastUpdate
                                    FROM Orion.Packages.Wireless.Controllers AS co
                                    LEFT JOIN Orion.Nodes n ON n.NodeID = co.NodeID
                                    where (co.NodeID = @nodeid)
                                    ", swqlParams);

            if (controllerTable.Rows.Count != 1)
            {
                this.OutputTable.Visible = false;
                return;
            }
            
            foreach (DataRow row in controllerTable.Rows)
            {                
                var swqlCntParams = new Dictionary<string, object> { { "parentid", row["ID"] } };
                rogueCnt = swis.Query(@"
                        select count(ap.id) as cntR
                        from Orion.Packages.Wireless.Rogues ap 
                        where ap.ControllerID = @parentid
                        ", swqlCntParams);
                thinCnt = swis.Query(@"
                        select count(ap.id) as cntR
                        from Orion.Packages.Wireless.AccessPoints.Thin ap 
                        where ap.ControllerID = @parentid
                        ", swqlCntParams);


                // create table
                AddTableRow(OutputTable, NewTableRow(Resources.WirelessWebContent.NPMWEBCODE_VB0_131, row["NodeDescription"].ToString()));
                AddTableRow(OutputTable, NewTableRow(Resources.WirelessWebContent.NPMWEBCODE_VB0_84, String.Format("<a href='/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}'>{1}</a>", NodeID, row["IPAddress"].ToString())));
                AddTableRow(OutputTable, NewTableRow(Resources.WirelessWebContent.NPMWEBCODE_VB0_132, row["LastUpdate"].ToString()));
                AddTableRow(OutputTable, NewTableRow(Resources.WirelessWebContent.NPMWEBCODE_VB0_133, thinCnt.Rows[0][0].ToString() ));
                AddTableRow(OutputTable, NewTableRow(Resources.WirelessWebContent.NPMWEBCODE_VB0_134, rogueCnt.Rows[0][0].ToString()));
            }
        }
    }
}
