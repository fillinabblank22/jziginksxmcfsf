using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.HtmlControls;
using SolarWinds.Logging;
using SolarWinds.Wireless.Web.DisplayTypes;
using SolarWinds.Wireless.Web;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.Web.InformationService;

public partial class Orion_Wireless_Resources_WirelessControllers_ThinAPActiveClients : WirelessThinAccessPointResourceBase
{
	private static readonly Log log = new Log();

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceActiveWirelessClients"; }
	}

	protected override string DefaultTitle
	{
		get { return Resources.WirelessWebContent.NPMWEBCODE_VB0_139; }
	}

	protected override void LoadData()
	{
		var swqlParams = new Dictionary<string, object> { { "nodeid", NodeID }, { "thinap", ThinApIndex } };
		DataTable clientsTable;
		bool hasClients = false;

		using (var swis = InformationServiceProxy.CreateV3())
		{

			DataTable apsTable =
				swis.Query(
					@"select ID from Orion.Packages.Wireless.AccessPoints.Thin where Nodeid=@nodeid and Index=@thinap
                ",
					swqlParams);

			if (apsTable.Rows.Count == 1)
			{
				clientsTable =
					swis.Query(
						@"
                    SELECT c.IPAddress, c.SSID, c.SignalStrength, ToLocal(c.FirstUpdate) as FirstUpdate, c.OutDataRate, c.InTotalBytes, c.OutTotalBytes
                    FROM Orion.Packages.Wireless.Clients c
                    where c.WirelessInterface.AccessPointID = @accesspointid
                ",
						new Dictionary<string, object> { { "accesspointid", apsTable.Rows[0][0].ToString() } });

				var icon = GetIconForDeviceType(PollerDeviceType.WirelessController);

				foreach (DataRow c in clientsTable.Rows)
				{
					hasClients = true;

					var row = new HtmlTableRow();

					row.Cells.Add(NewTableCell(icon));
					row.Cells.Add(NewTableCell(c["IPAddress"].ToString()));
					row.Cells.Add(NewTableCell(ValueFormatter.EmptyToNotAvailable(c["SSID"])));
					row.Cells.Add(NewTableCell(new SignalStrength(c["SignalStrength"]).ToString(), "center"));
					row.Cells.Add(NewTableCell(c["FirstUpdate"].ToString()));
					row.Cells.Add(NewTableCell(GetTxRateString(c, "OutDataRate", Resources.WirelessWebContent.NPMWEBCODE_VB0_12), "right")); // N/A
					row.Cells.Add(NewTableCell(new Bytes(c["OutTotalBytes"]).ToShort1024String(), "right"));
					row.Cells.Add(NewTableCell(new Bytes(c["InTotalBytes"]).ToShort1024String(), "right"));

					OutputTable.Rows.Add(row);
				}
			}
		}

		if (!hasClients)
		{
			this.OutputTable.Visible = false;
			this.ErrorString.Text = Resources.WirelessWebContent.NPMWEBCODE_VB0_141;
			this.ErrorString.Visible = true;
			return;
		}

	}
}
