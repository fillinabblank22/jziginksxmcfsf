using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.HtmlControls;
using SolarWinds.Wireless.Web;
using SolarWinds.Orion.Web.InformationService;


public partial class Orion_Wireless_Resources_WirelessControllers_ThinAPsList : WirelessResourceBase
{
    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceListofThinAccessPoints"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.WirelessWebContent.NPMWEBCODE_VB0_127; }
    }
      
    protected override void LoadData()
    {
        var swqlParams = new Dictionary<string, object> { { "node", NodeID } };
        DataTable apsTable;        
        // list of all devices with specific NodeID, in this case there should be just one device in the list
        using (var swis = InformationServiceProxy.CreateV3())
        {
            apsTable =
            swis.Query(
                @"
SELECT tbl.DisplayName, tbl.IPAddress, tbl.ID, tbl.Index, tbl.Status
FROM Orion.Packages.Wireless.AccessPoints.Thin AS tbl  
WHERE tbl.NodeId = @node
", swqlParams);

            // test if there is just one device (one autonomousAP)
            if (apsTable.Rows.Count == 0)
            {
                this.OutputTable.Visible = false;
                return;
            }

            foreach (DataRow ap in apsTable.Rows)
            {
                int status;
                Int32.TryParse(ap["Status"].ToString(), out status);

                DataTable interfacesTable = swis.Query("SELECT tbl.Clients,tbl.Channel FROM Orion.Packages.Wireless.Interfaces AS tbl WHERE tbl.AccessPointID = @parent",
                    new Dictionary<string, object> { { "parent", ap["ID"].ToString() } });

                int clientsCount = 0;
                List<string> channels = new List<string>();
                foreach (DataRow i in interfacesTable.Rows)
                {
                    int cc = 0;
                    if (Int32.TryParse(i["Clients"].ToString(), out cc))
                    {
                        clientsCount += cc;
                    }
                    string channel = i["Channel"].ToString();
                    if (!channels.Contains(channel))
                        channels.Add(channel);
                }

                string allChannels = string.Empty;
                channels.Sort();
                allChannels = string.Join(", ", channels);

				string Name = ap["DisplayName"].ToString();

                var row = new HtmlTableRow();

                row.Cells.Add(NewTableCell(GetIconForAP(status, PollerDeviceType.WirelessController, true)));

                row.Cells.Add(NewTableCell(String.Format("<a href='{1}'>{0}</a>", Server.HtmlEncode(Name), CreateThinAPViewLink(ap["ID"]))));
                row.Cells.Add(NewTableCell(ap["IPAddress"].ToString()));
                row.Cells.Add(NewTableCell(allChannels));
                row.Cells.Add(NewTableCell(clientsCount.ToString(), "center"));

                OutputTable.Rows.Add(row);
            }
        }
    }
}
