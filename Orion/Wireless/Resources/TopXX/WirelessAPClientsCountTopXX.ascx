<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WirelessAPClientsCountTopXX.ascx.cs" Inherits="Orion_NetPerfMon_Resources_TopXX_WirelessAPClientsCount" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
    
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%=Resources.WirelessWebContent.NPMWEBDATA_VB0_38%>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        
        <table id="OutputTable" border="0" cellpadding="2" cellspacing="0" width="100%" runat="server">
            <tr>
	            <td class="ReportHeader">&nbsp;</td>
	            <td class="ReportHeader"><%=Resources.WirelessWebContent.NPMWEBDATA_VB0_49%></td>
	            <td class="ReportHeader"><%=Resources.WirelessWebContent.NPMWEBDATA_VB0_45%></td>
	            <td class="ReportHeader"><%=Resources.WirelessWebContent.NPMWEBDATA_VB0_50%></td>
	        </tr>
        </table>
        
    </Content>
</orion:resourceWrapper>