#region USING
using System;
using System.Data;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Wireless.Web.DisplayTypes;

#endregion

public partial class Orion_NetPerfMon_Resources_TopXX_WirelessClientsTraffic : TopXXResourceControl
{
    private static readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    private static readonly string EditResourcePage = "/Orion/NetPerfMon/Resources/TopXX/EditTopXX.aspx";

    protected override string TitleTemplate
    {
        get { return Resources.WirelessWebContent.NPMWEBCODE_VB0_11; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Wrapper.ShowEditButton = this.EnableEdit;

        LoadData();
    }

    public override string EditURL
    {
        get
        {
            return ExtendCustomEditUrl(EditResourcePage) + "&HideInterfaceFilter=True";
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceTopXWirelessClientsTraffic"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    private HtmlTableCell NewTableCell(string text)
    {
        var cell = new HtmlTableCell();

        cell.Attributes["class"] = "Property";
        cell.InnerHtml = text;

        return cell;
    }

    private static string GetRate(DataRow row)
    {
        int rateMbps;
        if (row.IsNull("Client_TxRate"))
            return Resources.WirelessWebContent.NPMWEBCODE_VB0_12;
        
        var rate = row["Client_TxRate"].ToString();
        if (rate.Contains(" Mbps")) // old data in db
            rate = rate.Substring(0, rate.IndexOf(' ')).Trim(); // remove the Mbps

        if (int.TryParse(rate, out rateMbps) && rateMbps != 0)
        {
            return new BitsPerSecond(rateMbps, DataUnitMagnitude.Mega).ToString();
        }
        else
        {
            return Resources.WirelessWebContent.NPMWEBCODE_VB0_12;
        }
    }

    protected void LoadData()
    {
        DataTable clients;
        try
        {
            clients = SolarWinds.Wireless.Web.DAL.TopXXDAL.GetWirelessClientsByTraffic(this.MaxRecords, this.Resource.Properties["Filter"]);
        }
		catch (System.Data.SqlClient.SqlException)
		{
            this.OutputTable.Visible = false;
            this.SQLErrorPanel.Visible = true;
			return;
		}

        foreach(DataRow client in clients.Rows)
        {
            var row = new HtmlTableRow();

            string ip = (client.IsNull("OrionLink") ? client["Client_IPAddress"].ToString() : String.Format("<a href='/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{1}'>{0}</a>", client["Client_IPAddress"].ToString(), (int)client["OrionLink"]));

            string rate = GetRate(client);

            row.Cells.Add(NewTableCell("<img src=\"/Orion/Wireless/images/Small-Broadcast-SSID.gif\" />"));
            row.Cells.Add(NewTableCell(ip));
            row.Cells.Add(NewTableCell(ValueFormatter.EmptyToNotAvailable(client["Client_SSID"])));
            row.Cells.Add(NewTableCell(client["FirstUpdate"].ToString()));

            row.Cells.Add(NewTableCell(rate));
            row.Cells.Add(NewTableCell( new BitsPerSecond(client["Client_TotalBytesTxPerSec"]).ToString() ));
            row.Cells.Add(NewTableCell(new BitsPerSecond(client["Client_TotalBytesRxPerSec"]).ToString() ));

            OutputTable.Rows.Add(row);
        }
    }
}
