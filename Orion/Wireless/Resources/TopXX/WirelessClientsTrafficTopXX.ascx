<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WirelessClientsTrafficTopXX.ascx.cs" Inherits="Orion_NetPerfMon_Resources_TopXX_WirelessClientsTraffic" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
    
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%=Resources.WirelessWebContent.NPMWEBDATA_VB0_38%>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        
        <table id="OutputTable" border="0" cellpadding="2" cellspacing="0" width="100%" runat="server">
            <tr>
	            <td class="ReportHeader">&nbsp;</td>
	            <td class="ReportHeader"><%=Resources.WirelessWebContent.NPMWEBDATA_VB0_45%></td>
	            <td class="ReportHeader"><%=Resources.WirelessWebContent.NPMWEBDATA_VB0_46%></td>
	            <td class="ReportHeader"><%=Resources.WirelessWebContent.NPMWEBDATA_VB0_47%></td>
	            <td class="ReportHeader"><%=Resources.WirelessWebContent.NPMWEBDATA_VB0_48%></td>
	            <td class="ReportHeader"><%=Resources.WirelessWebContent.NPMWEBDATA_VB0_40%></td>
	            <td class="ReportHeader"><%=Resources.WirelessWebContent.NPMWEBDATA_VB0_39%></td>
	        </tr>
        </table>
        
    </Content>
</orion:resourceWrapper>