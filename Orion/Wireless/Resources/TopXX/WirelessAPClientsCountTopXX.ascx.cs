#region USING
using System;
using System.Data;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
#endregion

public partial class Orion_NetPerfMon_Resources_TopXX_WirelessAPClientsCount : TopXXResourceControl
{
    private static readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    private static readonly string EditResourcePage = "/Orion/NetPerfMon/Resources/TopXX/EditTopXX.aspx";

    protected override string TitleTemplate
    {
        get { return Resources.WirelessWebContent.NPMWEBCODE_VB0_14; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Wrapper.ShowEditButton = this.EnableEdit;

        LoadData();
    }

    public override string EditURL
    {
        get
        {
            return ExtendCustomEditUrl(EditResourcePage) + "&HideInterfaceFilter=True";
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceTopXWirelessAPsClientsCount"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    private HtmlTableCell NewTableCell(string text)
    {
        var cell = new HtmlTableCell();

        cell.Attributes["class"] = "Property";
        cell.InnerHtml = text;

        return cell;
    }

    protected void LoadData()
    {
        DataTable aps;
        try
        {
            aps = SolarWinds.Wireless.Web.DAL.TopXXDAL.GetAPsByClientsCount(this.MaxRecords, this.Resource.Properties["Filter"]);
        }
		catch (System.Data.SqlClient.SqlException)
		{
            this.OutputTable.Visible = false;
            this.SQLErrorPanel.Visible = true;
			return;
		}

        foreach (DataRow ap in aps.Rows)
        {
            var row = new HtmlTableRow();
			/* ap[caption] doesnt exist */
            string name = (ap.IsNull("AP_Name")) ? "" : ap["AP_Name"].ToString();
			string icon = !ap.IsNull("ParentID") ? "wireless_thin.gif" : "wireless_auto.gif";

            row.Cells.Add(NewTableCell(String.Format("<img src=\"/Orion/Wireless/images/{0}\" />", icon)));
            row.Cells.Add(NewTableCell(String.Format("<a href='/Orion/Wireless/WirelessAP.aspx?AccessPointID={0}'>{1}</a>", (Int64)ap["ID"], name)));
            row.Cells.Add(NewTableCell(ap["IPAddress"].ToString()));
            row.Cells.Add(NewTableCell(ap["ClientsCount"].ToString()));

            OutputTable.Rows.Add(row);
        }
    }
}
