﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WirelessSearchResults.aspx.cs"
    Inherits="Orion_NetPerfMon_Resources_WirelessSearchResults" MasterPageFile="~/Orion/OrionMasterPage.master"
    Title="<%$ Resources:WirelessWebContent,NPMWEBDATA_AK0_73%>" %>

<%@ Register TagPrefix="orion" TagName="SmallNodeStatus" Src="~/Orion/Controls/SmallNodeStatus.ascx" %>
<%@ Import Namespace="SolarWinds.Orion.NPM.Web" %>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .resultsTable {
            margin-left: 15px;
            margin-bottom: 15px;
        }

            .resultsTable td {
                padding: 7px 0 7px 15px;
                
            }

        .title {
            padding: 15px;
        }

        .noLeftPadding {
            padding-left: 0;
        }

        thead > tr.ReportHeader {
            text-transform: uppercase;
        }


        tr.tr-parent > td {
            margin-top: 7px;
        }


        tr.tr-parent + tr.tr-child > td {
            border-top: 1px solid #ECEDEE;
        }

        tr.LastClient > td {
            border-bottom: 1px solid #ECEDEE;
        }

        tr.tr-child > td:last-child {
            border-right: 1px solid #ECEDEE;
            padding-right: 15px;
        }

        tr.tr-parent > td:first-child {
            padding-right: 0;
        }

        /*tr.tr-parent > td:nth-child(2) {
            padding-left: 15px;
        }*/

        tr.tr-child > td:nth-child(2) {
            border-left: 1px solid #ECEDEE;
            padding-right: 0;
        }

        tr.tr-child > td:nth-child(3) {
            padding-left: 0;
        }


        tr:nth-last-of-type(even) {
            background-color: #ffffff;
        }

        tr:nth-last-of-type(odd):not(.ReportHeader) {
            background-color: #f6f6f6;
        }

        tr.tr-child > td.empty {
            background-color: white;
            border: none !important;
            padding: 0;
        }
    </style>
    <asp:PlaceHolder ID="ErrorMessage" runat="server" Visible="false">
        <asp:Label ID="ErrorLabel" runat="server" Text="" ForeColor="Red" Font-Bold="true" />
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="Results" runat="server">
        <h1>
            <asp:Label runat="server" ID="TitleLabel" />
            <%--CssClass="title"--%>
        </h1>
        <table cellspacing="0" class="resultsTable">
            <thead>
                <tr class="ReportHeader">

                    <td colspan="3"><%=Resources.WirelessWebContent.NPMWEBDATA_TM0_01%></td>
                    <td><%=Resources.WirelessWebContent.NPMWEBCODE_VB0_84%></td>
                    <td><%=Resources.WirelessWebContent.NPMWEBDATA_VB0_120%></td>
                    <td><%=Resources.WirelessWebContent.NPMWEBDATA_VB0_46%></td>
                </tr>
            </thead>
            <asp:Repeater ID="ResultsRepeater" runat="server" OnItemDataBound="ResultsRepeater_OnItemDataBound">
                <ItemTemplate>
                    <asp:PlaceHolder runat="server" ID="apRow" Visible="false">
                        <tr class="tr-parent" runat="server">
                            <td>
                                <a href='<%#Eval("Url")%>'>
                                    <img runat="server" id="controllerIcon" border="0" alt="" title="" src="" />
                                </a>
                            </td>
                            <td colspan="2">
                                <a href='<%#Eval("Url")%>'><%#Eval("APName")%></a>
                            </td>
                            <td><%#Eval("APIP")%></td>
                            <td colspan="2">&nbsp</td>
                        </tr>
                    </asp:PlaceHolder>
                    <tr class="tr-child <%#Eval("Classes")%>">
                        <td class="empty">&nbsp</td>
                        <td>
                            <img border="0" alt="" title="" src="/Orion/Wireless/images/Small-Broadcast-SSID.gif" />
                        </td>
                        <td>
                            <%#Eval("ClientName")%>
                        </td>
                        <td><%#Eval("ClientIP")%></td>
                        <td><%#Eval("ClientMAC")%></td>
                        <td><%#Eval("ClientSSID")%></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </asp:PlaceHolder>
</asp:Content>
