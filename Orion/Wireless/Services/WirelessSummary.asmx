﻿<%@ WebService Language="C#" Class="WirelessSummary" %>

using System;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Web.Services;
using System.Web.Script.Services;
using System.Collections.Generic;
using SolarWinds.Logging;
using SolarWinds.Wireless.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Wireless.Web.DisplayTypes;

using i18nExternalized = Resources.WirelessWebContent;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class WirelessSummary : WebService
{
    private static readonly Log log = new Log();

    private static T GetValueOrDefault<T>(DataRow dr, string col)
    {
		Type t = typeof(T);
		t = Nullable.GetUnderlyingType(t) ?? t;

		return (dr.IsNull(col)) ? default(T) : (T)Convert.ChangeType(dr[col], t);
    }

    private static XElement CreateClientsXML(DataTable clients, IEnumerable<XElement> paging)
    {
        return new XElement("rows",
            paging,
            from DataRow dr in clients.Rows
            let id = Convert.ToInt64(dr["ID"])
            select new XElement("row",
                new XAttribute("id", id),
                new XElement("cell", WirelessResourceBase.GetIconForDeviceType(dr.IsNull("ControllerID")?PollerDeviceType.WirelessAutonomous:PollerDeviceType.WirelessController)), // icon
                new XElement("cell", dr["Name"]),
                new XElement("cell", ValueFormatter.EmptyToNotAvailable(dr["SSID"])),
                new XElement("cell", dr["IPAddress"]),
                new XElement("cell", dr["MAC"]),
                new XElement("cell", new SignalStrength(GetValueOrDefault<double?>(dr, "SignalStrength"))),
                new XElement("cell", GetValueOrDefault<DateTime>(dr, "FirstUpdate").ToString()),
                new XElement("cell", WirelessResourceBase.GetTxRateString(dr, "OutDataRate", Resources.WirelessWebContent.NPMWEBCODE_VB0_12)),
                new XElement("cell", new Bytes(GetValueOrDefault<double>(dr, "InTotalBytes")).ToShort1024String()),
                new XElement("cell", new Bytes(GetValueOrDefault<double>(dr, "OutTotalBytes")).ToShort1024String()),
                new XElement("cell", dr["AccessPoint"])
                )
            );
    }

    private static IEnumerable<XElement> CreatePaging(int page, int pages, int records)
    {
        yield return new XElement("page", page);
        yield return new XElement("total", pages);
        yield return new XElement("records", records);
    }

    private static XElement CreateRoguesXML(DataTable aps, IEnumerable<XElement> paging)
    {
        return new XElement("rows",
            paging,
            from DataRow dr in aps.Rows
            let id = Convert.ToInt64(dr["ID"])
            select new XElement("row",
                new XAttribute("id", id),
                new XElement("cell", OrionLink(dr["Name"].ToString(), Convert.ToInt32(dr["NodeID"]))),
                new XElement("cell", String.Empty),
                new XElement("cell", Resources.WirelessWebContent.NPMWEBCODE_LF0_6),
                new XElement("cell", ValueFormatter.EmptyToNotAvailable(dr["SSID"])),
                new XElement("cell", dr["CurrentChannel"]),
                new XElement("cell", String.Empty),
                new XElement("cell", String.Empty)
            )
            );
    }

    private static XElement CreateAPsXML(DataTable aps, DataTable ifaces, IEnumerable<XElement> paging)
    {
        return new XElement("rows",
            paging,
            from DataRow dr in aps.Rows
            let thinAp = !dr.IsNull("ControllerID")
            let id = Convert.ToInt64(dr["ID"])
            let ifaceRows = from DataRow row in ifaces.Rows where Convert.ToInt64(row["AccessPointID"]) == id select row
            let name = (WirelessResourceBase.GetIconForAP(dr["Status"], thinAp ? PollerDeviceType.WirelessController : PollerDeviceType.WirelessAutonomous, thinAp) + " ") + dr["Name"]
            select new XElement("row",
                new XAttribute("id", id),
                new XElement("cell",
                    thinAp ?
                    OrionThinAPLink(name, id) :
                    OrionLink(name, Convert.ToInt32(dr["NodeID"]))),
                new XElement("cell", dr["IPAddress"]),
                new XElement("cell", thinAp ? Resources.WirelessWebContent.NPMWEBCODE_LF0_5 : Resources.WirelessWebContent.NPMWEBCODE_JF0_30),
                new XElement("cell", ValueFormatter.EmptyToNotAvailable(string.Join(", ", (from row in ifaceRows where (row["SSID"] != DBNull.Value && !String.IsNullOrEmpty(row["SSID"].ToString())) select row["SSID"].ToString()).Distinct()))),
                new XElement("cell", string.Join(", ", (from row in ifaceRows where (row["CurrentChannel"] != DBNull.Value) select row["CurrentChannel"].ToString()).Distinct())),
                new XElement("cell", dr["Clients"]),
                new XElement("cell", "")
            )
            );
    }

    private static void ComputeOffset(int records, int rows, ref int page, out int pages, out int offset)
    {
        pages = 0;
        if (records > 0)
        {
            pages = (int)Math.Ceiling((float)records / rows);
        }

        if (pages == 0) pages = 1;
        if (page > pages) page = pages;
        offset = (rows * page) - rows;
    }


    private static string LoadClients(string where, IDictionary<string, object> swqlParams, int page, int rows, string sidx, string sord)
    {
        using (var swql = InformationServiceProxy.CreateV3())
        {
            int records =
                Convert.ToInt32(swql.Query(@"
SELECT COUNT(C.ID) as CNT FROM Orion.Packages.Wireless.Clients C 
INNER JOIN Orion.Nodes ON Nodes.NodeID = C.NodeID
WHERE " + where, swqlParams).Rows[0][0]);

            int pages, offset;
            ComputeOffset(records, rows, ref page, out pages, out offset);


            var clients =
                swql.Query(@"
SELECT 
C.ID, C.DisplayName as Name, C.NodeID, C.SSID, C.IPAddress, C.MAC, ToLocal(C.FirstUpdate) as FirstUpdate, C.SignalStrength, 
C.OutDataRate, C.OutTotalBytes, C.InTotalBytes, A.ControllerID, A.DisplayName AS AccessPoint
FROM Orion.Packages.Wireless.Clients C 
INNER JOIN Orion.Packages.Wireless.AccessPoints A ON A.ID = C.WirelessInterface.AccessPointID
INNER JOIN Orion.Nodes ON Nodes.NodeID = C.NodeID            
            WHERE " +
                    where +
                    CreateOrderBy(sidx, sord, offset, rows), swqlParams);

            return CreateClientsXML(clients, CreatePaging(page, pages, records)).ToString();
        }
    }

    [WebMethod(EnableSession = false)]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Xml)]
    public String GetClients(long id, int page, int rows, string sidx, string sord)
    {
        var swqlParams = new Dictionary<string, object>();
        string where = "";

        if (id != 0)
            where = " C.WirelessInterface.AccessPointID = " + id;
        else
            where = "1=1";

        return LoadClients(where, swqlParams, page, rows, sidx, sord);
    }

    private static string SQLMask(string mask)
    {
        string SQLMask = mask.Replace("%", "`%");  // '`' is used as SQL escape symbol
        return ("%" + SQLMask + "%"); // functionality of Node Management search
    }

    [WebMethod(EnableSession = false)]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Xml)]
    public String GetClientsSearch(string mask, int page, int rows, string sidx, string sord)
    {
        var swqlParams = new Dictionary<string, object>();
        string where = @"
(
 (C.IPAddress LIKE @mask) OR
 (C.MAC LIKE @mask) OR
 (C.SSID LIKE @mask) OR
 (C.OutDataRate LIKE @mask) OR
 (C.DisplayName LIKE @mask)
)
        ";
        swqlParams["mask"] = SQLMask(mask);
        return LoadClients(where, swqlParams, page, rows, sidx, sord);
    }

    static string OrionLink(string data, int nodeID)
    {
        if (String.IsNullOrEmpty(data) || data.Trim() == String.Empty)
            return String.Empty;
        return String.Format("<a href='/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}'>{1}</a>", nodeID, data);
    }

    static string OrionThinAPLink(string data, Int64 ThinAPID)
    {
        if (String.IsNullOrEmpty(data) || data.Trim() == String.Empty)
            return String.Empty;
        return String.Format("<a href='{0}'>{1}</a>", WirelessResourceBase.CreateThinAPViewLink(ThinAPID), data);
    }

    [WebMethod(EnableSession = false)]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Xml)]
    public String GetAPs(long id, int page, int rows, string sidx, string sord)
    {
        var swqlParams = new Dictionary<string, object>();

        if (id < -2) id = 0;

        string table = "";
        string where = " 1=1 ";

        switch (id)
        {
            case -2: // rogues APs
                return LoadRogueAPs(page, rows, sidx, sord);

            case -1: // autonomous APs
                table = "Orion.Packages.Wireless.AccessPoints.Autonomous";
                break;

            case 0: // all APs except rogues
                table = "Orion.Packages.Wireless.AccessPoints";
                break;

            default: // thin APs by Controller ID
                table = "Orion.Packages.Wireless.AccessPoints.Thin";
                where = " A.ControllerID= @id";
                swqlParams["id"] = id;
                break;
        }
        return LoadAPs(where, table, swqlParams, page, rows, sidx, sord);
    }

    private string LoadRogueAPs(int page, int rows, string sidx, string sord)
    {
        using (var swql = InformationServiceProxy.CreateV3())
        {
            var records = Convert.ToInt32(swql.Query(@"
SELECT COUNT(R.ID) as cnt FROM Orion.Packages.Wireless.Rogues R
INNER JOIN Orion.Nodes ON Nodes.NodeID = R.NodeID
").Rows[0][0]);

            int pages, offset;
            ComputeOffset(records, rows, ref page, out pages, out offset);

            var data = swql.Query(@"
SELECT A.ID, A.NodeID, A.DisplayName as Name, A.SSID, A.Channel as CurrentChannel
FROM Orion.Packages.Wireless.Rogues A
INNER JOIN Orion.Nodes N ON N.NodeID=A.NodeID
LEFT JOIN Orion.NodesCustomProperties C ON C.NodeID=A.NodeID

" + CreateOrderBy(sidx, sord, offset, rows));

            return CreateRoguesXML(data, CreatePaging(page, pages, records)).ToString();
        }

    }

    private static string CreateOrderBy(string sidx, string sord, int offset, int rows)
    {
        return string.Format(" ORDER BY {0} {1}, ID {1} WITH ROWS {2} TO {3} ", EscapeColumn(sidx), (sord == "asc" ? "ASC" : "DESC"), offset + 1, offset + rows);
    }

    /// <summary>
    /// returns col, if everything's OK (no sql injection), or ID otherwise
    /// accepted chars + a-z,0-9,.
    /// </summary>
    /// <param name="col"></param>
    /// <returns></returns>
    private static string EscapeColumn(string col)
    {
        char[] allowedChars = new char[] { '.', '_' };

        if (col.Any(c => !char.IsLetterOrDigit(c) &&  !allowedChars.Contains(c)))

            return "ID";

        return col;
    }

    private string LoadAPs(string where, string table, IDictionary<string, object> swqlParams, int page, int rows, string sidx, string sord)
    {
        using (var swql = InformationServiceProxy.CreateV3())
        {
            int records = Convert.ToInt32(swql.Query(@"
SELECT COUNT(TBL.ID) as CNT 

FROM 
(SELECT A.ID FROM
" + table + @" A 
INNER JOIN Orion.Nodes N ON N.NodeID=A.NodeID
LEFT JOIN Orion.NodesCustomProperties C ON C.NodeID=A.NodeID
WHERE " + where + ") TBL"
, swqlParams).Rows[0][0]);

            int pages, offset;
            ComputeOffset(records, rows, ref page, out pages, out offset);

            var aps = swql.Query(@"
    SELECT TBL.ID, TBL.NodeID, TBL.Name, TBL.IPAddress, TBL.WirelessType, TBL.Clients, TBL.Status, TBL.ControllerID, TBL.Index

    FROM (
SELECT A.ID, A.NodeID, A.DisplayName AS Name, A.IPAddress, A.WirelessType, A.Clients, A.Status, A.ControllerID, A.Index FROM
" + table + @" A 
    INNER JOIN Orion.Nodes N ON N.NodeID=A.NodeID
    LEFT JOIN Orion.NodesCustomProperties C ON C.NodeID=A.NodeID
    WHERE " + where + ") TBL" + CreateOrderBy("TBL." + sidx, sord, offset, rows), swqlParams);

            string rowIDs =
                aps.Rows.Count == 0 ? "0 = 1" :
                string.Format("AccessPointID IN ({0})", string.Join(",", aps.Rows.Cast<DataRow>().Select(x => x["ID"])));

            var ifaces = swql.Query(
                string.Format(
                    "SELECT I.AccessPointID, I.SSID, I.Channel AS CurrentChannel FROM Orion.Packages.Wireless.Interfaces I where {0} ORDER BY I.Channel ASC",
                    rowIDs
                    ));

            return CreateAPsXML(aps, ifaces, CreatePaging(page, pages, records)).ToString();
        }
    }

    [WebMethod(EnableSession = false)]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Xml)]
    public String GetAPsWhere(string column, string value, int page, int rows, string sidx, string sord)
    {
        string where;
        var swqlParams = new Dictionary<string, object>();

        if (value == i18nExternalized.NPMWEBDATA_VT0_3)
        {
            where = EscapeColumn(column) + " IS NULL";
        }
        else
        {
            where = "(" + EscapeColumn(column) + " = @val";
            if (string.IsNullOrEmpty(value.Trim()))
                where += " OR " + EscapeColumn(column) + " IS NULL";

            where += ")";
            swqlParams["val"] = value;
        }



        return LoadAPs(where, "Orion.Packages.Wireless.AccessPoints", swqlParams, page, rows, sidx, sord);
    }


    [WebMethod(EnableSession = false)]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Xml)]
    public String GetAPsSearch(string mask, int page, int rows, string sidx, string sord)
    {
        string where;
        var swqlParams = new Dictionary<string, object>();

        mask = SQLMask(mask);
        where = @" 
(
 (A.DisplayName LIKE @mask) OR
 (A.IPAddress LIKE @mask) OR
 (A.WirelessType LIKE @mask)
)
        ";
        swqlParams["mask"] = mask;

        return LoadAPs(where, "Orion.Packages.Wireless.AccessPoints", swqlParams, page, rows, sidx, sord);
    }


    public class GroupByListItem
    {
        public string Name { set; get; }
        public string Value { set; get; }
        public int Count { set; get; }
        public string Custom { set; get; }

        internal GroupByListItem(DataRow row, bool fillCustom = false, bool translateWirelessType = false)
        {
            Name = row["Name"].ToString();
            Value = ((IConvertible)row["Name"]).ToString(CultureInfo.InvariantCulture);

            if (translateWirelessType)
            {
                switch (Name)
                {
                    case "0":
                        Name = i18nExternalized.NPMWEBCODE_JF0_30;
                        break;
                    case "1":
                        Name = i18nExternalized.NPMWEBCODE_LF0_5;
                        break;
                    case "2":
                        Name = i18nExternalized.NPMWEBCODE_LF0_6;
                        break;
                    default:
                        Name = i18nExternalized.NPMWEBDATA_VT0_3;
                        break;
                }
            }

            Count = Convert.ToInt32(row["RowCount"]);
            if (fillCustom)
                Custom = row["Custom"].ToString();

            if (string.IsNullOrEmpty(Name.Trim()))
            {
                Name = i18nExternalized.NPMWEBDATA_VT0_3;
            }
        }
    }

    [WebMethod(EnableSession = false)]
    public IEnumerable<GroupByListItem> GroupByList(string column)
    {
        bool addVendor = column == "N.Vendor";
        bool translateWirelessType = column == "A.WirelessType";

        var col = EscapeColumn(column);
        using (var swql = InformationServiceProxy.CreateV3())
        {
            var db = swql.Query(string.Format(@"
SELECT M.Name," + (addVendor ? "MAX(M.VendorIcon) as Custom," : "") +
@"
COUNT(ISNULL(M.Name, '')) AS RowCount
FROM        
(Select {0} as Name, N.VendorIcon FROM
Orion.Packages.Wireless.AccessPoints A
INNER JOIN  Orion.Nodes N ON N.NodeID=A.NodeID
) M
GROUP BY M.Name
ORDER BY M.Name ASC
", col
            ));

            return
                from DataRow row in db.Rows
                select new GroupByListItem(row, addVendor, translateWirelessType);
        }
    }


    [WebMethod(EnableSession = false)]
    public IEnumerable<GroupByListItem> CustomPropertyList(string column)
    {
        var col = EscapeColumn(column);
        using (var swql = InformationServiceProxy.CreateV3())
        {
            var db = swql.Query(string.Format(@"
SELECT C.Name,
COUNT(ISNULL(C.Name, '')) AS RowCount
FROM        
( SELECT {0} as Name FROM 
Orion.Packages.Wireless.AccessPoints A
INNER JOIN  Orion.Nodes N ON N.NodeID=A.NodeID
INNER JOIN  Orion.NodesCustomProperties C ON C.NodeID=A.NodeID
) C
GROUP BY C.Name ORDER BY Name ASC
", col
            ));

            return
                from DataRow row in db.Rows
                select new GroupByListItem(row);
        }
    }


    public class ListItem
    {
        public string Icon { set; get; }
        public string Name { set; get; }
        public int Count { set; get; }
        public long ID { set; get; }

        public ListItem()
        {
        }

        public ListItem(string Icon, string name, int count, long id)
        {
            this.Icon = Icon;
            this.Name = name;
            this.Count = count;
            this.ID = id;
        }

        public ListItem(DataRow r)
        {
            Icon = WirelessResourceBase.GetIcon(r["Status"], i18nExternalized.NPMWEBDATA_VT0_1);
            Name = r["DisplayName"].ToString();
            Count = Convert.ToInt32(r["ThinAPsCount"]);
            ID = Convert.ToInt64(r["ID"]);
        }
    }

    [WebMethod(EnableSession = false)]
    public IEnumerable<ListItem> GroupByControllers()
    {
        string AutoIcon = "<img src='/Orion/Wireless/images/Object-WAP.png' border='0' title='' alt='' />";

        var list = new List<ListItem>();

        list.Clear();

        using (var swql = InformationServiceProxy.CreateV3())
        {
            var autoCount = Convert.ToInt32(swql.Query(@"
SELECT COUNT(A.ID) as cnt FROM Orion.Packages.Wireless.AccessPoints.Autonomous A
INNER JOIN Orion.Nodes ON Nodes.NodeID=A.NodeID
").Rows[0][0]);
            var rogueCount = Convert.ToInt32(swql.Query(@"
SELECT COUNT(R.ID) as cnt FROM Orion.Packages.Wireless.Rogues R
INNER JOIN Orion.Nodes ON Nodes.NodeID=R.NodeID
").Rows[0][0]);
            var controllers = swql.Query(@"
SELECT C.DisplayName, C.ID, COUNT(APT.ID) as ThinAPsCount, N.Status 
FROM Orion.Packages.Wireless.Controllers C 
INNER JOIN Orion.Nodes N on N.NodeID=C.NodeID
LEFT JOIN Orion.Packages.Wireless.AccessPoints.Thin APT ON C.ID=APT.ControllerID
GROUP BY C.ID, C.DisplayName, N.Status
ORDER BY DisplayName ASC
");

            yield return new ListItem(AutoIcon, i18nExternalized.NPMWEBDATA_VT0_4, autoCount, -1);
            yield return new ListItem(AutoIcon, i18nExternalized.NPMWEBDATA_VT0_5, rogueCount, -2);

            foreach (DataRow row in controllers.Rows)
                yield return new ListItem(row);

        }
    }
}