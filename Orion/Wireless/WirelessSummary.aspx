<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true" CodeFile="WirelessSummary.aspx.cs" Inherits="Orion_Wireless_Summary" Title="<%$ Resources: WirelessWebContent, NPMWEBDATA_TM0_80%>" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton HelpUrlFragment="OrionPHWirelessSummaryView" ID="IconHelpButton1" runat="server" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPlaceHolder" runat="Server">

    <orion:Include File="Admin.css" runat="server" />
    <orion:Include File="NodeMNG.css" runat="server" />
    <style type="text/css">
        #container {
            background: white;
        }
    </style>

    <orion:Include File="NodeMNGMenu.css" runat="server" />

    <orion:Include File="../Wireless/js/jqGrid/4.4.1/css/ui.jqgrid.css" runat="server" />
    <orion:Include File="../Wireless/js/jqGrid/4.4.1/js/grid.locale.js" runat="server" />
    <orion:Include File="../Wireless/js/jqGrid/4.4.1/js/jquery.jqGrid.min.js" runat="server" />
    <orion:Include File="../Wireless/js/WirelessSummary.js" runat="server" />
    <orion:Include File="../Wireless/styles/wirelessSummary.css" runat="server" />
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table cellspacing="0" cellpadding="0" width="100%" style="padding: 0 10px">
        <tr>
            <td>

                <asp:ScriptManagerProxy ID="ScriptManager1" runat="server">
                    <Services>
                        <asp:ServiceReference Path="Services/WirelessSummary.asmx" />
                        <asp:ServiceReference Path="/Orion/Services/WebAdmin.asmx" />
                    </Services>
                </asp:ScriptManagerProxy>

                <input type="hidden" name="ObjectType" id="ObjectType" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("WirelessSummary_ObjectType")%>' />
                <input type="hidden" name="APs_GroupBy" id="APs_GroupBy" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("WirelessSummary_APs_GroupBy")%>' />
                <input type="hidden" name="APs_SelectedItem" id="APs_SelectedItem" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("WirelessSummary_APs_SelectedItem")%>' />
                <input type="hidden" name="APs_RowsNumber" id="APs_RowsNumber" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("WirelessSummary_APs_RowsNumber")%>' />
                <input type="hidden" name="Clients_GroupBy" id="Clients_GroupBy" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("WirelessSummary_Clients_GroupBy")%>' />
                <input type="hidden" name="Clients_SelectedItem" id="Clients_SelectedItem" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("WirelessSummary_Clients_SelectedItem")%>' />
                <input type="hidden" name="Clients_RowsNumber" id="Clients_RowsNumber" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("WirelessSummary_Clients_RowsNumber")%>' />

                <input type="hidden" name="NodeCustomProperties" id="NodeCustomProperties" value='<%= String.Join(",", GetCustomProperties("NodesCustomProperties")) %>' />
                <input type="hidden" name="NodeCustomPropertiesTypes" id="NodeCustomPropertiesTypes" value='<%= String.Join(",", GetCustomPropertiesTypes("NodesCustomProperties")) %>' />


                <table width="100%">
                    <tr>
                        <%-- Page title --%>
                        <td style="width: 350px;" valign="baseline">
                            <h1><%=Page.Title%></h1>
                        </td>

                        <td style="margin-top: 8px;" valign="baseline" id="wireless-search-box">
                            <%-- Object type box --%>
                            <label for="showObjectType"><%=Resources.WirelessWebContent.NPMWEBDATA_AK0_50%></label>
                            <select id="showObjectType">
                                <option value="View.APs"><%=Resources.WirelessWebContent.NPMWEBDATA_TM0_81%></option>
                                <option value="View.Clients"><%=Resources.WirelessWebContent.NPMWEBDATA_TM0_82%></option>
                            </select>
                            <span class="separator"></span>
                            <%-- Search box --%>
                            <input id="search" type="text" />
                            <orion:LocalizableButtonLink runat="server" LocalizedText="CustomText" Text="<%$ Resources: WirelessWebContent, NPMWEBDATA_VB0_218%>"
                                ID="searchButton" DisplayType="Primary" CssClass="wirelessSearchClick" />
                        </td>
                    </tr>
                </table>

                <table class="NodeManagement" width="100%">
                    <tr valign="top" align="left">

                        <td style="width: 200px; min-width: 200px; border-right: #cccccc 1px solid; border-left: 0; border-top: 0; border-bottom: 0; background-color: White;">
                            <div class="NodeGrouping">

                                <!-- Group by combo box -->
                                <div class="GroupSection">
                                    <div><%=Resources.WirelessWebContent.NPMWEBDATA_TM0_83%></div>
                                    <div id="groupByTag" style="width: 100%">
                                    </div>
                                </div>

                                <ul class="NodeGroupItems">
                                    <!-- Items for selected group by -->
                                </ul>
                            </div>
                        </td>

                        <td id="GridPlace">
                            <%-- Data for Aps/Clients --%>
                            <table id="MainGrid" class="scroll" cellpadding="0" cellspacing="0"></table>
                            <div id="MainPager" class="scroll" style="text-align: left;"></div>
                        </td>
                    </tr>
                </table>


                <%-- Error messages --%>
                <div id="originalQuery"></div>
                <div id="test"></div>
                <pre id="stackTrace"></pre>

                <script type="text/javascript" src="../js/jquery/jquery.bgiframe.min.js"></script>

            </td>
        </tr>
    </table>
</asp:Content>
