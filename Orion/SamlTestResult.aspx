﻿<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMinReqs.master" Title="<%$ Resources:AccountManagementWebContent,SamlTestResultPageTitle %>" CodeFile="SamlTestResult.aspx.cs" Inherits="SolarWinds.Orion.AccountManagement.LegacyWebSite.Orion_SamlTestResult" ValidateRequest="false" %>

<%@ Import Namespace="SolarWinds.Orion.AccountManagement.Models" %>
<%@ Import Namespace="Resx=Resources.AccountManagementWebContent" %>

<%@ Register TagPrefix="orion" TagName="PageHeader" Src="~/Orion/Controls/PageHeader.ascx" %>
<%@ Register TagPrefix="orion" TagName="PageFooter" Src="~/Orion/Controls/PageFooter.ascx" %>
<asp:Content ContentPlaceHolderID="HeadContent" runat="Server">
	<link rel="stylesheet" type="text/css" href="/Orion/js/google-code-prettify/prettify.css">
	<script type="text/javascript" src="/Orion/js/google-code-prettify/prettify.js"></script>
	<script type="text/javascript">
		$(window).on('load', function() {
			PR.prettyPrint();
		});
		$(document).ready(function () {
			$(".expander>span").click(function () {
				$(this).find(".expander-icon").toggleClass("expand collapse");
				$(this).parent().next(".expander-content").slideToggle();
			});
		});
	</script>
	<style>
		body:not(.sw-eval-mode) .container {
			padding: 40px 25px 65px 25px;
		}

		body.sw-eval-mode .container {
			padding: 60px 25px 65px 25px;
		}

		.header {
			padding: 20px 15px 15px 0;
		}

		#footer {
			position: fixed;
		}

		.message-success {
			border: #30B230 1px solid;
			background-color: #E7F4DF;
		}

		.message-error {
			border: #D50000 1px solid;
			background-color: #FFE4E0;
			color: #D50000;
		}

		.message-warning {
			border: #FEC405 1px solid;
			background-color: #FCF8E0;
		}

		.message-icon {
			padding-left: 10px;
			padding-top: 10px;
			float: left;
			clear: both;
		}

		.message-content {
			padding-top: 11px;
			padding-bottom: 11px;
			padding-left: 10px;
			display: inline-block;
			width: 90%;
		}

		.prettyprint {
			white-space: pre-wrap;
			word-wrap: break-word;
			word-break: break-word;
		}

			.prettyprint li {
				list-style-type: decimal !important;
			}

		.expander {
			margin-top: 20px;
		}

			.expander > span {
				cursor: pointer;
			}

		.expander-icon {
			display: inline-block;
			height: 13px;
			width: 15px;
		}

		.expander-header {
			font-weight: bold;
			font-size: 14px;
		}

		.expander-content {
			display: none;
		}

		.expand {
			background: url("/Orion/images/Button.Collapse.gif") no-repeat top left;
		}

		.collapse {
			background: url("/Orion/images/Button.Expand.gif") no-repeat top left;
		}

		.hide {
			display: none;
		}

		.user-container {
			margin-top: 20px;
			margin-left: 17px;
		}

		.time-container {
			margin-top: 20px;
			margin-left: 17px;
		}

		.exception-container {
			margin-top: 12px;
			margin-left: 17px;
		}

		.line {
			display: table;
			padding-bottom: 5px;
		}

		.line-header {
			display: table-cell;
			width: 130px;
			padding-right: 15px;
		}

		.line-content {
			display: table-cell;
			word-break: break-all;
		}

		@media print {
			#pageHeader, #footer {
				display: none;
			}

			.expander-content {
				display: block !important;
			}
		}
	</style>
</asp:Content>

<asp:Content ContentPlaceHolderID="BodyContent" runat="Server">
	<orion:PageHeader Minimalistic="true" runat="server" />
	<div class="container">
		<h1 class="header"><%= Resx.SamlTestResultPageHeading %></h1>
		<% if (samlTestResult == null)
			{ %>
		<div class="message-warning">
			<i class="message-icon">
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/" x="0px" y="0px" viewBox="0 0 20 20" width="20" height="20" enable-background="new 0 0 20 20" xml:space="preserve">
					<polygon fill="#F9C700" points="20,20 0,20 10,0 "></polygon>
					<path fill="#333333" fill-opacity="0.6" d="M11,17H9v-2h2V17z M11,7H9v6h2V7z"></path>
				</svg>
			</i>
			<div class="message-content">
				<b><%= Resx.SamlTestResultNotStored %></b>
			</div>
		</div>
		<% }
			else
			{ %>
		<div class="message-error <%= samlTestResult?.TestResult == SamlTestResultType.Error ? "" : "hide" %>">
			<i class="message-icon">
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
					x="0px" y="0px" viewBox="0 0 20 20" width="20" height="20" enable-background="new 0 0 20 20"
					xml:space="preserve">
					<polygon fill="#D50000" points="5.858,20 0,14.143 0,5.858 5.858,0 14.143,0 20,5.858 20,14.143 14.143,20 "></polygon>
					<path fill="none" stroke="#FFFFFF" stroke-width="1.5" stroke-miterlimit="10" d="M14,6l-8,8 M6,6l8,8"></path>
				</svg>
			</i>
			<div class="message-content">
				<b><%= Resx.SamlTestResultError %></b>
				<div><%= samlTestResult?.Message %></div>
			</div>
		</div>
		<div class="message-warning <%= samlTestResult?.TestResult == SamlTestResultType.Warning ? "" : "hide" %>">
			<i class="message-icon">
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/" x="0px" y="0px" viewBox="0 0 20 20" width="20" height="20" enable-background="new 0 0 20 20" xml:space="preserve">
					<polygon fill="#F9C700" points="20,20 0,20 10,0 "></polygon>
					<path fill="#333333" fill-opacity="0.6" d="M11,17H9v-2h2V17z M11,7H9v6h2V7z"></path>
				</svg>
			</i>
			<div class="message-content">
				<b><%= samlTestResult?.Title %></b>
				<div><%= samlTestResult?.Message %></div>
			</div>
		</div>
		<div class="message-success <%= samlTestResult?.TestResult == SamlTestResultType.Success ? "" : "hide " %>">
			<i class="message-icon">
				<svg version="1.1" id="Ok" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
					x="0px" y="0px" viewBox="0 0 20 20" width="20" height="20" enable-background="new 0 0 20 20"
					xml:space="preserve">
					<circle fill="#30B230" cx="10" cy="10" r="10"></circle>
					<polyline fill="none" stroke="#FFFFFF" stroke-width="1.5" stroke-miterlimit="10" points="15,7 9,13 5,9 "></polyline>
				</svg>
			</i>
			<div class="message-content">
				<b><%= Resx.SamlTestResultSuccess %></b>
				<div><%= samlTestResult?.Message %></div>
			</div>
		</div>
		<div class="user-container">
			<div class="line">
				<div class="line-header"><b><%= Resx.SamlTestResultUserName %></b></div>
				<div class="line-content"><%= !String.IsNullOrWhiteSpace(samlTestResult?.UserName) ? samlTestResult?.UserName : "N/A" %></div>
			</div>
			<div class="line">
				<div class="line-header"><b><%= Resx.SamlTestResultGroups %></b></div>
				<div class="line-content"><%= samlTestResult?.Groups?.Count > 0 ? String.Join(", ", samlTestResult?.Groups) : "N/A" %></div>
				<div></div>
			</div>
		</div>
		<div class="time-container">
			<div class="line">
				<div class="line-header"><b><%= Resx.SamlTestResultInitLoaded %></b></div>
				<div class="line-content"><%= (bool)samlTestResult?.SamlInitLoaded.HasValue ? samlTestResult?.SamlInitLoaded.Value.ToString() : "N/A" %></div>
			</div>
			<% if ((bool)samlTestResult?.SamlInitLoaded.HasValue && (bool)samlTestResult?.SamlLoginLoaded.HasValue)
				{ %>
			<div class="line">
				<div class="line-header"><b><%= Resx.SamlTestResultInitDuration %></b></div>
				<div class="line-content"><%= samlTestResult?.SamlLoginLoaded.Value.Subtract((DateTime)samlTestResult?.SamlInitLoaded.Value).TotalMilliseconds.ToString("0") %> ms</div>
			</div>
			<% } %>
			<div class="line">
				<div class="line-header"><b><%= Resx.SamlTestResultLoginLoaded %></b></div>
				<div class="line-content"><%= (bool)samlTestResult?.SamlLoginLoaded.HasValue ? samlTestResult?.SamlLoginLoaded.Value.ToString() : "N/A" %></div>
			</div>
			<div class="line">
				<div class="line-header"><b><%= Resx.SamlTestResultLoginFinished %></b></div>
				<div class="line-content"><%= (bool)samlTestResult?.SamlLoginFinished.HasValue ? samlTestResult?.SamlLoginFinished.Value.ToString() : "N/A" %></div>
			</div>
			<% if ((bool)samlTestResult?.SamlLoginLoaded.HasValue && (bool)samlTestResult?.SamlLoginFinished.HasValue)
				{ %>
			<div class="line">
				<div class="line-header"><b><%= Resx.SamlTestResultLoginDuration %></b></div>
				<div class="line-content"><%= samlTestResult?.SamlLoginFinished.Value.Subtract((DateTime)samlTestResult?.SamlLoginLoaded.Value).TotalMilliseconds.ToString("0") %> ms</div>
			</div>
			<% } %>
		</div>
		<% if (!String.IsNullOrWhiteSpace(samlTestResult.ExceptionType))
			{ %>
		<div class="expander">
			<span>
				<span class="expander-icon collapse"></span>
				<span class="expander-header"><%= Resx.SamlTestResultException %></span>
			</span>
		</div>
		<div class="expander-content exception-container">
			<div class="line">
				<div class="line-header"><b><%= Resx.SamlTestResultExceptionType %></b></div>
				<div class="line-content"><%= samlTestResult?.ExceptionType %></div>
			</div>
			<div class="line">
				<div class="line-header"><b><%= Resx.SamlTestResultExceptionMessage %></b></div>
				<div class="line-content"><%= samlTestResult?.ExceptionMessage %></div>
			</div>
			<div class="line">
				<div class="line-header"><b><%= Resx.SamlTestResultExceptionStackTrace %></b></div>
				<div class="line-content">
					<div><%= String.Join("</div><div>", samlTestResult?.ExceptionStackTrace?.Split(new [] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)) %></div>
				</div>
			</div>
		</div>
		<% } %>
		<% if (!String.IsNullOrWhiteSpace(samlRequest))
			{ %>
		<div class="expander">
			<span>
				<span class="expander-icon collapse"></span>
				<span class="expander-header"><%= Resx.SamlTestResultRequest %></span>
			</span>
		</div>
		<pre class="expander-content prettyprint lang-xml linenums:1"><%= samlRequest %></pre>
		<% } %>
		<% if (!String.IsNullOrWhiteSpace(samlResponse))
			{ %>
		<div class="expander">
			<span>
				<span class="expander-icon collapse"></span>
				<span class="expander-header"><%= Resx.SamlTestResultResponse %></span>
			</span>
		</div>
		<pre class="expander-content prettyprint lang-xml linenums:1"><%= samlResponse %></pre>
		<% } %>
		<% } %>
	</div>
	<orion:PageFooter runat="server" />
</asp:Content>
