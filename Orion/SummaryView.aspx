<%@ Page Language="C#" MasterPageFile="~/Orion/View.master" AutoEventWireup="true" CodeFile="SummaryView.aspx.cs" Inherits="Orion_SummaryView" Title="Untitled Page" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="orion" TagName="Refresher" Src="~/Orion/Controls/Refresher.ascx" %>

<asp:Content runat="server" ContentPlaceHolderID="ViewPageTitle">
	<h1>
	    <%= DefaultSanitizer.SanitizeHtml(ViewInfo.ViewGroupTitle) %>
        <%= DefaultSanitizer.SanitizeHtml(ViewInfo.ViewHtmlTitle) %> 
	</h1>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="MainContentPlaceHolder">
	<orion:ResourceHostControl ID="ResourceHostControl1" runat="server">
		<orion:ResourceContainer runat="server" ID="resContainer" />
	</orion:ResourceHostControl>
</asp:Content>
