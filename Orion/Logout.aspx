<%@ Page Language="C#" AutoEventWireup="true" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.Indications" %>

<script runat="server">
    private void Page_Load(object sender, EventArgs e)
    {
        LoggedInUser.LogOut();
    
        // Clear also session cookie 
        CookieHelper.ClearCookie("ASP.NET_SessionId", true);

        Response.Redirect("/Orion/Login.aspx?autologin=no&SuccessfulLogout=yes");
    }
</script>