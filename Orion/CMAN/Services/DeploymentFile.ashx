﻿<%@ WebHandler Language="C#" Class="DeploymentFile" %>

using System;
using System.IO;
using System.Web;
using SolarWinds.Orion.CMAN.Web.Deployment;
using SolarWinds.Orion.ContainerMgmt.Common.Deployment;
using SolarWinds.Orion.ContainerMgmt.Common.CentralizedSettings;

public class DeploymentFile : IHttpHandler
{
    private readonly IDeploymentCache _deploymentCache = new StaticDeploymentCache();
    private readonly IEnvironmentConfig _environmentConfig = new EnvironmentConfig(HttpRuntime.AppDomainAppPath);

    public void ProcessRequest(HttpContext context)
    {
        string guid = context.Request.QueryString["guid"];

        var deploymentData = _deploymentCache.GetData(guid);
        if (deploymentData == null)
        {
            context.Response.StatusCode = 404;
            return;
        }

        var env = _environmentConfig.GetEnvironment(deploymentData.EnvType);

        string appPath = HttpRuntime.AppDomainAppPath;
        string pathString = Path.Combine(appPath, env.DeploymentFile);
        if (!File.Exists(pathString))
        {
            context.Response.StatusCode = 404;
            return;
        }

        string readText = File.ReadAllText(pathString);

        readText = readText.Replace("#user#", deploymentData.User);
        readText = readText.Replace("#orionurl#", deploymentData.ApiBaseUrl);
        readText = readText.Replace("#hostip#", deploymentData.HostIp);
        readText = readText.Replace("#containeragentname#", deploymentData.AgentName);
        readText = readText.Replace("#containeragentguid#", guid);
        readText = readText.Replace("#pollingInterval#", ((int)Math.Round(PollingSettings.Instance.PollingInterval.TotalSeconds)).ToString());
        readText = readText.Replace("#certificateFingerprint#", deploymentData.CertificateFingerprint);

        context.Response.ContentType = "text/plain";
        context.Response.Write(readText);
    }

    public bool IsReusable => false;
}