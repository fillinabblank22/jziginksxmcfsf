﻿<%@ WebService Language="C#" Class="AgentService" %>

using System;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Orion.CMAN.Web.Deployment;
using SolarWinds.Orion.ContainerMgmt.Common.Deployment;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class AgentService : WebService
{
    private readonly IDeploymentCache _deploymentCache = new StaticDeploymentCache();
    private readonly IEnvironmentConfig _environmentConfig = new EnvironmentConfig(HttpRuntime.AppDomainAppPath);

    [WebMethod]
    [ScriptMethod(UseHttpGet=true)]
    public string GetScript(string user, string hostip, string name, string environment, string orionendpoint, string certificateFingerprint)
    {
        Guid scriptGuid = Guid.NewGuid();

        var env = _environmentConfig.GetEnvironment(environment);
            
        var deploymentData = new DeploymentData
        {
            EnvType = env.EnvType,
            User = user,
            HostIp = hostip,
            AgentName = name,
            ApiBaseUrl = "https://"+orionendpoint,
            CertificateFingerprint = certificateFingerprint
        };

        _deploymentCache.SetData(scriptGuid.ToString(), deploymentData);

        string endpoint = string.Format("{0}/Orion/CMAN/Services/DeploymentFile.ashx?guid={1}", HostUrlFromContext(), scriptGuid.ToString());

        string script = env.Command.Replace("#endpoint#", endpoint);

        return script;
    }
    
    private static string HostUrlFromContext(string overrideScheme = "")
    {
        var url = HttpContext.Current.Request.Url;
        return (overrideScheme != "" ? overrideScheme : url.Scheme) + Uri.SchemeDelimiter + url.Host + (url.IsDefaultPort ? "" : ":" + url.Port);
    }
}
