#!/bin/bash
echo "Collecting data..."

ORION_CONSOLE_URL="#orionurl#"
MARATHON_API_URL="#hostip#:8080"
MESOS_MASTER_IP="#hostip#"
CONTAINER_AGENT_NAME="#containeragentname#"
CONTAINER_AGENT_GUID="#containeragentguid#"
USER="#user#"
CERTIFICATE_FINGERPRINT="#certificateFingerprint#"
read -sp 'Please enter password for Orion user ['$USER']: ' PASSWORD && echo
PASSWORD=$(echo -ne $PASSWORD | base64)

HOST_NAME=$(hostname)

RED='\033[0;31m'
NC='\033[0m'


echo "Deploying OrionAggregator on $HOST_NAME"

SCOPE_TO_ORION_DATA=$(cat << EOF
{ 
  "id": "/orion-aggregator",
  "cmd": null,
  "cpus": 1,
  "mem": 128,
  "disk": 0,
  "instances": 1,
  "acceptedResourceRoles": [
    "*"
  ],
  "constraints": [
	["hostname", "LIKE", "$HOST_NAME"] 
  ],  
  "container": {
    "type": "DOCKER",
    "docker": {
      "forcePullImage": false,
      "image": "solarwinds/orion:v3_stable",
      "parameters": [],
      "privileged": false
    },
    "volumes": [
      {
        "containerPath": "/swdata",
        "hostPath": "/var/cman-volume",
        "mode": "RW"
      }
    ]
  },
  "env": {
    "USER": "$USER",
    "PASSWORD": "$PASSWORD",
    "ORION_URL": "$ORION_CONSOLE_URL",
    "CONTAINER_AGENT_NAME": "$CONTAINER_AGENT_NAME",
    "CONTAINER_AGENT_GUID": "$CONTAINER_AGENT_GUID",
    "CERTIFICATE_FINGERPRINT": "$CERTIFICATE_FINGERPRINT",
	"POLLING_INTERVAL": "#pollingInterval#"
  }
}
EOF
)

getDataFromJson(){   
  cat | python -c "import sys, json; print(json.load(sys.stdin)['$1'])"
}

showCurlError(){
  if [[ ${1:2:7} = message ]]
  then 
    echo -e ${RED}$2${NC}
    echo -e ${RED}$(echo $1 | getDataFromJson "message")${NC}  
  fi
}


RESULT=$(curl -H "Content-Type:application/json" -d "$SCOPE_TO_ORION_DATA" $MARATHON_API_URL/v2/apps)
showCurlError "$RESULT" "Deploying OrionAggregator failed"


echo "Deploying OrionMonitor to every slave"

SLAVES_DATA=$(curl -s "$MESOS_MASTER_IP:5050/slaves" | getDataFromJson "slaves")

NUMBER_OF_SLAVES=$(python -c "import sys, json; print(len($SLAVES_DATA))")

SCOPE_DATA=$(cat << EOF
{
  "id": "/orion-monitor",
  "cmd": null,
  "cpus": 1,
  "mem": 64,
  "disk": 0,
  "instances": $NUMBER_OF_SLAVES,
  "acceptedResourceRoles": [
    "*"
  ],
  "constraints":[
	["hostname", "UNIQUE"],
	["hostname", "GROUP_BY"]
  ], 
  "container": {
    "type": "DOCKER",
    "docker": {
      "forcePullImage": false,
      "image": "weaveworks/scope:1.8.0",
      "parameters": [],
      "privileged": false
    },
    "volumes": [
      {
        "containerPath": "/var/run/docker.sock",
        "hostPath": "/var/run/docker.sock",
        "mode": "RW"
      }
    ]
  },
  "labels": {
    "works.weave.role": "system"
  },
  "args": [
    "--probe.docker=true",
    "--no-app",
    "--probe.docker.bridge=docker0",
    "--probe.kubernetes=false",
    "--probe.publish.interval=#pollingInterval#s",
    "--probe.spy.interval=30s",
    "$MESOS_MASTER_IP:4043"
  ]
}
EOF
)

RESULT=$(curl -H "Content-Type:application/json" -d "$SCOPE_DATA" $MARATHON_API_URL/v2/apps)
showCurlError "$RESULT" "Deploying OrionMonitor failed"
