﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.CMAN.Web.NetObjects;
using SolarWinds.Orion.CMAN.Web.NetObjectsProviders;
using SolarWinds.Orion.NPM.Web.UI;
using SolarWinds.Orion.Web.UI;

public partial class ContainerDetailsPage : OrionView, ICMANProvider
{
    public override string ViewType
    {
        get { return "CMAN Container Details"; }
    }

    protected override void OnInit(EventArgs e)
    {
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);

        this.Title = CManContainer.Name;
    }

    protected override void OnLoad(EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
        }

        base.OnLoad(e);
    }

    public CManContainer CManContainer
    {
        get { return (CManContainer)this.NetObject; }
    }
}