﻿<%@ Page Title="a" Language="C#" AutoEventWireup="true" CodeFile="ContainerDetailsPage.aspx.cs" Inherits="ContainerDetailsPage" MasterPageFile="CmanView.master" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="cman" Namespace="SolarWinds.Orion.CMAN.Web" Assembly="SolarWinds.Orion.CMAN.Web" %>
<%@ Register TagPrefix="orion" TagName="IconHelpButton" Src="~/Orion/Controls/IconHelpButton.ascx" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <orion:IconHelpButton HelpUrlFragment="OrionCoreContainerDetails" ID="IconHelpButton1" runat="server" />
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ViewPageTitle" ID="Content1">
	<h1>
	    <a href='<%=BaseResourceControl.GetViewLink(CManContainer)%>'><%= UIHelper.Escape(Title) %></a> - Summary
	    <%= ViewInfo.IsSubView ? ViewInfo.ViewHtmlTitle : string.Empty%>	
	</h1>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContentPlaceHolder" runat="server"> 
    <cman:CMANResourceHost ID="CMANResourceHost" runat="server">
        <orion:ResourceContainer runat="server" ID="resContainer" />
    </cman:CMANResourceHost>
</asp:Content>