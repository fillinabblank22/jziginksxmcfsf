﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Profile;
using System.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_LoginApi : Page
{
    private static readonly Log log = new Log();

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        FillDefaultsForApiUser(Profile);

        string returnUrl = Request.QueryString["ReturnUrl"] ?? ResolveUrl("~/Orion/Default.aspx");
        log.DebugFormat("Redirecting account ID '{0}' to original url '{1}'.", Profile.UserName, returnUrl);

        Response.Redirect(returnUrl);
    }

    /// <summary>
    /// This code needs to be in Web project because it works with the auto-generated <see cref="ProfileCommon"/> object.
    /// </summary>
    /// <param name="profile"></param>
    private static void FillDefaultsForApiUser(ProfileCommon profile)
    {
        log.DebugFormat("Checking if logged in account ID '{0}' is marked for additional processing on first login.", 
            profile.UserName);

        // loading user's Profile calls into IProfilePlugin implementations
        // that can do anything, including remote calls to Module Engine or SWIS
        // users should be able to get past Login page as this additional processing
        // is probably NOT critical for majority of user accounts.
        try
        {
            string originalUser = profile.UserName;
            bool isGroupMember = profile.AccountType == (int)GroupAccountType.WindowsVirtual;
            if (isGroupMember)
            {
                profile = profile.GetProfile(profile.GroupInfo);

                log.DebugFormat("Account ID '{0}' is a group member, checking it's parent group '{1}'.", 
                    originalUser, profile.UserName);
            }

            Dictionary<string, string> settings = WebUserSettingsDAL.GetAll(profile.UserName);
            if (!settings.ContainsKey(CoreConstants.ApiNewAccountFillDefaultsOnLogin))
            {
                log.DebugFormat("Account ID '{0}' doesn't need additional processing on first login.", 
                    profile.UserName);

                return;
            }

            FillDefaultsForUser(profile);

            WebUserSettingsDAL.Remove(profile.UserName, CoreConstants.ApiNewAccountFillDefaultsOnLogin);

            if (isGroupMember)
            {
                log.DebugFormat("Applying settings from group account ID '{0}' to its members.", 
                    profile.UserName);

                AuthorizationManager.CloneGroupAccountToVirtualUser(originalUser, profile.UserName, (GroupAccountType)profile.AccountType);
            }
        }
        catch (Exception ex)
        {
            // this is a problem but not fatal, allow the user to move past Login page
            // they'll likely see another error though
            log.Error("Failed to fill account created through API with default profile values.", ex);
        }
    }

    private static void FillDefaultsForUser(ProfileCommon profile)
    {
        log.DebugFormat("Filling default View IDs and Menu Bars for account ID '{0}'.", 
            profile.UserName);

        AssignViewIDs(profile);

        profile.Save();

        AssignMenuBars(profile.UserName, profile.MenuName);
    }

    private static void AssignViewIDs(ProfileCommon profile)
    {
        foreach (string viewType in ViewManager.GetViewTypes())
        {
            string propertyName = ViewManager.GetUserPropertyNameByViewType(viewType);

            // TT #7375 Node Details view = By Device Type as default for new accounts
            if (propertyName.Equals("NetPerfMon.NodeDetailsViewID", StringComparison.OrdinalIgnoreCase))
            {
                UpdateIfNotAlreadySet(profile, propertyName, -1);
            }
            else
            {
                ViewInfo viewInfo = ViewManager.GetViewsByType(viewType).FirstOrDefault();

                if (viewInfo != null)
                {
                    log.DebugFormat("Found default view ID for view type '{0}', property name '{1}': view '{2}'.", 
                        viewType, propertyName, viewInfo.ViewID);
                    if (viewInfo.IsStaticSubview)
                        continue;

                    UpdateIfNotAlreadySet(profile, propertyName, viewInfo.ViewID);
                }
                else
                {
                    log.DebugFormat("Couldn't find default view ID for view type '{0}', property name '{1}'.", 
                        viewType, propertyName);
                }
            }
        }

        if (profile.HomePageViewID == 0)
        {
            log.DebugFormat("Updating property 'HomePageViewID' to value '{0}', the same as 'SummaryViewID'.", 
                profile.SummaryViewID);

            profile.HomePageViewID = profile.SummaryViewID;
        }
    }

    private static void UpdateIfNotAlreadySet(ProfileBase profile, string propertyName, int defaultValue)
    {
        // if property was set to non-default value, then do not overwrite it
        object currentValue = profile.GetPropertyValue(propertyName);
        bool isNonDefaultValue = currentValue != null && !currentValue.Equals(0);

        if (isNonDefaultValue)
        {
            log.DebugFormat("Skipping property '{0}' because it has non-default value of '{1}'.", 
                propertyName, currentValue);

            return;
        }

        log.DebugFormat("Updating property '{0}' to default value of '{1}'.", propertyName, defaultValue);

        profile.SetPropertyValue(propertyName, defaultValue);
    }

    private static void AssignMenuBars(string accountId, string defaultMenuName)
    {
        //Assign menubars for new created user

        ITabManager manager = new TabManager();
        IList<Tab> tabs = manager.GetAllTabs();
        foreach (var tab in tabs)
        {
            if (tab.TabName == "Home")
            {
                //Assign Default menubar to Home tab for new created user
                manager.AssignBarToTab(accountId, tab.TabId, defaultMenuName);
            }
            else
            {
                //Assign menubars for other modules
                string menuName = manager.GetCurrentBarName(accountId, tab.TabId);
                //assign the menu of the current user
                //if no current user then get default
                if (string.IsNullOrEmpty(menuName))
                {
                    menuName = manager.GetDefaultBarForTab(tab.TabName);
                }

                //Assign the menubar value
                if (!string.IsNullOrEmpty(menuName))
                {
                    manager.AssignBarToTab(accountId, tab.TabId, menuName);
                }
            }
        }
    }
}