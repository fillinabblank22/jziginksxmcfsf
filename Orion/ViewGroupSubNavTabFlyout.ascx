<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ViewGroupSubNavTabFlyout.ascx.cs" Inherits="ViewGroupSubNavTabFlyout" %>

<div class="tabFlyout">
    <div class="selectionArrow"></div>
    <span class="SubNav-FlyoutTitle"><%# DefaultSanitizer.SanitizeHtml(Eval("ViewShortTitle")) %></span> <span class="SubNav-FlyouTitleText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_59) %></span> 
    <hr />
    
    <div runat="server" Visible="<%# DefaultSanitizer.SanitizeHtml(this.View.ViewID == 0) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_60) %></div>
    
    <table class="tabFlyout-LayoutTable">
        <tr>
            <td>
                 <ul>
                    <asp:Repeater runat="server" ID="rptColumn1Resources" DataSource='<%# DefaultSanitizer.SanitizeHtml(this.Column1Resources) %>' >
                        <ItemTemplate>
                                <li> 
                                <a href='<%# DefaultSanitizer.SanitizeHtml(GetViewGroupLink(  ((BaseViewGroupControl)Container.Parent.Parent).View ) + "#ResourceID-" + ((System.Data.DataRow)Container.DataItem)["ResourceID"]) %>' >
                                    <%# DefaultSanitizer.SanitizeHtml(((System.Data.DataRow)Container.DataItem)["ResourceTitle"]) %>
                                </a> 
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </td>
            <td>
             <ul>
               <asp:Repeater runat="server" ID="rptColumn2Resources" DataSource='<%# DefaultSanitizer.SanitizeHtml(this.Column2Resources) %>' >
                        <ItemTemplate>
                            <li> 
                                <a href='<%# DefaultSanitizer.SanitizeHtml(GetViewGroupLink( ((BaseViewGroupControl)Container.Parent.Parent).View ) + "#ResourceID-" + ((System.Data.DataRow)Container.DataItem)["ResourceID"]) %>' >
                                    <%# DefaultSanitizer.SanitizeHtml(((System.Data.DataRow)Container.DataItem)["ResourceTitle"]) %>
                                </a> 
                            </li>
                        </ItemTemplate>
                </asp:Repeater>
            </ul>
            </td>
            <td>
                  <ul>
                    <asp:Repeater runat="server" ID="rptColumn3Resources" DataSource='<%# DefaultSanitizer.SanitizeHtml(this.Column3Resources) %>' >
                        <ItemTemplate>
                            <li> 
                                <a href='<%# DefaultSanitizer.SanitizeHtml(GetViewGroupLink(  ((BaseViewGroupControl)Container.Parent.Parent).View ) + "#ResourceID-" + ((System.Data.DataRow)Container.DataItem)["ResourceID"]) %>' >
                                    <%# DefaultSanitizer.SanitizeHtml(((System.Data.DataRow)Container.DataItem)["ResourceTitle"]) %>
                                </a> 
                            </li>
                        </ItemTemplate>
                    </asp:Repeater> 
                </ul>
            </td>
        </tr>
    </table>
   
   
   
               
</div>
               
            

