﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web.Reporting;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common.Indications;
using SolarWinds.Orion.Core.Common.Notification;
using SolarWinds.Orion.Swis.PubSub;
using SolarWinds.Orion.Web.Settings;

public partial class Orion_Report : System.Web.UI.Page
{
    const string QueryStringParamReportIsReady = "showid";
    const string WebConfigParamSkipWaitThreshold = "ReportingSkipWaitThreshold";

    private OrionReport report;
    private bool printable;
    protected int ReportID { get; set; }
    protected string ReportName { get; set; }
    protected bool Printable
    {
        get
        {
            return printable;
        }
    }

    private string SanitizeHttpRequestObject(string key)
        => SolarWinds.Orion.Web.Helpers.WebSecurityHelper.SanitizeHtmlV2(Request[key]);

    protected void Page_Load(object sender, EventArgs e)
    {
		string reportId = SanitizeHttpRequestObject("ReportID");
		OrionReportBase reportBase = null;
		string reportName;
		if (!string.IsNullOrEmpty(reportId))
		{
			reportBase = OrionReportHelper.GetReportbyID(Convert.ToInt32(reportId));
            if (reportBase == null)
            {
                throw new Exception(string.Format(Resources.CoreWebContent.WEBDATA_VL0_56, reportId));
            }
            ReportID = reportBase.Id;
			if (string.IsNullOrEmpty(reportBase.LegacyPath))
			{
				ProcessAsNewReport(reportBase);
				return;
			}
		}

		if (reportBase == null)
		{
			reportName = SanitizeHttpRequestObject("Report");
			//doublecheck if its legacy report
			reportBase = OrionReportHelper.GetReportbyName(reportName);
            if (reportBase == null)
            {
                throw new Exception(string.Format(Resources.CoreWebContent.WEBDATA_VL0_55, reportName));
            }
            ReportID = reportBase.Id;
		}
		else
		{
			reportName = DefaultSanitizer.SanitizeHtml(reportBase.Name)?.ToString();
		}

		if (string.IsNullOrEmpty(reportBase.LegacyPath))
		{
            ProcessAsNewReport(reportBase);
            return;
		}

        bool.TryParse(Request["Printable"], out printable);
        if (Printable)
            PrintableLink.Visible = false;
        else
        {
            var printableUrl = string.Format("{0}{1}Printable=TRUE", Request.RawUrl, (Request.RawUrl.Contains("?") ? "&" : "?"));
            PrintableLink.NavigateUrl = SolarWinds.Orion.Web.Helpers.WebSecurityHelper.SanitizeHtmlV2(printableUrl);
        }
        this.ReportName = reportName;
        report = OrionReport.Load(reportName);

        Title = DefaultSanitizer.SanitizeHtml(report.Title)?.ToString();
        if (!String.IsNullOrEmpty(report.Subtitle))
        {
            this.subtitleText.Controls.Add(new Literal() { Text = DefaultSanitizer.SanitizeHtml(report.Subtitle).ToString() });
        }

        string dataFormat = Request["DataFormat"] ?? "";
        switch (dataFormat.ToLowerInvariant())
        {
            case "csv":
            case "cdf":
            case "raw":
                SendReport(new OrionReportCsvFormatter(), "text/csv", "csv"); // does not return
                break;

            case "txt":
            case "text":
                SendReport(new OrionReportTabDelimitedFormatter(), "text/plain", null); // does not return
                break;
            
            case "xml":
                SendReport(new OrionReportXmlFormatter(), "text/xml", null); // does not return
                break;
            
            case "xls":
            case "excel":
            case "excell":
                SendReport(new OrionReportExcelFormatter(), "application/vnd.ms-excel", "xls"); // does not return
                break;
            
            default:
                // html report. let the page generate normally
                break;
        }
    }

    private void ProcessAsNewReport(OrionReportBase reportBase)
    {
        if (reportBase == null)
            throw new ArgumentNullException("reportBase must not be null");
        
        string showid = Request.QueryString[QueryStringParamReportIsReady];

        Guid gid;

        SolarWinds.Orion.Core.Reporting.TempFiles.Details details = null;

        if (string.IsNullOrWhiteSpace(showid))
            gid = Guid.Empty;

        else if (Guid.TryParse(showid, out gid) == false)
            gid = Guid.Empty;

        else
        {
            details = SolarWinds.Orion.Core.Reporting.TempFiles.GetDetails(gid);

            if (details.Exists == false)
                gid = Guid.Empty;
        }

        // is the report ready for sending?

        if (gid != Guid.Empty && 
            details != null &&
            details.IsInProgress == false)
        {
            if(GeneralSettings.Instance.AuditReportGeneration)
                SendReportGeneratedNotification(reportBase.Name);

            // send the content out the port. do not pass go, go directly to jail.
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "text/html; charset=utf-8";
            Response.WriteFile(details.FullPath);
            Response.Flush();
            System.IO.File.Delete(details.FullPath);
            Response.End();
            return;
        }

        // no, so we have to make it. let's look at how long it took to render last.

        string schedulerTxt = Request.Params["X-SW-Scheduler"] ?? string.Empty;
        bool isScheduler = StringComparer.OrdinalIgnoreCase.Equals(schedulerTxt, "true");

        int? lastTime;
        int minRenderTimeToWait = GetSetting(WebConfigParamSkipWaitThreshold, 5);

        if (isScheduler)
            lastTime = 0; // report scheduler never waits!
        else if (minRenderTimeToWait < 1)
            lastTime = 0;
        else
            lastTime = SolarWinds.Orion.Web.DAL.ReportDAL.GetLastRenderTime(reportBase.Id);

        if (lastTime.HasValue && minRenderTimeToWait > 0 && lastTime < minRenderTimeToWait)
        {
            if (GeneralSettings.Instance.AuditReportGeneration)
                SendReportGeneratedNotification(reportBase.Name);

            var results = HttpUtility.ParseQueryString(Request.Url.Query);
            results["ReportID"] = reportBase.Id.ToString(CultureInfo.InvariantCulture);
            var url = "/Orion/Reports/Preview.aspx?" + results;
            Server.Transfer(url);
            return;
        }

        // ok, make 'em wait

        var ps = HttpUtility.ParseQueryString(Request.Url.Query);
        ps.Remove(QueryStringParamReportIsReady);
        ps["waitid"] = Guid.NewGuid().ToString("N");
        if (ps["ReportID"] == null)
            ps["ReportID"] = Convert.ToString(reportBase.Id);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Redirect("/Orion/ReportWait.aspx?" + ps);
    }

    private static void SendReportGeneratedNotification(string reportName)
    {
        var userName = HttpContext.Current.User.Identity.Name;
        var variables = new Dictionary<string, object>() { { "ReportName", reportName } };
        var notification = new CommonNotification(IndicationHelper.GetIndicationType(IndicationType.Orion_ReportGenerated), userName, variables);
        PublisherClient.Instance.Publish(notification);
    }

    private static int GetSetting(string name, int defaultValue)
    {
        var stringValue = System.Configuration.ConfigurationManager.AppSettings[name] ?? string.Empty;
        int value;
        if (!int.TryParse(stringValue, out value))
            value = defaultValue;

        return value;
    }

    void SendReport(OrionReportFormatter formatter, string contentType, string fileExtension)
    {
        if (GeneralSettings.Instance.AuditReportGeneration)
            SendReportGeneratedNotification(report.Title);

        string content = ReportRunner.Execute(
            report,
            formatter,
            Request["Sort"],
            Request["Grouping"],
            Request["Filter"],
            Request["NetObject"]);

        Response.Clear();
        Response.ContentType = contentType;

        if (!string.IsNullOrEmpty(fileExtension))
            Response.AddHeader("Content-disposition", string.Format("attachment; filename=\"{0}.{1}\"", report.Filename, fileExtension));

        Response.Write(content);
        Response.End(); // does not return
    }

    protected string GetReportHtml()
    {
        if (GeneralSettings.Instance.AuditReportGeneration)
            SendReportGeneratedNotification(report.Title);

        return ReportRunner.Execute(
            report,
            new OrionReportHtmlFormatter(Request),
            Request["Sort"],
            Request["Grouping"],
            Request["Filter"],
            Request["NetObject"]);
    }
}
