﻿using System;
using System.Linq;
using Newtonsoft.Json;
using SolarWinds.Orion.Web.Actions;
using SolarWinds.Orion.Core.Models.Actions;


public partial class Orion_Actions_Default : System.Web.UI.Page
{
    protected string Plugins { get; private set; }

    protected bool UserHasRightToEnableOrDisableAction
    {
        get { return Profile.AllowDisableAction; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var plugins = ActionManager.Instance.GetPlugins(ActionEnviromentType.Alerting).Where(p => p.Enabled);
        Plugins = ToJson(plugins);
    }

    public string ToJson(object objectGraph)
    {
        var settings = new JsonSerializerSettings();
        settings.TypeNameHandling = TypeNameHandling.Objects;

        return JsonConvert.SerializeObject(objectGraph, Formatting.Indented, settings);
    }
}