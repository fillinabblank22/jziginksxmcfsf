<%@ Page Language="C#" MasterPageFile="~/Orion/Alerts/AlertsMasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_Actions_Default" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_167%>" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Actions.Impl" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Controls" %>
<%@ Register Src="~/Orion/Controls/NavigationTabBar.ascx" TagPrefix="orion" TagName="NavigationTabBar" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" Assembly="OrionWeb" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/Actions/Controls/ActionNameControl.ascx" TagPrefix="orion" TagName="ActionNameControl" %>
<%@ Register Src="~/Orion/Actions/Controls/ExecutionSettingsControl.ascx" TagPrefix="orion" TagName="ExecutionSettingsControl" %>
<%@ Register Src="~/Orion/Actions/Controls/ActionTimeOfDayControl.ascx" TagPrefix="orion" TagName="ActionTimeOfDay" %>
<%@ Register Src="~/Orion/Actions/Controls/TestActionView.ascx" TagPrefix="orion" TagName="TestActionView" %>
<%@ Register Src="~/Orion/Controls/NetObjectPicker.ascx" TagPrefix="orion" TagName="NetObjectPicker" %>
<%@ Register TagPrefix="orion" TagName="MacroVariablePicker" Src="~/Orion/Controls/MacroVariablePicker.ascx" %>
<%@ Register tagPrefix="orion" tagName="AlertPicker" src="~/Orion/Alerts/Controls/AlertPicker.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headplaceholder" runat="server">
     <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
     <orion:Include ID="Include7" runat="server" File="Actions.css" />
     <orion:Include ID="Include2" runat="server" File="OrionCore.js" />
     <orion:Include ID="Include6" runat="server" File="RenderControl.js" />
     <orion:Include ID="Include3" runat="server" File="Actions/js/ActionManagerGrid.js" />
     <orion:Include ID="Include4" runat="server" File="Admin/js/SearchField.js" />
     <orion:Include ID="Include5" runat="server" File="Actions/js/ActionsMultiEditor.js" />
     <style type="text/css">
        #mainContent { margin: 0px 10px 15px 10px; }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="TopRightPageLinks" runat="server">
<div class="contTitleBarDate">
            <span class="activeAlert"><a href="../NetPerfMon/Alerts.aspx"> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_AY0_94) %></a></span>
		    <orion:IconHelpButton ID="btnHelp" runat="server"  HelpUrlFragment="orioncoreph_managealertactions" />
		    <div style="padding-top: 5px; font-size: 8pt; text-align: right;"><%= DateTime.Now.ToString("F") %></div>
	    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceholder" Runat="Server">
    <orion:ExecutionSettingsControl runat="server" ID="ExecutionSettingsControl" />
    <orion:ActionNameControl runat="server" ID="ActionNameControl" />   
    <orion:ActionTimeOfDay runat="server" ID="ActionTimeOfDay" UseTimePeriodMode="true" />   
    <orion:MacroVariablePicker ID="MacroVariablePicker" runat="server"/>
	<orion:TestActionView runat="server" ID="TestActionView" />
    <orion:AlertPicker ID="AlertPicker" runat="server" Title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AY0_102 %>" 
        GetHeaderTitleFn="SW.Orion.ActionManager.getAssignAlertsHeaderTitle"
        OnAlertsSelected="SW.Orion.ActionManager.alertsSelected"/>

<div id="mainContent">
    <h1 class="page-title" style="padding:15px 0px 0px 0px!important"><%= DefaultSanitizer.SanitizeHtml(this.Title) %></h1>
      <div>
             <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_77) %>
      </div>
      <br/>
      <orion:NavigationTabBar ID="AlertListNavigationTabBar" runat="server" Visible="true">   
            <orion:NavigationTabItem Name="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_YK0_75 %>" Url="~/Orion/Alerts/Default.aspx" />
            <orion:NavigationTabItem Name="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_YK0_76 %>" Url="~/Orion/Actions/Default.aspx" />
       </orion:NavigationTabBar>
     <input type="hidden" name="ActionManager_PageSize" id="ActionManager_PageSize" value='<%= DefaultSanitizer.SanitizeHtml(WebUserSettingsDAL.Get("ActionManager_PageSize")) %>' />     
     <input type="hidden" name="ActionManager_SelectedColumns" id="ActionManager_SelectedColumns" value='<%= DefaultSanitizer.SanitizeHtml(WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, "ActionManager_SelectedColumns",WebConstants.ActionDafultSelectedColumns)) %>' />
     <input type="hidden" name="ActionManager_SortOrder" id="ActionManager_SortOrder" value='<%= DefaultSanitizer.SanitizeHtml(WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, "ActionManager_SortOrder","Title,Asc")) %>' />
     <input type="hidden" name="ActionManager_GroupingValue" id="ActionManager_GroupingValue" value='<%= DefaultSanitizer.SanitizeHtml(HttpContext.Current.Server.HtmlEncode(WebUserSettingsDAL.Get("ActionManager_GroupingValue"))) %>' />   
     <input type="hidden" name="SelectedActionIDs" id="SelectedActionIDs" value='<%= DefaultSanitizer.SanitizeHtml(Request["ActionIds"]) %>' /> 
     <input type="hidden" name="SelectedGrouping" id="SelectedGrouping" value='<%= DefaultSanitizer.SanitizeHtml(Request["Grouping"]) %>' />   
     <input type="hidden" name="ActionManager_UserHasRightToEnableOrDisableAction" id="ActionManager_UserHasRightToEnableOrDisableAction" value='<%= UserHasRightToEnableOrDisableAction %>' />
     <input type="hidden" name="plugins" id="plugins" value='<%= DefaultSanitizer.SanitizeHtml(Plugins) %>' /> 
     <input type="hidden" name="escalationLevelPropertyName" id="escalationLevelPropertyName" value='<%= DefaultSanitizer.SanitizeHtml(CommonActionConstants.EscalationLevelPropertyName) %>'/>
      <div ng-non-bindable id="ActionManagerGrid"></div>
</div>
  <div id="dialogContainer" class="actionsOffPage dialogContainer" >
</div>
<div style="display: none">
    <orion:NetObjectPicker runat="server" ID="NetObjectPicker" />
</div>
</asp:Content>
