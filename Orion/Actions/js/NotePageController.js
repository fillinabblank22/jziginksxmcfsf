﻿SW.Core.namespace("SW.Core.Actions").NotePageController = function (config) {
    "use strict";

    // Private methods
    var getMessage = function () {
        return $container.find('[data-form="' + config.dataFormMapping.message + '"]').val();
    };
    var getFrom = function () {
        return $container.find('[data-form="' + config.dataFormMapping.from + '"]').val();
    };
    var getRecipients = function () {
        if ($container.find('[data-form="' + config.dataFormMapping.recipients + '"]').val() == "") {
            return new Array();
        }
        return $container.find('[data-form="' + config.dataFormMapping.recipients + '"]').val().split(',');
    };

    function isEditable(key) {
        return !config.multiEditMode || $container.find('[data-edited="' + key + '"] input:checked').length == 1;
    }

    function validateIsEmpty(dataForm) {
        var isValid = true;
        if (isEditable(dataForm)) {
            var element = $container.find('[data-form="' + dataForm + '"]');
            var value = element.val();
            if (value.trim().length == 0) {
                element.addClass('invalidValue');
                isValid = false;
            } else {
                element.removeClass('invalidValue');
                isValid = true;
            }
        }
        return isValid;
    }

    var validateNotePageSection = function () {
        var isValid = validateIsEmpty(config.dataFormMapping.message);
        if (isEditable(config.dataFormMapping.recipients)) {
            var recipients = getRecipients();
            var recipientsElement = $container.find('[data-form="' + config.dataFormMapping.recipients + '"]');
            if (recipients.length == 0) {
                recipientsElement.addClass('invalidValue');
                isValid = false;
            } else {
                recipientsElement.removeClass('invalidValue');
                isValid = isValid && true;
            }
        }
        isValid = validateIsEmpty(config.dataFormMapping.from) && isValid;
        return isValid;
    };

    var createConfiguration = function () {

        var configuration = {
            Recipients: getRecipients(),
            From: getFrom(),
            Message: getMessage()
        };
        return configuration;
    };

    function setSelectedRecipients() {
        var recipientsArray = new Array();
        if ($container.find('[data-form="' + config.dataFormMapping.recipients + '"]').val() != "") {
            $.each($container.find('[data-form="' + config.dataFormMapping.recipients + '"]').val().split(","), function () {
                recipientsArray.push({ name: this.toString() });
            });
        }
        SW.Orion.NotePageRecipientsSelector.setSelectedRecipients(recipientsArray);
    }
    function showRecipientsDialog(buttonElement) {

        if ($(buttonElement).hasClass('sw-btn-disabled'))
            return;

        var $elem = $("#selectRecipients");
        setSelectedRecipients();
        $("#selectRecipients").dialog({
            modal: true,
            draggable: true,
            resizable: false,
            position: ['center', 'center'],
            width: 420,
            title: "Recipients",
            dialogClass: 'ui-dialog-osx',
            autoOpen: true,
            close: function (ev, ui) {
                $("#selectRecipients .buttons").empty();
            }
        });
        $(SW.Core.Widgets.Button("Cancel", { type: 'secondary', id: 'cancelBtn' })).appendTo("#selectRecipients .buttons").css('float', 'right').css('margin', '10px').click(function () {
            setSelectedRecipients();
            $elem.dialog("close");
        });
        $(SW.Core.Widgets.Button("OK", { type: 'primary', id: 'OkBtn' })).appendTo("#selectRecipients .buttons").css('float', 'right').css('margin', '10px').click(function () {
            var selectedRecipients = SW.Orion.NotePageRecipientsSelector.getSelectedRecipients();
            var displayString = "";
            $.each(selectedRecipients, function () {
                displayString += this.data.name;
                if (selectedRecipients.indexOf(this) != selectedRecipients.length - 1) {
                    displayString += ",";
                }
            });
            $container.find('[data-form="' + config.dataFormMapping.recipients + '"]').val(displayString);
            $elem.dialog("close");
        });
        $elem.removeClass("dialogOffPage");
    };

    var createActionAndExecuteCallback = function (configuration, callback) {
        callActionAndExecuteCallback("Create", configuration, callback);
    };

    var updateActionAndExecuteCallback = function (action, configuration, callback) {

        configuration.Action = action;
        callActionAndExecuteCallback("Update", configuration, callback);
    };

    var callActionAndExecuteCallback = function (action, param, callback) {

        SW.Core.Services.callController("/api/NotePage/" + action, param,
        // On success
            function (response) {
                if ($.isFunction(callback)) {
                    callback({ isError: false, actionDefinition: response });
                }
            },
        // On error
            function (msg) {
                if ($.isFunction(callback)) {
                    callback({ isError: true, ErrorMessage: msg });
                }
            }
        );
    };

    // Public methods

    this.init = function () {
        if ($.isFunction(config.onReadyCallback)) {
            config.onReadyCallback(self);
        }
        if (config.isActionSupported == false) {
            $("#createActionDefinitionBtn").hide();
            $("#nextSectionInActionDefinitionBtn").hide();
            $("#addActionDefinitionBtn").hide();
            $('#_basicActionPlugins_').remove();
            $('#_topActionPlugins_').remove();
            $('#_timeOfDayActionPlugins_').remove();
        } else {
            $container = $('#' + config.containerID);
            SW.Orion.NotePageRecipientsSelector.ShowDialod = showRecipientsDialog;

            var $manageRecipientsButton = $(".manageRecipientsButton");
            if (config.multiEditMode) {
                for (var key in config.dataFormMapping) {
                    var dataform = config.dataFormMapping[key];
                    if (dataform == config.dataFormMapping.recipients) {
                        $container.find('[data-form="' + dataform + '"]').attr("disabled", true);
                        $manageRecipientsButton.addClass('sw-btn-disabled');
                        $container.find('[data-edited="' + dataform + '"]').click(function() {
                            var dataedited = $(this).attr("data-edited");
                            $container.find('[data-form="' + dataedited + '"]').attr("disabled", !$(this).find("input").is(':checked'));
                            if (!$(this).find("input").is(':checked'))
                                $manageRecipientsButton.addClass('sw-btn-disabled');
                            else
                                $manageRecipientsButton.removeClass('sw-btn-disabled');
                        });
                    } else {
                        $container.find('[data-form="' + dataform + '"]').attr("disabled", true);
                        $container.find('[data-edited="' + dataform + '"]').click(function() {
                            var dataedited = $(this).attr("data-edited");
                            $container.find('[data-form="' + dataedited + '"]').attr("disabled", !$(this).find("input").is(':checked'));
                        });
                    }
                }
            }
        }
    };

    // Validate action configuration. 
    this.validateSectionAsync = function (sectionID, callback) {
        var valid = false;
        valid = validateNotePageSection();

        if ($.isFunction(callback)) {
            callback(valid);
        }
    };

    this.getUpdatedActionProperies = function (callback) {
        var jsonProperties = new Array();
        for (var key in config.dataFormMapping) {
            var dataform = config.dataFormMapping[key];
            if ($container.find('[data-edited="' + dataform + '"] input:checked').length == 1)
                if (dataform == config.dataFormMapping.recipients)
                    jsonProperties.push({ PropertyName: dataform, PropertyValue: JSON.stringify(getRecipients()) });
                else
                    jsonProperties.push({ PropertyName: dataform, PropertyValue: $container.find('[data-form="' + dataform + '"]').val() });
        }
        if (callback) {
            callback(JSON.stringify(jsonProperties));
        }
    };

    this.getActionDefinitionAsync = function (callback) {

        var configuration = createConfiguration();

        // Edit mode.
        if (config.actionDefinition) {
            // We have action definition we just want to updated existing one.
            updateActionAndExecuteCallback(config.actionDefinition, configuration, callback);

        } else {
            // New action definition mode.
            createActionAndExecuteCallback(configuration, callback);
        }
    };

    // Constructor

    var self = this;
    var $container = null;
}
