﻿SW.Core.namespace("SW.Core.Actions").NetMessageController = function (config) {
    "use strict";
    var validHostnameOrIpRegex = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$|^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])$|^(?:(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){6})(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:::(?:(?:(?:[0-9a-fA-F]{1,4})):){5})(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})))?::(?:(?:(?:[0-9a-fA-F]{1,4})):){4})(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){0,1}(?:(?:[0-9a-fA-F]{1,4})))?::(?:(?:(?:[0-9a-fA-F]{1,4})):){3})(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){0,2}(?:(?:[0-9a-fA-F]{1,4})))?::(?:(?:(?:[0-9a-fA-F]{1,4})):){2})(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){0,3}(?:(?:[0-9a-fA-F]{1,4})))?::(?:(?:[0-9a-fA-F]{1,4})):)(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){0,4}(?:(?:[0-9a-fA-F]{1,4})))?::)(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){0,5}(?:(?:[0-9a-fA-F]{1,4})))?::)(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){0,6}(?:(?:[0-9a-fA-F]{1,4})))?::))))$";

    function validateConfiguration(configuration) {
        var isValid = true;
        $container.find('[data-form="' + config.dataFormMapping.address + '"]').removeClass("invalidValue");
        $container.find('[data-form="' + config.dataFormMapping.message + '"]').removeClass("invalidValue");

        if (config.multiEditMode === false || $container.find('[data-edited="' + config.dataFormMapping.address + '"]').is(':checked')) {
            if (!configuration.SendToAllInDomainOrWorkgroup) {
                isValid = validateAddressInput(configuration.Address);
                if (isValid === false) {
                    $container.find('[data-form="' + config.dataFormMapping.address + '"]').addClass("invalidValue");
                }
            }
        }
        if (config.multiEditMode === false || $container.find('[data-edited="' + config.dataFormMapping.message + '"]').is(':checked')) {
            if (configuration.Message.trim() === "") {
                isValid = false;
                $container.find('[data-form="' + config.dataFormMapping.message + '"]').addClass("invalidValue");
            }
        }
        return isValid;
    };

    function isCorrect(val) {
        if (val.match(validHostnameOrIpRegex) == null)
            return false;
        else
            return true;
    }

    function validateAddressInput(value) {
        var result = true;
        if (value !== "") {
            $.each(value.split(','), function () {
                if (!isCorrect($.trim(this))) {
                    result = false;
                }
            });
            return result;
        }
        return false;
    }

    function validateTestConfiguration(configuration) {
        var isValid = true;
        $container.find('[data-form="' + config.dataFormMapping.address + '"]').removeClass("invalidValue");
        $container.find('[data-form="' + config.dataFormMapping.message + '"]').removeClass("invalidValue");
        if (!configuration.SendToAllInDomainOrWorkgroup) {
            isValid = validateAddressInput(configuration.Address);
            if (isValid === false) {
                $container.find('[data-form="' + config.dataFormMapping.address + '"]').addClass("invalidValue");
            }
        }
        return isValid;
    }

    function getAddressValue() {
        var sendToAllChecked = $container.find('[data-form="' + config.dataFormMapping.sendToAllInDomainOrWorkgroup + '"]').is(':checked');
        var address = $container.find('[data-form="' + config.dataFormMapping.address + '"]').val();
        if (sendToAllChecked === true) {
            address = '*';
        }
        return address;
    }

    var createConfiguration = function () {
        var configuration = {
            Address: getAddressValue(),
            SendToAllInDomainOrWorkgroup: $container.find('[data-form="' + config.dataFormMapping.sendToAllInDomainOrWorkgroup + '"]').is(':checked'),
            Message: $container.find('[data-form="' + config.dataFormMapping.message + '"]').val()
        };
        return configuration;
    };

    var callActionAndExecuteCallback = function (action, param, callback) {
        SW.Core.Services.callController("/api/NetMessage/" + action, param,
        // On success
            function (response) {
                if ($.isFunction(callback)) {
                    callback({ isError: false, actionDefinition: response });
                }
            },
        // On error
            function (msg) {
                if ($.isFunction(callback)) {
                    callback({ isError: true, ErrorMessage: msg });
                }
            }
        );
    };

    var testSendMessage = function () {
        if (config.multiEditMode === true && !$container.find('[data-edited="' + config.dataFormMapping.address + '"]').is(':checked')) {
            return false;
        }
        var configuration = createConfiguration();
        var isValid = validateTestConfiguration(configuration);
        if (isValid === true) {
            callTestActionAndExecuteCallback(configuration);
        }
        return false;
    };

    var callTestActionAndExecuteCallback = function (param) {
        $container.find('#testProgress').show();
        $container.find('#testResult').hide();
        SW.Core.Services.callController("/api/NetMessage/TestSendMessage", param,
        // On success
            function (response) {
                $container.find('#testProgress').hide();
                if (response == true) {
                    $container.find('#testResult').html('<div class="sw-suggestion sw-suggestion-pass" style="margin: 0px;"><span class="sw-suggestion-icon"></span>@{R=Core.Strings;K=WEBJS_AY0_6;E=js}</div>');
                } else {
                    $container.find('#testResult').html('<div class="sw-suggestion sw-suggestion-fail" style="margin-bottom: 0px;"><span class="sw-suggestion-icon"></span>@{R=Core.Strings;K=WEBJS_AY0_7;E=js}</div>');
                }
                $container.find('#testResult').show();
            }
        );
    };

    function sendToAllInDomainOrWorkgroupKeyChanged() {
        var addressInput = $container.find('[data-form="' + config.dataFormMapping.address + '"]');
        if ($container.find('[data-form="' + config.dataFormMapping.sendToAllInDomainOrWorkgroup + '"]').is(':checked')) {
            addressInput.val('');
            addressInput.attr('disabled', 'disabled');
        } else {
            addressInput.removeAttr('disabled');
        }
    }

    function addressMultiEditModeChanged() {
        if ($container.find('[data-edited="' + config.dataFormMapping.address + '"]').is(':checked')) {
            $container.find('[data-form="' + config.dataFormMapping.address + '"]').removeAttr('disabled');
            $container.find('[data-form="' + config.dataFormMapping.sendToAllInDomainOrWorkgroup + '"]').removeAttr('disabled');
        } else {
            $container.find('[data-form="' + config.dataFormMapping.address + '"]').val('');
            $container.find('[data-form="' + config.dataFormMapping.address + '"]').attr('disabled', 'disabled');
            $container.find('[data-form="' + config.dataFormMapping.sendToAllInDomainOrWorkgroup + '"]').attr('disabled', 'disabled');
        }
    }

    function messageMultiEditModeChanged() {
        if ($container.find('[data-edited="' + config.dataFormMapping.message + '"]').is(':checked')) {
            $container.find('[data-form="' + config.dataFormMapping.message + '"]').removeAttr('disabled');
        } else {
            $container.find('[data-form="' + config.dataFormMapping.message + '"]').val('');
            $container.find('[data-form="' + config.dataFormMapping.message + '"]').attr('disabled', 'disabled');
        }
    }

    this.init = function () {
        if ($.isFunction(config.onReadyCallback)) {
            config.onReadyCallback(self);
        }
        if (config.isActionSupported == false) {
            $("#createActionDefinitionBtn").hide();
            $("#nextSectionInActionDefinitionBtn").hide();
            $("#addActionDefinitionBtn").hide();
            $('#_basicActionPlugins_').remove();
            $('#_topActionPlugins_').remove();
            $('#_timeOfDayActionPlugins_').remove();
        } else {
            $container = $('#' + config.containerID);
            $container.find('[data-form="' + config.dataFormMapping.sendToAllInDomainOrWorkgroup + '"]').change(sendToAllInDomainOrWorkgroupKeyChanged);
            sendToAllInDomainOrWorkgroupKeyChanged();
            $container.find('#' + config.testSendNetMessageClientId).click(testSendMessage);

            if (config.multiEditMode === true) {
                $container.find('[data-edited="' + config.dataFormMapping.address + '"]').change(addressMultiEditModeChanged);
                $container.find('[data-edited="' + config.dataFormMapping.message + '"]').change(messageMultiEditModeChanged);
                addressMultiEditModeChanged();
                messageMultiEditModeChanged();
            }
        }
    };
    // Constructor

    // Validate action configuration. 
    this.validateSectionAsync = function (sectionID, callback) {
        var configuration = createConfiguration();
        var isValid = validateConfiguration(configuration);
        if ($.isFunction(callback)) {
            callback(isValid);
        }
    };

    this.getActionDefinitionAsync = function (callback) {
        var configuration = createConfiguration();
        // Edit mode.
        if (config.actionDefinition) {
            //We have action definition we just want to updated existing one.
            var updateConfig = {
                Action: config.actionDefinition,
                Configuration: configuration
            };
            callActionAndExecuteCallback("Update", updateConfig, callback);
        } else {
            // New action definition mode.
            callActionAndExecuteCallback("Create", configuration, callback);
        }
    };

    this.getUpdatedActionProperies = function (callback) {
        var jsonProperties = new Array();
        if ($container.find('[data-edited="' + config.dataFormMapping.address + '"]').is(':checked')) {
            jsonProperties.push({ PropertyName: config.dataFormMapping.address, PropertyValue: getAddressValue() });
            jsonProperties.push({ PropertyName: config.dataFormMapping.sendToAllInDomainOrWorkgroup, PropertyValue: $container.find('[data-form="' + config.dataFormMapping.sendToAllInDomainOrWorkgroup + '"]').is(':checked') });
        }

        if ($container.find('[data-edited="' + config.dataFormMapping.message + '"]').is(':checked')) {
            jsonProperties.push({ PropertyName: config.dataFormMapping.message, PropertyValue: $container.find('[data-form="' + config.dataFormMapping.message + '"]').val() });
        }

        if (callback) {
            callback(JSON.stringify(jsonProperties));
        }
    };

    var self = this;
    var $container = null;
}
