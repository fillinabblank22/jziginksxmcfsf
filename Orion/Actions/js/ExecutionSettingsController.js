﻿SW.Core.namespace("SW.Core.Actions").ExecutionSettingsController = function (config) {
    "use strict";

    this.getRepeatTimeSpan = function () {
        var type = $container.find('[data-form="executionTimeSpanType"] select').val();
        var timeSpan = $container.find('[data-form="executionRepeatTimeSpan"] input').val();
        var enabled = $container.find('[data-form="executionRepeatTimeSpanEnable"] input').is(':checked');
        var result = 0;
        result = timeSpan;
        switch(type) {
            case "Days":
                result = timeSpan * 60 * 24;
                break;
            case "Hours":
                result = timeSpan * 60;
                break;
        }
        if (!enabled) {
            result = 0;
        }
        result = parseInt(result);
        return result;
    };

    this.getProperties = function () {       
        var result = [];
        if ($multiEditEnable == true && $multiTimeSpan.is(':checked') == true) {
            result.push({ "PropertyName": "executionRepeatTimeSpan", "PropertyValue": this.getRepeatTimeSpan() });
        }

        if ($multiEditEnable == true && $multiIfAknowlage.is(':checked') == true) {
            result.push({ "PropertyName": "executionIfAknowledge", "PropertyValue": $container.find('[data-form="executionIfAknowledge"] input').is(':checked') });
        }
        if ($multiEditEnable == false) {
            result.push({ "PropertyName": "executionIfAknowledge", "PropertyValue": $container.find('[data-form="executionIfAknowledge"] input').is(':checked') });
            result.push({ "PropertyName": "executionRepeatTimeSpan", "PropertyValue": this.getRepeatTimeSpan() });
        }
        return result;
    };

    this.init = function () {
        $container = config.containerID;
        $container.find('[data-form="executionRepeatTimeSpan"] input').keypress(function (ev) {
            return isNumber(ev);
        });

        $multiIfAknowlage = $container.find('[data-edited="executionIfAknowledgeEnabled"] input');
        $multiTimeSpan = $container.find('[data-edited="executionRepeatTimeSpan"] input');

        try {
            $multiEditEnable = (config.IsMultiEdit.toString().toLowerCase() === "true");
        } catch (e) {
            $multiEditEnable = false;
        }

        var execIfAknowledge = false;
        var timeSpan = 0;
        if ($multiEditEnable == false) {
            if (config.ActionDefinition != undefined) {
                execIfAknowledge = $.grep(config.ActionDefinition.ActionProperties, function (e) { return e.PropertyName == "executionIfAknowledge"; })[0].PropertyValue;
                timeSpan = setTimePriod($.grep(config.ActionDefinition.ActionProperties, function (e) { return e.PropertyName == "executionRepeatTimeSpan"; })[0].PropertyValue);
            } else {
            	execIfAknowledge = true;	// true by default
            }
        }
        execIfAknowledge = (execIfAknowledge.toString().toLowerCase() === "true");


        showHidefrequencyControl();
        $container.find("#cbRepeatActionEnable").click(showHidefrequencyControl);
        if (timeSpan > 0) {
            $container.find('[data-form="executionRepeatTimeSpanEnable"] input').prop("checked", true);
            showHidefrequencyControl();
        }

        if ($multiEditEnable == true) {
            $container.find('[data-edited="executionRepeatTimeSpan"]').show();
            $container.find('[data-edited="executionIfAknowledgeEnabled"]').show();
            $container.find('[data-form="executionRepeatTimeSpan"] input').attr("disabled", true);
            $container.find('[data-form="executionIfAknowledge"] input').attr("disabled", true);
            $container.find('[data-form="executionTimeSpanType"] select').attr("disabled", true);
            $container.find('[data-form="executionRepeatTimeSpanEnable"] input').attr("disabled", true);
            showHidefrequencyControl();
        }
        else {
            $container.find('[data-edited="executionRepeatTimeSpan"]').hide();
            $container.find('[data-edited="executionIfAknowledgeEnabled"]').hide();
        }
        $container.find('[data-edited="executionRepeatTimeSpan"]').click(function () {
            $container.find('[data-form="executionRepeatTimeSpan"] input').attr("disabled", !$multiTimeSpan.is(':checked'));
            $container.find('[data-form="executionTimeSpanType"] select').attr("disabled", !$multiTimeSpan.is(':checked'));
            $container.find('[data-form="executionRepeatTimeSpanEnable"] input').attr("disabled", !$multiTimeSpan.is(':checked'));
        });
        $container.find('[data-edited="executionIfAknowledgeEnabled"]').click(function () {
            $container.find('[data-form="executionIfAknowledge"] input').attr("disabled", !$multiIfAknowlage.is(':checked'));
        });

        $container.find('[data-form="executionRepeatTimeSpan"] input').val(timeSpan);
        $container.find('[data-form="executionIfAknowledge"] input').prop("checked", execIfAknowledge);
    };

    this.validate = function () {
        
        var valid = true;
        $container.find('[data-form="executionRepeatTimeSpan"] input').removeClass("invalidValue");
        if (isNaN(this.getRepeatTimeSpan())) {
            $container.find('[data-form="executionRepeatTimeSpan"] input').addClass("invalidValue");
            valid = false;
        }
        return valid;
    };

    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    function showHidefrequencyControl() {

        if ($container.find("#cbRepeatActionEnable").is(":checked")) {
            $container.find("#waitContainer").show();
        } else {
            $container.find("#waitContainer").hide();
        }
    }

    function setTimePriod(timePeriod) {
        var result = timePeriod;

        if (timePeriod == 0) {
            $container.find('[data-form="executionTimeSpanType"] select').val('Minutes');
            return result;
        }

        if (timePeriod % 60 == 0) {
            $container.find('[data-form="executionTimeSpanType"] select').val('Hours');
            result = timePeriod / 60;
        }

        if (timePeriod % 1440 == 0) {
            $container.find('[data-form="executionTimeSpanType"] select').val('Days');
            result = timePeriod / 1440;
        }
        return result;
    };

    this.getSectionAnnotation = function() {
        var annotation = "";

        if ($($('[data-form="executionIfAknowledge"] input[type="checkbox"]')[1]).is(":checked")) {
            annotation = "@{R=Core.Strings.2;K=WEBJS_OS0_1;E=js}";
        } else {
            annotation = "@{R=Core.Strings.2;K=WEBJS_OS0_2;E=js}";
        }

        if ($('[data-form="executionRepeatTimeSpanEnable"] input[type="checkbox"]').is(":checked")) {
            var selectedTime = "";
            $.each($('#ExecutionSettingsPlugin [id*=ddTimePeriodType] option'), function(i) {
                if (i > 2) {//skip first three as duplicated
                    if ($($('#ExecutionSettingsPlugin [id*=ddTimePeriodType] option')[i]).is(":selected")) {
                        selectedTime = $($('#ExecutionSettingsPlugin [id*=ddTimePeriodType] option')[i]).text();
                        return false;
                    }
                }
            });
            var repeatText = String.format("@{R=Core.Strings.2;K=WEBJS_OS0_4;E=js}",
                $($("#ExecutionSettingsPlugin .executionSettings_repeatTimeSpan input[name$='tbTimeToExecute_textBox']")[1]).val(),
                selectedTime);
            annotation = annotation == '' ? repeatText : String.format("@{R=Core.Strings;K=LIBCODE_AB0_9;E=js}", annotation, repeatText);
        }

        return annotation;
    }

    var $container = null;
    var $multiTimeSpan = null;
    var $multiIfAknowlage = null;
    var $multiEditEnable = false;
}