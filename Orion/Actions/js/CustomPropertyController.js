﻿SW.Core.namespace("SW.Core.Actions").CustomPropertyController = function (config) {
    "use strict";

    var validateConfiguration = function (configuration) {
        var isValid = true;
        $container.find('[data-form="' + config.dataFormMapping.name + '"]').removeClass("invalidValue");
        if (configuration.Name === '') {
            $container.find('[data-form="' + config.dataFormMapping.name + '"]').addClass("invalidValue");
            isValid = false;
        }
        return isValid;
    };

    var createConfiguration = function () {
        var propertyValueConfiguration = null;
        if (!config.hasError) {
            propertyValueConfiguration = SW.Core.EditCustomPropertyValueController.createConfiguration().Value;
        }

        var activeObjectType = $container.find('[data-form="' + config.dataFormMapping.activeObjectType + '"]').val();
        var parentObjectType = $container.find('[data-form="' + config.dataFormMapping.parentObjectType + '"]').val();
        var name = $container.find('[data-form="' + config.dataFormMapping.name + '"]').val();
        if (activeObjectType === parentObjectType) {
            name = $container.find('[data-form="' + config.dataFormMapping.parentName + '"]').val();
        }
        var configuration = {
            ActiveObjectType: activeObjectType,
            ParentObjectType: parentObjectType,
            ObjectType: $container.find('[data-form="' + config.dataFormMapping.objectType + '"]').val(),
            Name: name,
            Value: propertyValueConfiguration
        };
        return configuration;
    };

    var createViewConfiguration = function () {
        var activeObjectType = $container.find('[data-form="' + config.dataFormMapping.activeObjectType + '"]').val();
        var name;
        if (activeObjectType === $container.find('[data-form="' + config.dataFormMapping.parentObjectType + '"]').val()) {
            name = $container.find('[data-form="' + config.dataFormMapping.parentName + '"]').val();
        } else {
            name = $container.find('[data-form="' + config.dataFormMapping.name + '"]').val();
        }
        var configuration = {
            ObjectType: activeObjectType,
            Name: name
        };
        return configuration;
    };

    var loadCustomPropertyControl = function () {
        var configuration = createViewConfiguration();
        $container.find('#testProgress').show();
        $container.find('#customProperyValueContainer').html('<div id="customProperyValueContainerDiv"></div>');
        SW.Core.Loader.Control('#customProperyValueContainerDiv',
            {
                Control: config.customPropertyViewPath
            },
            {
                'config': {
                    'EnviromentType': config.enviromentType,
                    'OnReadyJsCallback': config.onPluginReadyFunctionName,
                    'ViewContextJsonString': JSON.stringify(config.viewContext),
                    'ActionDefinitionJsonString': JSON.stringify(config.actionDefinition),
                    'MultiEditEnabled': (config.mutiEditMode == true),
                    'ViewContextConfiguration': configuration
                }
            },
            'replace',
            function () { alert('Error!!!'); },
            function() {
                $container.find('#testProgress').hide();
                if (SW.Core.MacroVariablePickerController) {
                    if (config.multiEditMode) {
                        SW.Core.MacroVariablePickerController.DisableInsertVariableButtons();
                    } else {
                        SW.Core.MacroVariablePickerController.EnableInsertVariableButtons();
                    }
                }
            }
        );
    };

    var enableDisableEditFields = function(value) {
        var parentCustomPropertyRadio = $container.find('[data-edited="parentCustomPropertyRadio"] input');
        var customPropertyRadio = $container.find('[data-edited="customPropertyRadio"] input');
        parentCustomPropertyRadio.prop('disabled', value);
        customPropertyRadio.prop('disabled', value);

        if (parentCustomPropertyRadio.is(':checked') || parentCustomPropertyRadio.length == 0)
            $container.find('[data-form="' + config.dataFormMapping.parentName + '"]').prop('disabled', value);
        if (customPropertyRadio.length == 0 || customPropertyRadio.is(':checked'))
            $container.find('[data-form="' + config.dataFormMapping.name + '"]').prop('disabled', value);
        $container.find('[data-edited="EditorValue"]').prop('disabled', value);
        if (value)
            $container.find('[data-edited="EditorValue"]').removeClass("invalidValue");
    };

    function isEditable(key) {
        return !config.multiEditMode || $container.find('[data-edited="' + key + '"] input:checked').length == 1;
    }

    //Constructor
    this.init = function () {
        $container = $('#' + config.containerID);
        if ($.isFunction(config.onReadyCallback)) {
            config.onReadyCallback(self);
        }
        if (config.isActionSupported == false) {
            $("#createActionDefinitionBtn").hide();
            $("#nextSectionInActionDefinitionBtn").hide();
            $("#addActionDefinitionBtn").hide();
            $('#_basicActionPlugins_').remove();
            $('#_topActionPlugins_').remove();
            $('#_timeOfDayActionPlugins_').remove();
        } else {
            $("#" + config.dataContainerID).toggle(!config.hasError);

            $container.find('[data-form="' + config.dataFormMapping.name + '"]').change(loadCustomPropertyControl);
            $container.find('[data-form="' + config.dataFormMapping.parentName + '"]').change(loadCustomPropertyControl);

            $container.find('[data-edited="parentCustomPropertyRadio"]').change(function() {
                $container.find('[data-form="' + config.dataFormMapping.name + '"]').prop('disabled', true);;
                $container.find('[data-form="' + config.dataFormMapping.parentName + '"]').prop('disabled', false);

                var parentType = $container.find('[data-form="' + config.dataFormMapping.parentObjectType + '"]').val();
                $container.find('[data-form="' + config.dataFormMapping.activeObjectType + '"]').val(parentType);
                loadCustomPropertyControl();
            });

            $container.find('[data-edited="customPropertyRadio"]').change(function() {
                $container.find('[data-form="' + config.dataFormMapping.name + '"]').prop('disabled', false);
                $container.find('[data-form="' + config.dataFormMapping.parentName + '"]').prop('disabled', true);

                var type = $container.find('[data-form="' + config.dataFormMapping.objectType + '"]').val();
                $container.find('[data-form="' + config.dataFormMapping.activeObjectType + '"]').val(type);
                loadCustomPropertyControl();
            });

            if (config.multiEditMode) {
                $container.find('[data-edited="' + config.dataFormMapping.name + '"]').change(function() {
                    enableDisableEditFields(!$(this).find("input").is(':checked'));
                });
                enableDisableEditFields(true);
            }
        }
    };

    this.validateSectionAsync = function (sectionID, callback) {
        var isValid = true;
        if (isEditable(config.dataFormMapping.name)) {
            var configuration = createConfiguration();
            isValid = validateConfiguration(configuration);
            if (isValid === true && !config.hasError) {
                isValid = SW.Core.EditCustomPropertyValueController.validateSectionAsync(sectionID, callback, config.multiEditMode == true);
            }
        }
        if ($.isFunction(callback)) {
            callback(isValid);
        }
    };

    this.getActionDefinitionAsync = function (callback) {
        var configuration = createConfiguration();
        // Edit mode.
        if (config.actionDefinition) {
            //We have action definition we just want to updated existing one.
            var updateConfig = {
                Action: config.actionDefinition,
                Configuration: configuration
            };
            callActionAndExecuteCallback("Update", updateConfig, callback);
        } else {
            // New action definition mode.
            callActionAndExecuteCallback("Create", configuration, callback);
        }
    };

    var callActionAndExecuteCallback = function (action, param, callback) {
        SW.Core.Services.callController("/api/CustomProperty/" + action, param,
        // On success
            function (response) {
                if ($.isFunction(callback)) {
                    callback({ isError: false, actionDefinition: response });
                }
            },
        // On error
            function (msg) {
                if ($.isFunction(callback)) {
                    callback({ isError: true, ErrorMessage: msg });
                }
            }
        );
    };

    this.getUpdatedActionProperies = function (callback) {
        var configuration = createConfiguration();
        var jsonProperties = new Array();

        if (!config.hasError && isEditable(config.dataFormMapping.name)) {
            jsonProperties.push({ PropertyName: config.dataFormMapping.activeObjectType, PropertyValue: configuration.ActiveObjectType });
            jsonProperties.push({ PropertyName: config.dataFormMapping.parentObjectType, PropertyValue: configuration.ParentObjectType });
            jsonProperties.push({ PropertyName: config.dataFormMapping.objectType, PropertyValue: configuration.ObjectType });
            jsonProperties.push({ PropertyName: config.dataFormMapping.name, PropertyValue: configuration.Name });
            jsonProperties.push({ PropertyName: config.dataFormMapping.value, PropertyValue: configuration.Value });
        }

        if (callback) {
            callback(JSON.stringify(jsonProperties));
        }
    };

    var self = this;
    var $container = null;
}