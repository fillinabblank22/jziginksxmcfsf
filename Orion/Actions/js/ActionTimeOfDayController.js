﻿
SW.Core.namespace("SW.Core.Actions").TimeOfDayScheduleController = function (config) {

    var showHidefrequencyControl = function () {
        if (config.container.find('[data-form="NoSchedule"] input').is(":checked")) {

            config.container.find("#timePeriod").hide();
        } else {
            config.container.find("#timePeriod").show();
        }
    };

    var display = function (visible) {
        if (visible) {
            config.container.find('[data-form="NoSchedule"] input').prop("checked", false);
            config.container.find('[data-form="EnableTriggerResetSchedule"] input').prop("checked", true);
            config.container.find("#timePeriod").show();
        } else {
            config.container.find('[data-form="EnableTriggerResetSchedule"] input').prop("checked", false);
            config.container.find('[data-form="NoSchedule"] input').prop("checked", true);
            config.container.find("#timePeriod").hide();
        }
    };

    this.isTimeOfDayScheduleModified = function() {
        return config.mutiEditMode && config.container.find('[data-edited="timeOfDayEnabled"] input').is(':checked');
    };

    this.init = function () {
        if (config.count > 0) {
            display(true);
        } else {
            display(false);
        }
        showHidefrequencyControl();
        config.container.find(".paragraph input[type='radio']").click(showHidefrequencyControl);

        SW.Orion.FrequencyControllerDefinition = new SW.Orion.FrequencyController({
            container: config.frequencyControlContainer,
            useTimePeriodMode: config.useTimePeriodMode,
            singleScheduleMode: config.singleScheduleMode,
            frequencies: config.frequencies,
            disableTimePeriodStr: '@{R=Core.Strings.2;K=WEBJS_AY0_8; E=js}',
            enableTimePeriodStr: '@{R=Core.Strings.2;K=WEBJS_AY0_9; E=js}',
            mutiEditMode: config.mutiEditMode
        });
        SW.Orion.FrequencyControllerDefinition.init();

        var annotation = SW.Core.Actions.TimeOfDaySchedule.getSectionAnnotation(config.frequencies);
        $("#actionTimeOfDayContainer h3 .contentInfo").text(annotation);
        if (config.mutiEditMode) {
            config.container.find(".paragraph input[type='radio']").attr("disabled", true);
            config.container.find('[data-edited="timeOfDayEnabled"] input').click(function () {
                var isChecked = $(this).is(':checked');
                if (!isChecked) {
                    config.container.find('[data-form="NoSchedule"] input').attr('checked', true);
                    showHidefrequencyControl();
                }
                config.container.find(".paragraph input[type='radio']").attr("disabled", !isChecked);
            });
        } else {
            config.container.find('[data-edited="timeOfDayEnabled"] input').hide();
        }
    };

    this.getSectionAnnotation = function (frequencies) {
        var annotation = "";
        if ($($('[data-form="EnableTriggerResetSchedule"] input')[1]).is(":checked")) {
            var currentFrequencies = SW.Orion.FrequencyControllerDefinition.getFrequency();
            if (currentFrequencies.length == 0) {
                currentFrequencies = frequencies;
            }
            var enabled = "";
            var disabled = "";
            for (var i = 0; i < currentFrequencies.length; i++) {
                if (currentFrequencies[i]["EnabledDuringTimePeriod"]) {
                    enabled = enabled ? String.format("@{R=Core.Strings;K=WEBJS_TM0_146;E=js}", enabled, currentFrequencies[i]["DisplayName"]) : currentFrequencies[i]["DisplayName"];
                } else {
                    disabled = disabled ? String.format("@{R=Core.Strings;K=WEBJS_TM0_146;E=js}", disabled, currentFrequencies[i]["DisplayName"]) : currentFrequencies[i]["DisplayName"];
                }
            }
            if (enabled) {
                annotation += String.format("@{R=Core.Strings.2;K=WEBJS_OS0_3;E=js}", enabled);
            }
            if (disabled) {
                annotation += String.format("@{R=Core.Strings.2;K=WEBJS_OS0_6;E=js}", disabled);
            }
        } else {
            annotation = "@{R=Core.Strings.2;K=WEBJS_OS0_5;E=js}";
        }

        return annotation;
    };
};
