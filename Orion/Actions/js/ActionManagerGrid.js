﻿Ext.namespace('SW');
Ext.namespace('SW.Orion');
Ext.QuickTips.init();
SW.Orion.ActionManager = function () {
    var selectorModel;
    var dataStore;
    var grid;
    var userPageSize;
    var selectedColumns;
    var sortOrder;
    var comboArray;
    var comboboxMaxWidth = 500;
    var groupingValue;
    var groupGrid;
    var grpGridFirstLoad;
    var pageSizeBox;
    var actionIdsToSelect;
    var customGrouping;
    var isSelectAllMode = false;
    var tempSelectedItemsArray;
    var userHasRightToEnableOrDisableAction;

    var updateToolbarButtons = function () {
        var selCount = getSelectedItemsArray().length;
        var map = grid.getTopToolbar().items.map;
        var needsToDisable = (selCount === 0);
        var needsToEditDisable = needsEditToDisable();

        map.Edit.setDisabled(needsToEditDisable);
        map.EnableDisable.setDisabled(!userHasRightToEnableOrDisableAction || needsToDisable);
        map.Delete.setDisabled(needsToDisable);
        map.AssignToAlert.setDisabled(needsToDisable);
        map.Test.setDisabled(selCount != 1);
        var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');

        if (grid.getStore().getCount() > 0 && grid.getSelectionModel().getCount() == grid.getStore().getCount()) {
            hd.addClass('x-grid3-hd-checker-on');
        } else {
            hd.removeClass('x-grid3-hd-checker-on');
        }
    };

    function needsEditToDisable() {
        var needsDisable = true;
        var selectionModel = grid.getSelectionModel();
        if (selectionModel.getCount() == 1) {
            needsDisable = false;
        }
        else if (selectionModel.getCount() > 1) {
            for (var i = 1; i < selectionModel.getCount(); i++) {
                if (selectionModel.selections.items[0].data.ActionTypeID == selectionModel.selections.items[i].data.ActionTypeID) {
                    needsDisable = false;
                }
                else {
                    return true;
                }
            }
        }
        return needsDisable;
    }

    function renderActionType(value, meta, record) {
        var actionPattern = '<a href="#"><span ext:qtitle="" ext:qtip="{0}">{1}</span></a>';
        var tipText = String.format("<b>{0}</b><br/><span style='word-wrap:break-word'>{1}</span>", encodeHTML(encodeHTML(record.data.Title)), record.data.Description); // encodeHTML twice as extjs tooltip requires.
        return String.format(actionPattern, tipText, renderString(value));
    }

    function renderEnabled(value) {
        if (userHasRightToEnableOrDisableAction) {
            if (value == true) {
                return ('<a href="#" class="toggleOn">@{R=Core.Strings;K=WEBJS_TM0_144;E=js}</a>');
            } else {
                return ('<a href="#" class="toggleOff">@{R=Core.Strings;K=LogLevel_off;E=js}</a>');
            }
        } else {
            if (value == true) {
                return ('<label class="toggleOn">@{R=Core.Strings;K=WEBJS_TM0_144;E=js}</label>');
            } else {
                return ('<label class="toggleOff">@{R=Core.Strings;K=LogLevel_off;E=js}</label>');
            }
        }

    };

    function renderCategoryType(value) {
        if (value == 'Reset') {
            return encodeHTML('@{R=Core.Strings;K=WEBJS_SO0_95;E=js}');
        };

        if (value == 'Trigger') {
            return encodeHTML('@{R=Core.Strings;K=WEBJS_SO0_94;E=js}');
        };

        return encodeHTML(value);
    };

    function stringToBoolean(value) {
        switch (value.toString().toLowerCase()) {
            case "true": case "yes": case "1": return true;
            case "false": case "no": case "0": case null: return false;
            default: return Boolean(value);
        }
    };

    function searchPattern(search) {
        var tmpfilterText = search.replace(/\*/g, "%");

        if (tmpfilterText.length > 0) {
            if (!tmpfilterText.match("^%"))
                tmpfilterText = "%" + tmpfilterText;

            if (!tmpfilterText.match("%$"))
                tmpfilterText = tmpfilterText + "%";
        }
        return tmpfilterText;
    }

    var refreshGridObjects = function (start, limit, elem, property, type, value, search, callback) {
        elem.store.removeAll();
        elem.store.proxy.conn.jsonData = { property: property, type: type, value: value || "", search: searchPattern(search) };
        
        if (limit)
            elem.store.load({ params: { start: start, limit: limit }, callback: callback });
        else
            elem.store.load({ callback: callback });
    };

    function selectAllTooltip(grid, pageSizeBox, webStore) {
        clearSelectAllTooltip();
        if (grid.getStore().getCount() == grid.getSelectionModel().getCount() && grid.getStore().getCount() != 0 && grid.getStore().getCount() != pageSizeBox.ownerCt.dsLoaded[0].totalLength) {
            var text = String.format("@{R=Core.Strings;K=WEBJS_AK0_105;E=js}", grid.getSelectionModel().getCount());
            $('<div id="selectionTip" style="padding-left: 5px; background-color:#FFFFCC;">' + text + ' </div>').insertAfter(".x-panel-tbar.x-panel-tbar-noheader");
            $("#selectionTip").append($("<span id='selectAllLink' style='text-decoration: underline;color:red;cursor:pointer;'>" + String.format("@{R=Core.Strings;K=WEBJS_AK0_104;E=js}", pageSizeBox.ownerCt.dsLoaded[0].totalLength) + "</span>").click(function () {
                var allGridElementsStore = webStore();
                allGridElementsStore.proxy.conn.jsonData = grid.store.proxy.conn.jsonData;
                allGridElementsStore.on('beforeload', function (store, options) {
                    options.params.limit = pageSizeBox.ownerCt.dsLoaded[0].totalLength;
                });
                allGridElementsStore.load({
                    callback: function (result) {
                        isSelectAllMode = true;
                        tempSelectedItemsArray = allGridElementsStore.data.items;
                        $("#selectionTip").text("@{R=Core.Strings;K=WEBJS_AK0_106;E=js}").css("background-color", "white");
                        $("#selectAllLink").remove();
                        updateToolbarButtons();

                        if (allGridElementsStore.getCount() > 0) {
                            grid.getTopToolbar().items.map.Edit.setDisabled(false);
                            var atype = allGridElementsStore.data.items[0].data.ActionTypeID;
                            for (var i = 1; i < allGridElementsStore.getCount(); i++) {
                                if (atype != allGridElementsStore.data.items[i].data.ActionTypeID) {
                                    grid.getTopToolbar().items.map.Edit.setDisabled(true);
                                    break;
                                }
                            }
                        }
                    }
                });
            }));
            var gridHeight = $("#selectAllLink").closest('.x-panel-body.x-panel-body-noheader').height();
            var tipHeight = $("#selectionTip").outerHeight();
            $("#selectAllLink").closest('.x-panel-body.x-panel-body-noheader').height(gridHeight + tipHeight);
            $('.x-layout-split.x-layout-split-west.x-splitbar-h').height(gridHeight + tipHeight);
        } else {
            clearSelectAllTooltip();
        }
    }

    function getSelectedItemsArray() {
        return isSelectAllMode ? tempSelectedItemsArray : grid.getSelectionModel().getSelections();
    }

    function clearSelectAllTooltip() {
        if ($("#selectionTip").length != 0) {
            var tipHeight = $("#selectionTip").outerHeight();
            var rowsContainerHeight = $("#selectAllLink").closest('.x-panel-body.x-panel-body-noheader').height();
            $('.x-layout-split.x-layout-split-west.x-splitbar-h').height(rowsContainerHeight - tipHeight);
            $("#selectAllLink").closest('.x-panel-body.x-panel-body-noheader').height(rowsContainerHeight - tipHeight);
            $("#selectionTip").remove();
            if (isSelectAllMode) {
                isSelectAllMode = false;
            }
        }
        updateToolbarButtons();
    }

    var getSelectedActionIds = function (items) {
        var ids = [];

        Ext.each(items, function (item) {
            ids.push(item.data.ActionID);
        });

        return ids;
    };

    var getSelectedAssugnmentIds = function (items) {
        var ids = [];
        Ext.each(items, function (item) {
            if (!$.isNumeric(item.data.ActionAssignmentID)) {
                ids = $.merge(ids, item.data.ActionAssignmentID.split(","));
            }
            else {
                ids.push(item.data.ActionAssignmentID);
            }
        });

        return ids;
    };

    var enableDisableSelectedItems = function (items, enable, onSuccess) {
        var toUpdate = getSelectedActionIds(items);

        ORION.callWebService("/Orion/Services/ActionManager.asmx",
            "EnableDisableActions", { ids: toUpdate, enable: enable },
            function (result) {
                onSuccess(result);
            });
    };

    var afterdelete = function() {
        if (comboArray.getValue() == "@{R=Core.Strings;K=WEBJS_VB0_76; E=js}" || comboArray.getValue() == "") {
            refreshGridObjects(0, userPageSize, grid, "", "", "", filterText, function() {});
        } else {
            refreshGridObjects(null, null, groupGrid, groupingValue[0], groupingValue[0], "", filterText, function() {
                groupGrid.getSelectionModel().selectRow(groupGrid.getStore().find("Value", groupingValue[1]), false);
            });
            refreshGridObjects(0, userPageSize, grid, groupingValue[0], "", groupingValue[1], filterText, function() {});
        }
    };

    var deleteSelectedItems = function (items, onSuccess) {
        var toDelete = getSelectedAssugnmentIds(items);
        var waitMsg = Ext.Msg.wait("@{R=Core.Strings;K=WEBJS_AY0_15; E=js}");

        DeleteActionsbyAssignmentID(toDelete, function (result) {
            waitMsg.hide();
            //  ErrorHandler(result);
            if (typeof onSuccess === "function")
                onSuccess(result);
        });
    };

    function deleteActions(ids, onSuccess) {
        ORION.callWebService("/Orion/Services/ActionManager.asmx",
            "DeleteActions", { ids: ids },
            function (result) {
                onSuccess(result);
            });
    }

    function DeleteActionsbyAssignmentID(ids, onSuccess) {
        ORION.callWebService("/Orion/Services/ActionManager.asmx",
            "DeleteActionsbyAssignmentID", { ids: ids },
            function (result) {
                onSuccess(result);
            });
    }

    var testAction = function () {
        if ($("#isOrionDemoMode").length != 0) {
            demoAction("Core_ActionManager_TestAction", this);
            return;
        }

        var alertId = grid.getSelectionModel().getSelections()[0].data.AlertID;
        if (!$.isNumeric(alertId)) {
            alertId = alertId.split(",")[0];
        }
        ORION.callWebService("/Orion/Services/ActionManager.asmx",
            "GetAlertingActionContext", { alertDefinitionId: alertId },
            function (result) {

                var selectedId = grid.getSelectionModel().getSelections()[0].data.ActionID;
                ORION.callWebService("/Orion/Services/ActionManager.asmx",
                    "GetActionDefinition", { id: selectedId },
                    function (actionResult) {

                        SW.Core.Actions.TestActionController.TestAction({
                            action: JSON.parse(actionResult),
                            enviromentType: 'Alerting',
                            viewContext: JSON.parse(result),
                            showNetObjectPicker: true
                        });
                    });
            });
    };

    function setMainGridHeight(pageSize) {
        window.setTimeout(function () {
            var mainGrid = Ext.getCmp('mainGrid');
            var maxHeight = calculateMaxGridHeight(mainGrid);
            var calculated = calculateGridHeight(pageSize);
            var height = (calculated > 0) ? Math.min(calculated, maxHeight) : maxHeight;
            setHeightForGrids(height);

            //we need this to add height if horizontal scroll bar is present, with low window width and in chrome
            if (grid.el.child(".x-grid3-scroller").dom.scrollWidth != grid.el.child(".x-grid3-scroller").dom.clientWidth) {
                height = Math.min(height + 20, maxHeight);
                setHeightForGrids(height);
            }
        }, 0);
    }

    function setHeightForGrids(height) {
        var mainGrid = Ext.getCmp('mainGrid');
        var groupingGrid = Ext.getCmp('groupingGrid');

        mainGrid.setHeight(Math.max(height, 300));
        groupingGrid.setHeight(Math.max(height - 50, 250)); //height without groupByTopPanel

        mainGrid.doLayout();
    }

    function calculateGridHeight(numberOfRows) {
        if (grid.store.getCount() == 0)
            return 0;
        var rowsHeight = Ext.fly(grid.getView().getRow(0)).getHeight() * (numberOfRows + 1);
        return grid.getHeight() - grid.getInnerHeight() + rowsHeight + 7;
    }

    function calculateMaxGridHeight(gridPanel) {
        var gridTop = gridPanel.getPosition()[1];
        return $(window).height() - gridTop - $('#footer').height() - 25;
    }

    function renderStatus(status) {
        if (status == '[All]') return '@{R=Core.Strings;K=LIBCODE_TM0_15;E=js}';
        return stringToBoolean(status) ? '@{R=Core.Strings;K=WEBJS_TM0_144;E=js}' : '@{R=Core.Strings;K=LogLevel_off;E=js}';
    }

    function renderGroup(value, meta, record) {
        var disp;

        if (comboArray.getValue().indexOf('Enabled') > -1) {
            disp = String.format('@{R=Core.Strings;K=WEBJS_TM0_145; E=js}', renderStatus(record.data.Value), record.data.Cnt);
        } else {
            if (value == '{empty}')
                value = '';

            if (!value && value != false || value == '') {
                value = "@{R=Core.Strings;K=StatusDesc_Unknown;E=js}";
            }
            if (value == "[All]") {
                value = "@{R=Core.Strings;K=WEBJS_SO0_31; E=js}";
            }
            disp = value + " (" + record.data.Cnt + ")";
        }
        return encodeHTML(disp);
    }

    function renderAssignedAlert(value, meta, record) {
        var alertData = record.data.AssignedAlertsData;
        var alertPattern = '<a href="#" ><span ext:qtitle="" ext:qtip="{0}">{1}</span></a>';
        var tipText = String.format("<b>{0}</b>{2}<span style='word-wrap:break-word'>{1}</span>", encodeHTML(encodeHTML(alertData[0].AlertName)), alertData[0].AlertDescription, alertData[0].AlertDescription.length > 0 ? "<br/>" : '');
        for (var i = 1; i < alertData.length; i++) {
            tipText = String.format('{0}{1}', tipText, (String.format('<br/><b>{0}</b>{1}', encodeHTML(encodeHTML(alertData[i].AlertName)), alertData[i].AlertDescription.length > 0 ? "<br/>" + alertData[i].AlertDescription : '')));
        }
        return String.format(alertPattern, tipText, renderString(value));
    }

    function encodeHTML(value) {
        return Ext.util.Format.htmlEncode(value);
    };

    function renderSchedule(value) {
        var schedulePattern = '<a href="#"><span ext:qtitle="" ext:qtip="{1}">{0}</span></a>';
        if (Ext.isEmpty(value))
            return String.format(schedulePattern, renderString("@{R=Core.Strings;K=WEBJS_YK0_20; E=js}"), "@{R=Core.Strings;K=WEBJS_YK0_20; E=js}");
        return String.format(schedulePattern, renderString(value.join(',')), value.join('<br/>'));
    }

    function escapeRegExp(str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }

    function renderString(value) {
        if (!value || value.length === 0)
            return value;

        if (!filterText || filterText.length === 0)
            return encodeHTML(value);

        var patternText = filterText;

        // replace any %'s with a *
        patternText = patternText.replace(/\%/g, "*");

        // replace multiple *'s with a single instance
        patternText = patternText.replace(/\*{2,}/g, "*");

        // check if the search string is now just down to a single *, and if so return without any further regex
        if (patternText == '*')
        { return '<span style=\"background-color: #FFE89E\">' + encodeHTML(value) + '</span>'; }

        // replace \ with \\
        patternText = patternText.replace(/\\/g, '\\\\');
        // replace . with \.
        patternText = patternText.replace(/\./g, '\\.');
        // replace * with .*
        patternText = patternText.replace(/\*/g, '.*');

        // set regex pattern
        var x = '((' + escapeRegExp(patternText) + ')+)\*';
        var content = value, pattern = new RegExp(x, "gi"), replaceWith = '{SPAN-START-MARKER}$1{SPAN-END-MARKER}';

        // do regex replace + content gets encoded
        var fieldValue = encodeHTML(content.replace(pattern, replaceWith));

        // now replace the literal SPAN markers with the HTML span tags
        fieldValue = fieldValue.replace(/{SPAN-START-MARKER}/g, '<span style=\"background-color: #FFE89E\">');
        fieldValue = fieldValue.replace(/{SPAN-END-MARKER}/g, '</span>');

        return fieldValue;
    }

    var checkForMultipleAssignments = function (titlemessage, message, icon, callback) {
        var ids = getSelectedActionIds(getSelectedItemsArray());
        ORION.callWebService("/Orion/Services/ActionManager.asmx",
            "CheckForMultipleAssignments", { ids: ids },
            function (result) {
                if (result) {
                    Ext.Msg.show({
                        title: titlemessage,
                        msg: message,
                        buttons: { yes: '@{R=Core.Strings;K=CommonButtonType_Ok; E=js}', cancel: '@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}' },
                        icon: icon,
                        fn: function (button) {
                            if (button == "yes") {
                                callback(true);
                            }
                        }
                    });
                } else {
                    callback(false);
                }
            });
    };

    function getActionRowNumber(actionIds, prop, type, value, succeeded) {
        if (!actionIds) {
            succeeded({ ind: -1 });
            return;
        }

        var sort = "";
        var dir = "";

        if (sortOrder != null) {
            sort = sortOrder[0];
            dir = sortOrder[1];
        }

        SW.Core.Services.callWebServiceSync('/Orion/Services/ActionManager.asmx', 'GetActionNumber', { actionIds: actionIds, property: prop, type: type, value: value, sort: sort, direction: dir, search: searchPattern(filterText) },
            function (result) {
                if (succeeded != null) {
                    succeeded({ ind: result });
                    return;
                }
                // not found
                succeeded({ ind: -1 });
                return;
            });
    }

    function getAssignedAlertsbyActionId(actionId) {
        var assignedAlerts = [];
        SW.Core.Services.callWebServiceSync('/Orion/Services/ActionManager.asmx', 'GetAssignedAlertsToAction', { actionId: actionId },
          function (result) {
              assignedAlerts = JSON.parse(result);
          });
        return assignedAlerts;
    }

    //Assign Actions to selected Alerts
    function assignToAlert() {
    if ($("#isOrionDemoMode").length != 0) {
            demoAction("Core_AlertManager_AssignTrigger", this);
            return;
        }

        var selected = getSelectedItemsArray();
        var assignedAlerts = [];
        var slectedActionId = 0;

        if (selected.length == 1)
            slectedActionId = selected[0].data.ActionID;
        assignedAlerts = getAssignedAlertsbyActionId(slectedActionId);

        SW.Orion.AlertPicker.selectAlertDialog();
        SW.Orion.AlertPicker.clearSelection();
        SW.Orion.AlertPicker.setSelectedAlertsIds(assignedAlerts);
        $("#shceduleReportBox").parent().css('margin-top', '7%');
    }

    return {
        getAssignAlertsHeaderTitle: function () {
            var selected = getSelectedItemsArray();
            var actionsNamesArray = [];
            $.each(selected, function () {
                actionsNamesArray.push(Ext.util.Format.htmlEncode(this.data.Title));
            });
            $("#headerTitle").html(String.format("@{R=Core.Strings.2;K=WEBJS_AY0_12;E=js}".replace(/&lt;/gi, "<").replace(/&gt;/gi, ">"), actionsNamesArray.join(", ")));
        },
        alertsSelected: function (selectedAlerts) {
            var alertIds = [];
            $.each(selectedAlerts, function () {
                alertIds.push(this.AlertID);
            });

            var selectedItems = getSelectedItemsArray();
            var actionsData = [];
            $.each(selectedItems, function () {
                actionsData.push({ ActionID: this.data.ActionID, CategoryType: this.data.CategoryType });
            });
            var oldAlertIds = [];
            if (selectedItems.length == 1) {
                var alertData = getAssignedAlertsbyActionId(selectedItems[0].data.ActionID);
                $.each(alertData, function () {
                    oldAlertIds.push(this.AlertID);
                });
            };
            ORION.callWebService('/Orion/Services/ActionManager.asmx',
                    'UpdateActionsAssignments', { actionsDataJson: JSON.stringify(actionsData), oldAlertIds: oldAlertIds, newAlertIds: alertIds },
                    function (result) {
                        if (result) {
                            filterText = "";
                            if (actionsData.length > 1) {
                                refreshGridObjects(0, userPageSize, grid, groupingValue[0], "", groupingValue[1], filterText, function () { });
                            } else {
                                var objectIndex = grid.store.indexOf(grid.getSelectionModel().getSelections()[0]);
                                refreshGridObjects(0, userPageSize, grid, groupingValue[0], "", groupingValue[1], filterText, function () {
                                    if (objectIndex > -1) {
                                        grid.getView().focusRow(objectIndex % userPageSize);
                                        var rowEl = grid.getView().getRow(objectIndex % userPageSize);
                                        rowEl.scrollIntoView(grid.getGridEl().id, false);
                                        grid.getSelectionModel().selectRow(objectIndex % userPageSize);
                                    }
                                });
                            }
                            var grVal = ((customGrouping && customGrouping != "") ? "AssignedAlert" : groupingValue[0]);
                            comboArray.setValue(grVal);
                            refreshGridObjects(null, null, groupGrid, grVal, grVal, "", "");
                        }
                    });
        },
        init: function () {
            ORION.prefix = 'ActionManager_';
            actionIdsToSelect = $("#SelectedActionIDs").val();
            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", updateToolbarButtons);

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            var getActionsEndpoint = "/Orion/Services/ActionManager.asmx/GetActions";
            var getActionsJson = [
                        { name: 'ActionID', mapping: 0 },
                        { name: 'ActionTypeID', mapping: 1 },
                        { name: 'Title', mapping: 2 },
                        { name: 'Enabled', mapping: 3 },
                        { name: 'EnvironmentType', mapping: 4 },
                        { name: 'AssignedAlert', mapping: 5 },
                        { name: 'AlertID', mapping: 6 },
                        { name: 'Description', mapping: 7 },
                        { name: 'CategoryType', mapping: 8 },
                        { name: 'TimeSchedule', mapping: 9 },
                        { name: 'ActionAssignmentID', mapping: 10 },
                        { name: 'AssignedAlertsData', mapping: 11 }
            ];

            selectorModel.on("rowselect", function () {
                selectAllTooltip(grid, pageSizeBox, function () {
                    return new ORION.WebServiceStore(getActionsEndpoint, getActionsJson);
                });
            });

            selectorModel.on("rowdeselect", function () {
                clearSelectAllTooltip();
            });

            sortOrder = ORION.Prefs.load('SortOrder', 'Title,ASC').split(',');
            userPageSize = parseInt(ORION.Prefs.load('PageSize', '20'));
            groupingValue = ORION.Prefs.load('GroupingValue', ',null').split(',');
            selectedColumns = ORION.Prefs.load('SelectedColumns', '').split(',');
            grpGridFirstLoad = true;
            customGrouping = decodeURIComponent($("#SelectedGrouping").val());
            userHasRightToEnableOrDisableAction = document.getElementById('ActionManager_UserHasRightToEnableOrDisableAction').value === 'True';

            dataStore = new ORION.WebServiceStore(getActionsEndpoint, getActionsJson);

            dataStore.sortInfo = { field: sortOrder[0], direction: sortOrder[1] };

            var groupingDataStore = new ORION.WebServiceStore(
                "/Orion/Services/ActionManager.asmx/GetObjectGroupProperties",
                [
                    { name: 'Value', mapping: 0 },
                    { name: 'Name', mapping: 1 },
                    { name: 'Type', mapping: 2 }
                ]);

            var groupingStore = new ORION.WebServiceStore(
                "/Orion/Services/ActionManager.asmx/GetGroupValues",
                [
                    { name: 'Value', mapping: 0 },
                    { name: 'DisplayNamePlural', mapping: 1 },
                    { name: 'Cnt', mapping: 2 }
                ],
                "Value", "");

            comboArray = new Ext.form.ComboBox(
                {
                    id: 'mrGroupCombo',
                    mode: 'local',
                    boxMaxWidth: comboboxMaxWidth,
                    fieldLabel: 'Name',
                    displayField: 'Name',
                    valueField: 'Value',
                    store: groupingDataStore,
                    triggerAction: 'all',
                    value: '@{R=Core.Strings;K=WEBJS_VB0_76; E=js}',
                    typeAhead: true,
                    forceSelection: true,
                    selectOnFocus: false,
                    multiSelect: false,
                    editable: false,
                    listeners: {
                        'select': function () {
                            var val = this.store.data.items[this.selectedIndex].data.Value;
                            var type = this.store.data.items[this.selectedIndex].data.Type;

                            refreshGridObjects(null, null, groupGrid, val, type, "", filterText);

                            if (val == '') {
                                ORION.Prefs.save('GroupingValue', ',');
                                refreshGridObjects(0, userPageSize, grid, "", "", "", filterText, function () {
                                    $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function () {
                                        this.tooltip = 'processed';
                                        $.swtooltip(this);
                                    });
                                });
                            }
                        },
                        'beforerender': function () {
                            refreshGridObjects(null, null, comboArray, "", "", "", "");
                        }
                    }
                });

            comboArray.getStore().on('load', function () {
                var grVal = ((customGrouping && customGrouping != "") ? "AssignedAlert" : groupingValue[0]);
                comboArray.setValue(grVal);

                refreshGridObjects(null, null, groupGrid, grVal, grVal, "", "");
            });

            pageSizeBox = new Ext.form.NumberField({
                id: 'PageSizeField',
                enableKeyEvents: true,
                allowNegative: false,
                width: 40,
                allowBlank: false,
                minValue: 1,
                maxValue: 100,
                value: userPageSize,
                listeners: {
                    scope: this,
                    'keydown': function (f, e) {
                        var k = e.getKey();
                        if (k == e.RETURN) {
                            e.stopEvent();
                            var v = parseInt(f.getValue());
                            if (!isNaN(v) && v > 0 && v <= 100) {
                                userPageSize = v;
                                pagingToolbar.pageSize = userPageSize;
                                ORION.Prefs.save('PageSize', userPageSize);
                                pagingToolbar.doLoad(0);
                                setMainGridHeight(userPageSize);
                            }
                            else {
                                Ext.Msg.show({
                                    title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                    msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                                });
                                return;
                            }
                        }
                    },
                    'focus': function (field) {
                        field.el.dom.select();
                    },
                    'change': function (f, numbox, o) {
                        var pSize = parseInt(f.getValue());
                        if (isNaN(pSize) || pSize < 1 || pSize > 100) {
                            Ext.Msg.show({
                                title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                            });
                            return;
                        }
                        if (pagingToolbar.pageSize != pSize) { // update page size only if it is different
                            pagingToolbar.pageSize = pSize;
                            userPageSize = pSize;

                            ORION.Prefs.save('PageSize', userPageSize);
                            pagingToolbar.doLoad(0);
                            setMainGridHeight(userPageSize);
                        }
                    }
                }
            });

            var pagingToolbar = new Ext.PagingToolbar(
                { id: 'rmGridPaging',
                    store: dataStore,
                    pageSize: userPageSize,
                    displayInfo: true,
                    displayMsg: "@{R=Core.Strings;K=WEBJS_TM0_5; E=js}",
                    emptyMsg: "@{R=Core.Strings;K=WEBJS_VL0_11; E=js}",
                    beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                    afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                    firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                    prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                    nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                    lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                    refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}",
                    items: [
                        '-',
                        new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_VB0_46; E=js}', style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;' }),
                        pageSizeBox
                    ]
                }
            );

            var getGridColumHeader = function (name) {
                return SW.Core.String.Format('<span title="{0}" style="vertical-align: middle;">{0}</span>', name);
            };

            grid = new Ext.grid.GridPanel({
                region: 'center',
                viewConfig: { forceFit: false, emptyText: "@{R=Core.Strings;K=WEBJS_VL0_11; E=js}" },
                store: dataStore,
                baseParams: { start: 0, limit: userPageSize },
                split: true,
                stripeRows: true,
                trackMouseOver: false,
                columns: [selectorModel,
                        { header: 'ActionID', width: 80, hidden: true, hideable: false, sortable: false, dataIndex: 'ActionID' },
                        { header: getGridColumHeader('@{R=Core.Strings;K=WEBJS_AY0_12; E=js}'), width: 350, hideable: false, sortable: true, dataIndex: 'Title', renderer: renderString },
                        { header: getGridColumHeader('@{R=Core.Strings;K=WEBJS_VL0_16; E=js}'), width: 120, sortable: true, dataIndex: 'Enabled', renderer: renderEnabled },
                        { header: getGridColumHeader('@{R=Core.Strings;K=WEBJS_SO0_100; E=js}'), width: 350, sortable: true, dataIndex: 'ActionTypeID', renderer: renderActionType },
                        { header: getGridColumHeader('@{R=Core.Strings;K=FILEDATA_IB0_7; E=js}'), width: 120, sortable: true, dataIndex: 'CategoryType', renderer: renderCategoryType },
                        { header: getGridColumHeader('@{R=Core.Strings.2;K=WEBJS_YK0_21; E=js}'), width: 350, sortable: true, dataIndex: 'AssignedAlert', renderer: renderAssignedAlert },
                        { header: getGridColumHeader('@{R=Core.Strings;K=WEBJS_YK0_19; E=js}'), width: 180, sortable: false, dataIndex: 'TimeSchedule', renderer: renderSchedule },
                        { header: getGridColumHeader('@{R=Core.Strings;K=ResourcesConfig_AppStack_Title; E=js}'), width: 150, sortable: true, dataIndex: 'EnvironmentType', renderer: renderString },
                        { header: 'ActionAssignmentID', width: 80, hidden: true, hideable: false, sortable: false, dataIndex: 'ActionAssignmentID' }
                    ],
                listeners: {
                    cellclick: function (grid, rowIndex, columnIndex, e) {
                        var fieldName = grid.getColumnModel().getDataIndex(columnIndex); // Get field name
                        if (userHasRightToEnableOrDisableAction && fieldName == 'Enabled') {
                            if ($("#isOrionDemoMode").length != 0) {
                                demoAction("Core_ActionManager_EnableDisableAction", this);
                                return;
                            }
                            var record = grid.getStore().getAt(rowIndex); // Get the Record
                            var data = record.get(fieldName);
                            var id = record.get('ActionID');

                            ORION.callWebService("/Orion/Services/ActionManager.asmx",
                                "EnableDisableAction", { actionId: id, value: !data },
                                function (result) {
                                    if (result) {
                                        record.set(fieldName, !stringToBoolean(data));
                                        record.commit();
                                    } else {
                                        Ext.Msg.show({
                                            title: 'Error', //todo - clarify message with Andrew
                                            msg: 'The action can not be turned on/off',
                                            icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                                        });
                                    }
                                });
                        } else if (fieldName == 'AssignedAlert') {
                            var selected = grid.getSelectionModel().getSelected();
                            SW.Core.Services.postToTarget("/Orion/Alerts/Default.aspx", { AlertID: selected.data.AlertID, Grouping: escape(selected.data.ActionID) });
                        }
                    }
                },
                sm: selectorModel, autoScroll: 'true', loadMask: true, width: 750, height: 350,
                tbar: [
                        { id: 'Edit', text: '@{R=Core.Strings;K=WEBJS_AY0_14; E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_AY0_14; E=js}', icon: '/Orion/Nodes/images/icons/icon_edit.gif', cls: 'x-btn-text-icon',
                            handler: function () {
                                checkForMultipleAssignments('@{R=Core.Strings;K=WEBJS_AY0_14;E=js}', '@{R=Core.Strings;K=WebJS_SO0_120;E=js}', Ext.Msg.WARNING, function (result) {
                                    var actionIds = new Array();
                                    var showExecutionSettings = true;
                                    $.each(getSelectedItemsArray(), function () {
                                        actionIds.push(this.get("ActionID"));
                                        if (this.get("CategoryType") === "Reset") {
                                            showExecutionSettings = false;
                                        }
                                    });

                                    var editController = new SW.Core.Actions.MultiEditController({ dialogContainerId: "dialogContainer", enableExecutionSettings: showExecutionSettings, anyshared: result });
                                    editController.editAction(actionIds, function () {
                                        getActionRowNumber(actionIds.join(), groupingValue[0], "", groupingValue[1], function (res) {
                                            var startFrom = 0;
                                            var objectIndex = res.ind[0];
                                            filterValueParam = '';

                                            if (objectIndex > -1) {
                                                var pageNumFl = objectIndex / userPageSize;
                                                var index = parseInt(pageNumFl.toString());
                                                startFrom = index * userPageSize;
                                            }

                                            refreshGridObjects(startFrom, userPageSize, grid, groupingValue[0], "", groupingValue[1], filterText, function () {
                                                grid.getView().focusRow(objectIndex % userPageSize);
                                                var rowEl = grid.getView().getRow(objectIndex % userPageSize);
                                                if (rowEl)
                                                    rowEl.scrollIntoView(grid.getGridEl().id, false);

                                                if (res.ind.length > 1) {
                                                    var rowArray = [];
                                                    for (var rowId = 0; rowId < res.ind.length; rowId = rowId + 1) {
                                                        rowArray.push(res.ind[rowId] % userPageSize);
                                                    }
                                                    grid.getSelectionModel().selectRows(rowArray);
                                                } else
                                                    grid.getSelectionModel().selectRow(objectIndex % userPageSize);
                                            });
                                        });
                                    });
                                });
                            }
                        }, '-',
                        { id: 'Test', text: '@{R=Core.Strings;K=CommonButtonType_Test; E=js}', tooltip: '@{R=Core.Strings;K=CommonButtonType_Test; E=js}', icon: '/Orion/images/Reports/duplicateEdit_report.png', cls: 'x-btn-text-icon', handler: testAction }, '-',
                        { id: 'EnableDisable', text: '@{R=Core.Strings;K=WEBJS_VL0_18;E=js}', tooltip: '@{R=Core.Strings;K=WEBJS_VL0_18;E=js}', icon: '/Orion/images/enable_disable.png', cls: 'x-btn-text-icon',
                            menu: {
                                xtype: 'menu',
                                items: [{
                                    id: 'EnableActions',
                                    text: '@{R=Core.Strings;K=WEBJS_YK0_22;E=js}',
                                    tooltip: '@{R=Core.Strings;K=WEBJS_YK0_14;E=js}',
                                    icon: '/Orion/images/Check.Green.gif',
                                    cls: 'x-btn-text-icon',
                                    handler: function () {
                                        if ($("#isOrionDemoMode").length != 0) {
                                            demoAction("Core_ActionManager_EnableDisableAction", this);
                                            return;
                                        }

                                        checkForMultipleAssignments('@{R=Core.Strings.2;K=WEBJS_SO0_3;E=js}', '@{R=Core.Strings.2;K=WEBJS_SO0_2;E=js}', Ext.Msg.QUESTION, function (result) {
                                                enableDisableSelectedItems(getSelectedItemsArray(), true, function() {
                                                    refreshGridObjects(0, userPageSize, grid, groupingValue[0], "", groupingValue[1], filterText, function() {});
                                                });
                                                this.parentMenu.hide();
                                        });
                                    }
                                },
                                    {
                                        id: 'DisableActions',
                                        text: '@{R=Core.Strings;K=WEBJS_YK0_23;E=js}',
                                        tooltip: '@{R=Core.Strings;K=WEBJS_YK0_15;E=js}',
                                        icon: '/Orion/images/failed_16x16.gif',
                                        cls: 'x-btn-text-icon',
                                        handler: function () {
                                            if ($("#isOrionDemoMode").length != 0) {
                                                demoAction("Core_ActionManager_EnableDisableAction", this);
                                                return;
                                            }

                                            checkForMultipleAssignments('@{R=Core.Strings.2;K=WEBJS_SO0_3;E=js}', '@{R=Core.Strings.2;K=WEBJS_SO0_2;E=js}', Ext.Msg.QUESTION, function(result) {
                                                enableDisableSelectedItems(getSelectedItemsArray(), false, function() {
                                                    refreshGridObjects(0, userPageSize, grid, groupingValue[0], "", groupingValue[1], filterText, function() {});
                                                });
                                                this.parentMenu.hide();
                                            });
                                        }
                                    }]
                            }
                        }, '-',
                        {
                            id: 'AssignToAlert', text: '@{R=Core.Strings.2;K=WEBJS_AY0_17; E=js}', tooltip: '@{R=Core.Strings.2;K=WEBJS_AY0_17; E=js}', icon: '/Orion/images/Reports/icon_assign_to.gif', cls: 'x-btn-text-icon', handler: assignToAlert
                        }, '-',
                        { id: 'Delete', text: '@{R=Core.Strings;K=CommonButtonType_Delete; E=js}', tooltip: '@{R=Core.Strings;K=CommonButtonType_Delete; E=js}', icon: '/Orion/Nodes/images/icons/icon_delete.gif', cls: 'x-btn-text-icon',
                            handler: function() {
                                if ($("#isOrionDemoMode").length != 0) {
                                    demoAction("Core_ActionManager_DeleteAction", this);
                                    return;
                                }

                                checkForMultipleAssignments('@{R=Core.Strings;K=WEBJS_AY0_11;E=js}', '@{R=Core.Strings.2;K=WEBJS_SO0_1;E=js}', Ext.Msg.QUESTION, function (result) {
                                    if (!result) {
                                        Ext.Msg.confirm("@{R=Core.Strings;K=WEBJS_AY0_11; E=js}", "@{R=Core.Strings;K=WEBJS_AY0_10; E=js}",
                                            function(btn) {
                                                if (btn == 'yes') {
                                                    deleteSelectedItems(getSelectedItemsArray(), afterdelete);
                                                }
                                            });
                                    }
                                    else
                                        deleteSelectedItems(getSelectedItemsArray(), afterdelete);
                                });
                            }
                        }, ' ', '->',
                    new Ext.ux.form.SearchField({
                        store: dataStore,
                        width: 200,
                        beforeSearchHook: function () {
                            // rewrite global variable before serializing input data to json format
                            filterValueParam = '';
                        }
                    })
                    ],
                bbar: pagingToolbar
            });

            // making columns visible or hidden according to saved configuration
            for (var i = 1; i < grid.getColumnModel().getColumnCount(); i++) {
                if (selectedColumns.indexOf(grid.getColumnModel().getDataIndex(i)) > -1) {
                    grid.getColumnModel().setHidden(i, false);
                } else {
                    grid.getColumnModel().setHidden(i, true);
                }
            }

            grid.store.on('beforeload', function (store, options) {
                var pn = store.paramNames;
                var sort = options.params[pn.sort];
                var dir = options.params[pn.dir];

                if (sort && dir) {
                    sortOrder[0] = sort;
                    sortOrder[1] = dir;
                    ORION.Prefs.save('SortOrder', sortOrder.join(','));
                }
            });

            grid.store.on('load', function () {
                grid.getEl().unmask();
                setMainGridHeight(userPageSize);
                $("#selectionTip").remove();
                if (isSelectAllMode) {
                    isSelectAllMode = false;
                    updateToolbarButtons();
                }
            });

            grid.getColumnModel().on('hiddenchange', function () {
                var cols = '';
                for (var i = 1; i < grid.getColumnModel().getColumnCount(); i++) {
                    if (!grid.getColumnModel().isHidden(i)) {
                        cols += grid.getColumnModel().getDataIndex(i) + ',';
                    }
                }
                ORION.Prefs.save('SelectedColumns', cols.slice(0, -1));
            });

            groupGrid = new Ext.grid.GridPanel({
                id: 'groupingGrid',
                store: groupingStore,
                cls: 'hide-header',
                columns: [
                        { id: 'Value', width: 193, editable: false, sortable: false, dataIndex: 'DisplayNamePlural', renderer: renderGroup }
                    ],
                selModel: new Ext.grid.RowSelectionModel(),
                autoScroll: true,
                heigth: 300,
                loadMask: true,
                listeners: {
                    cellclick: function (mygrid, row, cell, e) {
                        var val = mygrid.store.data.items[row].data[mygrid.store.data.items[row].fields.keys[cell]];
                        var type = "";
                        $.each(comboArray.store.data.items, function (index, item) {
                            if (item.data.Value == comboArray.getValue()) {
                                type = item.data.Type;
                                return false;
                            }
                            return false;
                        });
                        if (val == '') {
                            val = '{empty}';
                        }

                        if (val == null) {
                            val = "null";
                        }

                        if (val == '[All]')
                            val = '';
                        var newGroupingValues = comboArray.getValue() + ',' + val;
                        ORION.Prefs.save('GroupingValue', newGroupingValues);
                        groupingValue = newGroupingValues.split(',');

                        refreshGridObjects(0, userPageSize, grid, comboArray.getValue(), type, val, filterText, function () {
                            $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function () {
                                this.tooltip = 'processed';
                                $.swtooltip(this);
                            });
                        });
                    }
                },
                anchor: '0 0', viewConfig: { forceFit: true }, split: true, autoExpandColumn: 'Value'
            });

            groupGrid.store.on('load', function () {
                if (grpGridFirstLoad) {
                    if (customGrouping && customGrouping != "") {
                        groupGrid.getSelectionModel().selectRow(groupGrid.getStore().find("Value", customGrouping), false);
                    }
                    else {
                        var selectedVal = groupGrid.getStore().find("Value", groupingValue[1]);
                        if (selectedVal == -1) {
                            groupingValue[1] = "";
                            refreshGridObjects(0, userPageSize, grid, groupingValue[0], "", groupingValue[1], filterText);
                            selectedVal = 0;
                        }
                        groupGrid.getSelectionModel().selectRow(selectedVal, false);
                    }
                    grpGridFirstLoad = false;
                }
            });

            var groupByTopPanel = new Ext.Panel({
                id: 'groupByTopPanel',
                region: 'center',
                split: false,
                heigth: 50,
                width: 200,
                collapsible: false,
                viewConfig: { forceFit: true },
                items: [new Ext.form.Label({ text: "@{R=Core.Strings;K=WEBJS_AK0_76; E=js}" }), comboArray],
                cls: 'panel-no-border panel-bg-gradient'
            });

            var navPanel = new Ext.Panel({
                id: 'navPanel',
                region: 'west',
                width: 200,
                heigth: 350,
                split: true,
                anchor: '0 0',
                collapsible: false,
                viewConfig: { forceFit: true },
                items: [groupByTopPanel, groupGrid],
                cls: 'panel-no-border'
            });

            navPanel.on("bodyresize", function () {
                groupGrid.setWidth(navPanel.getSize().width);
            });

            var mainGridPanel = new Ext.Panel({ id: 'mainGrid', region: 'center', split: true, layout: 'border', collapsible: false, items: [navPanel, grid], cls: 'no-border' });
            mainGridPanel.render("ActionManagerGrid");
            if (actionIdsToSelect && actionIdsToSelect != "") {
                getActionRowNumber(actionIdsToSelect, "AssignedAlert", "", customGrouping, function (res) {
                    var startFrom = 0;
                    var objectIndex = res.ind[0];

                    if (objectIndex > -1) {
                        var pageNumFl = objectIndex / userPageSize;
                        var index = parseInt(pageNumFl.toString());
                        startFrom = index * userPageSize;
                    }

                    var newGroupingValues = "AssignedAlert" + ',' + customGrouping;
                    ORION.Prefs.save('GroupingValue', newGroupingValues);
                    groupingValue = newGroupingValues.split(',');

                    refreshGridObjects(startFrom, userPageSize, grid, "AssignedAlert", "", customGrouping, filterText, function () {
                        if (actionIdsToSelect && actionIdsToSelect != "") {
                            grid.getView().focusRow(objectIndex % userPageSize);
                            var rowEl = grid.getView().getRow(objectIndex % userPageSize);
                            rowEl.scrollIntoView(grid.getGridEl().id, false);

                            if (res.ind.length > 1) {
                                var rowArray = [];
                                for (var rowId = 0; rowId < res.ind.length; rowId = rowId + 1) {
                                    rowArray.push(res.ind[rowId] % userPageSize);
                                }
                                grid.getSelectionModel().selectRows(rowArray);
                            } else
                                grid.getSelectionModel().selectRow(objectIndex % userPageSize);
                        }
                    });
                });
            }
            else {
                refreshGridObjects(0, userPageSize, grid, groupingValue[0], "", groupingValue[1], filterText);
            }
            updateToolbarButtons();
            $(window).resize(function () {
                setMainGridHeight(userPageSize);
                mainGridPanel.doLayout();
            });
        }
    };

} ();
Ext.onReady(SW.Orion.ActionManager.init, SW.Orion.ActionManager);
