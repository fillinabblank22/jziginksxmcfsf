﻿SW.Core.namespace("SW.Core.Actions").CustomStatusController = function (config) {
    "use strict";
    
    function toUsePolledStatus() {
        var usePollStatusSelected = $container.find(".UsePolledStatusCl :checked").val();
        if (!usePollStatusSelected)
            return "false";
        else
            return "true";
    }

    function isEditable(key) {
        return !config.multiEditMode || $container.find('[data-edited="' + key + '"] input:checked').length == 1;
    }

    var createConfiguration = function () {
        var targetUri = "";
        if (selectedNetObject != null && selectedNetObject.uri != "")
            targetUri = selectedNetObject.uri;
        var configuration = {
            UsePolledStatus: toUsePolledStatus(),
            SelectedNodeStatus: $container.find('[data-form="' + config.dataFormMapping.SelectedNodeStatus + '"]').val(),
            TargetNodeUri: targetUri
        };
        return configuration;
    };
    
    var callActionAndExecuteCallback = function (action, param, callback) {
        SW.Core.Services.callController("/api/CustomStatus/" + action, param,
        // On success
            function (response) {
                if ($.isFunction(callback))
                    callback({ isError: false, actionDefinition: response });
            },
        // On error
            function (msg) {
                if ($.isFunction(callback)) {
                    callback({ isError: true, ErrorMessage: msg });
                }
            }
        );
    };

    this.getSectionAnnotation = function (sectionID) {
        var configuration = createConfiguration();
        var annotation = "";
        if (sectionID == 'CustomStatus') {
            if (configuration.UsePolledStatus == 'true') {
                annotation = '@{R=Core.Strings.2;K=LIBCODE_PR_04; E=js}';
            } else {
                annotation = String.format('@{R=Core.Strings.2;K=LIBCODE_PR_06; E=js}', configuration.SelectedNodeStatus);
            }
        }
        return annotation;
    };
    var updateActionAndExecuteCallback = function (action, configuration, callback) {
        var updateConfig = {
            Action: action,
            Configuration: configuration
        };
        callActionAndExecuteCallback("Update", updateConfig, callback);
    };

    var createActionAndExecuteCallback = function (configuration, callback) {
        callActionAndExecuteCallback("Create", configuration, callback);
    };

    // Validate action configuration. 
    this.validateSectionAsync = function (sectionID, callback) {
        var customSectionValid = false;
        if (isEditable(config.dataFormMapping.UsePolledStatus)) {
            $container.find('[data-form="' + config.dataFormMapping.UsePolledStatus + '"]').removeClass("invalidValue");
            $container.find('[data-form="' + config.dataFormMapping.SelectedNodeStatus + '"]').removeClass("invalidValue");
            if ($("select[id='CbAffectedNode'] option:selected").index() == "1" && selectedNetObject == null) {
                $container.find('[data-form="' + config.dataFormMapping.TargetNodeUri + '"]').addClass("invalidValue");
            } else {
                $container.find('[data-form="' + config.dataFormMapping.TargetNodeUri + '"]').removeClass("invalidValue");
                customSectionValid = true;
            }
        }
        return callback(customSectionValid);
    };

    this.getUpdatedActionProperies = function (callback) {
        var jsonProperties = new Array();
        if (isEditable(config.dataFormMapping.UsePolledStatus)) {
            jsonProperties.push({ PropertyName: config.dataFormMapping.UsePolledStatus, PropertyValue: toUsePolledStatus() });
            jsonProperties.push({ PropertyName: config.dataFormMapping.SelectedNodeStatus, PropertyValue: $container.find('[data-form="' + config.dataFormMapping.SelectedNodeStatus + '"]').val() });
            jsonProperties.push({ PropertyName: config.dataFormMapping.TargetNodeUri, PropertyValue: selectedNetObject!=null ? selectedNetObject.uri : ''});
        }
        if (callback) {
            callback(JSON.stringify(jsonProperties));
        }
    };

    this.getActionDefinitionAsync = function (callback) {
        var configuration = createConfiguration();
        // Edit mode.
        if (config.actionDefinition) {
            // We have action definition we just want to updated existing one.
            updateActionAndExecuteCallback(config.actionDefinition, configuration, callback);
        } else {
            // New action definition mode.
            createActionAndExecuteCallback(configuration, callback);
        }
    };

    function openSelectorDialog() {
        var selectedUri = [];
        if (selectedNetObject != null)
            selectedUri = [selectedNetObject.uri];
        SW.Orion.NetObjectPicker.SetUpEntities([{ MasterEntity: 'Orion.Nodes' }], 'Orion.Nodes', selectedUri, 'Single', true);
        $('#netObjectPickerPlaceHolder').prependTo('#insertVariableNetObjectPickerDlgCS');
        $('#insertVariableNetObjectPickerDlgCS').dialog({
            position: ['middle', 100],
            width: 700,
            height: 'auto',
            modal: true,
            draggable: true,
            title: '@{R=Core.Strings.2;K=WEBJS_PR0_2; E=js}',
            show: { duration: 0 }
        });
    }

    function loadSelectedNetObject() {
        var selectNodeChooserTitle = '@{R=Core.Strings.2;K=WEBJS_PR0_1; E=js}';

        var targetNodeUri = $('input[id$=hfTargetNodeUri]').val();;
        if (targetNodeUri == "") {
            $('#SelectNodeChooser').text(selectNodeChooserTitle);
        }
        else {
            ORION.callWebService("/Orion/Services/AddRemoveObjects.asmx", "LoadEntitiesByUris", { entityType: 'Orion.Nodes', uris: [targetNodeUri] }, function (result) {
                if (result.length > 0) {
                    selectedNetObject = { uri: result[0].Uri, fullName: result[0].FullName };
                    $('#SelectNodeChooser').text(selectedNetObject.fullName);
                } else {
                    $('#SelectNodeChooser').text(selectNodeChooserTitle);
                }
            });
        }
    }

    function closeNetObjectPicker() {
        $('#insertVariableNetObjectPickerDlgCS').dialog('close');
    };

    function changeNetObject() {
        var selectedEntities = SW.Orion.NetObjectPicker.GetSelectedEntitiesDefinitions();
        if (selectedEntities.length > 0) {
            $container.find('[data-form="' + config.dataFormMapping.TargetNodeUri + '"]').removeClass("invalidValue");
            selectedNetObject = selectedEntities[0];
            $('#SelectNodeChooser').text(selectedNetObject.fullName);
        }
    }

    this.init = function () {
        $container = $('#' + config.containerID);
        
        $container.find(".UseCustomStatusCL input").on('change', function () {
            var nodeStatusDiv = $container.find('.NodeStatusCl');
            nodeStatusDiv.find(':input').prop('disabled', false);
        });

        $container.find(".UsePolledStatusCl input").on('change', function () {
            var nodeStatusDiv = $container.find('.NodeStatusCl');
            nodeStatusDiv.find(':input').prop('disabled', true);
        });
        
        $('#CbAffectedNode').on('change', function (e) {
            var selectedNodeDiv = $container.find('.SelectedNodeCL');
            if ($("select[id='CbAffectedNode'] option:selected").index() == "0") //Node that Triggered this Alert
            {
                $container.find('[data-form="' + config.dataFormMapping.TargetNodeUri + '"]').removeClass("invalidValue");
                selectedNetObject = null;
                $('#SelectNodeChooser').text('@{R=Core.Strings.2;K=WEBJS_PR0_1; E=js}');
                selectedNodeDiv.hide();
            } else {
                selectedNodeDiv.show();
            }
        });

        $('#SelectNodeChooser').on("click", function (e) {
            e.preventDefault();
            openSelectorDialog();

            $('#btnNetObjectSelectCS').on('click', function (e) {
                e.preventDefault();
                changeNetObject();
                closeNetObjectPicker();
            });

            $('#btnNetObjectCancelCS').on('click', function (e) {
                e.preventDefault();
                closeNetObjectPicker();
            });
        });

        loadSelectedNetObject();

        if (toUsePolledStatus() == "true") {
            var nodeStatusDiv = $container.find('.NodeStatusCl');
            nodeStatusDiv.find(':input').prop('disabled', true);
        } else {
            var nodeStatusDiv = $container.find('.NodeStatusCl');
            nodeStatusDiv.find(':input').prop('disabled', false);
        }

        if (config.multiEditMode) {
            $container.find('[data-edited="' + config.dataFormMapping.UsePolledStatus + '"]').click(function () {
                var dataEdited = $(this).attr("data-edited");                
                $container.find('.NodeStatusCl').find(':input').prop('disabled', !$(this).find("input").is(':checked'));
                $container.find('.UseCustomStatusCL').find(':input').prop('disabled', !$(this).find("input").is(':checked'));
                $container.find('.UsePolledStatusCl').find(':input').prop('disabled', !$(this).find("input").is(':checked'));
                $container.find('.SelectAffectedNodeCL').find(':input').prop('disabled', !$(this).find("input").is(':checked'));
            });
            $container.find('.NodeStatusCl').find(':input').prop('disabled', true);
            $container.find('.UseCustomStatusCL').find(':input').prop('disabled', true);
            $container.find('.UsePolledStatusCl').find(':input').prop('disabled', true);
            $container.find('.SelectAffectedNodeCL').find(':input').prop('disabled', true);
        }
       
        if ($.isFunction(config.onReadyCallback)) {
            config.onReadyCallback(self);
        }
    };
    
    var self = this;
    var $container = null;
    var selectedNetObject = null;
};
