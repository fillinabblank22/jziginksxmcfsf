﻿SW.Core.namespace("SW.Core.Actions").ExecuteVBScriptController = function (config) {
    "use strict";

    var getElementByKey = function (key) {
        return $container.find('[data-form="' + key + '"]');
    };

    var createConfiguration = function () {
        var configuration = {
            FilePath: getElementByKey(config.dataFormMapping.filePath).val(),
            Interpreter: getElementByKey(config.dataFormMapping.interpreter).val(),
            Credentials: SW.Core.WindowsCredentialSelectorControl.getCredentials()
        };

        return configuration;
    };

    function isEditable(key) {
        return !config.multiEditMode || $container.find('[data-edited="' + key + '"] input:checked').length == 1;
    }

    function validateConfiguration(configuration, callback) {
        var valid = true;
        getElementByKey(config.dataFormMapping.filePath).removeClass("invalidValue");
        getElementByKey(config.dataFormMapping.interpreter).removeClass("invalidValue");

        valid = validatePathInput(configuration.FilePath, getElementByKey(config.dataFormMapping.filePath)) && valid;
        valid = SW.Core.WindowsCredentialSelectorControl.ValidateCredentials() && valid;
        valid = SW.Core.WindowsCredentialSelectorControl.ValidateCredentialName() && valid;

        if ($.isFunction(callback))
            callback(valid);
    }

    function validatePathInput(value, elem) {
        var validationResult = true;
        if (value.length == 0) {
            validationResult = false;
            elem.addClass("invalidValue");
        }
        return validationResult;
    }

    var createActionAndExecuteCallback = function (configuration, callback) {
        callActionAndExecuteCallback("Create", configuration, callback);
    };

    var updateActionAndExecuteCallback = function (action, configuration, callback) {
        configuration.Action = action;
        callActionAndExecuteCallback("Update", configuration, callback);
    };

    var callActionAndExecuteCallback = function (action, param, callback) {

        SW.Core.Services.callController("/api/ExecuteVBScript/" + action, param,
            // On success
            function (response) {
                if ($.isFunction(callback))
                    callback({ isError: false, actionDefinition: response });
            },
            // On error
            function (msg) {
                if ($.isFunction(callback)) {
                    callback({ isError: true, ErrorMessage: msg });
                }
            }
        );
    };

    // Validate action configuration. 
    this.validateSectionAsync = function (sectionID, callback) {
        if (sectionID == "executeVBScriptSection"
            && isEditable(config.dataFormMapping.filePath)) {
            var configuration = createConfiguration();
            validateConfiguration(configuration, callback);
        }
        else {
            callback(true);
        }
    };

    this.getActionDefinitionAsync = function (callback) {
        var configuration = createConfiguration();
        // Edit mode.
        if (config.actionDefinition) {
            // We have action definition we just want to updated existing one.
            updateActionAndExecuteCallback(config.actionDefinition, configuration, callback);

        } else {
            // New action definition mode.
            createActionAndExecuteCallback(configuration, callback);
        }
    };

    var changeDisabledState = function (state, dataEditKey) {
        SW.Core.WindowsCredentialSelectorControl.radioDisabledChange(state);
        $container.find('[data-form="' + dataEditKey + '"]').attr("disabled", state);
    };

    var changeCredentialsState = function () {
        if (config.multiEditMode) {
            SW.Core.WindowsCredentialSelectorControl.radioDisabledChange(!isEditable(config.dataFormMapping.filePath));
        } else {
            SW.Core.WindowsCredentialSelectorControl.radioDisabledChange(false);
        }
    }

    this.init = function () {
        SW.Core.WindowsCredentialSelectorControl.setConfig({
            TestCredentialsRoute: "/api/ExecuteVBScript/TestCredentials",
            ValidateCredentialNameRoute: "/api/ExecuteVBScript/ValidateCredentialName",
            GetConfiguration: createConfiguration,
            ValidateConfiguration: validateConfiguration
        });
        if (SW.Core.MacroVariablePickerController)
            SW.Core.MacroVariablePickerController.BindPicker();
        $container = $('#' + config.containerID);

        if (config.multiEditMode) {
            for (var key in config.dataFormMapping) {
                var dataform = config.dataFormMapping[key];
                $container.find('[data-edited="' + dataform + '"]').click(function () {
                    var dataEdited = $(this).attr("data-edited");
                    if (dataEdited == config.dataFormMapping.filePath) {
                        changeDisabledState(!$(this).find("input").is(':checked'), dataEdited);
                    }
                    getElementByKey(dataEdited).attr("disabled", !$(this).find("input").is(':checked'));
                });
                if (dataform == config.dataFormMapping.filePath) {
                    changeDisabledState(true, dataform);
                }
                getElementByKey(dataform).attr("disabled", true);
            }
        }

        changeCredentialsState();

        if ($.isFunction(config.onReadyCallback)) {
            config.onReadyCallback(self);
        }
    };

    this.getUpdatedActionProperies = function (callback) {
        var jsonProperties = new Array();

        if (isEditable(config.dataFormMapping.interpreter)) {
            jsonProperties.push({ PropertyName: config.dataFormMapping.interpreter, PropertyValue: getElementByKey(config.dataFormMapping.interpreter).val() });
        }

        if (isEditable(config.dataFormMapping.filePath)) {
            jsonProperties.push({ PropertyName: config.dataFormMapping.filePath, PropertyValue: getElementByKey(config.dataFormMapping.filePath).val() });
            if (SW.Core.WindowsCredentialSelectorControl.isCredentialsDefined()) {
                jsonProperties.push({ PropertyName: "Credentials", PropertyValue: SW.Core.WindowsCredentialSelectorControl.addUpdateWindowsCredential() });
            }
        }

        if (callback) {
            callback(JSON.stringify(jsonProperties));
        }
    };

    var self = this;
    var $container = null;
};
