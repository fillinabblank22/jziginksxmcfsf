﻿SW.Core.namespace("SW.Core.Actions").PrintersSelector = function (config) {

    "use strict";

    // Private methods:
    var rowClicked = function (selectionModel, rowIndex, record) {
        selectedPrinterName = record.data["FullName"];
    };

    var printerSelectedClicked = function () {
        if (selectedPrinterName) {
            var selectedPrinter = _.find(config.printers, function (item) {
                return item.FullName == selectedPrinterName;
            });

            if (selectedPrinter && $.isFunction(config.onSet)) {
                config.onSet(selectedPrinter);
                self.Hide();
            }
        }
    };

    var preselectPrinter = function () {
        if (printersGrid && printersGrid.store && printersGrid.store.data && printersGrid.store.data.items  && config.preselectedPrinter) {

            var items = printersGrid.store.data.items;
            for (var i = 0; i < items.length; i++) {

                if (items[i].data["FullName"] == config.preselectedPrinter.FullName) {

                    printersGrid.getSelectionModel().selectRow(i);

                    break;
                }
            }
        }
    };

    var onClose = function () {
        self.Hide();

        if ($.isFunction(config.onClose)) {
            config.onClose();
        }
    };

    var createInfoMessage = function (containerID) {
        $('#' + containerID).html('<div>' + '@{R=Core.Strings;K=WEBJS_TM0_159;E=js}' + '</div>');
    };

    var createButtons = function (containerID) {

        var $containerID = '#' + containerID;

        $($containerID).css('text-align', 'right');

        $(SW.Core.Widgets.Button('@{R=Core.Strings;K=WEBJS_TM0_160;E=js}', { type: 'primary', id: 'selectPrinterBtn' }))
            .appendTo($containerID)
            .css('margin', '10px')
            .on('click', printerSelectedClicked);

        $(SW.Core.Widgets.Button('@{R=Core.Strings;K=WEBJS_ZT0_22;E=js}', { type: 'secondary', id: 'cancelPrinterBtn' }))
            .appendTo($containerID)
            .css('margin', '10px 0 10px 10px')
            .on('click', onClose);
    };

    var createPrintersGrid = function (containerID) {

        printersStore = new Ext.data.JsonStore({
            fields: ['Name', 'FullName', 'Description', 'Location'],
            autoLoad: true,
            data: _.sortBy(config.printers, "Name")
        });

        printersSelectionModel.on("rowselect", rowClicked);

        printersGrid = new Ext.grid.GridPanel({
            id: 'printersSelectorGrid',
            viewConfig: { forceFit: true, scrollOffset: 0, autoFill: true },
            store: printersStore,
            stripeRows: true,
            columns: [
                printersSelectionModel,
                { id: 'Name', header: '@{R=Core.Strings;K=WEBJS_TD0_1;E=js}', width: 100, dataIndex: 'Name', sortable: true },
                { id: 'FullName', header: '@{R=Core.Strings;K=WEBJS_TM0_161;E=js}', width: 250, dataIndex: 'FullName', sortable: true },
                { id: 'Description', header: '@{R=Core.Strings;K=WEBJS_ZT0_24;E=js}', width: 300, dataIndex: 'Description', sortable: true },
                { id: 'Location', header: '@{R=Core.Strings;K=WEBJS_AK0_23;E=js}', width: 200, dataIndex: 'Location', sortable: true }
            ],
            sm: printersSelectionModel,

            autoExpandColumn: 'Location',
            enableHdMenu: false,
            enableColumnResize: true,
            enableColumnMove: false,
            width: 800,
            autoHeight: true,
            renderTo: containerID,
            listeners: {
                'render': function () {
                    setTimeout(preselectPrinter, 0);
                }
            },
            cls: 'actionsSelectorGrid'

        });

    };

    // Public methods:

    this.ShowInDialog = function () {

        var $container = $('#' + config.containerID);

        $container.empty().append("<div class='actionPluginSelector' id='_printersSelectorDialog_' />");

        $dialogElement = $('#_printersSelectorDialog_', $container);

        $dialogElement.html($("<div class='info' id='_printersSelectorInfo_' /> <div id='_printersSelectorGrid_' /> <div id='_printersSelectorButtons_' />"));

        createInfoMessage('_printersSelectorInfo_');
        createPrintersGrid('_printersSelectorGrid_');
        createButtons('_printersSelectorButtons_');

        $dialog = $dialogElement.dialog({
            modal: true,
            title: 'Printers',
            width: 820,
            heigh: 'auto',
            minWidth: 820,
            close: onClose
        });

    };

    this.Hide = function () {
        $dialog.dialog('destroy');
        $('#_printersSelectorDialog_').remove();
    };

    // Constructor:
    var self = this;
    var $dialogElement = null;
    var $dialog = null;
    var selectedPrinterName = null;
    var printersSelectionModel = new Ext.sw.grid.RadioSelectionModel();
    var printersStore = null;
    var printersGrid = null;
};