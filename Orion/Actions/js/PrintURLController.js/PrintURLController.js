﻿SW.Core.namespace("SW.Core.Actions").PrintURLController = function (config) {
    "use strict";

    var isValidCredentials = false;
    // Private methods

    var validate = function (configuration, callback) {
        if (validateConfiguration(configuration)) {
            //SW.Core.Services.callController("/api/PrintURL/ValidateConfg", configuration, function (result) {
            //});
            callback(true);
        } else {
            if ($.isFunction(callback))
                callback(false);
        }
    };

    var createConfiguration = function () {
        var configuration = {
            ID: $container.find('[data-form="actionDefinitionID"]').val(),
            PrinterName: $container.find('[data-form="printerName"]').val(),
            Pages: $container.find('[data-form="pagesAll"] input[type="radio"]').is(':checked') ? '' : $container.find('[data-form="pagesInput"]').val(),
            Copies: parseInt($container.find('[data-form="copies"]').val()),
            Layout: $container.find('[data-form="layoutPortrait"] input[type="radio"]').is(':checked') ? 0 : 1,
            Color: $container.find('[data-form="color"] input[type="radio"]').is(':checked') ? 0 : 1,
            Margins: parseInt($container.find('[data-form="margins"]').val()),
            Printable: true,
            Credentials: {
				Name: $container.find('[data-form="username"]').val(),
                Username: $container.find('[data-form="username"]').val(),
                Password: $container.find('[data-form="password"]').val()
            }
        };

        return configuration;
    };

    function validateConfiguration(configuration) {
        $('#copiesValidation').empty();
        var valid = true;
        if (configuration.PrinterName == null) {
            valid = false;
            $('.no-printers-info').text('@{R=Core.Strings;K=WEBJS_TM0_157;E=js}');
        }
        var copies = $container.find('[data-form="copies"]').val();
        if (!$.isNumeric(copies) || parseInt(copies) <= 0 || parseInt(copies) > 999 || copies.match(/^\d{1,3}$/) == null) {
            valid = false;
            $('#copiesValidation').text('@{R=Core.Strings;K=WEBJS_TM0_158;E=js}');
        }

        return valid;
    }

    var createActionAndExecuteCallback = function (configuration, callback) {
        callActionAndExecuteCallback("Create", configuration, callback);
    };

    var updateActionAndExecuteCallback = function (action, configuration, callback) {

        var updateConfig = {
            Action: action,
            Configuration: configuration
        };

        callActionAndExecuteCallback("Update", updateConfig, callback);
    };

    var callActionAndExecuteCallback = function (action, param, callback) {
        SW.Core.Services.callController("/api/PrintURL/" + action, param,
        // On success
            function (response) {
                if ($.isFunction(callback))
                    callback({ isError: false, actionDefinition: response });
            },
        // On error
            function (msg) {
                if ($.isFunction(callback)) {
                    callback({ isError: true, ErrorMessage: msg });
                }
            }
        );

    };

    var hideDialogContainer = function () {
        $("#" + config.dialogContainerID).empty().attr('style', '').addClass('actionsOffPage');
    };

    var showDialogContainer = function () {
        $("#" + config.dialogContainerID).removeClass('actionsOffPage');
    };

    var printerSelected = function (selectedPrinter) {
        if (selectedPrinter) {

            $("#" + config.dialogContainerID).addClass('loading');
            config.selectedPrinter = selectedPrinter;
            renderPrinters(selectedPrinter);
            hideDialogContainer();
        }
    };

    var selectPrinter = function () {
        if (isValidCredentials) {
            showDialogContainer();

            printersSelectorController = new SW.Core.Actions.PrintersSelector({
                containerID: config.dialogContainerID,
                preselectedPrinter: config.selectedPrinter,
                printers: config.printers,
                onSet: printerSelected,
                onClose: hideDialogContainer
            });

            printersSelectorController.ShowInDialog();
        } else {
            Ext.Msg.show({ title: '@{R=Core.Strings;K=LIBCODE_TM0_56;E=js}', msg: '@{R=Core.Strings;K=WEBJS_VL0_21;E=js}', modal: true, closable: false, minWidth: 500,
                buttons: { ok: '@{R=Core.Strings;K=CommonButtonType_Ok;E=js}' },
                icon: Ext.MessageBox.WARNING
            });
        }
    };

    var renderPrinters = function (selectedPrinter) {
        var template;

        if (selectedPrinter) {
            template = $('#' + config.printerDefinitionsTemplateID).html();
        } else {
            template = $('#' + config.noPrintersTemplateID).html();
        }

        var html = $(_.template(template, { printer: selectedPrinter }));

        html.delegate('[data-form="selectNew"]', "click", selectPrinter);


        var $container = $('#' + config.containerID);


        $container.find('[data-form="printersList"]').html(html);
    };

    var isUserName = function (userName) {
        var regex = /^[^\/:*?"<>|]+$/;
        return regex.test(userName);
    };

    var validateUserName = function (value, elem) {
        var validationResult = true;
        $.each(value.split(';'), function () {
            if (!isUserName($.trim(this))) {
                validationResult = false;
                elem.addClass("invalidValue");
            } else {
                elem.removeClass("invalidValue");
            }
        });

        return validationResult;
    };

    var isPassword = function (password) {
        var validationResult = true;
        if (password.length == 0)
            validationResult = false;
        return validationResult;
    };

    var validateUserPassword = function (value, elem) {
        var validationResult = true;
        $.each(value.split(';'), function () {
            if (!isPassword($.trim(this))) {
                validationResult = false;
                elem.addClass("invalidValue");
            }
        });
        return validationResult;
    };

    var clearPrinterData = function () {
        config.selectedPrinter = null;
        config.printers = null;
        renderPrinters(false);
    };

    var hasCurentUserSelectedPrinter = function (printers) {
        var hasSelectedPrinter = false;
        for (var i = 0; i < printers.length; i++) {

            if (printers[i].FullName == config.selectedPrinter.FullName) {

                hasSelectedPrinter = true;
                break;
            }
        }
        return hasSelectedPrinter;
    };

    var validateCredentials = function (configuration, callback) {
        $container.find('[data-form="username"]"]').removeClass("invalidValue");
        $container.find('[data-form="password"]').removeClass("invalidValue");

        var valid = true;
        valid = validateUserName(configuration.Credentials.Username, $container.find('[data-form="username"]')) && valid;
        valid = validateUserPassword(configuration.Credentials.Password, $container.find('[data-form="password"]')) && valid;
        if (valid == false) {
            clearPrinterData();
            isValidCredentials = false;
            callback(false);
            return;
        } else {
            SW.Core.Services.callController("/api/PrintURL/TestCredentials", configuration, function (result) {
                if (result == false) {
                    $container.find('[data-form="username"]').addClass("invalidValue");
                    $container.find('[data-form="password"]').addClass("invalidValue");
                    isValidCredentials = false;
                    clearPrinterData();
                    callback(false);
                } else {
                    SW.Core.Services.callController('/api/PrintURL/GetPrintersForUser', configuration,
                    // On success
                        function (response) {
                            if (config.selectedPrinter&&!hasCurentUserSelectedPrinter(response)) {
                                clearPrinterData();
                            }
                            config.printers = response;
                            isValidCredentials = true;
                            callback(true);
                        },
                    // On error
                        function (msg) {
                            alert(msg);
                            clearPrinterData();
                            isValidCredentials = false;
                            callback(false);
                        }
                    );
                }
            });
        }
    };

    // Public methods

    this.init = function () {
        $container = $('#' + config.containerID);
        renderPrinters(config.selectedPrinter);
        if ($.isFunction(config.onReadyCallback)) {
            config.onReadyCallback(self);
        }
    };

    // Validate action configuration. 
    this.validateSectionAsync = function (sectionID, callback) {
        var configuration = createConfiguration();
        if (sectionID == 'printAuthorization') {
            validateCredentials(configuration, callback);

        }

        if (sectionID == 'printSettings') {
            validate(configuration, callback);
        }
    };
    this.getSectionAnnotation = function (sectionID) {
        var configuration = createConfiguration();
        var annotation = "";
        if (sectionID == 'printAuthorization') {
            annotation = configuration.Credentials.Username;
        } else if (sectionID == 'printAuthorization') {
            annotation = configuration.PrinterName;
        }
        return annotation;
    };


    this.getActionDefinitionAsync = function (callback) {
        var configuration = createConfiguration();
        // Edit mode.
        if (config.actionDefinition) {

            // We have action definition we just want to updated existing one.
            updateActionAndExecuteCallback(config.actionDefinition, configuration, callback);

        } else {

            // New action definition mode.
            createActionAndExecuteCallback(configuration, callback);
        }
    };
    this.clearPrintersSelection = function () {
        renderPrinters();
        config.selectedPrinter = null;
    };


    // Constructor

    var self = this;
    var $container = null;
    var printersSelectorController = null;
}
