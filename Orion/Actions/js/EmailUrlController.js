﻿SW.Core.namespace("SW.Core.Actions").EmailUrlController = function (config) {
    "use strict";
    // Private methods

    var validateConfg = function (configuration) {
        var isValid = true;
        SW.Core.Services.callControllerSync("/api/Email/ValidateConfg", configuration, function (result) {
            isValid = checkValidationResults(result);
        });

        return isValid;
    };

    var checkValidationResults = function (result) {
        var isValid = true;
        for (var key in result) {
            $container.find('[data-form="' + key + '"]').removeClass("invalidValue");
            if (!result[key]) {
                $container.find('[data-form="' + key + '"]').addClass("invalidValue");
                isValid = false;
            }
        }
        return isValid;
    };

    var createConfiguration = function () {
        var contentTypesArray = new Array();
        var isPrintable;
        var messageContentType = "";
        $.each($container.find('[data-form="' + config.dataFormMapping.stringContentType + '"]:checked'), function () {
            contentTypesArray.push($(this).attr("enum-value"));
        });
        isPrintable = $container.find('[data-form="' + config.dataFormMapping.printable + '"]').is(':checked');

        $.each($container.find('[data-from="' + config.dataFormMapping.messageContentType + '"]:checked'), function () {
            messageContentType = $(this).attr("enum-value");
        });

        var configuration = {
            Sender: $container.find('[data-form="' + config.dataFormMapping.nameOfSender + '"]').val(),
            EmailTo: $container.find('[data-form="' + config.dataFormMapping.emailTo + '"]').val(),
            Subject: $container.find('[data-form="' + config.dataFormMapping.emailSubject + '"]').val(),
            EmailMessage: $container.find('[data-form="' + config.dataFormMapping.emailMessage + '"]').val(),
            Printable: isPrintable,
            EmailFrom: $container.find('[data-form="' + config.dataFormMapping.emailFrom + '"]').val(),
            EmailCC: $container.find('[data-form="' + config.dataFormMapping.emailCC + '"]').val(),
            EmailBCC: $container.find('[data-form="' + config.dataFormMapping.emailBCC + '"]').val(),
            SMTPServer: SW.Core.SMTPServerSettingsController.getSmtpServerAsync(),
            StringContentType: contentTypesArray,
            EnvironmentType: config.environmentType,
            MessageContentType: messageContentType,
            IsEmailWebPage: config.isEmailWebPage,
            WebPageURL: $container.find('[data-form="' + config.dataFormMapping.webPageURL + '"]').val(),
            Priority: $container.find('[data-form="' + config.dataFormMapping.priority + '"]').val()
        };
        if (config.isEmailWebPage)
            configuration.UserID = SW.Core.UserAccountControl.getUserName();
        return configuration;
    };

    function isEmail(email, allowMacros) {
        var validationResult;
        var regex = /.+@.+/;
        var regexMacros = /^\$\{([a-zA-Z0-9_.+-=;\s\$\{\}\'\"])+\}\;*$/;
        validationResult = regex.test(email);
        if (!validationResult && allowMacros)
            validationResult = regexMacros.test(email);
        return validationResult;
    }

    function isEditable(key) {
        return !config.multiEditMode || $container.find('[data-edited="' + key + '"] input:checked').length == 1;
    }

    function validateEmailsInput(value, dataMappingKey) {
        var emailSplitRegex = /,|;(?!M=)/;
        var validationResult = true;
        var elem = $container.find('[data-form="' + dataMappingKey + '"]');
        var allowMacros = $(elem).attr("AllowMacros") == 'True';
        elem = elem.parent().find(".emailInputField");
        $.each(value.split(emailSplitRegex), function() {
            if (!isEmail($.trim(this), allowMacros)) {
                validationResult = false;
                elem.addClass("invalidValue");
                //expand
                if (elem.parents('#senderDetails').length) {
                    $('#senderDetails').show();
                    $("#collapseExpandButton").attr('src', '/Orion/images/Button.Collapse.gif');
                    $("#senderInfo").text("");
                }
            }
        });
        return validationResult;
    }
    function validateMessageSection(configuration, callback) {
        var valid = true;
        $container.find('[data-form="' + config.dataFormMapping.emailSubject + '"]').removeClass("warningValue");
        $("#checkboxValidation").hide();
        if (!config.isEmailWebPage && $container.find('[data-form="' + config.dataFormMapping.stringContentType + '"]:checked').length == 0) {
            $("#checkboxValidation").show();
            valid = false;
        }
        $container.find('[data-form="' + config.dataFormMapping.webPageURL + '"]').removeClass("invalidValue");
        if (config.isEmailWebPage) {
            if (isEditable(config.dataFormMapping.webPageURL) && configuration.WebPageURL == "") {
                valid = false;
                $container.find('[data-form="' + config.dataFormMapping.webPageURL + '"]').addClass("invalidValue");
                if (valid) {
                    SW.Core.Services.callControllerSync("/api/Email/ValidateUrl", configuration, function (result) {
                        valid = result;
                        if (!valid)
                            $container.find('[data-form="' + config.dataFormMapping.webPageURL + '"]').addClass("invalidValue");
                    });
                }
            }

            if (isEditable(config.dataFormMapping.userID) && $("input[id*='useAnotherUser']").is(':checked')) {
                SW.Core.UserAccountControl.testCredentials(function (isValid) {
                    callback(isValid && valid);
                });
                return;
            }
        }
        callback(valid);
    }

    function validateResipientsSection(configuration) {
        var valid = true;
        $container.find('[data-form="' + config.dataFormMapping.emailTo + '"]').parent().find(".emailInputField").removeClass("invalidValue");
        $container.find('[data-form="' + config.dataFormMapping.emailFrom + '"]').parent().find(".emailInputField").removeClass("invalidValue");
        $container.find('[data-form="' + config.dataFormMapping.emailCC + '"]').parent().find(".emailInputField").removeClass("invalidValue");
        $container.find('[data-form="' + config.dataFormMapping.emailBCC + '"]').parent().find(".emailInputField").removeClass("invalidValue");
        if (isEditable(config.dataFormMapping.emailTo)) {
            valid = validateEmailsInput(configuration.EmailTo, config.dataFormMapping.emailTo) && valid;
        }
        if (isEditable(config.dataFormMapping.emailCC) && configuration.EmailCC != "") {
            valid = validateEmailsInput(configuration.EmailCC, config.dataFormMapping.emailCC) && valid;
        }
        if (isEditable(config.dataFormMapping.emailBCC) && configuration.EmailBCC != "") {
            valid = validateEmailsInput(configuration.EmailBCC, config.dataFormMapping.emailBCC) && valid;
        }
        if (isEditable(config.dataFormMapping.emailFrom)) {
            valid = validateEmailsInput(configuration.EmailFrom, config.dataFormMapping.emailFrom) && valid;
        }

        if (valid) {
            valid = validateConfg(configuration);
        }
        return valid;
    }

    function validateSMTP(configuration) {
        var valid = true;
        if (isEditable(config.dataFormMapping.smtpServer))
            if (configuration.SMTPServer.ServerID == -1) {
                valid = SW.Core.SMTPServerSettingsController.validate();
            }
        return valid;
    }
    var createActionAndExecuteCallback = function (configuration, callback) {
        if (configuration.Subject == "") {
            Ext.Msg.show({
                title: '@{R=Core.Strings;K=WEBJS_VL0_1;E=js}',
                msg: '@{R=Core.Strings;K=WEBJS_VL0_2;E=js}',
                modal: true,
                closable: false,
                minWidth: 500,
                buttons: { ok: '@{R=Core.Strings;K=WEBJS_VL0_3;E=js}', cancel: "@{R=Core.Strings;K=WEBJS_VL0_4;E=js}" },
                icon: Ext.MessageBox.WARNING,
                fn: function (btn, text) {
                    if (btn == 'cancel') {
                    } else
                        callActionAndExecuteCallback("Create", configuration, callback);
                }
            });
        } else {
            callActionAndExecuteCallback("Create", configuration, callback);
        }
    };

    var updateActionAndExecuteCallback = function (action, configuration, callback) {

        var updateConfig = {
            Action: action,
            Configuration: configuration
        };
        if (configuration.Subject == "") {
            Ext.Msg.show({
                title: '@{R=Core.Strings;K=WEBJS_VL0_1;E=js}',
                msg: '@{R=Core.Strings;K=WEBJS_VL0_2;E=js}',
                modal: true,
                closable: false,
                minWidth: 500,
                buttons: { ok: '@{R=Core.Strings;K=WEBJS_VL0_3;E=js}', cancel: "@{R=Core.Strings;K=WEBJS_VL0_4;E=js}" },
                icon: Ext.MessageBox.WARNING,
                fn: function (btn, text) {
                    if (btn == 'cancel') {
                    } else
                        callActionAndExecuteCallback("Update", updateConfig, callback);
                }
            });
        } else {
            callActionAndExecuteCallback("Update", updateConfig, callback);
        }
    };

    var callActionAndExecuteCallback = function (action, param, callback) {
        SW.Core.Services.callController("/api/Email/" + action, param,
        // On success
            function (response) {
                if ($.isFunction(callback)) {
                    callback({ isError: false, actionDefinition: response });
                }
            },
        // On error
            function (msg) {
                if ($.isFunction(callback)) {
                    callback({ isError: true, ErrorMessage: msg });
                }
            }
        );
    };

    function testSmtpCreadentials() {
        var configuration = createConfiguration();
        $testSmtpSettings(configuration.EmailFrom, configuration.EmailTo);
    };

    // Public methods

    this.init = function () {
        $container = $('#' + config.containerID);

        if ($.isFunction(config.onReadyCallback)) {
            config.onReadyCallback(self);
        }
        if (config.isEmailWebPage) {
            setEnabledEditEmailField();
        }
        $testSmtpSettings = SW.Core.SMTPServerSettingsController.testSmtpCreadentials;
        SW.Core.SMTPServerSettingsController.testSmtpCreadentials = testSmtpCreadentials;

        if (!config.isEmailWebPage) {
            $("input[id*='checkHtml']").change(function() {
                if (this.checked) {
                    $("input[id*='checkPrintable']")[0].checked = true;
                    $("input[id*='checkPrintable']").attr('disabled', true);
                } else {
                    $("input[id*='checkPrintable']").removeAttr('disabled');
                }
            });

            if ($("input[id*='checkHtml']")[0] && $("input[id*='checkHtml']")[0].checked) {
                $("input[id*='checkPrintable']")[0].checked = true;
                $("input[id*='checkPrintable']").attr('disabled', true);
            }
        }
    };

    // Validate action configuration. 
    this.validateSectionAsync = function (sectionID, callback) {
        var valid = true;
        var configuration = createConfiguration();
        if (sectionID == "recipients")
            valid = validateResipientsSection(configuration);
        else if (sectionID == "messageDetails") {
            validateMessageSection(configuration, callback);
            return;
        }
        else if (sectionID == "SmtpSection") {
            valid = validateSMTP(configuration);
        }
        if ($.isFunction(callback)) {
            callback(valid);
        }
    };
    this.getSectionAnnotation = function (sectionID) {
        var configuration = createConfiguration();
        var annotation = "";
        switch (sectionID) {
            case "recipients":
                annotation = configuration.EmailTo;
                break;
            case "messageDetails":
                annotation = configuration.Subject;
                break;
            case "SmtpSection":
                annotation = $('[data-form="smtpServerID"]').find("option[value='" + configuration.SMTPServer.ServerID + "']").text();
                if (annotation == "@{R=Core.Strings.2;K=WEBJS_OS0_7;E=js}") {
                    annotation = $('[data-form="smtpServerIP"]').val();
                }
                break;
            case "timeOfDay":
                annotation = "";
                break;
            case "escalation":
                annotation = "";
                break;
            default:
                annotation = "";
                break;
        }
        return annotation;
    };

    this.getActionDefinitionAsync = function (callback) {
        var configuration = createConfiguration();

        // Edit mode.
        if (config.actionDefinition) {

            // We have action definition we just want to updated existing one.
            updateActionAndExecuteCallback(config.actionDefinition, configuration, callback);

        } else {

            // New action definition mode.
            createActionAndExecuteCallback(configuration, callback);
        }
    };

    function setEnabledEditEmailField() {
        $container.find('[data-form="' + config.dataFormMapping.previewURL + '"]').on("click", previewUrl);
        if (config.multiEditMode) {

            for (var key in config.dataFormMapping) {
                var dataform = config.dataFormMapping[key];

                //Enable/Disable Email message with radio content type
                if (dataform == config.dataFormMapping.userID) {
                    $container.find(".contentType").attr("disabled", true);
                    var notUserElement = $("input[id*='notUseUser']");
                    notUserElement.attr("disabled", true);
                    var useCurrentUserElement = $("input[id*='useCurrentUser']");
                    useCurrentUserElement.attr("disabled", true);
                    var useAnotherUserElement = $("input[id*='useAnotherUser']");
                    useAnotherUserElement.attr("disabled", true);

                    $container.find('[data-edited="' + dataform + '"]').click(function () {
                        var disabled = !$(this).find("input").is(':checked');
                        if (disabled) {
                            SW.Core.UserAccountControl.radioSelectionChange(disabled);
                            notUserElement.attr('checked', true);
                        }
                        useAnotherUserElement.attr("disabled", disabled);
                        notUserElement.attr("disabled", disabled);
                        useCurrentUserElement.attr("disabled", disabled);
                    });
                }
                else if ((dataform == config.dataFormMapping.emailTo) || (dataform == config.dataFormMapping.emailFrom) || (dataform == config.dataFormMapping.emailCC) || (dataform == config.dataFormMapping.emailBCC)) {
                    $container.find('[data-form="' + dataform + '"]').parent().find(".emailInputText").attr("disabled", true).css("background-color", "transparent");
                    $container.find('[data-form="' + dataform + '"]').parent().find(".emailInputField").css("background-color", "#EBEBE4");
                    $container.find('[data-edited="' + dataform + '"]').click(function () {
                        var dataedited = $(this).attr("data-edited");
                        var isChecked = $(this).find("input").is(':checked');
                        var $emailInput = $container.find('[data-form="' + dataedited + '"]').parent();
                        $emailInput.find(".emailInputText").attr("disabled", !isChecked).css("background-color", "transparent");
                        $emailInput.find(".emailInputField").css("background-color", isChecked ? "" : "#EBEBE4");
                        $emailInput.find(".emailItem").attr("disabled", !isChecked);
                    });
                } else {
                    $container.find('[data-form="' + dataform + '"]').attr("disabled", true);
                    if (dataform == config.dataFormMapping.webPageURL) {
                        $container.find('[data-form="' + config.dataFormMapping.previewURL + '"]').addClass('sw-btn-disabled');
                        $container.find('[data-form="' + config.dataFormMapping.previewURL + '"]').off('click', previewUrl);
                    }

                    $container.find('[data-edited="' + dataform + '"]').click(function () {
                        var dataedited = $(this).attr("data-edited");
                        $container.find('[data-form="' + dataedited + '"]').attr("disabled", !$(this).find("input").is(':checked'));

                        if (dataedited == config.dataFormMapping.webPageURL) {
                            if (!$(this).find("input").is(':checked')) {
                                $container.find('[data-form="' + config.dataFormMapping.previewURL + '"]').addClass('sw-btn-disabled');
                                $container.find('[data-form="' + config.dataFormMapping.previewURL + '"]').off("click", previewUrl);
                            } else {
                                $container.find('[data-form="' + config.dataFormMapping.previewURL + '"]').removeClass('sw-btn-disabled');
                                $container.find('[data-form="' + config.dataFormMapping.previewURL + '"]').on("click", previewUrl);
                            }
                        }
                    });
                }

            }

            //Enable/Disable Smtp
            $container.find('[data-form="smtpServerID"]').attr("disabled", true);
            $container.find('[data-form="smtpServerSettings"]').hide();

            $container.find('[data-edited="' + config.dataFormMapping.smtpServer + '"]').click(function () {
                $container.find('[data-form="smtpServerID"]').attr("disabled", !$(this).find("input").is(':checked'));
                if ($container.find('[data-form="smtpServerID"]').val() == -1) {
                    if ($(this).find("input").is(':checked')) {
                        $container.find('[data-form="smtpServerSettings"]').show();
                        $container.find('[data-form="smtpServerEnableSSL"] input[type=checkbox]').change();
                    } else {
                        $container.find('[data-form="smtpServerSettings"]').hide();
                    }
                }
            });
        }
    }

    this.getUpdatedActionProperies = function (callback) {
        var isSenderEmpty = false;
        var jsonProperties = new Array();
        for (var key in config.dataFormMapping) {
            var dataform = config.dataFormMapping[key];
            if ($container.find('[data-edited="' + dataform + '"] input:checked').length == 1) {
                if (dataform == config.dataFormMapping.smtpServer) {
                    if ($container.find('[data-form="smtpServerID"]').val() == -1) {
                        jsonProperties.push({ PropertyName: dataform, PropertyValue: SW.Core.SMTPServerSettingsController.createSMTPServer() });
                    } else {
                        jsonProperties.push({ PropertyName: dataform, PropertyValue: $container.find('[data-form="smtpServerID"]').val() });
                    }
                } else if (dataform == config.dataFormMapping.emailSubject) {
                    isSenderEmpty = $container.find('[data-form="' + dataform + '"]').val() == "";
                    jsonProperties.push({ PropertyName: dataform, PropertyValue: $container.find('[data-form="' + dataform + '"]').val() });
                } else if (dataform == config.dataFormMapping.userID) {
                    jsonProperties.push({ PropertyName: dataform, PropertyValue: SW.Core.UserAccountControl.getUserName() });
                }
                else {
                    jsonProperties.push({ PropertyName: dataform, PropertyValue: $container.find('[data-form="' + dataform + '"]').val() });
                }
            }
        }
        if (callback) {
            if (isSenderEmpty) {
                Ext.Msg.show({
                    title: '@{R=Core.Strings;K=WEBJS_VL0_1;E=js}',
                    msg: '@{R=Core.Strings;K=WEBJS_VL0_2;E=js}',
                    modal: true,
                    closable: false,
                    minWidth: 500,
                    buttons: { ok: '@{R=Core.Strings;K=WEBJS_VL0_3;E=js}', cancel: "@{R=Core.Strings;K=WEBJS_VL0_4;E=js}" },
                    icon: Ext.MessageBox.WARNING,
                    fn: function (btn, text) {
                        if (btn == 'cancel') {
                        } else
                            callback(JSON.stringify(jsonProperties));
                    }
                });
            } else {
                callback(JSON.stringify(jsonProperties));
            }

        }
    };


    // Constructor

    var self = this;
    var $container = null;
    var $testSmtpSettings = null;
}
