﻿var enums = SW.Core.namespace("SW.Core.Actions.Enums");

// Enum is defined in SolarWinds.Orion.Core.Models.Actions.ActionStatus and needs to be kept in sync.
enums.ActionStatus = enums.ActionStatus || {
        Unknown : 0,
        Success : 1,
        Failed : 2,
        Canceled : 3
};