﻿SW.Core.namespace("SW.Core.Actions").ActionNameController = function(config) {
    "use strict";

    this.getActionName = function () {
        var text;
        text = $container.find('[data-form="ActionName"]').val();
        if ($multiEditEnable == true && $multiActioName.is(':checked') == false) {
            text = undefined;
        };
        return text;
    };
    this.setActionName = function (value) {
        $container.find('[data-form="ActionName"]').val(value);
    };

    this.init = function() {
        $container = config.containerID;
        try {
            $multiEditEnable = (config.IsMultiEdit.toString().toLowerCase() === "true");
        } catch (e) {
            $multiEditEnable = false;
        }

        try {
            this.setActionName(config.ActionDefinition.Title);
        } catch (e) {
            $container.find('[data-form="ActionName"]').focus();
            $container.find('[data-form="ActionName"]').select();
        }
        $multiActioName = $container.find('[data-edited="actionNameMultiEnable"] input');

        $multiActioName.click(function () {
            $container.find('[data-form="ActionName"]').attr("disabled", !$multiActioName.is(':checked'));
        });

        if (!$multiEditEnable) {
            $multiActioName.hide();
            $container.find("#divTextContainer").removeClass("textContainer");  
        } else {
            $container.find('[data-form="ActionName"]').attr("disabled", true);
            $container.find('[data-id="labelActionName"]').attr("style", "font-weight: bold;");
        }
    };

    var $multiActioName;
    var $container;
    var $multiEditEnable;
}