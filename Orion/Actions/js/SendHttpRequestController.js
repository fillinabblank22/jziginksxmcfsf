﻿SW.Core.namespace("SW.Core.Actions").SendHttpRequestController = function (config) {
    "use strict";
    var RequestMethods = {
        GET: 1,
        POST: 2
    };
    var self = this;
    var container = null;
    const AUTH_NONE = "1";
    const AUTH_BASIC = "2";
    const AUTH_NTLM = "3";
    const AUTH_TOKEN = "4";
    const NO_CREDS = "-1";
    const REFRESH_CREDS = "-2";
    // Private methods

    var getRequestMethod = function () {
        return container.find('[data-form="' + config.dataFormMapping.method + '"] :checked').val();
    };

    var isAuthEnabled = function () {
        var authMethod = getAuthMethod();
        if (authMethod == 1) {
            return false;
        }
        return true;
    };

    var getAuthMethod = function () {
        return container.find('[data-form="' + config.dataFormMapping.authMethod + '"] :checked').val();
    };

    var getPostBody = function () {
        if (getRequestMethod() === RequestMethods.POST.toString()) {
            return container.find('[data-form="' + config.dataFormMapping.body + '"]').val();
        } else {
            return "";
        }
    };

    var getUrlElement = function () {
        return container.find('[data-form="' + config.dataFormMapping.url + '"]');
    };

    var getLoginElement = function () {
        if (isAuthEnabled()) {
            return container.find('[data-form="' + config.dataFormMapping.login + '"]').val();
        } else {
            return "";
        }
    };

    var getCredName = function () {
        if (isAuthEnabled()) {
            if (getAuthMethod() === "4") {
                return getHeaderNameInputElement().val();
            }
            return getCredDescription().val();
        }
        return "";
    };
    
    var getCredDescription = function() {
        return container.find('[data-form="' + config.dataFormMapping.credDescription + '"]');
    };

    var getSelectedCredId = function () {
        if (isAuthEnabled()) {
            return container.find('[data-form="' + config.dataFormMapping.credList + '"]')[0].value;
        } else {
            return "";
        }
    };	

    var getPassword = function() {
        if (isAuthEnabled()) {
            if (getAuthMethod() === "4") {
                return getHeaderValueInputElement().val();
            }
            return getPasswordElement();
        }
        return "";
    }

    var getLogin = function() {
        if (isAuthEnabled()) {
            if (getAuthMethod() === "4") {
                return getHeaderNameInputElement().val();
            }
            return getLoginElement();
        }
        return "";
    }
    var getPasswordElement = function () {
        if (isAuthEnabled()) {
            return container.find('[data-form="' + config.dataFormMapping.password + '"]').val();
        } else {
            return "";
        }
    };

    var getConfirmPasswordElement = function () {
        if (isAuthEnabled()) {
            return container.find('[data-form="' + config.dataFormMapping.confirmPassword + '"]').val();
        } else {
            return "";
        }
    };

    var getHeaderValueInputElement = function () {
        return container.find('[data-form="' + config.dataFormMapping.headerValue + '"]');
    };

    var getHeaderNameInputElement = function () {
        return container.find('[data-form="' + config.dataFormMapping.headerName + '"]');
    };

    var getContentType = function () {
        return container.find('[data-form="' + config.dataFormMapping.contentType + '"]').val();
    };

    var toggleMessageSection = function () {
        var isPostRequestMethodSelected = getRequestMethod() === RequestMethods.POST.toString();
        container.find('[data-form="' + config.dataFormMapping.bodySection + '"]').toggle(isPostRequestMethodSelected);
        container.find('#txtBodyInsertVariable').toggle(isPostRequestMethodSelected);
    };

    var toggleAuthSection = function () {
        container.find('[data-form="RequestAuthSection"]').toggle(getAuthMethod() !== AUTH_NONE);
        container.find('[data-form="RequestLoginForm"]').toggle(getAuthMethod() === AUTH_NTLM || getAuthMethod() == AUTH_BASIC);
        container.find('[data-form="headerManagerDialog"]').toggle(getAuthMethod() === AUTH_TOKEN);
    };

    var isEditable = function (key) {
        return !config.multiEditMode || container.find('[data-edited="' + key + '"] input:checked').length == 1;
    };

    var validateConfiguration = function (value) {
        // remove invalidValue class from all inputs
        container.find('input').toggleClass('invalidValue', false);
        var isValid = true;
        if (isEditable(config.dataFormMapping.url)) {

            if (getUrlElement().val().length == 0) {
                isValid = false;
                getUrlElement().toggleClass('invalidValue', true);
            }

            if (isAuthEnabled() && (getSelectedCredId() === NO_CREDS)) {
                if ((getAuthMethod() === AUTH_BASIC) || ((getAuthMethod() === AUTH_NTLM))) {
                    if (getLoginElement().length == 0) {
                        isValid = false;
                        container.find('[data-form="' + config.dataFormMapping.login + '"]').toggleClass('invalidValue', true);
                    }
                    if (getCredDescription().val().length == 0) {
                        isValid = false;
                        container.find('[data-form="' + config.dataFormMapping.credDescription + '"]').toggleClass('invalidValue', true);
                    }    
                    if (getPasswordElement().length == 0) {
                        isValid = false;
                        container.find('[data-form="' + config.dataFormMapping.password + '"]').toggleClass('invalidValue', true);
                    }
                    if (getConfirmPasswordElement().length == 0) {
                        isValid = false;
                        container.find('[data-form="' + config.dataFormMapping.confirmPassword + '"]').toggleClass('invalidValue', true);
                    }
                    if (getConfirmPasswordElement() !== getPasswordElement()) {
                        isValid = false;
                        container.find('[data-form="' + config.dataFormMapping.password + '"]').toggleClass('invalidValue', true);
                        container.find('[data-form="' + config.dataFormMapping.confirmPassword + '"]').toggleClass('invalidValue', true);
                    }
                }
                if (getAuthMethod() === AUTH_TOKEN) {
                    if (getHeaderNameInputElement().val().length === 0) {
                        isValid = false;
                        getHeaderNameInputElement().toggleClass('invalidValue', true);
                    }
                    if (getHeaderValueInputElement().val().length === 0) {
                        isValid = false;
                        getHeaderValueInputElement().toggleClass('invalidValue', true);
                    }
                }
            }

            if (Configuration.getConfiguration().CredentialsId == NO_CREDS) {
                container.find('[data-form="' + config.dataFormMapping.credDescription + '"]').toggleClass('invalidValue', true);
                isValid = false;

                // we need to do another request in case if user will fix this field
                Configuration.resetConfiguration();
            }

            if (isValid) {
                SW.Core.Services.callControllerSync("/api/SendHttpRequest/ValidateUrl", Configuration.getConfiguration(), function (result) {
                    isValid = result;
                    $('[data-form="HttpRequestUrl"]').toggleClass('invalidValue', true);
                });
            }
        }

        // we need to clear configuration in case of any validation erros
        if(!isValid) {
            Configuration.resetConfiguration();
        }      

        return isValid;
    };

    var Configuration = (function () {
        var instance;
     
        function createInstance() {
            return {
                Method: getRequestMethod(),
                Url: getUrlElement().val(),
                Body: getPostBody(),
                AuthType: getAuthMethod(),
                Login: (getAuthMethod() === AUTH_TOKEN) ? getHeaderValueInputElement().val() : getLoginElement(),
                Password: (getAuthMethod() === AUTH_TOKEN) ? getHeaderNameInputElement().val() : getPasswordElement(),
                CredentialsId: getCredId(),
                ContentType: getContentType()
            };
        }
    
        return {
            getConfiguration: function () {
                if (!instance) {
                    instance = createInstance();
                }
                return instance;
            },
            resetConfiguration: function() {
                instance = null;
            }
        };
    })();

    var createActionAndExecuteCallback = function (configuration, callback) {
        callActionAndExecuteCallback("Create", configuration, callback);
    };

    var updateActionAndExecuteCallback = function (action, configuration, callback) {
        var request = {
            Action: action,
            Configuration: configuration
        };
        callActionAndExecuteCallback("Update", request, callback);
    };

    var callActionAndExecuteCallback = function (action, param, callback) {
        SW.Core.Services.callController("/api/SendHttpRequest/" + action, param,
        // On success
            function (response) {
                if ($.isFunction(callback)) {
                    callback({ isError: false, actionDefinition: response });
                }
            },
        // On error
            function (msg) {
                if ($.isFunction(callback)) {
                    callback({ isError: true, ErrorMessage: msg });
                }
            }
        );

    };

    var toggleEnabled = function (elementName, enable) {
        var dataElement = container.find('[data-form="' + elementName + '"]');
        dataElement.prop("disabled", !enable);
        dataElement.find("*").prop("disabled", !enable);
    };

    var getCredId = function() {
        var ddCredList = container.find('[data-form="' + config.dataFormMapping.credList + '"]')[0];
        if (ddCredList.value == NO_CREDS) {
            var newId = saveNewCred();

            if (newId == NO_CREDS) {
                return newId;
            }

            var option = document.createElement('option');
            option.value = newId;
            option.innerHTML = getCredName();
            ddCredList.options.add(option);

            ddCredList.value = newId;

            return ddCredList.value;
        }
        return container.find('[data-form="' + config.dataFormMapping.credList + '"]')[0].value;
    };

    var saveNewCred = function() {

        if (getAuthMethod() === AUTH_NONE) {
            return "";
        }

        var credToSave = {
            credentialId: null, 
            name: getCredName(), 
            password: getPassword(), 
            username: getLogin()
        };
        var createdCredId;

        if (getAuthMethod() === AUTH_TOKEN) {
            var apiCred = {
                token: getHeaderValueInputElement().val(),
                name: getHeaderNameInputElement().val(),
                credentialId: null
            };
            SW.Core.Services.callWebServiceSync("/Orion/Services/CredentialManagerService.asmx",
            "InsertUpdateApiCredentails", apiCred,
            function (result) {
                createdCredId = result;
            });
        } else {
            SW.Core.Services.callWebServiceSync("/Orion/Services/CredentialManagerService.asmx",
            "InsertNewCredential", credToSave,
            function (result) {
                createdCredId = result;
            });
        }

        return createdCredId;
    }

    var getApiCreds = function () {
        SW.Core.Services.callWebServiceSync("/Orion/Services/CredentialManagerService.asmx",
            "GetApiCredentails", { credentialId: config.dataFormMapping.selectedCredId },
        function (result) {
            $('[data-form="HeaderValue"]').val(result.ApiKey);
            $('[data-form="HeaderName"]').val(result.Name);
        });
    }

    var refreshCredentialsList = function () {
        var ddCredentiallist = getCredList();
        var selectedId = config.dataFormMapping.selectedCredId;

        // Refresh if selected "Refres this list" item
        if (ddCredentiallist.value === REFRESH_CREDS || ddCredentiallist.options.length === 0) {
            SW.Core.Services.callWebServiceSync("/Orion/Services/CredentialManagerService.asmx",
                "GetCredentialsForDropDown", {},
                function (result) {
                    ddCredentiallist.options.length = 0;
                    var valueExist = false;

                    $.each(result, function (key, value) {
                        var option = document.createElement('option');
                        option.value = value.id;
                        option.innerHTML = value.name;
                        ddCredentiallist.options.add(option);

                        if (option.value === selectedId) {
                            valueExist = true;
                        } 
                    });

                    if (valueExist) {
                        ddCredentiallist.value = selectedId;
                    } else {
                        ddCredentiallist.value = NO_CREDS;
                    }
            });
        } 
        
        toggleCreateNewCredPanel();
    };

    var toggleCreateNewCredPanel = function () {
        container.find('[data-form="' + config.dataFormMapping.requestLoginForm + '"]').hide();
        container.find('[data-form="' + config.dataFormMapping.headerManagerDiag + '"]').hide();
        container.find('[data-form="' + config.dataFormMapping.credList + '"]').show();

        if (getAuthMethod() === AUTH_TOKEN) {
            container.find('[data-form="' + config.dataFormMapping.headerManagerDiag + '"]').show();
            container.find('[data-form="' + config.dataFormMapping.credList + '"]').hide();
        }

        if (getCredList().value === NO_CREDS) {
            container.find('[data-form="' + config.dataFormMapping.credCreateDialog + '"]').show();
            if (getAuthMethod() !== AUTH_TOKEN) {
                container.find('[data-form="' + config.dataFormMapping.requestLoginForm + '"]').show();
            }
        }
    };

    var getCredListElement = function () {
        return container.find('[data-form="' + config.dataFormMapping.credList + '"]');
    };

    var getCredList = function () {
        return getCredListElement()[0];
    }

    // Public methods

    this.init = function () {
        container = $('#' + config.containerID);

        container.find('[data-form="' + config.dataFormMapping.method + '"]').on('change', function () {
            toggleMessageSection();
        });


        container.find('[data-form="' + config.dataFormMapping.authMethod + '"]').on('change', function () {
            config.dataFormMapping.selectedCredId = NO_CREDS;

            if (isAuthEnabled()) {
                // check if we changed authentication method and clear credentials
                getCredList().value = config.dataFormMapping.selectedCredId;
            }

            toggleAuthSection();
            toggleCreateNewCredPanel();
        });


        toggleMessageSection();

        if (!isAuthEnabled()) {
            toggleAuthSection();
        }

        container.find('[data-form="' + config.dataFormMapping.credDialog + '"]').hide();

        if (getAuthMethod() === AUTH_TOKEN) {
            getApiCreds();
        }

        refreshCredentialsList();

        getCredListElement().change(function() { 
            refreshCredentialsList(); 
        });

        if (config.multiEditMode) {
            for (var key in config.dataFormMapping) {
                var dataForm = config.dataFormMapping[key];
                var dataEditCtrl = container.find('[data-edited="' + dataForm + '"]');
                if (dataEditCtrl.length > 0) {
                    toggleEnabled(dataForm, false);

                    dataEditCtrl.click(function (dataName, editCtrl) {
                        return function () {
                            var enable = $(editCtrl).find("input").is(':checked');
                            toggleEnabled(dataName, enable);
                        };
                    } (dataForm, dataEditCtrl));
                }
            }
        }

        if ($.isFunction(config.onReadyCallback)) {
            config.onReadyCallback(self);
        }
    };

    this.validateSectionAsync = function (sectionID, callback) {
        var valid = false;

        valid = validateConfiguration();

        if ($.isFunction(callback)) {
            callback(valid);
        }
    };

    this.getUpdatedActionProperies = function (callback) {
        var jsonProperties = [];
        for (var key in config.dataFormMapping) {
            var dataForm = config.dataFormMapping[key];
            if (container.find('[data-edited="' + dataForm + '"]  input:checked').length == 1) {
                switch (dataForm) {
                    case config.dataFormMapping.url:
                        jsonProperties.push({ PropertyName: dataForm, PropertyValue: getUrlElement().val() });
                        break;
                    case config.dataFormMapping.methodSection:
                        jsonProperties.push({ PropertyName: config.dataFormMapping.method, PropertyValue: getRequestMethod() });
                        jsonProperties.push({ PropertyName: config.dataFormMapping.body, PropertyValue: getPostBody() });
                        break;
                }
            }
        }
        if (callback) {
            callback(JSON.stringify(jsonProperties));
        }
    };

    this.getActionDefinitionAsync = function (callback) {
        var configuration = Configuration.getConfiguration();

        if (config.actionDefinition) {
            updateActionAndExecuteCallback(config.actionDefinition, configuration, callback);
        } else {
            createActionAndExecuteCallback(configuration, callback);
        }
    };
};
