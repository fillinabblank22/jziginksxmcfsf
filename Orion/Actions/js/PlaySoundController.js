﻿SW.Core.namespace("SW.Core.Actions").PlaySoundController = function (config) {
    "use strict";

    var createConfiguration = function () {
        var configuration = {
            Message: $container.find('[data-form="' + config.dataFormMapping.message + '"]').val()
        };

        return configuration;
    };

    function isEditable(key) {
        return !config.multiEditMode || $container.find('[data-edited="' + key + '"] input:checked').length == 1;
    }

    var callActionAndExecuteCallback = function (action, param, callback) {

        SW.Core.Services.callController("/api/PlaySound/" + action, param,
        // On success
            function (response) {
                if ($.isFunction(callback))
                    callback({ isError: false, actionDefinition: response });
            },
        // On error
            function (msg) {
                if ($.isFunction(callback)) {
                    callback({ isError: true, ErrorMessage: msg });
                }
            }
        );
    };

    var updateActionAndExecuteCallback = function (action, configuration, callback) {
        var updateConfig = {
            Action: action,
            Configuration: configuration
        };

        callActionAndExecuteCallback("Update", updateConfig, callback);
    };

    var createActionAndExecuteCallback = function (configuration, callback) {
        callActionAndExecuteCallback("Create", configuration, callback);
    };

    // Validate action configuration. 
    this.validateSectionAsync = function (sectionID, callback) {
        var configuration = createConfiguration();
        validate(configuration, callback);
    };

    this.getUpdatedActionProperies = function (callback) {
        var jsonProperties = new Array();
        if (isEditable(config.dataFormMapping.message)) {
            jsonProperties.push({ PropertyName: config.dataFormMapping.message, PropertyValue: $container.find('[data-form="' + config.dataFormMapping.message + '"]').val() });
        }

        if (callback) {
            callback(JSON.stringify(jsonProperties));
        }
    };

    this.getActionDefinitionAsync = function (callback) {
        var configuration = createConfiguration();

        // Edit mode.
        if (config.actionDefinition) {

            // We have action definition we just want to updated existing one.
            updateActionAndExecuteCallback(config.actionDefinition, configuration, callback);

        } else {

            // New action definition mode.
            createActionAndExecuteCallback(configuration, callback);
        }
    };

    function validateConfiguration(configuration, callback) {
        var valid = true;
        $container.find('[data-form="' + config.dataFormMapping.message + '"]').removeClass("invalidValue");

        if (isEditable(config.dataFormMapping.message) && configuration.Message != "") {
            valid = validatePathInput(configuration.Message, $container.find('[data-form="' + config.dataFormMapping.message + '"]')) && valid;
        }

        return callback(valid);
    }

    var validate = function (configuration, callback) {
        if (!isEditable(config.dataFormMapping.message)) {
            callback(true);
        }
        validateConfiguration(configuration, function (isValid) {
            if (isValid) {
                SW.Core.Services.callController("/api/PlaySound/ValidateConfg", configuration, function (result) {
                    var isValid = true;
                    for (var key in result) {
                        $container.find('[data-form="' + key + '"]').removeClass("invalidValue");
                        if (!result[key]) {
                            $container.find('[data-form="' + key + '"]').addClass("invalidValue");
                            isValid = false;
                        }
                    }
                    if ($.isFunction(callback))
                        callback(isValid);
                });
            } else {
                if ($.isFunction(callback))
                    callback(false);
            }
        });
    };

    function validatePathInput(value, elem) {
        var validationResult = true;
        if (!$.trim(value)) {
            validationResult = false;
            elem.addClass("invalidValue");
        }
        return validationResult;
    }


    this.init = function () {
        $container = $('#' + config.containerID);
        if (config.multiEditMode) {
            for (var key in config.dataFormMapping) {
                var dataform = config.dataFormMapping[key];
                $container.find('[data-edited="' + dataform + '"]').click(function () {
                    var dataEdited = $(this).attr("data-edited");
                    $container.find('[data-form="' + dataEdited + '"]').attr("disabled", !$(this).find("input").is(':checked'));
                });
                $container.find('[data-form="' + dataform + '"]').attr("disabled", true);
            }
        }

        if ($.isFunction(config.onReadyCallback)) {
            config.onReadyCallback(self);
        }
    };

    // Constructor
    var self = this;
    var $container = null;
}