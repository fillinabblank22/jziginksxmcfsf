﻿SW.Core.namespace("SW.Core.Actions").WriteToFileController = function (config) {
    "use strict";

    var numericStringRegex = /^\d+$/;

    var self = this;
    var $container = null;

    // Private methods

    function isEditable(key) {
        return !config.multiEditMode || $container.find('[data-edited="' + key + '"]').is(':checked');
    }

    var validateConfiguration = function (configuration, callback) {
        $container.find('[data-form="' + config.dataFormMapping.filePath + '"]').removeClass("invalidValue");
        $container.find('[data-form="' + config.dataFormMapping.message + '"]').removeClass("invalidValue");
        $container.find("[data-form='" + config.dataFormMapping.fileSizeMb + "']").removeClass("invalidValue");
        var isValid = true;
        $("#requiredFilePath").hide();

        if (isEditable(config.dataFormMapping.filePath)) {
            if (configuration.FilePath == '') {
                $container.find('[data-form="' + config.dataFormMapping.filePath + '"]').toggleClass('invalidValue', true);
                isValid = false;
            }
        }

        if (isEditable(config.dataFormMapping.message)) {
            if (configuration.Message == '') {
                $container.find('[data-form="' + config.dataFormMapping.message + '"]').toggleClass('invalidValue', true);
                isValid = false;
            }
        }

        if (isEditable(config.dataFormMapping.fileSizeMb)) {
            if (configuration.FileSizeMb != "" && parseInt(configuration.FileSizeMb) != "NaN") {
                if (!numericStringRegex.test(configuration.FileSizeMb)) {
                    $container.find("[data-form='" + config.dataFormMapping.fileSizeMb + "']").toggleClass('invalidValue', true);
                    isValid = false;
                }
            } else {
                $container.find("[data-form='" + config.dataFormMapping.fileSizeMb + "']").toggleClass('invalidValue', true);
                isValid = false;
            }
        }

        isValid = SW.Core.WindowsCredentialSelectorControl.ValidateCredentials() && isValid;
        isValid = SW.Core.WindowsCredentialSelectorControl.ValidateCredentialName() && isValid;
        isValid = validateFilePath(configuration, callback) && isValid;

        if ($.isFunction(callback))
            callback(isValid);
    };

    var validateFilePath = function (configuration, callback) {
        var isValid = false;

        SW.Core.Services.callControllerSync("/api/WriteToFile/ValidateFilePath", configuration,
            function (response) {// On success
                if (response) {
                    isValid = true;
                } else {
                    $container.find('[data-form="' + config.dataFormMapping.filePath + '"]').toggleClass('invalidValue', true);
                }
            },
            function () {// On error
                $container.find('[data-form="' + config.dataFormMapping.filePath + '"]').toggleClass('invalidValue', true);
            }
        );

        if (!isValid) {
            $("#requiredFilePath").show();
        }

        return isValid;
    }

    var createConfiguration = function () {
        var configuration = {
            FilePath: $container.find('[data-form="' + config.dataFormMapping.filePath + '"]').val(),
            Message: $container.find('[data-form="' + config.dataFormMapping.message + '"]').val(),
            FileSizeMb: $container.find('[data-form="' + config.dataFormMapping.fileSizeMb + '"]').val(),
            Credentials: SW.Core.WindowsCredentialSelectorControl.getCredentials()
        };

        return configuration;
    };

    var createActionAndExecuteCallback = function (configuration, callback) {
        callActionAndExecuteCallback("Create", configuration, callback);
    };

    var updateActionAndExecuteCallback = function (action, configuration, callback) {
        var request = {
            Action: action,
            Configuration: configuration
        };
        callActionAndExecuteCallback("Update", request, callback);
    };

    var callActionAndExecuteCallback = function (action, param, callback) {
        SW.Core.Services.callController("/api/WriteToFile/" + action, param,
            // On success
            function (response) {
                if ($.isFunction(callback)) {
                    callback({ isError: false, actionDefinition: response });
                }
            },
            // On error
            function (msg) {
                if ($.isFunction(callback)) {
                    callback({ isError: true, ErrorMessage: msg });
                }
            }
        );

    };

    var fieldEditEnabled = function (state, dataEditKey) {
        SW.Core.WindowsCredentialSelectorControl.radioDisabledChange(state);
        $container.find('[data-form="' + dataEditKey + '"]').attr("disabled", state);
    };

    // Public methods

    this.init = function () {
        SW.Core.WindowsCredentialSelectorControl.setConfig({
            TestCredentialsRoute: "/api/WriteToFile/TestCredentials",
            ValidateCredentialNameRoute: "/api/WriteToFile/ValidateCredentialName",
            GetConfiguration: createConfiguration,
            ValidateConfiguration: validateConfiguration
        });

        $container = $('#' + config.containerID);
        if (SW.Core.MacroVariablePickerController)
            SW.Core.MacroVariablePickerController.BindPicker();
        if ($.isFunction(config.onReadyCallback)) {
            config.onReadyCallback(self);
        }

        if (config.multiEditMode) {
            for (var key in config.dataFormMapping) {
                var dataform = config.dataFormMapping[key];
                $container.find('[data-form="' + dataform + '"]').attr("disabled", true);
                $container.find('[data-edited="' + dataform + '"]').click(function () {
                    var dataedited = $(this).attr("data-edited");
                    $container.find('[data-form="' + dataedited + '"]').attr("disabled", !$(this).is(':checked'));
                    fieldEditEnabled(disabled, dataEdited);
                });
            }
            fieldEditEnabled(true, config.dataFormMapping.programPath);
        }
    };

    this.getUpdatedActionProperies = function (callback) {
        var jsonProperties = new Array();
        for (var key in config.dataFormMapping) {
            var dataform = config.dataFormMapping[key];
            if ($container.find('[data-edited="' + dataform + '"]').is(':checked'))
                jsonProperties.push({ PropertyName: dataform, PropertyValue: $container.find('[data-form="' + dataform + '"]').val() });
        }
        if (isEditable(config.dataFormMapping.programPath)) {
            if (SW.Core.WindowsCredentialSelectorControl.isCredentialsDefined()) {
                jsonProperties.push({ PropertyName: "Credentials", PropertyValue: SW.Core.WindowsCredentialSelectorControl.addUpdateWindowsCredential() });
            }
        }
        if (callback) {
            callback(JSON.stringify(jsonProperties));
        }
    };

    this.validateSectionAsync = function (sectionID, callback) {
        if (sectionID == "writeToFileSection") {
            validateConfiguration(createConfiguration(), callback);
        }
        else {
            callback(true);
        }
    };

    this.getActionDefinitionAsync = function (callback) {
        var configuration = createConfiguration();
        if (config.actionDefinition) {
            updateActionAndExecuteCallback(config.actionDefinition, configuration, callback);
        } else {
            createActionAndExecuteCallback(configuration, callback);
        }
    };
}
