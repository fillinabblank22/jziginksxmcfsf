﻿﻿

/* Object is responsible for rendering the list of action plugins in grid.
 *
 * @param {Object} config 
 */
SW.Core.namespace("SW.Core.Actions").ActionPluginSelector = function (config) {
    
    "use strict";
    
    // Private methods:
    var actionRowClicked = function (selectionModel, rowIndex, record) {
        selectedActionID = record.data["ActionTypeID"];
   };

    var actionSelectedClicked = function() {
        
        if (selectedActionID) {
            var selectedPlugin = _.find(config.plugins, function (item) {
                return item.ActionTypeID == selectedActionID;
            });

            if (selectedPlugin && $.isFunction(config.onSet)) {
                config.onSet(selectedPlugin);
            }
        }
    };

    var preselectActionPlugin = function() {
        if (actionsGrid && actionsGrid.store && actionsGrid.store.data && actionsGrid.store.data.items) {
            
            var items = actionsGrid.store.data.items;
            for (var i = 0; i < items.length; i++) {
                 
                if (items[i].data["ActionTypeID"] == config.preselectedActionTypeID) {
                   
                    actionsGrid.getSelectionModel().selectRow(i);
                    
                     break;
                }
            }
        }
    };
    
    var onClose = function () {
        self.Hide();

        if ($.isFunction(config.onClose)) {
            config.onClose();
        }
    };

    var createAddActionInfoMessage = function(containerID) {
        $('#' + containerID).html('<div>' + '@{R=Core.Strings;K=WEBJS_ZT0_20;E=js}' + '</div>');
    };

    var createAddActionButtons = function(containerID) {
        
        var $containerID = '#' + containerID;

        $($containerID).css('text-align', 'right');

        $(SW.Core.Widgets.Button('@{R=Core.Strings;K=WEBJS_ZT0_21;E=js}', { type: 'primary', id: 'configureActionBtn' }))
            .appendTo($containerID)
            .css('margin', '10px')
            .on('click', actionSelectedClicked);
        
         $(SW.Core.Widgets.Button('@{R=Core.Strings;K=WEBJS_ZT0_22;E=js}', { type: 'secondary', id: 'cancelAddActionBtn' }))
            .appendTo($containerID)
            .css('margin', '10px 0 10px 10px')
            .on('click',  onClose);
    };

    var renderTooltip = function(value, metadata, record) {
        return '<span ext:qtip="' + value + '">' + value + '</span>';

    };

    var renderIcon = function(value, metadata, record) {
        if (value != '')
            return '<img src="' + value + '" alt="' + record.data.Title + '"/>';
        return value;
    };

    var createAddActionGrid = function(containerID) {
        
        actionsStore = new Ext.data.JsonStore({
          fields: [ 'ActionTypeID', 'Title', 'Description', 'IconPath' ],
          autoLoad: true,
          data: _.sortBy(config.plugins, "Title")
        });

        actionsSelectionModel.on("rowselect", actionRowClicked);

        actionsGrid = new Ext.grid.GridPanel({
            id: 'actionsSelectorGrid',
            viewConfig: { forceFit: true, scrollOffset: 0, autoFill: true },
            store: actionsStore,
            stripeRows: true,
            trackMouseOver: false,
            columns: [
                actionsSelectionModel,
                { id: 'ActionTypeID', dataIndex: 'ActionTypeID', hidden: true}, 
                { id: 'IconPath', header: '' , dataIndex: 'IconPath', sortable: false, width: 10,  renderer: renderIcon },
                { id: 'Title', header: '@{R=Core.Strings;K=WEBJS_ZT0_23;E=js}' , dataIndex: 'Title', sortable: true, width: 50, renderer: renderTooltip },
                { id: 'Description', header: '@{R=Core.Strings;K=WEBJS_ZT0_24;E=js}', dataIndex: 'Description', sortable: true, width: 440, renderer: renderTooltip}  
            ],
            sm: actionsSelectionModel,

            autoExpandColumn: 'Description',
            enableHdMenu: false,
            enableColumnResize: true,
            enableColumnMove: false,
            width: 980,
            autoHeight: true,
            renderTo: containerID,
            listeners: {
                'viewready': function() {
                    setTimeout(preselectActionPlugin, 0);
                }
            },
            cls: 'actionsSelectorGrid'
       
        });

        var columnWidth = 10;
        var textMetrics = Ext.util.TextMetrics.createInstance(actionsGrid.getEl());
        actionsStore.each(function (record) {
            var minWidth = textMetrics.getWidth(record.get("Title")) + 20;
            if (minWidth > columnWidth) {
                columnWidth = minWidth;
            }
        });

        var cm = actionsGrid.getColumnModel();
        cm.setColumnWidth(3, columnWidth);
    };

    // Public methods:

    this.ShowInDialog = function () {

        var $container = $('#' + config.containerID);
        
        $container.empty().append("<div class='actionPluginSelector' id='_actionPluginSelectorDialog_' />");        

        $dialogElement = $('#_actionPluginSelectorDialog_', $container);

        $dialogElement.html($("<div class='info' id='_actionPluginSelectorInfo_' /> <div id='_actionPluginSelectorGrid_' /> <div id='_actionPluginSelectorButtons_' />"));

        createAddActionInfoMessage('_actionPluginSelectorInfo_');
        createAddActionGrid('_actionPluginSelectorGrid_');
        createAddActionButtons('_actionPluginSelectorButtons_');

        $dialog = $dialogElement.dialog({
            modal: true,
            position: ['middle', 100],
            title: '@{R=Core.Strings;K=WEBJS_ZT0_25;E=js}',
            width: 1000,
            heigh: 'auto',
            minWidth : 1000,
            close: onClose
        });

    };

    this.ShowLoading = function() {
        $dialogElement.addClass('actionLoading');
    };

    this.Hide = function () {    
        $dialog.dialog('destroy');
        $('#_actionPluginSelectorDialog_').remove();
    };

    // Constructor:
    var self = this;
    var $dialogElement = null;
    var $dialog = null;
    var selectedActionID = null;
    var actionsSelectionModel = new Ext.sw.grid.RadioSelectionModel();
    var actionsStore = null;
    var actionsGrid = null;
    
     Ext.QuickTips.init();

    Ext.apply(Ext.QuickTips.getQuickTip(), {
        showDelay: 0,
        trackMouse: false
    });
};
