﻿SW.Core.namespace("SW.Core.Actions").ActionsEditorController = function (config) {
    "use strict";

    // Constants:

    var ON_PLUGIN_READY_FUNCTION_NAME = "SW.Core.Actions.ActionsEditor.onPluginReady";
    var DEFAULLT_ESCALATION_LEVEL = "0";

    // Private methods:

    var hideDialogContainer = function () {
        $("#" + config.dialogContainerID).empty().attr('style', '').addClass('actionsOffPage');
    };

    var showDialogContainer = function () {
        $("#" + config.dialogContainerID).removeClass('actionsOffPage');
    };

    var getAction = function (index, escalationLevel) {
        return actionsByEscalationLevel[escalationLevel].actions[index];
    };

    var editAction = function (index, escalationLevel) {
        showDialogContainer();
        var action = getAction(index, escalationLevel);

        var actionPlugin = _.find(config.plugins, function (item) {
            return item.ActionTypeID == action.ActionTypeID;
        });

        currentActionDefinitionController = new SW.Core.Actions.ActionDefinitionController({
            containerID: config.dialogContainerID,
            plugin: actionPlugin,
            viewContext: config.viewContext,
            onPluginReadyFunctionName: ON_PLUGIN_READY_FUNCTION_NAME,
            enviromentType: config.enviromentType,
            onCreated: function (actionDefinition) { actionDefinitionUpdated(actionDefinition, index, escalationLevel); },
            onError: handleActionDefinitionError,
            actionDefinition: action,
            onClose: hideDialogContainer
        });
		
		currentActionDefinitionController.LoadPlugin(null, true);
    };

    var testAction = function (index, escalationLevel) {
        var action = getAction(index, escalationLevel);

        SW.Core.Actions.TestActionController.TestAction({
            action: action,
            enviromentType: config.enviromentType,
            viewContext: config.viewContext,
            showNetObjectPicker: true
        });

    };

    var deleteAction = function (index, escalationLevel) {
        var actionDefinition = getAction(index, escalationLevel);

        if (actionDefinition) {
            removeAction(index, escalationLevel);
            renderAndSaveActions(actionsByEscalationLevel);
        }
    };
    var removeAction = function (index, escalationLevel) {
        actionsByEscalationLevel[escalationLevel].actions.splice(index, 1);
    };

    var copyAction = function (index, escalationLevel) {
        var actionDefinition = getAction(index, escalationLevel);

        if (actionDefinition) {
            var newAction = $.extend(true, {}, actionDefinition);
            newAction.ID = 0;
            newAction.IsShared = false;
            actionsByEscalationLevel[escalationLevel].actions.push(newAction);
            renderAndSaveActions(actionsByEscalationLevel);
        }

    };

    function htmlEncode(value) {
        return $('<div/>').text(value).html();
    }

    var saveActions = function (actions) {
        config.actions = [];
        for (var level in actions) {
            for (var i = 0; i < actions[level].actions.length; i++) {
                actions[level].actions[i].Order = i + 1;
                actions[level].actions[i].Tooltip = htmlEncode(actions[level].actions[i].Description);

                config.actions.push(actions[level].actions[i]);
            }
        }

        var $input = $('#' + config.actionsInputID);

        $input.val(JSON.stringify(config.actions));
    };

    var compareActionWaitTime = function (a, b) {
        var waitA = parseInt(getEscalationLevelPropertyValue(a));
        var waitB = parseInt(getEscalationLevelPropertyValue(b));
        if (waitA < waitB)
            return -1;
        if (waitA > waitB)
            return 1;
        return 0;
    };

    var groupActionbyEscalationLevel = function (actions) {
        if (!actions) {
            return [];
        }
        escalationNumberCount = 1;
        var actionsGroupedByEscalationLevel = {};

        actions.sort(compareActionWaitTime);
        var prevWaitTime = actions.length > 0 ? parseInt(getEscalationLevelPropertyValue(actions[0])) : 0;
        if (prevWaitTime != 0) {
            actionsGroupedByEscalationLevel[escalationNumberCount] = { waitTime: 0, actions: [] };
            escalationNumberCount++;
        }
        for (var i = 0; i < actions.length; i++) {
            if (prevWaitTime != parseInt(getEscalationLevelPropertyValue(actions[i]))) {
                escalationNumberCount++;
                prevWaitTime = parseInt(getEscalationLevelPropertyValue(actions[i]));
            }
            if (actionsGroupedByEscalationLevel[escalationNumberCount] == null) {
                actionsGroupedByEscalationLevel[escalationNumberCount] = { waitTime: getEscalationLevelPropertyValue(actions[i]), actions: [] };
            }
            actionsGroupedByEscalationLevel[escalationNumberCount].actions.push(actions[i]);
        }

        if (actionsGroupedByEscalationLevel[escalationNumberCount] == null) {
            actionsGroupedByEscalationLevel[escalationNumberCount] = { waitTime: 0, actions: [] };
        }

        return actionsGroupedByEscalationLevel;
    };

    var getEscalationLevelPropertyValue = function (action) {
        for (var i = 0; i < action.ActionProperties.length; i++) {
            if (action.ActionProperties[i].PropertyName == config.mapping.escalationLevelPropertyName) {
                return action.ActionProperties[i].PropertyValue;
            }
        }
        return 0;
    };

    var setEscalationLevelProperty = function (action, escalationLevel) {
        var availible = false;
        for (var i = 0; i < action.ActionProperties.length; i++) {
            if (action.ActionProperties[i].PropertyName == config.mapping.escalationLevelPropertyName) {
                action.ActionProperties[i].PropertyValue = actionsByEscalationLevel[escalationLevel].waitTime;
                action.ActionProperties[i].IsShared = action.IsShared;
                availible = true;
                break;
            }
        }
        if (!availible) {
            action.ActionProperties.push({ PropertyName: config.mapping.escalationLevelPropertyName, PropertyValue: actionsByEscalationLevel[escalationLevel].waitTime, IsShared: action.IsShared });
        }
    };
    var escalationNumberCount = 0;

    var getEscalationLevelDelta = function (escalationLevel) {
        var delta = 0;
        for (var level in actionsByEscalationLevel) {
            if (parseInt(level) < parseInt(escalationLevel)) {
                delta = parseInt(actionsByEscalationLevel[level].waitTime);
            }
        }
        return delta;
    };

    var renderActions = function (groupedActions) {
        var $container = $('#' + config.containerID);
        var $templatesContainer = $('<div></div>');

        for (var level in groupedActions) {
            var $actionsGroupContainer = $(String.format('<div data-section="action-group" data-wait="{0}" data-escalation-level="{1}"></div>', groupedActions[level].waitTime, level));

            if (escalationLevelsEnabled() && groupedActions[level].waitTime != 0) {
                var waitTemplate = $('#' + config.waitTemplateID).html();
                var waitHtml = $(_.template(waitTemplate, { wait: parseInt(groupedActions[level].waitTime) - getEscalationLevelDelta(level) }));

                waitHtml.delegate('[data-form="wait-time"]', "blur", waitChanged);
                waitHtml.delegate('[data-form="wait-time-span-type"]', "change", waitChanged);

                $actionsGroupContainer.append(waitHtml);
            }

            var actions = groupedActions[level].actions;

            for (var i = 0; i < actions.length; i++) {
                var action = actions[i];

                for (var j = 0; j < action.ActionProperties.length; j++) {
                    var actionProperty = action.ActionProperties[j];

                    if (actionProperty.PropertyName === config.mapping.executionRepeatTimeSpanPropertName) {
                        var repeatTimeSpanMinutes = actionProperty.PropertyValue;
                        if (repeatTimeSpanMinutes != 0) {
                            var interval = getRepeatInterval(repeatTimeSpanMinutes);
                            var intervalType = getRepeatIntervalType(repeatTimeSpanMinutes);
                            var intervalTypeShort = getRepeatIntervalTypeShort(repeatTimeSpanMinutes);

                            action.repeatInterval = interval + intervalTypeShort;
                            action.repeatIntervalTooltip = "@{R=Core.Strings.2;K=WEBJS_BV0_ALERT_REPEAT_TOOLTIP;E=js}";

                            if (interval === 1) {
                                action.repeatIntervalTooltip += intervalType;
                            } else {
                                action.repeatIntervalTooltip += interval + " " + intervalType;
                            }
                        }

                        break;
                    }
                }
            }

            var template;

            if (actions && actions.length > 0) {
                template = $('#' + config.actionDefinitionsTemplateID).html();
            } else {
                template = $('#' + config.noActionsTemplateID).html();
            }
            var html = $(_.template(template, { actions: actions, wait: groupedActions[level].waitTime, escalationNumberCount: level }));

            html.delegate(".edit", "click", function () {
                editAction($(this).parents('tr').attr('data-index'), $(this).parents('tr').attr('data-escalation-level'));
            });

            html.delegate(".copy", "click", function () {
                copyAction($(this).parents('tr').attr('data-index'), $(this).parents('tr').attr('data-escalation-level'));
            });

            if (config.enableTest) {
                html.delegate(".test", "click", function () {
                    testAction($(this).parents('tr').attr('data-index'), $(this).parents('tr').attr('data-escalation-level'));
                });
            } else {
                html.find(".empty td").attr("colspan", "4");
                html.find(".addNextAction td").attr("colspan", "4");
            }

            html.delegate(".delete", "click", function () {
                deleteAction($(this).parents('tr').attr('data-index'), $(this).parents('tr').attr('data-escalation-level'));
            });

            html.delegate('[data-form="addNew"]', "click", addNewAction);
            html.delegate('[data-form="assignAction"]', "click", assignNewActions);

            if (escalationLevelsEnabled()) {
                html.delegate('[data-form="escalation-level-close"]', "click", levelDelete);
            }

            $actionsGroupContainer.append(html);
            $templatesContainer.append($actionsGroupContainer);
        }

        if (escalationLevelsEnabled() && actionsByEscalationLevel[escalationNumberCount].actions.length > 0) {
            var addEscalationLevelButtonTemplate = $('#' + config.addEscalationLevelButtonTemplateID).html();
            var addEscalationLevelButtonContainer = $(_.template(addEscalationLevelButtonTemplate, {}));
            addEscalationLevelButtonContainer.delegate('[data-form="add-escalation-level"]', "click", addNewEscalationLevel);

            $templatesContainer.append(addEscalationLevelButtonContainer);
        }

        $container.find('[data-form="actionsList"]').html($templatesContainer);
    };

    var renderAndSaveActions = function (groupedActions) {
        saveActions(groupedActions);
        renderActions(groupedActions);
        initDragAndDrop();
    };

    var initDragAndDrop = function () {
        $(".actions-list-sortable").sortable({ items: ".action-list-item", connectWith: ".actions-list-sortable",
            cursor: "move",
            handle: "span.action-drag-icon",
            forcePlaceholderSize: true,
            tolerance: 'pointer',
            placeholder: "action-dragdrop-placeholder",
            stop: function (event, ui) {
                var item = ui.item;
                var actionDefinition = getAction(item.attr("data-index"), item.attr("data-escalation-level"));
                removeAction(item.attr("data-index"), item.attr("data-escalation-level"));

                var parentColumn = ui.item.parent(".actions-list-sortable");
                if (!parentColumn || parentColumn.length == 0) return;

                var escalationLevel = parentColumn.attr("data-escalation-level");

                var position = parseInt(parentColumn.children(".action-list-item").index(ui.item));

                setEscalationLevelProperty(actionDefinition, escalationLevel);
                actionsByEscalationLevel[escalationLevel].actions.splice(position, 0, actionDefinition);

                renderAndSaveActions(actionsByEscalationLevel);
            }
        });
    };

    var actionDefinitionUpdated = function (actionDefinition, index, escalationLevel) {
        if (escalationLevelsEnabled()) {
            setEscalationLevelProperty(actionDefinition, escalationLevel);
        }

        collectPluginsData(actionDefinition);

        hideDialogContainer();

        actionsByEscalationLevel[escalationLevel].actions[index] = actionDefinition;

        renderAndSaveActions(actionsByEscalationLevel);
    };

    var collectPluginsData = function (actionDefinition) {
        if (SW.Core.Actions.ActionName.getActionName() != null && SW.Core.Actions.ActionName.getActionName() != '') {
            actionDefinition.Title = SW.Core.Actions.ActionName.getActionName();
        }
        if (config.enviromentType == "Alerting") {
            actionDefinition.ActionProperties = actionDefinition.ActionProperties.concat(SW.Core.Actions.ExecutionSettings.getProperties());
            actionDefinition.TimePeriods = SW.Orion.FrequencyControllerDefinition.getFrequency();
        }
    };

    var actionDefinitionCreated = function (actionDefinition, createdActionEscalationLevel) {
        if (escalationLevelsEnabled()) {
            setEscalationLevelProperty(actionDefinition, createdActionEscalationLevel);
        }
        collectPluginsData(actionDefinition);
        hideDialogContainer();

        actionsByEscalationLevel[createdActionEscalationLevel] = actionsByEscalationLevel[createdActionEscalationLevel] || { waitTime: config.mapping.defaultEscalationWaitTime, actions: [] };
        actionsByEscalationLevel[createdActionEscalationLevel].actions.push(actionDefinition);

        renderAndSaveActions(actionsByEscalationLevel);
    };

    var actionsDefinitionAssigned = function (actionDefinitions, createdActionEscalationLevel) {

        hideDialogContainer();

        actionsByEscalationLevel[createdActionEscalationLevel] = actionsByEscalationLevel[createdActionEscalationLevel] || { waitTime: config.mapping.defaultEscalationWaitTime, actions: [] };
        $.each(actionDefinitions, function (index, value) {
            value.IsShared = true;
            if (escalationLevelsEnabled()) {
                setEscalationLevelProperty(value, createdActionEscalationLevel);
            }
            actionsByEscalationLevel[createdActionEscalationLevel].actions.push(value);
        });

        renderAndSaveActions(actionsByEscalationLevel);
    };

    var actionSelected = function (selectedPlugin, createdActionEscalationLevel) {

        if (selectedPlugin) {
            currentActionSelectorController.ShowLoading();

            currentActionDefinitionController = new SW.Core.Actions.ActionDefinitionController({
                containerID: config.dialogContainerID,
                plugin: selectedPlugin,
                viewContext: config.viewContext,
                enviromentType: config.enviromentType,
                onPluginReadyFunctionName: ON_PLUGIN_READY_FUNCTION_NAME,
                onCreated: function (actionDefinition) { actionDefinitionCreated(actionDefinition, createdActionEscalationLevel); },
                onError: handleActionDefinitionError,
                onClose: hideDialogContainer
            });

            currentActionDefinitionController.LoadPlugin(null, false);
        }
    };

    var assignNewActions = function () {
        if (escalationLevelsEnabled() && areAllWaitInputsValid() == false) {
            Ext.Msg.show({
                title: '@{R=Core.Strings.2;K=WEBJS_TM0_1; E=js}',
                msg: '@{R=Core.Strings.2;K=WEBJS_TM0_2; E=js}',
                modal: true,
                closable: false,
                minWidth: 500,
                buttons: { ok: 'OK' },
                icon: Ext.MessageBox.WARNING,
                fn: function () { }
            });
            return;
        }

        showDialogContainer();

        var createdActionEscalationLevel = DEFAULLT_ESCALATION_LEVEL;
        if (escalationLevelsEnabled()) {
            createdActionEscalationLevel = $(this).parents('[data-section="action-group"]').attr('data-escalation-level');
        }

        assignActionSelectorController = new SW.Core.Actions.AssignActionSelector({
            containerID: config.dialogContainerID,
            enviromentType: config.enviromentType,
            categoryType: config.categoryType,
            actions: config.actions,
            onAssigned: function (actionDefinitions) { actionsDefinitionAssigned(actionDefinitions, createdActionEscalationLevel); },
            preselectedActionTypeID: config.preselectedActionTypeID,
            onClose: hideDialogContainer
        });

        assignActionSelectorController.ShowInDialog();
    };

    var addNewAction = function () {
        if (escalationLevelsEnabled() && areAllWaitInputsValid() == false) {
            Ext.Msg.show({
                title: '@{R=Core.Strings.2;K=WEBJS_TM0_1; E=js}',
                msg: '@{R=Core.Strings.2;K=WEBJS_TM0_2; E=js}',
                modal: true,
                closable: false,
                minWidth: 500,
                buttons: { ok: 'OK' },
                icon: Ext.MessageBox.WARNING,
                fn: function () { }
            });
            return;
        }

        showDialogContainer();
        // todo: this is temporary solution to replace plugin description
        // we need to split Email plugin for alerts and reports and this should be removed after FB290588 will be implemented
        if (config.enviromentType == 'Alerting') {
            $.each(config.plugins, function (index, plugin) {
                if (plugin.ActionTypeID == 'Email')
                    plugin.Description = '@{R=Core.Strings;K=LIBCODE_IB0_9; E=js}';
            });
        }

        var createdActionEscalationLevel = DEFAULLT_ESCALATION_LEVEL;
        if (escalationLevelsEnabled()) {
            createdActionEscalationLevel = $(this).parents('[data-section="action-group"]').attr('data-escalation-level');
        }

        currentActionSelectorController = new SW.Core.Actions.ActionPluginSelector({
            containerID: config.dialogContainerID,
            plugins: config.plugins,
            onSet: function (selectedPlugin) { actionSelected(selectedPlugin, createdActionEscalationLevel); },
            preselectedActionTypeID: config.preselectedActionTypeID,
            onClose: hideDialogContainer
        });

        currentActionSelectorController.ShowInDialog();
    };

    var addNewEscalationLevel = function () {
        if (areAllWaitInputsValid() == false) {
            Ext.Msg.show({
                title: '@{R=Core.Strings.2;K=WEBJS_TM0_1; E=js}',
                msg: '@{R=Core.Strings.2;K=WEBJS_TM0_2; E=js}',
                modal: true,
                closable: false,
                minWidth: 500,
                buttons: { ok: 'OK' },
                icon: Ext.MessageBox.WARNING,
                fn: function () { }
            });
            return;
        } else {
            escalationNumberCount++;
            actionsByEscalationLevel[escalationNumberCount] = { waitTime: config.mapping.defaultEscalationWaitTime + getEscalationLevelDelta(escalationNumberCount), actions: [] };
            renderActions(actionsByEscalationLevel);
            initDragAndDrop();
        }
    };

    var waitChanged = function () {
        var waitInputElement = $(this).parents('.escalationWaitInfo').find('[data-form="wait-time"]');
        var typeInputElement = $(this).parents('.escalationWaitInfo').find('[data-form="wait-time-span-type"]');
        if (isWaitInputValid(waitInputElement) == false) {
            return;
        }

        var escalationLevel = $(this).parents('[data-section="action-group"]').attr('data-escalation-level');
        var newWaitValue = parseInt(getWaitValue(waitInputElement, typeInputElement)) + getEscalationLevelDelta(escalationLevel);

        var actionElements = $(this).parents('[data-section="action-group"]').find('.actionDefinition');
        var prviousValue = $(this).parents('[data-section="action-group"]').attr('data-wait');

        if (prviousValue == newWaitValue) {
            return;
        }

        var delta = newWaitValue - parseInt(prviousValue);

        if (actionsByEscalationLevel[escalationLevel]) {
            actionsByEscalationLevel[escalationLevel].waitTime = newWaitValue;
        } else {
            actionsByEscalationLevel[escalationLevel] = { waitTime: newWaitValue, actions: [] };
        }

        for (var level in actionsByEscalationLevel) {
            if (parseInt(level) > parseInt(escalationLevel)) {
                actionsByEscalationLevel[level].waitTime = parseInt(actionsByEscalationLevel[level].waitTime) + delta;

                for (var i = actionsByEscalationLevel[level].actions.length - 1; i >= 0; i--) {
                    setEscalationLevelProperty(actionsByEscalationLevel[level].actions[i], level);
                }
            }
        }

        for (var i = actionElements.length - 1; i >= 0; i--) {
            setEscalationLevelProperty(actionsByEscalationLevel[escalationLevel].actions[i], escalationLevel);
        }

        if (areAllWaitInputsValid() == false) {
            return;
        }

        renderAndSaveActions(actionsByEscalationLevel);
    };

    var getWaitValue = function (waitInputElement, typeInputElement) {
        var timeSpan = waitInputElement.val();
        var type = typeInputElement.val();

        var result = timeSpan;

        switch (type) {
            case "Days":
                result = timeSpan * 60 * 24;
                break;
            case "Hours":
                result = timeSpan * 60;
                break;
        }

        return parseInt(result).toString();
    };

    var isWaitInputValid = function (waitInput) {
        if (waitInput == null) {
            return false;
        }

        var newWaitValue = $(waitInput).val();

        if (isNaN(newWaitValue) || newWaitValue <= 0 || newWaitValue === '') {
            $(waitInput).addClass('invalidValue');
            return false;
        }

        $(waitInput).removeClass('invalidValue');
        return true;
    };

    var areAllWaitInputsValid = function () {
        return $('[data-section="action-group"]').find('.invalidValue[data-form="wait-time"]').length <= 0;
    };

    var levelDelete = function () {
        // Get the level to delete.
        var escalationLevelToDelete = parseInt($(this).attr('data-escalation-level'));

        // Shift each higher level down one to fill the gap.
        for (var i = escalationLevelToDelete; i < escalationNumberCount; i++) {
            var nextLevel = i + 1;
            actionsByEscalationLevel[i] = actionsByEscalationLevel[nextLevel];

            // If escalation level 1 was deleted, reset the wait time of the next level to zero.
            if (i == 1) {
                actionsByEscalationLevel[1].waitTime = 0;

                for (var j = 0; j < actionsByEscalationLevel[1].actions.length; j++) {
                    var action = actionsByEscalationLevel[1].actions[j];
                    for (var k = 0; k < action.ActionProperties.length; k++) {
                        if (action.ActionProperties[k].PropertyName == config.mapping.escalationLevelPropertyName) {
                            action.ActionProperties[k].PropertyValue = 0;
                            break;
                        }
                    }
                }
            }
        }

        // Delete the former top level, now that all have been shifted down.
        delete actionsByEscalationLevel[escalationNumberCount];
        escalationNumberCount--;

        // Save and display.
        saveActions(actionsByEscalationLevel);
        refreshEscalationLevels();
        renderActions(actionsByEscalationLevel);
    };

    var handleActionDefinitionError = function (errorMsg) {
        alert(errorMsg);

        currentActionSelectorController.Hide();

        hideDialogContainer();

    };

    var handleAssignActionDefinitionError = function (errorMsg) {
        alert(errorMsg);

        assignActionSelectorController.Hide();

        hideDialogContainer();

    };

    var refreshEscalationLevels = function () {
        if (escalationLevelsEnabled()) {
            actionsByEscalationLevel = groupActionbyEscalationLevel(config.actions);
        } else {
            actionsByEscalationLevel = new Object();
            actionsByEscalationLevel[escalationNumberCount] = { waitTime: DEFAULLT_ESCALATION_LEVEL, actions: config.actions };
        }
    };

    var init = function () {
        refreshEscalationLevels();
        renderAndSaveActions(actionsByEscalationLevel);
    };

    // Public methods:


    this.onPluginReady = function (pluginController) {
        // Method is called when plugin is loaded.

        $("#" + config.dialogContainerID).removeClass('actionLoading');

        if (currentActionSelectorController) {
            currentActionSelectorController.Hide();
        }

        currentActionDefinitionController.PluginReady(pluginController);
    };

    this.getActions = function () {
        return config.actions;
    };

    this.getViewContext = function () {
        return config.viewContext;
    };

    this.setViewContext = function (value) {
        config.viewContext = value;
    };

    // Constructor:

    var self = this;
    var currentActionDefinitionController = null;
    var currentActionSelectorController = null;
    var assignActionSelectorController = null;
    var actionsByEscalationLevel = null;

    function getRepeatInterval(minutes) {
        var interval = minutes;
        if (minutes % 60 == 0) {
            interval = minutes / 60;
        }

        if (minutes % 1440 == 0) {
            interval = minutes / 1440;
        }
        return interval;
    }

    function getRepeatIntervalType(minutes) {
        var intervalType = "@{R=Core.Strings.2;K=WEBJS_BV0_ALERT_REPEAT_MINUTES;E=js}";

        if (minutes === 1) {
            intervalType = "@{R=Core.Strings.2;K=WEBJS_BV0_ALERT_REPEAT_MINUTE;E=js}";
        }

        if (minutes % 60 == 0) {
            if (minutes / 60 == 1) {
                intervalType = "@{R=Core.Strings.2;K=WEBJS_BV0_ALERT_REPEAT_HOUR;E=js}";
            } else {
                intervalType = "@{R=Core.Strings.2;K=WEBJS_BV0_ALERT_REPEAT_HOURS;E=js}";
            }
        }

        if (minutes % 1440 == 0) {
            if (minutes / 1440 === 1) {
                intervalType = "@{R=Core.Strings.2;K=WEBJS_BV0_ALERT_REPEAT_DAY;E=js}";
            } else {
                intervalType = "@{R=Core.Strings.2;K=WEBJS_BV0_ALERT_REPEAT_DAYS;E=js}";
            }
        }

        return intervalType;
    }

    function getRepeatIntervalTypeShort(minutes) {
        var intervalType = "@{R=Core.Strings.2;K=WEBJS_BV0_ALERT_REPEAT_MIN;E=js}";

        if (minutes % 60 == 0) {
            intervalType = "@{R=Core.Strings.2;K=WEBJS_BV0_ALERT_REPEAT_HR;E=js}";
        }

        if (minutes % 1440 == 0) {
            intervalType = "@{R=Core.Strings.2;K=WEBJS_BV0_ALERT_REPEAT_DAY;E=js}";
        }

        return intervalType;
    }

    function escalationLevelsEnabled() {
        return config.enviromentType == 'Alerting' && config.enableEscalationLevels;
    }

    $(init);
};

SW.Core.Actions.ActionsEditorController.instances = [];

SW.Core.Actions.ActionsEditorController.saveInstance = function(instanceName, instance) {

    SW.Core.Actions.ActionsEditorController.instances[instanceName] = instance;
};

SW.Core.Actions.ActionsEditorController.getInstance = function(instanceName) {
    return SW.Core.Actions.ActionsEditorController.instances[instanceName];
};
