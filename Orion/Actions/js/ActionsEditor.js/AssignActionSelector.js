﻿﻿

/* Object is responsible for rendering the list of action to assign in grid.
 *
 * @param {Object} config 
 */
SW.Core.namespace("SW.Core.Actions").AssignActionSelector = function (config) {
    
    "use strict";

    var renderGroup = function(value, meta, record) {
        var disp;
        if (comboArray.getValue().indexOf('Enabled') > -1) {
            disp = String.format('@{R=Core.Strings;K=WEBJS_TM0_145; E=js}', renderStatus(value), record.data.Cnt);
        } else {
            if (value == '{empty}')
                value = '';

            if (!value && value != false || value == '') {
                value = "@{R=Core.Strings;K=StatusDesc_Unknown;E=js}";
            }
            if (value == "[All]") {
                value = "@{R=Core.Strings;K=WEBJS_SO0_31; E=js}";
            }
            disp = value + " (" + record.data.Cnt + ")";
        }
        disp = Ext.util.Format.htmlEncode(disp);
        return '<span ext:qtip="' + disp + '">' + disp + '</span>';
    };

    var renderStatus = function(status) {
        if (status == '[All]') return '@{R=Core.Strings;K=LIBCODE_TM0_15;E=js}';
        return stringToBoolean(status) ? '@{R=Core.Strings;K=WEBJS_TM0_144;E=js}' : '@{R=Core.Strings;K=LogLevel_off;E=js}';
    };
    
    var stringToBoolean = function(value) {
        switch (value.toString().toLowerCase()) {
            case "true": case "yes": case "1": return true;
            case "false": case "no": case "0": case null: return false;
            default: return Boolean(value);
        }
    };

    var actionSelectedClicked = function() {
        var selectedActions = actionsSelectionModel.getSelections();
        if (selectedActions && selectedActions.length > 0) {
            var actionsToAssign = $.map(selectedActions, function(element) {
                return element.data.ActionID;
            });
            var param = {
                ActionsToAssign: actionsToAssign
            };
            SW.Core.Services.callController("/api/Action/GetActionsByIds", param,
                function(selectedActionDefinitions) {
                    if (selectedActionDefinitions && selectedActionDefinitions.length > 0 && $.isFunction(config.onAssigned)) {
                        config.onAssigned(selectedActionDefinitions);
                    }
                    onClose();
                },
                function(msg) {
                    alert(msg);
                    onClose();
                }
            );
        }
    };

    var preselectActionPlugin = function() {
    };
    
    var onClose = function () {
        self.Hide();

        if ($.isFunction(config.onClose)) {
            config.onClose();
        }
        filterText = "";
    };

    var createAddActionSearch = function (containerID) {
        $('#' + containerID).html('<div id="searchField"></div>');
    };

    var createAddActionButtons = function(containerID) {
        var $containerID = '#' + containerID;

        $($containerID).css('text-align', 'right');

        $(SW.Core.Widgets.Button('@{R=Core.Strings;K=WEBJS_PS1_62;E=js}', { type: 'primary', id: 'configureActionBtn' }))
            .appendTo($containerID)
            .css('margin', '10px')
            .on('click', actionSelectedClicked);
        
         $(SW.Core.Widgets.Button('@{R=Core.Strings;K=WEBJS_ZT0_22;E=js}', { type: 'secondary', id: 'cancelAddActionBtn' }))
            .appendTo($containerID)
            .css('margin', '10px 0 10px 10px')
            .on('click',  onClose);
    };

function encodeHTML(value) {
        return Ext.util.Format.htmlEncode(value);
    }

    function escapeRegExp(str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }

    function renderString(value) {
        if (!value || value.length === 0)
            return value;

        if (!filterText || filterText.length === 0)
            return encodeHTML(value);

        var patternText = filterText;

        // replace any %'s with a *
        patternText = patternText.replace(/\%/g, "*");

        // replace multiple *'s with a single instance
        patternText = patternText.replace(/\*{2,}/g, "*");

        // check if the search string is now just down to a single *, and if so return without any further regex
        if (patternText == '*') {
            return '<span style=\"background-color: #FFE89E\">' + encodeHTML(value) + '</span>';
        }

        // replace \ with \\
        patternText = patternText.replace(/\\/g, '\\\\');
        // replace . with \.
        patternText = patternText.replace(/\./g, '\\.');
        // replace * with .*
        patternText = patternText.replace(/\*/g, '.*');

        // set regex pattern
        var x = '((' + escapeRegExp(patternText) + ')+)\*';
        var content = value, pattern = new RegExp(x, "gi"), replaceWith = '{SPAN-START-MARKER}$1{SPAN-END-MARKER}';

        // do regex replace + content gets encoded
        var fieldValue = encodeHTML(content.replace(pattern, replaceWith));

        // now replace the literal SPAN markers with the HTML span tags
        fieldValue = fieldValue.replace(/{SPAN-START-MARKER}/g, '<span style=\"background-color: #FFE89E\">');
        fieldValue = fieldValue.replace(/{SPAN-END-MARKER}/g, '</span>');

        return fieldValue;
    }

    var renderTitle = function(value, metadata, record) {
        var tipText = String.format("<b>{0}</b><br/><span style='word-wrap:break-word'>{1}</span>", Ext.util.Format.htmlEncode(value), Ext.util.Format.htmlEncode(record.data.Description));
        return '<span ext:qtip="' + Ext.util.Format.htmlEncode(tipText) + '">' + renderString(value) + '</span>';
    };
    
    var renderTooltip = function (value, metadata, record) {
        return '<span ext:qtip="' + Ext.util.Format.htmlEncode(value) + '">' + renderString(value) + '</span>';
    };
    
    var renderIcon = function(value, metadata, record) {
        if (value != '')
            return '<img src="' + value + '" alt="' + Ext.util.Format.htmlEncode(record.data.Title) + '"/>';
        return value;
    };
    
    var refreshGridObjects = function (start, limit, elem, environmentType, categoryType, property, type, value, search, callback) {
        var existingActions = $.map(config.actions, function(element) {
            return element.ID;
        });
        elem.store.removeAll();
        elem.store.proxy.conn.jsonData = { EnvironmentType: environmentType, CategoryType: categoryType, ExistingActions: existingActions, Property: property, Type: type, Value: value, FilterText:search };
        if (limit)
            elem.store.load({ params: { start: start, limit: limit }, callback: callback });
        else
            elem.store.load({ callback: callback });

        groupProperty = property;
        groupType = type;
        groupValue = value;
    };

    var createAssignActionGrid = function(containerID) {
        var getActionsEndpoint = "/api/Action/GetActions";
            var getActionsJson = [
                        { name: 'ActionID', mapping: 0 },
                        { name: 'ActionTypeID', mapping: 1 },
                        { name: 'Title', mapping: 2 },
						{ name: 'ActionTypeIDDisplay', mapping: 3 },
                        { name: 'IconPath', mapping: 4 },
                        { name: 'Description', mapping: 5 }
            ];
        
        actionsStore = new ORION.WebApiStore(getActionsEndpoint, getActionsJson);
        actionsStore.sortInfo = { field: 'Title', direction: 'Asc' };
        
        groupingValue = ',null'.split(',');
        grpGridFirstLoad = true;

        var groupingDataStore = new ORION.WebApiStore(
            "/api/Action/GetObjectGroupProperties",
            [
                { name: 'Value', mapping: 0 },
                { name: 'Name', mapping: 1 },
                { name: 'Type', mapping: 2 }
            ]);

        var groupingStore = new ORION.WebApiStore(
            "/api/Action/GetGroupValues",
            [
                { name: 'Value', mapping: 0 },
                { name: 'DisplayNamePlural', mapping: 1 },
                { name: 'Cnt', mapping: 2 }
            ],
            "Value", "");

        comboArray = new Ext.form.ComboBox(
            {
                id: 'actionSelectorGroupCombo',
                mode: 'local',
                boxMaxWidth: 500,
                fieldLabel: 'Name',
                displayField: 'Name',
                valueField: 'Value',
                store: groupingDataStore,
                triggerAction: 'all',
                value: '@{R=Core.Strings;K=WEBJS_VB0_76; E=js}',
                typeAhead: false,
                forceSelection: true,
                selectOnFocus: false,
                multiSelect: false,
                editable: false,
                listeners: {
                    'select': function() {
                        var val = this.store.data.items[this.selectedIndex].data.Value;
                        var type = this.store.data.items[this.selectedIndex].data.Type;
                        refreshGridObjects(null, null, groupGrid, config.enviromentType, config.categoryType, val, type, "", filterText);

                        if (val == '') {
                            refreshGridObjects(0, pageSize, actionsGrid, config.enviromentType, config.categoryType, "", "", "", filterText, function() {
                                $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function() {
                                    this.tooltip = 'processed';
                                    $.swtooltip(this);
                                });
                            });
                        }
                    },
                    'beforerender': function() {
                        refreshGridObjects(null, null, comboArray, config.enviromentType, config.categoryType, "", "", "", "");
                    }
                }
            });

        comboArray.getStore().on('load', function() {
            refreshGridObjects(null, null, groupGrid, config.enviromentType, config.categoryType, defaultGroupingValue, "", "", "");
            comboArray.setValue(defaultGroupingValue);
        });
        
        pageSizeBox = new Ext.form.NumberField({
                id: 'schPageSizeField',
                enableKeyEvents: true,
                allowNegative: false,
                width: 40,
                allowBlank: false,
                minValue: 1,
                maxValue: 100,
                value: pageSize,
                listeners: {
                    scope: this,
                    'keydown': function (f, e) {
                        var k = e.getKey();
                        if (k == e.RETURN) {
                            e.stopEvent();
                            var v = parseInt(f.getValue());
                            if (!isNaN(v) && v > 0 && v <= 100) {
                                pageSize = v;
                                actionPagingToolbar.pageSize = pageSize;
                                actionPagingToolbar.doLoad(0);
                            } else {
                                Ext.Msg.show({
                                    title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                    msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                    icon: Ext.Msg.ERROR,
                                    buttons: Ext.Msg.OK
                                });
                                return;
                            }
                        }
                    },
                    'focus': function (field) {
                        field.el.dom.select();
                    },
                    'change': function (f, numbox, o) {
                        var pSize = parseInt(f.getValue());
                        if (isNaN(pSize) || pSize < 1 || pSize > 100) {
                            Ext.Msg.show({
                                title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                icon: Ext.Msg.ERROR,
                                buttons: Ext.Msg.OK
                            });
                            return;
                        }

                        if (actionPagingToolbar.pageSize != pSize) { // update page size only if it is different
                            actionPagingToolbar.pageSize = pSize;
                            pageSize = pSize;
                            actionPagingToolbar.doLoad(0);
                        }
                    }
                }
            });
        
        actionPagingToolbar = new Ext.PagingToolbar(
                {
                    id: 'schGridPaging',
                    store: actionsStore,
                    pageSize: pageSize,
                    displayInfo: true,
                    displayMsg: "@{R=Core.Strings;K=WEBJS_TM0_5; E=js}",
                    emptyMsg: "@{R=Core.Strings;K=WEBJS_VL0_11; E=js}",
                    beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                    afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                    firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                    prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                    nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                    lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                    refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}",
                    items: [
                        '-',
                        new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_VB0_46; E=js}', style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;' }),
                        pageSizeBox
                    ]
                }
            );

        actionsGrid = new Ext.grid.GridPanel({
            region: 'center',
            id: 'actionsSelectorGrid',
            viewConfig: { forceFit: false, emptyText: "@{R=Core.Strings;K=WEBJS_VL0_11; E=js}" },
            store: actionsStore,
            stripeRows: true,
            trackMouseOver: false,
            autoScroll: true,
            columns: [
                actionsSelectionModel,
                { id: 'ActionID', dataIndex: 'ActionID', hidden: true}, 
                { id: 'IconPath', header: '' , dataIndex: 'IconPath', sortable: false, width: 25,  renderer: renderIcon },
                { id: 'Title', header: '@{R=Core.Strings;K=WEBJS_ZT0_23;E=js}' , dataIndex: 'Title', sortable: true, width: 510, renderer: renderTitle },
                { id: 'ActionTypeID', header: '@{R=Core.Strings;K=FILEDATA_IB0_7;E=js}', dataIndex: 'ActionTypeIDDisplay', sortable: true, width: 210, renderer: renderTooltip},
            ],
            sm: actionsSelectionModel,
            enableHdMenu: false,
            enableColumnResize: true,
            enableColumnMove: false,
            height: 300,
            listeners: {
                'viewready': function() {
                    setTimeout(preselectActionPlugin, 0);
                }
            },
            bbar: actionPagingToolbar,
            cls: 'panel-with-border'
       
        });
        
        groupGrid = new Ext.grid.GridPanel({
                id: 'groupingGrid',
                store: groupingStore,
                cls: 'hide-header',
                columns: [
                    { id: 'Value', width: 193, editable: false, sortable: false, dataIndex: 'DisplayNamePlural', renderer: renderGroup }
                ],
                selModel: new Ext.grid.RowSelectionModel(),
                autoScroll: true,
                heigth: 250,
                loadMask: true,
                listeners: {
                    cellclick: function(mygrid, row, cell, e) {
                        var val = mygrid.store.data.items[row].data[mygrid.store.data.items[row].fields.keys[cell]];
                        var type = "";
                        $.each(comboArray.store.data.items, function(index, item) {
                            if (item.data.Value == comboArray.getValue()) {
                                type = item.data.Type;
                                return false;
                            }
                            return false;
                        });
                        if (val == '') {
                            val = '{empty}';
                        }

                        if (val == null) {
                            val = "null";
                        }

                        if (val == '[All]')
                            val = '';
                        var newGroupingValues = comboArray.getValue() + ',' + val;
                        groupingValue = newGroupingValues.split(',');

                        refreshGridObjects(0, pageSize, actionsGrid, config.enviromentType, config.categoryType, comboArray.getValue(), type, val, filterText, function() {
                            $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function() {
                                this.tooltip = 'processed';
                                $.swtooltip(this);
                            });
                        });
                    }
                },
                anchor: '0 0', viewConfig: { forceFit: true }, split: false, autoExpandColumn: 'Value'
            });

        groupGrid.store.on('load', function() {
            if (grpGridFirstLoad) {
                groupGrid.setHeight(270);
                var selectedVal = groupGrid.getStore().find("Value", groupingValue[1]);
                if (selectedVal == -1) {
                    groupingValue[1] = "";
                    refreshGridObjects(0, pageSize, actionsGrid, config.enviromentType, config.categoryType, groupingValue[0], "", groupingValue[1], filterText);
                    selectedVal = 0;
                }
                groupGrid.getSelectionModel().selectRow(selectedVal, false);
                grpGridFirstLoad = false;
            }
        });

        var groupByTopPanel = new Ext.Panel({
                id: 'groupByTopPanel',
                region: 'center',
                split: false,
                heigth: 50,
                width: 200,
                collapsible: false,
                viewConfig: { forceFit: true },
                items: [new Ext.form.Label({ text: "@{R=Core.Strings;K=WEBJS_AK0_76; E=js}" }), comboArray],
                cls: 'panel-no-border panel-bg-gradient'
            });

        var navPanel = new Ext.Panel({
                id: 'navPanel',
                region: 'west',
                width: 200,
                heigth: 300,
                split: false,
                anchor: '0 0',
                collapsible: false,
                viewConfig: { forceFit: true },
                items: [groupByTopPanel, groupGrid],
                cls: 'panel-with-border'
            });
        
        mainGridPanel = new Ext.Panel({ id: 'mainGrid', region: 'center', height: 320, width: 1000, split: false, layout: 'border', collapsible: false, items: [navPanel, actionsGrid], cls: 'panel-no-border' });
        mainGridPanel.render(containerID);

        var initialSearchText = "@{R=Core.Strings.2;K=WEBJS_AY0_18;E=js}";

        var searchField = new Ext.Panel({
            id: 'searchFieldPanel',
            region: 'center',
            split: false,
            heigth: 50,
            collapsible: false,
            layout: 'table',
            layoutConfig: {
                columns: 3
            },
            items: [
                new Ext.form.Label({ text: "@{R=Core.Strings.2;K=WEBJS_YK0_16;E=js}", width: 718, style: 'float: left; font-weight: bold; padding: 9px 16px 5px 0px;' }),
                new Ext.ux.form.SearchField({
                    id: 'alertsSearchField',
                    guid: 'assign-actions',
                    emptyText: initialSearchText,
                    store: actionsStore,
                    width: 280,
                    //Override onTrigger2Click and onTrigger1Click. Needs for correct search in DataTable Rows
                    onTrigger1Click: function () {
                        if (this.hasSearch) {
                            this.el.dom.value = '';
                            filterText = this.getRawValue();

                            var existingActions = $.map(config.actions, function (element) { return element.ID; });
                            this.store.proxy.conn.jsonData = { EnvironmentType: config.enviromentType, CategoryType: config.categoryType, ExistingActions: existingActions, Property: groupProperty, Type: groupType, Value: groupValue, FilterText: '' };
                            this.fireEvent('searchStarting', this.store.proxy.conn.jsonData);

                            var o = { start: 0, limit: parseInt(ORION.Prefs.load('PageSize', '20')) };
                            this.store.load({ params: o });

                            this.triggers[0].hide();
                            this.hasSearch = false;
                            this.fireEvent('searchStarted', filterText);
                        }
                    },
                    //  this is the search icon button in the search field
                    onTrigger2Click: function () {
                        //set the global variable for the SQL query and regex highlighting
                        filterText = this.getRawValue();

                        if (filterText.length < 1) {
                            this.onTrigger1Click();
                            return;
                        }

                        var tmpfilterText = "";
                        tmpfilterText = filterText;

                        if (tmpfilterText == "@{R=Core.Strings;K=WEBJS_TM0_53;E=js}") {
                            tmpfilterText = "";
                        }

                        if (tmpfilterText.length > 0) {
                            if (!tmpfilterText.match("^%"))
                                tmpfilterText = "%" + tmpfilterText;

                            if (!tmpfilterText.match("%$"))
                                tmpfilterText = tmpfilterText + "%";
                        }

                        var existingActions = $.map(config.actions, function (element) { return element.ID; });

                        // provide search string for Id param when loading the page       
                        this.store.proxy.conn.jsonData = { EnvironmentType: config.enviromentType, CategoryType: config.categoryType, ExistingActions: existingActions, Property: groupProperty, Type: groupType, Value: groupValue, FilterText: filterText };
                        var o = { start: 0, limit: parseInt(ORION.Prefs.load('PageSize', '20')) };
                        this.store.load({ params: o });

                        this.hasSearch = true;
                        this.triggers[0].show();

                        this.fireEvent('searchStarted', filterText);
                    }
                })
            ],
            cls: 'panel-no-border'
        });
        searchField.render('searchField');
        refreshGridObjects(0, pageSize, actionsGrid, config.enviromentType, config.categoryType, groupingValue[0], "", groupingValue[1], filterText);
    };

    // Public methods:

    this.ShowInDialog = function () {
        filterText = "";
        var $container = $('#' + config.containerID);
        
        $container.empty().append("<div class='actionPluginSelector' id='_assignActionSelectorDialog_' />");        

        $dialogElement = $('#_assignActionSelectorDialog_', $container);

        $dialogElement.html($("<div id='_assignActionSelectorSearch_' /> <div id='_assignActionSelectorGrid_' /> <div id='_assignActionSelectorButtons_' />"));

        createAddActionSearch('_assignActionSelectorSearch_');
        createAssignActionGrid('_assignActionSelectorGrid_');
        createAddActionButtons('_assignActionSelectorButtons_');

        $(window).resize(function() {
            mainGridPanel.doLayout();
        });

        $dialog = $dialogElement.dialog({
            modal: true,
            position: ['middle', 100],
            title: '@{R=Core.Strings.2;K=WEBJS_YK0_17;E=js}',
            width: 1025,
            heigh: 'auto',
            minWidth : 1025,
            close: onClose
        });

    };

    this.ShowLoading = function() {
        $dialogElement.addClass('actionLoading');
    };

    this.Hide = function () {    
        $dialog.dialog('destroy');
        $('#_assignActionSelectorDialog_').remove();
    };

    // Constructor:
    var self = this;
    var $dialogElement = null;
    var $dialog = null;
    var actionsSelectionModel = new Ext.grid.CheckboxSelectionModel();
    var actionsStore = null;
    var actionsGrid = null;
    var pageSize = 10;
    var pageSizeBox;
    var actionPagingToolbar;
    var mainGridPanel;
    var groupingValue;
    var comboArray;
    var groupGrid;
    var grpGridFirstLoad;
    var groupProperty;
    var groupType;
    var groupValue;
    var defaultGroupingValue = "ActionTypeID";
    
     Ext.QuickTips.init();

    Ext.apply(Ext.QuickTips.getQuickTip(), {
        showDelay: 0,
        trackMouse: false
    });
};
