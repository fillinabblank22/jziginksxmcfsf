﻿/* Object is responsible for rendering action plugin and manage creating of action definition.
 *
 * @param {Object} config 
 */
SW.Core.namespace("SW.Core.Actions").ActionDefinitionController = function (config) {
    "use strict";

    // Private methods
    var getParameter = function (name) {
        
        var part = window.location.search.split(name + "=")[1];
        if (typeof part !== "undefined") {
            return decodeURIComponent(part.split("&")[0]);
        }
        return undefined;
    }
	
    var getCurrentSectionID = function () {
        return $(sections[currentSectionIndex]).attr('id');
    };

    var isLastSection = function () {
        // check if this section is last required
        var requiredSectionsCount = $(sections.slice(currentSectionIndex)).filter(".required").length - 1;
        return requiredSectionsCount > 0 ? false : true;
    };

    var showSection = function (sectionIndex) {
        $accordion.accordion('activate', sectionIndex);
    };

    var createButtons = function () {
        var $buttonsContainer = $('#_actionPluginButtons_', $container);

        $($buttonsContainer).css('text-align', 'right');

        $smallLoading = $("<span class='actionSmallLoading' />").appendTo($buttonsContainer);

        $nextSectionBtn = $(SW.Core.Widgets.Button('@{R=Core.Strings;K=WEBJS_ZT0_26;E=js}', { type: 'primary', id: 'nextSectionInActionDefinitionBtn' }))
             .appendTo($buttonsContainer)
             .css('margin', '10px')
             .click(onNextSectionClicked);

        $createActionBtn = $(SW.Core.Widgets.Button('@{R=Core.Strings.2;K=WEBJS_OM0_01;E=js}', { type: 'primary', id: 'createActionDefinitionBtn' }))
             .appendTo($buttonsContainer)
             .css('margin', '10px')
             .click(onCreateActionClicked);

        $addActionBtn = $(SW.Core.Widgets.Button('@{R=Core.Strings.2;K=WEBJS_OM0_10;E=js}', { type: 'primary', id: 'addActionDefinitionBtn' }))
             .appendTo($buttonsContainer)
             .css('margin', '10px')
             .click(onCreateActionClicked);

        $cancelActionBtn = $(SW.Core.Widgets.Button('@{R=Core.Strings;K=WEBJS_ZT0_22;E=js}', { type: 'secondary', id: 'cancelActionDefinitionBtn' }))
             .appendTo($buttonsContainer)
             .css('margin', '10px 0 10px 10px')
             .on('click', onClose);

        setButtonsVisibility();
    };

    var disableCreateAndNextBtn = function () {
        $createActionBtn.addClass('sw-btn-disabled');
        $nextSectionBtn.addClass('sw-btn-disabled');
        $addActionBtn.addClass('sw-btn-disabled');
    };

    var enableCreateAndNextBtn = function () {
        if ($("#isOrionDemoMode").length == 0) {
            $createActionBtn.removeClass('sw-btn-disabled');
        }
        $nextSectionBtn.removeClass('sw-btn-disabled');
        $addActionBtn.removeClass('sw-btn-disabled');
    };

    var isDisabled = function (btn) {
        return btn.hasClass('sw-btn-disabled');
    };

    var setButtonsVisibility = function () {
        // If we are on the last section or we are in edit mode we always show CREATE ACTION button.
        if (isLastSection() && editMode) {
            $nextSectionBtn.hide();
            $createActionBtn.show();
            $addActionBtn.hide();
            // otherwise we show NEXT button.
        }
        else if (isLastSection() && !editMode) {
            $addActionBtn.show();
            $nextSectionBtn.hide();
            $createActionBtn.hide();
        } else {
            $nextSectionBtn.show();
            $createActionBtn.hide();
            $addActionBtn.hide();
        }

        if ($("#isOrionDemoMode").length != 0) {
            $createActionBtn.addClass('sw-btn-disabled');
            $addActionBtn.addClass('sw-btn-disabled');
        }
    };

    var validateSectionsAsyncRecursive = function (sectionIndexToValidate) {

        // We have validated all sections succesfully.
        if (sectionIndexToValidate === sections.length - 1) {
            if (SW.Core.Actions.ExecutionSettings != undefined && !SW.Core.Actions.ExecutionSettings.validate()) {
                showSection(sections.length - 1);
            } else {
                currentPluginController.validateSectionAsync($(sections[sectionIndexToValidate]).attr('id'), function (isValid) {
                    currentSectionIndex = sectionIndexToValidate;
                    hideShowInvalidMessage(isValid);
                    if (!isValid) {
                        showSection(sectionIndexToValidate);
                    } else {
                        collectActionInfo();
                    }
                });
            }
            enableCreateAndNextBtn();
        } else {

            (function (sectionIndex) {
                var sId = $(sections[sectionIndex]).attr('id');
                currentPluginController.validateSectionAsync(sId, function (isValid) {
                    currentSectionIndex = sectionIndex;
                    hideShowInvalidMessage(isValid);
                    if (isValid) {
                        // Validate next section.
                        validateSectionsAsyncRecursive(++sectionIndex);
                    } else {
                        // Not valid, we will show sections with errors.

                        enableCreateAndNextBtn();

                        showSection(sectionIndex);
                    }
                });
            })(sectionIndexToValidate);
        }
    };

    var onActionDefinitionCreated = function (result) {
        if (!result.isError && $.isFunction(config.onCreated)) {
            result.actionDefinition.IconPath = config.plugin.IconPath;
            self.Hide();
            config.onCreated(result.actionDefinition);

        } else if ($.isFunction(config.onError)) {

            config.onError(result.errorMsg);
        }
    };
    var onPropertiesUpdated = function (result) {
        if (!result.isError && $.isFunction(config.onCreated)) {

            self.Hide();
            config.onCreated(result);

        } else if ($.isFunction(config.onError)) {

            config.onError("");
        }
    };
    var collectActionInfo = function () {
        if (jQuery.isFunction(currentPluginController.getUpdatedActionProperies) && (config.mutiEditMode == true)) {
            currentPluginController.getUpdatedActionProperies(function (result) {
                onPropertiesUpdated(result);
            });
        } else {
            currentPluginController.getActionDefinitionAsync(function (result) {
                onActionDefinitionCreated(result);
            });
        }
    };

    var validationCallback = function (isValid) {
        hideShowInvalidMessage(isValid);
        if (isValid) {
            collectActionInfo();
        }
        enableCreateAndNextBtn();
    };

    var onCreateActionClicked = function () {
        if (isDisabled($createActionBtn) || isDisabled($addActionBtn)) {
            return;
        }
        disableCreateAndNextBtn();

        // User jumped over some sections we need to validate all sections, otherwise we validate only current sections. 
        if (validateAllSectionsBeforeSave) {
            validateSectionsAsyncRecursive(0);
        } else {
            if (SW.Core.Actions.ExecutionSettings != undefined) {
                if (getCurrentSectionID() === executionSettingsPluginId) {
                    validationCallback(SW.Core.Actions.ExecutionSettings.validate());
                    return;
                }
            }
            currentPluginController.validateSectionAsync(getCurrentSectionID(), validationCallback);
        }
    };

    var onNextSectionClicked = function () {
        if (isDisabled($nextSectionBtn)) {
            return;
        }
        disableCreateAndNextBtn();
        if (SW.Core.Actions.ExecutionSettings != undefined) {
            if (getCurrentSectionID() === executionSettingsPluginId) {
                validationCallback(SW.Core.Actions.ExecutionSettings.validate());
                return;
            }
        }

        if (SW.Core.Actions.TimeOfDaySchedule != undefined) {
            if (getCurrentSectionID() === timeOfDayPluginId) {
                currentSectionIndex++;
                setButtonsVisibility();
                showSection(currentSectionIndex);
                enableCreateAndNextBtn();
                return;
            }
        }

        currentPluginController.validateSectionAsync(getCurrentSectionID(), function (isValid) {
            if (isValid) {
                var annotation = "";
                if (jQuery.isFunction(currentPluginController.getSectionAnnotation)) {
                    annotation = currentPluginController.getSectionAnnotation(getCurrentSectionID());
                }
                $("#" + getCurrentSectionID()).prev("h3").find(".contentInfo").text(annotation).css('color', '#646464').show();
                currentSectionIndex++;
                setButtonsVisibility();
                showSection(currentSectionIndex);
            }
            enableCreateAndNextBtn();
        });
    };

    var onAccordionHeaderClicked = function () {
        var clickedSectionIndex = $('h3.ui-accordion-header', $container).index(this);

        // User click on current section.
        if (currentSectionIndex === clickedSectionIndex) {
            return;
        } else if ((currentSectionIndex + 1) < clickedSectionIndex) {
            // User jumps over some sections, we will validate all sections before saving.
            validateAllSectionsBeforeSave = true;
        }
        disableCreateAndNextBtn();
        currentPluginController.validateSectionAsync(getCurrentSectionID(), function (isValid) {
            hideShowInvalidMessage(isValid);
            currentSectionIndex = clickedSectionIndex;
            setButtonsVisibility();
            enableCreateAndNextBtn();
            showSection(currentSectionIndex);
        });
    };

    var hideShowInvalidMessage = function (isValid) {
        if (isValid && sections.length > 1) {
            var annotation = "";
            var sectionID = getCurrentSectionID();
            if (sectionID == 'actionTimeOfDayContainer') {
                annotation = SW.Core.Actions.TimeOfDaySchedule.getSectionAnnotation(sectionID);
                $("#actionTimeOfDayContainer h3 .contentInfo").text(annotation);
            } else if (sectionID == 'ExecutionSettingsPlugin') {
                annotation = SW.Core.Actions.ExecutionSettings.getSectionAnnotation();
                $("#ExecutionSettingsPlugin h3 .contentInfo").text(annotation);
            } else if (jQuery.isFunction(currentPluginController.getSectionAnnotation)) {
                annotation = currentPluginController.getSectionAnnotation(sectionID);
                $("#" + getCurrentSectionID()).prev("h3").find(".contentInfo").text(annotation).css('color', '#646464');
            }
        } else {
            if ($("#" + getCurrentSectionID()).hasClass("required")) {
                validateAllSectionsBeforeSave = true;
                $("#" + getCurrentSectionID()).prev("h3").find(".contentInfo").text("@{R=Core.Strings;K=WEBJS_AB0_59;E=js}").css('color', 'red');
            }
        }
    };

    var onClose = function () {
        self.Hide();

        if ($.isFunction(config.onClose)) {
            config.onClose();
        }
    };

    // Public methods:

    /// <summary>Method is called by plugin controller.</summary>
    /// <param name="pluginController" type="IPluginController">Reference to plugin controller.</param>
    this.PluginReady = function (pluginController) {
        currentPluginController = pluginController;

        if (config.mutiEditMode) {
            SW.Core.MacroVariablePickerController.DisableInsertVariableButtons();
        } else {
            SW.Core.MacroVariablePickerController.EnableInsertVariableButtons();
        }

        currentSectionIndex = 0;

        sections = $('.section', $container);

        if (sections.length == 0) {
            throw "Every action plugin UI has to have at least one section.";
        }

        if (sections.length > 1) {

            $accordion = $container.accordion({
                header: "h3",
                change: function (event, ui) {
                    var headers = $('.ui-accordion-header', $container);
                    headers.addClass('ui-accordion-inactive');
                    headers.removeClass('ui-accordion-active');
                    ui.newHeader.addClass('ui-accordion-active');
                    ui.newHeader.removeClass('ui-accordion-inactive');

                },
                active: false,
                autoHeight: false,
                clearStyle: true,
                animated: false,
                event: '' // disable default header click.
            });

            showSection(currentSectionIndex);

            // Creates sections annotations holders.
            $.each(sections, function () {
                var that = $(this);
                if (sections.length > 1) {
                    if (that.hasClass("required") && !editMode)
                        that.prev("h3").append("<span class='contentInfo'>" + "@{R=Core.Strings.2;K=WEBDATA_BV0_0049;E=js}" + "</span>");
                    else {
                        if (that.attr("id") == getCurrentSectionID()) {
                            that.prev("h3").append("<span class='contentInfo'></span>");
                        } else {
                            var annotation = "";
                            if (that.attr("id") == 'ExecutionSettingsPlugin') {
                                annotation = SW.Core.Actions.ExecutionSettings.getSectionAnnotation();
                                $("#ExecutionSettingsPlugin h3 .contentInfo").text(annotation);
                            } else if (jQuery.isFunction(currentPluginController.getSectionAnnotation)) {
                                annotation = currentPluginController.getSectionAnnotation(that.attr("id"));
                                that.prev("h3").append("<span class='contentInfo'>" + annotation + "</span>");
                            }
                        }
                    }
                }
            });

            validateAllSectionsBeforeSave = editMode;

            $container.find('h3').click(onAccordionHeaderClicked);						
        }
        // Disable the loading mask
        loadingMask.hide();

        createButtons();
        if (config.actionDefinition && (config.actionDefinition.IsShared || config.anyshared)) {
          
            var param = {
                ActionDefinitionIds:  config.mutiEditMode ? config.editedActionIds : [config.actionDefinition.ID],
                EnvironmentType: config.enviromentType 
            };

            //Need for correct count displaying of assign alerts to actions) except displayed alerts name
            var subtractFromAlertsCount = 2;

            SW.Core.Services.callController("/api/Action/GetActionAssigments", param,
             function (actionAssigments) {
                 var message;
                 if (actionAssigments.TotalRows === 1) {
                     message = String.format("@{R=Core.Strings.2;K=WEBJS_AY0_24;E=js}", config.enviromentType == "Alerting" ? "@{R=Core.Strings.2;K=WEBJS_AY0_25;E=js}" : "@{R=Core.Strings.2;K=WEBJS_AY0_26;E=js}", actionAssigments.DataTable.Rows[0][1]);
                 }
                 else if (actionAssigments.TotalRows === 2) {
                     message = String.format("@{R=Core.Strings.2;K=WEBJS_AY0_22;E=js}", config.enviromentType == "Alerting" ? "@{R=Core.Strings.2;K=WEBJS_YK0_19;E=js}" : "@{R=Core.Strings.2;K=WEBJS_YK0_20;E=js}", actionAssigments.DataTable.Rows[0][1], actionAssigments.DataTable.Rows[1][1]);
                 } else {
                     message = String.format("@{R=Core.Strings.2;K=WEBJS_AY0_23;E=js}", config.enviromentType == "Alerting" ? "@{R=Core.Strings.2;K=WEBJS_YK0_19;E=js}" : "@{R=Core.Strings.2;K=WEBJS_YK0_20;E=js}", actionAssigments.DataTable.Rows[0][1], actionAssigments.DataTable.Rows[1][1], actionAssigments.TotalRows-subtractFromAlertsCount);
                 }
                 $container.find("#_topActionPlugins_").prepend(String.format("<div style='font-size: 14px;background-color: #fff7cd;padding: 3px; width: 629px;' id='isSharedWarning'><img alt='fd' src='/Orion/images/Icon.Lightbulb.gif'><b>{0}</b></div>",
                    message));
             },
             function (msg) {
                 alert(msg);
             }
         );

        }
        $dialog = $container.dialog({
            dialogClass: "action-plugin-definition-dialog",
            modal: true,
            position: ['middle', 100],
            height: 'auto',
            width: 'auto',
            minWidth: 600,
            title: config.mutiEditMode == true ? String.format('@{R=Core.Strings;K=WEBJS_SO0_106;E=js}', config.plugin.Title) : String.format("@{R=Core.Strings.2;K=WEBJS_OM0_9;E=js}", config.plugin.Title),

            close: onClose
        });
        $actionName.removeClass("hidden");
        if (config.enviromentType == 'Alerting') {
            if ($execSettings) {
                $execSettings.removeClass("hidden");
            }

            $timeOfDay.removeClass("hidden");
            SW.Core.Actions.TimeOfDaySchedule.init();
        }
    };

    this.LoadPlugin = function (indexes, showMask) {
		// Disable controls with loading mask
		if (showMask == true) {
			loadingMask.show();
		} 
		
        $container = $('#' + config.containerID);
        
        var html = "<div class='actionPluginDefinition'>";
        if (config.mutiEditMode == true)
            html += ("<div style='padding: 5px 0 9px 0; font-weight: bold;'>" + "@{R=Core.Strings;K=WEBJS_SO0_105;E=js}" + "</div>");

        html += ("<div id='_topActionPlugins_' />");
        html += ("<div id='_actionPluginHolder_' class='actionPluginHolder' />");
        html += ("<div id='_timeOfDayActionPlugins_' />");
        html += ("<div id='_basicActionPlugins_' />");
        html += ("<div id='_actionPluginButtons_' />");
        html += ("</div>");

        var actionLayout = $container.accordion('destroy').empty().append(html);
		var parameters = {Control: config.plugin.ViewPath};
        var alertWizardGuid = getParameter("AlertWizardGuid");
		if (typeof alertWizardGuid !== "undefined") {   
			parameters.AlertWizardGuid = alertWizardGuid;
		}
		
        SW.Core.Loader.Control('#_actionPluginHolder_', parameters,
		{
            'config': {
                'EnviromentType': config.enviromentType,
                'OnReadyJsCallback': config.onPluginReadyFunctionName,
                'ViewContextJsonString': JSON.stringify(config.viewContext),
                'ActionDefinitionJsonString': JSON.stringify(config.actionDefinition),
                'MultiEditEnabled': (config.mutiEditMode == true),
                'ActionIds': indexes
            },
            'IgnoreResourceID': true
        }, 'replace', function () {
            onActionDefinitionCreated({ isError: true, errorMsg: "Can't load action edit form. Please select another one." });
        });

        if (config.enviromentType == "Alerting" && config.enableExecutionSettings != false) {

            $execSettings = $("#" + executionSettingsPluginId).clone();
            var containerExecutionSettings = actionLayout.find("#_basicActionPlugins_").append($execSettings);
            SW.Core.Actions.ExecutionSettings = new SW.Core.Actions.ExecutionSettingsController(
            {
                containerID: containerExecutionSettings,
                IsMultiEdit: (config.mutiEditMode == true),
                ActionDefinition: config.actionDefinition
            });
            SW.Core.Actions.ExecutionSettings.init();
        }
        $actionName = $("#ActionNamePlugin").clone();
        var containerActionName = actionLayout.find("#_topActionPlugins_").append($actionName);
        SW.Core.Actions.ActionName = new SW.Core.Actions.ActionNameController({
            containerID: containerActionName,
            ActionDefinition: config.actionDefinition,
            IsMultiEdit: (config.mutiEditMode == true)
        });
        SW.Core.Actions.ActionName.init();
        if (config.enviromentType == "Alerting") {
            var frequencies = [];
            if (config.actionDefinition != undefined && config.actionDefinition.TimePeriods) {
                frequencies = config.actionDefinition.TimePeriods;
            }
            $timeOfDay = $("#actionTimeOfDayContainer").clone();
            var containerTimeOfDay = actionLayout.find("#_timeOfDayActionPlugins_").append($timeOfDay);
            SW.Core.Actions.TimeOfDaySchedule = new SW.Core.Actions.TimeOfDayScheduleController({
                container: $timeOfDay,
                count: frequencies.length,
                frequencyControlContainer: containerTimeOfDay,
                useTimePeriodMode: config.enviromentType == "Alerting",
                singleScheduleMode: false,
                frequencies: frequencies,
                mutiEditMode: config.mutiEditMode
            });
        }
    };

    this.Hide = function () {
        $dialog.dialog('destroy');
        $container.accordion('destroy');

        // Remove a mess from dialog.
        $container.attr('style', '');
    };

    // Constructor:

    var self = this;
    var currentPluginController = null;
    var $container = null;
    var $dialog = null;
    var currentSectionIndex = 0;
    var $accordion = null;
    var editMode = !!config.actionDefinition;
    var $nextSectionBtn = null;
    var $createActionBtn = null;
    var $addActionBtn = null;
    var $cancelActionBtn = null;
    var $smallLoading = null;
    var sections = null;
    var validateAllSectionsBeforeSave = false;

    var $actionName = null;
    var $execSettings = null;
    var $timeOfDay = null;

    var executionSettingsPluginId = 'ExecutionSettingsPlugin';
    var timeOfDayPluginId = 'actionTimeOfDayContainer';
	
	var loadingMask = new Ext.LoadMask(Ext.getBody(), { msg : "@{R=Core.Strings.2;K=Alerting_ActionEdit_LoadingMaskText;E=js}" });
};
