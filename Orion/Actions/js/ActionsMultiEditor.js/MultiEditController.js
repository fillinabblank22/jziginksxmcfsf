﻿SW.Core.namespace("SW.Core.Actions").MultiEditController = function (config) {
    "use strict";
    var loadingMask = new Ext.LoadMask(Ext.getBody(), { msg: "@{R=Core.Strings.2;K=Alerting_ActionEdit_LoadingMaskText;E=js}" });
    var EnvType = "Alerting";
    var onPluginReadyFunctionName = "SW.Core.Actions.MultiEditController.instance.pluginReady";
    var currentActionDefinitionController;
    var editedActionsIds;
    var dialogContainerID = config.dialogContainerId;
    var showExecutionSettings = config.enableExecutionSettings;
    var onEditEndCallback;
    var showDialogContainer = function () {
        $("#" + dialogContainerID).removeClass('actionsOffPage');
    };

    var getMultiActions = function (indexes) {
        ORION.callWebService("/Orion/Services/ActionManager.asmx",
            "GetActionDefinition", { id: indexes[0] },
            function (result) {
                editedActionsIds = indexes;
                getMultiActionDefinitionController(JSON.parse(result));
                currentActionDefinitionController.LoadPlugin(indexes);
            });
    };
    var setMacroContexts = function (actionId) {
        ORION.callWebService("/Orion/Services/ActionManager.asmx",
            "GetMacroContexts", { actionId: actionId },
            function (result) {
                SW.Core.MacroVariablePickerController.SetMacroContexts(JSON.parse(result));
            });
    };
    var getActionByIndex = function (index) {
        ORION.callWebService("/Orion/Services/ActionManager.asmx",
            "GetActionDefinition", { id: index },
            function (result) {
                getActionDefinitionController(JSON.parse(result));
                currentActionDefinitionController.LoadPlugin();
                setMacroContexts(index);
            });
    };
    var saveEditedAction = function (actionDefinition) {
        if (showExecutionSettings != false) {
            actionDefinition.ActionProperties = actionDefinition.ActionProperties.concat(SW.Core.Actions.ExecutionSettings.getProperties());
        }
        var title = SW.Core.Actions.ActionName.getActionName();
        if (title != null && title != '') {
            actionDefinition.Title = title;
        }
        actionDefinition.TimePeriods = SW.Orion.FrequencyControllerDefinition.getFrequency();

        ORION.callWebService("/Orion/Services/ActionManager.asmx",
            "UpdateAction", {
                actionDefinition: JSON.stringify(actionDefinition)
            }, function () {
                if (onEditEndCallback) {
                    onEditEndCallback();
                }
            });
    };
    var updateActionProperties = function (actionProperties) {
        var propertiesArray = JSON.parse(actionProperties);
        var unionArray = propertiesArray;
        if (showExecutionSettings == true) {
            unionArray = unionArray.concat(SW.Core.Actions.ExecutionSettings.getProperties());
        }
        actionProperties = JSON.stringify(unionArray);
        var title = SW.Core.Actions.ActionName.getActionName();
        if (title != null && title != '') {
            updateActionsTitle(title, editedActionsIds);
        }
        var frequencies = SW.Orion.FrequencyControllerDefinition.getFrequency();
        if (SW.Core.Actions.TimeOfDaySchedule.isTimeOfDayScheduleModified()) {
            updateActionsFrequencies(JSON.stringify(frequencies), editedActionsIds);
        }
        if (JSON.parse(actionProperties).length > 0) {
            ORION.callWebService("/Orion/Services/ActionManager.asmx",
                "UpdateActionsProperties", {
                    actionsProperties: actionProperties,
                    actionsIds: editedActionsIds
                }, function () {
                    if (onEditEndCallback) {
                        onEditEndCallback();
                    }
                });
        }
    };

    var updateActionsTitle = function (actionsTitle, actionsId) {
        ORION.callWebService("/Orion/Services/ActionManager.asmx",
            "UpdateActionsTitle", {
                actionsTitle: actionsTitle,
                actionsIds: actionsId
            }, function () {
                if (onEditEndCallback) {
                    onEditEndCallback();
                }
            });
        };

        var updateActionsFrequencies = function (frequencies, actionsIds) {
        ORION.callWebService("/Orion/Services/ActionManager.asmx",
            "UpdateActionsFrequencies", {
                timePeriods: frequencies,
                actionsIds: actionsIds
            }, function() {
                if (onEditEndCallback) {
                    onEditEndCallback();
                }
            });
    };

    var getMultiActionDefinitionController = function (action) {
        // Disable controls with loading mask
        loadingMask.show();

        var actionPlugin = _.find(JSON.parse($("#plugins").val()), function (item) {
            return item.ActionTypeID == action.ActionTypeID;
        });
        currentActionDefinitionController = new SW.Core.Actions.ActionDefinitionController({
            containerID: dialogContainerID,
            plugin: actionPlugin,
            viewContext: {},
            onPluginReadyFunctionName: onPluginReadyFunctionName,
            enviromentType: EnvType,
            onCreated: actionPropertiesUpdated,
            onError: handleActionDefinitionError,
            actionDefinition: {},
            onClose: hideDialogContainer,
            mutiEditMode: true,
            enableExecutionSettings: showExecutionSettings,
            anyshared: config.anyshared,
            editedActionIds: editedActionsIds
        });
    };

    var fillEscalationLevel = function(originAction, newAction) {
        if ($("#escalationLevelPropertyName").length > 0) {
            var propertyName = $("#escalationLevelPropertyName").val();
            var propertyExists = false;
            for (var i = 0; i < newAction.ActionProperties.length; i++) {
                if (newAction.ActionProperties[i].PropertyName == propertyName) {
                    propertyExists = true;
                    break;
                }
            }

            if (!propertyExists) {
                for (var j = 0; j < originAction.ActionProperties.length; j++) {
                    if (originAction.ActionProperties[j].PropertyName == propertyName) {
                        newAction.ActionProperties.push(originAction.ActionProperties[j]);
                        break;
                    }
                }
            }
        }
    };

    var getActionDefinitionController = function (action) {
        // Disable controls with loading mask
        loadingMask.show();

        var actionPlugin = _.find(JSON.parse($("#plugins").val()), function (item) {
            return item.ActionTypeID == action.ActionTypeID;
        });
        currentActionDefinitionController = new SW.Core.Actions.ActionDefinitionController({
            containerID: dialogContainerID,
            plugin: actionPlugin,
            viewContext: {},
            onPluginReadyFunctionName: onPluginReadyFunctionName,
            enviromentType: EnvType,
            onCreated: function(actionDefinition) {
                // fix for FB361451 - don't lose EscalationLevel property
                fillEscalationLevel(action, actionDefinition);
                actionDefinitionUpdated(actionDefinition);
            },
            onError: handleActionDefinitionError,
            actionDefinition: action,
            onClose: hideDialogContainer,
            mutiEditMode: false,
            enableExecutionSettings: showExecutionSettings,
            anyshared: config.anyshared
    });
    };
    var actionDefinitionUpdated = function (actionDefinition) {
        hideDialogContainer();
        saveEditedAction(actionDefinition);
    };
    var actionPropertiesUpdated = function (actionProperties) {
        hideDialogContainer();
        updateActionProperties(actionProperties);
    };
    var handleActionDefinitionError = function (errorMsg) {
        alert(errorMsg);
        hideDialogContainer();
    };
    var hideDialogContainer = function () {
        $("#" + dialogContainerID).empty().attr('style', '').addClass('actionsOffPage');
    };
    this.pluginReady = function (pluginController) {
        // Method is called when plugin is loaded.
        $("#" + dialogContainerID).removeClass('actionLoading');
        currentActionDefinitionController.PluginReady(pluginController);

        // Disable the loading mask
        loadingMask.hide();
    };
    SW.Core.Actions.MultiEditController.instance = this;
    return {
        editAction: function (ids, callback) {
            showDialogContainer();
            onEditEndCallback = callback;
            if (ids.length == 1)
                getActionByIndex(ids[0]);
            else {
                getMultiActions(ids);
            }
        }
    };
}
