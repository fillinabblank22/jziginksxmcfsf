﻿SW.Core.namespace("SW.Core.Actions").TextToSpeechController = function (config) {
    "use strict";

    // Private methods

    function isEditable(key) {
        return !config.multiEditMode || $container.find('[data-edited="' + key + '"] input:checked').length == 1;
    }

    function getDataElement(key) {
        return $container.find('[data-from="' + key + '"]');
    };

    function getEditedElement(key) {
        return $container.find('[data-edited="' + key + '"]');
    }

    function disableEnableElement(key, value) {
        getDataElement(key).attr('disabled', value);
    }

    function disableEnableSettingsElements(value) {
        if (value) {
            $('input:radio[name*=speechSettings][value="0"]').attr("checked", true);
            $('[id*=customSpeechSettings]').hide();
        }
        disableEnableElement(config.dataFormMapping.Speed, value);
        disableEnableElement(config.dataFormMapping.Pitch, value);
        disableEnableElement(config.dataFormMapping.Volume, value);
        $('input:radio[name*=speechSettings]').attr('disabled', value);
    }

    function configureMultiEditField() {
        if (config.multiEditMode) {
            for (var key in config.dataFormMapping) {
                var dataform = config.dataFormMapping[key];
                var editedElement = getEditedElement(dataform);
                if (dataform == config.dataFormMapping.CustomizeSpeechSettings) {
                    disableEnableSettingsElements(true);
                    editedElement.click(function () {
                        disableEnableSettingsElements(!$(this).find("input").is(':checked'));
                    });
                } else if (editedElement) {
                    disableEnableElement(dataform, true);
                    editedElement.click(function () {
                        disableEnableElement($(this).attr("data-edited"), !$(this).find("input").is(':checked'));
                    });
                }
            }
        }
    }

    var createConfiguration = function () {
        var configuration = {
            SpeechEngine: getDataElement(config.dataFormMapping.SpeechEngine).val(),
            CustomizeSpeechSettings: getDataElement(config.dataFormMapping.CustomizeSpeechSettings).find('input[type="radio"]').is(':checked'),
            Speed: getDataElement(config.dataFormMapping.Speed).val(),
            Pitch: getDataElement(config.dataFormMapping.Pitch).val(),
            Volume: getDataElement(config.dataFormMapping.Volume).val(),
            Text: getDataElement(config.dataFormMapping.Text).val()
            //Pronunciation:getDataElement(config.dataFormMapping.Pronunciation).val()
        };
        return configuration;
    };

    var callActionAndExecuteCallback = function (action, param, callback) {

        SW.Core.Services.callController("/api/TextToSpeech/" + action, param,
        // On success
            function (response) {
                if ($.isFunction(callback))
                    callback({ isError: false, actionDefinition: response });
            },
        // On error
            function (msg) {
                if ($.isFunction(callback)) {
                    callback({ isError: true, ErrorMessage: msg });
                }
            }
        );
    };

    var createAction = function (callback) {
        callActionAndExecuteCallback("Create", createConfiguration(), callback);
    };

    var updateAction = function (callback) {
        var configuration = createConfiguration();
        configuration.Action = config.actionDefinition;
        callActionAndExecuteCallback('Update', configuration, callback);
    };

    var validateTextSection = function (configuration) {
        if (isEditable(config.dataFormMapping.Text)) {
            var textElement = getDataElement(config.dataFormMapping.Text);
            if (configuration.Text.length == 0) {
                textElement.addClass('invalidValue');
                return false;
            }
            else {
                textElement.removeClass('invalidValue');
                return true;
            }
        } else {
            return true;
        }
    };

    var validateVoiceSection = function (configuration) {
        var isValid = true;
        if (isEditable(config.dataFormMapping.CustomizeSpeechSettings)) {
            for (var key in config.dataFormMapping) {
                if (config.dataFormMapping[key] != config.dataFormMapping.Text)
                    getDataElement(config.dataFormMapping[key]).removeClass("invalidValue");
            }

            if (configuration.CustomizeSpeechSettings) {
                isValid = validateSpeed(getDataElement(config.dataFormMapping.Speed), configuration.Speed) && isValid;
                isValid = validatePitchAndVolume(getDataElement(config.dataFormMapping.Pitch), configuration.Pitch) && isValid;
                isValid = validatePitchAndVolume(getDataElement(config.dataFormMapping.Volume), configuration.Volume) && isValid;
            }
        }

        return isValid;
    };

    var validateSpeed = function (element, value) {
        var isValid = $.isNumeric(value) && parseInt(value) >= 0 && parseInt(value) <= 1000;
        changeValidationState(element, isValid);
        return isValid;
    };

    var validatePitchAndVolume = function (element, value) {
        var isValid = $.isNumeric(value) && parseInt(value) >= 0 && parseInt(value) <= 100;
        changeValidationState(element, isValid);
        return isValid;
    };

    var changeValidationState = function (element, isValid) {
        if (!isValid) {
            element.addClass('invalidValue');
        }
    };

    // Public methods

    this.init = function () {
        $container = $('#' + config.containerID);

        $('input:radio[name*=speechSettings]').change(function (event) {
            var content = $('[id*=customSpeechSettings]');
            if (event.srcElement.value == '1') {
                content.show();
            }
            else {
                content.hide();
            }
        });

        configureMultiEditField();

        if ($.isFunction(config.onReadyCallback)) {
            config.onReadyCallback(self);
        }
    };

    // Validate action configuration. 
    this.validateSectionAsync = function (sectionID, callback) {
        var configuration = createConfiguration();
        var isValid;
        if (sectionID == 'textSection') {
            isValid = validateTextSection(configuration);
        } else {
            isValid = validateVoiceSection(configuration);
        }
        if ($.isFunction(callback)) {
            callback(isValid);
        }
    };

    this.getActionDefinitionAsync = function (callback) {

        // Edit mode.
        if (config.actionDefinition) {
            updateAction(callback);
        } else {
            createAction(callback);
        }

    };

    this.getUpdatedActionProperies = function (callback) {
        var jsonProperties = new Array();
        var configuration = createConfiguration();

        for (var key in config.dataFormMapping) {
            var property = config.dataFormMapping[key];

            if (isEditable(property)) {
                jsonProperties.push({ PropertyName: property, PropertyValue: configuration[property] });
                if (configuration.CustomizeSpeechSettings && config.dataFormMapping.CustomizeSpeechSettings == property) {
                    jsonProperties.push({ PropertyName: config.dataFormMapping.Speed, PropertyValue: configuration.Speed });
                    jsonProperties.push({ PropertyName: config.dataFormMapping.Pitch, PropertyValue: configuration.Pitch });
                    jsonProperties.push({ PropertyName: config.dataFormMapping.Volume, PropertyValue: configuration.Volume });
                }
            }
        }
        if (callback) {
            callback(JSON.stringify(jsonProperties));
        }
    };

    // Constructor

    var self = this;
    var $container = null;
}