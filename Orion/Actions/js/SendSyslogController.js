﻿SW.Core.namespace("SW.Core.Actions").SendSyslogController = function (config) {
    "use strict";

    var SECURE_PROTOCOL = "TcpOverTls";
    // Private methods

    var getValue = function (key) {
        return $container.find('[data-form="' + key + '"]').val();
    };

    var getCheckBoxValue = function (key) {
        return $container.find('[data-form="' + key + '"]').is(':checked');
    }

    var createConfiguration = function () {
        var syslogProtocol = getValue(config.dataFormMapping.protocol);
        var configuration = {
            HostsString: getValue(config.dataFormMapping.hosts),
            Facility: getValue(config.dataFormMapping.facility),
            Severity: getValue(config.dataFormMapping.severity),
            Message: getValue(config.dataFormMapping.message),
            Protocol: syslogProtocol,
            IgnoreCertificateChainErrors: getCheckBoxValue(config.dataFormMapping.ignoreCertificateChainErrors),
            DisableCheckCertificateRevocation: getCheckBoxValue(config.dataFormMapping.disableCheckCertificateRevocation),
            IgnoreCertificateNameMismatch: getCheckBoxValue(config.dataFormMapping.ignoreCertificateNameMismatch)
        };

        return configuration;
    };

    var createActionAndExecuteCallback = function (configuration, callback) {
        callActionAndExecuteCallback("Create", configuration, callback);
    };

    var updateActionAndExecuteCallback = function (action, configuration, callback) {
        var request = {
            Action: action,
            Configuration: configuration
        };

        callActionAndExecuteCallback("Update", request, callback);
    };

    var callActionAndExecuteCallback = function (action, param, callback) {

        SW.Core.Services.callController("/api/SendSyslog/" + action, param,
        // On success   
            function (response) {
                if ($.isFunction(callback)) {
                    callback({ isError: false, actionDefinition: response });
                }
            },
        // On error
            function (msg) {
                if ($.isFunction(callback)) {
                    callback({ isError: true, ErrorMessage: msg });
                }
            }
        );
    };

    var getElement = function (key) {
        return $container.find('[data-form="' + key + '"]');
    };

    function isEditable(key) {
        return !config.multiEditMode || $container.find('[data-edited="' + key + '"] input:checked').length == 1;
    }

    var validateConfiguration = function (configuration) {
        var isValid = true;
        var hostsElement = getElement(config.dataFormMapping.hosts);
        hostsElement.removeClass('invalidValue');
       
        if (isEditable(config.dataFormMapping.hosts)) {
            isValid = isValidInput(configuration.HostsString, hostsElement) && isValid;
            if (isValid) {
                isValid = validateHosts(configuration.HostsString);
                if(!isValid) {
                    hostsElement.addClass("invalidValue");
                }
            }
        }
        return isValid;
    };

    var isValidInput = function (value, element) {
        if (value.trim().length == 0) {
            element.addClass("invalidValue");
            return false;
        }
        return true;
    };

    var validateHosts = function (hosts) {
        var isValid = false;
        SW.Core.Services.callControllerSync("/api/SendSyslog/ValidateServerName", { HostsString: hosts }, function (result) {
            isValid = result;
        }, function () {
        });
        return isValid;
    };

    var showCertificateSettings = function () {
        if ($container.find('[data-form="' + config.dataFormMapping.protocol + '"]').val() === SECURE_PROTOCOL) {
            $container.find('[class="certificateSettings"').show();
        } else {
            $container.find('[class="certificateSettings"').hide();
        }
    };

    // Public methods

    this.init = function () {
        $container = $('#' + config.containerID);
        if (config.multiEditMode) {
            for (var key in config.dataFormMapping) {
                var dataform = config.dataFormMapping[key];
                $container.find('[data-form="' + dataform + '"]').attr("disabled", true);
                $container.find('[data-edited="' + dataform + '"]').click(function () {
                    var dataedited = $(this).attr("data-edited");
                    $container.find('[data-form="' + dataedited + '"]').attr("disabled", !$(this).find("input").is(':checked'));
                    if (dataedited === config.dataFormMapping.protocol) {
                        $container.find('[data-form="' + config.dataFormMapping.ignoreCertificateChainErrors + '"]').attr("disabled", !$(this).find("input").is(':checked'));
                        $container.find('[data-form="' + config.dataFormMapping.disableCheckCertificateRevocation + '"]').attr("disabled", !$(this).find("input").is(':checked'));
                        $container.find('[data-form="' + config.dataFormMapping.ignoreCertificateNameMismatch + '"]').attr("disabled", !$(this).find("input").is(':checked'));
                    }
                });
            }
        }
        showCertificateSettings();
        $container.find('[data-form="' + config.dataFormMapping.protocol + '"]').change(showCertificateSettings);

        if ($.isFunction(config.onReadyCallback)) {
            config.onReadyCallback(self);
        }
    };

    // Validate action configuration. 
    this.validateSectionAsync = function (sectionID, callback) {
        var configuration = createConfiguration();
        if ($.isFunction(callback)) {
            callback(validateConfiguration(configuration));
        }
    };

    this.getUpdatedActionProperies = function (callback) {
        var jsonProperties = new Array();
        for (var key in config.dataFormMapping) {
            var dataform = config.dataFormMapping[key];
            if ($container.find('[data-edited="' + dataform + '"] input:checked').length == 1) {
                var propertyValue = $container.find('[data-form="' + dataform + '"]').val();
                jsonProperties.push({ PropertyName: dataform, PropertyValue: propertyValue });
                if (dataform === config.dataFormMapping.protocol) {
                    jsonProperties.push({ PropertyName: config.dataFormMapping.ignoreCertificateChainErrors, PropertyValue: propertyValue === SECURE_PROTOCOL && $container.find('[data-form="' + config.dataFormMapping.ignoreCertificateChainErrors + '"]').is(':checked') });
                    jsonProperties.push({ PropertyName: config.dataFormMapping.disableCheckCertificateRevocation, PropertyValue: propertyValue === SECURE_PROTOCOL && $container.find('[data-form="' + config.dataFormMapping.disableCheckCertificateRevocation + '"]').is(':checked') });
                    jsonProperties.push({ PropertyName: config.dataFormMapping.ignoreCertificateNameMismatch, PropertyValue: propertyValue === SECURE_PROTOCOL && $container.find('[data-form="' + config.dataFormMapping.ignoreCertificateNameMismatch + '"]').is(':checked') });
                }

            }
        }
        if (callback) {
            callback(JSON.stringify(jsonProperties));
        }
    };

    this.getActionDefinitionAsync = function (callback) {
        var configuration = createConfiguration();

        // Edit mode.
        if (config.actionDefinition) {

            // We have action definition we just want to updated existing one.
            updateActionAndExecuteCallback(config.actionDefinition, configuration, callback);

        } else {

            // New action definition mode.
            createActionAndExecuteCallback(configuration, callback);
        }
    };

    // Constructor

    var self = this;
    var $container = null;
}
