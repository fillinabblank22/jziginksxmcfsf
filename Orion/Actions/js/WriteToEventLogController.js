﻿SW.Core.namespace("SW.Core.Actions").WriteToEventLogController = function (config) {
    "use strict";

    // Private methods
    var getMessageElement = function () {
        return $container.find('[data-form="' + config.dataFormMapping.message + '"]');
    };
    var getMessage = function () {
        return $container.find('[data-form="' + config.dataFormMapping.message + '"]').val();
    };

    var getMachineConfiguration = function () {
        return "";
    };

    function isEditable(key) {
        return !config.multiEditMode || $container.find('[data-edited="' + key + '"] input:checked').length == 1;
    }

    var validateEventLogSection = function () {
        var isValid = true;
        if (isEditable(config.dataFormMapping.message)) {
            var message = getMessage();
            var messageElement = getMessageElement();
            if (message.trim().length == 0) {
                messageElement.addClass('invalidValue');
                isValid = isValid && false;
            } else {
                messageElement.removeClass('invalidValue');
                isValid = isValid && true;
            }
        }
        
        return isValid;
    };
    
    var createConfiguration = function () {

        var configuration = {
            Machine: getMachineConfiguration(),
            Message: getMessage()
        };

        return configuration;
    };

    var createActionAndExecuteCallback = function (configuration, callback) {
        callActionAndExecuteCallback("Create", configuration, callback);
    };

    var updateActionAndExecuteCallback = function (action, configuration, callback) {

        configuration.Action = action;
        callActionAndExecuteCallback("Update", configuration, callback);
    };

    var callActionAndExecuteCallback = function (action, param, callback) {

        SW.Core.Services.callController("/api/WriteToEventLog/" + action, param,
        // On success
            function (response) {
                if ($.isFunction(callback)) {
                    callback({ isError: false, actionDefinition: response });
                }
            },
        // On error
            function (msg) {
                if ($.isFunction(callback)) {
                    callback({ isError: true, ErrorMessage: msg });
                }
            }
        );

    };

    // Public methods

    this.init = function () {
        $container = $('#' + config.containerID);

        if (config.multiEditMode) {
            for (var key in config.dataFormMapping) {
                var dataform = config.dataFormMapping[key];
                if (dataform == config.dataFormMapping.machine) {
                    $container.find(".machineType input").attr("disabled", true);
                    $container.find('[data-edited="' + dataform + '"]').click(function () {
                        $container.find(".machineType input").attr("disabled", !$(this).find("input").is(':checked'));
                    });
                }
                $container.find('[data-form="' + dataform + '"]').attr("disabled", true);
                $container.find('[data-edited="' + dataform + '"]').click(function () {
                    var dataedited = $(this).attr("data-edited");
                    $container.find('[data-form="' + dataedited + '"]').attr("disabled", !$(this).find("input").is(':checked'));
                });
            }
        }
        if ($.isFunction(config.onReadyCallback)) {
            config.onReadyCallback(self);
        }
    };

    // Validate action configuration. 
    this.validateSectionAsync = function (sectionID, callback) {
        var valid = true;

        if (sectionID === 'eventLogSection') {
            valid = validateEventLogSection();
        }

        if ($.isFunction(callback)) {
            callback(valid);
        }
    };

    this.getUpdatedActionProperies = function (callback) {
        var jsonProperties = new Array();
        for (var key in config.dataFormMapping) {
            var dataform = config.dataFormMapping[key];
            if ($container.find('[data-edited="' + dataform + '"] input:checked').length == 1)
                if (dataform == config.dataFormMapping.machine) {
                    jsonProperties.push({ PropertyName: dataform, PropertyValue: getMachineConfiguration() });
                } else {
                    jsonProperties.push({ PropertyName: dataform, PropertyValue: $container.find('[data-form="' + dataform + '"]').val() });
                }
        }
        if (callback) {
            callback(JSON.stringify(jsonProperties));
        }
    };

    this.getActionDefinitionAsync = function (callback) {

        var configuration = createConfiguration();

        // Edit mode.
        if (config.actionDefinition) {

            // We have action definition we just want to updated existing one.
            updateActionAndExecuteCallback(config.actionDefinition, configuration, callback);

        } else {

            // New action definition mode.
            createActionAndExecuteCallback(configuration, callback);
        }
    };

    // Constructor

    var self = this;
    var $container = null;
}
