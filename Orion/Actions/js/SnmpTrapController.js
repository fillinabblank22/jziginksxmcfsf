﻿SW.Core.namespace("SW.Core.Actions").SNMPTrapController = function (config) {
    "use strict";

    var numericStringRegex = /^\d+$/;

    var createConfiguration = function () {
        var configuration = {
            EnvironmentType: config.environmentType,
            TrapDestinations: getTrapDestinations(),
            TrapTemplate: $container.find("[data-form='" + config.dataFormMapping.trapTemplate + "']").val(),
            AlertMesssage: $container.find("[data-form='" + config.dataFormMapping.alertMessage + "']").val(),
            UdpPort: $container.find("[data-form='" + config.dataFormMapping.udpPort + "']").val(),
            SnmpCredentials: SW.Core.SNMPCredentialsController.getSnmpCredentialsAsync(),
            SnmpVersion: SW.Core.SNMPCredentialsController.getSnmpVersion()
        };
        return configuration;
    };

    function getTrapDestinations() {
        var inputValue = $container.find("[data-form='" + config.dataFormMapping.trapDestinations + "']").val();
        var resultArray = new Array();
        SW.Core.Services.callControllerSync("/api/SnmpTrap/ValidateDestination", { TrapDestinations: inputValue }, function (result) {
            for (var key in result) {
                resultArray.push(key);
                if (!result[key])
                    $container.find("[data-form='" + config.dataFormMapping.trapDestinations + "']").addClass("invalid");
            }
        });
        return resultArray.join(";");
    }

    var callActionAndExecuteCallback = function (action, param, callback) {

        SW.Core.Services.callController("/api/SnmpTrap/" + action, param,
        // On success
            function (response) {
                if ($.isFunction(callback))
                    callback({ isError: false, actionDefinition: response });
            },
        // On error
            function (msg) {
                if ($.isFunction(callback)) {
                    callback({ isError: true, ErrorMessage: msg });
                }
            }
        );
    };

    var updateActionAndExecuteCallback = function (action, configuration, callback) {

        var updateConfig = {
            Action: action,
            Configuration: configuration
        };

        callActionAndExecuteCallback("Update", updateConfig, callback);
    };

    var createActionAndExecuteCallback = function (configuration, callback) {
        callActionAndExecuteCallback("Create", configuration, callback);
    };

    this.init = function () {

        $container = $('#' + config.containerID);
        setMultyEditMode();
        if ($.isFunction(config.onReadyCallback)) {
            config.onReadyCallback(self);
        }
    };

    function setMultyEditMode() {
        if (config.multiEditMode) {
            for (var key in config.dataFormMapping) {
                var dataform = config.dataFormMapping[key];
                if (dataform == config.dataFormMapping.snmpCredentials) {
                    $container.find("[data-form='SNMPVersion']").attr("disabled", true);
                    $container.find(".credContainer").hide();
                    $container.find('[data-edited="' + dataform + '"]').click(function () {
                        var isChecked = $(this).find("input").is(':checked');
                        $container.find("[data-form='SNMPVersion']").attr("disabled", !isChecked);
                        if (isChecked)
                            SW.Core.SNMPCredentialsController.init();
                        else {
                            $container.find("[data-form='SNMPVersion']").attr("disabled", true);
                            $container.find(".credContainer").hide();
                        }
                    });
                } else {
                    $container.find("[data-form='" + dataform + "']").attr("disabled", true);
                    $container.find('[data-edited="' + dataform + '"]').click(function () {
                        var dataedited = $(this).attr("data-edited");
                        var isChecked = $(this).find("input").is(':checked');
                        $container.find("[data-form='" + dataedited + "']").attr("disabled", !isChecked);
                    });
                }
            }
        }
    }

    // Validate action configuration. 
    this.validateSectionAsync = function (sectionID, callback) {
        var valid = true;
        var configuration = createConfiguration();
        if (sectionID == "SNMPTrapMessage")
            valid = validateSnmpSectionSection(configuration);
        else if (sectionID == "SNMPProperties")
            valid = validateSnmpPropertiesSection(configuration);
        if ($.isFunction(callback)) {
            callback(valid);
        }
    };


    this.getActionDefinitionAsync = function (callback) {
        var configuration = createConfiguration();

        // Edit mode.
        if (config.actionDefinition) {

            // We have action definition we just want to updated existing one.
            updateActionAndExecuteCallback(config.actionDefinition, configuration, callback);

        } else {

            // New action definition mode.
            createActionAndExecuteCallback(configuration, callback);
        }
    };
    this.getSectionAnnotation = function (sectionID) {
        var configuration = createConfiguration();
        var annotation = "";
        switch (sectionID) {
            case "SNMPTrapMessage":
                if (isEditable(config.dataFormMapping.trapDestinations))
                    annotation = configuration.TrapDestinations;
                break;
            case "SNMPProperties":
                if (configuration.SnmpVersion && isEditable(config.dataFormMapping.snmpCredentials))
                    annotation = configuration.SnmpVersion;
                break;
            default:
                annotation = "";
                break;
        }
        return annotation;
    };

    function isEditable(key) {
        return !config.multiEditMode || $container.find('[data-edited="' + key + '"] input:checked').length == 1;
    }

    function validateSnmpSectionSection(configuration) {
        var valid = true;
        $container.find("[data-form='" + config.dataFormMapping.trapDestinations + "']").removeClass("invalid");
        $container.find("[data-form='" + config.dataFormMapping.trapTemplate + "']").removeClass("invalid");

        if (isEditable(config.dataFormMapping.trapDestinations)) {
            if (configuration.TrapDestinations.length == 0) {
                valid = false;
            }
            else {
                SW.Core.Services.callControllerSync("/api/SnmpTrap/ValidateDestination", configuration, function(result) {
                    for (var key in result) {
                        valid = valid && result[key];
                    }
                });
            }
            if (!valid)
                $container.find("[data-form='" + config.dataFormMapping.trapDestinations + "']").addClass("invalid");
        }
        if (isEditable(config.dataFormMapping.trapTemplate) && configuration.TrapTemplate == "") {
            valid = false;
            $container.find("[data-form='" + config.dataFormMapping.trapTemplate + "']").addClass("invalid");
        }
        return valid;
    }

    function validateSnmpPropertiesSection(configuration) {
        var valid = true;
        $container.find("[data-form='" + config.dataFormMapping.udpPort + "']").removeClass("invalid");
        if (isEditable(config.dataFormMapping.udpPort)) {
            if (configuration.UdpPort != "" && parseInt(configuration.UdpPort, 10) != "NaN" && parseInt(configuration.UdpPort, 10) <= 65535) {
                if (!numericStringRegex.test(configuration.UdpPort)) {
                    valid = false;
                    $container.find("[data-form='" + config.dataFormMapping.udpPort + "']").addClass("invalid");
                }
            } else {
                valid = false;
                $container.find("[data-form='" + config.dataFormMapping.udpPort + "']").addClass("invalid");
            }
        }
        if (isEditable(config.dataFormMapping.snmpCredentials)) {
            if (!SW.Core.SNMPCredentialsController.validate()) {
                valid = false;
                $container.find("[data-form='" + config.dataFormMapping.snmpCredentials + "']").addClass("invalid");
            }
        }
        return valid;
    }

    this.getUpdatedActionProperies = function (callback) {
        var jsonProperties = new Array();
        for (var key in config.dataFormMapping) {
            var dataform = config.dataFormMapping[key];
            if ($container.find('[data-edited="' + dataform + '"] input:checked').length == 1)
                if (dataform == config.dataFormMapping.snmpCredentials) {
                    jsonProperties.push({ PropertyName: dataform, PropertyValue: JSON.stringify(SW.Core.SNMPCredentialsController.getSnmpCredentialsAsync()) });
                    jsonProperties.push({ PropertyName: "SnmpVersion", PropertyValue: SW.Core.SNMPCredentialsController.getSnmpVersion() });
                }
                else if (dataform == config.dataFormMapping.Tra) {
                    jsonProperties.push({ PropertyName: dataform, PropertyValue: getTrapDestinations() });
                }
                else {
                    jsonProperties.push({ PropertyName: dataform, PropertyValue: $container.find('[data-form="' + dataform + '"]').val() });
                }
        }
        if (callback) {
            callback(JSON.stringify(jsonProperties));
        }
    };

    var self = this;
    var $container = null;
}
