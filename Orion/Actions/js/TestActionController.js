﻿SW.Core.namespace("SW.Core.Actions").TestActionController = function () {
    var config = null;

    var texts = {
         'dialog.title': '@{R=Core.Strings;K=WEBJS_ZT0_30; E=js}'
    };

    $(function() {
    
        $('#btnNetObjectSelected').on('click', function(e) {
            e.preventDefault();      
            netObjectSelected();          
            return false;
        });

        $('#btnSimulate').on('click', function (e) {
            e.preventDefault();
            simulate();
            return false;
        });


        $('#btnCloseNetObjectSelector, #btnCloseTestActionResult, #btnOKTestActionResult').on('click', function (e) {
            e.preventDefault();      
            closeTestActionDialog();          
            return false;
        });
    
    });

    var selectedEntities = [];
 
    var showNetObjectPicker = function (dialogTitle) {

        SW.Orion.NetObjectPicker.SetUpEntities([{ MasterEntity: config.viewContext.EntityType }], config.viewContext.EntityType, selectedEntities, 'Single');

        $('#testActionResult').hide();
        $('#testActionNetObjectPicker').show();
        $('#netObjectPickerPlaceHolder').prependTo('#testActionNetObjectPicker');
        
        $('#testActionDialog').dialog({
            position: 'center',
            width: 700,
            height: 'auto',
            modal: true,
            draggable: true,
            dialogClass: 'crop-action-title',
            title: dialogTitle,
            show: { duration: 0 },
            close: function() {
                var entities = SW.Orion.NetObjectPicker.GetSelectedEntities();
                selectedEntities = (entities) ? entities : [];
            }
        });

    };

    var closeTestActionDialog = function() {
        $('#testActionDialog').dialog('close');
    };

    var netObjectSelected = function () {
        var entities = SW.Orion.NetObjectPicker.GetSelectedEntities();

        if (entities && entities.length == 1) {
            selectedEntities = entities;
            $('#testActionDialog').addClass('actionLoading');
            
            executeActionOnServer(entities[0]);
        }
    };

    var simulate = function () {
        var entities = SW.Orion.NetObjectPicker.GetSelectedEntitiesForTestAction();

        if (entities && entities.length == 1) {
            $('#testActionDialog').addClass('actionLoading');
            selectedEntities = entities;
            var param = {
                EnvironmentType: config.enviromentType,
                ActionDefinition: config.action,
                ActionContext: config.viewContext
            };

            if (entities[0] && config.enviromentType == 'Alerting') {
                param.ActionContext.EntityUri = entities[0];
                param.ActionContext.EntityType = config.viewContext.EntityType;
            }


            SW.Core.Services.callController("/api/Action/Simulate", param,
                // On success
                function (response) {
                    $('#testActionResultMsg').removeClass('sw-suggestion').removeClass('sw-suggestion-pass').removeClass('sw-suggestion-fail').html(response);
                    showResult();
                },
                // On error
                function (msg) {
                    showActionTestResult({ Status: SW.Core.Actions.Enums.ActionStatus.Failed, ErrorMessage: msg });
                }
            );
        }
    };

    var showResult = function() {
        $('#testActionNetObjectPicker').hide();
        $('#testActionResult').show();
        $('#testActionDialog').removeClass('actionLoading');
    };

     var showActionTestResult = function(result) {
         var $msg = $('#testActionResultMsg').addClass('sw-suggestion');
         $msg.removeClass('sw-suggestion-pass').removeClass('sw-suggestion-fail');

         if (result.Status == SW.Core.Actions.Enums.ActionStatus.Success) {
             $('#btnOKTestActionResult').show();
             $msg.html('<span style="width: 16px; height: 16px" class="sw-suggestion-icon" ></span>' + '@{R=Core.Strings;K=WEBJS_ZT0_31; E=js}')
                  .addClass('sw-suggestion-pass');  
         } else {
             $('#btnOKTestActionResult').hide();
               $msg.html('<span style="width: 16px; height: 16px" class="sw-suggestion-icon" ></span>' + result.ErrorMessage)
                   .addClass('sw-suggestion-fail');
          }
         showResult();
    };


     var executeActionOnServer = function (entityUri) {

         var entities = SW.Orion.NetObjectPicker.GetSelectedEntitiesForTestAction();
         if (entities && entities.length == 1) {
             entityUri = entities[0];
         }
         var param = {
             EnvironmentType : config.enviromentType,
             ActionDefinition : config.action,
             ActionContext :  config.viewContext
        };

         if (entityUri && config.enviromentType == 'Alerting') {
            param.ActionContext.EntityUri = entityUri;
            param.ActionContext.EntityType = config.viewContext.EntityType;
        }


        SW.Core.Services.callController("/api/Action/TestAction", param,
            // On success
            function (response) {
                showActionTestResult(response);
            },
            // On error
            function (msg) {
                 showActionTestResult({ Status: SW.Core.Actions.Enums.ActionStatus.Failed, ErrorMessage: msg });
            }
        );
    };
  
    var testAction = function (c) {
        config = c;

        SW.Core.Services.callController("/api/Action/SimulateExecuteAvailable", { actionTypeId: c.action.ActionTypeID },
            // On success
            function (response) {
                if (!response.SimulateAvaible && !response.ExecuteAvaible) {
                    showActionTestResult({ Status: SW.Core.Actions.Enums.ActionStatus.Failed, ErrorMessage: '@{R=Core.Strings.2;K=WEBJS_IT0_14; E=js}' });
                } else {
                    $('#btnSimulate').toggle(response.SimulateAvaible);
                    $('#btnNetObjectSelected').toggle(response.ExecuteAvaible);
                }
            },
            // On error
            function (msg) {
                showActionTestResult({ Status: SW.Core.Actions.Enums.ActionStatus.Failed, ErrorMessage: msg });
            }
        );

        var dialogTitle = texts['dialog.title'].replace('{0}', c.action.Title);
        showNetObjectPicker(dialogTitle);
    };

    // Public interface
    return {
        
        /* 
        * @param {Object} config 
        *  config.ActionContext.EntityType {String} entity type which will be shown in net object picker.
        */
        TestAction: testAction
    };

}();