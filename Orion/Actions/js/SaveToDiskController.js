﻿SW.Core.namespace("SW.Core.Actions").SaveToDiskController = function (config) {
    "use strict";

    var createConfiguration = function () {
        var contentTypesArray = new Array();
        $.each($container.find('[data-form="contentType"]:checked'), function () {
            contentTypesArray.push($(this).attr("enum-value"));
        });
        var configuration = {
            Configuration: {
                SavePath: $container.find('[data-form="savePath"]').val(),
                Printable: $container.find('[data-form="printable"]').is(':checked'),
                Credentials: {
					Name: $container.find('[data-form="userName"]').val(),
                    Username: $container.find('[data-form="userName"]').val(),
                    Password: $container.find('[data-form="password"]').val()
                }
            },
            StringContentType: contentTypesArray
        };

        return configuration;
    };

    var callActionAndExecuteCallback = function (action, param, callback) {

        SW.Core.Services.callController("/api/SaveToDiskURL/" + action, param,
        // On success
            function (response) {
                if ($.isFunction(callback))
                    callback({ isError: false, actionDefinition: response });
            },
        // On error
            function (msg) {
                if ($.isFunction(callback)) {
                    callback({ isError: true, ErrorMessage: msg });
                }
            }
        );
    };

    var updateActionAndExecuteCallback = function (action, configuration, callback) {

        var updateConfig = {
            Action: action,
            Configuration: configuration
        };

        callActionAndExecuteCallback("Update", updateConfig, callback);
    };

    var createActionAndExecuteCallback = function (configuration, callback) {
        callActionAndExecuteCallback("Create", configuration, callback);
    };

    this.init = function () {
        $container = $('#' + config.containerID);

        if ($.isFunction(config.onReadyCallback)) {
            config.onReadyCallback(self);
        }
    };

    // Validate action configuration. 
    this.validateSectionAsync = function (sectionID, callback) {
        var configuration = createConfiguration();
        validate(configuration, callback);
    };

    this.getActionDefinitionAsync = function (callback) {
        var configuration = createConfiguration();

        // Edit mode.
        if (config.actionDefinition) {

            // We have action definition we just want to updated existing one.
            updateActionAndExecuteCallback(config.actionDefinition, configuration, callback);

        } else {

            // New action definition mode.
            createActionAndExecuteCallback(configuration, callback);
        }
    };

    function validateConfiguration(config, callback) {
        var valid = true;
        $container.find('[data-form="savePath"]').removeClass("invalidValue");
        $container.find('[data-form="userName"]').removeClass("invalidValue");
        $container.find('[data-form="password"]').removeClass("invalidValue");
        $("#checkboxValidation").hide();
        $('#testResult').empty();

        valid = validatePathInput(config.Configuration.SavePath, $container.find('[data-form="savePath"]')) && valid;
        valid = validateUserName(config.Configuration.Credentials.Username, $container.find('[data-form="userName"]')) && valid;
        valid = validateUserPassword(config.Configuration.Credentials.Password, $container.find('[data-form="password"]')) && valid;

        if ($container.find('[data-form="contentType"]:checked').length == 0) {
            $("#checkboxValidation").show();
            valid = false;
        }
        return callback(valid);
    }

    var validate = function (configuration, callback) {
        validateConfiguration(configuration, function (isValid) {
            if (isValid) {
                $('#testProgress').show();
                SW.Core.Services.callController("/api/SaveToDiskURL/ValidateConfg", configuration, function (result) {
                    var isValid = true;
                    for (var key in result) {
                        $container.find('[data-form="' + key + '"]').removeClass("invalidValue");
                        if (!result[key]) {
                            $container.find('[data-form="' + key + '"]').addClass("invalidValue");
                            isValid = false;
                        }
                    }
                    showHideCredentialsMessage(result["credentials"]);
                    if ($.isFunction(callback))
                        callback(isValid);
                });
            } else {
                if ($.isFunction(callback))
                    callback(false);
            }
        });
    };

    // Validation data from form
    function isFilePath(path) {
        var regex = /^(\\)(\\[^\/:*?"<>|]+)+$/;
        return regex.test(path);
    }

    function validatePathInput(value, elem) {
        var validationResult = true;
        $.each(value.split(';'), function () {
            if (!isFilePath($.trim(value))) {
                validationResult = false;
                elem.addClass("invalidValue");
            }
        });
        return validationResult;
    }

    function isUserName(userName) {
        var regex = /^[^\/:*?"<>|]+$/;
        return regex.test(userName);
    }

    function validateUserName(value, elem) {
        var validationResult = true;
        if (!isUserName($.trim(value))) {
            validationResult = false;
            elem.addClass("invalidValue");
        }
        return validationResult;
    }

    function isPassword(password) {
        var validationResult = true;
        if (password.length == 0)
            validationResult = false;
        return validationResult;
    }

    function validateUserPassword(value, elem) {
        var validationResult = true;
        if (!isPassword($.trim(value))) {
            validationResult = false;
            elem.addClass("invalidValue");
        }
        return validationResult;
    }

    this.testUserCredentials = function () {
        $('#testResult').empty();
        var configuration = createConfiguration();
        $('#testResult').empty();
        $('#testProgress').show();

        SW.Core.Services.callController("/api/SaveToDiskURL/TestCredentials", configuration,
        // On success
                function (response) {
                    $('#testProgress').hide();
                    showHideCredentialsMessage(response);
                    $container.find('[data-form="userName"]').removeClass("invalidValue");
                    $container.find('[data-form="password"]').removeClass("invalidValue");
                },
        // On error
                function (msg) {
                    alert(msg);
                    $('#testProgress').hide();
                });
    };

    function showHideCredentialsMessage(isValid) {
        $('#testProgress').hide();
        if (isValid) {
            $('#testResult').html('<div class="sw-suggestion sw-suggestion-pass" style="margin-bottom: 10px;margin-left: 0px;"><span class="sw-suggestion-icon"></span>@{R=Core.Strings;K=WEBJS_AY0_6;E=js}</div>');
        } else {
            $('#testResult').html('<div class="sw-suggestion sw-suggestion-fail" style="margin-bottom: 10px;margin-left: 0px;"><span class="sw-suggestion-icon"></span>@{R=Core.Strings;K=WEBJS_AY0_7;E=js}</div>');
        }
    }

    var self = this;
    var $container = null;
}
