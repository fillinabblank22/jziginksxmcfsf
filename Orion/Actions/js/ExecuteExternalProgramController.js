﻿SW.Core.namespace("SW.Core.Actions").ExecuteExternalProgramController = function (config) {
    "use strict";

    var createConfiguration = function () {
        var configuration = {
            ProgramPath: $container.find('[data-form="' + config.dataFormMapping.programPath + '"]').val(),
            Credentials: SW.Core.WindowsCredentialSelectorControl.getCredentials()
        };

        return configuration;
    };

    function isEditable(key) {
        return !config.multiEditMode || $container.find('[data-edited="' + key + '"] input:checked').length == 1;
    }

    var callActionAndExecuteCallback = function (action, param, callback) {

        SW.Core.Services.callController("/api/ExecuteExternalProgram/" + action, param,
            // On success
            function (response) {
                if ($.isFunction(callback))
                    callback({ isError: false, actionDefinition: response });
            },
            // On error
            function (msg) {
                if ($.isFunction(callback)) {
                    callback({ isError: true, ErrorMessage: msg });
                }
            }
        );
    };

    var updateActionAndExecuteCallback = function (action, configuration, callback) {
        var updateConfig = {
            Action: action,
            Configuration: configuration
        };

        callActionAndExecuteCallback("Update", updateConfig, callback);
    };

    var createActionAndExecuteCallback = function (configuration, callback) {
        callActionAndExecuteCallback("Create", configuration, callback);
    };

    // Validate action configuration. 
    this.validateSectionAsync = function (sectionID, callback) {
        if (sectionID == "executeProgramSection") {
            var configuration = createConfiguration();
            validateConfiguration(configuration, callback);
        }
        else {
            callback(true);
        }
    };

    this.getUpdatedActionProperies = function (callback) {
        var jsonProperties = new Array();
        if (isEditable(config.dataFormMapping.programPath)) {
            jsonProperties.push({ PropertyName: config.dataFormMapping.programPath, PropertyValue: $container.find('[data-form="' + config.dataFormMapping.programPath + '"]').val() });
            if (SW.Core.WindowsCredentialSelectorControl.isCredentialsDefined()) {
                jsonProperties.push({ PropertyName: "Credentials", PropertyValue: SW.Core.WindowsCredentialsControl.addUpdateWindowsCredential() });
            }
        }

        if (callback) {
            callback(JSON.stringify(jsonProperties));
        }
    };

    this.getActionDefinitionAsync = function (callback) {
        var configuration = createConfiguration();

        // Edit mode.
        if (config.actionDefinition) {

            // We have action definition we just want to updated existing one.
            updateActionAndExecuteCallback(config.actionDefinition, configuration, callback);

        } else {

            // New action definition mode.
            createActionAndExecuteCallback(configuration, callback);
        }
    };

    function validateConfiguration(configuration, callback) {
        var valid = true;
        var programPathField = $container.find('[data-form="' + config.dataFormMapping.programPath + '"]');
        programPathField.removeClass("invalidValue")
        $("#requiredProgramPath").hide();

        if (isEditable(config.dataFormMapping.programPath) && configuration.programPath != "") {
            valid = validatePathInput(configuration.ProgramPath, programPathField) && valid;
            valid = SW.Core.WindowsCredentialSelectorControl.ValidateCredentials() && valid;
            valid = SW.Core.WindowsCredentialSelectorControl.ValidateCredentialName() && valid;
        }

        if ($.isFunction(callback))
            callback(valid);
    }

    // Validation data from form
    function isFilePath(path) {
        if (path.length == 0) {
            return false;
        }
        return true;
    }

    function validatePathInput(value, elem) {
        var validationResult = true;
        $.each(value.split(';'), function () {
            if (!isFilePath($.trim(value))) {
                validationResult = false;
            }
        });

        if (!validationResult) {
            elem.addClass("invalidValue");
            $("#requiredProgramPath").show();
        }

        return validationResult;
    }

    var fieldEditEnabled = function (state, dataEditKey) {
        SW.Core.WindowsCredentialsControl.radioDisabledChange(state);
        $container.find('[data-form="' + dataEditKey + '"]').attr("disabled", state);
    };

    this.init = function () {
        SW.Core.WindowsCredentialSelectorControl.setConfig({
            TestCredentialsRoute: "/api/ExecuteExternalProgram/TestCredentials",
            ValidateCredentialNameRoute: "/api/ExecuteExternalProgram/ValidateCredentialName",
            GetConfiguration: createConfiguration,
            ValidateConfiguration: validateConfiguration
        });
        if (SW.Core.MacroVariablePickerController)
            SW.Core.MacroVariablePickerController.BindPicker();
        $container = $('#' + config.containerID);
        if (config.multiEditMode) {
            $container.find('[data-edited="' + config.dataFormMapping.programPath + '"]').click(function () {
                var dataEdited = $(this).attr("data-edited");
                var disabled = !$(this).find("input").is(':checked');
                fieldEditEnabled(disabled, dataEdited);
            });
            fieldEditEnabled(true, config.dataFormMapping.programPath);
        }
        if ($.isFunction(config.onReadyCallback)) {
            config.onReadyCallback(self);
        }
    };

    // Constructor
    var self = this;
    var $container = null;
}
