﻿SW.Core.namespace("SW.Core.Actions").EmailController = function (config) {
    "use strict";
    // Private methods

    var validateConfg = function (configuration) {
        var isValid = true;
        SW.Core.Services.callControllerSync("/api/Email/ValidateConfg", configuration, function (result) {
            isValid = checkValidationResults(result);
        });

        return isValid;
    };

    var checkValidationResults = function (result) {
        var isValid = true;
        for (var key in result) {
            $container.find('[data-form="' + key + '"]').removeClass("invalidValue");
            if (!result[key]) {
                $container.find('[data-form="' + key + '"]').addClass("invalidValue");
                isValid = false;
            }
        }
        return isValid;
    };

    var createConfiguration = function () {
        var contentTypesArray = new Array();
        var messageContentType = "";
        $.each($container.find('[data-from="' + config.dataFormMapping.messageContentType + '"]:checked'), function () {
            messageContentType = $(this).attr("enum-value");
        });
        var configuration = {
            Sender: $container.find('[data-form="' + config.dataFormMapping.nameOfSender + '"]').val(),
            Title: $container.find('[data-form="ActionName"]').val(),
            EmailTo: $container.find('[data-form="' + config.dataFormMapping.emailTo + '"]').val(),
            Subject: Ext.util.Format.htmlEncode($container.find('[data-form="' + config.dataFormMapping.emailSubject + '"]').val()),
            EmailMessage: $container.find('[data-form="' + config.dataFormMapping.emailMessage + '"]').val(),
            Printable: false,
            EmailFrom: $container.find('[data-form="' + config.dataFormMapping.emailFrom + '"]').val(),
            EmailCC: $container.find('[data-form="' + config.dataFormMapping.emailCC + '"]').val(),
            EmailBCC: $container.find('[data-form="' + config.dataFormMapping.emailBCC + '"]').val(),
            SMTPServer: SW.Core.SMTPServerSettingsController.getSmtpServerAsync(),
            StringContentType: contentTypesArray,
            EnvironmentType: config.environmentType,
            MessageContentType: messageContentType,
            Priority: $container.find('[data-form="' + config.dataFormMapping.priority + '"]').val()
        };
        return configuration;
    };
    function isEmail(email, allowMacros) {
        var validationResult;
        var regex = /.+@.+/;
        var regexMacros = /^\$\{([a-zA-Z0-9_.+-=;\s\$\{\}\'\"])+\}\;*$/;
        validationResult = regex.test(email);
        if (!validationResult && allowMacros)
            validationResult = regexMacros.test(email);
        return validationResult;
    }

    function validateEmailsInput(value, dataMappingKey) {
        var emailSplitRegex = /,|;(?!M=)/;
        var validationResult = true;
        var elem = $container.find('[data-form="' + dataMappingKey + '"]');
        var allowMacros = $(elem).attr("AllowMacros") == 'True';
        elem = elem.parent().find(".emailInputField");
        $.each(value.split(emailSplitRegex), function () {
            if (!isEmail($.trim(this), allowMacros)) {
                validationResult = false;
                elem.parent().find(".validationMessage").show();
                elem.addClass("invalidValue");
                //expand
                if (elem.parents('#senderDetails').length) {
                    $('#senderDetails').show();
                    $("#collapseExpandButton").attr('src', '/Orion/images/Button.Collapse.gif');
                    $("#senderInfo").text("");
                }
            }
        });
        return validationResult;
    }
    function isEditable(key) {
        return !config.multiEditMode || $container.find('[data-edited="' + key + '"] input:checked').length == 1;
    }
    function validateMessageSection(configuration) {
        var valid = true;
        $container.find('[data-form="' + config.dataFormMapping.emailSubject + '"]').removeClass("warningValue");
        $("#checkboxValidation").hide();
        if (configuration.EmailMessage == "" && isEditable(config.dataFormMapping.emailMessage)) {
            $container.find('[data-form="' + config.dataFormMapping.emailMessage + '"]').addClass("invalidValue");
            $container.find('[data-form="' + config.dataFormMapping.emailMessage + '"]').parent().parent().find(".validationMessage").show()
            valid = false;
        } else {
            $container.find('[data-form="' + config.dataFormMapping.emailMessage + '"]').removeClass("invalidValue");
            $container.find('[data-form="' + config.dataFormMapping.emailMessage + '"]').parent().parent().find(".validationMessage").hide()
        }
        return valid;
    }

    function validateResipientsSection(configuration) {
        var valid = true;
        $container.find('[data-form="' + config.dataFormMapping.emailTo + '"]').parent().find(".emailInputField").removeClass("invalidValue");
        $container.find('[data-form="' + config.dataFormMapping.emailTo + '"]').parent().find(".validationMessage").hide();
        $container.find('[data-form="' + config.dataFormMapping.emailFrom + '"]').parent().find(".emailInputField").removeClass("invalidValue");
        $container.find('[data-form="' + config.dataFormMapping.emailFrom + '"]').parent().find(".validationMessage").hide();
        $container.find('[data-form="' + config.dataFormMapping.emailCC + '"]').parent().find(".emailInputField").removeClass("invalidValue");
        $container.find('[data-form="' + config.dataFormMapping.emailCC + '"]').parent().find(".validationMessage").hide();
        $container.find('[data-form="' + config.dataFormMapping.emailBCC + '"]').parent().find(".emailInputField").removeClass("invalidValue");
        $container.find('[data-form="' + config.dataFormMapping.emailBCC + '"]').parent().find(".validationMessage").hide();


        if (isEditable(config.dataFormMapping.emailTo)) {
            valid = validateEmailsInput(configuration.EmailTo, config.dataFormMapping.emailTo) && valid;
        }
        if (isEditable(config.dataFormMapping.emailCC) && configuration.EmailCC != "") {
            valid = validateEmailsInput(configuration.EmailCC, config.dataFormMapping.emailCC) && valid;
        }
        if (isEditable(config.dataFormMapping.emailBCC) && configuration.EmailBCC != "") {
            valid = validateEmailsInput(configuration.EmailBCC, config.dataFormMapping.emailBCC) && valid;
        }
        if (isEditable(config.dataFormMapping.emailFrom)) {
            valid = validateEmailsInput(configuration.EmailFrom, config.dataFormMapping.emailFrom) && valid;
        }

        if (valid && !config.multiEditMode) {
            valid = validateConfg(configuration);
        }
        return valid;
    }

    function validateSMTP(configuration) {
        var valid = true;
        if (isEditable(config.dataFormMapping.smtpServer)) {
            if (configuration.SMTPServer.ServerID == -1) {
                valid = SW.Core.SMTPServerSettingsController.validate();
            }
        }
        return valid;
    }

    function validateTimeOfDay(configuration) {
        var valid = true;
        //Some validation for current step (if needed) should be here.
        return valid;

    }

    function validateEscalation(configuration) {
        var valid = true;
        //Some validation for current step (if needed) should be here.
        return valid;
    }


    var createActionAndExecuteCallback = function (configuration, callback) {
        if (configuration.Subject == "") {
            Ext.Msg.show({
                title: '@{R=Core.Strings;K=WEBJS_VL0_1;E=js}',
                msg: '@{R=Core.Strings;K=WEBJS_VL0_2;E=js}',
                modal: true,
                closable: false,
                minWidth: 500,
                buttons: { ok: '@{R=Core.Strings;K=WEBJS_VL0_3;E=js}', cancel: "@{R=Core.Strings;K=WEBJS_VL0_4;E=js}" },
                icon: Ext.MessageBox.WARNING,
                fn: function (btn, text) {
                    if (btn == 'cancel') {
                    } else
                        callActionAndExecuteCallback("Create", configuration, callback);
                }
            });
        } else {
            callActionAndExecuteCallback("Create", configuration, callback);
        }
    };

    var updateActionAndExecuteCallback = function (action, configuration, callback) {

        var updateConfig = {
            Action: action,
            Configuration: configuration
        };
        if (configuration.Subject == "") {
            Ext.Msg.show({
                title: '@{R=Core.Strings;K=WEBJS_VL0_1;E=js}',
                msg: '@{R=Core.Strings;K=WEBJS_VL0_2;E=js}',
                modal: true,
                closable: false,
                minWidth: 500,
                buttons: { ok: '@{R=Core.Strings;K=WEBJS_VL0_3;E=js}', cancel: "@{R=Core.Strings;K=WEBJS_VL0_4;E=js}" },
                icon: Ext.MessageBox.WARNING,
                fn: function (btn, text) {
                    if (btn == 'cancel') {
                    } else
                        callActionAndExecuteCallback("Update", updateConfig, callback);
                }
            });
        } else {
            callActionAndExecuteCallback("Update", updateConfig, callback);
        }
    };

    var callActionAndExecuteCallback = function (action, param, callback) {
        SW.Core.Services.callController("/api/Email/" + action, param,
        // On success
            function (response) {
                if ($.isFunction(callback)) {
                    callback({ isError: false, actionDefinition: response });
                }
            },
        // On error
            function (msg) {
                if ($.isFunction(callback)) {
                    callback({ isError: true, ErrorMessage: msg });
                }
            }
        );
    };

    function testSmtpCreadentials() {
        var configuration = createConfiguration();
        $testSmtpSettings(configuration.EmailFrom, configuration.EmailTo);
    };

    // Public methods

    this.init = function () {
        $container = $('#' + config.containerID);

        if ($.isFunction(config.onReadyCallback)) {
            config.onReadyCallback(self);
        }
        setEnabledEditEmailField();
        $testSmtpSettings = SW.Core.SMTPServerSettingsController.testSmtpCreadentials;
        SW.Core.SMTPServerSettingsController.testSmtpCreadentials = testSmtpCreadentials;
    };

    // Validate action configuration. 
    this.validateSectionAsync = function (sectionID, callback) {
        var valid = true;
        var configuration = createConfiguration();
        if (sectionID == "recipients")
            valid = validateResipientsSection(configuration);
        else if (sectionID == "messageDetails")
            valid = validateMessageSection(configuration);
        else if (sectionID == "SmtpSection") {
            valid = validateSMTP(configuration);
        }
        else if (sectionID == "timeOfDay") {
            valid = validateTimeOfDay(configuration);
        }
        else if (sectionID == "escalation") {
            valid = validateEscalation(configuration);
        }
        if ($.isFunction(callback)) {
            callback(valid);
        }
    };
    this.getSectionAnnotation = function (sectionID) {
        var configuration = createConfiguration();
        var annotation = "";
        switch (sectionID) {
            case "recipients":
                annotation = configuration.EmailTo;
                break;
            case "messageDetails":
                annotation = configuration.Subject;
                break;
            case "SmtpSection":
                annotation = $('[data-form="smtpServerID"]').find("option[value='" + configuration.SMTPServer.ServerID + "']").text();
                if (annotation == "@{R=Core.Strings.2;K=WEBJS_OS0_7;E=js}") {
                    annotation = $('[data-form="smtpServerIP"]').val(); 
                }
                break;
            case "timeOfDay":
                annotation = "";
                break;
            case "escalation":
                annotation = "";
                break;
            default:
                annotation = "";
                break;
        }
        return annotation;
    };

    this.getActionDefinitionAsync = function (callback) {
        var configuration = createConfiguration();

        // Edit mode.
        if (config.actionDefinition) {
            // We have action definition we just want to updated existing one.
            updateActionAndExecuteCallback(config.actionDefinition, configuration, callback);

        } else {

            // New action definition mode.
            createActionAndExecuteCallback(configuration, callback);
        }
    };

    function setEnabledEditEmailField() {
        if (config.multiEditMode) {

            for (var key in config.dataFormMapping) {
                var dataform = config.dataFormMapping[key];

                //Enable/Disable Email message with radio content type
                if (dataform == config.dataFormMapping.emailMessage) {
                    $container.find(".contentType").attr("disabled", true);
                    $($container.find('[data-from="' + config.dataFormMapping.messageContentType + '"]')[0]).attr("checked", true);
                    $container.find('[data-form="' + dataform + '"]').attr("disabled", true);
                    $container.find('[data-edited="' + dataform + '"]').click(function () {
                        var dataedited = $(this).attr("data-edited");
                        $container.find('[data-form="' + dataedited + '"]').attr("disabled", !$(this).find("input").is(':checked'));
                        $container.find(".contentType").attr("disabled", !$(this).find("input").is(':checked'));
                        $container.find(".emailPriority").attr("disabled", !$(this).find("input").is(':checked'));
                        $container.find(".emailPriority select").attr("disabled", !$(this).find("input").is(':checked'));
                    });
                }
                else if ((dataform == config.dataFormMapping.emailTo) || (dataform == config.dataFormMapping.emailFrom) || (dataform == config.dataFormMapping.emailCC) || (dataform == config.dataFormMapping.emailBCC)) {
                    $container.find('[data-form="' + dataform + '"]').parent().find(".emailInputText").attr("disabled", true).css("background-color", "transparent");
                    $container.find('[data-form="' + dataform + '"]').parent().find(".emailInputField").css("background-color", "#EBEBE4");
                    $container.find('[data-edited="' + dataform + '"]').click(function () {
                        var dataedited = $(this).attr("data-edited");
                        var isChecked = $(this).find("input").is(':checked');
                        var $emailInput = $container.find('[data-form="' + dataedited + '"]').parent();
                        $emailInput.find(".emailInputText").attr("disabled", !isChecked).css("background-color", "transparent");
                        $emailInput.find(".emailInputField").css("background-color", isChecked ? "" : "#EBEBE4");
                        $emailInput.find(".emailItem").attr("disabled", !isChecked);
                    });
                } else {
                    $container.find('[data-form="' + dataform + '"]').attr("disabled", true);
                    $container.find('[data-edited="' + dataform + '"]').click(function () {
                        var dataedited = $(this).attr("data-edited");
                        $container.find('[data-form="' + dataedited + '"]').attr("disabled", !$(this).find("input").is(':checked'));
                    });
                }

            }

            //Enable/Disable Smtp
            $container.find('[data-form="smtpServerID"]').attr("disabled", true);
            $container.find('[data-form="smtpServerSettings"]').hide();

            $container.find('[data-edited="' + config.dataFormMapping.smtpServer + '"]').click(function () {
                $container.find('[data-form="smtpServerID"]').attr("disabled", !$(this).find("input").is(':checked'));
                if ($container.find('[data-form="smtpServerID"]').val() == -1) {
                    if ($(this).find("input").is(':checked')) {
                        $container.find('[data-form="smtpServerSettings"]').show();
                        $container.find('[data-form="smtpServerEnableSSL"] input[type=checkbox]').change();
                    } else {
                        $container.find('[data-form="smtpServerSettings"]').hide();
                    }
                }
            });
        }
    }

    function getMessageContentType() {
        return $container.find('[data-from="' + config.dataFormMapping.messageContentType + '"]').index($container.find('[data-from="' + config.dataFormMapping.messageContentType + '"]:checked')).toString();
    }

    this.getUpdatedActionProperies = function (callback) {
        var isSenderEmpty = false;
        var jsonProperties = new Array();
        for (var key in config.dataFormMapping) {
            var dataform = config.dataFormMapping[key];
            if ($container.find('[data-edited="' + dataform + '"] input:checked').length == 1) {
                if (dataform == config.dataFormMapping.smtpServer) {
                    if ($container.find('[data-form="smtpServerID"]').val() == -1) {
                        jsonProperties.push({ PropertyName: dataform, PropertyValue: SW.Core.SMTPServerSettingsController.createSMTPServer() });
                    } else {
                        jsonProperties.push({ PropertyName: dataform, PropertyValue: $container.find('[data-form="smtpServerID"]').val() });
                    }
                } else if (dataform == config.dataFormMapping.emailSubject) {
                    isSenderEmpty = $container.find('[data-form="' + dataform + '"]').val() == "";
                    jsonProperties.push({ PropertyName: dataform, PropertyValue: Ext.util.Format.htmlEncode($container.find('[data-form="' + dataform + '"]').val()) });
                } else if (dataform == config.dataFormMapping.emailMessage) {
                    jsonProperties.push({ PropertyName: dataform, PropertyValue: $container.find('[data-form="' + dataform + '"]').val() });
                    jsonProperties.push({ PropertyName: config.dataFormMapping.messageContentType, PropertyValue: getMessageContentType() });
                } else {
                    jsonProperties.push({ PropertyName: dataform, PropertyValue: $container.find('[data-form="' + dataform + '"]').val() });
                }
            }
        }
        if (callback) {
            if (isSenderEmpty) {
                Ext.Msg.show({
                    title: '@{R=Core.Strings;K=WEBJS_VL0_1;E=js}',
                    msg: '@{R=Core.Strings;K=WEBJS_VL0_2;E=js}',
                    modal: true,
                    closable: false,
                    minWidth: 500,
                    buttons: { ok: '@{R=Core.Strings;K=WEBJS_VL0_3;E=js}', cancel: "@{R=Core.Strings;K=WEBJS_VL0_4;E=js}" },
                    icon: Ext.MessageBox.WARNING,
                    fn: function (btn, text) {
                        if (btn == 'cancel') {
                        } else
                            callback(JSON.stringify(jsonProperties));
                    }
                });
            } else {
                callback(JSON.stringify(jsonProperties));
            }

        }
    };

    // Constructor

    var self = this;
    var $container = null;
    var $testSmtpSettings = null;
}
