﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.Orion.Core.Actions.DAL;
using SolarWinds.Orion.Core.Actions.Impl.CustomProperty;
using SolarWinds.Orion.Core.Alerting.Plugins.Conditions.Dynamic;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.InformationService;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Models.Actions;
using SolarWinds.Orion.Core.Models.Alerting;
using SolarWinds.Orion.Web.Actions;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.InformationService;
using Entity = SolarWinds.Orion.Core.Models.Alerting.Entity;
using InformationServiceProxy = SolarWinds.Orion.Web.InformationService.InformationServiceProxy;

public partial class Orion_Actions_Controls_CustomPropertyView : ActionPluginBaseView
{
    private List<CustomProperty> _customProperties;
    private List<CustomProperty> _parentCustomProperties;
    private IEntityProvider _entityProvider;

    public IEntityProvider EntityProvider
    {
        get
        {
            if (_entityProvider != null) return _entityProvider;
            var swisSchemaProvider = new SwisSchemaProvider(new InformationServiceProxyCreator(InformationServiceProxy.CreateV3));
            _entityProvider = new EntityProviderDynamic(swisSchemaProvider);
            return _entityProvider;
        }
    }

    public bool HasError { get; set; }

    public string EntityType { get; set; }

    public string ParentEnityType { get; set; }

    public List<CustomProperty> CustomProperties
    {
        get { return _customProperties ?? (_customProperties = PopulateCustomProperties(EntityType)); }
    }

    public List<CustomProperty> ParentCustomProperties
    {
        get { return _parentCustomProperties ?? (_parentCustomProperties = PopulateCustomProperties(ParentEnityType)); }
    }

    private void SetDefaultEntityType()
    {
        if ((AlertingViewContext == null || AlertingViewContext.EntityType == null) && ActionDefinition == null)
        {
            return;
        }
        
        var objectType = AlertingViewContext != null && AlertingViewContext.EntityType != null
                        ? AlertingViewContext.EntityType
                        : ActionDefinition.Properties.GetPropertyValue(CustomPropertyConstants.ObjectTypeKey);

        EntityType = EntityProvider.GetEntityByObjectType(objectType).ObjectType;
        if (string.IsNullOrEmpty(EntityType)) return;
        var parentEntity = GetParentEntity(EntityType);
        if (parentEntity != null)
        {
            ParentEnityType = parentEntity.ObjectType;
        }
    }

    private List<CustomProperty> PopulateCustomProperties(string objectType)
    {
        if (string.IsNullOrEmpty(objectType))
        {
            return new List<CustomProperty>();
        }
        var entity = EntityProvider.GetEntityByObjectType(objectType);

        string targetEntity;
        try
        {
            targetEntity = CustomPropertyHelper.GetCustomPropertyTarget(entity.FullName, true);
        }
        catch
        {
            // unable to get target entity, try to get CPs by source entity
            return CustomPropertyMgr.GetCustomPropertiesForSourceEntity(entity.FullName).ToList();
        }
        return CustomPropertyMgr.GetCustomPropertiesForEntity(targetEntity).ToList();
    }

    private Entity GetParentEntity(string objectType)
    {
        var entity = EntityProvider.GetEntityByObjectType(objectType);
        var swisEntityName = EntityHelper.GetSwisEntityParent(entity.FullName);
        var parentEntity = EntityProvider.GetEntityByObjectType(EntityProvider.GetObjectTypeByEntityFullName(swisEntityName));
        return parentEntity != null && !string.Equals(parentEntity.ObjectType, objectType) 
                    ? parentEntity 
                    : null;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ApplyMultiEditMode();
        if (!MultiEditEnabled)
        {            
            if (ActionDefinition == null)
            {
                BindDefaultConfiguration();
            }
            else //edit
            {
                BindConfiguration();
            }
        }
        else
        {
            BindMultiEditConfiguration();
        }
    }

    private void BindDefaultConfiguration()
    {
        if (AlertingViewContext == null)
        {
            return;
        }
        try
        {
            SetDefaultEntityType();
        }
        catch (Exception ex)
        {
            ShowNoCustomPropertiesErrorMsg();
            return;
        }
        if (!CustomProperties.Any() && !ParentCustomProperties.Any())
        {
            ShowNoCustomPropertiesErrorMsg();
            return;
        }
        CustomPropertyBinding();
        ParentCustomPropertyBinding();
        SetDefaultSectionVisibility();
        SetDefaultCp();
    }

    private void BindConfiguration()
    {

        var objectType = ActionDefinition.Properties.GetPropertyValue(CustomPropertyConstants.ObjectTypeKey);
        objectIdTypeHd.Value = objectType;
        EntityType = objectType;

        var activeObjectType = ActionDefinition.Properties.GetPropertyValue(CustomPropertyConstants.ActiveObjectTypeKey);
        activeObjectType = activeObjectType ?? objectType;
        activeObjectTypeHd.Value = activeObjectType;

        var parentObjectType = ActionDefinition.Properties.GetPropertyValue(CustomPropertyConstants.ParentObjectTypeKey);
        if (string.IsNullOrEmpty(parentObjectType))
        {
            var parentEntity = GetParentEntity(objectType);
            parentObjectType = parentEntity != null ? parentEntity.ObjectType : null;
        }
        parentObjectTypeHd.Value = parentObjectType;
        ParentEnityType = parentObjectType;

        var definedPropertyName = ActionDefinition.Properties.GetPropertyValue(CustomPropertyConstants.NameKey);
        string selectedPropertyName;

        if (!CustomProperties.Any() && !ParentCustomProperties.Any())
        {
            ShowNoCustomPropertiesErrorMsg();
            return;
        }

        CustomPropertyBinding();
        ParentCustomPropertyBinding();

        SetCpSectionVisibility(CustomProperties.Any());
        SetPcpSectionVisibility(ParentCustomProperties.Any());

        if (!string.IsNullOrEmpty(activeObjectType) && string.Equals(parentObjectType, activeObjectType, StringComparison.InvariantCultureIgnoreCase))
        {
            if (ParentCustomProperties.Any())
            {
                selectedPropertyName = ParentCustomPropertyNameBinding(definedPropertyName);
                SetCpSectionChecked(false);
                SetPcpSectionChecked(true);
            }
            else
            {
                activeObjectType = objectType;
                selectedPropertyName = CustomPropertyNameBinding(definedPropertyName);
                SetCpSectionChecked(true);
                SetPcpSectionChecked(false);
            }
        }
        else
        {
            if (CustomProperties.Any())
            {
                selectedPropertyName = CustomPropertyNameBinding(definedPropertyName);
                SetCpSectionChecked(true);
                SetPcpSectionChecked(false);
            }
            else
            {
                activeObjectType = parentObjectType;
                selectedPropertyName = ParentCustomPropertyNameBinding(definedPropertyName);
                SetCpSectionChecked(false);
                SetPcpSectionChecked(true);
            }
        }
        SetDefaultRadioVisibility();
        var propertyValue = selectedPropertyName.Equals(definedPropertyName)
            ? ActionDefinition.Properties.GetPropertyValue(CustomPropertyConstants.ValueKey)
            : string.Empty;
        EditCp.SetUpProperties(activeObjectType, selectedPropertyName, propertyValue);
        EditCp.SetUpConfiguration();
    }

    private void BindMultiEditConfiguration()
    {
        var actionIds = ActionIds.Cast<int>().ToArray();
        IEnumerable<ActionDefinition> actionDefenitions;
        var creator = InformationServiceProxy.CreateV3Creator();
        IActionsDAL actionsDAL = new ActionsDAL(creator);
        actionDefenitions = actionsDAL.LoadActions(actionIds).ToArray();
        var objectTypes = actionDefenitions.Select(a => a.Properties.GetPropertyValue(CustomPropertyConstants.ObjectTypeKey)).ToArray();
        var objectType = objectTypes.First();
        if (objectTypes.All(o => string.Equals(o, objectType, StringComparison.InvariantCultureIgnoreCase)))
        {
            EntityType = objectType;
            var parentEntity = GetParentEntity(objectType);
            ParentEnityType = parentEntity != null ? parentEntity.ObjectType : null;

            if (!CustomProperties.Any() && !ParentCustomProperties.Any())
            {
                ShowNoCustomPropertiesErrorMsg();
                return;
            }

            CustomPropertyBinding();
            ParentCustomPropertyBinding();
            SetDefaultSectionVisibility();
            SetDefaultCp();
        }
        else
        {
            ShowDifferentCustomPropertiesTypeErrorMsg();
        }
    }

    private void SetDefaultCp()
    {
        if (CustomProperties.Any())
        {
            EditCp.SetUpProperties(EntityType, customProperyList.SelectedItem.Value, string.Empty);
            EditCp.SetUpConfiguration();
            activeObjectTypeHd.Value = EntityType;
            return;
        }

        if (!ParentCustomProperties.Any()) return;
        EditCp.SetUpProperties(ParentEnityType, parentCustomProperyList.SelectedItem.Value, string.Empty);
        EditCp.SetUpConfiguration();
        activeObjectTypeHd.Value = ParentEnityType;
    }

    private void SetDefaultSectionVisibility()
    {
        SetCpSectionVisibility(CustomProperties.Any());
        SetPcpSectionVisibility(ParentCustomProperties.Any());

        if (CustomProperties.Any())
        {
            SetCpSectionChecked(true);
            SetPcpSectionChecked(false);
        }
        else if (ParentCustomProperties.Any())
        {
            SetCpSectionChecked(false);
            SetPcpSectionChecked(true);
        }
        SetDefaultRadioVisibility();
    }

    private void SetDefaultRadioVisibility()
    {
        if (CustomProperties.Any() && !ParentCustomProperties.Any())
        {
            customPropertyRadio.Visible = false;
        }
        if (!CustomProperties.Any() && ParentCustomProperties.Any())
        {
            parentCustomPropertyRadio.Visible = false;
        }
    }

    private void SetCpSectionVisibility(bool state)
    {
        customPropertySection.Visible = state;
        customProperyList.Visible = state;
        customPropertyRadio.Visible = state;
    }

    private void SetCpSectionChecked(bool state)
    {
        customPropertyRadio.Checked = state;
        customProperyList.Enabled = state;
    }

    private void SetPcpSectionVisibility(bool state)
    {
        parentCustomPropertySection.Visible = state;
        parentCustomProperyList.Visible = state;
        parentCustomPropertyRadio.Visible = state;
        parentCustomPropertyHint.Visible = state;
    }

    private void SetPcpSectionChecked(bool state)
    {
        parentCustomPropertyRadio.Checked = state;
        parentCustomProperyList.Enabled = state;
    }

    private string CustomPropertyNameBinding(string propertyName)
    {
        var propName = propertyName;
        if (CustomProperties.Any(cp => string.Equals(cp.PropertyName, propName, StringComparison.InvariantCultureIgnoreCase)))
        {
            customProperyList.SelectedValue = propertyName;
        }
        else
        {
            propertyName = customProperyList.SelectedItem.Value;
            ShowInvalidCustomPropertyMessage(propName);
        }
        return propertyName;
    }

    private string ParentCustomPropertyNameBinding(string propertyName)
    {
        var propName = propertyName;
        if (ParentCustomProperties.Any(cp => string.Equals(cp.PropertyName, propName, StringComparison.InvariantCultureIgnoreCase)))
        {
            parentCustomProperyList.SelectedValue = propertyName;
        }
        else
        {
            propertyName = parentCustomProperyList.SelectedItem.Value;
            ShowInvalidCustomPropertyMessage(propName);
        }
        return propertyName;
    }

    private void ApplyMultiEditMode()
    {
        CustomPropertyEditEnabled.Visible = MultiEditEnabled;
    }

    private void CustomPropertyBinding()
    {
        objectIdTypeHd.Value = EntityType;
        if (!CustomProperties.Any()) return;
        customProperyList.DataTextField = "PropertyName";
        customProperyList.DataValueField = "PropertyName";
        customProperyList.DataSource = CustomProperties;
        customProperyList.DataBind();
    }

    private void ParentCustomPropertyBinding()
    {
        if (!ParentCustomProperties.Any()) return;
        parentCustomProperyList.DataTextField = "PropertyName";
        parentCustomProperyList.DataValueField = "PropertyName";
        parentCustomProperyList.DataSource = ParentCustomProperties;
        parentCustomProperyList.DataBind();
        parentObjectTypeHd.Value = ParentEnityType;
    }

    private void ShowNoCustomPropertiesErrorMsg()
    {
        noCustomPropertiesErrorMsg.Visible = true;
        container.Visible = false;
        IsActionSupported = false;
    }

    private void ShowDifferentCustomPropertiesTypeErrorMsg()
    {
        differentCustomPropertiesTypeErrorMsg.Visible = true;
        HasError = true;
    }

    private void ShowInvalidCustomPropertyMessage(string propertyName)
    {
        InvalidCustomPropertyMessage.Visible = true;
        InvalidCustomPropertyMessageText.Text = string.Format(Resources.CoreWebContent.WEBCODE_VL0_1, propertyName);
    }

    public override string ActionTypeID
    {
        get { return CustomPropertyConstants.ActionTypeId; }
    }
}