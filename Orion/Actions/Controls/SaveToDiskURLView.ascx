<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SaveToDiskURLView.ascx.cs" Inherits="Orion.Actions.Controls.SaveToDiskURLView" %>
<orion:Include runat="server" File="Actions/js/SaveToDiskController.js" />

<style type="text/css">
    .section { min-width: 600px; }
    .paragraph { padding-top: 10px; }
</style>

<div runat="server" id="container" class="section">
    <div style="padding: 10px">
        <div>
            <div class="paragraph"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_60) %></div>
            <asp:TextBox runat="server" Width="82%" ID="SavePath" data-form="savePath"/>
            <orion:LocalizableButton ID="txtMessageInsertVariable" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IT0_1 %>"
                 DisplayType="Small" OnClientClick="return false" data-macro="savePath" runat="server"/>
        </div>
        
        <div style="padding-bottom: 5px; padding-top: 10px;">
            <input id="checkPrintable" runat="server" type="checkbox" data-form="printable" />
            <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_326) %></label>
            <img title="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_327) %>" alt="" src="/Orion/images/Icon.Info.gif" />
        </div>

        <div style="padding-left: 10px; padding-bottom: 10px;" >
        <div>
            <div class="paragraph"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_59) %></div>
            <asp:TextBox runat="server" ID="UserName" Width="300" data-form="userName"/>
            <div class="windowsUserName">
            <span class="hintText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_AK0_166) %></span> 
            </div>
        </div>
        
        <div>
            <div class="paragraph"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK1_8) %></div>
            <asp:TextBox ID="Password" TextMode="Password" runat="server" data-form="password"/>
        </div>

        <div style="padding-top: 5px">
            <span id="testProgress" style="display: none;"><img src="/Orion/images/AJAX-Loader.gif" alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_11) %>"/><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_11) %></span>
            <div id="testResult"></div>
            <orion:LocalizableButton runat="server" ID="btnTestCredentials" Text= "<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_62 %>" OnClientClick="SW.Core.Actions.SaveToDiskSettingsController.testUserCredentials(); return false;" DisplayType="Small" />
            </div>
        </div>
        <h4><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_33) %></h4>
        <div style="float: left;">
            <input id="checkPdf" runat="server" type="checkbox" data-form="contentType" enum-value="PDF"/>
            <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TD0_5) %><% if (DisablePdf) { %><img title="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_94) %>" alt="" src="/Orion/images/Icon.Info.gif" /><% } %></label>
                          <input id="checkCsv" runat="server" type="checkbox" data-form="contentType" enum-value="CSV"/>
                          <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TD0_6) %></label>
                          <input id="checkExcel" runat="server" type="checkbox" data-form="contentType" enum-value="Excel"/>
                          <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TD0_7) %></label>
                          </div>
                          <div style="clear: both;"></div>
                          <span id="checkboxValidation" style="display: none; color: red;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_47) %></span>
    </div>
</div>

<script type="text/javascript">
    // Self executing function creates local scope to avoid globals.
    (function() {
        SW.Core.Actions.SaveToDiskSettingsController = new SW.Core.Actions.SaveToDiskController({
            containerID : '<%= container.ClientID %>', 
            onReadyCallback : <%= OnReadyJsCallback %>,
            actionDefinition : <%= ToJson(ActionDefinition, true) %>,
            viewContext : <%= ToJson(ViewContext, true) %>
        });
        SW.Core.Actions.SaveToDiskSettingsController.init();
        SW.Core.MacroVariablePickerController.BindPicker();
    })();
</script>