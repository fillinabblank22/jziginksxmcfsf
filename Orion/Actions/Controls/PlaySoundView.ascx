<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PlaySoundView.ascx.cs" Inherits="Orion.Actions.Controls.PlaySoundView" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Actions.Impl.PlaySound" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Actions.Impl.TextToSpeech" %>
<orion:Include runat="server" File="Actions/js/PlaySoundController.js" />

<style type="text/css">
    #playSoundSection { padding: 10px; width: 500px; }
    #playSoundSection .helpfulFlow { padding-bottom: 10px; }
</style>

 <div runat="server" id="container">
     <h3><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB3_4) %></h3>
    <div id="playSoundSection" class="section required">
        <div class="helpfulFlow">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_335) %>
            <%= DefaultSanitizer.SanitizeHtml(TextToSpeechConstants.LinkToInstaller) %>
        </div>
        <div>
            <asp:CheckBox ID="chbMessageEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: PlaySoundConstants.MessagePropertyKey %>"/>
            <asp:Label ID="lblMessage" runat="server" Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_AB0_91 %>" AssociatedControlID="txtMessage" />
            <div>
            <asp:TextBox runat="server" Width="80%" ID="txtMessage" TextMode="MultiLine" Rows="10" data-form="<%$ HtmlEncodedCode: PlaySoundConstants.MessagePropertyKey %>"/>
            <orion:LocalizableButton ID="AlertMessageInsertVariable" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IT0_1 %>"
                  DisplayType="Small" OnClientClick="return false" data-macro="<%$ HtmlEncodedCode: PlaySoundConstants.MessagePropertyKey %>" runat="server"/>
                  </div>
        </div>
     </div>
 </div>

 <script type="text/javascript">
    // Self executing function creates local scope to avoid globals.
    (function() {
        SW.Core.Actions.PlaySoundSettingsController = new SW.Core.Actions.PlaySoundController({
            containerID : '<%= container.ClientID %>', 
            onReadyCallback : <%= OnReadyJsCallback %>,
            actionDefinition :  <%= ToJson(ActionDefinition, true) %>,
            viewContext :  <%= ToJson(ViewContext, true) %>,
            multiEditMode: <%= MultiEditEnabled.ToString().ToLower() %>,
            dataFormMapping : {
                message: "<%= PlaySoundConstants.MessagePropertyKey %>"
            }
        });
        SW.Core.Actions.PlaySoundSettingsController.init();
    })();
 </script>