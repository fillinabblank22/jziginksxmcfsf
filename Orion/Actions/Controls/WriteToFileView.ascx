<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WriteToFileView.ascx.cs" Inherits="Orion_Actions_Controls_WriteToFileView" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Actions.Impl.WriteToFile" %>
<%@ Register TagPrefix="orion" TagName="WindowsCredentialSelectorControl" Src="~/Orion/Controls/WindowsCredentialSelectorControl.ascx" %>

<orion:Include runat="server" File="Actions/js/WriteToFileController.js" />

<style type="text/css">
    #writeToFileSection { padding: 10px; }
    #writeToFileSection .filePathInput, #writeToFileSection .messageInput { width: 550px; }
    #writeToFileSection .messageInput { height: 100px; }
    #writeToFileSection .messageSection, #writeToFileSection .fileSizeSection { margin-top: 10px; }
    #writeToFileSection .fileSizehInput{ width: 80px; }
    #writeToFileSection .helpfulText { color: #5f5f5f; font-size: 11px; padding: 5px 0px;}
    #requiredFilePath { color: red; }
</style>

<div runat="server" id="container">
    <h3><%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_TM0_333) %></h3>
    <div id="writeToFileSection" class="section required">
        <div>
            <div>
                <input type="checkbox" runat="server" ID="chFilePath" data-edited="<%$ HtmlEncodedCode: WriteToFileConstants.FilePathPropertyKey %>"/>
                <asp:Label ID="labelPath" runat="server" AssociatedControlID="txtFilePath" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_332 %>" />
            </div>
            <asp:TextBox ID="txtFilePath" Width="82%" CssClass="filePathInput" runat="server" 
                         data-form="<%$ HtmlEncodedCode: WriteToFileConstants.FilePathPropertyKey %>"/>
            <orion:LocalizableButton ID="FilePathInsertVariable" LocalizedText="CustomText" 
                         Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IT0_1 %>" DisplayType="Small" runat="server" 
                         data-macro ="<%$ HtmlEncodedCode: WriteToFileConstants.FilePathPropertyKey %>" OnClientClick="return false"/>
            <div id="requiredFilePath" class="error-message" style="display: none;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WriteToFile_WriteToFileFailed) %></div>
            <div class="hintText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB3_9) %></div>
        </div>
        
        <div class="fileSizeSection">
            <div>
                <input type="checkbox" runat="server" ID="chFileSize" data-edited="<%$HtmlEncodedCode:WriteToFileConstants.FileSizeMbPropertyKey %>"/>
                <asp:label ID="labelFileSize" runat="server" AssociatedControlID="txtFileSize" text="<%$HtmlEncodedResources: CoreWebContent, WEBDATA_VL0_52 %>" />
            </div>
            <asp:TextBox runat="server" ID="txtFileSize" CssClass="fileSizehInput" data-form="<%$ HtmlEncodedCode: WriteToFileConstants.FileSizeMbPropertyKey %>"></asp:TextBox>
            <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_54) %></span>
            <br/>
            <span class="helpfulText" style="margin-top:10px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_53) %></span>
        </div>
                
        <div class="messageSection">
            <div>
                <input type="checkbox" runat="server" ID="chMessage" data-edited="<%$ HtmlEncodedCode: WriteToFileConstants.MessagePropertyKey %>"/>
                <asp:Label runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_150 %>" AssociatedControlID="txtMessage" ID="labelMessage"/>
            </div>
            <asp:TextBox ID="txtMessage" CssClass="messageInput" runat="server" TextMode="MultiLine" 
                         data-form="<%$ HtmlEncodedCode: WriteToFileConstants.MessagePropertyKey %>"/>
        </div>
        <div>
             <orion:LocalizableButton ID="txtMessageInsertVariable" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IT0_1 %>"
                  DisplayType="Small" OnClientClick="return false" data-macro="<%$ HtmlEncodedCode: WriteToFileConstants.MessagePropertyKey %>" runat="server"/>
        </div>

        <orion:WindowsCredentialSelectorControl ID="credentials" runat="server"/>  
    </div>
</div>

<script type="text/javascript">
    (function() {
        var controller = new SW.Core.Actions.WriteToFileController({
            containerID : '<%= container.ClientID %>', 
            onReadyCallback : <%= OnReadyJsCallback %>,
            actionDefinition :  <%= ToJson(ActionDefinition, true) %>,
            viewContext :  <%= ToJson(ViewContext, true) %>,
            multiEditMode: <%= MultiEditEnabled.ToString().ToLower() %>, 
            dataFormMapping : {
                filePath: '<%= WriteToFileConstants.FilePathPropertyKey %>',
                message: '<%= WriteToFileConstants.MessagePropertyKey %>',
                fileSizeMb: '<%= WriteToFileConstants.FileSizeMbPropertyKey %>'
            }
        });
        controller.init();
    })();
</script>
