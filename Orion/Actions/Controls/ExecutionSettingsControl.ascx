<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExecutionSettingsControl.ascx.cs" Inherits="Orion_Actions_Controls_BaseActionPluginsContainer" %>
<orion:Include runat="server" File="Actions/js/ExecutionSettingsController.js" />

<style type="text/css">
    .executionSettings_checkbox {margin: 5px;}

    .executionSettings_timePeriodType {margin-left: 5px;}

    .executionSettings_repeatTimeSpan input {width: 40px;}

</style>

<div id="ExecutionSettingsPlugin" class="hidden section">
    <h3 class="hintContentInfo"><span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_OM0_11) %></span> <span class="contentInfo"></span></h3>
    <div>
        <div>
            <div class="executionSettings_checkbox">
                <asp:CheckBox ID="executionIfAknowledgeEnabled" data-edited="executionIfAknowledgeEnabled" runat="server" />
                <span class="executionSettings_checkbox">
                    <asp:CheckBox data-form="executionIfAknowledge" runat="server" ID="cbAknowledgExecuteEnable" Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_OM0_6 %>" />
                </span>
            </div>
            <div class="executionSettings_checkbox" style="padding-left: 20px">
            </div>
            <div class="executionSettings_checkbox">
                <asp:CheckBox ID="TimeSpanToEditEnabled" runat="server" data-edited="executionRepeatTimeSpan" />
                <span class="executionSettings_checkbox">
                    <asp:CheckBox data-form="executionRepeatTimeSpanEnable" data-edited="executionRepeatTimeSpanEnable" runat="server" ClientIDMode="Static" ID="cbRepeatActionEnable" Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_OM0_7 %>" />
                </span>
            </div>
            <div class="executionSettings_checkbox" style="padding-left: 20px">
            </div>
        </div>

        <div>
            <div id="waitContainer">
                <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_OM0_8) %></span>
                <span class="executionSettings_repeatTimeSpan">
                    <orion:ValidatedTextBox data-form="executionRepeatTimeSpan" Type="Integer" runat="server" MaxLength="3" ID="tbTimeToExecute" />
                </span>
                <span class="executionSettings_timePeriodType" data-form="executionTimeSpanType">
                    <asp:DropDownList runat="server" ID="ddTimePeriodType">
                        <asp:ListItem Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_YK0_40 %>" Value="Minutes"></asp:ListItem>
                        <asp:ListItem Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_YK0_41 %>" Value="Hours"></asp:ListItem>
                        <asp:ListItem Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_YK0_42 %>" Value="Days"></asp:ListItem>
                    </asp:DropDownList>
                </span>
            </div>
        </div>
    </div>
</div>


