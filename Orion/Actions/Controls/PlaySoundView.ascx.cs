﻿using System;
using SolarWinds.Orion.Core.Actions.Impl.PlaySound;
using SolarWinds.Orion.Web.Actions;

namespace Orion.Actions.Controls
{
    public partial class PlaySoundView : ActionPluginBaseView
    {
        public override string ActionTypeID
        {
            get { return PlaySoundConstants.ActionTypeID; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ApplyMultiEditMode();
            // Edit Mode
            if (ActionDefinition != null)
            {
                // Edit Mode
                BindConfiguration();
            }
            else
            {
                // New mode
                SetDefaultValues();
            }
        }

        private void ApplyMultiEditMode()
        {
            chbMessageEditEnabled.Visible = MultiEditEnabled;
            if (MultiEditEnabled)
                lblMessage.Attributes.Add("style", "font-weight: bold;");
        }

        private void SetDefaultValues()
        {
            if (AlertingViewContext != null)
            {
                //set default values
            }
        }

        private void BindConfiguration()
        {
            txtMessage.Text = ActionDefinition.Properties.GetPropertyValue(PlaySoundConstants.MessagePropertyKey);
        }
    }
}