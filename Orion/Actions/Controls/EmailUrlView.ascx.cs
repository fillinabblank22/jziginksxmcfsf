﻿using System;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Orion.Core.Actions.Impl.Email;
using SolarWinds.Orion.Core.Actions.Impl.EmailWebPage;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Models.Actions;
using SolarWinds.Orion.Web.Actions;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web.Helpers;
using WebSettingsDAL = SolarWinds.Orion.Web.DAL.WebSettingsDAL;

public partial class Orion_Actions_Controls_EmailUrlView : ActionPluginBaseView
{
    public override string ActionTypeID
    {
        get { return IsEmailWebPage ? EmailWebPageConstants.ActionTypeID : EmailUrlConstants.ActionTypeID; }
    }

    public bool DisablePdf
    {
        get;
        set;
    }

    public bool IsEmailWebPage
    {
        get { return ViewContext.EnviromentType == ActionEnviromentType.Alerting; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        checkPdf.Disabled = DisablePdf = PDFExportHelper.IsPDFExportFIPSIncompatible();
        ApplyMultiEditMode();
        PopulateImportanceDDL();

        if (IsEmailWebPage)
        {
            reportingMessageDetails.Visible = false;
        }
        else
        {
            alertingMessageDetails.Visible = false;
        }

        if (ActionDefinition != null) //edit
        {
            BindConfiguration();
        }
        else //new
        {
            SetDefaultValues();
        }
    }

    private void PopulateImportanceDDL()
    {
        ListItem item = new ListItem(CoreWebContent.WEBDATA_SO0_231, "0");
        emailPriority.Items.Add(item);
        item = new ListItem(CoreWebContent.WEBDATA_SO0_230, "1");
        emailPriority.Items.Add(item);
        item = new ListItem(CoreWebContent.WEBDATA_SO0_232, "2");
        emailPriority.Items.Add(item);
    }

    private void ApplyMultiEditMode()
    {
        EmailToEditEnabled.Visible = MultiEditEnabled;
        WebPageURLEditEnabled.Visible = MultiEditEnabled;
        EmailCcEditEnabled.Visible = MultiEditEnabled;
        EmailBccEditEnabled.Visible = MultiEditEnabled;
        EmailSenderNameEditEnabled.Visible = MultiEditEnabled;
        EmailSenderFromEditEnabled.Visible = MultiEditEnabled;
        EmailSubjectEditEnabled.Visible = MultiEditEnabled;
        EmailSmtpEditEnabled.Visible = MultiEditEnabled;
        UserIDEditEnabled.Visible = MultiEditEnabled;

        if (MultiEditEnabled)
        {
            divBcc.Attributes.Add("style", "font-weight: bold;");
            divCC.Attributes.Add("style", "font-weight: bold;");
            divEmailSender.Attributes.Add("style", "font-weight: bold;");
            divEmailSenderFrom.Attributes.Add("style", "font-weight: bold;");
            divEmailSubject.Attributes.Add("style", "font-weight: bold;");
            divEmailTo.Attributes.Add("style", "font-weight: bold;");
            divWebPageURL.Attributes.Add("style", "font-weight: bold;");
        }
    }

    private void SetDefaultValues()
    {
        var emailTo = WebSettingsDAL.GetValue(EmailConstants.DefaultEmailTo, string.Empty);
        txtEmailTo.Value = emailTo;

        var emailFrom = WebSettingsDAL.GetValue(EmailConstants.DefaultEmailFrom, EmailConstants.DefaultEmailFromValue);
        txtEmailFrom.Value = emailFrom;

        var emailSenderName = WebSettingsDAL.GetValue(EmailConstants.DefaultEmailSenderName, EmailConstants.DefaultEmailSenderNameValue);
        nameOfSender.Text = emailSenderName;

        var emailCc = WebSettingsDAL.GetValue(EmailConstants.DefaultEmailCc, string.Empty);
        ChangeCcAttributes(emailCc);
        txtCC.Value = emailCc;

        var emailBcc = WebSettingsDAL.GetValue(EmailConstants.DefaultEmailBcc, string.Empty);
        ChangeBccAttributes(emailBcc);
        txtBCC.Value = emailBcc;


        if (IsEmailWebPage)
        {
            userAccountControl.setAccount(this.Profile.UserName);
        }
        radioHtml.Checked = true;
        //set default values
        checkPdf.Checked = !DisablePdf;
    }

    private void BindConfiguration()
    {
        txtEmailTo.Value = ActionDefinition.Properties.GetPropertyValue(EmailConstants.EmailToPropertyKey);
        txtEmailSubject.Text = ActionDefinition.Properties.GetPropertyValue(EmailConstants.SubjectPropertyKey);
        nameOfSender.Text = ActionDefinition.Properties.GetPropertyValue(EmailConstants.SenderPropertyKey);
        txtEmailFrom.Value = ActionDefinition.Properties.GetPropertyValue(EmailConstants.EmailFromPropertyKey);
        emailPriority.SelectedValue = ActionDefinition.Properties.GetPropertyValue(EmailConstants.Priority);

        ChangeCcAttributes(ActionDefinition.Properties.GetPropertyValue(EmailConstants.EmailCCPropertyKey));
        txtCC.Value = ActionDefinition.Properties.GetPropertyValue(EmailConstants.EmailCCPropertyKey);

        ChangeBccAttributes(ActionDefinition.Properties.GetPropertyValue(EmailConstants.EmailBCCPropertyKey));
        txtBCC.Value = ActionDefinition.Properties.GetPropertyValue(EmailConstants.EmailBCCPropertyKey);

        if(IsEmailWebPage)
        {
            txtEmailPageURL.Text = ActionDefinition.Properties.GetPropertyValue(EmailWebPageConstants.WebPageURLPropertyKey);
            userAccountControl.setAccount(ActionDefinition.Properties.GetPropertyValue(EmailWebPageConstants.UserIDPropertyKey));
        }
        txtEmailMessage.Text = ActionDefinition.Properties.GetPropertyValue(EmailConstants.EmailMessagePropertyKey);
        string messageContentType = (string)OrionSerializationHelper.FromJSON(ActionDefinition.Properties.GetPropertyValue(EmailConstants.MessageContentTypePropertyKey), typeof(string));

        if (messageContentType != null)
        {
            MessageContentType messageContentTypeEnum = (MessageContentType) Enum.Parse(typeof (MessageContentType), messageContentType);

            switch (messageContentTypeEnum)
            {
                case MessageContentType.HTML:
                    radioHtml.Checked = true;
                    break;
                case MessageContentType.PlainText:
                    radioPlainText.Checked = true;
                    break;
            }
        }
        else
        {
            radioPlainText.Checked = true;
        }

        if (ViewContext.EnviromentType == ActionEnviromentType.Reporting)
        {
            checkPrintable.Checked = Convert.ToBoolean(ActionDefinition.Properties.GetPropertyValue(EmailUrlConstants.PrintablePropertyKey));

            string[] enumStringsArray = (string[])OrionSerializationHelper.FromJSON(ActionDefinition.Properties.GetPropertyValue(EmailUrlConstants.ContentTypePropertyKey), typeof(string[]));

            if (enumStringsArray != null)
            {
                EmailContent[] content = new EmailContent[enumStringsArray.Length];

                for (int i = 0; i < enumStringsArray.Length; i++)
                {
                    content[i] = (EmailContent) Enum.Parse(typeof (EmailContent), enumStringsArray[i]);
                }

                foreach (EmailContent type in content)
                {
                    switch (type)
                    {
                        case EmailContent.CSV:
                            checkCsv.Checked = true;
                            break;
                        case EmailContent.PDF:
                            checkPdf.Checked = !DisablePdf;
                            break;
                        case EmailContent.HTML:
                            checkHtml.Checked = true;
                            break;
                        case EmailContent.Link:
                            checkLink.Checked = true;
                            break;
                        case EmailContent.Excel:
                            checkExcel.Checked = true;
                            break;
                    }
                }
            }
        }
        var smtpServerID = ActionDefinition.Properties.GetPropertyValue(EmailConstants.SmtpServerIDPropertyKey);
        SmtpSettings.SmtpServer = smtpServerID != EmailConstants.DefaultSMTPServerID
            ? new SmtpServer { ServerID = Convert.ToInt16(smtpServerID) }
            : null;
    }

    private void ChangeCcAttributes(string property)
    {
        if (!string.IsNullOrEmpty(property))
        {
            ccContainer.Attributes["style"] = "display:block";
            btnCC.Attributes["style"] = "display:none";
        }
        else
        {
            ccContainer.Attributes["style"] = "display:none";
            btnCC.Attributes["style"] = "display:inline-block";
        }
    }

    private void ChangeBccAttributes(string property)
    {
        if (!string.IsNullOrEmpty(property))
        {
            bccContainer.Attributes["style"] = "display:block";
            btnBCC.Attributes["style"] = "display:none";
        }
        else
        {
            bccContainer.Attributes["style"] = "display:none";
            btnBCC.Attributes["style"] = "display:inline-block";
        }
    }
}
