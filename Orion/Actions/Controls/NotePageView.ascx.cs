﻿using System;
using SolarWinds.Orion.Core.Actions.Impl.NotePage;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.Actions;

namespace Orion.Actions.Controls
{
    public partial class NotePageView : ActionPluginBaseView
    {
        public override string ActionTypeID
        {
            get { return NotePageConstants.ActionTypeID; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(InvokeActionMethod(PrimaryEngineID,NotePageConstants.FindNotePageExecutableMethodName, string.Empty)))
            {
                notePageNotInstalled.Visible = true;
                notePageSettings.Visible = false;
                IsActionSupported = false;
            }
            else
            {
                notePageNotInstalled.Visible = false;

                ApplyMultiEditMode();
                LoadRecipients();
                // Edit Mode
                if (ActionDefinition != null)
                {
                    BindConfiguration();
                }
                else // New mode
                {
                    SetDefaultValues();
                }
            }
        }

        private void ApplyMultiEditMode()
        {
            recipientsEditEnabled.Visible = MultiEditEnabled;
            fromEditEnabled.Visible = MultiEditEnabled;
            messageEditEnabled.Visible = MultiEditEnabled;

            if (MultiEditEnabled)
            {
                divFrom.Attributes.Add("style", "font-weight: bold;");
                divRecepients.Attributes.Add("style", "font-weight: bold;");
                divMessage.Attributes.Add("style", "font-weight: bold;");
            }
        }

        private void SetDefaultValues()
        {
            if (ReportingViewContext != null)
            {
                tbMessage.Text = String.Empty;
            }
        }

        private void BindConfiguration()
        {
            var recipientsValue = ActionDefinition.Properties.GetPropertyValue(NotePageConstants.RecipientsPropertyKey);
            if (!string.IsNullOrEmpty(recipientsValue))
            {
                recipients.Text = string.Join(",", (string[])OrionSerializationHelper.FromJSON(recipientsValue, typeof(string[])));
            }
            tbMessage.Text = ActionDefinition.Properties.GetPropertyValue(NotePageConstants.MessagePropertyKey);
            from.Text = ActionDefinition.Properties.GetPropertyValue(NotePageConstants.FromPropertyKey);
        }

        private void LoadRecipients()
        {
            RecipientsList.Resipients = InvokeActionMethod(PrimaryEngineID, NotePageConstants.GetRecipientsMethodNameMethodName, string.Empty);
        }
    }
}