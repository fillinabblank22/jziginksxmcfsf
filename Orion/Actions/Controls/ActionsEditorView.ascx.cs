﻿using System.Collections.Generic;
using SolarWinds.Orion.Core.Models.Actions;
using SolarWinds.Orion.Web.Actions;
using System;
using System.Linq;
using SolarWinds.Orion.Core.Models.Actions.Contexts;

namespace Orion.Actions.Controls
{
    public partial class ActionsEditorView : ActionBaseView
    {
        private IEnumerable<ActionDefinition> _actions;

        public ActionContextBase ViewContext { get; set; }

        public ActionEnviromentType EnvironmentType { get; set; }
        public ActionExecutionModeType? CategoryType { get; set; }

		public bool EnableEscalationLevels { get; set; }

        public IEnumerable<ActionDefinition> Actions
        {
            get { return _actions ?? Enumerable.Empty<ActionDefinition>(); }
            set { _actions = value; }
        }

        public string PreSelectedActionTypeID { get; set; }

        public string ClientInstanceName { get; set; }

        private bool _enableTestAction;
        public bool EnableTestAction
        {
            get { return !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer && _enableTestAction; }
            set { _enableTestAction = value; }
        }

        private bool _enableExecutionSettings = true;

        public bool EnableExecutionSettings 
        { 
            get { return _enableExecutionSettings; }
            set { _enableExecutionSettings = value; }
        } 

        protected IEnumerable<ActionPlugin> Plugins { get; private set; }


        protected override void OnInit(EventArgs e)
        {
            Plugins = ActionManager.Instance.GetPlugins(EnvironmentType).Where(p => p.Enabled);

            base.OnInit(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            if (IsPostBack)
            {
                _actions = jsonInputActions.GetObject<ActionDefinition[]>();
            }

            base.OnLoad(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ActionTimeOfDay.UseTimePeriodMode = EnvironmentType == ActionEnviromentType.Alerting;
            MacroVariablePicker.MacroContexts = ViewContext.MacroContext.Contexts;
            if (EnvironmentType == ActionEnviromentType.Alerting)
            {
                MacroVariablePicker.EntityType = ((AlertingActionContext)ViewContext).EntityType;
            }

            if (!IsPostBack)
            {
                ExecutionSettingsControl.Visible = EnableExecutionSettings;
            }
        }
    }
}