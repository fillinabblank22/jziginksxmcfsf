﻿using System;
using SolarWinds.Orion.Core.Actions.Impl.SaveToDiskURL;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Orion.Web.Actions;
using SolarWinds.Orion.Web.Helpers;

namespace Orion.Actions.Controls
{
    public partial class SaveToDiskURLView : ActionPluginBaseView
    {
        public override string ActionTypeID
        {
            get { return SaveToDiskURLConstants.ActionTypeID; }
        }

        public bool DisablePdf
        {
            get;
            set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            checkPdf.Disabled = DisablePdf = PDFExportHelper.IsPDFExportFIPSIncompatible();

            if (ActionDefinition != null)
            {
                // Edit Mode
                BindConfiguration();
            }
            else
            {
                // New mode
                SetDefaultValues();
            }
        }

        private void SetDefaultValues()
        {
            if (ReportingViewContext != null)
            {
                //set default values
            }
        }

        private void BindConfiguration()
        {
            SavePath.Text = ActionDefinition.Properties.GetPropertyValue(SaveToDiskURLConstants.SavePathPropertyKey);
            var credentialsId = 0;
            
            if (int.TryParse(ActionDefinition.Properties.GetPropertyValue(SaveToDiskURLConstants.CredentialsPropertyKey), out credentialsId))
            UserName.Text =new CredentialManager().GetCredential<UsernamePasswordCredential>(credentialsId).Name;

            checkPrintable.Checked = Convert.ToBoolean(ActionDefinition.Properties.GetPropertyValue(SaveToDiskURLConstants.PrintablePropertyKey));

            string[] enumStringsArray = (string[])OrionSerializationHelper.FromJSON(ActionDefinition.Properties.GetPropertyValue(SaveToDiskURLConstants.ContentTypePropertyKey), typeof(string[])); 

            if (enumStringsArray != null)
            {
                SaveToDiskContent[] content = new SaveToDiskContent[enumStringsArray.Length];

                for (int i = 0; i < enumStringsArray.Length; i++)
                {
                    content[i] = (SaveToDiskContent)Enum.Parse(typeof(SaveToDiskContent), enumStringsArray[i]);
                }
                foreach (SaveToDiskContent type in content)
                {
                    switch (type)
                    {
                        case SaveToDiskContent.CSV:
                            checkCsv.Checked = true;
                            break;
                        case SaveToDiskContent.PDF:
                            checkPdf.Checked = !DisablePdf;
                            break;
                        case SaveToDiskContent.Excel:
                            checkExcel.Checked = true;
                            break;
                    }
                }
            }
        }
    }
}

