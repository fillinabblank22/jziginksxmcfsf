<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SendSnmpTrapView.ascx.cs"
    Inherits="Orion_Actions_Controls_SendSnmpTrapView" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Actions.Impl.SnmpTrap" %>
<%@ Register TagPrefix="orion" TagName="SnmpCredentials" Src="~/Orion/Controls/SnmpCredentialsControl.ascx" %>
<orion:Include runat="server" File="Actions/js/SnmpTrapController.js" />
<style type="text/css">
    .port-input { margin: 0 10px; }
    .invalid {
        background-color: #FFE0E0;
        background-image: url('/Orion/images/failed_16x16.gif');
        background-repeat: no-repeat;
        padding-left: 20px;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
     }
    .beforeLabelCheckbox{ padding-right: 5px;}
    .beforeInputCheckbox{ padding-right: 5px; line-height: 22px; float: left;}
    .helpfulText
    {
        color: #5f5f5f;
        font-size: 11px;
        padding: 5px 0px;
    }
</style>
<div runat="server" id="container">
    <h3><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_99) %></h3>
    <div id="SNMPTrapMessage" style="padding: 10px; width: 600px;" class="section required">
      <asp:CheckBox CssClass="beforeLabelCheckbox" ID="TrapDestinationsEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: SnmpTrapConstants.TrapDestinationsPropertyKey %>"/>
      <span id="spanDestination" runat="server"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_100) %></span>
      <asp:TextBox runat="server" Width="97%" ID="Destinations" data-form="<%$ HtmlEncodedCode: SnmpTrapConstants.TrapDestinationsPropertyKey %>"/>
      <span class="helpfulText" style="margin-top:10px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_74) %></span>
        <br/>
       <asp:CheckBox CssClass="brforeInputCheckbox" ID="TrapTemplateEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: SnmpTrapConstants.TrapTemplatePropertyKey %>"/>
      <span id="spanTemplate" runat="server"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_101) %></span>
      <asp:DropDownList ID="TrapTemplates" runat="server" data-form="<%$ HtmlEncodedCode: SnmpTrapConstants.TrapTemplatePropertyKey %>">
      </asp:DropDownList>
        <br/>
      <asp:CheckBox CssClass="beforeLabelCheckbox" ID="AlertMesssageEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: SnmpTrapConstants.AlertMesssagePropertyKey %>"/>
      <span id="spanMessage" runat="server"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_102) %></span>
      <asp:TextBox runat="server" Width="97%" TextMode="MultiLine" Rows="5" ID="AlertMessage" data-form="<%$ HtmlEncodedCode: SnmpTrapConstants.AlertMesssagePropertyKey %>"/>
        <div>
             <orion:LocalizableButton ID="AlertMessageInsertVariable" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IT0_1 %>"
                  DisplayType="Small" OnClientClick="return false" data-macro="<%$ HtmlEncodedCode: SnmpTrapConstants.AlertMesssagePropertyKey %>" runat="server"/>
        </div>
    </div>
    <h3><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_103) %></h3>
    <div id="SNMPProperties" class="section required">
        <asp:CheckBox CssClass="beforeInputCheckbox" ID="UDPPortEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: SnmpTrapConstants.UdpPortPropertyKey %>"/>
        <span id="spanPort" runat="server"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_104) %></span>
        <asp:TextBox runat="server" ID="UDPPort" CssClass="port-input" Text="162" Width="70" data-form="<%$ HtmlEncodedCode: SnmpTrapConstants.UdpPortPropertyKey %>" />
        <div style="clear: both;"></div>
        <asp:CheckBox CssClass="beforeInputCheckbox" ID="SNMPVersionEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: SnmpTrapConstants.SnmpCredentialsPropertyKey %>"/>
        <orion:SnmpCredentials runat="server" ID="SNMPCredentialsControl"/>
    </div>
</div>
<script type="text/javascript">
    // Self executing function creates local scope to avoid globals.
    (function() {
        var controller = new SW.Core.Actions.SNMPTrapController({
            
            containerID : '<%= container.ClientID %>', 
            onReadyCallback : <%= OnReadyJsCallback %>,
            actionDefinition :  <%= ToJson(ActionDefinition, true) %>,
            viewContext :  <%= ToJson(ViewContext, true) %> ,
            environmentType : <%= ToJson(EnviromentType) %>,
            multiEditMode: <%= MultiEditEnabled.ToString().ToLower() %>,
            dataFormMapping : {
                 trapDestinations: "<%= SnmpTrapConstants.TrapDestinationsPropertyKey %>",
                 trapTemplate: "<%= SnmpTrapConstants.TrapTemplatePropertyKey %>",
                 alertMessage: "<%= SnmpTrapConstants.AlertMesssagePropertyKey %>",
                 udpPort: "<%= SnmpTrapConstants.UdpPortPropertyKey %>",
                 snmpCredentials: "<%= SnmpTrapConstants.SnmpCredentialsPropertyKey %>"
            }
        });
        controller.init();
        SW.Core.MacroVariablePickerController.BindPicker();
    })();
</script>
