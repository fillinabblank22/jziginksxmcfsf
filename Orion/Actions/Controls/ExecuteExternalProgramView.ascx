<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExecuteExternalProgramView.ascx.cs" Inherits="Orion.Actions.Controls.ExecuteExternalProgramView" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Actions.Impl.ExecuteExternalProgram" %>
<%@ Register TagPrefix="orion" TagName="WindowsCredentialSelectorControl" Src="~/Orion/Controls/WindowsCredentialSelectorControl.ascx" %>

<orion:Include runat="server" File="Actions/js/ExecuteExternalProgramController.js" />

<style type="text/css">
    #requiredProgramPath { color: red; }
    #executeProgramSection { padding: 10px; }
</style>

 <div runat="server" id="container">
     <h3><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB3_2) %></h3>
     <div id="executeProgramSection" class="section required">
        <div>
            <asp:CheckBox ID="programPathEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: ExecuteExternalProgramConstants.ProgramPathPropertyKey %>"/>
            <asp:Label ID="Label1" runat="server" Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_AY0_72 %>" AssociatedControlID="ProgramPath" />
            <asp:TextBox runat="server" Width="82%" ID="ProgramPath" data-form="<%$ HtmlEncodedCode: ExecuteExternalProgramConstants.ProgramPathPropertyKey %>"/>
            <orion:LocalizableButton ID="ProgramPathInsertVariable" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IT0_1 %>" DisplayType="Small" runat="server" data-macro ="<%$ HtmlEncodedCode: ExecuteExternalProgramConstants.ProgramPathPropertyKey %>" OnClientClick="return false"/>
            <div id="requiredProgramPath" class="error-message" style="display: none;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.ExecuteExternalProgram_ProgramPathRequired) %></div>
            <div class="hintText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_179) %></div>
        </div>

        <orion:WindowsCredentialSelectorControl ID="credentials" runat="server"/>      
    </div>
 </div>

 <script type="text/javascript">
    // Self executing function creates local scope to avoid globals.
    (function() {
        SW.Core.Actions.ExecuteExternalProgramSettingsController = new SW.Core.Actions.ExecuteExternalProgramController({
            containerID : '<%= container.ClientID %>', 
            onReadyCallback : <%= OnReadyJsCallback %>,
            actionDefinition :  <%= ToJson(ActionDefinition, true) %>,
            viewContext :  <%= ToJson(ViewContext, true) %>,
            multiEditMode: <%= MultiEditEnabled.ToString().ToLower() %>,
            dataFormMapping : {
                programPath: "<%= ExecuteExternalProgramConstants.ProgramPathPropertyKey %>"
            }
        });
        SW.Core.Actions.ExecuteExternalProgramSettingsController.init();
    })();
 </script>
