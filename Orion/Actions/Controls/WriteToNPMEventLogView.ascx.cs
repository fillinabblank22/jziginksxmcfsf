﻿using System;
using SolarWinds.Orion.Core.Actions.Impl.WriteToNPMEventLog;
using SolarWinds.Orion.Web.Actions;

namespace Orion.Actions.Controls
{
    public partial class WriteToNPMEventLogView : ActionPluginBaseView
    {
        public override string ActionTypeID
        {
            get { return WriteToNPMEventLogConstants.ActionTypeID; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ApplyMultiEditMode();
            // Edit Mode
            if (ActionDefinition != null)
            {
                BindConfiguration();
            }
            else // New mode
            {
                SetDefaultValues();
            }
        }
        private void ApplyMultiEditMode()
        {
            messageEditEnabled.Visible = MultiEditEnabled;
            if(MultiEditEnabled)
                labelMessage.Attributes.Add("style", "font-weight: bold;");
        }
        private void SetDefaultValues()
        {
            // Todo: added "ReportingViewContext" to be able to test this action, 
            // it should be removed after alerts will have posibility to test actions
            if (ReportingViewContext != null || AlertingViewContext != null)
            {
                tbMessage.Text = Resources.CoreWebContent.WEBDATA_AY0_76;
            }
        }

        private void BindConfiguration()
        {
            tbMessage.Text = ActionDefinition.Properties.GetPropertyValue(WriteToNPMEventLogConstants.MessagePropertyKey);
        }
    }
}