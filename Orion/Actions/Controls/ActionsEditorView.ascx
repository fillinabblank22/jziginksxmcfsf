<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActionsEditorView.ascx.cs" Inherits="Orion.Actions.Controls.ActionsEditorView" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Actions.Impl" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Models.Actions" %>
<%@ Register Src="~/Orion/Actions/Controls/TestActionView.ascx" TagPrefix="orion" TagName="TestActionView" %>
<%@ Register Src="~/Orion/Actions/Controls/ActionNameControl.ascx" TagPrefix="orion" TagName="ActionNameControl" %>
<%@ Register Src="~/Orion/Actions/Controls/ExecutionSettingsControl.ascx" TagPrefix="orion" TagName="ExecutionSettingsControl" %>
<%@ Register TagPrefix="orion" TagName="MacroVariablePicker" Src="~/Orion/Controls/MacroVariablePicker.ascx" %>
<%@ Register Src="~/Orion/Controls/NetObjectPicker.ascx" TagPrefix="orion" TagName="NetObjectPicker" %>
<%@ Register Src="ActionTimeOfDayControl.ascx" TagPrefix="orion" TagName="ActionTimeOfDay" %>

<orion:Include runat="server" File="Actions.css" />
<orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include runat="server" File="RenderControl.js" />
<orion:Include runat="server" File="Actions/js/ActionsEditor.js" />

<orion:JsonInput runat="server" ID="jsonInputActions" />
<orion:ExecutionSettingsControl runat="server" ID="ExecutionSettingsControl" />
<orion:ActionNameControl runat="server" ID="ActionNameControl" />
<orion:ActionTimeOfDay runat="server" ID="ActionTimeOfDay" />

<script id="actionDefinitionsTemplate" type="text/x-template">
<% if (ViewContext.EnviromentType.Equals(ActionEnviromentType.Alerting) && EnableEscalationLevels) { %>
<div class="escalationLevelWrapper" >
    <span class="escalationLevelHeader">{{ String.format('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_343) %>', escalationNumberCount) }}</span>
    {# if (escalationNumberCount == 1) { #}
    <span class="escalationLevelHeaderNotes"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Alerts_EscalationLevelOneHeaderNotes) %></span>
    {# } else { #}
    <span class="escalationLevelHeaderNotes"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Alerts_EscalationLevelAfterOneHeaderNotes) %></span>
    {# } #}
    <span class="escalationLevelCloser" data-form="escalation-level-close" data-wait="{{ wait }}" data-escalation-level="{{ escalationNumberCount }}" >
        <img src="/Orion/images/delete_icon_blue_16x16.png" alt="" />
    </span>
<% } %>
    <table class='actionDefinitions'>
    <tr class="tableHeader">
           <th class="titleColumn" style="padding-left: 27px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_88) %></th>
           <th class="buttonColumn"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZT0_40) %></th> 
           <th class="buttonColumn"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZT0_41) %></th>
           <% if ((ViewContext.EnviromentType != ActionEnviromentType.Reporting))
              { %>
             <th class="buttonColumn"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZT0_42) %></th>
           <% } %>
           <th class="buttonColumn"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_31) %></th>      
        </tr>
        <tr class='empty'><td colspan='<%= (ViewContext.EnviromentType != ActionEnviromentType.Reporting)?"5":"4" %>'>&nbsp;</td></tr>
        <tr><td colspan='<%= (ViewContext.EnviromentType != ActionEnviromentType.Reporting)?"5":"4" %>' class='actions-list-sortable' data-escalation-level="{{ escalationNumberCount }}">
    {# _.each(actions, function(current, index) { #}
        <table class='action-list-item' data-index='{{index}}' data-escalation-level="{{ escalationNumberCount }}" colspacing=0 colpadding=0>
        <tr class='actionDefinition' data-actionID='{{current.ID}}' data-index='{{index}}' data-wait="{{ wait }}" data-escalation-level="{{ escalationNumberCount }}">
           <td class='title titleColumn'>
            <% if ((ViewContext.EnviromentType != ActionEnviromentType.Reporting))
               { %>
            <span class="action-drag-icon"><img alt="" src="/Orion/images/LayoutBuilder/ddpickres.png"/></span>
            <% } %>
            <% if ((ViewContext.EnviromentType == ActionEnviromentType.Alerting))
            { %>
               <img ext:qtip="{{current.Tooltip}}" src="{{current.IconPath}}">  
            <% } %>
            <span>{{$('<div/>').text(current.Title).html()}}</span>
            <span class='info btn'><img ext:qtip="{{current.Tooltip}}" alt="" src='/Orion/images/Show_Last_Note_icon16x16.png' /></span> 
            {# if (current.TimePeriods && current.TimePeriods.length > 0) { #}
            {# current.actionMaintenanceScheduleTooltip = SW.Core.Actions.ActionsEditor.getActionMaintenanceScheduleTooltip(current.TimePeriods); #}
				<div class="verticalDivider"></div>
				<span class='info btn'><img ext:qtip="{{ current.actionMaintenanceScheduleTooltip }}" alt="" src='/Orion/images/Time_icon_Small16x16.png'/> </span>
            {# } #}
            {# if (current.repeatInterval) { #}
				<div class="verticalDivider"></div>
				<span class='info btn'><img ext:qtip="{{current.repeatIntervalTooltip}}" alt="" src='/Orion/images/Repeat_Small16x16.png'/> {{current.repeatInterval}}</span>
            {# } #}
	
           </td>

           <td class='edit btn buttonColumn'><img alt="" src='/Orion/images/edit_icon_gray_16x16.png' /></td>
           <td class='copy btn buttonColumn'><img alt="" src='/Orion/images/clone_icon_gray_16x16.png' /></td> 
           <% if (ViewContext.EnviromentType.ToString() != "Reporting")
              { %>
				<!-- Simulate (test) button -->
				<td ext:qtip="<%= HttpUtility.HtmlEncode(Resources.CoreWebContent.WEBDATA_BV0_0014) %>" class='test btn buttonColumn'><img alt="" src='/Orion/images/test_action_gray_16x16.png' /></td>     
              <% } %>
           <td class='delete btn buttonColumn'><img alt="" src='/Orion/images/delete_icon_gray_16x16.png' /></td> 
        </tr>    
    
        <tr class='empty'><td colspan='<%= (ViewContext.EnviromentType != ActionEnviromentType.Reporting)?"5":"4" %>'>&nbsp;</td></tr>
        </table>
     {# }); #}
    </td></tr>
        <tr class='addNextAction'> 
            <td colspan='5'>
            <div style= "width:100%;">
            <span class='noActionInfo'>&nbsp;</span>
                <orion:LocalizableButton ID="btnAddNew" data-form="addNew" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_ZT0_43 %>" OnClientClick="return false;" DisplayType="Secondary"></orion:LocalizableButton>
                <orion:LocalizableButton ID="btnAssignAction" data-form="assignAction" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_YK0_92 %>" OnClientClick="return false;" DisplayType="Secondary"></orion:LocalizableButton>
            <span class='rightActionInfo'>&nbsp;</span>
            </div>
            </td>
        </tr>

    </table>
<% if (ViewContext.EnviromentType.Equals(ActionEnviromentType.Alerting) && EnableEscalationLevels) { %>
</div>
<% } %> 

</script>

<script id="noActionsTemplate" type="text/x-template">
<% if (ViewContext.EnviromentType.Equals(ActionEnviromentType.Alerting) && EnableEscalationLevels) { %>    
{# if (escalationNumberCount > 1) { #}
<div class="escalationLevelWrapper"><span class="escalationLevelHeader">{{ String.format('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_343) %>', escalationNumberCount) }}</span>
    <span class='escalationLevelCloser' data-form="escalation-level-close" data-wait="{{ wait }}" data-escalation-level="{{ escalationNumberCount }}" >
        <img src='/Orion/images/delete_icon_blue_16x16.png' alt=''/>
    </span>
{# } #}
<% } %>
     <table class='actionDefinitions'>
      <tr><td colspan='6' class='actions-list-sortable' data-escalation-level="{{ escalationNumberCount }}" ></td></tr>
      <tr class='addNextAction'style='background-color: #ececec; border: none;'> 
      <td colspan='6'>
            <div style= 'width:100%;'>
            <span class='noActionInfo'> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZT0_44) %></span>
                <orion:LocalizableButton data-form="addNew" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_ZT0_43 %>" OnClientClick="return false;" DisplayType="Primary"></orion:LocalizableButton>
                <orion:LocalizableButton data-form="assignAction" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_YK0_92 %>" OnClientClick="return false;" DisplayType="Secondary"></orion:LocalizableButton>
            <span class='rightActionInfo'>&nbsp;<span>
        </div>
        </td>   
        </tr>
       </table>
<% if (ViewContext.EnviromentType.Equals(ActionEnviromentType.Alerting) && EnableEscalationLevels) { %>
{# if (escalationNumberCount > 1) { #}
</div>
{# } #}
<% } %>
</script>

<script id="waitTemplate" type="text/x-template">
    <div style='text-align:center; line-height: 8px;'><img src='/Orion/images/ActiveAlerts/Escalation_Arrow_Blue_8x8px.png' alt=''/></div>
    <div class='actionDefinitions'>
        <div class='waitSplitter'>
            <span style='display: inline-block; width: 350px;'>&nbsp;</span>
            <span class='escalationWaitInfo'>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_342) %>
                <input type="text" data-form="wait-time" 
                        {# if (wait % (60 * 24) == 0) { #}
                        value="{{ wait / (60 * 24) }}"
                        {# } else if (wait % 60 == 0) { #}
                        value="{{ wait / 60 }}"
                        {# } else { #}
                        value="{{ wait }}"
                        {# } #}
                        onkeydown="if (event.keyCode == 13) { this.blur(); return false; }"
                />
                <select data-form="wait-time-span-type">
                    <option value="Minutes"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_40) %></option>
                    <option value="Hours"{{ wait % 60 == 0 ? 'selected="true"' : '' }}><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_41) %></option>
                    <option value="Days"{{ wait % (60 * 24) == 0 ? 'selected="true"' : '' }}><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_42) %></option>
                </select>
            </span>
            <span style='display: inline-block; width: 350px; text-align: left; color: #888888; font-size:10px; padding-left: 10px;'> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Alerts_EscalationLevelWaitHelpText) %></span>
        </div>
        <div style='text-align:center; line-height: 8px;'><img src='/Orion/images/ActiveAlerts/Escalation_Arrow_Blue_8x8px.png' alt=''/></div>
    </div>
</script>

<script id="addEscalationLevelButtonTemplate" type="text/x-template">
    <table class='actionDefinitions' style='margin-top: 10px;'>
        <tr class='addNextAction' style='border: dashed 1px #006699;'> 
            <td>
                <div class="addEscalationLevelWrapper">
                    <span class='noActionInfo'>&nbsp;</span>
                        <orion:LocalizableButton data-form="add-escalation-level" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_196 %>" OnClientClick="return false;" DisplayType="Secondary" />
                    <span class='rightActionInfo'>&nbsp;</span>
                </div>
            </td>
        </tr>
    </table>
</script>

<div runat="server" id="container" style="margin-bottom: 20px;" >
    <div runat="server" id="actions" data-form="actionsList"></div>

</div>

<div runat="server" id="dialogContainer" class="actionsOffPage dialogContainer" >
</div>
<div style="display: none">
    <orion:NetObjectPicker runat="server" ID="NetObjectPicker" />
</div>
<orion:TestActionView runat="server" ID="TestActionView" />
<orion:MacroVariablePicker ID="MacroVariablePicker" runat="server"/>

<script type="text/javascript">
    Ext.QuickTips.init();
    SW.Core.namespace("SW.Core.Actions").ActionsEditor = (function() {
    	// Local scope, not visible to others:

        var controller = new SW.Core.Actions.ActionsEditorController({
            plugins: <%= ToJson(Plugins) %>,
            viewContext:<%= ToJson(ViewContext, true) %>,
        	actions: <%= ToJson(Actions, true) %>,
            containerID:  '<%= container.ClientID %>',
            // Global function used for communication beetwen action controller and plugin's ascx rendered by RenderControl.
            onPluginReadyFunctionName: 'SW.Core.Actions.ActionsEditor.onPluginReady',
            dialogContainerID: '<%= dialogContainer.ClientID %>',
            actionsInputID: '<%= jsonInputActions.ClientID %>',
            actionDefinitionsTemplateID: 'actionDefinitionsTemplate',
            noActionsTemplateID: 'noActionsTemplate',
            addEscalationLevelButtonTemplateID: 'addEscalationLevelButtonTemplate',
            waitTemplateID: 'waitTemplate',
            enviromentType: '<%= ViewContext.EnviromentType %>',
            categoryType: '<%= (!CategoryType.HasValue) ? string.Empty : CategoryType.Value.ToString() %>',
        	enableEscalationLevels: <%= (ViewContext.EnviromentType.Equals(ActionEnviromentType.Alerting) && EnableEscalationLevels).ToString().ToLower() %>,
            preselectedActionTypeID: '<%= PreSelectedActionTypeID %>',
            enableTest: <%= EnableTestAction.ToString().ToLower() %>,
        	mapping: {
				defaultEscalationWaitTime: 10,
            	escalationLevelPropertyName: '<%= CommonActionConstants.EscalationLevelPropertyName %>',
            	executionRepeatTimeSpanPropertName: '<%= CommonActionConstants.ExecutionRepeatTimeSpanPropertName %>'
            }
        });

        var clientInstanceName = '<%= ClientInstanceName %>';
        
        if (clientInstanceName != '') {
            SW.Core.Actions.ActionsEditorController.saveInstance(clientInstanceName, controller);
        }

        var triggerTypes = {
            Daily: 1,
            Weekly: 2,
            Monthly: 3
        };

        // Exports global function from local scope. 
        return {
            onPluginReady: controller.onPluginReady,
            getActions: controller.getActions,
            getActionMaintenanceScheduleTooltip: function(timeFrames) {
                var tooltip = "";
                var ttypes = [];
                for (var i = 0; i < timeFrames.length; i++) {
                    var cronFields = timeFrames[i].CronExpression.split(' ');
                    
                    if ((cronFields[2].indexOf("*/") > -1 || cronFields[4] == "1-5") && cronFields[3] == "*") //daily
                    {
                        ttypes.push(triggerTypes.Daily);
                    } else if (cronFields[4] != "*" && cronFields[3] == "*") // weekly
                    {
                        ttypes.push(triggerTypes.Weekly);
                    } else if (cronFields[3] != "*") // monthly
                    {
                        ttypes.push(triggerTypes.Monthly);
                    }
                }

                var br = "";
                if (ttypes.indexOf(triggerTypes.Daily) > -1) {
                    tooltip += "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0001) %>";
                    br = "<br/>";
                }
                if (ttypes.indexOf(triggerTypes.Weekly) > -1) {
                    tooltip += br + "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0002) %>";
                    br = "<br/>";
                }
                if (ttypes.indexOf(triggerTypes.Monthly) > -1) {
                    tooltip += br + "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0003) %>";
                }

                return tooltip;
            }
        };

    })();

</script>
