﻿using System;
using System.Web;
using SolarWinds.Orion.Core.Actions.Impl.SendHttpRequest;
using SolarWinds.Orion.Web.Actions;

namespace Orion.Actions.Controls
{
    public partial class SendHttpRequestView : ActionPluginBaseView
    {
        public override string ActionTypeID
        {
            get { return SendHttpRequestConstants.ActionTypeID; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserHasAdminRights())
            {
                authBlock.Attributes.CssStyle.Add("display", "none");
            }

            ApplyMultiEditMode();

            if (ActionDefinition != null && ActionDefinition.Properties.Count != 0)
            {
                BindConfiguration();
            } else {
                rblRequestAuthMethod.SelectedValue = ((int)RequestAuthType.NONE).ToString();
                txtContentType.Text = SendHttpRequestConfiguration.DefaultContentType;
            }

            if (String.IsNullOrEmpty(txtContentType.Text))
            {
                txtContentType.Text = SendHttpRequestConfiguration.DefaultContentType;
            }
        }

        public string SelectedCredId { get; set; } = "-1";

        private void BindConfiguration()
        {
            RequestMethod method = (RequestMethod)Enum.Parse(typeof(RequestMethod), ActionDefinition.Properties.GetPropertyValue(SendHttpRequestConstants.HttpRequestMethodPropertyKey));
            rblRequestMethod.SelectedValue = ((int)method).ToString();
            RequestAuthType authType = (RequestAuthType)Enum.Parse(typeof(RequestAuthType), ActionDefinition.Properties.GetPropertyValue(SendHttpRequestConstants.HttpRequestAuthTypeKey) ?? "1");
            rblRequestAuthMethod.SelectedValue = ((int)authType).ToString();
            txtUrl.Text = ActionDefinition.Properties.GetPropertyValue(SendHttpRequestConstants.HttpRequestUrlPropertyKey);
            txtBody.Text = ActionDefinition.Properties.GetPropertyValue(SendHttpRequestConstants.HttpRequestBodyPropertyKey);
            SelectedCredId = ActionDefinition.Properties.GetPropertyValue(SendHttpRequestConstants.HttpRequestCredentialsIdKey);
            txtContentType.Text = ActionDefinition.Properties.GetPropertyValue(SendHttpRequestConstants.HttpRequestContentTypePropertyKey);
        }

        private void ApplyMultiEditMode()
        {
            urlEditEnabled.Visible = MultiEditEnabled;
            methodEditEnabled.Visible = MultiEditEnabled;

            if (MultiEditEnabled)
            {
                Label1.Attributes.Add("style", "font-weight: bold;");
            }
        }

        private bool UserHasAdminRights()
        {
            return ((ProfileCommon)HttpContext.Current.Profile).AllowAdmin;
        }
    }
}