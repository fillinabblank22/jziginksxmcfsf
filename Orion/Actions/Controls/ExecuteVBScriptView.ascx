<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExecuteVBScriptView.ascx.cs" Inherits="Orion_Actions_Controls_ExecuteVBScriptView" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Actions.Impl.ExecuteVBScript" %>
<%@ Register TagPrefix="orion" TagName="WindowsCredentialSelectorControl" Src="~/Orion/Controls/WindowsCredentialSelectorControl.ascx" %>

<orion:Include ID="Include1" runat="server" File="Actions/js/ExecuteVBScriptController.js" />

<style type="text/css">
    .paragraph { padding-top: 10px; }
    #executeVBScriptSection { padding: 10px; }
</style>

<div runat="server" id="container">
    <h3><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB3_1) %></h3>
    <div id="executeVBScriptSection" class="section required">
           <div class="paragraph">
                <asp:CheckBox ID="InterpretersEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: ExecuteVBScriptConstants.InterpreterPropertyKey %>"/> 
                <asp:Label runat="server" ID="labelScriptInterpreter" AssociatedControlID="ScriptInterpreters" Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_VL0_39 %>"></asp:Label>
                <asp:DropDownList CssClass="scriptInterpreters" ID="ScriptInterpreters" data-form="<%$ HtmlEncodedCode: ExecuteVBScriptConstants.InterpreterPropertyKey %>" Width="150" runat="server"/>
           </div>
           <div class="paragraph">
                <asp:CheckBox ID="FilePathEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: ExecuteVBScriptConstants.FilePathPropertyKey %>"/>
                <asp:Label ID="labelFilePath" AssociatedControlID="FilePath" runat="server" Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_VL0_40 %>"></asp:Label>
                <div>
                    <asp:TextBox ID="FilePath" Width="82%" data-form="<%$ HtmlEncodedCode: ExecuteVBScriptConstants.FilePathPropertyKey %>" runat="server"/>
                    <orion:LocalizableButton ID="FilePathInsertVariable" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IT0_1 %>" DisplayType="Small" runat="server" data-macro ="<%$ HtmlEncodedCode: ExecuteVBScriptConstants.FilePathPropertyKey %>" OnClientClick="return false"/>
                    <div class="hintText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_180) %></div>
                </div>
           </div>
          <orion:WindowsCredentialSelectorControl ID="credentials" runat="server"/> 
    </div>
</div>

<script type="text/javascript">
(function() {
        var controller = new SW.Core.Actions.ExecuteVBScriptController({
            containerID : '<%= container.ClientID %>', 
            onReadyCallback : <%= OnReadyJsCallback %>,
            actionDefinition : <%= ToJson(ActionDefinition, true) %>,
            multiEditMode: <%= MultiEditEnabled.ToString().ToLower() %>,
            viewContext : <%= ToJson(ViewContext, true) %>,
                dataFormMapping : {
                interpreter: "<%= ExecuteVBScriptConstants.InterpreterPropertyKey %>",
                filePath: "<%= ExecuteVBScriptConstants.FilePathPropertyKey %>",
                credentials: "<%=ExecuteVBScriptConstants.CredentialsPropertyKey %>"
            }
        });
        controller.init();
    })();
</script>


