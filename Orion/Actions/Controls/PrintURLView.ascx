<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PrintURLView.ascx.cs" Inherits="Orion.Actions.Controls.PrintURLView" %>
<orion:Include runat="server" File="Actions/js/PrintURLController.js" />

<style type="text/css">
    .section {min-width: 400px;}
    .section .printer-name {
        background-image: url('/Orion/images/printer.png');
        background-position: center left;
        background-repeat: no-repeat;
        padding: 5px 0 10px 30px;
        margin-bottom: 5px;
        font-weight: bold;
     }
    .section .printer-name span {padding-top: 5px;display: inline-block;}
    .section .section-content {padding:5px 0 10px 0;}    
    .section.section-content label {padding: 0 5px 0 2px;}
    .no-printers-info {color: #eb3300; padding: 5px 0;}
</style>


<script id="printerDefinitionsTemplate" type="text/x-template">
    <div class="section-content">
        <div class="printer-name">
            <span>{{printer.Name}}</span>
            <input type="hidden" name="printerName" data-form="printerName" value="{{printer.FullName}}" />
        </div>
        <orion:LocalizableButton ID="btnAddNew" data-form="selectNew" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_310 %>" OnClientClick="return false;" DisplayType="Small" />
    </div>
</script>


<script id="noPrintersTemplate" type="text/x-template">
    <div class="section-content">
        <div class="no-printers-info">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_65) %>
        </div>
        <orion:LocalizableButton ID="selectNew" data-form="selectNew" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_311 %>" OnClientClick="return false;" DisplayType="Small" />
    </div>
</script>

<div runat="server" id="container">
    <h3><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_AK0_178) %></h3>
    <div id="printAuthorization" class="section required">
        <h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_63) %></h2>

        <label for="username"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_AK0_165) %></label>
        <div>
            <asp:TextBox runat="server" ID="Username" data-form="username" Width="350" /> 
    <span style="padding-left:10px;"><orion:LocalizableButton runat='server' ID='editCredentialsButton' LocalizedText='Edit' DisplayType='Small' CausesValidation='false' OnClientClick='editUserCredentials(this); return false;' Visible="False" /></span>
    
        </div>
        <div class="windowsUserName">
        <span class="hintText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_AK0_166) %></span> 
        </div>
        
        <label for="password"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_40) %></label>
        <div>
            <asp:TextBox runat="server" ID="Password" TextMode="Password" data-form="password" />
        </div>
    </div>
    
    <h3><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_64) %></h3>
    
    <div id="printSettings" class="section required">
        <h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_320) %></h2>
        <div id="printers" data-form="printersList"></div>
    

        <h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_322) %></h2>
        <div class="section-content" >
            <asp:TextBox runat="server" ID="txtNumberOfCopies" Width="20" Text="1" data-form="copies" />
            <span id="copiesValidation" style="color: #eb3300;"></span>
        </div>
    
        <h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_323) %></h2>
        <div class="section-content">
            <asp:RadioButton runat="server" ID="radioPortrait" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_313 %>" GroupName="Layout" data-form="layoutPortrait" />
            <asp:RadioButton runat="server" ID="radioLandscape" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_314 %>" GroupName="Layout" data-form="layoutLandscape" />
        </div>
    
        <h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_324) %></h2>
        <div class="section-content">
            <asp:RadioButton runat="server" ID="radioColor" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_315 %>" GroupName="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_315 %>" data-form="color" />
            <asp:RadioButton runat="server" ID="radioBlackAndWhite" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_316 %>" GroupName="Color" data-form="blackAndWhite" />
        </div>
    
        <h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_325) %></h2>
        <div class="section-content">
            <asp:DropDownList runat="server" ID="ddlMargins" data-form="margins">
                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_317 %>" Value="0" />
                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_318 %>"    Value="1" />
                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_319 %>" Value="2" />
            </asp:DropDownList>
        </div>
    
        <div runat="server" id="dialogContainer" class="actionsOffPage"></div>

    </div>
    <% if (EditMode == true) { %>
    <input type="hidden" runat="server" ID="hiddenUsername" data-form="username" value="{{printer.FullName}}" />
    <% } %>
</div>

<script type="text/javascript">
    
    // Self executing function creates local scope to avoid globals.
    var validateSectionFun;
    var clearSelectedPrinters;
    (function() {
        var controller = new SW.Core.Actions.PrintURLController({
            printers : <%= ToJson(Printers) %>,
            selectedPrinter : <%= ToJson(SelectedPrinter) %>,
            noPrintersTemplateID: 'noPrintersTemplate',
            printerDefinitionsTemplateID : 'printerDefinitionsTemplate',
            dialogContainerID: '<%= dialogContainer.ClientID %>',
            containerID : '<%= container.ClientID %>', 
            onReadyCallback : <%= OnReadyJsCallback %>,
            actionDefinition :  <%= ToJson(ActionDefinition, true) %>,
            viewContext :  <%= ToJson(ViewContext, true) %>
        });
        
        controller.init();
        validateSectionFun = controller.validateSectionAsync;
        clearSelectedPrinters = controller.clearPrintersSelection;
    })();
    <% if (EditMode)
            { %>
        
        $("#printAuthorization input").attr("readonly", true);

        function falseFunction() {
        return false;
        }

        function editUserCredentials(elem) {
            $(elem).hide();
            $("#printAuthorization input").attr("readonly", false);
            $("#<%=Password.ClientID%>").val("");
            $(".dialogContainer").accordion('activate', 0);
            $("#printSettings").prev(".ui-accordion-header").bind('click',falseFunction);
            $("#nextSectionInActionDefinitionBtn").show();
            $("#nextSectionInActionDefinitionBtn").bind('click', onEditNextClick);
            $("#createActionDefinitionBtn").hide();
        }
        
        function onEditNextClick () {
            validateSectionFun("printAuthorization", function(isValid) {
                if (isValid) {
                    clearSelectedPrinters();
                    $(".dialogContainer").accordion('activate', 1);
                    $("#printSettings").prev(".ui-accordion-header").unbind('click', falseFunction);
                    $("#nextSectionInActionDefinitionBtn").hide();
                    $("#<%=editCredentialsButton.ClientID%>").show();
                    $("#printAuthorization input").attr("readonly", true);
                    $("#nextSectionInActionDefinitionBtn").hide();
                    $("#createActionDefinitionBtn").show();
                    $("#nextSectionInActionDefinitionBtn").unbind('click', onEditNextClick);
                }
            });
        }
        <% } %>
         <% if (EditMode && string.IsNullOrEmpty(hiddenUsername.Value))
            { %>
            editUserCredentials($("#<%=editCredentialsButton.ClientID%>")[0]);
        <% } %>
</script>
