﻿using System;
using SolarWinds.Orion.Core.Actions.Impl.NetMessage;
using SolarWinds.Orion.Web.Actions;

public partial class Orion_Actions_Controls_NetMessageView : ActionPluginBaseView
{
    protected void Page_Load(object sender, EventArgs e)
    {
        bool isAvailable;
        Boolean.TryParse(InvokeActionMethod(PrimaryEngineID, NetMessageConstants.IsActionAvailibleMethodName, string.Empty), out isAvailable);          

        if (isAvailable)
        {
            ApplyMultiEditMode();
            if (ActionDefinition != null) //edit
            {
                BindConfiguration();
            }
        }
        else
        {
            warningMessage.Visible = true;
            container.Visible = false;
            IsActionSupported = false;
        }
    }

    private void ApplyMultiEditMode()
    {
        addressEditEnabledContainer.Visible = MultiEditEnabled;
        messageEditEnabled.Visible = MultiEditEnabled;
        if (MultiEditEnabled)
            lblMessage.Attributes.Add("style", "font-weight: bold;");
    }

    private void BindConfiguration()
    {
        txtAddress.Text = ActionDefinition.Properties.GetPropertyValue(NetMessageConstants.AddressKey);
        var propValue = ActionDefinition.Properties.GetPropertyValue(NetMessageConstants.SendToAllInDomainOrWorkgroupKey);
        bool value;
        if (bool.TryParse(propValue, out value))
        {
            chSendToAll.Checked = value;
        }
        txtMessage.Text = ActionDefinition.Properties.GetPropertyValue(NetMessageConstants.MessageKey);
    }

    public override string ActionTypeID
    {
        get { return NetMessageConstants.ActionTypeID; }
    }
}
