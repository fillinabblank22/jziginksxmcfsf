﻿using System;
using SolarWinds.Orion.Core.Actions.Impl.SendSyslog;
using SolarWinds.Orion.Web.Actions;

public partial class Orion_Actions_Controls_SendSyslogView : ActionPluginBaseView
{
    readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    protected void Page_Load(object sender, EventArgs e)
    {
        ApplyMultiEditMode();
        // Edit Mode
        if (ActionDefinition != null)
        {
            BindConfiguration();
        }
        else // New mode
        {
            SetDefaultValues();
        }
    }

    public override string ActionTypeID
    {
        get { return SendSyslogConstants.ActionTypeID; }
    }

    private void ApplyMultiEditMode()
    {
        hostsEditEnabled.Visible = MultiEditEnabled;
        severitiesEditEnabled.Visible = MultiEditEnabled;
        facilitiesEditEnabled.Visible = MultiEditEnabled;
        messageEditEnabled.Visible = MultiEditEnabled;
        protocolEditEnabled.Visible = MultiEditEnabled;

        if (MultiEditEnabled)
        {
            spanFacility.Attributes.Add("style", "font-weight: bold;");
            spanMessage.Attributes.Add("style", "font-weight: bold;");
            spanSeverity.Attributes.Add("style", "font-weight: bold;");
            spanDestination.Attributes.Add("style", "font-weight: bold;");
            spanProtocol.Attributes.Add("style", "font-weight: bold;");
        }
    }

    private void BindConfiguration()
    {
        tbHosts.Text = ActionDefinition.Properties.GetPropertyValue(SendSyslogConstants.HostsPropertyKey);
        var facility = ActionDefinition.Properties.GetPropertyValue(SendSyslogConstants.FacilityPropertyKey);
        var severity = ActionDefinition.Properties.GetPropertyValue(SendSyslogConstants.SeverityPropertyKey);
        
        int code;

        facilities.FacilityCode = (int.TryParse(facility, out code)) ? facility : SendSyslogConstants.DefaultFacility.ToString();
        severities.SeverityCode = (int.TryParse(facility, out code)) ? severity : SendSyslogConstants.DefaultSeverity.ToString();


        string protocolValue = ActionDefinition.Properties.GetPropertyValue(SendSyslogConstants.ProtocolPropertyKey);
        protocol.SelectedValue = String.IsNullOrEmpty(protocolValue)
            ? SendSyslogConstants.DefaultProtocol.ToString()
            : protocolValue;
        
        string ignoreCertificateChainErrorsValue = ActionDefinition.Properties.GetPropertyValue(SendSyslogConstants.IgnoreCertificateChainErrorsPropertyKey);
        ignoreCertificateChainErrors.Checked = String.IsNullOrWhiteSpace(ignoreCertificateChainErrorsValue) || Convert.ToBoolean(ignoreCertificateChainErrorsValue);

        string disableCheckCertificateRevocationValue = ActionDefinition.Properties.GetPropertyValue(SendSyslogConstants.DisableCheckCertificateRevocationPropertyKey);
        disableCheckCertificateRevocation.Checked = String.IsNullOrWhiteSpace(disableCheckCertificateRevocationValue) || Convert.ToBoolean(disableCheckCertificateRevocationValue);

        string ignoreCertificateNameMismatchValue = ActionDefinition.Properties.GetPropertyValue(SendSyslogConstants.IgnoreCertificateNameMismatchPropertyKey);
        ignoreCertificateNameMismatch.Checked = String.IsNullOrWhiteSpace(ignoreCertificateNameMismatchValue) || Convert.ToBoolean(ignoreCertificateNameMismatchValue);


        tbMessage.Text = ActionDefinition.Properties.GetPropertyValue(SendSyslogConstants.MessagePropertyKey);
    }

    private void SetDefaultValues()
    {
        tbHosts.Text = string.Empty;
        tbMessage.Text = String.Empty;
        facilities.FacilityCode = SendSyslogConstants.DefaultFacility.ToString();
        severities.SeverityCode = SendSyslogConstants.DefaultSeverity.ToString();
        protocol.SelectedValue = SendSyslogConstants.DefaultProtocol.ToString();

        ignoreCertificateChainErrors.Checked = SendSyslogConstants.DefaultIgnoreCertificateChainErrors;
        disableCheckCertificateRevocation.Checked = SendSyslogConstants.DefaultDisableCheckCertificateRevocation;
        ignoreCertificateNameMismatch.Checked = SendSyslogConstants.DefaultIgnoreCertificateNameMismatch;
    }
}