﻿using System;
using SolarWinds.Orion.Core.Actions.Impl.ExecuteExternalProgram;
using SolarWinds.Orion.Core.Actions.Impl.WriteToFile;
using SolarWinds.Orion.Web.Actions;

public partial class Orion_Actions_Controls_WriteToFileView : ActionPluginBaseView
{
    private const string defaultFileSize = "0";

    public override string ActionTypeID
    {
        get { return WriteToFileConstants.ActionTypeID; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ApplyMultiEditMode();
        if (ActionDefinition != null)
        {
            BindConfiguration();
        }
        else
        {
            txtFileSize.Text = defaultFileSize;
            // New mode
            SetDefaultCredential();
        }
    }

    private void ApplyMultiEditMode()
    {
        chFilePath.Visible = MultiEditEnabled;
        chMessage.Visible = MultiEditEnabled;
        chFileSize.Visible = MultiEditEnabled;
        if (MultiEditEnabled)
        {
            labelFileSize.Attributes.Add("style", "font-weight: bold;");
            labelMessage.Attributes.Add("style", "font-weight: bold;");
            labelPath.Attributes.Add("style", "font-weight: bold;");
        }
    }

    private void BindConfiguration()
    {
        txtFilePath.Text = ActionDefinition.Properties.GetPropertyValue(WriteToFileConstants.FilePathPropertyKey);
        txtMessage.Text = ActionDefinition.Properties.GetPropertyValue(WriteToFileConstants.MessagePropertyKey);
        txtFileSize.Text = ActionDefinition.Properties.GetPropertyValue(WriteToFileConstants.FileSizeMbPropertyKey);

        if (MultiEditEnabled)
        {
            SetDefaultCredential();
        }
        else
        {
            credentials.SetCredentials(ActionDefinition.Properties.GetPropertyValue(WriteToFileConstants.CredentialsPropertyKey));
        }
    }

    private void SetDefaultCredential()
    {
        credentials.SetCredentials(WriteToFileConstants.NotCredentialsDefinedId);
    }
}
