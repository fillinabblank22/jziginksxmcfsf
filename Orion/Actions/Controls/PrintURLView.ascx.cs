﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Orion.Web.Actions;
using SolarWinds.Orion.Core.Actions.Impl.PrintURL;

namespace Orion.Actions.Controls
{
    public partial class PrintURLView : ActionPluginBaseView
    {
        public override string ActionTypeID
        {
            get { return PrintURLConstants.ActionTypeID; }
        }

        public List<SharedPrinter> Printers { get; private set; }

        public SharedPrinter SelectedPrinter { get; private set; }

        public bool EditMode { get; private set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ActionDefinition != null)
            {
                // Edit Mode
                BindConfiguration();
            }
            else 
            {
                // New mode
                SetDefaultValues();
            }

        }

        private void SetDefaultValues()
        {
            radioPortrait.Checked = true;
            radioColor.Checked = true;
        }

        private void BindConfiguration()
        {
            EditMode = true;
            
            int credentialsID;
            UsernamePasswordCredential credentials = null;
            if (int.TryParse(ActionDefinition.Properties.GetPropertyValue(PrintURLConstants.CredentialsIDPropertyKey),
                out credentialsID))
            {
                credentials = new CredentialManager().GetCredential<UsernamePasswordCredential>(credentialsID);
            }

            editCredentialsButton.Visible = true;
            if (credentials == null || string.IsNullOrEmpty(credentials.Username) && string.IsNullOrEmpty(credentials.Password))
            {
                credentials = new UsernamePasswordCredential() { Username = string.Empty, Password = string.Empty };
            }
            else
            {
                Username.Text = credentials.Username;
                Password.Attributes.Add("value", credentials.Password);
                string args = SerializationHelper.ToXmlString(credentials);
                string printersList = InvokeActionMethod(PrimaryEngineID, PrintURLConstants.GetPrintersMethodName, args);
                Printers = SerializationHelper.FromXmlString<List<SharedPrinter>>(printersList);
                SelectedPrinter = Printers.FirstOrDefault(printer => printer.FullName.Equals(ActionDefinition.Properties.GetPropertyValue(PrintURLConstants.PrinterNamePropertyKey)));
            }
            txtNumberOfCopies.Text = ActionDefinition.Properties.GetPropertyValue(PrintURLConstants.CopiesPropertyKey);
           
            PrintLayout printLayout;
            if (Enum.TryParse(ActionDefinition.Properties.GetPropertyValue(PrintURLConstants.LayoutPropertyKey),
                    out printLayout) && printLayout == PrintLayout.Portrait)
                radioPortrait.Checked = true;
            else
                radioLandscape.Checked = true;

            PrintColor printColor;
            if (Enum.TryParse(ActionDefinition.Properties.GetPropertyValue(PrintURLConstants.ColorPropertyKey),
                   out printColor) && printColor == PrintColor.Color)
                radioColor.Checked = true;
            else
                radioBlackAndWhite.Checked = true;

            PrintMargins printMargins;
            Enum.TryParse(ActionDefinition.Properties.GetPropertyValue(PrintURLConstants.MarginsPropertyKey),
                out printMargins);

            ddlMargins.SelectedValue = ((int)printMargins).ToString();

            hiddenUsername.Value = credentials.Username;
        }
    }
}
