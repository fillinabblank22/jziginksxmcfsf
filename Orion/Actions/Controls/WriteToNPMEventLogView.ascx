<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WriteToNPMEventLogView.ascx.cs" Inherits="Orion.Actions.Controls.WriteToNPMEventLogView" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Actions.Impl.WriteToNPMEventLog" %>

<orion:Include runat="server" File="Actions/js/WriteToNPMEventLogController.js" />

<style type="text/css">
     .writeToNPMEventLogAction .messageBox {
         margin-top: 5px;
    }

    .writeToNPMEventLogAction .message {
        width: 550px;
        height: 100px;
    }
</style>

<div runat="server" id="container" class="writeToNPMEventLogAction">
    <h3><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB3_3) %></h3>
    <div id="eventLogSection" class="section required">

        <div class="messageBox">
            <asp:CheckBox ID="messageEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: WriteToNPMEventLogConstants.MessagePropertyKey %>"/>
            <asp:Label ID="labelMessage" runat="server" Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_AY0_70 %>" AssociatedControlID="tbMessage" />
            <br />
            <asp:TextBox ID="tbMessage" CssClass="message" runat="server" TextMode="MultiLine" data-form="<%$ HtmlEncodedCode: WriteToNPMEventLogConstants.MessagePropertyKey %>"></asp:TextBox>
        </div>
        <div>
             <orion:LocalizableButton ID="tbMessageInsertVariable" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IT0_1 %>" DisplayType="Small" OnClientClick="return false" data-macro="<%$ HtmlEncodedCode: WriteToNPMEventLogConstants.MessagePropertyKey %>" runat="server"/>
        </div>

    </div>
</div>

<script type="text/javascript">

    // Self executing function creates local scope to avoid globals.
    (function() {
        var controller = new SW.Core.Actions.WriteToNPMEventLogController({
            containerID : '<%= container.ClientID %>', 
            onReadyCallback : <%= OnReadyJsCallback %>,
            actionDefinition :  <%= ToJson(ActionDefinition, true) %>,
            viewContext :  <%= ToJson(ViewContext, true) %>, 
            multiEditMode: <%= MultiEditEnabled.ToString().ToLower() %>,
            dataFormMapping : {
                message: "<%= WriteToNPMEventLogConstants.MessagePropertyKey %>"
            }
        });
        controller.init();
        SW.Core.MacroVariablePickerController.BindPicker();
    })();
    
</script>
