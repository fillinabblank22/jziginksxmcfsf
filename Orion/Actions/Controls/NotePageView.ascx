<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NotePageView.ascx.cs" Inherits="Orion.Actions.Controls.NotePageView" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Actions.Impl.NotePage" %>
<%@ Register TagPrefix="orion" TagName="Recipients" Src="~/Orion/Controls/NotePageRecipientsControl.ascx" %>
<orion:Include runat="server" File="Actions/js/NotePageController.js" />
<orion:Include runat="server" File="OrionCore.js" />

<style>
    .notePageAction .message {
        width: 550px;
        height: 100px;
    }
    .dialogOffPage {
        position: absolute;
        left: -1999px;   
        top: -2000px;  
    }
    .notInstalled {
        text-align: center;
    }
    .inputBox {
        padding: 5px 0;
    }
</style>

<div runat="server" id="container" class="notePageAction">
    <div id="notePageSettings" runat="server">
        <h3><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB3_7) %></h3>
        <div class="section required">
        <div class="recipientsBox inputBox">
        <div>
           <asp:CheckBox ID="recipientsEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: NotePageConstants.RecipientsPropertyKey %>"/>
           <span runat="server" id="divRecepients"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_106) %></span>
        </div>
           <asp:TextBox ID="recipients" runat="server" data-form="<%$ HtmlEncodedCode: NotePageConstants.RecipientsPropertyKey %>" Width="520"/>
           <orion:LocalizableButton runat='server' ID='addRecipientsButton' CssClass="manageRecipientsButton" LocalizedText='CustomText' Text="..." DisplayType='Small' CausesValidation='false' OnClientClick='SW.Orion.NotePageRecipientsSelector.ShowDialod(this); return false;' />
        </div>
        <div class="fromBox inputBox">
            <div>
                <asp:CheckBox ID="fromEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: NotePageConstants.FromPropertyKey %>"/>
                <span runat="server" id="divFrom"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_30) %></span>
            </div>
           <asp:TextBox ID="from" runat="server" data-form="<%$ HtmlEncodedCode: NotePageConstants.FromPropertyKey %>" Width="550"/>
        </div>
        <div class="messageBox inputBox">
            <div>
                <asp:CheckBox ID="messageEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: NotePageConstants.MessagePropertyKey %>"/>
                <span runat="server" id="divMessage"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VM0_17) %></span>
            </div>
            <asp:TextBox ID="tbMessage" CssClass="message" runat="server" TextMode="MultiLine" data-form="<%$ HtmlEncodedCode: NotePageConstants.MessagePropertyKey %>" MaxLength="400"></asp:TextBox>
            <div>
                <orion:LocalizableButton ID="tbMessageInsertVariable" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IT0_1 %>"
                  DisplayType="Small" OnClientClick="return false" data-macro="<%$ HtmlEncodedCode: NotePageConstants.MessagePropertyKey %>" runat="server"/>
            </div>
        </div>
        <div id="selectRecipients" section="" class="dialogOffPage">
         <orion:Recipients runat="server" ID="RecipientsList" />
         <div class="buttons"></div>
         </div>
        </div>
    </div>
    <div id="notePageNotInstalled" class="notInstalled section" runat="server">
    <h1><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_107) %></h1>
    <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_108) %></p>
        <a target="blank" href = "http://www.notepage.net/notepagerpro.htm" style="color: blue; text-decoration: underline;">http://www.notepage.net/notepagerpro.htm</a> 

    </div>
</div>

<script type="text/javascript">
    // Self executing function creates local scope to avoid globals.
    (function() {
        var controller = new SW.Core.Actions.NotePageController({
            containerID : '<%= container.ClientID %>', 
            onReadyCallback : <%= OnReadyJsCallback %>,
            actionDefinition :  <%= ToJson(ActionDefinition, true) %>,
            viewContext :  <%= ToJson(ViewContext, true) %>,
            multiEditMode: <%= MultiEditEnabled.ToString().ToLower() %>,
            isActionSupported: <%= IsActionSupported.ToString().ToLower() %>,
            dataFormMapping : {
                message: "<%= NotePageConstants.MessagePropertyKey %>",
                from: "<%= NotePageConstants.FromPropertyKey %>",
                recipients: "<%= NotePageConstants.RecipientsPropertyKey %>"
            }
        });
        controller.init();
        SW.Core.MacroVariablePickerController.BindPicker();
    })();
</script>
