﻿using System;

using SolarWinds.Orion.Core.Actions.Impl.TextToSpeech;
using SolarWinds.Orion.Web.Actions;

namespace Orion.Actions.Controls
{
    public partial class TextToSpeechView : ActionPluginBaseView
    {
        public override string ActionTypeID
        {
            get { return TextToSpeechConstants.ActionTypeID; }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            ApplyMultiEditMode();

            if (ActionDefinition != null) // Edit Mode
            {
                BindConfiguration();
            }
        }

        private void ApplyMultiEditMode()
        {
            speechTextEditEnabled.Visible = MultiEditEnabled;
            if(MultiEditEnabled)
                divText.Attributes.Add("style", "font-weight: bold;");
        }
      
        private void BindConfiguration()
        {
            speechText.Text = ActionDefinition.Properties.GetPropertyValue(TextToSpeechConstants.TextPropertyKey);
        }  
    }
}
