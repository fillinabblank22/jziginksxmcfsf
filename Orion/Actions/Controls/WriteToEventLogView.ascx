<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WriteToEventLogView.ascx.cs" Inherits="Orion.Actions.Controls.WriteToEventLogView" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Actions.Impl.WriteToEventLog" %>

<orion:Include runat="server" File="Actions/js/WriteToEventLogController.js" />

<style>
    .writeToEventLogAction .message {
        width: 550px;
        height: 100px;
    }
</style>

<div runat="server" id="container" class="writeToEventLogAction">
    
    <h3><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ZT0_52) %></h3>
    <div id="eventLogSection" class="section required">
      <div class="messageBox">
            <asp:CheckBox ID="messageEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: WriteToEventLogConstants.MessagePropertyKey %>"/>
            <asp:Label ID="labelMessage" runat="server" Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_ZT0_53 %>" AssociatedControlID="tbMessage" />
            <br />
            <asp:TextBox ID="tbMessage" CssClass="message" runat="server" TextMode="MultiLine" data-form="<%$ HtmlEncodedCode: WriteToEventLogConstants.MessagePropertyKey %>"></asp:TextBox>
            </div>
        <div>
            <orion:LocalizableButton ID="tbMessageInsertVariable" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IT0_1 %>"
                 DisplayType="Small" OnClientClick="return false" data-macro="<%$ HtmlEncodedCode: WriteToEventLogConstants.MessagePropertyKey %>" runat="server"/>
        </div>

    </div>
</div>


<script type="text/javascript">

    // Self executing function creates local scope to avoid globals.
    (function() {

        var controller = new SW.Core.Actions.WriteToEventLogController({
            
            containerID : '<%= container.ClientID %>', 
            onReadyCallback : <%= OnReadyJsCallback %>,
            actionDefinition :  <%= ToJson(ActionDefinition, true) %>,
            viewContext :  <%= ToJson(ViewContext, true) %>,
            multiEditMode: <%= MultiEditEnabled.ToString().ToLower() %>,
            dataFormMapping : {
                message: "<%= WriteToEventLogConstants.MessagePropertyKey %>",
                machine: "<%= WriteToEventLogConstants.MachinePropertyKey %>"
            }
        });

        controller.init();
        SW.Core.MacroVariablePickerController.BindPicker();
    })();
    
</script>
