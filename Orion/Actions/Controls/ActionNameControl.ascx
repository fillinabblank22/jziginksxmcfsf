<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActionNameControl.ascx.cs" Inherits="Orion_Actions_Controls_ActionrNameControl" %>
<orion:Include ID="Include1" runat="server" File="Actions/js/ActionNameController.js" />
<style type="text/css">
    .actionName_blueBox 
    {
        padding-bottom: 15px;
    }
    .actionName_textContainer{margin-left:20px;}
</style>

<div class="hidden" id="ActionNamePlugin">
    <div class="actionName_blueBox">
        <div>
            <div style="float: left;">
                <asp:CheckBox ID="actionNameMultiEnable" runat="server" data-edited="actionNameMultiEnable" />
            </div>
            <div class="actionName_textContainer" id="divTextContainer">
                <label data-id="labelActionName"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_84) %></label>
                <asp:TextBox runat="server" Width="97%" ID="actionName" data-form="ActionName" />
            </div>
        </div>
    </div>
</div>