<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TestActionView.ascx.cs" Inherits="Orion_Actions_Controls_TestActionView" %>
<%@ Register Src="~/Orion/Controls/NetObjectPicker.ascx" TagPrefix="orion" TagName="NetObjectPicker" %>

<orion:Include ID="Include1" runat="server" File="Actions/js/TestActionController.js" />

<style type="style/css">
    #testActionDialog .selected-objects-panel {  padding-left: 10px; vertical-align: top; }
    #testActionDialog #selectedObjectsEmpty, .selected-objects-panel table { background-color: #ECECEC; width: 100%; }
    #testActionDialog #selectedObjectsEmpty { padding: 3px 3px 3px 3px; }
    #testActionDialog .selected-objects-panel table td { padding: 3px 3px 3px 3px; }
    #testActionDialog, #testActionDialog .ui-dialog-content, #testActionDialog .ui-accordion-content {
        background-color: white !important;
        background-image: none;
    }
</style>

<div id="testActionDialog" style="display: none;">
    <span class="sw-suggestion">
        <span class="sw-suggestion-icon"></span>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_199) %>
    </span>

    <div id="testActionNetObjectPicker">
        <div class='sw-btn-bar-wizard' style="margin-bottom: 0; padding-bottom: 0;">
            <orion:LocalizableButton ID="btnSimulate"  CausesValidation="false" ClientIDMode="Static" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SS0_2 %>" OnClientClick="return false;" style="display: none;" ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_198 %>" />
            <orion:LocalizableButton ID="btnNetObjectSelected"  CausesValidation="false" ClientIDMode="Static" runat="server" DisplayType="Secondary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SS0_1 %>" OnClientClick="return false;" ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_197 %>"/>
            <orion:LocalizableButton ID="btnCloseNetObjectSelector"  CausesValidation="false" ClientIDMode="Static" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClientClick="return false;" />
        </div>
    </div>
    
    <div id="testActionResult" style="display:none">
        <div id="testActionResultMsg" class="sw-suggestion"></div>
        <div class='sw-btn-bar-wizard' style="margin-bottom: 0; padding-bottom: 0; margin-top: 50px;">
            <orion:LocalizableButton ID="btnCloseTestActionResult"  CausesValidation="false" ClientIDMode="Static" runat="server" DisplayType="Secondary" LocalizedText="Close" OnClientClick="return false;" />
        </div>
    </div>
</div>
