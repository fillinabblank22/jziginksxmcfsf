﻿using System;

public partial class Orion_Controls_ActionTimeOfDayControl : System.Web.UI.UserControl
{
    public bool UseTimePeriodMode { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        FrequencyControl.UseTimePeriodMode = UseTimePeriodMode;
    }
}