<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SendHttpRequestView.ascx.cs" Inherits="Orion.Actions.Controls.SendHttpRequestView" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Actions.Impl.SendHttpRequest" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>
<%@ Register Src="~/Orion/Controls/WindowsCredentialsControl.ascx" TagPrefix="orion" TagName="Credential" %>

<orion:Include ID="Include1" runat="server" File="Actions/js/SendHttpRequestController.js" />

<style type="text/css">
    .sendHttpRequestAction .radio label {
        margin-right: 30px;
    }

    .sendHttpRequestAction .requestMethod .check {
        float: left;
        margin-right: 3px;
    }

    .sendHttpRequestAction .urlInput, .sendHttpRequestAction .requestInput {
        width: 550px;
    }

    .sendHttpRequestAction .requestInput {
        height: 100px;
    }

    .sendHttpRequestAction .requestBody, .sendHttpRequestAction .requestMethod {
        margin-top: 10px;
    }

    .authBlock {
        width: 350px;
        padding-top: 10px;
    }

    .authBlock .title {
        margin-bottom: 5px;
    }

    .authMethod {
        padding-top: 5px;
    }

    .loginForm {
        margin: 0 10px 10px 10px;
        background-color: #f5f5f5;
        min-width: 255px;
        display: inline-block;
        padding: 10px;
    }

    .credentialsDropDown {
        width: 275px;
        margin-top: 5px;
        margin-left: 10px;
    }
    
    .header {
        text-align: center;
        margin-bottom: 10px;
    }

    .header span {
        font-weight: 600;
    }

    .hidden {
        display: none;
    }

    .apiTokensBlock .input {
        width: 550px;
    }
</style>

<div runat="server" id="container" class="sendHttpRequestAction">

    <h3><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TD0_15) %></h3>
    <div id="httpRequestSection" class="section required">
        <div class="url">
            <asp:CheckBox ID="urlEditEnabled" runat="server" data-edited="<%$ HtmlEncodedCode: SendHttpRequestConstants.HttpRequestUrlPropertyKey %>" />
            <asp:Label ID="Label1" runat="server" AssociatedControlID="txtUrl" Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_TM0_128 %>" /><br />
            <asp:TextBox ID="txtUrl" runat="server" CssClass="urlInput" data-form="<%$ HtmlEncodedCode: SendHttpRequestConstants.HttpRequestUrlPropertyKey %>" />
        </div>
        <div class="requestMethod">
            <asp:CheckBox ID="methodEditEnabled" runat="server" data-edited="RequestMethodSection" CssClass="check" />
            <div data-form="RequestMethodSection">
                <asp:RadioButtonList ID="rblRequestMethod" runat="server" RepeatDirection="Horizontal" CssClass="radio">
                    <asp:ListItem Value="<%$ HtmlEncodedCode: (int)RequestMethod.GET %>" Selected="True" Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_TD0_12 %>"
                        data-form="<%$ HtmlEncodedCode: SendHttpRequestConstants.HttpRequestMethodPropertyKey %>" />
                    <asp:ListItem Value="<%$ HtmlEncodedCode: (int)RequestMethod.POST %>" Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_TD0_13 %>"
                        data-form="<%$ HtmlEncodedCode: SendHttpRequestConstants.HttpRequestMethodPropertyKey %>" />
                </asp:RadioButtonList>
                <div class="requestBody" data-form="MessageBodySection">
                    <asp:Label runat="server" Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_TD0_14 %>" AssociatedControlID="txtBody" /><br />
                    <asp:TextBox ID="txtBody" CssClass="requestInput" runat="server" TextMode="MultiLine" data-form="<%$ HtmlEncodedCode: SendHttpRequestConstants.HttpRequestBodyPropertyKey %>" />
                    <div>
                        <orion:LocalizableButton ID="txtBodyInsertVariable" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IT0_1 %>"
                            DisplayType="Small" OnClientClick="return false" data-macro="<%$ HtmlEncodedCode: SendHttpRequestConstants.HttpRequestBodyPropertyKey %>" runat="server" />
                    </div>
                    <asp:Label runat="server" Text="<%$ HtmlEncodedResources:CoreWebContent, ContentType %>" AssociatedControlID="txtContentType" /><br />
                    <asp:TextBox ID="txtContentType" CssClass="urlInput" runat="server" data-form="<%$ HtmlEncodedCode: SendHttpRequestConstants.HttpRequestContentTypePropertyKey %>" />
                </div>
            </div>

        </div>
        <div id="authBlock" runat="server" class="authBlock">
            <div class="title"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK1_3) %></div>
            <asp:RadioButtonList ID="rblRequestAuthMethod" runat="server" RepeatDirection="Horizontal" CssClass="radio">
                        <asp:ListItem Value="<%$ HtmlEncodedCode: (int)RequestAuthType.NONE %>" Text="<%$ HtmlEncodedResources: CoreWebContent, AuthType_None %>"
                            data-form="<%$ HtmlEncodedCode: SendHttpRequestConstants.HttpRequestAuthTypeKey %>" />
                        <asp:ListItem Value="<%$ HtmlEncodedCode: (int)RequestAuthType.BASIC %>" Text="<%$ HtmlEncodedResources: CoreWebContent, AuthType_Basic %>"
                            data-form="<%$ HtmlEncodedCode: SendHttpRequestConstants.HttpRequestAuthTypeKey %>" />
                        <asp:ListItem Value="<%$ HtmlEncodedCode: (int)RequestAuthType.NTLM %>" Text="<%$ HtmlEncodedResources: CoreWebContent, AuthType_NTLM %>"
                            data-form="<%$ HtmlEncodedCode: SendHttpRequestConstants.HttpRequestAuthTypeKey %>" />
                        <asp:ListItem Value="<%$ HtmlEncodedCode: (int)RequestAuthType.TOKEN %>" Text="<%$ HtmlEncodedResources: CoreWebContent, AuthType_Token %>"
                            data-form="<%$ HtmlEncodedCode: SendHttpRequestConstants.HttpRequestAuthTypeKey %>" />
            </asp:RadioButtonList>

            <div id="authMethod" data-form="RequestAuthSection">
                <div data-form="RequestAuthTypeSection">
                    <asp:DropDownList runat="server" ID="ddCredentials" data-form="CredentialsList" class="credentialsDropDown"/>
                    <div ID="CredManagerDialogFrame" data-form="RequestLoginForm">
                        <div class="loginForm">
                            <div class="header"><span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Enter_Credential) %></span></div>
                            <div id="CredManagerDialogBody" class="dialogBody" data-form="credManagerDialog">
                                <table>
                                    <tr>
                                        <td><span class="Label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_521) %></span></td>
                                        <td>
                                            <asp:TextBox ID="credDescription" maxlength="50"  data-form="BasicAuthLoginDescription" runat="server" required/></td>
                                    </tr>
                                    <tr>
                                        <td><span class="Label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_73) %></span></td>
                                        <td>
                                            <asp:TextBox ID="credUsername" maxlength="50" data-form="BasicAuthLogin" runat="server" /></td>
                                    </tr>
                                    <tr>
                                        <td><span class="Label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_40) %></span></td>
                                        <td>
                                            <asp:TextBox ID="credPassword" maxlength="50" type="password" data-form="CredentialsPassword" runat="server" /></td>
                                    </tr>
                                    <tr>
                                        <td><span class="Label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_182) %></span></td>
                                        <td>
                                            <asp:TextBox ID="credConfirm" maxlength="50" type="password" data-form="CredentialsConfirmPassword" runat="server"/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="color: red;"><span id="errorDiv"></span></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div id="HeaderDialogBody" class="apiTokensBlock"  data-form="headerManagerDialog">

                        <div class="url">
                            <label for="headerName"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_252) %></label><br>
                            <asp:TextBox ID="headerName" data-form="HeaderName" runat="server" CssClass="input" />
                        </div>

                        <div class="url">
                            <label for="headerValue" id="header-value-label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_253) %></label><br>
                            <asp:TextBox ID="headerValue" data-form="HeaderValue" runat="server" CssClass="input" />
                        </div>
                        
                        <span id="errorDiv"></span>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    (function () {
        var controller = new SW.Core.Actions.SendHttpRequestController({
            containerID: '<%= container.ClientID %>',
            onReadyCallback: <%= OnReadyJsCallback %>,
            actionDefinition:  <%= ToJson(ActionDefinition, true) %>,
            viewContext:  <%= ToJson(ViewContext, true) %>,
            multiEditMode: <%= MultiEditEnabled.ToString().ToLower() %>,
            dataFormMapping: {
                method: '<%= SendHttpRequestConstants.HttpRequestMethodPropertyKey %>',
                url: '<%= SendHttpRequestConstants.HttpRequestUrlPropertyKey %>',
                body: '<%= SendHttpRequestConstants.HttpRequestBodyPropertyKey %>',
                bodySection: 'MessageBodySection',
                methodSection: 'RequestMethodSection',
                authEnabled: 'AuthentificationEnabled',
                authMethod: '<%= SendHttpRequestConstants.HttpRequestAuthTypeKey %>',
                authSection: 'RequestAuthSection',
                login: 'BasicAuthLogin',
                password: 'CredentialsPassword',
                confirmPassword: 'CredentialsConfirmPassword',
                credDescription: 'BasicAuthLoginDescription',
                credCreateDialog: 'credManagerDialog',
                credList: 'CredentialsList',
                selectedCredId: '<%= SelectedCredId %>',
                headerManagerDiag: 'headerManagerDialog',
                headerName: 'HeaderName',
                headerValue: 'HeaderValue',
                requestLoginForm: 'RequestLoginForm',
                contentType: '<%= SendHttpRequestConstants.HttpRequestContentTypePropertyKey %>'
            }
        });

        controller.init();
        SW.Core.MacroVariablePickerController.BindPicker();
    })();
</script>