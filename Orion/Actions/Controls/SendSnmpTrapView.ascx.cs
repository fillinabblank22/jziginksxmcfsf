﻿using System;

using SolarWinds.Orion.Core.Actions.Impl.SnmpTrap;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web.Actions;


public partial class Orion_Actions_Controls_SendSnmpTrapView : ActionPluginBaseView
{
    public override string ActionTypeID
    {
        get { return SnmpTrapConstants.ActionTypeID; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            LoadTrapTemplates();
        ApplyMultiEditMode();
        if (ActionDefinition != null) //edit
        {
            BindConfiguration();
        }
        else //new
        {
            SetDefaultValues();
        }
    }

    private void SetDefaultValues()
    {
        SNMPCredentialsControl.SnmpVersion = SNMPVersion.SNMP2c;
        SNMPCredentialsControl.EditedCredentials = SnmpCredentials.CreateSnmpCredentials(new SnmpEntry() { Version = SNMPVersion.SNMP2c, Name = "public"});
    }

    private void ApplyMultiEditMode()
    {
        UDPPortEditEnabled.Visible = MultiEditEnabled;
        SNMPVersionEditEnabled.Visible = MultiEditEnabled;
        AlertMesssageEditEnabled.Visible = MultiEditEnabled;
        TrapTemplateEditEnabled.Visible = MultiEditEnabled;
        TrapDestinationsEditEnabled.Visible = MultiEditEnabled;

        if (MultiEditEnabled)
        {
            spanPort.Attributes.Add("style", "font-weight: bold;");
            spanMessage.Attributes.Add("style", "font-weight: bold;");
            spanTemplate.Attributes.Add("style", "font-weight: bold;");
            spanDestination.Attributes.Add("style", "font-weight: bold;");
        }
    }

    private void LoadTrapTemplates()
    {
        var trapTemplatesJson = InvokeActionMethod(PrimaryEngineID, SnmpTrapConstants.SnmpGetTrapTemplateMethodName, string.Empty);
        TrapTemplate[] trapTemplates = (TrapTemplate[])OrionSerializationHelper.FromJSON(trapTemplatesJson, typeof(TrapTemplate[]));
        foreach (var template in trapTemplates)
        {
            TrapTemplates.Items.Add(template.Name);
        }
    }
    
    private void BindConfiguration()
    {
        if (!string.IsNullOrEmpty(ActionDefinition.Properties.GetPropertyValue(SnmpTrapConstants.SnmpCredentialsPropertyKey)))
        {
            var snmpCredentials = SnmpCredentials.CreateSnmpCredentials((SnmpEntry)OrionSerializationHelper.FromJSON(ActionDefinition.Properties[SnmpTrapConstants.SnmpCredentialsPropertyKey].PropertyValue, typeof(SnmpEntry)));
            SNMPCredentialsControl.EditedCredentials = snmpCredentials;
        }
        if (!string.IsNullOrEmpty(ActionDefinition.Properties.GetPropertyValue(SnmpTrapConstants.SnmpVersionPropertyKey)))
            SNMPCredentialsControl.SnmpVersion = (SNMPVersion)Enum.Parse(typeof(SNMPVersion), ActionDefinition.Properties.GetPropertyValue(SnmpTrapConstants.SnmpVersionPropertyKey));
        AlertMessage.Text = ActionDefinition.Properties.GetPropertyValue(SnmpTrapConstants.AlertMesssagePropertyKey);
        Destinations.Text = ActionDefinition.Properties.GetPropertyValue(SnmpTrapConstants.TrapDestinationsPropertyKey);
        AlertMessage.Text = ActionDefinition.Properties.GetPropertyValue(SnmpTrapConstants.AlertMesssagePropertyKey);
        TrapTemplates.SelectedValue = ActionDefinition.Properties.GetPropertyValue(SnmpTrapConstants.TrapTemplatePropertyKey);
        UDPPort.Text = ActionDefinition.Properties.GetPropertyValue(SnmpTrapConstants.UdpPortPropertyKey) ?? "300";
    }
}
