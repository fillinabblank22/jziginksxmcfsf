<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EmailUrlView.ascx.cs"
    Inherits="Orion_Actions_Controls_EmailUrlView" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Actions.Impl.Email" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Actions.Impl.EmailWebPage" %>
<%@ Register TagPrefix="orion" TagName="SmtpSettings" Src="~/Orion/Controls/SmtpSettingsControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EmailsInput" Src="~/Orion/Controls/EmailsInput.ascx" %>
<%@ Register TagPrefix="orion" TagName="UserAccountControl" Src="~/Orion/Controls/UserAccountControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="HelpLink" Src="~/Orion/Controls/HelpLink.ascx" %>
<orion:Include runat="server" File="Actions/js/EmailUrlController.js" />
<style type="text/css">
    .email-label { padding-top: 10px; }
    .blueBox { background-color: #ecf6fb; padding: 10px; margin-bottom: 10px; }
    .alignCheckBox { float: left; margin-top: 20px;}
    .disabledEdit { color: rgb(84, 84, 84);}
    #senderDetails  { width: 400px; background-color: #EFEFEF; margin-left: 20px; padding: 5px 15px 10px 10px; }
    #senderInfo  { margin: 0 5px -3px 0; overflow: hidden; white-space:nowrap; text-overflow:ellipsis; width:400px; display:inline-block; color: #646464; }
    .userAccount { padding-top: 10px; padding-left:10px; }
</style>

<script type="text/javascript">
    function showNotification() {
        var atLeastOneIsChecked = $('input[class=exgroup]:checked').length > 0;
        if (atLeastOneIsChecked === true)
            $('#notifMessage').show();
        else
            $('#notifMessage').hide();
    }
</script>

    <div runat="server" id="container">
    <h3>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_27) %></h3>
    <div id="recipients" style="padding: 10px; width: 600px;" class="section required">
        <div runat="server" id="divEmailTo">
            <asp:CheckBox ID="EmailToEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: EmailConstants.EmailToPropertyKey %>"/>
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_28) %>
        </div>
        <orion:EmailsInput ID = "txtEmailTo" runat="server" DataForm ="<%$ HtmlEncodedCode: EmailConstants.EmailToPropertyKey %>" MultipleMode="True" AllowMacros="True"/>
         <div id="ccContainer" runat="server" style="display: none;">
            <div runat="server" id="divCC">
                <asp:CheckBox ID="EmailCcEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: EmailConstants.EmailCCPropertyKey %>"/>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_31) %>
            </div>
            <table>
                <tr>
                    <td style="width: 570px;">
                        <orion:EmailsInput ID = "txtCC" runat="server" DataForm ="<%$ HtmlEncodedCode: EmailConstants.EmailCCPropertyKey %>"  MultipleMode="True" AllowMacros="True"/>
                    </td> 
                    <td style="padding-left: 10px; vertical-align: top;">
                        <span id="ccClose" onclick="hideAdditionalInput(this); return false;">
                        <img style="vertical-align: text-bottom; cursor: pointer; margin-top: 4px;" alt="" src="/Orion/images/delete_icon_Black16x16.png" />
                    </span>
                    </td>
                </tr>
            </table>
        </div>
        <div id="bccContainer" runat="server" style="display: none;">
            <div id="divBcc" runat="server">
                <asp:CheckBox ID="EmailBccEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: EmailConstants.EmailBCCPropertyKey %>"/>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_32) %>
            </div>
                <table>
                    <tr><td style="width: 570px;">
            <orion:EmailsInput ID = "txtBCC" runat="server" DataForm ="<%$ HtmlEncodedCode: EmailConstants.EmailBCCPropertyKey %>" MultipleMode="True" AllowMacros="True"/>
            </td>
            <td style="padding-left: 10px; vertical-align: top;">
            <span id="bccClose" onclick="hideAdditionalInput(this); return false;">
                <img style="vertical-align: text-bottom;cursor: pointer; margin-top: 4px;" alt="" src="/Orion/images/delete_icon_Black16x16.png" />
            </span>
            </td>
            </tr>
            </table>
        </div>
        <div class="sw-btn-bar">
            <orion:LocalizableButton runat="server" ID="btnCC" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_31 %>"
                DisplayType="Small" CausesValidation="false" OnClientClick="showAdditionalInput(this); return false;" />
            <orion:LocalizableButton runat="server" ID="btnBCC" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_32 %>"
                DisplayType="Small" CausesValidation="false" OnClientClick="showAdditionalInput(this); return false;" />
        </div>
        <span>
            <img alt="" style="margin: 0 5px -2px 0;" id = "collapseExpandButton" src="/Orion/images/Button.Collapse.gif" onclick="collapseExpandSenderInfo();"/>
            <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_85) %></label>
        </span><span id="senderInfo"></span> 
        <div id="senderDetails">
            <div class="email-label" id="divEmailSender" runat="server">
                <asp:CheckBox ID="EmailSenderNameEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: EmailConstants.SenderPropertyKey %>"/>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_83) %> 
            </div>
            <asp:TextBox  runat="server" Width="100%" ID="nameOfSender" data-form="<%$ HtmlEncodedCode: EmailConstants.SenderPropertyKey %>" />
            <div class="email-label" id="divEmailSenderFrom" runat="server">
                <asp:CheckBox ID="EmailSenderFromEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: EmailConstants.EmailFromPropertyKey %>"/>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_82) %>
            </div>
            <orion:EmailsInput ID="txtEmailFrom" runat="server" DataForm ="<%$ HtmlEncodedCode: EmailConstants.EmailFromPropertyKey %>"  MultipleMode="False" AllowMacros="True"/>
        </div>
    </div>
    <h3><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_79) %></h3>
    <div id="messageDetails" class="section required">
        <div class="email-label" id="divEmailSubject" runat="server">
            <asp:CheckBox ID="EmailSubjectEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: EmailConstants.SubjectPropertyKey %>"/>
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_29) %>
        </div>

        <div>
            <asp:TextBox runat="server" Width="82%" ID="txtEmailSubject" data-form="<%$ HtmlEncodedCode: EmailConstants.SubjectPropertyKey %>" />
            <orion:LocalizableButton ID="txtEmailSubjectInsertVariable" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IT0_1 %>"
                  DisplayType="Small" OnClientClick="return false" data-macro="<%$ HtmlEncodedCode: EmailConstants.SubjectPropertyKey %>" runat="server"/>
        </div>        
        
        <div class="email-label">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_91) %>
        </div>

        <div>
            <asp:TextBox runat="server" Width="82%" TextMode="MultiLine" Rows="5" ID="txtEmailMessage" data-form="<%$ HtmlEncodedCode: EmailConstants.EmailMessagePropertyKey %>" />
            <orion:LocalizableButton ID="LocalizableButton1" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IT0_1 %>"
                  DisplayType="Small" OnClientClick="return false" data-macro="<%$ HtmlEncodedCode: EmailConstants.EmailMessagePropertyKey %>" runat="server"/>
        </div>

        <div style="padding-bottom: 5px">
            <input type="radio" runat="server" ID="radioHtml" class="contentType" name="MessageFormat" enum-value="HTML" data-from="<%$ HtmlEncodedCode: EmailConstants.MessageContentTypePropertyKey %>" />
            <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TD0_3) %></label>
            <input type="radio" runat="server" ID="radioPlainText" class="contentType" name="MessageFormat" enum-value="PlainText"  data-from="<%$ HtmlEncodedCode: EmailConstants.MessageContentTypePropertyKey %>"/>
            <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TD0_4) %></label>
        </div>

        <asp:Panel ID="reportingMessageDetails" runat="server">
            
         <div style="padding-bottom: 5px; padding-top: 5px;">
            <input id="checkPrintable" runat="server" type="checkbox" data-form="<%$ HtmlEncodedCode: EmailUrlConstants.PrintablePropertyKey %>" />
            <label>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_326) %></label>
            <img title="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_327) %>" alt="" src="/Orion/images/Icon.Info.gif" />
        </div>
        
        <div style="float: left">
            <h4>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_33) %></h4>
            <div style="float: left;">
                <input id="checkPdf" runat="server" type="checkbox" data-form="<%$ HtmlEncodedCode: EmailUrlConstants.ContentTypePropertyKey %>" enum-value="PDF" />
                <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TD0_5) %><% if (DisablePdf) { %><img title="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_94) %>" alt="" src="/Orion/images/Icon.Info.gif" /><% } %></label>
                <input id="checkCsv" runat="server" type="checkbox" data-form="<%$ HtmlEncodedCode: EmailUrlConstants.ContentTypePropertyKey %>" enum-value="CSV" class="exgroup" onclick="showNotification()"/>
                <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TD0_6) %></label>
                <input id="checkExcel" runat="server" type="checkbox" data-form="<%$ HtmlEncodedCode: EmailUrlConstants.ContentTypePropertyKey %>" enum-value="Excel" class="exgroup" onclick="showNotification()" />
                <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TD0_7) %></label>
                <input id="checkHtml" runat="server" type="checkbox" data-form="<%$ HtmlEncodedCode: EmailUrlConstants.ContentTypePropertyKey %>" enum-value="HTML"/>
                <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TD0_3) %></label>
                <input id="checkLink" runat="server" type="checkbox" data-form="<%$ HtmlEncodedCode: EmailUrlConstants.ContentTypePropertyKey %>" enum-value="Link" />
                <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_34) %></label><br/>
                <span id="notifMessage" style="color: red; float: left"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_SO0_55) %></span>
            </div>
        </div>
        <div style="clear: both;">
        </div>
        <span id="checkboxValidation" style="display: none; color: red;">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_47) %></span>
      </asp:Panel>
        
         <asp:Panel ID="alertingMessageDetails" runat="server">         
            <div class="email-label" runat="server" id="divWebPageURL">
                <asp:CheckBox ID="WebPageURLEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: EmailWebPageConstants.WebPageURLPropertyKey %>"/>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_69) %>
            </div>
            <asp:TextBox runat="server" Width="97%" ID="txtEmailPageURL" data-form="<%$ HtmlEncodedCode: EmailWebPageConstants.WebPageURLPropertyKey %>"></asp:TextBox>
           <asp:Panel Width="97%" class="email-label" runat="server">
             <orion:HelpLink runat="server" HelpUrlFragment="OrionCoreCreateDynamicURLs" HelpDescription="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_VL0_70 %>" CssClass="coloredLink" />
             <div style="float: right">
            <orion:LocalizableButton runat="server" ID="PreviewButton" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VL0_76 %>" 
             data-form="<%$ HtmlEncodedCode: EmailWebPageConstants.PreviewURLPropertyKey %>"   DisplayType="Small" CausesValidation="false" OnClientClick="return false;" /> 
             </div>
           </asp:Panel> 
           <div class="email-label"  id="divUserID" runat="server">
                <asp:CheckBox ID="UserIDEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: EmailWebPageConstants.UserIDPropertyKey %>"/>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_71) %></div> 
           <div class="userAccount">
             <orion:UserAccountControl AllowNoUser="true" runat="server" ID="userAccountControl" /> 
           </div>                     
         </asp:Panel>

         <div id="emailPriorityDiv" class="emailPriority" runat="server">
            <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_229) %></label> <asp:DropDownList ID="emailPriority" data-form="<%$ HtmlEncodedCode: EmailConstants.Priority %>" Width="100" runat="server"/>           
        </div>
    </div>
    <h3>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_71) %></h3>
    <div id="SmtpSection" class="section required">
        <asp:CheckBox CssClass="alignCheckBox" ID="EmailSmtpEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: EmailConstants.SmtpServerIDPropertyKey %>"/>
        <div style="float: left">
        <br/>
            <orion:SmtpSettings runat="server" ID="SmtpSettings" />
        </div>
        <% if (Profile.AllowAdmin)
           { %>
        <div style="float: left; padding-left: 15px; padding-top: 22px;">
            <span>
                <img src="/Orion/Nodes/images/icons/icon_edit.gif" alt="" />
                <a href="/Orion/Admin/SmtpServersManager.aspx" class="coloredLink" target="_blank"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TD0_1) %></a>
            </span>
        </div>
        <% } %>
    </div>
</div>
<script type="text/javascript">
    // Self executing function creates local scope to avoid globals.
    (function() {
        var controller = new SW.Core.Actions.EmailUrlController({
            containerID : '<%= container.ClientID %>', 
            onReadyCallback : <%= OnReadyJsCallback %>,
            actionDefinition :  <%= ToJson(ActionDefinition, true) %>,
            viewContext :  <%= ToJson(ViewContext, true) %> ,
            environmentType : <%= ToJson(EnviromentType) %>,
            multiEditMode: <%= MultiEditEnabled.ToString().ToLower() %>,
            isEmailWebPage :<%= ToJson(IsEmailWebPage) %>,
            dataFormMapping : {
                nameOfSender: "<%= EmailConstants.SenderPropertyKey %>",
                emailTo: "<%= EmailConstants.EmailToPropertyKey %>",
                emailSubject: "<%= EmailConstants.SubjectPropertyKey %>",
                emailMessage: "<%= EmailConstants.EmailMessagePropertyKey %>",
                emailFrom: "<%= EmailConstants.EmailFromPropertyKey %>",
                emailCC: "<%= EmailConstants.EmailCCPropertyKey %>",
                emailBCC: "<%= EmailConstants.EmailBCCPropertyKey %>",
                stringContentType: "<%= EmailUrlConstants.ContentTypePropertyKey %>",
                messageContentType: "<%= EmailConstants.MessageContentTypePropertyKey %>",
                smtpServer: "<%= EmailConstants.SmtpServerIDPropertyKey %>",
                printable: "<%= EmailUrlConstants.PrintablePropertyKey %>",
                webPageURL: "<%= EmailWebPageConstants.WebPageURLPropertyKey %>",
                previewURL: "<%= EmailWebPageConstants.PreviewURLPropertyKey %>",
                userID: "<%= EmailWebPageConstants.UserIDPropertyKey %>",
                priority: "<%= EmailConstants.Priority %>"
            }
        });
        controller.init();
        SW.Core.MacroVariablePickerController.BindPicker();
        showNotification();
    })();
    function collapseExpandSenderInfo() {
        var div = document.getElementById('senderDetails');
        if (div.style.display == "none") {
            div.style.display = "block";
            $("#collapseExpandButton").attr('src','/Orion/images/Button.Collapse.gif');
            $("#senderInfo").text("");
        }
        else {
            div.style.display = "none";
            $("#collapseExpandButton").attr('src','/Orion/images/Button.Expand.gif');
            var infoText = String.format("{0}({1})", $(div).find('[data-form="<%= EmailConstants.SenderPropertyKey %>"]').val(), $(div).find('[data-form="<%= EmailConstants.EmailFromPropertyKey %>"]').val());
            if(infoText != "()")
                $("#senderInfo").text(infoText);
            else
                 $("#senderInfo").text("");
        }
    }    
    
    function previewUrl() {
            window.open($('#<%= txtEmailPageURL.ClientID %>').val(), '_blank');
        }
        
    function showAdditionalInput(param) {
        switch(param.id)
        {
            case '<%= btnCC.ClientID %>':
                $('#<%= ccContainer.ClientID %>').show();
                $('#<%= ccContainer.ClientID %>').find(".emailInputText").focus();
                $('#<%= btnCC.ClientID %>').hide();
            break;
            case '<%= btnBCC.ClientID %>':
                $('#<%= bccContainer.ClientID %>').show();
                $('#<%= bccContainer.ClientID %>').find(".emailInputText").focus();
                $('#<%= btnBCC.ClientID %>').hide();
            break;
        }
    }
    function hideAdditionalInput(param) {
        switch(param.id)
        {
            case 'ccClose':
                $('#<%= ccContainer.ClientID %>').hide();
                $('#<%= ccContainer.ClientID %>').find(".emailInputText").empty();
                $('#<%= ccContainer.ClientID %>').find("input:hidden[data-form='<%= EmailConstants.EmailCCPropertyKey %>']").val("");
                $('#<%= btnCC.ClientID %>').show();
            break;
            case 'bccClose':
                $('#<%= bccContainer.ClientID %>').hide();
                $('#<%= bccContainer.ClientID %>').find(".emailInputText").empty();
                $('#<%= bccContainer.ClientID %>').find("input:hidden[data-form='<%= EmailConstants.EmailBCCPropertyKey %>']").val("");
                $('#<%= btnBCC.ClientID %>').show();
            break;
        }
    }
        collapseExpandSenderInfo();
</script>
