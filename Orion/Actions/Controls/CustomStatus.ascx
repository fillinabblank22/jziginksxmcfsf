<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomStatus.ascx.cs" Inherits="Orion.Actions.Controls.Orion_Actions_Controls_CustomStatus" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Actions.Impl.CustomStatus" %>

<orion:Include ID="Include1" runat="server" File="Actions/js/CustomStatus.js" />

<style type="text/css">
    .CustomStatusAction .section { width: 600px; }    
    .CustomStatusAction .paragraph { padding-top: 10px; }
    .helpfulFlow { padding-bottom: 10px; }
    .radio label { margin-right: 30px; }

    .CustomStatusAction .UseCustomStatusCL {
        float:left;
    }
    .CustomStatusAction .UsePolledStatusCl {
        float:left;
    }
    .CustomStatusAction .SelectAffectedNodeLabel {
        float:left;clear: both;margin-top: 10px;
    }
    .CustomStatusAction .SelectAffectedNodeCL {
        float:left;clear: both;
    }
    .CustomStatusAction .SelectedNodeCL {
        float:left;clear: both;
    }
</style>

<div runat="server" id="container" class ="CustomStatusAction">
    <h3><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PR02_01) %></h3>
    <div id="CustomStatus" class="section required CustomStatus">
        <div class="helpfulFlow"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AV0_24) %></div>
        <div class="UseCustomStatusEd">
            <asp:CheckBox ID="chbUseCustomStatus" runat="server" data-edited ="<%$ HtmlEncodedCode: CustomStatusConstants.UsePolledStatusPropertyName %>"/>
        </div>
        <div class="UseCustomStatusCL">
            <asp:RadioButton runat="server" ID="UseCustomStatus" GroupName="NodeStatusGroup"/>
            <asp:Label runat="server" AssociatedControlID="NodeStatus" Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_PR02_03 %>"/>
         </div>
        <div id="NodeStatusCl" class="NodeStatusCl" runat="server">
            <asp:DropDownList ID="NodeStatus" data-form="<%$ HtmlEncodedCode: CustomStatusConstants.SelectedNodeStatusPropertyName %>" Width="100" runat="server"/>           
        </div>
        <div class="UsePolledStatusCl">
            <asp:RadioButton runat="server" ID="UsePolledStatus" GroupName="NodeStatusGroup" data-form="polls"/>
            <asp:Label runat="server" AssociatedControlID="NodeStatus" Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_PR02_06 %>"/>
        </div>
        <div class="SelectAffectedNodeLabel">
            <asp:Label runat="server" AssociatedControlID="NodeStatus" Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_SO_202 %>" Font-Bold="True"/>
        </div>
        <div class="SelectAffectedNodeCL">
            <asp:DropDownList ID="CbAffectedNode" name = "CbAffectedNode" ClientIDMode="Static" runat="server" />              
            <asp:HiddenField runat="server" ID="hfTargetNodeUri" ClientIDMode="static"/>
        </div>
        <div id="SelectedNodeCL" class="SelectedNodeCL" runat="server" >
            <a runat ="server" id="SelectNodeChooser" style=" color: #336699;text-decoration: underline;cursor:pointer" ClientIDMode="Static"  data-form="<%$ HtmlEncodedCode: CustomStatusConstants.TargetNodeUriPropertyName %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PR02_07) %></a>
        </div>
        <div id="insertVariableNetObjectPickerDlgCS" style="display: none">
            <div class='sw-btn-bar-wizard' style="margin-bottom: 0; padding-bottom: 0;">
                <orion:LocalizableButton ID="btnNetObjectSelectCS" CausesValidation="false" ClientIDMode="Static" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PR02_08 %>"/>
                <orion:LocalizableButton ID="btnNetObjectCancelCS" CausesValidation="false" ClientIDMode="Static" runat="server" DisplayType="Secondary" LocalizedText="Cancel" />
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
(function() {
        var controller = new SW.Core.Actions.CustomStatusController({
            containerID : '<%= container.ClientID %>', 
            onReadyCallback : <%= OnReadyJsCallback %>,
            actionDefinition : <%= ToJson(ActionDefinition, true) %>,
            multiEditMode: <%= MultiEditEnabled.ToString().ToLower() %>,
            viewContext : <%= ToJson(ViewContext, true) %>,
                dataFormMapping : {
                UsePolledStatus: "<%= CustomStatusConstants.UsePolledStatusPropertyName %>",
                SelectedNodeStatus: "<%= CustomStatusConstants.SelectedNodeStatusPropertyName %>",
                TargetNodeUri: "<%= CustomStatusConstants.TargetNodeUriPropertyName %>"
            }
        });
        controller.init();
    })();
</script>


