<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NetMessageView.ascx.cs" Inherits="Orion_Actions_Controls_NetMessageView" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Actions.Impl.NetMessage" %>
<orion:Include ID="Include1" runat="server" File="Actions/js/NetMessageController.js" />
<style type="text/css">
    .item-container input[type=text] { border: 1px solid gray; }
    .item-container textarea { border: 1px solid gray; }
    .item-box { margin-top: 15px }
    .item-smal-box { margin-top: 3px }
    .item-status-box {margin-top: 3px; height: 27px; width: 100% }
    #testResult div {
        margin: 0 !important;
    }
    .warningMessage {text-align: center; padding:20px}
</style>
<div runat="server" id="container" class="item-container">
    <h3><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB3_7) %></h3>
    <div id="actionPluginDefinition" runat="server" class="section required actionPluginDefinition">
        <div>
            <div class="item-box">
                <div ID="addressEditEnabledContainer" runat="server">
                    <input type="checkbox" ID="addressEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: NetMessageConstants.AddressKey %>"/>
                </div>
                <input type="checkbox" runat="server" ID="chSendToAll" data-form="<%$ HtmlEncodedCode: NetMessageConstants.SendToAllInDomainOrWorkgroupKey %>"/>
                <label for="<%= DefaultSanitizer.SanitizeHtml(chSendToAll.ClientID) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VM0_16) %></label>
                <div class="item-box">
                    <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VM0_15) %></label>
                    <asp:TextBox runat="server" Width="98%" ID="txtAddress" data-form="<%$ HtmlEncodedCode: NetMessageConstants.AddressKey %>" />
                    <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VM0_14) %></label>
                </div>
            </div>
            <div class="item-smal-box">
                <orion:LocalizableButton runat="server" ID="btnTestSendMessage" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources:CoreWebContent, WEBDATA_VM0_13 %>" DisplayType="Small" OnClientClick="return false;" />
                <div class="item-status-box">
                    <span id="testProgress" style="display: none;"><img src="/Orion/images/AJAX-Loader.gif" alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_11) %>"/><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_11) %></span>                        
                    <div id="testResult"></div>
                </div>
            </div>
            <div class="item-box">
                <input type="checkbox" ID="messageEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: NetMessageConstants.MessageKey %>"/>
                <label runat="server" id="lblMessage"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VM0_17) %></label>
                <asp:TextBox runat="server" Width="98%" TextMode="MultiLine" Rows="10" ID="txtMessage" data-form="<%$ HtmlEncodedCode: NetMessageConstants.MessageKey %>" />
                <div>
                    <orion:LocalizableButton ID="txtMessageInsertVariable" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IT0_1 %>"
                 DisplayType="Small" OnClientClick="return false" data-macro="<%$ HtmlEncodedCode: NetMessageConstants.MessageKey %>" runat="server"/>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="warningMessage" class="warningMessage" runat="server" Visible="false">
    <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VL0_75) %></label>
</div>
<script type="text/javascript">
    $(function() {
        var netMessageController = new SW.Core.Actions.NetMessageController({
                containerID: '<%= container.ClientID %>',
                onReadyCallback: <%= OnReadyJsCallback %> ,
                actionDefinition: <%= ToJson(ActionDefinition, true) %> ,
                viewContext: <%= ToJson(ViewContext, true) %> ,
                environmentType: <%= ToJson(EnviromentType) %> ,
                multiEditMode: <%= MultiEditEnabled.ToString().ToLower() %> ,
                isActionSupported: <%= IsActionSupported.ToString().ToLower() %>,
                testSendNetMessageClientId: '<%= btnTestSendMessage.ClientID %>',
                dataFormMapping: {
                    address: "<%= NetMessageConstants.AddressKey %>",
                    sendToAllInDomainOrWorkgroup: "<%= NetMessageConstants.SendToAllInDomainOrWorkgroupKey %>",
                    message: "<%= NetMessageConstants.MessageKey %>"
                }
            });
        netMessageController.init();
        SW.Core.MacroVariablePickerController.BindPicker();
    });
    
</script>