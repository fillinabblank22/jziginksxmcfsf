<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EmailView.ascx.cs"
    Inherits="Orion_Actions_Controls_EmailView" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Actions.Impl.Email" %>
<%@ Register TagPrefix="orion" TagName="SmtpSettings" Src="~/Orion/Controls/SmtpSettingsControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EmailsInput" Src="~/Orion/Controls/EmailsInput.ascx" %>
<orion:Include runat="server" File="Actions/js/EmailController.js" />
<style type="text/css">
    .email-label { padding-top: 10px; }
    .alignCheckBox { float: left; margin-top: 20px;}
    .disabledEdit { color: rgb(84, 84, 84);}
    #senderDetails  { width: 400px; background-color: #EFEFEF; margin-left: 20px; padding: 5px 15px 10px 10px; }
    #senderInfo  { margin: 0 5px -3px 0; overflow: hidden; white-space:nowrap; text-overflow:ellipsis; width:400px; display:inline-block; color: #646464; }
</style>
<div runat="server" id="container">
    <h3>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_27) %></h3>
    <div id="recipients" style="padding: 10px; width: 600px;" class="section required">
        <div runat="server" id="divEmailTo">
            <asp:CheckBox ID="EmailToEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: EmailConstants.EmailToPropertyKey %>"/>
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_28) %>
        </div>
        <orion:EmailsInput ID = "txtEmailTo" runat="server" DataForm ="<%$ HtmlEncodedCode: EmailConstants.EmailToPropertyKey %>" MultipleMode="True" AllowMacros="True"/>
         <div id="ccContainer" runat="server" style="display: none;">
            <div  runat="server" id="divCC">
                <asp:CheckBox ID="EmailCcEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: EmailConstants.EmailCCPropertyKey %>"/>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_31) %>
            </div>
            <table>
                <tr>
                    <td style="width: 570px;">
                        <orion:EmailsInput ID = "txtCC" runat="server" DataForm ="<%$ HtmlEncodedCode: EmailConstants.EmailCCPropertyKey %>"  MultipleMode="True" AllowMacros="True"/>
                    </td> 
                    <td style="padding-left: 10px; vertical-align: top;">
                        <span id="ccClose" onclick="hideAdditionalInput(this); return false;">
                        <img style="vertical-align: text-bottom; cursor: pointer; margin-top: 4px;" alt="" src="/Orion/images/delete_icon_Black16x16.png" />
                    </span>
                    </td>
                </tr>
            </table>
        </div>
        <div id="bccContainer" runat="server" style="display: none;">
            <div runat="server" id="divBcc">
                <asp:CheckBox ID="EmailBccEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: EmailConstants.EmailBCCPropertyKey %>"/>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_32) %>
            </div>
                <table>
                    <tr><td style="width: 570px;">
            <orion:EmailsInput ID = "txtBCC" runat="server" DataForm ="<%$ HtmlEncodedCode: EmailConstants.EmailBCCPropertyKey %>" MultipleMode="True" AllowMacros="True"/>
            </td>
            <td style="padding-left: 10px; vertical-align: top;">
            <span id="bccClose" onclick="hideAdditionalInput(this); return false;">
                <img style="vertical-align: text-bottom;cursor: pointer; margin-top: 4px;" alt="" src="/Orion/images/delete_icon_Black16x16.png" />
            </span>
            </td>
            </tr>
            </table>
        </div>
        <div class="sw-btn-bar">
            <orion:LocalizableButton runat="server" ID="btnCC" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_31 %>"
                DisplayType="Small" CausesValidation="false" OnClientClick="showAdditionalInput(this); return false;" />
            <orion:LocalizableButton runat="server" ID="btnBCC" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_32 %>"
                DisplayType="Small" CausesValidation="false" OnClientClick="showAdditionalInput(this); return false;" />
        </div>
        <span>
            <img alt="" style="margin: 0 5px -2px 0;" id = "collapseExpandButton" src="/Orion/images/Button.Collapse.gif" onclick="collapseExpandSenderInfo();"/>
            <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_85) %></label>
        </span><span id="senderInfo"></span> 
        <div id="senderDetails">
            <div class="email-label" runat="server" id="divEmailSender">
                <asp:CheckBox ID="EmailSenderNameEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: EmailConstants.SenderPropertyKey %>"/>
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_83) %> 
            </div>
            <asp:TextBox  runat="server" Width="100%" ID="nameOfSender" data-form="<%$ HtmlEncodedCode: EmailConstants.SenderPropertyKey %>" />
            <div class="email-label"  runat="server" id="divEmailSenderFrom">
                <asp:CheckBox ID="EmailSenderFromEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: EmailConstants.EmailFromPropertyKey %>"/>
                 <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_82) %>
            </div>
            <orion:EmailsInput ID="txtEmailFrom" runat="server" DataForm ="<%$ HtmlEncodedCode: EmailConstants.EmailFromPropertyKey %>"  MultipleMode="False" AllowMacros="True"/>
        </div>
    </div>
    <h3><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_79) %></h3>
    <div id="messageDetails" class="section required">
        <div class="email-label"  runat="server" id="divEmailSubject">
            <asp:CheckBox ID="EmailSubjectEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: EmailConstants.SubjectPropertyKey %>"/>
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_29) %>
        </div>

        <div>
            <asp:TextBox runat="server" Width="80%" ID="txtEmailSubject" data-form="<%$ HtmlEncodedCode: EmailConstants.SubjectPropertyKey %>" />
            <orion:LocalizableButton ID="btSubjectInsertVariable" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IT0_1 %>" DisplayType="Small" runat="server" data-macro ="<%$ HtmlEncodedCode: EmailConstants.SubjectPropertyKey %>" OnClientClick="return false"/>
        </div>
        <div class="email-label"  runat="server" id="divEmailMessage">
             <asp:CheckBox ID="EmailMessageEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: EmailConstants.EmailMessagePropertyKey %>"/>
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AB0_91) %>
        </div>

        <div>
            <asp:TextBox runat="server" Width="80%" TextMode="MultiLine" Rows="10" ID="txtEmailMessage" data-form="<%$ HtmlEncodedCode: EmailConstants.EmailMessagePropertyKey %>" />
            <orion:LocalizableButton ID="btMessageInsertVariable" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IT0_1 %>" DisplayType="Small" runat="server" data-macro = "<%$ HtmlEncodedCode: EmailConstants.EmailMessagePropertyKey %>" OnClientClick="return false"/>
        </div>
		<div class="validationMessage sw-suggestion sw-suggestion-fail" style="display: none;">
			<span class="sw-suggestion-icon"></span>
			<span style="color: red; font-size: 12px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0050) %></span>
		</div>

        <div style="padding-bottom: 5px">
            <input type="radio" runat="server" ID="radioHtml" class="contentType" name="MessageFormat" enum-value="HTML" data-from="<%$ HtmlEncodedCode: EmailConstants.MessageContentTypePropertyKey %>" />
            <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TD0_3) %></label>
            <input type="radio" runat="server" ID="radioPlainText" class="contentType" name="MessageFormat" enum-value="PlainText"  data-from="<%$ HtmlEncodedCode: EmailConstants.MessageContentTypePropertyKey %>"/>
            <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TD0_4) %></label>
        </div>
        
        <div id="emailPriorityDiv" class="emailPriority" runat="server">
            <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_229) %></label> <asp:DropDownList ID="emailPriority" data-form="<%$ HtmlEncodedCode: EmailConstants.Priority %>" Width="100" runat="server"/>           
        </div>
    </div>
    <h3>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_71) %></h3>
    <div id="SmtpSection" class="section required">
         <asp:CheckBox CssClass="alignCheckBox" ID="EmailSmtpEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: EmailConstants.SmtpServerIDPropertyKey %>"/>
        <div style="float: left">
        <br/>
            <orion:SmtpSettings runat="server" ID="SmtpSettings" />
        </div>
        <% if (Profile.AllowAdmin)
           { %>
        <div style="float: left; padding-left: 15px; padding-top: 22px;">
            <span>
                <img src="/Orion/Nodes/images/icons/icon_edit.gif" alt="" />
                <a href="/Orion/Admin/SmtpServersManager.aspx" class="coloredLink" target="_blank"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TD0_1) %></a>
            </span>
        </div>
        <% } %>
    </div>
</div>
<script type="text/javascript">
    // Self executing function creates local scope to avoid globals.
    (function() {
        var controller = new SW.Core.Actions.EmailController({
            containerID : '<%= container.ClientID %>', 
            onReadyCallback : <%= OnReadyJsCallback %>,
            actionDefinition :  <%= ToJson(ActionDefinition, true) %>,
            viewContext :  <%= ToJson(ViewContext, true) %> ,
            environmentType : <%= ToJson(EnviromentType) %>,
            multiEditMode: <%= MultiEditEnabled.ToString().ToLower() %>,
            dataFormMapping : {
                nameOfSender: "<%= EmailConstants.SenderPropertyKey %>",
                emailTo: "<%= EmailConstants.EmailToPropertyKey %>",
                emailSubject: "<%= EmailConstants.SubjectPropertyKey %>",
                emailMessage: "<%= EmailConstants.EmailMessagePropertyKey %>",
                emailFrom: "<%= EmailConstants.EmailFromPropertyKey %>",
                emailCC: "<%= EmailConstants.EmailCCPropertyKey %>",
                emailBCC: "<%= EmailConstants.EmailBCCPropertyKey %>",
                messageContentType: "<%= EmailConstants.MessageContentTypePropertyKey %>",
                smtpServer: "<%= EmailConstants.SmtpServerIDPropertyKey %>",
                priority: "<%= EmailConstants.Priority %>"
            }
        });
        controller.init();
        SW.Core.MacroVariablePickerController.BindPicker();
    })();
	
    function collapseExpandSenderInfo() {
        var div = document.getElementById('senderDetails');
        if (div.style.display == "none") {
            div.style.display = "block";
            $("#collapseExpandButton").attr('src','/Orion/images/Button.Collapse.gif');
            $("#senderInfo").text("");
        }
        else {
            div.style.display = "none";
            $("#collapseExpandButton").attr('src','/Orion/images/Button.Expand.gif');
            var infoText = String.format("{0}({1})", 
								encodeBrackets($(div).find('[data-form="<%= EmailConstants.SenderPropertyKey %>"]').val()), 
								encodeBrackets($(div).find('[data-form="<%= EmailConstants.EmailFromPropertyKey %>"]').val()));
            
			if(infoText != "()")
                $("#senderInfo").text(infoText);
            else
                 $("#senderInfo").text("");
        }
    }    
    
	function encodeBrackets(value) {
        if (!value || value.length === 0)
            return value;

        return value.replace(/{/g, "{{").replace(/}/g, "}}");
    }  
    
    function showAdditionalInput(param) {
        switch(param.id)
        {
            case '<%= btnCC.ClientID %>':
                $('#<%= ccContainer.ClientID %>').show();
                $('#<%= ccContainer.ClientID %>').find(".emailInputText").focus();
                $('#<%= btnCC.ClientID %>').hide();
            break;
            case '<%= btnBCC.ClientID %>':
                $('#<%= bccContainer.ClientID %>').show();
                $('#<%= bccContainer.ClientID %>').find(".emailInputText").focus();
                $('#<%= btnBCC.ClientID %>').hide();
            break;
        }
    }
    function hideAdditionalInput(param) {
        switch(param.id)
        {
            case 'ccClose':
                $('#<%= ccContainer.ClientID %>').hide();
                $('#<%= ccContainer.ClientID %>').find(".emailInputText").empty();
                $('#<%= ccContainer.ClientID %>').find("input:hidden[data-form='<%= EmailConstants.EmailCCPropertyKey %>']").val("");
                $('#<%= btnCC.ClientID %>').show();
            break;
            case 'bccClose':
                $('#<%= bccContainer.ClientID %>').hide();
                $('#<%= bccContainer.ClientID %>').find(".emailInputText").empty();
                $('#<%= bccContainer.ClientID %>').find("input:hidden[data-form='<%= EmailConstants.EmailBCCPropertyKey %>']").val("");
                $('#<%= btnBCC.ClientID %>').show();
            break;
        }
    }
        collapseExpandSenderInfo();
</script>
