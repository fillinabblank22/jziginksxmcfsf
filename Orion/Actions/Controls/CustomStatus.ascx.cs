﻿using System;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Orion.Core.Actions.Impl.CustomStatus;
using SolarWinds.Orion.Web.Actions;

namespace Orion.Actions.Controls
{
    public partial class Orion_Actions_Controls_CustomStatus : ActionPluginBaseView
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadControls();
            
            ApplyMultiEditMode();
            var targetNodeId = String.Empty;
            if (ActionDefinition != null)
            {
                var usePolledStatus = false;
                bool.TryParse(ActionDefinition.Properties.GetPropertyValue(CustomStatusConstants.UsePolledStatusPropertyName),
                    out usePolledStatus);
                var nodeStatus = ActionDefinition.Properties.GetPropertyValue(CustomStatusConstants.SelectedNodeStatusPropertyName);
                targetNodeId = ActionDefinition.Properties.GetPropertyValue(CustomStatusConstants.TargetNodeUriPropertyName);
                InitializeControls(usePolledStatus, nodeStatus, targetNodeId);
            }
            else
            {
                InitializeControls(true, CoreWebContent.WEBDATA_PR02_04, targetNodeId);
            }
        }

        public override string ActionTypeID
        {
            get { return CustomStatusConstants.ActionTypeID; }
        }

        private void LoadControls()
        {
            this.NodeStatus.Items.Add(CoreWebContent.WEBDATA_PR02_04); //up 
            this.NodeStatus.Items.Add(CoreWebContent.NodeCustomStatus_Warning); //warning 
            this.NodeStatus.Items.Add(CoreWebContent.NodeCustomStatus_Critical); //critical 
            this.NodeStatus.Items.Add(CoreWebContent.WEBDATA_PR02_05); //down

            CbAffectedNode.Items.Insert(0, new ListItem(CoreWebContent.WEBDATA_PR02_09));
            CbAffectedNode.Items.Insert(1, new ListItem(CoreWebContent.WEBDATA_PR02_10));
        }

        private void InitializeControls(bool usePolledStatus, string nodeStatus, string tartgetNodeId)
        {
            if (usePolledStatus)
            {
                UseCustomStatus.Checked = false;
                UsePolledStatus.Checked = true;
                NodeStatusCl.Style["disabled"] = "disabled";
            }
            else
            {
                UseCustomStatus.Checked = true;
                UsePolledStatus.Checked = false;
                NodeStatusCl.Attributes.Remove("disabled");
                NodeStatus.SelectedValue = nodeStatus;
            }

            if (String.IsNullOrEmpty(tartgetNodeId))
            {
                CbAffectedNode.SelectedIndex = 0;
                hfTargetNodeUri.Value = string.Empty;
                SelectedNodeCL.Style["display"] = "none";
            }
            else
            {
                CbAffectedNode.SelectedIndex = 1;
                hfTargetNodeUri.Value = tartgetNodeId;
                SelectedNodeCL.Style["display"] = "block";
            }
        }

        private void ApplyMultiEditMode()
        {
            chbUseCustomStatus.Visible = MultiEditEnabled;
        }
    }
}