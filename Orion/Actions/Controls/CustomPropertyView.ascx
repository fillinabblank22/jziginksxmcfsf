<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomPropertyView.ascx.cs" Inherits="Orion_Actions_Controls_CustomPropertyView" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Actions.Impl.CustomProperty" %>
<%@ Register TagPrefix="orion" TagName="EditCustomPropertyValueControl" Src="~/Orion/Controls/EditCustomPropertyValueControl.ascx" %>
<orion:Include runat="server" File="Actions/js/CustomPropertyController.js" />

<style type="text/css">
    .item-container { padding: 5px; width: 650px; }
    .item-box { margin-top: 15px; }
    .item-error { color: red; }
</style>
<div class="item-box item-error" ID="noCustomPropertiesErrorMsg" runat="server"  Visible="False">
    <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VM0_20) %></label>
</div>
<div runat="server" id="container" class="item-container">
    <h3><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB3_0) %></h3>
    <div class="item-box item-error" ID="differentCustomPropertiesTypeErrorMsg" runat="server"  Visible="False">
        <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VM0_21) %></label>
    </div>
    <div runat="server" id="dataContainer" class="section required">
        <input type="hidden" runat="server" ID="activeObjectTypeHd" data-form="<%$ HtmlEncodedCode: CustomPropertyConstants.ActiveObjectTypeKey %>"/>
        <input type="hidden" runat="server" ID="parentObjectTypeHd" data-form="<%$ HtmlEncodedCode: CustomPropertyConstants.ParentObjectTypeKey %>"/>
        <input type="hidden" runat="server" ID="objectIdTypeHd" data-form="<%$ HtmlEncodedCode: CustomPropertyConstants.ObjectTypeKey %>"/>
        <asp:CheckBox ID="CustomPropertyEditEnabled" data-edited="<%$ HtmlEncodedCode: CustomPropertyConstants.NameKey %>" runat="server"/>
        <div runat="server" id="parentCustomPropertyHint" Visible="True" ><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_JP0_2) %></div>
        <div class="item-box item-error" id="InvalidCustomPropertyMessage" Visible="False" runat="server">
            <asp:Label ID="InvalidCustomPropertyMessageText" runat="server"></asp:Label>
        </div>
        <div class="item-box" runat="server" id="parentCustomPropertySection" Visible="True">
            <asp:RadioButton runat="server" ID="parentCustomPropertyRadio" GroupName="customProperty" data-edited='parentCustomPropertyRadio'/>
            <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VM0_22) %></label><br/>
            <asp:DropDownList runat="server" ID="parentCustomProperyList" data-form="<%$ HtmlEncodedCode: CustomPropertyConstants.ParentNameKey %>"/>
        </div>
        <div class="item-box" runat="server" id="customPropertySection" Visible="True">
            <asp:RadioButton runat="server" ID="customPropertyRadio" GroupName="customProperty" data-edited='customPropertyRadio'/>
            <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VM0_18) %></label><br/>
            <asp:DropDownList runat="server" ID="customProperyList" data-form="<%$ HtmlEncodedCode: CustomPropertyConstants.NameKey %>"/>
        </div>
        <div class="item-box">
            <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VM0_19) %></label><br/>
            <div id="customProperyValueContainer">
                <orion:EditCustomPropertyValueControl ID="EditCp" runat="server"/>
            </div>
            <span id="testProgress" style="display: none;"><img src="/Orion/images/AJAX-Loader.gif" alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_11) %>"/><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_213) %></span>                        
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        var customPropertyController = new SW.Core.Actions.CustomPropertyController({
            containerID: '<%= container.ClientID %>',
            onReadyCallback: <%= OnReadyJsCallback %>,
            actionDefinition: <%= ToJson(ActionDefinition, true) %>,
            viewContext: <%= ToJson(ViewContext, true) %>,
            environmentType: <%= ToJson(EnviromentType) %>,
            customPropertyViewPath: '~/Orion/Controls/EditCustomPropertyValueControl.ascx',
            hasError: <%= HasError.ToString().ToLower() %>,
            isActionSupported: <%= IsActionSupported.ToString().ToLower() %>,
            multiEditMode: <%= MultiEditEnabled.ToString().ToLower() %>,
            dataContainerID: '<%= dataContainer.ClientID %>',
            dataFormMapping : {
                activeObjectType: "<%=  CustomPropertyConstants.ActiveObjectTypeKey %>",
                objectType: "<%=  CustomPropertyConstants.ObjectTypeKey %>",
                parentObjectType: "<%=  CustomPropertyConstants.ParentObjectTypeKey %>",
                name: "<%= CustomPropertyConstants.NameKey %>",
                parentName: "<%= CustomPropertyConstants.ParentNameKey %>",
                value: "<%= CustomPropertyConstants.ValueKey %>"
            }
        });
        customPropertyController.init();
    });    
</script>