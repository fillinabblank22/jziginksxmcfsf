<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TextToSpeechView.ascx.cs" Inherits="Orion.Actions.Controls.TextToSpeechView" %>

<%@ Import Namespace="SolarWinds.Orion.Core.Actions.Impl.TextToSpeech" %>

<orion:Include ID="Include1" runat="server" File="Actions/js/TextToSpeechController.js" />
<orion:Include runat="server" File="js/slider.js"/>

<style type="text/css">
    #textSection { padding: 10px; width: 500px; }
    #textSection .helpfulFlow { padding-bottom: 10px; }
</style>

<div runat="server" id="container" class="textToSpeechAction">
    <h3><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB3_6) %></h3>
    <div id="textSection" class="section required">
        <div class="helpfulFlow">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_337) %>
            <%= DefaultSanitizer.SanitizeHtml(TextToSpeechConstants.LinkToInstaller) %>
        </div>
        <div>
            <asp:CheckBox ID="speechTextEditEnabled" data-edited="<%$ HtmlEncodedCode: TextToSpeechConstants.TextPropertyKey %>" runat="server"/>
            <div runat="server" id="divText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_SO0_29) %></div>
            <div>
            <asp:TextBox ID="speechText" Width="80%" TextMode="MultiLine" Rows="10" data-from="<%$ HtmlEncodedCode: TextToSpeechConstants.TextPropertyKey %>" runat="server" data-form="<%$ HtmlEncodedCode: TextToSpeechConstants.TextPropertyKey %>"></asp:TextBox>
            <orion:LocalizableButton ID="AlertMessageInsertVariable" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IT0_1 %>"
                  DisplayType="Small" OnClientClick="return false" data-macro="<%$ HtmlEncodedCode: TextToSpeechConstants.TextPropertyKey %>" runat="server"/>
                  </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    // Self executing function creates local scope to avoid globals.
    (function() {
        var controller = new SW.Core.Actions.TextToSpeechController({
            containerID : '<%= container.ClientID %>', 
            onReadyCallback : <%= OnReadyJsCallback %>,
            actionDefinition :  <%= ToJson(ActionDefinition, true) %>,
            multiEditMode: <%= MultiEditEnabled.ToString().ToLower() %>,
            viewContext :  <%= ToJson(ViewContext, true) %>,
                 dataFormMapping: {
                    Text:"<%=TextToSpeechConstants.TextPropertyKey %>"
                }
        });

        controller.init();
    })();
    
</script>
