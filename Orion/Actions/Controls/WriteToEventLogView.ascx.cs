﻿using System;
using SolarWinds.Orion.Core.Actions.Impl.WriteToEventLog;
using SolarWinds.Orion.Web.Actions;

namespace Orion.Actions.Controls
{
    public partial class WriteToEventLogView : ActionPluginBaseView
    {
        public override string ActionTypeID
        {
            get { return WriteToEventLogConstants.ActionTypeID; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ApplyMultiEditMode();
            // Edit Mode
            if (ActionDefinition != null)
            {
                BindConfiguration();
            }
            else // New mode
            {
                SetDefaultValues();
            }
        }

        private void ApplyMultiEditMode()
        {
            messageEditEnabled.Visible = MultiEditEnabled;
            if (MultiEditEnabled)
            {
                labelMessage.Attributes.Add("style", "font-weight: bold;");
            }
        }

        private void SetDefaultValues()
        {
            if (ReportingViewContext != null)
            {
                tbMessage.Text = String.Empty;
            }
        }

        private void BindConfiguration()
        {
            tbMessage.Text =  ActionDefinition.Properties.GetPropertyValue(WriteToEventLogConstants.MessagePropertyKey);
        }
    }
}