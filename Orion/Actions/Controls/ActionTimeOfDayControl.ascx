<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActionTimeOfDayControl.ascx.cs" Inherits="Orion_Controls_ActionTimeOfDayControl" %>
<%@ Register TagPrefix="orion" TagName="FrequencyControl" Src="~/Orion/Controls/FrequencyControl.ascx" %>

<orion:Include ID="Include1" runat="server" File="Actions/js/ActionTimeOfDayController.js"/>
<div id="actionTimeOfDayContainer" class="hidden section">
    <h3 class="hintContentInfo"><span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_OM0_15) %></span><span class="contentInfo"></span></h3>
    <div>
        <div class="paragraph">
            <asp:CheckBox ID="timeOfDayEnabled" data-edited="timeOfDayEnabled" runat="server" />
            <div>
                <asp:RadioButton ID="NoSchedule" runat="server" GroupName="timeOfDay" data-form="NoSchedule" />
                <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_OM0_12) %></label>
            </div>
            <div>
                <asp:RadioButton ID="EnableTriggerResetSchedule" runat="server" GroupName="timeOfDay" data-form="EnableTriggerResetSchedule" />
                <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_OM0_13) %></span>
            </div>
            <div class="sw-text-helpful" style="margin-left: 15px"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_OM0_14) %></div>
        </div>
        <br />
        <div id="timePeriod" style="width: 99%">
            <orion:FrequencyControl runat="server" ID="FrequencyControl" />
        </div>
    </div>
</div>
