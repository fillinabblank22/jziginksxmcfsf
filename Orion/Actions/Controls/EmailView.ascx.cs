﻿using System;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Orion.Core.Actions.Impl.Email;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Models.Actions;
using SolarWinds.Orion.Core.Models.Actions.Contexts;
using SolarWinds.Orion.Web.Actions;
using SolarWinds.Orion.Core.Common.Models;
using WebSettingsDAL = SolarWinds.Orion.Web.DAL.WebSettingsDAL;

public partial class Orion_Actions_Controls_EmailView : ActionPluginBaseView
{
    private const string OrionNodesEntityTypeName = "Orion.Nodes";

    public override string ActionTypeID
    {
        get { return EmailConstants.ActionTypeID; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ApplyMultiEditMode();

        PopulateImportanceDDL();

        if (ActionDefinition != null) //edit
        {
            BindConfiguration();
        }
        else //new
        {
            SetDefaultValues();
        }
    }

    private void PopulateImportanceDDL()
    {
        ListItem item = new ListItem(CoreWebContent.WEBDATA_SO0_231, "0");
        emailPriority.Items.Add(item);
        item = new ListItem(CoreWebContent.WEBDATA_SO0_230, "1");
        emailPriority.Items.Add(item);
        item = new ListItem(CoreWebContent.WEBDATA_SO0_232, "2");
        emailPriority.Items.Add(item);
    }

    private void ApplyMultiEditMode() 
    {
        EmailToEditEnabled.Visible = MultiEditEnabled;
        EmailCcEditEnabled.Visible = MultiEditEnabled;
        EmailBccEditEnabled.Visible = MultiEditEnabled;
        EmailSenderNameEditEnabled.Visible = MultiEditEnabled;
        EmailSenderFromEditEnabled.Visible = MultiEditEnabled;
        EmailSubjectEditEnabled.Visible = MultiEditEnabled;
        EmailMessageEditEnabled.Visible = MultiEditEnabled;
        EmailSmtpEditEnabled.Visible = MultiEditEnabled;

        if (MultiEditEnabled)
        {
            divBcc.Attributes.Add("style", "font-weight: bold;");
            divCC.Attributes.Add("style", "font-weight: bold;");
            divEmailSender.Attributes.Add("style", "font-weight: bold;");
            divEmailSenderFrom.Attributes.Add("style", "font-weight: bold;");
            divEmailSubject.Attributes.Add("style", "font-weight: bold;");
            divEmailTo.Attributes.Add("style", "font-weight: bold;");
            divEmailMessage.Attributes.Add("style", "font-weight: bold;");
        }
    }

    private void SetDefaultValues()
    {
        var emailTo = WebSettingsDAL.GetValue(EmailConstants.DefaultEmailTo, string.Empty);
        txtEmailTo.Value = emailTo;

        var emailFrom = WebSettingsDAL.GetValue(EmailConstants.DefaultEmailFrom, EmailConstants.DefaultEmailFromValue);
        txtEmailFrom.Value = emailFrom;

        var emailSenderName = WebSettingsDAL.GetValue(EmailConstants.DefaultEmailSenderName, EmailConstants.DefaultEmailSenderNameValue);
        nameOfSender.Text = emailSenderName;

        var emailCc = WebSettingsDAL.GetValue(EmailConstants.DefaultEmailCc, string.Empty);
        ChangeCcAttributes(emailCc);
        txtCC.Value = emailCc;

        var emailBcc = WebSettingsDAL.GetValue(EmailConstants.DefaultEmailBcc, string.Empty);
        ChangeBccAttributes(emailBcc);
        txtBCC.Value = emailBcc;
        radioHtml.Checked = true;

        var context = this.ViewContext as AlertingActionContext;
        if (context != null)
        {
            if (context.ExecutionMode == ActionExecutionModeType.Trigger)
            {
                if (context.EntityType == OrionNodesEntityTypeName)
                {
                    txtEmailMessage.Text = CoreWebContent.WEBCODE_IT0_2;
                    txtEmailSubject.Text = CoreWebContent.WEBCODE_IT0_1;
                }
                else
                {
                    txtEmailMessage.Text = CoreWebContent.WEBCODE_OM0_01;
                    txtEmailSubject.Text = CoreWebContent.WEBCODE_TM0_121;
                }
            }
            if (context.ExecutionMode == ActionExecutionModeType.Reset)
            {
                if (context.EntityType == OrionNodesEntityTypeName)
                {
                    txtEmailSubject.Text = CoreWebContent.WEBCODE_IT0_3;
                    txtEmailMessage.Text = CoreWebContent.WEBCODE_IT0_4;
                }
                else
                {
                    txtEmailSubject.Text = CoreWebContent.WEBCODE_TM0_122;
                    txtEmailMessage.Text = CoreWebContent.WEBCODE_TM0_123;
                }
            }
        }
    }

    private void BindConfiguration()
    {
        txtEmailTo.Value = ActionDefinition.Properties.GetPropertyValue(EmailConstants.EmailToPropertyKey);
        txtEmailSubject.Text = (string)DefaultSanitizer.SanitizeHtml(ActionDefinition.Properties.GetPropertyValue(EmailConstants.SubjectPropertyKey));
        nameOfSender.Text = ActionDefinition.Properties.GetPropertyValue(EmailConstants.SenderPropertyKey);
        txtEmailFrom.Value = ActionDefinition.Properties.GetPropertyValue(EmailConstants.EmailFromPropertyKey);
        txtEmailMessage.Text = ActionDefinition.Properties.GetPropertyValue(EmailConstants.EmailMessagePropertyKey);

        ChangeCcAttributes(ActionDefinition.Properties.GetPropertyValue(EmailConstants.EmailCCPropertyKey));
        txtCC.Value = ActionDefinition.Properties.GetPropertyValue(EmailConstants.EmailCCPropertyKey);

        ChangeBccAttributes(ActionDefinition.Properties.GetPropertyValue(EmailConstants.EmailBCCPropertyKey));
        txtBCC.Value = ActionDefinition.Properties.GetPropertyValue(EmailConstants.EmailBCCPropertyKey);
        emailPriority.SelectedValue = ActionDefinition.Properties.GetPropertyValue(EmailConstants.Priority);

        string messageContentType = (string)OrionSerializationHelper.FromJSON(ActionDefinition.Properties.GetPropertyValue(EmailConstants.MessageContentTypePropertyKey), typeof(string));

        if (messageContentType != null)
        {
            MessageContentType messageContentTypeEnum = (MessageContentType)Enum.Parse(typeof(MessageContentType), messageContentType);

            switch (messageContentTypeEnum)
            {
                case MessageContentType.HTML:
                    radioHtml.Checked = true;
                    break;
                case MessageContentType.PlainText:
                    radioPlainText.Checked = true;
                    break;
            }
        }
        var smtpServerID = ActionDefinition.Properties.GetPropertyValue(EmailConstants.SmtpServerIDPropertyKey);
        SmtpSettings.SmtpServer = smtpServerID != EmailConstants.DefaultSMTPServerID
            ? new SmtpServer {ServerID = Convert.ToInt16(smtpServerID)}
            : null;
    }

    private void ChangeCcAttributes(string property)
    {
        if (!string.IsNullOrEmpty(property))
        {
            ccContainer.Attributes["style"] = "display:block";
            btnCC.Attributes["style"] = "display:none";
        }
        else
        {
            ccContainer.Attributes["style"] = "display:none";
            btnCC.Attributes["style"] = "display:inline-block";
        }
    }

    private void ChangeBccAttributes(string property)
    {
        if (!string.IsNullOrEmpty(property))
        {
            bccContainer.Attributes["style"] = "display:block";
            btnBCC.Attributes["style"] = "display:none";
        }
        else
        {
            bccContainer.Attributes["style"] = "display:none";
            btnBCC.Attributes["style"] = "display:inline-block";
        }
    }
}
