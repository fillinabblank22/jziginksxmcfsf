﻿using System;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Actions.Impl.ExecuteExternalProgram;
using SolarWinds.Orion.Core.Actions.Impl.ExecuteVBScript;
using SolarWinds.Orion.Web.Actions;

public partial class Orion_Actions_Controls_ExecuteVBScriptView : ActionPluginBaseView
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.ApplyMultiEditMode();

        this.LoadInterpreters();

        if (ActionDefinition != null)
        {
            // Edit Mode
            BindConfiguration();
        }
        else
        {
            // New mode
            SetDefaultCredentials();
        }
    }

    private void ApplyMultiEditMode()
    {
        InterpretersEditEnabled.Visible = MultiEditEnabled;
        FilePathEditEnabled.Visible = MultiEditEnabled;
        if (MultiEditEnabled)
        {
            labelFilePath.Attributes.Add("style", "font-weight: bold;");
            labelScriptInterpreter.Attributes.Add("style", "font-weight: bold;");
        }
    }

    public override string ActionTypeID
    {
        get { return ExecuteVBScriptConstants.ActionTypeID; }
    }

    private void LoadInterpreters()
    {
        this.ScriptInterpreters.Items.Add(new ListItem("WScript.exe"));
        this.ScriptInterpreters.Items.Add(new ListItem("CScript.exe"));
    }

    private void BindConfiguration()
    {
        if (this.MultiEditEnabled)
        {
            SetDefaultCredentials();
        }
        else
        {
            this.ScriptInterpreters.SelectedIndex = this.ActionDefinition.Properties.GetPropertyValue(
                ExecuteVBScriptConstants.InterpreterPropertyKey).
                Equals("WScript.exe")
                ? 0
                : 1;
            credentials.SetCredentials(this.ActionDefinition.Properties.GetPropertyValue(ExecuteVBScriptConstants.CredentialsPropertyKey));

            this.FilePath.Text =
                this.ActionDefinition.Properties.GetPropertyValue(ExecuteVBScriptConstants.FilePathPropertyKey);
        }
    }

    private void SetDefaultCredentials()
    {
        credentials.SetCredentials(ExecuteExternalProgramConstants.NotCredentialsDefinedId);
    }
}