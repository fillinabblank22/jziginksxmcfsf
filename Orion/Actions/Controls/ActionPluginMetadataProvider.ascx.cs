﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Models.Actions;
using SolarWinds.Orion.Web.Actions;

public partial class Orion_Actions_Controls_ActionPluginMetadataProvider : System.Web.UI.UserControl
{
    protected IEnumerable<ActionPlugin> Plugins 
    {
        get { return ActionManager.Instance.GetPlugins(); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    /// <summary>
    /// provide path to icon of specified action plugin
    /// </summary>
    /// <param name="actionTypeId">identifier of Action plugin</param>
    /// <param name="environmentType">Enviroment type limits set of action plugins which are supported for given environment</param>
    /// <returns>icon url path</returns>
    public string GetActionIconPath(string actionTypeId)
    {
        var selectedAction = this.Plugins.FirstOrDefault(p => p.ActionTypeID.Equals(actionTypeId));
        return selectedAction != null ? selectedAction.IconPath : string.Empty;
    }
}