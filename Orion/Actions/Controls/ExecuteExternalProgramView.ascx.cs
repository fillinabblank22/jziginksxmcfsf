﻿using System;
using SolarWinds.Orion.Core.Actions.Impl.ExecuteExternalProgram;
using SolarWinds.Orion.Web.Actions;

namespace Orion.Actions.Controls
{
    public partial class ExecuteExternalProgramView : ActionPluginBaseView
    {
        public override string ActionTypeID
        {
            get { return ExecuteExternalProgramConstants.ActionTypeID; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ApplyMultiEditMode();
            // Edit Mode
            if (ActionDefinition != null)
            {
                // Edit Mode
                BindConfiguration();
            }
            else
            {
                // New mode
                SetDefaultCredential();
            }
        }

        private void ApplyMultiEditMode()
        {
            programPathEditEnabled.Visible = MultiEditEnabled;
            if (MultiEditEnabled)
                Label1.Attributes.Add("style", "font-weight: bold;");
        }

        private void BindConfiguration()
        {
            if (MultiEditEnabled)
            {
                SetDefaultCredential();
            }
            else
            {
                ProgramPath.Text = ActionDefinition.Properties.GetPropertyValue(ExecuteExternalProgramConstants.ProgramPathPropertyKey);
                credentials.SetCredentials(ActionDefinition.Properties.GetPropertyValue(ExecuteExternalProgramConstants.CredentialsPropertyKey));
            }
        }

        private void SetDefaultCredential()
        {
            credentials.SetCredentials(ExecuteExternalProgramConstants.NotCredentialsDefinedId);
        }
    }
}