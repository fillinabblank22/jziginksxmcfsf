<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SendSyslogView.ascx.cs" Inherits="Orion_Actions_Controls_SendSyslogView" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Actions.Impl.SendSyslog" %>
<%@ Import Namespace="SolarWinds.Syslog.Contract" %>
<%@Register src="~/Orion/Controls/FacilityControl.ascx" tagName="Facilities" tagPrefix="orion" %>
    <%@Register src="~/Orion/Controls/SeverityControl.ascx" tagName="Severities" tagPrefix="orion" %>
<orion:Include ID="Include1" runat="server" File="Actions/js/SendSyslogController.js" />
<style type="text/css">
    .certificateSettings{ border: 1px dashed gray; margin-top: 10px; padding: 10px; }
    .helpfulText, .helpfulText td
    {
        color: #5f5f5f;
        font-size: 11px;
        padding: 5px 0px;
    }
    .message{width:98%; height: 100px;}
</style>
<div runat="server" id="container">
    <h3><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB3_5) %></h3>
    <div id="syslogSettings" class="section required blueBox">
        <div>
            <asp:CheckBox ID="hostsEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: SendSyslogConstants.HostsPropertyKey %>"/>
            <label runat="server" id="spanDestination"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_189) %></label>
            <asp:TextBox runat="server" Width="98%" ID="tbHosts" data-form="<%$ HtmlEncodedCode: SendSyslogConstants.HostsPropertyKey %>" /><br/>
            <span class="helpfulText" style="margin-top:10px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_190) %></span>
        </div>
        
        <div id="recipients" style="padding: 10px; width: 600px;">
        <table style="width: 100%;">
            <tr>
                <td colspan="2">
                    <asp:CheckBox ID="protocolEditEnabled" runat="server" data-edited="<%$ HtmlEncodedCode: SendSyslogConstants.ProtocolPropertyKey %>"/>
                    <label runat="server" id="spanProtocol"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_230) %>&nbsp;</label>
                    <asp:DropDownList runat="server" ID="protocol" AddShowAllItem="false" data-form="<%$ HtmlEncodedCode: SendSyslogConstants.ProtocolPropertyKey %>">
                        <asp:ListItem Value="<%$ HtmlEncodedCode: SyslogProtocol.Udp %>" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_231%>"></asp:ListItem>
                        <asp:ListItem Value="<%$ HtmlEncodedCode: SyslogProtocol.Tcp %>" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_232%>"></asp:ListItem>
                        <asp:ListItem Value="<%$ HtmlEncodedCode: SyslogProtocol.TcpOverTls %>" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_233%>"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="certificateSettings" >
                        <input type="checkbox" ID="ignoreCertificateChainErrors" runat="server" data-form="<%$ HtmlEncodedCode: SendSyslogConstants.IgnoreCertificateChainErrorsPropertyKey %>" /><label runat="server">&nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_234) %></label><br />
                        <input type="checkbox" ID="disableCheckCertificateRevocation" runat="server" data-form="<%$ HtmlEncodedCode: SendSyslogConstants.DisableCheckCertificateRevocationPropertyKey %>" /><label runat="server">&nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_235) %></label><br />
                        <input type="checkbox" ID="ignoreCertificateNameMismatch" runat="server" data-form="<%$ HtmlEncodedCode: SendSyslogConstants.IgnoreCertificateNameMismatchPropertyKey %>" /><label runat="server">&nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_236) %></label>
                    </div>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="severitiesEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: SendSyslogConstants.SeverityPropertyKey %>"/>
                    <label runat="server" id="spanSeverity"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_391) %>&nbsp;</label>
                    <orion:Severities runat="server" ID="severities" AddShowAllItem="false" DataForm="<%$ HtmlEncodedCode: SendSyslogConstants.SeverityPropertyKey %>"></orion:Severities>
                </td>
                <td style="text-align: right;">
                    <asp:CheckBox ID="facilitiesEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: SendSyslogConstants.FacilityPropertyKey %>"/>
                    <label runat="server" id="spanFacility"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_392) %>&nbsp;</label>
                    <orion:Facilities runat="server" ID="facilities" AddShowAllItem="false" DataForm="<%$ HtmlEncodedCode: SendSyslogConstants.FacilityPropertyKey %>"></orion:Facilities>
                </td>
            </tr>
        </table><br/>
            <asp:CheckBox ID="messageEditEnabled" runat="server" data-edited ="<%$ HtmlEncodedCode: SendSyslogConstants.MessagePropertyKey %>"/>
            <label  runat="server" id="spanMessage"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_191) %></label><br/>
            <asp:TextBox ID="tbMessage" CssClass="message" runat="server" TextMode="MultiLine" data-form="<%$ HtmlEncodedCode: SendSyslogConstants.MessagePropertyKey %>" MaxLength="400"></asp:TextBox>
            <div>
                 <orion:LocalizableButton ID="tbMessageInsertVariable" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IT0_1 %>"
                      DisplayType="Small" OnClientClick="return false" data-macro="<%$ HtmlEncodedCode: SendSyslogConstants.MessagePropertyKey %>" runat="server"/>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    // Self executing function creates local scope to avoid globals.
    (function() {
        SW.Core.Actions.SendSyslogSettingsController = new SW.Core.Actions.SendSyslogController({
                containerID: '<%= container.ClientID %>',
                onReadyCallback: <%= OnReadyJsCallback %> ,
                actionDefinition: <%= ToJson(ActionDefinition, true) %> ,
                viewContext: <%= ToJson(ViewContext, true) %> ,
                multiEditMode: <%= MultiEditEnabled.ToString().ToLower() %>,
                dataFormMapping: {
                    hosts: "<%= SendSyslogConstants.HostsPropertyKey %>",
                    facility: "<%= SendSyslogConstants.FacilityPropertyKey %>",
                    severity: "<%= SendSyslogConstants.SeverityPropertyKey %>",
                    message: "<%= SendSyslogConstants.MessagePropertyKey %>",
                    protocol: "<%= SendSyslogConstants.ProtocolPropertyKey %>",
                    ignoreCertificateChainErrors: "<%= SendSyslogConstants.IgnoreCertificateChainErrorsPropertyKey %>",
                    disableCheckCertificateRevocation: "<%= SendSyslogConstants.DisableCheckCertificateRevocationPropertyKey %>",
                    ignoreCertificateNameMismatch: "<%= SendSyslogConstants.IgnoreCertificateNameMismatchPropertyKey %>",
                }
            });
        SW.Core.Actions.SendSyslogSettingsController.init();
        SW.Core.MacroVariablePickerController.BindPicker();
    })();
</script>
