﻿using System;

public partial class Orion_Training : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Redirect("/Orion/Admin/WebIntegrationProxy.aspx?linkId=OrionTrainingEducation");
    }
}