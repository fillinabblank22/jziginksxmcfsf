using System;
using System.Linq;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Common;

public partial class ViewGroupSubNavTab : BaseViewGroupControl
{
    private string _tabHoverCssClass;

    private string _tabStyleCssClass;
    public bool IsSelected { get; set; }

    public ViewInfo ParentView { get; set; }

    protected string TabTitle => View.ViewShortTitle;

    protected bool IsAddTab => View.ViewID == 0;

    protected bool IsDemoMode => OrionConfiguration.IsDemoServer;

    protected IconInfo TabIconInfo => new IconInfo(View);

    protected string TabHoverCssClass
    {
        get
        {
            _tabHoverCssClass = "SubNav-Tab SubNav-TabHover";

            if (IsSelected)
                _tabHoverCssClass += " SubNav-SelectedTab";

            return _tabHoverCssClass;
        }
    }

    protected string TabStyleCssClass
    {
        get
        {
            _tabStyleCssClass = "SubNav-TabCenter";

            if (IsSelected)
                _tabStyleCssClass += " SubNav-TabSelected";

            return _tabStyleCssClass;
        }
    }

    protected override void RebuildUI()
    {
        if (RelatedViews == null)
            return;

        btnDelete1.Visible =
            !View.IsSystem &&
            RelatedViews.Count() > 2; //Delete is allowed if not system and not the last tab in the group
        btnMoveDown1.Visible = GetIndexOfCurrentView() != RelatedViews.Count() - 2;
        btnMoveUp1.Visible = GetIndexOfCurrentView() != 0;
    }

    private void Delete()
    {
        var parentView = ViewManager.GetFirstViewInGroup(View.ViewGroup);
        ViewManager.DeleteView(View);
        RefreshPage(parentView.ViewID);
    }

    private void MoveUp()
    {
        var indexOfPreviousView = GetIndexOfCurrentView() - 1;
        //first in list goes up
        var view2 = RelatedViews.ElementAt(indexOfPreviousView);
        if (view2.ViewGroupPosition == View.ViewGroupPosition)
            view2.ViewGroupPosition++;
        ViewManager.SwapViewGroupPositions(View, view2);
        RefreshPage();
    }

    private void MoveDown()
    {
        var indexOfPreviousView = GetIndexOfCurrentView() + 1;
        //first in list goes up
        var view1 = RelatedViews.ElementAt(indexOfPreviousView);
        if (view1.ViewGroupPosition == View.ViewGroupPosition)
            View.ViewGroupPosition++;

        ViewManager.SwapViewGroupPositions(view1, View);
        RefreshPage();
    }

    protected int GetIndexOfCurrentView()
    {
        for (var x = 0; x < RelatedViews.Count - 1; x++)
            if (RelatedViews.ElementAt(x).ViewID == View.ViewID)
                return x;
        return -1;
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        Delete();
    }

    protected void btnMoveUp_Click(object sender, EventArgs e)
    {
        MoveUp();
    }

    protected void btnMoveDown_Click(object sender, EventArgs e)
    {
        MoveDown();
    }
}