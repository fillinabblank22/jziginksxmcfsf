﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI.Localizer;

public partial class Orion_Resource : System.Web.UI.Page
{
	private string resorcePath;
	private string resourceTitle;

	protected override void OnPreInit(EventArgs e)
	{
		base.OnPreInit(e);

		if (!string.IsNullOrEmpty(Request.QueryString["Resource"]))
		{
			resorcePath = Request.QueryString["Resource"].Trim();
		}
		else
		{
			throw new InvalidOperationException("Resource path is missing from Query String.");
		}	
	}

	private string GetTitleFromPath(string resorcePath)
	{
		string[] array = resorcePath.Split('/');
		foreach (string str in array)
		{
			if (str.Contains(".aspx") || str.Contains(".ascx"))
			{
				return str.Substring(0, str.IndexOf(".as"));
			}
		}
		return string.Empty;
	}

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		ResourceInfo resource = new ResourceInfo();
		resource.ID = 0;
		resource.View = new ViewInfo("", "", "", false, false, null);
		resource.View.ViewID = 0;
		resource.Column = 1;
		resource.View.ColumnCount = 1;
		resource.View.Column1Width = 800;

		this.divContainer.Style[HtmlTextWriterStyle.Width] = string.Format("{0}px", 800);
		this.divContainer.Style[HtmlTextWriterStyle.MarginLeft] = "12px";

		BaseResourceControl ctrl = (BaseResourceControl)LoadControl(resorcePath);
		ctrl.Resource = resource;
		ResourceHostControl host = ResourceHostManager.GetSupportingControl(ctrl);

		this.divContainer.Controls.Add(host);
		host.LoadFromRequest();

		host.Controls.Add(ctrl);

        resourceTitle = Request.QueryString["Title"];
        if (string.IsNullOrEmpty(resourceTitle))
            resourceTitle = ctrl.Title;
        if (string.IsNullOrEmpty(resourceTitle))
            resourceTitle = GetTitleFromPath(resorcePath);
        this.Title = resourceTitle;

		HtmlMeta metaTag = new HtmlMeta();
		metaTag.HttpEquiv = "REFRESH";
		metaTag.Content = string.Format("{0}; URL='{1}'", WebSettingsDAL.AutoRefreshSeconds, this.Request.Url.ToString());
		this.Page.Header.Controls.Add(metaTag);

        OrionInclude.CoreThemeStylesheets();
	}
}
