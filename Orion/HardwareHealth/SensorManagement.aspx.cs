﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_HardwareHealth_SensorManagement : System.Web.UI.Page
{

    protected readonly double SensorLimit = 1000;

    public double SelectedSensorsLimit
    {
        get
        {
            var setting = SettingsDAL.GetSetting("HardwareHealth.SelectedSensorsLimit");

            if (setting != null)
            {
                return setting.SettingValue;
            }

            return SensorLimit;
        }
    }
    

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}