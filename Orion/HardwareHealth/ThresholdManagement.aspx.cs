﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_HardwareHealth_ThresholdManagement : System.Web.UI.Page
{
    public string ReturnUrl { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        // store the URL (with query arguments) so we are able to get back it it in the same form.
        ReturnUrl = CancelButton.NavigateUrl = Request.UrlReferrer.Segments.Last().Contains("SensorManagement.aspx")? Request.UrlReferrer.AbsoluteUri : "SensorManagement.aspx";
    }
}