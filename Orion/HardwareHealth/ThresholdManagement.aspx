﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ThresholdManagement.aspx.cs" Inherits="Orion_HardwareHealth_ThresholdManagement" MasterPageFile="~/Orion/HardwareHealth/hardwareHealthMasterPage.master"
    Title="<%$ Resources: HardwareHealthWebContent, HHWEBDATA_GK0_1 %>" %>

<%@ Import Namespace="SolarWinds.Orion.Common" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>


<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:Include ID="Include3" runat="server" File="OrionMinReqs.js" />
    <orion:Include ID="Include1" runat="server" File="OrionCore.js" />
    <orion:Include ID="Include4" runat="server" Module="HardwareHealth" File="styles/ThresholdManagement.css" />
    <orion:Include ID="Include2" runat="server" File="MainLayout.css" />
    <orion:Include ID="Include6" runat="server" File="NestedExpressionBuilder.css" />
    <orion:Include ID="Include8" runat="server" File="NestedExpressionBuilder.js" />
    <orion:Include ID="Include12" runat="server" Module="HardwareHealth" File="js/ThresholdManagement/neb.input.field.sensorvalue.js" />
    <orion:Include ID="Include5" runat="server" Module="HardwareHealth" File="js/ThresholdManagement/ThresholdManagement.js" />
    <script type="text/javascript">
        SW.Core.namespace("SW.HardwareHealth").IsDemo = ("<%= OrionConfiguration.IsDemoServer %>".toLowerCase() === "true") ? true : false;
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <asp:scriptmanagerproxy id="ScriptManagerProxy1" runat="server">
        <Services>
            <asp:ServiceReference Path="/Orion/Services/Information.asmx" />
            <asp:ServiceReference Path="/Orion/Services/WebAdmin.asmx" />
        </Services>
    </asp:scriptmanagerproxy>

    <div class="header">
        <h1 class="sw-hdr-title"><%=Page.Title%></h1>
        <div class="subtitle"></div>
    </div>
    <div id="thresholdContent">
        <p><%= Resources.HardwareHealthWebContent.HHWEBDATA_GK0_2 %></p>

        <div id="noItemsErrorPlaceholder" class="sw-suggestion sw-suggestion-fail">
            <span class="sw-suggestion-icon"></span>
            <span id="noItemsErrorText"></span>
        </div>

        <div id="multipleItemsWarningPlaceholder" class="sw-suggestion sw-suggestion-warn">
            <span class="sw-suggestion-icon"></span>
            <span id="multipleItemsWarningText"></span>
        </div>

        <ul id="thresholdOptionsList">
            <li>
                <label class="thresholdRadio">
                    <input type="radio" name="thresholdRadio" id="thresholdRadioUseOrion" value="useOrion" checked /><%= Resources.HardwareHealthWebContent.HHWEBDATA_GK0_3 %></label>
                - <%= Resources.HardwareHealthWebContent.HHWEBDATA_GK0_4 %>
            </li>
            <li>
                <label class="thresholdRadio">
                    <input type="radio" name="thresholdRadio" id="thresholdRadioUseUp" value="useUp" /><%= Resources.HardwareHealthWebContent.HHWEBDATA_GK0_5 %></label>
                - <%= Resources.HardwareHealthWebContent.HHWEBDATA_GK0_6 %>
            </li>
            <li>
                <label class="thresholdRadio">
                    <input type="radio" name="thresholdRadio" id="thresholdRadioUseCustom" value="useCustom" /><%= Resources.HardwareHealthWebContent.HHWEBDATA_GK0_7 %></label>
                - <%= Resources.HardwareHealthWebContent.HHWEBDATA_GK0_8 %>
            </li>
        </ul>

        <div id="incompatibleUnitsWarningPlaceholder" class="sw-suggestion sw-suggestion-warn">
            <span class="sw-suggestion-icon"></span>
            <span id="incompatibleWarningText"></span>
        </div>

        <div id="sensorsWithoutValuesWarningPlaceholder" class="sw-suggestion sw-suggestion-warn">
            <span class="sw-suggestion-icon"></span>
            <span id="sensorsWithoutValuesWarningList"></span>
            <span id="moreSensorsWithoutValuesWarningList" class="hidden"></span>
            <span id="moreSensorsWithoutValuesWarningNote" class="hidden">
                <span class="LinkArrow">»</span>
                <%= string.Format(Resources.HardwareHealthWebContent.HHWEBCODE_PS0_1, 20) %>
            </span>
            <span id="sensorsWithoutValuesWarningText"></span>
        </div>

        <div id="thresholdsPlaceholder">
            <div id="thresholdTemplateSelectorHolder"></div>
            <div class="sw_undpthr_wrapper_main">
                <h2 id="sw_undpthr_pollerTitle"></h2>
                <div id="sw_neb_thresholds_warning"></div>
                <div class="sw_undpthr_clear"></div>
                <div id="sw_neb_thresholds_critical"></div>
            </div>
            
            <div id="emptyThresholdsSetPlaceHolder" class="sw-suggestion sw-suggestion-fail">
            <span class="sw-suggestion-icon"></span>
            <%= Resources.HardwareHealthWebContent.HHWEBDATA_GK0_13 %>
        </div>
        </div>
        
        <div id="progressDiv" class="hidden">
            <img src="../images/animated_loading_sm3_whbg.gif">
            <span style="font-weight:bold;"><%= Resources.CoreWebContent.WEBDATA_AK0_213 %></span>
        </div>
        <div class="sw-btn-bar">
            <orion:LocalizableButtonLink DisplayType="Primary" LocalizedText="Submit" runat="server" CssClass="thresholdSubmitButton" ID="SubmitButton" />
            <orion:LocalizableButtonLink DisplayType="Secondary" NavigateUrl="SensorManagement.aspx" CssClass="NoTip" LocalizedText="Cancel" runat="server" ID="CancelButton" />
        </div>
    </div>

    <script type="text/javascript">
        $(function () {
            SW = SW || {};
            SW.HWH = SW.HWH || {};

            ORION.prefix = 'ThresholdManagement_';

            SW.HWH.returnTo = ORION.Prefs.load('ReturnTo', 'SensorManagement.aspx');          

            SW.HWH.Thresholds = SW.HWH.Thresholds || {};
            SW.HWH.Thresholds.NEBGroupOpers = <%= Newtonsoft.Json.JsonConvert.SerializeObject((new SolarWinds.Orion.Core.Reporting.OperatorProvider()).GetGroupOperators().ToArray()) %>;
            SW.HWH.Thresholds.NEBLogicalOpers = <%= Newtonsoft.Json.JsonConvert.SerializeObject((new SolarWinds.Orion.Core.Reporting.OperatorProvider()).GetLogicalOperators().ToArray()) %>;
            SW.HWH.Thresholds.NEBFieldConstant = JSON.parse("{\"NodeType\":0,\"Value\":null,\"Child\":[{\"NodeType\":1,\"Value\":null,\"Child\":null},{\"NodeType\":2,\"Value\":null,\"Child\":null}]}");
            SW.HWH.Thresholds.NEBAddConditionTitle = "<%= Resources.HardwareHealthWebContent.HHWEBDATA_GK0_11 %>";
            SW.HWH.Thresholds.NUM_FIELD = "<%= Resources.HardwareHealthWebContent.HHWEBDATA_GK0_12 %>";
            SW.HWH.Thresholds.ValueType = SW.HWH.Thresholds.NUM_FIELD; //setting default to Number
            SW.HWH.Thresholds.SelectedSensorId = 1;
            SW.HWH.Thresholds.SelectedPollerName = "";
            SW.HWH.Thresholds.SearchValue = "";
            SW.HWH.Thresholds.WarningNEB = $("#sw_neb_thresholds_warning");
            SW.HWH.Thresholds.CriticalNEB = $("#sw_neb_thresholds_critical");
            SW.HWH.Thresholds.WarningTitle = "<%= Resources.HardwareHealthWebContent.HHWEBDATA_GK0_9 %>";
            SW.HWH.Thresholds.CriticalTitle = "<%= Resources.HardwareHealthWebContent.HHWEBDATA_GK0_10 %>";
            SW.HWH.Thresholds.NotificationWrapper = $("#sw_undpthr_notification");
            SW.HWH.Thresholds.NotificationSuccess = $("Success");
            SW.HWH.Thresholds.NotificationFail = $("Failed");
            SW.HWH.Thresholds.NotificationValidationFail = $("Validation failed");
            SW.HWH.Thresholds.IsDemo = false;

            // RENDER BOTH NEB INSTANCES
            SW.HWH.Thresholds.RenderNEBInstances = function (threshold) {
                if (threshold) {
                    SW.HWH.Thresholds.InitNEB(SW.HWH.Thresholds.CriticalNEB, threshold.CriticalExpression, SW.HWH.Thresholds.CriticalTitle, "#FACECE");
                    SW.HWH.Thresholds.InitNEB(SW.HWH.Thresholds.WarningNEB, threshold.WarningExpression, SW.HWH.Thresholds.WarningTitle, "#FACECE");
                } else {
                    SW.HWH.Thresholds.InitNEB(SW.HWH.Thresholds.CriticalNEB, null, SW.HWH.Thresholds.CriticalTitle, "#FACECE");
                    SW.HWH.Thresholds.InitNEB(SW.HWH.Thresholds.WarningNEB, null, SW.HWH.Thresholds.WarningTitle, "#FACECE");
                }
            };

            SW.HWH.Thresholds.InitNEB = function (target, expression, instanceTitle, color) {
                target.empty();
                var nebConfig = {                    
                    allowGroups: true,
                    groupOperators: SW.HWH.Thresholds.NEBGroupOpers,
                    logicalOperators: SW.HWH.Thresholds.NEBLogicalOpers,
                    ruleTypes: [
                                { displayName: SW.HWH.Thresholds.NEBAddConditionTitle, expr: SW.HWH.Thresholds.NEBFieldConstant }
                    ],
                    fieldInput: SW.Core.NestedExpressionBuilder.Input.Field.SensorValue,
                    disableAutocomplete: true,
                    sensorValueType: SW.HWH.Thresholds.ValueType,
                    decimal_separator: Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator,
                    preferenceOptions: []
                };
                if (expression) {
                    // NEB WITH EXPRESSION
                    nebConfig.expr = expression;
                }
                target.nested_expression_builder(nebConfig);
                $("<h4>").html(instanceTitle).prependTo(target);
                $("<div class='sw_undpthr_coloredBar'></div>").attr("style", "background-color:" + color).prependTo(target);
            };

            // VALIDATION
            SW.HWH.Thresholds.Validate = function () {

                var warningIsEmpty = SW.HWH.Thresholds.WarningNEB.nested_expression_builder("getExpression") === null;
                var criticalIsEmpty =  SW.HWH.Thresholds.CriticalNEB.nested_expression_builder("getExpression") === null;

                if (warningIsEmpty && criticalIsEmpty) {
                    $('#emptyThresholdsSetPlaceHolder').show();
                    return false;
                } else {
                    $('#emptyThresholdsSetPlaceHolder').hide();
                }

                var warningIsValid = SW.HWH.Thresholds.WarningNEB.nested_expression_builder("validateConstantValues");
                var criticalIsValid = SW.HWH.Thresholds.CriticalNEB.nested_expression_builder("validateConstantValues");
                if (warningIsValid && criticalIsValid) {
                    SW.HWH.Thresholds.NotificationWrapper.children().hide();
                    return true;
                }
                else return false;
            };

            SW.HWH.Thresholds.SelectedPollerName = "My sensor thresholds";
            SW.HWH.Thresholds.NotificationWrapper.children().hide();
        });
    </script>

    <input type="hidden" name="ThresholdManagement_ReturnTo" id="ThresholdManagement_ReturnTo" value="<%= ReturnUrl %>" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
</asp:Content>
