﻿<%@ WebService Language="C#" Class="ChartData" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.HardwareHealth.Common;
using SolarWinds.HardwareHealth.Common.Models.HardwareHealth;
using SolarWinds.HardwareHealth.Web.Shared.Controls.HardwareHealthTree;
using SolarWinds.HardwareHealth.Web.Shared.DAL;
using SolarWinds.HardwareHealth.Web.Utility;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.HardwareHealth.Common.Models;
using SolarWinds.HardwareHealth.Web;
using SolarWinds.HardwareHealth.Web.UI.Resource;

using HWHCharting = SolarWinds.HardwareHealth.Web.Charting;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class ChartData : ChartDataWebService
{
    private static SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();

    // @NodeID - list of NetObjetIDs from request, which are actually SensorIDsByUnit
    private const string sensorNameLookupSwql = "select ID, DisplayName from Orion.HardwareHealth.HardwareItemBase where ID in @NodeID";

    private string sqlString = @"
SELECT [TimeStamp], [AvgValue] 
FROM APM_HardwareItemValueStatistics
WHERE HardwareItemID = @NetObjectId
AND [TimeStamp] >= @StartTime AND [TimeStamp] <= @EndTime
ORDER BY [TimeStamp]";

    private HardwareHealthDal dal;
    private HardwareHealthDal HardwareDal
    {
        get
        {
            if(dal == null )
            {
                dal = new HardwareHealthDal();
            }
            return dal;
        }
    }

    [WebMethod]
    public ChartDataResults GetDataSeries(ChartDataRequest request)
    {
        string serieTag = string.Empty;

        var properties = (Dictionary<string,object>)request.AllSettings["ResourceProperties"];

        if (properties.ContainsKey("chartname") && string.Compare((string)properties["chartname"], "HardwareHealthTemperature", true, CultureInfo.InvariantCulture) == 0)
        {
            // 'Temperature' tag returns exported data always in celcius degrees, no Fahrenheits -> 'Max_Statistic_Data' returns data without units
            //serieTag = "Temperature";
            serieTag = "Max_Statistic_Data";
        }
        else if (properties.ContainsKey("chartname") && string.Compare((string)properties["chartname"], "HardwareHealthFans", true, CultureInfo.InvariantCulture) == 0)
        {
            serieTag = "Fan";
        }
        else if (properties.ContainsKey("chartname") && string.Compare((string)properties["chartname"], "HardwareHealthPowerSupplyA", true, CultureInfo.InvariantCulture) == 0)
        {
            serieTag = "Power_Supply_A";
        }
        else if (properties.ContainsKey("chartname") && string.Compare((string)properties["chartname"], "HardwareHealthPowerSupplyV", true, CultureInfo.InvariantCulture) == 0)
        {
            serieTag = "Power_Supply_V";
        }
        else if (properties.ContainsKey("chartname") && string.Compare((string)properties["chartname"], "HardwareHealthPowerSupplyW", true, CultureInfo.InvariantCulture) == 0)
        {
            serieTag = "Power_Supply_W";
        }


        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, sqlString, sensorNameLookupSwql, DateTimeKind.Utc, serieTag);

        double minVal;
        double maxVal;
        bool noPoints = CalculateMinMaxValueForDataSeries(result.DataSeries, out minVal, out maxVal);

        minVal=Math.Min(minVal,0);		//when no points result is double.MaxValue

        //if (string.Compare(serieTag, "Temperature", true, CultureInfo.InvariantCulture) == 0)
        if (string.Compare(serieTag, "Max_Statistic_Data", true, CultureInfo.InvariantCulture) == 0)
        {
            if(result.ChartOptionsOverride == null)
                result.ChartOptionsOverride = new JsonObject();

            dynamic options = result.ChartOptionsOverride as JsonObject;
            if(options==null)
                _log.Error("ChartOptionsOverride is not a JsonObject");

            options.yAxis = JsonObject.CreateJsonArray(1);
            options.yAxis[0].min = minVal;
            options.yAxis[0].minRange = 10;

            if(GetSavedTemperatureUnit() == HardwareHealthUnit.DegreesF)
            {
                options.yAxis[0].unit = Resources.HardwareHealthWebContent.HHWEBCODE_DO0_4;
                options.yAxis[0].title.text = Resources.HardwareHealthWebContent.HHWEBCODE_DO0_6;
                result.ChartOptionsOverride = options;

                ConvertTemperatureUnit(result);
            }
            else
            {
                options.yAxis[0].unit = Resources.HardwareHealthWebContent.HHWEBCODE_DO0_5;
                options.yAxis[0].title.text = Resources.HardwareHealthWebContent.HHWEBCODE_DO0_7;
                result.ChartOptionsOverride = options;
            }
        }

        return result;
    }

    private HardwareHealthUnit GetSavedTemperatureUnit()
    {
        return EnumHelper.GetEnumValue(SolarWinds.HardwareHealth.Web.HardwareHealthRoleAccessor.TemperatureUnit, HardwareHealthUnit.DegreesF);
    }

    private void ConvertTemperatureUnit(ChartDataResults result)
    {
        var series = result.DataSeries;

        var convertedDataSeries = new List<DataSeries>();

        foreach (var dataSerie in series)
        {
            var convertedData = new DataSeries();
            convertedData.Label = dataSerie.Label;
            convertedData.NetObjectId = dataSerie.NetObjectId;
            convertedData.ShowTrendLine = dataSerie.ShowTrendLine;
            convertedData.TagName = dataSerie.TagName;

            foreach (var dataPoint in dataSerie.Data)
            {
                if(!dataPoint.IsNullPoint)
                {
                    var convertedDataPoint = DataPoint.CreatePoint(dataPoint.UnixTime,
                                                                   HardwareHealthTreeHelper.ConvertTemperature(
                                                                       dataPoint.Value, HardwareHealthUnit.DegreesC,
                                                                       HardwareHealthUnit.DegreesF));

                    convertedData.AddPoint(convertedDataPoint);
                }
                else
                {
                    convertedData.AddPoint(dataPoint);
                }
            }

            convertedDataSeries.Add(convertedData);
        }

        result.DataSeries = convertedDataSeries;
    }

    [WebMethod]
    public ChartDataResults GetAvailabilityDataSeries(ChartDataRequest request)
    {
        DateRange dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);

        int hardwareItem = 0;
        if (request.NetObjectIds.Length > 0)
        {
            Int32.TryParse(request.NetObjectIds[0], out hardwareItem);
        }
        if (hardwareItem == 0)
        {
            throw new ArgumentException("No Location ID is defined in request.");
        }

        DataTable dt = HardwareDal.GetAvailability(hardwareItem, dateRange.StartDate, dateRange.EndDate);

        var sampledSeries = ChartHelper.GetBasicAvailabilitySeries(dt, request, true);
        var result = new ChartDataResults(sampledSeries);
        var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);
        return dynamicResult;
    }

    [WebMethod]
    public HWHCharting.ChartDataResults HardwareHealthOverview(ChartDataRequest request)
    {
        var resource = ResourceManager.GetResourceByID((int)request.AllSettings["ResourceID"]);
        Context.Items[typeof(ViewInfo).Name] = resource.View;

        SolarWinds.Orion.Security.CryptoHelper cryptoHelper = new SolarWinds.Orion.Security.CryptoHelper();
        string cryptoViewId = Server.UrlEncode(cryptoHelper.Encrypt(resource.View.ViewID.ToString()));

        string objectType = resource.Properties[HardwareHealthSummary.PropertyEntityName];
        if (string.IsNullOrEmpty(objectType))
        {
            resource.Properties.Add(HardwareHealthSummary.PropertyEntityName, HardwareHealthSummary.DefaultEntityName);
            objectType = HardwareHealthSummary.DefaultEntityName;
        }
        string[] selectedObjectTypes = objectType.Split(':');
        
        DataTable data = HardwareDal.GetHardwareHealthOverviewInformation(selectedObjectTypes);

        var selectedEntityNames = EntityTypesFactory.GetAvailableDisplayNamePluralsFromType(selectedObjectTypes);
        string objectsDisplayName = string.Join(", ", selectedEntityNames.Values);

        var dataSeriesColection = new List<HWHCharting.DataSeries>();
        dataSeriesColection.Add(new HWHCharting.DataSeries()
        {
            TagName = "Default"
        });

        DataRow row = null;

        if (data != null && data.Rows.Count > 0)
        {
            row = data.Rows[0];
        }


        dataSeriesColection[0].Data.Add(new HWHCharting.DataPoint("Up",
                                                      new Dictionary<string, object>()
                                                              {   
                                                                  {"y", row != null ? row["UpStatus"] : 0},
                                                                  {"count", row != null ? row["UpStatus"] : 0},
                                                                  {"size", row != null ? row["ServersCount"] : 0},
                                                                  {"legendLabel", OrionStatusHelper.GetLocalizedStatusText((int)OrionStatus.Up)},
                                                                  {"icon", "Server_Up.png"},
                                                                  {"color", "#359630"},
                                                                  {"ViewID", cryptoViewId},
                                                                  {"objectType", objectType },
                                                                  {"objectsDisplayName", objectsDisplayName }
                                                              }));

        dataSeriesColection[0].Data.Add(new HWHCharting.DataPoint("Warning",
                                                      new Dictionary<string, object>()
                                                              {
                                                                  {"y", row != null ? row["WarningStatus"] : 0},
                                                                  {"count", row != null ? row["WarningStatus"] : 0},
                                                                  {"size", row != null ? row["ServersCount"] : 0},
                                                                  {"legendLabel", OrionStatusHelper.GetLocalizedStatusText((int)OrionStatus.Warning)},
                                                                  {"icon", "Server_Warning.png"},
                                                                  {"color", "#bebe00"},
                                                                  {"ViewID", cryptoViewId},
                                                                  {"objectType", objectType },
                                                                  {"objectsDisplayName", objectsDisplayName }
                                                              }));

        dataSeriesColection[0].Data.Add(new HWHCharting.DataPoint("Critical",
                                                      new Dictionary<string, object>()
                                                              {
                                                                  {"y", row != null ? row["CriticalStatus"] : 0},
                                                                  {"count", row != null ? row["CriticalStatus"] : 0},
                                                                  {"size", row != null ? row["ServersCount"] : 0},
                                                                  {"legendLabel", OrionStatusHelper.GetLocalizedStatusText((int)OrionStatus.Critical)},
                                                                  {"icon", "Server_Down.png"},
                                                                  {"color", "#c50000"},
                                                                  {"ViewID", cryptoViewId},
                                                                  {"objectType", objectType },
                                                                  {"objectsDisplayName", objectsDisplayName }
                                                              }));

        dataSeriesColection[0].Data.Add(new HWHCharting.DataPoint("Other",
                                                      new Dictionary<string, object>()
                                                              {
                                                                  // when row == null, set y to 1, because when no data we want to display fully grey pie chart by default
                                                                  {"y", row != null ? row["OtherStatus"] : 1},
                                                                  {"count", row != null ? row["OtherStatus"] : 0},
                                                                  {"size", row != null ? row["ServersCount"] : 0},
                                                                  {"legendLabel", Resources.HardwareHealthWebContent.HHWEBCODE_DO0_9},
                                                                  {"icon", "Server_Unknown.png"},
                                                                  {"color", "#b4b4b4"},
                                                                  {"ViewID", cryptoViewId},
                                                                  {"objectType", objectType },
                                                                  {"objectsDisplayName", objectsDisplayName }
                                                              }));

        return new HWHCharting.ChartDataResults(dataSeriesColection);
    }
}
