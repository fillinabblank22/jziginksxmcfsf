﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.HardwareHealth.Common.Models;
using SolarWinds.HardwareHealth.Common.Models.HardwareHealth;
using SolarWinds.HardwareHealth.Web;
using SolarWinds.HardwareHealth.Web.Shared.DAL;
using SolarWinds.HardwareHealth.Web.Shared.Models;
using SolarWinds.Orion.Core.Common.i18n;

public partial class Orion_APM_NodePopupExtension : System.Web.UI.UserControl
{
    protected int NormalPercentage { get; set; }
    protected int WarningPercentage { get; set; }
    protected int ErrorPercentage { get; set; }
    protected int UnknownPercentage { get; set; }

    private HardwareInfoModel hardwareInfo;
    protected HardwareInfoModel HardwareInfo
    {
        get
        {
            if(hardwareInfo == null)
            {
                hardwareInfo = HardwareDal.GetHardwareInfoFromNetObjectID(Request["NetObject"]);
            }
            return hardwareInfo;
        }
    }

    private HardwareHealthDal hardwareDal;
    protected HardwareHealthDal HardwareDal
    {
        get
        {
            if (hardwareDal == null)
            {
                hardwareDal = new HardwareHealthDal();
            }
            return hardwareDal;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected string GetHardwareStatusIcon()
    {
        string root = "/Orion/HardwareHealth/Images/";
        string iconName = string.Empty;

        if(HardwareInfo == null)
        {
            return root + "#.png";
        }

        switch (HardwareInfo.Status)
        {
            case OrionStatus.Up:
                {
                    iconName = "Server_Small_Up.png";
                    break;
                }
            case OrionStatus.Warning:
                {
                    iconName = "Server_Small_Warning.png";
                    break;
                }
            case OrionStatus.Critical:
            case OrionStatus.Down:
                {
                    iconName = "Server_Small_Down.png";
                    break;
                }
            default:
                {
                    iconName = "Server_Small_Unknown.png";
                    break;
                }
        }

        return string.Format(CultureInfo.InvariantCulture, "{0}{1}", root, iconName);
    }

    protected string GetHardwareStatusText()
    {
        using (LocaleThreadState.EnsurePrimaryLocale())
        {
            string statusMessage;

            if (HardwareInfo == null)
            {
                return string.Empty;
            }

            switch (HardwareInfo.Status)
            {
                case OrionStatus.Up:
                    {
                        statusMessage = Resources.HardwareHealthWebContent.HHWEBCODE_DO0_10;
                        break;
                    }
                case OrionStatus.Warning:
                    {
                        statusMessage = string.Format(CultureInfo.InvariantCulture, "<span style=\"color: #FEC405\">{0}</span>", Resources.HardwareHealthWebContent.HHWEBCODE_DO0_11);
                        break;
                    }
                case OrionStatus.Critical:
                case OrionStatus.Down:
                    {
                        statusMessage = string.Format(CultureInfo.InvariantCulture, "<span style=\"color: red\"><b>{0}</b></span>", Resources.HardwareHealthWebContent.HHWEBCODE_DO0_12);
                        break;
                    }
                default:
                    {
                        statusMessage = string.Format(CultureInfo.InvariantCulture, "<span style=\"color: gray\">{0}</span>", Resources.HardwareHealthWebContent.HHWEBCODE_DO0_9);
                        break;
                    }
            }

            return statusMessage;
        }
    }
}



