﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.HardwareHealth.Web.Shared.DAL;
using SolarWinds.Orion.Web.UI;
using SolarWinds.HardwareHealth.Web;
using SolarWinds.HardwareHealth.Common.Utility;

public partial class Orion_HardwareHealth_HardwareDetails : Page //Not inheriting from OrionView for now as it causes side effects
{
    private HardwareHealthDal hardwareDal;
    protected HardwareHealthDal HardwareDal
    {
        get
        {
            if (hardwareDal == null)
            {
                hardwareDal = new HardwareHealthDal();
            }
            return hardwareDal;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //This page does not do anything at this point just redirects to node details

        string netObjectId = this.Request["NetObject"];
        if ( !String.IsNullOrEmpty(netObjectId))
        {
            int index = netObjectId.IndexOf(':');
            string prefix = netObjectId.Substring(0, index);

            NetObjectBase netObject = null;
            switch ( prefix )
            {
                case "HWH":
                    netObject = new Hardware(netObjectId);
                    break;
                case "HWHT":
                    netObject = new HardwareType(netObjectId);
                    break;
                case "HWHS":
                    netObject = new HardwareSensor(netObjectId);
                    break;
            }

            if (netObject != null)
            {
                string redirectUrl = InvariantString.Format("/Orion/View.aspx?NetObject={0}", netObject.ParentObject.ToString());

                int? viewId = HardwareDal.GetViewIDByViewKey("Network SubView");
                Response.Redirect(viewId.HasValue ? string.Format(CultureInfo.InvariantCulture, "{0}&ViewID={1}", redirectUrl, viewId.Value) : redirectUrl);
            }
        }
    }
}