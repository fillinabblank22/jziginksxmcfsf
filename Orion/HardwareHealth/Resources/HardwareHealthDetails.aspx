﻿<%@ Page Language="C#" MasterPageFile="~/Orion/HardwareHealth/HardwareHealthView.master" AutoEventWireup="true" CodeFile="HardwareHealthDetails.aspx.cs" Inherits="Orion_HardwareHealth_Resources_Hardware_HardwareHealthDetails" %>

<asp:Content ID="bodyContent" ContentPlaceHolderID="HardwareHealthMainContentPlaceHolder" runat="server">
	<h1><%= this.ViewHardwareStatusLocalized %></h1>
	<div class="ResourceWrapper" style="width:500px;margin-left:15px;">
		<asp:Repeater ID="nodeViewGrid" OnInit="OnHardwareHealthViewGrid_Init" runat="server">
			<HeaderTemplate>
				<table class="DataGrid" style="width:500px;">
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td>
					    <img ID="serverIcon" src="<%# GetHardwareStatusIcon(this.HardwareStatus) %>" runat="server"/>
						<%--<apm:StatusIcon ID="appIcon" StatusValue='<%# this.AppStatus %>' runat="server" />--%>
						<asp:Label ID="nodeName" Text="<%# this.GetRowItem(Container.DataItem) %>" runat="server" />
					</td>
				</tr>
			</ItemTemplate>
			<FooterTemplate>
				</table>
			</FooterTemplate>
		</asp:Repeater>
	</div>
</asp:Content>