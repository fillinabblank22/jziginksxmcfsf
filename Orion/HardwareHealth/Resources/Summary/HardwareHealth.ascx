﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HardwareHealth.ascx.cs" Inherits="Orion_HardwareHealth_Resources_Summary_HardwareHealth" %>

<orion:resourceWrapper runat="server" ID="Wrapper" ShowEditButton="true" CssClass="hwh_HardwareHealthOverview">
    <Content>
       <asp:PlaceHolder runat="server" ID="WrapperContents"></asp:PlaceHolder>
    </Content>
</orion:resourceWrapper> 