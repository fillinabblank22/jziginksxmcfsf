﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.UI;
using SolarWinds.HardwareHealth.Web.UI.Resource;
using SolarWinds.HardwareHealth.Web.Shared.DAL;
using SolarWinds.Orion.Core.Common.i18n;

public partial class Orion_HardwareHealth_Resources_Summary_HardwareHealth : StandardChartResource
{
    private HardwareHealthDal hardwareHealthDal;

    private HardwareHealthDal HardwareHealthDal
    {
        get
        {
            if (hardwareHealthDal == null)
            {
                hardwareHealthDal = new HardwareHealthDal();
            }
            return hardwareHealthDal;
        }
    }

    public override string EditURL
    {
        get
        {
            return ExtendCustomEditUrl("/Orion/NetPerfMon/Resources/EditResource.aspx");
        }
    }

    protected override void OnInit(EventArgs e)
    {

        if (!"SummaryView".Equals(Resource.View.ViewType, StringComparison.InvariantCultureIgnoreCase) 
            && SelectedParentObjectTypes.Length == 1 
            && SelectedParentObjectTypes[0].Equals(HardwareHealthSummary.DefaultEntityName) 
            && "NodeDetails".Equals(Resource.View.ViewType, StringComparison.InvariantCultureIgnoreCase) 
            && GetHardwareObjectCount() == 0)
        {
            Visible = false;
        }

        base.OnInit(e);

        ChartWidth = Math.Max((int)(Resource.Width * 0.45), 270);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Resource.Properties.ContainsKey(HardwareHealthSummary.PropertyChartName))
        {
            Resource.Properties.Add(HardwareHealthSummary.PropertyChartName, HardwareHealthSummary.DefaultChartName);
        }

        HandleInit(WrapperContents);

        OrionInclude.CoreFile("/HardwareHealth/Styles/HardwareHealth.css");
        OrionInclude.CoreFile("/HardwareHealth/js/Charts/Charts.HardwareHealth.HardwareOverviewChartLegend.js", OrionInclude.Section.Middle);
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        return new string[0];
    }

    protected override string NetObjectPrefix
    {
        get { return "HHOW"; }
    }

    protected override string DefaultTitle
    {
        get
        {
            using (LocaleThreadState.EnsurePrimaryLocale())
            {
                return Resources.HardwareHealthWebContent.HHWEBCODE_DO0_24;
            }
        }
    }

    protected override void SetAdditionalStyles(HtmlGenericControl ChartPlaceHolder)
    {
        var chartWrapper = ChartPlaceHolder.Parent as HtmlGenericControl;
        chartWrapper = chartWrapper ?? ChartPlaceHolder;

        chartWrapper.Style["display"] = "inline-block";
        chartWrapper.Style["height"] = ChartResourceSettings.Height + "px";
        chartWrapper.Style["margin"] = "0 10px";
    }

    protected Int32 GetHardwareObjectCount()
    {
        return IsValidData ? (int)Data.Rows[0][0] : 0;
    }

    protected string[] SelectedParentObjectTypes
    {
        get
        {
            string hardwareHealthParentObjectType = Resource.Properties[HardwareHealthSummary.PropertyEntityName];
            return (!string.IsNullOrEmpty( hardwareHealthParentObjectType) ? hardwareHealthParentObjectType.Split(':') : new string[] { HardwareHealthSummary.DefaultEntityName });
        }
    }
    private DataTable data;
    private DataTable Data
    {
        get
        {
            if (data == null)
            {
                data = HardwareHealthDal.GetHardwareInfoCount(SelectedParentObjectTypes);
            }
            return data;
        }
    }

    protected Boolean IsValidData
    {
        get
        {
            if (Data != null && Data.Rows.Count > 0)
            {
                var row = Data.Rows[0];

                if ((int)row[0] > 0)
                {
                    return true;
                }
            }
            return false;
        }
    }

    protected override Dictionary<string, object> GenerateDisplayDetails()
    {
        var details = base.GenerateDisplayDetails();

        details["ResourceID"] = Resource.ID;

        return details;
    }

    protected override void AddChartExportControl(System.Web.UI.Control resourceWrapperContents)
    {
        // There is no need for export button
    }

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }

    public override string EditControlLocation
    {
        get { return "/Orion/HardwareHealth/Controls/EditResourceControls/EditHardwareHealthSummary.ascx"; }
    }
}