﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using Resources;
using SolarWinds.HardwareHealth.Common.Models;
using SolarWinds.HardwareHealth.Web.Controls.HardwareHealthTree;
using SolarWinds.HardwareHealth.Web.Shared.Controls.HardwareHealthTree;
using SolarWinds.HardwareHealth.Web.Shared.Models;
using SolarWinds.HardwareHealth.Web.UI.Resource;
using SolarWinds.HardwareHealth.Web.Utility;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.i18n;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.HardwareHealth.Web.Shared.DAL;
using SolarWinds.HardwareHealth.Web.Shared.DAL.Queries;

public partial class Orion_HardwareHealth_Resources_Hardware_HardwareHealthForGroup : HardwareHealthBaseResource
{
    private static readonly Log log = new Log();

    private const int DefaultItemLimit = 10;
    private const int DefaultTopItemLimit = 10;

    private IHardwareTreeProvider dataProvider;
    private IHardwareTreeProviderFactory hardwareTreeProviderFactory;
    private IEnumerable<string> orderableColumns = new List<string>();
    private string providerTemplatePath;
    private string dataProviderNameOverride;

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IContainerProvider) }; }
    }
    
    public override IEnumerable<string> RequiredDynamicInfoKeys
    {
        get { return new string[0]; }
    }

    public int ResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    public bool IsInteractive
    {
        get { return Resource.ID > 0; }
    }

    protected IHardwareTreeProviderFactory HardwareTreeProviderFactory
    {
        get
        {
            if (hardwareTreeProviderFactory == null)
            {
                var defaultProviders = new HardwareTreeProviderCollection();
                hardwareTreeProviderFactory = new HardwareTreeProviderFactory();
                hardwareTreeProviderFactory.RegisterProviders(defaultProviders.Providers);
            }

            return hardwareTreeProviderFactory;
        }
    }

    private IHardwareHealthDal hardwareHealthDal;
    private IHardwareHealthDal HardwareHealthDal
    {
        get
        {
            if (hardwareHealthDal == null)
            {
                hardwareHealthDal = new HardwareHealthDal(new HardwareHealthDalGroupQueryProvider(), new HardwareHealthDalCache());
            }

            return hardwareHealthDal;
        }
    }

    protected override string DefaultTitle
    {
        get
        {
            using (LocaleThreadState.EnsurePrimaryLocale())
            {
                return HardwareHealthWebContent.HHWEBCODE_DO0_25;
            }
        }
    }

    public override string HelpLinkFragment
    {
        get { return "APMAGHardwareHealth"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/HardwareHealth/Controls/EditResourceControls/EditCurrentHardwareHealthForGroup.ascx"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public string ErrorMessage { get; private set; }

    public bool RememberState
    {
        get { return IsInteractive; }
    }

    public bool ExpandAllByDefault
    {
        get
        {
            var property = Resource.Properties["HardwareTreeExpandByDefault"];
            if (string.IsNullOrEmpty(property))
            {
                return !IsInteractive;
            }
            
            return Boolean.Parse(property);
        }
    }

    public HardwareTreeExpansionState HardwareTreeExpansionState { get; private set; }

    public int ItemLimit
    {
        get { return Resource.Properties.ContainsKey("ItemLimit") ? Convert.ToInt32(Resource.Properties["ItemLimit"]) : DefaultItemLimit; }
    }

    public int TopItemLimit
    {
        get { return Resource.Properties.ContainsKey("TopItemLimit") ? Convert.ToInt32(Resource.Properties["TopItemLimit"]) : DefaultTopItemLimit; }
    }

    public string DataProviderName
    {   // keep empty for automatic detection
        get { return dataProviderNameOverride ?? Resource.Properties["HardwareTreeProvider"]; }
    }

    private string DefaultTemplatesFolder
    {
        get { return Path.Combine(MapPathSecure(Path.GetDirectoryName(AppRelativeVirtualPath)), "TreeTemplates"); }
    }

    public IEnumerable<string> OrderableColumns
    {
        get { return orderableColumns; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(ParentObject == null)
        {
            using (LocaleThreadState.EnsurePrimaryLocale())
            {
                ErrorMessage = HardwareHealthWebContent.HHWEBCODE_JP0_4;
            }
            log.Warn(@"Failed to acquire valid NodeID");
            return;
        }
        
        if (ParentObject.Type != ParentObjectType.Group
            || !HardwareHealthDal.HasAnyHardwareInfo(ParentObject))
        {
            Visible = false;
            return;
        }

        var context = new HardwareTreeContext(ParentObject, Resource.ID, Resource.View.ViewID, DataProviderName);
        var provider = SetupTreeNodeProvider(ParentObject);
        if (provider == null)
        {
            using (LocaleThreadState.EnsurePrimaryLocale())
            {
                ErrorMessage = HardwareHealthWebContent.HHWEBCODE_JP0_5;
            }
            log.Error(@"No suitable tree data provider found");
            return;
        }

        AddResourceButtons(ParentObject);
        Visible = provider.GetTopChildrenCount(ParentObject) > 0;
        if (Visible)
        {
            SetupExpansionState(context);
        }
    }

    protected void AddResourceButtons(ParentObject parentObject)
    {
        using (LocaleThreadState.EnsurePrimaryLocale())
        {
            Wrapper.ManageButtonText = HardwareHealthWebContent.HHWEBDATA_ZB0_1;
        }
        Wrapper.ManageButtonTarget = string.Format("~/Orion/HardwareHealth/SensorManagement.aspx?NetObject={0}", parentObject);
        Wrapper.ShowManageButton = Profile.AllowNodeManagement || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
    }

    protected void SetupExpansionState(HardwareTreeContext context)
    {
        if (!RememberState)
            return;

        try
        {
            HardwareTreeExpansionState = new HardwareTreeExpansionManager().LoadState(context);
        }
        catch (Exception ex)
        {
            log.Warn(ex);
        }
    }

    protected IHardwareTreeProvider SetupTreeNodeProvider(ParentObject parentObject)
    {
        dataProvider = HardwareTreeProviderFactory.GetHardwareHealthTreeProvider(new HardwareTreeProviderRequest(parentObject, DataProviderName));
        if (dataProvider == null)
        {   // provider possibly not applicable - fall back to next best
            dataProvider = HardwareTreeProviderFactory.GetValidHardwareHealthTreeProviders(new HardwareTreeProviderRequest(parentObject)).FirstOrDefault();
            if (dataProvider == null)
            {
                return null;
            }
            
            dataProviderNameOverride = dataProvider.GetUniqueName();
        }

        providerTemplatePath = dataProvider.GetDefaultTemplatePath(DefaultTemplatesFolder);
        orderableColumns = dataProvider.GetOrderableColumns();

        return dataProvider;
    }

    protected override void Render(HtmlTextWriter writer)
    {
        templatesContainer.InnerHtml = string.IsNullOrEmpty(providerTemplatePath) ? string.Empty : TokenSubstitution.Parse(File.ReadAllText(providerTemplatePath));

        tabTemplate.Text = TokenSubstitution.Parse(tabTemplate.Text);
        tabTemplate2.Text = TokenSubstitution.Parse(tabTemplate2.Text);
        base.Render(writer);
    }
}