﻿using System;
using System.Collections.Generic;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.i18n;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.UI;

public partial class Orion_HardwareHealth_Resources_Hardware_SensorAvailabilityChart : StandardChartResource, IResourceIsInternal
{
    private static readonly Log _log = new Log();

    protected void Page_Init(object sender, EventArgs e)
    {
        OrionInclude.CoreFile("/HardwareHealth/Styles/HardwareHealth.css");
        OrionInclude.CoreFile("/HardwareHealth/js/Charts/Charts.HardwareHealth.Common.js", OrionInclude.Section.Middle);

        HandleInit(WrapperContents);
    }

    /// <summary>
    /// Gets a value indicating whether [allow customization].
    /// </summary>
    /// <value>
    ///   <c>true</c> if [allow customization]; otherwise, <c>false</c>.
    /// </value>
    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    /// <summary>
    /// Gets the net object prefix.
    /// </summary>
    protected override string NetObjectPrefix
    {
        get { return "HHSA"; }
    }

    /// <summary>
    /// Gets the list of uniqueidentifier elements for which we will need to retrieve data points for charts
    /// </summary>
    /// <returns>List of uniqueidentifiers of elements for which we via webservice will want to retrieved data.</returns>
    protected override IEnumerable<string> GetElementIdsForChart()
    {
        var netObjectProvider = GetInterfaceInstance<INetObjectProvider>();
        if (netObjectProvider != null)
        {
            if (Resource.Properties.ContainsKey("ElementList"))
            {
                return Resource.Properties["ElementList"].Split(',');
            }
        }

        return new string[0];
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }

    /// <summary>
    /// Gets the default title for resource which will be displayed in the case that template will not override this.
    /// </summary>
    protected override string DefaultTitle
    {
        get
        {
            using (LocaleThreadState.EnsurePrimaryLocale())
            {
                return Resources.HardwareHealthWebContent.HHWEBCODE_DO0_26;
            }
        }
    }

    public bool IsInternal
    {
        get { return true; }
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }
}