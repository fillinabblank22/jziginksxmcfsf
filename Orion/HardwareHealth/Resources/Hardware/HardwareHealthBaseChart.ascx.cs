﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.HardwareHealth.Common.Models;
using SolarWinds.HardwareHealth.Web.Shared.Charting;
using SolarWinds.HardwareHealth.Web.DAL;
using SolarWinds.Logging;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.UI;
using SolarWinds.HardwareHealth.Web.UI.Resource;
using SolarWinds.Orion.Core.Common.i18n;

public partial class Orion_HardwareHealth_Resources_Hardware_HardwareHealthBaseChart : StandardChartResource, IResourceIsInternal
{
    private static readonly Log _log = new Log();
    private ParentObject parentObject;
    public ParentObject ParentObject
    {
        get
        {
            if (parentObject == null)
            {
                var netObjectManager = new NetObjectManager(this);
                var netObject = netObjectManager.TryGetNetObject();
                if (netObject != null)
                {
                    parentObject = ParentObject.Parse(netObject.NetObjectID);
                    if (parentObject == null && netObject.Parent != null)
                    {
                        parentObject = ParentObject.Parse(netObject.Parent.NetObjectID);
                    }
                }
            }

            return parentObject;
        }
    }

    private ISensorSwisDal sensorSwisDal;
    internal ISensorSwisDal SensorSwisDal
    {
        get
        {
               return sensorSwisDal ?? (sensorSwisDal = new SensorsSwisDal());
        }
        set { sensorSwisDal = value; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);
    }

    /// <summary>
    /// Gets a value indicating whether [allow customization].
    /// </summary>
    /// <value>
    ///   <c>true</c> if [allow customization]; otherwise, <c>false</c>.
    /// </value>
    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    public override IEnumerable<string> RequiredDynamicInfoKeys
    {
        get { return new[] { HardwareHealthResourceCommon.RequiredDynamicInfoKey }; }
    }
    /// <summary>
    /// Gets the net object prefix.
    /// </summary>
    protected override string NetObjectPrefix
    {
        get { return "HH"; }
    }

    /// <summary>
    /// Gets the list of uniqueidentifier elements for which we will need to retrieve data points for charts
    /// </summary>
    /// <returns>List of uniqueidentifiers of elements for which we via webservice will want to retrieved data.</returns>
    protected override IEnumerable<string> GetElementIdsForChart()
    {
        if (ParentObject != null)
        {
            if (Resource.Properties.ContainsKey("ElementList"))
            {
                return Resource.Properties["ElementList"].Split(',');
            }
            else
            {
                return SensorSwisDal.GetSensorIDsByUnit(ParentObject, (int)ChartDataHelper.GetHardwareUnit(Resource.Properties["ChartName"])).Select(x => x.ToString());
            }
        }

        return new string[0];
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(INodeProvider) }; }
    }

    /// <summary>
    /// Gets the default title for resource which will be displayed in the case that template will not override this.
    /// </summary>
    protected override string DefaultTitle
    {
        get
        {
            using (LocaleThreadState.EnsurePrimaryLocale())
            {
                return Resources.HardwareHealthWebContent.HHWEBCODE_DO0_25;
            }
        }
    }

    public bool IsInternal
    {
        get { return true; }
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }
}