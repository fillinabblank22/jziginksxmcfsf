﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HardwareHealthChartsWithTabs.ascx.cs" Inherits="Orion_HardwareHealth_Resources_Hardware_HardwareHealthChartsWithTabs" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div class="sw-tabs"><div class="x-tab-panel-header x-unselectable x-tab-panel-header-plain" style="width: 100%;">

        <div class="x-tab-strip-wrap">
          <ul class="x-tab-strip x-tab-strip-top">
            <asp:Literal ID="TabPH" EnableViewState="False" runat="server" />
            <li class="x-tab-edge"></li>
            <div class="x-clear"><!-- --></div>
          </ul>
          <div class="x-tab-strip-spacer" style="border-bottom: none;"><!-- --></div>
        </div>
        </div>
        </div>

        <asp:PlaceHolder ID="chartContents" runat="server" />
        
        <script type="text/javascript">
            SW.Core.Charts.Legend.core_standardLegendInitializer__<%= legend.ClientID %> = function (chart, dataUrlParameters) {
                $('#<%= legend.ClientID %>').empty();
                SW.Core.Charts.Legend.createStandardLegend(chart, dataUrlParameters, '<%= legend.ClientID %>');
            };
        
        
        </script>

        <table runat="server" id="legend" class="chartLegend"></table>
        <div style="float:right;">
            <img class="chartLegendLogo" src="/orion/images/SolarWinds.Logo.Footer.png"/>
        </div>

        <script type="text/javascript">
            <asp:Literal ID="ScriptContainer" runat="server"/>
        </script>
    </Content>
</orion:resourceWrapper>
