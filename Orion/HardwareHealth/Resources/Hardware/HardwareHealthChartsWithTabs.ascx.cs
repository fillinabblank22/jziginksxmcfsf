﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using SolarWinds.Common.Snmp;
using SolarWinds.HardwareHealth.Common.Models.HardwareHealth;
//using SolarWinds.HardwareHealth.Web.ChartingCoreBased;
using SolarWinds.HardwareHealth.Web.DAL;
using SolarWinds.HardwareHealth.Web.Shared.Charting;
using SolarWinds.HardwareHealth.Web.Shared.DAL;
using SolarWinds.HardwareHealth.Web.Shared.Models;
using SolarWinds.HardwareHealth.Web.UI.Resource;
using SolarWinds.HardwareHealth.Web.Utility;
using SolarWinds.Orion.Core.Common.i18n;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.UI;

public partial class Orion_HardwareHealth_Resources_Hardware_HardwareHealthChartsWithTabs : HardwareHealthBaseResource, IResourceIsInternal
{
    private Dictionary<string, string> _added = new Dictionary<string, string>();
    private bool _alreadyactive = false;
    private string _pageTitleOverride = null;

    protected string NetObjectPrefix = "HH";

    #region Properties

    private HtmlGenericControl ChartPlaceHolder;

    private ISensorSwisDal sensorSwisDal;
    internal ISensorSwisDal SensorSwisDal
    {
        get
        {
            return sensorSwisDal ?? (sensorSwisDal = new SensorsSwisDal());
        }
        set { sensorSwisDal = value; }
    }
    protected string DisplayDetails { get; set; }
    public ChartResourceSettings ChartResourceSettings { get; private set; }
    protected IChartLegendControl ChartLegendControl { get; private set; }

    protected string ChartOptions
    {
        get { return ChartResourceSettings.IsValid ? ChartResourceSettings.ChartOptionsJson : "{}"; }
    }

    /// <summary>
    /// Gets or sets the title of the chart. If this property will not be null, we will override standard behavior.
    /// </summary>
    /// <value>
    /// The chart title.
    /// </value>
    public string ChartTitle { get; set; }

    /// <summary>
    /// Gets or sets the chart sub title. If this property will not be null, we will override standard behavior.
    /// </summary>
    /// <value>
    /// The chart sub title.
    /// </value>
    public string ChartSubTitle { get; set; }

    /// <summary>
    /// Gets or sets the time span in days. If this property will be set, we will override standard behavior.
    /// </summary>
    /// <value>
    /// The time span in days.
    /// </value>
    public double? TimeSpanInDays { get; set; }

    /// <summary>
    /// Gets or sets the date from.
    /// </summary>
    /// <value>
    /// The date from.
    /// </value>
    public DateTime? DateFrom { get; set; }

    /// <summary>
    /// Gets or sets the date to.
    /// </summary>
    /// <value>
    /// The date to.
    /// </value>
    public DateTime? DateTo { get; set; }

    /// <summary>
    /// Gets or sets the size of the sample.
    /// </summary>
    /// <value>
    /// The size of the sample.
    /// </value>
    public int? SampleSize { get; set; }

    /// <summary>
    /// Gets or sets the width of the resource. In the case, that width will not be set, it will be used width from ChartResourceSettings.
    /// </summary>
    /// <value>
    /// The width.
    /// </value>
    public int? Width { get; set; }

    /// <summary>
    /// Gets or sets the height of the resource. In the case, that height will not be set, it will be used height from ChartResourceSettings.
    /// </summary>
    /// <value>
    /// The height.
    /// </value>
    public int? Height { get; set; }

    /// <summary>
    /// Gets or sets the initial zoom of chart. If this property will be set, we will override standard behavior.
    /// </summary>
    /// <value>
    /// The initial zoom.
    /// </value>
    public string InitialZoom { get; set; }

    /// <summary>
    /// Gets or sets whether to calculate trend line. If this property will be set, we will override standard behavior.
    /// </summary>
    /// <value>
    /// The calculate trend line.
    /// </value>
    public bool? CalculateTrendLine { get; set; }

    /// <summary>
    /// Gets or sets whether to calculate sum line. If this property will be set, we will override standard behavior.
    /// </summary>
    /// <value>
    /// The calculate sum line.
    /// </value>
    public bool? CalculateSumLine { get; set; }

    /// <summary>
    /// Gets or sets the calculate95th percentile line. If this property will be set, we will override standard behavior.
    /// </summary>
    /// <value>
    /// The calculate95th percentile line.
    /// </value>
    public bool? Calculate95thPercentileLine { get; set; }

    public string PageTitleOverride
    {
        get { return _pageTitleOverride; }
        set { _pageTitleOverride = value; }
    }

    #endregion

    protected override string DefaultTitle
    {
        get
        {
            using (LocaleThreadState.EnsurePrimaryLocale())
            {
                return Resources.HardwareHealthWebContent.HHWEBCODE_DO0_8;
            }
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceHardwareHealthChart"; }
    }


    private HardwareHealthDal hardwareHealthDal;

    private HardwareHealthDal HardwareHealthDal
    {
        get
        {
            if (hardwareHealthDal == null)
            {
                hardwareHealthDal = new HardwareHealthDal();
            }
            return hardwareHealthDal;
        }
    }

    public bool IsInternal
    {
        get { return true; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        HardwareInfoModel hardwareInfo;
        this.Visible = HardwareHealthHelper.ShouldBeResourceVisible(this, out hardwareInfo);

        OrionInclude.CoreFile("/HardwareHealth/Styles/HardwareHealth.css");
        OrionInclude.CoreFile("/js/extjs/3.4/resources/css/ext-all-notheme.css");
        OrionInclude.CoreFile("/js/extjs/3.4/resources/css/xtheme-gray.css");
        OrionInclude.CoreFile("/Charts/Charts.css");
        OrionInclude.CoreFile("/Charts/js/allcharts.js");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ChartResourceSettings = ChartResourceSettings.FromResource(Resource, new ChartSettingsDAL());

        PrepareChartPlaceHolder();

        DataTable table = null;
        if (ParentObject != null)
        {
            //ChartTab.NodeId = this.NPMNode.NodeID;
            //ChartTab.Resource = this.Resource;
            table = HardwareHealthDal.GetHardwareCategoriesForTabedChartByParentObject(ParentObject);
        }

        // Check if there is any category to display
        if (table == null || table.Rows.Count == 0)
        {
            this.Visible = false;
            return;
        }

        //ScriptContainer.Text = ScriptContainer.Text + Environment.NewLine + GetChartLegendScript() + Environment.NewLine;
        bool hasTabs = false;

        foreach (DataRow row in table.Rows)
        {
            int categoryId = (int) row["CategoryID"];
            string categoryName = (string) row["CategoryName"];
            string unitName = (string) row["UnitName"];
            int unitId = (int) row["UnitID"];

            if (!HardwareHealthDal.HasCategoryPolledData(ParentObject, categoryId, unitId))
            {
                continue;
            }

            string displayName =
                table.Select(string.Format(CultureInfo.InvariantCulture, "CategoryID = {0}", categoryId)).Length > 1
                    ? string.Format(CultureInfo.InvariantCulture, "{0} ({1})", categoryName, unitName)
                    : categoryName;

            AddTab(categoryId, displayName, unitId, true);
            hasTabs = true;
        }

        if(!hasTabs)
        {
            this.Visible = false;
        }
    }

    public override string EditUrlLocation
    {
        get
        {
            string url = string.Format("/Orion/Charts/EditChartResource.aspx?ResourceID={0}", this.Resource.ID);

            if (!string.IsNullOrEmpty(Request["NetObject"]))
                url = string.Format("{0}&NetObject={1}", url, Request["NetObject"]);
            if (Page is OrionView)
                url = string.Format("{0}&ViewID={1}", url, ((OrionView)Page).ViewInfo.ViewID);
            if (!String.IsNullOrEmpty(Request.QueryString["ThinAP"]))
                url = String.Format("{0}&ThinAP={1}", url, Request.QueryString["ThinAP"]);

            return (url);
        }
    }

    private void PrepareChartPlaceHolder()
    {
        ChartPlaceHolder = new HtmlGenericControl("div");
        ChartPlaceHolder.Attributes.Add("style", "page-break-inside: avoid; width: auto;");

        chartContents.Controls.Add(ChartPlaceHolder);

        ChartPlaceHolder.Style[HtmlTextWriterStyle.Height] = ChartResourceSettings.Height + "px";
    }

    public bool AddTab(int categoryId, string displayName, int unitId, bool Active, bool Enabled)
    {
        if (_added.ContainsKey(displayName))
            return false;

        this.Resource.Properties["ChartName"] = GetChartName((HardwareHealthCategory) categoryId, unitId);

        var details = GenerateDisplayDetails();
        DisplayDetails = new JavaScriptSerializer().Serialize(details);

        ChartResourceSettings = ChartResourceSettings.FromResource(Resource, new ChartSettingsDAL());

        string tabId = string.Format(CultureInfo.InvariantCulture, "apm_tabID-{0}-{1}-{2}-{3}", ParentObject.ID, categoryId, unitId, this.ClientID);
        string javaScriptFunctionName = string.Format(CultureInfo.InvariantCulture, "createChartForTab{0}{1}", categoryId, unitId);
        string javaScriptFunction = GetChartInitializerScript(javaScriptFunctionName, tabId);

        ScriptContainer.Text = ScriptContainer.Text + Environment.NewLine + javaScriptFunction + Environment.NewLine;

        if (_alreadyactive)
        {
            Active = false;
        }
        else
        {
            ScriptContainer.Text = ScriptContainer.Text + Environment.NewLine + "$(" + javaScriptFunctionName + "() );" + Environment.NewLine;
        }

        _added.Add(displayName, javaScriptFunction);

        const string innerTabFormat = "<em class=\"x-tab-left\"><span class=\"x-tab-strip-inner\"><span class=\"x-tab-strip-text\">{0}</span></span></em>";
        const string nonClickingFormat = "<li class=\"apm_hhTab_{4} sw-tab {0}\" id=\"{1}\"><a href=\"#\" onclick=\"return false;\" class=\"x-tab-right\">{3}</a></li>";
        const string clickingFormat = "<li class=\"apm_hhTab_{4} sw-tab {0}\"  id=\"{1}\"><a href=\"#\" onclick=\"{2}(); return false;\" class=\"x-tab-right\">{3}</a></li>";

        string fmt = nonClickingFormat;
        string p1;
        string p2;

        if (!Enabled)
        {
            p1 = "x-item-disabled";
            p2 = "#";
        }
        else if (Active)
        {
            _alreadyactive = true;
            p1 = "x-tab-strip-active";
            p2 = javaScriptFunctionName;
            fmt = clickingFormat;
        }
        else
        {
            p1 = "";
            p2 = javaScriptFunctionName;
            fmt = clickingFormat;
        }

        string inner = string.Format(innerTabFormat, HttpUtility.HtmlEncode(displayName));
        string newTab = string.Format(fmt, p1, tabId, p2, inner, Resource.ID);
        TabPH.Text += newTab;

        return Active;
    }

    public bool AddTab(int categoryId, string displayName, int unitId, bool Active)
    {
        return AddTab(categoryId, displayName, unitId, Active, true);
    }

    protected string GetChartInitializerScript(string functionName, string tabId)
    {

        var script = String.Format(@"var {0} = function() {{ 
if($('#{3}').hasClass('hasChart'))
{{
    var chart = $('#{3}').data();
    if(chart)
    {{
        chart.subtitle = null;
        chart.destroy();
    }}
}}

$('.apm_hhTab_{4}.x-tab-strip-active').removeClass('x-tab-strip-active');
$('#{5}').addClass('x-tab-strip-active');

SW.Core.Charts.initializeStandardChart({1}, {2}); 

}};

", functionName, DisplayDetails, ChartOptions, ChartPlaceHolder.ClientID, Resource.ID, tabId);

        return script;
    }

    protected string GetChartLegendScript()
    {
        return String.Format(CultureInfo.InvariantCulture, @"var core_standardLegendInitializer__{0} = function (chart, dataUrlParameters) {{
        $('#{0}').empty();
        SW.Core.Charts.Legend.createStandardLegend(chart, dataUrlParameters, '{0}');
    }};", legend.ClientID);
    }

    protected virtual Dictionary<string, object> GenerateDisplayDetails()
    {
        var title = ChartResourceSettings.Title;
        var subtitle = ChartResourceSettings.SubTitle;

        if (ChartTitle != null)
        {
            title = ChartTitle;
        }

        if (ChartSubTitle != null)
        {
            subtitle = ChartSubTitle;
        }

        var netObjectProvider = GetInterfaceInstance<INetObjectProvider>();

        if (netObjectProvider != null)
        {
            var netObject = netObjectProvider.NetObject;

            netObject = NetObjectFactory.FindNetObjectByNetObjectPrefix(netObject, this.NetObjectPrefix);

            if (netObject != null)
            {
                if (String.IsNullOrEmpty(title))
                    title = netObject.Name;

                // Parse macros in the title/subtitle (if they are there)
                if (title.Contains("${") || subtitle.Contains("${"))
                {
                    var context = GetContextForMacros(netObject);

                    title = Macros.ParseDataMacros(title, context, true, false, true);
                    subtitle = Macros.ParseDataMacros(subtitle, context, true, false, true);
                }
            }
        }

        var details = new Dictionary<string, object>();

        details["dataUrl"] = ChartResourceSettings.DataUrl;
        details["title"] = title;
        details["subtitle"] = subtitle;
        details["showTitle"] = ChartResourceSettings.ShowTitle || (ChartTitle != null) || (ChartSubTitle != null);
        details["netObjectIds"] = GetElementIdsForChart().ToArray();
        details["sampleSizeInMinutes"] = SampleSize.HasValue ? SampleSize.Value : ChartResourceSettings.SampleSize;
        details["timespanInDays"] = TimeSpanInDays.HasValue ? TimeSpanInDays.Value : ChartResourceSettings.ChartDateSpan.TotalDays;
        details["renderTo"] = ChartPlaceHolder.ClientID;
        details["CalculateTrendLine"] = (!CalculateTrendLine.HasValue) ? ChartResourceSettings.CalculateTrendLine : CalculateTrendLine.Value;
        details["CalculateSum"] = (!CalculateSumLine.HasValue) ? ChartResourceSettings.CalculateSum : CalculateSumLine.Value;
        details["Calculate95thPercentile"] = (!Calculate95thPercentileLine.HasValue) ? ChartResourceSettings.Calculate95thPercentile : Calculate95thPercentileLine.Value;
        details["initialZoom"] = string.IsNullOrEmpty(InitialZoom) ? ChartResourceSettings.ChartInitialZoom : InitialZoom;
        details["ResourceProperties"] = GetResourcePropertiesAsDictionary();
        details["titleWidth"] = Width ?? ChartResourceSettings.Width;
        details["subTitleWidth"] = Width ?? ChartResourceSettings.Width;

        if (DateFrom.HasValue)
        {
            details["DateFrom"] = DateFrom.Value;
        }

        if (DateTo.HasValue)
        {
            details["DateTo"] = DateTo.Value;
        }

        details["legendInitializer"] = "core_standardLegendInitializer__" + legend.ClientID;

        string url = string.Format("/Orion/Charts/CustomChart.aspx?ResourceID={0}", this.Resource.ID);

        if (!string.IsNullOrEmpty(Request["NetObject"]))
            url = string.Format("{0}&NetObject={1}", url, Request["NetObject"]);
        details["ChartEditUrl"] = url;

        if (ChartResourceSettings != null && ChartResourceSettings.ChartSettings != null)
        {
            details["loadingMode"] = (short)ChartResourceSettings.ChartSettings.LoadingMode;
        }
        else
        {
            details["loadingMode"] = LoadingModeEnum.StandardLoading;
        }
		
        return details;
    }

    protected virtual Dictionary<string, object> GetContextForMacros(NetObject netObject)
    {
        return netObject.ObjectProperties;
    }

    /// <summary>
    /// Gets the list of uniqueidentifier elements for which we will need to retrieve data points for charts
    /// </summary>
    /// <returns>List of uniqueidentifiers of elements for which we via webservice will want to retrieved data.</returns>
    protected IEnumerable<string> GetElementIdsForChart()
    {
        if (ParentObject != null)
        {
            if (Resource.Properties.ContainsKey("ElementList"))
            {
                return Resource.Properties["ElementList"].Split(',');
            }
            else
            {
                return SensorSwisDal.GetSensorIDsByUnit(ParentObject, (int)ChartDataHelper.GetHardwareUnit(Resource.Properties["ChartName"])).Select(x=>x.ToString());
            }
        }

        return new string[0];
    }

    private Dictionary<string, string> GetResourcePropertiesAsDictionary()
    {
        var result = new Dictionary<string, string>();
        foreach (DictionaryEntry entry in Resource.Properties)
        {
            result[entry.Key.ToString()] = entry.Value.ToString();
        }
        return result;
    }

    private string GetChartName(HardwareHealthCategory hardwareHealthCategory, int unit)
    {
        string chartName = string.Empty;

        switch (hardwareHealthCategory)
        {
            case HardwareHealthCategory.Fan:
                chartName = "HardwareHealthFans";
                break;
            case HardwareHealthCategory.Temperature:
                chartName = "HardwareHealthTemperature";
                break;
            case HardwareHealthCategory.PowerSupply:
                if (unit == (int)HardwareHealthUnit.Volts)
                {
                    chartName = "HardwareHealthPowerSupplyV";
                }
                else if (unit == (int)HardwareHealthUnit.Watts)
                {
                    chartName = "HardwareHealthPowerSupplyW";
                }
                else if (unit == (int)HardwareHealthUnit.Amps)
                {
                    chartName = "HardwareHealthPowerSupplyA";
                }
                break;
            default:
                break;
        }

        return chartName;
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
