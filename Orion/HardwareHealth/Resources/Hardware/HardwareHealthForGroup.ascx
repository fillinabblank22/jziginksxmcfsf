﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HardwareHealthForGroup.ascx.cs" Inherits="Orion_HardwareHealth_Resources_Hardware_HardwareHealthForGroup" %>
<%@ Import Namespace="System.Web.Script.Serialization" %>

<orion:ResourceWrapper runat="server" ID="Wrapper">
    <content>
        <% if (string.IsNullOrEmpty(ErrorMessage)) { %>
        <div id="hardwareTree-<%= Resource.ID %>" class="hardwareTree" data-resource-id="<%= Resource.ID %>">

            <!-- includes -->
            <orion:Include ID="Include1" runat="server" File="HardwareHealth/js/Tree.js" />
		    <orion:Include ID="Include2" runat="server" File="HardwareHealth/Styles/HardwareTree.css" />
        
            <!-- template base -->
            <script id="hardwareTree-containerTemplate-<%= Resource.ID %>" type="text/x-template">
                {# if (children && children.length > 0) { #}
                    <table class="hardwareTree-header">
                        <thead>
                            <tr class="header-row">
                                {{ columns }}
                            </tr>
                        </thead>
                    </table>
            
                    <div class="hardwareTree-innerContent">
                        {# _.each(children, function(item, index) { #}
                            {{ #template.item }}
                            {{ #template.children }}
                        {# }); #}     
                    </div>
                {# } else { #}
                    <div class="sw-suggestion sw-suggestion-warn">
                        <span class="sw-suggestion-icon" />
                        <asp:Literal runat="server" ID="tabTemplate2">
                        @{R=HardwareHealth.Strings;K=HHWEBJS_PS0_5; E=js}
                        </asp:Literal>
                    </div>
                {# } #}
            </script>
            
            <!-- expander template -->
            <script id="hardwareTree-expander-<%= Resource.ID %>" type="text/x-template">
                {# if (item.HasChildren) { #}
                    <img src="/Orion/HardwareHealth/Images/Button.Expand.gif" class="hardwareTree-expander" />
                {# } else { #}
                    <div class="hardwareTree-emptyExpander">&nbsp;</div>
                {# } #}&nbsp;
            </script>
            
            <!-- row error template -->
            <script id="hardwareTree-row-error-<%= Resource.ID %>" type="text/x-template">
                <div class="hardwareTree-node">
                    <span class="error sw-pg-hint-body-error">
                        <%= Resources.HardwareHealthWebContent.HHWEBCODE_JP0_3 %>
                    </span>
                </div>
            </script>
            
            <!-- children template -->
            <script id="hardwareTree-children-<%= Resource.ID %>" type="text/x-template">
                <div class="hardwareTree-node-children hidden" data-row-id="{{ item.ID }}">
                    {# if (item.HasChildren) { #}
                    <div class="showMoreRows hidden">
                        <a href="#"><%= Resources.HardwareHealthWebContent.HHWEBDATA_JP0_1 %></a>
                    </div>
                    <div class="loadingRow hidden">
                        <%= Resources.CoreWebContent.WEBDATA_AK0_213 %>
                    </div>
                    {# } else {#}  
                    <div>
                        <asp:Literal runat="server" ID="tabTemplate">
                        @{R=HardwareHealth.Strings;K=HHWEBJS_PS0_6; E=js}
                        </asp:Literal>
                    </div>
                    {# } #}
                </div>
            </script>
        
            <!-- templates per node type -->
            <div class="hardwareTree-templatesContainer" ID="templatesContainer" runat="server" style="display: none"></div>
        
            <!-- controls -->
            <div id="hardwareTree-content-<%= Resource.ID %>"></div>
            <div id="hardwareTree-pager-<%= Resource.ID %>" class="ReportFooter ResourcePagerControl"></div>
		
            <!-- schedule setup -->
            <script type="text/javascript">
                SW.HWH.Tree.Setup('<%= Resource.ID %>', '<%= Resource.View.ViewID %>', '<%= ParentObject.ToString() %>', {
                    dataProvider: '<%= DataProviderName %>',
                    rememberState: <%= RememberState ? "true" : "false" %>,
                    expandAllByDefault: <%= ExpandAllByDefault ? "true" : "false"%>,
                    isInteractive: <%= IsInteractive ? "true" : "false"%>,
                    initialState: <%= RememberState && HardwareTreeExpansionState != null ? new JavaScriptSerializer().Serialize(HardwareTreeExpansionState.ToDictionary()) : "null" %>,
                    itemLimit: <%= ItemLimit %>,
                    topItemLimit: <%= TopItemLimit%>,
                    orderableColumns: [<%= OrderableColumns.Any() ? "'" + String.Join("','", OrderableColumns) + "'" : String.Empty %>]
                });
            </script>

        </div>
        <% } else { %>
        <div class="sw-suggestion sw-suggestion-fail">
            <span class="sw-suggestion-icon"></span>

            <%= ErrorMessage%>
        </div>
        <% } %>
    </content>
</orion:ResourceWrapper>