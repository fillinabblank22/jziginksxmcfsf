﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using SolarWinds.HardwareHealth.Common.Extensions.System;
using SolarWinds.HardwareHealth.Common.Models.HardwareHealth;
using SolarWinds.HardwareHealth.Web.Shared.DAL;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using SolarWinds.Orion.Security;
using System.Text;
using System.Linq;
using Castle.Core.Internal;
using SolarWinds.HardwareHealth.Web;
using SolarWinds.Orion.Core.Common.i18n;

public partial class Orion_HardwareHealth_Resources_Hardware_HardwareHealthDetails : System.Web.UI.Page
{
    #region Event Handlers

    protected void OnHardwareHealthViewGrid_Init(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            String selStatus = String.Empty;
            if (this.ViewHardwareStatus == "Other")
            {
                List<OrionStatus> availableStatus = new List<OrionStatus> {
                    OrionStatus.Up, OrionStatus.Critical, OrionStatus.Warning
                };
                foreach (Object status in Enum.GetValues(typeof(OrionStatus)))
                {
                    if (!availableStatus.Contains((OrionStatus)status))
                    {
                        selStatus = string.IsNullOrEmpty(selStatus) ? String.Format(CultureInfo.InvariantCulture, "{0}", (Int32)status) : String.Format(CultureInfo.InvariantCulture, "{0},{1}", (Int32)status, selStatus);
                    }
                }
            }
            else
            {
                selStatus = ((Int32)this.HardwareStatus).ToString(CultureInfo.InvariantCulture);
            }

            Int32 viewID;
            if (tryDecrypt(Request["ViewID"], out viewID) && viewID > 0)
            {
                var view = SolarWinds.Orion.Web.ViewManager.GetViewById(viewID);
                if (view != null)
                {
                    System.Web.HttpContext.Current.Items[view.GetType().Name] = view;
                }

                string filterSql = this.FilterBySelectedObjectType;
                this.nodeViewGrid.DataSource = HardwareDal.GetParentObjectsByCurrentHardwareHealthStatus(filterSql, selStatus);
                this.nodeViewGrid.DataBind();
            }
            else
                throw new ArgumentException("ViewID");
        }
    }

    private bool tryDecrypt(string cryptoText, out int viewID)
    {
        viewID = 0;
        SolarWinds.Orion.Security.CryptoHelper cryptoHelper = new SolarWinds.Orion.Security.CryptoHelper();
        string Id = null;
        if (!string.IsNullOrEmpty(cryptoText))
        {
            Id = cryptoHelper.Decrypt(cryptoText);
            return Int32.TryParse(Id, out viewID);
        }
        else
        {
            return false;
        }
    }

    #endregion

    #region Property

    private HardwareHealthDal hardwareDal;
    protected HardwareHealthDal HardwareDal
    {
        get
        {
            if (hardwareDal == null)
            {
                hardwareDal = new HardwareHealthDal();
            }
            return hardwareDal;
        }
    }

    protected string FilterBySelectedObjectType
    {
        get
        {
            if (ParentObjectTypes != null)
            {
                string filterSql = @" AND ParentObjectEntity in ({0})";
                var selectedObjectTypeFilter = string.Format(filterSql, string.Join(",", ParentObjectTypes.Select(i => String.Format("'{0}'", i))));
                return selectedObjectTypeFilter;
            }
            throw new ArgumentNullException("entityType");
        }
    }

    protected string[] ParentObjectTypes
    {
        get
        {
            String parObjectType = this.Request["objectType"];
            return (parObjectType.IsNullOrEmpty() ? null : parObjectType.Split(':'));
        }
    }
    protected string ParentObjectTypeLocalized
    {
        get
        {
            using (LocaleThreadState.EnsurePrimaryLocale())
            {
                var selectedTypes = EntityTypesFactory.GetAvailableDisplayNamePluralsFromType(ParentObjectTypes);
                if (selectedTypes == null)
                    throw new ArgumentNullException("entityType");

                var parObjectType = string.Join(", ", selectedTypes.Values);
                parObjectType = parObjectType + Resources.HardwareHealthWebContent.HHWEBCODE_DO0_27;
                return parObjectType;
            }
        }
    }

    protected string ViewHardwareStatusLocalized
    {
        get
        {
            using (LocaleThreadState.EnsurePrimaryLocale())
            {
                var status = this.Request["SelectedStatus"];
                if (string.CompareOrdinal("Up", status) == 0)
                {
                    status = Resources.HardwareHealthWebContent.HHWEBCODE_DO0_10;
                }
                else if (string.CompareOrdinal("Warning", status) == 0)
                {
                    status = Resources.HardwareHealthWebContent.HHWEBCODE_DO0_11;
                }
                else if (string.CompareOrdinal("Critical", status) == 0)
                {
                    status = Resources.HardwareHealthWebContent.HHWEBCODE_DO0_12;
                }
                else
                {
                    status = Resources.HardwareHealthWebContent.HHWEBCODE_DO0_9;
                }

                return string.Format(CultureInfo.InvariantCulture, Resources.HardwareHealthWebContent.HHWEBDATA_VB1_1, ParentObjectTypeLocalized, status);
            }
        }
    }

    protected String ViewHardwareStatus
    {
        get
        {
            String status = this.Request["SelectedStatus"];
            if (status == Int32.MaxValue.ToString())
            {
                return "Other";
            }
            return status;
        }
    }

    protected Int32 LimitationID
    {
        get
        {
            Int32 id = 0;
            Int32.TryParse(this.Request["Limit"], out id);

            return id;
        }
    }

    protected OrionStatus HardwareStatus
    {
        get
        {
            switch (this.ViewHardwareStatus)
            {
                case "Up":
                    return OrionStatus.Up;
                case "Critical":
                    return OrionStatus.Critical;
                case "Unknown":
                    return OrionStatus.Unknown;
                case "Warning":
                    return OrionStatus.Warning;
                case "Down":
                    return OrionStatus.Down;
                case "Unmanaged":
                    return OrionStatus.Unmanaged;
                case "Unreachable":
                    return OrionStatus.Unreachable;
            }
            return OrionStatus.Undefined;
        }
    }

    #endregion

    #region Helper Members

    protected String GetRowItem(Object item)
    {
        DataRowView row = item as DataRowView;
        if (row != null)
        {
            const String HTML = @"<a href='/Orion/View.aspx?NetObject={0}:{1}'>{2}</a>";

            return String.Format(HTML, row["ParentObjectType"], row["ParentObjectID"], row["ParentObjectName"]);
        }
        return "N/A";
    }

    protected string GetHardwareStatusIcon(OrionStatus status)
    {
        string root = "/Orion/HardwareHealth/Images/";
        string iconName = string.Empty;

        switch (status)
        {
            case OrionStatus.Up:
                {
                    iconName = "Server_Small_Up.png";
                    break;
                }
            case OrionStatus.Warning:
                {
                    iconName = "Server_Small_Warning.png";
                    break;
                }
            case OrionStatus.Critical:
            case OrionStatus.Down:
                {
                    iconName = "Server_Small_Down.png";
                    break;
                }
            default:
                {
                    iconName = "Server_Small_Unknown.png";
                    break;
                }
        }

        return string.Format(CultureInfo.InvariantCulture, "{0}{1}", root, iconName);
    }

    #endregion
}