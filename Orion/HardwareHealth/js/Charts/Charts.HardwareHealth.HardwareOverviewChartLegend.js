﻿SW = SW || {};
SW.HardwareHealth = SW.HardwareHealth || {};
SW.HardwareHealth.Charts = SW.HardwareHealth.Charts || {};
SW.HardwareHealth.Charts.HardwareOverviewChartLegend = SW.HardwareHealth.Charts.HardwareOverviewChartLegend || {};
(function (legend) {
    legend.createStandardLegend = function (chart, legendContainerId) {
        var table = $('#' + legendContainerId);
        var serie = chart.series[0];
        var row = $('<tr />');

        // we need to clean the table, because of async refresh
        table.empty();

        var parentTypeLocalized;
        parentTypeLocalized = serie.data[0].objectsDisplayName + " @{R=HardwareHealth.Strings;K=HHWEBJS_DO0_07;E=js} ";

        $('<td colspan = 6 ><b> ' + parentTypeLocalized + serie.data[0].size + '</b></td>').appendTo(row);
        row.appendTo(table);

        row = $('<tr />');
        $('<td colspan = 6 > </td>').appendTo(row);
        row.appendTo(table);

        row = $('<tr />');

        for (var i = serie.data.length - 1, index = 0; i >= 0; i--) {
            var point = serie.data[index];
            var targetUrl = "/Orion/HardwareHealth/Resources/HardwareHealthDetails.aspx?SelectedStatus=" +
                point.name +
                "&objectType=" + point.objectType +
                "&ViewID=" +
                point.ViewID;

            $('<td class="hwh_HardwareHealthStatusCount' + point.name + '"><b>' + point.count + '</b></td>').appendTo(row);
            $('<td class="hwh_HardwareHealthStatusIcon"><a href="' + targetUrl + '" target=""_blank""><img src="/Orion/HardwareHealth/Images/' + point.icon + '"/></a></td>').appendTo(row);
            $('<td class="hwh_HardwareHealthStatusName" style="white-space: nowrap;"><a href="' + targetUrl + '" target=""_blank"">' + point.legendLabel + '</a></td>').appendTo(row);

            if (index % 2 != 0) {
                row.appendTo(table);
                row = $('<tr />');
            }

            index++;
        }
    };
}(SW.HardwareHealth.Charts.HardwareOverviewChartLegend));