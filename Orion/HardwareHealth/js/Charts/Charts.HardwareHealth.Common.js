﻿SW = SW || {};
SW.HardwareHealth = SW.HardwareHealth || {};
SW.HardwareHealth.Charts = SW.HardwareHealth.Charts || {};
SW.HardwareHealth.Charts.Common = SW.HardwareHealth.Charts.Common || {};
(function (common) {
    common.AvailabilitySeriesTemplates = {
        Unknown_Count: {
            name: "Unknown",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(0,0,0)"],
                    [0.5, "rgb(50,50,50)"],
                    [1, "rgb(100,100,100)"]
                ]
            }
        },
        Up_Count: {
            name: "Up",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(63,134,0)"],
                    [0.5, "rgb(93,163,19)"],
                    [1, "rgb(119,189,45)"]
                ]
            }
        },
        Warning_Count: {
            name: "Warning",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(196,163,0)"],
                    [0.5, "rgb(228,193,16)"],
                    [1, "rgb(252,217,40)"]
                ]
            }
        },
        Critical_Count: {
            name: "Critical",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(145,0,0)"],
                    [0.5, "rgb(205,0,16)"],
                    [1, "rgb(230,25,41)"]
                ]
            }
        },
        Unmanaged_Count: {
            name: "Unmanaged",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(0,0,145)"],
                    [0.5, "rgb(16,0,205)"],
                    [1, "rgb(41,25,230)"]
                ]
            }
        },
        Unreachable_Count: {
            name: "Unreachable",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(50,50,50)"],
                    [0.5, "rgb(100,100,100)"],
                    [1, "rgb(150,150,150)"]
                ]
            }
        },
        Undefined_Count: {
            name: "Undefined",
            type: "column",
            zIndex: 2,
            color: {
                linearGradient: {
                    x1: 1,
                    y1: 0.8,
                    x2: 0,
                    y2: 0
                },
                stops: [
                    [0, "rgb(100,100,100)"],
                    [0.5, "rgb(150,150,150)"],
                    [1, "rgb(200,200,200)"]
                ]
            }
        },
        Availability: {
            name: "Availability",
            id: "navigator",
            showInLegend: false,
            visible: false
        }
    };

    common.TooltipDateFormat = "%A, %b %e %Y, %H:%M";

    common.AvailabilityTooltip = {
        xDateFormat: common.TooltipDateFormat,
        formatter: function () {
            var pThis = this,
                items = pThis.points || splat(pThis),
                series = items[0].series,
                s;

            // build the header
            var header = series.tooltipHeaderFormatter(items[0].key);
            
            // Galaga changed formatting to use tables so we have to count with it.
            var isTable = (header.indexOf('</tr>') != -1); // if header contains </tr> then it's a table layout
            s = [header];

            for (var i = 0; i < items.length; i++) {
                if (!isTable) {
                    s.push("<b>" + items[i].series.name + ":</b> " + items[i].point.y + "x (" + items[i].point.percentage.toFixed(2) + "%)<br />");
                } else {
                    var color = items[i].series.color;
                    if (typeof color === 'object' && color.stops && color.stops.length > 0)
                    {
                      color = color.stops[color.stops.length - 1][1];
                    }
                    s.push('<tr style="line-height: 90%; font-weight: bold;"><td style="border: 0; font-size: 12px; color: ' + color + ';">' + items[i].series.name + ': </td><td style="border: 0px; font-size: 12px"><b>' + items[i].point.y + 'x (' + items[i].point.percentage.toFixed(2) + '%)</b></td></td></tr>');
                }
            }

            s.push(series.chart.options.tooltip.footerFormat || '');
            
            return s.join("");
        }
    };

} (SW.HardwareHealth.Charts.Common));