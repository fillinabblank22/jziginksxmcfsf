﻿(function (ExpansionState, DomElements) {
    var settingDefaults = {
        dataProvider: null,
        initialState: null,
        rememberState: false,
        expandAllByDefault: false,
        columnSelector: '> span',
        initialPage: 0,
        itemLimit: 10,
        topItemLimit: 10,
        isInteractive: true,
        orderableColumns: [],
        orderBy: null,
        pageManager: null,
        onExpansionFinished: null,
        onPostCreate: null,
        onPostTreeDraw: null,
        onPostItemLoad: null
    };

    SW.Core.namespace("SW.HWH.Tree").Settings = {
        instances: {},
        create: function (resourceId, viewId, netObjectId, extras) {
            var instance = $.extend({
                id: resourceId,
                netObjectId: netObjectId,
                resourceId: resourceId,
                viewId: viewId
            }, settingDefaults, extras);

            if (extras.initialState) {
                instance.initialState = new ExpansionState(extras.initialState);
            }

            if (instance.orderableColumns.length) {
                if (instance.initialState && instance.initialState.getOrderColumn()) {
                    instance.orderBy = {
                        column: instance.initialState.getOrderColumn(),
                        order: instance.initialState.getOrderDirection()
                    };
                } else {
                    instance.orderBy = {
                        column: instance.orderableColumns[0],
                        order: 'Asc'
                    };
                }
            }

            this.instances[resourceId] = instance;
        },
        get: function (id) {
            return this.instances[id];
        },
        findFor: function ($contentElement) {
            var container = $contentElement.closest(DomElements.selectors.containerGeneric);
            return container.length ? this.get(container.data('resourceId')) : null;
        }
    };

}(SW.HWH.Tree.ExpansionState, SW.HWH.Tree.DomElements));
