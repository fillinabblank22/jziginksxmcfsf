﻿(function (Settings, Service, PageManagers, DomElements, ExpansionManager, PagerControls) {
   
    var setupInternal = function (id) {
        var settings = Settings.get(id);
        var dom = DomElements.create(settings);

        var onExpansionFinished = settings.onExpansionFinished;
        settings.onExpansionFinished = function () {
            if (settings.rememberState) {
                saveExpansionState();
            }

            onExpansionFinished && onExpansionFinished();
        };
        
        var createTable = function (topChildrenCount) {
            loadTable(settings.initialPage, settings.topItemLimit, topChildrenCount, true);
            dom.getResource().bind('resized', dom.refreshColumnWidths.bind(dom));
        };

        var loadTable = function (pageIndex, pageSize, totalRows, isFirstLoad) {
            settings.pageManager = PageManagers.create(id, pageIndex, pageSize || PagerControls.getCurrentPageSize(id));
            var data = {
                context: Service.createTreeContext(settings.id),
                startItem: settings.pageManager.startItem(),
                lastItem: settings.pageManager.lastItem(),
                orderColumn: settings.orderBy ? settings.orderBy['column'] : null,
                orderDirection: settings.orderBy ? settings.orderBy['order'] : null,
                parentProperties: {},
                level: 0
            };

            Service.call('getChildren', data, function (result) {
                processTableResult(result, totalRows, isFirstLoad);
            }, function () {
                dom.getContent().html(dom.getRowError());
            });
        };

        var processHeaders = function () {
            if (!settings.isInteractive) {
                return;
            }

            var handleClick = function () {
                var self = $(this);
                var column = self.closest('.' + DomElements.selectors.headerColumnOrderableClassname);
                var orderName = column.data('hardwareTree-orderName');

                if (settings.orderBy['column'] != orderName) {
                    settings.orderBy['column'] = orderName;
                    settings.orderBy['order'] = self.hasClass(DomElements.selectors.headerColumnOrderAscClassname) ? 'Asc' : 'Desc';
                } else {
                    settings.orderBy['order'] = self.hasClass(DomElements.selectors.headerColumnOrderAscClassname) ? 'Desc' : 'Asc';
                }

                loadTable(settings.pageManager.currentPageIndex, settings.pageManager.rowsPerPage);
                processHeaders();
            };

            $.each(dom.getOrderableColumns(), function (index, column) {
                var self = $(column);
                self.addClass(DomElements.selectors.headerColumnOrderableClassname);

                var arrow = self.find('.' + DomElements.selectors.headerColumnOrderArrowClassname);
                if (!arrow.length) {
                    arrow = dom.createOrderArrow(self, handleClick);
                }

                dom.updateOrderArrow(arrow, settings.orderBy, self.data('hardwareTree-orderName'));
            });
        };

        var processTableResult = function (result, totalRows, isFirstLoad) {
            totalRows && (settings.pageManager.totalRowsCount = totalRows);

            var rowsToOutput = result.slice(0, settings.pageManager.rowsPerPage);
            var source = dom.getContainerTemplate().html();
            var itemTypeTemplate = rowsToOutput.length ? dom.getItemTemplate(rowsToOutput[0]["Type"]) : "";
            var newHtml = dom.template(source, { children: rowsToOutput, columns: dom.getHeaderColumnsTemplate() }, { item: itemTypeTemplate });
            dom.getContent().html(newHtml);
            processHeaders();

            var $topItems = dom.getInnerContent().children(DomElements.selectors.itemNode);

            dom.setColumnWidths($topItems);
            processTopNodes($topItems, 0, rowsToOutput);

            PagerControls.updatePagerControls(id, settings.pageManager, totalRows || settings.pageManager.totalRowsCount, loadTable);

            if (isFirstLoad) {
                settings.onPostCreate && settings.onPostCreate($topItems, settings);
            }

            settings.onPostTreeDraw && settings.onPostTreeDraw($topItems, settings);

            if (settings.expandAllByDefault || settings.initialState) {
                settings.expansionManager.expandTable(settings.expandAllByDefault || settings.initialState.getExpansionSchema(), dom.getInnerContent());
            }
        };

        var processRowsResult = function (result, currentItem, rows) {
            if (!result) {
                return;
            }

            var itemLimit = rows || settings.itemLimit;
            var rowsToOutput = result.slice(0, itemLimit);
            var $childrenContainer = dom.getChildrenContainer(currentItem);
            var $showMoreRows = $childrenContainer.children(DomElements.selectors.showMoreRows);

            $.each(rowsToOutput, function (index, value) {
                var source = rowsToOutput.length ? dom.getItemTemplate(rowsToOutput[0]['Type']) : '';
                var newHtml = dom.template(source, { item: value });
                $showMoreRows.before(newHtml);
                settings.onPostItemLoad && settings.onPostItemLoad($showMoreRows.before(), settings);
            });

            var $newItems = $childrenContainer.children(DomElements.selectors.itemNode);
            dom.setColumnWidths($newItems);
            processNodes($newItems, currentItem.data('level') + 1, rowsToOutput);

            currentItem.data('startFrom', currentItem.data('startFrom') + itemLimit);

            if (settings.isInteractive && result.length > itemLimit) {
                $showMoreRows.show();
            } else {
                $showMoreRows.hide();
            }
        };

        var processTopNodes = function ($nodes, level, data) {
            $nodes.addClass(DomElements.selectors.itemNodeParentClassname);
            $nodes.filter(':odd').addClass(DomElements.selectors.itemOddClassname);
            processNodes($nodes, level, data);
        };

        var processNodes = function ($nodes, level, data) {
            $nodes = $nodes.filter(':not(.hardwareTree-processed)');
            $nodes.data('level', level);
            $nodes.each(function (index, element) {
                if (data[index]['StoreProperties']) {
                    $(element).data('nodeProperties', data[index]);
                }
            });
            $nodes.each(function () {
                var self = $(this).find(DomElements.selectors.expanderGeneric);
                self.click(function () {
                    toggleExpanderImage(self, showCallback, hideCallback);

                    if (settings.rememberState) {
                        saveExpansionState();
                    }
                });
            });
            $nodes.addClass('hardwareTree-processed');
        };

        var saveExpansionState = function () {
            var schema = settings.expansionManager.getTableExpansionSchema(dom.getInnerContent());
            Service.call('saveExpansionState', {
                context: Service.createTreeContext(id),
                pageIndex: settings.pageManager.currentPageIndex,
                pageSize: settings.pageManager.rowsPerPage,
                expansionSchema: schema,
                orderColumn: settings.orderBy ? settings.orderBy['column'] : null,
                orderDirection: settings.orderBy ? settings.orderBy['order'] : null
            });
        };

        var showError = function (currentItem) {
            var $childrenContainer = dom.getChildrenContainer(currentItem);
            $childrenContainer.html(dom.getRowError());
        };

        var showCallback = function (currentItem, rows, onLoaded) {
            var childrenItem = dom.getChildrenContainer(currentItem);
            childrenItem.show('fade', 250);
            if (!currentItem.data('loaded')) {
                var errorCallback = function () {
                    showError(currentItem);
                };

                currentItem.data('startFrom', 0);
                callGetItemsService(currentItem, (function (result) {
                    currentItem.data('loaded', true);
                    currentItem.find('.loadingRow').hide();
                    processRowsResult(result, currentItem, rows);
                    onLoaded && onLoaded(childrenItem);
                }), errorCallback, rows);
                childrenItem.find('.showMoreRows a').click(function () {
                    callGetItemsService(currentItem, (function (result) {
                        currentItem.find('.loadingRow').hide();
                        processRowsResult(result, currentItem);
                    }), errorCallback);
                    return false;
                });
            }
        };

        var hideCallback = function (currentItem) {
            dom.getChildrenContainer(currentItem).hide('fade', 150);
        };

        var toggleExpanderImage = function ($button, showCb, hideCb) {
            if (!$button.length) {
                return;
            }

            var $currentItem = $button.closest(DomElements.selectors.itemNode);
            if ($button.attr('src').indexOf('Expand') != -1) {
                $button.attr('src', '/Orion/HardwareHealth/Images/Button.Collapse.gif');
                $currentItem.data('expanded', true);
                showCb($currentItem);
            } else {
                $button.attr('src', '/Orion/HardwareHealth/Images/Button.Expand.gif');
                $currentItem.data('expanded', false);
                hideCb($currentItem);
            }
        };

        var expandNode = function ($button, callback) {
            if (!$button.length) {
                return false;
            }

            var $currentItem = $button.closest(DomElements.selectors.itemNode);
            $button.attr('src', '/Orion/HardwareHealth/Images/Button.Collapse.gif');
            $currentItem.data('expanded', true);
            callback($currentItem);
        };

        var callGetItemsService = function (currentItem, callback, errorCallback, rows) {
            currentItem.find('.loadingRow').show();

            var data = {
                context: Service.createTreeContext(settings.id),
                startItem: currentItem.data('startFrom'),
                lastItem: currentItem.data('startFrom') + (rows || settings.itemLimit),
                orderColumn: settings.orderBy ? settings.orderBy['column'] : null,
                orderDirection: settings.orderBy ? settings.orderBy['order'] : null,
                parentProperties: currentItem.data('nodeProperties'),
                level: currentItem.data('level') + 1
            };

            Service.call('getChildren', data, callback, errorCallback);
        };

        settings.expansionManager = new ExpansionManager(settings, showCallback, expandNode);

        var refresh = function () {
            Service.call('getTopChildrenCount', { dataProvider: settings.dataProvider, netObjectId: settings.netObjectId }, createTable);
        };

        SW.Core.View.AddOnRefresh(refresh, settings.resourceId);
        refresh();
    };

    SW.Core.namespace("SW.HWH").Tree = {
        Setup: function (resourceId, viewId, netObjectId, extras) {
            $(function() {
                Settings.create(resourceId, viewId, netObjectId, extras || {});
                setupInternal(resourceId);
            });
        },
        ExpandAll: function (resourceId) {
            var settings = Settings.get(resourceId);
            var dom = DomElements.get(settings);
            settings.expandAllByDefault = true;
            settings.expansionManager.expandTable(true, dom.getInnerContent());
        },
        GetData: function (resourceId) {
            var settings = Settings.get(resourceId);
            return DomElements.get(settings).getTreeData();
        },
        GetExpandedAllData: function (resourceId, callback) {
            var self = this;
            var settings = Settings.get(resourceId);
            var onExpansionFinished = settings.onExpansionFinished;
            var itemLimit = settings.itemLimit;
            settings.itemLimit = 1000000;
            settings.onExpansionFinished = function () {
                callback && callback(self.GetData(resourceId));

                settings.onExpansionFinished = onExpansionFinished;
                settings.itemLimit = itemLimit;

                onExpansionFinished && onExpansionFinished();
            };

            this.ExpandAll(resourceId);
        }
    };

}(
    SW.HWH.Tree.Settings,
    SW.HWH.Tree.Service,
    SW.HWH.Tree.PageManagers,
    SW.HWH.Tree.DomElements,
    SW.HWH.Tree.ExpansionManager,
    SW.HWH.Tree.PagerControls
));
