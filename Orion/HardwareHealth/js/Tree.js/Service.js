﻿(function (CoreServices, Settings) {

    SW.Core.namespace("SW.HWH.Tree").Service = {
        settings: {
            uri: '/Orion/HardwareHealth/Services/HardwareTreeService.asmx',
            getChildren: 'GetChildren',
            getTopChildrenCount: 'GetTopChildrenCount',
            saveExpansionState: 'SaveExpansionState'
        },
        call: function(method, data, successCallback, errorCallback) {
            CoreServices.callWebService(this.settings.uri, this.settings[method], data || {}, successCallback || function() {}, errorCallback);
        },
        createTreeContext: function(settingsId) {
            var settings = Settings.get(settingsId);
            return {
                parentObject: settings.netObjectId,
                resourceID: settings.resourceId,
                viewID: settings.viewId,
                dataProvider: settings.dataProvider
            };
        }
    };

}(SW.Core.Services, SW.HWH.Tree.Settings));
