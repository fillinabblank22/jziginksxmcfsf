﻿(function (DomElements) {

    SW.Core.namespace("SW.HWH.Tree").ExpansionManager = function (settings, showCallback, expandNodeCallback) {
        var nodesCurrentlyExpanding = 0;
        var dom = DomElements.get(settings);

        var isExpandAll = function(state) {
            return typeof(state) === "boolean" && state;
        }

        var createEmptyExpansionEntry = function($node) {
            return {
                type: $node.data('itemType'),
                id: $node.data('itemId'),
                childrenLoadedCount: 0,
                children: []
            };
        };

        var getNodesToBeExpandedCount = function (state) {
            if (isExpandAll(state)) {
                return null;
            }

            var length = state.length;
            var result = length;
            for (var i = 0; i < length; i++) {
                result += getNodesToBeExpandedCount(state[i]['children']);
            }

            return result;
        };

        var isExpanded = function($node) {
            return $node.data('expanded');
        };

        var getExpansionSchema = function($nodes) {
            var result = [];
            $nodes.each(function() {
                var $child = $(this);
                if (isExpanded($child)) {
                    var children = dom.getChildrenOfNode($child);
                    var expansionEntry = createEmptyExpansionEntry($child);
                    result.push(expansionEntry);

                    expansionEntry['children'] = getExpansionSchema(children);
                    expansionEntry['childrenLoadedCount'] = children.length;
                }
            });

            return result;
        };

        var nodeShowCallback = function(node, subexpansion) {
            showCallback(node, settings.expandAllByDefault ? settings.itemLimit : subexpansion['childrenLoadedCount'], function(childrenContainer) {
                if (settings.expandAllByDefault || subexpansion['children'].length) {
                    var children = childrenContainer.children(DomElements.selectors.itemNode);
                    if (isExpandAll(subexpansion)) {
                        nodesCurrentlyExpanding += children.length;
                    }

                    expandNodes(settings.expandAllByDefault || subexpansion['children'], children);
                }
            });

            if (--nodesCurrentlyExpanding <= 0) {
                settings.onExpansionFinished && settings.onExpansionFinished();
            }
        };

        var expandNodes = function (state, elements) {
            elements.each(function() {
                var node = $(this);
                var subexpansion;
                if (settings.expandAllByDefault || (subexpansion = settings.initialState.getExpansion(node.data('itemId'), state))) {
                    expandNode(node, function() {
                        nodeShowCallback(node, subexpansion);
                    });
                }
            });
        };

        var expandNode = function ($node, callback) {
            var $button = $node.find(DomElements.selectors.expanderGeneric);
            var hasButton = expandNodeCallback($button, callback);
            if (hasButton === false) {
                if (--nodesCurrentlyExpanding <= 0) {
                    settings.onExpansionFinished && settings.onExpansionFinished();
                }
            }
        };

        return {
            getTableExpansionSchema: function($innerContent) {
                return getExpansionSchema($innerContent.children(DomElements.selectors.itemNode));
            },
            expandTable: function (state, $innerContent) {
                var topElements = $innerContent.children(DomElements.selectors.itemNode);
                nodesCurrentlyExpanding = isExpandAll(state) ? dom.getExpansionButtons(topElements).length : getNodesToBeExpandedCount(state);
                
                if (nodesCurrentlyExpanding == 0) {
                    settings.onExpansionFinished && settings.onExpansionFinished();
                } else {
                    expandNodes(state, topElements);
                }
            }
        };
    };

}(SW.HWH.Tree.DomElements));
