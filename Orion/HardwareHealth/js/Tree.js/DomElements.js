﻿(function () {
    var selectors = {
        containerGeneric: '.hardwareTree',
        content: '#hardwareTree-content-{0}',
        containerTemplate: '#hardwareTree-containerTemplate-{0}',
        expanderGeneric: '.hardwareTree-expander',
        expanderTemplate: '#hardwareTree-expander-{0}',
        childrenTemplate: '#hardwareTree-children-{0}',
        innerContent: '.hardwareTree-innerContent',
        headerColumnGeneric: '.hardwareTree-header-column',
        headerColumnsTemplate: '.hardwareTree-header-columns',
        headerColumnOrderableClassname: 'hardwareTree-header-orderable',
        headerColumnOrderArrowClassname: 'hardwareTree-header-orderArrow',
        headerColumnOrderAscClassname: 'orderAsc',
        headerColumnOrderDescClassname: 'orderDesc',
        headerColumnOrderActiveClassname: 'orderActive',
        pager: '#hardwareTree-pager-{0}',
        itemTemplatesContainer: '.hardwareTree-templatesContainer',
        itemTemplates: '#hardwareTree-templates-{0}',
        itemTemplateGeneric: '.hardwareTree-template-item',
        itemNode: '.hardwareTree-node',
        itemNodeParent: '.hardwareTree-node-parent',
        itemNodeParentClassname: 'hardwareTree-node-parent',
        itemNodeChildren: '.hardwareTree-node-children',
        itemOddClassname: 'hardwareTree-node-odd',
        rowErrorTemplate: '#hardwareTree-row-error-{0}',
        showMoreRows: '.showMoreRows',
        wrapper: '#hardwareTree-{0}'
    };

    var literals = {
        showAllText: '@{R=Core.Strings;K=WEBJS_TM1_CUSTOMQUERY_SHOWALL;E=js}', //Show all
        displayingObjectsText: '@{R=Core.Strings;K=WEBJS_AK0_54;E=js}', //Displaying objects {0} - {1} of {2}
        pageXofYText: '@{R=Core.Strings;K=WEBJS_JT0_2;E=js}', //Page {0} of {1}
        itemsOnPageText: '@{R=Core.Strings;K=WEBJS_JT0_3;E=js}', // Items on page
        style: 'style="vertical-align:middle"',
        firstImgRoot: '/Orion/images/Arrows/button_white_paging_first',
        previousImgRoot: '/Orion/images/Arrows/button_white_paging_previous',
        nextImgRoot: '/Orion/images/Arrows/button_white_paging_next',
        lastImgRoot: '/Orion/images/Arrows/button_white_paging_last'
    };

    SW.Core.namespace("SW.HWH.Tree").DomElements = {
        instances: {},
        get: function (settings) {
            return this.instances[settings.resourceId] || this.create(settings);
        },
        getById: function(settingsId) {
            return this.instances[settingsId];
        },
        create: function(settings) {
            var settingsId = settings.resourceId;

            var select = function(selector) {
                return selector.replace('{0}', settingsId);
            };
            
            // preload elements
            var $wrapper = $(select(selectors.wrapper));
            var $content = $(String.format(selectors.content, settingsId), $wrapper);
            var $innerContent = $(select(selectors.innerContent), $content);
            var $containerTemplate = $(select(selectors.containerTemplate), $wrapper);
            var $expanderTemplate = $(select(selectors.expanderTemplate), $wrapper);
            var $childrenTemplate = $(select(selectors.childrenTemplate), $wrapper);
            var $pager = $(select(selectors.pager), $wrapper);
            var $itemTemplatesContainer = $(select(selectors.itemTemplatesContainer), $wrapper);

            var itemTemplates = {};
            var itemTemplateTypes = [];
            $(select(selectors.itemTemplateGeneric), $itemTemplatesContainer).each(function() {
                var item = $(this);
                itemTemplates[item.data('itemType')] = item;
                itemTemplateTypes.push(item.data('itemType'));
            });

            var rowErrorContent = $(select(selectors.rowErrorTemplate), $wrapper).html();
            var headerColumnsTemplate = $(selectors.headerColumnsTemplate, $itemTemplatesContainer).html();

            var templateData = {
                expander: $expanderTemplate.html(),
                children: $childrenTemplate.html()
            };

            // header
            var headerWidths;

            this.instances[settingsId] = {
                getContent: function() {
                    return $content;
                },
                getInnerContent: function() {
                    return $innerContent.length ? $innerContent : $(select(selectors.innerContent), $content);
                },
                getContainerTemplate: function() {
                    return $containerTemplate;
                },
                getExpanderTemplate: function() {
                    return $expanderTemplate;
                },
                getHeaderColumnsTemplate: function() {
                    return headerColumnsTemplate;
                },
                getItemTemplate: function(type) {
                    return itemTemplates[type].html();
                },
                getItemTemplateTypes: function () {
                    return itemTemplateTypes;
                },
                getPager: function () {
                    if (!$pager.length) {
                        $pager = $(select(selectors.pager), $wrapper);
                    }

                    return $pager;
                },
                getChildrenContainer: function($parent) {
                    return $parent.next(selectors.itemNodeChildren);
                },
                getChildrenOfNode: function($node) {
                    return this.getChildrenContainer($node).children(selectors.itemNode);
                },
                getRowError: function() {
                    return rowErrorContent;
                },
                getExpansionButtons: function($for) {
                    return $(selectors.expanderGeneric, $for);
                },
                getResource: function() {
                    return $wrapper.closest('.ResourceWrapper');
                },
                createOrderArrow: function(column, clickHandler) {
                    var arrow = $("<span />");
                    arrow.addClass(selectors.headerColumnOrderArrowClassname);
                    arrow.addClass(selectors.headerColumnOrderAscClassname);
                    arrow.appendTo(column);
                    arrow.click(clickHandler);
                    return arrow;
                },
                updateOrderArrow: function(arrow, orderBy, orderName) {
                    if (orderBy == null || orderBy['column'] != orderName) {
                        arrow.removeClass(selectors.headerColumnOrderActiveClassname)
                        return;
                    }

                    arrow.addClass(selectors.headerColumnOrderActiveClassname);
                    arrow.removeClass(selectors.headerColumnOrderAscClassname);
                    arrow.removeClass(selectors.headerColumnOrderDescClassname);
                    if (orderBy['order'] == 'Asc') {
                        arrow.addClass(selectors.headerColumnOrderAscClassname);
                    } else {
                        arrow.addClass(selectors.headerColumnOrderDescClassname);
                    }
                },
                getColumnWidths: function() {
                    if (headerWidths == null) {
                        headerWidths = [];
                        $content.find(selectors.headerColumnGeneric).each(function() {
                            headerWidths.push($(this).outerWidth());
                        });
                    }

                    return headerWidths;
                },
                refreshColumnWidths: function() {
                    headerWidths = null;
                    var $allNodes = this.getInnerContent().find(selectors.itemNode);
                    this.setColumnWidths($allNodes);
                },
                setColumnWidths: function($nodes) {
                    var allParentalIndent = function ($currentParent) {
                        var nodePadding = $currentParent.outerWidth(true) - $currentParent.width();
                        var $nextParent = $currentParent.parent().closest(selectors.itemNodeChildren);

                        return nodePadding - ($nextParent.length ? -allParentalIndent($nextParent) : 0);
                    };

                    var columnWidths = this.getColumnWidths();
                    $nodes.each(function () {
                        var self = $(this);
                        var nodePadding = allParentalIndent(self.parent().closest(selectors.itemNodeChildren));
                        self.find(settings.columnSelector).each(function(index, column) {
                            var $column = $(column);
                            var columnPadding = $column.outerWidth(true) - $column.width();
                            var totalWidth = columnWidths[index];
                            var targetWidth = totalWidth - nodePadding - columnPadding;

                            $column.innerWidth(targetWidth);

                            nodePadding = 0;
                        });
                    });
                },
                getOrderableColumns: function() {
                    var orderableColumns = [];
                    var instance = this;
                    $.each(settings.orderableColumns, function(index, name) {
                        var column = $(".hardwareTree-header-" + name, instance.getContent());
                        column.data('hardwareTree-orderName', name);
                        orderableColumns.push(column);
                    });

                    return orderableColumns;
                },
                getTreeData: function() {
                    var result = {
                        columns: [],
                        values: []
                    };
                    var getData = function($node) {
                        var subresult = {};
                        subresult['data'] = [];
                        subresult['children'] = [];
                        $(settings.columnSelector, $node).each(function() {
                            subresult['data'].push($.trim($(this).html()));
                        });
                        $node.next(selectors.itemNodeChildren).children(selectors.itemNode).each(function() {
                            subresult['children'].push(getData($(this)));
                        });
                        return subresult;
                    };

                    $(selectors.headerColumnGeneric, $wrapper).each(function () {
                        result['columns'].push($(this).text());
                    });

                    this.getInnerContent().children(selectors.itemNode).each(function() {
                        result['values'].push(getData($(this)));
                    });

                    return result;
                },
                template: function(template, data, preprocessData) {
                    preprocessData = $.extend(templateData, preprocessData || {});

                    var limit = 10;
                    while (template.indexOf('{{ #template.') != -1 && --limit > 0) {
                        _.each(preprocessData, function(value, key) {
                            template = template.replace('{{ #template.' + key + ' }}', value);
                        });
                    }

                    var result = "";
                    try {
                        result = _.template(template, data);
                    } catch (ex) {
                        (console.error || console.log)(ex);
                    }

                    return result;
                }
            }

            return this.instances[settingsId];
        },
        selectors: selectors,
        literals: literals
    };
}());
