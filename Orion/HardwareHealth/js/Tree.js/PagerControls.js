﻿(function(DomElements, Settings) {

    SW.Core.namespace("SW.HWH.Tree").PagerControls = {
        getCurrentPageSize: function (uniqueId) {
            var pager = DomElements.getById(uniqueId).getPager();
            var pageSize = pager.find('.pageSize').val();
            if (typeof (pageSize) === 'undefined' || pageSize == '' || pageSize <= 0) {
                pageSize = 1;
            }
            return pageSize;
        },
        updatePagerControls: function (uniqueId, pageManager, rowCount, createTableFromQuery) {
            var self = this;
            var settings = Settings.get(uniqueId);
            var pager = DomElements.getById(uniqueId).getPager();
            var pageIndex = pageManager.currentPageIndex;
            var literals = DomElements.literals;

            var html = [];
            if (!settings.isInteractive) {
                pager.empty();
                return;
            }

            var startHtml, endHtml, contents;

            if (pageIndex > 0) {
                startHtml = '<a href="#" class="firstPage NoTip">';
                contents = String.format('<img src="{0}.gif" {1}/>', literals.firstImgRoot, literals.style);
                endHtml = '</a>';
                html.push(startHtml + contents + endHtml);
                html.push(' | ');
                startHtml = '<a href="#" class="previousPage NoTip">';
                contents = String.format('<img src="{0}.gif" {1}/>', literals.previousImgRoot, literals.style);
                endHtml = '</a>';
                html.push(startHtml + contents + endHtml);
                html.push(' | ');
            } else {
                startHtml = '<span style="color:#646464;">';
                contents = String.format('<img src="{0}_disabled.gif" {1}/>', literals.firstImgRoot, literals.style);
                endHtml = '</span>';
                html.push(startHtml + contents + endHtml);
                html.push(' | ');
                startHtml = '<span style="color:#646464;">';
                contents = String.format('<img src="{0}_disabled.gif" {1}/>', literals.previousImgRoot, literals.style);
                endHtml = '</span>';
                html.push(startHtml + contents + endHtml);
                html.push(' | ');
            }
            startHtml = String.format(literals.pageXofYText, '<input type="text" class="pageNumber SmallInput" value="' + (pageManager.currentPageIndex + 1) + '" />', pageManager.numberOfPages());
            html.push(startHtml);
            html.push(' | ');
            if (!pageManager.isLastPage(rowCount)) {
                startHtml = '<a href="#" class="nextPage NoTip">';
                contents = String.format('<img src="{0}.gif" {1}/>', literals.nextImgRoot, literals.style);
                endHtml = '</a>';
                html.push(startHtml + contents + endHtml);
                html.push(' | ');
                startHtml = '<a href="#" class="lastPage NoTip">';
                contents = String.format('<img src="{0}.gif" {1}/>', literals.lastImgRoot, literals.style);
                endHtml = '</a>';
                html.push(startHtml + contents + endHtml);
                html.push(' | ');
            } else {
                startHtml = '<span style="color:#646464;">';
                contents = String.format('<img src="{0}_disabled.gif" {1}/>', literals.nextImgRoot, literals.style);
                endHtml = '</span>';
                html.push(startHtml + contents + endHtml);
                html.push(' | ');
                startHtml = '<span style="color:#646464;">';
                contents = String.format('<img src="{0}_disabled.gif" {1}/>', literals.lastImgRoot, literals.style);
                endHtml = '</span>';
                html.push(startHtml + contents + endHtml);
                html.push(' | ');
            }
            contents = literals.itemsOnPageText;
            endHtml = '<input type="text" class="pageSize SmallInput" value="' + (pageManager.rowsPerPage > 0 ? pageManager.rowsPerPage : "") + '" />';
            html.push(contents + endHtml);
            html.push(' | ');
            html.push('<a href="#" class="showAll NoTip">' + literals.showAllText + '</a>');
            html.push(' | ');
            html.push('<div class="ResourcePagerInfo">');
            startHtml = String.format(literals.displayingObjectsText, pageManager.startItem() + 1, Math.min(pageManager.lastItem() + 1, pageManager.totalRowsCount), pageManager.totalRowsCount);
            html.push(startHtml);
            html.push('</div>');

            // update pager
            var showPagination = pageIndex > 0 || !pageManager.isLastPage(rowCount);

            pager.empty().append(html.join(' '));
            pager[showPagination ? 'show' : 'hide']();
            pager.data('initialized', true);

            // set pager handlers
            pager.find('.firstPage').click(function() {
                createTableFromQuery(0, pageManager.rowsPerPage);
                return false;
            });
            pager.find('.previousPage').click(function() {
                createTableFromQuery(pageIndex - 1, pageManager.rowsPerPage);
                return false;
            });
            pager.find('.nextPage').click(function() {
                createTableFromQuery(pageIndex + 1, pageManager.rowsPerPage);
                return false;
            });
            pager.find('.lastPage').click(function() {
                createTableFromQuery(pageManager.numberOfPages() - 1, pageManager.rowsPerPage);
                return false;
            });
            pager.find('.showAll').click(function() {
                createTableFromQuery(0, pageManager.totalRowsCount);
                return false;
            });
            var changePageSize = function () {
                createTableFromQuery(0, self.getCurrentPageSize(uniqueId));
            };
            pager.find('.pageSize').change(function() {
                changePageSize();
            });
            pager.find('.pageSize').keydown(function(e) {
                if (e.keyCode == 13) {
                    changePageSize();
                    return false;
                }
                return true;
            });
            var changePageNumber = function() {
                var pageNumber = pager.find('.pageNumber').val();
                if (pageNumber <= 0) {
                    pageNumber = 1;
                } else if (pageNumber > pageManager.numberOfPages()) {
                    pageNumber = pageManager.numberOfPages();
                }
                createTableFromQuery(pageNumber - 1, pageManager.rowsPerPage);
            };
            pager.find('.pageNumber').change(function() {
                changePageNumber();
            });
            pager.find('.pageNumber').keyup(function(e) {
                if (e.keyCode == 13) {
                    changePageNumber();
                    return false;
                }
                return true;
            });
        }
    };
}(SW.HWH.Tree.DomElements, SW.HWH.Tree.Settings))
