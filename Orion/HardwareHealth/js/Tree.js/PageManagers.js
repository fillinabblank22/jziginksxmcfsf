﻿SW.Core.namespace("SW.HWH.Tree").PageManagers = {
    instances: {},
    get: function (resourceId) {
        return this.instances[resourceId];
    },
    create: function (resourceId, pageIndex, pageSize) {
        if (this.instances[resourceId]) {
            this.instances[resourceId].currentPageIndex = pageIndex;
            this.instances[resourceId].rowsPerPage = pageSize;
            return this.instances[resourceId];
        }

        var instance = {};
        instance.currentPageIndex = pageIndex || 0;
        instance.rowsPerPage = pageSize;
        instance.totalRowsCount = 0;
        instance.startItem = function () {
            return (instance.rowsPerPage * instance.currentPageIndex);
        };
        instance.lastItem = function () {
            return (instance.rowsPerPage * (instance.currentPageIndex + 1)) - 1;
        };
        instance.numberOfPages = function () {
            return Math.ceil(instance.totalRowsCount / instance.rowsPerPage);
        };
        instance.isLastPage = function (rowCount) {
            return rowCount <= instance.lastItem() + 1;
        };

        return this.instances[resourceId] = instance;
    }
};
