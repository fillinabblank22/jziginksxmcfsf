﻿SW.Core.namespace("SW.HWH.Tree").ExpansionState = function (initialState) {
    return {
        getExpansionSchema: function() {
            return initialState.expansionSchema;
        },
        getOrderColumn: function() {
            return initialState.orderColumn;
        },
        getOrderDirection: function() {
            return initialState.orderDirection;
        },
        getPageIndex: function() {
            return initialState.pageIndex;
        },
        getPageSize: function() {
            return initialState.pageSize;
        },
        getStartItem: function () {
            return initialState.pageIndex * initialState.pageSize;
        },
        getLastItem: function () {
            return (initialState.pageIndex + 1) * initialState.pageSize;
        },
        getExpansion: function (id, substate) {
            var length = substate.length;
            for (var i = 0; i < length; i++) {
                if (substate[i]['id'] == id) {
                    return substate[i];
                }
            }

            return false;
        }
    }
};

