﻿var customThresholdsDisabled = false;

$(function () {
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json;charset=utf-8',
        url: '/sapi/HardwareHealthThresholdManagement/GetStoredSensors',
        success: function (result) {
            if (result.SelectedSensorCount === 0) {
                showNoItemsError();
                return;
            }
            if (result.SelectedSensorCount > 1) {
                showMultipleSensorsWarning(result.SelectedSensorCount);
            }
            if (result.IncompatibleUnits.length > 0) {
                $('#thresholdRadioUseCustom').attr('disabled', true);
                showIncompatibleUnitsWarning(result.IncompatibleUnits);
                customThresholdsDisabled = true;
            }
            if (result.SensorsWithoutValues.length > 0) {
                $('#thresholdRadioUseCustom').attr('disabled', true);
                showSensorsWithoutValuesWarning(result.SensorsWithoutValues);
                customThresholdsDisabled = true;
            }
            SW.HWH.Thresholds.UnitName = result.UnitDescription;
            SW.HWH.Thresholds.UnitHtml = result.UnitHtmlText;

            evaluateStoredThresholds(result.ThresholdModel);
        },
        error: function () {
            showNoItemsError();
        }
    });
});

$('.thresholdSubmitButton').live('click', function () {
    var selectedOption = $('input[name=thresholdRadio]:checked').val();
    if (selectedOption === 'useCustom') {
        if (SW.HWH.Thresholds.Validate()) {
            var warningExpression = SW.HWH.Thresholds.WarningNEB.nested_expression_builder('getExpression');
            var criticalExpression = SW.HWH.Thresholds.CriticalNEB.nested_expression_builder("getExpression");
            $('#progressDiv').show();
            $.ajax({
                type: 'POST',
                dataType: "json",
                contentType: 'application/json;charset=utf-8',
                url: '/sapi/HardwareHealthThresholdManagement/StoreThresholdsForStoredData',
                data: JSON.stringify({
                    Warning: warningExpression,
                    Critical: criticalExpression
                }),
                success: function () {
                    location.href = SW.HWH.returnTo;
                },
                error: function () {
                    $('#progressDiv').hide();
                    Ext42.Msg.show({
                        title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",//Error
                        msg: "@{R=HardwareHealth.Strings;K=HHWEBJS_PS0_4; E=js}",//Unable to save thresholds.
                        icon: Ext42.Msg.ERROR, buttons: Ext42.Msg.OK,
                        fn: function() {
                            location.href = SW.HWH.returnTo;
                        }
                    });
                }
            });
        } else {
            SW.HWH.Thresholds.NotificationWrapper.append(SW.HWH.Thresholds.NotificationValidationFail.show());
        }
    } else {
        $('#progressDiv').show();
        $.ajax({
            type: 'GET',
            dataType: "json",
            contentType: 'application/json;charset=utf-8',
            url: '/sapi/HardwareHealthThresholdManagement/UpdateThresholdOnStoredSensors?thresholdMode=' + selectedOption,
            success: function () {
                location.href = SW.HWH.returnTo;
            },
            error: function () {
                $('#progressDiv').hide();
                Ext42.Msg.show({
                    title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",//Error
                    msg: "@{R=HardwareHealth.Strings;K=HHWEBJS_PS0_4; E=js}",//Unable to save thresholds.
                    icon: Ext42.Msg.ERROR, buttons: Ext42.Msg.OK,
                    fn: function() {
                        location.href = SW.HWH.returnTo;
                    }
                });
                
            }
        });
    }
    return false;
});

$('input[name=thresholdRadio]').live('change', function () {
    var selection = $(this).val();
    if (selection === 'useCustom') {
        $('#thresholdsPlaceholder').show();
        try {
        var warningIsEmpty = SW.HWH.Thresholds.WarningNEB.nested_expression_builder("getExpression") === null;
        var criticalIsEmpty = SW.HWH.Thresholds.CriticalNEB.nested_expression_builder("getExpression") === null;
        if (warningIsEmpty && criticalIsEmpty) {
            SW.HWH.Thresholds.RenderNEBInstances();
        }
        }
        catch (e) {
            SW.HWH.Thresholds.RenderNEBInstances();
        }
    } else {
        $('#thresholdsPlaceholder').hide();
    }
});

function showMultipleSensorsWarning(count) {
    var warningText = String.format('@{R=HardwareHealth.Strings;K=HHWEBJS_GK0_04; E=js}', count);
    $('#multipleItemsWarningPlaceholder').show();
    $('#multipleItemsWarningText').text(warningText);
}

function showIncompatibleUnitsWarning(units) {
    var warningHtml = '@{R=HardwareHealth.Strings;K=HHWEBJS_GK0_05; E=js}';
    warningHtml += '<ul>';
    $(units).each(function (index, unit) {
        warningHtml += String.format('<li>{0} ({1})</li>', unit.DisplayName, unit.Count);
    });
    warningHtml += '</ul>';
    warningHtml += '<p>@{R=HardwareHealth.Strings;K=HHWEBJS_GK0_06; E=js}</p>';

    $('#incompatibleUnitsWarningPlaceholder').show();
    $('#incompatibleWarningText').html(warningHtml);
}

function showSensorsWithoutValuesWarning(sensors) {
    var i;
    var warningHtml = '@{R=HardwareHealth.Strings;K=HHWEBJS_GK0_07; E=js}';
    warningHtml += '<ul>';
    for (i = 0; i < Math.min(sensors.length, 20); i++) {
        warningHtml += String.format('<li>{0}</li>', sensors[i]);
    }
    warningHtml += '</ul>';
    
    $('#sensorsWithoutValuesWarningPlaceholder').show();
    $('#sensorsWithoutValuesWarningList').html(warningHtml);
    $('#sensorsWithoutValuesWarningText').html('<p>@{R=HardwareHealth.Strings;K=HHWEBJS_GK0_08; E=js}</p>');
    
    if (sensors.length > 20) {
        warningHtml = '<ul>';
        for (i = 20; i < sensors.length; i++) {
            warningHtml += String.format('<li>{0}</li>', sensors[i]);
        }
        warningHtml += '</ul>';
        
        $('#moreSensorsWithoutValuesWarningList').html(warningHtml);
        $('#moreSensorsWithoutValuesWarningNote').click(function() {
            $('#moreSensorsWithoutValuesWarningList').show();
            $(this).hide();
        }).show();
    }
}

function showNoItemsError() {
    var errorHtml = '@{R=HardwareHealth.Strings;K=HHWEBJS_GK0_09; E=js}';
    errorHtml += '<p>@{R=HardwareHealth.Strings;K=HHWEBJS_GK0_10; E=js}</p>';

    $('#noItemsErrorText').html(errorHtml);
    $('#noItemsErrorPlaceholder').show();

    $('input[name=thresholdRadio]').attr('disabled', 'disabled');
    $('.thresholdSubmitButton').hide();
}

function evaluateStoredThresholds(thresholdData) {
    if (thresholdData.SelectionType === 'useOrion') {
        $('#thresholdRadioUseOrion').attr('checked', 'checked');
    }
    else if (thresholdData.SelectionType === 'useUp') {
        $('#thresholdRadioUseUp').attr('checked', 'checked');
    }
    else if (thresholdData.SelectionType === 'useCustom') {
        if (customThresholdsDisabled === false) {
            $('#thresholdRadioUseCustom').attr('checked', 'checked');
            if (thresholdData.MatchingThreshold !== null) {
                SW.HWH.Thresholds.RenderNEBInstances(thresholdData.MatchingThreshold);
            } else if (thresholdData.Templates.length != 0) {
                renderTemplatesSelector(thresholdData.Templates);
            }
        }
    }
    else if (thresholdData.SelectionType === 'conflict') {
        $('input[name=thresholdRadio]').removeAttr('checked');

        $('#thresholdsPlaceholder').hide();

        if (customThresholdsDisabled === false) {
            if (thresholdData.MatchingThreshold !== null) {
                SW.HWH.Thresholds.RenderNEBInstances(thresholdData.MatchingThreshold);
            } else if (thresholdData.Templates && _.size(thresholdData.Templates) > 0) {
                renderTemplatesSelector(thresholdData.Templates);
            }
        }
    }
}

function renderTemplatesSelector(templatesData) {
    $('#thresholdTemplateSelectorHolder').html('');
    var infoHtml = '<span class="thresholdRadio">@{R=HardwareHealth.Strings;K=HHWEBJS_GK0_11; E=js}</span> - @{R=HardwareHealth.Strings;K=HHWEBJS_GK0_12; E=js}';

    var dropDown = '<select id="thresholdTemplateSelector">';
    var count = 0;
    for (var template in templatesData) {
        dropDown += "<option value='" + JSON.stringify(templatesData[template]) + "'>" + template + "</option>";
        if (count === 0) {
            SW.HWH.Thresholds.RenderNEBInstances(templatesData[template]);
        }
        count++;
    }

    dropDown += '</select>';
    $('#thresholdTemplateSelectorHolder').append(infoHtml);
    $('#thresholdTemplateSelectorHolder').append(dropDown);
}

$('#thresholdTemplateSelector').live("change", function () {
    var selectedTemplate = $(this).val();
    SW.HWH.Thresholds.RenderNEBInstances(JSON.parse(selectedTemplate));
});