﻿SW.Core.namespace("SW.Core.NestedExpressionBuilder.Input.Field").SensorValue = function (config) {

    // CONSTANTS
    var DEFAULT_CONFIG = {
        enabledWhenEmpty: false,
        dataTypeFilter: 0
    };

    // PUBLIC METHODS
    this.getExpr = function () {
        return {
            Child: null,
            NodeType: 1,
            Value: labelHolder.attr("data-value")
        };
    };

    this.validate = function () {
        return true;
    };

    this.setFilter = function (masterField) {
    };

    // PRIVATE METHODS
    var showItems = function () {
        if (!expr.Value) {
            labelHolder.attr("data-value", options.globalOptions.sensorValueType);
        }
        else {
            labelHolder.attr("data-value", expr.Value);
        }
        setTimeout(propagateSet, 0);
    };

    var propagateSet = function () {
        if (typeof options.onSet === 'function') {
            options.onSet({
                DataTypeInfo: {
                    DeclType: 4
                    // 4 : FLOAT
                    // 5 : TEXT
                }
            });
        }
    };

    var setUnitForRhs = function(options) {
        var unitHolder = $("<span>" + SW.HWH.Thresholds.UnitHtml + "</span>");
        var currentRow = options.renderTo.parent().parent();
        $('.neb-rhs', currentRow).parent().append(unitHolder);
    };

    // CONSTRUCTOR
    var self = this;
    var options = $.extend({}, DEFAULT_CONFIG, config);
    var expr = options.expr;

    var labelHolder = $("<span data-value='' title='" + SW.HWH.Thresholds.UnitName + "'>" + options.globalOptions.sensorValueType + "</span>");
    options.renderTo.append(labelHolder);
    setUnitForRhs(options);
    showItems();

    return this;
};