﻿Ext42.define('MyApp.util.JsonPStorageProvider', {
    /* Begin Definitions */

    extend: 'Ext.state.Provider',
    alias: 'state.jsonpstorage',

    config: {
        userId: null,
        url: "/api/HardwareHealthSensorManagement/",
        timeout: 30000
    },

    constructor: function (config) {
        this.initConfig(config);
        var me = this;
        me.callParent(arguments);
        me.restoreState();

    },
    set: function (name, value) {
        var me = this;
        me.persist(name, value);
        me.callParent(arguments);
    },
    get: function (name) {
        return this.state[name];
    },
    // private
    restoreState: function () {
        var me = this;
        var keys = ['sensorGrid', 'HWHsideBar', 'sensorToolbar', 'HWHappContainer', 'SensorManagement_PageSize', 'DoNotRedirectToDiscoveryWizard'];
        keys.forEach(function (key) {
            Ext42.Ajax.request({
                async: false,
                url: '/orion/services/WebAdmin.asmx/GetUserSetting',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': '*/*'
                },
                params: Ext42.JSON.encode({
                    name: key
                }),
                disableCaching: true,
                success: function (response) {
                    if (!this.state) {
                        this.state = {};
                    }
                    if (response && response.responseText) {
                        var results = JSON.parse(response.responseText);
                        this.state[key] = this.decodeValue(results['d']);
                    }
                },
                scope: me,
            });
        });
    },
    // private
    persist: function (name, value) {
        var me = this;
        Ext42.Ajax.request({
            async: false,
            url: '/orion/services/WebAdmin.asmx/SaveUserSetting',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': '*/*'
            },
            params: Ext42.JSON.encode({
                name: name,
                value: me.encodeValue(value)
            }),
            disableCaching: true,
            success: function () {
                // console.log('success');
            },
            failure: function () {
                console.log('failed', arguments);
            },
            scope: this
        });
    }
});