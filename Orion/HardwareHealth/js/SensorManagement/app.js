﻿/// <reference path="../../../js/extjs/4.2.2/debug/ext-all-sandbox-42-debug.js" />
/// <reference path="../../../js/OrionMinReqs.js/underscore-min.js" />
Ext42.Loader.setConfig({ enabled: true });
Ext42.application({
    name: 'SensorManagement',
    appFolder: 'js/SensorManagement/app',
    requires: [
        'Ext.layout.container.*',
        'Ext.resizer.Splitter',
        'Ext.fx.target.Element',
        'Ext.fx.target.Component',
        'SensorManagement.view.SensorGrid',
        'SensorManagement.view.Sidebar',
        'SensorManagement.view.SelectAllStripe',
        'MyApp.util.JsonPStorageProvider'
    ],

    models: ['Sensor'],
    stores: ['Sensor', 'GroupingOptions', 'GroupingValues'],
    controllers: ['Sensor', 'SensorToolbar', 'Sidebar'],
    listeners: {
        resize: {
            fn: function () {
                this.setMainGridHeight();
                Ext42.getCmp("appContainer").setHeight(Ext42.get("appContent").getHeight());
                Ext42.getCmp("appContainer").doLayout();
            }
        }
    },
    options: {
        ParentNetObject: undefined
    },
    launch: function () {

        ORION.prefix = "SensorManagement_";

        Ext42.state.Manager.setProvider(
            Ext42.create('MyApp.util.JsonPStorageProvider', {
                userId: ['uid'],
                url: ['/api/HardwareHealthSensorManagement/']
            })
        );

        this.loadUrlArguments();

        Ext42.create("Ext.container.Container", {
            renderTo: Ext42.getElementById("appContent"),
            autoCreateViewPort: false,
            layout: 'border',
            id: "appContainer",
            stateId: 'HWHappContainer',
            stateful: true,
            listeners: {
                afterrender: function () {
                    SensorManagement.getApplication().setMainGridHeight();
                    Ext42.getCmp("appContainer").setHeight(Ext42.get("appContent").getHeight());
                    Ext42.getCmp("appContainer").doLayout();

                    Ext42.EventManager.onWindowResize(_.debounce(function () {
                        SensorManagement.getApplication().setMainGridHeight();
                        Ext42.getCmp("appContainer").setHeight(Ext42.get("appContent").getHeight());
                        Ext42.getCmp("appContainer").doLayout();
                    }, 50));
                }
            },
            items: [
                {
                    xtype: "sidebar",
                    region: 'west',
                    width: 200,
                    minWidth: 100,
                    maxWidth: 400,
                    split: true,
                    collapsible: true,
                    collapseMode: 'mini',
                    stateId: 'HWHsideBar',
                    stateful: true
                },
                {
                    xtype: "sensorgrid",
                    region: 'center'
                }
            ],
            padding: '0 10px'
        });
    },
    autoCreateViewport: false,
    setMainGridHeight: function () {
        var contentPanel = $('#appContent');
        var maxHeight = this.calculateMaxGridHeight(contentPanel);
        var height = maxHeight;
        contentPanel.height((Math.max(height, 300)));
    },
    calculateMaxGridHeight: function (contentPanel) {
        var contentPanelTop = contentPanel.offset().top;
        var height = $(window).height() - contentPanelTop - $('#footer').outerHeight() - 23;
        return height;
    },

    loadUrlArguments: function () {
        var urlParams = SW.Core.UriHelper.decodeURIParams();
        if (urlParams.NetObject) {
            this.options.ParentNetObject = urlParams.NetObject;
        } else if (urlParams.NodeId) {
            this.options.ParentNetObject = 'N:' + urlParams.NodeId;
        }
    }
});
