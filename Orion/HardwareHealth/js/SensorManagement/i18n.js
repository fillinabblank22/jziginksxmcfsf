﻿Ext42.define('SolarWinds.i18n.grid.header.Container', {
    override: 'Ext.grid.header.Container',
    sortAscText: "@{R=Core.Strings;K=WEBJS_LH0_01; E=js}",
    sortDescText: "@{R=Core.Strings;K=WEBJS_LH0_02; E=js}",
    sortClearText: "@{R=Core.Strings;K=WEBJS_LH0_03; E=js}",
    columnsText: "@{R=Core.Strings;K=WEBJS_LH0_04; E=js}",
});

Ext42.define('SolarWinds.i18n.ux.grid.FiltersFeature', {
    override: 'Ext.ux.grid.FiltersFeature',
    menuFilterText: "@{R=Core.Strings;K=WEBJS_LH0_05; E=js}",
});


Ext42.define('SolarWinds.i18n.toolbar.Paging', {
    override: 'Ext.toolbar.Paging',
    beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
    afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
    firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
    prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
    nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
    lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
    refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}",
    displayMsg: "@{R=Core.Strings;K=WEBJS_LH0_06; E=js}",
    emptyMsg: "@{R=Core.Strings;K=WEBJS_LH0_07; E=js}",
});


Ext42.define('SolarWinds.i18n.view.View', {
    override: 'Ext.view.View',
    loadingText: '@{R=Core.Strings;K=WEBJS_AK0_2;E=js}'
});

