﻿Ext42.define('SensorManagement.view.GroupingCombobox', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.groupingcombobox',
    displayField: 'Caption',
    valueField: 'Value',
    store: 'GroupingOptions',
    autoSelect: true,
    editable: false,
    mode: 'local',
    value: 'ParentObjectVendor',
    initComponent: function () {
        
        this.callParent();
    }
});