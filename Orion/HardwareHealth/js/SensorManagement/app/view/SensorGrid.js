﻿Ext42.define('SensorManagement.view.SensorGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.sensorgrid',
    requires: [
        'SensorManagement.view.SensorToolbar',
        'SensorManagement.view.PageSizeBox',
        'SensorManagement.view.FilteredColumn'
    ],
    refs: [
        {
            ref: 'searchBox',
            selector: 'sensortoolbar searchbox'
        }
    ],
    layout: 'fit',
    stateId: 'sensorGrid',
    stateful: true,
    cls: 'sensorGrid',
    columns: [
        {
            text: '@{R=HardwareHealth.Strings;K=HHWEBJS_LH0_10; E=js}',
            dataIndex: 'SensorName',
            xtype: 'filteredcolumn',
            flex: 1,
            minWidth: 220,
            hideable: false,
            renderer: function (value, meta, record) {
                return this.renderSensorNameColumn(value, meta, record);
            },
            filter: {
                type: 'string',
                emptyText: '@{R=Core.Strings;K=WEBJS_SO0_46; E=js}',
                allowEmptyValue: false
            }
        },
        {
            text: '@{R=HardwareHealth.Strings;K=HHWEBJS_LH0_9; E=js}',
            dataIndex: 'ParentObjectName',
            xtype: 'filteredcolumn',
            width: 200,
            renderer: function(value, meta, record) {
                return this.renderParentObjectColumn(value, meta, record);
            },
            filter: {
                type: 'string',
                emptyText: '@{R=Core.Strings;K=WEBJS_SO0_46; E=js}',
                allowEmptyValue: false
            }
        },
        {
            text: '@{R=HardwareHealth.Strings;K=HHWEBJS_LH0_6; E=js}',
            dataIndex: 'Category',
            xtype: 'filteredcolumn',
            filter: {
                type: 'list',
                storeName: 'Category',
                phpMode: true,
                getSerialArgs: function () {
                    var args = { type: 'list', value: this.phpMode ? this.getValue().join('$#') : this.getValue() };
                    return args;
                }
            }
        },
        {
            text: '@{R=HardwareHealth.Strings;K=HHWEBJS_LH0_5; E=js}',
            dataIndex: 'ProductLine',
            xtype: 'filteredcolumn',
            renderer: function(value) {
                return this.renderString(value);
            },
            filter: {
                type: 'string',
                emptyText: '@{R=Core.Strings;K=WEBJS_SO0_46; E=js}'
            }
        },
        {
            text: '@{R=HardwareHealth.Strings;K=HHWEBJS_LH0_4; E=js}',
            dataIndex: 'ParentObjectVendor',
            xtype: 'filteredcolumn',
            renderer: function (value, meta, record) {
                return this.renderVendorColumn(value, meta, record);
            },
            filter: {
                type: 'list',
                storeName: 'Vendor',
                phpMode: true,
                getSerialArgs: function () {
                    var args = { type: 'list', value: this.phpMode ? this.getValue().join('$#') : this.getValue() };
                    return args;
                }
            }
        },
        {
            text: '@{R=HardwareHealth.Strings;K=HHWEBJS_LH0_11; E=js}',
            dataIndex: 'LastValue',
            xtype: 'filteredcolumn',
            tpl: '<div class="align right"><tpl if="LastValue">{LastValue} {Unit}</tpl></div>',
            filter: {
                type: 'numeric',
                menuItemCfgs: {
                    emptyText: '@{R=Core.Strings;K=WEBJS_SO0_46; E=js}',
                    decimalSeparator: Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator,
                    allowDecimals: true,
                    allowNegative: true,
                    maxValue: 3.40282347E+38,
                    minValue: -3.40282347E+38,
                    decimalPrecision: 7
                }
            }
        },
        {
            text: '@{R=HardwareHealth.Strings;K=HHWEBJS_LH0_7; E=js}',
            dataIndex: 'LastStatus',
            renderer: function(value, meta, record) {
                return this.lastStatusRenderer(value, meta, record);
            },
            xtype: 'filteredcolumn',
            filter: {
                type: 'list',
                storeName: 'LastStatus',
                phpMode: true,
                getSerialArgs: function () {
                    var args = { type: 'list', value: this.phpMode ? this.getValue().join('$#') : this.getValue() };
                    return args;
                }
            }
        },
        {
            text: '@{R=HardwareHealth.Strings;K=HHWEBJS_GK0_01; E=js}',
            dataIndex: 'HasCustomThreshold',
            xtype: 'filteredcolumn',
            renderer: function(value) {
                return this.yesNoRenderer(value);
            },
            filter: {
                type: 'bool',
                defaultValue: null,
                yesText: '@{R=Core.Strings;K=WEBJS_VB0_10; E=js}',
                noText: '@{R=Core.Strings;K=WEBJS_VB0_11; E=js}'
            }
        },
        {
            text: '@{R=HardwareHealth.Strings;K=HHWEBJS_LH0_8; E=js}',
            dataIndex: 'IsEnabled',
            xtype: 'filteredcolumn',
            width: 80,
            action: 'toggleenabled',
            renderer: function(value, meta, record, row, column, grid, store) {
                var method = (value) ? "disable" : "enable";
                return "<a href='#' onclick='SensorManagement.getApplication().getSensorController()." + method + "(" + record.get("ParentObjectId") + ", \"" + record.get("SensorUniqueName") + "\")' class='toggle" + (value ? "On" : "Off") + "'>" + (value ? "@{R=Core.Strings;K=WEBJS_TM0_144;E=js}" : "@{R=Core.Strings;K=LogLevel_off;E=js}") + "</a>";
            },
            filter: {
                type: 'bool',
                defaultValue: null,
                yesText: '@{R=Core.Strings;K=WEBJS_VB0_10; E=js}',
                noText: '@{R=Core.Strings;K=WEBJS_VB0_11; E=js}'
            }
        }
    ],
    store: 'Sensor',
    selType: 'checkboxmodel',

    features: [{
            ftype: 'filters',
            encode: true,
            local: false
        }],
    dockedItems: [{
            xtype: 'sensortoolbar',
            dock: 'top'
        },
        {
            xtype: 'selectAllStripe',
            hidden: true,
            dock: 'top'
        },
        {
            xtype: 'pagingtoolbar',
            store: 'Sensor',
            dock: 'bottom',
            displayInfo: true,
            items: [
                '-',
                { xtype: 'tbspacer', width: 10 },
                { xtype: 'label', text: '@{R=Core.Strings;K=WEBJS_VB0_46; E=js}' },
                { xtype: 'pagesizebox' }
            ]
        }],
    listeners: {
        beforecellclick: {
            fn: function (table, td, cellIndex, record, tr, rowIndex, e) {

                // register when user clicks to checkbox cell but not on the checkbox...
                var isCheckBoxCellClicked = cellIndex === 0 && e.target.className !== 'x42-grid-row-checker';

                if (isCheckBoxCellClicked) {
                    var sm = table.getSelectionModel();

                    if (sm.isSelected(record)) {
                        sm.deselect([record]);
                    } else {
                        sm.select([record], true);
                    }
                }

                return !isCheckBoxCellClicked;
            },
            scope: this
        }
    },
    lastStatusRenderer: function(value, meta, record) {

        var orionStatus = {
            'unknown': { value: '0', caption: '@{R=HardwareHealth.Strings;K=HHWEBJS_LH0_12; E=js}' },
            'up': { value: '1', caption: '@{R=HardwareHealth.Strings;K=HHWEBJS_LH0_13; E=js}' },
            'warning': { value: '3', caption: '@{R=HardwareHealth.Strings;K=HHWEBJS_LH0_14; E=js}' },
            'critical': { value: '14', caption: '@{R=HardwareHealth.Strings;K=HHWEBJS_LH0_15; E=js}' },
            'undefined': { value: '17', caption: '@{R=HardwareHealth.Strings;K=HHWEBJS_LH0_23;E=js}' },
            'disabled': { value: '27', caption: '@{R=HardwareHealth.Strings;K=HHWEBJS_LH0_16; E=js}' }
        };

        var finalStatus = _.find(orionStatus, function(status) { return status.value === value })
        if (!finalStatus) {
            finalStatus = orionStatus.unknown;
        }

        return finalStatus.caption;
    },
    yesNoRenderer: function(value) {
        if (value) {
            return "@{R=Core.Strings;K=WEBJS_VB0_10; E=js}";
        } else {
            return "@{R=Core.Strings;K=WEBJS_VB0_11; E=js}";
        }
    },
    renderParentObjectColumn: function(value, meta, record) {
        var data = Ext42.apply({}, record.data, record.getAssociatedData());
        data['ParentObjectName'] = this.renderString(value);
        var tpl = new Ext42.XTemplate('<a href="/Orion/View.aspx?NetObject={ParentObjectType}:{ParentObjectId}">' +
            '<img NetObject="{ParentObjectType}:{ParentObjectId}" class="StatusIcon icon" src="/Orion/StatusIcon.ashx?entity={ParentObjectEntity}&status={ParentObjectStatus}&size=small&id={ParentObjectId}">' +
            '<span>{ParentObjectName}</span></a>');
        return tpl.apply(data);
    },
    renderSensorNameColumn: function(value, meta, record) {
        var tpl = new Ext42.XTemplate('<img class="StatusIcon sensor-icon" src="{SensorIcon}"><span>' + this.renderString(value) + '</span>');
        return tpl.apply(record.data);
    },
    renderVendorColumn: function (value, meta, record) {
        var iconSrc = record.data["ParentObjectVendorIcon"] ? "{ParentObjectVendorIcon}" : "/NetPerfMon/images/Vendors/Unknown.gif";
        var parentVendorIcon = '<img class="icon" src="' + iconSrc + '">';
        var tpl = new Ext42.XTemplate(parentVendorIcon + this.renderString(value));
        return tpl.apply(record.data);
    },
    renderString: function(value) {
        if (!value || value.length === 0)
            return value;

        var filterText = this.searchText;
        if (!filterText || filterText.length === 0)
            return this.encodeHTML(value);

        var patternText = filterText;

        // replace any %'s with a *
        patternText = patternText.replace(/\%/g, "*");

        // replace multiple *'s with a single instance
        patternText = patternText.replace(/\*{2,}/g, "*");

        // check if the search string is now just down to a single *, and if so return without any further regex
        if (patternText == '*') {
            return '<span style=\"background-color: #FFE89E\">' + this.encodeHTML(value) + '</span>';
        }

        // replace \ with \\
        patternText = patternText.replace(/\\/g, '\\\\');
        // replace . with \.
        patternText = patternText.replace(/\./g, '\\.');
        // replace * with .*
        patternText = patternText.replace(/\*/g, '.*');

        // set regex pattern
        var x = '((' + patternText + ')+)\*';
        var content = value, pattern = new RegExp(x, "gi"), replaceWith = '{SPAN-START-MARKER}$1{SPAN-END-MARKER}';

        // do regex replace + content gets encoded
        var fieldValue = this.encodeHTML(content.replace(pattern, replaceWith));

        // now replace the literal SPAN markers with the HTML span tags
        fieldValue = fieldValue.replace(/{SPAN-START-MARKER}/g, '<span style=\"background-color: #FFE89E\">');
        fieldValue = fieldValue.replace(/{SPAN-END-MARKER}/g, '</span>');

        return fieldValue;
    },
    encodeHTML: function(value) {
        return Ext42.util.Format.htmlEncode(value);
    },
    searchText: '',
    columnMapping: new Object()
});

