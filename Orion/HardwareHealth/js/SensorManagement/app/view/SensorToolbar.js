﻿Ext42.define('SensorManagement.view.SensorToolbar', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.sensortoolbar',
    stateId: 'sensorToolbar',
    stateful: true,
    requires: [
        'SensorManagement.view.SearchBox'
    ],
    items: [
        {
            icon: '/Orion/HardwareHealth/images/Enable.gif',
            text: "@{R=HardwareHealth.Strings;K=HHWEBJS_LH0_17; E=js}",
            id: 'enableSensorsButton'
        },
        '-',
        {
            icon: '/Orion/HardwareHealth/images/Disable.png',
            text: "@{R=HardwareHealth.Strings;K=HHWEBJS_LH0_18; E=js}",
            id: 'disableSensorsButton'
        },
        {
            icon: '/Orion/images/edit_16x16.gif',
            text: "@{R=HardwareHealth.Strings;K=HHWEBJS_GK0_02; E=js}",
            id: 'editThresholdsButton'
        },
        '->',
        {
            xtype: 'searchbox',
            name: 'search'
        }
    ],
    initComponent: function () {
        this.callParent();
    }

});