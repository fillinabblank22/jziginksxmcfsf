﻿Ext42.define('SensorManagement.view.SelectAllStripe', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.selectAllStripe',
    cls: 'select-whole-page',
    items: [
        {
            text: "@{R=HardwareHealth.Strings;K=HHWEBJS_PS0_1; E=js}",
            id: "selectAllSensorsCaption"
        },
        '-',
        {
            text: "@{R=HardwareHealth.Strings;K=HHWEBJS_PS0_2; E=js}",
            id: 'selectAllSensorsButton'
        }
    ],
    initComponent: function () {
        this.callParent();
    },
    setSelectionInformation: function(pageSize, sensorCount) {
        this.items.getByKey('selectAllSensorsCaption').update(String.format('@{R=HardwareHealth.Strings;K=HHWEBJS_PS0_1; E=js}', pageSize));
        this.items.getByKey('selectAllSensorsButton').update(String.format('@{R=HardwareHealth.Strings;K=HHWEBJS_PS0_2; E=js}', sensorCount));
    }
});