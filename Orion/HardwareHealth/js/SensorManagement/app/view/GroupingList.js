﻿Ext42.define('SensorManagement.view.GroupingList', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.groupinglist',
    requires: [
        
    ],
    layout: 'fit',
    columns: [
        { text: 'Value', dataIndex: 'DisplayName', flex:1, xtype:'templatecolumn', tpl: '{DisplayName} ({Count})' }
    ],
    store: 'GroupingValues',
    hideHeaders: true,
    frame: false,

    initComponent: function () {
        this.callParent();
    }

});