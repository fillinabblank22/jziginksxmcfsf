﻿Ext42.define('SensorManagement.view.Sidebar', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.sidebar',
    requires: [
        'SensorManagement.view.GroupingCombobox',
        'SensorManagement.view.GroupingList'
    ],
    layout: 'fit',
    title: "@{R=HardwareHealth.Strings;K=HHWEBJS_LH0_20; E=js}",
    items: [
        {
            xtype: 'container',
            layout: {
                type: 'vbox',
                align : 'stretch',
                pack  : 'start'
            },
            cls: 'sidebar',
            items: [
                { xtype: 'groupingcombobox', margin: 10 },
                { xtype: 'groupinglist', flex: 1 }
            ]
        }


    ],

    initComponent: function () {
        this.callParent();
    }
});