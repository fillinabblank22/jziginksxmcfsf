﻿Ext42.define('SensorManagement.view.FilteredColumn', {
    extend: 'Ext.grid.column.Template',
    alias: 'widget.filteredcolumn',
    renderTpl: [
                '<div id="{id}-titleEl" role="presentation" {tipMarkup}class="', Ext42.baseCSSPrefix, 'column-header-inner',
                    '<tpl if="empty"> ', Ext42.baseCSSPrefix, 'column-header-inner-empty</tpl>">',
                    '<img class="filter-icon" onclick="SensorManagement.getApplication().getSensorController().showFilter(event, \'{dataIndex}\');" src="/Orion/js/extjs/resources/images/default/s.gif" alt="" />',
                    '<span id="{id}-textEl" class="', Ext42.baseCSSPrefix, 'column-header-text',
                        '{childElCls}">',
                        '{text}',
                    '</span>',
                    '<tpl if="!menuDisabled">',
                        '<div id="{id}-triggerEl" role="presentation" class="', Ext42.baseCSSPrefix, 'column-header-trigger',
                        '{childElCls}"></div>',
                    '</tpl>',
                '</div>',
                '{%this.renderContainer(out,values)%}'
    ],
    initRenderData: function () {
        var me = this;
        return Ext42.applyIf(me.callParent(arguments), {
            dataIndex: me.dataIndex
        });
    },
    initComponent: function () {
        if (!this.tpl) {
            this.tpl = '{' + this.dataIndex + '}';
        }
        this.callParent(arguments);
    },
});