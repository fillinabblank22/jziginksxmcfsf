﻿Ext42.define('SensorManagement.controller.Sidebar', {
    extend: 'Ext.app.Controller',

    stores: ['GroupingValues', 'Sensor'],
    refs: [
        {
            ref: 'combobox',
            selector: 'sidebar groupingcombobox'
        },
        {
            ref: 'list',
            selector: 'sidebar groupinglist'
        }
    ],

    init: function () {

        var store = this.getGroupingValuesStore();
        store.on({ "beforeload": { fn: this.onBeforeStoreLoad, scope: this } });

        this.control({
            'sidebar groupingcombobox': {
                change: this.onGroupingOptionChange, scope: this
            }
        });

        this.control({
            'sidebar groupinglist': {
                selectionchange: this.onGroupingValueChange, scope: this
            }
        });


        this.callParent();
    },

    onLaunch: function () {
        var store = this.getGroupingValuesStore();
        if (!this.application.options.ParentNetObject) {
            store.load();
        }
    },

    onBeforeStoreLoad: function () {
        var combobox = this.getCombobox();
        var groupByProperty = combobox.getValue();
        var groupingValuesStore = this.getGroupingValuesStore();

        if (groupByProperty) {
            Ext42.apply(groupingValuesStore.getProxy().extraParams, { 'selectedGroupByValue': groupByProperty });
        }

    },

    onGroupingOptionChange: function (sender, newValue, oldValue) {

        if (newValue) {
            var store = this.getGroupingValuesStore();
            store.load();
        } else {
            var groupingValuesStore = this.getGroupingValuesStore();
            groupingValuesStore.removeAll();

            var store = this.getSensorStore();
            store.loadPage(1);
        }

    },

    onGroupingValueChange: function (sender, selected) {

        if (selected.length > 0) {
            var store = this.getSensorStore();
            store.loadPage(1);
        }
    }

});
