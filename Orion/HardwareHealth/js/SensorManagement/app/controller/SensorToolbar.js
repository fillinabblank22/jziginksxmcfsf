﻿Ext42.define('SensorManagement.controller.SensorToolbar', function () {
    var areAllSensorsSelected = false;
    var postedSearchText = '';
    var postedFilters;
    var postedGroupByProperty;
    var postedGroupByValue;

    /**
     * Collects selected items from grid and passes them to action Url. Returns AJAX request.
     * 
     * @param {string} actionUrl The URL of web method which should be performed.
     */
    function enableDisableSensors(actionUrl) {
        var sensorGrid = this.getSensorGrid();

        if (areAllSensorsSelected) {
            actionUrl += 'Bulk';
            var queryString = "?";

            if (postedGroupByProperty && typeof (postedGroupByValue) === "string") {
            } else {
                postedGroupByProperty = null;
                postedGroupByValue = null;
            }

            queryString += 'groupByProperty=' + postedGroupByProperty;
            queryString += '&groupByValue=' + postedGroupByValue;
            queryString += '&filter=' + postedFilters;
            queryString += '&search=' + postedSearchText;

            return $.ajax({
                type: "POST",
                dataType: "json",
                contentType: 'application/json;charset=utf-8',
                url: actionUrl + queryString
            });

        } else {
            var selectedItems = sensorGrid.getSelectionModel().getSelection();

            var data = _.map(selectedItems, function (value, key, list) {
                return {
                    'HardwareInfoID': value.get('HardwareInfoID'),
                    'UniqueName': value.get('SensorUniqueName'),
                    'HardwareCategoryStatusId': value.get('SensorCategoryStatusId')
                };
            });

            return $.ajax({
                type: "POST",
                dataType: "json",
                contentType: 'application/json;charset=utf-8',
                url: actionUrl,
                data: JSON.stringify(data)
            });
        }
    }

    /*
     * Refreshes the grouping list and/or sensor grid
     */
    function refreshData(controller) {
        // get the 1st selected item (if there is any)
        var selectedGroupingValueItem = _.first(controller.getGroupingList().getSelectionModel().getSelection());
        var selectedGroupingValue = null;
        if (selectedGroupingValueItem) {
            // get the selected item's value
            selectedGroupingValue = selectedGroupingValueItem.get('Name');
        } else if (controller.getGroupingValuesStore().count() === 1) {
            // when there is just one value we make it selected by default
            selectedGroupingValue = controller.getGroupingValuesStore().getAt(0).get('Name');
        }
        // store the grouping settings
        controller.previousGrouping = {
            'Property': controller.getGroupingCombobox().getValue(),
            'Value': selectedGroupingValue
        }

        // we need to refresh the grouping list only for 'enabled' and 'laststatus' group by option
        if (_.contains(['enabled', 'laststatus'], controller.previousGrouping.Property)) {
            controller.getGroupingValuesStore().reload({
                callback: function () {
                    this.onStoreReload()
                },
                scope: controller
            });
        } else {
            controller.getSensorStore().reload();
        }
    }

    function storeSelection() {
        if (areAllSensorsSelected) {
            var queryString = '?';
            queryString += 'groupByProperty=' + postedGroupByProperty;
            queryString += '&groupByValue=' + postedGroupByValue;
            queryString += '&filter=' + postedFilters;
            return $.ajax({
                type: 'POST',
                dataType: "json",
                contentType: 'application/json;charset=utf-8',
                url: '/sapi/HardwareHealthThresholdManagement/StoreSensorsByFilters' + queryString
            });

        } else {
            var sensorGrid = this.getSensorGrid();
            var selectedItems = sensorGrid.getSelectionModel().getSelection();

            var data = _.map(selectedItems, function (value, key, list) {
                return {
                    'HardwareInfoID': value.get('HardwareInfoID'),
                    'UniqueName': value.get('SensorUniqueName'),
                    'HardwareCategoryStatusId': value.get('SensorCategoryStatusId'),
                    'UnitIdentifier': value.get('UnitIdentifier'),
                    'LastValue': value.get('LastValue'),
                    'SensorName': value.get('SensorName'),
                    'SensorId': value.get('SensorId')
                };
            });

            return $.ajax({
                type: 'POST',
                dataType: "json",
                contentType: 'application/json;charset=utf-8',
                url: '/sapi/HardwareHealthThresholdManagement/StoreSensorsByKeys',
                data: JSON.stringify(data)
            });
        }
    }
    
    function storeCurrentPage() {
        var store = this.getSensorGrid().getStore();
        var currentPage = store.currentPage;
        if (isNaN(currentPage) || currentPage === 1) {
            return;
        }
        document.cookie = 'hwhSensorManagementCurrentPage=' + currentPage;
    }

    return {
        extend: 'Ext42.app.Controller',
        stores: ['Sensor', 'GroupingValues'],
        refs: [
            {
                ref: 'enableSensorsButton',
                selector: 'sensortoolbar > #enableSensorsButton'
            },
            {
                ref: 'disableSensorsButton',
                selector: 'sensortoolbar > #disableSensorsButton'
            },
            {
                ref: 'editThresholdsButton',
                selector: 'sensortoolbar > #editThresholdsButton'
            },
            {
                ref: 'sensorGrid',
                selector: 'sensorgrid'
            },
            {
                ref: 'groupingCombobox',
                selector: 'sidebar groupingcombobox'
            },
            {
                ref: 'groupingList',
                selector: 'sidebar groupinglist'
            }],

        init: function () {

            this.control({
                'sensortoolbar > #enableSensorsButton': {
                    click: this.onEnableSensors, scope: this
                }
            });

            this.control({
                'sensortoolbar > #disableSensorsButton': {
                    click: this.onDisableSensors, scope: this
                }
            });

            this.control({
                'sensortoolbar > #editThresholdsButton': {
                    click: this.onEditThresholds, scope: this
                }
            });

            this.control({
                'sensorgrid': {
                    selectionchange: this.onSensorGridSelectionChange, scope: this
                }
            });

            this.callParent();
        },

        onLaunch: function () {
            this.setupButtonsState(true);
        },

        onEditThresholds: function () {
            var view = this.getSensorGrid().getView();
            view.mask('@{R=Core.Strings;K=WEBJS_AK0_2;E=js}', 'x-mask-loading');
            storeCurrentPage.call(this);
            var request = storeSelection.call(this);
            
            request.done(function() {
                window.location.href = '/Orion/HardwareHealth/ThresholdManagement.aspx';
            });
            request.fail(function (data) {
                view.unmask();
                Ext42.Msg.show({
                    title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",//Error
                    msg: "@{R=HardwareHealth.Strings;K=HHWEBJS_PS0_3; E=js}",//Unable to navigate to Edit Threshold page, see weblog for details...
                    icon: Ext42.Msg.ERROR, buttons: Ext42.Msg.OK
                });
            });
        },

        onEnableSensors: function () {
            var me = this;
            var view = me.getSensorGrid().getView();
            view.mask('@{R=Core.Strings;K=WEBJS_AK0_2;E=js}', 'x-mask-loading');
            var request = enableDisableSensors.call(this, '/api/HardwareHealthSensorManagement/EnableSensors');

            request.done(function (data) {
                view.unmask();
                refreshData(me);
            });

            request.fail(function (data) {
                view.unmask();
                Ext42.Msg.show({
                    title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",//Error
                    msg: "@{R=HardwareHealth.Strings;K=HHWEBJS_LH0_1; E=js}",//Sensors enabling failed
                    icon: Ext42.Msg.ERROR, buttons: Ext42.Msg.OK
                });
            });
        },

        onDisableSensors: function () {
            var me = this;
            var view = me.getSensorGrid().getView();
            view.mask('@{R=Core.Strings;K=WEBJS_AK0_2;E=js}', 'x-mask-loading');
            var request = enableDisableSensors.call(this, '/api/HardwareHealthSensorManagement/DisableSensors');

            request.done(function (data) {
                view.unmask();
                refreshData(me);
            });

            request.fail(function (data) {
                view.unmask();
                Ext42.Msg.show({
                    title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",//Error
                    msg: "@{R=HardwareHealth.Strings;K=HHWEBJS_LH0_2; E=js}",//Sensors disabling failed
                    icon: Ext42.Msg.ERROR, buttons: Ext42.Msg.OK
                });
            });
        },

        onSensorGridSelectionChange: function (sender, selected) {
            var isEmptySelection = (selected && selected.length > 0) === false;
            this.setupButtonsState(isEmptySelection);


        },
        onStoreReload: function () {
            // try to restore previously selected value in grouping list or select 1st one if not found in the list
            if (this.previousGrouping && this.previousGrouping.Property === this.getGroupingCombobox().getValue() && this.previousGrouping.Value) {
                var recordIndex = this.getGroupingValuesStore().findExact('Name', this.previousGrouping.Value);
                var selectionModel = this.getGroupingList().getSelectionModel();
                recordIndex = (recordIndex === -1) ? 0 : recordIndex;
                selectionModel.select(recordIndex, false, false);
            } else {
                this.getSensorStore().reload();
            }
        },
        setupButtonsState: function (isDisabled) {
            this.getEnableSensorsButton().setDisabled(isDisabled);
            this.getDisableSensorsButton().setDisabled(isDisabled);
            this.getEditThresholdsButton().setDisabled(isDisabled);
        },
        setAllItemsAreSelected: function (shouldSelectAllItems, groupByProperty, groupByValue, filters, searchText) {
            areAllSensorsSelected = shouldSelectAllItems;
            postedFilters = filters;
            postedGroupByProperty = groupByProperty;
            postedGroupByValue = groupByValue;
            postedSearchText = searchText;
        }
    };
});
