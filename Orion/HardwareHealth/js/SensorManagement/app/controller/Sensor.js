﻿Ext42.define('SensorManagement.controller.Sensor', {
    extend: 'Ext.app.Controller',
    stores: ['GroupingValues', 'Sensor', 'Vendor', 'Category','LastStatus'],
    controllers: ['SensorToolbar'],
    refs: [
        {
            ref: 'combobox',
            selector: 'sidebar groupingcombobox'
        },
        {
            ref: 'groupingList',
            selector: 'sidebar groupinglist'
        },
        {
            ref: 'pagingToolbar',
            selector: 'sensorgrid pagingtoolbar'
        },
        {
            ref: 'pageSizeBox',
            selector: 'sensorgrid pagingtoolbar pagesizebox'
        },
        {
            ref: 'sensorGrid',
            selector: 'sensorgrid'
        },
        {
            ref: 'searchBox',
            selector: 'sensorgrid sensortoolbar searchbox'
        },
		{
		    ref: 'selectAllStripe',
		    selector: 'sensorgrid selectAllStripe'
		}
    ],

    init: function () {
        var store = this.getSensorStore();
        store.on({
            "beforeload": { fn: this.onBeforeStoreLoad, scope: this },
        });
        store.on({
            "load": { fn: this.onAfterStoreLoad, scope: this }
        });

        this.control({
            'sensorgrid pagesizebox': {
                'change': _.debounce(this.onPageSizeBoxValueChange, 300)
            }
        });
        this.control({
            'sensorgrid': {
                'selectionchange': this.onSelectionChanged
            }
        });
        this.control({
            '#selectAllSensorsButton': {
                click: this.onAllSensorsSelected
            }
        });
    },

    getPageSize: function () {

        return this.getSensorStore().pageSize;
    },
    setPageSize: function (value) {
        if (value) {
            this.getSensorStore().pageSize = value;
            ORION.Prefs.save('PageSize', value);
        }
    },

    isSelectAllAvailable: function (){
        var selectedSensorsLimit = parseInt(ORION.Prefs.load("SelectedSensorsLimit"));       
        var pageSize = this.getPageSize();
        var selected = this.getSensorGrid().getSelectionModel().getCount();
        var total = this.getSensorStore().totalCount;

        if(pageSize == selected){
            return total <= selectedSensorsLimit;
        } 

        return false;
    },

    onLaunch: function () {

        var store = this.getSensorStore();
        store.pageSize = ORION.Prefs.load('PageSize');
        this.getPageSizeBox().setRawValue(store.pageSize);
        store.load();

        var grid = this.getSensorGrid();
        grid.on({
            "filterupdate": { fn: this.filterUpdate, scope: this }
        });
        $.each(grid.columns, function (index, elem) {
            if (elem.filter && elem.filter.storeName) {
                elem.filter.store = Ext42.getStore(elem.filter.storeName);
            }
            grid.columnMapping[elem.dataIndex] = elem.text;
        });
        // hack to load filters because in ExtJS 4.2 the load is lazy and only after column menu is about to show
        grid.filters.createFilters();

        var searchbox = this.getSearchBox();
        searchbox.on({
            "searchEvent": { fn: this.onSearchEvent, scope: this },
        });
        if (this.application.options.ParentNetObject) {
            var combo = this.getCombobox();
            combo.select('ParentObject');
            this.getGroupingValuesStore().on({
                "load": { fn: this.parentObjectGroupByLoad , scope: this}
            });
        }
    },
    onBeforeStoreLoad: function (store, operation) {
        var combobox = this.getCombobox();
        var groupingList = this.getGroupingList();
        var groupingListSelection = groupingList.getSelectionModel().getSelection();
        var groupByProperty = combobox.getValue();

        var filtersFeature = this.getSensorGrid().filters;
        var filters = filtersFeature.buildQuery(filtersFeature.getFilterData());

        if (groupingListSelection.length > 0) {
            var selectedRow = _.first(groupingListSelection);
            var groupByValue = selectedRow.get('Name');

            if (groupByProperty && typeof (groupByValue) === "string") {
                Ext42.apply(store.getProxy().extraParams, { 'groupByProperty': groupByProperty, 'groupByValue': groupByValue, 'filter': filters });
            }
        } else {
            Ext42.apply(store.getProxy().extraParams, { 'groupByProperty': null, 'groupByValue': null, 'filter': filters });
        }
    },
    onAfterStoreLoad: function (store) {
        if (document.cookie.length > 0) {
            var cookieStartIndex = document.cookie.indexOf('hwhSensorManagementCurrentPage=');
            if (cookieStartIndex != -1) {

                var cookieEndIndex = document.cookie.indexOf(';', cookieStartIndex);
                if (cookieEndIndex == -1) {
                    cookieEndIndex = document.cookie.length;
                }
                var cookie = unescape(document.cookie.substring(cookieStartIndex, cookieEndIndex));
                var index = cookie.split('=')[1];
                if (isNaN(index) == false) {
                    store.loadPage(parseInt(index));
                    document.cookie = 'hwhSensorManagementCurrentPage' + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                }
            }
        }
    },
    onSearchEvent: function (searchText) {
        var stores = [this.getSensorStore(), this.getGroupingValuesStore()];
        $.each(stores, function (index, element) {
            Ext42.apply(element.getProxy().extraParams, { 'search': searchText });
            element.loadPage(1);
        });
        this.getSensorGrid().searchText = this.getSearchBox().getValue();
    },
    onPageSizeBoxValueChange: function (sender, newValue, oldValue) {
        var pSize = parseInt(newValue);
        if (isNaN(pSize) || pSize < 1 || pSize > 100) {
            Ext.Msg.show({
                title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",//Error
                msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",//Invalid page size
                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
            });
            return;
        }

        var pagingToolbar = this.getPagingToolbar();

        if (pagingToolbar.pageSize != pSize) { // update page size only if it is different
            this.setPageSize(pSize);
            pagingToolbar.moveFirst();
        }
    },
    onSelectionChanged: function () {
        var c = this.getSensorToolbarController();
        c.setAllItemsAreSelected(false);
        var pageSize = this.getPageSize();
        var selectionCount = this.getSensorGrid().getSelectionModel().getCount();
        if (pageSize == selectionCount || selectionCount > pageSize) {

            // don't show Select All stripe when the selection would go beyond the limit of allowed selected sensors
            if (this.isSelectAllAvailable()) {

                var totalCount = this.getSensorGrid().getSelectionModel().getStore().getTotalCount();
                this.getSelectAllStripe().setSelectionInformation(pageSize, totalCount);
                this.getSelectAllStripe().show();
            } else {
                this.getSelectAllStripe().hide();
            }
        } else {
            this.getSelectAllStripe().hide();
        }
    },
    onAllSensorsSelected: function () {
        var combobox = this.getCombobox();
        var groupingList = this.getGroupingList();
        var groupingListSelection = groupingList.getSelectionModel().getSelection();
        var groupByProperty = combobox.getValue();
        var groupByValue = '';

        var sensoreGrid = this.getSensorGrid();
        var searchText = sensoreGrid.searchText;
        var filtersFeature = sensoreGrid.filters;

        var filters = filtersFeature.buildQuery(filtersFeature.getFilterData());
        if (filters && filters.filter) {
            filters = filters.filter;
        } else {
            filters = null;
        }

        if (groupingListSelection.length > 0) {
            var selectedRow = _.first(groupingListSelection);
            groupByValue = selectedRow.get('Name');
        } else {
            groupByProperty = null;
        }

        var c = this.getSensorToolbarController();
        c.setAllItemsAreSelected(true, groupByProperty, groupByValue, filters, searchText);
        this.getSelectAllStripe().hide();
    },
    disable: function (nodeId, sensorUniqueName) {
        var store = this.getSensorStore();
        var comparer = function (record, id) {
            var id = record.get('ParentObjectId');
            var uniqueName = record.get('SensorUniqueName');

            return id === nodeId && uniqueName === sensorUniqueName;
        };

        var index = store.findBy(comparer);
        var record = store.getAt(index);
        var grid = this.getSensorGrid();
        grid.getSelectionModel().select([record]);

        var c = this.getSensorToolbarController();
        c.onDisableSensors();
    },
    enable: function (nodeId, sensorUniqueName) {
        var store = this.getSensorStore();
        var comparer = function (record, id) {
            var id = record.get('ParentObjectId');
            var uniqueName = record.get('SensorUniqueName');

            return id === nodeId && uniqueName === sensorUniqueName;
        };

        var index = store.findBy(comparer);
        var record = store.getAt(index);
        var grid = this.getSensorGrid();
        grid.getSelectionModel().select([record]);

        var c = this.getSensorToolbarController();
        c.onEnableSensors();
    },
    showFilter: function (evt, name) {
        evt = (evt) ? evt : window.event;
        evt.cancelBubble = true;

        var grid = this.getSensorGrid();
        for (var i = 0; i < grid.filters.filters.items.length; i++) {
            if (grid.filters.filters.items[i].dataIndex == name) {
                var menu = grid.filters.filters.items[i].menu;
                var event = Ext42.EventObject;
                menu.showAt(event.getXY());
            }
        }
        return false;
    },
    filterUpdate: function (filters, filter) {
        var mapping = this.getSensorGrid().columnMapping;
        var filterData = filters.getFilterData();
        var notificationTemplate = '\
        <div style="position: static;" runat="server"> \
            <div class="sw-filter"> \
                <table cellpadding="0" cellspacing="0"> \
                    <tr> \
                        <td><span class="sw-filter-icon sw-filter-icon-hint"></span></td> \
                        <td><span style="vertical-align: middle; padding-right: 10px">{0}</span></td> \
                        <td><span><img id="deleteButton" src="/Orion/Nodes/images/icons/icon_delete.gif" style="cursor: pointer;" onclick="SensorManagement.getApplication().getSensorController().removeFilter(\'{1}\',\'{2}\')" /></td> \
                    </tr> \
                </table> \
            </div> \
        </div>';

        $("#filterNotifications").each(function () {
            $(this).empty();

            for (var i = 0; i < filterData.length; i++) {
                var displayName = mapping[filterData[i].field] || filterData[i].field;
                var comparison = (filterData[i].data.comparison == undefined) ? "" : filterData[i].data.comparison;

                var relatedFilter = _.find(filters.getFilterItems(), function (item) { return item.dataIndex == filterData[i].field; })



                var notification = $(String.format(notificationTemplate, getFilterNotificationMessage(filterData[i], displayName, relatedFilter), filterData[i].field, comparison));
                $(this).append(notification);
            }
        });

        this.getApplication().fireEvent('resize');

        function getFilterNotificationMessage(filter, displayName, relatedFilter) {
            if (Ext42.isEmpty(filter.data.value)) {
                return String.format('@{R=Core.Strings;K=WEBJS_RB0_2; E=js}', displayName);
            }
            if (filter.data.type == 'numeric') {
                var floatVal = String(filter.data.value).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator);
                if (filter.data.comparison == 'lt')
                    return String.format('@{R=Core.Strings;K=WEBJS_IB0_16; E=js}', displayName, floatVal);
                if (filter.data.comparison == 'gt')
                    return String.format('@{R=Core.Strings;K=WEBJS_IB0_15; E=js}', displayName, floatVal);
                return String.format('@{R=Core.Strings;K=WEBJS_IB0_17; E=js}', displayName, floatVal);
            }
            if (filter.data.type == 'boolean') {
                return String.format('@{R=Core.Strings;K=WEBJS_IB0_14; E=js}', displayName, (filter.data.value ? '@{R=Core.Strings;K=Boolean_true; E=js}' : '@{R=Core.Strings;K=Boolean_false; E=js}'));
            }
            if (filter.data.type.toString().startsWith('list')) {

                if (relatedFilter && relatedFilter.store) {
                    var values = filter.data.value.split('$#');
                    var displayNames = _.map(values, function (value) {
                        var record = relatedFilter.store.findRecord("id", value);

                        if (record) {
                            return record.get("text");
                        } else {
                            return value;
                        }
                    });

                    // return corresponding text values
                    return String.format('@{R=Core.Strings;K=WEBJS_IB0_14; E=js}', displayName, Ext42.util.Format.htmlEncode(displayNames.join("; ")));
                }

                // when store is not available return values as they are
                return String.format('@{R=Core.Strings;K=WEBJS_IB0_14; E=js}', displayName, Ext42.util.Format.htmlEncode(filter.data.value.split('$#').join("; ")));
            }

            return String.format('@{R=Core.Strings;K=WEBJS_IB0_14; E=js}', displayName, Ext42.util.Format.htmlEncode(filter.data.value));
        }
    },
    removeFilter: function (name, comparison) {
        var grid = this.getSensorGrid();
        for (var i = 0; i < grid.filters.filters.items.length; i++) {
            var filterItem = grid.filters.filters.items[i];
            if (filterItem.active && filterItem.dataIndex == name) {
                // for one numeric column we're able to define several filters (less then, greater then etc.)
                // so we need to clear filter with exact the same comparison
                switch (filterItem.type) {
                    case "numeric":
                        var arr = filterItem.getValue();
                        arr[comparison] = null;
                        filterItem.setValue(arr);
                        break;
                    default:
                        filterItem.setValue(null);
                        filterItem.setActive(false);
                        break;
                }
            }
        }
    },
    parentObjectGroupByLoad: function(store) {
        store.un('load', this.parentObjectGroupByLoad, this);
        var listStore = this.getGroupingList().getStore();
        var listIndex = listStore.find('Name', this.application.options.ParentNetObject);
        if (listIndex > -1) {
            this.getGroupingList().getSelectionModel().select(listIndex);
        }
        
    }
});
