﻿Ext42.define('SensorManagement.model.Sensor', {
    extend: 'Ext.data.Model',
    fields: [
        'SensorName',
        'Category',
        'LastValue',
        'LastStatus',
        'ProductLine',
        'ParentObjectVendor',
        'IsEnabled',
        'Unit',
        'UnitIdentifier',
        'ParentObjectName',
        'SensorUniqueName',
        'SensorCategoryStatusId',
        'ParentObjectId',
        'ParentObjectType',
        'ParentObjectEntity',
        'ParentObjectStatus',
        'ParentObjectVendorIcon',
        'HasCustomThreshold',
        'SensorId',
        'SensorIcon',
        'HardwareInfoID'
    ]
});
