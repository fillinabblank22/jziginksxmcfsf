﻿Ext42.define('SensorManagement.store.Vendor', {
    extend: 'Ext.data.Store',
    fields: ['id', 'text'],

    proxy: {
        type: 'ajax',
        url: '/api/HardwareHealthSensorManagement/GetVendors',
        headers: {
            'content-type': 'application/json',
            'accept': 'application/json'
        },
        reader: {
            type: 'json'

        }
    }
});