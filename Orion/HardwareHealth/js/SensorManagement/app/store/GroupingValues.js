﻿Ext42.define('SensorManagement.store.GroupingValues', {
    extend: 'Ext.data.Store',
    
    fields: ['Name', 'DisplayName', 'Count'],

    proxy: {
        type: 'ajax',
        url: '/api/HardwareHealthSensorManagement/GetGroupByValues',
        headers: {
            'content-type': 'application/json',
            'accept': 'application/json'
        },
        reader: {
            type: 'json'
            
        }
    }
});