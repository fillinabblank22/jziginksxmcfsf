﻿Ext42.define('SensorManagement.store.Category', {
    extend: 'Ext.data.Store',
    fields: ['id','text'],

    proxy: {
        type: 'ajax',
        url: '/api/HardwareHealthSensorManagement/GetCategories',
        headers: {
            'content-type': 'application/json',
            'accept': 'application/json'
        },
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});