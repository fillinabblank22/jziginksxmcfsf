﻿Ext42.define('SensorManagement.store.LastStatus', {
    extend: 'Ext.data.Store',
    fields: ['id', 'text'],

    proxy: {
        type: 'ajax',
        url: '/api/HardwareHealthSensorManagement/GetLastStatuses',
        headers: {
            'content-type': 'application/json',
            'accept': 'application/json'
        },
        reader: {
            type: 'json'

        }
    }
});