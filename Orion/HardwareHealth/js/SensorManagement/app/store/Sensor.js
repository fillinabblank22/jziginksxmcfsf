﻿Ext42.define('SensorManagement.store.Sensor', {
    extend: 'Ext.data.Store',
    requires: 'SensorManagement.model.Sensor',
    model: 'SensorManagement.model.Sensor',
    remoteSort: true,
    remoteFilter: true,

    proxy: {
        type: 'ajax',
        url: '/api/HardwareHealthSensorManagement/Get',
        headers: {
            'content-type': 'application/json',
            'accept': 'application/json'
        },
        reader: {
            type: 'json',
            root: 'Sensors',
            totalProperty: 'Count'
        }
    }
});