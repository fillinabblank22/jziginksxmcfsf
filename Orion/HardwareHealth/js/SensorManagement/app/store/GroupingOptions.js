﻿Ext42.define('SensorManagement.store.GroupingOptions', {
    extend: 'Ext.data.Store',
    fields: ['Value', 'Caption'],
    data: [
        { 'Value': '', 'Caption': "@{R=HardwareHealth.Strings;K=HHWEBJS_LH0_3; E=js}" }, // No Grouping
        { 'Value': 'ParentObjectVendor', 'Caption': "@{R=HardwareHealth.Strings;K=HHWEBJS_LH0_4; E=js}" }, // Vendor
        { 'Value': 'ParentObject', 'Caption': "@{R=HardwareHealth.Strings;K=HHWEBJS_LH0_9; E=js}" }, // ParentObject
        { 'Value': 'productline', 'Caption': "@{R=HardwareHealth.Strings;K=HHWEBJS_LH0_5; E=js}" }, // Product Line
        { 'Value': 'sensorcategory', 'Caption': "@{R=HardwareHealth.Strings;K=HHWEBJS_LH0_6; E=js}" }, // Sensor Category
        { 'Value': 'laststatus', 'Caption': "@{R=HardwareHealth.Strings;K=HHWEBJS_LH0_7; E=js}" }, // Last status
        { 'Value': 'enabled', 'Caption': "@{R=HardwareHealth.Strings;K=HHWEBJS_LH0_8; E=js}" }, // Enabled
        { 'Value': 'hascustomthresholds', 'Caption': "@{R=HardwareHealth.Strings;K=HHWEBJS_GK0_03; E=js}" } // Custom Thresholds Defined 
    ]
}); 