﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/HardwareHealth/hardwareHealthMasterPage.master" 
    CodeFile="SensorManagement.aspx.cs" Inherits="Orion_HardwareHealth_SensorManagement" 
    Title="<%$ Resources: HardwareHealthWebContent, HHWEBDATA_LH0_05 %>" %>

<%@ Import Namespace="SolarWinds.Orion.Common" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>


<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:Include ID="IncludeExtJs1" runat="server" Framework="Ext" FrameworkVersion="4.2" Section="Top" SpecificOrder="0" />
    <orion:Include ID="Include3" runat="server" File="OrionMinReqs.js" />
    <orion:Include ID="Include1" runat="server" File="OrionCore.js" />
    <orion:Include ID="Extjs42GridFiltering" runat="server" File="extjs/4.2.2/ux/grid.js" />
    <orion:Include ID="Extjs42GridFilteringCSS" runat="server" File="../../Orion/js/extjs/4.2.2/ux/grid.css" />
    <orion:Include ID="Include6" runat="server" Module="HardwareHealth" File="js/SensorManagement/JSONStorageProvider.js" Section="Bottom" />
    <orion:Include ID="Include2" runat="server" Module="HardwareHealth" File="js/SensorManagement/app.js" Section="Bottom" />
    <orion:Include ID="Include5" runat="server" Module="HardwareHealth" File="js/SensorManagement/i18n.js" />
    <orion:Include ID="Include4" runat="server" Module="HardwareHealth" File="styles/SensorManagement.css" />
    <script type="text/javascript">
        SW.Core.namespace("SW.HardwareHealth").IsDemo = ("<%= OrionConfiguration.IsDemoServer %>".toLowerCase() === "true") ? true : false;

        Ext42.onReady(function () {
            // fix font style issue caused by EXTJS 4.2
            if ($('body').hasClass('x42-body')) {
                $('body').removeClass('x42-body');
            };
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
        <Services>
            <asp:ServiceReference Path="/Orion/Services/Information.asmx" />
            <asp:ServiceReference Path="/Orion/Services/WebAdmin.asmx" />
        </Services>
    </asp:ScriptManagerProxy>

    <div class="header">
        <h1 class="sw-hdr-title"><%=Page.Title%></h1>
        <div class="subtitle"></div>
    </div>

    <input type="hidden" name="SensorManagement_PageSize" id="SensorManagement_PageSize" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, "SensorManagement_PageSize", "20")%>' />
    <input type="hidden" name="SensorManagement_SelectedSensorsLimit" id="SensorManagement_SelectedSensorsLimit" value='<%=SelectedSensorsLimit %>' />
    
    <div id="filterNotifications" class="sw-filter-notifications"></div>
    <div id="appContent"></div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
</asp:Content>
