﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using SolarWinds.HardwareHealth.Common;
using SolarWinds.HardwareHealth.Common.Models;
using SolarWinds.HardwareHealth.Common.Models.HardwareHealth;
using SolarWinds.HardwareHealth.Web.Controls.HardwareHealthTree;
using SolarWinds.Orion.Core.Common.i18n;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;

public partial class Orion_HardwareHealth_Controls_EditResourceControls_EditCurrentHardwareHealth : BaseResourceEditControl
{
    private readonly IHardwareTreeProviderFactory hardwareTreeProviderFactory = new HardwareTreeProviderFactory(new HardwareTreeProviderCollection().Providers);
    private const int DefaultTopItemLimit = 10;

    private IHardwareTreeProviderFactory HardwareTreeProviderFactory
    {
        get { return hardwareTreeProviderFactory; }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            ucTemperatureUnit.SaveProperties(properties);

            properties.Add("HardwareTreeProvider", ListOfHardwareTreeProviders.SelectedValue);
            int topItemLimitValue;
            if (int.TryParse(TopItemLimit.Text, out topItemLimitValue))
            {
                properties.Add("TopItemLimit", Convert.ToInt32(TopItemLimit.Text));
            }
            else
            {
                properties.Add("TopItemLimit", DefaultTopItemLimit.ToString(CultureInfo.InvariantCulture));
            }

            return properties;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        var parentObject = ParentObject.Parse(this.NetObjectID);

        // by default can be parentObject node or storage array. In case we put the resource
        // on other view of entity which is hosted on node, we try to get nodeID even then
        // and construct parentObject on our own.
        if (parentObject == null)
        {
            parentObject = FindParentNode(NetObjectID);
        }

        if (parentObject == null)
        {
            throw new InvalidOperationException("Resource is present on unsupported view");
        }

        using (LocaleThreadState.EnsurePrimaryLocale())
        {
            var request = new HardwareTreeProviderRequest(parentObject);
            IDictionary<string, string> providers = HardwareTreeProviderFactory.GetValidHardwareHealthTreeProviders(request).OrderBy(p => p.GetPriority())
                .Select(g => new { Key = g.GetUniqueName(), Value = g.GetLocalizedName() })
                .ToDictionary(Item => Item.Key, Item => Item.Value);
            SortedDictionary<string, string> providerDataSource = new SortedDictionary<string, string>(providers) { { string.Empty, Resources.HardwareHealthWebContent.HHWEBDATA_JP0_2 } };

            ucTemperatureUnit.SelectedTemperatureUnit = GetSavedTemperatureUnit();
            ListOfHardwareTreeProviders.DataSource = providerDataSource;
            ListOfHardwareTreeProviders.DataBind();

            if (!IsPostBack)
            {
                ListOfHardwareTreeProviders.SelectedValue = Resource.Properties["HardwareTreeProvider"] ?? string.Empty;
                TopItemLimit.Text = Resource.Properties["TopItemLimit"] ?? DefaultTopItemLimit.ToString(CultureInfo.InvariantCulture);
            }
        }
    }

    private ParentObject FindParentNode(string netObjectId)
    {
        if (netObjectId == null)
        {
            throw new ArgumentNullException("netObjectId");
        }

        var netObject = NetObjectFactory.Create(netObjectId);
        Node node = null;
        while (netObject != null)
        {
            node = netObject as Node;

            if (node != null)
            {
                break;
            }
            else
            {
                netObject = netObject.Parent;
            }
        }

        ParentObject parentObject = null;

        if (node != null)
        {
            parentObject = new ParentObject(ParentObjectType.Node, node.NodeID);
        }

        return parentObject;
    }

    private HardwareHealthUnit GetSavedTemperatureUnit()
    {
        return EnumHelper.GetEnumValue(SolarWinds.HardwareHealth.Web.HardwareHealthRoleAccessor.TemperatureUnit, HardwareHealthUnit.DegreesF);
    }
}
