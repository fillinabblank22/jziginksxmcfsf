﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditHardwareHealthSummary.ascx.cs" Inherits="Orion_HardwareHealth_Controls_EditResourceControls_EditHardwareHealthSummary" %>

 <asp:Panel ID="EntitiesObjectPanel" runat="server">
    <table border="0" style="width:100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <div runat="server" ID="SectionObjectType" class="sectionHeader">
                    <%= Resources.CoreWebContent.WEBDATA_AK0_333%>
                </div>
                <asp:CheckBoxList runat="server" ID="cbl_ObjectTypes"> </asp:CheckBoxList>
                <asp:CustomValidator ID="CustomValidatorSelectedEntities" runat="server" OnServerValidate="ValidateIfAnyItemSelected" ErrorMessage="<%$ Resources: HardwareHealthWebContent, HHWEBCODE_JP0_8 %>" ></asp:CustomValidator>
            </td>
        </tr>
    </table>
       
</asp:Panel> 