﻿using System;
using System.Collections.Generic;
using System.Globalization;
using SolarWinds.HardwareHealth.Common;
using SolarWinds.HardwareHealth.Common.Models;
using SolarWinds.HardwareHealth.Common.Models.HardwareHealth;
using SolarWinds.Orion.Web.UI;

public partial class Orion_HardwareHealth_Controls_EditResourceControls_EditCurrentHardwareHealthForGroup : BaseResourceEditControl
{
    private const int DefaultTopItemLimit = 10;
    
    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            ucTemperatureUnit.SaveProperties(properties);

            int topItemLimitValue;
            if (int.TryParse(TopItemLimit.Text, out topItemLimitValue))
            {
                properties.Add("TopItemLimit", Convert.ToInt32(TopItemLimit.Text));
            }
            else
            {
                properties.Add("TopItemLimit", DefaultTopItemLimit.ToString(CultureInfo.InvariantCulture));
            }

            return properties;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        var parentObject = ParentObject.Parse(this.NetObjectID);
        
        if (parentObject == null)
        {
            throw new InvalidOperationException(@"Resource is present on unsupported view");
        }
        
        ucTemperatureUnit.SelectedTemperatureUnit = GetSavedTemperatureUnit();
        
        if (!IsPostBack)
        {
            TopItemLimit.Text = Resource.Properties[@"TopItemLimit"] ?? DefaultTopItemLimit.ToString(CultureInfo.InvariantCulture);
        }
    }
    
    private HardwareHealthUnit GetSavedTemperatureUnit()
    {
        return EnumHelper.GetEnumValue(SolarWinds.HardwareHealth.Web.HardwareHealthRoleAccessor.TemperatureUnit, HardwareHealthUnit.DegreesF);
    }
}
