﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditCurrentHardwareHealthForGroup.ascx.cs" Inherits="Orion_HardwareHealth_Controls_EditResourceControls_EditCurrentHardwareHealthForGroup" %>
<%@ Register Src="~/Orion/HardwareHealth/Controls/SelectHardwareTemperature.ascx" TagPrefix="hh" TagName="SelectHardwareTemperature" %>

<% if (!HttpContext.Current.Request.Url.AbsolutePath.ToLowerInvariant().Contains("editcustomobjectresource.aspx"))
{ // Setting preffered temperature unit from context of custom object resource does not work
  // because it is handled in special way
%>
<b><%= Resources.HardwareHealthWebContent.HHWEBDATA_DO0_4%></b>
<hh:SelectHardwareTemperature runat="server" ID="ucTemperatureUnit" />
<%}%>

<table width="650">
    <tr>
        <td class="formRightInput" style="font-weight: bold;">
            <%=Resources.HardwareHealthWebContent.HHWEBCODE_JP0_2%>
        </td>
        <td class="formRightInput">
            <asp:TextBox ID="TopItemLimit" runat="server" Width="50"></asp:TextBox>
        </td>
        <td class="formHelpfulText">
            &nbsp;
        </td>
    </tr>
</table>
