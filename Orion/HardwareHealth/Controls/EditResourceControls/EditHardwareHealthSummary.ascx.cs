﻿using SolarWinds.HardwareHealth.Common.Extensions;
using SolarWinds.HardwareHealth.Common.Extensions.System;
using SolarWinds.HardwareHealth.Common.Models;
using SolarWinds.HardwareHealth.Web.UI.Resource;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Castle.Core.Internal;
using SolarWinds.HardwareHealth.Web;

public partial class Orion_HardwareHealth_Controls_EditResourceControls_EditHardwareHealthSummary : BaseResourceEditControl
{
    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            SetupSelectedPropertyTypesProrerty(properties);
            return properties;
        }
    }

    private string[] SelectedPropertyObjects
    {
        get
        {
            var entityNameValue = Resource.Properties[HardwareHealthSummary.PropertyEntityName];
            return (!String.IsNullOrEmpty(entityNameValue)) ? entityNameValue.Split(':') : new string[] { HardwareHealthSummary.DefaultEntityName };
        }
    }

    private void SetupSelectedPropertyTypesProrerty(Dictionary<string, object> properties)
    {
        List<string> entityNamePropertyCollection = new List<string>();
        string propertyString = string.Empty;
        foreach (ListItem item in cbl_ObjectTypes.Items)
        {
            if (item.Selected)
            {
                entityNamePropertyCollection.Add(item.Value);
            }
        }
        propertyString = string.Join(":", entityNamePropertyCollection);
        properties.Add(HardwareHealthSummary.PropertyEntityName, propertyString);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var existingEntityTypes = EntityTypesFactory.GetAvailableDisplayNamePluralsFromType(GetParentObjectEntityTypes());
            InitializeObjectTypes(existingEntityTypes);

            foreach (ListItem item in cbl_ObjectTypes.Items)
            {
                item.Selected =
                 SelectedPropertyObjects.Contains(item.Value);
            }
        }
    }


    /// <summary>
    /// Initialize checkboxList with object types
    /// </summary>
    private void InitializeObjectTypes(Dictionary<string, string> entities)
    {
        if (entities != null)
        {
            cbl_ObjectTypes.DataSource = entities;
            cbl_ObjectTypes.DataTextField = "Value";
            cbl_ObjectTypes.DataValueField = "Key";
            cbl_ObjectTypes.DataBind();
            return;
        }
        throw new ArgumentNullException("entityType");
    }

    private string[] GetParentObjectEntityTypes()
    {
        List<string> parentObjectTypeList = new List<string>();
        foreach (var entityType in ParentObjectTypeFactory.GetKnownTypes())
        {
            parentObjectTypeList.Add(entityType.GetNetObjectEntityType());
        }
        return parentObjectTypeList.ToArray();
    }


    protected void ValidateIfAnyItemSelected(object source, ServerValidateEventArgs args)
    {
        for (int i = 0; i < cbl_ObjectTypes.Items.Count; i++)
        {
            if (cbl_ObjectTypes.Items[i].Selected)
            {
                args.IsValid = true;
                return;
            }
        }
        args.IsValid = false;
    }

}