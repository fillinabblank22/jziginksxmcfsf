﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AvailabilityChartLegend.ascx.cs" Inherits="Orion_HardwareHealth_Controls_Legends_AvailabilityChartLegend" %>

<script type="text/javascript">
    SW.Core.Charts.Legend.hardwarehealth_availabilityChartLegendInitializer__<%= legend.ClientID %> = function (chart, dataUrlParameters) {
        SW.HardwareHealth.Charts.AvailabilityChartLegend.createStandardLegend(chart, '<%= legend.ClientID %>');
    };
        
        
</script>

<table runat="server" id="legend" class="chartLegend sw-HardwareHealth-availability-legend"></table>
<div style="float:right;">
    <img class="chartLegendLogo" src="/orion/images/SolarWinds.Logo.Footer.png"/>
</div>
