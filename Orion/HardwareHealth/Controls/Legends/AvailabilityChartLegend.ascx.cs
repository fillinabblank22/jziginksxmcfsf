﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;

public partial class Orion_HardwareHealth_Controls_Legends_AvailabilityChartLegend : UserControl, IChartLegendControl
{
    public string LegendInitializer { get { return "hardwarehealth_availabilityChartLegendInitializer__" + legend.ClientID; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        OrionInclude.CoreFile("/HardwareHealth/js/Charts/Charts.HardwareHealth.AvailabilityChartLegend.js", OrionInclude.Section.Middle);
        OrionInclude.CoreFile("/HardwareHealth/Styles/HardwareHealth.css");
    }
}