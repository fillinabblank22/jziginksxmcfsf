﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HardwareHealthOverviewLegend.ascx.cs" Inherits="Orion_HardwareHealth_Controls_Charts_HardwareHealthOverviewLegend" %>
<%@ Import Namespace="SolarWinds.Orion.Web" %>
<script type="text/C#" runat="server">
    protected override void OnInit(EventArgs e)
    {
        OrionInclude.CoreFile("/Charts/js/allcharts.js").AddJsInit(
            string.Format(@"SW.Core.Charts.Legend.hardwareHealth_OverviewLegendInitializer__{0} = function (chart) {{ SW.HardwareHealth.Charts.HardwareOverviewChartLegend.createStandardLegend(chart, '{0}'); }};", legend.ClientID ));
    }  
</script>
<div id="HardwareHealthLegend" class="hwh_HardwareHealthLegend" runat="server">
    <div class="hwh_HardwareHealthLegendInner">
        <table runat="server" id="legend" class="chartLegend hwh_HardwareHealthStatus" ></table>
    </div>
</div>