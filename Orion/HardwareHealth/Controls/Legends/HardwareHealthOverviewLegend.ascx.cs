﻿using System;
using System.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;

public partial class Orion_HardwareHealth_Controls_Charts_HardwareHealthOverviewLegend : UserControl, IChartLegendControl
{
    public string LegendInitializer { get { return "hardwareHealth_OverviewLegendInitializer__" + legend.ClientID; } }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}