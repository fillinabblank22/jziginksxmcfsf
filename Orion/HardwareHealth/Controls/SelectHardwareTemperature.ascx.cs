﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web;
using System.Web.UI.WebControls;
using SolarWinds.HardwareHealth.Common;
using SolarWinds.HardwareHealth.Common.Models.HardwareHealth;
using SolarWinds.HardwareHealth.Web;
using SolarWinds.HardwareHealth.Web.Shared.Controls.HardwareHealthTree;
using SolarWinds.Orion.Core.Common.i18n;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;

public partial class Orion_HardwareHealth_Controls_SelectHardwareTemperature : System.Web.UI.UserControl, IChartEditorSettings
{
    private HardwareHealthUnit temperatureUnit;
    
    public HardwareHealthUnit SelectedTemperatureUnit
    {
        get
        {
            return HardwareHealthTreeHelper.GetTemperatureUnitFromInt(Convert.ToInt32(rblTemperatureUnit.SelectedValue));
        }
        set
        {
            HardwareHealthUnit temperature = value;

            if (temperature != HardwareHealthUnit.DegreesC && temperature != HardwareHealthUnit.DegreesF)
                temperature = HardwareHealthTreeHelper.DefaultTemperatureUnit;

            switch (temperature)
            {
                case HardwareHealthUnit.DegreesF:
                    rblTemperatureUnit.Items[0].Selected = true;
                    break;
                case HardwareHealthUnit.DegreesC:
                    rblTemperatureUnit.Items[1].Selected = true;
                    break;
            }
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        using (LocaleThreadState.EnsurePrimaryLocale())
        {
            rblTemperatureUnit.Items.Add(new ListItem(Resources.HardwareHealthWebContent.HHWEBCODE_DO0_2, ((int)HardwareHealthUnit.DegreesF).ToString(CultureInfo.InvariantCulture)));
            rblTemperatureUnit.Items.Add(new ListItem(Resources.HardwareHealthWebContent.HHWEBCODE_DO0_3, ((int)HardwareHealthUnit.DegreesC).ToString(CultureInfo.InvariantCulture)));
        }
    }

    public void Initialize(ChartSettings settings, ResourceInfo resourceInfo, string netObjectId)
    {
        SelectedTemperatureUnit = GetSavedTemperatureUnit();
    }

    public void SaveProperties(Dictionary<string, object> properties)
    {
        var profile = HttpContext.Current.Profile.GetProfileGroup("HardwareHealth");
        profile.SetPropertyValue(HardwareHealthRoleAccessor.KeyTemperatureUnit, (int)SelectedTemperatureUnit);
        Profile.Save();
    }

    private HardwareHealthUnit GetSavedTemperatureUnit()
    {
        return EnumHelper.GetEnumValue(HardwareHealthRoleAccessor.TemperatureUnit, HardwareHealthUnit.DegreesF);
    }
}