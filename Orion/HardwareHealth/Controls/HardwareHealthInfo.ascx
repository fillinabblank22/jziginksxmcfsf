﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HardwareHealthInfo.ascx.cs" Inherits="Orion_APM_Controls_HardwareHealthInfo" %>

<tr>
    <td><%= Resources.HardwareHealthWebContent.HwhWeb_PollingInfo_HWHealthMonitorCount %></td>
    <td><asp:literal id="numberOfHWHMonitors" runat="server" /></td>
</tr>
<tr>
    <td><%= Resources.HardwareHealthWebContent.HwhWeb_PollingInfo_HWHealthSensorCount %></td>
    <td><asp:literal id="numberOfHWHSensors" runat="server" /></td>
</tr>