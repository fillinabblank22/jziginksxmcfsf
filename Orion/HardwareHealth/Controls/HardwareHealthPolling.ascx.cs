﻿using SolarWinds.HardwareHealth.Common;
using SolarWinds.HardwareHealth.Common.Notifications;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.ModuleManager;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Swis.PubSub;
using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.HardwareHealth.Common.Models.HardwareHealth;
using SolarWinds.HardwareHealth.Web.Controllers;
using SolarWinds.HardwareHealth.Web.DAL;
using SolarWinds.HardwareHealth.Web.UI.ViewModels;

public partial class Orion_HardwareHealth_Controls_HardwareHealthPolling : System.Web.UI.UserControl, INodePropertyPlugin
{
    private readonly string globalSettingOptionValue = "GLOBAL";
    private PollingSettingsViewModel model;

    public void Initialize(IList<Node> nodes, NodePropertyPluginExecutionMode mode, Dictionary<string, object> pluginState)
    {
        HardwareHealthPollingSettingsController controller =
            new HardwareHealthPollingSettingsController(ModuleManager.InstanceWithCache, new SupportedNodesDal(), 
            new SettingsDAL(), NodeSettingsDAL.GetLastNodeSettings);
        model = controller.GetPollingSettingsViewModel(nodes.Select(n => n.Id));
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        HardwareHealthPollingContent.Visible = model.PollingSettingsVisible;
        if (!HardwareHealthPollingContent.Visible || IsPostBack)
        {
            return;
        }

        // Statistics poll interval
        ApplyStatisticsPollInterval.Visible = model.ApplyStatisticsPollIntervalVisible;
        OverrideStatisticsPollInterval.Enabled = model.OverrideStatisticsPollIntervalEnabled;
        OverrideStatisticsPollInterval.Checked = model.OverrideStatisticsPollIntervalChecked;
        SettingsStatisticsPollInterval.Visible = model.OverrideStatisticsPollIntervalChecked;
        StatisticsPollInterval.Enabled = model.OverrideStatisticsPollIntervalChecked;
        StatisticsPollInterval.Text = model.StatisticsPollInterval;

        RangeValidatorStatisticsPollInterval.MinimumValue = HardwareHealthConstants.StatisticsPollIntervalMin.ToString();
        RangeValidatorStatisticsPollInterval.MaximumValue = HardwareHealthConstants.StatisticsPollIntervalMax.ToString();
        RangeValidatorStatisticsPollInterval.ErrorMessage = string.Format(
            "({0}-{1})",
            HardwareHealthConstants.StatisticsPollIntervalMin,
            HardwareHealthConstants.StatisticsPollIntervalMax
        );

        // Preferred Cisco MIB
        RowPreferredCiscoMIBSetting.Visible = model.CiscoMibSelectorVisible;
        ApplyPreferredCiscoMIBSetting.Visible = model.ApplyCiscoMibSelectorVisible;
        PreferredCiscoMIBSetting.Enabled = model.CiscoMibSelectorEnabled;

        if (model.CiscoMibSelectorValue != null)
        {
            PreferredCiscoMIBSetting.SelectedValue = model.CiscoMibSelectorValue;
        }
    }

    public bool Update()
    {
        if ((model.SupportedNodes == null) || !model.SupportedNodes.Any())
        {
            return true;
        }

        if (!model.IsMultiSelect || ApplyStatisticsPollInterval.Checked)
        {
            if (OverrideStatisticsPollInterval.Checked)
            {
                NodeSettingsDAL.UpdateSpecificSettingForAllNodes(
                    HardwareHealthConstants.StatisticsPollIntervalSettingID,
                    Convert.ToInt32(StatisticsPollInterval.Text).ToString(),
                    string.Format("NodeId IN ({0})", string.Join(",", model.SupportedNodes.Select(node => node.NodeId)))
                    );
            }
            else
            {
                foreach (var node in model.SupportedNodes)
                {
                    NodeSettingsDAL.DeleteSpecificSettingForNode(node.NodeId, HardwareHealthConstants.StatisticsPollIntervalSettingID);
                }
            }
        }

        if (!model.IsMultiSelect || ApplyPreferredCiscoMIBSetting.Checked)
        {
            string ciscoMibValue = PreferredCiscoMIBSetting.SelectedValue;

            foreach (var supportedNode in model.SupportedCiscoNodes)
            {
                if (ciscoMibValue.Equals(globalSettingOptionValue))
                {
                    if (supportedNode.PreferredCiscoMIBSetting != null)
                    {
                        NodeSettingsDAL.DeleteSpecificSettingForNode(supportedNode.NodeId, HardwareHealthConstants.PreferredCiscoMIBKey);

                        var proxy = PublisherClient.Instance;
                        var notification = new PreferredCiscoMIBNodeSettingRemovedNotification(supportedNode.NodeId, supportedNode.PreferredCiscoMIBSetting);
                        proxy.Publish(notification);
                    }
                }
                else if (supportedNode.PreferredCiscoMIBSetting != ciscoMibValue)
                {
                    NodeSettingsDAL.SafeInsertNodeSetting(supportedNode.NodeId, HardwareHealthConstants.PreferredCiscoMIBKey, ciscoMibValue);

                    var proxy = PublisherClient.Instance;
                    var notification = supportedNode.PreferredCiscoMIBSetting == null
                        ? new PreferredCiscoMIBNodeSettingCreatedNotification(supportedNode.NodeId, ciscoMibValue)
                        : new PreferredCiscoMIBNodeSettingChangedNotification(supportedNode.NodeId, ciscoMibValue, supportedNode.PreferredCiscoMIBSetting);
                    proxy.Publish(notification);
                }
            }
        }

        return true;
    }

    public bool Validate()
    {
        return true;
    }

    protected void ApplyPreferredCiscoMIBSetting_CheckedChanged(object sender, EventArgs e)
    {
        if (model.IsMultiSelect) {
            PreferredCiscoMIBSetting.Enabled = ApplyPreferredCiscoMIBSetting.Checked;
        }
    }

    protected void ApplyStatisticsPollInterval_CheckedChanged(object sender, EventArgs e)
    {
        if (model.IsMultiSelect)
        {
            OverrideStatisticsPollInterval.Enabled = ApplyStatisticsPollInterval.Checked;
            StatisticsPollInterval.Enabled = ApplyStatisticsPollInterval.Checked && OverrideStatisticsPollInterval.Checked;
        }
    }

    protected void OverrideStatisticsPollInterval_CheckedChanged(object sender, EventArgs e)
    {
        StatisticsPollInterval.Enabled = OverrideStatisticsPollInterval.Checked;
        SettingsStatisticsPollInterval.Visible = OverrideStatisticsPollInterval.Checked;
    }
}