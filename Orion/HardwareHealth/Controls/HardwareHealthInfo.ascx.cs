﻿using System;
using System.Data;
using SolarWinds.InformationService.Contract2;
using SolarWinds.Orion.Web.Admin.Engines;
using SolarWinds.Orion.Web.InformationService;

public partial class Orion_APM_Controls_HardwareHealthInfo : EngineInfoBaseControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        using (var swis = InformationServiceProxy.CreateV3())
        {
            string query = @"SELECT count(hwInfo.ID) AS cnt
FROM Orion.HardwareHealth.HardwareInfo hwInfo
LEFT JOIN Orion.Nodes n on n.NodeID=hwInfo.NodeID
WHERE (n.EngineID = @EngineID) and (hwInfo.IsDisabled = 'False')";

            numberOfHWHMonitors.Text = GetCount(swis, query);

            query = @"SELECT count(hwItem.ID) AS cnt
FROM Orion.HardwareHealth.HardwareItem hwItem
LEFT JOIN Orion.HardwareHealth.HardwareInfo hwInfo on hwInfo.NodeID = hwItem.NodeID
LEFT JOIN Orion.Nodes n on n.NodeID = hwItem.NodeID
WHERE (n.EngineID = @EngineID) and (hwItem.IsDeleted = 'False') and (hwInfo.IsDisabled = 'False')";

            numberOfHWHSensors.Text = GetCount(swis, query);
        }
   }

    private string GetCount(InformationServiceProxy swis, string query)
    {
        string result = string.Empty;
        DataTable table = swis.Query(query, new PropertyBag()
        {
            {"EngineID", this.EngineID}
        });

        if (table != null && table.Rows.Count > 0)
        {
            result = table.Rows[0][0].ToString();
        }

        return result;
    }
}