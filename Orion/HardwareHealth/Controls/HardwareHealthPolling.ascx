﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HardwareHealthPolling.ascx.cs" Inherits="Orion_HardwareHealth_Controls_HardwareHealthPolling" %>

<div class="contentBlock" runat="server" id="HardwareHealthPollingContent">
    <div runat="server" id="HardwareHealthPollingHeader">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td class="contentBlockHeader">
                    <asp:Literal runat="server" Text="<%$ Resources: HardwareHealthWebContent,HHWEBDATA_LH0_01 %>" />
                </td>
            </tr>
        </table>
    </div>
    <table class="blueBox">
        <tr>
            <td class="leftLabelColumn" style="vertical-align: text-top;">
                <asp:CheckBox runat="server" ID="ApplyStatisticsPollInterval" AutoPostBack="true" OnCheckedChanged="ApplyStatisticsPollInterval_CheckedChanged" />

                <asp:Literal runat="server" ID="LabelStatisticsPollInterval" Text="<%$ Resources: HardwareHealthWebContent,HHWEBCODE_JP0_6 %>"/>
            </td>
            <td class="rightInputColumn">
                <asp:CheckBox runat="server" ID="OverrideStatisticsPollInterval" AutoPostBack="true" OnCheckedChanged="OverrideStatisticsPollInterval_CheckedChanged"/>

                <asp:Literal runat="server" ID="Literal1" Text="<%$ Resources: HardwareHealthWebContent,HHWEBCODE_JP0_7 %>"/>
                
                <div runat="server" ID="SettingsStatisticsPollInterval">
                    <asp:TextBox runat="server" ID="StatisticsPollInterval"/>

                    <asp:RequiredFieldValidator runat="server" SetFocusOnError="True"
                        ID="RequiredValidatorStatisticsPollInterval"
                        ControlToValidate="StatisticsPollInterval"
                        ErrorMessage="*"/>
                    <asp:RegularExpressionValidator runat="server" SetFocusOnError="True"
                        ID="RegexValidatorStatisticsPollInterval"
                        ControlToValidate="StatisticsPollInterval"
                        ErrorMessage="*"
                        ValidationExpression="^\d+$"/>
                    <asp:RangeValidator runat="server" Display="Dynamic" SetFocusOnError="True" Type="Integer"
                        ID="RangeValidatorStatisticsPollInterval"
                        ControlToValidate="StatisticsPollInterval"/>

		            <asp:Literal runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_MZ0_3 %>" />
                </div>
            </td>
        </tr>
        <tr id="RowPreferredCiscoMIBSetting" runat="server">
            <td class="leftLabelColumn" style="vertical-align: text-top;">
                <asp:CheckBox runat="server" ID="ApplyPreferredCiscoMIBSetting" AutoPostBack="true" OnCheckedChanged="ApplyPreferredCiscoMIBSetting_CheckedChanged" />&nbsp;
                <asp:Literal runat="server" ID="LabelPreferredCiscoMIBSetting" Text="<%$ Resources: HardwareHealthWebContent,HHWEBDATA_LH0_02 %>"></asp:Literal>
            </td>
            <td class="rightInputColumn">
                <div>
                    <asp:DropDownList ID="PreferredCiscoMIBSetting" runat="server">
                        <asp:ListItem Value="GLOBAL" Text="<%$ Resources: HardwareHealthWebContent,HHWEBDATA_LH0_03 %>"></asp:ListItem>
                        <asp:ListItem Value="CISCO-ENTITY-SENSOR-MIB" Text="CISCO-ENTITY-SENSOR-MIB"></asp:ListItem>
                        <asp:ListItem Value="CISCO-ENVMON-MIB" Text="CISCO-ENVMON-MIB"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <span class="helpfulText" style="padding: 0px;">
                    <asp:Literal ID="DescriptionPreferredCiscoMIBSetting" runat="server" Text="<%$ Resources: HardwareHealthWebContent,HHWEBDATA_LH0_04 %>" />
                </span>
            </td>
        </tr>

    </table>
</div>
