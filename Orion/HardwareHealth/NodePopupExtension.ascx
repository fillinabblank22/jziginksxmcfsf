﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodePopupExtension.ascx.cs" Inherits="Orion_APM_NodePopupExtension" %>

<%if (this.HardwareInfo != null)
  {%>
<tr>
	<th style="font-size:11px;"><%= Resources.HardwareHealthWebContent.HHWEBDATA_DO0_5 %></th>
	<td style="font-size:11px;">
	    <img src="<%= GetHardwareStatusIcon() %>" />
        <%= GetHardwareStatusText() %>
	</td>
</tr>
<%} %>
