﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HardwareHealthTemperatureUnit.ascx.cs" Inherits="Orion_HardwareHealth_EditViews_HardwareHealthTemperatureUnit" %>

<asp:ListBox ID="ctrTemperatureUnit" SelectionMode="Single" Rows="1" runat="server">
	<asp:ListItem Text="<%$ Resources: HardwareHealthWebContent, HHWEBCODE_DO0_2 %>" Selected="True" Value="3"/>
	<asp:ListItem Text="<%$ Resources: HardwareHealthWebContent, HHWEBCODE_DO0_3 %>" Value="2"/>
</asp:ListBox>