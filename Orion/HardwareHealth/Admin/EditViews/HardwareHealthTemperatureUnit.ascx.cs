﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.HardwareHealth.Common.Models.HardwareHealth;
using SolarWinds.HardwareHealth.Web;

public partial class Orion_HardwareHealth_EditViews_HardwareHealthTemperatureUnit : SolarWinds.Orion.Web.UI.ProfilePropEditUserControl
{
    public override String PropertyValue { get; set; }

    protected void Page_Load(Object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
            int temperatureUnit = (int)HardwareHealthUnit.DegreesF;
			if (!String.IsNullOrEmpty(PropertyValue))
			{
			    TryConvertStringToTemperatureUnit(PropertyValue, out temperatureUnit);
			}
			ctrTemperatureUnit.ClearSelection();
			HtmlHelper.SetListSelectedValue(ctrTemperatureUnit, temperatureUnit.ToString(CultureInfo.InvariantCulture), ((int)HardwareHealthUnit.DegreesF).ToString(CultureInfo.InvariantCulture));
		}
        PropertyValue = ctrTemperatureUnit.SelectedValue;
	}

    protected static bool TryConvertStringToTemperatureUnit(string value, out int unit)
    {
        unit = (int)HardwareHealthUnit.DegreesF;
        try
        {
            unit = (int)Enum.Parse(typeof(HardwareHealthUnit), value);
            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }
}