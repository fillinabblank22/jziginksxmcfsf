﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterfaceManagementPlugin.ascx.cs" Inherits="Orion_Interfaces_Controls_InterfaceManagementPlugin" %>
<%@ Register tagPrefix="orion" TagName="MaintenanceSchedulerDialog" Src="~/Orion/Controls/MaintenanceSchedulerDialog.ascx"%>

<orion:MaintenanceSchedulerDialog runat="server" />
