﻿<%@ Control Language="C#" ClassName="ErrorsDiscards" EnableViewState="false" %>
<%@ Import Namespace="SolarWinds.Orion.NPM.Web" %>

<script runat="server">
     
    private int _value;

    public int Value
    {
        get { return _value; }
        set { _value = value; }
    }


    protected string ClassName
    {
        get
        {
            if (Thresholds.ErrorsDiscardsError.SettingValue <= this.Value)
                return "Error";
            else if (Thresholds.ErrorsDiscardsWarning.SettingValue <= this.Value)
                return "Warning";

            return string.Empty;
        }
    }
    	
            
</script>

<span class="<%= this.ClassName %>"><%= this.Value %></span>