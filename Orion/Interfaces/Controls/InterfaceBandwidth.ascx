<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterfaceBandwidth.ascx.cs" Inherits="Orion_Nodes_Controls_InterfaceBandwidth" %>
<table>
<tr>
<td class="contentBlockHeader" colspan="2">
    <asp:CheckBox ID="cbMultipleSelection" runat="server" AutoPostBack="True" OnCheckedChanged="cbMultipleSelection_CheckedChanged" />
    <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_186%>
</td>
</tr>
</table>
<table style="background-color: #e4f1f8;" width="100%">
<tr>
	<td class="leftLabelColumn">
	&nbsp;
	</td>
	<td class="rightInputColumn">
	<asp:CheckBox ID="cbCustomBandwidth" runat="server" AutoPostBack="true" OnCheckedChanged="cbCustomBandwidth_CheckedChanged" /><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_187%>
	</td>
</tr>
<tr>
	<td class="leftLabelColumn">
	<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_188%>
	</td>
	<td class="rightInputColumn"><asp:TextBox onchange="javascript:setTimeout('__doPostBack(\'ctl00$ctl00$ContentPlaceHolder1$adminContentPlaceholder$InterfaceBandwidth$cbCustomBandwidth\',\'\')', 0)" runat="server" ID="tbTBandwidth"   Enabled="false">1000</asp:TextBox> <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_189%>
	<asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="<%$ Resources: InterfacesWebContent, NPMWEBDATA_VB0_191%>" ControlToValidate="tbTBandwidth" Display="Static" Operator="DataTypeCheck" SetFocusOnError="True" Type="Double" EnableViewState="False"></asp:CompareValidator>    
	</td>
</tr>
<tr>
	<td class="leftLabelColumn">
	<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_190%>
	</td>
	<td class="rightInputColumn"><asp:TextBox onchange="javascript:setTimeout('__doPostBack(\'ctl00$ctl00$ContentPlaceHolder1$adminContentPlaceholder$InterfaceBandwidth$cbCustomBandwidth\',\'\')', 0)" runat="server" ID="tbRBandwidth" Enabled="false">1000</asp:TextBox> <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_189%>
	<asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="<%$ Resources: InterfacesWebContent, NPMWEBDATA_VB0_191%>" ControlToValidate="tbRBandwidth" Display="Static" Operator="DataTypeCheck" SetFocusOnError="True" Type="Double" EnableViewState="False"></asp:CompareValidator>
	</td>
</tr>
</table>