using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Orion_Nodes_Controls_InterfaceBandwidth : System.Web.UI.UserControl
{
	public bool MultipleSelection
	{
		get { return cbMultipleSelection.Visible; }
		set
		{ cbMultipleSelection.Visible = value; }
	}

	public bool MultipleSelection_Selected
	{
		get { return cbMultipleSelection.Checked; }
		set
		{
			cbMultipleSelection.Checked = value;
			cbMultipleSelection_CheckedChanged(null, null);
		}
	}

	protected void cbMultipleSelection_CheckedChanged(object sender, EventArgs e)
	{
		tbRBandwidth.Enabled = cbMultipleSelection.Checked;
		tbTBandwidth.Enabled = cbMultipleSelection.Checked;
		cbCustomBandwidth.Enabled = cbMultipleSelection.Checked;

		cbCustomBandwidth_CheckedChanged(sender, e);
	}


	public bool CustomBandwidth
	{
		get
		{
			return cbCustomBandwidth.Checked;
		}
		set
		{
			cbCustomBandwidth.Checked = value;
			cbCustomBandwidth_CheckedChanged(null, null);
		}
	}

	public double TransmitBandwidth
	{
		get
		{
			return ValueFromDisplayValue(tbTBandwidth.Text);
		}
		set
		{
			tbTBandwidth.Text = DisplayValueFromValue(value);
		}
	}

	public double ReceiveBandwidth
	{
		get
		{
			return ValueFromDisplayValue(tbRBandwidth.Text);
		}
		set
		{
			tbRBandwidth.Text = DisplayValueFromValue(value);
		}
	}

	private const double Scale = 1000 * 1000;

	private double ValueFromDisplayValue(string displayValue)
	{
		double bits = 0;
		double.TryParse(displayValue, out bits);
		return bits * Scale;
	}

	private string DisplayValueFromValue(double value)
	{
		return (value / Scale).ToString();
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		Page.Validate();
	}

	private void SetTextBoxesStyle()
	{
		if (!tbTBandwidth.Enabled)
			tbTBandwidth.CssClass = "disabled";
		else
			tbTBandwidth.CssClass = string.Empty;

		if (!tbRBandwidth.Enabled)
			tbRBandwidth.CssClass = "disabled";
		else
			tbRBandwidth.CssClass = string.Empty;

		//CompareValidator1.Enabled
		//CompareValidator2.Enabled
	}

	protected void cbCustomBandwidth_CheckedChanged(object sender, EventArgs e)
	{
		if (cbCustomBandwidth.Enabled)
		{
			tbTBandwidth.Enabled = cbCustomBandwidth.Checked;
			tbRBandwidth.Enabled = cbCustomBandwidth.Checked;
		}
		SetTextBoxesStyle();
	}
}
