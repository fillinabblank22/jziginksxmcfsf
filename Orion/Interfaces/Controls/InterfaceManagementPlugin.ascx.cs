﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.Models.Alerts;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Plugins;

public partial class Orion_Interfaces_Controls_InterfaceManagementPlugin : ManagementTasksPluginBase
{
    private static readonly IList<EntityAlertSuppressionMode> showSuppressAlertsNowLinkModes = new List<EntityAlertSuppressionMode>
        {
            EntityAlertSuppressionMode.NotSuppressed,
            EntityAlertSuppressionMode.SuppressionScheduledForItself,
            EntityAlertSuppressionMode.SuppressionScheduledForParent
        };

    public override IEnumerable<ManagementTaskItem> GetManagementTasks(NetObject netObject, string returnUrl)
    {
        var interfaceNetObject = netObject as Interface;
        if (interfaceNetObject == null)
        {
            yield break;
        }

        string maintenanceModeGroupLabel = $"<img src='/Orion/Nodes/images/icons/maintenance_mode_icon16.png' alt='' />&nbsp;{Resources.CoreWebContent.WEBDATA_MH0_01}";

        if (Profile.AllowNodeManagement)
        {
            yield return new ManagementTaskItem
            {
                Section = string.Empty,
                ClientID = "editInterface",
                LinkInnerHtml = string.Format(
                    "<img src='/Orion/Nodes/images/icons/icon_edit.gif' alt='' />&nbsp;{0}", Resources.InterfacesWebContent.NPMWEBDATA_VB0_110),
                LinkUrl = string.Format("/Orion/Interfaces/InterfaceProperties.aspx?Interfaces={0}&ReturnTo={1}", interfaceNetObject.InterfaceID, returnUrl)
            };
        }

        if (Profile.AllowUnmanage)
        {
            if (!(interfaceNetObject.Unmanaged || interfaceNetObject.Node.UnManaged))
            {
                yield return new ManagementTaskItem
                {
                    Section = string.Empty,
                    Group = maintenanceModeGroupLabel,
                    MouseHoverTooltip = Resources.CoreWebContent.WEBDATA_MH0_03,
                    ClientID = "unmanageNowLink",
                    LinkInnerHtml =
                        string.Format("<img src='/Orion/Nodes/images/icons/icon_manage.gif' alt='' />&nbsp;{0}",
                            GetLocalizedString("@{R=Core.Strings.2;K=MaintenanceMode_UnmanageNow;E=xml}")),
                    OnClick = IsDemoServer ? GetDemoActionCallback("Core_Alerting_Unmanage") : 
                        string.Format("return SW.Core.MaintenanceMode.UnmanageNowSingleEntity('{0}');", interfaceNetObject.NetObjectID)
                };
            }

            if (interfaceNetObject.Unmanaged && !interfaceNetObject.Node.UnManaged)
            {
                yield return new ManagementTaskItem
                {
                    Section = string.Empty,
                    Group = maintenanceModeGroupLabel,
                    ClientID = "remanageLink",
                    LinkInnerHtml =
                        string.Format("<img src='/Orion/Nodes/images/icons/icon_remanage.gif' alt='' />&nbsp;{0}",
                            GetLocalizedString("@{R=Core.Strings.2;K=MaintenanceMode_ManageAgain;E=xml}")),
                    OnClick = IsDemoServer ? GetDemoActionCallback("Core_Alerting_Unmanage") :
                        string.Format("return SW.Core.MaintenanceMode.ManageAgainSingleEntity('{0}');", interfaceNetObject.NetObjectID)
                };
            }
        }

        if (Profile.AllowNodeManagement)
        {
            if (interfaceNetObject.Node.IsSNMP && OrionModuleManager.IsInstalled("NPM"))
            {
                yield return new ManagementTaskItem
                {
                    Section = string.Empty,
                    LinkInnerHtml =
                        string.Format("<img src='/Orion/Nodes/images/icons/icon_assignpoller.gif' alt='' />&nbsp;{0}",
                            Resources.InterfacesWebContent.NPMWEBDATA_VB0_113),
                    LinkUrl =
                        string.Format("/Orion/NPM/InterfaceCustomPollers.aspx?Interfaces={0}&ReturnTo={1}",
                            interfaceNetObject.InterfaceID, returnUrl),
                };
            }

            yield return new ManagementTaskItem
            {
                Section = string.Empty,
                ClientID = "pollNowLnk",
                LinkInnerHtml =
                    "<img src='/Orion/images/pollnow_16x16.gif' alt='' />&nbsp;" +
                    Resources.InterfacesWebContent.NPMWEBDATA_VB0_115,
                OnClick = string.Format("return showPollNowDialog(['{0}']);", interfaceNetObject.NetObjectID)
            };

            yield return new ManagementTaskItem
            {
                Section = string.Empty,
                ClientID = "rediscoverLnk",
                LinkInnerHtml =
                    string.Format("<img src='/Orion/Nodes/images/icons/icon_discover.gif' alt='' />&nbsp;{0}",
                        Resources.InterfacesWebContent.NPMWEBDATA_VB0_114),
                OnClick = string.Format("return showRediscoveryDialog(['{0}']);", interfaceNetObject.NetObjectID)
            };
        }

        if (Profile.AllowUnmanage)
        {
            if (interfaceNetObject.AlertSuppressionState.SuppressionMode == EntityAlertSuppressionMode.SuppressedByItself)
            {
                yield return new ManagementTaskItem
                {
                    Section = string.Empty,
                    Group = maintenanceModeGroupLabel,
                    ClientID = "resumeAlertsLink",
                    LinkInnerHtml =
                        string.Format("<img src='/Orion/Nodes/images/icons/icon_remanage.gif' alt='' />&nbsp;{0}",
                            Resources.CoreWebContent.WEBDATA_LH0_15),
                    OnClick = IsDemoServer
                        ? GetDemoActionCallback("Core_Alerting_Resume")
                        : string.Format("return SW.Core.MaintenanceMode.ResumeAlertsSingleEntity('{0}')",
                            Server.HtmlEncode(interfaceNetObject.SwisUri))
                };
            }

            if (showSuppressAlertsNowLinkModes.Contains(interfaceNetObject.AlertSuppressionState.SuppressionMode))            
            {
                yield return new ManagementTaskItem
                {
                    Section = string.Empty,
                    Group = maintenanceModeGroupLabel,
                    MouseHoverTooltip = Resources.CoreWebContent.WEBDATA_MH0_02,
                    ClientID = "suppressAlertsNowLink",
                    LinkInnerHtml =
                        string.Format("<img src='/Orion/Nodes/images/icons/icon_manage.gif' alt='' />&nbsp;{0}",
                            Resources.CoreWebContent.WEBDATA_LH0_16),
                    OnClick = IsDemoServer
                        ? GetDemoActionCallback("Core_Alerting_Suppress")
                        : string.Format("return SW.Core.MaintenanceMode.SuppressAlertsNowSingleEntity('{0}')",
                            Server.HtmlEncode(interfaceNetObject.SwisUri))
                };
            }

            yield return new ManagementTaskItem
            {
                Section = string.Empty,
                Group = maintenanceModeGroupLabel,
                ClientID = "scheduleMaintenanceLnk",
                LinkInnerHtml =
                    string.Format("<img src='/Orion/Nodes/images/icons/icon_manage.gif' alt='' />&nbsp;{0}",
                        Resources.CoreWebContent.WEBDATA_LH0_17),
                OnClick = IsDemoServer ?
                GetDemoActionCallback("Core_Alerting_Schedule") :
                    string.Format(
                        "SW.Core.MaintenanceMode.MaintenanceScheduler.ShowDialogForSingleEntity('{0}', '{1}', '{2}'); return false;",
                        Server.HtmlEncode(interfaceNetObject.SwisUri),
                        interfaceNetObject.NetObjectID, 
                        Server.HtmlEncode(EscapeQuotes(interfaceNetObject.InterfaceName)))
            };
        }

        yield return new ManagementTaskItem
        {
            Section = string.Empty,
            ClientID = "performanceAnalyzer",
            LinkInnerHtml = string.Format("<img src='/Orion/PerfStack/images/icons/perfstack_launch.svg' height='16' width='16' alt='' />&nbsp;{0}", Resources.InterfacesWebContent.NPMWEBDATA_InterfaceDetails_LinkTitle_PerformanceAnalyzer),
            LinkUrl = string.Format("/ui/perfstack/?context=0_Orion.NPM.Interfaces_{0}&withRelationships=true&presetTime=last12Hours&charts=0_Orion.NPM.Interfaces_{0}" +
                                    "-Orion.PerfStack.Status;0_Orion.NPM.Interfaces_{0}-Orion.PerfStack.Alerts;0_Orion.NPM.Interfaces_{0}" +
                                    "-Orion.NPM.InterfaceTraffic.InPercentUtil,0_Orion.NPM.Interfaces_{0}-Orion.NPM.InterfaceTraffic.OutPercentUtil;0_Orion.NPM.Interfaces_{0}" +
                                    "-Orion.NPM.InterfaceTraffic.InAveragebps,0_Orion.NPM.Interfaces_{0}-Orion.NPM.InterfaceTraffic.OutAveragebps;0_Orion.NPM.Interfaces_{0}" +
                                    "-Orion.NPM.InterfaceErrors.InDiscards,0_Orion.NPM.Interfaces_{0}-Orion.NPM.InterfaceErrors.InErrors;0_Orion.NPM.Interfaces_{0}" +
                                    "-Orion.NPM.InterfaceErrors.OutDiscards,0_Orion.NPM.Interfaces_{0}-Orion.NPM.InterfaceErrors.OutErrors;", interfaceNetObject.InterfaceID)
        };
    }

    private string EscapeQuotes(string input)
    {   
        return input.Replace("'", "\\'");
    }
}