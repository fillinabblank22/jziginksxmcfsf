﻿using System;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Interfaces_Controls_Discovery_InterfaceDiscoveryPlugin : System.Web.UI.UserControl
{
    private static Log _log;
    private static Log _logger { get { return _log ?? (_log = new Log()); } }

    protected int InterfaceCount { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            using (WebDAL webDAL = new WebDAL())
            {
                InterfaceCount = webDAL.GetInterfaceCount();
            }
        }
        catch (Exception ex) {
            _logger.ErrorFormat("Cannot get number of managed interfaces. Details: {0}",ex);
        }

        DataBind();
    }

    protected void Button_Click(object sender, EventArgs e)
    {
        var imageButton = sender as LocalizableButton;
        
        DiscoveryCentralHelper.Leave();

        Response.Redirect(imageButton.CommandArgument);
    }


    protected string GetDiscoveryButtonString(int elementCount)
    {
        if (elementCount > 0)
        {
            return "~/Orion/images/Button.NetworkSonarDiscovery.gif";
        }
        return "~/Orion/images/Button.NetworkSonarDiscovery.highlight.gif";
    }

    protected string GetCssClass(int elementCount)
    {
        if (elementCount > 0)
        {
            return "pluginButton";
        }
        return "highlightButton";
    }
}
