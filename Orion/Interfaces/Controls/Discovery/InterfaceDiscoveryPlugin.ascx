﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterfaceDiscoveryPlugin.ascx.cs" Inherits="Orion_Interfaces_Controls_Discovery_InterfaceDiscoveryPlugin" %>
<%@ Register Src="~/Orion/Admin/ManagedNetObjectsInfo.ascx" TagPrefix="orion" TagName="ManagedNetObjectsInfo" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>

<div class="coreDiscoveryIcon">
<asp:Image ID="Image1" runat="server" ImageUrl="~/Orion/Interfaces/Images/DiscoveryCentral/discovery.svg"/>
</div>

<div class="coreDiscoveryPluginBody">
    <h2><%=Resources.InterfacesWebContent.NPMWEBDATA_TM0_84%></h2>
    <div>
        <%=Resources.InterfacesWebContent.NPMWEBDATA_TM0_85%>
    </div>
    <span class="LinkArrow">&#0187;</span>
    <orion:HelpLink ID="DiscoveryHelpLink" runat="server" HelpUrlFragment="OrionPHDiscoveryHome"
        HelpDescription="<%$ Resources: InterfacesWebContent, NPMWEBDATA_TM0_86%>" CssClass="helpLink" />
        
    <orion:ManagedNetObjectsInfo ID="nodesInfo" runat="server" EntityName="<%$ Resources: InterfacesWebContent, NPMWEBDATA_AK0_8%>" NumberOfElements="<%# InterfaceCount %>" />    
    
    <div class="sw-btn-bar">
        <orion:LocalizableButton runat="server" ID="discoverNetworkButton" LocalizedText="DiscoverNodes" DisplayType="Secondary" 
                                 OnClick="Button_Click" CssClass="<%# GetCssClass(InterfaceCount) %>" CommandArgument="~/Orion/Discovery/Default.aspx" />
    </div>
</div>
