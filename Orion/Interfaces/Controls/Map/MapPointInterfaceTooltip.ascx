﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MapPointInterfaceTooltip.ascx.cs" Inherits="Orion_Interfaces_Controls_Map_MapPointInterfaceTooltip" %>

<p class="StatusDescription"><%= InterfaceStatus %></p>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<% if (HasVlans)
   { %>
    <tr>
        <th width="auto" nowrap="nowrap"><%=Resources.InterfacesWebContent.NPMWEBDATA_GK0_9 %></th>
        <td><span id="portType"><%= PortMode %></span></td>
    </tr>
    <tr>
        <th width="auto" nowrap="nowrap"><%=Resources.InterfacesWebContent.NPMWEBDATA_GK0_10 %></th>
        <td><span id="vlans"><%= Vlans %></span></td>
    </tr>
<% } %>
<%= ChildObjects %>
</table>