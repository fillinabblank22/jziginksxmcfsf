﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using SolarWinds.Interfaces.Web;
using SolarWinds.Orion.Core.Models.Enums;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.Map;

public partial class Orion_Interfaces_Controls_Map_MapPointInterfaceTooltip : MapPointTooltipControlBase
{
    protected String InterfaceStatus;
    protected bool HasVlans;
    protected String PortMode;
    protected String Vlans;
    protected String ChildObjects;

    protected void Page_Load(object sender, EventArgs e)
    {
        var status = Resources.CoreWebContent.Status_Unknown;
        const string statusQuery = "SELECT StatusDescription, NodeID, Index FROM Orion.NPM.Interfaces WHERE InterfaceID = @InterfaceID";

        var ifIndex = 0;
        var nodeId = 0;
        using (var swis = InformationServiceProxy.CreateV3())
        {
            var result = swis.Query(statusQuery, new Dictionary<string, object> {{"InterfaceID", int.Parse(TooltipParams.EntityKey)}});
            if (result != null && result.Rows.Count > 0)
            {
                status = (String) result.Rows[0]["StatusDescription"];
                nodeId = (int)result.Rows[0]["NodeID"];
                ifIndex = (int)result.Rows[0]["Index"];
            }
        }

        var netObjectData = new JavaScriptSerializer().Deserialize<Dictionary<string,string>>(TooltipParams.NetObjectData);
        var childStatus = netObjectData.ContainsKey("ChildStatus") ? netObjectData["ChildStatus"] : String.Empty;
        ChildObjects = netObjectData.ContainsKey("Metrics") ? netObjectData["Metrics"] : String.Empty;
        
        InterfaceStatus = String.IsNullOrEmpty(childStatus) 
            ? String.Format(Resources.InterfacesWebContent.NPMWEBDATA_PS1_8, status)
            : String.Format(Resources.InterfacesWebContent.NPMWEBDATA_PS1_10, status, childStatus);

        if (nodeId > 0)
        {
            HasVlans = VlanInterfaceHelper.IsInterfaceInVlan(nodeId, ifIndex);
            var portType = HasVlans ? VlanInterfaceHelper.GetPortType(nodeId, ifIndex) : VlanPortType.Unknown;
            PortMode = VlanInterfaceHelper.TranslatePortType(portType);
            Vlans = VlanInterfaceHelper.GenerateVlansString(portType, VlanInterfaceHelper.GetVlansByType(nodeId, ifIndex));
        }
        
    }
}