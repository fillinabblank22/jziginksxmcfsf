﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MapEdgeInterfaceTooltip.ascx.cs" Inherits="Orion_Interfaces_Controls_Map_MapEdgeInterfaceTooltip" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="SolarWinds.Orion.NPM.Web" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" Assembly="OrionWeb" %>

<table cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
<% for (int i = 0; i < TooltipData.Count(); i++)
   {
       if (i > 0)
       {
           // we will add horizontal line between individual tooltip infos
%>
            <tr><td colspan="4" style="padding-top: 8px; padding-bottom: 0px;"></td></tr>
            <tr style="height: 4px;"><td colspan="4" style="border-top: 1px solid #ebebeb; padding-top: 1px; padding-bottom: 0px;"></td></tr>
    <% }
       if ((TooltipData.ElementAt(i).StartComesFrom != SolarWinds.Interfaces.Web.Map.InterfaceEdgeSourceType.ShadowNode) || (TooltipData.ElementAt(i).EndComesFrom != SolarWinds.Interfaces.Web.Map.InterfaceEdgeSourceType.ShadowNode))
       {
           var key = TooltipData.ElementAt(i).StartNodeName + "|" + TooltipData.ElementAt(i).EndNodeName;
           if (!AlreadyDisplayedNodeNameTuples.Contains(key))
           {
               AlreadyDisplayedNodeNameTuples.Add(key);
    %>
          <tr>
              <td style="white-space: nowrap; padding-top: 1px; padding-bottom: 0px;"><%= Resources.InterfacesWebContent.NPMWEBDATA_PS0_1 %></td>
              <td style='padding-left: 0.3em; padding-right: 0.3em; white-space: nowrap; padding-top: 1px; padding-bottom: 0px;'><img src="<%= StartNodeStatusIconPath.ElementAt(i) %>" style="height: 15px;" />&nbsp;<b><%= (TooltipData.ElementAt(i).StartComesFrom != SolarWinds.Interfaces.Web.Map.InterfaceEdgeSourceType.ShadowNode) ? TooltipData.ElementAt(i).StartNodeName : Resources.InterfacesWebContent.NPMWEBDATA_PS0_16 %></b></td>
              <td style="padding-top: 1px; padding-bottom: 0px;">&nbsp;</td>
              <td style='padding-left: 0.3em; white-space: nowrap; padding-top: 1px; padding-bottom: 0px;'><img src="<%= EndNodeStatusIconPath.ElementAt(i) %>" style="height: 15px;" />&nbsp;<b><%= (TooltipData.ElementAt(i).EndComesFrom != SolarWinds.Interfaces.Web.Map.InterfaceEdgeSourceType.ShadowNode) ? TooltipData.ElementAt(i).EndNodeName : Resources.InterfacesWebContent.NPMWEBDATA_PS0_16 %></b></td>
          </tr>
    <% } %>
    <tr>
        <td style="white-space: nowrap; padding-top: 1px; padding-bottom: 0px;"><%= Resources.InterfacesWebContent.NPMWEBDATA_PS0_2 %></td>
        <td style='padding-left: 0.3em; padding-right: 0.3em; padding-top: 1px; padding-bottom: 0px; border-right: 1px solid #A1A1A1'><%= TooltipData.ElementAt(i).StartInterfaceIndex.HasValue ? TooltipData.ElementAt(i).StartInterfaceIndex.Value.ToString() : Resources.InterfacesWebContent.NPMWEBDATA_PS0_9 %></td>
        <td style='border-right: 1px solid #A1A1A1; padding-top: 1px; padding-bottom: 0px;'>&nbsp;</td><td style='padding-left: 0.3em; padding-top: 1px; padding-bottom: 0px;'><%= TooltipData.ElementAt(i).EndInterfaceIndex.HasValue ? TooltipData.ElementAt(i).EndInterfaceIndex.Value.ToString() : Resources.InterfacesWebContent.NPMWEBDATA_PS0_9 %></td>
    </tr>
    <tr>
        <td style="white-space: nowrap; padding-top: 1px; padding-bottom: 0px;"><%= Resources.InterfacesWebContent.NPMWEBDATA_PS0_3 %></td>
        <td style='padding-left: 0.3em; padding-right: 0.3em; padding-top: 1px; padding-bottom: 0px; border-right: 1px solid #A1A1A1; white-space: nowrap;'>
            <% if ((TooltipData.ElementAt(i).StartComesFrom != SolarWinds.Interfaces.Web.Map.InterfaceEdgeSourceType.ShadowNode) && (TooltipData.ElementAt(i).StartComesFrom != SolarWinds.Interfaces.Web.Map.InterfaceEdgeSourceType.Node))
               { %>
                    <img src="<%= StartInterfaceStatusIconPath.ElementAt(i) %>" style="height: 15px;" />&nbsp;<%= TooltipData.ElementAt(i).StartInterfaceName %>
            <% }
               else
               { %>
                    <%= Resources.InterfacesWebContent.NPMWEBDATA_PS0_9 %>
            <% } %>
        </td>
        <td style='border-right: 1px solid grey; text-align: center; white-space: nowrap; padding-left: 3px; padding-top: 1px; padding-bottom: 0px;'><%= (TooltipData.ElementAt(i).LayerType == SolarWinds.Interfaces.Web.Enums.TopologyLayerType.L3) ? LayerType.ElementAt(i) : "&nbsp;" %></td>
        <td style='padding-left: 0.3em; white-space: nowrap; padding-top: 1px; padding-bottom: 0px;'>
            <% if ((TooltipData.ElementAt(i).EndComesFrom != SolarWinds.Interfaces.Web.Map.InterfaceEdgeSourceType.ShadowNode) && (TooltipData.ElementAt(i).EndComesFrom != SolarWinds.Interfaces.Web.Map.InterfaceEdgeSourceType.Node))
               { %>
                    <img src="<%= EndInterfaceStatusIconPath.ElementAt(i) %>" style="height: 15px;" />&nbsp;<%= TooltipData.ElementAt(i).EndInterfaceName %>
            <% }
               else
               { %>
                <%= Resources.InterfacesWebContent.NPMWEBDATA_PS0_9 %>
            <% } %>
        </td>
    </tr>
    <tr style="display: none;"><td style="white-space: nowrap; padding-top: 1px; padding-bottom: 0px;"><%= Resources.InterfacesWebContent.NPMWEBDATA_PS0_7 %></td><td style='padding-left: 0.3em; padding-right: 0.3em; border-right: 1px solid #A1A1A1; white-space: nowrap; padding-top: 1px; padding-bottom: 0px;'><%= string.Format(Resources.InterfacesWebContent.NPMWEBDATA_PS0_10, (int) Math.Round(TooltipData.ElementAt(i).StartInterfaceTrafficInUtilization), 0) %></td><td style='border-right: 1px solid #A1A1A1; padding-top: 1px;'>&nbsp;</td><td style='padding-left: 0.3em; white-space: nowrap; padding-top: 1px; padding-bottom: 0px;'><%= string.Format(Resources.InterfacesWebContent.NPMWEBDATA_PS0_10, (int) Math.Round(TooltipData.ElementAt(i).EndInterfaceTrafficInUtilization), 0) %></td></tr>
    <tr style="display: none;"><td style="white-space: nowrap; padding-top: 1px; padding-bottom: 0px;"><%= Resources.InterfacesWebContent.NPMWEBDATA_PS0_8 %></td><td style='padding-left: 0.3em; padding-right: 0.3em; border-right: 1px solid #A1A1A1; white-space: nowrap; padding-top: 1px; padding-bottom: 0px;'><%= string.Format(Resources.InterfacesWebContent.NPMWEBDATA_PS0_10, (int) Math.Round(TooltipData.ElementAt(i).StartInterfaceTrafficOutUtilization), 0) %></td><td style='border-right: 1px solid #A1A1A1; padding-bottom: 0px;'>&nbsp;</td><td style='padding-left: 0.3em; white-space: nowrap; padding-top: 1px; padding-bottom: 0px;'><%= string.Format(Resources.InterfacesWebContent.NPMWEBDATA_PS0_10, (int) Math.Round(TooltipData.ElementAt(i).EndInterfaceTrafficOutUtilization, 0)) %></td></tr>
    
    <tr>
        <td style="white-space: nowrap; padding-top: 1px; padding-bottom: 0px;"><%= Resources.InterfacesWebContent.NPMWEBDATA_PS0_4 %></td>
        <td style='padding-left: 0.3em; padding-right: 0.3em; border-right: 1px solid #A1A1A1; padding-top: 1px; padding-bottom: 0px;'><%= !TooltipData.ElementAt(i).StartPortNumber.HasValue ? Resources.InterfacesWebContent.NPMWEBDATA_PS0_9 : TooltipData.ElementAt(i).StartPortNumber.Value.ToString() %></td>
        <td style='border-right: 1px solid #A1A1A1; padding-right: 0px; padding-left: 0px; padding-top: 1px; padding-bottom: 0px;'>
            <table style="border-collapse: collapse;">
                <tr>
                    <% if (TooltipData.ElementAt(i).LayerType == SolarWinds.Interfaces.Web.Enums.TopologyLayerType.L2)
                       { %>
                            <td style="padding: 0px; padding-top: 1px;"><img src="/Orion/Interfaces/Images/Map/Left_Arrow.png" /></td>
                            <td style="padding-left: 7px; padding-right:6px;"><div style="width: 10px; height: 10px; background-color: <%= GetColorByLinkSpeed(TooltipData.ElementAt(i).EdgeSpeed) %>; padding-top: 0px; padding-bottom: 0px;"></div></td>
                            <td style="white-space: nowrap; text-align: center; padding-top: 0px; padding-bottom: 0px;"><%= EdgeSpeed.ElementAt(i) %></td>
                            <td style="padding: 0px; padding-top: 2px;"><img src="/Orion/Interfaces/Images/Map/Right_Arrow.png" style="margin-right: -1px" /></td>
                    <% }
                       else if (TooltipData.ElementAt(i).LayerType == SolarWinds.Interfaces.Web.Enums.TopologyLayerType.Custom)
                       { %>
                            <td style="padding: 0px" colspan="2"></td>
                            <td style="white-space: nowrap; text-align: center; padding-top: 1px; padding-bottom: 0px;"><%= Resources.InterfacesWebContent.NPMWEBDATA_JM0_0 %></td>
                            <td style="padding: 0px;"></td>
                    <% }
                       else
                       { %>
                            <td style="padding: 0px" colspan="2"></td>
                            <td style="white-space: nowrap; text-align: center; padding-top: 1px; padding-bottom: 0px;">&nbsp;</td>
                            <td style="padding: 0px;"></td>
                    <% } %>
                </tr>
            </table>
        </td>
        <td style='padding-left: 0.3em; padding-top: 1px; padding-bottom: 0px;'><%= !TooltipData.ElementAt(i).EndPortNumber.HasValue ? Resources.InterfacesWebContent.NPMWEBDATA_PS0_9 : TooltipData.ElementAt(i).EndPortNumber.Value.ToString() %></td>
    </tr>
    <% if (TooltipData.ElementAt(i).LayerType == SolarWinds.Interfaces.Web.Enums.TopologyLayerType.L2)
       { %>
    <tr>
        <td style="white-space: nowrap;"><%= Resources.InterfacesWebContent.NPMWEBDATA_PS0_5 %></td>
        <td style='padding-left: 0.3em; padding-right: 0.3em; border-right: 1px solid #A1A1A1; padding-top: 1px; padding-bottom: 0px;'><%= (TooltipData.ElementAt(i).StartComesFrom != SolarWinds.Interfaces.Web.Map.InterfaceEdgeSourceType.ShadowNode) ? TooltipData.ElementAt(i).StartSTPDetails.ToString() : Resources.InterfacesWebContent.NPMWEBDATA_PS0_9 %></td>
        <td style='border-right: 1px solid #A1A1A1; text-align: center; padding-top: 1px; padding-bottom: 0px;'></td>
        <td style='padding-left: 0.3em; padding-top: 1px; padding-bottom: 0px;'><%= (TooltipData.ElementAt(i).EndComesFrom != SolarWinds.Interfaces.Web.Map.InterfaceEdgeSourceType.ShadowNode) ? TooltipData.ElementAt(i).EndSTPDetails.ToString() : Resources.InterfacesWebContent.NPMWEBDATA_PS0_9 %></td>
    </tr>
    <tr><td style="white-space: nowrap; padding-top: 1px; padding-bottom: 0px;"><%= Resources.InterfacesWebContent.NPMWEBDATA_PS0_6 %></td><td style='padding-left: 0.3em; padding-right: 0.3em; padding-top: 1px; padding-bottom: 0px; border-right: 1px solid #A1A1A1;'><%= string.IsNullOrEmpty(StartInterfaceVlanIds.ElementAt(i)) ? Resources.InterfacesWebContent.NPMWEBDATA_PS0_9 : StartInterfaceVlanIds.ElementAt(i) %></td><td style='border-right: 1px solid #A1A1A1; padding-top: 1px; padding-bottom: 0px;'>&nbsp;</td><td style='padding-left: 0.3em; padding-top: 1px; padding-bottom: 0px;'><%= string.IsNullOrEmpty(EndInterfaceVlanIds.ElementAt(i)) ? Resources.InterfacesWebContent.NPMWEBDATA_PS0_9 : EndInterfaceVlanIds.ElementAt(i) %></td></tr>
    <% } %>
    <!-- here begin interface utilization block -->
    <% if (TooltipData.ElementAt(i).LayerType != SolarWinds.Interfaces.Web.Enums.TopologyLayerType.L3)
       { %>
    <tr>
        <td style="padding-top: 1px; padding-bottom: 0px;">
            <table style="border-collapse: collapse;">
               <tr>
                   <td style="white-space: nowrap; padding-top: 1px; padding-bottom: 0px;"><%= Resources.InterfacesWebContent.NPMWEBDATA_PS0_14 %></td>
               </tr>
                <tr>
                    <td style="white-space: nowrap; padding-top: 1px; padding-bottom: 0px;"><%= Resources.InterfacesWebContent.NPMWEBDATA_PS0_15 %></td>
                </tr> 
            </table>
        </td>
        <td style='padding-left: 0.3em; padding-right: 0.3em; border-right: 1px solid #A1A1A1; padding-top: 1px; padding-bottom: 0px;'>
            <% if (TooltipData.ElementAt(i).StartInterfaceIndex.HasValue) // If interface index is unknown therefore it means that we don't have any info about interface and we can't show Rx, Tx and utilization
               { %>
            <table style="border-collapse: collapse;">
                <tr class="InlineMultiBar">
                    <td style="white-space: nowrap; padding-top: 1px; padding-bottom: 0px;" class="<%= GetTrafficRowClassNameByValue(TooltipData.ElementAt(i).StartInterfaceTrafficInUtilization) %>"><%= string.Format(Resources.InterfacesWebContent.NPMWEBDATA_PS0_13, GetFormattedTrafficbpsValue(TooltipData.ElementAt(i).StartInterfaceTrafficReceive), (int) TooltipData.ElementAt(i).StartInterfaceTrafficInUtilization) %></td>
                    <%
                        var ctrlSInU = new MyInlineBar();
                        ctrlSInU.Style.Add("width", "35px");
                        ctrlSInU.Style.Add("height", "10px");
                        SetupBar(ctrlSInU, TooltipData.ElementAt(i).StartInterfaceTrafficInUtilization, 100, Thresholds.IfPercentUtilizationWarning.SettingValue, Thresholds.IfPercentUtilizationError.SettingValue);
                        Response.Write(GetRenderedBar(ctrlSInU));
                    %> 
                    <td style="width: 100%; padding-top: 1px; padding-bottom: 0px;"></td>
                </tr>
                <tr class="InlineMultiBar">
                    <td style="white-space: nowrap; padding-top: 1px; padding-bottom: 0px;" class="<%= GetTrafficRowClassNameByValue(TooltipData.ElementAt(i).StartInterfaceTrafficOutUtilization) %>"><%= string.Format(Resources.InterfacesWebContent.NPMWEBDATA_PS0_12, GetFormattedTrafficbpsValue(TooltipData.ElementAt(i).StartInterfaceTrafficTransmit), (int) TooltipData.ElementAt(i).StartInterfaceTrafficOutUtilization) %></td>
                    <%
                        var ctrlSOutU = new MyInlineBar();
                        ctrlSOutU.Style.Add("width", "35px");
                        ctrlSOutU.Style.Add("height", "10px");
                        SetupBar(ctrlSOutU, TooltipData.ElementAt(i).StartInterfaceTrafficOutUtilization, 100, Thresholds.IfPercentUtilizationWarning.SettingValue, Thresholds.IfPercentUtilizationError.SettingValue);
                        Response.Write(GetRenderedBar(ctrlSOutU));
                    %> 
                    <td style="width: 100%; padding-top: 1px; padding-bottom: 0px;"></td>
                </tr>
            </table>
            <% }
               else
               { %>
                    <table style="border-collapse: collapse;">
                        <tr><td style="padding-top: 1px; padding-bottom: 0px;"><%= Resources.InterfacesWebContent.NPMWEBDATA_PS0_9 %></td></tr>
                        <tr><td style="padding-top: 1px; padding-bottom: 0px;"><%= Resources.InterfacesWebContent.NPMWEBDATA_PS0_9 %></td></tr>
                    </table>
            <% } %>
        </td>
        <td style="padding-top: 1px; padding-bottom: 0px;">&nbsp;</td>
        <td style='padding-left: 0.3em; padding-right: 0.3em; border-left: 1px solid #A1A1A1; padding-top: 1px; padding-bottom: 0px;'>
           <% if (TooltipData.ElementAt(i).EndInterfaceIndex.HasValue) // If interface index is unknown therefore it means that we don't have any info about interface and we can't show Rx, Tx and utilization
              { %>
            <table style="border-collapse: collapse;">
                <tr class="InlineMultiBar">
                    <td style="white-space: nowrap; padding-top: 1px; padding-bottom: 0px;" class="<%= GetTrafficRowClassNameByValue(TooltipData.ElementAt(i).EndInterfaceTrafficInUtilization) %>"><%= string.Format(Resources.InterfacesWebContent.NPMWEBDATA_PS0_13, GetFormattedTrafficbpsValue(TooltipData.ElementAt(i).EndInterfaceTrafficReceive), (int) TooltipData.ElementAt(i).EndInterfaceTrafficInUtilization) %></td>
                    <%
                        var ctrlEInU = new MyInlineBar();
                        ctrlEInU.Style.Add("width", "35px");
                        ctrlEInU.Style.Add("height", "10px");
                        SetupBar(ctrlEInU, TooltipData.ElementAt(i).EndInterfaceTrafficInUtilization, 100, Thresholds.IfPercentUtilizationWarning.SettingValue, Thresholds.IfPercentUtilizationError.SettingValue);
                        Response.Write(GetRenderedBar(ctrlEInU));
                    %>
                    <td style="width: 100%; padding-top: 1px; padding-bottom: 0px;"></td>
                </tr>
                <tr class="InlineMultiBar">
                    <td style="white-space: nowrap; padding-top: 1px; padding-bottom: 0px;" class="<%= GetTrafficRowClassNameByValue(TooltipData.ElementAt(i).EndInterfaceTrafficOutUtilization) %>"><%= string.Format(Resources.InterfacesWebContent.NPMWEBDATA_PS0_12, GetFormattedTrafficbpsValue(TooltipData.ElementAt(i).EndInterfaceTrafficTransmit), (int) TooltipData.ElementAt(i).EndInterfaceTrafficOutUtilization) %></td>
                    <%
                        var ctrlEOutU = new MyInlineBar();
                        ctrlEOutU.Style.Add("width", "35px");
                        ctrlEOutU.Style.Add("height", "10px");
                        SetupBar(ctrlEOutU, TooltipData.ElementAt(i).EndInterfaceTrafficOutUtilization, 100, Thresholds.IfPercentUtilizationWarning.SettingValue, Thresholds.IfPercentUtilizationError.SettingValue);
                        Response.Write(GetRenderedBar(ctrlEOutU));
                    %>
                    <td style="width: 100%; padding-top: 1px; padding-bottom: 0px;"></td>
                </tr>
            </table>
            <% }
              else
              { %>
                <table style="border-collapse: collapse;">
                    <tr>
                        <td style="padding-top: 1px; padding-bottom: 0px;"><%= Resources.InterfacesWebContent.NPMWEBDATA_PS0_9 %></td>
                    </tr>
                    <tr>
                        <td style="padding-top: 1px; padding-bottom: 0px;"><%= Resources.InterfacesWebContent.NPMWEBDATA_PS0_9 %></td>
                    </tr>
                </table>
            <% } %>
        </td>
    </tr>
    <% } %>
    <!-- here ends interface utilization block -->
<% } // if ((TooltipData.ElementAt(i).StartComesFrom != SolarWinds.Interfaces.Web.Map.InterfaceEdgeSourceType.ShadowNode) || (TooltipData.ElementAt(i).EndComesFrom != SolarWinds.Interfaces.Web.Map.InterfaceEdgeSourceType.ShadowNode)) 
   else { %>
    <tr><td colspan="4" style="padding-bottom: 0px; height: 4px;"><%= Resources.InterfacesWebContent.NPMWEBDATA_PS0_17 %></td></tr>
<%   }
    }
   if (TooltipDataWasLimited)
   { %>
    <tr><td colspan="4" style="padding-top: 2em;"><span><%= string.Format(Resources.InterfacesWebContent.NPMWEBDATA_PS0_11, NumberOfHidenTooltips) %></span></td></tr>
<% } if (DoesntExistConnections) { %>
    <tr><td colspan="4" style="padding-top: 4px; white-space: nowrap;"><span><%= DoesntExistsConnectionsMessage %></span></td></tr>
  <% } %>
</table>


<% if (RedefineStatusInfo) { %>
<statusinfo status="StatusUnknown" />
<% } %>