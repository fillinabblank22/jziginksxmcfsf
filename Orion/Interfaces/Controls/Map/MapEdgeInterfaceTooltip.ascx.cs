﻿using System.Drawing;
using System.Text;
using System.Web.UI.HtmlControls;
using SolarWinds.Interfaces.Web.DAL;
using SolarWinds.Interfaces.Web.Map;
using SolarWinds.Internationalization.Extensions;
using SolarWinds.MapEngine.Data;
using SolarWinds.MapEngine.Model;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.Map;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Interfaces_Controls_Map_MapEdgeInterfaceTooltip : MapEdgeTooltipControlBase
{
    /// <summary>
    /// Class serve for rendering progress bar. Note we need to do inherit from InlineBar, because we render it into memory and therefore
    /// we need to ensure call EnsureChildControls method to correctly render it.
    /// </summary>
    public class MyInlineBar : InlineBar
    {
        /// <summary>
        /// Outputs server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter" /> object and stores tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter" /> object that receives the control content.</param>
        public override void RenderControl(HtmlTextWriter writer)
        {
            EnsureChildControls();
            if ((Controls.Count > 0) && ((Controls[0] as HtmlGenericControl) != null))
            {
                (Controls[0] as HtmlGenericControl).Style.Add("width", "35px");
                (Controls[0] as HtmlGenericControl).Style.Add("height", "10px");
            }

            base.RenderControl(writer);
        }
    }

    private readonly string statusIconsBasePath = "/Orion/images/StatusIcons/";
    private readonly string unknownStatusIconPath = "/Orion/images/StatusIcons/Small-Unknown.gif";

    protected bool RedefineStatusInfo { get; set; }

    protected bool DoesntExistConnections { get; set; }
    protected string DoesntExistsConnectionsMessage { get; set; }

    private IEnumerable<InterfaceEdgeTooltipData> tooltipData = null;
    protected IEnumerable<InterfaceEdgeTooltipData> TooltipData
    {
        get
        {
            return tooltipData ?? (tooltipData = new List<InterfaceEdgeTooltipData>());
        }
        set
        {
            tooltipData = value;
        }
    }

    private HashSet<string> alreadyDisplayedNodeNameTuples = null;
    protected HashSet<string> AlreadyDisplayedNodeNameTuples
    {
        get { return alreadyDisplayedNodeNameTuples ?? (alreadyDisplayedNodeNameTuples = new HashSet<string>()); }
    }

    private List<string> startNodeStatusIconPath = null;
    protected IEnumerable<string> StartNodeStatusIconPath
    {
        get
        {
            if ((startNodeStatusIconPath == null) && (TooltipData != null))
            {
                startNodeStatusIconPath = new List<string>();
                for (int i = 0; i < TooltipData.Count(); i++)
                {
                    var curElement = TooltipData.ElementAt(i);
                    if (!string.IsNullOrEmpty(curElement.StartNodeGroupStatus) && (curElement.StartComesFrom != InterfaceEdgeSourceType.ShadowNode))
                    {
                        startNodeStatusIconPath.Add(CombineStatusIconUrlPath(statusIconsBasePath, TooltipData.ElementAt(i).StartNodeGroupStatus));
                    }
                    else
                    {
                        startNodeStatusIconPath.Add(unknownStatusIconPath);
                    }
                }
            }

            return startNodeStatusIconPath;
        }
    }

    private List<string> endNodeStatusIconPath = null;
    protected IEnumerable<string> EndNodeStatusIconPath
    {
        get
        {
            if ((endNodeStatusIconPath == null) && (TooltipData != null))
            {
                endNodeStatusIconPath = new List<string>();
                for (int i = 0; i < TooltipData.Count(); i++)
                {
                    var curElement = TooltipData.ElementAt(i);
                    if (!string.IsNullOrEmpty(curElement.EndNodeGroupStatus) && (curElement.EndComesFrom != InterfaceEdgeSourceType.ShadowNode))
                    {
                        endNodeStatusIconPath.Add(CombineStatusIconUrlPath(statusIconsBasePath, TooltipData.ElementAt(i).EndNodeGroupStatus));
                    }
                    else
                    {
                        endNodeStatusIconPath.Add(unknownStatusIconPath);
                    }
                }
            }

            return endNodeStatusIconPath;
        }
    }

    private List<string> startInterfaceStatusIconPath = null;
    protected IEnumerable<string> StartInterfaceStatusIconPath
    {
        get
        {
            if ((startInterfaceStatusIconPath == null) && (TooltipData != null))
            {
                startInterfaceStatusIconPath = new List<string>();
                for (int i = 0; i < TooltipData.Count(); i++)
                {
                    if (!string.IsNullOrEmpty(TooltipData.ElementAt(i).StartInterfaceIconStatus))
                    {
                        startInterfaceStatusIconPath.Add(CombineStatusIconUrlPath(statusIconsBasePath, TooltipData.ElementAt(i).StartInterfaceIconStatus));
                    }
                    else
                    {
                        startInterfaceStatusIconPath.Add(unknownStatusIconPath);
                    }
                }
            }

            return startInterfaceStatusIconPath;
        }
    }

    private List<string> startInterfaceVlanIds = null;
    protected IEnumerable<string> StartInterfaceVlanIds
    {
        get
        {
            if ((startInterfaceVlanIds == null) && (TooltipData != null))
            {
                startInterfaceVlanIds = new List<string>();
                for (int i = 0; i < TooltipData.Count(); i++)
                {
                    if ((TooltipData.ElementAt(i).StartVlans != null) && (TooltipData.ElementAt(i).StartVlans.Any()))
                    {
                        startInterfaceVlanIds.Add(FormatVlansListForDisplayInTooltip(TooltipData.ElementAt(i).StartVlans));
                    }
                    else
                    {
                        startInterfaceVlanIds.Add(string.Empty);
                    }
                }
            }

            return startInterfaceVlanIds;
        }
    }

    private List<string> endInterfaceStatusIconPath = null;
    protected IEnumerable<string> EndInterfaceStatusIconPath
    {
        get
        {
            if ((endInterfaceStatusIconPath == null) && (TooltipData != null))
            {
                endInterfaceStatusIconPath = new List<string>();
                for (int i = 0; i < TooltipData.Count(); i++)
                {
                    if (!string.IsNullOrEmpty(TooltipData.ElementAt(i).EndInterfaceIconStatus))
                    {
                        endInterfaceStatusIconPath.Add(CombineStatusIconUrlPath(statusIconsBasePath, TooltipData.ElementAt(i).EndInterfaceIconStatus));
                    }
                    else
                    {
                        endInterfaceStatusIconPath.Add(unknownStatusIconPath);
                    }
                }
            }

            return endInterfaceStatusIconPath;
        }
    }

    private List<string> endInterfaceVlanIds = null;
    protected IEnumerable<string> EndInterfaceVlanIds
    {
        get
        {
            if ((endInterfaceVlanIds == null) && (TooltipData != null))
            {
                endInterfaceVlanIds = new List<string>();
                for (int i = 0; i < TooltipData.Count(); i++)
                {
                    if ((TooltipData.ElementAt(i).EndVlans != null) && (TooltipData.ElementAt(i).EndVlans.Any()))
                    {
                        endInterfaceVlanIds.Add(FormatVlansListForDisplayInTooltip(TooltipData.ElementAt(i).EndVlans));
                    }
                    else
                    {
                        endInterfaceVlanIds.Add(string.Empty);
                    }
                }
            }

            return endInterfaceVlanIds;
        }
    }

    private List<string> layerType = null;
    protected IEnumerable<string> LayerType
    {
        get
        {
            if ((layerType == null) && (TooltipData != null))
            {
                layerType = new List<string>();
                for (int i = 0; i < TooltipData.Count(); i++)
                {
                    if (TooltipData.ElementAt(i).LayerType.HasValue)
                        layerType.Add(TooltipData.ElementAt(i).LayerType.LocalizedLabel()); 
                    else
                        layerType.Add(Resources.InterfacesWebContent.NPMWEBDATA_PS0_9);
                }
            }

            return layerType;
        }
    }

    private List<string> edgeSpeed = null;
    protected IEnumerable<string> EdgeSpeed
    {
        get
        {
            if ((edgeSpeed == null) && (TooltipData != null))
            {
                edgeSpeed = new List<string>();
                for (int i = 0; i < TooltipData.Count(); i++)
                {
                    var item = TooltipData.ElementAt(i);
                    if (item.EdgeSpeed.HasValue)
                    {
                        if (item.EdgeSpeed >= (10 * 1000))
                        {
                            edgeSpeed.Add(Resources.InterfacesWebContent.NPMWEBCODE_PS0_1);
                        }
                        else if (item.EdgeSpeed >= (1000))
                        {
                            edgeSpeed.Add(Resources.InterfacesWebContent.NPMWEBCODE_PS0_2);
                        }
                        else if (item.EdgeSpeed >= (100))
                        {
                            edgeSpeed.Add(Resources.InterfacesWebContent.NPMWEBCODE_PS0_3);
                        }
                        else if (item.EdgeSpeed >= (10))
                        {
                            edgeSpeed.Add(Resources.InterfacesWebContent.NPMWEBCODE_PS0_4);
                        }
                        else if (item.EdgeSpeed >= (1.5))
                        {
                            edgeSpeed.Add(Resources.InterfacesWebContent.NPMWEBCODE_PS0_5);
                        }
                        else
                        {
                            edgeSpeed.Add(Resources.InterfacesWebContent.NPMWEBCODE_PS0_6);
                        }
                    }
                    else
                    {
                        edgeSpeed.Add(Resources.InterfacesWebContent.NPMWEBDATA_PS0_9);
                    }
                }
            }

            return edgeSpeed;
        }
    }

    protected bool TooltipDataWasLimited { get; set; }

    protected int NumberOfHidenTooltips { get; set; }

    /// <summary>
    /// Method initialize
    /// </summary>
    /// <param name="bar">Percent bar which we initilize</param>
    /// <param name="value">Current value</param>
    /// <param name="max">Max possible value</param>
    /// <param name="warning">Value which state threshold when if value in parameter value is greater, bar should be switched to warning state</param>
    /// <param name="error">When parameter value reaches error value, bar should be switched to error state.</param>
    protected void SetupBar(MyInlineBar bar, double value, double max, double warning, double error)
    {
        if (value < 0)
            value = 0;

        bar.Percentage = Math.Min(100.0 * value / max, 100);
        if (value >= error)
            bar.ThresholdLevelValue = InlineBar.ThresholdLevel.Error;
        else if (value > warning)
            bar.ThresholdLevelValue = InlineBar.ThresholdLevel.Warning;
        else
            bar.ThresholdLevelValue = InlineBar.ThresholdLevel.Normal;
    }

    protected string GetRenderedBar(MyInlineBar bar)
    {
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);
        HtmlTextWriter writer = new HtmlTextWriter(sw);

        bar.RenderControl(writer);
        return sb.ToString();
    }

    protected string GetTrafficRowClassNameByValue(double value)
    {
        if (Thresholds.IfPercentUtilizationError.SettingValue <= value)
            return "Error";
        else if (Thresholds.IfPercentUtilizationWarning.SettingValue <= value)
            return "Warning";

        return string.Empty;
    }

    protected string GetFormattedTrafficbpsValue(double value)
    {
        string res = string.Empty;
        if (value > (1000*1000*1000)) // Gbps
        {
            return string.Format(Resources.InterfacesWebContent.NPMWEBCODE_PS0_7, Math.Round(value/(1000*1000*1000), 3));
        }
        else if (value > (1000*1000)) // Mbps
        {
            return string.Format(Resources.InterfacesWebContent.NPMWEBCODE_PS0_8, Math.Round(value/(1000*1000), 3));
        }
        else if (value > 1000) // Kbps
        {
            return string.Format(Resources.InterfacesWebContent.NPMWEBCODE_PS0_9, Math.Round(value/1000, 3));
        }
        else
        {
            return string.Format(Resources.InterfacesWebContent.NPMWEBCODE_PS0_10, Math.Round(value, 3));
        }

        return res;
    }

    /// <summary>
    /// Return color by passed linkSpeed parameter which can be used as parameter to css style.
    /// </summary>
    /// <param name="linkSpeed">This parameter is linkSpeed in Mbps.</param>
    /// <returns>Color which can be used in css style.</returns>
    protected string GetColorByLinkSpeed(double? linkSpeed)
    {
        var items = MapService.GetMapLegend(SolarWinds.MapEngine.Enums.MapStyle.Topology).Items.Cast<MapLegendLinkSpeedItem>().OrderBy(item => item.ThresholdFrom).ToArray();
        linkSpeed = linkSpeed.HasValue ? linkSpeed : 0.0001;
        for (int i = items.Length - 1; i >= 0; i--)
        {
            if (items[i].ThresholdFrom <= linkSpeed)
            {
                return ColorTranslator.ToHtml(items[i].Color);
            }
        }

        return ColorTranslator.ToHtml(items[0].Color);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var mapTooltipDataProvider = new MapInterfaceEdgeTooltipDAL();
        if ((TooltipParams.StartPointEntityIds != null) && (TooltipParams.EndPointEntityIds != null))
        {
            TooltipData = mapTooltipDataProvider.GetTooltipDataForEdge(TooltipParams.StartPointEntityName, TooltipParams.EndPointEntityName, TooltipParams.StartPointEntityIds, TooltipParams.EndPointEntityIds);
            int numberOfRows = TooltipData.Count();
            TooltipData =
                TooltipData.OrderByDescending(
                    item =>
                    Math.Max(Math.Max(item.StartInterfaceTrafficInUtilization, item.EndInterfaceTrafficInUtilization),
                             Math.Max(item.StartInterfaceTrafficOutUtilization, item.EndInterfaceTrafficOutUtilization)))
                           .Take(4);

            int tDataCount = TooltipData.Count();
            TooltipDataWasLimited = numberOfRows > tDataCount;
            NumberOfHidenTooltips = numberOfRows - tDataCount;
           
            if (!TooltipData.Any(item => item.LayerType.HasValue))
            { // case when we for edge don't have connection in topologyconnection table
                RedefineStatusInfo = true;
                if (TooltipData.Any())
                {
                    DoesntExistConnections = true;
                    DoesntExistsConnectionsMessage = string.Format(Resources.InterfacesWebContent.NPMWEBCODE_PS0_11, TooltipData.ElementAt(0).StartNodeName, TooltipData.ElementAt(0).EndNodeName);
                    TooltipData = Enumerable.Empty<InterfaceEdgeTooltipData>();
                }
            }
        }
    }

    private string FormatVlansListForDisplayInTooltip(IEnumerable<string> vlans)
    {
        string res = string.Empty;
        int multiply = 1;
        string newline = "<br/>";
        const int numberOfCharactersPerRow = 18;

        if (vlans != null)
        {
            for (int i = 0; i < vlans.Count(); i++)
            {
                if (res != string.Empty)
                    res += ", ";

                if ((res.Length - (multiply - 1) * newline.Length) > (multiply * numberOfCharactersPerRow))
                {
                    res += newline;
                    multiply++;
                }

                res += vlans.ElementAt(i);
            }
        }

        return res;
    }

    private string CombineStatusIconUrlPath(string path1, string path2)
    {
        return Path.Combine(path1, "Small-" + path2).Replace("\\", "/");
    }
}