﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditHighPercentUtil.ascx.cs" Inherits="Orion_Interfaces_Controls_EditResourceControls_EditHighPercentUtil" %>
<%@ Register TagPrefix="orion" TagName="FilterEdit" Src="~/Orion/Controls/FilterNodesSql.ascx" %>
<%@ Register TagPrefix="orion" TagName="ServerFilter" Src="~/Orion/Controls/OrionServerFilterControl.ascx" %>


<table>
    <tr>
        <td>
            <orion:ServerFilter ID="ServerFilter" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            <p>
                <b><%= Resources.CoreWebContent.WEBCODE_PSR_4 %></b><br/>
                <asp:TextBox runat="server" ID="RowsPerPage" Width="50"></asp:TextBox>
                <asp:RangeValidator runat="server" Display="Dynamic" ControlToValidate="RowsPerPage" Type="Integer" MaximumValue="100" MinimumValue="1" ErrorMessage="*"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" Display="Dynamic" ControlToValidate="RowsPerPage" Text="*"></asp:RequiredFieldValidator>
            </p>                
            <p>
                <b><%= Resources.CoreWebContent.WEBDATA_PCC_WM_ER_FILTERNODES %></b><br />
                <asp:TextBox runat="server" ID="Filter" Width="330"></asp:TextBox>
            </p>

            <p><%= Resources.InterfacesWebContent.NPMWEBDATA_PS1_11 %></p>
            
            <orion:CollapsePanel runat="server" Collapsed="true">
	<TitleTemplate><b><%= Resources.CoreWebContent.WEBDATA_TM0_15 %></b></TitleTemplate>
	
	<BlockTemplate>
        <p><%= Resources.CoreWebContent.WEBDATA_PC0_01 %></p>

		        <p><%= Resources.CoreWebContent.WEBDATA_PC0_02 %><br />
		        <b><%= Resources.InterfacesWebContent.NPMWEBDATA_PS1_12 %></b></p>

		        <p><%= Resources.CoreWebContent.WEBDATA_PC0_04 %>
		
			        </p><table>

				        <tbody><tr>
					        <td><%= Resources.CoreWebContent.WEBDATA_PC0_05 %></td><td><%= Resources.CoreWebContent.WEBDATA_PC0_06 %></td>
				        </tr>
				        <tr>
					        <td><%= Resources.CoreWebContent.WEBDATA_PC0_07 %></td><td><%= Resources.CoreWebContent.WEBDATA_PC0_08 %></td>
				        </tr><tr>

					        <td><%= Resources.CoreWebContent.WEBDATA_PC0_09 %></td><td><%= Resources.CoreWebContent.WEBDATA_PC0_10 %></td>
				        </tr><tr>
					        <td><%= Resources.CoreWebContent.WEBDATA_PC0_11 %></td><td><%= Resources.CoreWebContent.WEBDATA_PC0_12 %></td>
				        </tr>
			        </tbody></table>
		

		        <p><%= Resources.CoreWebContent.WEBDATA_PC0_13 %><br />
		        <b><%= Resources.InterfacesWebContent.NPMWEBDATA_PS1_13 %></b></p>

		        <p><%= Resources.CoreWebContent.WEBDATA_PC0_15 %><br />
		        <b><%= Resources.InterfacesWebContent.NPMWEBDATA_PS1_14 %></b></p>
						
		        <p><%= Resources.CoreWebContent.WEBDATA_PC0_17 %><br />
		        <b><%= Resources.InterfacesWebContent.NPMWEBDATA_PS1_15 %></b></p>

		        <p><%= Resources.CoreWebContent.WEBDATA_PC0_19 %><br />
		        <b><%= Resources.InterfacesWebContent.NPMWEBDATA_PS1_16 %></b></p>

		        <p><%= Resources.CoreWebContent.WEBDATA_PC0_21 %><br /> 
                <b><%= Resources.InterfacesWebContent.NPMWEBDATA_PS1_17 %></b></p>
		        <p>
	</BlockTemplate>
</orion:CollapsePanel>
            <orion:CollapsePanel ID="CollapsePanel1" runat="server" Collapsed="true">
			    <TitleTemplate><b><%= Resources.CoreWebContent.WEBDATA_TM0_37 %></b></TitleTemplate>
			    <BlockTemplate>
				    <ul>
					    <%foreach (string column in NodesProperties) {%>
						    <li><%=column%></li>
					    <%}%>
				    </ul>
			    </BlockTemplate>
		    </orion:CollapsePanel>
            <orion:CollapsePanel ID="CollapsePanel2" runat="server" Collapsed="true">
			    <TitleTemplate><b><%= Resources.CoreWebContent.WEBDATA_TM0_38 %></b></TitleTemplate>
			    <BlockTemplate>
				    <ul>
					    <%foreach (string column in InterfacesProperties) {%>
						    <li><%=column%></li>
					    <%}%>
				    </ul>
			    </BlockTemplate>
		    </orion:CollapsePanel>

        </td>
    </tr>
</table>