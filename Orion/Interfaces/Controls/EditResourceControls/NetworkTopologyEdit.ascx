﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NetworkTopologyEdit.ascx.cs"
    Inherits="Orion_NPM_Controls_EditResourceControls_NetworkTopologyEdit" %>
        <%@ Register TagPrefix="orion" TagName="AutoHide" Src="~/Orion/Controls/AutoHideControl.ascx" %>
<table>
    <tr>
        <td colspan="2">
            <b><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_256%></b>
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox runat="server" ID="maxCount" Columns="5"></asp:TextBox>
            <asp:RangeValidator runat="server" Display="Dynamic" ControlToValidate="maxCount"
                Type="Integer" MaximumValue="250" MinimumValue="1" ErrorMessage="*"></asp:RangeValidator>
        </td>
    </tr>
    <tr>
        <td>
            <b><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_257%></b><br />
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox runat="server" ID="NodeSrcFilter" Width="330"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <b><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_258%></b><br />
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox runat="server" ID="InterfaceSrcFilter" Width="330"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <b><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_259%></b><br />
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox runat="server" ID="NodeDestFilter" Width="330"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td  colspan="2">
            <b><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_260%></b><br />
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox runat="server" ID="InterfaceDestFilter" Width="330"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <p>
                <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_261%></p>
            <orion:CollapsePanel ID="CollapsePanel1" runat="server" Collapsed="true">
                <TitleTemplate>
                    <b><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_262%></b></TitleTemplate>
                <BlockTemplate>
                    <p>
                        <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_263%></p>
                    <p>
                        <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_264%><br />
                        <b>Status&lt;&gt;1</b></p>
                    <p>
                        <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_265%>
                        <table>
                            <tr>
                                <td>
                                    0 = <b><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_266%></b>
                                </td>
                                <td>
                                    <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_267%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    1 = <b><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_268%></b>
                                </td>
                                <td>
                                    <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_269%>
                                </td>
                                <tr>
                                    <td>
                                        2 = <b><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_270%></b>
                                    </td>
                                    <td>
                                        <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_271%>
                                    </td>
                                    <tr>
                                        <td>
                                            3 = <b><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_272%></b>
                                        </td>
                                        <td>
                                            <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_273%>
                                        </td>
                                    </tr>
                        </table>
                    </p>
                    <p>
                        <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_274%></p>
                    <p>
                        <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_275%></p>
                    <p>
                        <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_276%></p>
                    <p>
                        <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_277%></p>
                    <p>
                        <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_278%></p>
                    <p>
                    </p>
                    <orion:CollapsePanel ID="CollapsePanel1" runat="server" Collapsed="true">
                        <TitleTemplate>
                            <b><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_279%></b></TitleTemplate>
                        <BlockTemplate>
                            <ul>
                                <%foreach (string column in SolarWinds.Orion.NPM.Web.Node.GetAllColumnNames())
                                  {%>
                                <li>
                                    <%=column%></li>
                                <%}%>
                            </ul>
                        </BlockTemplate>
                    </orion:CollapsePanel>
                    <orion:CollapsePanel ID="CollapsePanel2" runat="server" Collapsed="true">
                        <TitleTemplate>
                            <b><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_280%></b></TitleTemplate>
                        <BlockTemplate>
                            <ul>
                                <%foreach (string column in SolarWinds.Orion.NPM.Web.Interface.GetAllColumnNames())
                                  {%> 
                                <li>
                                    <%=column%></li>
                                <%}%>
                            </ul>
                        </BlockTemplate> 
                    </orion:CollapsePanel>
                </BlockTemplate>
            </orion:CollapsePanel>
        </td>
    </tr>
      <tr>
        <orion:AutoHide runat="server" ID="autoHide" Description="<%$ Resources: InterfacesWebContent, NPMWEBDATA_VB0_281%>"/>
    </tr>
</table>
