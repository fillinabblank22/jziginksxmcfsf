﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Interfaces_Controls_EditResourceControls_DuplexMismatchEdit : BaseResourceEditControl
{
    private readonly Dictionary<string, object> _properties = new Dictionary<string, object>();

    protected void Page_Load(object sender, EventArgs e)
    {
        base.OnInit(e);
        _properties.Add("SortDuplex", string.Empty);

        var sort = Resource.Properties["SortDuplex"];
        var sortFields = Resource.Properties["SortDuplexFields"];

        if (String.IsNullOrEmpty(sortFields))
        {
            sortFields = String.Format(
                   "ReceivePercentErrors DESC:{0}, TransmitPercentErrors DESC:{1}, LateCollisions DESC:{2}, CRCAlignErrors DESC:{3}",
                   Resources.InterfacesWebContent.NPMWEBCODE_OD0_5,
                   Resources.InterfacesWebContent.NPMWEBCODE_OD0_6,
                   Resources.InterfacesWebContent.NPMWEBCODE_OD0_7,
                   Resources.InterfacesWebContent.NPMWEBCODE_OD0_8);
            sort = "TransmitPercentErrors DESC";
        }

        string[] temp = sortFields.Split(',');
        foreach (string s in temp)
        {
            string[] result = s.Trim().Split(':');
            if (result.Length == 2)
                SortItems.Items.Add(new ListItem(result[1], result[0]));
        }

        SortItems.SelectedValue = sort ?? "TransmitPercentErrors DESC";

        if (HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
        {
            SortItems.Attributes.Add("onchange", "javascript:SaveData('SortDuplex', this.value);");
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            _properties["SortDuplex"] = SortItems.SelectedValue;
            return _properties;
        }
    }

}