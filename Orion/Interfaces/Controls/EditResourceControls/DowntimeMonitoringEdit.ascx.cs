﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Interfaces.Web.Enums;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Interfaces_Controls_EditResourceControls_DowntimeMonitoringEdit : BaseResourceEditControl
{
    private static readonly List<Tuple<string,string>> Periods = new List<Tuple<string, string>>
    {
        new Tuple<string, string>(InterfaceDownTime.Mode.OneDay, InterfacesWebContent.NPMWEBDATA_OD0_15),
        new Tuple<string, string>(InterfaceDownTime.Mode.LastWeek, InterfacesWebContent.NPMWEBDATA_OD0_16),
        new Tuple<string, string>(InterfaceDownTime.Mode.CustomPeriod, InterfacesWebContent.NPMWEBDATA_OD0_17)
    };

    private static readonly List<Tuple<string,string>> DisplayModes = new List<Tuple<string, string>>
    {
        new Tuple<string, string>(InterfaceDownTime.Display.Default, InterfacesWebContent.NPMWEBDATA_OD0_18),
        new Tuple<string, string>(InterfaceDownTime.Display.Custom, InterfacesWebContent.NPMWEBDATA_OD0_17)
    };

    protected string StartDate;
    protected string EndDate;

    protected void Page_Load(object sender, EventArgs e)
    {
        periodDropDown.Items.AddRange(Periods.Select(p => new ListItem(p.Item2, p.Item1)).ToArray());
        displayDropDown.Items.AddRange(DisplayModes.Select(p => new ListItem(p.Item2, p.Item1)).ToArray());

        if (!IsPostBack)
        {
            periodDropDown.SelectedValue = Resource.Properties["InterfaceDowntimeMode"] ?? InterfaceDownTime.Mode.OneDay;
            displayDropDown.SelectedValue = Resource.Properties["InterfaceDowntimeDefaultDisplay"] ?? InterfaceDownTime.Display.Default;

            var periodSettingsVisible = periodDropDown.SelectedValue.Equals(InterfaceDownTime.Mode.CustomPeriod);
            var displaySettingsVisible = displayDropDown.SelectedValue.Equals(InterfaceDownTime.Display.Custom);  

            periodSettings.Style.Value = periodSettingsVisible ? "display: block" : "display:none";
            displaySettings.Style.Value = displaySettingsVisible ? "display: block" : "display:none";

            if (periodSettingsVisible)
            {
                StartDate = Resource.Properties["InterfaceDowntimeStartDate"];
                EndDate = Resource.Properties["InterfaceDowntimeEndDate"];
            }
            else
            {
                StartDate = DateTime.Now.AddDays(-1).ToJsonTicks().ToString();
                EndDate = DateTime.Now.ToJsonTicks().ToString();
            }

            if (displaySettingsVisible)
            {
                hoursPerBlock.Text = Resource.Properties["InterfaceDowntimeHoursPerBlock"];
            }
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            var properties = new Dictionary<string, object>();

            properties["InterfaceDowntimeMode"] = periodDropDown.SelectedValue;
            properties["InterfaceDowntimeDefaultDisplay"] = displayDropDown.SelectedValue;
            if (periodDropDown.SelectedValue.Equals(InterfaceDownTime.Mode.CustomPeriod))
            {
                properties["InterfaceDowntimeStartDate"] = dtStartDateTime.Value.AddSeconds(-dtStartDateTime.Value.Second).ToJsonTicks().ToString();
                properties["InterfaceDowntimeEndDate"] = dtEndDateTime.Value.AddSeconds(-dtEndDateTime.Value.Second).ToJsonTicks().ToString();
            }

            if (displayDropDown.SelectedValue.Equals(InterfaceDownTime.Display.Custom))
            {
                properties["InterfaceDowntimeHoursPerBlock"] = hoursPerBlock.Text;
            }
            
            return properties;
        }
    }

}