﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DuplexMismatchEdit.ascx.cs" Inherits="Orion_Interfaces_Controls_EditResourceControls_DuplexMismatchEdit" EnableViewState="true" %>
<table border="0">
    <tr>
        <td style="font-weight: bold">
            <%= Resources.InterfacesWebContent.NPMWEBDATA_OD0_22 %>
        </td>
    </tr>
    <tr>
        <td>
            <asp:DropDownList runat="server" ID="SortItems" Width="180px" />
        </td>
    </tr>
</table>
