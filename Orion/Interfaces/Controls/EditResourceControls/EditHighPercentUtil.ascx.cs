﻿using System;
using System.Collections.Generic;
using System.Data;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.Web.Enums;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Interfaces_Controls_EditResourceControls_EditHighPercentUtil : BaseResourceEditControl
{
    protected List<string> NodesProperties { get; set; }
    protected List<string> InterfacesProperties { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            RowsPerPage.Text = Resource.Properties["RowsPerPage"] ?? "5";
            Filter.Text = Resource.Properties["Filter"];
        }
    }

    protected override void OnInit(EventArgs e)
    {
        if (SwisFederationInfo.IsFederationEnabled)
        {
            ServerFilter.DisplayOption = OrionServerFilterControlDisplayOptions.RemoteServers;
            ServerFilter.InitialServerIDsToSkip = GetProperty("OrionServersFilter", String.Empty).Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        }
        else
        {
            ServerFilter.Visible = false;
        }

        NodesProperties = new List<string>();
        InterfacesProperties = new List<string>();

        using (var swis = InformationServiceProxy.CreateV3())
        {
            const string query = @"
SELECT Name, CASE WHEN EntityName = 'Orion.NPM.Interfaces' THEN 'I' ELSE 'N' END AS EntityName 
FROM Metadata.Property 
WHERE (EntityName = 'Orion.NPM.Interfaces' OR EntityName = 'Orion.Nodes') AND IsNAvigable = False";

            var table = swis.Query(query);

            if (table != null && table.Rows != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    var entityName = (string) row["EntityName"];
                    var propertyName = (string) row["Name"];

                    if (entityName.Equals("I"))
                        InterfacesProperties.Add(propertyName);
                    else 
                        NodesProperties.Add(propertyName);
                }
            }
        }
        base.OnInit(e);
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            var properties = new Dictionary<string, object>
            {
                {"Filter", Filter.Text},
                {"RowsPerPage", RowsPerPage.Text}
            };

            if (SwisFederationInfo.IsFederationEnabled)
            {
                properties["OrionServersFilter"] = ServerFilter.GetCommaSeparatedServerIDsToSkip();
            }

            return properties;
        }
    }
}