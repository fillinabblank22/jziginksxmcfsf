﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DowntimeMonitoringEdit.ascx.cs" Inherits="Orion_Interfaces_Controls_EditResourceControls_DowntimeMonitoringEdit" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.Interfaces.Web.Enums" %>
<orion:Include ID="Include1" runat="server" File="js/jquery/jquery.timePicker.js" />
<orion:Include ID="Include2" runat="server" File="js/jquery/timePicker.css" />
<orion:Include ID="Include3" runat="server" File="Interfaces/Styles/DowntimeMonitoring.css" />
<%@ Register Src="~/Orion/Controls/DateTimePicker.ascx" TagPrefix="orion" TagName="DateTimePicker" %>

<div>
    <%=Resources.InterfacesWebContent.NPMWEBDATA_OD0_10%>
    <asp:DropDownList ID="periodDropDown" runat="server"/>
    
    <br />
    <table id="periodSettings" class="sw-downtime-edit-table" runat="server">
        <tbody>
            <tr>
                <td><%=Resources.InterfacesWebContent.NPMWEBDATA_OD0_11%></td>
                <td>
                    <orion:DateTimePicker ID="dtStartDateTime" runat="server" EnableDateTimeValidation="True" />
                </td>
            </tr>
            <tr>
                <td><%=Resources.InterfacesWebContent.NPMWEBDATA_OD0_12%></td>
                <td>
                    <orion:DateTimePicker ID="dtEndDateTime" runat="server" EnableDateTimeValidation="True" />
                </td>
            </tr>
        </tbody>
    </table>
</div>
<br />
<div>
    <%=Resources.InterfacesWebContent.NPMWEBDATA_OD0_13%>
    <asp:DropDownList id="displayDropDown" runat="server"/>
    <br />
    <table id="displaySettings" class="sw-downtime-edit-table" runat="server">
        <tbody>
            <tr>
                <td><%=Resources.InterfacesWebContent.NPMWEBDATA_OD0_14%></td>
                <td>
                    <asp:TextBox ID="hoursPerBlock" Width="30" runat="server" value="1"></asp:TextBox>
                    <asp:RangeValidator
                        ControlToValidate="hoursPerBlock"
                        MinimumValue="1"
                        MaximumValue="1440"
                        Text="<%$ Resources:InterfacesWebContent, NPMWEBDATA_OD0_19 %>"
                        runat="server" Type="Integer" />
                </td>
            </tr>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#<%=dtStartDateTime.ClientID %>').orionSetDate(new Date(<%= StartDate %>));
        $('#<%=dtEndDateTime.ClientID %>').orionSetDate(new Date(<%= EndDate %>));

        $('#<%= periodDropDown.ClientID %>').on('change', function() {
            if ($('#<%= periodDropDown.ClientID%> option:selected').val() == '<%= InterfaceDownTime.Mode.CustomPeriod %>') {
                $('#<%= periodSettings.ClientID %>').show();
            } else {
                $('#<%= periodSettings.ClientID %>').hide();
            }
        });

        $('#<%= displayDropDown.ClientID %>').on('change', function () {
            if ($('#<%= displayDropDown.ClientID%> option:selected').val() == '<%= InterfaceDownTime.Display.Custom %>') {
                $('#<%= displaySettings.ClientID %>').show();
            } else {
                $('#<%= displaySettings.ClientID %>').hide();
            }
        });

        $('#<%=dtStartDateTime.ClientID %>').on('change', changeDateTime);
        $('#<%=dtEndDateTime.ClientID %>').on('change', changeDateTime);

        function changeDateTime() {
            var startDate = $('#<%= dtStartDateTime.ClientID %>').orionGetDate();
            var endDate = $('#<%= dtEndDateTime.ClientID %>').orionGetDate();

            if (startDate >= endDate) {
                startDate = addMinutes(endDate, -2);

                $('#<%= dtStartDateTime.ClientID %>').orionSetDate(startDate);
            }
        }

        function addMinutes(date, minutes) {
            return new Date(date.getTime() + minutes * 60000);
        }
    });
</script>
