﻿<%@ Control Language="C#" ClassName="Interfaces_ThresholdButton" %>

<script runat="server">
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		if (!Profile.AllowAdmin)
			this.Visible = false;
	}
</script>

<orion:LocalizableButtonLink runat="server" LocalizedText="CustomText" Text="<%$ Resources: CoreWebContent, ResourcesAll_Thresholds %>" DisplayType="Resource" CssClass="EditResourceButton"
    NavigateUrl="/Orion/Interfaces/Admin/InterfacesSettings.aspx" />
