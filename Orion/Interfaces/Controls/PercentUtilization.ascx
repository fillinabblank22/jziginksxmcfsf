﻿<%@ Control Language="C#" ClassName="PercentUtilization" EnableViewState="false" %>
<%@ Import Namespace="SolarWinds.Orion.NPM.Web" %>

<script runat="server">
     
    private short _value;

    public short Value
    {
        get { return _value; }
        set { _value = value; }
    }


    protected string ClassName
    {
        get
        {
            if (Thresholds.IfPercentUtilizationError.SettingValue <= this.Value)
                return "Error";
            else if (Thresholds.IfPercentUtilizationWarning.SettingValue <= this.Value)
                return "Warning";

            return string.Empty;
        }
    }
    	
            
</script>

<span class="<%= this.ClassName %>"><%= this.Value %> %</span>