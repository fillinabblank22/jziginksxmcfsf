﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterfaceHasObsoleteDataTooltip.ascx.cs" Inherits="Orion_Interfaces_Controls_InterfaceHasObsoleteDataTooltip" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<orion:Include ID="InterfaceObsoleteDataStyles" runat="server" File="../Interfaces/Styles/InterfaceObsoleteData.css" />

<div runat="server" class="sw-suggestion sw-suggestion-warn obsoleteDataBox">
    <span class="sw-suggestion-icon"></span>
    <% = string.Format(Resources.InterfacesWebContent.WEBDATA_LH0_1, HelpHelper.GetHelpUrl("npm", "OrionNPMAG-Why-Are-IF-Data-Obsolete.htm")) %>
</div>
