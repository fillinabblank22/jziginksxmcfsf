using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.DAL;
using RegistrySettings = SolarWinds.Orion.Core.Common.RegistrySettings;


public partial class Orion_NetPerfMon_Controls_InterfaceLink : System.Web.UI.UserControl
{
    [PersistenceMode(PersistenceMode.InnerProperty)]
    public PlaceHolder Content
    {
        get { return this.phContents; }
    }

    private string _interfaceID;
    protected Interface MyInterface;

    public string InterfaceID
    {
        get { return _interfaceID; }
        set
        {
            _interfaceID = value;
            try
            {
                MyInterface = (Interface)NetObjectFactory.Create(this.InterfaceID);
            }
            catch
            {
                this.Visible = false;
            }
        }
    }

	public Interface Interface
	{
		get { return MyInterface; }
		set
		{
			MyInterface = value;
			_interfaceID = MyInterface.NetObjectID;
			AddAttributes();
		}
	}

    [PersistenceMode(PersistenceMode.Attribute)]
    public string TargetURLFormat = "/Orion/Interfaces/InterfaceDetails.aspx?NetObject={0}";

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (this.Visible)
        {
			AddAttributes();
        }
    }

	private bool attributesAdded = false;

	private void AddAttributes()
	{
		if (attributesAdded)
			return;

		attributesAdded = true;

		this.lnkInterface.HRef = string.Format(this.TargetURLFormat, this.InterfaceID);

		if (Profile.ToolsetIntegration)
		{
			try
			{
				// acquire node and set properties
				Node myNode = (Node)NetObjectFactory.Create(string.Format("N:{0}", this.MyInterface.NodeID));
				this.lnkInterface.Attributes.Add("IP", myNode.IPAddress.ToString());
				this.lnkInterface.Attributes.Add("NodeHostname", myNode.Hostname);

				if (RegistrySettings.AllowSecureDataOnWeb())
				{
					this.lnkInterface.Attributes.Add("Community", myNode.CommunityString);
				}
				else
				{
                    string attrValue = FormatHelper.GetGuidParamString(WebCommunityStringsDAL.GetCommunityStringGuid(myNode.CommunityString));
					this.lnkInterface.Attributes.Add("Community", attrValue);
				}

				// now set interface properties
				this.lnkInterface.Attributes.Add("IFName", this.MyInterface.InterfaceName);
				this.lnkInterface.Attributes.Add("IFIndex", this.MyInterface.InterfaceIndex.ToString());
			}
			catch
			{
				// do nothing
			}

			if (!this.phContents.HasControls())
			{
				this.phContents.Controls.Add(new LiteralControl(HttpUtility.HtmlEncode(this.MyInterface.Name)));
			}
		}
	}
}
