﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Models.Thresholds;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Internationalization.Extensions;
using SolarWinds.Orion.Core.Common.Thresholds;
using SolarWinds.Orion.Web.Controllers;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.Model.Thresholds;

public partial class InterfaceThresholdControl : System.Web.UI.UserControl
{
    private static readonly Log _log = new Log();
    private IList<Interface> _interfaces;
    private DataTable ExistingThresholds;
    private Dictionary<string, string> controlsIds = new Dictionary<string, string>();

    private string entityType = "Orion.NPM.Interfaces";
    public string submitButton = "imbtnSubmit";

    public Dictionary<string, string> EscapedControlsIds
    {
        get
        {
            return controlsIds.ToDictionary(
                kv => WebSecurityHelper.HtmlEncode(kv.Key),
                kv => WebSecurityHelper.HtmlEncode(kv.Value)
            );
        }
    }

    private bool IsMultiEdit
    {
        get { return (_interfaces.Count > 1); }
    }

    protected void BusinessLayerExceptionHandler(Exception ex)
    {
        _log.Error(ex);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                DataTable thresholdsNames = service.Query(string.Format(
                    "SELECT [Name], [DisplayName], [DefaultThresholdOperator], [Unit] FROM Orion.ThresholdsNames WHERE [EntityType] = '{0}' ORDER BY [ThresholdOrder]",
                    entityType
                ));

                repThresholds.DataSource = thresholdsNames.Rows;
                repThresholds.DataBind();
            }
        }
        else
        {
            InitThresholdControlAfterPostback();
        }
    }


    private void InitThresholdControlAfterPostback()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "InitThresholdControlAfterPostback", "InitThresholdControlAfterPostback();", true);
    }

    private string[] GetControlsForJS()
    {
        return new string[]
        {
            "sectionThresholdDefinition",
            "sectionGeneralThresholds",
            "sectionCustomThresholds",
            "sectionErrorMessage",
            "sectionWarningMessage",

            "lblOperatorText",
            "lblComputedWarningValue",
            "lblComputedCriticalValue",
            "lblGeneralWarningValue",
            "lblGeneralCriticalValue",

            "chbOverrideGeneral",
            "chbOverrideMultipleObjects",

            "ddlOperator",

            "btnUseLatestBaseline",

            "tbWarningValue",
            "tbCriticalValue",

            "hfUnit",
            "hfIsValid"
        };
    }

    private void RegisterOnSubmitFunction(string thresholdName)
    {
        ScriptManager.RegisterOnSubmitStatement(this,this.GetType(),
                                                 "ValidateThreshold_" + WebSecurityHelper.JavaScriptStringEncode(thresholdName.Replace(".", "_")),
                                                 "if (IsThresholdValid('" + WebSecurityHelper.JavaScriptStringEncode(thresholdName) + "') == false) return false;");
    }

    private void RegisterStartUpFunction(string thresholdName)
    {
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), string.Format("Recompute_{0}", thresholdName.Replace(".", "_")),
                                                     string.Format("Recompute('{0}');", thresholdName),
                                                     true);
    }

    private void CreateControlsDictionary(string thresholdName, RepeaterItem item)
    {
        foreach (string controlId in GetControlsForJS())
        {
            Control c = item.FindControl(controlId);
            controlsIds.Add(thresholdName + "_" + c.ID, c.ClientID);
        }
    }

    private void FillDdlOperator(DropDownList ddlOperator)
    {
        ddlOperator.DataValueField = "Value";
        ddlOperator.DataTextField = "Text";
        ddlOperator.DataSource =
            Enum.GetValues(typeof(ThresholdOperatorEnum))
                .Cast<ThresholdOperatorEnum>()
                .Select(x => new { Value = string.Format("{0}|{1}", (int)x, x.ToString()), Text = x.LocalizedLabel() });
        ddlOperator.DataBind();
    }

    protected void repThresholds_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string thresholdName = ((DataRow)e.Item.DataItem)["Name"].ToString();
            string thresholdUnit = ((DataRow)e.Item.DataItem)["Unit"].ToString();

            CreateControlsDictionary(thresholdName, e.Item);
            RegisterOnSubmitFunction(thresholdName);

            #region Get controls

            HiddenField hfThresholdName = e.Item.FindControl("hfThresholdName") as HiddenField;
            HiddenField hfUnit = e.Item.FindControl("hfUnit") as HiddenField;

            HtmlGenericControl sectionThresholdDefinition = e.Item.FindControl("sectionThresholdDefinition") as HtmlGenericControl;
            HtmlGenericControl sectionGeneralThreshold = e.Item.FindControl("sectionGeneralThresholds") as HtmlGenericControl;
            HtmlGenericControl sectionCustomThresholds = e.Item.FindControl("sectionCustomThresholds") as HtmlGenericControl;
            HtmlGenericControl sectionErrorMessage = e.Item.FindControl("sectionErrorMessage") as HtmlGenericControl;
            HtmlGenericControl sectionWarningMessage = e.Item.FindControl("sectionWarningMessage") as HtmlGenericControl;
            HtmlGenericControl sectionDetailHyperlink = e.Item.FindControl("sectionDetailHyperlink") as HtmlGenericControl;

            Label lblOperatorText = e.Item.FindControl("lblOperatorText") as Label;


            Label lblGeneralWarningValue = e.Item.FindControl("lblGeneralWarningValue") as Label;
            Label lblGeneralCriticalValue = e.Item.FindControl("lblGeneralCriticalValue") as Label;
            Label lblComputedWarningValue = e.Item.FindControl("lblComputedWarningValue") as Label;
            Label lblComputedCriticalValue = e.Item.FindControl("lblComputedCriticalValue") as Label;

            CheckBox chbOverrideGeneral = e.Item.FindControl("chbOverrideGeneral") as CheckBox;
            CheckBox chbOverrideMultipleObjects = e.Item.FindControl("chbOverrideMultipleObjects") as CheckBox;

            DropDownList ddlOperator = e.Item.FindControl("ddlOperator") as DropDownList;

            Button btnUseLatestBaseline = e.Item.FindControl("btnUseLatestBaseline") as Button;

            TextBox tbWarningValue = e.Item.FindControl("tbWarningValue") as TextBox;
            TextBox tbCriticalValue = e.Item.FindControl("tbCriticalValue") as TextBox;

            #endregion

            hfThresholdName.Value = thresholdName;
            hfUnit.Value = thresholdUnit;


            FillDdlOperator(ddlOperator);
            lblOperatorText.Text = WebSecurityHelper.HtmlEncode(ddlOperator.SelectedItem.Text);

            btnUseLatestBaseline.Attributes.Add("OnClick", string.Format("UseBaseline(this, '{0}'); return false;", thresholdName));

            ddlOperator.Attributes.Add("OnChange", string.Format("ThresholdOperatorChanged(this, '{0}');", thresholdName));
            ddlOperator.Attributes.Add("data-form-name", "threshold-operator");
            ddlOperator.Attributes.Add("data-form-threshold", thresholdName);

            chbOverrideGeneral.Attributes.Add("OnClick", string.Format("ThresholdSectionCheckBoxChanged(this, '{0}');", thresholdName));
            chbOverrideGeneral.Attributes.Add("data-form-name", "threshold-overrideGeneral");
            chbOverrideGeneral.Attributes.Add("data-form-threshold", thresholdName);

            tbWarningValue.Attributes.Add("OnChange", string.Format("TextBoxChanged(this, '{0}');", thresholdName));
            tbCriticalValue.Attributes.Add("OnChange", string.Format("TextBoxChanged(this,'{0}');", thresholdName));

            sectionErrorMessage.Style.Add("display", "none");
            sectionWarningMessage.Style.Add("display", "none");
            sectionCustomThresholds.Style.Add("display", "none");

            if (IsMultiEdit)
            {
                DataRow generalThreshold = GetRelatedThreshold(((DataRow)e.Item.DataItem), 0);

                lblGeneralWarningValue.Text = WebSecurityHelper.HtmlEncode(string.Format("{0} {1}", generalThreshold["GlobalWarningValue"].ToString(), thresholdUnit));
                lblGeneralCriticalValue.Text = WebSecurityHelper.HtmlEncode(string.Format("{0} {1}", generalThreshold["GlobalCriticalValue"].ToString(), thresholdUnit));

                hfIsMultiedit.Value = "1";
                chbOverrideMultipleObjects.Visible = true;
                chbOverrideMultipleObjects.Attributes.Add("OnClick",
                    string.Format("OverrideMultipleObjectsChanged(this, '{0}');", WebSecurityHelper.JavaScriptStringEncode(thresholdName))
                );
                chbOverrideMultipleObjects.Attributes.Add("data-form-name", "threshold-overrideMultipleObjects");
                chbOverrideMultipleObjects.Attributes.Add("data-form-threshold", thresholdName);

                chbOverrideGeneral.Enabled = false;
                sectionThresholdDefinition.Style.Add("display", "none");
                sectionDetailHyperlink.Style.Add("display", "none");

                tbWarningValue.Text = WebSecurityHelper.HtmlEncode(generalThreshold["GlobalWarningValue"].ToString());
                tbCriticalValue.Text = WebSecurityHelper.HtmlEncode(generalThreshold["GlobalCriticalValue"].ToString());
            }
            else
            {
                hfIsMultiedit.Value = "0";

                DataRow rowThreshold = GetRelatedThreshold(((DataRow)e.Item.DataItem), _interfaces[0].InterfaceID);

                if (rowThreshold != null)
                {
                    lblGeneralWarningValue.Text = WebSecurityHelper.HtmlEncode(string.Format("{0} {1}", rowThreshold["GlobalWarningValue"].ToString(), thresholdUnit));
                    lblGeneralCriticalValue.Text = WebSecurityHelper.HtmlEncode(string.Format("{0} {1}", rowThreshold["GlobalCriticalValue"].ToString(), thresholdUnit));

                    ThresholdType thrType = (ThresholdType) rowThreshold["ThresholdType"];
                    ThresholdOperatorEnum thrOperator = (ThresholdOperatorEnum) rowThreshold["ThresholdOperator"];

                    if (thrType == ThresholdType.Global)
                    {
                        sectionGeneralThreshold.Style.Add("display", "block");
                        sectionCustomThresholds.Style.Add("display", "none");

                        tbWarningValue.Text = WebSecurityHelper.HtmlEncode(rowThreshold["GlobalWarningValue"].ToString());
                        tbCriticalValue.Text = WebSecurityHelper.HtmlEncode(rowThreshold["GlobalCriticalValue"].ToString());
                    }
                    else
                    {
                        sectionGeneralThreshold.Style.Add("display", "none");
                        sectionCustomThresholds.Style.Add("display", "block");

                        chbOverrideGeneral.Checked = true;

                        ddlOperator.ClearSelection();
                        ddlOperator.Items.FindByValue(string.Format("{0}|{1}", (int)thrOperator, thrOperator.ToString())).Selected = true;

                        lblOperatorText.Text = WebSecurityHelper.HtmlEncode(ddlOperator.SelectedItem.Text);

                        if (thrType == ThresholdType.Static)
                        {
                            tbWarningValue.Text = ThresholdsHelper.FromNumeric((double) rowThreshold["Level1Value"]);
                            tbCriticalValue.Text = ThresholdsHelper.FromNumeric((double) rowThreshold["Level2Value"]);
                        }

                        if (thrType == ThresholdType.Dynamic)
                        {
                            tbWarningValue.Text = rowThreshold["Level1Formula"].ToString();
                            tbCriticalValue.Text = rowThreshold["Level2Formula"].ToString();
                        }

                        RegisterStartUpFunction(thresholdName);
                    }

                }
            }
        }
    }

    public void Initialize(IList<Interface> interfaces)
    {
        _interfaces = interfaces;
        hfObjectIds.Value = string.Join(" ", _interfaces.Select(x => x.InterfaceID.ToString()).ToArray());
        LoadExistingThresholds();
    }

    private void LoadExistingThresholds()
    {
        using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
        {
            ExistingThresholds = service.Query(
                string.Format(
                    @"SELECT
                        [Name],
                        [Level1Value],
                        [Level1Formula],
                        [Level2Value],
                        [Level2Formula],
                        [InstanceId],
                        [InstanceType],
                        [ThresholdOperator],
                        [ThresholdType],
                        [EntityType],
                        [CurrentValue],
                        [GlobalWarningValue],
                        [GlobalCriticalValue]
                    FROM Orion.NPM.InterfacesThresholds
                    WHERE
                        [EntityType] = '{0}' AND
                        [InstanceId] IN (0, {1})", entityType, string.Join(",", _interfaces.Select(x => x.InterfaceID))
                )
            );
        }
    }

    private bool IsExistingThreshold(Threshold threshold)
    {
        string condition;

        condition = string.Format("[EntityType] = '{0}' AND [InstanceId] = {1} AND [ThresholdType] = {2} AND [Name] = '{3}' ", entityType, threshold.InstanceId, (int)threshold.ThresholdType, threshold.ThresholdName);

        if (threshold.ThresholdType != ThresholdType.Global)
        {
            condition += string.Format(" AND [ThresholdOperator] = {0}", (int)threshold.ThresholdOperator);

            if (threshold.ThresholdType == ThresholdType.Static)
            {
                condition += string.Format(" AND [Level1Value] = {0} AND [Level2Value] = {1}", ThresholdsHelper.FromNumeric(threshold.Warning),ThresholdsHelper.FromNumeric(threshold.Critical));
            }

            if (threshold.ThresholdType == ThresholdType.Dynamic)
            {
                condition += string.Format(" AND [Level1Formula] = '{0}' AND [Level2Formula] = '{1}'", threshold.WarningFormula, threshold.CriticalFormula);
            }
        }

        return ExistingThresholds.Select(condition).Count() > 0;
    }

    private DataRow GetRelatedThreshold(DataRow rowFromThresholdNames, int instanceId)
    {
        string selectQuery = string.Format("[Name] = '{0}' AND [InstanceId] = {1}", WebSecurityHelper.HtmlEncode(rowFromThresholdNames["Name"].ToString()), instanceId);
        DataRow[] result = ExistingThresholds.Select(selectQuery);

        return result.Count() == 1 ? result[0] : null;
    }

    public bool UpdateThresholds()
    {

        if (_interfaces == null)
            return false;

        List<Threshold> thresholds = new List<Threshold>();

        foreach (RepeaterItem repItem in repThresholds.Items)
        {
            if (IsMultiEdit)
            {
                CheckBox chbOverrideMultipleObjects = repItem.FindControl("chbOverrideMultipleObjects") as CheckBox;

                if (!chbOverrideMultipleObjects.Checked)
                {
                    continue;
                }
            }

            CheckBox chb = repItem.FindControl("chbOverrideGeneral") as CheckBox;
            HiddenField hfThresholdName = repItem.FindControl("hfThresholdName") as HiddenField;

            if (chb.Checked)
            {

                DropDownList ddlOperator = repItem.FindControl("ddlOperator") as DropDownList;
                TextBox tbWarningValue = repItem.FindControl("tbWarningValue") as TextBox;
                TextBox tbCriticalValue = repItem.FindControl("tbCriticalValue") as TextBox;

                ThresholdOperatorEnum thresholdOperator = (ThresholdOperatorEnum)System.Enum.Parse(typeof(ThresholdOperatorEnum), ddlOperator.SelectedValue.Split('|')[1]);

                foreach (var inf in _interfaces)
                {
                    Interface tmp = inf;

                    if (tmp.InterfaceID == 0)
                    {
                        continue;
                    }

                    var threshold = CreateThreshold(inf, hfThresholdName.Value, thresholdOperator, tbWarningValue.Text, tbCriticalValue.Text, true);

                    if(threshold!=null)
                        thresholds.Add(threshold);
                }
            }
            else
            {
                foreach (var inf in _interfaces)
                {
                    thresholds.Add(CreateThreshold(inf, hfThresholdName.Value, ThresholdOperatorEnum.Greater, null, null, false));
                }
            }
        }

        using (ICoreBusinessLayer proxy = CoreBusinessLayerProxyCreatorFactory.GetCreator().Create(BusinessLayerExceptionHandler))
        {
            foreach (var threshold in thresholds)
            {
                if (!IsExistingThreshold(threshold))
                {
                    proxy.SetThreshold(threshold);
                }
            }
        }

        return true;
    }

    //expose this to make it unittestable
    public Threshold CreateThreshold(Interface inf, string thresholdName, ThresholdOperatorEnum thresholdOperator,
                                      string warning, string critical, bool overrideGeneral)
    {
        Threshold threshold = new Threshold()
        {
            InstanceId = inf.InterfaceID,
            ThresholdName = thresholdName,
            ThresholdOperator = thresholdOperator,
            ThresholdType = ThresholdType.Global,
            Warning = null,
            Critical = null,
            WarningFormula = null,
            CriticalFormula = null
        };

        if (!overrideGeneral)
            return threshold;

        //Override threshold must have warning and critical specified.
        //validation code is already doing that. This might be removed.
        if (String.IsNullOrEmpty(warning) || String.IsNullOrEmpty(critical))
            return null;

        threshold.WarningEnabled = true;
        threshold.CriticalEnabled = true;

        double warningValue;
        double criticalValue;
        bool bothNumeric = double.TryParse(warning, out warningValue) &
                           double.TryParse(critical, out criticalValue);

        if (bothNumeric)
        {
            threshold.ThresholdType = ThresholdType.Static;
            threshold.Warning = warningValue;
            threshold.Critical = criticalValue;
        }
        else
        {
            threshold.ThresholdType = ThresholdType.Dynamic;
            threshold.WarningFormula = warning;
            threshold.CriticalFormula = critical;
            threshold = ComputeThresholdValues(threshold);
        }

        return threshold;
    }

	private static Threshold ComputeThresholdValues(Threshold threshold)
    {
        try
        {
            ComputeThresholdRequest request = new ComputeThresholdRequest();

            request.ThresholdName = threshold.ThresholdName;

            request.InstancesId = new[] { threshold.InstanceId };
            request.Operator = threshold.ThresholdOperator;

            request.CriticalFormula = threshold.CriticalFormula;
            request.WarningFormula = threshold.WarningFormula;

            ComputeThresholdResponse response = new ThresholdsController().Compute(request);

            if (response.IsComputed)
            {
                threshold.Warning = response.WarningThreshold;
                threshold.Critical = response.CriticalThreshold;
            }
        }
        catch (Exception e)
        {
            _log.Error(string.Format("Can't compute current values for threshold {0}.", threshold), e);
        }

        return threshold;
    }

}
