﻿<%@ WebService Language="C#" Class="SummaryChartData" %>

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Interfaces.Web.Charting;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.InformationService;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public sealed class SummaryChartData : InterfacesChartDataWebServiceBase
{
    private static SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();
    
    private static void FillViewLimitationInformation(ChartDataRequest request)
    {
        int limitationID = -1;

        if (request.NetObjectIds.Any() && int.TryParse(request.NetObjectIds[0], out limitationID))
        {

            if (System.Web.HttpContext.Current.Items[(object)typeof(ViewInfo).Name] == null)
            {
                System.Web.HttpContext.Current.Items[(object)typeof(ViewInfo).Name] = new ViewInfo("npm", null, null, false,
                                                                                                     false, null)
                {
                    LimitationID = limitationID
                };
            }
        }
    }
    
    private static string GetDateTimeSampled(ChartDataRequest request)
    {
        return string.Format("Convert([DateTime],Floor(Cast(CAST([DateTime] AS DATETIME) as Float)/{0}*1440)*{0}/1440,0)",
            request.SampleSizeInMinutes);
    }
    
    private static string ApplySamplingOnSql(ChartDataRequest request, string query, string sqlFunc, params string[] columns)
    {
        const string queryTemplate = @"
                SELECT DATEADD(ms,500 - DATEPART(ms,{2} + '00:00:00.500'),{2}) as [DateTime],
                {0}
                FROM (
                    {1}
                ) subselect
                GROUP BY {2}
                ORDER BY [DateTime]
                ";

        var rx = new Regex("ORDER BY +[^ \n]+"); // remove order by from inner clause


        return string.Format(queryTemplate,
                                string.Join(",", columns.Select(x => string.Format("{0}({1}) as {1}", sqlFunc, x))),
                                rx.Replace(query, ""),
                                GetDateTimeSampled(request)
            );
    }

    

    [WebMethod]
    public ChartDataResults NetworkWideBytesTransferred(ChartDataRequest request)
    {
        FillViewLimitationInformation(request);
        const string sql = @"SELECT it.DateTime, it.In_TotalBytes + it.Out_TotalBytes as TotalBytes
                             From InterfaceTraffic it
                             INNER JOIN Interfaces ON Interfaces.InterfaceID = it.InterfaceID
                             INNER JOIN Nodes ON Nodes.NodeID = Interfaces.NodeID
                             WHERE 
                                it.DateTime >= @StartTime AND it.DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var data = DoSimpleLoadData(request, dateRange, 
                   ApplySamplingOnSql(request, sql, "SUM", "TotalBytes"),
                   "", DateTimeKind.Utc, "TotalBytes", null,
                                    SampleMethod.Total, false);

        var result = new ChartDataResults(ComputeSubseries(request, dateRange, data));
        var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);
        
        return CalculateMinMaxForYAxis(dynamicResult);
    }

    [WebMethod]
    public ChartDataResults NetworkWideAvgUtil(ChartDataRequest request)
    {
        FillViewLimitationInformation(request);
        const string sql = @"SELECT it.DateTime, 
                                (it.In_Averagebps / Interfaces.InBandwidth) * 100 as InUtil,
                                (it.Out_Averagebps / Interfaces.OutBandwidth) * 100 as OutUtil
                             From InterfaceTraffic it
                             INNER JOIN Interfaces ON Interfaces.InterfaceID = it.InterfaceID
                             INNER JOIN Nodes ON Nodes.NodeID = Interfaces.NodeID
                             WHERE 
                                Interfaces.InBandwidth > 0 
                                AND Interfaces.OutBandwidth > 0
                                AND it.DateTime >= @StartTime AND it.DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var data = DoSimpleLoadData(request, dateRange,
                   ApplySamplingOnSql(request, sql, "AVG", "InUtil", "OutUtil"),
                   "", DateTimeKind.Utc, "InUtil", "OutUtil", SampleMethod.Average, false);

        var result = ApplyColorsForChartsIfNeeded(new ChartDataResults(ComputeSubseries(request, dateRange, data)), request);
        var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);
        
        return CalculateMinMaxForYAxis(dynamicResult);
    }

    [WebMethod]
    public ChartDataResults NetworkWideAvgFRUtil(ChartDataRequest request)
    {
        FillViewLimitationInformation(request);
        const string sql = @"SELECT it.DateTime, 
                                (it.In_Averagebps / Interfaces.InBandwidth) * 100 as InUtil,
                                (it.Out_Averagebps / Interfaces.OutBandwidth) * 100 as OutUtil
                             From InterfaceTraffic it
                             INNER JOIN Interfaces ON Interfaces.InterfaceID = it.InterfaceID
                             INNER JOIN Nodes ON Nodes.NodeID = Interfaces.NodeID
                             WHERE 
                                Interfaces.InBandwidth > 0 
                                AND Interfaces.OutBandwidth > 0
                                AND Interfaces.InterfaceTypeName='FrameRelay'
                                AND it.DateTime >= @StartTime AND it.DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var data = DoSimpleLoadData(request, dateRange,
                   ApplySamplingOnSql(request, sql, "AVG", "InUtil", "OutUtil"), 
                   "", DateTimeKind.Utc, "InUtil", "OutUtil", SampleMethod.Average, false);

        var result = ApplyColorsForChartsIfNeeded(new ChartDataResults(ComputeSubseries(request, dateRange, data)), request);
        var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);
        
        return CalculateMinMaxForYAxis(dynamicResult);
    }
    
    //Calculate minimal and maximal value for Y-axis. When is minVal higher then 0, we use 0.
    private ChartDataResults CalculateMinMaxForYAxis(ChartDataResults result)
    {
        double minVal;
        double maxVal;
        bool noPoints = CalculateMinMaxValueForDataSeries(result.DataSeries, out minVal, out maxVal);

        minVal=Math.Min(minVal,0);        //when no points result is double.MaxValue
        if(!noPoints)
            maxVal = 0;

        if(result.ChartOptionsOverride == null)
            result.ChartOptionsOverride = new JsonObject();

        dynamic options = result.ChartOptionsOverride as JsonObject;
        if(options==null)
            _log.Error("ChartOptionsOverride is not a JsonObject");
        
        options.yAxis = JsonObject.CreateJsonArray(1);
        options.yAxis[0].min = minVal;
        options.yAxis[0].minRange = 10;

        result.ChartOptionsOverride = options;

        return result;
    }    
}
