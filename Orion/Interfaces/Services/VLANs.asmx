﻿<%@ WebService Language="C#" CodeBehind="~/App_Code/VLANs.cs" Class="VLANs" %>
using Resources;
using SolarWinds.Orion.Core.Models.Enums;
using SolarWinds.Orion.Web.InformationService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class VLANs : WebService
{
    private const int TreeSubItemsCount = 10;
    
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<VlanResult> GetNodeVlans(int nodeId, int startItem, int lastItem)
    {
        var result = new List<VlanResult>();
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            string queryString = @"SELECT VlanId, VlanName, VlanTag, VlanStatus FROM Orion.NodeVlans 
WHERE NodeId = @NodeId
ORDER BY VlanId
WITH ROWS @StartRow TO @EndRow";
            var parameters = new Dictionary<string, object>
            {
                {"NodeId", nodeId},
                {"StartRow", startItem},
                {"EndRow", lastItem}
            };
            DataTable proxyResult = proxy.Query(queryString, parameters);
            foreach (DataRow row in proxyResult.Rows)
            {
                int? status = row["VlanStatus"] == DBNull.Value ? null : (int?)row["VlanStatus"];
                var vlan = new VlanResult
                {
                    VlanId = (int) row["VlanId"],
                    VlanName = row["VlanName"] == DBNull.Value ? string.Empty : (string) row["VlanName"],
                    Tag = row["VlanTag"] == DBNull.Value ? string.Empty : (string) row["VlanTag"],
                    VlanStatusIcon = calculateVlanStatus(status),
                    VlanStatusIconTitle = calculateVlanStatusTitle(status),
                    Interfaces = new List<VlanInterfacesResult>()
                };
                int? interfaceCount = queryInterfaceCount(nodeId, vlan.VlanId, proxy);
                if (interfaceCount.HasValue)
                {
                    vlan.ItemCount = interfaceCount.Value;
                }
                result.Add(vlan);
            }
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public int GetNodeVlansCount(int nodeId)
    {
        int count = 0;
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            const string queryString = "SELECT COUNT(VlanId) AS VlanCount FROM Orion.NodeVlans WHERE NodeId = @NodeId";
            var parameters = new Dictionary<string, object> {{"NodeId", nodeId}};
            DataTable proxyResult = proxy.Query(queryString, parameters);
            if (proxyResult.Rows.Count > 0 && proxyResult.Columns["VlanCount"] != null)
            {
                count = (int)proxyResult.Rows[0]["VlanCount"];
            }
        }
        return count;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<VlanInterfacesResult> GetVlanInterfaces(string rowId, int startFrom)
    {
        var result = new List<VlanInterfacesResult>();
        var splitted = rowId.Split('|');
        if (splitted.Length != 2)
        {
            return result;
        }
        
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            const string queryString = @"SELECT i.InterfaceID, i.Caption, npim.PortType, i.StatusLED
FROM Orion.NodePortInterfaceMap npim 
JOIN Orion.NPM.Interfaces i on npim.NodeID = i.NodeId AND npim.IfIndex = i.InterfaceIndex
WHERE npim.NodeVlan.NodeId = @NodeId AND npim.NodeVlan.VlanId = @VlanId
ORDER BY i.Caption
WITH ROWS @StartRow TO @EndRow";
            var parameters = new Dictionary<string, object> { { "NodeId", splitted[0] }, { "VlanId", splitted[1] }, { "StartRow", startFrom }, { "EndRow", startFrom + TreeSubItemsCount } };
            var proxyResult = proxy.Query(queryString, parameters);
            foreach (DataRow row in proxyResult.Rows)
            {
                if (row["InterfaceId"] != DBNull.Value)
                {
                    //If there's value for interface, we show it.
                    var portType = (byte)row["PortType"];
                    var iface = new VlanInterfacesResult
                    {
                        InterfaceName = (string)row["Caption"],
                        InterfaceStatusIcon = (string)row["StatusLED"],
                        InterfaceDetailsLink = string.Format("/Orion/Interfaces/InterfaceDetails.aspx?NetObject=I:{0}&view=InterfaceDetails", (int)row["InterfaceID"]),
                        InterfaceType = mapPortType((VlanPortType)portType)
                    };
                    result.Add(iface);
                }
            }
        }
        return result;
    }
        
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<VlanListResult> GetAllVlans(int startItem, int lastItem)
    {
        var result = new List<VlanListResult>();
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            string queryString = @"SELECT DISTINCT nv.VlanId, COUNT(nv.VlanId) AS Nodes FROM Orion.NodeVlans AS nv
WHERE (
    SELECT COUNT(p.PollerType) AS PollerCount 
    FROM Orion.Pollers AS p
    JOIN Orion.Nodes AS n ON p.NetObjectID = n.NodeID
    WHERE p.NetObjectType = 'N' AND p.NetObjectID = nv.NodeId AND p.Enabled = true 
        AND (p.PollerType LIKE '%_Vlans.%' OR p.PollerType LIKE '%_PortsMap.%')
) = 2
GROUP BY nv.VlanId
ORDER BY nv.VlanId
WITH ROWS @StartRow TO @EndRow";
            var parameters = new Dictionary<string, object>();
            parameters.Add("StartRow", startItem);
            parameters.Add("EndRow", lastItem);
            DataTable proxyResult = proxy.Query(queryString, parameters);
            foreach (DataRow row in proxyResult.Rows)
            {
                result.Add(new VlanListResult
                {
                    VlanId = (int) row["VlanId"], 
                    ItemCount = (int) row["Nodes"],
                    Nodes = new List<VlanNodeResult>(),
                    VlanName = String.Empty,
                });
            }

            // no vlans, no nodes to get
            if (result.Count == 0)
            {
                return result;
            }

            queryString = String.Format(@"SELECT nv.Node.NodeID, nv.VlanStatus, nv.VlanName, nv.VlanId 
FROM Orion.NodeVlans nv
WHERE nv.VlanId IN ({0})", String.Join(",", result.Select(p => p.VlanId)));
            proxyResult = proxy.Query(queryString);
            foreach (DataRow row in proxyResult.Rows)
            {
                var vlanId = (int) row["VlanId"];
                var item = result.Find(p => p.VlanId == vlanId);
                if (item != null && row["NodeID"] != DBNull.Value)
                {
                    int? status = row["VlanStatus"] == DBNull.Value ? null : (int?)row["VlanStatus"];
                    var node = new VlanNodeResult
                    {
                        VlanName = row["VlanName"] == DBNull.Value ? string.Empty : (string)row["VlanName"],
                        VlanStatus = status,
                        VlanStatusIcon = calculateVlanStatus(status),
                    };
                    item.Nodes.Add(node);
                }
            }
            
            result.ForEach(p => { 
                p.VlansStatusIcon = calculateOveralVlanStatus(p.Nodes);
                p.VlansStatusIconTitle = calculateOveralVlanStatusTitle(p.Nodes);
                if (p.Nodes.Count > 0 && p.Nodes.All(n => n.VlanName.Equals(p.Nodes[0].VlanName)))
                {
                    p.VlanName = p.Nodes[0].VlanName;
                }
            });
        }
        return result.Where(p => p.Nodes.Count > 0).ToList();
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<VlanNodeResult> GetVlanNodes(string rowId, int startFrom)
    {
        var result = new List<VlanNodeResult>();
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            const string queryString = @"SELECT nv.Node.NodeID, nv.Node.StatusLED, nv.Node.Caption, nv.VlanStatus, nv.VlanName, nv.VlanId 
FROM Orion.NodeVlans nv
WHERE nv.VlanId = @VlanId AND nv.Node.NodeID IS NOT NULL
ORDER BY nv.NodeID
WITH ROWS @StartRow TO @EndRow";
            var parameters = new Dictionary<string, object> { { "VlanId", rowId }, { "StartRow", startFrom }, { "EndRow", startFrom + TreeSubItemsCount } };
            var proxyResult = proxy.Query(queryString, parameters);
            foreach (DataRow row in proxyResult.Rows)
            {
                if (row["NodeID"] != DBNull.Value)
                {
                    int? status = row["VlanStatus"] == DBNull.Value ? null : (int?)row["VlanStatus"];
                    var node = new VlanNodeResult
                    {
                        NodeName = (string)row["Caption"],
                        NodeDetailsLink = string.Format("/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}", (int)row["NodeID"]),
                        NodeStatusIcon = (string)row["StatusLED"],
                        VlanName = row["VlanName"] == DBNull.Value ? ("<i>" + InterfacesWebContent.NPMWEBCODE_VB0_12 + "</i>") : (string)row["VlanName"],
                        VlanId = (int)row["VlanId"],
                        VlanStatus = status,
                        VlanStatusIcon = calculateVlanStatus(status),
                        VlanStatusIconTitle = calculateVlanStatusTitle(status),
                    };
                    result.Add(node);
                }
            }
        }
        return result;
    }
    
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public int GetAllVlansCount()
    {
        int count = 0;
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            string queryString = @"SELECT COUNT(DISTINCT nv.VlanId) AS VlanCount 
FROM Orion.NodeVlans AS nv
WHERE (
    SELECT COUNT(PollerType) AS PollerCount 
    FROM Orion.Pollers AS p
    JOIN Orion.Nodes AS n ON p.NetObjectID = n.NodeID
    WHERE p.NetObjectType = 'N' AND p.NetObjectID = nv.NodeId AND p.Enabled = true 
        AND (p.PollerType LIKE '%_Vlans.%' OR p.PollerType LIKE '%_PortsMap.%')
) = 2";
            var parameters = new Dictionary<string, object>();
            DataTable proxyResult = proxy.Query(queryString, parameters);
            if (proxyResult.Rows.Count > 0 && proxyResult.Columns["VlanCount"] != null)
            {
                count = (int)proxyResult.Rows[0]["VlanCount"];
            }
        }
        return count;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public VlanPopupResult GetPopup(int vlanId, int nodeId)
    {
        var result = new VlanPopupResult();
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            string popupQuery = @"SELECT nv.VlanId, nv.VlanName, i.InBps, i.OutBps, i.InPercentUtil, i.OutPercentUtil, nv.VlanIfIndex, nia.IPAddress FROM Orion.NodeVlans nv
LEFT JOIN Orion.NPM.Interfaces i
ON i.NodeId = nv.NodeId
AND i.InterfaceIndex = nv.VlanIfIndex
LEFT JOIN Orion.NodeIPAddresses nia
ON nv.VlanIfIndex = nia.InterfaceIndex
AND nv.NodeID = nia.NodeID
WHERE VlanId = @VlanId
AND nv.NodeId = @NodeId";
            var parameters = new Dictionary<string, object>();
            parameters.Add("VlanId", vlanId);
            parameters.Add("NodeId", nodeId);
            var dataTable = proxy.Query(popupQuery, parameters);
            Single? inBps;
            Single? outBps;
            Single? inPercentUtil;
            Single? outPercentUtil;
            foreach (DataRow row in dataTable.Rows)
            {
                inBps = row["InBps"] == DBNull.Value ? null : (Single?)row["InBps"];
                outBps = row["OutBps"] == DBNull.Value ? null : (Single?)row["OutBps"];
                inPercentUtil = row["InPercentUtil"] == DBNull.Value ? null : (Single?)row["InPercentUtil"];
                outPercentUtil = row["OutPercentUtil"] == DBNull.Value ? null : (Single?)row["OutPercentUtil"];
                int? ifIndex = row["VlanIfIndex"] == DBNull.Value ? null : (int?) row["VlanIfIndex"];
                result.Name = row["VlanName"] == DBNull.Value ? string.Empty : (string)row["VlanName"];
                result.InBps = getInterfaceText(inBps, ifIndex, false);
                result.OutBps = getInterfaceText(outBps, ifIndex, false);
                result.InPercentUtil = getInterfaceText(inPercentUtil, ifIndex, true);
                result.OutPercentUtil = getInterfaceText(outPercentUtil, ifIndex, true);
				result.IpAddress = row["IPAddress"] == DBNull.Value ? "n/a" : (string)row["IPAddress"];
            }
        }
        return result;
    }
    
    private string getInterfaceText(Single? value, int? ifIndex, bool isPercentage)
    {
        if (value.HasValue)
        {
			if(isPercentage)
			{
				return string.Format("{0} %", value.ToString());
			}
			else
			{
				return new SolarWinds.Orion.Web.DisplayTypes.BitsPerSecond(value.Value).ToString();
			}
        }
        else
        {
            return "n/a";
        }
    }

    private int? queryInterfaceCount(int nodeId, int vlanId, InformationServiceProxy proxy)
    {
        const string interfaceCountQuery = @"SELECT COUNT(*) AS InterfaceCount 
FROM Orion.NodePortInterfaceMap npim 
    JOIN Orion.NPM.Interfaces i ON npim.NodeID = i.NodeID AND npim.IfIndex = i.InterfaceIndex  
WHERE npim.NodeId = @NodeId AND npim.VlanId = @VlanId";
        var interfaceParameters = new Dictionary<string, object>();
        interfaceParameters.Add("NodeId", nodeId);
        interfaceParameters.Add("VlanId", vlanId);
        DataTable interfaceResult = proxy.Query(interfaceCountQuery, interfaceParameters);
        if (interfaceResult.Rows.Count > 0 && interfaceResult.Columns.Contains("InterfaceCount"))
        {
            return (int)interfaceResult.Rows[0]["InterfaceCount"];
        }
        else
        {
            return null;
        }
    }

    private string calculateOveralVlanStatus(List<VlanNodeResult> nodes)
    {
        if (nodes.Count == 0)
        {
            return String.Empty;
        }
        
        if (nodes.All(p => p.VlanStatusIcon.Equals(nodes[0].VlanStatusIcon)))
        {
            return nodes[0].VlanStatusIcon;
        }

        // mixed state
        return "/Orion/Interfaces/Images/Icons/vlan_icon16x16_mixed-status.png";
    }

    private string calculateOveralVlanStatusTitle(List<VlanNodeResult> nodes)
    {
        if (nodes.Count == 0)
        {
            return String.Empty;
        }

        if (nodes.All(p => p.VlanStatusIcon.Equals(nodes[0].VlanStatusIcon)))
        {
            return calculateVlanStatusTitle(nodes[0].VlanStatus);
        }

        // mixed state
        return Resources.InterfacesWebContent.NPMWEBDATA_PS1_7;
    }
    
    private string calculateVlanStatus(int? status)
    {
        string statusName = string.Empty;
        switch (status)
        {
            case (int)VlanStatus.Up:
                statusName = "active";
                break;
            case (int)VlanStatus.Down:
                statusName = "down";
                break;
            case (int)VlanStatus.Suspended:
                statusName = "suspended";
                break;
            case (int)VlanStatus.MtuIssues:
                statusName = "MTUissues";
                break;
            default:
                statusName = "unknown";
                break;
        }
        return string.Format("/Orion/Interfaces/Images/Icons/vlan_icon16x16_{0}.png", statusName);
    }

    private string calculateVlanStatusTitle(int? status)
    {
        switch (status)
        {
            case (int)VlanStatus.Up:
                return Resources.InterfacesWebContent.NPMWEBDATA_PS1_2;
            case (int)VlanStatus.Down:
                return Resources.InterfacesWebContent.NPMWEBDATA_PS1_3;
            case (int)VlanStatus.Suspended:
                return Resources.InterfacesWebContent.NPMWEBDATA_PS1_4;
            case (int)VlanStatus.MtuIssues:
                return Resources.InterfacesWebContent.NPMWEBDATA_PS1_5;
            default:
                return Resources.InterfacesWebContent.NPMWEBDATA_PS1_6;
        }
    }

    private string mapPortType(VlanPortType portType)
    {
        switch (portType)
        {
            case VlanPortType.Access:
                return Resources.InterfacesWebContent.NPMWEBCODE_PS1_3;
            case VlanPortType.Trunk:
                return Resources.InterfacesWebContent.NPMWEBCODE_PS1_2;
            default:
                return Resources.InterfacesWebContent.NPMWEBDATA_GK0_11;
        }
    }

    public class VlanListResult
    {
        public int VlanId { get; set; }
        public int ItemCount { get; set; }
        public string VlansStatusIcon { get; set; }
        public string VlansStatusIconTitle { get; set; }
        public string VlanName { get; set; }
        public List<VlanNodeResult> Nodes { get; set; }
    }
    
    public class VlanResult
    {
        public int VlanId { get; set; }
        public int ItemCount { get; set; }
        public string VlanName { get; set; }
        public string Tag { get; set; }
        public string VlanStatusIcon { get; set; }
        public string VlanStatusIconTitle { get; set; }
        public List<VlanInterfacesResult> Interfaces { get; set; }
    }

    public class VlanNodeResult
    {
        public int? VlanStatus { get; set; }
        public int VlanId { get; set; }
        public string VlanName { get; set; }
        public string VlanStatusIcon { get; set; }
        public string VlanStatusIconTitle { get; set; }
        public string NodeDetailsLink { get; set; }
        public string NodeStatusIcon { get; set; }
        public string NodeName { get; set; }
    }
    
    public class VlanInterfacesResult
    {
        public string InterfaceDetailsLink { get; set; }
        public string InterfaceStatusIcon { get; set; }
        public string InterfaceName { get; set; }
        public string InterfaceType { get; set; }
    }

    public class VlanPopupResult
    {
        public string Name { get; set; }
        public string InBps { get; set; }
        public string OutBps { get; set; }
        public string InPercentUtil { get; set; }
        public string OutPercentUtil { get; set; }
        public string InterfaceIndex { get; set; }
		public string IpAddress {get; set; }
    }
}
