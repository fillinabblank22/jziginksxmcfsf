﻿<%@ WebService Language="C#" Class="InterfaceChartData" %>

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Interfaces.Web.Charting;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.InformationService;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class InterfaceChartData : InterfacesChartDataWebServiceBase
{
	private static SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();

    private const string _interfaceNameLookupSwql = "SELECT InterfaceID, FullName FROM Orion.NPM.Interfaces WHERE InterfaceID in @NodeID";

    private const int NumberOfSeriesPerInterface = 2;
    private const int NumberOfMMSeriesPerInterface = NumberOfSeriesPerInterface * 2;

    private static string GetInterfaceLookupIfNeeded(ChartDataRequest request)
    {
        return _interfaceNameLookupSwql;
    }

    [WebMethod]
    public ChartDataResults InterfaceAvailability(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request, NumberOfSeriesPerInterface);
        string sql =
            string.Format(
                @"
    declare @StartDate as datetime 
    set @StartDate = @StartTime

    declare @EndDate as datetime 
    set @EndDate = @EndTime

    declare @relativeDate AS DATETIME 
    set @relativeDate = '2010-01-01'

    declare @interval As int
    set @interval = {0} * 60

    DECLARE @startTick AS INT
    DECLARE @endTick AS INT

    SET @startTick = DATEDIFF(SECOND, @relativeDate, @StartDate) / @interval
    SET @endTick = DATEDIFF(SECOND, @relativeDate, @EndDate) / @interval

    SELECT 
     DATEADD(SECOND,(DATEDIFF(SECOND, @relativeDate, IA.DateTime) / @interval) * @interval,@relativeDate) AS DateTime,
     SUM(Availability * Weight)/ SUM(Weight) as Availability 
    FROM InterfaceAvailability AS IA 
    LEFT JOIN Interfaces 
    on 
     IA.InterfaceID = Interfaces.InterfaceID
    WHERE
      IA.DateTime between @StartDate and @EndDate AND
      Interfaces.InterfaceID = @NetObjectId
    GROUP BY
     (DATEDIFF(SECOND, @relativeDate, IA.DateTime) / @interval)
    ORDER BY [DateTime] 
",
                request.SampleSizeInMinutes);

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var data = DoSimpleLoadData(request, dateRange, sql, GetInterfaceLookupIfNeeded(request), DateTimeKind.Utc, "Availability", null, SampleMethod.Average, false);

        var result = new ChartDataResults(ComputeSubseries(request, dateRange, data));
        result.AppliedLimitation = request.AppliedLimitation;
		var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);
        
		return CalculateMinMaxForYAxis(dynamicResult);
    }

    [WebMethod]
    public ChartDataResults InterfaceUtilization(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request, NumberOfSeriesPerInterface);
        const string sql = @"SELECT it.DateTime, 
                                (it.In_Averagebps / Interfaces.InBandwidth) * 100 as InUtil,
                                (it.Out_Averagebps / Interfaces.OutBandwidth) * 100 as OutUtil
                             From InterfaceTraffic it
                             INNER JOIN Interfaces ON Interfaces.InterfaceID = it.InterfaceID
                             WHERE 
                                Interfaces.InterfaceID = @NetObjectId
                                AND Interfaces.InBandwidth > 0 
                                AND Interfaces.OutBandwidth > 0
                                AND it.DateTime >= @StartTime AND it.DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var data = DoSimpleLoadData(request, dateRange, sql, GetInterfaceLookupIfNeeded(request), DateTimeKind.Utc, "InUtil", "OutUtil", SampleMethod.Average, false);
        var result = ApplyColorsForChartsIfNeeded(new ChartDataResults(ComputeSubseries(request, dateRange, data)), request);
        result.AppliedLimitation = request.AppliedLimitation;
		var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);

		return CalculateMinMaxForYAxis(dynamicResult);
    }

    [WebMethod]
    public ChartDataResults InInterfaceUtilization(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request, NumberOfSeriesPerInterface);
        const string sql = @"SELECT it.DateTime, 
                                (it.In_Averagebps / Interfaces.InBandwidth) * 100 as InUtilization
                             From InterfaceTraffic it
                             INNER JOIN Interfaces ON Interfaces.InterfaceID = it.InterfaceID
                             WHERE 
                                Interfaces.InterfaceID = @NetObjectId
                                AND Interfaces.InBandwidth > 0
                                AND it.DateTime >= @StartTime AND it.DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var data = DoSimpleLoadData(request, dateRange, sql, GetInterfaceLookupIfNeeded(request), DateTimeKind.Utc, new[] { "InUtilization" }, SampleMethod.Average, false);

        var result = ApplyColorsForChartsIfNeeded(new ChartDataResults(ComputeSubseries(request, dateRange, data)), request);
        result.AppliedLimitation = request.AppliedLimitation;
        var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);

        return CalculateMinMaxForYAxis(dynamicResult);
    }

    [WebMethod]
    public ChartDataResults OutInterfaceUtilization(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request, NumberOfSeriesPerInterface);
        const string sql = @"SELECT it.DateTime, 
                                (it.Out_Averagebps / Interfaces.OutBandwidth) * 100 as OutUtilization
                             From InterfaceTraffic it
                             INNER JOIN Interfaces ON Interfaces.InterfaceID = it.InterfaceID
                             WHERE 
                                Interfaces.InterfaceID = @NetObjectId
                                AND Interfaces.OutBandwidth > 0
                                AND it.DateTime >= @StartTime AND it.DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var data = DoSimpleLoadData(request, dateRange, sql, GetInterfaceLookupIfNeeded(request), DateTimeKind.Utc, new[] { "OutUtilization" }, SampleMethod.Average, false);

        var result = ApplyColorsForChartsIfNeeded(new ChartDataResults(ComputeSubseries(request, dateRange, data)), request);
        result.AppliedLimitation = request.AppliedLimitation;
        var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);

        return CalculateMinMaxForYAxis(dynamicResult);
    }

    [WebMethod]
    public ChartDataResults AvgPps(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request, NumberOfSeriesPerInterface);
        const string sql = @"SELECT it.DateTime, 
                                it.In_AvgUniCastPkts as InAvg,
                                it.Out_AvgUniCastPkts as OutAvg
                             From InterfaceTraffic it
                             LEFT JOIN Interfaces ON Interfaces.InterfaceID = it.InterfaceID
                             WHERE 
                                it.InterfaceID = @NetObjectId
                                AND it.DateTime >= @StartTime AND it.DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var data = DoSimpleLoadData(request, dateRange, sql, GetInterfaceLookupIfNeeded(request), DateTimeKind.Utc, "InAvg", "OutAvg", SampleMethod.Average, false);
        var result = ApplyColorsForChartsIfNeeded(new ChartDataResults(ComputeSubseries(request, dateRange, data)), request);
        result.AppliedLimitation = request.AppliedLimitation;
		var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);

		return CalculateMinMaxForYAxis(dynamicResult);
	}

    [WebMethod]
    public ChartDataResults AvgBps(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request, NumberOfSeriesPerInterface);
        const string sql = @"SELECT it.DateTime, 
                                it.In_Averagebps as InAvg,
                                it.Out_Averagebps as OutAvg
                             From InterfaceTraffic it
                            INNER JOIN Interfaces ON Interfaces.InterfaceID = it.InterfaceID
                             WHERE 
                                it.InterfaceID = @NetObjectId
                                AND it.DateTime >= @StartTime AND it.DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var data = DoSimpleLoadData(request, dateRange, sql, GetInterfaceLookupIfNeeded(request), DateTimeKind.Utc, "InAvg", "OutAvg", SampleMethod.Average, false);
        var result = ApplyColorsForChartsIfNeeded(new ChartDataResults(ComputeSubseries(request, dateRange, data)), request);
	    result.AppliedLimitation = request.AppliedLimitation;

		var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);

		return CalculateMinMaxForYAxis(dynamicResult);
    }

    [WebMethod]
    public ChartDataResults TotalPackets(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request, NumberOfSeriesPerInterface);
        const string sql = @"SELECT it.DateTime, 
                                it.In_TotalPkts as InPackets,
                                it.Out_TotalPkts as OutPackets
                             From InterfaceTraffic it
                            INNER JOIN Interfaces ON Interfaces.InterfaceID = it.InterfaceID
                             WHERE 
                                it.InterfaceID = @NetObjectId
                                AND it.DateTime >= @StartTime AND it.DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);

        var data = DoSimpleLoadData(request, dateRange, sql, GetInterfaceLookupIfNeeded(request), DateTimeKind.Utc, "InPackets", "OutPackets", SampleMethod.Total, false);

        var result = ApplyColorsForChartsIfNeeded(new ChartDataResults(ComputeSubseries(request, dateRange, data)), request);
        result.AppliedLimitation = request.AppliedLimitation;
		var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);

		return CalculateMinMaxForYAxis(dynamicResult);
    }

    [WebMethod]
    public ChartDataResults TotalBytesTransferred(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request, NumberOfSeriesPerInterface);

        // we may have data in history tables which covers longer period than is sampling interval.
        // These data needs to be adjusted so they reflect average value for that sampling interval.
        // Example: Lets say we have daily data OutBytes=100MB, this means 100MB transmitted per day.
        // Lets say the sampling period is 1 hour, so we need to adjust our daily data to actually correspond
        // with the sampling period, thus we need to push into chart the daily value averaged to hourly value which for 100MB/day is around 4MB/hour.
        // This way the chart displays more granular data.
        // Note: Unfortunately the data are not distributed to smaller intervals. For example when we have daily data and sampling interval 1hr, we still get only 1 value for that day, not 24 values (as would be the day value distributed).
        // This causes that the chart contains gaps, but this is by design at the moment.
        var sampleSizeInSeconds = request.SampleSizeInMinutes * 60;
        string sql = string.Format(@"
        SELECT it.DateTime, 
           CASE when it.Weight > {0} THEN
			ROUND(it.In_TotalBytes / it.Weight * {0},0)
			ELSE
			it.In_TotalBytes
			END as InBytes,
           CASE when IT.Weight > {0} THEN
			ROUND(it.Out_TotalBytes / it.Weight * {0},0)
			ELSE
			it.Out_TotalBytes
			END as OutBytes
        FROM InterfaceTraffic it
        INNER JOIN Interfaces ON Interfaces.InterfaceID = it.InterfaceID
        WHERE it.InterfaceID = @NetObjectId AND it.DateTime >= @StartTime AND it.DateTime <= @EndTime                    
        ORDER BY [DateTime]
        ", sampleSizeInSeconds);

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);

        var data = DoSimpleLoadData(request, dateRange, sql, GetInterfaceLookupIfNeeded(request), DateTimeKind.Utc, "InBytes", "OutBytes", SampleMethod.Total, false);

        var result = ApplyColorsForChartsIfNeeded(new ChartDataResults(ComputeSubseries(request, dateRange, data)), request);
        result.AppliedLimitation = request.AppliedLimitation;
		var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);

		return CalculateMinMaxForYAxis(dynamicResult);
    }

    protected ChartDataResults DoSimpleLoadMinMaxAvgData_InOut(ChartDataRequest request, DateRange dateRange, string dataSql, string labelNamesSwql, DateTimeKind dateFormatInDatabase,
        string inAverageSeriesTag, string inMinMaxSeriesTag,
        string outAverageSeriesTag, string outMinMaxSeriesTag)
    {
        var limitedSql = Limitation.LimitSQL(dataSql);

        var allSeries = new List<DataSeries>();

        var dynamicLoader = new DynamicLoader(request, new ChartWidthSampleSizeCalculator());

        Dictionary<string, string> labelNames = null;

        if (request.NetObjectIds.Length >= 1 && !string.IsNullOrWhiteSpace(labelNamesSwql))
        {
            labelNames = LoadDisplayNameLookup(request.NetObjectIds, labelNamesSwql);
        }

        int dynamicSampleSize = dynamicLoader.CalculateDynamicSampleSize(dateRange);

        foreach (var netObjectId in request.NetObjectIds)
        {
            string label = null;
            if (labelNames != null)
            {
                labelNames.TryGetValue(netObjectId, out label);
            }

            var parameters = new[]
                {
                    new SqlParameter("NetObjectId", netObjectId),
                    new SqlParameter("StartTime", dateRange.StartDate),
                    new SqlParameter("EndTime", dateRange.EndDate)
                };

            var rawSeries = GetData(limitedSql, parameters, dateFormatInDatabase, netObjectId,
                inAverageSeriesTag, "inmin", "inmax",
                outAverageSeriesTag, "outmin", "outmax");

            /* in */
            {
                var rawAverage = rawSeries[0];
                var rawMin = rawSeries[1];
                var rawMax = rawSeries[2];

                var sampledAverage = rawAverage.CreateSampledSeries(dateRange, dynamicSampleSize,
                                                                    SampleMethod.Average);

                var sampledMin = rawMin.CreateSampledSeries(dateRange, dynamicSampleSize, SampleMethod.Min);
                var sampledMax = rawMax.CreateSampledSeries(dateRange, dynamicSampleSize, SampleMethod.Max);

                var sampledMinMax = DataSeries.CombineMinMaxSeries(sampledMin, sampledMax, inMinMaxSeriesTag);

                sampledAverage.TagName = inAverageSeriesTag;
                sampledAverage.Label = label;
                sampledAverage.ComputedFrom = inAverageSeriesTag;
                sampledMinMax.TagName = inMinMaxSeriesTag;
                sampledMinMax.Label = label;

                foreach (var serie in ComputeSubseries(request, dateRange, new[] { sampledAverage }, false))
                    allSeries.Add(serie);

                allSeries.Add(sampledMinMax);
            }

            /* out */
            {
                var rawAverage = rawSeries[3];
                var rawMin = rawSeries[4];
                var rawMax = rawSeries[5];

                var sampledAverage = rawAverage.CreateSampledSeries(dateRange, dynamicSampleSize,
                                                                    SampleMethod.Average);

                var sampledMin = rawMin.CreateSampledSeries(dateRange, dynamicSampleSize, SampleMethod.Min);
                var sampledMax = rawMax.CreateSampledSeries(dateRange, dynamicSampleSize, SampleMethod.Max);

                var sampledMinMax = DataSeries.CombineMinMaxSeries(sampledMin, sampledMax, outMinMaxSeriesTag);


                sampledAverage.TagName = outAverageSeriesTag;
                sampledAverage.Label = label;
                sampledAverage.ComputedFrom = outAverageSeriesTag;
                sampledMinMax.TagName = outMinMaxSeriesTag;
                sampledMinMax.Label = label;

                foreach (var serie in ComputeSubseries(request, dateRange, new[] { sampledAverage }, false))
                    allSeries.Add(serie);

                allSeries.Add(sampledMinMax);
            }
        }
        var result = new ChartDataResults(allSeries);
        result.AppliedLimitation = request.AppliedLimitation;
        return dynamicLoader.SetDynamicChartOptions(result);
    }


    protected void ApplyLimitOnNumberOfMMAvgNetObjects(ChartDataRequest request, int seriesPerNetObject = 1)
    {
        var limit = GetLimitOfSeries(request);
        var beforeLimit = request.NetObjectIds.Count();

        request.NetObjectIds = request.NetObjectIds.Take(limit / (
            seriesPerNetObject +
            (request.CalculateTrendLine ? seriesPerNetObject / 2 : 0) +
            (request.Calculate95thPercentile ? seriesPerNetObject / 2 : 0))
        ).ToArray();

        if (beforeLimit > request.NetObjectIds.Count())
            request.AppliedLimitation = true;
    }


    [WebMethod]
    public ChartDataResults MMAvgPps(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfMMAvgNetObjects(request, NumberOfMMSeriesPerInterface);
        const string sql =
            @"SELECT it.DateTime, 
                                it.In_AvgUniCastPkts as InAvg,
                                it.In_MinUniCastPkts as InMin,
                                it.In_MaxUniCastPkts as InMax,

                                it.Out_AvgUniCastPkts as OutAvg,
                                it.Out_MinUniCastPkts as OutMin,
                                it.Out_MaxUniCastPkts as OutMax

                             From InterfaceTraffic it
                            INNER JOIN Interfaces ON Interfaces.InterfaceID = it.InterfaceID
                             WHERE 
                                it.InterfaceID = @NetObjectId
                                AND it.DateTime >= @StartTime AND it.DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        dateRange = this.SetCustomDateRangeBasedOnRequest(request, dateRange, DateTimeKind.Utc);
        var data = DoSimpleLoadMinMaxAvgData_InOut(request, dateRange, sql, GetInterfaceLookupIfNeeded(request), DateTimeKind.Utc, "InAvgPps",
                                             "InMinMaxPps", "OutAvgPps", "OutMinMaxPps");

        var result = ApplyColorsForChartsIfNeeded(data, request);
        result.AppliedLimitation = request.AppliedLimitation;
		var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);

		return CalculateMinMaxForYAxis(dynamicResult);
    }

    [WebMethod]
    public ChartDataResults MMAvgBps(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfMMAvgNetObjects(request, NumberOfMMSeriesPerInterface);
        const string sql =
            @"SELECT it.DateTime, 
                                it.In_Averagebps as InAvg,
                                it.In_Minbps as InMin,
                                it.In_Maxbps as InMax,

                                it.Out_Averagebps as OutAvg,
                                it.Out_Minbps as OutMin,
                                it.Out_Maxbps as OutMax

                             From InterfaceTraffic it
                            INNER JOIN Interfaces ON Interfaces.InterfaceID = it.InterfaceID
                             WHERE 
                                it.InterfaceID = @NetObjectId
                                AND it.DateTime >= @StartTime AND it.DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        dateRange = this.SetCustomDateRangeBasedOnRequest(request, dateRange, DateTimeKind.Utc);
        var data = DoSimpleLoadMinMaxAvgData_InOut(request, dateRange, sql, GetInterfaceLookupIfNeeded(request), DateTimeKind.Utc, "InAvgBps",
                                             "InMinMaxBps", "OutAvgBps", "OutMinMaxBps");

        var result = ApplyColorsForChartsIfNeeded(data, request);
        result.AppliedLimitation = request.AppliedLimitation;
		var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);

		return CalculateMinMaxForYAxis(dynamicResult);
    }

    [WebMethod]
    public ChartDataResults MMAvgUtil(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfMMAvgNetObjects(request, NumberOfMMSeriesPerInterface);
        const string sql =
            @"SELECT it.DateTime, 
                                (it.In_Averagebps / Interfaces.InBandwidth) * 100 as InAvg,
                                (it.In_Minbps / Interfaces.InBandwidth) * 100 as InMin,
                                (it.In_Maxbps / Interfaces.InBandwidth) * 100 as InMax,

                                (it.Out_Averagebps / Interfaces.OutBandwidth) * 100 as OutAvg,
                                (it.Out_Minbps / Interfaces.OutBandwidth) * 100 as OutMin,
                                (it.Out_Maxbps / Interfaces.OutBandwidth) * 100 as OutMax

                             From InterfaceTraffic it
                             INNER JOIN Interfaces ON Interfaces.InterfaceID=it.InterfaceID
                             WHERE 
                                it.InterfaceID = @NetObjectId
                                AND Interfaces.InBandwidth > 0 
                                AND Interfaces.OutBandwidth > 0
                                AND it.DateTime >= @StartTime AND it.DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        dateRange = this.SetCustomDateRangeBasedOnRequest(request, dateRange, DateTimeKind.Utc);
        var data = DoSimpleLoadMinMaxAvgData_InOut(request, dateRange, sql, GetInterfaceLookupIfNeeded(request), DateTimeKind.Utc, "InAvgUtil",
                                             "InMinMaxUtil", "OutAvgUtil", "OutMinMaxUtil");

        var result = ApplyColorsForChartsIfNeeded(data, request);
        result.AppliedLimitation = request.AppliedLimitation;
		var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);

		return CalculateMinMaxForYAxis(dynamicResult);
    }

    [WebMethod]
    public ChartDataResults InterfaceErrorsDiscards(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request, NumberOfSeriesPerInterface * 2);
        const string sql = @"SELECT ie.DateTime, 
                                ie.In_Errors as InErrors,
                                ie.In_Discards as InDiscards,
                                ie.Out_Errors as OutErrors,
                                ie.Out_Discards as OutDiscards
                             From InterfaceErrors ie
                             INNER JOIN Interfaces ON Interfaces.InterfaceID=ie.InterfaceID
                             WHERE 
                                ie.InterfaceID = @NetObjectId
                                AND ie.DateTime >= @StartTime AND ie.DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);

        var data = DoSimpleLoadData(request, dateRange, sql, GetInterfaceLookupIfNeeded(request), DateTimeKind.Utc,
            new[] { "InErrors", "InDiscards", "OutErrors", "OutDiscards" }, SampleMethod.Total, false).ToList();

        dynamic options = new JsonObject();

        options.seriesTemplates.InErrors.color = "#C0C0FF";
        options.seriesTemplates.OutErrors.color = "#C0FFC0";
        options.seriesTemplates.OutDiscards.color = "#00C000";
        options.seriesTemplates.InDiscards.color = "#4040FF";

        var result = ApplyColorsForChartsIfNeeded(new ChartDataResults(ComputeSubseries(request, dateRange, data)), request, options);
        result.AppliedLimitation = request.AppliedLimitation;
		var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);

		return CalculateMinMaxForYAxis(dynamicResult);
    }

    [WebMethod]
    public ChartDataResults InInterfaceErrorsDiscards(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request, NumberOfSeriesPerInterface * 2);
        const string sql = @"SELECT ie.DateTime, 
                                (ie.In_Errors+ie.In_Discards) as InErrorsDiscards
                             From InterfaceErrorsByHours ie
                             INNER JOIN Interfaces ON Interfaces.InterfaceID=ie.InterfaceID
                             WHERE 
                                ie.InterfaceID = @NetObjectId
                                AND ie.DateTime >= @StartTime AND ie.DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);

        var data = DoSimpleLoadData(request, dateRange, sql, GetInterfaceLookupIfNeeded(request), DateTimeKind.Utc,
            new[] {"InErrorsDsicards"}, SampleMethod.Total, false).ToList();

        dynamic options = new JsonObject();

        options.seriesTemplates.InErrorsDiscards.color = "#C0C0FF";

        var result = ApplyColorsForChartsIfNeeded(new ChartDataResults(ComputeSubseries(request, dateRange, data)), request, options);
        result.AppliedLimitation = request.AppliedLimitation;
        var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);

        return CalculateMinMaxForYAxis(dynamicResult);
    }

    [WebMethod]
    public ChartDataResults OutInterfaceErrorsDiscards(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request, NumberOfSeriesPerInterface * 2);
        const string sql = @"SELECT ie.DateTime, 
                                (ie.Out_Errors+ie.Out_Discards) as OutErrorsDiscards
                             From InterfaceErrorsByHours ie
                             INNER JOIN Interfaces ON Interfaces.InterfaceID=ie.InterfaceID
                             WHERE 
                                ie.InterfaceID = @NetObjectId
                                AND ie.DateTime >= @StartTime AND ie.DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);

        var data = DoSimpleLoadData(request, dateRange, sql, GetInterfaceLookupIfNeeded(request), DateTimeKind.Utc,
            new[] { "OutErrorsDsicards" }, SampleMethod.Total, false).ToList();

        dynamic options = new JsonObject();

        options.seriesTemplates.OutErrorsDiscards.color = "#C0C0FF";

        var result = ApplyColorsForChartsIfNeeded(new ChartDataResults(ComputeSubseries(request, dateRange, data)), request, options);
        result.AppliedLimitation = request.AppliedLimitation;
        var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);

        return CalculateMinMaxForYAxis(dynamicResult);
    }

    [WebMethod]
    public ChartDataResults InterfaceErrors(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request, NumberOfSeriesPerInterface);
        const string sql = @"SELECT ie.DateTime, 
                                ie.In_Errors as InErrors,
                                ie.Out_Errors as OutErrors
                             From InterfaceErrors ie
                             INNER JOIN Interfaces ON Interfaces.InterfaceID=ie.InterfaceID
                             WHERE 
                                ie.InterfaceID = @NetObjectId
                                AND ie.DateTime >= @StartTime AND ie.DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);

        var data = DoSimpleLoadData(request, dateRange, sql, GetInterfaceLookupIfNeeded(request), DateTimeKind.Utc, "InErrors", "OutErrors", SampleMethod.Total, false);

        var result = ApplyColorsForChartsIfNeeded(new ChartDataResults(ComputeSubseries(request, dateRange, data)), request);
        result.AppliedLimitation = request.AppliedLimitation;
		var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);

		return CalculateMinMaxForYAxis(dynamicResult);
    }

    [WebMethod]
    public ChartDataResults MCastPps(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request, NumberOfSeriesPerInterface);
        const string sql = @"SELECT it.DateTime, 
                                it.In_AvgMultiCastPkts as InAvg,
                                it.Out_AvgMultiCastPkts as OutAvg
                             From InterfaceTraffic it
                             INNER JOIN Interfaces ON Interfaces.InterfaceID = it.InterfaceID
                             WHERE 
                                it.InterfaceID = @NetObjectId
                                AND it.DateTime >= @StartTime AND it.DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);

        var data = DoSimpleLoadData(request, dateRange, sql, GetInterfaceLookupIfNeeded(request), DateTimeKind.Utc, "InAvg", "OutAvg", SampleMethod.Average, false);

        var result = ApplyColorsForChartsIfNeeded(new ChartDataResults(ComputeSubseries(request, dateRange, data)), request);
        result.AppliedLimitation = request.AppliedLimitation;
		var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);

		return CalculateMinMaxForYAxis(dynamicResult);
	}

    [WebMethod]
    public ChartDataResults InterfaceDiscards(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request, NumberOfSeriesPerInterface);
        const string sql = @"SELECT ie.DateTime, 
                                ie.In_Discards as InDiscards,
                                ie.Out_Discards as OutDiscards
                             From InterfaceErrors ie
                             INNER JOIN Interfaces ON Interfaces.InterfaceID=ie.InterfaceID
                             WHERE 
                                ie.InterfaceID = @NetObjectId
                                AND ie.DateTime >= @StartTime AND ie.DateTime <= @EndTime
                             ORDER BY [DateTime]";

        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);

        var data = DoSimpleLoadData(request, dateRange, sql, GetInterfaceLookupIfNeeded(request), DateTimeKind.Utc, "InDiscards", "OutDiscards", SampleMethod.Total, false);

        var result = ApplyColorsForChartsIfNeeded(new ChartDataResults(ComputeSubseries(request, dateRange, data)), request);
        result.AppliedLimitation = request.AppliedLimitation;
		var dynamicResult = new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);

		return CalculateMinMaxForYAxis(dynamicResult);
    }

	//Calculate minimal and maximal value for Y-axis. When is minVal higher then 0, we use 0.
	private ChartDataResults CalculateMinMaxForYAxis(ChartDataResults result)
	{
        double minVal;
        double maxVal;
		bool noPoints = CalculateMinMaxValueForDataSeries(result.DataSeries, out minVal, out maxVal);

		minVal=Math.Min(minVal,0);		//when no points result is double.MaxValue
		if(!noPoints)
			maxVal = 0;

		if(result.ChartOptionsOverride == null)
			result.ChartOptionsOverride = new JsonObject();

        dynamic options = result.ChartOptionsOverride as JsonObject;
		if(options==null)
			_log.Error("ChartOptionsOverride is not a JsonObject");

        options.yAxis = JsonObject.CreateJsonArray(1);
        options.yAxis[0].min = minVal;
        options.yAxis[0].minRange = 10;

        result.ChartOptionsOverride = options;

        return result;
	}
}
