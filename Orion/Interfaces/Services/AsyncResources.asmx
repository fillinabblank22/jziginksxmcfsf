﻿<%@ WebService Language="C#" Class="InterfacesAsyncResources" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using SolarWinds.Interfaces.Common.Models.Discovery;
using SolarWinds.Interfaces.Web.DAL;
using SolarWinds.Logging;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common.ModuleManager;
using SolarWinds.Orion.Core.Common.PackageManager;
using SolarWinds.Orion.Core.Models.Enums;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Interfaces.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.Web.Plugins;
using SolarWinds.Interfaces.Common.Enums;
using SolarWinds.Orion.Web.InformationService;
using Newtonsoft.Json;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class InterfacesAsyncResources : WebService
{
    private F5InterfaceHelper f5helper = null;
    protected F5InterfaceHelper F5Helper
    {
        get
        {
            // lazy load, we do not need this class for all interfaces
            if (f5helper == null)
                f5helper = new F5InterfaceHelper();

            return f5helper;
        }
    }

    [WebMethod]
    public object GetInterfaceDetails(int interfaceId, int viewLimitationId)
    {
        // add view info with limitation id to apply limitations when NetObjectFactory.Create is called
        if (viewLimitationId > 0 && !HttpContext.Current.Items.Contains(typeof(ViewInfo).Name))
        {
            var viewInfo = new ViewInfo("", "", "", false, false, typeof(Interface));
            viewInfo.LimitationID = viewLimitationId;
            HttpContext.Current.Items[typeof(ViewInfo).Name] = viewInfo;
        }

        Interface iface = (Interface)NetObjectFactory.Create(string.Format("I:{0}", interfaceId));

        double errorThreshold = SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting("NetPerfMon-PercentUtilization-Error").SettingValue;
        double warningThreshold = SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting("NetPerfMon-PercentUtilization-Warning").SettingValue;

        var ipAdapter = new InterfacesIPAddressHelper();
        string ipAddress = ipAdapter.GetIPAddressById(iface.InterfaceID);

        var hasVlans = VlanInterfaceHelper.IsInterfaceInVlan(iface.NodeID, iface.InterfaceIndex);
        var portType = hasVlans ? VlanInterfaceHelper.GetPortType(iface.NodeID, iface.InterfaceIndex) : VlanPortType.Unknown;
        return new
        {
            NodeUnManaged = iface.Node.UnManaged,
            NodeIsSNMP = iface.Node.IsSNMP,
            
            iface.Unmanaged,
            iface.InterfaceID,
            iface.InterfaceSubType,
            InterfaceName = MakeBreakableString(iface.Caption),
            iface.Caption,
            iface.InterfaceAlias,
            iface.InterfaceIndex,
            iface.InterfaceTypeIconPath,
            iface.InterfaceTypeDescription,
            MACAddress = iface.MACAddress.Value != "000000000000" ? iface.MACAddress.ToString() : Resources.InterfacesWebContent.NPMWEBCODE_AK0_2,
            IPAddress = string.IsNullOrEmpty(ipAddress) == false ? ipAddress : Resources.InterfacesWebContent.NPMWEBCODE_AK0_2,
            
            InterfaceStatusAltText = iface.Status.ToLocalizedString(),
            InterfaceStatusDescription = MakeBreakableString(iface.Status.ToLocalizedString()),
            InterfaceStatusImagePath = iface.StatusLEDPath(false),

            InterfaceAdminStatusAltText = iface.AdministrativeStatus.ToLocalizedString(),
            AdministrativeStatus = MakeBreakableString(iface.AdministrativeStatus.ToLocalizedString()),
            InterfaceAdminStatusImagePath = iface.AdminStatusLEDPath(true),

            InterfaceOperStatusAltText = iface.OperationalStatus.ToLocalizedString(),
            OperationalStatus = MakeBreakableString(iface.OperationalStatus.ToLocalizedString()),
            InterfaceOperStatusImagePath = iface.OperStatusLEDPath(true),

            LastStatusChange = Utils.FormatCurrentCultureDate(iface.LastStatusChange) + " " + Utils.FormatToLocalTime(iface.LastStatusChange),

            InBandwidth = iface.InBandwidth.Value > 0 ? iface.InBandwidth.ToString() : "",
            OutBandwidth = iface.OutBandwidth.Value > 0 ? iface.OutBandwidth.ToString() : "",

            InBps = iface.InBps.Value > 0 ? iface.InBps.ToString() : "",
            OutBps = iface.OutBps.Value > 0 ? iface.OutBps.ToString() : "", 
            
            NetObjectID = string.Format("I:{0}", iface.InterfaceID),
            
            InPercentUtil = FormatHelper.GetPercentFormat(iface.InPercentUtil.Value, errorThreshold, warningThreshold),
            OutPercentUtil = FormatHelper.GetPercentFormat(iface.OutPercentUtil.Value, errorThreshold, warningThreshold),
            
            InPps = iface.InPps.ToString(),
            OutPps = iface.OutPps.ToString(),

            InPktSize = iface.InPktSize.Value >= 0 ? iface.InPktSize.ToString() : "",
            OutPktSize = iface.OutPktSize.Value >= 0 ? iface.OutPktSize.ToString() : "", 
            
            InterfaceMTU = iface.InterfaceMTU.ToString(),
            InterfaceSpeed = iface.InterfaceSpeed.ToString(),
            
            F5InterfaceEnabled = iface.InterfaceSubType == (int)InterfaceSubType.F5 ? F5Helper.GetF5InterfaceEnabled(iface.InterfaceID) : "",
            F5InterfaceStatus = iface.InterfaceSubType == (int)InterfaceSubType.F5 ? F5Helper.GetF5InterfaceStatus(iface.InterfaceID) : "",
            
            Counter64 = iface.Counter64 ? Resources.InterfacesWebContent.NPMWEBCODE_VB0_30 : Resources.InterfacesWebContent.NPMWEBCODE_VB0_31,

            HasVlans = hasVlans,
            PortType = VlanInterfaceHelper.TranslatePortType(portType),
            Vlans = VlanInterfaceHelper.GenerateVlansString(portType, VlanInterfaceHelper.GetVlansByType(iface.NodeID, iface.InterfaceIndex)),
        };
    }

    private static string MakeBreakableString(object val)
    {
        // do we even support IE6?
        return FormatHelper.MakeBreakableString(val.ToString(), HttpContext.Current.Request.Browser.Browser == "IE" && HttpContext.Current.Request.Browser.MajorVersion == 6);
    }
}