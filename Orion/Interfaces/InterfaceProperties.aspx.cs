using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using CustomPropertyMgr = SolarWinds.Orion.Core.Common.CustomPropertyMgr;
using Node = SolarWinds.Orion.Core.Common.Models.Node;
using System.Web.Security.AntiXss;
using SolarWinds.Interfaces.Common;

public partial class Orion_Nodes_InterfaceProperties : System.Web.UI.Page
{
    private static readonly Log log = new Log();

    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

    protected override void OnLoad(EventArgs e)
    {
        ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
        base.OnLoad(e);
    }

    private void FillControls()
    {
        CustomProperties.CustomPropertiesTable = "Interfaces";
        bool Multiselection = false;

        if (NodeWorkflowHelper.MultiSelectedInterfaceIds.Count > 1)
            Multiselection = true;

        dependencyObjectIds.InnerText = new JavaScriptSerializer().Serialize(NodeWorkflowHelper.MultiSelectedInterfaceIds);

        InterfaceBandwidth.MultipleSelection = Multiselection;
        PollingSettings.MultipleSelection = Multiselection;

        Interface _interface;
        Interfaces interfaces = new Interfaces();

        using (ICoreBusinessLayer proxy = CoreBusinessLayerProxyCreatorFactory.GetCreator().Create(BusinessLayerExceptionHandler))
        {
            for (int i = 0; i < NodeWorkflowHelper.MultiSelectedInterfaceIds.Count; i++)
            {
                int engineId = proxy.GetEngineIdForNetObject("I:" + NodeWorkflowHelper.MultiSelectedInterfaceIds[i]);
                using (var proxy2 = InterfacesBusinessLayerProxyFactory.Instance.Create(engineId))
                {
                    try
                    {
                        _interface = proxy2.Api.GetInterface(NodeWorkflowHelper.MultiSelectedInterfaceIds[i]);
                    }
                    catch(Exception ex)
                    {
                        BusinessLayerExceptionHandler(ex);
                        throw;
                    }
                    interfaces.Add(_interface.ID, _interface);
                    blInterfaces.Items.Add(_interface.FullName);
                    proxy2.Dispose();
                }
            }
        }

        Thresholds.Initialize(interfaces.ToArray());

        if (!Multiselection)
            SingleselectionFillControl(interfaces.FindByID(NodeWorkflowHelper.MultiSelectedInterfaceIds[0]));
        else
            MultiselectionFillControls(interfaces, NodeWorkflowHelper.MultiSelectedInterfaceIds.ToArray());
    }

    private void FillControlsFromPostData()
    {
        CustomProperties.CustomPropertiesTable = "Interfaces";
        bool Multiselection = false;

        if (NodeWorkflowHelper.MultiSelectedInterfaceIdsWithEngineIds.Count > 1)
            Multiselection = true;

        InterfaceBandwidth.MultipleSelection = Multiselection;
        PollingSettings.MultipleSelection = Multiselection;

        Interfaces engineInterfaces = null;
        Interfaces interfaces = new Interfaces();
        var ids = new List<int>();

        foreach (KeyValuePair<int, List<int>> engineInterf in GetGroupedInterfaceIds())
        {
            using (var proxy = InterfacesBusinessLayerProxyFactory.Instance.Create(engineInterf.Key))
            {
                try
                {
                    engineInterfaces = proxy.Api.GetInterfacesByIds(engineInterf.Value.ToArray());
                }
                catch(Exception ex)
                {
                    BusinessLayerExceptionHandler(ex);
                    throw;
                }
            }

            foreach (Interface intf in engineInterfaces)
            {
                interfaces.Add(intf.ID, intf);
                ids.Add(intf.ID);
                blInterfaces.Items.Add(intf.FullName);
            }
        }

        dependencyObjectIds.InnerText = new JavaScriptSerializer().Serialize(ids);
        Thresholds.Initialize(interfaces.ToArray());

        if (!Multiselection)
        {
            SingleselectionFillControl(interfaces.FindByID(NodeWorkflowHelper.GetObjectId(NodeWorkflowHelper.MultiSelectedInterfaceIdsWithEngineIds[0])));
        }
        else
        {
            List<int> interfaceIds = new List<int>();
            foreach (string id in NodeWorkflowHelper.MultiSelectedInterfaceIdsWithEngineIds)
                interfaceIds.Add(NodeWorkflowHelper.GetObjectId(id));

            MultiselectionFillControls(interfaces, interfaceIds.ToArray());
        }
    }

    private void SingleselectionFillControl(Interface _interface)
    {
        InterfaceBandwidth.MultipleSelection_Selected = true;
        PollingSettings.MultipleSelection_Selected = true;

        using (ICoreBusinessLayer proxy = CoreBusinessLayerProxyCreatorFactory.GetCreator().Create(BusinessLayerExceptionHandler))
        {
            Node node = proxy.GetNodeWithOptions(_interface.NodeID, false, false);
            lblNodeName.Text = AntiXssEncoder.HtmlEncode(node.Caption, false);
            lblNodeName.NavigateUrl = Page.ResolveUrl(string.Format("../NetPerfMon/NodeDetails.aspx?NetObject=N:{0}", node.ID));
        }
        tbInterfaceCaption.Text = _interface.Caption;
        InterfaceBandwidth.CustomBandwidth = _interface.CustomBandwidth;
        InterfaceBandwidth.TransmitBandwidth = _interface.OutBandwidth;
        InterfaceBandwidth.ReceiveBandwidth = _interface.InBandwidth;

        if (_interface.UnPluggable)
            cbUnPluggable.State = ThreeStateCheckBox.CheckBoxState.Checked;
        else
            cbUnPluggable.State = ThreeStateCheckBox.CheckBoxState.Unchecked;

        PollingSettings.NodeStatusPolling = _interface.PollInterval;
        PollingSettings.CollectStatisticsEvery = _interface.StatCollection;
    }

    private void MultiselectionFillControls(Interfaces interfaces, int[] interfaceIds)
    {
        lblNodeName.Visible = false;
        tbInterfaceCaption.Visible = false;
        lblInterfaceCaption.Visible = false;

        bool unplFlag = false;
        bool notUnplFlag = false;

        for (int i = 0; i < interfaceIds.Length; i++)
            if (interfaces.FindByID(interfaceIds[i]).UnPluggable)
                unplFlag = true;
            else
                notUnplFlag = true;

        Interface _interface = interfaces.FindByID(interfaceIds[0]);

        ThreeStateCheckBox.CheckBoxState unpluggable = ThreeStateCheckBox.CheckBoxState.Unchecked;

        if (unplFlag && notUnplFlag)
            unpluggable = ThreeStateCheckBox.CheckBoxState.HalfChecked;
        else if (unplFlag && !notUnplFlag)
            unpluggable = ThreeStateCheckBox.CheckBoxState.Checked;


        bool custBand = true;
        bool inBand = true;
        bool outBand = true;
        bool pollInterval = true;
        bool statCollEvery = true;

        for (int i = 1; i < interfaceIds.Length; i++)
        {
            if (_interface.CustomBandwidth != interfaces.FindByID(interfaceIds[i]).CustomBandwidth)
                custBand = false;

            if (_interface.InBandwidth != interfaces.FindByID(interfaceIds[i]).InBandwidth)
                inBand = false;

            if (_interface.OutBandwidth != interfaces.FindByID(interfaceIds[i]).OutBandwidth)
                outBand = false;

            if (_interface.PollInterval != interfaces.FindByID(interfaceIds[i]).PollInterval)
                pollInterval = false;

            if (_interface.StatCollection != interfaces.FindByID(interfaceIds[i]).StatCollection)
                statCollEvery = false;
        }

        if (custBand)
            InterfaceBandwidth.CustomBandwidth = _interface.CustomBandwidth;

        if (inBand)
            InterfaceBandwidth.TransmitBandwidth = _interface.OutBandwidth;

        if (outBand)
            InterfaceBandwidth.ReceiveBandwidth = _interface.InBandwidth;

        if (pollInterval)
            PollingSettings.NodeStatusPolling = _interface.PollInterval;

        if (statCollEvery)
            PollingSettings.CollectStatisticsEvery = _interface.StatCollection;

        cbUnPluggable.State = unpluggable;

        PollingSettings.MultipleSelection_Selected = false;
        InterfaceBandwidth.MultipleSelection_Selected = false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            NodeWorkflowHelper.FillNetObjectIds("Interfaces");
            PollingSettings.StatusPollingText = Resources.InterfacesWebContent.NPMWEBCODE_VB0_60;

            if (NodeWorkflowHelper.MultiSelectedInterfaceIds.Count != 0)
                FillControls();
            else if (NodeWorkflowHelper.MultiSelectedInterfaceIdsWithEngineIds.Count != 0)
                FillControlsFromPostData();
            else
                ReferrerRedirectorBase.Return(@"/Orion/Nodes/Default.aspx");
        }
    }
    

    #region protected void imbtnSubmit_Click(object sender, EventArgs e)
    protected void imbtnSubmit_Click(object sender, EventArgs e)
    {
		if (!Page.IsValid) return;

        if (NodeWorkflowHelper.MultiSelectedInterfaceIds.Count > 0)
            UpdateInterfaces(sender, e);
        else if (NodeWorkflowHelper.MultiSelectedInterfaceIdsWithEngineIds.Count > 0)
            UpdateInterfacesWithEngineId(sender, e);
        else
            imbtnCancel_Click(sender, e);
    }
    #endregion
    
    private void UpdateInterfaces(object sender, EventArgs e)
    {
        if (!CustomProperties.ValidateDateTimePropertiesAtServer())
            return;

        using (ICoreBusinessLayer proxy = CoreBusinessLayerProxyCreatorFactory.GetCreator().Create(BusinessLayerExceptionHandler))
        {
            Interfaces interfaces = new Interfaces();
            for (int i = 0; i < NodeWorkflowHelper.MultiSelectedInterfaceIds.Count; i++)
            {
                int engineId = proxy.GetEngineIdForNetObject("I:" + NodeWorkflowHelper.MultiSelectedInterfaceIds[i]);
                using (var proxy2 = InterfacesBusinessLayerProxyFactory.Instance.Create(engineId))
                {
                    Interface _interface = proxy2.Api.GetInterface(NodeWorkflowHelper.MultiSelectedInterfaceIds[i]);
                    Node _node = proxy.GetNodeWithOptions(_interface.NodeID, false, false);

                    UpdateInterfaceProperties(_interface);
                    interfaces.Add(_interface.ID, _interface);
                    try
                    {
                        proxy2.Api.UpdateInterfaceWithCheck(_interface, _node.UnManageFrom, _node.UnManageUntil);
                    }
                    catch (Exception ex)
                    {
                        BusinessLayerExceptionHandler(ex);
                        throw;
                    }
                }
            }
            UpdateInterfaceThresholds(interfaces);
        }
		CustomProperties.UpdateCustomPropertiesRestrictions();
        imbtnCancel_Click(sender, e);
    }

    private void UpdateInterfacesWithEngineId(object sender, EventArgs e)
    {
        if (!CustomProperties.ValidateDateTimePropertiesAtServer())
            return;

        foreach (KeyValuePair<int, List<int>> engineInterfaces in GetGroupedInterfaceIds())
        {
            Interfaces interfaces;
            using (var proxy = InterfacesBusinessLayerProxyFactory.Instance.Create(engineInterfaces.Key))
            {
                try
                {
                    interfaces = proxy.Api.GetInterfacesByIds(engineInterfaces.Value.ToArray());
                }
                catch(Exception ex)
                {
                    BusinessLayerExceptionHandler(ex);
                    throw;
                }
            }

            if (interfaces != null)
            {
                foreach (Interface _interface in interfaces)
                    UpdateInterfaceProperties(_interface);

                using (var proxy = InterfacesBusinessLayerProxyFactory.Instance.Create(engineInterfaces.Key))
                {
                    try
                    {
                        proxy.Api.UpdateInterfacesWithCheck(interfaces);
                    }
                    catch(Exception ex)
                    {
                        BusinessLayerExceptionHandler(ex);
                        throw;
                    }
                }
            }

            if (interfaces != null) UpdateInterfaceThresholds(interfaces);
        }
		CustomProperties.UpdateCustomPropertiesRestrictions();
        imbtnCancel_Click(sender, e);
    }


    private void UpdateInterfaceThresholds(Interfaces interfaces)
    {
        Thresholds.Initialize(interfaces.ToArray());
        Thresholds.UpdateThresholds();
    }


    private void UpdateInterfaceProperties(Interface _interface)
    {
        if (PollingSettings.MultipleSelection_Selected)
        {
            _interface.PollInterval = PollingSettings.NodeStatusPolling;
            _interface.StatCollection = (short)PollingSettings.CollectStatisticsEvery;
        }

        if (tbInterfaceCaption.Visible)
        {
            Node _node = null;
            using (ICoreBusinessLayer proxy = CoreBusinessLayerProxyCreatorFactory.GetCreator().Create(BusinessLayerExceptionHandler))
                _node = proxy.GetNodeWithOptions(_interface.NodeID, false, false);

            tbInterfaceCaption.Text = tbInterfaceCaption.Text.Trim();

            if (!string.IsNullOrEmpty(tbInterfaceCaption.Text))
            {
                _interface.Caption = tbInterfaceCaption.Text;
            }
            else
            {
                _interface.Caption = InterfacesHelper.GetCaption(_interface.InterfaceName, _interface.InterfaceAlias, _interface.IfName);
            }

            _interface.FullName = InterfacesHelper.GetFullName(_node.Caption, _interface.Caption);
        }

        if (InterfaceBandwidth.MultipleSelection_Selected)
        {
            _interface.CustomBandwidth = InterfaceBandwidth.CustomBandwidth;
            if (InterfaceBandwidth.CustomBandwidth)
            {
                _interface.InBandwidth = InterfaceBandwidth.ReceiveBandwidth;
                _interface.OutBandwidth = InterfaceBandwidth.TransmitBandwidth;
            }
            else
            {
                _interface.InBandwidth = _interface.InterfaceSpeed;
                _interface.OutBandwidth = _interface.InterfaceSpeed;
            }
        }

        if (cbUnPluggable.State != ThreeStateCheckBox.CheckBoxState.HalfChecked)
        {
            _interface.UnPluggable = cbUnPluggable.State == ThreeStateCheckBox.CheckBoxState.Checked ? true : false;
        }

        _interface.CustomProperties.Clear();
        foreach (KeyValuePair<string, object> prop in CustomProperties.CustomProperties)
            _interface.CustomProperties.Add(prop);
    }

    protected void imbtnCancel_Click(object sender, EventArgs e)
    {
        NodeWorkflowHelper.MultiSelectedInterfaceIds.Clear();
        NodeWorkflowHelper.MultiSelectedInterfaceIdsWithEngineIds.Clear();
        ReferrerRedirectorBase.Return(@"/Orion/Nodes/Default.aspx");
    }

    private Dictionary<int, List<int>> GetGroupedInterfaceIds()
    {
        var interfaceIdsCollection = new Dictionary<int, List<int>>();
        var engines = new List<int>();

        for (int i = 0; i < NodeWorkflowHelper.MultiSelectedInterfaceIdsWithEngineIds.Count; i++)
        {
            int id, engineID;
            string[] parts = NodeWorkflowHelper.MultiSelectedInterfaceIdsWithEngineIds[i].Split(':');
            if (parts.Length < 3 || !int.TryParse(parts[1], out id) || !int.TryParse(parts[2], out engineID))
            {
                throw new ArgumentException(string.Format("'{0}' is not a valid net object id", NodeWorkflowHelper.MultiSelectedInterfaceIdsWithEngineIds[i]));
            }

            if (!engines.Contains(engineID))
            {
                engines.Add(engineID);
            }

            if (!interfaceIdsCollection.ContainsKey(engineID))
            {
                interfaceIdsCollection[engineID] = new List<int>();
            }

            interfaceIdsCollection[engineID].Add(id);
        }
        return interfaceIdsCollection;
    }
}
