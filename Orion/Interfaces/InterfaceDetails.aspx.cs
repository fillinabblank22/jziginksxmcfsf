using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;
using System.Collections.Generic;
using SolarWinds.Interfaces.Web;
using SolarWinds.Logging;


public partial class Orion_InterfaceDetails : OrionView
{
    private static readonly Log log = new Log();

    public override string ViewType => "InterfaceDetails";

    protected void InterfacesSiteMapPath_OnInit(object sender, EventArgs e)
    {
        if (!CommonWebHelper.IsBreadcrumbsDisabled)
        {
            var renderer = new SolarWinds.Interfaces.Web.UI.InterfacesSiteMapRenderer();
            renderer.SetUpData(new KeyValuePair<string, string>("NetObject", Request["NetObject"] ?? string.Empty));
            InterfacesSiteMapPath.SetUpRenderer(renderer);
        }
    }

	protected override void OnInit(EventArgs e)
	{
        ((Orion_View)this.Master).HelpFragment = "OrionPHViewInterfaceDetails";
		this.Title = this.ViewInfo.ViewTitle;
        this.ifHost.Interface = (Interface)this.NetObject;
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        // we cannot use hidden element without content because Pendo does not support it
        if (ifHost.Interface.ShouldPendoUniqueIdBeInserted())
	    {
	        header.Attributes.Add("class", "cisco_pa_status_6541573869");
	    }
	    
        base.OnInit(e);
	}

    protected override ViewInfo GetViewForDeviceType()
	{
		if (null != this.NetObject)
		{
			int newViewID = ViewsByDeviceTypeDAL.GetViewIDForInterfaceType(((Interface)this.NetObject).InterfaceTypeDescription);
			if (newViewID > 0)
			{
				ViewInfo view = ViewManager.GetViewById(newViewID);
			    if (view == null)
			        log.WarnFormat($"ViewID {newViewID} for device type {((Node) this.NetObject).MachineType} does not exist. " +
			                       $"Reverting to default.");
			    else
			        return view;
			}
		}

		return base.GetViewForDeviceType();
	}
}
