<%@ Page Language="C#" MasterPageFile="~/Orion/Nodes/NodeManagementPage.master" AutoEventWireup="true"
    CodeFile="InterfaceProperties.aspx.cs" Inherits="Orion_Nodes_InterfaceProperties"
    Title="<%$ Resources: InterfacesWebContent, NPMWEBDATA_VB0_110%>" %>

<%@ Register Src="~/Orion/Nodes/Controls/PollingSettings.ascx" TagPrefix="orion"
    TagName="PollingSettings" %>
<%@ Register Src="~/Orion/Interfaces/Controls/InterfaceBandwidth.ascx" TagPrefix="orion"
    TagName="InterfaceBandwidth" %>
<%@ Register Src="~/Orion/Nodes/Controls/CustomProperties.ascx" TagPrefix="orion"
    TagName="CustomProperties" %>
<%@ Register Src="~/Orion/Controls/ThreeStateCheckBox.ascx" TagPrefix="orion" TagName="ThreeStateCheckBox" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/Nodes/Controls/DependenciesControl.ascx" TagPrefix="orion"
    TagName="Dependencies" %>
<%@ Register Src="~/Orion/Interfaces/Controls/InterfaceThresholdControl.ascx" TagPrefix="orion" TagName="Thresholds" %>
<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <style type="text/css">
        .sw-pg-selected-items li, ul {list-style: disc outside none; margin-left: 20px;}
        .sw-pg-selected-items-list ul { margin: 0px 20px 20px 20px; }
        table#breadcrumb { margin-bottom: 0px; }
        .sw-hdr-title { margin-top: 15px; } /* old breadcrumb + new title styling */
    </style>
    <orion:Include runat="server" File="RenderControl.js" />
</asp:Content>

<asp:Content ID="topRightContent" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionPHNodeManagementEditInterface" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <table  id="breadcrumb">
        <tr>
            <td>
                <a href="../Admin"><%=Resources.InterfacesWebContent.NPMWEBCODE_VB0_53%></a> &gt; <a href="../Nodes"><%=Resources.InterfacesWebContent.NPMWEBCODE_VB0_54%></a> &gt;
                <asp:HyperLink runat="server" ID="lblNodeName"></asp:HyperLink>
            </td>
        </tr>
    </table>
    <h1 class="sw-hdr-title"><%=Page.Title%></h1>
    <div class="sw-hdr-subtitle"><%= Resources.InterfacesWebContent.NPMWEBDATA_VB0_183 %></div>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress DynamicLayout="false" ID="UpdateProgress1" runat="server">
                <ProgressTemplate>
                    <img alt="Loading" src="../images/AJAX-Loader.gif" /><%=Resources.InterfacesWebContent.NPMWEBCODE_VB0_55%>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="sw-pg-selected-items sw-pg-selected-items-list" >
                <asp:BulletedList runat="server" ID="blInterfaces">
                </asp:BulletedList>
            </div>
            <div class="node-groupbox">
                <table width="100%">
                    <tr><td>
                    <div class="contentBlockHeader">
                        <%=Resources.InterfacesWebContent.NPMWEBCODE_VB0_57%>
                    </div>
                    </td></tr>
                    <tr><td>
                    <table class="blueBox"  >
                    <tr>
                        <td class="leftLabelColumn">
                            <asp:Label runat="server" ID="lblInterfaceCaption"><%=Resources.InterfacesWebContent.NPMWEBCODE_VB0_58%> </asp:Label>
                        </td>
						<td class="rightInputColumn">
							<asp:TextBox Width="75%" ID="tbInterfaceCaption" runat="server"></asp:TextBox>
						</td>
                    </tr>
                    <tr>
                        <td class="leftLabelColumn">
                            &nbsp;
                        </td>
                        <td class="rightInputColumn">
                            <orion:ThreeStateCheckBox runat="server" ID="cbUnPluggable" State="HalfChecked" /><%=Resources.InterfacesWebContent.NPMWEBCODE_VB0_59%>
                        </td>
                    </tr>
                    </table>
                    </td></tr>
                    <tr>
                        <td colspan="2">
                            <orion:InterfaceBandwidth runat="server" ID="InterfaceBandwidth"></orion:InterfaceBandwidth>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <orion:PollingSettings runat="server" ID="PollingSettings" HideTopologySection="true" />
                        </td>
                    </tr>
				</table>

                <orion:CustomProperties runat="server" ID="CustomProperties" NetObjectType="Interfaces" />

                <div id="dependencyObjectIds" style="display:none" runat="server" />
                <div id="dependenciesAsyncResource">
                    <div class="leftSectionTitle" style="margin-top: 10px"><%= Resources.CoreWebContent.WEBDATA_AK0_385 %></div>
                    <br/>
                    <img src="/Orion/images/loading_gen_small.gif"/>
                    <span style="padding-left: 5px; vertical-align: top;"><%=Resources.InterfacesWebContent.DependenciesLoadingMessage%></span>
                </div>

                <orion:Thresholds runat="server" ID="Thresholds" />
            </div>
            <br />
            <table width="100%" style="padding-left:10px;">
                <tr>
                    <td class="leftLabelColumn">
                        &nbsp;
                    </td>
                    <td class="rightInputColumn" style="padding-left:12px !important;">
                    <div class="sw-btn-bar">
                        <orion:LocalizableButton runat="server" LocalizedText="Submit" DisplayType="Primary"
                            ID="imbtnSubmit" OnClick="imbtnSubmit_Click" />&nbsp;
                        <orion:LocalizableButton runat="server" ID="imbtnCancel" CausesValidation="false" OnClick="imbtnCancel_Click"
                            LocalizedText="Cancel" DisplayType="Secondary" />
                    </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            var objectIds = $("div[id$='dependencyObjectIds").text();

            SW.Core.Loader.Control(
                '#dependenciesAsyncResource',
                { Control: '/Orion/Nodes/Controls/DependenciesControl.ascx' },
                { 'config': { 'ObjectPrefix': 'I', 'ObjectIdsInJson': objectIds } },
                'replace',
                function() { $.error('Error while loading DependenciesControl resource.'); },
                null
            );
        }
    </script>
</asp:Content>
