<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InterfacePopup.aspx.cs" Inherits="Orion_NPM_InterfacePopup" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>
<%@ Register TagPrefix="npm" TagName="PercentUtilization" Src="~/Orion/Interfaces/Controls/PercentUtilization.ascx" %>
<%@ Register TagPrefix="npm" TagName="ErrorsDiscards" Src="~/Orion/Interfaces/Controls/ErrorsDiscards.ascx" %>

<%--This is intended to be displayed as the body of a cluetip tooltip/popup.--%>

<h3 class="Status<%=this.Interface.Status.ToString()%>"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_71%></h3>

<div class="NetObjectTipBody">
	<p class="StatusDescription"><%:this.Interface.Caption%></p>
	<table cellpadding="0" cellspacing="0">
	    <tr runat="server" ID="AlertsMuted">
	        <th><%=Resources.InterfacesWebContent.NPMWEBDATA_ToolTip_AlertStatus %></th>
            <td colspan="2"><%= this.AlertStatus %></td>
	    </tr>
		<tr runat="server" id="F5InterfaceStatusRow" visible="false">
			<th><%=Resources.InterfacesWebContent.F5InterfaceStatus%></th>
			<td colspan="2"><asp:Label runat="server" ID="F5InterfaceStatus" Text="" /></td>
		</tr>
		<tr runat="server" id="F5InterfaceEnabledRow" visible="false">
			<th><%=Resources.InterfacesWebContent.F5InterfaceEnabled%></th>
			<td colspan="2"><asp:Label runat="server" ID="F5InterfaceEnabled" Text="" /></td>
		</tr>

		<tr>
			<th><%=Resources.InterfacesWebContent.NPMWEBCODE_VB0_21%></th>
			<td colspan="2"><%=this.Interface.OperationalStatus.ToLocalizedString()%></td>
		</tr>
		<tr>
			<th><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_62%></th>
			<td colspan="2"><%=this.Interface.AdministrativeStatus.ToLocalizedString()%></td>
		</tr>
		<tr>
			<th><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_63%></th>
			<td colspan="2"><img src="<%=this.Interface.InterfaceTypeIconPath %>" /> <%=this.Interface.InterfaceTypeDescription%></td>
		</tr>

        <tr runat="server" id="PortType" visible="false">
            <th><%=Resources.InterfacesWebContent.NPMWEBDATA_GK0_9%></th>
            <td colspan="2"><asp:Label runat="server" ID="PortTypeLabel" Text="" /></td>
        </tr>
		<tr runat="server" id="PortWWNRow" visible="false">
			<th><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_64%></th>
			<td colspan="2"><asp:Label runat="server" ID="PortWWNLabel" Text="" /></td>
		</tr>

		<tr runat="server" id="TxRow">
			<th colspan="3" style="font-style:italic"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_65%></th>
		</tr>
	    <tr runat="server" id="BpsTxRow">
		    <th><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_70%></th>
		    <td colspan="2"><%= this.Interface.OutBps.ToString() %></td>
	    </tr>
	    <tr runat="server" id="PercentUtilTxRow">
		    <th><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_66%></th>
		    <td><npm:PercentUtilization runat="server" ID="PercentUtilizationTx" /></td>
		    <orion:InlineBar runat="server" ID="PercentUtilizationTxBar" />
	    </tr>
        <tr runat="server" id="ErrorsTxRow" visible="false">
	        <th><asp:Label runat="server" ID="ErrorsTxLabel" Text="<%$ Resources: InterfacesWebContent, NPMWEBDATA_VB0_67%>" /></th>
	        <td><npm:ErrorsDiscards runat="server" ID="ErrorsTx" /></td>
	        <orion:InlineBar runat="server" ID="ErrorsTxBar" />
        </tr>
        <tr runat="server" id="DiscardsTxRow" visible="false">
	        <th><asp:Label runat="server" ID="DiscardsTxLabel" Text="<%$ Resources: InterfacesWebContent, NPMWEBDATA_VB0_68%>" /></th>
	        <td><npm:ErrorsDiscards runat="server" ID="DiscardsTx" /></td>
	        <orion:InlineBar runat="server" ID="DiscardsTxBar" />
        </tr>
		<tr runat="server" id="RxRow">
			<th colspan="3" style="font-style:italic"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_69%></th>
		</tr>
	    <tr runat="server" id="BpsRxRow">
		    <th><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_70%></th>
		    <td colspan="2"><%= this.Interface.InBps.ToString() %></td>
	    </tr>
	    <tr runat="server" id="PercentUtilRxRow">
		    <th><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_66%></th>
		    <td><npm:PercentUtilization runat="server" ID="PercentUtilizationRx" /></td>
		    <orion:InlineBar runat="server" ID="PercentUtilizationRxBar" />
	    </tr>
        <tr runat="server" id="ErrorsRxRow" visible="false">
	        <th><asp:Label runat="server" ID="ErrorsRxLabel" Text="<%$ Resources: InterfacesWebContent, NPMWEBDATA_VB0_67%>" /></th>
	        <td><npm:ErrorsDiscards runat="server" ID="ErrorsRx" /></td>
	        <orion:InlineBar runat="server" ID="ErrorsRxBar" />
        </tr>
        <tr runat="server" id="DiscardsRxRow" visible="false">
	        <th><asp:Label runat="server" ID="DiscardsRxLabel" Text="<%$ Resources: InterfacesWebContent, NPMWEBDATA_VB0_68%>" /></th>
	        <td><npm:ErrorsDiscards runat="server" ID="DiscardsRx" /></td>
	        <orion:InlineBar runat="server" ID="DiscardsRxBar" />
        </tr>
	</table>
</div>