<%@ Page Language="C#" MasterPageFile="~/Orion/View.master" AutoEventWireup="true" 
         CodeFile="InterfaceDetails.aspx.cs" Inherits="Orion_InterfaceDetails" 
         Title="Interface Details"%>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="npm" Assembly="SolarWinds.Interfaces.Web" Namespace="SolarWinds.Interfaces.Web.UI" %>
<%@ Register TagPrefix="npm" TagName="NodeLink" Src="~/Orion/NetPerfMon/Controls/NodeLink.ascx" %>
<%@ Register TagPrefix="npm" TagName="InterfaceLink" Src="~/Orion/Interfaces/Controls/InterfaceLink.ascx" %>
<%@ Register TagPrefix="npm" TagName="IntegrationIcons" Src="~/Orion/NetPerfMon/Controls/IntegrationIcons.ascx" %>
<%@ Import Namespace="SolarWinds.Orion.NPM.Web" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Register TagPrefix="orion" TagName="Refresher" Src="~/Orion/Controls/Refresher.ascx" %>
<%@ Register TagPrefix="orion" TagName="StatusIconControl" Src="~/Orion/NetPerfMon/Controls/StatusIconControl.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ViewPageTitle" Runat="Server">
    <%if (!CommonWebHelper.IsBreadcrumbsDisabled)
               { %>
               <br />
	            <div style="margin-left: 10px" >
                    <orion:FileBasedDropDownMapPath ID="InterfacesSiteMapPath" runat="server" Provider="InterfacesSitemapProvider" SiteMapFilePath="\Orion\Interfaces\Interfaces.sitemap" OnInit="InterfacesSiteMapPath_OnInit">
                    <RootNodeTemplate>
                        <a href="<%# Eval("url") %>" ><u> <%# Eval("title") %></u> </a>
                    </RootNodeTemplate>
                    <CurrentNodeTemplate>
				        <a href="<%# Eval("url") %>" ><%# Eval("title") %> </a>
				    </CurrentNodeTemplate>
                    </orion:FileBasedDropDownMapPath>
				</div>
				<%} %>
	<h1 runat="server" id="header">
	    <span style="float: right; padding-right: 30px;">
	        <npm:IntegrationIcons runat="server" NodeID='<%$ Code: this.NetObject["NodeID"] %>' />
	    </span>

        <%= ViewInfo.IsSubView ? WebSecurityHelper.HtmlEncode(ViewInfo.ViewGroupTitle) : ViewInfo.ViewHtmlTitle %> 
	    - <img src="<%= ((SolarWinds.Orion.NPM.Web.Interface) this.NetObject).Node.Status.ToString("smallled", null) %>" alt="Status"  />
        <npm:NodeLink runat="server" NodeID='<%$ Code: string.Format("N:{0}", this.NetObject["NodeID"]) %>' />
	    - <orion:StatusIconControl ID="statusIconControl" runat="server" EntityFullName="Orion.NPM.Interfaces" EntityIdName="InterfaceID" EntityId='<%$ Code: this.NetObject["InterfaceID"] %>' />
        <npm:InterfaceLink runat="server" InterfaceID='<%$ Code: this.NetObject.NetObjectID %>'>
	        <Content>
	            <asp:PlaceHolder runat="server"><%= WebSecurityHelper.HtmlEncode(((Interface)this.NetObject).Caption) %></asp:PlaceHolder>
	        </Content>
	      </npm:InterfaceLink>
        <%= ViewInfo.IsSubView ? ViewInfo.ViewHtmlTitle : string.Empty%>
	</h1>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="MainContentPlaceHolder">
	<npm:InterfaceResourceHost runat="server" ID="ifHost">
	    <orion:ResourceContainer runat="server" ID="resContainer" />
	</npm:InterfaceResourceHost>
</asp:Content>
