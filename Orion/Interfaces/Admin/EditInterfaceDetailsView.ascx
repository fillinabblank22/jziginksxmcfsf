<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditInterfaceDetailsView.ascx.cs" Inherits="Orion_Interfaces_Admin_EditInterfaceDetailsView" %>

<asp:ListBox runat="server" ID="lbxInterfaceDetails" SelectionMode="single" Rows="1" >
    <asp:ListItem Text="<%$Resources:InterfacesWebContent,NPMWEBDATA_AK1_1%>" Value="0" />
    <asp:ListItem Text="<%$Resources:InterfacesWebContent,NPMWEBDATA_AK1_2%>" Value="-1" />
</asp:ListBox>
<%-- More ListItems will be populated at run-time --%>