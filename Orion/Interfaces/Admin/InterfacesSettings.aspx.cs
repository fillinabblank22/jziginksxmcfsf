using System;

using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;

public partial class Orion_Interfaces_Admin_InterfacesSettings : System.Web.UI.Page
{
    protected override void OnLoad(EventArgs e)
    {
        ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
        base.OnLoad(e);
    }

    protected override void OnInit(EventArgs e)
    {
        stgErrorsDiscardsError.Setting = Thresholds.ErrorsDiscardsError;
        stgErrorsDiscardsWarning.Setting = Thresholds.ErrorsDiscardsWarning;
        stgIfUtilError.Setting = Thresholds.IfPercentUtilizationError;
        stgIfUtilWarning.Setting = Thresholds.IfPercentUtilizationWarning;
        
        base.OnInit(e);
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        stgErrorsDiscardsError.Setting.SaveSetting(Convert.ToDouble(stgErrorsDiscardsError.Value));
        stgErrorsDiscardsWarning.Setting.SaveSetting(Convert.ToDouble(stgErrorsDiscardsWarning.Value));
        stgIfUtilError.Setting.SaveSetting(Convert.ToDouble(stgIfUtilError.Value));
        stgIfUtilWarning.Setting.SaveSetting(Convert.ToDouble(stgIfUtilWarning.Value));

        ReferrerRedirectorBase.Return("/Orion/Admin/");
    }
}
