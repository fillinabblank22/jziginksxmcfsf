﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Interfaces.Common.Models.Discovery;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Web;

public partial class Orion_Interfaces_AddProfileWizardSteps_InterfacesFilteringStep : ResultsWizardBasePage, IStep
{
    protected override void Next()
    {
        if (!ValidateUserInput()) return;
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

	if (!IsPostBack)
        {
            interfacesList.DataBind();
        }
    }

    #region IStep Members

    public string Step
    {
        get { return "Interfaces"; }
    }

    #endregion
}
