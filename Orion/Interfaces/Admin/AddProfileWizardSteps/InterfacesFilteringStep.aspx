﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Discovery/Results/ResultsWizardPage.master" AutoEventWireup="true" 
    CodeFile="InterfacesFilteringStep.aspx.cs" Inherits="Orion_Interfaces_AddProfileWizardSteps_InterfacesFilteringStep" Title="<%$ Resources:InterfacesWebContent,NPMWEBDATA_TM0_3%>"%>
<%@ Register Src="~/Orion/InterfacesDiscovery/Controls/Discovery/InterfaceFilteringList.ascx" TagPrefix="orion" TagName="InterfacesFiltering" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <orion:InterfacesFiltering ID="interfacesList" runat="server"
        FilteringServiceUrl="/Orion/Interfaces/Admin/AddProfileWizardSteps/InterfacesFiltering.asmx/" 
        NoResultsKbLinkUrl="https://www.solarwinds.com/documentation/kbloader.aspx?lang={0}&kb=4540"
        NoResultsLabelText="<%#(Resources.InterfacesWebContent.INTERFACESFILTERING_MP0_1)%>"
        NoResultsInfoText="<%#(Resources.InterfacesWebContent.INTERFACESFILTERING_MP0_2)%>"
        HeaderText="<%#(Resources.InterfacesWebContent.INTERFACESFILTERING_MP0_4)%>"
        NumOfItemsSelectedText="<%#(Resources.InterfacesWebContent.INTERFACESFILTERING_MP0_5)%>"
        ReselectItemsText="<%#(Resources.InterfacesWebContent.INTERFACESFILTERING_MP0_19)%>"
        ListOfItemsText="<%#(Resources.InterfacesWebContent.INTERFACESFILTERING_MP0_10)%>"
        TypeLabelText="<%#(Resources.InterfacesWebContent.INTERFACESFILTERING_MP0_16)%>"
        AliasLabelText="<%#(Resources.InterfacesWebContent.INTERFACESFILTERING_MP0_18)%>" />
            
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton id="imgBack" runat="server" DisplayType="Secondary" LocalizedText="Back" OnClick="imgbBack_Click"/>
        <orion:LocalizableButton id="imgbNext" runat="server" DisplayType="Primary" LocalizedText="Next"  OnClick="imgbNext_Click"/>
        <orion:LocalizableButton id="imgbCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="imgbCancel_Click" CausesValidation="false"/>
    </div>

</asp:Content>
