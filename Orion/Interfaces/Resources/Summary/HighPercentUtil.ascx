<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HighPercentUtil.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Summary_HighPercentUtil" EnableViewState="false" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>

<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>

<orion:Include ID="Include1" runat="server" Module="Interfaces" File="CombinedSearchBox.css" Section="Top" />

<style type="text/css">
    div .progressValue
    {
        float:left; font-size:8pt; margin-right:3px;
    }

    div .progressValue.warning
    {
        color: red;
    }

    div .progressValue.critical
    {
        color: red;
        font-weight: bold;
    }
</style>

<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <orion:CustomQueryTable runat="server" ID="CustomTable"/>

        <script type="text/javascript">
            $(function () {

                $(".HeaderBar:has(.sw-combined-searchbox)").addClass("clearfix");

                function ShowProgress(value, warning, error, link)
                {
                    var barColor = value < warning ? "normal" : value < error ? "warning" : "critical";
                    var valuePart = '<div class="progressValue ' + barColor + '">' + value + '%</div>';

                    return '<a href="' + link + '" target="_blank">' + valuePart + '</div><div class="utilization-bar ' + barColor + '" style="width:80px; float:left"><div class="progress" style="width:' + value*0.8 + 'px"></div></div></a>'; 
                }

                function ToolsetLink(value, url, isToolset, ip, dns, sysName, community, ifIndex, ifName)
                {
                    var attributes = 'href="' + url + '"';

                    if(isToolset && ip)
                    {
                        var hostName = dns.trim();
                        if(!hostName)
                            hostName = sysName.trim();

                        attributes += ' ip="' + ip + '"';
                        attributes += ' community="GUID{' + community + '}"';

                        if(hostName)
                            attributes += ' hostname="' + hostName + '"';

                        if(ifIndex)
                        {
                            attributes += ' ifindex="' + ifIndex + '"';
                            attributes += ' ifname="' + ifName + '"';
                        }
                    }

                    return '<a ' + attributes + '>' + value + '</a>';
                }

                var utilizationURL = "/Orion/Charts/CustomChart.aspx?ChartName=AvgUtil-Step&Period=Today&NetObject=I:";

                SW.Core.Resources.CustomQuery.initialize(
                    {
                        // This must be unique ID for the whole page. It should be the same as CustomTable uses.
                        uniqueId: <%= CustomTable.UniqueClientID %>,
                        // Which page should resource display when loaded
                        initialPage: 0,
                        // How many rows there can be on one page. If there are more rows, paging toolbar is displayed.
                        rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "5" %>,
                        // If you want a search feature, this must be set to client ID of search textbox
                        searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
                        // If you want a search feature, this must be set to client ID of search button
                        searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
                        // True to allow sorting of rows by clicking on column headers
                        allowSort: true,
                        // This holds advanced settings that can be defined per-column
                        columnSettings: {
                            // Use column name (or alias if it's defined) from SWQL query to define which column this setting applies to. Column name is case sensitive.
                            "Receive": {
                                // You can overwrite column header (taken by default from SWQL) with any value that can also contain HTML tags.
                                header: '<%= Resources.InterfacesWebContent.NPMWEBDATA_VB0_39 %>',
                                // This can be used to format value. Arguments are:
                                // value - current cell value
                                // row - array of all values for the whole row to be able to get values from other cells
                                formatter: function (value, row, cellInfo) {
                                    return ShowProgress(value, <%= WarningLevel %>, <%= ErrorLevel %>, utilizationURL + row[3]);

                                },
                                // If this is true, content of cell is rendered as HTML. If it false (default), HTML tags are escaped.
                                isHtml: true
                            },
                            "Transmit": {
                                header: '<%= Resources.InterfacesWebContent.NPMWEBDATA_VB0_40 %>',
                                formatter: function (value, row, cellInfo) {
                                    return ShowProgress(value, <%= WarningLevel %>, <%= ErrorLevel %>, utilizationURL + row[3]);
                                },
                                isHtml: true
                            },
                            "Interface": {
                                header: '<%= Resources.InterfacesWebContent.NPMWEBCODE_VB0_6 %>',
                                formatter: function (value, row, cellInfo) {
                                    return ToolsetLink(value, row[6], '<%= IsToolset %>', row[11], row[12], row[13], row[14], row[9], row[10]);
                                },
                                isHtml: true
                            },
                            "Node": {
                                header: '<%= Resources.InterfacesWebContent.NPMWEBDATA_VB0_37 %>',
                                formatter: function (value, row, cellInfo) {
                                    return ToolsetLink(value, row[2], '<%= IsToolset %>', row[11], row[12], row[13], row[14], null, null);
                                },
                                isHtml: true
                            }
                        }
                    });
 
                // This reloads data for this resource (see the same ID as we used for uniqueId in initialize method)
                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
                // This registers resource in view refresh routine
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                // And finally loads initial data
                refresh();
            });
        </script>
    </Content>
</orion:ResourceWrapper>
