using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;

using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Core.Web;

public partial class Orion_NetPerfMon_Resources_Summary_HighErrorsDiscardsToday : BaseResourceControl
{
	protected void Page_Load(object sender, EventArgs e)
	{
		if (String.IsNullOrEmpty(this.Resource.SubTitle))
		{
			this.Resource.SubTitle = Resources.InterfacesWebContent.NPMWEBCODE_VB0_122;
		}
		
		DataTable table;
		try
		{
			table = SqlDAL.GetErrorsDiscardsToday(this.Resource.Properties["Filter"]);
		}
		catch (SqlException)
		{
			this.SQLErrorPanel.Visible = true;
			return;
		}

		this.errorsDiscardsTable.DataSource = table;
		this.errorsDiscardsTable.DataBind();
	}

	
	protected override string DefaultTitle
	{
		get { return Resources.InterfacesWebContent.NPMWEBCODE_VB0_123; }
	}

	public override string EditControlLocation
	{
		get { return "/Orion/NetPerfMon/Controls/EditResourceControls/FilterEdit.ascx"; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceHighErrorsDiscards"; }
	}

	public override string SubTitle
	{
		get
		{
			return base.SubTitle.Replace("XX", Thresholds.ErrorsDiscardsWarning.SettingValue.ToString("0"));
		}
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

	public string FormatNodeParamString(object dataItem)
	{
		return this.Profile.ToolsetIntegration ?
			FormatHelper.GetNodeParamString(
				DataBinder.Eval(dataItem, "IP_Address").ToString(),
				DataBinder.Eval(dataItem, "DNS").ToString(),
				DataBinder.Eval(dataItem, "SysName").ToString(),
				DataBinder.Eval(dataItem, "GUID").ToString()) :
			String.Empty;
	}

	public string FormatInterfaceParamString(object dataItem)
	{
		return this.Profile.ToolsetIntegration ?
			FormatHelper.GetInterfaceParamString(
				DataBinder.Eval(dataItem, "IP_Address").ToString(),
				DataBinder.Eval(dataItem, "DNS").ToString(),
				DataBinder.Eval(dataItem, "SysName").ToString(),
				DataBinder.Eval(dataItem, "GUID").ToString(),
				DataBinder.Eval(dataItem, "InterfaceName").ToString(),
				DataBinder.Eval(dataItem, "InterfaceIndex").ToString()) :
			String.Empty;
	}

	public string FormatErrors(object value)
	{
		try
		{
			return FormatHelper.GetErrorsDiscardsFormat(Convert.ToDouble(value),
				Thresholds.ErrorsDiscardsError.SettingValue, Thresholds.ErrorsDiscardsWarning.SettingValue, Resources.InterfacesWebContent.NPMWEBCODE_VB0_35);
		}
		catch
		{
			return String.Empty;
		}
	}

	public string FormatDiscards(object value)
	{
		try
		{
			return FormatHelper.GetErrorsDiscardsFormat(Convert.ToDouble(value),
				Thresholds.ErrorsDiscardsError.SettingValue, Thresholds.ErrorsDiscardsWarning.SettingValue, Resources.InterfacesWebContent.NPMWEBCODE_VB0_34);
		}
		catch
		{
			return String.Empty;
		}
	}
}
