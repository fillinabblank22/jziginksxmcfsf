﻿using System;
using System.Collections.Generic;
using Castle.Core.Internal;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Interfaces_Resources_Summary_HealthSummary :BaseResourceControl, INodeProvider
{
    private const string InterfacesSubviewViewKey = "Interfaces Subview";

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadData();
        ShouldDisplayResource();
    }

    private void ShouldDisplayResource()
    {
        Visible = InterfacesOverviewRow.Visible;
    }

    protected override string DefaultTitle => Resources.InterfacesWebContent.HealthSummaryTitle;

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.RenderControl;

    public Node Node => GetInterfaceInstance<INodeProvider>()?.Node;

    private int ViewId => ViewManager.GetViewByViewKey(InterfacesSubviewViewKey).ViewID;

    protected string InterfacesSubviewLink => $"/ui/interfaces/interfaces-list?NetObject=N:{Node.NodeID}&ViewID={ViewId}";

    protected string ComposeUrlToInterfacesSubview(object status = null)
    {
        var url = InterfacesSubviewLink;

        if (!string.IsNullOrEmpty(status?.ToString()))
        {
            var queryString = $"&Status={status}";
            url += queryString;
        }

        return url;
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { yield return typeof(INodeProvider); }
    }
    
    private void LoadData()
    {
        LoadInterfacesData();
    }

    private void LoadInterfacesData()
    {
        using (var swis = InformationServiceProxy.CreateV3())
        {
            const string query = @"
SELECT Status,count(Status) AS cnt,StatusName
    FROM Orion.NPM.Interfaces
    LEFT JOIN Orion.StatusInfo si ON Status=si.StatusId
    WHERE NodeID=@nodeId
    GROUP BY Status,StatusName,Ranking
    ORDER BY Ranking";

            InterfacesOverviewRepeater.DataSource = swis.Query(query, new Dictionary<string, object> { { "nodeId", Node.NodeID } });
            InterfacesOverviewRepeater.DataBind();
        }

        if (InterfacesOverviewRepeater.Items.Count == 0)
        {
            InterfacesOverviewRow.Visible = false;
        }
    }

}