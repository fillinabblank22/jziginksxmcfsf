﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HealthSummary.ascx.cs" Inherits="Orion_Interfaces_Resources_Summary_HealthSummary" %>

<orion:Include ID="Include" runat="server" File="Interfaces/Styles/HealthSummary.css" />

<orion:ResourceWrapper ID="ResourceWrapper" runat="server">
    <Content>    
        <table class="NeedsZebraStripes HealthSummaryTable">
            <tr runat="server" ID="InterfacesOverviewRow">  
                <td class="PropertyHeader">
                    <img src="/Orion/Interfaces/images/Icons/Interfaces.png" /><a href="<%= this.InterfacesSubviewLink %>"><%= Resources.InterfacesWebContent.InterfacesLabel %></a>
                </td>
                <td class="Property">
                    <asp:Repeater ID="InterfacesOverviewRepeater" runat="server">
                        <ItemTemplate>
                            <span>
                                <a href="<%# this.ComposeUrlToInterfacesSubview(Eval("status")) %>">
                                    <img src='/Orion/StatusIcon.ashx?entity=Orion.Nodes&status=<%#Eval("Status")%>&size=small' alt='<%#Eval("StatusName")%>' title='<%#Eval("StatusName")%>' />
                                    <%#Eval("cnt")%>
                                </a>
                            </span>
                        </ItemTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>
    </Content>
</orion:ResourceWrapper>