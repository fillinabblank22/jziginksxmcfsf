<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HighErrorsDiscardsToday.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Summary_HighErrorsDiscardsToday" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_38%>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="errorsDiscardsTable">
            <HeaderTemplate>
                <table border="0" cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes">
	                <tr>
		                <td class="ReportHeader" colspan="2"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_37%></td>
		                <td class="ReportHeader" colspan="2"><%=Resources.InterfacesWebContent.NPMWEBCODE_VB0_6%></td>
		                <td class="ReportHeader"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_251%></td>
		                <td class="ReportHeader"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_252%></td>
		                <td class="ReportHeader"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_253%></td>
		                <td class="ReportHeader"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_254%></td>
	                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                    <tr>
				        <td class="Property" valign="middle" width="20"><img src="<%# SolarWinds.Orion.Web.UI.StatusIcons.NodeIconFactory.SmallIconURL( Eval("NodeID"), Eval("NodeStatus") ) %>" alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_108%>" style="vertical-align:bottom;" />&nbsp;</td>
				        <td class="PropertyHeader"><a <%# this.FormatNodeParamString(Container.DataItem) %>  href="/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:<%# Eval("NodeID")%>"><%# Eval("NodeName")%></a>&nbsp;</td>	
				        <td class="Property" valign="middle" width="20"><img src="/NetPerfMon/images/small-<%# Eval("StatusLED").ToString().Trim() %>" alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_108%>" style="vertical-align:bottom;" />&nbsp;</td>
				        <td class="Property"><a <%# this.FormatInterfaceParamString(Container.DataItem) %>  href="/Orion/Interfaces/InterfaceDetails.aspx?NetObject=I:<%# Eval("InterfaceID")%>"><%# Eval("Caption")%></a>&nbsp;</td>
				        <td class="Property" ><a href="/Orion/Charts/CustomChart.aspx?ChartName=InterfaceErrors&NetObject=I:<%# Eval("InterfaceID")%>&Period=Today&SampleSize=60" target="_blank"><%# this.FormatErrors(Eval("InErrorsToday"))%></a>&nbsp;</td>
				        <td class="Property" ><a href="/Orion/Charts/CustomChart.aspx?ChartName=InterfaceDiscards&NetObject=I:<%# Eval("InterfaceID")%>&Period=Today&SampleSize=60" target="_blank"><%# this.FormatDiscards(Eval("InDiscardsToday"))%></a>&nbsp;</td>
				        <td class="Property" ><a href="/Orion/Charts/CustomChart.aspx?ChartName=InterfaceErrors&NetObject=I:<%# Eval("InterfaceID")%>&Period=Today&SampleSize=60" target="_blank"><%# this.FormatErrors(Eval("OutErrorsToday"))%></a>&nbsp;</td>
				        <td class="Property" ><a href="/Orion/Charts/CustomChart.aspx?ChartName=InterfaceDiscards&NetObject=I:<%# Eval("InterfaceID")%>&Period=Today&SampleSize=60" target="_blank"><%# this.FormatDiscards(Eval("OutDiscardsToday"))%></a>&nbsp;</td>
			        </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>
