<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DownInterfaces.ascx.cs" Inherits="Orion_NetPerfMon_Resources_Summary_DownInterfaces" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="PercentStatusBar" Src="~/Orion/Controls/PercentStatusBar.ascx" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_38%>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="NodesRepeater" OnItemDataBound="ItemDataBound">
            <HeaderTemplate>
                <table border="0" cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes">
                    <tr id="Tr1" runat="server" >
                        <td class="ReportHeader">
                            &nbsp;
                        </td>
                        <td class="ReportHeader">
                            <%=Resources.InterfacesWebContent.NPMWEBCODE_VB0_6%>
                        </td>
                        <td class="ReportHeader">
                            &nbsp;
                        </td>
                        <td class="ReportHeader">
                            <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_255%>
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr runat="server" id="groupHeaderRow">
                    <td class="Property" valign="middle" style="width:20px">
                        <img alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_108%>" src="<%# SolarWinds.Orion.Web.UI.StatusIcons.NodeIconFactory.SmallIconURL( Eval("NodeID"), Eval("NodeStatus") ) %>" />&nbsp;
                    </td>
                    <td class="Property" style="font-weight: bold;" valign="middle">
                        <orion:ToolsetLink  runat="server" ID="link" 
                                            IPAddress='<%#Eval("IP_Address")%>'
                                            DNS='<%#Eval("DNS")%>'
                                            SysName='<%#Eval("SysName")%>' 
                                            CommunityGUID='<%#Eval("GUID")%>'
                                            NavigateUrl='<%#Eval("NodeURL")%>'>
                        <%#Eval("NodeName")%>
                        </orion:ToolsetLink>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr runat="server" id="itemRow">
                    <td class="Property" style="padding-left: 20px">
                        <asp:Panel ID="Panel1" runat="server" Width="40">
                            <img alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_108%>" src="/Orion/images/StatusIcons/Small-<%# Eval("StatusLED") %>" />
                            <img alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_108%>" src="/NetPerfMon/images/Interfaces/<%# Eval("InterfaceIcon") %>" />
                        </asp:Panel>
                    </td>
                    <td>
                        <orion:ToolsetLink  runat="server" ID="ToolsetLink1" 
                                            IPAddress='<%#Eval("IP_Address")%>'
                                            DNS='<%#Eval("DNS")%>'
                                            SysName='<%#Eval("SysName")%>' 
                                            CommunityGUID='<%#Eval("GUID")%>'
                                            NavigateUrl='<%#Eval("InterfaceURL")%>'
                                            InterfaceIndex='<%#Eval("InterfaceIndex")%>'
                                            InterfaceName='<%#Eval("InterfaceName")%>'>
                        <%# Eval("Caption") %>
                        </orion:ToolsetLink>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                    <%# (Eval("InterfaceLastChange") is DBNull) ? string.Empty : SolarWinds.Orion.Common.Utils.FormatCurrentCultureDate((DateTime)Eval("InterfaceLastChange")) + "&nbsp;" + SolarWinds.Orion.Common.Utils.FormatToLocalTime((DateTime)Eval("InterfaceLastChange"))%>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:ResourceWrapper>
