using System;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Core.Common;
using System.Web;


public partial class Orion_NetPerfMon_Resources_Summary_HighPercentUtil : BaseResourceControl
{
    // overriden DefaultTitle
    protected override string DefaultTitle
    {
        get { return Resources.InterfacesWebContent.NPMWEBCODE_VB0_120; }
    }

    public override string SubTitle
    {
        get
        {
            return base.SubTitle.Replace("XX", Math.Round(Thresholds.IfPercentUtilizationWarning.SettingValue).ToString() );
        }
    }

    // overriden HelpLink
    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceInterfacesHighPercentUtilization";
        }
    }

	public override string EditControlLocation
	{
		get { return "/Orion/Interfaces/Controls/EditResourceControls/EditHighPercentUtil.ascx"; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected int WarningLevel { get; set; }
    protected int ErrorLevel { get; set; }
    protected string Filter { get; set; }

    protected string IsToolset
    {
        get 
        {
            var toolset = HttpContext.Current.Profile.GetPropertyValue("ToolsetIntegration");
            return (toolset != null && (bool)toolset) ? "true" : String.Empty;
        }
    }

    protected ResourceSearchControl SearchControl { get; private set; }

    // This is here to make resource working in reports. Resources in reports get negative ID assigned.
    // If we use negative ID in CustomQueryTable control, it won't work because it's used
    // on some places where JS does not allow it.
    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    public string SWQL
    {
        get
        {
            return String.Format(@"
SELECT 
n.Caption as [Node], 
'/Orion/StatusIcon.ashx?entity=Orion.Nodes&size=Small&id=' + TOSTRING(n.NodeID) + '&status=' + TOSTRING(n.Status) AS [_IconFor_Node],
n.DetailsUrl AS [_NodeURL],
i.InterfaceID as [_InterfaceID],
i.Caption as [Interface],
'/Orion/images/StatusIcons/Small-' + i.StatusLED AS [_IconFor_Interface],
i.DetailsUrl AS [_InterfaceURL],
i.InPercentUtil AS [Receive], 
i.OutPercentUtil AS [Transmit],
i.InterfaceIndex as [_ifIndex],
i.InterfaceName as [_ifName],
n.IPAddress as [_IP],
n.DNS as [_DNS], 
n.SysName as [_SysName],
n.WebCommunityString.GUID as [_Community]
FROM Orion.NPM.Interfaces i
INNER JOIN Orion.Nodes n ON (n.NodeID = i.NodeID)
WHERE (i.InPercentUtil >= {0} OR i.OutPercentUtil >= {0}) {1}", WarningLevel, String.IsNullOrEmpty(Filter) ? String.Empty : "AND " + Filter);
        }
    }

    public string SearchSWQL
    {
        get
        {
            return SWQL + @" AND (i.Caption LIKE '%${SEARCH_STRING}%' OR n.Caption LIKE '%${SEARCH_STRING}%')";
        }
    }

    public string OrderBy
    {
        get
        {
            return "(i.InPercentUtil + i.OutPercentUtil) DESC";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Filter = Resource.Properties["Filter"];
        if (!String.IsNullOrEmpty(Filter))
            Filter = CommonHelper.FormatFilter(Filter);

        ErrorLevel = Convert.ToInt32(Thresholds.IfPercentUtilizationError.SettingValue);
        WarningLevel = Convert.ToInt32(Thresholds.IfPercentUtilizationWarning.SettingValue);

        INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();
        if (nodeProvider != null)
        {
            if (String.IsNullOrEmpty(Filter))
                Filter = String.Format("n.NodeID={0}", nodeProvider.Node.NodeID);
            else
                Filter = String.Format("{0} AND n.NodeID={1}", Filter, nodeProvider.Node.NodeID);
        }

        // Load search control and add it to resource header area next to Edit button.
        // This is currently easiest way how to do that.
        SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        var searchControlWrapper = new HtmlGenericControl("div");
        searchControlWrapper.Attributes["class"] = "sw-combined-searchbox";
        searchControlWrapper.Controls.Add(SearchControl);

        ResourceWrapper.HeaderButtons.Controls.Add(searchControlWrapper);

        // Set unique ID CustomQueryTable instance that you put to .ascx file
        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        // Set SWQL query that loads data to display
        CustomTable.SWQL = SWQL;
        // Set search SWQL query that is used for searching
        CustomTable.SearchSWQL = SearchSWQL;
        // Optional - set default order by for the table (otherwise first column)
        CustomTable.OrderBy = OrderBy;
    }

}

