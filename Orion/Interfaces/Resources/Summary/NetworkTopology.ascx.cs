using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NPM_Resources_Summary_NetworkTopology : SolarWinds.Orion.Web.UI.BaseResourceControl
{
	private int _pagecount;
	private int _itemsCount;
	private string _lastSWQLError;

	public bool IsResourceShownAtReportsPage
	{
		get
		{
			return Request.Url.AbsolutePath.ToLowerInvariant().Contains("/orion/reports/") || Request.Url.AbsolutePath.ToLowerInvariant().Contains("/orion/report.aspx");
		}
	}

	public string LastSWQLError
	{
		get
		{
			return _lastSWQLError;
		}
	}

	public int PageIndex
	{
		get
		{
			int pageIntex = 0;
			int.TryParse(hfPageIndex.Value, out pageIntex);
			return pageIntex;
		}
		set
		{
			hfPageIndex.Value = value.ToString();
		}
	}

	public string SearchStr
	{
		get { return hfSearchStr.Value; }
		set { hfSearchStr.Value = value; }
	}

    public string LayerStr
    {
        get { return hfLayerStr.Value; }
        set { hfLayerStr.Value = value; }
    }

	public bool IsShowedAll
	{
		get
		{
			bool val;
			bool.TryParse(hfShowAll.Value, out val);
			return val;
		}
		set
		{
			hfShowAll.Value = value.ToString();
		}
	}

	public int PageCount
	{
		get { return _pagecount; }
		set { _pagecount = value; }
	}

	public int ItemsCount
	{
		get { return _itemsCount; }
		set { _itemsCount = value; }
	}

    /// <summary>
    /// count items on previous page. Is is 0 (first page) or PageCount on next pages
    /// </summary>
    public int PrevCount
    {
        get { 
            if (PageIndex == 0)
                return 0;
            else
                return PageCount; 
        }        
    }

    /// <summary>
    /// count items on next page. Is is 0 (last page), PageCount (pages 1- last - 2) or something between 0 and PageCount (last but one page)
    /// </summary>
    public int NextCount
    {
        get
        {
            if ((PageIndex + 2) * PageCount > ItemsCount)
            {
                if ((PageIndex + 1) * PageCount > ItemsCount) // last page
                    return 0;
                else
                    return ItemsCount - ((PageIndex + 1) * PageCount); // last but one page
            }
            else
                return PageCount;
        }

    }



	protected override string DefaultTitle
	{
		get
		{
			if (Resource != null)
			{
				string title = String.IsNullOrEmpty(Resource.Title) ? Resources.InterfacesWebContent.NPMWEBCODE_VB0_117 : Resource.Title;
				Resource.Properties["Title"] = title;
				return title;
			}
			return Resources.InterfacesWebContent.NPMWEBCODE_VB0_117;
		}
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceNPMTopology"; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

        // Show this resource only if NPM is installed
        if (!SolarWinds.Orion.Core.Common.ModuleManager.ModuleManager.InstanceWithCache.IsThereModule("NPM"))
        {
            this.Visible = false;
            return;
        }

		int pagecount;
		if (int.TryParse(Resource.Properties["ShowItemsCount"], out pagecount))
			PageCount = pagecount;
		else
			PageCount = 5;

        LinkButtonRefreshResource.Style.Add("display", "none");

		if (!IsPostBack)
		{
			string netObjectType;
			int netObjectID = -1;

			GetCurrentNetObjectID(out netObjectType, out netObjectID);
			var nodeSrcFilter = Resource.Properties["NodeSrcFilter"];
			var nodeDestFilter = Resource.Properties["NodeDestFilter"];
			var interfaceSrcFilter = Resource.Properties["InterfaceSrcFilter"];
			var interfaceDestFilter = Resource.Properties["InterfaceDestFilter"];

			DataTable table = null;
			_lastSWQLError = string.Empty;

            layerList.Items.Insert(0, new ListItem(Resources.InterfacesWebContent.LAYERFILTERING_PR0_1, "ALL"));
            layerList.Items.Insert(1, new ListItem(Resources.InterfacesWebContent.LAYERFILTERING_PR0_2, "L2"));
            layerList.Items.Insert(2, new ListItem(Resources.InterfacesWebContent.LAYERFILTERING_PR0_3, "L3"));
            layerList.ClearSelection();
            ListItem li = layerList.Items.FindByValue("ALL");
            if (li != null)
            {
                li.Selected = true;
            }

			try
			{
				using (WebDAL webdal = new WebDAL())
				{
                    ItemsCount = webdal.GetNPMNetworkTopologyConnectionItemsCount(netObjectType, netObjectID, nodeSrcFilter, nodeDestFilter,
                        interfaceSrcFilter, interfaceDestFilter, SearchStr, string.Empty);
                    table = webdal.GetNPMNetworkTopologyConnectionsTable(netObjectType, netObjectID, nodeSrcFilter, nodeDestFilter,
                    interfaceSrcFilter, interfaceDestFilter, string.Empty, 1, PageCount, string.Empty);
				}
			}
			catch (System.ServiceModel.FaultException ex)
			{
				if (ex is System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>)
				{
					_lastSWQLError = ((System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract>)(ex)).Detail.Message;
						if (ex.Message.Contains("RunQuery failed") && (!string.IsNullOrEmpty(nodeSrcFilter) ||
						!string.IsNullOrEmpty(nodeDestFilter) ||
						!string.IsNullOrEmpty(interfaceSrcFilter) ||
						!string.IsNullOrEmpty(interfaceDestFilter)))
						{
							this.SQLErrorPanel.Visible = true;
							this.updatePanel.Visible = false;
							return;
						}
				}
				base.HandleResourceException(ex);
			}
            catch (SolarWinds.Orion.Web.InformationService.SwisQueryException ex)
            {
                _lastSWQLError = ex.LocalizedMessage;
                if (ex.Cause == SolarWinds.Orion.Web.InformationService.SwisQueryException.Reason.RunQueryFailed &&
                    (!string.IsNullOrEmpty(nodeSrcFilter) || !string.IsNullOrEmpty(nodeDestFilter)))
                {
                    this.SQLErrorPanel.Visible = true;
                    this.updatePanel.Visible = false;
                    return;
                }
                base.HandleResourceException(ex);
            }
			catch (Exception ex)
			{
				base.HandleResourceException(ex);
			}

			PageIndex = 0;
			SetPagerStates();

			if (IsResourceShownAtReportsPage)
			{
				IsShowedAll = true;
				gridFooter.Visible = false;
			}

			if (table == null || table.Rows.Count == 0)
			{
				if (string.IsNullOrEmpty(nodeSrcFilter + nodeDestFilter + interfaceSrcFilter + interfaceDestFilter) && Resource.Properties["AutoHide"] == "1")
					this.Visible = false;
				else
					this.TopologyData.Visible = false;
				InitializeLinkButtons();
				return;
			}

			this.TopologyData.DataSource = table;
			this.TopologyData.DataBind();

			SearchStr = string.Empty;
		}
		if (string.IsNullOrEmpty(tbSearch.Value) || tbSearch.Value.Equals(Resources.InterfacesWebContent.NPMWEBDATA_VB0_250, StringComparison.OrdinalIgnoreCase))
		{
			tbSearch.Style["color"] = "#555";
			tbSearch.Style["font-style"] = "italic";
			tbSearch.Value = Resources.InterfacesWebContent.NPMWEBDATA_VB0_250;
		}
		else
		{
			tbSearch.Style["color"] = "#000";
			tbSearch.Style["font-style"] = "normal";
		}

        tbSearch.Attributes.Add("onclick", String.Format("if (this.value == '{0}') {1} this.style.fontStyle = 'normal'; this.style.fontSize = '9pt'", Resources.InterfacesWebContent.NPMWEBDATA_VB0_250, "{ this.value = ''; this.style.color = '#000'; }"));
        tbSearch.Attributes.Add("onblur", String.Format("if (this.value == '') {0}", String.Format("this.value = '{0}'; this.style.color = '#555'; this.style.fontStyle = 'italic'; this.style.fontSize = '9pt'; ", Resources.InterfacesWebContent.NPMWEBDATA_VB0_250)));
	    InitializeLinkButtons();
	}

	protected void GetCurrentNetObjectID(out string netObjectType, out int netObjectID)
	{
		netObjectType = String.Empty;
		netObjectID = -1;

		string netObjectString = Request.QueryString["NetObject"];

		if (!String.IsNullOrEmpty(netObjectString))
		{
			string[] temp = netObjectString.Split(':');
			if (temp.Length == 2)
			{
				if (Int32.TryParse(temp[1].Trim(), out netObjectID))
				{
					netObjectType = temp[0].Trim();
					return;
				}
			}
		}
	}

	protected void TopologyData_IndexChanging(object sender, CommandEventArgs e)
	{
		switch (e.CommandArgument.ToString().ToLowerInvariant())
		{
			case "next":
				SetItemsCount();
				PageIndex = PageIndex + 1;
				break;
			case "prev":
				SetItemsCount();
				PageIndex = PageIndex - 1;
				break;
			case "all":
				SetSearchString();
				SetItemsCount();
				PageIndex = 0;
				PageCount = ItemsCount;
				IsShowedAll = true;
                break;
            case "refresh":
                SetItemsCount();
                break;
            case "first":
                SetItemsCount();
				PageIndex = 0;
                break;
            case "last":
                SetItemsCount();
                int maxpages = 0;
                if (PageCount > 0)
                    maxpages = ItemsCount / PageCount + ((ItemsCount % PageCount) > 0 ? 1 : 0);
		        PageIndex = maxpages - 1;
                break;
            
		}

		string netObjectType;
		int netObjectID = -1;

		GetCurrentNetObjectID(out netObjectType, out netObjectID);

        DataBind();
	}

	private void SetItemsCount()
	{
		string netObjectType;
		int netObjectID = -1;

		GetCurrentNetObjectID(out netObjectType, out netObjectID);

		var nodeSrcFilter = Resource.Properties["NodeSrcFilter"];
		var nodeDestFilter = Resource.Properties["NodeDestFilter"];
		var interfaceSrcFilter = Resource.Properties["InterfaceSrcFilter"];
		var interfaceDestFilter = Resource.Properties["InterfaceDestFilter"];

		try
		{
			using (WebDAL webdal = new WebDAL())
                ItemsCount = webdal.GetNPMNetworkTopologyConnectionItemsCount(netObjectType, netObjectID, nodeSrcFilter, nodeDestFilter,
                                        interfaceSrcFilter, interfaceDestFilter, SearchStr, LayerStr);
		}
		catch (Exception ex)
		{
			base.HandleResourceException(ex);
		}

	}

	private new void DataBind()
	{
		string netObjectType;
		int netObjectID = -1;

		if (IsShowedAll)
		{
			PageIndex = 0;
			PageCount = ItemsCount;
		}

		GetCurrentNetObjectID(out netObjectType, out netObjectID);

		var nodeSrcFilter = Resource.Properties["NodeSrcFilter"];
		var nodeDestFilter = Resource.Properties["NodeDestFilter"];
		var interfaceSrcFilter = Resource.Properties["InterfaceSrcFilter"];
		var interfaceDestFilter = Resource.Properties["InterfaceDestFilter"];

		int fromRow = (PageIndex * PageCount) + 1;
		int toRow = fromRow + PageCount - 1;
		DataTable table = null;

		try
		{
			using (WebDAL webdal = new WebDAL())
                table = webdal.GetNPMNetworkTopologyConnectionsTable(netObjectType, netObjectID, nodeSrcFilter, nodeDestFilter,
                    interfaceSrcFilter, interfaceDestFilter, SearchStr, fromRow, toRow, LayerStr);
		}
		catch (Exception ex)
		{
			base.HandleResourceException(ex);
		}

        if (table == null || table.Rows.Count == 0)
        {
            TopologyData.Visible = false;
        }
        else
        {
            TopologyData.Visible = true;
            TopologyData.DataSource = table;
            TopologyData.DataBind();
            SetPagerStates();            
        }        
        InitializeLinkButtons();
	}

	private void SetSearchString()
	{
		if (!string.IsNullOrEmpty(tbSearch.Value) && !tbSearch.Value.Equals(Resources.InterfacesWebContent.NPMWEBDATA_VB0_250, StringComparison.OrdinalIgnoreCase))
			SearchStr = tbSearch.Value.Replace("<wbr>", "").Replace("\u200B", "");
		else
			SearchStr = string.Empty;
	}

	protected void Search_Click(object sender, EventArgs e)
	{
		PageIndex = 0;

		SetSearchString();
		SetItemsCount();
		DataBind();
	}

	private void SetPagerStates()
	{
		lbPrev.Enabled = (PageIndex != 0);
		lbNext.Enabled = (PageIndex < ((double)ItemsCount / PageCount - 1));
	}

	protected string MakeBreakableString(object val)
	{
		if (Request.Browser.Browser == "IE" && Request.Browser.MajorVersion == 6)
			return FormatHelper.MakeBreakableString(val.ToString(), true);
		else
            return FormatHelper.MakeBreakableString(val.ToString(), false).Replace(":", ":<wbr>");
	}

	protected string HighlightSearchValues(object val)
	{
		string str = SearchStr.Replace("*", "");
		string result = val.ToString();
		if (!string.IsNullOrEmpty(str))
		{
			string[] arr = Regex.Split(val.ToString(), Regex.Escape(str), RegexOptions.IgnoreCase);
			foreach (string item in arr)
				if (!string.IsNullOrEmpty(item))
					result = Regex.Replace(result, Regex.Escape(item), new MatchEvaluator(MakeBreakable), RegexOptions.IgnoreCase);

			return Regex.Replace(
				result,
				Regex.Escape(str),
				new MatchEvaluator(ReplaceHighlight),
				RegexOptions.IgnoreCase);
		}
		return MakeBreakableString(val);
	}

	private string MakeBreakable(Match m)
	{
		return MakeBreakableString(m.ToString());
	}

	private string ReplaceHighlight(Match m)
	{
		return string.Format("<span class=\"selectedSearchStr\" >{0}</span>", MakeBreakableString(m));
	}

	public override string EditControlLocation
	{
		get
		{
			return "/Orion/Interfaces/Controls/EditResourceControls/NetworkTopologyEdit.ascx";
		}
	}
    protected void LayerList_Selection_Change(object sender, EventArgs e)
    {
        if (string.Compare(layerList.SelectedValue, "ALL") == 0)
            LayerStr = string.Empty;
        else
            LayerStr = layerList.SelectedValue;
        PageIndex = 0;
        SetSearchString();
        SetItemsCount();
        DataBind();
    }

    protected void InitializeLinkButtons()
    {
        int maxpages = 0;

        lbFirst_Disabled.Visible = false;
        lbPrev_Disabled.Visible = false;
        lbNext_Disabled.Visible = false;
        lbLast_Disabled.Visible = false;
        lbFirst.Visible = true;
        lbPrev.Visible = true;
        lbLast.Visible = true;
        lbNext.Visible = true;
        if (PageIndex <= 0)
        {
            lbFirst.Visible = false;
            lbPrev.Visible = false;
            lbFirst_Disabled.Visible = true;
            lbPrev_Disabled.Visible = true;
        }
        
        if (PageCount > 0)
            maxpages = ItemsCount / PageCount + ((ItemsCount % PageCount) > 0 ? 1 : 0);

        if (IsShowedAll == true || PageIndex >= maxpages - 1)
        {
            lbLast.Visible = false;
            lbNext.Visible = false;
            lbNext_Disabled.Visible = true;
            lbLast_Disabled.Visible = true;
        }
        if (ItemsCount == 0)
        {
            lblPageDisplay.Text = "";
            gridFooterTraverser.Visible = false;
            gridFooterEmpty.Visible = true;
            lblNoConnections.Text = Resources.InterfacesWebContent.NPMWEBCODE_PR0_4;
        }
        else
        {
            gridFooterEmpty.Visible = false;
            gridFooterTraverser.Visible = true;
            lblPageDisplay.Text = string.Format(Resources.InterfacesWebContent.NPMWEBCODE_PR0_3, (PageIndex * PageCount) + 1, Math.Min((PageIndex + 1) * PageCount, ItemsCount), ItemsCount);
            lblPage1.Text = Resources.InterfacesWebContent.NPMWEBCODE_PR0_1;
            txtCurrentPage.Text = Convert.ToString(PageIndex + 1);
            lblPage2.Text = string.Format(Resources.InterfacesWebContent.NPMWEBCODE_PR0_2, maxpages);
        }
    }

}
