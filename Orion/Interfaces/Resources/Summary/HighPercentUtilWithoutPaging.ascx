<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HighPercentUtilWithoutPaging.ascx.cs"
    Inherits="Orion_NetPerfMon_Resources_Summary_HighPercentUtilWithoutPaging" EnableViewState="false" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="PercentStatusBar" Src="~/Orion/Controls/PercentStatusBar.ascx" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_38%>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="NodesRepeater" OnItemDataBound="ItemDataBound">
            <HeaderTemplate>
                <table border="0" cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes">
                    <tr runat="server" >
                        <td class="ReportHeader">&nbsp;
                        </td>
                        <td class="ReportHeader">
                            <%=Resources.InterfacesWebContent.NPMWEBCODE_VB0_6%>
                        </td>
                        <td class="ReportHeader">&nbsp;
                        </td>
                        <td class="ReportHeader">
                            <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_39%>
                        </td>
                        <td class="ReportHeader">&nbsp;
                        </td>
                        <td class="ReportHeader">&nbsp;
                        </td>
                        <td class="ReportHeader">
                            <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_40%>
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr runat="server" id="groupHeaderRow">
                    <td class="Property" valign="middle" style="width:20px">
                        <img alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_108%>" src="<%# SolarWinds.Orion.Web.UI.StatusIcons.NodeIconFactory.SmallIconURL( Convert.ToInt32(Eval("NodeID")), Convert.ToInt32(Eval("NodeStatus")), Convert.ToInt32(Eval("ChildStatus")) ) %>" />&nbsp;
                    </td>
                    <td class="Property" valign="middle">
                        <orion:ToolsetLink  runat="server" ID="link" 
                                            IPAddress='<%#Eval("IP_Address")%>'
                                            DNS='<%#Eval("DNS")%>'
                                            SysName='<%#Eval("SysName")%>' 
                                            CommunityGUID='<%#Eval("GUID")%>'
                                            NavigateUrl='<%#Eval("NodeURL")%>'>
                        <%#Eval("NodeName")%>
                        </orion:ToolsetLink>
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr runat="server" id="itemRow">
                    <td class="Property" style="width:10px; padding-left: 20px">
                        <img alt="Status" src="/Orion/images/StatusIcons/Small-<%# Eval("StatusLED") %>" />
                    </td>
                    <td>
                        <orion:ToolsetLink  runat="server" ID="ToolsetLink1" 
                                            IPAddress='<%#Eval("IP_Address")%>'
                                            DNS='<%#Eval("DNS")%>'
                                            SysName='<%#Eval("SysName")%>' 
                                            CommunityGUID='<%#Eval("GUID")%>'
                                            NavigateUrl='<%#Eval("InterfaceURL")%>'
                                            InterfaceIndex='<%#Eval("InterfaceIndex")%>'
                                            InterfaceName='<%#Eval("InterfaceName")%>'>
                        <%# Eval("Caption") %>
                        </orion:ToolsetLink>
                    </td>
                    <td class="Property" valign="middle" align="center">
                        <asp:HyperLink ID="VolumePercerUseLink" runat="server" NavigateUrl='<%# Eval("InterfaceURL") %>'
                            Target="_blank">
                            <orion:PercentLabel runat="server" ID="PercentLabel" Status='<%#Convert.ToInt32((System.Single)Eval("InPercentUtil")) %>'
                                WarningLevel='<%# WarningLevel %>' ErrorLevel='<%# ErrorLevel %>' NoLimit='<%#NoLimit %>'/>
                        </asp:HyperLink>
                    </td>
                    <td>
                        <asp:HyperLink ID="VolumeBar" runat="server" NavigateUrl='<%# Eval("InterfaceUtilizationURL") %>'
                            Target="_blank">
                            <orion:PercentStatusBar runat="server" ID="StatusBar" Status='<%#Convert.ToInt32((System.Single)Eval("InPercentUtil")) %>'
                                WarningLevel='<%#WarningLevel %>' ErrorLevel='<%#ErrorLevel %>' />
                        </asp:HyperLink>
                    </td>
                    <td>
                        &nbsp;&nbsp;
                    </td>
                    <td class="Property" valign="middle" align="center">
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Eval("InterfaceURL") %>'
                            Target="_blank">
                            <orion:PercentLabel runat="server" ID="PercentLabel1" Status='<%#Convert.ToInt32((System.Single)Eval("OutPercentUtil")) %>'
                                WarningLevel='<%# WarningLevel %>' ErrorLevel='<%# ErrorLevel %>' NoLimit='<%#NoLimit %>' />
                        </asp:HyperLink>
                    </td>
                    <td>
                        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# Eval("InterfaceUtilizationURL") %>'
                            Target="_blank">
                            <orion:PercentStatusBar runat="server" ID="PercentStatusBar1" Status='<%#Convert.ToInt32((System.Single)Eval("OutPercentUtil")) %>'
                                WarningLevel='<%#WarningLevel %>' ErrorLevel='<%#ErrorLevel %>' />
                        </asp:HyperLink>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:ResourceWrapper>
