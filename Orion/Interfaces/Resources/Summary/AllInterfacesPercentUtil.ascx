<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllInterfacesPercentUtil.ascx.cs"
    Inherits="Orion_NetPerfMon_Resources_Summary_AllInterfacesPercentUtil" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="PercentStatusBar" Src="~/Orion/Controls/PercentStatusBar.ascx" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
<orion:Include ID="Include1" runat="server" Module="Interfaces" File="AllInterfacesPercentUtil.css" Section="Top" />
<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_38%>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="NodesRepeater" OnItemDataBound="ItemDataBound">
            <HeaderTemplate>
                <table border="0" cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes AllInterfacesPercentUtil">
                    <tr id="Tr1" runat="server" >
                        <td class="ReportHeader">
                            &nbsp;
                        </td>
                        <td class="ReportHeader">
                            <%=Resources.InterfacesWebContent.NPMWEBCODE_VB0_6%>
                        </td>
                        <td class="ReportHeader">
                            &nbsp;
                        </td>
                        <td class="ReportHeader">
                            <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_39%>
                        </td>
                        <td class="ReportHeader">
                            &nbsp;
                        </td>
                        <td class="ReportHeader">
                            &nbsp;
                        </td>
                        <td class="ReportHeader">
                            <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_40%>
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr runat="server" id="groupHeaderRow" class="header-row">
                    <td class="Property" valign="middle" colspan="2">
                       <img class="node-status-icon" alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_108%>" src="<%# SolarWinds.Orion.Web.UI.StatusIcons.NodeIconFactory.SmallIconURL( Eval("NodeID"), Eval("NodeStatus") ) %>" />&nbsp;                    
                       <orion:ToolsetLink  runat="server" ID="link" 
                                            IPAddress='<%#Eval("IP_Address")%>'
                                            DNS='<%#Eval("DNS")%>'
                                            SysName='<%#Eval("SysName")%>' 
                                            CommunityGUID='<%#Eval("GUID")%>'
                                            NavigateUrl='<%#Eval("NodeURL")%>'>
                        <%#Eval("NodeName")%>
                        </orion:ToolsetLink>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr runat="server" id="itemRow">
                    <td class="Property" style="padding-left: 20px">
                        <asp:Panel ID="Panel1" runat="server" Width="40">
                            <img alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_108%>" src="/Orion/images/StatusIcons/Small-<%# Eval("StatusLED") %>" />
                            <img alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_108%>" src="/NetPerfMon/images/Interfaces/<%# Eval("InterfaceIcon") %>" />
                        </asp:Panel>
                    </td>
                    <td>
                        <orion:ToolsetLink  runat="server" ID="ToolsetLink1" 
                                            IPAddress='<%#Eval("IP_Address")%>'
                                            DNS='<%#Eval("DNS")%>'
                                            SysName='<%#Eval("SysName")%>' 
                                            CommunityGUID='<%#Eval("GUID")%>'
                                            NavigateUrl='<%#Eval("InterfaceURL")%>'
                                            InterfaceIndex='<%#Eval("InterfaceIndex")%>'
                                            InterfaceName='<%#Eval("InterfaceName")%>'>
                        <%# Eval("Caption") %>
                        </orion:ToolsetLink>
                    </td>
                    <td class="Property" valign="middle" align="center" >
                       <asp:HyperLink ID="VolumePercerUseLink" runat="server" NavigateUrl='<%# Eval("InterfaceUtilizationURL") %>'
                            Target="_blank">
                            <%# FormatPercentValue(SolarWinds.Orion.Common.Utils.SafeToInt(Eval("InPercentUtil")), (bool)Eval("BoolStatus"))%>
                        </asp:HyperLink>
                 </td>
                    <td>
                        <asp:HyperLink ID="VolumeBar" runat="server" NavigateUrl='<%# Eval("InterfaceUtilizationURL") %>'
                            Target="_blank">
                            <%# FormatPercentBar(SolarWinds.Orion.Common.Utils.SafeToInt(Eval("InPercentUtil")),50, (bool)Eval("BoolStatus"))%>
                        </asp:HyperLink>
                    </td>
                    <td>
                        &nbsp;&nbsp;
                    </td>
                    <td class="Property" valign="middle" align="center">
                       <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Eval("InterfaceUtilizationURL") %>'
                            Target="_blank">
                            <%# FormatPercentValue(SolarWinds.Orion.Common.Utils.SafeToInt(Eval("OutPercentUtil")), (bool)Eval("BoolStatus"))%>
                        </asp:HyperLink>
                    </td>
                    <td>
                        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# Eval("InterfaceUtilizationURL") %>'
                            Target="_blank">
                             <%# FormatPercentBar(SolarWinds.Orion.Common.Utils.SafeToInt(Eval("OutPercentUtil")), 50, (bool)Eval("BoolStatus"))%>
                        </asp:HyperLink>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:ResourceWrapper>
