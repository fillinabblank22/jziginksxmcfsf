using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Resources_Summary_AllInterfacesTraffic : SolarWinds.Orion.Web.UI.BaseResourceControl
{
    private string currentNodeId = String.Empty;

    #region properties

    // overriden DefaultTitle
    protected override string DefaultTitle
    {
        get { return Resources.InterfacesWebContent.NPMWEBCODE_VB0_121; }
    }

    // overriden HelpLink
    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourcePercentUtilizationAllInterfaces";
        }
    }

	public override string EditControlLocation
	{
		get { return "/Orion/NetPerfMon/Controls/EditResourceControls/FilterEdit.ascx"; }
	}

    public int WarningLevel { get; set; }
    public int ErrorLevel { get; set; }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }

    #endregion

    // returns double value or default value if conversion is not aviable or is invalid
    protected double GetNotNullValue(object what, double defaultValue)
    {
        double value = 0.0;

        try
        {
            value = Convert.ToDouble(what);
        }
        catch { return defaultValue; }

        return value;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string filter = Resource.Properties["Filter"];

        // if is netObject a Node set filter for a node
        INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();

        if (nodeProvider != null)
            if (String.IsNullOrEmpty(filter))
                filter = String.Format("NodesData.NodeID={0}", nodeProvider.Node.NodeID);
            else
                filter = String.Format("{0} AND NodesData.NodeID={1}", filter, nodeProvider.Node.NodeID);

        ErrorLevel = Convert.ToInt32(Thresholds.IfPercentUtilizationError.SettingValue);
        WarningLevel = Convert.ToInt32(Thresholds.IfPercentUtilizationWarning.SettingValue);

        DataTable table = null;

        try
        {
            table = SqlDAL.GetProblemInterfaces(filter);
        }
        catch { this.SQLErrorPanel.Visible = true; return; }

        if (table == null || table.Rows.Count == 0)
			return;
        
        table.Columns.Add("NodeURL", typeof(string));
        table.Columns.Add("InterfaceURL", typeof(string));
        table.Columns.Add("InterfaceUtilizationURL", typeof(string));
        table.Columns.Add("BoolStatus", typeof(bool));
        table.Columns.Add("FormatedInbps", typeof(string));
        table.Columns.Add("FormatedOutbps", typeof(string));

        // preprocessing
        foreach (DataRow r in table.Rows)
        {
            // node details url
            r["NodeURL"] = String.Format("/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}", r["NodeID"]);

            // volume details
            r["InterfaceURL"] = String.Format("/Orion/Interfaces/InterfaceDetails.aspx?NetObject=I:{0}", r["InterfaceID"]);

            // interface traffic chart
            r["InterfaceUtilizationURL"] = String.Format("/Orion/NetPerfMon/CustomChart.aspx?ChartName=AvgUtil-Step&NetObject=I:{0}&Period=Today", r["InterfaceID"]);

            r["InterfaceIcon"] = SolarWinds.Orion.Web.Helpers.ImageHelper.GetInterfaceTypeIcon(r["InterfaceIcon"].ToString().Trim());

			int status = Convert.ToInt32(r["Status"]);
            bool show = (status == 0 || status == 2) ? false : true;

            // interface traffic chart
            r["BoolStatus"] = show;

            r["FormatedInbps"] = show ? Utils.ConvertToMB(Utils.SafeToDouble(r["Inbps"]), "bps", false).Replace(" ", "&nbsp;") : "&nbsp;";
            r["FormatedOutbps"] = show ? Utils.ConvertToMB(Utils.SafeToDouble(r["Outbps"]), "bps", false).Replace(" ", "&nbsp;") : "&nbsp;";
        }

        // bind data to repeater
        this.NodesRepeater.DataSource = table;
        this.NodesRepeater.DataBind();
    }

    // item bound event
    protected void ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HtmlTableRow groupHeaderRow = e.Item.FindControl("groupHeaderRow") as HtmlTableRow;

            DataRowView row = e.Item.DataItem as DataRowView;
            bool newNode = false;
            if (row["NodeID"].ToString() != currentNodeId)
            {
                currentNodeId = row["NodeID"].ToString();
                newNode = true;
            }

            groupHeaderRow.Visible = newNode;
        }
    }

	protected string FormatPercentValue(int value, bool visible)
	{
		if (!visible)
			return "&nbsp;";

		try
		{
			return FormatHelper.GetPercentFormat(value, ErrorLevel, WarningLevel, false);
		}
		catch
		{ return "&nbsp;"; }
	}
}