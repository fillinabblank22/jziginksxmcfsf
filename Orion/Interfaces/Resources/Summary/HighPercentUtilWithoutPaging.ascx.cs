using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;

public partial class Orion_NetPerfMon_Resources_Summary_HighPercentUtilWithoutPaging : SolarWinds.Orion.Web.UI.BaseResourceControl
{
    private string currentNodeId = String.Empty;

    #region properties

    // overriden DefaultTitle
    protected override string DefaultTitle
    {
        get { return Resources.InterfacesWebContent.NPMWEBCODE_MP0_1; }
    }

    public override string SubTitle
    {
        get
        {
            return base.SubTitle.Replace("XX", Math.Round(Thresholds.IfPercentUtilizationWarning.SettingValue).ToString() );
        }
    }

    // overriden HelpLink
    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceInterfacesHighPercentUtilization";
        }
    }

	public override string EditControlLocation
	{
		get { return "/Orion/NetPerfMon/Controls/EditResourceControls/FilterEdit.ascx"; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    public int WarningLevel { get; set; }
    public int ErrorLevel { get; set; }
    public bool NoLimit { get; set; }

    #endregion

    // returns double value or default value if conversion is not aviable or is invalid
    protected double GetNotNullValue(object what, double defaultValue)
    {
        double value = 0.0;
        
        try
        {
            value = Convert.ToDouble(what);
        }
        catch { return defaultValue; }

        return value;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var filter = Resource.Properties["Filter"];

        // if is netObject a Node set filter for a node
        INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();

        if (nodeProvider != null)
            if (String.IsNullOrEmpty(filter))
                filter = String.Format("NodesData.NodeID={0}", nodeProvider.Node.NodeID);
            else
                filter = String.Format("{0} AND NodesData.NodeID={1}", filter, nodeProvider.Node.NodeID);

        ErrorLevel = Convert.ToInt32(Thresholds.IfPercentUtilizationError.SettingValue);
        WarningLevel = Convert.ToInt32(Thresholds.IfPercentUtilizationWarning.SettingValue);
        NoLimit = true;

        DataTable table = null;

        try
        {
            var temp = (String.IsNullOrEmpty(filter)) ? String.Empty : " AND ";
            table = SqlDAL.GetProblemInterfacesWithChildStatus(String.Format("(Interfaces.InPercentUtil > {0} OR Interfaces.OutPercentUtil > {0}) {1}{2}", WarningLevel, temp, filter), true);
        }
        catch { this.SQLErrorPanel.Visible = true; return; }

        // if there are no records do show information panel
        if (table == null || table.Rows.Count == 0)
        {
            return;
        }
        
        table.Columns.Add("NodeURL", typeof(string));
        table.Columns.Add("InterfaceURL", typeof(string));
        table.Columns.Add("InterfaceUtilizationURL", typeof(string));
        
        // preprocessing
        foreach(DataRow r in table.Rows)
        {
            // node details url
            r["NodeURL"] = String.Format("/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}", r["NodeID"]);
            
            // volume details
            r["InterfaceURL"] = String.Format("/Orion/Interfaces/InterfaceDetails.aspx?NetObject=I:{0}", r["InterfaceID"]);

            // interface traffic chart
            r["InterfaceUtilizationURL"] = String.Format("/Orion/Charts/CustomChart.aspx?ChartName=AvgUtil-Step&NetObject=I:{0}&Period=Today", r["InterfaceID"]);
        }

        // bind data to repeater
        this.NodesRepeater.DataSource = table;
        this.NodesRepeater.DataBind();
    }

    // item bound event
    protected void ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HtmlTableRow groupHeaderRow = e.Item.FindControl("groupHeaderRow") as HtmlTableRow;

            DataRowView row = e.Item.DataItem as DataRowView;
            bool newNode = false;
            if (row["NodeID"].ToString() != this.currentNodeId)
            {
                this.currentNodeId = row["NodeID"].ToString();
                newNode = true;
            }

            groupHeaderRow.Visible = newNode;
        }
    }

}

