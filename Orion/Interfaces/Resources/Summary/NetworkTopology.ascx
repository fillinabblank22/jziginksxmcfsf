<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NetworkTopology.ascx.cs"
    Inherits="Orion_NPM_Resources_Summary_NetworkTopology" %>
<%@ Import Namespace="SolarWinds.Orion.NPM.Web" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
    
<script type="text/javascript">
    $(function () {
        var refresh = function () { <%= Page.ClientScript.GetPostBackEventReference(LinkButtonRefreshResource, string.Empty) %>; };
        SW.Core.View.AddOnRefresh(refresh, '<%= updatePanel.ClientID %>');
    });
</script>

<orion:resourceWrapper ID="ResourceWrapper1" runat="server" ShowEditButton="true">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_243%> <%= LastSWQLError %>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:UpdatePanel runat="server" ID="updatePanel" UpdateMode="Conditional">
            <ContentTemplate>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td valign="middle" style="white-space: nowrap;">
                            <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_155%>
                            <asp:DropDownList id="layerList" AutoPostBack="True" runat="server" OnSelectedIndexChanged="LayerList_Selection_Change">
                            </asp:DropDownList>
                        </td>
                        <td style="white-space: nowrap;float: right;padding-right:0px;">
                            <asp:Panel ID="Panel2" CssClass="topologyDiv" Height="22px" runat="server" DefaultButton="btnSearch">
                                <input type="text" runat="server" id="tbSearch" style="min-width: 158px;float: left"/>&nbsp;
                                <span style="height: 15px;width: 15px;">
                                <asp:ImageButton runat="server" ID="btnSearch" OnClick="Search_Click" ImageUrl="~/Orion/images/search_button.gif" />
                                </span>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
                <asp:Repeater ID="TopologyData" runat="server">
                    <HeaderTemplate>
                        <table width="100%" cellspacing="0" cellpadding="1">
                            <tr>
                                <td class="ReportHeader" colspan="2">
                                    &nbsp;<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_37%>
                                </td>
                                <td class="ReportHeader" align="center">
                                    &nbsp;<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_244%>
                                </td>
                                <td class="ReportHeader" colspan="2">
                                    &nbsp;<%=Resources.InterfacesWebContent.NPMWEBCODE_VB0_6%>
                                </td>
                                <td class="ReportHeader" align="center" colspan="3">
                                    &nbsp;<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_245%>
                                </td>
                                <td class="ReportHeader" colspan="2">
                                    &nbsp;<%=Resources.InterfacesWebContent.NPMWEBCODE_VB0_6%>
                                </td>
                                <td class="ReportHeader" align="center">
                                    &nbsp;<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_246%>
                                </td>
                                <td class="ReportHeader" colspan='2'>
                                    &nbsp;<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_37%>
                                </td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td width="10px" style="border-left-style:solid;border-left-width:1px;border-left-color:#0066a4;border-bottom-width:0px">
                                <img src="/Orion/images/StatusIcons/Small-<%#Eval("AGroupStatus") %>" alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_108%>" />
                            </td>
                            <td width="20%" style="border-bottom-width:0px">
                                <orion:ToolsetLink runat="server" IPAddress='<%# Eval("AIPAddress") %>' DNS='<%# Eval("ADNS") %>'
                                    SysName='<%# Eval("ASysName") %>' CommunityGUID='<%# Eval("AGuid") %>' NodeID='<%#Eval("ANodeID") %>'
                                    ToolTip=''>
		                    <%#HighlightSearchValues(Eval("ACaption")) %>
                                </orion:ToolsetLink>
                            </td>
                            <td style="vertical-align: middle; text-align: center;
                                width: 30px;border-bottom-width:0px">
                                <img alt="" src="/Orion/images/Dash_Blue_16x16.png" />
                            </td>
                            <td width="36px" style="border-bottom-width:0px">
                                <img src="/Orion/images/StatusIcons/Small-<%#Eval("AOperStatusLED") %>" alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_108%>" />
                                <img src="/NetPerfMon/images/Interfaces/<%#Eval("AIntType") %>.gif" alt="" />
                            </td>
                            <td width="20%" style="border-bottom-width:0px">
                                <orion:ToolsetLink ID="ToolsetLink1" runat="server" IPAddress='<%# Eval("AIPAddress") %>'
                                    DNS='<%# Eval("ADNS") %>' SysName='<%# Eval("ASysName") %>' CommunityGUID='<%# Eval("AGuid") %>'
                                    InterfaceID='<%#Eval("AInterfaceID") %>' ToolTip=''>
		                    <%#HighlightSearchValues(Eval("AInterfaceName")) %>
                                </orion:ToolsetLink>
                            </td>
                            <td style="vertical-align: middle; text-align: left;border-left-style:solid;border-left-width:1px;border-left-color:#0066a4;padding-left:0px;border-bottom-width:0px">
                                <img alt="" src="/Orion/images/TopologyItems/RArrow2.png" />
                            </td>
                            <td style="vertical-align: middle; text-align: center;border-bottom-width:0px">
                                <%# Eval("LinkSpeed")%>
                            </td>
                            <td style="vertical-align: middle; text-align: right;border-right-style:solid;border-right-width:1px;border-right-color:#966a00;padding-right:0px;border-bottom-width:0px">
                                <img alt="" src="/Orion/images/TopologyItems/LArrow2.png" />
                            </td>
                            <td width="36px" style="border-bottom-width:0px">
                                <img src="/Orion/images/StatusIcons/Small-<%#Eval("BOperStatusLED") %>" alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_108%>" />
                                <img src="/NetPerfMon/images/Interfaces/<%#Eval("BIntType") %>.gif" alt="" />
                            </td>
                            <td width="20%" style="border-bottom-width:0px">
                                <orion:ToolsetLink ID="ToolsetLink2" runat="server" IPAddress='<%# Eval("BIPAddress") %>'
                                    DNS='<%# Eval("BDNS") %>' SysName='<%# Eval("BSysName") %>' CommunityGUID='<%# Eval("BGuid") %>'
                                    InterfaceID='<%#Eval("BInterfaceID") %>' ToolTip=''>
		                    <%#HighlightSearchValues(Eval("BInterfaceName")) %>
                                </orion:ToolsetLink>
                            </td>
                            <td style="vertical-align: middle; text-align: center;
                                width: 20px;border-bottom-width:0px">
                                <img alt="" src="/Orion/images/Dash_Brown_16x16.png" />
                            </td>
                            <td width="20px" style="border-bottom-width:0px">
                                <img src="/Orion/images/StatusIcons/Small-<%#Eval("BGroupStatus") %>" alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_108%>" />
                            </td>
                            <td width="20%" style="text-align: left;border-right-style:solid;border-right-width:1px;border-right-color:#966a00;border-bottom-width:0px">
                                <orion:ToolsetLink runat="server" IPAddress='<%# Eval("BIPAddress") %>' DNS='<%# Eval("BDNS") %>'
                                    SysName='<%# Eval("BSysName") %>' CommunityGUID='<%# Eval("BGuid") %>' NodeID='<%#Eval("BNodeID") %>'
                                    ToolTip=''>
		                    <%#HighlightSearchValues(Eval("BCaption")) %>
                                </orion:ToolsetLink>
                            </td>
                        </tr>          
                        <tr height="5px" >
                        <td colspan="13" style="border-bottom-style:solid;border-bottom-width:1px;border-bottom-color:#BBBBBB;">
                        </td>
                        </tr>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <tr>
                            <td width="10px" style="border-left-style:solid;border-left-width:1px;border-left-color:#0066a4;background-color: #f6f6f6;">
                                <img src="/Orion/images/StatusIcons/Small-<%#Eval("AGroupStatus") %>" alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_108%>"/>                            
                            </td>
                            <td width="20%" style="background-color: #f6f6f6;">
                                <orion:ToolsetLink ID="ToolsetLink3" runat="server" IPAddress='<%# Eval("AIPAddress") %>' DNS='<%# Eval("ADNS") %>'
                                    SysName='<%# Eval("ASysName") %>' CommunityGUID='<%# Eval("AGuid") %>' NodeID='<%#Eval("ANodeID") %>'
                                    ToolTip=''>
		                    <%#HighlightSearchValues(Eval("ACaption")) %>
                                </orion:ToolsetLink>
                            </td>                            
                            <td style="vertical-align: middle; text-align: center;
                                width: 30px;background-color: #f6f6f6;">
                                <img alt="" src="/Orion/images/Dash_Blue_16x16.png" />
                            </td>
                            <td width="36px" style="background-color: #f6f6f6;">
                                <img src="/Orion/images/StatusIcons/Small-<%#Eval("AOperStatusLED") %>" alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_108%>" />
                                <img src="/NetPerfMon/images/Interfaces/<%#Eval("AIntType") %>.gif" alt="" />
                            </td>
                            <td width="20%" style="background-color: #f6f6f6;">
                                <orion:ToolsetLink ID="ToolsetLink4" runat="server" IPAddress='<%# Eval("AIPAddress") %>'
                                    DNS='<%# Eval("ADNS") %>' SysName='<%# Eval("ASysName") %>' CommunityGUID='<%# Eval("AGuid") %>'
                                    InterfaceID='<%#Eval("AInterfaceID") %>' ToolTip=''>
		                    <%#HighlightSearchValues(Eval("AInterfaceName")) %>
                                </orion:ToolsetLink>
                            </td>
                            <td style="vertical-align: middle; text-align: left;border-left-style:solid;border-left-width:1px;border-left-color:#0066a4;padding-left:0px;background-color: #f6f6f6;">
                                <img alt="" src="/Orion/images/TopologyItems/RArrow2.png" />
                            </td>                            
                            <td style="vertical-align: middle; text-align: center;background-color: #f6f6f6;">
                                <%# Eval("LinkSpeed")%>
                            </td>
                            <td style="vertical-align: middle; text-align: right;border-right-style:solid;border-right-width:1px;border-right-color:#966a00;padding-right:0px;background-color: #f6f6f6;padding-top: 5px;">
                                <img alt="" src="/Orion/images/TopologyItems/LArrow2.png" />
                            </td>
                            <td width="36px" style="background-color: #f6f6f6;">
                                <img src="/Orion/images/StatusIcons/Small-<%#Eval("BOperStatusLED") %>" alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_108%>" />
                                <img src="/NetPerfMon/images/Interfaces/<%#Eval("BIntType") %>.gif" alt="" />
                            </td>
                            <td width="20%" style="background-color: #f6f6f6;">
                                <orion:ToolsetLink ID="ToolsetLink5" runat="server" IPAddress='<%# Eval("BIPAddress") %>'
                                    DNS='<%# Eval("BDNS") %>' SysName='<%# Eval("BSysName") %>' CommunityGUID='<%# Eval("BGuid") %>'
                                    InterfaceID='<%#Eval("BInterfaceID") %>' ToolTip=''>
		                    <%#HighlightSearchValues(Eval("BInterfaceName")) %>
                                </orion:ToolsetLink>
                            </td>
                            <td style="vertical-align: middle; text-align: center;
                                width: 20px;background-color: #f6f6f6;">
                                <img alt="" src="/Orion/images/Dash_Brown_16x16.png" />
                            </td>
                            <td width="20px" style="background-color: #f6f6f6;">
                                <img src="/Orion/images/StatusIcons/Small-<%#Eval("BGroupStatus") %>" alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_108%>" />
                            </td>
                            <td width="20%" style="text-align: left;border-right-style:solid;border-right-width:1px;border-right-color:#966a00;background-color: #f6f6f6;">
                                <orion:ToolsetLink ID="ToolsetLink6" runat="server" IPAddress='<%# Eval("BIPAddress") %>' DNS='<%# Eval("BDNS") %>'
                                    SysName='<%# Eval("BSysName") %>' CommunityGUID='<%# Eval("BGuid") %>' NodeID='<%#Eval("BNodeID") %>'
                                    ToolTip=''>
		                    <%#HighlightSearchValues(Eval("BCaption")) %>
                                </orion:ToolsetLink>
                            </td>
                        </tr>          
                        <tr height="5px"></tr>
                    </AlternatingItemTemplate>                    
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatePanel">
                    <ProgressTemplate>
                        <img alt="" src="/Orion/images/AJAX-Loader.gif" /></ProgressTemplate>
                </asp:UpdateProgress>
                <table width="100%" runat="server" ID="gridFooter" cellpadding="0" cellspacing="0" class="TopologyFooter">
                    <tr runat="server" ID="gridFooterTraverser">
                        <td colspan="3">
                            <div class="topologyDiv" style="padding-right:5px;">
                                <asp:LinkButton CommandName="Page" OnCommand="TopologyData_IndexChanging" CausesValidation="false"
                                    CommandArgument="First" ID="lbFirst" runat="server">
                                    <img src="/Orion/images/Arrows/button_white_paging_first.gif" style="vertical-align:top;"/>
                                </asp:LinkButton>
                                <asp:LinkButton ID="lbFirst_Disabled" runat="server" OnCommand="TopologyData_IndexChanging" CommandArgument="first_disabled" Enabled="False">
                                    <img src="/Orion/images/Arrows/button_white_paging_first_disabled.gif" style="border:none"/>
                                </asp:LinkButton>                
                             </div>
                            <div class="topologyDiv" style="padding-right:5px;">
                                <asp:LinkButton CommandName="Page" OnCommand="TopologyData_IndexChanging" CausesValidation="false"
                                    CommandArgument="Prev" ID="lbPrev" runat="server">
                                    <img src="/Orion/images/Arrows/button_paging_previous.gif" style="vertical-align:top;"/>
                                </asp:LinkButton>
                                <asp:LinkButton ID="lbPrev_Disabled" runat="server" OnCommand="TopologyData_IndexChanging" CommandArgument="prev_disabled" Enabled="False">
                                    <img src="/Orion/images/Arrows/button_paging_previous_disabled.gif" style="border:none"/>
                                </asp:LinkButton>
                            </div>
                            <div class="topologyDiv" style="padding-right:5px;padding-left:5px;">
                                <asp:Label ID="lblPage1" runat="server" Text="0"></asp:Label>
                            </div>
                            <div class="topologyDiv" style="padding-right:5px;padding-left:5px;">
                                <asp:TextBox runat="server" ID="txtCurrentPage" style="width: 15px;" Font-Bold="True"></asp:TextBox>
                            </div>
                            <div class="topologyDiv" style="padding-right:5px;padding-left:5px;">
                                <asp:Label ID="lblPage2" runat="server" Text="0"></asp:Label>
                            </div>
                            <div class="topologyDiv" style="padding-right:5px;padding-left:5px;">
                                <asp:LinkButton CommandName="Page" OnCommand="TopologyData_IndexChanging" CausesValidation="false"
                                    CommandArgument="Next" ID="lbNext" runat="server">
                                <img src="/Orion/images/Arrows/button_paging_next.gif" style="vertical-align:top;" />
                                </asp:LinkButton>
                                <asp:LinkButton ID="lbNext_Disabled" runat="server" OnCommand="TopologyData_IndexChanging" CommandArgument="next_disabled" Enabled="False">
                                    <img src="/Orion/images/Arrows/button_paging_next_disabled.gif" style="border:none"/>
                                </asp:LinkButton>   
                            </div>
                            <div class="topologyDiv" style="padding-right:5px;padding-left:5px;">
                                <asp:LinkButton CommandName="Page" OnCommand="TopologyData_IndexChanging" CommandArgument="Last"
                                    ID="lbLast" runat="server">
                                    <img src="/Orion/images/Arrows/button_white_paging_last.gif" style="vertical-align:top"/>&nbsp;
                                </asp:LinkButton>
                                <asp:LinkButton ID="lbLast_Disabled" runat="server" OnCommand="TopologyData_IndexChanging" CommandArgument="last_disabled" Enabled="False">
                                    <img src="/Orion/images/Arrows/button_white_paging_last_disabled.gif" style="border:none"/>
                                </asp:LinkButton>
                            </div>
                            <div class="topologyDiv" style="padding-left:5px;">
                                <asp:LinkButton CommandName="Page" OnCommand="TopologyData_IndexChanging" CommandArgument="All"
                                    ID="LinkButton1" runat="server" ForeColor="#336699">
                                &nbsp;<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_249%>&nbsp;
                                </asp:LinkButton>
                            </div>
                        </td>
                        <td style="text-align: right;border: none">
                            <div class="topologyDiv" style="padding-left:5px;float:right">
                                <asp:Label ID="lblPageDisplay" runat="server" Text=""></asp:Label>
                            </div>                            
                        </td>
                    </tr>
                    <tr runat="server" ID="gridFooterEmpty">
                        <td style="text-align: left;border: none">
                            <div class="topologyDiv" style="padding-left:5px;">
                                <asp:Label ID="lblNoConnections" runat="server" Text=""></asp:Label>
                            </div>                            
                        </td>
                    </tr>
                </table>
                <%-- this button is here because of async views refreshing, we need to generate postback event to refresh update panel --%>
                <asp:LinkButton CommandName="Page" OnCommand="TopologyData_IndexChanging" CausesValidation="false" CommandArgument="Refresh" ID="LinkButtonRefreshResource" runat="server" />               
                <asp:HiddenField runat="server" ID="hfPageIndex" />
                <asp:HiddenField runat="server" ID="hfSearchStr" />
                <asp:HiddenField runat="server" ID="hfShowAll" />
                <asp:HiddenField runat="server" ID="hfLayerStr" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="tbSearch" />
                <asp:AsyncPostBackTrigger ControlID="btnSearch" />
                <asp:AsyncPostBackTrigger ControlID="lbPrev" />
                <asp:AsyncPostBackTrigger ControlID="lbNext" />
                <asp:AsyncPostBackTrigger ControlID="LinkButton1" />
            </Triggers>
        </asp:UpdatePanel>
        <script type="text/javascript">
            $(function () {
                $('#<%=tbSearch.ClientID%>').each(function () {
                    this.value = '<%= ControlHelper.EncodeJsString(Resources.InterfacesWebContent.NPMWEBDATA_VB0_250)%>';
                    this.style.color = '#555';
                    this.style.fontStyle = 'italic';
                    this.style.fontSize = '9pt';
                });

                $('#<%=tbSearch.ClientID%>').click(function () {
                    if (this.value == '<%= ControlHelper.EncodeJsString(Resources.InterfacesWebContent.NPMWEBDATA_VB0_250)%>') {
                        this.value = '';
                        this.style.color = '#000';
                    }
                    this.style.fontStyle = 'normal';
                    this.style.fontSize = '9pt';
                });

                $('#<%=tbSearch.ClientID%>').blur(function () {
                    if (this.value == '') {
                        this.value = '<%= ControlHelper.EncodeJsString(Resources.InterfacesWebContent.NPMWEBDATA_VB0_250) %>';
                        this.style.color = '#555';
                        this.style.fontStyle = 'italic';
                        this.style.fontSize = '9pt';
                    }
                });
            });
        </script>
        <script type="text/javascript" language="javascript">
            function EnterEvent(e) {
                if (e.keyCode == 13) {
                    document.getElementById('<%= hfPageIndex.ClientID %>').value = document.getElementById('<%= txtCurrentPage.ClientID %>').value;
                    var value = document.getElementById('<%= hfPageIndex.ClientID %>').value;
                    __doPostBack('keydown', '');
                }
            }
</script>
    </Content>
</orion:resourcewrapper>
