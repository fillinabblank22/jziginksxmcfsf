﻿using System;
using System.Collections.Generic;
using System.Globalization;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Interfaces_Resources_SummaryCharts_NetworkWideChart : StandardChartResource
{
    private static readonly Log _log = new Log();

    protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override string NetObjectPrefix
    {
        get { return "NPM_SUMMARY"; }
    }

    protected override string DefaultTitle
    {
		get { return notAvailable; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
		get { yield break; }
    }


	protected override IEnumerable<string> GetElementIdsForChart()
	{
		yield return string.Empty;
	}
}

