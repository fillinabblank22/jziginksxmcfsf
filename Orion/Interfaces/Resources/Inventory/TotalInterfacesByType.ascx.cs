﻿using System;
using System.Data;

using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Resources_Inventory_TotalInterfacesByType : SolarWinds.Orion.Web.UI.BaseResourceControl
{
    #region properties

    // overriden DefaultTitle
    protected override string DefaultTitle
    {
        get { return Resources.InterfacesWebContent.NPMWEBCODE_TM0_21; }
    }

    // overriden HelpLink
    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceNumberInterfacesByType";
        }
    }

	public override string EditControlLocation
	{
		get { return "/Orion/NetPerfMon/Controls/EditResourceControls/FilterEdit.ascx"; }
	}

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        string filter = Resource.Properties["Filter"];

		int nodeID = -1;
		// if is netObject a Node set filter for a node
		INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();
		if (nodeProvider != null)
			nodeID = nodeProvider.Node.NodeID;
		else
			nodeID = CommonWebHelper.TryToGetNodeIDFromRequest(Request.QueryString["NetObject"]);


		if (nodeID >0)
            if (String.IsNullOrEmpty(filter))
				filter = String.Format("NodesData.NodeID={0}", nodeID);
            else
				filter = String.Format("{0} AND NodesData.NodeID={1}", filter, nodeID);

        DataTable table = null;

        try
        {
            table = SqlDAL.GetTotalInterfacesByType(filter);
        }
        catch
		{
			this.SQLErrorPanel.Visible = true;
			return; 
		}

        if (table == null || table.Rows.Count == 0)
        {
            return;
        }

        // bind data to repeater
        this.RowRepeater.DataSource = table;
        this.RowRepeater.DataBind();
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
