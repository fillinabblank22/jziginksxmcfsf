﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using SolarWinds.Interfaces.Web;
using SolarWinds.Logging;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Interfaces_Resources_InterfaceCharts_InterfaceChart : StandardChartResource, IResourceIsInternal
{
    private static readonly Log _log = new Log();

    protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override string NetObjectPrefix
    {
        get { return "I"; }
    }

    protected override IEnumerable<ChartCustomPropertyKey> GetAnnotationsFromModule(NetObject netObject)
    {
        if (string.Equals(ChartResourceSettings.CustomPropertyKey, "bps", StringComparison.OrdinalIgnoreCase)) // charts that can have BPS can have also SLA
        {
            if (netObject.ObjectProperties.ContainsKey("SLA"))
            {
                double sla;
                if (double.TryParse(netObject.ObjectProperties["SLA"].ToString(), out sla))
                {
                    var rv = new ChartCustomPropertyKey
                                 {
                                     Annotation =
                                         string.Format(Resources.InterfacesWebContent.NPMWEBCODE_VT0_4, new BitsPerSecond(sla)),
                                     Marker = sla.ToString(CultureInfo.InvariantCulture)
                                 };

                    return new[] {rv};
                }
                else
                {
                    _log.InfoFormat("Interface {0} - double.Parse for SLA ({1}) failed.", netObject.NetObjectID, netObject.ObjectProperties["SLA"]);
                }
            }
        }

        return Enumerable.Empty<ChartCustomPropertyKey>();
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        var multipleInterfaceProvider = GetInterfaceInstance<IMultiInterfaceProvider>();
        if (multipleInterfaceProvider != null)
        {
            var netObjects = multipleInterfaceProvider.LimitedNetObjects;
            if (netObjects.Length != 0)
                return netObjects;
        }

        var interfaceProvider = GetInterfaceInstance<IInterfaceProvider>();
        if (interfaceProvider != null)
        {
            return new[] { interfaceProvider.Interface.InterfaceID.ToString(CultureInfo.InvariantCulture) };
        }

        var nodeProvider = GetInterfaceInstance<INodeProvider>();
        if (nodeProvider != null)
        {
            const string swql = "SELECT InterfaceID FROM Orion.NPM.Interfaces WHERE NodeID = @NodeId";
            return GetElementsFromSwql(swql, "NodeId", nodeProvider.Node.NodeID);
        }

        return new string[0];
    }

    protected override string DefaultTitle
    {
        get { return Resources.InterfacesWebContent.NPMWEBCODE_ZB0_1; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(INodeProvider) }; }
    }

    public override IEnumerable<Type> SupportedInterfaces
    {
        get { return new[] { typeof(IInterfaceProvider), typeof(IMultiInterfaceProvider) }; }
    }

    public bool IsInternal
    {
        get { return true; }
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }
}

