﻿using System;
using System.Collections.Generic;
using System.Globalization;
using SolarWinds.Interfaces.Web.ResourcesMetadata.Enums;
using SolarWinds.Logging;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, InterfacesMetadataSearchTagsChartsValues.Interface)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, InterfacesMetadataSearchTagsChartsValues.Utilization)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, InterfacesMetadataSearchTagsChartsValues.Receive)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, InterfacesMetadataSearchTagsChartsValues.Forecast)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, InterfacesMetadataSearchTagsChartsValues.Chart)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, InterfacesMetadataSearchTagsChartsValues.Capacity)]
public partial class Orion_Interfaces_Resources_InterfaceCharts_InterfaceUtilizationReceiveForecastChart : StandardChartResource, IResourceIsInternal
{
    private static readonly Log _log = new Log();

    protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override string NetObjectPrefix
    {
        get { return "I"; }
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        var interfaceProvider = GetInterfaceInstance<IInterfaceProvider>();
        if (interfaceProvider != null)
        {
            return new[] { interfaceProvider.Interface.InterfaceID.ToString(CultureInfo.InvariantCulture) };
        }

        return new string[0];
    }

    protected override string DefaultTitle
    {
        get { return Resources.InterfacesWebContent.NPMWEBCODE_LM0_2; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IInterfaceProvider) }; }
    }

    public override IEnumerable<Type> SupportedInterfaces
    {
        get { return new[] { typeof(IInterfaceProvider) }; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public bool IsInternal
    {
        get { return true; }
    }
}