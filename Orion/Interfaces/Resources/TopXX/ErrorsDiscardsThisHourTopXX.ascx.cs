using System;
using System.Data;
using SolarWinds.Interfaces.Web.DAL;
using SolarWinds.Interfaces.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Resources_TopXX_ErrorsDiscardsThisHour : TopXXResourceControl
{
    protected override string TitleTemplate
    {
        get { return Resources.InterfacesWebContent.NPMWEBCODE_VB0_10; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Wrapper.ShowEditButton = this.EnableEdit;
		DataTable table;

		try
		{
			table = TopXXDAL.GetErrorsDiscardsThisHour(this.MaxRecords, this.Resource.Properties["Filter"]);
            table.EncodeColumns();
		}
		catch (System.Data.SqlClient.SqlException)
		{
			this.SQLErrorPanel.Visible = true;
			return;
		}
        this.resourceTable.DataSource = table;
        this.resourceTable.DataBind();

    }

    public override string EditURL
    {
		get { return CreateEditUrl(EditNetObjectType.Interfaces); }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceTopXErrorsDiscardsThisHour"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
