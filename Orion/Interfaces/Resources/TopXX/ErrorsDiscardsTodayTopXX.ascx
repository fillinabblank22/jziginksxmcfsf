<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ErrorsDiscardsTodayTopXX.ascx.cs" Inherits="Orion_NetPerfMon_Resources_TopXX_ErrorsDiscardsToday" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:Include ID="Include1" runat="server" File="../../Orion/Interfaces/Styles/Interfaces.css" />
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_38%>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="resourceTable">
            <HeaderTemplate>
    <table border="0" cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes">
        <tr>
            <td class="ReportHeader" colspan="2"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_37%></td>
            <td class="ReportHeader" colspan="2"><%=Resources.InterfacesWebContent.NPMWEBCODE_VB0_6%></td>
            <td class="ReportHeader"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_31%></td>
            <td class="ReportHeader"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_32%></td>
            <td class="ReportHeader"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_33%></td>
            <td class="ReportHeader"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_34%></td>
        </tr>
            </HeaderTemplate>
            <ItemTemplate>
	    <tr>
	        <td class="Property" valign="middle" width="20"><img src="<%# SolarWinds.Orion.Web.UI.StatusIcons.NodeIconFactory.SmallIconURL( Eval("NodeID"), Eval("NodeStatus") ) %>" alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_35%>" style="vertical-align:bottom;" />&nbsp;</td>
	        <td class="Property"><a
                class="interfaces-allowNewlines"
                <%# this.FormatParamString(Container.DataItem, this.Profile.AllowNodeManagement) %> href="/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:<%# Eval("NodeID")%>"><%# Eval("NodeName")%></a>&nbsp;</td>
	        <td class="Property" valign="middle" width="20"><img src="/NetPerfMon/images/small-<%# Eval("StatusLED").ToString().Trim() %>" alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_36%>" style="vertical-align:bottom;" />&nbsp;</td>
	        <td class="Property"><a
                class="interfaces-allowNewlines"
                <%# this.FormatInterfaceParamString(Container.DataItem, this.Profile.AllowNodeManagement) %> href="/Orion/Interfaces/InterfaceDetails.aspx?NetObject=I:<%# Eval("InterfaceID")%>"><%# Eval("Caption") %></a>&nbsp;</td>
	        <td class="Property"><a href="/Orion/Charts/CustomChart.aspx?ChartName=InterfaceErrors&CalculateTrendLine=True&NetObject=I:<%# Eval("InterfaceID")%>&Period=Today&SampleSize=1H" target="_blank"><%# this.FormatErrors(Eval("InErrorsToday"))%></a>&nbsp;</td>
	        <td class="Property"><a href="/Orion/Charts/CustomChart.aspx?ChartName=InterfaceDiscards&CalculateTrendLine=True&NetObject=I:<%# Eval("InterfaceID")%>&Period=Today&SampleSize=1H" target="_blank"><%# this.FormatDiscards(Eval("InDiscardsToday"))%></a>&nbsp;</td>
	        <td class="Property"><a href="/Orion/Charts/CustomChart.aspx?ChartName=InterfaceErrors&CalculateTrendLine=True&NetObject=I:<%# Eval("InterfaceID")%>&Period=Today&SampleSize=1H" target="_blank"><%# this.FormatErrors(Eval("OutErrorsToday"))%></a>&nbsp;</td>
	        <td class="Property"><a href="/Orion/Charts/CustomChart.aspx?ChartName=InterfaceDiscards&CalculateTrendLine=True&NetObject=I:<%# Eval("InterfaceID")%>&Period=Today&SampleSize=1H" target="_blank"><%# this.FormatDiscards(Eval("OutDiscardsToday"))%></a>&nbsp;</td>
        </tr>
            </ItemTemplate>
            <FooterTemplate>
    </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>