<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RelativeMcastPacketRateTopXX.ascx.cs" Inherits="Orion_NetPerfMon_Resources_TopXX_RelativeMcastPacketRate" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_38%>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="resourceTable">
            <HeaderTemplate>
    <table border="0" cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes">
        <tr>
            <td class="ReportHeader" colspan="2"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_37%></td>
            <td class="ReportHeader" colspan="2"><%=Resources.InterfacesWebContent.NPMWEBCODE_VB0_6%></td>
            <td class="ReportHeader" style="width:45px;"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_41%></td>
            <td class="ReportHeader" style="width:45px;"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_42%></td>
            <td class="ReportHeader" style="width:45px;"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_43%></td>
            <td class="ReportHeader" style="width:48px;"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_44%></td>
        </tr>
            </HeaderTemplate>
            <ItemTemplate>
	    <tr>
	        <td class="Property" valign="middle" width="20"><img src="<%# SolarWinds.Orion.Web.UI.StatusIcons.NodeIconFactory.SmallIconURL( Eval("NodeID"), Eval("NodeStatus") ) %>" alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_35%>" style="vertical-align:bottom;" />&nbsp;</td>
	        <td class="Property"><a <%# this.FormatParamString(Container.DataItem, this.Profile.AllowNodeManagement) %> href="/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:<%# Eval("NodeID")%>"><%# WebSecurityHelper.HtmlEncode(Eval("NodeName").ToString()) %></a>&nbsp;</td>
	        <td class="Property" valign="middle" width="20"><img src="/NetPerfMon/images/small-<%# Eval("StatusLED").ToString().Trim() %>" alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_36%>" style="vertical-align:bottom;" />&nbsp;</td>
	        <td class="Property"><a <%# this.FormatInterfaceParamString(Container.DataItem, this.Profile.AllowNodeManagement) %> href="/Orion/Interfaces/InterfaceDetails.aspx?NetObject=I:<%# Eval("InterfaceID")%>"><%# WebSecurityHelper.HtmlEncode(Eval("Caption").ToString()) %></a>&nbsp;</td>
	        <td class="Property" style="width:45px;"><a href="/Orion/Charts/CustomChart.aspx?ChartName=MCastPps&CalculateTrendLine=True&NetObject=I:<%# Eval("InterfaceID")%>&Period=Today" target="_blank"><%# this.FormatNumber(Eval("InBroadcastPercent"), "%")%></a>&nbsp;</td>
	        <td class="Property" style="width:45px;"><a href="/Orion/Charts/CustomChart.aspx?ChartName=MMAvgPps&CalculateTrendLine=True&NetObject=I:<%# Eval("InterfaceID")%>&Period=Today" target="_blank"><%# this.FormatNumber(Eval("InPps"), Resources.InterfacesWebContent.NPMWEBCODE_VB0_20)%></a>&nbsp;</td>
	        <td class="Property" style="width:45px;"><a href="/Orion/Charts/CustomChart.aspx?ChartName=MCastPps&CalculateTrendLine=True&NetObject=I:<%# Eval("InterfaceID")%>&Period=Today" target="_blank"><%# this.FormatNumber(Eval("OutBroadcastPercent"), "%")%></a>&nbsp;</td>
	        <td class="Property" style="width:48px;"><a href="/Orion/Charts/CustomChart.aspx?ChartName=MMAvgPps&CalculateTrendLine=True&NetObject=I:<%# Eval("InterfaceID")%>&Period=Today" target="_blank"><%# this.FormatNumber(Eval("OutPps"), Resources.InterfacesWebContent.NPMWEBCODE_VB0_20)%></a>&nbsp;</td>
        </tr>
            </ItemTemplate>
            <FooterTemplate>
    </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>