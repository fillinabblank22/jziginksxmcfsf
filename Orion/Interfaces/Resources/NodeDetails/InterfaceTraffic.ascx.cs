﻿using System;
using System.Data;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Resources_NodeDetails_InterfaceTraffic : SolarWinds.Orion.Web.UI.BaseResourceControl
{
    private string currentNodeId = String.Empty;

    #region properties

    // overriden DefaultTitle
    protected override string DefaultTitle
    {
        get { return Resources.InterfacesWebContent.NPMWEBCODE_VB0_105; }
    }

    public string GetSortName(string sort, string sortFields)
    {
        if (String.IsNullOrEmpty(sort) || String.IsNullOrEmpty(sortFields))
            return String.Empty;
        
        string[] temp = sortFields.Split(',');
        foreach(string s in temp)
        {
            if (s.Contains(sort))
            {
                string[] result = s.Split(':');
                if (result != null && result.Length == 2)
                    return result[1];
                else return String.Empty;
            }
        }

        return String.Empty;
    }

    // overriden HelpLink
    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceTrafficPercentUtilizationInterface";
        }
    }

	public override string EditControlLocation
	{
		get
		{
			return "/Orion/NetPerfMon/Controls/EditResourceControls/SortEdit.ascx";
		}
	}

    public int WarningLevel { get; set; }
    public int ErrorLevel { get; set; }

    #endregion

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ErrorLevel = Convert.ToInt32(Thresholds.IfPercentUtilizationError.SettingValue);
        WarningLevel = Convert.ToInt32(Thresholds.IfPercentUtilizationWarning.SettingValue);

        // if is netObject a Node set filter for a node
        INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();

        if (nodeProvider == null) return;
        
        DataTable table = null;

        var orderBy = this.Resource.Properties["Sort"];

		// InterfaceIndex is deprecated and must be cleaned from ResourceProperties
        if (String.IsNullOrEmpty(this.Resource.Properties["SortFields"]) ||
			this.Resource.Properties["SortFields"].Contains("InterfaceIndex") ||
			this.Resource.Properties["Sort"].Contains("InterfaceIndex"))
        {
            this.Resource.Properties["SortFields"] = String.Format("Interfaces.Index:{0},Interfaces.Status DESC:{1},Interfaces.Caption:{2},Interfaces.OutPercentUtil DESC:{3},Interfaces.InPercentUtil DESC:{4},(Interfaces.OutPercentUtil+Interfaces.InPercentUtil) DESC:{5}",
                Resources.InterfacesWebContent.NPMWEBCODE_VB0_112, Resources.InterfacesWebContent.NPMWEBDATA_VB0_36, Resources.InterfacesWebContent.NPMWEBCODE_VB0_88, Resources.InterfacesWebContent.NPMWEBCODE_VB0_114, Resources.InterfacesWebContent.NPMWEBCODE_VB0_113, Resources.InterfacesWebContent.NPMWEBCODE_VB0_115);
            orderBy = "Interfaces.Index";
            this.Resource.Properties["Sort"] = orderBy;
        }
        
		try
		{
            using (WebDAL dal = new WebDAL())
            {
                table = dal.GetInterfaces(nodeProvider.Node.NodeID, orderBy);
            }
		}
		catch { this.SQLErrorPanel.Visible = true; return; }

        if (table == null || table.Rows.Count == 0)
        {
            return;
        }

        table.Columns.Add("InterfaceURL", typeof(string));
        table.Columns.Add("StatusText", typeof(string));
        table.Columns.Add("InterfaceUtilizationURL", typeof(string));
        table.Columns.Add("BoolStatus", typeof(bool));
        table.Columns.Add("FormatedInbps", typeof(string));
        table.Columns.Add("FormatedOutbps", typeof(string));
        
        // preprocessing
        foreach (DataRow r in table.Rows)
        {
            // interface details
            r["InterfaceURL"] = String.Format("/Orion/Interfaces/InterfaceDetails.aspx?NetObject=I:{0}", r["InterfaceID"]);
            r["Icon"] = SolarWinds.Orion.Web.Helpers.ImageHelper.GetInterfaceTypeIcon(r["Icon"].ToString().Trim());

			try
			{
				r["StatusText"] = FormatHelper.GetStatusText(Convert.ToInt32(r["Status"]));
			}
			catch
			{
				r["StatusText"] = Resources.InterfacesWebContent.NPMWEBCODE_AK0_2;
			}

			// interface traffic chart
            r["InterfaceUtilizationURL"] = String.Format("/Orion/Charts/CustomChart.aspx?ChartName=AvgUtil-Step&NetObject=I:{0}&Period=Today", r["InterfaceID"]);

			bool show = (Convert.ToInt32(r["Status"]) == 1) ? true : false;

            // interface traffic chart
            r["BoolStatus"] = show;

            r["FormatedInbps"] = show ? Utils.ConvertToMB(Utils.SafeToDouble(r["Inbps"]), "bps", true).Replace(" ", "&nbsp;") : "&nbsp;";
            r["FormatedOutbps"] = show ? Utils.ConvertToMB(Utils.SafeToDouble(r["Outbps"]), "bps", true).Replace(" ", "&nbsp;") : "&nbsp;";
        }

        // bind data to repeater
        this.NodesRepeater.DataSource = table;
        this.NodesRepeater.DataBind();
    }

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}