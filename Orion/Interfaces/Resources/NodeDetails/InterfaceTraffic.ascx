<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterfaceTraffic.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeDetails_InterfaceTraffic" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <orion:Include ID="Include1" runat="server" File="../../Orion/Interfaces/Styles/InterfaceTraffic.css" />
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_223%>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="NodesRepeater">
            <HeaderTemplate>
                <table border="0" cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes">
                    <tr id="Tr1" runat="server" >
                        <td class="ReportHeader" colspan="2">
                        <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_231%>
                        </td >
                        <td class="ReportHeader spaceColumn">
							&nbsp;
                        </td>
                        <td class="ReportHeader" colspan="2">
                            <%=Resources.InterfacesWebContent.NPMWEBCODE_VB0_6%>
                        </td>
                        <td class="ReportHeader spaceColumn">
							&nbsp;
                        </td>                        
                        <td class="ReportHeader" colspan="2" >
                            <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_39%>
                        </td>
                        <td class="ReportHeader spaceColumn">
							&nbsp;
                        </td>                 
                        
                        <td class="ReportHeader" colspan="2" >
                            <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_40%>
                        </td>
						<td class="ReportHeader spaceColumn">
							&nbsp;
                        </td>
                        <td class="ReportHeader">
                            &nbsp;
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td class="Property" valign="top">
                        <asp:Panel ID="Panel1" runat="server" >
                            <img alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_108%>" src="/Orion/images/StatusIcons/Small-<%# Eval("StatusLED") %>" />
                        </asp:Panel>
                    </td>
                    <td valign="top"><%#Eval("StatusText")%></td>
                    <td class="spaceColumn">&nbsp</td>
					<td valign="top">
						<img alt="Status" src="/NetPerfMon/images/Interfaces/<%# Eval("Icon") %>" />
                    </td>
                    <td valign="top">
                        <orion:ToolsetLink  runat="server" ID="ToolsetLink1" 
                                            IPAddress='<%#Eval("IPAddress")%>'
                                            DNS='<%#Eval("DNS")%>'
                                            SysName='<%#Eval("SysName")%>' 
                                            CommunityGUID='<%#Eval("GUID")%>'
                                            NavigateUrl='<%#Eval("InterfaceURL")%>'
                                            InterfaceIndex='<%#Eval("Index")%>'
                                            InterfaceName='<%#Eval("Name")%>'>
                        <%# Eval("Caption") %>
                        </orion:ToolsetLink>
                    </td>
					<td class="spaceColumn">&nbsp</td>
                    <td class="Property" valign="top" align="left">
                        <asp:HyperLink ID="PLink1" runat="server" NavigateUrl='<%# Eval("InterfaceUtilizationURL") %>'
                            Target="_blank">
                            <%#Eval("FormatedInbps") %>
                        </asp:HyperLink>
                    </td>

                    <td valign="top" align="right" class="Property ProgressBarProperty">
                        <asp:HyperLink ID="PLink3" runat="server" NavigateUrl='<%# Eval("InterfaceUtilizationURL") %>'
                            Target="_blank">
                            <%# CommonWebHelper.FormatPercentStatusBar(Eval("InPercentUtil"), ErrorLevel, WarningLevel, 50, (bool)Eval("BoolStatus"))%>
                        </asp:HyperLink>
						
						<asp:HyperLink ID="PLink2" runat="server" NavigateUrl='<%# Eval("InterfaceUtilizationURL") %>'
                            Target="_blank">
                            <%# CommonWebHelper.FormatPercentValue(Eval("InPercentUtil"), ErrorLevel, WarningLevel, (bool)Eval("BoolStatus"))%>
                        </asp:HyperLink>
                    </td>
                    <td class="spaceColumn">&nbsp</td>
                    <td class="Property" valign="top" align="left">
                        <asp:HyperLink ID="PLink4" runat="server" NavigateUrl='<%# Eval("InterfaceUtilizationURL") %>'
                            Target="_blank">
                            <%#Eval("FormatedOutbps") %>
                        </asp:HyperLink>
                    </td>
                    <td valign="top" align="right" class="Property ProgressBarProperty">
                        <asp:HyperLink ID="PLink6" runat="server" NavigateUrl='<%# Eval("InterfaceUtilizationURL") %>'
                            Target="_blank">
                            <%# CommonWebHelper.FormatPercentStatusBar(Eval("OutPercentUtil"), ErrorLevel, WarningLevel, 50, (bool)Eval("BoolStatus"))%>
                        </asp:HyperLink>
						
						<asp:HyperLink ID="PLink5" runat="server"  NavigateUrl='<%# Eval("InterfaceUtilizationURL") %>'
                            Target="_blank">
                            <%# CommonWebHelper.FormatPercentValue(Eval("OutPercentUtil"), ErrorLevel, WarningLevel, (bool)Eval("BoolStatus"))%>
                        </asp:HyperLink>
                    </td>
					<td class="spaceColumn">&nbsp</td>
                    <td valign="top">
                    <orion:ToolsetImage  runat="server" ID="ToolsetLink2" 
                                            IPAddress='<%#Eval("IPAddress")%>'
                                            DNS='<%#Eval("DNS")%>'
                                            SysName='<%#Eval("SysName")%>' 
                                            CommunityGUID='<%#Eval("GUID")%>'
                                            NavigateUrl='<%#Eval("InterfaceURL")%>'
                                            InterfaceIndex='<%#Eval("Index")%>'
                                            InterfaceName='<%#Eval("Name")%>'
                                            ImageUrl="/NetPerfMon/Images/Small.Gauge.png"
                                            ImageAlt="<%$ Resources: InterfacesWebContent, NPMWEBDATA_VB0_234%>"
                                            OnClientClick="javascript:DoAction('SWTOOL:BandwidthGauge', '');"
                                            OnClientMouseOver="javascript:status='Launch SolarWinds Real-Time Bandwidth Gauges'; return true;"
                                            OnClientMouseOut="javascript:status=''; return false;" />
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:ResourceWrapper>
