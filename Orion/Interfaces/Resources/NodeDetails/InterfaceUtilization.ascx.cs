using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using SolarWinds.Interfaces.Web;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;


public partial class Orion_NetPerfMon_Resources_NodeDetails_InterfaceUtilization : ResourceControl
{
	private const string StatusLedImage = "<img src=\"/NetPerfMon/images/small-{0}\" border=\"0\" />";
	private const string IconImage = "<img src=\"/NetPerfMon/images/Interfaces/{0}\" border=\"0\" />";
	private const string UtilChartLink = "<a href=\"/Orion/Charts/CustomChart.aspx?ChartName=AvgUtil-Step&NetObject=I:{0}\" target=\"_blank\">{1}</a>";
	private const string InterfaceDetailLink = "<a href=\"/Orion/Interfaces/InterfaceDetails.aspx?NetObject=I:{0}&view=InterfaceDetails\" {2}>{1}</a>";

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		if (NodeHelper.IsResourceUnderExternalNode(this))
		{
			this.Visible = false;
		}
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		SolarWinds.Orion.NPM.Web.Node node = GetInterfaceInstance<INodeProvider>().Node;
	    HasVlans = false;
		if (null != node)
		{
			int netObjectId = node.NodeID;

			if (String.IsNullOrEmpty(this.Resource.Properties["SortFields"]) ||
				this.Resource.Properties["SortFields"].Contains("Interfaces.InterfaceIndex"))
			{
				this.Resource.Properties["SortFields"] = String.Format("Interfaces.Index:{0},Interfaces.Status DESC:{1},Interfaces.Caption:{2},Interfaces.OutPercentUtil DESC:{3},Interfaces.InPercentUtil DESC:{4},(Interfaces.OutPercentUtil+Interfaces.InPercentUtil) DESC:{5}",
				Resources.InterfacesWebContent.NPMWEBCODE_VB0_112, Resources.InterfacesWebContent.NPMWEBDATA_VB0_36, Resources.InterfacesWebContent.NPMWEBCODE_VB0_88, Resources.InterfacesWebContent.NPMWEBCODE_VB0_114, Resources.InterfacesWebContent.NPMWEBCODE_VB0_113, Resources.InterfacesWebContent.NPMWEBCODE_VB0_115);
			}
			if (String.IsNullOrEmpty(this.Resource.Properties["Sort"]) ||
				this.Resource.Properties["Sort"].Contains("Interfaces.InterfaceIndex"))
			{
				this.Resource.Properties["Sort"] = "Interfaces.Index";
			}

            HasVlans = VlanInterfaceHelper.HasNodeEnabledVlanPollers(node.NodeID);
			DataTable interfaces = this.GetInterfaceUtilizationTable(netObjectId, this.Resource.Properties["Sort"]);
			this.interfaceUtilizationTable.DataSource = interfaces;
			this.interfaceUtilizationTable.DataBind();
		}
	}

	private DataTable GetInterfaceUtilizationTable(int nodeId, string sort)
	{
		using (WebDAL proxy = new WebDAL())
		{
			DataTable table = proxy.GetInterfaceUtilization(nodeId, sort);

			DataTable formatedTable = new DataTable();
			formatedTable.Columns.Add("StatusLed", typeof(string));
			formatedTable.Columns.Add("Status", typeof(string));
			formatedTable.Columns.Add("Icon", typeof(string));
			formatedTable.Columns.Add("InterfaceLink", typeof(string));
            formatedTable.Columns.Add("PortType", typeof(string));
            formatedTable.Columns.Add("VLANs", typeof(string));
			formatedTable.Columns.Add("OutUtilChartLink", typeof(string));
			formatedTable.Columns.Add("OutUtilBar", typeof(string));
			formatedTable.Columns.Add("InUtilChartLink", typeof(string));
			formatedTable.Columns.Add("InUtilBar", typeof(string));
			formatedTable.Columns.Add("BandwidthGauge", typeof(string));

		    Dictionary<int, string> portTypes = null;
		    Dictionary<int, List<string>> portVlans = null;
            if (HasVlans)
            {
                portTypes = VlanInterfaceHelper.GetVlanPortTypesForInterfaces(nodeId);
                portVlans = VlanInterfaceHelper.GetVlansForInterfaces(nodeId);
            }

			double percentUtil;
			double? errorLimit = proxy.LookupSetting("NetPerfMon-PercentUtilization-Error");
			double? warningLimit = proxy.LookupSetting("NetPerfMon-PercentUtilization-Warning");

			foreach (DataRow row in table.Rows)
			{
				int interfaceID = Convert.ToInt32(row["InterfaceID"]);
				int interfaceIndex = Convert.ToInt32(row["Index"]);

				string paramString = FormatHelper.GetInterfaceParamString(Convert.ToString(row["IPAddress"]),
					Convert.ToString(row["DNS"]), Convert.ToString(row["SysName"]),
					Convert.ToString(row["GUID"]), Convert.ToString(row["Name"]), Convert.ToString(row["Index"]));

				DataRow formattedRow = formatedTable.NewRow();

				formattedRow["StatusLed"] = String.Format(StatusLedImage, Convert.ToString(row["StatusLed"]).Trim());
				formattedRow["Status"] = FormatHelper.GetStatusText(Convert.ToInt32(row["Status"]));
				formattedRow["Icon"] = String.Format(IconImage, SolarWinds.Orion.Web.Helpers.ImageHelper.GetInterfaceTypeIcon(Convert.ToString(row["Icon"]).Trim()));
				formattedRow["InterfaceLink"] = String.Format(InterfaceDetailLink, interfaceID, HttpUtility.HtmlEncode(row["Caption"]), paramString);

				percentUtil = Utils.SafeToDouble(row["OutPercentUtil"]);
				formattedRow["OutUtilChartLink"] = String.Format(UtilChartLink, interfaceID,
					FormatHelper.GetPercentUtilization(percentUtil, errorLimit, warningLimit));
				formattedRow["OutUtilBar"] = FormatHelper.GetPercentUtilizationBar(percentUtil, errorLimit, warningLimit);

				percentUtil = Utils.SafeToDouble(row["InPercentUtil"]);
				formattedRow["InUtilChartLink"] = String.Format(UtilChartLink, interfaceID,
					FormatHelper.GetPercentUtilization(percentUtil, errorLimit, warningLimit));
				formattedRow["InUtilBar"] = FormatHelper.GetPercentUtilizationBar(percentUtil, errorLimit, warningLimit);

				formattedRow["BandwidthGauge"] = FormatHelper.GetSWToolsetIcon(SWToolsetApp.BandwidthGauge, paramString);

                if (portTypes != null && portTypes.ContainsKey(interfaceIndex))
                {
                    formattedRow["PortType"] = portTypes[interfaceIndex];
                }
                else
                {
                    formattedRow["PortType"] = string.Empty;
                }

                if (portVlans != null && portVlans.ContainsKey(interfaceIndex))
                {
                    formattedRow["VLANs"] = String.Join("<br>", portVlans[interfaceIndex].Take(5));
                    if (portVlans[interfaceIndex].Count > 5)
                    {
                        paramString += " style=\"color: blue; text-decoration: underline;\"";
                        formattedRow["VLANs"] += "<br>" + String.Format(InterfaceDetailLink, interfaceID, Resources.InterfacesWebContent.NPMWEBDATA_PS1_1, paramString);
                    }
                }
                else
                {
                    formattedRow["VLANs"] = string.Empty; 
                }
				formatedTable.Rows.Add(formattedRow);
			}
			return formatedTable;
		}
	}

    protected bool HasVlans { get; private set; }

	protected override string DefaultTitle
	{
		get { return Resources.InterfacesWebContent.NPMWEBCODE_VB0_107; }
	}

	public override string EditControlLocation
	{
		get
		{
			return "/Orion/NetPerfMon/Controls/EditResourceControls/SortEdit.ascx";
		}
	}

	public override string HelpLinkFragment
	{
		get
		{
			return "OrionPHResourceCurrentPercentUtilizationInterface";
		}
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
