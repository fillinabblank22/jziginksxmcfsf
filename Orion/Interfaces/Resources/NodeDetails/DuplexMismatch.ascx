﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DuplexMismatch.ascx.cs" Inherits="Orion_Interfaces_Resources_NodeDetails_DuplexMismatch" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<orion:Include ID="Include" runat="server" File="DuplexMismatch.css" Module="Interfaces" />
<orion:ResourceWrapper ID="ResourceWrapper" runat="server">
    <content>
    <table border="0" cellpadding="3" cellspacing="0" width="100%" class="NeedsZebraStripes duplex-table">
        <thead>
            <tr>
	            <td class="ReportHeader duplex-interface-type">&nbsp;</td>
	            <td class="ReportHeader"><%=Resources.InterfacesWebContent.NPMWEBCODE_VB0_6%></td>
	            <td class="ReportHeader duplex-util-bar"><%=Resources.InterfacesWebContent.NPMWEBDATA_OC0_02%></td>
	            <td class="ReportHeader duplex-util-bar"><%=Resources.InterfacesWebContent.NPMWEBDATA_OC0_01%></td>
                <td class="ReportHeader duplex-neighbor-node duplex-break-word"><%=Resources.InterfacesWebContent.NPMWEBDATA_OD0_2%></td>
               <% if (IsNeighborInterface)
                  { %>
                <td class="ReportHeader duplex-break-word"><%=Resources.InterfacesWebContent.NPMWEBDATA_OD0_21%></td>
               <% } %>
	            <td class="ReportHeader duplex-mismatch"><%=Resources.InterfacesWebContent.NPMWEBDATA_OD0_3%></td>
            </tr>
        </thead> 
         <asp:Repeater runat="server" ID="DuplexMismatchTable">
        <ItemTemplate>
		        <tr style="vertical-align:top">
			        <td class="Property duplex-interface-type"><%# Eval("Icon") %></td>
			        <td class="Property duplex-truncate-text"><%# Eval("InterfaceLink")%>&nbsp;</td>
			        <td class="Property" align="right"><%# Eval("OutUtilBar")%>&nbsp;<%# Eval("OutUtilChartLink")%>&nbsp;</td>
			        <td class="Property" align="right"><%# Eval("InUtilBar")%>&nbsp;<%# Eval("InUtilChartLink")%>&nbsp;</td>
                    <td class="Property duplex-truncate-text"><%# Eval("DestIPAddress") %></td>
                   <% if (IsNeighborInterface)
                      { %>
		            <td class="Property duplex-truncate-text"><%# Eval("DestInterfaceLink") %></td>
                   <% } %>
                    <td class="Property"><%# Eval("DuplexMode") %></td>
		        </tr>
        </ItemTemplate>
    </asp:Repeater>
   </table> 
</content>
</orion:ResourceWrapper>
