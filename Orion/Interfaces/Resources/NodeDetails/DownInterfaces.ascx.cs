﻿using System;
using System.Data;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NetPerfMon_Resources_NodeDetails_DownInterfaces : SolarWinds.Orion.Web.UI.BaseResourceControl
{
    private string currentNodeId = String.Empty;

    #region properties

    // overriden DefaultTitle
    protected override string DefaultTitle
    {
        get { return Resources.InterfacesWebContent.NPMWEBCODE_VB0_103; }
    }

    // overriden HelpLink
    public override string HelpLinkFragment
    {
        get
        {
            return "OrionPHResourceDownInterfaces";
        }
    }

	public override string EditControlLocation
	{
		get
		{
			return "/Orion/NetPerfMon/Controls/EditResourceControls/SortEdit.ascx";
		}
	}
    #endregion

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // if is netObject a Node set filter for a node
        INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();

        if (nodeProvider == null) return;
        
        DataTable table = null;

        var orderBy = this.Resource.Properties["Sort"];

        if (String.IsNullOrEmpty(this.Resource.Properties["SortFields"]))
        {
            this.Resource.Properties["SortFields"] = String.Format("Interfaces.Index:{0},Interfaces.Status DESC:{1},Interfaces.Caption:{2},Interfaces.OutPercentUtil DESC:{3},Interfaces.InPercentUtil DESC:{4},(Interfaces.OutPercentUtil+Interfaces.InPercentUtil) DESC:{5}",
                Resources.InterfacesWebContent.NPMWEBCODE_VB0_112, Resources.InterfacesWebContent.NPMWEBDATA_VB0_36, Resources.InterfacesWebContent.NPMWEBCODE_VB0_88, Resources.InterfacesWebContent.NPMWEBCODE_VB0_114, Resources.InterfacesWebContent.NPMWEBCODE_VB0_113, Resources.InterfacesWebContent.NPMWEBCODE_VB0_115);
            orderBy = "Interfaces.Index";
            this.Resource.Properties["Sort"] = orderBy;
        }
        
        try
        {
            using (WebDAL dal = new WebDAL())
            {
                table = dal.GetDownInterfaces(nodeProvider.Node.NodeID, orderBy);
            }
        }
        catch { this.SQLErrorPanel.Visible = true; return; }

        if (table == null || table.Rows.Count == 0)
        {
            return;
        }

        table.Columns.Add("InterfaceURL", typeof(string));
        table.Columns.Add("StatusText", typeof(string));
        
        // preprocessing
        foreach (DataRow r in table.Rows)
        {
            // interface details
            r["InterfaceURL"] = String.Format("/Orion/Interfaces/InterfaceDetails.aspx?NetObject=I:{0}", r["InterfaceID"]);

            r["StatusText"] = FormatHelper.GetStatusText(Convert.ToInt32(r["Status"]));
        }

        // bind data to repeater
        this.NodesRepeater.DataSource = table;
        this.NodesRepeater.DataBind();
    }

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}

