<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterfaceUtilization.ascx.cs" Inherits="Orion_NetPerfMon_Resources_NodeDetails_InterfaceUtilization" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
<Content>
    <table border="0" cellpadding="3" cellspacing="0" width="100%" class="NeedsZebraStripes">
        <thead style="text-align:left;">
            <tr>
	            <td class="ReportHeader">&nbsp;</td>
	            <td class="ReportHeader"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_231%></td>
	            <td class="ReportHeader">&nbsp;</td>
	            <td class="ReportHeader"><%=Resources.InterfacesWebContent.NPMWEBCODE_VB0_6%></td>
                <% if (this.HasVlans) { %>
                <td class="ReportHeader"><%=Resources.InterfacesWebContent.NPMWEBDATA_GK0_9%></td>
                <td class="ReportHeader"><%=Resources.InterfacesWebContent.NPMWEBDATA_GK0_10%></td>
                <% } %>
	            <td class="ReportHeader" width="100px"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_40%></td>
	            <td class="ReportHeader" width="100px"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_39%></td>
	            <% if (this.Profile.ToolsetIntegration && !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
                    {%>
	            <td class="ReportHeader">&nbsp;</td>
	                <%} %>
            </tr>
        </thead>
    <asp:Repeater runat="server" ID="interfaceUtilizationTable">
        <ItemTemplate>
		        <tr style="vertical-align:top">
			        <td class="Property"><%# Eval("StatusLed") %></td>
			        <td class="Property"><%# Eval("Status") %></td>
			        <td class="Property"><%# Eval("Icon") %></td>
			        <td class="Property"><%# Eval("InterfaceLink")%>&nbsp;</td>
                    <% if (this.HasVlans) { %>
                    <td class="Property"><%# Eval("PortType")%></td>
                    <td class="Property"><%# Eval("VLANs")%></td>
                    <% } %>
			        <td class="Property" align="right"><%# Eval("OutUtilBar")%>&nbsp;<%# Eval("OutUtilChartLink")%>&nbsp;</td>
			        <td class="Property" align="right"><%# Eval("InUtilBar")%>&nbsp;<%# Eval("InUtilChartLink")%>&nbsp;</td>
		            <% if (this.Profile.ToolsetIntegration)
                        {%>
			        <td class="Property"><%# Eval("BandwidthGauge") %>&nbsp;</td>
			            <%} %>
		        </tr>
        </ItemTemplate>
    </asp:Repeater>
    </table>
</Content>
</orion:resourceWrapper>