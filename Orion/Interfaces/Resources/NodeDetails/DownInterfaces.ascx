<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DownInterfaces.ascx.cs"
    Inherits="Orion_NetPerfMon_Resources_NodeDetails_DownInterfaces" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
<orion:resourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_223%>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="NodesRepeater">
            <HeaderTemplate>
                <table border="0" cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes">
                    <tr id="Tr1" runat="server" >
                        <td class="ReportHeader">
                            &nbsp;
                        </td>
                        <td class="ReportHeader">
                            <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_231%>
                        </td>
                        <td class="ReportHeader">
                            &nbsp;
                        </td>
                        <td class="ReportHeader">
                            <%=Resources.InterfacesWebContent.NPMWEBCODE_VB0_6%>
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <img alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_108%>" src="/Orion/images/StatusIcons/Small-<%# Eval("StatusLED") %>" />
                    </td>
                    <td>
                        <%#Eval("StatusText")%>
                    </td>
                    <td>
                        <img alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_108%>" src="/NetPerfMon/images/Interfaces/<%# Eval("Icon") %>" />
                    </td>
                    <td>
                        <orion:ToolsetLink runat="server" ID="ToolsetLink1" IPAddress='<%#Eval("IPAddress")%>'
                            DNS='<%#Eval("DNS")%>' SysName='<%#Eval("SysName")%>' CommunityGUID='<%#Eval("GUID")%>'
                            NavigateUrl='<%#Eval("InterfaceURL")%>' InterfaceIndex='<%#Eval("Index")%>'
                            InterfaceName='<%#Eval("Name")%>'>
                        <%# Eval("Caption") %>
                        </orion:ToolsetLink>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>
