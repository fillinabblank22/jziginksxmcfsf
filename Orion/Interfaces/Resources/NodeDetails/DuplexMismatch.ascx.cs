﻿using System.Linq;
using SolarWinds.Interfaces.Import.Common.Helpers;
using SolarWinds.Interfaces.Web.DAL;
using SolarWinds.Interfaces.Web.NodeDetails;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;
using System;
using System.Data;
using System.Web;
using Node = SolarWinds.Orion.NPM.Web.Node;

public partial class Orion_Interfaces_Resources_NodeDetails_DuplexMismatch : ResourceControl, IIsInterfaceNeighborObserver
{
    private const string DestinationPrefix = "Dest";
    private const string SourcePrefix = "Source";

    public bool IsNeighborInterface { get; set; }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            Visible = false;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Resource.Properties["SortDuplexFields"]))
        {
            Resource.Properties["SortDuplexFields"] =
                String.Format(
                    "ReceivePercentErrors DESC:{0}, TransmitPercentErrors DESC:{1}, LateCollisions DESC:{2}, CRCAlignErrors DESC:{3}",
                    Resources.InterfacesWebContent.NPMWEBCODE_OD0_5,
                    Resources.InterfacesWebContent.NPMWEBCODE_OD0_6,
                    Resources.InterfacesWebContent.NPMWEBCODE_OD0_7,
                    Resources.InterfacesWebContent.NPMWEBCODE_OD0_8);
        }
        if (String.IsNullOrEmpty(Resource.Properties["SortDuplex"]) ||
            Resource.Properties["SortDuplex"].Contains("TransmitPercentErrors"))
        {
            Resource.Properties["SortDuplex"] = "TransmitPercentErrors DESC";
        }

        var nodeInstance = GetInterfaceInstance<INodeProvider>();
        Node currentNode = nodeInstance == null ? null : nodeInstance.Node;

        var interfaceInstance = GetInterfaceInstance<IInterfaceProvider>();
        int? interfaceId = interfaceInstance == null ? null : (int?)interfaceInstance.Interface.InterfaceID;

        var interfaces = new DataTable();

        if (currentNode != null)
        {
            int nodeId = currentNode.NodeID;
            interfaces = GetDuplexMismatchTable(nodeId, interfaceId);
        }

        if (interfaces.Rows.Count > 0)
        {
            DuplexMismatchTable.DataSource = interfaces;
            DuplexMismatchTable.DataBind();
        }
        else
            Visible = false;
    }

    private DataTable GetDuplexMismatchTable(int nodeId, int? interfaceId)
    {
        using (var swisProxy = InformationServiceProxy.CreateV3())
        {
            var provider = new DuplexMismatchProvider(new DuplexMismatchDAL(swisProxy), new HistoricalDataDAL(swisProxy), SourcePrefix, DestinationPrefix);
            var formatter = new DuplexMismatchDecorator(provider, new SettingsDAL(swisProxy), this, Resources.InterfacesWebContent.NPMWEBDATA_OC0_03, SourcePrefix, DestinationPrefix);

            var formattedTable = formatter.GetMismatchesTable(nodeId, interfaceId);

            return formattedTable;
        }
    }
    
    protected override string DefaultTitle
    {
        get { return Resources.InterfacesWebContent.NPMWEBCODE_OD0_4; }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/Interfaces/Controls/EditResourceControls/DuplexMismatchEdit.ascx";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }
}