﻿using System;
using System.Collections.Generic;
using System.Data;
//using SolarWinds.NPM.Web.EnergyWise;
//using SolarWinds.NPM.Web.EnergyWise.Models;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;


public partial class Orion_NetPerfMon_Resources_NodeDetails_InterfaceList : SolarWinds.Orion.Web.UI.BaseResourceControl
{
	// ========== PUBLIC ========== //


	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(INodeProvider) }; }
	}


	// overriden DefaultTitle
	protected override string DefaultTitle
	{
		get { return Resources.InterfacesWebContent.NPMWEBCODE_VB0_104; }
	}

	// overriden HelpLink
	public override string HelpLinkFragment
	{
		get
		{
			return "OrionPHResourceAllInterfacesSelectedNode";
		}
	}

	public override string EditControlLocation
	{
		get
		{
			return "/Orion/NetPerfMon/Controls/EditResourceControls/SortEdit.ascx";
		}
	}

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		if (NodeHelper.IsResourceUnderExternalNode(this))
		{
			this.Visible = false;
		}
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		//this.ewLevelHead.InnerText = " ";

		// if is netObject a Node set filter for a node
		INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();

		if (nodeProvider == null) return;

		DataTable table = null;

		var orderBy = this.Resource.Properties["Sort"];

		if (String.IsNullOrEmpty(this.Resource.Properties["SortFields"]))
		{
			this.Resource.Properties["SortFields"] = String.Format("Interfaces.Index:{0},Interfaces.Status DESC:{1},Interfaces.Caption:{2},Interfaces.OutPercentUtil DESC:{3},Interfaces.InPercentUtil DESC:{4},(Interfaces.OutPercentUtil+Interfaces.InPercentUtil) DESC:{5}",
				Resources.InterfacesWebContent.NPMWEBCODE_VB0_112, Resources.InterfacesWebContent.NPMWEBDATA_VB0_36, Resources.InterfacesWebContent.NPMWEBCODE_VB0_88, Resources.InterfacesWebContent.NPMWEBCODE_VB0_114, Resources.InterfacesWebContent.NPMWEBCODE_VB0_113, Resources.InterfacesWebContent.NPMWEBCODE_VB0_115);
			orderBy = (string.IsNullOrEmpty(orderBy)) ? "Interfaces.Index" : orderBy;
			this.Resource.Properties["Sort"] = orderBy;
		}

		try
		{
			using (WebDAL dal = new WebDAL())
			{
				table = dal.GetInterfaces(nodeProvider.Node.NodeID, orderBy);
			}
		}
		catch (Exception)
		{
			this.SQLErrorPanel.Visible = true;
			return;
		}

		if (table == null || table.Rows.Count == 0)
		{
			return;
		}

		table.Columns.Add("InterfaceURL", typeof(string));
		table.Columns.Add("StatusText", typeof(string));
        /*
		table.Columns.Add("EWPort", typeof(string));
		var ewInterfaces = new Dictionary<int, EWInterface>();
		using (var swql = InformationServiceProxy.CreateV3())
		{
			foreach (DataRow dr in swql.Query(@"

SELECT I.InterfaceID, E.EnergyWiseCurrentLevel 
FROM Orion.NPM.EW.Entity E 
INNER JOIN Orion.NPM.Interfaces I ON (I.InterfaceIndex=E.InterfaceIndex AND I.NodeID=E.NodeID)
WHERE E.NodeID=@id",
					   new Dictionary<string, object> { { "id", nodeProvider.Node.NodeID } }).Rows)
			{
				ewInterfaces.Add(Convert.ToInt32(dr[0]), new EWInterface(dr));
			}
		}
        */
		// preprocessing
		foreach (DataRow r in table.Rows)
		{
			// interface details
			r["InterfaceURL"] = String.Format("/Orion/Interfaces/InterfaceDetails.aspx?NetObject=I:{0}", r["InterfaceID"]);
			r["StatusText"] = FormatHelper.GetStatusText(Convert.ToInt32(r["Status"]));
            r["Icon"] = SolarWinds.Orion.Web.Helpers.ImageHelper.GetInterfaceTypeIcon(r["Icon"].ToString().Trim());

            /*
			int interfaceID = 0;
			int.TryParse(r["InterfaceID"].ToString(), out interfaceID);

			String ewIcon = "&nbsp;";
            
			if (ewInterfaces.ContainsKey(interfaceID))
			{
				this.ewLevelHead.InnerText = Resources.InterfacesWebContent.NPMWEBDATA_VB0_232;
				ewIcon = EnergyWiseResourceBase.GetPowerLevelIcon(ewInterfaces[interfaceID]);
			}

			r["EWPort"] = ewIcon;
            */
		}

		// bind data to repeater
		this.NodesRepeater.DataSource = table;
		this.NodesRepeater.DataBind();
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
