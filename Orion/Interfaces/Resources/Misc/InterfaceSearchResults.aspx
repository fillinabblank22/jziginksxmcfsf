<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" Title="<%$ Resources:InterfacesWebContent,NPMWEBDATA_AK0_71%>" %>
<%@ Register TagPrefix="orion" TagName="SmallNodeStatus" Src="~/Orion/Controls/SmallNodeStatus.ascx" %>
<%@ Register TagPrefix="orion" TagName="SmallStatusIcon" Src="~/Orion/Controls/SmallStatusIcon.ascx" %>
<%@ Import Namespace="SolarWinds.Orion.NPM.Web" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DisplayTypes" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>

<script runat="server">
    protected override void OnInit(EventArgs e)
    {
        string ifProp = Request.QueryString["Property"];
        string searchText = Request.QueryString["SearchText"];
        
        if (string.IsNullOrEmpty(ifProp) || string.IsNullOrEmpty(searchText))
        {
            phInvalidURLMessage.Visible = true;
        }
        else
        {
            searchText = searchText.Replace("*", ".*");
            searchText = searchText.Replace("?", ".?");

            Dictionary<int, List<Interface>> myList = BuildList(searchText, ifProp);
            if (0 == myList.Count)
            {
                this.phNoInterfacesMessage.Visible = true;
            }
            else
            {
                phMessage.Visible = true;
                // data bind
                this.rptNodes.DataSource = myList;
                this.rptNodes.DataBind();  
            }
        }
        
        base.OnInit(e);
    }

    private Dictionary<int, List<Interface>> BuildList(string searchText, string ifProp)
    {
        Dictionary<int, List<Interface>> myList = new Dictionary<int, List<Interface>>();

        List<NetObject> objs = new List<NetObject>(NetObjectFactory.GetAllObjects(typeof(Interface)));

        List<Interface> interfaces = new List<Interface>();
        foreach (NetObject o in objs)
        {
            if (o is Interface)
            {
                Interface iFace = o as Interface;
                string prop = ifProp.Equals("Status", StringComparison.OrdinalIgnoreCase)
                                  ? (new Status(Status.FromStringInt(iFace[ifProp]))).ToLocalizedString()
                                  : iFace[ifProp].ToString();
                if (Regex.IsMatch(prop, searchText, RegexOptions.IgnoreCase))
                {
                    interfaces.Add(iFace);
                }
            }
        }

        foreach (Interface iFace in interfaces)
        {
            if (!myList.ContainsKey(iFace.NodeID))
            {
                myList[iFace.NodeID] = new List<Interface>();                
            }
        
            myList[iFace.NodeID].Add(iFace);
        }

        return myList;
    }

    private Node _node;
    protected Node GetNode(int nodeID)
    {
        if (null == this._node || this._node.NodeID != nodeID)
        {
            this._node = (Node)NetObjectFactory.Create(string.Format("N:{0}", nodeID));
        }

        return _node;
    }

    protected string GetInterfacePropertyDisplayName(string key)
    {
        switch (key)
        {
            case "Caption":
                return Resources.InterfacesWebContent.NPMWEBCODE_VB0_87;
            case "InterfaceName":
                return Resources.InterfacesWebContent.NPMWEBCODE_VB0_88;
            case "InterfaceAlias":
                return Resources.InterfacesWebContent.NPMWEBCODE_VB0_89;
            case "Status":
                return Resources.InterfacesWebContent.NPMWEBDATA_VB0_108;
            case "PhysicalAddress":
                return Resources.InterfacesWebContent.NPMWEBDATA_VB0_120;
            case "AdminStatus":
                return Resources.InterfacesWebContent.NPMWEBDATA_VB0_121;
            case "InterfaceTypeDescription":
                return Resources.InterfacesWebContent.NPMWEBCODE_VB0_90;
            case "NodeName":
                return Resources.InterfacesWebContent.NPMWEBCODE_VB0_91;
            case "FullName":
                return Resources.InterfacesWebContent.NPMWEBCODE_VB0_92;
            case "CustomPollerLastStatisticsPoll":
                return Resources.InterfacesWebContent.NPMWEBCODE_VB0_93;
            default:
                return key;
        }
    }
</script>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:PlaceHolder ID="phMessage" runat="server" Visible="false">
        <div class="StatusMessage DefaultShading">
            <%= string.Format(Resources.InterfacesWebContent.NPMWEBDATA_AK0_68, GetInterfacePropertyDisplayName(Server.HtmlEncode(Request.QueryString["Property"])), Server.HtmlEncode(Request.QueryString["SearchText"]))%>
        </div>
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="phNoInterfacesMessage" runat="server" Visible="false">
        <div class="StatusMessage DefaultShading">
            <%= string.Format(Resources.InterfacesWebContent.NPMWEBDATA_AK0_69, GetInterfacePropertyDisplayName(Server.HtmlEncode(Request.QueryString["Property"])), Server.HtmlEncode(Request.QueryString["SearchText"]))%>
        </div>
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="phInvalidURLMessage" runat="server" Visible="false">
        <div class="StatusMessage DefaultShading">
            <%=Resources.InterfacesWebContent.NPMWEBDATA_AK0_70%>
        </div>
    </asp:PlaceHolder>
    
    <asp:Repeater ID="rptNodes" runat="server">
        <ItemTemplate>
            <div style="margin-left: 24px;">
                <orion:CollapsePanel runat="server" DataSource='<%# Container.DataItem %>'>
                    <TitleTemplate>
                        <orion:SmallNodeStatus runat="server" StatusValue='<%# this.GetNode(Convert.ToInt32(Eval("Key"))).Status %>' />
                        <a href='/Orion/NetPerfMon/NodeDetails.aspx?NetObject=<%# this.GetNode(Convert.ToInt32(Eval("Key"))).NetObjectID %>'>
                            <%# this.GetNode(Convert.ToInt32(Eval("Key"))).Name %>
                        </a>
                    </TitleTemplate>
                    <BlockTemplate>
                        <asp:Repeater runat="server" DataSource='<%# Eval("Value") %>'>
                            <ItemTemplate>
                            <div style="margin-left: 12px;">
                                    <orion:SmallStatusIcon ID="SmallStatusIcon1" runat="server" StatusValue='<%# Eval("Status") %>' />
                                    <a href='/Orion/NPM/InterfaceDetails.aspx?NetObject=<%# Eval("NetObjectID") %>'><%# Eval("Caption") %></a>
                                </div>                        
                            </ItemTemplate>
                        </asp:Repeater>
                    </BlockTemplate>
                </orion:CollapsePanel>
            </div>
            
        </ItemTemplate>
    </asp:Repeater>
</asp:Content>

