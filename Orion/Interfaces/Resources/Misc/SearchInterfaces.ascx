<%@ Control Language="C#" ClassName="SearchNodes" Inherits="SolarWinds.Orion.Web.UI.BaseResourceControl" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Import Namespace="System.Text.RegularExpressions" %>
<%@ Import Namespace="SolarWinds.Orion.NPM.Web" %>
<%@ Import Namespace="SolarWinds.Orion.Web.UI" %>

<script runat="server">
    protected override void OnInit(EventArgs e)
    {
        InitInterfaceProps();

        txtSearchString.Attributes.Add("onkeydown", "if(event.keyCode==13){var e=jQuery.Event(event); e.stopPropagation(); e.preventDefault(); __doPostBack('" + btnSearch.UniqueID + "',''); return false; }");

        var script = string.Format("if($('#{0}').val()=='') return false;", txtSearchString.ClientID);
        btnSearch.Attributes.Add("onclick", script);
        
        base.OnInit(e);
    }
    
    private void InitInterfaceProps()
    {
        ListItemCollection properties = new ListItemCollection();
        lbxInterfaceProperty.Items.Clear();
        lbxInterfaceProperty.Items.Add(new ListItem(Resources.InterfacesWebContent.NPMWEBCODE_VB0_87, "Caption"));
		lbxInterfaceProperty.Items.Add(new ListItem(Resources.InterfacesWebContent.NPMWEBCODE_VB0_88, "InterfaceName"));
		lbxInterfaceProperty.Items.Add(new ListItem(Resources.InterfacesWebContent.NPMWEBCODE_VB0_89, "InterfaceAlias"));
        lbxInterfaceProperty.Items.Add(new ListItem(Resources.InterfacesWebContent.NPMWEBDATA_VB0_108,"Status"));
        lbxInterfaceProperty.Items.Add(new ListItem(Resources.InterfacesWebContent.NPMWEBDATA_VB0_120, "PhysicalAddress"));
        lbxInterfaceProperty.Items.Add(new ListItem(Resources.InterfacesWebContent.NPMWEBDATA_VB0_121, "AdminStatus"));
        lbxInterfaceProperty.Items.Add(new ListItem(Resources.InterfacesWebContent.NPMWEBCODE_VB0_90, "InterfaceTypeDescription"));
		lbxInterfaceProperty.Items.Add(new ListItem(Resources.InterfacesWebContent.NPMWEBCODE_VB0_91, "NodeName"));
		lbxInterfaceProperty.Items.Add(new ListItem(Resources.InterfacesWebContent.NPMWEBCODE_VB0_92, "FullName"));
		lbxInterfaceProperty.Items.Add(new ListItem(Resources.InterfacesWebContent.NPMWEBCODE_VB0_93,"CustomPollerLastStatisticsPoll"));

        foreach (string prop in Interface.GetCustomPropertyNames(true))
        {
            lbxInterfaceProperty.Items.Add(prop);
        }

        //lbxNodeProperty.DataBind();
    }

    protected void SearchClick(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtSearchString.Text))
        {
            Response.Redirect(string.Format("/Orion/Interfaces/Resources/Misc/InterfaceSearchResults.aspx?Property={0}&SearchText={1}",
                                lbxInterfaceProperty.SelectedValue, txtSearchString.Text)
                            );
        }
    }
    
    protected override string DefaultTitle
    {
        get { return Resources.InterfacesWebContent.NPMWEBCODE_VB0_94; }
    }

    public override string HelpLinkFragment
    {
        get
        {
			return "OrionPHResourceSearchInterfaces";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Static; }
    }
</script>

<orion:ResourceWrapper runat="server">
    <Content>
        <table class="DefaultShading LayoutTable">
            <tr>
                <td><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_205%></td>
                <td><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_206%></td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox runat="server" ID="txtSearchString" Width="95%" Rows="1" />
                </td>
                <td>
                    <asp:ListBox runat="server" ID="lbxInterfaceProperty" Rows="1" SelectionMode="Single" Width="100%" />
                </td>
                <td>
                    <orion:LocalizableButton runat="server" ID="btnSearch" CssClass="resList" LocalizedText="CustomText" Text="<%$ Resources: InterfacesWebContent, NPMWEBDATA_VB0_218%>" DisplayType="Primary" OnClick="SearchClick" />
                </td>
            </tr>
            <tr><td colspan="3" class="helpfulTextWithoutPadding"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_208%></td></tr>
        </table>
    </Content>
</orion:ResourceWrapper>