﻿using System;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Interfaces_Resources_VLANs_ListOfVlans : BaseResourceControl
{
    private const string query = @"SELECT n.NodeID
FROM Orion.Nodes AS n 
JOIN Orion.NodeVlans AS nv on n.NodeID = nv.NodeID
WHERE (2 IN (
	SELECT Count(p.PollerType) AS Cnt
	FROM Orion.Pollers AS p
	WHERE p.NetObjectID = n.NodeID AND p.NetObjectType = 'N' AND Enabled = true AND (p.PollerType LIKE '%_PortsMap.%' OR p.PollerType LIKE '%_Vlans.%')
	))
GROUP By n.NodeID";

    protected override string DefaultTitle
    {
        get { return Resources.InterfacesWebContent.NPMWEBDATA_GK0_7; }
    }

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        using (var swis = InformationServiceProxy.CreateV3())
        {
            var pollers = swis.Query(query);

            this.Visible = pollers != null && pollers.Rows.Count > 0;
        }
    }
}