﻿using SolarWinds.Orion.Core.Common.i18n;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;

public partial class Orion_Interfaces_Resources_VLANs_VlanInterfaces : BaseResourceControl
{
    private const string VLANPOLLERSQUERY = @"SELECT PollerType, Enabled FROM Orion.Pollers WHERE NetObject = @NetObjectID AND (PollerType LIKE '%Topology_PortsMap%' OR PollerType LIKE '%Topology_Vlans%')";

    private bool IsVlanPollerEnabled(string netObjectId)
    {
        using (var swis = InformationServiceProxy.CreateV3())
        {
            var pollers = swis.Query(VLANPOLLERSQUERY, new Dictionary<string, object> { { "NetObjectID", netObjectId } });

            bool isPollerTableEmpty = pollers == null || pollers.Rows.Count == 0;
            bool isPortMapPollerPresent = false;
            bool isVlanPollerPresent = false;

            if (!isPollerTableEmpty)
            {
                foreach (DataRow poller in pollers.Rows)
                {
                    string pollerType = Convert.ToString(poller["PollerType"]);
                    bool isPollerEnabled = Convert.ToBoolean(poller["Enabled"]);

                    if (isPollerEnabled)
                    {
                        if (pollerType.ToUpperInvariant().Contains("TOPOLOGY_PORTSMAP"))
                        {
                            isPortMapPollerPresent = true;
                        }
                        else if (pollerType.ToUpperInvariant().Contains("TOPOLOGY_VLANS"))
                        {
                            isVlanPollerPresent = true;
                        }
                    }

                }
            }

            return isPortMapPollerPresent && isVlanPollerPresent;
        }
    }

    public int NodeId;

    protected void Page_Load(object sender, EventArgs e)
    {
        INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();
        NodeId = -1;
        if (nodeProvider != null)
        {
            NodeId = nodeProvider.Node.NodeID;

            // hide if poller doesn't exist or is disabled
            if (!IsVlanPollerEnabled(nodeProvider.Node.NetObjectID))
            {
                this.Visible = false;
                return;
            }
        }

        if (NodeId <= 0)
        {
            this.Visible = false;
            return;
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.InterfacesWebContent.NPMWEBDATA_GK0_6; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new Type[] { typeof(INodeProvider) };
        }
    }
	
	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }

    protected override void Render(HtmlTextWriter writer)
    {
        tabTemplate.Text = TokenSubstitution.Parse(tabTemplate.Text);
        base.Render(writer);
    }
}