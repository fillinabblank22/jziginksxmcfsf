﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListOfAllVlans.ascx.cs" Inherits="Orion_Interfaces_Resources_VLANs_ListOfVlans" %>

<orion:ResourceWrapper runat="server" ID="Wrapper1">
    <content>
        <orion:Include ID="Include1" runat="server" File="../../Orion/Interfaces/Resources/VLANs/VlanSummary.js" />
		<orion:Include ID="Include2" runat="server" File="../../Orion/Interfaces/Styles/VLAN.css" />
		
        <script type="text/javascript">
            $(function () {
                SW.Interfaces.ListOfVlans.SetupListOfVlans('<%= Resource.ID %>', 'listOfVlansTemplate-<%= Resource.ID %>');
            });
        </script>
        <script id="listOfVlansTemplate-<%= Resource.ID %>" type="text/x-template">
        <table class="vlanInterfacesTable">
            <thead>
                <tr class="HeaderRow">
                    <td class="ReportHeader vlanId"><%= Resources.InterfacesWebContent.NPMWEBDATA_GK0_1 %></td>
                    <td class="ReportHeader vlanName"><%= Resources.InterfacesWebContent.NPMWEBDATA_GK0_2 %></td>
                    <td class="ReportHeader vlanNodes"><%= Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_17 %></td>
                </tr>
            </thead>
            {# _.each(vlans, function(current, index) { #}
            <tbody>
                <tr class="vlanAccordion">
                    <td class="vlanId"><img src="/Orion/Images/Button.Expand.gif" class="vlanExpander"/><img class="vlanStatusIcon" src="{{ current.VlansStatusIcon }}" title="{{ current.VlansStatusIconTitle }}" alt="{{ current.VlansStatusIconTitle }}"/>&nbsp;{{ current.VlanId }}</td>
                    <td>{# if (current.VlanName) { #}{{ current.VlanName }}{# } else { #}&nbsp;{# } #}</td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
            <tbody class="itemList hidden" data-row-id="{{ current.VlanId }}">
                <tr class="showMoreRow hidden">
                    <td>&nbsp;</td>
                    <td class="showMoreLink"><a href="#"><%= Resources.InterfacesWebContent.NPMWEBDATA_PS1_9 %></a></td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="loadingRow hidden">
                    <td>&nbsp;</td>
                    <td colspan="2"><%= Resources.CoreWebContent.WEBDATA_AK0_213 %></td>
                </tr>
            </tbody>
            {# }); #}
        </table>
        </script>
        <script id="listOfVlansRowTemplate-<%= Resource.ID %>" type="text/x-template">
                <tr>
                    <td>&nbsp;</td>
                    <td><img class="vlanStatusIcon" src="{{ item.VlanStatusIcon }}" title="{{ item.VlanStatusIconTitle }}" alt="{{ item.VlanStatusIconTitle }}"/>&nbsp;{# if (item.VlanName !== undefined) { #}{{ item.VlanName }}{# } else { #}<i>VLAN#{{ item.VlanId }}</i>{# } #}</td>
                    <td><a href="{{ item.NodeDetailsLink }}"><img class="StatusIcon" alt="{{ item.VlanStatusIconTitle }}" title="{{ item.VlanStatusIconTitle }}" src="/NetPerfmon/images/small-{{ item.NodeStatusIcon }}"/>{{ item.NodeName }}</a></td>
                </tr>
        </script> 
        <span id="Content-<%= Resource.ID %>"></span>
        <div id="Pager-<%= Resource.ID %>" class="ReportFooter ResourcePagerControl"></div>
    </content>
</orion:ResourceWrapper>

