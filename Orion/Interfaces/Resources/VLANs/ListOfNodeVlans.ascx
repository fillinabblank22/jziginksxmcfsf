﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListOfNodeVlans.ascx.cs" Inherits="Orion_Interfaces_Resources_VLANs_VlanInterfaces" %>

<orion:ResourceWrapper runat="server" ID="Wrapper1">
    <content>
        <orion:Include ID="Include1" runat="server" File="../../Orion/Interfaces/Resources/VLANs/VlanSummary.js" />
		<orion:Include ID="Include2" runat="server" File="../../Orion/Interfaces/Styles/VLAN.css" />
		
        <script type="text/javascript">
            $(function () {
                SW.Interfaces.ListOfVlans.SetupListOfVlansOnNode('<%= Resource.ID %>', '<%= NodeId %>');
            });
        </script>
        <script id="listOfVlansTemplate-<%= Resource.ID %>" type="text/x-template">
        <table class="vlanInterfacesTable">
            <thead>
                <tr class="HeaderRow">
                    <td class="ReportHeader vlanId"><%= Resources.InterfacesWebContent.NPMWEBDATA_GK0_1 %></td>
                    <td class="ReportHeader vlanName"><%= Resources.InterfacesWebContent.NPMWEBDATA_GK0_2 %></td>
                    <td class="ReportHeader vlanSaid"><%= Resources.InterfacesWebContent.NPMWEBDATA_GK0_3 %></td>
                    <td class="ReportHeader portType"><%= Resources.InterfacesWebContent.NPMWEBCODE_PS1_1 %></td>
                </tr>
            </thead>
            {# _.each(vlans, function(current, index) { #}
            <tbody>
                <tr class="vlanAccordion">
                    <td class="vlanId">{# if (current.ItemCount > 0) { #}<img src="/Orion/Images/Button.Expand.gif" class="vlanExpander" />{# } else { #}<div class="emptyExpander">&nbsp;</div>{# } #}&nbsp;
                        <img class="vlanStatusIcon" src="{{ current.VlanStatusIcon }}" alt="{{ current.VlanStatusIconTitle }}" title="{{ current.VlanStatusIconTitle }}"/>{{ current.VlanId }}</td>
                    <td class="vlanId"><a title="VLAN" class="vlanTip" data-vlanid="{{ current.VlanId }}" href="#" rel="/Orion/Interfaces/Services/VLANs.asmx/GetPopup">{# if (current.VlanName) { #}{{ current.VlanName }}{# } else { #}<i>VLAN#{{current.VlanId }}</i>{# } #}</a></td>
                    <td>{# if (current.Tag) { #}{{ current.Tag }}{# } else { #}<span class="noTag"><%= Resources.InterfacesWebContent.NPMWEBCODE_VB0_12 %></span>{# } #}</td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
            <tbody class="itemList hidden" data-row-id="<%= NodeId %>|{{ current.VlanId }}">
                {# if (current.ItemCount > 0) { #}
                <tr class="showMoreRow hidden">
                    <td>&nbsp;</td>
                    <td class="showMoreLink"><a href="#"><%= Resources.InterfacesWebContent.NPMWEBDATA_PS1_9 %></a></td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="loadingRow hidden">
                    <td>&nbsp;</td>
                    <td colspan="2"><%= Resources.CoreWebContent.WEBDATA_AK0_213 %></td>
                </tr>
                {# } else {#}  
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="3">
                        <asp:Literal runat="server" ID="tabTemplate">
                        @{R=Interfaces.Strings;K=NPMWEBJS_GK0_6; E=js}
                        </asp:Literal>
                    </td>
                </tr>
                {# } #}
            </tbody>
            {# }); #}     
            
        </table>
        </script>
        <script id="listOfVlansRowTemplate-<%= Resource.ID %>" type="text/x-template">
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2">
                    <a href="{{ item.InterfaceDetailsLink }}">
                        <img class="StatusIcon" alt="Status" src="/NetPerfmon/images/small-{{ item.InterfaceStatusIcon }}"/>
                        {{ item.InterfaceName }}
                    </a>
                    </td>
                    <td>{{ item.InterfaceType }}</td>
                </tr>
        </script>
        <span id="Content-<%= Resource.ID %>"></span>
        <div id="Pager-<%= Resource.ID %>" class="ReportFooter ResourcePagerControl"></div>
    </content>
</orion:ResourceWrapper>