using System;
using System.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.NPM.Web.UI;
using SolarWinds.Orion.Web.UI;

public partial class PercentUtilizationPieCharts : GaugeResource<IInterfaceProvider>
{
	protected void Page_Load(object sender, EventArgs e)
	{
		if (!isError())
		{
			ScriptManager.RegisterStartupScript(this, typeof(string), "clickpie", GetOpenChartScript(), true);
			gaugeIN.Value = (int)GetCurrentNetObject.Interface.InPercentUtil.Value;
			gaugeIN.TitleBottom = Convert.ToString((int)GetCurrentNetObject.Interface.InPercentUtil.Value) + " %";
			gaugeIN.TitleTop = Resources.InterfacesWebContent.NPMWEBCODE_VB0_42;
			gaugeIN.isWarning = (GetCurrentNetObject.Interface.InPercentUtil.Value > Thresholds.IfPercentUtilizationWarning.SettingValue);


			gaugeOUT.Value = (int)GetCurrentNetObject.Interface.OutPercentUtil.Value;
			gaugeOUT.TitleBottom = Convert.ToString((int)GetCurrentNetObject.Interface.OutPercentUtil.Value) + " %";
			gaugeOUT.TitleTop = Resources.InterfacesWebContent.NPMWEBCODE_VB0_43;
			gaugeOUT.isWarning = (GetCurrentNetObject.Interface.InPercentUtil.Value > Thresholds.IfPercentUtilizationWarning.SettingValue);
		}
	}

	public override Control GetSourceControl() { return Source;	}

	public override ITextControl GetErrorControl() { return (ITextControl)ErrorLiteral; }

	public override string GetErrorText() { return Resources.InterfacesWebContent.NPMWEBDATA_VB0_132; }

	//public override string DisplayTitle { get { return "Percent Utilization"; } }

	protected override string DefaultTitle { get { return Resources.InterfacesWebContent.NPMWEBCODE_VB0_44; } }

	public override string HelpLinkFragment { get { return "OrionPHResourcePercentUtilizationGauge"; } }

    public override string EditControlLocation
    {
        get { return string.Empty; }
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
