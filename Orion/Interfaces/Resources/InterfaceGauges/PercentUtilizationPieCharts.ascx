<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PercentUtilizationPieCharts.ascx.cs" Inherits="PercentUtilizationPieCharts" %>
<%@ Register Src="~/Orion/NetPerfMon/Controls/Pie3D.ascx" TagName="Pie3D" TagPrefix="uc1" %>
<%@ Register TagPrefix="npm" Namespace="SolarWinds.NPM.Web.Gauge.V1" Assembly="SolarWinds.NPM.Web.Gauge.V1" %>
<%@ Register TagPrefix="npm" TagName="ThresholdButton" Src="~/Orion/Interfaces/Controls/ThresholdButton.ascx"%>

<link rel="stylesheet" type="text/css" href="/Orion/styles/GaugeStyle.css" />
<orion:resourceWrapper runat="server" ShowEditButton="false" ID="Wrapper">
    <Content>
        <asp:PlaceHolder ID="Source" runat="server">
        <br />
		
		<script type="text/javascript">

		    $(function () {
		        $("a.openlink > div").on("click", function (e) {
		            var link = $(e.target).parents(".openlink");
		            var url = link.attr("href");
		            window.open(url, '_blank');
		        });
		    });

		</script>
		
        <table class="GaugeTable">
	        <tr>
		        <td>
			        <a class="openlink" href="/Orion/Charts/CustomChart.aspx?chartName=MMAvgUtil&NetObject=<%=GetCurrentNetObject.Interface.NetObjectID%>&Period=Today" target="_blank">
			            
			            <uc1:Pie3D ID="gaugeIN" runat="server" />
			            
			        </a>
	            </td>
	            <td>
			        <a class="openlink" href="/Orion/Charts/CustomChart.aspx?chartName=MMAvgUtil&NetObject=<%=GetCurrentNetObject.Interface.NetObjectID%>&Period=Today" target="_blank">
			            <uc1:Pie3D ID="gaugeOUT" runat="server" />
			        </a>
	            </td>
	        </tr>
       </table>
       </asp:PlaceHolder>
       <asp:Literal ID="ErrorLiteral" runat="server" />
    </Content>
    <HeaderButtons>
		<npm:ThresholdButton ID="ThresholdButton1" runat="server" />
    </HeaderButtons>
</orion:resourceWrapper>

