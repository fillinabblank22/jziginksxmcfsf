using System;
using System.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.NPM.Web.UI;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Interfaces.Web.DAL;


public partial class InOutPercentUtilRadial : GaugeResource<IInterfaceProvider>
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!isError())
        {
            if (IsVisible())
            {
                Parent.Visible = true;

                CreateGauge(GetIntProperty("Scale", 100), GetStringValue("Style", "Elegant Black"),
                    Resources.InterfacesWebContent.NPMWEBCODE_VB0_42, gaugePlaceHolder1, "/Orion/Charts/CustomChart.aspx?chartName=MMAvgUtil&NetObject=" + GetCurrentNetObject.Interface.NetObjectID + "&Period=Today",
                    GetCurrentNetObject.Interface.NetObjectID, "RECVUtilization", "Radial", " %", 0, 100);
                CreateGauge(GetIntProperty("Scale", 100), GetStringValue("Style", "Elegant Black"),
                    Resources.InterfacesWebContent.NPMWEBCODE_VB0_43, gaugePlaceHolder2, "/Orion/Charts/CustomChart.aspx?chartName=MMAvgUtil&NetObject=" + GetCurrentNetObject.Interface.NetObjectID + "&Period=Today",
                    GetCurrentNetObject.Interface.NetObjectID, "XMITUtilization", "Radial", " %", 0, 100);

                var provider = this.GetCurrentNetObject;

                if (provider != null && provider.Interface != null)
                {
                    int interfaceID = provider.Interface.InterfaceID;
                    using (var swisProxy = InformationServiceProxy.CreateV3())
                    {
                        var dal = new InterfacesDAL(swisProxy);
                        interfaceObsoleteData.Visible = dal.HasInterfaceObsoleteData(interfaceID);
                    }
                }
            }
            else
            {
                Parent.Visible = false;
            }
        }
    }

    public override Control GetSourceControl() { return Source; }

    public override ITextControl GetErrorControl() { return (ITextControl)ErrorLiteral; }

    public override string GetErrorText() { return Resources.InterfacesWebContent.NPMWEBDATA_VB0_132; }

    protected override string DefaultTitle { get { return Resources.InterfacesWebContent.NPMWEBCODE_VB0_47; } }

    public override string HelpLinkFragment { get { return "OrionPHResourcePercentUtilizationGauge"; } }

    private bool IsVisible()
    {
        return ((GetCurrentNetObject.Interface.InPercentUtil.Value != -2) &&
            (GetCurrentNetObject.Interface.OutPercentUtil.Value != -2));
    }

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }

}
