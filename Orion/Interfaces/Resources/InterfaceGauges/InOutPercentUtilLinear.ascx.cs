using System;
using System.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.NPM.Web.UI;
using SolarWinds.NPM.Web.Gauge.V1;
using SolarWinds.Orion.Web.UI;


public partial class InOutPercentUtilLinear : GaugeResource<IInterfaceProvider>
{
	protected void Page_Load(object sender, EventArgs e)
	{
		if (!isError())
		{	
			CreateGauge(GetIntProperty("Scale", 100), GetStringValue("Style", "Tan Beveled Flood"), 
				Resources.InterfacesWebContent.NPMWEBCODE_VB0_42, gaugePlaceHolder1, "/Orion/Charts/CustomChart.aspx?chartName=MMAvgUtil&NetObject=" + GetCurrentNetObject.Interface.NetObjectID + "&Period=Today",
				GetCurrentNetObject.Interface.NetObjectID, "RECVUtilization", "Linear", " %", 0, 100);
			CreateGauge(GetIntProperty("Scale", 100), GetStringValue("Style", "Tan Beveled Flood"), 
				Resources.InterfacesWebContent.NPMWEBCODE_VB0_43, gaugePlaceHolder2, "/Orion/Charts/CustomChart.aspx?chartName=MMAvgUtil&NetObject=" + GetCurrentNetObject.Interface.NetObjectID + "&Period=Today",
				GetCurrentNetObject.Interface.NetObjectID, "XMITUtilization", "Linear", " %", 0, 100);
		}
	}
	public override GaugeType GaugeType { get { return GaugeType.Linear; } }

	public override Control GetSourceControl() { return Source; }

	public override ITextControl GetErrorControl() { return (ITextControl)ErrorLiteral; }

	public override string GetErrorText() { return Resources.InterfacesWebContent.NPMWEBDATA_VB0_132; }

	protected override string DefaultTitle { get { return Resources.InterfacesWebContent.NPMWEBCODE_VB0_49; } }

	public override string HelpLinkFragment { get { return "OrionPHResourcePercentUtilizationGauge"; } }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
	
	
}
