<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InOutPercentUtilLinear.ascx.cs" Inherits="InOutPercentUtilLinear" %>
<%@ Register TagPrefix="npm" Namespace="SolarWinds.NPM.Web.Gauge.V1" Assembly="SolarWinds.NPM.Web.Gauge.V1" %>
<%@ Register TagPrefix="npm" TagName="ThresholdButton" Src="~/Orion/Interfaces/Controls/ThresholdButton.ascx"%>

<link rel="stylesheet" type="text/css" href="/Orion/styles/GaugeStyle.css" />
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:PlaceHolder ID="Source" runat="server">
        <br />
        <table class="GaugeTable">
	        <tr>
		        <td>
			        <a href="/Orion/Charts/CustomChart.aspx?chartName=MMAvgUtil&NetObject=<%=GetCurrentNetObject.Interface.NetObjectID%>&Period=Today" target="_blank">
			            <asp:PlaceHolder runat="server" ID="gaugePlaceHolder1" />
			        </a>
	            </td>
	        </tr>
	        <tr>
	            <td>
			        <a href="/Orion/Charts/CustomChart.aspx?chartName=MMAvgUtil&NetObject=<%=GetCurrentNetObject.Interface.NetObjectID%>&Period=Today" target="_blank">
			            <asp:PlaceHolder runat="server" ID="gaugePlaceHolder2" />
			        </a>
	            </td>
	        </tr>
       </table>
       </asp:PlaceHolder>
       <asp:Literal ID="ErrorLiteral" runat="server" />
    </Content>
    <HeaderButtons>
		<npm:ThresholdButton runat="server" />
    </HeaderButtons>
</orion:resourceWrapper>