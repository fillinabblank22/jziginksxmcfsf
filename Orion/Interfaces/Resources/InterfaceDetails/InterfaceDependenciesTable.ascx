<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterfaceDependenciesTable.ascx.cs" Inherits="Orion_NPM_Resources_InterfaceDetails_InterfaceDependenciesTable" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div runat="server" id="errorMessage" style="color:Red;" visible="false"></div>
        <asp:Repeater ID="DependencyTable" runat="server">
            <HeaderTemplate>
                <table width="100%" cellspacing="0" cellpadding="2">
                    <tr class="ReportHeader">
                        <td class="ReportHeader" colspan="2">
                            &nbsp;<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_105%>
                        </td>
                        <td class="ReportHeader" colspan="3">
                            &nbsp;<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_106%>
                        </td>
                        <td class="ReportHeader" colspan="2">
                            &nbsp;<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_107%>
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td class="Property" width="10px" style="padding: 0px 5px 0px 5px;">
                        <img src="/Orion/images/dependency_16x16.gif" alt="" />
                    </td>
                    <td class="Property" width="35%">
                        <%#SolarWinds.Orion.Core.Web.FormatHelper.MakeBreakableString(Eval("Name").ToString())%>
                    </td>
                    <td class="Property" style="background-color: #eaf7fe; padding: 0px 5px 0px 5px;" width="10px">
                        <img src="<%#CommonWebHelper.GetStatusIconPath(Eval("ParentUri"), Eval("Status"))%>"
                            alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_108%>" />
                    </td>
                    <td class="Property" style="background-color: #eaf7fe;" width="25%">
                        <%#CommonWebHelper.RenderObjectName(Eval("ParentUri"), Eval("DisplayName"))%>
                    </td>
                    <td class="Property" style="background-color: #eaf7fe; padding: 0px 5px 0px 5px;" width="16px">
                        <img src="/Orion/images/arrow_forward.gif" alt="" />
                    </td>
                    <td class="Property" style="background-color: #eaf7fe; padding: 0px 5px 0px 5px;" width="10px">
                        <img src="<%#CommonWebHelper.GetStatusIconPath(Eval("ChildUri"), Eval("Status1"))%>"
                            alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_108%>" />
                    </td>
                    <td class="Property" style="background-color: #eaf7fe;" width="25%">
                        <%#CommonWebHelper.RenderObjectName(Eval("ChildUri"), Eval("DisplayName1"))%>
                    </td>
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr>
                    <td class="Property" style="background-color: #e4f1f8; padding: 0px 5px 0px 5px;" width="10px">
                        <img src="/Orion/images/dependency_16x16.gif" alt="" />
                    </td>
                    <td class="Property" style="background-color: #e4f1f8;" width="35%">
                        <%#SolarWinds.Orion.Core.Web.FormatHelper.MakeBreakableString(Eval("Name").ToString())%>
                    </td>
                    <td class="Property" style="background-color: #d3ebf8; padding: 0px 5px 0px 5px;" width="10px">
                        <img src="<%#CommonWebHelper.GetStatusIconPath(Eval("ParentUri"), Eval("Status"))%>"
                            alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_108%>" />
                    </td>
                    <td class="Property" style="background-color: #d3ebf8;" width="25%">
                        <%#CommonWebHelper.RenderObjectName(Eval("ParentUri"), Eval("DisplayName"))%>
                    </td>
                    <td class="Property" style="background-color: #d3ebf8; padding: 0px 5px 0px 5px;" width="16px">
                        <img src="/Orion/images/arrow_forward.gif" alt="" />
                    </td>
                    <td class="Property" style="background-color: #d3ebf8; padding: 0px 5px 0px 5px;" width="10px">
                        <img src="<%#CommonWebHelper.GetStatusIconPath(Eval("ChildUri"), Eval("Status1"))%>"
                            alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_108%>" />
                    </td>
                    <td class="Property" style="background-color: #d3ebf8;" width="25%">
                        <%#CommonWebHelper.RenderObjectName(Eval("ChildUri"), Eval("DisplayName1"))%>
                    </td>
                </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>
