using System;
using SolarWinds.Logging;
using SolarWinds.NPM.Common;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Interfaces.Common;

public partial class Orion_NetPerfMon_Resources_InterfaceDetails_InterfaceCustomProperties : BaseResourceControl
{
    private static Log log = new Log();

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.customPropertyList.Resource = this.Resource;
        Interface intf = GetInterfaceInstance<IInterfaceProvider>().Interface;
        if (intf != null)
        {
            this.customPropertyList.NetObjectId = intf.InterfaceID;
        }

        customPropertyList.CustomPropertyLoader = (netObjectId, displayProperties) =>
        {
            using (var proxy = InterfacesBusinessLayerProxyFactory.Instance.Create())
            {
                try
                {
                    return proxy.Api.GetInterfaceCustomProperties(netObjectId, displayProperties);
                }
                catch(Exception ex)
                {
                    BusinessLayerExceptionHandler(ex);
                    throw;
                }
            }
        };

        customPropertyList.EditCustomPropertiesLinkGenerator = (netObjectId, redirectUrl) =>
        {
            if (Request.UrlReferrer != null)
            {
                redirectUrl = UrlHelper.ToSafeUrlParameter(Request.UrlReferrer.PathAndQuery);
            }
            return String.Format("/Orion/Interfaces/InterfaceProperties.aspx?Interfaces={0}&ReturnTo={1}", netObjectId, redirectUrl);	
        };
    }

    protected void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

    protected override string DefaultTitle
    {
        get { return Resources.InterfacesWebContent.NPMWEBCODE_VB0_26; }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/NetPerfMon/Controls/EditResourceControls/EditCustomPropertyList.ascx";
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceEditCustomInterfaceProperty"; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IInterfaceProvider) }; }
    }

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
