﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterfaceDowntimeMonitoring.ascx.cs" Inherits="Orion_Interfaces_Resources_InterfaceDetails_InterfaceDowntimeMonitoring" %>

<% if (IsDowntimeMonitoringEnabled) { %>
<orion:Include ID="Include1" runat="server" File="DowntimeMonitoring.js" />
<orion:Include ID="Include2" runat="server" File="DowntimeMonitoring.css" />
<orion:Include ID="Include3" runat="server" File="Legend.css" />
<orion:Include ID="Include4" runat="server" File="DowntimeTooltip.css" />

<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>

<script type="text/javascript">
    $(function () {
        SW.Core.DowntimeMonitoring.ToolTip.ContentManager.initializeDowntimeControl({
            minRes: '<%=Resources.InterfacesWebContent.NPMWEBDATA_OD0_20%>',
            startDate: '<%=this.StartDate%>',
            endDate: '<%=this.EndDate%>',
            hoursPerBlock: '<%=this.HoursPerBlock%>',
            dateFormatSettings: '<%= DatePickerRegionalSettings.GetDatePickerRegionalSettingsWithHtmlEncoding()%>',
            uniqueId: '<%= CustomTable.UniqueClientID %>',
            searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
            searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
            clientId: '<%= CustomTable.ClientID %>',
            rowsPerPage: '<%= Resource.Properties["RowsPerPage"] ?? "5" %>',
            scriptFriendlyResourceId: '<%= ScriptFriendlyResourceID %>',
        });

        SW.Core.DowntimeMonitoring.Legend.createLegend(function (legendTemplate) {
            SW.Core.DowntimeMonitoring.Legend.initLegend(legendTemplate, '<%= CustomTable.ClientID %>');
			
			//Adding scroll into DowntimeControl
			var clientId = '<%= CustomTable.UniqueClientID %>';
			$('#ErrorMsg-' + clientId).after('<div id="DowntimeScrollWrapper-' + clientId + '" style="overflow: auto; overflow-y: hidden;"></div>');
		    var queryTable = $('#Grid-' + clientId).detach();
			queryTable.appendTo('#DowntimeScrollWrapper-' + clientId);
			
			//FB395284 RC2
        });
    });
</script>

<orion:resourceWrapper ID="ResourceWrapper" runat="server">
    <Content>
        <orion:CustomQueryTable runat="server" ID="CustomTable" />
    </Content>
</orion:resourceWrapper>
<% } else { %>

<orion:resourceWrapper ID="ResourceWrapper1" runat="server">
    <Content>
        <%= String.Format(Resources.InterfacesWebContent.NPMWEBDATA_JP0_01, "/Orion/Admin/PollingSettings.aspx", Resources.CoreWebContent.WEBCODE_TM0_49)%>
    </Content>
</orion:resourceWrapper>

<% } %>
