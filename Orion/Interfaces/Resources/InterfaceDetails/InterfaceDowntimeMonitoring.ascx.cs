﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using SolarWinds.Interfaces.Web.Enums;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.NPM.Web;

public partial class Orion_Interfaces_Resources_InterfaceDetails_InterfaceDowntimeMonitoring : ResourceControl
{
    protected ResourceSearchControl SearchControl { get; private set; }

    public static Boolean IsDowntimeMonitoringEnabled
    {
        get { return Convert.ToBoolean(SettingsDAL.GetSetting("SWNetPerfMon-Settings-EnableDowntimeMonitoring").SettingValue); }
    }

    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    protected override string DefaultTitle
    {
        get { return Resources.InterfacesWebContent.NPMWEBCODE_OD0_9; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (NodeHelper.IsResourceUnderExternalNode(this))
        {
            this.Visible = false;
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/Interfaces/Controls/EditResourceControls/DowntimeMonitoringEdit.ascx";
        }
    }

    protected string StartDate;
    protected string EndDate;
    protected string HoursPerBlock;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsDowntimeMonitoringEnabled)
        {
            return;
        }

        SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        ResourceWrapper.HeaderButtons.Controls.Add(SearchControl);

        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        CustomTable.SWQL = SWQL;
        CustomTable.SearchSWQL = SearchSWQL;

        var mode = Resource.Properties["InterfaceDowntimeMode"] ?? InterfaceDownTime.Mode.OneDay;
        var displayMode = Resource.Properties["InterfaceDowntimeDefaultDisplay"] ?? InterfaceDownTime.Display.Default;
        var dateNow = DateTime.Now;
        var date = dateNow.AddSeconds(-dateNow.Second).AddMilliseconds(-dateNow.Millisecond);

        HoursPerBlock = Resource.Properties["InterfaceDowntimeHoursPerBlock"] ?? "1";
        switch (mode)
        {
            case InterfaceDownTime.Mode.OneDay:
                StartDate = date.AddDays(-1).ToJsonTicks().ToString();
                EndDate = date.ToJsonTicks().ToString();
                if (displayMode == InterfaceDownTime.Display.Default)
                    HoursPerBlock = "1";
                break;
            case InterfaceDownTime.Mode.LastWeek:
                StartDate = date.AddDays(-7).ToJsonTicks().ToString();
                EndDate = date.ToJsonTicks().ToString();
                if (displayMode == InterfaceDownTime.Display.Default)
                    HoursPerBlock = "24";
                break;
            case InterfaceDownTime.Mode.CustomPeriod:
                StartDate = Resource.Properties["InterfaceDowntimeStartDate"];
                EndDate = Resource.Properties["InterfaceDowntimeEndDate"];
                break;
        }
    }

    private static readonly Regex NetObjectRegex = new Regex(@"(\w):(\d+)", RegexOptions.Compiled);

    private string GetWherePart()
    {
        Match netObject; 
        string whereQuery;

        var nodeInstance = GetInterfaceInstance<INodeProvider>();
        var interfaceInstance = GetInterfaceInstance<IInterfaceProvider>();
        
        if (interfaceInstance != null)
            netObject = NetObjectRegex.Match(interfaceInstance.Interface.NetObjectID);
        else if (nodeInstance != null)
            netObject = NetObjectRegex.Match(nodeInstance.Node.NetObjectID);
        else 
            netObject = NetObjectRegex.Match(this.Page.Request.RawUrl);

        var netObjectType = netObject.Groups[1].Value;
        var netObjectId = netObject.Groups[2].Value;

        switch (netObjectType)
        {
            case "I":
                whereQuery = String.Format(" WHERE I.InterfaceID={0}", netObjectId);
                break;
            case "N":
                whereQuery = String.Format(" WHERE I.NodeID={0}", netObjectId);
                break;
            default:
                throw new NotSupportedException(String.Format("{0} - is not supported for InterfaceDowntimeMonitoring", netObject.Groups[1].Value));
        }
        return whereQuery;
    }

    public string SWQL
    {
        get
        {
            return String.Format(@"
              SELECT S.StatusName AS Status,
                    '/NetPerfMon/images/small-' + TOSTRING(I.StatusLed) AS [_IconFor_Status],
                     I.InterfaceID as [_InterfaceID],
                     I.Caption AS Name, 
                    '/Orion/Interfaces/InterfaceDetails.aspx?NetObject=I:'+TOSTRING(I.InterfaceID) AS [_LinkFor_Caption], 
                    '/NetPerfMon/images/Interfaces/' + TOSTRING(I.Icon) AS [_IconFor_Caption]  
                     FROM Orion.NPM.Interfaces I
                     LEFT JOIN Orion.StatusInfo S ON I.Status = S.StatusId{0}", GetWherePart());
        }
    }

    public string SearchSWQL
    {
        get
        {
            return SWQL + @" AND I.Caption LIKE '%${SEARCH_STRING}%'";
        }
    }
}