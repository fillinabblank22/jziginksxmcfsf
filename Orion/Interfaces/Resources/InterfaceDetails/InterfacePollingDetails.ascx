<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterfacePollingDetails.ascx.cs" Inherits="Orion_NetPerfMon_Resources_InterfaceDetails_InterfacePollingDetails" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div runat="server" id="interfaceError" visible="false" style="color:Red;font-weight:bold;" class="Text">
            <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_132%>
        </div>
        <table border="0" cellpadding="2" cellspacing="0" width="100%" runat="server" id="pollingDetailsTable">
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" valign="middle"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_133%></td>
	          <td class="Property"><asp:Label runat="server" ID="pollingEngine" />&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" valign="middle"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_134%></td>
	          <td class="Property"><asp:Label runat="server" ID="pollingInterval" />&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" valign="middle"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_135%></td>
	          <td class="Property"><asp:Label runat="server" ID="nextPoll" />&nbsp;</td>
	        </tr>
	        <tr>
	          <td colspan="3" class="Property" width="10">&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" valign="middle"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_136%></td>
	          <td class="Property"><asp:Label runat="server" ID="statCollection" />&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" valign="middle"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_137%></td>
	          <td class="Property"><asp:Label runat="server" ID="allow64bitCounters" />&nbsp;</td>
	        </tr>
	        <tr>
	          <td colspan="3" class="Property" width="10">&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" valign="middle"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_138%></td>
	          <td class="Property"><asp:Label runat="server" ID="rediscoveryInterval" />&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" valign="middle"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_139%></td>
	          <td class="Property"><asp:Label runat="server" ID="nextRediscovery" />&nbsp;</td>
	        </tr>
	        <tr>
	          <td colspan="3" class="Property" width="10">&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" valign="middle"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_140%></td>
	          <td class="Property"><asp:Label runat="server" ID="lastSync" />&nbsp;</td>
	        </tr>
	    </table>
    </Content>
</orion:resourceWrapper>    