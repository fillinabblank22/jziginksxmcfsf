<%@ Control Language="C#" AutoEventWireup="false" CodeFile="InterfaceDetailsResource.ascx.cs" Inherits="Orion_Interfaces_Resources_InterfaceDetails_InterfaceDetailsResource" %>
<%@ Register TagPrefix="orion" TagName="UnmanageDialog" Src="~/Orion/Controls/UnmanageDialog.ascx" %>
<%@ Register TagPrefix="orion" TagName="PollRediscoverDialog" Src="~/Orion/Controls/PollRediscoverDialog.ascx" %>
<%@ Register TagPrefix="npm" TagName="InterfaceHasObsoleteDataTooltip" Src="~/Orion/Interfaces/Controls/InterfaceHasObsoleteDataTooltip.ascx" %>
<%@ Register TagPrefix="orion" TagName="ManagementTasks" Src="~/Orion/NetPerfMon/Controls/ManagementTasksControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="AlertSuppressionStatusDescription" Src="~/Orion/Controls/AlertSuppressionStatusDescription.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <content>
        <orion:PollRediscoverDialog runat="server" />
        <orion:UnmanageDialog runat="server" />
        <orion:AlertSuppressionStatusDescription runat="server" />
        <orion:Include ID="Include" runat="server" File="Interfaces/Styles/InterfaceDetails.css" />
        
        <script type="text/javascript">
            $(function () {

                var processResults = function (result, planDescription) {
                    var source = $('#interfaceDetailsTemplate').html();
                    result.EscapedCaption = escape(result.Caption);
                    result.MaintenancePlanDescription = planDescription;
                    var newHtml = _.template(source, result);
                    $('#interfaceDetailsTableContent<%# Resource.ID %>').html(newHtml);
                };

                var refresh = function () {

                    var detailsPromise = $.Deferred();
                    var planPromise = SW.Core.MaintenanceMode.GetStatusDescription('<%=Interface.SwisUri %>', <%=IsDemo.ToString().ToLower() %>, <%=Profile.AllowUnmanage.ToString().ToLower() %>);

                    $.when(detailsPromise, planPromise).done(processResults);


                    SW.Core.Services.callWebService(
                        "/Orion/Interfaces/Services/AsyncResources.asmx",
                        "GetInterfaceDetails",
                        { 
                            interfaceId: <%= InterfaceID %>,
                            viewLimitationId: <%= ViewLimitationId %>
                        },                        
                        detailsPromise.resolve
                    );

                    
                };
                SW.Core.View.AddOnRefresh(refresh, '<%= resourceContent.ClientID %>');
                refresh();
            });
        </script>
        <npm:InterfaceHasObsoleteDataTooltip ID="interfaceObsoleteData" runat="server" Visible="false" />
        <table border="0" cellpadding="2" cellspacing="0" width="100%" id="detailsTable">
            <tbody>
                <%if ((Profile.AllowNodeManagement || Profile.AllowUnmanage) && (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer))
                    {%>
                <tr>
                    <td class="Property" width="10">&nbsp;</td>
                    <td class="PropertyHeader" valign="center"><%=(Resources.InterfacesWebContent.NPMWEBDATA_VB0_109)%></td>
                    <td class="Property NodeManagementIcons NodeManagementIconsInlined" colspan="3">
                        <orion:ManagementTasks ID="ManagementTasks" runat="server" />
                    </td>
                </tr>
                <%} %>
            </tbody>
            <tbody id="interfaceDetailsTableContent<%# Resource.ID %>">
            </tbody>
        </table>

        <script id="interfaceDetailsTemplate" type="text/x-template">
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" valign="middle"><%=(Resources.InterfacesWebContent.NPMWEBDATA_VB0_108)%></td>
	          <td class="Property" align="center"><span id="statusLED"><img class="StatusIcon" alt="{{ InterfaceStatusAltText }}" src="{{ _.escape(InterfaceStatusImagePath) }}" /></span></td>
	          <td class="Property" colspan="2">
                <span id="status">{{ InterfaceStatusDescription }}</span><br />
                <span id="maintenancePlan">{{ MaintenancePlanDescription }}</span>
              </td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%=(Resources.InterfacesWebContent.NPMWEBDATA_VB0_116)%></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property" colspan="2"><span id="interfaceName">{{ _.escape(Caption) }}</span>&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%=(Resources.InterfacesWebContent.NPMWEBDATA_VB0_117)%></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property" colspan="2"><span id="interfaceAlias">{{ _.escape(InterfaceAlias) }}</span>&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%=(Resources.InterfacesWebContent.NPMWEBDATA_VB0_118)%></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property" colspan="2"><span id="interfaceIndex">{{ InterfaceIndex }}</span>&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%=(Resources.InterfacesWebContent.NPMWEBDATA_VB0_119)%></td>
	          <td class="Property"  align="center"><span id="interfaceTypeIcon"><img src="{{ _.escape(InterfaceTypeIconPath) }}" alt="{{ _.escape(InterfaceTypeDescription) }}" /></span></td>
	          <td class="Property" colspan="2"><span id="interfaceTypeDescription">{{ _.escape(InterfaceTypeDescription) }}</span></td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%=(Resources.InterfacesWebContent.NPMWEBDATA_VB0_120)%></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property" colspan="2"><span id="macAddress">{{ MACAddress }}</span>&nbsp;</td>
	        </tr>
            <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%=(Resources.InterfacesWebContent.NPMWEBCODE_GKO_01)%></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property" colspan="2"><span id="interfaceIpAddress">{{ _.escape(IPAddress) }}</span>&nbsp;</td>
	        </tr>
	        <tr>
			    <td colspan="5">&nbsp;</td>
		    </tr>

            {# if(InterfaceSubType == 4) { #}

	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%=(Resources.InterfacesWebContent.F5InterfaceStatus)%></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property" colspan="2"><span id="F5InterfaceStatus">{{ F5InterfaceStatus }}</span>&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%=(Resources.InterfacesWebContent.F5InterfaceEnabled)%></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property" colspan="2"><span id="F5InterfaceEnabled">{{ F5InterfaceEnabled }}</span>&nbsp;</td>
	        </tr>

            {# } #}

	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%=(Resources.InterfacesWebContent.NPMWEBDATA_LF0_121)%></td>
	          <td class="Property" align="center"><span id="smallAdminStatusLED"><img class="StatusIcon" alt="{{ InterfaceAdminStatusAltText }}" src="{{ _.escape(InterfaceAdminStatusImagePath) }}" /></span></td>
	          <td class="Property" colspan="2"><span id="adminStatus">{{ AdministrativeStatus }}</span></td>
	        </tr>

	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%=(Resources.InterfacesWebContent.NPMWEBDATA_VB0_122)%></td>
	          <td class="Property" align="center"><span id="smallOperStatusLED"><img class="StatusIcon" alt="{{ InterfaceOperStatusAltText }}" src="{{ _.escape(InterfaceOperStatusImagePath) }}" /></span></td>
	          <td class="Property" colspan="2"><span id="operStatus">{{ OperationalStatus }}</span></td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%=(Resources.InterfacesWebContent.NPMWEBDATA_VB0_123)%></td>
	          <td class="Property" align="center">&nbsp;</td>
	          <td class="Property" colspan="2"><span id="interfaceLastChange">{{ LastStatusChange }}</span></td>
	        </tr>
	        <tr>
			    <td colspan="5">&nbsp;</td>
		    </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader">&nbsp;</td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property"><%=(Resources.InterfacesWebContent.NPMWEBDATA_VB0_69)%></td>
	          <td class="Property"><%=(Resources.InterfacesWebContent.NPMWEBDATA_VB0_65)%></td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%=(Resources.InterfacesWebContent.NPMWEBDATA_VB0_124)%></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property"><span id="inBandwidth">{{ InBandwidth }}</span>&nbsp;</td>
	          <td class="Property"><span id="outBandwidth">{{ OutBandwidth }}</span>&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><a href="/Orion/Charts/CustomChart.aspx?ChartName=AvgBps-Step&NetObject={{ NetObjectID }}" target="_blank"><%=(Resources.InterfacesWebContent.NPMWEBDATA_VB0_125)%></a></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property"><a href="/Orion/Charts/CustomChart.aspx?ChartName=AvgBps-Step&NetObject={{ NetObjectID }}" target="_blank"><span id="inBps">{{ InBps }}</span></a>&nbsp;</td>
	          <td class="Property"><a href="/Orion/Charts/CustomChart.aspx?ChartName=AvgBps-Step&NetObject={{ NetObjectID }}" target="_blank"><span id="outBps">{{ OutBps }}</span></a>&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><a href="/Orion/Charts/CustomChart.aspx?ChartName=AvgUtil-Step&NetObject={{ NetObjectID }}" target="_blank"><%=(Resources.InterfacesWebContent.NPMWEBDATA_VB0_126)%></a></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property"><a href="/Orion/Charts/CustomChart.aspx?ChartName=AvgUtil-Step&NetObject={{ NetObjectID }}" target="_blank"><span id="inPercentUtil">{{ InPercentUtil }}</span></a>&nbsp;</td>
	          <td class="Property"><a href="/Orion/Charts/CustomChart.aspx?ChartName=AvgUtil-Step&NetObject={{ NetObjectID }}" target="_blank"><span id="outPercentUtil">{{ OutPercentUtil }}</span></a>&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><a href="/Orion/Charts/CustomChart.aspx?ChartName=AvgPps&CalculateTrendLine=True&NetObject={{ NetObjectID }}" target="_blank"><%=(Resources.InterfacesWebContent.NPMWEBDATA_VB0_127)%></a></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property"><a href="/Orion/Charts/CustomChart.aspx?ChartName=AvgPps&CalculateTrendLine=True&NetObject={{ NetObjectID }}" target="_blank"><span id="inPps">{{ InPps }}</span></a>&nbsp;</td>
	          <td class="Property"><a href="/Orion/Charts/CustomChart.aspx?ChartName=AvgPps&CalculateTrendLine=True&NetObject={{ NetObjectID }}" target="_blank"><span id="outPps">{{ OutPps }}</span></a>&nbsp;</td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%=(Resources.InterfacesWebContent.NPMWEBDATA_VB0_128)%></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property"><span id="inPktSize">{{ InPktSize }}</span>&nbsp;</td>
	          <td class="Property"><span id="outPktSize">{{ OutPktSize }}</span>&nbsp;</td>
	        </tr>
	        <tr>
			    <td colspan="5">&nbsp;</td>
		    </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%=(Resources.InterfacesWebContent.NPMWEBDATA_VB0_129)%></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property" colspan="2"><span id="interfaceMTU">{{ InterfaceMTU }}</span></td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%=(Resources.InterfacesWebContent.NPMWEBDATA_VB0_130)%></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property" colspan="2"><span id="interfaceSpeed">{{ InterfaceSpeed }}</span></td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%=(Resources.InterfacesWebContent.NPMWEBDATA_VB0_131)%></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property" colspan="2"><span id="counter64">{{ Counter64 }}</span></td>
	        </tr>
            {# if (HasVlans) { #}
            <tr>
			    <td colspan="5">&nbsp;</td>
		    </tr>
            <tr>
                <td class="Property" width="10">&nbsp;</td>
			    <td class="PropertyHeader" colspan="2"><%=Resources.InterfacesWebContent.NPMWEBDATA_GK0_9 %></td>
                <td colspan="2"><span id="portType">{{ PortType }}</span></td>
		    </tr>
            <tr>
                <td class="Property" width="10">&nbsp;</td>
			    <td class="PropertyHeader" colspan="2"><%=Resources.InterfacesWebContent.NPMWEBDATA_GK0_10 %></td>
                <td colspan="2"><span id="vlans">{{ Vlans }}</span></td>
		    </tr>
            {# } #}
        </script>



        <span id="resourceContent" runat="server"></span>
    </content>
</orion:resourceWrapper>
