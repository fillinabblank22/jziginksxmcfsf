<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterfaceErrors.ascx.cs" Inherits="Orion_NetPerfMon_Resources_InterfaceDetails_InterfaceErrors" %>
<%@ Register TagPrefix="npm" TagName="InterfaceHasObsoleteDataTooltip" Src="~/Orion/Interfaces/Controls/InterfaceHasObsoleteDataTooltip.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div runat="server" id="interfaceError" visible="false" style="color:Red;font-weight:bold;" class="Text">
        <%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_72%>
        </div>
        
        <npm:InterfaceHasObsoleteDataTooltip ID="interfaceObsoleteData" runat="server" Visible ="false" />

        <table border="0" cellpadding="2" cellspacing="0" width="100%" runat="server" id="detailsTable" class="NeedsZebraStripes">
            <tr>
	          <td class="ReportHeader" style="width:50%;">&nbsp;</td>
	          <td class="ReportHeader"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_69%></td>
	          <td class="ReportHeader"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_65%></td>
	        </tr>
	        <tr>
	          <td><a href="/Orion/Charts/CustomChart.aspx?ChartName=InterfaceErrors&Period=LastHour&SampleSize=15&NetObject=<%=this.Interface.NetObjectID%>" target="_blank"><%=Resources.InterfacesWebContent.NPMWEBCODE_VB0_23%></a></td>
	          <td><a href="/Orion/Charts/CustomChart.aspx?ChartName=InterfaceErrors&Period=LastHour&SampleSize=15&NetObject=<%=this.Interface.NetObjectID%>" target="_blank"><asp:PlaceHolder runat="server" ID="inErrorsThisHour" /></a>&nbsp;</td>
	          <td><a href="/Orion/Charts/CustomChart.aspx?ChartName=InterfaceErrors&Period=LastHour&SampleSize=15&NetObject=<%=this.Interface.NetObjectID%>" target="_blank"><asp:PlaceHolder runat="server" ID="outErrorsThisHour" /></a>&nbsp;</td>
	        </tr>
	        <tr>
	          <td><a href="/Orion/Charts/CustomChart.aspx?ChartName=InterfaceErrors&Period=Today&SampleSize=60&NetObject=<%=this.Interface.NetObjectID%>" target="_blank"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_67%></a></td>
	          <td><a href="/Orion/Charts/CustomChart.aspx?ChartName=InterfaceErrors&Period=Today&SampleSize=60&NetObject=<%=this.Interface.NetObjectID%>" target="_blank"><asp:PlaceHolder runat="server" ID="inErrorsToday" /></a>&nbsp;</td>
	          <td><a href="/Orion/Charts/CustomChart.aspx?ChartName=InterfaceErrors&Period=Today&SampleSize=60&NetObject=<%=this.Interface.NetObjectID%>" target="_blank"><asp:PlaceHolder runat="server" ID="outErrorsToday" /></a>&nbsp;</td>
	        </tr>
	        <tr>
	          <td><a href="/Orion/Charts/CustomChart.aspx?ChartName=InterfaceDiscards&Period=LastHour&SampleSize=15&NetObject=<%=this.Interface.NetObjectID%>" target="_blank"><%=Resources.InterfacesWebContent.NPMWEBCODE_VB0_22%></a></td>
	          <td><a href="/Orion/Charts/CustomChart.aspx?ChartName=InterfaceDiscards&Period=LastHour&SampleSize=15&NetObject=<%=this.Interface.NetObjectID%>" target="_blank"><asp:PlaceHolder runat="server" ID="inDiscardsThisHour" /></a>&nbsp;</td>
	          <td><a href="/Orion/Charts/CustomChart.aspx?ChartName=InterfaceDiscards&Period=LastHour&SampleSize=15&NetObject=<%=this.Interface.NetObjectID%>" target="_blank"><asp:PlaceHolder runat="server" ID="outDiscardsThisHour" /></a>&nbsp;</td>
	        </tr>
	        <tr>
	          <td><a href="/Orion/Charts/CustomChart.aspx?ChartName=InterfaceDiscards&Period=Today&SampleSize=60&NetObject=<%=this.Interface.NetObjectID%>" target="_blank"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_68%></a></td>
	          <td><a href="/Orion/Charts/CustomChart.aspx?ChartName=InterfaceDiscards&Period=Today&SampleSize=60&NetObject=<%=this.Interface.NetObjectID%>" target="_blank"><asp:PlaceHolder runat="server" ID="inDiscardsToday" /></a>&nbsp;</td>
	          <td><a href="/Orion/Charts/CustomChart.aspx?ChartName=InterfaceDiscards&Period=Today&SampleSize=60&NetObject=<%=this.Interface.NetObjectID%>" target="_blank"><asp:PlaceHolder runat="server" ID="outDiscardsToday" /></a>&nbsp;</td>
	        </tr>
		</table>
	</Content>
</orion:resourceWrapper>
