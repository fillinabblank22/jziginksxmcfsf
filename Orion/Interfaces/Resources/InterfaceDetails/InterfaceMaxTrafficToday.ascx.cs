using System;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Common;

public partial class Orion_NetPerfMon_Resources_InterfaceDetails_InterfaceMaxTrafficToday : BaseResourceControl
{
	private Interface intf;

	protected Interface Interface 
	{ 
		get { return this.intf; }
		set { this.intf = value; }
	}

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		this.Interface = GetInterfaceInstance<IInterfaceProvider>().Interface;
		if (this.Interface == null)
		{
			this.Wrapper.Visible = false;
		}
		else
		{
			this.MaxIn.Text = String.Format(Resources.InterfacesWebContent.NPMWEBCODE_VB0_37,
				this.Interface.MaxInBpsToday, Utils.FormatToLocalTime(this.Interface.MaxInBpsTime));
			this.MaxOut.Text = String.Format(Resources.InterfacesWebContent.NPMWEBCODE_VB0_37,
				this.Interface.MaxOutBpsToday, Utils.FormatToLocalTime(this.Interface.MaxOutBpsTime));
		}
	}

	protected override string DefaultTitle
	{
		get { return Resources.InterfacesWebContent.NPMWEBCODE_VB0_38; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceInterfaceMaxTrafficToday"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(IInterfaceProvider) }; }
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
