using System;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using System.Web;
using SolarWinds.Interfaces.Web.DAL;
using SolarWinds.Orion.Common;

public partial class Orion_Interfaces_Resources_InterfaceDetails_InterfaceDetailsResource : ResourceControl
{
	protected Interface Interface { get; set; }
	protected int InterfaceID { get { return Interface.InterfaceID; } }

	protected int ViewLimitationId
	{
		get
		{
			if (HttpContext.Current.Items.Contains(typeof(ViewInfo).Name))
			{
				var viewInfo = HttpContext.Current.Items[typeof(ViewInfo).Name] as ViewInfo;
				return viewInfo != null ? viewInfo.LimitationID : 0;
			}
			return 0;
		}
	}

	protected override void OnLoad(EventArgs e)
	{
		base.OnLoad(e);

		OrionInclude.CoreFile("/js/jquery/timePicker.css");
		OrionInclude.CoreFile("/styles/UnmanageDialog.css");
		OrionInclude.CoreFile("/styles/NodeMNG.css");

		Interface = GetInterfaceInstance<IInterfaceProvider>().Interface;

	    if (Interface != null)
	    {
	        using (var swisProxy = InformationServiceProxy.CreateV3())
	        {
	            var dal = new InterfacesDAL(swisProxy);
	            interfaceObsoleteData.Visible = dal.HasInterfaceObsoleteData(Interface.InterfaceID);
	        }
	        
	    }

	    this.ManagementTasks.ResourceID = Resource.ID;
	    this.ManagementTasks.NetObject = Interface;

	}

	protected string ReturnUrl
	{
		get { return ReferrerRedirectorBase.GetReturnUrl(); }
	}

	protected override string DefaultTitle
	{
		get { return Resources.InterfacesWebContent.NPMWEBCODE_VB0_32; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceInterfaceDetails"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(IInterfaceProvider) }; }
	}

	public override ResourceLoadingMode ResourceLoadingMode
	{
		get { return ResourceLoadingMode.Ajax; }
	}

    public bool IsDemo
    {
        get { return OrionConfiguration.IsDemoServer; }
    }
}
