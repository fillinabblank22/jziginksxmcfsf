<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterfaceCustomProperties.ascx.cs" Inherits="Orion_NetPerfMon_Resources_InterfaceDetails_InterfaceCustomProperties" %>
<%@ Register TagPrefix="orion" TagName="CustomPropertyList" Src="~/Orion/NetPerfMon/Controls/CustomPropertyList.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper1">
    <Content>
        <orion:CustomPropertyList runat="server" ID="customPropertyList" CustomPropertyTableName="Interfaces" />
    </Content>
</orion:resourceWrapper>