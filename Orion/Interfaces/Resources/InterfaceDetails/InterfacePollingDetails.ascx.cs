using System;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Common;
using System.Data;

public partial class Orion_NetPerfMon_Resources_InterfaceDetails_InterfacePollingDetails : BaseResourceControl
{
	protected Interface Interface { get; set; }

	protected void Page_Load(object sender, EventArgs e)
	{
		this.Interface = GetInterfaceInstance<IInterfaceProvider>().Interface;
		if (this.Interface != null)
		{
			DataTable table;
			using (var proxy = new WebDAL())
			{
				table = proxy.GetInterfacePollingDetails(this.Interface.InterfaceID);
			}
			if(table != null && table.Rows.Count > 0)
			{
                this.pollingEngine.Text = String.Format("{0} ({1})", table.Rows[0]["ServerName"], table.Rows[0]["IP"]).Replace(":", ":<wbr>"); // <wbr> tag breaks word if necessary but do not split it
             
				this.pollingInterval.Text = String.Format(Resources.InterfacesWebContent.NPMWEBCODE_VB0_39, table.Rows[0]["PollInterval"]);

				try
				{
					DateTime poll = Convert.ToDateTime(table.Rows[0]["NextPoll"]).ToLocalTime();
					this.nextPoll.Text = (poll.Date == DateTime.Today) ? String.Empty :	(Utils.FormatMediumDate(poll) + " ");
					this.nextPoll.Text += Utils.FormatToLocalTime(poll);
				}
				catch 
				{
					this.nextPoll.Text = String.Empty;
				}

				this.statCollection.Text = String.Format(Resources.InterfacesWebContent.NPMWEBCODE_VB0_40, table.Rows[0]["StatCollection"]);

				this.allow64bitCounters.Text = Convert.ToBoolean(table.Rows[0]["Allow64bitCounters"]) ? Resources.InterfacesWebContent.NPMWEBCODE_VB0_30 : Resources.InterfacesWebContent.NPMWEBCODE_VB0_31;

				this.rediscoveryInterval.Text = String.Format(Resources.InterfacesWebContent.NPMWEBCODE_VB0_40, table.Rows[0]["RediscoveryInterval"]);
				
				try
				{
					DateTime rediscovery = Convert.ToDateTime(table.Rows[0]["NextRediscovery"]).ToLocalTime();
					this.nextRediscovery.Text = Utils.FormatDateTimeForDisplayingOnWebsite(rediscovery);
				}
				catch
				{
					this.nextRediscovery.Text = String.Empty;
				}

				try
				{
					DateTime lastSync = Convert.ToDateTime(table.Rows[0]["LastSync"]).ToLocalTime();
					this.lastSync.Text = Utils.FormatDateTimeForDisplayingOnWebsite(lastSync, true);
				}
				catch
				{
					this.lastSync.Text = String.Empty;
				}

				return;
			}
		}
		// in case of any error, hide the table with data
		this.interfaceError.Visible = true;
		this.pollingDetailsTable.Visible = false;
	}

	protected override string DefaultTitle
	{
		get { return Resources.InterfacesWebContent.NPMWEBCODE_VB0_41; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceInterfacePollingDetails"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(IInterfaceProvider) }; }
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
