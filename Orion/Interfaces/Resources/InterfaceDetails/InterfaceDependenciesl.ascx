﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterfaceDependenciesl.ascx.cs" Inherits="Orion_NPM_Resources_InterfaceDetails_InterfaceDependenciesl" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>

<asp:ScriptManagerProxy id="DepTreeScriptManager" runat="server">
	<Services>
		<asp:ServiceReference path="/Orion/Services/DependenciesTree.asmx" />
	</Services>
	<Scripts>
		<asp:ScriptReference Path="/Orion/NetPerfMon/js/Dependencies.js" />
	</Scripts>
</asp:ScriptManagerProxy>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div runat="server" id="errorMessage" style="color:Red;" visible="false"></div>
          <%if (!errorMessage.Visible)
            {%>
        <script type="text/javascript">
	        //<![CDATA[
            $(function () {
                ORIONDependencies.DependenciesTree.LoadDependencies($('#<%=this.DepTree.ClientID%>'), '<%=this.Resource.ID %>', '<%=this.NetObectId %>');
            });
	        //]]>
	    </script>
    <%} %>
        <div class="Tree" ID="DepTree" runat="server">
            <asp:Literal runat="server" ID="TreeLiteral" />
        </div>
    </Content>
</orion:resourceWrapper>