﻿using System;
using System.Web.UI;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Interfaces.Web.DAL;
using SettingsDAL = SolarWinds.Orion.Web.DAL.SettingsDAL;

public partial class Orion_NetPerfMon_Resources_InterfaceDetails_InterfaceErrors : BaseResourceControl
{
    protected Interface Interface { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Interface = GetInterfaceInstance<IInterfaceProvider>().Interface;
        if (this.Interface != null)
        {
            double errorThreshold = SettingsDAL.GetSetting("NetPerfMon-ErrorsDiscards-Error").SettingValue;
            double warningThreshold = SettingsDAL.GetSetting("NetPerfMon-ErrorsDiscards-Warning").SettingValue;

            this.inDiscardsThisHour.Controls.Add(new LiteralControl(
                FormatHelper.GetErrorsDiscardsFormat(this.Interface.InDiscardsThisHour, errorThreshold, warningThreshold, Resources.InterfacesWebContent.NPMWEBCODE_VB0_34)));
            this.inDiscardsToday.Controls.Add(new LiteralControl(
                FormatHelper.GetErrorsDiscardsFormat(this.Interface.InDiscardsToday, errorThreshold, warningThreshold, Resources.InterfacesWebContent.NPMWEBCODE_VB0_34)));
            this.outDiscardsThisHour.Controls.Add(new LiteralControl(
                FormatHelper.GetErrorsDiscardsFormat(this.Interface.OutDiscardsThisHour, errorThreshold, warningThreshold, Resources.InterfacesWebContent.NPMWEBCODE_VB0_34)));
            this.outDiscardsToday.Controls.Add(new LiteralControl(
                FormatHelper.GetErrorsDiscardsFormat(this.Interface.OutDiscardsToday, errorThreshold, warningThreshold, Resources.InterfacesWebContent.NPMWEBCODE_VB0_34)));
            this.inErrorsThisHour.Controls.Add(new LiteralControl(
                FormatHelper.GetErrorsDiscardsFormat(this.Interface.InErrorsThisHour, errorThreshold, warningThreshold, Resources.InterfacesWebContent.NPMWEBCODE_VB0_35)));
            this.inErrorsToday.Controls.Add(new LiteralControl(
                FormatHelper.GetErrorsDiscardsFormat(this.Interface.InErrorsToday, errorThreshold, warningThreshold, Resources.InterfacesWebContent.NPMWEBCODE_VB0_35)));
            this.outErrorsThisHour.Controls.Add(new LiteralControl(
                FormatHelper.GetErrorsDiscardsFormat(this.Interface.OutErrorsThisHour, errorThreshold, warningThreshold, Resources.InterfacesWebContent.NPMWEBCODE_VB0_35)));
            this.outErrorsToday.Controls.Add(new LiteralControl(
                FormatHelper.GetErrorsDiscardsFormat(this.Interface.OutErrorsToday, errorThreshold, warningThreshold, Resources.InterfacesWebContent.NPMWEBCODE_VB0_35)));

            using (var swisProxy = InformationServiceProxy.CreateV3())
            {
                var dal = new InterfacesDAL(swisProxy);
                interfaceObsoleteData.Visible = dal.HasInterfaceObsoleteData(Interface.InterfaceID);
            }            
        }
        else
        {
            this.interfaceError.Visible = true;
            this.detailsTable.Visible = false;
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.InterfacesWebContent.NPMWEBCODE_VB0_36; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceInterfaceErrorsDiscards"; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IInterfaceProvider) }; }
    }

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
