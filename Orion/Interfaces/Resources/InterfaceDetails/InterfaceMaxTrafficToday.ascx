<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterfaceMaxTrafficToday.ascx.cs" Inherits="Orion_NetPerfMon_Resources_InterfaceDetails_InterfaceMaxTrafficToday" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <table border="0" cellpadding="2" cellspacing="0" width="100%">
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><a href="/Orion/Charts/CustomChart.aspx?ChartName=MMAvgBps&CalculateTrendLine=true&Period=Today&NetObject=<%= this.Interface.NetObjectID %>" target="_blank"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_69%></a></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property"><a href="/Orion/Charts/CustomChart.aspx?ChartName=MMAvgBps&CalculateTrendLine=true&Period=Today&NetObject=<%= this.Interface.NetObjectID %>" target="_blank">
	            <asp:Label runat="server" ID="MaxIn" />
	            </a></td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><a href="/Orion/Charts/CustomChart.aspx?ChartName=MMAvgBps&CalculateTrendLine=true&Period=Today&NetObject=<%= this.Interface.NetObjectID %>" target="_blank"><%=Resources.InterfacesWebContent.NPMWEBDATA_VB0_65%></a></td>
	          <td class="Property">&nbsp;</td>
	          <td class="Property"><a href="/Orion/Charts/CustomChart.aspx?ChartName=MMAvgBps&CalculateTrendLine=true&Period=Today&NetObject=<%= this.Interface.NetObjectID %>" target="_blank">
	            <asp:Label runat="server" ID="MaxOut" />
	            </a></td>
	        </tr>
	    </table>
    </Content>
</orion:resourceWrapper>