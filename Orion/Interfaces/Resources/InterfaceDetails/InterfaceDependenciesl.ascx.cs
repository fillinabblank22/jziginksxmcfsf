﻿using System;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.Dependencies;
using System.ServiceModel;
using System.Web.UI;

public partial class Orion_NPM_Resources_InterfaceDetails_InterfaceDependenciesl : BaseResourceControl
{
    public string NetObectId
    {
        get
        {
            Interface _interface = GetInterfaceInstance<IInterfaceProvider>().Interface;
            return _interface != null ? _interface.NetObjectID : Request["netobject"] ?? "";
        }
    }

	protected bool AutoHide
	{
		get
		{
			if (string.IsNullOrEmpty(Resource.Properties["AutoHide"]))
				return false;
			return Resource.Properties["AutoHide"].Equals("1");
		}
	}

	protected string Filter
	{
		get
		{
			if (string.IsNullOrEmpty(Resource.Properties["Filter"]))
				return string.Empty;
			return Resource.Properties["Filter"];
		}
	}

    protected override void OnInit(EventArgs e)
    {
        Wrapper.ManageButtonText = Resources.CoreWebContent.ResourcesAll_ManageDependencies;
        Wrapper.ManageButtonTarget = "~/Orion/Admin/DependenciesView.aspx";
        Wrapper.ShowManageButton = Profile.AllowAdmin || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;

		string parentFilter = string.IsNullOrEmpty(Resource.Properties["ParentFilter"]) ? string.Empty : Resource.Properties["ParentFilter"];
		string childFilter = string.IsNullOrEmpty(Resource.Properties["ChildFilter"]) ? string.Empty : Resource.Properties["ChildFilter"];
		string dependencyFilter = string.IsNullOrEmpty(Resource.Properties["DependencyFilter"]) ? string.Empty : Resource.Properties["DependencyFilter"];

		try
		{
			DependencyCache.UpdateDependencyCache(NetObectId, parentFilter, childFilter, dependencyFilter);
		}
		catch (FaultException ex) 
		{
			errorMessage.Controls.Clear();
			if (!string.IsNullOrEmpty(parentFilter) || !string.IsNullOrEmpty(childFilter) || !string.IsNullOrEmpty(dependencyFilter))
				errorMessage.Controls.Add(new LiteralControl(string.Format(Resources.InterfacesWebContent.NPMWEBCODE_VB0_27, ex.Message, string.Format("{0} {1} {2}", parentFilter, childFilter, dependencyFilter))));
			else
                errorMessage.Controls.Add(new LiteralControl(ex.Message));
			errorMessage.Visible = true;
		}
		catch (Exception ex)
		{
			errorMessage.Controls.Clear();
            errorMessage.Controls.Add(new LiteralControl(ex.Message));
			errorMessage.Visible = true;
		}

        base.OnInit(e);
    }

	protected override void OnPreRender(EventArgs e)
	{
		if (string.IsNullOrEmpty(Filter) && AutoHide)
		{
			bool allowInterfaces = new RemoteFeatureManager().AllowInterfaces;
			using (var dal = new DependenciesDAL())
				if (dal.GetDependenciesCount(GetInterfaceInstance<IInterfaceProvider>().Interface.InterfaceID, "Orion.NPM.Interfaces", allowInterfaces, Filter) == 0)
					Wrapper.Visible = false;
		}

		base.OnPreRender(e);
	}

    protected override string DefaultTitle
    {
        get { return Resources.InterfacesWebContent.NPMWEBCODE_VB0_28; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceInterfaceDependencies"; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IInterfaceProvider) }; }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/NetPerfMon/Controls/EditResourceControls/EditDependencies.ascx";
        }
    }

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }
}