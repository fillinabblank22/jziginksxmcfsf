using System;
using System.Data;

using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Dependencies;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Containers;
using System.ServiceModel;
using System.Web.UI;

public partial class Orion_NPM_Resources_InterfaceDetails_InterfaceDependenciesTable : BaseResourceControl
{
	private const string object_type = "Orion.NPM.Interfaces";

	private string _parentFilter;
	private string _childFilter;
	private string _dependencyFilter;

	public string NetObectId
	{
		get
		{
			IInterfaceProvider interfaceProvider = GetInterfaceInstance<IInterfaceProvider>();
			if (interfaceProvider != null)
			{
				Interface _interface = interfaceProvider.Interface;
				return _interface != null ? _interface.NetObjectID : Request["netobject"] ?? "";
			}
			return string.Empty;
		}
	}

	protected bool AutoHide
	{
		get
		{
			if (string.IsNullOrEmpty(Resource.Properties["AutoHide"]))
				return false;
			return Resource.Properties["AutoHide"].Equals("1");
		}
	}

	protected string Filter
	{
		get
		{
			if (string.IsNullOrEmpty(Resource.Properties["Filter"]))
				return string.Empty;
			return Resource.Properties["Filter"];
		}
	}

	protected override string DefaultTitle
	{
		get { return Resources.InterfacesWebContent.NPMWEBCODE_VB0_29; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionPHResourceNodeDependencies"; }
	}

	public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(IInterfaceProvider) }; }
	}

	public override string EditControlLocation
	{
		get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditDependencies.ascx"; }
	}

	protected override void OnInit(EventArgs e)
	{
        Wrapper.ManageButtonText = Resources.CoreWebContent.ResourcesAll_ManageDependencies;
		Wrapper.ManageButtonTarget = "/Orion/Admin/DependenciesView.aspx";
		Wrapper.ShowManageButton = Profile.AllowAdmin || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;

		_parentFilter = string.IsNullOrEmpty(Resource.Properties["ParentFilter"]) ? string.Empty : Resource.Properties["ParentFilter"];
		_childFilter = string.IsNullOrEmpty(Resource.Properties["ChildFilter"]) ? string.Empty : Resource.Properties["ChildFilter"];
		_dependencyFilter = string.IsNullOrEmpty(Resource.Properties["DependencyFilter"]) ? string.Empty : Resource.Properties["DependencyFilter"];
		try
		{
			DependencyCache.UpdateDependencyCache(NetObectId, _parentFilter, _childFilter, _dependencyFilter);
		}
		catch (FaultException ex)
		{
			errorMessage.Controls.Clear();
			if (!string.IsNullOrEmpty(_parentFilter) || !string.IsNullOrEmpty(_childFilter) || !string.IsNullOrEmpty(_dependencyFilter))
				errorMessage.Controls.Add(new LiteralControl(string.Format(Resources.InterfacesWebContent.NPMWEBCODE_VB0_27, ex.Message, string.Format("{0} {1} {2}", _parentFilter, _childFilter, _dependencyFilter))));
			else
				errorMessage.Controls.Add(new LiteralControl(ex.Message));
			errorMessage.Visible = true;
		}
		catch (Exception ex)
		{
			errorMessage.Controls.Clear();
			errorMessage.Controls.Add(new LiteralControl(ex.Message));
			errorMessage.Visible = true;
		}
		base.OnInit(e);
	}

	protected override void OnPreRender(EventArgs e)
	{
		int objectId = 0;

		bool allowInterfaces = new RemoteFeatureManager().AllowInterfaces;

		if (int.TryParse(NetObjectHelper.GetObjectID(NetObectId), out objectId))
		{
			// hide resource if there aren't any data
			if (string.IsNullOrEmpty(Filter) && AutoHide)
			{
				using (var dal = new DependenciesDAL())
					if (dal.GetDependenciesCount(objectId, object_type, allowInterfaces, Filter) == 0)
						Wrapper.Visible = false;
			}

			DataTable sourceDataTable = null;
			DataTable groupDataTable = null;
			try
			{
				using (DependenciesDAL dal = new DependenciesDAL())
				{
					sourceDataTable = dal.GetAllDependenciesWithStatus(objectId, object_type, allowInterfaces, _parentFilter, _childFilter, _dependencyFilter);
					groupDataTable = dal.GetFilteredGroupDependencies(_parentFilter, _childFilter, _dependencyFilter);
				}

				if (sourceDataTable != null && groupDataTable != null)
				{
					foreach (DataRow row in groupDataTable.Rows)
					{
						int depId = int.Parse(row["DependencyId"].ToString());
						if (DependencyCache.GroupDependenciesContains(NetObectId, depId, row["ParentUri"].ToString(), row["ChildUri"].ToString()))
							sourceDataTable.ImportRow(row);
					}
					sourceDataTable.DefaultView.Sort = "Name Asc";

					DependencyTable.DataSource = sourceDataTable;
					DependencyTable.DataBind();
				}
			}
			catch (FaultException ex)
			{
				errorMessage.Controls.Clear();
				if (!string.IsNullOrEmpty(_parentFilter) || !string.IsNullOrEmpty(_childFilter) || !string.IsNullOrEmpty(_dependencyFilter))
					errorMessage.Controls.Add(new LiteralControl(string.Format(Resources.InterfacesWebContent.NPMWEBCODE_VB0_27, ex.Message, string.Format("{0} {1} {2}", _parentFilter, _childFilter, _dependencyFilter))));
				else
					errorMessage.Controls.Add(new LiteralControl(ex.Message));
				errorMessage.Visible = true;
			}
			catch (Exception ex)
			{
				errorMessage.Controls.Clear();
				errorMessage.Controls.Add(new LiteralControl(ex.Message));
				errorMessage.Visible = true;
			}
		}

		base.OnPreRender(e);
	}

	public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }
}
