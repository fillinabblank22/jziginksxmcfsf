﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.InformationService;

public partial class Orion_WirelessHeatmaps_Controls_WirelessHeatMapEditNodeProperties : UserControl, INodePropertyPlugin
{
    protected class SimplePropertyControl
    {
        public HtmlContainerControl ContainerControl { get; set; }
        public string SettingId { get; set; }
        public CheckBox ApplyToAllNodesControl { get; set; }
        public CheckBox OverrideValueControl { get; set; }
        public TextBox InputControl { get; set; }
        public RangeValidator RangeValidator { get; set; }
        public List<int> SupportedNodeIds { get; set; }
        public bool IsSupported { get { return SupportedNodeIds.Any(); } }

        public SimplePropertyControl()
        {
            SupportedNodeIds = new List<int>();
        }
    }

    private IList<Node> editedNodes;
    private IList<SimplePropertyControl> simplePropertyControls;
    protected IList<SimplePropertyControl> SimplePropertyControls
    {
        get { return simplePropertyControls ?? (simplePropertyControls = GetSimplePropertyControlsList(editedNodes)); }
    }

    public bool IsMultiselected
    {
        get { return editedNodes != null && editedNodes.Count > 1; }
    }

    protected bool IsSupported
    {
        get { return SimplePropertyControls.Any(p => p.IsSupported); }
    }
    
    protected IList<SimplePropertyControl> GetSimplePropertyControlsList(IList<Node> editedNodes)
    {
        return new List<SimplePropertyControl> {
            new SimplePropertyControl
            {
                SettingId = "NPM_Settings_WLHM_ClientLocation_PollInterval",
                ContainerControl = ClientLocation,
                ApplyToAllNodesControl = ApplyClientLocationPollInterval,
                OverrideValueControl = OverrideClientLocationPollInterval,
                InputControl = ClientLocationPollInterval,
                RangeValidator = RangeValidatorClientLocationPollInterval,
                SupportedNodeIds = GetNodesByPollerType(editedNodes, "N.WLHM.")
            }
        };
    }

    protected virtual void Page_Load(object sender, EventArgs e)
    {
        WirelessHeatmapsSection.Visible = IsSupported;

        if (!WirelessHeatmapsSection.Visible || IsPostBack)
        {
            return;
        }

        var propertyRanges = GetRangesForProperties(SimplePropertyControls);
        foreach (var simplePropertyControl in SimplePropertyControls)
        {
            if (!simplePropertyControl.IsSupported)
            {
                simplePropertyControl.ContainerControl.Visible = false;
                continue;
            }

            var firstNodeId = simplePropertyControl.SupportedNodeIds.First();
            var overridenValue = NodeSettingsDAL.GetLastNodeSettings(firstNodeId, simplePropertyControl.SettingId);

            simplePropertyControl.ApplyToAllNodesControl.Visible = IsMultiselected;
            simplePropertyControl.OverrideValueControl.Enabled = !IsMultiselected;
            simplePropertyControl.OverrideValueControl.Checked = !IsMultiselected && !string.IsNullOrEmpty(overridenValue);
            simplePropertyControl.InputControl.Parent.Visible = simplePropertyControl.OverrideValueControl.Checked;
            simplePropertyControl.InputControl.Enabled = simplePropertyControl.OverrideValueControl.Checked;
            if (IsMultiselected || string.IsNullOrEmpty(overridenValue))
            {
                simplePropertyControl.InputControl.Text = SettingsDAL.GetCurrentInt(simplePropertyControl.SettingId, 0).ToString();
            }
            else
            {
                simplePropertyControl.InputControl.Text = overridenValue;
            }

            if (simplePropertyControl.RangeValidator != null)
            {
                var min = propertyRanges[simplePropertyControl.SettingId].Item1;
                var max = propertyRanges[simplePropertyControl.SettingId].Item2;
                SetupRangeValidator(simplePropertyControl.RangeValidator, min, max);
            }
        }
    }

    public virtual void Initialize(IList<Node> nodes, NodePropertyPluginExecutionMode mode, Dictionary<string, object> pluginState)
    {
        editedNodes = nodes;
    }

    public virtual bool Update()
    {
        if (!IsSupported)
        {
            return true;
        }

        foreach (var simplePropertyControl in SimplePropertyControls.Where(p => p.SettingId != null))
        {
            if (!IsMultiselected || simplePropertyControl.ApplyToAllNodesControl.Checked)
            {
                if (simplePropertyControl.OverrideValueControl.Checked)
                {
                    NodeSettingsDAL.UpdateSpecificSettingForAllNodes(
                        simplePropertyControl.SettingId,
                        simplePropertyControl.InputControl.Text,
                        string.Format("NodeId IN ({0})", string.Join(",", simplePropertyControl.SupportedNodeIds))
                        );
                }
                else
                {
                    foreach (var nodeId in simplePropertyControl.SupportedNodeIds)
                    {
                        NodeSettingsDAL.DeleteSpecificSettingForNode(nodeId, simplePropertyControl.SettingId);
                    }
                }
            }
        }

        return true;
    }

    public virtual bool Validate()
    {
        return true;
    }

    protected virtual void ApplyProperty_CheckedChanged(object sender, EventArgs e)
    {
        if (!IsMultiselected)
            return;

        ICheckBoxControl applyPropertyCheckBox = sender as ICheckBoxControl;
        if (applyPropertyCheckBox == null)
            return;

        var property = SimplePropertyControls.FirstOrDefault(c => c.ApplyToAllNodesControl.Equals(applyPropertyCheckBox));
        if (property != null)
        {
            property.OverrideValueControl.Enabled = applyPropertyCheckBox.Checked;
            property.InputControl.Enabled = applyPropertyCheckBox.Checked && property.OverrideValueControl.Checked;
        }
    }

    protected virtual void OverrideProperty_CheckedChanged(object sender, EventArgs e)
    {
        ICheckBoxControl overridePropertyCheckBox = sender as ICheckBoxControl;
        if (overridePropertyCheckBox == null)
            return;

        var property = SimplePropertyControls.FirstOrDefault(c => c.OverrideValueControl.Equals(overridePropertyCheckBox));
        if (property != null)
        {
            property.InputControl.Enabled = overridePropertyCheckBox.Checked;
            property.InputControl.Parent.Visible = overridePropertyCheckBox.Checked;
        }
    }

    protected void SetupRangeValidator(RangeValidator validator, int min, int max)
    {
        validator.MinimumValue = min.ToString();
        validator.MaximumValue = max.ToString();
        validator.ErrorMessage = string.Format("({0}-{1})", min, max);
    }

    private IDictionary<string, Tuple<int, int>> GetRangesForProperties(IList<SimplePropertyControl> propertyControls)
    {
        var result = new Dictionary<string, Tuple<int, int>>();
        var settingIds = propertyControls
            .Where(p => p.RangeValidator != null && p.SettingId != null)
            .Select(p => p.SettingId)
            .ToList();

        if (!settingIds.Any())
        {
            return result;
        }

        using (var proxy = InformationServiceProxy.CreateV3())
        {
            var query = string.Format("SELECT SettingID, Minimum, Maximum FROM Orion.Settings WHERE SettingID IN ('{0}')", string.Join("','", settingIds));
            var queryResult = proxy.Query(query);

            if (queryResult != null && queryResult.Rows.Count > 0)
            {
                result = queryResult.Rows
                    .Cast<DataRow>()
                    .ToDictionary(
                        k => k[0].ToString(),
                        k =>
                        {
                            var min = Convert.ToInt32(k[1]);
                            var max = Convert.ToInt32(k[2]);
                            return new Tuple<int, int>(min, max);
                        }
                    );
            }

            return result;
        }
    }

    protected List<int> GetNodesByPollerType(IList<Node> nodes, string pollerType)
    {
        string nodeIdList = string.Join(",", nodes.Select(n => n.Id.ToString(CultureInfo.InvariantCulture)));
        using (var proxy = InformationServiceProxy.CreateV3())
        {
            const string query = @"
                    SELECT
                        NetObjectID
                    FROM 
                        Orion.Pollers
                    WHERE
                        NetObjectType = 'N' AND
                        PollerType LIKE '{0}%' AND
                        NetObjectID IN ({1})";

            var queryResult = proxy.Query(string.Format(query, pollerType, nodeIdList));

            if (queryResult != null && queryResult.Rows.Count > 0)
            {
                return queryResult.Rows
                    .Cast<DataRow>()
                    .Select(r => Convert.ToInt32(r[0]))
                    .ToList();
            }
        }

        return new List<int>();
    }
}