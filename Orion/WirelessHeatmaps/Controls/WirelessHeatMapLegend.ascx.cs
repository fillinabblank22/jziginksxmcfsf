﻿using System;
using System.Collections.ObjectModel;
using System.Web.UI;
using SolarWinds.MapEngine.Enums;
using SolarWinds.Wireless.Heatmaps.Common.Models;

namespace Orion.WirelessHeatmaps.Controls
{
    // WLHM needs different visual style for it's legend +
    // there are few more specific thing => new control.
    public partial class WirelessHeatMapLegend : UserControl
    {

        // This will be set by the WirelessHeatMap control ->
        // it depends on resource settings controlled by user.
        public bool IsVisible { get; set; }

        // I don't know who needs these additional metadata,
        // but we are adding them in the original Legend control in Core
        // so I am keeping it here too.
        public void SetMapStyleAttributes(string resourceId)
        {
            int validResourceId = 0;
            int.TryParse(resourceId, out validResourceId);

            const MapStyle mapStyle = MapStyle.WirelessHeatMap;
            string attribute = string.Format("data-map-style-{0}", validResourceId);
            wlhmLegendWrapper.Attributes.Add(attribute, mapStyle.ToString());
            wlhmLegendWrapper.ID = string.Format("{0}-{1}-{2}", wlhmLegendWrapper.ID, validResourceId, mapStyle);
        }

        // OnPreRender is used because of connection to X different 
        // rendering ways and relations to other components.
        // Either load and show data, or don't if !IsVisible
        protected override void OnPreRender(EventArgs e)
        {
            if (!IsVisible)
            {
                wlhmLegendWrapper.Visible = false;
                return;
            }

            legendTitle.Text = Resources.WirelessHeatmapsWebContent.WEBDATA_BI_07;

            ReadOnlyCollection<WLHMSignalStrengthThreshold> legendData = WLHMSignalStrengthThresholds.GetAll();
            
            mapLegendItems.DataSource = legendData;
            mapLegendItems.DataBind();
        }
    }
}