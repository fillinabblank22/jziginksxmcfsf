﻿using System.Data;
using System.Globalization;
using Newtonsoft.Json;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Wireless.Heatmaps.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using SolarWinds.Orion.Web.UI;

public partial class Orion_WirelessHeatmaps_Controls_ClientsFilter : CustomResourceContentBase
{
    private enum FType
    {
        None,
        Count,
        Ap,
        List
    };

    private static readonly Log Log = new Log();

    public string MapGUID
    {
        get
        {
            return GetGuidOfSelectedMap();
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        wlhmClientsFilter.Attributes.Add("data-rid", Resource.ID.ToString(CultureInfo.InvariantCulture));
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // Since there are multiple postback sources, I need
        // to identify the one caused by submit button so
        // I can store the filter values. If I would do that
        // during every postback, settings would be overriden.
        var whoDidThatPostback = Request.Params.Get("__EVENTTARGET");
        if (!string.IsNullOrEmpty(whoDidThatPostback) &&
            whoDidThatPostback.IndexOf("btnSubmit", StringComparison.OrdinalIgnoreCase) != -1)
        {
            try
            {
                var filterEnabledField = (HiddenField)FindControl("wlhmcfFilterEnabled");
                bool isWlhmSelected = filterEnabledField.Value == "1";
                if (!isWlhmSelected)
                    return;

                var filterValueField = (HiddenField)FindControl("wlhmcfFilterValue");
                string settingsJson = filterValueField.Value;
                if (string.IsNullOrEmpty(settingsJson))
                {
                    Log.Error("The filter value field is empty. Cannot update the filter settings.");
                    return;
                }

                var settings = JsonConvert.DeserializeObject<WirelessClientLimitationSetting>(settingsJson);
                settings.MapGUID = MapGUID;

                // in case of listFilter, I have to translated the SWIS URI
                // (from NetObjectPicker) to MAC Address
                if (settings.ClientLimitations != null && settings.ClientLimitations.Count > 0)
                {
                    var macList = ConvertSwisUrisToMacAddresses(settings.ClientLimitations);
                    settings.ClientLimitations = macList;
                }

                StoreClientLimitationSettings(settings);
            }
            catch (Exception ex)
            {
                // yaya, I know.. pokemon
                // I might specify all the possible exceptions later..
                Log.ErrorFormat("Failed to store clients filter settings. Ex: {0}", ex.Message);
            }
        }
    }

    // Using OnPreRender because I need to wait for processing of the 
    // list control events in EditNetworkMap (happens after page Load
    // and the next closest event (avilable from User Control) is this.
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // I need to find out GUIDs of WLHM and store
        // them in DOM. I will use this to determine
        // if postback is needed or not when map
        // selection changes. (Postback should happen
        // only for WLHM).
        var jss = new JavaScriptSerializer();
        var guidsJson = jss.Serialize(
            new
            {
                wlhm = LoadWlhmGuids()
            }
        );
        var wlhmGuidsField = (HiddenField)FindControl("wlhmcfMapGuids");
        wlhmGuidsField.Value = guidsJson;

        // Read GUID of selected map from properties
        // Has to happen on each postback, because the Properties collection
        // is being updated. SelectedMapId is filled only when WLHM is selected
        if (string.IsNullOrEmpty(MapGUID))
            return;

        LoadAccessPointFilterValues();
        InitFilters();
        ScriptManager.RegisterOnSubmitStatement(this, GetType(), "parseClientsFilter", "SW.Wireless.HeatMaps.clientsFilter.parseSettings();");
    }

    public override void Hide()
    {
        wlhmClientsFilter.Style.Add("display", "none");
        wlhmClientsFilterCollapse.Style.Add("display", "none");
        wlhmcfFilterEnabled.Value = "0";
    }

    public override void Show()
    {
        wlhmClientsFilter.Style.Add("display", "block");
        wlhmClientsFilterCollapse.Style.Add("display", "block");
        wlhmcfFilterEnabled.Value = "1";
    }

    /// <summary>
    /// Upon loading, I need to check if there's already an
    /// active filter set. In such case, I need to load
    /// and show the stored values. Otherwise I'll just
    /// preselect an empty count filter as default.
    /// </summary>
    private void InitFilters()
    {
        var currentSettings = GetClientLimitationSettings(Resource.ID, MapGUID);
        var activeFilter = DetermineFilterType(currentSettings);

        InitCountFilter(currentSettings.MapLimitationCount);
        InitApFilter(currentSettings.APMACAddress, currentSettings.APLimitationCount);
        InitListFilter(currentSettings.ClientLimitations);

        MakeFilterSelected(activeFilter);

        if (activeFilter != FType.None)
            wlhmClientsFilterCollapse.CssClass = wlhmClientsFilterCollapse.CssClass.Replace("sw-wlhm-cf-collapsed", string.Empty);
    }

    private void InitCountFilter(int? count)
    {
        if (count.HasValue)
        {
            countFilterInputEnabler.Checked = true;
            countFilterInput.Text = count.ToString();
            countFilterInput.Enabled = true;
        }
        else
        {
            countFilterInputEnabler.Checked = false;
            countFilterInput.Text = string.Empty;
            countFilterInput.Enabled = false;
        }
    }

    private void InitApFilter(string mac, int? count)
    {
        if (!string.IsNullOrEmpty(mac))
        {
            var macItemIndex = apFilterDropDown.Items.IndexOf(apFilterDropDown.Items.FindByValue(mac));
            apFilterDropDown.SelectedIndex = macItemIndex;
        }
        if (count.HasValue)
        {
            apFilterInputEnabler.Checked = true;
            apFilterInput.Text = count.ToString();
            apFilterInput.Enabled = true;
        }
        else
        {
            apFilterInputEnabler.Checked = false;
            apFilterInput.Text = string.Empty;
            apFilterInput.Enabled = false;
        }
    }

    private void InitListFilter(List<string> clients)
    {
        if (clients.Count > 0)
        {
            var jsonString = ConvertMacAddressesToSwisUriJSONString(clients);
            wlhmcfListFilterCurrentValues.Value = jsonString;
        }
        else
        {
            wlhmcfListFilterCurrentValues.Value = string.Empty;
        }
    }

    private void LoadAccessPointFilterValues()
    {

        if (string.IsNullOrEmpty(MapGUID))
        {
            Log.Debug("Skipping AP loading because WLHM isn't selected.");
            return;
        }

        const string queryForAps = @"
SELECT DISTINCT ap.WirelessInterfaces.MAC, ap.Name
FROM Orion.WirelessHeatMap.MapPoint mp
INNER JOIN Orion.Packages.Wireless.AccessPoints ap
ON mp.InstanceId=ap.ID
INNER JOIN Orion.WirelessHeatMap.Map m
ON mp.MapID = m.MapID
WHERE mp.EntityType='Orion.WirelessHeatMap.AccessPoints'
AND m.MapStudioFileID = @mapGuid
ORDER BY ap.Name
";
        DataTable accessPointsOnMap;
        using (var swis = InformationServiceProxy.CreateV3())
        {
            accessPointsOnMap = swis.Query(queryForAps, new Dictionary<string, object> { { "mapGuid", MapGUID } });
            if (accessPointsOnMap == null || accessPointsOnMap.Rows.Count == 0)
            {
                Log.DebugFormat("No Access Points have been loaded for WLHM id: {0}", MapGUID);
                apFilterDropDown.DataTextField = "Text";
                apFilterDropDown.DataValueField = "Value";
                apFilterDropDown.DataSource = new List<ListItem> { new ListItem("N/A", string.Empty) };
                apFilterDropDown.DataBind();
                return;
            }
        }

        apFilterDropDown.DataTextField = "Name";
        apFilterDropDown.DataValueField = "MAC";
        apFilterDropDown.DataSource = accessPointsOnMap;
        apFilterDropDown.DataBind();
    }

    private FType DetermineFilterType(WirelessClientLimitationSetting settings)
    {
        if (settings == null)
            return FType.None;
        if (settings.MapLimitationCount > 0)
            return FType.Count;
        if (!string.IsNullOrEmpty(settings.APMACAddress))
            return FType.Ap;
        if (settings.ClientLimitations.Count > 0)
            return FType.List;
        return FType.None;
    }

    private void MakeFilterSelected(FType type)
    {
        // Since making one radio checked didn't
        // make others in the group unchecked,
        // I have to do that here manually.
        countFilterActivator.Checked = false;
        apFilterActivator.Checked = false;
        listFilterActivator.Checked = false;

        switch (type)
        {
            case FType.None:
            case FType.Count:
                {
                    countFilterActivator.Checked = true;
                    countFilter.Style.Add("display", "block");
                    break;
                }
            case FType.Ap:
                {
                    apFilterActivator.Checked = true;
                    apFilter.Style.Add("display", "block");
                    break;
                }
            case FType.List:
                {
                    listFilterActivator.Checked = true;
                    listFilter.Style.Add("display", "block");
                    break;
                }
        }
    }

    private List<string> ConvertSwisUrisToMacAddresses(IEnumerable<string> uris)
    {
        if (uris.Count() == 0)
            return new List<string>(); // TODO: log?

        // wlhm clients uris expected
        var uriString = string.Join("','", uris);
        var q = string.Format("SELECT MAC FROM Orion.Packages.Wireless.Clients WHERE Uri IN ('{0}')", uriString);
        var macList = new List<string>();

        using (var swis = InformationServiceProxy.CreateV3())
        {
            var data = swis.Query(q);
            if (data != null && data.Rows.Count > 0)
            {
                macList.AddRange(from DataRow row in data.Rows select row[0].ToString());
            }
        }

        return macList;
    }

    private string ConvertMacAddressesToSwisUriJSONString(IEnumerable<string> addresses)
    {
        if (addresses.Count() == 0)
            return string.Empty; // TODO: log?

        var macString = string.Join("','", addresses);
        var q = string.Format("SELECT Uri FROM Orion.Packages.Wireless.Clients WHERE MAC IN ('{0}')", macString);
        var uriList = new List<string>();

        using (var swis = InformationServiceProxy.CreateV3())
        {
            var data = swis.Query(q);
            if (data != null && data.Rows.Count > 0)
                uriList.AddRange(from DataRow row in data.Rows select row[0].ToString());
        }

        var jss = new JavaScriptSerializer();
        var jsonString = jss.Serialize(
            new
            {
                entities = uriList
            }
        );
        return jsonString;
    }

    private string GetGuidOfSelectedMap()
    {
        if (!Properties.ContainsKey("SelectedMapId"))
            return string.Empty;

        string fullMapId = Properties["SelectedMapId"].ToString();

        if (string.IsNullOrEmpty(fullMapId))
            return string.Empty;

        int dotIndex = fullMapId.IndexOf('.');

        return dotIndex == -1 ? string.Empty : fullMapId.Substring(0, dotIndex);
    }

    private List<string> LoadWlhmGuids()
    {
        var mapGuids = new List<string>();
        try
        {
            const string selectQuery = "SELECT MapStudioFileID FROM Orion.WirelessHeatMap.Map";
            using (var swis = InformationServiceProxy.CreateV3())
            {
                var data = swis.Query(selectQuery);
                if (data != null && data.Rows.Count != 0)
                {
                    mapGuids.AddRange(from DataRow row in data.Rows select row[0].ToString().ToLowerInvariant());
                }
            }
        }
        catch (Exception ex)
        {
            Log.Error("Failed to load WLHM guids. Ex: " + ex.Message);
        }
        return mapGuids;
    }

    #region TODO: Move this to some shared place and use both in here and clients controller

    private const string QueryResourceLimitation =
@"SELECT rl.ResourceLimitationCount, rl.APMACAddress, rl.APLimitationCount
FROM Orion.WirelessHeatMap.ResourceLimitation rl
WHERE rl.ResourceId=@resourceId AND rl.MapId=@mapId";

    private const string QueryResourceClientLimitation =
@"SELECT ClientMACAddress
FROM Orion.WirelessHeatMap.ResourceClientLimitation
WHERE ResourceId=@resourceId AND MapId=@mapId";

    private const string QueryMapGuiDtoId = @"SELECT TOP 1 MapID FROM Orion.WirelessHeatMap.Map WHERE MapStudioFileID=@mapGUID";

    private WirelessClientLimitationSetting GetClientLimitationSettings(int resourceId, string mapGUID)
    {
        WirelessClientLimitationSetting setting = new WirelessClientLimitationSetting { ResourceId = resourceId, MapGUID = mapGUID };

        using (var proxy = InformationServiceProxy.CreateV3())
        {
            setting.MapId = ConvertMapGUIDToMapId(proxy, mapGUID);

            var resourceDataTable = proxy.Query(QueryResourceLimitation, new Dictionary<string, object> { { "resourceId", resourceId }, { "mapId", setting.MapId } });
            if ((resourceDataTable != null) && (resourceDataTable.Rows.Count > 0))
            {
                var row = resourceDataTable.Rows[0];
                if ((row["ResourceLimitationCount"] != null) && (row["ResourceLimitationCount"] != DBNull.Value))
                    setting.MapLimitationCount = Convert.ToInt32(row["ResourceLimitationCount"]);

                if ((row["APMACAddress"] != null) && (row["APMACAddress"] != DBNull.Value))
                    setting.APMACAddress = row["APMACAddress"].ToString();

                if ((row["APLimitationCount"] != null) && (row["APLimitationCount"] != DBNull.Value))
                    setting.APLimitationCount = Convert.ToInt32(row["APLimitationCount"]);
            }

            var resourceClientDataTable = proxy.Query(QueryResourceClientLimitation, new Dictionary<string, object> { { "resourceId", resourceId }, { "mapId", setting.MapId } });
            if ((resourceClientDataTable != null) && (resourceClientDataTable.Rows.Count > 0))
            {
                foreach (DataRow row in resourceClientDataTable.Rows)
                {
                    setting.ClientLimitations.Add(row["ClientMACAddress"].ToString());
                }
            }
        }

        return setting;
    }
    private void StoreClientLimitationSettings(WirelessClientLimitationSetting settings)
    {
        if (settings == null)
        {
            throw new ArgumentNullException("settings");
        }

        SWISVerbsHelper.InvokeV3(
            "Orion.WirelessHeatMap.ResourceLimitation",
            "InsertResourceLimitation",
            settings.ResourceId,
            new Guid(settings.MapGUID),
            settings.MapLimitationCount ?? 0,
            (!string.IsNullOrEmpty(settings.APMACAddress)) ? settings.APMACAddress : "",
            settings.APLimitationCount ?? 0,
            settings.ClientLimitations ?? new List<string>()
        );
    }
    private int ConvertMapGUIDToMapId(InformationServiceProxy proxy, string guid)
    {
        var mapIdTable = proxy.Query(QueryMapGuiDtoId, new Dictionary<string, object> { { "mapGUID", guid } });

        if ((mapIdTable == null) || (mapIdTable.Rows.Count == 0))
            throw new ArgumentException("Map GUID was not found.");

        return Convert.ToInt32(mapIdTable.Rows[0][0]);
    }
    #endregion

}
