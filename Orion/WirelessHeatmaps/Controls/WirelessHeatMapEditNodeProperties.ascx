﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WirelessHeatMapEditNodeProperties.ascx.cs" Inherits="Orion_WirelessHeatmaps_Controls_WirelessHeatMapEditNodeProperties" %>
<div class="contentBlock" runat="server" id="WirelessHeatmapsSection">
    <div class="contentBlockHeader" runat="server">
        <table>
            <tr>
                <td class="contentBlockHeader">
                    <asp:Literal runat="server" Text="<%$ Resources: WirelessHeatmapsWebContent,WEBDATA_JP0_4 %>" />
                </td>
            </tr>
        </table>
    </div>
    <table class="blueBox">
        <tr runat="server" id="ClientLocation">
            <td class="leftLabelColumn" style="vertical-align: text-top;">
                <asp:CheckBox runat="server" ID="ApplyClientLocationPollInterval" AutoPostBack="true" OnCheckedChanged="ApplyProperty_CheckedChanged" />

                <asp:Literal runat="server" ID="LabelClientLocationPollInterval" Text="<%$ Resources: WirelessHeatmapsWebContent,WEBDATA_JP0_5 %>"/>
            </td>
            <td class="rightInputColumn">
                <asp:CheckBox runat="server" ID="OverrideClientLocationPollInterval" AutoPostBack="true" OnCheckedChanged="OverrideProperty_CheckedChanged"/>

                <asp:Literal runat="server" ID="Literal1" Text="<%$ Resources: WirelessHeatmapsWebContent,WEBDATA_JP0_6 %>"/>
                
                <div runat="server" ID="SettingsClientLocationPollInterval">
                    <asp:TextBox runat="server" ID="ClientLocationPollInterval"/>

                    <asp:RequiredFieldValidator runat="server" Display="Dynamic" SetFocusOnError="True"
                        ID="RequiredValidatorClientLocationPollInterval"
                        ControlToValidate="ClientLocationPollInterval"
                        ErrorMessage="<%$ Resources: WirelessHeatmapsWebContent,WEBDATA_JP0_2 %>"/>
                    <asp:RegularExpressionValidator runat="server" Display="Dynamic" SetFocusOnError="True"
                        ID="RegexValidatorClientLocationPollInterval"
                        ControlToValidate="ClientLocationPollInterval"
                        ErrorMessage="<%$ Resources: WirelessHeatmapsWebContent,WEBDATA_JP0_3 %>"
                        ValidationExpression="^\d+$"/>
                    <asp:RangeValidator runat="server" Display="Dynamic" SetFocusOnError="True" Type="Integer"
                        ID="RangeValidatorClientLocationPollInterval"
                        ControlToValidate="ClientLocationPollInterval"/>

		            <asp:Literal runat="server" Text="<%$ Resources: WirelessHeatmapsWebContent, WEBDATA_JP0_7 %>" />
                </div>
            </td>
        </tr>
    </table>
</div>

