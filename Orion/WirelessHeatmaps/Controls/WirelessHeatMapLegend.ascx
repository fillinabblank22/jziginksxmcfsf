﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WirelessHeatMapLegend.ascx.cs" Inherits="Orion.WirelessHeatmaps.Controls.WirelessHeatMapLegend" %>

<asp:Panel ID="wlhmLegendWrapper" runat="server" ClientIDMode="static">
    <asp:Label ID="legendTitle" CssClass="sw-wlhm-legend-title" runat="server"></asp:Label>
    <div class="sw-wlhm-legend">
        <asp:Repeater ID="mapLegendItems" runat="server">
            <ItemTemplate>
                <div class="sw-wlhm-legend-cell">
                    <div class="sw-wlhm-legend-icon">
                        <img src="<%# Eval("Icon") %>" />
                    </div>
                    <asp:Panel CssClass="sw-wlhm-legend-bar" BackColor='<%# Eval("Color") %>' runat="server" />
                    <div class="sw-wlhm-legend-label">
                        <span><%# Eval("text") %></span> <%--span is being used for positioning!--%>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Panel>
