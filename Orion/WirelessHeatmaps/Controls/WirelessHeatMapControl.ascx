﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WirelessHeatMapControl.ascx.cs" Inherits="WirelessHeatMapControl" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Register Src="~/Orion/WirelessHeatmaps/Controls/WirelessHeatMapLegend.ascx" TagPrefix="orion" TagName="WirelessHeatMapLegend" %>


<div class="sw-wlhm-map-legend-container">
    <asp:ScriptManagerProxy runat="server" ID="ScriptManagerProxy1">
        <Services>
            <asp:ServiceReference Path="~/Orion/NetPerfMon/MapService.asmx" />
        </Services>
    </asp:ScriptManagerProxy>
    <div class="sw-text-reset-small sw-wlhm-normal sw-wlhm-last-generated">
        <span class="sw-wlhm-last-generated-pre sw-wlhm-bold" runat="server" id="LastGeneratedTimestampPre"></span><%--Map Last Generated: --%>
        <span runat="server" id="LastGeneratedTimestamp"></span>
    </div>
    <orion:WirelessHeatMapLegend runat="server" ID="WirelessHeatMapLegend1" />
    <div runat="server" id="LastGeneratedError" class="sw-suggestion sw-suggestion-fail sw-wlhm-normal">
        <span class="sw-suggestion-icon"></span>
        <span id="LastGeneratedErrorMessage" runat="server"></span>
    </div>

    <asp:Label ID="Debug" runat="server" />
    <div id="ClientLegendBox" runat="server">
        <div runat="server" id="ConnectedWirelessClienstLabel" class="sw-wlhm-bold"><%= Resources.WirelessHeatmapsWebContent.WEBDATA_BI0_11 %></div><%--Connected Wireless Clients:--%>
        <div runat="server" id="ShowWirelessClientsCheckboxWrapper" class="sw-wlhm-show-clients-wrapper">
            <input type="checkbox" class="showClients" id="ShowClientsCheckBox" runat="server" /><label><img src="/Orion/WirelessHeatmaps/Images/mappin.png" class="sw-wlhm-map-point-legend"><%= Resources.WirelessHeatmapsWebContent.WEBDATA_LH0_1 %></label>
        </div>
        <div class="sw-wlhm-map-client-info">
            <p>
                <span class="sw-wlhm-info-description-word"><%= Resources.WirelessHeatmapsWebContent.WEBDATA_BI0_12 %></span><%--Displaying: --%>
                <%= Resources.WirelessHeatmapsWebContent.WEBDATA_BI_01 %><span id="InvisibleClientInfoSpan">;<%= Resources.WirelessHeatmapsWebContent.WEBDATA_BI_04 %>
                    <img src="/Orion/WirelessHeatmaps/Images/eyeIconGrey_16x16.png" class="sw-wlhm-icon-outside-clients" alt="" >
                </span>
                <span id="openCFDDivider" class="sw-wlhm-client-divider" runat="server">|</span>
                <a href="javascript:void(0);" class="sw-link" id="openCFD" runat="server"><%= Resources.WirelessHeatmapsWebContent.WEBDATA_BI_02 %></a>
            </p>
            <div class="sw-wlhm-filter-info-wrapper">
                <span class="sw-wlhm-info-description-word"><%= Resources.WirelessHeatmapsWebContent.WEBDATA_BI0_13 %></span><%--Filter applied: --%>
                <img src="/Orion/WirelessHeatmaps/Images/filter.png" class="sw-wlhm-filter-info-img">
                <span class="sw-wlhm-filter-info-text"></span>
            </div> 
            <div>
                <span class="sw-wlhm-info-description-word"><%= Resources.WirelessHeatmapsWebContent.WEBDATA_BI0_14 %></span><%--Next Client Calculation: --%>
                <span runat="server" id="LastCalculatedClientsPositionsTimestamp"></span>
            </div>
            <div class="sw-wlhm-clients-alert-message">
                <div class="sw-suggestion sw-suggestion-warn">
                    <span class="sw-suggestion-icon"></span><span><%= Resources.WirelessHeatmapsWebContent.WEBDATA_BI_03 %></span>
                </div>
            </div>
            <div class="sw-suggestion sw-wlhm-map-client-suggestion">
                 <div>
                    <span class="sw-suggestion-icon"></span>
                    <span class="sw-suggestion-title" runat="server"><%= Resources.WirelessHeatmapsWebContent.WEBDATA_AO0_00 %></span>
                 </div>
		         <div runat="server">
		             <%= string.Format(Resources.WirelessHeatmapsWebContent.WEBDATA_AO0_01,  HelpHelper.GetHelpUrl("orionag-viewingwirelessclientlocation")) %>
                 </div>
            </div>
        </div>
        
<%--        <div id="LastCalculatedClientsPositionsTimestampOuter-<%= ResourceId %>">
            <div runat="server" id="LastCalculatedClientsPositionsTimestamp" class="sw-text-reset-small sw-wlhm-normal sw-wlhm-client-generated-timestamp"></div>
        </div>--%>
    </div>    
    <div runat="server" class="sw-text-reset sw-resource-wantedit" id="downloadButton" visible="false" style="background-color: #E4F1F8; padding: 10px 5px 10px 10px; margin-top: 10px;">
        <ul>
            <li><%= Resources.CoreWebContent.WEBDATA_PF0_1 %></li>
            <li><%= String.Format(Resources.CoreWebContent.WEBCODE_VB0_103, "<strong>", "</strong>", @"<a href=""/NetworkAtlas/NetworkAtlas.exe"" class=""sw-link"">", "</a>")%></li>
        </ul>
    </div>

</div>

<div runat="server">
    <script type="text/javascript">
    
        $(function () {
            var rid = <%=ResourceId%>;
            var showClients = <%=IsShowWirelessClientsEnabled() ? "true" : "false"%>;
            var showClientsCbClientId = '<%=ShowClientsCheckBox.ClientID%>';
            var clientLegendBoxId = '<%= ClientLegendBox.ClientID %>';
            var openClientsFilterDialogClientId = '<%= openCFD.ClientID %>';            
            var mapId = $('#networkMap-' + rid).attr('data-wlhmMap');
            var viewLimitationString = $('#networkMap-' + rid).attr('data-limitation');
            var $mapContainer = $('.sw-wlhm-map-container#networkMap-'+rid);
            var $showClientsCbClient = $('.ResourceWrapper[resourceid=' + rid + '] #' + showClientsCbClientId);
            var $resourceWrapper = $('.ResourceWrapper[resourceid=' + rid + ']');

            $mapContainer.wirelessClientsOverlayer(
            {
                ResourceId: rid,
                showWirelessClients: showClients
            });

            var wlhmcli = new SW.Wireless.HeatMaps.ClientLocationInfo({
                resourceId: rid,
                checkboxId: showClientsCbClientId,
                clientboxId: clientLegendBoxId,
                showWirelessClients: showClients
            });

            wlhmcli.init();

            // Clicking the link will let user filter clients.
            // => need to get to the edit page
            // => that page needs to know where I am coming from,
            // because there is some processing related to WLHM view
            // => I am just stealing the generated link from edit button
            // because I wasn't abble to trigger click on it
            $resourceWrapper.find('#'+openClientsFilterDialogClientId).on('click', function(e) {
                e.preventDefault();
                var editUrl = $resourceWrapper.find('a.EditResourceButton').attr('href');
                if (editUrl)
                    window.location.href = editUrl+'#wlhmClientsFilterCollapse';
            });

            var wlhmDataRefresher;
            //data initialization after refresh if check box is checked
            if(showClients){
                getAndRefreshWlhmData({mapId: mapId, resourseId: rid, ViewLimitationString: viewLimitationString });
                //standart server client calculation 2 min(Network Atlas)
                //standart page refresh - 5 min
                //so we need to sync Network Atlas changes and Map resource
                wlhmDataRefresher = setInterval(function() {
                    getAndRefreshWlhmData({ mapId: mapId, resourseId: rid, ViewLimitationString: viewLimitationString });
                }, 90000);
            }
            //hiding wireless legend after refresh if selected wlhm map has been deleted from Network Atlas
            if (mapId == 0) {
                $resourceWrapper.find('.sw-wlhm-map-legend-container').hide();
            }

            // We need to refresh the map clients both periodically and on demand.
            // All used components should provide some kind of "reload" function,
            // which will be called in this wrapper, so everything is refreshed
            // at the same time.
            function wlhmClientsRefresh() {
                getAndRefreshWlhmData({ mapId: mapId, resourseId: rid, ViewLimitationString: viewLimitationString });
                wlhmcli.refreshFilterInfo();
            }

            //we need to retreive updated data from server and refresh all data
            $showClientsCbClient.on('change', function(){
                if(this.checked) {
                    getAndRefreshWlhmData({ mapId: mapId, resourseId: rid });
                    wlhmDataRefresher = setInterval(function() {
                        getAndRefreshWlhmData({ mapId: mapId, resourseId: rid, ViewLimitationString: viewLimitationString });
                    }, 90000);
                }
                else {
                    if(wlhmDataRefresher) {
                        //we should refresh data by timer only when showing clients
                        clearInterval(wlhmDataRefresher);
                    }    
                }
            });

            //Also we have multiple js instances which retreve the same data, so the main caller method is here.
            function getAndRefreshWlhmData(options){
                SW.Core.Services.callControllerAction(
                    "/api/WirelessClients", 
                    "GetClients", 
                {MapId:  options.mapId, ResourceId: options.resourseId, ViewLimitationString: viewLimitationString}, 
                    function (data) {
                        wlhmcli.refresh(data);
                        $mapContainer.wirelessClientsOverlayer('reload', data);
                    });   
            }
        });
    </script>
</div>
