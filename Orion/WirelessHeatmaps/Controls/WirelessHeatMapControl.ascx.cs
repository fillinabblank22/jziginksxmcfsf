﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using SolarWinds.MapEngine.Helpers;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Logging;
using SolarWinds.Wireless.Heatmaps.Common.Enums;

using SettingsDAL = SolarWinds.Orion.Core.Common.SettingsDAL;
using System.IO;

public partial class WirelessHeatMapControl : NetworkMapControl
{
    // INLINE RENDERING:
    // Creating (mostly) GenericHtmlControls and adding them
    // directly to Controls collections. (Server does rendering)
    // Useful for reports / resource rendered in RenderControl mode.

    // NO INLINE RENDERING:
    // Using AJAX to get map HTML from web service. (Client does Rendering)

    private static readonly Log Log = new Log();

    private DateTime _lastStarted;
    private DateTime _lastFinished;
    private WLHMErrorCodes _errorCode;
    private string _errorDescription = null;
    private int _wlhmMapId;
    private const string ClientLocationPollInterval = "NPM_Settings_WLHM_ClientLocation_PollInterval";

    private static readonly Log _log = new Log();

    // note: the same key uses also Orion\NetPerfMon\Controls\EditResourceControls\EditNetworkMap.ascx.cs
    const string ShowWirelessClientsPropertyKeyFormat = "ShowWirelessClients{0}";
    private string ViewLimitationString = string.Empty;

    private readonly Dictionary<string, string> _ResourceProperties = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

    protected override void OnInit(EventArgs e)
    {
        OrionInclude.CoreFile("WirelessHeatmaps/styles/WirelessHeatMaps.css", OrionInclude.Section.Top);
        OrionInclude.CoreFile("NetPerfMon/js/networkmapcontrol.js");
        OrionInclude.CoreFile("WirelessHeatmaps/js/ClientLocation.js", OrionInclude.Section.Bottom);
        OrionInclude.CoreFile("WirelessHeatmaps/js/ClientLocationInfo.js", OrionInclude.Section.Bottom);
        OrionInclude.CoreFile("WirelessHeatmaps/js/ClientsFilterDialog.js", OrionInclude.Section.Bottom);
        OrionInclude.CoreFile("WirelessHeatmaps/js/ClientsFilterDialog.helper.js", OrionInclude.Section.Bottom);

        #region AJAX rendering (InlineRendering != TRUE)

        //this is for reports - no AJAX
        if (string.IsNullOrEmpty(Request.QueryString["InlineRendering"]) || Request.QueryString["InlineRendering"] != "TRUE")
        {
            // when inline rendering is not used for web then we call web service to get map data
            if (!UseInlineRenderingForWeb)
            {
                const string mapLoadScript = "WLHMMapLoadScript";
                if (!Page.ClientScript.IsClientScriptBlockRegistered(this.GetType(), mapLoadScript))
                {
                    var sbScript = new StringBuilder();
                    sbScript.AppendLine("$(function () {");
                    sbScript.AppendLine("   MapService.set_timeout(180000);");
                    sbScript.AppendLine("   $('.mapIdentifier').each(function (i) {");
                    // mapIdentifier contains string {mapId}|{resourceId}
                    // the resource id is sent to the OnComplete function, so it can update the networkMap-{resourceId} element
                    sbScript.AppendLine("   var identifiers = $(this).val().split('|');");
                    sbScript.AppendLine("   MapService.GetMap(identifiers[1], function(htmlMapCode, resourceIdentifier){MapControlOnAjaxComplete(htmlMapCode, resourceIdentifier);$('.sw-wlhm-map-container#networkMap-" + ResourceId + "').wirelessClientsOverlayer({ResourceId: " + ResourceId + "});}, MapControlOnAjaxError, identifiers[2]);");
                    sbScript.AppendLine("});});");

                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), mapLoadScript, sbScript.ToString(), true);
                }
            }
        }
        else
        {
            InlineRendering = true;
        }

        #endregion

        #region setting inherited properties

        int shortCacheTimeout = 30;
        try
        {
            string cacheTimeoutString = SolarWinds.Orion.Core.Common.WebSettingsDAL.Get("MapStudio-ShortCacheTimeout");
            if (!string.IsNullOrEmpty(cacheTimeoutString))
            {
                shortCacheTimeout = int.Parse(cacheTimeoutString);
            }
        }
        catch (KeyNotFoundException)
        { }
        ShortCacheTimeout = shortCacheTimeout;
        UseCaching = true;
        string swoisEndpoint = string.Format(ConfigurationManager.AppSettings["SWOISv3.RemoteEndpoint"], "localhost");
        string server, port, postfix;
        MapService.ParseEndpointLocation(swoisEndpoint, out server, out port, out postfix);

        RootLocalPath = MapService.GetTempPath();
        ApplicationLocalPath = MapService.GetMapStudioInstallPath();
        TargetDir = RootLocalPath;
        ServerAddress = server;
        SwisPort = port;
        SwisPostfix = postfix;
        UserName = HttpContext.Current.Profile.UserName;
        AccountId = HttpContext.Current.Profile.UserName;

        var viewInfo = ((ViewInfo)HttpContext.Current.Items[typeof(ViewInfo).Name]);
        ViewId = viewInfo == null ? 0 : viewInfo.ViewID;

        ViewLimitationString = (viewInfo != null && viewInfo.LimitationID > 0)
            ? ViewLimitationString = string.Format(" WITH LIMITATION {0} ", viewInfo.LimitationID)
            : String.Empty;

        if (UsernameCredentials == null)
        {
            UsernameCredentials = CredentialsHelper.GetOrionCertificateCredentials();
        }

        try
        {
            CustomizedTooltips = SolarWinds.MapStudio.Web.CustomTooltips.DeserializeFromString(SolarWinds.Orion.Web.DAL.WebSettingsDAL.Get("MapStudio-CustomTooltips"));
        }
        catch (ArgumentNullException)
        {
            CustomizedTooltips = new SolarWinds.MapStudio.Web.CustomTooltips();
        }
        catch (KeyNotFoundException)
        {
            CustomizedTooltips = new SolarWinds.MapStudio.Web.CustomTooltips();
        }

        #endregion

        base.OnInit(e);
    }

    private void ReadResourceProperties(IDataReader reader)
    {
        if (reader != null && reader.GetOrdinal("PropertyName") > -1 && reader.GetOrdinal("PropertyValue") > -1)
        {
            var name = Convert.ToString(reader["PropertyName"]);
            var value = Convert.ToString(reader["PropertyValue"]);

            _ResourceProperties[name] = value;
        }
        else
        {
            throw new InvalidOperationException("Unknown Resource properties structure detected. The db schema has probably changed");
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        
    }

    protected override void OnPreRender(EventArgs e)
    {
        LoadMapGenerationInfo();
        ShowMapGenerationResults();

        int resourceId = 0;
        bool isValidResourceId = int.TryParse(ResourceId, out resourceId);

        if (isValidResourceId)
        {
            ResourcePropertiesDAL.GetRecordsByResourceID(resourceId, ReadResourceProperties);
            InitClientLocationControls(resourceId);
        }

        //this is for reports - no AJAX
        if (InlineRendering)
        {
            base.OnPreRender(e);

            var mapService = new MapService();
            string mapHtml = mapService.GetMap(SessionId);
            if (UseInlineRenderingForWeb)
            {
                var mapDivBuilder = new StringBuilder();
                mapDivBuilder.AppendFormat("<div class='sw-wlhm-map-wrapper'><div id='networkMap-{0}' data-wlhmMap='{1}' data-mapGuid='{2}' data-limitation='{3}' class='sw-wlhm-map-container'>", ResourceId, _wlhmMapId, MapId.Replace(".OrionMap", ""), ViewLimitationString);
                mapDivBuilder.Append(mapHtml);
                mapDivBuilder.Append("</div></div>");

                var literalControl = new LiteralControl(mapDivBuilder.ToString());
                networkMapContainer.Controls.Add(literalControl);
                AddLegend();
                return;
            }

            string map = MapService.RemoveTooltips(mapHtml);
            networkMapContainer.Controls.Add(new LiteralControl(map));
            AddLegend();
            return;
        }

        if (string.IsNullOrEmpty(ResourceId))
        {
            ResourceId = "0";
        }
        networkMapContainer.Controls.Add(new LiteralControl("<div class='sw-wlhm-map-wrapper'><div id=\"networkMap-" + ResourceId + "\" class='sw-wlhm-map-container'></div></div>"));

        var mapId = new HtmlInputHidden
        {
            ID = string.Format("mapId-{0}", ResourceId),
            Value = MapId,
            ClientIDMode = ClientIDMode.Static
        };
        Controls.Add(mapId);

        base.OnPreRender(e);
    }

    /// <summary>Initializes the client location controls.</summary>
    /// <param name="resourceId">The resource identifier.</param>
    private void InitClientLocationControls(int resourceId)
    {
        ShowClientsCheckBox.Checked = IsShowWirelessClientsEnabled();
        bool shownOnReport = resourceId < 0;
        if (shownOnReport)
        {
            ShowWirelessClientsCheckboxWrapper.Visible = false;      

            if (ShowClientsCheckBox.Checked)             
                ConnectedWirelessClienstLabel.Visible = true;            
            else            
                ConnectedWirelessClienstLabel.Visible = false;            
        }

        if (Profile.AllowCustomize && !shownOnReport)
        {
            openCFD.Visible = true;
            openCFDDivider.Visible = true;
        }
        else
        {
            openCFD.Visible = false;
            openCFDDivider.Visible = false;
        }        

        var lastCalculated = GetLastClientPositionCalculationDate(_wlhmMapId);
        var pollInterval = SettingsDAL.GetCurrentInt(ClientLocationPollInterval, 5);
        bool isOnNotReportAndClientsVisible = IsShowWirelessClientsEnabled() || !shownOnReport;

        if (lastCalculated.HasValue && isOnNotReportAndClientsVisible)
        {
            LastCalculatedClientsPositionsTimestamp.InnerText = string.Format(Resources.WirelessHeatmapsWebContent.WEBDATA_BI0_15, lastCalculated.Value.ToString("h:mm tt", CultureInfo.CurrentCulture), pollInterval.ToString(CultureInfo.InvariantCulture));
        }
        else
        {
            LastCalculatedClientsPositionsTimestamp.Visible = false;
        }

    }

    /// <summary>
    /// Determines whether is "Show wireless clients" enabled. It reads the resource property setting. In case it was not yet set then <code>false</code> is returned.
    /// </summary>
    public bool IsShowWirelessClientsEnabled()
    {
        string mapId = Path.GetFileNameWithoutExtension(_ResourceProperties["selectedMapId"]);
        string ShowWirelessClientsPropertyKey = string.Format(ShowWirelessClientsPropertyKeyFormat, mapId);

        bool hasSetting = _ResourceProperties.ContainsKey(ShowWirelessClientsPropertyKey);

        if (hasSetting)
        {
            return Convert.ToBoolean(_ResourceProperties[ShowWirelessClientsPropertyKey]);
        }
        else
        {
            return false;
        }
    }

    private void AddLegend()
    {
        if (ShowColorLegend)
        {
            WirelessHeatMapLegend1.SetMapStyleAttributes(ResourceId);
            WirelessHeatMapLegend1.IsVisible = true;
        }
    }

    private DateTime? GetLastClientPositionCalculationDate(int mapId)
    {
        const string detailsQuery = @"
            SELECT MIN(LastCalculationTime) As LastCalculated
            FROM Orion.WirelessHeatMap.ClientLocation
            WHERE MapId = @mapId";

        var detailsQueryParams = new Dictionary<string, object>
        {
            {"mapId", mapId}
        };

        using (var swis = InformationServiceProxy.CreateV3())
        {
            var rv = swis.Query(detailsQuery, detailsQueryParams);

            if (rv != null && rv.Rows.Count > 0)
            {
                DataRow row = rv.Rows[0];

                if (rv.Columns.Contains("LastCalculated") && row["LastCalculated"] != DBNull.Value)
                    return ((DateTime)row["LastCalculated"]).ToLocalTime();
            }
            else
            {
                // Failed to get last calculated datetime, maybe no clients positions are available
            }
        }

        return null;
    }

    private void LoadMapGenerationInfo()
    {
        Log.Debug("Loading WLHM map for map ID: " + MapId);

        string swisQueryMapIdProperty;
        string mapId;
        if (IsMapIdGuid(MapId))
        {
            swisQueryMapIdProperty = "MapStudioFileID";
            mapId = MapId.Replace(".OrionMap", "");
        }
        else
        {
            swisQueryMapIdProperty = "DisplayName";
            mapId = MapId;
        }

        string detailsQuery = string.Format(@"
            SELECT wlhm.MapId, wlhm.LastGenerationStarted, wlhm.LastGenerationFinished, wlhm.ErrorCode, err.Description
            FROM Orion.WirelessHeatMap.Map as wlhm
            LEFT JOIN Orion.WirelessHeatMap.ErrorCode as err ON err.Code = wlhm.ErrorCode
            WHERE wlhm.{0} = @mapId", swisQueryMapIdProperty);

        var detailsQueryParams = new Dictionary<string, object>
        {
            {"mapId", mapId}
        };

        using (var swis = InformationServiceProxy.CreateV3())
        {
            var rv = swis.Query(detailsQuery, detailsQueryParams);

            if (rv != null && rv.Rows.Count > 0)
            {
                Log.Warn("Map found for map ID: " + MapId);
                DataRow row = rv.Rows[0];

                if (rv.Columns.Contains("MapId") && row["MapId"] != DBNull.Value)
                    _wlhmMapId = ((int)row["MapId"]);

                if (rv.Columns.Contains("LastGenerationStarted") && row["LastGenerationStarted"] != DBNull.Value)
                    _lastStarted = ((DateTime)row["LastGenerationStarted"]).ToLocalTime();

                if (rv.Columns.Contains("LastGenerationFinished") && row["LastGenerationFinished"] != DBNull.Value)
                    _lastFinished = ((DateTime)row["LastGenerationFinished"]).ToLocalTime();

                if (rv.Columns.Contains("ErrorCode") && row["ErrorCode"] != DBNull.Value)
                    WLHMErrorCodes.TryParse(row["ErrorCode"].ToString(), out _errorCode);

                if (rv.Columns.Contains("Description") && row["Description"] != DBNull.Value)
                    _errorDescription = row["Description"].ToString();
            }
            else
            {
                Log.Warn("No map found for map ID: " + MapId);
                Log.Debug("SWIS Query: " + detailsQuery);
                Log.Debug("SWIS Query map ID used: " + detailsQueryParams["mapId"]);
                //Core code handles displaying of appropriate message on UI
            }
        }
    }

    /// <summary>
    /// Determines whether provided map ID is map GUID or map name.
    /// </summary>
    /// <param name="mapId">Provided map ID</param>
    /// <returns>True if provided map ID is GUID, false otherwise</returns>
    private bool IsMapIdGuid(string mapId)
    {
        if (mapId.EndsWith(".OrionMap"))
        {
            mapId = mapId.Replace(".OrionMap", "");
            Guid guid;
            if (Guid.TryParse(mapId, out guid))
            {
                Log.Debug("Given map ID: '" + mapId + "' is GUID.");
                return true;
            }
        }
        Log.Debug("Given map ID: '" + mapId + "' is map name.");
        return false;
    }

    protected override void SetDownloadButtonVisibility(bool show)
    {
        downloadButton.Visible = show;
    }

    private void ShowMapGenerationResults()
    {
        var timeFormat = "MMM d yyyy; h:mm:ss tt";
        LastGeneratedTimestampPre.InnerText = Resources.WirelessHeatmapsWebContent.WEBDATA_BI0_10;
        LastGeneratedTimestamp.InnerText = _lastFinished.ToString(timeFormat, CultureInfo.CurrentCulture);
        if (_errorCode == WLHMErrorCodes.OK)
        {
            LastGeneratedError.Visible = false;
            return;
        }

        LastGeneratedErrorMessage.InnerHtml = GetErrorMessage(_errorCode);
    }

    private String GetErrorMessage(WLHMErrorCodes code)
    {
        switch (code)
        {
            case WLHMErrorCodes.RequirementsNotMet:
                return Resources.WirelessHeatmapsWebContent.WEBDATA_JP0_1;
            default:
                string lastGeneratedErrorFormat = Resources.WirelessHeatmapsWebContent.NPMWEBDATA_LF0_LastGenerated;
                if (_errorDescription != null)
                {
                    lastGeneratedErrorFormat = string.Format(
                        "{0}<br><br>{1}",
                        Resources.WirelessHeatmapsWebContent.NPMWEBDATA_LF0_LastGenerated,
                        Resources.WirelessHeatmapsWebContent.NPMWEBDATA_LF0_LastError);
                }

                return string.Format(lastGeneratedErrorFormat, _lastStarted.ToString(CultureInfo.CurrentCulture), _errorDescription);
        }
    }
}
