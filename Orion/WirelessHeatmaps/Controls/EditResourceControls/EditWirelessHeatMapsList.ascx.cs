﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;

public partial class Orion_WirelessHeatmaps_Controls_EditResourceControls_EditWirelessHeatMapsList : BaseResourceEditControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            return new Dictionary<string, object>();
        }
    }
  
    protected string GetHintText
    {
        get
        {
            return string.Format(Resources.WirelessHeatmapsWebContent.NPMWEBDATA_PD0_08,
                string.Format("<a class=\"sw-link\" href=\"/Orion/Admin/PollingSettings.aspx\">{0}</a>", Resources.CoreWebContent.WEBDATA_AK0_486));
        }
    }
}