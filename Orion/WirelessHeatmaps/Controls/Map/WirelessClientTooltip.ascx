﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WirelessClientTooltip.ascx.cs" Inherits="Orion_WirelessHeatmaps_Controls_Map_WirelessClientTooltip" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<table>
    <tr>
        <th>
            <%= WirelessHeatmapsWebContent.NPMWEBDATA_PS0_12 %>
        </th>
        <td>
            <%= WebSecurityHelper.HtmlEncode(Hostname) %>
        </td>
    </tr>
    <tr>
        <th>
            <%= WirelessHeatmapsWebContent.NPMWEBDATA_PS0_13 %>
        </th>
        <td>
            <%= WebSecurityHelper.HtmlEncode(IpAddress) %>
        </td>
    </tr>
    <tr>
        <th>
            <%= WirelessHeatmapsWebContent.NPMWEBDATA_PS0_14 %>
        </th>
        <td>
            <%= WebSecurityHelper.HtmlEncode(MacAddress) %>
        </td>
    </tr>
    <tr>
        <th>
            <%= WirelessHeatmapsWebContent.NPMWEBDATA_PS0_15 %>
        </th>
        <td>
            <%= WebSecurityHelper.HtmlEncode(ApName) %>
        </td>
    </tr>
</table>