﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Resources;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.Map;

public partial class Orion_WirelessHeatmaps_Controls_Map_WirelessClientTooltip : MapPointTooltipControlBase
{
    protected string Hostname;
    protected string IpAddress;
    protected string MacAddress;
    protected string ApName;

    private const string ClientDetailsQuery = @"SELECT c.RDNS, c.IPAddress, cl.ClientMACAddress AS MAC, ap.DisplayName AS APName
FROM Orion.WirelessHeatMap.ClientLocation cl
LEFT JOIN Orion.Wireless.Clients c ON cl.ClientMACAddress = c.MAC
LEFT JOIN Orion.Wireless.AccessPoints ap ON c.AccessPointID = ap.ID
WHERE cl.Id = @id";

    protected void Page_Load(object sender, EventArgs e)
    {
        Hostname = IpAddress = MacAddress = ApName = WirelessHeatmapsWebContent.NPMWEBDATA_PS0_1;

        using (var swis = InformationServiceProxy.CreateV3())
        {
            var result = swis.Query(ClientDetailsQuery, new Dictionary<string, object>
                {
                    {"id", int.Parse(TooltipParams.EntityKey)}
                });

            if (result != null && result.Rows.Count > 0)
            {
                var row = result.Rows[0];
                MacAddress = FormatMacAddress((string)row["MAC"]);
                if (!row.IsNull("IPAddress"))
                {
                    IpAddress = (string)row["IPAddress"];
                }
                if (!row.IsNull("RDNS"))
                {
                    Hostname = (string)row["RDNS"];
                }
                if (!row.IsNull("APName"))
                {
                    ApName = (string)row["APName"];
                }

            }
        }

    }

    private string FormatMacAddress(string mac)
    {
        return Regex.Replace(mac, @"\w{2}", "$0:").Trim(':');
    }
}