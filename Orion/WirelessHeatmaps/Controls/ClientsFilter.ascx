﻿<%@ Control ClientIDMode="Predictable" Language="C#" AutoEventWireup="true" CodeFile="ClientsFilter.ascx.cs" Inherits="Orion_WirelessHeatmaps_Controls_ClientsFilter" %>
<%@ Register Src="~/Orion/Controls/NetObjectPicker.ascx" TagPrefix="orion" TagName="NetObjectPicker" %>

<orion:Include ID="ClientsFilterHelper" File="/WirelessHeatmaps/js/ClientsFilterHelper.js" runat="server" />
<orion:Include ID="ClientsFilter" File="/WirelessHeatmaps/js/clientsFilter.js" runat="server" />
<orion:Include ID="WLHMCSS" File="/WirelessHeatmaps/styles/WirelessHeatMaps.css" runat="server" />

<br />
<asp:Label ID="wlhmClientsFilterCollapse" ClientIDMode="Static" CssClass="sw-wlhm-cf-collapse-title sw-wlhm-cf-collapsed" runat="server" >
    <%= Resources.WirelessHeatmapsWebContent.WEBDATA_BI0_16 %><%--Select Wireless Clients To Be Displayed:--%>
</asp:Label>
<div id="wlhmClientsFilter" class="sw-wlhm-cf" runat="server">

    <label>
        <input class="sw-wlhm-cf-activator" data-cftype="count" id="countFilterActivator" name="wlhmcfActivator" runat="server" type="radio" />
        <%= Resources.WirelessHeatmapsWebContent.WEBDATA_PD0_01 %><%--Show every client connected to any AP on the map--%>
    </label>
    <div class="sw-wlhm-cf-body" data-cftype="count" id="countFilter" runat="server">
        <label class="sw-wlhm-cf-firstCol">
            <input id="countFilterInputEnabler" runat="server" type="checkbox" />
            <%= Resources.WirelessHeatmapsWebContent.WEBDATA_PD0_02 %><%--Limit the number of clients to:--%>
        </label>
        <div class="sw-wlhm-cf-inputWithHint sw-wlhm-cf-secondCol">
            <asp:TextBox Enabled="False" ID="countFilterInput" runat="server"></asp:TextBox>
            <span><%= Resources.WirelessHeatmapsWebContent.WEBDATA_BI_09 %> <%--Use number between 1 and 100--%></span>
        </div>
        <span class="sw-suggestion sw-suggestion-fail sw-wlhm-cf-thirdCol" id="countFilterError">
            <span class="sw-suggestion-icon"></span>
            <span class="sw-wlhm-cf-errorText">
                <%= Resources.WirelessHeatmapsWebContent.WEBDATA_PD0_05 %> <%--Entered value is not valid: --%>
            </span>
        </span>
        <asp:CustomValidator
            ControlToValidate="countFilterInput"
            ClientValidationFunction="SW.Wireless.HeatMaps.clientsFilter.validateCountFilterAmount"
            ValidateEmptyText="True"
            EnableClientScript="True"
            runat="server"></asp:CustomValidator>
    </div>
    <!-- sw-wlhm-cf-body end for count filter -->

    <label>
        <input class="sw-wlhm-cf-activator" data-cftype="ap" id="apFilterActivator" name="wlhmcfActivator" runat="server" type="radio" />
        <%= Resources.WirelessHeatmapsWebContent.WEBDATA_PD0_03 %><%--Only show clients connected to a specific AP--%>
    </label>
    <div class="sw-wlhm-cf-body" data-cftype="ap" id="apFilter" runat="server">
        <span class="sw-wlhm-cf-firstCol">
            <%= Resources.WirelessHeatmapsWebContent.WEBDATA_PD0_04 %> <%--Wireless AP:--%> 
        </span>
        <div class="sw-wlhm-cf-secondCol">
            <asp:DropDownList runat="server" ID="apFilterDropDown"></asp:DropDownList>
        </div>
        
        <div class="sw-wlhm-cf-rowSeparator"></div>
        
        <label class="sw-wlhm-cf-firstCol">
            <input id="apFilterInputEnabler" runat="server" type="checkbox" />
            <%= Resources.WirelessHeatmapsWebContent.WEBDATA_PD0_02 %><%--Limit the number of clients to:--%>
        </label>
        <div class="sw-wlhm-cf-inputWithHint sw-wlhm-cf-secondCol">
            <asp:TextBox Enabled="False" ID="apFilterInput" runat="server"></asp:TextBox>
            <span><%= Resources.WirelessHeatmapsWebContent.WEBDATA_BI_09 %> <%--Use number between 1 and 100--%></span>
        </div>
        <span class="sw-suggestion sw-suggestion-fail sw-wlhm-cf-thirdCol" id="apFilterError">
            <span class="sw-suggestion-icon"></span>
            <span class="sw-wlhm-cf-errorText">
                <%= Resources.WirelessHeatmapsWebContent.WEBDATA_PD0_05 %> <%--Entered value is not valid: --%>
            </span>
        </span>
        <asp:CustomValidator
            ControlToValidate="apFilterInput"
            ClientValidationFunction="SW.Wireless.HeatMaps.clientsFilter.validateApFilterAmount"
            ValidateEmptyText="True"
            EnableClientScript="True"
            runat="server"></asp:CustomValidator>
    </div>
    <!-- sw-wlhm-cf-body end for ap filter -->

    <label>
        <input class="sw-wlhm-cf-activator" data-cftype="list" id="listFilterActivator" name="wlhmcfActivator" runat="server" type="radio" />
        <%= Resources.WirelessHeatmapsWebContent.WEBDATA_PD0_06 %> <%--Let me pick specific clients to show--%>
    </label>
    <div class="sw-wlhm-cf-body" data-cftype="list" id="listFilter" runat="server">
        <orion:NetObjectPicker ID="netObjectPicker" runat="server" />
        <%--This field is needed to initialize NetObjectPicker with alredy selected entities.--%>
        <asp:HiddenField ID="wlhmcfListFilterCurrentValues" runat="server" />
    </div>
    <!-- sw-wlhm-cf-body end for list filter -->
    
    <%--These fields are used to store filter settings. The values are saved onSubmit
    via registered JavaScript function and read during postback on server.--%>
    <asp:HiddenField ID="wlhmcfFilterEnabled" runat="server" />
    <asp:HiddenField ID="wlhmcfFilterType" runat="server" />
    <asp:HiddenField ID="wlhmcfFilterValue" runat="server" />
    <asp:HiddenField ID="wlhmcfMapGuids" runat="server" />

</div>

<script>
    (function() {

        var resourceId = <%= Resource.ID %>;
        var selectedMapGuid = '<%= MapGUID %>';
        
        // Following part is being executed on every postback,
        // but we can ignore the cases when wlhm isn't selected.
        // That's determined by existence of selectedMapGuid
        if (selectedMapGuid) {
            SW.Wireless.HeatMaps.clientsFilter.init(resourceId);
            SW.Wireless.HeatMaps.clientsFilter.initNetObjectPicker(selectedMapGuid);

            var collapseClass = 'sw-wlhm-cf-collapsed';
            var $collapseSwitch = $('#<%= wlhmClientsFilterCollapse.ClientID %>');
            var $filtersWrapper = $('#<%= wlhmClientsFilter.ClientID %>');

            // Make the collapse switch active
            $collapseSwitch.off('click.collapse').on('click.collapse', function(e){
                // toggle icon
                $(this).toggleClass(collapseClass);
                // toggle content
                $filtersWrapper.toggle();
            });

            // Set initial visibility state of wrapper
            $filtersWrapper.toggle(!($collapseSwitch.hasClass(collapseClass) && $filtersWrapper.is(':visible')));
        }

        // Maplist is available when editing map resource outside of
        // WLHM details view. Specific handling is needed for WLHM.
        var $maplist = $('[id*="mapsListbox"]');
        if (exists($maplist)) {
            
            var mapGuidsInput = $('input[id*="wlhmcfMapGuids"]');
            var mapGuids = JSON.parse(mapGuidsInput.val())["wlhm"];

            $maplist.focus();
            $maplist.data('selectedOption', $maplist.find('option:selected'));
            $maplist.off('change.mapSelected').on('change.mapSelected', function(e) {

                // FB383625: We apparently want mixed behaivor.. the map list
                // shouldn't cause postback when switching between network maps.
                var previousMap = parseGuid($maplist.data('selectedOption'));
                var wasWlhm = _.contains(mapGuids, previousMap);

                var currentMap = parseGuid($maplist.find('option:selected'));
                var isWlhm = _.contains(mapGuids, currentMap);

                if (wasWlhm || isWlhm) {
                    __doPostBack($maplist.attr('id'), $maplist.val());
                }

            });

            // When I change the map selection without postback, I need to update
            // the selectedOption data. Click fires after Change event.
            $maplist.off('click.mapSelected').on('click.mapSelected', function() {
                $maplist.data('selectedOption', $maplist.find('option:selected'));
            });
        }

        // Helper function to check if jQuery selector found something
        function exists(element){
            return element && element.length > 0;
        }

        function parseGuid($option) {
            var mapId = $option.val();
            var guid = mapId.replace(".OrionMap", "");
            return guid.toLowerCase();
        }

    }());
</script>
