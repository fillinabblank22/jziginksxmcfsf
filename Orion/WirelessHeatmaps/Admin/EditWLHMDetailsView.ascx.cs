﻿using System;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_WirelessHeatmaps_Admin_EditWLHMDetailsView : ProfilePropEditUserControl 
{
    #region ProfilePropEditUserControl members
    public override string PropertyValue
    {
        get
        {
            return this.lbxWLHMDetails.SelectedValue;
        }
        set
        {
            ListItem item = this.lbxWLHMDetails.Items.FindByValue(value);
            if (null != item)
            {
                item.Selected = true;
            }
            else
            {
                //TODO default to "by device type"
                this.lbxWLHMDetails.Items.FindByValue("-1").Selected = true;
            }
        }
    }
    #endregion

    protected override void OnInit(EventArgs e)
    {
        foreach (ViewInfo info in ViewManager.GetViewsByType("WLHMDetails"))
        {
            ListItem item = new ListItem(info.ViewTitle, info.ViewID.ToString());
            this.lbxWLHMDetails.Items.Add(item);
        }

        base.OnInit(e);
    }
}