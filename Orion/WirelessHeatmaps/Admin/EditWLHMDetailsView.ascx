﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditWLHMDetailsView.ascx.cs" Inherits="Orion_WirelessHeatmaps_Admin_EditWLHMDetailsView" %>

<asp:ListBox runat="server" ID="lbxWLHMDetails" SelectionMode="single" Rows="1" >
    <asp:ListItem Text="<%$Resources:WirelessHeatmapsWebContent,NPMWEBDATA_AK1_5%>" Value="0" />
    <asp:ListItem Text="<%$Resources:WirelessHeatmapsWebContent,NPMWEBDATA_AK1_6%>" Value="-1" />
    
</asp:ListBox>
<%-- More ListItems will be populated at run-time --%>