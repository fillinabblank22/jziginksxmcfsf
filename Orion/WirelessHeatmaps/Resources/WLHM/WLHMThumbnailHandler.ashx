﻿<%@ WebHandler Language="C#" Class="WLHMThumbnailHandler" %>

using System;
using System.Activities.Expressions;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;
using SolarWinds.Orion.Common;

public class WLHMThumbnailHandler : IHttpHandler {
    
    public void ProcessRequest (HttpContext context)
    {

        const string queryDefinition = @"SELECT NA.FileData FROM MapStudioFiles NA
INNER JOIN WirelessHeatMap WLHM ON NA.FileId=WLHM.MapStudioFileGuid
WHERE WLHM.ID = @ID";

        const string queryThumbnail = @"SELECT NA.FileData FROM MapStudioFiles NA WHERE NA.FileId = @FileId";
        
        string wlhmId =  context.Request.QueryString["WLHM"];
        if (wlhmId == null)
        {
            throw new HttpException(400, "Invalid WLHM ID format");
        }


        using (var cmd = SqlHelper.GetTextCommand(queryDefinition))
        {
            cmd.Parameters.AddWithValue("@ID", wlhmId);

            byte[] binaryDefinition = (byte[])SqlHelper.ExecuteScalar(cmd);
            string textDefinition = UnicodeEncoding.Unicode.GetString(binaryDefinition, 0, binaryDefinition.Length);
            Match result = Regex.Match(textDefinition, @"(ThumbnailImageName=)([0-9A-Fa-f\-]{36})(.jpg)");
            if (!result.Success)
                return;     //return dummy thumbnail

            string fileId = result.Groups[2].Value;

            cmd.CommandText = queryThumbnail;
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@FileId", Guid.Parse(fileId));

            byte[] buffer = (byte[])SqlHelper.ExecuteScalar(cmd);

            ReturnOriginalImage(context, buffer);
        }
        
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

    private void ReturnOriginalImage(HttpContext context, byte[] buffer)
    {
        context.Response.Clear();
        context.Response.ContentType = "image/jpeg";
        context.Response.BinaryWrite(buffer);
        context.Response.Flush();        
    }   
}
