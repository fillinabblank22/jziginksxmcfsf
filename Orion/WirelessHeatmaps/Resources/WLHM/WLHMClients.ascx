﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WLHMClients.ascx.cs" Inherits="Orion_WirelessHeatmaps_Resources_WLHMClients" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper" CssClass="sw-wlhm-maplist">
    <Content>
        <orion:CustomQueryTable runat="server" ID="CustomTable" />
        <script>
            $(function() {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= CustomTable.UniqueClientID %>,
                        initialPage: 0,
                        rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "10" %>,
                        searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
                        searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
                        allowSort: false,
                        columnSettings:
                        {
                            "Client_MAC": {
                                header: '<%= Resources.WirelessHeatmapsWebContent.NPMWEBDATA_VB0_120 %>', // MAC Address
                                formatter: function(value, row, cellInfo) {
                                    return value.replace(/(.{2})/g,"$1:").slice(0,-1);
                                },
                            },
                            "Client_Name": {
                                header:'<%=Resources.WirelessHeatmapsWebContent.NPMWEBCODE_PD0_24 %> / <%=Resources.WirelessHeatmapsWebContent.NPMWEBDATA_VB0_116 %>' // IP Address / Name
                            },
                            "AP_Name": {
                                header: '<%= Resources.WirelessHeatmapsWebContent.NPMWEBDATA_VB0_296 %>',  // Access Point

                            },
                            "Client_SS": {
                                header: '<%=Resources.WirelessHeatmapsWebContent.NPMWEBDATA_PD0_01 %>', // Signal Strength (RSSI)
                                isHtml: true,
                                formatter: function(value, row, cellInfo) {
                                    return String.format('<%= Resources.WirelessHeatmapsWebContent.NPMWEBDATA_PD0_02 %>', value);
                                }
                            }
                        }
                    });
                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceId %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });
        </script>
    </Content>
</orion:resourceWrapper>