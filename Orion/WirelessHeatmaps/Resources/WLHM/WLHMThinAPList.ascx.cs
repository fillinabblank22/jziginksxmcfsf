﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Wireless.Heatmaps.Web;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_WirelessHeatmaps_Resources_WLHMThinAPList : BaseResourceControl
{
    protected ResourceSearchControl SearchControl { get; private set; }

    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        Wrapper.HeaderButtons.Controls.Add(SearchControl);

        var wlhm = GetInterfaceInstance<IWLHMProvider>();
        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        CustomTable.SWQL = string.Format(SWQL, wlhm.WLHM.ID);
        CustomTable.SearchSWQL = string.Format(SearchSWQL, wlhm.WLHM.ID, "%${SEARCH_STRING}%");
    }

    public string SWQL
    {
        get
        {
            return @"
                SELECT ap.DisplayName AS AP_Name, ap.IPAddress AS AP_IP, COUNT(DISTINCT cl.ClientMACAddress) AS Clients_Count,
                '/Orion/View.aspx?NetObject=WLTAP:' + TOSTRING(ap.ID) AS [_LinkFor_AP_Name]
                FROM Orion.WirelessHeatMap.MapPoint AS p
                JOIN Orion.WirelessHeatMap.AccessPoints AS ap ON ap.ID = p.InstanceID AND p.EntityType LIKE '%AccessPoint%' AND p.MapID = {0}
                LEFT JOIN Orion.Packages.Wireless.Interfaces AS i ON i.AccessPointID=ap.ID
                LEFT JOIN Orion.WirelessHeatMap.ClientLocation AS cl ON i.WirelessClients.MAC=cl.ClientMACAddress AND cl.MapID=p.MapID
                GROUP BY ap.DisplayName, ap.IPAddress,ap.ID
                ORDER BY ap.DisplayName
            ";
        }
    }

    public string SearchSWQL
    {
        get
        {
            return @"
                SELECT ap.DisplayName AS AP_Name, ap.IPAddress AS AP_IP, ap.Clients AS Clients_Count,
                '/Orion/View.aspx?NetObject=WLTAP:' + TOSTRING(ap.ID) AS [_LinkFor_AP_Name]
                FROM Orion.WirelessHeatMap.MapPoint AS p
                JOIN Orion.WirelessHeatMap.AccessPoints AS ap ON ap.ID = p.InstanceID AND p.EntityType LIKE '%AccessPoint%' AND p.MapID = {0}
                WHERE ap.DisplayName LIKE '{1}' OR ap.IPAddress LIKE '{1}'
            ";
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceListofThinAccessPoints"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.WirelessHeatmapsWebContent.NPMWEBDATA_PD0_03; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IWLHMProvider) }; }
    }
}