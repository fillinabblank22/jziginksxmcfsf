﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WLHMThinAPList.ascx.cs" Inherits="Orion_WirelessHeatmaps_Resources_WLHMThinAPList" %>

<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<!-- This is needed if you want a search feature -->
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
 
 
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <!-- Include CustomQueryTable control in your resource -->
        <orion:CustomQueryTable runat="server" ID="CustomTable"/>
 
        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= CustomTable.UniqueClientID %>,
                        initialPage: 0,
                        rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "10" %>,
                        searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
                        searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
                        allowSort: true,
                        columnSettings:
                        {
                            "AP_Name": {
                                header:'<%=Resources.WirelessHeatmapsWebContent.NPMWEBDATA_VB0_116 %>' // Name
                            },
                            "AP_IP": {
                                header:'<%=Resources.WirelessHeatmapsWebContent.NPMWEBCODE_PD0_24 %>' // IP Address
                            },
                            "Clients_Count": {
                                header:'<%=Resources.WirelessHeatmapsWebContent.NPMWEBDATA_PD0_04 %>' // Clients Count
                            }
                        }
                    });
 
                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });    
        </script>
    </Content>
</orion:resourceWrapper>