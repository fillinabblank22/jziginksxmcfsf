﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WLHMList.ascx.cs" Inherits="Orion_WirelessHeatmaps_Resources_WLHMList" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%-- Custom Query Table Resource Tutorial: https://cp.solarwinds.com/x/qUhrAg --%>
<orion:Include ID="CSS_WLHM" runat="server" File="WirelessHeatmaps/styles/WirelessHeatMaps.css" />
<orion:resourceWrapper runat="server" ID="Wrapper" CssClass="sw-wlhm-maplist">
    <Content>
        <orion:CustomQueryTable runat="server" ID="CustomTable" />
        <script>
            $(function() {
                
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= CustomTable.UniqueClientID %>,
                        initialPage: 0,
                        rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "5" %>,
                        allowSort: true,
                        columnSettings:
                        {
                            "MapID": {
                                header:'',
                                formatter: function(value, row, cellInfo) {
                                    return '<div><img src="/Orion/WirelessHeatmaps/Resources/WLHM/WLHMThumbnailHandler.ashx?WLHM='+ value +'" alt="" /></div>';
                                },
                                cellCssClassProvider: function(value, row, cellInfo) {
                                    return 'sw-wlhm-maplist-thumbnail-column';
                                },
                                isHtml: true
                            },
                            "DisplayName": {
                                header: '<%=Resources.WirelessHeatmapsWebContent.NPMWEBCODE_PD0_41%>', // Name
                                cellCssClassProvider: function(value, row, cellInfo) {
                                    return 'sw-wlhm-maplist-bold-cell';
                                }
                            },
                            "AccessPoints": {
                                header:'<%=Resources.WirelessHeatmapsWebContent.NPMWEBCODE_PD0_42%>', // APs
                            },
                            "Clients": {
                                header: '<%=Resources.WirelessHeatmapsWebContent.NPMWEBDATA_TM0_82%>', // Clients
                            },
                            "LastGenerationFinished": {
                                header: '<%=Resources.WirelessHeatmapsWebContent.NPMWEBDATA_PD0_05%>', // Last Generated
                                isHtml: true,
                                formatter: function(value, row, cellInfo) {
                                    // LastGenerationFinished isn't set, leave value empty
                                    if (!value)
                                        return '';
									
									// row[6] returns an actual UTC date object
									// value contains just it's string representation
                                    var lastGenerationFinishedInUtc = row[6];
									
                                    // ErrorCode isn't set or is OK (0), just return the timestamp
                                    var timestamp = SW.Core.DateHelper.utcToTimeAgo(lastGenerationFinishedInUtc);
                                    if (!row[7] || row[7] === '0')
                                        return timestamp;

                                    // There was some error, show it together with timestamp (of previous attempt)
                                    var errorMessage = '<br /><div class="sw-suggestion sw-suggestion-fail sw-wlhm-maplist-error">';
                                    errorMessage += '<span class="sw-suggestion-icon"></span>';
                                    errorMessage += row[8];
                                    errorMessage += '</div>';
                                    return timestamp + errorMessage;
                                }
                            }
                        },
                        onLoad: function(rows, columns) {
                            if (rows.length == 0) {
                                $('#Grid-<%= CustomTable.UniqueClientID %>').hide();
                                //hide also the container containing the table (sw-custom-query-table-container)
                                $('#Grid-<%= CustomTable.UniqueClientID %>').parent().hide();
                                $('#wlhm_maplist-<%= Resource.ID %>').show();
                            } else {
                                $('#Grid-<%= CustomTable.UniqueClientID %> .HeaderRow .Sortable').first().removeClass('Sortable').off('click').empty();
                                $('#Grid-<%= CustomTable.UniqueClientID %>').parent().show();
                                $('#Grid-<%= CustomTable.UniqueClientID %>').show();
                                $('#wlhm_maplist-<%= Resource.ID %>').hide();
                            }
                        }
                    });
                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceId %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });
        </script>
        <div id="wlhm_maplist-<%= Resource.ID %>" style="display:none" class="sw-text-reset sw-wlhm-nomap">
            <div style="min-height:180px">
                <div>
                    <p><%=Resources.WirelessHeatmapsWebContent.NPMWEBDATA_PS0_4 %></p>
                    <ul>
                        <li><%=Resources.WirelessHeatmapsWebContent.NPMWEBDATA_PS0_5 %></li>
                        <li><%= String.Format(Resources.WirelessHeatmapsWebContent.NPMWEBDATA_PS0_6, @"<a href=""/NetworkAtlas/NetworkAtlas.exe"" class=""sw-link"">", "</a>")%></li>
                        <li><%=Resources.WirelessHeatmapsWebContent.NPMWEBDATA_PS0_7 %></li>
                    </ul>
                    <p><%=Resources.WirelessHeatmapsWebContent.NPMWEBDATA_PS0_10 %></p>
                </div>
            </div>
        </div>
    </Content>
</orion:resourceWrapper>
