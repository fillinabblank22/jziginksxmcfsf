﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Wireless.Heatmaps.Web;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_WirelessHeatmaps_Resources_WLHMClients : BaseResourceControl
{
    protected ResourceSearchControl SearchControl { get; private set; }

    protected int ScriptFriendlyResourceId
    {
        get { return Math.Abs(Resource.ID); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        Wrapper.HeaderButtons.Controls.Add(SearchControl);

        var wlhm = GetInterfaceInstance<IWLHMProvider>();
        CustomTable.UniqueClientID = ScriptFriendlyResourceId;
        CustomTable.SWQL = string.Format(SWQL, wlhm.WLHM.ID);
        CustomTable.SearchSWQL = string.Format(SearchSWQL, wlhm.WLHM.ID, "%${SEARCH_STRING}%");
    }

    // TODO: Query only clients located on the map
    public string SWQL
    {
        get
        {
            return @"
                SELECT c.ID AS _ClientID, c.MAC AS Client_MAC,
                CASE 
                  WHEN c.Name != '' THEN c.Name
                  ELSE c.IPAddress
                END AS Client_Name,
                ap.DisplayName AS AP_Name, 
                '/Orion/View.aspx?NetObject=WLTAP:' + TOSTRING(ap.ID) AS [_LinkFor_AP_Name],
                c.SignalStrength AS Client_SS
                FROM Orion.WirelessHeatMap.MapPoint AS p
                JOIN Orion.WirelessHeatMap.AccessPoints AS ap ON ap.ID = p.InstanceID AND p.EntityType LIKE '%AccessPoint%' AND p.MapID = {0}
                JOIN Orion.Packages.Wireless.Interfaces AS if ON if.AccessPointID = p.InstanceID 
                JOIN Orion.Packages.Wireless.Clients AS c ON c.InterfaceID = if.ID
                JOIN Orion.WirelessHeatMap.ClientLocation AS cl ON cl.ClientMACAddress = c.MAC AND cl.MapId=p.MapID
            ";
        }
    }

    public string SearchSWQL
    {
        get
        {
            return @"
                SELECT c.ID AS _ClientID, c.MAC AS Client_MAC,
                CASE 
                  WHEN c.Name != '' THEN c.Name
                  ELSE c.IPAddress
                END AS Client_Name,
                ap.DisplayName AS AP_Name, 
                '/Orion/View.aspx?NetObject=WLTAP:' + TOSTRING(ap.ID) AS [_LinkFor_AP_Name],
                c.SignalStrength AS Client_SS
                FROM Orion.WirelessHeatMap.MapPoint AS p
                JOIN Orion.WirelessHeatMap.AccessPoints AS ap ON ap.ID = p.InstanceID AND p.EntityType LIKE '%AccessPoint%' AND p.MapID = {0}
                JOIN Orion.Packages.Wireless.Interfaces AS if ON if.AccessPointID = p.InstanceID 
                JOIN Orion.Packages.Wireless.Clients AS c ON c.InterfaceID = if.ID
                JOIN Orion.WirelessHeatMap.ClientLocation AS cl ON cl.ClientMACAddress = c.MAC AND cl.MapId=p.MapID
                WHERE c.MAC LIKE '{1}' OR c.IPAddress LIKE '{1}' OR c.Name LIKE '{1}' OR ap.DisplayName LIKE '{1}'
            ";
        }
    }


    public override string HelpLinkFragment
    {
        get { return GetStringValue("HelpLinkFragment", "OrionPHWirelessHeatMapClients"); }
    }

    protected override string DefaultTitle
    {
        get { return Resources.WirelessHeatmapsWebContent.NPMWEBDATA_PS0_11; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { yield return typeof(IWLHMProvider); }
    }
}