﻿<%@ WebHandler Language="C#" Class="WLHMImageHandler" %>

using System;
using System.Activities.Expressions;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Web;
using SolarWinds.Orion.Common;

public class WLHMImageHandler : IHttpHandler {
    
    public void ProcessRequest (HttpContext context)
    {

        const string query = @"SELECT TOP 1 PercentProgress, MAP FROM Wireless_HeatMap WHERE ID = @ID";
        byte[] buffer = null;

        string wlhmId =  context.Request.QueryString["WLHM"];
        if (wlhmId == null)
        {
            throw new HttpException(400, "Invalid WLHM ID format");
        }
            

        using (var cmd = SqlHelper.GetTextCommand(query))
        {
            cmd.Parameters.AddWithValue("@ID", wlhmId);

            using (var reader = SqlHelper.ExecuteReader(cmd))
            {
                if (reader.Read())
                {
                    buffer = (byte[])reader["MAP"];

                    if (buffer == null || buffer.Length == 0)
                    {
                        context.Response.Status = "Image Not Found";
                        context.Response.StatusCode = 404;
                    }
                    else
                    {
                        if (reader.IsDBNull(0))
                            ReturnOriginalImage(context, buffer);
                        else
                            ReturnChangedImage(context, buffer, reader.GetByte(0));
                    }
                }

                reader.Close();
            }
        }
        
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

    private void ReturnOriginalImage(HttpContext context, byte[] buffer)
    {
        context.Response.Clear();
        context.Response.ContentType = "image/png";
        context.Response.BinaryWrite(buffer);
        context.Response.Flush();        
    }

    private void ReturnChangedImage(HttpContext context, byte[] buffer, byte progress)
    {
        Bitmap progressBar = CreateProgressBar(progress);
        Bitmap bitmap = MakeBWAndMerge(buffer, progressBar);

        ChangeColorPalette(bitmap);
            
        context.Response.Clear();
        context.Response.ContentType = "image/png";
        bitmap.Save(context.Response.OutputStream, ImageFormat.Png);
        context.Response.Flush();        
    }

    private Bitmap MakeBWAndMerge(byte[] buffer, Bitmap progressBar)
    {
        Bitmap bitmap;
        
        using (MemoryStream stream = new MemoryStream(buffer))
        {
            bitmap = new Bitmap(stream);

            // Lock the bitmap's bits.  
            Rectangle rect = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
            BitmapData bmpData = bitmap.LockBits(rect, ImageLockMode.ReadWrite, bitmap.PixelFormat);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            // Declare an array to hold the bytes of the bitmap. 
            int bytes = Math.Abs(bmpData.Stride) * bitmap.Height;
            byte[] rgbValues = new byte[bytes];

            // Copy values into the array.
            Marshal.Copy(ptr, rgbValues, 0, bytes);

            //chenge to B/W
            for (int y = 0; y < bitmap.Height; y++)
            {
                for (int x = 0; x < bitmap.Width; x++)
                {
                    rgbValues[(bmpData.Stride * y) + x] = (byte)((rgbValues[(bmpData.Stride * y) + x] != 0) ? 1 : 0);
                }
            }

            MergeBitmaps(rgbValues, progressBar, bmpData.Stride, bitmap.Width, bitmap.Height);
            
            // Copy the RGB values back to the bitmap
            Marshal.Copy(rgbValues, 0, ptr, bytes);

            // Unlock the bits.
            bitmap.UnlockBits(bmpData);
        }

        return bitmap;
    }

    private void ChangeColorPalette(Bitmap bitmap)
    {
        ColorPalette pal = bitmap.Palette;
        pal.Entries[0] = Color.FromArgb(192, 192, 192);
        pal.Entries[2] = Color.FromArgb(0, 0, 0);
        pal.Entries[3] = Color.FromArgb(0, 0, 192);
        bitmap.Palette = pal;
    }

    private Bitmap CreateProgressBar(byte progress)
    {
        Bitmap bitmap = new Bitmap(300, 70, PixelFormat.Format24bppRgb);
        Graphics g = Graphics.FromImage(bitmap);

        Font font = new Font("Arial", 10.0F, FontStyle.Regular);
        Pen blackPen = Pens.Black;
        Brush blackBrush = Brushes.Black;
        Brush blueBrush = Brushes.Blue;

        g.FillRectangle(Brushes.White, new Rectangle(0, 0, bitmap.Width, bitmap.Height));
        
        g.DrawRectangle(blackPen, new Rectangle(5, 5, 290, 60));

        SizeF size = g.MeasureString("Generating heat map in progress", font);
        g.DrawString("Generating heat map in progress", font, blackBrush, new PointF(((290.0F - size.Width)/2), 10.0F));

        if ((progress > 0) && (progress <= 100))
            g.FillRectangle(blueBrush, new Rectangle(50, 30, progress * 2, 20));
        
        g.DrawRectangle(blackPen, new Rectangle(50, 30, 200, 20));
        
        g.Flush();
        
        return bitmap;
    }
    
    private void MergeBitmaps(byte[] rgbValues, Bitmap progressBar, int stride, int width, int height)
    {
        Color pixelColor;
        int offset = (((height - progressBar.Height)/2)*stride) + ((width - progressBar.Width)/2);
        for (int y = 0; y < progressBar.Height; y++)
        {
            for (int x = 0; x < progressBar.Width; x++)
            {
                pixelColor = progressBar.GetPixel(x, y);

                if (pixelColor.ToArgb() == Color.White.ToArgb())
                {
                    rgbValues[(stride * y) + x + offset] = 1;
                    continue;
                }


                if (pixelColor.ToArgb() == Color.Blue.ToArgb())
                {
                    rgbValues[(stride * y) + x + offset] = 3;
                    continue;
                }

                rgbValues[(stride * y) + x + offset] = 2;
            }
        }
    }   
}
