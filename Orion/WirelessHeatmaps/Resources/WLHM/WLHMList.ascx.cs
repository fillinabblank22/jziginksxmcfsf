﻿using System;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_WirelessHeatmaps_Resources_WLHMList : BaseResourceControl
{
    protected int ScriptFriendlyResourceId
    {
        get { return Math.Abs(Resource.ID); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CustomTable.UniqueClientID = ScriptFriendlyResourceId;
        CustomTable.SWQL = SWQL;
    }

    public string SWQL
    {
        get
        {
            return @"
SELECT
map.MapID,
map.DisplayName,
'/Orion/View.aspx?NetObject=WLHM%3a' + TOSTRING(map.MapID) AS _LinkFor_DisplayName,
'/Orion/View.aspx?NetObject=WLHM%3a' + TOSTRING(map.MapID) AS _LinkFor_MapID,
(
  SELECT COUNT(point.PointID) AS pointsCount
  FROM Orion.WirelessHeatMap.MapPoint AS point
  WHERE point.EntityType LIKE '%AccessPoint%' AND point.MapID = map.MapID
) AS AccessPoints,
(
  SELECT COUNT(cl.Id) AS clientsSum
  FROM Orion.WirelessHeatMap.ClientLocation cl
  WHERE cl.MapID = map.MapID
) AS Clients,
map.LastGenerationFinished,
map.ErrorCode AS _ErrorCode,
code.Description AS _ErrorDescription
FROM Orion.WirelessHeatMap.Map AS map
LEFT JOIN Orion.WirelessHeatMap.ErrorCode AS code ON code.Code = map.ErrorCode
";
        }
    }

    public override string HelpLinkFragment
    {
        get { return GetStringValue("HelpLinkFragment", "OrionPHWirelessHeatMaps"); }
    }

    protected override string DefaultTitle
    {
        get { return Resources.WirelessHeatmapsWebContent.NPMWEBCODE_PD0_40; } // Wireless Heat Maps
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/WirelessHeatmaps/Controls/EditResourceControls/EditWirelessHeatMapsList.ascx";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.Ajax;
        }
    }
}