﻿using System;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Wireless.Heatmaps.Web;

public partial class Orion_WirelessHeatmaps_WLHMDetails : OrionView
{
    private static Log log = new Log();

    protected override void OnInit(EventArgs e)
    {
        this.mapID.WLHM = (WLHM)this.NetObject;
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnInit(e);
    }

    public override string ViewType
    {
        get { return "WLHMDetails"; }
    }
}