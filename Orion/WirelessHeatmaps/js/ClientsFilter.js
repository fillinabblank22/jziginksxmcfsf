﻿/// <reference path="../../js/OrionMinReqs.js/OrionMinReqs.js" />

/**
 * This object isn't testable. Test the used helper instead.
 */
SW.Core.namespace('SW.Wireless.HeatMaps').clientsFilter = (function (Helper) {

    var rid; // Orion ResourceID
    var h; // instance of SW.Wireless.HeatMaps.ClientsFilterHelper

    function activateFilterSwitching() {
        var $radios = $(h.controls.filterActivators);
        $radios.each(function () {
            $(this).off('change.switchFilter').on('change.switchFilter', function () {
                refreshFiltersVisibility();
            });
        });
    }

    function connectInputEnablersToInputs() {
        $(h.controls.countFilterInputEnabler).off('change.enableInput').on('change.enableInput', function () {
            if (this.checked)
                h.controls.countFilterInput.removeAttribute('disabled');
            else {
                h.controls.countFilterInput.setAttribute('disabled', 'true');
                h.controls.countFilterInput.value = '';
                clearErrors();
            }
        });
        $(h.controls.apFilterInputEnabler).off('change.enableInput').on('change.enableInput', function () {
            if (this.checked)
                h.controls.apFilterInput.removeAttribute('disabled');
            else {
                h.controls.apFilterInput.setAttribute('disabled', 'true');
                h.controls.apFilterInput.value = '';
                clearErrors();
            }
        });
    }

    function refreshFiltersVisibility() {
        h.hideFilters();
        var selectedType = h.findSelectedFilter();
        h.showFilter(selectedType);
    }

    /**
     * @param {int} mapId - 
     */
    function initNetObjectPicker(mapGuid) {
        // SW.Orion.NetObjectPicker.SetFilter("Vendor = '" + vendor + "'");
        // SW.Orion.NetObjectPicker.SetFilter("ObjectSubType = 'SNMP'");
        // SW.Orion.NetObjectPicker.GetSelectedEntities();

        // Setting custom titles

        // If some clients are already set, we should show them
        var preselectedEntities = [];
        var listFilterSelectedEntities = h.controls.listFilterCurrentValues.value;
        if (listFilterSelectedEntities) {
            preselectedEntities = JSON.parse(listFilterSelectedEntities).entities;
        }

        // Make only clients from APs on the map available
        if (mapGuid) {
            var filter = "InterfaceID IN (";
            filter += "SELECT i.ID FROM Orion.Packages.Wireless.Interfaces i ";
            filter += "INNER JOIN Orion.Packages.Wireless.AccessPoints ap ON ap.ID = i.AccessPointID ";
            filter += "INNER JOIN Orion.WirelessHeatMap.MapPoint mp ON mp.InstanceId = ap.ID ";
            filter += "INNER JOIN Orion.WirelessHeatMap.Map m ON m.MapID = mp.MapID ";
            filter += "WHERE m.MapStudioFileID = '" + mapGuid;
            filter += "')";
            //we need to take client with calculated positions
            filter += " AND MAC IN (SELECT ClientMACAddress FROM Orion.WirelessHeatMap.ClientLocation)";
            SW.Orion.NetObjectPicker.SetFilter(filter);
        }

        // Init
        SW.Orion.NetObjectPicker.init();
        SW.Orion.NetObjectPicker.SetUpEntities(
            [{ MasterEntity: 'Orion.Packages.Wireless.Clients' }],
            '', // Orion.Packages.Wireless.Clients
            preselectedEntities, // Pass array of swis uris of pre-selected entities
            'Multi',
            false
        );
        SW.Orion.NetObjectPicker.SetTitles('@{R=Wireless.Heatmaps.Strings; K=WEBJS_BI0_03; E=js}', '@{R=Wireless.Heatmaps.Strings; K=WEBJS_BI0_04; E=js}');
    }

    function init(resourceId) {
        rid = resourceId;
        h = new Helper(rid);
        activateFilterSwitching();
        connectInputEnablersToInputs();
        refreshFiltersVisibility();
    }

    /**
     * Used for ASP.NET CustomValidator. Cannot use the simpler
     * solution with RangeValidator, because I need custom
     * error message style (nested spans with 'sw' classes).
     */
    function validateCountFilterAmount(sender, args) {
        // In this case we choose network map. Because initialization of ClientFilter has been skipped
        // but CustomValidator calls this method always even if we choose network map
        if (!h)
            return;

        // Ignore validation when filter isn't selected
        if (h.findSelectedFilter() !== 'count')
            return;

        // user didn't chose to set value, skip validation
        if (!h.controls.countFilterInputEnabler.checked) {
            args.IsValid = true;
            return;
        }
        // checkbox selected, validate input
        var errorMessage = h.validateAmount(args.Value);
        if (errorMessage) {
            showCountFilterError(errorMessage);
            args.IsValid = false;
        } else {
            clearErrors();
            args.IsValid = true;
        }
    }

    /**
     * Used for ASP.NET CustomValidator. Cannot use the simpler
     * solution with RangeValidator, because I need custom
     * error message style (nested spans with 'sw' classes).
     */
    function validateApFilterAmount(sender, args) {
        // In this case we choose network map. Because initialization of ClientFilter has been skipped
        // but CustomValidator calls this method always even if we choose network map
        if (!h)
            return;

        // Ignore validation when filter isn't selected
        if (h.findSelectedFilter() !== 'ap')
            return;

        // user didn't chose to set value, skip validation
        if (!h.controls.apFilterInputEnabler.checked) {
            args.IsValid = true;
            return;
        }
        // checkbox selected, validate input
        var errorMessage = h.validateAmount(args.Value);
        if (errorMessage) {
            showApFilterError(errorMessage);
            args.IsValid = false;
        } else {
            clearErrors();
            args.IsValid = true;
        }
    }

    function showCountFilterError() {
        h.show(h.controls.countFilterError);
    }

    function showApFilterError() {
        h.show(h.controls.apFilterError);
    }

    function clearErrors() {
        h.hide(h.controls.countFilterError);
        h.hide(h.controls.apFilterError);
    }

    /**
     * This will read the filter settings and save the
     * results to hidden input fields if wlhm is selected.
     * WLHM selection is determined via another hidden
     * field. Its value is set during Show/Hide calls.
     */
    function parseSettings() {

        if (h.controls.filterEnabledField.value === "0")
            return; // Filter is hidden

        var selectedFilterType = h.findSelectedFilter();
        if (!selectedFilterType)
            return; // Throw? Error durring selection?

        var filterSettings = null;
        switch (selectedFilterType.toLowerCase()) {
            case 'count':
                {
                    filterSettings = parseCountFilter();
                    break;
                }
            case 'ap':
                {
                    filterSettings = parseApFilter();
                    break;
                }
            case 'list':
                {
                    filterSettings = parseListFilter();
                    break;
                }
            case 'none':
                {
                    // used for filter reset
                    filterSettings = new Settings(null, null, null, null);
                    break;
                }
        }

        if (!filterSettings)
            return; // TODO ... set defaults?

        h.controls.filterTypeField.value = selectedFilterType;
        h.controls.filterValueField.value = JSON.stringify(filterSettings);

    }

    function parseCountFilter() {
        if (!h.controls.countFilterInputEnabler.checked) {
            return new Settings(null, null, null, null);
        }
        var amount = h.controls.countFilterInput.value;
        return new Settings(amount, null, null, null);
    }

    function parseApFilter() {
        var dd, apMac, amount;

        // get AP
        dd = h.controls.apFilterDropDown;
        apMac = dd.options[dd.selectedIndex].value;

        // get amount
        if (!h.controls.apFilterInputEnabler.checked)
            amount = null;
        else
            amount = h.controls.apFilterInput.value;

        return new Settings(null, apMac, amount, null);
    }

    function parseListFilter() {
        var se = SW.Orion.NetObjectPicker.GetSelectedEntities();
        return new Settings(null, null, null, se);
    }

    // model (same as WirelessClientLimiationSettings.cs)
    function Settings(b, c, d, e) {
        this.ResourceId = rid;
        this.MapLimitationCount = b;
        this.APMACAddress = c;
        this.APLimitationCount = d;
        this.ClientLimitations = e;
    }

    return {
        init: init,
        initNetObjectPicker: initNetObjectPicker,
        validateCountFilterAmount: validateCountFilterAmount,
        validateApFilterAmount: validateApFilterAmount,
        parseSettings: parseSettings
    };

}(SW.Wireless.HeatMaps.ClientsFilterHelper));