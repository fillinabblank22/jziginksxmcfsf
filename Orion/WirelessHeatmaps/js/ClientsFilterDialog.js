﻿/* global SW:true */ // JSHint directive

// You can have multiple map resources at one page,
// => I need a different instace of dialog for each

SW.Core.namespace('SW.Wireless.HeatMaps').ClientsFilterDialog = (function () {

    // options
    // wrapperId: parent dom element
    // resourceId: integer id of Orion resource
    // onSuccess: callback, used after submit
    function CFD(options) {

        // I need to know wrapperId to identify the correct parent
        // and resourceId for saving the filter settings. Yes, it
        // could be unified.
        if (!options.wrapperId)
            throw 'wrapperId parameter has to be passed to ClientFilterDialog constructor';
        if (!options.resourceId)
            throw 'resourceId parameter has to be passed to ClientFilterDialog constructor';
        if (!SW.Wireless.HeatMaps.ClientsFilterDialog.helper.isInteger(options.resourceId))
            throw 'resourceId parameter has to be a number';

        this.selector = '#' + options.wrapperId;
        this.resourceId = parseInt(options.resourceId, 10);

        // I am going to work with included DOM elements at
        // different places. Better keep the selectors at
        // one place.
        this.selectors = {
            filterType: '.sw-wlhm-cf-type',
            filterActivator: '.sw-wlhm-cf-type input[type="radio"]',
            filterWrapper: '.sw-wlhm-cf-wrapper',
            countCheckbox: '#wlhmcfCountCheckbox',
            countInput: '#wlhmcfCountInput',
            apDropdown: '#wlhmcfApSelector',
            apCountCheckbox: '#wlhmcfApCountCheckbox',
            apCountInput: '#wlhmcfApCountInput',
            errorPlaceholder: '#wlhmcfErrorMessage',
            submitButton: '#wlhmcfSubmit',
            cancelButton: '#wlhmcfCancel',
            loadingOverlay: '.sw-wlhm-cf-loadingOverlay',
            errorMessage: '.sw-suggestion-fail',
            errorMessageInnerText: '.sw-suggestion-error-text'
        };

        // I need to access the inner controls multiple times,
        // so I am going to get their references here
        // TODO: rename selectors to match these new names
        this.controls = {
            countFilterAmountCheckbox: $(this.selector + ' ' + this.selectors.countCheckbox),
            countFilterAmountInput: $(this.selector + ' ' + this.selectors.countInput),
            apFilterDropdown: $(this.selector + ' ' + this.selectors.apDropdown),
            apFilterAmountCheckbox: $(this.selector + ' ' + this.selectors.apCountCheckbox),
            apFilterAmountInput: $(this.selector + ' ' + this.selectors.apCountInput),
            loadingOverlay: $(this.selector + ' ' + this.selectors.loadingOverlay)
        };

        this.errorBorderClass = 'sw-wlhm-error-border';
        this.visibleClass = 'sw-wlhm-cf-visible';
        this.$filterActivators = $(this.selector + ' ' + this.selectors.filterActivator);
        this.$filterWrappers = $(this.selector + ' ' + this.selectors.filterWrapper);
        this.selectedType = null;

        // Reference needed  because of dynamic 'this'
        var me = this;

        // Init the jQuery UI dialog
        this.$dlg = $(this.selector);
        this.$dlg.dialog({
            autoOpen: false,
            modal: true,
            width: 'auto',
            title: '@{R=Wireless.Heatmaps.Strings; K=WEBJS_PD0_01; E=js}', // Select Wireless Clients to Display
            open: function () {
                me.resetControls();
            },
            close: function () {
            }
        });

        // Attach event handlers to activators (radio buttons)
        // Show matching wrapper, hide others
        this.$filterActivators.each(function () {
            $(this).change(function () {
                hideFilters();
                me.findSelectedType();
                me.showFilterForSelectedType();
            });
        });

        // Adding these methods directly in constructor (so each instance has one),
        // because I need them in both constructor and methods added via prototype
        this.findSelectedType = function () {
            var $selectedActivator = this.$filterActivators.filter(':checked');
            if ($selectedActivator.length === 0)
                return null;
            var selectedType = $selectedActivator.closest(this.selectors.filterType).attr('data-filtertype');
            this.selectedType = selectedType;
            return this.selectedType;
        };
        this.showFilterForSelectedType = function () {
            var $filterWrapper = $(this.selector + ' ' + this.selectors.filterType + '[data-filtertype="' + this.selectedType + '"] ' + this.selectors.filterWrapper);
            $filterWrapper.addClass(this.visibleClass);
        };

        // Count input has to be enabled only when checkbox is checked
        var $countCheckbox = $(this.selector + ' ' + this.selectors.countCheckbox);
        var $countInput = $(this.selector + ' ' + this.selectors.countInput);
        $countCheckbox.on('change', function () {
            me.hideError(me.selectors.countInput);
            $countInput.attr('disabled', !this.checked);
        });

        // AccessPoint count input has to be enabled only when checkbox is checked
        var $apCountCheckbox = $(this.selector + ' ' + this.selectors.apCountCheckbox);
        var $apCountInput = $(this.selector + ' ' + this.selectors.apCountInput);
        $apCountCheckbox.on('change', function () {
            me.hideError(me.selectors.apCountInput);
            $apCountInput.attr('disabled', !this.checked);
        });

        // Attaching the event handler to submit button
        // Figure out, what filter is selected
        // Parse needed settings and submit
        $(this.selector + ' ' + this.selectors.submitButton).on('click', function (e) {
            e.preventDefault();
            switch (me.selectedType) {
                case 'count': {
                    if ($countCheckbox.is(':checked')) {
                        var settings = me.parseSettings('count');
                        if (settings) {
                            me.submit(settings, function () {
                                me.close();
                                if (options.onSuccess)
                                    options.onSuccess();
                            });
                        }
                    }
                    else {
                        // When user submits the filter without checkbox
                        // we should treat it as a request for filter reset.
                        var settings = {
                            ResourceId: me.resourceId,
                            MapLimitationCount: null,
                            APMACAddress: null,
                            APLimitationCount: null,
                            ClientLimitations: null
                        };
                        me.submit(settings, function () {
                            me.close();
                            if (options.onSuccess)
                                options.onSuccess();
                        });
                    }
                    break;
                }
                case 'accesspoint': {
                    var settings = me.parseSettings('accesspoint');
                    if (settings) {
                        me.submit(settings, function () {
                            me.close();
                            if (options.onSuccess)
                                options.onSuccess();
                        });
                    }
                    break;
                }
                default: {
                    return null;
                }
            }

        });

        // Close button will close the dialog when clicked
        $(this.selector + ' ' + this.selectors.cancelButton).on('click', function (e) {
            e.preventDefault();
            me.close();
        });

        // I need to hide all filters but the selected one.
        // I'll hide them all and then make one visible.
        var hideFilters = function () {
            me.$filterWrappers.removeClass(me.visibleClass);
        };
    };

    CFD.prototype.open = function () {
        this.$dlg.dialog('open');
    };

    CFD.prototype.close = function () {
        this.$dlg.dialog('close');
    };

    CFD.prototype.submit = function (filterSettings, onSuccessAction) {

        if (!filterSettings)
            throw 'Cannot submit the clients filter settings without input data.';

        SW.Core.Services.callControllerAction(
                "/api/WirelessClients",
                "StoreClientLimitationSettings",
                filterSettings,
                function () {
                    if (onSuccessAction)
                        onSuccessAction();
                });
    };

    CFD.prototype.parseSettings = function (type) {
        // read the values from DOM, based on type
        // return the settings object
        var message;
        switch (type) {
            case 'count': {
                var count = this.getValueFromInput(this.selectors.countInput);
                message = this.checkValue(count);
                if (message) {
                    this.showError(message, this.selectors.countInput);
                    return null;
                } else {
                    this.hideError(this.selectors.countInput);
                }

                return {
                    ResourceId: this.resourceId,
                    MapLimitationCount: parseInt(count, 10),
                    APMACAddress: null,
                    APLimitationCount: null,
                    ClientLimitations: null
                };
            }
            case 'accesspoint': {
                // get the MAC of selected AP
                var $apDropdown = $(this.selector + ' ' + this.selectors.apDropdown);
                var selectedAp = $apDropdown.val();
                if (!selectedAp)
                    break;

                var $apCountCheckbox = $(this.selector + ' ' + this.selectors.apCountCheckbox);
                var apCount = null;
                if ($apCountCheckbox.is(':checked')) {
                    // get the count
                    apCount = this.getValueFromInput(this.selectors.apCountInput);
                    //check if 'count' is correct value
                    message = this.checkValue(apCount);
                    if (message) {
                        this.showError(message, this.selectors.apCountCheckbox);
                        return null;
                    } else {
                        this.hideError(this.selectors.apCountCheckbox);
                    }

                }

                return {
                    ResourceId: this.resourceId,
                    MapLimitationCount: null,
                    APMACAddress: selectedAp,
                    APLimitationCount: parseInt(apCount, 10),
                    ClientLimitations: null
                };
                break;
            }
            default: {
                return null;
            }
        }
    };

    CFD.prototype.selectActivator = function (type) {
        var selector = this.selector;
        selector += ' ' + this.selectors.filterType;
        selector += '[data-filtertype="' + type + '"]';
        selector += ' input[type="radio"]';
        var $activator = $(selector);
        $activator.trigger('click');
        $activator.focus();
    };

    CFD.prototype.clearControls = function () {
        this.controls.countFilterAmountCheckbox.attr('checked', false);
        this.controls.countFilterAmountInput.val('');
        this.controls.countFilterAmountInput.attr('disabled', true);
        this.controls.apFilterDropdown.val('');
        this.controls.apFilterAmountCheckbox.attr('checked', false);
        this.controls.apFilterAmountInput.val('');
        this.controls.apFilterAmountInput.attr('disabled', true);
    };

    CFD.prototype.fillControls = function (settings) {
        // count filter should be enabled if response knows the MapLimitationCount
        // = check the checkbox, prefill value to input and enable it
        if (settings.MapLimitationCount && settings.MapLimitationCount > 0) {
            this.controls.countFilterAmountCheckbox.attr('checked', true);
            this.controls.countFilterAmountInput.val(settings.MapLimitationCount);
            this.controls.countFilterAmountInput.attr('disabled', false);
            this.selectedType = 'count';
            this.selectActivator('count');
            return true;
        }
        // If the AP filter was set, preselect AP
        if (settings.APMACAddress) {
            this.controls.apFilterDropdown.val(settings.APMACAddress);
            // Also need to check if the amount was limited for AP
            if (settings.APLimitationCount && settings.APLimitationCount > 0) {
                this.controls.apFilterAmountCheckbox.attr('checked', true);
                this.controls.apFilterAmountInput.val(settings.APLimitationCount);
                this.controls.apFilterAmountInput.attr('disabled', false);
            }
            this.selectedType = 'accesspoint';
            this.selectActivator('accesspoint');
            return true;
        }
        // There is no limitation yet
        this.selectedType = 'count';
        this.selectActivator('count');
    };

    CFD.prototype.resetControls = function () {
        this.hideError(this.selectors.countInput);
        this.hideError(this.selectors.apCountInput);
        this.clearControls();
        this.controls.loadingOverlay.show();
        var me = this;
        $.ajax({
            url: '/api/WirelessClients/GetClientLimitationSettings',
            data: { resourceId: this.resourceId },
            success: function (response) {
                me.fillControls(response);
                me.showFilterForSelectedType();
                me.controls.loadingOverlay.hide();
            },
            error: function (xhr, status, errorThrown) {
                SW.Core.Services.handleError(xhr, onError);
            },
            contentType: "application/json; charset=utf-8",
            dataType: 'json'
        });
    };

    //get value from the checkbox
    //{inputSelector} - current checkbox 
    CFD.prototype.getValueFromInput = function (inputSelector) {
        var $countInput = $(this.selector + ' ' + inputSelector);
        if ($countInput.length === 0)
            throw 'Failed to locate the count filter value input.';
        var count = $countInput.val();
        return count;
    };

    //we want to check if value is correct and return
    // message == null - value is corrert
    // message != null - value is wrong
    CFD.prototype.checkValue = function (value) {
        var message = null;
        if (!value) {
            message = '@{R=Wireless.Heatmaps.Strings; K=WEBJS_PD0_02; E=js}'; // Value not present.
        } else if (!SW.Wireless.HeatMaps.ClientsFilterDialog.helper.isInteger(value) || value < 1 || value > 100) {
            message = '@{R=Wireless.Heatmaps.Strings; K=WEBJS_PD0_03; E=js}'; // Entered value is not valid.
        }
        return message;
    };

    CFD.prototype.showError = function (message, inputSelector) {
        var $countInput = $(this.selector + ' ' + inputSelector);
        //finding parent to identify needed alert message div and alert label.
        //We have the same messages for inputcount and apinputcount
        var $parentDiv = $countInput.closest(this.selectors.filterWrapper);
        var $errorMessage = $parentDiv.find(this.selectors.errorMessage);
        var $errorMessageInnerText = $errorMessage.find(this.selectors.errorMessageInnerText);
        $errorMessageInnerText.text(message);
        $errorMessage.show();
        $countInput.addClass(this.errorBorderClass);
    };

    CFD.prototype.hideError = function (inputSelector) {
        var $countInput = $(this.selector + ' ' + inputSelector);
        var $parentDiv = $countInput.closest(this.selectors.filterWrapper);
        var $errorMessage = $parentDiv.find(this.selectors.errorMessage);
        $countInput.removeClass(this.errorBorderClass);
        $errorMessage.hide();
    };

    return CFD;

}());