﻿SW.Core.namespace('SW.Wireless.HeatMaps').ClientsFilterHelper = (function () {

    /**
	* Represents a ClientsFilterHelper
	* @constructor
	* @param {int} resourceId - Orion ResourceID. Used for precise DOM selection.
	* @param {object} config - Optional object containing overrides for default properties. [selector,selectors...]
	*/
    function helper(resourceId, config) {
        this.rid = resourceId;
        this.selector = config && config.selector || '.sw-wlhm-cf[data-rid="' + this.rid + '"]';
        this.selectors = config && config.selectors || {
            filterType: '.sw-wlhm-cf-type',
            filterActivator: '.sw-wlhm-cf-activator',
            filterBody: '.sw-wlhm-cf-body',
            filterEnabledField: 'input[id*="wlhmcfFilterEnabled"][type="hidden"]',
            filterTypeField: 'input[id*="wlhmcfFilterType"][type="hidden"]',
            filterValueField: 'input[id*="wlhmcfFilterValue"][type="hidden"]',
            countFilterInputEnabler: 'input[id*="countFilterInputEnabler"]',
            countFilterInput: 'input[id*="countFilterInput"][type="text"]',
            countFilterError: '.sw-suggestion[id*="countFilterError"]',
            countFilterErrorText: '.sw-suggestion[id*="countFilterError"] .sw-wlhm-cf-errorText',
            apFilterDropDown: 'select[id*="apFilterDropDown"]',
            apFilterInputEnabler: 'input[id*="apFilterInputEnabler"]',
            apFilterInput: 'input[id*="apFilterInput"][type="text"]',
            apFilterError: '.sw-suggestion[id*="apFilterError"]',
            apFilterErrorText: '.sw-suggestion[id*="apFilterError"] .sw-wlhm-cf-errorText',
            listFilterCurrentValues: 'input[id*="wlhmcfListFilterCurrentValues"][type="hidden"]'
        };
        this.controls = {
            filterActivators: document.querySelectorAll(cs(this.selector, this.selectors.filterActivator)),
            filterBodies: document.querySelectorAll(cs(this.selector, this.selectors.filterBody)),
            filterEnabledField: document.querySelector(cs(this.selector, this.selectors.filterEnabledField)),
            filterTypeField: document.querySelector(cs(this.selector, this.selectors.filterTypeField)),
            filterValueField: document.querySelector(cs(this.selector, this.selectors.filterValueField)),
            countFilterInputEnabler: document.querySelector(cs(this.selector, this.selectors.countFilterInputEnabler)),
            countFilterInput: document.querySelector(cs(this.selector, this.selectors.countFilterInput)),
            countFilterError: document.querySelector(cs(this.selector, this.selectors.countFilterError)),
            countFilterErrorText: document.querySelector(cs(this.selector, this.selectors.countFilterErrorText)),
            apFilterDropDown: document.querySelector(cs(this.selector, this.selectors.apFilterDropDown)),
            apFilterInputEnabler: document.querySelector(cs(this.selector, this.selectors.apFilterInputEnabler)),
            apFilterInput: document.querySelector(cs(this.selector, this.selectors.apFilterInput)),
            apFilterError: document.querySelector(cs(this.selector, this.selectors.apFilterError)),
            apFilterErrorText: document.querySelector(cs(this.selector, this.selectors.apFilterErrorText)),
            listFilterCurrentValues: document.querySelector(cs(this.selector, this.selectors.listFilterCurrentValues)),
        };
    }

    helper.prototype.hideFilters = function () {
        for (var i = 0; i < this.controls.filterBodies.length; i++) {
            this.hide(this.controls.filterBodies[i]);
        }
    };

    helper.prototype.findSelectedFilter = function () {
        for (var i = 0; i < this.controls.filterActivators.length; i++) {
            if (this.controls.filterActivators[i].checked) {
                return this.controls.filterActivators[i].getAttribute('data-cftype');
            }
        }
        return 'none';
    };

    helper.prototype.showFilter = function (type) {
        var bodySelector = cs(this.selector, this.selectors.filterBody) + '[data-cftype="' + type + '"]';
        var selectedFilter = document.querySelector(bodySelector);
        this.show(selectedFilter);
    };

    helper.prototype.show = function(element) {
        element.style.display = 'block';
    };

    helper.prototype.hide = function(element) {
        element.style.display = 'none';
    };

    /**
     * Used to validate amount entered by user.
     * Returns error message or nothing when the value is ok.
     */
    helper.prototype.validateAmount = function(amount) {
        if (amount === null || typeof amount === 'undefined' || amount === '')
            return 'Value cannot be empty.';
        if (!this.isInteger(amount))
            return 'Value must be digit.';
        if (amount < 1)
            return 'Value cannot be less than 1.';
        if (amount > 100)
            return 'Value cannot be bigger than 100.';
        return '';
    };

    /**
     * Checks if the value is integer.
     */
    helper.prototype.isInteger = function(n) {
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/parseInt
        if (/^(\-|\+)?([0-9]+|Infinity)$/.test(n))
            return true;
        return false;
    };

    /**
	* 'Concat Selectors'. Used to join multiple selectors into one.
	*/
    function cs() {
        if (arguments.length === 0)
            throw 'Nothing to concat';
        var selector = arguments[0];
        for (var i = 1; i < arguments.length; i++) {
            selector += ' ' + arguments[i];
        }
        return selector;
    }

    return helper;

}());
