﻿/* global SW:true */ // JSHint directive
SW.Core.namespace('SW.Wireless.HeatMaps.ClientsFilterDialog').helper = (function () {
    return {
        isNumber: function (n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        },
        isInteger: function (n) {
            // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/parseInt
            if (/^(\-|\+)?([0-9]+|Infinity)$/.test(n))
                return Number(n);
            return false;
        }
    };
}());