﻿

SW.Core.namespace("SW.Wireless.HeatMaps").ClientLocationInfo = (function () {

    /*
     * mapId - {int} id of the wireless map
     * showWirelessClients - {bool} current state of visibility client info box
     * mapClientsDiv - {div} contains all client map information
     * checkBox - {input} toggles visibility of mapClientsDiv
     * resourceId - {int} resource ID 
     * alertMessageDiv - {div} contains yellow warning message
     * invisibleClientSpan - {span} contains message and icon which could be hidden depends on data received from server
     */
    function CLI(options) {

        if (!options)
            throw 'Missing options';
        this.resourceId = options.resourceId;
        this.showWirelessClients = options.showWirelessClients;
        this.$resourceWrapper = $('.ResourceWrapper[resourceid=' + this.resourceId + ']');
        this.mapId = $('#networkMap-' + this.resourceId).attr('data-wlhmMap');
        this.mapGuid = $('#networkMap-' + this.resourceId).attr('data-mapGuid');
        this.$mapClientsDiv = this.$resourceWrapper.find('div.sw-wlhm-map-client-info');
        this.$checkBox = this.$resourceWrapper.find('#' + options.checkboxId);
        this.$alertMessageDiv = this.$resourceWrapper.find('div.sw-wlhm-clients-alert-message');
        this.$clientBox = this.$resourceWrapper.find('#' + options.clientboxId);
        this.$mapSuggestionDiv = this.$resourceWrapper.find('div.sw-wlhm-map-client-suggestion');

        this.selectors = {
            filterInfoWrapper: '.sw-wlhm-filter-info-wrapper',
            filterInfoText: '.sw-wlhm-filter-info-text',
            eyeImg: '.sw-wlhm-icon-outside-clients',
            invisClientInfoSpan: '#InvisibleClientInfoSpan'
        };

        this.processClientNames = function (clientNames) {
            var nameList = '<span class="sw-wlhm-client-outside-tooltip">@{R=Wireless.Heatmaps.Strings; K=WEBDATA_BI_05; E=js}</span>';
            $.each(clientNames, function (index, name) {
                nameList += '<br>' + name;
            });

            return '<div class="sw-wlhm-client-outside-tooltip-outer">' + nameList + '</div>';
        };
    }

    //showing applied filter to current client info
    CLI.prototype.fillFilterInfo = function (settings) {
        var $filterInfoWrapper = this.$clientBox.find(this.selectors.filterInfoWrapper);
        var $filterInfoText = this.$clientBox.find(this.selectors.filterInfoText);

        if (settings.APMACAddress) {
            $filterInfoText.text('@{R=Wireless.Heatmaps.Strings; K=WEBJS_BI0_01; E=js}');// showing only clients connected to a specific AP
            $filterInfoWrapper.show();
        } else if ((settings.MapLimitationCount && settings.MapLimitationCount > 0) || settings.ClientLimitations.length > 0) {
            $filterInfoText.text('@{R=Wireless.Heatmaps.Strings; K=WEBJS_BI0_02; E=js}');// showing only some clients
            $filterInfoWrapper.show();
        } else {
            $filterInfoWrapper.hide();
        }
    };

    CLI.prototype.refreshFilterInfo = function () {
        var me = this;
        $.ajax({
            url: '/api/WirelessClients/GetClientLimitationSettings',
            data: { resourceId: this.resourceId, mapGUID: this.mapGuid },
            success: function (response) {
                me.fillFilterInfo(response);
            },
            contentType: "application/json; charset=utf-8",
            dataType: 'json'
        });
    };

    CLI.prototype.processClientStringsInfo = function (data) {
        this.$mapClientsDiv.find('span[data-target="visClients"]').html(data.VisibleClientsCount);
        this.$mapClientsDiv.find('span[data-target="allClients"]').html(data.TotalClientsCount);
        this.$mapClientsDiv.find('span[data-target="invisClients"]').html(data.InvisibleClientsCount);
    };

    CLI.prototype.initClientStrings = function () {
        var clientText = this.$mapClientsDiv.find('p').html();
        clientText = clientText.replace(/\{visClients\}/g, '<span data-target="visClients"></span>');
        clientText = clientText.replace(/\{allClients\}/g, '<span data-target="allClients"></span>');
        clientText = clientText.replace(/\{invisClients\}/g, '<span data-target="invisClients"></span>');
        this.$mapClientsDiv.find('p').html(clientText);
    };

    CLI.prototype.init = function () {
        var me = this;

        me.initClientStrings();


        this.$checkBox.on("change", function (e) {
            if (this.checked) {
                me.show();
                me.refreshFilterInfo();
            } else {
                me.hide();
            }
        });

        if (me.showWirelessClients || me.$checkBox.is(":checked")) {
            me.show();
            me.refreshFilterInfo();
        } else {
            me.hide();
        }
    };

    CLI.prototype.show = function () {
        this.$mapClientsDiv.show();
    };

    CLI.prototype.hide = function () {
        this.$mapClientsDiv.hide();
    };

    CLI.prototype.refresh = function (data) {
        var me = this;


        if (me.$checkBox.is(":checked") && data.TotalClientsCount == 0) {
            this.$mapSuggestionDiv.show();
        }
        else
            this.$mapSuggestionDiv.hide();

        me.processClientStringsInfo(data);
        if (data.InvisibleClientsCount > 0) {
            me.$mapClientsDiv.find(me.selectors.eyeImg).attr('title', me.processClientNames(data.InvisibleClientNames)).tooltip({ showURL: false });
            me.$resourceWrapper.find(me.selectors.invisClientInfoSpan).show();
        } else {
            me.$resourceWrapper.find(me.selectors.invisClientInfoSpan).hide();
        }

        if (data.TotalClientsCount < 101)
            me.$alertMessageDiv.hide();
        else {
            me.$alertMessageDiv.show();
        }
    };

    return CLI;

}());