﻿/// <reference path="../../js/OrionMinReqs.js/OrionMinReqs.js" />



$.widget("SolarWinds.wirelessClientsOverlayer", {

    references: {
        showClientsCheckbox: null
    },

    options: {
        showWirelessClients: false,
        isLoaded: false,
        mapBaseX: 0,
        mapBaseY: 0,
        scale: 1,
        width: 0,
        height: 0,
        // represents the width and height of the image used as pin point
        pointBox: {
            width: 23,
            height: 24
        },
        // represents offset of the pin point so the point image shows the original location of the point.
        pointOffset: {
            left: -8,
            top: -23
        },
        templates: {
            //turning off clicking on a mappoint to prevent leaping page to the start(top part)
            mappoint: "<a href='#' class='sw-wlhm-map-point' onclick='return false'><img src='/Orion/WirelessHeatmaps/Images/mappin.png' height='24' width='23' /></a>",
            tooltipLoading: "<div style='height: 60px; padding-top: 25px;'><center><img src='/Orion/images/AJAX-Loader.gif' />&nbsp;@{R=Core.Strings; K=WEBJS_VB0_1;E=js}</center></div>",
        }
    },

    _pointFactory: function (x, y) {
        var point = $(this.options.templates.mappoint);
        point.css("left", x + this.options.mapBaseX);
        point.css("top", y + this.options.mapBaseY);
        return point;
    },

    _initHover: function (point) {
        var self = this;
        var $tooltipText = point.data('tooltip');
        var tempTooltipText = $tooltipText;
        var tooltiptext = $tooltipText.find("div.NetObjectTipBody").html();
        var tooltipParameters = JSON.parse(tooltiptext);

        var tooltipDivId = 'mapPoint-' + this.options.ResourceId + '-' + point.data('id');

        point.on("mouseover", function (event) {
            var elControl = this;

            var loadingTooltip = $(elControl).data("loadingTooltip");
            if (loadingTooltip) {
                return;
            }

            $(elControl).data("loadingTooltip", true);
            var $tooltip = $("<div id='" + tooltipDivId + "'></div>");
            var loadingTooltipTextTemplate = self.options.templates.tooltipLoading;
            $.swtooltip(elControl, $tooltip.append($(tempTooltipText).find("div.NetObjectTipBody").empty().append(loadingTooltipTextTemplate).parent()));
            $(elControl).trigger("mouseover");

            var url = "/Orion/NetPerfMon/MapPointTooltip.aspx";

            $.ajax({
                url: url,
                type: "POST",
                cache: false,
                data: tooltipParameters,
                dataType: "html"
            }).done(function (returnedData, textStatus, xhr) {
                $(elControl).data("loadingTooltip", false);
                if (!returnedData.trim().startsWith("<!DOCTYPE html PUBLIC")) {
                    $(elControl).off(event); // will unbind current event handler
                    var $tempTooltipText = $("<div />").append($(tempTooltipText));
                    var $returnedData = $("<div />").append($(returnedData));
                    var $returnedDataStatusEl = $returnedData.find("statusinfo");
                    if ($returnedDataStatusEl.length > 0) {
                        $tempTooltipText.find("h3").removeClass();
                        $tempTooltipText.find("h3").addClass($returnedDataStatusEl.attr("status"));
                    }

                    tempTooltipText = $tempTooltipText.find("div.NetObjectTipBody").empty().append($(returnedData)).parent();
                    $tooltip.empty().append(tempTooltipText);
                } else { // user was most likely log out and we get data for login page
                    var errorTextTemplate = $(tempTooltipText).find("div.NetObjectTipBody").empty().append(String.format('<table style="width:270px;"><tr><td style="height:40px;vertical-align:middle;"><i>{0}</i><td><tr><table>', '@{R=Core.Strings; K=WEBJS_VB1_4; E=js}')).parent();
                    $tooltip.empty().append(errorTextTemplate);
                }

            }).fail(function (err) {
                $(elControl).data("loadingTooltip", false);
                var $tempTooltipText = $("<div />").append($(tempTooltipText));
                var errorTextTemplate = $tempTooltipText.find("div.NetObjectTipBody").empty().append(String.format('<table style="width:270px;"><tr><td style="height:40px;vertical-align:middle;"><i>{0}</i><td><tr><table>', '@{R=Core.Strings; K=WEBJS_VB1_4; E=js}')).parent();
                $tooltip.empty().append(errorTextTemplate);
            });
        });
    },

    _tooltipFactory: function (id) {
        var data = { TooltipFor: 'MapPoint', EntityName: 'Orion.Wireless.Clients', EntityKey: id };
        var tooltip = $('<tooltip />');

        tooltip.append($('<h3 />').addClass('StatusUp').text('@{R=Wireless.Heatmaps.Strings; K=WEBJS_PS0_1; E=js}'));
        tooltip.append($('<div />').addClass('NetObjectTipBody').text(JSON.stringify(data)));

        return tooltip;
    },

    _addPoint: function (point, id) {
        point.data("id", id);
        point.data("tooltip", this._tooltipFactory(id));
        this._initHover(point);
        this.element.append(point);

    },

    _addPointAtPosition: function (point) {

        var pointPosition = {
            left: point.X * this.options.scale + this.options.mapBaseX,
            top: point.Y * this.options.scale + this.options.mapBaseY
        };

        this._addPoint(this._pointFactory(pointPosition.left + this.options.pointOffset.left, pointPosition.top + this.options.pointOffset.top), point.Id);
        
    },

    storeSettings: function (showClients) {
        var input = {
            ResourceId: this.options.ResourceId,
            ShowClients: showClients
        };

        SW.Core.Services.callControllerAction(
            "/api/WirelessClients",
            "StoreMapSettings",
            input,
            function () {
                // setting saved
            });
    },

    _createPoints: function (data) {
        var self = this;
        _.each(data, function (point) { self._addPointAtPosition(point); });
    },

    _init: function () {

        var self = this;
        var coords;
        this.options.MapId = self.element.attr("data-wlhmMap");
        var id = self.element.attr("id");
        this.options.ResourceId = id.substring(id.lastIndexOf("-") + 1);
        this.options.TimestampDiv = $('#LastCalculatedClientsPositionsTimestampOuter-' + this.options.ResourceId);

        // find anchor point
        var point = $(self.element).find("area[title^='MapAnchorPoint&']");
        if (point.length === 1) {
            coords = $(point[0]).attr('coords').split(',');
            //actually i dont know why, but when we have some padding left on the map
            //base x/y for all points doubles. it is fotfix
            //ToDo: to find out what is wrong in map control.
            this.options.mapBaseX = parseInt(coords[0]) / 2;
            this.options.mapBaseY = parseInt(coords[1]) / 2;
            var title = point.attr('title');
            var ampIndex = title.indexOf('&');
            var pipeIndex = title.indexOf('|');
            if (ampIndex > 0 && pipeIndex > 0) {

                var scale = parseInt(title.substring(ampIndex + 1, pipeIndex));
                this.options.scale = scale / 100.0;
            }
            point.attr('title', '');
        }

        // find point at bottom right of map
        point = $(self.element).find("area[title='MapSizePoint|']");
        if (point.length === 1) {
            coords = $(point[0]).attr('coords').split(',');
            this.options.width = parseInt(coords[0]);
            this.options.height = parseInt(coords[1]);
        }

        // find checkbox and attach event handler to it (show/hide clients)
        var wrapper = this.element.closest(".ResourceWrapper");
        var checkbox = wrapper.find("input[type=checkbox].showClients");

        this.references.showClientsCheckbox = checkbox;
        checkbox.on("change", function (e) {

            //need to pass value from here. Because with multiple Map resources checkbox can show wrong value
            self.storeSettings(e.target.checked);

            if (e.target.checked) {
                checkbox.attr('checked', 'checked');
                self.show();
            } else {
                self.hide();
                checkbox.removeAttr('checked');
            }
        });

        checkbox.siblings("label").on("click", function (e) {
            checkbox.trigger('click');
        });

        if (this.options.showWirelessClients || checkbox.is(":checked")) {
            this.show();
        } else {
            this.hide();
        }
    },

    reload: function (data) {

        this._clear();
        this.options.isLoaded = true;

        this._createPoints(data.ClientsPositions);
    },

    show: function () {
        this.options.TimestampDiv.show();
        if (this.options.isLoaded) {
            this.element.find(".sw-wlhm-map-point").show();
        }
    },

    hide: function () {
        this.element.find(".sw-wlhm-map-point").hide();
        this.options.TimestampDiv.hide();
    },

    _clear: function () {
        this.element.find(".sw-wlhm-map-point").remove();
        this.options.isLoaded = false;
    }
});


