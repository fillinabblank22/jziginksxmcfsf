﻿<%@ Page Language="C#" MasterPageFile="~/Orion/View.master" AutoEventWireup="true" 
         CodeFile="WLHMDetails.aspx.cs" Inherits="Orion_WirelessHeatmaps_WLHMDetails" 
         Title="WLHM Details"%>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="wlhm" Namespace="SolarWinds.Wireless.Heatmaps.Web.UI" Assembly="SolarWinds.Wireless.Heatmaps.Web" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ViewPageTitle" Runat="Server">
	<h1>
	    <%= WebSecurityHelper.HtmlEncode(Title) %>
	</h1>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainContentPlaceHolder">
	<wlhm:WLHMResourceHost runat="server" ID="mapID">
	    <orion:ResourceContainer runat="server" ID="resContainer" />
	</wlhm:WLHMResourceHost>
</asp:Content>