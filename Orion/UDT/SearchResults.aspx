﻿<%@ Page Title="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_19%>" Language="C#" MasterPageFile="~/Orion/UDT/UdtPageWithoutRefresher.master" AutoEventWireup="true"
     CodeFile="SearchResults.aspx.cs" Inherits="Orion_UDT_SearchResults" %>
<%@ Register TagPrefix="udt" TagName="PageTitleBar" Src="~/Orion/UDT/Controls/PageTitleBar.ascx" %>
<%@ Register TagPrefix="udt" TagName="LearnMoreLink" Src="~/Orion/UDT/Controls/LearnMoreLink.ascx" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>

<asp:Content ID="udtHeader" ContentPlaceHolderID="UdtHeadPlaceHolder" Runat="Server">
    <orion:IncludeExtJs id="IncludeExtJs1" runat="server" debug="false" Version="3.4"/>
    <orion:Include runat="server" File="OrionCore.js"/>
    <orion:Include runat="server" Module="UDT" File="SearchResults.js"/>

    <style type="text/css">
        .smallText {color:#979797;font-size:11px;}
        .watchList { background-image:url(/Orion/UDT/Images/detective_16x16.png) !important; }
        .export { background-image:url(/Orion/UDT/images/export_file_icon.png) !important; }
        .allConn { background-image:url(/Orion/UDT/images/icon_all_connection_types.png) !important; }
        .direct { background-image:url(/Orion/UDT/images/icon_direct.png) !important; }
        .unknown { background-image:url(/Orion/UDT/images/icon_direct_unknown.png) !important; }
        .indirect { background-image:url(/Orion/UDT/images/icon_indirect.png) !important; }
        .allLayers { background-image:url(/Orion/UDT/images/icon_all_layers.png) !important; }
        .layer2 { background-image:url(/Orion/UDT/images/icon_layer2.png) !important; }
        .layer3 { background-image:url(/Orion/UDT/images/icon_layer3.png) !important; }
         .activeconnections { background-image:url(/Orion/UDT/images/icon_activeconnections.png) !important; }
         .allhistoricalresults { background-image:url(/Orion/UDT/images/icon_allhistoricalresults.png) !important; }
        #adminContent .dialogBody table td { padding-top: 5px; padding-left: 10px; }
        #adminContent .dialogBody ul { margin-left: 20px; list-style-type: square; }
        #addWatchDialog .dialogBody table td { padding-right: 10px; }
        #addWatchDialog td.x-btn-center { text-align: center !important; }
        #addWatchDialog td { padding-right: 0; padding-bottom: 0; }
        .learnMoreLink a {color: #336699 !important; font-size:11px;}
        .learnMoreLink a:hover { color: #f99d1c !important; font-size:11px;}
        #Grid td { padding-right: 0; padding-bottom: 0; vertical-align: middle; }
        .iconText { padding-left: 25px !important; }
        .radiobutton_title{padding-left: 5px;}
        .rightColumnStyle{ width: 50%;white-space: nowrap;}
        .leftColumnStyle{ width: 50%;}
        
    </style>
</asp:Content>

<asp:Content id="SearchResultsTitle" ContentPlaceHolderID="TopRightPageLinks" runat="Server">
    <udt:PageTitleBar runat="server" id="pageTitleBar" ShowTitle="False"/>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ViewPageTitle">
    <h1 style="padding-bottom: 7px"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_19%></h1>
</asp:Content>

<asp:Content id="searchResults" ContentPlaceHolderID="UdtMainContentPlaceHolder" Runat="Server">
<input type="hidden" name="UDT_SearchResultsEx_PageSize" id="UDT_SearchResultsEx_PageSize" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("UDT_SearchResultsEx_PageSizeEx")%>' />
<input type="hidden" name="UDT_SearchResultsEx_SearchItemOptionEx" id="UDT_SearchResultsEx_SearchItemOptionEx" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("UDT_SearchResultsEx_SearchItemOptionEx")%>' />
<input type="hidden" name="UDT_SearchResultsEx_SearchConnectionTypeOptionEx" id="UDT_SearchResultsEx_SearchConnectionTypeOptionEx" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("UDT_SearchResultsEx_SearchConnectionTypeOptionEx")%>' />
<input type="hidden" name="UDT_SearchResultsEx_SearchLayerTypeOptionEx" id="UDT_SearchResultsEx_SearchLayerTypeOptionEx" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("UDT_SearchResultsEx_SearchLayerTypeOptionEx")%>' />

    <div style="padding-left: 10px; padding-bottom: 10px;"><asp:Literal runat="server" id="litPageSubTitle"></asp:Literal>  <udt:LearnMoreLink ID="linkLearnMore" runat="server" Visible="true" /></div>
     <div id="searchContent" style="padding-left: 10px; padding-right: 10px;">	
        <table class="GridTable" cellpadding="0" cellspacing="0" width="100%" style="padding-top: 10px">
	        <tr valign="top" align="left">
		        <td id="gridCell" style="padding-right:0px;">
<%--                        <div id="Searching" style="width:355px">
                        <img src="/Orion/UDT/images/animated/loading_gen_16x16.gif" alt="Searching. Please wait..."/>
                        &nbsp;<b>searching...</b></div>--%>
                        <div id="Grid" style="visibility:visible; display:block;"/>
		        </td>
	        </tr>
        </table>
	    </div>

<div id="addWatchDialog" class="x-hidden">
	<div class="x-window-header"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_23 %></div>
	<div id="addWatchDialogBody" class="x-panel-body dialogBody" style="font-family: Arial;">
	<table style="padding:8px;  margin:10px;">
	    <tr>
	        <td colspan="2" style="padding-bottom:4px;"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_24 %></td>
	    </tr>
	    <tr>
	        <td class="leftColumnStyle" style="padding-bottom:2px;"><input type="radio" id="radMacAddress" name="addOptions" checked="checked" onclick="addDialogUI()" value="0" title="<%= Resources.UDTWebContent.UDTWEBDATA_VB1_6 %>" /><span class="radiobutton_title"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_25%></span></td>
            <td class="rightColumnStyle" style="padding-top:2px;"><input type="text" id="MACAddress" style="width:160px; border:0" readonly="readonly" /></td>
	    </tr>
        <tr>
	        <td class="leftColumnStyle" style="padding-bottom:2px;"><input type="radio" id="radIpAddress" name="addOptions" onclick="addDialogUI()" value="1" title="<%= Resources.UDTWebContent.UDTWEBDATA_VB1_7 %>" /><span class="radiobutton_title"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_26%></span></td>
            <td class="rightColumnStyle" style="padding-top:2px;"><input type="text" id="IPAddress" style="width:160px; border:0" readonly="readonly" /></td>
	    </tr>
        <tr>
	        <td class="leftColumnStyle" style="padding-bottom:2px;"><input type="radio" id="radHostname" name="addOptions" onclick="addDialogUI()" value="2" title="<%= Resources.UDTWebContent.UDTWEBDATA_VB1_8 %>" /><span class="radiobutton_title"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_27%></span></td>
            <td class="rightColumnStyle" style="padding-top:2px;"><input type="text" id="Hostname" style="width:160px; border:0" readonly="readonly" /></td>
	    </tr>
        <tr>
	        <td colspan="2" style="padding-bottom:4px; padding-top:20px;"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_28 %></td>
	    </tr>
        <tr>
	        <td colspan="2" style="padding-bottom:4px;"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_29 %></td>
	    </tr>
        <tr>
	        <td class="leftColumnStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_10 %></td>
            <td class="rightColumnStyle" style="padding-top:2px;"><input type="text" id="Name" style="width:160px" /></td>
	    </tr>
        <tr>
	        <td class="leftColumnStyle" style="vertical-align:top;"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_11%></td>
            <td class="rightColumnStyle" style="padding-bottom:2px;"><textarea id="Description" rows="5" cols="16" style="width:160px; height:70px;"></textarea></td>
	    </tr>
    </table>
	</div>
</div>

<div id="csvExport" class="x-hidden">
	<div class="x-window-header"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_30 %></div>
	<div id="csvExportBody" class="x-panel-body dialogBody">
	    <div id="exportProgress" ></div>
        <div id="exportDownload" ></div>
	</div>
</div>
 <%if (_isOrionDemoMode) {%> <div id="isOrionDemoMode" style="display: none;"/> <%}%>
 <%if (!Profile.AllowAdmin) {%> <div id="isNonAdmin" style="display: none;"/> <%}%>

</asp:Content>
