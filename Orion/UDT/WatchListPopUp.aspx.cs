﻿using System;
using SolarWinds.Orion.Web;
using SolarWinds.UDT.Web.NetObjects;

public partial class  Orion_UDT_WatchListPopUp : System.Web.UI.Page
{
    protected UDTWatchListNetObject UDTWatchList { get; set; }
    
    protected string FirstSeenMessage 
    { 
        get
        {
            string message = Resources.UDTWebContent.UDTWEBDATA_AK1_151;
            switch (UDTWatchList.WatchItemType)
            {
                case "1":
                    message = Resources.UDTWebContent.UDTWEBCODE_AK1_47;
                    break;
                case "2":
                    message = Resources.UDTWebContent.UDTWEBCODE_AK1_48;
                    break;
                case "3":
                    message = Resources.UDTWebContent.UDTWEBCODE_AK1_49;
                    break;
                case "4":
                    message = Resources.UDTWebContent.UDTWEBCODE_AK1_50;
                    break;
            }
            return message;
        }
    }

    protected string StatusCssClass
    {
        get
        {
            if (UDTWatchList != null)
            {
                if (UDTWatchList.IsActive)
                {
                    return "StatusUp";
                }
                return "StatusDown";
            }
            return "StatusUnknown";
        }
    }

    protected string ActivelyConnected
    {
        get
        {
            if (UDTWatchList != null)
            {
                if (!string.IsNullOrEmpty(UDTWatchList.ErrorMessage))
                    return Resources.UDTWebContent.UDTWEBCODE_AK1_51;
                if (UDTWatchList.IsActive)
                    return Resources.UDTWebContent.UDTWEBCODE_AK1_52;
                if (!UDTWatchList.IsFound)
                    return Resources.UDTWebContent.UDTWEBCODE_AK1_53;
                return Resources.UDTWebContent.UDTWEBCODE_AK1_54;
            }
            return Resources.UDTWebContent.UDTWEBCODE_AK1_55;
        }
    }

    protected string Caption
    {
        get { return string.IsNullOrEmpty(UDTWatchList.WatchName) 
            ? UDTWatchList.Name
            : string.Format("{0}{2}[{1}]", UDTWatchList.WatchName, UDTWatchList.WatchItem, UDTWatchList.WatchItem.Length + UDTWatchList.WatchName.Length > 20 ? "\r\n" : " ");
        }
    }

    protected string WiredVisible { get { return UDTWatchList.IsWireless ? "style=\"display: none\"" : ""; } }
    protected string WirelessVisible { get { return UDTWatchList.IsWireless ? "" : "style=\"display: none\""; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            UDTWatchList = NetObjectFactory.Create(Request["NetObject"], true) as UDTWatchListNetObject;
        }
        catch{}
    }
}
