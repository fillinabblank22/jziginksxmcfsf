﻿<%@ Page Title="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_14 %>" Language="C#" MasterPageFile="~/Orion/UDT/UdtPageWithoutRefresher.master" AutoEventWireup="true"
     CodeFile="ManageDomainControllers.aspx.cs" Inherits="Orion_UDT_ManageDomainControllers" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register TagPrefix="udt" TagName="PageTitleBar" Src="~/Orion/UDT/Controls/PageTitleBar.ascx" %>
<%@ Register TagPrefix="udt" TagName="LearnMoreLink" Src="~/Orion/UDT/Controls/LearnMoreLink.ascx" %>

<asp:Content ID="udtHeader" ContentPlaceHolderID="UdtHeadPlaceHolder" Runat="Server">

    <orion:IncludeExtJs id="IncludeExtJs1" runat="server" debug="false" Version="3.4"/>
    <orion:include ID="OrionCoreJs" runat="server" File="/js/OrionCore.js" />
    <orion:include ID="ManageDomainControllerJs" runat="server" File="/UDT/js/ManageDomainControllers.js" />
    <orion:include ID="AssignCredentialJs" runat="server" File="/UDT/js/AssignCredentials.js" />  
    <style type="text/css">
        span.Label { white-space:nowrap;  }
        .smallText {color:#979797;font-size:11px;}
        .add { background-image:url(/Orion/images/add_16x16.gif) !important; width:auto !important}
        .edit { background-image:url(/Orion/images/edit_16x16.gif) !important; width:auto !important }
        .assign { background-image: url(/Orion/images/icon_assign_cred_16x16.gif) !important; width:auto !important}
        .del { background-image:url(/Orion/images/delete_16x16.gif) !important; width:auto !important }
        #delDialog .dialogBody table td { padding-right: 10px; }
        #delDialog td.x-btn-center { text-align: center !important; }
        #delDialog td { padding-right: 0; padding-bottom: 0; }
        #adminContent .dialogBody table td { padding-top: 5px; padding-left: 10px; }
        #adminContent .dialogBody ul { margin-left: 20px; list-style-type: square; }
        #Grid td { padding-right: 0; padding-bottom: 0; vertical-align: middle; }
        .radiobutton_title{padding-left: 5px;}
    </style>
</asp:Content>

<asp:Content id="ManageDomainControllersTitle" ContentPlaceHolderID="TopRightPageLinks" runat="Server">
    <udt:PageTitleBar runat="server" id="PageTitleBar" ShowTitle="False"/>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ViewPageTitle">
    <h1><%= Resources.UDTWebContent.UDTWCODE_VB1_4 %></h1>
</asp:Content>

<asp:Content id="DomainControllers" ContentPlaceHolderID="UdtMainContentPlaceHolder" Runat="Server">
<input type="hidden" name="UDT_ManageDomainControllers_PageSize" id="UDT_ManageDomainControllers_PageSize" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("UDT_ManageDomainControllers_PageSize")%>' />
<input type="hidden" id="ReturnToUrl" value="<%= ReturnUrl %>" />

     <div id="DomainControllerContent" style="padding-left: 10px; padding-right: 10px;">	
        <div>
        <table class="TitleTable">
            <tr>
                <td>
                    <div><asp:Literal runat="server" id="PageSubTitle"></asp:Literal></div>
                    <br />
                </td>
            </tr>
        </table>
        <table class="GridTable" cellpadding="0" cellspacing="0" width="100%" >
	        <tr valign="top" align="left">
		        <td id="gridCell" style="padding-right:0px;">
                    <div id="Grid" style="visibility:visible; display:block;"/>
		        </td>
	        </tr>
        </table>
	    </div>
    </div>

<%--used for the delete nodes confirmation dialog--%>
<div id="delDialog" class="x-hidden">
	    <div class="x-window-header"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_15 %></div>
	    <div id="delDialogBody" class="x-panel-body dialogBody">
	    <table>
	        <tr>
	            <td><div id="delDialogDescription"></div></td>
	        </tr>

            <tr>
                <td style="padding-bottom:2px; padding-top:10px;"><input type="radio" id="radDeleteUDT" name="delOptions" checked="checked" value="0" title="<%= Resources.UDTWebContent.UDTWEBDATA_VB1_16 %>" /><span class="radiobutton_title"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_16 %></span></td>
            </tr>
            <tr>
	            <td style="padding-bottom:2px;"><input type="radio" id="radDeleteAll" name="delOptions" value="1" title="<%= Resources.UDTWebContent.UDTWEBDATA_VB1_17 %>" /><span class="radiobutton_title"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_17 %></span></td>
            </tr>
        </table>
    </div>
</div>

<div id="deleteProgressDialog" class="x-hidden">
	<div class="x-window-header"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_18 %></div>
	<div id="deleteProgressBody" class="x-panel-body dialogBody">
	    <div id="deleteProgress" ></div>
	</div>
</div>

<div id="validateDCDialog"></div>

<%--
 <%if (_isOrionDemoMode) {%> <div id="isOrionDemoMode" style="display: none;"/> <%}%>
 <%if (!Profile.AllowAdmin) {%> <div id="isNonAdmin" style="display: none;"/> <%}%>--%>




</asp:Content>
