﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EndpointIPPopUp.aspx.cs" Inherits="Orion_UDT_EndpointIPPopUp" %>

<h3 class="<%=StatusCssClass%>"><%=SolarWinds.Orion.Web.Helpers.UIHelper.Escape(UDTEndpoint.Name)%></h3>

<div class="NetObjectTipBody">
    <table cellpadding="0" cellspacing="0">
	    <tr>
			<th style="white-space: nowrap;"><%=StatusText%></th>	   
		</tr>
    <% if (!string.IsNullOrEmpty(UDTEndpoint.FirstSeen)) { %>	
        <tr>
			<th width="30%"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_151 %></th>
            <td width="70%" colspan="2"><%=FirstSeen%></td> 
		</tr>
        <tr>
			<th width="30%"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_152 %></th>
            <td width="70%" colspan="2"><%=LastSeen%></td> 
		</tr>
    <% } else { %>
        <tr>
			<td width="100%">&nbsp;</td> 
		</tr>
    <% } %>

        <% if (!string.IsNullOrEmpty(UDTEndpoint.LastUserLogIn) && !string.IsNullOrEmpty(UDTEndpoint.LastLogInTime))
            { %>
        <tr>
            <th width="30%"><%= Resources.UDTWebContent.EndpointPopUp_LastUserLogin %></th>
            <td width="70%" colspan="2"><%=LastUserLogIn%></td>
        </tr>
        <tr>
            <th width="30%"><%= Resources.UDTWebContent.EndpointPopUp_LoginTime %></th>
            <td width="70%" colspan="2"><%=LastLogInTime%></td>
        </tr>
        <% } %>
	</table>
</div>
