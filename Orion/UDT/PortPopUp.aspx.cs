﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.UDT.Web.NetObjects;

public partial class Orion_UDT_PortPopUp : System.Web.UI.Page
{
    protected UDTPortNetObject UDTPort { get; set; }
    public string vlanStringTrunk;
    protected string StatusCssClass
    {
        get
        {
            if (UDTPort.IsAdminDown)
            {
                return "StatusDown";
            }
            else
            {
                if (UDTPort.ActiveConnection)
                {
                    return "StatusUp";
                }
                else
                {
                    return "StatusUnknown";
                }
            }
        }
    }

    protected string ActiveConnectionMessage
    {
        get
        {
            if (UDTPort.ActiveConnection)
            {
                return Resources.UDTWebContent.UDTWEBCODE_AK1_22;
            }
            else
            {
                return Resources.UDTWebContent.UDTWEBCODE_AK1_23;
                
            }
        }
    }

    protected string IsMonitoredMessage
    {
        get
        {
            if (UDTPort.IsMonitored)
            {
                return Resources.UDTWebContent.UDTWEBCODE_AK1_24;
            }
            else
            {
                return Resources.UDTWebContent.UDTWEBCODE_AK1_25;
            }
        }
    }

    protected string PortDescription
    {
        get
        {
            string description = UDTPort.PortDescription;
            string tmpDescription=description;
            if (description.Length>24 && (description.IndexOf(" ") > 24 || description.IndexOf(" ")==-1))
            {
                tmpDescription = "";
                while (description.Length > 24)
                {
                    tmpDescription += " " + description.Substring(0, 24);
                    description = description.Substring(24);
                }
                tmpDescription += " " + description;
            }
            return tmpDescription;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.UDTPort = NetObjectFactory.Create(Request["NetObject"], true) as UDTPortNetObject;
        
        vlanStringTrunk = TruncateVlanString(UDTPort.VLANstring, 100);

        
        //hide some details if it is not monitored
        if(!UDTPort.IsMonitored)
        {
            VLANPanel.Visible = false;
            IpPanel.Visible = false;
        }
    }
	
	private string TruncateVlanString(string value, int maxLength)
	{
		if (value.Length <= maxLength)
			return value;
		
		var truncated = value.Substring(0, maxLength);

		return truncated.Substring(0, truncated.LastIndexOf(",")) + ",...";
	} 
}