﻿Ext.namespace('SW');
Ext.namespace('SW.UDT');

var searchForStringRaw;
var searchInColumnList;
var searchForMacString;
var searchTargets;

var watchTypeId; //1=MACAddress 2=IPAddress 3=Hostname


// function to reformat search text for SQL query execution
function formatSearchStringForSQL(searchText, searchInColumns) {
//alert("searchText in: " + searchText)
    var arrSearchText = [];

    if (searchText != "") {

        //replace all single quote ' instances to a double single quote instance.  e.g. test's becomes test''s
        searchText = searchText.replace(/\'/g, "''");

        //append escape character if % literal found e.g. test% becomes test[%]
        searchText = searchText.replace(/\%/g, "[%]");

        //append escape character if % literal found e.g. test_ becomes test[_]
        searchText = searchText.replace(/\_/g, "[_]");

        //ToDo: replace wildcard * with SQL % in order to enforce explicit usage of wildcard searching
        searchText = searchText.replace(/\*/g, "%");
        //searchText = searchText.replace(/\?/g, "_"); // single character replace?

        // push searchText (non MAC address formatted) onto the searchString array
        arrSearchText.push(searchText);

        //REFORMAT SEARCH STRING FOR MAC ADDRESS SEARCH: 
        //if searchtext looks like part of mac address then strip MAC address formatting before search
        //as our database values are free from any :, - or . formatting.

        if ((searchText.indexOf(":") != -1) || (searchText.indexOf(".") != -1) || (searchText.indexOf("-") != -1)) {

            //first, strip out any % wildcard chars
            //var tmpSearchText = searchText.replace(/\*/g, "");
            var tmpSearchText = searchText.replace(/\%/g, "");

            var ciscoDot = /^([0-9a-f]{0,4})(([.-]|$)([0-9a-f]{0,4}$|(([0-9a-f]{4}[.-]$)|(([0-9a-f]{4}[.-][0-9a-f]{0,4}$)))))/i; //this works OK!
            var unix = /^([0-9a-f]{0,2})(([:-]|$)([0-9a-f]{0,2}$|(([0-9a-f]{2}[:-]$)|(([0-9a-f]{2}[:-][0-9a-f]{0,2}$)|((([0-9a-f]{2}[:-]){2}[0-9a-f]{0,2}$)|((([0-9a-f]{2}[:-]){3}[0-9a-f]{0,2}$)|((([0-9a-f]{2}[:-]){4}[0-9a-f]{0,2}$))))))))/i;
            var windows = /^([0-9a-f]{0,2})(([--]|$)([0-9a-f]{0,2}$|(([0-9a-f]{2}[--]$)|(([0-9a-f]{2}[--][0-9a-f]{0,2}$)|((([0-9a-f]{2}[--]){2}[0-9a-f]{0,2}$)|((([0-9a-f]{2}[--]){3}[0-9a-f]{0,2}$)|((([0-9a-f]{2}[--]){4}[0-9a-f]{0,2}$))))))))/i;

            if (ciscoDot.test(tmpSearchText)) searchText = searchText.replace(/\./g, "");
            if (unix.test(tmpSearchText)) searchText = searchText.replace(/\:/g, "");
            if (windows.test(tmpSearchText)) searchText = searchText.replace(/\-/g, "");

            // push recoded MAC searchText onto the array
            arrSearchText.push(searchText);
        }

        else {
            // we aren't searching on MAC address column, so push an empty string
            arrSearchText.push(searchText);
        }
    }

    else {
        // no searchText string, so push Searchtext onto array twice (2nd is for the MAC search string)
        arrSearchText.push(searchText);
        arrSearchText.push(searchText);
    }

   // alert("searchText out: " + searchText);
    return arrSearchText;

}

function DisplayProgress(value) {

    if (value == true) {
        $("#searchProgressLabel").show();
        $("#searchProgress").show();
    }
    else {
         $("#searchProgressLabel").hide();
         $("#searchProgress").hide();
    }
}

////Custom WebServiceStore that supports Grouping
ORION.WebServiceGroupStore = function (url, readerFields, sortOrderField, grouperField) {

    var c = {};
    
    ORION.WebServiceGroupStore.superclass.constructor.call(this, Ext.apply(c, {
        proxy: new Ext.data.HttpProxy({
            url: url,
            method: "POST",
            success: function (xhr, options) {
                var obj = Ext.decode(xhr.responseText);
            },
            failure: function (xhr, options) {
                ORION.handleError(xhr);
            }
        }),
        reader: new Ext.data.JsonReader({
            totalProperty: "d.TotalRows",
            root: "d.DataTable.Rows"
        },
            readerFields
        ),

        remoteSort: true,
        sortInfo: {
            field: sortOrderField,
            direction: "ASC"
        },
        groupField: grouperField
    }));
};
Ext.extend(ORION.WebServiceGroupStore, Ext.data.GroupingStore);

SW.UDT.AsyncSearch = function () {

    ORION.prefix = "UDT_SearchResults_";

    UpdateToolbarButtons = function () {
        var selItems = grid.getSelectionModel(), count = selItems.getCount(), map = grid.getTopToolbar().items.map;

        // set buttons disabled by default
        map.AddToWatchList.setDisabled(true);
        map.AddToWhiteList.setDisabled(true);

        if (count > 0) {
            // now check what item type we have selected
            var record = selItems.getSelected();
            var itemTypeToLower = record.data['MatchType'].toLowerCase();

            switch (itemTypeToLower) {
                case 'endpoint ip address':
                case 'endpoint mac address':
                case 'endpoint hostname':
                case 'vendor':
                    map.AddToWhiteList.setDisabled(false);
                    map.AddToWatchList.setDisabled(false);
                    break;
                case 'user name':
                    map.AddToWatchList.setDisabled(false);
                    map.AddToWhiteList.setDisabled(true);
                    break;
                default:
                    map.AddToWatchList.setDisabled(true);
                    map.AddToWhiteList.setDisabled(true);
            }
        }
        if ($("#isOrionDemoMode").length != 0) {
            map.AddToWhiteList.setDisabled(true);
            return;
        }
    };


    // *** START - GRID LAYOUT FUNCTIONS ***

    // update the Ext grid size when window size is changed
    $(window).resize(gridResize);

    function gridResize() {
        // need to set grid width to small width first as it appears setWidth only resizes upwards! (bug in Ext 3.x?)
        grid.setWidth(1);
        grid.setWidth($('#gridCell').width());
    }

    function gridDisplayMask(value) {
        var el = grid.getGridEl();
        // show or hide the grid mask searching message
        if (value == true) {
            grid.getView().emptyText = "";
            el.mask('@{R=UDT.Strings;K=UDTWEBJS_AK1_21;E=js}', 'x-mask');
        }
        else {
            el.unmask();
        }
        grid.view.refresh.defer(1, grid.view);
    }

    // *** END - GRID LAYOUT FUNCTIONS ***


    // *** START - GRID COLUMN RENDERING FUNCTIONS ***

    // Column rendering functions
    function encodeHTML(htmlText) {
        return $('<div/>').text(htmlText).html();
    }

    // main entry point for MatchItem column rendering
    function renderText(value, meta, record, rowIndex, colIndex, store) {

        var retValue;
        var itemTypeToLower = record.data['MatchType'].toLowerCase();

        switch (itemTypeToLower) {
            case 'user name':
                retValue = '@{R=UDT.Strings;K=UDTWEBJS_AK1_2;E=js}';
                break;
            case 'endpoint hostname':
                retValue = '@{R=UDT.Strings;K=UDTWEBJS_AK1_3;E=js}';
                break;
            case 'endpoint ip address':
                retValue = '@{R=UDT.Strings;K=UDTWEBJS_AK1_4;E=js}';
                break;
            case 'endpoint mac address':
                retValue = '@{R=UDT.Strings;K=UDTWEBJS_AK1_5;E=js}';
                break;
            case 'node name':
                retValue = '@{R=UDT.Strings;K=UDTWEBJS_AK1_6;E=js}';
                break;
            case 'node port':
                retValue = '@{R=UDT.Strings;K=UDTWEBJS_AK1_7;E=js}';
                break;
            case 'port description':
                retValue = '@{R=UDT.Strings;K=UDTWEBJS_AK1_110;E=js}';
                break;
            case 'port name (description)':
                retValue = '@{R=UDT.Strings;K=UDTLIBCODE_AK1_1;E=js}';
                break;
            default:
                retValue = value;
        }

        if (colIndex == 1) {
            // determine the 'Item Type' in order to call the relevant renderer for that type
            switch (itemTypeToLower) {
                case 'user name':
                    retValue = renderUserItem(value, record);
                    break;
                case 'node name':
                    retValue = renderNodeStatusIconWithNodeItem(value, record);
                    break;
                case 'port description':
                case 'port name (description)':
                    retValue = renderPortStatusIconWithPortItem(value, record);
                    break;
                default:
                    retValue = renderEndpointItem(value, record);
            }
        }
        return retValue;
    }


    //    function groupRenderText(value, o, record, rowIndex, colIndex, store) {

    //        var retValue = value;
    //        var itemTypeToLower = record.data['MatchType'].toLowerCase();

    //        //alert("colIndex:" + colIndex.toString());

    //        // determine the 'Item Type' in order to call the relevant renderer for that type
    //        switch (itemTypeToLower) {
    //            case 'port description':
    //            case 'node port':
    //                retValue = renderPortStatusIconWithPortItem(value, record);
    //                break;
    //            case 'node name':
    //                retValue = renderNodeStatusIconWithNodeItem(value, record);
    //                break;
    //            default:
    //                retValue = value;
    //        }

    //        return retValue;
    //    }

    function renderUserItem(value, record) {

        if (value == null) {
            formatted = '';
        }
        else {
            var urlLink = '<a href="/Orion/UDT/UserDetails.aspx?NetObject=UU:' + record.data['ID'].toString() + '">' + value + '</a>';
            var formatted = urlLink;
        }

        return formatted;
    }

    function formatMacAddressForDisplay(value) {
        // renders the MAC address in a standard display format
        var regex = /(.{2})(.{2})(.{2})(.{2})(.{2})(.{2})/;
        var formatted = value.replace(regex, "$1:$2:$3:$4:$5:$6");
        return formatted;
    }

    function renderEndpointItem(value, record) {

        if (value == null) {
            formatted = '';
        }
        else // check the ItemType in order to set the Endpoint netobject correctly
        {
            var itemTypeToLower = record.data['MatchType'].toLowerCase();
            var netObjectEndpointItem;

            switch (itemTypeToLower) {
                case 'endpoint hostname':
                    netObjectEndpointItem = "DNS";
                    break;
                case 'endpoint ip address':
                    netObjectEndpointItem = "IP";
                    break;
                case 'endpoint mac address':
                default:
                    // If the MAC Address is formatted check is here.
                    if (value.indexOf(":") == -1)
                        value = formatMacAddressForDisplay(value);
                    netObjectEndpointItem = "MAC";
            }

            var urlLink = '<a href="/Orion/UDT/EndpointDetails.aspx?NetObject=UE-' + netObjectEndpointItem + ':VAL=' + value + '">' + value + '</a>';
            var formatted = urlLink;
        }

        return formatted;
    }

    function renderPortStatusIconWithPortItem(value, record) {

        var img;
        var formatted;

        if (value == null || value == "") {
            // set [Unknown] text for ports with no name
            value = '@{R=UDT.Strings;K=UDTWEBJS_AK1_20;E=js}';
        }

        var portStatus = [];
        //split the status field back into the OperationalStatus and Flag columns
        portStatus = record.data['Status'].split(':');

        if (portStatus[1].toString() == "2") // Port Flag is 'Missing'
        {
            img = String.format('<img src="/Orion/UDT/Images/Status/icon_port_missing.gif" title="{0}" alt="{0}" style="vertical-align:bottom;" />&nbsp;', '@{R=UDT.Strings;K=UDTWEBJS_AK1_22;E=js}');
        }

        else if (portStatus[0].toString() == "1") // Port Status is Up
        {
            img = String.format('<img src="/Orion/UDT/Images/Status/icon_port_active_dot.gif" title="{0}" alt="{0}" style="vertical-align:bottom;" />&nbsp;', '@{R=UDT.Strings;K=UDTWEBJS_AK1_23;E=js}');
        }
        else //anything other than up, (can actually have any status between 2 - 7)
        {
            img = String.format('<img src="/Orion/UDT/Images/Status/icon_port_inactive_dot.gif" title="{0}" alt="{0}" style="vertical-align:bottom;" />&nbsp;', '@{R=UDT.Strings;K=UDTWEBJS_AK1_24;E=js}');
        }

        var urlLink = '<a href="/Orion/UDT/PortDetails.aspx?NetObject=UP:' + record.data['ID'].toString() + '">' + value + '</a>';
        return img + urlLink;
    }


    function renderNodeStatusIconWithNodeItem(value, record) {
        // renders node status icon next to node caption
        var img;

        if (value == null) {
            formatted = '';
        }
        else // add node caption text and node status icon
        {
            if (record.data['Status'].toLowerCase() == "up.gif") // Node Status is Up
            {
                img = String.format('<img src="/Orion/images/StatusIcons/Small-up.gif" title="{0}" alt="{0}" style="vertical-align:bottom;" />&nbsp;', '@{R=UDT.Strings;K=UDTWEBJS_AK1_25;E=js}');
            }
            else {
                if (record.data['Status'].toLowerCase() == "down.gif") // Node Status is Down
                {
                    img = String.format('<img src="/Orion/images/StatusIcons/Small-down.gif" title="{0}" alt="{0}" style="vertical-align:bottom;" />&nbsp;', '@{R=UDT.Strings;K=UDTWEBJS_AK1_26;E=js}');
                }
                else //anything other than up - Unknown
                {
                    img = String.format('<img src="/Orion/images/StatusIcons/Small-warning.gif" title="{0}" alt="{0}" style="vertical-align:bottom;" />&nbsp;', '@{R=UDT.Strings;K=UDTWEBJS_AK1_27;E=js}');
                }
            }

            var urlLink = '<a href="/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:' + record.data['ID'].toString() + '">' + value + '</a>';
            var formatted = img + urlLink;
        }
        return formatted;

    }

    function renderFirstSeen(value, o, record, rowIndex, colIndex, store) {

        var retValue;
        var itemTypeToLower = record.data['MatchType'].toLowerCase();

        retValue = record.data['FirstSeenDisplay'];

        return retValue;
    }

    function renderLastSeen(value, o, record, rowIndex, colIndex, store) {

        var retValue = value;
        var itemTypeToLower = record.data['MatchType'].toLowerCase();

        // determine the 'Item Type' in order to call the relevant renderer for that type
        switch (itemTypeToLower) {
            case 'endpoint hostname':
            case 'endpoint ip address':
            case 'endpoint mac address':
                if (record.data['LastSeenDisplay'] == null || record.data['LastSeenDisplay'].length == 0) {
                    retValue = "Current connection";
                }
                else {
                    retValue = record.data['LastSeenDisplay'];
                }
                break;
            default:
                retValue = record.data['LastSeenDisplay'];
        }

        return retValue;
    }
    // *** END - GRID COLUMN RENDERING FUNCTIONS ***

    //*** WATCHLIST FUNCTIONS ***

    function StripMacFormatting(value) {
        return value.replace(/[\.\:-]/g, '');
    }

    function DecompressIPv6(ip) {

    }

    // Add selected item to whitelist
    addToWhiteList = function () {

        if (($("#isNonAdmin").length != 0)) {
            alert('@{R=UDT.Strings;K=UDTWEBJS_AK1_29;E=js}');
            return;
        }
        // get data for selected record
        grid.getSelectionModel().each(function (record) {

            var itemTypeToLower = record.data['MatchType'].toLowerCase();
            var endpoint = record.data['MatchItem'];

            var endpointType;

            // determine the 'Item Type' in order to enable the relevant watch field
            switch (itemTypeToLower) {
                case 'endpoint hostname':
                    endpointType = "UE-DNS";
                    break;
                case 'endpoint ip address':
                    endpointType = "UE-IP";
                    break;
                case 'endpoint mac address':
                    endpointType = "UE-MAC";
                    break;
                case 'vendor':
                    endpointType = "UE-MAC";
                    break;
                default:
                    endpointType = "UE-DNS";
            }

            //bust out to White list page
            window.location = "/Orion/UDT/Admin/WhiteList/Wizard/Default.aspx?edit=0&type=2&endpoint=" + endpoint + "&endpointType=" + endpointType;

        });
    };

    // Add selected item to watchlist function.  
    // Pops a dialog to select which item from the selected record (MAC, IP or Hostname) you want to add to watchlist.
    // Calls on webservice to perform add to watchlist.
    addToWatchList = function () {
              // get data for selected record
        grid.getSelectionModel().each(function (record) {
            var itemTypeToLower = record.data['MatchType'].toLowerCase();
            var watchValue = record.data['MatchItem'];
            var vendorIcon = record.data['VendorIcon'];
            var td = document.getElementById("WatchType");

            // determine the 'Item Type' in order to enable the relevant watch field
            switch (itemTypeToLower) {
                case 'endpoint hostname':
                    watchTypeId = 3;
                    td.innerHTML = '@{R=UDT.Strings;K=UDTWEBJS_AK1_125;E=js}';
                    $("#VendorIcon").html('');
                    $("#VendorMacRow").hide();
                    break;
                case 'endpoint ip address':
                    watchTypeId = 2;
                    td.innerHTML = '@{R=UDT.Strings;K=UDTWEBJS_AK1_126;E=js}';
                    $("#VendorIcon").html('');
                    $("#VendorMacRow").hide();
                    break;
                case 'vendor':
                case 'endpoint mac address':
                    if (watchValue.indexOf(":") == -1)
                        watchValue = formatMacAddressForDisplay(record.data['MatchItem']);
                    $("#VendorMacRow").show();
                    $("#VendorIcon").html(vendorIcon);
                    watchTypeId = 1;
                    td.innerHTML = '@{R=UDT.Strings;K=UDTWEBJS_AK1_127;E=js}';
                    break;
                case 'user name':
                    watchTypeId = 4;
                    td.innerHTML = '@{R=UDT.Strings;K=UDTWEBJS_SW1_1;E=js}';
                    $("#VendorIcon").html('');
                    $("#VendorMacRow").hide();
                    break;
                default:
                    watchTypeId = 3;
            }
            // set the relevent text of the WatchType <TD>
            $("#WatchValue").val(watchValue);
        });

        // leave optional field values blank
        $("#Name").val("");
        $("#Description").val("");

        if (!addWatchDialog) {
            addWatchDialog = new Ext.Window({
                applyTo: 'addWatchDialog',
                layout: 'fit',
                width: 380,
                height: 380, //Google Chrome seems to render dialog shorter then other browsers, so increase the dialog hieght so all controls display 
                closeAction: 'hide',
                modal: true,
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'addWatchDialogBody',
                    //layout: 'fit',  //fails to fit properly in Chrome
                    border: false
                }),
                buttons:
                [
                {
                    text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_5;E=js}',
                    handler: function (me) {
                        AddWatchServiceCall(watchTypeId);
                    }
                },
                {
                    text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_6;E=js}',
                    style: 'margin-left: 5px;',
                    handler: function () {
                        addWatchDialog.hide();
                    }
                }
                ]
            });
        }

        addWatchDialog.setTitle("@{R=UDT.Strings;K=UDTWEBJS_AK1_28;E=js}");

        // Set the location
        addWatchDialog.alignTo(document.body, "c-c");

        if (($("#isNonAdmin").length != 0) && !($("#isOrionDemoMode").length != 0)) {
            alert('@{R=UDT.Strings;K=UDTWEBJS_AK1_29;E=js}');
            return;
        }

        addWatchDialog.show();

        //return false;

    };

    function AddWatchServiceCall(id) {
        // restrict adding to watch list for demo server mode, unless user is an admin.
        if (($("#isOrionDemoMode").length != 0) && ($("#isNonAdmin").length != 0)) {
            alert('@{R=UDT.Strings;K=UDTWEBJS_AK1_30;E=js}');
            addWatchDialog.hide();
        }
        else {

            var _watchType = document.getElementsByName('WatchType');
            var _watchItemType = id;
            var _watchItemValue = $("#WatchValue").val();
            var _watchName = $("#Name").val();
            var _watchNote = $("#Description").val();

            if (_watchItemType == 1) {
                _watchItemValue = StripMacFormatting(_watchItemValue);
            }
            ORION.callWebService("/Orion/UDT/Services/Discovery.asmx", "AddDeviceWatchList", { watchItemType: _watchItemType, watchItemValue: _watchItemValue, watchName: _watchName, watchNote: _watchNote }, function (result) {
                if (result != "") {
                    //some error
                    Ext.Msg.alert('@{R=UDT.Strings;K=UDTWEBJS_AK1_31;E=js}', result);
                }
                //grid.getStore().load();
                addWatchDialog.hide();
            });
        }
    }

    //*** END OF WATCHLIST FUNCTIONS ***

    // GUID functions
    function S4() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }

    function guid() {
        return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
    }

    var selectorModel;
    var dataStore;
    var grid;
    var pageSizeNum;
    var radioItemChecked;
    var addWatchDialog;
    var timeoutHandle;
    var searchId;
    var cachedProgress;

    var groupByField = 'MatchItem';

    var progressBar;

    GetSearchProgress = function () {
        ORION.callWebService("/Orion/UDT/Services/SearchResults.asmx", "GetSearchProgress", { id: searchId }, function (result) {
            if (cachedProgress !== result) {
                //rebind, progress has been made
                cachedProgress = result;
                SW.UDT.AsyncSearch.RefreshGridData();
            }
            cachedProgress = result;
            if (result != 100) {
                //not complete yet... schedule next progress checking 
                //progressBar.updateProgress(cachedProgress / 100, cachedProgress + "%");
                progressBar.updateProgress(cachedProgress / 100, "");
                timeoutHandle = setTimeout(GetSearchProgress, 1000);
            } else {
                //progressBar.updateProgress(1, "100%");
                progressBar.updateProgress(1, "");
                timeoutHandle = setTimeout("DisplayProgress(false)", 1000);
                gridDisplayMask(false);
            }
        });
    };

    var redirectOnExactMatch = function () {
        var totalResults = dataStore.getTotalCount();
        if (totalResults == 1) {
            var firstRecord = dataStore.getAt(0);
            var url = getDetailsUrlForRecord(firstRecord);
            window.location = url;
        }
    };

    var getDetailsUrlForRecord = function (record) {

        var itemTypeToLower = record.data['MatchType'].toLowerCase();
        var matchedItem = record.data['MatchItem'].toLowerCase();

        // determine the 'Item Type' in order to call the relevant renderer for that type
        switch (itemTypeToLower) {
            case 'user name':
                return '/Orion/UDT/UserDetails.aspx?NetObject=UU:' + record.data['ID'].toString();
            case 'node name':
                return '/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:' + record.data['ID'].toString();
            case 'port description':
            case 'port name (description)':
                return '/Orion/UDT/PortDetails.aspx?NetObject=UP:' + record.data['ID'].toString();
            case 'endpoint hostname':
                return '/Orion/UDT/EndpointDetails.aspx?NetObject=UE-DNS:VAL=' + matchedItem;
            case 'endpoint ip address':
                return '/Orion/UDT/EndpointDetails.aspx?NetObject=UE-IP:VAL=' + matchedItem;
            default:
                // If the MAC Address is formatted check is here.
                if (matchedItem.indexOf(":") == -1)
                    matchedItem = formatMacAddressForDisplay(matchedItem);
                return '/Orion/UDT/EndpointDetails.aspx?NetObject=UE-MAC:VAL=' + matchedItem;
        }

    };

    return {

        RefreshGridData: function () {
            grid.getStore().proxy.conn.jsonData = { id: searchId, grouper: groupByField };
            grid.getStore().load();
        },

        Search: function () {

            var checkeditems = [];
            var searchColumnsCheckedTotal = 0;

            checkeditems = searchInColumnList.split(',');
            for (i = checkeditems.length; i--; ) {
                searchColumnsCheckedTotal += parseInt(checkeditems[i]);
            }

            // get a new GUID for the search
            searchId = guid();
            cachedProgress = 0;
            //progressBar.updateProgress(0, "0%");
            progressBar.updateProgress(0, "");
            $("#searchProgressLabel").show();
            $("#searchProgress").show();
            gridDisplayMask(true);

            ORION.callWebService("/Orion/UDT/Services/SearchResults.asmx", "InitiateSearch", { id: searchId, searchText: searchForString, searchTextMac: searchForMacString, searchTargets: searchColumnsCheckedTotal }, function (result) {
                //schedule next progress checking 
                timeoutHandle = setTimeout(GetSearchProgress, 1000);
            });
        },

        init: function () {

            pageSizeNum = parseInt(ORION.Prefs.load('PageSize', '10'));

            Ext.override(Ext.PagingToolbar, {
                addPageSizer: function () {

                    // add a combobox to the toolbar
                    var store = new Ext.data.SimpleStore({
                        id: 'pagingStore',
                        fields: ['pageSize'],
                        data: [[10], [20], [30], [50]]
                    });
                    var combo = new Ext.form.ComboBox({
                        regex: /^\d*$/,
                        store: store,
                        displayField: 'pageSize',
                        mode: 'local',
                        triggerAction: 'all',
                        selectOnFocus: true,
                        width: 50,
                        editable: false,
                        value: pageSizeNum
                    });
                    this.addField(new Ext.form.Label({ text: '@{R=UDT.Strings;K=UDTWEBJS_AK1_33;E=js}', style: 'margin-left: 5px; margin-right: 5px' }));
                    this.addField(combo);

                    combo.on("select", function (c, record) {
                        this.pageSize = record.get("pageSize");
                        this.cursor = 0;
                        ORION.Prefs.save('PageSize', this.pageSize);
                        this.onClick("refresh");
                    }, this);

                    this.addField(new Ext.form.Label({ id: 'searchProgressLabel', text: '@{R=UDT.Strings;K=UDTWEBJS_AK1_34;E=js}', style: 'text-align:right; margin-left: 50px; margin-right: 5px' }));
                    progressBar = new Ext.ProgressBar({
                        id: 'searchProgress',
                        text: '',
                        width: 100
                    });

                    this.addField(progressBar);

                    //                    var box = new Ext.BoxComponent({
                    //                        el: 'searchProgress'
                    //                    });
                    //                    
                    //                    progressBar = new Ext.ProgressBar({
                    //                        //text: '0%',
                    //                        text: '',
                    //                        width: 100,
                    //                        renderTo: Ext.get('searchProgress')
                    //                    });

                    //progressBar.show();

                }
            });

            //selectorModel = new Ext.grid.RowSelectionModel({ header: 'Match Item', hidden: false, width: 100, sortable: true, dataIndex: 'MatchItem', renderer: renderText, groupRenderer: renderText });
            selectorModel = new Ext.grid.CheckboxSelectionModel({ singleSelect: true, header: '', id: 'removeDefaultCheckbox', width: 30 });
            selectorModel.on("selectionchange", UpdateToolbarButtons);

            dataStore = new ORION.WebServiceGroupStore(
            "/Orion/UDT/Services/SearchResults.asmx/GetAsyncSearchResults",
                            [
                { name: 'MatchItem', mapping: 0 },
                { name: 'MatchType', mapping: 1 },
                { name: 'Status', mapping: 2 },
                { name: 'ID', mapping: 3 },
                { name: 'VendorIcon', mapping: 4 }
                ],
                "MatchItem");
            //                [
            //                { name: 'MatchItem', mapping: 0 },
            //                { name: 'MatchType', mapping: 5 },
            //                { name: 'FirstSeen', mapping: 1 },
            //                { name: 'LastSeen', mapping: 2 },
            //                { name: 'FirstSeenDisplay', mapping: 3 },
            //                { name: 'LastSeenDisplay', mapping: 4 },
            //                { name: 'Status', mapping: 6 },
            //                { name: 'ID', mapping: 7 }
            //                ],
            //                "MatchItem", groupByField);

            grid = new Ext.grid.GridPanel({

                store: dataStore,

                // ensure we prevent columns from being located to other positions
                enableColumnMove: false,

                columns: [
                    selectorModel,
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_AK1_14;E=js}', width: 100, sortable: true, dataIndex: 'MatchItem', renderer: renderText, groupRenderer: renderText },
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_AK1_15;E=js}', width: 80, sortable: true, dataIndex: 'MatchType', renderer: renderText, groupRenderer: renderText }
                //                        { header: 'First Seen', width: 80, sortable: true, dataIndex: 'FirstSeen', renderer: renderFirstSeen, groupRenderer: renderFirstSeen },
                //                        { header: 'Last Seen', width: 80, sortable: true, dataIndex: 'LastSeen', renderer: renderLastSeen, groupRenderer: renderLastSeen }
                //                //                        { header: 'First Seen Display', width: 80, sortable: true, dataIndex: 'FirstSeenDisplay', renderer: renderTextColumn, groupRenderer: renderText },
                //                        { header: 'Last Seen Display', width: 80, sortable: true, dataIndex: 'LastSeenDisplay', renderer: renderTextColumn, groupRenderer: renderText },
                //                        { header: 'Status', width: 50, hidden: false, sortable: true, dataIndex: 'Status', renderer: renderTextColumn, groupRenderer: renderText },
                //                        { header: 'ID', width: 30, hidden: false, sortable: true, dataIndex: 'ID', renderer: renderTextColumn, groupRenderer: renderText }
                ],

                sm: selectorModel,

                // Add grouping view, with checkbox
                //                view: new Ext.grid.GroupingView({
                //                    forceFit: true,
                //                    groupTextTpl: '{[values.rs[0].data["MatchItem"]]}',
                //                    //groupTextTpl: renderGroupingText('boo', values.rs[0].data["MatchType"]),
                //                    //startCollapsed: true,
                //                    enableGroupingMenu: false
                //                }),

                viewConfig: {

                    //set to true if you want to resize each column with the window resize.  False maintains existing column sizes.
                    forceFit: true //,
                    //emptyText: 'No results found'
                },

                stripeRows: true,

                // width: 700,
                height: 350,

                // top toobar on grid with Add to watch list button
                tbar: [
                    {
                        id: 'AddToWatchList',
                        text: '&nbsp;@{R=UDT.Strings;K=UDTWEBJS_AK1_16;E=js}',
                        iconCls: 'watchList',
                        handler: function () {
                            addToWatchList();
                        }
                    },
                    {
                        id: 'AddToWhiteList',
                        text: '&nbsp;@{R=UDT.Strings;K=UDTWEBJS_MK1_16;E=js}',
                        iconCls: 'watchList',
                        handler: function () {
                            addToWhiteList();
                        }
                    }
                ],

                // bottom toolbar
                bbar: new Ext.PagingToolbar(
                    {
                        id: 'asyncSearchResults',
                        store: dataStore,
                        pageSize: pageSizeNum,
                        displayInfo: true,
                        displayMsg: '@{R=UDT.Strings;K=UDTWEBJS_AK1_17;E=js}',
                        emptyMsg: "@{R=UDT.Strings;K=UDTWEBJS_VB1_28;E=js}"
                    }
                )
            });

            dataStore.on('beforeload', function () {

                grid.view.emptyText = "@{R=UDT.Strings;K=UDTWEBJS_AK1_19;E=js}";
            });

            dataStore.on('load', function () {

                // update the toolbar once store is loaded
                UpdateToolbarButtons();
                //alert("cachedProgress:" + cachedProgress);
                if (cachedProgress == 100) {
                    RebindNetObjectTips();
                    redirectOnExactMatch();
                }
            });


            grid.render('Grid');

            UpdateToolbarButtons(); // this sets the buttons to disabled by default

            grid.bottomToolbar.addPageSizer();

            // resize the grid
            gridResize();

            // retain original unformatted search text for matching MAC address highlighting
            searchForStringRaw = searchForString;
            // get the list of checked columns to search in
            searchInColumnList = ORION.Prefs.load('SearchItemList', '');
            //alert("searchInColumnList: " + searchInColumnList);

            // reformat search text for SQL query execution
            var searchForStringArray = formatSearchStringForSQL(searchForString, searchInColumnList);

            searchForString = searchForStringArray[0];
            searchForMacString = searchForStringArray[1];
           
            if (isOtherVendorSearch == true)
            {
                searchForString = 'other_vendor';
            }

            SW.UDT.AsyncSearch.Search(searchForString);

        }
    };

} ();


Ext.EventManager.onDocumentReady(SW.UDT.AsyncSearch.init, SW.UDT.AsyncSearch, true);
