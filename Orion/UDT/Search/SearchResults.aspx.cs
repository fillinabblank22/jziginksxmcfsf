﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common;

public partial class Orion_UDT_Search : System.Web.UI.Page
{

    internal bool _isOrionDemoMode = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        //Get Feature Codes from feature manager
        var featureManager = new FeatureManager();
        List<string> enabledFeatures = new List<string>(featureManager.EnabledFeatures);

        string searchString = Request.QueryString["q"] ?? String.Empty;
        string searchColumns = Request.QueryString["cols"] ?? String.Empty;

        pageTitleBar.PageTitle = Resources.UDTWebContent.UDTWEBCODE_AK1_10;

        //hide the customize page link for this page
        pageTitleBar.HideCustomizePageLink = true;
        //todo add help link
        pageTitleBar.HelpFragment = "OrionUDTPHNewSearchResults";

        if (searchString == "" || searchColumns == "")
        {
            litPageSubTitle.Text = String.Format(Resources.UDTWebContent.UDTWEBCODE_AK1_11, "<b>" + searchColumns + "</b>");
        }
        else
        {
            litPageSubTitle.Text = String.Format(Resources.UDTWebContent.UDTWEBCODE_AK1_12, "<b>" + searchString + "</b>", "<b>" + searchColumns + "</b>");
        }
    }

    protected override void OnInit(EventArgs e)
    {
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            _isOrionDemoMode = true;
        }

        base.OnInit(e);
    }
}