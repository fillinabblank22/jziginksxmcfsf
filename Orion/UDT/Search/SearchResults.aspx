﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SearchResults.aspx.cs" MasterPageFile="~/Orion/UDT/UdtPageWithoutRefresher.master" Inherits="Orion_UDT_Search" Title="<%$ Resources: UDTWebContent, UDTWEBCODE_AK1_10 %>" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register TagPrefix="udt" TagName="PageTitleBar" Src="~/Orion/UDT/Controls/PageTitleBar.ascx" %>
<%@ Register TagPrefix="udt" TagName="LearnMoreLink" Src="~/Orion/UDT/Controls/LearnMoreLink.ascx" %>

<asp:Content ID="udtHeader" ContentPlaceHolderID="UdtHeadPlaceHolder" Runat="Server">

    <orion:IncludeExtJs id="IncludeExtJs1" runat="server" debug="false" Version="3.4"/>
    <orion:Include File="../Orion/js/OrionCore.js" runat="server" />
    <orion:Include File="../Orion/UDT/Search/js/AsyncSearch.js" runat="server" />
    <style type="text/css">
        .smallText {color:#979797;font-size:11px;}
        .watchList {background-image:url(/Orion/UDT/Images/detective_16x16.png) !important; }
        #adminContent .dialogBody table td { padding-top: 5px; padding-left: 10px; }
        #adminContent .dialogBody ul { margin-left: 20px; list-style-type: square; }
        .learnMoreLink a {color: #336699 !important; font-size:11px;}
        .learnMoreLink a:hover { color: #f99d1c !important; font-size:11px;}
        #addWatchDialog .dialogBody table td { padding-right: 10px; }
        #addWatchDialog td.x-btn-center { text-align: center !important; }
        #addWatchDialog td { padding-right: 0; padding-bottom: 0; }
        #Grid td { padding-right: 0; padding-bottom: 0; vertical-align: middle; }
        .x-menu-item-text { padding-left: 0px !important; }
    </style>
</asp:Content>

<asp:Content id="SearchResultsTitle" ContentPlaceHolderID="TopRightPageLinks" runat="Server">
    <udt:PageTitleBar runat="server" id="pageTitleBar" ShowTitle="False"/>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ViewPageTitle">
    <h1 style="padding-bottom: 7px"><%= Resources.UDTWebContent.UDTWEBCODE_AK1_10 %></h1>
</asp:Content>

<asp:Content id="searchResults" ContentPlaceHolderID="UdtMainContentPlaceHolder" Runat="Server">
<input type="hidden" name="UDT_SearchResults_PageSize" id="UDT_SearchResults_PageSize" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("UDT_SearchResults_PageSize")%>' />
    <div style="padding-left: 10px; padding-bottom: 10px;"><asp:Literal runat="server" id="litPageSubTitle"></asp:Literal></div>
     <div id="searchContent" style="padding-left: 10px; padding-right: 10px;">	
        
        <table class="GridTable" cellpadding="0" cellspacing="0" width="100%" style="padding-top: 10px">
	        <tr valign="top" align="left">
		        <td id="gridCell" style="padding-right:0px;">
                    <div id="Grid" style="visibility:visible; display:block;"/>
		        </td>
	        </tr>
        </table>
	</div>
<div id="addWatchDialog" class="x-hidden">
	<div class="x-window-header"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_138 %></div>
	<div id="addWatchDialogBody" class="x-panel-body dialogBody" style="font-family: Arial;">
	<table style="padding:8px; margin:10px;">
        <tr>
	        <td colspan="2" style="padding-bottom:2px; width:inherit;" id="WatchType"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_139 %></td>
        </tr>
        <tr>
	        <td style="width:40px;">&nbsp;</td>
            <td style="padding-top:2px;"><input type="text" id="WatchValue" style="font-weight:bold; width:260px; border:0" /></td>
	    </tr>
        <tr id="VendorMacRow">
            <td style="width:40px;"><%= Resources.UDTWebContent.UDTWEBDATA_GK_1%></td>
            <td style="padding-top:2px;"><div id="VendorIcon"> </div></td>
        </tr>
    </table>
    <table style="padding:8px;  margin:10px;">
        <tr>
	        <td colspan="2" style="padding-bottom:4px; padding-top:20px;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_140 %></td>
	    </tr>
        <tr>
	        <td colspan="2" style="padding-bottom:4px;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_141 %></td>
	    </tr>
        <tr>
	        <td width="140"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_142 %></td>
            <td width="160" style="padding-top:2px;"><input type="text" id="Name" style="width:inherit" /></td>
	    </tr>
        <tr>
	        <td width="140" style="vertical-align:top;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_143 %></td>
            <td width="160" style="padding-bottom:2px;"><textarea id="Description" rows="5" cols="16" style="width:inherit; height:70px;"></textarea></td>
	    </tr>
    </table>
	</div>
</div>



 <%if (_isOrionDemoMode) {%> <div id="isOrionDemoMode" style="display: none;"/> <%}%>
 <%if (!Profile.AllowAdmin) {%> <div id="isNonAdmin" style="display: none;"/> <%}%>

 <script type="text/javascript"> 
    function RebindNetObjectTips() {
        SW.Core.NetObjectTips.Init({
            ViewLimitationID: <%= SolarWinds.Orion.Web.Limitation.GetCurrentViewLimitationID() %>,
            NetObjectTypeToTipPagePathRaw: <%= ControlHelper.ToJSON(NetObjectFactory.GetNetObjectTipMap()) %>
            }); 
     }

</script>


</asp:Content>
