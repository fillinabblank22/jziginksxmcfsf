﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserPopUp.aspx.cs" Inherits="Orion_UDT_UserPopUp" %>

<h3 class="<%=this.StatusCssClass%>"><%=SolarWinds.Orion.Web.Helpers.UIHelper.Escape(UDTUser.AccountName.Normalize()) %></h3>


<div class="NetObjectTipBody">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_171 %></th>	   
             <td colspan="2"><%=UDTUser.AccountName%></td> 
		</tr>
        <tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_172 %></th>
			<td colspan="2"><%=UDTUser.Title%></td>    
		</tr>
        <tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_173 %></th>
			<td colspan="2"><%=UDTUser.Department%></td>    
		</tr>
        <tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_174 %></th>
			<td colspan="2"><%=UDTUser.Office%></td>    
		</tr>
        <tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_175 %></th>
			<td colspan="2"><%=UDTUser.Company%></td>    
		</tr>
        <tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_176 %></th>
			<td colspan="2"><%=UDTUser.Address%></td>    
		</tr>
        <tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_177 %></th>
			<td colspan="2"><%=UDTUser.City%></td>    
		</tr>
        <tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_178 %></th>
			<td colspan="2"><%=UDTUser.State%></td>    
		</tr>
                <tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_179 %></th>
			<td colspan="2"><%=UDTUser.ZipCode%></td>    
		</tr>
        <tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_180 %></th>
			<td colspan="2"><%=UDTUser.CountryRegion%></td>    
		</tr>

<%--        <asp:Panel ID="IpPanel" runat="server">
        <tr>
			<th style="white-space: nowrap;"># of connected IPs:</th>
			<td colspan="2"><%=UDTPort.connectedIPsCount%></td> 
		</tr>--%>

	</table>
</div>

