﻿using System;
using SolarWinds.Orion.Web;
using SolarWinds.UDT.Web.Helpers;
using SolarWinds.UDT.Web.NetObjects;

public partial class Orion_UDT_EndpointMacPopUp : System.Web.UI.Page
{
    protected UDTEndpointNetObject UDTEndpoint { get; set; }

    protected string StatusCssClass
    {
        get
        {
            if (UDTEndpoint != null)
            {
                return UDTEndpoint.IsActive ? "StatusUp" : "StatusDown";
            }
            return "StatusUnknown";
        }
    }

    protected string StatusText
    {
        get
        {
            if (UDTEndpoint != null)
            {
                if (string.IsNullOrEmpty(UDTEndpoint.FirstSeen)) return Resources.UDTWebContent.UDTWEBCODE_AK1_14;

                return UDTEndpoint.IsActive ? Resources.UDTWebContent.UDTWEBCODE_AK1_15 : Resources.UDTWebContent.UDTWEBCODE_AK1_16;
            }
            return Resources.UDTWebContent.UDTWEBCODE_AK1_17;
        }
    }

    protected string FirstSeen
    {
        get
        {
            if (UDTEndpoint != null)
            {
                return UDTEndpoint.FirstSeen;
            }
            return string.Empty;
        }
    }

    protected string LastSeen
    {
        get
        {
            if (UDTEndpoint != null)
            {
                return UDTEndpoint.LastSeen;
            }
            return string.Empty;
        }
    }

    protected string LastUserLogIn
    {
        get
        {
            if (UDTEndpoint != null)
            {
                return UDTEndpoint.LastUserLogIn;
            }
            return string.Empty;
        }
    }

    protected string LastLogInTime
    {
        get
        {
            if (UDTEndpoint != null)
            {
                return UDTEndpoint.LastLogInTime;
            }
            return string.Empty;
        }
    }

    protected string VendorIcon
    {
        get
        {
            if (UDTEndpoint != null)
            {
                return IconHelper.GetVendorIcon(UDTEndpoint.Name,false);
            }
            return string.Empty;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        UDTEndpoint = NetObjectFactory.Create(Request["NetObject"], true) as UDTEndpointNetObject;
    }
}