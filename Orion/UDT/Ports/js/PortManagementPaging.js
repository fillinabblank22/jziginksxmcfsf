﻿
var delDialog;
var delPortsDialog;
var pollDialog;
var monitoringDialog;
var overLicenseLimitDialog;
var rwCommunityValidationDialog;
//var maxPorts; //maximum monitored port count that license allows
//var portCount; //current monitored port count

String.prototype.format = function (params) {
    var pattern = /{([^}]+)}/g;
    return this.replace(pattern, function ($0, $1) { return params[$1]; });
};

Array.prototype.unique = function () {
    var a = [], i, l = this.length;
    for (i = 0; i < l; i++) {
        if ($.inArray(this[i], a) < 0) { a.push(this[i]); }
    }
    return a;
};

Array.prototype.contains = function (element) {
    for (var i = 0; i < this.length; i++)
        if (this[i] == element) return true;
    return false;
};

Array.prototype.GetNameFromPortType = function(type) {
    for (var i = 0; i < this.length; i++) {
        if (this[i].type == type) {
            return this[i].name;
        }
    }
    return null;
};

var Pager =
{
    isInSearchMode: false,
    forbidReset: false,
    actualPage: 1,
    defaultPageSize: 40,
    maxPageSize: 250,
    pageSize: 40,
    totalCount: null,
    pageCount: null,
    GoToNextPage: function () {
        Pager.actualPage++;
        MNG.Prefs.save("UDTActualPage", Pager.actualPage);
        Pager.MakePaging();
        Pager.Init();
        return false;
    },
    GoToPreviousPage: function () {
        Pager.actualPage--;
        MNG.Prefs.save("UDTActualPage", Pager.actualPage);
        Pager.MakePaging();
        Pager.Init();
        return false;
    },
    GoToFirstPage: function () {
        Pager.actualPage = 1;
        MNG.Prefs.save("UDTActualPage", Pager.actualPage);
        Pager.MakePaging();
        Pager.Init();
        return false;
    },
    GoToLastPage: function () {
        Pager.actualPage = Pager.pageCount;
        MNG.Prefs.save("UDTActualPage", Pager.actualPage);
        Pager.MakePaging();
        Pager.Init();
        return false;
    },
    Init: function () {
        //console.info("Pager.Init - PageSize: " + Pager.pageSize + " ActualPage: " + Pager.actualPage + " TotalCount: " + Pager.totalCount);
        Pager.SetTotalObjectsCount(Pager.totalCount);

        // cause hiding select all panel if displayed
        MNG.DisplaySelectPan(false);

        if (Pager.totalCount <= Pager.pageSize && (Pager.totalCount < 2)) {
            $("#extPager").hide();
            $("#extPagerTop").hide();
        }
        else {
            $("#extPager").show();
            $("#extPagerTop").show();
            // if it isn't first page
            if (Pager.actualPage > 1) {
                $("#previousPageTable").removeClass("x-item-disabled");
                $("#previousPage").unbind('click', Pager.GoToPreviousPage);
                $("#previousPage").click(Pager.GoToPreviousPage);
                $("#previousPageTableTop").removeClass("x-item-disabled");
                $("#previousPageTop").unbind('click', Pager.GoToPreviousPage);
                $("#previousPageTop").click(Pager.GoToPreviousPage);

                $("#firstPageTable").removeClass("x-item-disabled");
                $("#firstPage").unbind('click', Pager.GoToFirstPage);
                $("#firstPage").click(Pager.GoToFirstPage);
                $("#firstPageTableTop").removeClass("x-item-disabled");
                $("#firstPageTop").unbind('click', Pager.GoToFirstPage);
                $("#firstPageTop").click(Pager.GoToFirstPage);
            }
            else {
                $("#previousPageTable").addClass("x-item-disabled");
                $("#previousPage").unbind('click', Pager.GoToPreviousPage);
                $("#previousPageTableTop").addClass("x-item-disabled");
                $("#previousPageTop").unbind('click', Pager.GoToPreviousPage);

                $("#firstPageTable").addClass("x-item-disabled");
                $("#firstPage").unbind('click', Pager.GoToFirstPage);
                $("#firstPageTableTop").addClass("x-item-disabled");
                $("#firstPageTop").unbind('click', Pager.GoToFirstPage);
            }

            //if it isn't last Page            
            if (Pager.actualPage < Pager.pageCount) {
                $("#nextPage").unbind('click', Pager.GoToNextPage);
                $("#nextPage").click(Pager.GoToNextPage);
                $("#nextPageTable").removeClass("x-item-disabled");
                $("#nextPageTop").unbind('click', Pager.GoToNextPage);
                $("#nextPageTop").click(Pager.GoToNextPage);
                $("#nextPageTableTop").removeClass("x-item-disabled");

                $("#lastPage").unbind('click', Pager.GoToLastPage);
                $("#lastPage").click(Pager.GoToLastPage);
                $("#lastPageTable").removeClass("x-item-disabled");
                $("#lastPageTop").unbind('click', Pager.GoToLastPage);
                $("#lastPageTop").click(Pager.GoToLastPage);
                $("#lastPageTableTop").removeClass("x-item-disabled");
            }
            else {
                $("#lastPage").unbind('click', Pager.GoToLastPage);
                $("#lastPageTable").addClass("x-item-disabled");
                $("#lastPageTop").unbind('click', Pager.GoToLastPage);
                $("#lastPageTableTop").addClass("x-item-disabled");

                $("#nextPage").unbind('click', Pager.GoToNextPage);
                $("#nextPageTable").addClass("x-item-disabled");
                $("#nextPageTop").unbind('click', Pager.GoToNextPage);
                $("#nextPageTableTop").addClass("x-item-disabled");
            }

            $("#pageNumber").val(Pager.actualPage);
            $("#pageNumberTop").val(Pager.actualPage);

            $("#totalPageCount").html(String.format("@{R=UDT.Strings;K=UDTWEBJS_AK1_112;E=js}", Pager.pageCount));
            $("#totalPageCountTop").html(String.format("@{R=UDT.Strings;K=UDTWEBJS_AK1_112;E=js}", Pager.pageCount));

            $("#displayInfo").html(Pager.GetDisplayInfo());
            $("#displayInfoTop").html(Pager.GetDisplayInfo());

            $("#pageSize").val(Pager.pageSize);
            $("#pageSizeTop").val(Pager.pageSize);

        }

        return false;
    },
    Reset: function () {
        //alert("Page reset");
        Pager.actualPage = 1;
        Pager.totalCount = 0;
        Pager.isInSearchMode = false;
        return false;
    },
    MakePaging: function () {
        if (Pager.isInSearchMode == true) {

            //var query = $("#search").val();
            MNG.RefreshObjects = function () {
                MNG.Sort.setArrow();
                MNG.search();
            };
        }
        else {
            MNG.RefreshObjects = MNG.RefreshObjectsByGroup;
        }
        //alert('NEW MakePaging set AllSelected to False');
        //sjw reset allSelected to false for any pager change operation otherwise buttons are not disabled correctly
        MNG.allSelected = false;
        MNG.RefreshObjects();
        return false;
    },
    SetPageSize: function (pageSize) {
        Pager.pageSize = pageSize;
        MNG.Prefs.save("UDTPageSize", Pager.pageSize);
        //Pager.pageCount = (Pager.totalCount - Pager.totalCount % Pager.pageSize) / Pager.pageSize + 1;
        Pager.pageCount = Math.max(1, Math.ceil(Pager.totalCount / Pager.pageSize));
        if (Pager.actualPage > Pager.pageCount) {
            Pager.actualPage = 1;
        }
        return false;
    },
    SetTotalObjectsCount: function (totalCount) {
        Pager.totalCount = totalCount;
        //Pager.pageCount = (Pager.totalCount - Pager.totalCount % Pager.pageSize) / Pager.pageSize + 1;
        Pager.pageCount = Math.max(1, Math.ceil(Pager.totalCount / Pager.pageSize));
        if (Pager.actualPage > Pager.pageCount) {
            Pager.SetActualPage(Pager.pageCount);
        }
        return false;
    },
    SetActualPage: function (actualPage) {
        Pager.actualPage = actualPage;
        MNG.Prefs.save("UDTActualPage", Pager.actualPage);
    },
    GetDisplayInfo: function () {
        var itemsCount;
        var endInterval = Pager.actualPage * Pager.pageSize;

        if (endInterval > Pager.totalCount) {
            itemsCount = Pager.totalCount;
        }
        else {
            itemsCount = endInterval;
        }
        var info = String.format("@{R=UDT.Strings;K=UDTWEBJS_AK1_35;E=js}", ((Pager.actualPage - 1) * Pager.pageSize + 1), itemsCount, Pager.totalCount);  // alert(info);
        return info;
    },

    // load web user settings preferences
    LoadSettings: function () {
        var pageSize = MNG.Prefs.load("UDTPageSize", Pager.defaultPageSize);
        Pager.SetPageSize(pageSize);
        var totalCount = MNG.Prefs.load("UDTTotalCount", 0);
        Pager.SetTotalObjectsCount(totalCount);
        var actualPage = MNG.Prefs.load("UDTActualPage", 1);
        Pager.actualPage = actualPage;
        return false;
    },

    //
    PageSizeChanging: function (e) {
        if (e.which == 13) {
            if ($(this).val() < 1) {
                $(this).val(1);
                alert(String.format("@{R=UDT.Strings;K=UDTWEBJS_AK1_57;E=js}", Pager.defaultPageSize, Pager.maxPageSize));
            }
            else
                if ($(this).val() > Pager.maxPageSize) {
                    $(this).val(Pager.maxPageSize);
                    alert(String.format("@{R=UDT.Strings;K=UDTWEBJS_AK1_58;E=js}", Pager.maxPageSize, Pager.defaultPageSize));
                }

            Pager.SetPageSize($(this).val());
            Pager.MakePaging();
            Pager.Init();
            return false;
        }
        else {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        }
    },
    PageNumberChanging: function (e) {
        if (e.which == 13) {
            if ($(this).val() < 1 || $(this).val() > Pager.pageCount) {
                $(this).val(Pager.actualPage);
                return false;
            }
            else {
                Pager.SetActualPage($(this).val());
                Pager.MakePaging();
                Pager.Init();
                return false;
            }
        }
        else {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //$("#pageNumber").val(Pager.actualPage);
                return false;
            }
        }
    },
    Hide: function () {
        $("#extPager").hide();
        $("#extPagerTop").hide();
    },
    GetSetting: function () {
        return "pageSize: " + Pager.pageSize + " actualPage: " + Pager.actualPage + " totalCount: " + Pager.totalCount + " pageCount: " + Pager.pageCount;
    }
};


var MNG = {
    allSelected: false,
    allowInterfaces: function () {
        var allowInterfaces = $('#AllowInterfaces').val();
        if (allowInterfaces == "True")
            return true;
        return false;
    },
    RefreshObjects: null,
    CancelLoading: function () { /* do nothing */ },
    expandImg: '/Orion/UDT/Ports/images/icons/Button.Expand.gif',
    collapseImg: '/Orion/UDT/Ports/images/icons/Button.Collapse.gif',
    chooseColumnsImg: '/Orion/UDT/Ports/images/icons/double_chevron_rt.gif',
    editPropertiesUrl: '/Orion/Nodes/{0}Properties.aspx?{0}s={1}&ReturnTo={2}',
    interfaceEditPropertiesUrl: '/Orion/NPM/{0}Properties.aspx',
    //listResourcesUrl: '/Orion/Nodes/ListResources.aspx?Nodes={0}&ReturnTo={1}',
    customPollerUrl: '/Orion/NPM/{0}CustomPollers.aspx?ReturnTo={1}',
    nodePageSize: 250,
    suspendEnableDisableMenu: false,
    Nodes: {
        AvailableColumns: [
			{ sortType: "Node", sortProp: "StatusDescription", header: "Status", headerText: "@{R=UDT.Strings;K=UDTWEBJS_AK1_39;E=js}" },
            { sortType: "Node", sortProp: "IPAddress", header: "IP Address", headerText: "@{R=UDT.Strings;K=UDTWEBJS_AK1_40;E=js}" },
			{ sortType: "Node", sortProp: "Contact", header: "Contact", headerText: "@{R=UDT.Strings;K=UDTWEBJS_AK1_41;E=js}" },
			{ sortType: "Node", sortProp: "Location", header: "Location", headerText: "@{R=UDT.Strings;K=UDTWEBJS_AK1_42;E=js}"}//,
            //{ sortType: "Node", sortProp: "ObjectSubType", header: "SNMP/ICMP" }
		],
        DefaultColumns: "IP Address,Status"
    },

    Ports: {
        AvailableColumns: [
            { sortType: "Node", sortProp: "Caption", selectAlias: "Node", header: "Node", headerText: "@{R=UDT.Strings;K=UDTWEBJS_AK1_43;E=js}" },
            { sortType: "Port", sortProp: "MACAddress", header: "MAC Address", headerText: "@{R=UDT.Strings;K=UDTWEBJS_AK1_44;E=js}" },
            { sortType: "Port", sortProp: "PortIndex", header: "Index", headerText: "@{R=UDT.Strings;K=UDTWEBJS_AK1_45;E=js}" },
            { sortType: "Port", sortProp: "PortType", header: "Type", headerText: "@{R=UDT.Strings;K=UDTWEBJS_AK1_46;E=js}" },
            { sortType: "Port", sortProp: "PortDescription", header: "Description", headerText: "@{R=UDT.Strings;K=UDTWEBJS_AK1_47;E=js}" },
            { sortType: "Port", sortProp: "IsMonitored", header: "Monitored", headerText: "@{R=UDT.Strings;K=UDTWEBJS_AK1_48;E=js}" }
		],
        DefaultColumns: "Node,MAC Address,Monitored"
    },
    NetObjects: null,
    Sort: {
        type: "Node",
        prop: "Caption",
        ascending: true,
        getSortHeader: function () {
            return $('th[sortProp=' + MNG.Sort.prop + '][sortType=' + MNG.Sort.type + ']');
        },
        setArrow: function () {
            $("#sortArrow").parent().removeClass('sortColumn');
            $("#sortArrow").attr('src', '/Orion/UDT/Ports/images/icons/arrow_sort' + (MNG.Sort.ascending ? 'Asc' : 'Desc') + '.gif').appendTo(MNG.Sort.getSortHeader());
            $("#sortArrow").parent().addClass('sortColumn');
        },
        getOrderBy: function (nodeAlias, interfaceAlias) {
            var tableAlias = (MNG.Sort.type == "Node") ? nodeAlias : interfaceAlias;
            return " ORDER BY {0}.{1} {2}".format([tableAlias, MNG.Sort.prop, MNG.Sort.ascending ? " ASC" : " DESC"]);
        },
        by: function (newType, newProp) {
            if (newProp == this.prop && newType == this.type) {
                this.ascending = !this.ascending;
            } else {
                this.ascending = true;
                this.prop = newProp;
                this.type = newType;
            }
            MNG.Prefs.save("UDT" + MNG.NetObjects.typename + "SortPropType", this.type);
            MNG.Prefs.save("UDT" + MNG.NetObjects.typename + "SortProp", this.prop);
            MNG.Prefs.save("UDT" + MNG.NetObjects.typename + "SortAsc", this.ascending);
            
            //alert('NEW change allselected to false');
            MNG.allSelected = false;
            this.setArrow();
        },
        loadPrefs: function () {
            this.type = MNG.Prefs.load("UDT" + MNG.NetObjects.typename + "SortPropType", "Node");
            this.prop = MNG.Prefs.load("UDT" + MNG.NetObjects.typename + "SortProp", "Caption");
            if (this.getSortHeader().length == 0) {
                this.type = "Node";
                this.prop = "Caption";
            }
            this.ascending = (MNG.Prefs.load("UDT" + MNG.NetObjects.typename + "SortAsc", "true") != "false");
            this.setArrow();
        }
    },
    Prefs: {
        load: function (prefName, defaultValue) {
            return $("#WebUDTPortManagement_" + prefName).val() || defaultValue;
        },
        save: function (prefName, value) {
            $("#WebUDTPortManagement_" + prefName).val(value);
            MNG.SaveUserSetting("WebUDTPortManagement_" + prefName, value);
        }
    },
    removeBrackets: function (ident) {
        if (ident.charAt(0) === '[' && ident.charAt(ident.length - 1) === ']')
            return ident.slice(1, ident.length - 1);
        else
            return ident;
    }
};

var SWQLBaseQueries = {
    PortsBase:
        " FROM Orion.UDT.Port (nolock=true) P \
        INNER JOIN Orion.UDT.NodeCapability (nolock=true) S ON P.NodeID=S.NodeID \
        INNER JOIN Orion.Nodes (nolock=true) N ON S.NodeID=N.NodeID \
	    WHERE  "
}

var SWQLQueries = {
        SelectPortIds:
            "SELECT DISTINCT P.PortID" + SWQLBaseQueries.PortsBase,

        SelectPortIdsAndIsMonitored:
            "SELECT DISTINCT P.PortID, P.IsMonitored" + SWQLBaseQueries.PortsBase + " P.IsMonitored = 1 AND ",
        
        SelectPortIdsAndIsNotMonitored:
            "SELECT DISTINCT P.PortID, P.IsMonitored" + SWQLBaseQueries.PortsBase + " P.IsMonitored = 0 AND ",
 
        SelectPortAndEngineIds:
            "SELECT N.EngineID, P.PortID" + SWQLBaseQueries.PortsBase,

    //    SelectPortIdsAndNames:
    //        "SELECT P.PortID, P.Name " + SWQLBaseQueries.PortsBase,

        SelectNodeIds:
    	    "SELECT DISTINCT N.NodeID FROM Orion.Nodes (nolock=true) N INNER JOIN Orion.UDT.NodeCapability (nolock=true) S ON S.NodeID=N.NodeID WHERE ",

        SelectNodeAndEngineIds:
    	    "SELECT DISTINCT N.EngineID, N.NodeID FROM Orion.Nodes (nolock=true) N INNER JOIN Orion.UDT.NodeCapability (nolock=true) S ON S.NodeID=N.NodeID WHERE ",

    //    SelectSNMPNodesIds:
    //	    "SELECT N.NodeID FROM Orion.Nodes (nolock=true) N WHERE N.ObjectSubType = 'SNMP' AND ",

        SelectUnmanageNodeCount:
    	    "SELECT COUNT(DISTINCT N.NodeID) AS Cnt FROM Orion.Nodes (nolock=true) N INNER JOIN Orion.UDT.NodeCapability (nolock=true) S ON S.NodeID=N.NodeID WHERE N.UnManaged = 1 AND ",
            

        SelectManageNodeCount:
    	    "SELECT COUNT(DISTINCT N.NodeID) AS Cnt FROM Orion.Nodes (nolock=true) N INNER JOIN Orion.UDT.NodeCapability (nolock=true) S ON S.NodeID=N.NodeID WHERE N.UnManaged = 0 AND ",

        SelectUnmonitoredPortCount:
    	    "SELECT COUNT(DISTINCT P.PortID) AS Cnt " + SWQLBaseQueries.PortsBase + " P.IsMonitored = 0 AND ",

        SelectMonitoredPortCount:
    	    "SELECT COUNT(DISTINCT P.PortID) AS Cnt " + SWQLBaseQueries.PortsBase + " P.IsMonitored = 1 AND ",

        SelectShutdownPortCount:
    	    "SELECT COUNT(DISTINCT P.PortID) AS Cnt " + SWQLBaseQueries.PortsBase + " P.Administrativestatus = 2 AND ",
    
        SelectEnablePortCount:
    	    "SELECT COUNT(DISTINCT P.PortID) AS Cnt " + SWQLBaseQueries.PortsBase + " P.Administrativestatus = 1 AND ",

        SelectUnMonitoredNodeIds:
                "SELECT DISTINCT N.NodeID FROM Orion.Nodes (nolock=true) N LEFT JOIN Orion.UDT.NodeCapability (nolock=true) UNC ON N.NodeID=UNC.NodeID WHERE UNC.NodeID IS NULL AND N.ObjectSubType = 'SNMP' AND ",

    //    SelectNonExternalNodeCount:
    //        "SELECT COUNT(N.NodeID) AS Cnt FROM Orion.Nodes N WHERE N.Status <> 11 AND ",

    GetSearchWhereFromPage: function () {
        var fields = { Node: [], Port: [] };

        $("th[sortProp]").each(function () {
            fields[$(this).attr('sortType')].push($(this).attr('sortProp'));
        });

        //var query = $("#search").val();
        var query = filterText;
        
        var searchClauses = function (fields, tableAlias) {
            var searchClause = function (field) { return "{0}.{1} LIKE '%{2}%'".format([tableAlias, field, MNG.quoteValueForSql(query)]); }
            return $.map(fields, searchClause);
        };

        var clauses = searchClauses(fields.Node, "N").concat(searchClauses(fields.Port, "P"));
        return "(" + clauses.join(" OR ") + ")";
    }
}

var Utils = {
    // returns selected option for grouping combo like Vendor, Machine Type, SNMP Version ...
    SelectedType: function () {
        return $("#groupByProperty option:selected").attr("propertyType");
    },

    // returns selected option value for grouping combo like Vendor, MachineType, SNMPVersion ...
    SelectedProperty: function () {
        return $("#groupByProperty option:selected").val();
    },

    // returns selected list item
    SelectedItem: function () {
        return $(".SelectedNodeGroupItem a");
    },

    // returns selected list item value
    SelectedValue: function () {
        return $(".SelectedNodeGroupItem a").attr('value');
    },

    EnableByID: function (id, enabled) {
        if (enabled) {
            $("#" + id).parent().removeClass("Disabled");
        }
        else {
            $("#" + id).parent().addClass("Disabled");
        }
    },

    IsEnabledByID: function (id) {
        return !$("#" + id).parent().hasClass("Disabled");
    }
}

var GUI = {
    allPagesSelected: false,

    SelectAllPages: function (value) {
        GUI.allPagesSelected = value;

        GUI.EnableDeleteButton(value);
    },

    EnableDeleteButton: function (enabled) { Utils.EnableByID("deleteLink"); },


    ProgressUdtDeleteDlg: function (options) {
        var width = 600;
        var bufferSize = 10;
        var statusBox = $('<div></div>').addClass('StatusDialog').width(width);
        var statusContent = $('<div></div>').appendTo(statusBox);
        var progressBarOuter = $('<div></div>').addClass('ProgressBarOuter').appendTo(statusContent);
        var progressBarInner = $('<div></div>').addClass('ProgressBarInner').appendTo(progressBarOuter);
        var progressText = $('<div></div>').addClass('ProgressText').appendTo(statusBox);
        var lastError = $('<div></div>').addClass('LastError').appendTo(statusBox).hide();

        $(SW.Core.Widgets.Button('Close', { cls: 'CloseButton' })).appendTo(statusBox).click(function () { statusBox.dialog('close').remove(); });
        
        $('body').append(statusBox);
        var numFinished = 0, numSucceeded = 0;

        statusBox.dialog({
            width: width, height: 170, modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: options.title
        });

        var onFinished = function (succeeded) {
            progressBarInner.width((++numFinished / options.items.length) * 100.0 + '%');

            if (succeeded && ++numSucceeded >= options.items.length) {
                MNG.EnableDisableMenu();
                setTimeout("$('.StatusDialog').fadeOut('slow', function() { $('.StatusDialog').dialog('destroy').remove(); })", 1500);
            }

            if (numFinished == numSucceeded) {
                progressText.text("@{R=UDT.Strings;K=UDTWEBJS_AK1_77;E=js}".format([numFinished, options.items.length]));
            }
            else {
                progressText.text("@{R=UDT.Strings;K=UDTWEBJS_AK1_78;E=js}".format([numFinished, options.items.length, numFinished - numSucceeded]));
            }

            if (options.afterFinished && options.items.length == numFinished) {
                options.afterFinished();
            }

        };

        var firstItems;
        var bufferedItems;
        if (options.items.length > bufferSize) {
            firstItems = options.items.slice(0, bufferSize);
            bufferedItems = options.items.slice(bufferSize, options.items.length);
        }
        else {
            firstItems = options.items;
            bufferedItems = new Array();
        }

//        //alert('options url:' + options.urlName + ' methodName:' + options.methodName + ' items:' + options.items);
//        var bufferedItems = [];

//        // clone the original array into our working array
//        bufferedItems = options.items.slice(0, options.items.length);

////        // alert('buffered items count start: '+ bufferedItems.length);
////        while (bufferedItems.length > bufferSize) {
////            //while (bufferedItems.length > 0) {
////            serverMethodCall(bufferedItems.splice(0, bufferSize));
////            //serverMethodCall(bufferedItems.splice(0, 1));
////            //              onFinished(true);

////        }
////        serverMethodCall(bufferedItems);

//alert('url: ' + options.urlName + options.methodName);
        var serverMethodCall = function (items) {
            //if ($.browser.msie && $.browser.version.substr(0, 1) > 7) {
            //alert('items: ' + items);
            $.ajax({
                type: "POST",
                url: options.urlName + options.methodName,
                data: "{netObjectIds:[" + items + "]}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                timeout: '12000000',
                success: function (data) {
                    // alert('success occurred');
                    if (data) {
                        //alert('data:' + data);
                        onFinished(true);
                    }

//                    if (bufferedItems.length > bufferSize) {
//                        //while (bufferedItems.length > 0) {
//                        serverMethodCall(bufferedItems.splice(0, bufferSize));
//                    }

                    if (bufferedItems.length > 0)
                        serverMethodCall(bufferedItems.pop());
                },
                error: function (error) {
                    //alert('error occurred');
                    if (error.statusText == "Authentication failed.") {
                        alert('@{R=UDT.Strings;K=UDTWEBJS_AK1_60;E=js}');
                        window.location.reload();
                    } else {
                        MNG.ReportError(error.statusText, lastError);
                    }
                    onFinished(false);

                    if (bufferedItems.length > 0)
                        serverMethodCall(bufferedItems.pop());
                }
            });
           // }
        };
        //begin the web method calls
        $.each(firstItems, function () {
            serverMethodCall(this);
        });
  },


    ProgressDiscDlg: function (options) {
        var width = 600;
        var bufferSize = 10; // SJW: reduced from 100 to try prevent web service call timeout;
        var statusBox = $('<div></div>').addClass('StatusDialog').width(width);
        var statusContent = $('<div></div>').appendTo(statusBox);
        var progressBarOuter = $('<div></div>').addClass('ProgressBarOuter').appendTo(statusContent);
        var progressBarInner = $('<div></div>').addClass('ProgressBarInner').appendTo(progressBarOuter);
        var progressText = $('<div></div>').addClass('ProgressText').appendTo(statusBox);
        var lastError = $('<div></div>').addClass('LastError').appendTo(statusBox).hide();

        $(SW.Core.Widgets.Button('Close', { cls: 'CloseButton' })).appendTo(statusBox).click(function () { statusBox.dialog('close').remove(); });
        
        $('body').append(statusBox);
        var numFinished = 0, numSucceeded = 0;
        var updateTree = function (arr, res) {
            if (arr.length >= 2) {
                if (arr[0].toUpperCase() == "N") {

                    $("img.StatusIcon[NetObject=N:" + arr[1] + "]").attr('src', '/Orion/images/StatusIcons/Small-' + res[0] + ((res[0].indexOf('-') < 0) || MNG.BlinkingNodeIcons ? "" : "-noblink") + '.gif');
                    $.each($("input:checkbox[name=N:" + arr[1] + "]"), function () {

                        $(this).data('netobject').Unmanaged = (res[2] == 'True');
                        for (var n = 0; n < MNG.Nodes.selectedColumns.length; ++n) {
                            if (MNG.Nodes.selectedColumns[n].sortProp == "StatusDescription") {
                                $(this).parent().parent().find("td:eq(" + (n + 2) + ")").text(res[1]);
                                break;
                            }
                        }
                    });
                }

                //sjw: need to get Port Pop-up showing here
                else if (arr[0].toUpperCase() == "P") {
                    $("img.StatusIcon[NetObject=UP:" + arr[1] + "]").attr('src', '/Orion/images/StatusIcons/Small-' + res[0] + '.gif');
                    $.each($("input:checkbox[name=P:" + arr[1] + "]"), function () {
                        $(this).data('netobject').AdminStatus = res[1];
                    });
                }
            }
        };

        var onFinished = function (succeeded) {
            progressBarInner.width((++numFinished / options.items.length) * 100.0 + '%');

            if (succeeded && ++numSucceeded >= options.items.length) {
                MNG.EnableDisableMenu();
                setTimeout("$('.StatusDialog').fadeOut('slow', function() { $('.StatusDialog').dialog('destroy').remove(); })", 1500);
            }

            if (numFinished == numSucceeded) {
                progressText.text("@{R=UDT.Strings;K=UDTWEBJS_AK1_77;E=js}".format([numFinished, options.items.length]));
            }
            else {
                progressText.text("@{R=UDT.Strings;K=UDTWEBJS_AK1_78;E=js}".format([numFinished, options.items.length, numFinished - numSucceeded]));
            }

            if (options.afterFinished && options.items.length == numFinished) {
                options.afterFinished();
            }

        };

        var firstItems;
        var bufferedItems;
        if (options.items.length > bufferSize) {
            firstItems = options.items.slice(0, bufferSize);
            bufferedItems = options.items.slice(bufferSize, options.items.length);
        }
        else {
            firstItems = options.items;
            bufferedItems = new Array();
        }

        var serverMethodCall = function (item) {
            var ids = [];
            ids.push(options.getArg(item).toString());
            //ids.push(item);
            //alert('options url:' + options.urlName + ' methodName:' + options.methodName + ' ids:' + ids);
            if ($.browser.msie && $.browser.version.substr(0, 1) > 7) {
                $.ajax({
                    type: 'Post',
                    //url: '/Orion/Services/NodeManagement.asmx/' + options.methodName,
                    url: options.urlName + options.methodName,
                    data: "{'netObjectIds':" + "'" + ids + "\'}",
                    contentType: 'application/json; charset=utf-8',
                    timeout: '12000000',
                    success: function (result) {
                        //alert('success occurred');
                        onFinished(true);
                        if (result && result.replace('{"d":', '').replace('}', '') != 'null') {
                            var arr = item.split(":");
                            var res = result.replace('{"d":"', '').replace('"}', '').split("::");
                            updateTree(arr, res);
                        }

                        if (bufferedItems.length > 0)
                            serverMethodCall(bufferedItems.pop());
                    },
                    error: function (error) {
                        //alert('error occured');
                        if (error.statusText == "Authentication failed.") {
                            alert('@{R=UDT.Strings;K=UDTWEBJS_AK1_60;E=js}');
                            window.location.reload();
                        } else {
                            MNG.ReportError(error.statusText, lastError);
                        }
                        onFinished(false);

                        if (bufferedItems.length > 0)
                            serverMethodCall(bufferedItems.pop());
                    }
                });
            }
            else {
                options.serverMethod(options.getArg(item).toString(), function (result) {
                    onFinished(true, item);

                    if (result) {
                        var arr = item.split(":");
                        var res = result.split("::");
                        updateTree(arr, res);
                    }

                    if (bufferedItems.length > 0)
                        serverMethodCall(bufferedItems.pop());

                }, function (error) {
                    options.afterFail(error, lastError);

                    onFinished(false);

                    if (bufferedItems.length > 0)
                        serverMethodCall(bufferedItems.pop());
                });
            }
        };

        $.each(firstItems, function () {
            serverMethodCall(this);
        });

        statusBox.dialog({
            width: width, height: 170, modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: options.title
        });
    },

    ProgressDlg: function (options) {
        var width = 800;
        var statusBox = $('<div></div>').addClass('StatusDialog').width(width);
        var statusContent = $('<div></div>').appendTo(statusBox);
        var progressBarOuter = $('<div></div>').addClass('ProgressBarOuter').appendTo(statusContent);
        var progressBarInner = $('<div></div>').addClass('ProgressBarInner').appendTo(progressBarOuter);
        var spinner = $('<div></div>').addClass('Spinner').appendTo(statusBox);
        var statusItems = $('<ul></ul>').addClass('InterfacesText').appendTo(statusContent);

        $(SW.Core.Widgets.Button('Close', { cls: 'CloseButton' })).appendTo(statusBox).click(function () { statusBox.dialog('close').remove(); });
        
        $('body').append(statusBox);
        var numFinished = 0, numSucceeded = 0;
        var onFinished = function (succeeded) {
            progressBarInner.width((++numFinished / options.items.length) * 100.0 + '%');
            if (numFinished == options.items.length)
                spinner.hide();
            if (succeeded && ++numSucceeded >= options.items.length)
                $('.StatusDialog').fadeOut('slow', function () { statusBox.dialog('destroy').remove(); });
        };
        $.each(options.items, function () {
            var item = this;
            var caption = options.getObject(item);
            var statusBlock = $('<li></li>').appendTo(statusItems).text(caption);
            options.serverMethod(options.getArg(item), function () {
                statusBlock.append(' - ' + '<font color="green">ok</font>');
                options.afterSucceeded(item);
                onFinished(true);
            }, function (error) {
                var before = statusItems.height();
                statusBlock.append(' - ' + '<font color="red">' + error.get_message() + '</font>');

                if (statusItems.height() > before) {
                    var val = $('.StatusDialog').height() + statusItems.height() - before;
                    $('.StatusDialog').data('height.dialog', val);
                }
                onFinished(false);
            });
        });

        statusBox.dialog({
            width: width, height: statusContent.height() + 120, modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: options.title
        });
    },
    InfoDialog: function (title, text) {
        //create the dialog window if it doesn't already exist
        if (!pollDialog) {
            pollDialog = new Ext.Window({
                applyTo: 'pollDialog',
                layout: 'fit',
                width: 310,
                height: 205,
                closeAction: 'destroy',
                modal: true,
                plain: true,
                resizable: false,
                //ids: ids,
                items: new Ext.BoxComponent({
                    applyTo: 'pollDialogBody',
                    layout: 'fit',
                    border: false
                }),
                buttons: [
                        {
                            text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_5;E=js}',
                            handler: function () {
                                pollDialog.hide();
                            }
                        }
                        ]
            });
        }
        $("#pollDialogHeader").text(title);
        $("#pollDialogDescription").text(text);
        // Set the location
        pollDialog.alignTo(document.body, "c-c");
        pollDialog.show();
    },



    ProgressMonitorNodeByUDTDlg: function (options) {    
       
        $.ajax({
            type: "POST",
            url: options.urlName + options.methodName,
            data: "{netObjectIds:[" + options.items + "]}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            timeout: '12000000',
            success: function (data) {

                $("#bannerImportNodesToUDT").show();
                $("#showObjectType").change();
                
            },
            error: function (error) {               
                if (error.statusText == "Authentication failed.") {
                    alert('@{R=UDT.Strings;K=UDTWEBJS_AK1_60;E=js}');
                    window.location.reload();
                } else {
                    MNG.ReportError(error.statusText, lastError);
                }
                

              
            }
        });
     
    
},
}

// Operation delagates
var Operations = {

    ShowStringArray: function (value) {
        alert(value.join(","));
    },

    DeleteObjects: function (value) {
        if (confirm(String.format('@{R=UDT.Strings;K=UDTWEBJS_AK1_84;E=js}', value.length))) {
            NodeManagement.DeleteObjects(value);
            MNG.LoadGroupByValues();
        }
    },

    DeleteNetObjects: function (value) {
        if (confirm(String.format('@{R=UDT.Strings;K=UDTWEBJS_AK1_84;E=js}', value.length))) {
            NodeManagement.DeleteNetObjects(value, function () { }, function (error) {
                if (error.get_message() == "Authentication failed.") {
                    // login cookie expired. reload the page so we get bounced to the login page.
                    alert('@{R=UDT.Strings;K=UDTWEBJS_AK1_60;E=js}');
                    window.location.reload();
                } else {
                    window.location = "/Orion/WCFError.aspx?Message=" + error.get_message();
                }
            });
            MNG.LoadGroupByValues();
        }
    },

    //    UnmanageObjects: function (objectsIds) {
    //        var ids = { N: [], I: [] }

    //        if (MNG.NetObjects.SelectedPrefix() == "N") {
    //            $(objectsIds).each(function () { ids.N.push(this); });
    //        }
    //        else if (MNG.NetObjects.SelectedPrefix() == "I") {
    //            $(objectsIds).each(function () { ids.I.push(this); });
    //        }

    //        showUnmanageDialog(ids);
    //    },

    UnmanageObjects: function (objectsIds) {
        var ids = { N: [] }

        if (MNG.NetObjects.SelectedPrefix() == "N") {
            $(objectsIds).each(function () { ids.N.push(this); });
        }
        //alert('passing N ids: ' + ids);
        showUnmanageDialog(ids);
    },

    UnmonitorObjects: function (objectsIds) {
        //        var ids = { P: [] }
        //alert('MNG.Ports.UnmonitorObjects: P.len: ' + objectsIds.P.length + ' / Monitored.len: ' + objectsIds.Monitored.length + ' / Unmonitored.len: ' + objectsIds.Unmonitored.length);
        //        //if (MNG.NetObjects.SelectedPrefix() == "P") {
        //        $(objectsIds).each(function () {
        //            ids.P.push(this.ID);
        //        });
        //        //}
        //        alert('passing P ids to UnMonitor: ' + ids);
        MNG.changePortMonitoring(objectsIds, "unmonitor");
    },

    RemonitorObjects: function (objectsIds) {
        //      var ids = { P: [], Monitored: [] }
        //alert('MNG.Ports.RemonitorObjects: P.len: ' + objectsIds.P.length + '  / Monitored.len: ' + objectsIds.Monitored.length + ' / Unmonitored.len: ' + objectsIds.Unmonitored.length);

        //        if (MNG.NetObjects.SelectedPrefix() == "P") {
        //            $(objectsIds).each(function () { 
        //                ids.P.push(this); 
        //                if(this.IsMonitored){ids.Monitored.push(this);}
        //            });
        //        }

        MNG.changePortMonitoring(objectsIds, "monitor");
    },




    EditProperties: function (joinedIds) {
        if (MNG.NetObjects.SelectedPrefix() == "N")
            window.location = MNG.editPropertiesUrl.format(["Node", joinedIds, $('#ReturnToUrl').val()]);
        else if (MNG.NetObjects.SelectedPrefix() == "P") {
            $('body').append(Operations.PreparePostForm("Port", joinedIds, $('#ReturnToUrl').val()));
            $('#selectedNetObjects').submit();
        }
    },

    DoUdtPortDelete: function (ids) {

        if (MNG.allSelected) {
            // do for all.  returns [] of ids
            //alert('delete ALL Ports:' + ids);
        }
        else {
            // do only for selected items or all on page                    
            var selectedIDs = MNG.GetSelectedObjectIDs();
            //alert('delete selected PortIds:' + selectedIDs.P);
            ids = selectedIDs.P;
        }

        //        if (ids.length > 0) {
        //            //alert('countIds:' + ids.length);
        //        }

        GUI.ProgressUdtDeleteDlg({
            title: "@{R=UDT.Strings;K=UDTWEBJS_AK1_79;E=js}",
            items: ids,
            urlName: '/Orion/UDT/Services/UdtPortManagement.asmx/',
            methodName: 'UdtDeletePorts'

            //            afterSucceeded: function (element, lastError) {
            //                alert('SUCCEEDED');
            //            },
            //            afterFinished: function () {
            //                alert('FINISHED');
            //                MNG.LoadGroupByValues();
            //            },
            //            afterFail: function (error, lastError) {
            //                if (error.get_message() == "Authentication failed.") {
            //                    // login cookie expired. reload the page so we get bounced to the login page.
            //                    alert('Your session expired.');
            //                    window.location.reload();
            //                } else {
            //                    MNG.ReportError(error.get_message(), lastError);
            //                }
            //            }
        });

        window.location.reload();
        //MNG.LoadGroupByValues();
    },


    DoUdtNodeDelete: function (ids) {

        if (MNG.allSelected) {
            // do for all.  returns [] of ids
            //alert('delete ALL UDT Nodes:' + ids);
        }
        else {
            // do only for selected items or all on page                    
            var selectedIDs = MNG.GetSelectedObjectIDs();
            //alert('delete selected NodeIds:' + selectedIDs.N);
            ids = selectedIDs.N;
        }

        GUI.ProgressUdtDeleteDlg({
            title: "@{R=UDT.Strings;K=UDTWEBJS_AK1_80;E=js}",
            items: ids,
            urlName: '/Orion/UDT/Services/UdtPortManagement.asmx/',
            methodName: 'UdtDeleteNodes'

        });
        window.location.reload();
    },


    DoDelete: function (NodeIds) {
        //alert('NodeIds:' + NodeIds);

        GUI.ProgressDiscDlg({
            title: "@{R=UDT.Strings;K=UDTWEBJS_AK1_61;E=js}",
            items: NodeIds,
            serverMethod: NodeManagement.DeleteObjNow,
            urlName: '/Orion/Services/NodeManagement.asmx/',
            methodName: 'DeleteObjNow',
            getArg: function (id) { return [id]; },
            afterSucceeded: function (element, lastError) { },
            afterFinished: function () { MNG.LoadGroupByValues(); },
            afterFail: function (error, lastError) {
                if (error.get_message() == "Authentication failed.") {
                    // login cookie expired. reload the page so we get bounced to the login page.
                    alert('@{R=UDT.Strings;K=UDTWEBJS_AK1_60;E=js}');
                    window.location.reload();
                } else {
                    MNG.ReportError(error.get_message(), lastError);
                }
            }
        });
    },

    DoPollNow: function (ids) {

        //alert('doing POLL Now for nodes: ' + ids);

        GUI.ProgressDiscDlg({
            title: "@{R=UDT.Strings;K=UDTWEBJS_AK1_38;E=js}",
            items: ids,

            serverMethod: UdtPortManagement.UdtPollNodesNow,
            urlName: '/Orion/UDT/Services/UdtPortManagement.asmx/',
            methodName: 'UdtPollNodesNow',

            getArg: function (id) { return [id]; },
            afterFinished: function () {
                GUI.InfoDialog("@{R=UDT.Strings;K=UDTWEBJS_AK1_94;E=js}", "@{R=UDT.Strings;K=UDTWEBJS_AK1_62;E=js}");
                $('#lastErrorMessage').hide();
            },
            afterFail: function (error, lastError) {
                if (error.get_message() == "Authentication failed.") {
                    // login cookie expired. reload the page so we get bounced to the login page.
                    alert('@{R=UDT.Strings;K=UDTWEBJS_AK1_60;E=js}');
                    window.location.reload();
                } else {
                    MNG.ReportError(error.get_message(), lastError);
                }
            }
        });

        //                NodeManagement.PollNow(ids.NetObjectIDs, function() {
        //                    $.each(ids.N, function() { MNG.UpdateStatusIconForNode(this); });
        //                    $.each(ids.I, function() { MNG.UpdateStatusIconForInterface(this); });
        //                    // no status icons for volumes
        //                }, MNG.ReportError);
    },

    DoRediscoverNow: function (ids) {
        GUI.ProgressDiscDlg({
            title: "@{R=UDT.Strings;K=UDTWEBJS_AK1_63;E=js}",
            items: ids,
            serverMethod: NodeManagement.RediscoverNow,
            urlName: '/Orion/Services/NodeManagement.asmx/',
            methodName: 'RediscoverNow',
            getArg: function (id) { return [id]; },
            afterFinished: function () {
                $('#lastErrorMessage').hide();
                GUI.InfoDialog("@{R=UDT.Strings;K=UDTWEBJS_AK1_64;E=js}", "@{R=UDT.Strings;K=UDTWEBJS_AK1_65;E=js}");
            },
            afterFail: function (error, lastError) {
                if (error.get_message() == "Authentication failed.") {
                    // login cookie expired. reload the page so we get bounced to the login page.
                    alert('@{R=UDT.Strings;K=UDTWEBJS_AK1_60;E=js}');
                    window.location.reload();
                } else {
                    MNG.ReportError(error.get_message(), lastError);
                }
            }
        });

        //NodeManagement.Rediscover(ids.NetObjectIDs, function() { /*alert('Rediscovered ' + ids.NetObjectIDs);*/ }, MNG.ReportError);
    },

    //    ShutdownEnableInterface: function (ids, shutdown) {
    //        //Operations.ShowStringArray(ids);

    //        var GetObjectFunction;
    //        var Objects = [];

    //        if (ids.Name != null) {
    //            Objects = ids.ID;

    //            GetObjectNameFunction = function (interfaceId) {

    //                for (var i = 0; i < ids.ID.length; i++) {
    //                    if (ids.ID[i] == interfaceId) {
    //                        return ids.Name[i];
    //                    }
    //                }
    //                return "I" + interfaceId;
    //            }
    //        }
    //        else {

    //            Objects = ids;
    //            GetObjectNameFunction = function (interfaceId) { return $("input:checkbox[name=I:" + interfaceId + "]").data('netobject').Caption; }
    //        }

    //        if (!shutdown || window.confirm("Are you sure you want to shut down " + (Objects.length == 1 ? "the " : "") + "selected interface" + (Objects.length > 1 ? "s" : "") + "?")) {
    //            GUI.ProgressDlg({
    //                title: shutdown ? "Shutting down interfaces" : "Enabling interfaces",
    //                items: Objects,
    //                getObject: GetObjectNameFunction,
    //                serverMethod: shutdown ? NodeManagement.AdminstrativelyShutDown : NodeManagement.AdministrativelyEnable,
    //                getArg: function (interfaceId) { return ['I:' + interfaceId]; },
    //                afterSucceeded: MNG.UpdateStatusIconForInterface
    //            });
    //        }
    //    },

    //    ShutdownInterface: function (ids) {
    //        Operations.ShutdownEnableInterface(ids, true);
    //    },

    //    EnableInterface: function (ids) {
    //        Operations.ShutdownEnableInterface(ids, false);
    //    },


    RemanageObjects: function (ids) {
        //alert('REMANAGE passing N ids: ' + ids);
        if (MNG.NetObjects.SelectedPrefix() == "N") {
            remanageNetObjects(ids, []);
        }
        //        else if (MNG.NetObjects.SelectedPrefix() == "P") {
        //            remanageNetObjects([], ids);
        //        }
    },

    EnableDisableManageButton: function (cnt, managed) {
        //alert('cnt: ' + cnt + ' managed: ' + managed);
        if (managed) {
            if (cnt > 0) {
                Utils.EnableByID("unmanageLink", true);
            }
            else {
                Utils.EnableByID("unmanageLink", false);
            }
        }
        else {
            if (cnt > 0) {
                Utils.EnableByID("remanageLink", true);
            }
            else {
                Utils.EnableByID("remanageLink", false);
            }
        }
    },

    EnableDisableMonitorButton: function (cnt, monitored) {
        //alert('cnt: ' + cnt + ' monitored: ' + monitored);
        if (monitored) {
            if (cnt > 0) {
                Utils.EnableByID("unmonitorLink", true);
            }
            else {
                Utils.EnableByID("unmonitorLink", false);
            }
        }
        else {
            if (cnt > 0) {
                Utils.EnableByID("remonitorLink", true);
            }
            else {
                Utils.EnableByID("remonitorLink", false);
            }
        }
    },
    EnableDisableShutdownButton: function (cnt, shutdown) {
        //alert('cnt: ' + cnt + ' monitored: ' + monitored);
        if (shutdown) {
            if (cnt > 0) {
                Utils.EnableByID("shutdownLink", true);
            }
            else {
                Utils.EnableByID("shutdownLink", false);
            }
        }
        else {
            if (cnt > 0) {
                Utils.EnableByID("enableLink", true);
            }
            else {
                Utils.EnableByID("enableLink", false);
            }
        }
    },

    EnableDisableAddNodeToUDTButton: function (cnt, managed) {
        //alert('cnt: ' + cnt + ' managed: ' + managed);
        
        if (cnt > 0 && $("#ShowUDTMonitorNodeStat").val() == 'Unmonitored_Nodes') {
                Utils.EnableByID("monitornodebyUDTLink", true);
            }
            else {
                Utils.EnableByID("monitornodebyUDTLink", false);
            }
        
    },
    ShutdownEnableInterface: function (selectedNodes, shutdown) {

        GetObjectNameFunction = function (portId) {
            var portIds = MNG.GetSelectedObjectIDs();

            for (var i = 0; i < portIds.P.length; i++) {
                if (portIds.P[i] == portId) {
                    return portIds.PortName[i];
                }
            }
            return portId;
        };
        var sNode = [];
        for (var item in selectedNodes) {
            sNode.push(item);
        }
        UdtPortManagement.GetRWCommunityNotDefinedNodes(sNode, function (noRwCommunityNode) {

            if (noRwCommunityNode.length >= 1) {
                var listRw = noRwCommunityNode.split(",");
                var list = "";
                list = "<ol>";
                for (var a = 0; a < listRw.length; a++) {
                    list += "<li>" + listRw[a] + "</li>";
                }
                list += "</ol>";

                var layout = $("#rwCommunityDialogDescription");
                var state = (shutdown == true) ? "Shutdown" : " Enable";
                var infocaption = String.format('@{R=UDT.Strings;K=UDTWEBJS_DF1_5}', state);
                layout.html('<table><tr><td>' + infocaption + '</td></tr><tr><td><font color=red>' + list + '</font></td></tr></table>');

                if (!rwCommunityValidationDialog) {
                    rwCommunityValidationDialog = new Ext.Window({
                        applyTo: 'rwCommunityValidationDialog',
                        layout: 'fit',
                        width: 510,
                        height: 205,
                        closeAction: 'hide',
                        modal: true,
                        plain: true,
                        resizable: false,
                        items: new Ext.BoxComponent({
                            applyTo: 'rwCommunityValidationDialogBody',
                            layout: 'fit',
                            border: false
                        }),
                        buttons: [
                                    {
                                        text: 'OK',
                                        style: 'margin-left: 5px;',
                                        handler: function () {
                                            rwCommunityValidationDialog.hide();
                                        }
                                    }
                                ]
                    });
                }

                rwCommunityValidationDialog.alignTo(document.body, "c-c");
                rwCommunityValidationDialog.show();
            }
            else {
                var portIds = MNG.GetSelectedObjectIDs();
                if (!shutdown || window.confirm(portIds.P.length == 1 ? "@{R=UDT.Strings;K=UDTWEBJS_DF1_1}" : "@{R=UDT.Strings;K=UDTWEBJS_DF1_2}")) {

                    var width = 800;
                    var statusBox = $('<div></div>').addClass('StatusDialog').width(width);
                    var statusContent = $('<div></div>').appendTo(statusBox);
                    var spinner = $('<div></div>').addClass('Spinner').appendTo(statusBox);
                    var statusItems = $('<ul></ul>').addClass('InterfacesText').appendTo(statusContent);


                    var i = 0;
                    $(SW.Core.Widgets.Button('Close', { cls: 'CloseButton' })).appendTo(statusBox).click(function () { statusBox.dialog('close').remove(); });

                    $('body').append(statusBox);
                    var numFinished = 0, numSucceeded = 0;
                    var onFinished = function (succeeded) {

                        // progressBarInner.width((++numFinished / i) * 100.0 + '%');
                        if (numFinished == i)
                            spinner.hide();
                        if (succeeded && ++numSucceeded >= i)
                            $('.StatusDialog').fadeOut('slow', function () { statusBox.dialog('destroy').remove(); });
                    };

                    for (var item in selectedNodes) {

                        for (var portLength = 0; portLength < selectedNodes[item].length; portLength++) {
                            var caption = GetObjectNameFunction(selectedNodes[item][portLength]);
                            var statusBlock = $('<li></li>').appendTo(statusItems).text(caption);
                            i++;
                        }


                        UdtPortManagement.AdminstrativelyShutDown(item, selectedNodes[item], shutdown, function () {
                            statusBlock.append(' - ' + '<font color="green">ok</font>');
                            spinner.css({ 'display': 'none' });
                            onFinished(true);
                            window.location.reload();

                        }, function (error) {
                            var before = statusItems.height();
                            statusBlock.append(' - ' + '<font color="red">' + error.get_message() + '</font>');
                            spinner.css({ 'display': 'none' });
                            if (statusItems.height() > before) {
                                var val = $('.StatusDialog').height() + statusItems.height() - before;
                                $('.StatusDialog').data('height.dialog', val);
                            }
                            onFinished(false);
                        });

                    }

                    statusBox.dialog({
                        width: width, height: statusContent.height() + 140, modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: shutdown ? "@{R=UDT.Strings;K=UDTWEBJS_DF1_3}" : "@{R=UDT.Strings;K=UDTWEBJS_DF1_4}"
                    });


                }
            }
        },
           function (error) { }
            );

    },

    ShutdownInterface: function (ids) {
        Operations.ShutdownEnableInterface(ids, true);
    },
    EnableInterface: function (ids) {
        Operations.ShutdownEnableInterface(ids, false);
    },

    PreparePostForm: function (netObjectType, ids, returnUrl) {
        var stringForm = "<form id='selectedNetObjects' action='{0}' method='POST'> \
                            <input type='hidden' name='{1}s' value='{2}'/> \
                            <input type='hidden' name='ReturnTo' value='{3}'/> \
                         </form>";
        var params = ids.join(",");
        var actionUrl = MNG.interfaceEditPropertiesUrl.format([netObjectType]);

        return $(stringForm.format([actionUrl, netObjectType, params, returnUrl]));
    },

    MonitoryNodesByUDT: function (ids) {
       GUI.ProgressMonitorNodeByUDTDlg({
            title: "@{R=UDT.Strings;K=UDTWEBJS_PG1_1;E=js}",
        items: ids,
        urlName: '/Orion/UDT/Services/UdtPortManagement.asmx/',
        methodName: 'UdtImportNodes'

    });
    
},
}

MNG.Nodes.typename = "Node";
MNG.Ports.typename = "Port";

MNG.Nodes.createColumnHeaders = function () {
    //
    //alert("MNG.Nodes.createColumnHeaders function");
    //
//    $('#NodeTree2 thead tr').html('<th><img src="/Orion/images/Button.Expand.gif" alt="" style="visibility:hidden;"/><input type="checkbox" id="selectAll" /></th>\
//							<th sortType="Node" sortProp="Caption">Name <img src="" alt="" id="sortArrow" /></th>' +
//							$.map(MNG.Nodes.selectedColumns, function (c) { return '<th sortType="{sortType}" sortProp="{sortProp}">{header} </th>'.format(c); }).join(''));

    $('#NodeTree2 thead tr').html('<th><img src="/Orion/UDT/Ports/images/icons/Button.Expand.gif" alt="" style="visibility:hidden;"/><input type="checkbox" id="selectAll" /></th>\
							<th sortType="Node" sortProp="Caption">@{R=UDT.Strings;K=UDTWEBJS_VB1_22;E=js} <img src="" alt="" id="sortArrow" /></th>' +
							$.map(MNG.Nodes.selectedColumns, function (c) { return '<th sortType="{sortType}" sortProp="{sortProp}">{headerText} </th>'.format(c); }).join('')
							+ '\<th>&nbsp;</th>');

    $('.nodeCommand').show();
    $('.portCommand').hide();
};

MNG.Ports.createColumnHeaders = function () {
    //
    //alert("MNG.Ports.createColumnHeaders function");
    //

    //    $('#NodeTree2 thead tr').html('<th><input type="checkbox" id="selectAll" /></th>\
    //							<th sortType="Port" sortProp="Name">PortName <img src="" alt="" id="sortArrow" /></th>' +
    //							$.map(MNG.Ports.selectedColumns, function (c) {
    //							    return '<th sortType="{sortType}" sortProp="{sortProp}">{header} </th>'.format(c);}).join(''));

    //    $('.nodeCommand').hide();

    $('#NodeTree2 thead tr').html('<th><input type="checkbox" id="selectAll" /></th>\
							<th sortType="Port" sortProp="Name">@{R=UDT.Strings;K=UDTWEBJS_VB1_22;E=js} <img src="" alt="" id="sortArrow" /></th>' +
							$.map(MNG.Ports.selectedColumns, function (c) {
							    return '<th sortType="{sortType}" sortProp="{sortProp}">{headerText} </th>'.format(c);}).join('')
							+ '\<th>&nbsp;</th>');

    $('.nodeCommand').hide();
    $('.portCommand').show();

};




// common DoQuery function used to call Information.asmx web service Query() webmethod
MNG.DoQuery = function (query, succeeded) {

    //
    //alert("DoQuery: " + query);
    //

    MNG.clearError();
    // call webservice: Information.asmx; web method: Query()
    Information.Query(query, function (result) {
        var table = [];
        for (var y = 0; y < result.Rows.length; ++y) {
            var row = result.Rows[y];
            var tableRow = {};
            for (var x = 0; x < result.Columns.length; ++x) {
                tableRow[result.Columns[x]] = row[x];
            }
            table.push(tableRow);
        }
        succeeded({ Rows: table });
        $('#lastErrorMessage').hide();
    }, function (error) {
        if (error.get_message() == "Authentication failed.") {
            // login cookie expired. reload the page so we get bounced to the login page.
            alert('@{R=UDT.Strings;K=UDTWEBJS_AK1_60;E=js}');
            window.location.reload();
        } else {
            MNG.ReportError(error);
            //			$("#originalQuery").text(query);
            //			$("#test").text(error.get_message());
            //			$("#stackTrace").text(error.get_stackTrace());
        }
    });
};

// common SaveUserSetting function used to call NodeManagement.asmx web service SaveUserSetting() webmethod
MNG.SaveUserSetting = function (name, value) {
    $("#" + name).val(value);
    NodeManagement.SaveUserSetting(name, value, function () { $('#lastErrorMessage').hide(); },
    function (error) {
        if (error.get_message() == "Authentication failed.") {
            // login cookie expired. reload the page so we get bounced to the login page.
            alert('@{R=UDT.Strings;K=UDTWEBJS_AK1_60;E=js}');
            window.location.reload();
        } else {
            MNG.ReportError(error);
        }
    });
    /*function(error) {
    $("#originalQuery").text(name + '=' + value);
    $("#test").text(error.get_message());
    $("#stackTrace").text(error.get_stackTrace());
    });*/
};

MNG.clearError = function () {
    $("#originalQuery,#test,#stackTrace").text('');
};

MNG.LoadGroupByValues = function () {

    Pager.Hide();
    // cause hiding select all panel if displayed
    MNG.DisplaySelectPan(false);
    var prop = $("#groupByProperty option:selected").val();

    var nonParsedTypes = ["SYSTEM.SINGLE", "SYSTEM.INT32", "SYSTEM.DATETIME"];
    var propType = $("#groupByProperty option:selected").attr("propertyType");

    if (propType == null || propType == undefined)
        propType = 'unknown';

    MNG.Prefs.save('UDTGroupBy', prop);
    if (prop) {
        MNG.ClearTable();
        var nodeGroupItems = $(".NodeGroupItems").text("@{R=UDT.Strings;K=UDTWEBJS_AK1_56;E=js}");
        // Due to current IS version incorrectly parsing nested queries we have to use such select
        var SQL;

        // SJW: queries had to be INNER JOIN, rather than RIGHT JOIN to prevent a SWIS query execution error occurring 'Input string was not in a correct format'.
        if (nonParsedTypes.contains(propType.toUpperCase())) {
            //alert("1");


            if (MNG.NetObjects.typename == "Node" && $("#ShowUDTMonitorNodeStat").val() == 'Unmonitored_Nodes') {
                SQL = "SELECT N.{0} AS Value, COUNT(ISNULL(N.{0},'')) AS Cnt FROM Orion.Nodes (nolock=true) N   LEFT JOIN  Orion.UDT.NodeCapability (nolock=true) S ON N.NODEID=S.NODEID WHERE S.NODEID IS NULL AND N.ObjectSubType = 'SNMP' GROUP BY N.{0} ORDER BY N.{0} ASC".format([prop]);
            }
            else if (MNG.NetObjects.typename == "Node") {
                //alert("node 1");
                SQL = "SELECT N.{0} AS Value, COUNT(ISNULL(N.{0},'')) AS Cnt FROM Orion.Nodes (nolock=true) N WHERE N.NodeID IN (SELECT DISTINCT S.NodeID FROM Orion.UDT.NodeCapability (nolock=true) S) GROUP BY N.{0} ORDER BY N.{0} ASC".format([prop]);
            }
            else {
                //alert("not node 1");
                SQL = "SELECT N.{0} AS Value, COUNT(P.PortID) AS Cnt FROM Orion.Nodes (nolock=true) N INNER JOIN Orion.UDT.Port (nolock=true) P ON N.NodeID=P.NodeID WHERE N.NodeID IN (SELECT DISTINCT S.NodeID FROM Orion.UDT.NodeCapability (nolock=true) S)  GROUP BY N.{0} ORDER BY N.{0} ASC".format([prop]);
            }
        }

        else {
            // alert("2");

            if (MNG.NetObjects.typename == "Node" && $("#ShowUDTMonitorNodeStat").val() == 'Unmonitored_Nodes') {
                SQL = "SELECT ISNULL(N.{0},'') AS Value, COUNT(ISNULL(N.{0},'')) AS Cnt FROM Orion.Nodes (nolock=true) N  LEFT JOIN Orion.UDT.NodeCapability (nolock=true) S ON N.NODEID=S.NODEID WHERE S.NodeID IS NULL AND N.ObjectSubType = 'SNMP' GROUP BY ISNULL(N.{0},'') ORDER BY ISNULL(N.{0},'') ASC".format([prop]);

            }
            else if (MNG.NetObjects.typename == "Node") {
                //alert("node 2");
                SQL = "SELECT ISNULL(N.{0},'') AS Value, COUNT(ISNULL(N.{0},'')) AS Cnt FROM Orion.Nodes (nolock=true) N WHERE N.NodeID IN (SELECT DISTINCT S.NodeID FROM Orion.UDT.NodeCapability (nolock=true) S) GROUP BY ISNULL(N.{0},'') ORDER BY ISNULL(N.{0},'') ASC".format([prop]);
            }
            else {
                //alert("not node 2");
                SQL = "SELECT ISNULL(N.{0},'') AS Value, COUNT(P.PortID) AS Cnt FROM Orion.Nodes (nolock=true) N INNER JOIN Orion.UDT.Port (nolock=true) P ON N.NodeID=P.NodeID WHERE N.NodeID IN (SELECT DISTINCT S.NodeID FROM Orion.UDT.NodeCapability (nolock=true) S) GROUP BY ISNULL(N.{0},'') ORDER BY ISNULL(N.{0},'') ASC".format([prop]);
            }
        }

        // SJW: adding the new NodeCapability grouping for UDT
        if (prop == 'Capability') {
            SQL = "SELECT S.Capability AS Value, COUNT(DISTINCT S.NodeID) AS Cnt FROM Orion.UDT.NodeCapability (nolock=true) S INNER JOIN Orion.Nodes (nolock=true) N ON S.NodeID = N.NodeID GROUP BY S.Capability";
        }
        
        // SJW: adding the new PortType grouping for UDT
        if (prop == 'PortType') {
            SQL = "SELECT P.PortType AS Value, COUNT(P.PortID) AS Cnt FROM Orion.UDT.Port (nolock=true) P WHERE P.NodeID IN (SELECT DISTINCT S.NodeID FROM Orion.UDT.NodeCapability (nolock=true) S)  GROUP BY P.PortType ORDER BY P.PortType ASC";
        }


        // go get the group data for the selected group by item 
        MNG.DoQuery(SQL, function (result, eventArgs) {
            nodeGroupItems.empty();
            
            var nodeGroupItemsElement = $(".NodeGroupItems");

            $(result.Rows).each(function () {
                var value = this.Value;

                //format disp to default culture patterns 
                var disp = ($.trim(String((Date.isInstanceOfType(value)) ?
                    value.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + " " + value.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern)
                    : (Number.isInstanceOfType(value)) ? String(value).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator) : this.Value))
                    || "@{R=UDT.Strings;K=UDTWEBJS_AK1_20;E=js}") + " (" + this.Cnt + ")";

                if (prop == "Status") {
                    disp = MNG.GetStatusText(String(this.Value)) + " (" + this.Cnt + ")";
                }

                var isUnknown = false;
                if (value == null || ((prop == "MachineType") && ($.trim(String(this.Value)) == "Unknown"))) {
                    disp = "@{R=UDT.Strings;K=UDTWEBJS_AK1_20;E=js} (" + this.Cnt + ")";
                    value = "null";
                    isUnknown = true;
                }
                else if (Date.isInstanceOfType(value)) {
                    //value = new Date(value.getTime() + value.getTimezoneOffset() * 60 * 1000); // to UTC
                    value = value.localeFormat(Sys.CultureInfo.InvariantCulture.dateTimeFormat.SortableDateTimePattern);
                }
                if (value != null && value.replace != undefined) {
                    value = value.replace(/\'/g, '&#39;');
                }

                var groupItem = $("<li class='NodeGroupItem'><a href='#' value='" + value + "'>" + disp + "</a></li>");
                $("a", groupItem).click(MNG.SelectGroup);
                groupItem = groupItem.appendTo(nodeGroupItemsElement);

            });

            if (MNG.RefreshObjects == MNG.RefreshObjectsByGroup) {

                var lastItem = MNG.Prefs.load("UDTGroupByValue")
                //
                if (lastItem) {
                    $(".NodeGroupItem a[value='" + MNG.Prefs.load("UDTGroupByValue") + "']").click();
                }
                else {
                    // try highlight the [Unknown] item if it exists in the list
                    $(".NodeGroupItem a[value='']").click();
                }

            } else {
                MNG.RefreshObjects();
            }
          
            // add vendor icons
            if (prop === "Vendor" ) {
                MNG.AddVendorIcons();
            }

            // add Node Capability name to Capability
            if (prop === "Capability") {
                MNG.AddCapabilityNames();
            }

            // add ifType name to PortTypes
            if (prop === "PortType") {
                MNG.AddIfTypeNames();
            }
        });
    } else {
        $(".NodeGroupItems").empty();
        Pager.Reset();
        MNG.RefreshObjects();
    }
};


MNG.GetStatusText = function (statusValue) {
    switch (statusValue) {
        case "1": return "@{R=UDT.Strings;K=UDTWEBJS_AK1_95;E=js}";
        case "2": return "@{R=UDT.Strings;K=UDTWEBJS_AK1_96;E=js}";
        case "3": return "@{R=UDT.Strings;K=UDTWEBJS_AK1_97;E=js}";
        case "9": return "@{R=UDT.Strings;K=UDTWEBJS_AK1_98;E=js}";
        case "11": return "@{R=UDT.Strings;K=UDTWEBJS_AK1_99;E=js}";
        case "12": return "@{R=UDT.Strings;K=UDTWEBJS_AK1_100;E=js}";
        default: return "@{R=UDT.Strings;K=UDTWEBJS_AK1_101;E=js}";
    }
}

MNG.SelectGroup = function () {

    //
    //alert('selected an item from grouped list');
    //
    $(this).parent().addClass("SelectedNodeGroupItem").siblings().removeClass("SelectedNodeGroupItem");
    if (Pager.forbidReset == false) {
        Pager.Reset();
    }
    else {
        Pager.forbidReset = false;
    }
    MNG.RefreshObjects = MNG.RefreshObjectsByGroup;
    MNG.RefreshObjects();
    return false;
};

MNG.NumericTypes = ["System.Single", "System.Double", "System.Int32"];

MNG.RefreshObjects = MNG.RefreshObjectsByGroup = function () {
    MNG.CancelLoading();
    MNG.Sort.setArrow();

    if (!Utils.SelectedProperty() || Utils.SelectedItem().length > 0) {

        var value = Utils.SelectedValue();

        // check if value is not null (as an [Unknown] item has an empty string value, which we want to save)
        if (value != null) {
            MNG.Prefs.save('UDTGroupByValue', value);
        }

        MNG.ClearTable();
        $("#selectAll").get(0).checked = false;
        MNG.NetObjects.Load(MNG.GetWhere, 0);
    }
};

// Gets where statement for table alias like N, I, V based on filter
MNG.GetWhere = function (tableAlias) {
    //
    //alert('GetWhere: ' + tableAlias);
    //
    if (Pager.isInSearchMode) {
        return SWQLQueries.GetSearchWhereFromPage();
    }

    var prop = Utils.SelectedProperty();
    var type = Utils.SelectedType();
    var value = Utils.SelectedValue();

    if (!prop || Utils.SelectedItem().length > 0) {
        if (prop) {
            // SJW: added to set PortType table alias to always be P (Port)
            if (prop == 'PortType')
                tableAlias = 'P';
            if (prop == 'Capability')
                tableAlias = 'S';

            if (prop == 'MachineType' && (value == "null" || value.trim() == ""))
                return "({0}.{1} IS NULL OR {0}.{1}='' OR {0}.{1}='Unknown')".format([tableAlias, prop]);
            if (value == "null" || value.trim() == "") {
                if ($.inArray(type, MNG.NumericTypes) > -1) {
                    return "({0}.{1} IS NULL)".format([tableAlias, prop]);
                } else {
                    return "({0}.{1} IS NULL OR {0}.{1}='')".format([tableAlias, prop]);
                }
            } else {
                return "{0}.{1}='{2}'".format([tableAlias, prop, MNG.quoteValueForSql(value)]);
            }
        }
        else {
            return "1=1";
        }
    }
    else {
        return "1=1";
    }
}

// add vendor icons
MNG.AddVendorIcons = function () {

    var iconQuery = "";
    
    if ($("#showObjectType").val() == "Nodes" && $("#ShowUDTMonitorNodeStat").val() == "Unmonitored_Nodes") {
        iconQuery = "SELECT DISTINCT N.Vendor, N.VendorIcon FROM Orion.Nodes (nolock=true) N LEFT JOIN Orion.UDT.NodeCapability (nolock=true) S ON S.NodeID = N.NodeID WHERE S.NODEID IS NULL AND  N.ObjectSubType = 'SNMP'";

    }
    else {
        iconQuery = "SELECT DISTINCT N.Vendor, N.VendorIcon FROM Orion.Nodes (nolock=true) N INNER JOIN Orion.UDT.NodeCapability (nolock=true) S ON S.NodeID = N.NodeID ";
    }

    MNG.DoQuery(iconQuery, function (result) {
        $(result.Rows).each(function () {
           
            if ($(".NodeGroupItems a[value='" + this.Vendor + "']") != null &&
            $(".NodeGroupItems a[value='" + this.Vendor + "']")[0].firstChild != null &&
            $(".NodeGroupItems a[value='" + this.Vendor + "']")[0].firstChild.nodeName != "IMG")
                $('<img src="/NetPerfMon/images/Vendors/' + this.VendorIcon + '" />').error(function () {
                    this.src = "/NetPerfMon/images/Vendors/Unknown.gif";
                    return true;
                }).prependTo(".NodeGroupItems a[value='" + this.Vendor + "']").after(" ");
        });
    });
};


// add capability names
MNG.AddCapabilityNames = function () {

    //alert("capability Array" + nodeCapabilities.length);
    for (var i = 0; i < nodeCapabilities.length; i++) {
        if ($(".NodeGroupItems a[value='" + nodeCapabilities[i].type + "']").length != 0) {
            // replace existing nodeCapability with type and name
            var newText = $(".NodeGroupItems a[value='" + nodeCapabilities[i].type + "']").text().replace(nodeCapabilities[i].type, nodeCapabilities[i].name);
            // update the link text
            $(".NodeGroupItems a[value='" + nodeCapabilities[i].type + "']").text(newText);
        };
    };
};

// add porttype names
MNG.AddIfTypeNames = function () {

    for (var i = 0; i < portTypes.length; i++) {
        if ($(".NodeGroupItems a[value='" + portTypes[i].type + "']").length != 0) {
            // replace existing ifType with type and name
            var newText = $(".NodeGroupItems a[value='" + portTypes[i].type + "']").text().replace(portTypes[i].type, portTypes[i].type + '-' + portTypes[i].name);
            // update the link text
            $(".NodeGroupItems a[value='" + portTypes[i].type + "']").text(newText);
        };
    };
};


MNG.MakeNodeLink = function (node) {
    return '<a href="/Orion/View.aspx?NetObject=N:{NodeID}" EngineID="{EngineID}" IP="{IPAddress}" NodeHostname="{Hostname}" Community="{GUID}"><img NetObject=N:{NodeID} class="StatusIcon" src="/Orion/images/StatusIcons/Small-{GroupStatus}" alt="{StatusDescription}"/><span>{Caption}</span></a>'.format(node);
};

MNG.MakePortLink = function (obj, nodeLink) {
    //
    var overAllStatus = obj.AdministrativeStatus == 2 ? "shutdown" : (obj.OperationalStatus == 1 ? "active" : "inactive");
    obj.Status = overAllStatus;
    return '<a href="/Orion/UDT/PortDetails.aspx?NetObject=UP:{PortID}"><img NetObject=UP:{PortID} class="StatusIcon" src="{IconPath}icon_port_{Status}_dot.gif" alt=""/><span>{Name}</span></a>'.format(obj);
};

MNG.MakeNodeExpander = function (node) {
    var hidden = '';
    if ((node.NumPorts || 0) == 0)
        hidden = 'style="visibility:hidden;"';
    return "<img class='NodeExpand' src='{0}' nodeid='{1}' {2}/>".format([MNG.expandImg, node.NodeID, hidden]);
};

MNG.MakeNodeCheckbox = function (node, check) {
    return '<input type="checkbox" name="N:' + node.NodeID + '"' + (check ? 'checked="checked"' : '') + '/>';
};

MNG.MakeCheckbox = function (obj) {
    return '<input type="checkbox" name="{Type}:{PortID}" />'.format(obj);
};

MNG.MakeRow = function (cells) {
    return $("<tr>" + $.map(cells, function (cell) { return "<td>" + cell + "</td>"; }).join("") + "<td> &nbsp;</td></tr>");
}

MNG.AddRow = function (tree, cells) {
    return MNG.MakeRow(cells).appendTo(tree);
}

MNG.ExpandNode = function () {
    var that = this;
    var nodeRow = $(this).parents("tr").eq(0);
    var nodeLink = $("a[ip]", nodeRow);
    this.src = MNG.collapseImg;
    var nodeId = this.attributes["nodeid"].value;
    var query = "";

    //(SJW: UDT SR1.0.1) - Limit ports shown by 'Flag=0'.  Also now order results DESC by PortIndex. This is to reverse the reversing effect of the results when the <TR>'s are added with .insertAfter(nodeRow);
    query = "SELECT 'UP' as Type, P.PortID, P.Name, P.OperationalStatus, P.IsMonitored, P.PortIndex FROM Orion.UDT.Port (nolock=true) P WHERE P.NodeID=@nodeid  ORDER BY P.PortIndex DESC";

    MNG.DoQuery(query.replace(/@nodeid/g, nodeId), function (results) {
        var ifxRows = [];
        var className = 'childOfN' + nodeId;
        var numCols = $("#NodeTree2 th").length - 1;

        if ($("." + className).length == 0) {
            var i = 0;
            for (i = 0; i < results.Rows.length; i++) {
                var row = results.Rows[i];
                row.IconPath = '/Orion/UDT/Images/Status/';
                row.Type = 'P';

                //                //Add a title row for ports
                //                if (i==0) {
                //                    var titleRow = $("<tr><td>&nbsp;</td><td class='ifxname'>Port Name</td><td colspan='" + (numCols - 2) + "'>Monitored</td></tr>");
                //                    ifxRows.push(titleRow);
                //                }

                //var node = $("<tr class='" + className + "'><td>&nbsp;</td><td class='ifxname' colspan='" + numCols + "'>" + MNG.MakeCheckbox(row) + MNG.MakePortLink(row, nodeLink) + "</td></tr>");
                var node = $("<tr class='" + className + "'><td>&nbsp;</td><td class='ifxname' >" + MNG.MakePortLink(row, nodeLink) + "</td><td colspan='" + (numCols - 2) + "'>" + MNG.FormatMonitoredCell(row, true) + "</td></tr>");
                node.find(":checkbox").data('netobject', row);
                ifxRows.push(node[0]);

                if (i >= 1000) {
                    var warnStrip = $(String.format("<tr><td>&nbsp;</td><td class='ifxname' colspan='{0}'><a id='warnStrip' href='#' title='link' >{1}</a></td></tr>", numCols, String.format('@{R=UDT.Strings;K=UDTWEBJS_AK1_102;E=js}', results.Rows.length)));
                    $("#warnStrip", warnStrip).attr("title", "@{R=UDT.Strings;K=UDTWEBJS_AK1_55;E=js}");
                    ifxRows.push(warnStrip[0]);
                    break;
                }
            }

            //SJW - Note: this has the effect of pushing the first record in the results to the bottom of the list
            $(ifxRows).insertAfter(nodeRow);
            MNG.stripeTree();
        }
        else {
            $("." + className).show();
            MNG.stripeTree();
        }
        $("." + className + " :checkbox").click(MNG.EnableDisableMenu);
        $(that).unbind().click(function () {
            $("." + className).hide();
            MNG.stripeTree();
            $(this).unbind().click(MNG.ExpandNode);
            this.src = MNG.expandImg;
        });
    });
};

MNG.ClearTable = function () {
    $("#NodeTree2>tbody").empty();
    MNG.EnableDisableMenu();
};

MNG.FormatCell = function (val) {
    //format value to default culture patterns 
    var str = val == null ? '' : (Date.isInstanceOfType(val)) ? val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) +
            ' ' + val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern) :
            (Number.isInstanceOfType(val)) ? String(val).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator) : String(val);
    return $.trim(str) || '&nbsp;';
};

MNG.FormatNodeCell = function (node) {
    
    return '<a href="/Orion/View.aspx?NetObject=N:{NodeID}" IP="{IPAddress}" NodeHostname="{Hostname}" Community="{GUID}">{NodeCaption}</a>'.format(node);
};

MNG.FormatPortTypeCell = function (obj) {
    // renders the PortType column with Icon
    var portType = '{PortType}'.format(obj);
    // use prototype method to get port type name
    var portTypeName = portTypes.GetNameFromPortType(portType);
    var portTypeWithIcon = "<img src=\"/NetPerfMon/images/Interfaces/" + portType + ".gif\" title=\"" + portTypeName + "\" alt=\"" + portTypeName + "\" style=\"vertical-align:bottom;\" />&nbsp;";
    return portTypeWithIcon + portTypeName + "(" +  portType + ")";
};

MNG.FormatMACAddressCell = function (obj) {
    // renders the MAC address in a standard display format
    var value = '{MACAddress}'.format(obj);
    var regex = /(.{2})(.{2})(.{2})(.{2})(.{2})(.{2})/;
    var formatted = value.replace(regex, "$1:$2:$3:$4:$5:$6");
    return formatted; 
};

MNG.FormatMonitoredCell = function (obj, isNodeExpand) {
    // renders the IsMonitored column as Yes/No with Icon
    var value = '{IsMonitored}'.format(obj);
    var monitoredText;
    if (isNodeExpand) {
        if (value == 'true') {
            return monitoredText = String.format('@{R=UDT.Strings;K=UDTWEBJS_AK1_53;E=js}', '<font color="green">', '</font>');
        }
        else {
            return monitoredText = String.format('@{R=UDT.Strings;K=UDTWEBJS_AK1_54;E=js}', '<font color="red">', '</font>');
        }
    }

    var monitoredWithIcon;
    if (value == 'true') {
        monitoredWithIcon = "<img src=\"/Orion/UDT/Ports/images/icons/OK_good.gif\" title=\"@{R=UDT.Strings;K=UDTWEBJS_AK1_48;E=js}\" alt=\"@{R=UDT.Strings;K=UDTWEBJS_AK1_48;E=js}\" style=\"vertical-align:bottom;\" />&nbsp;@{R=UDT.Strings;K=UDTWEBJS_AK1_50;E=js}";
    }
    else {
        monitoredWithIcon = "<img src=\"/Orion/UDT/Ports/images/icons/disable.png\" title=\"@{R=UDT.Strings;K=UDTWEBJS_AK1_52;E=js}\" alt=\"@{R=UDT.Strings;K=UDTWEBJS_AK1_52;E=js}\" style=\"vertical-align:bottom;\" />&nbsp;@{R=UDT.Strings;K=UDTWEBJS_AK1_51;E=js}";
    }
    return monitoredWithIcon;
};

MNG.Nodes.Load = function (getWhereFragment, onComplete) {

    //
    //alert("do NODES query");
    //

    var canceled = false;
    MNG.CancelLoading = function () { canceled = true; }
    var loadSome = function (nodesLoaded) {
        //$("#loadingStatus").show();
        var fields = ["Caption", "NodeID", "Status", "GroupStatus", "StatusDescription", "IPAddress", "DNS", "SysName",
			"WebCommunityString.GUID", "Unmanaged", "ObjectSubType", "EngineID"]
			.concat($.map(MNG.Nodes.selectedColumns, function (c) { return c.selectAlias ? (c.sortProp + ' AS ' + c.selectAlias) : c.sortProp; })).unique();
        var addTableAliasPrefix = function (clauses, prefix) { return $.map(clauses, function (s) { return prefix + "." + s; }); };

        //        var q = "SELECT " + addTableAliasPrefix(fields, 'N').join(", ") +
        //			", I2.NumInterfaces, V2.NumVolumes" +
        //			" FROM Orion.Nodes N " +
        //			" LEFT JOIN Orion.NPM.EW.Nodes EW ON (N.NodeID = EW.NodeID) " +
        //			" LEFT JOIN (SELECT I.NodeID, COUNT(I.InterfaceID) AS NumInterfaces FROM Orion.NPM.Interfaces I GROUP BY I.NodeID) I2 ON N.NodeID=I2.NodeID" +
        //			" LEFT JOIN (SELECT V.NodeID, COUNT(V.VolumeID) AS NumVolumes FROM Orion.Volumes V GROUP BY V.NodeID) V2 ON N.NodeID=V2.NodeID" +
        //			" WHERE " + getWhereFragment("N") + MNG.Sort.getOrderBy("N") +
        //			" WITH ROWS " + (nodesLoaded + 1) + " TO " + (nodesLoaded + MNG.nodePageSize);

        var q = "";

       
        var prop = $("#groupByProperty option:selected").val();

        if ($("#ShowUDTMonitorNodeStat").val() == "Monitored_Nodes") {
            // capability group query needs to be handled differently
            if (prop != 'Capability') {
                q = "SELECT " + addTableAliasPrefix(fields, 'N').join(", ") +
                    ", P2.NumPorts" +
                    " FROM Orion.Nodes (nolock=true) N" +
                    " LEFT JOIN (SELECT P.NodeID, COUNT(P.PortID) AS NumPorts FROM Orion.UDT.Port (nolock=true) P  GROUP BY P.NodeID) P2 ON N.NodeID=P2.NodeID" +
                    " WHERE N.NodeID IN (SELECT DISTINCT NodeID FROM Orion.UDT.NodeCapability (nolock=true)) AND " + getWhereFragment("N") + MNG.Sort.getOrderBy("N") +
                    " WITH ROWS " + ((Pager.actualPage - 1) * Pager.pageSize + 1) + " TO " + (Pager.actualPage * Pager.pageSize);
            } else {
                q = "SELECT " + addTableAliasPrefix(fields, 'N').join(", ") +
                    ", P2.NumPorts" +
                    " FROM Orion.UDT.NodeCapability (nolock=true) S INNER JOIN Orion.Nodes (nolock=true) N ON S.NodeID = N.NodeID " +
                    " LEFT JOIN (SELECT P.NodeID, COUNT(P.PortID) AS NumPorts FROM Orion.UDT.Port (nolock=true) P  GROUP BY P.NodeID) P2 ON N.NodeID=P2.NodeID" +
                    " WHERE " + getWhereFragment("N") + MNG.Sort.getOrderBy("N") +
                    " WITH ROWS " + ((Pager.actualPage - 1) * Pager.pageSize + 1) + " TO " + (Pager.actualPage * Pager.pageSize);
            }
        } else {
           q = "SELECT " + addTableAliasPrefix(fields, 'N').join(", ") +
                   ", 0 as NumPorts" +
                   " FROM Orion.Nodes (nolock=true) N" +
                   " LEFT JOIN Orion.UDT.NodeCapability (nolock=true) UNC  ON N.NodeID=UNC.NodeID" +
                   " WHERE  UNC.NodeID IS NULL AND N.ObjectSubType = 'SNMP' AND " + getWhereFragment("N") + MNG.Sort.getOrderBy("N") +
                   " WITH ROWS " + ((Pager.actualPage - 1) * Pager.pageSize + 1) + " TO " + (Pager.actualPage * Pager.pageSize);
           
        }
        var q_totalcount = "";

        if ($("#ShowUDTMonitorNodeStat").val() == "Monitored_Nodes") {
             q_totalcount = "SELECT COUNT(DISTINCT S.NodeID) AS TotalCount" +
                " FROM Orion.UDT.NodeCapability (nolock=true) S INNER JOIN Orion.Nodes (nolock=true) N ON S.NodeID = N.NodeID " +
                " WHERE " + getWhereFragment("N");
        } else {
            q_totalcount = "SELECT COUNT(DISTINCT N.NodeID) AS TotalCount" +
                " FROM Orion.Nodes (nolock=true) N LEFT JOIN Orion.UDT.NodeCapability (nolock=true) UNC  ON N.NodeID=UNC.NodeID " +
                " WHERE UNC.NodeID IS NULL AND N.ObjectSubType = 'SNMP' AND " + getWhereFragment("N");
        }
        
        MNG.DoQuery(q_totalcount, function (results) {
            if (results.Rows.length > 0) {
                Pager.totalCount = results.Rows[0]["TotalCount"];
                Pager.Init();
            }
        });

        var startQuery = new Date().getTime();
        MNG.DoQuery(q, function (results) {
            var endQuery = new Date().getTime();
            if (canceled) return;
            var check = $("#selectAll").get(0).checked;
            $(results.Rows).each(function () {
                var row = this;
                row.Hostname = ($.trim(row.DNS) || $.trim(row.SysName) || row.IPAddress);
                row.GUID = "GUID{" + row.GUID + "}";
                MNG.AddRow($("#NodeTree2>tbody"), [MNG.MakeNodeExpander(row) + MNG.MakeNodeCheckbox(row, check), MNG.MakeNodeLink(row)]
					.concat($.map(MNG.Nodes.selectedColumns, function (c) { return MNG.FormatCell(row[MNG.removeBrackets(c.selectAlias || c.sortProp)]); })))
					.find(":checkbox").data('netobject', row);
            });
            $("#NodeTree2 .NodeExpand").click(MNG.ExpandNode);
            $("#NodeTree2 tr td :checkbox").click(MNG.EnableDisableMenu);

            MNG.stripeTree();
            var endRender = new Date().getTime();
            var queryTime = endQuery - startQuery;
            var renderTime = endRender - endQuery;
            if (typeof (onComplete) == 'function')
                onComplete();
            //            if (results.Rows.length >= MNG.nodePageSize) {
            //                //setTimeout(function() {
            //                //if (canceled) return;
            //                loadSome(nodesLoaded + results.Rows.length);
            //                //}, 3000);
            //                //$("#loadingStatus span").text("Loaded " + (nodesLoaded + results.Rows.length) + " nodes, continuing...");
            //            } else {
            //                //$("#loadingStatus").hide();
            //                if (typeof (onComplete) == 'function')
            //                    onComplete();
            //            }
        });
    };
    loadSome(0);
};

MNG.Ports.Load = function (getWhereFragment, onComplete) {
    //
    //alert("do PORTS query");
    //
    var canceled = false;
    MNG.CancelLoading = function () { canceled = true; }
    var loadSome = function (objectsLoaded) {
        //$("#loadingStatus").show();        
        //var ifxFields = ["PortID AS ID", "StatusLED", "Caption", "Index", "Name", "FullName", "Unmanaged", "AdminStatus"]
        var ifxFields = ["Name", "PortID", "IsMonitored", "MACAddress", "PortIndex", "PortDescription", "OperationalStatus", "AdministrativeStatus"]
			.concat($.map(MNG.Ports.selectedColumns, function (c) { return c.sortType == 'Port' ? (c.selectAlias ? (c.sortProp + ' AS ' + c.selectAlias) : c.sortProp) : null; })).unique();
        var nodeFields = ["Caption", "NodeID", "Status", "GroupStatus", "StatusDescription", "IPAddress", "DNS", "SysName",
			"WebCommunityString.GUID", "EngineID", "RWCommunity"]
			.concat($.map(MNG.Ports.selectedColumns, function (c) { return c.sortType == 'Node' ? (c.selectAlias ? (c.sortProp + ' AS ' + c.selectAlias) : c.sortProp) : null; })).unique();

        var addTableAliasPrefix = function (clauses, prefix) { return $.map(clauses, function (s) { return prefix + "." + s; }); };


        var q = "\
	        SELECT {ifxFields}, {nodeFields} \
            FROM Orion.UDT.Port (nolock=true) P \
	        INNER JOIN Orion.Nodes (nolock=true) N ON P.NodeID=N.NodeID \
	        WHERE {whereFragment} AND N.NodeID IN (SELECT DISTINCT NodeID FROM Orion.UDT.NodeCapability (nolock=true))  {orderBy} \
	        WITH ROWS {startRow} TO {endRow}".format({
	            ifxFields: addTableAliasPrefix(ifxFields, 'P').join(", "),
	            nodeFields: addTableAliasPrefix(nodeFields, 'N').join(", "),
	            whereFragment: getWhereFragment("N", "P"),
	            orderBy: MNG.Sort.getOrderBy("N", "P"),
	            startRow: (Pager.actualPage - 1) * Pager.pageSize + 1,
	            endRow: Pager.actualPage * Pager.pageSize
	        });

        var q_totalcount = "SELECT COUNT(P.PortID) AS TotalCount" +
            " FROM Orion.UDT.Port (nolock=true) P " +
            " INNER JOIN Orion.Nodes (nolock=true) N ON P.NodeID=N.NodeID" +
            " WHERE " + getWhereFragment("N", "P") + " AND N.NodeID IN (SELECT DISTINCT NodeID FROM Orion.UDT.NodeCapability (nolock=true)) ";

        //alert(q_totalcount);
        MNG.DoQuery(q_totalcount, function (results) {
            if (results.Rows.length > 0) {
                Pager.totalCount = results.Rows[0]["TotalCount"];
                //alert("Pager.totalCount " + Pager.totalCount);
                Pager.Init();
            }
            //alert(results.Rows[0]["TotalCount"]);
        });

        MNG.DoQuery(q, function (results) {
            if (canceled) return;
            var check = $("#selectAll").get(0).checked;
            $(results.Rows).each(function () {

                var row = this;
                row.IconPath = '/Orion/UDT/Images/Status/';
                row.Type = 'P';
                row.Hostname = ($.trim(row.DNS) || $.trim(row.SysName) || row.IPAddress);
                row.GUID = "GUID{" + row.GUID + "}";

                //SJW - Orioginal code.  removed MakeInterfaceOrVolumeLink
                MNG.AddRow($("#NodeTree2>tbody"), [MNG.MakeCheckbox(row), MNG.MakePortLink(row)]
                //.concat($.map(MNG.Ports.selectedColumns, function (c) { return c.header == "Node" ? MNG.MakeNodeLink(row) : MNG.FormatCell(row[MNG.removeBrackets(c.selectAlias || c.sortProp)]); })))
                    .concat($.map(MNG.Ports.selectedColumns, function (c) {

                        if (c.header == "Node") { return MNG.MakeNodeLink(row); }
                        if (c.header == "Type") { return MNG.FormatPortTypeCell(row); }
                        if (c.header == "MAC Address") { return MNG.FormatMACAddressCell(row); }
                        if (c.header == "Monitored") { return MNG.FormatMonitoredCell(row, false); }
                        else { return MNG.FormatCell(row[MNG.removeBrackets(c.selectAlias || c.sortProp)]); }

                    })))
                	.find(":checkbox").data('netobject', row);

                //                MNG.AddRow($("#NodeTree2>tbody"), [MNG.MakeCheckbox(row), MNG.MakeNodeLink(row)]
                //					.concat($.map(MNG.Ports.selectedColumns, function (c) { return c.header == "Node" ? MNG.FormatNodeCell(row) : MNG.FormatCell(row[MNG.removeBrackets(c.selectAlias || c.sortProp)]); })))
                //					.find(":checkbox").data('netobject', row);
            });
            $("#NodeTree2 tr td :checkbox").click(MNG.EnableDisableMenu);
            MNG.stripeTree();
            if (typeof (onComplete) == 'function') {
                onComplete();
            }
            //$("#testoutput").html(results.Rows.length + " " + MNG.nodePageSize);
            //alert(results.Rows.length + ' >= ' + MNG.nodePageSize);

            //            if (results.Rows.length >= MNG.nodePageSize) {
            //                //setTimeout(function() {
            //                //if (canceled) return;
            //                loadSome(objectsLoaded + results.Rows.length);
            //                //}, 3000);
            //                //$("#loadingStatus span").text("Loaded " + (nodesLoaded + results.Rows.length) + " nodes, continuing...");
            //            } else {
            //                //$("#loadingStatus").hide();
            //                if (typeof (onComplete) == 'function')
            //                    onComplete();
            //            }
        });
    };
    loadSome(0);
};


//MNG.Nodes.WithAllIDs = function (callback) {
//    MNG.DoQuery(SWQLQueries.SelectNodeIds + MNG.GetWhere("N"), function (results) {
//        var ids = [];
//        $(results.Rows).each(function () {
//            ids.push("N:" + this.NodeID);
//        });
//        callback(ids);
//    });
//};

MNG.Nodes.WithAllIDsWithEngineIds = function (callback) {
    MNG.DoQuery(SWQLQueries.SelectNodeAndEngineIds + MNG.GetWhere("N"), function (results) {
        var ids = [];
        $(results.Rows).each(function () {
            ids.push("N:" + this.NodeID + ":" + this.EngineID);
        });
        callback(ids);
    });
};

MNG.Nodes.WithAllIDsWithoutPrefix = function (callback) {

    MNG.DoQuery(SWQLQueries.SelectNodeIds + MNG.GetWhere("N"), function (results) {
        var ids = [];
        $(results.Rows).each(function () { ids.push(this.NodeID); });
        callback(ids);
    });
};

MNG.Nodes.WithManagedObjectCount = function (callback) {
    MNG.DoQuery(SWQLQueries.SelectManageNodeCount + MNG.GetWhere("N"), function (results) {
        callback(results.Rows[0].Cnt, true);
    });
};

MNG.Nodes.WithUnmanagedObjectCount = function (callback) {
    MNG.DoQuery(SWQLQueries.SelectUnmanageNodeCount + MNG.GetWhere("N"), function (results) {
        callback(results.Rows[0].Cnt, false);
    });
};

MNG.Nodes.WithAllIDsUnMonitoredNodes = function (callback) {
    MNG.DoQuery(SWQLQueries.SelectUnMonitoredNodeIds + MNG.GetWhere("N"), function (results) {
        var ids = [];
        $(results.Rows).each(function () { ids.push(this.NodeID); });
        callback(ids);
    });
};

//MNG.Nodes.WithAllFormatedSNMPIDs = function (callback) {
//    MNG.DoQuery(SWQLQueries.SelectSNMPNodesIds + MNG.GetWhere("N"), function (results) {
//        var ids = [];
//        $(results.Rows).each(function () { ids.push(this.NodeID); });
//        callback(ids.join(","), "N");
//    });
//};

//MNG.Nodes.StoreAllFormatedSNMPIDs = function () {
//    NodeManagement.StoreAllIDs(SWQLQueries.SelectSNMPNodesIds + MNG.GetWhere("N"), "Nodes", function () {
//        window.location = MNG.customPollerUrl.format(["Node", $('#ReturnToUrl').val()]);
//    }, function (error) { alert("Assign Pollers failed!\n" + error.get_message()); });
//};

//MNG.Ports.WithAllIDs = function (callback) {
//    MNG.DoQuery(SWQLQueries.SelectPortIds + MNG.GetWhere("N"), function (results) {
//        var ids = [];
//        $(results.Rows).each(function () { ids.push("P:" + this.PortID); });
//        callback(ids);
//    });
//};


MNG.Ports.WithAllIDsWithEngineIds = function (callback) {
    MNG.DoQuery(SWQLQueries.SelectPortAndEngineIds + MNG.GetWhere("N"), function (results) {
        var ids = [];
        $(results.Rows).each(function () { ids.push("P:" + this.PortID + ":" + this.EngineID); });
        callback(ids);
    });
};

MNG.Ports.WithAllIDsWithoutPrefix = function (callback) {
    MNG.DoQuery(SWQLQueries.SelectPortIds + MNG.GetWhere("N"), function (results) {
        var ids = [];
        $(results.Rows).each(function () { ids.push(this.PortID); });
        callback(ids);
    });
};

MNG.Ports.WithAllIDsAndIsMonitored = function (callback) {
    MNG.DoQuery(SWQLQueries.SelectPortIdsAndIsMonitored + MNG.GetWhere("N"), function (results) {
        var ids = { P: [], Unmonitored: [], Monitored: []};
        $(results.Rows).each(function () { 
            ids.P.push(this.PortID);
            ids.Monitored.push(this.IsMonitored);
        });
        callback(ids);
    });
};

MNG.Ports.WithAllIDsAndIsNotMonitored = function (callback) {
    MNG.DoQuery(SWQLQueries.SelectPortIdsAndIsNotMonitored + MNG.GetWhere("N"), function (results) {
        var ids = { P: [], Unmonitored: [], Monitored: [] };
        $(results.Rows).each(function () {
            ids.P.push(this.PortID);
            ids.Unmonitored.push(this.IsMonitored);
        });
        callback(ids);
    });
};
//MNG.Ports.WithAllFormatedSNMPIDs = function (callback) {
//    MNG.DoQuery(SWQLQueries.SelectPortIds + MNG.GetWhere("N"), function (results) {
//        var ids = [];
//        $(results.Rows).each(function () { ids.push(this.PortID); });
//        callback(ids.join(","), "P");
//    });
//};

//MNG.Ports.StoreAllFormatedSNMPIDs = function () {
//    NodeManagement.StoreAllIDs(SWQLQueries.SelectPortIds + MNG.GetWhere("N"), "Ports", function () {
//        window.location = MNG.customPollerUrl.format(["Port", $('#ReturnToUrl').val()]);
//    }, function (error) { alert("Assing Pollers failed!\n" + error.get_message()); });
//};

//MNG.Ports.WithAllIDsAndNames = function (callback) {
//    MNG.DoQuery(SWQLQueries.SelectPortIdsAndNames + MNG.GetWhere("N"), function (results) {
//        var ids = { ID: [], Name: [] };
//        $(results.Rows).each(function () { ids.ID.push(this.PortID); ids.Name.push(this.Caption); });
//        callback(ids);
//    });
//};

MNG.Ports.WithMonitoredObjectCount = function (callback) {
    MNG.DoQuery(SWQLQueries.SelectMonitoredPortCount + MNG.GetWhere("N"), function (results) {
        callback(results.Rows[0].Cnt, true);
    });
};

MNG.Ports.WithUnmonitoredObjectCount = function (callback) {
    MNG.DoQuery(SWQLQueries.SelectUnmonitoredPortCount + MNG.GetWhere("N"), function (results) {
        callback(results.Rows[0].Cnt, false);
    });
};

MNG.Ports.WithShutdownPortObjectCount = function (callback) {
    MNG.DoQuery(SWQLQueries.SelectShutdownPortCount + MNG.GetWhere("N"), function (results) {
        callback(results.Rows[0].Cnt, false);
    });
};
MNG.Ports.WithEnablePortObjectCount = function(callback) {
    MNG.DoQuery(SWQLQueries.SelectEnablePortCount + MNG.GetWhere("N"), function(results) {
        callback(results.Rows[0].Cnt, false);
    });
};

MNG.Nodes.SelectedPrefix = function () { return "N"; };
MNG.Ports.SelectedPrefix = function () { return "P"; };

// SJW - this function needs a clean-up as probably contains redundant code
MNG.GetSelectedObjectIDs = function () {
    var ids = { N: [], P: [], ICMP: [], SNMP: [], Unmanaged: [], Managed: [], NetObjectIDs: [], EngineNetObjectIDs: [], AdminShutdown: [], AdminNotShutdown: [], External: [], NonExternal: [], Unmonitored: [], Monitored: [], NodeID: [], PortName: [], rwCommunity: [], Caption: [] };
    $('[name*=":"]:checked').each(function () {
        ids.NetObjectIDs.push(this.name);
        var parts = this.name.split(":");
        var obj = $(this).data('netobject');
        ids.NodeID.push(obj.NodeID);
        ids.Caption.push(obj.Caption);
        ids.EngineNetObjectIDs.push(this.name + ':' + obj.EngineID);
        // this pushes the NetObjectID onto the N or P array
        //ids[parts[0]].push(parts[1]);
        ids.rwCommunity.push(obj.RWCommunity);
        if (parts[0] == "N") {
            ids[parts[0]].push(parts[1]);
            ids[obj.ObjectSubType].push(parts[1]);
            ids[obj.Unmanaged ? "Unmanaged" : "Managed"].push(parts[1]);
            ids[obj.Status == "11" ? "External" : "NonExternal"].push(parts[1]);
        }

        if (parts[0] == "P") {
            //            alert('PortID: '+ obj.PortID);
            ids.P.push(obj.PortID);
            //            ids[obj.Unmanaged ? "Unmanaged" : "Managed"].push(parts[1]);
            (obj.AdministrativeStatus == 1 ? ids.AdminShutdown : ids.AdminNotShutdown).push(parts[1]);
            //(ids.AdminShutdown).push(parts[1]);
            ids[obj.IsMonitored ? "Monitored" : "Unmonitored"].push(parts[1]);

            ids.PortName.push(obj.Name);
        }

    });
    return ids;
};

MNG.GetSelectedNodeIDs = function () {
    return MNG.GetSelectedObjectIDs().N;
};

MNG.GetSelectedPortIDs = function () {
    return MNG.GetSelectedObjectIDs().P;
};

// SJW - this function probably needs a bit of a clean-up as most likely has some redundant code.
MNG.EnableDisableMenu = function () {
    if (MNG.suspendEnableDisableMenu) return;
    var ids = MNG.GetSelectedObjectIDs();
    var N = ids.N.length, P = ids.P.length, SNMP = ids.SNMP.length, ICMP = ids.ICMP.length,
		Managed = ids.Managed.length, Unmanaged = ids.Unmanaged.length, External = ids.External.length,
		NonExternal = ids.NonExternal.length, AdminShutdown = ids.AdminShutdown.length, AdminNotShutdown = ids.AdminNotShutdown.length,
        Monitored = ids.Monitored.length, Unmonitored = ids.Unmonitored.length;

    var disable = function (id) { $("#" + id + "Link").parent().addClass("Disabled"); };
    $(".NodeManagementMenu li").removeClass("Disabled");

    if (($("#ShowUDTMonitorNodeStat").val().toLowerCase() == 'monitored_nodes') || $("#showObjectType").val() == 'Ports') {
        $("#monitornodebyUDTLink").parent().hide();
    }
    // alert('N:' + N + ' P:' + P + ' Monitored:' + Monitored + ' Unmonitored:' + Unmonitored + ' Managed:' + Managed + ' Unmanaged:' + Unmanaged);
  
    if (N == 0 && P == 0) { disable('edit'); disable('monitornodebyUDT'); }
    if ((N > 0 && $("#ShowUDTMonitorNodeStat").val().toLowerCase() == 'monitored_nodes') || $("#showObjectType").val() == 'Ports') {
        $("#bannerImportNodesToUDT").hide();
        $("#monitornodebyUDTLink").parent().hide();
    }
    else if ($("#ShowUDTMonitorNodeStat").val().toLowerCase() == 'unmonitored_nodes') {
        $("#pollNowLink").parent().hide();
    }
    
    if (N > 0 && P > 0) disable('edit');
    //    if (N > 0) disable('edit');
    if (P > 0) disable('edit');
    //if (P == 0 || N > 0) disable('powerLevel');
    //if (SNMP != 1 || P > 0 || External > 0) disable('listResources');
    if (Managed == 0 || P > 0) { disable('unmanage'); }
    if (Unmanaged == 0 || P > 0) { disable('remanage'); }
    if (N > 0 || P == 0 || Monitored == 0) { disable('unmonitor'); }
    if (N > 0 || P == 0 || Unmonitored == 0) { disable('remonitor'); }
    if (N > 0 || P == 0 || AdminShutdown == 0) { disable('shutdown'); }
    if (N > 0 || P == 0 || AdminNotShutdown == 0) { disable('enable'); }
    //if (AdminNotShutdown == 0 || AdminShutdown > 0 || N > 0 || V > 0) disable('shutdown');
    //if (AdminShutdown == 0 || AdminNotShutdown > 0 || N > 0 || V > 0) disable('enable');

    if (N == 0 && P == 0) { disable('delete'); disable('pollNow'); disable('rediscover') }
    //if (P > 0) { disable('delete'); disable('pollNow'); disable('rediscover') }
    if (P > 0) { disable('pollNow'); disable('rediscover') }

    //    if (AdminNotShutdown == 0 || AdminShutdown > 0 || N > 0) disable('shutdown');
    //    if (AdminShutdown == 0 || AdminNotShutdown > 0 || N > 0) disable('enable');
    if ($("#isDemoMode").length != 0) {
        disable('edit'); disable('listResources'); disable('unmanage'); disable('remanage');
        disable('pollNow'); disable("rediscover"); disable('shutdown'); disable('enable'); disable('delete'); disable('add');
        disable('unmonitor'); disable("remonitor");
        $("#addLink")[0].href = "#";
    }
    var objectType = MNG.Prefs.load("UDTObjectType", "Nodes").substring(0, 1);

    if ($('[name*="' + objectType + ':"]:checked').length != $('[name*="' + objectType + ':"]').length) {
        //alert('checking are all items checked');
        MNG.allSelected = false;
        $("#selectAllPan").empty();
        $("#selectAll").removeAttr("checked");
    }

    //alert('allselected:' + MNG.allSelected);
    if (MNG.allSelected) {
        // all object across pages are selected lets find out if we can do unmanage or remanage
        //alert('P ==?' + P);
        //        MNG.NetObjects.WithManagedObjectCount(Operations.EnableDisableManageButton);
        //        MNG.NetObjects.WithUnmanagedObjectCount(Operations.EnableDisableManageButton);
        MNG.Nodes.WithManagedObjectCount(Operations.EnableDisableManageButton);
        MNG.Nodes.WithUnmanagedObjectCount(Operations.EnableDisableManageButton);

        MNG.Ports.WithMonitoredObjectCount(Operations.EnableDisableMonitorButton);
        MNG.Ports.WithUnmonitoredObjectCount(Operations.EnableDisableMonitorButton);

        MNG.Ports.WithShutdownPortObjectCount(Operations.EnableDisableShutdownButton);
        MNG.Ports.WithEnablePortObjectCount(Operations.EnableDisableShutdownButton);
    }


};

MNG.stripeTree = function () {
   /* $("#NodeTree2 tbody tr:nth-child(odd)").css('background-color', '#ECF6FB');
    $("#NodeTree2 tbody tr:nth-child(even)").css('background-color', 'white');*/
};

MNG.quoteValueForSql = function (query) {
    //return query.replace(/'/g, "''");

    var filterText = query;

    if (filterText != "") {

        //replace all single quote ' instances to a double single quote instance.  e.g. test's becomes test''s
        filterText = filterText.replace(/\'/g, "''");
    }

    return filterText;
};

MNG.search = function () {

    var query = filterText;
    
    MNG.CancelLoading();
    $(".SelectedNodeGroupItem").removeClass("SelectedNodeGroupItem");
    MNG.ClearTable();

    $("#selectAll").get(0).checked = false;
    var fields = { Node: [], Port: [] };
    $("th[sortProp]").each(function () {
        fields[$(this).attr('sortType')].push($(this).attr('sortProp'));
    });
	
	var quoteSearchValueForSwql = function (str) {
        var filterText = query;

		if (filterText != "") {
			filterText = filterText.replace(/\'/g, "''");
			filterText = filterText.replace(/\*/g, "%");
			filterText = filterText.replace(/\?/g, "_");
			
			//if looks like part of mac address then strip formatting before search
			if ((filterText.indexOf(":") != -1) || (filterText.indexOf(".") != -1) || (filterText.indexOf("-") != -1)) {
				//first, strip out any * wildcard chars
				var tmpSearchText = filterText.replace(/\%/g, "").replace(/\_/g, "");
				var ciscoDot = /^([0-9a-f]{0,4})(([.-]|$)([0-9a-f]{0,4}$|(([0-9a-f]{4}[.-]$)|(([0-9a-f]{4}[.-][0-9a-f]{0,4}$)))))/i; //this works OK!
				var unix = /^([0-9a-f]{0,2})(([:-]|$)([0-9a-f]{0,2}$|(([0-9a-f]{2}[:-]$)|(([0-9a-f]{2}[:-][0-9a-f]{0,2}$)|((([0-9a-f]{2}[:-]){2}[0-9a-f]{0,2}$)|((([0-9a-f]{2}[:-]){3}[0-9a-f]{0,2}$)|((([0-9a-f]{2}[:-]){4}[0-9a-f]{0,2}$))))))))/i;
				var windows = /^([0-9a-f]{0,2})(([--]|$)([0-9a-f]{0,2}$|(([0-9a-f]{2}[--]$)|(([0-9a-f]{2}[--][0-9a-f]{0,2}$)|((([0-9a-f]{2}[--]){2}[0-9a-f]{0,2}$)|((([0-9a-f]{2}[--]){3}[0-9a-f]{0,2}$)|((([0-9a-f]{2}[--]){4}[0-9a-f]{0,2}$))))))))/i;

				if (ciscoDot.test(tmpSearchText)) filterText = filterText.replace(/\./g, "");
				if (unix.test(tmpSearchText)) filterText = filterText.replace(/\:/g, "");
				if (windows.test(tmpSearchText)) filterText = filterText.replace(/\-/g, "");
			}
		}
		return filterText;
    };
	
    var searchClauses = function (fields, tableAlias) {
        var searchClause = function (field) { return "{0}.{1} LIKE '%{2}%'".format([tableAlias, field, quoteSearchValueForSwql(query)]); }
        return $.map(fields, searchClause);
    };

    var quoteForRegExp = function (str) {
        var toQuote = '\\/[](){}?+*|.^$'; // note: backslash must be first
        for (var i = 0; i < toQuote.length; ++i)
            str = str.replace(toQuote.charAt(i), '\\' + toQuote.charAt(i));
        return str;
    };
    MNG.NetObjects.Load(function (nodeAlias, interfaceAlias) {
        var clauses = searchClauses(fields.Node, nodeAlias).concat(searchClauses(fields.Port, interfaceAlias));
        return "(" + clauses.join(" OR ") + ")";
    }, function () {
        // search term highlighting adapted from http://dossy.org/archives/000338.html
        var re = new RegExp().compile('(' + quoteForRegExp(query) + ')', 'ig');
        $("#NodeTree2 tbody *").each(function () {
            if ($(this).children().size() > 0) return;
            var html = $(this).html();
            var newhtml = html.replace(re, '<span class="searchterm">$1</span>');
            if (html) {
                $(this).html(newhtml);
            }
        });
        if ($("#NodeTree2>tbody>tr").length == 0) {
            $("#NodeTree2>tbody").append("<tr><td></td></tr>").find("td").attr('colSpan', $("#NodeTree2 th").length).css("text-align", "center").css("padding", "1em")
				.text("No {0}s were found matching '{1}'.".format([MNG.NetObjects.typename.toLowerCase(), query]));
        }
    });
};

MNG.Nodes.selectAll = function () {
    var c = this.checked;
    MNG.suspendEnableDisableMenu = true;
    $('input:checkbox[name^="N:"]').each(function () {
        this.checked = c;
    });
    // always uncheck any ports when doing select all on nodes
    $('input:checkbox[name*=":"]').not('[name^="N:"]').each(function () {
        this.checked = false;
    });
    MNG.suspendEnableDisableMenu = false;
    MNG.EnableDisableMenu();

    MNG.DisplaySelectPan(this.checked);
};

MNG.Ports.selectAll = function () {
    var c = this.checked;
    MNG.suspendEnableDisableMenu = true;
    $('input:checkbox[name^="P:"]').each(function () {
        this.checked = c;
    });
    MNG.suspendEnableDisableMenu = false;
    MNG.EnableDisableMenu();

    MNG.DisplaySelectPan(this.checked);
};

MNG.showObjectType = function (objectType, skipLoadingObjects) {

    //
    //alert("MNG.showObjectType function: " + objectType);
    //
    Pager.LoadSettings();
    Pager.forbidReset = true;

    MNG.Prefs.save("UDTObjectType", objectType);

    // only show the Node Capability grouping option in Group by dropdown is we are showing Nodes
    if ($("#groupByProperty option[value='Capability']").length == 0) {
        if ((objectType == "Nodes") && ($("#ShowUDTMonitorNodeStat").val() == "Monitored_Nodes")) {
            $("#groupByProperty").append('<option value="Capability">@{R=UDT.Strings;K=UDTWEBJS_AK1_86;E=js}</option>');
        }
    }
    else {
        //remove if not showing Nodes
        if (objectType != "Nodes") {
            $("#groupByProperty option[value='Capability']").remove();
        }
    }
    
    if (objectType != "Nodes") {
        $("#ShowUDTMonitorNodeStat").hide();
        $("#ShowUDTMonitorNodeStatlbl").hide();
    } else {
        $("#ShowUDTMonitorNodeStat").show();
        $("#ShowUDTMonitorNodeStatlbl").show();
    }
    
    // only show the Port Type grouping option in Group by dropdown is we are showing Ports
    if ($("#groupByProperty option[value='PortType']").length == 0) {
        if (objectType == "Ports") {
            $("#groupByProperty").append('<option value="PortType">@{R=UDT.Strings;K=UDTWEBJS_AK1_85;E=js}</option>');
        }
    }
    else {
        //remove if not showing Ports
        if (objectType != "Ports") {
            $("#groupByProperty option[value='PortType']").remove();
        }
    }
    
    MNG.NetObjects = (objectType == "Nodes") ? MNG.Nodes : MNG.Ports;

    MNG.NetObjects.selectedColumns = MNG.loadColumns(MNG.NetObjects, MNG.NetObjects.DefaultColumns);
    MNG.NetObjects.createColumnHeaders();


    $("<img id='addColumns' alt='@{R=UDT.Strings;K=UDTWEBJS_AK1_87;E=js}' src='" + MNG.chooseColumnsImg + "'>").prependTo("#NodeTree2 thead th:last").css({ 'margin': '5 5 5 5' }).click(function () {
        MNG.chooseColumns();
        return false;
    });
    MNG.Sort.loadPrefs();
    $("#selectAll").click(MNG.NetObjects.selectAll);
    $("th[sortProp]").click(function () {
        MNG.Sort.by($(this).attr('sortType'), $(this).attr('sortProp'));
        MNG.RefreshObjects();
    });
    MNG.LoadGroupByValues();
};

MNG.ShowUDTMonitorNodeStat = function (objectType) {
    
    MNG.Prefs.save("UDTObjectTypeFilter", objectType);
    Pager.LoadSettings();
    Pager.forbidReset = true;
    
   // MNG.Prefs.save("UDTMonUnMonObjectType", objectType);
    // only show the Node Capability grouping option in Group by dropdown is we are showing Nodes
    if ($("#groupByProperty option[value='Capability']").length == 0) {
        if (($("#showObjectType").val() == "Nodes") && (objectType == "Monitored_Nodes")) {
            $("#groupByProperty").append('<option value="Capability">@{R=UDT.Strings;K=UDTWEBJS_AK1_86;E=js}</option>');
        }
    }
    else {
        //remove if not showing unmonitor nodes
        if ($("#showObjectType").val() != "Nodes" || objectType == "Unmonitored_Nodes") {
            $("#groupByProperty option[value='Capability']").remove();
        }
        
    }


    // only show the Port Type grouping option in Group by dropdown is we are showing Ports
    if ($("#groupByProperty option[value='PortType']").length == 0) {
        if ($("#showObjectType").val() == "Ports") {
            $("#groupByProperty").append('<option value="PortType">@{R=UDT.Strings;K=UDTWEBJS_AK1_85;E=js}</option>');
        }
    }
    else {
        //remove if not showing Ports
        if ($("#showObjectType").val() != "Ports") {
            $("#groupByProperty option[value='PortType']").remove();
        }
    }
   
    MNG.NetObjects = MNG.Nodes;
    MNG.NetObjects.selectedColumns = MNG.loadColumns(MNG.NetObjects, MNG.NetObjects.DefaultColumns);
    MNG.NetObjects.createColumnHeaders();


    $("<img id='addColumns' alt='@{R=UDT.Strings;K=UDTWEBJS_AK1_87;E=js}' src='" + MNG.chooseColumnsImg + "'>").prependTo("#NodeTree2 thead th:last").css({ 'margin': '5 5 5 5' }).click(function () {
        MNG.chooseColumns();
        return false;
    });
    MNG.Sort.loadPrefs();
    $("#selectAll").click(MNG.NetObjects.selectAll);
    $("th[sortProp]").click(function () {
        MNG.Sort.by($(this).attr('sortType'), $(this).attr('sortProp'));
        MNG.RefreshObjects();
    });
    MNG.LoadGroupByValues();
};

MNG.chooseColumns = function () {
    var list = $("#availableColumnList").html('');
    $(MNG.NetObjects.AvailableColumns).each(function (i) {
        if (this.headerText == null) 
            this.headerText = this.header;
        $("<li><input type='checkbox' id='{id}' header='{header}' /> <label for='{id}'>{headerText}</label></li>".format({ id: "columnChooser" + i, header: this.header, headerText: this.headerText })).appendTo(list);
    });
    var check = function (obj) {
        if ($.browser.msie && $.browser.version == "6.0")
            obj.defaultChecked = true;
        else
            obj.checked = true;
    };
    $(MNG.NetObjects.selectedColumns).each(function () {
        $(":checkbox[header='" + this.header + "']").each(function () { check(this); });
    });
    columnChooserOKClick = function() {
        var headers = []
        $("#availableColumnList :checked[header]").each(function() {
            headers.push($(this).attr('header'));
        });
        MNG.Prefs.save("UDT" + MNG.NetObjects.typename + "Columns", headers.join(','));
        $("#showObjectType").change();
        $("#ShowUDTMonitorNodeStat").change();
        // close the dialog
        $("#columnChooserDialog").dialog('close');
        
    };
    columnChooserCancelClick = function () {
        $("#columnChooserDialog").dialog('close');
    };
    var contents = $("#columnChooserDialogContents");

    contents.appendTo("#columnChooserDialog");

    $("#columnChooserDialog").show().dialog({
        width: 600, height: "auto", modal: true, overlay: { "background-color": "black", opacity: 0.4 }, bgiframe: true
    });
};

// start the search
MNG.startSearch = function () {
    //var query = $("#search").val();
    Pager.Reset();
    Pager.isInSearchMode = true;
    MNG.RefreshObjects = function () {
        MNG.Sort.setArrow();
        MNG.search();
    };
    MNG.RefreshObjects();
};

MNG.loadColumns = function (type, defaults) {
    var selectedHeaders = MNG.Prefs.load("UDT" + type.typename + "Columns", defaults).split(',');
    return $.grep(type.AvailableColumns, function (col) { return selectedHeaders.contains(col.header); });
};

MNG.ReportError = function (error, lastError) {
    if (lastError) {
        lastError.text(String.format("@{R=UDT.Strings;K=UDTWEBJS_AK1_88;E=js}", error)).show();
    }
    else {
        $('#lastErrorMessage').text(String.format("@{R=UDT.Strings;K=UDTWEBJS_AK1_89;E=js}", error.get_message())).show();
    }
};


// SJW: need to amend below to set status icon for UDT Port
MNG.UpdateStatusIconForInterface = function (interfaceId) {
    MNG.DoQuery("SELECT I.StatusLED, I.AdminStatus FROM Orion.NPM.Interfaces I WHERE I.InterfaceID=" + interfaceId, function (results) {
        $("img.StatusIcon[NetObject=I:" + interfaceId + "]").attr('src', '/Orion/images/StatusIcons/Small-' + results.Rows[0].StatusLED);
        $.each($("input:checkbox[name=I:" + interfaceId + "]"), function () {
            $(this).data('netobject').AdminStatus = results.Rows[0].AdminStatus;
        });
        MNG.EnableDisableMenu();
    });
};

MNG.UpdateStatusIconForNode = function (nodeId) {
    MNG.DoQuery("SELECT N.GroupStatus, N.Unmanaged, N.StatusDescription FROM Orion.Nodes (nolock=true) N WHERE N.NodeId=" + nodeId, function (results) {
        $("img.StatusIcon[NetObject=N:" + nodeId + "]").attr('src', '/Orion/images/StatusIcons/Small-' + results.Rows[0].GroupStatus);
        $.each($("input:checkbox[name=N:" + nodeId + "]"), function () {
            $(this).data('netobject').Unmanaged = results.Rows[0].Unmanaged;
            for (var n = 0; n < MNG.Nodes.selectedColumns.length; ++n) {
                if (MNG.Nodes.selectedColumns[n].sortProp == "StatusDescription") {
                    $(this).parent().parent().find("td:eq(" + (n + 2) + ")").text(results.Rows[0].StatusDescription);
                    break;
                }
            }
        });
        MNG.EnableDisableMenu();
    });
};

MNG.DisplaySelectPan = function (checked) {
    $("#selectAllPan").empty();
    //console.info("Pager.totalCount:" + Pager.totalCount + " Pager.pageSize:" + Pager.pageSize);

    //alert("Pager.totalCount:" + Pager.totalCount + " Pager.pageSize:" + Pager.pageSize);

    if (checked && (Pager.totalCount > Pager.pageSize)) {
        //alert('loading object type');
        var objectType = MNG.Prefs.load("UDTObjectType", "Nodes");
        //alert('objectType: ' + objectType);
        var countOfObjectsOnPage = Pager.pageSize;
        if ((Pager.actualPage == Pager.pageCount) && ((Pager.totalCount % Pager.pageSize) != 0)) {
            countOfObjectsOnPage = Pager.totalCount % Pager.pageSize;
        }
        $("#selectAllPan").append(String.format("@{R=UDT.Strings;K=UDTWEBJS_AK1_91;E=js}", "<span>", objectType, countOfObjectsOnPage, "</span><a href='#'>", objectType, Pager.totalCount, "</a>")).addClass("select-whole-page");
        $("#selectAllPan a").click(function () {
            //alert('checked all clicked');
            var objectType = MNG.Prefs.load("UDTObjectType", "Nodes");
            //alert('objectType2: ' + objectType);
            $("#selectAllPan").empty();
            $("#selectAllPan").append(String.format("@{R=UDT.Strings;K=UDTWEBJS_AK1_92;E=js}", "<span>", objectType, "</span>")).addClass("select-all-objects");
            $("#selectAllPan a ").click(function () {

            });
            MNG.allSelected = true;
            MNG.EnableDisableMenu();
        })
    }
    else {
        //alert('CHANGE allselected to false');
        MNG.allSelected = false;
    }
};

MNG.getPortCount = function (type) {
    // use JQuery to determine which option is checked
    var item;
    //alert('type:' + type);
    if (type == "max") {
        item = $('#txtMaxPorts').val();
    }
    else {
        item = $('#txtCurrentPorts').val();
    }
    return item;
}


MNG.changePortMonitoring = function (ids, setToStatus) {
    // Changes monitoring flag for selected ports function.  Pops a confirmation dialog and call on webservice to perform monitoring change.
    var maxPorts = MNG.getPortCount("max");
    var currentPorts = MNG.getPortCount("current");
    var unmonitoredSelectedPorts = ids.Unmonitored.length;
    var monitoredSelectedPorts = ids.Monitored.length;
    //alert('max:' + maxPorts + ' / current:' + currentPorts + ' / unmonitoredSelected:' + unmonitoredSelectedPorts);

    var totalMonitoredCount = parseInt(currentPorts) + parseInt(unmonitoredSelectedPorts);
    //alert('totalports:' + totalMonitoredCount);
    var state = (setToStatus == "monitor" ? true : false);
    //alert('state: ' + state);

    if (totalMonitoredCount > maxPorts && state) {
        var portCountOver = (totalMonitoredCount - parseInt(maxPorts))
        var layout = $("#overLicenseLimitDialogDescription");
        layout.html("<font color=red>" + String.format('@{R=UDT.Strings;K=UDTWEBJS_AK1_93;E=js}', "<b>" + maxPorts + "</b>", portCountOver) + "</font>");

        if (!overLicenseLimitDialog) {
            overLicenseLimitDialog = new Ext.Window({
                applyTo: 'overLicenseLimitDialog',
                layout: 'fit',
                width: 310,
                height: 205,
                closeAction: 'destroy',
                modal: true,
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'overLicenseLimitDialogBody',
                    layout: 'fit',
                    border: false
                }),
                buttons: [
                        {
                            text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_5;E=js}',
                            style: 'margin-left: 5px;',
                            handler: function () {
                                overLicenseLimitDialog.hide();
                            }
                        }
                    ]
            });
        }
        // Set the location
        overLicenseLimitDialog.alignTo(document.body, "c-c");
        overLicenseLimitDialog.show();
    }
    else {
//        alert('ids length:' + ids.P.length);
//        alert('monitored id count:' + ids.Monitored.length);
//        alert('UNmonitored id count:' + ids.Unmonitored.length);

        var selectedCount = ids.P.length;
        
        // set selected record count to the totalCount if the 'select all' link was clicked
        if (MNG.allSelected){
            selectedCount = Pager.totalCount;
        }

        //alert('isall seleted:' + MNG.allSelected);

        //alert('set to: ' + setToStatus);
        var layout = $("#monitoringDialogDescription");

        layout.html(String.format('@{R=UDT.Strings;K=UDTWEBJS_AK1_90;E=js}', String.format('<b><font color={0}>{1}</font></b>', (setToStatus == "monitor" ? "green" : "red"), setToStatus), selectedCount));

        if (!monitoringDialog) {
            monitoringDialog = new Ext.Window({
                applyTo: 'monitoringDialog',
                layout: 'fit',
                width: 310,
                height: 205,
                closeAction: 'hide',
                modal: true,
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'monitoringDialogBody',
                    layout: 'fit',
                    border: false
                }),
                buttons: [

                {
                    text: '@{R=UDT.Strings;K=UDTWEBJS_AK1_50;E=js}',
                    handler: function () {

                        if (ids.P.length > 0)

                            UdtPortManagement.UdtChangePortMonitoringState(ids.P, state,
                            function () {

                                window.location.reload();

                            },

                            function (error) {
                                alert(error.get_message() + '\n' + error.get_stackTrace());

                                $("#monitoringDialog").dialog("close");

                            }
                            );

                        monitoringDialog.hide();
                    }
                },

                {
                    text: '@{R=UDT.Strings;K=UDTWEBJS_AK1_51;E=js}',
                    style: 'margin-left: 5px;',
                    handler: function () {
                        monitoringDialog.hide();
                    }
                }
                ]
            });

        }
        // }

        // Set the location
        monitoringDialog.alignTo(document.body, "c-c");
        monitoringDialog.show();
    }
    return false;
};


$(function () {
    if ($('#PortCustomProperties').val()) {
        $($('#PortCustomProperties').val().split(',')).each(function () {
            MNG.Ports.AvailableColumns.push({
                sortType: "Port",
                sortProp: "CustomProperties.[" + this + "]",
                selectAlias: "[p_" + this + "]",
                header: this,
                headerText: this
            });
        });
    }

    if ($('#NodeCustomProperties').val()) {
        $($('#NodeCustomProperties').val().split(',')).each(function () {
            MNG.Nodes.AvailableColumns.push({
                sortType: "Node",
                sortProp: "CustomProperties.[" + this + "]",
                selectAlias: "[n_" + this + "]",
                header: this,
                headerText: this
            });
            MNG.Ports.AvailableColumns.push({
                sortType: "Node",
                sortProp: "CustomProperties.[" + this + "]",
                selectAlias: "[n_" + this + "]",
                header: "Node " + this,
                headerText: String.format("@{R=UDT.Strings;K=UDTWEBJS_AK1_113;E=js}", this)
            });
        });
    }

    // default the page to show 'Ports' (in the Show: drop-down)
    var objectType = MNG.Prefs.load("UDTObjectType", "Ports");  //Modifying based on 
    //var objectType = MNG.Prefs.load("UDTMonUnMonObjectType", "Ports");
    var objectTypeFilter = MNG.Prefs.load("UDTObjectTypeFilter", "Monitored_Nodes");
    $("#groupByProperty").val(MNG.Prefs.load("UDTGroupBy", "Vendor")).change(MNG.LoadGroupByValues);
    $("#showObjectType").val(objectType).change(function () { MNG.showObjectType($(this).val()); });
    $("#ShowUDTMonitorNodeStat").val(objectTypeFilter).change(function () {
        MNG.ShowUDTMonitorNodeStat($(this).val());
    });
    MNG.showObjectType(objectType, true);
    //MNG.ShowUDTMonitorNodeStat(objectType, true);
    // causes double data loading during page load
    //$("#groupByProperty").change();

    $("form").submit(function () { return false; });

    //    $("#search").keypress(function (e) {
    //        if (e.which == 13)
    //            MNG.startSearch();
    //    });
    //    $("#searchButton").click(MNG.startSearch);

    $(".NodeManagementMenu li").hover(function () {
        $(this).addClass("over");
    }, function () {
        $(this).removeClass("over");
    });

    if ($("#isDemoMode").length == 0) {
        $("#editLink").click(function () {
            if (!$(this).parent().hasClass("Disabled")) {

                //sjw
                //alert("MNG.NetObjects.SelectedPrefix: " + MNG.NetObjects.SelectedPrefix());

                if (MNG.allSelected) {
                    if (MNG.NetObjects.SelectedPrefix() == "P")
                        MNG.NetObjects.WithAllIDsWithEngineIds(Operations.EditProperties);
                    else
                        MNG.NetObjects.WithAllIDsWithoutPrefix(Operations.EditProperties);
                }
                else {
                    var ids = MNG.GetSelectedObjectIDs();

                    if (ids.N.length)
                        window.location = MNG.editPropertiesUrl.format(["Node", ids.N.join(","), $('#ReturnToUrl').val()]);
                    //                    else if (ids.P.length) {
                    //                        $('body').append(Operations.PreparePostForm("Interface", ids.EngineNetObjectIDs, $('#ReturnToUrl').val()));
                    //                        $('#selectedNetObjects').submit();
                    //                    }
                }
            }
            return false;
        });
        $("#listResourcesLink").click(function () {
            if (!$(this).parent().hasClass("Disabled")) {
                window.location = MNG.listResourcesUrl.format([MNG.GetSelectedNodeIDs()[0], $('#ReturnToUrl').val()]);
            }
            return false;
        });


        $("#unmanageLink").click(function () {
            if (!$(this).parent().hasClass("Disabled")) {
                if (MNG.allSelected) {
                    //alert('unmanage ALL nodes');
                    MNG.NetObjects.WithAllIDsWithoutPrefix(Operations.UnmanageObjects);
                }
                else {
                    //alert('unmanage selected nodes');
                    //showUnmanageDialog(MNG.GetSelectedObjectIDs());
                    showUnmanageDialog(MNG.GetSelectedNodeIDs());
                }
            }
            return false;
        });

        $("#remanageLink").click(function () {
            if (!$(this).parent().hasClass("Disabled")) {
                if (MNG.allSelected) {
                    //alert('MANAGE ALL nodes');
                    MNG.NetObjects.WithAllIDsWithoutPrefix(Operations.RemanageObjects);
                }
                else {
                    //alert('MANAGE selected nodes');
                    //remanageNetObjects(MNG.GetSelectedNodeIDs(), MNG.GetSelectedInterfaceIDs());
                    remanageNetObjects(MNG.GetSelectedNodeIDs());
                }
            }
            return false;
        });

        // monitor and unmonitor button click
        $("#unmonitorLink").click(function () {
            if (!$(this).parent().hasClass("Disabled")) {
                if (MNG.allSelected) {
                    //alert('unmonitor ALL ports');
                    //MNG.NetObjects.WithAllIDsWithoutPrefix(Operations.UnmonitorObjects);
                    MNG.NetObjects.WithAllIDsAndIsMonitored(Operations.UnmonitorObjects);
                }
                else {
                    //alert('unmonitor selected ports');
                    //showUnmanageDialog(MNG.GetSelectedObjectIDs());
                    //showUnmonitorDialog(MNG.GetSelectedPortIDs(), "Unmonitor");
                    //MNG.changePortMonitoring(MNG.GetSelectedPortIDs(), "unmonitor");
                    MNG.changePortMonitoring(MNG.GetSelectedObjectIDs(), "@{R=UDT.Strings;K=UDTWEBJS_AK1_129;E=js}");
                }
            }
            return false;
        });

        $("#remonitorLink").click(function () {
            if (!$(this).parent().hasClass("Disabled")) {
                if (MNG.allSelected) {
                    //alert('MONITOR ALL ports');
                    //MNG.NetObjects.WithAllIDsWithoutPrefix(Operations.RemonitorObjects);
                    MNG.NetObjects.WithAllIDsAndIsNotMonitored(Operations.RemonitorObjects);
                }
                else {
                    //alert('MONITOR selected ports');
                    //remanageNetObjects(MNG.GetSelectedNodeIDs(), MNG.GetSelectedInterfaceIDs());
                    // remanageNetObjects(MNG.GetSelectedPortIDs());
                    //MNG.changePortMonitoring(MNG.GetSelectedPortIDs(), "monitor");
                    MNG.changePortMonitoring(MNG.GetSelectedObjectIDs(), "@{R=UDT.Strings;K=UDTWEBJS_AK1_128;E=js}");

                    //MNG.GetSelectedObjectIDs(Operations.RemonitorObjects);

                }
            }
            return false;
        });

        deleteDialogUI = function () {
            var delOptions = document.getElementsByName('delOptions');
            //            if (delOptions[0].checked) {
            //                $("#MACAddress").attr('disabled', false);
            //                $("#IPAddress").attr('disabled', true);
            //            } else {
            //                $("#MACAddress").attr('disabled', true);
            //                $("#IPAddress").attr('disabled', false);
            //            }
        }


        $("#deleteLink").click(function () {

            if (!$(this).parent().hasClass("Disabled")) {

                // check if we are deleting Nodes or Ports?
                if (MNG.NetObjects.SelectedPrefix() == "P") {


                    //set the appropriate message
                    var layout = $("#delPortsDialogDescription");
                    var txt; //for layout.text

                    if (MNG.allSelected) {
                        // delete all
                        txt = '@{R=UDT.Strings;K=UDTWEBJS_AK1_70;E=js}';
                    }
                    else {
                        var ids = MNG.GetSelectedObjectIDs();
                        var count = ids.EngineNetObjectIDs;
                        if (count.length > 1) {
                            txt = String.format('@{R=UDT.Strings;K=UDTWEBJS_AK1_71;E=js}', count.length);
                        }
                        else {
                            txt = '@{R=UDT.Strings;K=UDTWEBJS_AK1_72;E=js}';
                        }
                    }

                    layout.html(txt + '<br><br><font style="color:grey">@{R=UDT.Strings;K=UDTWEBJS_AK1_73;E=js}</font>');
                    //create the ports dialog window if it doesn't already exist
                    if (!delPortsDialog) {
                        delPortsDialog = new Ext.Window({
                            applyTo: 'delPortsDialog',
                            layout: 'fit',
                            width: 400,
                            height: 240,
                            closeAction: 'hide',
                            modal: true,
                            plain: true,
                            resizable: false,
                            //ids: ids,
                            items: new Ext.BoxComponent({
                                applyTo: 'delPortsDialogBody',
                                layout: 'fit',
                                border: false
                            }),
                            buttons: [
                        {
                            text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_15;E=js}',
                            handler: function () {

                                if (MNG.allSelected) {
                                    // do for all
                                    var allIDs = MNG.NetObjects.WithAllIDsWithoutPrefix(Operations.DoUdtPortDelete);
                                }
                                else {
                                    Operations.DoUdtPortDelete();
                                }
                                delPortsDialog.hide();
                            }
                        },

                        {
                            text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_6;E=js}',
                            style: 'margin-left: 5px;',
                            handler: function () {
                                confirmDelete = false;
                                delPortsDialog.hide();
                            }
                        }]
                        });
                    }
                    // Set the location
                    delPortsDialog.alignTo(document.body, "c-c");
                    delPortsDialog.show();


                }
                // end of delete ports


                //delete nodes dialog
                else {


                    //set the appropriate message
                    var layout = $("#delDialogDescription");
                    var txt;
                    if (MNG.allSelected) {
                        // do for all
                        layout.html('@{R=UDT.Strings;K=UDTWEBJS_VB1_63;E=js}');
                    }
                    else {
                        var ids = MNG.GetSelectedObjectIDs();
                        var count = ids.EngineNetObjectIDs;
                        if (count.length > 1) {
                            layout.html(String.format('@{R=UDT.Strings;K=UDTWEBJS_VB1_64;E=js}', count.length));
                        }
                        else {
                            layout.html('@{R=UDT.Strings;K=UDTWEBJS_VB1_65;E=js}');
                        }
                    }
                    //create the dialog window if it doesn't already exist
                    if (!delDialog) {
                        delDialog = new Ext.Window({
                            applyTo: 'delDialog',
                            layout: 'fit',
                            width: 400,
                            height: 205,
                            // SJW: SR1.0.1 change closeAction to hide. Destroy causes Show and Group by controls to remain missing if the 'x' (close) button is clicked for the pop-up - EXT bug?
                            //closeAction: 'destroy',
                            closeAction: 'hide',
                            modal: true,
                            plain: true,
                            resizable: false,
                            ids: ids,
                            items: new Ext.BoxComponent({
                                applyTo: 'delDialogBody',
                                layout: 'fit',
                                border: false
                            }),


                            buttons: [
                            {
                                text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_15;E=js}',
                                handler: function () {

                                    var delOptions = document.getElementsByName('delOptions');
                                    if (delOptions[0].checked) {
                                        //alert('UDT');
                                        //delete only from UDT
                                        if (MNG.allSelected) {
                                            // do for all
                                            var allIDs = MNG.NetObjects.WithAllIDsWithoutPrefix(Operations.DoUdtNodeDelete);
                                        }
                                        else {
                                            Operations.DoUdtNodeDelete();
                                        }
                                    }
                                    else {
                                        //alert('ALL');
                                        //delete from all modules
                                        if (MNG.allSelected) {
                                            // do for all
                                            var allIDs = MNG.NetObjects.WithAllIDsWithEngineIds(Operations.DoDelete);
                                        }
                                        else {
                                            // do only for selected items or all on page                    
                                            var ids = MNG.GetSelectedObjectIDs();
                                            Operations.DoDelete(ids.EngineNetObjectIDs);
                                        }
                                    }



                                    delDialog.hide();
                                }
                            },
                        {
                            text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_6;E=js}',
                            style: 'margin-left: 5px;',
                            handler: function () {
                                confirmDelete = false;
                                delDialog.hide();
                            }
                        }]
                        });
                    }
                    // Set the location
                    delDialog.alignTo(document.body, "c-c");
                    delDialog.show();
                }
                // end of delete nodes dialog
            }
            return false;
        });

        $("#pollNowLink").click(function () {
            if (!$(this).parent().hasClass("Disabled")) {

                if (MNG.allSelected) {
                    //alert('Poll ALL UDT now');
                    MNG.NetObjects.WithAllIDsWithoutPrefix(Operations.DoPollNow);
                    //MNG.NetObjects.WithAllIDsWithEngineIds(Operations.DoPollNow);
                }
                else {
                    //alert('Poll selected UDT now');
                    Operations.DoPollNow(MNG.GetSelectedNodeIDs());
                }
            }
            return false;
        });
        $("#rediscoverLink").click(function () {
            if (!$(this).parent().hasClass("Disabled")) {
                if (MNG.allSelected) {
                    MNG.NetObjects.WithAllIDsWithEngineIds(Operations.DoRediscoverNow);
                }
                else {
                    Operations.DoRediscoverNow(MNG.GetSelectedObjectIDs().EngineNetObjectIDs);
                }
            }
            return false;
        });
        $("#shutdownLink").click(function () {
            if (!$(this).parent().hasClass("Disabled")) {
                if (MNG.allSelected) {
                    MNG.NetObjects.WithAllIDsAndNames(Operations.ShutdownInterface);
                }
                else {
                    Operations.ShutdownInterface(MNG.GetSelectedPortsGroupByNode());
                }
            }
            return false;
        });
        $("#enableLink").click(function() {
            if (!$(this).parent().hasClass("Disabled")) {
                if (MNG.allSelected) {
                    MNG.NetObjects.WithAllIDsAndNames(Operations.EnableInterface);
                }
                else {
                    Operations.EnableInterface(MNG.GetSelectedPortsGroupByNode());
                }
            }
            return false;
        });

        $("#monitornodebyUDTLink").click(function () {
            if (!$(this).parent().hasClass("Disabled")) {
                if (MNG.allSelected) {
                    MNG.NetObjects.WithAllIDsUnMonitoredNodes(Operations.MonitoryNodesByUDT);
                }
                else {
                    Operations.MonitoryNodesByUDT(MNG.GetSelectedNodeIDs());
                }
            }
            return false;
        });

        $("#udtAddNodeToUDTBannerClsBtn").click(function () {
            $("#bannerImportNodesToUDT").hide();
        });

       

        //        $("#shutdownLink").click(function () {
        //            if (!$(this).parent().hasClass("Disabled")) {
        //                if (MNG.allSelected) {
        //                    MNG.NetObjects.WithAllIDsAndNames(Operations.ShutdownInterface);
        //                }
        //                else {
        //                    Operations.ShutdownInterface(MNG.GetSelectedObjectIDs().I);
        //                }
        //            }
        //            return false;
        //        });

        //        $("#enableLink").click(function () {
        //            if (!$(this).parent().hasClass("Disabled")) {
        //                if (MNG.allSelected) {
        //                    MNG.NetObjects.WithAllIDsAndNames(Operations.EnableInterface);
        //                }
        //                else {
        //                    Operations.EnableInterface(MNG.GetSelectedObjectIDs().I);
        //                }
        //            }
        //            return false;
        //        });
    }
    MNG.EnableDisableMenu();

    $("#pageNumber").keypress(Pager.PageNumberChanging);
    $("#pageNumberTop").keypress(Pager.PageNumberChanging);
    $("#pageSize").keypress(Pager.PageSizeChanging);
    $("#pageSizeTop").keypress(Pager.PageSizeChanging);
    $(window).unload(function () { MNG.Prefs.save("UDTActualPage", Pager.actualPage); });

});
MNG.GetSelectedPortsGroupByNode = function () {

    var selectedNodes = {};


    $('[name*=":"]:checked').each(function () {
        var obj = $(this).data('netobject');

        if (selectedNodes[obj.NodeID] == undefined)
            selectedNodes[obj.NodeID] = [];
        selectedNodes[obj.NodeID].push(obj.PortID);
    });
    // alert(ids[1]);
    return selectedNodes;
};
