﻿Ext.namespace('SW');
Ext.namespace('SW.UDT');
Ext.ns('Ext.ux.form');

var filterText = ''; //global search value
var rawText = '';


SW.UDT.SearchField = Ext.extend(Ext.form.TwinTriggerField, {
    initComponent: function () {
        SW.UDT.SearchField.superclass.initComponent.call(this);
        this.triggerConfig = {
            tag: 'span', cls: 'x-form-twin-triggers', cn: [
            { tag: "img", style: 'height:18px !important; width:18px !important;', src: "/Orion/Admin/Accounts/images/search/clear_button.gif", cls: "x-form-trigger x-form-clear-trigger" },
            { tag: "img", style: 'height:18px !important; width:18px !important;', src: "/Orion/Admin/Accounts/images/search/search_button.gif", cls: "x-form-trigger x-form-search-trigger" }
        ]
        };

        this.on('specialkey', function (f, e) {
            if (e.getKey() == e.ENTER) {
                this.onTrigger2Click();
            }
        }, this);

        // set search field text styles
        this.on('focus', function () {
            this.el.applyStyles('font-style:normal;');
        }, this);

        // reset search field text style
        this.on('blur', function () {
            if (Ext.isEmpty(this.el.dom.value) || this.el.dom.value == this.emptyText) {
                this.el.applyStyles('font-style:italic;');
            }
        }, this);
    },


    id: 'searchPortList',
    validationEvent: false,
    validateOnBlur: false,

   

    // default search field text style
    style: 'height:15px; font-style:italic; font-size:11px;',
    emptyText: '@{R=UDT.Strings;K=UDTWEBJS_AK1_37;E=js}',

    hideTrigger1: true,
    //width: 180,
    hasSearch: false,

    // this is the clear (X) icon button in the search field
    onTrigger1Click: function () {
        if (this.hasSearch) {
            this.el.dom.value = '';
            filterText = this.getRawValue();
            //            this.store.proxy.conn.jsonData = { searchText: '' };
            //            this.store.load();

            this.triggers[0].hide();
            this.hasSearch = false;
            MNG.startSearch();
        }
    },

    //  this is the search icon button in the search field
    onTrigger2Click: function () {

        //set the global variable for the SQL query and regex highlighting (render)
        //rawText = this.getRawValue().toLowerCase();
        filterText = this.getRawValue().toLowerCase();

        if (filterText.length < 1) {
            this.onTrigger1Click();
            return;
        }

//        if (filterText != "") {

//            //replace all single quote ' instances to a double single quote instance.  e.g. test's becomes test''s
//            filterText = filterText.replace(/\'/g, "''");

//            //append escape character if % literal found e.g. test% becomes test[%]
//            filterText = filterText.replace(/\%/g, "[%]");

//            //append escape character if % literal found e.g. test% becomes test[%]
//            filterText = filterText.replace(/\_/g, "[_]");

//            //ToDo: replace wildcard * with SQL % in order to enforce explicit usage of wildcard searching
//            filterText = filterText.replace(/\*/g, "%");
//            //filterText = filterText.replace(/\?/g, "_"); // single character replace?

//            //if looks like part of mac address then strip formatting before search
//            if ((filterText.indexOf(":") != -1) || (filterText.indexOf(".") != -1) || (filterText.indexOf("-") != -1)) {

//                //first, strip out any * wildcard chars
//                var tmpSearchText = filterText.replace(/\%/g, "");
//                var ciscoDot = /^([0-9a-f]{0,4})(([.-]|$)([0-9a-f]{0,4}$|(([0-9a-f]{4}[.-]$)|(([0-9a-f]{4}[.-][0-9a-f]{0,4}$)))))/i; //this works OK!
//                var unix = /^([0-9a-f]{0,2})(([:-]|$)([0-9a-f]{0,2}$|(([0-9a-f]{2}[:-]$)|(([0-9a-f]{2}[:-][0-9a-f]{0,2}$)|((([0-9a-f]{2}[:-]){2}[0-9a-f]{0,2}$)|((([0-9a-f]{2}[:-]){3}[0-9a-f]{0,2}$)|((([0-9a-f]{2}[:-]){4}[0-9a-f]{0,2}$))))))))/i;
//                var windows = /^([0-9a-f]{0,2})(([--]|$)([0-9a-f]{0,2}$|(([0-9a-f]{2}[--]$)|(([0-9a-f]{2}[--][0-9a-f]{0,2}$)|((([0-9a-f]{2}[--]){2}[0-9a-f]{0,2}$)|((([0-9a-f]{2}[--]){3}[0-9a-f]{0,2}$)|((([0-9a-f]{2}[--]){4}[0-9a-f]{0,2}$))))))))/i;

//                if (ciscoDot.test(tmpSearchText)) filterText = filterText.replace(/\./g, "");
//                if (unix.test(tmpSearchText)) filterText = filterText.replace(/\:/g, "");
//                if (windows.test(tmpSearchText)) filterText = filterText.replace(/\-/g, "");
//            }
//        }

        // provide search string for param when loading the page
        //        this.store.proxy.conn.jsonData = { searchText: filterText };
        //        this.store.load();
        this.hasSearch = true;
        this.triggers[0].show();
        MNG.startSearch();
    }
});