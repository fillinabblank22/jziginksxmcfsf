<%--<%@ Page Language="C#" MasterPageFile="~/Orion/Nodes/NodeManagementPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_UDT_Ports_Default" Title="Port Management"  %>--%>
<%@ Page Language="C#" MasterPageFile="~/Orion/UDT/Ports/PortManagementPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_UDT_Ports_Default" Title="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_3 %>"  %>

<%@ Import Namespace="SolarWinds.Orion.Core.Common"%>
<%@ Register TagPrefix="udt" TagName="PageTitleBar" Src="~/Orion/UDT/Controls/PageTitleBar.ascx" %>
<%@ Register TagPrefix="orion" TagName="UnmanageDialog" Src="./Controls/UdtUnmanageDialog.ascx" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">

<orion:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false" Version="3.4"/>

<orion:Include runat="server" File="js/jquery/timePicker.css" />
<orion:Include runat="server" File="UnmanageDialog.css" />
<orion:Include runat="server" File="NodeMNGMenu.css" />
<orion:Include runat="server" File="jquery/jquery.timePicker.js" />
<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="extjs/Ext.sw.grid.RadioSelectionModel.js" />
<orion:Include runat="server" File="Nodes/js/ChangeEngineDialog.js" />
<orion:Include runat="server" File="jquery/jquery.bgiframe.min.js" Section="Bottom" />
<orion:Include runat="server" Module="UDT" File="./Ports/js/PortManagementPaging.js" />
<orion:Include runat="server" Module="UDT" File="./Ports/js/SearchField.js" />

<link rel="stylesheet" type="text/css" href="../../../Orion/UDT/UDT.css" />

<%--used for the delete nodes confirmation dialog--%>
<style type="text/css">
        #adminContent .dialogBody table td { padding-top: 5px; padding-left: 10px; }
        #delDialog .dialogBody table td { padding-right: 10px; }
        #delPortsDialog .dialogBody table td { padding-right: 10px; }
        #monitoringDialog .dialogBody table td { padding-right: 10px; }
        #overLicenseLimitDialog .dialogBody table td { padding-right: 10px; }
        #rwCommunityValidationDialog .dialogBody table td { padding-right: 10px; }
        #addDialog td.x-btn-center, #delDialog td.x-btn-center, #updateDialog td.x-btn-center { text-align: center !important; }
        #addDialog td, #delDialog td, #delPortsDialog td, #updateDialog td { padding-right: 0; padding-bottom: 0; }
        #adminContent .dialogBody ul { margin-left: 20px; list-style-type: square; }
        #adminContent .dialogBody ul { margin-left: 20px; list-style-type: square; }

        .ImportNodeNotification
{
	width: 50%;
	/*background: white url(../Orion/images/NotificationImages/notification_background_gradient.gif) 50% top repeat-x;*/
	background-image: url(/Orion/images/NotificationImages/notification_background_gradient.gif);
	background-repeat: repeat-x;
	vertical-align: middle;
	background-color: #ffdb7c;
    margin-left:250px;
}
        
</style>
</asp:Content>

<asp:Content id="PortManagmentTitle" ContentPlaceHolderID="TopRightPageLinks" runat="Server">
    <udt:PageTitleBar runat="server" id="pageTitleBar" ShowTitle="False"/>
</asp:Content>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="ViewPageTitle">
    <h1><%= Resources.UDTWebContent.UDTWEBDATA_AK1_3%></h1>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
<%--	<asp:ScriptManagerProxy ID="ScriptManager2" runat="server" >
		<Services>
			<asp:ServiceReference path="~/Orion/UDT/Services/Information.asmx" />
			<asp:ServiceReference path="~/Orion/UDT/Services/NodeManagement.asmx" />
		</Services>
	</asp:ScriptManagerProxy>--%>
    <asp:ScriptManagerProxy ID="ScriptManager1" runat="server" >
		<Services>
			<asp:ServiceReference path="~/Orion/Services/Information.asmx" />
			<asp:ServiceReference path="~/Orion/Services/NodeManagement.asmx" />
            <asp:ServiceReference path="~/Orion/UDT/Services/UdtPortManagement.asmx" />
		</Services>
	</asp:ScriptManagerProxy>

    

	<%foreach (string setting in new string[] { "UDTGroupBy", "UDTGroupByValue", "UDTObjectType", "UDTNodeSortProp", "UDTNodeSortPropType", "UDTNodeSortAsc",
			"UDTPortSortProp", "UDTPortSortPropType", "UDTPortSortAsc", "UDTNodeColumns", "UDTPortColumns", "UDTPageSize", "UDTActualPage","UDTTotalCount", "UDTObjectTypeFilter"
			 }) { %>
		<%--<input type="hidden" name="WebNodeManagement_<%=setting%>" id="WebNodeManagement_<%=setting%>" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("WebNodeManagement_" + setting)%>' />--%>
        <input type="hidden" name="WebUDTPortManagement_<%=setting%>" id="WebUDTPortManagement_<%=setting%>" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("WebUDTPortManagement_" + setting)%>' />
	<%}%>
	<input type="hidden" id="PortCustomProperties" value='<%=string.Join(",", GetCustomProperties("UDTPorts"))%>' />
	<input type="hidden" id="NodeCustomProperties" value='<%=string.Join(",", GetCustomProperties("UDTNodes"))%>' />
	<input type="hidden" id="ReturnToUrl" value="<%= ReturnUrl %>" />
    <input type="hidden" id="txtMaxPorts" value="<%= GetMonitoredPortCount("max") %>" />
    <input type="hidden" id="txtCurrentPorts" value="<%= GetMonitoredPortCount("current") %>" />

    <div id="bannerImportNodesToUDT" style="display:none">
    <table class="ImportNodeNotification" id="availableUpdates">
        <tbody>
            <tr>
                <td class="AvailableUpdatesIcon" style="width: 1px;" >
                    <img alt="" class="NotifImage" src="/Orion/UDT/Images/Admin/Discovery/icon_lightbulb.gif">

                </td>
                <td id="availableUpdatesContainer" style="padding-left:10px;" >	 
                 <div class="NotificationMess">
                
            <span><b><%= Resources.UDTWebContent.UDTWEBDATA_PG_2 %></b> <br /><%= Resources.UDTWebContent.UDTWEBDATA_PG_3 %></span>&nbsp;
                
               
                      </div>               

                </td>
                <td>
                    <div id="udtAddNodeToUDTBannerClsBtn" style="text-align:right; vertical-align:top; height:30px;">
                    <img  src="/Orion/images/NotificationImages/notification_close_button.gif" style="cursor: pointer; " alt="Close the information">
                        </div>
                </td>
               </tr>

        </tbody>
    </table>
    <br />
     </div>
    
	<table width="100%" id="breadcrumb" cellpadding="0" cellspacing="0" style="margin-top: 0">







		<tr>
<%--			<td style="width:250px;"><a href="../Admin">Admin</a> &gt;
				<h1><%=Page.Title%></h1>
			</td>--%>
            <td style="width:250px;"></td>
			<td valign="bottom">
				<label for="showObjectType"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_99 %></label>
				<select id="showObjectType">
					<option value="Nodes" ><%= Resources.UDTWebContent.UDTWEBDATA_AK1_100 %></option>
					<option value="Ports" selected="selected"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_101 %></option>
				</select>
                &nbsp;<label id="ShowUDTMonitorNodeStatlbl" for="showObjectType"><%= Resources.UDTWebContent.UDTWEBDATA_GK_9 %></label>
                <select id="ShowUDTMonitorNodeStat">
					<option value="Monitored_Nodes" selected="selected"><%= Resources.UDTWebContent.UDTWEBDATA_GK_10 %></option>
					<option value="Unmonitored_Nodes" ><%= Resources.UDTWebContent.UDTWEBDATA_GK_11 %></option>
				</select>
			</td>
            
		</tr>
	</table>
	
	<table class="NodeManagement">
		<tr valign="top" align="left">
			<td style="border-right: #e1e1e0 1px solid; border-left: 0; border-top: 0; border-bottom: 0; background-color: white;">
				<div class="NodeGrouping" style="margin-left: 10;">
					<div class="GroupSection" style="margin-left: 10;">
						<div style="width: 180px"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_102 %></div>
						<select id="groupByProperty" style="width:100%">
							<option value=""><%= Resources.UDTWebContent.UDTWEBDATA_AK1_103 %></option>
							<option value="Vendor"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_104 %></option>
							<option value="MachineType"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_105 %></option>
							<option value="SNMPVersion"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_106 %></option>
							<option value="Status"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_107 %></option>
							<option value="Location"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_108 %></option>
							<option value="Contact"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_109 %></option>
							<option value="Community"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_110 %></option>
							<option value="RWCommunity"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_111 %></option>
							<%foreach (string prop in CustomPropertyMgr.GetPropNamesForTable("Nodes", false)) { %>
								<option value="CustomProperties.[<%=prop%>]" propertyType="<%=CustomPropertyMgr.GetTypeForProp("Nodes", prop)%>"><%=prop%></option>
							<%}%>
						</select>
					</div>
					<ul class="NodeGroupItems"></ul>
				</div>
			</td>
			<td>
				<table cellpadding="0" cellspacing="0" style="width:100%;">
		            <tr valign="top" align="left">
			            <td style="border:0 0 0 0; padding:0 0 0 0; background-color: white;">
                            
				
						
				
                            <div class="NodeManagementMenuWrapper  ui-helper-clearfix">
					            <ul class="NodeManagementMenu">
						            <li><a id="addLink" href="/Orion/Nodes/Add/Default.aspx"><img src="images/icons/icon_add.gif" alt="" /> <%= Resources.UDTWebContent.UDTWEBDATA_AK1_112 %></a></li>
						            <li><a id="editLink" href="#"><img src="images/icons/icon_edit.gif" alt="" /> <%= Resources.UDTWebContent.UDTWEBDATA_AK1_113 %></a></li>
						            <%--<li class="nodeCommand"><a id="listResourcesLink" href="#"><img src="images/icons/icon_list.gif" alt="" /> List Ports</a></li>
						            --%>
                                    <li class="nodeCommand"><a id="unmanageLink" href="#"><img src="images/icons/icon_manage.gif" alt="" /> <%= Resources.UDTWebContent.UDTWEBDATA_AK1_114 %></a></li>
						            <li class="nodeCommand"><a id="remanageLink" href="#"><img src="images/icons/icon_remanage.gif" alt="" /> <%= Resources.UDTWebContent.UDTWEBDATA_AK1_115 %></a></li>
						            <li class="portCommand"><a id="unmonitorLink" href="#"><img src="images/icons/disable_monitoring.gif" alt="" /> <%= Resources.UDTWebContent.UDTWEBDATA_AK1_116 %></a></li>
						            <li class="portCommand"><a id="remonitorLink" href="#"><img src="images/icons/enable_monitoring.gif" alt="" /> <%= Resources.UDTWebContent.UDTWEBDATA_AK1_117 %></a></li>
                                    <% if (Profile.AllowNodeManagement)
			                        { %>
									<li class="portCommand"><a id="shutdownLink" href="#"><img src="images/icons/icon_shutdown.gif" alt="" /> <%=Resources.UDTWebContent.UDTWEBDATA_DF1_4 %></a></li> <%--href="#"><%= Resources.CoreWebContent.WEBDATA_TM0_256 %></a></li>--%>
								    <li class="portCommand"><a id="enableLink" href="#"><img src="images/icons/icon_enable.gif" alt="" /> <%=Resources.UDTWebContent.UDTWEBDATA_DF1_5 %></a></li><%--<%= Resources.CoreWebContent.WEBDATA_TM0_257 %></a></li>--%>
                                    <% } %>
                                    <%--<li style='display: <%= (_allowInterfaces)? "block": "none" %>'><a id="pollersLink" href="#"><img src="images/icons/icon_assignpoller.gif" alt="" /> Assign Pollers</a></li>--%>
            <%--						<li class="submenu"><span>More Actions</span>
							            <ul>
								            <li><a id="pollNowLink" href="#">Poll Now</a></li>
								            <li><a id="rediscoverLink" href="#">Rediscover</a></li>
							            </ul>
						            </li>--%>
                                    <li class="nodeCommand"><a id="pollNowLink" href="#"><img src="images/icons/pollnow_16x16.gif" alt="" /> <%= Resources.UDTWebContent.UDTWEBDATA_AK1_118 %></a></li>
                                    <li class="nodeCommand"><a id="monitornodebyUDTLink" href="#"><img src="images/icons/Monitor_Nodes_UDT_icon16x16_blue.png" alt="" /> <%= Resources.UDTWebContent.UDTWEBDATA_PG_1 %></a></li>
						            <li><a id="deleteLink" href="#"><img src="images/icons/icon_delete.gif" alt="" /> <%= Resources.UDTWebContent.UDTWEBDATA_AK1_119 %></a></li>
                                    <li><div id="Div1"></div></li>
					            
                                </ul>
                                
				            </div>

                        </td>
                    </tr>
                </table>
                
			    <table cellpadding="0" cellspacing="0" style="width:100%; border:0 0 0 0; margin:10 10 10 10;">
		            <tr valign="top" align="left">
			            <td style="border:0 0 0 0; padding:0 0 0 0; background-color: white;">
                            <div id="selectAllPan" ></div>	
                        </td>
                    </tr>
                </table>
				
                <%--toolbar based on ext-js 2.2 styles--%>
                <div class="x-toolbar x-small-editor" id="extPagerTop" style="height:24px;">
                    <table cellspacing="0">
                        <tbody>
                            <tr>
                                <td>
                                    <table cellspacing="0" cellpadding="0" border="0" class="x-btn-wrap x-btn x-btn-icon x-item-disabled"
                                        id="firstPageTableTop" style="width: auto;">
                                        <tbody>
                                            <tr>
                                                <td class="x-btn-left">
                                                    <i>&nbsp;</i>
                                                </td>
                                                <td class="x-btn-center">
                                                    <em unselectable="on">
                                                        <button type="button" class="x-btn-text x-tbar-page-first" id="firstPageTop">
                                                            &nbsp;</button></em>
                                                </td>
                                                <td class="x-btn-right">
                                                    <i>&nbsp;</i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <table cellspacing="0" cellpadding="0" border="0" class="x-btn-wrap x-btn x-btn-icon x-item-disabled"
                                        id="previousPageTableTop" style="width: auto;">
                                        <tbody>
                                            <tr>
                                                <td class="x-btn-left">
                                                    <i>&nbsp;</i>
                                                </td>
                                                <td class="x-btn-center">
                                                    <em unselectable="on">
                                                        <button type="button" class="x-btn-text x-tbar-page-prev" id="previousPageTop">
                                                            &nbsp;</button></em>
                                                </td>
                                                <td class="x-btn-right">
                                                    <i>&nbsp;</i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <span class="ytb-sep" id="Span5"></span>
                                </td>
                                <td>
                                    <span class="ytb-text" id="Span6"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_120 %></span>
                                </td>
                                <td>
                                    <input type="text" class="sw-tbar-page-number x-tbar-page-number" value="1" size="3" id="pageNumberTop"/>
                                </td>
                                <td>
                                    <span class="ytb-text" id="totalPageCountTop"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_121 %> </span>
                                </td>
                                <td>
                                    <span class="ytb-sep"></span>
                                </td>
                                <td>
                                    <table cellspacing="0" cellpadding="0" border="0" class="x-btn-wrap x-btn x-btn-icon "
                                        id="nextPageTableTop" style="width: auto;">
                                        <tbody>
                                            <tr>
                                                <td class="x-btn-left">
                                                    <i>&nbsp;</i>
                                                </td>
                                                <td class="x-btn-center">
                                                    <em unselectable="on">
                                                        <button type="button" class="x-btn-text x-tbar-page-next" id="nextPageTop">
                                                            &nbsp;</button></em>
                                                </td>
                                                <td class="x-btn-right">
                                                    <i>&nbsp;</i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <table cellspacing="0" cellpadding="0" border="0" class="x-btn-wrap x-btn x-btn-icon "
                                        id="lastPageTableTop" style="width: auto;">
                                        <tbody>
                                            <tr>
                                                <td class="x-btn-left">
                                                    <i>&nbsp;</i>
                                                </td>
                                                <td class="x-btn-center">
                                                    <em unselectable="on">
                                                        <button type="button" class="x-btn-text x-tbar-page-last" id="lastPageTop">
                                                            &nbsp;</button></em>
                                                </td>
                                                <td class="x-btn-right">
                                                    <i>&nbsp;</i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <span class="ytb-sep"></span>
                                </td>
                                <td>
                                    <span class="ytb-text" id="paegSizeTitleTop"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_122 %></span>
                                </td>
                                <td>
                                    <input type="text" class="x-tbar-page-number sw-tbar-page-number" value="1" size="3" id="pageSizeTop"/>
                                </td>                                                               
                            </tr>
                        </tbody>
                    </table>
                    <div class="x-paging-info" id="displayInfoTop" style="padding:2px 2px 0px 2px;color:Black;">
                        </div>
                </div>
                <table id="NodeTree2" cellpadding="0" cellspacing="0" class="NeedsZebraStripes">
					<thead>
						<tr>
						</tr>
					</thead>
					<tbody></tbody>					
				</table>				
							    
                <%-- bottom pager based on extJs v 2.2 --%>
                <div class="x-toolbar x-small-editor" id="extPager" style="height: 24px;">
                    <table cellspacing="0">
                        <tbody>
                            <tr>
                                <td id="Td1">
                                    <table cellspacing="0" cellpadding="0" border="0" class="x-btn-wrap x-btn x-btn-icon x-item-disabled"
                                        id="firstPageTable" style="width: auto;">
                                        <tbody>
                                            <tr>
                                                <td class="x-btn-left">
                                                    <i>&nbsp;</i>
                                                </td>
                                                <td class="x-btn-center">
                                                    <em unselectable="on">
                                                        <button type="button" class="x-btn-text x-tbar-page-first" id="firstPage">
                                                            &nbsp;</button></em>
                                                </td>
                                                <td class="x-btn-right">
                                                    <i>&nbsp;</i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td id="Td2">
                                    <table cellspacing="0" cellpadding="0" border="0" class="x-btn-wrap x-btn x-btn-icon x-item-disabled"
                                        id="previousPageTable" style="width: auto;">
                                        <tbody>
                                            <tr>
                                                <td class="x-btn-left">
                                                    <i>&nbsp;</i>
                                                </td>
                                                <td class="x-btn-center">
                                                    <em unselectable="on">
                                                        <button type="button" class="x-btn-text x-tbar-page-prev" id="previousPage">
                                                            &nbsp;</button></em>
                                                </td>
                                                <td class="x-btn-right">
                                                    <i>&nbsp;</i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <span class="ytb-sep" id="Span1"></span>
                                </td>
                                <td>
                                    <span class="ytb-text" id="Span2"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_120 %></span>
                                </td>
                                <td>
                                    <input type="text" class="sw-tbar-page-number x-tbar-page-number" value="1" size="3"
                                        id="pageNumber" />
                                </td>
                                <td>
                                    <span class="ytb-text" id="totalPageCount"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_121 %> </span>
                                </td>
                                <td>
                                    <span class="ytb-sep" id="Span3"></span>
                                </td>
                                <td id="Td3">
                                    <table cellspacing="0" cellpadding="0" border="0" class="x-btn-wrap x-btn x-btn-icon "
                                        id="nextPageTable" style="width: auto;">
                                        <tbody>
                                            <tr>
                                                <td class="x-btn-left">
                                                    <i>&nbsp;</i>
                                                </td>
                                                <td class="x-btn-center">
                                                    <em unselectable="on">
                                                        <button type="button" class="x-btn-text x-tbar-page-next" id="nextPage">
                                                            &nbsp;</button></em>
                                                </td>
                                                <td class="x-btn-right">
                                                    <i>&nbsp;</i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td id="Td4">
                                    <table cellspacing="0" cellpadding="0" border="0" class="x-btn-wrap x-btn x-btn-icon "
                                        id="lastPageTable" style="width: auto;">
                                        <tbody>
                                            <tr>
                                                <td class="x-btn-left">
                                                    <i>&nbsp;</i>
                                                </td>
                                                <td class="x-btn-center">
                                                    <em unselectable="on">
                                                        <button type="button" class="x-btn-text x-tbar-page-last" id="lastPage">
                                                            &nbsp;</button></em>
                                                </td>
                                                <td class="x-btn-right">
                                                    <i>&nbsp;</i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <span class="ytb-sep" id="Span4"></span>
                                </td>
                                <td>
                                    <span class="ytb-text" id="paegSizeTitle"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_122 %></span>
                                </td>
                                <td>
                                    <input type="text" class="x-tbar-page-number sw-tbar-page-number" value="1" size="3"
                                        id="pageSize" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="x-paging-info" id="displayInfo" style="padding: 2px 2px 0px 2px; color: Black;">
                    </div>
                </div>
                <div id="testoutput">
                </div>
            </td>
        </tr>
    </table>
    <div id="lastErrorMessage"></div>	

	<orion:UnmanageDialog ID="UnmanageDialog3" runat="server" />


	<div id='columnChooserDialog' style="display:none;" title="<%= Resources.UDTWebContent.UDTWEBDATA_AK1_123 %>">
		<div id='columnChooserDialogContents'>
			<ul id='availableColumnList'></ul>
            
			<div class="sw-btn-bar-wizard">
			    <orion:LocalizableButton runat="server" id="columnChooserOK" LocalizedText="Ok" DisplayType="Primary" OnClientClick="columnChooserOKClick(); return false;" />
                <orion:LocalizableButton runat="server" id="columnChooserCancel" LocalizedText="Cancel" DisplayType="Secondary" OnClientClick="columnChooserCancelClick(); return false;"/>
			</div>
		</div>
	</div>
    
   <%--used for setting monitoring on or off for selected ports--%>
<div id="overLicenseLimitDialog" class="x-hidden">
    <div class="x-window-header" id="overLicenseLimitDialogHeader"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_124 %></div>
	    <div id="overLicenseLimitDialogBody" class="x-panel-body dialogBody">
	    <table>
	        <tr>
	            <td><div id="overLicenseLimitDialogDescription"></div></td>
	        </tr>
	        <tr>
	            <td><ul id="Ul4"></ul></td>
	        </tr>
        </table>
	    </div>
	</div>
    <div id="rwCommunityValidationDialog" class="x-hidden">
    <div class="x-window-header" id="Div3"><%=Resources.UDTWebContent.UDTWEBDATA_DF1_6 %></div>
	    <div id="rwCommunityValidationDialogBody" class="x-panel-body dialogBody">
	    <table>
	        <tr>
	            <td><div id="rwCommunityDialogDescription"></div></td>
	        </tr>
	        <tr>
	            <td><ul id="Ul1"></ul></td>
	        </tr>
        </table>
	    </div>
	</div>

<%--used for setting monitoring on or off for selected ports--%>
<div id="monitoringDialog" class="x-hidden">
    <div class="x-window-header" id="monitoringDialogHeader"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_125 %></div>
	    <div id="monitoringDialogBody" class="x-panel-body dialogBody">
	    <table>
	        <tr>
	            <td><div id="monitoringDialogDescription"></div></td>
	        </tr>
	        <tr>
	            <td><ul id="Ul3"></ul></td>
	        </tr>
        </table>
	    </div>
	</div>

<%--used for the poll net objects info box--%>
<div id="pollDialog" class="x-hidden">
	    <div class="x-window-header" id="pollDialogHeader"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_126 %></div>
	    <div id="pollDialogBody" class="x-panel-body dialogBody">
	    <table>
	        <tr>
	            <td><div id="pollDialogDescription"></div></td>
	        </tr>
	        <tr>
	            <td><ul id="Ul2"></ul></td>
	        </tr>
        </table>
	    </div>
	</div>
    
<%--used for the delete nodes confirmation dialog--%>
<div id="delDialog" class="x-hidden">
	    <div class="x-window-header"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_127 %></div>
	    <div id="delDialogBody" class="x-panel-body dialogBody">
	    <table>
	        <tr>
	            <td><div id="delDialogDescription"></div></td>
	        </tr>

            <tr>
	            <td style="padding-bottom:2px; padding-top:10px;"><input type="radio" id="radDeleteUDT" name="delOptions" checked="checked" onclick="deleteDialogUI()" value="0" title="<%= Resources.UDTWebContent.UDTWEBDATA_AK1_128 %>" /> <%= Resources.UDTWebContent.UDTWEBDATA_AK1_129 %></td>
            </tr>
            <tr>
	            <td style="padding-bottom:2px;"><input type="radio" id="radDeleteAll" name="delOptions" onclick="deleteDialogUI()" value="1" title="<%= Resources.UDTWebContent.UDTWEBDATA_AK1_130 %>" /> <%= Resources.UDTWebContent.UDTWEBDATA_AK1_131 %></td>
            </tr>
<%--	        <tr>
	            <td><ul id="accounts"></ul></td>
	        </tr>--%>
        </table>
	    </div>
	</div>
<%--        <div id="Div1" class="x-hidden">
	    <div class="x-window-header">Delete Account</div>
	    <div id="Div2" class="x-panel-body dialogBody">
	    <table>
	        <tr>
	            <td><div id="Div3"></div></td>
	        </tr>
	        <tr>
	            <td><ul id="Ul1"></ul></td>
	        </tr>
        </table>
	    </div>
	</div>     --%>  

<%--used for the delete ports confirmation dialog--%>
<div id="delPortsDialog" class="x-hidden">
	    <div class="x-window-header"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_132 %></div>
	    <div id="delPortsDialogBody" class="x-panel-body dialogBody">
	    <table>
	        <tr>
	            <td><div id="delPortsDialogDescription"></div></td>
	        </tr>
<%--	        <tr>
	            <td><ul id="Ul5"></ul></td>
	        </tr>--%>
        </table>
	    </div>
	</div>
<%--        <div id="Div7" class="x-hidden">
	    <div class="x-window-header">Delete Account</div>
	    <div id="Div8" class="x-panel-body dialogBody">
	    <table>
	        <tr>
	            <td><div id="Div9"></div></td>
	        </tr>
	        <tr>
	            <td><ul id="Ul6"></ul></td>
	        </tr>
        </table>
	    </div>
	</div>--%>   
	<%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer || !Profile.AllowNodeManagement) {%>
		<div id="isDemoMode" style="display:none;"></div>
	<%}%>

<%--	<div id="originalQuery"></div>	
	<div id="test"></div>
	<pre id="stackTrace"></pre>

	<script type="text/javascript" src="../js/jquery/jquery.bgiframe.min.js"></script>--%>

    <script type="text/javascript">
        Ext.onReady(function () {
            var searchField = new SW.UDT.SearchField({
                width: 180,
                style: 'height:15px; font-style:italic; font-size:11px;'
            });
            searchField.render(document.getElementById("Div1"));
            
            //            applyTo: 'searchbox-panel',
            //            //height: 30,
            //            autoScroll: false,
            //            autoHeight: true,

            //            items:
            //                new Ext.ux.form.SearchBox({
            //                    width: 200
            //                })
            //        });
        });
   </script>


</asp:Content>
