<%@ Control Language="C#" ClassName="UdtUnmanageDialog" %>
<%@ Register Src="~/Orion/Controls/DateTimePicker.ascx" TagPrefix="orion" TagName="DateTimePicker" %>

<style type="text/css">
    .ui-dialog-titlebar{ padding-right:30px;}
</style>
<script type="text/javascript">
//<![CDATA[
    var showUnmanageDialog, remanageNodes, remanageNetObjects;

    var resizeValue = 0;
//    TextResizeDetector.TARGET_ELEMENT_ID = 'unmanageDialog3';
//    TextResizeDetector.USER_INIT_FUNC = init;
    var dialog, width, height;
    var minWidht = 490, minHeight = 165;

    $(function () {
        showUnmanageDialog = function (netObjectIds, nodeCaption) {
            //alert('netObjects.N: ' + netObjectIds + ' / nodeCaption: ' + nodeCaption);
            if (netObjectIds.N == null) {
                var temp = netObjectIds;
                netObjectIds = { N: [] };
                //                if (isInterfaces)
                //                    netObjectIds.I = temp;
                //                else
                netObjectIds.N = temp;
            }

            if (nodeCaption == null) {
                nodeCaption = "";
            }
            else {
                nodeCaption = nodeCaption.replace("&apos;", "'");
            }
            $("#<%=unmanageOk.ClientID%>").unbind().click(function () {
                var startDate = $('#<%=unmanageStart.ClientID%>').orionGetDate();
                var endDate = $('#<%=unmanageEnd.ClientID%>').orionGetDate();
                if (netObjectIds.N.length > 0)
                    NodeManagement.UnmanageNodes(netObjectIds.N, startDate, endDate,
					function () {
					    //if (netObjectIds.I.length == 0) {
					    window.location.reload();
					    //}
					},
					function (error) {
					    alert(error.get_message() + '\n' + error.get_stackTrace());
					    //if (netObjectIds.I.length == 0) {
					    $("#unmanageDialog").dialog("close");
					    //}
					}
				);
                //                if (netObjectIds.I.length > 0)
                //                    NodeManagement.UnmanageInterfaces(netObjectIds.I, startDate, endDate,
                //					function() { window.location.reload(); },
                //					function(error) {
                //					    alert(error.get_message() + '\n' + error.get_stackTrace());
                //					    $("#unmanageDialog").dialog("close");
                //					}
                //				);
            });
            //            caption = "Unmanage " + ((nodeCaption != "") ? nodeCaption : (
            //		((netObjectIds.N.length > 0) ? (netObjectIds.N.length + " node" + (netObjectIds.N.length == 1 ? "" : "s")) : "")
            //		+ (netObjectIds.I.length > 0 ? (((netObjectIds.N.length > 0) ? " & " : " ") + netObjectIds.I.length + " interface" + (netObjectIds.I.length == 1 ? "" : "s")) : "")));

            caption = "Unmanage " + ((nodeCaption != "") ? nodeCaption : (
		((netObjectIds.N.length > 0) ? (netObjectIds.N.length + " node" + (netObjectIds.N.length == 1 ? "" : "s")) : "")));


            var textSize = $("#fontSizeTest")[0].offsetHeight;
            width = 487;
            height = 250;
            $("#unmanageDialog").width((width >= minWidht) ? width : minWidht);
            $("#unmanageDialog").height((height >= minHeight) ? height : minHeight);
            $("#unmanageDialog").find('input').attr("tabindex", "-1");
            dialog = $("#unmanageDialog").dialog({
                width: (width >= minWidht) ? width : minWidht, height: (height >= minHeight) ? height : minHeight, modal: true,
                open: function () {
                    //Workaround JQuery dialog issue - according to jquery UI ticket will be fixed in v1.9 by using focusSelector option
                    $(this).find('input').attr("tabindex", "1");
                },
                overlay: { "background-color": "black", opacity: 0.4 }, title: caption, resizable: false
            });
            dialog.css('overflow', 'hidden');
            dialog.show();
            allowEvent = false;
//            TextResizeDetector.startDetector();

            return false;
        };

        remanageNetObjects = function (nodeIds) {
            if (nodeIds.length > 0) {
                NodeManagement.RemanageNodes(nodeIds,
			        function () {
			            //if (interfaceIds.length == 0) {
			            window.location.reload();
			            //}
			        },
			        function (error) {
			            alert(error.get_message() + '\n' + error.get_stackTrace());
			        }
		        );
            }

            //            if (interfaceIds.length > 0) {
            //                NodeManagement.RemanageInterfaces(interfaceIds,
            //			        function() { window.location.reload(); },
            //			        function(error) {
            //			            alert(error.get_message() + '\n' + error.get_stackTrace());
            //			        }
            //		        );
            //            }
        };

        remanageNodes = function (nodeIds) {
            NodeManagement.RemanageNodes(nodeIds,
			function () { window.location.reload(); },
			function (error) {
			    alert(error.get_message() + '\n' + error.get_stackTrace());
			}
		);
        };

        //        remanageInterfaces = function(interfaceIds) {
        //            NodeManagement.RemanageInterfaces(interfaceIds,
        //			function() { window.location.reload(); },
        //			function(error) {
        //			    alert(error.get_message() + '\n' + error.get_stackTrace());
        //			}
        //		);
        //      };

        var now = new Date();
        $('#<%=unmanageStart.ClientID%>').orionSetDate(now);
        now = new Date(now.getTime() + 24 * 60 * 60 * 1000);
        $('#<%=unmanageEnd.ClientID%>').orionSetDate(now);

        $("#<%=unmanageCancel.ClientID%>").click(function () {
            $("#unmanageDialog").dialog("close");
        });

//        $('div#unmanageDialog').bind('dialogclose', function (event) {
//            TextResizeDetector.stopDetector();
//        });
    });
//    function init() {
//        iBase = TextResizeDetector.addEventListener(onFontResize, null);
//    }
    function onFontResize(e, args) {
        resizeValue = args[0].iDelta;
        if (allowEvent) {
            width = width + resizeValue * 38;
            height = height + resizeValue * 11;
            $("#unmanageDialog").width((width >= minWidht) ? width : minWidht);
            $("#unmanageDialog").height((height >= minHeight) ? height : minHeight);
            var dlg = dialog.parents(".ui-dialog:first");
            dlg.animate({ width: (width >= minWidht) ? width : minWidht, height: (height >= minHeight) ? height : minHeight });
            dialog.data('position.dialog', ['center', 'middle'] ); 
        }
        allowEvent = true;
    }
//]]>
</script>

<asp:ScriptManagerProxy runat="server">
	<Services>
		<asp:ServiceReference path="~/Orion/Services/NodeManagement.asmx" />
	</Services>
	<Scripts>
		<asp:ScriptReference Path="~/Orion/js/jquery/jquery.timePicker.js" />
	</Scripts>
</asp:ScriptManagerProxy>

<% if (Request["Printable"] == null) { %>
<div id="fontSizeTest" style="visibility:hidden!important;">text size test</div>
<div id="unmanageDialog" style="display:none;" class="disposable">
	<%= Resources.UDTWebContent.UDTWEBDATA_AK1_134 %>
	<table width="100%">
		<tr>
			<td rowspan="3" style="vertical-align: top;"><img src="/Orion/images/StatusIcons/Unmanaged.gif" alt="" /></td>
			<td><%= Resources.UDTWebContent.UDTWEBDATA_AK1_135 %></td>
			<td colspan="2"><orion:DateTimePicker runat="server" ID="unmanageStart" /></td>
		</tr>
		<tr>
			<!--<td rowspan="3">big unmanage icon</td>-->
			<td><%= Resources.UDTWebContent.UDTWEBDATA_AK1_136 %></td>
			<td colspan="2"><orion:DateTimePicker runat="server" ID="unmanageEnd" /></td>
		</tr>
		<tr>
			<!--<td rowspan="3">big unmanage icon</td>-->
			<td colspan="3"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_137 %></td>
		</tr>
	</table>

	<div class="bottom">
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButtonLink runat="server" LocalizedText="Ok" DisplayType="Primary" ID="unmanageOk" />
		<orion:LocalizableButtonLink runat="server" LocalizedText="Cancel" DisplayType="Secondary" ID="unmanageCancel" />
        </div>
	</div>
</div>
<% } %>
