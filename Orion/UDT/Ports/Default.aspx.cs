using System;
using System.Linq;
using System.Text;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using System.Collections.Generic;
using SolarWinds.UDT.Common;
using SolarWinds.UDT.Common.Models;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Web.Helpers;

public partial class Orion_UDT_Ports_Default : System.Web.UI.Page
{
    protected bool _allowInterfaces;

	protected override void OnInit(EventArgs e)
	{
        if (!String.IsNullOrEmpty(this.Request.QueryString["SelectedTab"]) && HtmlHelper.FindBadCharacters(this.Request.QueryString["SelectedTab"]))
        {
            Server.Transfer(String.Format("~/Orion/Error.aspx?Message={0}",
                                          this.Request.QueryString["SelectedTab"]));
            return;
        }
        _allowInterfaces = (new RemoteFeatureManager()).AllowInterfaces;
        base.OnInit(e);

        if (!Profile.AllowNodeManagement)
        {
            Server.Transfer(String.Format("~/Orion/Error.aspx?Message={0}", Resources.UDTWebContent.UDTWEBCODE_AK1_9));
        }
	}

	protected override void OnLoad(EventArgs e)
    {
		//bool gotNcm55 = OrionModuleManager.IsModuleVersionAtLeast("NCM", "5.5");
        //this.energyWiseListItem.Visible = gotNcm55;

        //if (DoNodeIconsBlink())
        //    ClientScript.RegisterStartupScript(GetType(), "MNG.BlinkingNodeIcons", "<script type='text/javascript'>if(!window.MNG)window.MNG={};window.MNG.BlinkingNodeIcons=1;</script>");
        pageTitleBar.PageTitle = Resources.UDTWebContent.UDTWEBDATA_AK1_3;
        pageTitleBar.HelpFragment = "OrionUDTPHConfigSettingsManagePorts";

        //hide the customize page link for this page
        pageTitleBar.HideCustomizePageLink = true;
        pageTitleBar.ShowPortDiscoveryLink = true;

       // var featureManager = new RemoteFeatureManager();
       // // maximum moniitored ports allowed by license
       // maxMonitoredPorts.Value = featureManager.GetMaxElementCount(SolarWinds.UDT.Common.WellKnownElementTypes.UDT.Ports).ToString();
        
       // //current monitored port count
       //currentMonitoredPorts.Value = UDTPortDAL.GetPortCount().ToString();
        
        if (!IsPostBack)
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "udtPortTypesList", GetPortTypesArray(), true);
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "udtNodeCapabilitiesList", GetNodeCapabilitiesArray(), true);
        }

    }

    public int GetMonitoredPortCount(string type)
    {
        var featureManager = new RemoteFeatureManager();
        int portCount;

        if (type == "max")
        {
            // maximum moniitored ports allowed by license
            portCount = featureManager.GetMaxElementCount(SolarWinds.UDT.Common.WellKnownElementTypes.UDT.Ports);
        }
        else
        {
            //current monitored port count
            portCount = UDTPortDAL.GetPortCount();
        }
        return portCount;
    }


    private string GetPortTypesArray()
    {
        StringBuilder portTypesScript = new StringBuilder();
        //portTypesScript.Append("var portTypes = new Array();");
        portTypesScript.Append("var portTypes = [];");

        //get portTypes for rendering in the Port management grid
        OidEnums ifTypes;
        ifTypes = MibDAL.GetInterfaceTypes();

        int i = 0;
        foreach(OidEnum ifType in ifTypes)
        {
            portTypesScript.Append("portTypes.push({type: " + ifType.Id + ", name:'" + ifType.Name + "'});");
        }

        return portTypesScript.ToString();
    }

    private string GetNodeCapabilitiesArray()
    {
        StringBuilder nodeCapabilitiesScript = new StringBuilder();
        nodeCapabilitiesScript.Append("var nodeCapabilities = [];");

        string[] names = Enum.GetNames(typeof(UDTNodeCapability));
        
        for( int i = 0; i < names.Length; i++ )
        {
            var id = (int) Enum.Parse(typeof(UDTNodeCapability), names[i]);
            nodeCapabilitiesScript.Append("nodeCapabilities.push({type: " + id + ", name:'" + names[i].ToString() + "'});");
        }
        return nodeCapabilitiesScript.ToString();
    }


    //protected bool DoNodeIconsBlink()
    //{
    //    try
    //    {
    //        return "Blinking".Equals( WebSettingsDAL.Get("NodeChildStatusDisplayMode"), StringComparison.OrdinalIgnoreCase );
    //    }
    //    catch (KeyNotFoundException)
    //    {
    //    }

    //    return false;
    //}

	protected string[] GetCustomProperties(string table)
	{
        return new List<string>(CustomPropertyMgr.GetPropNamesForTable(table, false)).ToArray();
	}

    protected string ReturnUrl
    {
        get { return ReferrerRedirectorBase.GetReturnUrl(); }
    }
}
