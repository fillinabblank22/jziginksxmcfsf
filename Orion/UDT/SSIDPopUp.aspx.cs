﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.UDT.Web.NetObjects;

public partial class Orion_UDT_SSIDPopUp : System.Web.UI.Page
{
    protected UDTWirelessSSIDNetObject UDTSsid { get; set; }

    protected string StatusCssClass
    {
        get
        {
            return "StatusUp";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.UDTSsid = NetObjectFactory.Create(Request["NetObject"], true) as UDTWirelessSSIDNetObject;
    }
}