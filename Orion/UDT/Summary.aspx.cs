using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.UDT.Web.Views;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Common;

public partial class Orion_UDT_Summary : UdtViewPage
{
    public override string ViewType
    {
        get
        {
            return "UDT Summary";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        pageTitleBar.PageTitle = this.ViewInfo.ViewTitle;
        pageTitleBar.ViewID = this.ViewInfo.ViewID;
        //todo add help link
        pageTitleBar.HelpFragment = "OrionUDTPHDeviceTrackerSummary";
        
    }

    protected override void OnInit(EventArgs e)
    {
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }
}
