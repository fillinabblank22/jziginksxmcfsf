using System;
using System.Data;
using System.Web;
using SolarWinds.UDT.Web.Views;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Web.Helpers;
using SolarWinds.UDT.Web.NetObjects;

public partial class Orion_UDT_SSIDDetails : UdtViewPage
{
    private static SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();
    
    public override string ViewType
    {
        get
        {
            return "UDT SSIDDetails";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        pageTitleBar.PageTitle = getTitle();
        pageTitleBar.ViewID = this.ViewInfo.ViewID;
        //todo add help link
        pageTitleBar.HelpFragment = "OrionUDTPHAllSSIDS";
    }

    protected override void OnInit(EventArgs e)
    {
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    
    public string SSID
    {
        get
        {
            string ssid;

            try
            {
                string tmpPort = HttpUtility.UrlDecode(Request.QueryString["NetObject"]);
                var netObject = new UDTNetObject(tmpPort);
                ssid = netObject.Value;
            }
            catch (Exception)
            {
                ssid = "Unknown";
            }
            return ssid;
        }
    }
    
    
    protected string getTitle()
    {
        string viewTitle = this.ViewInfo.ViewTitle;

        try
        {
            viewTitle += UDTWirelessDAL.GetSSIDCaption(SSID);
        }
        catch (Exception ex)
        {
            _log.ErrorFormat(String.Format("Error occurred loading title for UDT SSIDDetails view.  Error:{0}", ex.Message));
        }

        return viewTitle;
    }
}
