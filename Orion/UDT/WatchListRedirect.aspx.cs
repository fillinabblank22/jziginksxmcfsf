using System;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Web.Helpers;
using SolarWinds.UDT.Web.NetObjects;

public partial class Orion_UDT_WatchListRedirect : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        var netObjInfo = UDTNetObject.FromQueryString(Request);

        if (netObjInfo != null)
        {
            int watchId;
            if (int.TryParse(netObjInfo.Value, out watchId))
            {
                var dt = UDTWatchListDAL.GetWatchItem(watchId);

                if (dt.Rows.Count > 0)
                {
                    var type = DALHelper.GetValueOrDefault(dt.Rows[0]["WatchItemType"], string.Empty);
                    var value = DALHelper.GetValueOrDefault(dt.Rows[0]["WatchItem"], string.Empty);
                    if (!string.IsNullOrEmpty(type))
                    {
                        if (UDTWatchListDAL.UDTNetObjectTypeFromWatchType(type.ToString()) == UDTNetObjectType.User)//User
                        {  
                            int userID = UDTUserDAL.GetUserID(value);
                            if (userID != 0)
                            {
                                //Transfer to User Details if user
                                Server.Transfer(string.Format(@"/Orion/UDT/UserDetails.aspx?NetObject={0}:{1}",
                                                          UDTWatchListDAL.UDTNetObjectTypeFromWatchType(type),
                                                           userID));
                            }
                        }
                        else //Other Endpoints
                        {
                            //Transfer to Endpoint Details Otherwise
                            Server.Transfer(string.Format(@"/Orion/UDT/EndpointDetails.aspx?NetObject={0}:VAL={1}",
                                                          UDTWatchListDAL.UDTNetObjectTypeFromWatchType(type), value));
                        }
                    }
                }
            }
        }
        //if all else fails bounce back to the referrer
        if (Page.Request.UrlReferrer != null) 
            Response.Redirect(Page.Request.UrlReferrer.ToString());
    }
}
