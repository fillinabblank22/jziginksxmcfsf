﻿<%@ WebHandler Language="C#" Class="ExportToCSV" %>

using System;
using System.Web;
using System.Web.SessionState;
using System.IO;

public class ExportToCSV : IHttpHandler, IRequiresSessionState {
    
    public void ProcessRequest (HttpContext context)
    {
        string downloadId = HttpContext.Current.Request.QueryString["DownloadId"];
        string downloadPath = Path.GetTempPath();
        string filename = Path.Combine(downloadPath, downloadId + ".csv");
        string downloadName = HttpContext.Current.Request.QueryString["DownloadName"]; 
        
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "text/csv";
        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + downloadName.Trim() + ".csv\"");
        StreamReader f = new StreamReader(filename);
        HttpContext.Current.Response.Write(f.ReadToEnd());
        f.Close();
        HttpContext.Current.Response.End();
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}