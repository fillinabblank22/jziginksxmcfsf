﻿using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.UDT.Web.NetObjects;

public partial class Orion_UDT_AccessPointPopUp : System.Web.UI.Page
{
    protected UDTWirelessAPNetObject UDTAccessPoint { get; set; }

    protected string StatusText
    {
        get
        {
            switch (UDTAccessPoint.Status)
            {
                case (int)OBJECT_STATUS.Up:
                    return Resources.UDTWebContent.UDTWEBCODE_AK1_18;
                case (int)OBJECT_STATUS.Down:
                    return Resources.UDTWebContent.UDTWEBCODE_AK1_19;
                case (int)OBJECT_STATUS.Unmanaged:
                    return Resources.UDTWebContent.UDTWEBCODE_AK1_20;

                default:
                    return Resources.UDTWebContent.UDTWEBCODE_AK1_21;
            }
        }
    }
    protected string StatusCssClass
    {
        get
        {
            switch (UDTAccessPoint.Status)
            {
                case (int)OBJECT_STATUS.Up:
                    return "StatusUp";
                case (int)OBJECT_STATUS.Down:
                    return "StatusDown";
                case (int)OBJECT_STATUS.Unmanaged:
                    return "StatusUnmanaged";

                default:
                    return "StatusUnknown";
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        UDTAccessPoint = NetObjectFactory.Create(Request["NetObject"], true) as UDTWirelessAPNetObject;
    }
}