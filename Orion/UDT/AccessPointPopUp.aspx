﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AccessPointPopUp.aspx.cs" Inherits="Orion_UDT_AccessPointPopUp" %>

<h3 class="<%=StatusCssClass%>"><%=SolarWinds.Orion.Web.Helpers.UIHelper.Escape(UDTAccessPoint.Name)%></h3>

<div class="NetObjectTipBody">
	<table cellpadding="0" cellspacing="0">
        <tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_153 %></th>
			<td><%=StatusText%></td>    
		</tr>
        <tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_154 %></th>
			<td><%=UDTAccessPoint.WirelessTypeText%></td>    
		</tr>
        
        <%if (!string.IsNullOrEmpty(UDTAccessPoint.ControllerName)) {%>
        <tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_155 %></th>
			<td><%=UDTAccessPoint.ControllerName%></td>    
		</tr>
        <%}%>
        <tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_156 %></th>
			<td><%=UDTAccessPoint.CountSSIDs.ToString()%></td>      
		</tr>
        <tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_157 %></th>
			<td><%=UDTAccessPoint.CountClients.ToString()%></td>        
		</tr>
	</table>
</div>
