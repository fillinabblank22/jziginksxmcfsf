﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SSIDPopUp.aspx.cs" Inherits="Orion_UDT_SSIDPopUp" %>
<%@ Import Namespace="SolarWinds.UDT.Common.Models" %>
<%@ Import Namespace="SolarWinds.UDT.Web.Helpers" %>

<h3 class="<%=this.StatusCssClass%>"><%=HtmlHelper.ParseSSIDs(SolarWinds.Orion.Web.Helpers.UIHelper.Escape(UDTSsid.Name)) %></h3>

<div class="NetObjectTipBody">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<th style="white-space: nowrap;"><%=HtmlHelper.ParseSSIDs(UDTSsid.Name)%></th>	   
		</tr>
        <tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_168 %></th>
			<td style="width:20px"><%=UDTSsid.CountAPs%></td>    
		</tr>

		<tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_169 %></th>
            <td style="width:20px"><%=UDTSsid.CountChannels%></td>  
		</tr>

        <tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_170 %></th>
			<td style="width:20px"><%=UDTSsid.CountClients%></td>  
		</tr>
	</table>
</div>
