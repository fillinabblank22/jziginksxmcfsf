﻿<%@ Page Language="C#" MasterPageFile="~/Orion/UDT/UdtPage.master" AutoEventWireup="true" CodeFile="RemoveResource.aspx.cs" Inherits="Orion_UDT_RemoveResource" %>

<%@ Register TagPrefix="udt" TagName="PageTitleBar" Src="~/Orion/UDT/Controls/PageTitleBar.ascx" %>

<asp:Content ID="udtHeader" ContentPlaceHolderID="UdtHeadPlaceHolder" Runat="Server">
    <style type="text/css">
        .RemoveResourceContainer 
        {
            padding: 10px;
        }
        .CheckboxContainer 
        {
            font-weight: bold;
        }
    </style>
</asp:Content>

<asp:Content ID="SummaryTitle" ContentPlaceHolderID="TopRightPageLinks" runat="Server">
    <udt:PageTitleBar runat="server" ID="pageTitleBar" ShowTitle="False"/>
</asp:Content>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="ViewPageTitle">
    <h1><%= String.Format(Resources.UDTWebContent.UDTWEBCODE_AK1_13, ResourceNameShort) %></h1>
</asp:Content>

<asp:Content ID="ContentPlaceHolder1" ContentPlaceHolderID="UdtMainContentPlaceHolder" runat="Server">
<div class="RemoveResourceContainer">
    <div class="ResourceWrapper" id="etxxRW" style="width: 800px;">
        <div class="etxxLH">
            <div runat="server" class="CheckboxContainer" id="CheckBoxDiv">
            </div>
            <br />
            <%= String.Format(Resources.UDTWebContent.UDTWEBDATA_AK1_145, ResourceNameShort,
                              String.Format("<a href=\"/Orion/Admin/ListViews.aspx\" title=\"{0}\" style=\"color: #86601c; text-decoration: underline;\">", 
                              Resources.UDTWebContent.UDTWEBDATA_AK1_146), "</a>") %>
            <br />
        </div>
    </div>
    <br />
    <div class="sw-btn-bar">
        <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" DisplayType="Primary" OnClick="SubmitClick" />
    </div>
</div>
</asp:Content>
