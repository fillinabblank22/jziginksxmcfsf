<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/UDT/UdtPage.master" AutoEventWireup="true"
    CodeFile="Summary.aspx.cs" Inherits="Orion_UDT_Summary" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%-- TODO ???: <%@ Register TagPrefix="udt" TagName="LicenseExpirationMessage" Src="~/Orion/UDT/Controls/LicenseExpirationMessage.ascx" %>--%>
<%@ Register TagPrefix="udt" TagName="PageTitleBar" Src="~/Orion/UDT/Controls/PageTitleBar.ascx" %>
<%@ Register TagPrefix="orion" TagName="Refresher" Src="~/Orion/Controls/Refresher.ascx" %>

<asp:Content ID="SummaryTitle" ContentPlaceHolderID="TopRightPageLinks" runat="Server">
    <udt:PageTitleBar runat="server" ID="pageTitleBar" ShowTitle="False"/>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ViewPageTitle">
    <h1><%=this.ViewInfo.ViewTitle%></h1>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="UdtMainContentPlaceHolder" runat="server">
    <orion:ResourceHostControl ID="ResourceHostControl2" runat="server">
        <orion:ResourceContainer runat="server" ID="resContainer" />
    </orion:ResourceHostControl>
    <%--TODO ??? <udt:LicenseExpirationMessage ID="_licenseExpirationMessage" runat="server" />--%>
</asp:Content>

