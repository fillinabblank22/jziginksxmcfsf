﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.UDT.Web.Resources;

public partial class Orion_UDT_Controls_ResourcePagerHtml : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {


    }

    public bool Hidden { get; set; }
    public int ResourceId { get; set; }

    protected string VisiblityMode { get { return Hidden ? "none" : "block"; } }


}