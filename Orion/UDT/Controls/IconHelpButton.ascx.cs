using System;

public partial class UdtIconHelpButton : System.Web.UI.UserControl
{
   public string HelpUrlFragment { get; set; }
    
    protected void Page_PreRender(object sender, EventArgs e)
    {
        lnkHelp.HelpUrlFragment = HelpUrlFragment;
    }

}
