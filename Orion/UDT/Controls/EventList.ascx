<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EventList.ascx.cs" Inherits="Orion_UDT_Controls_EventList" %>

<asp:Repeater ID="EventsGrid" runat="server" >
	<HeaderTemplate>
		<table width="100%" cellspacing="0" cellpadding="0" class="Events NeedsZebraStripes">
	</HeaderTemplate>
	
	<ItemTemplate>

        <tr>
          <td style="width: 100px;" ><%#Eval("EventTime", "{0:g}") %></td>
 				<td>
                	<div class="event-icon" style="background-color:#<%# Convert.ToInt32(Eval("BackColor")).ToString("X") %>;">
                    	<img src="/NetPerfMon/images/Event-<%#DataBinder.Eval(Container.DataItem, "EventType") %>.gif" border="0" alt="Event Type">
                	</div>
            	</td>
				<td>
                	<%#DataBinder.Eval(Container.DataItem, "Message") %>
            	</td>
               

        </tr>
        

	</ItemTemplate>
	
	<FooterTemplate>
		</table>
	</FooterTemplate>
</asp:Repeater>
