﻿Ext.namespace('SW');
Ext.namespace('SW.UDT');

Ext.ns('Ext.ux.form');

var checkedItemsList = [];

function getQsValueByItem(qsItem) {

    var results, regex = new RegExp("(?:[?]|&)" + qsItem + "=([^&]*)");
    
    return (results = regex.exec(window.location.search)) ?
                           decodeURIComponent(results[1]) :
                           '';
};

function FindSearchHref(q, col, idx) {
//    alert('q: ' + q + ' col: ' + col + ' idx: ' + idx);
    var qs = "?q=" + encodeURIComponent(q);
    var qsCols = "&cols=" + encodeURIComponent(col);
    var url = "/Orion/UDT/SearchResults.aspx";
    ORION.Prefs.save('SearchItemList', idx);
//    alert('Formatted: q: ' + q + ' col: ' + col + ' idx: ' + idx);  
    document.location.href = url + qs + qsCols;
};
// basically the same as the above but it will save a setting to force the radio button to select historical records also
function FindSearchHrefIncludeHistory(q, col, idx) {
    //    alert('q: ' + q + ' col: ' + col + ' idx: ' + idx);
    var qs = "?q=" + encodeURIComponent(q);
    var qsCols = "&cols=" + encodeURIComponent(col);
    var url = "/Orion/UDT/SearchResults.aspx";
    ORION.Prefs.save('SearchItemList', idx);
    ORION.Prefs.save('SearchItemOption','All');
    //    alert('Formatted: q: ' + q + ' col: ' + col + ' idx: ' + idx);

    document.location.href = url + qs + qsCols;
};
var searchForString = (getQsValueByItem('q') == null) ? "" : getQsValueByItem('q');

var isOtherVendorSearch = (getQsValueByItem('isOtherLegend') == null || getQsValueByItem('isOtherLegend')=="") ? false : true;

Ext.ux.form.SearchBox = Ext.extend(Ext.form.TwinTriggerField, {
    // data for the menu items
    store: [
            ['1', '@{R=UDT.Strings;K=UDTWEBJS_AK1_2;E=js}', '@{R=UDT.Strings;K=UDTWEBJS_AK1_8;E=js}'],
            ['2', '@{R=UDT.Strings;K=UDTWEBJS_AK1_3;E=js}', '@{R=UDT.Strings;K=UDTWEBJS_AK1_7;E=js}'],
            ['4', '@{R=UDT.Strings;K=UDTWEBJS_AK1_4;E=js}', '@{R=UDT.Strings;K=UDTWEBJS_AK1_9;E=js}'],
            ['8', '@{R=UDT.Strings;K=UDTWEBJS_AK1_5;E=js}', '@{R=UDT.Strings;K=UDTWEBJS_AK1_10;E=js}'],
            ['16', '@{R=UDT.Strings;K=UDTWEBJS_AK1_6;E=js}', '@{R=UDT.Strings;K=UDTWEBJS_AK1_11;E=js}'],
            ['32', '@{R=UDT.Strings;K=UDTWEBJS_AK1_7;E=js}', '@{R=UDT.Strings;K=UDTWEBJS_AK1_12;E=js}'],
            ['64', '@{R=UDT.Strings;K=UDTWEBJS_GK1_1;E=js}']
            
            ],
    //['128', 'VLAN', 'Port or Endpoint VLAN membership']],

    initComponent: function () {
        Ext.ux.form.SearchBox.superclass.initComponent.call(this);

        this.triggerConfig = {
            tag: 'span', cls: 'x-form-twin-triggers', cn: [
            { tag: "img", src: Ext.BLANK_IMAGE_URL, cls: 'x-form-trigger ' },
            { tag: "img", src: "/Orion/Admin/Accounts/images/search/search_button.gif", cls: "x-form-trigger " + this.trigger2Class }
            ]
        };

        ORION.prefix = "UDT_SearchResults_";

        this.on('specialkey', function (f, e) {
            if (e.getKey() == e.ENTER) {
                this.onTrigger2Click();
            }
        }, this);

        // set search field text styles
        this.on('focus', function () {
            this.el.applyStyles('font-style:normal;');
        }, this);


        //auto-configure store from local array data 
        this.store = new Ext.data.SimpleStore({
            id: 'value',
            fields: ['value', 'text'],
            data: this.store
        });
        this.valueField = 'value';
        this.displayField = 'text';

    },

    //id: 'searchAccounts',
    validationEvent: false,
    validateOnBlur: false,

    trigger1Class: 'x-form-trigger',
    trigger2Class: 'x-form-search-trigger',

    // default search field text style
    emptyText: '@{R=UDT.Strings;K=UDTWEBJS_AK1_1;E=js}',
    value: searchForString,

    //width: 180,
    hasSearch: false,

    onRender: function (ct, position) {

        Ext.ux.form.SearchBox.superclass.onRender.call(this, ct, position);

        // build the menu
        if (this.menu == null) {
            this.menu = new Ext.menu.Menu({ width: this.width });
            this.menu.on('hide', function () {
                // bug fix FB43495 - i.e. does not clear the text box emptytext after menu has been displayed.
                // issue is focus event not firing correctly in i.e. so lets force it to fire.
                this.fireEvent('focus', this);

                if (this.tmp != this.lastSelectionText && this.lastSelectionText != '') {
                    this.tmp = this.lastSelectionText;
                    this.fireEvent('change', this);
                }
            }, this)

            // build a menu item from each value in the store
            this.store.each(function (itm) {
                this.menu.add(
        			new Ext.menu.CheckItem({
        			    id: 'checkitem' + itm.data[this.valueField],
        			    text: itm.data[this.displayField],
        			    value: itm.data[this.valueField],
        			    hideOnClick: false,
        			    checked: true
        			})
        		).on('click', this.checkboxClickHandler, this);

            }, this);

        }

        // update the checked item from the hidden field
        this.updateCheckedFromHidden();

    },
    // this is the dropdown list icon button
    onTrigger1Click: function () {
        
        this.menu.show(this.el,'bl');

        // Correcting container height.
        if (this.containerHeight != '' && this.menu.el.getHeight() > this.containerHeight) {
            this.menu.el.setHeight(this.containerHeight);
            this.menu.el.dom.style.overflowY = 'auto';
        }

        this.menu.items.each(function (r) {
            r.el.dom.style.overflowX = 'hidden';
        }, this);

        this.menu.el.dom.style.overflowX = 'hidden';

    },

    //update the checked items from the user preferences
    updateCheckedFromHidden: function () {
        var selectedCheckItems = ORION.Prefs.load('SearchItemList', '');

        // if no SearchItemList value (or an empty string), default to selecting IP, Hostname and MAC address columns
        if (selectedCheckItems == null || selectedCheckItems == '') {
            // ensure we save the user setting back to the database as the SearchResults.aspx page relies on it to disply the correct search results.
            ORION.Prefs.save('SearchItemList', '1,2,4,8,16,32,64');
            selectedCheckItems = '1,2,4,8,16,32,64';
        }

        var checkeditems = [];
        checkeditems = selectedCheckItems.split(',');

        if (this.menu) {
            this.menu.items.each(function(item) {
                var idx = checkeditems.indexOf(item.value);
                if (idx != undefined && idx > -1) {
                    item.setChecked(true);
                    // add checked item to the checkedItemsList array
                    checkedItemsList.push(item.value);
                } else {
                    item.setChecked(false);
                }
                ;
            });
        }
    },


    onDestroy: function () {
        if (this.menu) {
            this.menu.destroy();
        }
        if (this.wrap) {
            this.wrap.remove();
        }
        Ext.ux.form.SearchBox.superclass.onDestroy.call(this);
    },

    checkboxClickHandler: function (i, c) {

        var checkedItemDelimitedString;
        // add item to list array if item is being checked
        if (i.checked == false) {
            checkedItemsList.push(i.value);
            checkedItemsList.sort();
        }
        // remove item from list array if item is being unchecked
        else {
            var idx = checkedItemsList.indexOf(i.value); // Find the index
            if (idx != -1) {
                checkedItemsList.splice(idx, 1); // Remove it if really found
            }
        }
        checkedItemDelimitedString = checkedItemsList.join();
        ORION.Prefs.save('SearchItemList', checkedItemDelimitedString);
    },

    //  this is the search icon button
    onTrigger2Click: function () {
                filterText = this.getRawValue().trim();
                var checkedItemsTextList = [];
                if (this.menu) {
                    this.menu.items.each(function (item) {
                        if (item.checked == true) {
                            checkedItemsTextList.push(item.text);
                        };
                    });
                }

                // if no search box drop-down list columns are selected, the default to selecting MAC, IP,  hostname and Vendor
                if (checkedItemsTextList.length == 0) {
                    ORION.Prefs.save('SearchItemList', '1,2,4,8,16,32,64');
                    checkedItemsTextList.push('@{R=UDT.Strings;K=UDTWEBJS_AK1_2;E=js}');
                    checkedItemsTextList.push('@{R=UDT.Strings;K=UDTWEBJS_AK1_3;E=js}');
                    checkedItemsTextList.push('@{R=UDT.Strings;K=UDTWEBJS_AK1_4;E=js}');
                    checkedItemsTextList.push('@{R=UDT.Strings;K=UDTWEBJS_AK1_5;E=js}');
                    checkedItemsTextList.push('@{R=UDT.Strings;K=UDTWEBJS_AK1_6;E=js}');
                    checkedItemsTextList.push('@{R=UDT.Strings;K=UDTWEBJS_AK1_7;E=js}');
                    checkedItemsTextList.push('@{R=UDT.Strings;K=UDTWEBJS_GK1_1;E=js}');
                }

                var qs = "?q=" + encodeURIComponent(filterText.toString());
                var qsCols = "&cols=" + encodeURIComponent(checkedItemsTextList.join(', '))
                var url = "/Orion/UDT/Search/SearchResults.aspx";

                document.location.href = url + qs + qsCols;

    }

});

