﻿Ext.namespace('SW');
Ext.namespace('SW.UDT');

Ext.ns('Ext.ux.form');

var checkedItemsList = [];

function getQsValueByItem(qsItem) {

    var results, regex = new RegExp("(?:[?]|&)" + qsItem + "=([^&]*)");
    
    return (results = regex.exec(window.location.search)) ?
                           decodeURIComponent(results[1]) :
                           '';
};

function FindSearchHref(q, col, idx) {
//    alert('q: ' + q + ' col: ' + col + ' idx: ' + idx);
    var qs = "?q=" + encodeURIComponent(q);
    var qsCols = "&cols=" + encodeURIComponent(col);
    var url = "/Orion/UDT/SearchResults.aspx";
    ORION.Prefs.save('SearchItemListEx', idx);
//    alert('Formatted: q: ' + q + ' col: ' + col + ' idx: ' + idx);

    document.location.href = url + qs + qsCols;
};
// basically the same as the above but it will save a setting to force the radio button to select historical records also
function FindSearchHrefIncludeHistory(q, col, idx) {
    //    alert('q: ' + q + ' col: ' + col + ' idx: ' + idx);
    var qs = "?q=" + encodeURIComponent(q);
    var qsCols = "&cols=" + encodeURIComponent(col);
    var url = "/Orion/UDT/SearchResults.aspx";
    ORION.Prefs.save('SearchItemListEx', idx);
    ORION.Prefs.save('SearchItemOptionEx','All');
    //    alert('Formatted: q: ' + q + ' col: ' + col + ' idx: ' + idx);

    document.location.href = url + qs + qsCols;
};

var searchForString = (getQsValueByItem('q') == null) ? "" : getQsValueByItem('q');

Ext.ux.form.SearchBoxEx = Ext.extend(Ext.form.TwinTriggerField, {
    // data for the menu items
    store: [['1', '@{R=UDT.Strings;K=UDTWEBJS_VB1_24;E=js}', '@{R=UDT.Strings;K=UDTWEBJS_AK1_10;E=js}'],
            ['2', '@{R=UDT.Strings;K=UDTWEBJS_VB1_25;E=js}', '@{R=UDT.Strings;K=UDTWEBJS_AK1_9;E=js}'],
            ['3', '@{R=UDT.Strings;K=UDTWEBJS_VB1_23;E=js}', '@{R=UDT.Strings;K=UDTWEBJS_AK1_11;E=js}'],
            ['4', '@{R=UDT.Strings;K=UDTWEBJS_VB1_96;E=js}', '@{R=UDT.Strings;K=UDTWEBJS_VB1_105;E=js}'],
            ['5', '@{R=UDT.Strings;K=UDTWEBJS_VB1_97;E=js}', '@{R=UDT.Strings;K=UDTWEBJS_VB1_106;E=js}'],
            ['6', '@{R=UDT.Strings;K=UDTWEBJS_VB1_98;E=js}', '@{R=UDT.Strings;K=UDTWEBJS_VB1_107;E=js}']
            ],
    //['7', 'VLAN', 'Port or Endpoint VLAN membership']],

    initComponent: function () {
        Ext.ux.form.SearchBoxEx.superclass.initComponent.call(this);

        ORION.prefix = "UDT_SearchResultsEx_";

        this.on('specialkey', function (f, e) {
            if (e.getKey() == e.ENTER) {
                this.onTrigger2Click();
            }
        }, this);

        // set search field text styles
        this.on('focus', function () {
            this.el.applyStyles('font-style:normal;');
        }, this);


        //auto-configure store from local array data 
        this.store = new Ext.data.SimpleStore({
            id: 'value',
            fields: ['value', 'text'],
            data: this.store
        });
        this.valueField = 'value';
        this.displayField = 'text';

    },

    //id: 'searchAccounts',
    validationEvent: false,
    validateOnBlur: false,

    trigger1Class: 'x-form-trigger',
    trigger2Class: 'x-form-search-trigger',

    // default search field text style
    emptyText: '@{R=UDT.Strings;K=UDTWEBJS_AK1_1;E=js}',
    value: searchForString,

    hasSearch: false,

    onRender: function (ct, position) {

        Ext.ux.form.SearchBoxEx.superclass.onRender.call(this, ct, position);

        // build the menu
        if (this.menu == null) {
            this.menu = new Ext.menu.Menu({ width: this.width });
            this.menu.on('hide', function () {
                // bug fix FB43495 - i.e. does not clear the text box emptytext after menu has been displayed.
                // issue is focus event not firing correctly in i.e. so lets force it to fire.
                this.fireEvent('focus', this);

                if (this.tmp != this.lastSelectionText && this.lastSelectionText != '') {
                    this.tmp = this.lastSelectionText;
                    this.fireEvent('change', this);
                }
            }, this)

            // build a menu item from each value in the store
            this.store.each(function (itm) {
                this.menu.add(
        			new Ext.menu.CheckItem({
        			    id: 'checkitem' + itm.data[this.valueField],
        			    text: itm.data[this.displayField],
        			    value: itm.data[this.valueField],
        			    hideOnClick: false,
        			    checked: true
        			})
        		).on('click', this.checkboxClickHandler, this);

            }, this);

        }

        // update the checked item from the hidden field
        this.updateCheckedFromHidden();

    },
    // this is the dropdown list icon button
    onTrigger1Click: function () {

        this.menu.show(this.el,'bl');

        // Correcting container height.
        if (this.containerHeight != '' && this.menu.el.getHeight() > this.containerHeight) {
            this.menu.el.setHeight(this.containerHeight);
            this.menu.el.dom.style.overflowY = 'auto';
        }

        this.menu.items.each(function (r) {
            r.el.dom.style.overflowX = 'hidden';
        }, this);
        this.menu.el.dom.style.overflowX = 'hidden';

    },

    //update the checked items from the user preferences
    updateCheckedFromHidden: function () {
        var selectedCheckItems = ORION.Prefs.load('SearchItemListEx', '');

        // if no SearchItemList value (or an empty string), default to selecting IP, Hostname and MAC address columns
        if (selectedCheckItems == null || selectedCheckItems == '') {
            // ensure we save the user setting back to the database as the SearchResults.aspx page relies on it to disply the correct search results.
            ORION.Prefs.save('SearchItemListEx', '1,2,3');
            selectedCheckItems = '1,2,3'; 
        }
        
        var checkeditems = [];
        checkeditems = selectedCheckItems.split(',');

        if (this.menu) {
            this.menu.items.each(function (item) {
                var idx = checkeditems.indexOf(item.value);
                if (idx != undefined && idx > -1) {
                    item.setChecked(true);
                    // add checked item to the checkedItemsList array
                    checkedItemsList.push(item.value);
                }
                else {
                    item.setChecked(false);
                };
            })
        }
    },


    onDestroy: function () {
        if (this.menu) {
            this.menu.destroy();
        }
        if (this.wrap) {
            this.wrap.remove();
        }
        Ext.ux.form.SearchBoxEx.superclass.onDestroy.call(this);
    },

    checkboxClickHandler: function (i, c) {

        var checkedItemDelimitedString;
        // add item to list array if item is being checked
        if (i.checked == false) {
            checkedItemsList.push(i.value);
            checkedItemsList.sort();
        }
        // remove item from list array if item is being unchecked
        else {
            var idx = checkedItemsList.indexOf(i.value); // Find the index
            if (idx != -1) {
                checkedItemsList.splice(idx, 1); // Remove it if really found
            }
        }
        checkedItemDelimitedString = checkedItemsList.join();
        ORION.Prefs.save('SearchItemListEx', checkedItemDelimitedString);
    },

    //  this is the search icon button
    onTrigger2Click: function () {
        filterText = this.getRawValue().trim();
        var checkedItemsTextList = [];
        if (this.menu) {
            this.menu.items.each(function (item) {
                if (item.checked == true) {
                    checkedItemsTextList.push(item.text);
                };
            });
        }

        // if no search box drop-down list columns are selected, the default to selecting MAC, IP and hostname
        if (checkedItemsTextList.length == 0) {
            ORION.Prefs.save('SearchItemListEx', '1,2,3');
            checkedItemsTextList.push('IP Address');
            checkedItemsTextList.push('Hostname');
            checkedItemsTextList.push('MAC Address');
        }

        var qs = "?q=" + encodeURIComponent(filterText.toString());
        var qsCols = "&cols=" + encodeURIComponent(checkedItemsTextList.join(', '))
        var url = "/Orion/UDT/SearchResults.aspx";

        document.location.href = url + qs + qsCols;

    }

});