using System;
using System.Data;
using SolarWinds.Orion.Common;

public partial class Orion_UDT_Controls_EventList : System.Web.UI.UserControl
{
    public object DataSource
    {
        get { return EventsGrid.DataSource; }
        set
        {
            var dataTable = (DataTable)value;
            foreach (DataRow row in dataTable.Rows)
            {
                var time = (DateTime)row["EventTime"];
                row["EventTime"] = time.ToLocalTime();
            }
            EventsGrid.DataSource = dataTable;
            EventsGrid.DataBind();
        }
    }

    protected string FormatTime(object time)
    {
        DateTime t = (DateTime)time;
        return Utils.FormatCurrentCultureDate(t.ToLocalTime()) + " " + Utils.FormatToLocalTime(t.ToLocalTime());
    }
}
