﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PageTitleBar.ascx.cs" Inherits="Orion_UDT_Controls_PageTitleBar" %>
<%@ Register TagPrefix="udt" TagName="IconHelpButton" Src="~/Orion/UDT/Controls/IconHelpButton.ascx" %>
<%@ Register TagPrefix="udt" TagName="IconSettingsButton" Src="~/Orion/UDT/Controls/IconSettingsButton.ascx" %>
<%@ Register TagPrefix="udt" TagName="SearchBox" Src="~/Orion/UDT/Controls/SearchBox.ascx"  %>
<%@ Register TagPrefix="udt" TagName="SearchBoxV1" Src="~/Orion/UDT/Controls/SearchBoxEx.ascx"  %>
<%--<%@ Register TagPrefix="orion" TagName="adminnav" Src="~/Orion/Admin/AdminNav.ascx" %>--%>

<%if (this.ShowTitle){%>
<style type="text/css">
        .sw-hdr-links { float: none; margin-right: 0px; margin-top: 0px; }
</style>
<table class="titleTable">
    <tr>
        <td style="margin-left: 10px; margin-bottom: 10px;">
            <%--<td style="width:250px;" id="breadcrumb">--%>
            <%--<div><a href="../Admin">Admin</a> &gt;</div>--%>
            <h1 style="padding-left: 10px;">
                <% = this.PageTitle %></h1>
        </td>
        <td class="dateArea"><% } %>
            <div align="right">
                <% if (!this.UseV1SearchControl)
                   { %>
                <udt:SearchBox ID="SearchBox1" runat="server" />
                <% }
                   else
                   { %>
                <udt:SearchBoxV1 ID="SearchBox2" runat="server" />
                <% } %>
            </div>
            <div align="right">
                <% if (!this.DisablePDFExport) { %>
                    <script type="text/javascript" src="/Orion/js/exporttopdf.js" ></script>
			        <orion:IconLinkExportToPDF runat="server" ID="ExportToPDFLink"/>
                <% } %>

<%--                <%if (Profile.AllowCustomize)
                    {%>--%>
                <% if (!this.HideCustomizePageLink) { %>                  
                    <a href="<%=CustomizeViewHref%>" id="CustomizePageLink" class="udtcustPageLink"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_33 %></a>
                <% } %>
<%--                <%}%>--%>

              <% if (this.ShowPortDiscoveryLink) { %>                  
                    <a href="<%=PortDiscoveryHref%>" id="PortDiscoveryLink" class="udtPortDiscoveryLink"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_34 %></a>
                <% } %>

                <% if (!this.HideSettingsLink) { %> 
                    <udt:IconSettingsButton ID="IconSettingsButton" runat="server" Visible="true" />
                <% } %>
               
                <udt:IconHelpButton ID="btnHelp" runat="server" Visible="false" />
                </div>
                <% if (!this.HideDate) { %> 
                <div class="udtTitleBarDate" style="text-align: right">
		            <%=DateTime.Now.ToString("F")%>
		        </div>
                <% } %>

<%--            <h1><%= this.PageTitle %></h1>--%>
<%if (this.ShowTitle){%>
        </td>
    </tr>
</table>
<% } %>