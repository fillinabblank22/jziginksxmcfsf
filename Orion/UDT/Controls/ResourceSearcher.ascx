﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResourceSearcher.ascx.cs" Inherits="Orion_UDT_Controls_ResourceSearcher" %>

<script type="text/javascript">
    var GetLocalizedTitle = function () {
        return '<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEBDATA_VB1_144)%>';
    }
</script>

<div style="width: 100%; display: <%=VisiblityMode%>" id="udtResourceSearcher-<%=HostResourceID%>">
<asp:UpdatePanel ID="SearcherUpdatePanel" runat="server" UpdateMode="Conditional">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="SearchButton" />
        <asp:AsyncPostBackTrigger ControlID="ClearButton" />
    </Triggers>
    <ContentTemplate>
        <script type="text/javascript">
            function KeyDownHandler(e, target) {
                var evt = e || window.event;
                var keyPressed = evt.which || evt.keyCode;
                if (keyPressed == 13) {
                    evt.returnValue = false;
                    evt.cancel = true;
                    document.getElementById(target).click();
                }
            }
        </script> 
        <asp:Panel  runat="server" ID="SearchPanel" DefaultButton="SearchButton" 
                Width="280" HorizontalAlign="Right" 
                style="margin: 1px 1px 3px 1px; float:right; ">
                        <asp:TextBox  runat="server" ID="SearchTextBox" Width="70%" 
                            style="vertical-align:middle; height:15px; color: gray; " Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_144 %>"
                            onclick="if(this.value==GetLocalizedTitle()){this.value=''; this.style.color='black' }" 
                            onblur="if(this.value==''){this.value=GetLocalizedTitle(); this.style.color='gray' }" />
                        <asp:ImageButton runat="server" ID="ClearButton" style="vertical-align:middle;" ImageUrl="/Orion/images/clear_button.gif" OnClick="ClearButton_Click" CausesValidation="false" Visible="false" />
                        <asp:ImageButton runat="server"  ID="SearchButton" style="vertical-align:middle;" ImageUrl="/Orion/images/button.searchicon.gif" OnClick="SearchButton_Click" CausesValidation="false" />
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel> 
</div>