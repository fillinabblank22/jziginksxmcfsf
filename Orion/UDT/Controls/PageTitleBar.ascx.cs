﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.UDT.Web.Helpers;

public partial class Orion_UDT_Controls_PageTitleBar : System.Web.UI.UserControl
{
    private bool _hideCustomizePageLink;
    private bool _hideSettingsLink;
    private bool? _isDemoServer;
    private bool showTilte = true;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(HelpFragment))
        {
            btnHelp.HelpUrlFragment = this.HelpFragment;
            btnHelp.Visible = true;
        }

    }

    internal bool IsDemoServer
    {
        get
        {   
            if (!_isDemoServer.HasValue)
                _isDemoServer = SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
            return _isDemoServer.Value;
        }
    }

    public string PageTitle { get; set; }
    public string HelpFragment { get; set; }
    public int ViewID { get; set; }
    public bool DisablePDFExport { get; set; }
    public bool UseV1SearchControl { get; set; }
    public bool HideCustomizePageLink
    {
        get
        {
            // force the customize button to hide if account does not allow customize
            string allow = HttpContext.Current.Profile.GetPropertyValue("AllowCustomize").ToString();
            if (allow == "False")
            {
                return true;
            }
            
            return _hideCustomizePageLink;
        }

       set { _hideCustomizePageLink = value; }
    }
    public bool HideSettingsLink
    {
        get
        {
            // force the settings button to hide if account does not allow admin
            string allow = HttpContext.Current.Profile.GetPropertyValue("AllowAdmin").ToString();
            if (allow == "False")
            {
                return true;
            }
            if (IsDemoServer)
                return true;

            return _hideSettingsLink;
        }
        
        set { _hideSettingsLink = value; }
    }

    public bool HideDate { get; set; }

    public bool ShowPortDiscoveryLink 
    { 
        get 
        {
            if (IsDemoServer)
                return false;

            return _showPortDiscoveryLink;
        }
        set { _showPortDiscoveryLink = value; } 
    }

    private bool _showPortDiscoveryLink;

    protected string CustomizeViewHref
    {
        get
        {
            return HtmlHelper.GetDefaultCustomizeViewHref(this.ViewID);
        }
    }

    protected string PortDiscoveryHref
    {
        get
        {
            return HtmlHelper.GetPortDiscoveryHref(this.ViewID);
        }
    }

    public bool ShowTitle
    {
        get { return showTilte; }
        set { showTilte = value; }
    }
}