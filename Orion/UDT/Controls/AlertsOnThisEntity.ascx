﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlertsOnThisEntity.ascx.cs" Inherits="Orion_UDT_Controls_AlertsOnThisEntity" %>
<%@ Register TagPrefix="orion" TagName="SortablePageableTable" Src="~/Orion/NetPerfMon/Controls/SortablePageableTable.ascx" %>

<style type="text/css">
    /*For own grid rendering*/
    .activeAlerts-rowInfo {
        background-color: #EEEEEE;
    }

    .activeAlerts-rowWarning {
        background-color: #FDE2BB;
    }

    .activeAlerts-rowError {
        background-color: #F8BABF;
    }
</style>
<orion:SortablePageableTable ID="SortablePageableTable" runat="server" />

<input type="hidden" id="OrderBy-<%= ResourceID %>" />

<script type="text/javascript">
    $(function () {
        SW.Core.Resources.ActiveAlerts.SortablePageableTable.initialize(
        {
            uniqueId: '<%= ResourceID %>',
            pageableDataTableProvider: function(pageIndex, pageSize, onSuccess, onError) {
                var uniqueId = '<%= ResourceID %>';
                var currentOrderBy = $('#OrderBy-' + uniqueId).val();
                SW.Core.Services.callController("/api/UDTActiveAlerts/GetActiveAlertsUDT",
                {
                    CurrentPageIndex: pageIndex,
                    PageSize: pageSize,
                    OrderByClause: currentOrderBy,
                    LimitationIds: [<%= string.Join(",", Limitation.GetCurrentListOfLimitationIDs()) %>],
                    ShowAcknowledgedAlerts: '<%= ShowAcknowledgedAlerts %>',
                    RelatedNodeEntityUri: '<%= RelatedNodeEntityUri %>',
                    RelatedNodeId: <%= RelatedNodeId %>,
                    TriggeringObjectEntityNames: [<%= string.Join(", ", TriggeringObjectEntityNames.Select(n => "'" + n + "'")) %>],
                    TriggeringObjectEntityUris: [<%= string.Join(", ", TriggeringObjectEntityUris.Select(n => "'" + n + "'")) %>]
                }, function(result) {
                    onSuccess(result);
                    var $resourceWrapperTitle = $("div.ResourceWrapper[resourceid=<%= ResourceID %>] h1:first");
                    if ($resourceWrapperTitle.find("span").length == 0) {
                        $resourceWrapperTitle.append(SW.Core.String.Format("<span><%= Resources.CoreWebContent.Resource_ActiveAlerts_TitleCount %></span>", result.TotalRows));
                    } else {
                        $resourceWrapperTitle.find("span").text(SW.Core.String.Format("<%= Resources.CoreWebContent.Resource_ActiveAlerts_TitleCount %>", result.TotalRows));
                    }
                }, function(errorResult) {
                    onError(errorResult);
                });
            },
            initialPage: 0,
            rowsPerPage: '<%= InitialRowsPerPage %>',
            allowSort: true,
            getRowClass: function(row) {
                if (row[9] === 0) {
                    return "activeAlerts-rowInfo";
                } else if (row[9] == 1) {
                    return "activeAlerts-rowWarning";
                } else if (row[9] == 2) {
                    return "activeAlerts-rowError";
                }
                return null;
            },
            columnSettings: {
                "AlertName": {
                    caption: "<%= Resources.CoreWebContent.Resource_ActiveAlerts_AlertName %>",
                    isHtml: true,
                    formatter: function(cellValue, rowArray) {
                        var severityImage = "InformationalAlert";
                        if (rowArray[9] === 0) {
                            severityImage = "InformationalAlert";
                        } else if (rowArray[9] == 1) {
                            severityImage = "Warning";
                        } else if (rowArray[9] == 2) {
                            severityImage = "Critical";
                        }

                        var alertName = cellValue;
                        if (rowArray[0] > 0)
                            alertName = SW.Core.String.Format('<a href="/Orion/NetPerfMon/ActiveAlertDetails.aspx?NetObject=AAT:{0}" >{1}</a>', rowArray[0], cellValue);

                        return SW.Core.String.Format('<img src="/Orion/images/ActiveAlerts/{0}.png" />&nbsp;<span style="vertical-align: super;" >{1}</span>',
                            severityImage,
                            alertName);
                    }
                },
                "AlertMessage": {
                    caption: "<%= Resources.CoreWebContent.Resource_ActiveAlerts_Message %>"
                },
                "Icon": {
                    caption: "",
                    isHtml: true,
                    formatter: function(cellValue, rowArray) {                        
					if (rowArray[3])
                        return SW.Core.String.Format('<img src="/NetPerfMon/images/Vendors/{0}" title="{1}">',
						rowArray[3],
						rowArray[10]);
						else
						return "&nbsp;";
						
                    }
                },
                "TriggeringObject": {
                    caption: "<%= Resources.CoreWebContent.Resource_ActiveAlerts_TriggeringObject %>",
                    isHtml: true,
                    formatter: function(cellValue, rowArray) {                        
                        return SW.Core.String.Format('<a href="{0}" >{1}</a>',
                            rowArray[5],
                            cellValue);
                    }
                },
                "ActiveTime": {
                    caption: "<%= Resources.CoreWebContent.Resource_ActiveAlerts_ActiveTime %>"
                }<% if (ShowAcknowledgedAlerts) { %>
                ,
                "Notes": {
                    caption: "<%= Resources.CoreWebContent.Resource_ActiveAlerts_Notes %>",
                    isHtml: true,
                    formatter: function (cellValue) {
                        var note = cellValue;
                        if (note.length >= 10) {
                            note = SW.Core.String.Format('<%= Resources.CoreWebContent.Resource_ActiveAlerts_Notes_Ellipses %>', note.substring(0, 6));
                            return SW.Core.String.Format('<span title="{0}" >{1}</span>', cellValue, note);
                        }
                        else
                            return cellValue;
                    }
                },
                "AcknowledgedByFullName": {
                    caption: "<%= Resources.CoreWebContent.Resource_ActiveAlerts_AckdBy %>",
                    isHtml: true,
                    formatter: function(cellValue) {
                        if ((cellValue !== null) && (cellValue !== ""))
                            return SW.Core.String.Format('<img src="/Orion/images/ActiveAlerts/Acknowliedged_icon16x16v1.png" /><span style="vertical-align: super">{0}</span>', cellValue);
                        else return "";
                    }
                }
                <% } %>
            }
        });

        var refresh = function () { SW.Core.Resources.ActiveAlerts.SortablePageableTable.refresh(<%= ResourceID %>); };
        SW.Core.View.AddOnRefresh(refresh, '<%= SortablePageableTable.ClientID %>');
        refresh();
    });
</script>
