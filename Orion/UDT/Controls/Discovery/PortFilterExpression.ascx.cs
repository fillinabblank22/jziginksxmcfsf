﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.UDT.Common.Discovery.Filtering.Expressions;
using Newtonsoft.Json;

public partial class Orion_UDT_Controls_Discovery_PortFilterExpression : UserControl
{
    public List<Expression> Expressions
    {
        get { return JsonConvert.DeserializeObject<List<Expression>>(PortSerializedExpression.Value); }
        set { PortSerializedExpression.Value = JsonConvert.SerializeObject(value); }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        ScriptManager.RegisterStartupScript(this, GetType(), ClientID + "Startup", "$(function() { initPortFilter('" + ClientID + "');}); ", true);
        ScriptManager.RegisterOnSubmitStatement(this, GetType(), ClientID + "Submit", "$('#" + ClientID + "').data('store_expression_callback')();");
    }
}