﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ImportPorts.ascx.cs" Inherits="Orion_UDT_Controls_Discovery_ImportPorts" %>
<%@ Register Src="~/Orion/UDT/Controls/Discovery/PortFilterExpression.ascx" TagPrefix="orion" TagName="PortFilterExpression" %>

<orion:Include runat="server" File="../Orion/UDT/Controls/Discovery/PortFiltering.css" />

<div id="PortPropertiesControls">
    <table class="port-table">
        <tr>
            <th class="status"><%=(Resources.UDTWebContent.DiscoveryPortFilter_Status)%></th>
            <th class="port"><%=(Resources.UDTWebContent.DiscoveryPortFilter_PortMode)%></th>
            <th class="hardware"><%=(Resources.UDTWebContent.DiscoveryPortFilter_Hardware)%></th>
        </tr>
        <tr>
            <td><asp:CheckBoxList runat="server" ID="PortStatusCheckboxes" RepeatLayout="UnorderedList" CssClass="port-checkboxes" /></td>
            <td><asp:CheckBoxList runat="server" ID="PortModeCheckboxes" RepeatLayout="UnorderedList" CssClass="port-checkboxes" /></td>
            <td><asp:CheckBoxList runat="server" ID="PortHardwareCheckboxes" RepeatLayout="UnorderedList" CssClass="port-checkboxes" /></td>
        </tr>
    </table>

    <orion:PortFilterExpression ID="PortFilterExpression" runat="server" />
</div>