﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.Interfaces.Common.Enums;
using SolarWinds.Orion.Core.Models.Enums;
using SolarWinds.UDT.Common.Discovery.Filtering.Expressions;

public partial class Orion_UDT_Controls_Discovery_ImportPorts : System.Web.UI.UserControl
{
    private const string IfStatusIconFormat = "<img src=\"/NetPerfMon/images/small-{0}.gif\" />";

    public CheckBoxList PortStatusCheckboxList
    {
        get { return PortStatusCheckboxes; }
        set { PortStatusCheckboxes = value; }
    }

    public CheckBoxList PortModeCheckboxList
    {
        get { return PortModeCheckboxes; }
        set { PortModeCheckboxes = value; }
    }

    public CheckBoxList PortHardwareCheckboxList
    {
        get { return PortHardwareCheckboxes; }
        set { PortHardwareCheckboxes = value; }
    }
    public List<Expression> Expressions
    {
        get { return PortFilterExpression.Expressions; }
        set { PortFilterExpression.Expressions = value; }
    }

    private Dictionary<string, IfAutoImportStatus> IfStatusCollection = new Dictionary<string, IfAutoImportStatus>()
        {
            { Resources.UDTWebContent.DiscoveryPortFilter_OperationallyUp, IfAutoImportStatus.Up },
            { Resources.UDTWebContent.DiscoveryPortFilter_OperationallyDown, IfAutoImportStatus.Down },
            { Resources.UDTWebContent.DiscoveryPortFilter_AdministrativelyShutdown, IfAutoImportStatus.Shutdown }
        };

    private Dictionary<string, VlanPortType> PortModeCollection = new Dictionary<string, VlanPortType>()
        {
            { Resources.UDTWebContent.DiscoveryPortFilter_Trunk, VlanPortType.Trunk },
            { Resources.UDTWebContent.DiscoveryPortFilter_Access, VlanPortType.Access},
            { Resources.UDTWebContent.DiscoveryPortFilter_PortUnknown, VlanPortType.Unknown}
        };

    private Dictionary<string, bool?> HardwateTypeCollection = new Dictionary<string, bool?>()
        {
            { Resources.UDTWebContent.DiscoveryPortFilter_Physical, false },
            { Resources.UDTWebContent.DiscoveryPortFilter_Virtual, true },
            { Resources.UDTWebContent.DiscoveryPortFilter_HardwareUnknown, null }
        };

    protected void Page_Load(object sender, EventArgs e)
    {
        EnsureDataBound();
    }

    public void EnsureDataBound()
    {
        if (PortStatusCheckboxes.Items.Count == 0)
        {
            PortStatusCheckboxes.DataTextField = "DisplayName";
            PortStatusCheckboxes.DataValueField = "Value";
            PortStatusCheckboxes.DataSource = IfStatusCollection.Select(status => new
            {
                DisplayName = string.Format(IfStatusIconFormat, status.Value.ToString()) + status.Key,
                Value = (int)status.Value
            });
            PortStatusCheckboxes.DataBind();
        }
        if (PortModeCheckboxes.Items.Count == 0)
        {
            PortModeCheckboxes.DataTextField = "DisplayName";
            PortModeCheckboxes.DataValueField = "Value";
            PortModeCheckboxes.DataSource = PortModeCollection.Select(port => new { DisplayName = port.Key, Value = (int)port.Value });
            PortModeCheckboxes.DataBind();
        }
        if (PortHardwareCheckboxes.Items.Count == 0)
        {
            PortHardwareCheckboxes.DataTextField = "DisplayName";
            PortHardwareCheckboxes.DataValueField = "Value";
            PortHardwareCheckboxes.DataSource = HardwateTypeCollection.Select(hw => new { DisplayName = hw.Key, Value = (bool?)hw.Value });
            PortHardwareCheckboxes.DataBind();
        }
    }
}