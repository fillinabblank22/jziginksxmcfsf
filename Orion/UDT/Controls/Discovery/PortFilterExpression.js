﻿function initPortFilter(controlID) {
    var PortRegexNum = 0; // # of regex operator selected, this is needed for hiding ninja tooltip

    // converts expressions to JSON
    function GetPortExpressions() {
        var portExpressions = new Array();

        $.each($("#PortAdvancedOptions:not(.hidden) .portExpression"), function (i, o) {
            portExpressions.push({
                __type: 'PortFiltering+Expression',
                Prop: $(".property", o).val(),
                Op: $(".operator:not(.hidden)", o).val(),
                Val: escape($(".value", o).val())
            });
        });

        return JSON.stringify(portExpressions);
    }

    function Load(initialState) {
        var expr = JSON.parse(initialState || '[]');
        if (!expr || expr.length == 0) {
            // prefill first expression (must be done here to know about VLANs
            var portFilledTemplate = _.template($('#PortExpressionTemplate').html(), '{}');
            $("#PortSelectedKeywords").html(portFilledTemplate);
            $("#PortSelectedKeywords .portExpressionCell.add").removeClass('hidden');
        }
        else {
            for (var i in expr) {
                AddPortExpression();
                var exprhtml = $("#PortSelectedKeywords .portExpression:last");
                var prop = $(".property", exprhtml);
                prop.val(expr[i].Prop);
                $(".operator:not(.hidden)", exprhtml).val(expr[i].Op);
                $(".value", exprhtml).val(unescape(expr[i].Val));
            }
            ShowPortAdvancedOptions();
        }
    }

    function ShowPortAdvancedOptions() {
        $("#PortAdvancedOptions").removeClass("hidden");
        $("#ShowPortAdvancedOptions").addClass("hidden");
    }

    function AddPortExpression() {
        var template = $('#PortExpressionTemplate').html();
        var portFilledTemplate = _.template(template, '{}');

        // add nex expression at the end
        if ($("#PortSelectedKeywords .portExpression:last").length == 0) {
            $("#PortSelectedKeywords").html(portFilledTemplate);
        }
        else {
            $("#PortSelectedKeywords .portExpression:last").after(portFilledTemplate);
        }

        $("#PortSelectedKeywords .portExpressionCell.add:not(.hidden)").addClass('hidden');
        $("#PortSelectedKeywords .portExpression:last .portExpressionCell.add").removeClass('hidden');
    }

    function RemovePortExpression() {
        if ($("#PortSelectedKeywords .portExpression").length == 1) {
            // there is only one expression, do not delete it, rather hide advanced options

            $("#PortAdvancedOptions").addClass("hidden");
            $("#ShowPortAdvancedOptions").removeClass("hidden");
        }
        else {
            // hide ninja of removing last regex row
            if ($(".operator", $(this).parent()).hasClass('reg')) {
                PortRegexNum--;
                if (PortRegexNum == 0)
                    $("#PortExpressionNinja").addClass('hidden');
            }
            $(this).parent().remove();
            // make sure last expression has add button visible
            $("#PortSelectedKeywords .portExpression:last .portExpressionCell.add").removeClass('hidden');
        }
    }

    function PortExpressionOperatorChange() {
        if ($(this).val() == 'regex' || $(this).val() == '!regex') {
            if (!$(this).hasClass('reg')) {
                $(this).addClass('reg');
                PortRegexNum++;
            }
            $("#PortExpressionNinja").removeClass('hidden');
        } else {
            if ($(this).hasClass('reg')) {
                $(this).removeClass('reg');
                PortRegexNum--;
                if (PortRegexNum == 0)
                    $("#PortExpressionNinja").addClass('hidden');
            }
        }
    }

    // called when page is loaded

    $(document).on("click", "#ShowPortAdvancedOptions", ShowPortAdvancedOptions);
    $(document).on("change", "#PortSelectedKeywords .portExpression .operator", PortExpressionOperatorChange);
    $(document).on("click", "#PortSelectedKeywords .portExpressionCell.add", AddPortExpression);
    $(document).on("click", "#PortSelectedKeywords .portExpressionCell.remove", RemovePortExpression);

    var control = $('#' + controlID);

    initialState = $('.PortSerializedExpression', control).val();
    Load(initialState);

    control.data('store_expression_callback', function () {
        $('.PortSerializedExpression', control).val(GetPortExpressions());
    });

    control.data('get_expression_callback', function () {
        return GetPortExpressions();
    });

    // add ninja tooltip row
    var portFilledTemplate = _.template($('#PortExpressionNinjaTemplate').html(), '{}');
    $("#PortSelectedKeywords .portExpression:last").after(portFilledTemplate);
}