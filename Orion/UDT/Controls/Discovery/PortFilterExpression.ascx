﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PortFilterExpression.ascx.cs" Inherits="Orion_UDT_Controls_Discovery_PortFilterExpression" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>

<orion:Include runat="server" File="UDT/Controls/Discovery/PortFilterExpression.js" />
<orion:Include runat="server" File="UDT/Controls/Discovery/PortFiltering.css" />

<script id="PortExpressionTemplate" type="text/x-template">
    <div class="portExpression">
        <span class="portExpressionCell">
            <select class="property">
                <option value="node_name"><%=(Resources.UDTWebContent.DiscoveryPortFilterExpression_NodeName)%></option>
                <option value="node_description"><%=(Resources.UDTWebContent.DiscoveryPortFilterExpression_NodeDescription)%></option>
                <option value="node_vendor"><%=(Resources.UDTWebContent.DiscoveryPortFilterExpression_NodeVendor)%></option>
                <option value="node_machine_type"><%=(Resources.UDTWebContent.DiscoveryPortFilterExpression_NodeMachineType)%></option>
                <option value="port_name"><%=(Resources.UDTWebContent.DiscoveryPortFilterExpression_PortName)%></option>
                <option value="port_description"><%=(Resources.UDTWebContent.DiscoveryPortFilterExpression_PortDescription)%></option>
            </select>
        </span>
        <span class="portExpressionCell">
            <select class="operator">                
                <option value="all"><%=(Resources.UDTWebContent.DiscoveryPortFilterExpression_All)%></option>
                <option value="!all"><%=(Resources.UDTWebContent.DiscoveryPortFilterExpression_NotAll)%></option>
                <option value="any"><%=(Resources.UDTWebContent.DiscoveryPortFilterExpression_Any)%></option>
                <option value="!any"><%=(Resources.UDTWebContent.DiscoveryPortFilterExpression_NotAny)%></option>
                <option value="equals"><%=(Resources.UDTWebContent.DiscoveryPortFilterExpression_Equal)%></option>
                <option value="!equals"><%=(Resources.UDTWebContent.DiscoveryPortFilterExpression_NotEqual)%></option>
                <option value="regex"><%=(Resources.UDTWebContent.DiscoveryPortFilterExpression_Regex)%></option>
                <option value="!regex"><%=(Resources.UDTWebContent.DiscoveryPortFilterExpression_NotRegex)%></option>
            </select>
        </span>
        <span class="portExpressionCell">
            <input type="text" class="value" onkeypress="return (event.keyCode != 13)">
        </span>
        <span class="portExpressionCell remove">
            <img src="/Orion/InterfacesDiscovery/Images/DiscoveryWizard/Button.Remove.png">
        </span>
        <span class="portExpressionCell add hidden">
            <img src="/Orion/InterfacesDiscovery/Images/DiscoveryWizard/Button.Add.png">
        </span>
    </div>
</script>

<script id="PortExpressionNinjaTemplate" type="text/x-template">
    <div id="PortExpressionNinja" class="hidden link">
        <img src="/Orion/InterfacesDiscovery/Images/DiscoveryWizard/Icon.Ninja.png"> 
        <small>
        <orion:HelpLink ID="ExpressionsHelpLink" runat="server" HelpUrlFragment="OrionCoreAGRegularExpressions"
        HelpDescription="<%$ Resources: InterfacesDiscoveryWebContent, INTERFACESFILTERING_MP0_62%>" CssClass="" />
        </small>
    </div>
</script>

<div id="<%= this.ClientID %>" class="FilterExpression">
    <div id="PortAdvancedOptions" class="hidden">
        <br />
        <span class="ButtonCollapse" style="display: none;">&nbsp;</span>
        <label><%=(Resources.UDTWebContent.DiscoveryPortFilterExpression_AdvancedFilteringOptions)%></label>
        <div id="PortSelectedKeywords" class="panel"></div>
    </div>
    <br />
    <div>
        <span id="ShowPortAdvancedOptions" class="link"><%=(Resources.UDTWebContent.DiscoveryPortFilterExpression_AdvancedSelectionOptions)%></span>
    </div>
    <input type="hidden" runat="server" id="PortSerializedExpression" class="PortSerializedExpression" />
</div>
