﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Web.Resources;

public partial class Orion_UDT_Controls_ResourcePager : System.Web.UI.UserControl
{
    private IUDTBaseResourceControl _resorceControl;
    private const string ImgFolder = "~/Orion/js/extjs/3.4/resources/images/default/grid/";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        StatusBarModeShowRefresh = UDTSettingDAL.ResourceStatusBarShowRefresh;
       
    }

    protected void Page_Load(object sender, EventArgs e)
    {
       
        _resorceControl = BaseResourceControl.GetInterfaceInstance<IUDTBaseResourceControl>(Parent);
        if (_resorceControl != null) 
            _resorceControl.RenderingDone +=
                (o, ev) =>
                    {
                        var rc = _resorceControl;

                        if (StatusBarMode)
                        {
                            btnFirst.Visible = false;
                            btnNext.Visible = false;
                            btnPrev.Visible = false;
                            btnViewAll.Visible = false;
                            btnLast.Visible = false;
                            
                            btnRefresh.ImageUrl = ImgFolder + (StatusBarModeShowRefresh ? "refresh.gif" : "refresh-disabled.gif");
                            btnRefresh.Enabled = !IsPostBack || StatusBarModeShowRefresh;
                            btnRefresh.Visible = !IsPostBack || StatusBarModeShowRefresh;

                            lblDispaly.Text = rc.StatusText;
                            return;
                        }

                        var navBackwardEnabled = rc.PageNumber > 1 && !rc.ShowAll;
                        var navForwardEnabled = (rc.Offset < rc.TotalRows - rc.VisibleRows) && !rc.ShowAll;
                        
                        if (navBackwardEnabled)
                        {
                            btnFirst.ImageUrl = ImgFolder + "page-first.gif";
                            btnFirst.Enabled = true;
                            btnFirst.Style.Add("cursor", "pointer");
                            btnPrev.ImageUrl = ImgFolder + "page-prev.gif";
                            btnPrev.Enabled = true;
                            btnPrev.Style.Add("cursor", "pointer");
                        }
                        else
                        {
                            btnFirst.ImageUrl = ImgFolder + "page-first-disabled.gif";
                            btnFirst.Enabled = false;
                            btnFirst.Style.Add("cursor", "default");
                            btnPrev.ImageUrl = ImgFolder + "page-prev-disabled.gif";
                            btnPrev.Enabled = false;
                            btnPrev.Style.Add("cursor", "default");
                        }

                        if (navForwardEnabled)
                        {
                            btnNext.ImageUrl = ImgFolder + "page-next.gif";
                            btnNext.Enabled = true;
                            btnNext.Style.Add("cursor", "pointer");
                            btnLast.ImageUrl = ImgFolder + "page-last.gif";
                            btnLast.Enabled = true;
                            btnLast.Style.Add("cursor", "pointer");
                        }
                        else
                        {
                            btnNext.ImageUrl = ImgFolder + "page-next-disabled.gif";
                            btnNext.Enabled = false;
                            btnNext.Style.Add("cursor", "default");
                            btnLast.ImageUrl = ImgFolder + "page-last-disabled.gif";
                            btnLast.Enabled = false;
                            btnLast.Style.Add("cursor", "default");
                        }

                        var viewAllEnabled = rc.TotalRows >= rc.VisibleRows && rc.TotalRows > rc.PageSize;
                        if (viewAllEnabled)
                        {
                            btnViewAll.Attributes.Clear();
                            btnViewAll.CssClass = "ReportHeader";
                            btnViewAll.Attributes.Add("style", "margin: 0px; vertical-align:middle; padding: 0px 5px 1px 0px");
                            btnViewAll.Style.Add("cursor", "pointer");
                            btnViewAll.Style.Add("color", "black");
                            btnViewAll.Text = rc.ShowAll ? string.Format(Resources.UDTWebContent.UDTWCODE_VB1_46, rc.PageSize) : Resources.UDTWebContent.UDTWEBDATA_VB1_130;    
                        }
                        else
                        {
                            btnViewAll.Attributes.Add("disabled", "disabled");
                            btnViewAll.Style.Add("cursor", "default");
                            btnViewAll.Style.Add("color", "gray");
                            btnViewAll.Text = Resources.UDTWebContent.UDTWEBDATA_VB1_130;
                        }

                        btnRefresh.ImageUrl = ImgFolder + "refresh.gif";

                        lblDispaly.Text = rc.StatusText ;
                    };
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        string pagerRefreshJavascriptfunction = string.Format("ShowProgress_UDTPager{0}()", HostResourceID);
            btnViewAll.Attributes.Add("onclick",pagerRefreshJavascriptfunction);
            btnFirst.Attributes.Add("onclick",pagerRefreshJavascriptfunction);
            btnPrev.Attributes.Add("onclick",pagerRefreshJavascriptfunction);
            btnNext.Attributes.Add("onclick",pagerRefreshJavascriptfunction);
            btnLast.Attributes.Add("onclick",pagerRefreshJavascriptfunction);
            btnRefresh.Attributes.Add("onclick",pagerRefreshJavascriptfunction);
        
    }
    public bool Hidden { get; set; }

    public bool StatusBarMode { get; set; }

    public bool StatusBarModeShowRefresh { get; set; }

    protected string VisiblityMode { get { return Hidden ? "none" : "block"; } }

    protected string CellMode { get { return StatusBarMode ? "; display: none" : ""; } }

    protected string HostResourceUID
    {
        get { return _resorceControl == null ? UDTBaseResourceControl.DefaultResourceUID : _resorceControl.ResourceUID; }
    }

    protected string HostResourceID
    {
        get { return _resorceControl == null ? "" : _resorceControl.ResourceID; }
    }

    protected void btnFirst_Click(object sender, EventArgs e)
    {
        if (_resorceControl != null)
            _resorceControl.PageNumber = 1;
    }

    protected void btnPrev_Click(object sender, EventArgs e)
    {
        if (_resorceControl != null)
            _resorceControl.PageNumber = _resorceControl.PageNumber - 1;
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        if (_resorceControl != null)
            _resorceControl.PageNumber = _resorceControl.PageNumber + 1;
    }

    protected void btnLast_Click(object sender, EventArgs e)
    {
        if (_resorceControl != null)
            _resorceControl.PageNumber = _resorceControl.TotalRows/_resorceControl.PageSize + 1;
    }

    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        if (_resorceControl != null)
            _resorceControl.ReloadData = true;
    }

    protected void btnViewAll_Click(object sender, EventArgs e)
    {
        if (_resorceControl != null)
            _resorceControl.ShowAll = !_resorceControl.ShowAll;
    }
}