﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_UDT_Controls_ChartImage : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        image.ImageUrl = URL;
    }

    public string URL
    {
        get;
        set;
    }
}