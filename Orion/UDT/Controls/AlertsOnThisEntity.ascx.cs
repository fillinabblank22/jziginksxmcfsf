﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.Controllers.Alerts.Resources;

public partial class Orion_UDT_Controls_AlertsOnThisEntity : System.Web.UI.UserControl
{
	public int ResourceID {get; set;}

	public int RelatedNodeId { get; set; }
	public string RelatedNodeEntityUri { get; set; }

	public List<string> TriggeringObjectEntityUris { get; set; }
	public List<string> TriggeringObjectEntityNames { get; set; }

	public bool ShowAcknowledgedAlerts { get; set; }
	public int InitialRowsPerPage { get; set; }

	public Orion_UDT_Controls_AlertsOnThisEntity()
	{
		InitialRowsPerPage = 5;
        TriggeringObjectEntityNames = new List<string>(){"Orion.UDT.RogueIPAlert", "Orion.UDT.RogueEmptyDNSAlert", "Orion.UDT.MovedMACAlert", "Orion.UDT.NewMACVendorAlert", "Orion.UDT.AccessPortEndpointCount", "Orion.UDT.RogueDNSAlert", "Orion.UDT.DNSNameCurrent", "Orion.UDT.NewMACAlert", "Orion.UDT.RogueMACAlert", "Orion.UDT.WatchListPresent"};
		TriggeringObjectEntityUris = new List<string>();
	}

    protected void Page_Load(object sender, EventArgs e)
    {
		
		SortablePageableTable.UniqueClientID = ResourceID;
    }
}