﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.UDT.Web.Resources;

public partial class Orion_UDT_Controls_ResourceSearcher : System.Web.UI.UserControl
{
    protected static readonly string DefaultEmptyText = Resources.UDTWebContent.UDTWEBDATA_VB1_144;

    private IUDTBaseResourceControl _resorceControl;

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchTextBox.Attributes.Add("onKeyDown", "KeyDownHandler(event, '" + SearchButton.ClientID + "')");
        
        _resorceControl = BaseResourceControl.GetInterfaceInstance<IUDTBaseResourceControl>(Parent);
        

        if (_resorceControl != null)
            _resorceControl.RenderingDone +=
                (o, ea) =>
                {
                    if (!string .IsNullOrEmpty(_resorceControl.SearchText))
                    {
                        SearchTextBox.Text = _resorceControl.SearchText;
                        ClearButton.Visible = true;
                        SearchTextBox.Style.Add("Color", "Black");
                    }
                    else
                    {
                        SearchTextBox.Text = DefaultEmptyText;
                        SearchTextBox.Style.Add("Color", "Gray");
                        ClearButton.Visible = false;
                    }

                    if (_resorceControl.TotalRows == 0 && string.IsNullOrEmpty(_resorceControl.SearchText))
                        Hidden = true;
                };
    }

    public bool Hidden { get; set; }

    protected string VisiblityMode { get { return Hidden ? "none" : "block"; } }

    protected string HostResourceID
    {
        get { return _resorceControl == null ? UDTBaseResourceControl.DefaultResourceUID : _resorceControl.ResourceUID; }
    }

    protected void SearchButton_Click(object sender, ImageClickEventArgs e)
    {
        if (SearchTextBox.Text == DefaultEmptyText)
            return;

        if (_resorceControl != null)
            _resorceControl.SearchText = SearchTextBox.Text;
    }

    protected void ClearButton_Click(object sender, ImageClickEventArgs e)
    {
        if (_resorceControl != null)
            _resorceControl.SearchText = string.Empty;
    }
}