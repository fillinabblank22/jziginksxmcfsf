﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchBoxEx.ascx.cs" Inherits="Orion_UDT_Controls_SearchBoxEx" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>

<orion:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false" Version="3.4"/>
<script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
<script type="text/javascript" src="/Orion/UDT/Controls/js/SearchBoxEx.js"></script>

<%--<style type="text/css">
    .x-tool { float: right; }
</style>   --%>

    <input type="hidden" name="UDT_SearchResultsEx_SearchItemListEx" id="UDT_SearchResultsEx_SearchItemListEx" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("UDT_SearchResultsEx_SearchItemListEx")%>' />

    <table class=".searchBox" id="searchControl" cellpadding="0" cellspacing="0">
	    <tr valign="top">
		    <td></td>
            <td id="searchboxCell" style="padding: 5px 0px 5px 0px" align="left">
              
                    <div id="searchbox-panel" onkeypress="if (event.keyCode==13) {return false;}" />
                    <div id="searchbox-multiselect-panel" />

		    </td>
	    </tr>
    </table>

    <script type="text/javascript">
//    Ext.onReady(function () {
        var panel = new Ext.Panel({
            applyTo: 'searchbox-panel',
            //height: 30,
            autoScroll: false,
            autoHeight: true,

            items:
                new Ext.ux.form.SearchBoxEx({
                width: 250
            })
        });
//    });
   </script>
