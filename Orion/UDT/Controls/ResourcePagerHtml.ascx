﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResourcePagerHtml.ascx.cs" Inherits="Orion_UDT_Controls_ResourcePagerHtml" %>



<div style="width: 100%; display: <%=VisiblityMode%>" id="udtResourcePager-<%=ResourceId%>">
    <style type="text/css">
        .pager-sep {
            width: 4px;
            height: 16px;
            overflow: hidden;
            margin: 0px 2px 0px 2px;
            background-repeat: no-repeat;
            background-position: center;
            cursor: default;
            display: block;
            background-image: url("/Orion/js/extjs/3.4/resources/images/default/grid/grid-split.gif");
        }
        .pager-no-border-bottom {
            border-bottom: none !important
        }
        .disabledButton {
            cursor: default;
        }
        .placeholder {
            color: gray;
        }
        .errorMsg {
            margin-top: 10px;
        }
    </style>
    <div style="width: 100%">
        <table style="width: 100%; border-collapse: collapse">
            <tr>
                <td class="ReportHeader pager-no-border-bottom" style="width: 5px">
                </td>
                <td class="ReportHeader pager-no-border-bottom" colspan="1" style="width: 20px">
                    <a class="btnFirst"><img src="/Orion/js/extjs/3.4/resources/images/default/grid/page-first-disabled.gif" style="vertical-align:middle;"/></a>
                </td>
                <td class="ReportHeader pager-no-border-bottom" colspan="1" style="width: 20px" >
                    <a class="btnPrev"><img src="/Orion/js/extjs/3.4/resources/images/default/grid/page-prev-disabled.gif" style="vertical-align:middle;"/></a>
                </td>
                <td  class="ReportHeader pager-no-border-bottom" colspan="1"  style="width: 10px"  >
                    <span class="pager-sep" />
                </td>
                <td class="ReportHeader pager-no-border-bottom" colspan="1" style="width: 20px" >
                    <a class="btnNext"><img  src="/Orion/js/extjs/3.4/resources/images/default/grid/page-next-disabled.gif" style="vertical-align:middle;"/></a>
                </td>
                <td class="ReportHeader pager-no-border-bottom" colspan="1" style="width: 20px" >
                    <a class="btnLast"><img  src="/Orion/js/extjs/3.4/resources/images/default/grid/page-last-disabled.gif" style="vertical-align:middle;"/></a>
                </td>
                <td class="ReportHeader pager-no-border-bottom" colspan="1"  style="width: 10px" >
                    <span class="pager-sep" />
                </td>
                <td class="ReportHeader pager-no-border-bottom" colspan="1"  style="width: 20px" >
                    <a class="btnRefresh"><img src="/Orion/js/extjs/3.4/resources/images/default/grid/refresh.gif" style="vertical-align:middle;"/></a>
                </td>
                <td class="ReportHeader pager-no-border-bottom">
                </td>
                <td class="ReportHeader pager-no-border-bottom" align ="right" >
                    <span class="displayText"></span>
                </td>
                <td class="ReportHeader pager-no-border-bottom" colspan="1"  style="width: 6px" align="right">
                    <span class="pager-sep" />
                </td>
                <td class="ReportHeader pager-no-border-bottom" align ="right" style="width: 40px" >
                    <a href="" class="ReportHeader btnViewAll" style="margin: 0px; vertical-align:middle; padding: 0px 5px 1px 0px; border-top-width: 0px; white-space: nowrap" ><%= Resources.UDTWebContent.UDTWEBDATA_VB1_130%></a>
                </td>
            </tr>
        </table>         
    </div>    
        
</div>

<div class='errorMsg sw-suggestion sw-suggestion-fail' style="display: none;"><span class='sw-suggestion-icon'></span><span class="errorMsgText"></span></div>
