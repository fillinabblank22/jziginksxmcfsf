﻿<%@ Control Language="C#" ClassName="UdtLearnMoreLink" %>

<script runat="server">
    private const string _helpTemplate = "{0}/documentation/helploader.aspx?topic={1}.htm";

    public string LearnMoreUrlFragment { get; set; }

    protected string LearnMoreUrl
    {
        get
        {
            return string.Format(_helpTemplate, SolarWinds.Orion.Web.DAL.WebSettingsDAL.HelpServer, LearnMoreUrlFragment);
        }
    }
</script>

<font class="learnMoreLink"><a href="<%=LearnMoreUrl%>" target="_blank"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_22 %></a></font>