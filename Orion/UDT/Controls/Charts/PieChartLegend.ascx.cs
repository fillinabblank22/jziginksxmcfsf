﻿using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.UDT.Web.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_UDT_Controls_Charts_PieChartLegend : System.Web.UI.UserControl, IChartLegendControl
{
	public string LegendInitializer { get { return "udt_chartLegendInitializer__" + legend.ClientID; } }
    public int VendorPieChartSliceCount
    {
        get
        {
           return UDTSettingDAL.VendorPieChartSliceCount;
        }
    }
}