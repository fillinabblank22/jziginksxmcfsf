﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PieChartLegend.ascx.cs" Inherits="Orion_UDT_Controls_Charts_PieChartLegend" %>
<orion:Include ID="Include1" runat="server" File="UDT/js/Charts/Charts.UDT.PieChartLegend.js" />

<script type="text/javascript">
	SW.Core.Charts.Legend.udt_chartLegendInitializer__<%= legend.ClientID %> = function (chart, dataUrlParameters) {
		$('#<%= legend.ClientID %>').empty();
	    SW.UDT.Charts.PieChartLegend.createLegend(chart, dataUrlParameters, '<%= legend.ClientID %>',' <%=  VendorPieChartSliceCount %>');
	};
</script>

<div id="UDTPieLegend" class="udt_PieChartLegend" style="display:-ms-flexbox;-ms-flex-align:center;display:-moz-box;-moz-box-align:center;display:-webkit-box;-webkit-box-align:center;display:box;box-align:center;" runat="server">
    <table runat="server" id="legend" class="chartLegend"></table>
</div>