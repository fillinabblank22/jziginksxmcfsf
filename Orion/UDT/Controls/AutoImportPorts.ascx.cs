﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.Interfaces.Common.Enums;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Models.Enums;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Web.Discovery;
using SolarWinds.UDT.Common.Models.Discovery;

public partial class Orion_UDT_Controls_AutoImportPorts : AutoImportWizardStep
{
    private static Log log = new Log();

    public override string Name
    {
        get
        {
            return Resources.UDTWebContent.UDTWEBDATA_VB1_220;
        }
    }

    protected UDTDiscoveryPluginConfiguration Config
    {
        get
        {
            var config = ConfigWorkflowHelper.DiscoveryConfigurationInfo.GetDiscoveryPluginConfiguration<UDTDiscoveryPluginConfiguration>();
            if (config == null)
            {
                config = new UDTDiscoveryPluginConfiguration();
                ConfigWorkflowHelper.DiscoveryConfigurationInfo.AddDiscoveryPluginConfiguration(config);
            }
            return config;
        }
    }

    public override void LoadData()
    {
        ImportPorts.Checked = Config.AutoImportPorts;

        ImportPortsFilter.EnsureDataBound();

        Config.SetDefaults();

        foreach (ListItem item in ImportPortsFilter.PortStatusCheckboxList.Items)
        {
            item.Selected = Config.AutoImportStatus.Contains(AsStatus(item.Value));
        }

        foreach (ListItem item in ImportPortsFilter.PortModeCheckboxList.Items)
        {
            item.Selected = Config.AutoImportVlanPortTypes.Contains(AsPortMode(item.Value));
        }

        foreach (ListItem item in ImportPortsFilter.PortHardwareCheckboxList.Items)
        {
            item.Selected = Config.AutoImportVirtualTypes.Contains(AsHardwareType(item.Value));
        }

        this.ImportPortsFilter.Expressions = Config.Expressions;
    }

    public override void SaveData()
    {
        Config.AutoImportPorts = ImportPorts.Checked;

        ImportPortsFilter.EnsureDataBound();

        Config.AutoImportStatus = ImportPortsFilter.PortStatusCheckboxList.Items
            .OfType<ListItem>()
            .Where(item => item.Selected)
            .Select(item => AsStatus(item.Value)).ToList();

        Config.AutoImportVlanPortTypes = ImportPortsFilter.PortModeCheckboxList.Items
            .OfType<ListItem>()
            .Where(item => item.Selected)
            .Select(item => AsPortMode(item.Value)).ToList();

        Config.AutoImportVirtualTypes = ImportPortsFilter.PortHardwareCheckboxList.Items
            .OfType<ListItem>()
            .Where(item => item.Selected)
            .Select(item => AsHardwareType(item.Value)).ToList();

        Config.Expressions = this.ImportPortsFilter.Expressions;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ImportPortsFilter.EnsureDataBound();
    }

    private static IfAutoImportStatus AsStatus(string value)
    {
        return (IfAutoImportStatus)int.Parse(value);
    }

    private static VlanPortType AsPortMode(string value)
    {
        return (VlanPortType)int.Parse(value);
    }

    private static bool? AsHardwareType(string value)
    {
        return !string.IsNullOrWhiteSpace(value) ? (bool?)bool.Parse(value) : (bool?)null;
    }
}