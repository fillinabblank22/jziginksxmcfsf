﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AutoImportPorts.ascx.cs" Inherits="Orion_UDT_Controls_AutoImportPorts" %>
<%@ Register Src="~/Orion/UDT/Controls/Discovery/ImportPorts.ascx" TagPrefix="orion" TagName="ImportPortsControl" %>

<div style="padding: 5px 0 5px 0;">
    <asp:CheckBox runat="server" ID="ImportPorts" OnClick="ShowHidePortProperties(this.checked)" ClientIDMode="Static" Text="<%$ Resources: UDTWebContent, DiscoveryAutoImportPorts %>" />
</div>

<orion:ImportPortsControl runat="server" ID="ImportPortsFilter" />

<script language="javascript" type="text/javascript">
    function ShowHidePortProperties(checked) {
        document.getElementById('PortPropertiesControls').style.display = checked ? 'block' : 'none';
    }
    window.onload = ShowHidePortProperties(document.getElementById('ImportPorts').checked);
</script>