﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResourcePager.ascx.cs" Inherits="Orion_UDT_Controls_ResourcePager" %>

<div style="width: 100%; display: <%=VisiblityMode%>" id="udtResourcePager-<%=HostResourceUID%>">
<asp:UpdatePanel ID="PagerUpdatePanel" runat="server" UpdateMode="Conditional">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnFirst" />
        <asp:AsyncPostBackTrigger ControlID="btnPrev" />
        <asp:AsyncPostBackTrigger ControlID="btnNext" />
        <asp:AsyncPostBackTrigger ControlID="btnLast" />
        <asp:AsyncPostBackTrigger ControlID="btnRefresh" />
        <asp:AsyncPostBackTrigger ControlID="btnViewAll" />
    </Triggers>
    <ContentTemplate>
        <style type="text/css">
            .pager-sep {
                width: 4px;
                height: 16px;
                overflow: hidden;
                margin: 0px 2px 0px 2px;
                background-repeat: no-repeat;
                background-position: center;
                cursor: default;
                display: block;
                background-image: url("/Orion/js/extjs/3.4/resources/images/default/grid/grid-split.gif");
            }
            .pager-no-border-bottom {
                border-bottom: none !important
            }
        </style>
        <div style="width: 100%">
            <table style="width: 100%; border-collapse: collapse" id="udt-pager-<%=HostResourceID%>">
                <tr>
                    <td class="ReportHeader pager-no-border-bottom" style="width: 5px">
                    </td>
                    <td class="ReportHeader pager-no-border-bottom" colspan="1" style="width: 20px<%=CellMode%>">
                        <asp:ImageButton runat="server" ID="btnFirst" style="vertical-align:middle; cursor: default"
                            ImageUrl="~/Orion/js/extjs/3.4/resources/images/default/grid/page-first-disabled.gif"  OnClick="btnFirst_Click" CausesValidation="false"  />
                    </td>
                    <td class="ReportHeader pager-no-border-bottom" colspan="1" style="width: 20px<%=CellMode%>" >
                        <asp:ImageButton runat="server"  ID="btnPrev" style="vertical-align:middle; cursor: default"
                            ImageUrl="~/Orion/js/extjs/3.4/resources/images/default/grid/page-prev-disabled.gif"  OnClick="btnPrev_Click" CausesValidation="false" />
                    </td>
                    <td  class="ReportHeader pager-no-border-bottom" colspan="1"  style="width: 10px<%=CellMode%>"  >
                        <span class="pager-sep" />
                    </td>
                    <td class="ReportHeader pager-no-border-bottom" colspan="1" style="width: 20px<%=CellMode%>" >
                        <asp:ImageButton runat="server"  ID="btnNext" style="vertical-align:middle; cursor: default"
                            ImageUrl="~/Orion/js/extjs/3.4/resources/images/default/grid/page-next-disabled.gif" OnClick="btnNext_Click" CausesValidation="false" />
                    </td>
                    <td class="ReportHeader pager-no-border-bottom" colspan="1" style="width: 20px<%=CellMode%>" >
                        <asp:ImageButton runat="server" ID="btnLast" style="vertical-align:middle; cursor: default"
                            ImageUrl="~/Orion/js/extjs/3.4/resources/images/default/grid/page-last-disabled.gif"  OnClick="btnLast_Click" CausesValidation="false"  />
                    </td>
                    <td class="ReportHeader pager-no-border-bottom" colspan="1"  style="width: 10px<%=CellMode%>" >
                        <span class="pager-sep" />
                    </td>
                    <td class="ReportHeader pager-no-border-bottom" colspan="1"  style="width: 20px" >
                        <asp:ImageButton runat="server"  ID="btnRefresh" style="vertical-align:middle;"
                            ImageUrl="~/Orion/js/extjs/3.4/resources/images/default/grid/grid-loading.gif"   OnClick="btnRefresh_Click" CausesValidation="false" />
                    </td>
                    <td class="ReportHeader pager-no-border-bottom">
                    </td>
                    <td class="ReportHeader pager-no-border-bottom" align ="right" >
                        <asp:Label ID="lblDispaly" runat="server" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_129 %>" ></asp:Label>
                    </td>
                    <td class="ReportHeader pager-no-border-bottom" colspan="1"  style="width: 6px<%=CellMode%>" align="right">
                        <span class="pager-sep" />
                    </td>
                    <td class="ReportHeader pager-no-border-bottom" align ="right" style="width: 40px<%=CellMode%>" >
                        <asp:Button ID="btnViewAll" runat="server"
                            style="margin: 0px; vertical-align:middle; padding: 0px 5px 1px 0px" 
                            CausesValidation="false" BorderStyle="None" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_130 %>" 
                            class="ReportHeader" OnClick="btnViewAll_Click"  />
                    </td>
                </tr>
            </table>         
        </div>    
        <script type="text/javascript">
            var shouldLoad = true;

            $(document).ready(function () {
                if (shouldLoad) {
                    shouldLoad = false;
                    // start data loading on full page reload
                    $('#<%=btnRefresh.ClientID %>').click();
                }
            });
            
            function ShowProgress_UDTPager<%=HostResourceID%>() {
              $('#<%=btnRefresh.ClientID %>').attr('src', '/Orion/js/extjs/3.4/resources/images/default/grid/grid-loading.gif');
              $('#<%=lblDispaly.ClientID%>').text('<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEBDATA_VB1_57)%>');
            }
        </script>
    </ContentTemplate>
</asp:UpdatePanel> 
</div>