﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UDTNodeProperties.ascx.cs" Inherits="Orion_UDT_Controls_Polling_UDTNodeProperties" %>
<%@ Register Assembly="OrionWeb" Namespace="SolarWinds.Orion.Web.UI" TagPrefix="web" %>

<style type="text/css">
    .sw-validation-error { padding-left: 5px;}
</style>

<div class="contentBlock">
    <div class="contentBlockHeader">
        <asp:CheckBox ID="cbMultipleSelection" runat="server" AutoPostBack="True" OnCheckedChanged="cbMultipleSelection_CheckedChanged" />
        <!-- Replace text below with plugin Title -->
        <%= Resources.UDTWebContent.UDTWEBDATA_VB1_227 %>
    </div>

    <div class="blueBox" runat="server" id="UDTNodePropertiesBox">
    <div runat="server" id="UDTLayer2Box">
    <table>
        <!-- !!! For similar look and feel use classes defined below in example to decorate your elements -->
        <tr>
	        <td class="leftLabelColumn">
                <asp:Label runat="server" ID="lblLayer2CapableCol0">&nbsp;</asp:Label>
            </td>
	        <td class="rightInputColumn" nowrap="nowrap">
                <asp:CheckBox runat="server" ID="cbLayer2Capable"  AutoPostBack="True" />
	            <asp:Label runat="server" ID="lblLayer2Capable" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_228 %>"/>
	        </td>
	        <td class="helpfulText">&nbsp;</td>
        </tr>
        <tr>
	        <td class="leftLabelColumn">
	            <asp:Label runat="server" ID="lblLayer2PollingInterval" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_229 %>"/>
            </td>
	        <td class="rightInputColumn" nowrap="nowrap">
                <orion:ValidatedTextBox CustomStringValue="UDTSwitchPollingInterval"
                    Text="30" ID="txtLayer2PollingInterval" Columns="20"
                    Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                    MinValue="5" MaxValue="1200" ValidatorText="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_237 %>"/>
	            <asp:Label runat="server" ID="lblLayer2PollingIntervalCol2" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_AK1_30 %>"/> 
	        </td>
	        <td class="helpfulText">&nbsp;</td>
        </tr>
    </table>
    </div>
    <div  runat="server" id="UDTLayer3Box">
    <table>
        <tr>
	        <td class="leftLabelColumn">
                <asp:Label runat="server" ID="lblLayer3CapableCol0">&nbsp;</asp:Label>
            </td>
	        <td class="rightInputColumn" nowrap="nowrap">
                <asp:CheckBox runat="server" ID="cbLayer3Capable"  AutoPostBack="True" />
	            <asp:Label runat="server" ID="lblLayer3Capable" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_230 %>"/>
	        </td>
	        <td class="helpfulText">&nbsp;</td>
        </tr>
        <tr>
	        <td class="leftLabelColumn">
	            <asp:Label runat="server" ID="lblLayer3PollingInterval" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_231 %>"/>
            </td>
	        <td class="rightInputColumn" nowrap="nowrap">
                <orion:ValidatedTextBox CustomStringValue="UDTRouterPollingInterval"
                    Text="30" ID="txtLayer3PollingInterval" Columns="20"
                    Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                    MinValue="5" MaxValue="1200" ValidatorText="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_237 %>"/>
	            <asp:Label runat="server" ID="lblLayer3PollingIntervalCol2" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_AK1_30 %>"/>
	        </td>
	        <td class="helpfulText">&nbsp;</td>
        </tr>
           <tr>
	        <td class="leftLabelColumn">
                <asp:Label runat="server" ID="lblDisableVRFPollingCol0">&nbsp;</asp:Label>
            </td>
	        <td class="rightInputColumn" nowrap="nowrap">
                <asp:CheckBox runat="server" ID="cbDisableVRFPolling"  AutoPostBack="True" />
	            <asp:Label runat="server" ID="lblDisableVRFPolling" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_GP1_1 %>"/>
                <!-- <asp:Label runat="server" ID="Label1" Text="Resources : UDTWebContent, UDTWEBDATA_GP1_1 "/> -->
	        </td>
	        <td class="helpfulText">&nbsp;</td>
        </tr>
    </table>
    </div>
    </div>
</div>
