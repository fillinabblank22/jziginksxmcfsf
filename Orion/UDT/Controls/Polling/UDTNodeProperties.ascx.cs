﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.UDT.Common;
using SolarWinds.UDT.Common.Models;
using Node = SolarWinds.Orion.Core.Common.Models.Node;

public partial class Orion_UDT_Controls_Polling_UDTNodeProperties : UserControl, INodePropertyPlugin
{
    private static SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();
    private Dictionary<string, object> _propertyBag;
    private IList<Node> _nodes;
    private NodePropertyPluginExecutionMode _mode;
    private UDTCapability[] _capabilities;

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    /// <summary>
    /// Defines if plugin "Enable" checkbox is visible. Normally property is set from this.Initialize()
    /// </summary>
    public bool MultipleSelectionVisible
    {
        get { return cbMultipleSelection.Visible; }
        set { cbMultipleSelection.Visible = value; }
    }

    /// <summary>
    /// Defines if plugin is enabled to edit multiple items. Normally property is set from this.Initialize()
    /// </summary>
    public bool MultipleSelectionSelected
    {
        get { return cbMultipleSelection.Checked; }
        set
        {
            cbMultipleSelection.Checked = value;
            cbMultipleSelection_CheckedChanged(this, EventArgs.Empty);
        }
    }

    protected void cbMultipleSelection_CheckedChanged(object sender, EventArgs e)
    {
        // Enable/Disable controls depending on checked state
        lblLayer2CapableCol0.Enabled = cbMultipleSelection.Checked;
        cbLayer2Capable.Enabled = cbMultipleSelection.Checked;
        lblLayer2Capable.Enabled = cbMultipleSelection.Checked;

        lblLayer2CapableCol0.CssClass = cbMultipleSelection.Checked ? string.Empty : "disabled";
        cbLayer2Capable.CssClass = cbMultipleSelection.Checked ? string.Empty : "disabled";
        lblLayer2Capable.CssClass = cbMultipleSelection.Checked ? string.Empty : "disabled";


        lblLayer2PollingInterval.Enabled = cbMultipleSelection.Checked;
        txtLayer2PollingInterval.Enabled = cbMultipleSelection.Checked;
        lblLayer2PollingIntervalCol2.Enabled = cbMultipleSelection.Checked;

        lblLayer2PollingInterval.CssClass = cbMultipleSelection.Checked ? string.Empty : "disabled";
        txtLayer2PollingInterval.CssClass = cbMultipleSelection.Checked ? string.Empty : "disabled";
        lblLayer2PollingIntervalCol2.CssClass = cbMultipleSelection.Checked ? string.Empty : "disabled";


        lblLayer3CapableCol0.Enabled = cbMultipleSelection.Checked;
        cbLayer3Capable.Enabled = cbMultipleSelection.Checked;
        lblLayer3Capable.Enabled = cbMultipleSelection.Checked;

        lblLayer3CapableCol0.CssClass = cbMultipleSelection.Checked ? string.Empty : "disabled";
        cbLayer3Capable.CssClass = cbMultipleSelection.Checked ? string.Empty : "disabled";
        lblLayer3Capable.CssClass = cbMultipleSelection.Checked ? string.Empty : "disabled";


        lblLayer3PollingInterval.Enabled = cbMultipleSelection.Checked;
        txtLayer3PollingInterval.Enabled = cbMultipleSelection.Checked;
        lblLayer3PollingIntervalCol2.Enabled = cbMultipleSelection.Checked;

        lblLayer3PollingInterval.CssClass = cbMultipleSelection.Checked ? string.Empty : "disabled";
        txtLayer3PollingInterval.CssClass = cbMultipleSelection.Checked ? string.Empty : "disabled";
        lblLayer3PollingIntervalCol2.CssClass = cbMultipleSelection.Checked ? string.Empty : "disabled";

        lblDisableVRFPollingCol0.Enabled = cbMultipleSelection.Checked;
        lblDisableVRFPolling.Enabled = cbMultipleSelection.Checked;
        cbDisableVRFPolling.Enabled = cbMultipleSelection.Checked;

        lblDisableVRFPollingCol0.CssClass = cbMultipleSelection.Checked ? string.Empty : "disabled";
        lblDisableVRFPolling.CssClass = cbMultipleSelection.Checked ? string.Empty : "disabled";
        cbDisableVRFPolling.CssClass = cbMultipleSelection.Checked ? string.Empty : "disabled";
    }

    public bool IsLayer2
    {
        get { return !(_capabilities == null || _capabilities.Length == 0) && _capabilities.Any(c => c.Capability == UDTNodeCapability.Layer2); }
    }

    public bool IsLayer3
    {
        get { return !(_capabilities == null || _capabilities.Length == 0) && _capabilities.Any(c => c.Capability == UDTNodeCapability.Layer3); }
    }

    #region == INodePropertyPlugin Members ==
    /// <summary>
    /// NodePropertyPluginManager calls this function on every page lifecycle typically from page OnLoad().
    /// </summary>
    /// <param name="nodes">A collection of nodes the plugin may operate upon. Nodes collection is cached inside NodePropertyPluginManager and passed into plugin on every page lifecycle.</param>
    /// <param name="mode">Meantime corresponds to page which uses plugin.</param>
    /// <param name="pluginState">Property bag to store/load plugin properties between lifecycle sessions.</param>
    public void Initialize(IList<Node> nodes, NodePropertyPluginExecutionMode mode, Dictionary<string, object> pluginState)
    {
        // save localy passed args
        _propertyBag = pluginState;
        _nodes = nodes;
        _mode = mode;

        if (!IsPostBack)
        { //initial load

            if (nodes == null || nodes.Count <= 1)
            {
                MultipleSelectionVisible = false;
                MultipleSelectionSelected = true;
            }
            else MultipleSelectionSelected = false;

            LoadNodeCapabilites();
        }
        else
        {
            object value;
            _propertyBag.TryGetValue("UDTNodeCapabilities", out value);

            _capabilities = value as UDTCapability[];
        }

        //Set plugin visibility
        var visible = _capabilities != null && _capabilities.Length > 0;
        foreach (var plugin in NodePropertyPluginManager.Plugins.Where(p => p.Name == "UDTNodeProperties"))
        {
            plugin.Visible = visible;
        }
    }

    private void LoadNodeCapabilites()
    {
        if (_nodes == null || _nodes .Count == 0)
            return;

        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                var res = bl.PollingGetNodesCapabilities(_nodes.Select(n => n.Id).ToArray());
                
                if (res == null)
                    return;

                _capabilities = res
                    .Where(c => c.Capability == UDTNodeCapability.Layer2 || c.Capability == UDTNodeCapability.Layer3)
                    .ToArray();

                _propertyBag["UDTNodeCapabilities"] = _capabilities;
            }
        }
        catch (Exception ex)
        {
            _log.ErrorFormat("Exception in UDT LoadNodeCapabilities(). Details: {0}", ex);
            _capabilities = null;
            return;
        }

        // set UI properties
        if (IsLayer2)
        {
            cbLayer2Capable.Checked = _capabilities
                .Where(c => c.Capability == UDTNodeCapability.Layer2)
                .All(c => c.Enabled);

            txtLayer2PollingInterval.Text = _capabilities
                .Where(c => c.Capability == UDTNodeCapability.Layer2)
                .GroupBy(c => c.PollingIntervalMinutes)
                .Select(g => new { Count = g.Count(), Capability = g.First() })
                .OrderByDescending(item => item.Count)
                .Select(item => item.Capability)
                .First().PollingIntervalMinutes.ToString();
        }
        else UDTLayer2Box.Visible = false;

        if (IsLayer3)
        {
            cbLayer3Capable.Checked = _capabilities
                .Where(c => c.Capability == UDTNodeCapability.Layer3)
                .All(c => c.Enabled);

            txtLayer3PollingInterval.Text = _capabilities
                .Where(c => c.Capability == UDTNodeCapability.Layer3)
                .GroupBy(c => c.PollingIntervalMinutes)
                .Select(g => new { Count = g.Count(), Capability = g.First() })
                .OrderByDescending(item => item.Count)
                .Select(item => item.Capability)
                .First().PollingIntervalMinutes.ToString();

            cbDisableVRFPolling.Checked = _capabilities.Where(c => c.Capability == UDTNodeCapability.Layer3)
               .All(c => c.ContextPollingDisabled);
        }
        else UDTLayer3Box.Visible = false;
    }

    public bool Validate()
    {
        return true;
    }

    public bool Update()
    {
        if (MultipleSelectionVisible && !MultipleSelectionSelected)
            return true;

        // update may depend on mode and thus
        switch (_mode)
        {
            case NodePropertyPluginExecutionMode.EditProperies:
                if (_capabilities == null || _capabilities.Length == 0)
                    return true;

                foreach(var c in _capabilities)
                {
                    if (c.Capability == UDTNodeCapability.Layer2)
                    {
                        c.Enabled = cbLayer2Capable.Checked;
                        c.PollingIntervalMinutes = int.Parse(txtLayer2PollingInterval.Text);
                        c.ContextPollingDisabled = false;
                    }
                    else if (c.Capability == UDTNodeCapability.Layer3)
                    {
                        c.Enabled = cbLayer3Capable.Checked;
                        c.PollingIntervalMinutes = int.Parse(txtLayer3PollingInterval.Text);
                        c.ContextPollingDisabled = cbDisableVRFPolling.Checked;
                    }
                }

                using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
                {
                    try
                    {
                        bl.PollingSetNodesCapabilities(_capabilities);
                        foreach (var c in _capabilities)
                        {
                            if (c.Capability == UDTNodeCapability.Layer2)
                            {
                                bl.PollingPollNow(c.NodeID, UDTJobType.Layer2);
                            }
                            else if (c.Capability == UDTNodeCapability.Layer3)
                            {
                                bl.PollingPollNow(c.NodeID, UDTJobType.Layer2);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        _log.ErrorFormat("Update UDT Node Properties failed. Details: {0}", ex.Message);
                        throw;
                    }
                }
                break;
        }

        return true;
    }

    #endregion
}
