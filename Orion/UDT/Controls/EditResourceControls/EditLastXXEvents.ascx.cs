﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Charting;

public partial class Orion_UDT_Controls_EditResourceControls_EditLastXXEvents : BaseResourceEditControl
{
	private static int defaultNumberOfEvents = 25;

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		// fill in period list
        foreach (var period in Periods.GenerateSelectionList())
            Period.Items.Add(new ListItem(Periods.GetLocalizedPeriod(period), period));

		this.MaxEventsText.Text = Resource.Properties["MaxEvents"];
		this.Period.Text = Resource.Properties["Period"];
	}

	public override Dictionary<string, object> Properties
	{
		get
		{
			Dictionary<string, object> properties = new Dictionary<string, object>();
			try
			{
				Int32.Parse(this.MaxEventsText.Text);
				properties.Add("MaxEvents", this.MaxEventsText.Text);
			}
			catch 
			{
				properties.Add("MaxEvents", defaultNumberOfEvents.ToString());
			}

			properties.Add("Period", this.Period.Text);

			return properties;
		}
	}
}
