﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditLastXXEvents.ascx.cs"
    Inherits="Orion_UDT_Controls_EditResourceControls_EditLastXXEvents" %>
<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/PeriodControl.ascx" %>
<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td style="padding-top: 10px;">
            <asp:Label runat="server" ID="label" Text="<%$ Resources: UDTWebContent, UDTWEBCODE_TM0_17 %>" Font-Bold="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox runat="server" ID="MaxEventsText" Width="50" />
        </td>
    </tr>
    <tr>
        <td style="padding-top: 20px;">
            <asp:Label runat="server" ID="label1" Text="<%$ Resources: UDTWebContent, UDTWEBCODE_TM0_18 %>" Font-Bold="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:DropDownList runat="server" ID="Period" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RangeValidator ID="EventsRangeValidator" runat="server" ErrorMessage="<%$ Resources: UDTWebContent, UDTWEBCODE_TM0_19 %>"
                Display="Dynamic" ControlToValidate="MaxEventsText" MinimumValue="1" MaximumValue="5000"
                Type="Integer" />
        </td>
    </tr>
</table>
