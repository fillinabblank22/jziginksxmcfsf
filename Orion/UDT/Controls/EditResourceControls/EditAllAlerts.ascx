﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditAllAlerts.ascx.cs" Inherits="Orion_UDT_Controls_EditResourceControls_EditAllAlerts" EnableViewState="true" %>

<table border="0">
    <tr>
        <td style="text-align: right">
            <asp:Label runat="server" ID="label1" Text="<%$ Resources: UDTWebContent, UDTWEBDATA_TM0_22 %>" Font-Bold="true" />
        </td>
        <td style="padding-left: 10px">
            <asp:CheckBox ID="ShowAckAlertsCheckBox" runat="server" />
        </td>
    </tr>
</table>