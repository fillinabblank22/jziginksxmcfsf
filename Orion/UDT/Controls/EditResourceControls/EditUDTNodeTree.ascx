﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditUDTNodeTree.ascx.cs"
    Inherits="Orion_UDT_Controls_EditResourceControls_EditUDTNodeTree" %>
<%@ Import Namespace="Resources" %>

<%@ Register TagPrefix="orion" TagName="FilterNodesSql" Src="~/Orion/Controls/FilterNodesSql.ascx" %>
<script type="text/javascript">
       function ClientValidate(source, arguments) {
           var lbxGroup1 = $('#<%= this.lbxGroup1.ClientID %>');
           var lbxGroup2 = $('#<%= this.lbxGroup2.ClientID %>');

           if (arguments.Value != lbxGroup1[0].value
                && arguments.Value != lbxGroup2[0].value)
               arguments.IsValid = true;
           else
               arguments.IsValid = false; 
       }
</script>
    <b><%= UDTWebContent.UDTWEBDATA_TM0_7 %></b><br />
    <%= UDTWebContent.UDTWEBDATA_TM0_8 %>
<p>
    <%= UDTWebContent.UDTWEBDATA_TM0_9 %><br />
    <asp:ListBox runat="server" ID="lbxGroup1" SelectionMode="single" Rows="1">
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_TM0_17 %>" Value="" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_107 %>" Value="Status" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_104 %>" Value="Vendor" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_105 %>" Value="MachineType" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_109 %>" Value="Contact" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_108 %>" Value="Location" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_TM0_18 %>" Value="IOSImage" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_TM0_19 %>" Value="IOSVersion" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_TM0_20 %>" Value="ObjectSubType" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_TM0_21 %>" Value="SysObjectID" />
    </asp:ListBox>
</p>
<p>
    <%= UDTWebContent.UDTWEBDATA_TM0_10 %><br />
    <asp:ListBox runat="server" ID="lbxGroup2" SelectionMode="single" Rows="1">
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_TM0_17 %>" Value="" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_107 %>" Value="Status" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_104 %>" Value="Vendor" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_105 %>" Value="MachineType" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_109 %>" Value="Contact" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_108 %>" Value="Location" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_TM0_18 %>" Value="IOSImage" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_TM0_19 %>" Value="IOSVersion" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_TM0_20 %>" Value="ObjectSubType" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_TM0_21 %>" Value="SysObjectID" />
    </asp:ListBox>
    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="lbxGroup2"
        ControlToCompare="lbxGroup1" Type="String" Operator="NotEqual" Display="Dynamic"
        ErrorMessage="<%$ Resources: UDTWebContent, UDTWEBDATA_TM0_11 %>"></asp:CompareValidator>
</p>
<p>
    <%= UDTWebContent.UDTWEBDATA_TM0_12 %><br />
    <asp:ListBox runat="server" ID="lbxGroup3" SelectionMode="single" Rows="1">
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_TM0_17 %>" Value="" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_107 %>" Value="Status" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_104 %>" Value="Vendor" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_105 %>" Value="MachineType" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_109 %>" Value="Contact" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_108 %>" Value="Location" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_TM0_18 %>" Value="IOSImage" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_TM0_19 %>" Value="IOSVersion" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_TM0_20 %>" Value="ObjectSubType" />
        <asp:ListItem Text="<%$ Resources: UDTWebContent, UDTWEBDATA_TM0_21 %>" Value="SysObjectID" />
    </asp:ListBox>
    <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="lbxGroup3" 
        ClientValidationFunction="ClientValidate" Display="Dynamic" ErrorMessage="<%$ Resources: UDTWebContent, UDTWEBDATA_TM0_11 %>"></asp:CustomValidator>
</p>
<b><%= UDTWebContent.UDTWEBDATA_TM0_13 %></b>
<asp:RadioButtonList runat="server" ID="GroupNulls">
    <asp:ListItem Value="true" Text="<%$ Resources: UDTWebContent, UDTWEBDATA_TM0_14 %>" />
    <asp:ListItem Value="false" Text="<%$ Resources: UDTWebContent, UDTWEBDATA_TM0_15 %>" />
</asp:RadioButtonList>
<p>
    <asp:CheckBox runat="server" ID="RememberCollapseState" Text="<%$ Resources: UDTWebContent, UDTWEBDATA_TM0_16 %>" />
</p>
<orion:FilterNodesSql runat="server" ID="SQLFilter" />