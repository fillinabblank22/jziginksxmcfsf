﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResourceSearcherHtml.ascx.cs" Inherits="Orion_UDT_Controls_ResourceSearcherHtml" %>


<div style="width: 100%; display: <%=VisiblityMode%>" id="udtResourceSearcher-<%=ResourceId%>">
    
   <div style="width:380px;text-align:right;margin: 1px 1px 3px 1px; float:right; ">
       <input class="searchInput" style="width:200px;vertical-align:middle;height:17px;" data-placeholder="<%= Resources.UDTWebContent.UDTWEBDATA_VB1_131 %>"/>
       <a href="" class="btnClearSearch" style="display: none;"><img src="/Orion/images/clear_button.gif" alt="<%= Resources.UDTWebContent.UDTWEBDATA_VB1_132 %>" style="vertical-align:middle;"/></a>
       <a href="" class="btnSearch"><img src="/Orion/images/button.searchicon.gif" alt="<%= Resources.UDTWebContent.UDTWEBDATA_VB1_132 %>" style="vertical-align:middle;"/></a>
   </div>
</div>