using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.UDT.Common.Models;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Web.Views;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Common;

public partial class Orion_UDT_UserDetails : UdtViewPage
{
    private static SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();

    public override string ViewType
    {
        get
        {
            return "UDT UserDetails";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        pageTitleBar.PageTitle = getTitle();
        pageTitleBar.ViewID = this.ViewInfo.ViewID;
        //todo add help link
        pageTitleBar.HelpFragment = "OrionUDTPHUserDetailsSummary";
    }

    protected override void OnInit(EventArgs e)
    {
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    protected string getTitle()
    {
        int userId = 0;
        string viewTitle = this.ViewInfo.ViewTitle;
       
        try
        {
            string tmpUser = HttpUtility.UrlDecode(Request.QueryString["NetObject"]);
            string[] netObjectParts = tmpUser.Split(':');
            userId = int.Parse(netObjectParts[1]);
            if (userId > 0)
            {
                string userName = string.Empty;
                try
                {
                    userName = UDTUserDAL.GetUserDetailsViewTitle(userId);
                }
                
                catch (Exception ex)
                {
                    _log.ErrorFormat(
                    "An error occurred getting UDT user name from userId{0}.  Details: {1}",
                    userId, ex.ToString());
                }

                if (!string.IsNullOrEmpty(userName))
                {
                    //viewTitle += String.Format(" - <a href='/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}'>{1}</a> - {2}", nodeId, caption, portName);
                    viewTitle += " - " + userName;
                }
            }
        }
        catch (Exception)
        {
        }

        return viewTitle;
    }

}
