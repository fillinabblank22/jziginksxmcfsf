﻿<%@ WebService Language="C#" Class="SearchResults" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.Services.Protocols;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.UDT.Web;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Common.Models;
using SolarWinds.UDT.Web.Helpers;
using System.Linq;




[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class SearchResults : System.Web.Services.WebService
{

    private static readonly Log _log = new Log();

    private Dictionary<string, SearchResultsExportHelper> csvExports;
    private Dictionary<string, SearchResultsExportHelperEx> csvExportsEx;

    private Dictionary<string, AsyncSearchResult> searchResults;
    
    [WebMethod(EnableSession = true)]
    public PageableDataTable GetSearchResults(bool activeOnly, string searchForString, string searchForMacString, string searchInColumnList, int connectionType, int layer)
    {
        
        int pageSize;
        int startRowNumber;
        int sortColumn;

        Int32.TryParse(this.Context.Request.QueryString["sort"], out sortColumn);
        string sortDirection = this.Context.Request.QueryString["dir"];

        if (sortColumn < 1)
        {
            sortColumn = 8;
        }

        
        Int32.TryParse(this.Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(this.Context.Request.QueryString["limit"], out pageSize);

        if (pageSize < 1)
        {
            if (!Int32.TryParse(WebUserSettingsDAL.Get("UDT_SearchResults_PageSize"), out pageSize))
                pageSize = 50;
        }
        
        UDTSearchDAL udtSearchDAL = new UDTSearchDAL();
        PageableDataTable gridData = udtSearchDAL.GetSearchResults(sortColumn, sortDirection, activeOnly, searchForString, searchForMacString, searchInColumnList, connectionType, layer, startRowNumber, pageSize);

        return gridData;

    }

    private string FormatMacAddress(string rawMac)
    {
        var regex = "(.{2})(.{2})(.{2})(.{2})(.{2})(.{2})";
        return Regex.Replace(rawMac, regex, "$1:$2:$3:$4:$5:$6");
    }

    private static string SetLastSeen(DataRow row)
    {
        string lastSeen = row[7].ToString(); // default to PortToEndpoint LastSeen date
        
        // check if the PortToEndpoint LastSeen date is null
        if (string.IsNullOrEmpty(lastSeen))
        {
            // if 'ViaLayer3' = 1, then set lastSeen to the RouterLastSuccessfulScan date, else set to SwitchLastSuccessfulScanDate
            lastSeen = Equals(row[14].ToString(), '1') ? row[17].ToString() : row[16].ToString();
        }
        
        return lastSeen;
    }
    
    [WebMethod(EnableSession = true)]
    public string InitiateSearch(string id, string searchText, string searchTextMac, AsyncSearchResult.SearchTarget searchTargets)
    {
        //store AsyncSearchResults in session
        if (HttpContext.Current.Session["UDT_AsyncSearchResults"] == null)
        {
            HttpContext.Current.Session["UDT_AsyncSearchResults"] = new Dictionary<string, AsyncSearchResult>();
        }

        try
        {
            searchResults = (Dictionary<string, AsyncSearchResult>)HttpContext.Current.Session["UDT_AsyncSearchResults"];
            searchResults[id] = new AsyncSearchResult();
            searchResults[id].InitiateSearch(id, searchText, searchTextMac, searchTargets);
        }
        catch (Exception ex)
        {
            _log.ErrorFormat("An error occurred initiating search.  Details: {0}", ex.ToString());
            return ex.Message;
        }
        
        return string.Empty;
        
    }
    
    [WebMethod(EnableSession = true)]
    public PageableDataTable GetAsyncSearchResults(string id, string grouper)
    {
        
        int pageSize;
        int startRowNumber;
        
        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = this.Context.Request.QueryString["dir"];

        if (String.IsNullOrEmpty(sortColumn))
        {
            sortColumn = "MatchItem";
        }

        Int32.TryParse(this.Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(this.Context.Request.QueryString["limit"], out pageSize);

        if (pageSize < 10)
        {
            if (!Int32.TryParse(WebUserSettingsDAL.Get("UDT_SearchResults_PageSize"), out pageSize))
                pageSize = 10;
        }

        DataTable merged = new DataTable();
        DataTable sorted = new DataTable();
        DataTable sortedPage = new DataTable();
        try
        {
            searchResults = (Dictionary<string, AsyncSearchResult>)HttpContext.Current.Session["UDT_AsyncSearchResults"];
            for (int i = 0; i < searchResults[id].Result.Data.Count; i++ )
            {
                merged.Merge(searchResults[id].Result.Data[i], true);
            }
            
            DataView searchView = new DataView(merged);
            //searchView.Sort = String.Format("{0},{1} {2}", grouper, sortColumn, sortDirection);
            searchView.Sort = String.Format("{0} {1}", sortColumn, sortDirection);
            sorted = searchView.ToTable();
            
            // check we have data before trying to sort it to avoid an exception
            if (sorted.Rows.Count > 0)
            {
                // do the paging
                sortedPage = sorted.AsEnumerable().Skip(startRowNumber).Take(pageSize).CopyToDataTable();
            }

        }
        catch (Exception ex)
        {
            _log.ErrorFormat("An error occurred retrieving search results.  Details: {0}", ex.ToString());
            searchResults.Remove(id);
            return new PageableDataTable(new DataTable(), 0);
        }

        return new PageableDataTable(sortedPage, sorted.Rows.Count);
        
    }
  
    
     [WebMethod(EnableSession = true)]
    public int GetSearchProgress(string id)
    {
         //get progress of asyncSearch(id)
         int progress;
         try
         {
             searchResults = (Dictionary<string, AsyncSearchResult>)HttpContext.Current.Session["UDT_AsyncSearchResults"];
             progress = searchResults[id].Progress;
         }
         catch (Exception ex)
         {
             _log.ErrorFormat("An error occurred retrieving search progress.  Details: {0}", ex.ToString());
             searchResults.Remove(id);
             progress = -1; //error indication
         }
         return progress;
     }
     [WebMethod(EnableSession = true)]
     public string ExportToCSVEx(string downloadId, bool activeOnly, string searchForString, string searchForMacString, string searchInColumnList, int connectionType, int layer)
     {
         //store in-progress csv exports in session
         if (HttpContext.Current.Session["UDT_CSVSearchResultsExportsEx"] == null)
         {
             HttpContext.Current.Session["UDT_CSVSearchResultsExportsEx"] = new Dictionary<string, SearchResultsExportHelperEx>();
         }

         int sortColumn;

         Int32.TryParse(this.Context.Request.QueryString["sort"], out sortColumn);
         string sortDirection = this.Context.Request.QueryString["dir"];

         if (sortColumn < 1)
         {
             sortColumn = 8;
         }
      

         //get data
         //UDTDeviceDAL dal = new UDTDeviceDAL();
         UDTSearchDAL dal = new UDTSearchDAL();
         DataTable searchResults = dal.GetUnpagedSearchResults(8, sortDirection, activeOnly, searchForString,
                                                        searchForMacString, searchInColumnList, connectionType, layer);
         searchResults.Columns[0].ColumnName = "IP Address";
         searchResults.Columns[1].ColumnName = "Hostname";
         searchResults.Columns[2].ColumnName = "MAC Address";
         searchResults.Columns[3].ColumnName = "Connected To";
         searchResults.Columns[4].ColumnName = "Port Number";
         searchResults.Columns[5].ColumnName = "Port Description";
         searchResults.Columns[6].ColumnName = "VLAN";
         searchResults.Columns[7].ColumnName = "Last Seen";
         searchResults.Columns[13].ColumnName = "First Seen";

         //reformat MAC address, LastSeen & FirstSeen dates
         foreach (DataRow r in searchResults.Rows)
         {
             //MAC Address
             r[2] = FormatMacAddress(r[2].ToString());

             //LastSeen date
             r[7] = HtmlHelper.ConvertToLocalTime(SetLastSeen(r));

             //FirstSeen date
             r[13] = HtmlHelper.ConvertToLocalTime(r[13].ToString());
         }

         //remove the unwanted columns
         searchResults.Columns.RemoveAt(17);
         searchResults.Columns.RemoveAt(16);
         searchResults.Columns.RemoveAt(15);
         searchResults.Columns.RemoveAt(14);
         searchResults.Columns.RemoveAt(12);
         searchResults.Columns.RemoveAt(11);
         searchResults.Columns.RemoveAt(10);
         searchResults.Columns.RemoveAt(9);
         searchResults.Columns.RemoveAt(8);


         //start new csv export (in background thread))
         try
         {
             csvExportsEx = (Dictionary<string, SearchResultsExportHelperEx>)HttpContext.Current.Session["UDT_CSVSearchResultsExportsEx"];
             csvExportsEx[downloadId] = new SearchResultsExportHelperEx();
             csvExportsEx[downloadId].DownloadId = downloadId;
             csvExportsEx[downloadId].DownloadPath = Path.GetTempPath();
             csvExportsEx[downloadId].ActiveOnly = activeOnly;
             csvExportsEx[downloadId].SearchForString = searchForString;
             csvExportsEx[downloadId].SearchForMacString = searchForMacString;
             csvExportsEx[downloadId].SearchInColumnList = searchInColumnList;
             csvExportsEx[downloadId].SortColumn = sortColumn;
             csvExportsEx[downloadId].SortDirection = sortDirection;
             csvExportsEx[downloadId].Start(searchResults);
         }
         catch (System.Exception ex)
         {
             _log.ErrorFormat("An error occurred exporting to CSV.  Details: {0}", ex.ToString());
             return ex.Message;
         }
         return string.Empty;
     }
    
    [WebMethod(EnableSession = true)]
     public string ExportToCSV(string downloadId, string searchForString, string searchForMacString, AsyncSearchResult.SearchTarget searchTargets)
    {
        //store in-progress csv exports in session
        if (HttpContext.Current.Session["UDT_CSVSearchResultsExports"] == null)
        {
            HttpContext.Current.Session["UDT_CSVSearchResultsExports"] = new Dictionary<string, SearchResultsExportHelper>();
        }

        string sortColumn = this.Context.Request.QueryString["sort"];
        string sortDirection = this.Context.Request.QueryString["dir"];

        if (String.IsNullOrEmpty(sortColumn))
        {
            sortColumn = "MatchItem";
        }
                    
        //start new csv export (in background thread))
        try
        {
            csvExports = (Dictionary<string, SearchResultsExportHelper>)HttpContext.Current.Session["UDT_CSVSearchResultsExports"];
            csvExports[downloadId] = new SearchResultsExportHelper();
            csvExports[downloadId].DownloadId = downloadId;
            csvExports[downloadId].DownloadPath = Path.GetTempPath();
            csvExports[downloadId].SearchForString = searchForString;
            csvExports[downloadId].SearchForMacString = searchForMacString;
            csvExports[downloadId].SearchTargets = searchTargets;
            csvExports[downloadId].SortColumn = sortColumn;
            csvExports[downloadId].SortDirection = sortDirection;
            var searchResults = new AsyncSearchResult();
            searchResults.InitiateSearch(downloadId, searchForString, searchForMacString, searchTargets);
            csvExports[downloadId].Start(searchResults);
        }
        catch (System.Exception ex)
        {
            _log.ErrorFormat("An error occurred exporting to CSV.  Details: {0}", ex.ToString());
            return ex.Message;
        }
        return string.Empty;
    }


    [WebMethod(EnableSession = true)]
    public string GetExportProgress(string downloadId)
    {
        //get progress of csv export
        string progress;
        try
        {
            csvExports = (Dictionary<string, SearchResultsExportHelper>)HttpContext.Current.Session["UDT_CSVSearchResultsExports"];
            progress = csvExports[downloadId].Progress;
        }
        catch (System.Exception ex)
        {
            _log.ErrorFormat("An error occurred retrieving export progress.  Details: {0}", ex.ToString());
            csvExports.Remove(downloadId);
            progress = "-1"; //error indication
        }

        if ((progress == "100") || (progress == "-1"))
        {
            csvExports.Remove(downloadId);
        }
        return progress;
    }

    [WebMethod(EnableSession = true)]
    public string GetExportProgressEx(string downloadId)
    {
        //get progress of csv export
        string progress;
        try
        {
            csvExportsEx = (Dictionary<string, SearchResultsExportHelperEx>)HttpContext.Current.Session["UDT_CSVSearchResultsExportsEx"];
            progress = csvExportsEx[downloadId].Progress;
        }
        catch (System.Exception ex)
        {
            _log.ErrorFormat("An error occurred retrieving export progress.  Details: {0}", ex.ToString());
            csvExportsEx.Remove(downloadId);
            progress = "-1"; //error indication
        }

        if ((progress == "100") || (progress == "-1"))
        {
            csvExportsEx.Remove(downloadId);
        }
        return progress;
    }
    
   }

