﻿<%@ WebService Language="C#" Class="ManageDomainControllers" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.Services.Protocols;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.UDT.Common;
using SolarWinds.UDT.Web;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Common.Models;
using SolarWinds.UDT.Web.Helpers;
using System.Web.Script.Serialization;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ManageDomainControllers : System.Web.Services.WebService
{

    private static readonly Log _log = new Log();
    private static readonly bool _isDemoServer = SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetDomainControllers()
    {

        int pageSize;
        int startRowNumber;

        string sortColumn = this.Context.Request.QueryString["sort"];
        string sortDirection = this.Context.Request.QueryString["dir"];

        Int32.TryParse(this.Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(this.Context.Request.QueryString["limit"], out pageSize);

        if (pageSize < 10)
        {
            if (!Int32.TryParse(WebUserSettingsDAL.Get("UDT_ManageDomainControllers_PageSize"), out pageSize))
                pageSize = 10;
        }

        UDTDeviceDAL udtDeviceDAL = new UDTDeviceDAL();
        PageableDataTable gridData = udtDeviceDAL.GetDomainControllers(sortColumn, sortDirection, startRowNumber, pageSize);

        return gridData;

    }

    // delete domain controller from UDT only by removing the node capability
    [WebMethod(EnableSession = true)]
    public bool DeleteDomainControllersFromUdtOnly(int nodeId)
    {
        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                bl.PollingDeleteNodeCapability(nodeId, UDTNodeCapability.DomainController);
                bl.UserSetDCCredential(nodeId, -1); // -1 will remove credential if any assigned
            }
            return true;
        }

        catch (Exception ex)
        {
            _log.Error("Error in DeleteDomainControllers function.", ex);

            throw new Exception(String.Format("UDT deleting domain controller failed for nodeId {0}.", nodeId));
        }
    }



    // delete domain controller from UDT only by removing the node capability
    [WebMethod(EnableSession = true)]
    public bool DeleteDomainControllersFromAllModules(int nodeId)
    {
        if (nodeId == null) return true;

        try
        {
            SolarWinds.Orion.Core.Common.Models.Node node = SolarWinds.Orion.Core.Common.NodeDAL.GetNode(nodeId);
            SolarWinds.Orion.Core.Common.NodeDAL.DeleteNode(node);
            return true;
        }

        catch (Exception ex)
        {
            _log.Error("Error in DeleteDomainControllersFromAllModules function.", ex);

            throw new Exception(String.Format("Deleting domain controller from all modules failed for nodeId {0}.", nodeId));
        }
    }
    
   
    // test the REL security credentials for the domain controller
    [WebMethod(EnableSession = true)]
    public string TestSecurityCredentials(int nodeId, string ipAddress, int credentialsId)
    {
         JavaScriptSerializer s = new JavaScriptSerializer();
        var result = new DomainControllerConfigStatus();
       
        if (_isDemoServer)
        {
            try
            {
                var swql = string.Format(@"select top 1 LastScanResult from Orion.UDT.NodeCapability (nolock=true) where NodeID = {0}", nodeId);
                if ( (byte)SWISHelper.Query(swql).Rows[0][0] == 1)
                {

                    result.UserDetailPollingStatus = true;
                    return s.Serialize(result);
                }
                result.UserDetailPollingStatus = false;
               
                return s.Serialize(result);
            }
            catch(Exception ex)
            {
                result.UserDetailPollingStatus = true;
                return s.Serialize(result);
            }
        }
       
        string nodeIp = null;

        //_log.ErrorFormat("UDT ManageDomainControllers START: TestSecurityCredentials(NodeID:{0})", nodeId);
        if (string.IsNullOrEmpty(ipAddress))
        {
            UDTDeviceDAL dal = new UDTDeviceDAL();
            nodeIp = dal.GetNodeIpFromId(nodeId);
        }
        else
        {
            nodeIp = ipAddress;
        }

        //_log.ErrorFormat("UDT ManageDomainControllers CONTINUE: TestSecurityCredentials(NodeID:{0} NodeIP:{1})", nodeId, nodeIp);

        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                if (credentialsId==-1)
                credentialsId = bl.UserGetCredentialForNode(nodeId);
            UsernamePasswordCredential DCCredential = bl.UserGetCredential(credentialsId);
            result. UserDetailPollingStatus = bl.UserTestCredential(nodeIp, DCCredential, UDTCredentialType.REL);
            _log.DebugFormat("UDT ManageDomainControllers: TestSecurityCredentials(NodeID:{1} NodeIP:{2} CredentialsID:{3}): {0}", (result.UserDetailPollingStatus ? "Success" : "Failure"), nodeId, nodeIp, credentialsId);

     

            }
        }
        catch (Exception ex)
        {
            _log.ErrorFormat(
                "An error occurred getting DC credentials from UDT Business Layer (ManageDomainControllers: TestSecurityCredentials).  Details: {0}",
                ex.ToString());
        }

        // log results of credential check for current CD
        
        return
            s.Serialize(result);

    }
	
	[WebMethod(EnableSession = true)]
    public string CheckLastScanResult(int nodeId)
    {
        JavaScriptSerializer s = new JavaScriptSerializer();
        var result = new DomainControllerConfigStatus();

        try
        {
            var swql = string.Format(@"select top 1 LastScanResult from Orion.UDT.NodeCapability (nolock=true) where NodeID = {0} and Capability = 8", nodeId);
            if ( (byte)SWISHelper.Query(swql).Rows[0][0] == 1)
            {

                result.UserDetailPollingStatus = true;
                return s.Serialize(result);
            }
            result.UserDetailPollingStatus = false;
               
            return s.Serialize(result);
        }
        catch(Exception ex)
        {
            result.UserDetailPollingStatus = false;
            return s.Serialize(result);
        }
    }


    // get credentials
    [WebMethod(EnableSession = true)]
    public List<string> GetCredentials()
    {
        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                List<UsernamePasswordCredential> allCredentials = bl.UserGetAllCredentials(false).ToList();
                return allCredentials.Select(item => item.Name).ToList();
            }
        }

        catch (Exception ex)
        {
            _log.Error("Error in GetCredentials function.", ex);

            throw new Exception("UDT getting credential names failed");
        }
    }

    
    [WebMethod(EnableSession = true)]
    public PageableDataTable GetDCCredentials()
    {
        DataTable table = new DataTable();
        table.Columns.Add("CredentialID", typeof(int));
        table.Columns.Add("CredentialName", typeof(string));
        table.Columns.Add("Username", typeof(string));
        table.Columns.Add("Password", typeof(string));

        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                const int nonExistingDCCredentialID = 0;
                List<UsernamePasswordCredential> dcCredentials;

                UsernamePasswordCredential[] allCredentials = bl.UserGetAllCredentials(false);
                dcCredentials = allCredentials.ToList();

                //this wil add a 'new credential'item to the list
                dcCredentials.Insert(0,
                                     new UsernamePasswordCredential() { ID = nonExistingDCCredentialID, Name = "<New Credential>" });

                foreach (var item in dcCredentials)
                {
                    DataRow r = table.NewRow();
                    r["CredentialID"] = item.ID;
                    r["CredentialName"] = item.Name;
                    r["Username"] = item.Username;
                    r["Password"] = item.Password;
                    table.Rows.Add(r);
                }

                return new PageableDataTable(table, table.Rows.Count);
            }
        }

        catch (Exception ex)
        {
            _log.Error("Error in GetDCCredentials function.", ex);

            throw new Exception("UDT getting credential names failed");
        }
    }

    [WebMethod(EnableSession = true)]
    public bool SetDCCredentials(string nodeIds, string credentialId)
    {

        //split nodeids into array
        string[] ids = nodeIds.Split(',');
        
        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                foreach (string id in ids)
                {
                    int nodeID;
                    int credID;
                    if (int.TryParse(id, out nodeID) && int.TryParse(credentialId, out credID))
                    {
                        bl.UserSetDCCredential(nodeID, credID);
                    }
                }
                return true;
            }
        }
        catch (Exception ex)
        {
            _log.ErrorFormat("Error in GetDCCredentials function. (Nodes: {0} CredentialID: {1}).  Details: {2}", nodeIds, credentialId, ex.ToString());
            throw new Exception("UDT setting credential(s) for nodes failed");
        }
    }


    [WebMethod]
    public int? InsertDCCredentialAndReturnId(string name, string username, string password)
    {
        int credentialId = 0;

        if (String.IsNullOrEmpty(name))
            return 0;

        if (String.IsNullOrEmpty(username))
            return 0;

        // create new credential object
        UsernamePasswordCredential credential = new UsernamePasswordCredential()
        {
            Description = "",
            Name = name,
            Username = username,
            Password = password
        };

        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                return bl.UserAddCredential(credential);
            }
        }

        catch (Exception ex)
        {
            _log.ErrorFormat("An error occurred contacting the UDT Business Layer (AddCredential).  Details: {0}", ex.ToString());
            throw;
        }


        
       
    }
   
    class DomainControllerConfigStatus
    {
        public bool UserDetailPollingStatus { get; set; }
    }
}