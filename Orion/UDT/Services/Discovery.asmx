﻿<%@ WebService Language="C#" Class="Discovery" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.Services.Protocols;
using SolarWinds.Logging;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.UDT.Common;
using SolarWinds.UDT.Web;
using SolarWinds.UDT.Web.Helpers;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Common.Models;
using SolarWinds.Orion.Core.Common.Models;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class Discovery  : System.Web.Services.WebService {

    private static readonly Log _log = new Log();
    
    private Dictionary<string, WatchListExportHelper> csvExports;
    
    [WebMethod]
    public PageableDataTable GetDevices(string grouper)
    {
        int pageSize;
        int startRowNumber;

        string sortColumn = this.Context.Request.QueryString["sort"];
        string sortDirection = this.Context.Request.QueryString["dir"];

        Int32.TryParse(this.Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(this.Context.Request.QueryString["limit"], out pageSize);

        if (pageSize < 10)
        {
            if (!Int32.TryParse(WebUserSettingsDAL.Get("UDT_PortDiscovery_PageSize"), out pageSize))
                pageSize = 10;
        }

        UDTDeviceDAL deviceDAL = new UDTDeviceDAL();
        PageableDataTable gridData = deviceDAL.GetDevices(sortColumn, sortDirection, grouper, startRowNumber, pageSize);
        return gridData;
        
    }

    [WebMethod(EnableSession = true)]
    public string[] GetDiscoveryErrors()
    {
        List<string> errorList = new List<string>();
        UDTDeviceDAL deviceDAL = new UDTDeviceDAL();
        List<int> nodeErrors = new List<int>();
        //Add error notification for devices with no ports found (will not be member of Result.Switches)
        foreach (string nodeID in DiscoveryWorkflowManager.Devices)
        {
           bool foundThisNode = false;
            
           foreach (UDTNode sw in DiscoveryWorkflowManager.Results.UDTNodes.Where(n => n.ContainsCapability(UDTNodeCapability.Layer2)))
           {
               if (sw.NodeID == int.Parse(nodeID))
               {
                   foundThisNode = true; 
               }
           }
           if (!foundThisNode)
           {
               errorList.Add(string.Format(Resources.UDTWebContent.UDTWCODE_VB1_21, deviceDAL.GetNodeCaption(int.Parse(nodeID))));
               nodeErrors.Add(int.Parse(nodeID));
           }
        }
        
        
        if (DiscoveryWorkflowManager.Errors != null)
        {
            UDTDiscoveryError[] errors = DiscoveryWorkflowManager.Errors;
            foreach (UDTDiscoveryError error in errors)
            {
                if (!nodeErrors.Contains(error.NodeID)) //only add another message if existing message doesn't already exist
                {
                    errorList.Add(string.Format(Resources.UDTWebContent.UDTWCODE_VB1_22, deviceDAL.GetNodeCaption(error.NodeID), error.Message));
                }
            }
            return errorList.ToArray();
        } else {
            if (errorList.Count > 0)
            {
                return errorList.ToArray();
            }
            else
            {
                return null;
            }
        }
    }
    
    [WebMethod(EnableSession = true)]
    public PageableDataTable GetPorts(  string grouper, 
                                        bool filterActiveStatus,
                                        bool filterInactiveStatus,
                                        bool filterTrunk,
                                        bool filterNonTrunk,
                                        bool filterPortExcluded,
                                        string filterPortRange,
                                        string filterVLAN,
                                        string filterPortDescription 
                                     )
    {
        int pageSize;
        int startRowNumber;

        string sortColumn = this.Context.Request.QueryString["sort"];
        string sortDirection = this.Context.Request.QueryString["dir"];

        Int32.TryParse(this.Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(this.Context.Request.QueryString["limit"], out pageSize);

        if (pageSize < 10)
        {
            if (!Int32.TryParse(WebUserSettingsDAL.Get("UDT_PortDiscovery_PageSize"), out pageSize))
                pageSize = 500;
        }
     
        UDTDeviceDAL deviceDAL = new UDTDeviceDAL();
        PageableDataTable gridData = deviceDAL.GetPorts(sortColumn, sortDirection, grouper, filterActiveStatus, filterInactiveStatus, filterTrunk, filterNonTrunk, filterPortExcluded, filterPortRange, filterVLAN, filterPortDescription, startRowNumber, pageSize);
        return gridData;

    }
    
    [WebMethod(EnableSession = true)]
    public int GetSelectedDeviceCount()
    {
        try
        {
            if (DiscoveryWorkflowManager.Devices == null)
            {
                return 0;
            } else
            {
                return DiscoveryWorkflowManager.Devices.Count;
            }
        }
        catch (Exception ex)
        {
            _log.ErrorFormat("DiscoveryWorkflowManager GetSelectedDeviceCount failed.  Details: {0}", ex);
            return 0;
        }
    }

    [WebMethod(EnableSession = true)]
    public int GetTotalPortCount(string grouper,
                                        bool filterActiveStatus,
                                        bool filterInactiveStatus,
                                        bool filterTrunk,
                                        bool filterNonTrunk,
                                        bool filterPortExcluded,
                                        string filterPortRange,
                                        string filterVLAN,
                                        string filterPortDescription)
    {
        //Get single page...
        int pageSize = 10;
        int startRowNumber = 0;
        string sortColumn = "NodeID";
        string sortDirection = "ASC";
        UDTDeviceDAL deviceDAL = new UDTDeviceDAL();
        PageableDataTable gridData = deviceDAL.GetPorts(sortColumn, sortDirection, grouper, filterActiveStatus, filterInactiveStatus, filterTrunk, filterNonTrunk, filterPortExcluded, filterPortRange, filterVLAN, filterPortDescription, startRowNumber, pageSize);
        
        //return total rows count
        return (int)gridData.TotalRows;
    }

    [WebMethod(EnableSession = true)]
    public string[] SelectAllPorts(string grouper,
                                        bool filterActiveStatus,
                                        bool filterInactiveStatus,
                                        bool filterTrunk,
                                        bool filterNonTrunk,
                                        bool filterPortExcluded,
                                        string filterPortRange,
                                        string filterVLAN,
                                        string filterPortDescription)
    {
        //Get all filtered rows
        int pageSize = int.MaxValue;
        int startRowNumber = 0;
        string sortColumn = "NodeID";
        string sortDirection = "ASC";
        UDTDeviceDAL deviceDAL = new UDTDeviceDAL();
        PageableDataTable gridData = deviceDAL.GetPorts(sortColumn, sortDirection, grouper, filterActiveStatus, filterInactiveStatus, filterTrunk, filterNonTrunk, filterPortExcluded, filterPortRange, filterVLAN, filterPortDescription, startRowNumber, pageSize);
        
        //Output array of selections
        List<string> selectedPorts = new List<string>();
        foreach (DataRow r in gridData.DataTable.Rows)
        {
            selectedPorts.Add(String.Format("{0}|{1}", r["NodeID"].ToString(), r["PortIndex"].ToString()));
        }
        return selectedPorts.ToArray();
    }
    

    [WebMethod(EnableSession = true)]
    public string[] GetSelectedDevices()
    {
        if (DiscoveryWorkflowManager.Devices == null)
        {
            return null;
        }
        else
        {
            return DiscoveryWorkflowManager.Devices.ToArray();
        }
    }
    
    [WebMethod(EnableSession = true)]
    public int SelectDevices(string[] nodeIds)
    {
        if (nodeIds == null) return 0;
        int addedCount = 0;
        try
        {
            DiscoveryWorkflowManager.Devices = new List<string>(nodeIds);
            return nodeIds.Length;
        }
        catch (Exception ex)
        {
            _log.ErrorFormat("DiscoveryWorkflowManagerAddDevices failed.  Details: {0}", ex);
            return addedCount;
        }
    }


    [WebMethod(EnableSession = true)]
    public PageableDataTable GetWatchList(string searchText, string searchTextFormatStripped)
    {
        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];
        
        int pageSize;
        int startRowNumber;
        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);
        
        if (pageSize < 10)
            if (!Int32.TryParse(WebUserSettingsDAL.Get("UDT_WatchList_PageSize"), out pageSize))
                pageSize = 10;

        // get full watch list
//TODO: WatchList: cache watch list in session and reuse until full page reload?
        var watchList = UDTWatchListDAL.PopulateWatchList();

        if (watchList == null)
            return new PageableDataTable(null, 0);

        watchList.Columns.Add("LastSeenFormatted", typeof(string));
        
        foreach(DataRow row in watchList .Rows)
        {
            row["MACAddress"] = row["MACAddress"] == DBNull.Value ? string.Empty : DALHelper.MacAddressToStorageFormat(row["MACAddress"].ToString());
            row["LastSeenFormatted"] = !(bool) row["IsFound"]
                                            ? Resources.UDTWebContent.UDTWCODE_VB1_2
                                            : (bool) row["IsActive"]
                                                    ? Resources.UDTWebContent.UDTWCODE_VB1_3
                                                    : row["LastSeen"] == DBNull.Value ? string.Empty : ((DateTime) row["LastSeen"]).ToString();
        }
        
        // remove unneccessary columns
        using (var view = new DataView(watchList))
        {
            watchList = view.ToTable(false, new[] { "WatchID", "WatchItemType", "WatchName", "MACAddress", "IPAddress", "DNSName", "WatchNote", "LastSeen", "LastSeenFormatted","UserName"});
        }

        if (watchList.Rows.Count == 0)
            return new PageableDataTable(watchList, 0);
        
        // sort and filter
        using (var view = new DataView(watchList))
        {
            view.Sort = string.Format("{0} {1}", sortColumn, sortDirection);
                
            if (!string.IsNullOrEmpty(searchText))
            {
                var filter = string.Format(@"
WatchName LIKE '{0}' OR MACAddress LIKE '{1}' OR IPAddress LIKE '{0}' OR 
DNSName LIKE '{0}' OR WatchNote LIKE '{0}' OR LastSeenFormatted LIKE '{0}' OR UserName LIKE '%{0}%'", searchText, searchTextFormatStripped);
                view.RowFilter = filter;
            }

            watchList = view.ToTable();
        }

        // paginate
        var totalRowCount = watchList.Rows.Count;

        // if pagesize is greater than rowcount of current page, reset pageSize to be current page rowcount
        if ((totalRowCount - startRowNumber) < pageSize)
            pageSize = totalRowCount - startRowNumber;

        // limit return rows
        var returnData = watchList.Clone();
        for (int i = startRowNumber; i < (startRowNumber + pageSize); i++)
            returnData.ImportRow(watchList.Rows[i]);
            
        return new PageableDataTable(returnData, totalRowCount);
    }

    [WebMethod(EnableSession = true)]
    public string AddDeviceWatchList(int watchItemType, string watchItemValue, string watchName, string watchNote)
    {
        try
        {
            return UDTWatchListDAL.AddDeviceWatchList(watchItemType, watchItemValue, watchName, watchNote);
        } 
        catch (Exception ex)
        {
            _log.ErrorFormat("An error occurred while adding device to watch list.  Details: {0}", ex.ToString());
            return Resources.UDTWebContent.UDTWCODE_VB1_23;
        }
    }

    [WebMethod(EnableSession = true)]
    public string EditDeviceWatchList(int watchId, int watchItemType, string watchItemValue, string watchName, string watchNote)
    {
        try
        {
            return UDTWatchListDAL.EditDeviceWatchList(watchId, watchItemType, watchItemValue, watchName, watchNote);
        }
        catch (Exception ex)
        {
            _log.ErrorFormat("An error occurred while editing watch list device.  Details: {0}", ex.ToString());
            return string.Format(Resources.UDTWebContent.UDTWCODE_VB1_24, ex.Message);
        }
    }

    [WebMethod]
    public string DeleteWatchList(IEnumerable<string> watchListIds)
    {
        try
        {
            UDTWatchListDAL.DeleteWatchList(watchListIds);
        }
        catch (Exception ex)
        {
            _log.ErrorFormat("An error occurred while deleting watch list device.  Details: {0}", ex.ToString());
            return string.Format(Resources.UDTWebContent.UDTWCODE_VB1_25, ex.Message);
        }
        return String.Empty;
    }
    
    [WebMethod(EnableSession = true)]
    public string ExportToCSV(string downloadId)
    {
        //store in-progress csv exports in session
        if (HttpContext.Current.Session["UDT_CSVWatchListExports"]==null)
        {
            HttpContext.Current.Session["UDT_CSVWatchListExports"] = new Dictionary<string, WatchListExportHelper>();
        }
        
        //start new csv export (in background thread)
        try
        {
            var watchList = UDTWatchListDAL.PopulateWatchList(true);
            
            csvExports = (Dictionary<string, WatchListExportHelper>)HttpContext.Current.Session["UDT_CSVWatchListExports"];
            csvExports[downloadId] = new WatchListExportHelper();
            csvExports[downloadId].DownloadId = downloadId;
            csvExports[downloadId].DownloadPath = Path.GetTempPath();
            csvExports[downloadId].Start(watchList);
        }
        catch (System.Exception ex)
        {
            _log.ErrorFormat("An error occurred exporting to CSV.  Details: {0}", ex.ToString());
            return ex.Message;
        }
        return string.Empty;
    }


    [WebMethod(EnableSession = true)]
    public string GetExportProgress(string downloadId)
    {
        //get progress of csv export
        string progress;
        try
        {
            csvExports = (Dictionary<string, WatchListExportHelper>)HttpContext.Current.Session["UDT_CSVWatchListExports"];
            progress = csvExports[downloadId].Progress;
        }
        catch (System.Exception ex)
        {
            _log.ErrorFormat("An error occurred retrieving export progress.  Details: {0}", ex.ToString());
            csvExports.Remove(downloadId);
            progress = "-1"; //error indication
        }

        if ((progress == "100") || (progress=="-1"))
        {
            csvExports.Remove(downloadId); 
        }
        return progress;
    }

    [WebMethod(EnableSession = true)]
    public string StartPortDiscovery()
    {
        //Create Discovery Job in BusinessLayer
        using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
        {
            try
            {
                Node node = NodeWorkflowHelper.Node;
                //Pass Node to DiscoveryStart
                return bl.DiscoveryPortDiscoveryStart(node).ToString();
            }
            catch (Exception ex)
            {
                //UDTBusinessLayer in fault state?
                _log.Error(ex.Message);
                _log.Error(ex.StackTrace);
                throw;
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public UDTDiscoveryResult ProcessPortDiscoveryResults(string discoveryId)
    {
        //Get Discovery Results
        using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
        {
            try
            {
                //Pump results into session and return
                UDTDiscoveryResult result = bl.DiscoveryGetResults(new Guid(discoveryId));
                HttpContext.Current.Session["UDT_AddNode_PortDiscoveryResults"] = result;
                return result;
            }
            catch (Exception ex)
            {
                //UDTBusinessLayer in fault state?
                _log.Error(ex.Message);
                _log.Error(ex.StackTrace);
                throw;
            }
        }
    }


    [WebMethod]
    public UDTDiscoveryProgress GetPortDiscoveryProgress(string discoveryId)
    {

        //Get Discovery Progress from BusinessLayer
        using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
        {
            try
            {
                UDTDiscoveryProgress info = bl.DiscoveryGetProgress(new Guid(discoveryId));
                return info;
            }
            catch (Exception ex)
            {
                //UDTBusinessLayer in fault state?
                _log.Error(ex.Message);
                _log.Error(ex.StackTrace);
                throw;
            }
        }

    }

    [WebMethod]
    public void CancelPortDiscovery(string discoveryId)
    {
        using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
        {
            try
            {
                bl.DiscoveryCancel(new Guid(discoveryId));
            }
            catch (Exception ex)
            {
                //UDTBusinessLayer in fault state?
                _log.Error(ex.Message);
                throw;
            }
        }

    }

    [WebMethod(EnableSession = true)]
    public string[] GetPortDiscoveryErrors(string discoveryId)
    {


        using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
        {
            try
            {
                UDTDiscoveryError[] errors = bl.DiscoveryGetErrors(new Guid(discoveryId));
                UDTDiscoveryResult result = (UDTDiscoveryResult) HttpContext.Current.Session["UDT_AddNode_PortDiscoveryResults"];
                
                List<string> errorList = new List<string>();

                int count = result.UDTNodes.Where(n => n.ContainsCapability(UDTNodeCapability.Layer2)).Count();
                
                if (count == 0)
                {
                    errorList.Add(Resources.UDTWebContent.UDTWCODE_VB1_26);
                }


                if (errors != null)
                {
                    foreach (UDTDiscoveryError error in errors)
                    {
                        if (errorList.Count != 0) //only add another message if existing message doesn't already exist
                        {
                            errorList.Add(error.Message);
                        }
                    }
                    return errorList.ToArray();
                }
                else
                {
                    if (errorList.Count > 0)
                    {
                        return errorList.ToArray();
                    }
                    else
                    {
                        return null;
                    }
                }
                
                
            }
            catch (Exception ex)
            {
                //UDTBusinessLayer in fault state?
                _log.Error(ex.Message);
                _log.Error(ex.StackTrace);
                throw;
            }
        }
    }
    
    [WebMethod]
    public string CheckLicenseStatus(int selectedPortCount)
    {

        string ErrorGettingLicensedElementsCount = Resources.UDTWebContent.UDTWCODE_VB1_15;
        string MainLicenseWarning = Resources.UDTWebContent.UDTWCODE_VB1_16;
        string HowManyLicensedMessage = Resources.UDTWebContent.UDTWCODE_VB1_17;
        string OverlappingMessage = Resources.UDTWebContent.UDTWCODE_VB1_18;
        string OnlySelectionOverMessage = Resources.UDTWebContent.UDTWCODE_VB1_19;

        string PortsHelpfulMessage = string.Format(Resources.UDTWebContent.UDTWCODE_VB1_20, "<a href=\"/Orion/Nodes/Default.aspx\" class=\"redLink\">", "</a>", "<a href=\"http://www.solarwinds.com/products/orion/upgrade/upgrade.aspx\" class=\"redLink\">", "</a>&nbsp;&nbsp;");
 
        
        int maxPorts = 0;
        int portCount = 0;
        try
        {
            using (WebDAL dal = new WebDAL())
            {
                var featureManager = new RemoteFeatureManager();
                maxPorts = featureManager.GetMaxElementCount(SolarWinds.UDT.Common.WellKnownElementTypes.UDT.Ports);
                portCount = UDTPortDAL.GetPortCount();
            }
        }
        catch (Exception ex)
        {
            _log.Error("Cannot get number of licensed elements.", ex);
            throw new Exception("Cannot get number of licensed elements.", ex);
        }

        StringBuilder licenseExceptionMessage = new StringBuilder();
        if (portCount > maxPorts)
        {
            licenseExceptionMessage.AppendLine(MainLicenseWarning);
            licenseExceptionMessage.AppendLine(String.Format(HowManyLicensedMessage, maxPorts.ToString()));
            licenseExceptionMessage.AppendLine(String.Format(OverlappingMessage, (portCount - maxPorts), selectedPortCount));
            licenseExceptionMessage.AppendLine(PortsHelpfulMessage);
        }
        else if ((selectedPortCount + portCount) > maxPorts)
        {
            licenseExceptionMessage.AppendLine(MainLicenseWarning);
            licenseExceptionMessage.AppendLine(String.Format(HowManyLicensedMessage, maxPorts.ToString()));
            licenseExceptionMessage.AppendLine(String.Format(OnlySelectionOverMessage, (selectedPortCount + portCount) - maxPorts));
            licenseExceptionMessage.AppendLine(PortsHelpfulMessage);
        }

        return (String.IsNullOrEmpty(licenseExceptionMessage.ToString()) ? null : licenseExceptionMessage.ToString());

    }
    
    [WebMethod]
    public string CheckLicensedPorts(List<string> portsInfo)
    {
        if (portsInfo.Count > 0)
        {
            Dictionary<string, string> parsedPorts =
                portsInfo.Select(p => p.Split(' ')).ToDictionary(pair => pair[0], pair => pair[1]);

            var result = UDTPortDAL.GetAlreadyMonitoredPortsCount(parsedPorts);

            return CheckLicenseStatus(portsInfo.Count - result);
        }

        return null;
    }
}
