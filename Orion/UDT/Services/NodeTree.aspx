﻿<%@ Page Language="C#" %>
<%@ Import Namespace="SolarWinds.UDT.Web" %>
<%@ Import Namespace="SolarWinds.UDT.Web.DAL" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Web.Script.Serialization" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="SolarWinds.UDT.Web.Helpers" %>

<script runat="server">

private static Dictionary<string, string> vendorImageFileNames;

// This should really be in an HttpHandler (.ashx) file.  The SwisProxy class
// doesn't handle this well.  We get a Null Exception at HttpContext.Current.Session[""]
// For now, we'll just do this in a page.
protected void Page_Load(object sender, EventArgs e)
{
	string groupCounts = Request["GroupCounts"].UrlEncode() ?? "";
	string groupBy = Request["groupby"].UrlEncode() ?? "Vendor";
	string rootNode = Request["node"].UrlEncode() ?? "/";

	JavaScriptSerializer serializer = new JavaScriptSerializer();
	object data = null;

	if (String.IsNullOrEmpty(groupCounts))
	{
		data = GetTreeNodes(groupBy, rootNode);
	}
	else
	{
		// Load initial group counts
		string itemString = Request["items"] ?? "";
		int[] items = serializer.Deserialize<int[]>(itemString);
		data = LoadGroupCounts(groupBy, items);
	}

	string content = serializer.Serialize(data);

	Response.ContentType = "text/html";
	Response.Write(content);
	Response.End();
}

private Dictionary<string, int[]> LoadGroupCounts(string groupBy, int[] items)
{
	Dictionary<string, int[]> results = new Dictionary<string, int[]>();

	const int PropertyIndex = 0;
	const int AllCountIndex = 1;
	const int SelCountIndex = 2;

	DataTable table = GetNodePropertiesForCateogry(groupBy, items);

	foreach (DataRow row in table.Rows)
	{
		string group = row[PropertyIndex].ToString();
		results[group] = new int[] { Convert.ToInt32(row[AllCountIndex]), Convert.ToInt32(row[SelCountIndex]) };
	}
	return results;
}

public DataTable GetNodePropertiesForCateogry(string category, int[] filterNodeIds)
{
    UDTDeviceDAL deviceDAL = new UDTDeviceDAL();
    DataTable allTable = deviceDAL.GetNodePropertiesForCateogry(category, filterNodeIds, Request["nodeFilter"]);
	return allTable;
}

private List<ExtTreeNode> GetTreeNodes(string groupBy, string rootNode)
{
	List<ExtTreeNode> nodes = new List<ExtTreeNode>();

	if (rootNode == "/")
	{
		AddRootNodes(nodes, groupBy);
	}
	else
	{
		GetChildNodes(nodes, groupBy, rootNode);
	}


	return nodes;
}

    private void AddRootNodes(List<ExtTreeNode> nodes, string groupBy)
{
	bool showVendorIcons = (groupBy.Equals("vendor", StringComparison.OrdinalIgnoreCase));

    UDTDeviceDAL deviceDAL = new UDTDeviceDAL();
    
	if ((vendorImageFileNames == null) && showVendorIcons)
	{
        vendorImageFileNames = UDTDeviceDAL.GetVendorIconFileNames(false);
		//vendorImageFileNames = SwisDAL.GetVendorIconFileNames(false);

		// Replace empty vendors with 'Unknown' text
		if (vendorImageFileNames != null)
		{
			vendorImageFileNames.Remove(" ");
			vendorImageFileNames["Unknown"] = "Unknown.gif";
		}
	}

	//DataTable table = SwisDAL.GetNodePropertiesForCateogry(groupBy, Request["nodeFilter"]);
    DataTable table = deviceDAL.GetNodePropertiesForCateogry(groupBy, Request["nodeFilter"]);

	Dictionary<string, string> uniquePropertyValues = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);

	foreach (DataRow dataRow in table.Rows)
	{
        string property = (dataRow["theProperty"].ToString().Trim() == string.Empty) ? "Unknown" : dataRow["theProperty"].ToString();
        string propertyDisplayName = (dataRow["theProperty"].ToString().Trim() == string.Empty) ? Resources.UDTWebContent.UDTWCODE_VB1_14 : dataRow["theProperty"].ToString();
		if (!uniquePropertyValues.ContainsKey(property))
			uniquePropertyValues[property] = propertyDisplayName;
	}

	foreach (KeyValuePair<string, string> property in uniquePropertyValues)
	{
		// FB31069 - slash in group name is parsed as path separator, replaced with double slash
		ExtTreeNode node = new ExtTreeNode("/" + property.Key.Replace("/", "//"), property.Value, false);

		if (showVendorIcons)
			node.icon = GetImageForKey(property.Key);

			nodes.Add(node);
		}
	}

	protected string GetImageForKey(string key)
	{
		const string vendorImageDir = "/NetPerfMon/images/Vendors/";
		if (vendorImageFileNames != null && vendorImageFileNames.ContainsKey(key))
		{
			string filename = vendorImageFileNames[key];
			if (!string.IsNullOrEmpty(filename))
			{
				string virtualPath = vendorImageDir + vendorImageFileNames[key].ToString().Trim();
				string physicalPath = Server.MapPath(virtualPath);
				if (File.Exists(physicalPath))
					return virtualPath;
			}
		}

		return vendorImageDir + "Unknown.gif";
	}


	private void GetChildNodes(List<ExtTreeNode> nodes, string groupBy, string rootNode)
	{
		// FB31069 - encode "//", which represents single slash in group id
		string[] pathParts = rootNode.Replace("//", "&#47;").Split(new char[] { '/' }, StringSplitOptions.None);
		pathParts = pathParts.Select(o => o.Replace("&#47;", "/")).ToArray();

		if (pathParts.Length == 0)
			return;

        UDTDeviceDAL deviceDAL = new UDTDeviceDAL();
        DataTable table = deviceDAL.GetNodesByProperty(groupBy, pathParts[pathParts.Length - 1], Request["nodeFilter"]);

		foreach (DataRow dataRow in table.Rows)
		{
			string name = dataRow["Caption"].ToString();
			ExtTreeNode node = new ExtTreeNode(rootNode + "/" + name, name, true);

            node.icon = SWISHelper.GetIconHandlerPath((int)dataRow["NodeId"], (int)dataRow["Status"], (int)dataRow["ChildStatus"], true);
			node.nodeId = (int)dataRow["NodeId"];
			node.ipAddress = dataRow["IPAddress"].ToString();

			nodes.Add(node);
		}
	}


    
	public class ExtTreeNode
	{
		public ExtTreeNode(string id, string text, bool leaf)
		{
			this.id = id;
			this.text = text;
			this.leaf = leaf;
			this.icon = null;
			this.uiProvider = "SW.UDT.SWTreeNodeUI";
			this.ipAddress = null;
		}

		public string id;
		public string text;
		public bool leaf;
		public string icon;
		public string uiProvider;

		public string ipAddress;
		public int nodeId;
	}    

</script>

