﻿<%@ WebService Language="C#" Class="ChartData" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

using SolarWinds.UDT.Web;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.Logging;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.UDT.Common;
using Resources;
using SolarWinds.Orion.Web.InformationService;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ChartData : ChartDataWebService
{
    static Log log = new Log();
    //Modifying the below constant would require considering constants defined in the file charts.UDT.PieChartLegend.js (createLegend method, constant name:maxRowsToShow) and AsyncSearchResult.cs (SearchVendor method, constant name:topVendorsTobeSkipped) 
    private int MAX_VENDORS_NUMBER = UDTSettingDAL.VendorPieChartSliceCount;

    [WebMethod]
    public ChartDataResults EndpointVendors(ChartDataRequest request)
    {
        var dataSeries = new DataSeries();
        dataSeries.TagName = "Default";

        var dataTable = UDTEndpointDAL.GetVendorsWithEndpointsQty();
        
        long totCount = 0;
        foreach (DataRow row in dataTable.Rows)
        {
            totCount += (int)(row["Qty"]);
        }

        int i = 1;
        int otherQty = 0;
        foreach (DataRow row in dataTable.Rows)
        {
           

            //Making extra data points not visible to limit number of vendors in chart
            //but have all vendors for legend
            if (i <= MAX_VENDORS_NUMBER && row["Vendor"]!=null && row["Vendor"].ToString().ToLower() != "unknown")
            {
                var values = new Dictionary<string, object>();
                values["id"] = row["Vendor"];
                values["y"] = row["Qty"];
                values["name"] = row["Vendor"];
                values["legendLabel"] = row["Vendor"];
                values["count"] = totCount;
                values["visible"] = true;
                var dp = DataPointWithOptions.CreatePointWithOptions(values);
                dataSeries.AddPoint(dp);
                i++;
            }
            else
            {              
                otherQty += (int)(row["Qty"]);
            }

           
           
        }

        //Creating aggregate data point to limit number of vendors in chart
        if (otherQty > 0)
        {
            var values = new Dictionary<string, object>();

            values["id"] = "Other";
            values["y"] = otherQty;
            values["name"] = "Other";
            values["legendLabel"] = "Other";
            values["count"] = totCount;
            values["visible"] = true;

            var dp = DataPointWithOptions.CreatePointWithOptions(values);
            dataSeries.AddPoint(dp);
        }

        var res = new ChartDataResults(new DataSeries[] { dataSeries });
        res.ChartOptionsOverride = new
        {
            DefaultSubtitle = Resources.UDTWebContent.UDTWEBDATA_VK1_1,
            ChartTypeLabel = Resources.UDTWebContent.UDTWEBDATA_VK1_2,
            HeaderColumnLabel = Resources.UDTWebContent.UDTWEBDATA_VK1_3,
            FooterTotalLabel = "",
            Formatter = "empty"
        };
        return res;
    }
}
