﻿<%@ WebService Language="C#" CodeBehind="~/App_Code/SwitchToRouter.cs" Class="SwitchToRouter" %>
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.Services.Protocols;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.UDT.Web;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Common.Models;
using SolarWinds.UDT.Web.Helpers;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class SwitchToRouter : System.Web.Services.WebService
{

    private static readonly Log _log = new Log();

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetSelectedRouters()
    {
        
        try
        {

            Dictionary<int, string[]> routerList =
                (Dictionary<int, string[]>) HttpContext.Current.Session["UDT_SwitchToRouter_State"];
            
            int numberOfRecords = 0;
            if (routerList != null)
            {
                numberOfRecords = routerList.Count;
            }

            DataTable tmpTable = new DataTable();
            DataColumn dc = new DataColumn("router");
            tmpTable.Columns.Add(dc);
            DataColumn dc1 = new DataColumn("ip");
            tmpTable.Columns.Add(dc1);
            DataColumn dc2 = new DataColumn("id");
            tmpTable.Columns.Add(dc2);
            DataColumn dc3 = new DataColumn("delete");
            tmpTable.Columns.Add(dc3);
            
            foreach (KeyValuePair<int,string[]> router in routerList)
            {
                DataRow tmpRow = tmpTable.NewRow();
                tmpRow["router"] = router.Value[0];
                tmpRow["ip"] = router.Value[1];
                tmpRow["id"] = router.Key;
                tmpRow["delete"] = 0;
                tmpTable.Rows.Add(tmpRow);
            }

            PageableDataTable gridData = new PageableDataTable(tmpTable, numberOfRecords);
            return gridData;
        }
        catch (Exception ex)
        {
            _log.ErrorFormat("GetSelectedRouters failed.  Details: {0}", ex);
        }
        return null;

    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetRouters(string search)
    {
        
        int pageSize;
        int startRowNumber;
        int sortColumn;

        Int32.TryParse(this.Context.Request.QueryString["sort"], out sortColumn);
        string sortDirection = this.Context.Request.QueryString["dir"];

        Int32.TryParse(this.Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(this.Context.Request.QueryString["limit"], out pageSize);

        if (sortColumn < 1) sortColumn = 2;
        
        if (pageSize < 10)
        {
            if (!Int32.TryParse(WebUserSettingsDAL.Get("UDT_Plugins_SwitchToRouter_PageSize"), out pageSize))
                pageSize = 10;
        }
        
        try
        {
            string swql = String.Format(@"SELECT r.NodeID as [1], n.Caption as [2], n.IPAddress as [3], n.Location as [4], n.Contact as [5] FROM Orion.UDT.Router r, Orion.Nodes (nolock=true) n WHERE r.NodeID = n.NodeID AND (n.Caption LIKE '%{4}%' OR n.IPAddress LIKE '%{4}%' OR n.Location LIKE '%{4}%' OR n.Contact LIKE '%{4}%') ORDER BY [{0}] {1} WITH ROWS {2} TO {3}", sortColumn, sortDirection, startRowNumber + 1, startRowNumber + pageSize, search);
            DataTable tmpTable = SWISHelper.Query(swql);

            //Count(*)
            swql =
                String.Format(
                    @"SELECT COUNT(r.NodeID) AS Cunt FROM Orion.UDT.Router r, Orion.Nodes (nolock=true) n WHERE r.NodeID = n.NodeID AND (n.Caption LIKE '%{0}%' OR n.IPAddress LIKE '%{0}%' OR n.Location LIKE '%{0}%' OR n.Contact LIKE '%{0}%')",
                    search);
            DataTable tmpCount = SWISHelper.Query(swql);
            int numberOfRecords = Convert.ToInt32(tmpCount.Rows[0][0]);

            PageableDataTable gridData = new PageableDataTable(tmpTable, numberOfRecords);
            return gridData;
        }
        catch (Exception ex)
        {
            _log.ErrorFormat("GetSelectedRouters failed.  Details: {0}", ex);
        }
        return null;

    }

    [WebMethod(EnableSession = true)]
    public void AddDeleteRouters(object[] addRouters, object[] delRouters)
    {

        try
        {
       
            Dictionary<int, string[]> routerList =
                (Dictionary<int, string[]>)HttpContext.Current.Session["UDT_SwitchToRouter_State"];

            // add (list of) accounts
            if (addRouters != null)
            {

                foreach (Dictionary<string, object> router in addRouters)
                {
                    int routerId = (int)router["id"];
                    string[] routerProps = new string[2];
                    routerProps[0] = (string)router["name"];
                    routerProps[1] = (string)router["ip"];

                    routerList.Add(routerId, routerProps);
                }

            }

            foreach (Dictionary<string, object> router in delRouters)
            {
                int routerId = (int)router["id"];
                routerList.Remove(routerId);
            }
            
            //update state
            HttpContext.Current.Session["UDT_SwitchToRouter_State"] = routerList;
        }

        catch (Exception e)
        {
            string err = e.Message;
            _log.ErrorFormat("Error occurred adding to selected Router list.  Details: {0}", err);
        }
        return;

    }

    [WebMethod(EnableSession = true)]
    public void DeleteRouter(string routerID)
    {

        try
        {

            Dictionary<int, string[]> routerList =
                (Dictionary<int, string[]>)HttpContext.Current.Session["UDT_SwitchToRouter_State"];

            routerList.Remove(int.Parse(routerID));

            //update state
            HttpContext.Current.Session["UDT_SwitchToRouter_State"] = routerList;
        }

        catch (Exception e)
        {
            string err = e.Message;
            _log.ErrorFormat("Error occurred deleting from selected Router list.  Details: {0}", err);
        }
        return;

    }
}