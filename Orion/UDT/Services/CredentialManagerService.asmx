﻿<%@ WebService Language="C#" Class="CredentialManagerService" %>

using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.SharedCredentials.Exceptions;
using SolarWinds.UDT.Common;
using SolarWinds.UDT.Web.DAL;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class CredentialManagerService : System.Web.Services.WebService
{
    private const string UDT_CREDENTIAL_OWNER = Strings.UDTCredentialOwner; 
    private CredentialManager cMan = new CredentialManager();
    private static readonly Log log = new Log();

    public class UDTCredential
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }

    [WebMethod]
    public PageableDataTable GetCredentials()
    {
        try
        {
            int pageSize;
            int startRowNumber;

            string sortColumn = this.Context.Request.QueryString["sort"];
            string sortDirection = this.Context.Request.QueryString["dir"];

            Int32.TryParse(this.Context.Request.QueryString["start"], out startRowNumber);
            Int32.TryParse(this.Context.Request.QueryString["limit"], out pageSize);

            if (pageSize < 10)
            {
                pageSize = 10;
            }

            DataTable table = new DataTable();
            table.Columns.Add("CredentialID", typeof(string));
            table.Columns.Add("CredentialName", typeof(string));
            table.Columns.Add("Username", typeof(string));
            table.Columns.Add("NodesAssigned", typeof(string));
            table.Columns.Add("Control", typeof(string));

            var upCredentials = cMan.GetCredentials<UsernamePasswordCredential>(UDT_CREDENTIAL_OWNER, true);

            if (upCredentials.Count() > 0)
            {
                List<UsernamePasswordCredential> sortedCredentials = SortCredentials(upCredentials, sortColumn, sortDirection);

                Dictionary<int, int> nodeCountByCredentialId = new UDTDeviceDAL().GetNodesAssignedCountsByUDTCredentialIds();

                for (int i = startRowNumber; i < startRowNumber + pageSize; i++)
                {
                    if (i >= sortedCredentials.Count)
                        break;

                    var r = table.NewRow();
                    r["CredentialID"] = sortedCredentials[i].ID.Value;
                    r["CredentialName"] = sortedCredentials[i].Name;
                    r["Username"] = sortedCredentials[i].Username;

                    if (nodeCountByCredentialId.ContainsKey(sortedCredentials[i].ID.Value))
                    {
                        r["NodesAssigned"] = nodeCountByCredentialId[sortedCredentials[i].ID.Value];
                    }
                    else
                    {
                        r["NodesAssigned"] = 0;
                    }

                    r["Control"] = "UsernamePasswordCredential";

                    table.Rows.Add(r);
                }
            }

            return new PageableDataTable(table, upCredentials.Count());
        }
        catch (Exception ex)
        {
            log.Error("Exception occured when getting credential table", ex);
            throw;
        }
    }

    private List<UsernamePasswordCredential> SortCredentials(IEnumerable<UsernamePasswordCredential> credSource, string sortColumn, string direction)
    {
        bool asc = direction == "ASC";

        if (sortColumn == "CredentialName")
        {
            if (asc)
                return credSource.OrderBy((c) => c.Name).ToList();
            else
                return credSource.OrderByDescending((c) => c.Name).ToList();
        }
        else if (sortColumn == "Username")
        {
            if (asc)
                return credSource.OrderBy((c) => c.Username).ToList();
            else
                return credSource.OrderByDescending((c) => c.Username).ToList();
        }
        else if (sortColumn == "NodesAssigned")
        {
            return credSource.ToList();
        }
        else
        {
            return credSource.OrderBy((c) => c.Name).ToList();
        }
    }

    [WebMethod]
    public UDTCredential GetUDTCredential(string credentialId)
    {
        try
        {
            if (String.IsNullOrEmpty(credentialId))
                return null;

            var cred = this.cMan.GetCredential<UsernamePasswordCredential>(UDT_CREDENTIAL_OWNER, Convert.ToInt32(credentialId), true);

            if (cred == null)
                return null;

            return new UDTCredential()
            {
                ID = cred.ID.Value,
                Name = cred.Name,
                Username = cred.Username,
                Password = String.Empty.PadLeft(cred.Password.Length, '*')
            };
        }
        catch (Exception ex)
        {
            log.Error(String.Format("Exception occured when getting UDT credential {0}", credentialId), ex);
            throw;
        }
    }

    [WebMethod]
    public void DeleteCredentials(List<string> credentialIDs)
    {
        if (credentialIDs == null && credentialIDs.Count == 0)
            return;

        string currentId = null;

        try
        {
            Dictionary<int, int> nodeCountByCredentialId = new UDTDeviceDAL().GetNodesAssignedCountsByUDTCredentialIds();
            
            foreach (string id in credentialIDs)
            {
                currentId = id;
                
                int idNumber = Convert.ToInt32(id);

                if (nodeCountByCredentialId.ContainsKey(idNumber) && nodeCountByCredentialId[idNumber] > 0)
                {
                    log.ErrorFormat("Crdential {0} will not be deleted because there are {1} node using it", id, nodeCountByCredentialId[idNumber]);
                    continue;
                }

                this.cMan.DeleteCredential(UDT_CREDENTIAL_OWNER, idNumber);
            }
        }
        catch (Exception ex)
        {
            log.Error(String.Format("Exception occured when deleting UDT credential {0}", currentId), ex);
            throw;
        }
    }

    [WebMethod]
    public bool InsertUpdateUDTCredential(string credentialId, string name, string username, string password)
    {
        try
        {
            if (String.IsNullOrEmpty(name))
                return false;

            if (String.IsNullOrEmpty(username))
                return false;

            if (String.IsNullOrEmpty(credentialId))
            {
                List<UsernamePasswordCredential> creds = new List<UsernamePasswordCredential>();

                try
                {
                    creds.AddRange(this.cMan.GetCredentials<UsernamePasswordCredential>(UDT_CREDENTIAL_OWNER, true));

                    if (creds.Any((c) => c.Name == name))
                        return false;
                }
                catch (CredentialNotFoundException)
                {
                    // no credentials found (why the exception ???), however, do nothing, proceed to insert
                }

                this.cMan.AddCredential<UsernamePasswordCredential>(UDT_CREDENTIAL_OWNER, new UsernamePasswordCredential()
                {
                    Description = null,
                    Name = name,
                    Password = password,
                    Username = username
                });

                return true;
            }
            else
            {
                var cred = this.cMan.GetCredential<UsernamePasswordCredential>(UDT_CREDENTIAL_OWNER, Convert.ToInt32(credentialId), true);

                if (cred != null)
                {
                    cred.Username = username;
                    cred.Password = password;

                    this.cMan.UpdateCredential<UsernamePasswordCredential>(UDT_CREDENTIAL_OWNER, cred);                    
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        catch (Exception ex)
        {
            log.Error(String.Format("Exception occured when inserting/updating UDT credential {0}", name), ex);
            throw;
        }
    }

}


