<%@ WebService Language="C#" Class="UdtPortManagement" %>

using System;
using System.Linq;
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.UDT.Common;
using SolarWinds.UDT.Common.Models;
using System.Collections.Generic;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class UdtPortManagement : WebService
{
    Log log = new Log();
    
  
    [WebMethod(EnableSession = true)]
    public void UdtChangePortMonitoringState(int[] portIds, bool state)
    {
        if (portIds == null) return;
        
        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                bl.PollingSetPortMonitoredState(portIds, state);
            }
        }
        catch (Exception ex)
        {
            log.Error("Error in UdtChangePortMonitoringState function.", ex);
            
            string ids = string.Join(",", portIds.Select(i => i.ToString()).ToArray());
            throw new Exception(String.Format("UDT setting port monitoring state to {1} failed for portIds {0}.", ids, state));
        }
    }

    [WebMethod(EnableSession = true)]
    public bool UdtDeleteNodes(int[] netObjectIds)
    {
        if (netObjectIds == null) return true;
        
        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                //TODO: this 'foreach' is a temporary fix for FB74197 (should be later changed back to bl.PollingRemoveNodes(ids)
                // and a new web method added UdtDeleteNode(int netObjectId)
                foreach(var id in netObjectIds)
                    bl.PollingRemoveNode(id);
            }
            return true;
        }
        catch (Exception ex)
        {
            log.Error("Error in UdtDeleteNodes function.", ex);

            string ids = string.Join(",", netObjectIds.Select(i => i.ToString()).ToArray());
            throw new Exception(String.Format("UDT deleting nodes failed for nodeIds {0}.", ids));
        }
    }
    
    [WebMethod(EnableSession = true)]
    public bool UdtDeletePorts(int[] netObjectIds)
    {
        if (netObjectIds == null) return true;
            
        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                bl.PollingSetPortsExcludedState(netObjectIds, true);
            }
            return true;
        }

        catch (Exception ex)
        {
            log.Error("Error in UdtDeletePorts function.", ex);

            string ids = string.Join(",", netObjectIds.Select(i => i.ToString()).ToArray());
            throw new Exception(String.Format("UDT deleting ports failed for portIds {0}.", ids));
        }
    }

    [WebMethod(EnableSession = true)]
    public bool UdtImportNodes(int[] netObjectIds)
    {
        if (netObjectIds == null) return true;

        UDTCapability[] capabilities = new UDTCapability[netObjectIds.Length * 2];
        int n = 0;
        foreach (int nodeId in netObjectIds)
        {
            capabilities[n++] = new UDTCapability()
            {
                Capability = UDTNodeCapability.Layer2,
                NodeID = nodeId,
                Enabled = true,
                LastScanResult = ResultStatus.Success,
                Options = -1,
                PollingIntervalMinutes = SolarWinds.UDT.Common.Settings.GetDefaultPollingIntervalMinutes(UDTNodeCapability.Layer2)
            };

            capabilities[n++] = new UDTCapability()
            {
                Capability = UDTNodeCapability.Layer3,
                NodeID = nodeId,
                Enabled = true,
                LastScanResult = ResultStatus.Success,
                Options = -1,
                PollingIntervalMinutes = SolarWinds.UDT.Common.Settings.GetDefaultPollingIntervalMinutes(UDTNodeCapability.Layer3)
            };
        }
        
        try
        {   
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                bl.PollingSetImportedNodes(netObjectIds, Context.User.Identity.Name);
                bl.PollingSetNodesCapabilities(capabilities);
                bl.PollingCheckUpdateJobs4Nodes(netObjectIds, true);
            }
            return true;
        }

        catch (Exception ex)
        {
            log.Error("Error in UdtImportNodes function.", ex);

            string ids = string.Join(",", netObjectIds.Select(i => i.ToString()).ToArray());
            throw new Exception(String.Format("UDT importing nodes failed for nodeIds {0}.", ids));
        }        
    }
    
    // poll now the UDT node
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void UdtPollNodesNow(string netObjectIds)
    {
        var nodeObjectIds = netObjectIds.Split(',');
        
        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                foreach (string nodeObjectId in nodeObjectIds)
                {
                    int nodeId = Convert.ToInt32(nodeObjectId);

                    var capbilities = bl.PollingGetAllNodeCapabilities(nodeId);

                    foreach (var c in capbilities)
                    {
                        bl.PollingPollNow(nodeId, UDTCapability.ToJobType(c.Capability));    
                    }
                }
            }
        }
        catch (Exception ex)
        {
            log.Error("Error in UdtPollNodesNow function.", ex);
            throw new Exception(String.Format("UDT Poll Nodes now failed for netobjects {0}.", netObjectIds ));
        }
    }
    [WebMethod(EnableSession = true)]
    public void AdminstrativelyShutDown(int nodeID, int[] portIds,bool status)
    {
        string state = string.Empty;
        if (status)
            state = "Administrative Shutdown";
        else
            state = "Administrative Enable";
        
        if (nodeID <=0 || portIds == null) return;

        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                bl.MngSetPortAdminStatus(nodeID, portIds,
                                         status ? PortAdministrativeStatus.Down : PortAdministrativeStatus.Up, System.Web.HttpContext.Current.Request.UserHostAddress, System.Web.HttpContext.Current.User.Identity.Name);
               
            }
        }
        catch (Exception ex)
        {
            log.Error("Error in " + state + " function.", ex);


            string ids = string.Join(",", portIds.Select(i => i.ToString()).ToArray());
            throw new Exception(String.Format("UDT setting " + state + "  to {1} failed for portIds {0}.", ids,
                                              ex.Message));
        }

    }
    [WebMethod(EnableSession = true)]
    public string GetRWCommunityNotDefinedNodes(int[] nodeIDs)
    {
        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                return bl.GetRWCommunityNotDefinedNodes(nodeIDs);
            }
        }catch(Exception ex)
        {
            log.Error("Error in GetRWCommunityNotDefinedNodes function", ex);
            string ids = string.Join(",", nodeIDs.Select(i => i.ToString()).ToArray());
            throw new Exception(String.Format("Get RwCommunityNotDefinedNodes for nodeIds{0}", ids));
        }
    }

    
}
