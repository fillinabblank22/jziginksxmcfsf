﻿<%@ WebService Language="C#" Class="Wireless" %>

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.Caching;
using System.Web.Configuration;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Web.Helpers;
using System.Linq;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class Wireless : WebService 
{
    private static readonly Log _log = new Log();

    [WebMethod]
    public Dictionary<string, object> LoadWirelessCurrentEndpointConnections(int pageNumber, int rowsPerPage, string searchText, bool sortDescending, string wirelessObject, bool apMode, bool useCacheIfPossible)
    {
        var results = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

        // TODO: UI: resource data cache - passing inside wirelessOblect a combination on netObject and ResourceID is a workaround and need to be addressed in next release
        var dataCacheKey = wirelessObject;
        wirelessObject = HtmlHelper.ExtractWirelessObject(wirelessObject);
        
        string warningMsg;

        // first try to get data without submitting search text into query
        var data = GetDataWirelessCurrentEndpointConnections(searchText, wirelessObject, apMode, useCacheIfPossible, dataCacheKey, out warningMsg);

        // apply filterring 
        if (!string.IsNullOrEmpty(searchText) && data.Rows.Count > 0)
        {

            searchText = DALHelper.FormatSearchTextForQuery(searchText, false);
            
            using (var view = new DataView(data))
            {
                var filter = string.Format(@"EndpointName LIKE {0} OR AccountName LIKE {0} ", searchText);
                if (data.Columns.Contains("SSID"))
                {
                    filter = filter + string.Format(@"OR SSID LIKE {0} ", searchText);
                }
                view.RowFilter = filter;

                data = view.ToTable();
            }
        }
        
        // Paging
        if (rowsPerPage < 0)
            rowsPerPage = UDTSettingDAL.ShowAllMaxRows;

        int totalRows = data.Rows.Count;
        int firstIndex = (pageNumber - 1) * rowsPerPage;

        var pageData = data.Clone();

        for (int i = firstIndex; i < firstIndex + rowsPerPage; i++)
        {
            if (i < totalRows)
                pageData.ImportRow(data.Rows[i]);
        }
        data = pageData;

        //build the table
        HtmlTable htmlTable = new HtmlTable();


        //build the table
        foreach (DataRow row in data.Rows)
        {

            // build the table rows
            var htmlRow = new HtmlTableRow();

            string endpointName = HtmlHelper.GetEndpointNameHref((int)row["WatchId"],
                                                                 row["WatchName"].ToString(),
                                                                 row["WatchItem"].ToString(),
                                                                 row["WatchItemType"].ToString(),
                                                                 row["EndpointDNS"].ToString(),
                                                                 row["EndpointIP"].ToString(),
                                                                 row["EndpointMAC"].ToString());

            htmlRow.Cells.Add(HtmlHelper.NewTableCell(endpointName));

            htmlRow.Cells.Add(
                HtmlHelper.NewTableCell(HtmlHelper.CreateUdtViewHref("user", row["UserID"].ToString(),
                                                                     row["AccountName"].ToString())));

            var firstUpdate = (DateTime)row["FirstUpdate"];
            var lastUpdate = (DateTime)row["LastUpdate"];

            // get the connection duration
            string connectionDuration;
            if (firstUpdate >= lastUpdate)
            {
                var pollInterval = (int)row["PollInterval"];
                // FirstUpdate and LastUpdate are equal, or lastUpdate is less (may be initial poll of device), so show the 'Less than [node wireless polling period]' message
                //int pollInterval = UDTWirelessDAL.GetWirelessPollIntervalForNode(Convert.ToInt32(row["NodeID"]));
                connectionDuration = string.Format(Resources.UDTWebContent.UDTWCODE_VB1_48, pollInterval);
            }
            else
            {
                connectionDuration = HtmlHelper.GetTimeDiff(firstUpdate, lastUpdate);
            }
            htmlRow.Cells.Add(HtmlHelper.NewTableCell(connectionDuration));

            // add the SSID if resource is for an Access Point
            if (apMode)
            {
                htmlRow.Cells.Add(HtmlHelper.NewTableCell(IconHelper.GetSSIDIcon(row["SSID"].ToString())));
                htmlRow.Cells.Add(HtmlHelper.NewTableCell(HtmlHelper.CreateUdtViewHref("ssid", row["SSID"].ToString(), row["SSID"].ToString())));
            }
else
{
     htmlRow.Cells.Add(HtmlHelper.NewTableCell(""));
 htmlRow.Cells.Add(HtmlHelper.NewTableCell(""));
}

            htmlTable.Rows.Add(htmlRow);
        }
        
        var writer = new StringWriter();
        using (var htmlTextWriter = new HtmlTextWriter(writer))
        {
            htmlTable.RenderControl(htmlTextWriter);
        }

        results["totalRows"] = totalRows;
        results["htmlRows"] = writer.ToString();
        results["rowsPerPageActual"] = rowsPerPage;
        results["errorMessage"] = warningMsg;
        
        return results;
    }


    [WebMethod]
    public Dictionary<string, object> LoadWirelessAllEndpointConnections(int pageNumber, int rowsPerPage, string searchText, bool sortDescending, string timePeriod, string wirelessObject, bool apMode, bool useCacheIfPossible)
    {
        var results = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

        var periodBegin = new DateTime();
        var periodEnd = new DateTime();
        Periods.Parse(ref timePeriod, ref periodBegin, ref periodEnd);

        // TODO: UI: resource data cache - passing inside wirelessOblect a combination on netObject and ResourceID is a workaround and need to be addressed in next release
        var dataCacheKey = wirelessObject;
        wirelessObject = HtmlHelper.ExtractWirelessObject(wirelessObject);
        
        string warningMsg;

        // first try to get data without submitting search text into query
        var data = GetDataWirelessAllEndpointConnections(searchText, wirelessObject, periodEnd, periodBegin, apMode, useCacheIfPossible, dataCacheKey, out warningMsg);

        // apply filterring 
        if (!string.IsNullOrEmpty(searchText) && data.Rows.Count > 0)
        {

            searchText = DALHelper.FormatSearchTextForQuery(searchText, false);

            using (var view = new DataView(data))
            {
                var filter = string.Format(@"EndpointName LIKE {0} ", searchText);

                if (data.Columns.Contains("SSID"))
                {
                    filter = filter + string.Format(@"OR SSID LIKE {0} ", searchText);
                }
                view.RowFilter = filter;

                data = view.ToTable();
            }
        }

        // Paging
        if (rowsPerPage < 0)
            rowsPerPage = UDTSettingDAL.ShowAllMaxRows;

        int totalRows = data.Rows.Count;
        int firstIndex = (pageNumber - 1) * rowsPerPage;

        var pageData = data.Clone();

        for (int i = firstIndex; i < firstIndex + rowsPerPage; i++)
        {
            if (i < totalRows)
                pageData.ImportRow(data.Rows[i]);
        }
        data = pageData;

        //build the table
        HtmlTable htmlTable = new HtmlTable();

        //build the table
        foreach (DataRow row in data.Rows)
        {
            // build the table rows=
            var htmlRow = new HtmlTableRow();

            var firstUpdate = (DateTime)row["FirstUpdate"];
            var lastUpdate = (DateTime)row["LastUpdate"];

            // add the period column data
            htmlRow.Cells.Add(HtmlHelper.NewTableCell(string.Format(Resources.UDTWebContent.UDTWCODE_VB1_49, firstUpdate, ((int)row["IsCurrent"] > 0 ? Resources.UDTWebContent.UDTWCODE_VB1_50 : lastUpdate.ToString()))));

            string endpointName = HtmlHelper.GetEndpointNameHref((int)row["WatchId"],
                                                                 row["WatchName"].ToString(),
                                                                 row["WatchItem"].ToString(),
                                                                 row["WatchItemType"].ToString(),
                                                                 row["EndpointDNS"].ToString(),
                                                                 DALHelper.IpAddressToDisplayFormat(row["EndpointIP"].ToString()),
                                                                 DALHelper.MacAddressToDisplayFormat(row["EndpointMAC"].ToString()));

            htmlRow.Cells.Add(HtmlHelper.NewTableCell(endpointName));

            // get the connection duration
            string connectionDuration;
            if (firstUpdate == lastUpdate)
            {
                var pollInterval = (int)row["PollInterval"];
                // FirstUpdate and LastUpdate are equal, or lastUpdate is less (may be initial poll of device), so show the 'Less than [node wireless polling period]' message
                connectionDuration = string.Format(Resources.UDTWebContent.UDTWCODE_VB1_48, pollInterval);
            }
            else
            {
                connectionDuration = HtmlHelper.GetTimeDiff(firstUpdate, lastUpdate);
            }
            htmlRow.Cells.Add(HtmlHelper.NewTableCell(connectionDuration));


            // add the SSID if resource is for an Access Point
            if (apMode)
            {
                htmlRow.Cells.Add(HtmlHelper.NewTableCell(IconHelper.GetSSIDIcon(row["SSID"].ToString())));
                htmlRow.Cells.Add(HtmlHelper.NewTableCell(HtmlHelper.CreateUdtViewHref("ssid", row["SSID"].ToString(), row["SSID"].ToString())));
            }
else
{
     htmlRow.Cells.Add(HtmlHelper.NewTableCell(""));
 htmlRow.Cells.Add(HtmlHelper.NewTableCell(""));
}

            htmlTable.Rows.Add(htmlRow);
        }

        var writer = new StringWriter();
        using (var htmlTextWriter = new HtmlTextWriter(writer))
        {
            htmlTable.RenderControl(htmlTextWriter);
        }

        results["totalRows"] = totalRows;
        results["htmlRows"] = writer.ToString();
        results["rowsPerPageActual"] = rowsPerPage;
        results["errorMessage"] = warningMsg;

        return results;
    }


    [WebMethod]
    public Dictionary<string, object> LoadWirelessSSIDs(int pageNumber, int rowsPerPage, string searchText, bool sortDescending, string wirelessObject, bool useCacheIfPossible)
    {
        var results = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

        // TODO: UI: resource data cache - passing inside wirelessOblect a combination on netObject and ResourceID is a workaround and need to be addressed in next release
        var dataCacheKey = wirelessObject;
        wirelessObject = HtmlHelper.ExtractWirelessObject(wirelessObject);
        
        string warningMsg;

        // first try to get data without submitting search text into query
        var data = GetDataWirelessSSIDs(searchText, wirelessObject, useCacheIfPossible, dataCacheKey, out warningMsg);

        // apply filterring 
        if (!string.IsNullOrEmpty(searchText) && data.Rows.Count > 0)
        {
            int channel;
            bool searchChannel = int.TryParse(searchText, out channel);

            searchText = DALHelper.FormatSearchTextForQuery(searchText, false);

            using (var view = new DataView(data))
            {
                var filter = string.Format(@"SSID LIKE {0} ", searchText);
                if (searchChannel) filter = filter + string.Format(@"OR Channel = {0}", channel);

                view.RowFilter = filter;

                data = view.ToTable();
            }
        }

        // Paging
        if (rowsPerPage < 0)
            rowsPerPage = UDTSettingDAL.ShowAllMaxRows;

        int totalRows = data.Rows.Count;
        int firstIndex = (pageNumber - 1) * rowsPerPage;

        var pageData = data.Clone();

        for (int i = firstIndex; i < firstIndex + rowsPerPage; i++)
        {
            if (i < totalRows)
                pageData.ImportRow(data.Rows[i]);
        }
        data = pageData;

        //build the table
        HtmlTable htmlTable = new HtmlTable();


        foreach (DataRow ssid in data.Rows)
        {
            int endpointCount = DALHelper.GetValueOrDefault(ssid["Clients"], 0);

            // build the table rows
            var htmlRow = new HtmlTableRow();
            htmlRow.Cells.Add(HtmlHelper.NewTableCell(IconHelper.GetSSIDIcon(ssid["SSID"].ToString())));
            htmlRow.Cells.Add(HtmlHelper.NewTableCell(HtmlHelper.CreateUdtViewHref("ssid", ssid["SSID"].ToString(), ssid["SSID"].ToString())));
            htmlRow.Cells.Add(HtmlHelper.NewTableCell(ssid["Channel"].ToString()));
            htmlRow.Cells.Add(HtmlHelper.NewTableCell(String.Format(Resources.UDTWebContent.UDTWCODE_VB1_45, endpointCount)));

            htmlTable.Rows.Add(htmlRow);

        }

        var writer = new StringWriter();
        using (var htmlTextWriter = new HtmlTextWriter(writer))
        {
            htmlTable.RenderControl(htmlTextWriter);
        }

        results["totalRows"] = totalRows;
        results["htmlRows"] = writer.ToString();
        results["rowsPerPageActual"] = rowsPerPage;
        results["errorMessage"] = warningMsg;

        return results;
    }


    [WebMethod]
    public Dictionary<string, object> LoadWirelessAllAccessPoints(int pageNumber, int rowsPerPage, string searchText, bool sortDescending, string wirelessObject, bool useCacheIfPossible)
    {
        var results = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

        // TODO: UI: resource data cache - passing inside wirelessOblect a combination on netObject and ResourceID is a workaround and need to be addressed in next release
        var dataCacheKey = wirelessObject;
        wirelessObject = HtmlHelper.ExtractWirelessObject(wirelessObject);
        
        string warningMsg;

        // first try to get data without submitting search text into query
        var data = GetDataWirelessAllAccessPoints(searchText, wirelessObject, useCacheIfPossible, dataCacheKey, out warningMsg);

        // apply filterring 
        if (!string.IsNullOrEmpty(searchText) && data.Rows.Count > 0)
        {
            int channel;
            bool searchChannel = int.TryParse(searchText, out channel);

            searchText = DALHelper.FormatSearchTextForQuery(searchText, false);

            using (var view = new DataView(data))
            {
                var filter = string.Format(@"DisplayName LIKE {0} ", searchText);
                if (searchChannel) filter = filter + string.Format(@"OR Channel = {0}", channel);

                view.RowFilter = filter;

                data = view.ToTable();
            }
        }

        // Paging
        if (rowsPerPage < 0)
            rowsPerPage = UDTSettingDAL.ShowAllMaxRows;

        int totalRows = data.Rows.Count;
        int firstIndex = (pageNumber - 1) * rowsPerPage;

        var pageData = data.Clone();

        for (int i = firstIndex; i < firstIndex + rowsPerPage; i++)
        {
            if (i < totalRows)
                pageData.ImportRow(data.Rows[i]);
        }
        data = pageData;

        //build the table
        HtmlTable htmlTable = new HtmlTable();


        foreach (DataRow ap in data.Rows)
        {

            long apId = DALHelper.GetValueOrDefault(ap["AccessPointID"], 0L);
            int endpointCount = DALHelper.GetValueOrDefault(ap["Clients"], 0);
            string apName = ap["DisplayName"].ToString();

            // get the AccessPoint Status and WirelessType to set the wireless icon
            int status = DALHelper.GetValueOrDefault(ap["Status"], 0);
            string wirelessType = ap["WirelessType"].ToString();

            // build the table rows
            var htmlRow = new HtmlTableRow();

            htmlRow.Cells.Add(HtmlHelper.NewTableCell(IconHelper.GetWirelessIcon(status, wirelessType)));
            htmlRow.Cells.Add(
                HtmlHelper.NewTableCell(String.Format("<a href='{1}'>{0}</a>",
                                                      HtmlHelper.CreateUdtViewHref("ap", apId.ToString(CultureInfo.InvariantCulture), apName),
                                                      ap["Index"])));
            htmlRow.Cells.Add(HtmlHelper.NewTableCell(ap["Channel"].ToString()));
            htmlRow.Cells.Add(HtmlHelper.NewTableCell(String.Format(Resources.UDTWebContent.UDTWCODE_VB1_45, endpointCount)));

            htmlTable.Rows.Add(htmlRow);

        }

        var writer = new StringWriter();
        using (var htmlTextWriter = new HtmlTextWriter(writer))
        {
            htmlTable.RenderControl(htmlTextWriter);
        }

        results["totalRows"] = totalRows;
        results["htmlRows"] = writer.ToString();
        results["rowsPerPageActual"] = rowsPerPage;
        results["errorMessage"] = warningMsg;

        return results;
    }

    private static DataTable GetDataWirelessAllAccessPoints(string searchText, string wirelessObject, bool useCacheIfPossible, string dataCacheKey, out string warningMsg)
    {

        var context = HttpContext.Current;
        var cacheKey = "WirelessAllAccessPoints_" + HtmlHelper.GetSessionId(context) + dataCacheKey;
        var cacheTimeout = TimeSpan.FromMinutes(6);

        warningMsg = String.Empty;

        DataTable dataTable = null;

        if (useCacheIfPossible)
        {
            dataTable = context.Cache[cacheKey] as DataTable;
        }

        if (dataTable != null)
            return dataTable;

        dataTable = UDTWirelessDAL.GetAccessPointsForSsid(wirelessObject, searchText, out warningMsg);

        context.Cache.Insert(cacheKey, dataTable, null, DateTime.UtcNow + cacheTimeout, Cache.NoSlidingExpiration);
        return dataTable;
    }
    
    private static DataTable GetDataWirelessSSIDs(string searchText, string wirelessObject, bool useCacheIfPossible, string dataCacheKey, out string warningMsg)
    {

        var context = HttpContext.Current;
        var cacheKey = "WirelessSSIDs_" + HtmlHelper.GetSessionId(context) + dataCacheKey;
        var cacheTimeout = TimeSpan.FromMinutes(6);

        warningMsg = String.Empty;

        DataTable dataTable = null;

        if (useCacheIfPossible)
        {
            dataTable = context.Cache[cacheKey] as DataTable;
        }

        if (dataTable != null)
            return dataTable;

        dataTable = UDTWirelessDAL.GetSSIDsForAccessPoint(wirelessObject, searchText, out warningMsg);

        context.Cache.Insert(cacheKey, dataTable, null, DateTime.UtcNow + cacheTimeout, Cache.NoSlidingExpiration);
        return dataTable;
    }
    
    private static DataTable GetDataWirelessAllEndpointConnections(string searchText, string wirelessObject, DateTime periodEnd, DateTime periodBegin, bool apMode, bool useCacheIfPossible, string dataCacheKey, out string warningMsg)
    {

        var context = HttpContext.Current;
        var cacheKey = "WirelessAllEndpointConnections_" + HtmlHelper.GetSessionId(context) + dataCacheKey;
        var cacheTimeout = TimeSpan.FromMinutes(6);

        warningMsg = String.Empty;

        DataTable dataTable = null;

        if (useCacheIfPossible)
        {
            dataTable = context.Cache[cacheKey] as DataTable;
        }

        if (dataTable != null)
            return dataTable;

        dataTable = !apMode  //If we're not in "AP mode" for this resource, then data source is for SSID view.  
                        ? UDTWirelessDAL.GetAllEndpointsForSsid(wirelessObject, periodBegin, periodEnd, searchText, out warningMsg)
                        : UDTWirelessDAL.GetAllEndpointsForAccessPoint(wirelessObject, periodBegin, periodEnd, searchText, out warningMsg);
        context.Cache.Insert(cacheKey, dataTable, null, DateTime.UtcNow + cacheTimeout, Cache.NoSlidingExpiration);
        return dataTable;
    }
    
    private static DataTable GetDataWirelessCurrentEndpointConnections(string searchText, string wirelessObject, bool apMode, bool useCacheIfPossible, string dataCacheKey, out string warningMsg)
    {
        
        var context = HttpContext.Current;
        var cacheKey = "WirelessCurrentEndpointConnections_" + HtmlHelper.GetSessionId(context) + dataCacheKey;
        var cacheTimeout = TimeSpan.FromMinutes(6);

        warningMsg = String.Empty;
        
        DataTable dataTable = null;
        
        if (useCacheIfPossible)
        {
            dataTable = context.Cache[cacheKey] as DataTable;
        }

        if (dataTable != null)
            return dataTable;

        dataTable = !apMode  //If we're not in "AP mode" for this resource, then data source is for SSID view.  
                        ? UDTWirelessDAL.GetCurrentEndpointsForSsid(wirelessObject, searchText, out warningMsg)
                        : UDTWirelessDAL.GetCurrentEndpointsForAccessPoint(wirelessObject, searchText, out warningMsg);
        context.Cache.Insert(cacheKey, dataTable, null, DateTime.UtcNow + cacheTimeout, Cache.NoSlidingExpiration);
        return dataTable;
    }
    
}