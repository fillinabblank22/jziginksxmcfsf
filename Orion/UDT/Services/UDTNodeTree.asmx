<%@ WebService Language="C#" Class="UDTNodeTree" %>

using System;
using System.Drawing;
using System.Linq;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.Serialization.Json;
using Resources;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Logging;
using SolarWinds.UDT.Common;
using SolarWinds.UDT.Common.Models;
using SolarWinds.UDT.Web;
using RegistrySettings = SolarWinds.Orion.Core.Common.RegistrySettings;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Web.NetObjects;
using WellKnownElementTypes = SolarWinds.UDT.Common.WellKnownElementTypes;

/// <summary>
/// Summary description for NodeTree
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class UDTNodeTree : System.Web.Services.WebService
{
    private static readonly Log log = new Log();

    const int MaxNodesPerPage = 50;
    bool GroupNodesWithNullPropertiesAsUnknown;
    private bool rememberExpandedGroups;

    //public NodeTree()
    //{
    //}

    //private ResourceInfo GetDefaultViewBehavior()
    //{
    //    // required to be compatible with /Orion/Resource.aspx
    //    ResourceInfo resource = new ResourceInfo();
    //    resource.ID = 0;
    //    resource.View = new ViewInfo("", "", "", false, false, null);
    //    resource.View.ViewID = 0;
    //    resource.Column = 1;
    //    resource.View.ColumnCount = 1;
    //    resource.View.Column1Width = 800;
    //    return resource;
    //}

    [WebMethod(EnableSession = true)]
    public string GetTreeSection(int resourceId, string rootId, object[] keys, int startIndex)
    {
        log.DebugFormat("GetTreeSection({1}, {2}, {3}, {4}) from user {0}",
            Context.User.Identity.Name, resourceId, rootId, FormatArray(keys), startIndex);

        ResourceInfo resource = ResourceManager.GetResourceByID(resourceId);
        //if (resource == null) resource = GetDefaultViewBehavior();
        Context.Items[typeof(ViewInfo).Name] = resource.View;

        if (!bool.TryParse(resource.Properties["GroupNodesWithNullPropertiesAsUnknown"], out GroupNodesWithNullPropertiesAsUnknown))
            GroupNodesWithNullPropertiesAsUnknown = true;

        if (!bool.TryParse(resource.Properties["RememberCollapseState"], out rememberExpandedGroups))
            rememberExpandedGroups = true;

        string filter = GetFilterClause(resource);

        try
        {
            try
            {
                StringWriter stringWriter = new StringWriter();
                HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);

                BuildTreeLevel(htmlWriter, resource, rootId, keys.Length + 1, keys, startIndex, filter);

                return stringWriter.ToString();
            }
            catch (SqlException ex)
            {
                StringWriter stringWriter = new StringWriter();
                HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);

                BuildTreeLevel(htmlWriter, resource, rootId, keys.Length + 1, keys, startIndex, string.Empty);

                // No exception when running without the filter. Inform the user.

                return string.Format("Bad filter '{0}' specified. {1}", filter, ex.Message);
            }
        }
        catch (Exception ex)
        {
            log.ErrorFormat("Exception in GetTreeSection({1}, {2}, {3}, {4}) from user {0}. {5}",
                Context.User.Identity.Name, resourceId, rootId, FormatArray(keys), startIndex, ex);
            throw;
        }
    }
    
    
    [WebMethod(EnableSession = true)]
    public void CollapseTreeSection(int resourceId, object[] keys)
    {
        try
        {
            TreeStateManager manager = new TreeStateManager(Context.Session, resourceId);

            string keysArray = FormatArray(keys);
            manager.Collapse(keysArray);
        }
        catch (Exception ex)
        {
            log.ErrorFormat("Exception in CollapseTreeSection({1}, {2}) from user {0}. {3}",
                Context.User.Identity.Name, resourceId, FormatArray(keys), ex);
            throw;
        }
    }

    private string GetFilterClause(ResourceInfo resource)
    {
        string filter = resource.Properties["Filter"] ?? string.Empty;
        filter = CommonHelper.FormatFilter(filter.Trim());
        if (filter.Length > 0)
            filter = "(Nodes.NodeID IN (SELECT C.NodeID FROM UDT_NodeCapability C WHERE C.Capability=2) AND " + filter + ")";
        else
            filter = "(Nodes.NodeID IN (SELECT C.NodeID FROM UDT_NodeCapability C WHERE C.Capability=2))";
        return filter;
    }

    // returns true if property contains table name
    private bool ForeignProperty(string property, out string propTable, out string propName)
    {
        // properties from other tables are defined as <table>.<column name>
        if (property.Contains("."))
        {
            string[] props = property.Split('.');
            if (props.Length != 2)
                throw (new FormatException(String.Format("NodeTree resource - Sort property \"{0}\" has invalid format!", property)));

            propTable = props[0];
            propName = props[1];
            return true;
        }
        else
        {
            propTable = "Nodes";
            propName = property;
            return false;
        }
    }

    private class Parameters : Dictionary<string, object>
    {
    }

    private string GetGroupingWhereClause(ResourceInfo resource, object[] keys, string filter, Parameters parameters)
    {
        List<string> clauses = new List<string>();

        if (!string.IsNullOrEmpty(filter))
            clauses.Add(filter);

        for (int level = 0; level < Math.Min(keys.Length, 3); ++level)
        {
            string property = resource.Properties[string.Format("Grouping{0}", level + 1)];
            if (property == null)
            {
                property = "";
            }
            string table, prop;
            ForeignProperty(property, out table, out prop);

            if (string.IsNullOrEmpty(property) || property.Equals("none", StringComparison.OrdinalIgnoreCase))
                break;
            
            Type cpType = CustomPropertyMgr.GetTypeForProp(table, prop);
            object value = CustomPropertyMgr.ChangeType(keys[level], cpType);

            if (keys[level] == null)
                clauses.Add(string.Format("({1}.{0} IS NULL OR RTRIM(LTRIM({1}.{0}))='')", prop, table));
            else
            {
                clauses.Add(string.Format("{1}.{0}=@{0}", prop, table));
                parameters.Add(property, value);
            }
        }

        return string.Join(" AND ", clauses.ToArray());
    }

    private static string FormatArray(object[] values)
    {
        DataContractJsonSerializer serializer = new DataContractJsonSerializer(values.GetType());
        using (MemoryStream ms = new MemoryStream())
        {
            serializer.WriteObject(ms, values);
            return Encoding.UTF8.GetString(ms.ToArray());
        }
    }

    // returns the aggregated status of all the children (whose statuses may be based on their children)
    private void BuildTreeLevel(HtmlTextWriter writer, ResourceInfo resource, string rootId, int treeLevel, object[] keys, int startIndex, string filter)
    {
        Parameters parameters = new Parameters();
        string whereClause = GetGroupingWhereClause(resource, keys, filter, parameters);

        Parameters p = null;
        string join = BuildJoin(parameters, out p);

        string property = resource.Properties[string.Format("Grouping{0}", treeLevel)];
        if(property==null)
        {
            property ="";
        }
        
        TreeStateManager manager = new TreeStateManager(Context.Session, resource.ID);

        if (rememberExpandedGroups)
        {
            string keysArray = FormatArray(keys);
            manager.Expand(keysArray);
        }

        if (keys.GetUpperBound(0) > -1 && keys[0]!=null &&  keys[0].ToString().Contains("showInterfaces"))
        {
            DataTable ports;
            try
            { ports = getPortsForNode(int.Parse(rootId)); }
            catch
            { ports = null; }
            
            RenderInterfaces(writer, int.Parse(rootId), "contentsId", resource.Width,true,ports);
            
        }
        else
            {        
            if (string.IsNullOrEmpty(property) || property.Equals("none", StringComparison.OrdinalIgnoreCase))
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                BuildNodeList(writer, whereClause, parameters, rootId, resource, keys, startIndex, manager);
                writer.RenderEndTag();
            }
            else
            {
                string table;
                if (ForeignProperty(property, out table, out property))
                    join = String.Format("{1} LEFT JOIN {0} ON (Nodes.NodeID = {0}.NodeID) ", table, join);

                List<string> propertyValues = Node.GetJoinPropertyValues(join, table, property, whereClause, p);

                int index = 0;
                Action atEnd = null;
                for (int valueIndex = 0; valueIndex < propertyValues.Count; ++valueIndex)
                {
                    if (valueIndex < startIndex)
                        continue;

                    string childId = string.Format("{0}-{1}", rootId, index++);

                    if (valueIndex - startIndex >= MaxNodesPerPage)
                    {
                        RenderGetMoreLink(writer, childId, resource, keys, valueIndex, "groups");
                        break;
                    }

                    string key = propertyValues[valueIndex];

                    if (string.IsNullOrEmpty(key == null ? key : key.Trim()) && !GroupNodesWithNullPropertiesAsUnknown)
                    {
                        // for null keys, just render the next level in place
                        // defer until the end of the list
                        atEnd =
                            delegate
                                {
                                    BuildTreeLevel(writer, resource, childId, treeLevel + 1, ArrayAppend(keys, key), 0,
                                                   filter);
                                };
                    }
                    else
                    {
                        string displayKey = (key == null ? string.Empty : key.Trim());
                        if ((displayKey.Length == 0) || (property == "MachineType" && displayKey == "Unknown"))
                            displayKey = UDTWebContent.UDTWEBCODE_TM0_7;

                        RenderGroup(writer, resource, childId, treeLevel, property, displayKey, ArrayAppend(keys, key),
                                    filter, manager);
                    }
                }

                if (atEnd != null) atEnd();
            }
    }
}

    private object[] ArrayAppend(object[] array, object toAppend)
    {
        object[] newArray = new object[array.Length + 1];
        array.CopyTo(newArray, 0);
        newArray[array.Length] = toAppend;
        return newArray;
    }

    private void RenderGroup(HtmlTextWriter writer, ResourceInfo resource, string rootId, int treeLevel, string property, string key, object[] keys, string filter, TreeStateManager manager)
    {
        string toggleId = rootId + "-toggle";
        string contentsId = rootId + "-contents";

        Parameters parameters = new Parameters();
        string where = GetGroupingWhereClause(resource, keys, filter, parameters);

        writer.BeginRender();

        string keysArray = FormatArray(keys);
        bool needsExpanding = manager.IsExpanded(keysArray);

        writer.AddAttribute(HtmlTextWriterAttribute.Onclick,
            string.Format("UDT.UDTNodeTree.Click('{0}', {1}, {2}); return false;", rootId, resource.ID, FormatArray(keys)));
        writer.AddAttribute(HtmlTextWriterAttribute.Href, "#");

        if (needsExpanding)
            writer.AddAttribute("expand", "1");

        writer.RenderBeginTag(HtmlTextWriterTag.A);

        writer.AddAttribute(HtmlTextWriterAttribute.Src, "/Orion/images/Button.Expand.gif");
        writer.AddAttribute(HtmlTextWriterAttribute.Id, toggleId);
        writer.AddAttribute(HtmlTextWriterAttribute.Class, "Toggle");
        writer.RenderBeginTag(HtmlTextWriterTag.Img);
        writer.RenderEndTag(); // img

        Parameters p = null;
        string join = BuildJoin(parameters, out p);

        RenderGroupStatus(writer, Status.Summarize(Node.GetJoinGroupStatus(join, where, p)));

        string text;
        if (property == "Status")
            text = ((Status)Convert.ToInt32(key)).ToLocalizedString();
        else
            text = key;

        writer.Write(text); // users expect that HTML in the group name will be rendered, not quoted. see Orion TT#2745.
        //writer.WriteEncodedText(text);
        writer.RenderEndTag(); // a

        writer.WriteBreak();

        writer.AddAttribute(HtmlTextWriterAttribute.Id, contentsId);
        writer.AddStyleAttribute(HtmlTextWriterStyle.Display, "none");
        writer.AddStyleAttribute(HtmlTextWriterStyle.PaddingLeft, "16px");
        writer.RenderBeginTag(HtmlTextWriterTag.Div);
        writer.RenderEndTag(); // div

        writer.EndRender();
    }

    // returns join query for parameters not from Nodes table 
    // and save that parameters without table prefix
    // todo: dont add already existing joins
    string BuildJoin(Parameters parameters, out Parameters p)
    {
        p = new Parameters();
        string join = String.Empty;

        foreach (var param in parameters)
        {
            string table, prop;
            if (ForeignProperty(param.Key, out table, out prop))
            {
                // prarameter is not from Nodes table, we need add join
                join = String.Format("{1} LEFT JOIN {0} ON (Nodes.NodeID = {0}.NodeID) ", table, join);
            }

            // we need parameters to be without <table>. preffix
            p[prop] = param.Value;
        }

        return join;
    }

    private void BuildNodeList(HtmlTextWriter writer, string whereClause, Parameters parameters, string childId, ResourceInfo resource, object[] newKeys, int startIndex,TreeStateManager manager )
    {
        
        List<Node> nodes = new List<Node>();
        if (!string.IsNullOrEmpty(whereClause))
        {
            Parameters p = null;
            string join = BuildJoin(parameters, out p);

            foreach (Node n in Node.GetJoinFilteredNodes(join, whereClause, p))
                nodes.Add(n);
        }
        else
        {
            foreach (Node n in Node.GetAllObjects())
                nodes.Add(n);
        }
       
        nodes.Sort(delegate(Node x, Node y)
        {
            return string.Compare(x.Name, y.Name, true);
        });
        
        for (int nodeIndex = 0; nodeIndex < nodes.Count; ++nodeIndex)
        {
            if (nodeIndex < startIndex)
                continue;

            if (nodeIndex - startIndex >= MaxNodesPerPage)
            {
                RenderGetMoreLink(writer, childId, resource, newKeys, nodeIndex, "nodes");
                break;
            }
            
            string rootId = nodes[nodeIndex].NodeID.ToString();
            object[] keys = new object[1];
            //keys[0] = nodes[nodeIndex].Name.ToString();
            keys[0] = "showInterfaces" + rootId; 
            string keysArray = FormatArray(keys);
            bool needsExpanding = manager.IsExpanded(keysArray);
            
            
            DataTable ports;
            try
            {ports = getPortsForNode(nodes[nodeIndex].NodeID);}
            catch
            {ports = null;}

            //check if we have ports so we know whether ro render the expando button
            if (ports != null && ports.Rows.Count > 0)
            {
                //expando button
                string toggleId = rootId + "-toggle";
                string contentsId = rootId + "-contents";
                writer.AddAttribute(HtmlTextWriterAttribute.Onclick,
                                    string.Format("UDT.UDTNodeTree.InterfaceClick('{0}', {1}, {2}); return false;",
                                                  rootId, resource.ID, FormatArray(keys)));
                writer.AddAttribute(HtmlTextWriterAttribute.Href, "#");

                if (needsExpanding)
                    writer.AddAttribute("expand", "1");

                writer.RenderBeginTag(HtmlTextWriterTag.A);

                writer.AddAttribute(HtmlTextWriterAttribute.Src, "/Orion/images/Button.Expand.gif");
                writer.AddAttribute(HtmlTextWriterAttribute.Id, toggleId);
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "Toggle");
                writer.RenderBeginTag(HtmlTextWriterTag.Img);
                writer.RenderEndTag(); // img
                
                RenderNode(writer, nodes[nodeIndex]);
                writer.RenderEndTag(); // A
                writer.WriteBreak();
                // render interfaces for node here
                writer.AddStyleAttribute(HtmlTextWriterStyle.Display, "none");
                RenderInterfaces(writer, nodes[nodeIndex].NodeID, contentsId, resource.Width, needsExpanding, ports);
                // end expando button
            }
            else
            {
                //if there are no ports then render the node details but don't render the expand button or ports
                //this could occur where the user has chosen not to exclude certain port types ie. the flag column
                //in the UDT_Port table is set to 1
                writer.RenderBeginTag(HtmlTextWriterTag.A);
                RenderNode(writer, nodes[nodeIndex]);
                writer.RenderEndTag(); // A
                writer.WriteBreak();

            }
        }
    }

    private void RenderGetMoreLink(HtmlTextWriter writer, string childId, ResourceInfo resource, object[] keys, int index, string itemsType)
    {
        string getMoreId = string.Format("{0}-startFrom-{1}", childId, index);
        writer.AddAttribute(HtmlTextWriterAttribute.Id, getMoreId);
        writer.RenderBeginTag(HtmlTextWriterTag.Div);

        writer.AddAttribute(HtmlTextWriterAttribute.Onclick, string.Format("UDT.UDTNodeTree.GetMore('{0}', {1}, {2}, {3}); return false;",
            getMoreId, resource.ID, FormatArray(keys), index));
        writer.AddAttribute(HtmlTextWriterAttribute.Href, "#");
        writer.AddAttribute(HtmlTextWriterAttribute.Class, "getMore");
        writer.RenderBeginTag(HtmlTextWriterTag.A);
        writer.WriteEncodedText(string.Format("Get next {0} {1}", MaxNodesPerPage, itemsType));
        writer.RenderEndTag();

        writer.RenderEndTag();
    }

    private void RenderNode(HtmlTextWriter writer, Node node)
    {
        writer.BeginRender();

        writer.AddAttribute(HtmlTextWriterAttribute.Href, BaseResourceControl.GetViewLink(node));

        if (((ProfileCommon)Context.Profile).ToolsetIntegration)
        {
            writer.AddAttribute("IP", node.IPAddress.ToString());
            writer.AddAttribute("NodeHostname", node.Hostname);

            if (RegistrySettings.AllowSecureDataOnWeb())
            {
                writer.AddAttribute("Community", node.CommunityString);
            }
            else
            {
                string attrValue = FormatHelper.GetGuidParamString(WebCommunityStringsDAL.GetCommunityStringGuid(node.CommunityString));
                writer.AddAttribute("Community", attrValue);
            }
        }

        writer.RenderBeginTag(HtmlTextWriterTag.A);
        RenderNodeStatus(writer, node.NodeID, (int)node.Status.ParentStatus.Value, node.ChildStatus);
        writer.WriteEncodedText(node.Name);
        writer.RenderEndTag(); // a
        writer.EndRender();
    }

    private void RenderGroupStatus(HtmlTextWriter writer, Status status)
    {
        RenderStatus(writer, status.ToString("smallimgpath", null), status.ToLocalizedString());
    }

    private void RenderNodeStatus(HtmlTextWriter writer, int NodeId, int StatusId, int ChildStatus)
    {
        string alt = "Status";
        string url = SWISHelper.GetIconHandlerPath(NodeId, StatusId, ChildStatus, true);
        //string url = SolarWinds.Orion.Web.UI.StatusIcons.NodeIconFactory.SmallIconURL(NodeId, StatusId);
        RenderStatus(writer, url, alt);
    }

    private void RenderPortStatus(HtmlTextWriter writer, string status,string administrativeStatus)
    {
        if (administrativeStatus == "2")
            RenderStatus(writer, "/Orion/UDT/Images/Status/icon_port_shutdown_dot.gif", administrativeStatus);
        else
        {
            if (status == "1") //up
            {
                RenderStatus(writer, "/Orion/UDT/Images/Status/icon_port_active_dot.gif", status.ToString());
            }
            else //anything other than up, can actually have values 1 - 7
            {
                RenderStatus(writer, "/Orion/UDT/Images/Status/icon_port_inactive_dot.gif", status.ToString());
            }
        }
    }
    
    private void RenderStatus(HtmlTextWriter writer, string iconPath, string alt)
    {
        writer.BeginRender();

        writer.AddAttribute(HtmlTextWriterAttribute.Class, "StatusIcon");
        writer.AddAttribute(HtmlTextWriterAttribute.Alt, alt);
        writer.AddAttribute(HtmlTextWriterAttribute.Src, iconPath);
        writer.RenderBeginTag(HtmlTextWriterTag.Img);
        writer.RenderEndTag();

        writer.EndRender();
    }

    private void RenderInterfaces(HtmlTextWriter writer, int nodeId, string contentsId, int resourceWidth, bool needsExpanding, DataTable Ports)
    {
       
        // writer.AddAttribute(HtmlTextWriterAttribute.Href, BaseResourceControl.GetViewLink(node));

            
        writer.AddAttribute(HtmlTextWriterAttribute.Id, contentsId);
        writer.AddStyleAttribute(HtmlTextWriterStyle.PaddingLeft, "8px");
        
        writer.RenderBeginTag(HtmlTextWriterTag.Div);
        if (needsExpanding)
        {
            bool addExtraBreak = true;

            //Measure strings
            //font-family: Arial, Verdana, Helvetica, sans-serif;
            //font-size: 13px;
            Font font = new Font("Arial", 13, GraphicsUnit.Pixel);
            Bitmap bmp = new Bitmap(100, 100);
            Graphics gfx = Graphics.FromImage(bmp);
            //Calculate maxWidth of ports
            gfx.PageUnit = GraphicsUnit.Pixel;
            gfx.PageScale = 1.0F;
            float maxWidth = 0;
            foreach (DataRow row in Ports.Rows)
            {
                SizeF size = gfx.MeasureString(EmptyPortNameToUnknown(row["Name"].ToString()), font);
                if (size.Width > maxWidth) maxWidth = size.Width;
            }
            //set number of port columns
            int imagePadding = 100;
            int portIndent = 48;
            int portColumns = (int) ((resourceWidth - portIndent)/(maxWidth + imagePadding));
            if (portColumns > 8) portColumns = 8;
            if (portColumns%2 == 1)
            {
                portColumns = portColumns - 1;
            }
            if (portColumns < 1) portColumns = 1;

            int portColumnRelativeWidth = (int) (100/portColumns);

            //begin render table
            writer.RenderBeginTag(HtmlTextWriterTag.Table);
            int portIndex = 0;
            int columnOffset = 0;
            int totalPorts = Ports.Rows.Count;
            foreach (DataRow row in Ports.Rows)
            {
                columnOffset = (portIndex%portColumns);
                if (columnOffset == 0)
                {
                    //begin new row
                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                }
                //begin new cell
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "PortResourceTableEntry");
                writer.AddAttribute(HtmlTextWriterAttribute.Width, portColumnRelativeWidth + "%");
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                //render cell content
                string name = EmptyPortNameToUnknown(row["Name"].ToString());
                string status = row["OperationalStatus"].ToString();
                string administrativeStatus = row["AdministrativeStatus"].ToString();
                int PortID = int.Parse(row["PortID"].ToString());
                RenderInterface(writer, name, status,administrativeStatus, PortID);
                writer.WriteBreak();
                addExtraBreak = false;
                //end new cell
                writer.RenderEndTag();

                if (portIndex == totalPorts - 1)
                {
                    //Last port has been rendered
                    //render blank columns
                    for (; columnOffset < portColumns - 1; columnOffset++)
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "PortResourceTableEntry");
                        writer.AddAttribute(HtmlTextWriterAttribute.Width, portColumnRelativeWidth + "%");
                        writer.RenderBeginTag(HtmlTextWriterTag.Td);
                        writer.WriteEncodedText(" ");
                        writer.RenderEndTag();
                    }
                }

                if (columnOffset == (portColumns - 1))
                {
                    //End new row
                    writer.RenderEndTag();
                }
                portIndex = portIndex + 1;
            }

            //end render table
            writer.RenderEndTag();

            if (addExtraBreak)
            {
                writer.WriteBreak();
            }
        }
        writer.RenderEndTag();
    }

    private static string EmptyPortNameToUnknown(string portName)
    {
        if (string.IsNullOrEmpty(portName))
        {
            return UDTWebContent.UDTWEBCODE_TM0_7;
        }  else
        {
            return portName;
        }
    }
    private void RenderInterface(HtmlTextWriter writer,string name, string status,string administrativeStatus, int PortId)
    {
        //add attributes for popups
        string netObjectDetail = string.Format("UP:{0}", PortId);
       //add the link to the Port Details page if they click on the port
        writer.AddAttribute(HtmlTextWriterAttribute.Href, BaseResourceControl.GetViewLink(netObjectDetail) + @"& tooltip=""""");
        
        //the below line should be changed to the manage ports link
        //string temp =string.Format( @"\Orion\UDT\PortPopUp.aspx?NetObject=UP:{0}",PortId);

       // string temp = string.Format(@"\Orion\UDT\Summary.aspx?NetObject=UP:{0} ", PortId);
        //string temp = string.Format(@"\Orion\UDT\PortDetails.aspx?NetObject=UP:{0} ", PortId);
      //  writer.AddAttribute(HtmlTextWriterAttribute.Href,temp.Trim());
        
       //temp = @"tooltip=""processed""";
      // string temp = @"tooltip=""""";
      // writer.AddAttribute(HtmlTextWriterAttribute.Href, temp.Trim());
        
        writer.RenderBeginTag(HtmlTextWriterTag.A);
        RenderPortStatus(writer, status,administrativeStatus);
        writer.WriteEncodedText(" " + name);
        writer.RenderEndTag(); // A
    }
    
    private DataTable getPortsForNode(int nodeID)
    {
        DataTable ports =UDTPortDAL.GetPortsForNode(nodeID, true);
        return ports;
    }
}
