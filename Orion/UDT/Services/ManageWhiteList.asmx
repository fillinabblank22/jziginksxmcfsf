﻿<%@ WebService Language="C#" Class="ManageWhiteList" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;
using System.Web.Caching;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.Services.Protocols;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.UDT.Common;
using SolarWinds.UDT.Common.Models;
using SolarWinds.UDT.Common.Models.Rules;
using SolarWinds.UDT.Web.DAL;
using System.Web.UI;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ManageWhiteList : System.Web.Services.WebService
{

    private static readonly Log _log = new Log();
    private static readonly bool _isDemoServer = SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;

    private const string cacheKey = "WhiteListRuleCache";
    private const int cacheTimeoutSeconds = 60;
    
    [WebMethod(EnableSession = true)]
    public PageableDataTable GetWhiteListRules()
    {
        int pageSize;
        int startRowNumber;
        bool included;
        int groupField;
        int groupValue;
        int ruleType;
        
        string includedParam = this.Context.Request.QueryString["included"];
        string sortColumn = this.Context.Request.QueryString["sort"];
        string sortDirection = this.Context.Request.QueryString["dir"];

        Int32.TryParse(this.Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(this.Context.Request.QueryString["limit"], out pageSize);
        Int32.TryParse(this.Context.Request.QueryString["ruleType"], out ruleType);
        Int32.TryParse(this.Context.Request.QueryString["groupField"], out groupField);
        Int32.TryParse(this.Context.Request.QueryString["groupValue"], out groupValue);

        if (pageSize < 10)
        {
            if (!Int32.TryParse(WebUserSettingsDAL.Get("UDT_ManageWhiteList_PageSize"), out pageSize))
                pageSize = 10;
        }

        //Get from cache, otherwise load from BL
        UDTRuleBase[] rules = HttpContext.Current.Cache[cacheKey] as UDTRuleBase[];
        if (rules==null) rules = GetRules();
        
        // ID, Name, Type, Action, Description, Status, LastEdited
        var dt = new DataTable();
        dt.Columns.Add("ID", typeof(int));
        dt.Columns.Add("Name", typeof (string));
        dt.Columns.Add("Type", typeof (string));
        dt.Columns.Add("Target", typeof (string));
        dt.Columns.Add("Decription", typeof (string));
        dt.Columns.Add("Status", typeof (int));
        dt.Columns.Add("LastEdited", typeof (string));

        DataRow row;
        
        //Load datatable with Rule data (filtered inline)
        foreach (UDTRuleBase rule in rules)
        {
            if (ruleType != (int)rule.RuleType) continue;
            
            UDTRuleValueList deviceList = rule as UDTRuleValueList;
            UDTRuleRangeList rangeList = rule as UDTRuleRangeList;
            UDTRuleIPSubnetList ipSubnetList = rule as UDTRuleIPSubnetList;
            UDTRulePatternList customList = rule as UDTRulePatternList;

            if (deviceList != null)
            {
                if ((groupField == 1) && (groupValue != (int)WhiteListSelectionMethod.Device) && (groupValue != -1)) continue;
                if ((groupField == 2) && (groupValue != (int)deviceList.Target) && (groupValue != -1)) continue;
                if ((groupField == 3) && (groupValue != (deviceList.Enabled?1:0)) && (groupValue != -1)) continue;
                
                row = dt.NewRow();
                row[0] = deviceList.RuleID;
                row[1] = deviceList.Name;
                row[2] = WhiteListSelectionMethod.Device;
                row[3] = deviceList.Target;
                row[4] = deviceList.Description;
                row[5] = deviceList.Enabled;
                row[6] = deviceList.LastUpdate.ToLocalTime().ToString(CultureInfo.InvariantCulture);
                dt.Rows.Add(row);
            }
            
            if ((rangeList != null) && (rule.Target == UDTRuleTarget.IPAddress))
            {
                if ((groupField == 1) && (groupValue != (int)WhiteListSelectionMethod.IPRange) && (groupValue != -1)) continue;
                if ((groupField == 2) && (groupValue != (int)rangeList.Target) && (groupValue != -1)) continue;
                if ((groupField == 3) && (groupValue != (rangeList.Enabled ? 1 : 0)) && (groupValue != -1)) continue;
                
                row = dt.NewRow();
                row[0] = rangeList.RuleID;
                row[1] = rangeList.Name;
                row[2] = WhiteListSelectionMethod.IPRange;
                row[3] = rangeList.Target;
                row[4] = rangeList.Description;
                row[5] = rangeList.Enabled;
                row[6] = rangeList.LastUpdate.ToLocalTime().ToString(CultureInfo.InvariantCulture); 
                dt.Rows.Add(row);
            }

            if ((rangeList != null) && (rule.Target == UDTRuleTarget.MACAddress))
            {
                if ((groupField == 1) && (groupValue != (int)WhiteListSelectionMethod.MacRange) && (groupValue != -1)) continue;
                if ((groupField == 2) && (groupValue != (int)rangeList.Target) && (groupValue != -1)) continue;
                if ((groupField == 3) && (groupValue != (rangeList.Enabled ? 1 : 0)) && (groupValue != -1)) continue;
                
                row = dt.NewRow();
                row[0] = rangeList.RuleID;
                row[1] = rangeList.Name;
                row[2] = WhiteListSelectionMethod.MacRange;
                row[3] = rangeList.Target;
                row[4] = rangeList.Description;
                row[5] = rangeList.Enabled;
                row[6] = rangeList.LastUpdate.ToLocalTime().ToString(CultureInfo.InvariantCulture);
                dt.Rows.Add(row);
            }

            if (ipSubnetList != null)
            {
                if ((groupField == 1) && (groupValue != (int)WhiteListSelectionMethod.Subnet) && (groupValue != -1)) continue;
                if ((groupField == 2) && (groupValue != (int)ipSubnetList.Target) && (groupValue != -1)) continue;
                if ((groupField == 3) && (groupValue != (ipSubnetList.Enabled ? 1 : 0)) && (groupValue != -1)) continue;
                
                row = dt.NewRow();
                row[0] = ipSubnetList.RuleID;
                row[1] = ipSubnetList.Name;
                row[2] = WhiteListSelectionMethod.Subnet;
                row[3] = ipSubnetList.Target;
                row[4] = ipSubnetList.Description;
                row[5] = ipSubnetList.Enabled;
                row[6] = ipSubnetList.LastUpdate.ToLocalTime().ToString(CultureInfo.InvariantCulture);
                dt.Rows.Add(row);
            }

            if (customList != null)
            {
                if ((groupField == 1) && (groupValue != (int)WhiteListSelectionMethod.Custom) && (groupValue != -1)) continue;
                if ((groupField == 2) && (groupValue != (int)customList.Target) && (groupValue != -1)) continue;
                if ((groupField == 3) && (groupValue != (customList.Enabled ? 1 : 0)) && (groupValue != -1)) continue;
                
                row = dt.NewRow();
                row[0] = customList.RuleID;
                row[1] = customList.Name;
                row[2] = WhiteListSelectionMethod.Custom;
                row[3] = customList.Target;
                row[4] = customList.Description;
                row[5] = customList.Enabled;
                row[6] = customList.LastUpdate.ToLocalTime().ToString(CultureInfo.InvariantCulture);
                dt.Rows.Add(row);
            }
        }

        //sort dt
        DataView dv = new DataView(dt);
        dv.Sort = (sortColumn + " " + sortDirection);
        DataTable sorted = dv.ToTable();
        DataTable sortedPage = sorted.Copy();
        
        //page dt
        if (sorted.Rows.Count > 0)
        {
            // do the paging
            sortedPage = sorted.AsEnumerable().Skip(startRowNumber).Take(pageSize).CopyToDataTable();
        }

        PageableDataTable gridData = new PageableDataTable(sortedPage, sorted.Rows.Count);

        return gridData;
    }

    
    private UDTRuleBase[] GetRules()
    {
        UDTRuleBase[] rules;
        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                rules = bl.RulesGetAll();
            }

        }
        catch (Exception ex)
        {
            _log.Error("Unable to fetch Whitelist rules.", ex);
            throw new ApplicationException("Unable to fetch Whitelist rules. \n" + ex.Message, ex);
        }
        
        return rules;
    }
    
    [WebMethod(EnableSession = true)]
    public bool DeleteRules(int[] ruleIds)
    {
        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                bl.RulesDelete(ruleIds);
            }
            //clear cache, force reload next time
            HttpContext.Current.Cache.Remove(cacheKey);
            return true;
        }
        catch (Exception ex)
        {
            _log.Error("Unable to delete Whitelist rules.", ex);
            return false;
        }
    }

    [WebMethod(EnableSession = true)]
    public bool EnableRules(int[] ruleIds)
    {
        var rules = new List<UDTRuleBase>();
        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                foreach (int ruleId in ruleIds)
                {
                    var rule = bl.RulesGetRule(ruleId);
                    rule.Enabled = true;
                    rules.Add(rule);
                }
                
                bl.RulesSet(rules.ToArray());
            }
            //clear cache, force reload next time
            HttpContext.Current.Cache.Remove(cacheKey);
            return true;
        }
        catch (Exception ex)
        {
            _log.Error("Unable to enable Whitelist rules.", ex);
            return false;
        }
    }

    [WebMethod(EnableSession = true)]
    public bool DisableRules(int[] ruleIds)
    {
        var rules = new List<UDTRuleBase>();
        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                foreach (int ruleId in ruleIds)
                {
                    var rule = bl.RulesGetRule(ruleId);
                    rule.Enabled = false;
                    rules.Add(rule);
                }

                bl.RulesSet(rules.ToArray());
            }
            //clear cache, force reload next time
            HttpContext.Current.Cache.Remove(cacheKey);
            return true;
        }
        catch (Exception ex)
        {
            _log.Error("Unable to disable Whitelist rules.", ex);
            return false;
        }
    }

    [WebMethod(EnableSession = true)]
    public Pair[] TestRules(string testText, int ruleType)
    {
        var ret = new List<Pair>();
        try
        {
            UDTRuleBase[] matchedRules;
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                matchedRules = bl.RulesGetRulesForEndpoint(testText, (UDTRuleType)ruleType);
            }
            ret.AddRange(matchedRules.Select(rule => new Pair {First = rule.RuleID, Second = rule.Name}));
            IPAddress ip;
            if (IPAddress.TryParse(testText, out ip))
            {
                ret.Add(new Pair
                            {
                                First = "IP",
                                Second = ((ip.AddressFamily == AddressFamily.InterNetwork) || (ip.AddressFamily == AddressFamily.InterNetworkV6)) ? "Valid" : "InValid"
                            });
            }
            else
            {
                ret.Add(new Pair { First = "IP", Second = "InValid" });
            }
            return ret.ToArray();
        }
        catch (Exception ex)
        {
            _log.Error("Unable to test Whitelist rules.", ex);
            return null;
        }
    }
    
    [WebMethod(EnableSession = true)]
    public Triplet[] GetGroupValues(string groupBy, int ruleType)
    {
                
        //Get from BL, and load into cache
        UDTRuleBase[] rules = GetRules();
        var cacheTimeout = TimeSpan.FromSeconds(cacheTimeoutSeconds);
        HttpContext.Current.Cache.Remove(cacheKey);
        HttpContext.Current.Cache.Insert(cacheKey, rules, null, DateTime.UtcNow + cacheTimeout, Cache.NoSlidingExpiration);
        
        if (String.IsNullOrEmpty(groupBy)) return null;
        var ret = new List<Triplet>();

        int macTargetCount = 0; // rules.Count(r => ((r.Target == UDTRuleTarget.MACAddress) && ((int)r.RuleType == ruleType)));
        int ipTargetCount = 0; // rules.Count(r => ((r.Target == UDTRuleTarget.IPAddress) && ((int)r.RuleType == ruleType)));
        int dnsTargetCount = 0; // rules.Count(r => ((r.Target == UDTRuleTarget.DNSName) && ((int)r.RuleType == ruleType)));

        int enabledCount = 0; // rules.Count(r => ((r.Enabled) && ((int)r.RuleType == ruleType)));
        int disabledCount = 0; // rules.Count(r => ((!r.Enabled) && ((int)r.RuleType == ruleType)));

        int deviceCount = 0;
        int ipRangeCount = 0;
        int macRangeCount = 0;
        int subnetCount = 0;
        int customCount = 0;
        
        foreach (var rule in rules)
        {
            var deviceList = rule as UDTRuleValueList;
            var rangeList = rule as UDTRuleRangeList;
            var ipSubnetList = rule as UDTRuleIPSubnetList;
            var customList = rule as UDTRulePatternList;

            if ((deviceList != null) && ((int)rule.RuleType == ruleType)) deviceCount++;
            if ((rangeList != null) && (rule.Target == UDTRuleTarget.IPAddress) && ((int)rule.RuleType == ruleType)) ipRangeCount++;
            if ((rangeList != null) && (rule.Target == UDTRuleTarget.MACAddress) && ((int)rule.RuleType == ruleType)) macRangeCount++;
            if ((ipSubnetList != null) && ((int)rule.RuleType == ruleType)) subnetCount++;
            if ((customList != null) && ((int)rule.RuleType == ruleType)) customCount++;

            if ((rule.Target == UDTRuleTarget.MACAddress) && ((int)rule.RuleType == ruleType)) macTargetCount++;
            if ((rule.Target == UDTRuleTarget.IPAddress) && ((int)rule.RuleType == ruleType)) ipTargetCount++;
            if ((rule.Target == UDTRuleTarget.DNSName) && ((int)rule.RuleType == ruleType)) dnsTargetCount++;
            
            if ((rule.Enabled) && ((int)rule.RuleType == ruleType)) enabledCount++;
            if ((!rule.Enabled) && ((int)rule.RuleType == ruleType)) disabledCount++;
           
        }
        
        switch (groupBy)
        {
            case "1": //Type
                
                ret.Add(new Triplet(-1, Resources.UDTWebContent.UDTWEBDATA_MK1_11, deviceCount + ipRangeCount + macRangeCount + subnetCount + customCount ));
                ret.Add(new Triplet((int)WhiteListSelectionMethod.Device, Resources.UDTWebContent.UDTWEBDATA_MK1_12, deviceCount));
                ret.Add(new Triplet((int)WhiteListSelectionMethod.IPRange, Resources.UDTWebContent.UDTWEBDATA_MK1_13, ipRangeCount));
                ret.Add(new Triplet((int)WhiteListSelectionMethod.MacRange, Resources.UDTWebContent.UDTWEBDATA_MK1_14, macRangeCount));
                ret.Add(new Triplet((int)WhiteListSelectionMethod.Subnet, Resources.UDTWebContent.UDTWEBDATA_MK1_15, subnetCount));
                ret.Add(new Triplet((int)WhiteListSelectionMethod.Custom, Resources.UDTWebContent.UDTWEBDATA_MK1_16, customCount));
                break;
                
            case "2":  //Target
                ret.Add(new Triplet(-1, Resources.UDTWebContent.UDTWEBDATA_MK1_17, macTargetCount + ipTargetCount + dnsTargetCount));
                ret.Add(new Triplet((int)UDTRuleTarget.MACAddress, Resources.UDTWebContent.UDTWEBDATA_MK1_18, macTargetCount));
                ret.Add(new Triplet((int)UDTRuleTarget.IPAddress, Resources.UDTWebContent.UDTWEBDATA_MK1_19, ipTargetCount));
                ret.Add(new Triplet((int)UDTRuleTarget.DNSName, Resources.UDTWebContent.UDTWEBDATA_MK1_20, dnsTargetCount));
                break;
                
            case "3": //Status
                ret.Add(new Triplet(-1, Resources.UDTWebContent.UDTWEBDATA_MK1_21, enabledCount + disabledCount));
                ret.Add(new Triplet(1, Resources.UDTWebContent.UDTWEBDATA_MK1_22, enabledCount));
                ret.Add(new Triplet(0, Resources.UDTWebContent.UDTWEBDATA_MK1_23, disabledCount));
                break;
                
        }

        return ret.ToArray();
    }
    
}