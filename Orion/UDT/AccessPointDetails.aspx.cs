using System;
using System.Data;
using System.Web;
using SolarWinds.UDT.Web.Views;
using SolarWinds.UDT.Web.DAL;

public partial class Orion_UDT_AccessPointDetails : UdtViewPage
{
    private static SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();
    
    public override string ViewType
    {
        get
        {
            return "UDT AccessPointDetails";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        pageTitleBar.PageTitle = getTitle();
        pageTitleBar.ViewID = this.ViewInfo.ViewID;
        //todo add help link
        pageTitleBar.HelpFragment = "OrionUDTPHAccessPointDetailsSummary";
    }

    protected override void OnInit(EventArgs e)
    {
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    
    public int AccessPointID
    {
        get
        {
            int apId;

            try
            {
                string tmpPort = HttpUtility.UrlDecode(Request.QueryString["NetObject"]);
                string[] netObjectParts = tmpPort.Split(':');
                apId = int.Parse(netObjectParts[1]);
            }
            catch (Exception)
            {
                apId = 0;
            }
            return apId;
        }
    }
    
    
    protected string getTitle()
    {
        string viewTitle = this.ViewInfo.ViewTitle;

        try
        {
            //string tmpAccessPoint = HttpUtility.UrlDecode(Request.QueryString["Id"]);
            //int apId = 0;
            //Int32.TryParse(HttpUtility.UrlDecode(Request.QueryString["Id"]), out apId);

            if (AccessPointID > 0)
            {

                try
                {
                    viewTitle += UDTWirelessDAL.GetAccessPointCaption(AccessPointID);
                    
                }
                catch (Exception ex)
                {
                    _log.ErrorFormat(String.Format("Error occurred retrieving access point caption for UDT AccessPointDetails view.  Error:{0}", ex.Message));
                }
            }
        }
        catch (Exception ex)
        {
            _log.ErrorFormat(String.Format("Error occurred loading title for UDT AccessPointDetails view.  Error:{0}", ex.Message));
        }

        return viewTitle;
    }
}
