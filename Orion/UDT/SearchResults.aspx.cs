﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.UDT.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common;
using SolarWinds.UDT.Web.DAL;


public partial class Orion_UDT_SearchResults : System.Web.UI.Page
{

    internal bool _isOrionDemoMode = false;

    protected void Page_Load(object sender, EventArgs e)
    {

        //Get Feature Codes from feature manager
        bool historyEnabled = false;
        var featureManager = new FeatureManager();
        List<string> enabledFeatures = new List<string>(featureManager.EnabledFeatures);
        if (enabledFeatures.Contains(SolarWinds.UDT.Common.WellKnownFeatures.UDT.HistoricalData))
        {
            historyEnabled = true;
        }

     

        //debugging searchcriteria cache
        //if (HttpContext.Current.Session["UDTSearchCriteriaCache"] != null)
        //{
        //    Dictionary<UDTSearchDAL.SearchCriteria, int> blah =
        //        (Dictionary<UDTSearchDAL.SearchCriteria, int>)HttpContext.Current.Session["UDTSearchCriteriaCache"];

        //    if (blah.Count > 0)
        //    {
        //        foreach (var itm in blah)
        //        {
        //            Response.Write(string.Format("Key: {0},{1},{2} Value: {3}<br>", itm.Key.A.ToString(),
        //                                         itm.Key.B.ToString(), itm.Key.C.ToString(), itm.Value));
        //        }
        //    }
        //}


        // Clear the UDT_SearchCache session object whenever the page reloads
        HttpContext.Current.Session["UDTSearchCriteriaCache"] = new Dictionary<UDTSearchDAL.SearchCriteria, int>();

        string searchString = Request.QueryString["q"] ?? String.Empty;
        string searchColumns = Request.QueryString["cols"] ?? String.Empty;

        //string searchColumns = SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("UDT_SearchBox_CheckedItems");

        pageTitleBar.PageTitle = Resources.UDTWebContent.UDTWEBDATA_VB1_19;

        //use to old search control
        pageTitleBar.UseV1SearchControl = true;

        //hide the customize page link for this page
        pageTitleBar.HideCustomizePageLink = true;
        //todo add help link
        pageTitleBar.HelpFragment = "OrionUDTPHDeviceSummaryUDTSearchResults";
        linkLearnMore.LearnMoreUrlFragment = "OrionUDTPHDeviceSummaryUDTSearchResultsLearnMore";

        if (searchColumns == "" && searchString == "")
        {
            litPageSubTitle.Text = String.Format(Resources.UDTWebContent.UDTWCODE_VB1_5,Resources.UDTWebContent.UDTWEBDATA_MR1_2);
        }
        else if (searchString == "")
        {
            litPageSubTitle.Text = String.Format(Resources.UDTWebContent.UDTWCODE_VB1_5, searchColumns);
        }
        else
        {
            litPageSubTitle.Text = String.Format(Resources.UDTWebContent.UDTWCODE_VB1_6, searchString, searchColumns);
        }

    }

    protected override void OnInit(EventArgs e)
    {
        //this.resContainer.DataSource = this.ViewInfo;
        //this.resContainer.DataBind();
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            _isOrionDemoMode = true;
        }

        base.OnInit(e);
    }
}
