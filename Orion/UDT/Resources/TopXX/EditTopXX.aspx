<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true" CodeFile="EditTopXX.aspx.cs" Inherits="Orion_UDT_Resources_TopXX_Edit" %>
<%@ Import Namespace="SolarWinds.Orion.Web" %>
<%@ Import Namespace="SolarWinds.UDT.Web.Helpers" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" Assembly="OrionWeb" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= String.Format(Resources.UDTWebContent.UDTWEBDATA_AK1_206, CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Resource.Title, Request["NetObject"].UrlEncode()))) %></h1>
    <div>
        <orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="true" ShowSubTitleHintMessage="false" />
        <%= Resources.UDTWebContent.UDTWEBDATA_AK1_210 %>
        <br /><br />
        <b><%= String.Format(Resources.UDTWebContent.UDTWEBDATA_AK1_211, this.NetObjectType)%></b><br />
        <asp:TextBox runat="server" ID="maxCount" Columns="5"/>
        <br /><br />
        <b><%= String.Format(Resources.UDTWebContent.UDTWEBDATA_AK1_212, this.NetObjectType) %></b><br />
        <asp:TextBox runat="server" ID="filter" Columns="60"/>
        <br /><br />
        <%= String.Format(Resources.UDTWebContent.UDTWEBDATA_AK1_213, this.NetObjectType) %>
        <br /><br />
        <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" DisplayType="Primary" OnClick="SubmitClick" />
        <br /><br />

        <script type="text/javascript">
            //<![CDATA[
            $(function() {
                $(".ExpanderLink + div").hide();
                $(".ExpanderLink").toggle(
                    function() {
                        $("img:first", this).attr("src", "/NetPerfMon/images/Icon_Minus.gif");
                        $(this).next().slideDown("fast");
                    },
                    function() {
                        $("img:first", this).attr("src", "/NetPerfMon/images/Icon_Plus.gif");
                        $(this).next().slideUp("fast");
                    }
                )
            });
        //]]>
        </script>
        
		<a href="#" class="ExpanderLink"><img src="/NetPerfMon/images/Icon_Plus.gif" alt="<%= Resources.UDTWebContent.UDTWEBDATA_AK1_222 %>" style="padding-right: 5px;"/><b><%= Resources.UDTWebContent.UDTWEBDATA_AK1_214 %></b></a>
		<div class="text" style="padding-left: 10px;">
		    <%= Resources.UDTWebContent.UDTWEBDATA_AK1_215 %>

            <asp:Panel ID="NodesColumns" runat="server">
            <a href="#" class="ExpanderLink"><img src="/NetPerfMon/images/Icon_Plus.gif" alt="<%= Resources.UDTWebContent.UDTWEBDATA_AK1_222 %>" style="padding-right: 5px;" /><b><%= Resources.UDTWebContent.UDTWEBDATA_AK1_216 %></b></a>
	        <div class="text" style="padding-left: 10px;">
				<%= Resources.UDTWebContent.UDTWEBDATA_AK1_217 %><br />
    	        <ul style="list-style-type:none;"> 
				    <asp:Repeater runat="server" ID="nodeColumns">
				        <ItemTemplate>
				            <li><%# Container.DataItem %></li>
				        </ItemTemplate>
				    </asp:Repeater>
    			</ul>
			</div>
			<br /><br />
			</asp:Panel>
	        
		</div>
    </div>
</asp:Content>
