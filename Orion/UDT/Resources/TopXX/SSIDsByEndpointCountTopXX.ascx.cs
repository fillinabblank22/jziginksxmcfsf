using System;
using System.Data;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.Orion.Core.Web;


public partial class Orion_UDT_Resources_TopXX_SSIDsByEndpointCount : TopXXResourceControl
{
    protected override string TitleTemplate
    {
        get { return Resources.UDTWebContent.UDTWEBCODE_AK1_45; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Wrapper.ShowEditButton = this.EnableEdit;
        DataTable table;

        string sortDirection = "DESC";

        try
        {
            table = UDTWirelessDAL.GetTopXXSSIDsByEndpointCount(this.MaxRecords, this.Resource.Properties["Filter"], sortDirection);
        }

        catch (System.Data.SqlClient.SqlException)
        {
            this.SQLErrorPanel.Visible = true;
            return;
        }
        this.resourceTable.DataSource = table;
        this.resourceTable.DataBind();
    }

    public override string EditURL
    {
        get { return string.Format("/Orion/UDT/Resources/TopXX/EditTopXX.aspx?Type=Nodes&ResourceID={0}&ViewID={1}", this.Resource.ID, this.Resource.View.ViewID); }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionUDTPHTop10SSIDS"; }
    }
    
    public string GetEndpointCount(int count)
    {
        return String.Format(Resources.UDTWebContent.UDTWEBCODE_AK1_44, count);
    }
 }
