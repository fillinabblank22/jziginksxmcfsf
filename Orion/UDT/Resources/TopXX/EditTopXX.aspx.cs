using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.UDT.Web.Helpers;

public partial class Orion_UDT_Resources_TopXX_Edit : System.Web.UI.Page
{
	#region Resource property
	private ResourceInfo resource;

	protected ResourceInfo Resource
	{
		get { return this.resource; }
		set { this.resource = value; }
	} 
	#endregion

	#region NetObjectType property
	private string netObjectType;

	protected string NetObjectType
	{
		get { return this.netObjectType; }
		set { this.netObjectType = value; }
	} 
	#endregion
	
	protected void Page_Load(object sender, EventArgs e)
	{
        if (!String.IsNullOrEmpty(this.Request.QueryString["Type"]) && HtmlHelper.FindBadCharacters(this.Request.QueryString["Type"]))
        {
            Server.Transfer(String.Format("~/Orion/Error.aspx?Message={0}",
                                          this.Request.QueryString["Type"]));
            return;
        }

	    this.NodesColumns.Visible = !((this.Request.QueryString["HideNodesFilter"] ?? string.Empty).ToUpperInvariant() == "TRUE");

	    this.NetObjectType = WebSecurityHelper.SanitizeHtml(this.Request.QueryString["Type"]);

		if (!String.IsNullOrEmpty(this.Request.QueryString["ResourceID"]))
		{
			int resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
			this.Resource = ResourceManager.GetResourceByID(resourceID);
			this.Title = String.Format(Resources.UDTWebContent.UDTWEBDATA_AK1_206, Resource.Title);
		}

		if (!this.IsPostBack)
		{
			int max;
			if (!Int32.TryParse(this.Resource.Properties["MaxRecords"], out max))
			{
				this.Resource.Properties["MaxRecords"] = "10";
			}
			this.maxCount.Text = this.Resource.Properties["MaxRecords"];
			this.filter.Text = this.Resource.Properties["Filter"];

			if (!string.IsNullOrEmpty(Resource.Properties["Title"]))
			{
				this.resourceTitleEditor.ResourceTitle = Resource.Properties["Title"];
			}
			else
			{
				this.resourceTitleEditor.ResourceTitle = string.Empty;
			}

			if (!string.IsNullOrEmpty(Resource.Properties["SubTitle"]))
			{
				this.resourceTitleEditor.ResourceSubTitle = Resource.Properties["SubTitle"];
			}
			else
			{
				this.resourceTitleEditor.ResourceSubTitle = string.Empty;
			}
		}

		this.nodeColumns.DataSource = TopXXDAL.GetNodeColumnNames();
	    this.nodeColumns.DataBind();
	}

	protected void SubmitClick(object sender, EventArgs e)
	{
		this.Resource.Properties["Title"] = this.resourceTitleEditor.ResourceTitle;
		this.Resource.Properties["SubTitle"] = this.resourceTitleEditor.ResourceSubTitle;
        
        try
        {
            // check Max number of nodes to display field value 
            int max = int.Parse(this.maxCount.Text);
            this.Resource.Properties["MaxRecords"] = max < 0 ? "10" : this.maxCount.Text;
        }
        catch
        {
	        this.Resource.Properties["MaxRecords"] = "10";
		}
        
        this.Resource.Properties["Filter"] = this.filter.Text;

		Response.Redirect(String.Format("/Orion/View.aspx?ViewID={0}", this.Resource.View.ViewID));
	}
}
