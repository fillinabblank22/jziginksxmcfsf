<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PercentPortUsedTopXX.ascx.cs" Inherits="Orion_UDT_Resources_TopXX_PercentPortUsed" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px" class="NeedsZebraStripes">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= Resources.UDTWebContent.UDTWEBDATA_AK1_207 %>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="resourceTable">
            <HeaderTemplate>
    <table border="0" cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes">
        <tr>
            <td class="ReportHeader" colspan="1">&nbsp;</td>
            <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_218 %></td>
            <td class="ReportHeader" colspan="2"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_219 %></td>
            <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_220 %></td>
            <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_221 %></td>
        </tr>
            </HeaderTemplate>
            <ItemTemplate>
	    <tr>
	        
        
   	        <%--<td class="Property"><a <%# this.FormatParamString(Container.DataItem, this.Profile.AllowNodeManagement) %> href="/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:<%# Eval("NodeID")%>"><%# Eval("Caption") %></a>&nbsp;</td>--%>

            <td class="Property" valign="middle" width="20"><img src="<%# SolarWinds.Orion.Web.UI.StatusIcons.NodeIconFactory.SmallIconURL( Eval("NodeID"), Eval("Status") ) %>" alt="<%= Resources.UDTWebContent.UDTWEBDATA_VB1_118 %>" style="vertical-align:bottom;" /></td>
            <td class="Property" ><a href="/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:<%# Eval("NodeID")%>"><%# Eval("Caption") %></a>&nbsp;</td>
            <td class="Property" width="160"><%# this.FormatPortPercentUtilizedBar(Eval("PortPercentUsed"), 155)%></td>
            <td class="Property" width="70"><%# this.FormatPortPercentUtilized(Eval("PortPercentUsed"))%></td>
            <td class="Property" width="85">&nbsp;<%# Eval("PortsAvailable")%></td>
            <td class="Property" width="80">&nbsp;<%# Eval("ActivePortCount")%></td>
        
        </tr>
            </ItemTemplate>
            <FooterTemplate>
    </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>