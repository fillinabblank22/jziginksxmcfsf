using System;
using System.Data;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.Orion.Core.Web;


public partial class Orion_UDT_Resources_TopXX_PercentPortUsed : TopXXResourceControl
{
    protected override string TitleTemplate
    {
        get { return Resources.UDTWebContent.UDTWEBCODE_AK1_46; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Wrapper.ShowEditButton = this.EnableEdit;
		DataTable table;

		try
		{
            table = UDTResourceDAL.GetPortPercentUtilization(this.MaxRecords, this.Resource.Properties["Filter"]);
		}
		catch (System.Data.SqlClient.SqlException)
		{
			this.SQLErrorPanel.Visible = true;
			return;
		}
        this.resourceTable.DataSource = table;
        this.resourceTable.DataBind();
    }

    public override string EditURL
    {
		get { return string.Format("/Orion/UDT/Resources/TopXX/EditTopXX.aspx?Type=Nodes&ResourceID={0}&ViewID={1}", this.Resource.ID, this.Resource.View.ViewID); }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionUDTPHDeviceSummaryTopTenNodesByPercentPortsUsed"; }
    }
   
    #region Thresholds
    private static OrionSetting PortPercentUtilizedError { get { return SettingsDAL.GetSetting("UDT-PortPercentUtilization-Error"); } }
    private static OrionSetting PortPercentUtilizedWarning { get { return SettingsDAL.GetSetting("UDT-PortPercentUtilization-Warning"); } }
    #endregion

    public string FormatPortPercentUtilized(object value)
    {
        try
        {
            return FormatHelper.GetPercentFormat(Convert.ToDouble(value), PortPercentUtilizedError.SettingValue, PortPercentUtilizedWarning.SettingValue);
        }
        catch
        {
            return String.Empty;
        }
    }

    public string FormatPortPercentUtilizedBar(object value, int width)
    {
        try
        {
            return FormatHelper.GetPercentBarFormat(Convert.ToDouble(value), PortPercentUtilizedError.SettingValue, PortPercentUtilizedWarning.SettingValue, width, false, true);
        }
        catch
        {
            return String.Empty;
        }
    }
 }
