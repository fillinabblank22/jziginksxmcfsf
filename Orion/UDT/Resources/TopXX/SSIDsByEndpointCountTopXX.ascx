<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SSIDsByEndpointCountTopXX.ascx.cs" Inherits="Orion_UDT_Resources_TopXX_SSIDsByEndpointCount" %>
<%@ Import Namespace="SolarWinds.UDT.Web.Helpers" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= Resources.UDTWebContent.UDTWEBDATA_AK1_207 %>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="resourceTable">
            <HeaderTemplate>
                <table border="0" cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes">
                    <tr>
                        <td class="ReportHeader" colspan="1">&nbsp;</td>
                        <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_124 %></td>
                        <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_126 %></td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
	                <tr>
                        <td width="4%"><%# IconHelper.GetSSIDIcon(DALHelper.GetValueOrDefault(Eval("SSID"), Resources.UDTWebContent.UDTWEBCODE_AK1_21))%></td>
                        <td class="Property" width="48%">&nbsp;<%# HtmlHelper.CreateUdtViewHref("ssid", Eval("SSID").ToString(), Eval("SSID").ToString())%></td>
                        <td class="Property" width="48%">&nbsp;<%# GetEndpointCount(DALHelper.GetValueOrDefault(Eval("TotalClients"),0))%></td>
                    </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>