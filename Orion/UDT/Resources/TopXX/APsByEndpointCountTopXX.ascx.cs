using System;
using System.Data;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.Orion.Core.Web;
using System.Collections.Generic;
using SolarWinds.UDT.Web.Helpers;

public partial class Orion_UDT_Resources_TopXX_APsByEndpointCount : TopXXResourceControl
{
   
    protected override string TitleTemplate
    {
        get { return Resources.UDTWebContent.UDTWEBCODE_AK1_43; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Wrapper.ShowEditButton = this.EnableEdit;
        DataTable table;

        string sortDirection = "DESC";

        try
        {
            table = UDTWirelessDAL.GetTopXXAPsByEndpointCount(this.MaxRecords, this.Resource.Properties["Filter"], sortDirection);
        }
        catch (System.Data.SqlClient.SqlException)
        {
            this.SQLErrorPanel.Visible = true;
            return;
        }
        this.resourceTable.DataSource = table;
        this.resourceTable.DataBind();
    }

    public override string EditURL
    {
        get { return string.Format("/Orion/UDT/Resources/TopXX/EditTopXX.aspx?Type=Nodes&ResourceID={0}&ViewID={1}", this.Resource.ID, this.Resource.View.ViewID); }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionUDTPHTop10AccessPoints"; }
    }

    public string GetEndpointCount(int count)
    {
        return String.Format(Resources.UDTWebContent.UDTWEBCODE_AK1_44, count);
    }
 }
