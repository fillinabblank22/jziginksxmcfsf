﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using Microsoft.Practices.ObjectBuilder2;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Web.NetObjects;
using SolarWinds.UDT.Web.Providers;

public partial class Orion_UDT_Resources_PortCharts_PortsUsedOverTime : GraphResource 
{
    private string netObjectID;
    private string editURL = string.Empty;
    
    protected override String DefaultTitle
    {
        get { return Resources.UDTWebContent.UDTWEBCODE_AK1_28; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionUDTPHDeviceSummaryTotalPortsUsedOverTime";
        }
    }

    protected string NetObjectID
    {
        get { return this.netObjectID; }
        set { this.netObjectID = value; }
    }


    //public override IEnumerable<Type> RequiredInterfaces
    //{
    //    get { return new Type[] { typeof(INodeProvider)}; }
    //}




    protected void Page_Init(object sender, EventArgs e)
    {
        //if the graph is on the Node Details page then we should be able to get
        //the nodes netobject details
        NetObjectID = Request.QueryString["NetObject"];
        if (string.IsNullOrEmpty(NetObjectID))
        {
            Resource.SubTitle = string.Empty;
           
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
       // string switchnetobject = GetInterfaceInstance<IUDTSwitchProvider>().UDTSwitchNetObject.NetObjectID;
       // CreateChart(null, GetInterfaceInstance<IUDTSwitchProvider>().UDTSwitchNetObject.NetObjectID, "UDTPortsUsedOverTime", chartPlaceHolder);
       
        if (string.IsNullOrEmpty(NetObjectID))
        {
            editURL = string.Format("/Orion/UDT/Resources/PortCharts/EditPortsByGroup.aspx?ResourceID={0}&ViewID={1}&NetObjectID={2}", this.Resource.ID, this.Resource.View.ViewID, HttpUtility.UrlDecode(Request.QueryString["NetObject"]));
            var grpNames = UDTChartsDAL.GetGroups();
            var groups = new System.Text.StringBuilder();
            grpNames.ForEach(grp =>
            {
                if (Resource.Properties.ContainsKey(grp.Key) && Convert.ToBoolean(Resource.Properties[grp.Key]))
                {
                    groups.Append( grp.Value + ",");
                }
            });
            Dictionary<string, string> filter = new Dictionary<string, string> { { "GroupFilter", groups.ToString().Trim(',') } };
            CreateChart(null, "", "UDTPortsUsedOverTime",filter, chartPlaceHolder);
            Wrapper.SetUpDrDownMenu("UDTPortsUsedOverTime", Resource, string.Empty, filter);  
           
        }
        else
        {
            editURL = base.EditURL;
            //Verifying whether the node is UDT or Non-UDT.
            //If non-UDT, then Need not to create Ports Used Chart.
            string[] NetObjectParts = NetObjectID.Split(':');

            if (NetObjectParts.Length == 2)
            {
                if (NetObjectParts[0].ToLower() == "n")
                {
                    int TotalPorts = UDTChartsDAL.GetPortCountForSwitch(NetObjectParts[1]);
                    if (TotalPorts == 0)
                    {
                        //if we returned no ports then the node is not a UDT node so hide the resource
                        Wrapper.Visible = false;
                        return;
                    }
                }
            }


            CreateChart(null, NetObjectID, "UDTPortsUsedOverTime", chartPlaceHolder);
            Wrapper.SetUpDrDownMenu("UDTPortsUsedOverTime", Resource, string.Empty, null);
        }




         
    }





    public override string EditURL
    {
        get
        {
            return editURL; 
        }
    }
}


