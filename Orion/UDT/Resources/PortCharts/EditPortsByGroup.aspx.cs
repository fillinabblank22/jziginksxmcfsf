﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using Microsoft.Practices.ObjectBuilder2;
using SolarWinds.Orion.Web;
using SolarWinds.UDT.Web.DAL;
using System.Collections.Generic;
using System.Web;

public partial class Orion_UDT_Resources_PortCharts_EditPortsByGroup : System.Web.UI.Page
{

    protected ResourceInfo Resource
    {
        get; set; 
    }

    protected string GroupListTitle { get; set; }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(this.Request.QueryString["ResourceID"]))
        {
            this.Resource = ResourceManager.GetResourceByID(Convert.ToInt32(Request.QueryString["ResourceID"]));
            this.Title = String.Format(Resources.UDTWebContent.UDTWEBDATA_AK1_206,
                                       CommonWebHelper.EncodeHTMLTags(
                                           Macros.ParseMacros(
                                               (!string.IsNullOrEmpty(Resource.Properties["Title"]))
                                                   ? Resource.Properties["Title"]
                                                   : this.Resource.Title, Request["netobject"])));
        }
        if (!IsPostBack)
        {
            resourceTitleEditor.ResourceTitle = !string.IsNullOrEmpty(Resource.Properties["Title"]) ? Resource.Properties["Title"] : string.Empty;
            resourceTitleEditor.ResourceSubTitle = !string.IsNullOrEmpty(Resource.Properties["SubTitle"]) ? Resource.Properties["SubTitle"] : string.Empty;
            var grpNames = UDTChartsDAL.GetGroups();
            if (grpNames.Count == 0)
            {
                this.GroupListTitle = Resources.UDTWebContent.UDTWEBCODE_AK1_29;
                return;
            }
            this.GroupListTitle = Resources.UDTWebContent.UDTWEBCODE_AK1_30;
           
            var groups = new List<object>();
            grpNames.ForEach(grp=>
                                 {
                                     if (Resource.Properties.ContainsKey(grp.Key) )
                                     {
                                        groups.Add(
                                             new
                                                 {
                                                     Name = grp.Key,
                                                     ShowInPortChart = Convert.ToBoolean(Resource.Properties[grp.Key])
                                                 });
                                     }
                                     else
                                     {
                                         groups.Add(
                                            new
                                            {
                                                Name = grp.Key,
                                                ShowInPortChart = false
                                            });
                                     }
                                 });

            rptrContainer.DataSource = groups;

             rptrContainer.DataBind();
        }

    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        this.Resource.Properties["Title"] = this.resourceTitleEditor.ResourceTitle;
        this.Resource.Properties["SubTitle"] = this.resourceTitleEditor.ResourceSubTitle;
       var modifiedGroups = (from RepeaterItem item in rptrContainer.Items let c = item.FindControl("grpCheckBox") as CheckBox let h = item.FindControl("hdnPreviousState") as HiddenField where c.Checked != Convert.ToBoolean(h.Value) select c).ToDictionary(c => c.Text, c => c.Checked);
            Resource.Properties.Remove("GroupCount");
            foreach (var modifiedGroup in modifiedGroups)
            {
                if (!Resource.Properties.ContainsKey(modifiedGroup.Key))
                {
                    Resource.Properties.Add(modifiedGroup.Key, modifiedGroup.Value.ToString());
                    continue;
                }

                Resource.Properties[modifiedGroup.Key] = modifiedGroup.Value.ToString();
            }
        //UDTChartsDAL.SaveGroupSetting(modifiedGroups,HttpContext.Current.Profile.UserName);
        string strRedirectUrl = String.Format("/Orion/View.aspx?ResourceID={0}&NetObject={1}&ViewID={2}", Request.QueryString["ResourceID"], Request.QueryString["NetObjectID"], Resource.View.ViewID);
        Response.Redirect(strRedirectUrl);
    }
}
