﻿using System;
using System.Collections.Generic;
using System.Web;
using Microsoft.Practices.ObjectBuilder2;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.UDT.Web.Charting;
using System.Data;
using SolarWinds.UDT.Web.DAL;

public partial class Orion_UDT_Resources_PortCharts_PortsCurrentlyInUse : SolarWinds.Orion.Web.UI.BaseResourceControl
{
    private readonly string defaultTitle = Resources.UDTWebContent.UDTWEBCODE_AK1_26;
    private static OrionSetting PortPercentUtilizedError { get { return SettingsDAL.GetSetting("UDT-PortPercentUtilization-Error"); } }
    private static OrionSetting PortPercentUtilizedWarning { get { return SettingsDAL.GetSetting("UDT-PortPercentUtilization-Warning"); } }
    private string netObjectID;
    private string editURL = string.Empty;
    public int PortsUsed { get; set; }
    public int PortsFree { get; set; }
    public int PercentPortsUsed { get; set; }
    public string formattedPercentPortsUsed { get; set; }
    public int PercentPortsFree { get; set; }
    public int TotalPorts { get; set; }

    protected string NetObjectID
    {
        get { return this.netObjectID; }
        set { this.netObjectID = value; }
    }
    
    protected string RememberExpandedGroups
    {
        get { return this.Resource.Properties["RememberCollapseState"] ?? "true"; }
    }

    protected string Filter
    {
        get { return this.Resource.Properties["Filter"] ?? string.Empty; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionUDTPHDeviceSummaryTotalPortsCurrUsed";
        }
    }


    protected void Page_Init(object sender, EventArgs e)
    {
        //if the graph is on the Node Details page then we should be able to get
        //the nodes netobject details
        if (IsPostBack) return;
        if (!string.IsNullOrEmpty(Resource.Properties["Title"])) Resource.Title = Resource.Properties["Title"];
        if (!string.IsNullOrEmpty(Resource.Properties["SubTitle"])) Resource.SubTitle = Resource.Properties["SubTitle"];
        NetObjectID = Request.QueryString["NetObject"];

        //This chart will be generated like summary Page for the views which have NetObject other than 'N' (Node)
        if (string.IsNullOrEmpty(NetObjectID) || NetObjectID.Split(':')[0].ToLower() != "n")
        {
            editURL = string.Format("/Orion/UDT/Resources/PortCharts/EditPortsByGroup.aspx?ResourceID={0}&ViewID={1}&NetObjectID={2}", this.Resource.ID, this.Resource.View.ViewID, HttpUtility.UrlDecode(NetObjectID)); 
            var grpNames = UDTChartsDAL.GetGroups();
            var groups = new System.Text.StringBuilder();
            grpNames.ForEach(grp =>
            {
                if (Resource.Properties.ContainsKey(grp.Key) && Convert.ToBoolean(Resource.Properties[grp.Key]))
                {
                    groups.Append( grp.Value + ",");
                }
            });
            NetObjectID = string.Empty;
            //no netobject so get all the port details
            PortsUsed = UDTChartsDAL.GetPortsInUseCount(groups.ToString().Trim(','), true);
            TotalPorts = UDTChartsDAL.GetPortCount(groups.ToString().Trim(','), true);
            chartImage.URL = UDTPortsCurrentlyUsedChartInfo.GenerateChartURL(this.Resource.ID, 200, 150, Filter, NetObjectID, groups.ToString().Trim(','));
            //UDTPortsCurrentlyUsedChartInfo.NetObjectID = string.Empty;
        }
        else
        {
            editURL = base.EditURL;
          //got a netobject so need to just get port details for that node
            string[] NetObjectParts = NetObjectID.Split(':');
            if (NetObjectParts.Length==2  &&  NetObjectParts[0].ToLower() == "n")
            {
                TotalPorts = UDTChartsDAL.GetPortCountForSwitch(NetObjectParts[1], true);
                if (TotalPorts==0)
                {
                    //if we returned no ports then the node is not a UDT node so hide the resource
                    Wrapper.Visible = false;
                    return;
                }
                PortsUsed = UDTChartsDAL.GetPortsInUseCountForSwitch(NetObjectParts[1], true);
                //UDTPortsCurrentlyUsedChartInfo.NetObjectID = netObjectID;
                string nodeName = UDTChartsDAL.GetNodeName(NetObjectParts[1]);
                //update the resource title to include the node name
                Resource.Title = String.Format(Resources.UDTWebContent.UDTWEBCODE_AK1_27, Resource.Title, nodeName);
                chartImage.URL = UDTPortsCurrentlyUsedChartInfo.GenerateChartURL(this.Resource.ID, 200, 150, Filter, NetObjectID, string.Empty);
            }
        }
        
        // create chart
      
      

        PortsFree = TotalPorts - PortsUsed;
        if (PortsFree < 0)
        {
            PortsFree = 0;
        }

        if (TotalPorts == 0)
        {
            PercentPortsUsed = 0;
            PercentPortsFree = 0;
        }
        else
        {
            double tmp = PortsUsed;
            tmp = (tmp/TotalPorts*100);
            PercentPortsUsed = (int) tmp;
            PercentPortsFree = 100 - PercentPortsUsed;
        }
        formattedPercentPortsUsed = FormatPortPercent(PercentPortsUsed);
    }
  

    protected void Page_Load(object sender, EventArgs e)
    {
  
    }

    public string FormatPortPercent(object value)
    {
        try
        {
            return FormatHelper.GetPercentFormat(Convert.ToDouble(value), PortPercentUtilizedError.SettingValue, PortPercentUtilizedWarning.SettingValue);
        }
        catch 
        {
            return String.Empty;
        }
    }
  

    #region Overrides of BaseResourceControl

    protected override string DefaultTitle
    {
        get { return defaultTitle; }
    }

    protected override void OnLoad(EventArgs e)
    {
 
    }

    public override string EditURL
    {
        get
        {

            return editURL; 
        }
    }

    #endregion

   

}



