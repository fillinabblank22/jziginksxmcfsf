﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PortsUsedOverTime.ascx.cs" Inherits="Orion_UDT_Resources_PortCharts_PortsUsedOverTime" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:PlaceHolder runat="server" ID="chartPlaceHolder" />
    </Content>
</orion:resourceWrapper>
