﻿<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true" CodeFile="EditPortsByGroup.aspx.cs" Inherits="Orion_UDT_Resources_PortCharts_EditPortsByGroup" %>
<%@ Import Namespace="SolarWinds.Orion.Web" %>
<%@ Import Namespace="SolarWinds.UDT.Web.Helpers" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" Assembly="OrionWeb" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

 <h1><%= String.Format(Resources.UDTWebContent.UDTWEBDATA_AK1_206, CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(!string.IsNullOrEmpty(Resource.Properties["Title"]) ? Resource.Properties["Title"] : Resource.Title, Request["NetObject"].UrlEncode())))%></h1>
 <orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="true" ShowSubTitleHintMessage="false" />
<table>
<tr>
    <td><h1><%= GroupListTitle%></h1></td>
</tr>
    <asp:Repeater runat="server" ID="rptrContainer">
    
        <ItemTemplate>
        <tr><td style="padding: 20px 10px;">
            <asp:CheckBox runat="server" AutoPostBack="False" Checked='<%# DataBinder.Eval(Container.DataItem,"ShowInPortChart") %>' Text='<%# DataBinder.Eval(Container.DataItem,"Name") %>'  ID="grpCheckBox" />
            <asp:HiddenField runat="server" ID="hdnPreviousState" Value='<%# DataBinder.Eval(Container.DataItem,"ShowInPortChart") %>'/>            
             </td></tr>
            </ItemTemplate>
      
    </asp:Repeater>
 </table>
          <div class="sw-btn-bar" style="padding-left: 15px;">
            <orion:LocalizableButton runat="server" ID="btnSubmit" OnClick="SubmitClick" LocalizedText="Submit" DisplayType="Primary" />            
        </div>
</asp:Content>
