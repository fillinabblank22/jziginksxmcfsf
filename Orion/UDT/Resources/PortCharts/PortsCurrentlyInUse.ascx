﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PortsCurrentlyInUse.ascx.cs" Inherits="Orion_UDT_Resources_PortCharts_PortsCurrentlyInUse" %>
<%@ Register TagPrefix="udt" TagName="ChartImage" Src="~/Orion/UDT/Controls/ChartImage.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
        <table border = "0" >
            <tr>
                <td style="border-width:0px;">
                    <table>
                        <tr>
                            <td style="border-width:0px;">    
                                <div class="UDT_PieChart">
                                    <udt:ChartImage runat="server" ID="chartImage" />
                                </div>
                             </td>
                           </tr>
                    </table>
                </td>
                <td style="border-width:0px;">
                    <table>
                        <tr>
                            <td bgcolor="#FCD928"></td>
                            <td> <%= Resources.UDTWebContent.UDTWEBDATA_AK1_181 %> </td>
                            <td> <%=PortsUsed%></td> 
                            <td> </td>               
                            <td> <%=formattedPercentPortsUsed%></td> 
                        </tr>
                        <tr>
                            <td bgcolor="#77BD2D" width="15px" ></td>
                            <td> <%= Resources.UDTWebContent.UDTWEBDATA_AK1_182 %> </td>
                            <td> <%=PortsFree%></td>                
                            <td> </td>
                            <td> <%=PercentPortsFree%>&nbsp;%</td>                             
                        </tr>
                        <tr>
                            <td></td>
                            <td> <%= Resources.UDTWebContent.UDTWEBDATA_AK1_183 %> </td>
                            <td> <%=TotalPorts%></td> 
                        </tr>
                    </table>
                  </td>
                </tr>
            </table>  
	</Content>
</orion:resourceWrapper>