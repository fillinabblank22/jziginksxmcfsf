<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LastXXEvents.ascx.cs" Inherits="Orion_UDT_Resources_Events_LastXXEvents" %>
<%@ Register TagPrefix="udt" TagName="EventList" Src="~/Orion/UDT/Controls/EventList.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
	<udt:EventList runat="server" ID="udtEventList" OnInit="grid_Init"/>
	</udt:EventList>
	</Content>
</orion:resourceWrapper>
