using System;
using System.Data;
using System.Text;
using Resources;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.UDT.Web.NetObjects;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;

using SolarWinds.UDT.Web.DAL;

public partial class Orion_UDT_Resources_Events_LastXXEvents : SolarWinds.Orion.Web.UI.BaseResourceControl
{
	// default number is used if there is no record in resource properties (this should never happened)
	static int defaultNumberOfEvents = 25;
    private string subTitle = "All Time";

    protected void grid_Init(object sender, EventArgs e)
    {
        DateTime periodBegin = DateTime.UtcNow;
        DateTime periodEnd = DateTime.UtcNow;

        // get period from resources
        string periodName = Resource.Properties["Period"];

        // if there is no valid period set "Last 12 Months" as default
        if (String.IsNullOrEmpty(periodName))
            periodName = "Last 12 Months";

        // parse period to begin and end
        Periods.Parse(ref periodName, ref periodBegin, ref periodEnd);
        
        int maxEvents;
        try
        {
            maxEvents = Int32.Parse(Resource.Properties["MaxEvents"]);
        }
        catch
        {
            maxEvents = defaultNumberOfEvents;
        }

        
        // data bind
        udtEventList.DataSource = UDTResourceDAL.GetAllUDTEvents(periodBegin, periodEnd, maxEvents);
        udtEventList.DataBind();
    }

	#region properties

	// overridden main title of resources
	public override string DisplayTitle
	{
		get
		{
            string result;
            try
            {
                result = Resource.Properties["MaxEvents"];
            }
            catch
            {
                result = defaultNumberOfEvents.ToString();
            }
            
			//if no title has been set (or the title matches the resource name ''Last XX Events, then use the maxevents value to display a default title
		    return !string.IsNullOrEmpty(Resource.Title.Trim()) && (Resource.Title.Trim() != Resource.Name)
		               ? String.Format(Resource.Title)
		               : String.Format(UDTWebContent.UDTWEBCODE_TM0_16, result);
		}
	}

	// overridden subtitle of resource
	public override string SubTitle
	{
		get
		{
			if (!string.IsNullOrEmpty(Resource.SubTitle.Trim()))
				return Resource.SubTitle;
			
			// if subtitle is not specified subtitle is a period
			string period = Resource.Properties["Period"];

			if (String.IsNullOrEmpty(period))
				return String.Empty;

			return Periods.GetLocalizedPeriod(period);
		}
	}

	// overridden default title
	protected override string DefaultTitle
	{
		get { return UDTWebContent.UDTWEBCODE_TM0_15; }
	}

	// overridden help fragment
	public override string HelpLinkFragment
	{
		get
		{
            return "OrionPHResourceLastXXEvents";
		}
	}

	public override string EditControlLocation
	{
		get { return "/Orion/UDT/Controls/EditResourceControls/EditLastXXEvents.ascx"; }
	}

	#endregion


    //// resolves url arguments (for backward redirection)
    //private string GetUrlArg()
    //{
    //    string netObjectType = String.Empty;
    //    int netObjectID = -1;
    //    int nodeID = -1;

    //    GetCurrentNetObjectID(out nodeID, out netObjectType, out netObjectID);

    //    StringBuilder urlArg = new StringBuilder("?");

    //    // resource
    //    urlArg.AppendFormat("ResourceID={0}", this.Resource.ID);

    //    // viewID
    //    urlArg.AppendFormat("&ViewID={0}", this.Resource.View.ViewID);

    //    // net object
    //    if (!String.IsNullOrEmpty(netObjectType) && netObjectID > 0)
    //        urlArg.AppendFormat("&NetObject={0}:{1}", netObjectType, netObjectID);
    //    else
    //        urlArg.Append("&NetObject=");

    //    return urlArg.ToString();
    //}

}