using System;
using Resources;
using SolarWinds.Orion.Web.UI;

public partial class Orion_UDT_NodeTree : BaseResourceControl
{
	public override string EditControlLocation
	{
		get { return "/Orion/UDT/Controls/EditResourceControls/EditUDTNodeTree.ascx"; }
	}

	public override string EditURL
	{
		get
		{
			string url = string.Format("/Orion/NetPerfMon/Resources/EditResource.aspx?ResourceID={0}&HideInterfaceFilter=True", this.Resource.ID);

			if (!string.IsNullOrEmpty(Request["NetObject"]))
				url = string.Format("{0}&NetObject={1}", url, Request["NetObject"]);
			if (Page is OrionView)
				url = string.Format("{0}&ViewID={1}", url, ((OrionView)Page).ViewInfo.ViewID);
			if (!String.IsNullOrEmpty(Request.QueryString["ThinAP"]))
				url = String.Format("{0}&ThinAP={1}", url, Request.QueryString["ThinAP"]);

			return (url);
		}
	}

	protected override void OnInit(EventArgs e)
	{
		Wrapper.ManageButtonImage="~/Orion/UDT/Images/Resources/button_manage_ports.gif";
        Wrapper.ManageButtonTarget = "~/Orion/UDT/Ports/Default.aspx";
        Wrapper.ShowManageButton = SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer || Profile.AllowNodeManagement;

		base.OnInit(e);
	}

	protected override string DefaultTitle
	{
		get { return UDTWebContent.UDTWEBCODE_TM0_1; }
	}

	public override string HelpLinkFragment
	{
		get
		{
            return "OrionUDTPHDeviceSummaryAllUDTNodes";
		}
	}
}
