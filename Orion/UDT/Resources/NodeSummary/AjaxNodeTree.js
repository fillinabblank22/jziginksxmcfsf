﻿UDT = {};

UDT.UDTNodeTree = {
    ResourceId: 0,
    
    Succeeded: function (result, context) {
        var contentsDiv = $(context);
        $(contentsDiv).get(0).innerHTML = result;
        contentsDiv.slideDown('fast');
        UDT.UDTNodeTree.AutoClickLinks(contentsDiv);
        if (result == "") {
            $("#UDTTreeNode-" + this.ResourceId + "-NoDataContent").show();
        }
    },

    Failed: function (error, context) {
        var contentsDiv = context;
        contentsDiv.innerHTML = String.format("@{R=UDT.Strings;K=UDTWEBJS_AK1_89;E=js}", error.get_message());
    },

    AutoClickLinks: function (contentsDiv) {
        // auto-click the "get 100 more Nodes" links
        $("[id*='-startFrom-'] a", contentsDiv).click();

        // expand any nodes that are marked to be expanded	    
        $("[expand]", contentsDiv).removeAttr("expand").click();
    },


    Click: function (rootId, resourceId, keys) {
        return UDT.UDTNodeTree.HandleClick(rootId, function (contentDiv) {
            UDTNodeTree.GetTreeSection(resourceId, rootId, keys, 0, UDT.UDTNodeTree.Succeeded, UDT.UDTNodeTree.Failed, contentDiv);
        }, function () {
            UDTNodeTree.CollapseTreeSection(resourceId, keys);
        });
    },

    InterfaceClick: function (rootId, resourceId, keys) {
        return UDT.UDTNodeTree.HandleClick(rootId, function (contentDiv) {
            UDTNodeTree.GetTreeSection(resourceId, rootId, keys, 0, UDT.UDTNodeTree.Succeeded, UDT.UDTNodeTree.Failed, contentDiv);
        }, function () {
            UDTNodeTree.CollapseTreeSection(resourceId, keys);
        });
    },

    LoadRoot: function (treeDiv, resourceId) {
        ResourceId = resourceId;
        treeDiv.html("<div>@{R=UDT.Strings;K=UDTWEBJS_AK1_56;E=js}</div>");
        UDTNodeTree.GetTreeSection(resourceId, "NT_r" + resourceId, [], 0, UDT.UDTNodeTree.Succeeded, UDT.UDTNodeTree.Failed, treeDiv.get(0));
    },

    HandleClick: function (rootId, expandCallback, collapseCallback) {
        var toggleImg = $('#' + rootId + '-toggle');
        var contentsDiv = $('#' + rootId + '-contents');

        if (!contentsDiv.is(':hidden')) {
            contentsDiv.slideUp('fast');
            toggleImg.attr("src", "/Orion/images/Button.Expand.gif");
            collapseCallback();
        } else {
            contentsDiv.html("@{R=UDT.Strings;K=UDTWEBJS_AK1_56;E=js}");
            contentsDiv.slideDown('fast');
            toggleImg.attr('src', "/Orion/images/Button.Collapse.gif");
            expandCallback(contentsDiv.get(0));
        }

        return false;
    },

    GetMore: function (linkId, resourceId, keys, startFrom) {
        var contentsDiv = $get(linkId);
        contentsDiv.innerHTML = "@{R=UDT.Strings;K=UDTWEBJS_AK1_56;E=js}";
        UDTNodeTree.GetTreeSection(resourceId, linkId, keys, startFrom, UDT.UDTNodeTree.Succeeded, UDT.UDTNodeTree.Failed, contentsDiv);
        return false;
    }


};

function UDTNodeTreeSectionReceived(result, context)
{
    var contentsDiv = $(context);
    contentsDiv.html(result);
    contentsDiv.slideDown('fast');
    AutoClickGetMoreLinks(contentsDiv);
}

function AutoClickGetMoreLinks(contentsDiv)
{
	// auto-click the "get 100 more nodes" links
	$("[id*='-startFrom-'] a", contentsDiv).click();
}

$(function() {
	AutoClickGetMoreLinks();
});

function UDTNodeTreeSectionFailed(error, context)
{
	var contentsDiv = context;
	contentsDiv.innerHTML = String.format("@{R=UDT.Strings;K=UDTWEBJS_AK1_89;E=js}", error.get_message()); 
}

function FastUDTNodeTree_Click(rootId, resourceId, keys)
{
	var toggleImg = $get(rootId + '-toggle');
	var contentsDiv = $get(rootId + '-contents');
	
	if (contentsDiv.style.display == "") {
		contentsDiv.style.display = "none";
		toggleImg.src = "/Orion/images/Button.Expand.gif";
	} else {
        contentsDiv.innerHTML = "@{R=UDT.Strings;K=UDTWEBJS_AK1_56;E=js}";
		contentsDiv.style.display = "";
		toggleImg.src = "/Orion/images/Button.Collapse.gif";
		UDTNodeTree.GetTreeSection(resourceId, rootId, keys, 0, UDTNodeTreeSectionReceived, UDTNodeTreeSectionFailed, contentsDiv);
	}
	
	return false;
}

function FastUDTNodeTree_GetMore_Click(linkId, resourceId, keys, startFrom)
{
	var contentsDiv = $get(linkId);
	contentsDiv.innerHTML = "@{R=UDT.Strings;K=UDTWEBJS_AK1_56;E=js}";
	UDTNodeTree.GetTreeSection(resourceId, linkId, keys, startFrom, UDTNodeTreeSectionReceived, UDTNodeTreeSectionFailed, contentsDiv);
	
	return false;
}
