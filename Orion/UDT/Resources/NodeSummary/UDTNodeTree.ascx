<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UDTNodeTree.ascx.cs" Inherits="Orion_UDT_NodeTree" %>
<%@ Import Namespace="Resources" %>

<style type="text/css">
.UDT_GettingStartedBox
{
    background-color: #e4f1f8;
    padding: 1em;
    margin-top: 1em;
}

.UDT_GettingStartedBox div
{
    padding-top: 1em;
    clear: both;
}

.UDT_GettingStartedBox div.UDT_First
{
    padding-top: 0em;
}

.UDT_GettingStartedBox div h3
{
    font-weight: normal;
    font-size: 11pt;
    margin: 12px 0px 0px 40px;
}

.UDT_GettingStartedBox div p
{
    margin: 0px 0px 0px 25px;
    line-height: 1.8em;
}

.UDT_GettingStartedBox div img
{
    float: left;
    margin-top: 2px;
}

.UDT_Buttons
{
    text-align: right;
}


</style>

<asp:ScriptManagerProxy id="NodeTreeScriptManager" runat="server">
    <Services>
		<asp:ServiceReference path="/Orion/UDT/Services/UDTNodeTree.asmx" />
	</Services>
	<Scripts>
		<asp:ScriptReference Path="/Orion/UDT/Resources/NodeSummary/AjaxNodeTree.js" />
	</Scripts>
</asp:ScriptManagerProxy>


<orion:resourceWrapper runat="server" ID="Wrapper" >
	<Content>
	    <script type="text/javascript">
	        //<![CDATA[
	        $(function() {
	            UDT.UDTNodeTree.LoadRoot($('#<%=this.nodetree.ClientID%>'), '<%=this.Resource.ID %>');
	        });
	        //]]>
	    </script>
	
		<div class="Tree" ID="nodetree" runat="server">
			<asp:Literal runat="server" ID="TreeLiteral" />
		</div>

   <!-- Following show me when no nodes exists in DB -->
        <div id="UDTTreeNode-<%= this.Resource.ID %>-NoDataContent" style="display: none">
        
          <%= UDTWebContent.UDTWEBDATA_TM0_5 %>
           <div class="UDT_GettingStartedBox">
              <div class="UDT_First">
                     <img src="/Orion/images/getting_started_36x36.gif" alt="<%= UDTWebContent.UDTWEBDATA_TM0_6 %>" />
                 <h3><b><%= UDTWebContent.UDTWEBDATA_TM0_1 %></b> <%= UDTWebContent.UDTWEBDATA_TM0_2 %></h3>
              </div>

              <div>
                 <%= UDTWebContent.UDTWEBDATA_TM0_3 %>
              </div>

              <div>
                <%= UDTWebContent.UDTWEBDATA_TM0_4 %>
              </div>

              <div class="UDT_Buttons">
                  <orion:LocalizableButton runat="server" ID="discoverPortButton" PostBackUrl="/Orion/Discovery/Default.aspx" DisplayType="Primary" LocalizedText="CustomText" Text= "<%$ Resources: UDTWebContent, UDTWEBDATA_TM0_2 %>"  /> 
              </div>
           </div>
        </div>


	</Content>
</orion:resourceWrapper>
