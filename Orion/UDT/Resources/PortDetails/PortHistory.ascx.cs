﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Web.Helpers;
using SolarWinds.UDT.Web.NetObjects;
using SolarWinds.UDT.Web.Providers;
using SolarWinds.UDT.Web.Resources;
using SolarWinds.UDT.Logging;


public partial class Orion_UDT_Resources_PortHistory : UDTBaseResourceControl
{
    private static readonly Log _log = new Log("PortHistoryResource");
    private bool _isIpamWithMinVersion;
    protected override Log GetLogger() { return _log;}

    protected string TimePeriod = Resources.UDTWebContent.UDTWCODE_VB1_51;

    private string[] ResourceSearchabelColumns;
    
    

    protected override String DefaultTitle { get { return Resources.UDTWebContent.UDTWEBCODE_VB1_71; } }

    public override String SubTitle { get { return Periods.GetLocalizedPeriod(TimePeriod); } }

    public override string HelpLinkFragment { get { return "OrionUDTPHPortHistory";} }

    private UDTPortNetObject _udtPort = null;

    public UDTPortNetObject UDTPort
    {
        get
        {
            var udtPortProvider = GetInterfaceInstance<IUDTPortProvider>();
            if (udtPortProvider != null)
            {
                return udtPortProvider.UDTPortNetObject;
            }
            return _udtPort;
        }
        set { _udtPort = value; }
    }

    public override string EditURL
    {
        get { return string.Format("/Orion/UDT/Resources/PortDetails/EditPortHistoryDetails.aspx?ResourceID={0}&ViewID={1}&NetObjectID={2}", this.Resource.ID, this.Resource.View.ViewID, HttpUtility.UrlDecode(UDTPort.NetObjectID)); }
    }

    protected string SortDirection
    {
        get { return _sortDirection ?? (_sortDirection = GetPersistentProperty(KeyNameSortDirection, "DESC")); }
        set 
        { 
            _sortDirection = value;
            
            SetPersistentProperty(KeyNameSortDirection, _sortDirection);
            
            _log.Trace(log => log.TraceFormat("{1}  set_SortDirection - '{0}'", _sortDirection, LogID));
        }
    }
    private string _sortDirection;

    protected string PrevSortDirection
    {
        get { return _prevSortDirection ?? (_prevSortDirection = GetPersistentProperty(KeyNamePrevSortDirection, "ASC")); }
        set
        {
            _prevSortDirection = value;
            
            SetPersistentProperty(KeyNamePrevSortDirection, _prevSortDirection);
            
            _log.Trace(log => log.TraceFormat("{1}  set_PrevSortDirection - '{0}'", _prevSortDirection, LogID));
        }
    }
    private string _prevSortDirection;

    protected override void OnInitResource(EventArgs e)
    {
        _isIpamWithMinVersion = UDTIPAMIntegration.IsIPAMInstalled();
        if (_isIpamWithMinVersion)
        {
            ResourceSearchabelColumns = new[] { "MACAddress", "IPAddress", "DNSName", "SubnetWithMask" };
        }
        else
        {
            ResourceSearchabelColumns = new[] {"MACAddress", "IPAddress", "DNSName"};
        }
        SearchableColumns = ResourceSearchabelColumns;

        try
        {
            if (Resource.Properties.ContainsKey("Period"))
                TimePeriod = Resource.Properties["Period"];
        }
        catch(Exception ex)
        {
            TimePeriod = Resources.UDTWebContent.UDTWCODE_VB1_51;
        }

        RenderingDone +=
            (o, ev) =>
                {
                    PrevSortDirection = SortDirection;
                };
    }

    protected override Control GetResourceWrapperControl()
    {
        return Wrapper;
    }

    protected override void OnPreRenderResource(EventArgs e)
    {
        base.OnPreRenderResource(e);

        if (!string.IsNullOrEmpty(WarningMsg) && IsSearchTextChanged)
        {
            SearchInCachedData = false;
            ReloadData = true;
        }
        if (!_isIpamWithMinVersion)
        {
            IPAMSubnetTd.Visible = false;
        }
       
    }

    protected override DataTable LoadResourceData(string searchText)
    {
        var periodBegin = new DateTime();
        var periodEnd = new DateTime();
        Periods.Parse(ref TimePeriod, ref periodBegin, ref periodEnd);
        string  warningMsg;

        var data = UDTPort.GetPortHistoryData(TimePeriod, out warningMsg);
        
        WarningMsg = warningMsg;

        // new data set loaded, so we need ensure that rendering will  do proper ordering
        PrevSortDirection = SortDirection == "ASC" ? "DESC" : "ASC";

        if (_isIpamWithMinVersion)
        {
          //  data.Columns.Remove("GroupId");
            DataTable dataWithSubnet = data.Clone();
            var distinctRows =
                data.DefaultView.ToTable(true, "FirstSeen", "LastSeen", "EndpointID", "MACAddress", "IPAddress", "ConnectionType",
                                         "DNSName").Rows.OfType<DataRow>();
            foreach (DataRow  disRow in distinctRows)
            {
                var rows = data.Select("CONVERT(FirstSeen, System.String) = '" + disRow["FirstSeen"] + "' and CONVERT(LastSeen, System.String) = '" + disRow["LastSeen"] + "' and MACAddress = '" + disRow["MACAddress"] + "' and IPAddress = '" + disRow["IPAddress"] + "'");
                string subnetMask = "";
                string subnet = ""; 
                string subnetUrl = "";
                string subnetStatusIcon = "";
                foreach (DataRow row in rows)
                {
                    if (row["SubnetWithMask"] != "" && row["SubnetAddress"] != "" && row["SubnetUrl"] != "" &&
                        row["SubnetStatusIcon"] != "")
                    {
                        subnetMask += row["SubnetWithMask"] + ",";
                        subnet += row["SubnetAddress"] + "$";
                        subnetUrl += row["SubnetUrl"] + "$";
                        subnetStatusIcon += row["SubnetStatusIcon"] + "$";
                    }
                }
                subnetMask = subnetMask.Trim(',');
                subnet = subnet.Trim('$');
                subnetUrl = subnetUrl.Trim('$');
                subnetStatusIcon = subnetStatusIcon.Trim('$');

                dataWithSubnet.Rows.Add(disRow["FirstSeen"], disRow["LastSeen"], disRow["EndpointID"], disRow["MACAddress"], disRow["IPAddress"], disRow["DNSName"], disRow["ConnectionType"], subnet, subnetUrl, subnetStatusIcon, subnetMask);
                 subnetMask = "";
                 subnet = "";
                 subnetUrl = "";
                 subnetStatusIcon = "";
            }
            return dataWithSubnet;
        }

        return data;
    }

    protected override void RenderResource()
    {
        //apply sorting here to full data set
        if (SortDirection != PrevSortDirection && Data.Rows.Count > 0)
        {
            var dv = new DataView(Data);
            dv.Sort = string .Format("ConnectionType ASC, LastSeen {0}, FirstSeen {0}", SortDirection);
            Data = dv.ToTable();
        }

        var data = ApplyFiltering(Data);

        data = ApplyPaging(data);

     //build the table
        if (data!=null)
        {
            foreach (DataRow row in data.Rows)
            {
                var htmlRow = new HtmlTableRow();

                if (row["FirstSeen"] == DBNull.Value ||
                    (row["LastSeen"].ToString() != DALHelper.SQLMaxDate.ToString() && searchCurrent))
                    continue;

                var fs = DALHelper.GetValueOrDefault(row["FirstSeen"], DALHelper.SQLMaxDate);
                var ls = DALHelper.GetValueOrDefault(row["LastSeen"], DALHelper.SQLMaxDate);
                var firstSeen = fs == DALHelper.SQLMaxDate ? Resources.UDTWebContent.UDTWEBCODE_VB1_72 : fs.ToString();
                var lastSeen = ls == DALHelper.SQLMaxDate ? Resources.UDTWebContent.UDTWCODE_VB1_50 : ls.ToString();

                var ipAddress = DALHelper.GetValueOrDefault(row["IPAddress"], string.Empty);
                var macAddress = DALHelper.GetValueOrDefault(row["MACAddress"], string.Empty);
                var hostName = DALHelper.GetValueOrDefault(row["DNSName"], string.Empty);
                var connectionType = DALHelper.GetValueOrDefault<byte>(row["ConnectionType"], 3);

                var htmlCell = new HtmlTableCell();

                //TimePeriod
                htmlCell.Attributes.Add("Class", "Property");
                htmlCell.InnerHtml = String.Format(Resources.UDTWebContent.UDTWCODE_VB1_49, firstSeen, lastSeen);
                htmlRow.Cells.Add(htmlCell);

                //Mac Address
                htmlCell = new HtmlTableCell();
                htmlCell.Attributes.Add("Class", "Property");
                string renderConnectionType;
                switch (connectionType)
                {
                    case 1:
                        renderConnectionType =
                            String.Format("<img src='/Orion/UDT/Images/icon_direct.png' title='{0}' alt='{0}' style='vertical-align:bottom;'/>&nbsp;", Resources.UDTWebContent.UDTWEBCODE_AK1_37);
                        break;
                    case 2:
                        renderConnectionType =
                            String.Format("<img src='/Orion/UDT/Images/icon_indirect.png' title='{0}' alt='{0}' style='vertical-align:bottom;'/>&nbsp;", Resources.UDTWebContent.UDTWEBCODE_AK1_38);
                        break;
                    default:
                        renderConnectionType =
                            String.Format("<img src='/Orion/UDT/Images/icon_direct_unknown.png' title='{0}' alt='{0}' style='vertical-align:bottom;'/>&nbsp;", Resources.UDTWebContent.UDTWEBCODE_AK1_39);
                        break;
                }
                htmlCell.InnerHtml = string.IsNullOrEmpty(macAddress)
                                         ? "&nbsp;"
                                         : String.Format("{1}<a href='/Orion/UDT/EndpointDetails.aspx?NetObject=UE-MAC:VAL={0}';>{0}</a>", macAddress, renderConnectionType);
                htmlRow.Cells.Add(htmlCell);

                //IP Address
                htmlCell = new HtmlTableCell();
                htmlCell.Attributes.Add("Class", "Property");
                htmlCell.InnerHtml = ipAddress == string.Empty
                                         ? "&nbsp;"
                                         : String.Format("<a href='/Orion/UDT/EndpointDetails.aspx?NetObject=UE-IP:VAL={1}';>{0}</a>", ipAddress, DALHelper.IpAddressToStorageFormat(ipAddress));
                htmlRow.Cells.Add(htmlCell);

                //Host
                htmlCell = new HtmlTableCell();
                htmlCell.Attributes.Add("Class", "Property");
                htmlCell.InnerHtml = hostName == string.Empty
                                         ? "&nbsp;"
                                         : String.Format("<a href='/Orion/UDT/EndpointDetails.aspx?NetObject=UE-DNS:VAL={0}';>{0}</a>", hostName);
                htmlRow.Cells.Add(htmlCell);


                if (_isIpamWithMinVersion)
                {
                    string subnet = DALHelper.GetValueOrDefault(row["SubnetAddress"], string.Empty);
                    string subnetsIconUrl = "";
                    if (!string.IsNullOrEmpty(subnet))
                    {
                        string iconImage = "<img src='{0}' style='vertical-align:bottom;'/>&nbsp;";
                        string subnetWithMasks = DALHelper.GetValueOrDefault(row["SubnetWithMask"], string.Empty);
                        string icons = DALHelper.GetValueOrDefault(row["SubnetStatusIcon"], string.Empty);
                        string url = DALHelper.GetValueOrDefault(row["SubnetUrl"], string.Empty);
                        string[] subnetsurl = url.Split('$');
                        string[] subnetIcons = icons.Split('$');
                        string[] subnetWithMask = subnetWithMasks.Split(',');

                        try
                        {
                            for (int i = 0; i < subnetIcons.Length; i++)
                            {
                                string conflictIconAnc = String.Format(iconImage, subnetIcons[i]) + "<a id='ConflictIconDiv'href={1} border='0' style='font-size: 8pt !important;'>{0}</a>";
                                subnetsIconUrl += string.Format(conflictIconAnc, subnetWithMask[i], subnetsurl[i]) + " <br/>";
                            }
                        }
                        catch (Exception ex)
                        {
                            _log.ErrorFormat(
                                "Error while constructing subnet icon urls. subnet: {0}, urls: {1}, icons :{2}, Exception: {3}",
                                subnetWithMask.Length, subnetsurl.Length, subnetIcons.Length, ex);
                            subnetsIconUrl = Resources.UDTWebContent.UDTWEBDATA_GK_7;
                        }
                    }
                    htmlCell = new HtmlTableCell();
                    htmlCell.Attributes.Add("Class", "Property");
                    htmlCell.InnerHtml = subnet == string.Empty
                                             ? Resources.UDTWebContent.UDTWEBDATA_GK_7
                                             : subnetsIconUrl;
                    htmlRow.Cells.Add(htmlCell);
                }

                PortHistory.Rows.Add(htmlRow);

            }
        }
    }

    protected override void HandleResourceWarning()
    {
        ExceptionWarning.Text = WarningMsg;
        ExceptionWarning.Visible = !string.IsNullOrEmpty(WarningMsg);
    }

    public void SortAsc_Click(object sender, ImageClickEventArgs e)
    {
        SortAscButton.Visible = false;
        SortDescButton.Visible = true;
        SortDirection = "ASC";
    }

    public void SortDesc_Click(object sender, ImageClickEventArgs e)
    {
        SortAscButton.Visible = true;
        SortDescButton.Visible = false;
        SortDirection = "DESC";
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IUDTPortProvider) }; }
    }
}



