﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.IO;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.UDT.Common;
using SolarWinds.UDT.Common.Models;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Web.Helpers;
using SolarWinds.UDT.Web.Resources;

public partial class Orion_UDT_Resources_PortDetails_PortDetailsList : UDTBaseResourceControl
{
    private static Log _log = new Log("PortDetailsList");
    public HtmlTable PortHtmlTable;
    protected override Log GetLogger()
    {
        return _log;
    }

    private class PortDataEntry
    {
        public readonly int PortID;
        public readonly int Type;
        public readonly string Name;
        public readonly int OperationalStatus;
        public readonly int VlanID;
        public readonly string Mac;
        public readonly string Ip;
        public readonly string Dns;
        public readonly string vrfName;
        public readonly int AdministrativeStatus;
        public readonly string VendorIcon;
        public readonly string Vendor;
        public readonly bool IsUserExist;

        public PortDataEntry(DataRow row, int portID = 0)
        {
            if (portID > 0)
                PortID = portID;

            if (row == null) return;

            PortID = portID == 0 ? DALHelper.GetValueOrDefault(row["PortID"], 0) : portID;
            Type = DALHelper.GetValueOrDefault<Int16>(row["PortType"], 0);
            Name = DALHelper.GetValueOrDefault(row["Name"], "");
            OperationalStatus = DALHelper.GetValueOrDefault<Int16>(row["OperationalStatus"], 2);
            VlanID = DALHelper.GetValueOrDefault(row["VLANID"], 0);
            Mac = DALHelper.GetValueOrDefault(row["MACAddress"], "");
            Ip = DALHelper.GetValueOrDefault(row["IPAddress"], "");
            Dns = DALHelper.GetValueOrDefault(row["DNSName"], "");
            vrfName = DALHelper.GetValueOrDefault(row["VrfName"], "");
            AdministrativeStatus = DALHelper.GetValueOrDefault<Int16>(row["AdministrativeStatus"], 2);
            VendorIcon = DALHelper.GetValueOrDefault(row["VendorIcon"], "");
            Vendor = DALHelper.GetValueOrDefault(row["Vendor"], "");
            IsUserExist = DALHelper.GetValueOrDefault<bool>(row["IsUserExist"], false);
        }
    }

    private static readonly string[] ResourceSearchabelColumns = new[] {"Name", "MACAddress", "IPAddress", "DNSName"};

    protected static readonly string EscapeMacChars = @"[:\-. ]";

    protected string PostBackStr;
    private List<int> _expendedPorts;

    protected override String DefaultTitle
    {
        get { return Resources.UDTWebContent.UDTWEBCODE_VB1_73; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionUDTPHPortDetails"; }
    }

    protected int NodeID
    {
        get
        {
            if (!_nodeID.HasValue)
            {
                int nodeID;
                _nodeID = int.TryParse(NetObject.Value, out nodeID) ? nodeID : 0;
            }
            return _nodeID.Value;
        }
    }

    private int? _nodeID;

    protected string NodeCaption
    {
        get { return _nodeCaption ?? (_nodeCaption = GetPersistentProperty("NodeCaption", string.Empty)); }
        set
        {
            _nodeCaption = value;
            SetPersistentProperty("NodeCaption", value);
        }
    }

    private string _nodeCaption;

    private OidEnums InterfaceTypes
    {
        get { return _ifTypes ?? (_ifTypes = GetPersistentProperty("InterfaceTypes", default(OidEnums))); }
        set
        {
            _ifTypes = value;
            SetPersistentProperty("InterfaceTypes", value);
        }
    }

    private OidEnums _ifTypes;

    private DataTable RouterList
    {
        get { return _routerList ?? (_routerList = GetPersistentProperty("RouterList", default(DataTable))); }
        set { SetPersistentProperty("RouterList", value); }
    }

    private DataTable _routerList;

    #region == Base class method implementation ==

    protected override Control GetResourceWrapperControl()
    {
        return Wrapper;
    }

    protected override void OnInitResource(EventArgs e)
    {
        AsyncDataLoad = false;
        CacheResourceData = true;
        TruncateCachedData = false;

        SearchableColumns = ResourceSearchabelColumns;

        if (Profile.AllowNodeManagement || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
              PortHtmlTable = PortHtmlTable2;
        else
            PortHtmlTable = PortHtmlTable1;
    }

    protected override void OnLoadResource(EventArgs e)
    {
        if (NodeID == 0)
            return;
        PostBackStr = Page.ClientScript.GetPostBackEventReference(updatePanel, "CustomArgument");

        _expendedPorts =
            expandedPortsString.Value.Split(',').Where(s => !string.IsNullOrEmpty(s)).Select(int.Parse).ToList();

        if (!IsPostBack)
        {
            NodeCaption = UDTChartsDAL.GetNodeName(NodeID.ToString(CultureInfo.InvariantCulture));

            try
            {
                InterfaceTypes = MibDAL.GetInterfaceTypes();
            }
            catch (Exception ex)
            {
                _log.ErrorFormat("There was an error fetching ifType Enum from MIB database.  Details: {0}",
                                 ex.ToString());
            }


            bool isFound = false;
            var scm = ScriptManager.GetCurrent(Page);
            if (scm.Services.Count>0)
            {
                for (int i = 0; i < scm.Services.Count; i++)
                {
                    ServiceReference sr = scm.Services[i];
                    if (sr.Path == "~/Orion/Services/Information.asmx")
                    {
                        isFound = true;
                        break;
                    }
                    
                }
                if (!isFound)
                    scm.Services.Add(new ServiceReference("~/Orion/Services/Information.asmx"));
            }
            else
            {
                scm.Services.Add(new ServiceReference("~/Orion/Services/Information.asmx"));
            }
           

        }
      


        Resource.Title = String.Format(Resources.UDTWebContent.UDTWEBCODE_VB1_74, Resource.Title, NodeCaption);
    }

    protected override DataTable LoadResourceData(string searchText)
    {
        if (NodeID == 0)
            return null;
        //hiddenNodeID.Value = NodeID.ToString();
        // load all port data
        var res = UDTPortDAL.GetPortDetailsList(NodeID);
        if (res.Rows.Count == 0)
            return res;
       
        using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
        {
            int[] nodeIDs = new int[1];
            nodeIDs[0] = NodeID;
            var RWCommunityDefinedNodes = bl.GetRWCommunityNotDefinedNodes(nodeIDs);
            hdnRWCommunityDefined.Value = RWCommunityDefinedNodes.Trim();
        }

        hdnnodeId.Value = NodeID.ToString();
        // extract router list
        using (var view = new DataView(res))
        {
            view.RowFilter = "RouterNodeID is not NULL";
            view.Sort = "RouterName ASC";
            RouterList = view.ToTable(true, new[] {"RouterNodeID", "RouterName", "RouterIP"});
        }

        FillRouterListControl();
        // Force search over cached data 
        // (without this if there are more rows in data than [UDTSettingDAL.SearchResultsMaxRows] search over cached data is disabled)
        SearchInCachedData = true;

        return res;
    }

    protected override void RenderResource()
    {
        if (Data == null || Data.Rows.Count == 0)
        {
            Wrapper.Visible = false;
            return;
        }

        var data = Data;

        // filter on status field
        if (portStatus.SelectedIndex == 1) 
        {
            using (var view = new DataView(data))
            {
                view.RowFilter = "OperationalStatus = 1";//Active ports are only the ports that are "up". 
                data = view.ToTable();
            }
        }
        else if (portStatus.SelectedIndex > 1) 
        {
            using (var view = new DataView(data))
            {
                view.RowFilter = "OperationalStatus > 1";//Inactive ports are any ports with status different than "up"
                data = view.ToTable();
            }
        }

        // filter on router ID
        if (lstRouters.SelectedIndex > 0)
        {
            var routerID = lstRouters.SelectedValue;
            using (var view = new DataView(data))
            {
                view.RowFilter = string.Format("RouterNodeID = {0} OR RouterNodeID IS NULL", routerID);
                data = view.ToTable();
            }
        }
        data = MakeDistinctAndOrderBy(data, /*"PortID"*/"Name");

        data = ApplyFiltering(data);

        TotalRows = data.Rows.Count;

        // fill output table
        var grouped = data.Rows.Cast<DataRow>().GroupBy(r => (int) r[0]); // by PortID

        foreach (
            var pg in grouped.Select(g => new {PortID = g.Key, PortRows = g.ToList()}).Where(g => g.PortRows.Count > 0))
        {
            if (pg.PortRows.Count == 1)
            {
                AddPortSingleTableRow(new PortDataEntry(pg.PortRows.First(), pg.PortID));

                continue;
            }

            AddGroupOfRows(pg.PortID, pg.PortRows);
        }
    }

    #endregion

    #region == Rendering helpers ==

    private const string styleExtraPadding = "padding-left: 6px; padding-right: 6px";
    private const string styleNoBorder = "border-bottom: none !important";
    private const string NetPerfMonImagePath = "/NetPerfMon/images/";
    private const string NetPerfMonImageVendorPath = NetPerfMonImagePath + "Vendors/";

    private void AddGroupOfRows(int portID, IList<DataRow> rows)
    {
        const int maxRowsPerPort = 5;
        var expended = _expendedPorts.Contains(portID);

        rows = OrderRowsBy(rows, "DNSName");

        var showExpendControl = rows.Count > maxRowsPerPort;
        var max = rows.Count <= maxRowsPerPort
                      ? rows.Count - 1
                      : expended
                            ? rows.Count
                            : maxRowsPerPort;

        AddPortFirstTableRow(new PortDataEntry(rows[0], portID));

        for (int i = 1; i < max; i++)
        {
            AddPortInnerTableRow(new PortDataEntry(rows[i], portID));
        }

        if (showExpendControl)
        {
            AddPortExpendControlTableRow(new PortDataEntry(null, portID), expended);
        }
        else
        {
            AddPortLastTableRow(new PortDataEntry(rows[rows.Count - 1], portID));
        }
    }

    private void AddPortSingleTableRow(PortDataEntry entry)
    {
        var htmlRow = new HtmlTableRow();

        AddPortCommonCells(htmlRow, entry);
        AddPortEndpointCells(htmlRow, entry, styleExtraPadding);
    
        PortHtmlTable.Rows.Add(htmlRow);
    }

    private void AddPortFirstTableRow(PortDataEntry entry)
    {
        var htmlRow = new HtmlTableRow();

        AddPortCommonCells(htmlRow, entry, styleNoBorder);
        AddPortEndpointCells(htmlRow, entry, styleNoBorder + "; " + styleExtraPadding);

        PortHtmlTable.Rows.Add(htmlRow);
    }

    private void AddPortInnerTableRow(PortDataEntry entry)
    {
        var htmlRow = new HtmlTableRow();

        AddPortCommonCells(htmlRow, null, styleNoBorder);
        AddPortEndpointCells(htmlRow, entry, styleNoBorder + "; " + styleExtraPadding);

        PortHtmlTable.Rows.Add(htmlRow);
    }

    private void AddPortLastTableRow(PortDataEntry entry)
    {
        var htmlRow = new HtmlTableRow();

        AddPortCommonCells(htmlRow, null);
        AddPortEndpointCells(htmlRow, entry, styleExtraPadding);
        AddCell(htmlRow, styleExtraPadding);
        PortHtmlTable.Rows.Add(htmlRow);
    }

    private void AddPortExpendControlTableRow(PortDataEntry entry, bool expended)
    {
        var htmlRow = new HtmlTableRow();

        AddPortCommonCells(htmlRow, null);
        AddCell(htmlRow, styleExtraPadding,
        expended
            ? String.Format("<a href=\"javaScript:LessReduce('{0}');\";>{1}</a>", entry.PortID, Resources.UDTWebContent.UDTWEBCODE_VB1_75)
            : String.Format("<a href=\"javaScript:MoreExapnd('{0}');\";>{1}</a>", entry.PortID, Resources.UDTWebContent.UDTWEBCODE_VB1_76));

        AddCell(htmlRow, styleExtraPadding);
        AddCell(htmlRow, styleExtraPadding);
        AddCell(htmlRow, styleExtraPadding);
        AddCell(htmlRow, styleExtraPadding);
        AddCell(htmlRow, styleExtraPadding);

        PortHtmlTable.Rows.Add(htmlRow);
        
        
    }

    private void AddPortCommonCells(HtmlTableRow htmlRow, PortDataEntry entry, string styleStr = "")
    {
        if (entry == null)
        {
            if (Profile.AllowNodeManagement || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {
                AddCell(htmlRow, styleStr);
            }

            AddCell(htmlRow, styleStr);
            AddCell(htmlRow, styleStr);
            AddCell(htmlRow, styleStr);

            return;
        }

        var statusImage = "";
        if (entry.AdministrativeStatus == 2)
            statusImage = "<img src=\"/Orion/UDT/Images/Status/icon_port_shutdown_dot.gif\" title=\""+ Resources.UDTWebContent.UDTWEBCODE_DF1_4+"\" alt=\"active port\" style=\"vertical-align:bottom;\" />";
        else
        {
            if (entry.OperationalStatus == 1)
                statusImage =
                    "<img src=\"/Orion/UDT/Images/Status/icon_port_active_dot.gif\" title=\"" + Resources.UDTWebContent.UDTWEBCODE_VB1_77 + "\" alt=\"active port\" style=\"vertical-align:bottom;\" />";
            else
                statusImage = "<img src=\"/Orion/UDT/Images/Status/icon_port_inactive_dot.gif\" title=\"" + Resources.UDTWebContent.UDTWEBCODE_VB1_78 + "\" alt=\"inactive port\" style=\"vertical-align:bottom;\" />";
        }

         var portTypeName = entry.Type.ToString(CultureInfo.InvariantCulture); // defautl value == type.Id
        try
        {
            portTypeName = InterfaceTypes.Where(t => t.Id == portTypeName).Select(t => t.Name).FirstOrDefault();
        }
        catch
        {
        }
		
		var iconName = "0";
		string interfaceImagePath = Server.MapPath("/NetPerfMon/images/Interfaces/") + entry.Type + ".gif";
        if (File.Exists(interfaceImagePath))
			iconName = entry.Type.ToString();
		
        var interfaceImage = string.Format(
                @"<img src=""/NetPerfMon/images/Interfaces/{0}.gif"" title=""{1}"" alt=""{1}"" style=""margin-top: 8px;"" />&nbsp;",
                iconName, portTypeName);
        
        if (Profile.AllowNodeManagement || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            var checkBox = "<input id =  \"" + entry.PortID + "\"" + "type = " + "\"" + "checkbox" + "\"" + " onclick=" +
                           "\"" + "javascript:SaveSelectedNodes();" + "\"" + " />";

            AddCell(htmlRow, styleStr, checkBox);
        }
        AddCell(htmlRow, styleStr, statusImage );

        AddCell(htmlRow, styleStr,  interfaceImage);

        // add port name
        AddCell(htmlRow, styleStr,
                string.Format(@"<a href=/Orion/UDT/PortDetails.aspx?NetObject=UP:{0} @tooltip="""""">{1}</a>",
                              entry.PortID.ToString(CultureInfo.InvariantCulture), entry.Name));


    }

    private void AddPortEndpointCells(HtmlTableRow htmlRow, PortDataEntry entry, string styleStr = "")
    {
        // User Icon
        AddCell(htmlRow, styleStr, entry.IsUserExist ? GetUserIcon() : "&nbsp;");

        // dns
        AddCell(htmlRow, styleStr,
                entry.Dns == ""
                    ? "&nbsp;"
                    : String.Format("<a href='/Orion/UDT/EndpointDetails.aspx?NetObject=UE-DNS:VAL={0}';>{0}</a>",
                                    entry.Dns));

        // ip
        AddCell(htmlRow, styleStr,
                entry.Ip == ""
                    ? "&nbsp;"
                    : String.Format("<a href='/Orion/UDT/EndpointDetails.aspx?NetObject=UE-IP:VAL={0}'>{0}</a>&nbsp;",
                                    entry.Ip));
        // Icon
        AddCell(htmlRow, styleStr,
            entry.Mac == string.Empty ? "&nbsp;" : VendorIcon(entry.Vendor,entry.VendorIcon));

        // mac
        AddCell(htmlRow, styleStr,
                entry.Mac == ""
                    ? "&nbsp;"
                    : String.Format("<a href='/Orion/UDT/EndpointDetails.aspx?NetObject=UE-MAC:VAL={0}';>{1}</a>",
                        Regex.Replace(entry.Mac, EscapeMacChars, string.Empty), entry.Mac));

        //vlan
        AddCell(htmlRow, styleStr,
            entry.VlanID <= 0 ? "&nbsp;" : entry.VlanID.ToString(CultureInfo.InvariantCulture));

        //Vrf Name
        //Addition of new column requires one more Extra cell to be added in AddPortExpendControlTableRow method.
        AddCell(htmlRow, styleStr,
            entry.vrfName == string.Empty ? "&nbsp;" : entry.vrfName);
    }
    private string VendorIcon(string vendor, string icon)
    {
       
        string imagePath = Server.MapPath(NetPerfMonImageVendorPath) + icon;
        if (icon!="")
        {
            if (File.Exists(imagePath))
                return IconHelper.HTMLVendorImage(icon);
            else
            {
                return IconHelper.HTMLVendorImage("Unknown.gif");
            }
        }
        return IconHelper.HTMLVendorImage("Unknown.gif"); 
    }

    private string GetUserIcon()
    {
        var icon = "USER";

        if (File.Exists(Server.MapPath(NetPerfMonImagePath) + "Object-User.png"))
        {
            icon = IconHelper.HtmlUserImage("Object-User.png");
        }

        return icon;
    }

    private static void AddCell(HtmlTableRow dstRow, string style, string text = "&nbsp;", string cssClass = "Property",
                                Action<HtmlTableCell> adjust = null)
    {
        var htmlCell = new HtmlTableCell();

        if (!string.IsNullOrEmpty(cssClass))
            htmlCell.Attributes.Add("Class", cssClass);

        if (!string.IsNullOrEmpty(style))
            htmlCell.Attributes.Add("Style", style);

        htmlCell.InnerHtml = text;

        if (adjust != null)
            adjust(htmlCell);

        dstRow.Cells.Add(htmlCell);
    }

    #endregion

    #region == Helper methods ==

    private void FillRouterListControl()
    {
        lstRouters.Items.Clear();
        lstRouters.Items.Insert(0, new ListItem(Resources.UDTWebContent.UDTWEBCODE_VB1_79, "0"));

        if (RouterList == null || RouterList.Rows.Count == 0)
            return;

        for (int indx = 0; indx < RouterList.Rows.Count; indx++)
        {
            var row = RouterList.Rows[indx];
            lstRouters.Items.Insert(indx + 1,
                                    new ListItem(row["RouterName"] + " (" + row["RouterIP"] + ")",
                                                 row["RouterNodeID"].ToString()));
        }
    }

    private static DataTable MakeDistinctAndOrderBy(DataTable data, string column)
    {
        string normalizedPortNameColumnName = "NameNormalized";
        if (column == "Name")
        {
            if (!data.Columns.Contains(normalizedPortNameColumnName))
                data.Columns.Add(new DataColumn(normalizedPortNameColumnName, typeof(string)));
            column = normalizedPortNameColumnName;
        }

        if (!data.AsEnumerable().Any())
            return data.Clone();

        return data.AsEnumerable().Select(row =>
        {
            DataRow newRow = data.NewRow();
            newRow["PortID"] = row["PortID"];
            newRow["PortType"] = row["PortType"];
            newRow["Name"] = row["Name"];

            if (column == normalizedPortNameColumnName)
                newRow[normalizedPortNameColumnName] = NormalizePortNameForSorting(row["Name"]);

            newRow["OperationalStatus"] = row["OperationalStatus"];
            newRow["VLANID"] = row["VLANID"];
            newRow["MACAddress"] = row["MACAddress"];
            newRow["IPAddress"] = row["IPAddress"];
            newRow["DNSName"] = row["DNSName"];
            newRow["VrfName"] = row["VrfName"];
            newRow["AdministrativeStatus"] = row["AdministrativeStatus"];
            newRow["VendorIcon"] = row["VendorIcon"];
            newRow["Vendor"] = row["Vendor"];
            newRow["IsUserExist"] = row["IsUserExist"];
            return newRow;
        }).Distinct(DataRowComparer.Default).OrderBy(r => r[column]).CopyToDataTable();
    }

    private static string NormalizePortNameForSorting(object value)
    {
        string strValue = (value == null || value == DBNull.Value) ? string.Empty : value.ToString();
        string[] strings = strValue.Split(new[] {'/'}, StringSplitOptions.RemoveEmptyEntries);
        if (strings.Length > 0)
        {
            var s = strings[0];

            int pos = 0;
            while (pos < s.Length && !char.IsDigit(s[pos]))
                pos++;

            if (pos < s.Length)
                strings[0] = s.Substring(0, pos) + s.Substring(pos, s.Length - pos).PadLeft(6, '0');

            for (int i = 1; i < strings.Length; i++)
                strings[i] = strings[i].PadLeft(4, '0');
        }

        return string.Join("/", strings);
    }

    private static IList<DataRow> OrderRowsBy(IList<DataRow> rows, string column)
    {
        // import into temp table
        var portTable = rows[0].Table.Clone();
        foreach (var row in rows)
        {
            var r = portTable.NewRow();
            r.ItemArray = row.ItemArray;
            portTable.Rows.Add(r);
        }

        var outputTable = portTable.Clone();

        // copy first only rows where order column value is not null
        foreach (var row in portTable.Rows.Cast<DataRow>().Where(r => r[column] != DBNull.Value))
        {
            outputTable.ImportRow(row);
        }

        // order them and copy into output table
        if (outputTable.Rows.Count > 1)
        {
            using (var view = new DataView(outputTable))
            {
                view.Sort = column;
                outputTable = view.ToTable();
            }
        }

        // insert into output table other rows
        foreach (var row in portTable.Rows.Cast<DataRow>().Where(r => r[column] == DBNull.Value))
        {
            outputTable.ImportRow(row);
        }

        return outputTable.Rows.Cast<DataRow>().ToList();
    }

    #endregion

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] {typeof (INodeProvider)}; }
    }

   
}
