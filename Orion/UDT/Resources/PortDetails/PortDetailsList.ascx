﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PortDetailsList.ascx.cs" Inherits="Orion_UDT_Resources_PortDetails_PortDetailsList" %>
<%@ Register TagPrefix="udt" TagName="ResourceSearcher" Src="~/Orion/UDT/Controls/ResourceSearcher.ascx" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
 <orion:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false" Version="3.4"/>
 <orion:Include ID="Include1" File="OrionMinReqs.js" Section="Top" runat="server" SpecificOrder="-1" />
 <link rel="stylesheet" type="text/css" href="/Orion/UDT/UDT.css" />

<script type="text/javascript">
    var hiddenValue = "";
    var selectedPorts = {};
    var RWCommunityDefinedNodes = "";
    var nodeId;
    DoQuery = function (query, succeeded) {
        Information.Query(query, function (result) {
            var table = [];
            for (var y = 0; y < result.Rows.length; ++y) {
                var row = result.Rows[y];
                var tableRow = {};
                for (var x = 0; x < result.Columns.length; ++x) {
                    tableRow[result.Columns[x]] = row[x];
                }
                table.push(tableRow);
            }
            succeeded({ Rows: table });
            $('#lastErrorMessage').hide();
        }, function (error) {
            if (error.get_message() == "Authentication failed.") {
                window.location.reload();
            } else {
            }
        });
    };

    function SaveSelectedNodes() {
        var isDemoServer = '<%=SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer%>';
        if (isDemoServer.toLowerCase() == 'true') {
            return;
        }

        if (RWCommunityDefinedNodes.value != "") return;
        var tbl = $('#<%=PortHtmlTable.ClientID%>');

            var hdn = $('#<%=hdnSelectedNodes.ClientID%>');
        var chkBoxes = tbl.find('input:checkbox');
        var selectedIds = "";
        var tempSelectedPorts = {};
        $(chkBoxes).each(function () {
            if (this.checked) {
                if ($.isNumeric(this.id)) {
                    if (selectedIds != "") {
                        selectedIds = selectedIds + "," + this.id;
                    }
                    else {

                        selectedIds = this.id;

                    }

                }
            }
        });
        EnableByID("Shutdownlink", false);
        EnableByID("Enablelink", false);
        if (selectedIds != "") {

            DoQuery("select portId as portId,Name as portName,AdministrativeStatus from orion.udt.port (nolock=true)  where portId in (" + selectedIds + ") ", function (result) {
                if (result.Rows.length > 0) {

                    for (var i = 0; i < result.Rows.length; i++) {
                        if (result.Rows[i]["AdministrativeStatus"] == 1) {
                            EnableByID("Shutdownlink", true);
                            EnableByID("Enablelink", false);
                        }
                        else {
                            EnableByID("Enablelink", true);
                            EnableByID("Shutdownlink", false);
                        }
                        if (tempSelectedPorts[result.Rows[i]["portId"]] == undefined)
                            tempSelectedPorts[result.Rows[i]["portId"]] = [];
                        tempSelectedPorts[result.Rows[i]["portId"]].push(result.Rows[i]["portName"]);
                    }
                }

            });



        }
        hdn.val(selectedIds);
        hiddenValue = selectedIds;
        selectedPorts = tempSelectedPorts;
    };


    function ShutdownEnablePort(e, shutdown) {
        if ($(e).parent().hasClass("Disabled"))
            return false;
        if (hiddenValue == "") return false;
        var selectedIds = new Array();
        selectedIds = hiddenValue.split(",");

        if (!shutdown || window.confirm(selectedIds.length == 1 ? "<%=Resources.UDTWebContent.UDTWEBCODE_DF1_5 %>" : "<%=Resources.UDTWebContent.UDTWEBCODE_DF1_6 %>")) {
            document.getElementById("<%=UpdateProgress1.ClientID%>").style.display = "block";
            var width = 800;
            var statusBox = $('<div></div>').addClass('StatusDialog').width(width);
            var statusContent = $('<div></div>').appendTo(statusBox);
            var statusItems = $('<ul></ul>').addClass('InterfacesText').appendTo(statusContent);
            var closeButton = $('<div></div>').addClass('CloseButtonBox').appendTo(statusBox);

            for (a in selectedIds) {
                selectedIds[a] = parseInt(selectedIds[a]);
            }
            UdtPortManagement.AdminstrativelyShutDown(nodeId.value, selectedIds, shutdown, function () {

                window.location.reload();

            }, function (error) {
                document.getElementById("<%=UpdateProgress1.ClientID%>").style.display = "none";
                $(SW.Core.Widgets.Button('Close', { cls: 'CloseButton' })).appendTo(closeButton).click(function () { statusBox.dialog('close').remove(); });
                $('body').append(statusBox);
                var statusBlock;
                for (var item in selectedPorts) {
                    var caption = selectedPorts[item][0];
                    statusBlock = $('<li></li>').appendTo(statusItems).text(caption);
                }
                statusBlock.append(' - ' + '<font color="red">' + error.get_message() + '</font>');
                statusBox.dialog({
                    width: width, minHeight: 0, modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: shutdown ? "<%=Resources.UDTWebContent.UDTWEBCODE_DF1_1 %>" : "<%=Resources.UDTWebContent.UDTWEBCODE_DF1_2 %>"
                });
            });
            return true;
        }
        return false;
    };


</script>

 <script type="text/javascript">
     var prm = Sys.WebForms.PageRequestManager.getInstance();
     prm.add_initializeRequest(InitializeRequest);
     prm.add_endRequest(EndRequest);



     var postBackElement;
     function pageLoad(sender, e) {
         DeSelectAll(this);
     }
     function InitializeRequest(sender, args) {
     }
     function EndRequest(sender, args) {
         EnableByID("Shutdownlink", false);
         EnableByID("Enablelink", false);
     }


    </script>

<orion:resourceWrapper runat="server" ID="Wrapper">
<Content>
 <style type="text/css">
        #adminContent .dialogBody table td { padding-top: 5px; padding-left: 10px; }
        #delDialog .dialogBody table td { padding-right: 10px; }
        #delPortsDialog .dialogBody table td { padding-right: 10px; }
        #monitoringDialog .dialogBody table td { padding-right: 10px; }
        #overLicenseLimitDialog .dialogBody table td { padding-right: 10px; }
        #rwCommunityValidationDialog .dialogBody table td { padding-right: 10px; }
        #addDialog td.x-btn-center, #delDialog td.x-btn-center, #updateDialog td.x-btn-center { text-align: center !important; }
        #addDialog td, #delDialog td, #delPortsDialog td, #updateDialog td { padding-right: 0; padding-bottom: 0; }
        #adminContent .dialogBody ul { margin-left: 20px; list-style-type: square; }
        #adminContent .dialogBody ul { margin-left: 20px; list-style-type: square; }
        
        
        
ul.NodeManagementMenu li.Disabled, .NodeManagementMenu li.Disabled:hover {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-image: none;
    border-style: none solid none none;
    border-width: medium 1px 0 0;
}
.NodeManagementMenu li.Disabled, .NodeManagementMenu li.Disabled a, .NodeManagementMenu li.Disabled a:hover, .NodeManagementMenu li.Disabled img {
    color: #A09D9D;
    cursor: default;
}
.NodeManagementMenu li {
    border-right: 1px solid white;
    float: left;
    padding: 5px 10px 5px 5px;
    position: relative;
}

 .List  td.disabled
{
    background-color: #f2f2f2;
    background: #f2f2f2;
    border: 0;
    border-right: solid 1px #e7e7e7;
    border-top: none;
}
.Test td.disabledAnchor a{
       cursor: default;
       color:Gray;
       
}
a.disabled
{
   color:Grey; text-decoration:none;
}
</style>
</Content>
   
    <Content>
        <asp:UpdatePanel ID="updatePanel" runat="server" style="width: 100%">
            <ContentTemplate>
             <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" >
        <Services>
            <asp:ServiceReference path="~/Orion/UDT/Services/UdtPortManagement.asmx" />
        </Services>
    </asp:ScriptManagerProxy>
               <asp:HiddenField runat="server"  ID="expandedPortsString"  />
                <table style="height:40px; border-collapse: collapse" >
                    <tr>
                      <td style="border-bottom: none !important; white-space: nowrap;text-align: left">
                           <udt:ResourceSearcher runat="server" ID="ResourceSearcher" Visible="False" Hidden="False"/>
                        </td>   
                        <td style="width:100px; border-bottom: none !important; white-space: nowrap;text-align:left">
                            <asp:UpdateProgress runat="server" ID="UpdateProgress1" DynamicLayout="true" DisplayAfter="0" AssociatedUpdatePanelID="updatePanel">
                                <ProgressTemplate>
                                    <span style="padding-left:2px; vertical-align:middle;white-space: nowrap;"><img alt="In Progress..." src="/Orion/images/loading_gen_small.gif" />
                                        <span style="font-size:8pt; vertical-align:middle;"><asp:Literal ID="Literal1" runat="server" Text="In Progress.." /></span>
                                    </span>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </td>
                         <% if (Profile.AllowNodeManagement || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
                                    { %>
                        <td style="width:1px; border:0px; white-space: nowrap;"><asp:Image ID="Image1" runat="server"  ImageUrl ="~/Orion/UDT/Ports/images/icons/icon_shutdown.gif" /></td>
                        <td style="width:60px; border:0px; white-space: nowrap;">
                        <ul class="NodeManagementMenu">
                                    <li>
                        <a id="Shutdownlink" onclick="return ShutdownEnablePort(Shutdownlink,true)" style="cursor:pointer;"><%=Resources.UDTWebContent.UDTWEBDATA_DF1_4 %></a>
                        </li></ul>
                        </td>
                        <td style="width:1px; border:0px; white-space: nowrap;"><asp:Image ID="Turnon" runat="server" ImageUrl ="~/Orion/UDT/Ports/images/icons/icon_enable.gif" /></td>
                        <td style="width:60px; border:0px;white-space: nowrap;">
                        <ul  class="NodeManagementMenu">
                                    <li>
                        <a id="Enablelink" onclick="return ShutdownEnablePort(Enablelink,false)" style="cursor:pointer;"><%=Resources.UDTWebContent.UDTWEBDATA_DF1_5 %></a>
                        </li></ul>
                        </td>
                         <% } %>
                        <td style="width:60px; border:0px; white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_DF1_23%></td>
                        <td style="width:60px; border:0px; white-space: nowrap;">
                            <asp:DropDownList runat="server" id="portStatus" AutoPostBack="true">
                                <asp:ListItem Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_208 %>" Value="-1" />
                                <asp:ListItem Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_209 %>" Value="1" Selected="true" />
                                <asp:ListItem Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_210 %>" Value="0" />
                            </asp:DropDownList>
                        </td>
                        <td style=" width:80px; border:0px; white-space: nowrap;">&nbsp;<%= Resources.UDTWebContent.UDTWEBDATA_VB1_211%></td>
                        <td style="width:60px; border:0px;">
                            <asp:DropDownList runat="server" id="lstRouters" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                
                 <% if (Profile.AllowNodeManagement || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
                    {%>
                     <table runat="server" id="PortHtmlTable2" border="0" cellpadding="2" cellspacing="0" width="100 %" visible="true" class="NeedsZebraStripes">
                     <tr>
                     <td class="ReportHeader" width ="2%" ><input id ="cb1"type ="checkbox" onclick="SelectAll(this)"/></td>
                       <td class="ReportHeader" width="6%" >&nbsp;</td>
                       <td class="ReportHeader" width="6%" >&nbsp;</td>
                       <td class="ReportHeader" width="10%" ><%= Resources.UDTWebContent.UDTWEBDATA_VB1_212 %></td>
                       <td class="ReportHeader" width="4%" >&nbsp;</td>
                       <td class="ReportHeader" width="24%" ><%= Resources.UDTWebContent.UDTWEBDATA_VB1_213%></td>
                       <td class="ReportHeader" width="21%" ><%= Resources.UDTWebContent.UDTWEBDATA_VB1_214%></td>
                       <td class="ReportHeader" width="12%" >&nbsp;</td>
                       <td class="ReportHeader" width="14%" ><%= Resources.UDTWebContent.UDTWEBDATA_VB1_215%></td>
                       <td class="ReportHeader" width="7%" ><%= Resources.UDTWebContent.UDTWEBDATA_AK1_205%></td>
                       <td class="ReportHeader" width="10%"><%= Resources.UDTWebContent.UDTWEBDATA_GM1_232%></td>
                    </tr>
                     </table>
                 <% } else {%>
                     <table runat="server" id= "PortHtmlTable1" border="0" cellpadding="2" cellspacing="0" width="100%" visible="true"  class="NeedsZebraStripes" >
                    <tr>
                       <td class="ReportHeader" width="4%" >&nbsp;</td>
                       <td class="ReportHeader" width="4%" >&nbsp;</td>
                       <td class="ReportHeader" width="8%" ><%= Resources.UDTWebContent.UDTWEBDATA_VB1_212 %></td>
                       <td class="ReportHeader" width="4%" >&nbsp;</td>
                       <td class="ReportHeader" width="28%" ><%= Resources.UDTWebContent.UDTWEBDATA_VB1_213%></td>
                       <td class="ReportHeader" width="23%" ><%= Resources.UDTWebContent.UDTWEBDATA_VB1_214%></td>
                       <td class="ReportHeader" width="12%" >&nbsp;</td>
                       <td class="ReportHeader" width="16%" ><%= Resources.UDTWebContent.UDTWEBDATA_VB1_215%></td>
                       <td class="ReportHeader" width="7%" ><%= Resources.UDTWebContent.UDTWEBDATA_AK1_205%></td>
                       <td class="ReportHeader" width="10%"><%= Resources.UDTWebContent.UDTWEBDATA_GM1_232%></td>
                    </tr>
               
                </table>
                 <% } %>
            </ContentTemplate>
        </asp:UpdatePanel> 
         
    </Content>
</orion:resourceWrapper>

<asp:HiddenField ID="hdnSelectedNodes" runat="server" />
<asp:HiddenField ID="hdnRWCommunityDefined" runat="server" />
<asp:HiddenField ID="hdnnodeId" runat="server" />
    <script type="text/javascript">
        EnableByID("Shutdownlink", false);
        EnableByID("Enablelink", false);
        function EnableByID(id, enabled) {
            if (enabled) {
                $("#" + id).parent().removeClass("Disabled");
            }
            else {
                $("#" + id).parent().addClass("Disabled");
            }
        };
        $(".NodeManagementMenu li").hover(function () {
            $(this).addClass("over");
        }, function () {
            $(this).removeClass("over");
        });

        RWCommunityDefinedNodes = document.getElementById("<%:hdnRWCommunityDefined.ClientID%>");
        if (RWCommunityDefinedNodes.value != "") {

            $('#Shutdownlink').attr('title', '<%=Resources.UDTWebContent.UDTWEBDATA_DF1_22 %>');
            $('#Enablelink').attr('title', '<%=Resources.UDTWebContent.UDTWEBDATA_DF1_22 %>');
        }
        else {
            $('#Shutdownlink').attr('title', '');
            $('#Enablelink').attr('title', '');
        }
        nodeId = document.getElementById("<%=hdnnodeId.ClientID %>");
        function GoSearchPageHref(q, col, idx) {
            var qs = "?q=" + encodeURIComponent(q);
            var qsCols = "&cols=" + encodeURIComponent(col);
            var url = "/Orion/UDT/SearchResults.aspx";
            ORION.prefix = "UDT_SearchResults_";
            ORION.Prefs.save('SearchItemList', idx);
            document.location.href = url + qs + qsCols;
        };
    </script>
   
    <script type="text/javascript">
        function MoreExapnd(portId) {
            var TB = this.document.getElementById("<%=expandedPortsString.ClientID%>");
                TB.value = TB.value + "," + portId;
                 <%= PostBackStr %>
            };

        function LessReduce(portId) {
            var TB = this.document.getElementById("<%=expandedPortsString.ClientID%>");
                var searchText = "," + portId;
                var tempText = TB.value;
                TB.value = tempText.replace(searchText, "");
                 <%= PostBackStr %>
            };
    </script>
   
    <script type="text/javascript">
        function SelectAll(checkBox) {
            var c = checkBox.checked;
            $('input:checkbox').each(function () {
                this.checked = c;
            });
            SaveSelectedNodes();

        }
        function DeSelectAll(checkBox) {
            $('input:checkbox').each(function () {
                this.checked = false;
            });
        }
    </script>
  
