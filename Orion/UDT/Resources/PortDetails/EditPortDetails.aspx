﻿<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true" CodeFile="EditPortDetails.aspx.cs" Inherits="Orion_UDT_Resources_PortDetails_EditPortDetails" %>
<%@ Import Namespace="SolarWinds.Orion.Web" %>
<%@ Import Namespace="SolarWinds.UDT.Web.Helpers" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" Assembly="OrionWeb" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= String.Format(Resources.UDTWebContent.UDTWEBDATA_VB1_135, CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Resource.Title, Request["NetObject"].UrlEncode()))) %></h1>
       
    <div>
        <b><%= Resources.UDTWebContent.UDTWEBDATA_VB1_203 %></b><br />
        <asp:TextBox runat="server" ID="maxIpV4Count" Columns="5"/>
        <br /><br/>

        <b><%= Resources.UDTWebContent.UDTWEBDATA_VB1_204 %></b><br />
        <asp:TextBox runat="server" ID="maxIpV6Count" Columns="5"/>
        <br /><br/>
        
        <b><%= Resources.UDTWebContent.UDTWEBDATA_VB1_205%></b><br />
        <asp:TextBox runat="server" ID="maxMacCount" Columns="5"/>
        <br /><br/>
        <orion:LocalizableButton runat="server" ID="btnSubmit" OnClick="SubmitClick" LocalizedText="Submit" DisplayType="Primary"/>                    
        <br /><br />

    </div>
</asp:Content>

