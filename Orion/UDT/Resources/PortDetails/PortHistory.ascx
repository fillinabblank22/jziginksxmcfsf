﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PortHistory.ascx.cs" Inherits="Orion_UDT_Resources_PortHistory" %>

<%@ Register TagPrefix="udt" TagName="ResourceSearcher" Src="~/Orion/UDT/Controls/ResourceSearcher.ascx" %>
<%@ Register TagPrefix="udt" TagName="ResourcePager" Src="~/Orion/UDT/Controls/ResourcePager.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>    
        <asp:UpdatePanel ID="ResourceUpdatePanel" runat="server" UpdateMode="Always" >
            <ContentTemplate> 
                <table style="width: 100%; border-collapse: collapse">
                    <tr>
                        <td>&nbsp;</td>
                        <td align = "right" width="200px">
                            <udt:ResourceSearcher runat="server" ID="ResourceSearcher" Visible="true" Hidden="False"/>            
                        </td>                        
                    </tr>
                </table>
            
                <table runat="server" id="PortHistory" border="0" cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes">
                    <tr>
                        <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_136%>&nbsp;<asp:ImageButton runat="server" style="vertical-align:middle;" Visible="true"  ID="SortAscButton" ImageUrl="/Orion/UDT/images/NavigationItems/sortable_arrow_down.gif" OnClick="SortAsc_Click" /><asp:ImageButton style="vertical-align:middle;" runat="server" ID="SortDescButton" ImageUrl="/Orion/UDT/images/NavigationItems/sortable_arrow_up.gif" OnClick="SortDesc_Click" CausesValidation="false" Visible="false" /></td>
                        <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_201%></td>                
                        <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_199%></td>                
                        <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_206 %></td>
                        <td class="ReportHeader" runat ="server" colspan="1" ID ="IPAMSubnetTd"><%= Resources.UDTWebContent.UDTWEBDATA_GK_6 %></td>
                    </tr>
                </table>
                <udt:ResourcePager runat="server" ID="ResourcePager" Visible="True" Hidden="False" />
                <orion:ExceptionWarning ID="ExceptionWarning" Visible="false" Text="" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel> 
	</Content>
</orion:resourceWrapper>
