﻿<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true" CodeFile="EditPortHistoryDetails.aspx.cs" Inherits="Orion_UDT_Resources_PortDetails_EditPortHistoryDetails" %>
<%@ Import Namespace="SolarWinds.Orion.Web" %>
<%@ Import Namespace="SolarWinds.UDT.Web.Helpers" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" Assembly="OrionWeb" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= String.Format(Resources.UDTWebContent.UDTWEBDATA_VB1_135, CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Resource.Title, Request["NetObject"].UrlEncode()))) %></h1>
       
    <div>
        <b><%= Resources.UDTWebContent.UDTWEBDATA_VB1_139 %></b><br/>
        <asp:DropDownList runat="server" ID="Period" />
        <br/>
        <br/>

        <b><%= Resources.UDTWebContent.UDTWEBDATA_AK1_189%></b><br/>
        <asp:TextBox runat="server" ID="maxRowsCount" Columns="5" />
        <br/>
        <br/>

        <orion:LocalizableButton runat="server" ID="btnSubmit" OnClick="SubmitClick" LocalizedText="Submit"
            DisplayType="Primary" />
    </div>
</asp:Content>

