﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PortDetails.ascx.cs" Inherits="Orion_UDT_Resources_PortDetails" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
 <orion:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false" Version="3.4"/>
 <orion:Include ID="Include1" runat="server" File="CommonViewStyles.css"/>
<script type="text/javascript">
    var RWCommunityNotDefinedNodes = "";
   
    function ShutdownEnablePort(e,shutdown) {
         if ($(e).parent().hasClass("Disabled"))
            return false;
            if (!shutdown || window.confirm("<%=Resources.UDTWebContent.UDTWEBCODE_DF1_5 %>")) {
           document.getElementById("<%=UpdateProgress1.ClientID%>").style.display = "block";
        var portCaption = document.getElementById("<%:portCaptionHidden.ClientID %>");
            var portIds = [];
            portIds.push( <%=UDTPort.Id%> );
            var width = 800;
            var statusBox = $('<div></div>').addClass('StatusDialog').width(width);
            var statusContent = $('<div></div>').appendTo(statusBox);
            var statusItems = $('<ul></ul>').addClass('InterfacesText').appendTo(statusContent);
            var closeButton = $('<div></div>').addClass('CloseButtonBox').appendTo(statusBox);
            
            UdtPortManagement.AdminstrativelyShutDown( <%=UDTPort.NodeID %> , portIds, shutdown, function() {

                window.location.reload();

            }, function(error) {
                document.getElementById("<%=UpdateProgress1.ClientID%>").style.display = "none";
                $(SW.Core.Widgets.Button('Close', { cls: 'CloseButton' })).appendTo(closeButton).click(function() { statusBox.dialog('close').remove(); });
                var statusBlock = $('<li></li>').appendTo(statusItems).text(portCaption.value);
                statusBlock.append(' - ' + '<font color="red">' + error.get_message() + '</font>');
                
                statusBox.dialog({
                    width: width, minHeight: 0, modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: shutdown ? "<%=Resources.UDTWebContent.UDTWEBCODE_DF1_1 %>" : "<%=Resources.UDTWebContent.UDTWEBCODE_DF1_2 %>"
                });
                
            });
            return true;
        }
        return false;
    };
    </script>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(InitializeRequest);
        prm.add_endRequest(EndRequest);
        var postBackElement;
        function InitializeRequest(sender, args) {
        }
        function EndRequest(sender, args) {
        }
        function pageLoad(sender, e) {
            EnableByID("ShutdownLinkButton", false);
            EnableByID("enableLinkButton", false);
            var isDemoServer = '<%=SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer%>';
            if (isDemoServer.toLowerCase() == 'true') {
                return;
            }
            RWCommunityNotDefinedNodes = document.getElementById("<%:hdnRWCommunityNotDefined.ClientID%>");
            if (RWCommunityNotDefinedNodes.value != "") {
                $('#ShutdownLinkButton').attr('title', '<%=Resources.UDTWebContent.UDTWEBDATA_DF1_22 %>' );
                $('#enableLinkButton').attr('title', '<%=Resources.UDTWebContent.UDTWEBDATA_DF1_22 %>' );
                return;
            }
            else {
                $('#ShutdownLinkButton').attr('title', '');
                $('#enableLinkButton').attr('title', '');
            }
            
            var adminStatus = document.getElementById("<%:administrativeStatus.ClientID%>").value;
            if (adminStatus == "Up") {
                EnableByID("ShutdownLinkButton", true);
                EnableByID("enableLinkButton", false);
            }
            else {
                EnableByID("enableLinkButton", true);
                EnableByID("ShutdownLinkButton", false);
            }

        }
    
    </script>
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <style type="text/css">
            .labelStyle { font-weight:bold; padding-left: 10px; }
            
        #adminContent .dialogBody table td { padding-top: 5px; padding-left: 10px; }
        #delDialog .dialogBody table td { padding-right: 10px; }
        #delPortsDialog .dialogBody table td { padding-right: 10px; }
        #monitoringDialog .dialogBody table td { padding-right: 10px; }
        #overLicenseLimitDialog .dialogBody table td { padding-right: 10px; }
        #rwCommunityValidationDialog .dialogBody table td { padding-right: 10px; }
        #addDialog td.x-btn-center, #delDialog td.x-btn-center, #updateDialog td.x-btn-center { text-align: center !important; }
        #addDialog td, #delDialog td, #delPortsDialog td, #updateDialog td { padding-right: 0; padding-bottom: 0; }
        #adminContent .dialogBody ul { margin-left: 20px; list-style-type: square; }
        #adminContent .dialogBody ul { margin-left: 20px; list-style-type: square; }
        
        ul.NodeManagementMenu li.Disabled, .NodeManagementMenu li.Disabled:hover {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-image: none;
    border-style: none solid none none;
    border-width: medium 1px 0 0;
}
.NodeManagementMenu li.Disabled, .NodeManagementMenu li.Disabled a, .NodeManagementMenu li.Disabled a:hover, .NodeManagementMenu li.Disabled img {
    color: #A09D9D;
    cursor: default;
}
.NodeManagementMenu li {
    border-right: 1px solid white;
    float: left;
    padding: 5px 10px 5px 5px;
    position: relative;
}
a.stylelinkColor {
    color: #336699;
 }
table.NeedsZebraStripes > tbody > tr > td {padding :0px !important} 
table.NeedsZebraStripes > tbody > tr > td.labelStyle {padding-left :9px !important}
 </style>
        <asp:UpdatePanel ID="MyPanel" runat="server">
            <ContentTemplate> 
            <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" >
        <Services>
            <asp:ServiceReference path="~/Orion/Services/Information.asmx" />
            <asp:ServiceReference path="~/Orion/UDT/Services/UdtPortManagement.asmx" />
            
        </Services>
    </asp:ScriptManagerProxy>
            <table id="Table1" border="0" cellpadding="2" cellspacing="0" width="100%"  visible="true" >
            <tr>
            <%--<td width="10" class="labelStyle">Management</td><td width="10%">&nbsp;</td><td width ="20%">--%>
            <td style="height:20px" width="10%" class="labelStyle"><%=Resources.UDTWebContent.UDTWEBDATA_DF1_21 %> &nbsp;&nbsp;</td>
           
           <td width ="5%" align="center" >
                            <asp:UpdateProgress runat="server" ID="UpdateProgress1" DynamicLayout="true" DisplayAfter="0" AssociatedUpdatePanelID="MyPanel">
                                <ProgressTemplate>
                                    <span style="padding-left:2px; vertical-align:middle;"><img alt="" src="/Orion/images/loading_gen_small.gif" />
                                        <span style="font-size:8pt; vertical-align:middle;"><asp:Literal ID="Literal1" runat="server" Text="" /></span>
                                    </span>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </td>


             <td width ="14%">
                           <asp:LinkButton ID="monitorLinkButton" runat="server"   onclick="monitorPortClick"> 
                                    <asp:Image ID="Image1" runat="server" ImageUrl ="~/Orion/UDT/Ports/images/icons/enable_monitoring.gif" />&nbsp;&nbsp;<%= Resources.UDTWebContent.UDTWEBDATA_AK1_117 %>
                          </asp:LinkButton></td>
                             <% if (Profile.AllowNodeManagement || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
                    {%>
                          <td width ="2%"><asp:Image ID="shutdown" runat="server" ImageUrl ="~/Orion/UDT/Ports/images/icons/icon_shutdown.gif" /></td>
                         <td width ="14%">
                        <ul class="NodeManagementMenu">
                                    <li>
                        <a id="ShutdownLinkButton" onclick="return ShutdownEnablePort(ShutdownLinkButton,true)" style="cursor:pointer;"><%=Resources.UDTWebContent.UDTWEBDATA_DF1_4 %></a>
                        </li></ul>
                        </td>


                         <td width ="2%"><asp:Image ID="Turnon" runat="server" ImageUrl ="~/Orion/UDT/Ports/images/icons/icon_enable.gif" /></td>

                         <td width ="18%">
                        <ul  class="NodeManagementMenu">
                                    <li>
                        <a id="enableLinkButton" onclick="return ShutdownEnablePort(enableLinkButton,false)" style="cursor:pointer;"><%=Resources.UDTWebContent.UDTWEBDATA_DF1_5 %></a>
                        </li></ul>
                        </td>
                          
                           <% } %>
                           <td width="41%">&nbsp;</td>
             </tr>
             </table>
           
                <table runat="server" id="PortNotMonitoredMessage" border="0" cellpadding="2" cellspacing="0" width="100%" visible="false" >
                    <tr>
                        <td><%=licenceMessage %></td>
                    </tr>
                </table>

                <table runat="server" id="PortNotMonitored" border="0" cellpadding="2" cellspacing="0" width="100%" visible="false" >
                    <tr>
                        <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_194 %></td>
                        <td><%=UDTPort.PortDescription %>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_195 %></td>
                        <td>
                            <asp:Label ID="PortNumberOnlyNotMonitored" runat="server"><%=UDTPort.Name %></asp:Label>
                            <asp:Label ID="PortNumberNPMLinkNotMonitored" runat="server"><a href="<%=NPMInterfaceDetailsLink %>"> <%=UDTPort.Name%></a></asp:Label>
                        </td>
                    </tr>
                </table>

                <%--the below tables should be shown only if the port is monitored--%>
                <table runat="server" id="PortDetails" border="0" cellpadding="2" cellspacing="0" width="100%" visible="false">
                    <tr>
                        <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_194 %></td><td><%=UDTPort.PortDescription%>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_195 %></td>
                        <td>
                            <asp:Label ID="PortNumberOnlyMonitored" runat="server"><%=UDTPort.Name%></asp:Label>
                            <asp:Label ID="PortNumberNPMLinkMonitored" runat="server"><a href="<%=NPMInterfaceDetailsLink %>"> <%=UDTPort.Name%></a></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_196%></td>
                        <td><%=PortStatus%>&nbsp;</td>
                    </tr>
                </table>
                                

                <asp:Repeater runat="server" id="ConflictIpV4Details">
                    <HeaderTemplate>
                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td width="200" class="labelStyle"><%# Container.ItemIndex == 0 ? Resources.UDTWebContent.UDTWEBDATA_GK_8 : "&nbsp;"%></td>
                            <td><%#FormatConflictIP(Eval("IPAddress").ToString(), Eval("ConflictTypeIcon").ToString())%></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table> 
                    </FooterTemplate>
                </asp:Repeater>

                <table runat="server" id="ConflictIpV4Links" border="0" cellpadding="2" cellspacing="0" width="100%" visible="false" >
                    <tr>
                        <td width="200" class="labelStyle">&nbsp;</td>
                        <td><asp:LinkButton ID="ConflictshowIpsV4Link" runat="server" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_180 %>" CSSClass="stylelinkColor" onclick="showConflictIpsV4Click" visible="false" /><asp:LinkButton ID="ConflicthideIpsV4Link" CssClass="stylelinkColor"  runat="server" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_181 %>" onclick="hideConflictIpsV4Click" visible="false" /></td>
                    </tr>
                </table>

                <asp:Repeater runat="server" id="IpV4Details">
                    <HeaderTemplate>
                        <table border="0" cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td width="200" class="labelStyle"><%# Container.ItemIndex == 0 ? Resources.UDTWebContent.UDTWEBDATA_VB1_197 : "&nbsp;"%></td>
                            <td><%#FormatIP(Eval("IPv4Address").ToString())%></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table> 
                    </FooterTemplate>
                </asp:Repeater>

                <table runat="server" id="IpV4Links" border="0" cellpadding="2" cellspacing="0" width="100%" visible="false" >
                    <tr>
                        <td width="200" class="labelStyle">&nbsp;</td>
                        <td><asp:LinkButton ID="showIpsV4Link" runat="server" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_180 %>"  CSSClass="stylelinkColor" onclick="showIpsV4Click" visible="false" /><asp:LinkButton ID="hideIpsV4Link"  CSSClass="stylelinkColor" runat="server" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_181 %>" onclick="hideIpsV4Click" visible="false" /></td>
                    </tr>
                </table>


                <asp:Repeater runat="server" id="IpV6Details">
                    <HeaderTemplate>
                        <table border="0" cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td width="200" class="labelStyle"><%# Container.ItemIndex == 0 ? Resources.UDTWebContent.UDTWEBDATA_VB1_198 : "&nbsp;"%></td>
                            <td><%#FormatIP(Eval("IPv6Address").ToString())%></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table> 
                    </FooterTemplate>
                </asp:Repeater>

                <table runat="server" id="IpV6Links" border="0" cellpadding="2" cellspacing="0" width="100%" visible="false" >
                    <tr>
                        <td width="200" class="labelStyle">&nbsp;</td>
                        <td><asp:LinkButton ID="showIpsV6Link" runat="server" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_180 %>" CSSClass="stylelinkColor" onclick="showIpsV6Click" visible="false" /><asp:LinkButton ID="hideIpsV6Link" CSSClass="stylelinkColor" runat="server" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_181 %>" onclick="hideIpsV6Click" visible="false" /></td>
                    </tr>
                </table>

                <asp:Repeater runat="server" id="MacDetails">
                    <HeaderTemplate>
                        <table border="0" cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td width="200" class="labelStyle"><%# Container.ItemIndex == 0 ? Resources.UDTWebContent.UDTWEBDATA_VB1_199 : "&nbsp;"%></td>
                            <td><%#FormatMAC(Eval("Mac").ToString())%></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table> 
                    </FooterTemplate>
                </asp:Repeater>

                
                <table runat="server" id="MacLinks" border="0" cellpadding="2" cellspacing="0" width="100%" visible="false" >
                    <tr>
                        <td width="200" class="labelStyle">&nbsp;</td>
                        <td><asp:LinkButton ID="showMacsLink" runat="server" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_180 %>" CSSClass="stylelinkColor" onclick="showMacsClick" visible="false" /><asp:LinkButton ID="hideMacsLink" CSSClass="stylelinkColor" runat="server" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_181 %>" onclick="hideMacsClick" visible="false" /></td>
                    </tr>
                </table>

                <asp:Repeater runat="server" id="VlanDetails">
                    <HeaderTemplate>
                        <table border="0" cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td width="200" class="labelStyle"><%# Container.ItemIndex == 0 ? Resources.UDTWebContent.UDTWEBDATA_AK1_205 : "&nbsp;"%></td>
                            <td><%#Eval("VLAN").ToString()%></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table> 
                    </FooterTemplate>
                </asp:Repeater>

                <table runat="server" id="VlanLinks" border="0" cellpadding="2" cellspacing="0" width="100%" visible="false" >
                    <tr>
                        <td width="200" class="labelStyle"><%=VlanMessage%></td>
                        <td><asp:LinkButton ID="showVlansLink" runat="server" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_200 %>" CSSClass="stylelinkColor" onclick="showVlansClick" visible="false" />
                            <asp:LinkButton ID="hideVlansLink" runat="server" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_181 %>" CSSClass="stylelinkColor" onclick="hideVlansClick" visible="false" />
                        </td>
                    </tr>
                </table>

                <table runat="server" id="SpeedDetails" border="0" cellpadding="2" cellspacing="0" width="100%" visible="false">
                    <tr>
                        <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_201%></td>
                        <td><%=UDTPort.Speed%></td>
                    </tr>
                </table> 

                <table runat="server" id="DuplexDetails" border="0" cellpadding="2" cellspacing="0" width="100%" visible="false">
                    <tr>
                        <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_202%></td>
                        <td><%=UDTPort.Duplex%></td>
                    </tr>
                </table> 
            </ContentTemplate>
        </asp:UpdatePanel> 
    </Content>
</orion:resourceWrapper>
<input type="hidden" id="hdnRWCommunityNotDefined" runat="server" />
<input type="hidden" id="portCaptionHidden" runat="server" />
<input type="hidden" id="administrativeStatus" runat="server" />

<script type="text/javascript">
    EnableByID("ShutdownLinkButton", false);
    EnableByID("enableLinkButton", false);
    function EnableByID(id, enabled) {
        if (enabled) {
            $("#" + id).parent().removeClass("Disabled");


        }
        else {
            $("#" + id).parent().addClass("Disabled");
        }
    };
    $(".NodeManagementMenu li").hover(function () {
        $(this).addClass("over");
    }, function () {
        $(this).removeClass("over");
    });
</script>
