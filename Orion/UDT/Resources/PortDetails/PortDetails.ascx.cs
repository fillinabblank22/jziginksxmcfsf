﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using SolarWinds.UDT.Common;
using SolarWinds.UDT.Common.DAL;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Web.Helpers;
using SolarWinds.UDT.Web.NetObjects;
using SolarWinds.UDT.Web.Providers;


public partial class Orion_UDT_Resources_PortDetails: SolarWinds.Orion.Web.UI.BaseResourceControl
{
    private static SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();

    private int _showNumberOfMacs = 4;
    private int _maxNumberOfMacs = 4;
    private int _showNumberOfIpV4s = 4;
    private int _maxNumberOfIpV4s = 4;
    private int _showNumberOfIpV6s = 4;
    private int _maxNumberOfIpV6s = 4;
    private bool _showTrunked = false;

    private string _vlanMessage = Resources.UDTWebContent.UDTWEBDATA_AK1_205;
    private string _licenceMessage;

    private int _npmInterfaceID = 0;
    private bool _allowInterfaces = false;
    private string _npmInterfaceDetailsLink = "/Orion/NPM/InterfaceDetails.aspx?NetObject=I:{0}";
    private UDTPortNetObject _udtPort = null;
    private bool _isIpamWithMinVersion;
    private DataTable _conflictIps;
    private string _imgPath = "<img src='{0}' border='0' title='{1}' alt='' style='vertical-align:middle' />";
    public UDTPortNetObject UDTPort
    {
        get
        {
            var udtPortProvider = GetInterfaceInstance<IUDTPortProvider>();
            if (udtPortProvider != null)
            {
                return udtPortProvider.UDTPortNetObject;
            }
            return _udtPort;
        }
        set { _udtPort = value; }
    }

    protected override String DefaultTitle
    {
        get { return Resources.UDTWebContent.UDTWEBCODE_VB1_68; }
    }

    private int ShowNumberOfMacs
    {
        get
        {
            if (ViewState["UDT_PortDetailsResource_ShowNumberOfMacs"] != null) _showNumberOfMacs = (int)ViewState["UDT_PortDetailsResource_ShowNumberOfMacs"];
            return _showNumberOfMacs;
        }
        set
        {
            _showNumberOfMacs = value;
            ViewState["UDT_PortDetailsResource_ShowNumberOfMacs"] = value;
        }
    }

    private int ShowNumberOfIpV4s
    {
        get
        {
            if (ViewState["UDT_PortDetailsResource_ShowNumberOfIpV4s"] != null) _showNumberOfIpV4s = (int)ViewState["UDT_PortDetailsResource_ShowNumberOfIpV4s"];
            return _showNumberOfIpV4s;
        }
        set
        {
            _showNumberOfIpV4s = value;
            ViewState["UDT_PortDetailsResource_ShowNumberOfIpV4s"] = value;
        }
    }

    private int ShowNumberOfIpV6s
    {
        get
        {
            if (ViewState["UDT_PortDetailsResource_ShowNumberOfIpV6s"] != null) _showNumberOfIpV6s = (int)ViewState["UDT_PortDetailsResource_ShowNumberOfIpV6s"];
            return _showNumberOfIpV6s;
        }
        set
        {
            _showNumberOfIpV6s = value;
            ViewState["UDT_PortDetailsResource_ShowNumberOfIpV6s"] = value;
        }
    }

    private bool ShowTrunked
    {
        get
        {
            if (ViewState["UDT_PortDetailsResource_ShowTrunked"] != null) _showTrunked = (bool)ViewState["UDT_PortDetailsResource_ShowTrunked"];
            return _showTrunked;
        }
        set
        {
            _showTrunked = value;
            ViewState["UDT_PortDetailsResource_ShowTrunked"] = value;
        }
    }

   
    public String PortStatus
    {
        get { return UDTPort.IsMissing ? Resources.UDTWebContent.UDTWEBCODE_VB1_69 : UDTPort.OperationalStatus; }
    }

    public String VlanMessage
    {
        get { return _vlanMessage; }
        set { _vlanMessage = value; }
    }

    public String licenceMessage
    {
        get { return _licenceMessage; }
        set { _licenceMessage = value; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionUDTPHPortDetails";
        }
    }

    public bool NPMInstalled
    {
        get { return _allowInterfaces; }
    }

    public bool NPMInterfaceFound
    {
        get { return (NPMInterfaceID != 0); }    
    }

    public int NPMInterfaceID
    {
        get { return _npmInterfaceID; }
        set { _npmInterfaceID = value; }
    }
    
    public void GetNPMInterfaceID()
    {
        int npmInterfaceID = 0;

        //Get NPM interface ID, if installed
        if (NPMInstalled)
        {
            try
            {
                //Get NPM Interface ID from PortIndex
                string sql = @"SELECT TOP 1 InterfaceID FROM Interfaces WHERE NodeID=@NodeID AND InterfaceIndex = @InterfaceIndex";
                using (SqlCommand cmd = SqlHelper.GetTextCommand(sql))
                {
                    cmd.Parameters.AddWithValue("@NodeID", UDTPort.NodeID);
                    cmd.Parameters.AddWithValue("@InterfaceIndex", UDTPort.PortIndex);
                    var intID = SqlHelperUDT.ExecuteScalar(cmd);
                    if (intID != null)
                    {
                        npmInterfaceID = (int)SqlHelperUDT.ExecuteScalar(cmd);
                    }
                    else
                    {
                        npmInterfaceID = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(
                    "An error occurred getting NPM InterfaceID from PortIndex {0} for NodeID {1}.  Details: {2}",
                    UDTPort.PortIndex, UDTPort.NodeID, ex.ToString());
                npmInterfaceID = 0;
            }

        }

        NPMInterfaceID = npmInterfaceID;
        
    }
    public void GetRWCommunity()
    {
        using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
        {
            int[] nodeIDs = new int[1];
            nodeIDs[0] = UDTPort.NodeID;
            var rwCommunityNotDefinedNodes = bl.GetRWCommunityNotDefinedNodes(nodeIDs);
            hdnRWCommunityNotDefined.Value = rwCommunityNotDefinedNodes.Trim();
        }

    }
    public string NPMInterfaceDetailsLink
    {
        get { return string.Format(_npmInterfaceDetailsLink, NPMInterfaceID); }
    }

    private void UpdateFromResourceProps()
    {
        if (!Int32.TryParse(this.Resource.Properties["MaxMacCount"], out _maxNumberOfMacs))
        {
            _maxNumberOfMacs = 5;
        }
        if (ShowNumberOfMacs != -1) ShowNumberOfMacs = _maxNumberOfMacs;
        
        if (!Int32.TryParse(this.Resource.Properties["MaxIpV4Count"], out _maxNumberOfIpV4s))
        {
            _maxNumberOfIpV4s = 5;
        }
        if (ShowNumberOfIpV4s != -1) ShowNumberOfIpV4s = _maxNumberOfIpV4s;

        if (!Int32.TryParse(this.Resource.Properties["MaxIpV6Count"], out _maxNumberOfIpV6s))
        {
            _maxNumberOfIpV6s = 5;
        }
        if (ShowNumberOfIpV6s != -1) ShowNumberOfIpV6s = _maxNumberOfIpV6s;
    }

    private void CheckForNPM()
    {
        //Check for NPM license installed
        try
        {
            _allowInterfaces = (new RemoteFeatureManager()).AllowInterfaces;
            GetNPMInterfaceID();
        }
        catch (Exception ex)
        {
            _log.ErrorFormat("Unable to determine if NPM is installed.  Details: {0}", ex.ToString());
            _allowInterfaces = false;
        }
    }

    public override string EditURL
    {
        get { return string.Format("/Orion/UDT/Resources/PortDetails/EditPortDetails.aspx?ResourceID={0}&ViewID={1}&NetObjectID={2}", this.Resource.ID, this.Resource.View.ViewID, HttpUtility.UrlDecode(UDTPort.NetObjectID)); }
    }

    protected override void OnInit(EventArgs e)
    {
       if  (UDTPort == null || string.IsNullOrEmpty(UDTPort.NetObjectID))
       {
           this.UDTPort = NetObjectFactory.Create("UP:15555", true) as UDTPortNetObject;  //we're doing this so the preview works on the cutomise page, page
       }

        base.OnInit(e);
    }

    protected override void OnLoad(EventArgs e)
    {

        //Update item counts from resource properties
        UpdateFromResourceProps();
        //GetRwCommunityString
        GetRWCommunity();

        _isIpamWithMinVersion = UDTIPAMIntegration.IsIPAMInstalled();
        if (_isIpamWithMinVersion)
        {
            if (UDTPort.IPv4List != null && UDTPort.IPv4List.Count>0)
            {
                _conflictIps = new DataTable();
                string portIps = string.Format("'{0}'", string.Join("','", UDTPort.IPv4List));
                _conflictIps = UDTPortDAL.GetConflictIps(portIps);
            }
        }
        
        //Update for NPM interface links (if NPM present)
        CheckForNPM();
        PortNumberNPMLinkNotMonitored.Visible = ((NPMInstalled) && (NPMInterfaceFound));
        PortNumberOnlyNotMonitored.Visible = !((NPMInstalled) && (NPMInterfaceFound));
        PortNumberNPMLinkMonitored.Visible = ((NPMInstalled) && (NPMInterfaceFound));
        PortNumberOnlyMonitored.Visible = !((NPMInstalled) && (NPMInterfaceFound));
        ToggleMonitoredState(UDTPort.IsMonitored);
        administrativeStatus.Value = UDTPort.AdministrativeStatus;
    }
   
    private void ToggleMonitoredState(bool isMonitored)
    {
        //Display monitored/unmonitored states
        if (!isMonitored)
        {
            //if it's not a monitored port show the empty table with the link to monitor it
            PortNotMonitored.Visible = true;
            monitorLinkButton.Visible = true;
            PortNotMonitoredMessage.Visible = true;
            // hide the other tables
            PortDetails.Visible = false;
            DuplexDetails.Visible = false;
            SpeedDetails.Visible = false;
        }
        else
        {
            //it is monitored so
            //hide the port not monitored table
            PortNotMonitored.Visible = false;
            PortNotMonitoredMessage.Visible = false;
            monitorLinkButton.Visible = false;
            //and show the other tables
            PortDetails.Visible = true;
            DuplexDetails.Visible = true;
            SpeedDetails.Visible = true;

            //bind repeaters
            PopulateIpV4Details();
            if (_isIpamWithMinVersion)
            {
                if (_conflictIps != null && _conflictIps.Rows.Count > 0)
                {
                    PopulateConflictIpV4Details();
                }
            }
            PopulateIpV6Details();
            PopulateMacs();
            PopulateVLANs();
        }
    }


    private void PopulateIpV4Details()
    {
        int recCount = ShowNumberOfIpV4s;
        if (recCount == -1) recCount = UDTPort.IPv4List.Count;

        IpV4Details.DataSource = UDTPort.IPv4List.Select(n => new { IPv4Address = n }).Take(recCount);
        IpV4Details.DataBind();

        if ((UDTPort.IPv4List.Count > recCount) && (UDTPort.IPv4List.Count != _maxNumberOfIpV4s))
        {
            showIpsV4Link.Visible = true;
            hideIpsV4Link.Visible = false;
            IpV4Links.Visible = true;
        }
        else if ((UDTPort.IPv4List.Count == recCount) && (UDTPort.IPv4List.Count != _maxNumberOfIpV4s))
        {
            showIpsV4Link.Visible = false;
            hideIpsV4Link.Visible = true;
            IpV4Links.Visible = true;
        }
        else
        {
            IpV4Links.Visible = false;
        }
    }

    private void PopulateConflictIpV4Details()
    {
        int recCount = ShowNumberOfIpV4s;
        if (recCount == -1) recCount = UDTPort.IPv4List.Count;

        ConflictIpV4Details.DataSource = _conflictIps;
        ConflictIpV4Details.DataBind();

        if ((_conflictIps.Rows.Count > recCount) && (_conflictIps.Rows.Count != _maxNumberOfIpV4s))
        {
            ConflictshowIpsV4Link.Visible = true;
            ConflicthideIpsV4Link.Visible = false;
            ConflictIpV4Links.Visible = true;
        }
        else if ((_conflictIps.Rows.Count == recCount) && (_conflictIps.Rows.Count != _maxNumberOfIpV4s))
        {
            ConflictshowIpsV4Link.Visible = false;
            ConflicthideIpsV4Link.Visible = true;
            ConflictIpV4Links.Visible = true;
        }
        else
        {
            ConflictIpV4Links.Visible = false;
        }
    }
    private void PopulateIpV6Details()
    {
        int recCount = ShowNumberOfIpV6s;
        if (recCount == -1) recCount = UDTPort.IPv6List.Count;

        IpV6Details.DataSource = UDTPort.IPv6List.Select(n => new { IPv6Address = n }).Take(recCount);
        IpV6Details.DataBind();

        if ((UDTPort.IPv6List.Count > recCount) && (UDTPort.IPv6List.Count != _maxNumberOfIpV6s))
        {
            showIpsV6Link.Visible = true;
            hideIpsV6Link.Visible = false;
            IpV6Links.Visible = true;
        }
        else if ((UDTPort.IPv6List.Count == recCount) && (UDTPort.IPv6List.Count != _maxNumberOfIpV6s))
        {
            showIpsV6Link.Visible = false;
            hideIpsV6Link.Visible = true;
            IpV6Links.Visible = true;
        }
        else
        {
            IpV6Links.Visible = false;
        }
    }

    private void PopulateMacs()
    {
        int recCount = ShowNumberOfMacs;
        if (recCount == -1) recCount = UDTPort.MacList.Count;

        MacDetails.DataSource = UDTPort.MacList.Select(n => new { Mac = n }).Take(recCount);
        MacDetails.DataBind();

        if ((UDTPort.MacList.Count > recCount) && (UDTPort.MacList.Count != _maxNumberOfMacs))
        {
            showMacsLink.Visible = true;
            hideMacsLink.Visible = false;
            MacLinks.Visible = true;
        }
        else if ((UDTPort.MacList.Count == recCount) && (UDTPort.MacList.Count != _maxNumberOfMacs))
        {
            showMacsLink.Visible = false;
            hideMacsLink.Visible = true;
            MacLinks.Visible = true;
        } else
        {
            MacLinks.Visible = false;
        }
    }

    internal class VlanInfo
    {
        private string _vlan;
        public string VLAN
        {
            get { return _vlan; }
            set { _vlan = value; }
        }
    }

    private void PopulateVLANs()
    {
        if (UDTPort.VLANs.Count == 0)
        {
            VlanDetails.DataSource = new List<VlanInfo>() { new VlanInfo() { VLAN = Resources.UDTWebContent.UDTWCODE_VB1_14 } };
            VlanLinks.Visible = false;
        }
        else if (ShowTrunked)
        {
            VlanMessage = "&nbsp;";
            IEnumerable<VlanInfo> vlanSource = UDTPort.VLANs.Select(n => new VlanInfo { VLAN = n });
            VlanDetails.DataSource = vlanSource;

            VlanLinks.Visible = true;
            showVlansLink.Visible = false;
            hideVlansLink.Visible = true;
        }
        else
        {
            if (UDTPort.VLANs.Count == 1)
            {
                VlanMessage = Resources.UDTWebContent.UDTWEBDATA_AK1_205;
                IEnumerable<VlanInfo> vlanSource = UDTPort.VLANs.Select(n => new VlanInfo { VLAN = n });
                VlanDetails.DataSource = vlanSource;
            }
            else
            {
                VlanMessage = Resources.UDTWebContent.UDTWEBDATA_AK1_205;
                VlanDetails.DataSource = null;
                VlanLinks.Visible = true;
                showVlansLink.Visible = true;
                hideVlansLink.Visible = false;
            }
        }

        VlanDetails.DataBind();
    }

    public string FormatMAC(string mac)
    {
        //return String.Format("<a href=\"javaScript:FindSearchHref('{0}','MAC Address', '3');\" >{0}</a>", formattedMac);
        return String.Format("<a href='/Orion/UDT/EndpointDetails.aspx?NetObject=UE-MAC:VAL={0}';>{0}</a>", mac);
    }

    public string FormatIP(string ip)
    {
       //return String.Format("<a href=\"javaScript:FindSearchHref('{0}','IP Address', '1');\";>{0}</a>", ip);
        return String.Format("<a href='/Orion/UDT/EndpointDetails.aspx?NetObject=UE-IP:VAL={0}';>{0}</a>", ip);

    }

    public string FormatConflictIP(string ip, string iconPath)
    {

        return String.Format("{1}&nbsp;<a href='/Orion/UDT/EndpointDetails.aspx?NetObject=UE-IP:VAL={0}';>{0}</a>", ip, string.Format(_imgPath, iconPath,""));

    }

    private string StringEmptyToNBSP(string cellContents)
    {
        if (String.IsNullOrEmpty(cellContents))
        {
            return "&nbsp;";
        }
        return cellContents;
    }

    protected void showMacsClick(object sender, EventArgs e )
    {
        ShowNumberOfMacs = -1;
        PopulateMacs();
    }

    protected void hideMacsClick(object sender, EventArgs e)
    {
        ShowNumberOfMacs = _maxNumberOfMacs;
        PopulateMacs();
    }

    protected void showIpsV4Click(object sender, EventArgs e)
    {
        ShowNumberOfIpV4s = -1;
        PopulateIpV4Details();
    }

    protected void hideIpsV4Click(object sender, EventArgs e)
    {
        ShowNumberOfIpV4s = _maxNumberOfIpV4s;
        PopulateIpV4Details();
    }
    protected void showConflictIpsV4Click(object sender, EventArgs e)
    {
        ShowNumberOfIpV4s = -1;
        PopulateConflictIpV4Details();
    }

    protected void hideConflictIpsV4Click(object sender, EventArgs e)
    {
        ShowNumberOfIpV4s = _maxNumberOfIpV4s;
        PopulateConflictIpV4Details();
    }

    protected void showIpsV6Click(object sender, EventArgs e)
    {
        ShowNumberOfIpV6s = -1;
        PopulateIpV6Details();
    }

    protected void hideIpsV6Click(object sender, EventArgs e)
    {
        ShowNumberOfIpV6s = _maxNumberOfIpV6s;
        PopulateIpV6Details();
    }

    protected void showVlansClick(object sender, EventArgs e)
    {
        ShowTrunked = true;
        PopulateVLANs();
    }

    protected void hideVlansClick(object sender, EventArgs e)
    {
        ShowTrunked = false;
        PopulateVLANs();
    }

    protected void monitorPortClick(object sender, EventArgs e)
    {
        if (!(SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer))
        {
            //Get max allowable monitored ports:
            var featureManager = new RemoteFeatureManager();
            int maxPorts = featureManager.GetMaxElementCount(SolarWinds.UDT.Common.WellKnownElementTypes.UDT.Ports);

            //Get current monitored port count:
            int portCount = UDTPortDAL.GetPortCount();

            if (portCount < maxPorts)
            {
                using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
                {
                    int[] id = new int[1] {UDTPort.Id};
                    bl.PollingSetPortMonitoredState(id, true);
                    licenceMessage = string.Empty;

                    //rebind port object now it has been updated
                    var nObject = Request["NetObject"];
                    this.UDTPort = NetObjectFactory.Create(nObject, true) as UDTPortNetObject;
                }
                // hide the unmonitored table and show the monitored
                ToggleMonitoredState(true);
            }
            else
            {
                licenceMessage = Resources.UDTWebContent.UDTWEBCODE_VB1_70;
            }
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IUDTPortProvider) }; }
    }

}



