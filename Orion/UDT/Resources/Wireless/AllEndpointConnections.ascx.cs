using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Web.Helpers;
using SolarWinds.UDT.Web.NetObjects;
using SolarWinds.UDT.Web.Providers;

public partial class Orion_UDT_Resources_Wireless_AllEndpointConnections : SolarWinds.Orion.Web.UI.BaseResourceControl
{
    private static readonly Log _log = new Log("WirelessEndpointConnections");

    protected string TimePeriod = Resources.UDTWebContent.UDTWCODE_VB1_51;

    protected override string DefaultTitle { get { return Resources.UDTWebContent.UDTWCODE_VB1_52; } }

    public override string HelpLinkFragment { get { return "OrionUDTPHAllEndpointConnections"; } }
    
    public override String SubTitle { get { return Periods.GetLocalizedPeriod(TimePeriod); } }

    public UDTNetObject NetObject { get; set; }

    public int DefaultPageSize
    {
        get
        {
            int pageSize;
            if (!Int32.TryParse(Resource.Properties["MaxRowsCount"], out pageSize))
                pageSize = 10;
            return pageSize;
        }
    }

    public string DataCacheEnabled { get { return UDTSettingDAL.CacheResourceData ? "true" : "false"; } }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ResourceSearcher.ResourceId = Resource.ID;
        ResourcePager.ResourceId = Resource.ID;
        //Resource.Properties["Period"] is come as NULL sometimes so it overwrites the default TimePeriod value;
        if (Resource.Properties["Period"] != null && Resource.Properties["Period"].ToString() != "")
            TimePeriod = Resource.Properties["Period"];

        NetObject = new UDTNetObject(Request.QueryString["NetObject"]);
    }

    public bool ApMode
    {
        get
        {
            //default to AccessPoint mode.  (This resource operates in two distinct modes, for AP and SSID view)
            if (string.IsNullOrEmpty(WirelessObject) || string.IsNullOrEmpty(NetObject.NetObjectSubType))
                return true;

            return String.Compare(NetObject.NetObjectSubType, "ssid", StringComparison.OrdinalIgnoreCase) != 0;
        }
    }

    public string WirelessObject
    {
        get
        {
            if (NetObject.NetObjectType != "UW-AP" && NetObject.NetObjectType != "UW-SSID")
                return string.Empty;

            return NetObject.Value;
        }
    }

    public string WirelessObjectEx { get { return HtmlHelper.AssembleWirelessObjectEx(WirelessObject, Resource.ID); } } 

    public override string EditURL
    {
        get { return string.Format("/Orion/UDT/Resources/Wireless/EditAllEndpointConnections.aspx?ResourceID={0}&ViewID={1}&NetObjectID={2}", this.Resource.ID, this.Resource.View.ViewID, HttpUtility.UrlDecode(Request.QueryString["NetObject"])); }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IUDTWirelessCommonProvider) }; }
    }

}



