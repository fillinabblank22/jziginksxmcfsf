<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true" CodeFile="EditAllEndpointConnections.aspx.cs" Inherits="Orion_UDT_Resources_Wireless_EditAllEndpointConnections" %>
<%@ Import Namespace="SolarWinds.Orion.Web" %>
<%@ Import Namespace="SolarWinds.UDT.Web.Helpers" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" Assembly="OrionWeb" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= string.Format(Resources.UDTWebContent.UDTWEBDATA_VB1_135, CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Resource.Title, Request["NetObject"].UrlEncode()))) %></h1>
    <div>
        <orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="false" ShowSubTitleHintMessage="true" />
        <br />
        <b><%= Resources.UDTWebContent.UDTWEBDATA_VB1_139 %></b><br />
        <asp:DropDownList runat="server" ID="Period" /><br /><br />
        <b><%= Resources.UDTWebContent.UDTWEBDATA_VB1_140 %></b><br />
        <asp:TextBox runat="server" ID="maxRowsCount" Columns="5"/>
        <br /><br />
        <orion:LocalizableButton runat="server" ID="btnSubmit" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitClick"/>                   
        <br /><br />
    </div>
</asp:Content>

