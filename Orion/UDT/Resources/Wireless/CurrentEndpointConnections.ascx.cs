﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Web.Helpers;
using SolarWinds.UDT.Web.NetObjects;
using SolarWinds.UDT.Web.Providers;

public partial class Orion_UDT_Resources_Wireless_CurrentEndpointConnections : SolarWinds.Orion.Web.UI.BaseResourceControl
{
    private static readonly Log _log = new Log("WirelessCurrentEndpointConnections");

    protected override string DefaultTitle { get { return Resources.UDTWebContent.UDTWEBCODE_VB1_53; } }

    public override string HelpLinkFragment { get { return "OrionUDTPHCurrentEndpointConnections"; } }

    public override String SubTitle { get { return this.Resource.Properties["SubTitle"]; } }

    public int DefaultPageSize
    {
        get
        {
            int pageSize;
            if (!Int32.TryParse(Resource.Properties["MaxRowsCount"], out pageSize))
                pageSize = 10;
            return pageSize;
        }
    }

    public UDTNetObject NetObject { get; set; }

    public string DataCacheEnabled { get { return UDTSettingDAL.CacheResourceData ? "true" : "false"; } }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ResourceSearcher.ResourceId = Resource.ID;
        ResourcePager.ResourceId = Resource.ID;

        NetObject = new UDTNetObject(Request.QueryString["NetObject"]);
    }

    public bool ApMode
    {
        get
        {
            //default to AccessPoint mode.  (This resource operates in two distinct modes, for AP and SSID view)
            if (string.IsNullOrEmpty(WirelessObject) || string.IsNullOrEmpty(NetObject.NetObjectSubType))
                return true;

            return String.Compare(NetObject.NetObjectSubType, "ssid", StringComparison.OrdinalIgnoreCase) != 0;
        }
    }

    public string WirelessObject
    {
        get
        {
            if (NetObject.NetObjectType != "UW-AP" && NetObject.NetObjectType != "UW-SSID")
                return string.Empty;

            return NetObject.Value;
        }
    }

    public string WirelessObjectEx { get { return HtmlHelper.AssembleWirelessObjectEx(WirelessObject, Resource.ID); } } 

    public override string EditURL
    {
        get { return string.Format("/Orion/UDT/Resources/Wireless/EditCurrentEndpointConnections.aspx?ResourceID={0}&ViewID={1}&NetObjectID={2}", this.Resource.ID, this.Resource.View.ViewID, HttpUtility.UrlDecode(Request.QueryString["NetObject"])); }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IUDTWirelessCommonProvider) }; }
    }

}



