﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CurrentEndpointConnections.ascx.cs" Inherits="Orion_UDT_Resources_Wireless_CurrentEndpointConnections" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Register TagPrefix="udt" TagName="ResourceSearcher" Src="~/Orion/UDT/Controls/ResourceSearcherHtml.ascx" %>
<%@ Register TagPrefix="udt" TagName="ResourcePager" Src="~/Orion/UDT/Controls/ResourcePagerHtml.ascx" %>

<orion:Include ID="Include1" runat="server" File="Charts/js/json2.js" /> <%-- This can go away once we the Core version is Galaga. At that point, json2.js is on every page. --%>
<orion:include ID="Include2" runat="server" File="UDT/js/PageableResource.js" />

<orion:resourceWrapper runat="server" ID="ResourceWrapper1">
	<Content>
	    <div id="wirelessCurrentEndpointConnectionsContainer<%=Resource.ID %>"> 
            <table style="width: 100%; border-collapse: collapse">
                <tr>
                    <td>&nbsp;</td>
                    <td align = "right" width="200px">
                        <udt:ResourceSearcher runat="server" ID="ResourceSearcher" Visible="true" Hidden="False"/>            
                    </td>                        
                </tr>
            </table>
            
            <table id="PortHistory" border="0" cellpadding="2" cellspacing="0" width="100%"  class="NeedsZebraStripes">
                <tbody class="headers">
                    <tr>
                        <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_137%></td>
                        <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_142 %></td>
                        <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_138 %></td>    
                        <td id="SsidIcon" class="ReportHeader" colspan="1"></td>
                        <td id="SsidName" class="ReportHeader" colspan="1"></td>
                    </tr>
                </tbody>
                <tbody class="results"/>
            </table>
            <udt:ResourcePager runat="server" ID="ResourcePager" Visible="True" Hidden="False" />

        </div>
        
        <script type="text/javascript">
            $(function () {
                
                //Check WirelessObject first, pointless but otherwise apMode ain't set
                var wirelessObject = '<%= WirelessObject %>';
                if (<%= ApMode.ToString(CultureInfo.InvariantCulture).ToLowerInvariant() %>) {
                    $('#SsidName').html('<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEBDATA_VB1_124)%>');
                } else {
                    $('#SsidName').html('');
                };
                
                SW.UDT.PageableResource.initialize({
                    container: $('#wirelessCurrentEndpointConnectionsContainer<%=Resource.ID %>'),
                    webServiceUrl: '/Orion/UDT/Services/Wireless.asmx',
                    webServiceMethod: 'LoadWirelessCurrentEndpointConnections',
                    defaultRowsPerPage: <%= DefaultPageSize %>,
                    cacheEnabled: <%= DataCacheEnabled %>,
                
                    createWebServiceParams: function (params) {
                        params.sortDescending = true; // do we need to support sorting?  Just let them jump to the last page and page backwards.
                        params.wirelessObject = '<%= WirelessObjectEx %>';
                        params.apMode = <%= ApMode.ToString(CultureInfo.InvariantCulture).ToLowerInvariant() %>;
                        return params;
                    }
                });
            });
        </script>
    </Content>
</orion:resourceWrapper>
