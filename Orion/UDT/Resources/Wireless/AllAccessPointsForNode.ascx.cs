using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using SolarWinds.Logging;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.UDT.Web.Resources;

public partial class Orion_UDT_Resources_Wireless_AllAccessPointsForNode : UDTBaseResourceControl
{
    private static readonly Log _log = new Log("AllAccessPointsForNode");
    private static readonly string[] ResourceSearchabelColumns = new[] { "DisplayName" };

    private bool IsInAsyncPostBack
    {
        get { return ScriptManager.GetCurrent(Page).IsInAsyncPostBack; }
    }

    protected override Log GetLogger() { return _log; }

    private List<string> showAllSSIDsForAP;
    public string PostBackStr;

    public override string HelpLinkFragment
    {
        get { return "OrionUDTPHAccessPointDetailsSummary"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.UDTWebContent.UDTWCODE_VB1_40; }
    }


    public int NodeID
    {
        get
        {
            var p = ((INodeProvider)GetInterfaceInstance(typeof(INodeProvider)));
            if (p != null && p.Node != null)
                return p.Node.NodeID;
            return -1;
        }
    }

    public bool IsWirelessNode
    {
        get { return UDTWirelessDAL.IsWirelessNode(NodeID); }
    }

    public override string EditURL
    {
        get { return string.Format("/Orion/UDT/Resources/Wireless/EditAllAccessPointsForNode.aspx?ResourceID={0}&ViewID={1}&NetObjectID={2}", this.Resource.ID, this.Resource.View.ViewID, HttpUtility.UrlDecode(Request.QueryString["NetObject"])); }
    }


    protected override void OnInitResource(EventArgs e)
    {
        //// Don't cache the data
        CacheResourceData = false;
        //SearchInCachedData = false;
        // Hide the pager control
        SearchableColumns = ResourceSearchabelColumns;
        if (NodeID > -1)
        {
            return;
        }
    }

    protected override Control GetResourceWrapperControl()
    {
        return Wrapper;
    }

    protected override void HandleDefaultDataLoad(bool suppressDataReload)
    {
        if (ResourceLoadingMode == ResourceLoadingMode.Ajax 
            && IsInAsyncPostBack)
        {
            base.HandleDefaultDataLoad(suppressDataReload);
        }
    }

    protected override DataTable LoadResourceData(string searchText)
    {
        return NodeID != -1 ? UDTWirelessDAL.GetAccessPointsForNode(NodeID) : UDTWirelessDAL.GetAccessPointsForNode(searchText);
    }

    protected override void OnLoadResource(EventArgs e)
    {  
        if (NodeID != -1 && !IsWirelessNode)
        {
            // if node is not being polled for wireless, then do not shoe the resource.
            Wrapper.Visible = false;
            return;
        }

        PostBackStr = Page.ClientScript.GetPostBackEventReference(updatePanel, "CustomArgument");

        if (showAllSSIDsForAP == null)
        {
            showAllSSIDsForAP = new List<string>();
        }
    }

    protected override void RenderResource()
    {
        if (ResourceLoadingMode == ResourceLoadingMode.Ajax 
            && !IsInAsyncPostBack)
        {
            return;
        }

        string hiddenAccessPoints = apsExpanded.Value;
        //string hiddenAccessPoints = apsExpanded.Text;
        if (!(string.IsNullOrEmpty(hiddenAccessPoints)))
        {
            string[] hiddenAccessPointsList = hiddenAccessPoints.Split(',');
            foreach (var p in hiddenAccessPointsList)
            {
                showAllSSIDsForAP.Add(p);
            }
        }
        var AccessPoints = ApplyFiltering(Data);
        AccessPoints = ApplyPaging(AccessPoints);
        // get cached data (on page load they are empty and is populated on 'refresh' postback from client )

        //build the table a cell at a time
        HtmlTable htmlTable;
        htmlTable = new HtmlTable();
        htmlTable.CellPadding = 2;
        htmlTable.CellSpacing = 0;

        foreach (DataRow ap in AccessPoints.Rows)
        {

            int endpointCount = 0;
            int endpointTotal = 0;
            long apId = DALHelper.GetValueOrDefault(ap["ID"], 0L);
            string apName = ap["DisplayName"].ToString();

            // get the AccessPoint Status and WirelessType to set the wireless icon
            int status = DALHelper.GetValueOrDefault(ap["Status"], 0);
            string wirelessType = ap["WirelessType"].ToString();

            DataTable interfacesTable = UDTWirelessDAL.GetInterfacesForAccessPoint(apId);

            // get the distinct SSID rows from the given AP
            DataView view = new DataView(interfacesTable);
            DataTable distinctSSID = view.ToTable(true, "SSID");

            // get count of distinct SSID rows for the given AP
            int ssidCount = distinctSSID.Rows.Count;
            //set SSID text based on unique SSID count

            string ssid = string.Empty;
            List<string> channels = new List<string>();

            foreach (DataRow i in interfacesTable.Rows)
            {
                // total up the count of endpoints
                endpointTotal += DALHelper.GetValueOrDefault(i["Clients"], 0);

                string channel = i["Channel"].ToString();
                if (!channels.Contains(channel))
                    channels.Add(channel);

                ssid = i["SSID"].ToString();
            }

            int channelCount = channels.Count;
            // if only one channel and only one SSID, then just show channel on main row with SSID (and no expand button)
            bool ssidAllowExpand = ssidCount > 1 || channelCount > 1;



            string allChannels = string.Empty;
            channels.Sort();
            allChannels = string.Join(", ", channels);


            // build the table rows
            var htmlRow = new HtmlTableRow();

            htmlRow.Cells.Add(HtmlHelper.NewTableCell(IconHelper.GetWirelessIcon(status, wirelessType)));
            htmlRow.Cells.Add(HtmlHelper.NewTableCell(String.Format("<a href='{1}'>{0}</a>", HtmlHelper.CreateUdtViewHref("ap", apId.ToString(), apName), ap["Index"])));

            if (showAllSSIDsForAP != null && showAllSSIDsForAP.Contains(ap["ID"].ToString()))
            {
                htmlRow.Cells.Add(HtmlHelper.NewTableCell(String.Format(
                                                                      "<a href=\"javaScript:minusCollapse{2}('{0}');\";>{1}</a>",
                                                                      ap["ID"].ToString(),
                                                                      IconHelper.GetPlusMinusIcon("collapse", Resources.UDTWebContent.UDTWCODE_VB1_41), Resource.ID)));
            }
            else
            {
                htmlRow.Cells.Add(HtmlHelper.NewTableCell(ssidAllowExpand
                                                              ? (String.Format(
                                                                  "<a href=\"javaScript:plusExpand{2}('{0}');\";>{1}</a>",
                                                                  ap["ID"].ToString(),
                                                                  IconHelper.GetPlusMinusIcon("expand", Resources.UDTWebContent.UDTWCODE_VB1_42),
                                                                  Resource.ID))
                                                              : IconHelper.GetSSIDIcon(ssid)));

            }


            htmlRow.Cells.Add(HtmlHelper.NewTableCell(ssidAllowExpand ? String.Format(Resources.UDTWebContent.UDTWCODE_VB1_43, ssidCount) : HtmlHelper.CreateUdtViewHref("ssid", ssid, ssid)));
            htmlRow.Cells.Add(HtmlHelper.NewTableCell(ssidAllowExpand ? "" : allChannels));
            htmlRow.Cells.Add(HtmlHelper.NewTableCell(String.Format(Resources.UDTWebContent.UDTWCODE_VB1_44, endpointTotal)));

            AccessPointsTable.Rows.Add(htmlRow);

            // add extra rows for SSID expand
            if (showAllSSIDsForAP != null && showAllSSIDsForAP.Contains(ap["ID"].ToString()))
            {
                foreach (DataRow j in interfacesTable.Rows)
                {
                    endpointCount = DALHelper.GetValueOrDefault(j["Clients"], 0);

                    htmlRow = new HtmlTableRow();

                    htmlRow.Cells.Add(HtmlHelper.NewTableCell(""));
                    htmlRow.Cells.Add(HtmlHelper.NewTableCell(""));
                    htmlRow.Cells.Add(HtmlHelper.NewTableCell(""));
                    htmlRow.Cells.Add(HtmlHelper.NewTableCell(String.Format("{0} {1}", IconHelper.GetSSIDIcon(j["SSID"].ToString()), HtmlHelper.CreateUdtViewHref("ssid", j["SSID"].ToString(), j["SSID"].ToString()))));
                    htmlRow.Cells.Add(HtmlHelper.NewTableCell(j["Channel"].ToString()));
                    htmlRow.Cells.Add(HtmlHelper.NewTableCell(String.Format(Resources.UDTWebContent.UDTWCODE_VB1_45, endpointCount)));
                    AccessPointsTable.Rows.Add(htmlRow);
                }
            }
        }
        InitializingTable.Visible = false;

    }

    public override ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }

}