<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllSSIDs.ascx.cs" Inherits="Orion_UDT_Resources_Wireless_AllSSIDs" %>
<%@ Register TagPrefix="udt" TagName="ResourceSearcher" Src="~/Orion/UDT/Controls/ResourceSearcherHtml.ascx" %>
<%@ Register TagPrefix="udt" TagName="ResourcePager" Src="~/Orion/UDT/Controls/ResourcePagerHtml.ascx" %>

<orion:Include ID="Include1" runat="server" File="Charts/js/json2.js" /> <%-- This can go away once we the Core version is Galaga. At that point, json2.js is on every page. --%>
<orion:include ID="Include2" runat="server" File="UDT/js/PageableResource.js" />

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>    
        
    	    <div id="wirelessSSIDsContainer<%=Resource.ID %>"> 
            <table style="width: 100%; border-collapse: collapse">
                <tr>
                    <td>&nbsp;</td>
                    <td align = "right" width="200px">
                        <udt:ResourceSearcher runat="server" ID="ResourceSearcher" Visible="true" Hidden="False"/>            
                    </td>                        
                </tr>
            </table>
            
            <table id="SSIDsTable" border="0" cellpadding="2" cellspacing="0" width="100%"  class="NeedsZebraStripes">
                <tbody class="headers">
                    <tr>
                       <td class="ReportHeader" width="5%" >&nbsp;</td>
                       <td class="ReportHeader" width="30%" ><%= Resources.UDTWebContent.UDTWEBDATA_VB1_124%></td>
                       <td class="ReportHeader" width="25%" ><%= Resources.UDTWebContent.UDTWEBDATA_VB1_125%></td>
                       <td class="ReportHeader" width="40%" ><%= Resources.UDTWebContent.UDTWEBDATA_VB1_126%></td>
                    </tr>
                </tbody>
                <tbody class="results"/>
            </table>
            <udt:ResourcePager runat="server" ID="ResourcePager" Visible="True" Hidden="False" />

        </div>
        
        <script type="text/javascript">
            $(function () {
                                
                SW.UDT.PageableResource.initialize({
                    container: $('#wirelessSSIDsContainer<%=Resource.ID %>'),
                    webServiceUrl: '/Orion/UDT/Services/Wireless.asmx',
                    webServiceMethod: 'LoadWirelessSSIDs',
                    defaultRowsPerPage: <%= DefaultPageSize %>,
                    cacheEnabled: <%= DataCacheEnabled %>, 
                
                    createWebServiceParams: function (params) {
                        params.sortDescending = true; // do we need to support sorting?  Just let them jump to the last page and page backwards.
                        params.wirelessObject = '<%= WirelessObjectEx %>';
                        return params;
                    }
                });
            });
        </script>
    </Content>
</orion:resourceWrapper>
