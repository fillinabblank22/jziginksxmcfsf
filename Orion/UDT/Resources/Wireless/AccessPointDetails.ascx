<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AccessPointDetails.ascx.cs" Inherits="Orion_UDT_Resources_Wireless_AccessPointDetails" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
        <style type="text/css">
            .labelStyle { font-weight:bold; padding-left: 10px; }
        </style>
        <%--ap details table--%>
        <table runat="server" id="AccessPointDetails" border="0" cellpadding="2" cellspacing="0" width="100%" visible="true" class="NeedsZebraStripes">
            <tr>
                <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_118 %></td><td><%=ApStatus%></td>
            </tr>
            <tr>
                <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_119 %></td><td><%=UdtWirelessAp.DisplayName%></td>
            </tr>
            <tr>
                <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_120 %></td><td><%=UdtWirelessAp.IPAddress%></td>
            </tr>
            <tr>
                <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_121%></td><td><%=UdtWirelessAp.WirelessTypeText%></td>
            </tr>
        </table>
        <table runat="server" id="AccessPointController" border="0" cellpadding="2" cellspacing="0" width="100%" visible="true" class="NeedsZebraStripes">
            <tr>
                <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_122%></td><td><%=ApController%></td>
            </tr>
        </table>

        <orion:ExceptionWarning ID="ExceptionWarning" Visible="False" Text='' runat="server" />

	</Content>
</orion:resourceWrapper>