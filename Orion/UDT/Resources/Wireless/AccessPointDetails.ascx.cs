using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI.HtmlControls;
using SolarWinds.Logging;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.Web.UI;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.UDT.Web.NetObjects;
using SolarWinds.UDT.Web.Providers;

public partial class Orion_UDT_Resources_Wireless_AccessPointDetails : SolarWinds.Orion.Web.UI.BaseResourceControl
{
    
    private static readonly Log _log = new Log();

    protected UDTWirelessAPNetObject UdtWirelessAp { get; set; }

    
    public override string HelpLinkFragment
    {
        get { return "OrionUDTPHWirelessAccessPointDetails"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.UDTWebContent.UDTWCODE_VB1_38; }
    }


    //public int NodeID
    //{
    //    get
    //    {
    //        int id;
    //        // check if we have a node id available
    //        try
    //        {
    //            id = ((INodeProvider)GetInterfaceInstance(typeof(INodeProvider))).Node.NodeID;
    //        }
    //        catch (Exception ex)
    //        {
    //            id = 0;
    //        }
    //        return id;
    //    }
    //}

    public int AccessPointID
    {
        get
        {
            int apId = 0;

            try
            {
                string tmpAccessPoint = HttpUtility.UrlDecode(Request.QueryString["NetObject"]);
                string[] netObjectParts = tmpAccessPoint.Split(':');
                // check that we have a UDT AP netObject
                if (netObjectParts[0].ToString() == "UW-AP")
                {
                    apId = int.Parse(netObjectParts[1]);
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat("There was an error getting AccessPointID:  Access Point Details resource.  Details: {0}", ex.ToString());
            }
            return apId;

        }
    }


    protected override void OnLoad(EventArgs e)
    {
        try
        {
            GetData();
        }
        catch (Exception ex)
        {
            ExceptionWarning.Text = String.Format(Resources.UDTWebContent.UDTWCODE_VB1_39, ex.Message);
            ExceptionWarning.Visible = true;
            _log.ErrorFormat("There was an error rendering:  Access Points DEtailst.  Details: {0}", ex.ToString());
        }
    }

    
    protected override void OnInit(EventArgs e)
    {

        // if we get a NodeID then resource is not on the Endpoint view, so hide.
        //if (NodeID > 0 || AccessPointID == 0)
        if (AccessPointID == 0)
        {
            Wrapper.Visible = false;
            return;
        }
        
        //Bind to netobject (UDT WirelessAccessPoint)
        var nObject = Request["NetObject"];
        if (nObject == null)
        {
            nObject = "UW-AP:15555"; //we're doing this so the preview works on the customize page, page
        }
        this.UdtWirelessAp = NetObjectFactory.Create(nObject, true) as UDTWirelessAPNetObject;

        base.OnInit(e);
    }

    protected void GetData()
    {
        try
        {
            // hide the controller row if ap is autonomous (0)                 
            if ((UdtWirelessAp.WirelessType.ToLower() == "0") ||(UdtWirelessAp.WirelessType.ToLower() == "autonomous"))
            {
                AccessPointController.Visible = false;
            }
                
        }
        catch (Exception ex)
        {
            _log.ErrorFormat(String.Format("Error occurred retrieving access point data for Access Points Details.GetData().  Error:{0}", ex.Message));
        }
        
    }

    
    // UI fields
    public String ApStatus
    {
        get
        {
            int apStatusInt = UdtWirelessAp.Status;

            string apStatusIcon = String.Format("{0} {1}", IconHelper.GetWirelessIcon(apStatusInt,
                                                                           UdtWirelessAp.WirelessType), IconHelper.GetWirelessStatus(apStatusInt));
            return apStatusIcon;
        }
    }

    public String ApController
    {
        get
        {
            string controllerLink = String.Format(
                "<a href='/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}'>{1}</a>", UdtWirelessAp.NodeId, UdtWirelessAp.ControllerName);

            string controllerStatusIcon = IconHelper.GetNodeStatusIcon(UdtWirelessAp.ControllerStatus, UdtWirelessAp.NodeId);

            return String.Format("{0} {1}", controllerStatusIcon, controllerLink);
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IUDTWirelessCommonProvider) }; }
    }
}