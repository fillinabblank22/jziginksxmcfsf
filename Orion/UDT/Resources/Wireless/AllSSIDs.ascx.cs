using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Web.Helpers;
using SolarWinds.UDT.Web.NetObjects;
using SolarWinds.UDT.Web.Providers;

public partial class Orion_UDT_Resources_Wireless_AllSSIDs : SolarWinds.Orion.Web.UI.BaseResourceControl
{

    private static readonly Log _log = new Log("AllSSIDs");

    protected override string DefaultTitle { get { return Resources.UDTWebContent.UDTWEBCODE_VB1_52; } }

    public override string HelpLinkFragment { get { return "OrionUDTPHAllSSIDS"; } }

    public override String SubTitle { get { return this.Resource.Properties["SubTitle"]; } }

    public UDTNetObject NetObject { get; set; }

    public int DefaultPageSize
    {
        get
        {
            int pageSize;
            if (!Int32.TryParse(Resource.Properties["MaxRowsCount"], out pageSize))
                pageSize = 10;
            return pageSize;
        }
    }

    public string DataCacheEnabled { get { return UDTSettingDAL.CacheResourceData ? "true" : "false"; } }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ResourceSearcher.ResourceId = Resource.ID;
        ResourcePager.ResourceId = Resource.ID;

        NetObject = new UDTNetObject(Request.QueryString["NetObject"]);
    }

    public string WirelessObject
    {
        get
        {
            if (NetObject.NetObjectType != "UW-AP" && NetObject.NetObjectType != "UW-SSID")
                return string.Empty;

            return NetObject.Value;
        }
    }

    public string WirelessObjectEx { get { return HtmlHelper.AssembleWirelessObjectEx(WirelessObject, Resource.ID); } } 

    public override string EditURL
    {
        get { return string.Format("/Orion/UDT/Resources/Wireless/EditAllSSIDs.aspx?ResourceID={0}&ViewID={1}&NetObjectID={2}", this.Resource.ID, this.Resource.View.ViewID, HttpUtility.UrlDecode(Request.QueryString["NetObject"])); }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IUDTAccessPointDetailProvider) }; }
    }
}