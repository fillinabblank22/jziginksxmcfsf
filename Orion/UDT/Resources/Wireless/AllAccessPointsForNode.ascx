<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllAccessPointsForNode.ascx.cs" Inherits="Orion_UDT_Resources_Wireless_AllAccessPointsForNode" %>
<%@ Register TagPrefix="udt" TagName="ResourceSearcher" Src="~/Orion/UDT/Controls/ResourceSearcher.ascx" %>
<%@ Register TagPrefix="udt" TagName="ResourcePager" Src="~/Orion/UDT/Controls/ResourcePager.ascx" %>


<orion:resourceWrapper runat="server" ID="Wrapper">  
	<Content>
     
        <asp:UpdatePanel ID="updatePanel" runat="server">
            <ContentTemplate> 
                <%--<asp:textbox runat="server" ID="apsExpanded" Visible="true" />--%>
                <asp:HiddenField runat="server" ID="apsExpanded"  />
              <table style="width: 100%; border-collapse: collapse">
                    <tr>
                        <td>&nbsp;</td>
                        <td align = "right" width="200px">
                            <udt:ResourceSearcher runat="server" ID="ResourceSearcher" Visible="true" Hidden="false"  />            
                        </td>                        
                    </tr>
                </table> 
                <table runat="server" id="AccessPointsTable" border="0" cellpadding="2" cellspacing="0" width="100%" visible="true" class="NeedsZebraStripes" >
                    <tr>
                       <td class="ReportHeader" width="5%" >&nbsp;</td>
                       <td class="ReportHeader" width="28%" ><%= Resources.UDTWebContent.UDTWEBDATA_VB1_123 %></td>
                       <td class="ReportHeader" width="5%" >&nbsp;</td>
                       <td class="ReportHeader" width="27%" ><%= Resources.UDTWebContent.UDTWEBDATA_VB1_124%></td>
                       <td class="ReportHeader" width="12%" ><%= Resources.UDTWebContent.UDTWEBDATA_VB1_125%></td>
                       <td class="ReportHeader" width="23%" ><%= Resources.UDTWebContent.UDTWEBDATA_VB1_126%></td>
                    </tr>
                </table>
                <table runat="server" id="InitializingTable" border="0" cellpadding="2" cellspacing="0" width="100%" visible="true"  >
                    <tr>
                       <td colspan="6" align="center"><img src="/Orion/UDT/Images/loading.gif" alt="<%= Resources.UDTWebContent.UDTWEBDATA_VB1_128 %>"></img>&nbsp;<%= Resources.UDTWebContent.UDTWEBDATA_VB1_127 %></td>
                    </tr>
                </table>
                <udt:ResourcePager runat="server" ID="ResourcePager" />
            </ContentTemplate>
        </asp:UpdatePanel> 
	</Content>
</orion:resourceWrapper>
 
<script type="text/javascript">
        function plusExpand<%=Resource.ID%>(apId) {
            var TB = this.document.getElementById("<%=apsExpanded.ClientID%>"); 
            TB.value = TB.value + "," + apId;               
                <%= PostBackStr %>           
        };
    
        function minusCollapse<%=Resource.ID%>(apId) {
            var TB = this.document.getElementById("<%=apsExpanded.ClientID%>");                
            var searchText="," + apId;
            var tempText=TB.value;
            TB.value = tempText.replace(searchText,"");             
                <%= PostBackStr %>  
        };
    
</script>

