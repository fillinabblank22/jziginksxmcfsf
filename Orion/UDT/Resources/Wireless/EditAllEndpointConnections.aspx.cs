using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_UDT_Resources_Wireless_EditAllEndpointConnections : System.Web.UI.Page
{
	#region Resource property
	private ResourceInfo resource;

	protected ResourceInfo Resource
	{
		get { return this.resource; }
		set { this.resource = value; }
	} 
	#endregion

    #region NetObjectType property
    private string netObjectType;

    protected string NetObjectType
    {
        get { return this.netObjectType; }
        set { this.netObjectType = value; }
    }
    #endregion

    private string netObjectID;

    protected string NetObjectID
    {
        get { return this.netObjectID; }
        set { this.netObjectID = value; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        // fill in period list
        foreach (string period in Periods.GenerateSelectionList())
            Period.Items.Add(new ListItem(Periods.GetLocalizedPeriod(period), period));
    }

	protected void Page_Load(object sender, EventArgs e)
	{
        this.NetObjectType = this.Request.QueryString["Type"];
        NetObjectID = Request.QueryString["NetObjectID"];

		if (!String.IsNullOrEmpty(this.Request.QueryString["ResourceID"]))
		{
			int resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
			this.Resource = ResourceManager.GetResourceByID(resourceID);
            this.Title = String.Format(Resources.UDTWebContent.UDTWEBDATA_VB1_134, Resource.Title);
		}

		if (!this.IsPostBack)
		{
            int max;
            if (!Int32.TryParse(this.Resource.Properties["MaxRowsCount"], out max))
            {
                this.Resource.Properties["MaxRowsCount"] = "10";
            }
            this.maxRowsCount.Text = this.Resource.Properties["MaxRowsCount"];

            if (!string.IsNullOrEmpty(Resource.Properties["Title"]))
            {
                this.resourceTitleEditor.ResourceTitle = Resource.Properties["Title"];
            }
            else
            {
                this.resourceTitleEditor.ResourceTitle = string.Empty;
            }

            if (!string.IsNullOrEmpty(Resource.Properties["SubTitle"]))
            {
                this.resourceTitleEditor.ResourceSubTitle = Resource.Properties["SubTitle"];
            }
            else
            {
                this.resourceTitleEditor.ResourceSubTitle = string.Empty;
            }
            Period.Text = Resource.Properties["Period"];
        }

	}

	protected void SubmitClick(object sender, EventArgs e)
	{
        this.Resource.Properties["Title"] = this.resourceTitleEditor.ResourceTitle;
        this.Resource.Properties["SubTitle"] = this.resourceTitleEditor.ResourceSubTitle;
        this.Resource.Properties["Period"] = this.Period.Text.ToString();
        this.Resource.Title = this.resourceTitleEditor.ResourceTitle;
        try
        {
            // check Max number of endpoint connections to display field value 
            this.Resource.Properties["MaxRowsCount"] = validateTextBox(maxRowsCount.Text, this.Resource.Properties["MaxRowsCount"]);
        }
        catch
        {
            this.Resource.Properties["MaxRowsCount"] = "10";
        }
       
		SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
        Response.Redirect(String.Format("/Orion/View.aspx?ViewID={0}&NetObject={1}", this.Resource.View.ViewID, NetObjectID));
	}

    protected string validateTextBox(string txtValue, string currentValue)
    {
        int newValue;
        string returnValue;
        bool result = int.TryParse(txtValue, out newValue);

        if (result)
        {
            if (newValue > 100)
            {
                newValue = 100;
            }

            if (newValue < 1)
            {
                newValue = 1;
            }
            returnValue = newValue.ToString();
        }
        else
        {
            returnValue = currentValue;
        }
        return returnValue;
    }
}
