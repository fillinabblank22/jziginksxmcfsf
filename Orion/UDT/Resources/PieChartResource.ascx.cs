﻿using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.Charting.v2;
using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.PieCharts)]
public partial class Orion_UDT_Resources_PieChartResource : StandardChartResource
{
    protected void Page_Init(object sender, EventArgs e)
	{
		HandleInit(WrapperContents);
	}

	protected override bool AllowCustomization
	{
		get { return Profile.AllowCustomize; }
	}

    protected override string NetObjectPrefix
    {
        get 
        { 
            return "UDT"; 
        }
    }

    protected override string DefaultTitle
    {
        get { return String.Empty; }
    }

	protected override void AddChartExportControl(System.Web.UI.Control resourceWrapperContents)
	{
		// There is no need for export button
	}

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        return new string[0];
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }
    public override string HelpLinkFragment
    {
        get
        {
            return String.Empty;
        }
    }
}
