﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EndpointUserLogins.ascx.cs" Inherits="Orion_UDT_Resources_EndpointUserLogins" %>

<%@ Register TagPrefix="udt" TagName="ResourceSearcher" Src="~/Orion/UDT/Controls/ResourceSearcher.ascx" %>
<%@ Register TagPrefix="udt" TagName="ResourcePager" Src="~/Orion/UDT/Controls/ResourcePager.ascx" %>
<style type="text/css">
.UDT_AllUserLoginBox
{
    background-color: #e4f1f8;
    padding: 1em;
    margin-top: 1em;
    padding-left:2em;
    font:inherit;
    font-size:9pt;
}

.UDT_AllUserLoginBox div
{
    padding-top: 1em;
    clear: both;    
    margin-left:-12pt;
}

.UDT_AllUserLoginBox div.CORE_First
{
    padding-top: 0em;
}
.UDT_AllUserLoginBox div p
{
    margin: 0px 0px 0px 25px;
    line-height: 1.8em;
}

.CORE_Buttons
{
    text-align: left;
     font:inherit;
    font-size:9pt;
     padding-left:-2;
}
</style>
<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>    
        <asp:UpdatePanel ID="ResourceUpdatePanel" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <table style="width: 100%; border-collapse: collapse">
                    <tr>
                        <td>&nbsp;</td>
                        <td align = "right" width="200px">
                            <udt:ResourceSearcher runat="server" ID="ResourceSearcher" Visible="true" Hidden="False"/>            
                        </td>                        
                    </tr>
                </table>
                
                <table runat="server" id="EndpointUserLogins" border="0" cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes">
                    <tr>
                        <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_164 %></td>
                        <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_165 %></td>                
                    </tr>
                </table>
                <udt:ResourcePager runat="server" ID="ResourcePager" Visible="True" Hidden="False" />             

          <div id="AllUserLoginsGetStarted" style="display: none" runat="server">
           <div class="UDT_AllUserLoginBox">
              <ul style="list-style: square">
              <li><%= Resources.UDTWebContent.UDTWEBDATA_VB1_166 %></li>
              <li><%= Resources.UDTWebContent.UDTWEBDATA_VB1_167 %></li>
              </ul>
              <div class="CORE_Buttons">                
                  »&nbsp;&nbsp;<asp:HyperLink CssClass="sw-link" ID="lnkCore" runat="server" NavigateUrl="~/Orion/Nodes/Add/Default.aspx?&amp;restart=false" Text="<%$ Resources:UDTWebContent,UDTWEBDATA_VB1_168%>"/>
               </div>
           </div>             
           </div>
                <orion:ExceptionWarning ID="ExceptionWarning" Visible="False" Text="" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
	</Content>
</orion:resourceWrapper>