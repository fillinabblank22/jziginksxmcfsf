﻿using System;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using SolarWinds.Logging;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Common.Models;
using SolarWinds.Orion.Web.Charting;
using System.Collections.Generic;
using SolarWinds.UDT.Web.DisplayTypes;
using SolarWinds.UDT.Web.Helpers;
using SolarWinds.UDT.Web.NetObjects;
using SolarWinds.UDT.Web.Providers;
using SolarWinds.UDT.Web.Resources;

public partial class Orion_UDT_Resources_EndpointDetails_EndpointCurrentConnections : UDTBaseResourceControl
{
    private static readonly Log _log = new Log("EndpointCurrentConnectionResource");
    protected override Log GetLogger() { return _log; }

    private static readonly string[] ResourceSearchabelColumns = new[] { "InterfaceName", "ParentName" };

    protected override string DefaultTitle
    {
        get { return Resources.UDTWebContent.UDTWEBCODE_VB1_54; }
    }

    public UDTEndpointNetObject UDTEndpointNetObject
    {
        get
        {
            var udtEndpointProvider = GetInterfaceInstance<IUDTEndpointProvider>();
            if (udtEndpointProvider != null)
            {
                return udtEndpointProvider.UDTEndpointNetObject;
            }
            return null;
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionUDTPHEndpointNetworkConnections";
        }
    }

    public int NodeID
    {
        get
        {
            try
            {
                return ((INodeProvider)GetInterfaceInstance(typeof(INodeProvider))).Node.NodeID;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public override string EditURL
    {
        get { return string.Format("/Orion/UDT/Resources/EndpointDetails/EditEndpointCurrentConnections.aspx?ResourceID={0}&ViewID={1}&NetObjectID={2}", Resource.ID, Resource.View.ViewID, HttpUtility.UrlDecode(UDTEndpointNetObject.NetObjectID)); }
    }

    protected override Control GetResourceWrapperControl() { return Wrapper; }

    protected override void OnInitResource(EventArgs e)
    {
        CacheResourceData = true;
        TruncateCachedData = false;

        SearchableColumns = ResourceSearchabelColumns;
    }

    protected override DataTable LoadResourceData(string searchText)
    {
        var data = UDTEndpointNetObject.GetCurrentNetworkData();
            
        // Force search over cached data 
        SearchInCachedData = true;
        
        return data;
    }

    protected override void RenderResource()
    {
        if (Data == null || Data.Rows.Count == 0)
        {
            TotalRows = 0;
            return;
        }

        var data = Data;

        data = ApplyFiltering(data);

        if (data.Rows.Count > 1)
        {
            using (var view = new DataView(data))
            {
                view.Sort = "ConnectionType ASC, EntryType ASC, ParentName ASC";
                data = view.ToTable();
            }
        }

        data = ApplyPaging(data);

        foreach (DataRow row in data.Rows)
        {
            var entryType = DALHelper.GetValueOrDefault(row["EntryType"], string.Empty);
            var isL3 = entryType == "L3";
            var isL2 = entryType == "L2";

            var ifID = DALHelper.GetValueOrDefault(row["InterfaceID"], 0L);
            var ifName = DALHelper.GetValueOrDefault(row["InterfaceName"], string.Empty);
            var ifStatus = DALHelper.GetValueOrDefault(row["InterfaceStatus"], 0);

            var parentID = DALHelper.GetValueOrDefault(row["ParentID"], 0L);
            var parentName = DALHelper.GetValueOrDefault(row["ParentName"], string.Empty);
            var parentStatus = DALHelper.GetValueOrDefault(row["ParentStatus"], 0);

            var firstSeen = DALHelper.GetValueOrDefault(row["FirstSeen"], DALHelper.SQLMaxDate);
            var lastSeen = DALHelper.GetValueOrDefault(row["LastSeen"], DALHelper.SQLMinDate);
            var pollingInterval = DALHelper.GetValueOrDefault(row["PollingIntervalMinutes"], 15);

            var htmlRow = new HtmlTableRow();

            if (isL2)
            {
                // port
                var statIcon = IconHelper.GetPortStatusIcon(ifStatus);
                var href = HtmlHelper.CreateUdtViewHref("port", ifID.ToString(CultureInfo.InvariantCulture), ifName);
                var portHtml = string.Format("{0} {1}", statIcon, ifName == "" ? Resources.UDTWebContent.UDTWEBCODE_VB1_55 : href);
                HtmlHelper.AddHtmlCell(htmlRow, (cell) => { cell.InnerHtml = portHtml; });

                // node
                statIcon = IconHelper.GetNodeStatusIcon(parentStatus, (int)parentID);
                href = HtmlHelper.CreateUdtViewHref("node", parentID.ToString(CultureInfo.InvariantCulture), parentName);
                var nodeHtml = string.Format("{0} {1}", statIcon, href);
                HtmlHelper.AddHtmlCell(htmlRow, (cell) => { cell.InnerHtml = nodeHtml; });
            }
            else if (isL3)
            {
                // port
                var statIcon = IconHelper.GetPortStatusIcon(UDTSettingDAL.ShowL3PortConnections ? ifStatus : 0);
                var href = HtmlHelper.CreateUdtViewHref("port", ifID.ToString(CultureInfo.InvariantCulture), String.Format(Resources.UDTWebContent.UDTWEBCODE_VB1_56, ifName));
                var portHtml = string.Format("{0} {1}", statIcon, ifName == "" || !UDTSettingDAL.ShowL3PortConnections ? Resources.UDTWebContent.UDTWEBCODE_VB1_57 : href);
                HtmlHelper.AddHtmlCell(htmlRow, (cell) => { cell.InnerHtml = portHtml; });

                // node
                statIcon = IconHelper.GetNodeStatusIcon(parentStatus, (int)parentID);
                href = HtmlHelper.CreateUdtViewHref("node", parentID.ToString(CultureInfo.InvariantCulture), parentName);
                var nodeHtml = string.Format("{0} {1}", statIcon, href);
                HtmlHelper.AddHtmlCell(htmlRow, (cell) => { cell.InnerHtml = nodeHtml; });
            }
            else
            {
                // ssid
                var clSSID = DALHelper.GetValueOrDefault(row["ClientSSID"], string.Empty);
                var statIcon = IconHelper.GetSSIDIcon(entryType);
                var href = HtmlHelper.CreateUdtViewHref("ssid", clSSID, clSSID);
                HtmlHelper.AddHtmlCell(htmlRow, (cell) => { cell.InnerHtml = String.Format("{0} {1}", statIcon, href); });

                // ap
                var apType = DALHelper.GetValueOrDefault(row["APType"], string.Empty);
                statIcon = IconHelper.GetWirelessIcon(parentStatus, apType);
                href = HtmlHelper.CreateUdtViewHref("ap", parentID.ToString(CultureInfo.InvariantCulture), parentName);
                HtmlHelper.AddHtmlCell(htmlRow, (cell) => { cell.InnerHtml = String.Format("{0} {1}", statIcon, href); });
            }

            // connection duration
            HtmlHelper.AddHtmlCell(htmlRow,
                                   (cell) =>
                                       {
                                           cell.InnerHtml = lastSeen - firstSeen <= TimeSpan.FromMinutes(0.9 * pollingInterval) // firstSeen >= lastSeen 
                                               ? string.Format(Resources.UDTWebContent.UDTWEBCODE_VB1_58, pollingInterval)
                                               : HtmlHelper.GetTimeDiff(firstSeen, lastSeen);
                                       });

            // connection type
            var connectionType = DALHelper.GetValueOrDefault(row["ConnectionType"], 3);
            HtmlHelper.AddHtmlCell(htmlRow, (cell) => { cell.InnerHtml = new UdtConnectionType((EndpointConnectionType)connectionType).AsHtml; });

            EndpointCurrentConnections.Rows.Add(htmlRow);
        }
    }

   
    private void Render(object sender, EventArgs e)
    {
        DataTable tblCurNetConnections;

        tblCurNetConnections = new DataTable();


        if (tblCurNetConnections.Rows.Count > 0)
        {
            // Following code removes duplicity
            DataTable tblCurNetConnections2 = tblCurNetConnections.Clone();

            var dCurConnections = new Dictionary<string, DataRow>();
            foreach (DataRow row in tblCurNetConnections.Rows)
            {
                string tmpKey = string.Empty;
                tmpKey += row["ParentID"].ToString();
                tmpKey += "|";
                tmpKey += row["ParentName"].ToString();
                tmpKey += "|";
                tmpKey += row["InterfaceName"].ToString();
                tmpKey += "|";
                tmpKey += row["PortStatus"].ToString();
                tmpKey += "|";
                tmpKey += row["WirelessStatus"].ToString();
                tmpKey += "|";
                tmpKey += row["ConnectionType"].ToString();
                if (!dCurConnections.ContainsKey(tmpKey))
                {
                    dCurConnections.Add(tmpKey, row);
                    tblCurNetConnections2.ImportRow(row);
                }
            }

            tblCurNetConnections = tblCurNetConnections2;


            tblCurNetConnections.Columns.Add("ConnectionDuration", typeof (string)); // FB80520
            tblCurNetConnections.Columns.Add(new DataColumn("Parent", typeof(string)));
            tblCurNetConnections.Columns.Add(new DataColumn("Interface", typeof (string)));
            tblCurNetConnections.Columns.Add(new DataColumn("ImgConnectionType", typeof (string)));
            
            foreach (DataRow row in tblCurNetConnections.Rows)
            {
                // check if we are a wireless row or not
                var isWireless = DALHelper.GetValueOrDefault(row["InterfaceID"], 0) == -1;

                var parentId = (Int64)row["ParentID"];
                var parentStatus = (int)row["ParentStatus"];
                var parentName = (string)row["ParentName"];
                var interfaceId = (int)row["InterfaceID"];
                var interfaceName = (string)row["InterfaceName"];
                var wirelessStatus = (string)row["WirelessStatus"];
                var portStatus = (Int16)row["PortStatus"];
                var connectionType = (byte)row["ConnectionType"];

                if (isWireless)
                {
                    // add the Interface (SSID) and Parent (Wireless Access Point) data
                    row["Interface"] = String.Format("{0} {1}", IconHelper.GetSSIDIcon(interfaceName), HtmlHelper.CreateUdtViewHref("ssid", interfaceName, interfaceName));
                    row["Parent"] = String.Format("{0} {1}", IconHelper.GetWirelessIcon(parentStatus, wirelessStatus), HtmlHelper.CreateUdtViewHref("ap", parentId.ToString(), parentName));
                }
                else
                {

                    // determine interface (Port) name
                    interfaceName = string.IsNullOrEmpty(interfaceName)
                      ? ((interfaceId == 0) ? Resources.UDTWebContent.UDTWEBCODE_VB1_57 : Resources.UDTWebContent.UDTWEBCODE_VB1_55)
                      : interfaceName;
                    
                    // add the Interface (Port) and Parent (Node) data
                    row["Parent"] = String.Format("{0} {1}", IconHelper.GetNodeStatusIcon(parentStatus, Convert.ToInt32(parentId)), HtmlHelper.CreateUdtViewHref("node", parentId.ToString(), parentName));

                    row["Interface"] = interfaceName == Resources.UDTWebContent.UDTWEBCODE_VB1_57 || interfaceName == Resources.UDTWebContent.UDTWEBCODE_VB1_55
                                           ? String.Format("{0} {1}", IconHelper.GetPortStatusIcon(Convert.ToInt32(portStatus)),
                                                           interfaceName)
                                           : String.Format("{0} {1}", IconHelper.GetPortStatusIcon(Convert.ToInt32(portStatus)),
                                                           HtmlHelper.CreateUdtViewHref("port",
                                                                                        row["InterfaceID"].ToString(),
                                                                                        interfaceName));
                }

                var displayType = new UdtConnectionType((EndpointConnectionType)connectionType);
                row["ImgConnectionType"] = displayType.AsHtml;
                
                // dates are converted to display format in the DAL method.
                var firstSeen = (row["StartDate"] != DBNull.Value)
                                         ? Convert.ToDateTime(row["StartDate"])
                                         : DateTime.Now.ToLocalTime();
                var lastSeen = (row["EndDate"] != DBNull.Value)
                                        ? Convert.ToDateTime(row["EndDate"])
                                        : DateTime.Now.ToLocalTime();

                //string strConnDuration = HtmlHelper.GetTimeDiff(firstSeen, lastSeen);
               // row["ConnectionDuration"] = strConnDuration;
                
                // get the connection duration
                string connectionDuration;
                if (firstSeen >= lastSeen)
                {
                    var pollInterval = (int)row["PollInterval"];
                    // if firstSeen and lastSeen are equal, or lastUpdate is less (may be initial poll of device) so show the node wired(udt)/wireless polling period
                    connectionDuration = string.Format(Resources.UDTWebContent.UDTWEBCODE_VB1_58, pollInterval);
                }
                else
                {
                    connectionDuration = HtmlHelper.GetTimeDiff(firstSeen, lastSeen);
                }
                row["ConnectionDuration"] = connectionDuration;
            }
            //NoDataFoundPanel.Visible = false;
        }
        else // no active records found for endpoint
        {
            //NoDataFoundPanel.Visible = true;
            
            // check if we are on a Node page and hide resource if no connection data found
            if (NodeID > 0)
            {
                this.Wrapper.Visible = false;
                return;
            }
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IUDTEndpointProvider) }; }
    }

    protected override void HandleDefaultDataLoad(bool suppressDataReload)
    {
        ReloadData = true;
        base.HandleDefaultDataLoad(suppressDataReload);
    }
    
}
