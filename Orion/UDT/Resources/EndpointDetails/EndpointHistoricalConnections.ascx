﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EndpointHistoricalConnections.ascx.cs" Inherits="Orion_UDT_Resources_EndpointDetails_EndpointHistoricalConnections" %>

<%@ Register TagPrefix="udt" TagName="ResourcePager" Src="~/Orion/UDT/Controls/ResourcePager.ascx" %>
<%@ Register TagPrefix="udt" TagName="ResourceSearcher" Src="~/Orion/UDT/Controls/ResourceSearcher.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>    
        <asp:UpdatePanel ID="ResourceUpdatePanel" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <table style="width: 100%; border-collapse: collapse">
                    <tr>
                        <td>&nbsp;</td>
                        <td align = "right" width="200px">
                            <udt:ResourceSearcher runat="server" ID="ResourceSearcher" Visible="true" Hidden="False"/>            
                        </td>                        
                    </tr>
                </table>
                <table runat="server" id="EndpointHistoryConnections" border="0" cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes">
                    <tr>
                    <td class="ReportHeader"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_136%></td>
                    <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_145%></td>
                    <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_146%></td>
                    <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_148%></td>                
                    </tr>
                </table>
                <udt:ResourcePager runat="server" ID="ResourcePager" Visible="True" Hidden="False" />
                <orion:ExceptionWarning ID="ExceptionWarning" Visible="false" Text="" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </Content>
</orion:resourceWrapper>
