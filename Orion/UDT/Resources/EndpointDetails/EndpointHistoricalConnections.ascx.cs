using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using SolarWinds.Logging;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.UDT.Common.Models;
using SolarWinds.UDT.Web.DAL;
using System.Web.UI;
using SolarWinds.UDT.Web.DisplayTypes;
using SolarWinds.UDT.Web.Helpers;
using SolarWinds.UDT.Web.NetObjects;
using SolarWinds.UDT.Web.Providers;
using SolarWinds.UDT.Web.Resources;

public partial class Orion_UDT_Resources_EndpointDetails_EndpointHistoricalConnections : UDTBaseResourceControl
{
    private static readonly Log _log = new Log("EndpointHistoricalConnectionResource");
    protected override Log GetLogger() { return _log; }

    private static readonly string[] ResourceSearchabelColumns = new[] { "InterfaceName", "ParentName" };

    protected string TimePeriod = Resources.UDTWebContent.UDTWCODE_VB1_51;

    protected override String DefaultTitle { get { return Resources.UDTWebContent.UDTWEBCODE_VB1_59; } }
    
    public override String SubTitle { get { return Periods.GetLocalizedPeriod(TimePeriod); } }

    public override string HelpLinkFragment { get { return "OrionUDTPHEndpointHistoricalConnections"; } }

    public UDTEndpointNetObject UDTEndpointNetObject
    {
        get
        {
            var udtEndpointProvider = GetInterfaceInstance<IUDTEndpointProvider>();
            if (udtEndpointProvider != null)
            {
                return udtEndpointProvider.UDTEndpointNetObject;
            }
            return null;
        }
    }

    public int NodeID
    {
        get
        {
            try
            {
                return ((INodeProvider)GetInterfaceInstance(typeof(INodeProvider))).Node.NodeID;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public override string EditURL
    {
        get { return string.Format("/Orion/UDT/Resources/EndpointDetails/EditEndpointHistoricalConnections.aspx?ResourceID={0}&ViewID={1}&NetObjectID={2}", Resource.ID, Resource.View.ViewID, HttpUtility.UrlDecode(UDTEndpointNetObject.NetObjectID)); }
    }

    public bool LoadDetailedHistory { get; set; }

    protected override void OnInitResource(EventArgs e)
    {
        SearchableColumns = ResourceSearchabelColumns;
        CacheResourceData = true;

        try
        {
            if (Resource.Properties.ContainsKey("Period"))
                TimePeriod = Resource.Properties["Period"];

            LoadDetailedHistory = Resource.Properties.ContainsKey("ShowDetailedHistory")
                ? bool.Parse(Resource.Properties["ShowDetailedHistory"]) 
                : UDTSettingDAL.EndpointShowDetailedHistory; 
        }
        catch (Exception ex)
        {
            TimePeriod = Resources.UDTWebContent.UDTWCODE_VB1_51;
            LoadDetailedHistory = UDTSettingDAL.EndpointShowDetailedHistory;
        }
    }

    protected override Control GetResourceWrapperControl()
    {
        return Wrapper;
    }

    protected override void OnPreRenderResource(EventArgs e)
    {
        base.OnPreRenderResource(e);

        if (!string.IsNullOrEmpty(WarningMsg) && IsSearchTextChanged)
        {
            SearchInCachedData = false;
            ReloadData = true;
        }

        if (Data == null || Data.Rows.Count == 0)
        {
            Wrapper.Visible = !(NodeID > 0); // hide resouce if no data and on a Node view page
        }
    }

    protected override void HandleResourceWarning()
    {
        ExceptionWarning.Text = WarningMsg;
        ExceptionWarning.Visible = !string.IsNullOrEmpty(WarningMsg);
    }

    protected override DataTable LoadResourceData(string searchText)
    {
        DataTable data = null;
        try
        {
            data = UDTEndpointNetObject.GetAllNetworkData(TimePeriod, LoadDetailedHistory);
        }
        catch (Exception ex)
        {
            _log.ErrorFormat("GetNetworkConnectionsHistoryByEndpoint() [{0}] failed: {1}", UDTEndpointNetObject.Value, ex.Message);

            WarningMsg = Resources.UDTWebContent.UDTWEBCODE_VB1_60;
        }

        return data;
    }

    protected override void RenderResource()
    {
        if (Data == null || Data.Rows.Count == 0)
        {
            TotalRows = 0;
            return;
        }

        var data = ApplyFiltering(Data);

        if (data.Rows.Count > 1)
        {
            using (var view = new DataView(data))
            {
                view.Sort = "ConnectionType ASC, LastSeen DESC, EntryType ASC";
                data = view.ToTable();
            }
        }

        data = ApplyPaging(data);

        foreach (DataRow row in data.Rows)
        {
            var entryType = DALHelper.GetValueOrDefault(row["EntryType"], string.Empty);
            var isL3 = entryType == "L3";
            var isL2 = entryType == "L2";

            var ifID =  (row["InterfaceID"]==DBNull.Value)?0:Convert.ToInt32(row["InterfaceID"]);
            var ifName = DALHelper.GetValueOrDefault(row["InterfaceName"], string.Empty);
            var ifStatus = (row["InterfaceStatus"] == DBNull.Value) ? 0 : Convert.ToInt32(row["InterfaceStatus"]);

            var parentID = (row["ParentID"] == DBNull.Value) ? 0 : Convert.ToInt32(row["ParentID"]); 
            var parentName = DALHelper.GetValueOrDefault(row["ParentName"], string.Empty);
            var parentStatus = (row["ParentStatus"] == DBNull.Value) ? 0 : Convert.ToInt32(row["ParentStatus"]); 

            var firstSeen = DALHelper.GetValueOrDefault(row["FirstSeen"].ToString(), string.Empty);
            var lastSeen = (DateTime)row["LastSeen"] == DALHelper.SQLMaxDate ? Resources.UDTWebContent.UDTWCODE_VB1_50 : row["LastSeen"].ToString();

            var htmlRow = new HtmlTableRow();

            // time interval
            HtmlHelper.AddHtmlCell(htmlRow, (cell) => { cell.InnerHtml = string.Format(Resources.UDTWebContent.UDTWCODE_VB1_49, firstSeen, lastSeen); });
            
            if (isL2)
            {
                // port
                var statIcon = IconHelper.GetPortStatusIcon(ifStatus);
                var href = HtmlHelper.CreateUdtViewHref("port", ifID.ToString(CultureInfo.InvariantCulture), ifName);
                var portHtml = string.Format("{0} {1}", statIcon, ifName == "" ? Resources.UDTWebContent.UDTWEBCODE_TM0_7 : href);
                HtmlHelper.AddHtmlCell(htmlRow, (cell) => { cell.InnerHtml = portHtml; });

                // node
                statIcon = IconHelper.GetNodeStatusIcon(parentStatus, (int) parentID);
                href = HtmlHelper.CreateUdtViewHref("node", parentID.ToString(CultureInfo.InvariantCulture), parentName);
                var nodeHtml = string.Format("{0} {1}", statIcon, href);
                HtmlHelper.AddHtmlCell(htmlRow, (cell) => { cell.InnerHtml = nodeHtml; });
            }
            else if (isL3)
            {
                // port
                var statIcon = IconHelper.GetPortStatusIcon(UDTSettingDAL.ShowL3PortConnections ? ifStatus : 0);
                var href = HtmlHelper.CreateUdtViewHref("port", ifID.ToString(CultureInfo.InvariantCulture), String.Format(Resources.UDTWebContent.UDTWEBCODE_VB1_56, ifName));
                var portHtml = string.Format("{0} {1}", statIcon, ifName == "" || !UDTSettingDAL.ShowL3PortConnections ? Resources.UDTWebContent.UDTWEBCODE_VB1_57 : href);
                HtmlHelper.AddHtmlCell(htmlRow, (cell) => { cell.InnerHtml = portHtml; });

                // node
                statIcon = IconHelper.GetNodeStatusIcon(parentStatus, (int) parentID);
                href = HtmlHelper.CreateUdtViewHref("node", parentID.ToString(CultureInfo.InvariantCulture), parentName);
                var nodeHtml = string.Format("{0} {1}", statIcon, href);
                HtmlHelper.AddHtmlCell(htmlRow, (cell) => { cell.InnerHtml = nodeHtml; });
            }
            else
            {
                // ssid
                var clSSID = DALHelper.GetValueOrDefault(row["ClientSSID"], string.Empty);
                var statIcon = IconHelper.GetSSIDIcon(entryType);
                var href = HtmlHelper.CreateUdtViewHref("ssid", clSSID, clSSID);
                HtmlHelper.AddHtmlCell(htmlRow, (cell) => { cell.InnerHtml = String.Format("{0} {1}", statIcon, href); });
                
                // ap
                var apType = DALHelper.GetValueOrDefault(row["APType"], string.Empty);
                statIcon = IconHelper.GetWirelessIcon(parentStatus, apType);
                href = HtmlHelper.CreateUdtViewHref("ap", parentID.ToString(CultureInfo.InvariantCulture), parentName);
                HtmlHelper.AddHtmlCell(htmlRow, (cell) => { cell.InnerHtml = String.Format("{0} {1}", statIcon, href); });
            }

            // connection type
            var connectionType = DALHelper.GetValueOrDefault(row["ConnectionType"], 3);
            HtmlHelper.AddHtmlCell(htmlRow, (cell) => { cell.InnerHtml = new UdtConnectionType((EndpointConnectionType)connectionType).AsHtml; });

            EndpointHistoryConnections.Rows.Add(htmlRow);
        }
    }
    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IUDTEndpointProvider) }; }
    }
    protected override void HandleDefaultDataLoad(bool suppressDataReload)
    {
        ReloadData = true;
        base.HandleDefaultDataLoad(suppressDataReload);
    }

}
