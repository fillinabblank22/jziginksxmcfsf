﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EndpointDetails.ascx.cs" Inherits="Orion_UDT_Resources_EndpointDetails_EndpointDetails" %>
<%@ Register TagPrefix="orion" TagName="HelpLink" Src="~/Orion/Controls/HelpLink.ascx" %>
<%@ Import Namespace="SolarWinds.UDT.Web.Helpers" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
        
        <style type="text/css">
            .labelStyle { font-weight:bold; padding-left: 10px; }
            .VendorIconcss { font-size: 8pt !important; }
            .bigLabelStyle { font-weight:bold; padding-left: 10px; font-size: 14px !important; border-bottom: 1px solid #CCC; }
            
            .IpamVisible {display:block}
        
            #linksContent a { color: #336699; }
	        #linksContent a:hover {color:orange;}  
	
            a.iconLink
            {
                background-image: url(/Orion/Discovery/images/icon_add.gif);
                background-position: left center;
                background-repeat: no-repeat;
                padding: 2px 0px 2px 20px;
                font-size: 8pt;
            }
    
            a.helpLink 
            {
                color: #336699;
                font-size:8pt;
            }
    
            a.helpLink:hover
            {
                color:Orange;
            }
			td.udtManagementIcons a
			{
				float:none;
			}
        </style>
        
        <asp:UpdatePanel ID="updateEndpointPanel" runat="server">
            <ContentTemplate>  
               <table runat="server" id="EndpointDetailsTop" border="0" cellpadding="2" cellspacing="0" width="100%" visible="true" class="NeedsZebraStripes">
                    <tr runat="server" id="Management">
                       <td style="width:40%" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_152%></td>
                       <td class="Property NodeManagementIcons udtManagementIcons">
                           <asp:LinkButton runat="server" ID="AddNode" OnClick="AddNode_Click" ><image src="/Orion/UDT/Images/add_16x16.png" alt="" />&nbsp;<%= Resources.UDTWebContent.UDTWEBDATA_VB1_153%></asp:LinkButton>
                           <asp:LinkButton runat="server" ID="AddToWhiteList" OnClick="AddToWhiteList_Click" Visible="False"><image src="/Orion/UDT/Images/add_16x16.png" alt=""  />&nbsp;<%= Resources.UDTWebContent.UDTWEBDATA_MK1_68%></asp:LinkButton>
                           <asp:LinkButton runat="server" ID="ManageWhiteList" OnClick="ManageWhiteList_Click" Visible="False"><%= Resources.UDTWebContent.UDTWEBDATA_SL1_06%></asp:LinkButton>
                       </td>
                    </tr>
                     <tr>
                       <td class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_154%></td>
                      <td><%= GetRowData("DisplayType") %></td>
                    </tr>
                    <tr>
                       <td class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_155%></td>
                      <td><%= GetRowData("DisplayName")%>
                      </td>
                    </tr>
                    <tr>
                      <td class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_156%></td>
                      <td><%= GetRowData("LastSeen") %></td>
                    </tr>
                    <tr>
                      <td class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_157%></td>
                      <td><%= GetRowData("FirstSeen") %></td>
                    </tr>
                    <tr>
                      <td class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_158%></td>
                        <td><%= HtmlHelper.CreateUdtViewHref("dns", GetRowData("HostName"), GetRowData("HostName")) %>&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lblWatchHostName" OnClick="lblWatchHostName_Click" runat="server" Visible="false"><image src="/Orion/UDT/Images/detective_16x16.png" alt="" />&nbsp;<asp:Label runat="server" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_159 %>"/></asp:LinkButton></td>
                    </tr>
                    <tr>
                      <td class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_160%></td>
                      <td><%= HtmlHelper.CreateUdtViewHref("ip", GetRowData("IPAddress"),GetRowData("IPAddress")) %><asp:LinkButton ID="lblWatchIPAddress" OnClick="lblWatchIPAddress_Click" runat="server" Visible="false"><image src="/Orion/UDT/Images/detective_16x16.png" alt="" />&nbsp;<asp:Label runat="server" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_161 %>"/></asp:LinkButton></td>
                    </tr>
                    <tr>
                      <td class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_162%></td>
                      <td><%= HtmlHelper.CreateUdtViewHref("mac", GetRowData("MACAddress"),GetRowData("MACAddress")) %>&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbStopWatchMACAddress" OnClick="lblStopWatchMACAddress_Click" runat="server" Visible="false"><image src="/Orion/UDT/Images/remove_filter_16x16.png" alt="" />&nbsp;<asp:Label runat="server" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_163 %>"/></asp:LinkButton></td>
                    </tr>
                    <tr runat ="server" id="tablerowVendorIcon">
                       <td class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_104%></td>
                      <td><%= VendorIconForMAC(GetRowData("MACAddress"))%> </td>
                    </tr>
                   <tr runat ="server" id="tablerowIpamDetails">
                       <td style="width:100%;padding-top: 15px; border-bottom: 0px;" colspan="2" ><p class="bigLabelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_GM1_241%></p>
                           <table runat="server" id="IpamDetails" border="0" cellpadding="2" cellspacing="0" width="100%" visible="true" class="NeedsZebraStripes">
                               <tr>
                                   <%--Subnet--%>
                                  <td class="labelStyle" style="padding-left:2em; width:40%" ><%= Resources.UDTWebContent.UDTWEBDATA_GM1_242%></td>
                                  <td><%= GetHTMLTag(GetRowData("Subnet"),IPAMPropertyType.Subnet)%></td>
                               </tr>
                               <tr>
                                   <%--Mask--%>
                                   <td class="labelStyle" style="padding-left:2em" ><%= Resources.UDTWebContent.UDTWEBDATA_GM1_243%></td>
                                   <td><%= GetRowData("Mask") %></td>
                                </tr>
                               <tr>
                                   <%--Type--%>
                                   <td class="labelStyle" style="padding-left:2em" ><%= Resources.UDTWebContent.UDTWEBDATA_GM1_244%></td>
                                   <td><%= GetRowData("Type") %></td>
                               </tr>
                                <tr>
                                   <%--IP Status--%>
                                   <td class="labelStyle" style="padding-left:2em" ><%= Resources.UDTWebContent.UDTWEBDATA_GM1_248%></td>
                                   <td><%= GetHTMLTag(GetRowData("IPStatus"), IPAMPropertyType.IPStatus) %></td>
                                </tr>
                               <tr runat ="server" id="tablerowConflictIcon">
                                   <%--Conflicting IP/IP--%>
                                   <td class="labelStyle" style="padding-left:2em" ><%=GetIPCaption()%></td>
                                   <td><%= GetHTMLTag(GetRowData("IPAddress"), IPAMPropertyType.IP)%> </td>
                               </tr>
                              
                           </table>
                       </td>
                    </tr>
                    <tr runat="server" id="tablerowIpamNA">
                       <td colspan="2" style="text-align:center">
                            <%= Resources.UDTWebContent.UDTWEBDATA_GM1_246 %> 
                            <orion:HelpLink ID="UDTIpamDataHelpLink" runat="server" HelpUrlFragment="OrionUDTPHdetailsUDTandIPAM" HelpDescription="<%$ Resources : UDTWebContent, UDTWEBDATA_GM1_247 %>" CssClass="helpLink" />
                        </td>
                    </tr>
               </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </Content>
</orion:resourceWrapper>
