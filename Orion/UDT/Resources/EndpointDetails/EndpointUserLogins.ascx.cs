﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SolarWinds.Logging;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Web.Providers;
using SolarWinds.UDT.Web.Resources;
using SolarWinds.UDT.Web.NetObjects;

public partial class Orion_UDT_Resources_EndpointUserLogins : UDTBaseResourceControl
{
    private static readonly Log _log = new Log("EndpointUserLogins");
    protected override Log GetLogger() { return _log; }

    private static readonly string[] ResourceSearchabelColumns = new[] { "AccountName" };


    public UDTEndpointNetObject UdtEndpointNetObject
    {
        get
        {
            var udtEndpointProvider = GetInterfaceInstance<IUDTEndpointProvider>();
            if (udtEndpointProvider != null)
            {
                return udtEndpointProvider.UDTEndpointNetObject;
            }
            return null;
        }
    }

    protected override String DefaultTitle
    {
        get { return Resources.UDTWebContent.UDTWEBCODE_VB1_63; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "orionUDTPHEndpointUserLogins";
        }
    }

    public int NodeID
    {
        get
        {
            int id;
            // check if we have a node id available
            try
            {
                id = ((INodeProvider)GetInterfaceInstance(typeof(INodeProvider))).Node.NodeID;
            }
            catch (Exception ex)
            {
                id = 0;
            }
            return id;
        }
    }

    public override string EditURL
    {
        get { return string.Format("/Orion/UDT/Resources/EndpointDetails/EditEndpointUserLogins.aspx?ResourceID={0}&ViewID={1}&NetObjectID={2}", this.Resource.ID, this.Resource.View.ViewID, HttpUtility.UrlDecode(UdtEndpointNetObject != null ? UdtEndpointNetObject.NetObjectID : string.Empty)); }
    }

    protected override Control GetResourceWrapperControl()
    {
        return Wrapper;
    }

    protected override void OnInitResource(EventArgs e)
    {
        Wrapper.ManageButtonText = Resources.UDTWebContent.UDTWEBDATA_GK1_1;
        Wrapper.ManageButtonTarget = "~/Orion/UDT/ManageDomainControllers.aspx";
        Wrapper.ShowManageButton = true;
        base.OnInitResource(e);

        SearchableColumns = ResourceSearchabelColumns;
    }

    protected override DataTable LoadResourceData(string searchText)
    {
        if (UdtEndpointNetObject != null)
            return UdtEndpointNetObject.GetAllUserLoginData(searchText);
        else
            return UDTUserDAL.GetLatestUserLogins(string.Empty, string.Empty, searchText);
    }

    protected override void RenderResource()
    {
        if (IsPostBack && (Data == null || Data.Rows.Count == 0) && (UDTDeviceDAL.GetDcCount() <= 0))
        {
            EndpointUserLogins.Style.Add("display", "none");
            ResourcePager.Visible = false;
            ResourceSearcher.Visible = false;
            AllUserLoginsGetStarted.Style.Add("display", "block");
            return;
        }
        var data = ApplyFiltering(Data);
        data = ApplyPaging(data);
        //build the table

        foreach (DataRow row in data.Rows)
        {
            var htmlRow = new HtmlTableRow();

            if (row["LogonDateTime"] != DBNull.Value)
            {
                //Endpoint
                var htmlCell = new HtmlTableCell();
                htmlCell.Attributes.Add("Class", "Property");
                var userName = Convert.ToString(row["AccountName"]);
                htmlCell.InnerHtml = string.IsNullOrEmpty(userName)
                    ? Resources.UDTWebContent.UDTWEBCODE_VB1_64
                    : String.Format("&nbsp;<a href='/Orion/UDT/UserDetails.aspx?NetObject=UU:{0}'>{1}</a>", row["UserID"], userName);
                htmlRow.Cells.Add(htmlCell);

                //Logon time
                htmlCell = new HtmlTableCell();
                htmlCell.Attributes.Add("Class", "Property");
                var logonDateTime = Convert.ToString(row["LogonDateTime"]);
                htmlCell.InnerHtml = String.Format("&nbsp;{0}", logonDateTime);
                htmlRow.Cells.Add(htmlCell);
            }

            EndpointUserLogins.Rows.Add(htmlRow);
        }
    }

}



