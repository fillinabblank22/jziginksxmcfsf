﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/OrionResourceEdit.master" CodeFile="EditEndpointHistoricalConnections.aspx.cs" Inherits="Orion_UDT_Resources_EndpointDetails_EditEndpointHistoricalConnections" %>
<%@ Import Namespace="SolarWinds.Orion.Web" %>
<%@ Import Namespace="SolarWinds.UDT.Web.Helpers" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" Assembly="OrionWeb" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= String.Format(Resources.UDTWebContent.UDTWEBDATA_VB1_135, CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Resource.Title, Request["NetObject"].UrlEncode()))) %></h1>
       
    <div>
        <orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="false" ShowSubTitleHintMessage="false" />
        <%= Resources.UDTWebContent.UDTWEBDATA_VB1_149%>
        <br /><br /><b><asp:Label runat="server" ID="label1" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_151 %>" /></b>
        <br /><br />
        <asp:DropDownList runat="server" ID="Period" />
        <br /><br />
        <b><%= Resources.UDTWebContent.UDTWEBCODE_VB1_61%></b>
        <br /><br />
        <asp:TextBox runat="server" ID="maxRowsCount" Columns="5"/>
        <br /><br />
        <asp:CheckBox runat="server" ID="chbxShowDetailedHistory" /><b><%= Resources.UDTWebContent.UDTWEBDATA_VB1_150%></b>
        <br /><br />
        <orion:LocalizableButton runat="server" ID="btnSubmit" OnClick="SubmitClick" LocalizedText="Submit" DisplayType="Primary"/>
        <br /><br />
    </div>
</asp:Content>
