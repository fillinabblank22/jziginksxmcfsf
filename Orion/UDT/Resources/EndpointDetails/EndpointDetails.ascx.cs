﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Resources;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.UDT.Common;
using SolarWinds.UDT.Common.Models.Rules;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Web.Helpers;
using SolarWinds.UDT.Web.NetObjects;
using SolarWinds.UDT.Web.Providers;

public partial class Orion_UDT_Resources_EndpointDetails_EndpointDetails : BaseResourceControl
{
    
    private DataRow _dataRow;
    private bool IsConflict;


    protected enum IPAMPropertyType
    {
        IP = 1,
        IPStatus = 2,
        Subnet = 3
    }

    public UDTEndpointNetObject UDTEndpointNetObject 
    {
        get
        {
            var udtEndpointProvider = GetInterfaceInstance<IUDTEndpointProvider>();
            if (udtEndpointProvider != null)
            {
                return udtEndpointProvider.UDTEndpointNetObject;
            }
            return null;
        }
    }

    private DataRow DataRow
    {
        get { return _dataRow ?? (_dataRow = UDTEndpointNetObject.GetEndpointData()); }
    }

    protected string GetRowData(string key)
    {
        var value = DataRow[key] ?? string.Empty;
        return value.ToString().UrlDecode();
    }
    
    protected override void OnInit(EventArgs e)
    {
        // if resource is not on the Endpoint view, so hide. In different words, if resource host of view does not implement IUDTEndpointProvider.
        // NOTE: Only 'UDT EndpointDetails' view and views which fullfills view bridging requirements expose this provider.
        if (UDTEndpointNetObject == null)
        {
            Wrapper.Visible = false;
            return;
        }
        tablerowVendorIcon.Visible = !String.IsNullOrEmpty(GetRowData("MACAddress"));
        
        var ipAddress = GetRowData("IPAddress");
        if (UDTIPAMIntegration.IsIPAMInstalled())
        {
            if (String.IsNullOrEmpty(GetRowData("Subnet")))
            {
                IpamDetails.Visible = false;
                tablerowIpamNA.Visible = true;
            }
            else
            {
                IpamDetails.Visible = true;
                tablerowIpamNA.Visible = false;
            }
            
            Boolean.TryParse(GetRowData("IsConflict"), out IsConflict);
            //tablerowConflictIcon.Visible = IsConflict;
        }
        else
        {
            tablerowIpamNA.Visible = false;
            tablerowIpamDetails.Visible = false;
        }
        
        if (String.IsNullOrEmpty(ipAddress) || OrionConfiguration.IsDemoServer || !Profile.AllowAdmin)
        {
            AddNode.Visible = false;
        } 
        else
        {
            System.Data.DataTable results;
            using (var swis = SolarWinds.Orion.Web.InformationService.InformationServiceProxy.CreateV3())
            {
                var queryParams = new Dictionary<string, object>();
                queryParams[@"ipAdr"] = ipAddress;
                string swql = "SELECT count(NodeID) as cnt FROM Orion.Nodes WHERE IPAddress=@ipAdr";
                results = swis.Query(swql, queryParams);
            }
            AddNode.Visible = ((int)results.Rows[0][0] == 0);
        }

        UDTEndpointIsRogue isRogue = UDTEndpointDAL.CheckWhiteListEndPoint(UDTEndpointNetObject.NetObjectType, UDTEndpointNetObject.Value);
        if (isRogue == UDTEndpointIsRogue.IsRogue)
            AddToWhiteList.Visible = true;
        else
        {
            ManageWhiteList.Visible = isRogue == UDTEndpointIsRogue.IsNotRogue;
        }

        if (OrionConfiguration.IsDemoServer)
        {
            AddToWhiteList.Enabled = false;
            AddToWhiteList.Style["color"] = "gray";
        }

        if (!Profile.AllowAdmin && !OrionConfiguration.IsDemoServer)
            AddToWhiteList.Visible = false;

        Management.Visible = AddNode.Visible || AddToWhiteList.Visible || ManageWhiteList.Visible; // or this or that

        base.OnInit(e);
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        //lblWatchHostName.Visible = string.IsNullOrEmpty();
    }


    protected void AddNode_Click(object sender, EventArgs e)
    {
        var engineID = EngineDAL.GetMyEngineId();
        var ipAddress = GetRowData("IPAddress");
        var url = "/Orion/Nodes/Add/Default.aspx?EngineID=" + engineID + "&IPAddress=" + ipAddress;
        Response.Redirect(url);
    }

    protected void AddToWhiteList_Click(object sender, EventArgs e)
    {
        var endpoint = UDTEndpointNetObject.Value;
        var type = UDTEndpointNetObject.NetObjectType;

        var url = String.Format("/Orion/UDT/Admin/WhiteList/Wizard/Default.aspx?edit=0&endpoint={0}&endpointType={1}&type={2}", endpoint, type, Convert.ToInt16(UDTRuleType.Whitelist));
        
        Response.Redirect(url);
    }

    protected void lblWatchHostName_Click(object sender, EventArgs e)
    {
        
        // Here will be execution code for Watch Hostname
    }

    protected void lblStopWatchMACAddress_Click(object sender, EventArgs e)
    {
        // Here will be execution code for Stop watching MAC Address
    }

    protected void lblWatchIPAddress_Click(object sender, EventArgs e)
    {
        // Here will be execution code for Watch IP Address
    }

    protected override string DefaultTitle
    {
        get { return UDTWebContent.UDTWEBCODE_VB1_62; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionUDTPHEndpointDetails"; }
    }

    /*public override string EditURL
    {
        get { return string.Format("/Orion/UDT/Resources/EndpointDetails/EditEndpointDetails.aspx?ResourceID={0}&ViewID={1}&NetObjectID={2}", this.Resource.ID, this.Resource.View.ViewID, HttpUtility.UrlDecode(Request.QueryString["NetObject"])); }
    }*/

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IUDTEndpointProvider) }; }
    }

    protected void ManageWhiteList_Click(object sender, EventArgs e)
    {
        var url = String.Format("/Orion/UDT/Admin/WhiteList/ManageWhiteList.aspx?testText={0}",
                                UDTEndpointNetObject.Value);
        Response.Redirect(url);
    }

    protected string VendorIconForMAC(string mac)
    {
        if (!String.IsNullOrEmpty(mac))
        {
            return IconHelper.GetVendorIcon(mac, false);

        }
        else
        {
            return "";
        }
    }

    protected string GetIPCaption()
    {
        if (IsConflict)
            return Resources.UDTWebContent.UDTWEBDATA_GM1_245;
        else
            return Resources.UDTWebContent.UDTWEBDATA_GM1_249;
    }

    protected string GetHTMLTag(string displayData, IPAMPropertyType type )
    {
        string retText = "";
       
            switch (type)
            {
                case IPAMPropertyType.IP:
                    if (IsConflict)
                        retText = IconHelper.GetIPAMLinkIconTag(GetRowData("IPAddress"),
                            GetRowData("IPAddressUrl"), GetRowData("ConflictIcon"));
                    else
                        retText = IconHelper.GetIPAMLinkTag(GetRowData("IPAddress"),
                            GetRowData("IPAddressUrl"));
                    break;
                case IPAMPropertyType.IPStatus:
                    retText = IconHelper.GetIPAMNonLinkIconTag(GetRowData("IPStatus"),
                        GetRowData("IPStatusIcon"));
                    break;
                case IPAMPropertyType.Subnet:
                    retText = IconHelper.GetIPAMLinkIconTag(GetRowData("Subnet"),
                      GetRowData("SubnetUrl"), GetRowData("SubnetStatusIcon"));
                    break;
            }
           
       
       
        return retText;
    }
}
