﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.UDT.Web.DAL;

public partial class Orion_UDT_Resources_EndpointDetails_EditEndpointHistoricalConnections : System.Web.UI.Page
{
    protected ResourceInfo Resource { get; set; }

    protected string NetObjectID { get; set; }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        foreach (string period in Periods.GenerateSelectionList())
            Period.Items.Add(new ListItem(Periods.GetLocalizedPeriod(period), period));
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        NetObjectID = Request.QueryString["NetObjectID"];

        if (!String.IsNullOrEmpty(Request.QueryString["ResourceID"]))
        {
            int resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
            Resource = ResourceManager.GetResourceByID(resourceID);
            Title = String.Format(Resources.UDTWebContent.UDTWEBDATA_VB1_134, Resource.Title);
        }

        if (!IsPostBack)
        {
            int max;
            if (!Int32.TryParse(Resource.Properties["MaxRowsCount"], out max))
            {
                Resource.Properties["MaxRowsCount"] = "10";
            }
            maxRowsCount.Text = Resource.Properties["MaxRowsCount"];

            Period.Text = Resource.Properties["Period"];
            
            resourceTitleEditor.ResourceTitle = !string.IsNullOrEmpty(Resource.Properties["Title"]) ? Resource.Properties["Title"] : string.Empty;

            chbxShowDetailedHistory.Checked = Resource.Properties.ContainsKey("ShowDetailedHistory")
                                                  ? bool.Parse(Resource.Properties["ShowDetailedHistory"])
                                                  : UDTSettingDAL.EndpointShowDetailedHistory;
        }
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        Resource.Properties["Title"] = resourceTitleEditor.ResourceTitle;
        //this.Resource.Properties["SubTitle"] = this.resourceTitleEditor.ResourceSubTitle;
        
        Resource.Title = resourceTitleEditor.ResourceTitle;

        try { Resource.Properties["MaxRowsCount"] = ValidateTextBox(maxRowsCount.Text, Resource.Properties["MaxRowsCount"]); }
        catch { Resource.Properties["MaxRowsCount"] = "10"; }
        
        Resource.Properties["Period"] = Period.Text;
        
        Resource.Properties["ShowDetailedHistory"] = chbxShowDetailedHistory.Checked.ToString(CultureInfo.InvariantCulture);
        
        SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
        
        Response.Redirect(String.Format("/Orion/View.aspx?ViewID={0}&NetObject={1}", Resource.View.ViewID, NetObjectID));
    }

    protected string ValidateTextBox(string newValue, string currentValue)
    {
        int value;
        if (int.TryParse(newValue, out value))
        {
            if (value > 100)
                value = 100;

            if (value < 1)
                value = 1;

            return value.ToString(CultureInfo.InvariantCulture);
        }

        return currentValue;
    }
}