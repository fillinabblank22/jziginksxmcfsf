﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserDetails.ascx.cs" Inherits="Orion_UDT_Resources_UserDetails" %>


<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
        <style type="text/css">
            .labelStyle { font-weight:bold; padding-left: 10px; }
        </style>
        <asp:UpdatePanel ID="updateUserPanel" runat="server">
            <ContentTemplate>            
                <%--user details table--%>
                <table runat="server" id="UserDetailsTop" border="0" cellpadding="2" cellspacing="0" width="100%" visible="true" class="NeedsZebraStripes">
                    <tr>
                       <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_170 %></td><td><%=UdtUser.AccountName%>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_171 %></td><td><%=UdtUser.FirstName%>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_172 %></td><td><%=UdtUser.LastName%>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_173%></td><td><%=UdtUser.Title%>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_174%></td><td><%=UdtUser.Department%>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_175%></td><td><%=UdtUser.Office%>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_176%></td><td><%=UdtUser.Company%>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_177%></td><td><%=UdtUser.Manager%>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_178%></td><td><%=UdtUser.Assistant%>&nbsp;</td>
                    </tr>
                </table>

                <asp:Repeater runat="server" id="EmailAddressesDetails">
                    <HeaderTemplate>
                        <table border="0" cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td width="200" class="labelStyle"><%# Container.ItemIndex == 0 ? Resources.UDTWebContent.UDTWEBDATA_VB1_179 : "&nbsp;"%></td>
                            <td><%#FormatEmailAddresses(Eval("EmailAddress").ToString())%></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table> 
                    </FooterTemplate>
                </asp:Repeater>

                <table runat="server" id="EmailAddressesLinks" border="0" cellpadding="2" cellspacing="0" width="100%" visible="false" class="NeedsZebraStripes">
                    <tr>
                        <td width="200" class="labelStyle">&nbsp;</td>
                        <td><asp:LinkButton ID="showEmailAddressesLink" runat="server" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_180 %>" onclick="showEmailAddressesClick" visible="false" /><asp:LinkButton ID="hideEmailAddressesLink" runat="server" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_181 %>" onclick="hideEmailAddressesClick" visible="false" /></td>
                    </tr>
                </table>

                <asp:Repeater runat="server" id="MemberOfDetails">
                    <HeaderTemplate>
                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td width="200" class="labelStyle"><%# Container.ItemIndex == 0 ? Resources.UDTWebContent.UDTWEBDATA_VB1_182 : "&nbsp;"%></td>
                            <td><%#FormatMemberOf(Eval("MemberOf").ToString())%></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table> 
                    </FooterTemplate>
                </asp:Repeater>
                
                <table runat="server" id="MemberOfLinks" border="0" cellpadding="2" cellspacing="0" width="100%" visible="false" class="NeedsZebraStripes" >
                    <tr>
                        <td width="200" class="labelStyle">&nbsp;</td>
                        <td><asp:LinkButton ID="showMemberOfLink" runat="server" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_180 %>" onclick="showMemberOfClick" visible="false" /><asp:LinkButton ID="hideMemberOfLink" runat="server" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_181 %> " onclick="hideMemberOfClick" visible="false" /></td>
                    </tr>
                </table>

                <table runat="server" id="UserDetailsBottom" border="0" cellpadding="2" cellspacing="0" width="100%" visible="true" class="NeedsZebraStripes">
                    <tr>
                        <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_183 %></td><td><%=UdtUser.Phone%>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_184 %></td><td><%=UdtUser.Address%>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_185%></td><td><%=UdtUser.City%>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_186%></td><td><%=UdtUser.State%>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_187%></td><td><%=UdtUser.ZipCode%>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_188%></td><td><%=UdtUser.CountryRegion%>&nbsp;</td>
                    </tr>
                     <tr>
                        <td width="200" class="labelStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_189%></td><td><%=UdtUser.PrimaryGroup%>&nbsp;</td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
	</Content>
</orion:resourceWrapper>
