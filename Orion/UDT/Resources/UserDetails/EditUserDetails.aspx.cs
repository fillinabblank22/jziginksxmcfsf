﻿using System;
using System.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_UDT_Resources_UserDetails_EditUserDetails  : System.Web.UI.Page
{
    private ResourceInfo resource;

    protected ResourceInfo Resource
    {
        get { return this.resource; }
        set { this.resource = value; }
    }
 

    private string netObjectID;

    protected string NetObjectID
    {
        get { return this.netObjectID; }
        set { this.netObjectID = value; }
    }
    

    protected void Page_Load(object sender, EventArgs e)
    {
        NetObjectID=Request.QueryString["NetObjectID"];

        if (!String.IsNullOrEmpty(this.Request.QueryString["ResourceID"]))
        {
            int resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
            this.Resource = ResourceManager.GetResourceByID(resourceID);
            this.Title = String.Format(Resources.UDTWebContent.UDTWEBDATA_VB1_134, Resource.Title);
        }

        if (!this.IsPostBack)
        {
            int max;
            if (!Int32.TryParse(this.Resource.Properties["MaxEmailAddressesCount"], out max))
            {
                this.Resource.Properties["MaxEmailAddressesCount"] = "1";
            }
            this.maxEmailAddressesCount.Text = this.Resource.Properties["MaxEmailAddressesCount"];

            if (!Int32.TryParse(this.Resource.Properties["MaxMemberOfCount"], out max))
            {
                this.Resource.Properties["MaxMemberOfCount"] = "5";
            }
            this.maxMemberOfCount.Text = this.Resource.Properties["MaxMemberOfCount"];

            //if (!string.IsNullOrEmpty(Resource.Properties["Title"]))
            //{
            //    this.resourceTitleEditor.ResourceTitle = Resource.Properties["Title"];
            //}
            //else
            //{
            //    this.resourceTitleEditor.ResourceTitle = string.Empty;
            //}

            //if (!string.IsNullOrEmpty(Resource.Properties["SubTitle"]))
            //{
            //    this.resourceTitleEditor.ResourceSubTitle = Resource.Properties["SubTitle"];
            //}
            //else
            //{
            //    this.resourceTitleEditor.ResourceSubTitle = string.Empty;
            //}
        }

    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        //this.Resource.Properties["Title"] = this.resourceTitleEditor.ResourceTitle;
        //this.Resource.Properties["SubTitle"] = this.resourceTitleEditor.ResourceSubTitle;
        this.Resource.Properties["MaxEmailAddressesCount"] = validateTextBox(maxEmailAddressesCount.Text, this.Resource.Properties["MaxEmailAddressesCount"]);
        this.Resource.Properties["MaxMemberOfCount"] = validateTextBox(maxMemberOfCount.Text, this.Resource.Properties["MaxMemberOfCount"]);
        
        Response.Redirect(String.Format("/Orion/View.aspx?ViewID={0}&NetObject={1}", this.Resource.View.ViewID, NetObjectID));
    }

    protected string validateTextBox(string txtValue, string currentValue)
    {
        int newValue;
        string returnValue;
        bool result = int.TryParse(txtValue, out newValue);

        if (result)
        {
            if (newValue > 100)
            {
                newValue = 100;
            }

            if (newValue < 1)
            {
                newValue = 1;
            }
            returnValue = newValue.ToString();
        }
        else
        {
            returnValue = currentValue;
        }
        return returnValue;
    }
}
