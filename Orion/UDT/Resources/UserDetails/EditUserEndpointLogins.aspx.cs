﻿using System;
using System.Collections.Generic;
using System.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web;


public partial class Orion_UDT_Resources_UserDetails_EditUserEndpointLogins : System.Web.UI.Page
{
    private ResourceInfo resource;

    protected ResourceInfo Resource
    {
        get { return this.resource; }
        set { this.resource = value; }
    }
 

    private string netObjectID;

    protected string NetObjectID
    {
        get { return this.netObjectID; }
        set { this.netObjectID = value; }
    }


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        NetObjectID=Request.QueryString["NetObjectID"];

        if (!String.IsNullOrEmpty(this.Request.QueryString["ResourceID"]))
        {
            int resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
            this.Resource = ResourceManager.GetResourceByID(resourceID);
            this.Title = String.Format(Resources.UDTWebContent.UDTWEBDATA_VB1_134, Resource.Title);
        }

        if (!this.IsPostBack)
        {
            int max;
            if (!Int32.TryParse(this.Resource.Properties["MaxRowsCount"], out max))
            {
                this.Resource.Properties["MaxRowsCount"] = "10";
            }
            this.maxRowsCount.Text = this.Resource.Properties["MaxRowsCount"];
        }

    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        this.Resource.Properties["MaxRowsCount"] = validateTextBox(maxRowsCount.Text, this.Resource.Properties["MaxRowsCount"]);
        Response.Redirect(String.Format("/Orion/View.aspx?ViewID={0}&NetObject={1}", this.Resource.View.ViewID, NetObjectID));
    }

    protected  string validateTextBox(string txtValue, string currentValue)
    {
        int newValue;
        string returnValue;
        bool result = int.TryParse(txtValue, out newValue);

        if (result)
        {
            if (newValue > 100)
            {
                newValue = 100;
            }

            if (newValue < 1)
            {
                newValue = 1;
            }
            returnValue = newValue.ToString();
        }
        else
        {
            returnValue = currentValue;
        }
        return returnValue;
    }
}
