﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SolarWinds.Orion.Web;
using SolarWinds.UDT.Web.NetObjects;
using SolarWinds.UDT.Web.Providers;



public partial class Orion_UDT_Resources_UserDetails: SolarWinds.Orion.Web.UI.BaseResourceControl
{
    private static SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();

    private int _showNumberOfEmailAddresses = 1;
    private int _maxNumberOfEmailAddresses = 1;
    private int _showNumberOfMemberOfs = 5;
    private int _maxNumberOfMemberOfs = 5;

    protected UDTUserNetObject UdtUser { get; set; }

    protected override String DefaultTitle
    {
        get { return Resources.UDTWebContent.UDTWEBCODE_VB1_65; }
    }

    private int ShowNumberOfEmailAddresses
    {
        get
        {
            if (ViewState["UDT_UserDetailsResource_ShowNumberOfEmailAddresses"] != null) _showNumberOfEmailAddresses = (int)ViewState["UDT_UserDetailsResource_ShowNumberOfEmailAddresses"];
            return _showNumberOfEmailAddresses;
        }
        set
        {
            _showNumberOfEmailAddresses = value;
            ViewState["UDT_UserDetailsResource_ShowNumberOfEmailAddresses"] = value;
        }
    }

    private int ShowNumberOfMemberOfs
    {
        get
        {
            if (ViewState["UDT_UserDetailsResource_ShowNumberOfMemberOfs"] != null) _showNumberOfMemberOfs = (int)ViewState["UDT_UserDetailsResource_ShowNumberOfMemberOfs"];
            return _showNumberOfMemberOfs;
        }
        set
        {
            _showNumberOfMemberOfs = value;
            ViewState["UDT_UserDetailsResource_ShowNumberOfMemberOfs"] = value;
        }
    }

    protected String UserName
    {
        get
        {
            string tmpUserName = UdtUser.Name;
            if (string.IsNullOrEmpty(tmpUserName))
            {
                tmpUserName = Resources.UDTWebContent.UDTWEBCODE_VB1_66;
            }
            return tmpUserName;
        }
    }


    public override string HelpLinkFragment
    {
        get
        {
            return "OrionUDTPHUserDetails";
        }
    }


    public int UserID
    {
        get
        {
            int userId;

            try
            {
                string tmpPort = HttpUtility.UrlDecode(Request.QueryString["NetObject"]);
                string[] netObjectParts = tmpPort.Split(':');
                userId = int.Parse(netObjectParts[1]);
            }
            catch (Exception)
            {
                userId = 0;
            }
            return userId;
        }
    }


    private void UpdateFromResourceProps()
    {
        if (!Int32.TryParse(this.Resource.Properties["MaxEmailAddressesCount"], out _maxNumberOfEmailAddresses))
        {
            _maxNumberOfEmailAddresses = 5;
        }
        if (ShowNumberOfEmailAddresses != -1) ShowNumberOfEmailAddresses = _maxNumberOfEmailAddresses;
        

        if (!Int32.TryParse(this.Resource.Properties["MaxMemberOfCount"], out _maxNumberOfMemberOfs))
        {
            _maxNumberOfMemberOfs = 5;
        }
        if (ShowNumberOfMemberOfs != -1) ShowNumberOfMemberOfs = _maxNumberOfMemberOfs;
    }
    
    
    public override string EditURL
    {
        get { return string.Format("/Orion/UDT/Resources/UserDetails/EditUserDetails.aspx?ResourceID={0}&ViewID={1}&NetObjectID={2}", this.Resource.ID, this.Resource.View.ViewID, HttpUtility.UrlDecode(Request.QueryString["NetObject"])); }
    }


    protected override void OnInit(EventArgs e)
    {

        //Bind to netobject (UDT User)
        var nObject = Request["NetObject"];
        if (nObject == null)
        {
            nObject = "UU:15555"; //we're doing this so the preview works on the customize page, page
        }
        this.UdtUser = NetObjectFactory.Create(nObject, true) as UDTUserNetObject;

        base.OnInit(e);
    }



    protected override void OnLoad(EventArgs e)
    {
        //Update item counts from resource properties
        UpdateFromResourceProps();
        
        PopulateEmailAddresses();
        PopulateMemberOfs();
    }
    
    

    private void PopulateEmailAddresses()
    {
        string[] emailAddresses = UdtUser.EmailAddressList.Split(',');

        int recCount = ShowNumberOfEmailAddresses;
        if (recCount == -1) recCount = emailAddresses.Length;
        

        EmailAddressesDetails.DataSource = emailAddresses.Select(n => new { EmailAddress = n }).Take(recCount);
        EmailAddressesDetails.DataBind();

        if ((emailAddresses.Length > recCount) && (emailAddresses.Length != _maxNumberOfEmailAddresses))
        {
            showEmailAddressesLink.Visible = true;
            hideEmailAddressesLink.Visible = false;
            EmailAddressesLinks.Visible = true;
        }
        else if ((emailAddresses.Length == recCount) && (emailAddresses.Length != _maxNumberOfEmailAddresses))
        {
            showEmailAddressesLink.Visible = false;
            hideEmailAddressesLink.Visible = true;
            EmailAddressesLinks.Visible = true;
        }
        else
        {
            EmailAddressesLinks.Visible = false;
        }
    }


    private void PopulateMemberOfs()
    {
        //Here including the Primaty group into Memberof List.
        string[] member = UdtUser.MemberOfList.Split(',');
        string[] memberOfs = new string[member.Length + 1];
        for (int i = 0; i < member.Length; i++)
        {
            memberOfs[i] = member[i];
        }
        memberOfs.SetValue(UdtUser.PrimaryGroup, memberOfs.Length - 1);
        int recCount = ShowNumberOfMemberOfs;


        if (recCount == -1) recCount = memberOfs.Length;

        MemberOfDetails.DataSource = memberOfs.Select(n => new { MemberOf = n }).Take(recCount);
        MemberOfDetails.DataBind();

        if ((memberOfs.Length > recCount) && (memberOfs.Length != _maxNumberOfMemberOfs))
        {
            showMemberOfLink.Visible = true;
            hideMemberOfLink.Visible = false;
            MemberOfLinks.Visible = true;
        }
        else if ((memberOfs.Length == recCount) && (memberOfs.Length != _maxNumberOfMemberOfs))
        {
            showMemberOfLink.Visible = false;
            hideMemberOfLink.Visible = true;
            MemberOfLinks.Visible = true;
        }
        else
        {
            MemberOfLinks.Visible = false;
        }
    }



    public string FormatEmailAddresses(string emailAddress)
    {
        return StringEmptyToNBSP(emailAddress);
    }

    public string FormatMemberOf(string memberOf)
    {
        return StringEmptyToNBSP(memberOf);
    }


    private string StringEmptyToNBSP(string cellContents)
    {
        if (String.IsNullOrEmpty(cellContents))
        {
            return "&nbsp;";
        }
        return cellContents;
    }

    protected void showEmailAddressesClick(object sender, EventArgs e)
    {
        ShowNumberOfEmailAddresses = -1;
        PopulateEmailAddresses();
    }

    protected void hideEmailAddressesClick(object sender, EventArgs e)
    {
        ShowNumberOfEmailAddresses = _maxNumberOfEmailAddresses;
        PopulateEmailAddresses();
    }

    protected void showMemberOfClick(object sender, EventArgs e)
    {
        ShowNumberOfMemberOfs = -1; 
        PopulateMemberOfs();
    }

    protected void hideMemberOfClick(object sender, EventArgs e)
    {
        ShowNumberOfMemberOfs = _maxNumberOfMemberOfs;
        PopulateMemberOfs();
    }


    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IUDTUserProvider) }; }
    }
}

