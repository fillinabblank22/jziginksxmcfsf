﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SolarWinds.Logging;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Web.Helpers;
using SolarWinds.UDT.Web.Providers;
using SolarWinds.UDT.Web.Resources;

public partial class Orion_UDT_Resources_UserEndpointLogins : UDTBaseResourceControl
{
    private static readonly Log _log = new Log("UserEndpointLogins");
    protected override Log GetLogger() { return _log; }

    private static readonly string[] ResourceSearchabelColumns = new[] { "EndpointValue" };

    protected override String DefaultTitle
    {
        get { return Resources.UDTWebContent.UDTWEBCODE_VB1_67; }
    }
    
    public override string HelpLinkFragment
    {
        get { return "OrionUDTPHUserEndpointLogins"; }
    }

    public int UserID
    {
        get
        {
            int userID;
            return int.TryParse(NetObject.Value, out userID) ? userID : 0;
        }
    }

    public override string EditURL
    {
        get { return string.Format("/Orion/UDT/Resources/UserDetails/EditUserEndpointLogins.aspx?ResourceID={0}&ViewID={1}&NetObjectID={2}", this.Resource.ID, this.Resource.View.ViewID, HttpUtility.UrlDecode(Request.QueryString["NetObject"])); }
    }

    protected override Control GetResourceWrapperControl()
    {
        return Wrapper;
    }

    protected override void OnInitResource(EventArgs e)
    {
        base.OnInitResource(e);
        SearchableColumns = ResourceSearchabelColumns;
    }

    protected override DataTable LoadResourceData(string searchText)
    {
        return UDTUserDAL.GetEndpointLoginsForUser(UserID, searchText, false);
    }

    protected override void RenderResource()
    {
        if (Data == null || Data .Rows .Count == 0)
            return;

        var data = SelectUniqueEndpoints(Data);

        data = ApplyFiltering(data);

        data = ApplyPaging(data);

        foreach (DataRow row in data.Rows )
        {
            var logonDateTime = row[0].ToString();
            var endpointValue = row[1].ToString();
            var endpointType = (int)row[2];
            var watchID = DALHelper.GetValueOrDefault(row[3], 0);

            var htmlRow = new HtmlTableRow();

            HtmlHelper.AddHtmlCell(htmlRow,
                (cell) => cell.InnerHtml = string.Format("{0}{1}",
                                               watchID > 0 ? IconHelper.GetWatchIcon(watchID, endpointValue) + " " : "",
                                               endpointType == 3 ? HtmlHelper.CreateUdtViewHref("dns", endpointValue, endpointValue) : HtmlHelper.CreateUdtViewHref("ip", endpointValue, endpointValue)));

            HtmlHelper.AddHtmlCell(htmlRow, cell => cell.InnerHtml = string.Format("&nbsp;{0}", logonDateTime));

            UserEndpointLogins.Rows.Add(htmlRow);
        }
    }

    private static DataTable SelectUniqueEndpoints(DataTable data)
    {
        // if there is multiple rows for the same EndpointVlaue, e.g. ip and dns added into WatchList as separate entries, but are actually match each other,
        // we need to select only one. DNS (type = 3) has priotity.

        var output = data.Clone();
    
        var grouped = data.Rows.Cast<DataRow>().GroupBy(r => r["EndpointValue"].ToString());

        foreach (var group in grouped)
        {
            var row = group
                .OrderByDescending(r => r["WatchItemtype"] != DBNull.Value ? r["WatchItemtype"].ToString() : r["EndpointType"].ToString())
                .First();

            output.ImportRow(row);
        }

        using (var view = new DataView(output))
        {
            view.Sort = "LogonDateTime DESC";
            return view.ToTable();
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IUDTUserProvider) }; }
    }
}



