﻿<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true" CodeFile="EditUserEndpointLogins.aspx.cs" Inherits="Orion_UDT_Resources_UserDetails_EditUserEndpointLogins" %>
<%@ Import Namespace="SolarWinds.Orion.Web" %>
<%@ Import Namespace="SolarWinds.UDT.Web.Helpers" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" Assembly="OrionWeb" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= String.Format(Resources.UDTWebContent.UDTWEBDATA_VB1_135, CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Resource.Title, Request["NetObject"].UrlEncode()))) %></h1>
    <div>
        <%= Resources.UDTWebContent.UDTWEBDATA_VB1_192 %><br />
        <asp:TextBox runat="server" ID="maxRowsCount" Columns="5"/>
        <br /><br/>
        <orion:LocalizableButton runat="server" ID="btnSubmit" OnClick="SubmitClick" DisplayType="Primary" LocalizedText="Submit"/>                  
        <br /><br />

    </div>
</asp:Content>

