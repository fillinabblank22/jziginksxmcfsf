﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, "Help")]
public partial class Orion_UDT_Resources_Misc_GettingStarted : BaseResourceControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.Wrapper.ShowEditButton = false;
        btnRemoveGettingStarted.PostBackUrl = string.Format("/Orion/UDT/RemoveResource.aspx?ViewPath={0}&ResourceName={1}&ResourceNameShort={2}",
            Page.Request.Url,
            Title,
            ShortName);
    }

    public string WrapperHeaderID
    {
        get
        {
            return this.Controls[0].Controls[0].ClientID;
        }
    }

    public string DiscoveryDCLink
    {
        get { return "/Orion/UDT/Admin/Discovery/Default.aspx"; }
    }

    public string CentralDiscoveryPortsLink
    {
        get { return "/Orion/Discovery/Default.aspx"; }
    }

    public string PortManagementLink
    {
        get { return "/Orion/UDT/Ports/Default.aspx"; }
    }

    public string ManageWatchListLink
    {
        get { return "/Orion/UDT/ManageWatchList.aspx"; }
    }

    public string SearchLink
    {
        get { return "/Orion/UDT/Search/SearchResults.aspx"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.UDTWebContent.UDTWEBDATA_AK1_196; }
    }

    public string ShortName
    {
        get { return Resources.UDTWebContent.UDTWEBDATA_AK1_197; }
    }

}