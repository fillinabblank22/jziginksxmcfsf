﻿<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true" CodeFile="EditRogueDevices.aspx.cs" Inherits="Orion_UDT_Resources_UserDetails_EditEndpointUserLogins" %>
<%@ Import Namespace="SolarWinds.Orion.Web" %>
<%@ Import Namespace="SolarWinds.UDT.Web.Helpers" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" Assembly="OrionWeb" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= String.Format(Resources.UDTWebContent.UDTWEBDATA_VB1_135, CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Resource.Title, Request["NetObject"].UrlEncode()))) %></h1>
    <div>
        <orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="false" ShowSubTitleHintMessage="false" />
        <%= Resources.UDTWebContent.UDTWEBDATA_AK1_188 %>
        <br /><br />         
        <b><%= Resources.UDTWebContent.UDTWEBDATA_VB1_151 %></b>
        <br /><br />
        <asp:DropDownList runat="server" ID="Period" />
        <br /><br />
        <b><%= Resources.UDTWebContent.UDTWEBDATA_DF1_24%></b><br />
        <asp:TextBox runat="server" ID="maxRowsCount" Columns="5"/>
        <br /><br/>
        <orion:LocalizableButton runat="server" ID="btnSubmit" OnClick="SubmitClick" LocalizedText="Submit" DisplayType="Primary"/>                    
        <br /><br />

    </div>
</asp:Content>

