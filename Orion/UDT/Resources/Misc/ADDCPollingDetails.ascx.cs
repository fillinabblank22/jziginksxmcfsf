﻿using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using SolarWinds.UDT.Common;
using SolarWinds.UDT.Common.Models;
using SolarWinds.UDT.Web.NetObjects;
using SolarWinds.UDT.Web.DAL;
using System.Collections.Generic;

public partial class Orion_UDT_Resources_Misc: SolarWinds.Orion.Web.UI.BaseResourceControl
{
    private static SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();
    private int _nodeId;
    private int _credentialsId;
    private string _nodeIp;
    protected UDTCapability Capability { get; set; }
    protected string NetObjectID { get; set; }

    protected override String DefaultTitle
    {
        get { return Resources.UDTWebContent.UDTWEBCODE_AK1_31; }
    }

    public override string HelpLinkFragment
    {
        get
        {
          
            return "OrionUDTPHADPollingDetails";
        }
    }
    
    public int NodeID
    {
        get { return _nodeId; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["NetObject"] == null)
            {
                if (!string.IsNullOrEmpty(Resource.Properties["netobjectid"]))
                {
                    NetObjectID = Resource.Properties["netobjectid"];
                }
            }
            else
            {
                NetObjectID = Request.QueryString["NetObject"] ;
            }


            //got a netobject so need to just get port details for that node
            string[] netObjectParts = NetObjectID.Split(':');
            if (netObjectParts.Length == 2)
            {
                //Check if node is a Domain Controller
                try
                {
                    using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
                    {
                        _nodeId = int.Parse(netObjectParts[1]);
                        Capability = bl.PollingGetNodeCapability(_nodeId, UDTNodeCapability.DomainController);
                    }
                }
                catch (Exception ex)
                {
                    _log.ErrorFormat("An error occurred contacting the UDT Business Layer (ADDCPollingDetails OnInit: UDTNodeCapability(DC)).  Details: {0}", ex.ToString());
                }
            }

            UDTDeviceDAL dal = new UDTDeviceDAL();
            _nodeIp = dal.GetNodeIpFromId(_nodeId);
			
			dcPollingStatusIcon.ImageUrl = "/Orion/images/loading_gen_small.gif";
            dcPollingStatus.Text = Resources.UDTWebContent.UDTWEBCODE_AK1_32;
        }

        // if node is not a DC, or error occurred getting node capabilities, or no netobject provided; hide the resource.
        if ((Capability==null) || (Capability.Enabled == false) || (string.IsNullOrEmpty(NetObjectID)))
        {
            Wrapper.Visible = false;
            return;
        }

        this.dcPollingInterval.Text = String.Format(Resources.UDTWebContent.UDTWEBCODE_AK1_33, Capability.PollingIntervalMinutes);

        try
        {
            DateTime nextPoll = Convert.ToDateTime(Capability.LastSuccessfulScan + TimeSpan.FromMinutes(Capability.PollingIntervalMinutes)).ToLocalTime();
            this.dcNextPoll.Text = Utils.FormatDateTime(nextPoll);
        }
        catch
        {
            this.dcNextPoll.Text = String.Empty;
        }

        try
        {
            DateTime lastSuccessfulPoll = Convert.ToDateTime(Capability.LastSuccessfulScan).ToLocalTime();
            this.dcLastSuccessfulPoll.Text = Utils.FormatDateTime(lastSuccessfulPoll, true);
        }
        catch
        {
            this.dcLastSuccessfulPoll.Text = String.Empty;
        }
    
    }
   
    protected void assignSecurityCredentials_Click(object sender, EventArgs e)
    {
        // ToDo: do something to assign domain controller security creds
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }

    public override ResourceLoadingMode ResourceLoadingMode 
	{ 
		get { return ResourceLoadingMode.RenderControl; }
	}
}



