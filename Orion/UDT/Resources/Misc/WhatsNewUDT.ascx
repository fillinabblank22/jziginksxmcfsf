﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WhatsNewUDT.ascx.cs"
    Inherits="Orion_UDT_Resources_WhatsNew_WhatsNewUDT" %>
<orion:resourceWrapper ID="Wrapper" runat="server">
    <Content>
        <style type="text/css">
            #whatsnew h1
            {
                font-size: 11px;
                font-weight: bold;
                font-family: Verdana;
                color: Gray;
                padding: 5px;
            }
            #whatsnew a
            {
                color: #336699;
            }
            #whatsnew td
            {
                border-bottom: 0px;
            }            
            #whatsnew td a img
            {
                vertical-align: baseline;
            }
            #whatsnew ul
            {
                padding-left: 22px; 
                margin-bottom: 5px;
            }                        
            #whatsnew li
            {
                list-style-type:square;
            }
    	   .sw-btn:hover 
    	   {
		color: #204a63 !important;
    	   }	    
        </style>
        <div id="whatsnew">
            <table>
                <colgroup>
                    <col width="auto" />
                    <col width="100%" />
                </colgroup>
                <tr>
                    <td valign="top">
                        <img ID="WhatNewIcon" src="/Orion/UDT/images/icon_new.gif" />
                    </td>
                    <td>
                        
                        <h1><%= Resources.UDTWebContent.WhatsNewInUdt_Title%></h1>
                        <ul>
                            <li>
                                <%= Resources.UDTWebContent.WhatsNewInUdt_Item1%><br />
                            </li>
                            <li>
                                <%= Resources.UDTWebContent.WhatsNewInUdt_Item2%><br />
                            </li>
                            <li>
                                <%= Resources.UDTWebContent.WhatsNewInUdt_Item3%><br />
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                         <orion:LocalizableButtonLink runat="server" Target="_blank" NavigateUrl="https://www.solarwinds.com/documentation/kbloader.aspx?kb=UDT_2020-2_release_notes" Text="<%$ Resources: UDTWebContent, WhatsNewInUdt_LearnMore %>" DisplayType="Primary"  />
                        &nbsp;
                        <orion:LocalizableButton runat="server" ID="btnRemoveResource" OnClick="OnRemoveResourceClick" Text="<%$ Resources: UDTWebContent, UDTWEBDATA_DF1_49 %>" DisplayType="secondary" />
                    </td>
                </tr>
            </table>
        </div>
    </Content>
</orion:resourceWrapper>
