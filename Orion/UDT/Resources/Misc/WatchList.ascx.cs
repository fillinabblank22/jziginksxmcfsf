﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Web.Helpers;
using SolarWinds.UDT.Web.NetObjects;
using SolarWinds.UDT.Web.Resources;


public partial class Orion_UDT_Resources_Misc_WatchList : UDTBaseResourceControl
{
    private static readonly Log _log = new Log("WatchListResource");
    protected override Log GetLogger() { return _log; }

    private static readonly string[] ResourceSearchabelColumns = new[] {"WatchName", "MACAddress", "IPAddress", "DNSName", "NodeName", "PortName", "APName", "clSSID","UserName" };

    protected override String DefaultTitle
    {
        get { return Resources.UDTWebContent.UDTWEBCODE_AK1_36; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionUDTPHWatchListOverview";
        }
    }

    public override string EditURL
    {
        get { return string.Format("/Orion/UDT/Resources/Misc/EditWatchList.aspx?ResourceID={0}&ViewID={1}&NetObjectID={2}", this.Resource.ID, this.Resource.View.ViewID, HttpUtility.UrlDecode(Request.QueryString["NetObject"])); }
    }

    protected override void OnInitResource(EventArgs e)
    {
        Wrapper.ManageButtonImage = "~/Orion/UDT/Images/Resources/button_manage_list.gif";
        Wrapper.ManageButtonTarget = "~/Orion/UDT/ManageWatchList.aspx";
        Wrapper.ShowManageButton =  Profile.AllowNodeManagement;

        SearchableColumns = ResourceSearchabelColumns;
    }

    protected override Control GetResourceWrapperControl()
    {
        return Wrapper;
    }

    protected override DataTable LoadResourceData(string searchText)
    {
        return UDTWatchListDAL.PopulateWatchList();
    }

    protected override void RenderResource()
    {
        var data = ApplyFiltering(Data);
        

        // order rows
        if (data.Rows.Count > 1)
        {
            DataTable output;
            using (var view = new DataView(data))
            {
                view.RowFilter = "WatchName IS NOT NULL AND LEN(WatchName) > 1";
                view.Sort = "WatchName ASC, IsActive DESC, ConnectionType ASC, LastSeen DESC";
                output = view.ToTable();
            }
            if (output.Rows.Count < data.Rows.Count)
            {
                using (var view = new DataView(data))
                {
                    view.RowFilter = "WatchName IS NULL OR LEN(WatchName) = 0";
                    view.Sort = "IsActive DESC, ConnectionType ASC, LastSeen DESC";
                    output.Merge(view.ToTable());
                }
            }
            data = output;
        }

        data = ApplyPaging(data);

        //Setting the visibility of VRF Column based on the VRF column values in Watchlist entries
        var isVrfVisible = data.Rows.Cast<DataRow>().Any(r => r["VrfName"] != DBNull.Value && r["VrfName"].ToString() != string.Empty);

        for (int i=0; i< DeviceWatchList.Rows[0].Cells.Count;i++)
        {
            if (DeviceWatchList.Rows[0].Cells[i].ID!= null && DeviceWatchList.Rows[0].Cells[i].ID.ToLower() == "vrf")
            {
                DeviceWatchList.Rows[0].Cells[i].Visible = isVrfVisible;
                break;
            }
                
        }
       
       // Fill html table
        foreach (DataRow row in data.Rows)
        {
            var htmlRow = new HtmlTableRow();
            HtmlTableCell htmlCell;

            var watchID = DALHelper.GetValueOrDefault(row["WatchID"], 0);
            var watchName = DALHelper.GetValueOrDefault(row["WatchName"], string.Empty);
            var errorMessage = DALHelper.GetValueOrDefault(row["ErrorMessage"], string.Empty);
            var isActive = DALHelper.GetValueOrDefault(row["IsActive"], false);
            var isFound = DALHelper.GetValueOrDefault(row["IsFound"], false);
            var isWireless = DALHelper.GetValueOrDefault(row["IsWireless"], false);

            if (isActive)
            {
                var ipAddress = DALHelper.GetValueOrDefault(row["IPAddress"], string.Empty);
                var macAddress = DALHelper.GetValueOrDefault(row["MACAddress"], string.Empty);
                var hostName = DALHelper.GetValueOrDefault(row["DNSName"], string.Empty);
                var userName = DALHelper.GetValueOrDefault(row["UserName"], string.Empty);
                var userId = DALHelper.GetValueOrDefault(row["UserID"], 0);
                 
                var nodeName = DALHelper.GetValueOrDefault(row["NodeName"], string.Empty);
                var connectionType = DALHelper.GetValueOrDefault(row["ConnectionType"], 3);
                var nodeID = DALHelper.GetValueOrDefault(row["NodeID"], 0);

                var portID = DALHelper.GetValueOrDefault(row["PortID"], 0);
                var portName = DALHelper.GetValueOrDefault(row["PortName"], string.Empty);
                var vlanID = DALHelper.GetValueOrDefault(row["VlanID"], 0) == 0 ? string.Empty : DALHelper.GetValueOrDefault(row["VlanID"], 0).ToString();
                var VrfName = DALHelper.GetValueOrDefault(row["VrfName"], string.Empty);

                var apName = DALHelper.GetValueOrDefault(row["APName"], string.Empty);
                var apID = DALHelper.GetValueOrDefault(row["APID"], 0L);
                var clSSID = DALHelper.GetValueOrDefault(row["clSSID"], string.Empty);
                var ifSSID = DALHelper.GetValueOrDefault(row["ifSSID"], string.Empty);

                //Status
                htmlCell = new HtmlTableCell();
                htmlCell.Attributes.Add("Class", "Property");
                htmlCell.InnerHtml = String.Format("<a href='/Orion/View.aspx?NetObject=UW:{0}'><img src='/Orion/UDT/Images/icon_user_active_dot.gif'/></a>", watchID);
                htmlRow.Cells.Add(htmlCell);

                //Name
                htmlCell = new HtmlTableCell();
                htmlCell.Attributes.Add("Class", "Property");
                htmlCell.InnerHtml = String.Format("<a href='/Orion/View.aspx?NetObject=UW:{0}'>{1}</a>", watchID, watchName == string.Empty ? "&nbsp;" : watchName);
                htmlRow.Cells.Add(htmlCell);

                //IP Address
                htmlCell = new HtmlTableCell();
                htmlCell.Attributes.Add("Class", "Property");
                htmlCell.InnerHtml = ipAddress == string.Empty
                                            ? "&nbsp;"
                                            : String.Format("<a href='/Orion/UDT/EndpointDetails.aspx?NetObject=UE-IP:VAL={1}';>{0}</a>", ipAddress, DALHelper.IpAddressToStorageFormat(ipAddress));
                htmlRow.Cells.Add(htmlCell);

                //Host
                htmlCell = new HtmlTableCell();
                htmlCell.Attributes.Add("Class", "Property");
                htmlCell.InnerHtml = hostName == string.Empty
                                            ? "&nbsp;"
                                            : String.Format("<a href='/Orion/UDT/EndpointDetails.aspx?NetObject=UE-DNS:VAL={0}';>{0}</a>", hostName);
                htmlRow.Cells.Add(htmlCell);



                //Mac Address
                htmlCell = new HtmlTableCell();
                htmlCell.Attributes.Add("Class", "Property");
                string renderConnectionType;
                switch (connectionType)
                {
                    case 1:
                        renderConnectionType =
                            String.Format("<img src='/Orion/UDT/Images/icon_direct.png' title='{0}' alt='{0}' style='vertical-align:bottom;'/>&nbsp;", Resources.UDTWebContent.UDTWEBCODE_AK1_37);
                        break;
                    case 2:
                        renderConnectionType =
                            String.Format("<img src='/Orion/UDT/Images/icon_indirect.png' title='{0}' alt='{0}' style='vertical-align:bottom;'/>&nbsp;", Resources.UDTWebContent.UDTWEBCODE_AK1_38);
                        break;
                    default:
                        renderConnectionType =
                            String.Format("<img src='/Orion/UDT/Images/icon_direct_unknown.png' title='{0}' alt='{0}' style='vertical-align:bottom;'/>&nbsp;", Resources.UDTWebContent.UDTWEBCODE_AK1_39);
                        break;
                }
                htmlCell.InnerHtml = string.IsNullOrEmpty(macAddress)
                                            ? "&nbsp;"
                                            : String.Format("<span style=\"white-space: nowrap;\">{1}<a href='/Orion/UDT/EndpointDetails.aspx?NetObject=UE-MAC:VAL={0}';>{0}</a></span>", macAddress, renderConnectionType);
                htmlRow.Cells.Add(htmlCell);

                //User Name
                htmlCell = new HtmlTableCell();
                htmlCell.Attributes.Add("Class", "Property");
                htmlCell.InnerHtml = userName == string.Empty
                                            ? "&nbsp;"
                                            : String.Format("<a href='/Orion/UDT/UserDetails.aspx?NetObject=UU:{0}';>{1}</a>", userId, userName);
                htmlRow.Cells.Add(htmlCell);

                //Switch / AP
                htmlCell = new HtmlTableCell();
                htmlCell.Attributes.Add("Class", "Property");
                htmlCell.InnerHtml = !isWireless && nodeID > 0 && nodeName != string.Empty
                                         ? HtmlHelper.CreateUdtViewHref("node", nodeID.ToString(CultureInfo.InvariantCulture), nodeName)
                                         : isWireless && nodeID > 0 && apName != string.Empty
                                               ? HtmlHelper.CreateUdtViewHref("ap", apID.ToString(CultureInfo.InvariantCulture), apName)
                                               : "&nbsp;";
                htmlRow.Cells.Add(htmlCell);

                //Port# / SSID
                htmlCell = new HtmlTableCell();
                htmlCell.Attributes.Add("Class", "Property");
                htmlCell.InnerHtml = !isWireless && portID > 0 && portName != string.Empty
                                         ? HtmlHelper.CreateUdtViewHref("port", portID.ToString(CultureInfo.InvariantCulture), portName)
                                         : isWireless && clSSID != string.Empty
                                               ? HtmlHelper.CreateUdtViewHref("ssid", clSSID, clSSID)
                                               : "&nbsp;";
                htmlRow.Cells.Add(htmlCell);

                //VLAN
                htmlCell = new HtmlTableCell();
                htmlCell.Attributes.Add("Class", "Property");
                htmlCell.InnerHtml = vlanID == string.Empty ? "&nbsp;" : vlanID;
                htmlRow.Cells.Add(htmlCell);

                //VRF
                if (isVrfVisible)
                {
                    htmlCell = new HtmlTableCell();
                    htmlCell.Attributes.Add("Class", "Property");
                    htmlCell.InnerHtml = VrfName == string.Empty ? "&nbsp;" : VrfName;
                    htmlRow.Cells.Add(htmlCell);
                }
            }
            else
            {
                var watchItem = DALHelper.GetValueOrDefault(row["WatchItem"], string.Empty);
                var displayName = watchName != string.Empty ? watchName : watchItem;
                var lastSeen = DALHelper.GetValueOrDefault(row["LastSeen"].ToString(), string.Empty);
                //Status
                htmlCell = new HtmlTableCell();
                htmlCell.Attributes.Add("Class", "Property");
                htmlCell.Style.Add("font-style", "italic");
                if (UDTWatchListDAL.UDTNetObjectTypeFromWatchType(DALHelper.GetValueOrDefault(row["WatchItemType"], string.Empty)) == UDTNetObjectType.User && (!isFound)) // if User is not found so far
                {   
                     htmlCell.InnerHtml = String.Format("<img src='/Orion/UDT/Images/icon_user_inactive_dot.gif'/>");    
                }
                else 
                {
                    htmlCell.InnerHtml =
                    String.Format(
                        "<a href='/Orion/View.aspx?NetObject=UW:{0}'><img src='/Orion/UDT/Images/icon_user_inactive_dot.gif'/></a>",
                        watchID);
                }
                htmlRow.Cells.Add(htmlCell);

                //Description
                htmlCell = new HtmlTableCell();
                htmlCell.Attributes.Add("Class", "Property");
                htmlCell.Style.Add("font-style", "italic");
                htmlCell.ColSpan = 8;
                htmlCell.InnerHtml = errorMessage != string.Empty
                                         ? string.Format(Resources.UDTWebContent.UDTWEBCODE_AK1_40, displayName)
                                         : isFound
                                               ? string.Format(Resources.UDTWebContent.UDTWEBCODE_AK1_41, displayName, lastSeen)
                                               : string.Format(Resources.UDTWebContent.UDTWEBCODE_AK1_42, displayName);

                htmlRow.Cells.Add(htmlCell);
            }
            DeviceWatchList.Rows.Add(htmlRow);
        }
    }
}



